package io.michaelrocks.libphonenumber.android;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.ArrayList;
import java.util.List;

public final class i {
   public static class a implements Externalizable {
      private boolean a;
      private String b = "";
      private boolean c;
      private String d = "";
      private List e = new ArrayList();
      private boolean f;
      private String g = "";
      private boolean h;
      private boolean i = false;
      private boolean j;
      private String k = "";

      public i.a a(String var1) {
         this.a = true;
         this.b = var1;
         return this;
      }

      public i.a a(boolean var1) {
         this.h = true;
         this.i = var1;
         return this;
      }

      public String a() {
         return this.b;
      }

      public String a(int var1) {
         return (String)this.e.get(var1);
      }

      public i.a b(String var1) {
         this.c = true;
         this.d = var1;
         return this;
      }

      public String b() {
         return this.d;
      }

      public int c() {
         return this.e.size();
      }

      public i.a c(String var1) {
         this.f = true;
         this.g = var1;
         return this;
      }

      public i.a d(String var1) {
         this.j = true;
         this.k = var1;
         return this;
      }

      public String d() {
         return this.g;
      }

      public String e() {
         return this.k;
      }

      public void readExternal(ObjectInput var1) throws IOException {
         this.a(var1.readUTF());
         this.b(var1.readUTF());
         int var3 = var1.readInt();

         for(int var2 = 0; var2 < var3; ++var2) {
            this.e.add(var1.readUTF());
         }

         if(var1.readBoolean()) {
            this.c(var1.readUTF());
         }

         if(var1.readBoolean()) {
            this.d(var1.readUTF());
         }

         this.a(var1.readBoolean());
      }

      public void writeExternal(ObjectOutput var1) throws IOException {
         var1.writeUTF(this.b);
         var1.writeUTF(this.d);
         int var3 = this.c();
         var1.writeInt(var3);

         for(int var2 = 0; var2 < var3; ++var2) {
            var1.writeUTF((String)this.e.get(var2));
         }

         var1.writeBoolean(this.f);
         if(this.f) {
            var1.writeUTF(this.g);
         }

         var1.writeBoolean(this.j);
         if(this.j) {
            var1.writeUTF(this.k);
         }

         var1.writeBoolean(this.i);
      }
   }

   public static class b implements Externalizable {
      private boolean A;
      private i.d B = null;
      private boolean C;
      private i.d D = null;
      private boolean E;
      private i.d F = null;
      private boolean G;
      private i.d H = null;
      private boolean I;
      private String J = "";
      private boolean K;
      private int L = 0;
      private boolean M;
      private String N = "";
      private boolean O;
      private String P = "";
      private boolean Q;
      private String R = "";
      private boolean S;
      private String T = "";
      private boolean U;
      private String V = "";
      private boolean W;
      private String X = "";
      private boolean Y;
      private boolean Z = false;
      private boolean a;
      private List aa = new ArrayList();
      private List ab = new ArrayList();
      private boolean ac;
      private boolean ad = false;
      private boolean ae;
      private String af = "";
      private boolean ag;
      private boolean ah = false;
      private boolean ai;
      private boolean aj = false;
      private i.d b = null;
      private boolean c;
      private i.d d = null;
      private boolean e;
      private i.d f = null;
      private boolean g;
      private i.d h = null;
      private boolean i;
      private i.d j = null;
      private boolean k;
      private i.d l = null;
      private boolean m;
      private i.d n = null;
      private boolean o;
      private i.d p = null;
      private boolean q;
      private i.d r = null;
      private boolean s;
      private i.d t = null;
      private boolean u;
      private i.d v = null;
      private boolean w;
      private i.d x = null;
      private boolean y;
      private i.d z = null;

      public i.b a(int var1) {
         this.K = true;
         this.L = var1;
         return this;
      }

      public i.b a(i.d var1) {
         if(var1 == null) {
            throw new NullPointerException();
         } else {
            this.a = true;
            this.b = var1;
            return this;
         }
      }

      public i.b a(String var1) {
         this.I = true;
         this.J = var1;
         return this;
      }

      public i.b a(boolean var1) {
         this.Y = true;
         this.Z = var1;
         return this;
      }

      public i.d a() {
         return this.b;
      }

      public i.b b(i.d var1) {
         if(var1 == null) {
            throw new NullPointerException();
         } else {
            this.c = true;
            this.d = var1;
            return this;
         }
      }

      public i.b b(String var1) {
         this.M = true;
         this.N = var1;
         return this;
      }

      public i.b b(boolean var1) {
         this.ac = true;
         this.ad = var1;
         return this;
      }

      public i.d b() {
         return this.d;
      }

      public i.b c(i.d var1) {
         if(var1 == null) {
            throw new NullPointerException();
         } else {
            this.e = true;
            this.f = var1;
            return this;
         }
      }

      public i.b c(String var1) {
         this.O = true;
         this.P = var1;
         return this;
      }

      public i.b c(boolean var1) {
         this.ag = true;
         this.ah = var1;
         return this;
      }

      public i.d c() {
         return this.f;
      }

      public i.b d(i.d var1) {
         if(var1 == null) {
            throw new NullPointerException();
         } else {
            this.g = true;
            this.h = var1;
            return this;
         }
      }

      public i.b d(String var1) {
         this.Q = true;
         this.R = var1;
         return this;
      }

      public i.b d(boolean var1) {
         this.ai = true;
         this.aj = var1;
         return this;
      }

      public i.d d() {
         return this.h;
      }

      public i.b e(i.d var1) {
         if(var1 == null) {
            throw new NullPointerException();
         } else {
            this.i = true;
            this.j = var1;
            return this;
         }
      }

      public i.b e(String var1) {
         this.S = true;
         this.T = var1;
         return this;
      }

      public i.d e() {
         return this.j;
      }

      public i.b f(i.d var1) {
         if(var1 == null) {
            throw new NullPointerException();
         } else {
            this.k = true;
            this.l = var1;
            return this;
         }
      }

      public i.b f(String var1) {
         this.U = true;
         this.V = var1;
         return this;
      }

      public i.d f() {
         return this.l;
      }

      public i.b g(i.d var1) {
         if(var1 == null) {
            throw new NullPointerException();
         } else {
            this.m = true;
            this.n = var1;
            return this;
         }
      }

      public i.b g(String var1) {
         this.W = true;
         this.X = var1;
         return this;
      }

      public i.d g() {
         return this.n;
      }

      public i.b h(i.d var1) {
         if(var1 == null) {
            throw new NullPointerException();
         } else {
            this.o = true;
            this.p = var1;
            return this;
         }
      }

      public i.b h(String var1) {
         this.ae = true;
         this.af = var1;
         return this;
      }

      public i.d h() {
         return this.p;
      }

      public i.b i(i.d var1) {
         if(var1 == null) {
            throw new NullPointerException();
         } else {
            this.q = true;
            this.r = var1;
            return this;
         }
      }

      public i.d i() {
         return this.r;
      }

      public i.b j(i.d var1) {
         if(var1 == null) {
            throw new NullPointerException();
         } else {
            this.s = true;
            this.t = var1;
            return this;
         }
      }

      public i.d j() {
         return this.t;
      }

      public i.b k(i.d var1) {
         if(var1 == null) {
            throw new NullPointerException();
         } else {
            this.u = true;
            this.v = var1;
            return this;
         }
      }

      public i.d k() {
         return this.x;
      }

      public int l() {
         return this.L;
      }

      public i.b l(i.d var1) {
         if(var1 == null) {
            throw new NullPointerException();
         } else {
            this.w = true;
            this.x = var1;
            return this;
         }
      }

      public i.b m(i.d var1) {
         if(var1 == null) {
            throw new NullPointerException();
         } else {
            this.y = true;
            this.z = var1;
            return this;
         }
      }

      public String m() {
         return this.N;
      }

      public i.b n(i.d var1) {
         if(var1 == null) {
            throw new NullPointerException();
         } else {
            this.A = true;
            this.B = var1;
            return this;
         }
      }

      public boolean n() {
         return this.S;
      }

      public i.b o(i.d var1) {
         if(var1 == null) {
            throw new NullPointerException();
         } else {
            this.C = true;
            this.D = var1;
            return this;
         }
      }

      public String o() {
         return this.T;
      }

      public i.b p(i.d var1) {
         if(var1 == null) {
            throw new NullPointerException();
         } else {
            this.E = true;
            this.F = var1;
            return this;
         }
      }

      public String p() {
         return this.V;
      }

      public i.b q(i.d var1) {
         if(var1 == null) {
            throw new NullPointerException();
         } else {
            this.G = true;
            this.H = var1;
            return this;
         }
      }

      public String q() {
         return this.X;
      }

      public List r() {
         return this.aa;
      }

      public void readExternal(ObjectInput var1) throws IOException {
         byte var3 = 0;
         i.d var5;
         if(var1.readBoolean()) {
            var5 = new i.d();
            var5.readExternal(var1);
            this.a(var5);
         }

         if(var1.readBoolean()) {
            var5 = new i.d();
            var5.readExternal(var1);
            this.b(var5);
         }

         if(var1.readBoolean()) {
            var5 = new i.d();
            var5.readExternal(var1);
            this.c(var5);
         }

         if(var1.readBoolean()) {
            var5 = new i.d();
            var5.readExternal(var1);
            this.d(var5);
         }

         if(var1.readBoolean()) {
            var5 = new i.d();
            var5.readExternal(var1);
            this.e(var5);
         }

         if(var1.readBoolean()) {
            var5 = new i.d();
            var5.readExternal(var1);
            this.f(var5);
         }

         if(var1.readBoolean()) {
            var5 = new i.d();
            var5.readExternal(var1);
            this.g(var5);
         }

         if(var1.readBoolean()) {
            var5 = new i.d();
            var5.readExternal(var1);
            this.h(var5);
         }

         if(var1.readBoolean()) {
            var5 = new i.d();
            var5.readExternal(var1);
            this.i(var5);
         }

         if(var1.readBoolean()) {
            var5 = new i.d();
            var5.readExternal(var1);
            this.j(var5);
         }

         if(var1.readBoolean()) {
            var5 = new i.d();
            var5.readExternal(var1);
            this.k(var5);
         }

         if(var1.readBoolean()) {
            var5 = new i.d();
            var5.readExternal(var1);
            this.l(var5);
         }

         if(var1.readBoolean()) {
            var5 = new i.d();
            var5.readExternal(var1);
            this.m(var5);
         }

         if(var1.readBoolean()) {
            var5 = new i.d();
            var5.readExternal(var1);
            this.n(var5);
         }

         if(var1.readBoolean()) {
            var5 = new i.d();
            var5.readExternal(var1);
            this.o(var5);
         }

         if(var1.readBoolean()) {
            var5 = new i.d();
            var5.readExternal(var1);
            this.p(var5);
         }

         if(var1.readBoolean()) {
            var5 = new i.d();
            var5.readExternal(var1);
            this.q(var5);
         }

         this.a(var1.readUTF());
         this.a(var1.readInt());
         this.b(var1.readUTF());
         if(var1.readBoolean()) {
            this.c(var1.readUTF());
         }

         if(var1.readBoolean()) {
            this.d(var1.readUTF());
         }

         if(var1.readBoolean()) {
            this.e(var1.readUTF());
         }

         if(var1.readBoolean()) {
            this.f(var1.readUTF());
         }

         if(var1.readBoolean()) {
            this.g(var1.readUTF());
         }

         this.a(var1.readBoolean());
         int var4 = var1.readInt();

         int var2;
         i.a var6;
         for(var2 = 0; var2 < var4; ++var2) {
            var6 = new i.a();
            var6.readExternal(var1);
            this.aa.add(var6);
         }

         var4 = var1.readInt();

         for(var2 = var3; var2 < var4; ++var2) {
            var6 = new i.a();
            var6.readExternal(var1);
            this.ab.add(var6);
         }

         this.b(var1.readBoolean());
         if(var1.readBoolean()) {
            this.h(var1.readUTF());
         }

         this.c(var1.readBoolean());
         this.d(var1.readBoolean());
      }

      public int s() {
         return this.aa.size();
      }

      public List t() {
         return this.ab;
      }

      public int u() {
         return this.ab.size();
      }

      public void writeExternal(ObjectOutput var1) throws IOException {
         byte var3 = 0;
         var1.writeBoolean(this.a);
         if(this.a) {
            this.b.writeExternal(var1);
         }

         var1.writeBoolean(this.c);
         if(this.c) {
            this.d.writeExternal(var1);
         }

         var1.writeBoolean(this.e);
         if(this.e) {
            this.f.writeExternal(var1);
         }

         var1.writeBoolean(this.g);
         if(this.g) {
            this.h.writeExternal(var1);
         }

         var1.writeBoolean(this.i);
         if(this.i) {
            this.j.writeExternal(var1);
         }

         var1.writeBoolean(this.k);
         if(this.k) {
            this.l.writeExternal(var1);
         }

         var1.writeBoolean(this.m);
         if(this.m) {
            this.n.writeExternal(var1);
         }

         var1.writeBoolean(this.o);
         if(this.o) {
            this.p.writeExternal(var1);
         }

         var1.writeBoolean(this.q);
         if(this.q) {
            this.r.writeExternal(var1);
         }

         var1.writeBoolean(this.s);
         if(this.s) {
            this.t.writeExternal(var1);
         }

         var1.writeBoolean(this.u);
         if(this.u) {
            this.v.writeExternal(var1);
         }

         var1.writeBoolean(this.w);
         if(this.w) {
            this.x.writeExternal(var1);
         }

         var1.writeBoolean(this.y);
         if(this.y) {
            this.z.writeExternal(var1);
         }

         var1.writeBoolean(this.A);
         if(this.A) {
            this.B.writeExternal(var1);
         }

         var1.writeBoolean(this.C);
         if(this.C) {
            this.D.writeExternal(var1);
         }

         var1.writeBoolean(this.E);
         if(this.E) {
            this.F.writeExternal(var1);
         }

         var1.writeBoolean(this.G);
         if(this.G) {
            this.H.writeExternal(var1);
         }

         var1.writeUTF(this.J);
         var1.writeInt(this.L);
         var1.writeUTF(this.N);
         var1.writeBoolean(this.O);
         if(this.O) {
            var1.writeUTF(this.P);
         }

         var1.writeBoolean(this.Q);
         if(this.Q) {
            var1.writeUTF(this.R);
         }

         var1.writeBoolean(this.S);
         if(this.S) {
            var1.writeUTF(this.T);
         }

         var1.writeBoolean(this.U);
         if(this.U) {
            var1.writeUTF(this.V);
         }

         var1.writeBoolean(this.W);
         if(this.W) {
            var1.writeUTF(this.X);
         }

         var1.writeBoolean(this.Z);
         int var4 = this.s();
         var1.writeInt(var4);

         int var2;
         for(var2 = 0; var2 < var4; ++var2) {
            ((i.a)this.aa.get(var2)).writeExternal(var1);
         }

         var4 = this.u();
         var1.writeInt(var4);

         for(var2 = var3; var2 < var4; ++var2) {
            ((i.a)this.ab.get(var2)).writeExternal(var1);
         }

         var1.writeBoolean(this.ad);
         var1.writeBoolean(this.ae);
         if(this.ae) {
            var1.writeUTF(this.af);
         }

         var1.writeBoolean(this.ah);
         var1.writeBoolean(this.aj);
      }
   }

   public static class c implements Externalizable {
      private List a = new ArrayList();

      public List a() {
         return this.a;
      }

      public int b() {
         return this.a.size();
      }

      public void readExternal(ObjectInput var1) throws IOException {
         int var3 = var1.readInt();

         for(int var2 = 0; var2 < var3; ++var2) {
            i.b var4 = new i.b();
            var4.readExternal(var1);
            this.a.add(var4);
         }

      }

      public void writeExternal(ObjectOutput var1) throws IOException {
         int var3 = this.b();
         var1.writeInt(var3);

         for(int var2 = 0; var2 < var3; ++var2) {
            ((i.b)this.a.get(var2)).writeExternal(var1);
         }

      }
   }

   public static class d implements Externalizable {
      private boolean a;
      private String b = "";
      private List c = new ArrayList();
      private List d = new ArrayList();
      private boolean e;
      private String f = "";

      public int a(int var1) {
         return ((Integer)this.c.get(var1)).intValue();
      }

      public i.d a(String var1) {
         this.a = true;
         this.b = var1;
         return this;
      }

      public String a() {
         return this.b;
      }

      public i.d b(String var1) {
         this.e = true;
         this.f = var1;
         return this;
      }

      public List b() {
         return this.c;
      }

      public int c() {
         return this.c.size();
      }

      public List d() {
         return this.d;
      }

      public int e() {
         return this.d.size();
      }

      public void readExternal(ObjectInput var1) throws IOException {
         byte var3 = 0;
         if(var1.readBoolean()) {
            this.a(var1.readUTF());
         }

         int var4 = var1.readInt();

         int var2;
         for(var2 = 0; var2 < var4; ++var2) {
            this.c.add(Integer.valueOf(var1.readInt()));
         }

         var4 = var1.readInt();

         for(var2 = var3; var2 < var4; ++var2) {
            this.d.add(Integer.valueOf(var1.readInt()));
         }

         if(var1.readBoolean()) {
            this.b(var1.readUTF());
         }

      }

      public void writeExternal(ObjectOutput var1) throws IOException {
         byte var3 = 0;
         var1.writeBoolean(this.a);
         if(this.a) {
            var1.writeUTF(this.b);
         }

         int var4 = this.c();
         var1.writeInt(var4);

         int var2;
         for(var2 = 0; var2 < var4; ++var2) {
            var1.writeInt(((Integer)this.c.get(var2)).intValue());
         }

         var4 = this.e();
         var1.writeInt(var4);

         for(var2 = var3; var2 < var4; ++var2) {
            var1.writeInt(((Integer)this.d.get(var2)).intValue());
         }

         var1.writeBoolean(this.e);
         if(this.e) {
            var1.writeUTF(this.f);
         }

      }
   }
}
