package io.michaelrocks.libphonenumber.android;

import android.content.res.AssetManager;
import java.io.IOException;
import java.io.InputStream;

public class b implements d {
   private final AssetManager a;

   public b(AssetManager var1) {
      this.a = var1;
   }

   public InputStream a(String var1) {
      var1 = var1.substring(1);

      InputStream var3;
      try {
         var3 = this.a.open(var1);
      } catch (IOException var2) {
         var3 = null;
      }

      return var3;
   }
}
