package io.michaelrocks.libphonenumber.android;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

final class g implements f {
   private final String a;
   private final String b;
   private final String c;
   private final e d;
   private final ConcurrentHashMap e;
   private final ConcurrentHashMap f;

   g(d var1) {
      this("/io/michaelrocks/libphonenumber/android/data/PhoneNumberMetadataProto", "/io/michaelrocks/libphonenumber/android/data/PhoneNumberAlternateFormatsProto", "/io/michaelrocks/libphonenumber/android/data/ShortNumberMetadataProto", var1);
   }

   g(String var1, String var2, String var3, d var4) {
      this.e = new ConcurrentHashMap();
      this.f = new ConcurrentHashMap();
      this.a = var1;
      this.b = var2;
      this.c = var3;
      this.d = new e(var4);
   }

   private boolean b(int var1) {
      List var3 = (List)c.a().get(Integer.valueOf(var1));
      boolean var2;
      if(var3.size() == 1 && "001".equals(var3.get(0))) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   public i.b a(int var1) {
      i.b var2;
      if(!this.b(var1)) {
         var2 = null;
      } else {
         var2 = this.d.a(Integer.valueOf(var1), this.f, this.a);
      }

      return var2;
   }

   public i.b a(String var1) {
      return this.d.a(var1, this.e, this.a);
   }
}
