package io.michaelrocks.libphonenumber.android;

import java.io.Serializable;

public final class j {
   public static class a implements Serializable {
      private boolean a;
      private int b = 0;
      private boolean c;
      private long d = 0L;
      private boolean e;
      private String f = "";
      private boolean g;
      private boolean h = false;
      private boolean i;
      private int j = 1;
      private boolean k;
      private String l = "";
      private boolean m;
      private j.a n;
      private boolean o;
      private String p = "";

      public a() {
         this.n = j.a.e;
      }

      public int a() {
         return this.b;
      }

      public j.a a(int var1) {
         this.a = true;
         this.b = var1;
         return this;
      }

      public j.a a(long var1) {
         this.c = true;
         this.d = var1;
         return this;
      }

      public j.a a(j.a var1) {
         if(var1 == null) {
            throw new NullPointerException();
         } else {
            this.m = true;
            this.n = var1;
            return this;
         }
      }

      public j.a a(String var1) {
         if(var1 == null) {
            throw new NullPointerException();
         } else {
            this.e = true;
            this.f = var1;
            return this;
         }
      }

      public j.a a(boolean var1) {
         this.g = true;
         this.h = var1;
         return this;
      }

      public boolean a(j.a var1) {
         boolean var3 = true;
         boolean var2;
         if(var1 == null) {
            var2 = false;
         } else {
            var2 = var3;
            if(this != var1) {
               if(this.b == var1.b && this.d == var1.d && this.f.equals(var1.f) && this.h == var1.h && this.j == var1.j && this.l.equals(var1.l) && this.n == var1.n && this.p.equals(var1.p)) {
                  var2 = var3;
                  if(this.n() == var1.n()) {
                     return var2;
                  }
               }

               var2 = false;
            }
         }

         return var2;
      }

      public long b() {
         return this.d;
      }

      public j.a b(int var1) {
         this.i = true;
         this.j = var1;
         return this;
      }

      public j.a b(String var1) {
         if(var1 == null) {
            throw new NullPointerException();
         } else {
            this.k = true;
            this.l = var1;
            return this;
         }
      }

      public j.a c(String var1) {
         if(var1 == null) {
            throw new NullPointerException();
         } else {
            this.o = true;
            this.p = var1;
            return this;
         }
      }

      public boolean c() {
         return this.e;
      }

      public String d() {
         return this.f;
      }

      public boolean e() {
         return this.g;
      }

      public boolean equals(Object var1) {
         boolean var2;
         if(var1 instanceof j.a && this.a((j.a)var1)) {
            var2 = true;
         } else {
            var2 = false;
         }

         return var2;
      }

      public boolean f() {
         return this.h;
      }

      public boolean g() {
         return this.i;
      }

      public int h() {
         return this.j;
      }

      public int hashCode() {
         short var2 = 1231;
         int var3 = this.a();
         int var5 = Long.valueOf(this.b()).hashCode();
         int var4 = this.d().hashCode();
         short var1;
         if(this.f()) {
            var1 = 1231;
         } else {
            var1 = 1237;
         }

         int var9 = this.h();
         int var7 = this.j().hashCode();
         int var6 = this.l().hashCode();
         int var8 = this.o().hashCode();
         if(!this.n()) {
            var2 = 1237;
         }

         return (((((var1 + (((var3 + 2173) * 53 + var5) * 53 + var4) * 53) * 53 + var9) * 53 + var7) * 53 + var6) * 53 + var8) * 53 + var2;
      }

      public boolean i() {
         return this.k;
      }

      public String j() {
         return this.l;
      }

      public boolean k() {
         return this.m;
      }

      public j.a l() {
         return this.n;
      }

      public j.a m() {
         this.m = false;
         this.n = j.a.e;
         return this;
      }

      public boolean n() {
         return this.o;
      }

      public String o() {
         return this.p;
      }

      public String toString() {
         StringBuilder var1 = new StringBuilder();
         var1.append("Country Code: ").append(this.b);
         var1.append(" National Number: ").append(this.d);
         if(this.e() && this.f()) {
            var1.append(" Leading Zero(s): true");
         }

         if(this.g()) {
            var1.append(" Number of leading zeros: ").append(this.j);
         }

         if(this.c()) {
            var1.append(" Extension: ").append(this.f);
         }

         if(this.k()) {
            var1.append(" Country Code Source: ").append(this.n);
         }

         if(this.n()) {
            var1.append(" Preferred Domestic Carrier Code: ").append(this.p);
         }

         return var1.toString();
      }
   }

   public static enum a {
      a,
      b,
      c,
      d,
      e;
   }
}
