package io.michaelrocks.libphonenumber.android.a;

import io.michaelrocks.libphonenumber.android.i;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class b implements a {
   private final c a = new c(100);

   public static a a() {
      return new b();
   }

   private static boolean a(CharSequence var0, Pattern var1, boolean var2) {
      Matcher var3 = var1.matcher(var0);
      if(!var3.lookingAt()) {
         var2 = false;
      } else if(var3.matches()) {
         var2 = true;
      }

      return var2;
   }

   public boolean a(CharSequence var1, i.d var2, boolean var3) {
      String var4 = var2.a();
      if(var4.length() == 0) {
         var3 = false;
      } else {
         var3 = a(var1, this.a.a(var4), var3);
      }

      return var3;
   }
}
