package io.michaelrocks.libphonenumber.android.a;

import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.regex.Pattern;

public class c {
   private c.a a;

   public c(int var1) {
      this.a = new c.a(var1);
   }

   public Pattern a(String var1) {
      Pattern var3 = (Pattern)this.a.a((Object)var1);
      Pattern var2 = var3;
      if(var3 == null) {
         var2 = Pattern.compile(var1);
         this.a.a(var1, var2);
      }

      return var2;
   }

   private static class a {
      private LinkedHashMap a;
      private int b;

      public a(int var1) {
         this.b = var1;
         this.a = new LinkedHashMap(var1 * 4 / 3 + 1, 0.75F, true) {
            protected boolean removeEldestEntry(Entry var1) {
               boolean var2;
               if(this.size() > a.this.b) {
                  var2 = true;
               } else {
                  var2 = false;
               }

               return var2;
            }
         };
      }

      public Object a(Object var1) {
         synchronized(this){}

         try {
            var1 = this.a.get(var1);
         } finally {
            ;
         }

         return var1;
      }

      public void a(Object var1, Object var2) {
         synchronized(this){}

         try {
            this.a.put(var1, var2);
         } finally {
            ;
         }

      }
   }
}
