package io.michaelrocks.libphonenumber.android;

import java.io.InputStream;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

final class e {
   private static final Logger a = Logger.getLogger(e.class.getName());
   private final d b;
   private final ConcurrentHashMap c = new ConcurrentHashMap();
   private final ConcurrentHashMap d = new ConcurrentHashMap();
   private final Set e = a.a();
   private final Set f = k.a();

   e(d var1) {
      this.b = var1;
   }

   private static i.c a(InputStream param0) {
      // $FF: Couldn't be decompiled
   }

   private static List a(String var0, d var1) {
      InputStream var2 = var1.a(var0);
      if(var2 == null) {
         throw new IllegalStateException("missing metadata: " + var0);
      } else {
         List var3 = a(var2).a();
         if(var3.size() == 0) {
            throw new IllegalStateException("empty metadata: " + var0);
         } else {
            return var3;
         }
      }
   }

   i.b a(Object var1, ConcurrentHashMap var2, String var3) {
      i.b var4 = (i.b)var2.get(var1);
      i.b var5;
      if(var4 != null) {
         var5 = var4;
      } else {
         String var8 = var3 + "_" + var1;
         List var6 = a(var8, this.b);
         if(var6.size() > 1) {
            a.log(Level.WARNING, "more than one metadata in file " + var8);
         }

         i.b var7 = (i.b)var6.get(0);
         var5 = (i.b)var2.putIfAbsent(var1, var7);
         if(var5 == null) {
            var5 = var7;
         }
      }

      return var5;
   }
}
