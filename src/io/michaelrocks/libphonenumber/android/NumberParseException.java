package io.michaelrocks.libphonenumber.android;

public class NumberParseException extends Exception {
   private NumberParseException.a a;
   private String b;

   public NumberParseException(NumberParseException.a var1, String var2) {
      super(var2);
      this.b = var2;
      this.a = var1;
   }

   public NumberParseException.a a() {
      return this.a;
   }

   public String toString() {
      return "Error type: " + this.a + ". " + this.b;
   }

   public static enum a {
      a,
      b,
      c,
      d,
      e;
   }
}
