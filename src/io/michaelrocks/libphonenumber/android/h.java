package io.michaelrocks.libphonenumber.android;

import android.content.Context;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class h {
   static final Pattern a;
   static final Pattern b;
   static final Pattern c;
   static final String d;
   static final Pattern e;
   private static final Logger f = Logger.getLogger(h.class.getName());
   private static final Map g;
   private static final Set h;
   private static final Set i;
   private static final Map j;
   private static final Map k;
   private static final Map l;
   private static final Map m;
   private static final Pattern n;
   private static final String o;
   private static final Pattern p;
   private static final Pattern q;
   private static final Pattern r;
   private static final Pattern s;
   private static final String t;
   private static final String u;
   private static final Pattern v;
   private static final Pattern w;
   private static final Pattern x;
   private static final Pattern y;
   private final Map A;
   private final io.michaelrocks.libphonenumber.android.a.a B = io.michaelrocks.libphonenumber.android.a.b.a();
   private final Set C = new HashSet(35);
   private final io.michaelrocks.libphonenumber.android.a.c D = new io.michaelrocks.libphonenumber.android.a.c(100);
   private final Set E = new HashSet(320);
   private final Set F = new HashSet();
   private final f z;

   static {
      HashMap var1 = new HashMap();
      var1.put(Integer.valueOf(52), "1");
      var1.put(Integer.valueOf(54), "9");
      g = Collections.unmodifiableMap(var1);
      HashSet var4 = new HashSet();
      var4.add(Integer.valueOf(86));
      h = Collections.unmodifiableSet(var4);
      HashSet var2 = new HashSet();
      var2.add(Integer.valueOf(52));
      var2.add(Integer.valueOf(54));
      var2.add(Integer.valueOf(55));
      var2.add(Integer.valueOf(62));
      var2.addAll(var4);
      i = Collections.unmodifiableSet(var2);
      var1 = new HashMap();
      var1.put(Character.valueOf('0'), Character.valueOf('0'));
      var1.put(Character.valueOf('1'), Character.valueOf('1'));
      var1.put(Character.valueOf('2'), Character.valueOf('2'));
      var1.put(Character.valueOf('3'), Character.valueOf('3'));
      var1.put(Character.valueOf('4'), Character.valueOf('4'));
      var1.put(Character.valueOf('5'), Character.valueOf('5'));
      var1.put(Character.valueOf('6'), Character.valueOf('6'));
      var1.put(Character.valueOf('7'), Character.valueOf('7'));
      var1.put(Character.valueOf('8'), Character.valueOf('8'));
      var1.put(Character.valueOf('9'), Character.valueOf('9'));
      HashMap var5 = new HashMap(40);
      var5.put(Character.valueOf('A'), Character.valueOf('2'));
      var5.put(Character.valueOf('B'), Character.valueOf('2'));
      var5.put(Character.valueOf('C'), Character.valueOf('2'));
      var5.put(Character.valueOf('D'), Character.valueOf('3'));
      var5.put(Character.valueOf('E'), Character.valueOf('3'));
      var5.put(Character.valueOf('F'), Character.valueOf('3'));
      var5.put(Character.valueOf('G'), Character.valueOf('4'));
      var5.put(Character.valueOf('H'), Character.valueOf('4'));
      var5.put(Character.valueOf('I'), Character.valueOf('4'));
      var5.put(Character.valueOf('J'), Character.valueOf('5'));
      var5.put(Character.valueOf('K'), Character.valueOf('5'));
      var5.put(Character.valueOf('L'), Character.valueOf('5'));
      var5.put(Character.valueOf('M'), Character.valueOf('6'));
      var5.put(Character.valueOf('N'), Character.valueOf('6'));
      var5.put(Character.valueOf('O'), Character.valueOf('6'));
      var5.put(Character.valueOf('P'), Character.valueOf('7'));
      var5.put(Character.valueOf('Q'), Character.valueOf('7'));
      var5.put(Character.valueOf('R'), Character.valueOf('7'));
      var5.put(Character.valueOf('S'), Character.valueOf('7'));
      var5.put(Character.valueOf('T'), Character.valueOf('8'));
      var5.put(Character.valueOf('U'), Character.valueOf('8'));
      var5.put(Character.valueOf('V'), Character.valueOf('8'));
      var5.put(Character.valueOf('W'), Character.valueOf('9'));
      var5.put(Character.valueOf('X'), Character.valueOf('9'));
      var5.put(Character.valueOf('Y'), Character.valueOf('9'));
      var5.put(Character.valueOf('Z'), Character.valueOf('9'));
      k = Collections.unmodifiableMap(var5);
      var5 = new HashMap(100);
      var5.putAll(k);
      var5.putAll(var1);
      l = Collections.unmodifiableMap(var5);
      var5 = new HashMap();
      var5.putAll(var1);
      var5.put(Character.valueOf('+'), Character.valueOf('+'));
      var5.put(Character.valueOf('*'), Character.valueOf('*'));
      var5.put(Character.valueOf('#'), Character.valueOf('#'));
      j = Collections.unmodifiableMap(var5);
      HashMap var3 = new HashMap();
      Iterator var6 = k.keySet().iterator();

      while(var6.hasNext()) {
         char var0 = ((Character)var6.next()).charValue();
         var3.put(Character.valueOf(Character.toLowerCase(var0)), Character.valueOf(var0));
         var3.put(Character.valueOf(var0), Character.valueOf(var0));
      }

      var3.putAll(var1);
      var3.put(Character.valueOf('-'), Character.valueOf('-'));
      var3.put(Character.valueOf('－'), Character.valueOf('-'));
      var3.put(Character.valueOf('‐'), Character.valueOf('-'));
      var3.put(Character.valueOf('‑'), Character.valueOf('-'));
      var3.put(Character.valueOf('‒'), Character.valueOf('-'));
      var3.put(Character.valueOf('–'), Character.valueOf('-'));
      var3.put(Character.valueOf('—'), Character.valueOf('-'));
      var3.put(Character.valueOf('―'), Character.valueOf('-'));
      var3.put(Character.valueOf('−'), Character.valueOf('-'));
      var3.put(Character.valueOf('/'), Character.valueOf('/'));
      var3.put(Character.valueOf('／'), Character.valueOf('/'));
      var3.put(Character.valueOf(' '), Character.valueOf(' '));
      var3.put(Character.valueOf('　'), Character.valueOf(' '));
      var3.put(Character.valueOf('\u2060'), Character.valueOf(' '));
      var3.put(Character.valueOf('.'), Character.valueOf('.'));
      var3.put(Character.valueOf('．'), Character.valueOf('.'));
      m = Collections.unmodifiableMap(var3);
      n = Pattern.compile("[\\d]+(?:[~⁓∼～][\\d]+)?");
      o = Arrays.toString(k.keySet().toArray()).replaceAll("[, \\[\\]]", "") + Arrays.toString(k.keySet().toArray()).toLowerCase().replaceAll("[, \\[\\]]", "");
      a = Pattern.compile("[+＋]+");
      p = Pattern.compile("[-x‐-―−ー－-／  \u00ad\u200b\u2060　()（）［］.\\[\\]/~⁓∼～]+");
      q = Pattern.compile("(\\p{Nd})");
      r = Pattern.compile("[+＋\\p{Nd}]");
      b = Pattern.compile("[\\\\/] *x");
      c = Pattern.compile("[[\\P{N}&&\\P{L}]&&[^#]]+$");
      s = Pattern.compile("(?:.*?[A-Za-z]){3}.*");
      t = "\\p{Nd}{2}|[+＋]*+(?:[-x‐-―−ー－-／  \u00ad\u200b\u2060　()（）［］.\\[\\]/~⁓∼～*]*\\p{Nd}){3,}[-x‐-―−ー－-／  \u00ad\u200b\u2060　()（）［］.\\[\\]/~⁓∼～*" + o + "\\p{Nd}" + "]*";
      u = b(",;" + "xｘ#＃~～");
      d = b("xｘ#＃~～");
      v = Pattern.compile("(?:" + u + ")$", 66);
      w = Pattern.compile(t + "(?:" + u + ")?", 66);
      e = Pattern.compile("(\\D+)");
      x = Pattern.compile("(\\$\\d)");
      y = Pattern.compile("\\(?\\$1\\)?");
   }

   h(f var1, Map var2) {
      this.z = var1;
      this.A = var2;
      Iterator var3 = var2.entrySet().iterator();

      while(true) {
         while(var3.hasNext()) {
            Entry var5 = (Entry)var3.next();
            List var4 = (List)var5.getValue();
            if(var4.size() == 1 && "001".equals(var4.get(0))) {
               this.F.add(var5.getKey());
            } else {
               this.E.addAll(var4);
            }
         }

         if(this.E.remove("001")) {
            f.log(Level.WARNING, "invalid metadata (country calling code was mapped to the non-geo entity as well as specific region(s))");
         }

         this.C.addAll((Collection)var2.get(Integer.valueOf(1)));
         return;
      }
   }

   private h.c a(CharSequence var1, i.b var2) {
      return this.a(var1, var2, h.b.l);
   }

   private h.c a(CharSequence var1, i.b var2, h.b var3) {
      i.d var7 = this.a(var2, var3);
      Object var6;
      if(var7.b().isEmpty()) {
         var6 = var2.a().b();
      } else {
         var6 = var7.b();
      }

      h.c var8;
      Object var9;
      label48: {
         List var12 = var7.d();
         if(var3 == h.b.c) {
            if(!a(this.a(var2, h.b.a))) {
               var8 = this.a(var1, var2, h.b.b);
               return var8;
            }

            i.d var11 = this.a(var2, h.b.b);
            if(a(var11)) {
               var6 = new ArrayList((Collection)var6);
               List var10;
               if(var11.b().size() == 0) {
                  var10 = var2.a().b();
               } else {
                  var10 = var11.b();
               }

               ((List)var6).addAll(var10);
               Collections.sort((List)var6);
               if(var12.isEmpty()) {
                  var9 = var11.d();
               } else {
                  var9 = new ArrayList(var12);
                  ((List)var9).addAll(var11.d());
                  Collections.sort((List)var9);
               }
               break label48;
            }
         }

         var9 = var12;
      }

      if(((Integer)((List)var6).get(0)).intValue() == -1) {
         var8 = h.c.e;
      } else {
         int var5 = var1.length();
         if(((List)var9).contains(Integer.valueOf(var5))) {
            var8 = h.c.b;
         } else {
            int var4 = ((Integer)((List)var6).get(0)).intValue();
            if(var4 == var5) {
               var8 = h.c.a;
            } else if(var4 > var5) {
               var8 = h.c.d;
            } else if(((Integer)((List)var6).get(((List)var6).size() - 1)).intValue() < var5) {
               var8 = h.c.f;
            } else if(((List)var6).subList(1, ((List)var6).size()).contains(Integer.valueOf(var5))) {
               var8 = h.c.a;
            } else {
               var8 = h.c.e;
            }
         }
      }

      return var8;
   }

   public static h a(Context var0) {
      if(var0 == null) {
         throw new IllegalArgumentException("context could not be null.");
      } else {
         return a((d)(new b(var0.getAssets())));
      }
   }

   public static h a(d var0) {
      if(var0 == null) {
         throw new IllegalArgumentException("metadataLoader could not be null.");
      } else {
         return a((f)(new g(var0)));
      }
   }

   public static h a(f var0) {
      if(var0 == null) {
         throw new IllegalArgumentException("metadataSource could not be null.");
      } else {
         return new h(var0, c.a());
      }
   }

   private i.b a(int var1, String var2) {
      i.b var3;
      if("001".equals(var2)) {
         var3 = this.a(var1);
      } else {
         var3 = this.a(var2);
      }

      return var3;
   }

   static CharSequence a(CharSequence var0) {
      Matcher var1 = r.matcher(var0);
      Object var4;
      if(var1.find()) {
         CharSequence var3 = var0.subSequence(var1.start(), var0.length());
         Matcher var2 = c.matcher(var3);
         var0 = var3;
         if(var2.find()) {
            var0 = var3.subSequence(0, var2.start());
            f.log(Level.FINER, "Stripped trailing characters: " + var0);
         }

         var2 = b.matcher(var0);
         var4 = var0;
         if(var2.find()) {
            var4 = var0.subSequence(0, var2.start());
         }
      } else {
         var4 = "";
      }

      return (CharSequence)var4;
   }

   private static String a(CharSequence var0, Map var1, boolean var2) {
      StringBuilder var6 = new StringBuilder(var0.length());

      for(int var4 = 0; var4 < var0.length(); ++var4) {
         char var3 = var0.charAt(var4);
         Character var5 = (Character)var1.get(Character.valueOf(Character.toUpperCase(var3)));
         if(var5 != null) {
            var6.append(var5);
         } else if(!var2) {
            var6.append(var3);
         }
      }

      return var6.toString();
   }

   private String a(String var1, i.a var2, h.a var3, CharSequence var4) {
      String var5 = var2.b();
      Matcher var6 = this.D.a(var2.a()).matcher(var1);
      String var7;
      if(var3 == h.a.c && var4 != null && var4.length() > 0 && var2.e().length() > 0) {
         var7 = var2.e().replace("$CC", var4);
         var1 = var6.replaceAll(x.matcher(var5).replaceFirst(var7));
      } else {
         var7 = var2.d();
         if(var3 == h.a.c && var7 != null && var7.length() > 0) {
            var1 = var6.replaceAll(x.matcher(var5).replaceFirst(var7));
         } else {
            var1 = var6.replaceAll(var5);
         }
      }

      var7 = var1;
      if(var3 == h.a.d) {
         Matcher var8 = p.matcher(var1);
         if(var8.lookingAt()) {
            var1 = var8.replaceFirst("");
         }

         var7 = var8.reset(var1).replaceAll("-");
      }

      return var7;
   }

   private String a(String var1, i.b var2, h.a var3) {
      return this.a((String)var1, (i.b)var2, var3, (CharSequence)null);
   }

   private String a(String var1, i.b var2, h.a var3, CharSequence var4) {
      List var5;
      if(var2.t().size() != 0 && var3 != h.a.c) {
         var5 = var2.t();
      } else {
         var5 = var2.r();
      }

      i.a var6 = this.a(var5, var1);
      if(var6 != null) {
         var1 = this.a(var1, var6, var3, var4);
      }

      return var1;
   }

   static StringBuilder a(CharSequence var0, boolean var1) {
      StringBuilder var5 = new StringBuilder(var0.length());

      for(int var3 = 0; var3 < var0.length(); ++var3) {
         char var2 = var0.charAt(var3);
         int var4 = Character.digit(var2, 10);
         if(var4 != -1) {
            var5.append(var4);
         } else if(var1) {
            var5.append(var2);
         }
      }

      return var5;
   }

   static StringBuilder a(StringBuilder var0) {
      if(s.matcher(var0).matches()) {
         var0.replace(0, var0.length(), a(var0, l, true));
      } else {
         var0.replace(0, var0.length(), c((CharSequence)var0));
      }

      return var0;
   }

   private void a(int var1, h.a var2, StringBuilder var3) {
      switch(null.b[var2.ordinal()]) {
      case 1:
         var3.insert(0, var1).insert(0, '+');
         break;
      case 2:
         var3.insert(0, " ").insert(0, var1).insert(0, '+');
         break;
      case 3:
         var3.insert(0, "-").insert(0, var1).insert(0, '+').insert(0, "tel:");
      }

   }

   private void a(j.a var1, i.b var2, h.a var3, StringBuilder var4) {
      if(var1.c() && var1.d().length() > 0) {
         if(var3 == h.a.d) {
            var4.append(";ext=").append(var1.d());
         } else if(var2.n()) {
            var4.append(var2.o()).append(var1.d());
         } else {
            var4.append(" ext. ").append(var1.d());
         }
      }

   }

   static void a(CharSequence var0, j.a var1) {
      if(var0.length() > 1 && var0.charAt(0) == 48) {
         var1.a(true);

         int var2;
         for(var2 = 1; var2 < var0.length() - 1 && var0.charAt(var2) == 48; ++var2) {
            ;
         }

         if(var2 != 1) {
            var1.b(var2);
         }
      }

   }

   private void a(CharSequence var1, String var2, boolean var3, boolean var4, j.a var5) throws NumberParseException {
      if(var1 == null) {
         throw new NumberParseException(NumberParseException.a.b, "The phone number supplied was null.");
      } else if(var1.length() > 250) {
         throw new NumberParseException(NumberParseException.a.e, "The string supplied was too long to parse.");
      } else {
         StringBuilder var10 = new StringBuilder();
         String var13 = var1.toString();
         this.a(var13, var10);
         if(!b((CharSequence)var10)) {
            throw new NumberParseException(NumberParseException.a.b, "The string supplied did not seem to be a phone number.");
         } else if(var4 && !this.b(var10, var2)) {
            throw new NumberParseException(NumberParseException.a.a, "Missing or invalid default region.");
         } else {
            if(var3) {
               var5.b(var13);
            }

            var13 = this.b(var10);
            if(var13.length() > 0) {
               var5.a(var13);
            }

            i.b var9 = this.a(var2);
            StringBuilder var8 = new StringBuilder();

            int var6;
            try {
               var6 = this.a(var10, var9, var8, var3, var5);
            } catch (NumberParseException var12) {
               Matcher var14 = a.matcher(var10);
               if(var12.a() != NumberParseException.a.a || !var14.lookingAt()) {
                  throw new NumberParseException(var12.a(), var12.getMessage());
               }

               int var7 = this.a(var10.substring(var14.end()), var9, var8, var3, var5);
               var6 = var7;
               if(var7 == 0) {
                  throw new NumberParseException(NumberParseException.a.a, "Could not interpret numbers after plus-sign.");
               }
            }

            i.b var15;
            if(var6 != 0) {
               String var18 = this.b(var6);
               var15 = var9;
               if(!var18.equals(var2)) {
                  var15 = this.a(var6, var18);
               }
            } else {
               var8.append(a(var10));
               if(var2 != null) {
                  var5.a(var9.l());
                  var15 = var9;
               } else {
                  var15 = var9;
                  if(var3) {
                     var5.m();
                     var15 = var9;
                  }
               }
            }

            if(var8.length() < 2) {
               throw new NumberParseException(NumberParseException.a.d, "The string supplied is too short to be a phone number.");
            } else {
               StringBuilder var16 = var8;
               if(var15 != null) {
                  var10 = new StringBuilder();
                  StringBuilder var17 = new StringBuilder(var8);
                  this.a(var17, var15, var10);
                  var16 = var8;
                  if(this.a((CharSequence)var17, (i.b)var15) != h.c.d) {
                     if(var3 && var10.length() > 0) {
                        var5.c(var10.toString());
                     }

                     var16 = var17;
                  }
               }

               var6 = var16.length();
               if(var6 < 2) {
                  throw new NumberParseException(NumberParseException.a.d, "The string supplied is too short to be a phone number.");
               } else if(var6 > 17) {
                  throw new NumberParseException(NumberParseException.a.e, "The string supplied is too long to be a phone number.");
               } else {
                  a((CharSequence)var16, (j.a)var5);
                  var5.a(Long.parseLong(var16.toString()));
               }
            }
         }
      }
   }

   private void a(String var1, StringBuilder var2) {
      int var4 = var1.indexOf(";phone-context=");
      int var3;
      if(var4 >= 0) {
         var3 = ";phone-context=".length() + var4;
         if(var3 < var1.length() - 1 && var1.charAt(var3) == 43) {
            int var5 = var1.indexOf(59, var3);
            if(var5 > 0) {
               var2.append(var1.substring(var3, var5));
            } else {
               var2.append(var1.substring(var3));
            }
         }

         var3 = var1.indexOf("tel:");
         if(var3 >= 0) {
            var3 += "tel:".length();
         } else {
            var3 = 0;
         }

         var2.append(var1.substring(var3, var4));
      } else {
         var2.append(a((CharSequence)var1));
      }

      var3 = var2.indexOf(";isub=");
      if(var3 > 0) {
         var2.delete(var3, var2.length());
      }

   }

   private static boolean a(i.d var0) {
      boolean var1 = false;
      if(var0.c() != 1 || var0.a(0) != -1) {
         var1 = true;
      }

      return var1;
   }

   private boolean a(Pattern var1, StringBuilder var2) {
      boolean var5 = false;
      Matcher var6 = var1.matcher(var2);
      boolean var4 = var5;
      if(var6.lookingAt()) {
         int var3 = var6.end();
         var6 = q.matcher(var2.substring(var3));
         if(var6.find() && c((CharSequence)var6.group(1)).equals("0")) {
            var4 = var5;
         } else {
            var2.delete(0, var3);
            var4 = true;
         }
      }

      return var4;
   }

   private static String b(String var0) {
      return ";ext=(\\p{Nd}{1,7})|[  \\t,]*(?:e?xt(?:ensi(?:ó?|ó))?n?|ｅ?ｘｔｎ?|[" + var0 + "]|int|anexo|ｉｎｔ)[:\\.．]?[  \\t,-]*" + "(\\p{Nd}{1,7})" + "#?|[- ]+(" + "\\p{Nd}" + "{1,5})#";
   }

   static boolean b(CharSequence var0) {
      boolean var1;
      if(var0.length() < 2) {
         var1 = false;
      } else {
         var1 = w.matcher(var0).matches();
      }

      return var1;
   }

   private boolean b(CharSequence var1, String var2) {
      boolean var3;
      if(this.c(var2) || var1 != null && var1.length() != 0 && a.matcher(var1).lookingAt()) {
         var3 = true;
      } else {
         var3 = false;
      }

      return var3;
   }

   public static String c(CharSequence var0) {
      return a(var0, false).toString();
   }

   private boolean c(int var1) {
      return this.A.containsKey(Integer.valueOf(var1));
   }

   private boolean c(String var1) {
      boolean var2;
      if(var1 != null && this.E.contains(var1)) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   int a(CharSequence var1, i.b var2, StringBuilder var3, boolean var4, j.a var5) throws NumberParseException {
      int var6;
      if(var1.length() == 0) {
         var6 = 0;
      } else {
         StringBuilder var7 = new StringBuilder(var1);
         String var9 = "NonMatch";
         if(var2 != null) {
            var9 = var2.m();
         }

         j.a var10 = this.a(var7, var9);
         if(var4) {
            var5.a(var10);
         }

         if(var10 != j.a.d) {
            if(var7.length() <= 2) {
               throw new NumberParseException(NumberParseException.a.c, "Phone number had an IDD, but after this was not long enough to be a viable phone number.");
            }

            var6 = this.a(var7, var3);
            if(var6 == 0) {
               throw new NumberParseException(NumberParseException.a.a, "Country calling code supplied was not recognised.");
            }

            var5.a(var6);
         } else {
            if(var2 != null) {
               var6 = var2.l();
               var9 = String.valueOf(var6);
               String var8 = var7.toString();
               if(var8.startsWith(var9)) {
                  StringBuilder var11 = new StringBuilder(var8.substring(var9.length()));
                  i.d var12 = var2.a();
                  this.a((StringBuilder)var11, (i.b)var2, (StringBuilder)null);
                  if(!this.B.a(var7, var12, false) && this.B.a(var11, var12, false) || this.a((CharSequence)var7, (i.b)var2) == h.c.f) {
                     var3.append(var11);
                     if(var4) {
                        var5.a(j.a.c);
                     }

                     var5.a(var6);
                     return var6;
                  }
               }
            }

            var5.a(0);
            var6 = 0;
         }
      }

      return var6;
   }

   int a(StringBuilder var1, StringBuilder var2) {
      int var3;
      if(var1.length() != 0 && var1.charAt(0) != 48) {
         int var5 = var1.length();

         for(var3 = 1; var3 <= 3 && var3 <= var5; ++var3) {
            int var4 = Integer.parseInt(var1.substring(0, var3));
            if(this.A.containsKey(Integer.valueOf(var4))) {
               var2.append(var1.substring(var3));
               var3 = var4;
               return var3;
            }
         }

         var3 = 0;
      } else {
         var3 = 0;
      }

      return var3;
   }

   i.a a(List var1, String var2) {
      Iterator var4 = var1.iterator();

      int var3;
      i.a var5;
      do {
         if(!var4.hasNext()) {
            var5 = null;
            break;
         }

         var5 = (i.a)var4.next();
         var3 = var5.c();
      } while(var3 != 0 && !this.D.a(var5.a(var3 - 1)).matcher(var2).lookingAt() || !this.D.a(var5.a()).matcher(var2).matches());

      return var5;
   }

   i.b a(int var1) {
      i.b var2;
      if(!this.A.containsKey(Integer.valueOf(var1))) {
         var2 = null;
      } else {
         var2 = this.z.a(var1);
      }

      return var2;
   }

   i.b a(String var1) {
      i.b var2;
      if(!this.c(var1)) {
         var2 = null;
      } else {
         var2 = this.z.a(var1);
      }

      return var2;
   }

   i.d a(i.b var1, h.b var2) {
      i.d var3;
      switch(null.c[var2.ordinal()]) {
      case 1:
         var3 = var1.e();
         break;
      case 2:
         var3 = var1.d();
         break;
      case 3:
         var3 = var1.c();
         break;
      case 4:
      case 5:
         var3 = var1.b();
         break;
      case 6:
         var3 = var1.f();
         break;
      case 7:
         var3 = var1.h();
         break;
      case 8:
         var3 = var1.g();
         break;
      case 9:
         var3 = var1.i();
         break;
      case 10:
         var3 = var1.j();
         break;
      case 11:
         var3 = var1.k();
         break;
      default:
         var3 = var1.a();
      }

      return var3;
   }

   j.a a(StringBuilder var1, String var2) {
      j.a var4;
      if(var1.length() == 0) {
         var4 = j.a.d;
      } else {
         Matcher var3 = a.matcher(var1);
         if(var3.lookingAt()) {
            var1.delete(0, var3.end());
            a(var1);
            var4 = j.a.a;
         } else {
            Pattern var5 = this.D.a(var2);
            a(var1);
            if(this.a(var5, var1)) {
               var4 = j.a.b;
            } else {
               var4 = j.a.d;
            }
         }
      }

      return var4;
   }

   public j.a a(CharSequence var1, String var2) throws NumberParseException {
      j.a var3 = new j.a();
      this.a(var1, var2, var3);
      return var3;
   }

   public String a(j.a var1) {
      StringBuilder var3 = new StringBuilder();
      if(var1.f() && var1.h() > 0) {
         char[] var2 = new char[var1.h()];
         Arrays.fill(var2, '0');
         var3.append(new String(var2));
      }

      var3.append(var1.b());
      return var3.toString();
   }

   public String a(j.a var1, h.a var2) {
      String var4;
      if(var1.b() == 0L && var1.i()) {
         String var3 = var1.j();
         if(var3.length() > 0) {
            var4 = var3;
            return var4;
         }
      }

      StringBuilder var5 = new StringBuilder(20);
      this.a(var1, var2, var5);
      var4 = var5.toString();
      return var4;
   }

   public void a(j.a var1, h.a var2, StringBuilder var3) {
      var3.setLength(0);
      int var4 = var1.a();
      String var5 = this.a(var1);
      if(var2 == h.a.a) {
         var3.append(var5);
         this.a(var4, h.a.a, var3);
      } else if(!this.c(var4)) {
         var3.append(var5);
      } else {
         i.b var6 = this.a(var4, this.b(var4));
         var3.append(this.a(var5, var6, var2));
         this.a(var1, var6, var2, var3);
         this.a(var4, var2, var3);
      }

   }

   public void a(CharSequence var1, String var2, j.a var3) throws NumberParseException {
      this.a(var1, var2, false, true, var3);
   }

   boolean a(StringBuilder var1, i.b var2, StringBuilder var3) {
      boolean var7 = false;
      int var5 = var1.length();
      String var8 = var2.p();
      boolean var6 = var7;
      if(var5 != 0) {
         if(var8.length() == 0) {
            var6 = var7;
         } else {
            Matcher var9 = this.D.a(var8).matcher(var1);
            var6 = var7;
            if(var9.lookingAt()) {
               i.d var12 = var2.a();
               var6 = this.B.a(var1, var12, false);
               int var4 = var9.groupCount();
               String var10 = var2.q();
               if(var10 != null && var10.length() != 0 && var9.group(var4) != null) {
                  StringBuilder var11 = new StringBuilder(var1);
                  var11.replace(0, var5, var9.replaceFirst(var10));
                  if(var6) {
                     var6 = var7;
                     if(!this.B.a(var11.toString(), var12, false)) {
                        return var6;
                     }
                  }

                  if(var3 != null && var4 > 1) {
                     var3.append(var9.group(1));
                  }

                  var1.replace(0, var1.length(), var11.toString());
                  var6 = true;
               } else {
                  if(var6) {
                     var6 = var7;
                     if(!this.B.a(var1.substring(var9.end()), var12, false)) {
                        return var6;
                     }
                  }

                  if(var3 != null && var4 > 0 && var9.group(var4) != null) {
                     var3.append(var9.group(1));
                  }

                  var1.delete(0, var9.end());
                  var6 = true;
               }
            }
         }
      }

      return var6;
   }

   public String b(int var1) {
      List var2 = (List)this.A.get(Integer.valueOf(var1));
      String var3;
      if(var2 == null) {
         var3 = "ZZ";
      } else {
         var3 = (String)var2.get(0);
      }

      return var3;
   }

   String b(StringBuilder var1) {
      Matcher var5 = v.matcher(var1);
      String var6;
      if(var5.find() && b((CharSequence)var1.substring(0, var5.start()))) {
         int var2 = 1;

         for(int var3 = var5.groupCount(); var2 <= var3; ++var2) {
            if(var5.group(var2) != null) {
               String var4 = var5.group(var2);
               var1.delete(var5.start(), var1.length());
               var6 = var4;
               return var6;
            }
         }
      }

      var6 = "";
      return var6;
   }

   public static enum a {
      a,
      b,
      c,
      d;
   }

   public static enum b {
      a,
      b,
      c,
      d,
      e,
      f,
      g,
      h,
      i,
      j,
      k,
      l;
   }

   public static enum c {
      a,
      b,
      c,
      d,
      e,
      f;
   }
}
