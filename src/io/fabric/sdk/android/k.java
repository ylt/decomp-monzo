package io.fabric.sdk.android;

public interface k {
   void a(int var1, String var2, String var3);

   void a(int var1, String var2, String var3, boolean var4);

   void a(String var1, String var2);

   void a(String var1, String var2, Throwable var3);

   boolean a(String var1, int var2);

   void b(String var1, String var2);

   void c(String var1, String var2);

   void d(String var1, String var2);

   void d(String var1, String var2, Throwable var3);

   void e(String var1, String var2);

   void e(String var1, String var2, Throwable var3);
}
