package io.fabric.sdk.android.services.e;

import io.fabric.sdk.android.services.network.HttpRequest;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

class l extends io.fabric.sdk.android.services.b.a implements x {
   public l(io.fabric.sdk.android.h var1, String var2, String var3, io.fabric.sdk.android.services.network.d var4) {
      this(var1, var2, var3, var4, io.fabric.sdk.android.services.network.c.a);
   }

   l(io.fabric.sdk.android.h var1, String var2, String var3, io.fabric.sdk.android.services.network.d var4, io.fabric.sdk.android.services.network.c var5) {
      super(var1, var2, var3, var4, var5);
   }

   private HttpRequest a(HttpRequest var1, w var2) {
      this.a(var1, "X-CRASHLYTICS-API-KEY", var2.a);
      this.a(var1, "X-CRASHLYTICS-API-CLIENT-TYPE", "android");
      this.a(var1, "X-CRASHLYTICS-API-CLIENT-VERSION", this.kit.getVersion());
      this.a(var1, "Accept", "application/json");
      this.a(var1, "X-CRASHLYTICS-DEVICE-MODEL", var2.b);
      this.a(var1, "X-CRASHLYTICS-OS-BUILD-VERSION", var2.c);
      this.a(var1, "X-CRASHLYTICS-OS-DISPLAY-VERSION", var2.d);
      this.a(var1, "X-CRASHLYTICS-ADVERTISING-TOKEN", var2.e);
      this.a(var1, "X-CRASHLYTICS-INSTALLATION-ID", var2.f);
      this.a(var1, "X-CRASHLYTICS-ANDROID-ID", var2.g);
      return var1;
   }

   private JSONObject a(String var1) {
      JSONObject var2;
      JSONObject var4;
      try {
         var2 = new JSONObject(var1);
      } catch (Exception var3) {
         io.fabric.sdk.android.c.h().a("Fabric", "Failed to parse settings JSON from " + this.getUrl(), var3);
         io.fabric.sdk.android.c.h().a("Fabric", "Settings response " + var1);
         var4 = null;
         return var4;
      }

      var4 = var2;
      return var4;
   }

   private void a(HttpRequest var1, String var2, String var3) {
      if(var3 != null) {
         var1.a(var2, var3);
      }

   }

   private Map b(w var1) {
      HashMap var2 = new HashMap();
      var2.put("build_version", var1.j);
      var2.put("display_version", var1.i);
      var2.put("source", Integer.toString(var1.k));
      if(var1.l != null) {
         var2.put("icon_hash", var1.l);
      }

      String var3 = var1.h;
      if(!io.fabric.sdk.android.services.b.i.c(var3)) {
         var2.put("instance", var3);
      }

      return var2;
   }

   public JSONObject a(w param1) {
      // $FF: Couldn't be decompiled
   }

   JSONObject a(HttpRequest var1) {
      int var2 = var1.b();
      io.fabric.sdk.android.c.h().a("Fabric", "Settings result was: " + var2);
      JSONObject var3;
      if(this.a(var2)) {
         var3 = this.a(var1.e());
      } else {
         io.fabric.sdk.android.c.h().e("Fabric", "Failed to retrieve settings from " + this.getUrl());
         var3 = null;
      }

      return var3;
   }

   boolean a(int var1) {
      boolean var2;
      if(var1 != 200 && var1 != 201 && var1 != 202 && var1 != 203) {
         var2 = false;
      } else {
         var2 = true;
      }

      return var2;
   }
}
