package io.fabric.sdk.android.services.e;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;

public class n {
   public final String a;
   public final int b;
   public final int c;
   public final int d;

   public n(String var1, int var2, int var3, int var4) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
      this.d = var4;
   }

   public static n a(Context var0, String var1) {
      n var6;
      if(var1 != null) {
         try {
            int var2 = io.fabric.sdk.android.services.b.i.l(var0);
            io.fabric.sdk.android.k var4 = io.fabric.sdk.android.c.h();
            StringBuilder var3 = new StringBuilder();
            var4.a("Fabric", var3.append("App icon resource ID is ").append(var2).toString());
            Options var7 = new Options();
            var7.inJustDecodeBounds = true;
            BitmapFactory.decodeResource(var0.getResources(), var2, var7);
            var6 = new n(var1, var2, var7.outWidth, var7.outHeight);
            return var6;
         } catch (Exception var5) {
            io.fabric.sdk.android.c.h().e("Fabric", "Failed to load icon", var5);
         }
      }

      var6 = null;
      return var6;
   }
}
