package io.fabric.sdk.android.services.e;

import io.fabric.sdk.android.services.network.HttpRequest;
import java.util.Locale;

abstract class a extends io.fabric.sdk.android.services.b.a {
   public a(io.fabric.sdk.android.h var1, String var2, String var3, io.fabric.sdk.android.services.network.d var4, io.fabric.sdk.android.services.network.c var5) {
      super(var1, var2, var3, var4, var5);
   }

   private HttpRequest a(HttpRequest var1, d var2) {
      return var1.a("X-CRASHLYTICS-API-KEY", var2.a).a("X-CRASHLYTICS-API-CLIENT-TYPE", "android").a("X-CRASHLYTICS-API-CLIENT-VERSION", this.kit.getVersion());
   }

   private HttpRequest b(HttpRequest param1, d param2) {
      // $FF: Couldn't be decompiled
   }

   String a(io.fabric.sdk.android.j var1) {
      return String.format(Locale.US, "app[build][libraries][%s][version]", new Object[]{var1.a()});
   }

   public boolean a(d var1) {
      HttpRequest var4 = this.b(this.a(this.getHttpRequest(), var1), var1);
      io.fabric.sdk.android.c.h().a("Fabric", "Sending app info to " + this.getUrl());
      if(var1.j != null) {
         io.fabric.sdk.android.c.h().a("Fabric", "App icon hash is " + var1.j.a);
         io.fabric.sdk.android.c.h().a("Fabric", "App icon size is " + var1.j.c + "x" + var1.j.d);
      }

      int var2 = var4.b();
      String var5;
      if("POST".equals(var4.p())) {
         var5 = "Create";
      } else {
         var5 = "Update";
      }

      io.fabric.sdk.android.c.h().a("Fabric", var5 + " app request ID: " + var4.b("X-REQUEST-ID"));
      io.fabric.sdk.android.c.h().a("Fabric", "Result was " + var2);
      boolean var3;
      if(io.fabric.sdk.android.services.b.r.a(var2) == 0) {
         var3 = true;
      } else {
         var3 = false;
      }

      return var3;
   }

   String b(io.fabric.sdk.android.j var1) {
      return String.format(Locale.US, "app[build][libraries][%s][type]", new Object[]{var1.a()});
   }
}
