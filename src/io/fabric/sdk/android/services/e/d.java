package io.fabric.sdk.android.services.e;

import java.util.Collection;

public class d {
   public final String a;
   public final String b;
   public final String c;
   public final String d;
   public final String e;
   public final String f;
   public final int g;
   public final String h;
   public final String i;
   public final n j;
   public final Collection k;

   public d(String var1, String var2, String var3, String var4, String var5, String var6, int var7, String var8, String var9, n var10, Collection var11) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
      this.d = var4;
      this.e = var5;
      this.f = var6;
      this.g = var7;
      this.h = var8;
      this.i = var9;
      this.j = var10;
      this.k = var11;
   }
}
