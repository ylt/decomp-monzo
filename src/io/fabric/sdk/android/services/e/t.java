package io.fabric.sdk.android.services.e;

public class t {
   public final e a;
   public final p b;
   public final o c;
   public final m d;
   public final b e;
   public final f f;
   public final long g;
   public final int h;
   public final int i;

   public t(long var1, e var3, p var4, o var5, m var6, b var7, f var8, int var9, int var10) {
      this.g = var1;
      this.a = var3;
      this.b = var4;
      this.c = var5;
      this.d = var6;
      this.h = var9;
      this.i = var10;
      this.e = var7;
      this.f = var8;
   }

   public boolean a(long var1) {
      boolean var3;
      if(this.g < var1) {
         var3 = true;
      } else {
         var3 = false;
      }

      return var3;
   }
}
