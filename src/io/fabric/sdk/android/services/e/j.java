package io.fabric.sdk.android.services.e;

import android.annotation.SuppressLint;
import android.content.SharedPreferences.Editor;
import org.json.JSONException;
import org.json.JSONObject;

class j implements s {
   private final w a;
   private final v b;
   private final io.fabric.sdk.android.services.b.k c;
   private final g d;
   private final x e;
   private final io.fabric.sdk.android.h f;
   private final io.fabric.sdk.android.services.d.c g;

   public j(io.fabric.sdk.android.h var1, w var2, io.fabric.sdk.android.services.b.k var3, v var4, g var5, x var6) {
      this.f = var1;
      this.a = var2;
      this.c = var3;
      this.b = var4;
      this.d = var5;
      this.e = var6;
      this.g = new io.fabric.sdk.android.services.d.d(this.f);
   }

   private void a(JSONObject var1, String var2) throws JSONException {
      io.fabric.sdk.android.c.h().a("Fabric", var2 + var1.toString());
   }

   private t b(r param1) {
      // $FF: Couldn't be decompiled
   }

   public t a() {
      return this.a(r.a);
   }

   public t a(r param1) {
      // $FF: Couldn't be decompiled
   }

   @SuppressLint({"CommitPrefEdits"})
   boolean a(String var1) {
      Editor var2 = this.g.b();
      var2.putString("existing_instance_identifier", var1);
      return this.g.a(var2);
   }

   String b() {
      return io.fabric.sdk.android.services.b.i.a(new String[]{io.fabric.sdk.android.services.b.i.m(this.f.getContext())});
   }

   String c() {
      return this.g.a().getString("existing_instance_identifier", "");
   }

   boolean d() {
      boolean var1;
      if(!this.c().equals(this.b())) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }
}
