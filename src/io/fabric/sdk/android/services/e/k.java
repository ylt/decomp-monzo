package io.fabric.sdk.android.services.e;

import org.json.JSONException;
import org.json.JSONObject;

class k implements v {
   private long a(io.fabric.sdk.android.services.b.k var1, long var2, JSONObject var4) throws JSONException {
      if(var4.has("expires_at")) {
         var2 = var4.getLong("expires_at");
      } else {
         var2 = var1.a() + 1000L * var2;
      }

      return var2;
   }

   private e a(JSONObject var1) throws JSONException {
      String var8 = var1.getString("identifier");
      String var7 = var1.getString("status");
      String var5 = var1.getString("url");
      String var6 = var1.getString("reports_url");
      boolean var2 = var1.optBoolean("update_required", false);
      Object var4 = null;
      c var3 = (c)var4;
      if(var1.has("icon")) {
         var3 = (c)var4;
         if(var1.getJSONObject("icon").has("hash")) {
            var3 = this.b(var1.getJSONObject("icon"));
         }
      }

      return new e(var8, var7, var5, var6, var2, var3);
   }

   private c b(JSONObject var1) throws JSONException {
      return new c(var1.getString("hash"), var1.getInt("width"), var1.getInt("height"));
   }

   private m c(JSONObject var1) {
      return new m(var1.optBoolean("prompt_enabled", false), var1.optBoolean("collect_logged_exceptions", true), var1.optBoolean("collect_reports", true), var1.optBoolean("collect_analytics", false));
   }

   private b d(JSONObject var1) {
      return new b(var1.optString("url", "https://e.crashlytics.com/spi/v2/events"), var1.optInt("flush_interval_secs", 600), var1.optInt("max_byte_size_per_file", 8000), var1.optInt("max_file_count_per_send", 1), var1.optInt("max_pending_send_file_count", 100), var1.optBoolean("track_custom_events", true), var1.optBoolean("track_predefined_events", true), var1.optInt("sampling_rate", 1), var1.optBoolean("flush_on_background", true));
   }

   private p e(JSONObject var1) throws JSONException {
      return new p(var1.optInt("log_buffer_size", '切'), var1.optInt("max_chained_exception_depth", 8), var1.optInt("max_custom_exception_events", 64), var1.optInt("max_custom_key_value_pairs", 64), var1.optInt("identifier_mask", 255), var1.optBoolean("send_session_without_crash", false), var1.optInt("max_complete_sessions_count", 4));
   }

   private o f(JSONObject var1) throws JSONException {
      return new o(var1.optString("title", "Send Crash Report?"), var1.optString("message", "Looks like we crashed! Please help us fix the problem by sending a crash report."), var1.optString("send_button_title", "Send"), var1.optBoolean("show_cancel_button", true), var1.optString("cancel_button_title", "Don't Send"), var1.optBoolean("show_always_send_button", true), var1.optString("always_send_button_title", "Always Send"));
   }

   private f g(JSONObject var1) throws JSONException {
      return new f(var1.optString("update_endpoint", u.a), var1.optInt("update_suspend_duration", 3600));
   }

   public t a(io.fabric.sdk.android.services.b.k var1, JSONObject var2) throws JSONException {
      int var4 = var2.optInt("settings_version", 0);
      int var3 = var2.optInt("cache_duration", 3600);
      e var5 = this.a(var2.getJSONObject("app"));
      p var9 = this.e(var2.getJSONObject("session"));
      o var6 = this.f(var2.getJSONObject("prompt"));
      m var7 = this.c(var2.getJSONObject("features"));
      b var10 = this.d(var2.getJSONObject("analytics"));
      f var8 = this.g(var2.getJSONObject("beta"));
      return new t(this.a(var1, (long)var3, var2), var5, var9, var6, var7, var10, var8, var4, var3);
   }
}
