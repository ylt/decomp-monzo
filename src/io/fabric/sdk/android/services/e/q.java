package io.fabric.sdk.android.services.e;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicReference;

public class q {
   private final AtomicReference a;
   private final CountDownLatch b;
   private s c;
   private boolean d;

   private q() {
      this.a = new AtomicReference();
      this.b = new CountDownLatch(1);
      this.d = false;
   }

   // $FF: synthetic method
   q(Object var1) {
      this();
   }

   public static q a() {
      return q.a.a;
   }

   private void a(t var1) {
      this.a.set(var1);
      this.b.countDown();
   }

   public q a(io.fabric.sdk.android.h param1, io.fabric.sdk.android.services.b.o param2, io.fabric.sdk.android.services.network.d param3, String param4, String param5, String param6) {
      // $FF: Couldn't be decompiled
   }

   public t b() {
      t var1;
      try {
         this.b.await();
         var1 = (t)this.a.get();
      } catch (InterruptedException var2) {
         io.fabric.sdk.android.c.h().e("Fabric", "Interrupted while waiting for settings data.");
         var1 = null;
      }

      return var1;
   }

   public boolean c() {
      synchronized(this){}
      boolean var4 = false;

      t var2;
      try {
         var4 = true;
         var2 = this.c.a();
         this.a(var2);
         var4 = false;
      } finally {
         if(var4) {
            ;
         }
      }

      boolean var1;
      if(var2 != null) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public boolean d() {
      // $FF: Couldn't be decompiled
   }

   static class a {
      private static final q a = new q();
   }
}
