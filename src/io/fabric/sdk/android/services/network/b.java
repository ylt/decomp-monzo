package io.fabric.sdk.android.services.network;

import io.fabric.sdk.android.k;
import java.util.Collections;
import java.util.Locale;
import java.util.Map;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSocketFactory;

public class b implements d {
   private final k a;
   private f b;
   private SSLSocketFactory c;
   private boolean d;

   public b() {
      this(new io.fabric.sdk.android.b());
   }

   public b(k var1) {
      this.a = var1;
   }

   private void a() {
      synchronized(this){}

      try {
         this.d = false;
         this.c = null;
      } finally {
         ;
      }

   }

   private boolean a(String var1) {
      boolean var2;
      if(var1 != null && var1.toLowerCase(Locale.US).startsWith("https")) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   private SSLSocketFactory b() {
      synchronized(this){}

      SSLSocketFactory var1;
      try {
         if(this.c == null && !this.d) {
            this.c = this.c();
         }

         var1 = this.c;
      } finally {
         ;
      }

      return var1;
   }

   private SSLSocketFactory c() {
      synchronized(this){}
      boolean var4 = false;

      SSLSocketFactory var1;
      try {
         var4 = true;
         this.d = true;

         try {
            var1 = e.a(this.b);
            this.a.a("Fabric", "Custom SSL pinning enabled");
            var4 = false;
            return var1;
         } catch (Exception var5) {
            this.a.e("Fabric", "Exception while validating pinned certs", var5);
            var4 = false;
         }
      } finally {
         if(var4) {
            ;
         }
      }

      var1 = null;
      return var1;
   }

   public HttpRequest a(c var1, String var2) {
      return this.a(var1, var2, Collections.emptyMap());
   }

   public HttpRequest a(c var1, String var2, Map var3) {
      HttpRequest var4;
      switch(null.a[var1.ordinal()]) {
      case 1:
         var4 = HttpRequest.a(var2, var3, true);
         break;
      case 2:
         var4 = HttpRequest.b(var2, var3, true);
         break;
      case 3:
         var4 = HttpRequest.d((CharSequence)var2);
         break;
      case 4:
         var4 = HttpRequest.e((CharSequence)var2);
         break;
      default:
         throw new IllegalArgumentException("Unsupported HTTP method!");
      }

      if(this.a(var2) && this.b != null) {
         SSLSocketFactory var5 = this.b();
         if(var5 != null) {
            ((HttpsURLConnection)var4.a()).setSSLSocketFactory(var5);
         }
      }

      return var4;
   }

   public void a(f var1) {
      if(this.b != var1) {
         this.b = var1;
         this.a();
      }

   }
}
