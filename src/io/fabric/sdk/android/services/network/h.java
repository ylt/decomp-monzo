package io.fabric.sdk.android.services.network;

import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.cert.X509Certificate;
import java.util.HashMap;

class h {
   final KeyStore a;
   private final HashMap b;

   public h(InputStream var1, String var2) {
      KeyStore var3 = this.a(var1, var2);
      this.b = this.a(var3);
      this.a = var3;
   }

   private KeyStore a(InputStream param1, String param2) {
      // $FF: Couldn't be decompiled
   }

   private HashMap a(KeyStore param1) {
      // $FF: Couldn't be decompiled
   }

   public boolean a(X509Certificate var1) {
      X509Certificate var3 = (X509Certificate)this.b.get(var1.getSubjectX500Principal());
      boolean var2;
      if(var3 != null && var3.getPublicKey().equals(var1.getPublicKey())) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   public X509Certificate b(X509Certificate var1) {
      X509Certificate var2 = (X509Certificate)this.b.get(var1.getIssuerX500Principal());
      if(var2 == null) {
         var1 = null;
      } else if(var2.getSubjectX500Principal().equals(var1.getSubjectX500Principal())) {
         var1 = null;
      } else {
         try {
            var1.verify(var2.getPublicKey());
         } catch (GeneralSecurityException var3) {
            var1 = null;
            return var1;
         }

         var1 = var2;
      }

      return var1;
   }
}
