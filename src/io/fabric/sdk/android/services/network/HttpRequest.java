package io.fabric.sdk.android.services.network;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.Flushable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.URL;
import java.net.Proxy.Type;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Callable;
import java.util.zip.GZIPInputStream;

public class HttpRequest {
   private static final String[] b = new String[0];
   private static HttpRequest.b c;
   public final URL a;
   private HttpURLConnection d = null;
   private final String e;
   private HttpRequest.d f;
   private boolean g;
   private boolean h = true;
   private boolean i = false;
   private int j = 8192;
   private String k;
   private int l;

   static {
      c = HttpRequest.b.a;
   }

   public HttpRequest(CharSequence var1, String var2) throws HttpRequest.HttpRequestException {
      try {
         URL var3 = new URL(var1.toString());
         this.a = var3;
      } catch (MalformedURLException var4) {
         throw new HttpRequest.HttpRequestException(var4);
      }

      this.e = var2;
   }

   public static HttpRequest a(CharSequence var0, Map var1, boolean var2) {
      String var4 = a(var0, var1);
      String var3 = var4;
      if(var2) {
         var3 = a((CharSequence)var4);
      }

      return b((CharSequence)var3);
   }

   public static String a(CharSequence param0) throws HttpRequest.HttpRequestException {
      // $FF: Couldn't be decompiled
   }

   public static String a(CharSequence var0, Map var1) {
      String var2 = var0.toString();
      String var3 = var2;
      if(var1 != null) {
         if(var1.isEmpty()) {
            var3 = var2;
         } else {
            StringBuilder var4 = new StringBuilder(var2);
            a(var2, var4);
            b(var2, var4);
            Iterator var5 = var1.entrySet().iterator();
            Entry var6 = (Entry)var5.next();
            var4.append(var6.getKey().toString());
            var4.append('=');
            Object var7 = var6.getValue();
            if(var7 != null) {
               var4.append(var7);
            }

            while(var5.hasNext()) {
               var4.append('&');
               var6 = (Entry)var5.next();
               var4.append(var6.getKey().toString());
               var4.append('=');
               var7 = var6.getValue();
               if(var7 != null) {
                  var4.append(var7);
               }
            }

            var3 = var4.toString();
         }
      }

      return var3;
   }

   private static StringBuilder a(String var0, StringBuilder var1) {
      if(var0.indexOf(58) + 2 == var0.lastIndexOf(47)) {
         var1.append('/');
      }

      return var1;
   }

   public static HttpRequest b(CharSequence var0) throws HttpRequest.HttpRequestException {
      return new HttpRequest(var0, "GET");
   }

   public static HttpRequest b(CharSequence var0, Map var1, boolean var2) {
      String var4 = a(var0, var1);
      String var3 = var4;
      if(var2) {
         var3 = a((CharSequence)var4);
      }

      return c((CharSequence)var3);
   }

   private static StringBuilder b(String var0, StringBuilder var1) {
      int var2 = var0.indexOf(63);
      int var3 = var1.length() - 1;
      if(var2 == -1) {
         var1.append('?');
      } else if(var2 < var3 && var0.charAt(var3) != 38) {
         var1.append('&');
      }

      return var1;
   }

   public static HttpRequest c(CharSequence var0) throws HttpRequest.HttpRequestException {
      return new HttpRequest(var0, "POST");
   }

   public static HttpRequest d(CharSequence var0) throws HttpRequest.HttpRequestException {
      return new HttpRequest(var0, "PUT");
   }

   public static HttpRequest e(CharSequence var0) throws HttpRequest.HttpRequestException {
      return new HttpRequest(var0, "DELETE");
   }

   private static String f(String var0) {
      if(var0 == null || var0.length() <= 0) {
         var0 = "UTF-8";
      }

      return var0;
   }

   private Proxy q() {
      return new Proxy(Type.HTTP, new InetSocketAddress(this.k, this.l));
   }

   private HttpURLConnection r() {
      // $FF: Couldn't be decompiled
   }

   public int a(String var1, int var2) throws HttpRequest.HttpRequestException {
      this.l();
      return this.a().getHeaderFieldInt(var1, var2);
   }

   public HttpRequest a(int var1) {
      this.a().setConnectTimeout(var1);
      return this;
   }

   protected HttpRequest a(final InputStream var1, final OutputStream var2) throws IOException {
      return (HttpRequest)(new HttpRequest.a(var1, this.h) {
         public HttpRequest a() throws IOException {
            byte[] var2x = new byte[HttpRequest.this.j];

            while(true) {
               int var1x = var1.read(var2x);
               if(var1x == -1) {
                  return HttpRequest.this;
               }

               var2.write(var2x, 0, var1x);
            }
         }

         // $FF: synthetic method
         public Object b() throws HttpRequest.HttpRequestException, IOException {
            return this.a();
         }
      }).call();
   }

   public HttpRequest a(String var1, Number var2) throws HttpRequest.HttpRequestException {
      return this.a(var1, (String)null, (Number)var2);
   }

   public HttpRequest a(String var1, String var2) {
      this.a().setRequestProperty(var1, var2);
      return this;
   }

   public HttpRequest a(String var1, String var2, Number var3) throws HttpRequest.HttpRequestException {
      String var4;
      if(var3 != null) {
         var4 = var3.toString();
      } else {
         var4 = null;
      }

      return this.b(var1, var2, var4);
   }

   protected HttpRequest a(String var1, String var2, String var3) throws IOException {
      StringBuilder var4 = new StringBuilder();
      var4.append("form-data; name=\"").append(var1);
      if(var2 != null) {
         var4.append("\"; filename=\"").append(var2);
      }

      var4.append('"');
      this.f("Content-Disposition", var4.toString());
      if(var3 != null) {
         this.f("Content-Type", var3);
      }

      return this.f((CharSequence)"\r\n");
   }

   public HttpRequest a(String param1, String param2, String param3, File param4) throws HttpRequest.HttpRequestException {
      // $FF: Couldn't be decompiled
   }

   public HttpRequest a(String var1, String var2, String var3, InputStream var4) throws HttpRequest.HttpRequestException {
      try {
         this.n();
         this.a(var1, var2, var3);
         this.a((InputStream)var4, (OutputStream)this.f);
         return this;
      } catch (IOException var5) {
         throw new HttpRequest.HttpRequestException(var5);
      }
   }

   public HttpRequest a(String var1, String var2, String var3, String var4) throws HttpRequest.HttpRequestException {
      try {
         this.n();
         this.a(var1, var2, var3);
         this.f.a(var4);
         return this;
      } catch (IOException var5) {
         throw new HttpRequest.HttpRequestException(var5);
      }
   }

   public HttpRequest a(Entry var1) {
      return this.a((String)var1.getKey(), (String)var1.getValue());
   }

   public HttpRequest a(boolean var1) {
      this.a().setUseCaches(var1);
      return this;
   }

   public String a(String var1) throws HttpRequest.HttpRequestException {
      ByteArrayOutputStream var2 = this.d();

      try {
         this.a((InputStream)this.f(), (OutputStream)var2);
         var1 = var2.toString(f(var1));
         return var1;
      } catch (IOException var3) {
         throw new HttpRequest.HttpRequestException(var3);
      }
   }

   public HttpURLConnection a() {
      if(this.d == null) {
         this.d = this.r();
      }

      return this.d;
   }

   public int b() throws HttpRequest.HttpRequestException {
      try {
         this.k();
         int var1 = this.a().getResponseCode();
         return var1;
      } catch (IOException var3) {
         throw new HttpRequest.HttpRequestException(var3);
      }
   }

   public HttpRequest b(String var1, String var2, String var3) throws HttpRequest.HttpRequestException {
      return this.a(var1, var2, (String)null, (String)var3);
   }

   public String b(String var1) throws HttpRequest.HttpRequestException {
      this.l();
      return this.a().getHeaderField(var1);
   }

   public String b(String var1, String var2) {
      return this.c(this.b(var1), var2);
   }

   public int c(String var1) throws HttpRequest.HttpRequestException {
      return this.a(var1, -1);
   }

   protected String c(String var1, String var2) {
      if(var1 != null && var1.length() != 0) {
         int var5 = var1.length();
         int var3 = var1.indexOf(59) + 1;
         if(var3 != 0 && var3 != var5) {
            int var4 = var1.indexOf(59, var3);
            if(var4 == -1) {
               var4 = var5;
            }

            while(true) {
               if(var3 >= var4) {
                  var1 = null;
                  break;
               }

               int var6 = var1.indexOf(61, var3);
               if(var6 != -1 && var6 < var4 && var2.equals(var1.substring(var3, var6).trim())) {
                  String var7 = var1.substring(var6 + 1, var4).trim();
                  var3 = var7.length();
                  if(var3 != 0) {
                     var1 = var7;
                     if(var3 > 2) {
                        var1 = var7;
                        if(34 == var7.charAt(0)) {
                           var1 = var7;
                           if(34 == var7.charAt(var3 - 1)) {
                              var1 = var7.substring(1, var3 - 1);
                           }
                        }
                     }
                     break;
                  }
               }

               ++var4;
               var6 = var1.indexOf(59, var4);
               var3 = var6;
               if(var6 == -1) {
                  var3 = var5;
               }

               var6 = var3;
               var3 = var4;
               var4 = var6;
            }
         } else {
            var1 = null;
         }
      } else {
         var1 = null;
      }

      return var1;
   }

   public boolean c() throws HttpRequest.HttpRequestException {
      boolean var1;
      if(200 == this.b()) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public HttpRequest d(String var1) {
      return this.d(var1, (String)null);
   }

   public HttpRequest d(String var1, String var2) {
      HttpRequest var3;
      if(var2 != null && var2.length() > 0) {
         var3 = this.a("Content-Type", var1 + "; charset=" + var2);
      } else {
         var3 = this.a("Content-Type", var1);
      }

      return var3;
   }

   protected ByteArrayOutputStream d() {
      int var1 = this.j();
      ByteArrayOutputStream var2;
      if(var1 > 0) {
         var2 = new ByteArrayOutputStream(var1);
      } else {
         var2 = new ByteArrayOutputStream();
      }

      return var2;
   }

   public HttpRequest e(String var1, String var2) {
      return this.b(var1, (String)null, var2);
   }

   public String e() throws HttpRequest.HttpRequestException {
      return this.a(this.h());
   }

   public HttpRequest f(CharSequence var1) throws HttpRequest.HttpRequestException {
      try {
         this.m();
         this.f.a(var1.toString());
         return this;
      } catch (IOException var2) {
         throw new HttpRequest.HttpRequestException(var2);
      }
   }

   public HttpRequest f(String var1, String var2) throws HttpRequest.HttpRequestException {
      return this.f((CharSequence)var1).f((CharSequence)": ").f((CharSequence)var2).f((CharSequence)"\r\n");
   }

   public BufferedInputStream f() throws HttpRequest.HttpRequestException {
      return new BufferedInputStream(this.g(), this.j);
   }

   public InputStream g() throws HttpRequest.HttpRequestException {
      InputStream var1;
      if(this.b() < 400) {
         try {
            var1 = this.a().getInputStream();
         } catch (IOException var5) {
            throw new HttpRequest.HttpRequestException(var5);
         }
      } else {
         InputStream var2 = this.a().getErrorStream();
         var1 = var2;
         if(var2 == null) {
            try {
               var1 = this.a().getInputStream();
            } catch (IOException var4) {
               throw new HttpRequest.HttpRequestException(var4);
            }
         }
      }

      Object var6 = var1;
      if(this.i) {
         if(!"gzip".equals(this.i())) {
            var6 = var1;
         } else {
            try {
               var6 = new GZIPInputStream(var1);
            } catch (IOException var3) {
               throw new HttpRequest.HttpRequestException(var3);
            }
         }
      }

      return (InputStream)var6;
   }

   public String h() {
      return this.b("Content-Type", "charset");
   }

   public String i() {
      return this.b("Content-Encoding");
   }

   public int j() {
      return this.c("Content-Length");
   }

   protected HttpRequest k() throws IOException {
      if(this.f != null) {
         if(this.g) {
            this.f.a("\r\n--00content0boundary00--\r\n");
         }

         if(this.h) {
            try {
               this.f.close();
            } catch (IOException var2) {
               ;
            }
         } else {
            this.f.close();
         }

         this.f = null;
      }

      return this;
   }

   protected HttpRequest l() throws HttpRequest.HttpRequestException {
      try {
         HttpRequest var1 = this.k();
         return var1;
      } catch (IOException var2) {
         throw new HttpRequest.HttpRequestException(var2);
      }
   }

   protected HttpRequest m() throws IOException {
      if(this.f == null) {
         this.a().setDoOutput(true);
         String var1 = this.c(this.a().getRequestProperty("Content-Type"), "charset");
         this.f = new HttpRequest.d(this.a().getOutputStream(), var1, this.j);
      }

      return this;
   }

   protected HttpRequest n() throws IOException {
      if(!this.g) {
         this.g = true;
         this.d("multipart/form-data; boundary=00content0boundary00").m();
         this.f.a("--00content0boundary00\r\n");
      } else {
         this.f.a("\r\n--00content0boundary00\r\n");
      }

      return this;
   }

   public URL o() {
      return this.a().getURL();
   }

   public String p() {
      return this.a().getRequestMethod();
   }

   public String toString() {
      return this.p() + ' ' + this.o();
   }

   public static class HttpRequestException extends RuntimeException {
      protected HttpRequestException(IOException var1) {
         super(var1);
      }

      public IOException a() {
         return (IOException)super.getCause();
      }

      // $FF: synthetic method
      public Throwable getCause() {
         return this.a();
      }
   }

   protected abstract static class a extends HttpRequest.c {
      private final Closeable a;
      private final boolean b;

      protected a(Closeable var1, boolean var2) {
         this.a = var1;
         this.b = var2;
      }

      protected void c() throws IOException {
         if(this.a instanceof Flushable) {
            ((Flushable)this.a).flush();
         }

         if(this.b) {
            try {
               this.a.close();
            } catch (IOException var2) {
               ;
            }
         } else {
            this.a.close();
         }

      }
   }

   public interface b {
      HttpRequest.b a = new HttpRequest.b() {
         public HttpURLConnection a(URL var1) throws IOException {
            return (HttpURLConnection)var1.openConnection();
         }

         public HttpURLConnection a(URL var1, Proxy var2) throws IOException {
            return (HttpURLConnection)var1.openConnection(var2);
         }
      };

      HttpURLConnection a(URL var1) throws IOException;

      HttpURLConnection a(URL var1, Proxy var2) throws IOException;
   }

   protected abstract static class c implements Callable {
      protected abstract Object b() throws HttpRequest.HttpRequestException, IOException;

      protected abstract void c() throws IOException;

      public Object call() throws HttpRequest.HttpRequestException {
         // $FF: Couldn't be decompiled
      }
   }

   public static class d extends BufferedOutputStream {
      private final CharsetEncoder a;

      public d(OutputStream var1, String var2, int var3) {
         super(var1, var3);
         this.a = Charset.forName(HttpRequest.f(var2)).newEncoder();
      }

      public HttpRequest.d a(String var1) throws IOException {
         ByteBuffer var2 = this.a.encode(CharBuffer.wrap(var1));
         super.write(var2.array(), 0, var2.limit());
         return this;
      }
   }
}
