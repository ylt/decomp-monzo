package io.fabric.sdk.android.services.network;

import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

class g implements X509TrustManager {
   private static final X509Certificate[] a = new X509Certificate[0];
   private final TrustManager[] b;
   private final h c;
   private final long d;
   private final List e = new LinkedList();
   private final Set f = Collections.synchronizedSet(new HashSet());

   public g(h var1, f var2) {
      this.b = this.a(var1);
      this.c = var1;
      this.d = var2.getPinCreationTimeInMillis();
      String[] var5 = var2.getPins();
      int var4 = var5.length;

      for(int var3 = 0; var3 < var4; ++var3) {
         String var6 = var5[var3];
         this.e.add(this.a(var6));
      }

   }

   private void a(X509Certificate[] var1) throws CertificateException {
      if(this.d != -1L && System.currentTimeMillis() - this.d > 15552000000L) {
         io.fabric.sdk.android.c.h().d("Fabric", "Certificate pins are stale, (" + (System.currentTimeMillis() - this.d) + " millis vs " + 15552000000L + " millis) falling back to system trust.");
      } else {
         var1 = a.a(var1, this.c);
         int var3 = var1.length;
         int var2 = 0;

         while(true) {
            if(var2 >= var3) {
               throw new CertificateException("No valid pins found in chain!");
            }

            if(this.a(var1[var2])) {
               break;
            }

            ++var2;
         }
      }

   }

   private void a(X509Certificate[] var1, String var2) throws CertificateException {
      TrustManager[] var5 = this.b;
      int var4 = var5.length;

      for(int var3 = 0; var3 < var4; ++var3) {
         ((X509TrustManager)var5[var3]).checkServerTrusted(var1, var2);
      }

   }

   private boolean a(X509Certificate param1) throws CertificateException {
      // $FF: Couldn't be decompiled
   }

   private byte[] a(String var1) {
      int var3 = var1.length();
      byte[] var4 = new byte[var3 / 2];

      for(int var2 = 0; var2 < var3; var2 += 2) {
         var4[var2 / 2] = (byte)((Character.digit(var1.charAt(var2), 16) << 4) + Character.digit(var1.charAt(var2 + 1), 16));
      }

      return var4;
   }

   private TrustManager[] a(h var1) {
      try {
         TrustManagerFactory var2 = TrustManagerFactory.getInstance("X509");
         var2.init(var1.a);
         TrustManager[] var5 = var2.getTrustManagers();
         return var5;
      } catch (NoSuchAlgorithmException var3) {
         throw new AssertionError(var3);
      } catch (KeyStoreException var4) {
         throw new AssertionError(var4);
      }
   }

   public void checkClientTrusted(X509Certificate[] var1, String var2) throws CertificateException {
      throw new CertificateException("Client certificates not supported!");
   }

   public void checkServerTrusted(X509Certificate[] var1, String var2) throws CertificateException {
      if(!this.f.contains(var1[0])) {
         this.a(var1, var2);
         this.a(var1);
         this.f.add(var1[0]);
      }

   }

   public X509Certificate[] getAcceptedIssuers() {
      return a;
   }
}
