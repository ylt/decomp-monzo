package io.fabric.sdk.android.services.concurrency.a;

public class e {
   private final int a;
   private final a b;
   private final d c;

   public e(int var1, a var2, d var3) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
   }

   public e(a var1, d var2) {
      this(0, var1, var2);
   }

   public long a() {
      return this.b.getDelayMillis(this.a);
   }

   public e b() {
      return new e(this.a + 1, this.b, this.c);
   }

   public e c() {
      return new e(this.b, this.c);
   }
}
