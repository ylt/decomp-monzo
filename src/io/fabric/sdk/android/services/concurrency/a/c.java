package io.fabric.sdk.android.services.concurrency.a;

public class c implements a {
   private final long a;
   private final int b;

   public c(long var1, int var3) {
      this.a = var1;
      this.b = var3;
   }

   public long getDelayMillis(int var1) {
      return (long)((double)this.a * Math.pow((double)this.b, (double)var1));
   }
}
