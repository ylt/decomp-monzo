package io.fabric.sdk.android.services.concurrency;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

public class j implements b, i, l {
   private final List dependencies = new ArrayList();
   private final AtomicBoolean hasRun = new AtomicBoolean(false);
   private final AtomicReference throwable = new AtomicReference((Object)null);

   public static boolean isProperDelegate(Object var0) {
      boolean var1;
      b var2;
      l var3;
      i var5;
      try {
         var2 = (b)var0;
         var3 = (l)var0;
         var5 = (i)var0;
      } catch (ClassCastException var4) {
         var1 = false;
         return var1;
      }

      if(var2 != null && var3 != null && var5 != null) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public void addDependency(l var1) {
      synchronized(this){}

      try {
         this.dependencies.add(var1);
      } finally {
         ;
      }

   }

   public boolean areDependenciesMet() {
      Iterator var2 = this.getDependencies().iterator();

      boolean var1;
      while(true) {
         if(var2.hasNext()) {
            if(((l)var2.next()).isFinished()) {
               continue;
            }

            var1 = false;
            break;
         }

         var1 = true;
         break;
      }

      return var1;
   }

   public int compareTo(Object var1) {
      return e.a(this, var1);
   }

   public Collection getDependencies() {
      synchronized(this){}

      Collection var1;
      try {
         var1 = Collections.unmodifiableCollection(this.dependencies);
      } finally {
         ;
      }

      return var1;
   }

   public Throwable getError() {
      return (Throwable)this.throwable.get();
   }

   public e getPriority() {
      return e.b;
   }

   public boolean isFinished() {
      return this.hasRun.get();
   }

   public void setError(Throwable var1) {
      this.throwable.set(var1);
   }

   public void setFinished(boolean var1) {
      synchronized(this){}

      try {
         this.hasRun.set(var1);
      } finally {
         ;
      }

   }
}
