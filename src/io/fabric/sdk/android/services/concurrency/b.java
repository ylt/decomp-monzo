package io.fabric.sdk.android.services.concurrency;

import java.util.Collection;

public interface b {
   void addDependency(Object var1);

   boolean areDependenciesMet();

   Collection getDependencies();
}
