package io.fabric.sdk.android.services.concurrency;

import java.util.Collection;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

public class h extends FutureTask implements b, i, l {
   final Object b;

   public h(Runnable var1, Object var2) {
      super(var1, var2);
      this.b = this.a((Object)var1);
   }

   public h(Callable var1) {
      super(var1);
      this.b = this.a((Object)var1);
   }

   public b a() {
      return (b)this.b;
   }

   protected b a(Object var1) {
      if(j.isProperDelegate(var1)) {
         var1 = (b)var1;
      } else {
         var1 = new j();
      }

      return (b)var1;
   }

   public void a(l var1) {
      ((b)((i)this.a())).addDependency(var1);
   }

   // $FF: synthetic method
   public void addDependency(Object var1) {
      this.a((l)var1);
   }

   public boolean areDependenciesMet() {
      return ((b)((i)this.a())).areDependenciesMet();
   }

   public int compareTo(Object var1) {
      return ((i)this.a()).compareTo(var1);
   }

   public Collection getDependencies() {
      return ((b)((i)this.a())).getDependencies();
   }

   public e getPriority() {
      return ((i)this.a()).getPriority();
   }

   public boolean isFinished() {
      return ((l)((i)this.a())).isFinished();
   }

   public void setError(Throwable var1) {
      ((l)((i)this.a())).setError(var1);
   }

   public void setFinished(boolean var1) {
      ((l)((i)this.a())).setFinished(var1);
   }
}
