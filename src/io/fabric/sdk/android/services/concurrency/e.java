package io.fabric.sdk.android.services.concurrency;

public enum e {
   a,
   b,
   c,
   d;

   static int a(i var0, Object var1) {
      e var2;
      if(var1 instanceof i) {
         var2 = ((i)var1).getPriority();
      } else {
         var2 = b;
      }

      return var2.ordinal() - var0.getPriority().ordinal();
   }
}
