package io.fabric.sdk.android.services.concurrency;

import java.util.Collection;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;

public abstract class f extends a implements b, i, l {
   private final j a = new j();

   public void a(l var1) {
      if(this.b() != a.d.a) {
         throw new IllegalStateException("Must not add Dependency after task is running");
      } else {
         ((b)((i)this.e())).addDependency(var1);
      }
   }

   public final void a(ExecutorService var1, Object... var2) {
      super.a((Executor)(new f.a(var1, this)), (Object[])var2);
   }

   // $FF: synthetic method
   public void addDependency(Object var1) {
      this.a((l)var1);
   }

   public boolean areDependenciesMet() {
      return ((b)((i)this.e())).areDependenciesMet();
   }

   public int compareTo(Object var1) {
      return e.a(this, var1);
   }

   public b e() {
      return this.a;
   }

   public Collection getDependencies() {
      return ((b)((i)this.e())).getDependencies();
   }

   public e getPriority() {
      return ((i)this.e()).getPriority();
   }

   public boolean isFinished() {
      return ((l)((i)this.e())).isFinished();
   }

   public void setError(Throwable var1) {
      ((l)((i)this.e())).setError(var1);
   }

   public void setFinished(boolean var1) {
      ((l)((i)this.e())).setFinished(var1);
   }

   private static class a implements Executor {
      private final Executor a;
      private final f b;

      public a(Executor var1, f var2) {
         this.a = var1;
         this.b = var2;
      }

      public void execute(Runnable var1) {
         this.a.execute(new h(var1, (Object)null) {
            public b a() {
               return a.this.b;
            }
         });
      }
   }
}
