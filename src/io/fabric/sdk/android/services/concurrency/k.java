package io.fabric.sdk.android.services.concurrency;

import android.annotation.TargetApi;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.RunnableFuture;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class k extends ThreadPoolExecutor {
   private static final int a = Runtime.getRuntime().availableProcessors();
   private static final int b;
   private static final int c;

   static {
      b = a + 1;
      c = a * 2 + 1;
   }

   k(int var1, int var2, long var3, TimeUnit var5, c var6, ThreadFactory var7) {
      super(var1, var2, var3, var5, var6, var7);
      this.prestartAllCoreThreads();
   }

   public static k a() {
      return a(b, c);
   }

   public static k a(int var0, int var1) {
      return new k(var0, var1, 1L, TimeUnit.SECONDS, new c(), new k.a(10));
   }

   protected void afterExecute(Runnable var1, Throwable var2) {
      l var3 = (l)var1;
      var3.setFinished(true);
      var3.setError(var2);
      this.b().d();
      super.afterExecute(var1, var2);
   }

   public c b() {
      return (c)super.getQueue();
   }

   @TargetApi(9)
   public void execute(Runnable var1) {
      if(j.isProperDelegate(var1)) {
         super.execute(var1);
      } else {
         super.execute(this.newTaskFor(var1, (Object)null));
      }

   }

   // $FF: synthetic method
   public BlockingQueue getQueue() {
      return this.b();
   }

   protected RunnableFuture newTaskFor(Runnable var1, Object var2) {
      return new h(var1, var2);
   }

   protected RunnableFuture newTaskFor(Callable var1) {
      return new h(var1);
   }

   protected static final class a implements ThreadFactory {
      private final int a;

      public a(int var1) {
         this.a = var1;
      }

      public Thread newThread(Runnable var1) {
         Thread var2 = new Thread(var1);
         var2.setPriority(this.a);
         var2.setName("Queue");
         return var2;
      }
   }
}
