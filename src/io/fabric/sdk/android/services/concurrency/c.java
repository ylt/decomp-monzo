package io.fabric.sdk.android.services.concurrency;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

public class c extends PriorityBlockingQueue {
   final Queue a = new LinkedList();
   private final ReentrantLock b = new ReentrantLock();

   public b a() throws InterruptedException {
      return this.b(0, (Long)null, (TimeUnit)null);
   }

   b a(int var1, Long var2, TimeUnit var3) throws InterruptedException {
      b var4;
      switch(var1) {
      case 0:
         var4 = (b)super.take();
         break;
      case 1:
         var4 = (b)super.peek();
         break;
      case 2:
         var4 = (b)super.poll();
         break;
      case 3:
         var4 = (b)super.poll(var2.longValue(), var3);
         break;
      default:
         var4 = null;
      }

      return var4;
   }

   public b a(long var1, TimeUnit var3) throws InterruptedException {
      return this.b(3, Long.valueOf(var1), var3);
   }

   boolean a(int param1, b param2) {
      // $FF: Couldn't be decompiled
   }

   boolean a(b var1) {
      return var1.areDependenciesMet();
   }

   Object[] a(Object[] var1, Object[] var2) {
      int var4 = var1.length;
      int var3 = var2.length;
      Object[] var5 = (Object[])((Object[])Array.newInstance(var1.getClass().getComponentType(), var4 + var3));
      System.arraycopy(var1, 0, var5, 0, var4);
      System.arraycopy(var2, 0, var5, var4, var3);
      return var5;
   }

   public b b() {
      b var1 = null;

      b var2;
      try {
         var2 = this.b(1, (Long)null, (TimeUnit)null);
      } catch (InterruptedException var3) {
         return var1;
      }

      var1 = var2;
      return var1;
   }

   b b(int var1, Long var2, TimeUnit var3) throws InterruptedException {
      while(true) {
         b var4 = this.a(var1, var2, var3);
         if(var4 == null || this.a(var4)) {
            return var4;
         }

         this.a(var1, var4);
      }
   }

   public b c() {
      b var1 = null;

      b var2;
      try {
         var2 = this.b(2, (Long)null, (TimeUnit)null);
      } catch (InterruptedException var3) {
         return var1;
      }

      var1 = var2;
      return var1;
   }

   public void clear() {
      try {
         this.b.lock();
         this.a.clear();
         super.clear();
      } finally {
         this.b.unlock();
      }

   }

   public boolean contains(Object var1) {
      boolean var4 = false;

      boolean var2;
      label43: {
         label42: {
            try {
               var4 = true;
               this.b.lock();
               if(super.contains(var1)) {
                  var4 = false;
                  break label42;
               }

               var2 = this.a.contains(var1);
               var4 = false;
            } finally {
               if(var4) {
                  this.b.unlock();
               }
            }

            if(!var2) {
               var2 = false;
               break label43;
            }
         }

         var2 = true;
      }

      this.b.unlock();
      return var2;
   }

   public void d() {
      // $FF: Couldn't be decompiled
   }

   public int drainTo(Collection param1) {
      // $FF: Couldn't be decompiled
   }

   public int drainTo(Collection param1, int param2) {
      // $FF: Couldn't be decompiled
   }

   // $FF: synthetic method
   public Object peek() {
      return this.b();
   }

   // $FF: synthetic method
   public Object poll() {
      return this.c();
   }

   // $FF: synthetic method
   public Object poll(long var1, TimeUnit var3) throws InterruptedException {
      return this.a(var1, var3);
   }

   public boolean remove(Object var1) {
      boolean var4 = false;

      boolean var2;
      label43: {
         label42: {
            try {
               var4 = true;
               this.b.lock();
               if(super.remove(var1)) {
                  var4 = false;
                  break label42;
               }

               var2 = this.a.remove(var1);
               var4 = false;
            } finally {
               if(var4) {
                  this.b.unlock();
               }
            }

            if(!var2) {
               var2 = false;
               break label43;
            }
         }

         var2 = true;
      }

      this.b.unlock();
      return var2;
   }

   public boolean removeAll(Collection var1) {
      boolean var2;
      boolean var3;
      try {
         this.b.lock();
         var2 = super.removeAll(var1);
         var3 = this.a.removeAll(var1);
      } finally {
         this.b.unlock();
      }

      return var2 | var3;
   }

   public int size() {
      int var1;
      int var2;
      try {
         this.b.lock();
         var1 = this.a.size();
         var2 = super.size();
      } finally {
         this.b.unlock();
      }

      return var1 + var2;
   }

   // $FF: synthetic method
   public Object take() throws InterruptedException {
      return this.a();
   }

   public Object[] toArray() {
      Object[] var1;
      try {
         this.b.lock();
         var1 = this.a(super.toArray(), this.a.toArray());
      } finally {
         this.b.unlock();
      }

      return var1;
   }

   public Object[] toArray(Object[] var1) {
      try {
         this.b.lock();
         var1 = this.a(super.toArray(var1), this.a.toArray(var1));
      } finally {
         this.b.unlock();
      }

      return var1;
   }
}
