package io.fabric.sdk.android.services.concurrency;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.util.Log;
import java.util.LinkedList;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class a {
   private static final int a = Runtime.getRuntime().availableProcessors();
   public static final Executor b;
   public static final Executor c;
   private static final int d;
   private static final int e;
   private static final ThreadFactory f;
   private static final BlockingQueue g;
   private static final a.b h;
   private static volatile Executor i;
   private final a.e j;
   private final FutureTask k;
   private volatile a.d l;
   private final AtomicBoolean m;
   private final AtomicBoolean n;

   static {
      d = a + 1;
      e = a * 2 + 1;
      f = new ThreadFactory() {
         private final AtomicInteger a = new AtomicInteger(1);

         public Thread newThread(Runnable var1) {
            return new Thread(var1, "AsyncTask #" + this.a.getAndIncrement());
         }
      };
      g = new LinkedBlockingQueue(128);
      b = new ThreadPoolExecutor(d, e, 1L, TimeUnit.SECONDS, g, f);
      c = new a.c(null);
      h = new a.b();
      i = c;
   }

   public a() {
      this.l = a.d.a;
      this.m = new AtomicBoolean();
      this.n = new AtomicBoolean();
      this.j = new a.e() {
         public Object call() throws Exception {
            a.this.n.set(true);
            Process.setThreadPriority(10);
            return a.this.d(a.this.a(this.b));
         }
      };
      this.k = new FutureTask(this.j) {
         protected void done() {
            try {
               a.this.c(this.get());
            } catch (InterruptedException var2) {
               Log.w("AsyncTask", var2);
            } catch (ExecutionException var3) {
               throw new RuntimeException("An error occured while executing doInBackground()", var3.getCause());
            } catch (CancellationException var4) {
               a.this.c((Object)null);
            }

         }
      };
   }

   private void c(Object var1) {
      if(!this.n.get()) {
         this.d(var1);
      }

   }

   private Object d(Object var1) {
      h.obtainMessage(1, new a.a(this, new Object[]{var1})).sendToTarget();
      return var1;
   }

   private void e(Object var1) {
      if(this.d()) {
         this.b(var1);
      } else {
         this.a(var1);
      }

      this.l = a.d.c;
   }

   public final a a(Executor var1, Object... var2) {
      if(this.l != a.d.a) {
         switch(null.a[this.l.ordinal()]) {
         case 1:
            throw new IllegalStateException("Cannot execute task: the task is already running.");
         case 2:
            throw new IllegalStateException("Cannot execute task: the task has already been executed (a task can be executed only once)");
         }
      }

      this.l = a.d.b;
      this.a();
      this.j.b = var2;
      var1.execute(this.k);
      return this;
   }

   protected abstract Object a(Object... var1);

   protected void a() {
   }

   protected void a(Object var1) {
   }

   public final boolean a(boolean var1) {
      this.m.set(true);
      return this.k.cancel(var1);
   }

   public final a.d b() {
      return this.l;
   }

   protected void b(Object var1) {
      this.c();
   }

   protected void b(Object... var1) {
   }

   protected void c() {
   }

   public final boolean d() {
      return this.m.get();
   }

   private static class a {
      final a a;
      final Object[] b;

      a(a var1, Object... var2) {
         this.a = var1;
         this.b = var2;
      }
   }

   private static class b extends Handler {
      public b() {
         super(Looper.getMainLooper());
      }

      public void handleMessage(Message var1) {
         a.a var2 = (a.a)var1.obj;
         switch(var1.what) {
         case 1:
            var2.a.e(var2.b[0]);
            break;
         case 2:
            var2.a.b(var2.b);
         }

      }
   }

   private static class c implements Executor {
      final LinkedList a;
      Runnable b;

      private c() {
         this.a = new LinkedList();
      }

      // $FF: synthetic method
      c(Object var1) {
         this();
      }

      protected void a() {
         // $FF: Couldn't be decompiled
      }

      public void execute(final Runnable var1) {
         synchronized(this){}

         try {
            LinkedList var3 = this.a;
            Runnable var2 = new Runnable() {
               public void run() {
                  try {
                     var1.run();
                  } finally {
                     c.this.a();
                  }

               }
            };
            var3.offer(var2);
            if(this.b == null) {
               this.a();
            }
         } finally {
            ;
         }

      }
   }

   public static enum d {
      a,
      b,
      c;
   }

   private abstract static class e implements Callable {
      Object[] b;

      private e() {
      }

      // $FF: synthetic method
      e(Object var1) {
         this();
      }
   }
}
