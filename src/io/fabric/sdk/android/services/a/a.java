package io.fabric.sdk.android.services.a;

import android.content.Context;

public abstract class a implements c {
   private final c a;

   public a(c var1) {
      this.a = var1;
   }

   private void b(Context var1, Object var2) {
      if(var2 == null) {
         throw new NullPointerException();
      } else {
         this.a(var1, var2);
      }
   }

   protected abstract Object a(Context var1);

   public final Object a(Context param1, d param2) throws Exception {
      // $FF: Couldn't be decompiled
   }

   protected abstract void a(Context var1, Object var2);
}
