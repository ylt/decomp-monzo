package io.fabric.sdk.android.services.b;

import android.app.ActivityManager;
import android.app.ActivityManager.MemoryInfo;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.hardware.SensorManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Debug;
import android.os.StatFs;
import android.provider.Settings.Secure;
import android.text.TextUtils;
import java.io.Closeable;
import java.io.File;
import java.io.Flushable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;

public class i {
   public static final Comparator a = new Comparator() {
      public int a(File var1, File var2) {
         return (int)(var1.lastModified() - var2.lastModified());
      }

      // $FF: synthetic method
      public int compare(Object var1, Object var2) {
         return this.a((File)var1, (File)var2);
      }
   };
   private static Boolean b = null;
   private static final char[] c = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
   private static long d = -1L;

   public static int a() {
      return i.a.a().ordinal();
   }

   public static int a(Context var0, String var1, String var2) {
      return var0.getResources().getIdentifier(var1, var2, j(var0));
   }

   public static int a(Context var0, boolean var1) {
      Float var3 = c(var0);
      byte var2;
      if(var1 && var3 != null) {
         if((double)var3.floatValue() >= 99.0D) {
            var2 = 3;
         } else if((double)var3.floatValue() < 99.0D) {
            var2 = 2;
         } else {
            var2 = 0;
         }
      } else {
         var2 = 1;
      }

      return var2;
   }

   static long a(String var0, String var1, int var2) {
      return Long.parseLong(var0.split(var1)[0].trim()) * (long)var2;
   }

   public static RunningAppProcessInfo a(String var0, Context var1) {
      List var4 = ((ActivityManager)var1.getSystemService("activity")).getRunningAppProcesses();
      RunningAppProcessInfo var3;
      if(var4 != null) {
         Iterator var2 = var4.iterator();

         while(var2.hasNext()) {
            RunningAppProcessInfo var5 = (RunningAppProcessInfo)var2.next();
            if(var5.processName.equals(var0)) {
               var3 = var5;
               return var3;
            }
         }
      }

      var3 = null;
      return var3;
   }

   public static SharedPreferences a(Context var0) {
      return var0.getSharedPreferences("com.crashlytics.prefs", 0);
   }

   public static String a(int var0) {
      if(var0 < 0) {
         throw new IllegalArgumentException("value must be zero or greater");
      } else {
         return String.format(Locale.US, "%1$10s", new Object[]{Integer.valueOf(var0)}).replace(' ', '0');
      }
   }

   public static String a(File param0, String param1) {
      // $FF: Couldn't be decompiled
   }

   public static String a(InputStream var0) throws IOException {
      Scanner var1 = (new Scanner(var0)).useDelimiter("\\A");
      String var2;
      if(var1.hasNext()) {
         var2 = var1.next();
      } else {
         var2 = "";
      }

      return var2;
   }

   private static String a(InputStream param0, String param1) {
      // $FF: Couldn't be decompiled
   }

   public static String a(String var0) {
      return a(var0, "SHA-1");
   }

   private static String a(String var0, String var1) {
      return a(var0.getBytes(), var1);
   }

   public static String a(byte[] var0) {
      char[] var3 = new char[var0.length * 2];

      for(int var1 = 0; var1 < var0.length; ++var1) {
         int var2 = var0[var1] & 255;
         var3[var1 * 2] = c[var2 >>> 4];
         var3[var1 * 2 + 1] = c[var2 & 15];
      }

      return new String(var3);
   }

   private static String a(byte[] var0, String var1) {
      MessageDigest var2;
      String var4;
      try {
         var2 = MessageDigest.getInstance(var1);
      } catch (NoSuchAlgorithmException var3) {
         io.fabric.sdk.android.c.h().e("Fabric", "Could not create hashing algorithm: " + var1 + ", returning empty string.", var3);
         var4 = "";
         return var4;
      }

      var2.update(var0);
      var4 = a(var2.digest());
      return var4;
   }

   public static String a(String... var0) {
      String var5;
      if(var0 != null && var0.length != 0) {
         ArrayList var3 = new ArrayList();
         int var2 = var0.length;

         for(int var1 = 0; var1 < var2; ++var1) {
            String var4 = var0[var1];
            if(var4 != null) {
               var3.add(var4.replace("-", "").toLowerCase(Locale.US));
            }
         }

         Collections.sort(var3);
         StringBuilder var6 = new StringBuilder();
         Iterator var7 = var3.iterator();

         while(var7.hasNext()) {
            var6.append((String)var7.next());
         }

         var5 = var6.toString();
         if(var5.length() > 0) {
            var5 = a(var5);
         } else {
            var5 = null;
         }
      } else {
         var5 = null;
      }

      return var5;
   }

   public static void a(Context var0, int var1, String var2, String var3) {
      if(e(var0)) {
         io.fabric.sdk.android.c.h().a(var1, "Fabric", var3);
      }

   }

   public static void a(Context var0, String var1) {
      if(e(var0)) {
         io.fabric.sdk.android.c.h().a("Fabric", var1);
      }

   }

   public static void a(Context var0, String var1, Throwable var2) {
      if(e(var0)) {
         io.fabric.sdk.android.c.h().e("Fabric", var1);
      }

   }

   public static void a(Closeable var0) {
      if(var0 != null) {
         try {
            var0.close();
         } catch (RuntimeException var1) {
            throw var1;
         } catch (Exception var2) {
            ;
         }
      }

   }

   public static void a(Closeable var0, String var1) {
      if(var0 != null) {
         try {
            var0.close();
         } catch (IOException var2) {
            io.fabric.sdk.android.c.h().e("Fabric", var1, var2);
         }
      }

   }

   public static void a(Flushable var0, String var1) {
      if(var0 != null) {
         try {
            var0.flush();
         } catch (IOException var2) {
            io.fabric.sdk.android.c.h().e("Fabric", var1, var2);
         }
      }

   }

   public static void a(InputStream var0, OutputStream var1, byte[] var2) throws IOException {
      while(true) {
         int var3 = var0.read(var2);
         if(var3 == -1) {
            return;
         }

         var1.write(var2, 0, var3);
      }
   }

   public static boolean a(Context var0, String var1, boolean var2) {
      boolean var4 = var2;
      if(var0 != null) {
         Resources var5 = var0.getResources();
         var4 = var2;
         if(var5 != null) {
            int var3 = a(var0, var1, "bool");
            if(var3 > 0) {
               var4 = var5.getBoolean(var3);
            } else {
               var3 = a(var0, var1, "string");
               var4 = var2;
               if(var3 > 0) {
                  var4 = Boolean.parseBoolean(var0.getString(var3));
               }
            }
         }
      }

      return var4;
   }

   public static long b() {
      // $FF: Couldn't be decompiled
   }

   public static long b(Context var0) {
      MemoryInfo var1 = new MemoryInfo();
      ((ActivityManager)var0.getSystemService("activity")).getMemoryInfo(var1);
      return var1.availMem;
   }

   public static long b(String var0) {
      StatFs var3 = new StatFs(var0);
      long var1 = (long)var3.getBlockSize();
      return (long)var3.getBlockCount() * var1 - (long)var3.getAvailableBlocks() * var1;
   }

   public static String b(int var0) {
      String var1;
      switch(var0) {
      case 2:
         var1 = "V";
         break;
      case 3:
         var1 = "D";
         break;
      case 4:
         var1 = "I";
         break;
      case 5:
         var1 = "W";
         break;
      case 6:
         var1 = "E";
         break;
      case 7:
         var1 = "A";
         break;
      default:
         var1 = "?";
      }

      return var1;
   }

   public static String b(Context var0, String var1) {
      int var2 = a(var0, var1, "string");
      String var3;
      if(var2 > 0) {
         var3 = var0.getString(var2);
      } else {
         var3 = "";
      }

      return var3;
   }

   public static String b(InputStream var0) {
      return a(var0, "SHA-1");
   }

   public static Float c(Context var0) {
      Object var3 = null;
      Intent var4 = var0.registerReceiver((BroadcastReceiver)null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
      Float var5;
      if(var4 == null) {
         var5 = (Float)var3;
      } else {
         int var2 = var4.getIntExtra("level", -1);
         int var1 = var4.getIntExtra("scale", -1);
         var5 = Float.valueOf((float)var2 / (float)var1);
      }

      return var5;
   }

   public static boolean c() {
      boolean var0;
      if(!Debug.isDebuggerConnected() && !Debug.waitingForDebugger()) {
         var0 = false;
      } else {
         var0 = true;
      }

      return var0;
   }

   public static boolean c(Context var0, String var1) {
      boolean var2;
      if(var0.checkCallingOrSelfPermission(var1) == 0) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   public static boolean c(String var0) {
      boolean var1;
      if(var0 != null && var0.length() != 0) {
         var1 = false;
      } else {
         var1 = true;
      }

      return var1;
   }

   public static boolean d(Context var0) {
      boolean var1 = false;
      if(!f(var0)) {
         if(((SensorManager)var0.getSystemService("sensor")).getDefaultSensor(8) != null) {
            var1 = true;
         } else {
            var1 = false;
         }
      }

      return var1;
   }

   public static boolean e(Context var0) {
      if(b == null) {
         b = Boolean.valueOf(a(var0, "com.crashlytics.Trace", false));
      }

      return b.booleanValue();
   }

   public static boolean f(Context var0) {
      String var2 = Secure.getString(var0.getContentResolver(), "android_id");
      boolean var1;
      if(!"sdk".equals(Build.PRODUCT) && !"google_sdk".equals(Build.PRODUCT) && var2 != null) {
         var1 = false;
      } else {
         var1 = true;
      }

      return var1;
   }

   public static boolean g(Context var0) {
      boolean var2 = true;
      boolean var3 = f(var0);
      String var4 = Build.TAGS;
      boolean var1;
      if(!var3 && var4 != null && var4.contains("test-keys")) {
         var1 = var2;
      } else {
         var1 = var2;
         if(!(new File("/system/app/Superuser.apk")).exists()) {
            File var5 = new File("/system/xbin/su");
            if(!var3) {
               var1 = var2;
               if(var5.exists()) {
                  return var1;
               }
            }

            var1 = false;
         }
      }

      return var1;
   }

   public static int h(Context var0) {
      byte var2 = 0;
      if(f(var0)) {
         var2 = 1;
      }

      int var1 = var2;
      if(g(var0)) {
         var1 = var2 | 2;
      }

      int var3 = var1;
      if(c()) {
         var3 = var1 | 4;
      }

      return var3;
   }

   public static boolean i(Context var0) {
      boolean var1;
      if((var0.getApplicationInfo().flags & 2) != 0) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public static String j(Context var0) {
      int var1 = var0.getApplicationContext().getApplicationInfo().icon;
      String var2;
      if(var1 > 0) {
         var2 = var0.getResources().getResourcePackageName(var1);
      } else {
         var2 = var0.getPackageName();
      }

      return var2;
   }

   public static String k(Context param0) {
      // $FF: Couldn't be decompiled
   }

   public static int l(Context var0) {
      return var0.getApplicationContext().getApplicationInfo().icon;
   }

   public static String m(Context var0) {
      String var3 = null;
      int var2 = a(var0, "io.fabric.android.build_id", "string");
      int var1 = var2;
      if(var2 == 0) {
         var1 = a(var0, "com.crashlytics.android.build_id", "string");
      }

      if(var1 != 0) {
         var3 = var0.getResources().getString(var1);
         io.fabric.sdk.android.c.h().a("Fabric", "Build ID is: " + var3);
      }

      return var3;
   }

   public static boolean n(Context var0) {
      boolean var1;
      if(c(var0, "android.permission.ACCESS_NETWORK_STATE")) {
         NetworkInfo var2 = ((ConnectivityManager)var0.getSystemService("connectivity")).getActiveNetworkInfo();
         if(var2 != null && var2.isConnectedOrConnecting()) {
            var1 = true;
         } else {
            var1 = false;
         }
      } else {
         var1 = true;
      }

      return var1;
   }

   static enum a {
      a,
      b,
      c,
      d,
      e,
      f,
      g,
      h,
      i,
      j;

      private static final Map k = new HashMap(4);

      static {
         k.put("armeabi-v7a", g);
         k.put("armeabi", f);
         k.put("arm64-v8a", j);
         k.put("x86", a);
      }

      static i.a a() {
         String var0 = Build.CPU_ABI;
         i.a var2;
         if(TextUtils.isEmpty(var0)) {
            io.fabric.sdk.android.c.h().a("Fabric", "Architecture#getValue()::Build.CPU_ABI returned null or empty");
            var2 = h;
         } else {
            var0 = var0.toLowerCase(Locale.US);
            i.a var1 = (i.a)k.get(var0);
            var2 = var1;
            if(var1 == null) {
               var2 = h;
            }
         }

         return var2;
      }
   }
}
