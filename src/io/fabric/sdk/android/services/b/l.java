package io.fabric.sdk.android.services.b;

public enum l {
   a(1),
   b(2),
   c(3),
   d(4);

   private final int e;

   private l(int var3) {
      this.e = var3;
   }

   public static l a(String var0) {
      l var1;
      if("io.crash.air".equals(var0)) {
         var1 = c;
      } else if(var0 != null) {
         var1 = d;
      } else {
         var1 = a;
      }

      return var1;
   }

   public int a() {
      return this.e;
   }

   public String toString() {
      return Integer.toString(this.e);
   }
}
