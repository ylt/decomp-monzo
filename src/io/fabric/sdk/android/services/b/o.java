package io.fabric.sdk.android.services.b;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Build.VERSION;
import android.provider.Settings.Secure;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Pattern;

public class o {
   private static final Pattern d = Pattern.compile("[^\\p{Alnum}]");
   private static final String e = Pattern.quote("/");
   c a;
   b b;
   boolean c;
   private final ReentrantLock f = new ReentrantLock();
   private final p g;
   private final boolean h;
   private final boolean i;
   private final Context j;
   private final String k;
   private final String l;
   private final Collection m;

   public o(Context var1, String var2, String var3, Collection var4) {
      if(var1 == null) {
         throw new IllegalArgumentException("appContext must not be null");
      } else if(var2 == null) {
         throw new IllegalArgumentException("appIdentifier must not be null");
      } else if(var4 == null) {
         throw new IllegalArgumentException("kits must not be null");
      } else {
         this.j = var1;
         this.k = var2;
         this.l = var3;
         this.m = var4;
         this.g = new p();
         this.a = new c(var1);
         this.h = i.a(var1, "com.crashlytics.CollectDeviceIdentifiers", true);
         if(!this.h) {
            io.fabric.sdk.android.c.h().a("Fabric", "Device ID collection disabled for " + var1.getPackageName());
         }

         this.i = i.a(var1, "com.crashlytics.CollectUserIdentifiers", true);
         if(!this.i) {
            io.fabric.sdk.android.c.h().a("Fabric", "User information collection disabled for " + var1.getPackageName());
         }

      }
   }

   private String a(SharedPreferences param1) {
      // $FF: Couldn't be decompiled
   }

   private String a(String var1) {
      if(var1 == null) {
         var1 = null;
      } else {
         var1 = d.matcher(var1).replaceAll("").toLowerCase(Locale.US);
      }

      return var1;
   }

   private void a(Map var1, o.a var2, String var3) {
      if(var3 != null) {
         var1.put(var2, var3);
      }

   }

   private String b(String var1) {
      return var1.replaceAll(e, "");
   }

   public boolean a() {
      return this.i;
   }

   public String b() {
      String var2 = this.l;
      String var1 = var2;
      if(var2 == null) {
         SharedPreferences var3 = i.a(this.j);
         var2 = var3.getString("crashlytics.installation.id", (String)null);
         var1 = var2;
         if(var2 == null) {
            var1 = this.a(var3);
         }
      }

      return var1;
   }

   public String c() {
      return this.k;
   }

   public String d() {
      return this.e() + "/" + this.f();
   }

   public String e() {
      return this.b(VERSION.RELEASE);
   }

   public String f() {
      return this.b(VERSION.INCREMENTAL);
   }

   public String g() {
      return String.format(Locale.US, "%s/%s", new Object[]{this.b(Build.MANUFACTURER), this.b(Build.MODEL)});
   }

   public String h() {
      String var1 = "";
      if(this.h) {
         String var2 = this.n();
         var1 = var2;
         if(var2 == null) {
            SharedPreferences var3 = i.a(this.j);
            var2 = var3.getString("crashlytics.installation.id", (String)null);
            var1 = var2;
            if(var2 == null) {
               var1 = this.a(var3);
            }
         }
      }

      return var1;
   }

   public Map i() {
      HashMap var1 = new HashMap();
      Iterator var2 = this.m.iterator();

      while(true) {
         io.fabric.sdk.android.h var3;
         do {
            if(!var2.hasNext()) {
               this.a(var1, o.a.d, this.n());
               this.a(var1, o.a.g, this.m());
               return Collections.unmodifiableMap(var1);
            }

            var3 = (io.fabric.sdk.android.h)var2.next();
         } while(!(var3 instanceof m));

         Iterator var4 = ((m)var3).getDeviceIdentifiers().entrySet().iterator();

         while(var4.hasNext()) {
            Entry var5 = (Entry)var4.next();
            this.a(var1, (o.a)var5.getKey(), (String)var5.getValue());
         }
      }
   }

   public String j() {
      return this.g.a(this.j);
   }

   b k() {
      synchronized(this){}

      b var1;
      try {
         if(!this.c) {
            this.b = this.a.a();
            this.c = true;
         }

         var1 = this.b;
      } finally {
         ;
      }

      return var1;
   }

   public Boolean l() {
      Object var2 = null;
      Boolean var1 = (Boolean)var2;
      if(this.h) {
         b var3 = this.k();
         var1 = (Boolean)var2;
         if(var3 != null) {
            var1 = Boolean.valueOf(var3.b);
         }
      }

      return var1;
   }

   public String m() {
      Object var2 = null;
      String var1 = (String)var2;
      if(this.h) {
         b var3 = this.k();
         var1 = (String)var2;
         if(var3 != null) {
            var1 = var3.a;
         }
      }

      return var1;
   }

   public String n() {
      Object var2 = null;
      String var1 = (String)var2;
      if(this.h) {
         String var3 = Secure.getString(this.j.getContentResolver(), "android_id");
         var1 = (String)var2;
         if(!"9774d56d682e549c".equals(var3)) {
            var1 = this.a(var3);
         }
      }

      return var1;
   }

   public static enum a {
      a(1),
      b(2),
      c(53),
      d(100),
      e(101),
      f(102),
      g(103);

      public final int h;

      private a(int var3) {
         this.h = var3;
      }
   }
}
