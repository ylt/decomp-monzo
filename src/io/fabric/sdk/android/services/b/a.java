package io.fabric.sdk.android.services.b;

import io.fabric.sdk.android.services.network.HttpRequest;
import java.util.Collections;
import java.util.Map;
import java.util.regex.Pattern;

public abstract class a {
   public static final String ACCEPT_JSON_VALUE = "application/json";
   public static final String ANDROID_CLIENT_TYPE = "android";
   public static final String CLS_ANDROID_SDK_DEVELOPER_TOKEN = "470fa2b4ae81cd56ecbcda9735803434cec591fa";
   public static final String CRASHLYTICS_USER_AGENT = "Crashlytics Android SDK/";
   public static final int DEFAULT_TIMEOUT = 10000;
   public static final String HEADER_ACCEPT = "Accept";
   public static final String HEADER_API_KEY = "X-CRASHLYTICS-API-KEY";
   public static final String HEADER_CLIENT_TYPE = "X-CRASHLYTICS-API-CLIENT-TYPE";
   public static final String HEADER_CLIENT_VERSION = "X-CRASHLYTICS-API-CLIENT-VERSION";
   public static final String HEADER_DEVELOPER_TOKEN = "X-CRASHLYTICS-DEVELOPER-TOKEN";
   public static final String HEADER_REQUEST_ID = "X-REQUEST-ID";
   public static final String HEADER_USER_AGENT = "User-Agent";
   private static final Pattern PROTOCOL_AND_HOST_PATTERN = Pattern.compile("http(s?)://[^\\/]+", 2);
   protected final io.fabric.sdk.android.h kit;
   private final io.fabric.sdk.android.services.network.c method;
   private final String protocolAndHostOverride;
   private final io.fabric.sdk.android.services.network.d requestFactory;
   private final String url;

   public a(io.fabric.sdk.android.h var1, String var2, String var3, io.fabric.sdk.android.services.network.d var4, io.fabric.sdk.android.services.network.c var5) {
      if(var3 == null) {
         throw new IllegalArgumentException("url must not be null.");
      } else if(var4 == null) {
         throw new IllegalArgumentException("requestFactory must not be null.");
      } else {
         this.kit = var1;
         this.protocolAndHostOverride = var2;
         this.url = this.overrideProtocolAndHost(var3);
         this.requestFactory = var4;
         this.method = var5;
      }
   }

   private String overrideProtocolAndHost(String var1) {
      String var2 = var1;
      if(!i.c(this.protocolAndHostOverride)) {
         var2 = PROTOCOL_AND_HOST_PATTERN.matcher(var1).replaceFirst(this.protocolAndHostOverride);
      }

      return var2;
   }

   protected HttpRequest getHttpRequest() {
      return this.getHttpRequest(Collections.emptyMap());
   }

   protected HttpRequest getHttpRequest(Map var1) {
      return this.requestFactory.a(this.method, this.getUrl(), var1).a(false).a(10000).a("User-Agent", "Crashlytics Android SDK/" + this.kit.getVersion()).a("X-CRASHLYTICS-DEVELOPER-TOKEN", "470fa2b4ae81cd56ecbcda9735803434cec591fa");
   }

   protected String getUrl() {
      return this.url;
   }
}
