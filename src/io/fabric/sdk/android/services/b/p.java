package io.fabric.sdk.android.services.b;

import android.content.Context;

public class p {
   private final io.fabric.sdk.android.services.a.d a = new io.fabric.sdk.android.services.a.d() {
      public String a(Context var1) throws Exception {
         String var2 = var1.getPackageManager().getInstallerPackageName(var1.getPackageName());
         String var3 = var2;
         if(var2 == null) {
            var3 = "";
         }

         return var3;
      }

      // $FF: synthetic method
      public Object load(Context var1) throws Exception {
         return this.a(var1);
      }
   };
   private final io.fabric.sdk.android.services.a.b b = new io.fabric.sdk.android.services.a.b();

   public String a(Context var1) {
      boolean var2;
      String var4;
      try {
         var4 = (String)this.b.a(var1, this.a);
         var2 = "".equals(var4);
      } catch (Exception var3) {
         io.fabric.sdk.android.c.h().e("Fabric", "Failed to determine installer package name", var3);
         var4 = null;
         return var4;
      }

      if(var2) {
         var4 = null;
      }

      return var4;
   }
}
