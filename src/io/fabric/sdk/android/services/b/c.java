package io.fabric.sdk.android.services.b;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;

class c {
   private final Context a;
   private final io.fabric.sdk.android.services.d.c b;

   public c(Context var1) {
      this.a = var1.getApplicationContext();
      this.b = new io.fabric.sdk.android.services.d.d(var1, "TwitterAdvertisingInfoPreferences");
   }

   private void a(final b var1) {
      (new Thread(new h() {
         public void onRun() {
            b var1x = c.this.e();
            if(!var1.equals(var1x)) {
               io.fabric.sdk.android.c.h().a("Fabric", "Asychronously getting Advertising Info and storing it to preferences");
               c.this.b(var1x);
            }

         }
      })).start();
   }

   @SuppressLint({"CommitPrefEdits"})
   private void b(b var1) {
      if(this.c(var1)) {
         this.b.a(this.b.b().putString("advertising_id", var1.a).putBoolean("limit_ad_tracking_enabled", var1.b));
      } else {
         this.b.a(this.b.b().remove("advertising_id").remove("limit_ad_tracking_enabled"));
      }

   }

   private boolean c(b var1) {
      boolean var2;
      if(var1 != null && !TextUtils.isEmpty(var1.a)) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   private b e() {
      b var1 = this.c().a();
      if(!this.c(var1)) {
         var1 = this.d().a();
         if(!this.c(var1)) {
            io.fabric.sdk.android.c.h().a("Fabric", "AdvertisingInfo not present");
         } else {
            io.fabric.sdk.android.c.h().a("Fabric", "Using AdvertisingInfo from Service Provider");
         }
      } else {
         io.fabric.sdk.android.c.h().a("Fabric", "Using AdvertisingInfo from Reflection Provider");
      }

      return var1;
   }

   public b a() {
      b var1 = this.b();
      if(this.c(var1)) {
         io.fabric.sdk.android.c.h().a("Fabric", "Using AdvertisingInfo from Preference Store");
         this.a(var1);
      } else {
         var1 = this.e();
         this.b(var1);
      }

      return var1;
   }

   protected b b() {
      return new b(this.b.a().getString("advertising_id", ""), this.b.a().getBoolean("limit_ad_tracking_enabled", false));
   }

   public f c() {
      return new d(this.a);
   }

   public f d() {
      return new e(this.a);
   }
}
