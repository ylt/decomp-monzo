package io.fabric.sdk.android.services.b;

import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

class e implements f {
   private final Context a;

   public e(Context var1) {
      this.a = var1.getApplicationContext();
   }

   public b a() {
      // $FF: Couldn't be decompiled
   }

   private static final class a implements ServiceConnection {
      private boolean a;
      private final LinkedBlockingQueue b;

      private a() {
         this.a = false;
         this.b = new LinkedBlockingQueue(1);
      }

      // $FF: synthetic method
      a(Object var1) {
         this();
      }

      public IBinder a() {
         if(this.a) {
            io.fabric.sdk.android.c.h().e("Fabric", "getBinder already called");
         }

         this.a = true;

         IBinder var1;
         try {
            var1 = (IBinder)this.b.poll(200L, TimeUnit.MILLISECONDS);
         } catch (InterruptedException var2) {
            var1 = null;
         }

         return var1;
      }

      public void onServiceConnected(ComponentName var1, IBinder var2) {
         try {
            this.b.put(var2);
         } catch (InterruptedException var3) {
            ;
         }

      }

      public void onServiceDisconnected(ComponentName var1) {
         this.b.clear();
      }
   }

   private static final class b implements IInterface {
      private final IBinder a;

      public b(IBinder var1) {
         this.a = var1;
      }

      public String a() throws RemoteException {
         Parcel var3 = Parcel.obtain();
         Parcel var4 = Parcel.obtain();
         String var1 = null;
         boolean var7 = false;

         String var2;
         label53: {
            try {
               var7 = true;
               var3.writeInterfaceToken("com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
               this.a.transact(1, var3, var4, 0);
               var4.readException();
               var2 = var4.readString();
               var7 = false;
               break label53;
            } catch (Exception var8) {
               io.fabric.sdk.android.c.h().a("Fabric", "Could not get parcel from Google Play Service to capture AdvertisingId");
               var7 = false;
            } finally {
               if(var7) {
                  var4.recycle();
                  var3.recycle();
               }
            }

            var4.recycle();
            var3.recycle();
            return var1;
         }

         var1 = var2;
         var4.recycle();
         var3.recycle();
         return var1;
      }

      public IBinder asBinder() {
         return this.a;
      }

      public boolean b() throws RemoteException {
         boolean var2 = true;
         Parcel var3 = Parcel.obtain();
         Parcel var4 = Parcel.obtain();
         boolean var8 = false;

         int var1;
         label63: {
            try {
               var8 = true;
               var3.writeInterfaceToken("com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
               var3.writeInt(1);
               this.a.transact(2, var3, var4, 0);
               var4.readException();
               var1 = var4.readInt();
               var8 = false;
               break label63;
            } catch (Exception var9) {
               io.fabric.sdk.android.c.h().a("Fabric", "Could not get parcel from Google Play Service to capture Advertising limitAdTracking");
               var8 = false;
            } finally {
               if(var8) {
                  var4.recycle();
                  var3.recycle();
               }
            }

            var4.recycle();
            var3.recycle();
            var2 = false;
            return var2;
         }

         if(var1 == 0) {
            var2 = false;
         }

         var4.recycle();
         var3.recycle();
         return var2;
      }
   }
}
