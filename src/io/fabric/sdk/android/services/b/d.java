package io.fabric.sdk.android.services.b;

import android.content.Context;

class d implements f {
   private final Context a;

   public d(Context var1) {
      this.a = var1.getApplicationContext();
   }

   private String b() {
      String var1;
      try {
         var1 = (String)Class.forName("com.google.android.gms.ads.identifier.AdvertisingIdClient$Info").getMethod("getId", new Class[0]).invoke(this.d(), new Object[0]);
      } catch (Exception var2) {
         io.fabric.sdk.android.c.h().d("Fabric", "Could not call getId on com.google.android.gms.ads.identifier.AdvertisingIdClient$Info");
         var1 = null;
      }

      return var1;
   }

   private boolean c() {
      boolean var1;
      try {
         var1 = ((Boolean)Class.forName("com.google.android.gms.ads.identifier.AdvertisingIdClient$Info").getMethod("isLimitAdTrackingEnabled", new Class[0]).invoke(this.d(), new Object[0])).booleanValue();
      } catch (Exception var3) {
         io.fabric.sdk.android.c.h().d("Fabric", "Could not call isLimitAdTrackingEnabled on com.google.android.gms.ads.identifier.AdvertisingIdClient$Info");
         var1 = false;
      }

      return var1;
   }

   private Object d() {
      Object var1 = null;

      Object var2;
      try {
         var2 = Class.forName("com.google.android.gms.ads.identifier.AdvertisingIdClient").getMethod("getAdvertisingIdInfo", new Class[]{Context.class}).invoke((Object)null, new Object[]{this.a});
      } catch (Exception var3) {
         io.fabric.sdk.android.c.h().d("Fabric", "Could not call getAdvertisingIdInfo on com.google.android.gms.ads.identifier.AdvertisingIdClient");
         return var1;
      }

      var1 = var2;
      return var1;
   }

   public b a() {
      b var1;
      if(this.a(this.a)) {
         var1 = new b(this.b(), this.c());
      } else {
         var1 = null;
      }

      return var1;
   }

   boolean a(Context var1) {
      int var2;
      boolean var3;
      try {
         var2 = ((Integer)Class.forName("com.google.android.gms.common.GooglePlayServicesUtil").getMethod("isGooglePlayServicesAvailable", new Class[]{Context.class}).invoke((Object)null, new Object[]{var1})).intValue();
      } catch (Exception var4) {
         var3 = false;
         return var3;
      }

      if(var2 == 0) {
         var3 = true;
      } else {
         var3 = false;
      }

      return var3;
   }
}
