package io.fabric.sdk.android.services.b;

import java.io.Closeable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.util.NoSuchElementException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class q implements Closeable {
   private static final Logger b = Logger.getLogger(q.class.getName());
   int a;
   private final RandomAccessFile c;
   private int d;
   private q.a e;
   private q.a f;
   private final byte[] g = new byte[16];

   public q(File var1) throws IOException {
      if(!var1.exists()) {
         a(var1);
      }

      this.c = b(var1);
      this.e();
   }

   private static int a(byte[] var0, int var1) {
      return ((var0[var1] & 255) << 24) + ((var0[var1 + 1] & 255) << 16) + ((var0[var1 + 2] & 255) << 8) + (var0[var1 + 3] & 255);
   }

   private q.a a(int var1) throws IOException {
      q.a var2;
      if(var1 == 0) {
         var2 = q.a.a;
      } else {
         this.c.seek((long)var1);
         var2 = new q.a(var1, this.c.readInt());
      }

      return var2;
   }

   private void a(int var1, int var2, int var3, int var4) throws IOException {
      a(this.g, new int[]{var1, var2, var3, var4});
      this.c.seek(0L);
      this.c.write(this.g);
   }

   private void a(int var1, byte[] var2, int var3, int var4) throws IOException {
      var1 = this.b(var1);
      if(var1 + var4 <= this.a) {
         this.c.seek((long)var1);
         this.c.write(var2, var3, var4);
      } else {
         int var5 = this.a - var1;
         this.c.seek((long)var1);
         this.c.write(var2, var3, var5);
         this.c.seek(16L);
         this.c.write(var2, var3 + var5, var4 - var5);
      }

   }

   private static void a(File var0) throws IOException {
      File var2 = new File(var0.getPath() + ".tmp");
      RandomAccessFile var1 = b(var2);

      try {
         var1.setLength(4096L);
         var1.seek(0L);
         byte[] var3 = new byte[16];
         a(var3, new int[]{4096, 0, 0, 0});
         var1.write(var3);
      } finally {
         var1.close();
      }

      if(!var2.renameTo(var0)) {
         throw new IOException("Rename failed!");
      }
   }

   private static void a(byte[] var0, int... var1) {
      int var3 = 0;
      int var4 = var1.length;

      for(int var2 = 0; var3 < var4; ++var3) {
         b(var0, var2, var1[var3]);
         var2 += 4;
      }

   }

   private int b(int var1) {
      if(var1 >= this.a) {
         var1 = var1 + 16 - this.a;
      }

      return var1;
   }

   private static RandomAccessFile b(File var0) throws FileNotFoundException {
      return new RandomAccessFile(var0, "rwd");
   }

   private static Object b(Object var0, String var1) {
      if(var0 == null) {
         throw new NullPointerException(var1);
      } else {
         return var0;
      }
   }

   private void b(int var1, byte[] var2, int var3, int var4) throws IOException {
      var1 = this.b(var1);
      if(var1 + var4 <= this.a) {
         this.c.seek((long)var1);
         this.c.readFully(var2, var3, var4);
      } else {
         int var5 = this.a - var1;
         this.c.seek((long)var1);
         this.c.readFully(var2, var3, var5);
         this.c.seek(16L);
         this.c.readFully(var2, var3 + var5, var4 - var5);
      }

   }

   private static void b(byte[] var0, int var1, int var2) {
      var0[var1] = (byte)(var2 >> 24);
      var0[var1 + 1] = (byte)(var2 >> 16);
      var0[var1 + 2] = (byte)(var2 >> 8);
      var0[var1 + 3] = (byte)var2;
   }

   private void c(int var1) throws IOException {
      int var5 = var1 + 4;
      var1 = this.f();
      if(var1 < var5) {
         int var2 = this.a;

         int var3;
         int var4;
         do {
            var4 = var1 + var2;
            var3 = var2 << 1;
            var2 = var3;
            var1 = var4;
         } while(var4 < var5);

         this.d(var3);
         var1 = this.b(this.f.b + 4 + this.f.c);
         if(var1 < this.e.b) {
            FileChannel var6 = this.c.getChannel();
            var6.position((long)this.a);
            var1 -= 4;
            if(var6.transferTo(16L, (long)var1, var6) != (long)var1) {
               throw new AssertionError("Copied insufficient number of bytes!");
            }
         }

         if(this.f.b < this.e.b) {
            var1 = this.a + this.f.b - 16;
            this.a(var3, this.d, this.e.b, var1);
            this.f = new q.a(var1, this.f.c);
         } else {
            this.a(var3, this.d, this.e.b, this.f.b);
         }

         this.a = var3;
      }

   }

   private void d(int var1) throws IOException {
      this.c.setLength((long)var1);
      this.c.getChannel().force(true);
   }

   private void e() throws IOException {
      this.c.seek(0L);
      this.c.readFully(this.g);
      this.a = a((byte[])this.g, 0);
      if((long)this.a > this.c.length()) {
         throw new IOException("File is truncated. Expected length: " + this.a + ", Actual length: " + this.c.length());
      } else {
         this.d = a((byte[])this.g, 4);
         int var2 = a((byte[])this.g, 8);
         int var1 = a((byte[])this.g, 12);
         this.e = this.a(var2);
         this.f = this.a(var1);
      }
   }

   private int f() {
      return this.a - this.a();
   }

   public int a() {
      int var1;
      if(this.d == 0) {
         var1 = 16;
      } else if(this.f.b >= this.e.b) {
         var1 = this.f.b - this.e.b + 4 + this.f.c + 16;
      } else {
         var1 = this.f.b + 4 + this.f.c + this.a - this.e.b;
      }

      return var1;
   }

   public void a(q.c param1) throws IOException {
      // $FF: Couldn't be decompiled
   }

   public void a(byte[] var1) throws IOException {
      this.a(var1, 0, var1.length);
   }

   public void a(byte[] param1, int param2, int param3) throws IOException {
      // $FF: Couldn't be decompiled
   }

   public boolean a(int var1, int var2) {
      boolean var3;
      if(this.a() + 4 + var1 <= var2) {
         var3 = true;
      } else {
         var3 = false;
      }

      return var3;
   }

   public boolean b() {
      synchronized(this){}
      boolean var5 = false;

      int var1;
      try {
         var5 = true;
         var1 = this.d;
         var5 = false;
      } finally {
         if(var5) {
            ;
         }
      }

      boolean var2;
      if(var1 == 0) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   public void c() throws IOException {
      synchronized(this){}

      try {
         if(this.b()) {
            NoSuchElementException var6 = new NoSuchElementException();
            throw var6;
         }

         if(this.d == 1) {
            this.d();
         } else {
            int var1 = this.b(this.e.b + 4 + this.e.c);
            this.b(var1, this.g, 0, 4);
            int var2 = a((byte[])this.g, 0);
            this.a(this.a, this.d - 1, var1, this.f.b);
            --this.d;
            q.a var3 = new q.a(var1, var2);
            this.e = var3;
         }
      } finally {
         ;
      }

   }

   public void close() throws IOException {
      synchronized(this){}

      try {
         this.c.close();
      } finally {
         ;
      }

   }

   public void d() throws IOException {
      synchronized(this){}

      try {
         this.a(4096, 0, 0, 0);
         this.d = 0;
         this.e = q.a.a;
         this.f = q.a.a;
         if(this.a > 4096) {
            this.d(4096);
         }

         this.a = 4096;
      } finally {
         ;
      }

   }

   public String toString() {
      final StringBuilder var1 = new StringBuilder();
      var1.append(this.getClass().getSimpleName()).append('[');
      var1.append("fileLength=").append(this.a);
      var1.append(", size=").append(this.d);
      var1.append(", first=").append(this.e);
      var1.append(", last=").append(this.f);
      var1.append(", element lengths=[");

      try {
         q.c var2 = new q.c() {
            boolean a = true;

            public void read(InputStream var1x, int var2) throws IOException {
               if(this.a) {
                  this.a = false;
               } else {
                  var1.append(", ");
               }

               var1.append(var2);
            }
         };
         this.a(var2);
      } catch (IOException var3) {
         b.log(Level.WARNING, "read error", var3);
      }

      var1.append("]]");
      return var1.toString();
   }

   static class a {
      static final q.a a = new q.a(0, 0);
      final int b;
      final int c;

      a(int var1, int var2) {
         this.b = var1;
         this.c = var2;
      }

      public String toString() {
         return this.getClass().getSimpleName() + "[position = " + this.b + ", length = " + this.c + "]";
      }
   }

   private final class b extends InputStream {
      private int b;
      private int c;

      private b(q.a var2) {
         this.b = q.this.b(var2.b + 4);
         this.c = var2.c;
      }

      // $FF: synthetic method
      b(q.a var2, Object var3) {
         this(var2);
      }

      public int read() throws IOException {
         int var1;
         if(this.c == 0) {
            var1 = -1;
         } else {
            q.this.c.seek((long)this.b);
            var1 = q.this.c.read();
            this.b = q.this.b(this.b + 1);
            --this.c;
         }

         return var1;
      }

      public int read(byte[] var1, int var2, int var3) throws IOException {
         q.b(var1, "buffer");
         if((var2 | var3) >= 0 && var3 <= var1.length - var2) {
            int var4;
            if(this.c > 0) {
               var4 = var3;
               if(var3 > this.c) {
                  var4 = this.c;
               }

               q.this.b(this.b, var1, var2, var4);
               this.b = q.this.b(this.b + var4);
               this.c -= var4;
            } else {
               var4 = -1;
            }

            return var4;
         } else {
            throw new ArrayIndexOutOfBoundsException();
         }
      }
   }

   public interface c {
      void read(InputStream var1, int var2) throws IOException;
   }
}
