package io.fabric.sdk.android.services.b;

import java.util.Locale;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

public final class n {
   public static ExecutorService a(String var0) {
      ExecutorService var1 = Executors.newSingleThreadExecutor(c(var0));
      a(var0, var1);
      return var1;
   }

   private static final void a(String var0, ExecutorService var1) {
      a(var0, var1, 2L, TimeUnit.SECONDS);
   }

   public static final void a(final String var0, final ExecutorService var1, final long var2, final TimeUnit var4) {
      Runtime.getRuntime().addShutdownHook(new Thread(new h() {
         public void onRun() {
            try {
               io.fabric.sdk.android.k var1x = io.fabric.sdk.android.c.h();
               StringBuilder var2x = new StringBuilder();
               var1x.a("Fabric", var2x.append("Executing shutdown hook for ").append(var0).toString());
               var1.shutdown();
               if(!var1.awaitTermination(var2, var4)) {
                  var1x = io.fabric.sdk.android.c.h();
                  var2x = new StringBuilder();
                  var1x.a("Fabric", var2x.append(var0).append(" did not shut down in the allocated time. Requesting immediate shutdown.").toString());
                  var1.shutdownNow();
               }
            } catch (InterruptedException var3) {
               io.fabric.sdk.android.c.h().a("Fabric", String.format(Locale.US, "Interrupted while waiting for %s to shut down. Requesting immediate shutdown.", new Object[]{var0}));
               var1.shutdownNow();
            }

         }
      }, "Crashlytics Shutdown Hook for " + var0));
   }

   public static ScheduledExecutorService b(String var0) {
      ScheduledExecutorService var1 = Executors.newSingleThreadScheduledExecutor(c(var0));
      a(var0, var1);
      return var1;
   }

   public static final ThreadFactory c(final String var0) {
      return new ThreadFactory(new AtomicLong(1L)) {
         // $FF: synthetic field
         final AtomicLong b;

         {
            this.b = var2;
         }

         public Thread newThread(final Runnable var1) {
            Thread var2 = Executors.defaultThreadFactory().newThread(new h() {
               public void onRun() {
                  var1.run();
               }
            });
            var2.setName(var0 + this.b.getAndIncrement());
            return var2;
         }
      };
   }
}
