package io.fabric.sdk.android.services.b;

import android.content.Context;
import android.text.TextUtils;

public class g {
   protected String a() {
      return "Fabric could not be initialized, API key missing from AndroidManifest.xml. Add the following tag to your Application element \n\t<meta-data android:name=\"io.fabric.ApiKey\" android:value=\"YOUR_API_KEY\"/>";
   }

   public String a(Context var1) {
      String var3 = this.b(var1);
      String var2 = var3;
      if(TextUtils.isEmpty(var3)) {
         var2 = this.c(var1);
      }

      if(TextUtils.isEmpty(var2)) {
         this.d(var1);
      }

      return var2;
   }

   protected String b(Context param1) {
      // $FF: Couldn't be decompiled
   }

   protected String c(Context var1) {
      String var4 = null;
      int var3 = i.a(var1, "io.fabric.ApiKey", "string");
      int var2 = var3;
      if(var3 == 0) {
         io.fabric.sdk.android.c.h().a("Fabric", "Falling back to Crashlytics key lookup from Strings");
         var2 = i.a(var1, "com.crashlytics.ApiKey", "string");
      }

      if(var2 != 0) {
         var4 = var1.getResources().getString(var2);
      }

      return var4;
   }

   protected void d(Context var1) {
      if(!io.fabric.sdk.android.c.i() && !i.i(var1)) {
         io.fabric.sdk.android.c.h().e("Fabric", this.a());
      } else {
         throw new IllegalArgumentException(this.a());
      }
   }
}
