package io.fabric.sdk.android.services.b;

public abstract class j {
   private final String a;
   private final String b;

   public j(String var1, String var2) {
      this.a = var1;
      this.b = var2;
   }

   public String a() {
      return this.a;
   }

   public String b() {
      return this.b;
   }

   public static class a extends j {
      public a(String var1, String var2) {
         super(var1, var2);
      }
   }

   public static class b extends j {
      public b(String var1, String var2) {
         super(var1, var2);
      }
   }
}
