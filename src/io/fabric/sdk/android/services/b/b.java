package io.fabric.sdk.android.services.b;

class b {
   public final String a;
   public final boolean b;

   b(String var1, boolean var2) {
      this.a = var1;
      this.b = var2;
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(this != var1) {
         if(var1 != null && this.getClass() == var1.getClass()) {
            b var3 = (b)var1;
            if(this.b != var3.b) {
               var2 = false;
            } else {
               if(this.a != null) {
                  if(this.a.equals(var3.a)) {
                     return var2;
                  }
               } else if(var3.a == null) {
                  return var2;
               }

               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public int hashCode() {
      byte var2 = 0;
      int var1;
      if(this.a != null) {
         var1 = this.a.hashCode();
      } else {
         var1 = 0;
      }

      if(this.b) {
         var2 = 1;
      }

      return var1 * 31 + var2;
   }
}
