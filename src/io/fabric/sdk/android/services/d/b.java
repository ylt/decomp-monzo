package io.fabric.sdk.android.services.d;

import android.content.Context;
import io.fabric.sdk.android.h;
import java.io.File;

public class b implements a {
   private final Context a;
   private final String b;
   private final String c;

   public b(h var1) {
      if(var1.getContext() == null) {
         throw new IllegalStateException("Cannot get directory before context has been set. Call Fabric.with() first");
      } else {
         this.a = var1.getContext();
         this.b = var1.getPath();
         this.c = "Android/" + this.a.getPackageName();
      }
   }

   public File a() {
      return this.a(this.a.getFilesDir());
   }

   File a(File var1) {
      File var2;
      if(var1 != null) {
         var2 = var1;
         if(var1.exists()) {
            return var2;
         }

         if(var1.mkdirs()) {
            var2 = var1;
            return var2;
         }

         io.fabric.sdk.android.c.h().d("Fabric", "Couldn't create file");
      } else {
         io.fabric.sdk.android.c.h().a("Fabric", "Null File");
      }

      var2 = null;
      return var2;
   }
}
