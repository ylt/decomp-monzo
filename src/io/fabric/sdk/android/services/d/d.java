package io.fabric.sdk.android.services.d;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Build.VERSION;
import io.fabric.sdk.android.h;

public class d implements c {
   private final SharedPreferences a;
   private final String b;
   private final Context c;

   public d(Context var1, String var2) {
      if(var1 == null) {
         throw new IllegalStateException("Cannot get directory before context has been set. Call Fabric.with() first");
      } else {
         this.c = var1;
         this.b = var2;
         this.a = this.c.getSharedPreferences(this.b, 0);
      }
   }

   @Deprecated
   public d(h var1) {
      this(var1.getContext(), var1.getClass().getName());
   }

   public SharedPreferences a() {
      return this.a;
   }

   @TargetApi(9)
   public boolean a(Editor var1) {
      boolean var2;
      if(VERSION.SDK_INT >= 9) {
         var1.apply();
         var2 = true;
      } else {
         var2 = var1.commit();
      }

      return var2;
   }

   public Editor b() {
      return this.a.edit();
   }
}
