package io.fabric.sdk.android.services.c;

import android.content.Context;
import io.fabric.sdk.android.services.b.k;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.TreeSet;
import java.util.concurrent.CopyOnWriteArrayList;

public abstract class b {
   public static final int MAX_BYTE_SIZE_PER_FILE = 8000;
   public static final int MAX_FILES_IN_BATCH = 1;
   public static final int MAX_FILES_TO_KEEP = 100;
   public static final String ROLL_OVER_FILE_NAME_SEPARATOR = "_";
   protected final Context context;
   protected final k currentTimeProvider;
   private final int defaultMaxFilesToKeep;
   protected final c eventStorage;
   protected volatile long lastRollOverTime;
   protected final List rollOverListeners = new CopyOnWriteArrayList();
   protected final a transform;

   public b(Context var1, a var2, k var3, c var4, int var5) throws IOException {
      this.context = var1.getApplicationContext();
      this.transform = var2;
      this.eventStorage = var4;
      this.currentTimeProvider = var3;
      this.lastRollOverTime = this.currentTimeProvider.a();
      this.defaultMaxFilesToKeep = var5;
   }

   private void rollFileOverIfNeeded(int var1) throws IOException {
      if(!this.eventStorage.a(var1, this.getMaxByteSizePerFile())) {
         String var2 = String.format(Locale.US, "session analytics events file is %d bytes, new event is %d bytes, this is over flush limit of %d, rolling it over", new Object[]{Integer.valueOf(this.eventStorage.a()), Integer.valueOf(var1), Integer.valueOf(this.getMaxByteSizePerFile())});
         io.fabric.sdk.android.services.b.i.a(this.context, 4, "Fabric", var2);
         this.rollFileOver();
      }

   }

   private void triggerRollOverOnListeners(String var1) {
      Iterator var2 = this.rollOverListeners.iterator();

      while(var2.hasNext()) {
         d var3 = (d)var2.next();

         try {
            var3.onRollOver(var1);
         } catch (Exception var4) {
            io.fabric.sdk.android.services.b.i.a((Context)this.context, (String)"One of the roll over listeners threw an exception", (Throwable)var4);
         }
      }

   }

   public void deleteAllEventsFiles() {
      this.eventStorage.a(this.eventStorage.c());
      this.eventStorage.d();
   }

   public void deleteOldestInRollOverIfOverMax() {
      List var4 = this.eventStorage.c();
      int var2 = this.getMaxFilesToKeep();
      if(var4.size() > var2) {
         int var1 = var4.size() - var2;
         io.fabric.sdk.android.services.b.i.a(this.context, String.format(Locale.US, "Found %d files in  roll over directory, this is greater than %d, deleting %d oldest files", new Object[]{Integer.valueOf(var4.size()), Integer.valueOf(var2), Integer.valueOf(var1)}));
         TreeSet var3 = new TreeSet(new Comparator() {
            public int a(b.a var1, b.a var2) {
               return (int)(var1.b - var2.b);
            }

            // $FF: synthetic method
            public int compare(Object var1, Object var2) {
               return this.a((b.a)var1, (b.a)var2);
            }
         });
         Iterator var7 = var4.iterator();

         while(var7.hasNext()) {
            File var5 = (File)var7.next();
            var3.add(new b.a(var5, this.parseCreationTimestampFromFileName(var5.getName())));
         }

         ArrayList var8 = new ArrayList();
         Iterator var6 = var3.iterator();

         while(var6.hasNext()) {
            var8.add(((b.a)var6.next()).a);
            if(var8.size() == var1) {
               break;
            }
         }

         this.eventStorage.a((List)var8);
      }

   }

   public void deleteSentFiles(List var1) {
      this.eventStorage.a(var1);
   }

   protected abstract String generateUniqueRollOverFileName();

   public List getBatchOfFilesToSend() {
      return this.eventStorage.a(1);
   }

   public long getLastRollOverTime() {
      return this.lastRollOverTime;
   }

   protected int getMaxByteSizePerFile() {
      return 8000;
   }

   protected int getMaxFilesToKeep() {
      return this.defaultMaxFilesToKeep;
   }

   public long parseCreationTimestampFromFileName(String var1) {
      long var4 = 0L;
      String[] var7 = var1.split("_");
      long var2;
      if(var7.length != 3) {
         var2 = var4;
      } else {
         try {
            var2 = Long.valueOf(var7[2]).longValue();
         } catch (NumberFormatException var6) {
            var2 = var4;
         }
      }

      return var2;
   }

   public void registerRollOverListener(d var1) {
      if(var1 != null) {
         this.rollOverListeners.add(var1);
      }

   }

   public boolean rollFileOver() throws IOException {
      boolean var1 = true;
      String var2 = null;
      if(!this.eventStorage.b()) {
         var2 = this.generateUniqueRollOverFileName();
         this.eventStorage.a(var2);
         io.fabric.sdk.android.services.b.i.a(this.context, 4, "Fabric", String.format(Locale.US, "generated new file %s", new Object[]{var2}));
         this.lastRollOverTime = this.currentTimeProvider.a();
      } else {
         var1 = false;
      }

      this.triggerRollOverOnListeners(var2);
      return var1;
   }

   public void writeEvent(Object var1) throws IOException {
      byte[] var2 = this.transform.toBytes(var1);
      this.rollFileOverIfNeeded(var2.length);
      this.eventStorage.a(var2);
   }

   static class a {
      final File a;
      final long b;

      public a(File var1, long var2) {
         this.a = var1;
         this.b = var2;
      }
   }
}
