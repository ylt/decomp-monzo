package io.fabric.sdk.android.services.c;

import android.content.Context;
import io.fabric.sdk.android.services.b.q;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class h implements c {
   private final Context a;
   private final File b;
   private final String c;
   private final File d;
   private q e;
   private File f;

   public h(Context var1, File var2, String var3, String var4) throws IOException {
      this.a = var1;
      this.b = var2;
      this.c = var4;
      this.d = new File(this.b, var3);
      this.e = new q(this.d);
      this.e();
   }

   private void a(File param1, File param2) throws IOException {
      // $FF: Couldn't be decompiled
   }

   private void e() {
      this.f = new File(this.b, this.c);
      if(!this.f.exists()) {
         this.f.mkdirs();
      }

   }

   public int a() {
      return this.e.a();
   }

   public OutputStream a(File var1) throws IOException {
      return new FileOutputStream(var1);
   }

   public List a(int var1) {
      ArrayList var4 = new ArrayList();
      File[] var5 = this.f.listFiles();
      int var3 = var5.length;

      for(int var2 = 0; var2 < var3; ++var2) {
         var4.add(var5[var2]);
         if(var4.size() >= var1) {
            break;
         }
      }

      return var4;
   }

   public void a(String var1) throws IOException {
      this.e.close();
      this.a(this.d, new File(this.f, var1));
      this.e = new q(this.d);
   }

   public void a(List var1) {
      Iterator var2 = var1.iterator();

      while(var2.hasNext()) {
         File var3 = (File)var2.next();
         io.fabric.sdk.android.services.b.i.a(this.a, String.format("deleting sent analytics file %s", new Object[]{var3.getName()}));
         var3.delete();
      }

   }

   public void a(byte[] var1) throws IOException {
      this.e.a(var1);
   }

   public boolean a(int var1, int var2) {
      return this.e.a(var1, var2);
   }

   public boolean b() {
      return this.e.b();
   }

   public List c() {
      return Arrays.asList(this.f.listFiles());
   }

   public void d() {
      try {
         this.e.close();
      } catch (IOException var2) {
         ;
      }

      this.d.delete();
   }
}
