package io.fabric.sdk.android.services.c;

import android.content.Context;

public class i implements Runnable {
   private final Context a;
   private final e b;

   public i(Context var1, e var2) {
      this.a = var1;
      this.b = var2;
   }

   public void run() {
      try {
         io.fabric.sdk.android.services.b.i.a(this.a, "Performing time based file roll over.");
         if(!this.b.rollFileOver()) {
            this.b.cancelTimeBasedFileRollOver();
         }
      } catch (Exception var2) {
         io.fabric.sdk.android.services.b.i.a((Context)this.a, (String)"Failed to roll over file", (Throwable)var2);
      }

   }
}
