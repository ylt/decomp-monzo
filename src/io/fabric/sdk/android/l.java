package io.fabric.sdk.android;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import io.fabric.sdk.android.services.e.n;
import io.fabric.sdk.android.services.e.q;
import io.fabric.sdk.android.services.e.t;
import io.fabric.sdk.android.services.e.y;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.Future;

class l extends h {
   private final io.fabric.sdk.android.services.network.d a = new io.fabric.sdk.android.services.network.b();
   private PackageManager b;
   private String c;
   private PackageInfo d;
   private String e;
   private String f;
   private String g;
   private String h;
   private String i;
   private final Future j;
   private final Collection k;

   public l(Future var1, Collection var2) {
      this.j = var1;
      this.k = var2;
   }

   private io.fabric.sdk.android.services.e.d a(n var1, Collection var2) {
      Context var5 = this.getContext();
      String var4 = (new io.fabric.sdk.android.services.b.g()).a(var5);
      String var6 = io.fabric.sdk.android.services.b.i.a(new String[]{io.fabric.sdk.android.services.b.i.m(var5)});
      int var3 = io.fabric.sdk.android.services.b.l.a(this.g).a();
      return new io.fabric.sdk.android.services.e.d(var4, this.getIdManager().c(), this.f, this.e, var6, this.h, var3, this.i, "0", var1, var2);
   }

   private boolean a(io.fabric.sdk.android.services.e.e var1, n var2, Collection var3) {
      io.fabric.sdk.android.services.e.d var4 = this.a(var2, var3);
      return (new y(this, this.b(), var1.c, this.a)).a(var4);
   }

   private boolean a(String var1, io.fabric.sdk.android.services.e.e var2, Collection var3) {
      boolean var5 = true;
      boolean var4;
      if("new".equals(var2.b)) {
         if(this.b(var1, var2, var3)) {
            var4 = q.a().d();
         } else {
            c.h().e("Fabric", "Failed to create app with Crashlytics service.", (Throwable)null);
            var4 = false;
         }
      } else if("configured".equals(var2.b)) {
         var4 = q.a().d();
      } else {
         var4 = var5;
         if(var2.e) {
            c.h().a("Fabric", "Server says an update is required - forcing a full App update.");
            this.c(var1, var2, var3);
            var4 = var5;
         }
      }

      return var4;
   }

   private boolean b(String var1, io.fabric.sdk.android.services.e.e var2, Collection var3) {
      io.fabric.sdk.android.services.e.d var4 = this.a(n.a(this.getContext(), var1), var3);
      return (new io.fabric.sdk.android.services.e.h(this, this.b(), var2.c, this.a)).a(var4);
   }

   private t c() {
      t var1;
      try {
         q.a().a(this, this.idManager, this.a, this.e, this.f, this.b()).c();
         var1 = q.a().b();
      } catch (Exception var2) {
         c.h().e("Fabric", "Error dealing with settings", var2);
         var1 = null;
      }

      return var1;
   }

   private boolean c(String var1, io.fabric.sdk.android.services.e.e var2, Collection var3) {
      return this.a(var2, n.a(this.getContext(), var1), var3);
   }

   protected Boolean a() {
      // $FF: Couldn't be decompiled
   }

   Map a(Map var1, Collection var2) {
      Iterator var4 = var2.iterator();

      while(var4.hasNext()) {
         h var3 = (h)var4.next();
         if(!var1.containsKey(var3.getIdentifier())) {
            var1.put(var3.getIdentifier(), new j(var3.getIdentifier(), var3.getVersion(), "binary"));
         }
      }

      return var1;
   }

   String b() {
      return io.fabric.sdk.android.services.b.i.b(this.getContext(), "com.crashlytics.ApiEndpoint");
   }

   // $FF: synthetic method
   protected Object doInBackground() {
      return this.a();
   }

   public String getIdentifier() {
      return "io.fabric.sdk.android:fabric";
   }

   public String getVersion() {
      return "1.3.17.dev";
   }

   protected boolean onPreExecute() {
      // $FF: Couldn't be decompiled
   }
}
