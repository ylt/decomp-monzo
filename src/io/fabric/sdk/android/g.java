package io.fabric.sdk.android;

import io.fabric.sdk.android.services.b.t;
import io.fabric.sdk.android.services.concurrency.UnmetDependencyException;

class g extends io.fabric.sdk.android.services.concurrency.f {
   final h a;

   public g(h var1) {
      this.a = var1;
   }

   private t a(String var1) {
      t var2 = new t(this.a.getIdentifier() + "." + var1, "KitInitialization");
      var2.a();
      return var2;
   }

   protected Object a(Void... var1) {
      t var2 = this.a("doInBackground");
      Object var3 = null;
      if(!this.d()) {
         var3 = this.a.doInBackground();
      }

      var2.b();
      return var3;
   }

   protected void a() {
      super.a();
      t var2 = this.a("onPreExecute");
      boolean var7 = false;

      boolean var1;
      label65: {
         try {
            var7 = true;
            var1 = this.a.onPreExecute();
            var7 = false;
            break label65;
         } catch (UnmetDependencyException var8) {
            throw var8;
         } catch (Exception var9) {
            c.h().e("Fabric", "Failure onPreExecute()", var9);
            var7 = false;
         } finally {
            if(var7) {
               var2.b();
               this.a(true);
            }
         }

         var2.b();
         this.a(true);
         return;
      }

      var2.b();
      if(!var1) {
         this.a(true);
      }

   }

   protected void a(Object var1) {
      this.a.onPostExecute(var1);
      this.a.initializationCallback.a(var1);
   }

   protected void b(Object var1) {
      this.a.onCancelled(var1);
      InitializationException var2 = new InitializationException(this.a.getIdentifier() + " Initialization was cancelled");
      this.a.initializationCallback.a((Exception)var2);
   }

   public io.fabric.sdk.android.services.concurrency.e getPriority() {
      return io.fabric.sdk.android.services.concurrency.e.c;
   }
}
