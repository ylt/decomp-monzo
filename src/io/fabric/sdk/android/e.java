package io.fabric.sdk.android;

import android.os.SystemClock;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

class e implements Callable {
   final String a;

   e(String var1) {
      this.a = var1;
   }

   private j a(ZipEntry param1, ZipFile param2) {
      // $FF: Couldn't be decompiled
   }

   private Map c() {
      HashMap var1 = new HashMap();

      try {
         Class.forName("com.google.android.gms.ads.AdView");
         j var2 = new j("com.google.firebase.firebase-ads", "0.0.0", "binary");
         var1.put(var2.a(), var2);
         c.h().b("Fabric", "Found kit: com.google.firebase.firebase-ads");
      } catch (Exception var3) {
         ;
      }

      return var1;
   }

   private Map d() throws Exception {
      HashMap var1 = new HashMap();
      ZipFile var3 = this.b();
      Enumeration var2 = var3.entries();

      while(var2.hasMoreElements()) {
         ZipEntry var4 = (ZipEntry)var2.nextElement();
         if(var4.getName().startsWith("fabric/") && var4.getName().length() > "fabric/".length()) {
            j var6 = this.a(var4, var3);
            if(var6 != null) {
               var1.put(var6.a(), var6);
               c.h().b("Fabric", String.format("Found kit:[%s] version:[%s]", new Object[]{var6.a(), var6.b()}));
            }
         }
      }

      if(var3 != null) {
         try {
            var3.close();
         } catch (IOException var5) {
            ;
         }
      }

      return var1;
   }

   public Map a() throws Exception {
      HashMap var3 = new HashMap();
      long var1 = SystemClock.elapsedRealtime();
      var3.putAll(this.c());
      var3.putAll(this.d());
      c.h().b("Fabric", "finish scanning in " + (SystemClock.elapsedRealtime() - var1));
      return var3;
   }

   protected ZipFile b() throws IOException {
      return new ZipFile(this.a);
   }

   // $FF: synthetic method
   public Object call() throws Exception {
      return this.a();
   }
}
