package io.fabric.sdk.android;

public class InitializationException extends RuntimeException {
   public InitializationException(String var1) {
      super(var1);
   }
}
