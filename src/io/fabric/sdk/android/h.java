package io.fabric.sdk.android;

import android.content.Context;
import io.fabric.sdk.android.services.b.o;
import java.io.File;
import java.util.Collection;

public abstract class h implements Comparable {
   Context context;
   final io.fabric.sdk.android.services.concurrency.d dependsOnAnnotation = (io.fabric.sdk.android.services.concurrency.d)this.getClass().getAnnotation(io.fabric.sdk.android.services.concurrency.d.class);
   c fabric;
   o idManager;
   f initializationCallback;
   g initializationTask = new g(this);

   public int compareTo(h var1) {
      byte var2 = 1;
      if(!this.containsAnnotatedDependency(var1)) {
         if(var1.containsAnnotatedDependency(this)) {
            var2 = -1;
         } else if(!this.hasAnnotatedDependency() || var1.hasAnnotatedDependency()) {
            if(!this.hasAnnotatedDependency() && var1.hasAnnotatedDependency()) {
               var2 = -1;
            } else {
               var2 = 0;
            }
         }
      }

      return var2;
   }

   boolean containsAnnotatedDependency(h var1) {
      boolean var5 = false;
      boolean var4 = var5;
      if(this.hasAnnotatedDependency()) {
         Class[] var6 = this.dependsOnAnnotation.a();
         int var3 = var6.length;
         int var2 = 0;

         while(true) {
            var4 = var5;
            if(var2 >= var3) {
               break;
            }

            if(var6[var2].isAssignableFrom(var1.getClass())) {
               var4 = true;
               break;
            }

            ++var2;
         }
      }

      return var4;
   }

   protected abstract Object doInBackground();

   public Context getContext() {
      return this.context;
   }

   protected Collection getDependencies() {
      return this.initializationTask.getDependencies();
   }

   public c getFabric() {
      return this.fabric;
   }

   protected o getIdManager() {
      return this.idManager;
   }

   public abstract String getIdentifier();

   public String getPath() {
      return ".Fabric" + File.separator + this.getIdentifier();
   }

   public abstract String getVersion();

   boolean hasAnnotatedDependency() {
      boolean var1;
      if(this.dependsOnAnnotation != null) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   final void initialize() {
      this.initializationTask.a(this.fabric.f(), new Void[]{(Void)null});
   }

   void injectParameters(Context var1, c var2, f var3, o var4) {
      this.fabric = var2;
      this.context = new d(var1, this.getIdentifier(), this.getPath());
      this.initializationCallback = var3;
      this.idManager = var4;
   }

   protected void onCancelled(Object var1) {
   }

   protected void onPostExecute(Object var1) {
   }

   protected boolean onPreExecute() {
      return true;
   }
}
