package io.fabric.sdk.android;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.app.Application.ActivityLifecycleCallbacks;
import android.content.Context;
import android.os.Bundle;
import android.os.Build.VERSION;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class a {
   private final Application a;
   private a.a b;

   public a(Context var1) {
      this.a = (Application)var1.getApplicationContext();
      if(VERSION.SDK_INT >= 14) {
         this.b = new a.a(this.a);
      }

   }

   public void a() {
      if(this.b != null) {
         this.b.a();
      }

   }

   public boolean a(a.b var1) {
      boolean var2;
      if(this.b != null && this.b.a(var1)) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   private static class a {
      private final Set a = new HashSet();
      private final Application b;

      a(Application var1) {
         this.b = var1;
      }

      @TargetApi(14)
      private void a() {
         Iterator var1 = this.a.iterator();

         while(var1.hasNext()) {
            ActivityLifecycleCallbacks var2 = (ActivityLifecycleCallbacks)var1.next();
            this.b.unregisterActivityLifecycleCallbacks(var2);
         }

      }

      @TargetApi(14)
      private boolean a(final a.b var1) {
         boolean var2;
         if(this.b != null) {
            ActivityLifecycleCallbacks var3 = new ActivityLifecycleCallbacks() {
               public void onActivityCreated(Activity var1x, Bundle var2) {
                  var1.onActivityCreated(var1x, var2);
               }

               public void onActivityDestroyed(Activity var1x) {
                  var1.onActivityDestroyed(var1x);
               }

               public void onActivityPaused(Activity var1x) {
                  var1.onActivityPaused(var1x);
               }

               public void onActivityResumed(Activity var1x) {
                  var1.onActivityResumed(var1x);
               }

               public void onActivitySaveInstanceState(Activity var1x, Bundle var2) {
                  var1.onActivitySaveInstanceState(var1x, var2);
               }

               public void onActivityStarted(Activity var1x) {
                  var1.onActivityStarted(var1x);
               }

               public void onActivityStopped(Activity var1x) {
                  var1.onActivityStopped(var1x);
               }
            };
            this.b.registerActivityLifecycleCallbacks(var3);
            this.a.add(var3);
            var2 = true;
         } else {
            var2 = false;
         }

         return var2;
      }
   }

   public abstract static class b {
      public void onActivityCreated(Activity var1, Bundle var2) {
      }

      public void onActivityDestroyed(Activity var1) {
      }

      public void onActivityPaused(Activity var1) {
      }

      public void onActivityResumed(Activity var1) {
      }

      public void onActivitySaveInstanceState(Activity var1, Bundle var2) {
      }

      public void onActivityStarted(Activity var1) {
      }

      public void onActivityStopped(Activity var1) {
      }
   }
}
