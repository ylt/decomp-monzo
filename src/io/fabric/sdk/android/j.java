package io.fabric.sdk.android;

public class j {
   private final String a;
   private final String b;
   private final String c;

   public j(String var1, String var2, String var3) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
   }

   public String a() {
      return this.a;
   }

   public String b() {
      return this.b;
   }

   public String c() {
      return this.c;
   }
}
