package io.fabric.sdk.android;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import io.fabric.sdk.android.services.b.o;
import io.fabric.sdk.android.services.concurrency.UnmetDependencyException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicBoolean;

public class c {
   static volatile c a;
   static final k b = new b();
   final k c;
   final boolean d;
   private final Context e;
   private final Map f;
   private final ExecutorService g;
   private final Handler h;
   private final f i;
   private final f j;
   private final o k;
   private a l;
   private WeakReference m;
   private AtomicBoolean n;

   c(Context var1, Map var2, io.fabric.sdk.android.services.concurrency.k var3, Handler var4, k var5, boolean var6, f var7, o var8, Activity var9) {
      this.e = var1;
      this.f = var2;
      this.g = var3;
      this.h = var4;
      this.c = var5;
      this.d = var6;
      this.i = var7;
      this.n = new AtomicBoolean(false);
      this.j = this.a(var2.size());
      this.k = var8;
      this.a(var9);
   }

   static c a() {
      if(a == null) {
         throw new IllegalStateException("Must Initialize Fabric before using singleton()");
      } else {
         return a;
      }
   }

   public static c a(Context param0, h... param1) {
      // $FF: Couldn't be decompiled
   }

   public static h a(Class var0) {
      return (h)a().f.get(var0);
   }

   private static void a(Map var0, Collection var1) {
      Iterator var2 = var1.iterator();

      while(var2.hasNext()) {
         h var3 = (h)var2.next();
         var0.put(var3.getClass(), var3);
         if(var3 instanceof i) {
            a(var0, ((i)var3).getKits());
         }
      }

   }

   private static Map b(Collection var0) {
      HashMap var1 = new HashMap(var0.size());
      a((Map)var1, (Collection)var0);
      return var1;
   }

   private static void c(c var0) {
      a = var0;
      var0.j();
   }

   private static Activity d(Context var0) {
      Activity var1;
      if(var0 instanceof Activity) {
         var1 = (Activity)var0;
      } else {
         var1 = null;
      }

      return var1;
   }

   public static k h() {
      k var0;
      if(a == null) {
         var0 = b;
      } else {
         var0 = a.c;
      }

      return var0;
   }

   public static boolean i() {
      boolean var0;
      if(a == null) {
         var0 = false;
      } else {
         var0 = a.d;
      }

      return var0;
   }

   private void j() {
      this.l = new a(this.e);
      this.l.a(new a.b() {
         public void onActivityCreated(Activity var1, Bundle var2) {
            c.this.a(var1);
         }

         public void onActivityResumed(Activity var1) {
            c.this.a(var1);
         }

         public void onActivityStarted(Activity var1) {
            c.this.a(var1);
         }
      });
      this.a(this.e);
   }

   public c a(Activity var1) {
      this.m = new WeakReference(var1);
      return this;
   }

   f a(final int var1) {
      return new f() {
         final CountDownLatch a = new CountDownLatch(var1);

         public void a(Exception var1x) {
            c.this.i.a(var1x);
         }

         public void a(Object var1x) {
            this.a.countDown();
            if(this.a.getCount() == 0L) {
               c.this.n.set(true);
               c.this.i.a((Object)c.this);
            }

         }
      };
   }

   void a(Context var1) {
      Future var2 = this.b(var1);
      Collection var3 = this.g();
      l var6 = new l(var2, var3);
      ArrayList var7 = new ArrayList(var3);
      Collections.sort(var7);
      var6.injectParameters(var1, this, f.d, this.k);
      Iterator var4 = var7.iterator();

      while(var4.hasNext()) {
         ((h)var4.next()).injectParameters(var1, this, this.j, this.k);
      }

      var6.initialize();
      StringBuilder var5;
      if(h().a("Fabric", 3)) {
         var5 = (new StringBuilder("Initializing ")).append(this.d()).append(" [Version: ").append(this.c()).append("], with the following kits:\n");
      } else {
         var5 = null;
      }

      Iterator var8 = var7.iterator();

      while(var8.hasNext()) {
         h var9 = (h)var8.next();
         var9.initializationTask.a((io.fabric.sdk.android.services.concurrency.l)var6.initializationTask);
         this.a(this.f, var9);
         var9.initialize();
         if(var5 != null) {
            var5.append(var9.getIdentifier()).append(" [Version: ").append(var9.getVersion()).append("]\n");
         }
      }

      if(var5 != null) {
         h().a("Fabric", var5.toString());
      }

   }

   void a(Map var1, h var2) {
      io.fabric.sdk.android.services.concurrency.d var5 = var2.dependsOnAnnotation;
      if(var5 != null) {
         Class[] var7 = var5.a();
         int var4 = var7.length;

         for(int var3 = 0; var3 < var4; ++var3) {
            Class var8 = var7[var3];
            if(var8.isInterface()) {
               Iterator var6 = var1.values().iterator();

               while(var6.hasNext()) {
                  h var9 = (h)var6.next();
                  if(var8.isAssignableFrom(var9.getClass())) {
                     var2.initializationTask.a((io.fabric.sdk.android.services.concurrency.l)var9.initializationTask);
                  }
               }
            } else {
               if((h)var1.get(var8) == null) {
                  throw new UnmetDependencyException("Referenced Kit was null, does the kit exist?");
               }

               var2.initializationTask.a((io.fabric.sdk.android.services.concurrency.l)((h)var1.get(var8)).initializationTask);
            }
         }
      }

   }

   public Activity b() {
      Activity var1;
      if(this.m != null) {
         var1 = (Activity)this.m.get();
      } else {
         var1 = null;
      }

      return var1;
   }

   Future b(Context var1) {
      e var2 = new e(var1.getPackageCodePath());
      return this.f().submit(var2);
   }

   public String c() {
      return "1.3.17.dev";
   }

   public String d() {
      return "io.fabric.sdk.android:fabric";
   }

   public a e() {
      return this.l;
   }

   public ExecutorService f() {
      return this.g;
   }

   public Collection g() {
      return this.f.values();
   }

   public static class a {
      private final Context a;
      private h[] b;
      private io.fabric.sdk.android.services.concurrency.k c;
      private Handler d;
      private k e;
      private boolean f;
      private String g;
      private String h;
      private f i;

      public a(Context var1) {
         if(var1 == null) {
            throw new IllegalArgumentException("Context must not be null.");
         } else {
            this.a = var1;
         }
      }

      public c.a a(h... var1) {
         if(this.b != null) {
            throw new IllegalStateException("Kits already set.");
         } else {
            this.b = var1;
            return this;
         }
      }

      public c a() {
         if(this.c == null) {
            this.c = io.fabric.sdk.android.services.concurrency.k.a();
         }

         if(this.d == null) {
            this.d = new Handler(Looper.getMainLooper());
         }

         if(this.e == null) {
            if(this.f) {
               this.e = new b(3);
            } else {
               this.e = new b();
            }
         }

         if(this.h == null) {
            this.h = this.a.getPackageName();
         }

         if(this.i == null) {
            this.i = f.d;
         }

         Object var1;
         if(this.b == null) {
            var1 = new HashMap();
         } else {
            var1 = c.b((Collection)Arrays.asList(this.b));
         }

         Context var3 = this.a.getApplicationContext();
         o var2 = new o(var3, this.h, this.g, ((Map)var1).values());
         return new c(var3, (Map)var1, this.c, this.d, this.e, this.f, this.i, var2, c.d(this.a));
      }
   }
}
