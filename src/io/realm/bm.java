package io.realm;

import io.realm.exceptions.RealmException;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.LinkView;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.SharedRealm;
import io.realm.internal.Table;
import io.realm.log.RealmLog;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class bm extends co.uk.getmondo.d.aj implements bn, io.realm.internal.l {
   private static final OsObjectSchemaInfo c = aj();
   private static final List e;
   private bm.a a;
   private au b;
   private az d;

   static {
      ArrayList var0 = new ArrayList();
      var0.add("id");
      var0.add("amountValue");
      var0.add("amountCurrency");
      var0.add("amountLocalValue");
      var0.add("amountLocalCurrency");
      var0.add("created");
      var0.add("createdDateFormatted");
      var0.add("updated");
      var0.add("merchantDescription");
      var0.add("description");
      var0.add("declineReason");
      var0.add("notes");
      var0.add("hideAmount");
      var0.add("settled");
      var0.add("merchant");
      var0.add("category");
      var0.add("fromAtm");
      var0.add("peerToPeer");
      var0.add("topUp");
      var0.add("includeInSpending");
      var0.add("attachments");
      var0.add("peer");
      var0.add("bankDetails");
      var0.add("fromMonzoMe");
      var0.add("scheme");
      var0.add("bacsDirectDebitInstructionId");
      e = Collections.unmodifiableList(var0);
   }

   bm() {
      this.b.f();
   }

   public static long a(av var0, co.uk.getmondo.d.aj var1, Map var2) {
      long var5;
      if(var1 instanceof io.realm.internal.l && ((io.realm.internal.l)var1).s_().a() != null && ((io.realm.internal.l)var1).s_().a().g().equals(var0.g())) {
         var5 = ((io.realm.internal.l)var1).s_().b().c();
      } else {
         Table var9 = var0.c(co.uk.getmondo.d.aj.class);
         long var7 = var9.getNativePtr();
         bm.a var11 = (bm.a)var0.f.c(co.uk.getmondo.d.aj.class);
         long var3 = var9.d();
         String var10 = ((bn)var1).H();
         if(var10 == null) {
            var3 = Table.nativeFindFirstNull(var7, var3);
         } else {
            var3 = Table.nativeFindFirstString(var7, var3, var10);
         }

         if(var3 == -1L) {
            var3 = OsObject.b(var9, var10);
         } else {
            Table.a((Object)var10);
         }

         var2.put(var1, Long.valueOf(var3));
         Table.nativeSetLong(var7, var11.b, var3, ((bn)var1).I(), false);
         String var16 = ((bn)var1).J();
         if(var16 != null) {
            Table.nativeSetString(var7, var11.c, var3, var16, false);
         }

         Table.nativeSetLong(var7, var11.d, var3, ((bn)var1).K(), false);
         var16 = ((bn)var1).L();
         if(var16 != null) {
            Table.nativeSetString(var7, var11.e, var3, var16, false);
         }

         Date var17 = ((bn)var1).M();
         if(var17 != null) {
            Table.nativeSetTimestamp(var7, var11.f, var3, var17.getTime(), false);
         }

         var16 = ((bn)var1).N();
         if(var16 != null) {
            Table.nativeSetString(var7, var11.g, var3, var16, false);
         }

         var17 = ((bn)var1).O();
         if(var17 != null) {
            Table.nativeSetTimestamp(var7, var11.h, var3, var17.getTime(), false);
         }

         var16 = ((bn)var1).P();
         if(var16 != null) {
            Table.nativeSetString(var7, var11.i, var3, var16, false);
         }

         var16 = ((bn)var1).Q();
         if(var16 != null) {
            Table.nativeSetString(var7, var11.j, var3, var16, false);
         }

         var16 = ((bn)var1).R();
         if(var16 != null) {
            Table.nativeSetString(var7, var11.k, var3, var16, false);
         }

         var16 = ((bn)var1).S();
         if(var16 != null) {
            Table.nativeSetString(var7, var11.l, var3, var16, false);
         }

         Table.nativeSetBoolean(var7, var11.m, var3, ((bn)var1).T(), false);
         Table.nativeSetBoolean(var7, var11.n, var3, ((bn)var1).U(), false);
         co.uk.getmondo.d.u var18 = ((bn)var1).V();
         Long var22;
         if(var18 != null) {
            var22 = (Long)var2.get(var18);
            if(var22 == null) {
               var22 = Long.valueOf(ag.a(var0, var18, var2));
            }

            Table.nativeSetLink(var7, var11.o, var3, var22.longValue(), false);
         }

         var16 = ((bn)var1).W();
         if(var16 != null) {
            Table.nativeSetString(var7, var11.p, var3, var16, false);
         }

         Table.nativeSetBoolean(var7, var11.q, var3, ((bn)var1).X(), false);
         Table.nativeSetBoolean(var7, var11.r, var3, ((bn)var1).Y(), false);
         Table.nativeSetBoolean(var7, var11.s, var3, ((bn)var1).Z(), false);
         Table.nativeSetBoolean(var7, var11.t, var3, ((bn)var1).aa(), false);
         az var23 = ((bn)var1).ab();
         if(var23 != null) {
            var5 = Table.nativeGetLinkView(var7, var11.u, var3);

            for(Iterator var12 = var23.iterator(); var12.hasNext(); LinkView.nativeAdd(var5, var22.longValue())) {
               co.uk.getmondo.d.d var13 = (co.uk.getmondo.d.d)var12.next();
               Long var19 = (Long)var2.get(var13);
               var22 = var19;
               if(var19 == null) {
                  var22 = Long.valueOf(c.a(var0, var13, var2));
               }
            }
         }

         co.uk.getmondo.d.aa var20 = ((bn)var1).ac();
         if(var20 != null) {
            var22 = (Long)var2.get(var20);
            if(var22 == null) {
               var22 = Long.valueOf(as.a(var0, var20, var2));
            }

            Table.nativeSetLink(var7, var11.v, var3, var22.longValue(), false);
         }

         co.uk.getmondo.payments.send.data.a.a var21 = ((bn)var1).ad();
         if(var21 != null) {
            var22 = (Long)var2.get(var21);
            Long var14;
            if(var22 == null) {
               var14 = Long.valueOf(e.a(var0, var21, var2));
            } else {
               var14 = var22;
            }

            Table.nativeSetLink(var7, var11.w, var3, var14.longValue(), false);
         }

         Table.nativeSetBoolean(var7, var11.x, var3, ((bn)var1).ae(), false);
         String var15 = ((bn)var1).af();
         if(var15 != null) {
            Table.nativeSetString(var7, var11.y, var3, var15, false);
         }

         var15 = ((bn)var1).ag();
         var5 = var3;
         if(var15 != null) {
            Table.nativeSetString(var7, var11.z, var3, var15, false);
            var5 = var3;
         }
      }

      return var5;
   }

   public static co.uk.getmondo.d.aj a(co.uk.getmondo.d.aj var0, int var1, int var2, Map var3) {
      if(var1 <= var2 && var0 != null) {
         io.realm.internal.l.a var7 = (io.realm.internal.l.a)var3.get(var0);
         co.uk.getmondo.d.aj var6;
         if(var7 == null) {
            var6 = new co.uk.getmondo.d.aj();
            var3.put(var0, new io.realm.internal.l.a(var1, var6));
         } else {
            if(var1 >= var7.a) {
               var0 = (co.uk.getmondo.d.aj)var7.b;
               return var0;
            }

            var6 = (co.uk.getmondo.d.aj)var7.b;
            var7.a = var1;
         }

         bn var11 = (bn)var6;
         bn var8 = (bn)var0;
         var11.c(var8.H());
         var11.a(var8.I());
         var11.d(var8.J());
         var11.b(var8.K());
         var11.e(var8.L());
         var11.a(var8.M());
         var11.f(var8.N());
         var11.b(var8.O());
         var11.g(var8.P());
         var11.h(var8.Q());
         var11.i(var8.R());
         var11.j(var8.S());
         var11.a(var8.T());
         var11.b(var8.U());
         var11.a(ag.a(var8.V(), var1 + 1, var2, var3));
         var11.k(var8.W());
         var11.c(var8.X());
         var11.d(var8.Y());
         var11.e(var8.Z());
         var11.f(var8.aa());
         if(var1 == var2) {
            var11.a((az)null);
         } else {
            az var9 = var8.ab();
            az var10 = new az();
            var11.a(var10);
            int var5 = var9.size();

            for(int var4 = 0; var4 < var5; ++var4) {
               var10.a((bb)c.a((co.uk.getmondo.d.d)var9.b(var4), var1 + 1, var2, var3));
            }
         }

         var11.a(as.a(var8.ac(), var1 + 1, var2, var3));
         var11.a(e.a(var8.ad(), var1 + 1, var2, var3));
         var11.g(var8.ae());
         var11.l(var8.af());
         var11.m(var8.ag());
         var0 = var6;
      } else {
         var0 = null;
      }

      return var0;
   }

   static co.uk.getmondo.d.aj a(av var0, co.uk.getmondo.d.aj var1, co.uk.getmondo.d.aj var2, Map var3) {
      bn var5 = (bn)var1;
      bn var10 = (bn)var2;
      var5.a(var10.I());
      var5.d(var10.J());
      var5.b(var10.K());
      var5.e(var10.L());
      var5.a(var10.M());
      var5.f(var10.N());
      var5.b(var10.O());
      var5.g(var10.P());
      var5.h(var10.Q());
      var5.i(var10.R());
      var5.j(var10.S());
      var5.a(var10.T());
      var5.b(var10.U());
      co.uk.getmondo.d.u var7 = var10.V();
      if(var7 == null) {
         var5.a((co.uk.getmondo.d.u)null);
      } else {
         co.uk.getmondo.d.u var6 = (co.uk.getmondo.d.u)var3.get(var7);
         if(var6 != null) {
            var5.a(var6);
         } else {
            var5.a(ag.a(var0, var7, true, var3));
         }
      }

      var5.k(var10.W());
      var5.c(var10.X());
      var5.d(var10.Y());
      var5.e(var10.Z());
      var5.f(var10.aa());
      az var11 = var10.ab();
      az var9 = var5.ab();
      var9.clear();
      if(var11 != null) {
         for(int var4 = 0; var4 < var11.size(); ++var4) {
            co.uk.getmondo.d.d var8 = (co.uk.getmondo.d.d)var11.b(var4);
            co.uk.getmondo.d.d var14 = (co.uk.getmondo.d.d)var3.get(var8);
            if(var14 != null) {
               var9.a((bb)var14);
            } else {
               var9.a((bb)c.a(var0, var8, true, var3));
            }
         }
      }

      co.uk.getmondo.d.aa var15 = var10.ac();
      if(var15 == null) {
         var5.a((co.uk.getmondo.d.aa)null);
      } else {
         co.uk.getmondo.d.aa var12 = (co.uk.getmondo.d.aa)var3.get(var15);
         if(var12 != null) {
            var5.a(var12);
         } else {
            var5.a(as.a(var0, var15, true, var3));
         }
      }

      co.uk.getmondo.payments.send.data.a.a var16 = var10.ad();
      if(var16 == null) {
         var5.a((co.uk.getmondo.payments.send.data.a.a)null);
      } else {
         co.uk.getmondo.payments.send.data.a.a var13 = (co.uk.getmondo.payments.send.data.a.a)var3.get(var16);
         if(var13 != null) {
            var5.a(var13);
         } else {
            var5.a(e.a(var0, var16, true, var3));
         }
      }

      var5.g(var10.ae());
      var5.l(var10.af());
      var5.m(var10.ag());
      return var1;
   }

   public static co.uk.getmondo.d.aj a(av var0, co.uk.getmondo.d.aj var1, boolean var2, Map var3) {
      if(var1 instanceof io.realm.internal.l && ((io.realm.internal.l)var1).s_().a() != null && ((io.realm.internal.l)var1).s_().a().c != var0.c) {
         throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
      } else {
         if(!(var1 instanceof io.realm.internal.l) || ((io.realm.internal.l)var1).s_().a() == null || !((io.realm.internal.l)var1).s_().a().g().equals(var0.g())) {
            g.b var8 = (g.b)g.g.get();
            io.realm.internal.l var7 = (io.realm.internal.l)var3.get(var1);
            if(var7 != null) {
               var1 = (co.uk.getmondo.d.aj)var7;
            } else {
               boolean var6;
               bm var13;
               if(var2) {
                  Table var12 = var0.c(co.uk.getmondo.d.aj.class);
                  long var4 = var12.d();
                  String var9 = ((bn)var1).H();
                  if(var9 == null) {
                     var4 = var12.k(var4);
                  } else {
                     var4 = var12.a(var4, var9);
                  }

                  if(var4 != -1L) {
                     try {
                        var8.a(var0, var12.f(var4), var0.f.c(co.uk.getmondo.d.aj.class), false, Collections.emptyList());
                        var13 = new bm();
                        var3.put(var1, (io.realm.internal.l)var13);
                     } finally {
                        var8.f();
                     }

                     var6 = var2;
                  } else {
                     var6 = false;
                     var13 = null;
                  }
               } else {
                  var6 = var2;
                  var13 = null;
               }

               if(var6) {
                  var1 = a(var0, var13, var1, var3);
               } else {
                  var1 = b(var0, var1, var2, var3);
               }
            }
         }

         return var1;
      }
   }

   public static bm.a a(SharedRealm var0, boolean var1) {
      if(!var0.a("class_Transaction")) {
         throw new RealmMigrationNeededException(var0.h(), "The 'Transaction' class is missing from the schema for this Realm.");
      } else {
         Table var8 = var0.b("class_Transaction");
         long var4 = var8.c();
         if(var4 != 26L) {
            if(var4 < 26L) {
               throw new RealmMigrationNeededException(var0.h(), "Field count is less than expected - expected 26 but was " + var4);
            }

            if(!var1) {
               throw new RealmMigrationNeededException(var0.h(), "Field count is more than expected - expected 26 but was " + var4);
            }

            RealmLog.a("Field count is more than expected - expected 26 but was %1$d", new Object[]{Long.valueOf(var4)});
         }

         HashMap var6 = new HashMap();

         for(long var2 = 0L; var2 < var4; ++var2) {
            var6.put(var8.b(var2), var8.c(var2));
         }

         bm.a var7 = new bm.a(var0, var8);
         if(!var8.e()) {
            throw new RealmMigrationNeededException(var0.h(), "Primary key not defined for field 'id' in existing Realm file. @PrimaryKey was added.");
         } else if(var8.d() != var7.a) {
            throw new RealmMigrationNeededException(var0.h(), "Primary Key annotation definition was changed, from field " + var8.b(var8.d()) + " to field id");
         } else if(!var6.containsKey("id")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'id' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var6.get("id") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'id' in existing Realm file.");
         } else if(!var8.a(var7.a)) {
            throw new RealmMigrationNeededException(var0.h(), "@PrimaryKey field 'id' does not support null values in the existing Realm file. Migrate using RealmObjectSchema.setNullable(), or mark the field as @Required.");
         } else if(!var8.j(var8.a("id"))) {
            throw new RealmMigrationNeededException(var0.h(), "Index not defined for field 'id' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex().");
         } else if(!var6.containsKey("amountValue")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'amountValue' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var6.get("amountValue") != RealmFieldType.INTEGER) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'long' for field 'amountValue' in existing Realm file.");
         } else if(var8.a(var7.b)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'amountValue' does support null values in the existing Realm file. Use corresponding boxed type for field 'amountValue' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var6.containsKey("amountCurrency")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'amountCurrency' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var6.get("amountCurrency") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'amountCurrency' in existing Realm file.");
         } else if(!var8.a(var7.c)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'amountCurrency' is required. Either set @Required to field 'amountCurrency' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var6.containsKey("amountLocalValue")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'amountLocalValue' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var6.get("amountLocalValue") != RealmFieldType.INTEGER) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'long' for field 'amountLocalValue' in existing Realm file.");
         } else if(var8.a(var7.d)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'amountLocalValue' does support null values in the existing Realm file. Use corresponding boxed type for field 'amountLocalValue' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var6.containsKey("amountLocalCurrency")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'amountLocalCurrency' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var6.get("amountLocalCurrency") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'amountLocalCurrency' in existing Realm file.");
         } else if(!var8.a(var7.e)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'amountLocalCurrency' is required. Either set @Required to field 'amountLocalCurrency' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var6.containsKey("created")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'created' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var6.get("created") != RealmFieldType.DATE) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'Date' for field 'created' in existing Realm file.");
         } else if(!var8.a(var7.f)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'created' is required. Either set @Required to field 'created' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var6.containsKey("createdDateFormatted")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'createdDateFormatted' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var6.get("createdDateFormatted") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'createdDateFormatted' in existing Realm file.");
         } else if(!var8.a(var7.g)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'createdDateFormatted' is required. Either set @Required to field 'createdDateFormatted' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var6.containsKey("updated")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'updated' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var6.get("updated") != RealmFieldType.DATE) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'Date' for field 'updated' in existing Realm file.");
         } else if(!var8.a(var7.h)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'updated' is required. Either set @Required to field 'updated' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var6.containsKey("merchantDescription")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'merchantDescription' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var6.get("merchantDescription") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'merchantDescription' in existing Realm file.");
         } else if(!var8.a(var7.i)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'merchantDescription' is required. Either set @Required to field 'merchantDescription' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var6.containsKey("description")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'description' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var6.get("description") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'description' in existing Realm file.");
         } else if(!var8.a(var7.j)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'description' is required. Either set @Required to field 'description' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var6.containsKey("declineReason")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'declineReason' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var6.get("declineReason") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'declineReason' in existing Realm file.");
         } else if(!var8.a(var7.k)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'declineReason' is required. Either set @Required to field 'declineReason' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var6.containsKey("notes")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'notes' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var6.get("notes") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'notes' in existing Realm file.");
         } else if(!var8.a(var7.l)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'notes' is required. Either set @Required to field 'notes' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var6.containsKey("hideAmount")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'hideAmount' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var6.get("hideAmount") != RealmFieldType.BOOLEAN) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'boolean' for field 'hideAmount' in existing Realm file.");
         } else if(var8.a(var7.m)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'hideAmount' does support null values in the existing Realm file. Use corresponding boxed type for field 'hideAmount' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var6.containsKey("settled")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'settled' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var6.get("settled") != RealmFieldType.BOOLEAN) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'boolean' for field 'settled' in existing Realm file.");
         } else if(var8.a(var7.n)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'settled' does support null values in the existing Realm file. Use corresponding boxed type for field 'settled' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var6.containsKey("merchant")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'merchant' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var6.get("merchant") != RealmFieldType.OBJECT) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'Merchant' for field 'merchant'");
         } else if(!var0.a("class_Merchant")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing class 'class_Merchant' for field 'merchant'");
         } else {
            Table var9 = var0.b("class_Merchant");
            if(!var8.e(var7.o).a(var9)) {
               throw new RealmMigrationNeededException(var0.h(), "Invalid RealmObject for field 'merchant': '" + var8.e(var7.o).j() + "' expected - was '" + var9.j() + "'");
            } else if(!var6.containsKey("category")) {
               throw new RealmMigrationNeededException(var0.h(), "Missing field 'category' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            } else if(var6.get("category") != RealmFieldType.STRING) {
               throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'category' in existing Realm file.");
            } else if(!var8.a(var7.p)) {
               throw new RealmMigrationNeededException(var0.h(), "Field 'category' is required. Either set @Required to field 'category' or migrate using RealmObjectSchema.setNullable().");
            } else if(!var6.containsKey("fromAtm")) {
               throw new RealmMigrationNeededException(var0.h(), "Missing field 'fromAtm' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            } else if(var6.get("fromAtm") != RealmFieldType.BOOLEAN) {
               throw new RealmMigrationNeededException(var0.h(), "Invalid type 'boolean' for field 'fromAtm' in existing Realm file.");
            } else if(var8.a(var7.q)) {
               throw new RealmMigrationNeededException(var0.h(), "Field 'fromAtm' does support null values in the existing Realm file. Use corresponding boxed type for field 'fromAtm' or migrate using RealmObjectSchema.setNullable().");
            } else if(!var6.containsKey("peerToPeer")) {
               throw new RealmMigrationNeededException(var0.h(), "Missing field 'peerToPeer' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            } else if(var6.get("peerToPeer") != RealmFieldType.BOOLEAN) {
               throw new RealmMigrationNeededException(var0.h(), "Invalid type 'boolean' for field 'peerToPeer' in existing Realm file.");
            } else if(var8.a(var7.r)) {
               throw new RealmMigrationNeededException(var0.h(), "Field 'peerToPeer' does support null values in the existing Realm file. Use corresponding boxed type for field 'peerToPeer' or migrate using RealmObjectSchema.setNullable().");
            } else if(!var6.containsKey("topUp")) {
               throw new RealmMigrationNeededException(var0.h(), "Missing field 'topUp' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            } else if(var6.get("topUp") != RealmFieldType.BOOLEAN) {
               throw new RealmMigrationNeededException(var0.h(), "Invalid type 'boolean' for field 'topUp' in existing Realm file.");
            } else if(var8.a(var7.s)) {
               throw new RealmMigrationNeededException(var0.h(), "Field 'topUp' does support null values in the existing Realm file. Use corresponding boxed type for field 'topUp' or migrate using RealmObjectSchema.setNullable().");
            } else if(!var6.containsKey("includeInSpending")) {
               throw new RealmMigrationNeededException(var0.h(), "Missing field 'includeInSpending' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            } else if(var6.get("includeInSpending") != RealmFieldType.BOOLEAN) {
               throw new RealmMigrationNeededException(var0.h(), "Invalid type 'boolean' for field 'includeInSpending' in existing Realm file.");
            } else if(var8.a(var7.t)) {
               throw new RealmMigrationNeededException(var0.h(), "Field 'includeInSpending' does support null values in the existing Realm file. Use corresponding boxed type for field 'includeInSpending' or migrate using RealmObjectSchema.setNullable().");
            } else if(!var6.containsKey("attachments")) {
               throw new RealmMigrationNeededException(var0.h(), "Missing field 'attachments'");
            } else if(var6.get("attachments") != RealmFieldType.LIST) {
               throw new RealmMigrationNeededException(var0.h(), "Invalid type 'Attachment' for field 'attachments'");
            } else if(!var0.a("class_Attachment")) {
               throw new RealmMigrationNeededException(var0.h(), "Missing class 'class_Attachment' for field 'attachments'");
            } else {
               var9 = var0.b("class_Attachment");
               if(!var8.e(var7.u).a(var9)) {
                  throw new RealmMigrationNeededException(var0.h(), "Invalid RealmList type for field 'attachments': '" + var8.e(var7.u).j() + "' expected - was '" + var9.j() + "'");
               } else if(!var6.containsKey("peer")) {
                  throw new RealmMigrationNeededException(var0.h(), "Missing field 'peer' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
               } else if(var6.get("peer") != RealmFieldType.OBJECT) {
                  throw new RealmMigrationNeededException(var0.h(), "Invalid type 'Peer' for field 'peer'");
               } else if(!var0.a("class_Peer")) {
                  throw new RealmMigrationNeededException(var0.h(), "Missing class 'class_Peer' for field 'peer'");
               } else {
                  var9 = var0.b("class_Peer");
                  if(!var8.e(var7.v).a(var9)) {
                     throw new RealmMigrationNeededException(var0.h(), "Invalid RealmObject for field 'peer': '" + var8.e(var7.v).j() + "' expected - was '" + var9.j() + "'");
                  } else if(!var6.containsKey("bankDetails")) {
                     throw new RealmMigrationNeededException(var0.h(), "Missing field 'bankDetails' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
                  } else if(var6.get("bankDetails") != RealmFieldType.OBJECT) {
                     throw new RealmMigrationNeededException(var0.h(), "Invalid type 'BankDetails' for field 'bankDetails'");
                  } else if(!var0.a("class_BankDetails")) {
                     throw new RealmMigrationNeededException(var0.h(), "Missing class 'class_BankDetails' for field 'bankDetails'");
                  } else {
                     var9 = var0.b("class_BankDetails");
                     if(!var8.e(var7.w).a(var9)) {
                        throw new RealmMigrationNeededException(var0.h(), "Invalid RealmObject for field 'bankDetails': '" + var8.e(var7.w).j() + "' expected - was '" + var9.j() + "'");
                     } else if(!var6.containsKey("fromMonzoMe")) {
                        throw new RealmMigrationNeededException(var0.h(), "Missing field 'fromMonzoMe' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
                     } else if(var6.get("fromMonzoMe") != RealmFieldType.BOOLEAN) {
                        throw new RealmMigrationNeededException(var0.h(), "Invalid type 'boolean' for field 'fromMonzoMe' in existing Realm file.");
                     } else if(var8.a(var7.x)) {
                        throw new RealmMigrationNeededException(var0.h(), "Field 'fromMonzoMe' does support null values in the existing Realm file. Use corresponding boxed type for field 'fromMonzoMe' or migrate using RealmObjectSchema.setNullable().");
                     } else if(!var6.containsKey("scheme")) {
                        throw new RealmMigrationNeededException(var0.h(), "Missing field 'scheme' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
                     } else if(var6.get("scheme") != RealmFieldType.STRING) {
                        throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'scheme' in existing Realm file.");
                     } else if(!var8.a(var7.y)) {
                        throw new RealmMigrationNeededException(var0.h(), "Field 'scheme' is required. Either set @Required to field 'scheme' or migrate using RealmObjectSchema.setNullable().");
                     } else if(!var6.containsKey("bacsDirectDebitInstructionId")) {
                        throw new RealmMigrationNeededException(var0.h(), "Missing field 'bacsDirectDebitInstructionId' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
                     } else if(var6.get("bacsDirectDebitInstructionId") != RealmFieldType.STRING) {
                        throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'bacsDirectDebitInstructionId' in existing Realm file.");
                     } else if(!var8.a(var7.z)) {
                        throw new RealmMigrationNeededException(var0.h(), "Field 'bacsDirectDebitInstructionId' is required. Either set @Required to field 'bacsDirectDebitInstructionId' or migrate using RealmObjectSchema.setNullable().");
                     } else {
                        return var7;
                     }
                  }
               }
            }
         }
      }
   }

   public static void a(av var0, Iterator var1, Map var2) {
      Table var14 = var0.c(co.uk.getmondo.d.aj.class);
      long var7 = var14.getNativePtr();
      bm.a var13 = (bm.a)var0.f.c(co.uk.getmondo.d.aj.class);
      long var9 = var14.d();

      while(true) {
         while(true) {
            co.uk.getmondo.d.aj var15;
            do {
               if(!var1.hasNext()) {
                  return;
               }

               var15 = (co.uk.getmondo.d.aj)var1.next();
            } while(var2.containsKey(var15));

            if(var15 instanceof io.realm.internal.l && ((io.realm.internal.l)var15).s_().a() != null && ((io.realm.internal.l)var15).s_().a().g().equals(var0.g())) {
               var2.put(var15, Long.valueOf(((io.realm.internal.l)var15).s_().b().c()));
            } else {
               String var11 = ((bn)var15).H();
               long var3;
               if(var11 == null) {
                  var3 = Table.nativeFindFirstNull(var7, var9);
               } else {
                  var3 = Table.nativeFindFirstString(var7, var9, var11);
               }

               long var5 = var3;
               if(var3 == -1L) {
                  var5 = OsObject.b(var14, var11);
               }

               var2.put(var15, Long.valueOf(var5));
               Table.nativeSetLong(var7, var13.b, var5, ((bn)var15).I(), false);
               var11 = ((bn)var15).J();
               if(var11 != null) {
                  Table.nativeSetString(var7, var13.c, var5, var11, false);
               } else {
                  Table.nativeSetNull(var7, var13.c, var5, false);
               }

               Table.nativeSetLong(var7, var13.d, var5, ((bn)var15).K(), false);
               var11 = ((bn)var15).L();
               if(var11 != null) {
                  Table.nativeSetString(var7, var13.e, var5, var11, false);
               } else {
                  Table.nativeSetNull(var7, var13.e, var5, false);
               }

               Date var18 = ((bn)var15).M();
               if(var18 != null) {
                  Table.nativeSetTimestamp(var7, var13.f, var5, var18.getTime(), false);
               } else {
                  Table.nativeSetNull(var7, var13.f, var5, false);
               }

               var11 = ((bn)var15).N();
               if(var11 != null) {
                  Table.nativeSetString(var7, var13.g, var5, var11, false);
               } else {
                  Table.nativeSetNull(var7, var13.g, var5, false);
               }

               var18 = ((bn)var15).O();
               if(var18 != null) {
                  Table.nativeSetTimestamp(var7, var13.h, var5, var18.getTime(), false);
               } else {
                  Table.nativeSetNull(var7, var13.h, var5, false);
               }

               var11 = ((bn)var15).P();
               if(var11 != null) {
                  Table.nativeSetString(var7, var13.i, var5, var11, false);
               } else {
                  Table.nativeSetNull(var7, var13.i, var5, false);
               }

               var11 = ((bn)var15).Q();
               if(var11 != null) {
                  Table.nativeSetString(var7, var13.j, var5, var11, false);
               } else {
                  Table.nativeSetNull(var7, var13.j, var5, false);
               }

               var11 = ((bn)var15).R();
               if(var11 != null) {
                  Table.nativeSetString(var7, var13.k, var5, var11, false);
               } else {
                  Table.nativeSetNull(var7, var13.k, var5, false);
               }

               var11 = ((bn)var15).S();
               if(var11 != null) {
                  Table.nativeSetString(var7, var13.l, var5, var11, false);
               } else {
                  Table.nativeSetNull(var7, var13.l, var5, false);
               }

               Table.nativeSetBoolean(var7, var13.m, var5, ((bn)var15).T(), false);
               Table.nativeSetBoolean(var7, var13.n, var5, ((bn)var15).U(), false);
               co.uk.getmondo.d.u var12 = ((bn)var15).V();
               Long var22;
               if(var12 != null) {
                  var22 = (Long)var2.get(var12);
                  if(var22 == null) {
                     var22 = Long.valueOf(ag.b(var0, var12, var2));
                  }

                  Table.nativeSetLink(var7, var13.o, var5, var22.longValue(), false);
               } else {
                  Table.nativeNullifyLink(var7, var13.o, var5);
               }

               var11 = ((bn)var15).W();
               if(var11 != null) {
                  Table.nativeSetString(var7, var13.p, var5, var11, false);
               } else {
                  Table.nativeSetNull(var7, var13.p, var5, false);
               }

               Table.nativeSetBoolean(var7, var13.q, var5, ((bn)var15).X(), false);
               Table.nativeSetBoolean(var7, var13.r, var5, ((bn)var15).Y(), false);
               Table.nativeSetBoolean(var7, var13.s, var5, ((bn)var15).Z(), false);
               Table.nativeSetBoolean(var7, var13.t, var5, ((bn)var15).aa(), false);
               var3 = Table.nativeGetLinkView(var7, var13.u, var5);
               LinkView.nativeClear(var3);
               az var23 = ((bn)var15).ab();
               if(var23 != null) {
                  for(Iterator var16 = var23.iterator(); var16.hasNext(); LinkView.nativeAdd(var3, var22.longValue())) {
                     co.uk.getmondo.d.d var17 = (co.uk.getmondo.d.d)var16.next();
                     Long var19 = (Long)var2.get(var17);
                     var22 = var19;
                     if(var19 == null) {
                        var22 = Long.valueOf(c.b(var0, var17, var2));
                     }
                  }
               }

               co.uk.getmondo.d.aa var20 = ((bn)var15).ac();
               if(var20 != null) {
                  var22 = (Long)var2.get(var20);
                  if(var22 == null) {
                     var22 = Long.valueOf(as.b(var0, var20, var2));
                  }

                  Table.nativeSetLink(var7, var13.v, var5, var22.longValue(), false);
               } else {
                  Table.nativeNullifyLink(var7, var13.v, var5);
               }

               co.uk.getmondo.payments.send.data.a.a var21 = ((bn)var15).ad();
               if(var21 != null) {
                  var22 = (Long)var2.get(var21);
                  if(var22 == null) {
                     var22 = Long.valueOf(e.b(var0, var21, var2));
                  }

                  Table.nativeSetLink(var7, var13.w, var5, var22.longValue(), false);
               } else {
                  Table.nativeNullifyLink(var7, var13.w, var5);
               }

               Table.nativeSetBoolean(var7, var13.x, var5, ((bn)var15).ae(), false);
               var11 = ((bn)var15).af();
               if(var11 != null) {
                  Table.nativeSetString(var7, var13.y, var5, var11, false);
               } else {
                  Table.nativeSetNull(var7, var13.y, var5, false);
               }

               var11 = ((bn)var15).ag();
               if(var11 != null) {
                  Table.nativeSetString(var7, var13.z, var5, var11, false);
               } else {
                  Table.nativeSetNull(var7, var13.z, var5, false);
               }
            }
         }
      }
   }

   public static OsObjectSchemaInfo ah() {
      return c;
   }

   public static String ai() {
      return "class_Transaction";
   }

   private static OsObjectSchemaInfo aj() {
      OsObjectSchemaInfo.a var0 = new OsObjectSchemaInfo.a("Transaction");
      var0.a("id", RealmFieldType.STRING, true, true, false);
      var0.a("amountValue", RealmFieldType.INTEGER, false, false, true);
      var0.a("amountCurrency", RealmFieldType.STRING, false, false, false);
      var0.a("amountLocalValue", RealmFieldType.INTEGER, false, false, true);
      var0.a("amountLocalCurrency", RealmFieldType.STRING, false, false, false);
      var0.a("created", RealmFieldType.DATE, false, false, false);
      var0.a("createdDateFormatted", RealmFieldType.STRING, false, false, false);
      var0.a("updated", RealmFieldType.DATE, false, false, false);
      var0.a("merchantDescription", RealmFieldType.STRING, false, false, false);
      var0.a("description", RealmFieldType.STRING, false, false, false);
      var0.a("declineReason", RealmFieldType.STRING, false, false, false);
      var0.a("notes", RealmFieldType.STRING, false, false, false);
      var0.a("hideAmount", RealmFieldType.BOOLEAN, false, false, true);
      var0.a("settled", RealmFieldType.BOOLEAN, false, false, true);
      var0.a("merchant", RealmFieldType.OBJECT, "Merchant");
      var0.a("category", RealmFieldType.STRING, false, false, false);
      var0.a("fromAtm", RealmFieldType.BOOLEAN, false, false, true);
      var0.a("peerToPeer", RealmFieldType.BOOLEAN, false, false, true);
      var0.a("topUp", RealmFieldType.BOOLEAN, false, false, true);
      var0.a("includeInSpending", RealmFieldType.BOOLEAN, false, false, true);
      var0.a("attachments", RealmFieldType.LIST, "Attachment");
      var0.a("peer", RealmFieldType.OBJECT, "Peer");
      var0.a("bankDetails", RealmFieldType.OBJECT, "BankDetails");
      var0.a("fromMonzoMe", RealmFieldType.BOOLEAN, false, false, true);
      var0.a("scheme", RealmFieldType.STRING, false, false, false);
      var0.a("bacsDirectDebitInstructionId", RealmFieldType.STRING, false, false, false);
      return var0.a();
   }

   public static long b(av var0, co.uk.getmondo.d.aj var1, Map var2) {
      long var3;
      if(var1 instanceof io.realm.internal.l && ((io.realm.internal.l)var1).s_().a() != null && ((io.realm.internal.l)var1).s_().a().g().equals(var0.g())) {
         var3 = ((io.realm.internal.l)var1).s_().b().c();
      } else {
         Table var10 = var0.c(co.uk.getmondo.d.aj.class);
         long var7 = var10.getNativePtr();
         bm.a var11 = (bm.a)var0.f.c(co.uk.getmondo.d.aj.class);
         var3 = var10.d();
         String var9 = ((bn)var1).H();
         long var5;
         if(var9 == null) {
            var5 = Table.nativeFindFirstNull(var7, var3);
         } else {
            var5 = Table.nativeFindFirstString(var7, var3, var9);
         }

         var3 = var5;
         if(var5 == -1L) {
            var3 = OsObject.b(var10, var9);
         }

         var2.put(var1, Long.valueOf(var3));
         Table.nativeSetLong(var7, var11.b, var3, ((bn)var1).I(), false);
         var9 = ((bn)var1).J();
         if(var9 != null) {
            Table.nativeSetString(var7, var11.c, var3, var9, false);
         } else {
            Table.nativeSetNull(var7, var11.c, var3, false);
         }

         Table.nativeSetLong(var7, var11.d, var3, ((bn)var1).K(), false);
         var9 = ((bn)var1).L();
         if(var9 != null) {
            Table.nativeSetString(var7, var11.e, var3, var9, false);
         } else {
            Table.nativeSetNull(var7, var11.e, var3, false);
         }

         Date var16 = ((bn)var1).M();
         if(var16 != null) {
            Table.nativeSetTimestamp(var7, var11.f, var3, var16.getTime(), false);
         } else {
            Table.nativeSetNull(var7, var11.f, var3, false);
         }

         var9 = ((bn)var1).N();
         if(var9 != null) {
            Table.nativeSetString(var7, var11.g, var3, var9, false);
         } else {
            Table.nativeSetNull(var7, var11.g, var3, false);
         }

         var16 = ((bn)var1).O();
         if(var16 != null) {
            Table.nativeSetTimestamp(var7, var11.h, var3, var16.getTime(), false);
         } else {
            Table.nativeSetNull(var7, var11.h, var3, false);
         }

         var9 = ((bn)var1).P();
         if(var9 != null) {
            Table.nativeSetString(var7, var11.i, var3, var9, false);
         } else {
            Table.nativeSetNull(var7, var11.i, var3, false);
         }

         var9 = ((bn)var1).Q();
         if(var9 != null) {
            Table.nativeSetString(var7, var11.j, var3, var9, false);
         } else {
            Table.nativeSetNull(var7, var11.j, var3, false);
         }

         var9 = ((bn)var1).R();
         if(var9 != null) {
            Table.nativeSetString(var7, var11.k, var3, var9, false);
         } else {
            Table.nativeSetNull(var7, var11.k, var3, false);
         }

         var9 = ((bn)var1).S();
         if(var9 != null) {
            Table.nativeSetString(var7, var11.l, var3, var9, false);
         } else {
            Table.nativeSetNull(var7, var11.l, var3, false);
         }

         Table.nativeSetBoolean(var7, var11.m, var3, ((bn)var1).T(), false);
         Table.nativeSetBoolean(var7, var11.n, var3, ((bn)var1).U(), false);
         co.uk.getmondo.d.u var17 = ((bn)var1).V();
         Long var21;
         if(var17 != null) {
            var21 = (Long)var2.get(var17);
            if(var21 == null) {
               var21 = Long.valueOf(ag.b(var0, var17, var2));
            }

            Table.nativeSetLink(var7, var11.o, var3, var21.longValue(), false);
         } else {
            Table.nativeNullifyLink(var7, var11.o, var3);
         }

         var9 = ((bn)var1).W();
         if(var9 != null) {
            Table.nativeSetString(var7, var11.p, var3, var9, false);
         } else {
            Table.nativeSetNull(var7, var11.p, var3, false);
         }

         Table.nativeSetBoolean(var7, var11.q, var3, ((bn)var1).X(), false);
         Table.nativeSetBoolean(var7, var11.r, var3, ((bn)var1).Y(), false);
         Table.nativeSetBoolean(var7, var11.s, var3, ((bn)var1).Z(), false);
         Table.nativeSetBoolean(var7, var11.t, var3, ((bn)var1).aa(), false);
         var5 = Table.nativeGetLinkView(var7, var11.u, var3);
         LinkView.nativeClear(var5);
         az var22 = ((bn)var1).ab();
         if(var22 != null) {
            for(Iterator var12 = var22.iterator(); var12.hasNext(); LinkView.nativeAdd(var5, var21.longValue())) {
               co.uk.getmondo.d.d var13 = (co.uk.getmondo.d.d)var12.next();
               Long var18 = (Long)var2.get(var13);
               var21 = var18;
               if(var18 == null) {
                  var21 = Long.valueOf(c.b(var0, var13, var2));
               }
            }
         }

         co.uk.getmondo.d.aa var19 = ((bn)var1).ac();
         if(var19 != null) {
            var21 = (Long)var2.get(var19);
            if(var21 == null) {
               var21 = Long.valueOf(as.b(var0, var19, var2));
            }

            Table.nativeSetLink(var7, var11.v, var3, var21.longValue(), false);
         } else {
            Table.nativeNullifyLink(var7, var11.v, var3);
         }

         co.uk.getmondo.payments.send.data.a.a var20 = ((bn)var1).ad();
         if(var20 != null) {
            var21 = (Long)var2.get(var20);
            Long var14;
            if(var21 == null) {
               var14 = Long.valueOf(e.b(var0, var20, var2));
            } else {
               var14 = var21;
            }

            Table.nativeSetLink(var7, var11.w, var3, var14.longValue(), false);
         } else {
            Table.nativeNullifyLink(var7, var11.w, var3);
         }

         Table.nativeSetBoolean(var7, var11.x, var3, ((bn)var1).ae(), false);
         String var15 = ((bn)var1).af();
         if(var15 != null) {
            Table.nativeSetString(var7, var11.y, var3, var15, false);
         } else {
            Table.nativeSetNull(var7, var11.y, var3, false);
         }

         var15 = ((bn)var1).ag();
         if(var15 != null) {
            Table.nativeSetString(var7, var11.z, var3, var15, false);
         } else {
            Table.nativeSetNull(var7, var11.z, var3, false);
         }
      }

      return var3;
   }

   public static co.uk.getmondo.d.aj b(av var0, co.uk.getmondo.d.aj var1, boolean var2, Map var3) {
      io.realm.internal.l var5 = (io.realm.internal.l)var3.get(var1);
      co.uk.getmondo.d.aj var11;
      if(var5 != null) {
         var11 = (co.uk.getmondo.d.aj)var5;
      } else {
         co.uk.getmondo.d.aj var13 = (co.uk.getmondo.d.aj)var0.a(co.uk.getmondo.d.aj.class, ((bn)var1).H(), false, Collections.emptyList());
         var3.put(var1, (io.realm.internal.l)var13);
         bn var12 = (bn)var1;
         bn var6 = (bn)var13;
         var6.a(var12.I());
         var6.d(var12.J());
         var6.b(var12.K());
         var6.e(var12.L());
         var6.a(var12.M());
         var6.f(var12.N());
         var6.b(var12.O());
         var6.g(var12.P());
         var6.h(var12.Q());
         var6.i(var12.R());
         var6.j(var12.S());
         var6.a(var12.T());
         var6.b(var12.U());
         co.uk.getmondo.d.u var7 = var12.V();
         if(var7 == null) {
            var6.a((co.uk.getmondo.d.u)null);
         } else {
            co.uk.getmondo.d.u var8 = (co.uk.getmondo.d.u)var3.get(var7);
            if(var8 != null) {
               var6.a(var8);
            } else {
               var6.a(ag.a(var0, var7, var2, var3));
            }
         }

         var6.k(var12.W());
         var6.c(var12.X());
         var6.d(var12.Y());
         var6.e(var12.Z());
         var6.f(var12.aa());
         az var17 = var12.ab();
         if(var17 != null) {
            az var9 = var6.ab();

            for(int var4 = 0; var4 < var17.size(); ++var4) {
               co.uk.getmondo.d.d var14 = (co.uk.getmondo.d.d)var17.b(var4);
               co.uk.getmondo.d.d var10 = (co.uk.getmondo.d.d)var3.get(var14);
               if(var10 != null) {
                  var9.a((bb)var10);
               } else {
                  var9.a((bb)c.a(var0, var14, var2, var3));
               }
            }
         }

         co.uk.getmondo.d.aa var18 = var12.ac();
         if(var18 == null) {
            var6.a((co.uk.getmondo.d.aa)null);
         } else {
            co.uk.getmondo.d.aa var15 = (co.uk.getmondo.d.aa)var3.get(var18);
            if(var15 != null) {
               var6.a(var15);
            } else {
               var6.a(as.a(var0, var18, var2, var3));
            }
         }

         co.uk.getmondo.payments.send.data.a.a var19 = var12.ad();
         if(var19 == null) {
            var6.a((co.uk.getmondo.payments.send.data.a.a)null);
         } else {
            co.uk.getmondo.payments.send.data.a.a var16 = (co.uk.getmondo.payments.send.data.a.a)var3.get(var19);
            if(var16 != null) {
               var6.a(var16);
            } else {
               var6.a(e.a(var0, var19, var2, var3));
            }
         }

         var6.g(var12.ae());
         var6.l(var12.af());
         var6.m(var12.ag());
         var11 = var13;
      }

      return var11;
   }

   public String H() {
      this.b.a().e();
      return this.b.b().k(this.a.a);
   }

   public long I() {
      this.b.a().e();
      return this.b.b().f(this.a.b);
   }

   public String J() {
      this.b.a().e();
      return this.b.b().k(this.a.c);
   }

   public long K() {
      this.b.a().e();
      return this.b.b().f(this.a.d);
   }

   public String L() {
      this.b.a().e();
      return this.b.b().k(this.a.e);
   }

   public Date M() {
      this.b.a().e();
      Date var1;
      if(this.b.b().b(this.a.f)) {
         var1 = null;
      } else {
         var1 = this.b.b().j(this.a.f);
      }

      return var1;
   }

   public String N() {
      this.b.a().e();
      return this.b.b().k(this.a.g);
   }

   public Date O() {
      this.b.a().e();
      Date var1;
      if(this.b.b().b(this.a.h)) {
         var1 = null;
      } else {
         var1 = this.b.b().j(this.a.h);
      }

      return var1;
   }

   public String P() {
      this.b.a().e();
      return this.b.b().k(this.a.i);
   }

   public String Q() {
      this.b.a().e();
      return this.b.b().k(this.a.j);
   }

   public String R() {
      this.b.a().e();
      return this.b.b().k(this.a.k);
   }

   public String S() {
      this.b.a().e();
      return this.b.b().k(this.a.l);
   }

   public boolean T() {
      this.b.a().e();
      return this.b.b().g(this.a.m);
   }

   public boolean U() {
      this.b.a().e();
      return this.b.b().g(this.a.n);
   }

   public co.uk.getmondo.d.u V() {
      this.b.a().e();
      co.uk.getmondo.d.u var1;
      if(this.b.b().a(this.a.o)) {
         var1 = null;
      } else {
         var1 = (co.uk.getmondo.d.u)this.b.a().a(co.uk.getmondo.d.u.class, this.b.b().m(this.a.o), false, Collections.emptyList());
      }

      return var1;
   }

   public String W() {
      this.b.a().e();
      return this.b.b().k(this.a.p);
   }

   public boolean X() {
      this.b.a().e();
      return this.b.b().g(this.a.q);
   }

   public boolean Y() {
      this.b.a().e();
      return this.b.b().g(this.a.r);
   }

   public boolean Z() {
      this.b.a().e();
      return this.b.b().g(this.a.s);
   }

   public void a(long var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var3 = this.b.b();
            var3.b().a(this.a.b, var3.c(), var1, true);
         }
      } else {
         this.b.a().e();
         this.b.b().a(this.a.b, var1);
      }

   }

   public void a(co.uk.getmondo.d.aa var1) {
      if(this.b.e()) {
         if(this.b.c() && !this.b.d().contains("peer")) {
            if(var1 != null && !bc.c(var1)) {
               var1 = (co.uk.getmondo.d.aa)((av)this.b.a()).a((bb)var1);
            }

            io.realm.internal.n var2 = this.b.b();
            if(var1 == null) {
               var2.o(this.a.v);
            } else {
               if(!bc.b(var1)) {
                  throw new IllegalArgumentException("'value' is not a valid managed object.");
               }

               if(((io.realm.internal.l)var1).s_().a() != this.b.a()) {
                  throw new IllegalArgumentException("'value' belongs to a different Realm.");
               }

               var2.b().b(this.a.v, var2.c(), ((io.realm.internal.l)var1).s_().b().c(), true);
            }
         }
      } else {
         this.b.a().e();
         if(var1 == null) {
            this.b.b().o(this.a.v);
         } else {
            if(!bc.c(var1) || !bc.b(var1)) {
               throw new IllegalArgumentException("'value' is not a valid managed object.");
            }

            if(((io.realm.internal.l)var1).s_().a() != this.b.a()) {
               throw new IllegalArgumentException("'value' belongs to a different Realm.");
            }

            this.b.b().b(this.a.v, ((io.realm.internal.l)var1).s_().b().c());
         }
      }

   }

   public void a(co.uk.getmondo.d.u var1) {
      if(this.b.e()) {
         if(this.b.c() && !this.b.d().contains("merchant")) {
            if(var1 != null && !bc.c(var1)) {
               var1 = (co.uk.getmondo.d.u)((av)this.b.a()).a((bb)var1);
            }

            io.realm.internal.n var2 = this.b.b();
            if(var1 == null) {
               var2.o(this.a.o);
            } else {
               if(!bc.b(var1)) {
                  throw new IllegalArgumentException("'value' is not a valid managed object.");
               }

               if(((io.realm.internal.l)var1).s_().a() != this.b.a()) {
                  throw new IllegalArgumentException("'value' belongs to a different Realm.");
               }

               var2.b().b(this.a.o, var2.c(), ((io.realm.internal.l)var1).s_().b().c(), true);
            }
         }
      } else {
         this.b.a().e();
         if(var1 == null) {
            this.b.b().o(this.a.o);
         } else {
            if(!bc.c(var1) || !bc.b(var1)) {
               throw new IllegalArgumentException("'value' is not a valid managed object.");
            }

            if(((io.realm.internal.l)var1).s_().a() != this.b.a()) {
               throw new IllegalArgumentException("'value' belongs to a different Realm.");
            }

            this.b.b().b(this.a.o, ((io.realm.internal.l)var1).s_().b().c());
         }
      }

   }

   public void a(co.uk.getmondo.payments.send.data.a.a var1) {
      if(this.b.e()) {
         if(this.b.c() && !this.b.d().contains("bankDetails")) {
            if(var1 != null && !bc.c(var1)) {
               var1 = (co.uk.getmondo.payments.send.data.a.a)((av)this.b.a()).a((bb)var1);
            }

            io.realm.internal.n var2 = this.b.b();
            if(var1 == null) {
               var2.o(this.a.w);
            } else {
               if(!bc.b(var1)) {
                  throw new IllegalArgumentException("'value' is not a valid managed object.");
               }

               if(((io.realm.internal.l)var1).s_().a() != this.b.a()) {
                  throw new IllegalArgumentException("'value' belongs to a different Realm.");
               }

               var2.b().b(this.a.w, var2.c(), ((io.realm.internal.l)var1).s_().b().c(), true);
            }
         }
      } else {
         this.b.a().e();
         if(var1 == null) {
            this.b.b().o(this.a.w);
         } else {
            if(!bc.c(var1) || !bc.b(var1)) {
               throw new IllegalArgumentException("'value' is not a valid managed object.");
            }

            if(((io.realm.internal.l)var1).s_().a() != this.b.a()) {
               throw new IllegalArgumentException("'value' belongs to a different Realm.");
            }

            this.b.b().b(this.a.w, ((io.realm.internal.l)var1).s_().b().c());
         }
      }

   }

   public void a(az var1) {
      az var2 = var1;
      if(this.b.e()) {
         if(!this.b.c() || this.b.d().contains("attachments")) {
            return;
         }

         var2 = var1;
         if(var1 != null) {
            var2 = var1;
            if(!var1.a()) {
               av var3 = (av)this.b.a();
               var2 = new az();
               Iterator var5 = var1.iterator();

               label51:
               while(true) {
                  while(true) {
                     if(!var5.hasNext()) {
                        break label51;
                     }

                     co.uk.getmondo.d.d var4 = (co.uk.getmondo.d.d)var5.next();
                     if(var4 != null && !bc.c(var4)) {
                        var2.a(var3.a((bb)var4));
                     } else {
                        var2.a((bb)var4);
                     }
                  }
               }
            }
         }
      }

      this.b.a().e();
      LinkView var6 = this.b.b().n(this.a.u);
      var6.a();
      if(var2 != null) {
         Iterator var8 = var2.iterator();

         while(var8.hasNext()) {
            bb var7 = (bb)var8.next();
            if(!bc.c(var7) || !bc.b(var7)) {
               throw new IllegalArgumentException("Each element of 'value' must be a valid managed object.");
            }

            if(((io.realm.internal.l)var7).s_().a() != this.b.a()) {
               throw new IllegalArgumentException("Each element of 'value' must belong to the same Realm.");
            }

            var6.b(((io.realm.internal.l)var7).s_().b().c());
         }
      }

   }

   public void a(Date var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            if(var1 == null) {
               var2.b().a(this.a.f, var2.c(), true);
            } else {
               var2.b().a(this.a.f, var2.c(), var1, true);
            }
         }
      } else {
         this.b.a().e();
         if(var1 == null) {
            this.b.b().c(this.a.f);
         } else {
            this.b.b().a(this.a.f, var1);
         }
      }

   }

   public void a(boolean var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            var2.b().a(this.a.m, var2.c(), var1, true);
         }
      } else {
         this.b.a().e();
         this.b.b().a(this.a.m, var1);
      }

   }

   public boolean aa() {
      this.b.a().e();
      return this.b.b().g(this.a.t);
   }

   public az ab() {
      this.b.a().e();
      az var1;
      if(this.d != null) {
         var1 = this.d;
      } else {
         this.d = new az(co.uk.getmondo.d.d.class, this.b.b().n(this.a.u), this.b.a());
         var1 = this.d;
      }

      return var1;
   }

   public co.uk.getmondo.d.aa ac() {
      this.b.a().e();
      co.uk.getmondo.d.aa var1;
      if(this.b.b().a(this.a.v)) {
         var1 = null;
      } else {
         var1 = (co.uk.getmondo.d.aa)this.b.a().a(co.uk.getmondo.d.aa.class, this.b.b().m(this.a.v), false, Collections.emptyList());
      }

      return var1;
   }

   public co.uk.getmondo.payments.send.data.a.a ad() {
      this.b.a().e();
      co.uk.getmondo.payments.send.data.a.a var1;
      if(this.b.b().a(this.a.w)) {
         var1 = null;
      } else {
         var1 = (co.uk.getmondo.payments.send.data.a.a)this.b.a().a(co.uk.getmondo.payments.send.data.a.a.class, this.b.b().m(this.a.w), false, Collections.emptyList());
      }

      return var1;
   }

   public boolean ae() {
      this.b.a().e();
      return this.b.b().g(this.a.x);
   }

   public String af() {
      this.b.a().e();
      return this.b.b().k(this.a.y);
   }

   public String ag() {
      this.b.a().e();
      return this.b.b().k(this.a.z);
   }

   public void b(long var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var3 = this.b.b();
            var3.b().a(this.a.d, var3.c(), var1, true);
         }
      } else {
         this.b.a().e();
         this.b.b().a(this.a.d, var1);
      }

   }

   public void b(Date var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            if(var1 == null) {
               var2.b().a(this.a.h, var2.c(), true);
            } else {
               var2.b().a(this.a.h, var2.c(), var1, true);
            }
         }
      } else {
         this.b.a().e();
         if(var1 == null) {
            this.b.b().c(this.a.h);
         } else {
            this.b.b().a(this.a.h, var1);
         }
      }

   }

   public void b(boolean var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            var2.b().a(this.a.n, var2.c(), var1, true);
         }
      } else {
         this.b.a().e();
         this.b.b().a(this.a.n, var1);
      }

   }

   public void c(String var1) {
      if(!this.b.e()) {
         this.b.a().e();
         throw new RealmException("Primary key field 'id' cannot be changed after object was created.");
      }
   }

   public void c(boolean var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            var2.b().a(this.a.q, var2.c(), var1, true);
         }
      } else {
         this.b.a().e();
         this.b.b().a(this.a.q, var1);
      }

   }

   public void d(String var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            if(var1 == null) {
               var2.b().a(this.a.c, var2.c(), true);
            } else {
               var2.b().a(this.a.c, var2.c(), var1, true);
            }
         }
      } else {
         this.b.a().e();
         if(var1 == null) {
            this.b.b().c(this.a.c);
         } else {
            this.b.b().a(this.a.c, var1);
         }
      }

   }

   public void d(boolean var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            var2.b().a(this.a.r, var2.c(), var1, true);
         }
      } else {
         this.b.a().e();
         this.b.b().a(this.a.r, var1);
      }

   }

   public void e(String var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            if(var1 == null) {
               var2.b().a(this.a.e, var2.c(), true);
            } else {
               var2.b().a(this.a.e, var2.c(), var1, true);
            }
         }
      } else {
         this.b.a().e();
         if(var1 == null) {
            this.b.b().c(this.a.e);
         } else {
            this.b.b().a(this.a.e, var1);
         }
      }

   }

   public void e(boolean var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            var2.b().a(this.a.s, var2.c(), var1, true);
         }
      } else {
         this.b.a().e();
         this.b.b().a(this.a.s, var1);
      }

   }

   public void f(String var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            if(var1 == null) {
               var2.b().a(this.a.g, var2.c(), true);
            } else {
               var2.b().a(this.a.g, var2.c(), var1, true);
            }
         }
      } else {
         this.b.a().e();
         if(var1 == null) {
            this.b.b().c(this.a.g);
         } else {
            this.b.b().a(this.a.g, var1);
         }
      }

   }

   public void f(boolean var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            var2.b().a(this.a.t, var2.c(), var1, true);
         }
      } else {
         this.b.a().e();
         this.b.b().a(this.a.t, var1);
      }

   }

   public void g(String var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            if(var1 == null) {
               var2.b().a(this.a.i, var2.c(), true);
            } else {
               var2.b().a(this.a.i, var2.c(), var1, true);
            }
         }
      } else {
         this.b.a().e();
         if(var1 == null) {
            this.b.b().c(this.a.i);
         } else {
            this.b.b().a(this.a.i, var1);
         }
      }

   }

   public void g(boolean var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            var2.b().a(this.a.x, var2.c(), var1, true);
         }
      } else {
         this.b.a().e();
         this.b.b().a(this.a.x, var1);
      }

   }

   public void h(String var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            if(var1 == null) {
               var2.b().a(this.a.j, var2.c(), true);
            } else {
               var2.b().a(this.a.j, var2.c(), var1, true);
            }
         }
      } else {
         this.b.a().e();
         if(var1 == null) {
            this.b.b().c(this.a.j);
         } else {
            this.b.b().a(this.a.j, var1);
         }
      }

   }

   public void i(String var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            if(var1 == null) {
               var2.b().a(this.a.k, var2.c(), true);
            } else {
               var2.b().a(this.a.k, var2.c(), var1, true);
            }
         }
      } else {
         this.b.a().e();
         if(var1 == null) {
            this.b.b().c(this.a.k);
         } else {
            this.b.b().a(this.a.k, var1);
         }
      }

   }

   public void j(String var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            if(var1 == null) {
               var2.b().a(this.a.l, var2.c(), true);
            } else {
               var2.b().a(this.a.l, var2.c(), var1, true);
            }
         }
      } else {
         this.b.a().e();
         if(var1 == null) {
            this.b.b().c(this.a.l);
         } else {
            this.b.b().a(this.a.l, var1);
         }
      }

   }

   public void k(String var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            if(var1 == null) {
               var2.b().a(this.a.p, var2.c(), true);
            } else {
               var2.b().a(this.a.p, var2.c(), var1, true);
            }
         }
      } else {
         this.b.a().e();
         if(var1 == null) {
            this.b.b().c(this.a.p);
         } else {
            this.b.b().a(this.a.p, var1);
         }
      }

   }

   public void l(String var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            if(var1 == null) {
               var2.b().a(this.a.y, var2.c(), true);
            } else {
               var2.b().a(this.a.y, var2.c(), var1, true);
            }
         }
      } else {
         this.b.a().e();
         if(var1 == null) {
            this.b.b().c(this.a.y);
         } else {
            this.b.b().a(this.a.y, var1);
         }
      }

   }

   public void m(String var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            if(var1 == null) {
               var2.b().a(this.a.z, var2.c(), true);
            } else {
               var2.b().a(this.a.z, var2.c(), var1, true);
            }
         }
      } else {
         this.b.a().e();
         if(var1 == null) {
            this.b.b().c(this.a.z);
         } else {
            this.b.b().a(this.a.z, var1);
         }
      }

   }

   public au s_() {
      return this.b;
   }

   public String toString() {
      String var1;
      if(!bc.b(this)) {
         var1 = "Invalid object";
      } else {
         StringBuilder var2 = new StringBuilder("Transaction = proxy[");
         var2.append("{id:");
         if(this.H() != null) {
            var1 = this.H();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{amountValue:");
         var2.append(this.I());
         var2.append("}");
         var2.append(",");
         var2.append("{amountCurrency:");
         if(this.J() != null) {
            var1 = this.J();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{amountLocalValue:");
         var2.append(this.K());
         var2.append("}");
         var2.append(",");
         var2.append("{amountLocalCurrency:");
         if(this.L() != null) {
            var1 = this.L();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{created:");
         Object var3;
         if(this.M() != null) {
            var3 = this.M();
         } else {
            var3 = "null";
         }

         var2.append(var3);
         var2.append("}");
         var2.append(",");
         var2.append("{createdDateFormatted:");
         if(this.N() != null) {
            var1 = this.N();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{updated:");
         if(this.O() != null) {
            var3 = this.O();
         } else {
            var3 = "null";
         }

         var2.append(var3);
         var2.append("}");
         var2.append(",");
         var2.append("{merchantDescription:");
         if(this.P() != null) {
            var1 = this.P();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{description:");
         if(this.Q() != null) {
            var1 = this.Q();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{declineReason:");
         if(this.R() != null) {
            var1 = this.R();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{notes:");
         if(this.S() != null) {
            var1 = this.S();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{hideAmount:");
         var2.append(this.T());
         var2.append("}");
         var2.append(",");
         var2.append("{settled:");
         var2.append(this.U());
         var2.append("}");
         var2.append(",");
         var2.append("{merchant:");
         if(this.V() != null) {
            var1 = "Merchant";
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{category:");
         if(this.W() != null) {
            var1 = this.W();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{fromAtm:");
         var2.append(this.X());
         var2.append("}");
         var2.append(",");
         var2.append("{peerToPeer:");
         var2.append(this.Y());
         var2.append("}");
         var2.append(",");
         var2.append("{topUp:");
         var2.append(this.Z());
         var2.append("}");
         var2.append(",");
         var2.append("{includeInSpending:");
         var2.append(this.aa());
         var2.append("}");
         var2.append(",");
         var2.append("{attachments:");
         var2.append("RealmList<Attachment>[").append(this.ab().size()).append("]");
         var2.append("}");
         var2.append(",");
         var2.append("{peer:");
         if(this.ac() != null) {
            var1 = "Peer";
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{bankDetails:");
         if(this.ad() != null) {
            var1 = "BankDetails";
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{fromMonzoMe:");
         var2.append(this.ae());
         var2.append("}");
         var2.append(",");
         var2.append("{scheme:");
         if(this.af() != null) {
            var1 = this.af();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{bacsDirectDebitInstructionId:");
         if(this.ag() != null) {
            var1 = this.ag();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append("]");
         var1 = var2.toString();
      }

      return var1;
   }

   public void u_() {
      if(this.b == null) {
         g.b var1 = (g.b)g.g.get();
         this.a = (bm.a)var1.c();
         this.b = new au(this);
         this.b.a(var1.a());
         this.b.a(var1.b());
         this.b.a(var1.d());
         this.b.a(var1.e());
      }

   }

   static final class a extends io.realm.internal.c {
      long a;
      long b;
      long c;
      long d;
      long e;
      long f;
      long g;
      long h;
      long i;
      long j;
      long k;
      long l;
      long m;
      long n;
      long o;
      long p;
      long q;
      long r;
      long s;
      long t;
      long u;
      long v;
      long w;
      long x;
      long y;
      long z;

      a(SharedRealm var1, Table var2) {
         super(26);
         this.a = this.a(var2, "id", RealmFieldType.STRING);
         this.b = this.a(var2, "amountValue", RealmFieldType.INTEGER);
         this.c = this.a(var2, "amountCurrency", RealmFieldType.STRING);
         this.d = this.a(var2, "amountLocalValue", RealmFieldType.INTEGER);
         this.e = this.a(var2, "amountLocalCurrency", RealmFieldType.STRING);
         this.f = this.a(var2, "created", RealmFieldType.DATE);
         this.g = this.a(var2, "createdDateFormatted", RealmFieldType.STRING);
         this.h = this.a(var2, "updated", RealmFieldType.DATE);
         this.i = this.a(var2, "merchantDescription", RealmFieldType.STRING);
         this.j = this.a(var2, "description", RealmFieldType.STRING);
         this.k = this.a(var2, "declineReason", RealmFieldType.STRING);
         this.l = this.a(var2, "notes", RealmFieldType.STRING);
         this.m = this.a(var2, "hideAmount", RealmFieldType.BOOLEAN);
         this.n = this.a(var2, "settled", RealmFieldType.BOOLEAN);
         this.o = this.a(var2, "merchant", RealmFieldType.OBJECT);
         this.p = this.a(var2, "category", RealmFieldType.STRING);
         this.q = this.a(var2, "fromAtm", RealmFieldType.BOOLEAN);
         this.r = this.a(var2, "peerToPeer", RealmFieldType.BOOLEAN);
         this.s = this.a(var2, "topUp", RealmFieldType.BOOLEAN);
         this.t = this.a(var2, "includeInSpending", RealmFieldType.BOOLEAN);
         this.u = this.a(var2, "attachments", RealmFieldType.LIST);
         this.v = this.a(var2, "peer", RealmFieldType.OBJECT);
         this.w = this.a(var2, "bankDetails", RealmFieldType.OBJECT);
         this.x = this.a(var2, "fromMonzoMe", RealmFieldType.BOOLEAN);
         this.y = this.a(var2, "scheme", RealmFieldType.STRING);
         this.z = this.a(var2, "bacsDirectDebitInstructionId", RealmFieldType.STRING);
      }

      a(io.realm.internal.c var1, boolean var2) {
         super(var1, var2);
         this.a(var1, this);
      }

      protected final io.realm.internal.c a(boolean var1) {
         return new bm.a(this, var1);
      }

      protected final void a(io.realm.internal.c var1, io.realm.internal.c var2) {
         bm.a var3 = (bm.a)var1;
         bm.a var4 = (bm.a)var2;
         var4.a = var3.a;
         var4.b = var3.b;
         var4.c = var3.c;
         var4.d = var3.d;
         var4.e = var3.e;
         var4.f = var3.f;
         var4.g = var3.g;
         var4.h = var3.h;
         var4.i = var3.i;
         var4.j = var3.j;
         var4.k = var3.k;
         var4.l = var3.l;
         var4.m = var3.m;
         var4.n = var3.n;
         var4.o = var3.o;
         var4.p = var3.p;
         var4.q = var3.q;
         var4.r = var3.r;
         var4.s = var3.s;
         var4.t = var3.t;
         var4.u = var3.u;
         var4.v = var3.v;
         var4.w = var3.w;
         var4.x = var3.x;
         var4.y = var3.y;
         var4.z = var3.z;
      }
   }
}
