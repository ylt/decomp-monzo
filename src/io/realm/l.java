package io.realm;

public enum l {
   a(true),
   b(false);

   private final boolean c;

   private l(boolean var3) {
      this.c = var3;
   }

   public boolean a() {
      return this.c;
   }
}
