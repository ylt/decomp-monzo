package io.realm;

import io.realm.annotations.RealmModule;
import io.realm.internal.SharedRealm;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

@RealmModule
class DefaultRealmModuleMediator extends io.realm.internal.m {
   private static final Set a;

   static {
      HashSet var0 = new HashSet();
      var0.add(co.uk.getmondo.d.p.class);
      var0.add(co.uk.getmondo.d.ag.class);
      var0.add(co.uk.getmondo.d.l.class);
      var0.add(co.uk.getmondo.payments.a.a.e.class);
      var0.add(co.uk.getmondo.d.t.class);
      var0.add(co.uk.getmondo.d.aa.class);
      var0.add(co.uk.getmondo.d.n.class);
      var0.add(co.uk.getmondo.d.u.class);
      var0.add(co.uk.getmondo.payments.send.data.a.a.class);
      var0.add(co.uk.getmondo.d.m.class);
      var0.add(co.uk.getmondo.d.am.class);
      var0.add(co.uk.getmondo.d.b.class);
      var0.add(co.uk.getmondo.d.r.class);
      var0.add(co.uk.getmondo.d.aj.class);
      var0.add(co.uk.getmondo.payments.a.a.a.class);
      var0.add(co.uk.getmondo.d.f.class);
      var0.add(co.uk.getmondo.d.d.class);
      var0.add(co.uk.getmondo.d.w.class);
      var0.add(co.uk.getmondo.d.o.class);
      var0.add(co.uk.getmondo.d.g.class);
      var0.add(co.uk.getmondo.payments.a.a.b.class);
      var0.add(co.uk.getmondo.d.v.class);
      a = Collections.unmodifiableSet(var0);
   }

   public bb a(av var1, bb var2, boolean var3, Map var4) {
      Class var5;
      if(var2 instanceof io.realm.internal.l) {
         var5 = var2.getClass().getSuperclass();
      } else {
         var5 = var2.getClass();
      }

      bb var6;
      if(var5.equals(co.uk.getmondo.d.p.class)) {
         var6 = (bb)var5.cast(aa.a(var1, (co.uk.getmondo.d.p)var2, var3, var4));
      } else if(var5.equals(co.uk.getmondo.d.ag.class)) {
         var6 = (bb)var5.cast(bj.a(var1, (co.uk.getmondo.d.ag)var2, var3, var4));
      } else if(var5.equals(co.uk.getmondo.d.l.class)) {
         var6 = (bb)var5.cast(q.a(var1, (co.uk.getmondo.d.l)var2, var3, var4));
      } else if(var5.equals(co.uk.getmondo.payments.a.a.e.class)) {
         var6 = (bb)var5.cast(aq.a(var1, (co.uk.getmondo.payments.a.a.e)var2, var3, var4));
      } else if(var5.equals(co.uk.getmondo.d.t.class)) {
         var6 = (bb)var5.cast(ae.a(var1, (co.uk.getmondo.d.t)var2, var3, var4));
      } else if(var5.equals(co.uk.getmondo.d.aa.class)) {
         var6 = (bb)var5.cast(as.a(var1, (co.uk.getmondo.d.aa)var2, var3, var4));
      } else if(var5.equals(co.uk.getmondo.d.n.class)) {
         var6 = (bb)var5.cast(u.a(var1, (co.uk.getmondo.d.n)var2, var3, var4));
      } else if(var5.equals(co.uk.getmondo.d.u.class)) {
         var6 = (bb)var5.cast(ag.a(var1, (co.uk.getmondo.d.u)var2, var3, var4));
      } else if(var5.equals(co.uk.getmondo.payments.send.data.a.a.class)) {
         var6 = (bb)var5.cast(e.a(var1, (co.uk.getmondo.payments.send.data.a.a)var2, var3, var4));
      } else if(var5.equals(co.uk.getmondo.d.m.class)) {
         var6 = (bb)var5.cast(s.a(var1, (co.uk.getmondo.d.m)var2, var3, var4));
      } else if(var5.equals(co.uk.getmondo.d.am.class)) {
         var6 = (bb)var5.cast(bo.a(var1, (co.uk.getmondo.d.am)var2, var3, var4));
      } else if(var5.equals(co.uk.getmondo.d.b.class)) {
         var6 = (bb)var5.cast(a.a(var1, (co.uk.getmondo.d.b)var2, var3, var4));
      } else if(var5.equals(co.uk.getmondo.d.r.class)) {
         var6 = (bb)var5.cast(ac.a(var1, (co.uk.getmondo.d.r)var2, var3, var4));
      } else if(var5.equals(co.uk.getmondo.d.aj.class)) {
         var6 = (bb)var5.cast(bm.a(var1, (co.uk.getmondo.d.aj)var2, var3, var4));
      } else if(var5.equals(co.uk.getmondo.payments.a.a.a.class)) {
         var6 = (bb)var5.cast(m.a(var1, (co.uk.getmondo.payments.a.a.a)var2, var3, var4));
      } else if(var5.equals(co.uk.getmondo.d.f.class)) {
         var6 = (bb)var5.cast(h.a(var1, (co.uk.getmondo.d.f)var2, var3, var4));
      } else if(var5.equals(co.uk.getmondo.d.d.class)) {
         var6 = (bb)var5.cast(c.a(var1, (co.uk.getmondo.d.d)var2, var3, var4));
      } else if(var5.equals(co.uk.getmondo.d.w.class)) {
         var6 = (bb)var5.cast(ao.a(var1, (co.uk.getmondo.d.w)var2, var3, var4));
      } else if(var5.equals(co.uk.getmondo.d.o.class)) {
         var6 = (bb)var5.cast(y.a(var1, (co.uk.getmondo.d.o)var2, var3, var4));
      } else if(var5.equals(co.uk.getmondo.d.g.class)) {
         var6 = (bb)var5.cast(j.a(var1, (co.uk.getmondo.d.g)var2, var3, var4));
      } else if(var5.equals(co.uk.getmondo.payments.a.a.b.class)) {
         var6 = (bb)var5.cast(w.a(var1, (co.uk.getmondo.payments.a.a.b)var2, var3, var4));
      } else {
         if(!var5.equals(co.uk.getmondo.d.v.class)) {
            throw c(var5);
         }

         var6 = (bb)var5.cast(ai.a(var1, (co.uk.getmondo.d.v)var2, var3, var4));
      }

      return var6;
   }

   public bb a(bb var1, int var2, Map var3) {
      Class var4 = var1.getClass().getSuperclass();
      if(var4.equals(co.uk.getmondo.d.p.class)) {
         var1 = (bb)var4.cast(aa.a((co.uk.getmondo.d.p)var1, 0, var2, var3));
      } else if(var4.equals(co.uk.getmondo.d.ag.class)) {
         var1 = (bb)var4.cast(bj.a((co.uk.getmondo.d.ag)var1, 0, var2, var3));
      } else if(var4.equals(co.uk.getmondo.d.l.class)) {
         var1 = (bb)var4.cast(q.a((co.uk.getmondo.d.l)var1, 0, var2, var3));
      } else if(var4.equals(co.uk.getmondo.payments.a.a.e.class)) {
         var1 = (bb)var4.cast(aq.a((co.uk.getmondo.payments.a.a.e)var1, 0, var2, var3));
      } else if(var4.equals(co.uk.getmondo.d.t.class)) {
         var1 = (bb)var4.cast(ae.a((co.uk.getmondo.d.t)var1, 0, var2, var3));
      } else if(var4.equals(co.uk.getmondo.d.aa.class)) {
         var1 = (bb)var4.cast(as.a((co.uk.getmondo.d.aa)var1, 0, var2, var3));
      } else if(var4.equals(co.uk.getmondo.d.n.class)) {
         var1 = (bb)var4.cast(u.a((co.uk.getmondo.d.n)var1, 0, var2, var3));
      } else if(var4.equals(co.uk.getmondo.d.u.class)) {
         var1 = (bb)var4.cast(ag.a((co.uk.getmondo.d.u)var1, 0, var2, var3));
      } else if(var4.equals(co.uk.getmondo.payments.send.data.a.a.class)) {
         var1 = (bb)var4.cast(e.a((co.uk.getmondo.payments.send.data.a.a)var1, 0, var2, var3));
      } else if(var4.equals(co.uk.getmondo.d.m.class)) {
         var1 = (bb)var4.cast(s.a((co.uk.getmondo.d.m)var1, 0, var2, var3));
      } else if(var4.equals(co.uk.getmondo.d.am.class)) {
         var1 = (bb)var4.cast(bo.a((co.uk.getmondo.d.am)var1, 0, var2, var3));
      } else if(var4.equals(co.uk.getmondo.d.b.class)) {
         var1 = (bb)var4.cast(a.a((co.uk.getmondo.d.b)var1, 0, var2, var3));
      } else if(var4.equals(co.uk.getmondo.d.r.class)) {
         var1 = (bb)var4.cast(ac.a((co.uk.getmondo.d.r)var1, 0, var2, var3));
      } else if(var4.equals(co.uk.getmondo.d.aj.class)) {
         var1 = (bb)var4.cast(bm.a((co.uk.getmondo.d.aj)var1, 0, var2, var3));
      } else if(var4.equals(co.uk.getmondo.payments.a.a.a.class)) {
         var1 = (bb)var4.cast(m.a((co.uk.getmondo.payments.a.a.a)var1, 0, var2, var3));
      } else if(var4.equals(co.uk.getmondo.d.f.class)) {
         var1 = (bb)var4.cast(h.a((co.uk.getmondo.d.f)var1, 0, var2, var3));
      } else if(var4.equals(co.uk.getmondo.d.d.class)) {
         var1 = (bb)var4.cast(c.a((co.uk.getmondo.d.d)var1, 0, var2, var3));
      } else if(var4.equals(co.uk.getmondo.d.w.class)) {
         var1 = (bb)var4.cast(ao.a((co.uk.getmondo.d.w)var1, 0, var2, var3));
      } else if(var4.equals(co.uk.getmondo.d.o.class)) {
         var1 = (bb)var4.cast(y.a((co.uk.getmondo.d.o)var1, 0, var2, var3));
      } else if(var4.equals(co.uk.getmondo.d.g.class)) {
         var1 = (bb)var4.cast(j.a((co.uk.getmondo.d.g)var1, 0, var2, var3));
      } else if(var4.equals(co.uk.getmondo.payments.a.a.b.class)) {
         var1 = (bb)var4.cast(w.a((co.uk.getmondo.payments.a.a.b)var1, 0, var2, var3));
      } else {
         if(!var4.equals(co.uk.getmondo.d.v.class)) {
            throw c(var4);
         }

         var1 = (bb)var4.cast(ai.a((co.uk.getmondo.d.v)var1, 0, var2, var3));
      }

      return var1;
   }

   public bb a(Class var1, Object var2, io.realm.internal.n var3, io.realm.internal.c var4, boolean var5, List var6) {
      g.b var7 = (g.b)g.g.get();

      bb var10;
      try {
         var7.a((g)var2, var3, var4, var5, var6);
         b(var1);
         if(var1.equals(co.uk.getmondo.d.p.class)) {
            aa var11 = new aa();
            var10 = (bb)var1.cast(var11);
         } else if(var1.equals(co.uk.getmondo.d.ag.class)) {
            bj var12 = new bj();
            var10 = (bb)var1.cast(var12);
         } else if(var1.equals(co.uk.getmondo.d.l.class)) {
            q var13 = new q();
            var10 = (bb)var1.cast(var13);
         } else if(var1.equals(co.uk.getmondo.payments.a.a.e.class)) {
            aq var14 = new aq();
            var10 = (bb)var1.cast(var14);
         } else if(var1.equals(co.uk.getmondo.d.t.class)) {
            ae var15 = new ae();
            var10 = (bb)var1.cast(var15);
         } else if(var1.equals(co.uk.getmondo.d.aa.class)) {
            as var16 = new as();
            var10 = (bb)var1.cast(var16);
         } else if(var1.equals(co.uk.getmondo.d.n.class)) {
            u var17 = new u();
            var10 = (bb)var1.cast(var17);
         } else if(var1.equals(co.uk.getmondo.d.u.class)) {
            ag var18 = new ag();
            var10 = (bb)var1.cast(var18);
         } else if(var1.equals(co.uk.getmondo.payments.send.data.a.a.class)) {
            e var19 = new e();
            var10 = (bb)var1.cast(var19);
         } else if(var1.equals(co.uk.getmondo.d.m.class)) {
            s var20 = new s();
            var10 = (bb)var1.cast(var20);
         } else if(var1.equals(co.uk.getmondo.d.am.class)) {
            bo var21 = new bo();
            var10 = (bb)var1.cast(var21);
         } else if(var1.equals(co.uk.getmondo.d.b.class)) {
            a var22 = new a();
            var10 = (bb)var1.cast(var22);
         } else if(var1.equals(co.uk.getmondo.d.r.class)) {
            ac var23 = new ac();
            var10 = (bb)var1.cast(var23);
         } else if(var1.equals(co.uk.getmondo.d.aj.class)) {
            bm var24 = new bm();
            var10 = (bb)var1.cast(var24);
         } else if(var1.equals(co.uk.getmondo.payments.a.a.a.class)) {
            m var25 = new m();
            var10 = (bb)var1.cast(var25);
         } else if(var1.equals(co.uk.getmondo.d.f.class)) {
            h var26 = new h();
            var10 = (bb)var1.cast(var26);
         } else if(var1.equals(co.uk.getmondo.d.d.class)) {
            c var27 = new c();
            var10 = (bb)var1.cast(var27);
         } else if(var1.equals(co.uk.getmondo.d.w.class)) {
            ao var28 = new ao();
            var10 = (bb)var1.cast(var28);
         } else if(var1.equals(co.uk.getmondo.d.o.class)) {
            y var29 = new y();
            var10 = (bb)var1.cast(var29);
         } else if(var1.equals(co.uk.getmondo.d.g.class)) {
            j var30 = new j();
            var10 = (bb)var1.cast(var30);
         } else if(var1.equals(co.uk.getmondo.payments.a.a.b.class)) {
            w var31 = new w();
            var10 = (bb)var1.cast(var31);
         } else {
            if(!var1.equals(co.uk.getmondo.d.v.class)) {
               throw c(var1);
            }

            ai var32 = new ai();
            var10 = (bb)var1.cast(var32);
         }
      } finally {
         var7.f();
      }

      return var10;
   }

   public io.realm.internal.c a(Class var1, SharedRealm var2, boolean var3) {
      b(var1);
      Object var4;
      if(var1.equals(co.uk.getmondo.d.p.class)) {
         var4 = aa.a(var2, var3);
      } else if(var1.equals(co.uk.getmondo.d.ag.class)) {
         var4 = bj.a(var2, var3);
      } else if(var1.equals(co.uk.getmondo.d.l.class)) {
         var4 = q.a(var2, var3);
      } else if(var1.equals(co.uk.getmondo.payments.a.a.e.class)) {
         var4 = aq.a(var2, var3);
      } else if(var1.equals(co.uk.getmondo.d.t.class)) {
         var4 = ae.a(var2, var3);
      } else if(var1.equals(co.uk.getmondo.d.aa.class)) {
         var4 = as.a(var2, var3);
      } else if(var1.equals(co.uk.getmondo.d.n.class)) {
         var4 = u.a(var2, var3);
      } else if(var1.equals(co.uk.getmondo.d.u.class)) {
         var4 = ag.a(var2, var3);
      } else if(var1.equals(co.uk.getmondo.payments.send.data.a.a.class)) {
         var4 = e.a(var2, var3);
      } else if(var1.equals(co.uk.getmondo.d.m.class)) {
         var4 = s.a(var2, var3);
      } else if(var1.equals(co.uk.getmondo.d.am.class)) {
         var4 = bo.a(var2, var3);
      } else if(var1.equals(co.uk.getmondo.d.b.class)) {
         var4 = a.a(var2, var3);
      } else if(var1.equals(co.uk.getmondo.d.r.class)) {
         var4 = ac.a(var2, var3);
      } else if(var1.equals(co.uk.getmondo.d.aj.class)) {
         var4 = bm.a(var2, var3);
      } else if(var1.equals(co.uk.getmondo.payments.a.a.a.class)) {
         var4 = m.a(var2, var3);
      } else if(var1.equals(co.uk.getmondo.d.f.class)) {
         var4 = h.a(var2, var3);
      } else if(var1.equals(co.uk.getmondo.d.d.class)) {
         var4 = c.a(var2, var3);
      } else if(var1.equals(co.uk.getmondo.d.w.class)) {
         var4 = ao.a(var2, var3);
      } else if(var1.equals(co.uk.getmondo.d.o.class)) {
         var4 = y.a(var2, var3);
      } else if(var1.equals(co.uk.getmondo.d.g.class)) {
         var4 = j.a(var2, var3);
      } else if(var1.equals(co.uk.getmondo.payments.a.a.b.class)) {
         var4 = w.a(var2, var3);
      } else {
         if(!var1.equals(co.uk.getmondo.d.v.class)) {
            throw c(var1);
         }

         var4 = ai.a(var2, var3);
      }

      return (io.realm.internal.c)var4;
   }

   public String a(Class var1) {
      b(var1);
      String var2;
      if(var1.equals(co.uk.getmondo.d.p.class)) {
         var2 = aa.i();
      } else if(var1.equals(co.uk.getmondo.d.ag.class)) {
         var2 = bj.d();
      } else if(var1.equals(co.uk.getmondo.d.l.class)) {
         var2 = q.e();
      } else if(var1.equals(co.uk.getmondo.payments.a.a.e.class)) {
         var2 = aq.q();
      } else if(var1.equals(co.uk.getmondo.d.t.class)) {
         var2 = ae.e();
      } else if(var1.equals(co.uk.getmondo.d.aa.class)) {
         var2 = as.r();
      } else if(var1.equals(co.uk.getmondo.d.n.class)) {
         var2 = u.d();
      } else if(var1.equals(co.uk.getmondo.d.u.class)) {
         var2 = ag.y();
      } else if(var1.equals(co.uk.getmondo.payments.send.data.a.a.class)) {
         var2 = e.i();
      } else if(var1.equals(co.uk.getmondo.d.m.class)) {
         var2 = s.y();
      } else if(var1.equals(co.uk.getmondo.d.am.class)) {
         var2 = bo.l();
      } else if(var1.equals(co.uk.getmondo.d.b.class)) {
         var2 = a.m();
      } else if(var1.equals(co.uk.getmondo.d.r.class)) {
         var2 = ac.g();
      } else if(var1.equals(co.uk.getmondo.d.aj.class)) {
         var2 = bm.ai();
      } else if(var1.equals(co.uk.getmondo.payments.a.a.a.class)) {
         var2 = m.q();
      } else if(var1.equals(co.uk.getmondo.d.f.class)) {
         var2 = h.i();
      } else if(var1.equals(co.uk.getmondo.d.d.class)) {
         var2 = c.h();
      } else if(var1.equals(co.uk.getmondo.d.w.class)) {
         var2 = ao.u();
      } else if(var1.equals(co.uk.getmondo.d.o.class)) {
         var2 = y.d();
      } else if(var1.equals(co.uk.getmondo.d.g.class)) {
         var2 = j.q();
      } else if(var1.equals(co.uk.getmondo.payments.a.a.b.class)) {
         var2 = w.l();
      } else {
         if(!var1.equals(co.uk.getmondo.d.v.class)) {
            throw c(var1);
         }

         var2 = ai.h();
      }

      return var2;
   }

   public Map a() {
      HashMap var1 = new HashMap();
      var1.put(co.uk.getmondo.d.p.class, aa.h());
      var1.put(co.uk.getmondo.d.ag.class, bj.c());
      var1.put(co.uk.getmondo.d.l.class, q.d());
      var1.put(co.uk.getmondo.payments.a.a.e.class, aq.p());
      var1.put(co.uk.getmondo.d.t.class, ae.d());
      var1.put(co.uk.getmondo.d.aa.class, as.q());
      var1.put(co.uk.getmondo.d.n.class, u.c());
      var1.put(co.uk.getmondo.d.u.class, ag.x());
      var1.put(co.uk.getmondo.payments.send.data.a.a.class, e.h());
      var1.put(co.uk.getmondo.d.m.class, s.x());
      var1.put(co.uk.getmondo.d.am.class, bo.j());
      var1.put(co.uk.getmondo.d.b.class, a.l());
      var1.put(co.uk.getmondo.d.r.class, ac.f());
      var1.put(co.uk.getmondo.d.aj.class, bm.ah());
      var1.put(co.uk.getmondo.payments.a.a.a.class, m.p());
      var1.put(co.uk.getmondo.d.f.class, h.h());
      var1.put(co.uk.getmondo.d.d.class, c.g());
      var1.put(co.uk.getmondo.d.w.class, ao.t());
      var1.put(co.uk.getmondo.d.o.class, y.c());
      var1.put(co.uk.getmondo.d.g.class, j.p());
      var1.put(co.uk.getmondo.payments.a.a.b.class, w.j());
      var1.put(co.uk.getmondo.d.v.class, ai.g());
      return var1;
   }

   public void a(av var1, bb var2, Map var3) {
      Class var4;
      if(var2 instanceof io.realm.internal.l) {
         var4 = var2.getClass().getSuperclass();
      } else {
         var4 = var2.getClass();
      }

      if(var4.equals(co.uk.getmondo.d.p.class)) {
         aa.a(var1, (co.uk.getmondo.d.p)var2, var3);
      } else if(var4.equals(co.uk.getmondo.d.ag.class)) {
         bj.a(var1, (co.uk.getmondo.d.ag)var2, var3);
      } else if(var4.equals(co.uk.getmondo.d.l.class)) {
         q.a(var1, (co.uk.getmondo.d.l)var2, var3);
      } else if(var4.equals(co.uk.getmondo.payments.a.a.e.class)) {
         aq.a(var1, (co.uk.getmondo.payments.a.a.e)var2, var3);
      } else if(var4.equals(co.uk.getmondo.d.t.class)) {
         ae.a(var1, (co.uk.getmondo.d.t)var2, var3);
      } else if(var4.equals(co.uk.getmondo.d.aa.class)) {
         as.a(var1, (co.uk.getmondo.d.aa)var2, var3);
      } else if(var4.equals(co.uk.getmondo.d.n.class)) {
         u.a(var1, (co.uk.getmondo.d.n)var2, var3);
      } else if(var4.equals(co.uk.getmondo.d.u.class)) {
         ag.a(var1, (co.uk.getmondo.d.u)var2, var3);
      } else if(var4.equals(co.uk.getmondo.payments.send.data.a.a.class)) {
         e.a(var1, (co.uk.getmondo.payments.send.data.a.a)var2, var3);
      } else if(var4.equals(co.uk.getmondo.d.m.class)) {
         s.a(var1, (co.uk.getmondo.d.m)var2, var3);
      } else if(var4.equals(co.uk.getmondo.d.am.class)) {
         bo.a(var1, (co.uk.getmondo.d.am)var2, var3);
      } else if(var4.equals(co.uk.getmondo.d.b.class)) {
         a.a(var1, (co.uk.getmondo.d.b)var2, var3);
      } else if(var4.equals(co.uk.getmondo.d.r.class)) {
         ac.a(var1, (co.uk.getmondo.d.r)var2, var3);
      } else if(var4.equals(co.uk.getmondo.d.aj.class)) {
         bm.a(var1, (co.uk.getmondo.d.aj)var2, var3);
      } else if(var4.equals(co.uk.getmondo.payments.a.a.a.class)) {
         m.a(var1, (co.uk.getmondo.payments.a.a.a)var2, var3);
      } else if(var4.equals(co.uk.getmondo.d.f.class)) {
         h.a(var1, (co.uk.getmondo.d.f)var2, var3);
      } else if(var4.equals(co.uk.getmondo.d.d.class)) {
         c.a(var1, (co.uk.getmondo.d.d)var2, var3);
      } else if(var4.equals(co.uk.getmondo.d.w.class)) {
         ao.a(var1, (co.uk.getmondo.d.w)var2, var3);
      } else if(var4.equals(co.uk.getmondo.d.o.class)) {
         y.a(var1, (co.uk.getmondo.d.o)var2, var3);
      } else if(var4.equals(co.uk.getmondo.d.g.class)) {
         j.a(var1, (co.uk.getmondo.d.g)var2, var3);
      } else if(var4.equals(co.uk.getmondo.payments.a.a.b.class)) {
         w.a(var1, (co.uk.getmondo.payments.a.a.b)var2, var3);
      } else {
         if(!var4.equals(co.uk.getmondo.d.v.class)) {
            throw c(var4);
         }

         ai.a(var1, (co.uk.getmondo.d.v)var2, var3);
      }

   }

   public void a(av var1, Collection var2) {
      Iterator var3 = var2.iterator();
      HashMap var4 = new HashMap(var2.size());
      if(var3.hasNext()) {
         bb var5 = (bb)var3.next();
         Class var6;
         if(var5 instanceof io.realm.internal.l) {
            var6 = var5.getClass().getSuperclass();
         } else {
            var6 = var5.getClass();
         }

         if(var6.equals(co.uk.getmondo.d.p.class)) {
            aa.b(var1, (co.uk.getmondo.d.p)var5, var4);
         } else if(var6.equals(co.uk.getmondo.d.ag.class)) {
            bj.b(var1, (co.uk.getmondo.d.ag)var5, var4);
         } else if(var6.equals(co.uk.getmondo.d.l.class)) {
            q.b(var1, (co.uk.getmondo.d.l)var5, var4);
         } else if(var6.equals(co.uk.getmondo.payments.a.a.e.class)) {
            aq.b(var1, (co.uk.getmondo.payments.a.a.e)var5, var4);
         } else if(var6.equals(co.uk.getmondo.d.t.class)) {
            ae.b(var1, (co.uk.getmondo.d.t)var5, var4);
         } else if(var6.equals(co.uk.getmondo.d.aa.class)) {
            as.b(var1, (co.uk.getmondo.d.aa)var5, var4);
         } else if(var6.equals(co.uk.getmondo.d.n.class)) {
            u.b(var1, (co.uk.getmondo.d.n)var5, var4);
         } else if(var6.equals(co.uk.getmondo.d.u.class)) {
            ag.b(var1, (co.uk.getmondo.d.u)var5, var4);
         } else if(var6.equals(co.uk.getmondo.payments.send.data.a.a.class)) {
            e.b(var1, (co.uk.getmondo.payments.send.data.a.a)var5, var4);
         } else if(var6.equals(co.uk.getmondo.d.m.class)) {
            s.b(var1, (co.uk.getmondo.d.m)var5, var4);
         } else if(var6.equals(co.uk.getmondo.d.am.class)) {
            bo.b(var1, (co.uk.getmondo.d.am)var5, var4);
         } else if(var6.equals(co.uk.getmondo.d.b.class)) {
            a.b(var1, (co.uk.getmondo.d.b)var5, var4);
         } else if(var6.equals(co.uk.getmondo.d.r.class)) {
            ac.b(var1, (co.uk.getmondo.d.r)var5, var4);
         } else if(var6.equals(co.uk.getmondo.d.aj.class)) {
            bm.b(var1, (co.uk.getmondo.d.aj)var5, var4);
         } else if(var6.equals(co.uk.getmondo.payments.a.a.a.class)) {
            m.b(var1, (co.uk.getmondo.payments.a.a.a)var5, var4);
         } else if(var6.equals(co.uk.getmondo.d.f.class)) {
            h.b(var1, (co.uk.getmondo.d.f)var5, var4);
         } else if(var6.equals(co.uk.getmondo.d.d.class)) {
            c.b(var1, (co.uk.getmondo.d.d)var5, var4);
         } else if(var6.equals(co.uk.getmondo.d.w.class)) {
            ao.b(var1, (co.uk.getmondo.d.w)var5, var4);
         } else if(var6.equals(co.uk.getmondo.d.o.class)) {
            y.b(var1, (co.uk.getmondo.d.o)var5, var4);
         } else if(var6.equals(co.uk.getmondo.d.g.class)) {
            j.b(var1, (co.uk.getmondo.d.g)var5, var4);
         } else if(var6.equals(co.uk.getmondo.payments.a.a.b.class)) {
            w.b(var1, (co.uk.getmondo.payments.a.a.b)var5, var4);
         } else {
            if(!var6.equals(co.uk.getmondo.d.v.class)) {
               throw c(var6);
            }

            ai.b(var1, (co.uk.getmondo.d.v)var5, var4);
         }

         if(var3.hasNext()) {
            if(var6.equals(co.uk.getmondo.d.p.class)) {
               aa.a(var1, (Iterator)var3, var4);
            } else if(var6.equals(co.uk.getmondo.d.ag.class)) {
               bj.a(var1, (Iterator)var3, var4);
            } else if(var6.equals(co.uk.getmondo.d.l.class)) {
               q.a(var1, (Iterator)var3, var4);
            } else if(var6.equals(co.uk.getmondo.payments.a.a.e.class)) {
               aq.a(var1, (Iterator)var3, var4);
            } else if(var6.equals(co.uk.getmondo.d.t.class)) {
               ae.a(var1, (Iterator)var3, var4);
            } else if(var6.equals(co.uk.getmondo.d.aa.class)) {
               as.a(var1, (Iterator)var3, var4);
            } else if(var6.equals(co.uk.getmondo.d.n.class)) {
               u.a(var1, (Iterator)var3, var4);
            } else if(var6.equals(co.uk.getmondo.d.u.class)) {
               ag.a(var1, (Iterator)var3, var4);
            } else if(var6.equals(co.uk.getmondo.payments.send.data.a.a.class)) {
               e.a(var1, (Iterator)var3, var4);
            } else if(var6.equals(co.uk.getmondo.d.m.class)) {
               s.a(var1, (Iterator)var3, var4);
            } else if(var6.equals(co.uk.getmondo.d.am.class)) {
               bo.a(var1, (Iterator)var3, var4);
            } else if(var6.equals(co.uk.getmondo.d.b.class)) {
               a.a(var1, (Iterator)var3, var4);
            } else if(var6.equals(co.uk.getmondo.d.r.class)) {
               ac.a(var1, (Iterator)var3, var4);
            } else if(var6.equals(co.uk.getmondo.d.aj.class)) {
               bm.a(var1, (Iterator)var3, var4);
            } else if(var6.equals(co.uk.getmondo.payments.a.a.a.class)) {
               m.a(var1, (Iterator)var3, var4);
            } else if(var6.equals(co.uk.getmondo.d.f.class)) {
               h.a(var1, (Iterator)var3, var4);
            } else if(var6.equals(co.uk.getmondo.d.d.class)) {
               c.a(var1, (Iterator)var3, var4);
            } else if(var6.equals(co.uk.getmondo.d.w.class)) {
               ao.a(var1, (Iterator)var3, var4);
            } else if(var6.equals(co.uk.getmondo.d.o.class)) {
               y.a(var1, (Iterator)var3, var4);
            } else if(var6.equals(co.uk.getmondo.d.g.class)) {
               j.a(var1, (Iterator)var3, var4);
            } else if(var6.equals(co.uk.getmondo.payments.a.a.b.class)) {
               w.a(var1, (Iterator)var3, var4);
            } else {
               if(!var6.equals(co.uk.getmondo.d.v.class)) {
                  throw c(var6);
               }

               ai.a(var1, (Iterator)var3, var4);
            }
         }
      }

   }

   public Set b() {
      return a;
   }

   public void b(av var1, bb var2, Map var3) {
      Class var4;
      if(var2 instanceof io.realm.internal.l) {
         var4 = var2.getClass().getSuperclass();
      } else {
         var4 = var2.getClass();
      }

      if(var4.equals(co.uk.getmondo.d.p.class)) {
         aa.b(var1, (co.uk.getmondo.d.p)var2, var3);
      } else if(var4.equals(co.uk.getmondo.d.ag.class)) {
         bj.b(var1, (co.uk.getmondo.d.ag)var2, var3);
      } else if(var4.equals(co.uk.getmondo.d.l.class)) {
         q.b(var1, (co.uk.getmondo.d.l)var2, var3);
      } else if(var4.equals(co.uk.getmondo.payments.a.a.e.class)) {
         aq.b(var1, (co.uk.getmondo.payments.a.a.e)var2, var3);
      } else if(var4.equals(co.uk.getmondo.d.t.class)) {
         ae.b(var1, (co.uk.getmondo.d.t)var2, var3);
      } else if(var4.equals(co.uk.getmondo.d.aa.class)) {
         as.b(var1, (co.uk.getmondo.d.aa)var2, var3);
      } else if(var4.equals(co.uk.getmondo.d.n.class)) {
         u.b(var1, (co.uk.getmondo.d.n)var2, var3);
      } else if(var4.equals(co.uk.getmondo.d.u.class)) {
         ag.b(var1, (co.uk.getmondo.d.u)var2, var3);
      } else if(var4.equals(co.uk.getmondo.payments.send.data.a.a.class)) {
         e.b(var1, (co.uk.getmondo.payments.send.data.a.a)var2, var3);
      } else if(var4.equals(co.uk.getmondo.d.m.class)) {
         s.b(var1, (co.uk.getmondo.d.m)var2, var3);
      } else if(var4.equals(co.uk.getmondo.d.am.class)) {
         bo.b(var1, (co.uk.getmondo.d.am)var2, var3);
      } else if(var4.equals(co.uk.getmondo.d.b.class)) {
         a.b(var1, (co.uk.getmondo.d.b)var2, var3);
      } else if(var4.equals(co.uk.getmondo.d.r.class)) {
         ac.b(var1, (co.uk.getmondo.d.r)var2, var3);
      } else if(var4.equals(co.uk.getmondo.d.aj.class)) {
         bm.b(var1, (co.uk.getmondo.d.aj)var2, var3);
      } else if(var4.equals(co.uk.getmondo.payments.a.a.a.class)) {
         m.b(var1, (co.uk.getmondo.payments.a.a.a)var2, var3);
      } else if(var4.equals(co.uk.getmondo.d.f.class)) {
         h.b(var1, (co.uk.getmondo.d.f)var2, var3);
      } else if(var4.equals(co.uk.getmondo.d.d.class)) {
         c.b(var1, (co.uk.getmondo.d.d)var2, var3);
      } else if(var4.equals(co.uk.getmondo.d.w.class)) {
         ao.b(var1, (co.uk.getmondo.d.w)var2, var3);
      } else if(var4.equals(co.uk.getmondo.d.o.class)) {
         y.b(var1, (co.uk.getmondo.d.o)var2, var3);
      } else if(var4.equals(co.uk.getmondo.d.g.class)) {
         j.b(var1, (co.uk.getmondo.d.g)var2, var3);
      } else if(var4.equals(co.uk.getmondo.payments.a.a.b.class)) {
         w.b(var1, (co.uk.getmondo.payments.a.a.b)var2, var3);
      } else {
         if(!var4.equals(co.uk.getmondo.d.v.class)) {
            throw c(var4);
         }

         ai.b(var1, (co.uk.getmondo.d.v)var2, var3);
      }

   }

   public boolean c() {
      return true;
   }
}
