package io.realm;

import io.realm.internal.Table;
import io.realm.internal.Util;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

public class bh {
   private final Map a = new HashMap();
   private final Map b = new HashMap();
   private final Map c = new HashMap();
   private final Map d = new HashMap();
   private final g e;
   private io.realm.internal.b f;

   bh(g var1) {
      this.e = var1;
   }

   private void f() {
      if(!this.d()) {
         throw new IllegalStateException("Attempt to use column index before set.");
      }
   }

   Table a(Class var1) {
      Table var3 = (Table)this.b.get(var1);
      if(var3 == null) {
         Class var4 = Util.a(var1);
         if(this.a(var4, var1)) {
            var3 = (Table)this.b.get(var4);
         }

         Table var2 = var3;
         if(var3 == null) {
            var2 = this.e.m().b(this.e.h().h().a(var4));
            this.b.put(var4, var2);
         }

         var3 = var2;
         if(this.a(var4, var1)) {
            this.b.put(var1, var2);
            var3 = var2;
         }
      }

      return var3;
   }

   Table a(String var1) {
      String var2 = Table.d(var1);
      Table var3 = (Table)this.a.get(var2);
      if(var3 == null) {
         var3 = this.e.m().b(var2);
         this.a.put(var2, var3);
      }

      return var3;
   }

   @Deprecated
   public void a() {
   }

   final void a(long var1, Map var3) {
      if(this.f != null) {
         throw new IllegalStateException("An instance of ColumnIndices is already set.");
      } else {
         this.f = new io.realm.internal.b(var1, var3);
      }
   }

   final void a(io.realm.internal.b var1) {
      if(this.f != null) {
         throw new IllegalStateException("An instance of ColumnIndices is already set.");
      } else {
         this.f = new io.realm.internal.b(var1, true);
      }
   }

   final boolean a(Class var1, Class var2) {
      return var1.equals(var2);
   }

   be b(Class var1) {
      be var3 = (be)this.c.get(var1);
      if(var3 == null) {
         Class var4 = Util.a(var1);
         if(this.a(var4, var1)) {
            var3 = (be)this.c.get(var4);
         }

         be var2 = var3;
         if(var3 == null) {
            Table var5 = this.a(var1);
            var2 = new be(this.e, this, var5, this.c(var4));
            this.c.put(var4, var2);
         }

         var3 = var2;
         if(this.a(var4, var1)) {
            this.c.put(var1, var2);
            var3 = var2;
         }
      }

      return var3;
   }

   protected final io.realm.internal.c b(String var1) {
      this.f();
      return this.f.a(var1);
   }

   public Set b() {
      int var2 = (int)this.e.m().g();
      LinkedHashSet var3 = new LinkedHashSet(var2);

      for(int var1 = 0; var1 < var2; ++var1) {
         String var4 = this.e.m().a(var1);
         if(Table.b(var4)) {
            var3.add(new be(this.e, this, this.e.m().b(var4)));
         }
      }

      return var3;
   }

   void b(io.realm.internal.b var1) {
      this.f.a(var1);
   }

   final io.realm.internal.b c() {
      this.f();
      return new io.realm.internal.b(this.f, false);
   }

   final io.realm.internal.c c(Class var1) {
      this.f();
      return this.f.a(var1);
   }

   final boolean d() {
      boolean var1;
      if(this.f != null) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   final long e() {
      this.f();
      return this.f.a();
   }
}
