package io.realm;

import io.realm.exceptions.RealmException;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.SharedRealm;
import io.realm.internal.Table;
import io.realm.log.RealmLog;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class ae extends co.uk.getmondo.d.t implements af, io.realm.internal.l {
   private static final OsObjectSchemaInfo c = f();
   private static final List d;
   private ae.a a;
   private au b;

   static {
      ArrayList var0 = new ArrayList();
      var0.add("currency");
      var0.add("amountValue");
      d = Collections.unmodifiableList(var0);
   }

   ae() {
      this.b.f();
   }

   public static long a(av var0, co.uk.getmondo.d.t var1, Map var2) {
      long var3;
      if(var1 instanceof io.realm.internal.l && ((io.realm.internal.l)var1).s_().a() != null && ((io.realm.internal.l)var1).s_().a().g().equals(var0.g())) {
         var3 = ((io.realm.internal.l)var1).s_().b().c();
      } else {
         Table var7 = var0.c(co.uk.getmondo.d.t.class);
         long var5 = var7.getNativePtr();
         ae.a var8 = (ae.a)var0.f.c(co.uk.getmondo.d.t.class);
         var3 = var7.d();
         String var9 = ((af)var1).b();
         if(var9 == null) {
            var3 = Table.nativeFindFirstNull(var5, var3);
         } else {
            var3 = Table.nativeFindFirstString(var5, var3, var9);
         }

         if(var3 == -1L) {
            var3 = OsObject.b(var7, var9);
         } else {
            Table.a((Object)var9);
         }

         var2.put(var1, Long.valueOf(var3));
         Table.nativeSetLong(var5, var8.b, var3, ((af)var1).c(), false);
      }

      return var3;
   }

   public static co.uk.getmondo.d.t a(co.uk.getmondo.d.t var0, int var1, int var2, Map var3) {
      if(var1 <= var2 && var0 != null) {
         io.realm.internal.l.a var4 = (io.realm.internal.l.a)var3.get(var0);
         co.uk.getmondo.d.t var6;
         if(var4 == null) {
            co.uk.getmondo.d.t var7 = new co.uk.getmondo.d.t();
            var3.put(var0, new io.realm.internal.l.a(var1, var7));
            var6 = var7;
         } else {
            if(var1 >= var4.a) {
               var0 = (co.uk.getmondo.d.t)var4.b;
               return var0;
            }

            var6 = (co.uk.getmondo.d.t)var4.b;
            var4.a = var1;
         }

         af var8 = (af)var6;
         af var5 = (af)var0;
         var8.a(var5.b());
         var8.a(var5.c());
         var0 = var6;
      } else {
         var0 = null;
      }

      return var0;
   }

   static co.uk.getmondo.d.t a(av var0, co.uk.getmondo.d.t var1, co.uk.getmondo.d.t var2, Map var3) {
      ((af)var1).a(((af)var2).c());
      return var1;
   }

   public static co.uk.getmondo.d.t a(av var0, co.uk.getmondo.d.t var1, boolean var2, Map var3) {
      if(var1 instanceof io.realm.internal.l && ((io.realm.internal.l)var1).s_().a() != null && ((io.realm.internal.l)var1).s_().a().c != var0.c) {
         throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
      } else {
         if(!(var1 instanceof io.realm.internal.l) || ((io.realm.internal.l)var1).s_().a() == null || !((io.realm.internal.l)var1).s_().a().g().equals(var0.g())) {
            g.b var8 = (g.b)g.g.get();
            io.realm.internal.l var7 = (io.realm.internal.l)var3.get(var1);
            if(var7 != null) {
               var1 = (co.uk.getmondo.d.t)var7;
            } else {
               boolean var6;
               ae var13;
               if(var2) {
                  Table var12 = var0.c(co.uk.getmondo.d.t.class);
                  long var4 = var12.d();
                  String var9 = ((af)var1).b();
                  if(var9 == null) {
                     var4 = var12.k(var4);
                  } else {
                     var4 = var12.a(var4, var9);
                  }

                  if(var4 != -1L) {
                     try {
                        var8.a(var0, var12.f(var4), var0.f.c(co.uk.getmondo.d.t.class), false, Collections.emptyList());
                        var13 = new ae();
                        var3.put(var1, (io.realm.internal.l)var13);
                     } finally {
                        var8.f();
                     }

                     var6 = var2;
                  } else {
                     var6 = false;
                     var13 = null;
                  }
               } else {
                  var6 = var2;
                  var13 = null;
               }

               if(var6) {
                  var1 = a(var0, var13, var1, var3);
               } else {
                  var1 = b(var0, var1, var2, var3);
               }
            }
         }

         return var1;
      }
   }

   public static ae.a a(SharedRealm var0, boolean var1) {
      if(!var0.a("class_LocalSpend")) {
         throw new RealmMigrationNeededException(var0.h(), "The 'LocalSpend' class is missing from the schema for this Realm.");
      } else {
         Table var7 = var0.b("class_LocalSpend");
         long var4 = var7.c();
         if(var4 != 2L) {
            if(var4 < 2L) {
               throw new RealmMigrationNeededException(var0.h(), "Field count is less than expected - expected 2 but was " + var4);
            }

            if(!var1) {
               throw new RealmMigrationNeededException(var0.h(), "Field count is more than expected - expected 2 but was " + var4);
            }

            RealmLog.a("Field count is more than expected - expected 2 but was %1$d", new Object[]{Long.valueOf(var4)});
         }

         HashMap var6 = new HashMap();

         for(long var2 = 0L; var2 < var4; ++var2) {
            var6.put(var7.b(var2), var7.c(var2));
         }

         ae.a var8 = new ae.a(var0, var7);
         if(!var7.e()) {
            throw new RealmMigrationNeededException(var0.h(), "Primary key not defined for field 'currency' in existing Realm file. @PrimaryKey was added.");
         } else if(var7.d() != var8.a) {
            throw new RealmMigrationNeededException(var0.h(), "Primary Key annotation definition was changed, from field " + var7.b(var7.d()) + " to field currency");
         } else if(!var6.containsKey("currency")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'currency' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var6.get("currency") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'currency' in existing Realm file.");
         } else if(!var7.a(var8.a)) {
            throw new RealmMigrationNeededException(var0.h(), "@PrimaryKey field 'currency' does not support null values in the existing Realm file. Migrate using RealmObjectSchema.setNullable(), or mark the field as @Required.");
         } else if(!var7.j(var7.a("currency"))) {
            throw new RealmMigrationNeededException(var0.h(), "Index not defined for field 'currency' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex().");
         } else if(!var6.containsKey("amountValue")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'amountValue' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var6.get("amountValue") != RealmFieldType.INTEGER) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'long' for field 'amountValue' in existing Realm file.");
         } else if(var7.a(var8.b)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'amountValue' does support null values in the existing Realm file. Use corresponding boxed type for field 'amountValue' or migrate using RealmObjectSchema.setNullable().");
         } else {
            return var8;
         }
      }
   }

   public static void a(av var0, Iterator var1, Map var2) {
      Table var14 = var0.c(co.uk.getmondo.d.t.class);
      long var7 = var14.getNativePtr();
      ae.a var12 = (ae.a)var0.f.c(co.uk.getmondo.d.t.class);
      long var9 = var14.d();

      while(true) {
         while(true) {
            co.uk.getmondo.d.t var11;
            do {
               if(!var1.hasNext()) {
                  return;
               }

               var11 = (co.uk.getmondo.d.t)var1.next();
            } while(var2.containsKey(var11));

            if(var11 instanceof io.realm.internal.l && ((io.realm.internal.l)var11).s_().a() != null && ((io.realm.internal.l)var11).s_().a().g().equals(var0.g())) {
               var2.put(var11, Long.valueOf(((io.realm.internal.l)var11).s_().b().c()));
            } else {
               String var13 = ((af)var11).b();
               long var3;
               if(var13 == null) {
                  var3 = Table.nativeFindFirstNull(var7, var9);
               } else {
                  var3 = Table.nativeFindFirstString(var7, var9, var13);
               }

               long var5 = var3;
               if(var3 == -1L) {
                  var5 = OsObject.b(var14, var13);
               }

               var2.put(var11, Long.valueOf(var5));
               Table.nativeSetLong(var7, var12.b, var5, ((af)var11).c(), false);
            }
         }
      }
   }

   public static long b(av var0, co.uk.getmondo.d.t var1, Map var2) {
      long var5;
      if(var1 instanceof io.realm.internal.l && ((io.realm.internal.l)var1).s_().a() != null && ((io.realm.internal.l)var1).s_().a().g().equals(var0.g())) {
         var5 = ((io.realm.internal.l)var1).s_().b().c();
      } else {
         Table var9 = var0.c(co.uk.getmondo.d.t.class);
         long var7 = var9.getNativePtr();
         ae.a var11 = (ae.a)var0.f.c(co.uk.getmondo.d.t.class);
         long var3 = var9.d();
         String var10 = ((af)var1).b();
         if(var10 == null) {
            var3 = Table.nativeFindFirstNull(var7, var3);
         } else {
            var3 = Table.nativeFindFirstString(var7, var3, var10);
         }

         var5 = var3;
         if(var3 == -1L) {
            var5 = OsObject.b(var9, var10);
         }

         var2.put(var1, Long.valueOf(var5));
         Table.nativeSetLong(var7, var11.b, var5, ((af)var1).c(), false);
      }

      return var5;
   }

   public static co.uk.getmondo.d.t b(av var0, co.uk.getmondo.d.t var1, boolean var2, Map var3) {
      io.realm.internal.l var4 = (io.realm.internal.l)var3.get(var1);
      co.uk.getmondo.d.t var5;
      if(var4 != null) {
         var5 = (co.uk.getmondo.d.t)var4;
      } else {
         var5 = (co.uk.getmondo.d.t)var0.a(co.uk.getmondo.d.t.class, ((af)var1).b(), false, Collections.emptyList());
         var3.put(var1, (io.realm.internal.l)var5);
         af var6 = (af)var1;
         ((af)var5).a(var6.c());
      }

      return var5;
   }

   public static OsObjectSchemaInfo d() {
      return c;
   }

   public static String e() {
      return "class_LocalSpend";
   }

   private static OsObjectSchemaInfo f() {
      OsObjectSchemaInfo.a var0 = new OsObjectSchemaInfo.a("LocalSpend");
      var0.a("currency", RealmFieldType.STRING, true, true, false);
      var0.a("amountValue", RealmFieldType.INTEGER, false, false, true);
      return var0.a();
   }

   public void a(long var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var3 = this.b.b();
            var3.b().a(this.a.b, var3.c(), var1, true);
         }
      } else {
         this.b.a().e();
         this.b.b().a(this.a.b, var1);
      }

   }

   public void a(String var1) {
      if(!this.b.e()) {
         this.b.a().e();
         throw new RealmException("Primary key field 'currency' cannot be changed after object was created.");
      }
   }

   public String b() {
      this.b.a().e();
      return this.b.b().k(this.a.a);
   }

   public long c() {
      this.b.a().e();
      return this.b.b().f(this.a.b);
   }

   public au s_() {
      return this.b;
   }

   public String toString() {
      String var1;
      if(!bc.b(this)) {
         var1 = "Invalid object";
      } else {
         StringBuilder var2 = new StringBuilder("LocalSpend = proxy[");
         var2.append("{currency:");
         if(this.b() != null) {
            var1 = this.b();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{amountValue:");
         var2.append(this.c());
         var2.append("}");
         var2.append("]");
         var1 = var2.toString();
      }

      return var1;
   }

   public void u_() {
      if(this.b == null) {
         g.b var1 = (g.b)g.g.get();
         this.a = (ae.a)var1.c();
         this.b = new au(this);
         this.b.a(var1.a());
         this.b.a(var1.b());
         this.b.a(var1.d());
         this.b.a(var1.e());
      }

   }

   static final class a extends io.realm.internal.c {
      long a;
      long b;

      a(SharedRealm var1, Table var2) {
         super(2);
         this.a = this.a(var2, "currency", RealmFieldType.STRING);
         this.b = this.a(var2, "amountValue", RealmFieldType.INTEGER);
      }

      a(io.realm.internal.c var1, boolean var2) {
         super(var1, var2);
         this.a(var1, this);
      }

      protected final io.realm.internal.c a(boolean var1) {
         return new ae.a(this, var1);
      }

      protected final void a(io.realm.internal.c var1, io.realm.internal.c var2) {
         ae.a var3 = (ae.a)var1;
         ae.a var4 = (ae.a)var2;
         var4.a = var3.a;
         var4.b = var3.b;
      }
   }
}
