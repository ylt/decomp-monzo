package io.realm;

import io.realm.exceptions.RealmException;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.SharedRealm;
import io.realm.internal.Table;
import io.realm.log.RealmLog;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class ag extends co.uk.getmondo.d.u implements ah, io.realm.internal.l {
   private static final OsObjectSchemaInfo c = z();
   private static final List d;
   private ag.a a;
   private au b;

   static {
      ArrayList var0 = new ArrayList();
      var0.add("id");
      var0.add("groupId");
      var0.add("name");
      var0.add("logoUrl");
      var0.add("emoji");
      var0.add("category");
      var0.add("online");
      var0.add("atm");
      var0.add("formattedAddress");
      var0.add("latitude");
      var0.add("longitude");
      d = Collections.unmodifiableList(var0);
   }

   ag() {
      this.b.f();
   }

   public static long a(av var0, co.uk.getmondo.d.u var1, Map var2) {
      long var5;
      if(var1 instanceof io.realm.internal.l && ((io.realm.internal.l)var1).s_().a() != null && ((io.realm.internal.l)var1).s_().a().g().equals(var0.g())) {
         var5 = ((io.realm.internal.l)var1).s_().b().c();
      } else {
         Table var9 = var0.c(co.uk.getmondo.d.u.class);
         long var7 = var9.getNativePtr();
         ag.a var11 = (ag.a)var0.f.c(co.uk.getmondo.d.u.class);
         long var3 = var9.d();
         String var10 = ((ah)var1).m();
         if(var10 == null) {
            var3 = Table.nativeFindFirstNull(var7, var3);
         } else {
            var3 = Table.nativeFindFirstString(var7, var3, var10);
         }

         if(var3 == -1L) {
            var3 = OsObject.b(var9, var10);
         } else {
            Table.a((Object)var10);
         }

         var2.put(var1, Long.valueOf(var3));
         String var13 = ((ah)var1).n();
         if(var13 != null) {
            Table.nativeSetString(var7, var11.b, var3, var13, false);
         }

         var13 = ((ah)var1).o();
         if(var13 != null) {
            Table.nativeSetString(var7, var11.c, var3, var13, false);
         }

         var13 = ((ah)var1).p();
         if(var13 != null) {
            Table.nativeSetString(var7, var11.d, var3, var13, false);
         }

         var13 = ((ah)var1).q();
         if(var13 != null) {
            Table.nativeSetString(var7, var11.e, var3, var13, false);
         }

         var13 = ((ah)var1).r();
         if(var13 != null) {
            Table.nativeSetString(var7, var11.f, var3, var13, false);
         }

         Table.nativeSetBoolean(var7, var11.g, var3, ((ah)var1).s(), false);
         Table.nativeSetBoolean(var7, var11.h, var3, ((ah)var1).t(), false);
         var13 = ((ah)var1).u();
         if(var13 != null) {
            Table.nativeSetString(var7, var11.i, var3, var13, false);
         }

         Double var14 = ((ah)var1).v();
         if(var14 != null) {
            Table.nativeSetDouble(var7, var11.j, var3, var14.doubleValue(), false);
         }

         Double var12 = ((ah)var1).w();
         var5 = var3;
         if(var12 != null) {
            Table.nativeSetDouble(var7, var11.k, var3, var12.doubleValue(), false);
            var5 = var3;
         }
      }

      return var5;
   }

   public static co.uk.getmondo.d.u a(co.uk.getmondo.d.u var0, int var1, int var2, Map var3) {
      if(var1 <= var2 && var0 != null) {
         io.realm.internal.l.a var4 = (io.realm.internal.l.a)var3.get(var0);
         co.uk.getmondo.d.u var6;
         if(var4 == null) {
            co.uk.getmondo.d.u var7 = new co.uk.getmondo.d.u();
            var3.put(var0, new io.realm.internal.l.a(var1, var7));
            var6 = var7;
         } else {
            if(var1 >= var4.a) {
               var0 = (co.uk.getmondo.d.u)var4.b;
               return var0;
            }

            var6 = (co.uk.getmondo.d.u)var4.b;
            var4.a = var1;
         }

         ah var8 = (ah)var6;
         ah var5 = (ah)var0;
         var8.a(var5.m());
         var8.b(var5.n());
         var8.c(var5.o());
         var8.d(var5.p());
         var8.e(var5.q());
         var8.f(var5.r());
         var8.a(var5.s());
         var8.b(var5.t());
         var8.g(var5.u());
         var8.a(var5.v());
         var8.b(var5.w());
         var0 = var6;
      } else {
         var0 = null;
      }

      return var0;
   }

   static co.uk.getmondo.d.u a(av var0, co.uk.getmondo.d.u var1, co.uk.getmondo.d.u var2, Map var3) {
      ah var4 = (ah)var1;
      ah var5 = (ah)var2;
      var4.b(var5.n());
      var4.c(var5.o());
      var4.d(var5.p());
      var4.e(var5.q());
      var4.f(var5.r());
      var4.a(var5.s());
      var4.b(var5.t());
      var4.g(var5.u());
      var4.a(var5.v());
      var4.b(var5.w());
      return var1;
   }

   public static co.uk.getmondo.d.u a(av var0, co.uk.getmondo.d.u var1, boolean var2, Map var3) {
      if(var1 instanceof io.realm.internal.l && ((io.realm.internal.l)var1).s_().a() != null && ((io.realm.internal.l)var1).s_().a().c != var0.c) {
         throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
      } else {
         if(!(var1 instanceof io.realm.internal.l) || ((io.realm.internal.l)var1).s_().a() == null || !((io.realm.internal.l)var1).s_().a().g().equals(var0.g())) {
            g.b var8 = (g.b)g.g.get();
            io.realm.internal.l var7 = (io.realm.internal.l)var3.get(var1);
            if(var7 != null) {
               var1 = (co.uk.getmondo.d.u)var7;
            } else {
               boolean var4;
               ag var13;
               if(var2) {
                  Table var12 = var0.c(co.uk.getmondo.d.u.class);
                  long var5 = var12.d();
                  String var9 = ((ah)var1).m();
                  if(var9 == null) {
                     var5 = var12.k(var5);
                  } else {
                     var5 = var12.a(var5, var9);
                  }

                  if(var5 != -1L) {
                     try {
                        var8.a(var0, var12.f(var5), var0.f.c(co.uk.getmondo.d.u.class), false, Collections.emptyList());
                        var13 = new ag();
                        var3.put(var1, (io.realm.internal.l)var13);
                     } finally {
                        var8.f();
                     }

                     var4 = var2;
                  } else {
                     var4 = false;
                     var13 = null;
                  }
               } else {
                  var4 = var2;
                  var13 = null;
               }

               if(var4) {
                  var1 = a(var0, var13, var1, var3);
               } else {
                  var1 = b(var0, var1, var2, var3);
               }
            }
         }

         return var1;
      }
   }

   public static ag.a a(SharedRealm var0, boolean var1) {
      if(!var0.a("class_Merchant")) {
         throw new RealmMigrationNeededException(var0.h(), "The 'Merchant' class is missing from the schema for this Realm.");
      } else {
         Table var7 = var0.b("class_Merchant");
         long var4 = var7.c();
         if(var4 != 11L) {
            if(var4 < 11L) {
               throw new RealmMigrationNeededException(var0.h(), "Field count is less than expected - expected 11 but was " + var4);
            }

            if(!var1) {
               throw new RealmMigrationNeededException(var0.h(), "Field count is more than expected - expected 11 but was " + var4);
            }

            RealmLog.a("Field count is more than expected - expected 11 but was %1$d", new Object[]{Long.valueOf(var4)});
         }

         HashMap var8 = new HashMap();

         for(long var2 = 0L; var2 < var4; ++var2) {
            var8.put(var7.b(var2), var7.c(var2));
         }

         ag.a var6 = new ag.a(var0, var7);
         if(!var7.e()) {
            throw new RealmMigrationNeededException(var0.h(), "Primary key not defined for field 'id' in existing Realm file. @PrimaryKey was added.");
         } else if(var7.d() != var6.a) {
            throw new RealmMigrationNeededException(var0.h(), "Primary Key annotation definition was changed, from field " + var7.b(var7.d()) + " to field id");
         } else if(!var8.containsKey("id")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'id' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var8.get("id") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'id' in existing Realm file.");
         } else if(!var7.a(var6.a)) {
            throw new RealmMigrationNeededException(var0.h(), "@PrimaryKey field 'id' does not support null values in the existing Realm file. Migrate using RealmObjectSchema.setNullable(), or mark the field as @Required.");
         } else if(!var7.j(var7.a("id"))) {
            throw new RealmMigrationNeededException(var0.h(), "Index not defined for field 'id' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex().");
         } else if(!var8.containsKey("groupId")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'groupId' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var8.get("groupId") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'groupId' in existing Realm file.");
         } else if(!var7.a(var6.b)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'groupId' is required. Either set @Required to field 'groupId' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var8.containsKey("name")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'name' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var8.get("name") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'name' in existing Realm file.");
         } else if(!var7.a(var6.c)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'name' is required. Either set @Required to field 'name' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var8.containsKey("logoUrl")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'logoUrl' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var8.get("logoUrl") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'logoUrl' in existing Realm file.");
         } else if(!var7.a(var6.d)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'logoUrl' is required. Either set @Required to field 'logoUrl' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var8.containsKey("emoji")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'emoji' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var8.get("emoji") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'emoji' in existing Realm file.");
         } else if(!var7.a(var6.e)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'emoji' is required. Either set @Required to field 'emoji' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var8.containsKey("category")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'category' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var8.get("category") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'category' in existing Realm file.");
         } else if(!var7.a(var6.f)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'category' is required. Either set @Required to field 'category' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var8.containsKey("online")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'online' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var8.get("online") != RealmFieldType.BOOLEAN) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'boolean' for field 'online' in existing Realm file.");
         } else if(var7.a(var6.g)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'online' does support null values in the existing Realm file. Use corresponding boxed type for field 'online' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var8.containsKey("atm")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'atm' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var8.get("atm") != RealmFieldType.BOOLEAN) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'boolean' for field 'atm' in existing Realm file.");
         } else if(var7.a(var6.h)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'atm' does support null values in the existing Realm file. Use corresponding boxed type for field 'atm' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var8.containsKey("formattedAddress")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'formattedAddress' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var8.get("formattedAddress") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'formattedAddress' in existing Realm file.");
         } else if(!var7.a(var6.i)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'formattedAddress' is required. Either set @Required to field 'formattedAddress' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var8.containsKey("latitude")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'latitude' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var8.get("latitude") != RealmFieldType.DOUBLE) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'Double' for field 'latitude' in existing Realm file.");
         } else if(!var7.a(var6.j)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'latitude' does not support null values in the existing Realm file. Either set @Required, use the primitive type for field 'latitude' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var8.containsKey("longitude")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'longitude' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var8.get("longitude") != RealmFieldType.DOUBLE) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'Double' for field 'longitude' in existing Realm file.");
         } else if(!var7.a(var6.k)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'longitude' does not support null values in the existing Realm file. Either set @Required, use the primitive type for field 'longitude' or migrate using RealmObjectSchema.setNullable().");
         } else {
            return var6;
         }
      }
   }

   public static void a(av var0, Iterator var1, Map var2) {
      Table var11 = var0.c(co.uk.getmondo.d.u.class);
      long var7 = var11.getNativePtr();
      ag.a var12 = (ag.a)var0.f.c(co.uk.getmondo.d.u.class);
      long var9 = var11.d();

      while(true) {
         while(true) {
            co.uk.getmondo.d.u var13;
            do {
               if(!var1.hasNext()) {
                  return;
               }

               var13 = (co.uk.getmondo.d.u)var1.next();
            } while(var2.containsKey(var13));

            if(var13 instanceof io.realm.internal.l && ((io.realm.internal.l)var13).s_().a() != null && ((io.realm.internal.l)var13).s_().a().g().equals(var0.g())) {
               var2.put(var13, Long.valueOf(((io.realm.internal.l)var13).s_().b().c()));
            } else {
               String var14 = ((ah)var13).m();
               long var3;
               if(var14 == null) {
                  var3 = Table.nativeFindFirstNull(var7, var9);
               } else {
                  var3 = Table.nativeFindFirstString(var7, var9, var14);
               }

               long var5 = var3;
               if(var3 == -1L) {
                  var5 = OsObject.b(var11, var14);
               }

               var2.put(var13, Long.valueOf(var5));
               var14 = ((ah)var13).n();
               if(var14 != null) {
                  Table.nativeSetString(var7, var12.b, var5, var14, false);
               } else {
                  Table.nativeSetNull(var7, var12.b, var5, false);
               }

               var14 = ((ah)var13).o();
               if(var14 != null) {
                  Table.nativeSetString(var7, var12.c, var5, var14, false);
               } else {
                  Table.nativeSetNull(var7, var12.c, var5, false);
               }

               var14 = ((ah)var13).p();
               if(var14 != null) {
                  Table.nativeSetString(var7, var12.d, var5, var14, false);
               } else {
                  Table.nativeSetNull(var7, var12.d, var5, false);
               }

               var14 = ((ah)var13).q();
               if(var14 != null) {
                  Table.nativeSetString(var7, var12.e, var5, var14, false);
               } else {
                  Table.nativeSetNull(var7, var12.e, var5, false);
               }

               var14 = ((ah)var13).r();
               if(var14 != null) {
                  Table.nativeSetString(var7, var12.f, var5, var14, false);
               } else {
                  Table.nativeSetNull(var7, var12.f, var5, false);
               }

               Table.nativeSetBoolean(var7, var12.g, var5, ((ah)var13).s(), false);
               Table.nativeSetBoolean(var7, var12.h, var5, ((ah)var13).t(), false);
               var14 = ((ah)var13).u();
               if(var14 != null) {
                  Table.nativeSetString(var7, var12.i, var5, var14, false);
               } else {
                  Table.nativeSetNull(var7, var12.i, var5, false);
               }

               Double var16 = ((ah)var13).v();
               if(var16 != null) {
                  Table.nativeSetDouble(var7, var12.j, var5, var16.doubleValue(), false);
               } else {
                  Table.nativeSetNull(var7, var12.j, var5, false);
               }

               Double var15 = ((ah)var13).w();
               if(var15 != null) {
                  Table.nativeSetDouble(var7, var12.k, var5, var15.doubleValue(), false);
               } else {
                  Table.nativeSetNull(var7, var12.k, var5, false);
               }
            }
         }
      }
   }

   public static long b(av var0, co.uk.getmondo.d.u var1, Map var2) {
      long var5;
      if(var1 instanceof io.realm.internal.l && ((io.realm.internal.l)var1).s_().a() != null && ((io.realm.internal.l)var1).s_().a().g().equals(var0.g())) {
         var5 = ((io.realm.internal.l)var1).s_().b().c();
      } else {
         Table var9 = var0.c(co.uk.getmondo.d.u.class);
         long var7 = var9.getNativePtr();
         ag.a var11 = (ag.a)var0.f.c(co.uk.getmondo.d.u.class);
         long var3 = var9.d();
         String var10 = ((ah)var1).m();
         if(var10 == null) {
            var3 = Table.nativeFindFirstNull(var7, var3);
         } else {
            var3 = Table.nativeFindFirstString(var7, var3, var10);
         }

         var5 = var3;
         if(var3 == -1L) {
            var5 = OsObject.b(var9, var10);
         }

         var2.put(var1, Long.valueOf(var5));
         String var13 = ((ah)var1).n();
         if(var13 != null) {
            Table.nativeSetString(var7, var11.b, var5, var13, false);
         } else {
            Table.nativeSetNull(var7, var11.b, var5, false);
         }

         var13 = ((ah)var1).o();
         if(var13 != null) {
            Table.nativeSetString(var7, var11.c, var5, var13, false);
         } else {
            Table.nativeSetNull(var7, var11.c, var5, false);
         }

         var13 = ((ah)var1).p();
         if(var13 != null) {
            Table.nativeSetString(var7, var11.d, var5, var13, false);
         } else {
            Table.nativeSetNull(var7, var11.d, var5, false);
         }

         var13 = ((ah)var1).q();
         if(var13 != null) {
            Table.nativeSetString(var7, var11.e, var5, var13, false);
         } else {
            Table.nativeSetNull(var7, var11.e, var5, false);
         }

         var13 = ((ah)var1).r();
         if(var13 != null) {
            Table.nativeSetString(var7, var11.f, var5, var13, false);
         } else {
            Table.nativeSetNull(var7, var11.f, var5, false);
         }

         Table.nativeSetBoolean(var7, var11.g, var5, ((ah)var1).s(), false);
         Table.nativeSetBoolean(var7, var11.h, var5, ((ah)var1).t(), false);
         var13 = ((ah)var1).u();
         if(var13 != null) {
            Table.nativeSetString(var7, var11.i, var5, var13, false);
         } else {
            Table.nativeSetNull(var7, var11.i, var5, false);
         }

         Double var14 = ((ah)var1).v();
         if(var14 != null) {
            Table.nativeSetDouble(var7, var11.j, var5, var14.doubleValue(), false);
         } else {
            Table.nativeSetNull(var7, var11.j, var5, false);
         }

         Double var12 = ((ah)var1).w();
         if(var12 != null) {
            Table.nativeSetDouble(var7, var11.k, var5, var12.doubleValue(), false);
         } else {
            Table.nativeSetNull(var7, var11.k, var5, false);
         }
      }

      return var5;
   }

   public static co.uk.getmondo.d.u b(av var0, co.uk.getmondo.d.u var1, boolean var2, Map var3) {
      io.realm.internal.l var4 = (io.realm.internal.l)var3.get(var1);
      co.uk.getmondo.d.u var5;
      if(var4 != null) {
         var5 = (co.uk.getmondo.d.u)var4;
      } else {
         var5 = (co.uk.getmondo.d.u)var0.a(co.uk.getmondo.d.u.class, ((ah)var1).m(), false, Collections.emptyList());
         var3.put(var1, (io.realm.internal.l)var5);
         ah var7 = (ah)var1;
         ah var6 = (ah)var5;
         var6.b(var7.n());
         var6.c(var7.o());
         var6.d(var7.p());
         var6.e(var7.q());
         var6.f(var7.r());
         var6.a(var7.s());
         var6.b(var7.t());
         var6.g(var7.u());
         var6.a(var7.v());
         var6.b(var7.w());
      }

      return var5;
   }

   public static OsObjectSchemaInfo x() {
      return c;
   }

   public static String y() {
      return "class_Merchant";
   }

   private static OsObjectSchemaInfo z() {
      OsObjectSchemaInfo.a var0 = new OsObjectSchemaInfo.a("Merchant");
      var0.a("id", RealmFieldType.STRING, true, true, false);
      var0.a("groupId", RealmFieldType.STRING, false, false, false);
      var0.a("name", RealmFieldType.STRING, false, false, false);
      var0.a("logoUrl", RealmFieldType.STRING, false, false, false);
      var0.a("emoji", RealmFieldType.STRING, false, false, false);
      var0.a("category", RealmFieldType.STRING, false, false, false);
      var0.a("online", RealmFieldType.BOOLEAN, false, false, true);
      var0.a("atm", RealmFieldType.BOOLEAN, false, false, true);
      var0.a("formattedAddress", RealmFieldType.STRING, false, false, false);
      var0.a("latitude", RealmFieldType.DOUBLE, false, false, false);
      var0.a("longitude", RealmFieldType.DOUBLE, false, false, false);
      return var0.a();
   }

   public void a(Double var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            if(var1 == null) {
               var2.b().a(this.a.j, var2.c(), true);
            } else {
               var2.b().a(this.a.j, var2.c(), var1.doubleValue(), true);
            }
         }
      } else {
         this.b.a().e();
         if(var1 == null) {
            this.b.b().c(this.a.j);
         } else {
            this.b.b().a(this.a.j, var1.doubleValue());
         }
      }

   }

   public void a(String var1) {
      if(!this.b.e()) {
         this.b.a().e();
         throw new RealmException("Primary key field 'id' cannot be changed after object was created.");
      }
   }

   public void a(boolean var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            var2.b().a(this.a.g, var2.c(), var1, true);
         }
      } else {
         this.b.a().e();
         this.b.b().a(this.a.g, var1);
      }

   }

   public void b(Double var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            if(var1 == null) {
               var2.b().a(this.a.k, var2.c(), true);
            } else {
               var2.b().a(this.a.k, var2.c(), var1.doubleValue(), true);
            }
         }
      } else {
         this.b.a().e();
         if(var1 == null) {
            this.b.b().c(this.a.k);
         } else {
            this.b.b().a(this.a.k, var1.doubleValue());
         }
      }

   }

   public void b(String var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            if(var1 == null) {
               var2.b().a(this.a.b, var2.c(), true);
            } else {
               var2.b().a(this.a.b, var2.c(), var1, true);
            }
         }
      } else {
         this.b.a().e();
         if(var1 == null) {
            this.b.b().c(this.a.b);
         } else {
            this.b.b().a(this.a.b, var1);
         }
      }

   }

   public void b(boolean var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            var2.b().a(this.a.h, var2.c(), var1, true);
         }
      } else {
         this.b.a().e();
         this.b.b().a(this.a.h, var1);
      }

   }

   public void c(String var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            if(var1 == null) {
               var2.b().a(this.a.c, var2.c(), true);
            } else {
               var2.b().a(this.a.c, var2.c(), var1, true);
            }
         }
      } else {
         this.b.a().e();
         if(var1 == null) {
            this.b.b().c(this.a.c);
         } else {
            this.b.b().a(this.a.c, var1);
         }
      }

   }

   public void d(String var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            if(var1 == null) {
               var2.b().a(this.a.d, var2.c(), true);
            } else {
               var2.b().a(this.a.d, var2.c(), var1, true);
            }
         }
      } else {
         this.b.a().e();
         if(var1 == null) {
            this.b.b().c(this.a.d);
         } else {
            this.b.b().a(this.a.d, var1);
         }
      }

   }

   public void e(String var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            if(var1 == null) {
               var2.b().a(this.a.e, var2.c(), true);
            } else {
               var2.b().a(this.a.e, var2.c(), var1, true);
            }
         }
      } else {
         this.b.a().e();
         if(var1 == null) {
            this.b.b().c(this.a.e);
         } else {
            this.b.b().a(this.a.e, var1);
         }
      }

   }

   public void f(String var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            if(var1 == null) {
               var2.b().a(this.a.f, var2.c(), true);
            } else {
               var2.b().a(this.a.f, var2.c(), var1, true);
            }
         }
      } else {
         this.b.a().e();
         if(var1 == null) {
            this.b.b().c(this.a.f);
         } else {
            this.b.b().a(this.a.f, var1);
         }
      }

   }

   public void g(String var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            if(var1 == null) {
               var2.b().a(this.a.i, var2.c(), true);
            } else {
               var2.b().a(this.a.i, var2.c(), var1, true);
            }
         }
      } else {
         this.b.a().e();
         if(var1 == null) {
            this.b.b().c(this.a.i);
         } else {
            this.b.b().a(this.a.i, var1);
         }
      }

   }

   public String m() {
      this.b.a().e();
      return this.b.b().k(this.a.a);
   }

   public String n() {
      this.b.a().e();
      return this.b.b().k(this.a.b);
   }

   public String o() {
      this.b.a().e();
      return this.b.b().k(this.a.c);
   }

   public String p() {
      this.b.a().e();
      return this.b.b().k(this.a.d);
   }

   public String q() {
      this.b.a().e();
      return this.b.b().k(this.a.e);
   }

   public String r() {
      this.b.a().e();
      return this.b.b().k(this.a.f);
   }

   public boolean s() {
      this.b.a().e();
      return this.b.b().g(this.a.g);
   }

   public au s_() {
      return this.b;
   }

   public boolean t() {
      this.b.a().e();
      return this.b.b().g(this.a.h);
   }

   public String toString() {
      String var1;
      if(!bc.b(this)) {
         var1 = "Invalid object";
      } else {
         StringBuilder var2 = new StringBuilder("Merchant = proxy[");
         var2.append("{id:");
         if(this.m() != null) {
            var1 = this.m();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{groupId:");
         if(this.n() != null) {
            var1 = this.n();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{name:");
         if(this.o() != null) {
            var1 = this.o();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{logoUrl:");
         if(this.p() != null) {
            var1 = this.p();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{emoji:");
         if(this.q() != null) {
            var1 = this.q();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{category:");
         if(this.r() != null) {
            var1 = this.r();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{online:");
         var2.append(this.s());
         var2.append("}");
         var2.append(",");
         var2.append("{atm:");
         var2.append(this.t());
         var2.append("}");
         var2.append(",");
         var2.append("{formattedAddress:");
         if(this.u() != null) {
            var1 = this.u();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{latitude:");
         Object var3;
         if(this.v() != null) {
            var3 = this.v();
         } else {
            var3 = "null";
         }

         var2.append(var3);
         var2.append("}");
         var2.append(",");
         var2.append("{longitude:");
         if(this.w() != null) {
            var3 = this.w();
         } else {
            var3 = "null";
         }

         var2.append(var3);
         var2.append("}");
         var2.append("]");
         var1 = var2.toString();
      }

      return var1;
   }

   public String u() {
      this.b.a().e();
      return this.b.b().k(this.a.i);
   }

   public void u_() {
      if(this.b == null) {
         g.b var1 = (g.b)g.g.get();
         this.a = (ag.a)var1.c();
         this.b = new au(this);
         this.b.a(var1.a());
         this.b.a(var1.b());
         this.b.a(var1.d());
         this.b.a(var1.e());
      }

   }

   public Double v() {
      this.b.a().e();
      Double var1;
      if(this.b.b().b(this.a.j)) {
         var1 = null;
      } else {
         var1 = Double.valueOf(this.b.b().i(this.a.j));
      }

      return var1;
   }

   public Double w() {
      this.b.a().e();
      Double var1;
      if(this.b.b().b(this.a.k)) {
         var1 = null;
      } else {
         var1 = Double.valueOf(this.b.b().i(this.a.k));
      }

      return var1;
   }

   static final class a extends io.realm.internal.c {
      long a;
      long b;
      long c;
      long d;
      long e;
      long f;
      long g;
      long h;
      long i;
      long j;
      long k;

      a(SharedRealm var1, Table var2) {
         super(11);
         this.a = this.a(var2, "id", RealmFieldType.STRING);
         this.b = this.a(var2, "groupId", RealmFieldType.STRING);
         this.c = this.a(var2, "name", RealmFieldType.STRING);
         this.d = this.a(var2, "logoUrl", RealmFieldType.STRING);
         this.e = this.a(var2, "emoji", RealmFieldType.STRING);
         this.f = this.a(var2, "category", RealmFieldType.STRING);
         this.g = this.a(var2, "online", RealmFieldType.BOOLEAN);
         this.h = this.a(var2, "atm", RealmFieldType.BOOLEAN);
         this.i = this.a(var2, "formattedAddress", RealmFieldType.STRING);
         this.j = this.a(var2, "latitude", RealmFieldType.DOUBLE);
         this.k = this.a(var2, "longitude", RealmFieldType.DOUBLE);
      }

      a(io.realm.internal.c var1, boolean var2) {
         super(var1, var2);
         this.a(var1, this);
      }

      protected final io.realm.internal.c a(boolean var1) {
         return new ag.a(this, var1);
      }

      protected final void a(io.realm.internal.c var1, io.realm.internal.c var2) {
         ag.a var3 = (ag.a)var1;
         ag.a var4 = (ag.a)var2;
         var4.a = var3.a;
         var4.b = var3.b;
         var4.c = var3.c;
         var4.d = var3.d;
         var4.e = var3.e;
         var4.f = var3.f;
         var4.g = var3.g;
         var4.h = var3.h;
         var4.i = var3.i;
         var4.j = var3.j;
         var4.k = var3.k;
      }
   }
}
