package io.realm.exceptions;

import io.realm.internal.Keep;

@Keep
public final class RealmException extends RuntimeException {
   public RealmException(String var1) {
      super(var1);
   }

   public RealmException(String var1, Throwable var2) {
      super(var1, var2);
   }
}
