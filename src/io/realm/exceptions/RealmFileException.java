package io.realm.exceptions;

import io.realm.internal.Keep;
import java.util.Locale;

@Keep
public class RealmFileException extends RuntimeException {
   private final RealmFileException.Kind kind;

   public RealmFileException(byte var1, String var2) {
      super(var2);
      this.kind = RealmFileException.Kind.getKind(var1);
   }

   public RealmFileException(RealmFileException.Kind var1, String var2) {
      super(var2);
      this.kind = var1;
   }

   public RealmFileException(RealmFileException.Kind var1, String var2, Throwable var3) {
      super(var2, var3);
      this.kind = var1;
   }

   public RealmFileException(RealmFileException.Kind var1, Throwable var2) {
      super(var2);
      this.kind = var1;
   }

   public RealmFileException.Kind getKind() {
      return this.kind;
   }

   public String toString() {
      return String.format(Locale.US, "%s Kind: %s.", new Object[]{super.toString(), this.kind});
   }

   @Keep
   public static enum Kind {
      ACCESS_ERROR,
      BAD_HISTORY,
      EXISTS,
      FORMAT_UPGRADE_REQUIRED,
      INCOMPATIBLE_LOCK_FILE,
      NOT_FOUND,
      PERMISSION_DENIED;

      static RealmFileException.Kind getKind(byte var0) {
         RealmFileException.Kind var1;
         switch(var0) {
         case 0:
            var1 = ACCESS_ERROR;
            break;
         case 1:
            var1 = BAD_HISTORY;
            break;
         case 2:
            var1 = PERMISSION_DENIED;
            break;
         case 3:
            var1 = EXISTS;
            break;
         case 4:
            var1 = NOT_FOUND;
            break;
         case 5:
            var1 = INCOMPATIBLE_LOCK_FILE;
            break;
         case 6:
            var1 = FORMAT_UPGRADE_REQUIRED;
            break;
         default:
            throw new RuntimeException("Unknown value for RealmFileException kind.");
         }

         return var1;
      }
   }
}
