package io.realm.exceptions;

import io.realm.internal.Keep;

@Keep
public final class RealmMigrationNeededException extends RuntimeException {
   private final String canonicalRealmPath;

   public RealmMigrationNeededException(String var1, String var2) {
      super(var2);
      this.canonicalRealmPath = var1;
   }

   public RealmMigrationNeededException(String var1, String var2, Throwable var3) {
      super(var2, var3);
      this.canonicalRealmPath = var1;
   }

   public String getPath() {
      return this.canonicalRealmPath;
   }
}
