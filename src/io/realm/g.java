package io.realm;

import android.content.Context;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.CheckedRow;
import io.realm.internal.SharedRealm;
import io.realm.internal.Table;
import io.realm.internal.UncheckedRow;
import io.realm.internal.Util;
import io.realm.log.RealmLog;
import java.io.Closeable;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

abstract class g implements Closeable {
   static volatile Context a;
   static final io.realm.internal.async.a b = io.realm.internal.async.a.a();
   public static final g.c g = new g.c();
   final long c;
   protected final ay d;
   protected SharedRealm e;
   protected final bh f;
   private aw h;

   g(aw var1) {
      this(var1.a());
      this.h = var1;
   }

   g(ay var1) {
      SharedRealm.c var2 = null;
      super();
      this.c = Thread.currentThread().getId();
      this.d = var1;
      this.h = null;
      if(this instanceof av) {
         var2 = new SharedRealm.c() {
            public void a(long var1) {
               if(g.this.h != null) {
                  g.this.h.a((av)g.this);
               }

            }
         };
      }

      this.e = SharedRealm.a(var1, var2, true);
      this.f = new bh(this);
   }

   protected static void a(final ay var0, final ba var1, final g.a var2, RealmMigrationNeededException var3) throws FileNotFoundException {
      if(var0 == null) {
         throw new IllegalArgumentException("RealmConfiguration must be provided");
      } else if(var0.q()) {
         throw new IllegalArgumentException("Manual migrations are not supported for synced Realms");
      } else if(var1 == null && var0.e() == null) {
         throw new RealmMigrationNeededException(var0.m(), "RealmMigration must be provided", var3);
      } else {
         final AtomicBoolean var4 = new AtomicBoolean(false);
         aw.a(var0, new aw.a() {
            public void a(int param1) {
               // $FF: Couldn't be decompiled
            }
         });
         if(var4.get()) {
            throw new FileNotFoundException("Cannot migrate a Realm file which doesn't exist: " + var0.m());
         }
      }
   }

   static boolean a(final ay var0) {
      final AtomicBoolean var1 = new AtomicBoolean(true);
      aw.a(var0, new aw.a() {
         public void a(int var1x) {
            if(var1x != 0) {
               throw new IllegalStateException("It's not allowed to delete the file associated with an open Realm. Remember to close() all the instances of the Realm before deleting its file: " + var0.m());
            } else {
               String var2 = var0.m();
               File var3 = var0.a();
               String var4 = var0.b();
               var1.set(Util.a(var2, var3, var4));
            }
         }
      });
      return var1.get();
   }

   bb a(Class var1, long var2, boolean var4, List var5) {
      UncheckedRow var6 = this.f.a(var1).f(var2);
      return this.d.h().a(var1, this, var6, this.f.c(var1), var4, var5);
   }

   bb a(Class var1, String var2, long var3) {
      boolean var5;
      if(var2 != null) {
         var5 = true;
      } else {
         var5 = false;
      }

      Table var8;
      if(var5) {
         var8 = this.f.a(var2);
      } else {
         var8 = this.f.a(var1);
      }

      Object var7;
      if(var5) {
         if(var3 != -1L) {
            var7 = var8.h(var3);
         } else {
            var7 = io.realm.internal.e.a;
         }

         var7 = new p(this, (io.realm.internal.n)var7);
      } else {
         io.realm.internal.m var6 = this.d.h();
         Object var9;
         if(var3 != -1L) {
            var9 = var8.f(var3);
         } else {
            var9 = io.realm.internal.e.a;
         }

         var7 = var6.a(var1, this, (io.realm.internal.n)var9, this.f.c(var1), false, Collections.emptyList());
      }

      return (bb)var7;
   }

   bb a(Class var1, String var2, UncheckedRow var3) {
      boolean var4;
      if(var2 != null) {
         var4 = true;
      } else {
         var4 = false;
      }

      Object var5;
      if(var4) {
         var5 = new p(this, CheckedRow.a(var3));
      } else {
         var5 = this.d.h().a(var1, this, var3, this.f.c(var1), false, Collections.emptyList());
      }

      return (bb)var5;
   }

   void a(long var1) {
      this.e.a(var1);
   }

   void a(boolean var1) {
      this.e();
      this.e.a(var1);
   }

   public boolean a() {
      this.e();
      return this.e.d();
   }

   public void b() {
      this.a(false);
   }

   public void c() {
      this.e();
      this.e.b();
   }

   public void close() {
      if(this.c != Thread.currentThread().getId()) {
         throw new IllegalStateException("Realm access from incorrect thread. Realm instance can only be closed on the thread it was created.");
      } else {
         if(this.h != null) {
            this.h.a(this);
         } else {
            this.j();
         }

      }
   }

   public void d() {
      this.e();
      this.e.c();
   }

   protected void e() {
      if(this.e != null && !this.e.i()) {
         if(this.c != Thread.currentThread().getId()) {
            throw new IllegalStateException("Realm access from incorrect thread. Realm objects can only be accessed on the thread they were created.");
         }
      } else {
         throw new IllegalStateException("This Realm instance has already been closed, making it unusable.");
      }
   }

   protected void f() {
      if(!this.a()) {
         throw new IllegalStateException("Changing Realm data can only be done from inside a transaction.");
      }
   }

   protected void finalize() throws Throwable {
      if(this.e != null && !this.e.i()) {
         RealmLog.b("Remember to call close() on all Realm instances. Realm %s is being finalized without being closed, this can lead to running out of native memory.", new Object[]{this.d.m()});
         if(this.h != null) {
            this.h.c();
         }
      }

      super.finalize();
   }

   public String g() {
      return this.d.m();
   }

   public ay h() {
      return this.d;
   }

   public long i() {
      return this.e.e();
   }

   void j() {
      this.h = null;
      if(this.e != null) {
         this.e.close();
         this.e = null;
      }

      if(this.f != null) {
         this.f.a();
      }

   }

   public bh k() {
      return this.f;
   }

   public void l() {
      this.e();
      Iterator var2 = this.f.b().iterator();

      while(var2.hasNext()) {
         be var1 = (be)var2.next();
         this.f.a(var1.a()).b();
      }

   }

   SharedRealm m() {
      return this.e;
   }

   protected interface a {
      void a();
   }

   public static final class b {
      private g a;
      private io.realm.internal.n b;
      private io.realm.internal.c c;
      private boolean d;
      private List e;

      g a() {
         return this.a;
      }

      public void a(g var1, io.realm.internal.n var2, io.realm.internal.c var3, boolean var4, List var5) {
         this.a = var1;
         this.b = var2;
         this.c = var3;
         this.d = var4;
         this.e = var5;
      }

      public io.realm.internal.n b() {
         return this.b;
      }

      public io.realm.internal.c c() {
         return this.c;
      }

      public boolean d() {
         return this.d;
      }

      public List e() {
         return this.e;
      }

      public void f() {
         this.a = null;
         this.b = null;
         this.c = null;
         this.d = false;
         this.e = null;
      }
   }

   static final class c extends ThreadLocal {
      protected g.b a() {
         return new g.b();
      }

      // $FF: synthetic method
      protected Object initialValue() {
         return this.a();
      }
   }
}
