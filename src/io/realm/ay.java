package io.realm;

import android.content.Context;
import io.realm.exceptions.RealmException;
import io.realm.exceptions.RealmFileException;
import io.realm.internal.SharedRealm;
import io.realm.internal.Util;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;

public class ay {
   protected static final io.realm.internal.m a;
   private static final Object b = av.p();
   private static Boolean c;
   private final File d;
   private final String e;
   private final String f;
   private final String g;
   private final byte[] h;
   private final long i;
   private final ba j;
   private final boolean k;
   private final SharedRealm.a l;
   private final io.realm.internal.m m;
   private final io.realm.a.b n;
   private final av.a o;
   private final boolean p;
   private final CompactOnLaunchCallback q;

   static {
      if(b != null) {
         io.realm.internal.m var0 = a(b.getClass().getCanonicalName());
         if(!var0.c()) {
            throw new ExceptionInInitializerError("RealmTransformer doesn't seem to be applied. Please update the project configuration to use the Realm Gradle plugin. See https://realm.io/news/android-installation-change/");
         }

         a = var0;
      } else {
         a = null;
      }

   }

   protected ay(File var1, String var2, String var3, String var4, byte[] var5, long var6, ba var8, boolean var9, SharedRealm.a var10, io.realm.internal.m var11, io.realm.a.b var12, av.a var13, boolean var14, CompactOnLaunchCallback var15) {
      this.d = var1;
      this.e = var2;
      this.f = var3;
      this.g = var4;
      this.h = var5;
      this.i = var6;
      this.j = var8;
      this.k = var9;
      this.l = var10;
      this.m = var11;
      this.n = var12;
      this.o = var13;
      this.p = var14;
      this.q = var15;
   }

   private static io.realm.internal.m a(String var0) {
      String[] var6 = var0.split("\\.");
      var0 = var6[var6.length - 1];
      var0 = String.format(Locale.US, "io.realm.%s%s", new Object[]{var0, "Mediator"});

      try {
         Constructor var1 = Class.forName(var0).getDeclaredConstructors()[0];
         var1.setAccessible(true);
         io.realm.internal.m var7 = (io.realm.internal.m)var1.newInstance(new Object[0]);
         return var7;
      } catch (ClassNotFoundException var2) {
         throw new RealmException("Could not find " + var0, var2);
      } catch (InvocationTargetException var3) {
         throw new RealmException("Could not create an instance of " + var0, var3);
      } catch (InstantiationException var4) {
         throw new RealmException("Could not create an instance of " + var0, var4);
      } catch (IllegalAccessException var5) {
         throw new RealmException("Could not create an instance of " + var0, var5);
      }
   }

   protected static io.realm.internal.m a(Set var0, Set var1) {
      Object var3;
      if(var1.size() > 0) {
         var3 = new io.realm.internal.b.b(a, var1);
      } else if(var0.size() == 1) {
         var3 = a(var0.iterator().next().getClass().getCanonicalName());
      } else {
         io.realm.internal.m[] var5 = new io.realm.internal.m[var0.size()];
         int var2 = 0;

         for(Iterator var4 = var0.iterator(); var4.hasNext(); ++var2) {
            var5[var2] = a(var4.next().getClass().getCanonicalName());
         }

         var3 = new io.realm.internal.b.a(var5);
      }

      return (io.realm.internal.m)var3;
   }

   protected static String a(File var0) {
      try {
         String var1 = var0.getCanonicalPath();
         return var1;
      } catch (IOException var2) {
         throw new RealmFileException(RealmFileException.Kind.ACCESS_ERROR, "Could not resolve the canonical path to the Realm file: " + var0.getAbsolutePath(), var2);
      }
   }

   static boolean p() {
      // $FF: Couldn't be decompiled
   }

   public File a() {
      return this.d;
   }

   public String b() {
      return this.e;
   }

   public byte[] c() {
      byte[] var1;
      if(this.h == null) {
         var1 = null;
      } else {
         var1 = Arrays.copyOf(this.h, this.h.length);
      }

      return var1;
   }

   public long d() {
      return this.i;
   }

   public ba e() {
      return this.j;
   }

   public boolean equals(Object var1) {
      boolean var3 = false;
      boolean var2;
      if(this == var1) {
         var2 = true;
      } else {
         var2 = var3;
         if(var1 != null) {
            var2 = var3;
            if(this.getClass() == var1.getClass()) {
               ay var4 = (ay)var1;
               var2 = var3;
               if(this.i == var4.i) {
                  var2 = var3;
                  if(this.k == var4.k) {
                     var2 = var3;
                     if(this.d.equals(var4.d)) {
                        var2 = var3;
                        if(this.e.equals(var4.e)) {
                           var2 = var3;
                           if(this.f.equals(var4.f)) {
                              var2 = var3;
                              if(Arrays.equals(this.h, var4.h)) {
                                 var2 = var3;
                                 if(this.l.equals(var4.l)) {
                                    if(this.j != null) {
                                       var2 = var3;
                                       if(!this.j.equals(var4.j)) {
                                          return var2;
                                       }
                                    } else if(var4.j != null) {
                                       var2 = var3;
                                       return var2;
                                    }

                                    if(this.n != null) {
                                       var2 = var3;
                                       if(!this.n.equals(var4.n)) {
                                          return var2;
                                       }
                                    } else if(var4.n != null) {
                                       var2 = var3;
                                       return var2;
                                    }

                                    if(this.o != null) {
                                       var2 = var3;
                                       if(!this.o.equals(var4.o)) {
                                          return var2;
                                       }
                                    } else if(var4.o != null) {
                                       var2 = var3;
                                       return var2;
                                    }

                                    var2 = var3;
                                    if(this.p == var4.p) {
                                       if(this.q != null) {
                                          var2 = var3;
                                          if(!this.q.equals(var4.q)) {
                                             return var2;
                                          }
                                       } else if(var4.q != null) {
                                          var2 = var3;
                                          return var2;
                                       }

                                       var2 = this.m.equals(var4.m);
                                    }
                                 }
                              }
                           }
                        }
                     }
                  }
               }
            }
         }
      }

      return var2;
   }

   public boolean f() {
      return this.k;
   }

   public SharedRealm.a g() {
      return this.l;
   }

   io.realm.internal.m h() {
      return this.m;
   }

   public int hashCode() {
      byte var6 = 1;
      int var7 = 0;
      int var9 = this.d.hashCode();
      int var8 = this.e.hashCode();
      int var10 = this.f.hashCode();
      int var1;
      if(this.h != null) {
         var1 = Arrays.hashCode(this.h);
      } else {
         var1 = 0;
      }

      int var11 = (int)this.i;
      int var2;
      if(this.j != null) {
         var2 = this.j.hashCode();
      } else {
         var2 = 0;
      }

      byte var3;
      if(this.k) {
         var3 = 1;
      } else {
         var3 = 0;
      }

      int var12 = this.m.hashCode();
      int var13 = this.l.hashCode();
      int var4;
      if(this.n != null) {
         var4 = this.n.hashCode();
      } else {
         var4 = 0;
      }

      int var5;
      if(this.o != null) {
         var5 = this.o.hashCode();
      } else {
         var5 = 0;
      }

      if(!this.p) {
         var6 = 0;
      }

      if(this.q != null) {
         var7 = this.q.hashCode();
      }

      return ((var5 + (var4 + (((var3 + (var2 + ((var1 + ((var9 * 31 + var8) * 31 + var10) * 31) * 31 + var11) * 31) * 31) * 31 + var12) * 31 + var13) * 31) * 31) * 31 + var6) * 31 + var7;
   }

   av.a i() {
      return this.o;
   }

   boolean j() {
      boolean var1;
      if(!Util.a(this.g)) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   String k() {
      return this.g;
   }

   public CompactOnLaunchCallback l() {
      return this.q;
   }

   public String m() {
      return this.f;
   }

   boolean n() {
      return (new File(this.f)).exists();
   }

   public boolean o() {
      return this.p;
   }

   boolean q() {
      return false;
   }

   public String toString() {
      StringBuilder var3 = new StringBuilder();
      var3.append("realmDirectory: ").append(this.d.toString());
      var3.append("\n");
      var3.append("realmFileName : ").append(this.e);
      var3.append("\n");
      var3.append("canonicalPath: ").append(this.f);
      var3.append("\n");
      StringBuilder var2 = var3.append("key: ").append("[length: ");
      byte var1;
      if(this.h == null) {
         var1 = 0;
      } else {
         var1 = 64;
      }

      var2.append(var1).append("]");
      var3.append("\n");
      var3.append("schemaVersion: ").append(Long.toString(this.i));
      var3.append("\n");
      var3.append("migration: ").append(this.j);
      var3.append("\n");
      var3.append("deleteRealmIfMigrationNeeded: ").append(this.k);
      var3.append("\n");
      var3.append("durability: ").append(this.l);
      var3.append("\n");
      var3.append("schemaMediator: ").append(this.m);
      var3.append("\n");
      var3.append("readOnly: ").append(this.p);
      var3.append("\n");
      var3.append("compactOnLaunch: ").append(this.q);
      return var3.toString();
   }

   public static class a {
      private File a;
      private String b;
      private String c;
      private byte[] d;
      private long e;
      private ba f;
      private boolean g;
      private SharedRealm.a h;
      private HashSet i;
      private HashSet j;
      private io.realm.a.b k;
      private av.a l;
      private boolean m;
      private CompactOnLaunchCallback n;

      public a() {
         this(g.a);
      }

      a(Context var1) {
         this.i = new HashSet();
         this.j = new HashSet();
         if(var1 == null) {
            throw new IllegalStateException("Call `Realm.init(Context)` before creating a RealmConfiguration");
         } else {
            io.realm.internal.k.a(var1);
            this.a(var1);
         }
      }

      private void a(Context var1) {
         this.a = var1.getFilesDir();
         this.b = "default.realm";
         this.d = null;
         this.e = 0L;
         this.f = null;
         this.g = false;
         this.h = SharedRealm.a.a;
         this.m = false;
         this.n = null;
         if(ay.b != null) {
            this.i.add(ay.b);
         }

      }

      public ay.a a() {
         if(this.c != null && this.c.length() != 0) {
            throw new IllegalStateException("Realm cannot clear its schema when previously configured to use an asset file by calling assetFile().");
         } else {
            this.g = true;
            return this;
         }
      }

      public ay b() {
         if(this.m) {
            if(this.l != null) {
               throw new IllegalStateException("This Realm is marked as read-only. Read-only Realms cannot use initialData(Realm.Transaction).");
            }

            if(this.c == null) {
               throw new IllegalStateException("Only Realms provided using 'assetFile(path)' can be marked read-only. No such Realm was provided.");
            }

            if(this.g) {
               throw new IllegalStateException("'deleteRealmIfMigrationNeeded()' and read-only Realms cannot be combined");
            }

            if(this.n != null) {
               throw new IllegalStateException("'compactOnLaunch()' and read-only Realms cannot be combined");
            }
         }

         if(this.k == null && ay.p()) {
            this.k = new io.realm.a.a();
         }

         return new ay(this.a, this.b, ay.a(new File(this.a, this.b)), this.c, this.d, this.e, this.f, this.g, this.h, ay.a(this.i, this.j), this.k, this.l, this.m, this.n);
      }
   }
}
