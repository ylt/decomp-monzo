package io.realm;

public enum bl {
   a(true),
   b(false);

   private final boolean c;

   private bl(boolean var3) {
      this.c = var3;
   }

   public boolean a() {
      return this.c;
   }
}
