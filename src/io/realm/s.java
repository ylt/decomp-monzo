package io.realm;

import io.realm.exceptions.RealmException;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.SharedRealm;
import io.realm.internal.Table;
import io.realm.log.RealmLog;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class s extends co.uk.getmondo.d.m implements io.realm.internal.l, t {
   private static final OsObjectSchemaInfo c = z();
   private static final List d;
   private s.a a;
   private au b;

   static {
      ArrayList var0 = new ArrayList();
      var0.add("id");
      var0.add("accountId");
      var0.add("created");
      var0.add("type");
      var0.add("appUri");
      var0.add("goldenTicket");
      var0.add("monthlySpendingReport");
      var0.add("transaction");
      var0.add("kycRejected");
      var0.add("sddRejectedReason");
      var0.add("basicItemInfo");
      var0.add("isDismissable");
      d = Collections.unmodifiableList(var0);
   }

   s() {
      this.b.f();
   }

   public static long a(av var0, co.uk.getmondo.d.m var1, Map var2) {
      long var3;
      if(var1 instanceof io.realm.internal.l && ((io.realm.internal.l)var1).s_().a() != null && ((io.realm.internal.l)var1).s_().a().g().equals(var0.g())) {
         var3 = ((io.realm.internal.l)var1).s_().b().c();
      } else {
         Table var7 = var0.c(co.uk.getmondo.d.m.class);
         long var5 = var7.getNativePtr();
         s.a var8 = (s.a)var0.f.c(co.uk.getmondo.d.m.class);
         var3 = var7.d();
         String var9 = ((t)var1).l();
         if(var9 == null) {
            var3 = Table.nativeFindFirstNull(var5, var3);
         } else {
            var3 = Table.nativeFindFirstString(var5, var3, var9);
         }

         if(var3 == -1L) {
            var3 = OsObject.b(var7, var9);
         } else {
            Table.a((Object)var9);
         }

         var2.put(var1, Long.valueOf(var3));
         String var11 = ((t)var1).m();
         if(var11 != null) {
            Table.nativeSetString(var5, var8.b, var3, var11, false);
         }

         Date var12 = ((t)var1).n();
         if(var12 != null) {
            Table.nativeSetTimestamp(var5, var8.c, var3, var12.getTime(), false);
         }

         var11 = ((t)var1).o();
         if(var11 != null) {
            Table.nativeSetString(var5, var8.d, var3, var11, false);
         }

         var11 = ((t)var1).p();
         if(var11 != null) {
            Table.nativeSetString(var5, var8.e, var3, var11, false);
         }

         co.uk.getmondo.d.o var14 = ((t)var1).q();
         Long var13;
         if(var14 != null) {
            var13 = (Long)var2.get(var14);
            if(var13 == null) {
               var13 = Long.valueOf(y.a(var0, var14, var2));
            }

            Table.nativeSetLink(var5, var8.f, var3, var13.longValue(), false);
         }

         co.uk.getmondo.d.v var15 = ((t)var1).r();
         if(var15 != null) {
            var13 = (Long)var2.get(var15);
            if(var13 == null) {
               var13 = Long.valueOf(ai.a(var0, var15, var2));
            }

            Table.nativeSetLink(var5, var8.g, var3, var13.longValue(), false);
         }

         co.uk.getmondo.d.aj var16 = ((t)var1).s();
         if(var16 != null) {
            var13 = (Long)var2.get(var16);
            if(var13 == null) {
               var13 = Long.valueOf(bm.a(var0, var16, var2));
            }

            Table.nativeSetLink(var5, var8.h, var3, var13.longValue(), false);
         }

         co.uk.getmondo.d.r var17 = ((t)var1).t();
         if(var17 != null) {
            var13 = (Long)var2.get(var17);
            if(var13 == null) {
               var13 = Long.valueOf(ac.a(var0, var17, var2));
            }

            Table.nativeSetLink(var5, var8.i, var3, var13.longValue(), false);
         }

         var11 = ((t)var1).u();
         if(var11 != null) {
            Table.nativeSetString(var5, var8.j, var3, var11, false);
         }

         co.uk.getmondo.d.f var18 = ((t)var1).v();
         if(var18 != null) {
            var13 = (Long)var2.get(var18);
            Long var10;
            if(var13 == null) {
               var10 = Long.valueOf(h.a(var0, var18, var2));
            } else {
               var10 = var13;
            }

            Table.nativeSetLink(var5, var8.k, var3, var10.longValue(), false);
         }

         Table.nativeSetBoolean(var5, var8.l, var3, ((t)var1).w(), false);
      }

      return var3;
   }

   public static co.uk.getmondo.d.m a(co.uk.getmondo.d.m var0, int var1, int var2, Map var3) {
      if(var1 <= var2 && var0 != null) {
         io.realm.internal.l.a var5 = (io.realm.internal.l.a)var3.get(var0);
         co.uk.getmondo.d.m var4;
         if(var5 == null) {
            var4 = new co.uk.getmondo.d.m();
            var3.put(var0, new io.realm.internal.l.a(var1, var4));
         } else {
            if(var1 >= var5.a) {
               var0 = (co.uk.getmondo.d.m)var5.b;
               return var0;
            }

            var4 = (co.uk.getmondo.d.m)var5.b;
            var5.a = var1;
         }

         t var7 = (t)var4;
         t var6 = (t)var0;
         var7.a(var6.l());
         var7.b(var6.m());
         var7.a(var6.n());
         var7.c(var6.o());
         var7.d(var6.p());
         var7.a(y.a(var6.q(), var1 + 1, var2, var3));
         var7.a(ai.a(var6.r(), var1 + 1, var2, var3));
         var7.a(bm.a(var6.s(), var1 + 1, var2, var3));
         var7.a(ac.a(var6.t(), var1 + 1, var2, var3));
         var7.e(var6.u());
         var7.a(h.a(var6.v(), var1 + 1, var2, var3));
         var7.a(var6.w());
         var0 = var4;
      } else {
         var0 = null;
      }

      return var0;
   }

   static co.uk.getmondo.d.m a(av var0, co.uk.getmondo.d.m var1, co.uk.getmondo.d.m var2, Map var3) {
      t var4 = (t)var1;
      t var7 = (t)var2;
      var4.b(var7.m());
      var4.a(var7.n());
      var4.c(var7.o());
      var4.d(var7.p());
      co.uk.getmondo.d.o var6 = var7.q();
      if(var6 == null) {
         var4.a((co.uk.getmondo.d.o)null);
      } else {
         co.uk.getmondo.d.o var5 = (co.uk.getmondo.d.o)var3.get(var6);
         if(var5 != null) {
            var4.a(var5);
         } else {
            var4.a(y.a(var0, var6, true, var3));
         }
      }

      co.uk.getmondo.d.v var8 = var7.r();
      if(var8 == null) {
         var4.a((co.uk.getmondo.d.v)null);
      } else {
         co.uk.getmondo.d.v var11 = (co.uk.getmondo.d.v)var3.get(var8);
         if(var11 != null) {
            var4.a(var11);
         } else {
            var4.a(ai.a(var0, var8, true, var3));
         }
      }

      co.uk.getmondo.d.aj var13 = var7.s();
      if(var13 == null) {
         var4.a((co.uk.getmondo.d.aj)null);
      } else {
         co.uk.getmondo.d.aj var9 = (co.uk.getmondo.d.aj)var3.get(var13);
         if(var9 != null) {
            var4.a(var9);
         } else {
            var4.a(bm.a(var0, var13, true, var3));
         }
      }

      co.uk.getmondo.d.r var14 = var7.t();
      if(var14 == null) {
         var4.a((co.uk.getmondo.d.r)null);
      } else {
         co.uk.getmondo.d.r var10 = (co.uk.getmondo.d.r)var3.get(var14);
         if(var10 != null) {
            var4.a(var10);
         } else {
            var4.a(ac.a(var0, var14, true, var3));
         }
      }

      var4.e(var7.u());
      co.uk.getmondo.d.f var12 = var7.v();
      if(var12 == null) {
         var4.a((co.uk.getmondo.d.f)null);
      } else {
         co.uk.getmondo.d.f var15 = (co.uk.getmondo.d.f)var3.get(var12);
         if(var15 != null) {
            var4.a(var15);
         } else {
            var4.a(h.a(var0, var12, true, var3));
         }
      }

      var4.a(var7.w());
      return var1;
   }

   public static co.uk.getmondo.d.m a(av var0, co.uk.getmondo.d.m var1, boolean var2, Map var3) {
      if(var1 instanceof io.realm.internal.l && ((io.realm.internal.l)var1).s_().a() != null && ((io.realm.internal.l)var1).s_().a().c != var0.c) {
         throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
      } else {
         if(!(var1 instanceof io.realm.internal.l) || ((io.realm.internal.l)var1).s_().a() == null || !((io.realm.internal.l)var1).s_().a().g().equals(var0.g())) {
            g.b var8 = (g.b)g.g.get();
            io.realm.internal.l var7 = (io.realm.internal.l)var3.get(var1);
            if(var7 != null) {
               var1 = (co.uk.getmondo.d.m)var7;
            } else {
               boolean var4;
               s var13;
               if(var2) {
                  Table var12 = var0.c(co.uk.getmondo.d.m.class);
                  long var5 = var12.d();
                  String var9 = ((t)var1).l();
                  if(var9 == null) {
                     var5 = var12.k(var5);
                  } else {
                     var5 = var12.a(var5, var9);
                  }

                  if(var5 != -1L) {
                     try {
                        var8.a(var0, var12.f(var5), var0.f.c(co.uk.getmondo.d.m.class), false, Collections.emptyList());
                        var13 = new s();
                        var3.put(var1, (io.realm.internal.l)var13);
                     } finally {
                        var8.f();
                     }

                     var4 = var2;
                  } else {
                     var4 = false;
                     var13 = null;
                  }
               } else {
                  var4 = var2;
                  var13 = null;
               }

               if(var4) {
                  var1 = a(var0, var13, var1, var3);
               } else {
                  var1 = b(var0, var1, var2, var3);
               }
            }
         }

         return var1;
      }
   }

   public static s.a a(SharedRealm var0, boolean var1) {
      if(!var0.a("class_FeedItem")) {
         throw new RealmMigrationNeededException(var0.h(), "The 'FeedItem' class is missing from the schema for this Realm.");
      } else {
         Table var6 = var0.b("class_FeedItem");
         long var4 = var6.c();
         if(var4 != 12L) {
            if(var4 < 12L) {
               throw new RealmMigrationNeededException(var0.h(), "Field count is less than expected - expected 12 but was " + var4);
            }

            if(!var1) {
               throw new RealmMigrationNeededException(var0.h(), "Field count is more than expected - expected 12 but was " + var4);
            }

            RealmLog.a("Field count is more than expected - expected 12 but was %1$d", new Object[]{Long.valueOf(var4)});
         }

         HashMap var8 = new HashMap();

         for(long var2 = 0L; var2 < var4; ++var2) {
            var8.put(var6.b(var2), var6.c(var2));
         }

         s.a var7 = new s.a(var0, var6);
         if(!var6.e()) {
            throw new RealmMigrationNeededException(var0.h(), "Primary key not defined for field 'id' in existing Realm file. @PrimaryKey was added.");
         } else if(var6.d() != var7.a) {
            throw new RealmMigrationNeededException(var0.h(), "Primary Key annotation definition was changed, from field " + var6.b(var6.d()) + " to field id");
         } else if(!var8.containsKey("id")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'id' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var8.get("id") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'id' in existing Realm file.");
         } else if(!var6.a(var7.a)) {
            throw new RealmMigrationNeededException(var0.h(), "@PrimaryKey field 'id' does not support null values in the existing Realm file. Migrate using RealmObjectSchema.setNullable(), or mark the field as @Required.");
         } else if(!var6.j(var6.a("id"))) {
            throw new RealmMigrationNeededException(var0.h(), "Index not defined for field 'id' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex().");
         } else if(!var8.containsKey("accountId")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'accountId' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var8.get("accountId") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'accountId' in existing Realm file.");
         } else if(!var6.a(var7.b)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'accountId' is required. Either set @Required to field 'accountId' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var8.containsKey("created")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'created' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var8.get("created") != RealmFieldType.DATE) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'Date' for field 'created' in existing Realm file.");
         } else if(!var6.a(var7.c)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'created' is required. Either set @Required to field 'created' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var8.containsKey("type")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'type' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var8.get("type") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'type' in existing Realm file.");
         } else if(!var6.a(var7.d)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'type' is required. Either set @Required to field 'type' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var8.containsKey("appUri")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'appUri' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var8.get("appUri") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'appUri' in existing Realm file.");
         } else if(!var6.a(var7.e)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'appUri' is required. Either set @Required to field 'appUri' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var8.containsKey("goldenTicket")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'goldenTicket' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var8.get("goldenTicket") != RealmFieldType.OBJECT) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'GoldenTicket' for field 'goldenTicket'");
         } else if(!var0.a("class_GoldenTicket")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing class 'class_GoldenTicket' for field 'goldenTicket'");
         } else {
            Table var9 = var0.b("class_GoldenTicket");
            if(!var6.e(var7.f).a(var9)) {
               throw new RealmMigrationNeededException(var0.h(), "Invalid RealmObject for field 'goldenTicket': '" + var6.e(var7.f).j() + "' expected - was '" + var9.j() + "'");
            } else if(!var8.containsKey("monthlySpendingReport")) {
               throw new RealmMigrationNeededException(var0.h(), "Missing field 'monthlySpendingReport' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            } else if(var8.get("monthlySpendingReport") != RealmFieldType.OBJECT) {
               throw new RealmMigrationNeededException(var0.h(), "Invalid type 'MonthlySpendingReport' for field 'monthlySpendingReport'");
            } else if(!var0.a("class_MonthlySpendingReport")) {
               throw new RealmMigrationNeededException(var0.h(), "Missing class 'class_MonthlySpendingReport' for field 'monthlySpendingReport'");
            } else {
               var9 = var0.b("class_MonthlySpendingReport");
               if(!var6.e(var7.g).a(var9)) {
                  throw new RealmMigrationNeededException(var0.h(), "Invalid RealmObject for field 'monthlySpendingReport': '" + var6.e(var7.g).j() + "' expected - was '" + var9.j() + "'");
               } else if(!var8.containsKey("transaction")) {
                  throw new RealmMigrationNeededException(var0.h(), "Missing field 'transaction' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
               } else if(var8.get("transaction") != RealmFieldType.OBJECT) {
                  throw new RealmMigrationNeededException(var0.h(), "Invalid type 'Transaction' for field 'transaction'");
               } else if(!var0.a("class_Transaction")) {
                  throw new RealmMigrationNeededException(var0.h(), "Missing class 'class_Transaction' for field 'transaction'");
               } else {
                  var9 = var0.b("class_Transaction");
                  if(!var6.e(var7.h).a(var9)) {
                     throw new RealmMigrationNeededException(var0.h(), "Invalid RealmObject for field 'transaction': '" + var6.e(var7.h).j() + "' expected - was '" + var9.j() + "'");
                  } else if(!var8.containsKey("kycRejected")) {
                     throw new RealmMigrationNeededException(var0.h(), "Missing field 'kycRejected' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
                  } else if(var8.get("kycRejected") != RealmFieldType.OBJECT) {
                     throw new RealmMigrationNeededException(var0.h(), "Invalid type 'KycRejected' for field 'kycRejected'");
                  } else if(!var0.a("class_KycRejected")) {
                     throw new RealmMigrationNeededException(var0.h(), "Missing class 'class_KycRejected' for field 'kycRejected'");
                  } else {
                     var9 = var0.b("class_KycRejected");
                     if(!var6.e(var7.i).a(var9)) {
                        throw new RealmMigrationNeededException(var0.h(), "Invalid RealmObject for field 'kycRejected': '" + var6.e(var7.i).j() + "' expected - was '" + var9.j() + "'");
                     } else if(!var8.containsKey("sddRejectedReason")) {
                        throw new RealmMigrationNeededException(var0.h(), "Missing field 'sddRejectedReason' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
                     } else if(var8.get("sddRejectedReason") != RealmFieldType.STRING) {
                        throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'sddRejectedReason' in existing Realm file.");
                     } else if(!var6.a(var7.j)) {
                        throw new RealmMigrationNeededException(var0.h(), "Field 'sddRejectedReason' is required. Either set @Required to field 'sddRejectedReason' or migrate using RealmObjectSchema.setNullable().");
                     } else if(!var8.containsKey("basicItemInfo")) {
                        throw new RealmMigrationNeededException(var0.h(), "Missing field 'basicItemInfo' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
                     } else if(var8.get("basicItemInfo") != RealmFieldType.OBJECT) {
                        throw new RealmMigrationNeededException(var0.h(), "Invalid type 'BasicItemInfo' for field 'basicItemInfo'");
                     } else if(!var0.a("class_BasicItemInfo")) {
                        throw new RealmMigrationNeededException(var0.h(), "Missing class 'class_BasicItemInfo' for field 'basicItemInfo'");
                     } else {
                        var9 = var0.b("class_BasicItemInfo");
                        if(!var6.e(var7.k).a(var9)) {
                           throw new RealmMigrationNeededException(var0.h(), "Invalid RealmObject for field 'basicItemInfo': '" + var6.e(var7.k).j() + "' expected - was '" + var9.j() + "'");
                        } else if(!var8.containsKey("isDismissable")) {
                           throw new RealmMigrationNeededException(var0.h(), "Missing field 'isDismissable' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
                        } else if(var8.get("isDismissable") != RealmFieldType.BOOLEAN) {
                           throw new RealmMigrationNeededException(var0.h(), "Invalid type 'boolean' for field 'isDismissable' in existing Realm file.");
                        } else if(var6.a(var7.l)) {
                           throw new RealmMigrationNeededException(var0.h(), "Field 'isDismissable' does support null values in the existing Realm file. Use corresponding boxed type for field 'isDismissable' or migrate using RealmObjectSchema.setNullable().");
                        } else {
                           return var7;
                        }
                     }
                  }
               }
            }
         }
      }
   }

   public static void a(av var0, Iterator var1, Map var2) {
      Table var12 = var0.c(co.uk.getmondo.d.m.class);
      long var7 = var12.getNativePtr();
      s.a var13 = (s.a)var0.f.c(co.uk.getmondo.d.m.class);
      long var9 = var12.d();

      while(true) {
         while(true) {
            co.uk.getmondo.d.m var14;
            do {
               if(!var1.hasNext()) {
                  return;
               }

               var14 = (co.uk.getmondo.d.m)var1.next();
            } while(var2.containsKey(var14));

            if(var14 instanceof io.realm.internal.l && ((io.realm.internal.l)var14).s_().a() != null && ((io.realm.internal.l)var14).s_().a().g().equals(var0.g())) {
               var2.put(var14, Long.valueOf(((io.realm.internal.l)var14).s_().b().c()));
            } else {
               String var11 = ((t)var14).l();
               long var3;
               if(var11 == null) {
                  var3 = Table.nativeFindFirstNull(var7, var9);
               } else {
                  var3 = Table.nativeFindFirstString(var7, var9, var11);
               }

               long var5 = var3;
               if(var3 == -1L) {
                  var5 = OsObject.b(var12, var11);
               }

               var2.put(var14, Long.valueOf(var5));
               var11 = ((t)var14).m();
               if(var11 != null) {
                  Table.nativeSetString(var7, var13.b, var5, var11, false);
               } else {
                  Table.nativeSetNull(var7, var13.b, var5, false);
               }

               Date var16 = ((t)var14).n();
               if(var16 != null) {
                  Table.nativeSetTimestamp(var7, var13.c, var5, var16.getTime(), false);
               } else {
                  Table.nativeSetNull(var7, var13.c, var5, false);
               }

               var11 = ((t)var14).o();
               if(var11 != null) {
                  Table.nativeSetString(var7, var13.d, var5, var11, false);
               } else {
                  Table.nativeSetNull(var7, var13.d, var5, false);
               }

               var11 = ((t)var14).p();
               if(var11 != null) {
                  Table.nativeSetString(var7, var13.e, var5, var11, false);
               } else {
                  Table.nativeSetNull(var7, var13.e, var5, false);
               }

               co.uk.getmondo.d.o var15 = ((t)var14).q();
               Long var17;
               if(var15 != null) {
                  var17 = (Long)var2.get(var15);
                  if(var17 == null) {
                     var17 = Long.valueOf(y.b(var0, var15, var2));
                  }

                  Table.nativeSetLink(var7, var13.f, var5, var17.longValue(), false);
               } else {
                  Table.nativeNullifyLink(var7, var13.f, var5);
               }

               co.uk.getmondo.d.v var18 = ((t)var14).r();
               if(var18 != null) {
                  var17 = (Long)var2.get(var18);
                  if(var17 == null) {
                     var17 = Long.valueOf(ai.b(var0, var18, var2));
                  }

                  Table.nativeSetLink(var7, var13.g, var5, var17.longValue(), false);
               } else {
                  Table.nativeNullifyLink(var7, var13.g, var5);
               }

               co.uk.getmondo.d.aj var19 = ((t)var14).s();
               if(var19 != null) {
                  var17 = (Long)var2.get(var19);
                  if(var17 == null) {
                     var17 = Long.valueOf(bm.b(var0, var19, var2));
                  }

                  Table.nativeSetLink(var7, var13.h, var5, var17.longValue(), false);
               } else {
                  Table.nativeNullifyLink(var7, var13.h, var5);
               }

               co.uk.getmondo.d.r var20 = ((t)var14).t();
               if(var20 != null) {
                  var17 = (Long)var2.get(var20);
                  if(var17 == null) {
                     var17 = Long.valueOf(ac.b(var0, var20, var2));
                  }

                  Table.nativeSetLink(var7, var13.i, var5, var17.longValue(), false);
               } else {
                  Table.nativeNullifyLink(var7, var13.i, var5);
               }

               var11 = ((t)var14).u();
               if(var11 != null) {
                  Table.nativeSetString(var7, var13.j, var5, var11, false);
               } else {
                  Table.nativeSetNull(var7, var13.j, var5, false);
               }

               co.uk.getmondo.d.f var21 = ((t)var14).v();
               if(var21 != null) {
                  var17 = (Long)var2.get(var21);
                  if(var17 == null) {
                     var17 = Long.valueOf(h.b(var0, var21, var2));
                  }

                  Table.nativeSetLink(var7, var13.k, var5, var17.longValue(), false);
               } else {
                  Table.nativeNullifyLink(var7, var13.k, var5);
               }

               Table.nativeSetBoolean(var7, var13.l, var5, ((t)var14).w(), false);
            }
         }
      }
   }

   public static long b(av var0, co.uk.getmondo.d.m var1, Map var2) {
      long var5;
      if(var1 instanceof io.realm.internal.l && ((io.realm.internal.l)var1).s_().a() != null && ((io.realm.internal.l)var1).s_().a().g().equals(var0.g())) {
         var5 = ((io.realm.internal.l)var1).s_().b().c();
      } else {
         Table var9 = var0.c(co.uk.getmondo.d.m.class);
         long var7 = var9.getNativePtr();
         s.a var10 = (s.a)var0.f.c(co.uk.getmondo.d.m.class);
         long var3 = var9.d();
         String var11 = ((t)var1).l();
         if(var11 == null) {
            var3 = Table.nativeFindFirstNull(var7, var3);
         } else {
            var3 = Table.nativeFindFirstString(var7, var3, var11);
         }

         var5 = var3;
         if(var3 == -1L) {
            var5 = OsObject.b(var9, var11);
         }

         var2.put(var1, Long.valueOf(var5));
         String var13 = ((t)var1).m();
         if(var13 != null) {
            Table.nativeSetString(var7, var10.b, var5, var13, false);
         } else {
            Table.nativeSetNull(var7, var10.b, var5, false);
         }

         Date var14 = ((t)var1).n();
         if(var14 != null) {
            Table.nativeSetTimestamp(var7, var10.c, var5, var14.getTime(), false);
         } else {
            Table.nativeSetNull(var7, var10.c, var5, false);
         }

         var13 = ((t)var1).o();
         if(var13 != null) {
            Table.nativeSetString(var7, var10.d, var5, var13, false);
         } else {
            Table.nativeSetNull(var7, var10.d, var5, false);
         }

         var13 = ((t)var1).p();
         if(var13 != null) {
            Table.nativeSetString(var7, var10.e, var5, var13, false);
         } else {
            Table.nativeSetNull(var7, var10.e, var5, false);
         }

         co.uk.getmondo.d.o var16 = ((t)var1).q();
         Long var15;
         if(var16 != null) {
            var15 = (Long)var2.get(var16);
            if(var15 == null) {
               var15 = Long.valueOf(y.b(var0, var16, var2));
            }

            Table.nativeSetLink(var7, var10.f, var5, var15.longValue(), false);
         } else {
            Table.nativeNullifyLink(var7, var10.f, var5);
         }

         co.uk.getmondo.d.v var17 = ((t)var1).r();
         if(var17 != null) {
            var15 = (Long)var2.get(var17);
            if(var15 == null) {
               var15 = Long.valueOf(ai.b(var0, var17, var2));
            }

            Table.nativeSetLink(var7, var10.g, var5, var15.longValue(), false);
         } else {
            Table.nativeNullifyLink(var7, var10.g, var5);
         }

         co.uk.getmondo.d.aj var18 = ((t)var1).s();
         if(var18 != null) {
            var15 = (Long)var2.get(var18);
            if(var15 == null) {
               var15 = Long.valueOf(bm.b(var0, var18, var2));
            }

            Table.nativeSetLink(var7, var10.h, var5, var15.longValue(), false);
         } else {
            Table.nativeNullifyLink(var7, var10.h, var5);
         }

         co.uk.getmondo.d.r var19 = ((t)var1).t();
         if(var19 != null) {
            var15 = (Long)var2.get(var19);
            if(var15 == null) {
               var15 = Long.valueOf(ac.b(var0, var19, var2));
            }

            Table.nativeSetLink(var7, var10.i, var5, var15.longValue(), false);
         } else {
            Table.nativeNullifyLink(var7, var10.i, var5);
         }

         var13 = ((t)var1).u();
         if(var13 != null) {
            Table.nativeSetString(var7, var10.j, var5, var13, false);
         } else {
            Table.nativeSetNull(var7, var10.j, var5, false);
         }

         co.uk.getmondo.d.f var20 = ((t)var1).v();
         if(var20 != null) {
            var15 = (Long)var2.get(var20);
            Long var12;
            if(var15 == null) {
               var12 = Long.valueOf(h.b(var0, var20, var2));
            } else {
               var12 = var15;
            }

            Table.nativeSetLink(var7, var10.k, var5, var12.longValue(), false);
         } else {
            Table.nativeNullifyLink(var7, var10.k, var5);
         }

         Table.nativeSetBoolean(var7, var10.l, var5, ((t)var1).w(), false);
      }

      return var5;
   }

   public static co.uk.getmondo.d.m b(av var0, co.uk.getmondo.d.m var1, boolean var2, Map var3) {
      io.realm.internal.l var4 = (io.realm.internal.l)var3.get(var1);
      co.uk.getmondo.d.m var8;
      if(var4 != null) {
         var8 = (co.uk.getmondo.d.m)var4;
      } else {
         co.uk.getmondo.d.m var10 = (co.uk.getmondo.d.m)var0.a(co.uk.getmondo.d.m.class, ((t)var1).l(), false, Collections.emptyList());
         var3.put(var1, (io.realm.internal.l)var10);
         t var5 = (t)var1;
         t var9 = (t)var10;
         var9.b(var5.m());
         var9.a(var5.n());
         var9.c(var5.o());
         var9.d(var5.p());
         co.uk.getmondo.d.o var6 = var5.q();
         if(var6 == null) {
            var9.a((co.uk.getmondo.d.o)null);
         } else {
            co.uk.getmondo.d.o var7 = (co.uk.getmondo.d.o)var3.get(var6);
            if(var7 != null) {
               var9.a(var7);
            } else {
               var9.a(y.a(var0, var6, var2, var3));
            }
         }

         co.uk.getmondo.d.v var11 = var5.r();
         if(var11 == null) {
            var9.a((co.uk.getmondo.d.v)null);
         } else {
            co.uk.getmondo.d.v var14 = (co.uk.getmondo.d.v)var3.get(var11);
            if(var14 != null) {
               var9.a(var14);
            } else {
               var9.a(ai.a(var0, var11, var2, var3));
            }
         }

         co.uk.getmondo.d.aj var12 = var5.s();
         if(var12 == null) {
            var9.a((co.uk.getmondo.d.aj)null);
         } else {
            co.uk.getmondo.d.aj var16 = (co.uk.getmondo.d.aj)var3.get(var12);
            if(var16 != null) {
               var9.a(var16);
            } else {
               var9.a(bm.a(var0, var12, var2, var3));
            }
         }

         co.uk.getmondo.d.r var13 = var5.t();
         if(var13 == null) {
            var9.a((co.uk.getmondo.d.r)null);
         } else {
            co.uk.getmondo.d.r var17 = (co.uk.getmondo.d.r)var3.get(var13);
            if(var17 != null) {
               var9.a(var17);
            } else {
               var9.a(ac.a(var0, var13, var2, var3));
            }
         }

         var9.e(var5.u());
         co.uk.getmondo.d.f var15 = var5.v();
         if(var15 == null) {
            var9.a((co.uk.getmondo.d.f)null);
         } else {
            co.uk.getmondo.d.f var18 = (co.uk.getmondo.d.f)var3.get(var15);
            if(var18 != null) {
               var9.a(var18);
            } else {
               var9.a(h.a(var0, var15, var2, var3));
            }
         }

         var9.a(var5.w());
         var8 = var10;
      }

      return var8;
   }

   public static OsObjectSchemaInfo x() {
      return c;
   }

   public static String y() {
      return "class_FeedItem";
   }

   private static OsObjectSchemaInfo z() {
      OsObjectSchemaInfo.a var0 = new OsObjectSchemaInfo.a("FeedItem");
      var0.a("id", RealmFieldType.STRING, true, true, false);
      var0.a("accountId", RealmFieldType.STRING, false, false, false);
      var0.a("created", RealmFieldType.DATE, false, false, false);
      var0.a("type", RealmFieldType.STRING, false, false, false);
      var0.a("appUri", RealmFieldType.STRING, false, false, false);
      var0.a("goldenTicket", RealmFieldType.OBJECT, "GoldenTicket");
      var0.a("monthlySpendingReport", RealmFieldType.OBJECT, "MonthlySpendingReport");
      var0.a("transaction", RealmFieldType.OBJECT, "Transaction");
      var0.a("kycRejected", RealmFieldType.OBJECT, "KycRejected");
      var0.a("sddRejectedReason", RealmFieldType.STRING, false, false, false);
      var0.a("basicItemInfo", RealmFieldType.OBJECT, "BasicItemInfo");
      var0.a("isDismissable", RealmFieldType.BOOLEAN, false, false, true);
      return var0.a();
   }

   public void a(co.uk.getmondo.d.aj var1) {
      if(this.b.e()) {
         if(this.b.c() && !this.b.d().contains("transaction")) {
            if(var1 != null && !bc.c(var1)) {
               var1 = (co.uk.getmondo.d.aj)((av)this.b.a()).a((bb)var1);
            }

            io.realm.internal.n var2 = this.b.b();
            if(var1 == null) {
               var2.o(this.a.h);
            } else {
               if(!bc.b(var1)) {
                  throw new IllegalArgumentException("'value' is not a valid managed object.");
               }

               if(((io.realm.internal.l)var1).s_().a() != this.b.a()) {
                  throw new IllegalArgumentException("'value' belongs to a different Realm.");
               }

               var2.b().b(this.a.h, var2.c(), ((io.realm.internal.l)var1).s_().b().c(), true);
            }
         }
      } else {
         this.b.a().e();
         if(var1 == null) {
            this.b.b().o(this.a.h);
         } else {
            if(!bc.c(var1) || !bc.b(var1)) {
               throw new IllegalArgumentException("'value' is not a valid managed object.");
            }

            if(((io.realm.internal.l)var1).s_().a() != this.b.a()) {
               throw new IllegalArgumentException("'value' belongs to a different Realm.");
            }

            this.b.b().b(this.a.h, ((io.realm.internal.l)var1).s_().b().c());
         }
      }

   }

   public void a(co.uk.getmondo.d.f var1) {
      if(this.b.e()) {
         if(this.b.c() && !this.b.d().contains("basicItemInfo")) {
            if(var1 != null && !bc.c(var1)) {
               var1 = (co.uk.getmondo.d.f)((av)this.b.a()).a((bb)var1);
            }

            io.realm.internal.n var2 = this.b.b();
            if(var1 == null) {
               var2.o(this.a.k);
            } else {
               if(!bc.b(var1)) {
                  throw new IllegalArgumentException("'value' is not a valid managed object.");
               }

               if(((io.realm.internal.l)var1).s_().a() != this.b.a()) {
                  throw new IllegalArgumentException("'value' belongs to a different Realm.");
               }

               var2.b().b(this.a.k, var2.c(), ((io.realm.internal.l)var1).s_().b().c(), true);
            }
         }
      } else {
         this.b.a().e();
         if(var1 == null) {
            this.b.b().o(this.a.k);
         } else {
            if(!bc.c(var1) || !bc.b(var1)) {
               throw new IllegalArgumentException("'value' is not a valid managed object.");
            }

            if(((io.realm.internal.l)var1).s_().a() != this.b.a()) {
               throw new IllegalArgumentException("'value' belongs to a different Realm.");
            }

            this.b.b().b(this.a.k, ((io.realm.internal.l)var1).s_().b().c());
         }
      }

   }

   public void a(co.uk.getmondo.d.o var1) {
      if(this.b.e()) {
         if(this.b.c() && !this.b.d().contains("goldenTicket")) {
            if(var1 != null && !bc.c(var1)) {
               var1 = (co.uk.getmondo.d.o)((av)this.b.a()).a((bb)var1);
            }

            io.realm.internal.n var2 = this.b.b();
            if(var1 == null) {
               var2.o(this.a.f);
            } else {
               if(!bc.b(var1)) {
                  throw new IllegalArgumentException("'value' is not a valid managed object.");
               }

               if(((io.realm.internal.l)var1).s_().a() != this.b.a()) {
                  throw new IllegalArgumentException("'value' belongs to a different Realm.");
               }

               var2.b().b(this.a.f, var2.c(), ((io.realm.internal.l)var1).s_().b().c(), true);
            }
         }
      } else {
         this.b.a().e();
         if(var1 == null) {
            this.b.b().o(this.a.f);
         } else {
            if(!bc.c(var1) || !bc.b(var1)) {
               throw new IllegalArgumentException("'value' is not a valid managed object.");
            }

            if(((io.realm.internal.l)var1).s_().a() != this.b.a()) {
               throw new IllegalArgumentException("'value' belongs to a different Realm.");
            }

            this.b.b().b(this.a.f, ((io.realm.internal.l)var1).s_().b().c());
         }
      }

   }

   public void a(co.uk.getmondo.d.r var1) {
      if(this.b.e()) {
         if(this.b.c() && !this.b.d().contains("kycRejected")) {
            if(var1 != null && !bc.c(var1)) {
               var1 = (co.uk.getmondo.d.r)((av)this.b.a()).a((bb)var1);
            }

            io.realm.internal.n var2 = this.b.b();
            if(var1 == null) {
               var2.o(this.a.i);
            } else {
               if(!bc.b(var1)) {
                  throw new IllegalArgumentException("'value' is not a valid managed object.");
               }

               if(((io.realm.internal.l)var1).s_().a() != this.b.a()) {
                  throw new IllegalArgumentException("'value' belongs to a different Realm.");
               }

               var2.b().b(this.a.i, var2.c(), ((io.realm.internal.l)var1).s_().b().c(), true);
            }
         }
      } else {
         this.b.a().e();
         if(var1 == null) {
            this.b.b().o(this.a.i);
         } else {
            if(!bc.c(var1) || !bc.b(var1)) {
               throw new IllegalArgumentException("'value' is not a valid managed object.");
            }

            if(((io.realm.internal.l)var1).s_().a() != this.b.a()) {
               throw new IllegalArgumentException("'value' belongs to a different Realm.");
            }

            this.b.b().b(this.a.i, ((io.realm.internal.l)var1).s_().b().c());
         }
      }

   }

   public void a(co.uk.getmondo.d.v var1) {
      if(this.b.e()) {
         if(this.b.c() && !this.b.d().contains("monthlySpendingReport")) {
            if(var1 != null && !bc.c(var1)) {
               var1 = (co.uk.getmondo.d.v)((av)this.b.a()).a((bb)var1);
            }

            io.realm.internal.n var2 = this.b.b();
            if(var1 == null) {
               var2.o(this.a.g);
            } else {
               if(!bc.b(var1)) {
                  throw new IllegalArgumentException("'value' is not a valid managed object.");
               }

               if(((io.realm.internal.l)var1).s_().a() != this.b.a()) {
                  throw new IllegalArgumentException("'value' belongs to a different Realm.");
               }

               var2.b().b(this.a.g, var2.c(), ((io.realm.internal.l)var1).s_().b().c(), true);
            }
         }
      } else {
         this.b.a().e();
         if(var1 == null) {
            this.b.b().o(this.a.g);
         } else {
            if(!bc.c(var1) || !bc.b(var1)) {
               throw new IllegalArgumentException("'value' is not a valid managed object.");
            }

            if(((io.realm.internal.l)var1).s_().a() != this.b.a()) {
               throw new IllegalArgumentException("'value' belongs to a different Realm.");
            }

            this.b.b().b(this.a.g, ((io.realm.internal.l)var1).s_().b().c());
         }
      }

   }

   public void a(String var1) {
      if(!this.b.e()) {
         this.b.a().e();
         throw new RealmException("Primary key field 'id' cannot be changed after object was created.");
      }
   }

   public void a(Date var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            if(var1 == null) {
               var2.b().a(this.a.c, var2.c(), true);
            } else {
               var2.b().a(this.a.c, var2.c(), var1, true);
            }
         }
      } else {
         this.b.a().e();
         if(var1 == null) {
            this.b.b().c(this.a.c);
         } else {
            this.b.b().a(this.a.c, var1);
         }
      }

   }

   public void a(boolean var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            var2.b().a(this.a.l, var2.c(), var1, true);
         }
      } else {
         this.b.a().e();
         this.b.b().a(this.a.l, var1);
      }

   }

   public void b(String var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            if(var1 == null) {
               var2.b().a(this.a.b, var2.c(), true);
            } else {
               var2.b().a(this.a.b, var2.c(), var1, true);
            }
         }
      } else {
         this.b.a().e();
         if(var1 == null) {
            this.b.b().c(this.a.b);
         } else {
            this.b.b().a(this.a.b, var1);
         }
      }

   }

   public void c(String var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            if(var1 == null) {
               var2.b().a(this.a.d, var2.c(), true);
            } else {
               var2.b().a(this.a.d, var2.c(), var1, true);
            }
         }
      } else {
         this.b.a().e();
         if(var1 == null) {
            this.b.b().c(this.a.d);
         } else {
            this.b.b().a(this.a.d, var1);
         }
      }

   }

   public void d(String var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            if(var1 == null) {
               var2.b().a(this.a.e, var2.c(), true);
            } else {
               var2.b().a(this.a.e, var2.c(), var1, true);
            }
         }
      } else {
         this.b.a().e();
         if(var1 == null) {
            this.b.b().c(this.a.e);
         } else {
            this.b.b().a(this.a.e, var1);
         }
      }

   }

   public void e(String var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            if(var1 == null) {
               var2.b().a(this.a.j, var2.c(), true);
            } else {
               var2.b().a(this.a.j, var2.c(), var1, true);
            }
         }
      } else {
         this.b.a().e();
         if(var1 == null) {
            this.b.b().c(this.a.j);
         } else {
            this.b.b().a(this.a.j, var1);
         }
      }

   }

   public String l() {
      this.b.a().e();
      return this.b.b().k(this.a.a);
   }

   public String m() {
      this.b.a().e();
      return this.b.b().k(this.a.b);
   }

   public Date n() {
      this.b.a().e();
      Date var1;
      if(this.b.b().b(this.a.c)) {
         var1 = null;
      } else {
         var1 = this.b.b().j(this.a.c);
      }

      return var1;
   }

   public String o() {
      this.b.a().e();
      return this.b.b().k(this.a.d);
   }

   public String p() {
      this.b.a().e();
      return this.b.b().k(this.a.e);
   }

   public co.uk.getmondo.d.o q() {
      this.b.a().e();
      co.uk.getmondo.d.o var1;
      if(this.b.b().a(this.a.f)) {
         var1 = null;
      } else {
         var1 = (co.uk.getmondo.d.o)this.b.a().a(co.uk.getmondo.d.o.class, this.b.b().m(this.a.f), false, Collections.emptyList());
      }

      return var1;
   }

   public co.uk.getmondo.d.v r() {
      this.b.a().e();
      co.uk.getmondo.d.v var1;
      if(this.b.b().a(this.a.g)) {
         var1 = null;
      } else {
         var1 = (co.uk.getmondo.d.v)this.b.a().a(co.uk.getmondo.d.v.class, this.b.b().m(this.a.g), false, Collections.emptyList());
      }

      return var1;
   }

   public co.uk.getmondo.d.aj s() {
      this.b.a().e();
      co.uk.getmondo.d.aj var1;
      if(this.b.b().a(this.a.h)) {
         var1 = null;
      } else {
         var1 = (co.uk.getmondo.d.aj)this.b.a().a(co.uk.getmondo.d.aj.class, this.b.b().m(this.a.h), false, Collections.emptyList());
      }

      return var1;
   }

   public au s_() {
      return this.b;
   }

   public co.uk.getmondo.d.r t() {
      this.b.a().e();
      co.uk.getmondo.d.r var1;
      if(this.b.b().a(this.a.i)) {
         var1 = null;
      } else {
         var1 = (co.uk.getmondo.d.r)this.b.a().a(co.uk.getmondo.d.r.class, this.b.b().m(this.a.i), false, Collections.emptyList());
      }

      return var1;
   }

   public String toString() {
      String var1;
      if(!bc.b(this)) {
         var1 = "Invalid object";
      } else {
         StringBuilder var2 = new StringBuilder("FeedItem = proxy[");
         var2.append("{id:");
         if(this.l() != null) {
            var1 = this.l();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{accountId:");
         if(this.m() != null) {
            var1 = this.m();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{created:");
         Object var3;
         if(this.n() != null) {
            var3 = this.n();
         } else {
            var3 = "null";
         }

         var2.append(var3);
         var2.append("}");
         var2.append(",");
         var2.append("{type:");
         if(this.o() != null) {
            var1 = this.o();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{appUri:");
         if(this.p() != null) {
            var1 = this.p();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{goldenTicket:");
         if(this.q() != null) {
            var1 = "GoldenTicket";
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{monthlySpendingReport:");
         if(this.r() != null) {
            var1 = "MonthlySpendingReport";
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{transaction:");
         if(this.s() != null) {
            var1 = "Transaction";
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{kycRejected:");
         if(this.t() != null) {
            var1 = "KycRejected";
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{sddRejectedReason:");
         if(this.u() != null) {
            var1 = this.u();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{basicItemInfo:");
         if(this.v() != null) {
            var1 = "BasicItemInfo";
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{isDismissable:");
         var2.append(this.w());
         var2.append("}");
         var2.append("]");
         var1 = var2.toString();
      }

      return var1;
   }

   public String u() {
      this.b.a().e();
      return this.b.b().k(this.a.j);
   }

   public void u_() {
      if(this.b == null) {
         g.b var1 = (g.b)g.g.get();
         this.a = (s.a)var1.c();
         this.b = new au(this);
         this.b.a(var1.a());
         this.b.a(var1.b());
         this.b.a(var1.d());
         this.b.a(var1.e());
      }

   }

   public co.uk.getmondo.d.f v() {
      this.b.a().e();
      co.uk.getmondo.d.f var1;
      if(this.b.b().a(this.a.k)) {
         var1 = null;
      } else {
         var1 = (co.uk.getmondo.d.f)this.b.a().a(co.uk.getmondo.d.f.class, this.b.b().m(this.a.k), false, Collections.emptyList());
      }

      return var1;
   }

   public boolean w() {
      this.b.a().e();
      return this.b.b().g(this.a.l);
   }

   static final class a extends io.realm.internal.c {
      long a;
      long b;
      long c;
      long d;
      long e;
      long f;
      long g;
      long h;
      long i;
      long j;
      long k;
      long l;

      a(SharedRealm var1, Table var2) {
         super(12);
         this.a = this.a(var2, "id", RealmFieldType.STRING);
         this.b = this.a(var2, "accountId", RealmFieldType.STRING);
         this.c = this.a(var2, "created", RealmFieldType.DATE);
         this.d = this.a(var2, "type", RealmFieldType.STRING);
         this.e = this.a(var2, "appUri", RealmFieldType.STRING);
         this.f = this.a(var2, "goldenTicket", RealmFieldType.OBJECT);
         this.g = this.a(var2, "monthlySpendingReport", RealmFieldType.OBJECT);
         this.h = this.a(var2, "transaction", RealmFieldType.OBJECT);
         this.i = this.a(var2, "kycRejected", RealmFieldType.OBJECT);
         this.j = this.a(var2, "sddRejectedReason", RealmFieldType.STRING);
         this.k = this.a(var2, "basicItemInfo", RealmFieldType.OBJECT);
         this.l = this.a(var2, "isDismissable", RealmFieldType.BOOLEAN);
      }

      a(io.realm.internal.c var1, boolean var2) {
         super(var1, var2);
         this.a(var1, this);
      }

      protected final io.realm.internal.c a(boolean var1) {
         return new s.a(this, var1);
      }

      protected final void a(io.realm.internal.c var1, io.realm.internal.c var2) {
         s.a var3 = (s.a)var1;
         s.a var4 = (s.a)var2;
         var4.a = var3.a;
         var4.b = var3.b;
         var4.c = var3.c;
         var4.d = var3.d;
         var4.e = var3.e;
         var4.f = var3.f;
         var4.g = var3.g;
         var4.h = var3.h;
         var4.i = var3.i;
         var4.j = var3.j;
         var4.k = var3.k;
         var4.l = var3.l;
      }
   }
}
