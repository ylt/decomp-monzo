package io.realm;

import io.realm.exceptions.RealmException;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.SharedRealm;
import io.realm.internal.Table;
import io.realm.log.RealmLog;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class bo extends co.uk.getmondo.d.am implements bp, io.realm.internal.l {
   private static final OsObjectSchemaInfo c = m();
   private static final List d;
   private bo.a a;
   private au b;

   static {
      ArrayList var0 = new ArrayList();
      var0.add("id");
      var0.add("statusId");
      var0.add("monzoMeUsername");
      var0.add("inboundP2P");
      var0.add("prepaidAccountMigrated");
      d = Collections.unmodifiableList(var0);
   }

   bo() {
      this.b.f();
   }

   public static long a(av var0, co.uk.getmondo.d.am var1, Map var2) {
      long var3;
      if(var1 instanceof io.realm.internal.l && ((io.realm.internal.l)var1).s_().a() != null && ((io.realm.internal.l)var1).s_().a().g().equals(var0.g())) {
         var3 = ((io.realm.internal.l)var1).s_().b().c();
      } else {
         Table var9 = var0.c(co.uk.getmondo.d.am.class);
         long var5 = var9.getNativePtr();
         bo.a var8 = (bo.a)var0.f.c(co.uk.getmondo.d.am.class);
         var3 = var9.d();
         String var7 = ((bp)var1).e();
         if(var7 == null) {
            var3 = Table.nativeFindFirstNull(var5, var3);
         } else {
            var3 = Table.nativeFindFirstString(var5, var3, var7);
         }

         if(var3 == -1L) {
            var3 = OsObject.b(var9, var7);
         } else {
            Table.a((Object)var7);
         }

         var2.put(var1, Long.valueOf(var3));
         var7 = ((bp)var1).f();
         if(var7 != null) {
            Table.nativeSetString(var5, var8.b, var3, var7, false);
         }

         var7 = ((bp)var1).g();
         if(var7 != null) {
            Table.nativeSetString(var5, var8.c, var3, var7, false);
         }

         co.uk.getmondo.d.p var12 = ((bp)var1).h();
         if(var12 != null) {
            Long var11 = (Long)var2.get(var12);
            Long var10;
            if(var11 == null) {
               var10 = Long.valueOf(aa.a(var0, var12, var2));
            } else {
               var10 = var11;
            }

            Table.nativeSetLink(var5, var8.d, var3, var10.longValue(), false);
         }

         Table.nativeSetBoolean(var5, var8.e, var3, ((bp)var1).i(), false);
      }

      return var3;
   }

   public static co.uk.getmondo.d.am a(co.uk.getmondo.d.am var0, int var1, int var2, Map var3) {
      if(var1 <= var2 && var0 != null) {
         io.realm.internal.l.a var5 = (io.realm.internal.l.a)var3.get(var0);
         co.uk.getmondo.d.am var4;
         if(var5 == null) {
            var4 = new co.uk.getmondo.d.am();
            var3.put(var0, new io.realm.internal.l.a(var1, var4));
         } else {
            if(var1 >= var5.a) {
               var0 = (co.uk.getmondo.d.am)var5.b;
               return var0;
            }

            var4 = (co.uk.getmondo.d.am)var5.b;
            var5.a = var1;
         }

         bp var6 = (bp)var4;
         bp var7 = (bp)var0;
         var6.a(var7.e());
         var6.b(var7.f());
         var6.c(var7.g());
         var6.a(aa.a(var7.h(), var1 + 1, var2, var3));
         var6.a(var7.i());
         var0 = var4;
      } else {
         var0 = null;
      }

      return var0;
   }

   static co.uk.getmondo.d.am a(av var0, co.uk.getmondo.d.am var1, co.uk.getmondo.d.am var2, Map var3) {
      bp var4 = (bp)var1;
      bp var6 = (bp)var2;
      var4.b(var6.f());
      var4.c(var6.g());
      co.uk.getmondo.d.p var5 = var6.h();
      if(var5 == null) {
         var4.a((co.uk.getmondo.d.p)null);
      } else {
         co.uk.getmondo.d.p var7 = (co.uk.getmondo.d.p)var3.get(var5);
         if(var7 != null) {
            var4.a(var7);
         } else {
            var4.a(aa.a(var0, var5, true, var3));
         }
      }

      var4.a(var6.i());
      return var1;
   }

   public static co.uk.getmondo.d.am a(av var0, co.uk.getmondo.d.am var1, boolean var2, Map var3) {
      if(var1 instanceof io.realm.internal.l && ((io.realm.internal.l)var1).s_().a() != null && ((io.realm.internal.l)var1).s_().a().c != var0.c) {
         throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
      } else {
         if(!(var1 instanceof io.realm.internal.l) || ((io.realm.internal.l)var1).s_().a() == null || !((io.realm.internal.l)var1).s_().a().g().equals(var0.g())) {
            g.b var8 = (g.b)g.g.get();
            io.realm.internal.l var7 = (io.realm.internal.l)var3.get(var1);
            if(var7 != null) {
               var1 = (co.uk.getmondo.d.am)var7;
            } else {
               boolean var4;
               bo var13;
               if(var2) {
                  Table var9 = var0.c(co.uk.getmondo.d.am.class);
                  long var5 = var9.d();
                  String var12 = ((bp)var1).e();
                  if(var12 == null) {
                     var5 = var9.k(var5);
                  } else {
                     var5 = var9.a(var5, var12);
                  }

                  if(var5 != -1L) {
                     try {
                        var8.a(var0, var9.f(var5), var0.f.c(co.uk.getmondo.d.am.class), false, Collections.emptyList());
                        var13 = new bo();
                        var3.put(var1, (io.realm.internal.l)var13);
                     } finally {
                        var8.f();
                     }

                     var4 = var2;
                  } else {
                     var4 = false;
                     var13 = null;
                  }
               } else {
                  var4 = var2;
                  var13 = null;
               }

               if(var4) {
                  var1 = a(var0, var13, var1, var3);
               } else {
                  var1 = b(var0, var1, var2, var3);
               }
            }
         }

         return var1;
      }
   }

   public static bo.a a(SharedRealm var0, boolean var1) {
      if(!var0.a("class_UserSettings")) {
         throw new RealmMigrationNeededException(var0.h(), "The 'UserSettings' class is missing from the schema for this Realm.");
      } else {
         Table var6 = var0.b("class_UserSettings");
         long var4 = var6.c();
         if(var4 != 5L) {
            if(var4 < 5L) {
               throw new RealmMigrationNeededException(var0.h(), "Field count is less than expected - expected 5 but was " + var4);
            }

            if(!var1) {
               throw new RealmMigrationNeededException(var0.h(), "Field count is more than expected - expected 5 but was " + var4);
            }

            RealmLog.a("Field count is more than expected - expected 5 but was %1$d", new Object[]{Long.valueOf(var4)});
         }

         HashMap var7 = new HashMap();

         for(long var2 = 0L; var2 < var4; ++var2) {
            var7.put(var6.b(var2), var6.c(var2));
         }

         bo.a var9 = new bo.a(var0, var6);
         if(!var6.e()) {
            throw new RealmMigrationNeededException(var0.h(), "Primary key not defined for field 'id' in existing Realm file. @PrimaryKey was added.");
         } else if(var6.d() != var9.a) {
            throw new RealmMigrationNeededException(var0.h(), "Primary Key annotation definition was changed, from field " + var6.b(var6.d()) + " to field id");
         } else if(!var7.containsKey("id")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'id' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var7.get("id") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'id' in existing Realm file.");
         } else if(!var6.a(var9.a)) {
            throw new RealmMigrationNeededException(var0.h(), "@PrimaryKey field 'id' does not support null values in the existing Realm file. Migrate using RealmObjectSchema.setNullable(), or mark the field as @Required.");
         } else if(!var6.j(var6.a("id"))) {
            throw new RealmMigrationNeededException(var0.h(), "Index not defined for field 'id' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex().");
         } else if(!var7.containsKey("statusId")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'statusId' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var7.get("statusId") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'statusId' in existing Realm file.");
         } else if(!var6.a(var9.b)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'statusId' is required. Either set @Required to field 'statusId' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var7.containsKey("monzoMeUsername")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'monzoMeUsername' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var7.get("monzoMeUsername") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'monzoMeUsername' in existing Realm file.");
         } else if(!var6.a(var9.c)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'monzoMeUsername' is required. Either set @Required to field 'monzoMeUsername' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var7.containsKey("inboundP2P")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'inboundP2P' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var7.get("inboundP2P") != RealmFieldType.OBJECT) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'InboundP2P' for field 'inboundP2P'");
         } else if(!var0.a("class_InboundP2P")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing class 'class_InboundP2P' for field 'inboundP2P'");
         } else {
            Table var8 = var0.b("class_InboundP2P");
            if(!var6.e(var9.d).a(var8)) {
               throw new RealmMigrationNeededException(var0.h(), "Invalid RealmObject for field 'inboundP2P': '" + var6.e(var9.d).j() + "' expected - was '" + var8.j() + "'");
            } else if(!var7.containsKey("prepaidAccountMigrated")) {
               throw new RealmMigrationNeededException(var0.h(), "Missing field 'prepaidAccountMigrated' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            } else if(var7.get("prepaidAccountMigrated") != RealmFieldType.BOOLEAN) {
               throw new RealmMigrationNeededException(var0.h(), "Invalid type 'boolean' for field 'prepaidAccountMigrated' in existing Realm file.");
            } else if(var6.a(var9.e)) {
               throw new RealmMigrationNeededException(var0.h(), "Field 'prepaidAccountMigrated' does support null values in the existing Realm file. Use corresponding boxed type for field 'prepaidAccountMigrated' or migrate using RealmObjectSchema.setNullable().");
            } else {
               return var9;
            }
         }
      }
   }

   public static void a(av var0, Iterator var1, Map var2) {
      Table var12 = var0.c(co.uk.getmondo.d.am.class);
      long var9 = var12.getNativePtr();
      bo.a var13 = (bo.a)var0.f.c(co.uk.getmondo.d.am.class);
      long var7 = var12.d();

      while(true) {
         while(true) {
            co.uk.getmondo.d.am var14;
            do {
               if(!var1.hasNext()) {
                  return;
               }

               var14 = (co.uk.getmondo.d.am)var1.next();
            } while(var2.containsKey(var14));

            if(var14 instanceof io.realm.internal.l && ((io.realm.internal.l)var14).s_().a() != null && ((io.realm.internal.l)var14).s_().a().g().equals(var0.g())) {
               var2.put(var14, Long.valueOf(((io.realm.internal.l)var14).s_().b().c()));
            } else {
               String var11 = ((bp)var14).e();
               long var3;
               if(var11 == null) {
                  var3 = Table.nativeFindFirstNull(var9, var7);
               } else {
                  var3 = Table.nativeFindFirstString(var9, var7, var11);
               }

               long var5 = var3;
               if(var3 == -1L) {
                  var5 = OsObject.b(var12, var11);
               }

               var2.put(var14, Long.valueOf(var5));
               var11 = ((bp)var14).f();
               if(var11 != null) {
                  Table.nativeSetString(var9, var13.b, var5, var11, false);
               } else {
                  Table.nativeSetNull(var9, var13.b, var5, false);
               }

               var11 = ((bp)var14).g();
               if(var11 != null) {
                  Table.nativeSetString(var9, var13.c, var5, var11, false);
               } else {
                  Table.nativeSetNull(var9, var13.c, var5, false);
               }

               co.uk.getmondo.d.p var15 = ((bp)var14).h();
               if(var15 != null) {
                  Long var16 = (Long)var2.get(var15);
                  if(var16 == null) {
                     var16 = Long.valueOf(aa.b(var0, var15, var2));
                  }

                  Table.nativeSetLink(var9, var13.d, var5, var16.longValue(), false);
               } else {
                  Table.nativeNullifyLink(var9, var13.d, var5);
               }

               Table.nativeSetBoolean(var9, var13.e, var5, ((bp)var14).i(), false);
            }
         }
      }
   }

   public static long b(av var0, co.uk.getmondo.d.am var1, Map var2) {
      long var5;
      if(var1 instanceof io.realm.internal.l && ((io.realm.internal.l)var1).s_().a() != null && ((io.realm.internal.l)var1).s_().a().g().equals(var0.g())) {
         var5 = ((io.realm.internal.l)var1).s_().b().c();
      } else {
         Table var11 = var0.c(co.uk.getmondo.d.am.class);
         long var7 = var11.getNativePtr();
         bo.a var10 = (bo.a)var0.f.c(co.uk.getmondo.d.am.class);
         long var3 = var11.d();
         String var9 = ((bp)var1).e();
         if(var9 == null) {
            var3 = Table.nativeFindFirstNull(var7, var3);
         } else {
            var3 = Table.nativeFindFirstString(var7, var3, var9);
         }

         var5 = var3;
         if(var3 == -1L) {
            var5 = OsObject.b(var11, var9);
         }

         var2.put(var1, Long.valueOf(var5));
         var9 = ((bp)var1).f();
         if(var9 != null) {
            Table.nativeSetString(var7, var10.b, var5, var9, false);
         } else {
            Table.nativeSetNull(var7, var10.b, var5, false);
         }

         var9 = ((bp)var1).g();
         if(var9 != null) {
            Table.nativeSetString(var7, var10.c, var5, var9, false);
         } else {
            Table.nativeSetNull(var7, var10.c, var5, false);
         }

         co.uk.getmondo.d.p var13 = ((bp)var1).h();
         if(var13 != null) {
            Long var14 = (Long)var2.get(var13);
            Long var12;
            if(var14 == null) {
               var12 = Long.valueOf(aa.b(var0, var13, var2));
            } else {
               var12 = var14;
            }

            Table.nativeSetLink(var7, var10.d, var5, var12.longValue(), false);
         } else {
            Table.nativeNullifyLink(var7, var10.d, var5);
         }

         Table.nativeSetBoolean(var7, var10.e, var5, ((bp)var1).i(), false);
      }

      return var5;
   }

   public static co.uk.getmondo.d.am b(av var0, co.uk.getmondo.d.am var1, boolean var2, Map var3) {
      io.realm.internal.l var4 = (io.realm.internal.l)var3.get(var1);
      co.uk.getmondo.d.am var8;
      if(var4 != null) {
         var8 = (co.uk.getmondo.d.am)var4;
      } else {
         co.uk.getmondo.d.am var10 = (co.uk.getmondo.d.am)var0.a(co.uk.getmondo.d.am.class, ((bp)var1).e(), false, Collections.emptyList());
         var3.put(var1, (io.realm.internal.l)var10);
         bp var5 = (bp)var1;
         bp var7 = (bp)var10;
         var7.b(var5.f());
         var7.c(var5.g());
         co.uk.getmondo.d.p var6 = var5.h();
         if(var6 == null) {
            var7.a((co.uk.getmondo.d.p)null);
         } else {
            co.uk.getmondo.d.p var9 = (co.uk.getmondo.d.p)var3.get(var6);
            if(var9 != null) {
               var7.a(var9);
            } else {
               var7.a(aa.a(var0, var6, var2, var3));
            }
         }

         var7.a(var5.i());
         var8 = var10;
      }

      return var8;
   }

   public static OsObjectSchemaInfo j() {
      return c;
   }

   public static String l() {
      return "class_UserSettings";
   }

   private static OsObjectSchemaInfo m() {
      OsObjectSchemaInfo.a var0 = new OsObjectSchemaInfo.a("UserSettings");
      var0.a("id", RealmFieldType.STRING, true, true, false);
      var0.a("statusId", RealmFieldType.STRING, false, false, false);
      var0.a("monzoMeUsername", RealmFieldType.STRING, false, false, false);
      var0.a("inboundP2P", RealmFieldType.OBJECT, "InboundP2P");
      var0.a("prepaidAccountMigrated", RealmFieldType.BOOLEAN, false, false, true);
      return var0.a();
   }

   public void a(co.uk.getmondo.d.p var1) {
      if(this.b.e()) {
         if(this.b.c() && !this.b.d().contains("inboundP2P")) {
            if(var1 != null && !bc.c(var1)) {
               var1 = (co.uk.getmondo.d.p)((av)this.b.a()).a((bb)var1);
            }

            io.realm.internal.n var2 = this.b.b();
            if(var1 == null) {
               var2.o(this.a.d);
            } else {
               if(!bc.b(var1)) {
                  throw new IllegalArgumentException("'value' is not a valid managed object.");
               }

               if(((io.realm.internal.l)var1).s_().a() != this.b.a()) {
                  throw new IllegalArgumentException("'value' belongs to a different Realm.");
               }

               var2.b().b(this.a.d, var2.c(), ((io.realm.internal.l)var1).s_().b().c(), true);
            }
         }
      } else {
         this.b.a().e();
         if(var1 == null) {
            this.b.b().o(this.a.d);
         } else {
            if(!bc.c(var1) || !bc.b(var1)) {
               throw new IllegalArgumentException("'value' is not a valid managed object.");
            }

            if(((io.realm.internal.l)var1).s_().a() != this.b.a()) {
               throw new IllegalArgumentException("'value' belongs to a different Realm.");
            }

            this.b.b().b(this.a.d, ((io.realm.internal.l)var1).s_().b().c());
         }
      }

   }

   public void a(String var1) {
      if(!this.b.e()) {
         this.b.a().e();
         throw new RealmException("Primary key field 'id' cannot be changed after object was created.");
      }
   }

   public void a(boolean var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            var2.b().a(this.a.e, var2.c(), var1, true);
         }
      } else {
         this.b.a().e();
         this.b.b().a(this.a.e, var1);
      }

   }

   public void b(String var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            if(var1 == null) {
               var2.b().a(this.a.b, var2.c(), true);
            } else {
               var2.b().a(this.a.b, var2.c(), var1, true);
            }
         }
      } else {
         this.b.a().e();
         if(var1 == null) {
            this.b.b().c(this.a.b);
         } else {
            this.b.b().a(this.a.b, var1);
         }
      }

   }

   public void c(String var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            if(var1 == null) {
               var2.b().a(this.a.c, var2.c(), true);
            } else {
               var2.b().a(this.a.c, var2.c(), var1, true);
            }
         }
      } else {
         this.b.a().e();
         if(var1 == null) {
            this.b.b().c(this.a.c);
         } else {
            this.b.b().a(this.a.c, var1);
         }
      }

   }

   public String e() {
      this.b.a().e();
      return this.b.b().k(this.a.a);
   }

   public String f() {
      this.b.a().e();
      return this.b.b().k(this.a.b);
   }

   public String g() {
      this.b.a().e();
      return this.b.b().k(this.a.c);
   }

   public co.uk.getmondo.d.p h() {
      this.b.a().e();
      co.uk.getmondo.d.p var1;
      if(this.b.b().a(this.a.d)) {
         var1 = null;
      } else {
         var1 = (co.uk.getmondo.d.p)this.b.a().a(co.uk.getmondo.d.p.class, this.b.b().m(this.a.d), false, Collections.emptyList());
      }

      return var1;
   }

   public boolean i() {
      this.b.a().e();
      return this.b.b().g(this.a.e);
   }

   public au s_() {
      return this.b;
   }

   public String toString() {
      String var1;
      if(!bc.b(this)) {
         var1 = "Invalid object";
      } else {
         StringBuilder var2 = new StringBuilder("UserSettings = proxy[");
         var2.append("{id:");
         if(this.e() != null) {
            var1 = this.e();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{statusId:");
         if(this.f() != null) {
            var1 = this.f();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{monzoMeUsername:");
         if(this.g() != null) {
            var1 = this.g();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{inboundP2P:");
         if(this.h() != null) {
            var1 = "InboundP2P";
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{prepaidAccountMigrated:");
         var2.append(this.i());
         var2.append("}");
         var2.append("]");
         var1 = var2.toString();
      }

      return var1;
   }

   public void u_() {
      if(this.b == null) {
         g.b var1 = (g.b)g.g.get();
         this.a = (bo.a)var1.c();
         this.b = new au(this);
         this.b.a(var1.a());
         this.b.a(var1.b());
         this.b.a(var1.d());
         this.b.a(var1.e());
      }

   }

   static final class a extends io.realm.internal.c {
      long a;
      long b;
      long c;
      long d;
      long e;

      a(SharedRealm var1, Table var2) {
         super(5);
         this.a = this.a(var2, "id", RealmFieldType.STRING);
         this.b = this.a(var2, "statusId", RealmFieldType.STRING);
         this.c = this.a(var2, "monzoMeUsername", RealmFieldType.STRING);
         this.d = this.a(var2, "inboundP2P", RealmFieldType.OBJECT);
         this.e = this.a(var2, "prepaidAccountMigrated", RealmFieldType.BOOLEAN);
      }

      a(io.realm.internal.c var1, boolean var2) {
         super(var1, var2);
         this.a(var1, this);
      }

      protected final io.realm.internal.c a(boolean var1) {
         return new bo.a(this, var1);
      }

      protected final void a(io.realm.internal.c var1, io.realm.internal.c var2) {
         bo.a var3 = (bo.a)var1;
         bo.a var4 = (bo.a)var2;
         var4.a = var3.a;
         var4.b = var3.b;
         var4.c = var3.c;
         var4.d = var3.d;
         var4.e = var3.e;
      }
   }
}
