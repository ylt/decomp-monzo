package io.realm;

import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.SharedRealm;
import io.realm.internal.Table;
import io.realm.log.RealmLog;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class q extends co.uk.getmondo.d.l implements io.realm.internal.l, r {
   private static final OsObjectSchemaInfo c = f();
   private static final List d;
   private q.a a;
   private au b;

   static {
      ArrayList var0 = new ArrayList();
      var0.add("currentAccountP2pEnabled");
      var0.add("potsEnabled");
      d = Collections.unmodifiableList(var0);
   }

   q() {
      this.b.f();
   }

   public static long a(av var0, co.uk.getmondo.d.l var1, Map var2) {
      long var3;
      if(var1 instanceof io.realm.internal.l && ((io.realm.internal.l)var1).s_().a() != null && ((io.realm.internal.l)var1).s_().a().g().equals(var0.g())) {
         var3 = ((io.realm.internal.l)var1).s_().b().c();
      } else {
         Table var7 = var0.c(co.uk.getmondo.d.l.class);
         long var5 = var7.getNativePtr();
         q.a var8 = (q.a)var0.f.c(co.uk.getmondo.d.l.class);
         var3 = OsObject.b(var7);
         var2.put(var1, Long.valueOf(var3));
         Table.nativeSetBoolean(var5, var8.a, var3, ((r)var1).b(), false);
         Table.nativeSetBoolean(var5, var8.b, var3, ((r)var1).c(), false);
      }

      return var3;
   }

   public static co.uk.getmondo.d.l a(co.uk.getmondo.d.l var0, int var1, int var2, Map var3) {
      if(var1 <= var2 && var0 != null) {
         io.realm.internal.l.a var4 = (io.realm.internal.l.a)var3.get(var0);
         co.uk.getmondo.d.l var6;
         if(var4 == null) {
            co.uk.getmondo.d.l var7 = new co.uk.getmondo.d.l();
            var3.put(var0, new io.realm.internal.l.a(var1, var7));
            var6 = var7;
         } else {
            if(var1 >= var4.a) {
               var0 = (co.uk.getmondo.d.l)var4.b;
               return var0;
            }

            var6 = (co.uk.getmondo.d.l)var4.b;
            var4.a = var1;
         }

         r var8 = (r)var6;
         r var5 = (r)var0;
         var8.a(var5.b());
         var8.b(var5.c());
         var0 = var6;
      } else {
         var0 = null;
      }

      return var0;
   }

   public static co.uk.getmondo.d.l a(av var0, co.uk.getmondo.d.l var1, boolean var2, Map var3) {
      if(var1 instanceof io.realm.internal.l && ((io.realm.internal.l)var1).s_().a() != null && ((io.realm.internal.l)var1).s_().a().c != var0.c) {
         throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
      } else {
         if(!(var1 instanceof io.realm.internal.l) || ((io.realm.internal.l)var1).s_().a() == null || !((io.realm.internal.l)var1).s_().a().g().equals(var0.g())) {
            g.b var4 = (g.b)g.g.get();
            io.realm.internal.l var5 = (io.realm.internal.l)var3.get(var1);
            if(var5 != null) {
               var1 = (co.uk.getmondo.d.l)var5;
            } else {
               var1 = b(var0, var1, var2, var3);
            }
         }

         return var1;
      }
   }

   public static q.a a(SharedRealm var0, boolean var1) {
      if(!var0.a("class_FeatureFlags")) {
         throw new RealmMigrationNeededException(var0.h(), "The 'FeatureFlags' class is missing from the schema for this Realm.");
      } else {
         Table var6 = var0.b("class_FeatureFlags");
         long var4 = var6.c();
         if(var4 != 2L) {
            if(var4 < 2L) {
               throw new RealmMigrationNeededException(var0.h(), "Field count is less than expected - expected 2 but was " + var4);
            }

            if(!var1) {
               throw new RealmMigrationNeededException(var0.h(), "Field count is more than expected - expected 2 but was " + var4);
            }

            RealmLog.a("Field count is more than expected - expected 2 but was %1$d", new Object[]{Long.valueOf(var4)});
         }

         HashMap var8 = new HashMap();

         for(long var2 = 0L; var2 < var4; ++var2) {
            var8.put(var6.b(var2), var6.c(var2));
         }

         q.a var7 = new q.a(var0, var6);
         if(var6.e()) {
            throw new RealmMigrationNeededException(var0.h(), "Primary Key defined for field " + var6.b(var6.d()) + " was removed.");
         } else if(!var8.containsKey("currentAccountP2pEnabled")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'currentAccountP2pEnabled' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var8.get("currentAccountP2pEnabled") != RealmFieldType.BOOLEAN) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'boolean' for field 'currentAccountP2pEnabled' in existing Realm file.");
         } else if(var6.a(var7.a)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'currentAccountP2pEnabled' does support null values in the existing Realm file. Use corresponding boxed type for field 'currentAccountP2pEnabled' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var8.containsKey("potsEnabled")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'potsEnabled' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var8.get("potsEnabled") != RealmFieldType.BOOLEAN) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'boolean' for field 'potsEnabled' in existing Realm file.");
         } else if(var6.a(var7.b)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'potsEnabled' does support null values in the existing Realm file. Use corresponding boxed type for field 'potsEnabled' or migrate using RealmObjectSchema.setNullable().");
         } else {
            return var7;
         }
      }
   }

   public static void a(av var0, Iterator var1, Map var2) {
      Table var9 = var0.c(co.uk.getmondo.d.l.class);
      long var5 = var9.getNativePtr();
      q.a var7 = (q.a)var0.f.c(co.uk.getmondo.d.l.class);

      while(true) {
         while(true) {
            co.uk.getmondo.d.l var8;
            do {
               if(!var1.hasNext()) {
                  return;
               }

               var8 = (co.uk.getmondo.d.l)var1.next();
            } while(var2.containsKey(var8));

            if(var8 instanceof io.realm.internal.l && ((io.realm.internal.l)var8).s_().a() != null && ((io.realm.internal.l)var8).s_().a().g().equals(var0.g())) {
               var2.put(var8, Long.valueOf(((io.realm.internal.l)var8).s_().b().c()));
            } else {
               long var3 = OsObject.b(var9);
               var2.put(var8, Long.valueOf(var3));
               Table.nativeSetBoolean(var5, var7.a, var3, ((r)var8).b(), false);
               Table.nativeSetBoolean(var5, var7.b, var3, ((r)var8).c(), false);
            }
         }
      }
   }

   public static long b(av var0, co.uk.getmondo.d.l var1, Map var2) {
      long var3;
      if(var1 instanceof io.realm.internal.l && ((io.realm.internal.l)var1).s_().a() != null && ((io.realm.internal.l)var1).s_().a().g().equals(var0.g())) {
         var3 = ((io.realm.internal.l)var1).s_().b().c();
      } else {
         Table var7 = var0.c(co.uk.getmondo.d.l.class);
         long var5 = var7.getNativePtr();
         q.a var8 = (q.a)var0.f.c(co.uk.getmondo.d.l.class);
         var3 = OsObject.b(var7);
         var2.put(var1, Long.valueOf(var3));
         Table.nativeSetBoolean(var5, var8.a, var3, ((r)var1).b(), false);
         Table.nativeSetBoolean(var5, var8.b, var3, ((r)var1).c(), false);
      }

      return var3;
   }

   public static co.uk.getmondo.d.l b(av var0, co.uk.getmondo.d.l var1, boolean var2, Map var3) {
      io.realm.internal.l var4 = (io.realm.internal.l)var3.get(var1);
      co.uk.getmondo.d.l var5;
      if(var4 != null) {
         var5 = (co.uk.getmondo.d.l)var4;
      } else {
         var5 = (co.uk.getmondo.d.l)var0.a(co.uk.getmondo.d.l.class, false, Collections.emptyList());
         var3.put(var1, (io.realm.internal.l)var5);
         r var6 = (r)var1;
         r var7 = (r)var5;
         var7.a(var6.b());
         var7.b(var6.c());
      }

      return var5;
   }

   public static OsObjectSchemaInfo d() {
      return c;
   }

   public static String e() {
      return "class_FeatureFlags";
   }

   private static OsObjectSchemaInfo f() {
      OsObjectSchemaInfo.a var0 = new OsObjectSchemaInfo.a("FeatureFlags");
      var0.a("currentAccountP2pEnabled", RealmFieldType.BOOLEAN, false, false, true);
      var0.a("potsEnabled", RealmFieldType.BOOLEAN, false, false, true);
      return var0.a();
   }

   public void a(boolean var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            var2.b().a(this.a.a, var2.c(), var1, true);
         }
      } else {
         this.b.a().e();
         this.b.b().a(this.a.a, var1);
      }

   }

   public void b(boolean var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            var2.b().a(this.a.b, var2.c(), var1, true);
         }
      } else {
         this.b.a().e();
         this.b.b().a(this.a.b, var1);
      }

   }

   public boolean b() {
      this.b.a().e();
      return this.b.b().g(this.a.a);
   }

   public boolean c() {
      this.b.a().e();
      return this.b.b().g(this.a.b);
   }

   public au s_() {
      return this.b;
   }

   public String toString() {
      String var1;
      if(!bc.b(this)) {
         var1 = "Invalid object";
      } else {
         StringBuilder var2 = new StringBuilder("FeatureFlags = proxy[");
         var2.append("{currentAccountP2pEnabled:");
         var2.append(this.b());
         var2.append("}");
         var2.append(",");
         var2.append("{potsEnabled:");
         var2.append(this.c());
         var2.append("}");
         var2.append("]");
         var1 = var2.toString();
      }

      return var1;
   }

   public void u_() {
      if(this.b == null) {
         g.b var1 = (g.b)g.g.get();
         this.a = (q.a)var1.c();
         this.b = new au(this);
         this.b.a(var1.a());
         this.b.a(var1.b());
         this.b.a(var1.d());
         this.b.a(var1.e());
      }

   }

   static final class a extends io.realm.internal.c {
      long a;
      long b;

      a(SharedRealm var1, Table var2) {
         super(2);
         this.a = this.a(var2, "currentAccountP2pEnabled", RealmFieldType.BOOLEAN);
         this.b = this.a(var2, "potsEnabled", RealmFieldType.BOOLEAN);
      }

      a(io.realm.internal.c var1, boolean var2) {
         super(var1, var2);
         this.a(var1, this);
      }

      protected final io.realm.internal.c a(boolean var1) {
         return new q.a(this, var1);
      }

      protected final void a(io.realm.internal.c var1, io.realm.internal.c var2) {
         q.a var3 = (q.a)var1;
         q.a var4 = (q.a)var2;
         var4.a = var3.a;
         var4.b = var3.b;
      }
   }
}
