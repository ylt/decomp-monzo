package io.realm;

import java.util.Arrays;
import java.util.Locale;

public class p extends bc implements io.realm.internal.l {
   private final au a = new au(this);

   p(g var1, io.realm.internal.n var2) {
      this.a.a(var1);
      this.a.a(var2);
      this.a.f();
   }

   public String[] a() {
      this.a.a().e();
      String[] var2 = new String[(int)this.a.b().a()];

      for(int var1 = 0; var1 < var2.length; ++var1) {
         var2[var1] = this.a.b().d((long)var1);
      }

      return var2;
   }

   public String b() {
      this.a.a().e();
      return this.a.b().b().k();
   }

   public boolean equals(Object var1) {
      boolean var3 = true;
      boolean var4 = false;
      this.a.a().e();
      boolean var2;
      if(this == var1) {
         var2 = true;
      } else {
         var2 = var4;
         if(var1 != null) {
            var2 = var4;
            if(this.getClass() == var1.getClass()) {
               p var7 = (p)var1;
               String var6 = this.a.a().g();
               String var5 = var7.a.a().g();
               if(var6 != null) {
                  var2 = var4;
                  if(!var6.equals(var5)) {
                     return var2;
                  }
               } else if(var5 != null) {
                  var2 = var4;
                  return var2;
               }

               var6 = this.a.b().b().j();
               var5 = var7.a.b().b().j();
               if(var6 != null) {
                  var2 = var4;
                  if(!var6.equals(var5)) {
                     return var2;
                  }
               } else if(var5 != null) {
                  var2 = var4;
                  return var2;
               }

               if(this.a.b().c() == var7.a.b().c()) {
                  var2 = var3;
               } else {
                  var2 = false;
               }
            }
         }
      }

      return var2;
   }

   public int hashCode() {
      int var2 = 0;
      this.a.a().e();
      String var6 = this.a.a().g();
      String var5 = this.a.b().b().j();
      long var3 = this.a.b().c();
      int var1;
      if(var6 != null) {
         var1 = var6.hashCode();
      } else {
         var1 = 0;
      }

      if(var5 != null) {
         var2 = var5.hashCode();
      }

      return (var2 + (var1 + 527) * 31) * 31 + (int)(var3 >>> 32 ^ var3);
   }

   public au s_() {
      return this.a;
   }

   public String toString() {
      this.a.a().e();
      String var5;
      if(!this.a.b().d()) {
         var5 = "Invalid object";
      } else {
         var5 = this.a.b().b().k();
         StringBuilder var6 = new StringBuilder(var5 + " = dynamic[");
         String[] var7 = this.a();
         int var2 = var7.length;

         for(int var1 = 0; var1 < var2; ++var1) {
            String var8 = var7[var1];
            long var3 = this.a.b().a(var8);
            RealmFieldType var9 = this.a.b().e(var3);
            var6.append("{");
            var6.append(var8).append(":");
            Object var10;
            switch(null.a[var9.ordinal()]) {
            case 1:
               if(this.a.b().b(var3)) {
                  var10 = "null";
               } else {
                  var10 = Boolean.valueOf(this.a.b().g(var3));
               }

               var6.append(var10);
               break;
            case 2:
               if(this.a.b().b(var3)) {
                  var10 = "null";
               } else {
                  var10 = Long.valueOf(this.a.b().f(var3));
               }

               var6.append(var10);
               break;
            case 3:
               if(this.a.b().b(var3)) {
                  var10 = "null";
               } else {
                  var10 = Float.valueOf(this.a.b().h(var3));
               }

               var6.append(var10);
               break;
            case 4:
               if(this.a.b().b(var3)) {
                  var10 = "null";
               } else {
                  var10 = Double.valueOf(this.a.b().i(var3));
               }

               var6.append(var10);
               break;
            case 5:
               var6.append(this.a.b().k(var3));
               break;
            case 6:
               var6.append(Arrays.toString(this.a.b().l(var3)));
               break;
            case 7:
               if(this.a.b().b(var3)) {
                  var10 = "null";
               } else {
                  var10 = this.a.b().j(var3);
               }

               var6.append(var10);
               break;
            case 8:
               if(this.a.b().a(var3)) {
                  var5 = "null";
               } else {
                  var5 = this.a.b().b().e(var3).k();
               }

               var6.append(var5);
               break;
            case 9:
               var5 = this.a.b().b().e(var3).k();
               var6.append(String.format(Locale.US, "RealmList<%s>[%s]", new Object[]{var5, Long.valueOf(this.a.b().n(var3).b())}));
               break;
            default:
               var6.append("?");
            }

            var6.append("},");
         }

         var6.replace(var6.length() - 1, var6.length(), "");
         var6.append("]");
         var5 = var6.toString();
      }

      return var5;
   }

   public void u_() {
   }
}
