package io.realm;

import io.realm.exceptions.RealmException;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.SharedRealm;
import io.realm.internal.Table;
import io.realm.log.RealmLog;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class aq extends co.uk.getmondo.payments.a.a.e implements ar, io.realm.internal.l {
   private static final OsObjectSchemaInfo d = r();
   private static final List e;
   private aq.a b;
   private au c;

   static {
      ArrayList var0 = new ArrayList();
      var0.add("id");
      var0.add("accountId");
      var0.add("scheme");
      var0.add("schemaData");
      var0.add("_amount");
      var0.add("_currency");
      var0.add("_startDate");
      var0.add("_nextIterationDate");
      var0.add("_endDate");
      var0.add("_intervalType");
      e = Collections.unmodifiableList(var0);
   }

   aq() {
      this.c.f();
   }

   public static long a(av var0, co.uk.getmondo.payments.a.a.e var1, Map var2) {
      long var5;
      if(var1 instanceof io.realm.internal.l && ((io.realm.internal.l)var1).s_().a() != null && ((io.realm.internal.l)var1).s_().a().g().equals(var0.g())) {
         var5 = ((io.realm.internal.l)var1).s_().b().c();
      } else {
         Table var9 = var0.c(co.uk.getmondo.payments.a.a.e.class);
         long var7 = var9.getNativePtr();
         aq.a var10 = (aq.a)var0.f.c(co.uk.getmondo.payments.a.a.e.class);
         long var3 = var9.d();
         String var11 = ((ar)var1).f();
         if(var11 == null) {
            var3 = Table.nativeFindFirstNull(var7, var3);
         } else {
            var3 = Table.nativeFindFirstString(var7, var3, var11);
         }

         if(var3 == -1L) {
            var3 = OsObject.b(var9, var11);
         } else {
            Table.a((Object)var11);
         }

         var2.put(var1, Long.valueOf(var3));
         String var14 = ((ar)var1).g();
         if(var14 != null) {
            Table.nativeSetString(var7, var10.b, var3, var14, false);
         }

         var14 = ((ar)var1).h();
         if(var14 != null) {
            Table.nativeSetString(var7, var10.c, var3, var14, false);
         }

         co.uk.getmondo.payments.a.a.b var16 = ((ar)var1).i();
         if(var16 != null) {
            Long var15 = (Long)var2.get(var16);
            Long var12;
            if(var15 == null) {
               var12 = Long.valueOf(w.a(var0, var16, var2));
            } else {
               var12 = var15;
            }

            Table.nativeSetLink(var7, var10.d, var3, var12.longValue(), false);
         }

         Table.nativeSetLong(var7, var10.e, var3, ((ar)var1).j(), false);
         String var13 = ((ar)var1).k();
         if(var13 != null) {
            Table.nativeSetString(var7, var10.f, var3, var13, false);
         }

         var13 = ((ar)var1).l();
         if(var13 != null) {
            Table.nativeSetString(var7, var10.g, var3, var13, false);
         }

         var13 = ((ar)var1).m();
         if(var13 != null) {
            Table.nativeSetString(var7, var10.h, var3, var13, false);
         }

         var13 = ((ar)var1).n();
         if(var13 != null) {
            Table.nativeSetString(var7, var10.i, var3, var13, false);
         }

         var13 = ((ar)var1).o();
         var5 = var3;
         if(var13 != null) {
            Table.nativeSetString(var7, var10.j, var3, var13, false);
            var5 = var3;
         }
      }

      return var5;
   }

   public static co.uk.getmondo.payments.a.a.e a(co.uk.getmondo.payments.a.a.e var0, int var1, int var2, Map var3) {
      if(var1 <= var2 && var0 != null) {
         io.realm.internal.l.a var5 = (io.realm.internal.l.a)var3.get(var0);
         co.uk.getmondo.payments.a.a.e var4;
         if(var5 == null) {
            var4 = new co.uk.getmondo.payments.a.a.e();
            var3.put(var0, new io.realm.internal.l.a(var1, var4));
         } else {
            if(var1 >= var5.a) {
               var0 = (co.uk.getmondo.payments.a.a.e)var5.b;
               return var0;
            }

            var4 = (co.uk.getmondo.payments.a.a.e)var5.b;
            var5.a = var1;
         }

         ar var6 = (ar)var4;
         ar var7 = (ar)var0;
         var6.a(var7.f());
         var6.b(var7.g());
         var6.c(var7.h());
         var6.a(w.a(var7.i(), var1 + 1, var2, var3));
         var6.a(var7.j());
         var6.d(var7.k());
         var6.e(var7.l());
         var6.f(var7.m());
         var6.g(var7.n());
         var6.h(var7.o());
         var0 = var4;
      } else {
         var0 = null;
      }

      return var0;
   }

   static co.uk.getmondo.payments.a.a.e a(av var0, co.uk.getmondo.payments.a.a.e var1, co.uk.getmondo.payments.a.a.e var2, Map var3) {
      ar var4 = (ar)var1;
      ar var5 = (ar)var2;
      var4.b(var5.g());
      var4.c(var5.h());
      co.uk.getmondo.payments.a.a.b var6 = var5.i();
      if(var6 == null) {
         var4.a((co.uk.getmondo.payments.a.a.b)null);
      } else {
         co.uk.getmondo.payments.a.a.b var7 = (co.uk.getmondo.payments.a.a.b)var3.get(var6);
         if(var7 != null) {
            var4.a(var7);
         } else {
            var4.a(w.a(var0, var6, true, var3));
         }
      }

      var4.a(var5.j());
      var4.d(var5.k());
      var4.e(var5.l());
      var4.f(var5.m());
      var4.g(var5.n());
      var4.h(var5.o());
      return var1;
   }

   public static co.uk.getmondo.payments.a.a.e a(av var0, co.uk.getmondo.payments.a.a.e var1, boolean var2, Map var3) {
      if(var1 instanceof io.realm.internal.l && ((io.realm.internal.l)var1).s_().a() != null && ((io.realm.internal.l)var1).s_().a().c != var0.c) {
         throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
      } else {
         if(!(var1 instanceof io.realm.internal.l) || ((io.realm.internal.l)var1).s_().a() == null || !((io.realm.internal.l)var1).s_().a().g().equals(var0.g())) {
            g.b var8 = (g.b)g.g.get();
            io.realm.internal.l var7 = (io.realm.internal.l)var3.get(var1);
            if(var7 != null) {
               var1 = (co.uk.getmondo.payments.a.a.e)var7;
            } else {
               boolean var4;
               aq var13;
               if(var2) {
                  Table var9 = var0.c(co.uk.getmondo.payments.a.a.e.class);
                  long var5 = var9.d();
                  String var12 = ((ar)var1).f();
                  if(var12 == null) {
                     var5 = var9.k(var5);
                  } else {
                     var5 = var9.a(var5, var12);
                  }

                  if(var5 != -1L) {
                     try {
                        var8.a(var0, var9.f(var5), var0.f.c(co.uk.getmondo.payments.a.a.e.class), false, Collections.emptyList());
                        var13 = new aq();
                        var3.put(var1, (io.realm.internal.l)var13);
                     } finally {
                        var8.f();
                     }

                     var4 = var2;
                  } else {
                     var4 = false;
                     var13 = null;
                  }
               } else {
                  var4 = var2;
                  var13 = null;
               }

               if(var4) {
                  var1 = a(var0, var13, var1, var3);
               } else {
                  var1 = b(var0, var1, var2, var3);
               }
            }
         }

         return var1;
      }
   }

   public static aq.a a(SharedRealm var0, boolean var1) {
      if(!var0.a("class_PaymentSeries")) {
         throw new RealmMigrationNeededException(var0.h(), "The 'PaymentSeries' class is missing from the schema for this Realm.");
      } else {
         Table var6 = var0.b("class_PaymentSeries");
         long var4 = var6.c();
         if(var4 != 10L) {
            if(var4 < 10L) {
               throw new RealmMigrationNeededException(var0.h(), "Field count is less than expected - expected 10 but was " + var4);
            }

            if(!var1) {
               throw new RealmMigrationNeededException(var0.h(), "Field count is more than expected - expected 10 but was " + var4);
            }

            RealmLog.a("Field count is more than expected - expected 10 but was %1$d", new Object[]{Long.valueOf(var4)});
         }

         HashMap var8 = new HashMap();

         for(long var2 = 0L; var2 < var4; ++var2) {
            var8.put(var6.b(var2), var6.c(var2));
         }

         aq.a var7 = new aq.a(var0, var6);
         if(!var6.e()) {
            throw new RealmMigrationNeededException(var0.h(), "Primary key not defined for field 'id' in existing Realm file. @PrimaryKey was added.");
         } else if(var6.d() != var7.a) {
            throw new RealmMigrationNeededException(var0.h(), "Primary Key annotation definition was changed, from field " + var6.b(var6.d()) + " to field id");
         } else if(!var8.containsKey("id")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'id' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var8.get("id") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'id' in existing Realm file.");
         } else if(!var6.a(var7.a)) {
            throw new RealmMigrationNeededException(var0.h(), "@PrimaryKey field 'id' does not support null values in the existing Realm file. Migrate using RealmObjectSchema.setNullable(), or mark the field as @Required.");
         } else if(!var6.j(var6.a("id"))) {
            throw new RealmMigrationNeededException(var0.h(), "Index not defined for field 'id' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex().");
         } else if(!var8.containsKey("accountId")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'accountId' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var8.get("accountId") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'accountId' in existing Realm file.");
         } else if(!var6.a(var7.b)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'accountId' is required. Either set @Required to field 'accountId' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var8.containsKey("scheme")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'scheme' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var8.get("scheme") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'scheme' in existing Realm file.");
         } else if(!var6.a(var7.c)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'scheme' is required. Either set @Required to field 'scheme' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var8.containsKey("schemaData")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'schemaData' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var8.get("schemaData") != RealmFieldType.OBJECT) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'FpsSchemaData' for field 'schemaData'");
         } else if(!var0.a("class_FpsSchemaData")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing class 'class_FpsSchemaData' for field 'schemaData'");
         } else {
            Table var9 = var0.b("class_FpsSchemaData");
            if(!var6.e(var7.d).a(var9)) {
               throw new RealmMigrationNeededException(var0.h(), "Invalid RealmObject for field 'schemaData': '" + var6.e(var7.d).j() + "' expected - was '" + var9.j() + "'");
            } else if(!var8.containsKey("_amount")) {
               throw new RealmMigrationNeededException(var0.h(), "Missing field '_amount' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            } else if(var8.get("_amount") != RealmFieldType.INTEGER) {
               throw new RealmMigrationNeededException(var0.h(), "Invalid type 'long' for field '_amount' in existing Realm file.");
            } else if(var6.a(var7.e)) {
               throw new RealmMigrationNeededException(var0.h(), "Field '_amount' does support null values in the existing Realm file. Use corresponding boxed type for field '_amount' or migrate using RealmObjectSchema.setNullable().");
            } else if(!var8.containsKey("_currency")) {
               throw new RealmMigrationNeededException(var0.h(), "Missing field '_currency' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            } else if(var8.get("_currency") != RealmFieldType.STRING) {
               throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field '_currency' in existing Realm file.");
            } else if(!var6.a(var7.f)) {
               throw new RealmMigrationNeededException(var0.h(), "Field '_currency' is required. Either set @Required to field '_currency' or migrate using RealmObjectSchema.setNullable().");
            } else if(!var8.containsKey("_startDate")) {
               throw new RealmMigrationNeededException(var0.h(), "Missing field '_startDate' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            } else if(var8.get("_startDate") != RealmFieldType.STRING) {
               throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field '_startDate' in existing Realm file.");
            } else if(!var6.a(var7.g)) {
               throw new RealmMigrationNeededException(var0.h(), "Field '_startDate' is required. Either set @Required to field '_startDate' or migrate using RealmObjectSchema.setNullable().");
            } else if(!var8.containsKey("_nextIterationDate")) {
               throw new RealmMigrationNeededException(var0.h(), "Missing field '_nextIterationDate' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            } else if(var8.get("_nextIterationDate") != RealmFieldType.STRING) {
               throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field '_nextIterationDate' in existing Realm file.");
            } else if(!var6.a(var7.h)) {
               throw new RealmMigrationNeededException(var0.h(), "Field '_nextIterationDate' is required. Either set @Required to field '_nextIterationDate' or migrate using RealmObjectSchema.setNullable().");
            } else if(!var8.containsKey("_endDate")) {
               throw new RealmMigrationNeededException(var0.h(), "Missing field '_endDate' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            } else if(var8.get("_endDate") != RealmFieldType.STRING) {
               throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field '_endDate' in existing Realm file.");
            } else if(!var6.a(var7.i)) {
               throw new RealmMigrationNeededException(var0.h(), "Field '_endDate' is required. Either set @Required to field '_endDate' or migrate using RealmObjectSchema.setNullable().");
            } else if(!var8.containsKey("_intervalType")) {
               throw new RealmMigrationNeededException(var0.h(), "Missing field '_intervalType' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            } else if(var8.get("_intervalType") != RealmFieldType.STRING) {
               throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field '_intervalType' in existing Realm file.");
            } else if(!var6.a(var7.j)) {
               throw new RealmMigrationNeededException(var0.h(), "Field '_intervalType' is required. Either set @Required to field '_intervalType' or migrate using RealmObjectSchema.setNullable().");
            } else {
               return var7;
            }
         }
      }
   }

   public static void a(av var0, Iterator var1, Map var2) {
      Table var12 = var0.c(co.uk.getmondo.payments.a.a.e.class);
      long var9 = var12.getNativePtr();
      aq.a var13 = (aq.a)var0.f.c(co.uk.getmondo.payments.a.a.e.class);
      long var7 = var12.d();

      while(true) {
         while(true) {
            co.uk.getmondo.payments.a.a.e var14;
            do {
               if(!var1.hasNext()) {
                  return;
               }

               var14 = (co.uk.getmondo.payments.a.a.e)var1.next();
            } while(var2.containsKey(var14));

            if(var14 instanceof io.realm.internal.l && ((io.realm.internal.l)var14).s_().a() != null && ((io.realm.internal.l)var14).s_().a().g().equals(var0.g())) {
               var2.put(var14, Long.valueOf(((io.realm.internal.l)var14).s_().b().c()));
            } else {
               String var11 = ((ar)var14).f();
               long var3;
               if(var11 == null) {
                  var3 = Table.nativeFindFirstNull(var9, var7);
               } else {
                  var3 = Table.nativeFindFirstString(var9, var7, var11);
               }

               long var5 = var3;
               if(var3 == -1L) {
                  var5 = OsObject.b(var12, var11);
               }

               var2.put(var14, Long.valueOf(var5));
               var11 = ((ar)var14).g();
               if(var11 != null) {
                  Table.nativeSetString(var9, var13.b, var5, var11, false);
               } else {
                  Table.nativeSetNull(var9, var13.b, var5, false);
               }

               var11 = ((ar)var14).h();
               if(var11 != null) {
                  Table.nativeSetString(var9, var13.c, var5, var11, false);
               } else {
                  Table.nativeSetNull(var9, var13.c, var5, false);
               }

               co.uk.getmondo.payments.a.a.b var15 = ((ar)var14).i();
               if(var15 != null) {
                  Long var16 = (Long)var2.get(var15);
                  if(var16 == null) {
                     var16 = Long.valueOf(w.b(var0, var15, var2));
                  }

                  Table.nativeSetLink(var9, var13.d, var5, var16.longValue(), false);
               } else {
                  Table.nativeNullifyLink(var9, var13.d, var5);
               }

               Table.nativeSetLong(var9, var13.e, var5, ((ar)var14).j(), false);
               var11 = ((ar)var14).k();
               if(var11 != null) {
                  Table.nativeSetString(var9, var13.f, var5, var11, false);
               } else {
                  Table.nativeSetNull(var9, var13.f, var5, false);
               }

               var11 = ((ar)var14).l();
               if(var11 != null) {
                  Table.nativeSetString(var9, var13.g, var5, var11, false);
               } else {
                  Table.nativeSetNull(var9, var13.g, var5, false);
               }

               var11 = ((ar)var14).m();
               if(var11 != null) {
                  Table.nativeSetString(var9, var13.h, var5, var11, false);
               } else {
                  Table.nativeSetNull(var9, var13.h, var5, false);
               }

               var11 = ((ar)var14).n();
               if(var11 != null) {
                  Table.nativeSetString(var9, var13.i, var5, var11, false);
               } else {
                  Table.nativeSetNull(var9, var13.i, var5, false);
               }

               var11 = ((ar)var14).o();
               if(var11 != null) {
                  Table.nativeSetString(var9, var13.j, var5, var11, false);
               } else {
                  Table.nativeSetNull(var9, var13.j, var5, false);
               }
            }
         }
      }
   }

   public static long b(av var0, co.uk.getmondo.payments.a.a.e var1, Map var2) {
      long var3;
      if(var1 instanceof io.realm.internal.l && ((io.realm.internal.l)var1).s_().a() != null && ((io.realm.internal.l)var1).s_().a().g().equals(var0.g())) {
         var3 = ((io.realm.internal.l)var1).s_().b().c();
      } else {
         Table var9 = var0.c(co.uk.getmondo.payments.a.a.e.class);
         long var7 = var9.getNativePtr();
         aq.a var10 = (aq.a)var0.f.c(co.uk.getmondo.payments.a.a.e.class);
         var3 = var9.d();
         String var11 = ((ar)var1).f();
         long var5;
         if(var11 == null) {
            var5 = Table.nativeFindFirstNull(var7, var3);
         } else {
            var5 = Table.nativeFindFirstString(var7, var3, var11);
         }

         var3 = var5;
         if(var5 == -1L) {
            var3 = OsObject.b(var9, var11);
         }

         var2.put(var1, Long.valueOf(var3));
         String var14 = ((ar)var1).g();
         if(var14 != null) {
            Table.nativeSetString(var7, var10.b, var3, var14, false);
         } else {
            Table.nativeSetNull(var7, var10.b, var3, false);
         }

         var14 = ((ar)var1).h();
         if(var14 != null) {
            Table.nativeSetString(var7, var10.c, var3, var14, false);
         } else {
            Table.nativeSetNull(var7, var10.c, var3, false);
         }

         co.uk.getmondo.payments.a.a.b var16 = ((ar)var1).i();
         if(var16 != null) {
            Long var15 = (Long)var2.get(var16);
            Long var12;
            if(var15 == null) {
               var12 = Long.valueOf(w.b(var0, var16, var2));
            } else {
               var12 = var15;
            }

            Table.nativeSetLink(var7, var10.d, var3, var12.longValue(), false);
         } else {
            Table.nativeNullifyLink(var7, var10.d, var3);
         }

         Table.nativeSetLong(var7, var10.e, var3, ((ar)var1).j(), false);
         String var13 = ((ar)var1).k();
         if(var13 != null) {
            Table.nativeSetString(var7, var10.f, var3, var13, false);
         } else {
            Table.nativeSetNull(var7, var10.f, var3, false);
         }

         var13 = ((ar)var1).l();
         if(var13 != null) {
            Table.nativeSetString(var7, var10.g, var3, var13, false);
         } else {
            Table.nativeSetNull(var7, var10.g, var3, false);
         }

         var13 = ((ar)var1).m();
         if(var13 != null) {
            Table.nativeSetString(var7, var10.h, var3, var13, false);
         } else {
            Table.nativeSetNull(var7, var10.h, var3, false);
         }

         var13 = ((ar)var1).n();
         if(var13 != null) {
            Table.nativeSetString(var7, var10.i, var3, var13, false);
         } else {
            Table.nativeSetNull(var7, var10.i, var3, false);
         }

         var13 = ((ar)var1).o();
         if(var13 != null) {
            Table.nativeSetString(var7, var10.j, var3, var13, false);
         } else {
            Table.nativeSetNull(var7, var10.j, var3, false);
         }
      }

      return var3;
   }

   public static co.uk.getmondo.payments.a.a.e b(av var0, co.uk.getmondo.payments.a.a.e var1, boolean var2, Map var3) {
      io.realm.internal.l var4 = (io.realm.internal.l)var3.get(var1);
      co.uk.getmondo.payments.a.a.e var8;
      if(var4 != null) {
         var8 = (co.uk.getmondo.payments.a.a.e)var4;
      } else {
         co.uk.getmondo.payments.a.a.e var10 = (co.uk.getmondo.payments.a.a.e)var0.a(co.uk.getmondo.payments.a.a.e.class, ((ar)var1).f(), false, Collections.emptyList());
         var3.put(var1, (io.realm.internal.l)var10);
         ar var7 = (ar)var1;
         ar var6 = (ar)var10;
         var6.b(var7.g());
         var6.c(var7.h());
         co.uk.getmondo.payments.a.a.b var9 = var7.i();
         if(var9 == null) {
            var6.a((co.uk.getmondo.payments.a.a.b)null);
         } else {
            co.uk.getmondo.payments.a.a.b var5 = (co.uk.getmondo.payments.a.a.b)var3.get(var9);
            if(var5 != null) {
               var6.a(var5);
            } else {
               var6.a(w.a(var0, var9, var2, var3));
            }
         }

         var6.a(var7.j());
         var6.d(var7.k());
         var6.e(var7.l());
         var6.f(var7.m());
         var6.g(var7.n());
         var6.h(var7.o());
         var8 = var10;
      }

      return var8;
   }

   public static OsObjectSchemaInfo p() {
      return d;
   }

   public static String q() {
      return "class_PaymentSeries";
   }

   private static OsObjectSchemaInfo r() {
      OsObjectSchemaInfo.a var0 = new OsObjectSchemaInfo.a("PaymentSeries");
      var0.a("id", RealmFieldType.STRING, true, true, false);
      var0.a("accountId", RealmFieldType.STRING, false, false, false);
      var0.a("scheme", RealmFieldType.STRING, false, false, false);
      var0.a("schemaData", RealmFieldType.OBJECT, "FpsSchemaData");
      var0.a("_amount", RealmFieldType.INTEGER, false, false, true);
      var0.a("_currency", RealmFieldType.STRING, false, false, false);
      var0.a("_startDate", RealmFieldType.STRING, false, false, false);
      var0.a("_nextIterationDate", RealmFieldType.STRING, false, false, false);
      var0.a("_endDate", RealmFieldType.STRING, false, false, false);
      var0.a("_intervalType", RealmFieldType.STRING, false, false, false);
      return var0.a();
   }

   public void a(long var1) {
      if(this.c.e()) {
         if(this.c.c()) {
            io.realm.internal.n var3 = this.c.b();
            var3.b().a(this.b.e, var3.c(), var1, true);
         }
      } else {
         this.c.a().e();
         this.c.b().a(this.b.e, var1);
      }

   }

   public void a(co.uk.getmondo.payments.a.a.b var1) {
      if(this.c.e()) {
         if(this.c.c() && !this.c.d().contains("schemaData")) {
            if(var1 != null && !bc.c(var1)) {
               var1 = (co.uk.getmondo.payments.a.a.b)((av)this.c.a()).a((bb)var1);
            }

            io.realm.internal.n var2 = this.c.b();
            if(var1 == null) {
               var2.o(this.b.d);
            } else {
               if(!bc.b(var1)) {
                  throw new IllegalArgumentException("'value' is not a valid managed object.");
               }

               if(((io.realm.internal.l)var1).s_().a() != this.c.a()) {
                  throw new IllegalArgumentException("'value' belongs to a different Realm.");
               }

               var2.b().b(this.b.d, var2.c(), ((io.realm.internal.l)var1).s_().b().c(), true);
            }
         }
      } else {
         this.c.a().e();
         if(var1 == null) {
            this.c.b().o(this.b.d);
         } else {
            if(!bc.c(var1) || !bc.b(var1)) {
               throw new IllegalArgumentException("'value' is not a valid managed object.");
            }

            if(((io.realm.internal.l)var1).s_().a() != this.c.a()) {
               throw new IllegalArgumentException("'value' belongs to a different Realm.");
            }

            this.c.b().b(this.b.d, ((io.realm.internal.l)var1).s_().b().c());
         }
      }

   }

   public void a(String var1) {
      if(!this.c.e()) {
         this.c.a().e();
         throw new RealmException("Primary key field 'id' cannot be changed after object was created.");
      }
   }

   public void b(String var1) {
      if(this.c.e()) {
         if(this.c.c()) {
            io.realm.internal.n var2 = this.c.b();
            if(var1 == null) {
               var2.b().a(this.b.b, var2.c(), true);
            } else {
               var2.b().a(this.b.b, var2.c(), var1, true);
            }
         }
      } else {
         this.c.a().e();
         if(var1 == null) {
            this.c.b().c(this.b.b);
         } else {
            this.c.b().a(this.b.b, var1);
         }
      }

   }

   public void c(String var1) {
      if(this.c.e()) {
         if(this.c.c()) {
            io.realm.internal.n var2 = this.c.b();
            if(var1 == null) {
               var2.b().a(this.b.c, var2.c(), true);
            } else {
               var2.b().a(this.b.c, var2.c(), var1, true);
            }
         }
      } else {
         this.c.a().e();
         if(var1 == null) {
            this.c.b().c(this.b.c);
         } else {
            this.c.b().a(this.b.c, var1);
         }
      }

   }

   public void d(String var1) {
      if(this.c.e()) {
         if(this.c.c()) {
            io.realm.internal.n var2 = this.c.b();
            if(var1 == null) {
               var2.b().a(this.b.f, var2.c(), true);
            } else {
               var2.b().a(this.b.f, var2.c(), var1, true);
            }
         }
      } else {
         this.c.a().e();
         if(var1 == null) {
            this.c.b().c(this.b.f);
         } else {
            this.c.b().a(this.b.f, var1);
         }
      }

   }

   public void e(String var1) {
      if(this.c.e()) {
         if(this.c.c()) {
            io.realm.internal.n var2 = this.c.b();
            if(var1 == null) {
               var2.b().a(this.b.g, var2.c(), true);
            } else {
               var2.b().a(this.b.g, var2.c(), var1, true);
            }
         }
      } else {
         this.c.a().e();
         if(var1 == null) {
            this.c.b().c(this.b.g);
         } else {
            this.c.b().a(this.b.g, var1);
         }
      }

   }

   public String f() {
      this.c.a().e();
      return this.c.b().k(this.b.a);
   }

   public void f(String var1) {
      if(this.c.e()) {
         if(this.c.c()) {
            io.realm.internal.n var2 = this.c.b();
            if(var1 == null) {
               var2.b().a(this.b.h, var2.c(), true);
            } else {
               var2.b().a(this.b.h, var2.c(), var1, true);
            }
         }
      } else {
         this.c.a().e();
         if(var1 == null) {
            this.c.b().c(this.b.h);
         } else {
            this.c.b().a(this.b.h, var1);
         }
      }

   }

   public String g() {
      this.c.a().e();
      return this.c.b().k(this.b.b);
   }

   public void g(String var1) {
      if(this.c.e()) {
         if(this.c.c()) {
            io.realm.internal.n var2 = this.c.b();
            if(var1 == null) {
               var2.b().a(this.b.i, var2.c(), true);
            } else {
               var2.b().a(this.b.i, var2.c(), var1, true);
            }
         }
      } else {
         this.c.a().e();
         if(var1 == null) {
            this.c.b().c(this.b.i);
         } else {
            this.c.b().a(this.b.i, var1);
         }
      }

   }

   public String h() {
      this.c.a().e();
      return this.c.b().k(this.b.c);
   }

   public void h(String var1) {
      if(this.c.e()) {
         if(this.c.c()) {
            io.realm.internal.n var2 = this.c.b();
            if(var1 == null) {
               var2.b().a(this.b.j, var2.c(), true);
            } else {
               var2.b().a(this.b.j, var2.c(), var1, true);
            }
         }
      } else {
         this.c.a().e();
         if(var1 == null) {
            this.c.b().c(this.b.j);
         } else {
            this.c.b().a(this.b.j, var1);
         }
      }

   }

   public co.uk.getmondo.payments.a.a.b i() {
      this.c.a().e();
      co.uk.getmondo.payments.a.a.b var1;
      if(this.c.b().a(this.b.d)) {
         var1 = null;
      } else {
         var1 = (co.uk.getmondo.payments.a.a.b)this.c.a().a(co.uk.getmondo.payments.a.a.b.class, this.c.b().m(this.b.d), false, Collections.emptyList());
      }

      return var1;
   }

   public long j() {
      this.c.a().e();
      return this.c.b().f(this.b.e);
   }

   public String k() {
      this.c.a().e();
      return this.c.b().k(this.b.f);
   }

   public String l() {
      this.c.a().e();
      return this.c.b().k(this.b.g);
   }

   public String m() {
      this.c.a().e();
      return this.c.b().k(this.b.h);
   }

   public String n() {
      this.c.a().e();
      return this.c.b().k(this.b.i);
   }

   public String o() {
      this.c.a().e();
      return this.c.b().k(this.b.j);
   }

   public au s_() {
      return this.c;
   }

   public String toString() {
      String var1;
      if(!bc.b(this)) {
         var1 = "Invalid object";
      } else {
         StringBuilder var2 = new StringBuilder("PaymentSeries = proxy[");
         var2.append("{id:");
         if(this.f() != null) {
            var1 = this.f();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{accountId:");
         if(this.g() != null) {
            var1 = this.g();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{scheme:");
         if(this.h() != null) {
            var1 = this.h();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{schemaData:");
         if(this.i() != null) {
            var1 = "FpsSchemaData";
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{_amount:");
         var2.append(this.j());
         var2.append("}");
         var2.append(",");
         var2.append("{_currency:");
         if(this.k() != null) {
            var1 = this.k();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{_startDate:");
         if(this.l() != null) {
            var1 = this.l();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{_nextIterationDate:");
         if(this.m() != null) {
            var1 = this.m();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{_endDate:");
         if(this.n() != null) {
            var1 = this.n();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{_intervalType:");
         if(this.o() != null) {
            var1 = this.o();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append("]");
         var1 = var2.toString();
      }

      return var1;
   }

   public void u_() {
      if(this.c == null) {
         g.b var1 = (g.b)g.g.get();
         this.b = (aq.a)var1.c();
         this.c = new au(this);
         this.c.a(var1.a());
         this.c.a(var1.b());
         this.c.a(var1.d());
         this.c.a(var1.e());
      }

   }

   static final class a extends io.realm.internal.c {
      long a;
      long b;
      long c;
      long d;
      long e;
      long f;
      long g;
      long h;
      long i;
      long j;

      a(SharedRealm var1, Table var2) {
         super(10);
         this.a = this.a(var2, "id", RealmFieldType.STRING);
         this.b = this.a(var2, "accountId", RealmFieldType.STRING);
         this.c = this.a(var2, "scheme", RealmFieldType.STRING);
         this.d = this.a(var2, "schemaData", RealmFieldType.OBJECT);
         this.e = this.a(var2, "_amount", RealmFieldType.INTEGER);
         this.f = this.a(var2, "_currency", RealmFieldType.STRING);
         this.g = this.a(var2, "_startDate", RealmFieldType.STRING);
         this.h = this.a(var2, "_nextIterationDate", RealmFieldType.STRING);
         this.i = this.a(var2, "_endDate", RealmFieldType.STRING);
         this.j = this.a(var2, "_intervalType", RealmFieldType.STRING);
      }

      a(io.realm.internal.c var1, boolean var2) {
         super(var1, var2);
         this.a(var1, this);
      }

      protected final io.realm.internal.c a(boolean var1) {
         return new aq.a(this, var1);
      }

      protected final void a(io.realm.internal.c var1, io.realm.internal.c var2) {
         aq.a var3 = (aq.a)var1;
         aq.a var4 = (aq.a)var2;
         var4.a = var3.a;
         var4.b = var3.b;
         var4.c = var3.c;
         var4.d = var3.d;
         var4.e = var3.e;
         var4.f = var3.f;
         var4.g = var3.g;
         var4.h = var3.h;
         var4.i = var3.i;
         var4.j = var3.j;
      }
   }
}
