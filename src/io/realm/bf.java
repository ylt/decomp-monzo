package io.realm;

import io.realm.internal.Collection;
import io.realm.internal.LinkView;
import io.realm.internal.SortDescriptor;
import io.realm.internal.Table;
import io.realm.internal.TableQuery;
import java.util.Date;
import java.util.Locale;

public class bf {
   private final Table a;
   private final g b;
   private final TableQuery c;
   private final be d;
   private Class e;
   private String f;
   private LinkView g;

   private bf(av var1, Class var2) {
      this.b = var1;
      this.e = var2;
      this.d = var1.k().b(var2);
      this.a = this.d.b();
      this.g = null;
      this.c = this.a.i();
   }

   public static bf a(av var0, Class var1) {
      return new bf(var0, var1);
   }

   private bg a(TableQuery var1, SortDescriptor var2, SortDescriptor var3, boolean var4) {
      Collection var5 = new Collection(this.b.e, var1, var2, var3);
      bg var6;
      if(this.l()) {
         var6 = new bg(this.b, var5, this.f);
      } else {
         var6 = new bg(this.b, var5, this.e);
      }

      if(var4) {
         var6.d();
      }

      return var6;
   }

   private bf b(String var1, Boolean var2) {
      io.realm.internal.a.c var3 = this.d.a(var1, new RealmFieldType[]{RealmFieldType.BOOLEAN});
      if(var2 == null) {
         this.c.a(var3.b(), var3.c());
      } else {
         this.c.a(var3.b(), var3.c(), var2.booleanValue());
      }

      return this;
   }

   private bf d(String var1, String var2, l var3) {
      io.realm.internal.a.c var4 = this.d.a(var1, new RealmFieldType[]{RealmFieldType.STRING});
      this.c.a(var4.b(), var4.c(), var2, var3);
      return this;
   }

   private bf i() {
      this.c.c();
      return this;
   }

   private bf j() {
      this.c.d();
      return this;
   }

   private bf k() {
      this.c.e();
      return this;
   }

   private boolean l() {
      boolean var1;
      if(this.f != null) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   private long m() {
      return this.c.g();
   }

   private bi n() {
      return new bi(this.b.k());
   }

   public bf a() {
      this.b.e();
      return this.i();
   }

   public bf a(String var1) {
      this.b.e();
      io.realm.internal.a.c var2 = this.d.a(var1, new RealmFieldType[0]);
      this.c.a(var2.b(), var2.c());
      return this;
   }

   public bf a(String var1, int var2) {
      this.b.e();
      io.realm.internal.a.c var3 = this.d.a(var1, new RealmFieldType[]{RealmFieldType.INTEGER});
      this.c.a(var3.b(), var3.c(), (long)var2);
      return this;
   }

   public bf a(String var1, long var2) {
      this.b.e();
      io.realm.internal.a.c var4 = this.d.a(var1, new RealmFieldType[]{RealmFieldType.INTEGER});
      this.c.a(var4.b(), var4.c(), var2);
      return this;
   }

   public bf a(String var1, Boolean var2) {
      this.b.e();
      return this.b(var1, var2);
   }

   public bf a(String var1, String var2) {
      return this.a(var1, var2, l.a);
   }

   public bf a(String var1, String var2, l var3) {
      this.b.e();
      return this.d(var1, var2, var3);
   }

   public bf a(String var1, Date var2) {
      this.b.e();
      io.realm.internal.a.c var3 = this.d.a(var1, new RealmFieldType[]{RealmFieldType.DATE});
      this.c.a(var3.b(), var3.c(), var2);
      return this;
   }

   public bf a(String var1, Date var2, Date var3) {
      this.b.e();
      io.realm.internal.a.c var4 = this.d.a(var1, new RealmFieldType[]{RealmFieldType.DATE});
      this.c.a(var4.b(), var2, var3);
      return this;
   }

   public bf a(String var1, String[] var2) {
      return this.a(var1, var2, l.a);
   }

   public bf a(String var1, String[] var2, l var3) {
      this.b.e();
      if(var2 != null && var2.length != 0) {
         this.i().d(var1, var2[0], var3);

         for(int var4 = 1; var4 < var2.length; ++var4) {
            this.k().d(var1, var2[var4], var3);
         }

         return this.j();
      } else {
         throw new IllegalArgumentException("Non-empty 'values' must be provided.");
      }
   }

   public bg a(String var1, bl var2) {
      this.b.e();
      this.b.e.d.a("Async query cannot be created on current thread.");
      SortDescriptor var3 = SortDescriptor.a((io.realm.internal.a.c.a)this.n(), (Table)this.c.a(), (String)var1, (bl)var2);
      return this.a(this.c, var3, (SortDescriptor)null, false);
   }

   public bf b() {
      this.b.e();
      return this.j();
   }

   public bf b(String var1) {
      this.b.e();
      io.realm.internal.a.c var2 = this.d.a(var1, new RealmFieldType[0]);
      this.c.b(var2.b(), var2.c());
      return this;
   }

   public bf b(String var1, String var2) {
      return this.b(var1, var2, l.a);
   }

   public bf b(String var1, String var2, l var3) {
      this.b.e();
      io.realm.internal.a.c var4 = this.d.a(var1, new RealmFieldType[]{RealmFieldType.STRING});
      if(var4.a() > 1 && !var3.a()) {
         throw new IllegalArgumentException("Link queries cannot be case insensitive - coming soon.");
      } else {
         this.c.b(var4.b(), var4.c(), var2, var3);
         return this;
      }
   }

   public bf b(String var1, Date var2) {
      this.b.e();
      io.realm.internal.a.c var3 = this.d.a(var1, new RealmFieldType[]{RealmFieldType.DATE});
      this.c.b(var3.b(), var3.c(), var2);
      return this;
   }

   public bf c() {
      this.b.e();
      return this.k();
   }

   public bf c(String var1, String var2, l var3) {
      this.b.e();
      io.realm.internal.a.c var4 = this.d.a(var1, new RealmFieldType[]{RealmFieldType.STRING});
      this.c.c(var4.b(), var4.c(), var2, var3);
      return this;
   }

   public Number c(String var1) {
      this.b.e();
      long var2 = this.d.a(var1);
      Object var4;
      switch(null.a[this.a.c(var2).ordinal()]) {
      case 1:
         var4 = Long.valueOf(this.c.a(var2));
         break;
      case 2:
         var4 = Double.valueOf(this.c.b(var2));
         break;
      case 3:
         var4 = Double.valueOf(this.c.c(var2));
         break;
      default:
         throw new IllegalArgumentException(String.format(Locale.US, "Field '%s': type mismatch - %s expected.", new Object[]{var1, "int, float or double"}));
      }

      return (Number)var4;
   }

   public bf d() {
      this.b.e();
      this.c.f();
      return this;
   }

   public Date d(String var1) {
      this.b.e();
      long var2 = this.d.a(var1);
      return this.c.d(var2);
   }

   public long e() {
      this.b.e();
      return this.c.h();
   }

   public bg f() {
      this.b.e();
      return this.a(this.c, (SortDescriptor)null, (SortDescriptor)null, true);
   }

   public bg g() {
      this.b.e();
      this.b.e.d.a("Async query cannot be created on current thread.");
      return this.a(this.c, (SortDescriptor)null, (SortDescriptor)null, false);
   }

   public bb h() {
      this.b.e();
      long var1 = this.m();
      bb var3;
      if(var1 < 0L) {
         var3 = null;
      } else {
         var3 = this.b.a(this.e, this.f, var1);
      }

      return var3;
   }
}
