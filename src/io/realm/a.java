package io.realm;

import io.realm.exceptions.RealmException;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.LinkView;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.SharedRealm;
import io.realm.internal.Table;
import io.realm.log.RealmLog;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class a extends co.uk.getmondo.d.b implements b, io.realm.internal.l {
   private static final OsObjectSchemaInfo c = o();
   private static final List e;
   private a.a a;
   private au b;
   private az d;

   static {
      ArrayList var0 = new ArrayList();
      var0.add("accountId");
      var0.add("balanceAmountValue");
      var0.add("balanceCurrency");
      var0.add("spentTodayAmountValue");
      var0.add("spentTodayCurrency");
      var0.add("localSpend");
      e = Collections.unmodifiableList(var0);
   }

   a() {
      this.b.f();
   }

   public static long a(av var0, co.uk.getmondo.d.b var1, Map var2) {
      long var5;
      if(var1 instanceof io.realm.internal.l && ((io.realm.internal.l)var1).s_().a() != null && ((io.realm.internal.l)var1).s_().a().g().equals(var0.g())) {
         var5 = ((io.realm.internal.l)var1).s_().b().c();
      } else {
         Table var11 = var0.c(co.uk.getmondo.d.b.class);
         long var7 = var11.getNativePtr();
         a.a var9 = (a.a)var0.f.c(co.uk.getmondo.d.b.class);
         long var3 = var11.d();
         String var10 = ((b)var1).e();
         if(var10 == null) {
            var3 = Table.nativeFindFirstNull(var7, var3);
         } else {
            var3 = Table.nativeFindFirstString(var7, var3, var10);
         }

         if(var3 == -1L) {
            var3 = OsObject.b(var11, var10);
         } else {
            Table.a((Object)var10);
         }

         var2.put(var1, Long.valueOf(var3));
         Table.nativeSetLong(var7, var9.b, var3, ((b)var1).f(), false);
         var10 = ((b)var1).g();
         if(var10 != null) {
            Table.nativeSetString(var7, var9.c, var3, var10, false);
         }

         Table.nativeSetLong(var7, var9.d, var3, ((b)var1).h(), false);
         var10 = ((b)var1).i();
         if(var10 != null) {
            Table.nativeSetString(var7, var9.e, var3, var10, false);
         }

         az var14 = ((b)var1).j();
         var5 = var3;
         if(var14 != null) {
            var7 = Table.nativeGetLinkView(var7, var9.f, var3);
            Iterator var12 = var14.iterator();

            while(true) {
               var5 = var3;
               if(!var12.hasNext()) {
                  break;
               }

               co.uk.getmondo.d.t var13 = (co.uk.getmondo.d.t)var12.next();
               Long var16 = (Long)var2.get(var13);
               Long var15 = var16;
               if(var16 == null) {
                  var15 = Long.valueOf(ae.a(var0, var13, var2));
               }

               LinkView.nativeAdd(var7, var15.longValue());
            }
         }
      }

      return var5;
   }

   public static co.uk.getmondo.d.b a(co.uk.getmondo.d.b var0, int var1, int var2, Map var3) {
      if(var1 <= var2 && var0 != null) {
         io.realm.internal.l.a var7 = (io.realm.internal.l.a)var3.get(var0);
         co.uk.getmondo.d.b var6;
         if(var7 == null) {
            var6 = new co.uk.getmondo.d.b();
            var3.put(var0, new io.realm.internal.l.a(var1, var6));
         } else {
            if(var1 >= var7.a) {
               var0 = (co.uk.getmondo.d.b)var7.b;
               return var0;
            }

            var6 = (co.uk.getmondo.d.b)var7.b;
            var7.a = var1;
         }

         b var11 = (b)var6;
         b var9 = (b)var0;
         var11.a(var9.e());
         var11.a(var9.f());
         var11.b(var9.g());
         var11.b(var9.h());
         var11.c(var9.i());
         if(var1 == var2) {
            var11.a((az)null);
         } else {
            az var10 = var9.j();
            az var8 = new az();
            var11.a(var8);
            int var5 = var10.size();

            for(int var4 = 0; var4 < var5; ++var4) {
               var8.a((bb)ae.a((co.uk.getmondo.d.t)var10.b(var4), var1 + 1, var2, var3));
            }
         }

         var0 = var6;
      } else {
         var0 = null;
      }

      return var0;
   }

   static co.uk.getmondo.d.b a(av var0, co.uk.getmondo.d.b var1, co.uk.getmondo.d.b var2, Map var3) {
      b var5 = (b)var1;
      b var8 = (b)var2;
      var5.a(var8.f());
      var5.b(var8.g());
      var5.b(var8.h());
      var5.c(var8.i());
      az var9 = var8.j();
      az var7 = var5.j();
      var7.clear();
      if(var9 != null) {
         for(int var4 = 0; var4 < var9.size(); ++var4) {
            co.uk.getmondo.d.t var6 = (co.uk.getmondo.d.t)var9.b(var4);
            co.uk.getmondo.d.t var10 = (co.uk.getmondo.d.t)var3.get(var6);
            if(var10 != null) {
               var7.a((bb)var10);
            } else {
               var7.a((bb)ae.a(var0, var6, true, var3));
            }
         }
      }

      return var1;
   }

   public static co.uk.getmondo.d.b a(av var0, co.uk.getmondo.d.b var1, boolean var2, Map var3) {
      if(var1 instanceof io.realm.internal.l && ((io.realm.internal.l)var1).s_().a() != null && ((io.realm.internal.l)var1).s_().a().c != var0.c) {
         throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
      } else {
         if(!(var1 instanceof io.realm.internal.l) || ((io.realm.internal.l)var1).s_().a() == null || !((io.realm.internal.l)var1).s_().a().g().equals(var0.g())) {
            g.b var8 = (g.b)g.g.get();
            io.realm.internal.l var7 = (io.realm.internal.l)var3.get(var1);
            if(var7 != null) {
               var1 = (co.uk.getmondo.d.b)var7;
            } else {
               boolean var4;
               a var13;
               if(var2) {
                  Table var9 = var0.c(co.uk.getmondo.d.b.class);
                  long var5 = var9.d();
                  String var12 = ((b)var1).e();
                  if(var12 == null) {
                     var5 = var9.k(var5);
                  } else {
                     var5 = var9.a(var5, var12);
                  }

                  if(var5 != -1L) {
                     try {
                        var8.a(var0, var9.f(var5), var0.f.c(co.uk.getmondo.d.b.class), false, Collections.emptyList());
                        var13 = new a();
                        var3.put(var1, (io.realm.internal.l)var13);
                     } finally {
                        var8.f();
                     }

                     var4 = var2;
                  } else {
                     var4 = false;
                     var13 = null;
                  }
               } else {
                  var4 = var2;
                  var13 = null;
               }

               if(var4) {
                  var1 = a(var0, var13, var1, var3);
               } else {
                  var1 = b(var0, var1, var2, var3);
               }
            }
         }

         return var1;
      }
   }

   public static a.a a(SharedRealm var0, boolean var1) {
      if(!var0.a("class_AccountBalance")) {
         throw new RealmMigrationNeededException(var0.h(), "The 'AccountBalance' class is missing from the schema for this Realm.");
      } else {
         Table var7 = var0.b("class_AccountBalance");
         long var4 = var7.c();
         if(var4 != 6L) {
            if(var4 < 6L) {
               throw new RealmMigrationNeededException(var0.h(), "Field count is less than expected - expected 6 but was " + var4);
            }

            if(!var1) {
               throw new RealmMigrationNeededException(var0.h(), "Field count is more than expected - expected 6 but was " + var4);
            }

            RealmLog.a("Field count is more than expected - expected 6 but was %1$d", new Object[]{Long.valueOf(var4)});
         }

         HashMap var8 = new HashMap();

         for(long var2 = 0L; var2 < var4; ++var2) {
            var8.put(var7.b(var2), var7.c(var2));
         }

         a.a var6 = new a.a(var0, var7);
         if(!var7.e()) {
            throw new RealmMigrationNeededException(var0.h(), "Primary key not defined for field 'accountId' in existing Realm file. @PrimaryKey was added.");
         } else if(var7.d() != var6.a) {
            throw new RealmMigrationNeededException(var0.h(), "Primary Key annotation definition was changed, from field " + var7.b(var7.d()) + " to field accountId");
         } else if(!var8.containsKey("accountId")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'accountId' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var8.get("accountId") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'accountId' in existing Realm file.");
         } else if(!var7.a(var6.a)) {
            throw new RealmMigrationNeededException(var0.h(), "@PrimaryKey field 'accountId' does not support null values in the existing Realm file. Migrate using RealmObjectSchema.setNullable(), or mark the field as @Required.");
         } else if(!var7.j(var7.a("accountId"))) {
            throw new RealmMigrationNeededException(var0.h(), "Index not defined for field 'accountId' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex().");
         } else if(!var8.containsKey("balanceAmountValue")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'balanceAmountValue' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var8.get("balanceAmountValue") != RealmFieldType.INTEGER) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'long' for field 'balanceAmountValue' in existing Realm file.");
         } else if(var7.a(var6.b)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'balanceAmountValue' does support null values in the existing Realm file. Use corresponding boxed type for field 'balanceAmountValue' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var8.containsKey("balanceCurrency")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'balanceCurrency' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var8.get("balanceCurrency") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'balanceCurrency' in existing Realm file.");
         } else if(!var7.a(var6.c)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'balanceCurrency' is required. Either set @Required to field 'balanceCurrency' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var8.containsKey("spentTodayAmountValue")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'spentTodayAmountValue' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var8.get("spentTodayAmountValue") != RealmFieldType.INTEGER) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'long' for field 'spentTodayAmountValue' in existing Realm file.");
         } else if(var7.a(var6.d)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'spentTodayAmountValue' does support null values in the existing Realm file. Use corresponding boxed type for field 'spentTodayAmountValue' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var8.containsKey("spentTodayCurrency")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'spentTodayCurrency' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var8.get("spentTodayCurrency") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'spentTodayCurrency' in existing Realm file.");
         } else if(!var7.a(var6.e)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'spentTodayCurrency' is required. Either set @Required to field 'spentTodayCurrency' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var8.containsKey("localSpend")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'localSpend'");
         } else if(var8.get("localSpend") != RealmFieldType.LIST) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'LocalSpend' for field 'localSpend'");
         } else if(!var0.a("class_LocalSpend")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing class 'class_LocalSpend' for field 'localSpend'");
         } else {
            Table var9 = var0.b("class_LocalSpend");
            if(!var7.e(var6.f).a(var9)) {
               throw new RealmMigrationNeededException(var0.h(), "Invalid RealmList type for field 'localSpend': '" + var7.e(var6.f).j() + "' expected - was '" + var9.j() + "'");
            } else {
               return var6;
            }
         }
      }
   }

   public static void a(av var0, Iterator var1, Map var2) {
      Table var14 = var0.c(co.uk.getmondo.d.b.class);
      long var9 = var14.getNativePtr();
      a.a var13 = (a.a)var0.f.c(co.uk.getmondo.d.b.class);
      long var7 = var14.d();

      while(true) {
         while(true) {
            co.uk.getmondo.d.b var11;
            do {
               if(!var1.hasNext()) {
                  return;
               }

               var11 = (co.uk.getmondo.d.b)var1.next();
            } while(var2.containsKey(var11));

            if(var11 instanceof io.realm.internal.l && ((io.realm.internal.l)var11).s_().a() != null && ((io.realm.internal.l)var11).s_().a().g().equals(var0.g())) {
               var2.put(var11, Long.valueOf(((io.realm.internal.l)var11).s_().b().c()));
            } else {
               String var12 = ((b)var11).e();
               long var3;
               if(var12 == null) {
                  var3 = Table.nativeFindFirstNull(var9, var7);
               } else {
                  var3 = Table.nativeFindFirstString(var9, var7, var12);
               }

               long var5 = var3;
               if(var3 == -1L) {
                  var5 = OsObject.b(var14, var12);
               }

               var2.put(var11, Long.valueOf(var5));
               Table.nativeSetLong(var9, var13.b, var5, ((b)var11).f(), false);
               var12 = ((b)var11).g();
               if(var12 != null) {
                  Table.nativeSetString(var9, var13.c, var5, var12, false);
               } else {
                  Table.nativeSetNull(var9, var13.c, var5, false);
               }

               Table.nativeSetLong(var9, var13.d, var5, ((b)var11).h(), false);
               var12 = ((b)var11).i();
               if(var12 != null) {
                  Table.nativeSetString(var9, var13.e, var5, var12, false);
               } else {
                  Table.nativeSetNull(var9, var13.e, var5, false);
               }

               var3 = Table.nativeGetLinkView(var9, var13.f, var5);
               LinkView.nativeClear(var3);
               az var17 = ((b)var11).j();
               Long var18;
               if(var17 != null) {
                  for(Iterator var15 = var17.iterator(); var15.hasNext(); LinkView.nativeAdd(var3, var18.longValue())) {
                     co.uk.getmondo.d.t var16 = (co.uk.getmondo.d.t)var15.next();
                     Long var19 = (Long)var2.get(var16);
                     var18 = var19;
                     if(var19 == null) {
                        var18 = Long.valueOf(ae.b(var0, var16, var2));
                     }
                  }
               }
            }
         }
      }
   }

   public static long b(av var0, co.uk.getmondo.d.b var1, Map var2) {
      long var5;
      if(var1 instanceof io.realm.internal.l && ((io.realm.internal.l)var1).s_().a() != null && ((io.realm.internal.l)var1).s_().a().g().equals(var0.g())) {
         var5 = ((io.realm.internal.l)var1).s_().b().c();
      } else {
         Table var11 = var0.c(co.uk.getmondo.d.b.class);
         long var7 = var11.getNativePtr();
         a.a var9 = (a.a)var0.f.c(co.uk.getmondo.d.b.class);
         long var3 = var11.d();
         String var10 = ((b)var1).e();
         if(var10 == null) {
            var5 = Table.nativeFindFirstNull(var7, var3);
         } else {
            var5 = Table.nativeFindFirstString(var7, var3, var10);
         }

         var3 = var5;
         if(var5 == -1L) {
            var3 = OsObject.b(var11, var10);
         }

         var2.put(var1, Long.valueOf(var3));
         Table.nativeSetLong(var7, var9.b, var3, ((b)var1).f(), false);
         var10 = ((b)var1).g();
         if(var10 != null) {
            Table.nativeSetString(var7, var9.c, var3, var10, false);
         } else {
            Table.nativeSetNull(var7, var9.c, var3, false);
         }

         Table.nativeSetLong(var7, var9.d, var3, ((b)var1).h(), false);
         var10 = ((b)var1).i();
         if(var10 != null) {
            Table.nativeSetString(var7, var9.e, var3, var10, false);
         } else {
            Table.nativeSetNull(var7, var9.e, var3, false);
         }

         var7 = Table.nativeGetLinkView(var7, var9.f, var3);
         LinkView.nativeClear(var7);
         az var12 = ((b)var1).j();
         var5 = var3;
         if(var12 != null) {
            Iterator var15 = var12.iterator();

            while(true) {
               var5 = var3;
               if(!var15.hasNext()) {
                  break;
               }

               co.uk.getmondo.d.t var16 = (co.uk.getmondo.d.t)var15.next();
               Long var14 = (Long)var2.get(var16);
               Long var13 = var14;
               if(var14 == null) {
                  var13 = Long.valueOf(ae.b(var0, var16, var2));
               }

               LinkView.nativeAdd(var7, var13.longValue());
            }
         }
      }

      return var5;
   }

   public static co.uk.getmondo.d.b b(av var0, co.uk.getmondo.d.b var1, boolean var2, Map var3) {
      io.realm.internal.l var5 = (io.realm.internal.l)var3.get(var1);
      if(var5 != null) {
         var1 = (co.uk.getmondo.d.b)var5;
      } else {
         co.uk.getmondo.d.b var11 = (co.uk.getmondo.d.b)var0.a(co.uk.getmondo.d.b.class, ((b)var1).e(), false, Collections.emptyList());
         var3.put(var1, (io.realm.internal.l)var11);
         b var9 = (b)var1;
         b var7 = (b)var11;
         var7.a(var9.f());
         var7.b(var9.g());
         var7.b(var9.h());
         var7.c(var9.i());
         az var6 = var9.j();
         var1 = var11;
         if(var6 != null) {
            az var12 = var7.j();
            int var4 = 0;

            while(true) {
               var1 = var11;
               if(var4 >= var6.size()) {
                  break;
               }

               co.uk.getmondo.d.t var10 = (co.uk.getmondo.d.t)var6.b(var4);
               co.uk.getmondo.d.t var8 = (co.uk.getmondo.d.t)var3.get(var10);
               if(var8 != null) {
                  var12.a((bb)var8);
               } else {
                  var12.a((bb)ae.a(var0, var10, var2, var3));
               }

               ++var4;
            }
         }
      }

      return var1;
   }

   public static OsObjectSchemaInfo l() {
      return c;
   }

   public static String m() {
      return "class_AccountBalance";
   }

   private static OsObjectSchemaInfo o() {
      OsObjectSchemaInfo.a var0 = new OsObjectSchemaInfo.a("AccountBalance");
      var0.a("accountId", RealmFieldType.STRING, true, true, false);
      var0.a("balanceAmountValue", RealmFieldType.INTEGER, false, false, true);
      var0.a("balanceCurrency", RealmFieldType.STRING, false, false, false);
      var0.a("spentTodayAmountValue", RealmFieldType.INTEGER, false, false, true);
      var0.a("spentTodayCurrency", RealmFieldType.STRING, false, false, false);
      var0.a("localSpend", RealmFieldType.LIST, "LocalSpend");
      return var0.a();
   }

   public void a(long var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var3 = this.b.b();
            var3.b().a(this.a.b, var3.c(), var1, true);
         }
      } else {
         this.b.a().e();
         this.b.b().a(this.a.b, var1);
      }

   }

   public void a(az var1) {
      az var2 = var1;
      if(this.b.e()) {
         if(!this.b.c() || this.b.d().contains("localSpend")) {
            return;
         }

         var2 = var1;
         if(var1 != null) {
            var2 = var1;
            if(!var1.a()) {
               av var3 = (av)this.b.a();
               var2 = new az();
               Iterator var5 = var1.iterator();

               label51:
               while(true) {
                  while(true) {
                     if(!var5.hasNext()) {
                        break label51;
                     }

                     co.uk.getmondo.d.t var4 = (co.uk.getmondo.d.t)var5.next();
                     if(var4 != null && !bc.c(var4)) {
                        var2.a(var3.a((bb)var4));
                     } else {
                        var2.a((bb)var4);
                     }
                  }
               }
            }
         }
      }

      this.b.a().e();
      LinkView var6 = this.b.b().n(this.a.f);
      var6.a();
      if(var2 != null) {
         Iterator var8 = var2.iterator();

         while(var8.hasNext()) {
            bb var7 = (bb)var8.next();
            if(!bc.c(var7) || !bc.b(var7)) {
               throw new IllegalArgumentException("Each element of 'value' must be a valid managed object.");
            }

            if(((io.realm.internal.l)var7).s_().a() != this.b.a()) {
               throw new IllegalArgumentException("Each element of 'value' must belong to the same Realm.");
            }

            var6.b(((io.realm.internal.l)var7).s_().b().c());
         }
      }

   }

   public void a(String var1) {
      if(!this.b.e()) {
         this.b.a().e();
         throw new RealmException("Primary key field 'accountId' cannot be changed after object was created.");
      }
   }

   public void b(long var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var3 = this.b.b();
            var3.b().a(this.a.d, var3.c(), var1, true);
         }
      } else {
         this.b.a().e();
         this.b.b().a(this.a.d, var1);
      }

   }

   public void b(String var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            if(var1 == null) {
               var2.b().a(this.a.c, var2.c(), true);
            } else {
               var2.b().a(this.a.c, var2.c(), var1, true);
            }
         }
      } else {
         this.b.a().e();
         if(var1 == null) {
            this.b.b().c(this.a.c);
         } else {
            this.b.b().a(this.a.c, var1);
         }
      }

   }

   public void c(String var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            if(var1 == null) {
               var2.b().a(this.a.e, var2.c(), true);
            } else {
               var2.b().a(this.a.e, var2.c(), var1, true);
            }
         }
      } else {
         this.b.a().e();
         if(var1 == null) {
            this.b.b().c(this.a.e);
         } else {
            this.b.b().a(this.a.e, var1);
         }
      }

   }

   public String e() {
      this.b.a().e();
      return this.b.b().k(this.a.a);
   }

   public long f() {
      this.b.a().e();
      return this.b.b().f(this.a.b);
   }

   public String g() {
      this.b.a().e();
      return this.b.b().k(this.a.c);
   }

   public long h() {
      this.b.a().e();
      return this.b.b().f(this.a.d);
   }

   public String i() {
      this.b.a().e();
      return this.b.b().k(this.a.e);
   }

   public az j() {
      this.b.a().e();
      az var1;
      if(this.d != null) {
         var1 = this.d;
      } else {
         this.d = new az(co.uk.getmondo.d.t.class, this.b.b().n(this.a.f), this.b.a());
         var1 = this.d;
      }

      return var1;
   }

   public au s_() {
      return this.b;
   }

   public String toString() {
      String var1;
      if(!bc.b(this)) {
         var1 = "Invalid object";
      } else {
         StringBuilder var2 = new StringBuilder("AccountBalance = proxy[");
         var2.append("{accountId:");
         if(this.e() != null) {
            var1 = this.e();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{balanceAmountValue:");
         var2.append(this.f());
         var2.append("}");
         var2.append(",");
         var2.append("{balanceCurrency:");
         if(this.g() != null) {
            var1 = this.g();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{spentTodayAmountValue:");
         var2.append(this.h());
         var2.append("}");
         var2.append(",");
         var2.append("{spentTodayCurrency:");
         if(this.i() != null) {
            var1 = this.i();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{localSpend:");
         var2.append("RealmList<LocalSpend>[").append(this.j().size()).append("]");
         var2.append("}");
         var2.append("]");
         var1 = var2.toString();
      }

      return var1;
   }

   public void u_() {
      if(this.b == null) {
         g.b var1 = (g.b)g.g.get();
         this.a = (a.a)var1.c();
         this.b = new au(this);
         this.b.a(var1.a());
         this.b.a(var1.b());
         this.b.a(var1.d());
         this.b.a(var1.e());
      }

   }

   static final class a extends io.realm.internal.c {
      long a;
      long b;
      long c;
      long d;
      long e;
      long f;

      a(SharedRealm var1, Table var2) {
         super(6);
         this.a = this.a(var2, "accountId", RealmFieldType.STRING);
         this.b = this.a(var2, "balanceAmountValue", RealmFieldType.INTEGER);
         this.c = this.a(var2, "balanceCurrency", RealmFieldType.STRING);
         this.d = this.a(var2, "spentTodayAmountValue", RealmFieldType.INTEGER);
         this.e = this.a(var2, "spentTodayCurrency", RealmFieldType.STRING);
         this.f = this.a(var2, "localSpend", RealmFieldType.LIST);
      }

      a(io.realm.internal.c var1, boolean var2) {
         super(var1, var2);
         this.a(var1, this);
      }

      protected final io.realm.internal.c a(boolean var1) {
         return new a.a(this, var1);
      }

      protected final void a(io.realm.internal.c var1, io.realm.internal.c var2) {
         a.a var3 = (a.a)var1;
         a.a var4 = (a.a)var2;
         var4.a = var3.a;
         var4.b = var3.b;
         var4.c = var3.c;
         var4.d = var3.d;
         var4.e = var3.e;
         var4.f = var3.f;
      }
   }
}
