package io.realm;

class bi implements io.realm.internal.a.c.a {
   private final bh a;

   public bi(bh var1) {
      this.a = var1;
   }

   public io.realm.internal.c a(String var1) {
      return this.a.b(var1);
   }

   public boolean a() {
      return this.a.d();
   }

   public long b(String var1) {
      return this.a.a(var1).getNativePtr();
   }
}
