package io.realm;

import io.realm.internal.Keep;
import java.nio.ByteBuffer;
import java.util.Date;

@Keep
public enum RealmFieldType {
   BINARY,
   BOOLEAN,
   DATE,
   DOUBLE,
   FLOAT,
   INTEGER,
   LINKING_OBJECTS,
   LIST,
   OBJECT,
   STRING,
   UNSUPPORTED_DATE,
   UNSUPPORTED_MIXED,
   UNSUPPORTED_TABLE;

   private static final RealmFieldType[] typeList;
   private final int nativeValue;

   static {
      int var0 = 0;
      INTEGER = new RealmFieldType("INTEGER", 0, 0);
      BOOLEAN = new RealmFieldType("BOOLEAN", 1, 1);
      STRING = new RealmFieldType("STRING", 2, 2);
      BINARY = new RealmFieldType("BINARY", 3, 4);
      UNSUPPORTED_TABLE = new RealmFieldType("UNSUPPORTED_TABLE", 4, 5);
      UNSUPPORTED_MIXED = new RealmFieldType("UNSUPPORTED_MIXED", 5, 6);
      UNSUPPORTED_DATE = new RealmFieldType("UNSUPPORTED_DATE", 6, 7);
      DATE = new RealmFieldType("DATE", 7, 8);
      FLOAT = new RealmFieldType("FLOAT", 8, 9);
      DOUBLE = new RealmFieldType("DOUBLE", 9, 10);
      OBJECT = new RealmFieldType("OBJECT", 10, 12);
      LIST = new RealmFieldType("LIST", 11, 13);
      LINKING_OBJECTS = new RealmFieldType("LINKING_OBJECTS", 12, 14);
      typeList = new RealmFieldType[15];

      for(RealmFieldType[] var1 = values(); var0 < var1.length; ++var0) {
         typeList[var1[var0].nativeValue] = var1[var0];
      }

   }

   private RealmFieldType(int var3) {
      this.nativeValue = var3;
   }

   public static RealmFieldType fromNativeValue(int var0) {
      if(var0 >= 0 && var0 < typeList.length) {
         RealmFieldType var1 = typeList[var0];
         if(var1 != null) {
            return var1;
         }
      }

      throw new IllegalArgumentException("Invalid native Realm type: " + var0);
   }

   public int getNativeValue() {
      return this.nativeValue;
   }

   public boolean isValid(Object var1) {
      boolean var3 = false;
      boolean var2 = var3;
      switch(this.nativeValue) {
      case 0:
         if(!(var1 instanceof Long) && !(var1 instanceof Integer) && !(var1 instanceof Short)) {
            var2 = var3;
            if(!(var1 instanceof Byte)) {
               break;
            }
         }

         var2 = true;
         break;
      case 1:
         var2 = var1 instanceof Boolean;
         break;
      case 2:
         var2 = var1 instanceof String;
         break;
      case 3:
      case 6:
      case 11:
      default:
         throw new RuntimeException("Unsupported Realm type:  " + this);
      case 4:
         if(!(var1 instanceof byte[])) {
            var2 = var3;
            if(!(var1 instanceof ByteBuffer)) {
               break;
            }
         }

         var2 = true;
         break;
      case 5:
         if(var1 != null) {
            var2 = var3;
            if(!(var1 instanceof Object[][])) {
               break;
            }
         }

         var2 = true;
         break;
      case 7:
         var2 = var1 instanceof Date;
         break;
      case 8:
         var2 = var1 instanceof Date;
         break;
      case 9:
         var2 = var1 instanceof Float;
         break;
      case 10:
         var2 = var1 instanceof Double;
      case 12:
      case 13:
      case 14:
      }

      return var2;
   }
}
