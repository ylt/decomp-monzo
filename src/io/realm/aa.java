package io.realm;

import io.realm.exceptions.RealmException;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.SharedRealm;
import io.realm.internal.Table;
import io.realm.log.RealmLog;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class aa extends co.uk.getmondo.d.p implements ab, io.realm.internal.l {
   private static final OsObjectSchemaInfo c = j();
   private static final List d;
   private aa.a a;
   private au b;

   static {
      ArrayList var0 = new ArrayList();
      var0.add("id");
      var0.add("enabled");
      var0.add("max");
      var0.add("min");
      var0.add("ineligibilityReason");
      d = Collections.unmodifiableList(var0);
   }

   aa() {
      this.b.f();
   }

   public static long a(av var0, co.uk.getmondo.d.p var1, Map var2) {
      long var5;
      if(var1 instanceof io.realm.internal.l && ((io.realm.internal.l)var1).s_().a() != null && ((io.realm.internal.l)var1).s_().a().g().equals(var0.g())) {
         var5 = ((io.realm.internal.l)var1).s_().b().c();
      } else {
         Table var9 = var0.c(co.uk.getmondo.d.p.class);
         long var7 = var9.getNativePtr();
         aa.a var11 = (aa.a)var0.f.c(co.uk.getmondo.d.p.class);
         long var3 = var9.d();
         String var10 = ((ab)var1).c();
         if(var10 == null) {
            var3 = Table.nativeFindFirstNull(var7, var3);
         } else {
            var3 = Table.nativeFindFirstString(var7, var3, var10);
         }

         if(var3 == -1L) {
            var3 = OsObject.b(var9, var10);
         } else {
            Table.a((Object)var10);
         }

         var2.put(var1, Long.valueOf(var3));
         Table.nativeSetBoolean(var7, var11.b, var3, ((ab)var1).d(), false);
         Table.nativeSetLong(var7, var11.c, var3, (long)((ab)var1).e(), false);
         Table.nativeSetLong(var7, var11.d, var3, (long)((ab)var1).f(), false);
         String var12 = ((ab)var1).g();
         var5 = var3;
         if(var12 != null) {
            Table.nativeSetString(var7, var11.e, var3, var12, false);
            var5 = var3;
         }
      }

      return var5;
   }

   public static co.uk.getmondo.d.p a(co.uk.getmondo.d.p var0, int var1, int var2, Map var3) {
      if(var1 <= var2 && var0 != null) {
         io.realm.internal.l.a var4 = (io.realm.internal.l.a)var3.get(var0);
         co.uk.getmondo.d.p var6;
         if(var4 == null) {
            co.uk.getmondo.d.p var7 = new co.uk.getmondo.d.p();
            var3.put(var0, new io.realm.internal.l.a(var1, var7));
            var6 = var7;
         } else {
            if(var1 >= var4.a) {
               var0 = (co.uk.getmondo.d.p)var4.b;
               return var0;
            }

            var6 = (co.uk.getmondo.d.p)var4.b;
            var4.a = var1;
         }

         ab var8 = (ab)var6;
         ab var5 = (ab)var0;
         var8.a(var5.c());
         var8.a(var5.d());
         var8.a(var5.e());
         var8.b(var5.f());
         var8.b(var5.g());
         var0 = var6;
      } else {
         var0 = null;
      }

      return var0;
   }

   static co.uk.getmondo.d.p a(av var0, co.uk.getmondo.d.p var1, co.uk.getmondo.d.p var2, Map var3) {
      ab var4 = (ab)var1;
      ab var5 = (ab)var2;
      var4.a(var5.d());
      var4.a(var5.e());
      var4.b(var5.f());
      var4.b(var5.g());
      return var1;
   }

   public static co.uk.getmondo.d.p a(av var0, co.uk.getmondo.d.p var1, boolean var2, Map var3) {
      if(var1 instanceof io.realm.internal.l && ((io.realm.internal.l)var1).s_().a() != null && ((io.realm.internal.l)var1).s_().a().c != var0.c) {
         throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
      } else {
         if(!(var1 instanceof io.realm.internal.l) || ((io.realm.internal.l)var1).s_().a() == null || !((io.realm.internal.l)var1).s_().a().g().equals(var0.g())) {
            g.b var8 = (g.b)g.g.get();
            io.realm.internal.l var7 = (io.realm.internal.l)var3.get(var1);
            if(var7 != null) {
               var1 = (co.uk.getmondo.d.p)var7;
            } else {
               boolean var4;
               aa var13;
               if(var2) {
                  Table var9 = var0.c(co.uk.getmondo.d.p.class);
                  long var5 = var9.d();
                  String var12 = ((ab)var1).c();
                  if(var12 == null) {
                     var5 = var9.k(var5);
                  } else {
                     var5 = var9.a(var5, var12);
                  }

                  if(var5 != -1L) {
                     try {
                        var8.a(var0, var9.f(var5), var0.f.c(co.uk.getmondo.d.p.class), false, Collections.emptyList());
                        var13 = new aa();
                        var3.put(var1, (io.realm.internal.l)var13);
                     } finally {
                        var8.f();
                     }

                     var4 = var2;
                  } else {
                     var4 = false;
                     var13 = null;
                  }
               } else {
                  var4 = var2;
                  var13 = null;
               }

               if(var4) {
                  var1 = a(var0, var13, var1, var3);
               } else {
                  var1 = b(var0, var1, var2, var3);
               }
            }
         }

         return var1;
      }
   }

   public static aa.a a(SharedRealm var0, boolean var1) {
      if(!var0.a("class_InboundP2P")) {
         throw new RealmMigrationNeededException(var0.h(), "The 'InboundP2P' class is missing from the schema for this Realm.");
      } else {
         Table var8 = var0.b("class_InboundP2P");
         long var4 = var8.c();
         if(var4 != 5L) {
            if(var4 < 5L) {
               throw new RealmMigrationNeededException(var0.h(), "Field count is less than expected - expected 5 but was " + var4);
            }

            if(!var1) {
               throw new RealmMigrationNeededException(var0.h(), "Field count is more than expected - expected 5 but was " + var4);
            }

            RealmLog.a("Field count is more than expected - expected 5 but was %1$d", new Object[]{Long.valueOf(var4)});
         }

         HashMap var6 = new HashMap();

         for(long var2 = 0L; var2 < var4; ++var2) {
            var6.put(var8.b(var2), var8.c(var2));
         }

         aa.a var7 = new aa.a(var0, var8);
         if(!var8.e()) {
            throw new RealmMigrationNeededException(var0.h(), "Primary key not defined for field 'id' in existing Realm file. @PrimaryKey was added.");
         } else if(var8.d() != var7.a) {
            throw new RealmMigrationNeededException(var0.h(), "Primary Key annotation definition was changed, from field " + var8.b(var8.d()) + " to field id");
         } else if(!var6.containsKey("id")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'id' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var6.get("id") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'id' in existing Realm file.");
         } else if(!var8.a(var7.a)) {
            throw new RealmMigrationNeededException(var0.h(), "@PrimaryKey field 'id' does not support null values in the existing Realm file. Migrate using RealmObjectSchema.setNullable(), or mark the field as @Required.");
         } else if(!var8.j(var8.a("id"))) {
            throw new RealmMigrationNeededException(var0.h(), "Index not defined for field 'id' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex().");
         } else if(!var6.containsKey("enabled")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'enabled' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var6.get("enabled") != RealmFieldType.BOOLEAN) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'boolean' for field 'enabled' in existing Realm file.");
         } else if(var8.a(var7.b)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'enabled' does support null values in the existing Realm file. Use corresponding boxed type for field 'enabled' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var6.containsKey("max")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'max' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var6.get("max") != RealmFieldType.INTEGER) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'int' for field 'max' in existing Realm file.");
         } else if(var8.a(var7.c)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'max' does support null values in the existing Realm file. Use corresponding boxed type for field 'max' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var6.containsKey("min")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'min' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var6.get("min") != RealmFieldType.INTEGER) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'int' for field 'min' in existing Realm file.");
         } else if(var8.a(var7.d)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'min' does support null values in the existing Realm file. Use corresponding boxed type for field 'min' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var6.containsKey("ineligibilityReason")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'ineligibilityReason' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var6.get("ineligibilityReason") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'ineligibilityReason' in existing Realm file.");
         } else if(!var8.a(var7.e)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'ineligibilityReason' is required. Either set @Required to field 'ineligibilityReason' or migrate using RealmObjectSchema.setNullable().");
         } else {
            return var7;
         }
      }
   }

   public static void a(av var0, Iterator var1, Map var2) {
      Table var12 = var0.c(co.uk.getmondo.d.p.class);
      long var7 = var12.getNativePtr();
      aa.a var11 = (aa.a)var0.f.c(co.uk.getmondo.d.p.class);
      long var9 = var12.d();

      while(true) {
         while(true) {
            co.uk.getmondo.d.p var14;
            do {
               if(!var1.hasNext()) {
                  return;
               }

               var14 = (co.uk.getmondo.d.p)var1.next();
            } while(var2.containsKey(var14));

            if(var14 instanceof io.realm.internal.l && ((io.realm.internal.l)var14).s_().a() != null && ((io.realm.internal.l)var14).s_().a().g().equals(var0.g())) {
               var2.put(var14, Long.valueOf(((io.realm.internal.l)var14).s_().b().c()));
            } else {
               String var13 = ((ab)var14).c();
               long var3;
               if(var13 == null) {
                  var3 = Table.nativeFindFirstNull(var7, var9);
               } else {
                  var3 = Table.nativeFindFirstString(var7, var9, var13);
               }

               long var5 = var3;
               if(var3 == -1L) {
                  var5 = OsObject.b(var12, var13);
               }

               var2.put(var14, Long.valueOf(var5));
               Table.nativeSetBoolean(var7, var11.b, var5, ((ab)var14).d(), false);
               Table.nativeSetLong(var7, var11.c, var5, (long)((ab)var14).e(), false);
               Table.nativeSetLong(var7, var11.d, var5, (long)((ab)var14).f(), false);
               var13 = ((ab)var14).g();
               if(var13 != null) {
                  Table.nativeSetString(var7, var11.e, var5, var13, false);
               } else {
                  Table.nativeSetNull(var7, var11.e, var5, false);
               }
            }
         }
      }
   }

   public static long b(av var0, co.uk.getmondo.d.p var1, Map var2) {
      long var3;
      if(var1 instanceof io.realm.internal.l && ((io.realm.internal.l)var1).s_().a() != null && ((io.realm.internal.l)var1).s_().a().g().equals(var0.g())) {
         var3 = ((io.realm.internal.l)var1).s_().b().c();
      } else {
         Table var9 = var0.c(co.uk.getmondo.d.p.class);
         long var7 = var9.getNativePtr();
         aa.a var11 = (aa.a)var0.f.c(co.uk.getmondo.d.p.class);
         var3 = var9.d();
         String var10 = ((ab)var1).c();
         long var5;
         if(var10 == null) {
            var5 = Table.nativeFindFirstNull(var7, var3);
         } else {
            var5 = Table.nativeFindFirstString(var7, var3, var10);
         }

         var3 = var5;
         if(var5 == -1L) {
            var3 = OsObject.b(var9, var10);
         }

         var2.put(var1, Long.valueOf(var3));
         Table.nativeSetBoolean(var7, var11.b, var3, ((ab)var1).d(), false);
         Table.nativeSetLong(var7, var11.c, var3, (long)((ab)var1).e(), false);
         Table.nativeSetLong(var7, var11.d, var3, (long)((ab)var1).f(), false);
         String var12 = ((ab)var1).g();
         if(var12 != null) {
            Table.nativeSetString(var7, var11.e, var3, var12, false);
         } else {
            Table.nativeSetNull(var7, var11.e, var3, false);
         }
      }

      return var3;
   }

   public static co.uk.getmondo.d.p b(av var0, co.uk.getmondo.d.p var1, boolean var2, Map var3) {
      io.realm.internal.l var4 = (io.realm.internal.l)var3.get(var1);
      co.uk.getmondo.d.p var5;
      if(var4 != null) {
         var5 = (co.uk.getmondo.d.p)var4;
      } else {
         var5 = (co.uk.getmondo.d.p)var0.a(co.uk.getmondo.d.p.class, ((ab)var1).c(), false, Collections.emptyList());
         var3.put(var1, (io.realm.internal.l)var5);
         ab var7 = (ab)var1;
         ab var6 = (ab)var5;
         var6.a(var7.d());
         var6.a(var7.e());
         var6.b(var7.f());
         var6.b(var7.g());
      }

      return var5;
   }

   public static OsObjectSchemaInfo h() {
      return c;
   }

   public static String i() {
      return "class_InboundP2P";
   }

   private static OsObjectSchemaInfo j() {
      OsObjectSchemaInfo.a var0 = new OsObjectSchemaInfo.a("InboundP2P");
      var0.a("id", RealmFieldType.STRING, true, true, false);
      var0.a("enabled", RealmFieldType.BOOLEAN, false, false, true);
      var0.a("max", RealmFieldType.INTEGER, false, false, true);
      var0.a("min", RealmFieldType.INTEGER, false, false, true);
      var0.a("ineligibilityReason", RealmFieldType.STRING, false, false, false);
      return var0.a();
   }

   public void a(int var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            var2.b().a(this.a.c, var2.c(), (long)var1, true);
         }
      } else {
         this.b.a().e();
         this.b.b().a(this.a.c, (long)var1);
      }

   }

   public void a(String var1) {
      if(!this.b.e()) {
         this.b.a().e();
         throw new RealmException("Primary key field 'id' cannot be changed after object was created.");
      }
   }

   public void a(boolean var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            var2.b().a(this.a.b, var2.c(), var1, true);
         }
      } else {
         this.b.a().e();
         this.b.b().a(this.a.b, var1);
      }

   }

   public void b(int var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            var2.b().a(this.a.d, var2.c(), (long)var1, true);
         }
      } else {
         this.b.a().e();
         this.b.b().a(this.a.d, (long)var1);
      }

   }

   public void b(String var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            if(var1 == null) {
               var2.b().a(this.a.e, var2.c(), true);
            } else {
               var2.b().a(this.a.e, var2.c(), var1, true);
            }
         }
      } else {
         this.b.a().e();
         if(var1 == null) {
            this.b.b().c(this.a.e);
         } else {
            this.b.b().a(this.a.e, var1);
         }
      }

   }

   public String c() {
      this.b.a().e();
      return this.b.b().k(this.a.a);
   }

   public boolean d() {
      this.b.a().e();
      return this.b.b().g(this.a.b);
   }

   public int e() {
      this.b.a().e();
      return (int)this.b.b().f(this.a.c);
   }

   public int f() {
      this.b.a().e();
      return (int)this.b.b().f(this.a.d);
   }

   public String g() {
      this.b.a().e();
      return this.b.b().k(this.a.e);
   }

   public au s_() {
      return this.b;
   }

   public String toString() {
      String var1;
      if(!bc.b(this)) {
         var1 = "Invalid object";
      } else {
         StringBuilder var2 = new StringBuilder("InboundP2P = proxy[");
         var2.append("{id:");
         if(this.c() != null) {
            var1 = this.c();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{enabled:");
         var2.append(this.d());
         var2.append("}");
         var2.append(",");
         var2.append("{max:");
         var2.append(this.e());
         var2.append("}");
         var2.append(",");
         var2.append("{min:");
         var2.append(this.f());
         var2.append("}");
         var2.append(",");
         var2.append("{ineligibilityReason:");
         if(this.g() != null) {
            var1 = this.g();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append("]");
         var1 = var2.toString();
      }

      return var1;
   }

   public void u_() {
      if(this.b == null) {
         g.b var1 = (g.b)g.g.get();
         this.a = (aa.a)var1.c();
         this.b = new au(this);
         this.b.a(var1.a());
         this.b.a(var1.b());
         this.b.a(var1.d());
         this.b.a(var1.e());
      }

   }

   static final class a extends io.realm.internal.c {
      long a;
      long b;
      long c;
      long d;
      long e;

      a(SharedRealm var1, Table var2) {
         super(5);
         this.a = this.a(var2, "id", RealmFieldType.STRING);
         this.b = this.a(var2, "enabled", RealmFieldType.BOOLEAN);
         this.c = this.a(var2, "max", RealmFieldType.INTEGER);
         this.d = this.a(var2, "min", RealmFieldType.INTEGER);
         this.e = this.a(var2, "ineligibilityReason", RealmFieldType.STRING);
      }

      a(io.realm.internal.c var1, boolean var2) {
         super(var1, var2);
         this.a(var1, this);
      }

      protected final io.realm.internal.c a(boolean var1) {
         return new aa.a(this, var1);
      }

      protected final void a(io.realm.internal.c var1, io.realm.internal.c var2) {
         aa.a var3 = (aa.a)var1;
         aa.a var4 = (aa.a)var2;
         var4.a = var3.a;
         var4.b = var3.b;
         var4.c = var3.c;
         var4.d = var3.d;
         var4.e = var3.e;
      }
   }
}
