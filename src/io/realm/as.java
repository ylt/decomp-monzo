package io.realm;

import io.realm.exceptions.RealmException;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.SharedRealm;
import io.realm.internal.Table;
import io.realm.log.RealmLog;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class as extends co.uk.getmondo.d.aa implements at, io.realm.internal.l {
   private static final OsObjectSchemaInfo c = s();
   private static final List d;
   private as.a a;
   private au b;

   static {
      ArrayList var0 = new ArrayList();
      var0.add("userId");
      var0.add("peerName");
      var0.add("phoneNumber");
      var0.add("username");
      var0.add("contactName");
      var0.add("contactPhoto");
      var0.add("isEnriched");
      d = Collections.unmodifiableList(var0);
   }

   as() {
      this.b.f();
   }

   public static long a(av var0, co.uk.getmondo.d.aa var1, Map var2) {
      long var3;
      if(var1 instanceof io.realm.internal.l && ((io.realm.internal.l)var1).s_().a() != null && ((io.realm.internal.l)var1).s_().a().g().equals(var0.g())) {
         var3 = ((io.realm.internal.l)var1).s_().b().c();
      } else {
         Table var7 = var0.c(co.uk.getmondo.d.aa.class);
         long var5 = var7.getNativePtr();
         as.a var9 = (as.a)var0.f.c(co.uk.getmondo.d.aa.class);
         var3 = var7.d();
         String var8 = ((at)var1).j();
         if(var8 == null) {
            var3 = Table.nativeFindFirstNull(var5, var3);
         } else {
            var3 = Table.nativeFindFirstString(var5, var3, var8);
         }

         if(var3 == -1L) {
            var3 = OsObject.b(var7, var8);
         } else {
            Table.a((Object)var8);
         }

         var2.put(var1, Long.valueOf(var3));
         String var10 = ((at)var1).k();
         if(var10 != null) {
            Table.nativeSetString(var5, var9.b, var3, var10, false);
         }

         var10 = ((at)var1).l();
         if(var10 != null) {
            Table.nativeSetString(var5, var9.c, var3, var10, false);
         }

         var10 = ((at)var1).m();
         if(var10 != null) {
            Table.nativeSetString(var5, var9.d, var3, var10, false);
         }

         var10 = ((at)var1).n();
         if(var10 != null) {
            Table.nativeSetString(var5, var9.e, var3, var10, false);
         }

         var10 = ((at)var1).o();
         if(var10 != null) {
            Table.nativeSetString(var5, var9.f, var3, var10, false);
         }

         Table.nativeSetBoolean(var5, var9.g, var3, ((at)var1).p(), false);
      }

      return var3;
   }

   public static co.uk.getmondo.d.aa a(co.uk.getmondo.d.aa var0, int var1, int var2, Map var3) {
      if(var1 <= var2 && var0 != null) {
         io.realm.internal.l.a var4 = (io.realm.internal.l.a)var3.get(var0);
         co.uk.getmondo.d.aa var6;
         if(var4 == null) {
            co.uk.getmondo.d.aa var7 = new co.uk.getmondo.d.aa();
            var3.put(var0, new io.realm.internal.l.a(var1, var7));
            var6 = var7;
         } else {
            if(var1 >= var4.a) {
               var0 = (co.uk.getmondo.d.aa)var4.b;
               return var0;
            }

            var6 = (co.uk.getmondo.d.aa)var4.b;
            var4.a = var1;
         }

         at var8 = (at)var6;
         at var5 = (at)var0;
         var8.c(var5.j());
         var8.d(var5.k());
         var8.e(var5.l());
         var8.f(var5.m());
         var8.g(var5.n());
         var8.h(var5.o());
         var8.b(var5.p());
         var0 = var6;
      } else {
         var0 = null;
      }

      return var0;
   }

   static co.uk.getmondo.d.aa a(av var0, co.uk.getmondo.d.aa var1, co.uk.getmondo.d.aa var2, Map var3) {
      at var4 = (at)var1;
      at var5 = (at)var2;
      var4.d(var5.k());
      var4.e(var5.l());
      var4.f(var5.m());
      var4.g(var5.n());
      var4.h(var5.o());
      var4.b(var5.p());
      return var1;
   }

   public static co.uk.getmondo.d.aa a(av var0, co.uk.getmondo.d.aa var1, boolean var2, Map var3) {
      if(var1 instanceof io.realm.internal.l && ((io.realm.internal.l)var1).s_().a() != null && ((io.realm.internal.l)var1).s_().a().c != var0.c) {
         throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
      } else {
         if(!(var1 instanceof io.realm.internal.l) || ((io.realm.internal.l)var1).s_().a() == null || !((io.realm.internal.l)var1).s_().a().g().equals(var0.g())) {
            g.b var8 = (g.b)g.g.get();
            io.realm.internal.l var7 = (io.realm.internal.l)var3.get(var1);
            if(var7 != null) {
               var1 = (co.uk.getmondo.d.aa)var7;
            } else {
               boolean var6;
               as var13;
               if(var2) {
                  Table var9 = var0.c(co.uk.getmondo.d.aa.class);
                  long var4 = var9.d();
                  String var12 = ((at)var1).j();
                  if(var12 == null) {
                     var4 = var9.k(var4);
                  } else {
                     var4 = var9.a(var4, var12);
                  }

                  if(var4 != -1L) {
                     try {
                        var8.a(var0, var9.f(var4), var0.f.c(co.uk.getmondo.d.aa.class), false, Collections.emptyList());
                        var13 = new as();
                        var3.put(var1, (io.realm.internal.l)var13);
                     } finally {
                        var8.f();
                     }

                     var6 = var2;
                  } else {
                     var6 = false;
                     var13 = null;
                  }
               } else {
                  var6 = var2;
                  var13 = null;
               }

               if(var6) {
                  var1 = a(var0, var13, var1, var3);
               } else {
                  var1 = b(var0, var1, var2, var3);
               }
            }
         }

         return var1;
      }
   }

   public static as.a a(SharedRealm var0, boolean var1) {
      if(!var0.a("class_Peer")) {
         throw new RealmMigrationNeededException(var0.h(), "The 'Peer' class is missing from the schema for this Realm.");
      } else {
         Table var8 = var0.b("class_Peer");
         long var4 = var8.c();
         if(var4 != 7L) {
            if(var4 < 7L) {
               throw new RealmMigrationNeededException(var0.h(), "Field count is less than expected - expected 7 but was " + var4);
            }

            if(!var1) {
               throw new RealmMigrationNeededException(var0.h(), "Field count is more than expected - expected 7 but was " + var4);
            }

            RealmLog.a("Field count is more than expected - expected 7 but was %1$d", new Object[]{Long.valueOf(var4)});
         }

         HashMap var7 = new HashMap();

         for(long var2 = 0L; var2 < var4; ++var2) {
            var7.put(var8.b(var2), var8.c(var2));
         }

         as.a var6 = new as.a(var0, var8);
         if(!var8.e()) {
            throw new RealmMigrationNeededException(var0.h(), "Primary key not defined for field 'userId' in existing Realm file. @PrimaryKey was added.");
         } else if(var8.d() != var6.a) {
            throw new RealmMigrationNeededException(var0.h(), "Primary Key annotation definition was changed, from field " + var8.b(var8.d()) + " to field userId");
         } else if(!var7.containsKey("userId")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'userId' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var7.get("userId") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'userId' in existing Realm file.");
         } else if(!var8.a(var6.a)) {
            throw new RealmMigrationNeededException(var0.h(), "@PrimaryKey field 'userId' does not support null values in the existing Realm file. Migrate using RealmObjectSchema.setNullable(), or mark the field as @Required.");
         } else if(!var8.j(var8.a("userId"))) {
            throw new RealmMigrationNeededException(var0.h(), "Index not defined for field 'userId' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex().");
         } else if(!var7.containsKey("peerName")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'peerName' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var7.get("peerName") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'peerName' in existing Realm file.");
         } else if(!var8.a(var6.b)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'peerName' is required. Either set @Required to field 'peerName' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var7.containsKey("phoneNumber")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'phoneNumber' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var7.get("phoneNumber") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'phoneNumber' in existing Realm file.");
         } else if(!var8.a(var6.c)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'phoneNumber' is required. Either set @Required to field 'phoneNumber' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var7.containsKey("username")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'username' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var7.get("username") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'username' in existing Realm file.");
         } else if(!var8.a(var6.d)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'username' is required. Either set @Required to field 'username' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var7.containsKey("contactName")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'contactName' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var7.get("contactName") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'contactName' in existing Realm file.");
         } else if(!var8.a(var6.e)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'contactName' is required. Either set @Required to field 'contactName' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var7.containsKey("contactPhoto")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'contactPhoto' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var7.get("contactPhoto") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'contactPhoto' in existing Realm file.");
         } else if(!var8.a(var6.f)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'contactPhoto' is required. Either set @Required to field 'contactPhoto' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var7.containsKey("isEnriched")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'isEnriched' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var7.get("isEnriched") != RealmFieldType.BOOLEAN) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'boolean' for field 'isEnriched' in existing Realm file.");
         } else if(var8.a(var6.g)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'isEnriched' does support null values in the existing Realm file. Use corresponding boxed type for field 'isEnriched' or migrate using RealmObjectSchema.setNullable().");
         } else {
            return var6;
         }
      }
   }

   public static void a(av var0, Iterator var1, Map var2) {
      Table var13 = var0.c(co.uk.getmondo.d.aa.class);
      long var9 = var13.getNativePtr();
      as.a var12 = (as.a)var0.f.c(co.uk.getmondo.d.aa.class);
      long var7 = var13.d();

      while(true) {
         while(true) {
            co.uk.getmondo.d.aa var11;
            do {
               if(!var1.hasNext()) {
                  return;
               }

               var11 = (co.uk.getmondo.d.aa)var1.next();
            } while(var2.containsKey(var11));

            if(var11 instanceof io.realm.internal.l && ((io.realm.internal.l)var11).s_().a() != null && ((io.realm.internal.l)var11).s_().a().g().equals(var0.g())) {
               var2.put(var11, Long.valueOf(((io.realm.internal.l)var11).s_().b().c()));
            } else {
               String var14 = ((at)var11).j();
               long var3;
               if(var14 == null) {
                  var3 = Table.nativeFindFirstNull(var9, var7);
               } else {
                  var3 = Table.nativeFindFirstString(var9, var7, var14);
               }

               long var5 = var3;
               if(var3 == -1L) {
                  var5 = OsObject.b(var13, var14);
               }

               var2.put(var11, Long.valueOf(var5));
               var14 = ((at)var11).k();
               if(var14 != null) {
                  Table.nativeSetString(var9, var12.b, var5, var14, false);
               } else {
                  Table.nativeSetNull(var9, var12.b, var5, false);
               }

               var14 = ((at)var11).l();
               if(var14 != null) {
                  Table.nativeSetString(var9, var12.c, var5, var14, false);
               } else {
                  Table.nativeSetNull(var9, var12.c, var5, false);
               }

               var14 = ((at)var11).m();
               if(var14 != null) {
                  Table.nativeSetString(var9, var12.d, var5, var14, false);
               } else {
                  Table.nativeSetNull(var9, var12.d, var5, false);
               }

               var14 = ((at)var11).n();
               if(var14 != null) {
                  Table.nativeSetString(var9, var12.e, var5, var14, false);
               } else {
                  Table.nativeSetNull(var9, var12.e, var5, false);
               }

               var14 = ((at)var11).o();
               if(var14 != null) {
                  Table.nativeSetString(var9, var12.f, var5, var14, false);
               } else {
                  Table.nativeSetNull(var9, var12.f, var5, false);
               }

               Table.nativeSetBoolean(var9, var12.g, var5, ((at)var11).p(), false);
            }
         }
      }
   }

   public static long b(av var0, co.uk.getmondo.d.aa var1, Map var2) {
      long var5;
      if(var1 instanceof io.realm.internal.l && ((io.realm.internal.l)var1).s_().a() != null && ((io.realm.internal.l)var1).s_().a().g().equals(var0.g())) {
         var5 = ((io.realm.internal.l)var1).s_().b().c();
      } else {
         Table var9 = var0.c(co.uk.getmondo.d.aa.class);
         long var7 = var9.getNativePtr();
         as.a var11 = (as.a)var0.f.c(co.uk.getmondo.d.aa.class);
         long var3 = var9.d();
         String var10 = ((at)var1).j();
         if(var10 == null) {
            var3 = Table.nativeFindFirstNull(var7, var3);
         } else {
            var3 = Table.nativeFindFirstString(var7, var3, var10);
         }

         var5 = var3;
         if(var3 == -1L) {
            var5 = OsObject.b(var9, var10);
         }

         var2.put(var1, Long.valueOf(var5));
         String var12 = ((at)var1).k();
         if(var12 != null) {
            Table.nativeSetString(var7, var11.b, var5, var12, false);
         } else {
            Table.nativeSetNull(var7, var11.b, var5, false);
         }

         var12 = ((at)var1).l();
         if(var12 != null) {
            Table.nativeSetString(var7, var11.c, var5, var12, false);
         } else {
            Table.nativeSetNull(var7, var11.c, var5, false);
         }

         var12 = ((at)var1).m();
         if(var12 != null) {
            Table.nativeSetString(var7, var11.d, var5, var12, false);
         } else {
            Table.nativeSetNull(var7, var11.d, var5, false);
         }

         var12 = ((at)var1).n();
         if(var12 != null) {
            Table.nativeSetString(var7, var11.e, var5, var12, false);
         } else {
            Table.nativeSetNull(var7, var11.e, var5, false);
         }

         var12 = ((at)var1).o();
         if(var12 != null) {
            Table.nativeSetString(var7, var11.f, var5, var12, false);
         } else {
            Table.nativeSetNull(var7, var11.f, var5, false);
         }

         Table.nativeSetBoolean(var7, var11.g, var5, ((at)var1).p(), false);
      }

      return var5;
   }

   public static co.uk.getmondo.d.aa b(av var0, co.uk.getmondo.d.aa var1, boolean var2, Map var3) {
      io.realm.internal.l var4 = (io.realm.internal.l)var3.get(var1);
      co.uk.getmondo.d.aa var5;
      if(var4 != null) {
         var5 = (co.uk.getmondo.d.aa)var4;
      } else {
         var5 = (co.uk.getmondo.d.aa)var0.a(co.uk.getmondo.d.aa.class, ((at)var1).j(), false, Collections.emptyList());
         var3.put(var1, (io.realm.internal.l)var5);
         at var7 = (at)var1;
         at var6 = (at)var5;
         var6.d(var7.k());
         var6.e(var7.l());
         var6.f(var7.m());
         var6.g(var7.n());
         var6.h(var7.o());
         var6.b(var7.p());
      }

      return var5;
   }

   public static OsObjectSchemaInfo q() {
      return c;
   }

   public static String r() {
      return "class_Peer";
   }

   private static OsObjectSchemaInfo s() {
      OsObjectSchemaInfo.a var0 = new OsObjectSchemaInfo.a("Peer");
      var0.a("userId", RealmFieldType.STRING, true, true, false);
      var0.a("peerName", RealmFieldType.STRING, false, false, false);
      var0.a("phoneNumber", RealmFieldType.STRING, false, false, false);
      var0.a("username", RealmFieldType.STRING, false, false, false);
      var0.a("contactName", RealmFieldType.STRING, false, false, false);
      var0.a("contactPhoto", RealmFieldType.STRING, false, false, false);
      var0.a("isEnriched", RealmFieldType.BOOLEAN, false, false, true);
      return var0.a();
   }

   public void b(boolean var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            var2.b().a(this.a.g, var2.c(), var1, true);
         }
      } else {
         this.b.a().e();
         this.b.b().a(this.a.g, var1);
      }

   }

   public void c(String var1) {
      if(!this.b.e()) {
         this.b.a().e();
         throw new RealmException("Primary key field 'userId' cannot be changed after object was created.");
      }
   }

   public void d(String var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            if(var1 == null) {
               var2.b().a(this.a.b, var2.c(), true);
            } else {
               var2.b().a(this.a.b, var2.c(), var1, true);
            }
         }
      } else {
         this.b.a().e();
         if(var1 == null) {
            this.b.b().c(this.a.b);
         } else {
            this.b.b().a(this.a.b, var1);
         }
      }

   }

   public void e(String var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            if(var1 == null) {
               var2.b().a(this.a.c, var2.c(), true);
            } else {
               var2.b().a(this.a.c, var2.c(), var1, true);
            }
         }
      } else {
         this.b.a().e();
         if(var1 == null) {
            this.b.b().c(this.a.c);
         } else {
            this.b.b().a(this.a.c, var1);
         }
      }

   }

   public void f(String var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            if(var1 == null) {
               var2.b().a(this.a.d, var2.c(), true);
            } else {
               var2.b().a(this.a.d, var2.c(), var1, true);
            }
         }
      } else {
         this.b.a().e();
         if(var1 == null) {
            this.b.b().c(this.a.d);
         } else {
            this.b.b().a(this.a.d, var1);
         }
      }

   }

   public void g(String var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            if(var1 == null) {
               var2.b().a(this.a.e, var2.c(), true);
            } else {
               var2.b().a(this.a.e, var2.c(), var1, true);
            }
         }
      } else {
         this.b.a().e();
         if(var1 == null) {
            this.b.b().c(this.a.e);
         } else {
            this.b.b().a(this.a.e, var1);
         }
      }

   }

   public void h(String var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            if(var1 == null) {
               var2.b().a(this.a.f, var2.c(), true);
            } else {
               var2.b().a(this.a.f, var2.c(), var1, true);
            }
         }
      } else {
         this.b.a().e();
         if(var1 == null) {
            this.b.b().c(this.a.f);
         } else {
            this.b.b().a(this.a.f, var1);
         }
      }

   }

   public String j() {
      this.b.a().e();
      return this.b.b().k(this.a.a);
   }

   public String k() {
      this.b.a().e();
      return this.b.b().k(this.a.b);
   }

   public String l() {
      this.b.a().e();
      return this.b.b().k(this.a.c);
   }

   public String m() {
      this.b.a().e();
      return this.b.b().k(this.a.d);
   }

   public String n() {
      this.b.a().e();
      return this.b.b().k(this.a.e);
   }

   public String o() {
      this.b.a().e();
      return this.b.b().k(this.a.f);
   }

   public boolean p() {
      this.b.a().e();
      return this.b.b().g(this.a.g);
   }

   public au s_() {
      return this.b;
   }

   public String toString() {
      String var1;
      if(!bc.b(this)) {
         var1 = "Invalid object";
      } else {
         StringBuilder var2 = new StringBuilder("Peer = proxy[");
         var2.append("{userId:");
         if(this.j() != null) {
            var1 = this.j();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{peerName:");
         if(this.k() != null) {
            var1 = this.k();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{phoneNumber:");
         if(this.l() != null) {
            var1 = this.l();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{username:");
         if(this.m() != null) {
            var1 = this.m();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{contactName:");
         if(this.n() != null) {
            var1 = this.n();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{contactPhoto:");
         if(this.o() != null) {
            var1 = this.o();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{isEnriched:");
         var2.append(this.p());
         var2.append("}");
         var2.append("]");
         var1 = var2.toString();
      }

      return var1;
   }

   public void u_() {
      if(this.b == null) {
         g.b var1 = (g.b)g.g.get();
         this.a = (as.a)var1.c();
         this.b = new au(this);
         this.b.a(var1.a());
         this.b.a(var1.b());
         this.b.a(var1.d());
         this.b.a(var1.e());
      }

   }

   static final class a extends io.realm.internal.c {
      long a;
      long b;
      long c;
      long d;
      long e;
      long f;
      long g;

      a(SharedRealm var1, Table var2) {
         super(7);
         this.a = this.a(var2, "userId", RealmFieldType.STRING);
         this.b = this.a(var2, "peerName", RealmFieldType.STRING);
         this.c = this.a(var2, "phoneNumber", RealmFieldType.STRING);
         this.d = this.a(var2, "username", RealmFieldType.STRING);
         this.e = this.a(var2, "contactName", RealmFieldType.STRING);
         this.f = this.a(var2, "contactPhoto", RealmFieldType.STRING);
         this.g = this.a(var2, "isEnriched", RealmFieldType.BOOLEAN);
      }

      a(io.realm.internal.c var1, boolean var2) {
         super(var1, var2);
         this.a(var1, this);
      }

      protected final io.realm.internal.c a(boolean var1) {
         return new as.a(this, var1);
      }

      protected final void a(io.realm.internal.c var1, io.realm.internal.c var2) {
         as.a var3 = (as.a)var1;
         as.a var4 = (as.a)var2;
         var4.a = var3.a;
         var4.b = var3.b;
         var4.c = var3.c;
         var4.d = var3.d;
         var4.e = var3.e;
         var4.f = var3.f;
         var4.g = var3.g;
      }
   }
}
