package io.realm;

import io.realm.internal.Util;
import java.io.File;
import java.util.Arrays;
import java.util.Collection;
import java.util.EnumMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;

final class aw {
   private static final List e = new LinkedList();
   private static final Collection g = new ConcurrentLinkedQueue();
   private final EnumMap a;
   private final String b;
   private ay c;
   private final io.realm.internal.b[] d;
   private final AtomicBoolean f;

   private aw(String var1) {
      int var2 = 0;
      super();
      this.d = new io.realm.internal.b[4];
      this.f = new AtomicBoolean(false);
      this.b = var1;
      this.a = new EnumMap(aw.b.class);
      aw.b[] var5 = aw.b.values();

      for(int var3 = var5.length; var2 < var3; ++var2) {
         aw.b var4 = var5[var2];
         this.a.put(var4, new aw.c());
      }

   }

   private static int a(io.realm.internal.b[] var0, io.realm.internal.b var1) {
      long var4 = Long.MAX_VALUE;
      int var3 = -1;
      int var2 = var0.length - 1;

      while(true) {
         if(var2 < 0) {
            var0[var3] = var1;
            var2 = var3;
            break;
         }

         if(var0[var2] == null) {
            var0[var2] = var1;
            break;
         }

         io.realm.internal.b var8 = var0[var2];
         long var6 = var4;
         if(var8.a() <= var4) {
            var6 = var8.a();
            var3 = var2;
         }

         --var2;
         var4 = var6;
      }

      return var2;
   }

   private static aw a(String param0, boolean param1) {
      // $FF: Couldn't be decompiled
   }

   static g a(ay var0, Class var1) {
      return a(var0.m(), true).b(var0, var1);
   }

   static io.realm.internal.b a(io.realm.internal.b[] var0, long var1) {
      int var3 = var0.length - 1;

      io.realm.internal.b var5;
      while(true) {
         if(var3 < 0) {
            var5 = null;
            break;
         }

         io.realm.internal.b var4 = var0[var3];
         if(var4 != null && var4.a() == var1) {
            var5 = var4;
            break;
         }

         --var3;
      }

      return var5;
   }

   private void a(aw.a var1) {
      synchronized(this){}

      try {
         var1.a(this.d());
      } finally {
         ;
      }

   }

   private void a(ay var1) {
      if(!this.c.equals(var1)) {
         if(!Arrays.equals(this.c.c(), var1.c())) {
            throw new IllegalArgumentException("Wrong key used to decrypt Realm.");
         } else {
            ba var3 = var1.e();
            ba var2 = this.c.e();
            if(var2 != null && var3 != null && var2.getClass().equals(var3.getClass()) && !var3.equals(var2)) {
               throw new IllegalArgumentException("Configurations cannot be different if used to open the same file. The most likely cause is that equals() and hashCode() are not overridden in the migration class: " + var1.e().getClass().getCanonicalName());
            } else {
               throw new IllegalArgumentException("Configurations cannot be different if used to open the same file. \nCached configuration: \n" + this.c + "\n\nNew configuration: \n" + var1);
            }
         }
      }
   }

   static void a(ay param0, aw.a param1) {
      // $FF: Couldn't be decompiled
   }

   private static void a(String param0, File param1) {
      // $FF: Couldn't be decompiled
   }

   private g b(ay param1, Class param2) {
      // $FF: Couldn't be decompiled
   }

   private static void b(ay var0) {
      if(var0.j()) {
         File var1 = new File(var0.a(), var0.b());
         a(var0.k(), var1);
      }

      String var2 = io.realm.internal.h.a(var0.q()).d(var0);
      if(!Util.a(var2)) {
         a(var2, new File(io.realm.internal.h.a(var0.q()).e(var0)));
      }

   }

   private int d() {
      Iterator var2 = this.a.values().iterator();

      int var1;
      for(var1 = 0; var2.hasNext(); var1 += ((aw.c)var2.next()).c) {
         ;
      }

      return var1;
   }

   public ay a() {
      return this.c;
   }

   void a(av param1) {
      // $FF: Couldn't be decompiled
   }

   void a(g param1) {
      // $FF: Couldn't be decompiled
   }

   public io.realm.internal.b[] b() {
      return this.d;
   }

   void c() {
      if(!this.f.getAndSet(true)) {
         g.add(this);
      }

   }

   interface a {
      void a(int var1);
   }

   private static enum b {
      a,
      b;

      static aw.b a(Class var0) {
         aw.b var1;
         if(var0 == av.class) {
            var1 = a;
         } else {
            if(var0 != o.class) {
               throw new IllegalArgumentException("The type of Realm class must be Realm or DynamicRealm.");
            }

            var1 = b;
         }

         return var1;
      }
   }

   private static class c {
      private final ThreadLocal a;
      private final ThreadLocal b;
      private int c;

      private c() {
         this.a = new ThreadLocal();
         this.b = new ThreadLocal();
         this.c = 0;
      }

      // $FF: synthetic method
      c(Object var1) {
         this();
      }

      // $FF: synthetic method
      static ThreadLocal a(aw.c var0) {
         return var0.a;
      }

      // $FF: synthetic method
      static ThreadLocal b(aw.c var0) {
         return var0.b;
      }

      // $FF: synthetic method
      static int d(aw.c var0) {
         int var1 = var0.c;
         var0.c = var1 + 1;
         return var1;
      }

      // $FF: synthetic method
      static int e(aw.c var0) {
         int var1 = var0.c;
         var0.c = var1 - 1;
         return var1;
      }
   }
}
