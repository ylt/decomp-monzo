package io.realm;

import io.realm.internal.Collection;
import io.realm.internal.LinkView;
import io.realm.internal.SortDescriptor;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;
import java.util.NoSuchElementException;

public class az extends AbstractList implements OrderedRealmCollection {
   protected Class a;
   protected String b;
   final LinkView c;
   protected g d;
   private final Collection e;
   private List f;

   public az() {
      this.e = null;
      this.c = null;
      this.f = new ArrayList();
   }

   az(Class var1, LinkView var2, g var3) {
      this.e = new Collection(var3.e, var2, (SortDescriptor)null);
      this.a = var1;
      this.c = var2;
      this.d = var3;
   }

   private bb a(boolean var1, bb var2) {
      if(this.a()) {
         this.e();
         if(!this.c.c()) {
            var2 = this.b(0);
            return var2;
         }
      } else if(this.f != null && !this.f.isEmpty()) {
         var2 = (bb)this.f.get(0);
         return var2;
      }

      if(var1) {
         throw new IndexOutOfBoundsException("The list is empty.");
      } else {
         return var2;
      }
   }

   private bb b(bb var1) {
      if(var1 instanceof io.realm.internal.l) {
         io.realm.internal.l var3 = (io.realm.internal.l)var1;
         if(var3 instanceof p) {
            String var4 = this.c.e().k();
            if(var3.s_().a() != this.d) {
               if(this.d.c == var3.s_().a().c) {
                  throw new IllegalArgumentException("Cannot copy DynamicRealmObject between Realm instances.");
               }

               throw new IllegalStateException("Cannot copy an object to a Realm instance created in another thread.");
            }

            String var5 = ((p)var1).b();
            if(!var4.equals(var5)) {
               throw new IllegalArgumentException(String.format(Locale.US, "The object has a different type from list's. Type of the list is '%s', type of object is '%s'.", new Object[]{var4, var5}));
            }

            return var1;
         }

         if(var3.s_().b() != null && var3.s_().a().g().equals(this.d.g())) {
            if(this.d != var3.s_().a()) {
               throw new IllegalArgumentException("Cannot copy an object from another Realm instance.");
            }

            return var1;
         }
      }

      av var2 = (av)this.d;
      if(var2.c(var1.getClass()).e()) {
         var1 = var2.b(var1);
      } else {
         var1 = var2.a(var1);
      }

      return var1;
   }

   private void c(bb var1) {
      if(var1 == null) {
         throw new IllegalArgumentException("RealmList does not accept null values");
      }
   }

   private boolean d() {
      boolean var1;
      if(this.c != null && this.c.d()) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   private void e() {
      this.d.e();
      if(this.c == null || !this.c.d()) {
         throw new IllegalStateException("Realm instance has been closed or this object or its parent has been deleted.");
      }
   }

   public bb a(int var1) {
      bb var2;
      if(this.a()) {
         this.e();
         var2 = this.b(var1);
         this.c.c((long)var1);
      } else {
         var2 = (bb)this.f.remove(var1);
      }

      ++this.modCount;
      return var2;
   }

   public void a(int var1, bb var2) {
      this.c(var2);
      if(this.a()) {
         this.e();
         if(var1 < 0 || var1 > this.size()) {
            throw new IndexOutOfBoundsException("Invalid index " + var1 + ", size is " + this.size());
         }

         io.realm.internal.l var3 = (io.realm.internal.l)this.b(var2);
         this.c.a((long)var1, var3.s_().b().c());
      } else {
         this.f.add(var1, var2);
      }

      ++this.modCount;
   }

   public boolean a() {
      boolean var1;
      if(this.d != null) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public boolean a(bb var1) {
      this.c(var1);
      if(this.a()) {
         this.e();
         io.realm.internal.l var2 = (io.realm.internal.l)this.b(var1);
         this.c.b(var2.s_().b().c());
      } else {
         this.f.add(var1);
      }

      ++this.modCount;
      return true;
   }

   // $FF: synthetic method
   public void add(int var1, Object var2) {
      this.a(var1, (bb)var2);
   }

   // $FF: synthetic method
   public boolean add(Object var1) {
      return this.a((bb)var1);
   }

   public bb b() {
      return this.a(true, (bb)null);
   }

   public bb b(int var1) {
      bb var4;
      if(this.a()) {
         this.e();
         long var2 = this.c.a((long)var1);
         var4 = this.d.a(this.a, this.b, var2);
      } else {
         var4 = (bb)this.f.get(var1);
      }

      return var4;
   }

   public bb b(int var1, bb var2) {
      this.c(var2);
      if(this.a()) {
         this.e();
         io.realm.internal.l var3 = (io.realm.internal.l)this.b(var2);
         var2 = this.b(var1);
         this.c.b((long)var1, var3.s_().b().c());
      } else {
         var2 = (bb)this.f.set(var1, var2);
      }

      return var2;
   }

   public boolean c() {
      return true;
   }

   public void clear() {
      if(this.a()) {
         this.e();
         this.c.a();
      } else {
         this.f.clear();
      }

      ++this.modCount;
   }

   public boolean contains(Object var1) {
      boolean var2;
      if(this.a()) {
         this.d.e();
         if(var1 instanceof io.realm.internal.l && ((io.realm.internal.l)var1).s_().b() == io.realm.internal.e.a) {
            var2 = false;
         } else {
            Iterator var3 = this.iterator();

            while(true) {
               if(!var3.hasNext()) {
                  var2 = false;
                  break;
               }

               if(((bb)var3.next()).equals(var1)) {
                  var2 = true;
                  break;
               }
            }
         }
      } else {
         var2 = this.f.contains(var1);
      }

      return var2;
   }

   // $FF: synthetic method
   public Object get(int var1) {
      return this.b(var1);
   }

   public Iterator iterator() {
      Object var1;
      if(this.a()) {
         var1 = new az.a();
      } else {
         var1 = super.iterator();
      }

      return (Iterator)var1;
   }

   public ListIterator listIterator() {
      return this.listIterator(0);
   }

   public ListIterator listIterator(int var1) {
      Object var2;
      if(this.a()) {
         var2 = new az.b(var1);
      } else {
         var2 = super.listIterator(var1);
      }

      return (ListIterator)var2;
   }

   // $FF: synthetic method
   public Object remove(int var1) {
      return this.a(var1);
   }

   public boolean remove(Object var1) {
      if(this.a() && !this.d.a()) {
         throw new IllegalStateException("Objects can only be removed from inside a write transaction");
      } else {
         return super.remove(var1);
      }
   }

   public boolean removeAll(java.util.Collection var1) {
      if(this.a() && !this.d.a()) {
         throw new IllegalStateException("Objects can only be removed from inside a write transaction");
      } else {
         return super.removeAll(var1);
      }
   }

   // $FF: synthetic method
   public Object set(int var1, Object var2) {
      return this.b(var1, (bb)var2);
   }

   public int size() {
      int var1;
      if(this.a()) {
         this.e();
         long var2 = this.c.b();
         if(var2 < 2147483647L) {
            var1 = (int)var2;
         } else {
            var1 = Integer.MAX_VALUE;
         }
      } else {
         var1 = this.f.size();
      }

      return var1;
   }

   public String toString() {
      StringBuilder var3 = new StringBuilder();
      String var2;
      if(this.a()) {
         var2 = this.a.getSimpleName();
      } else {
         var2 = this.getClass().getSimpleName();
      }

      var3.append(var2);
      var3.append("@[");
      if(this.a() && !this.d()) {
         var3.append("invalid");
      } else {
         for(int var1 = 0; var1 < this.size(); ++var1) {
            if(this.a()) {
               var3.append(((io.realm.internal.l)this.b(var1)).s_().b().c());
            } else {
               var3.append(System.identityHashCode(this.b(var1)));
            }

            if(var1 < this.size() - 1) {
               var3.append(',');
            }
         }
      }

      var3.append("]");
      return var3.toString();
   }

   private class a implements Iterator {
      int a;
      int b;
      int c;

      private a() {
         this.a = 0;
         this.b = -1;
         this.c = az.this.modCount;
      }

      // $FF: synthetic method
      a(Object var2) {
         this();
      }

      public bb a() {
         az.this.d.e();
         this.b();
         int var1 = this.a;

         try {
            bb var2 = az.this.b(var1);
            this.b = var1;
            this.a = var1 + 1;
            return var2;
         } catch (IndexOutOfBoundsException var3) {
            this.b();
            throw new NoSuchElementException("Cannot access index " + var1 + " when size is " + az.this.size() + ". Remember to check hasNext() before using next().");
         }
      }

      final void b() {
         if(az.this.modCount != this.c) {
            throw new ConcurrentModificationException();
         }
      }

      public boolean hasNext() {
         az.this.d.e();
         this.b();
         boolean var1;
         if(this.a != az.this.size()) {
            var1 = true;
         } else {
            var1 = false;
         }

         return var1;
      }

      // $FF: synthetic method
      public Object next() {
         return this.a();
      }

      public void remove() {
         az.this.d.e();
         if(this.b < 0) {
            throw new IllegalStateException("Cannot call remove() twice. Must call next() in between.");
         } else {
            this.b();

            try {
               az.this.a(this.b);
               if(this.b < this.a) {
                  --this.a;
               }

               this.b = -1;
               this.c = az.this.modCount;
            } catch (IndexOutOfBoundsException var2) {
               throw new ConcurrentModificationException();
            }
         }
      }
   }

   private class b extends az.a implements ListIterator {
      b(int var2) {
         super(null);
         if(var2 >= 0 && var2 <= az.this.size()) {
            this.a = var2;
         } else {
            throw new IndexOutOfBoundsException("Starting location must be a valid index: [0, " + (az.this.size() - 1) + "]. Index was " + var2);
         }
      }

      public void a(bb var1) {
         az.this.d.e();
         if(this.b < 0) {
            throw new IllegalStateException();
         } else {
            this.b();

            try {
               az.this.b(this.b, var1);
               this.c = az.this.modCount;
            } catch (IndexOutOfBoundsException var2) {
               throw new ConcurrentModificationException();
            }
         }
      }

      // $FF: synthetic method
      public void add(Object var1) {
         this.b((bb)var1);
      }

      public void b(bb var1) {
         az.this.d.e();
         this.b();

         try {
            int var2 = this.a;
            az.this.a(var2, var1);
            this.b = -1;
            this.a = var2 + 1;
            this.c = az.this.modCount;
         } catch (IndexOutOfBoundsException var3) {
            throw new ConcurrentModificationException();
         }
      }

      public bb c() {
         this.b();
         int var1 = this.a - 1;

         try {
            bb var2 = az.this.b(var1);
            this.a = var1;
            this.b = var1;
            return var2;
         } catch (IndexOutOfBoundsException var3) {
            this.b();
            throw new NoSuchElementException("Cannot access index less than zero. This was " + var1 + ". Remember to check hasPrevious() before using previous().");
         }
      }

      public boolean hasPrevious() {
         boolean var1;
         if(this.a != 0) {
            var1 = true;
         } else {
            var1 = false;
         }

         return var1;
      }

      public int nextIndex() {
         return this.a;
      }

      // $FF: synthetic method
      public Object previous() {
         return this.c();
      }

      public int previousIndex() {
         return this.a - 1;
      }

      // $FF: synthetic method
      public void set(Object var1) {
         this.a((bb)var1);
      }
   }
}
