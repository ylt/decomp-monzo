package io.realm;

import android.content.Context;
import android.os.SystemClock;
import io.realm.exceptions.RealmException;
import io.realm.exceptions.RealmFileException;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.OsObject;
import io.realm.internal.Table;
import io.realm.log.RealmLog;
import java.io.File;
import java.io.FileNotFoundException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class av extends g {
   private static final Object h = new Object();
   private static ay i;

   private av(aw var1) {
      super(var1);
   }

   static av a(aw var0) {
      ay var2 = var0.a();

      av var5;
      av var6;
      try {
         var6 = b(var0);
      } catch (RealmMigrationNeededException var4) {
         RealmMigrationNeededException var1 = var4;
         if(var2.f()) {
            c(var2);
         } else {
            try {
               if(var2.e() != null) {
                  a(var2, var1);
               }
            } catch (FileNotFoundException var3) {
               throw new RealmFileException(RealmFileException.Kind.NOT_FOUND, var3);
            }
         }

         var5 = b(var0);
         return var5;
      }

      var5 = var6;
      return var5;
   }

   private bb a(bb var1, int var2, Map var3) {
      this.e();
      return this.d.h().a(var1, var2, var3);
   }

   private bb a(bb var1, boolean var2, Map var3) {
      this.e();
      return this.d.h().a(this, var1, var2, var3);
   }

   private void a(int var1) {
      if(var1 < 0) {
         throw new IllegalArgumentException("maxDepth must be > 0. It was: " + var1);
      }
   }

   public static void a(Context param0) {
      // $FF: Couldn't be decompiled
   }

   private static void a(av param0) {
      // $FF: Couldn't be decompiled
   }

   private static void a(ay var0, RealmMigrationNeededException var1) throws FileNotFoundException {
      g.a(var0, (ba)null, new g.a() {
         public void a() {
         }
      }, var1);
   }

   private static av b(aw var0) {
      av var5 = new av(var0);
      ay var6 = var5.d;
      long var1 = var5.i();
      long var3 = var6.d();
      io.realm.internal.b var8 = aw.a(var0.b(), var3);
      if(var8 != null) {
         var5.f.a(var8);
      } else {
         if(!var6.q() && var1 != -1L) {
            if(var1 < var3) {
               var5.j();
               throw new RealmMigrationNeededException(var6.m(), String.format(Locale.US, "Realm on disk need to migrate from v%s to v%s", new Object[]{Long.valueOf(var1), Long.valueOf(var3)}));
            }

            if(var3 < var1) {
               var5.j();
               throw new IllegalArgumentException(String.format(Locale.US, "Realm on disk is newer than the one specified: v%s vs. v%s", new Object[]{Long.valueOf(var1), Long.valueOf(var3)}));
            }
         }

         try {
            a(var5);
         } catch (RuntimeException var7) {
            var5.j();
            throw var7;
         }
      }

      return var5;
   }

   private static void b(Context var0) {
      File var6 = var0.getFilesDir();
      if(var6 != null) {
         if(var6.exists()) {
            return;
         }

         try {
            var6.mkdirs();
         } catch (SecurityException var8) {
            ;
         }
      }

      if(var6 == null || !var6.exists()) {
         long[] var9 = new long[]{1L, 2L, 5L, 10L, 16L};
         long var2 = 0L;
         int var1 = -1;

         while(var0.getFilesDir() == null || !var0.getFilesDir().exists()) {
            ++var1;
            long var4 = var9[Math.min(var1, var9.length - 1)];
            SystemClock.sleep(var4);
            var4 += var2;
            var2 = var4;
            if(var4 > 200L) {
               break;
            }
         }
      }

      if(var0.getFilesDir() == null || !var0.getFilesDir().exists()) {
         throw new IllegalStateException("Context.getFilesDir() returns " + var0.getFilesDir() + " which is not an existing directory. See https://issuetracker.google.com/issues/36918154");
      }
   }

   public static void b(ay param0) {
      // $FF: Couldn't be decompiled
   }

   public static boolean c(ay var0) {
      return g.a(var0);
   }

   private void d(Class var1) {
      if(!this.f.a(var1).e()) {
         throw new IllegalArgumentException("A RealmObject with no @PrimaryKey cannot be updated: " + var1.toString());
      }
   }

   private void f(bb var1) {
      if(var1 == null) {
         throw new IllegalArgumentException("Null objects cannot be copied into Realm.");
      }
   }

   private void g(bb var1) {
      if(var1 == null) {
         throw new IllegalArgumentException("Null objects cannot be copied from Realm.");
      } else if(bc.c(var1) && bc.b(var1)) {
         if(var1 instanceof p) {
            throw new IllegalArgumentException("DynamicRealmObject cannot be copied from Realm.");
         }
      } else {
         throw new IllegalArgumentException("Only valid managed objects can be copied from Realm.");
      }
   }

   public static av n() {
      ay var0 = o();
      if(var0 == null) {
         if(g.a == null) {
            throw new IllegalStateException("Call `Realm.init(Context)` before calling this method.");
         } else {
            throw new IllegalStateException("Set default configuration by using `Realm.setDefaultConfiguration(RealmConfiguration)`.");
         }
      } else {
         return (av)aw.a(var0, av.class);
      }
   }

   public static ay o() {
      // $FF: Couldn't be decompiled
   }

   public static Object p() {
      Object var0;
      try {
         Constructor var5 = Class.forName("io.realm.DefaultRealmModule").getDeclaredConstructors()[0];
         var5.setAccessible(true);
         var0 = var5.newInstance(new Object[0]);
      } catch (ClassNotFoundException var1) {
         var0 = null;
      } catch (InvocationTargetException var2) {
         throw new RealmException("Could not create an instance of " + "io.realm.DefaultRealmModule", var2);
      } catch (InstantiationException var3) {
         throw new RealmException("Could not create an instance of " + "io.realm.DefaultRealmModule", var3);
      } catch (IllegalAccessException var4) {
         throw new RealmException("Could not create an instance of " + "io.realm.DefaultRealmModule", var4);
      }

      return var0;
   }

   public bb a(bb var1) {
      this.f(var1);
      return this.a((bb)var1, false, (Map)(new HashMap()));
   }

   public bb a(bb var1, int var2) {
      this.a(var2);
      this.g(var1);
      return this.a(var1, var2, new HashMap());
   }

   bb a(Class var1, Object var2, boolean var3, List var4) {
      Table var5 = this.f.a(var1);
      return this.d.h().a(var1, this, OsObject.a(var5, var2), this.f.c(var1), var3, var4);
   }

   bb a(Class var1, boolean var2, List var3) {
      Table var4 = this.f.a(var1);
      if(var4.e()) {
         throw new RealmException(String.format(Locale.US, "'%s' has a primary key, use 'createObject(Class<E>, Object)' instead.", new Object[]{var4.k()}));
      } else {
         return this.d.h().a(var1, this, OsObject.a(var4), this.f.c(var1), var2, var3);
      }
   }

   public bf a(Class var1) {
      this.e();
      return bf.a(this, var1);
   }

   io.realm.internal.b a(io.realm.internal.b[] param1) {
      // $FF: Couldn't be decompiled
   }

   public List a(Iterable var1) {
      return this.a(var1, Integer.MAX_VALUE);
   }

   public List a(Iterable var1, int var2) {
      this.a(var2);
      ArrayList var6;
      if(var1 == null) {
         var6 = new ArrayList(0);
      } else {
         ArrayList var3 = new ArrayList();
         HashMap var4 = new HashMap();
         Iterator var7 = var1.iterator();

         while(var7.hasNext()) {
            bb var5 = (bb)var7.next();
            this.g(var5);
            var3.add(this.a(var5, var2, var4));
         }

         var6 = var3;
      }

      return var6;
   }

   public void a(av.a var1) {
      if(var1 == null) {
         throw new IllegalArgumentException("Transaction should not be null");
      } else {
         this.b();

         try {
            var1.a(this);
            this.c();
         } catch (Throwable var2) {
            if(this.a()) {
               this.d();
            } else {
               RealmLog.b("Could not cancel transaction, not currently in a transaction.", new Object[0]);
            }

            throw var2;
         }
      }
   }

   public void a(Collection var1) {
      this.f();
      if(var1 == null) {
         throw new IllegalArgumentException("Null objects cannot be inserted into Realm.");
      } else {
         if(!var1.isEmpty()) {
            this.d.h().a(this, var1);
         }

      }
   }

   public bb b(bb var1) {
      this.f(var1);
      this.d(var1.getClass());
      return this.a((bb)var1, true, (Map)(new HashMap()));
   }

   public void b(Class var1) {
      this.e();
      this.f.a(var1).b();
   }

   Table c(Class var1) {
      return this.f.a(var1);
   }

   public void c(bb var1) {
      this.f();
      if(var1 == null) {
         throw new IllegalArgumentException("Null object cannot be inserted into Realm.");
      } else {
         HashMap var2 = new HashMap();
         this.d.h().a(this, var1, var2);
      }
   }

   public void d(bb var1) {
      this.f();
      if(var1 == null) {
         throw new IllegalArgumentException("Null object cannot be inserted into Realm.");
      } else {
         HashMap var2 = new HashMap();
         this.d.h().b(this, var1, var2);
      }
   }

   public bb e(bb var1) {
      return this.a(var1, Integer.MAX_VALUE);
   }

   public interface a {
      void a(av var1);
   }
}
