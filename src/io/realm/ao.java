package io.realm;

import io.realm.exceptions.RealmException;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.SharedRealm;
import io.realm.internal.Table;
import io.realm.log.RealmLog;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class ao extends co.uk.getmondo.d.w implements ap, io.realm.internal.l {
   private static final OsObjectSchemaInfo c = v();
   private static final List d;
   private ao.a a;
   private au b;

   static {
      ArrayList var0 = new ArrayList();
      var0.add("accountId");
      var0.add("active");
      var0.add("eligible");
      var0.add("_currency");
      var0.add("_accruedFeesAmount");
      var0.add("_bufferAmount");
      var0.add("_dailyFeeAmount");
      var0.add("_limitAmount");
      var0.add("_preapprovedLimitAmount");
      var0.add("_chargeDate");
      d = Collections.unmodifiableList(var0);
   }

   ao() {
      this.b.f();
   }

   public static long a(av var0, co.uk.getmondo.d.w var1, Map var2) {
      long var5;
      if(var1 instanceof io.realm.internal.l && ((io.realm.internal.l)var1).s_().a() != null && ((io.realm.internal.l)var1).s_().a().g().equals(var0.g())) {
         var5 = ((io.realm.internal.l)var1).s_().b().c();
      } else {
         Table var9 = var0.c(co.uk.getmondo.d.w.class);
         long var7 = var9.getNativePtr();
         ao.a var11 = (ao.a)var0.f.c(co.uk.getmondo.d.w.class);
         long var3 = var9.d();
         String var10 = ((ap)var1).j();
         if(var10 == null) {
            var3 = Table.nativeFindFirstNull(var7, var3);
         } else {
            var3 = Table.nativeFindFirstString(var7, var3, var10);
         }

         if(var3 == -1L) {
            var3 = OsObject.b(var9, var10);
         } else {
            Table.a((Object)var10);
         }

         var2.put(var1, Long.valueOf(var3));
         Table.nativeSetBoolean(var7, var11.b, var3, ((ap)var1).k(), false);
         Table.nativeSetBoolean(var7, var11.c, var3, ((ap)var1).l(), false);
         String var13 = ((ap)var1).m();
         if(var13 != null) {
            Table.nativeSetString(var7, var11.d, var3, var13, false);
         }

         Table.nativeSetLong(var7, var11.e, var3, ((ap)var1).n(), false);
         Table.nativeSetLong(var7, var11.f, var3, ((ap)var1).o(), false);
         Table.nativeSetLong(var7, var11.g, var3, ((ap)var1).p(), false);
         Table.nativeSetLong(var7, var11.h, var3, ((ap)var1).q(), false);
         Table.nativeSetLong(var7, var11.i, var3, ((ap)var1).r(), false);
         String var12 = ((ap)var1).s();
         var5 = var3;
         if(var12 != null) {
            Table.nativeSetString(var7, var11.j, var3, var12, false);
            var5 = var3;
         }
      }

      return var5;
   }

   public static co.uk.getmondo.d.w a(co.uk.getmondo.d.w var0, int var1, int var2, Map var3) {
      if(var1 <= var2 && var0 != null) {
         io.realm.internal.l.a var4 = (io.realm.internal.l.a)var3.get(var0);
         co.uk.getmondo.d.w var6;
         if(var4 == null) {
            co.uk.getmondo.d.w var7 = new co.uk.getmondo.d.w();
            var3.put(var0, new io.realm.internal.l.a(var1, var7));
            var6 = var7;
         } else {
            if(var1 >= var4.a) {
               var0 = (co.uk.getmondo.d.w)var4.b;
               return var0;
            }

            var6 = (co.uk.getmondo.d.w)var4.b;
            var4.a = var1;
         }

         ap var8 = (ap)var6;
         ap var5 = (ap)var0;
         var8.a(var5.j());
         var8.a(var5.k());
         var8.b(var5.l());
         var8.b(var5.m());
         var8.a(var5.n());
         var8.b(var5.o());
         var8.c(var5.p());
         var8.d(var5.q());
         var8.e(var5.r());
         var8.c(var5.s());
         var0 = var6;
      } else {
         var0 = null;
      }

      return var0;
   }

   static co.uk.getmondo.d.w a(av var0, co.uk.getmondo.d.w var1, co.uk.getmondo.d.w var2, Map var3) {
      ap var4 = (ap)var1;
      ap var5 = (ap)var2;
      var4.a(var5.k());
      var4.b(var5.l());
      var4.b(var5.m());
      var4.a(var5.n());
      var4.b(var5.o());
      var4.c(var5.p());
      var4.d(var5.q());
      var4.e(var5.r());
      var4.c(var5.s());
      return var1;
   }

   public static co.uk.getmondo.d.w a(av var0, co.uk.getmondo.d.w var1, boolean var2, Map var3) {
      if(var1 instanceof io.realm.internal.l && ((io.realm.internal.l)var1).s_().a() != null && ((io.realm.internal.l)var1).s_().a().c != var0.c) {
         throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
      } else {
         if(!(var1 instanceof io.realm.internal.l) || ((io.realm.internal.l)var1).s_().a() == null || !((io.realm.internal.l)var1).s_().a().g().equals(var0.g())) {
            g.b var8 = (g.b)g.g.get();
            io.realm.internal.l var7 = (io.realm.internal.l)var3.get(var1);
            if(var7 != null) {
               var1 = (co.uk.getmondo.d.w)var7;
            } else {
               boolean var4;
               ao var13;
               if(var2) {
                  Table var12 = var0.c(co.uk.getmondo.d.w.class);
                  long var5 = var12.d();
                  String var9 = ((ap)var1).j();
                  if(var9 == null) {
                     var5 = var12.k(var5);
                  } else {
                     var5 = var12.a(var5, var9);
                  }

                  if(var5 != -1L) {
                     try {
                        var8.a(var0, var12.f(var5), var0.f.c(co.uk.getmondo.d.w.class), false, Collections.emptyList());
                        var13 = new ao();
                        var3.put(var1, (io.realm.internal.l)var13);
                     } finally {
                        var8.f();
                     }

                     var4 = var2;
                  } else {
                     var4 = false;
                     var13 = null;
                  }
               } else {
                  var4 = var2;
                  var13 = null;
               }

               if(var4) {
                  var1 = a(var0, var13, var1, var3);
               } else {
                  var1 = b(var0, var1, var2, var3);
               }
            }
         }

         return var1;
      }
   }

   public static ao.a a(SharedRealm var0, boolean var1) {
      if(!var0.a("class_OverdraftStatus")) {
         throw new RealmMigrationNeededException(var0.h(), "The 'OverdraftStatus' class is missing from the schema for this Realm.");
      } else {
         Table var7 = var0.b("class_OverdraftStatus");
         long var4 = var7.c();
         if(var4 != 10L) {
            if(var4 < 10L) {
               throw new RealmMigrationNeededException(var0.h(), "Field count is less than expected - expected 10 but was " + var4);
            }

            if(!var1) {
               throw new RealmMigrationNeededException(var0.h(), "Field count is more than expected - expected 10 but was " + var4);
            }

            RealmLog.a("Field count is more than expected - expected 10 but was %1$d", new Object[]{Long.valueOf(var4)});
         }

         HashMap var6 = new HashMap();

         for(long var2 = 0L; var2 < var4; ++var2) {
            var6.put(var7.b(var2), var7.c(var2));
         }

         ao.a var8 = new ao.a(var0, var7);
         if(!var7.e()) {
            throw new RealmMigrationNeededException(var0.h(), "Primary key not defined for field 'accountId' in existing Realm file. @PrimaryKey was added.");
         } else if(var7.d() != var8.a) {
            throw new RealmMigrationNeededException(var0.h(), "Primary Key annotation definition was changed, from field " + var7.b(var7.d()) + " to field accountId");
         } else if(!var6.containsKey("accountId")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'accountId' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var6.get("accountId") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'accountId' in existing Realm file.");
         } else if(!var7.a(var8.a)) {
            throw new RealmMigrationNeededException(var0.h(), "@PrimaryKey field 'accountId' does not support null values in the existing Realm file. Migrate using RealmObjectSchema.setNullable(), or mark the field as @Required.");
         } else if(!var7.j(var7.a("accountId"))) {
            throw new RealmMigrationNeededException(var0.h(), "Index not defined for field 'accountId' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex().");
         } else if(!var6.containsKey("active")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'active' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var6.get("active") != RealmFieldType.BOOLEAN) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'boolean' for field 'active' in existing Realm file.");
         } else if(var7.a(var8.b)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'active' does support null values in the existing Realm file. Use corresponding boxed type for field 'active' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var6.containsKey("eligible")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'eligible' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var6.get("eligible") != RealmFieldType.BOOLEAN) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'boolean' for field 'eligible' in existing Realm file.");
         } else if(var7.a(var8.c)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'eligible' does support null values in the existing Realm file. Use corresponding boxed type for field 'eligible' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var6.containsKey("_currency")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field '_currency' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var6.get("_currency") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field '_currency' in existing Realm file.");
         } else if(!var7.a(var8.d)) {
            throw new RealmMigrationNeededException(var0.h(), "Field '_currency' is required. Either set @Required to field '_currency' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var6.containsKey("_accruedFeesAmount")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field '_accruedFeesAmount' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var6.get("_accruedFeesAmount") != RealmFieldType.INTEGER) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'long' for field '_accruedFeesAmount' in existing Realm file.");
         } else if(var7.a(var8.e)) {
            throw new RealmMigrationNeededException(var0.h(), "Field '_accruedFeesAmount' does support null values in the existing Realm file. Use corresponding boxed type for field '_accruedFeesAmount' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var6.containsKey("_bufferAmount")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field '_bufferAmount' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var6.get("_bufferAmount") != RealmFieldType.INTEGER) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'long' for field '_bufferAmount' in existing Realm file.");
         } else if(var7.a(var8.f)) {
            throw new RealmMigrationNeededException(var0.h(), "Field '_bufferAmount' does support null values in the existing Realm file. Use corresponding boxed type for field '_bufferAmount' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var6.containsKey("_dailyFeeAmount")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field '_dailyFeeAmount' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var6.get("_dailyFeeAmount") != RealmFieldType.INTEGER) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'long' for field '_dailyFeeAmount' in existing Realm file.");
         } else if(var7.a(var8.g)) {
            throw new RealmMigrationNeededException(var0.h(), "Field '_dailyFeeAmount' does support null values in the existing Realm file. Use corresponding boxed type for field '_dailyFeeAmount' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var6.containsKey("_limitAmount")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field '_limitAmount' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var6.get("_limitAmount") != RealmFieldType.INTEGER) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'long' for field '_limitAmount' in existing Realm file.");
         } else if(var7.a(var8.h)) {
            throw new RealmMigrationNeededException(var0.h(), "Field '_limitAmount' does support null values in the existing Realm file. Use corresponding boxed type for field '_limitAmount' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var6.containsKey("_preapprovedLimitAmount")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field '_preapprovedLimitAmount' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var6.get("_preapprovedLimitAmount") != RealmFieldType.INTEGER) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'long' for field '_preapprovedLimitAmount' in existing Realm file.");
         } else if(var7.a(var8.i)) {
            throw new RealmMigrationNeededException(var0.h(), "Field '_preapprovedLimitAmount' does support null values in the existing Realm file. Use corresponding boxed type for field '_preapprovedLimitAmount' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var6.containsKey("_chargeDate")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field '_chargeDate' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var6.get("_chargeDate") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field '_chargeDate' in existing Realm file.");
         } else if(!var7.a(var8.j)) {
            throw new RealmMigrationNeededException(var0.h(), "Field '_chargeDate' is required. Either set @Required to field '_chargeDate' or migrate using RealmObjectSchema.setNullable().");
         } else {
            return var8;
         }
      }
   }

   public static void a(av var0, Iterator var1, Map var2) {
      Table var11 = var0.c(co.uk.getmondo.d.w.class);
      long var7 = var11.getNativePtr();
      ao.a var12 = (ao.a)var0.f.c(co.uk.getmondo.d.w.class);
      long var9 = var11.d();

      while(true) {
         while(true) {
            co.uk.getmondo.d.w var13;
            do {
               if(!var1.hasNext()) {
                  return;
               }

               var13 = (co.uk.getmondo.d.w)var1.next();
            } while(var2.containsKey(var13));

            if(var13 instanceof io.realm.internal.l && ((io.realm.internal.l)var13).s_().a() != null && ((io.realm.internal.l)var13).s_().a().g().equals(var0.g())) {
               var2.put(var13, Long.valueOf(((io.realm.internal.l)var13).s_().b().c()));
            } else {
               String var14 = ((ap)var13).j();
               long var3;
               if(var14 == null) {
                  var3 = Table.nativeFindFirstNull(var7, var9);
               } else {
                  var3 = Table.nativeFindFirstString(var7, var9, var14);
               }

               long var5 = var3;
               if(var3 == -1L) {
                  var5 = OsObject.b(var11, var14);
               }

               var2.put(var13, Long.valueOf(var5));
               Table.nativeSetBoolean(var7, var12.b, var5, ((ap)var13).k(), false);
               Table.nativeSetBoolean(var7, var12.c, var5, ((ap)var13).l(), false);
               var14 = ((ap)var13).m();
               if(var14 != null) {
                  Table.nativeSetString(var7, var12.d, var5, var14, false);
               } else {
                  Table.nativeSetNull(var7, var12.d, var5, false);
               }

               Table.nativeSetLong(var7, var12.e, var5, ((ap)var13).n(), false);
               Table.nativeSetLong(var7, var12.f, var5, ((ap)var13).o(), false);
               Table.nativeSetLong(var7, var12.g, var5, ((ap)var13).p(), false);
               Table.nativeSetLong(var7, var12.h, var5, ((ap)var13).q(), false);
               Table.nativeSetLong(var7, var12.i, var5, ((ap)var13).r(), false);
               String var15 = ((ap)var13).s();
               if(var15 != null) {
                  Table.nativeSetString(var7, var12.j, var5, var15, false);
               } else {
                  Table.nativeSetNull(var7, var12.j, var5, false);
               }
            }
         }
      }
   }

   public static long b(av var0, co.uk.getmondo.d.w var1, Map var2) {
      long var3;
      if(var1 instanceof io.realm.internal.l && ((io.realm.internal.l)var1).s_().a() != null && ((io.realm.internal.l)var1).s_().a().g().equals(var0.g())) {
         var3 = ((io.realm.internal.l)var1).s_().b().c();
      } else {
         Table var9 = var0.c(co.uk.getmondo.d.w.class);
         long var7 = var9.getNativePtr();
         ao.a var11 = (ao.a)var0.f.c(co.uk.getmondo.d.w.class);
         var3 = var9.d();
         String var10 = ((ap)var1).j();
         long var5;
         if(var10 == null) {
            var5 = Table.nativeFindFirstNull(var7, var3);
         } else {
            var5 = Table.nativeFindFirstString(var7, var3, var10);
         }

         var3 = var5;
         if(var5 == -1L) {
            var3 = OsObject.b(var9, var10);
         }

         var2.put(var1, Long.valueOf(var3));
         Table.nativeSetBoolean(var7, var11.b, var3, ((ap)var1).k(), false);
         Table.nativeSetBoolean(var7, var11.c, var3, ((ap)var1).l(), false);
         String var13 = ((ap)var1).m();
         if(var13 != null) {
            Table.nativeSetString(var7, var11.d, var3, var13, false);
         } else {
            Table.nativeSetNull(var7, var11.d, var3, false);
         }

         Table.nativeSetLong(var7, var11.e, var3, ((ap)var1).n(), false);
         Table.nativeSetLong(var7, var11.f, var3, ((ap)var1).o(), false);
         Table.nativeSetLong(var7, var11.g, var3, ((ap)var1).p(), false);
         Table.nativeSetLong(var7, var11.h, var3, ((ap)var1).q(), false);
         Table.nativeSetLong(var7, var11.i, var3, ((ap)var1).r(), false);
         String var12 = ((ap)var1).s();
         if(var12 != null) {
            Table.nativeSetString(var7, var11.j, var3, var12, false);
         } else {
            Table.nativeSetNull(var7, var11.j, var3, false);
         }
      }

      return var3;
   }

   public static co.uk.getmondo.d.w b(av var0, co.uk.getmondo.d.w var1, boolean var2, Map var3) {
      io.realm.internal.l var4 = (io.realm.internal.l)var3.get(var1);
      co.uk.getmondo.d.w var5;
      if(var4 != null) {
         var5 = (co.uk.getmondo.d.w)var4;
      } else {
         var5 = (co.uk.getmondo.d.w)var0.a(co.uk.getmondo.d.w.class, ((ap)var1).j(), false, Collections.emptyList());
         var3.put(var1, (io.realm.internal.l)var5);
         ap var6 = (ap)var1;
         ap var7 = (ap)var5;
         var7.a(var6.k());
         var7.b(var6.l());
         var7.b(var6.m());
         var7.a(var6.n());
         var7.b(var6.o());
         var7.c(var6.p());
         var7.d(var6.q());
         var7.e(var6.r());
         var7.c(var6.s());
      }

      return var5;
   }

   public static OsObjectSchemaInfo t() {
      return c;
   }

   public static String u() {
      return "class_OverdraftStatus";
   }

   private static OsObjectSchemaInfo v() {
      OsObjectSchemaInfo.a var0 = new OsObjectSchemaInfo.a("OverdraftStatus");
      var0.a("accountId", RealmFieldType.STRING, true, true, false);
      var0.a("active", RealmFieldType.BOOLEAN, false, false, true);
      var0.a("eligible", RealmFieldType.BOOLEAN, false, false, true);
      var0.a("_currency", RealmFieldType.STRING, false, false, false);
      var0.a("_accruedFeesAmount", RealmFieldType.INTEGER, false, false, true);
      var0.a("_bufferAmount", RealmFieldType.INTEGER, false, false, true);
      var0.a("_dailyFeeAmount", RealmFieldType.INTEGER, false, false, true);
      var0.a("_limitAmount", RealmFieldType.INTEGER, false, false, true);
      var0.a("_preapprovedLimitAmount", RealmFieldType.INTEGER, false, false, true);
      var0.a("_chargeDate", RealmFieldType.STRING, false, false, false);
      return var0.a();
   }

   public void a(long var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var3 = this.b.b();
            var3.b().a(this.a.e, var3.c(), var1, true);
         }
      } else {
         this.b.a().e();
         this.b.b().a(this.a.e, var1);
      }

   }

   public void a(String var1) {
      if(!this.b.e()) {
         this.b.a().e();
         throw new RealmException("Primary key field 'accountId' cannot be changed after object was created.");
      }
   }

   public void a(boolean var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            var2.b().a(this.a.b, var2.c(), var1, true);
         }
      } else {
         this.b.a().e();
         this.b.b().a(this.a.b, var1);
      }

   }

   public void b(long var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var3 = this.b.b();
            var3.b().a(this.a.f, var3.c(), var1, true);
         }
      } else {
         this.b.a().e();
         this.b.b().a(this.a.f, var1);
      }

   }

   public void b(String var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            if(var1 == null) {
               var2.b().a(this.a.d, var2.c(), true);
            } else {
               var2.b().a(this.a.d, var2.c(), var1, true);
            }
         }
      } else {
         this.b.a().e();
         if(var1 == null) {
            this.b.b().c(this.a.d);
         } else {
            this.b.b().a(this.a.d, var1);
         }
      }

   }

   public void b(boolean var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            var2.b().a(this.a.c, var2.c(), var1, true);
         }
      } else {
         this.b.a().e();
         this.b.b().a(this.a.c, var1);
      }

   }

   public void c(long var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var3 = this.b.b();
            var3.b().a(this.a.g, var3.c(), var1, true);
         }
      } else {
         this.b.a().e();
         this.b.b().a(this.a.g, var1);
      }

   }

   public void c(String var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            if(var1 == null) {
               var2.b().a(this.a.j, var2.c(), true);
            } else {
               var2.b().a(this.a.j, var2.c(), var1, true);
            }
         }
      } else {
         this.b.a().e();
         if(var1 == null) {
            this.b.b().c(this.a.j);
         } else {
            this.b.b().a(this.a.j, var1);
         }
      }

   }

   public void d(long var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var3 = this.b.b();
            var3.b().a(this.a.h, var3.c(), var1, true);
         }
      } else {
         this.b.a().e();
         this.b.b().a(this.a.h, var1);
      }

   }

   public void e(long var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var3 = this.b.b();
            var3.b().a(this.a.i, var3.c(), var1, true);
         }
      } else {
         this.b.a().e();
         this.b.b().a(this.a.i, var1);
      }

   }

   public String j() {
      this.b.a().e();
      return this.b.b().k(this.a.a);
   }

   public boolean k() {
      this.b.a().e();
      return this.b.b().g(this.a.b);
   }

   public boolean l() {
      this.b.a().e();
      return this.b.b().g(this.a.c);
   }

   public String m() {
      this.b.a().e();
      return this.b.b().k(this.a.d);
   }

   public long n() {
      this.b.a().e();
      return this.b.b().f(this.a.e);
   }

   public long o() {
      this.b.a().e();
      return this.b.b().f(this.a.f);
   }

   public long p() {
      this.b.a().e();
      return this.b.b().f(this.a.g);
   }

   public long q() {
      this.b.a().e();
      return this.b.b().f(this.a.h);
   }

   public long r() {
      this.b.a().e();
      return this.b.b().f(this.a.i);
   }

   public String s() {
      this.b.a().e();
      return this.b.b().k(this.a.j);
   }

   public au s_() {
      return this.b;
   }

   public void u_() {
      if(this.b == null) {
         g.b var1 = (g.b)g.g.get();
         this.a = (ao.a)var1.c();
         this.b = new au(this);
         this.b.a(var1.a());
         this.b.a(var1.b());
         this.b.a(var1.d());
         this.b.a(var1.e());
      }

   }

   static final class a extends io.realm.internal.c {
      long a;
      long b;
      long c;
      long d;
      long e;
      long f;
      long g;
      long h;
      long i;
      long j;

      a(SharedRealm var1, Table var2) {
         super(10);
         this.a = this.a(var2, "accountId", RealmFieldType.STRING);
         this.b = this.a(var2, "active", RealmFieldType.BOOLEAN);
         this.c = this.a(var2, "eligible", RealmFieldType.BOOLEAN);
         this.d = this.a(var2, "_currency", RealmFieldType.STRING);
         this.e = this.a(var2, "_accruedFeesAmount", RealmFieldType.INTEGER);
         this.f = this.a(var2, "_bufferAmount", RealmFieldType.INTEGER);
         this.g = this.a(var2, "_dailyFeeAmount", RealmFieldType.INTEGER);
         this.h = this.a(var2, "_limitAmount", RealmFieldType.INTEGER);
         this.i = this.a(var2, "_preapprovedLimitAmount", RealmFieldType.INTEGER);
         this.j = this.a(var2, "_chargeDate", RealmFieldType.STRING);
      }

      a(io.realm.internal.c var1, boolean var2) {
         super(var1, var2);
         this.a(var1, this);
      }

      protected final io.realm.internal.c a(boolean var1) {
         return new ao.a(this, var1);
      }

      protected final void a(io.realm.internal.c var1, io.realm.internal.c var2) {
         ao.a var3 = (ao.a)var1;
         ao.a var4 = (ao.a)var2;
         var4.a = var3.a;
         var4.b = var3.b;
         var4.c = var3.c;
         var4.d = var3.d;
         var4.e = var3.e;
         var4.f = var3.f;
         var4.g = var3.g;
         var4.h = var3.h;
         var4.i = var3.i;
         var4.j = var3.j;
      }
   }
}
