package io.realm;

import io.realm.exceptions.RealmException;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.SharedRealm;
import io.realm.internal.Table;
import io.realm.log.RealmLog;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class y extends co.uk.getmondo.d.o implements io.realm.internal.l, z {
   private static final OsObjectSchemaInfo c = e();
   private static final List d;
   private y.a a;
   private au b;

   static {
      ArrayList var0 = new ArrayList();
      var0.add("ticketId");
      d = Collections.unmodifiableList(var0);
   }

   y() {
      this.b.f();
   }

   public static long a(av var0, co.uk.getmondo.d.o var1, Map var2) {
      long var3;
      if(var1 instanceof io.realm.internal.l && ((io.realm.internal.l)var1).s_().a() != null && ((io.realm.internal.l)var1).s_().a().g().equals(var0.g())) {
         var3 = ((io.realm.internal.l)var1).s_().b().c();
      } else {
         Table var7 = var0.c(co.uk.getmondo.d.o.class);
         var3 = var7.getNativePtr();
         y.a var8 = (y.a)var0.f.c(co.uk.getmondo.d.o.class);
         long var5 = var7.d();
         String var9 = ((z)var1).b();
         if(var9 == null) {
            var3 = Table.nativeFindFirstNull(var3, var5);
         } else {
            var3 = Table.nativeFindFirstString(var3, var5, var9);
         }

         if(var3 == -1L) {
            var3 = OsObject.b(var7, var9);
         } else {
            Table.a((Object)var9);
         }

         var2.put(var1, Long.valueOf(var3));
      }

      return var3;
   }

   public static co.uk.getmondo.d.o a(co.uk.getmondo.d.o var0, int var1, int var2, Map var3) {
      if(var1 <= var2 && var0 != null) {
         io.realm.internal.l.a var4 = (io.realm.internal.l.a)var3.get(var0);
         co.uk.getmondo.d.o var5;
         if(var4 == null) {
            co.uk.getmondo.d.o var6 = new co.uk.getmondo.d.o();
            var3.put(var0, new io.realm.internal.l.a(var1, var6));
            var5 = var6;
         } else {
            if(var1 >= var4.a) {
               var0 = (co.uk.getmondo.d.o)var4.b;
               return var0;
            }

            var5 = (co.uk.getmondo.d.o)var4.b;
            var4.a = var1;
         }

         ((z)var5).a(((z)var0).b());
         var0 = var5;
      } else {
         var0 = null;
      }

      return var0;
   }

   static co.uk.getmondo.d.o a(av var0, co.uk.getmondo.d.o var1, co.uk.getmondo.d.o var2, Map var3) {
      z var4 = (z)var1;
      var4 = (z)var2;
      return var1;
   }

   public static co.uk.getmondo.d.o a(av var0, co.uk.getmondo.d.o var1, boolean var2, Map var3) {
      if(var1 instanceof io.realm.internal.l && ((io.realm.internal.l)var1).s_().a() != null && ((io.realm.internal.l)var1).s_().a().c != var0.c) {
         throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
      } else {
         if(!(var1 instanceof io.realm.internal.l) || ((io.realm.internal.l)var1).s_().a() == null || !((io.realm.internal.l)var1).s_().a().g().equals(var0.g())) {
            g.b var8 = (g.b)g.g.get();
            io.realm.internal.l var7 = (io.realm.internal.l)var3.get(var1);
            if(var7 != null) {
               var1 = (co.uk.getmondo.d.o)var7;
            } else {
               boolean var6;
               y var13;
               if(var2) {
                  Table var9 = var0.c(co.uk.getmondo.d.o.class);
                  long var4 = var9.d();
                  String var12 = ((z)var1).b();
                  if(var12 == null) {
                     var4 = var9.k(var4);
                  } else {
                     var4 = var9.a(var4, var12);
                  }

                  if(var4 != -1L) {
                     try {
                        var8.a(var0, var9.f(var4), var0.f.c(co.uk.getmondo.d.o.class), false, Collections.emptyList());
                        var13 = new y();
                        var3.put(var1, (io.realm.internal.l)var13);
                     } finally {
                        var8.f();
                     }

                     var6 = var2;
                  } else {
                     var6 = false;
                     var13 = null;
                  }
               } else {
                  var6 = var2;
                  var13 = null;
               }

               if(var6) {
                  var1 = a(var0, var13, var1, var3);
               } else {
                  var1 = b(var0, var1, var2, var3);
               }
            }
         }

         return var1;
      }
   }

   public static y.a a(SharedRealm var0, boolean var1) {
      if(!var0.a("class_GoldenTicket")) {
         throw new RealmMigrationNeededException(var0.h(), "The 'GoldenTicket' class is missing from the schema for this Realm.");
      } else {
         Table var6 = var0.b("class_GoldenTicket");
         long var4 = var6.c();
         if(var4 != 1L) {
            if(var4 < 1L) {
               throw new RealmMigrationNeededException(var0.h(), "Field count is less than expected - expected 1 but was " + var4);
            }

            if(!var1) {
               throw new RealmMigrationNeededException(var0.h(), "Field count is more than expected - expected 1 but was " + var4);
            }

            RealmLog.a("Field count is more than expected - expected 1 but was %1$d", new Object[]{Long.valueOf(var4)});
         }

         HashMap var8 = new HashMap();

         for(long var2 = 0L; var2 < var4; ++var2) {
            var8.put(var6.b(var2), var6.c(var2));
         }

         y.a var7 = new y.a(var0, var6);
         if(!var6.e()) {
            throw new RealmMigrationNeededException(var0.h(), "Primary key not defined for field 'ticketId' in existing Realm file. @PrimaryKey was added.");
         } else if(var6.d() != var7.a) {
            throw new RealmMigrationNeededException(var0.h(), "Primary Key annotation definition was changed, from field " + var6.b(var6.d()) + " to field ticketId");
         } else if(!var8.containsKey("ticketId")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'ticketId' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var8.get("ticketId") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'ticketId' in existing Realm file.");
         } else if(!var6.a(var7.a)) {
            throw new RealmMigrationNeededException(var0.h(), "@PrimaryKey field 'ticketId' does not support null values in the existing Realm file. Migrate using RealmObjectSchema.setNullable(), or mark the field as @Required.");
         } else if(!var6.j(var6.a("ticketId"))) {
            throw new RealmMigrationNeededException(var0.h(), "Index not defined for field 'ticketId' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex().");
         } else {
            return var7;
         }
      }
   }

   public static void a(av var0, Iterator var1, Map var2) {
      Table var11 = var0.c(co.uk.getmondo.d.o.class);
      long var9 = var11.getNativePtr();
      y.a var12 = (y.a)var0.f.c(co.uk.getmondo.d.o.class);
      long var7 = var11.d();

      while(true) {
         while(true) {
            co.uk.getmondo.d.o var13;
            do {
               if(!var1.hasNext()) {
                  return;
               }

               var13 = (co.uk.getmondo.d.o)var1.next();
            } while(var2.containsKey(var13));

            if(var13 instanceof io.realm.internal.l && ((io.realm.internal.l)var13).s_().a() != null && ((io.realm.internal.l)var13).s_().a().g().equals(var0.g())) {
               var2.put(var13, Long.valueOf(((io.realm.internal.l)var13).s_().b().c()));
            } else {
               String var14 = ((z)var13).b();
               long var3;
               if(var14 == null) {
                  var3 = Table.nativeFindFirstNull(var9, var7);
               } else {
                  var3 = Table.nativeFindFirstString(var9, var7, var14);
               }

               long var5 = var3;
               if(var3 == -1L) {
                  var5 = OsObject.b(var11, var14);
               }

               var2.put(var13, Long.valueOf(var5));
            }
         }
      }
   }

   public static long b(av var0, co.uk.getmondo.d.o var1, Map var2) {
      long var3;
      if(var1 instanceof io.realm.internal.l && ((io.realm.internal.l)var1).s_().a() != null && ((io.realm.internal.l)var1).s_().a().g().equals(var0.g())) {
         var3 = ((io.realm.internal.l)var1).s_().b().c();
      } else {
         Table var7 = var0.c(co.uk.getmondo.d.o.class);
         var3 = var7.getNativePtr();
         y.a var8 = (y.a)var0.f.c(co.uk.getmondo.d.o.class);
         long var5 = var7.d();
         String var9 = ((z)var1).b();
         if(var9 == null) {
            var5 = Table.nativeFindFirstNull(var3, var5);
         } else {
            var5 = Table.nativeFindFirstString(var3, var5, var9);
         }

         var3 = var5;
         if(var5 == -1L) {
            var3 = OsObject.b(var7, var9);
         }

         var2.put(var1, Long.valueOf(var3));
      }

      return var3;
   }

   public static co.uk.getmondo.d.o b(av var0, co.uk.getmondo.d.o var1, boolean var2, Map var3) {
      io.realm.internal.l var4 = (io.realm.internal.l)var3.get(var1);
      co.uk.getmondo.d.o var5;
      if(var4 != null) {
         var5 = (co.uk.getmondo.d.o)var4;
      } else {
         var5 = (co.uk.getmondo.d.o)var0.a(co.uk.getmondo.d.o.class, ((z)var1).b(), false, Collections.emptyList());
         var3.put(var1, (io.realm.internal.l)var5);
         z var6 = (z)var1;
         var6 = (z)var5;
      }

      return var5;
   }

   public static OsObjectSchemaInfo c() {
      return c;
   }

   public static String d() {
      return "class_GoldenTicket";
   }

   private static OsObjectSchemaInfo e() {
      OsObjectSchemaInfo.a var0 = new OsObjectSchemaInfo.a("GoldenTicket");
      var0.a("ticketId", RealmFieldType.STRING, true, true, false);
      return var0.a();
   }

   public void a(String var1) {
      if(!this.b.e()) {
         this.b.a().e();
         throw new RealmException("Primary key field 'ticketId' cannot be changed after object was created.");
      }
   }

   public String b() {
      this.b.a().e();
      return this.b.b().k(this.a.a);
   }

   public au s_() {
      return this.b;
   }

   public String toString() {
      String var1;
      if(!bc.b(this)) {
         var1 = "Invalid object";
      } else {
         StringBuilder var2 = new StringBuilder("GoldenTicket = proxy[");
         var2.append("{ticketId:");
         if(this.b() != null) {
            var1 = this.b();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append("]");
         var1 = var2.toString();
      }

      return var1;
   }

   public void u_() {
      if(this.b == null) {
         g.b var1 = (g.b)g.g.get();
         this.a = (y.a)var1.c();
         this.b = new au(this);
         this.b.a(var1.a());
         this.b.a(var1.b());
         this.b.a(var1.d());
         this.b.a(var1.e());
      }

   }

   static final class a extends io.realm.internal.c {
      long a;

      a(SharedRealm var1, Table var2) {
         super(1);
         this.a = this.a(var2, "ticketId", RealmFieldType.STRING);
      }

      a(io.realm.internal.c var1, boolean var2) {
         super(var1, var2);
         this.a(var1, this);
      }

      protected final io.realm.internal.c a(boolean var1) {
         return new y.a(this, var1);
      }

      protected final void a(io.realm.internal.c var1, io.realm.internal.c var2) {
         y.a var3 = (y.a)var1;
         ((y.a)var2).a = var3.a;
      }
   }
}
