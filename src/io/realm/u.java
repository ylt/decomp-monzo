package io.realm;

import io.realm.exceptions.RealmException;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.SharedRealm;
import io.realm.internal.Table;
import io.realm.log.RealmLog;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class u extends co.uk.getmondo.d.n implements io.realm.internal.l, v {
   private static final OsObjectSchemaInfo c = e();
   private static final List d;
   private u.a a;
   private au b;

   static {
      ArrayList var0 = new ArrayList();
      var0.add("id");
      var0.add("lastFetched");
      d = Collections.unmodifiableList(var0);
   }

   u() {
      this.b.f();
   }

   public static long a(av var0, co.uk.getmondo.d.n var1, Map var2) {
      long var5;
      if(var1 instanceof io.realm.internal.l && ((io.realm.internal.l)var1).s_().a() != null && ((io.realm.internal.l)var1).s_().a().g().equals(var0.g())) {
         var5 = ((io.realm.internal.l)var1).s_().b().c();
      } else {
         Table var9 = var0.c(co.uk.getmondo.d.n.class);
         long var7 = var9.getNativePtr();
         u.a var11 = (u.a)var0.f.c(co.uk.getmondo.d.n.class);
         long var3 = var9.d();
         String var10 = ((v)var1).a();
         if(var10 == null) {
            var3 = Table.nativeFindFirstNull(var7, var3);
         } else {
            var3 = Table.nativeFindFirstString(var7, var3, var10);
         }

         if(var3 == -1L) {
            var3 = OsObject.b(var9, var10);
         } else {
            Table.a((Object)var10);
         }

         var2.put(var1, Long.valueOf(var3));
         Date var12 = ((v)var1).b();
         var5 = var3;
         if(var12 != null) {
            Table.nativeSetTimestamp(var7, var11.b, var3, var12.getTime(), false);
            var5 = var3;
         }
      }

      return var5;
   }

   public static co.uk.getmondo.d.n a(co.uk.getmondo.d.n var0, int var1, int var2, Map var3) {
      if(var1 <= var2 && var0 != null) {
         io.realm.internal.l.a var4 = (io.realm.internal.l.a)var3.get(var0);
         co.uk.getmondo.d.n var6;
         if(var4 == null) {
            co.uk.getmondo.d.n var7 = new co.uk.getmondo.d.n();
            var3.put(var0, new io.realm.internal.l.a(var1, var7));
            var6 = var7;
         } else {
            if(var1 >= var4.a) {
               var0 = (co.uk.getmondo.d.n)var4.b;
               return var0;
            }

            var6 = (co.uk.getmondo.d.n)var4.b;
            var4.a = var1;
         }

         v var8 = (v)var6;
         v var5 = (v)var0;
         var8.a(var5.a());
         var8.a(var5.b());
         var0 = var6;
      } else {
         var0 = null;
      }

      return var0;
   }

   static co.uk.getmondo.d.n a(av var0, co.uk.getmondo.d.n var1, co.uk.getmondo.d.n var2, Map var3) {
      ((v)var1).a(((v)var2).b());
      return var1;
   }

   public static co.uk.getmondo.d.n a(av var0, co.uk.getmondo.d.n var1, boolean var2, Map var3) {
      if(var1 instanceof io.realm.internal.l && ((io.realm.internal.l)var1).s_().a() != null && ((io.realm.internal.l)var1).s_().a().c != var0.c) {
         throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
      } else {
         if(!(var1 instanceof io.realm.internal.l) || ((io.realm.internal.l)var1).s_().a() == null || !((io.realm.internal.l)var1).s_().a().g().equals(var0.g())) {
            g.b var8 = (g.b)g.g.get();
            io.realm.internal.l var7 = (io.realm.internal.l)var3.get(var1);
            if(var7 != null) {
               var1 = (co.uk.getmondo.d.n)var7;
            } else {
               boolean var4;
               u var13;
               if(var2) {
                  Table var12 = var0.c(co.uk.getmondo.d.n.class);
                  long var5 = var12.d();
                  String var9 = ((v)var1).a();
                  if(var9 == null) {
                     var5 = var12.k(var5);
                  } else {
                     var5 = var12.a(var5, var9);
                  }

                  if(var5 != -1L) {
                     try {
                        var8.a(var0, var12.f(var5), var0.f.c(co.uk.getmondo.d.n.class), false, Collections.emptyList());
                        var13 = new u();
                        var3.put(var1, (io.realm.internal.l)var13);
                     } finally {
                        var8.f();
                     }

                     var4 = var2;
                  } else {
                     var4 = false;
                     var13 = null;
                  }
               } else {
                  var4 = var2;
                  var13 = null;
               }

               if(var4) {
                  var1 = a(var0, var13, var1, var3);
               } else {
                  var1 = b(var0, var1, var2, var3);
               }
            }
         }

         return var1;
      }
   }

   public static u.a a(SharedRealm var0, boolean var1) {
      if(!var0.a("class_FeedMetadata")) {
         throw new RealmMigrationNeededException(var0.h(), "The 'FeedMetadata' class is missing from the schema for this Realm.");
      } else {
         Table var7 = var0.b("class_FeedMetadata");
         long var4 = var7.c();
         if(var4 != 2L) {
            if(var4 < 2L) {
               throw new RealmMigrationNeededException(var0.h(), "Field count is less than expected - expected 2 but was " + var4);
            }

            if(!var1) {
               throw new RealmMigrationNeededException(var0.h(), "Field count is more than expected - expected 2 but was " + var4);
            }

            RealmLog.a("Field count is more than expected - expected 2 but was %1$d", new Object[]{Long.valueOf(var4)});
         }

         HashMap var8 = new HashMap();

         for(long var2 = 0L; var2 < var4; ++var2) {
            var8.put(var7.b(var2), var7.c(var2));
         }

         u.a var6 = new u.a(var0, var7);
         if(!var7.e()) {
            throw new RealmMigrationNeededException(var0.h(), "Primary key not defined for field 'id' in existing Realm file. @PrimaryKey was added.");
         } else if(var7.d() != var6.a) {
            throw new RealmMigrationNeededException(var0.h(), "Primary Key annotation definition was changed, from field " + var7.b(var7.d()) + " to field id");
         } else if(!var8.containsKey("id")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'id' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var8.get("id") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'id' in existing Realm file.");
         } else if(!var7.a(var6.a)) {
            throw new RealmMigrationNeededException(var0.h(), "@PrimaryKey field 'id' does not support null values in the existing Realm file. Migrate using RealmObjectSchema.setNullable(), or mark the field as @Required.");
         } else if(!var7.j(var7.a("id"))) {
            throw new RealmMigrationNeededException(var0.h(), "Index not defined for field 'id' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex().");
         } else if(!var8.containsKey("lastFetched")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'lastFetched' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var8.get("lastFetched") != RealmFieldType.DATE) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'Date' for field 'lastFetched' in existing Realm file.");
         } else if(!var7.a(var6.b)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'lastFetched' is required. Either set @Required to field 'lastFetched' or migrate using RealmObjectSchema.setNullable().");
         } else {
            return var6;
         }
      }
   }

   public static void a(av var0, Iterator var1, Map var2) {
      Table var12 = var0.c(co.uk.getmondo.d.n.class);
      long var7 = var12.getNativePtr();
      u.a var11 = (u.a)var0.f.c(co.uk.getmondo.d.n.class);
      long var9 = var12.d();

      while(true) {
         while(true) {
            co.uk.getmondo.d.n var13;
            do {
               if(!var1.hasNext()) {
                  return;
               }

               var13 = (co.uk.getmondo.d.n)var1.next();
            } while(var2.containsKey(var13));

            if(var13 instanceof io.realm.internal.l && ((io.realm.internal.l)var13).s_().a() != null && ((io.realm.internal.l)var13).s_().a().g().equals(var0.g())) {
               var2.put(var13, Long.valueOf(((io.realm.internal.l)var13).s_().b().c()));
            } else {
               String var14 = ((v)var13).a();
               long var3;
               if(var14 == null) {
                  var3 = Table.nativeFindFirstNull(var7, var9);
               } else {
                  var3 = Table.nativeFindFirstString(var7, var9, var14);
               }

               long var5 = var3;
               if(var3 == -1L) {
                  var5 = OsObject.b(var12, var14);
               }

               var2.put(var13, Long.valueOf(var5));
               Date var15 = ((v)var13).b();
               if(var15 != null) {
                  Table.nativeSetTimestamp(var7, var11.b, var5, var15.getTime(), false);
               } else {
                  Table.nativeSetNull(var7, var11.b, var5, false);
               }
            }
         }
      }
   }

   public static long b(av var0, co.uk.getmondo.d.n var1, Map var2) {
      long var3;
      if(var1 instanceof io.realm.internal.l && ((io.realm.internal.l)var1).s_().a() != null && ((io.realm.internal.l)var1).s_().a().g().equals(var0.g())) {
         var3 = ((io.realm.internal.l)var1).s_().b().c();
      } else {
         Table var9 = var0.c(co.uk.getmondo.d.n.class);
         long var7 = var9.getNativePtr();
         u.a var11 = (u.a)var0.f.c(co.uk.getmondo.d.n.class);
         var3 = var9.d();
         String var10 = ((v)var1).a();
         long var5;
         if(var10 == null) {
            var5 = Table.nativeFindFirstNull(var7, var3);
         } else {
            var5 = Table.nativeFindFirstString(var7, var3, var10);
         }

         var3 = var5;
         if(var5 == -1L) {
            var3 = OsObject.b(var9, var10);
         }

         var2.put(var1, Long.valueOf(var3));
         Date var12 = ((v)var1).b();
         if(var12 != null) {
            Table.nativeSetTimestamp(var7, var11.b, var3, var12.getTime(), false);
         } else {
            Table.nativeSetNull(var7, var11.b, var3, false);
         }
      }

      return var3;
   }

   public static co.uk.getmondo.d.n b(av var0, co.uk.getmondo.d.n var1, boolean var2, Map var3) {
      io.realm.internal.l var4 = (io.realm.internal.l)var3.get(var1);
      co.uk.getmondo.d.n var5;
      if(var4 != null) {
         var5 = (co.uk.getmondo.d.n)var4;
      } else {
         var5 = (co.uk.getmondo.d.n)var0.a(co.uk.getmondo.d.n.class, ((v)var1).a(), false, Collections.emptyList());
         var3.put(var1, (io.realm.internal.l)var5);
         v var6 = (v)var1;
         ((v)var5).a(var6.b());
      }

      return var5;
   }

   public static OsObjectSchemaInfo c() {
      return c;
   }

   public static String d() {
      return "class_FeedMetadata";
   }

   private static OsObjectSchemaInfo e() {
      OsObjectSchemaInfo.a var0 = new OsObjectSchemaInfo.a("FeedMetadata");
      var0.a("id", RealmFieldType.STRING, true, true, false);
      var0.a("lastFetched", RealmFieldType.DATE, false, false, false);
      return var0.a();
   }

   public String a() {
      this.b.a().e();
      return this.b.b().k(this.a.a);
   }

   public void a(String var1) {
      if(!this.b.e()) {
         this.b.a().e();
         throw new RealmException("Primary key field 'id' cannot be changed after object was created.");
      }
   }

   public void a(Date var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            if(var1 == null) {
               var2.b().a(this.a.b, var2.c(), true);
            } else {
               var2.b().a(this.a.b, var2.c(), var1, true);
            }
         }
      } else {
         this.b.a().e();
         if(var1 == null) {
            this.b.b().c(this.a.b);
         } else {
            this.b.b().a(this.a.b, var1);
         }
      }

   }

   public Date b() {
      this.b.a().e();
      Date var1;
      if(this.b.b().b(this.a.b)) {
         var1 = null;
      } else {
         var1 = this.b.b().j(this.a.b);
      }

      return var1;
   }

   public au s_() {
      return this.b;
   }

   public String toString() {
      String var1;
      if(!bc.b(this)) {
         var1 = "Invalid object";
      } else {
         StringBuilder var2 = new StringBuilder("FeedMetadata = proxy[");
         var2.append("{id:");
         if(this.a() != null) {
            var1 = this.a();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{lastFetched:");
         Object var3;
         if(this.b() != null) {
            var3 = this.b();
         } else {
            var3 = "null";
         }

         var2.append(var3);
         var2.append("}");
         var2.append("]");
         var1 = var2.toString();
      }

      return var1;
   }

   public void u_() {
      if(this.b == null) {
         g.b var1 = (g.b)g.g.get();
         this.a = (u.a)var1.c();
         this.b = new au(this);
         this.b.a(var1.a());
         this.b.a(var1.b());
         this.b.a(var1.d());
         this.b.a(var1.e());
      }

   }

   static final class a extends io.realm.internal.c {
      long a;
      long b;

      a(SharedRealm var1, Table var2) {
         super(2);
         this.a = this.a(var2, "id", RealmFieldType.STRING);
         this.b = this.a(var2, "lastFetched", RealmFieldType.DATE);
      }

      a(io.realm.internal.c var1, boolean var2) {
         super(var1, var2);
         this.a(var1, this);
      }

      protected final io.realm.internal.c a(boolean var1) {
         return new u.a(this, var1);
      }

      protected final void a(io.realm.internal.c var1, io.realm.internal.c var2) {
         u.a var3 = (u.a)var1;
         u.a var4 = (u.a)var2;
         var4.a = var3.a;
         var4.b = var3.b;
      }
   }
}
