package io.realm.a;

import java.util.IdentityHashMap;
import java.util.Map;

public class a implements b {
   ThreadLocal a = new ThreadLocal() {
      protected a.a a() {
         return new a.a(null);
      }

      // $FF: synthetic method
      protected Object initialValue() {
         return this.a();
      }
   };
   ThreadLocal b = new ThreadLocal() {
      protected a.a a() {
         return new a.a(null);
      }

      // $FF: synthetic method
      protected Object initialValue() {
         return this.a();
      }
   };
   ThreadLocal c = new ThreadLocal() {
      protected a.a a() {
         return new a.a(null);
      }

      // $FF: synthetic method
      protected Object initialValue() {
         return this.a();
      }
   };

   public boolean equals(Object var1) {
      return var1 instanceof a;
   }

   public int hashCode() {
      return 37;
   }

   private static class a {
      private final Map a;

      private a() {
         this.a = new IdentityHashMap();
      }

      // $FF: synthetic method
      a(Object var1) {
         this();
      }
   }
}
