package io.realm;

import io.realm.internal.OsObject;
import io.realm.internal.UncheckedRow;
import java.util.List;

public final class au implements io.realm.internal.j.a {
   private static au.a i = new au.a();
   private bb a;
   private boolean b = true;
   private io.realm.internal.n c;
   private OsObject d;
   private g e;
   private boolean f;
   private List g;
   private io.realm.internal.i h = new io.realm.internal.i();

   public au() {
   }

   public au(bb var1) {
      this.a = var1;
   }

   private void g() {
      this.h.a((io.realm.internal.i.a)i);
   }

   private void h() {
      if(this.e.e != null && !this.e.e.i() && this.c.d() && this.d == null) {
         this.d = new OsObject(this.e.e, (UncheckedRow)this.c);
         this.d.a(this.h);
         this.h = null;
      }

   }

   public g a() {
      return this.e;
   }

   public void a(g var1) {
      this.e = var1;
   }

   public void a(io.realm.internal.n var1) {
      this.c = var1;
   }

   public void a(List var1) {
      this.g = var1;
   }

   public void a(boolean var1) {
      this.f = var1;
   }

   public io.realm.internal.n b() {
      return this.c;
   }

   public void b(io.realm.internal.n var1) {
      this.c = var1;
      this.g();
      if(var1.d()) {
         this.h();
      }

   }

   public boolean c() {
      return this.f;
   }

   public List d() {
      return this.g;
   }

   public boolean e() {
      return this.b;
   }

   public void f() {
      this.b = false;
      this.g = null;
   }

   private static class a implements io.realm.internal.i.a {
      private a() {
      }

      // $FF: synthetic method
      a(Object var1) {
         this();
      }

      public void a(OsObject.b var1, Object var2) {
         var1.a((bb)var2, (ak)null);
      }
   }
}
