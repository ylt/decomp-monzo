package io.realm;

import io.realm.exceptions.RealmException;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.SharedRealm;
import io.realm.internal.Table;
import io.realm.log.RealmLog;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class ai extends co.uk.getmondo.d.v implements aj, io.realm.internal.l {
   private static final OsObjectSchemaInfo c = i();
   private static final List d;
   private ai.a a;
   private au b;

   static {
      ArrayList var0 = new ArrayList();
      var0.add("id");
      var0.add("amountValue");
      var0.add("amountCurrency");
      var0.add("month");
      d = Collections.unmodifiableList(var0);
   }

   ai() {
      this.b.f();
   }

   public static long a(av var0, co.uk.getmondo.d.v var1, Map var2) {
      long var5;
      if(var1 instanceof io.realm.internal.l && ((io.realm.internal.l)var1).s_().a() != null && ((io.realm.internal.l)var1).s_().a().g().equals(var0.g())) {
         var5 = ((io.realm.internal.l)var1).s_().b().c();
      } else {
         Table var9 = var0.c(co.uk.getmondo.d.v.class);
         long var7 = var9.getNativePtr();
         ai.a var11 = (ai.a)var0.f.c(co.uk.getmondo.d.v.class);
         long var3 = var9.d();
         String var10 = ((aj)var1).c();
         if(var10 == null) {
            var3 = Table.nativeFindFirstNull(var7, var3);
         } else {
            var3 = Table.nativeFindFirstString(var7, var3, var10);
         }

         if(var3 == -1L) {
            var3 = OsObject.b(var9, var10);
         } else {
            Table.a((Object)var10);
         }

         var2.put(var1, Long.valueOf(var3));
         Table.nativeSetLong(var7, var11.b, var3, ((aj)var1).d(), false);
         String var13 = ((aj)var1).e();
         if(var13 != null) {
            Table.nativeSetString(var7, var11.c, var3, var13, false);
         }

         String var12 = ((aj)var1).f();
         var5 = var3;
         if(var12 != null) {
            Table.nativeSetString(var7, var11.d, var3, var12, false);
            var5 = var3;
         }
      }

      return var5;
   }

   public static co.uk.getmondo.d.v a(co.uk.getmondo.d.v var0, int var1, int var2, Map var3) {
      if(var1 <= var2 && var0 != null) {
         io.realm.internal.l.a var4 = (io.realm.internal.l.a)var3.get(var0);
         co.uk.getmondo.d.v var6;
         if(var4 == null) {
            co.uk.getmondo.d.v var7 = new co.uk.getmondo.d.v();
            var3.put(var0, new io.realm.internal.l.a(var1, var7));
            var6 = var7;
         } else {
            if(var1 >= var4.a) {
               var0 = (co.uk.getmondo.d.v)var4.b;
               return var0;
            }

            var6 = (co.uk.getmondo.d.v)var4.b;
            var4.a = var1;
         }

         aj var8 = (aj)var6;
         aj var5 = (aj)var0;
         var8.a(var5.c());
         var8.a(var5.d());
         var8.b(var5.e());
         var8.c(var5.f());
         var0 = var6;
      } else {
         var0 = null;
      }

      return var0;
   }

   static co.uk.getmondo.d.v a(av var0, co.uk.getmondo.d.v var1, co.uk.getmondo.d.v var2, Map var3) {
      aj var4 = (aj)var1;
      aj var5 = (aj)var2;
      var4.a(var5.d());
      var4.b(var5.e());
      var4.c(var5.f());
      return var1;
   }

   public static co.uk.getmondo.d.v a(av var0, co.uk.getmondo.d.v var1, boolean var2, Map var3) {
      if(var1 instanceof io.realm.internal.l && ((io.realm.internal.l)var1).s_().a() != null && ((io.realm.internal.l)var1).s_().a().c != var0.c) {
         throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
      } else {
         if(!(var1 instanceof io.realm.internal.l) || ((io.realm.internal.l)var1).s_().a() == null || !((io.realm.internal.l)var1).s_().a().g().equals(var0.g())) {
            g.b var8 = (g.b)g.g.get();
            io.realm.internal.l var7 = (io.realm.internal.l)var3.get(var1);
            if(var7 != null) {
               var1 = (co.uk.getmondo.d.v)var7;
            } else {
               boolean var6;
               ai var13;
               if(var2) {
                  Table var12 = var0.c(co.uk.getmondo.d.v.class);
                  long var4 = var12.d();
                  String var9 = ((aj)var1).c();
                  if(var9 == null) {
                     var4 = var12.k(var4);
                  } else {
                     var4 = var12.a(var4, var9);
                  }

                  if(var4 != -1L) {
                     try {
                        var8.a(var0, var12.f(var4), var0.f.c(co.uk.getmondo.d.v.class), false, Collections.emptyList());
                        var13 = new ai();
                        var3.put(var1, (io.realm.internal.l)var13);
                     } finally {
                        var8.f();
                     }

                     var6 = var2;
                  } else {
                     var6 = false;
                     var13 = null;
                  }
               } else {
                  var6 = var2;
                  var13 = null;
               }

               if(var6) {
                  var1 = a(var0, var13, var1, var3);
               } else {
                  var1 = b(var0, var1, var2, var3);
               }
            }
         }

         return var1;
      }
   }

   public static ai.a a(SharedRealm var0, boolean var1) {
      if(!var0.a("class_MonthlySpendingReport")) {
         throw new RealmMigrationNeededException(var0.h(), "The 'MonthlySpendingReport' class is missing from the schema for this Realm.");
      } else {
         Table var8 = var0.b("class_MonthlySpendingReport");
         long var4 = var8.c();
         if(var4 != 4L) {
            if(var4 < 4L) {
               throw new RealmMigrationNeededException(var0.h(), "Field count is less than expected - expected 4 but was " + var4);
            }

            if(!var1) {
               throw new RealmMigrationNeededException(var0.h(), "Field count is more than expected - expected 4 but was " + var4);
            }

            RealmLog.a("Field count is more than expected - expected 4 but was %1$d", new Object[]{Long.valueOf(var4)});
         }

         HashMap var7 = new HashMap();

         for(long var2 = 0L; var2 < var4; ++var2) {
            var7.put(var8.b(var2), var8.c(var2));
         }

         ai.a var6 = new ai.a(var0, var8);
         if(!var8.e()) {
            throw new RealmMigrationNeededException(var0.h(), "Primary key not defined for field 'id' in existing Realm file. @PrimaryKey was added.");
         } else if(var8.d() != var6.a) {
            throw new RealmMigrationNeededException(var0.h(), "Primary Key annotation definition was changed, from field " + var8.b(var8.d()) + " to field id");
         } else if(!var7.containsKey("id")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'id' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var7.get("id") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'id' in existing Realm file.");
         } else if(!var8.a(var6.a)) {
            throw new RealmMigrationNeededException(var0.h(), "@PrimaryKey field 'id' does not support null values in the existing Realm file. Migrate using RealmObjectSchema.setNullable(), or mark the field as @Required.");
         } else if(!var8.j(var8.a("id"))) {
            throw new RealmMigrationNeededException(var0.h(), "Index not defined for field 'id' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex().");
         } else if(!var7.containsKey("amountValue")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'amountValue' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var7.get("amountValue") != RealmFieldType.INTEGER) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'long' for field 'amountValue' in existing Realm file.");
         } else if(var8.a(var6.b)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'amountValue' does support null values in the existing Realm file. Use corresponding boxed type for field 'amountValue' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var7.containsKey("amountCurrency")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'amountCurrency' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var7.get("amountCurrency") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'amountCurrency' in existing Realm file.");
         } else if(!var8.a(var6.c)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'amountCurrency' is required. Either set @Required to field 'amountCurrency' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var7.containsKey("month")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'month' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var7.get("month") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'month' in existing Realm file.");
         } else if(!var8.a(var6.d)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'month' is required. Either set @Required to field 'month' or migrate using RealmObjectSchema.setNullable().");
         } else {
            return var6;
         }
      }
   }

   public static void a(av var0, Iterator var1, Map var2) {
      Table var12 = var0.c(co.uk.getmondo.d.v.class);
      long var9 = var12.getNativePtr();
      ai.a var11 = (ai.a)var0.f.c(co.uk.getmondo.d.v.class);
      long var7 = var12.d();

      while(true) {
         while(true) {
            co.uk.getmondo.d.v var13;
            do {
               if(!var1.hasNext()) {
                  return;
               }

               var13 = (co.uk.getmondo.d.v)var1.next();
            } while(var2.containsKey(var13));

            if(var13 instanceof io.realm.internal.l && ((io.realm.internal.l)var13).s_().a() != null && ((io.realm.internal.l)var13).s_().a().g().equals(var0.g())) {
               var2.put(var13, Long.valueOf(((io.realm.internal.l)var13).s_().b().c()));
            } else {
               String var14 = ((aj)var13).c();
               long var3;
               if(var14 == null) {
                  var3 = Table.nativeFindFirstNull(var9, var7);
               } else {
                  var3 = Table.nativeFindFirstString(var9, var7, var14);
               }

               long var5 = var3;
               if(var3 == -1L) {
                  var5 = OsObject.b(var12, var14);
               }

               var2.put(var13, Long.valueOf(var5));
               Table.nativeSetLong(var9, var11.b, var5, ((aj)var13).d(), false);
               var14 = ((aj)var13).e();
               if(var14 != null) {
                  Table.nativeSetString(var9, var11.c, var5, var14, false);
               } else {
                  Table.nativeSetNull(var9, var11.c, var5, false);
               }

               String var15 = ((aj)var13).f();
               if(var15 != null) {
                  Table.nativeSetString(var9, var11.d, var5, var15, false);
               } else {
                  Table.nativeSetNull(var9, var11.d, var5, false);
               }
            }
         }
      }
   }

   public static long b(av var0, co.uk.getmondo.d.v var1, Map var2) {
      long var5;
      if(var1 instanceof io.realm.internal.l && ((io.realm.internal.l)var1).s_().a() != null && ((io.realm.internal.l)var1).s_().a().g().equals(var0.g())) {
         var5 = ((io.realm.internal.l)var1).s_().b().c();
      } else {
         Table var9 = var0.c(co.uk.getmondo.d.v.class);
         long var7 = var9.getNativePtr();
         ai.a var11 = (ai.a)var0.f.c(co.uk.getmondo.d.v.class);
         long var3 = var9.d();
         String var10 = ((aj)var1).c();
         if(var10 == null) {
            var3 = Table.nativeFindFirstNull(var7, var3);
         } else {
            var3 = Table.nativeFindFirstString(var7, var3, var10);
         }

         var5 = var3;
         if(var3 == -1L) {
            var5 = OsObject.b(var9, var10);
         }

         var2.put(var1, Long.valueOf(var5));
         Table.nativeSetLong(var7, var11.b, var5, ((aj)var1).d(), false);
         String var13 = ((aj)var1).e();
         if(var13 != null) {
            Table.nativeSetString(var7, var11.c, var5, var13, false);
         } else {
            Table.nativeSetNull(var7, var11.c, var5, false);
         }

         String var12 = ((aj)var1).f();
         if(var12 != null) {
            Table.nativeSetString(var7, var11.d, var5, var12, false);
         } else {
            Table.nativeSetNull(var7, var11.d, var5, false);
         }
      }

      return var5;
   }

   public static co.uk.getmondo.d.v b(av var0, co.uk.getmondo.d.v var1, boolean var2, Map var3) {
      io.realm.internal.l var4 = (io.realm.internal.l)var3.get(var1);
      co.uk.getmondo.d.v var5;
      if(var4 != null) {
         var5 = (co.uk.getmondo.d.v)var4;
      } else {
         var5 = (co.uk.getmondo.d.v)var0.a(co.uk.getmondo.d.v.class, ((aj)var1).c(), false, Collections.emptyList());
         var3.put(var1, (io.realm.internal.l)var5);
         aj var6 = (aj)var1;
         aj var7 = (aj)var5;
         var7.a(var6.d());
         var7.b(var6.e());
         var7.c(var6.f());
      }

      return var5;
   }

   public static OsObjectSchemaInfo g() {
      return c;
   }

   public static String h() {
      return "class_MonthlySpendingReport";
   }

   private static OsObjectSchemaInfo i() {
      OsObjectSchemaInfo.a var0 = new OsObjectSchemaInfo.a("MonthlySpendingReport");
      var0.a("id", RealmFieldType.STRING, true, true, false);
      var0.a("amountValue", RealmFieldType.INTEGER, false, false, true);
      var0.a("amountCurrency", RealmFieldType.STRING, false, false, false);
      var0.a("month", RealmFieldType.STRING, false, false, false);
      return var0.a();
   }

   public void a(long var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var3 = this.b.b();
            var3.b().a(this.a.b, var3.c(), var1, true);
         }
      } else {
         this.b.a().e();
         this.b.b().a(this.a.b, var1);
      }

   }

   public void a(String var1) {
      if(!this.b.e()) {
         this.b.a().e();
         throw new RealmException("Primary key field 'id' cannot be changed after object was created.");
      }
   }

   public void b(String var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            if(var1 == null) {
               var2.b().a(this.a.c, var2.c(), true);
            } else {
               var2.b().a(this.a.c, var2.c(), var1, true);
            }
         }
      } else {
         this.b.a().e();
         if(var1 == null) {
            this.b.b().c(this.a.c);
         } else {
            this.b.b().a(this.a.c, var1);
         }
      }

   }

   public String c() {
      this.b.a().e();
      return this.b.b().k(this.a.a);
   }

   public void c(String var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            if(var1 == null) {
               var2.b().a(this.a.d, var2.c(), true);
            } else {
               var2.b().a(this.a.d, var2.c(), var1, true);
            }
         }
      } else {
         this.b.a().e();
         if(var1 == null) {
            this.b.b().c(this.a.d);
         } else {
            this.b.b().a(this.a.d, var1);
         }
      }

   }

   public long d() {
      this.b.a().e();
      return this.b.b().f(this.a.b);
   }

   public String e() {
      this.b.a().e();
      return this.b.b().k(this.a.c);
   }

   public String f() {
      this.b.a().e();
      return this.b.b().k(this.a.d);
   }

   public au s_() {
      return this.b;
   }

   public String toString() {
      String var1;
      if(!bc.b(this)) {
         var1 = "Invalid object";
      } else {
         StringBuilder var2 = new StringBuilder("MonthlySpendingReport = proxy[");
         var2.append("{id:");
         if(this.c() != null) {
            var1 = this.c();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{amountValue:");
         var2.append(this.d());
         var2.append("}");
         var2.append(",");
         var2.append("{amountCurrency:");
         if(this.e() != null) {
            var1 = this.e();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{month:");
         if(this.f() != null) {
            var1 = this.f();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append("]");
         var1 = var2.toString();
      }

      return var1;
   }

   public void u_() {
      if(this.b == null) {
         g.b var1 = (g.b)g.g.get();
         this.a = (ai.a)var1.c();
         this.b = new au(this);
         this.b.a(var1.a());
         this.b.a(var1.b());
         this.b.a(var1.d());
         this.b.a(var1.e());
      }

   }

   static final class a extends io.realm.internal.c {
      long a;
      long b;
      long c;
      long d;

      a(SharedRealm var1, Table var2) {
         super(4);
         this.a = this.a(var2, "id", RealmFieldType.STRING);
         this.b = this.a(var2, "amountValue", RealmFieldType.INTEGER);
         this.c = this.a(var2, "amountCurrency", RealmFieldType.STRING);
         this.d = this.a(var2, "month", RealmFieldType.STRING);
      }

      a(io.realm.internal.c var1, boolean var2) {
         super(var1, var2);
         this.a(var1, this);
      }

      protected final io.realm.internal.c a(boolean var1) {
         return new ai.a(this, var1);
      }

      protected final void a(io.realm.internal.c var1, io.realm.internal.c var2) {
         ai.a var3 = (ai.a)var1;
         ai.a var4 = (ai.a)var2;
         var4.a = var3.a;
         var4.b = var3.b;
         var4.c = var3.c;
         var4.d = var3.d;
      }
   }
}
