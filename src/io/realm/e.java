package io.realm;

import io.realm.exceptions.RealmException;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.SharedRealm;
import io.realm.internal.Table;
import io.realm.log.RealmLog;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class e extends co.uk.getmondo.payments.send.data.a.a implements f, io.realm.internal.l {
   private static final OsObjectSchemaInfo c = j();
   private static final List d;
   private e.a a;
   private au b;

   static {
      ArrayList var0 = new ArrayList();
      var0.add("id");
      var0.add("name");
      var0.add("sortCode");
      var0.add("accountNumber");
      d = Collections.unmodifiableList(var0);
   }

   e() {
      this.b.f();
   }

   public static long a(av var0, co.uk.getmondo.payments.send.data.a.a var1, Map var2) {
      long var5;
      if(var1 instanceof io.realm.internal.l && ((io.realm.internal.l)var1).s_().a() != null && ((io.realm.internal.l)var1).s_().a().g().equals(var0.g())) {
         var5 = ((io.realm.internal.l)var1).s_().b().c();
      } else {
         Table var9 = var0.c(co.uk.getmondo.payments.send.data.a.a.class);
         long var7 = var9.getNativePtr();
         e.a var11 = (e.a)var0.f.c(co.uk.getmondo.payments.send.data.a.a.class);
         long var3 = var9.d();
         String var10 = ((f)var1).d();
         if(var10 == null) {
            var3 = Table.nativeFindFirstNull(var7, var3);
         } else {
            var3 = Table.nativeFindFirstString(var7, var3, var10);
         }

         if(var3 == -1L) {
            var3 = OsObject.b(var9, var10);
         } else {
            Table.a((Object)var10);
         }

         var2.put(var1, Long.valueOf(var3));
         String var13 = ((f)var1).e();
         if(var13 != null) {
            Table.nativeSetString(var7, var11.b, var3, var13, false);
         }

         var13 = ((f)var1).f();
         if(var13 != null) {
            Table.nativeSetString(var7, var11.c, var3, var13, false);
         }

         String var12 = ((f)var1).g();
         var5 = var3;
         if(var12 != null) {
            Table.nativeSetString(var7, var11.d, var3, var12, false);
            var5 = var3;
         }
      }

      return var5;
   }

   public static co.uk.getmondo.payments.send.data.a.a a(co.uk.getmondo.payments.send.data.a.a var0, int var1, int var2, Map var3) {
      if(var1 <= var2 && var0 != null) {
         io.realm.internal.l.a var4 = (io.realm.internal.l.a)var3.get(var0);
         co.uk.getmondo.payments.send.data.a.a var6;
         if(var4 == null) {
            co.uk.getmondo.payments.send.data.a.a var7 = new co.uk.getmondo.payments.send.data.a.a();
            var3.put(var0, new io.realm.internal.l.a(var1, var7));
            var6 = var7;
         } else {
            if(var1 >= var4.a) {
               var0 = (co.uk.getmondo.payments.send.data.a.a)var4.b;
               return var0;
            }

            var6 = (co.uk.getmondo.payments.send.data.a.a)var4.b;
            var4.a = var1;
         }

         f var8 = (f)var6;
         f var5 = (f)var0;
         var8.a(var5.d());
         var8.b(var5.e());
         var8.c(var5.f());
         var8.d(var5.g());
         var0 = var6;
      } else {
         var0 = null;
      }

      return var0;
   }

   static co.uk.getmondo.payments.send.data.a.a a(av var0, co.uk.getmondo.payments.send.data.a.a var1, co.uk.getmondo.payments.send.data.a.a var2, Map var3) {
      f var4 = (f)var1;
      f var5 = (f)var2;
      var4.b(var5.e());
      var4.c(var5.f());
      var4.d(var5.g());
      return var1;
   }

   public static co.uk.getmondo.payments.send.data.a.a a(av var0, co.uk.getmondo.payments.send.data.a.a var1, boolean var2, Map var3) {
      if(var1 instanceof io.realm.internal.l && ((io.realm.internal.l)var1).s_().a() != null && ((io.realm.internal.l)var1).s_().a().c != var0.c) {
         throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
      } else {
         if(!(var1 instanceof io.realm.internal.l) || ((io.realm.internal.l)var1).s_().a() == null || !((io.realm.internal.l)var1).s_().a().g().equals(var0.g())) {
            g.b var8 = (g.b)g.g.get();
            io.realm.internal.l var7 = (io.realm.internal.l)var3.get(var1);
            if(var7 != null) {
               var1 = (co.uk.getmondo.payments.send.data.a.a)var7;
            } else {
               boolean var4;
               e var13;
               if(var2) {
                  Table var12 = var0.c(co.uk.getmondo.payments.send.data.a.a.class);
                  long var5 = var12.d();
                  String var9 = ((f)var1).d();
                  if(var9 == null) {
                     var5 = var12.k(var5);
                  } else {
                     var5 = var12.a(var5, var9);
                  }

                  if(var5 != -1L) {
                     try {
                        var8.a(var0, var12.f(var5), var0.f.c(co.uk.getmondo.payments.send.data.a.a.class), false, Collections.emptyList());
                        var13 = new e();
                        var3.put(var1, (io.realm.internal.l)var13);
                     } finally {
                        var8.f();
                     }

                     var4 = var2;
                  } else {
                     var4 = false;
                     var13 = null;
                  }
               } else {
                  var4 = var2;
                  var13 = null;
               }

               if(var4) {
                  var1 = a(var0, var13, var1, var3);
               } else {
                  var1 = b(var0, var1, var2, var3);
               }
            }
         }

         return var1;
      }
   }

   public static e.a a(SharedRealm var0, boolean var1) {
      if(!var0.a("class_BankDetails")) {
         throw new RealmMigrationNeededException(var0.h(), "The 'BankDetails' class is missing from the schema for this Realm.");
      } else {
         Table var7 = var0.b("class_BankDetails");
         long var4 = var7.c();
         if(var4 != 4L) {
            if(var4 < 4L) {
               throw new RealmMigrationNeededException(var0.h(), "Field count is less than expected - expected 4 but was " + var4);
            }

            if(!var1) {
               throw new RealmMigrationNeededException(var0.h(), "Field count is more than expected - expected 4 but was " + var4);
            }

            RealmLog.a("Field count is more than expected - expected 4 but was %1$d", new Object[]{Long.valueOf(var4)});
         }

         HashMap var8 = new HashMap();

         for(long var2 = 0L; var2 < var4; ++var2) {
            var8.put(var7.b(var2), var7.c(var2));
         }

         e.a var6 = new e.a(var0, var7);
         if(!var7.e()) {
            throw new RealmMigrationNeededException(var0.h(), "Primary key not defined for field 'id' in existing Realm file. @PrimaryKey was added.");
         } else if(var7.d() != var6.a) {
            throw new RealmMigrationNeededException(var0.h(), "Primary Key annotation definition was changed, from field " + var7.b(var7.d()) + " to field id");
         } else if(!var8.containsKey("id")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'id' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var8.get("id") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'id' in existing Realm file.");
         } else if(!var7.a(var6.a)) {
            throw new RealmMigrationNeededException(var0.h(), "@PrimaryKey field 'id' does not support null values in the existing Realm file. Migrate using RealmObjectSchema.setNullable(), or mark the field as @Required.");
         } else if(!var7.j(var7.a("id"))) {
            throw new RealmMigrationNeededException(var0.h(), "Index not defined for field 'id' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex().");
         } else if(!var8.containsKey("name")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'name' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var8.get("name") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'name' in existing Realm file.");
         } else if(!var7.a(var6.b)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'name' is required. Either set @Required to field 'name' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var8.containsKey("sortCode")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'sortCode' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var8.get("sortCode") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'sortCode' in existing Realm file.");
         } else if(!var7.a(var6.c)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'sortCode' is required. Either set @Required to field 'sortCode' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var8.containsKey("accountNumber")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'accountNumber' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var8.get("accountNumber") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'accountNumber' in existing Realm file.");
         } else if(!var7.a(var6.d)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'accountNumber' is required. Either set @Required to field 'accountNumber' or migrate using RealmObjectSchema.setNullable().");
         } else {
            return var6;
         }
      }
   }

   public static void a(av var0, Iterator var1, Map var2) {
      Table var12 = var0.c(co.uk.getmondo.payments.send.data.a.a.class);
      long var7 = var12.getNativePtr();
      e.a var11 = (e.a)var0.f.c(co.uk.getmondo.payments.send.data.a.a.class);
      long var9 = var12.d();

      while(true) {
         while(true) {
            co.uk.getmondo.payments.send.data.a.a var13;
            do {
               if(!var1.hasNext()) {
                  return;
               }

               var13 = (co.uk.getmondo.payments.send.data.a.a)var1.next();
            } while(var2.containsKey(var13));

            if(var13 instanceof io.realm.internal.l && ((io.realm.internal.l)var13).s_().a() != null && ((io.realm.internal.l)var13).s_().a().g().equals(var0.g())) {
               var2.put(var13, Long.valueOf(((io.realm.internal.l)var13).s_().b().c()));
            } else {
               String var14 = ((f)var13).d();
               long var3;
               if(var14 == null) {
                  var3 = Table.nativeFindFirstNull(var7, var9);
               } else {
                  var3 = Table.nativeFindFirstString(var7, var9, var14);
               }

               long var5 = var3;
               if(var3 == -1L) {
                  var5 = OsObject.b(var12, var14);
               }

               var2.put(var13, Long.valueOf(var5));
               var14 = ((f)var13).e();
               if(var14 != null) {
                  Table.nativeSetString(var7, var11.b, var5, var14, false);
               } else {
                  Table.nativeSetNull(var7, var11.b, var5, false);
               }

               var14 = ((f)var13).f();
               if(var14 != null) {
                  Table.nativeSetString(var7, var11.c, var5, var14, false);
               } else {
                  Table.nativeSetNull(var7, var11.c, var5, false);
               }

               String var15 = ((f)var13).g();
               if(var15 != null) {
                  Table.nativeSetString(var7, var11.d, var5, var15, false);
               } else {
                  Table.nativeSetNull(var7, var11.d, var5, false);
               }
            }
         }
      }
   }

   public static long b(av var0, co.uk.getmondo.payments.send.data.a.a var1, Map var2) {
      long var3;
      if(var1 instanceof io.realm.internal.l && ((io.realm.internal.l)var1).s_().a() != null && ((io.realm.internal.l)var1).s_().a().g().equals(var0.g())) {
         var3 = ((io.realm.internal.l)var1).s_().b().c();
      } else {
         Table var9 = var0.c(co.uk.getmondo.payments.send.data.a.a.class);
         long var7 = var9.getNativePtr();
         e.a var11 = (e.a)var0.f.c(co.uk.getmondo.payments.send.data.a.a.class);
         var3 = var9.d();
         String var10 = ((f)var1).d();
         long var5;
         if(var10 == null) {
            var5 = Table.nativeFindFirstNull(var7, var3);
         } else {
            var5 = Table.nativeFindFirstString(var7, var3, var10);
         }

         var3 = var5;
         if(var5 == -1L) {
            var3 = OsObject.b(var9, var10);
         }

         var2.put(var1, Long.valueOf(var3));
         String var13 = ((f)var1).e();
         if(var13 != null) {
            Table.nativeSetString(var7, var11.b, var3, var13, false);
         } else {
            Table.nativeSetNull(var7, var11.b, var3, false);
         }

         var13 = ((f)var1).f();
         if(var13 != null) {
            Table.nativeSetString(var7, var11.c, var3, var13, false);
         } else {
            Table.nativeSetNull(var7, var11.c, var3, false);
         }

         String var12 = ((f)var1).g();
         if(var12 != null) {
            Table.nativeSetString(var7, var11.d, var3, var12, false);
         } else {
            Table.nativeSetNull(var7, var11.d, var3, false);
         }
      }

      return var3;
   }

   public static co.uk.getmondo.payments.send.data.a.a b(av var0, co.uk.getmondo.payments.send.data.a.a var1, boolean var2, Map var3) {
      io.realm.internal.l var4 = (io.realm.internal.l)var3.get(var1);
      co.uk.getmondo.payments.send.data.a.a var5;
      if(var4 != null) {
         var5 = (co.uk.getmondo.payments.send.data.a.a)var4;
      } else {
         var5 = (co.uk.getmondo.payments.send.data.a.a)var0.a(co.uk.getmondo.payments.send.data.a.a.class, ((f)var1).d(), false, Collections.emptyList());
         var3.put(var1, (io.realm.internal.l)var5);
         f var6 = (f)var1;
         f var7 = (f)var5;
         var7.b(var6.e());
         var7.c(var6.f());
         var7.d(var6.g());
      }

      return var5;
   }

   public static OsObjectSchemaInfo h() {
      return c;
   }

   public static String i() {
      return "class_BankDetails";
   }

   private static OsObjectSchemaInfo j() {
      OsObjectSchemaInfo.a var0 = new OsObjectSchemaInfo.a("BankDetails");
      var0.a("id", RealmFieldType.STRING, true, true, false);
      var0.a("name", RealmFieldType.STRING, false, false, false);
      var0.a("sortCode", RealmFieldType.STRING, false, false, false);
      var0.a("accountNumber", RealmFieldType.STRING, false, false, false);
      return var0.a();
   }

   public void a(String var1) {
      if(!this.b.e()) {
         this.b.a().e();
         throw new RealmException("Primary key field 'id' cannot be changed after object was created.");
      }
   }

   public void b(String var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            if(var1 == null) {
               var2.b().a(this.a.b, var2.c(), true);
            } else {
               var2.b().a(this.a.b, var2.c(), var1, true);
            }
         }
      } else {
         this.b.a().e();
         if(var1 == null) {
            this.b.b().c(this.a.b);
         } else {
            this.b.b().a(this.a.b, var1);
         }
      }

   }

   public void c(String var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            if(var1 == null) {
               var2.b().a(this.a.c, var2.c(), true);
            } else {
               var2.b().a(this.a.c, var2.c(), var1, true);
            }
         }
      } else {
         this.b.a().e();
         if(var1 == null) {
            this.b.b().c(this.a.c);
         } else {
            this.b.b().a(this.a.c, var1);
         }
      }

   }

   public String d() {
      this.b.a().e();
      return this.b.b().k(this.a.a);
   }

   public void d(String var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            if(var1 == null) {
               var2.b().a(this.a.d, var2.c(), true);
            } else {
               var2.b().a(this.a.d, var2.c(), var1, true);
            }
         }
      } else {
         this.b.a().e();
         if(var1 == null) {
            this.b.b().c(this.a.d);
         } else {
            this.b.b().a(this.a.d, var1);
         }
      }

   }

   public String e() {
      this.b.a().e();
      return this.b.b().k(this.a.b);
   }

   public String f() {
      this.b.a().e();
      return this.b.b().k(this.a.c);
   }

   public String g() {
      this.b.a().e();
      return this.b.b().k(this.a.d);
   }

   public au s_() {
      return this.b;
   }

   public void u_() {
      if(this.b == null) {
         g.b var1 = (g.b)g.g.get();
         this.a = (e.a)var1.c();
         this.b = new au(this);
         this.b.a(var1.a());
         this.b.a(var1.b());
         this.b.a(var1.d());
         this.b.a(var1.e());
      }

   }

   static final class a extends io.realm.internal.c {
      long a;
      long b;
      long c;
      long d;

      a(SharedRealm var1, Table var2) {
         super(4);
         this.a = this.a(var2, "id", RealmFieldType.STRING);
         this.b = this.a(var2, "name", RealmFieldType.STRING);
         this.c = this.a(var2, "sortCode", RealmFieldType.STRING);
         this.d = this.a(var2, "accountNumber", RealmFieldType.STRING);
      }

      a(io.realm.internal.c var1, boolean var2) {
         super(var1, var2);
         this.a(var1, this);
      }

      protected final io.realm.internal.c a(boolean var1) {
         return new e.a(this, var1);
      }

      protected final void a(io.realm.internal.c var1, io.realm.internal.c var2) {
         e.a var3 = (e.a)var1;
         e.a var4 = (e.a)var2;
         var4.a = var3.a;
         var4.b = var3.b;
         var4.c = var3.c;
         var4.d = var3.d;
      }
   }
}
