package io.realm;

import io.realm.internal.Keep;

@Keep
public interface CompactOnLaunchCallback {
   boolean shouldCompact(long var1, long var3);
}
