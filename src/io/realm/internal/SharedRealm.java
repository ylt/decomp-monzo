package io.realm.internal;

import io.realm.CompactOnLaunchCallback;
import io.realm.ay;
import io.realm.internal.android.AndroidRealmNotifier;
import java.io.Closeable;
import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public final class SharedRealm implements g, Closeable {
   private static final long f = nativeGetFinalizerPtr();
   private static volatile File g;
   public final List a = new CopyOnWriteArrayList();
   public final List b = new ArrayList();
   public final RealmNotifier c;
   public final a d;
   final f e;
   private final List h = new CopyOnWriteArrayList();
   private final SharedRealm.c i;
   private final ay j;
   private final long k;
   private long l;

   private SharedRealm(long var1, ay var3, SharedRealm.c var4) {
      io.realm.internal.android.a var5 = new io.realm.internal.android.a();
      AndroidRealmNotifier var6 = new AndroidRealmNotifier(this, var5);
      this.k = nativeGetSharedRealm(var1, var6);
      this.j = var3;
      this.d = var5;
      this.c = var6;
      this.i = var4;
      this.e = new f();
      this.e.a(this);
      if(var4 == null) {
         var1 = -1L;
      } else {
         var1 = this.e();
      }

      this.l = var1;
      nativeSetAutoRefresh(this.k, var5.a());
   }

   public static SharedRealm a(ay var0) {
      return a(var0, (SharedRealm.c)null, false);
   }

   public static SharedRealm a(ay var0, SharedRealm.c var1, boolean var2) {
      Object[] var12 = h.a().b(var0);
      String var9 = (String)var12[0];
      String var8 = (String)var12[1];
      String var11 = (String)var12[2];
      String var10 = (String)var12[3];
      boolean var5 = Boolean.TRUE.equals(var12[4]);
      String var13 = (String)var12[5];
      String var18 = var0.m();
      byte[] var14 = var0.c();
      byte var3;
      if(var8 != null) {
         var3 = SharedRealm.b.d.a();
      } else {
         var3 = SharedRealm.b.e.a();
      }

      boolean var4;
      if(var0.g() == SharedRealm.a.b) {
         var4 = true;
      } else {
         var4 = false;
      }

      long var6 = nativeCreateConfig(var18, var14, var3, var4, false, var0.d(), true, var2, var0.l(), var8, var11, var9, var10, var5, var13);

      SharedRealm var17;
      try {
         h.a().c(var0);
         var17 = new SharedRealm(var6, var0, var1);
      } finally {
         nativeCloseConfig(var6);
      }

      return var17;
   }

   public static void a(File var0) {
      if(g == null) {
         if(var0 == null) {
            throw new IllegalArgumentException("'tempDirectory' must not be null.");
         }

         String var2 = var0.getAbsolutePath();
         if(!var0.isDirectory() && !var0.mkdirs() && !var0.isDirectory()) {
            throw new IOException("failed to create temporary directory: " + var2);
         }

         String var1 = var2;
         if(!var2.endsWith("/")) {
            var1 = var2 + "/";
         }

         nativeInit(var1);
         g = var0;
      }

   }

   private void m() {
      Iterator var2 = this.h.iterator();

      while(var2.hasNext()) {
         j var1 = (j)((WeakReference)var2.next()).get();
         if(var1 != null) {
            var1.e();
         }
      }

      this.h.clear();
   }

   private static native void nativeBeginTransaction(long var0);

   private static native void nativeCancelTransaction(long var0);

   private static native void nativeCloseConfig(long var0);

   private static native void nativeCloseSharedRealm(long var0);

   private static native void nativeCommitTransaction(long var0);

   private static native long nativeCreateConfig(String var0, byte[] var1, byte var2, boolean var3, boolean var4, long var5, boolean var7, boolean var8, CompactOnLaunchCallback var9, String var10, String var11, String var12, String var13, boolean var14, String var15);

   private static native long nativeCreateTable(long var0, String var2);

   private static native long nativeGetFinalizerPtr();

   private static native long nativeGetSharedRealm(long var0, RealmNotifier var2);

   private static native long nativeGetTable(long var0, String var2);

   private static native String nativeGetTableName(long var0, int var2);

   private static native long nativeGetVersion(long var0);

   private static native boolean nativeHasTable(long var0, String var2);

   private static native void nativeInit(String var0);

   private static native boolean nativeIsClosed(long var0);

   private static native boolean nativeIsInTransaction(long var0);

   private static native long nativeReadGroup(long var0);

   private static native void nativeSetAutoRefresh(long var0, boolean var2);

   private static native void nativeSetVersion(long var0, long var2);

   private static native long nativeSize(long var0);

   private static native void nativeUpdateSchema(long var0, long var2, long var4);

   public String a(int var1) {
      return nativeGetTableName(this.k, var1);
   }

   public void a() {
      this.a(false);
   }

   public void a(long var1) {
      nativeSetVersion(this.k, var1);
   }

   void a(Collection.d var1) {
      this.b.add(new WeakReference(var1));
   }

   public void a(OsSchemaInfo var1, long var2) {
      nativeUpdateSchema(this.k, var1.getNativePtr(), var2);
   }

   void a(j var1) {
      Iterator var4 = this.h.iterator();

      while(true) {
         j var2;
         WeakReference var3;
         do {
            if(!var4.hasNext()) {
               return;
            }

            var3 = (WeakReference)var4.next();
            var2 = (j)var3.get();
         } while(var2 != null && var2 != var1);

         this.h.remove(var3);
      }
   }

   public void a(boolean var1) {
      if(!var1 && this.j.o()) {
         throw new IllegalStateException("Write transactions cannot be used when a Realm is marked as read-only.");
      } else {
         this.k();
         this.m();
         nativeBeginTransaction(this.k);
         this.j();
      }
   }

   public boolean a(String var1) {
      return nativeHasTable(this.k, var1);
   }

   public Table b(String var1) {
      return new Table(this, nativeGetTable(this.k, var1));
   }

   public void b() {
      nativeCommitTransaction(this.k);
   }

   public Table c(String var1) {
      return new Table(this, nativeCreateTable(this.k, var1));
   }

   public void c() {
      nativeCancelTransaction(this.k);
   }

   public void close() {
      // $FF: Couldn't be decompiled
   }

   public boolean d() {
      return nativeIsInTransaction(this.k);
   }

   public long e() {
      return nativeGetVersion(this.k);
   }

   long f() {
      return nativeReadGroup(this.k);
   }

   public long g() {
      return nativeSize(this.k);
   }

   public long getNativeFinalizerPtr() {
      return f;
   }

   public long getNativePtr() {
      return this.k;
   }

   public String h() {
      return this.j.m();
   }

   public boolean i() {
      return nativeIsClosed(this.k);
   }

   public void j() {
      if(this.i != null) {
         long var1 = this.l;
         long var3 = this.e();
         if(var3 != var1) {
            this.l = var3;
            this.i.a(var3);
         }
      }

   }

   void k() {
      Iterator var2 = this.b.iterator();

      while(var2.hasNext()) {
         Collection.d var1 = (Collection.d)((WeakReference)var2.next()).get();
         if(var1 != null) {
            var1.a();
         }
      }

      this.b.clear();
   }

   void l() {
      Iterator var2 = this.b.iterator();

      while(var2.hasNext()) {
         Collection.d var1 = (Collection.d)((WeakReference)var2.next()).get();
         if(var1 != null) {
            var1.b();
         }
      }

      this.b.clear();
   }

   public static enum a {
      a(0),
      b(1);

      final int c;

      private a(int var3) {
         this.c = var3;
      }
   }

   private static enum b {
      a(0),
      b(1),
      c(2),
      d(3),
      e(4);

      final byte f;

      private b(byte var3) {
         this.f = var3;
      }

      public byte a() {
         return this.f;
      }
   }

   public interface c {
      void a(long var1);
   }
}
