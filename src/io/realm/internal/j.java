package io.realm.internal;

import io.realm.RealmFieldType;
import io.realm.ax;
import java.lang.ref.WeakReference;
import java.util.Date;

public class j implements n {
   private SharedRealm a;
   private Collection b;
   private ax c;
   private WeakReference d;
   private boolean e;

   private void f() {
      this.b.removeListener(this, (ax)this.c);
      this.b = null;
      this.c = null;
      this.a.a(this);
   }

   private void g() {
      if(this.d == null) {
         throw new IllegalStateException("The 'frontEnd' has not been set.");
      } else {
         j.a var3 = (j.a)this.d.get();
         if(var3 == null) {
            this.f();
         } else if(this.b.isValid()) {
            UncheckedRow var2 = this.b.firstUncheckedRow();
            this.f();
            if(var2 != null) {
               Object var1 = var2;
               if(this.e) {
                  var1 = CheckedRow.a(var2);
               }

               var3.b((n)var1);
            } else {
               var3.b(e.a);
            }
         } else {
            this.f();
         }

      }
   }

   public long a() {
      throw new IllegalStateException("The pending query has not been executed.");
   }

   public long a(String var1) {
      throw new IllegalStateException("The pending query has not been executed.");
   }

   public void a(long var1, double var3) {
      throw new IllegalStateException("The pending query has not been executed.");
   }

   public void a(long var1, long var3) {
      throw new IllegalStateException("The pending query has not been executed.");
   }

   public void a(long var1, String var3) {
      throw new IllegalStateException("The pending query has not been executed.");
   }

   public void a(long var1, Date var3) {
      throw new IllegalStateException("The pending query has not been executed.");
   }

   public void a(long var1, boolean var3) {
      throw new IllegalStateException("The pending query has not been executed.");
   }

   public boolean a(long var1) {
      throw new IllegalStateException("The pending query has not been executed.");
   }

   public Table b() {
      throw new IllegalStateException("The pending query has not been executed.");
   }

   public void b(long var1, long var3) {
      throw new IllegalStateException("The pending query has not been executed.");
   }

   public boolean b(long var1) {
      throw new IllegalStateException("The pending query has not been executed.");
   }

   public long c() {
      throw new IllegalStateException("The pending query has not been executed.");
   }

   public void c(long var1) {
      throw new IllegalStateException("The pending query has not been executed.");
   }

   public String d(long var1) {
      throw new IllegalStateException("The pending query has not been executed.");
   }

   public boolean d() {
      return false;
   }

   public RealmFieldType e(long var1) {
      throw new IllegalStateException("The pending query has not been executed.");
   }

   public void e() {
      if(this.b == null) {
         throw new IllegalStateException("The query has been executed. This 'PendingRow' is not valid anymore.");
      } else {
         this.g();
      }
   }

   public long f(long var1) {
      throw new IllegalStateException("The pending query has not been executed.");
   }

   public boolean g(long var1) {
      throw new IllegalStateException("The pending query has not been executed.");
   }

   public float h(long var1) {
      throw new IllegalStateException("The pending query has not been executed.");
   }

   public double i(long var1) {
      throw new IllegalStateException("The pending query has not been executed.");
   }

   public Date j(long var1) {
      throw new IllegalStateException("The pending query has not been executed.");
   }

   public String k(long var1) {
      throw new IllegalStateException("The pending query has not been executed.");
   }

   public byte[] l(long var1) {
      throw new IllegalStateException("The pending query has not been executed.");
   }

   public long m(long var1) {
      throw new IllegalStateException("The pending query has not been executed.");
   }

   public LinkView n(long var1) {
      throw new IllegalStateException("The pending query has not been executed.");
   }

   public void o(long var1) {
      throw new IllegalStateException("The pending query has not been executed.");
   }

   public interface a {
      void b(n var1);
   }
}
