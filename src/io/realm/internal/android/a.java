package io.realm.internal.android;

import android.os.Looper;

public class a implements io.realm.internal.a {
   private final Looper a = Looper.myLooper();
   private final boolean b = c();

   private boolean b() {
      boolean var1;
      if(this.a != null) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   private static boolean c() {
      String var1 = Thread.currentThread().getName();
      boolean var0;
      if(var1 != null && var1.startsWith("IntentService[")) {
         var0 = true;
      } else {
         var0 = false;
      }

      return var0;
   }

   public void a(String var1) {
      if(!this.b()) {
         if(var1 == null) {
            var1 = "";
         } else {
            var1 = var1 + " " + "Realm cannot be automatically updated on a thread without a looper.";
         }

         throw new IllegalStateException(var1);
      } else if(this.b) {
         if(var1 == null) {
            var1 = "";
         } else {
            var1 = var1 + " " + "Realm cannot be automatically updated on an IntentService thread.";
         }

         throw new IllegalStateException(var1);
      }
   }

   public boolean a() {
      boolean var1;
      if(this.b() && !this.b) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }
}
