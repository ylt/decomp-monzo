package io.realm.internal.android;

import android.os.Handler;
import android.os.Looper;
import io.realm.internal.Keep;
import io.realm.internal.RealmNotifier;
import io.realm.internal.SharedRealm;

@Keep
public class AndroidRealmNotifier extends RealmNotifier {
   private Handler handler;

   public AndroidRealmNotifier(SharedRealm var1, io.realm.internal.a var2) {
      super(var1);
      if(var2.a()) {
         this.handler = new Handler(Looper.myLooper());
      } else {
         this.handler = null;
      }

   }

   public boolean post(Runnable var1) {
      boolean var2;
      if(this.handler != null && this.handler.post(var1)) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }
}
