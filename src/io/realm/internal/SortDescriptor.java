package io.realm.internal;

import io.realm.RealmFieldType;
import io.realm.bl;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

@KeepMember
public class SortDescriptor {
   static final Set a;
   static final Set b;
   private final Table c;
   private final long[][] d;
   private final boolean[] e;

   static {
      a = Collections.unmodifiableSet(new HashSet(Arrays.asList(new RealmFieldType[]{RealmFieldType.BOOLEAN, RealmFieldType.INTEGER, RealmFieldType.FLOAT, RealmFieldType.DOUBLE, RealmFieldType.STRING, RealmFieldType.DATE})));
      b = Collections.unmodifiableSet(new HashSet(Arrays.asList(new RealmFieldType[]{RealmFieldType.BOOLEAN, RealmFieldType.INTEGER, RealmFieldType.STRING, RealmFieldType.DATE})));
   }

   private SortDescriptor(Table var1, long[][] var2, bl[] var3) {
      this.c = var1;
      this.d = var2;
      if(var3 != null) {
         this.e = new boolean[var3.length];

         for(int var4 = 0; var4 < var3.length; ++var4) {
            this.e[var4] = var3[var4].a();
         }
      } else {
         this.e = null;
      }

   }

   public static SortDescriptor a(io.realm.internal.a.c.a var0, Table var1, String var2, bl var3) {
      return a(var0, var1, new String[]{var2}, new bl[]{var3});
   }

   public static SortDescriptor a(io.realm.internal.a.c.a var0, Table var1, String[] var2, bl[] var3) {
      if(var3 != null && var3.length != 0) {
         if(var2.length != var3.length) {
            throw new IllegalArgumentException("Number of fields and sort orders do not match.");
         } else {
            return a(var0, var1, var2, var3, io.realm.internal.a.c.d, a, "Sort is not supported");
         }
      } else {
         throw new IllegalArgumentException("You must provide at least one sort order.");
      }
   }

   private static SortDescriptor a(io.realm.internal.a.c.a var0, Table var1, String[] var2, bl[] var3, Set var4, Set var5, String var6) {
      if(var2 != null && var2.length != 0) {
         long[][] var8 = new long[var2.length][];

         for(int var7 = 0; var7 < var2.length; ++var7) {
            io.realm.internal.a.c var9 = io.realm.internal.a.c.a((io.realm.internal.a.c.a)var0, (Table)var1, (String)var2[var7], (Set)var4, (Set)null);
            a(var9, var5, var6, var2[var7]);
            var8[var7] = var9.b();
         }

         return new SortDescriptor(var1, var8, var3);
      } else {
         throw new IllegalArgumentException("You must provide at least one field name.");
      }
   }

   private static void a(io.realm.internal.a.c var0, Set var1, String var2, String var3) {
      if(!var1.contains(var0.e())) {
         throw new IllegalArgumentException(String.format(Locale.US, "%s on '%s' field '%s' in '%s'.", new Object[]{var2, var0.e(), var0.d(), var3}));
      }
   }

   @KeepMember
   private long getTablePtr() {
      return this.c.getNativePtr();
   }

   @KeepMember
   boolean[] getAscendings() {
      return this.e;
   }

   @KeepMember
   long[][] getColumnIndices() {
      return this.d;
   }
}
