package io.realm.internal;

import java.lang.ref.PhantomReference;
import java.lang.ref.ReferenceQueue;

final class NativeObjectReference extends PhantomReference {
   private static NativeObjectReference.a f = new NativeObjectReference.a();
   private final long a;
   private final long b;
   private final f c;
   private NativeObjectReference d;
   private NativeObjectReference e;

   NativeObjectReference(f var1, g var2, ReferenceQueue var3) {
      super(var2, var3);
      this.a = var2.getNativePtr();
      this.b = var2.getNativeFinalizerPtr();
      this.c = var1;
      f.a(this);
   }

   // $FF: synthetic method
   static NativeObjectReference a(NativeObjectReference var0) {
      return var0.e;
   }

   // $FF: synthetic method
   static NativeObjectReference b(NativeObjectReference var0) {
      return var0.d;
   }

   private static native void nativeCleanUp(long var0, long var2);

   void a() {
      // $FF: Couldn't be decompiled
   }

   private static class a {
      NativeObjectReference a;

      private a() {
      }

      // $FF: synthetic method
      a(Object var1) {
         this();
      }

      void a(NativeObjectReference var1) {
         synchronized(this){}

         try {
            var1.d = null;
            var1.e = this.a;
            if(this.a != null) {
               this.a.d = var1;
            }

            this.a = var1;
         } finally {
            ;
         }

      }

      void b(NativeObjectReference param1) {
         // $FF: Couldn't be decompiled
      }
   }
}
