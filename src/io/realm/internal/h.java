package io.realm.internal;

import android.content.Context;
import io.realm.ay;
import io.realm.exceptions.RealmException;
import java.lang.reflect.InvocationTargetException;

public class h {
   private static final h a = new h();
   private static h b = null;

   static {
      try {
         b = (h)Class.forName("io.realm.internal.SyncObjectServerFacade").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
      } catch (ClassNotFoundException var1) {
         ;
      } catch (InstantiationException var2) {
         throw new RealmException("Failed to init SyncObjectServerFacade", var2);
      } catch (IllegalAccessException var3) {
         throw new RealmException("Failed to init SyncObjectServerFacade", var3);
      } catch (NoSuchMethodException var4) {
         throw new RealmException("Failed to init SyncObjectServerFacade", var4);
      } catch (InvocationTargetException var5) {
         throw new RealmException("Failed to init SyncObjectServerFacade", var5.getTargetException());
      }

   }

   public static h a() {
      h var0;
      if(b != null) {
         var0 = b;
      } else {
         var0 = a;
      }

      return var0;
   }

   public static h a(boolean var0) {
      h var1;
      if(var0) {
         var1 = b;
      } else {
         var1 = a;
      }

      return var1;
   }

   public void a(Context var1) {
   }

   public void a(ay var1) {
   }

   public Object[] b(ay var1) {
      return new Object[6];
   }

   public void c(ay var1) {
   }

   public String d(ay var1) {
      return null;
   }

   public String e(ay var1) {
      return null;
   }

   public void f(ay var1) {
   }
}
