package io.realm.internal;

import io.realm.bc;
import io.realm.log.RealmLog;
import java.io.File;
import java.util.Arrays;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicBoolean;

public class Util {
   public static Class a(Class var0) {
      Class var2 = var0.getSuperclass();
      Class var1 = var0;
      if(!var2.equals(Object.class)) {
         var1 = var0;
         if(!var2.equals(bc.class)) {
            var1 = var2;
         }
      }

      return var1;
   }

   public static String a() {
      return nativeGetTablePrefix();
   }

   public static boolean a(String var0) {
      boolean var1;
      if(var0 != null && var0.length() != 0) {
         var1 = false;
      } else {
         var1 = true;
      }

      return var1;
   }

   public static boolean a(String var0, File var1, String var2) {
      boolean var7 = true;
      File var9 = new File(var1, var2 + ".management");
      File[] var10 = var9.listFiles();
      boolean var3;
      boolean var5;
      if(var10 != null) {
         int var6 = var10.length;
         int var4 = 0;
         var3 = true;

         while(true) {
            var5 = var3;
            if(var4 >= var6) {
               break;
            }

            File var8 = var10[var4];
            if(var3 && var8.delete()) {
               var3 = true;
            } else {
               var3 = false;
            }

            ++var4;
         }
      } else {
         var5 = true;
      }

      if(var5 && var9.delete()) {
         var3 = true;
      } else {
         var3 = false;
      }

      if(!var3 || !b(var0, var1, var2)) {
         var7 = false;
      }

      return var7;
   }

   private static boolean b(String var0, File var1, String var2) {
      AtomicBoolean var3 = new AtomicBoolean(true);
      Iterator var4 = Arrays.asList(new File[]{new File(var1, var2), new File(var1, var2 + ".lock"), new File(var1, var2 + ".log_a"), new File(var1, var2 + ".log_b"), new File(var1, var2 + ".log"), new File(var0)}).iterator();

      while(var4.hasNext()) {
         var1 = (File)var4.next();
         if(var1.exists() && !var1.delete()) {
            var3.set(false);
            RealmLog.b("Could not delete the file %s", new Object[]{var1});
         }
      }

      return var3.get();
   }

   static native String nativeGetTablePrefix();
}
