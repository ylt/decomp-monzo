package io.realm.internal.async;

import io.realm.internal.Keep;

@Keep
public class BadVersionException extends Exception {
   public BadVersionException(String var1) {
      super(var1);
   }

   public BadVersionException(String var1, Throwable var2) {
      super(var1, var2);
   }
}
