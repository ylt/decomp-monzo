package io.realm.internal;

import io.realm.RealmFieldType;
import java.util.Date;

public interface n {
   long a();

   long a(String var1);

   void a(long var1, double var3);

   void a(long var1, long var3);

   void a(long var1, String var3);

   void a(long var1, Date var3);

   void a(long var1, boolean var3);

   boolean a(long var1);

   Table b();

   void b(long var1, long var3);

   boolean b(long var1);

   long c();

   void c(long var1);

   String d(long var1);

   boolean d();

   RealmFieldType e(long var1);

   long f(long var1);

   boolean g(long var1);

   float h(long var1);

   double i(long var1);

   Date j(long var1);

   String k(long var1);

   byte[] l(long var1);

   long m(long var1);

   LinkView n(long var1);

   void o(long var1);
}
