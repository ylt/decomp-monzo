package io.realm.internal;

import java.util.Iterator;

public class OsSchemaInfo implements g {
   private static final long b = nativeGetFinalizerPtr();
   private long a;

   public OsSchemaInfo(java.util.Collection var1) {
      long[] var3 = new long[var1.size()];
      Iterator var4 = var1.iterator();

      for(int var2 = 0; var4.hasNext(); ++var2) {
         var3[var2] = ((OsObjectSchemaInfo)var4.next()).getNativePtr();
      }

      this.a = nativeCreateFromList(var3);
      f.a.a(this);
   }

   private static native long nativeCreateFromList(long[] var0);

   private static native long nativeGetFinalizerPtr();

   public long getNativeFinalizerPtr() {
      return b;
   }

   public long getNativePtr() {
      return this.a;
   }
}
