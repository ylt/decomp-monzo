package io.realm.internal;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public final class b {
   private final Map a;
   private final Map b;
   private final Map c;
   private final boolean d;
   private long e;

   public b(long var1, Map var3) {
      this(var1, new HashMap(var3), true);
      Iterator var7 = var3.entrySet().iterator();

      while(var7.hasNext()) {
         Entry var5 = (Entry)var7.next();
         c var4 = (c)var5.getValue();
         if(this.d != var4.a()) {
            throw new IllegalArgumentException("ColumnInfo mutability does not match ColumnIndices");
         }

         io.realm.internal.c.a var6 = (io.realm.internal.c.a)var5.getKey();
         this.b.put(var6.a, var4);
         this.c.put(var6.b, var4);
      }

   }

   private b(long var1, Map var3, boolean var4) {
      this.e = var1;
      this.a = var3;
      this.d = var4;
      this.b = new HashMap(var3.size());
      this.c = new HashMap(var3.size());
   }

   public b(b var1, boolean var2) {
      this(var1.e, new HashMap(var1.a.size()), var2);
      Iterator var5 = var1.a.entrySet().iterator();

      while(var5.hasNext()) {
         Entry var4 = (Entry)var5.next();
         c var3 = ((c)var4.getValue()).a(var2);
         io.realm.internal.c.a var6 = (io.realm.internal.c.a)var4.getKey();
         this.b.put(var6.a, var3);
         this.c.put(var6.b, var3);
         this.a.put(var6, var3);
      }

   }

   public long a() {
      return this.e;
   }

   public c a(Class var1) {
      return (c)this.b.get(var1);
   }

   public c a(String var1) {
      return (c)this.c.get(var1);
   }

   public void a(b var1) {
      if(!this.d) {
         throw new UnsupportedOperationException("Attempt to modify immutable cache");
      } else {
         Iterator var4 = this.c.entrySet().iterator();

         while(var4.hasNext()) {
            Entry var2 = (Entry)var4.next();
            c var3 = (c)var1.c.get(var2.getKey());
            if(var3 == null) {
               throw new IllegalStateException("Failed to copy ColumnIndices cache for class: " + (String)var2.getKey());
            }

            ((c)var2.getValue()).a(var3);
         }

         this.e = var1.e;
      }
   }

   public String toString() {
      StringBuilder var4 = new StringBuilder("ColumnIndices[");
      var4.append(this.e).append(",");
      var4.append(this.d).append(",");
      if(this.b != null) {
         Iterator var3 = this.c.entrySet().iterator();

         for(boolean var1 = false; var3.hasNext(); var1 = true) {
            Entry var2 = (Entry)var3.next();
            if(var1) {
               var4.append(",");
            }

            var4.append((String)var2.getKey()).append("->").append(var2.getValue());
         }
      }

      return var4.append("]").toString();
   }
}
