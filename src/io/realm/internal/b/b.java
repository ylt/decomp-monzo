package io.realm.internal.b;

import io.realm.av;
import io.realm.bb;
import io.realm.internal.SharedRealm;
import io.realm.internal.Util;
import io.realm.internal.c;
import io.realm.internal.m;
import io.realm.internal.n;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

public class b extends m {
   private final m a;
   private final Set b;

   public b(m var1, Collection var2) {
      this.a = var1;
      HashSet var3 = new HashSet();
      if(var1 != null) {
         Set var5 = var1.b();
         Iterator var6 = var2.iterator();

         while(var6.hasNext()) {
            Class var4 = (Class)var6.next();
            if(var5.contains(var4)) {
               var3.add(var4);
            }
         }
      }

      this.b = Collections.unmodifiableSet(var3);
   }

   private void d(Class var1) {
      if(!this.b.contains(var1)) {
         throw new IllegalArgumentException(var1.getSimpleName() + " is not part of the schema for this Realm");
      }
   }

   public bb a(av var1, bb var2, boolean var3, Map var4) {
      this.d(Util.a(var2.getClass()));
      return this.a.a(var1, var2, var3, var4);
   }

   public bb a(bb var1, int var2, Map var3) {
      this.d(Util.a(var1.getClass()));
      return this.a.a(var1, var2, var3);
   }

   public bb a(Class var1, Object var2, n var3, c var4, boolean var5, List var6) {
      this.d(var1);
      return this.a.a(var1, var2, var3, var4, var5, var6);
   }

   public c a(Class var1, SharedRealm var2, boolean var3) {
      this.d(var1);
      return this.a.a(var1, var2, var3);
   }

   public String a(Class var1) {
      this.d(var1);
      return this.a.a(var1);
   }

   public Map a() {
      HashMap var2 = new HashMap();
      Iterator var3 = this.a.a().entrySet().iterator();

      while(var3.hasNext()) {
         Entry var1 = (Entry)var3.next();
         if(this.b.contains(var1.getKey())) {
            var2.put(var1.getKey(), var1.getValue());
         }
      }

      return var2;
   }

   public void a(av var1, bb var2, Map var3) {
      this.d(Util.a(var2.getClass()));
      this.a.a(var1, var2, var3);
   }

   public void a(av var1, Collection var2) {
      this.d(Util.a(((bb)var2.iterator().next()).getClass()));
      this.a.a(var1, var2);
   }

   public Set b() {
      return this.b;
   }

   public void b(av var1, bb var2, Map var3) {
      this.d(Util.a(var2.getClass()));
      this.a.b(var1, var2, var3);
   }

   public boolean c() {
      boolean var1;
      if(this.a == null) {
         var1 = true;
      } else {
         var1 = this.a.c();
      }

      return var1;
   }
}
