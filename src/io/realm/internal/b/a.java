package io.realm.internal.b;

import io.realm.av;
import io.realm.bb;
import io.realm.internal.SharedRealm;
import io.realm.internal.Util;
import io.realm.internal.c;
import io.realm.internal.m;
import io.realm.internal.n;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

public class a extends m {
   private final Map a;

   public a(m... var1) {
      HashMap var5 = new HashMap();
      if(var1 != null) {
         int var3 = var1.length;

         for(int var2 = 0; var2 < var3; ++var2) {
            m var6 = var1[var2];
            Iterator var4 = var6.b().iterator();

            while(var4.hasNext()) {
               var5.put((Class)var4.next(), var6);
            }
         }
      }

      this.a = Collections.unmodifiableMap(var5);
   }

   private m d(Class var1) {
      m var2 = (m)this.a.get(var1);
      if(var2 == null) {
         throw new IllegalArgumentException(var1.getSimpleName() + " is not part of the schema for this Realm");
      } else {
         return var2;
      }
   }

   public bb a(av var1, bb var2, boolean var3, Map var4) {
      return this.d(Util.a(var2.getClass())).a(var1, var2, var3, var4);
   }

   public bb a(bb var1, int var2, Map var3) {
      return this.d(Util.a(var1.getClass())).a(var1, var2, var3);
   }

   public bb a(Class var1, Object var2, n var3, c var4, boolean var5, List var6) {
      return this.d(var1).a(var1, var2, var3, var4, var5, var6);
   }

   public c a(Class var1, SharedRealm var2, boolean var3) {
      return this.d(var1).a(var1, var2, var3);
   }

   public String a(Class var1) {
      return this.d(var1).a(var1);
   }

   public Map a() {
      HashMap var1 = new HashMap();
      Iterator var2 = this.a.values().iterator();

      while(var2.hasNext()) {
         var1.putAll(((m)var2.next()).a());
      }

      return var1;
   }

   public void a(av var1, bb var2, Map var3) {
      this.d(Util.a(var2.getClass())).a(var1, var2, var3);
   }

   public void a(av var1, Collection var2) {
      this.d(Util.a(Util.a(((bb)var2.iterator().next()).getClass()))).a(var1, var2);
   }

   public Set b() {
      return this.a.keySet();
   }

   public void b(av var1, bb var2, Map var3) {
      this.d(Util.a(var2.getClass())).b(var1, var2, var3);
   }

   public boolean c() {
      Iterator var2 = this.a.entrySet().iterator();

      boolean var1;
      while(true) {
         if(var2.hasNext()) {
            if(((m)((Entry)var2.next()).getValue()).c()) {
               continue;
            }

            var1 = false;
            break;
         }

         var1 = true;
         break;
      }

      return var1;
   }
}
