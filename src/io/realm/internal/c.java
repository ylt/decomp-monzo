package io.realm.internal;

import io.realm.RealmFieldType;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public abstract class c {
   private final Map a;
   private final boolean b;

   protected c(int var1) {
      this(var1, true);
   }

   private c(int var1, boolean var2) {
      this.a = new HashMap(var1);
      this.b = var2;
   }

   protected c(c var1, boolean var2) {
      int var3;
      if(var1 == null) {
         var3 = 0;
      } else {
         var3 = var1.a.size();
      }

      this(var3, var2);
      if(var1 != null) {
         this.a.putAll(var1.a);
      }

   }

   protected final long a(Table var1, String var2, RealmFieldType var3) {
      long var4 = var1.a(var2);
      if(var4 >= 0L) {
         String var6;
         if(var3 != RealmFieldType.OBJECT && var3 != RealmFieldType.LIST) {
            var6 = null;
         } else {
            var6 = var1.e(var4).k();
         }

         this.a.put(var2, new c.a(var4, var3, var6));
      }

      return var4;
   }

   public long a(String var1) {
      c.a var4 = (c.a)this.a.get(var1);
      long var2;
      if(var4 == null) {
         var2 = -1L;
      } else {
         var2 = var4.a;
      }

      return var2;
   }

   protected abstract c a(boolean var1);

   public void a(c var1) {
      if(!this.b) {
         throw new UnsupportedOperationException("Attempt to modify an immutable ColumnInfo");
      } else if(var1 == null) {
         throw new NullPointerException("Attempt to copy null ColumnInfo");
      } else {
         this.a.clear();
         this.a.putAll(var1.a);
         this.a(var1, this);
      }
   }

   protected abstract void a(c var1, c var2);

   public final boolean a() {
      return this.b;
   }

   public RealmFieldType b(String var1) {
      c.a var2 = (c.a)this.a.get(var1);
      RealmFieldType var3;
      if(var2 == null) {
         var3 = RealmFieldType.UNSUPPORTED_TABLE;
      } else {
         var3 = var2.b;
      }

      return var3;
   }

   public String c(String var1) {
      c.a var2 = (c.a)this.a.get(var1);
      if(var2 == null) {
         var1 = null;
      } else {
         var1 = var2.c;
      }

      return var1;
   }

   public String toString() {
      StringBuilder var4 = new StringBuilder("ColumnInfo[");
      var4.append(this.b).append(",");
      if(this.a != null) {
         Iterator var2 = this.a.entrySet().iterator();

         for(boolean var1 = false; var2.hasNext(); var1 = true) {
            Entry var3 = (Entry)var2.next();
            if(var1) {
               var4.append(",");
            }

            var4.append((String)var3.getKey()).append("->").append(var3.getValue());
         }
      }

      return var4.append("]").toString();
   }

   private static final class a {
      public final long a;
      public final RealmFieldType b;
      public final String c;

      a(long var1, RealmFieldType var3, String var4) {
         this.a = var1;
         this.b = var3;
         this.c = var4;
      }

      public String toString() {
         StringBuilder var1 = new StringBuilder("ColumnDetails[");
         var1.append(this.a);
         var1.append(", ").append(this.b);
         var1.append(", ").append(this.c);
         return var1.append("]").toString();
      }
   }
}
