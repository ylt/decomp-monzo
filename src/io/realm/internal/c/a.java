package io.realm.internal.c;

public class a {
   public Object a;
   public Object b;

   public a(Object var1, Object var2) {
      this.a = var1;
      this.b = var2;
   }

   public static a a(Object var0, Object var1) {
      return new a(var0, var1);
   }

   private boolean b(Object var1, Object var2) {
      boolean var3;
      if(var1 != var2 && (var1 == null || !var1.equals(var2))) {
         var3 = false;
      } else {
         var3 = true;
      }

      return var3;
   }

   public boolean equals(Object var1) {
      boolean var3 = false;
      boolean var2;
      if(!(var1 instanceof a)) {
         var2 = var3;
      } else {
         a var4 = (a)var1;
         var2 = var3;
         if(this.b(var4.a, this.a)) {
            var2 = var3;
            if(this.b(var4.b, this.b)) {
               var2 = true;
            }
         }
      }

      return var2;
   }

   public int hashCode() {
      int var2 = 0;
      int var1;
      if(this.a == null) {
         var1 = 0;
      } else {
         var1 = this.a.hashCode();
      }

      if(this.b != null) {
         var2 = this.b.hashCode();
      }

      return var1 ^ var2;
   }

   public String toString() {
      return "Pair{" + String.valueOf(this.a) + " " + this.b + "}";
   }
}
