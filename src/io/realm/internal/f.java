package io.realm.internal;

import java.lang.ref.ReferenceQueue;

public class f {
   static final f a;
   private static final ReferenceQueue b = new ReferenceQueue();
   private static final Thread c;

   static {
      c = new Thread(new d(b));
      a = new f();
      c.setName("RealmFinalizingDaemon");
      c.start();
   }

   void a(g var1) {
      new NativeObjectReference(this, var1, b);
   }
}
