package io.realm.internal.a;

import io.realm.RealmFieldType;
import java.util.List;
import java.util.Locale;
import java.util.Set;

class a extends c {
   private final c.a f;
   private final String g;

   a(c.a var1, String var2, String var3, Set var4, Set var5) {
      super(var3, var4, var5);
      this.g = var2;
      this.f = var1;
   }

   protected void a(List var1) {
      int var3 = var1.size();
      long[] var11 = new long[var3];
      long[] var10 = new long[var3];
      String var6 = this.g;
      String var7 = null;
      RealmFieldType var8 = null;

      String var12;
      for(int var2 = 0; var2 < var3; var6 = var12) {
         var7 = (String)var1.get(var2);
         if(var7 == null || var7.length() <= 0) {
            throw new IllegalArgumentException("Invalid query: Field descriptor contains an empty field.  A field description may not begin with or contain adjacent periods ('.').");
         }

         io.realm.internal.c var9 = this.f.a(var6);
         if(var9 == null) {
            throw new IllegalArgumentException(String.format(Locale.US, "Invalid query: table '%s' not found in this schema.", new Object[]{var6}));
         }

         long var4 = var9.a(var7);
         if(var4 < 0L) {
            throw new IllegalArgumentException(String.format(Locale.US, "Invalid query: field '%s' not found in table '%s'.", new Object[]{var7, var6}));
         }

         var8 = var9.b(var7);
         if(var2 < var3 - 1) {
            this.a(var6, var7, var8);
         }

         var12 = var9.c(var7);
         var11[var2] = var4;
         if(var8 != RealmFieldType.LINKING_OBJECTS) {
            var4 = 0L;
         } else {
            var4 = this.f.b(var12);
         }

         var10[var2] = var4;
         ++var2;
         var8 = var8;
      }

      this.a(this.g, var7, var8, var11, var10);
   }
}
