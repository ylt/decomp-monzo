package io.realm.internal.a;

import io.realm.RealmFieldType;
import io.realm.internal.Table;
import java.util.List;
import java.util.Locale;
import java.util.Set;

class b extends c {
   private final Table f;

   b(Table var1, String var2, Set var3, Set var4) {
      super(var2, var3, var4);
      this.f = var1;
   }

   protected void a(List var1) {
      RealmFieldType var9 = null;
      int var3 = var1.size();
      long[] var10 = new long[var3];
      Table var6 = this.f;
      int var2 = 0;
      String var7 = null;

      String var8;
      for(var8 = null; var2 < var3; ++var2) {
         var7 = (String)var1.get(var2);
         if(var7 == null || var7.length() <= 0) {
            throw new IllegalArgumentException("Invalid query: Field descriptor contains an empty field.  A field description may not begin with or contain adjacent periods ('.').");
         }

         var8 = var6.k();
         long var4 = var6.a(var7);
         if(var4 < 0L) {
            throw new IllegalArgumentException(String.format(Locale.US, "Invalid query: field '%s' not found in table '%s'.", new Object[]{var7, var8}));
         }

         var9 = var6.c(var4);
         if(var2 < var3 - 1) {
            this.a(var8, var7, var9);
            var6 = var6.e(var4);
         }

         var10[var2] = var4;
      }

      this.a(var8, var7, var9, var10, new long[var3]);
   }
}
