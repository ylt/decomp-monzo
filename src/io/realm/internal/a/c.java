package io.realm.internal.a;

import io.realm.RealmFieldType;
import io.realm.internal.Table;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

public abstract class c {
   public static final Set a;
   public static final Set b;
   public static final Set c;
   public static final Set d;
   public static final Set e;
   private final List f;
   private final Set g;
   private final Set h;
   private String i;
   private RealmFieldType j;
   private long[] k;
   private long[] l;

   static {
      HashSet var0 = new HashSet(3);
      var0.add(RealmFieldType.OBJECT);
      var0.add(RealmFieldType.LIST);
      var0.add(RealmFieldType.LINKING_OBJECTS);
      a = Collections.unmodifiableSet(var0);
      var0 = new HashSet(2);
      var0.add(RealmFieldType.OBJECT);
      var0.add(RealmFieldType.LIST);
      b = Collections.unmodifiableSet(var0);
      var0 = new HashSet(1);
      var0.add(RealmFieldType.LIST);
      c = Collections.unmodifiableSet(var0);
      var0 = new HashSet(1);
      var0.add(RealmFieldType.OBJECT);
      d = Collections.unmodifiableSet(var0);
      e = Collections.emptySet();
   }

   protected c(String var1, Set var2, Set var3) {
      this.f = this.a(var1);
      if(this.f.size() <= 0) {
         throw new IllegalArgumentException("Invalid query: Empty field descriptor");
      } else {
         this.g = var2;
         this.h = var3;
      }
   }

   public static c a(c.a var0, Table var1, String var2, Set var3, Set var4) {
      Object var5;
      if(var0 != null && var0.a()) {
         String var6 = var1.k();
         if(var3 == null) {
            var3 = a;
         }

         var5 = new a(var0, var6, var2, var3, var4);
      } else {
         if(var3 == null) {
            var3 = b;
         }

         var5 = new b(var1, var2, var3, var4);
      }

      return (c)var5;
   }

   public static c a(c.a var0, Table var1, String var2, RealmFieldType... var3) {
      return a((c.a)var0, (Table)var1, (String)var2, (Set)null, (Set)(new HashSet(Arrays.asList(var3))));
   }

   private List a(String var1) {
      if(var1 != null && !var1.equals("")) {
         if(var1.endsWith(".")) {
            throw new IllegalArgumentException("Invalid query: field name must not end with a period ('.')");
         } else {
            return Arrays.asList(var1.split("\\."));
         }
      } else {
         throw new IllegalArgumentException("Invalid query: field name is empty");
      }
   }

   private void a(String var1, String var2, RealmFieldType var3, Set var4) {
      if(!var4.contains(var3)) {
         throw new IllegalArgumentException(String.format(Locale.US, "Invalid query: field '%s' in table '%s' is of invalid type '%s'.", new Object[]{var2, var1, var3.toString()}));
      }
   }

   private void f() {
      if(this.j == null) {
         this.a(this.f);
      }

   }

   public final int a() {
      return this.f.size();
   }

   protected final void a(String var1, String var2, RealmFieldType var3) {
      this.a(var1, var2, var3, this.g);
   }

   protected final void a(String var1, String var2, RealmFieldType var3, long[] var4, long[] var5) {
      if(this.h != null && this.h.size() > 0) {
         this.a(var1, var2, var3, this.h);
      }

      this.i = var2;
      this.j = var3;
      this.k = var4;
      this.l = var5;
   }

   protected abstract void a(List var1);

   public final long[] b() {
      this.f();
      return Arrays.copyOf(this.k, this.k.length);
   }

   public final long[] c() {
      this.f();
      return Arrays.copyOf(this.l, this.l.length);
   }

   public final String d() {
      this.f();
      return this.i;
   }

   public final RealmFieldType e() {
      this.f();
      return this.j;
   }

   public interface a {
      io.realm.internal.c a(String var1);

      boolean a();

      long b(String var1);
   }
}
