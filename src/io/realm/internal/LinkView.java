package io.realm.internal;

public class LinkView implements g {
   private static final long e = nativeGetFinalizerPtr();
   final Table a;
   final long b;
   private final f c;
   private final long d;

   public LinkView(f var1, Table var2, long var3, long var5) {
      this.c = var1;
      this.a = var2;
      this.b = var3;
      this.d = var5;
      var1.a(this);
   }

   private void f() {
      if(this.a.g()) {
         throw new IllegalStateException("Changing Realm data can only be done from inside a write transaction.");
      }
   }

   public static native void nativeAdd(long var0, long var2);

   public static native void nativeClear(long var0);

   private static native long nativeGetFinalizerPtr();

   private native long nativeGetTargetRowIndex(long var1, long var3);

   private native long nativeGetTargetTable(long var1);

   private native void nativeInsert(long var1, long var3, long var5);

   private native boolean nativeIsAttached(long var1);

   private native boolean nativeIsEmpty(long var1);

   private native void nativeRemove(long var1, long var3);

   private native void nativeSet(long var1, long var3, long var5);

   private native long nativeSize(long var1);

   public long a(long var1) {
      return this.nativeGetTargetRowIndex(this.d, var1);
   }

   public void a() {
      this.f();
      nativeClear(this.d);
   }

   public void a(long var1, long var3) {
      this.f();
      this.nativeInsert(this.d, var1, var3);
   }

   public long b() {
      return this.nativeSize(this.d);
   }

   public void b(long var1) {
      this.f();
      nativeAdd(this.d, var1);
   }

   public void b(long var1, long var3) {
      this.f();
      this.nativeSet(this.d, var1, var3);
   }

   public void c(long var1) {
      this.f();
      this.nativeRemove(this.d, var1);
   }

   public boolean c() {
      return this.nativeIsEmpty(this.d);
   }

   public boolean d() {
      return this.nativeIsAttached(this.d);
   }

   public Table e() {
      long var1 = this.nativeGetTargetTable(this.d);
      return new Table(this.a, var1);
   }

   public long getNativeFinalizerPtr() {
      return e;
   }

   public long getNativePtr() {
      return this.d;
   }
}
