package io.realm.internal;

import io.realm.ax;
import java.io.Closeable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Keep
public abstract class RealmNotifier implements Closeable {
   private final i.a onChangeCallBack = new i.a() {
      public void a(RealmNotifier.a var1, Object var2) {
         if(RealmNotifier.this.sharedRealm != null && !RealmNotifier.this.sharedRealm.i()) {
            var1.a(var2);
         }

      }
   };
   private i realmObserverPairs = new i();
   private SharedRealm sharedRealm;
   private List transactionCallbacks = new ArrayList();

   protected RealmNotifier(SharedRealm var1) {
      this.sharedRealm = var1;
   }

   private void removeAllChangeListeners() {
      this.realmObserverPairs.b();
   }

   public void addChangeListener(Object var1, ax var2) {
      RealmNotifier.a var3 = new RealmNotifier.a(var1, var2);
      this.realmObserverPairs.a((i.b)var3);
   }

   public void addTransactionCallback(Runnable var1) {
      this.transactionCallbacks.add(var1);
   }

   void beforeNotify() {
      this.sharedRealm.l();
   }

   public void close() {
      this.removeAllChangeListeners();
   }

   void didChange() {
      this.realmObserverPairs.a(this.onChangeCallBack);
      if(!this.transactionCallbacks.isEmpty()) {
         List var1 = this.transactionCallbacks;
         this.transactionCallbacks = new ArrayList();
         Iterator var2 = var1.iterator();

         while(var2.hasNext()) {
            ((Runnable)var2.next()).run();
         }
      }

   }

   public int getListenersListSize() {
      return this.realmObserverPairs.c();
   }

   public abstract boolean post(Runnable var1);

   public void removeChangeListener(Object var1, ax var2) {
      this.realmObserverPairs.a(var1, var2);
   }

   public void removeChangeListeners(Object var1) {
      this.realmObserverPairs.a(var1);
   }

   private static class a extends i.b {
      public a(Object var1, ax var2) {
         super(var1, var2);
      }

      private void a(Object var1) {
         if(var1 != null) {
            ((ax)this.b).a(var1);
         }

      }
   }
}
