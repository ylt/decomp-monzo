package io.realm.internal;

public class IOException extends RuntimeException {
   public IOException() {
   }

   public IOException(String var1) {
      super(var1);
   }
}
