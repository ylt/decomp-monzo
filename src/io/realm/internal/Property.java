package io.realm.internal;

import io.realm.RealmFieldType;

public class Property implements g {
   private static final long b = nativeGetFinalizerPtr();
   private long a;

   Property(String var1, RealmFieldType var2, String var3) {
      this.a = nativeCreateProperty(var1, var2.getNativeValue(), var3);
      f.a.a(this);
   }

   Property(String var1, RealmFieldType var2, boolean var3, boolean var4, boolean var5) {
      int var6 = var2.getNativeValue();
      if(!var5) {
         var5 = true;
      } else {
         var5 = false;
      }

      this.a = nativeCreateProperty(var1, var6, var3, var4, var5);
      f.a.a(this);
   }

   private static native long nativeCreateProperty(String var0, int var1, String var2);

   private static native long nativeCreateProperty(String var0, int var1, boolean var2, boolean var3, boolean var4);

   private static native long nativeGetFinalizerPtr();

   public long getNativeFinalizerPtr() {
      return b;
   }

   public long getNativePtr() {
      return this.a;
   }
}
