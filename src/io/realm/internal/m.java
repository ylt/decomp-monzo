package io.realm.internal;

import io.realm.av;
import io.realm.bb;
import io.realm.exceptions.RealmException;
import java.util.List;
import java.util.Map;
import java.util.Set;

public abstract class m {
   protected static void b(Class var0) {
      if(var0 == null) {
         throw new NullPointerException("A class extending RealmObject must be provided");
      }
   }

   protected static RealmException c(Class var0) {
      return new RealmException(var0 + " is not part of the schema for this Realm.");
   }

   public abstract bb a(av var1, bb var2, boolean var3, Map var4);

   public abstract bb a(bb var1, int var2, Map var3);

   public abstract bb a(Class var1, Object var2, n var3, c var4, boolean var5, List var6);

   public abstract c a(Class var1, SharedRealm var2, boolean var3);

   public abstract String a(Class var1);

   public abstract Map a();

   public abstract void a(av var1, bb var2, Map var3);

   public abstract void a(av var1, java.util.Collection var2);

   public abstract Set b();

   public abstract void b(av var1, bb var2, Map var3);

   public boolean c() {
      return false;
   }

   public boolean equals(Object var1) {
      boolean var2;
      if(!(var1 instanceof m)) {
         var2 = false;
      } else {
         m var3 = (m)var1;
         var2 = this.b().equals(var3.b());
      }

      return var2;
   }

   public int hashCode() {
      return this.b().hashCode();
   }
}
