package io.realm.internal;

import io.realm.RealmFieldType;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class OsObjectSchemaInfo implements g {
   private static final long b = nativeGetFinalizerPtr();
   private long a;

   private OsObjectSchemaInfo(long var1) {
      this.a = var1;
      f.a.a(this);
   }

   private OsObjectSchemaInfo(String var1) {
      this(nativeCreateRealmObjectSchema(var1));
   }

   // $FF: synthetic method
   OsObjectSchemaInfo(String var1, Object var2) {
      this(var1);
   }

   private static native void nativeAddProperty(long var0, long var2);

   private static native long nativeCreateRealmObjectSchema(String var0);

   private static native long nativeGetFinalizerPtr();

   public long getNativeFinalizerPtr() {
      return b;
   }

   public long getNativePtr() {
      return this.a;
   }

   public static class a {
      private String a;
      private List b = new ArrayList();

      public a(String var1) {
         this.a = var1;
      }

      public OsObjectSchemaInfo.a a(String var1, RealmFieldType var2, String var3) {
         Property var4 = new Property(var1, var2, var3);
         this.b.add(var4);
         return this;
      }

      public OsObjectSchemaInfo.a a(String var1, RealmFieldType var2, boolean var3, boolean var4, boolean var5) {
         Property var6 = new Property(var1, var2, var3, var4, var5);
         this.b.add(var6);
         return this;
      }

      public OsObjectSchemaInfo a() {
         OsObjectSchemaInfo var2 = new OsObjectSchemaInfo(this.a);
         Iterator var3 = this.b.iterator();

         while(var3.hasNext()) {
            Property var1 = (Property)var3.next();
            OsObjectSchemaInfo.nativeAddProperty(var2.a, var1.getNativePtr());
         }

         return var2;
      }
   }
}
