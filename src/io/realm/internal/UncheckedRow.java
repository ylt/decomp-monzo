package io.realm.internal;

import io.realm.RealmFieldType;
import java.util.Date;

public class UncheckedRow implements g, n {
   private static final long a = nativeGetFinalizerPtr();
   private final f b;
   private final Table c;
   private final long d;

   UncheckedRow(UncheckedRow var1) {
      this.b = var1.b;
      this.c = var1.c;
      this.d = var1.d;
   }

   UncheckedRow(f var1, Table var2, long var3) {
      this.b = var1;
      this.c = var2;
      this.d = var3;
      var1.a(this);
   }

   static UncheckedRow b(f var0, Table var1, long var2) {
      return new UncheckedRow(var0, var1, var1.nativeGetRowPtr(var1.getNativePtr(), var2));
   }

   static UncheckedRow c(f var0, Table var1, long var2) {
      return new UncheckedRow(var0, var1, var2);
   }

   private static native long nativeGetFinalizerPtr();

   public long a() {
      return this.nativeGetColumnCount(this.d);
   }

   public long a(String var1) {
      if(var1 == null) {
         throw new IllegalArgumentException("Column name can not be null.");
      } else {
         return this.nativeGetColumnIndex(this.d, var1);
      }
   }

   public void a(long var1, double var3) {
      this.c.h();
      this.nativeSetDouble(this.d, var1, var3);
   }

   public void a(long var1, long var3) {
      this.c.h();
      this.b().a(var1, this.c(), var3);
      this.nativeSetLong(this.d, var1, var3);
   }

   public void a(long var1, String var3) {
      this.c.h();
      if(var3 == null) {
         this.b().a(var1, this.c());
         this.nativeSetNull(this.d, var1);
      } else {
         this.b().a(var1, this.c(), var3);
         this.nativeSetString(this.d, var1, var3);
      }

   }

   public void a(long var1, Date var3) {
      this.c.h();
      if(var3 == null) {
         throw new IllegalArgumentException("Null Date is not allowed.");
      } else {
         long var4 = var3.getTime();
         this.nativeSetTimestamp(this.d, var1, var4);
      }
   }

   public void a(long var1, boolean var3) {
      this.c.h();
      this.nativeSetBoolean(this.d, var1, var3);
   }

   public void a(long var1, byte[] var3) {
      this.c.h();
      this.nativeSetByteArray(this.d, var1, var3);
   }

   public boolean a(long var1) {
      return this.nativeIsNullLink(this.d, var1);
   }

   public Table b() {
      return this.c;
   }

   public void b(long var1, long var3) {
      this.c.h();
      this.nativeSetLink(this.d, var1, var3);
   }

   public boolean b(long var1) {
      return this.nativeIsNull(this.d, var1);
   }

   public long c() {
      return this.nativeGetIndex(this.d);
   }

   public void c(long var1) {
      this.c.h();
      this.b().a(var1, this.c());
      this.nativeSetNull(this.d, var1);
   }

   public String d(long var1) {
      return this.nativeGetColumnName(this.d, var1);
   }

   public boolean d() {
      boolean var1;
      if(this.d != 0L && this.nativeIsAttached(this.d)) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public RealmFieldType e(long var1) {
      return RealmFieldType.fromNativeValue(this.nativeGetColumnType(this.d, var1));
   }

   public long f(long var1) {
      return this.nativeGetLong(this.d, var1);
   }

   public boolean g(long var1) {
      return this.nativeGetBoolean(this.d, var1);
   }

   public long getNativeFinalizerPtr() {
      return a;
   }

   public long getNativePtr() {
      return this.d;
   }

   public float h(long var1) {
      return this.nativeGetFloat(this.d, var1);
   }

   public double i(long var1) {
      return this.nativeGetDouble(this.d, var1);
   }

   public Date j(long var1) {
      return new Date(this.nativeGetTimestamp(this.d, var1));
   }

   public String k(long var1) {
      return this.nativeGetString(this.d, var1);
   }

   public byte[] l(long var1) {
      return this.nativeGetByteArray(this.d, var1);
   }

   public long m(long var1) {
      return this.nativeGetLink(this.d, var1);
   }

   public LinkView n(long var1) {
      long var3 = this.nativeGetLinkView(this.d, var1);
      return new LinkView(this.b, this.c, var1, var3);
   }

   protected native boolean nativeGetBoolean(long var1, long var3);

   protected native byte[] nativeGetByteArray(long var1, long var3);

   protected native long nativeGetColumnCount(long var1);

   protected native long nativeGetColumnIndex(long var1, String var3);

   protected native String nativeGetColumnName(long var1, long var3);

   protected native int nativeGetColumnType(long var1, long var3);

   protected native double nativeGetDouble(long var1, long var3);

   protected native float nativeGetFloat(long var1, long var3);

   protected native long nativeGetIndex(long var1);

   protected native long nativeGetLink(long var1, long var3);

   protected native long nativeGetLinkView(long var1, long var3);

   protected native long nativeGetLong(long var1, long var3);

   protected native String nativeGetString(long var1, long var3);

   protected native long nativeGetTimestamp(long var1, long var3);

   protected native boolean nativeIsAttached(long var1);

   protected native boolean nativeIsNull(long var1, long var3);

   protected native boolean nativeIsNullLink(long var1, long var3);

   protected native void nativeNullifyLink(long var1, long var3);

   protected native void nativeSetBoolean(long var1, long var3, boolean var5);

   protected native void nativeSetByteArray(long var1, long var3, byte[] var5);

   protected native void nativeSetDouble(long var1, long var3, double var5);

   protected native void nativeSetLink(long var1, long var3, long var5);

   protected native void nativeSetLong(long var1, long var3, long var5);

   protected native void nativeSetNull(long var1, long var3);

   protected native void nativeSetString(long var1, long var3, String var5);

   protected native void nativeSetTimestamp(long var1, long var3, long var5);

   public void o(long var1) {
      this.c.h();
      this.nativeNullifyLink(this.d, var1);
   }
}
