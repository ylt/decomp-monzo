package io.realm.internal;

import io.realm.al;

public class CollectionChangeSet implements al, g {
   private static long a = nativeGetFinalizerPtr();
   private final long b;

   public CollectionChangeSet(long var1) {
      this.b = var1;
      f.a.a(this);
   }

   private al.a[] a(int[] var1) {
      int var2 = 0;
      al.a[] var4;
      if(var1 == null) {
         var4 = new al.a[0];
      } else {
         al.a[] var3;
         for(var3 = new al.a[var1.length / 2]; var2 < var3.length; ++var2) {
            var3[var2] = new al.a(var1[var2 * 2], var1[var2 * 2 + 1]);
         }

         var4 = var3;
      }

      return var4;
   }

   private static native long nativeGetFinalizerPtr();

   private static native int[] nativeGetRanges(long var0, int var2);

   public al.a[] a() {
      return this.a(nativeGetRanges(this.b, 0));
   }

   public al.a[] b() {
      return this.a(nativeGetRanges(this.b, 1));
   }

   public al.a[] c() {
      return this.a(nativeGetRanges(this.b, 2));
   }

   public long getNativeFinalizerPtr() {
      return a;
   }

   public long getNativePtr() {
      return this.b;
   }
}
