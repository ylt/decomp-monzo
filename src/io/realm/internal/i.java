package io.realm.internal;

import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class i {
   private List a = new CopyOnWriteArrayList();
   private boolean b = false;

   public void a(i.a var1) {
      Iterator var3 = this.a.iterator();

      while(var3.hasNext()) {
         i.b var2 = (i.b)var3.next();
         if(this.b) {
            break;
         }

         Object var4 = var2.a.get();
         if(var4 == null) {
            this.a.remove(var2);
         } else if(!var2.c) {
            var1.a(var2, var4);
         }
      }

   }

   public void a(i.b var1) {
      if(!this.a.contains(var1)) {
         this.a.add(var1);
         var1.c = false;
      }

      if(this.b) {
         this.b = false;
      }

   }

   void a(Object var1) {
      Iterator var2 = this.a.iterator();

      while(true) {
         i.b var3;
         Object var4;
         do {
            if(!var2.hasNext()) {
               return;
            }

            var3 = (i.b)var2.next();
            var4 = var3.a.get();
         } while(var4 != null && var4 != var1);

         var3.c = true;
         this.a.remove(var3);
      }
   }

   public void a(Object var1, Object var2) {
      Iterator var3 = this.a.iterator();

      while(var3.hasNext()) {
         i.b var4 = (i.b)var3.next();
         if(var1 == var4.a.get() && var2.equals(var4.b)) {
            var4.c = true;
            this.a.remove(var4);
            break;
         }
      }

   }

   public boolean a() {
      return this.a.isEmpty();
   }

   public void b() {
      this.b = true;
      this.a.clear();
   }

   public int c() {
      return this.a.size();
   }

   public interface a {
      void a(i.b var1, Object var2);
   }

   public abstract static class b {
      final WeakReference a;
      protected final Object b;
      boolean c = false;

      b(Object var1, Object var2) {
         this.b = var2;
         this.a = new WeakReference(var1);
      }

      public boolean equals(Object var1) {
         boolean var2 = true;
         if(this != var1) {
            if(var1 instanceof i.b) {
               i.b var3 = (i.b)var1;
               if(!this.b.equals(var3.b) || this.a.get() != var3.a.get()) {
                  var2 = false;
               }
            } else {
               var2 = false;
            }
         }

         return var2;
      }

      public int hashCode() {
         int var2 = 0;
         Object var3 = this.a.get();
         int var1;
         if(var3 != null) {
            var1 = var3.hashCode();
         } else {
            var1 = 0;
         }

         if(this.b != null) {
            var2 = this.b.hashCode();
         }

         return (var1 + 527) * 31 + var2;
      }
   }
}
