package io.realm.internal;

import io.realm.RealmFieldType;

public class CheckedRow extends UncheckedRow {
   private UncheckedRow a;

   private CheckedRow(UncheckedRow var1) {
      super(var1);
      this.a = var1;
   }

   private CheckedRow(f var1, Table var2, long var3) {
      super(var1, var2, var3);
   }

   public static CheckedRow a(UncheckedRow var0) {
      return new CheckedRow(var0);
   }

   public static CheckedRow a(f var0, Table var1, long var2) {
      return new CheckedRow(var0, var1, var1.nativeGetRowPtr(var1.getNativePtr(), var2));
   }

   public boolean a(long var1) {
      RealmFieldType var4 = this.e(var1);
      boolean var3;
      if(var4 != RealmFieldType.OBJECT && var4 != RealmFieldType.LIST) {
         var3 = false;
      } else {
         var3 = super.a(var1);
      }

      return var3;
   }

   public boolean b(long var1) {
      return super.b(var1);
   }

   public void c(long var1) {
      if(this.e(var1) == RealmFieldType.BINARY) {
         super.a(var1, (byte[])null);
      } else {
         super.c(var1);
      }

   }

   protected native boolean nativeGetBoolean(long var1, long var3);

   protected native byte[] nativeGetByteArray(long var1, long var3);

   protected native long nativeGetColumnCount(long var1);

   protected native long nativeGetColumnIndex(long var1, String var3);

   protected native String nativeGetColumnName(long var1, long var3);

   protected native int nativeGetColumnType(long var1, long var3);

   protected native double nativeGetDouble(long var1, long var3);

   protected native float nativeGetFloat(long var1, long var3);

   protected native long nativeGetLink(long var1, long var3);

   protected native long nativeGetLinkView(long var1, long var3);

   protected native long nativeGetLong(long var1, long var3);

   protected native String nativeGetString(long var1, long var3);

   protected native long nativeGetTimestamp(long var1, long var3);

   protected native boolean nativeIsNullLink(long var1, long var3);

   protected native void nativeNullifyLink(long var1, long var3);

   protected native void nativeSetBoolean(long var1, long var3, boolean var5);

   protected native void nativeSetByteArray(long var1, long var3, byte[] var5);

   protected native void nativeSetDouble(long var1, long var3, double var5);

   protected native void nativeSetLink(long var1, long var3, long var5);

   protected native void nativeSetLong(long var1, long var3, long var5);

   protected native void nativeSetString(long var1, long var3, String var5);

   protected native void nativeSetTimestamp(long var1, long var3, long var5);
}
