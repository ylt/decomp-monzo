package io.realm.internal;

import io.realm.log.RealmLog;
import java.lang.ref.ReferenceQueue;

class d implements Runnable {
   private final ReferenceQueue a;

   d(ReferenceQueue var1) {
      this.a = var1;
   }

   public void run() {
      while(true) {
         try {
            ((NativeObjectReference)this.a.remove()).a();
         } catch (InterruptedException var2) {
            Thread.currentThread().interrupt();
            RealmLog.c("The FinalizerRunnable thread has been interrupted. Native resources cannot be freed anymore", new Object[0]);
            return;
         }
      }
   }
}
