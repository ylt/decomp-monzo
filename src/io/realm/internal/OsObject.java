package io.realm.internal;

import io.realm.RealmFieldType;
import io.realm.ak;
import io.realm.bb;
import io.realm.bd;
import io.realm.exceptions.RealmException;

@KeepMember
public class OsObject implements g {
   private static final long b = nativeGetFinalizerPtr();
   private final long a;
   private i c = new i();

   public OsObject(SharedRealm var1, UncheckedRow var2) {
      this.a = nativeCreate(var1.getNativePtr(), var2.getNativePtr());
      var1.e.a(this);
   }

   public static UncheckedRow a(Table var0) {
      SharedRealm var1 = var0.f();
      return new UncheckedRow(var1.e, var0, nativeCreateNewObject(var1.getNativePtr(), var0.getNativePtr()));
   }

   public static UncheckedRow a(Table var0, Object var1) {
      long var5 = c(var0);
      RealmFieldType var12 = var0.c(var5);
      SharedRealm var11 = var0.f();
      UncheckedRow var13;
      if(var12 == RealmFieldType.STRING) {
         if(var1 != null && !(var1 instanceof String)) {
            throw new IllegalArgumentException("Primary key value is not a String: " + var1);
         }

         var13 = new UncheckedRow(var11.e, var0, nativeCreateNewObjectWithStringPrimaryKey(var11.getNativePtr(), var0.getNativePtr(), var5, (String)var1));
      } else {
         if(var12 != RealmFieldType.INTEGER) {
            throw new RealmException("Cannot check for duplicate rows for unsupported primary key type: " + var12);
         }

         long var3;
         if(var1 == null) {
            var3 = 0L;
         } else {
            var3 = Long.parseLong(var1.toString());
         }

         f var14 = var11.e;
         long var7 = var11.getNativePtr();
         long var9 = var0.getNativePtr();
         boolean var2;
         if(var1 == null) {
            var2 = true;
         } else {
            var2 = false;
         }

         var13 = new UncheckedRow(var14, var0, nativeCreateNewObjectWithLongPrimaryKey(var7, var9, var5, var3, var2));
      }

      return var13;
   }

   public static long b(Table var0) {
      return nativeCreateRow(var0.f().getNativePtr(), var0.getNativePtr());
   }

   public static long b(Table var0, Object var1) {
      long var5 = c(var0);
      RealmFieldType var12 = var0.c(var5);
      SharedRealm var11 = var0.f();
      long var3;
      if(var12 == RealmFieldType.STRING) {
         if(var1 != null && !(var1 instanceof String)) {
            throw new IllegalArgumentException("Primary key value is not a String: " + var1);
         }

         var3 = nativeCreateRowWithStringPrimaryKey(var11.getNativePtr(), var0.getNativePtr(), var5, (String)var1);
      } else {
         if(var12 != RealmFieldType.INTEGER) {
            throw new RealmException("Cannot check for duplicate rows for unsupported primary key type: " + var12);
         }

         if(var1 == null) {
            var3 = 0L;
         } else {
            var3 = Long.parseLong(var1.toString());
         }

         long var9 = var11.getNativePtr();
         long var7 = var0.getNativePtr();
         boolean var2;
         if(var1 == null) {
            var2 = true;
         } else {
            var2 = false;
         }

         var3 = nativeCreateRowWithLongPrimaryKey(var9, var7, var5, var3, var2);
      }

      return var3;
   }

   private static long c(Table var0) {
      long var1 = var0.d();
      if(var1 == -2L) {
         throw new IllegalStateException(var0.j() + " has no primary key defined.");
      } else {
         return var1;
      }
   }

   private static native long nativeCreate(long var0, long var2);

   private static native long nativeCreateNewObject(long var0, long var2);

   private static native long nativeCreateNewObjectWithLongPrimaryKey(long var0, long var2, long var4, long var6, boolean var8);

   private static native long nativeCreateNewObjectWithStringPrimaryKey(long var0, long var2, long var4, String var6);

   private static native long nativeCreateRow(long var0, long var2);

   private static native long nativeCreateRowWithLongPrimaryKey(long var0, long var2, long var4, long var6, boolean var8);

   private static native long nativeCreateRowWithStringPrimaryKey(long var0, long var2, long var4, String var6);

   private static native long nativeGetFinalizerPtr();

   private native void nativeStartListening(long var1);

   @KeepMember
   private void notifyChangeListeners(String[] var1) {
      this.c.a((i.a)(new OsObject.a(var1)));
   }

   public void a(i var1) {
      if(!this.c.a()) {
         throw new IllegalStateException("'observerPairs' is not empty. Listeners have been added before.");
      } else {
         this.c = var1;
         if(!var1.a()) {
            this.nativeStartListening(this.a);
         }

      }
   }

   public long getNativeFinalizerPtr() {
      return b;
   }

   public long getNativePtr() {
      return this.a;
   }

   private static class a implements i.a {
      private final String[] a;

      a(String[] var1) {
         this.a = var1;
      }

      private ak a() {
         boolean var1;
         if(this.a == null) {
            var1 = true;
         } else {
            var1 = false;
         }

         String[] var2;
         if(var1) {
            var2 = new String[0];
         } else {
            var2 = this.a;
         }

         return new OsObject.c(var2, var1);
      }

      public void a(OsObject.b var1, Object var2) {
         var1.a((bb)var2, this.a());
      }
   }

   public static class b extends i.b {
      public void a(bb var1, ak var2) {
         ((bd)this.b).a(var1, var2);
      }
   }

   private static class c implements ak {
      final String[] a;
      final boolean b;

      c(String[] var1, boolean var2) {
         this.a = var1;
         this.b = var2;
      }
   }
}
