package io.realm.internal;

import io.realm.al;
import io.realm.am;
import io.realm.ax;
import java.util.ConcurrentModificationException;
import java.util.Date;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.NoSuchElementException;

@Keep
public class Collection implements g {
   public static final byte AGGREGATE_FUNCTION_AVERAGE = 3;
   public static final byte AGGREGATE_FUNCTION_MAXIMUM = 2;
   public static final byte AGGREGATE_FUNCTION_MINIMUM = 1;
   public static final byte AGGREGATE_FUNCTION_SUM = 4;
   private static final String CLOSED_REALM_MESSAGE = "This Realm instance has already been closed, making it unusable.";
   public static final byte MODE_EMPTY = 0;
   public static final byte MODE_LINKVIEW = 3;
   public static final byte MODE_QUERY = 2;
   public static final byte MODE_TABLE = 1;
   public static final byte MODE_TABLEVIEW = 4;
   private static final long nativeFinalizerPtr = nativeGetFinalizerPtr();
   private final f context;
   private boolean isSnapshot;
   private boolean loaded;
   private final long nativePtr;
   private final i observerPairs;
   private final SharedRealm sharedRealm;
   private final Table table;

   public Collection(SharedRealm var1, LinkView var2, SortDescriptor var3) {
      this.isSnapshot = false;
      this.observerPairs = new i();
      this.nativePtr = nativeCreateResultsFromLinkView(var1.getNativePtr(), var2.getNativePtr(), var3);
      this.sharedRealm = var1;
      this.context = var1.e;
      this.table = var2.e();
      this.context.a(this);
      this.loaded = true;
   }

   private Collection(SharedRealm var1, Table var2, long var3) {
      this(var1, var2, var3, false);
   }

   private Collection(SharedRealm var1, Table var2, long var3, boolean var5) {
      this.isSnapshot = false;
      this.observerPairs = new i();
      this.sharedRealm = var1;
      this.context = var1.e;
      this.table = var2;
      this.nativePtr = var3;
      this.context.a(this);
      this.loaded = var5;
   }

   public Collection(SharedRealm var1, TableQuery var2) {
      this(var1, var2, (SortDescriptor)null, (SortDescriptor)null);
   }

   public Collection(SharedRealm var1, TableQuery var2, SortDescriptor var3) {
      this(var1, var2, var3, (SortDescriptor)null);
   }

   public Collection(SharedRealm var1, TableQuery var2, SortDescriptor var3, SortDescriptor var4) {
      this.isSnapshot = false;
      this.observerPairs = new i();
      var2.b();
      this.nativePtr = nativeCreateResults(var1.getNativePtr(), var2.getNativePtr(), var3, var4);
      this.sharedRealm = var1;
      this.context = var1.e;
      this.table = var2.a();
      this.context.a(this);
      this.loaded = false;
   }

   public static Collection createBacklinksCollection(SharedRealm var0, UncheckedRow var1, Table var2, String var3) {
      return new Collection(var0, var2, nativeCreateResultsFromBacklinks(var0.getNativePtr(), var1.getNativePtr(), var2.getNativePtr(), var2.a(var3)), true);
   }

   private static native Object nativeAggregate(long var0, long var2, byte var4);

   private static native void nativeClear(long var0);

   private static native boolean nativeContains(long var0, long var2);

   private static native long nativeCreateResults(long var0, long var2, SortDescriptor var4, SortDescriptor var5);

   private static native long nativeCreateResultsFromBacklinks(long var0, long var2, long var4, long var6);

   private static native long nativeCreateResultsFromLinkView(long var0, long var2, SortDescriptor var4);

   private static native long nativeCreateSnapshot(long var0);

   private static native void nativeDelete(long var0, long var2);

   private static native boolean nativeDeleteFirst(long var0);

   private static native boolean nativeDeleteLast(long var0);

   private static native long nativeDistinct(long var0, SortDescriptor var2);

   private static native long nativeFirstRow(long var0);

   private static native long nativeGetFinalizerPtr();

   private static native byte nativeGetMode(long var0);

   private static native long nativeGetRow(long var0, int var2);

   private static native long nativeIndexOf(long var0, long var2);

   private static native long nativeIndexOfBySourceRowIndex(long var0, long var2);

   private static native boolean nativeIsValid(long var0);

   private static native long nativeLastRow(long var0);

   private static native long nativeSize(long var0);

   private static native long nativeSort(long var0, SortDescriptor var2);

   private native void nativeStartListening(long var1);

   private native void nativeStopListening(long var1);

   private static native long nativeWhere(long var0);

   private void notifyChangeListeners(long var1) {
      if(var1 != 0L || !this.isLoaded()) {
         boolean var3 = this.loaded;
         this.loaded = true;
         i var5 = this.observerPairs;
         CollectionChangeSet var4;
         if(var1 != 0L && var3) {
            var4 = new CollectionChangeSet(var1);
         } else {
            var4 = null;
         }

         var5.a((i.a)(new Collection.b(var4)));
      }

   }

   public void addListener(Object var1, am var2) {
      if(this.observerPairs.a()) {
         this.nativeStartListening(this.nativePtr);
      }

      Collection.c var3 = new Collection.c(var1, var2);
      this.observerPairs.a((i.b)var3);
   }

   public void addListener(Object var1, ax var2) {
      this.addListener(var1, (am)(new Collection.g(var2)));
   }

   public Date aggregateDate(Collection.a var1, long var2) {
      return (Date)nativeAggregate(this.nativePtr, var2, var1.a());
   }

   public Number aggregateNumber(Collection.a var1, long var2) {
      return (Number)nativeAggregate(this.nativePtr, var2, var1.a());
   }

   public void clear() {
      nativeClear(this.nativePtr);
   }

   public boolean contains(UncheckedRow var1) {
      return nativeContains(this.nativePtr, var1.getNativePtr());
   }

   public Collection createSnapshot() {
      Collection var1;
      if(this.isSnapshot) {
         var1 = this;
      } else {
         var1 = new Collection(this.sharedRealm, this.table, nativeCreateSnapshot(this.nativePtr));
         var1.isSnapshot = true;
      }

      return var1;
   }

   public void delete(long var1) {
      nativeDelete(this.nativePtr, var1);
   }

   public boolean deleteFirst() {
      return nativeDeleteFirst(this.nativePtr);
   }

   public boolean deleteLast() {
      return nativeDeleteLast(this.nativePtr);
   }

   public Collection distinct(SortDescriptor var1) {
      return new Collection(this.sharedRealm, this.table, nativeDistinct(this.nativePtr, var1));
   }

   public UncheckedRow firstUncheckedRow() {
      long var1 = nativeFirstRow(this.nativePtr);
      UncheckedRow var3;
      if(var1 != 0L) {
         var3 = this.table.g(var1);
      } else {
         var3 = null;
      }

      return var3;
   }

   public Collection.f getMode() {
      return Collection.f.a(nativeGetMode(this.nativePtr));
   }

   public long getNativeFinalizerPtr() {
      return nativeFinalizerPtr;
   }

   public long getNativePtr() {
      return this.nativePtr;
   }

   public Table getTable() {
      return this.table;
   }

   public UncheckedRow getUncheckedRow(int var1) {
      return this.table.g(nativeGetRow(this.nativePtr, var1));
   }

   public int indexOf(long var1) {
      var1 = nativeIndexOfBySourceRowIndex(this.nativePtr, var1);
      int var3;
      if(var1 > 2147483647L) {
         var3 = Integer.MAX_VALUE;
      } else {
         var3 = (int)var1;
      }

      return var3;
   }

   public int indexOf(UncheckedRow var1) {
      long var3 = nativeIndexOf(this.nativePtr, var1.getNativePtr());
      int var2;
      if(var3 > 2147483647L) {
         var2 = Integer.MAX_VALUE;
      } else {
         var2 = (int)var3;
      }

      return var2;
   }

   public boolean isLoaded() {
      return this.loaded;
   }

   public boolean isValid() {
      return nativeIsValid(this.nativePtr);
   }

   public UncheckedRow lastUncheckedRow() {
      long var1 = nativeLastRow(this.nativePtr);
      UncheckedRow var3;
      if(var1 != 0L) {
         var3 = this.table.g(var1);
      } else {
         var3 = null;
      }

      return var3;
   }

   public void load() {
      if(!this.loaded) {
         this.notifyChangeListeners(0L);
      }

   }

   public void removeAllListeners() {
      this.observerPairs.b();
      this.nativeStopListening(this.nativePtr);
   }

   public void removeListener(Object var1, am var2) {
      this.observerPairs.a(var1, var2);
      if(this.observerPairs.a()) {
         this.nativeStopListening(this.nativePtr);
      }

   }

   public void removeListener(Object var1, ax var2) {
      this.removeListener(var1, (am)(new Collection.g(var2)));
   }

   public long size() {
      return nativeSize(this.nativePtr);
   }

   public Collection sort(SortDescriptor var1) {
      return new Collection(this.sharedRealm, this.table, nativeSort(this.nativePtr, var1));
   }

   public TableQuery where() {
      long var1 = nativeWhere(this.nativePtr);
      return new TableQuery(this.context, this.table, var1);
   }

   public static enum a {
      a(1),
      b(2),
      c(3),
      d(4);

      private final byte e;

      private a(byte var3) {
         this.e = var3;
      }

      public byte a() {
         return this.e;
      }
   }

   private static class b implements i.a {
      private final al a;

      b(al var1) {
         this.a = var1;
      }

      public void a(Collection.c var1, Object var2) {
         var1.a(var2, this.a);
      }
   }

   private static class c extends i.b {
      public c(Object var1, Object var2) {
         super(var1, var2);
      }

      public void a(Object var1, al var2) {
         if(this.b instanceof am) {
            ((am)this.b).a(var1, var2);
         } else {
            if(!(this.b instanceof ax)) {
               throw new RuntimeException("Unsupported listener type: " + this.b);
            }

            ((ax)this.b).a(var1);
         }

      }
   }

   public abstract static class d implements Iterator {
      Collection b;
      protected int c = -1;

      public d(Collection var1) {
         if(var1.sharedRealm.i()) {
            throw new IllegalStateException("This Realm instance has already been closed, making it unusable.");
         } else {
            this.b = var1;
            if(!var1.isSnapshot) {
               if(var1.sharedRealm.d()) {
                  this.a();
               } else {
                  this.b.sharedRealm.a(this);
               }
            }

         }
      }

      Object a(int var1) {
         return this.b(this.b.getUncheckedRow(var1));
      }

      void a() {
         this.b = this.b.createSnapshot();
      }

      protected abstract Object b(UncheckedRow var1);

      void b() {
         this.b = null;
      }

      void c() {
         if(this.b == null) {
            throw new ConcurrentModificationException("No outside changes to a Realm is allowed while iterating a living Realm collection.");
         }
      }

      public boolean hasNext() {
         this.c();
         boolean var1;
         if((long)(this.c + 1) < this.b.size()) {
            var1 = true;
         } else {
            var1 = false;
         }

         return var1;
      }

      public Object next() {
         this.c();
         ++this.c;
         if((long)this.c >= this.b.size()) {
            throw new NoSuchElementException("Cannot access index " + this.c + " when size is " + this.b.size() + ". Remember to check hasNext() before using next().");
         } else {
            return this.a(this.c);
         }
      }

      @Deprecated
      public void remove() {
         throw new UnsupportedOperationException("remove() is not supported by RealmResults iterators.");
      }
   }

   public abstract static class e extends Collection.d implements ListIterator {
      public e(Collection var1, int var2) {
         super(var1);
         if(var2 >= 0 && (long)var2 <= this.b.size()) {
            this.c = var2 - 1;
         } else {
            throw new IndexOutOfBoundsException("Starting location must be a valid index: [0, " + (this.b.size() - 1L) + "]. Yours was " + var2);
         }
      }

      @Deprecated
      public void add(Object var1) {
         throw new UnsupportedOperationException("Adding an element is not supported. Use Realm.createObject() instead.");
      }

      public boolean hasPrevious() {
         this.c();
         boolean var1;
         if(this.c >= 0) {
            var1 = true;
         } else {
            var1 = false;
         }

         return var1;
      }

      public int nextIndex() {
         this.c();
         return this.c + 1;
      }

      public Object previous() {
         this.c();

         try {
            Object var1 = this.a(this.c);
            --this.c;
            return var1;
         } catch (IndexOutOfBoundsException var2) {
            throw new NoSuchElementException("Cannot access index less than zero. This was " + this.c + ". Remember to check hasPrevious() before using previous().");
         }
      }

      public int previousIndex() {
         this.c();
         return this.c;
      }

      @Deprecated
      public void set(Object var1) {
         throw new UnsupportedOperationException("Replacing and element is not supported.");
      }
   }

   public static enum f {
      a,
      b,
      c,
      d,
      e;

      static Collection.f a(byte var0) {
         Collection.f var1;
         switch(var0) {
         case 0:
            var1 = a;
            break;
         case 1:
            var1 = b;
            break;
         case 2:
            var1 = c;
            break;
         case 3:
            var1 = d;
            break;
         case 4:
            var1 = e;
            break;
         default:
            throw new IllegalArgumentException("Invalid value: " + var0);
         }

         return var1;
      }
   }

   private static class g implements am {
      private final ax a;

      g(ax var1) {
         this.a = var1;
      }

      public void a(Object var1, al var2) {
         this.a.a(var1);
      }

      public boolean equals(Object var1) {
         boolean var2;
         if(var1 instanceof Collection.g && this.a == ((Collection.g)var1).a) {
            var2 = true;
         } else {
            var2 = false;
         }

         return var2;
      }

      public int hashCode() {
         return this.a.hashCode();
      }
   }
}
