package io.realm.internal;

import java.util.Date;

public class TableQuery implements g {
   private static final long a = nativeGetFinalizerPtr();
   private final f b;
   private final Table c;
   private final long d;
   private boolean e = true;

   public TableQuery(f var1, Table var2, long var3) {
      this.b = var1;
      this.c = var2;
      this.d = var3;
      var1.a(this);
   }

   private native void nativeBetweenTimestamp(long var1, long[] var3, long var4, long var6);

   private native void nativeContains(long var1, long[] var3, long[] var4, String var5, boolean var6);

   private native long nativeCount(long var1, long var3, long var5, long var7);

   private native void nativeEndGroup(long var1);

   private native void nativeEqual(long var1, long[] var3, long[] var4, String var5, boolean var6);

   private native void nativeEqual(long var1, long[] var3, long[] var4, boolean var5);

   private native long nativeFind(long var1, long var3);

   private static native long nativeGetFinalizerPtr();

   private native void nativeGreaterEqualTimestamp(long var1, long[] var3, long[] var4, long var5);

   private native void nativeGroup(long var1);

   private native void nativeIsNotNull(long var1, long[] var3, long[] var4);

   private native void nativeIsNull(long var1, long[] var3, long[] var4);

   private native void nativeLess(long var1, long[] var3, long[] var4, long var5);

   private native void nativeLessEqualTimestamp(long var1, long[] var3, long[] var4, long var5);

   private native Long nativeMaximumTimestamp(long var1, long var3, long var5, long var7, long var9);

   private native void nativeNot(long var1);

   private native void nativeNotEqual(long var1, long[] var3, long[] var4, String var5, boolean var6);

   private native void nativeOr(long var1);

   private native double nativeSumDouble(long var1, long var3, long var5, long var7, long var9);

   private native double nativeSumFloat(long var1, long var3, long var5, long var7, long var9);

   private native long nativeSumInt(long var1, long var3, long var5, long var7, long var9);

   private native String nativeValidateQuery(long var1);

   public long a(long var1) {
      this.b();
      return this.nativeSumInt(this.d, var1, 0L, -1L, -1L);
   }

   public Table a() {
      return this.c;
   }

   public TableQuery a(long[] var1, Date var2, Date var3) {
      if(var2 != null && var3 != null) {
         this.nativeBetweenTimestamp(this.d, var1, var2.getTime(), var3.getTime());
         this.e = false;
         return this;
      } else {
         throw new IllegalArgumentException("Date values in query criteria must not be null.");
      }
   }

   public TableQuery a(long[] var1, long[] var2) {
      this.nativeIsNull(this.d, var1, var2);
      this.e = false;
      return this;
   }

   public TableQuery a(long[] var1, long[] var2, long var3) {
      this.nativeLess(this.d, var1, var2, var3);
      this.e = false;
      return this;
   }

   public TableQuery a(long[] var1, long[] var2, String var3, io.realm.l var4) {
      this.nativeEqual(this.d, var1, var2, var3, var4.a());
      this.e = false;
      return this;
   }

   public TableQuery a(long[] var1, long[] var2, Date var3) {
      if(var3 == null) {
         throw new IllegalArgumentException("Date value in query criteria must not be null.");
      } else {
         this.nativeGreaterEqualTimestamp(this.d, var1, var2, var3.getTime());
         this.e = false;
         return this;
      }
   }

   public TableQuery a(long[] var1, long[] var2, boolean var3) {
      this.nativeEqual(this.d, var1, var2, var3);
      this.e = false;
      return this;
   }

   public double b(long var1) {
      this.b();
      return this.nativeSumFloat(this.d, var1, 0L, -1L, -1L);
   }

   public TableQuery b(long[] var1, long[] var2) {
      this.nativeIsNotNull(this.d, var1, var2);
      this.e = false;
      return this;
   }

   public TableQuery b(long[] var1, long[] var2, String var3, io.realm.l var4) {
      this.nativeNotEqual(this.d, var1, var2, var3, var4.a());
      this.e = false;
      return this;
   }

   public TableQuery b(long[] var1, long[] var2, Date var3) {
      if(var3 == null) {
         throw new IllegalArgumentException("Date value in query criteria must not be null.");
      } else {
         this.nativeLessEqualTimestamp(this.d, var1, var2, var3.getTime());
         this.e = false;
         return this;
      }
   }

   void b() {
      if(!this.e) {
         String var1 = this.nativeValidateQuery(this.d);
         if(!var1.equals("")) {
            throw new UnsupportedOperationException(var1);
         }

         this.e = true;
      }

   }

   public double c(long var1) {
      this.b();
      return this.nativeSumDouble(this.d, var1, 0L, -1L, -1L);
   }

   public TableQuery c() {
      this.nativeGroup(this.d);
      this.e = false;
      return this;
   }

   public TableQuery c(long[] var1, long[] var2, String var3, io.realm.l var4) {
      this.nativeContains(this.d, var1, var2, var3, var4.a());
      this.e = false;
      return this;
   }

   public TableQuery d() {
      this.nativeEndGroup(this.d);
      this.e = false;
      return this;
   }

   public Date d(long var1) {
      this.b();
      Long var3 = this.nativeMaximumTimestamp(this.d, var1, 0L, -1L, -1L);
      Date var4;
      if(var3 != null) {
         var4 = new Date(var3.longValue());
      } else {
         var4 = null;
      }

      return var4;
   }

   public TableQuery e() {
      this.nativeOr(this.d);
      this.e = false;
      return this;
   }

   public TableQuery f() {
      this.nativeNot(this.d);
      this.e = false;
      return this;
   }

   public long g() {
      this.b();
      return this.nativeFind(this.d, 0L);
   }

   public long getNativeFinalizerPtr() {
      return a;
   }

   public long getNativePtr() {
      return this.d;
   }

   public long h() {
      this.b();
      return this.nativeCount(this.d, 0L, -1L, -1L);
   }
}
