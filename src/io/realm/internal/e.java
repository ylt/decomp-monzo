package io.realm.internal;

import io.realm.RealmFieldType;
import java.util.Date;

public enum e implements n {
   a;

   private RuntimeException e() {
      return new IllegalStateException("Object is no longer managed by Realm. Has it been deleted?");
   }

   public long a() {
      throw this.e();
   }

   public long a(String var1) {
      throw this.e();
   }

   public void a(long var1, double var3) {
      throw this.e();
   }

   public void a(long var1, long var3) {
      throw this.e();
   }

   public void a(long var1, String var3) {
      throw this.e();
   }

   public void a(long var1, Date var3) {
      throw this.e();
   }

   public void a(long var1, boolean var3) {
      throw this.e();
   }

   public boolean a(long var1) {
      throw this.e();
   }

   public Table b() {
      throw this.e();
   }

   public void b(long var1, long var3) {
      throw this.e();
   }

   public boolean b(long var1) {
      throw this.e();
   }

   public long c() {
      throw this.e();
   }

   public void c(long var1) {
      throw this.e();
   }

   public String d(long var1) {
      throw this.e();
   }

   public boolean d() {
      return false;
   }

   public RealmFieldType e(long var1) {
      throw this.e();
   }

   public long f(long var1) {
      throw this.e();
   }

   public boolean g(long var1) {
      throw this.e();
   }

   public float h(long var1) {
      throw this.e();
   }

   public double i(long var1) {
      throw this.e();
   }

   public Date j(long var1) {
      throw this.e();
   }

   public String k(long var1) {
      throw this.e();
   }

   public byte[] l(long var1) {
      throw this.e();
   }

   public long m(long var1) {
      throw this.e();
   }

   public LinkView n(long var1) {
      throw this.e();
   }

   public void o(long var1) {
      throw this.e();
   }
}
