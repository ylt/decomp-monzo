package io.realm.internal;

import io.realm.RealmFieldType;
import io.realm.exceptions.RealmPrimaryKeyConstraintException;
import java.util.Date;

public class Table implements g {
   private static final String a = Util.a();
   private static final long b = nativeGetFinalizerPtr();
   private final long c;
   private final f d;
   private final SharedRealm e;
   private long f;

   Table(SharedRealm var1, long var2) {
      this.f = -1L;
      this.d = var1.e;
      this.e = var1;
      this.c = var2;
      this.d.a(this);
   }

   Table(Table var1, long var2) {
      this(var1.e, var2);
   }

   public static void a(Object var0) {
      throw new RealmPrimaryKeyConstraintException("Value already exists: " + var0);
   }

   public static boolean a(SharedRealm var0) {
      if(var0 == null || !var0.d()) {
         m();
      }

      boolean var1;
      if(!var0.a("pk")) {
         var1 = false;
      } else {
         Table var2 = var0.b("pk");
         var1 = nativeMigratePrimaryKeyTableIfNeeded(var0.f(), var2.c);
      }

      return var1;
   }

   public static boolean b(SharedRealm var0) {
      boolean var1;
      if(!var0.a("pk")) {
         var1 = false;
      } else {
         var1 = nativePrimaryKeyTableNeedsMigration(var0.b("pk").c);
      }

      return var1;
   }

   public static boolean b(String var0) {
      return var0.startsWith(a);
   }

   public static String c(String var0) {
      String var1;
      if(var0 == null) {
         var1 = null;
      } else {
         var1 = var0;
         if(var0.startsWith(a)) {
            var1 = var0.substring(a.length());
         }
      }

      return var1;
   }

   public static String d(String var0) {
      String var1;
      if(var0 == null) {
         var1 = null;
      } else {
         var1 = var0;
         if(!var0.startsWith(a)) {
            var1 = a + var0;
         }
      }

      return var1;
   }

   private void e(String var1) {
      if(var1.length() > 63) {
         throw new IllegalArgumentException("Column names are currently limited to max 63 characters.");
      }
   }

   private Table l() {
      Table var1;
      if(this.e == null) {
         var1 = null;
      } else {
         if(!this.e.a("pk")) {
            this.e.c("pk");
         }

         Table var2 = this.e.b("pk");
         var1 = var2;
         if(var2.c() == 0L) {
            this.h();
            var2.i(var2.a(RealmFieldType.STRING, "pk_table"));
            var2.a(RealmFieldType.STRING, "pk_property");
            var1 = var2;
         }
      }

      return var1;
   }

   private boolean l(long var1) {
      boolean var3;
      if(var1 == this.d()) {
         var3 = true;
      } else {
         var3 = false;
      }

      return var3;
   }

   private static void m() {
      throw new IllegalStateException("Cannot modify managed objects outside of a write transaction.");
   }

   private boolean m(long var1) {
      boolean var3;
      if(var1 >= 0L && var1 == this.d()) {
         var3 = true;
      } else {
         var3 = false;
      }

      return var3;
   }

   private native long nativeAddColumn(long var1, int var3, String var4, boolean var5);

   private native void nativeAddSearchIndex(long var1, long var3);

   private native void nativeClear(long var1);

   public static native long nativeFindFirstInt(long var0, long var2, long var4);

   public static native long nativeFindFirstNull(long var0, long var2);

   public static native long nativeFindFirstString(long var0, long var2, String var4);

   private native long nativeGetColumnCount(long var1);

   private native long nativeGetColumnIndex(long var1, String var3);

   private native String nativeGetColumnName(long var1, long var3);

   private native int nativeGetColumnType(long var1, long var3);

   private static native long nativeGetFinalizerPtr();

   private native long nativeGetLinkTarget(long var1, long var3);

   public static native long nativeGetLinkView(long var0, long var2, long var4);

   private native String nativeGetName(long var1);

   private native boolean nativeHasSameSchema(long var1, long var3);

   private native boolean nativeHasSearchIndex(long var1, long var3);

   private native boolean nativeIsColumnNullable(long var1, long var3);

   private static native boolean nativeMigratePrimaryKeyTableIfNeeded(long var0, long var2);

   private native void nativeMoveLastOver(long var1, long var3);

   public static native void nativeNullifyLink(long var0, long var2, long var4);

   private static native boolean nativePrimaryKeyTableNeedsMigration(long var0);

   public static native void nativeSetBoolean(long var0, long var2, long var4, boolean var6, boolean var7);

   public static native void nativeSetDouble(long var0, long var2, long var4, double var6, boolean var8);

   public static native void nativeSetLink(long var0, long var2, long var4, long var6, boolean var8);

   public static native void nativeSetLong(long var0, long var2, long var4, long var6, boolean var8);

   public static native void nativeSetNull(long var0, long var2, long var4, boolean var6);

   public static native void nativeSetString(long var0, long var2, long var4, String var6, boolean var7);

   public static native void nativeSetTimestamp(long var0, long var2, long var4, long var6, boolean var8);

   private native long nativeSize(long var1);

   private native long nativeWhere(long var1);

   public long a() {
      return this.nativeSize(this.c);
   }

   public long a(long var1, String var3) {
      if(var3 == null) {
         throw new IllegalArgumentException("null is not supported");
      } else {
         return nativeFindFirstString(this.c, var1, var3);
      }
   }

   public long a(RealmFieldType var1, String var2) {
      return this.a(var1, var2, false);
   }

   public long a(RealmFieldType var1, String var2, boolean var3) {
      this.e(var2);
      return this.nativeAddColumn(this.c, var1.getNativeValue(), var2, var3);
   }

   public long a(String var1) {
      if(var1 == null) {
         throw new IllegalArgumentException("Column name can not be null.");
      } else {
         return this.nativeGetColumnIndex(this.c, var1);
      }
   }

   void a(long var1, long var3) {
      if(this.l(var1)) {
         RealmFieldType var5 = this.c(var1);
         switch(null.a[var5.ordinal()]) {
         case 1:
         case 2:
            var1 = this.k(var1);
            if(var1 != var3 && var1 != -1L) {
               a((Object)"null");
            }
         }
      }

   }

   public void a(long var1, long var3, double var5, boolean var7) {
      this.h();
      nativeSetDouble(this.c, var1, var3, var5, var7);
   }

   void a(long var1, long var3, long var5) {
      if(this.l(var1)) {
         var1 = this.b(var1, var5);
         if(var1 != var3 && var1 != -1L) {
            a((Object)Long.valueOf(var5));
         }
      }

   }

   public void a(long var1, long var3, long var5, boolean var7) {
      this.h();
      this.a(var1, var3, var5);
      nativeSetLong(this.c, var1, var3, var5, var7);
   }

   void a(long var1, long var3, String var5) {
      if(this.m(var1)) {
         var1 = this.a(var1, var5);
         if(var1 != var3 && var1 != -1L) {
            a((Object)var5);
         }
      }

   }

   public void a(long var1, long var3, String var5, boolean var6) {
      this.h();
      if(var5 == null) {
         this.a(var1, var3);
         nativeSetNull(this.c, var1, var3, var6);
      } else {
         this.a(var1, var3, var5);
         nativeSetString(this.c, var1, var3, var5, var6);
      }

   }

   public void a(long var1, long var3, Date var5, boolean var6) {
      if(var5 == null) {
         throw new IllegalArgumentException("Null Date is not allowed.");
      } else {
         this.h();
         nativeSetTimestamp(this.c, var1, var3, var5.getTime(), var6);
      }
   }

   public void a(long var1, long var3, boolean var5) {
      this.h();
      this.a(var1, var3);
      nativeSetNull(this.c, var1, var3, var5);
   }

   public void a(long var1, long var3, boolean var5, boolean var6) {
      this.h();
      nativeSetBoolean(this.c, var1, var3, var5, var6);
   }

   public boolean a(long var1) {
      return this.nativeIsColumnNullable(this.c, var1);
   }

   public boolean a(Table var1) {
      if(var1 == null) {
         throw new IllegalArgumentException("The argument cannot be null");
      } else {
         return this.nativeHasSameSchema(this.c, var1.c);
      }
   }

   public long b(long var1, long var3) {
      return nativeFindFirstInt(this.c, var1, var3);
   }

   public String b(long var1) {
      return this.nativeGetColumnName(this.c, var1);
   }

   public void b() {
      this.h();
      this.nativeClear(this.c);
   }

   public void b(long var1, long var3, long var5, boolean var7) {
      this.h();
      nativeSetLink(this.c, var1, var3, var5, var7);
   }

   public long c() {
      return this.nativeGetColumnCount(this.c);
   }

   public RealmFieldType c(long var1) {
      return RealmFieldType.fromNativeValue(this.nativeGetColumnType(this.c, var1));
   }

   public long d() {
      long var1 = -2L;
      if(this.f < 0L && this.f != -2L) {
         Table var3 = this.l();
         if(var3 != null) {
            var1 = var3.a(0L, this.k());
            if(var1 != -1L) {
               this.f = this.a(var3.f(var1).k(1L));
            } else {
               this.f = -2L;
            }

            var1 = this.f;
         }
      } else {
         var1 = this.f;
      }

      return var1;
   }

   public void d(long var1) {
      this.h();
      this.nativeMoveLastOver(this.c, var1);
   }

   public Table e(long var1) {
      var1 = this.nativeGetLinkTarget(this.c, var1);
      return new Table(this.e, var1);
   }

   public boolean e() {
      boolean var1;
      if(this.d() >= 0L) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   SharedRealm f() {
      return this.e;
   }

   public UncheckedRow f(long var1) {
      return UncheckedRow.b(this.d, this, var1);
   }

   public UncheckedRow g(long var1) {
      return UncheckedRow.c(this.d, this, var1);
   }

   boolean g() {
      boolean var1;
      if(this.e != null && !this.e.d()) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public long getNativeFinalizerPtr() {
      return b;
   }

   public long getNativePtr() {
      return this.c;
   }

   public CheckedRow h(long var1) {
      return CheckedRow.a(this.d, this, var1);
   }

   void h() {
      if(this.g()) {
         m();
      }

   }

   public TableQuery i() {
      long var1 = this.nativeWhere(this.c);
      return new TableQuery(this.d, this, var1);
   }

   public void i(long var1) {
      this.h();
      this.nativeAddSearchIndex(this.c, var1);
   }

   public String j() {
      return this.nativeGetName(this.c);
   }

   public boolean j(long var1) {
      return this.nativeHasSearchIndex(this.c, var1);
   }

   public long k(long var1) {
      return nativeFindFirstNull(this.c, var1);
   }

   public String k() {
      return c(this.j());
   }

   native long nativeGetRowPtr(long var1, long var3);

   public String toString() {
      long var2 = this.c();
      String var5 = this.j();
      StringBuilder var4 = new StringBuilder("The Table ");
      if(var5 != null && !var5.isEmpty()) {
         var4.append(this.j());
         var4.append(" ");
      }

      if(this.e()) {
         var5 = this.b(this.d());
         var4.append("has '").append(var5).append("' field as a PrimaryKey, and ");
      }

      var4.append("contains ");
      var4.append(var2);
      var4.append(" columns: ");

      for(int var1 = 0; (long)var1 < var2; ++var1) {
         if(var1 != 0) {
            var4.append(", ");
         }

         var4.append(this.b((long)var1));
      }

      var4.append(".");
      var4.append(" And ");
      var4.append(this.a());
      var4.append(" rows.");
      return var4.toString();
   }
}
