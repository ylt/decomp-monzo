package io.realm;

import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.SharedRealm;
import io.realm.internal.Table;
import io.realm.log.RealmLog;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class bj extends co.uk.getmondo.d.ag implements bk, io.realm.internal.l {
   private static final OsObjectSchemaInfo c = e();
   private static final List d;
   private bj.a a;
   private au b;

   static {
      ArrayList var0 = new ArrayList();
      var0.add("sddMigrationType");
      d = Collections.unmodifiableList(var0);
   }

   bj() {
      this.b.f();
   }

   public static long a(av var0, co.uk.getmondo.d.ag var1, Map var2) {
      long var3;
      if(var1 instanceof io.realm.internal.l && ((io.realm.internal.l)var1).s_().a() != null && ((io.realm.internal.l)var1).s_().a().g().equals(var0.g())) {
         var3 = ((io.realm.internal.l)var1).s_().b().c();
      } else {
         Table var9 = var0.c(co.uk.getmondo.d.ag.class);
         long var7 = var9.getNativePtr();
         bj.a var10 = (bj.a)var0.f.c(co.uk.getmondo.d.ag.class);
         long var5 = OsObject.b(var9);
         var2.put(var1, Long.valueOf(var5));
         String var11 = ((bk)var1).b();
         var3 = var5;
         if(var11 != null) {
            Table.nativeSetString(var7, var10.a, var5, var11, false);
            var3 = var5;
         }
      }

      return var3;
   }

   public static co.uk.getmondo.d.ag a(co.uk.getmondo.d.ag var0, int var1, int var2, Map var3) {
      if(var1 <= var2 && var0 != null) {
         io.realm.internal.l.a var4 = (io.realm.internal.l.a)var3.get(var0);
         co.uk.getmondo.d.ag var5;
         if(var4 == null) {
            co.uk.getmondo.d.ag var6 = new co.uk.getmondo.d.ag();
            var3.put(var0, new io.realm.internal.l.a(var1, var6));
            var5 = var6;
         } else {
            if(var1 >= var4.a) {
               var0 = (co.uk.getmondo.d.ag)var4.b;
               return var0;
            }

            var5 = (co.uk.getmondo.d.ag)var4.b;
            var4.a = var1;
         }

         ((bk)var5).a(((bk)var0).b());
         var0 = var5;
      } else {
         var0 = null;
      }

      return var0;
   }

   public static co.uk.getmondo.d.ag a(av var0, co.uk.getmondo.d.ag var1, boolean var2, Map var3) {
      if(var1 instanceof io.realm.internal.l && ((io.realm.internal.l)var1).s_().a() != null && ((io.realm.internal.l)var1).s_().a().c != var0.c) {
         throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
      } else {
         if(!(var1 instanceof io.realm.internal.l) || ((io.realm.internal.l)var1).s_().a() == null || !((io.realm.internal.l)var1).s_().a().g().equals(var0.g())) {
            g.b var4 = (g.b)g.g.get();
            io.realm.internal.l var5 = (io.realm.internal.l)var3.get(var1);
            if(var5 != null) {
               var1 = (co.uk.getmondo.d.ag)var5;
            } else {
               var1 = b(var0, var1, var2, var3);
            }
         }

         return var1;
      }
   }

   public static bj.a a(SharedRealm var0, boolean var1) {
      if(!var0.a("class_SddMigration")) {
         throw new RealmMigrationNeededException(var0.h(), "The 'SddMigration' class is missing from the schema for this Realm.");
      } else {
         Table var6 = var0.b("class_SddMigration");
         long var4 = var6.c();
         if(var4 != 1L) {
            if(var4 < 1L) {
               throw new RealmMigrationNeededException(var0.h(), "Field count is less than expected - expected 1 but was " + var4);
            }

            if(!var1) {
               throw new RealmMigrationNeededException(var0.h(), "Field count is more than expected - expected 1 but was " + var4);
            }

            RealmLog.a("Field count is more than expected - expected 1 but was %1$d", new Object[]{Long.valueOf(var4)});
         }

         HashMap var8 = new HashMap();

         for(long var2 = 0L; var2 < var4; ++var2) {
            var8.put(var6.b(var2), var6.c(var2));
         }

         bj.a var7 = new bj.a(var0, var6);
         if(var6.e()) {
            throw new RealmMigrationNeededException(var0.h(), "Primary Key defined for field " + var6.b(var6.d()) + " was removed.");
         } else if(!var8.containsKey("sddMigrationType")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'sddMigrationType' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var8.get("sddMigrationType") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'sddMigrationType' in existing Realm file.");
         } else if(!var6.a(var7.a)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'sddMigrationType' is required. Either set @Required to field 'sddMigrationType' or migrate using RealmObjectSchema.setNullable().");
         } else {
            return var7;
         }
      }
   }

   public static void a(av var0, Iterator var1, Map var2) {
      Table var8 = var0.c(co.uk.getmondo.d.ag.class);
      long var3 = var8.getNativePtr();
      bj.a var7 = (bj.a)var0.f.c(co.uk.getmondo.d.ag.class);

      while(true) {
         while(true) {
            co.uk.getmondo.d.ag var9;
            do {
               if(!var1.hasNext()) {
                  return;
               }

               var9 = (co.uk.getmondo.d.ag)var1.next();
            } while(var2.containsKey(var9));

            if(var9 instanceof io.realm.internal.l && ((io.realm.internal.l)var9).s_().a() != null && ((io.realm.internal.l)var9).s_().a().g().equals(var0.g())) {
               var2.put(var9, Long.valueOf(((io.realm.internal.l)var9).s_().b().c()));
            } else {
               long var5 = OsObject.b(var8);
               var2.put(var9, Long.valueOf(var5));
               String var10 = ((bk)var9).b();
               if(var10 != null) {
                  Table.nativeSetString(var3, var7.a, var5, var10, false);
               } else {
                  Table.nativeSetNull(var3, var7.a, var5, false);
               }
            }
         }
      }
   }

   public static long b(av var0, co.uk.getmondo.d.ag var1, Map var2) {
      long var3;
      if(var1 instanceof io.realm.internal.l && ((io.realm.internal.l)var1).s_().a() != null && ((io.realm.internal.l)var1).s_().a().g().equals(var0.g())) {
         var3 = ((io.realm.internal.l)var1).s_().b().c();
      } else {
         Table var7 = var0.c(co.uk.getmondo.d.ag.class);
         long var5 = var7.getNativePtr();
         bj.a var8 = (bj.a)var0.f.c(co.uk.getmondo.d.ag.class);
         var3 = OsObject.b(var7);
         var2.put(var1, Long.valueOf(var3));
         String var9 = ((bk)var1).b();
         if(var9 != null) {
            Table.nativeSetString(var5, var8.a, var3, var9, false);
         } else {
            Table.nativeSetNull(var5, var8.a, var3, false);
         }
      }

      return var3;
   }

   public static co.uk.getmondo.d.ag b(av var0, co.uk.getmondo.d.ag var1, boolean var2, Map var3) {
      io.realm.internal.l var4 = (io.realm.internal.l)var3.get(var1);
      co.uk.getmondo.d.ag var5;
      if(var4 != null) {
         var5 = (co.uk.getmondo.d.ag)var4;
      } else {
         var5 = (co.uk.getmondo.d.ag)var0.a(co.uk.getmondo.d.ag.class, false, Collections.emptyList());
         var3.put(var1, (io.realm.internal.l)var5);
         bk var6 = (bk)var1;
         ((bk)var5).a(var6.b());
      }

      return var5;
   }

   public static OsObjectSchemaInfo c() {
      return c;
   }

   public static String d() {
      return "class_SddMigration";
   }

   private static OsObjectSchemaInfo e() {
      OsObjectSchemaInfo.a var0 = new OsObjectSchemaInfo.a("SddMigration");
      var0.a("sddMigrationType", RealmFieldType.STRING, false, false, false);
      return var0.a();
   }

   public void a(String var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            if(var1 == null) {
               var2.b().a(this.a.a, var2.c(), true);
            } else {
               var2.b().a(this.a.a, var2.c(), var1, true);
            }
         }
      } else {
         this.b.a().e();
         if(var1 == null) {
            this.b.b().c(this.a.a);
         } else {
            this.b.b().a(this.a.a, var1);
         }
      }

   }

   public String b() {
      this.b.a().e();
      return this.b.b().k(this.a.a);
   }

   public au s_() {
      return this.b;
   }

   public String toString() {
      String var1;
      if(!bc.b(this)) {
         var1 = "Invalid object";
      } else {
         StringBuilder var2 = new StringBuilder("SddMigration = proxy[");
         var2.append("{sddMigrationType:");
         if(this.b() != null) {
            var1 = this.b();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append("]");
         var1 = var2.toString();
      }

      return var1;
   }

   public void u_() {
      if(this.b == null) {
         g.b var1 = (g.b)g.g.get();
         this.a = (bj.a)var1.c();
         this.b = new au(this);
         this.b.a(var1.a());
         this.b.a(var1.b());
         this.b.a(var1.d());
         this.b.a(var1.e());
      }

   }

   static final class a extends io.realm.internal.c {
      long a;

      a(SharedRealm var1, Table var2) {
         super(1);
         this.a = this.a(var2, "sddMigrationType", RealmFieldType.STRING);
      }

      a(io.realm.internal.c var1, boolean var2) {
         super(var1, var2);
         this.a(var1, this);
      }

      protected final io.realm.internal.c a(boolean var1) {
         return new bj.a(this, var1);
      }

      protected final void a(io.realm.internal.c var1, io.realm.internal.c var2) {
         bj.a var3 = (bj.a)var1;
         ((bj.a)var2).a = var3.a;
      }
   }
}
