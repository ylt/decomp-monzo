package io.realm;

import io.realm.internal.Collection;
import io.realm.internal.UncheckedRow;
import java.util.AbstractList;
import java.util.Iterator;
import java.util.ListIterator;

abstract class an extends AbstractList implements OrderedRealmCollection {
   final g a;
   final Class b;
   final String c;
   final Collection d;

   an(g var1, Collection var2, Class var3) {
      this(var1, var2, var3, (String)null);
   }

   private an(g var1, Collection var2, Class var3, String var4) {
      this.a = var1;
      this.d = var2;
      this.b = var3;
      this.c = var4;
   }

   an(g var1, Collection var2, String var3) {
      this(var1, var2, (Class)null, var3);
   }

   public bb a(int var1) {
      this.a.e();
      return this.a.a(this.b, this.c, this.d.getUncheckedRow(var1));
   }

   @Deprecated
   public bb a(int var1, bb var2) {
      throw new UnsupportedOperationException("This method is not supported by 'RealmResults' or 'OrderedRealmCollectionSnapshot'.");
   }

   public boolean a() {
      return this.d.isValid();
   }

   @Deprecated
   public boolean a(bb var1) {
      throw new UnsupportedOperationException("This method is not supported by 'RealmResults' or 'OrderedRealmCollectionSnapshot'.");
   }

   // $FF: synthetic method
   @Deprecated
   public void add(int var1, Object var2) {
      this.b(var1, (bb)var2);
   }

   // $FF: synthetic method
   @Deprecated
   public boolean add(Object var1) {
      return this.a((bb)var1);
   }

   @Deprecated
   public boolean addAll(int var1, java.util.Collection var2) {
      throw new UnsupportedOperationException("This method is not supported by 'RealmResults' or 'OrderedRealmCollectionSnapshot'.");
   }

   @Deprecated
   public boolean addAll(java.util.Collection var1) {
      throw new UnsupportedOperationException("This method is not supported by 'RealmResults' or 'OrderedRealmCollectionSnapshot'.");
   }

   @Deprecated
   public bb b(int var1) {
      throw new UnsupportedOperationException("This method is not supported by 'RealmResults' or 'OrderedRealmCollectionSnapshot'.");
   }

   @Deprecated
   public void b(int var1, bb var2) {
      throw new UnsupportedOperationException("This method is not supported by 'RealmResults' or 'OrderedRealmCollectionSnapshot'.");
   }

   public boolean b() {
      this.a.e();
      boolean var1;
      if(this.size() > 0) {
         this.d.clear();
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   @Deprecated
   public void clear() {
      throw new UnsupportedOperationException("This method is not supported by 'RealmResults' or 'OrderedRealmCollectionSnapshot'.");
   }

   public boolean contains(Object var1) {
      boolean var2;
      if(this.c()) {
         if(var1 instanceof io.realm.internal.l && ((io.realm.internal.l)var1).s_().b() == io.realm.internal.e.a) {
            var2 = false;
            return var2;
         }

         Iterator var3 = this.iterator();

         while(var3.hasNext()) {
            if(((bb)var3.next()).equals(var1)) {
               var2 = true;
               return var2;
            }
         }
      }

      var2 = false;
      return var2;
   }

   // $FF: synthetic method
   public Object get(int var1) {
      return this.a(var1);
   }

   public Iterator iterator() {
      return new an.a();
   }

   public ListIterator listIterator() {
      return new an.b(0);
   }

   public ListIterator listIterator(int var1) {
      return new an.b(var1);
   }

   // $FF: synthetic method
   @Deprecated
   public Object remove(int var1) {
      return this.b(var1);
   }

   @Deprecated
   public boolean remove(Object var1) {
      throw new UnsupportedOperationException("This method is not supported by 'RealmResults' or 'OrderedRealmCollectionSnapshot'.");
   }

   @Deprecated
   public boolean removeAll(java.util.Collection var1) {
      throw new UnsupportedOperationException("This method is not supported by 'RealmResults' or 'OrderedRealmCollectionSnapshot'.");
   }

   @Deprecated
   public boolean retainAll(java.util.Collection var1) {
      throw new UnsupportedOperationException("This method is not supported by 'RealmResults' or 'OrderedRealmCollectionSnapshot'.");
   }

   // $FF: synthetic method
   @Deprecated
   public Object set(int var1, Object var2) {
      return this.a(var1, (bb)var2);
   }

   public int size() {
      int var1;
      if(this.c()) {
         long var2 = this.d.size();
         if(var2 > 2147483647L) {
            var1 = Integer.MAX_VALUE;
         } else {
            var1 = (int)var2;
         }
      } else {
         var1 = 0;
      }

      return var1;
   }

   private class a extends Collection.d {
      a() {
         super(an.this.d);
      }

      protected bb a(UncheckedRow var1) {
         return an.this.a.a(an.this.b, an.this.c, var1);
      }

      // $FF: synthetic method
      protected Object b(UncheckedRow var1) {
         return this.a(var1);
      }
   }

   private class b extends Collection.e {
      b(int var2) {
         super(an.this.d, var2);
      }

      protected bb a(UncheckedRow var1) {
         return an.this.a.a(an.this.b, an.this.c, var1);
      }

      // $FF: synthetic method
      protected Object b(UncheckedRow var1) {
         return this.a(var1);
      }
   }
}
