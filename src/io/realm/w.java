package io.realm;

import io.realm.exceptions.RealmException;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.SharedRealm;
import io.realm.internal.Table;
import io.realm.log.RealmLog;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class w extends co.uk.getmondo.payments.a.a.b implements io.realm.internal.l, x {
   private static final OsObjectSchemaInfo d = m();
   private static final List e;
   private w.a b;
   private au c;

   static {
      ArrayList var0 = new ArrayList();
      var0.add("id");
      var0.add("reference");
      var0.add("accountNumber");
      var0.add("sortCode");
      var0.add("customerName");
      e = Collections.unmodifiableList(var0);
   }

   w() {
      this.c.f();
   }

   public static long a(av var0, co.uk.getmondo.payments.a.a.b var1, Map var2) {
      long var5;
      if(var1 instanceof io.realm.internal.l && ((io.realm.internal.l)var1).s_().a() != null && ((io.realm.internal.l)var1).s_().a().g().equals(var0.g())) {
         var5 = ((io.realm.internal.l)var1).s_().b().c();
      } else {
         Table var9 = var0.c(co.uk.getmondo.payments.a.a.b.class);
         long var7 = var9.getNativePtr();
         w.a var11 = (w.a)var0.f.c(co.uk.getmondo.payments.a.a.b.class);
         long var3 = var9.d();
         String var10 = ((x)var1).e();
         if(var10 == null) {
            var3 = Table.nativeFindFirstNull(var7, var3);
         } else {
            var3 = Table.nativeFindFirstString(var7, var3, var10);
         }

         if(var3 == -1L) {
            var3 = OsObject.b(var9, var10);
         } else {
            Table.a((Object)var10);
         }

         var2.put(var1, Long.valueOf(var3));
         String var13 = ((x)var1).f();
         if(var13 != null) {
            Table.nativeSetString(var7, var11.b, var3, var13, false);
         }

         var13 = ((x)var1).g();
         if(var13 != null) {
            Table.nativeSetString(var7, var11.c, var3, var13, false);
         }

         var13 = ((x)var1).h();
         if(var13 != null) {
            Table.nativeSetString(var7, var11.d, var3, var13, false);
         }

         String var12 = ((x)var1).i();
         var5 = var3;
         if(var12 != null) {
            Table.nativeSetString(var7, var11.e, var3, var12, false);
            var5 = var3;
         }
      }

      return var5;
   }

   public static co.uk.getmondo.payments.a.a.b a(co.uk.getmondo.payments.a.a.b var0, int var1, int var2, Map var3) {
      if(var1 <= var2 && var0 != null) {
         io.realm.internal.l.a var4 = (io.realm.internal.l.a)var3.get(var0);
         co.uk.getmondo.payments.a.a.b var6;
         if(var4 == null) {
            co.uk.getmondo.payments.a.a.b var7 = new co.uk.getmondo.payments.a.a.b();
            var3.put(var0, new io.realm.internal.l.a(var1, var7));
            var6 = var7;
         } else {
            if(var1 >= var4.a) {
               var0 = (co.uk.getmondo.payments.a.a.b)var4.b;
               return var0;
            }

            var6 = (co.uk.getmondo.payments.a.a.b)var4.b;
            var4.a = var1;
         }

         x var8 = (x)var6;
         x var5 = (x)var0;
         var8.b(var5.e());
         var8.c(var5.f());
         var8.d(var5.g());
         var8.e(var5.h());
         var8.f(var5.i());
         var0 = var6;
      } else {
         var0 = null;
      }

      return var0;
   }

   static co.uk.getmondo.payments.a.a.b a(av var0, co.uk.getmondo.payments.a.a.b var1, co.uk.getmondo.payments.a.a.b var2, Map var3) {
      x var4 = (x)var1;
      x var5 = (x)var2;
      var4.c(var5.f());
      var4.d(var5.g());
      var4.e(var5.h());
      var4.f(var5.i());
      return var1;
   }

   public static co.uk.getmondo.payments.a.a.b a(av var0, co.uk.getmondo.payments.a.a.b var1, boolean var2, Map var3) {
      if(var1 instanceof io.realm.internal.l && ((io.realm.internal.l)var1).s_().a() != null && ((io.realm.internal.l)var1).s_().a().c != var0.c) {
         throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
      } else {
         if(!(var1 instanceof io.realm.internal.l) || ((io.realm.internal.l)var1).s_().a() == null || !((io.realm.internal.l)var1).s_().a().g().equals(var0.g())) {
            g.b var8 = (g.b)g.g.get();
            io.realm.internal.l var7 = (io.realm.internal.l)var3.get(var1);
            if(var7 != null) {
               var1 = (co.uk.getmondo.payments.a.a.b)var7;
            } else {
               boolean var4;
               w var13;
               if(var2) {
                  Table var12 = var0.c(co.uk.getmondo.payments.a.a.b.class);
                  long var5 = var12.d();
                  String var9 = ((x)var1).e();
                  if(var9 == null) {
                     var5 = var12.k(var5);
                  } else {
                     var5 = var12.a(var5, var9);
                  }

                  if(var5 != -1L) {
                     try {
                        var8.a(var0, var12.f(var5), var0.f.c(co.uk.getmondo.payments.a.a.b.class), false, Collections.emptyList());
                        var13 = new w();
                        var3.put(var1, (io.realm.internal.l)var13);
                     } finally {
                        var8.f();
                     }

                     var4 = var2;
                  } else {
                     var4 = false;
                     var13 = null;
                  }
               } else {
                  var4 = var2;
                  var13 = null;
               }

               if(var4) {
                  var1 = a(var0, var13, var1, var3);
               } else {
                  var1 = b(var0, var1, var2, var3);
               }
            }
         }

         return var1;
      }
   }

   public static w.a a(SharedRealm var0, boolean var1) {
      if(!var0.a("class_FpsSchemaData")) {
         throw new RealmMigrationNeededException(var0.h(), "The 'FpsSchemaData' class is missing from the schema for this Realm.");
      } else {
         Table var7 = var0.b("class_FpsSchemaData");
         long var4 = var7.c();
         if(var4 != 5L) {
            if(var4 < 5L) {
               throw new RealmMigrationNeededException(var0.h(), "Field count is less than expected - expected 5 but was " + var4);
            }

            if(!var1) {
               throw new RealmMigrationNeededException(var0.h(), "Field count is more than expected - expected 5 but was " + var4);
            }

            RealmLog.a("Field count is more than expected - expected 5 but was %1$d", new Object[]{Long.valueOf(var4)});
         }

         HashMap var8 = new HashMap();

         for(long var2 = 0L; var2 < var4; ++var2) {
            var8.put(var7.b(var2), var7.c(var2));
         }

         w.a var6 = new w.a(var0, var7);
         if(!var7.e()) {
            throw new RealmMigrationNeededException(var0.h(), "Primary key not defined for field 'id' in existing Realm file. @PrimaryKey was added.");
         } else if(var7.d() != var6.a) {
            throw new RealmMigrationNeededException(var0.h(), "Primary Key annotation definition was changed, from field " + var7.b(var7.d()) + " to field id");
         } else if(!var8.containsKey("id")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'id' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var8.get("id") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'id' in existing Realm file.");
         } else if(!var7.a(var6.a)) {
            throw new RealmMigrationNeededException(var0.h(), "@PrimaryKey field 'id' does not support null values in the existing Realm file. Migrate using RealmObjectSchema.setNullable(), or mark the field as @Required.");
         } else if(!var7.j(var7.a("id"))) {
            throw new RealmMigrationNeededException(var0.h(), "Index not defined for field 'id' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex().");
         } else if(!var8.containsKey("reference")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'reference' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var8.get("reference") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'reference' in existing Realm file.");
         } else if(!var7.a(var6.b)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'reference' is required. Either set @Required to field 'reference' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var8.containsKey("accountNumber")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'accountNumber' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var8.get("accountNumber") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'accountNumber' in existing Realm file.");
         } else if(!var7.a(var6.c)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'accountNumber' is required. Either set @Required to field 'accountNumber' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var8.containsKey("sortCode")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'sortCode' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var8.get("sortCode") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'sortCode' in existing Realm file.");
         } else if(!var7.a(var6.d)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'sortCode' is required. Either set @Required to field 'sortCode' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var8.containsKey("customerName")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'customerName' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var8.get("customerName") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'customerName' in existing Realm file.");
         } else if(!var7.a(var6.e)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'customerName' is required. Either set @Required to field 'customerName' or migrate using RealmObjectSchema.setNullable().");
         } else {
            return var6;
         }
      }
   }

   public static void a(av var0, Iterator var1, Map var2) {
      Table var12 = var0.c(co.uk.getmondo.payments.a.a.b.class);
      long var9 = var12.getNativePtr();
      w.a var11 = (w.a)var0.f.c(co.uk.getmondo.payments.a.a.b.class);
      long var7 = var12.d();

      while(true) {
         while(true) {
            co.uk.getmondo.payments.a.a.b var13;
            do {
               if(!var1.hasNext()) {
                  return;
               }

               var13 = (co.uk.getmondo.payments.a.a.b)var1.next();
            } while(var2.containsKey(var13));

            if(var13 instanceof io.realm.internal.l && ((io.realm.internal.l)var13).s_().a() != null && ((io.realm.internal.l)var13).s_().a().g().equals(var0.g())) {
               var2.put(var13, Long.valueOf(((io.realm.internal.l)var13).s_().b().c()));
            } else {
               String var14 = ((x)var13).e();
               long var3;
               if(var14 == null) {
                  var3 = Table.nativeFindFirstNull(var9, var7);
               } else {
                  var3 = Table.nativeFindFirstString(var9, var7, var14);
               }

               long var5 = var3;
               if(var3 == -1L) {
                  var5 = OsObject.b(var12, var14);
               }

               var2.put(var13, Long.valueOf(var5));
               var14 = ((x)var13).f();
               if(var14 != null) {
                  Table.nativeSetString(var9, var11.b, var5, var14, false);
               } else {
                  Table.nativeSetNull(var9, var11.b, var5, false);
               }

               var14 = ((x)var13).g();
               if(var14 != null) {
                  Table.nativeSetString(var9, var11.c, var5, var14, false);
               } else {
                  Table.nativeSetNull(var9, var11.c, var5, false);
               }

               var14 = ((x)var13).h();
               if(var14 != null) {
                  Table.nativeSetString(var9, var11.d, var5, var14, false);
               } else {
                  Table.nativeSetNull(var9, var11.d, var5, false);
               }

               String var15 = ((x)var13).i();
               if(var15 != null) {
                  Table.nativeSetString(var9, var11.e, var5, var15, false);
               } else {
                  Table.nativeSetNull(var9, var11.e, var5, false);
               }
            }
         }
      }
   }

   public static long b(av var0, co.uk.getmondo.payments.a.a.b var1, Map var2) {
      long var5;
      if(var1 instanceof io.realm.internal.l && ((io.realm.internal.l)var1).s_().a() != null && ((io.realm.internal.l)var1).s_().a().g().equals(var0.g())) {
         var5 = ((io.realm.internal.l)var1).s_().b().c();
      } else {
         Table var9 = var0.c(co.uk.getmondo.payments.a.a.b.class);
         long var7 = var9.getNativePtr();
         w.a var11 = (w.a)var0.f.c(co.uk.getmondo.payments.a.a.b.class);
         long var3 = var9.d();
         String var10 = ((x)var1).e();
         if(var10 == null) {
            var3 = Table.nativeFindFirstNull(var7, var3);
         } else {
            var3 = Table.nativeFindFirstString(var7, var3, var10);
         }

         var5 = var3;
         if(var3 == -1L) {
            var5 = OsObject.b(var9, var10);
         }

         var2.put(var1, Long.valueOf(var5));
         String var13 = ((x)var1).f();
         if(var13 != null) {
            Table.nativeSetString(var7, var11.b, var5, var13, false);
         } else {
            Table.nativeSetNull(var7, var11.b, var5, false);
         }

         var13 = ((x)var1).g();
         if(var13 != null) {
            Table.nativeSetString(var7, var11.c, var5, var13, false);
         } else {
            Table.nativeSetNull(var7, var11.c, var5, false);
         }

         var13 = ((x)var1).h();
         if(var13 != null) {
            Table.nativeSetString(var7, var11.d, var5, var13, false);
         } else {
            Table.nativeSetNull(var7, var11.d, var5, false);
         }

         String var12 = ((x)var1).i();
         if(var12 != null) {
            Table.nativeSetString(var7, var11.e, var5, var12, false);
         } else {
            Table.nativeSetNull(var7, var11.e, var5, false);
         }
      }

      return var5;
   }

   public static co.uk.getmondo.payments.a.a.b b(av var0, co.uk.getmondo.payments.a.a.b var1, boolean var2, Map var3) {
      io.realm.internal.l var4 = (io.realm.internal.l)var3.get(var1);
      co.uk.getmondo.payments.a.a.b var5;
      if(var4 != null) {
         var5 = (co.uk.getmondo.payments.a.a.b)var4;
      } else {
         var5 = (co.uk.getmondo.payments.a.a.b)var0.a(co.uk.getmondo.payments.a.a.b.class, ((x)var1).e(), false, Collections.emptyList());
         var3.put(var1, (io.realm.internal.l)var5);
         x var6 = (x)var1;
         x var7 = (x)var5;
         var7.c(var6.f());
         var7.d(var6.g());
         var7.e(var6.h());
         var7.f(var6.i());
      }

      return var5;
   }

   public static OsObjectSchemaInfo j() {
      return d;
   }

   public static String l() {
      return "class_FpsSchemaData";
   }

   private static OsObjectSchemaInfo m() {
      OsObjectSchemaInfo.a var0 = new OsObjectSchemaInfo.a("FpsSchemaData");
      var0.a("id", RealmFieldType.STRING, true, true, false);
      var0.a("reference", RealmFieldType.STRING, false, false, false);
      var0.a("accountNumber", RealmFieldType.STRING, false, false, false);
      var0.a("sortCode", RealmFieldType.STRING, false, false, false);
      var0.a("customerName", RealmFieldType.STRING, false, false, false);
      return var0.a();
   }

   public void b(String var1) {
      if(!this.c.e()) {
         this.c.a().e();
         throw new RealmException("Primary key field 'id' cannot be changed after object was created.");
      }
   }

   public void c(String var1) {
      if(this.c.e()) {
         if(this.c.c()) {
            io.realm.internal.n var2 = this.c.b();
            if(var1 == null) {
               var2.b().a(this.b.b, var2.c(), true);
            } else {
               var2.b().a(this.b.b, var2.c(), var1, true);
            }
         }
      } else {
         this.c.a().e();
         if(var1 == null) {
            this.c.b().c(this.b.b);
         } else {
            this.c.b().a(this.b.b, var1);
         }
      }

   }

   public void d(String var1) {
      if(this.c.e()) {
         if(this.c.c()) {
            io.realm.internal.n var2 = this.c.b();
            if(var1 == null) {
               var2.b().a(this.b.c, var2.c(), true);
            } else {
               var2.b().a(this.b.c, var2.c(), var1, true);
            }
         }
      } else {
         this.c.a().e();
         if(var1 == null) {
            this.c.b().c(this.b.c);
         } else {
            this.c.b().a(this.b.c, var1);
         }
      }

   }

   public String e() {
      this.c.a().e();
      return this.c.b().k(this.b.a);
   }

   public void e(String var1) {
      if(this.c.e()) {
         if(this.c.c()) {
            io.realm.internal.n var2 = this.c.b();
            if(var1 == null) {
               var2.b().a(this.b.d, var2.c(), true);
            } else {
               var2.b().a(this.b.d, var2.c(), var1, true);
            }
         }
      } else {
         this.c.a().e();
         if(var1 == null) {
            this.c.b().c(this.b.d);
         } else {
            this.c.b().a(this.b.d, var1);
         }
      }

   }

   public String f() {
      this.c.a().e();
      return this.c.b().k(this.b.b);
   }

   public void f(String var1) {
      if(this.c.e()) {
         if(this.c.c()) {
            io.realm.internal.n var2 = this.c.b();
            if(var1 == null) {
               var2.b().a(this.b.e, var2.c(), true);
            } else {
               var2.b().a(this.b.e, var2.c(), var1, true);
            }
         }
      } else {
         this.c.a().e();
         if(var1 == null) {
            this.c.b().c(this.b.e);
         } else {
            this.c.b().a(this.b.e, var1);
         }
      }

   }

   public String g() {
      this.c.a().e();
      return this.c.b().k(this.b.c);
   }

   public String h() {
      this.c.a().e();
      return this.c.b().k(this.b.d);
   }

   public String i() {
      this.c.a().e();
      return this.c.b().k(this.b.e);
   }

   public au s_() {
      return this.c;
   }

   public String toString() {
      String var1;
      if(!bc.b(this)) {
         var1 = "Invalid object";
      } else {
         StringBuilder var2 = new StringBuilder("FpsSchemaData = proxy[");
         var2.append("{id:");
         if(this.e() != null) {
            var1 = this.e();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{reference:");
         if(this.f() != null) {
            var1 = this.f();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{accountNumber:");
         if(this.g() != null) {
            var1 = this.g();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{sortCode:");
         if(this.h() != null) {
            var1 = this.h();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{customerName:");
         if(this.i() != null) {
            var1 = this.i();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append("]");
         var1 = var2.toString();
      }

      return var1;
   }

   public void u_() {
      if(this.c == null) {
         g.b var1 = (g.b)g.g.get();
         this.b = (w.a)var1.c();
         this.c = new au(this);
         this.c.a(var1.a());
         this.c.a(var1.b());
         this.c.a(var1.d());
         this.c.a(var1.e());
      }

   }

   static final class a extends io.realm.internal.c {
      long a;
      long b;
      long c;
      long d;
      long e;

      a(SharedRealm var1, Table var2) {
         super(5);
         this.a = this.a(var2, "id", RealmFieldType.STRING);
         this.b = this.a(var2, "reference", RealmFieldType.STRING);
         this.c = this.a(var2, "accountNumber", RealmFieldType.STRING);
         this.d = this.a(var2, "sortCode", RealmFieldType.STRING);
         this.e = this.a(var2, "customerName", RealmFieldType.STRING);
      }

      a(io.realm.internal.c var1, boolean var2) {
         super(var1, var2);
         this.a(var1, this);
      }

      protected final io.realm.internal.c a(boolean var1) {
         return new w.a(this, var1);
      }

      protected final void a(io.realm.internal.c var1, io.realm.internal.c var2) {
         w.a var3 = (w.a)var1;
         w.a var4 = (w.a)var2;
         var4.a = var3.a;
         var4.b = var3.b;
         var4.c = var3.c;
         var4.d = var3.d;
         var4.e = var3.e;
      }
   }
}
