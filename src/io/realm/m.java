package io.realm;

import io.realm.exceptions.RealmException;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.SharedRealm;
import io.realm.internal.Table;
import io.realm.log.RealmLog;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class m extends co.uk.getmondo.payments.a.a.a implements io.realm.internal.l, n {
   private static final OsObjectSchemaInfo d = r();
   private static final List e;
   private m.a b;
   private au c;

   static {
      ArrayList var0 = new ArrayList();
      var0.add("id");
      var0.add("accountId");
      var0.add("payerSortCode");
      var0.add("payerAccountNumber");
      var0.add("payerName");
      var0.add("serviceUserNumber");
      var0.add("serviceUserName");
      var0.add("reference");
      var0.add("active");
      var0.add("_created");
      var0.add("_lastCollected");
      e = Collections.unmodifiableList(var0);
   }

   m() {
      this.c.f();
   }

   public static long a(av var0, co.uk.getmondo.payments.a.a.a var1, Map var2) {
      long var5;
      if(var1 instanceof io.realm.internal.l && ((io.realm.internal.l)var1).s_().a() != null && ((io.realm.internal.l)var1).s_().a().g().equals(var0.g())) {
         var5 = ((io.realm.internal.l)var1).s_().b().c();
      } else {
         Table var9 = var0.c(co.uk.getmondo.payments.a.a.a.class);
         long var7 = var9.getNativePtr();
         m.a var11 = (m.a)var0.f.c(co.uk.getmondo.payments.a.a.a.class);
         long var3 = var9.d();
         String var10 = ((n)var1).e();
         if(var10 == null) {
            var3 = Table.nativeFindFirstNull(var7, var3);
         } else {
            var3 = Table.nativeFindFirstString(var7, var3, var10);
         }

         if(var3 == -1L) {
            var3 = OsObject.b(var9, var10);
         } else {
            Table.a((Object)var10);
         }

         var2.put(var1, Long.valueOf(var3));
         String var13 = ((n)var1).f();
         if(var13 != null) {
            Table.nativeSetString(var7, var11.b, var3, var13, false);
         }

         var13 = ((n)var1).g();
         if(var13 != null) {
            Table.nativeSetString(var7, var11.c, var3, var13, false);
         }

         var13 = ((n)var1).h();
         if(var13 != null) {
            Table.nativeSetString(var7, var11.d, var3, var13, false);
         }

         var13 = ((n)var1).i();
         if(var13 != null) {
            Table.nativeSetString(var7, var11.e, var3, var13, false);
         }

         var13 = ((n)var1).j();
         if(var13 != null) {
            Table.nativeSetString(var7, var11.f, var3, var13, false);
         }

         var13 = ((n)var1).t_();
         if(var13 != null) {
            Table.nativeSetString(var7, var11.g, var3, var13, false);
         }

         var13 = ((n)var1).l();
         if(var13 != null) {
            Table.nativeSetString(var7, var11.h, var3, var13, false);
         }

         Table.nativeSetBoolean(var7, var11.i, var3, ((n)var1).m(), false);
         var13 = ((n)var1).n();
         if(var13 != null) {
            Table.nativeSetString(var7, var11.j, var3, var13, false);
         }

         String var12 = ((n)var1).o();
         var5 = var3;
         if(var12 != null) {
            Table.nativeSetString(var7, var11.k, var3, var12, false);
            var5 = var3;
         }
      }

      return var5;
   }

   public static co.uk.getmondo.payments.a.a.a a(co.uk.getmondo.payments.a.a.a var0, int var1, int var2, Map var3) {
      if(var1 <= var2 && var0 != null) {
         io.realm.internal.l.a var4 = (io.realm.internal.l.a)var3.get(var0);
         co.uk.getmondo.payments.a.a.a var6;
         if(var4 == null) {
            co.uk.getmondo.payments.a.a.a var7 = new co.uk.getmondo.payments.a.a.a();
            var3.put(var0, new io.realm.internal.l.a(var1, var7));
            var6 = var7;
         } else {
            if(var1 >= var4.a) {
               var0 = (co.uk.getmondo.payments.a.a.a)var4.b;
               return var0;
            }

            var6 = (co.uk.getmondo.payments.a.a.a)var4.b;
            var4.a = var1;
         }

         n var8 = (n)var6;
         n var5 = (n)var0;
         var8.a(var5.e());
         var8.b(var5.f());
         var8.c(var5.g());
         var8.d(var5.h());
         var8.e(var5.i());
         var8.f(var5.j());
         var8.g(var5.t_());
         var8.h(var5.l());
         var8.a(var5.m());
         var8.i(var5.n());
         var8.j(var5.o());
         var0 = var6;
      } else {
         var0 = null;
      }

      return var0;
   }

   static co.uk.getmondo.payments.a.a.a a(av var0, co.uk.getmondo.payments.a.a.a var1, co.uk.getmondo.payments.a.a.a var2, Map var3) {
      n var4 = (n)var1;
      n var5 = (n)var2;
      var4.b(var5.f());
      var4.c(var5.g());
      var4.d(var5.h());
      var4.e(var5.i());
      var4.f(var5.j());
      var4.g(var5.t_());
      var4.h(var5.l());
      var4.a(var5.m());
      var4.i(var5.n());
      var4.j(var5.o());
      return var1;
   }

   public static co.uk.getmondo.payments.a.a.a a(av var0, co.uk.getmondo.payments.a.a.a var1, boolean var2, Map var3) {
      if(var1 instanceof io.realm.internal.l && ((io.realm.internal.l)var1).s_().a() != null && ((io.realm.internal.l)var1).s_().a().c != var0.c) {
         throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
      } else {
         if(!(var1 instanceof io.realm.internal.l) || ((io.realm.internal.l)var1).s_().a() == null || !((io.realm.internal.l)var1).s_().a().g().equals(var0.g())) {
            g.b var8 = (g.b)g.g.get();
            io.realm.internal.l var7 = (io.realm.internal.l)var3.get(var1);
            if(var7 != null) {
               var1 = (co.uk.getmondo.payments.a.a.a)var7;
            } else {
               boolean var4;
               m var13;
               if(var2) {
                  Table var12 = var0.c(co.uk.getmondo.payments.a.a.a.class);
                  long var5 = var12.d();
                  String var9 = ((n)var1).e();
                  if(var9 == null) {
                     var5 = var12.k(var5);
                  } else {
                     var5 = var12.a(var5, var9);
                  }

                  if(var5 != -1L) {
                     try {
                        var8.a(var0, var12.f(var5), var0.f.c(co.uk.getmondo.payments.a.a.a.class), false, Collections.emptyList());
                        var13 = new m();
                        var3.put(var1, (io.realm.internal.l)var13);
                     } finally {
                        var8.f();
                     }

                     var4 = var2;
                  } else {
                     var4 = false;
                     var13 = null;
                  }
               } else {
                  var4 = var2;
                  var13 = null;
               }

               if(var4) {
                  var1 = a(var0, var13, var1, var3);
               } else {
                  var1 = b(var0, var1, var2, var3);
               }
            }
         }

         return var1;
      }
   }

   public static m.a a(SharedRealm var0, boolean var1) {
      if(!var0.a("class_DirectDebit")) {
         throw new RealmMigrationNeededException(var0.h(), "The 'DirectDebit' class is missing from the schema for this Realm.");
      } else {
         Table var8 = var0.b("class_DirectDebit");
         long var4 = var8.c();
         if(var4 != 11L) {
            if(var4 < 11L) {
               throw new RealmMigrationNeededException(var0.h(), "Field count is less than expected - expected 11 but was " + var4);
            }

            if(!var1) {
               throw new RealmMigrationNeededException(var0.h(), "Field count is more than expected - expected 11 but was " + var4);
            }

            RealmLog.a("Field count is more than expected - expected 11 but was %1$d", new Object[]{Long.valueOf(var4)});
         }

         HashMap var6 = new HashMap();

         for(long var2 = 0L; var2 < var4; ++var2) {
            var6.put(var8.b(var2), var8.c(var2));
         }

         m.a var7 = new m.a(var0, var8);
         if(!var8.e()) {
            throw new RealmMigrationNeededException(var0.h(), "Primary key not defined for field 'id' in existing Realm file. @PrimaryKey was added.");
         } else if(var8.d() != var7.a) {
            throw new RealmMigrationNeededException(var0.h(), "Primary Key annotation definition was changed, from field " + var8.b(var8.d()) + " to field id");
         } else if(!var6.containsKey("id")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'id' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var6.get("id") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'id' in existing Realm file.");
         } else if(!var8.a(var7.a)) {
            throw new RealmMigrationNeededException(var0.h(), "@PrimaryKey field 'id' does not support null values in the existing Realm file. Migrate using RealmObjectSchema.setNullable(), or mark the field as @Required.");
         } else if(!var8.j(var8.a("id"))) {
            throw new RealmMigrationNeededException(var0.h(), "Index not defined for field 'id' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex().");
         } else if(!var6.containsKey("accountId")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'accountId' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var6.get("accountId") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'accountId' in existing Realm file.");
         } else if(!var8.a(var7.b)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'accountId' is required. Either set @Required to field 'accountId' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var6.containsKey("payerSortCode")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'payerSortCode' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var6.get("payerSortCode") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'payerSortCode' in existing Realm file.");
         } else if(!var8.a(var7.c)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'payerSortCode' is required. Either set @Required to field 'payerSortCode' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var6.containsKey("payerAccountNumber")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'payerAccountNumber' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var6.get("payerAccountNumber") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'payerAccountNumber' in existing Realm file.");
         } else if(!var8.a(var7.d)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'payerAccountNumber' is required. Either set @Required to field 'payerAccountNumber' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var6.containsKey("payerName")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'payerName' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var6.get("payerName") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'payerName' in existing Realm file.");
         } else if(!var8.a(var7.e)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'payerName' is required. Either set @Required to field 'payerName' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var6.containsKey("serviceUserNumber")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'serviceUserNumber' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var6.get("serviceUserNumber") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'serviceUserNumber' in existing Realm file.");
         } else if(!var8.a(var7.f)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'serviceUserNumber' is required. Either set @Required to field 'serviceUserNumber' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var6.containsKey("serviceUserName")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'serviceUserName' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var6.get("serviceUserName") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'serviceUserName' in existing Realm file.");
         } else if(!var8.a(var7.g)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'serviceUserName' is required. Either set @Required to field 'serviceUserName' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var6.containsKey("reference")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'reference' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var6.get("reference") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'reference' in existing Realm file.");
         } else if(!var8.a(var7.h)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'reference' is required. Either set @Required to field 'reference' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var6.containsKey("active")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'active' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var6.get("active") != RealmFieldType.BOOLEAN) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'boolean' for field 'active' in existing Realm file.");
         } else if(var8.a(var7.i)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'active' does support null values in the existing Realm file. Use corresponding boxed type for field 'active' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var6.containsKey("_created")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field '_created' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var6.get("_created") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field '_created' in existing Realm file.");
         } else if(!var8.a(var7.j)) {
            throw new RealmMigrationNeededException(var0.h(), "Field '_created' is required. Either set @Required to field '_created' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var6.containsKey("_lastCollected")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field '_lastCollected' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var6.get("_lastCollected") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field '_lastCollected' in existing Realm file.");
         } else if(!var8.a(var7.k)) {
            throw new RealmMigrationNeededException(var0.h(), "Field '_lastCollected' is required. Either set @Required to field '_lastCollected' or migrate using RealmObjectSchema.setNullable().");
         } else {
            return var7;
         }
      }
   }

   public static void a(av var0, Iterator var1, Map var2) {
      Table var12 = var0.c(co.uk.getmondo.payments.a.a.a.class);
      long var7 = var12.getNativePtr();
      m.a var11 = (m.a)var0.f.c(co.uk.getmondo.payments.a.a.a.class);
      long var9 = var12.d();

      while(true) {
         while(true) {
            co.uk.getmondo.payments.a.a.a var13;
            do {
               if(!var1.hasNext()) {
                  return;
               }

               var13 = (co.uk.getmondo.payments.a.a.a)var1.next();
            } while(var2.containsKey(var13));

            if(var13 instanceof io.realm.internal.l && ((io.realm.internal.l)var13).s_().a() != null && ((io.realm.internal.l)var13).s_().a().g().equals(var0.g())) {
               var2.put(var13, Long.valueOf(((io.realm.internal.l)var13).s_().b().c()));
            } else {
               String var14 = ((n)var13).e();
               long var3;
               if(var14 == null) {
                  var3 = Table.nativeFindFirstNull(var7, var9);
               } else {
                  var3 = Table.nativeFindFirstString(var7, var9, var14);
               }

               long var5 = var3;
               if(var3 == -1L) {
                  var5 = OsObject.b(var12, var14);
               }

               var2.put(var13, Long.valueOf(var5));
               var14 = ((n)var13).f();
               if(var14 != null) {
                  Table.nativeSetString(var7, var11.b, var5, var14, false);
               } else {
                  Table.nativeSetNull(var7, var11.b, var5, false);
               }

               var14 = ((n)var13).g();
               if(var14 != null) {
                  Table.nativeSetString(var7, var11.c, var5, var14, false);
               } else {
                  Table.nativeSetNull(var7, var11.c, var5, false);
               }

               var14 = ((n)var13).h();
               if(var14 != null) {
                  Table.nativeSetString(var7, var11.d, var5, var14, false);
               } else {
                  Table.nativeSetNull(var7, var11.d, var5, false);
               }

               var14 = ((n)var13).i();
               if(var14 != null) {
                  Table.nativeSetString(var7, var11.e, var5, var14, false);
               } else {
                  Table.nativeSetNull(var7, var11.e, var5, false);
               }

               var14 = ((n)var13).j();
               if(var14 != null) {
                  Table.nativeSetString(var7, var11.f, var5, var14, false);
               } else {
                  Table.nativeSetNull(var7, var11.f, var5, false);
               }

               var14 = ((n)var13).t_();
               if(var14 != null) {
                  Table.nativeSetString(var7, var11.g, var5, var14, false);
               } else {
                  Table.nativeSetNull(var7, var11.g, var5, false);
               }

               var14 = ((n)var13).l();
               if(var14 != null) {
                  Table.nativeSetString(var7, var11.h, var5, var14, false);
               } else {
                  Table.nativeSetNull(var7, var11.h, var5, false);
               }

               Table.nativeSetBoolean(var7, var11.i, var5, ((n)var13).m(), false);
               var14 = ((n)var13).n();
               if(var14 != null) {
                  Table.nativeSetString(var7, var11.j, var5, var14, false);
               } else {
                  Table.nativeSetNull(var7, var11.j, var5, false);
               }

               String var15 = ((n)var13).o();
               if(var15 != null) {
                  Table.nativeSetString(var7, var11.k, var5, var15, false);
               } else {
                  Table.nativeSetNull(var7, var11.k, var5, false);
               }
            }
         }
      }
   }

   public static long b(av var0, co.uk.getmondo.payments.a.a.a var1, Map var2) {
      long var5;
      if(var1 instanceof io.realm.internal.l && ((io.realm.internal.l)var1).s_().a() != null && ((io.realm.internal.l)var1).s_().a().g().equals(var0.g())) {
         var5 = ((io.realm.internal.l)var1).s_().b().c();
      } else {
         Table var9 = var0.c(co.uk.getmondo.payments.a.a.a.class);
         long var7 = var9.getNativePtr();
         m.a var11 = (m.a)var0.f.c(co.uk.getmondo.payments.a.a.a.class);
         long var3 = var9.d();
         String var10 = ((n)var1).e();
         if(var10 == null) {
            var3 = Table.nativeFindFirstNull(var7, var3);
         } else {
            var3 = Table.nativeFindFirstString(var7, var3, var10);
         }

         var5 = var3;
         if(var3 == -1L) {
            var5 = OsObject.b(var9, var10);
         }

         var2.put(var1, Long.valueOf(var5));
         String var13 = ((n)var1).f();
         if(var13 != null) {
            Table.nativeSetString(var7, var11.b, var5, var13, false);
         } else {
            Table.nativeSetNull(var7, var11.b, var5, false);
         }

         var13 = ((n)var1).g();
         if(var13 != null) {
            Table.nativeSetString(var7, var11.c, var5, var13, false);
         } else {
            Table.nativeSetNull(var7, var11.c, var5, false);
         }

         var13 = ((n)var1).h();
         if(var13 != null) {
            Table.nativeSetString(var7, var11.d, var5, var13, false);
         } else {
            Table.nativeSetNull(var7, var11.d, var5, false);
         }

         var13 = ((n)var1).i();
         if(var13 != null) {
            Table.nativeSetString(var7, var11.e, var5, var13, false);
         } else {
            Table.nativeSetNull(var7, var11.e, var5, false);
         }

         var13 = ((n)var1).j();
         if(var13 != null) {
            Table.nativeSetString(var7, var11.f, var5, var13, false);
         } else {
            Table.nativeSetNull(var7, var11.f, var5, false);
         }

         var13 = ((n)var1).t_();
         if(var13 != null) {
            Table.nativeSetString(var7, var11.g, var5, var13, false);
         } else {
            Table.nativeSetNull(var7, var11.g, var5, false);
         }

         var13 = ((n)var1).l();
         if(var13 != null) {
            Table.nativeSetString(var7, var11.h, var5, var13, false);
         } else {
            Table.nativeSetNull(var7, var11.h, var5, false);
         }

         Table.nativeSetBoolean(var7, var11.i, var5, ((n)var1).m(), false);
         var13 = ((n)var1).n();
         if(var13 != null) {
            Table.nativeSetString(var7, var11.j, var5, var13, false);
         } else {
            Table.nativeSetNull(var7, var11.j, var5, false);
         }

         String var12 = ((n)var1).o();
         if(var12 != null) {
            Table.nativeSetString(var7, var11.k, var5, var12, false);
         } else {
            Table.nativeSetNull(var7, var11.k, var5, false);
         }
      }

      return var5;
   }

   public static co.uk.getmondo.payments.a.a.a b(av var0, co.uk.getmondo.payments.a.a.a var1, boolean var2, Map var3) {
      io.realm.internal.l var4 = (io.realm.internal.l)var3.get(var1);
      co.uk.getmondo.payments.a.a.a var5;
      if(var4 != null) {
         var5 = (co.uk.getmondo.payments.a.a.a)var4;
      } else {
         var5 = (co.uk.getmondo.payments.a.a.a)var0.a(co.uk.getmondo.payments.a.a.a.class, ((n)var1).e(), false, Collections.emptyList());
         var3.put(var1, (io.realm.internal.l)var5);
         n var7 = (n)var1;
         n var6 = (n)var5;
         var6.b(var7.f());
         var6.c(var7.g());
         var6.d(var7.h());
         var6.e(var7.i());
         var6.f(var7.j());
         var6.g(var7.t_());
         var6.h(var7.l());
         var6.a(var7.m());
         var6.i(var7.n());
         var6.j(var7.o());
      }

      return var5;
   }

   public static OsObjectSchemaInfo p() {
      return d;
   }

   public static String q() {
      return "class_DirectDebit";
   }

   private static OsObjectSchemaInfo r() {
      OsObjectSchemaInfo.a var0 = new OsObjectSchemaInfo.a("DirectDebit");
      var0.a("id", RealmFieldType.STRING, true, true, false);
      var0.a("accountId", RealmFieldType.STRING, false, false, false);
      var0.a("payerSortCode", RealmFieldType.STRING, false, false, false);
      var0.a("payerAccountNumber", RealmFieldType.STRING, false, false, false);
      var0.a("payerName", RealmFieldType.STRING, false, false, false);
      var0.a("serviceUserNumber", RealmFieldType.STRING, false, false, false);
      var0.a("serviceUserName", RealmFieldType.STRING, false, false, false);
      var0.a("reference", RealmFieldType.STRING, false, false, false);
      var0.a("active", RealmFieldType.BOOLEAN, false, false, true);
      var0.a("_created", RealmFieldType.STRING, false, false, false);
      var0.a("_lastCollected", RealmFieldType.STRING, false, false, false);
      return var0.a();
   }

   public void a(String var1) {
      if(!this.c.e()) {
         this.c.a().e();
         throw new RealmException("Primary key field 'id' cannot be changed after object was created.");
      }
   }

   public void a(boolean var1) {
      if(this.c.e()) {
         if(this.c.c()) {
            io.realm.internal.n var2 = this.c.b();
            var2.b().a(this.b.i, var2.c(), var1, true);
         }
      } else {
         this.c.a().e();
         this.c.b().a(this.b.i, var1);
      }

   }

   public void b(String var1) {
      if(this.c.e()) {
         if(this.c.c()) {
            io.realm.internal.n var2 = this.c.b();
            if(var1 == null) {
               var2.b().a(this.b.b, var2.c(), true);
            } else {
               var2.b().a(this.b.b, var2.c(), var1, true);
            }
         }
      } else {
         this.c.a().e();
         if(var1 == null) {
            this.c.b().c(this.b.b);
         } else {
            this.c.b().a(this.b.b, var1);
         }
      }

   }

   public void c(String var1) {
      if(this.c.e()) {
         if(this.c.c()) {
            io.realm.internal.n var2 = this.c.b();
            if(var1 == null) {
               var2.b().a(this.b.c, var2.c(), true);
            } else {
               var2.b().a(this.b.c, var2.c(), var1, true);
            }
         }
      } else {
         this.c.a().e();
         if(var1 == null) {
            this.c.b().c(this.b.c);
         } else {
            this.c.b().a(this.b.c, var1);
         }
      }

   }

   public void d(String var1) {
      if(this.c.e()) {
         if(this.c.c()) {
            io.realm.internal.n var2 = this.c.b();
            if(var1 == null) {
               var2.b().a(this.b.d, var2.c(), true);
            } else {
               var2.b().a(this.b.d, var2.c(), var1, true);
            }
         }
      } else {
         this.c.a().e();
         if(var1 == null) {
            this.c.b().c(this.b.d);
         } else {
            this.c.b().a(this.b.d, var1);
         }
      }

   }

   public String e() {
      this.c.a().e();
      return this.c.b().k(this.b.a);
   }

   public void e(String var1) {
      if(this.c.e()) {
         if(this.c.c()) {
            io.realm.internal.n var2 = this.c.b();
            if(var1 == null) {
               var2.b().a(this.b.e, var2.c(), true);
            } else {
               var2.b().a(this.b.e, var2.c(), var1, true);
            }
         }
      } else {
         this.c.a().e();
         if(var1 == null) {
            this.c.b().c(this.b.e);
         } else {
            this.c.b().a(this.b.e, var1);
         }
      }

   }

   public String f() {
      this.c.a().e();
      return this.c.b().k(this.b.b);
   }

   public void f(String var1) {
      if(this.c.e()) {
         if(this.c.c()) {
            io.realm.internal.n var2 = this.c.b();
            if(var1 == null) {
               var2.b().a(this.b.f, var2.c(), true);
            } else {
               var2.b().a(this.b.f, var2.c(), var1, true);
            }
         }
      } else {
         this.c.a().e();
         if(var1 == null) {
            this.c.b().c(this.b.f);
         } else {
            this.c.b().a(this.b.f, var1);
         }
      }

   }

   public String g() {
      this.c.a().e();
      return this.c.b().k(this.b.c);
   }

   public void g(String var1) {
      if(this.c.e()) {
         if(this.c.c()) {
            io.realm.internal.n var2 = this.c.b();
            if(var1 == null) {
               var2.b().a(this.b.g, var2.c(), true);
            } else {
               var2.b().a(this.b.g, var2.c(), var1, true);
            }
         }
      } else {
         this.c.a().e();
         if(var1 == null) {
            this.c.b().c(this.b.g);
         } else {
            this.c.b().a(this.b.g, var1);
         }
      }

   }

   public String h() {
      this.c.a().e();
      return this.c.b().k(this.b.d);
   }

   public void h(String var1) {
      if(this.c.e()) {
         if(this.c.c()) {
            io.realm.internal.n var2 = this.c.b();
            if(var1 == null) {
               var2.b().a(this.b.h, var2.c(), true);
            } else {
               var2.b().a(this.b.h, var2.c(), var1, true);
            }
         }
      } else {
         this.c.a().e();
         if(var1 == null) {
            this.c.b().c(this.b.h);
         } else {
            this.c.b().a(this.b.h, var1);
         }
      }

   }

   public String i() {
      this.c.a().e();
      return this.c.b().k(this.b.e);
   }

   public void i(String var1) {
      if(this.c.e()) {
         if(this.c.c()) {
            io.realm.internal.n var2 = this.c.b();
            if(var1 == null) {
               var2.b().a(this.b.j, var2.c(), true);
            } else {
               var2.b().a(this.b.j, var2.c(), var1, true);
            }
         }
      } else {
         this.c.a().e();
         if(var1 == null) {
            this.c.b().c(this.b.j);
         } else {
            this.c.b().a(this.b.j, var1);
         }
      }

   }

   public String j() {
      this.c.a().e();
      return this.c.b().k(this.b.f);
   }

   public void j(String var1) {
      if(this.c.e()) {
         if(this.c.c()) {
            io.realm.internal.n var2 = this.c.b();
            if(var1 == null) {
               var2.b().a(this.b.k, var2.c(), true);
            } else {
               var2.b().a(this.b.k, var2.c(), var1, true);
            }
         }
      } else {
         this.c.a().e();
         if(var1 == null) {
            this.c.b().c(this.b.k);
         } else {
            this.c.b().a(this.b.k, var1);
         }
      }

   }

   public String l() {
      this.c.a().e();
      return this.c.b().k(this.b.h);
   }

   public boolean m() {
      this.c.a().e();
      return this.c.b().g(this.b.i);
   }

   public String n() {
      this.c.a().e();
      return this.c.b().k(this.b.j);
   }

   public String o() {
      this.c.a().e();
      return this.c.b().k(this.b.k);
   }

   public au s_() {
      return this.c;
   }

   public String t_() {
      this.c.a().e();
      return this.c.b().k(this.b.g);
   }

   public String toString() {
      String var1;
      if(!bc.b(this)) {
         var1 = "Invalid object";
      } else {
         StringBuilder var2 = new StringBuilder("DirectDebit = proxy[");
         var2.append("{id:");
         if(this.e() != null) {
            var1 = this.e();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{accountId:");
         if(this.f() != null) {
            var1 = this.f();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{payerSortCode:");
         if(this.g() != null) {
            var1 = this.g();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{payerAccountNumber:");
         if(this.h() != null) {
            var1 = this.h();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{payerName:");
         if(this.i() != null) {
            var1 = this.i();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{serviceUserNumber:");
         if(this.j() != null) {
            var1 = this.j();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{serviceUserName:");
         if(this.t_() != null) {
            var1 = this.t_();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{reference:");
         if(this.l() != null) {
            var1 = this.l();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{active:");
         var2.append(this.m());
         var2.append("}");
         var2.append(",");
         var2.append("{_created:");
         if(this.n() != null) {
            var1 = this.n();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{_lastCollected:");
         if(this.o() != null) {
            var1 = this.o();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append("]");
         var1 = var2.toString();
      }

      return var1;
   }

   public void u_() {
      if(this.c == null) {
         g.b var1 = (g.b)g.g.get();
         this.b = (m.a)var1.c();
         this.c = new au(this);
         this.c.a(var1.a());
         this.c.a(var1.b());
         this.c.a(var1.d());
         this.c.a(var1.e());
      }

   }

   static final class a extends io.realm.internal.c {
      long a;
      long b;
      long c;
      long d;
      long e;
      long f;
      long g;
      long h;
      long i;
      long j;
      long k;

      a(SharedRealm var1, Table var2) {
         super(11);
         this.a = this.a(var2, "id", RealmFieldType.STRING);
         this.b = this.a(var2, "accountId", RealmFieldType.STRING);
         this.c = this.a(var2, "payerSortCode", RealmFieldType.STRING);
         this.d = this.a(var2, "payerAccountNumber", RealmFieldType.STRING);
         this.e = this.a(var2, "payerName", RealmFieldType.STRING);
         this.f = this.a(var2, "serviceUserNumber", RealmFieldType.STRING);
         this.g = this.a(var2, "serviceUserName", RealmFieldType.STRING);
         this.h = this.a(var2, "reference", RealmFieldType.STRING);
         this.i = this.a(var2, "active", RealmFieldType.BOOLEAN);
         this.j = this.a(var2, "_created", RealmFieldType.STRING);
         this.k = this.a(var2, "_lastCollected", RealmFieldType.STRING);
      }

      a(io.realm.internal.c var1, boolean var2) {
         super(var1, var2);
         this.a(var1, this);
      }

      protected final io.realm.internal.c a(boolean var1) {
         return new m.a(this, var1);
      }

      protected final void a(io.realm.internal.c var1, io.realm.internal.c var2) {
         m.a var3 = (m.a)var1;
         m.a var4 = (m.a)var2;
         var4.a = var3.a;
         var4.b = var3.b;
         var4.c = var3.c;
         var4.d = var3.d;
         var4.e = var3.e;
         var4.f = var3.f;
         var4.g = var3.g;
         var4.h = var3.h;
         var4.i = var3.i;
         var4.j = var3.j;
         var4.k = var3.k;
      }
   }
}
