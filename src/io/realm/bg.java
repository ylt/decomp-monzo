package io.realm;

import io.realm.internal.Collection;

public class bg extends an {
   bg(g var1, Collection var2, Class var3) {
      super(var1, var2, var3);
   }

   bg(g var1, Collection var2, String var3) {
      super(var1, var2, var3);
   }

   private void a(Object var1, boolean var2) {
      if(var2 && var1 == null) {
         throw new IllegalArgumentException("Listener should not be null");
      } else {
         this.a.e();
         this.a.e.d.a("Listeners cannot be used on current thread.");
      }
   }

   public void a(am var1) {
      this.a(var1, true);
      this.d.addListener(this, (am)var1);
   }

   public void b(am var1) {
      this.a(var1, true);
      this.d.removeListener(this, (am)var1);
   }

   public boolean c() {
      this.a.e();
      return this.d.isLoaded();
   }

   public boolean d() {
      this.a.e();
      this.d.load();
      return true;
   }
}
