package io.realm;

import io.realm.internal.Table;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class be {
   private static final Map a;
   private static final Map b;
   private final bh c;
   private final g d;
   private final io.realm.internal.c e;
   private final Table f;

   static {
      HashMap var0 = new HashMap();
      var0.put(String.class, new be.b(RealmFieldType.STRING, true));
      var0.put(Short.TYPE, new be.b(RealmFieldType.INTEGER, false));
      var0.put(Short.class, new be.b(RealmFieldType.INTEGER, true));
      var0.put(Integer.TYPE, new be.b(RealmFieldType.INTEGER, false));
      var0.put(Integer.class, new be.b(RealmFieldType.INTEGER, true));
      var0.put(Long.TYPE, new be.b(RealmFieldType.INTEGER, false));
      var0.put(Long.class, new be.b(RealmFieldType.INTEGER, true));
      var0.put(Float.TYPE, new be.b(RealmFieldType.FLOAT, false));
      var0.put(Float.class, new be.b(RealmFieldType.FLOAT, true));
      var0.put(Double.TYPE, new be.b(RealmFieldType.DOUBLE, false));
      var0.put(Double.class, new be.b(RealmFieldType.DOUBLE, true));
      var0.put(Boolean.TYPE, new be.b(RealmFieldType.BOOLEAN, false));
      var0.put(Boolean.class, new be.b(RealmFieldType.BOOLEAN, true));
      var0.put(Byte.TYPE, new be.b(RealmFieldType.INTEGER, false));
      var0.put(Byte.class, new be.b(RealmFieldType.INTEGER, true));
      var0.put(byte[].class, new be.b(RealmFieldType.BINARY, true));
      var0.put(Date.class, new be.b(RealmFieldType.DATE, true));
      a = Collections.unmodifiableMap(var0);
      var0 = new HashMap();
      var0.put(bc.class, new be.b(RealmFieldType.OBJECT, false));
      var0.put(az.class, new be.b(RealmFieldType.LIST, false));
      b = Collections.unmodifiableMap(var0);
   }

   be(g var1, bh var2, Table var3) {
      this(var1, var2, var3, new be.a(var3));
   }

   be(g var1, bh var2, Table var3, io.realm.internal.c var4) {
      this.c = var2;
      this.d = var1;
      this.f = var3;
      this.e = var4;
   }

   private bi c() {
      return new bi(this.c);
   }

   long a(String var1) {
      long var2 = this.e.a(var1);
      if(var2 < 0L) {
         throw new IllegalArgumentException("Field does not exist: " + var1);
      } else {
         return var2;
      }
   }

   protected final io.realm.internal.a.c a(String var1, RealmFieldType... var2) {
      return io.realm.internal.a.c.a((io.realm.internal.a.c.a)this.c(), (Table)this.b(), (String)var1, (RealmFieldType[])var2);
   }

   public String a() {
      return this.f.k();
   }

   Table b() {
      return this.f;
   }

   private static final class a extends io.realm.internal.c {
      private final Table a;

      a(Table var1) {
         super((io.realm.internal.c)null, false);
         this.a = var1;
      }

      public long a(String var1) {
         return this.a.a(var1);
      }

      protected io.realm.internal.c a(boolean var1) {
         throw new UnsupportedOperationException("DynamicColumnIndices cannot be copied");
      }

      public void a(io.realm.internal.c var1) {
         throw new UnsupportedOperationException("DynamicColumnIndices cannot be copied");
      }

      protected void a(io.realm.internal.c var1, io.realm.internal.c var2) {
         throw new UnsupportedOperationException("DynamicColumnIndices cannot copy");
      }

      public RealmFieldType b(String var1) {
         throw new UnsupportedOperationException("DynamicColumnIndices do not support 'getColumnType'");
      }

      public String c(String var1) {
         throw new UnsupportedOperationException("DynamicColumnIndices do not support 'getLinkedTable'");
      }
   }

   private static final class b {
      final RealmFieldType a;
      final boolean b;

      b(RealmFieldType var1, boolean var2) {
         this.a = var1;
         this.b = var2;
      }
   }
}
