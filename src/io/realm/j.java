package io.realm;

import io.realm.exceptions.RealmException;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.SharedRealm;
import io.realm.internal.Table;
import io.realm.log.RealmLog;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class j extends co.uk.getmondo.d.g implements io.realm.internal.l, k {
   private static final OsObjectSchemaInfo c = r();
   private static final List d;
   private j.a a;
   private au b;

   static {
      ArrayList var0 = new ArrayList();
      var0.add("id");
      var0.add("accountId");
      var0.add("expires");
      var0.add("lastDigits");
      var0.add("processorToken");
      var0.add("cardStatus");
      var0.add("replacementOrdered");
      d = Collections.unmodifiableList(var0);
   }

   j() {
      this.b.f();
   }

   public static long a(av var0, co.uk.getmondo.d.g var1, Map var2) {
      long var3;
      if(var1 instanceof io.realm.internal.l && ((io.realm.internal.l)var1).s_().a() != null && ((io.realm.internal.l)var1).s_().a().g().equals(var0.g())) {
         var3 = ((io.realm.internal.l)var1).s_().b().c();
      } else {
         Table var7 = var0.c(co.uk.getmondo.d.g.class);
         long var5 = var7.getNativePtr();
         j.a var9 = (j.a)var0.f.c(co.uk.getmondo.d.g.class);
         var3 = var7.d();
         String var8 = ((k)var1).i();
         if(var8 == null) {
            var3 = Table.nativeFindFirstNull(var5, var3);
         } else {
            var3 = Table.nativeFindFirstString(var5, var3, var8);
         }

         if(var3 == -1L) {
            var3 = OsObject.b(var7, var8);
         } else {
            Table.a((Object)var8);
         }

         var2.put(var1, Long.valueOf(var3));
         String var10 = ((k)var1).j();
         if(var10 != null) {
            Table.nativeSetString(var5, var9.b, var3, var10, false);
         }

         var10 = ((k)var1).r_();
         if(var10 != null) {
            Table.nativeSetString(var5, var9.c, var3, var10, false);
         }

         var10 = ((k)var1).l();
         if(var10 != null) {
            Table.nativeSetString(var5, var9.d, var3, var10, false);
         }

         var10 = ((k)var1).m();
         if(var10 != null) {
            Table.nativeSetString(var5, var9.e, var3, var10, false);
         }

         var10 = ((k)var1).n();
         if(var10 != null) {
            Table.nativeSetString(var5, var9.f, var3, var10, false);
         }

         Table.nativeSetBoolean(var5, var9.g, var3, ((k)var1).o(), false);
      }

      return var3;
   }

   public static co.uk.getmondo.d.g a(co.uk.getmondo.d.g var0, int var1, int var2, Map var3) {
      if(var1 <= var2 && var0 != null) {
         io.realm.internal.l.a var4 = (io.realm.internal.l.a)var3.get(var0);
         co.uk.getmondo.d.g var6;
         if(var4 == null) {
            co.uk.getmondo.d.g var7 = new co.uk.getmondo.d.g();
            var3.put(var0, new io.realm.internal.l.a(var1, var7));
            var6 = var7;
         } else {
            if(var1 >= var4.a) {
               var0 = (co.uk.getmondo.d.g)var4.b;
               return var0;
            }

            var6 = (co.uk.getmondo.d.g)var4.b;
            var4.a = var1;
         }

         k var8 = (k)var6;
         k var5 = (k)var0;
         var8.a(var5.i());
         var8.b(var5.j());
         var8.c(var5.r_());
         var8.d(var5.l());
         var8.e(var5.m());
         var8.f(var5.n());
         var8.a(var5.o());
         var0 = var6;
      } else {
         var0 = null;
      }

      return var0;
   }

   static co.uk.getmondo.d.g a(av var0, co.uk.getmondo.d.g var1, co.uk.getmondo.d.g var2, Map var3) {
      k var4 = (k)var1;
      k var5 = (k)var2;
      var4.b(var5.j());
      var4.c(var5.r_());
      var4.d(var5.l());
      var4.e(var5.m());
      var4.f(var5.n());
      var4.a(var5.o());
      return var1;
   }

   public static co.uk.getmondo.d.g a(av var0, co.uk.getmondo.d.g var1, boolean var2, Map var3) {
      if(var1 instanceof io.realm.internal.l && ((io.realm.internal.l)var1).s_().a() != null && ((io.realm.internal.l)var1).s_().a().c != var0.c) {
         throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
      } else {
         if(!(var1 instanceof io.realm.internal.l) || ((io.realm.internal.l)var1).s_().a() == null || !((io.realm.internal.l)var1).s_().a().g().equals(var0.g())) {
            g.b var8 = (g.b)g.g.get();
            io.realm.internal.l var7 = (io.realm.internal.l)var3.get(var1);
            if(var7 != null) {
               var1 = (co.uk.getmondo.d.g)var7;
            } else {
               boolean var4;
               j var13;
               if(var2) {
                  Table var9 = var0.c(co.uk.getmondo.d.g.class);
                  long var5 = var9.d();
                  String var12 = ((k)var1).i();
                  if(var12 == null) {
                     var5 = var9.k(var5);
                  } else {
                     var5 = var9.a(var5, var12);
                  }

                  if(var5 != -1L) {
                     try {
                        var8.a(var0, var9.f(var5), var0.f.c(co.uk.getmondo.d.g.class), false, Collections.emptyList());
                        var13 = new j();
                        var3.put(var1, (io.realm.internal.l)var13);
                     } finally {
                        var8.f();
                     }

                     var4 = var2;
                  } else {
                     var4 = false;
                     var13 = null;
                  }
               } else {
                  var4 = var2;
                  var13 = null;
               }

               if(var4) {
                  var1 = a(var0, var13, var1, var3);
               } else {
                  var1 = b(var0, var1, var2, var3);
               }
            }
         }

         return var1;
      }
   }

   public static j.a a(SharedRealm var0, boolean var1) {
      if(!var0.a("class_Card")) {
         throw new RealmMigrationNeededException(var0.h(), "The 'Card' class is missing from the schema for this Realm.");
      } else {
         Table var7 = var0.b("class_Card");
         long var4 = var7.c();
         if(var4 != 7L) {
            if(var4 < 7L) {
               throw new RealmMigrationNeededException(var0.h(), "Field count is less than expected - expected 7 but was " + var4);
            }

            if(!var1) {
               throw new RealmMigrationNeededException(var0.h(), "Field count is more than expected - expected 7 but was " + var4);
            }

            RealmLog.a("Field count is more than expected - expected 7 but was %1$d", new Object[]{Long.valueOf(var4)});
         }

         HashMap var6 = new HashMap();

         for(long var2 = 0L; var2 < var4; ++var2) {
            var6.put(var7.b(var2), var7.c(var2));
         }

         j.a var8 = new j.a(var0, var7);
         if(!var7.e()) {
            throw new RealmMigrationNeededException(var0.h(), "Primary key not defined for field 'id' in existing Realm file. @PrimaryKey was added.");
         } else if(var7.d() != var8.a) {
            throw new RealmMigrationNeededException(var0.h(), "Primary Key annotation definition was changed, from field " + var7.b(var7.d()) + " to field id");
         } else if(!var6.containsKey("id")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'id' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var6.get("id") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'id' in existing Realm file.");
         } else if(!var7.a(var8.a)) {
            throw new RealmMigrationNeededException(var0.h(), "@PrimaryKey field 'id' does not support null values in the existing Realm file. Migrate using RealmObjectSchema.setNullable(), or mark the field as @Required.");
         } else if(!var7.j(var7.a("id"))) {
            throw new RealmMigrationNeededException(var0.h(), "Index not defined for field 'id' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex().");
         } else if(!var6.containsKey("accountId")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'accountId' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var6.get("accountId") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'accountId' in existing Realm file.");
         } else if(!var7.a(var8.b)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'accountId' is required. Either set @Required to field 'accountId' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var6.containsKey("expires")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'expires' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var6.get("expires") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'expires' in existing Realm file.");
         } else if(!var7.a(var8.c)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'expires' is required. Either set @Required to field 'expires' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var6.containsKey("lastDigits")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'lastDigits' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var6.get("lastDigits") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'lastDigits' in existing Realm file.");
         } else if(!var7.a(var8.d)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'lastDigits' is required. Either set @Required to field 'lastDigits' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var6.containsKey("processorToken")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'processorToken' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var6.get("processorToken") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'processorToken' in existing Realm file.");
         } else if(!var7.a(var8.e)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'processorToken' is required. Either set @Required to field 'processorToken' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var6.containsKey("cardStatus")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'cardStatus' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var6.get("cardStatus") != RealmFieldType.STRING) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'String' for field 'cardStatus' in existing Realm file.");
         } else if(!var7.a(var8.f)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'cardStatus' is required. Either set @Required to field 'cardStatus' or migrate using RealmObjectSchema.setNullable().");
         } else if(!var6.containsKey("replacementOrdered")) {
            throw new RealmMigrationNeededException(var0.h(), "Missing field 'replacementOrdered' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
         } else if(var6.get("replacementOrdered") != RealmFieldType.BOOLEAN) {
            throw new RealmMigrationNeededException(var0.h(), "Invalid type 'boolean' for field 'replacementOrdered' in existing Realm file.");
         } else if(var7.a(var8.g)) {
            throw new RealmMigrationNeededException(var0.h(), "Field 'replacementOrdered' does support null values in the existing Realm file. Use corresponding boxed type for field 'replacementOrdered' or migrate using RealmObjectSchema.setNullable().");
         } else {
            return var8;
         }
      }
   }

   public static void a(av var0, Iterator var1, Map var2) {
      Table var12 = var0.c(co.uk.getmondo.d.g.class);
      long var9 = var12.getNativePtr();
      j.a var13 = (j.a)var0.f.c(co.uk.getmondo.d.g.class);
      long var7 = var12.d();

      while(true) {
         while(true) {
            co.uk.getmondo.d.g var11;
            do {
               if(!var1.hasNext()) {
                  return;
               }

               var11 = (co.uk.getmondo.d.g)var1.next();
            } while(var2.containsKey(var11));

            if(var11 instanceof io.realm.internal.l && ((io.realm.internal.l)var11).s_().a() != null && ((io.realm.internal.l)var11).s_().a().g().equals(var0.g())) {
               var2.put(var11, Long.valueOf(((io.realm.internal.l)var11).s_().b().c()));
            } else {
               String var14 = ((k)var11).i();
               long var3;
               if(var14 == null) {
                  var3 = Table.nativeFindFirstNull(var9, var7);
               } else {
                  var3 = Table.nativeFindFirstString(var9, var7, var14);
               }

               long var5 = var3;
               if(var3 == -1L) {
                  var5 = OsObject.b(var12, var14);
               }

               var2.put(var11, Long.valueOf(var5));
               var14 = ((k)var11).j();
               if(var14 != null) {
                  Table.nativeSetString(var9, var13.b, var5, var14, false);
               } else {
                  Table.nativeSetNull(var9, var13.b, var5, false);
               }

               var14 = ((k)var11).r_();
               if(var14 != null) {
                  Table.nativeSetString(var9, var13.c, var5, var14, false);
               } else {
                  Table.nativeSetNull(var9, var13.c, var5, false);
               }

               var14 = ((k)var11).l();
               if(var14 != null) {
                  Table.nativeSetString(var9, var13.d, var5, var14, false);
               } else {
                  Table.nativeSetNull(var9, var13.d, var5, false);
               }

               var14 = ((k)var11).m();
               if(var14 != null) {
                  Table.nativeSetString(var9, var13.e, var5, var14, false);
               } else {
                  Table.nativeSetNull(var9, var13.e, var5, false);
               }

               var14 = ((k)var11).n();
               if(var14 != null) {
                  Table.nativeSetString(var9, var13.f, var5, var14, false);
               } else {
                  Table.nativeSetNull(var9, var13.f, var5, false);
               }

               Table.nativeSetBoolean(var9, var13.g, var5, ((k)var11).o(), false);
            }
         }
      }
   }

   public static long b(av var0, co.uk.getmondo.d.g var1, Map var2) {
      long var3;
      if(var1 instanceof io.realm.internal.l && ((io.realm.internal.l)var1).s_().a() != null && ((io.realm.internal.l)var1).s_().a().g().equals(var0.g())) {
         var3 = ((io.realm.internal.l)var1).s_().b().c();
      } else {
         Table var9 = var0.c(co.uk.getmondo.d.g.class);
         long var7 = var9.getNativePtr();
         j.a var11 = (j.a)var0.f.c(co.uk.getmondo.d.g.class);
         var3 = var9.d();
         String var10 = ((k)var1).i();
         long var5;
         if(var10 == null) {
            var5 = Table.nativeFindFirstNull(var7, var3);
         } else {
            var5 = Table.nativeFindFirstString(var7, var3, var10);
         }

         var3 = var5;
         if(var5 == -1L) {
            var3 = OsObject.b(var9, var10);
         }

         var2.put(var1, Long.valueOf(var3));
         String var12 = ((k)var1).j();
         if(var12 != null) {
            Table.nativeSetString(var7, var11.b, var3, var12, false);
         } else {
            Table.nativeSetNull(var7, var11.b, var3, false);
         }

         var12 = ((k)var1).r_();
         if(var12 != null) {
            Table.nativeSetString(var7, var11.c, var3, var12, false);
         } else {
            Table.nativeSetNull(var7, var11.c, var3, false);
         }

         var12 = ((k)var1).l();
         if(var12 != null) {
            Table.nativeSetString(var7, var11.d, var3, var12, false);
         } else {
            Table.nativeSetNull(var7, var11.d, var3, false);
         }

         var12 = ((k)var1).m();
         if(var12 != null) {
            Table.nativeSetString(var7, var11.e, var3, var12, false);
         } else {
            Table.nativeSetNull(var7, var11.e, var3, false);
         }

         var12 = ((k)var1).n();
         if(var12 != null) {
            Table.nativeSetString(var7, var11.f, var3, var12, false);
         } else {
            Table.nativeSetNull(var7, var11.f, var3, false);
         }

         Table.nativeSetBoolean(var7, var11.g, var3, ((k)var1).o(), false);
      }

      return var3;
   }

   public static co.uk.getmondo.d.g b(av var0, co.uk.getmondo.d.g var1, boolean var2, Map var3) {
      io.realm.internal.l var4 = (io.realm.internal.l)var3.get(var1);
      co.uk.getmondo.d.g var5;
      if(var4 != null) {
         var5 = (co.uk.getmondo.d.g)var4;
      } else {
         var5 = (co.uk.getmondo.d.g)var0.a(co.uk.getmondo.d.g.class, ((k)var1).i(), false, Collections.emptyList());
         var3.put(var1, (io.realm.internal.l)var5);
         k var7 = (k)var1;
         k var6 = (k)var5;
         var6.b(var7.j());
         var6.c(var7.r_());
         var6.d(var7.l());
         var6.e(var7.m());
         var6.f(var7.n());
         var6.a(var7.o());
      }

      return var5;
   }

   public static OsObjectSchemaInfo p() {
      return c;
   }

   public static String q() {
      return "class_Card";
   }

   private static OsObjectSchemaInfo r() {
      OsObjectSchemaInfo.a var0 = new OsObjectSchemaInfo.a("Card");
      var0.a("id", RealmFieldType.STRING, true, true, false);
      var0.a("accountId", RealmFieldType.STRING, false, false, false);
      var0.a("expires", RealmFieldType.STRING, false, false, false);
      var0.a("lastDigits", RealmFieldType.STRING, false, false, false);
      var0.a("processorToken", RealmFieldType.STRING, false, false, false);
      var0.a("cardStatus", RealmFieldType.STRING, false, false, false);
      var0.a("replacementOrdered", RealmFieldType.BOOLEAN, false, false, true);
      return var0.a();
   }

   public void a(String var1) {
      if(!this.b.e()) {
         this.b.a().e();
         throw new RealmException("Primary key field 'id' cannot be changed after object was created.");
      }
   }

   public void a(boolean var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            var2.b().a(this.a.g, var2.c(), var1, true);
         }
      } else {
         this.b.a().e();
         this.b.b().a(this.a.g, var1);
      }

   }

   public void b(String var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            if(var1 == null) {
               var2.b().a(this.a.b, var2.c(), true);
            } else {
               var2.b().a(this.a.b, var2.c(), var1, true);
            }
         }
      } else {
         this.b.a().e();
         if(var1 == null) {
            this.b.b().c(this.a.b);
         } else {
            this.b.b().a(this.a.b, var1);
         }
      }

   }

   public void c(String var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            if(var1 == null) {
               var2.b().a(this.a.c, var2.c(), true);
            } else {
               var2.b().a(this.a.c, var2.c(), var1, true);
            }
         }
      } else {
         this.b.a().e();
         if(var1 == null) {
            this.b.b().c(this.a.c);
         } else {
            this.b.b().a(this.a.c, var1);
         }
      }

   }

   public void d(String var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            if(var1 == null) {
               var2.b().a(this.a.d, var2.c(), true);
            } else {
               var2.b().a(this.a.d, var2.c(), var1, true);
            }
         }
      } else {
         this.b.a().e();
         if(var1 == null) {
            this.b.b().c(this.a.d);
         } else {
            this.b.b().a(this.a.d, var1);
         }
      }

   }

   public void e(String var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            if(var1 == null) {
               var2.b().a(this.a.e, var2.c(), true);
            } else {
               var2.b().a(this.a.e, var2.c(), var1, true);
            }
         }
      } else {
         this.b.a().e();
         if(var1 == null) {
            this.b.b().c(this.a.e);
         } else {
            this.b.b().a(this.a.e, var1);
         }
      }

   }

   public void f(String var1) {
      if(this.b.e()) {
         if(this.b.c()) {
            io.realm.internal.n var2 = this.b.b();
            if(var1 == null) {
               var2.b().a(this.a.f, var2.c(), true);
            } else {
               var2.b().a(this.a.f, var2.c(), var1, true);
            }
         }
      } else {
         this.b.a().e();
         if(var1 == null) {
            this.b.b().c(this.a.f);
         } else {
            this.b.b().a(this.a.f, var1);
         }
      }

   }

   public String i() {
      this.b.a().e();
      return this.b.b().k(this.a.a);
   }

   public String j() {
      this.b.a().e();
      return this.b.b().k(this.a.b);
   }

   public String l() {
      this.b.a().e();
      return this.b.b().k(this.a.d);
   }

   public String m() {
      this.b.a().e();
      return this.b.b().k(this.a.e);
   }

   public String n() {
      this.b.a().e();
      return this.b.b().k(this.a.f);
   }

   public boolean o() {
      this.b.a().e();
      return this.b.b().g(this.a.g);
   }

   public String r_() {
      this.b.a().e();
      return this.b.b().k(this.a.c);
   }

   public au s_() {
      return this.b;
   }

   public String toString() {
      String var1;
      if(!bc.b(this)) {
         var1 = "Invalid object";
      } else {
         StringBuilder var2 = new StringBuilder("Card = proxy[");
         var2.append("{id:");
         if(this.i() != null) {
            var1 = this.i();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{accountId:");
         if(this.j() != null) {
            var1 = this.j();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{expires:");
         if(this.r_() != null) {
            var1 = this.r_();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{lastDigits:");
         if(this.l() != null) {
            var1 = this.l();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{processorToken:");
         if(this.m() != null) {
            var1 = this.m();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{cardStatus:");
         if(this.n() != null) {
            var1 = this.n();
         } else {
            var1 = "null";
         }

         var2.append(var1);
         var2.append("}");
         var2.append(",");
         var2.append("{replacementOrdered:");
         var2.append(this.o());
         var2.append("}");
         var2.append("]");
         var1 = var2.toString();
      }

      return var1;
   }

   public void u_() {
      if(this.b == null) {
         g.b var1 = (g.b)g.g.get();
         this.a = (j.a)var1.c();
         this.b = new au(this);
         this.b.a(var1.a());
         this.b.a(var1.b());
         this.b.a(var1.d());
         this.b.a(var1.e());
      }

   }

   static final class a extends io.realm.internal.c {
      long a;
      long b;
      long c;
      long d;
      long e;
      long f;
      long g;

      a(SharedRealm var1, Table var2) {
         super(7);
         this.a = this.a(var2, "id", RealmFieldType.STRING);
         this.b = this.a(var2, "accountId", RealmFieldType.STRING);
         this.c = this.a(var2, "expires", RealmFieldType.STRING);
         this.d = this.a(var2, "lastDigits", RealmFieldType.STRING);
         this.e = this.a(var2, "processorToken", RealmFieldType.STRING);
         this.f = this.a(var2, "cardStatus", RealmFieldType.STRING);
         this.g = this.a(var2, "replacementOrdered", RealmFieldType.BOOLEAN);
      }

      a(io.realm.internal.c var1, boolean var2) {
         super(var1, var2);
         this.a(var1, this);
      }

      protected final io.realm.internal.c a(boolean var1) {
         return new j.a(this, var1);
      }

      protected final void a(io.realm.internal.c var1, io.realm.internal.c var2) {
         j.a var3 = (j.a)var1;
         j.a var4 = (j.a)var2;
         var4.a = var3.a;
         var4.b = var3.b;
         var4.c = var3.c;
         var4.d = var3.d;
         var4.e = var3.e;
         var4.f = var3.f;
         var4.g = var3.g;
      }
   }
}
