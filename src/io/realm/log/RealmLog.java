package io.realm.log;

import android.util.Log;
import java.util.Locale;

public final class RealmLog {
   private static String a = "REALM_JAVA";

   public static int a() {
      return nativeGetLogLevel();
   }

   private static void a(int var0, Throwable var1, String var2, Object... var3) {
      if(var0 >= a()) {
         StringBuilder var5 = new StringBuilder();
         String var4 = var2;
         if(var3 != null) {
            var4 = var2;
            if(var3.length > 0) {
               var4 = String.format(Locale.US, var2, var3);
            }
         }

         if(var1 != null) {
            var5.append(Log.getStackTraceString(var1));
         }

         if(var4 != null) {
            if(var1 != null) {
               var5.append("\n");
            }

            var5.append(var4);
         }

         nativeLog(var0, a, var1, var5.toString());
      }

   }

   public static void a(String var0, Object... var1) {
      a((Throwable)null, var0, var1);
   }

   public static void a(Throwable var0, String var1, Object... var2) {
      a(3, var0, var1, var2);
   }

   public static void b(String var0, Object... var1) {
      b((Throwable)null, var0, var1);
   }

   public static void b(Throwable var0, String var1, Object... var2) {
      a(5, var0, var1, var2);
   }

   public static void c(String var0, Object... var1) {
      c((Throwable)null, var0, var1);
   }

   public static void c(Throwable var0, String var1, Object... var2) {
      a(7, var0, var1, var2);
   }

   private static native int nativeGetLogLevel();

   private static native void nativeLog(int var0, String var1, Throwable var2, String var3);
}
