package io.realm.log;

import io.realm.internal.Keep;

@Keep
public interface RealmLogger {
   void log(int var1, String var2, Throwable var3, String var4);
}
