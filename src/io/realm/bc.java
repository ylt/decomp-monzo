package io.realm;

public abstract class bc implements bb {
   public static void a(bb var0) {
      if(!(var0 instanceof io.realm.internal.l)) {
         throw new IllegalArgumentException("Object not managed by Realm, so it cannot be removed.");
      } else {
         io.realm.internal.l var2 = (io.realm.internal.l)var0;
         if(var2.s_().b() == null) {
            throw new IllegalStateException("Object malformed: missing object in Realm. Make sure to instantiate RealmObjects with Realm.createObject()");
         } else if(var2.s_().a() == null) {
            throw new IllegalStateException("Object malformed: missing Realm. Make sure to instantiate RealmObjects with Realm.createObject()");
         } else {
            var2.s_().a().e();
            io.realm.internal.n var1 = var2.s_().b();
            var1.b().d(var1.c());
            var2.s_().a((io.realm.internal.n)io.realm.internal.e.a);
         }
      }
   }

   public static boolean b(bb var0) {
      boolean var2 = true;
      boolean var1 = var2;
      if(var0 instanceof io.realm.internal.l) {
         io.realm.internal.n var3 = ((io.realm.internal.l)var0).s_().b();
         if(var3 != null && var3.d()) {
            var1 = var2;
         } else {
            var1 = false;
         }
      }

      return var1;
   }

   public static boolean c(bb var0) {
      return var0 instanceof io.realm.internal.l;
   }
}
