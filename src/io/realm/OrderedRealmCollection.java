package io.realm;

import java.util.List;

public interface OrderedRealmCollection extends RealmCollection, List {
}
