package io.intercom.retrofit2;

import android.os.Handler;
import android.os.Looper;
import android.os.Build.VERSION;
import java.lang.invoke.MethodHandles.Lookup;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.concurrent.Executor;
import javax.annotation.Nullable;
import org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement;

class Platform {
   private static final Platform PLATFORM = findPlatform();

   private static Platform findPlatform() {
      Object var0;
      try {
         Class.forName("android.os.Build");
         if(VERSION.SDK_INT != 0) {
            var0 = new Platform.Android();
            return (Platform)var0;
         }
      } catch (ClassNotFoundException var2) {
         ;
      }

      try {
         Class.forName("java.util.Optional");
         var0 = new Platform.Java8();
      } catch (ClassNotFoundException var1) {
         var0 = new Platform();
      }

      return (Platform)var0;
   }

   static Platform get() {
      return PLATFORM;
   }

   CallAdapter.Factory defaultCallAdapterFactory(@Nullable Executor var1) {
      Object var2;
      if(var1 != null) {
         var2 = new ExecutorCallAdapterFactory(var1);
      } else {
         var2 = DefaultCallAdapterFactory.INSTANCE;
      }

      return (CallAdapter.Factory)var2;
   }

   @Nullable
   Executor defaultCallbackExecutor() {
      return null;
   }

   @Nullable
   Object invokeDefaultMethod(Method var1, Class var2, Object var3, @Nullable Object... var4) throws Throwable {
      throw new UnsupportedOperationException();
   }

   boolean isDefaultMethod(Method var1) {
      return false;
   }

   static class Android extends Platform {
      CallAdapter.Factory defaultCallAdapterFactory(@Nullable Executor var1) {
         if(var1 == null) {
            throw new AssertionError();
         } else {
            return new ExecutorCallAdapterFactory(var1);
         }
      }

      public Executor defaultCallbackExecutor() {
         return new Platform.MainThreadExecutor();
      }
   }

   static class MainThreadExecutor implements Executor {
      private final Handler handler = new Handler(Looper.getMainLooper());

      public void execute(Runnable var1) {
         this.handler.post(var1);
      }
   }

   @IgnoreJRERequirement
   static class Java8 extends Platform {
      Object invokeDefaultMethod(Method var1, Class var2, Object var3, @Nullable Object... var4) throws Throwable {
         Constructor var5 = Lookup.class.getDeclaredConstructor(new Class[]{Class.class, Integer.TYPE});
         var5.setAccessible(true);
         return ((Lookup)var5.newInstance(new Object[]{var2, Integer.valueOf(-1)})).unreflectSpecial(var1, var2).bindTo(var3).invokeWithArguments(var4);
      }

      boolean isDefaultMethod(Method var1) {
         return var1.isDefault();
      }
   }
}
