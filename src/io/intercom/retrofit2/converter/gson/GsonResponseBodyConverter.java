package io.intercom.retrofit2.converter.gson;

import io.intercom.com.google.gson.e;
import io.intercom.com.google.gson.q;
import io.intercom.com.google.gson.stream.a;
import io.intercom.okhttp3.ResponseBody;
import io.intercom.retrofit2.Converter;
import java.io.IOException;

final class GsonResponseBodyConverter implements Converter {
   private final q adapter;
   private final e gson;

   GsonResponseBodyConverter(e var1, q var2) {
      this.gson = var1;
      this.adapter = var2;
   }

   public Object convert(ResponseBody var1) throws IOException {
      a var2 = this.gson.a(var1.charStream());

      Object var5;
      try {
         var5 = this.adapter.b(var2);
      } finally {
         var1.close();
      }

      return var5;
   }
}
