package io.intercom.retrofit2.converter.gson;

import io.intercom.a.c;
import io.intercom.com.google.gson.e;
import io.intercom.com.google.gson.q;
import io.intercom.okhttp3.MediaType;
import io.intercom.okhttp3.RequestBody;
import io.intercom.retrofit2.Converter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;

final class GsonRequestBodyConverter implements Converter {
   private static final MediaType MEDIA_TYPE = MediaType.parse("application/json; charset=UTF-8");
   private static final Charset UTF_8 = Charset.forName("UTF-8");
   private final q adapter;
   private final e gson;

   GsonRequestBodyConverter(e var1, q var2) {
      this.gson = var1;
      this.adapter = var2;
   }

   public RequestBody convert(Object var1) throws IOException {
      c var2 = new c();
      OutputStreamWriter var3 = new OutputStreamWriter(var2.c(), UTF_8);
      io.intercom.com.google.gson.stream.c var4 = this.gson.a((Writer)var3);
      this.adapter.a(var4, var1);
      var4.close();
      return RequestBody.create(MEDIA_TYPE, var2.q());
   }
}
