package io.intercom.retrofit2.converter.gson;

import io.intercom.com.google.gson.e;
import io.intercom.com.google.gson.q;
import io.intercom.com.google.gson.c.a;
import io.intercom.retrofit2.Converter;
import io.intercom.retrofit2.Retrofit;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

public final class GsonConverterFactory extends Converter.Factory {
   private final e gson;

   private GsonConverterFactory(e var1) {
      this.gson = var1;
   }

   public static GsonConverterFactory create() {
      return create(new e());
   }

   public static GsonConverterFactory create(e var0) {
      if(var0 == null) {
         throw new NullPointerException("gson == null");
      } else {
         return new GsonConverterFactory(var0);
      }
   }

   public Converter requestBodyConverter(Type var1, Annotation[] var2, Annotation[] var3, Retrofit var4) {
      q var5 = this.gson.a(a.a(var1));
      return new GsonRequestBodyConverter(this.gson, var5);
   }

   public Converter responseBodyConverter(Type var1, Annotation[] var2, Retrofit var3) {
      q var4 = this.gson.a(a.a(var1));
      return new GsonResponseBodyConverter(this.gson, var4);
   }
}
