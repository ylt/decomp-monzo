package io.intercom.retrofit2;

import io.intercom.okhttp3.Headers;
import io.intercom.okhttp3.MultipartBody;
import io.intercom.okhttp3.RequestBody;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import javax.annotation.Nullable;

abstract class ParameterHandler {
   abstract void apply(RequestBuilder var1, @Nullable Object var2) throws IOException;

   final ParameterHandler array() {
      return new ParameterHandler() {
         void apply(RequestBuilder var1, @Nullable Object var2) throws IOException {
            if(var2 != null) {
               int var3 = 0;

               for(int var4 = Array.getLength(var2); var3 < var4; ++var3) {
                  ParameterHandler.this.apply(var1, Array.get(var2, var3));
               }
            }

         }
      };
   }

   final ParameterHandler iterable() {
      return new ParameterHandler() {
         void apply(RequestBuilder var1, @Nullable Iterable var2) throws IOException {
            if(var2 != null) {
               Iterator var3 = var2.iterator();

               while(var3.hasNext()) {
                  Object var4 = var3.next();
                  ParameterHandler.this.apply(var1, var4);
               }
            }

         }
      };
   }

   static final class Body extends ParameterHandler {
      private final Converter converter;

      Body(Converter var1) {
         this.converter = var1;
      }

      void apply(RequestBuilder var1, @Nullable Object var2) {
         if(var2 == null) {
            throw new IllegalArgumentException("Body parameter value must not be null.");
         } else {
            RequestBody var3;
            try {
               var3 = (RequestBody)this.converter.convert(var2);
            } catch (IOException var4) {
               throw new RuntimeException("Unable to convert " + var2 + " to RequestBody", var4);
            }

            var1.setBody(var3);
         }
      }
   }

   static final class Field extends ParameterHandler {
      private final boolean encoded;
      private final String name;
      private final Converter valueConverter;

      Field(String var1, Converter var2, boolean var3) {
         this.name = (String)Utils.checkNotNull(var1, "name == null");
         this.valueConverter = var2;
         this.encoded = var3;
      }

      void apply(RequestBuilder var1, @Nullable Object var2) throws IOException {
         if(var2 != null) {
            String var3 = (String)this.valueConverter.convert(var2);
            if(var3 != null) {
               var1.addFormField(this.name, var3, this.encoded);
            }
         }

      }
   }

   static final class FieldMap extends ParameterHandler {
      private final boolean encoded;
      private final Converter valueConverter;

      FieldMap(Converter var1, boolean var2) {
         this.valueConverter = var1;
         this.encoded = var2;
      }

      void apply(RequestBuilder var1, @Nullable Map var2) throws IOException {
         if(var2 == null) {
            throw new IllegalArgumentException("Field map was null.");
         } else {
            Iterator var6 = var2.entrySet().iterator();

            while(var6.hasNext()) {
               Entry var4 = (Entry)var6.next();
               String var3 = (String)var4.getKey();
               if(var3 == null) {
                  throw new IllegalArgumentException("Field map contained null key.");
               }

               Object var5 = var4.getValue();
               if(var5 == null) {
                  throw new IllegalArgumentException("Field map contained null value for key '" + var3 + "'.");
               }

               String var7 = (String)this.valueConverter.convert(var5);
               if(var7 == null) {
                  throw new IllegalArgumentException("Field map value '" + var5 + "' converted to null by " + this.valueConverter.getClass().getName() + " for key '" + var3 + "'.");
               }

               var1.addFormField(var3, var7, this.encoded);
            }

         }
      }
   }

   static final class Header extends ParameterHandler {
      private final String name;
      private final Converter valueConverter;

      Header(String var1, Converter var2) {
         this.name = (String)Utils.checkNotNull(var1, "name == null");
         this.valueConverter = var2;
      }

      void apply(RequestBuilder var1, @Nullable Object var2) throws IOException {
         if(var2 != null) {
            String var3 = (String)this.valueConverter.convert(var2);
            if(var3 != null) {
               var1.addHeader(this.name, var3);
            }
         }

      }
   }

   static final class HeaderMap extends ParameterHandler {
      private final Converter valueConverter;

      HeaderMap(Converter var1) {
         this.valueConverter = var1;
      }

      void apply(RequestBuilder var1, @Nullable Map var2) throws IOException {
         if(var2 == null) {
            throw new IllegalArgumentException("Header map was null.");
         } else {
            Iterator var3 = var2.entrySet().iterator();

            while(var3.hasNext()) {
               Entry var4 = (Entry)var3.next();
               String var5 = (String)var4.getKey();
               if(var5 == null) {
                  throw new IllegalArgumentException("Header map contained null key.");
               }

               Object var6 = var4.getValue();
               if(var6 == null) {
                  throw new IllegalArgumentException("Header map contained null value for key '" + var5 + "'.");
               }

               var1.addHeader(var5, (String)this.valueConverter.convert(var6));
            }

         }
      }
   }

   static final class Part extends ParameterHandler {
      private final Converter converter;
      private final Headers headers;

      Part(Headers var1, Converter var2) {
         this.headers = var1;
         this.converter = var2;
      }

      void apply(RequestBuilder var1, @Nullable Object var2) {
         if(var2 != null) {
            RequestBody var3;
            try {
               var3 = (RequestBody)this.converter.convert(var2);
            } catch (IOException var4) {
               throw new RuntimeException("Unable to convert " + var2 + " to RequestBody", var4);
            }

            var1.addPart(this.headers, var3);
         }

      }
   }

   static final class PartMap extends ParameterHandler {
      private final String transferEncoding;
      private final Converter valueConverter;

      PartMap(Converter var1, String var2) {
         this.valueConverter = var1;
         this.transferEncoding = var2;
      }

      void apply(RequestBuilder var1, @Nullable Map var2) throws IOException {
         if(var2 == null) {
            throw new IllegalArgumentException("Part map was null.");
         } else {
            Iterator var5 = var2.entrySet().iterator();

            while(var5.hasNext()) {
               Entry var4 = (Entry)var5.next();
               String var3 = (String)var4.getKey();
               if(var3 == null) {
                  throw new IllegalArgumentException("Part map contained null key.");
               }

               Object var6 = var4.getValue();
               if(var6 == null) {
                  throw new IllegalArgumentException("Part map contained null value for key '" + var3 + "'.");
               }

               var1.addPart(Headers.of(new String[]{"Content-Disposition", "form-data; name=\"" + var3 + "\"", "Content-Transfer-Encoding", this.transferEncoding}), (RequestBody)this.valueConverter.convert(var6));
            }

         }
      }
   }

   static final class Path extends ParameterHandler {
      private final boolean encoded;
      private final String name;
      private final Converter valueConverter;

      Path(String var1, Converter var2, boolean var3) {
         this.name = (String)Utils.checkNotNull(var1, "name == null");
         this.valueConverter = var2;
         this.encoded = var3;
      }

      void apply(RequestBuilder var1, @Nullable Object var2) throws IOException {
         if(var2 == null) {
            throw new IllegalArgumentException("Path parameter \"" + this.name + "\" value must not be null.");
         } else {
            var1.addPathParam(this.name, (String)this.valueConverter.convert(var2), this.encoded);
         }
      }
   }

   static final class Query extends ParameterHandler {
      private final boolean encoded;
      private final String name;
      private final Converter valueConverter;

      Query(String var1, Converter var2, boolean var3) {
         this.name = (String)Utils.checkNotNull(var1, "name == null");
         this.valueConverter = var2;
         this.encoded = var3;
      }

      void apply(RequestBuilder var1, @Nullable Object var2) throws IOException {
         if(var2 != null) {
            String var3 = (String)this.valueConverter.convert(var2);
            if(var3 != null) {
               var1.addQueryParam(this.name, var3, this.encoded);
            }
         }

      }
   }

   static final class QueryMap extends ParameterHandler {
      private final boolean encoded;
      private final Converter valueConverter;

      QueryMap(Converter var1, boolean var2) {
         this.valueConverter = var1;
         this.encoded = var2;
      }

      void apply(RequestBuilder var1, @Nullable Map var2) throws IOException {
         if(var2 == null) {
            throw new IllegalArgumentException("Query map was null.");
         } else {
            Iterator var3 = var2.entrySet().iterator();

            while(var3.hasNext()) {
               Entry var4 = (Entry)var3.next();
               String var6 = (String)var4.getKey();
               if(var6 == null) {
                  throw new IllegalArgumentException("Query map contained null key.");
               }

               Object var5 = var4.getValue();
               if(var5 == null) {
                  throw new IllegalArgumentException("Query map contained null value for key '" + var6 + "'.");
               }

               String var7 = (String)this.valueConverter.convert(var5);
               if(var7 == null) {
                  throw new IllegalArgumentException("Query map value '" + var5 + "' converted to null by " + this.valueConverter.getClass().getName() + " for key '" + var6 + "'.");
               }

               var1.addQueryParam(var6, var7, this.encoded);
            }

         }
      }
   }

   static final class QueryName extends ParameterHandler {
      private final boolean encoded;
      private final Converter nameConverter;

      QueryName(Converter var1, boolean var2) {
         this.nameConverter = var1;
         this.encoded = var2;
      }

      void apply(RequestBuilder var1, @Nullable Object var2) throws IOException {
         if(var2 != null) {
            var1.addQueryParam((String)this.nameConverter.convert(var2), (String)null, this.encoded);
         }

      }
   }

   static final class RawPart extends ParameterHandler {
      static final ParameterHandler.RawPart INSTANCE = new ParameterHandler.RawPart();

      void apply(RequestBuilder var1, @Nullable MultipartBody.Part var2) throws IOException {
         if(var2 != null) {
            var1.addPart(var2);
         }

      }
   }

   static final class RelativeUrl extends ParameterHandler {
      void apply(RequestBuilder var1, @Nullable Object var2) {
         Utils.checkNotNull(var2, "@Url parameter is null.");
         var1.setRelativeUrl(var2);
      }
   }
}
