package io.intercom.retrofit2;

import io.intercom.okhttp3.HttpUrl;
import io.intercom.okhttp3.OkHttpClient;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import javax.annotation.Nullable;

public final class Retrofit {
   final List adapterFactories;
   final HttpUrl baseUrl;
   final io.intercom.okhttp3.Call.Factory callFactory;
   @Nullable
   final Executor callbackExecutor;
   final List converterFactories;
   private final Map serviceMethodCache = new ConcurrentHashMap();
   final boolean validateEagerly;

   Retrofit(io.intercom.okhttp3.Call.Factory var1, HttpUrl var2, List var3, List var4, @Nullable Executor var5, boolean var6) {
      this.callFactory = var1;
      this.baseUrl = var2;
      this.converterFactories = Collections.unmodifiableList(var3);
      this.adapterFactories = Collections.unmodifiableList(var4);
      this.callbackExecutor = var5;
      this.validateEagerly = var6;
   }

   private void eagerlyValidateMethods(Class var1) {
      Platform var4 = Platform.get();
      Method[] var5 = var1.getDeclaredMethods();
      int var3 = var5.length;

      for(int var2 = 0; var2 < var3; ++var2) {
         Method var6 = var5[var2];
         if(!var4.isDefaultMethod(var6)) {
            this.loadServiceMethod(var6);
         }
      }

   }

   public HttpUrl baseUrl() {
      return this.baseUrl;
   }

   public CallAdapter callAdapter(Type var1, Annotation[] var2) {
      return this.nextCallAdapter((CallAdapter.Factory)null, var1, var2);
   }

   public List callAdapterFactories() {
      return this.adapterFactories;
   }

   public io.intercom.okhttp3.Call.Factory callFactory() {
      return this.callFactory;
   }

   @Nullable
   public Executor callbackExecutor() {
      return this.callbackExecutor;
   }

   public List converterFactories() {
      return this.converterFactories;
   }

   public Object create(final Class var1) {
      Utils.validateServiceInterface(var1);
      if(this.validateEagerly) {
         this.eagerlyValidateMethods(var1);
      }

      ClassLoader var3 = var1.getClassLoader();
      InvocationHandler var2 = new InvocationHandler() {
         private final Platform platform = Platform.get();

         public Object invoke(Object var1x, Method var2, @Nullable Object[] var3) throws Throwable {
            if(var2.getDeclaringClass() == Object.class) {
               var1x = var2.invoke(this, var3);
            } else if(this.platform.isDefaultMethod(var2)) {
               var1x = this.platform.invokeDefaultMethod(var2, var1, var1x, var3);
            } else {
               ServiceMethod var4 = Retrofit.this.loadServiceMethod(var2);
               OkHttpCall var5 = new OkHttpCall(var4, var3);
               var1x = var4.callAdapter.adapt(var5);
            }

            return var1x;
         }
      };
      return Proxy.newProxyInstance(var3, new Class[]{var1}, var2);
   }

   ServiceMethod loadServiceMethod(Method param1) {
      // $FF: Couldn't be decompiled
   }

   public Retrofit.Builder newBuilder() {
      return new Retrofit.Builder(this);
   }

   public CallAdapter nextCallAdapter(@Nullable CallAdapter.Factory var1, Type var2, Annotation[] var3) {
      Utils.checkNotNull(var2, "returnType == null");
      Utils.checkNotNull(var3, "annotations == null");
      int var4 = this.adapterFactories.indexOf(var1) + 1;
      int var6 = this.adapterFactories.size();

      int var5;
      for(var5 = var4; var5 < var6; ++var5) {
         CallAdapter var7 = ((CallAdapter.Factory)this.adapterFactories.get(var5)).get(var2, var3, this);
         if(var7 != null) {
            return var7;
         }
      }

      StringBuilder var8 = (new StringBuilder("Could not locate call adapter for ")).append(var2).append(".\n");
      if(var1 != null) {
         var8.append("  Skipped:");

         for(var5 = 0; var5 < var4; ++var5) {
            var8.append("\n   * ").append(((CallAdapter.Factory)this.adapterFactories.get(var5)).getClass().getName());
         }

         var8.append('\n');
      }

      var8.append("  Tried:");

      for(var5 = this.adapterFactories.size(); var4 < var5; ++var4) {
         var8.append("\n   * ").append(((CallAdapter.Factory)this.adapterFactories.get(var4)).getClass().getName());
      }

      throw new IllegalArgumentException(var8.toString());
   }

   public Converter nextRequestBodyConverter(@Nullable Converter.Factory var1, Type var2, Annotation[] var3, Annotation[] var4) {
      Utils.checkNotNull(var2, "type == null");
      Utils.checkNotNull(var3, "parameterAnnotations == null");
      Utils.checkNotNull(var4, "methodAnnotations == null");
      int var5 = this.converterFactories.indexOf(var1) + 1;
      int var7 = this.converterFactories.size();

      int var6;
      for(var6 = var5; var6 < var7; ++var6) {
         Converter var8 = ((Converter.Factory)this.converterFactories.get(var6)).requestBodyConverter(var2, var3, var4, this);
         if(var8 != null) {
            return var8;
         }
      }

      StringBuilder var9 = (new StringBuilder("Could not locate RequestBody converter for ")).append(var2).append(".\n");
      if(var1 != null) {
         var9.append("  Skipped:");

         for(var6 = 0; var6 < var5; ++var6) {
            var9.append("\n   * ").append(((Converter.Factory)this.converterFactories.get(var6)).getClass().getName());
         }

         var9.append('\n');
      }

      var9.append("  Tried:");

      for(var6 = this.converterFactories.size(); var5 < var6; ++var5) {
         var9.append("\n   * ").append(((Converter.Factory)this.converterFactories.get(var5)).getClass().getName());
      }

      throw new IllegalArgumentException(var9.toString());
   }

   public Converter nextResponseBodyConverter(@Nullable Converter.Factory var1, Type var2, Annotation[] var3) {
      Utils.checkNotNull(var2, "type == null");
      Utils.checkNotNull(var3, "annotations == null");
      int var4 = this.converterFactories.indexOf(var1) + 1;
      int var6 = this.converterFactories.size();

      int var5;
      for(var5 = var4; var5 < var6; ++var5) {
         Converter var7 = ((Converter.Factory)this.converterFactories.get(var5)).responseBodyConverter(var2, var3, this);
         if(var7 != null) {
            return var7;
         }
      }

      StringBuilder var8 = (new StringBuilder("Could not locate ResponseBody converter for ")).append(var2).append(".\n");
      if(var1 != null) {
         var8.append("  Skipped:");

         for(var5 = 0; var5 < var4; ++var5) {
            var8.append("\n   * ").append(((Converter.Factory)this.converterFactories.get(var5)).getClass().getName());
         }

         var8.append('\n');
      }

      var8.append("  Tried:");

      for(var5 = this.converterFactories.size(); var4 < var5; ++var4) {
         var8.append("\n   * ").append(((Converter.Factory)this.converterFactories.get(var4)).getClass().getName());
      }

      throw new IllegalArgumentException(var8.toString());
   }

   public Converter requestBodyConverter(Type var1, Annotation[] var2, Annotation[] var3) {
      return this.nextRequestBodyConverter((Converter.Factory)null, var1, var2, var3);
   }

   public Converter responseBodyConverter(Type var1, Annotation[] var2) {
      return this.nextResponseBodyConverter((Converter.Factory)null, var1, var2);
   }

   public Converter stringConverter(Type var1, Annotation[] var2) {
      Utils.checkNotNull(var1, "type == null");
      Utils.checkNotNull(var2, "annotations == null");
      int var4 = this.converterFactories.size();
      int var3 = 0;

      Object var6;
      while(true) {
         if(var3 >= var4) {
            var6 = BuiltInConverters.ToStringConverter.INSTANCE;
            break;
         }

         Converter var5 = ((Converter.Factory)this.converterFactories.get(var3)).stringConverter(var1, var2, this);
         if(var5 != null) {
            var6 = var5;
            break;
         }

         ++var3;
      }

      return (Converter)var6;
   }

   public static final class Builder {
      private final List adapterFactories;
      private HttpUrl baseUrl;
      @Nullable
      private io.intercom.okhttp3.Call.Factory callFactory;
      @Nullable
      private Executor callbackExecutor;
      private final List converterFactories;
      private final Platform platform;
      private boolean validateEagerly;

      public Builder() {
         this(Platform.get());
      }

      Builder(Platform var1) {
         this.converterFactories = new ArrayList();
         this.adapterFactories = new ArrayList();
         this.platform = var1;
         this.converterFactories.add(new BuiltInConverters());
      }

      Builder(Retrofit var1) {
         this.converterFactories = new ArrayList();
         this.adapterFactories = new ArrayList();
         this.platform = Platform.get();
         this.callFactory = var1.callFactory;
         this.baseUrl = var1.baseUrl;
         this.converterFactories.addAll(var1.converterFactories);
         this.adapterFactories.addAll(var1.adapterFactories);
         this.adapterFactories.remove(this.adapterFactories.size() - 1);
         this.callbackExecutor = var1.callbackExecutor;
         this.validateEagerly = var1.validateEagerly;
      }

      public Retrofit.Builder addCallAdapterFactory(CallAdapter.Factory var1) {
         this.adapterFactories.add(Utils.checkNotNull(var1, "factory == null"));
         return this;
      }

      public Retrofit.Builder addConverterFactory(Converter.Factory var1) {
         this.converterFactories.add(Utils.checkNotNull(var1, "factory == null"));
         return this;
      }

      public Retrofit.Builder baseUrl(HttpUrl var1) {
         Utils.checkNotNull(var1, "baseUrl == null");
         List var2 = var1.pathSegments();
         if(!"".equals(var2.get(var2.size() - 1))) {
            throw new IllegalArgumentException("baseUrl must end in /: " + var1);
         } else {
            this.baseUrl = var1;
            return this;
         }
      }

      public Retrofit.Builder baseUrl(String var1) {
         Utils.checkNotNull(var1, "baseUrl == null");
         HttpUrl var2 = HttpUrl.parse(var1);
         if(var2 == null) {
            throw new IllegalArgumentException("Illegal URL: " + var1);
         } else {
            return this.baseUrl(var2);
         }
      }

      public Retrofit build() {
         if(this.baseUrl == null) {
            throw new IllegalStateException("Base URL required.");
         } else {
            io.intercom.okhttp3.Call.Factory var2 = this.callFactory;
            Object var1 = var2;
            if(var2 == null) {
               var1 = new OkHttpClient();
            }

            Executor var3 = this.callbackExecutor;
            Executor var5 = var3;
            if(var3 == null) {
               var5 = this.platform.defaultCallbackExecutor();
            }

            ArrayList var4 = new ArrayList(this.adapterFactories);
            var4.add(this.platform.defaultCallAdapterFactory(var5));
            ArrayList var6 = new ArrayList(this.converterFactories);
            return new Retrofit((io.intercom.okhttp3.Call.Factory)var1, this.baseUrl, var6, var4, var5, this.validateEagerly);
         }
      }

      public Retrofit.Builder callFactory(io.intercom.okhttp3.Call.Factory var1) {
         this.callFactory = (io.intercom.okhttp3.Call.Factory)Utils.checkNotNull(var1, "factory == null");
         return this;
      }

      public Retrofit.Builder callbackExecutor(Executor var1) {
         this.callbackExecutor = (Executor)Utils.checkNotNull(var1, "executor == null");
         return this;
      }

      public Retrofit.Builder client(OkHttpClient var1) {
         return this.callFactory((io.intercom.okhttp3.Call.Factory)Utils.checkNotNull(var1, "client == null"));
      }

      public Retrofit.Builder validateEagerly(boolean var1) {
         this.validateEagerly = var1;
         return this;
      }
   }
}
