package io.intercom.retrofit2;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

final class DefaultCallAdapterFactory extends CallAdapter.Factory {
   static final CallAdapter.Factory INSTANCE = new DefaultCallAdapterFactory();

   public CallAdapter get(Type var1, Annotation[] var2, Retrofit var3) {
      CallAdapter var4;
      if(getRawType(var1) != Call.class) {
         var4 = null;
      } else {
         var4 = new CallAdapter(Utils.getCallResponseType(var1)) {
            // $FF: synthetic field
            final Type val$responseType;

            {
               this.val$responseType = var2;
            }

            public Call adapt(Call var1) {
               return var1;
            }

            public Type responseType() {
               return this.val$responseType;
            }
         };
      }

      return var4;
   }
}
