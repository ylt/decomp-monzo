package io.intercom.retrofit2;

public class HttpException extends RuntimeException {
   private final int code;
   private final String message;
   private final transient Response response;

   public HttpException(Response var1) {
      super(getMessage(var1));
      this.code = var1.code();
      this.message = var1.message();
      this.response = var1;
   }

   private static String getMessage(Response var0) {
      Utils.checkNotNull(var0, "response == null");
      return "HTTP " + var0.code() + " " + var0.message();
   }

   public int code() {
      return this.code;
   }

   public String message() {
      return this.message;
   }

   public Response response() {
      return this.response;
   }
}
