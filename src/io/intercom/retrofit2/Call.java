package io.intercom.retrofit2;

import io.intercom.okhttp3.Request;
import java.io.IOException;

public interface Call extends Cloneable {
   void cancel();

   Call clone();

   void enqueue(Callback var1);

   Response execute() throws IOException;

   boolean isCanceled();

   boolean isExecuted();

   Request request();
}
