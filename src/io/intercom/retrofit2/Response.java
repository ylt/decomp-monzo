package io.intercom.retrofit2;

import io.intercom.okhttp3.Headers;
import io.intercom.okhttp3.Protocol;
import io.intercom.okhttp3.Request;
import io.intercom.okhttp3.ResponseBody;
import javax.annotation.Nullable;

public final class Response {
   @Nullable
   private final Object body;
   @Nullable
   private final ResponseBody errorBody;
   private final io.intercom.okhttp3.Response rawResponse;

   private Response(io.intercom.okhttp3.Response var1, @Nullable Object var2, @Nullable ResponseBody var3) {
      this.rawResponse = var1;
      this.body = var2;
      this.errorBody = var3;
   }

   public static Response error(int var0, ResponseBody var1) {
      if(var0 < 400) {
         throw new IllegalArgumentException("code < 400: " + var0);
      } else {
         return error(var1, (new io.intercom.okhttp3.Response.Builder()).code(var0).message("Response.error()").protocol(Protocol.HTTP_1_1).request((new Request.Builder()).url("http://localhost/").build()).build());
      }
   }

   public static Response error(ResponseBody var0, io.intercom.okhttp3.Response var1) {
      Utils.checkNotNull(var0, "body == null");
      Utils.checkNotNull(var1, "rawResponse == null");
      if(var1.isSuccessful()) {
         throw new IllegalArgumentException("rawResponse should not be successful response");
      } else {
         return new Response(var1, (Object)null, var0);
      }
   }

   public static Response success(@Nullable Object var0) {
      return success(var0, (new io.intercom.okhttp3.Response.Builder()).code(200).message("OK").protocol(Protocol.HTTP_1_1).request((new Request.Builder()).url("http://localhost/").build()).build());
   }

   public static Response success(@Nullable Object var0, Headers var1) {
      Utils.checkNotNull(var1, "headers == null");
      return success(var0, (new io.intercom.okhttp3.Response.Builder()).code(200).message("OK").protocol(Protocol.HTTP_1_1).headers(var1).request((new Request.Builder()).url("http://localhost/").build()).build());
   }

   public static Response success(@Nullable Object var0, io.intercom.okhttp3.Response var1) {
      Utils.checkNotNull(var1, "rawResponse == null");
      if(!var1.isSuccessful()) {
         throw new IllegalArgumentException("rawResponse must be successful response");
      } else {
         return new Response(var1, var0, (ResponseBody)null);
      }
   }

   @Nullable
   public Object body() {
      return this.body;
   }

   public int code() {
      return this.rawResponse.code();
   }

   @Nullable
   public ResponseBody errorBody() {
      return this.errorBody;
   }

   public Headers headers() {
      return this.rawResponse.headers();
   }

   public boolean isSuccessful() {
      return this.rawResponse.isSuccessful();
   }

   public String message() {
      return this.rawResponse.message();
   }

   public io.intercom.okhttp3.Response raw() {
      return this.rawResponse;
   }

   public String toString() {
      return this.rawResponse.toString();
   }
}
