package io.intercom.retrofit2;

public interface Callback {
   void onFailure(Call var1, Throwable var2);

   void onResponse(Call var1, Response var2);
}
