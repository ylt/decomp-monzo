package io.intercom.android.sdk.identity;

import android.text.TextUtils;
import io.intercom.android.sdk.UserAttributes;
import io.intercom.android.sdk.logger.LumberMill;
import io.intercom.android.sdk.twig.Twig;

public class Registration {
   private UserAttributes attributes;
   private String email = "";
   private final Twig twig = LumberMill.getLogger();
   private String userId = "";
   private Registration.Validity validity;

   public Registration() {
      this.validity = Registration.Validity.NOT_SET;
   }

   public static Registration create() {
      return new Registration();
   }

   private void updateState(boolean var1) {
      if(this.validity == Registration.Validity.NOT_SET || this.validity == Registration.Validity.VALID) {
         Registration.Validity var2;
         if(var1) {
            var2 = Registration.Validity.VALID;
         } else {
            var2 = Registration.Validity.INVALID;
         }

         this.validity = var2;
      }

   }

   public UserAttributes getAttributes() {
      return this.attributes;
   }

   public String getEmail() {
      return this.email;
   }

   public String getUserId() {
      return this.userId;
   }

   boolean isValidRegistration() {
      return Registration.Validity.VALID.equals(this.validity);
   }

   public Registration withEmail(String var1) {
      boolean var2;
      if(!TextUtils.isEmpty(var1)) {
         var2 = true;
      } else {
         var2 = false;
      }

      if(var2) {
         this.email = var1;
      } else {
         this.twig.e("Email cannot be null or empty", new Object[0]);
      }

      this.updateState(var2);
      return this;
   }

   public Registration withUserAttributes(UserAttributes var1) {
      if(var1 == null) {
         this.validity = Registration.Validity.INVALID;
         this.twig.e("Registration.withUserAttributes method failed: the attributes Map provided is null", new Object[0]);
      } else if(var1.isEmpty()) {
         this.validity = Registration.Validity.INVALID;
         this.twig.e("Registration.withUserAttributes method failed: the attributes Map provided is empty", new Object[0]);
      } else {
         this.attributes = var1;
      }

      return this;
   }

   public Registration withUserId(String var1) {
      boolean var2;
      if(!TextUtils.isEmpty(var1)) {
         var2 = true;
      } else {
         var2 = false;
      }

      if(var2) {
         this.userId = var1;
      } else {
         this.twig.e("UserId cannot be null or empty", new Object[0]);
      }

      this.updateState(var2);
      return this;
   }

   private static enum Validity {
      INVALID,
      NOT_SET,
      VALID;
   }
}
