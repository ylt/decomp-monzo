package io.intercom.android.sdk.identity;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.SOURCE)
public @interface FeatureFlag {
   String CONVERSATION_WEB_VIEW = "conversation-web-view";
   String IMAGE_ANNOTATION = "image-annotation";
   String SCREENSHOT_SHARING = "screenshot-sharing";
}
