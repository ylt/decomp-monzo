package io.intercom.android.sdk.identity;

final class AutoValue_SoftUserIdentity extends SoftUserIdentity {
   private final String anonymousId;
   private final String data;
   private final String email;
   private final String fingerprint;
   private final String hmac;
   private final String intercomId;
   private final String userId;

   AutoValue_SoftUserIdentity(String var1, String var2, String var3, String var4, String var5, String var6, String var7) {
      if(var1 == null) {
         throw new NullPointerException("Null anonymousId");
      } else {
         this.anonymousId = var1;
         if(var2 == null) {
            throw new NullPointerException("Null data");
         } else {
            this.data = var2;
            if(var3 == null) {
               throw new NullPointerException("Null email");
            } else {
               this.email = var3;
               if(var4 == null) {
                  throw new NullPointerException("Null fingerprint");
               } else {
                  this.fingerprint = var4;
                  if(var5 == null) {
                     throw new NullPointerException("Null hmac");
                  } else {
                     this.hmac = var5;
                     if(var6 == null) {
                        throw new NullPointerException("Null intercomId");
                     } else {
                        this.intercomId = var6;
                        if(var7 == null) {
                           throw new NullPointerException("Null userId");
                        } else {
                           this.userId = var7;
                        }
                     }
                  }
               }
            }
         }
      }
   }

   String anonymousId() {
      return this.anonymousId;
   }

   String data() {
      return this.data;
   }

   String email() {
      return this.email;
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(var1 != this) {
         if(var1 instanceof SoftUserIdentity) {
            SoftUserIdentity var3 = (SoftUserIdentity)var1;
            if(!this.anonymousId.equals(var3.anonymousId()) || !this.data.equals(var3.data()) || !this.email.equals(var3.email()) || !this.fingerprint.equals(var3.fingerprint()) || !this.hmac.equals(var3.hmac()) || !this.intercomId.equals(var3.intercomId()) || !this.userId.equals(var3.userId())) {
               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   String fingerprint() {
      return this.fingerprint;
   }

   public int hashCode() {
      return ((((((this.anonymousId.hashCode() ^ 1000003) * 1000003 ^ this.data.hashCode()) * 1000003 ^ this.email.hashCode()) * 1000003 ^ this.fingerprint.hashCode()) * 1000003 ^ this.hmac.hashCode()) * 1000003 ^ this.intercomId.hashCode()) * 1000003 ^ this.userId.hashCode();
   }

   String hmac() {
      return this.hmac;
   }

   String intercomId() {
      return this.intercomId;
   }

   public String toString() {
      return "SoftUserIdentity{anonymousId=" + this.anonymousId + ", data=" + this.data + ", email=" + this.email + ", fingerprint=" + this.fingerprint + ", hmac=" + this.hmac + ", intercomId=" + this.intercomId + ", userId=" + this.userId + "}";
   }

   String userId() {
      return this.userId;
   }
}
