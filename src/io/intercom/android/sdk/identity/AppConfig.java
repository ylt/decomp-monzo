package io.intercom.android.sdk.identity;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v4.content.a;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.models.Config;
import io.intercom.android.sdk.models.events.ConfigUpdateEvent;
import io.intercom.android.sdk.nexus.NexusConfig;
import io.intercom.android.sdk.utilities.ColorUtils;
import io.intercom.com.a.a.b;
import java.util.HashSet;
import java.util.Set;

public class AppConfig {
   private static final String APP_AUDIO_ENABLED = "app_audio_enabled";
   private static final String APP_BACKGROUND_REQUESTS_ENABLED = "app_background_requests_enabled";
   private static final String APP_BATCH_USER_UPDATE_PERIOD_MS = "batch_user_update_period_ms";
   private static final String APP_FEATURES = "features";
   private static final String APP_INBOUND_MESSAGES = "app_inbound_messages";
   private static final String APP_INTERCOM_LINK = "app_intercom_link";
   private static final String APP_LOCALE = "app_locale";
   private static final String APP_METRICS_ENABLED = "app_metrics_enabled";
   private static final String APP_NAME = "app_name";
   private static final String APP_NEW_SESSION_THRESHOLD_MS = "new_session_threshold_ms";
   private static final String APP_PING_DELAY_MS = "ping_delay_ms";
   private static final String APP_PRIMARY_COLOR = "app_primary_color";
   private static final String APP_RATE_LIMIT_COUNT = "app_rate_limit_count";
   private static final String APP_RATE_LIMIT_PERIOD_MS = "app_rate_limit_period_ms";
   private static final String APP_RECEIVED_FROM_SERVER = "app_received_from_server";
   private static final String APP_SOFT_RESET_TIMEOUT_MS = "app_soft_reset_timeout_ms";
   private static final String APP_TEAM_BIO = "app_team_bio";
   private static final String APP_USER_UPDATE_CACHE_MAX_AGE_MS = "app_user_update_cache_max_age_ms";
   private static final String APP_WALLPAPER = "app_wallpaper";
   private boolean audioEnabled;
   private boolean backgroundRequestsEnabled;
   private int baseColor;
   private int baseColorDark;
   private long batchUserUpdatePeriodMs;
   private final int defaultColor;
   private Set features = new HashSet();
   private boolean inboundMessages;
   private String locale = "";
   private boolean metricsEnabled;
   private String name = "";
   private long newSessionThresholdMs;
   private long pingDelayMs;
   private final SharedPreferences prefs;
   private int rateLimitCount;
   private long rateLimitPeriodMs;
   private NexusConfig realTimeConfig = new NexusConfig();
   private boolean receivedFromServer;
   private boolean showIntercomLink;
   private long softResetTimeoutMs;
   private String teamProfileBio = "";
   private long userUpdateCacheMaxAgeMs;
   private String wallpaper = "";

   public AppConfig(Context var1) {
      this.defaultColor = a.c(var1, R.color.intercom_main_blue);
      this.prefs = var1.getSharedPreferences("INTERCOM_SDK_PREFS", 0);
      this.name = this.prefs.getString("app_name", "");
      this.baseColor = this.prefs.getInt("app_primary_color", this.defaultColor);
      this.baseColorDark = ColorUtils.darkenColor(this.baseColor);
      this.showIntercomLink = this.prefs.getBoolean("app_intercom_link", true);
      this.inboundMessages = this.prefs.getBoolean("app_inbound_messages", false);
      this.rateLimitCount = this.prefs.getInt("app_rate_limit_count", 100);
      this.rateLimitPeriodMs = this.prefs.getLong("app_rate_limit_period_ms", Config.DEFAULT_RATE_LIMIT_PERIOD_MS);
      this.userUpdateCacheMaxAgeMs = this.prefs.getLong("app_user_update_cache_max_age_ms", Config.DEFAULT_CACHE_MAX_AGE_MS);
      this.newSessionThresholdMs = this.prefs.getLong("new_session_threshold_ms", Config.DEFAULT_SESSION_TIMEOUT_MS);
      this.softResetTimeoutMs = this.prefs.getLong("app_soft_reset_timeout_ms", Config.DEFAULT_SOFT_RESET_TIMEOUT_MS);
      this.batchUserUpdatePeriodMs = this.prefs.getLong("batch_user_update_period_ms", Config.DEFAULT_BATCH_USER_UPDATE_PERIOD_MS);
      this.pingDelayMs = this.prefs.getLong("ping_delay_ms", Config.DEFAULT_PING_DELAY_MS);
      this.metricsEnabled = this.prefs.getBoolean("app_metrics_enabled", true);
      this.audioEnabled = this.prefs.getBoolean("app_audio_enabled", true);
      this.teamProfileBio = this.prefs.getString("app_team_bio", "");
      this.wallpaper = this.prefs.getString("app_wallpaper", "");
      this.locale = this.prefs.getString("app_locale", "");
      this.receivedFromServer = this.prefs.getBoolean("app_received_from_server", false);
      this.backgroundRequestsEnabled = this.prefs.getBoolean("app_background_requests_enabled", true);
      this.features = this.prefs.getStringSet("features", new HashSet());
   }

   private int getConfigColor(String var1) {
      int var2;
      if(var1 == null) {
         var2 = this.defaultColor;
      } else {
         var2 = Color.parseColor(var1);
      }

      return var2;
   }

   private boolean isNewConfig(Config var1) {
      boolean var2;
      if(var1.getWelcomeMessage().equals(this.teamProfileBio) && var1.getMessengerBackground().equals(this.wallpaper) && var1.getName().equals(this.name) && var1.getLocale().equals(this.locale) && this.getConfigColor(var1.getBaseColor()) == this.baseColor && var1.getUserUpdateCacheMaxAge() == this.userUpdateCacheMaxAgeMs && var1.isMetricsEnabled() == this.metricsEnabled && var1.isAudioEnabled() == this.audioEnabled && var1.isShowPoweredBy() == this.showIntercomLink && var1.isInboundMessages() == this.inboundMessages && var1.getRateLimitCount() == this.rateLimitCount && var1.getRateLimitPeriod() == this.rateLimitPeriodMs && var1.getNewSessionThreshold() == this.newSessionThresholdMs && var1.getSoftResetTimeout() == this.softResetTimeoutMs && var1.getBatchUserUpdatePeriod() == this.batchUserUpdatePeriodMs && var1.getPingDelayMs() == this.pingDelayMs && var1.isBackgroundRequestsEnabled() == this.backgroundRequestsEnabled && var1.getFeatures().equals(this.features)) {
         var2 = false;
      } else {
         var2 = true;
      }

      return var2;
   }

   public boolean backgroundRequestsDisabled() {
      boolean var1;
      if(!this.backgroundRequestsEnabled) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public int getBaseColor() {
      return this.baseColor;
   }

   public int getBaseColorDark() {
      return this.baseColorDark;
   }

   public long getBatchUserUpdatePeriodMs() {
      return this.batchUserUpdatePeriodMs;
   }

   public String getLocale() {
      return this.locale;
   }

   public String getName() {
      return this.name;
   }

   public long getNewSessionThresholdMs() {
      return this.newSessionThresholdMs;
   }

   public long getPingDelayMs() {
      return this.pingDelayMs;
   }

   public int getRateLimitCount() {
      return this.rateLimitCount;
   }

   public long getRateLimitPeriodMs() {
      return this.rateLimitPeriodMs;
   }

   public NexusConfig getRealTimeConfig() {
      return this.realTimeConfig;
   }

   public long getSoftResetTimeoutMs() {
      return this.softResetTimeoutMs;
   }

   public String getTeamProfileBio() {
      return this.teamProfileBio;
   }

   public long getUserUpdateCacheMaxAgeMs() {
      return this.userUpdateCacheMaxAgeMs;
   }

   public String getWallpaper() {
      return this.wallpaper;
   }

   public boolean hasFeature(String var1) {
      return this.features.contains(var1);
   }

   public boolean isAudioEnabled() {
      return this.audioEnabled;
   }

   public boolean isInboundMessages() {
      return this.inboundMessages;
   }

   public boolean isMetricsEnabled() {
      return this.metricsEnabled;
   }

   public boolean isReceivedFromServer() {
      return this.receivedFromServer;
   }

   public void resetRealTimeConfig() {
      this.realTimeConfig = new NexusConfig();
   }

   public boolean shouldShowIntercomLink() {
      return this.showIntercomLink;
   }

   public void update(Config var1, b var2) {
      if(var1 != Config.NULL) {
         this.realTimeConfig = var1.getRealTimeConfig();
         this.receivedFromServer = true;
         if(this.isNewConfig(var1)) {
            this.name = var1.getName();
            this.teamProfileBio = var1.getWelcomeMessage();
            this.wallpaper = var1.getMessengerBackground();
            this.baseColor = this.getConfigColor(var1.getBaseColor());
            this.baseColorDark = ColorUtils.darkenColor(this.baseColor);
            this.inboundMessages = var1.isInboundMessages();
            this.showIntercomLink = var1.isShowPoweredBy();
            this.audioEnabled = var1.isAudioEnabled();
            this.metricsEnabled = var1.isMetricsEnabled();
            this.userUpdateCacheMaxAgeMs = var1.getUserUpdateCacheMaxAge();
            this.rateLimitPeriodMs = var1.getRateLimitPeriod();
            this.rateLimitCount = var1.getRateLimitCount();
            this.newSessionThresholdMs = var1.getNewSessionThreshold();
            this.softResetTimeoutMs = var1.getSoftResetTimeout();
            this.batchUserUpdatePeriodMs = var1.getBatchUserUpdatePeriod();
            this.pingDelayMs = var1.getPingDelayMs();
            this.backgroundRequestsEnabled = var1.isBackgroundRequestsEnabled();
            this.locale = var1.getLocale();
            this.features = var1.getFeatures();
            this.prefs.edit().putString("app_name", this.name).putString("app_team_bio", this.teamProfileBio).putString("app_wallpaper", this.wallpaper).putString("app_locale", this.locale).putInt("app_primary_color", this.baseColor).putInt("app_rate_limit_count", this.rateLimitCount).putLong("app_user_update_cache_max_age_ms", this.userUpdateCacheMaxAgeMs).putLong("app_rate_limit_period_ms", this.rateLimitPeriodMs).putLong("new_session_threshold_ms", this.newSessionThresholdMs).putLong("batch_user_update_period_ms", this.batchUserUpdatePeriodMs).putLong("ping_delay_ms", this.pingDelayMs).putLong("app_soft_reset_timeout_ms", this.softResetTimeoutMs).putBoolean("app_intercom_link", this.showIntercomLink).putBoolean("app_inbound_messages", this.inboundMessages).putBoolean("app_audio_enabled", this.audioEnabled).putBoolean("app_metrics_enabled", this.metricsEnabled).putBoolean("app_received_from_server", true).putBoolean("app_background_requests_enabled", this.backgroundRequestsEnabled).putStringSet("features", this.features).apply();
            var2.post(new ConfigUpdateEvent());
         }
      }

   }
}
