package io.intercom.android.sdk.identity;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import io.intercom.android.sdk.models.User;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class UserIdentity {
   public static final String ANONYMOUS_ID = "anonymous_id";
   public static final String EMAIL = "email";
   public static final String INTERCOM_ID = "intercom_id";
   private static final String KEY_ANONYMOUS_ID = "INTERCOM_SDK_ANONYMOUS_ID";
   private static final String KEY_EMAIL_ID = "INTERCOM_SDK_EMAIL_ID";
   private static final String KEY_INTERCOM_ID = "INTERCOM_SDK_INTERCOM_ID";
   private static final String KEY_PREFIX = "intercomsdk-session-";
   private static final String KEY_SECURE_DATA = "SecureMode_Data";
   private static final String KEY_SECURE_HMAC = "SecureMode_HMAC";
   private static final String KEY_USER_ID = "INTERCOM_SDK_USER_ID";
   private static final String TYPE = "type";
   private static final String USER = "user";
   private static final String USER_ID = "user_id";
   private String anonymousId;
   private String data;
   private String email;
   private String fingerprint = "";
   private String hmac;
   private String intercomId;
   private final SharedPreferences prefs;
   private SoftUserIdentity softUserIdentity;
   private String userId;

   public UserIdentity(Context var1) {
      this.softUserIdentity = SoftUserIdentity.NONE;
      this.prefs = var1.getSharedPreferences("INTERCOM_SDK_USER_PREFS", 0);
      this.anonymousId = this.prefs.getString("intercomsdk-session-INTERCOM_SDK_ANONYMOUS_ID", "");
      this.intercomId = this.prefs.getString("intercomsdk-session-INTERCOM_SDK_INTERCOM_ID", "");
      this.userId = this.prefs.getString("intercomsdk-session-INTERCOM_SDK_USER_ID", "");
      this.email = this.prefs.getString("intercomsdk-session-INTERCOM_SDK_EMAIL_ID", "");
      this.data = this.prefs.getString("intercomsdk-session-SecureMode_Data", "");
      this.hmac = this.prefs.getString("intercomsdk-session-SecureMode_HMAC", "");
      if(this.identityExists()) {
         this.fingerprint = this.generateFingerprint();
      }

   }

   private String generateFingerprint() {
      return UUID.randomUUID().toString();
   }

   public boolean canRegisterIdentifiedUser(Registration var1) {
      boolean var2;
      if(var1.isValidRegistration() && !this.isIdentified()) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   public boolean canRegisterUnidentifiedUser() {
      boolean var1;
      if(!this.identityExists()) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public String getAnonymousId() {
      return this.anonymousId;
   }

   public String getData() {
      return this.data;
   }

   public String getEmail() {
      return this.email;
   }

   public String getFingerprint() {
      return this.fingerprint;
   }

   public String getHmac() {
      return this.hmac;
   }

   public String getIntercomId() {
      return this.intercomId;
   }

   public String getSoftUserIdentityHmac() {
      return this.softUserIdentity.hmac();
   }

   public String getUserId() {
      return this.userId;
   }

   public void hardReset() {
      this.softUserIdentity = SoftUserIdentity.NONE;
   }

   public boolean hasIntercomId() {
      boolean var1;
      if(!this.getIntercomId().isEmpty()) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public boolean identityExists() {
      boolean var1;
      if(this.email.isEmpty() && this.userId.isEmpty() && this.intercomId.isEmpty() && this.anonymousId.isEmpty()) {
         var1 = false;
      } else {
         var1 = true;
      }

      return var1;
   }

   public boolean isIdentified() {
      boolean var1;
      if(this.identityExists() && !this.isUnidentified()) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public boolean isSameUser(Registration var1) {
      boolean var2;
      if(this.isUnidentified()) {
         var2 = false;
      } else {
         var2 = this.softUserIdentity.isSameUser(var1);
      }

      return var2;
   }

   public boolean isSoftReset() {
      return this.softUserIdentity.isPresent();
   }

   public boolean isUnidentified() {
      boolean var1;
      if(!this.anonymousId.isEmpty() && this.email.isEmpty() && this.userId.isEmpty()) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public void registerIdentifiedUser(Registration var1) {
      this.intercomId = "";
      Editor var2 = this.prefs.edit().putString("intercomsdk-session-INTERCOM_SDK_INTERCOM_ID", this.intercomId);
      if(!var1.getUserId().isEmpty()) {
         this.userId = var1.getUserId();
         var2.putString("intercomsdk-session-INTERCOM_SDK_USER_ID", this.userId);
      }

      if(!var1.getEmail().isEmpty()) {
         this.email = var1.getEmail();
         var2.putString("intercomsdk-session-INTERCOM_SDK_EMAIL_ID", this.email);
      }

      var2.apply();
      if(this.fingerprint.isEmpty()) {
         this.fingerprint = this.generateFingerprint();
      }

   }

   public void registerUnidentifiedUser() {
      this.anonymousId = UUID.randomUUID().toString();
      this.prefs.edit().putString("intercomsdk-session-INTERCOM_SDK_ANONYMOUS_ID", this.anonymousId).apply();
      if(this.fingerprint.isEmpty()) {
         this.fingerprint = this.generateFingerprint();
      }

   }

   public boolean registrationHasAttributes(Registration var1) {
      boolean var2;
      if(var1 != null && var1.getAttributes() != null && !var1.getAttributes().isEmpty()) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   public void setSecureMode(String var1, String var2) {
      this.data = var2;
      this.hmac = var1;
      this.prefs.edit().putString("intercomsdk-session-SecureMode_Data", var2).putString("intercomsdk-session-SecureMode_HMAC", var1).apply();
   }

   public void setUserHash(String var1) {
      this.hmac = var1;
      this.data = "";
      this.prefs.edit().putString("intercomsdk-session-SecureMode_HMAC", var1).remove("intercomsdk-session-SecureMode_Data").apply();
   }

   public void softReset() {
      if(!this.isSoftReset()) {
         this.softUserIdentity = SoftUserIdentity.create(this.anonymousId, this.data, this.email, this.fingerprint, this.hmac, this.intercomId, this.userId);
         this.prefs.edit().clear().apply();
         this.anonymousId = "";
         this.intercomId = "";
         this.userId = "";
         this.email = "";
         this.data = "";
         this.hmac = "";
         this.fingerprint = "";
      }

   }

   public void softRestart() {
      this.userId = this.softUserIdentity.userId();
      this.email = this.softUserIdentity.email();
      this.anonymousId = this.softUserIdentity.anonymousId();
      this.intercomId = this.softUserIdentity.intercomId();
      this.data = this.softUserIdentity.data();
      this.hmac = this.softUserIdentity.hmac();
      this.fingerprint = this.softUserIdentity.fingerprint();
      this.prefs.edit().putString("intercomsdk-session-INTERCOM_SDK_USER_ID", this.userId).putString("intercomsdk-session-INTERCOM_SDK_EMAIL_ID", this.email).putString("intercomsdk-session-INTERCOM_SDK_ANONYMOUS_ID", this.anonymousId).putString("intercomsdk-session-INTERCOM_SDK_INTERCOM_ID", this.intercomId).apply();
      this.softUserIdentity = SoftUserIdentity.NONE;
   }

   public boolean softUserIdentityHmacDiffers(String var1) {
      boolean var2;
      if(this.softUserIdentity.isPresent() && !this.getSoftUserIdentityHmac().equals(var1)) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   public Map toMap() {
      HashMap var1 = new HashMap();
      if(!this.anonymousId.isEmpty()) {
         var1.put("anonymous_id", this.anonymousId);
      } else if(!this.intercomId.isEmpty()) {
         var1.put("intercom_id", this.intercomId);
      }

      if(!this.userId.isEmpty()) {
         var1.put("user_id", this.userId);
      }

      if(!this.email.isEmpty()) {
         var1.put("email", this.email);
      }

      var1.put("type", "user");
      return var1;
   }

   public void update(User var1) {
      if(var1 != User.NULL) {
         this.userId = var1.getUserId();
         this.email = var1.getEmail();
         this.anonymousId = var1.getAnonymousId();
         Editor var2 = this.prefs.edit().putString("intercomsdk-session-INTERCOM_SDK_USER_ID", this.userId).putString("intercomsdk-session-INTERCOM_SDK_EMAIL_ID", this.email).putString("intercomsdk-session-INTERCOM_SDK_ANONYMOUS_ID", this.anonymousId);
         if(!var1.getIntercomId().isEmpty()) {
            this.intercomId = var1.getIntercomId();
            var2.putString("intercomsdk-session-INTERCOM_SDK_INTERCOM_ID", this.intercomId);
         }

         var2.apply();
      }

   }
}
