package io.intercom.android.sdk.identity;

import android.text.TextUtils;

abstract class SoftUserIdentity {
   static final SoftUserIdentity NONE = create("", "", "", "", "", "", "");

   static SoftUserIdentity create(String var0, String var1, String var2, String var3, String var4, String var5, String var6) {
      return new AutoValue_SoftUserIdentity(var0, var1, var2, var3, var4, var5, var6);
   }

   abstract String anonymousId();

   abstract String data();

   abstract String email();

   abstract String fingerprint();

   abstract String hmac();

   abstract String intercomId();

   boolean isPresent() {
      boolean var1;
      if(!this.equals(NONE)) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   boolean isSameUser(Registration var1) {
      boolean var4 = true;
      String var5 = var1.getUserId();
      String var6 = var1.getEmail();
      boolean var3;
      if(TextUtils.isEmpty(var5) && TextUtils.isEmpty(var6)) {
         var3 = false;
      } else {
         var3 = true;
      }

      boolean var2 = var3;
      if(!TextUtils.isEmpty(var5)) {
         if(var3 && var5.equals(this.userId())) {
            var2 = true;
         } else {
            var2 = false;
         }
      }

      if(!TextUtils.isEmpty(var6)) {
         if(var2 && var6.equals(this.email())) {
            var2 = var4;
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   abstract String userId();
}
