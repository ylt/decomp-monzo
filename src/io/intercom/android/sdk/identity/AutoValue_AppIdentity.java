package io.intercom.android.sdk.identity;

final class AutoValue_AppIdentity extends AppIdentity {
   private final String apiKey;
   private final String appId;

   AutoValue_AppIdentity(String var1, String var2) {
      if(var1 == null) {
         throw new NullPointerException("Null apiKey");
      } else {
         this.apiKey = var1;
         if(var2 == null) {
            throw new NullPointerException("Null appId");
         } else {
            this.appId = var2;
         }
      }
   }

   public String apiKey() {
      return this.apiKey;
   }

   public String appId() {
      return this.appId;
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(var1 != this) {
         if(var1 instanceof AppIdentity) {
            AppIdentity var3 = (AppIdentity)var1;
            if(!this.apiKey.equals(var3.apiKey()) || !this.appId.equals(var3.appId())) {
               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public int hashCode() {
      return (this.apiKey.hashCode() ^ 1000003) * 1000003 ^ this.appId.hashCode();
   }

   public String toString() {
      return "AppIdentity{apiKey=" + this.apiKey + ", appId=" + this.appId + "}";
   }
}
