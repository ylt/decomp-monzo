package io.intercom.android.sdk.identity;

import android.content.Context;
import android.content.SharedPreferences;

public abstract class AppIdentity {
   private static final String PREFS_API_KEY = "ApiKey";
   private static final String PREFS_APP_ID = "AppId";

   public static AppIdentity create(String var0, String var1) {
      return new AutoValue_AppIdentity(var0, var1);
   }

   public static AppIdentity loadFromDevice(Context var0) {
      SharedPreferences var1 = var0.getSharedPreferences("INTERCOM_SDK_PREFS", 0);
      return create(var1.getString("ApiKey", ""), var1.getString("AppId", ""));
   }

   public abstract String apiKey();

   public abstract String appId();

   public void persist(Context var1) {
      var1.getSharedPreferences("INTERCOM_SDK_PREFS", 0).edit().putString("ApiKey", this.apiKey()).putString("AppId", this.appId()).apply();
   }
}
