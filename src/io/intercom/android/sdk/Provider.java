package io.intercom.android.sdk;

public interface Provider {
   Object get();
}
