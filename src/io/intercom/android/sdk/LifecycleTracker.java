package io.intercom.android.sdk;

import android.app.Activity;
import android.app.KeyguardManager;
import android.app.Application.ActivityLifecycleCallbacks;
import android.content.Context;
import android.os.Looper;
import io.intercom.android.sdk.actions.Actions;
import io.intercom.android.sdk.api.DeDuper;
import io.intercom.android.sdk.api.UserUpdateBatcher;
import io.intercom.android.sdk.commons.utilities.TimeProvider;
import io.intercom.android.sdk.logger.LumberMill;
import io.intercom.android.sdk.metrics.MetricsStore;
import io.intercom.android.sdk.push.SystemNotificationManager;
import io.intercom.android.sdk.store.Selectors;
import io.intercom.android.sdk.store.Store;
import io.intercom.android.sdk.twig.Twig;
import io.intercom.android.sdk.utilities.SimpleActivityLifecycleCallbacks;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

class LifecycleTracker extends SimpleActivityLifecycleCallbacks implements ActivityLifecycleCallbacks {
   private final DeDuper deDuper;
   private final ActivityLifecycleHandler handler;
   private final MetricsStore metricsStore;
   private final ResetManager resetManager;
   private final Set startedActivities = new HashSet();
   final Store store;
   private final SystemNotificationManager systemNotificationManager;
   private final TimeProvider timeProvider;
   private final Twig twig = LumberMill.getLogger();
   private final UserUpdateBatcher userUpdateBatcher;

   LifecycleTracker(SystemNotificationManager var1, MetricsStore var2, DeDuper var3, TimeProvider var4, UserUpdateBatcher var5, Store var6, ResetManager var7, ActivityLifecycleHandler var8) {
      this.systemNotificationManager = var1;
      this.metricsStore = var2;
      this.deDuper = var3;
      this.timeProvider = var4;
      this.userUpdateBatcher = var5;
      this.store = var6;
      this.resetManager = var7;
      this.handler = var8;
   }

   static LifecycleTracker create(SystemNotificationManager var0, MetricsStore var1, DeDuper var2, TimeProvider var3, UserUpdateBatcher var4, Store var5, ResetManager var6, float var7) {
      return new LifecycleTracker(var0, var1, var2, var3, var4, var5, var6, new ActivityLifecycleHandler(Looper.getMainLooper(), var5, var7));
   }

   private boolean hasApplicationBecomeBackgrounded(Activity var1) {
      boolean var2;
      if(this.isApplicationInBackground(var1) && !((Boolean)this.store.select(Selectors.APP_IS_BACKGROUNDED)).booleanValue()) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   private static boolean isScreenLocked(Context var0) {
      return ((KeyguardManager)var0.getSystemService("keyguard")).inKeyguardRestrictedInputMode();
   }

   private void registerActivity(Activity var1) {
      this.startedActivities.add(Integer.valueOf(System.identityHashCode(var1)));
      this.twig.i("Started observing " + var1, new Object[0]);
   }

   private void unregisterActivity(Activity var1) {
      this.startedActivities.remove(Integer.valueOf(System.identityHashCode(var1)));
      this.twig.i("Stopped observing " + var1, new Object[0]);
   }

   boolean isApplicationInBackground(Activity var1) {
      boolean var2;
      if(!isScreenLocked(var1) && (!this.startedActivities.isEmpty() || var1.isChangingConfigurations())) {
         var2 = false;
      } else {
         var2 = true;
      }

      return var2;
   }

   public void onActivityPaused(Activity var1) {
      this.handler.sendMessage(ActivityLifecycleHandler.getMessage(2, var1));
   }

   public void onActivityResumed(Activity var1) {
      this.handler.removeMessages(1, var1);
      this.handler.sendMessage(ActivityLifecycleHandler.getMessage(0, var1));
   }

   public void onActivityStarted(Activity var1) {
      this.registerActivity(var1);
      if(((Boolean)this.store.select(Selectors.APP_IS_BACKGROUNDED)).booleanValue()) {
         this.store.dispatch(Actions.appEnteredForeground(this.timeProvider.currentTimeMillis()));
         this.metricsStore.loadAndSendCachedMetrics();
         this.systemNotificationManager.clear();
      }

   }

   public void onActivityStopped(Activity var1) {
      this.unregisterActivity(var1);
      this.handler.removeMessages(1, var1);
      this.handler.sendMessage(ActivityLifecycleHandler.getMessage(3, var1));
      if(this.hasApplicationBecomeBackgrounded(var1)) {
         if(this.resetManager.isSoftReset()) {
            this.resetManager.hardReset();
         }

         this.store.dispatch(Actions.appEnteredBackground(this.timeProvider.currentTimeMillis()));
         this.deDuper.reset();
         this.userUpdateBatcher.submitPendingUpdate();
         this.metricsStore.sendMetrics();
      }

   }

   void registerActivities(Collection var1) {
      this.startedActivities.addAll(var1);
   }
}
