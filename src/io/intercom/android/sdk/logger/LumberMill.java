package io.intercom.android.sdk.logger;

import io.intercom.android.sdk.Intercom;
import io.intercom.android.sdk.twig.Twig;

public class LumberMill {
   private static final Twig BLOCK_TWIG = new Twig(5, "Intercom", false);
   private static final Twig MESSENGER_TWIG = new Twig(5, "Intercom", false);
   private static final Twig NEXUS_TWIG = new Twig(5, "Intercom realtime", false);

   public static Twig getBlocksTwig() {
      return BLOCK_TWIG;
   }

   public static Twig getLogger() {
      return MESSENGER_TWIG;
   }

   public static Twig getNexusTwig() {
      return NEXUS_TWIG;
   }

   public static void setLogLevel(@Intercom.LogLevel int var0) {
      MESSENGER_TWIG.setLogLevel(var0);
      NEXUS_TWIG.setLogLevel(var0);
      BLOCK_TWIG.setLogLevel(var0);
   }
}
