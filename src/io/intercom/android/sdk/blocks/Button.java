package io.intercom.android.sdk.blocks;

import android.content.Context;
import android.support.v4.content.a;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;
import io.intercom.android.sdk.Provider;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.blocks.blockInterfaces.ButtonBlock;
import io.intercom.android.sdk.blocks.blockInterfaces.FacebookBlock;
import io.intercom.android.sdk.blocks.blockInterfaces.TwitterBlock;
import io.intercom.android.sdk.blocks.views.ParagraphView;
import io.intercom.android.sdk.commons.utilities.ScreenUtils;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.android.sdk.utilities.BackgroundUtils;
import io.intercom.android.sdk.utilities.BlockUtils;
import io.intercom.android.sdk.utilities.FontUtils;
import io.intercom.android.sdk.views.ButtonSelector;

class Button implements ButtonBlock, FacebookBlock, TwitterBlock {
   private static final int FACEBOOK_LOGO_PADDING_LEFT_DP = 20;
   private static final int TWITTER_LOGO_PADDING_LEFT_DP = 16;
   private final Provider appConfigProvider;
   private final ButtonClickListener buttonClickListener;
   private final StyleType style;

   public Button(StyleType var1, Provider var2, ButtonClickListener var3) {
      this.style = var1;
      this.appConfigProvider = var2;
      this.buttonClickListener = var3;
   }

   private View createButtonWithLogo(Context var1, String var2, int var3, int var4) {
      FrameLayout var8 = new FrameLayout(var1);
      BlockUtils.createLayoutParams(var8, -1, ScreenUtils.dpToPx(44.0F, var1));
      BlockUtils.setDefaultMarginBottom(var8);
      int var5 = ((AppConfig)this.appConfigProvider.get()).getBaseColor();
      BackgroundUtils.setBackground(var8, new ButtonSelector(var1, R.drawable.intercom_border, var5));
      ((LayoutParams)var8.getLayoutParams()).gravity = 17;
      ImageView var6 = new ImageView(var1);
      BlockUtils.createLayoutParams(var6, -2, -1);
      var6.setImageResource(var4);
      var6.setBackgroundResource(R.color.intercom_full_transparent_full_black);
      var6.setPadding(var3, 0, 0, 0);
      ((LayoutParams)var6.getLayoutParams()).gravity = 19;
      TextView var7 = new TextView(var1);
      BlockUtils.createLayoutParams(var7, -1, -1);
      var7.setText(var2);
      var7.setTextSize(16.0F);
      var7.setTextColor(a.c(var1, R.color.intercom_white));
      var7.setBackgroundResource(R.color.intercom_full_transparent_full_black);
      var7.setGravity(17);
      FontUtils.setRobotoMediumTypeface(var7);
      var8.addView(var6);
      var8.addView(var7);
      return var8;
   }

   private TextView createDefaultTextView(Context var1, String var2, BlockAlignment var3) {
      int var4 = ((AppConfig)this.appConfigProvider.get()).getBaseColor();
      ParagraphView var5 = new ParagraphView(var1, this.style, var3, var4);
      var5.setTextColor(var4);
      var5.setPaintFlags(var5.getPaintFlags() | 8);
      var5.setGravity(var3.getGravity());
      var5.setText(var2);
      return var5;
   }

   private android.widget.Button createFullWidthButton(Context var1, String var2) {
      android.widget.Button var4 = new android.widget.Button(var1);
      var4.setTextSize(16.0F);
      var4.setTextColor(a.c(var1, R.color.intercom_white));
      var4.setAllCaps(false);
      var4.setGravity(17);
      var4.setText(var2);
      BlockUtils.createLayoutParams(var4, -1, -2);
      BlockUtils.setDefaultMarginBottom(var4);
      int var3 = ((AppConfig)this.appConfigProvider.get()).getBaseColor();
      BackgroundUtils.setBackground(var4, new ButtonSelector(var1, R.drawable.intercom_border, var3));
      FontUtils.setRobotoMediumTypeface(var4);
      return var4;
   }

   private View setButtonDefaults(View var1, final String var2, BlockAlignment var3, boolean var4) {
      if(this.buttonClickListener.shouldHandleClicks()) {
         var1.setOnClickListener(new OnClickListener() {
            public void onClick(View var1) {
               Button.this.buttonClickListener.onButtonClicked(var1, var2);
            }
         });
      }

      BlockUtils.setLayoutMarginsAndGravity(var1, var3.getGravity(), var4);
      return var1;
   }

   public View addButton(String var1, String var2, BlockAlignment var3, boolean var4, boolean var5, ViewGroup var6) {
      Context var7 = var6.getContext();
      Object var8;
      if(this.style != StyleType.NOTE && this.style != StyleType.POST && this.style != StyleType.CONTAINER_CARD) {
         var8 = this.createDefaultTextView(var7, var1, var3);
      } else {
         var8 = this.createFullWidthButton(var7, var1);
      }

      return this.setButtonDefaults((View)var8, var2, var3, var5);
   }

   public View addFacebookButton(String var1, BlockAlignment var2, boolean var3, boolean var4, ViewGroup var5) {
      Context var7 = var5.getContext();
      String var6 = var7.getString(R.string.intercom_facebook_like);
      Object var8;
      if(this.style != StyleType.NOTE && this.style != StyleType.POST && this.style != StyleType.CONTAINER_CARD) {
         var8 = this.createDefaultTextView(var7, var6, var2);
      } else {
         var8 = this.createButtonWithLogo(var7, var6, ScreenUtils.dpToPx(20.0F, var7), R.drawable.intercom_icn_fb);
      }

      return this.setButtonDefaults((View)var8, var1, var2, var4);
   }

   public View addTwitterButton(String var1, BlockAlignment var2, boolean var3, boolean var4, ViewGroup var5) {
      Context var7 = var5.getContext();
      String var6 = var7.getString(R.string.intercom_twitter_follow);
      Object var8;
      if(this.style != StyleType.NOTE && this.style != StyleType.POST && this.style != StyleType.CONTAINER_CARD) {
         var8 = this.createDefaultTextView(var7, var6, var2);
      } else {
         var8 = this.createButtonWithLogo(var7, var6, ScreenUtils.dpToPx(16.0F, var7), R.drawable.intercom_icn_twitter);
      }

      return this.setButtonDefaults((View)var8, var1, var2, var4);
   }
}
