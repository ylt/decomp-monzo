package io.intercom.android.sdk.blocks;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Build.VERSION;
import android.support.v4.app.b;
import android.support.v4.content.a;
import android.text.TextUtils.TruncateAt;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.FrameLayout.LayoutParams;
import io.intercom.android.sdk.Provider;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.activities.IntercomArticleActivity;
import io.intercom.android.sdk.activities.IntercomHelpCenterActivity;
import io.intercom.android.sdk.api.Api;
import io.intercom.android.sdk.blocks.blockInterfaces.LinkListBlock;
import io.intercom.android.sdk.blocks.models.Link;
import io.intercom.android.sdk.blocks.models.LinkList;
import io.intercom.android.sdk.commons.utilities.ScreenUtils;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.android.sdk.utilities.BackgroundUtils;
import io.intercom.android.sdk.utilities.LinkOpener;
import java.util.Iterator;

class LinkListRenderer implements LinkListBlock {
   private final Api api;
   private final Provider appConfigProvider;
   private final String conversationId;

   LinkListRenderer(Api var1, Provider var2, String var3) {
      this.api = var1;
      this.appConfigProvider = var2;
      this.conversationId = var3;
   }

   private void addLinkToLayout(final LinearLayout var1, final Link var2, int var3, final Context var4, boolean var5) {
      FrameLayout var6 = new FrameLayout(var4);
      var6.setLayoutParams(new LayoutParams(-1, -2));
      TextView var7 = new TextView(var4);
      LayoutParams var8 = new LayoutParams(-2, -1);
      if(var5) {
         var8.gravity = 17;
      }

      var7.setLayoutParams(var8);
      var7.setTextColor(var3);
      var7.setTextSize(16.0F);
      var7.setText(var2.getTitle());
      var7.setMaxLines(2);
      var7.setEllipsize(TruncateAt.END);
      var7.setPadding(ScreenUtils.dpToPx(24.0F, var4), ScreenUtils.dpToPx(16.0F, var4), ScreenUtils.dpToPx(16.0F, var4), ScreenUtils.dpToPx(16.0F, var4));
      if("educate.help_center".equals(var2.getLinkType())) {
         Drawable var10 = a.a(var4, R.drawable.intercom_open_help_center);
         var10.setColorFilter(var3, Mode.SRC_IN);
         var7.setCompoundDrawablesWithIntrinsicBounds(var10, (Drawable)null, (Drawable)null, (Drawable)null);
         var7.setCompoundDrawablePadding(ScreenUtils.dpToPx(8.0F, var4));
      }

      var6.addView(var7);
      var6.setOnClickListener(new OnClickListener() {
         public void onClick(View var1x) {
            if(("educate.article".equals(var2.getLinkType()) || "educate.suggestion".equals(var2.getLinkType())) && var4 instanceof Activity) {
               Activity var5 = (Activity)var4;
               Intent var4x = IntercomArticleActivity.buildIntent(var5, var2.getArticleId(), LinkListRenderer.this.conversationId);
               if(VERSION.SDK_INT >= 16) {
                  var5.startActivity(var4x, b.a(var5, var1, "link_background").a());
               } else {
                  var5.startActivity(var4x);
               }
            } else if("educate.help_center".equals(var2.getLinkType()) && var4 instanceof Activity) {
               Activity var3 = (Activity)var4;
               Intent var2x = IntercomHelpCenterActivity.buildIntent(var3, var2.getUrl(), LinkListRenderer.this.conversationId);
               if(VERSION.SDK_INT >= 16) {
                  var3.startActivity(var2x, b.a(var3, var1, "link_background").a());
               } else {
                  var3.startActivity(var2x);
               }
            } else {
               LinkOpener.handleUrl(var2.getUrl(), var4, LinkListRenderer.this.api);
            }

         }
      });
      TypedValue var9 = new TypedValue();
      var4.getTheme().resolveAttribute(16843534, var9, true);
      var6.setBackgroundResource(var9.resourceId);
      var1.addView(var6);
   }

   public View addLinkListBlock(LinkList var1, boolean var2, boolean var3, ViewGroup var4) {
      Context var9 = var4.getContext();
      LinearLayout var6 = new LinearLayout(var9);
      GradientDrawable var7 = new GradientDrawable();
      var7.setColor(-1);
      var7.setStroke(1, ((AppConfig)this.appConfigProvider.get()).getBaseColor());
      var7.setCornerRadius((float)ScreenUtils.dpToPx(8.0F, var9));
      BackgroundUtils.setBackground(var6, var7);
      int var5 = ((AppConfig)this.appConfigProvider.get()).getBaseColor();
      var6.setShowDividers(2);
      var6.setDividerDrawable(new ColorDrawable(var5));
      var6.setOrientation(1);
      Iterator var10 = var1.getLinks().iterator();

      while(var10.hasNext()) {
         this.addLinkToLayout(var6, (Link)var10.next(), var5, var9, false);
      }

      Link var8 = var1.getFooterLink();
      if(!var8.getText().isEmpty()) {
         this.addLinkToLayout(var6, var8, var5, var9, true);
      }

      return var6;
   }
}
