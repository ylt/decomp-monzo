package io.intercom.android.sdk.blocks;

import android.content.Context;
import android.support.v4.content.a;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import io.intercom.android.sdk.Provider;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.blocks.blockInterfaces.OrderedListBlock;
import io.intercom.android.sdk.blocks.blockInterfaces.UnorderedListBlock;
import io.intercom.android.sdk.commons.utilities.HtmlCompat;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.android.sdk.spans.OrderedListSpan;
import io.intercom.android.sdk.spans.UnorderedListSpan;
import io.intercom.android.sdk.utilities.BlockUtils;
import io.intercom.android.sdk.utilities.TrackingLinkMovementMethod;
import java.util.List;

class ListBlock implements OrderedListBlock, UnorderedListBlock {
   private static final int LARGE_TEXT_SIZE_SP = 16;
   private static final int SMALL_TEXT_SIZE_SP = 14;
   private final Provider appConfigProvider;
   private final StyleType style;

   ListBlock(StyleType var1, Provider var2) {
      this.style = var1;
      this.appConfigProvider = var2;
   }

   private TextView getListStyledTextView(Context var1) {
      TextView var2 = new TextView(var1);
      var2.setLinkTextColor(((AppConfig)this.appConfigProvider.get()).getBaseColor());
      BlockUtils.createLayoutParams(var2, -2, -2);
      BlockUtils.setMarginLeft(var2, 4);
      switch(null.$SwitchMap$io$intercom$android$sdk$blocks$StyleType[this.style.ordinal()]) {
      case 1:
         this.styleAsChatList(var2, a.c(var1, R.color.intercom_grey_800));
         var2.setMovementMethod(new TrackingLinkMovementMethod());
         break;
      case 2:
      case 3:
         this.styleAsAnnouncementList(var2, a.c(var1, R.color.intercom_grey_700));
         var2.setMovementMethod(new TrackingLinkMovementMethod());
         break;
      case 4:
         this.styleAsAnnouncementList(var2, a.c(var1, R.color.intercom_white));
         var2.setMovementMethod(new TrackingLinkMovementMethod());
         break;
      case 5:
         this.styleAsChatList(var2, a.c(var1, R.color.intercom_grey_800));
         break;
      default:
         this.styleAsChatList(var2, a.c(var1, R.color.intercom_white));
         var2.setMovementMethod(new TrackingLinkMovementMethod());
      }

      return var2;
   }

   private void styleAsAnnouncementList(TextView var1, int var2) {
      var1.setTextSize(16.0F);
      var1.setTextColor(var2);
      BlockUtils.setMarginBottom(var1, 16);
      BlockUtils.setLargeLineSpacing(var1);
   }

   private void styleAsChatList(TextView var1, int var2) {
      var1.setTextSize(14.0F);
      var1.setTextColor(var2);
      BlockUtils.setDefaultMarginBottom(var1);
      BlockUtils.setSmallLineSpacing(var1);
   }

   public View addOrderedList(List var1, boolean var2, boolean var3, ViewGroup var4) {
      TextView var9 = this.getListStyledTextView(var4.getContext());
      int var7 = (int)var4.getResources().getDimension(R.dimen.intercom_list_indentation);
      Object var12 = "";
      int var6 = var1.size();

      for(int var5 = 0; var5 < var6; ++var5) {
         String var10 = (String)var1.get(var5);
         if(!var10.isEmpty()) {
            String var8;
            if(var5 < var6 - 1) {
               var8 = "<br />";
            } else {
               var8 = "";
            }

            Spanned var11 = HtmlCompat.fromHtml(var10 + var8);
            SpannableString var13 = new SpannableString(var11);
            var13.setSpan(new OrderedListSpan(var7, var5 + 1 + "."), 0, var11.length(), 0);
            var12 = TextUtils.concat(new CharSequence[]{(CharSequence)var12, var13});
         }
      }

      var9.setText((CharSequence)var12);
      BlockUtils.setLayoutMarginsAndGravity(var9, 3, var3);
      return var9;
   }

   public View addUnorderedList(List var1, boolean var2, boolean var3, ViewGroup var4) {
      Context var10 = var4.getContext();
      TextView var9 = this.getListStyledTextView(var10);
      int var7 = (int)var4.getResources().getDimension(R.dimen.intercom_list_indentation);
      Object var13 = "";
      int var6 = var1.size();

      for(int var5 = 0; var5 < var6; ++var5) {
         String var11 = (String)var1.get(var5);
         if(!var11.isEmpty()) {
            String var8;
            if(var5 < var6 - 1) {
               var8 = "<br />";
            } else {
               var8 = "";
            }

            Spanned var12 = HtmlCompat.fromHtml(var11 + var8);
            SpannableString var14 = new SpannableString(var12);
            var14.setSpan(new UnorderedListSpan(var7, var10), 0, var12.length(), 0);
            var13 = TextUtils.concat(new CharSequence[]{(CharSequence)var13, var14});
         }
      }

      var9.setText((CharSequence)var13);
      BlockUtils.setLayoutMarginsAndGravity(var9, 3, var3);
      return var9;
   }
}
