package io.intercom.android.sdk.blocks.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LinkList implements Parcelable {
   public static final Creator CREATOR = new Creator() {
      public LinkList createFromParcel(Parcel var1) {
         ArrayList var2 = new ArrayList();
         var1.readList(var2, Link.class.getClassLoader());
         return new LinkList(var2, (Link)var1.readParcelable(Link.class.getClassLoader()));
      }

      public LinkList[] newArray(int var1) {
         return new LinkList[var1];
      }
   };
   private final Link footerLink;
   private final List links;

   public LinkList(List var1, Link var2) {
      this.links = var1;
      this.footerLink = var2;
   }

   public static LinkList fromBlock(Block var0) {
      LinkList var1;
      if(var0 == null) {
         var1 = new LinkList(Collections.emptyList(), (new Link.Builder()).build());
      } else {
         var1 = new LinkList(var0.getLinks(), var0.getFooterLink());
      }

      return var1;
   }

   public int describeContents() {
      return 0;
   }

   public boolean equals(Object var1) {
      boolean var3 = false;
      boolean var2;
      if(this == var1) {
         var2 = true;
      } else {
         var2 = var3;
         if(var1 != null) {
            var2 = var3;
            if(this.getClass() == var1.getClass()) {
               LinkList var4 = (LinkList)var1;
               var2 = var3;
               if(this.links.equals(var4.links)) {
                  var2 = this.footerLink.equals(var4.footerLink);
               }
            }
         }
      }

      return var2;
   }

   public Link getFooterLink() {
      return this.footerLink;
   }

   public List getLinks() {
      return this.links;
   }

   public int hashCode() {
      return this.links.hashCode() * 31 + this.footerLink.hashCode();
   }

   public void writeToParcel(Parcel var1, int var2) {
      var1.writeList(this.links);
      var1.writeParcelable(this.footerLink, var2);
   }
}
