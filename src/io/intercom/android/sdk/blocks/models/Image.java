package io.intercom.android.sdk.blocks.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class Image implements Parcelable {
   public static final Creator CREATOR = new Creator() {
      public Image createFromParcel(Parcel var1) {
         return new Image(var1);
      }

      public Image[] newArray(int var1) {
         return new Image[var1];
      }
   };
   private final String alt;
   private final String attribution;
   private final int height;
   private final String previewUrl;
   private final String url;
   private final int width;

   Image() {
      this(new Image.Builder());
   }

   protected Image(Parcel var1) {
      this.attribution = var1.readString();
      this.previewUrl = var1.readString();
      this.alt = var1.readString();
      this.url = var1.readString();
      this.width = var1.readInt();
      this.height = var1.readInt();
   }

   private Image(Image.Builder var1) {
      String var2;
      if(var1.alt == null) {
         var2 = "";
      } else {
         var2 = var1.alt;
      }

      this.alt = var2;
      if(var1.url == null) {
         var2 = "";
      } else {
         var2 = var1.url;
      }

      this.url = var2;
      if(var1.previewUrl == null) {
         var2 = "";
      } else {
         var2 = var1.previewUrl;
      }

      this.previewUrl = var2;
      if(var1.attribution == null) {
         var2 = "";
      } else {
         var2 = var1.attribution;
      }

      this.attribution = var2;
      this.width = var1.width;
      this.height = var1.height;
   }

   // $FF: synthetic method
   Image(Image.Builder var1, Object var2) {
      this(var1);
   }

   public int describeContents() {
      return 0;
   }

   public boolean equals(Object var1) {
      boolean var4 = true;
      boolean var3 = false;
      boolean var2;
      if(this == var1) {
         var2 = true;
      } else {
         var2 = var3;
         if(var1 != null) {
            var2 = var3;
            if(this.getClass() == var1.getClass()) {
               Image var5 = (Image)var1;
               if(this.alt != null) {
                  var2 = var3;
                  if(!this.alt.equals(var5.alt)) {
                     return var2;
                  }
               } else if(var5.alt != null) {
                  var2 = var3;
                  return var2;
               }

               if(this.previewUrl != null) {
                  var2 = var3;
                  if(!this.previewUrl.equals(var5.previewUrl)) {
                     return var2;
                  }
               } else if(var5.previewUrl != null) {
                  var2 = var3;
                  return var2;
               }

               if(this.attribution != null) {
                  var2 = var3;
                  if(!this.attribution.equals(var5.attribution)) {
                     return var2;
                  }
               } else if(var5.attribution != null) {
                  var2 = var3;
                  return var2;
               }

               var2 = var3;
               if(this.width == var5.width) {
                  var2 = var3;
                  if(this.height == var5.height) {
                     if(this.url != null) {
                        var2 = var4;
                        if(this.url.equals(var5.url)) {
                           return var2;
                        }
                     } else if(var5.url == null) {
                        var2 = var4;
                        return var2;
                     }

                     var2 = false;
                  }
               }
            }
         }
      }

      return var2;
   }

   public String getAlt() {
      return this.alt;
   }

   public String getAttribution() {
      return this.attribution;
   }

   public int getHeight() {
      return this.height;
   }

   public String getPreviewUrl() {
      return this.previewUrl;
   }

   public String getUrl() {
      return this.url;
   }

   public int getWidth() {
      return this.width;
   }

   public int hashCode() {
      int var4 = 0;
      int var1;
      if(this.alt != null) {
         var1 = this.alt.hashCode();
      } else {
         var1 = 0;
      }

      int var2;
      if(this.url != null) {
         var2 = this.url.hashCode();
      } else {
         var2 = 0;
      }

      int var3;
      if(this.previewUrl != null) {
         var3 = this.previewUrl.hashCode();
      } else {
         var3 = 0;
      }

      if(this.attribution != null) {
         var4 = this.attribution.hashCode();
      }

      return (((var3 + (var2 + var1 * 31) * 31) * 31 + var4) * 31 + this.width) * 31 + this.height;
   }

   public void writeToParcel(Parcel var1, int var2) {
      var1.writeString(this.attribution);
      var1.writeString(this.previewUrl);
      var1.writeString(this.alt);
      var1.writeString(this.url);
      var1.writeInt(this.width);
      var1.writeInt(this.height);
   }

   public static final class Builder {
      String alt;
      String attribution;
      int height;
      String previewUrl;
      String url;
      int width;

      public Image build() {
         return new Image(this, null);
      }

      public Image.Builder withAlt(String var1) {
         this.alt = var1;
         return this;
      }

      public Image.Builder withAttribution(String var1) {
         this.attribution = var1;
         return this;
      }

      public Image.Builder withHeight(int var1) {
         this.height = var1;
         return this;
      }

      public Image.Builder withPreviewUrl(String var1) {
         this.previewUrl = var1;
         return this;
      }

      public Image.Builder withUrl(String var1) {
         this.url = var1;
         return this;
      }

      public Image.Builder withWidth(int var1) {
         this.width = var1;
         return this;
      }
   }
}
