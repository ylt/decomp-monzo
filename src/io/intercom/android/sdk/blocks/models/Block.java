package io.intercom.android.sdk.blocks.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import io.intercom.android.sdk.blocks.BlockAlignment;
import io.intercom.android.sdk.blocks.BlockType;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class Block implements Parcelable {
   public static final Creator CREATOR = new Creator() {
      public Block createFromParcel(Parcel var1) {
         return new Block(var1);
      }

      public Block[] newArray(int var1) {
         return new Block[var1];
      }
   };
   private final BlockAlignment align;
   private final String articleId;
   private final List attachments;
   private final String attribution;
   private final Author author;
   private final List channels;
   private final Map data;
   private final String description;
   private final String embedUrl;
   private final Link footerLink;
   private final int height;
   private final String id;
   private final Image image;
   private final List items;
   private final String language;
   private final String linkType;
   private final String linkUrl;
   private final List links;
   private final List options;
   private final String previewUrl;
   private final String provider;
   private final int ratingIndex;
   private final String remark;
   private final String siteName;
   private final String text;
   private final String title;
   private final String trackingUrl;
   private final BlockType type;
   private final String url;
   private final String username;
   private final int width;

   Block() {
      this(new Block.Builder());
   }

   protected Block(Parcel var1) {
      Object var5 = null;
      super();
      this.attachments = var1.createTypedArrayList(BlockAttachment.CREATOR);
      this.items = var1.createStringArrayList();
      int var3 = var1.readInt();
      this.data = new HashMap(var3);

      int var2;
      for(var2 = 0; var2 < var3; ++var2) {
         String var4 = var1.readString();
         String var6 = var1.readString();
         this.data.put(var4, var6);
      }

      var2 = var1.readInt();
      BlockType var7;
      if(var2 == -1) {
         var7 = null;
      } else {
         var7 = BlockType.values()[var2];
      }

      this.type = var7;
      var2 = var1.readInt();
      BlockAlignment var8;
      if(var2 == -1) {
         var8 = (BlockAlignment)var5;
      } else {
         var8 = BlockAlignment.values()[var2];
      }

      this.align = var8;
      this.author = (Author)var1.readParcelable(Author.class.getClassLoader());
      this.image = (Image)var1.readParcelable(Image.class.getClassLoader());
      this.text = var1.readString();
      this.title = var1.readString();
      this.description = var1.readString();
      this.linkType = var1.readString();
      this.siteName = var1.readString();
      this.articleId = var1.readString();
      this.language = var1.readString();
      this.url = var1.readString();
      this.linkUrl = var1.readString();
      this.embedUrl = var1.readString();
      this.trackingUrl = var1.readString();
      this.username = var1.readString();
      this.provider = var1.readString();
      this.previewUrl = var1.readString();
      this.attribution = var1.readString();
      this.id = var1.readString();
      this.width = var1.readInt();
      this.height = var1.readInt();
      this.channels = var1.createTypedArrayList(Channel.CREATOR);
      this.ratingIndex = var1.readInt();
      this.remark = var1.readString();
      this.options = new ArrayList();
      var1.readList(this.options, ConversationRatingOption.class.getClassLoader());
      this.links = new ArrayList();
      var1.readList(this.links, Link.class.getClassLoader());
      this.footerLink = (Link)var1.readParcelable(Link.class.getClassLoader());
   }

   private Block(Block.Builder var1) {
      byte var3 = 0;
      super();
      this.type = BlockType.typeValueOf(var1.type);
      String var4;
      if(var1.text == null) {
         var4 = "";
      } else {
         var4 = var1.text;
      }

      this.text = var4;
      if(var1.title == null) {
         var4 = "";
      } else {
         var4 = var1.title;
      }

      this.title = var4;
      if(var1.description == null) {
         var4 = "";
      } else {
         var4 = var1.description;
      }

      this.description = var4;
      if(var1.linkType == null) {
         var4 = "";
      } else {
         var4 = var1.linkType;
      }

      this.linkType = var4;
      if(var1.siteName == null) {
         var4 = "";
      } else {
         var4 = var1.siteName;
      }

      this.siteName = var4;
      if(var1.articleId == null) {
         var4 = "";
      } else {
         var4 = var1.articleId;
      }

      this.articleId = var4;
      Author var13;
      if(var1.author == null) {
         var13 = new Author();
      } else {
         var13 = var1.author;
      }

      this.author = var13;
      Image var14;
      if(var1.image == null) {
         var14 = new Image();
      } else {
         var14 = var1.image;
      }

      this.image = var14;
      Map var15;
      if(var1.data == null) {
         var15 = Collections.emptyMap();
      } else {
         var15 = var1.data;
      }

      this.data = var15;
      if(var1.language == null) {
         var4 = "";
      } else {
         var4 = var1.language;
      }

      this.language = var4;
      if(var1.url == null) {
         var4 = "";
      } else {
         var4 = var1.url;
      }

      this.url = var4;
      if(var1.linkUrl == null) {
         var4 = "";
      } else {
         var4 = var1.linkUrl;
      }

      this.linkUrl = var4;
      if(var1.embedUrl == null) {
         var4 = "";
      } else {
         var4 = var1.embedUrl;
      }

      this.embedUrl = var4;
      if(var1.trackingUrl == null) {
         var4 = "";
      } else {
         var4 = var1.trackingUrl;
      }

      this.trackingUrl = var4;
      if(var1.username == null) {
         var4 = "";
      } else {
         var4 = var1.username;
      }

      this.username = var4;
      if(var1.provider == null) {
         var4 = "";
      } else {
         var4 = var1.provider;
      }

      this.provider = var4;
      if(var1.id == null) {
         var4 = "";
      } else {
         var4 = var1.id;
      }

      this.id = var4;
      this.align = BlockAlignment.alignValueOf(var1.align);
      int var2;
      if(var1.width == null) {
         var2 = 0;
      } else {
         var2 = var1.width.intValue();
      }

      this.width = var2;
      if(var1.height == null) {
         var2 = var3;
      } else {
         var2 = var1.height.intValue();
      }

      this.height = var2;
      if(var1.previewUrl == null) {
         var4 = "";
      } else {
         var4 = var1.previewUrl;
      }

      this.previewUrl = var4;
      if(var1.attribution == null) {
         var4 = "";
      } else {
         var4 = var1.attribution;
      }

      this.attribution = var4;
      this.attachments = new ArrayList();
      Iterator var7;
      if(var1.attachments != null) {
         var7 = var1.attachments.iterator();

         while(var7.hasNext()) {
            BlockAttachment var5 = (BlockAttachment)var7.next();
            if(var5 != null) {
               this.attachments.add(var5);
            }
         }
      }

      this.channels = new ArrayList();
      if(var1.channels != null) {
         var7 = var1.channels.iterator();

         while(var7.hasNext()) {
            Channel var9 = (Channel)var7.next();
            if(var9 != null) {
               this.channels.add(var9);
            }
         }
      }

      this.items = new ArrayList();
      if(var1.items != null) {
         var7 = var1.items.iterator();

         while(var7.hasNext()) {
            String var10 = (String)var7.next();
            if(var10 != null) {
               this.items.add(var10);
            }
         }
      }

      if(var1.ratingIndex != null) {
         this.ratingIndex = var1.ratingIndex.intValue();
      } else if(var1.rating_index != null) {
         this.ratingIndex = var1.rating_index.intValue();
      } else {
         this.ratingIndex = -1;
      }

      if(var1.remark == null) {
         var4 = "";
      } else {
         var4 = var1.remark;
      }

      this.remark = var4;
      this.options = new ArrayList();
      if(var1.options != null) {
         Iterator var11 = var1.options.iterator();

         while(var11.hasNext()) {
            ConversationRatingOption.Builder var8 = (ConversationRatingOption.Builder)var11.next();
            if(var8 != null) {
               this.options.add(var8.build());
            }
         }
      }

      this.links = new ArrayList();
      if(var1.links != null) {
         var7 = var1.links.iterator();

         while(var7.hasNext()) {
            Block.Builder var12 = (Block.Builder)var7.next();
            if(var12 != null) {
               this.links.add(Link.fromBlock(var12.build()));
            }
         }
      }

      Link var6;
      if(var1.footerLink == null) {
         var6 = new Link();
      } else {
         var6 = Link.fromBlock(var1.footerLink.build());
      }

      this.footerLink = var6;
   }

   // $FF: synthetic method
   Block(Block.Builder var1, Object var2) {
      this(var1);
   }

   public int describeContents() {
      return 0;
   }

   public boolean equals(Object var1) {
      boolean var4 = true;
      boolean var3 = false;
      boolean var2;
      if(this == var1) {
         var2 = true;
      } else {
         var2 = var3;
         if(var1 != null) {
            var2 = var3;
            if(this.getClass() == var1.getClass()) {
               Block var5 = (Block)var1;
               var2 = var3;
               if(this.width == var5.width) {
                  var2 = var3;
                  if(this.height == var5.height) {
                     var2 = var3;
                     if(this.ratingIndex == var5.ratingIndex) {
                        if(this.attachments != null) {
                           var2 = var3;
                           if(!this.attachments.equals(var5.attachments)) {
                              return var2;
                           }
                        } else if(var5.attachments != null) {
                           var2 = var3;
                           return var2;
                        }

                        if(this.items != null) {
                           var2 = var3;
                           if(!this.items.equals(var5.items)) {
                              return var2;
                           }
                        } else if(var5.items != null) {
                           var2 = var3;
                           return var2;
                        }

                        if(this.data != null) {
                           var2 = var3;
                           if(!this.data.equals(var5.data)) {
                              return var2;
                           }
                        } else if(var5.data != null) {
                           var2 = var3;
                           return var2;
                        }

                        var2 = var3;
                        if(this.type == var5.type) {
                           var2 = var3;
                           if(this.align == var5.align) {
                              if(this.author != null) {
                                 var2 = var3;
                                 if(!this.author.equals(var5.author)) {
                                    return var2;
                                 }
                              } else if(var5.author != null) {
                                 var2 = var3;
                                 return var2;
                              }

                              if(this.image != null) {
                                 var2 = var3;
                                 if(!this.image.equals(var5.image)) {
                                    return var2;
                                 }
                              } else if(var5.image != null) {
                                 var2 = var3;
                                 return var2;
                              }

                              if(this.text != null) {
                                 var2 = var3;
                                 if(!this.text.equals(var5.text)) {
                                    return var2;
                                 }
                              } else if(var5.text != null) {
                                 var2 = var3;
                                 return var2;
                              }

                              if(this.title != null) {
                                 var2 = var3;
                                 if(!this.title.equals(var5.title)) {
                                    return var2;
                                 }
                              } else if(var5.title != null) {
                                 var2 = var3;
                                 return var2;
                              }

                              if(this.description != null) {
                                 var2 = var3;
                                 if(!this.description.equals(var5.description)) {
                                    return var2;
                                 }
                              } else if(var5.description != null) {
                                 var2 = var3;
                                 return var2;
                              }

                              if(this.linkType != null) {
                                 var2 = var3;
                                 if(!this.linkType.equals(var5.linkType)) {
                                    return var2;
                                 }
                              } else if(var5.linkType != null) {
                                 var2 = var3;
                                 return var2;
                              }

                              if(this.siteName != null) {
                                 var2 = var3;
                                 if(!this.siteName.equals(var5.siteName)) {
                                    return var2;
                                 }
                              } else if(var5.siteName != null) {
                                 var2 = var3;
                                 return var2;
                              }

                              if(this.articleId != null) {
                                 var2 = var3;
                                 if(!this.articleId.equals(var5.articleId)) {
                                    return var2;
                                 }
                              } else if(var5.articleId != null) {
                                 var2 = var3;
                                 return var2;
                              }

                              if(this.language != null) {
                                 var2 = var3;
                                 if(!this.language.equals(var5.language)) {
                                    return var2;
                                 }
                              } else if(var5.language != null) {
                                 var2 = var3;
                                 return var2;
                              }

                              if(this.url != null) {
                                 var2 = var3;
                                 if(!this.url.equals(var5.url)) {
                                    return var2;
                                 }
                              } else if(var5.url != null) {
                                 var2 = var3;
                                 return var2;
                              }

                              if(this.previewUrl != null) {
                                 var2 = var3;
                                 if(!this.previewUrl.equals(var5.previewUrl)) {
                                    return var2;
                                 }
                              } else if(var5.previewUrl != null) {
                                 var2 = var3;
                                 return var2;
                              }

                              if(this.attribution != null) {
                                 var2 = var3;
                                 if(!this.attribution.equals(var5.attribution)) {
                                    return var2;
                                 }
                              } else if(var5.attribution != null) {
                                 var2 = var3;
                                 return var2;
                              }

                              if(this.linkUrl != null) {
                                 var2 = var3;
                                 if(!this.linkUrl.equals(var5.linkUrl)) {
                                    return var2;
                                 }
                              } else if(var5.linkUrl != null) {
                                 var2 = var3;
                                 return var2;
                              }

                              if(this.embedUrl != null) {
                                 var2 = var3;
                                 if(!this.embedUrl.equals(var5.embedUrl)) {
                                    return var2;
                                 }
                              } else if(var5.embedUrl != null) {
                                 var2 = var3;
                                 return var2;
                              }

                              if(this.trackingUrl != null) {
                                 var2 = var3;
                                 if(!this.trackingUrl.equals(var5.trackingUrl)) {
                                    return var2;
                                 }
                              } else if(var5.trackingUrl != null) {
                                 var2 = var3;
                                 return var2;
                              }

                              if(this.username != null) {
                                 var2 = var3;
                                 if(!this.username.equals(var5.username)) {
                                    return var2;
                                 }
                              } else if(var5.username != null) {
                                 var2 = var3;
                                 return var2;
                              }

                              if(this.provider != null) {
                                 var2 = var3;
                                 if(!this.provider.equals(var5.provider)) {
                                    return var2;
                                 }
                              } else if(var5.provider != null) {
                                 var2 = var3;
                                 return var2;
                              }

                              if(this.id != null) {
                                 var2 = var3;
                                 if(!this.id.equals(var5.id)) {
                                    return var2;
                                 }
                              } else if(var5.id != null) {
                                 var2 = var3;
                                 return var2;
                              }

                              if(this.channels != null) {
                                 var2 = var3;
                                 if(!this.channels.equals(var5.channels)) {
                                    return var2;
                                 }
                              } else if(var5.channels != null) {
                                 var2 = var3;
                                 return var2;
                              }

                              if(this.remark != null) {
                                 var2 = var3;
                                 if(!this.remark.equals(var5.remark)) {
                                    return var2;
                                 }
                              } else if(var5.remark != null) {
                                 var2 = var3;
                                 return var2;
                              }

                              if(this.options != null) {
                                 var2 = var3;
                                 if(!this.options.equals(var5.options)) {
                                    return var2;
                                 }
                              } else if(var5.options != null) {
                                 var2 = var3;
                                 return var2;
                              }

                              if(this.links != null) {
                                 var2 = var3;
                                 if(!this.links.equals(var5.links)) {
                                    return var2;
                                 }
                              } else if(var5.links != null) {
                                 var2 = var3;
                                 return var2;
                              }

                              if(this.footerLink != null) {
                                 var2 = this.footerLink.equals(var5.footerLink);
                              } else {
                                 var2 = var4;
                                 if(var5.footerLink != null) {
                                    var2 = false;
                                 }
                              }
                           }
                        }
                     }
                  }
               }
            }
         }
      }

      return var2;
   }

   public BlockAlignment getAlign() {
      return this.align;
   }

   public String getArticleId() {
      return this.articleId;
   }

   public List getAttachments() {
      return this.attachments;
   }

   public String getAttribution() {
      return this.attribution;
   }

   public Author getAuthor() {
      return this.author;
   }

   public List getChannels() {
      return this.channels;
   }

   public Map getData() {
      return this.data;
   }

   public String getDescription() {
      return this.description;
   }

   public String getEmbedUrl() {
      return this.embedUrl;
   }

   public Link getFooterLink() {
      return this.footerLink;
   }

   public int getHeight() {
      return this.height;
   }

   public String getId() {
      return this.id;
   }

   public Image getImage() {
      return this.image;
   }

   public List getItems() {
      return this.items;
   }

   public String getLanguage() {
      return this.language;
   }

   public String getLinkType() {
      return this.linkType;
   }

   public String getLinkUrl() {
      return this.linkUrl;
   }

   public List getLinks() {
      return this.links;
   }

   public List getOptions() {
      return this.options;
   }

   public String getPreviewUrl() {
      return this.previewUrl;
   }

   public String getProvider() {
      return this.provider;
   }

   public int getRatingIndex() {
      return this.ratingIndex;
   }

   public String getRemark() {
      return this.remark;
   }

   public String getSiteName() {
      return this.siteName;
   }

   public String getText() {
      return this.text;
   }

   public String getTitle() {
      return this.title;
   }

   public String getTrackingUrl() {
      return this.trackingUrl;
   }

   public BlockType getType() {
      return this.type;
   }

   public String getUrl() {
      return this.url;
   }

   public String getUsername() {
      return this.username;
   }

   public int getWidth() {
      return this.width;
   }

   public int hashCode() {
      int var28 = 0;
      int var1;
      if(this.attachments != null) {
         var1 = this.attachments.hashCode();
      } else {
         var1 = 0;
      }

      int var2;
      if(this.items != null) {
         var2 = this.items.hashCode();
      } else {
         var2 = 0;
      }

      int var3;
      if(this.data != null) {
         var3 = this.data.hashCode();
      } else {
         var3 = 0;
      }

      int var4;
      if(this.type != null) {
         var4 = this.type.hashCode();
      } else {
         var4 = 0;
      }

      int var5;
      if(this.align != null) {
         var5 = this.align.hashCode();
      } else {
         var5 = 0;
      }

      int var6;
      if(this.author != null) {
         var6 = this.author.hashCode();
      } else {
         var6 = 0;
      }

      int var7;
      if(this.image != null) {
         var7 = this.image.hashCode();
      } else {
         var7 = 0;
      }

      int var8;
      if(this.text != null) {
         var8 = this.text.hashCode();
      } else {
         var8 = 0;
      }

      int var9;
      if(this.title != null) {
         var9 = this.title.hashCode();
      } else {
         var9 = 0;
      }

      int var10;
      if(this.description != null) {
         var10 = this.description.hashCode();
      } else {
         var10 = 0;
      }

      int var11;
      if(this.linkType != null) {
         var11 = this.linkType.hashCode();
      } else {
         var11 = 0;
      }

      int var12;
      if(this.siteName != null) {
         var12 = this.siteName.hashCode();
      } else {
         var12 = 0;
      }

      int var13;
      if(this.articleId != null) {
         var13 = this.articleId.hashCode();
      } else {
         var13 = 0;
      }

      int var14;
      if(this.language != null) {
         var14 = this.language.hashCode();
      } else {
         var14 = 0;
      }

      int var15;
      if(this.url != null) {
         var15 = this.url.hashCode();
      } else {
         var15 = 0;
      }

      int var16;
      if(this.previewUrl != null) {
         var16 = this.previewUrl.hashCode();
      } else {
         var16 = 0;
      }

      int var17;
      if(this.attribution != null) {
         var17 = this.attribution.hashCode();
      } else {
         var17 = 0;
      }

      int var18;
      if(this.linkUrl != null) {
         var18 = this.linkUrl.hashCode();
      } else {
         var18 = 0;
      }

      int var19;
      if(this.embedUrl != null) {
         var19 = this.embedUrl.hashCode();
      } else {
         var19 = 0;
      }

      int var20;
      if(this.trackingUrl != null) {
         var20 = this.trackingUrl.hashCode();
      } else {
         var20 = 0;
      }

      int var21;
      if(this.username != null) {
         var21 = this.username.hashCode();
      } else {
         var21 = 0;
      }

      int var22;
      if(this.provider != null) {
         var22 = this.provider.hashCode();
      } else {
         var22 = 0;
      }

      int var23;
      if(this.id != null) {
         var23 = this.id.hashCode();
      } else {
         var23 = 0;
      }

      int var29 = this.width;
      int var30 = this.height;
      int var24;
      if(this.channels != null) {
         var24 = this.channels.hashCode();
      } else {
         var24 = 0;
      }

      int var31 = this.ratingIndex;
      int var25;
      if(this.remark != null) {
         var25 = this.remark.hashCode();
      } else {
         var25 = 0;
      }

      int var26;
      if(this.options != null) {
         var26 = this.options.hashCode();
      } else {
         var26 = 0;
      }

      int var27;
      if(this.links != null) {
         var27 = this.links.hashCode();
      } else {
         var27 = 0;
      }

      if(this.footerLink != null) {
         var28 = this.footerLink.hashCode();
      }

      return (var27 + (var26 + (var25 + ((var24 + (((var23 + (var22 + (var21 + (var20 + (var19 + (var18 + (var17 + (var16 + (var15 + (var14 + (var13 + (var12 + (var11 + (var10 + (var9 + (var8 + (var7 + (var6 + (var5 + (var4 + (var3 + (var2 + var1 * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31 + var29) * 31 + var30) * 31) * 31 + var31) * 31) * 31) * 31) * 31 + var28;
   }

   public Block.Builder toBuilder() {
      Object var2 = null;
      Block.Builder var3 = new Block.Builder();
      switch(null.$SwitchMap$io$intercom$android$blocks$BlockType[this.type.ordinal()]) {
      case 1:
         var3.withText(this.text).withType(this.type.getSerializedName());
         break;
      case 2:
      case 3:
         Block.Builder var4 = var3.withType(this.type.getSerializedName()).withUrl(this.url);
         String var1;
         if(this.attribution.isEmpty()) {
            var1 = null;
         } else {
            var1 = this.attribution;
         }

         var4 = var4.withAttribution(var1);
         if(this.previewUrl.isEmpty()) {
            var1 = (String)var2;
         } else {
            var1 = this.previewUrl;
         }

         var4.withPreviewUrl(var1).withHeight(this.height).withWidth(this.width);
         break;
      case 4:
         var3.withType(this.type.getSerializedName()).withAttachments(this.attachments);
      }

      return var3;
   }

   public String toString() {
      return "Block{attachments=" + this.attachments + ", items=" + this.items + ", data=" + this.data + ", type=" + this.type + ", align=" + this.align + ", author=" + this.author + ", image=" + this.image + ", text='" + this.text + '\'' + ", title='" + this.title + '\'' + ", description='" + this.description + '\'' + ", linkType='" + this.linkType + '\'' + ", siteName='" + this.siteName + '\'' + ", articleId='" + this.articleId + '\'' + ", language='" + this.language + '\'' + ", url='" + this.url + '\'' + ", previewUrl='" + this.previewUrl + '\'' + ", attribution='" + this.attribution + '\'' + ", linkUrl='" + this.linkUrl + '\'' + ", embedUrl='" + this.embedUrl + '\'' + ", trackingUrl='" + this.trackingUrl + '\'' + ", username='" + this.username + '\'' + ", provider='" + this.provider + '\'' + ", id='" + this.id + '\'' + ", width=" + this.width + ", height=" + this.height + ", channels=" + this.channels + ", ratingIndex=" + this.ratingIndex + ", remark='" + this.remark + '\'' + ", options=" + this.options + ", links=" + this.links + ", footerLink=" + this.footerLink + '}';
   }

   public void writeToParcel(Parcel var1, int var2) {
      byte var4 = -1;
      var1.writeTypedList(this.attachments);
      var1.writeStringList(this.items);
      var1.writeInt(this.data.size());
      Iterator var6 = this.data.entrySet().iterator();

      while(var6.hasNext()) {
         Entry var5 = (Entry)var6.next();
         var1.writeString((String)var5.getKey());
         var1.writeString((String)var5.getValue());
      }

      int var3;
      if(this.type == null) {
         var3 = -1;
      } else {
         var3 = this.type.ordinal();
      }

      var1.writeInt(var3);
      if(this.align == null) {
         var3 = var4;
      } else {
         var3 = this.align.ordinal();
      }

      var1.writeInt(var3);
      var1.writeParcelable(this.author, var2);
      var1.writeParcelable(this.image, var2);
      var1.writeString(this.text);
      var1.writeString(this.title);
      var1.writeString(this.description);
      var1.writeString(this.linkType);
      var1.writeString(this.siteName);
      var1.writeString(this.articleId);
      var1.writeString(this.language);
      var1.writeString(this.url);
      var1.writeString(this.linkUrl);
      var1.writeString(this.embedUrl);
      var1.writeString(this.trackingUrl);
      var1.writeString(this.username);
      var1.writeString(this.provider);
      var1.writeString(this.previewUrl);
      var1.writeString(this.attribution);
      var1.writeString(this.id);
      var1.writeInt(this.width);
      var1.writeInt(this.height);
      var1.writeTypedList(this.channels);
      var1.writeInt(this.ratingIndex);
      var1.writeString(this.remark);
      var1.writeTypedList(this.options);
      var1.writeTypedList(this.links);
      var1.writeParcelable(this.footerLink, var2);
   }

   public static final class Builder {
      String align;
      String articleId;
      List attachments;
      String attribution;
      Author author;
      List channels;
      Map data;
      String description;
      String embedUrl;
      Block.Builder footerLink;
      Integer height;
      String id;
      Image image;
      List items;
      String language;
      String linkType;
      String linkUrl;
      List links;
      List options;
      String previewUrl;
      String provider;
      Integer ratingIndex;
      Integer rating_index;
      String remark;
      String siteName;
      String text;
      String title;
      String trackingUrl;
      String type;
      String url;
      String username;
      Integer width;

      public Block build() {
         return new Block(this, null);
      }

      public boolean equals(Object var1) {
         boolean var3 = true;
         boolean var4 = false;
         boolean var2;
         if(this == var1) {
            var2 = true;
         } else {
            var2 = var4;
            if(var1 != null) {
               var2 = var4;
               if(this.getClass() == var1.getClass()) {
                  Block.Builder var5 = (Block.Builder)var1;
                  if(this.type != null) {
                     var2 = var4;
                     if(!this.type.equals(var5.type)) {
                        return var2;
                     }
                  } else if(var5.type != null) {
                     var2 = var4;
                     return var2;
                  }

                  if(this.text != null) {
                     var2 = var4;
                     if(!this.text.equals(var5.text)) {
                        return var2;
                     }
                  } else if(var5.text != null) {
                     var2 = var4;
                     return var2;
                  }

                  if(this.title != null) {
                     var2 = var4;
                     if(!this.title.equals(var5.title)) {
                        return var2;
                     }
                  } else if(var5.title != null) {
                     var2 = var4;
                     return var2;
                  }

                  if(this.description != null) {
                     var2 = var4;
                     if(!this.description.equals(var5.description)) {
                        return var2;
                     }
                  } else if(var5.description != null) {
                     var2 = var4;
                     return var2;
                  }

                  if(this.linkType != null) {
                     var2 = var4;
                     if(!this.linkType.equals(var5.linkType)) {
                        return var2;
                     }
                  } else if(var5.linkType != null) {
                     var2 = var4;
                     return var2;
                  }

                  if(this.siteName != null) {
                     var2 = var4;
                     if(!this.siteName.equals(var5.siteName)) {
                        return var2;
                     }
                  } else if(var5.siteName != null) {
                     var2 = var4;
                     return var2;
                  }

                  if(this.articleId != null) {
                     var2 = var4;
                     if(!this.articleId.equals(var5.articleId)) {
                        return var2;
                     }
                  } else if(var5.articleId != null) {
                     var2 = var4;
                     return var2;
                  }

                  if(this.author != null) {
                     var2 = var4;
                     if(!this.author.equals(var5.author)) {
                        return var2;
                     }
                  } else if(var5.author != null) {
                     var2 = var4;
                     return var2;
                  }

                  if(this.image != null) {
                     var2 = var4;
                     if(!this.image.equals(var5.image)) {
                        return var2;
                     }
                  } else if(var5.image != null) {
                     var2 = var4;
                     return var2;
                  }

                  if(this.data != null) {
                     var2 = var4;
                     if(!this.data.equals(var5.data)) {
                        return var2;
                     }
                  } else if(var5.data != null) {
                     var2 = var4;
                     return var2;
                  }

                  if(this.language != null) {
                     var2 = var4;
                     if(!this.language.equals(var5.language)) {
                        return var2;
                     }
                  } else if(var5.language != null) {
                     var2 = var4;
                     return var2;
                  }

                  if(this.url != null) {
                     var2 = var4;
                     if(!this.url.equals(var5.url)) {
                        return var2;
                     }
                  } else if(var5.url != null) {
                     var2 = var4;
                     return var2;
                  }

                  if(this.linkUrl != null) {
                     var2 = var4;
                     if(!this.linkUrl.equals(var5.linkUrl)) {
                        return var2;
                     }
                  } else if(var5.linkUrl != null) {
                     var2 = var4;
                     return var2;
                  }

                  if(this.embedUrl != null) {
                     var2 = var4;
                     if(!this.embedUrl.equals(var5.embedUrl)) {
                        return var2;
                     }
                  } else if(var5.embedUrl != null) {
                     var2 = var4;
                     return var2;
                  }

                  if(this.trackingUrl != null) {
                     var2 = var4;
                     if(!this.trackingUrl.equals(var5.trackingUrl)) {
                        return var2;
                     }
                  } else if(var5.trackingUrl != null) {
                     var2 = var4;
                     return var2;
                  }

                  if(this.username != null) {
                     var2 = var4;
                     if(!this.username.equals(var5.username)) {
                        return var2;
                     }
                  } else if(var5.username != null) {
                     var2 = var4;
                     return var2;
                  }

                  if(this.provider != null) {
                     var2 = var4;
                     if(!this.provider.equals(var5.provider)) {
                        return var2;
                     }
                  } else if(var5.provider != null) {
                     var2 = var4;
                     return var2;
                  }

                  if(this.id != null) {
                     var2 = var4;
                     if(!this.id.equals(var5.id)) {
                        return var2;
                     }
                  } else if(var5.id != null) {
                     var2 = var4;
                     return var2;
                  }

                  if(this.align != null) {
                     var2 = var4;
                     if(!this.align.equals(var5.align)) {
                        return var2;
                     }
                  } else if(var5.align != null) {
                     var2 = var4;
                     return var2;
                  }

                  if(this.width != null) {
                     var2 = var4;
                     if(!this.width.equals(var5.width)) {
                        return var2;
                     }
                  } else if(var5.width != null) {
                     var2 = var4;
                     return var2;
                  }

                  if(this.height != null) {
                     var2 = var4;
                     if(!this.height.equals(var5.height)) {
                        return var2;
                     }
                  } else if(var5.height != null) {
                     var2 = var4;
                     return var2;
                  }

                  if(this.previewUrl != null) {
                     var2 = var4;
                     if(!this.previewUrl.equals(var5.previewUrl)) {
                        return var2;
                     }
                  } else if(var5.previewUrl != null) {
                     var2 = var4;
                     return var2;
                  }

                  if(this.attribution != null) {
                     var2 = var4;
                     if(!this.attribution.equals(var5.attribution)) {
                        return var2;
                     }
                  } else if(var5.attribution != null) {
                     var2 = var4;
                     return var2;
                  }

                  if(this.attachments != null) {
                     var2 = var4;
                     if(!this.attachments.equals(var5.attachments)) {
                        return var2;
                     }
                  } else if(var5.attachments != null) {
                     var2 = var4;
                     return var2;
                  }

                  if(this.items != null) {
                     var2 = var4;
                     if(!this.items.equals(var5.items)) {
                        return var2;
                     }
                  } else if(var5.items != null) {
                     var2 = var4;
                     return var2;
                  }

                  if(this.channels != null) {
                     var2 = var4;
                     if(!this.channels.equals(var5.channels)) {
                        return var2;
                     }
                  } else if(var5.channels != null) {
                     var2 = var4;
                     return var2;
                  }

                  if(this.rating_index != null) {
                     var2 = var4;
                     if(!this.rating_index.equals(var5.rating_index)) {
                        return var2;
                     }
                  } else if(var5.rating_index != null) {
                     var2 = var4;
                     return var2;
                  }

                  if(this.ratingIndex != null) {
                     var2 = var4;
                     if(!this.ratingIndex.equals(var5.ratingIndex)) {
                        return var2;
                     }
                  } else if(var5.ratingIndex != null) {
                     var2 = var4;
                     return var2;
                  }

                  if(this.remark != null) {
                     var2 = var4;
                     if(!this.remark.equals(var5.remark)) {
                        return var2;
                     }
                  } else if(var5.remark != null) {
                     var2 = var4;
                     return var2;
                  }

                  if(this.options != null) {
                     var2 = var4;
                     if(!this.options.equals(var5.options)) {
                        return var2;
                     }
                  } else if(var5.options != null) {
                     var2 = var4;
                     return var2;
                  }

                  if(this.links != null) {
                     var2 = var4;
                     if(!this.links.equals(var5.links)) {
                        return var2;
                     }
                  } else if(var5.links != null) {
                     var2 = var4;
                     return var2;
                  }

                  if(this.footerLink != null) {
                     var2 = this.footerLink.equals(var5.footerLink);
                  } else {
                     var2 = var3;
                     if(var5.footerLink != null) {
                        var2 = false;
                     }
                  }
               }
            }
         }

         return var2;
      }

      public int hashCode() {
         int var32 = 0;
         int var1;
         if(this.type != null) {
            var1 = this.type.hashCode();
         } else {
            var1 = 0;
         }

         int var2;
         if(this.text != null) {
            var2 = this.text.hashCode();
         } else {
            var2 = 0;
         }

         int var3;
         if(this.title != null) {
            var3 = this.title.hashCode();
         } else {
            var3 = 0;
         }

         int var4;
         if(this.description != null) {
            var4 = this.description.hashCode();
         } else {
            var4 = 0;
         }

         int var5;
         if(this.linkType != null) {
            var5 = this.linkType.hashCode();
         } else {
            var5 = 0;
         }

         int var6;
         if(this.siteName != null) {
            var6 = this.siteName.hashCode();
         } else {
            var6 = 0;
         }

         int var7;
         if(this.articleId != null) {
            var7 = this.articleId.hashCode();
         } else {
            var7 = 0;
         }

         int var8;
         if(this.author != null) {
            var8 = this.author.hashCode();
         } else {
            var8 = 0;
         }

         int var9;
         if(this.image != null) {
            var9 = this.image.hashCode();
         } else {
            var9 = 0;
         }

         int var10;
         if(this.data != null) {
            var10 = this.data.hashCode();
         } else {
            var10 = 0;
         }

         int var11;
         if(this.language != null) {
            var11 = this.language.hashCode();
         } else {
            var11 = 0;
         }

         int var12;
         if(this.url != null) {
            var12 = this.url.hashCode();
         } else {
            var12 = 0;
         }

         int var13;
         if(this.linkUrl != null) {
            var13 = this.linkUrl.hashCode();
         } else {
            var13 = 0;
         }

         int var14;
         if(this.embedUrl != null) {
            var14 = this.embedUrl.hashCode();
         } else {
            var14 = 0;
         }

         int var15;
         if(this.trackingUrl != null) {
            var15 = this.trackingUrl.hashCode();
         } else {
            var15 = 0;
         }

         int var16;
         if(this.username != null) {
            var16 = this.username.hashCode();
         } else {
            var16 = 0;
         }

         int var17;
         if(this.provider != null) {
            var17 = this.provider.hashCode();
         } else {
            var17 = 0;
         }

         int var18;
         if(this.id != null) {
            var18 = this.id.hashCode();
         } else {
            var18 = 0;
         }

         int var19;
         if(this.align != null) {
            var19 = this.align.hashCode();
         } else {
            var19 = 0;
         }

         int var20;
         if(this.width != null) {
            var20 = this.width.hashCode();
         } else {
            var20 = 0;
         }

         int var21;
         if(this.height != null) {
            var21 = this.height.hashCode();
         } else {
            var21 = 0;
         }

         int var22;
         if(this.previewUrl != null) {
            var22 = this.previewUrl.hashCode();
         } else {
            var22 = 0;
         }

         int var23;
         if(this.attribution != null) {
            var23 = this.attribution.hashCode();
         } else {
            var23 = 0;
         }

         int var24;
         if(this.attachments != null) {
            var24 = this.attachments.hashCode();
         } else {
            var24 = 0;
         }

         int var25;
         if(this.items != null) {
            var25 = this.items.hashCode();
         } else {
            var25 = 0;
         }

         int var26;
         if(this.channels != null) {
            var26 = this.channels.hashCode();
         } else {
            var26 = 0;
         }

         int var27;
         if(this.rating_index != null) {
            var27 = this.rating_index.hashCode();
         } else {
            var27 = 0;
         }

         int var28;
         if(this.ratingIndex != null) {
            var28 = this.ratingIndex.hashCode();
         } else {
            var28 = 0;
         }

         int var29;
         if(this.remark != null) {
            var29 = this.remark.hashCode();
         } else {
            var29 = 0;
         }

         int var30;
         if(this.options != null) {
            var30 = this.options.hashCode();
         } else {
            var30 = 0;
         }

         int var31;
         if(this.links != null) {
            var31 = this.links.hashCode();
         } else {
            var31 = 0;
         }

         if(this.footerLink != null) {
            var32 = this.footerLink.hashCode();
         }

         return (var31 + (var30 + (var29 + (var28 + (var27 + (var26 + (var25 + (var24 + (var23 + (var22 + (var21 + (var20 + (var19 + (var18 + (var17 + (var16 + (var15 + (var14 + (var13 + (var12 + (var11 + (var10 + (var9 + (var8 + (var7 + (var6 + (var5 + (var4 + (var3 + (var2 + var1 * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31 + var32;
      }

      public Block.Builder withAlign(String var1) {
         this.align = var1;
         return this;
      }

      public Block.Builder withArticleId(String var1) {
         this.articleId = var1;
         return this;
      }

      public Block.Builder withAttachments(List var1) {
         this.attachments = var1;
         return this;
      }

      public Block.Builder withAttribution(String var1) {
         this.attribution = var1;
         return this;
      }

      public Block.Builder withAuthor(Author var1) {
         this.author = var1;
         return this;
      }

      public Block.Builder withChannels(List var1) {
         this.channels = var1;
         return this;
      }

      public Block.Builder withData(Map var1) {
         this.data = var1;
         return this;
      }

      public Block.Builder withDescription(String var1) {
         this.description = var1;
         return this;
      }

      public Block.Builder withHeight(int var1) {
         this.height = Integer.valueOf(var1);
         return this;
      }

      public Block.Builder withImage(Image var1) {
         this.image = var1;
         return this;
      }

      public Block.Builder withItems(List var1) {
         this.items = var1;
         return this;
      }

      public Block.Builder withLinkType(String var1) {
         this.linkType = var1;
         return this;
      }

      public Block.Builder withOptions(List var1) {
         this.options = var1;
         return this;
      }

      public Block.Builder withPreviewUrl(String var1) {
         this.previewUrl = var1;
         return this;
      }

      public Block.Builder withRatingIndex(Integer var1) {
         this.ratingIndex = var1;
         return this;
      }

      public Block.Builder withRemark(String var1) {
         this.remark = var1;
         return this;
      }

      public Block.Builder withSiteName(String var1) {
         this.siteName = var1;
         return this;
      }

      public Block.Builder withText(String var1) {
         this.text = var1;
         return this;
      }

      public Block.Builder withTitle(String var1) {
         this.title = var1;
         return this;
      }

      public Block.Builder withType(String var1) {
         this.type = var1;
         return this;
      }

      public Block.Builder withUrl(String var1) {
         this.url = var1;
         return this;
      }

      public Block.Builder withWidth(int var1) {
         this.width = Integer.valueOf(var1);
         return this;
      }
   }
}
