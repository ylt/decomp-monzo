package io.intercom.android.sdk.blocks.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.util.ArrayList;
import java.util.List;

public class ConversationRating implements Parcelable {
   public static final Creator CREATOR = new Creator() {
      public ConversationRating createFromParcel(Parcel var1) {
         return new ConversationRating(var1);
      }

      public ConversationRating[] newArray(int var1) {
         return new ConversationRating[var1];
      }
   };
   private final List options;
   private int ratingIndex;
   private String remark;

   ConversationRating(int var1, String var2, List var3) {
      this.ratingIndex = var1;
      this.remark = var2;
      this.options = var3;
   }

   protected ConversationRating(Parcel var1) {
      this.ratingIndex = var1.readInt();
      this.remark = var1.readString();
      this.options = new ArrayList();
      var1.readList(this.options, ConversationRatingOption.class.getClassLoader());
   }

   public static ConversationRating fromBlock(Block var0) {
      ConversationRating var1;
      if(var0 == null) {
         var1 = new ConversationRating(-1, "", new ArrayList());
      } else {
         var1 = new ConversationRating(var0.getRatingIndex(), var0.getRemark(), var0.getOptions());
      }

      return var1;
   }

   public int describeContents() {
      return 0;
   }

   public boolean equals(Object var1) {
      boolean var3 = true;
      boolean var4 = false;
      boolean var2;
      if(this == var1) {
         var2 = true;
      } else {
         var2 = var4;
         if(var1 != null) {
            var2 = var4;
            if(this.getClass() == var1.getClass()) {
               ConversationRating var5 = (ConversationRating)var1;
               var2 = var4;
               if(this.ratingIndex == var5.ratingIndex) {
                  if(this.remark != null) {
                     var2 = var4;
                     if(!this.remark.equals(var5.remark)) {
                        return var2;
                     }
                  } else if(var5.remark != null) {
                     var2 = var4;
                     return var2;
                  }

                  if(this.options != null) {
                     var2 = this.options.equals(var5.options);
                  } else {
                     var2 = var3;
                     if(var5.options != null) {
                        var2 = false;
                     }
                  }
               }
            }
         }
      }

      return var2;
   }

   public List getOptions() {
      return this.options;
   }

   public Integer getRatingIndex() {
      return Integer.valueOf(this.ratingIndex);
   }

   public String getRemark() {
      return this.remark;
   }

   public int hashCode() {
      int var2 = 0;
      int var3 = this.ratingIndex;
      int var1;
      if(this.remark != null) {
         var1 = this.remark.hashCode();
      } else {
         var1 = 0;
      }

      if(this.options != null) {
         var2 = this.options.hashCode();
      }

      return (var1 + var3 * 31) * 31 + var2;
   }

   public void setRatingIndex(int var1) {
      this.ratingIndex = var1;
   }

   public void setRemark(String var1) {
      this.remark = var1;
   }

   public void writeToParcel(Parcel var1, int var2) {
      var1.writeInt(this.ratingIndex);
      var1.writeString(this.remark);
      var1.writeList(this.options);
   }
}
