package io.intercom.android.sdk.blocks.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import io.intercom.android.sdk.blocks.BlockType;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class Link implements Parcelable {
   public static final Creator CREATOR = new Creator() {
      public Link createFromParcel(Parcel var1) {
         return new Link(var1);
      }

      public Link[] newArray(int var1) {
         return new Link[var1];
      }
   };
   private final String articleId;
   private final Author author;
   private final Map data;
   private final String description;
   private final Image image;
   private final String linkType;
   private final String siteName;
   private final String text;
   private final String title;
   private final BlockType type;
   private final String url;

   Link() {
      this(new Link.Builder());
   }

   protected Link(Parcel var1) {
      this.type = BlockType.typeValueOf(var1.readString());
      this.text = var1.readString();
      this.title = var1.readString();
      this.description = var1.readString();
      this.linkType = var1.readString();
      this.siteName = var1.readString();
      this.articleId = var1.readString();
      this.url = var1.readString();
      this.author = (Author)var1.readParcelable(Author.class.getClassLoader());
      this.image = (Image)var1.readParcelable(Image.class.getClassLoader());
      this.data = new HashMap();
      int var3 = var1.readInt();

      for(int var2 = 0; var2 < var3; ++var2) {
         String var5 = var1.readString();
         String var4 = var1.readString();
         this.data.put(var5, var4);
      }

   }

   private Link(Link.Builder var1) {
      this.type = BlockType.typeValueOf(var1.type);
      String var2;
      if(var1.text == null) {
         var2 = "";
      } else {
         var2 = var1.text;
      }

      this.text = var2;
      if(var1.title == null) {
         var2 = "";
      } else {
         var2 = var1.title;
      }

      this.title = var2;
      if(var1.description == null) {
         var2 = "";
      } else {
         var2 = var1.description;
      }

      this.description = var2;
      if(var1.linkType == null) {
         var2 = "";
      } else {
         var2 = var1.linkType;
      }

      this.linkType = var2;
      if(var1.siteName == null) {
         var2 = "";
      } else {
         var2 = var1.siteName;
      }

      this.siteName = var2;
      if(var1.articleId == null) {
         var2 = "";
      } else {
         var2 = var1.articleId;
      }

      this.articleId = var2;
      Author var4;
      if(var1.author == null) {
         var4 = new Author();
      } else {
         var4 = var1.author;
      }

      this.author = var4;
      Image var5;
      if(var1.image == null) {
         var5 = new Image();
      } else {
         var5 = var1.image;
      }

      this.image = var5;
      Object var6;
      if(var1.data == null) {
         var6 = new HashMap();
      } else {
         var6 = var1.data;
      }

      this.data = (Map)var6;
      String var3;
      if(var1.url == null) {
         var3 = "";
      } else {
         var3 = var1.url;
      }

      this.url = var3;
   }

   // $FF: synthetic method
   Link(Link.Builder var1, Object var2) {
      this(var1);
   }

   public static Link fromBlock(Block var0) {
      Link var2;
      if(var0 == null) {
         var2 = new Link();
      } else {
         Link.Builder var1 = new Link.Builder();
         var1.type = var0.getType().name();
         var1.text = var0.getText();
         var1.title = var0.getTitle();
         var1.description = var0.getDescription();
         var1.linkType = var0.getLinkType();
         var1.author = var0.getAuthor();
         var1.image = var0.getImage();
         var1.data = var0.getData();
         var1.siteName = var0.getSiteName();
         var1.articleId = var0.getArticleId();
         var1.url = var0.getUrl();
         var2 = new Link(var1);
      }

      return var2;
   }

   public int describeContents() {
      return 0;
   }

   public boolean equals(Object var1) {
      boolean var4 = true;
      boolean var3 = false;
      boolean var2;
      if(this == var1) {
         var2 = true;
      } else {
         var2 = var3;
         if(var1 != null) {
            var2 = var3;
            if(this.getClass() == var1.getClass()) {
               Link var5 = (Link)var1;
               var2 = var3;
               if(this.type == var5.type) {
                  if(this.text != null) {
                     var2 = var3;
                     if(!this.text.equals(var5.text)) {
                        return var2;
                     }
                  } else if(var5.text != null) {
                     var2 = var3;
                     return var2;
                  }

                  if(this.title != null) {
                     var2 = var3;
                     if(!this.title.equals(var5.title)) {
                        return var2;
                     }
                  } else if(var5.title != null) {
                     var2 = var3;
                     return var2;
                  }

                  if(this.description != null) {
                     var2 = var3;
                     if(!this.description.equals(var5.description)) {
                        return var2;
                     }
                  } else if(var5.description != null) {
                     var2 = var3;
                     return var2;
                  }

                  if(this.linkType != null) {
                     var2 = var3;
                     if(!this.linkType.equals(var5.linkType)) {
                        return var2;
                     }
                  } else if(var5.linkType != null) {
                     var2 = var3;
                     return var2;
                  }

                  if(this.author != null) {
                     var2 = var3;
                     if(!this.author.equals(var5.author)) {
                        return var2;
                     }
                  } else if(var5.author != null) {
                     var2 = var3;
                     return var2;
                  }

                  if(this.image != null) {
                     var2 = var3;
                     if(!this.image.equals(var5.image)) {
                        return var2;
                     }
                  } else if(var5.image != null) {
                     var2 = var3;
                     return var2;
                  }

                  if(this.data != null) {
                     var2 = var3;
                     if(!this.data.equals(var5.data)) {
                        return var2;
                     }
                  } else if(var5.data != null) {
                     var2 = var3;
                     return var2;
                  }

                  if(this.siteName != null) {
                     var2 = var3;
                     if(!this.siteName.equals(var5.siteName)) {
                        return var2;
                     }
                  } else if(var5.siteName != null) {
                     var2 = var3;
                     return var2;
                  }

                  if(this.articleId != null) {
                     var2 = var3;
                     if(!this.articleId.equals(var5.articleId)) {
                        return var2;
                     }
                  } else if(var5.articleId != null) {
                     var2 = var3;
                     return var2;
                  }

                  if(this.url != null) {
                     var2 = this.url.equals(var5.url);
                  } else {
                     var2 = var4;
                     if(var5.url != null) {
                        var2 = false;
                     }
                  }
               }
            }
         }
      }

      return var2;
   }

   public String getArticleId() {
      return this.articleId;
   }

   public Author getAuthor() {
      return this.author;
   }

   public Map getData() {
      return this.data;
   }

   public String getDescription() {
      return this.description;
   }

   public Image getImage() {
      return this.image;
   }

   public String getLinkType() {
      return this.linkType;
   }

   public String getSiteName() {
      return this.siteName;
   }

   public String getText() {
      return this.text;
   }

   public String getTitle() {
      return this.title;
   }

   public BlockType getType() {
      return this.type;
   }

   public String getUrl() {
      return this.url;
   }

   public int hashCode() {
      int var11 = 0;
      int var1;
      if(this.type != null) {
         var1 = this.type.hashCode();
      } else {
         var1 = 0;
      }

      int var2;
      if(this.text != null) {
         var2 = this.text.hashCode();
      } else {
         var2 = 0;
      }

      int var3;
      if(this.title != null) {
         var3 = this.title.hashCode();
      } else {
         var3 = 0;
      }

      int var4;
      if(this.description != null) {
         var4 = this.description.hashCode();
      } else {
         var4 = 0;
      }

      int var5;
      if(this.linkType != null) {
         var5 = this.linkType.hashCode();
      } else {
         var5 = 0;
      }

      int var6;
      if(this.author != null) {
         var6 = this.author.hashCode();
      } else {
         var6 = 0;
      }

      int var7;
      if(this.image != null) {
         var7 = this.image.hashCode();
      } else {
         var7 = 0;
      }

      int var8;
      if(this.data != null) {
         var8 = this.data.hashCode();
      } else {
         var8 = 0;
      }

      int var9;
      if(this.siteName != null) {
         var9 = this.siteName.hashCode();
      } else {
         var9 = 0;
      }

      int var10;
      if(this.articleId != null) {
         var10 = this.articleId.hashCode();
      } else {
         var10 = 0;
      }

      if(this.url != null) {
         var11 = this.url.hashCode();
      }

      return (var10 + (var9 + (var8 + (var7 + (var6 + (var5 + (var4 + (var3 + (var2 + var1 * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31 + var11;
   }

   public String toString() {
      return "LinkCard{type=" + this.type + ", text='" + this.text + '\'' + ", title='" + this.title + '\'' + ", description='" + this.description + '\'' + ", linkType='" + this.linkType + '\'' + ", author=" + this.author + ", image=" + this.image + ", data=" + this.data + ", siteName='" + this.siteName + '\'' + ", articleId='" + this.articleId + '\'' + ", url='" + this.url + '\'' + '}';
   }

   public void writeToParcel(Parcel var1, int var2) {
      var1.writeString(this.type.name());
      var1.writeString(this.text);
      var1.writeString(this.title);
      var1.writeString(this.description);
      var1.writeString(this.linkType);
      var1.writeString(this.siteName);
      var1.writeString(this.articleId);
      var1.writeString(this.url);
      var1.writeParcelable(this.author, var2);
      var1.writeParcelable(this.image, var2);
      var1.writeInt(this.data.size());
      Iterator var3 = this.data.entrySet().iterator();

      while(var3.hasNext()) {
         Entry var4 = (Entry)var3.next();
         var1.writeString((String)var4.getKey());
         var1.writeString((String)var4.getValue());
      }

   }

   public static final class Builder {
      String articleId;
      Author author;
      Map data;
      String description;
      Image image;
      String linkType;
      String siteName;
      String text;
      String title;
      String type;
      String url;

      public Link build() {
         return new Link(this, null);
      }

      public Link.Builder withArticleId(String var1) {
         this.articleId = var1;
         return this;
      }

      public Link.Builder withAuthor(Author var1) {
         this.author = var1;
         return this;
      }

      public Link.Builder withData(Map var1) {
         this.data = var1;
         return this;
      }

      public Link.Builder withDescription(String var1) {
         this.description = var1;
         return this;
      }

      public Link.Builder withImage(Image var1) {
         this.image = var1;
         return this;
      }

      public Link.Builder withLinkType(String var1) {
         this.linkType = var1;
         return this;
      }

      public Link.Builder withSiteName(String var1) {
         this.siteName = var1;
         return this;
      }

      public Link.Builder withText(String var1) {
         this.text = var1;
         return this;
      }

      public Link.Builder withTitle(String var1) {
         this.title = var1;
         return this;
      }

      public Link.Builder withType(String var1) {
         this.type = var1;
         return this;
      }

      public Link.Builder withUrl(String var1) {
         this.url = var1;
         return this;
      }
   }
}
