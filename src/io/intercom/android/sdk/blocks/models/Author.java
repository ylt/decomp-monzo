package io.intercom.android.sdk.blocks.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class Author implements Parcelable {
   public static final Creator CREATOR = new Creator() {
      public Author createFromParcel(Parcel var1) {
         return new Author(var1);
      }

      public Author[] newArray(int var1) {
         return new Author[var1];
      }
   };
   private final String avatar;
   private final String firstName;
   private final String lastName;
   private final String name;

   Author() {
      this(new Author.Builder());
   }

   protected Author(Parcel var1) {
      this.name = var1.readString();
      this.avatar = var1.readString();
      this.firstName = var1.readString();
      this.lastName = var1.readString();
   }

   private Author(Author.Builder var1) {
      String var2;
      if(var1.name == null) {
         var2 = "";
      } else {
         var2 = var1.name;
      }

      this.name = var2;
      if(var1.avatar == null) {
         var2 = "";
      } else {
         var2 = var1.avatar;
      }

      this.avatar = var2;
      if(var1.firstName == null) {
         var2 = "";
      } else {
         var2 = var1.firstName;
      }

      this.firstName = var2;
      String var3;
      if(var1.lastName == null) {
         var3 = "";
      } else {
         var3 = var1.lastName;
      }

      this.lastName = var3;
   }

   // $FF: synthetic method
   Author(Author.Builder var1, Object var2) {
      this(var1);
   }

   public int describeContents() {
      return 0;
   }

   public boolean equals(Object var1) {
      boolean var3 = true;
      boolean var4 = false;
      boolean var2;
      if(this == var1) {
         var2 = true;
      } else {
         var2 = var4;
         if(var1 != null) {
            var2 = var4;
            if(this.getClass() == var1.getClass()) {
               Author var5 = (Author)var1;
               if(this.name != null) {
                  var2 = var4;
                  if(!this.name.equals(var5.name)) {
                     return var2;
                  }
               } else if(var5.name != null) {
                  var2 = var4;
                  return var2;
               }

               if(this.firstName != null) {
                  var2 = var4;
                  if(!this.firstName.equals(var5.firstName)) {
                     return var2;
                  }
               } else if(var5.firstName != null) {
                  var2 = var4;
                  return var2;
               }

               if(this.lastName != null) {
                  var2 = var4;
                  if(!this.lastName.equals(var5.lastName)) {
                     return var2;
                  }
               } else if(var5.lastName != null) {
                  var2 = var4;
                  return var2;
               }

               if(this.avatar != null) {
                  var2 = var3;
                  if(this.avatar.equals(var5.avatar)) {
                     return var2;
                  }
               } else if(var5.avatar == null) {
                  var2 = var3;
                  return var2;
               }

               var2 = false;
            }
         }
      }

      return var2;
   }

   public String getAvatar() {
      return this.avatar;
   }

   public String getFirstName() {
      return this.firstName;
   }

   public String getLastName() {
      return this.lastName;
   }

   public String getName() {
      return this.name;
   }

   public int hashCode() {
      int var4 = 0;
      int var1;
      if(this.name != null) {
         var1 = this.name.hashCode();
      } else {
         var1 = 0;
      }

      int var2;
      if(this.firstName != null) {
         var2 = this.firstName.hashCode();
      } else {
         var2 = 0;
      }

      int var3;
      if(this.lastName != null) {
         var3 = this.lastName.hashCode();
      } else {
         var3 = 0;
      }

      if(this.avatar != null) {
         var4 = this.avatar.hashCode();
      }

      return (var3 + (var2 + var1 * 31) * 31) * 31 + var4;
   }

   public void writeToParcel(Parcel var1, int var2) {
      var1.writeString(this.name);
      var1.writeString(this.avatar);
      var1.writeString(this.firstName);
      var1.writeString(this.lastName);
   }

   public static final class Builder {
      String avatar;
      String firstName;
      String lastName;
      String name;

      public Author build() {
         return new Author(this, null);
      }

      public Author.Builder withAvatar(String var1) {
         this.avatar = var1;
         return this;
      }

      public Author.Builder withFirstName(String var1) {
         this.firstName = var1;
         return this;
      }

      public Author.Builder withLastName(String var1) {
         this.lastName = var1;
         return this;
      }

      public Author.Builder withName(String var1) {
         this.name = var1;
         return this;
      }
   }
}
