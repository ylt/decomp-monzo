package io.intercom.android.sdk.blocks.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class ConversationRatingOption implements Parcelable {
   public static final Creator CREATOR = new Creator() {
      public ConversationRatingOption createFromParcel(Parcel var1) {
         return new ConversationRatingOption(var1);
      }

      public ConversationRatingOption[] newArray(int var1) {
         return new ConversationRatingOption[var1];
      }
   };
   private final String emoji;
   private final int index;
   private final String unicode;

   protected ConversationRatingOption(Parcel var1) {
      this.index = var1.readInt();
      this.emoji = var1.readString();
      this.unicode = var1.readString();
   }

   private ConversationRatingOption(ConversationRatingOption.Builder var1) {
      int var2;
      if(var1.index == null) {
         var2 = -1;
      } else {
         var2 = var1.index.intValue();
      }

      this.index = var2;
      String var3;
      if(var1.emoji == null) {
         var3 = "";
      } else {
         var3 = var1.emoji;
      }

      this.emoji = var3;
      String var4;
      if(var1.unicode == null) {
         var4 = "";
      } else {
         var4 = var1.unicode;
      }

      this.unicode = var4;
   }

   // $FF: synthetic method
   ConversationRatingOption(ConversationRatingOption.Builder var1, Object var2) {
      this(var1);
   }

   public int describeContents() {
      return 0;
   }

   public boolean equals(Object var1) {
      boolean var3 = true;
      boolean var4 = false;
      boolean var2;
      if(this == var1) {
         var2 = true;
      } else {
         var2 = var4;
         if(var1 != null) {
            var2 = var4;
            if(this.getClass() == var1.getClass()) {
               ConversationRatingOption var5 = (ConversationRatingOption)var1;
               var2 = var4;
               if(this.index == var5.index) {
                  if(this.emoji != null) {
                     var2 = var4;
                     if(!this.emoji.equals(var5.emoji)) {
                        return var2;
                     }
                  } else if(var5.emoji != null) {
                     var2 = var4;
                     return var2;
                  }

                  if(this.unicode != null) {
                     var2 = this.unicode.equals(var5.unicode);
                  } else {
                     var2 = var3;
                     if(var5.unicode != null) {
                        var2 = false;
                     }
                  }
               }
            }
         }
      }

      return var2;
   }

   public String getEmoji() {
      return this.emoji;
   }

   public Integer getIndex() {
      return Integer.valueOf(this.index);
   }

   public String getUnicode() {
      return this.unicode;
   }

   public int hashCode() {
      int var2 = 0;
      int var3 = this.index;
      int var1;
      if(this.emoji != null) {
         var1 = this.emoji.hashCode();
      } else {
         var1 = 0;
      }

      if(this.unicode != null) {
         var2 = this.unicode.hashCode();
      }

      return (var1 + var3 * 31) * 31 + var2;
   }

   public void writeToParcel(Parcel var1, int var2) {
      var1.writeInt(this.index);
      var1.writeString(this.emoji);
      var1.writeString(this.unicode);
   }

   public static final class Builder {
      String emoji;
      Integer index;
      String unicode;

      public ConversationRatingOption build() {
         return new ConversationRatingOption(this, null);
      }

      public ConversationRatingOption.Builder withEmoji(String var1) {
         this.emoji = var1;
         return this;
      }

      public ConversationRatingOption.Builder withIndex(Integer var1) {
         this.index = var1;
         return this;
      }

      public ConversationRatingOption.Builder withUnicode(String var1) {
         this.unicode = var1;
         return this;
      }
   }
}
