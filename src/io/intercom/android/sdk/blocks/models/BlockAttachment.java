package io.intercom.android.sdk.blocks.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class BlockAttachment implements Parcelable {
   public static final Creator CREATOR = new Creator() {
      public BlockAttachment createFromParcel(Parcel var1) {
         return new BlockAttachment(var1);
      }

      public BlockAttachment[] newArray(int var1) {
         return new BlockAttachment[var1];
      }
   };
   private final String contentType;
   private final int id;
   private final String name;
   private final long size;
   private final String url;

   public BlockAttachment() {
      this(new BlockAttachment.Builder());
   }

   protected BlockAttachment(Parcel var1) {
      this.name = var1.readString();
      this.url = var1.readString();
      this.contentType = var1.readString();
      this.id = var1.readInt();
      this.size = var1.readLong();
   }

   public BlockAttachment(BlockAttachment.Builder var1) {
      String var2;
      if(var1.name == null) {
         var2 = "";
      } else {
         var2 = var1.name;
      }

      this.name = var2;
      if(var1.url == null) {
         var2 = "";
      } else {
         var2 = var1.url;
      }

      this.url = var2;
      if(var1.contentType == null) {
         var2 = "";
      } else {
         var2 = var1.contentType;
      }

      this.contentType = var2;
      this.id = var1.id;
      this.size = var1.size;
   }

   public int describeContents() {
      return 0;
   }

   public boolean equals(Object var1) {
      boolean var3 = true;
      boolean var4 = false;
      boolean var2;
      if(this == var1) {
         var2 = true;
      } else {
         var2 = var4;
         if(var1 != null) {
            var2 = var4;
            if(this.getClass() == var1.getClass()) {
               BlockAttachment var5 = (BlockAttachment)var1;
               var2 = var4;
               if(this.size == var5.size) {
                  var2 = var4;
                  if(this.id == var5.id) {
                     if(this.name != null) {
                        var2 = var4;
                        if(!this.name.equals(var5.name)) {
                           return var2;
                        }
                     } else if(var5.name != null) {
                        var2 = var4;
                        return var2;
                     }

                     if(this.url != null) {
                        var2 = var4;
                        if(!this.url.equals(var5.url)) {
                           return var2;
                        }
                     } else if(var5.url != null) {
                        var2 = var4;
                        return var2;
                     }

                     if(this.contentType != null) {
                        var2 = this.contentType.equals(var5.contentType);
                     } else {
                        var2 = var3;
                        if(var5.contentType != null) {
                           var2 = false;
                        }
                     }
                  }
               }
            }
         }
      }

      return var2;
   }

   public String getContentType() {
      return this.contentType;
   }

   public int getId() {
      return this.id;
   }

   public String getName() {
      return this.name;
   }

   public long getSize() {
      return this.size;
   }

   public String getUrl() {
      return this.url;
   }

   public int hashCode() {
      int var3 = 0;
      int var1;
      if(this.name != null) {
         var1 = this.name.hashCode();
      } else {
         var1 = 0;
      }

      int var2;
      if(this.url != null) {
         var2 = this.url.hashCode();
      } else {
         var2 = 0;
      }

      if(this.contentType != null) {
         var3 = this.contentType.hashCode();
      }

      return (((var2 + var1 * 31) * 31 + var3) * 31 + (int)(this.size ^ this.size >>> 32)) * 31 + this.id;
   }

   public BlockAttachment.Builder toBuilder() {
      return (new BlockAttachment.Builder()).withName(this.name).withUrl(this.url).withContentType(this.contentType).withId(this.id).withSize(this.size);
   }

   public void writeToParcel(Parcel var1, int var2) {
      var1.writeString(this.name);
      var1.writeString(this.url);
      var1.writeString(this.contentType);
      var1.writeInt(this.id);
      var1.writeLong(this.size);
   }

   public static final class Builder {
      String contentType;
      int id;
      String name;
      long size;
      String url;

      public BlockAttachment build() {
         return new BlockAttachment(this);
      }

      public BlockAttachment.Builder withContentType(String var1) {
         this.contentType = var1;
         return this;
      }

      public BlockAttachment.Builder withId(int var1) {
         this.id = var1;
         return this;
      }

      public BlockAttachment.Builder withName(String var1) {
         this.name = var1;
         return this;
      }

      public BlockAttachment.Builder withSize(long var1) {
         this.size = var1;
         return this;
      }

      public BlockAttachment.Builder withUrl(String var1) {
         this.url = var1;
         return this;
      }
   }
}
