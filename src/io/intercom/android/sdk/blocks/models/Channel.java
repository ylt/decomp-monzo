package io.intercom.android.sdk.blocks.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class Channel implements Parcelable {
   public static final Creator CREATOR = new Creator() {
      public Channel createFromParcel(Parcel var1) {
         return new Channel(var1);
      }

      public Channel[] newArray(int var1) {
         return new Channel[var1];
      }
   };
   private final String label;
   private final String name;
   private final String value;

   Channel() {
      this(new Channel.Builder());
   }

   protected Channel(Parcel var1) {
      this.name = var1.readString();
      this.label = var1.readString();
      this.value = var1.readString();
   }

   private Channel(Channel.Builder var1) {
      String var2;
      if(var1.name == null) {
         var2 = "";
      } else {
         var2 = var1.name;
      }

      this.name = var2;
      if(var1.label == null) {
         var2 = "";
      } else {
         var2 = var1.label;
      }

      this.label = var2;
      String var3;
      if(var1.value == null) {
         var3 = "";
      } else {
         var3 = var1.value;
      }

      this.value = var3;
   }

   // $FF: synthetic method
   Channel(Channel.Builder var1, Object var2) {
      this(var1);
   }

   public int describeContents() {
      return 0;
   }

   public boolean equals(Object var1) {
      boolean var4 = true;
      boolean var3 = false;
      boolean var2;
      if(this == var1) {
         var2 = true;
      } else {
         var2 = var3;
         if(var1 != null) {
            var2 = var3;
            if(this.getClass() == var1.getClass()) {
               Channel var5 = (Channel)var1;
               if(this.name != null) {
                  var2 = var3;
                  if(!this.name.equals(var5.name)) {
                     return var2;
                  }
               } else if(var5.name != null) {
                  var2 = var3;
                  return var2;
               }

               if(this.label != null) {
                  var2 = var3;
                  if(!this.label.equals(var5.label)) {
                     return var2;
                  }
               } else if(var5.label != null) {
                  var2 = var3;
                  return var2;
               }

               if(this.value != null) {
                  var2 = this.value.equals(var5.value);
               } else {
                  var2 = var4;
                  if(var5.value != null) {
                     var2 = false;
                  }
               }
            }
         }
      }

      return var2;
   }

   public String getLabel() {
      return this.label;
   }

   public String getName() {
      return this.name;
   }

   public String getValue() {
      return this.value;
   }

   public int hashCode() {
      int var3 = 0;
      int var1;
      if(this.name != null) {
         var1 = this.name.hashCode();
      } else {
         var1 = 0;
      }

      int var2;
      if(this.label != null) {
         var2 = this.label.hashCode();
      } else {
         var2 = 0;
      }

      if(this.value != null) {
         var3 = this.value.hashCode();
      }

      return (var2 + var1 * 31) * 31 + var3;
   }

   public String toString() {
      return "Channel{name='" + this.name + '\'' + ", label='" + this.label + '\'' + ", value='" + this.value + '\'' + '}';
   }

   public void writeToParcel(Parcel var1, int var2) {
      var1.writeString(this.name);
      var1.writeString(this.label);
      var1.writeString(this.value);
   }

   public static final class Builder {
      String label;
      String name;
      String value;

      public Channel build() {
         return new Channel(this, null);
      }

      public Channel.Builder withLabel(String var1) {
         this.label = var1;
         return this;
      }

      public Channel.Builder withName(String var1) {
         this.name = var1;
         return this;
      }

      public Channel.Builder withValue(String var1) {
         this.value = var1;
         return this;
      }
   }
}
