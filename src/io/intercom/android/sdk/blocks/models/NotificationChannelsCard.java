package io.intercom.android.sdk.blocks.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import io.intercom.android.sdk.blocks.BlockType;
import java.util.ArrayList;
import java.util.List;

public class NotificationChannelsCard implements Parcelable {
   public static final Creator CREATOR = new Creator() {
      public NotificationChannelsCard createFromParcel(Parcel var1) {
         return new NotificationChannelsCard(var1);
      }

      public NotificationChannelsCard[] newArray(int var1) {
         return new NotificationChannelsCard[var1];
      }
   };
   private final List channels;
   private final String text;
   private final String title;
   private final BlockType type;

   NotificationChannelsCard() {
      this(new NotificationChannelsCard.Builder());
   }

   protected NotificationChannelsCard(Parcel var1) {
      int var2 = var1.readInt();
      BlockType var3;
      if(var2 == -1) {
         var3 = null;
      } else {
         var3 = BlockType.values()[var2];
      }

      this.type = var3;
      this.text = var1.readString();
      this.title = var1.readString();
      this.channels = var1.createTypedArrayList(Channel.CREATOR);
   }

   private NotificationChannelsCard(NotificationChannelsCard.Builder var1) {
      BlockType var2;
      if(var1.type == null) {
         var2 = BlockType.NOTIFICATIONCHANNELSCARD;
      } else {
         var2 = BlockType.typeValueOf(var1.type);
      }

      this.type = var2;
      String var4;
      if(var1.text == null) {
         var4 = "";
      } else {
         var4 = var1.text;
      }

      this.text = var4;
      if(var1.title == null) {
         var4 = "";
      } else {
         var4 = var1.title;
      }

      this.title = var4;
      Object var3;
      if(var1.channels == null) {
         var3 = new ArrayList();
      } else {
         var3 = var1.channels;
      }

      this.channels = (List)var3;
   }

   // $FF: synthetic method
   NotificationChannelsCard(NotificationChannelsCard.Builder var1, Object var2) {
      this(var1);
   }

   public static NotificationChannelsCard fromBlock(Block var0) {
      NotificationChannelsCard var2;
      if(var0 == null) {
         var2 = new NotificationChannelsCard();
      } else {
         NotificationChannelsCard.Builder var1 = new NotificationChannelsCard.Builder();
         var1.type = var0.getType().name();
         var1.title = var0.getTitle();
         var1.text = var0.getText();
         var1.channels = var0.getChannels();
         var2 = new NotificationChannelsCard(var1);
      }

      return var2;
   }

   public int describeContents() {
      return 0;
   }

   public boolean equals(Object var1) {
      boolean var4 = true;
      boolean var3 = false;
      boolean var2;
      if(this == var1) {
         var2 = true;
      } else {
         var2 = var3;
         if(var1 != null) {
            var2 = var3;
            if(this.getClass() == var1.getClass()) {
               NotificationChannelsCard var5 = (NotificationChannelsCard)var1;
               var2 = var3;
               if(this.type == var5.type) {
                  if(this.text != null) {
                     var2 = var3;
                     if(!this.text.equals(var5.text)) {
                        return var2;
                     }
                  } else if(var5.text != null) {
                     var2 = var3;
                     return var2;
                  }

                  if(this.title != null) {
                     var2 = var3;
                     if(!this.title.equals(var5.title)) {
                        return var2;
                     }
                  } else if(var5.title != null) {
                     var2 = var3;
                     return var2;
                  }

                  if(this.channels != null) {
                     var2 = this.channels.equals(var5.channels);
                  } else {
                     var2 = var4;
                     if(var5.channels != null) {
                        var2 = false;
                     }
                  }
               }
            }
         }
      }

      return var2;
   }

   public List getChannels() {
      return this.channels;
   }

   public String getText() {
      return this.text;
   }

   public String getTitle() {
      return this.title;
   }

   public BlockType getType() {
      return this.type;
   }

   public int hashCode() {
      int var4 = 0;
      int var1;
      if(this.type != null) {
         var1 = this.type.hashCode();
      } else {
         var1 = 0;
      }

      int var2;
      if(this.text != null) {
         var2 = this.text.hashCode();
      } else {
         var2 = 0;
      }

      int var3;
      if(this.title != null) {
         var3 = this.title.hashCode();
      } else {
         var3 = 0;
      }

      if(this.channels != null) {
         var4 = this.channels.hashCode();
      }

      return (var3 + (var2 + var1 * 31) * 31) * 31 + var4;
   }

   public String toString() {
      return "NotificationChannelsCard{type=" + this.type + ", text='" + this.text + '\'' + ", title='" + this.title + '\'' + ", channels=" + this.channels + '}';
   }

   public void writeToParcel(Parcel var1, int var2) {
      if(this.type == null) {
         var2 = -1;
      } else {
         var2 = this.type.ordinal();
      }

      var1.writeInt(var2);
      var1.writeString(this.text);
      var1.writeString(this.title);
      var1.writeTypedList(this.channels);
   }

   public static final class Builder {
      List channels;
      String text;
      String title;
      String type;

      public NotificationChannelsCard build() {
         return new NotificationChannelsCard(this, null);
      }

      public NotificationChannelsCard.Builder withChannels(List var1) {
         this.channels = var1;
         return this;
      }

      public NotificationChannelsCard.Builder withText(String var1) {
         this.text = var1;
         return this;
      }

      public NotificationChannelsCard.Builder withTitle(String var1) {
         this.title = var1;
         return this;
      }
   }
}
