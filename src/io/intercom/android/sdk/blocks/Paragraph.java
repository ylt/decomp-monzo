package io.intercom.android.sdk.blocks;

import android.text.Spanned;
import android.view.View;
import android.view.ViewGroup;
import io.intercom.android.sdk.Provider;
import io.intercom.android.sdk.blocks.blockInterfaces.ParagraphBlock;
import io.intercom.android.sdk.blocks.views.ParagraphView;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.android.sdk.utilities.BlockUtils;

class Paragraph implements ParagraphBlock {
   private final Provider appConfigProvider;
   private final StyleType style;

   Paragraph(StyleType var1, Provider var2) {
      this.style = var1;
      this.appConfigProvider = var2;
   }

   public View addParagraph(Spanned var1, BlockAlignment var2, boolean var3, boolean var4, ViewGroup var5) {
      int var6 = ((AppConfig)this.appConfigProvider.get()).getBaseColor();
      ParagraphView var7 = new ParagraphView(var5.getContext(), this.style, var2, var6);
      if(StyleType.PREVIEW == this.style) {
         var7.setText(var1.toString());
      } else {
         var7.setText(var1);
      }

      BlockUtils.setLayoutMarginsAndGravity(var7, var2.getGravity(), var4);
      return var7;
   }
}
