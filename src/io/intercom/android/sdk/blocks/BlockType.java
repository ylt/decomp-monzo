package io.intercom.android.sdk.blocks;

import android.view.View;
import android.view.ViewGroup;
import io.intercom.android.sdk.blocks.models.Block;
import io.intercom.android.sdk.blocks.models.BlockAttachment;
import io.intercom.android.sdk.blocks.models.ConversationRating;
import io.intercom.android.sdk.blocks.models.Link;
import io.intercom.android.sdk.blocks.models.LinkList;
import io.intercom.android.sdk.blocks.models.NotificationChannelsCard;
import io.intercom.android.sdk.commons.utilities.HtmlCompat;
import java.util.Locale;

public enum BlockType {
   ATTACHMENTLIST("attachmentList") {
      public View generateViewFromBlockAndLayout(BlocksViewHolder var1, Block var2, ViewGroup var3, boolean var4, boolean var5) {
         return var1.getAttachmentList().addAttachmentList(var2.getAttachments(), var4, var5, var3);
      }
   },
   BUTTON("button") {
      public View generateViewFromBlockAndLayout(BlocksViewHolder var1, Block var2, ViewGroup var3, boolean var4, boolean var5) {
         return var1.getButton().addButton(var2.getText(), getUrl(var2), var2.getAlign(), var4, var5, var3);
      }
   },
   CODE("code") {
      public View generateViewFromBlockAndLayout(BlocksViewHolder var1, Block var2, ViewGroup var3, boolean var4, boolean var5) {
         return var1.getCode().addCode(HtmlCompat.fromHtml(var2.getText()), var4, var5, var3);
      }
   },
   CONVERSATIONRATING("conversationRating") {
      public View generateViewFromBlockAndLayout(BlocksViewHolder var1, Block var2, ViewGroup var3, boolean var4, boolean var5) {
         return var1.getConversationRating().addConversationRatingBlock(ConversationRating.fromBlock(var2), var4, var5, var3);
      }
   },
   FACEBOOKLIKEBUTTON("facebookLikeButton") {
      public View generateViewFromBlockAndLayout(BlocksViewHolder var1, Block var2, ViewGroup var3, boolean var4, boolean var5) {
         return var1.getFacebookButton().addFacebookButton(var2.getUrl(), var2.getAlign(), var4, var5, var3);
      }
   },
   HEADING("heading") {
      public View generateViewFromBlockAndLayout(BlocksViewHolder var1, Block var2, ViewGroup var3, boolean var4, boolean var5) {
         return var1.getHeading().addHeading(HtmlCompat.fromHtml(var2.getText()), var2.getAlign(), var4, var5, var3);
      }
   },
   IMAGE("image") {
      public View generateViewFromBlockAndLayout(BlocksViewHolder var1, Block var2, ViewGroup var3, boolean var4, boolean var5) {
         return var1.getImage().addImage(var2.getUrl(), getUrl(var2), var2.getWidth(), var2.getHeight(), var2.getAlign(), var4, var5, var3);
      }
   },
   LINK("link") {
      public View generateViewFromBlockAndLayout(BlocksViewHolder var1, Block var2, ViewGroup var3, boolean var4, boolean var5) {
         return var1.getLink().addLinkBlock(Link.fromBlock(var2), var4, var5, var3);
      }
   },
   LINKLIST("linkList") {
      public View generateViewFromBlockAndLayout(BlocksViewHolder var1, Block var2, ViewGroup var3, boolean var4, boolean var5) {
         return var1.getLinkList().addLinkListBlock(LinkList.fromBlock(var2), var4, var5, var3);
      }
   },
   LOCALIMAGE("localImage") {
      public View generateViewFromBlockAndLayout(BlocksViewHolder var1, Block var2, ViewGroup var3, boolean var4, boolean var5) {
         return var1.getLocalImage().addImage(var2.getUrl(), var2.getWidth(), var2.getHeight(), var2.getAlign(), var4, var5, var3);
      }
   },
   LOCAL_ATTACHMENT("local_attachment") {
      public View generateViewFromBlockAndLayout(BlocksViewHolder var1, Block var2, ViewGroup var3, boolean var4, boolean var5) {
         return var1.getLocalAttachment().addAttachment((BlockAttachment)var2.getAttachments().get(0), var4, var5, var3);
      }
   },
   LWR("lwr") {
      public View generateViewFromBlockAndLayout(BlocksViewHolder var1, Block var2, ViewGroup var3, boolean var4, boolean var5) {
         return var1.getLwr().addLWR(var2.getText(), var4, var5, var3);
      }
   },
   NOTIFICATIONCHANNELSCARD("notificationChannelCard") {
      public View generateViewFromBlockAndLayout(BlocksViewHolder var1, Block var2, ViewGroup var3, boolean var4, boolean var5) {
         return var1.getNotificationChannelsCard().addNotificationChannelsCardBlock(NotificationChannelsCard.fromBlock(var2), var4, var5, var3);
      }
   },
   ORDEREDLIST("orderedList") {
      public View generateViewFromBlockAndLayout(BlocksViewHolder var1, Block var2, ViewGroup var3, boolean var4, boolean var5) {
         return var1.getOrderedList().addOrderedList(var2.getItems(), var4, var5, var3);
      }
   },
   PARAGRAPH("paragraph") {
      public View generateViewFromBlockAndLayout(BlocksViewHolder var1, Block var2, ViewGroup var3, boolean var4, boolean var5) {
         return var1.getParagraph().addParagraph(HtmlCompat.fromHtml(var2.getText()), var2.getAlign(), var4, var5, var3);
      }
   },
   SUBHEADING("subheading") {
      public View generateViewFromBlockAndLayout(BlocksViewHolder var1, Block var2, ViewGroup var3, boolean var4, boolean var5) {
         return var1.getSubheading().addSubheading(HtmlCompat.fromHtml(var2.getText()), var2.getAlign(), var4, var5, var3);
      }
   },
   TWITTERFOLLOWBUTTON("twitterFollowButton") {
      public View generateViewFromBlockAndLayout(BlocksViewHolder var1, Block var2, ViewGroup var3, boolean var4, boolean var5) {
         return var1.getTwitterButton().addTwitterButton("http://twitter.com/" + var2.getUsername(), var2.getAlign(), var4, var5, var3);
      }
   },
   UNKNOWN("unknown") {
      public View generateViewFromBlockAndLayout(BlocksViewHolder var1, Block var2, ViewGroup var3, boolean var4, boolean var5) {
         if(!var2.getText().isEmpty() && var1.getParagraph() != null) {
            return PARAGRAPH.generateViewFromBlockAndLayout(var1, var2, var3, var4, var5);
         } else {
            throw new BlockTypeNotImplementedException();
         }
      }
   },
   UNORDEREDLIST("unorderedList") {
      public View generateViewFromBlockAndLayout(BlocksViewHolder var1, Block var2, ViewGroup var3, boolean var4, boolean var5) {
         return var1.getUnorderedList().addUnorderedList(var2.getItems(), var4, var5, var3);
      }
   },
   VIDEO("video") {
      public View generateViewFromBlockAndLayout(BlocksViewHolder var1, Block var2, ViewGroup var3, boolean var4, boolean var5) {
         return var1.getVideo().addVideo(var2.getEmbedUrl(), VideoProvider.videoValueOf(var2.getProvider()), var2.getId(), var4, var5, var3);
      }
   };

   private final String serializedName;

   private BlockType(String var3) {
      this.serializedName = var3;
   }

   // $FF: synthetic method
   BlockType(String var3, Object var4) {
      this(var3);
   }

   protected static String getUrl(Block var0) {
      String var1;
      if(var0.getTrackingUrl().isEmpty()) {
         var1 = var0.getLinkUrl();
      } else {
         var1 = var0.getTrackingUrl();
      }

      return var1;
   }

   public static BlockType typeValueOf(String var0) {
      BlockType var1 = UNKNOWN;

      BlockType var4;
      try {
         var4 = valueOf(var0.toUpperCase(Locale.ENGLISH));
      } catch (IllegalArgumentException var2) {
         var4 = var1;
      } catch (NullPointerException var3) {
         var4 = var1;
      }

      return var4;
   }

   abstract View generateViewFromBlockAndLayout(BlocksViewHolder var1, Block var2, ViewGroup var3, boolean var4, boolean var5);

   public String getSerializedName() {
      return this.serializedName;
   }

   public View getView(BlocksViewHolder var1, Block var2, ViewGroup var3, boolean var4, boolean var5) throws BlockTypeNotImplementedException {
      View var6;
      View var10;
      try {
         var6 = this.generateViewFromBlockAndLayout(var1, var2, var3, var4, var5);
      } catch (NullPointerException var9) {
         try {
            var10 = UNKNOWN.generateViewFromBlockAndLayout(var1, var2, var3, var4, var5);
            return var10;
         } catch (NullPointerException var7) {
            ;
         } catch (BlockTypeNotImplementedException var8) {
            ;
         }

         throw new BlockTypeNotImplementedException();
      }

      var10 = var6;
      return var10;
   }
}
