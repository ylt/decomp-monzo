package io.intercom.android.sdk.blocks;

public enum StyleType {
   ADMIN,
   CHAT_FULL,
   CONTAINER_CARD,
   NOTE,
   POST,
   PREVIEW,
   USER;
}
