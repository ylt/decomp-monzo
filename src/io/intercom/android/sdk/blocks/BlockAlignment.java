package io.intercom.android.sdk.blocks;

import java.util.Locale;

public enum BlockAlignment {
   CENTER {
      public int getGravity() {
         return 1;
      }
   },
   LEFT {
      public int getGravity() {
         return 3;
      }
   },
   RIGHT {
      public int getGravity() {
         return 5;
      }
   };

   private BlockAlignment() {
   }

   // $FF: synthetic method
   BlockAlignment(Object var3) {
      this();
   }

   public static BlockAlignment alignValueOf(String var0) {
      BlockAlignment var1 = LEFT;

      BlockAlignment var4;
      try {
         var4 = valueOf(var0.toUpperCase(Locale.ENGLISH));
      } catch (IllegalArgumentException var2) {
         var4 = var1;
      } catch (NullPointerException var3) {
         var4 = var1;
      }

      return var4;
   }

   public abstract int getGravity();
}
