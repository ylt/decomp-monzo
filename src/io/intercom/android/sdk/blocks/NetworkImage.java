package io.intercom.android.sdk.blocks;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView.ScaleType;
import io.intercom.android.sdk.Provider;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.blocks.blockInterfaces.ImageBlock;
import io.intercom.android.sdk.commons.utilities.ScreenUtils;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.android.sdk.logger.LumberMill;
import io.intercom.android.sdk.transforms.RoundedCornersTransform;
import io.intercom.android.sdk.twig.Twig;
import io.intercom.android.sdk.utilities.BlockUtils;
import io.intercom.android.sdk.utilities.ColorUtils;
import io.intercom.android.sdk.utilities.ImageUtils;
import io.intercom.android.sdk.views.ResizableImageView;
import io.intercom.com.bumptech.glide.h;
import io.intercom.com.bumptech.glide.i;
import io.intercom.com.bumptech.glide.j;
import io.intercom.com.bumptech.glide.f.e;
import io.intercom.com.bumptech.glide.f.f;
import io.intercom.com.bumptech.glide.load.a;
import io.intercom.com.bumptech.glide.load.l;
import io.intercom.com.bumptech.glide.load.engine.GlideException;
import io.intercom.com.bumptech.glide.load.resource.b.b;

class NetworkImage extends Image implements ImageBlock {
   private static final int PROGRESSBAR_DIAMETER_DP = 40;
   private final Provider appConfigProvider;
   private final ImageClickListener listener;
   private final i requestManager;
   private final Twig twig = LumberMill.getLogger();
   private final UploadingImageCache uploadingImageCache;

   NetworkImage(StyleType var1, Provider var2, UploadingImageCache var3, ImageClickListener var4, i var5) {
      super(var1);
      this.appConfigProvider = var2;
      this.uploadingImageCache = var3;
      this.listener = var4;
      this.requestManager = var5;
   }

   private void loadImageFromUrl(final String var1, final String var2, final int var3, final int var4, final Context var5, int var6, int var7, final ResizableImageView var8, final ProgressBar var9) {
      if(TextUtils.isEmpty(var1)) {
         this.hideLoadingState(var9, var8);
         var8.setImageResource(R.drawable.intercom_error);
      } else {
         h var12 = this.requestManager.a((Object)var1);
         this.setImageViewBounds(var6, var7, var8, var12);
         String var13 = this.uploadingImageCache.getLocalImagePathForRemoteUrl(var1);
         Resources var14 = var5.getResources();
         f var11 = (new f()).a((l)(new RoundedCornersTransform(var14.getDimensionPixelSize(R.dimen.intercom_image_rounded_corners)))).b(R.drawable.intercom_error).b(ImageUtils.getDiskCacheStrategy(var1));
         f var10 = var11;
         if(!TextUtils.isEmpty(var13)) {
            Options var15 = new Options();
            var15.inSampleSize = this.getSampleSize(var6, var7, var14.getDisplayMetrics());
            BitmapDrawable var16 = new BitmapDrawable(var14, BitmapFactory.decodeFile(var13, var15));
            var16.setColorFilter(ColorUtils.newGreyscaleFilter());
            var10 = var11.a((Drawable)var16).h();
         }

         var11 = var10;
         if(io.intercom.com.bumptech.glide.h.i.a(var3, var4)) {
            var11 = var10.a(var3, var4);
         }

         var12.a(var11).a((j)b.c()).a(new e() {
            public boolean onLoadFailed(GlideException var1x, Object var2x, io.intercom.com.bumptech.glide.f.a.h var3x, boolean var4x) {
               NetworkImage.this.hideLoadingState(var9, var8);
               NetworkImage.this.twig.internal("images", "FAILURE");
               return false;
            }

            public boolean onResourceReady(Drawable var1x, Object var2x, io.intercom.com.bumptech.glide.f.a.h var3x, a var4x, boolean var5x) {
               NetworkImage.this.twig.internal("images", "SUCCESS");
               NetworkImage.this.hideLoadingState(var9, var8);
               if(NetworkImage.this.getStyle() != StyleType.CHAT_FULL) {
                  var8.setOnClickListener(new OnClickListener() {
                     public void onClick(View var1x) {
                        ((InputMethodManager)var5.getSystemService("input_method")).hideSoftInputFromWindow(var1x.getWindowToken(), 0);
                        NetworkImage.this.listener.onImageClicked(var1, var2, var8, var3, var4);
                     }
                  });
               }

               return false;
            }
         }).a((ImageView)var8);
      }

   }

   public View addImage(String var1, String var2, int var3, int var4, BlockAlignment var5, boolean var6, boolean var7, ViewGroup var8) {
      Context var13 = var8.getContext();
      int var11 = ScreenUtils.dpToPx((float)var3, var13);
      int var12 = ScreenUtils.dpToPx((float)var4, var13);
      FrameLayout var14 = new FrameLayout(var13);
      BlockUtils.createLayoutParams(var14, -2, -2);
      BlockUtils.setDefaultMarginBottom(var14);
      ResizableImageView var17 = new ResizableImageView(var13);
      if(VERSION.SDK_INT >= 21) {
         var17.setTransitionName("lightbox_image");
      }

      var17.setLayoutParams(new LayoutParams(-2, -2));
      var17.setAdjustViewBounds(true);
      var17.setScaleType(ScaleType.FIT_START);
      this.setBackground(var17);
      int var10 = ((AppConfig)this.appConfigProvider.get()).getBaseColor();
      ProgressBar var15 = new ProgressBar(var13);
      int var9 = ScreenUtils.dpToPx(40.0F, var13);
      var15.setLayoutParams(new LayoutParams(var9, var9, 17));
      Drawable var16 = android.support.v4.content.a.a(var13, R.drawable.intercom_progress_wheel);
      var16.setColorFilter(var10, Mode.SRC_IN);
      var15.setIndeterminateDrawable(var16);
      var15.setIndeterminate(true);
      var14.addView(var17);
      var14.addView(var15);
      this.loadImageFromUrl(var1, var2, var3, var4, var13, var11, var12, var17, var15);
      BlockUtils.setLayoutMarginsAndGravity(var14, var5.getGravity(), var7);
      return var14;
   }

   int getSampleSize(int var1, int var2, DisplayMetrics var3) {
      int var4 = 1;
      if(var1 > var3.widthPixels || var2 > var3.heightPixels) {
         var4 = (int)Math.pow(2.0D, (double)((int)Math.ceil(Math.log((double)var3.widthPixels / (double)Math.max(var2, var1)) / Math.log(0.5D))));
      }

      return var4;
   }

   void hideLoadingState(ProgressBar var1, ImageView var2) {
      if(var1 != null) {
         var1.setVisibility(8);
         var2.setBackgroundResource(17170445);
      }

   }
}
