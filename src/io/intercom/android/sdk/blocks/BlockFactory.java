package io.intercom.android.sdk.blocks;

import io.intercom.android.sdk.blocks.logic.TextSplittingStrategy;
import io.intercom.android.sdk.blocks.models.Block;
import java.util.ArrayList;
import java.util.List;

public class BlockFactory {
   private final TextSplittingStrategy textSplittingStrategy;

   public BlockFactory(TextSplittingStrategy var1) {
      this.textSplittingStrategy = var1;
   }

   public List getBlocksForText(String var1) {
      List var5 = this.textSplittingStrategy.apply(var1);
      int var3 = var5.size();
      ArrayList var4 = new ArrayList(var3);

      for(int var2 = 0; var2 < var3; ++var2) {
         var4.add((new Block.Builder()).withType(BlockType.PARAGRAPH.name().toLowerCase()).withText((String)var5.get(var2)));
      }

      return var4;
   }
}
