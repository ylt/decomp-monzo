package io.intercom.android.sdk.blocks;

import java.util.Locale;

public enum VideoProvider {
   STREAMIO,
   UNKNOWN,
   VIMEO,
   WISTIA,
   YOUTUBE;

   public static VideoProvider videoValueOf(String var0) {
      VideoProvider var1 = UNKNOWN;

      VideoProvider var3;
      try {
         var3 = valueOf(var0.toUpperCase(Locale.ENGLISH));
      } catch (IllegalArgumentException var2) {
         var3 = var1;
      }

      return var3;
   }
}
