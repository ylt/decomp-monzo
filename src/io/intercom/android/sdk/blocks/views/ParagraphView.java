package io.intercom.android.sdk.blocks.views;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v4.content.a;
import android.support.v7.widget.aa;
import android.text.TextUtils.TruncateAt;
import android.view.ViewGroup.MarginLayoutParams;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.blocks.BlockAlignment;
import io.intercom.android.sdk.blocks.StyleType;
import io.intercom.android.sdk.commons.utilities.ScreenUtils;
import io.intercom.android.sdk.utilities.BlockUtils;
import io.intercom.android.sdk.utilities.ColorUtils;
import io.intercom.android.sdk.utilities.TrackingLinkMovementMethod;

@SuppressLint({"ViewConstructor"})
public class ParagraphView extends aa {
   private static final int DEFAULT_LINE_SPACING_DP = 2;
   private static final int POST_BOTTOM_MARGIN_DP = 24;
   private static final int POST_LINE_SPACING_DP = 4;

   public ParagraphView(Context var1, StyleType var2, BlockAlignment var3, int var4) {
      super(var1);
      BlockUtils.createLayoutParams(this, -2, -2);
      BlockUtils.setDefaultMarginBottom(this);
      this.setLineSpacing((float)ScreenUtils.dpToPx(2.0F, var1), 1.0F);
      this.setTextSize(16.0F);
      this.setTextIsSelectable(false);
      this.setEllipsize(TruncateAt.END);
      this.setScrollContainer(false);
      this.setFocusable(false);
      this.setClickable(false);
      this.setLongClickable(false);
      this.setGravity(var3.getGravity());
      MarginLayoutParams var5 = (MarginLayoutParams)this.getLayoutParams();
      switch(null.$SwitchMap$io$intercom$android$sdk$blocks$StyleType[var2.ordinal()]) {
      case 1:
         this.setMovementMethod(new TrackingLinkMovementMethod());
         this.setTextColor(a.c(var1, R.color.intercom_grey_800));
         this.setLinkTextColor(var4);
         break;
      case 2:
      case 3:
         this.setMovementMethod(new TrackingLinkMovementMethod());
         this.setTextColor(a.c(var1, R.color.intercom_grey_700));
         this.setLinkTextColor(var4);
         break;
      case 4:
         var5.bottomMargin = ScreenUtils.dpToPx(24.0F, var1);
         this.setMovementMethod(new TrackingLinkMovementMethod());
         this.setLineSpacing((float)ScreenUtils.dpToPx(4.0F, var1), 1.0F);
         this.setTextColor(a.c(var1, R.color.intercom_white));
         this.setLinkTextColor(ColorUtils.lightenColor(var4));
         break;
      case 5:
         this.setTextSize(14.0F);
         this.setTextColor(a.c(var1, R.color.intercom_grey_600));
         this.setLinkTextColor(var4);
         break;
      default:
         this.setMovementMethod(new TrackingLinkMovementMethod());
         this.setIncludeFontPadding(false);
         this.setTextColor(a.c(var1, R.color.intercom_white));
         this.setLinkTextColor(a.c(var1, R.color.intercom_white));
      }

   }
}
