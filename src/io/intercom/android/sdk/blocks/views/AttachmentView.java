package io.intercom.android.sdk.blocks.views;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.content.a;
import android.text.TextUtils.TruncateAt;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.blocks.StyleType;
import io.intercom.android.sdk.blocks.models.BlockAttachment;
import io.intercom.android.sdk.commons.utilities.ScreenUtils;
import io.intercom.android.sdk.utilities.IntentUtils;

@SuppressLint({"ViewConstructor"})
public class AttachmentView extends LinearLayout implements OnClickListener {
   static final int ATTACHMENT_ICON_WIDTH_DP = 20;
   static final int DIVIDER_MARGIN_DP = 6;
   static final int DIVIDER_WIDTH_DP = 1;
   private final String url;

   public AttachmentView(Context var1, StyleType var2, int var3, BlockAttachment var4) {
      super(var1);
      this.url = var4.getUrl();
      this.setOrientation(0);
      this.addView(this.setupIcon(var1, var2, var3));
      if(this.isUserMessage(var2)) {
         this.addView(this.setupDivider(var1));
      }

      this.addView(this.setupName(var1, var2, var3, var4.getName()));
   }

   private boolean isUserMessage(StyleType var1) {
      boolean var2;
      if(StyleType.PREVIEW != var1 && StyleType.USER != var1) {
         var2 = false;
      } else {
         var2 = true;
      }

      return var2;
   }

   private View setupDivider(Context var1) {
      View var4 = new View(var1);
      int var2 = ScreenUtils.dpToPx(1.0F, var1);
      int var3 = ScreenUtils.dpToPx(6.0F, var1);
      LayoutParams var5 = new LayoutParams(var2, -1);
      var5.setMargins(var3, 0, var3, 0);
      var4.setLayoutParams(var5);
      var4.setBackgroundResource(R.color.intercom_white);
      return var4;
   }

   private ImageView setupIcon(Context var1, StyleType var2, int var3) {
      ImageView var4 = new ImageView(var1);
      LayoutParams var5 = new LayoutParams(ScreenUtils.dpToPx(20.0F, var1), -2);
      var5.gravity = 19;
      var4.setLayoutParams(var5);
      var4.setImageResource(R.drawable.intercom_icn_attachment);
      if(!this.isUserMessage(var2)) {
         var4.setColorFilter(var3);
      }

      return var4;
   }

   private TextView setupName(Context var1, StyleType var2, int var3, String var4) {
      TextView var5 = new TextView(var1);
      LayoutParams var6 = new LayoutParams(-2, -2);
      var6.gravity = 19;
      var5.setLayoutParams(var6);
      var5.setEllipsize(TruncateAt.END);
      var5.setBackgroundResource(R.color.intercom_full_transparent_full_black);
      var5.setGravity(19);
      var5.setTextSize(15.0F);
      var5.setText(var4);
      var5.setOnClickListener(this);
      if(this.isUserMessage(var2)) {
         var5.setTextColor(a.c(var1, R.color.intercom_white));
      } else {
         var5.setTextColor(var3);
      }

      return var5;
   }

   public void onClick(View var1) {
      Uri var2 = Uri.parse(this.url);
      if(!Uri.EMPTY.equals(var2)) {
         Intent var3 = new Intent("android.intent.action.VIEW", var2);
         var3.setFlags(268435456);
         IntentUtils.safelyOpenIntent(this.getContext(), var3);
      }

   }
}
