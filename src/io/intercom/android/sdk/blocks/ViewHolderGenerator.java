package io.intercom.android.sdk.blocks;

import android.content.Context;
import android.widget.LinearLayout;
import io.intercom.android.sdk.Provider;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.api.Api;
import io.intercom.android.sdk.blocks.models.Block;
import io.intercom.android.sdk.blocks.models.BlockAttachment;
import io.intercom.android.sdk.logger.LumberMill;
import io.intercom.android.sdk.models.Attachments;
import io.intercom.android.sdk.models.Part;
import io.intercom.com.bumptech.glide.i;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ViewHolderGenerator {
   private final Api api;
   private final Provider appConfigProvider;
   private final ButtonClickListener buttonClickListener;
   private final String conversationId;
   private final ImageClickListener imageListener;
   private final i requestManager;
   private final UploadingImageCache uploadingImageCache;

   public ViewHolderGenerator(UploadingImageCache var1, Api var2, Provider var3, String var4, ImageClickListener var5, ButtonClickListener var6, i var7) {
      this.uploadingImageCache = var1;
      this.api = var2;
      this.appConfigProvider = var3;
      this.conversationId = var4;
      this.imageListener = var5;
      this.buttonClickListener = var6;
      this.requestManager = var7;
   }

   public static LinearLayout createLayoutFromBlocks(BlocksViewHolder var0, List var1, Context var2) {
      return (new Blocks(var2, LumberMill.getBlocksTwig())).createBlocks(var1, var0);
   }

   public static LinearLayout createPartsLayout(BlocksViewHolder var0, Part var1, Context var2) {
      List var3 = var1.getBlocks();
      if(!var1.getAttachments().isEmpty()) {
         ArrayList var4 = new ArrayList();
         Iterator var5 = var1.getAttachments().iterator();

         while(var5.hasNext()) {
            Attachments var6 = (Attachments)var5.next();
            var4.add((new BlockAttachment.Builder()).withName(var6.getName()).withUrl(var6.getUrl()).build());
         }

         var3.add((new Block.Builder()).withType(BlockType.ATTACHMENTLIST.name()).withAttachments(var4).build());
      }

      return (new Blocks(var2, LumberMill.getBlocksTwig())).createBlocks(var3, var0);
   }

   private BlocksViewHolder generateHolder(int var1, StyleType var2) {
      BlocksViewHolder var4 = new BlocksViewHolder();
      Button var7 = new Button(var2, this.appConfigProvider, this.buttonClickListener);
      Heading var5 = new Heading(var2, this.appConfigProvider);
      ListBlock var3 = new ListBlock(var2, this.appConfigProvider);
      Attachment var6 = new Attachment(var2, this.appConfigProvider);
      var4.setLayout(var1);
      var4.setParagraph(new Paragraph(var2, this.appConfigProvider));
      var4.setHeading(var5);
      var4.setSubheading(var5);
      var4.setCode(new Code());
      var4.setUnorderedList(var3);
      var4.setOrderedList(var3);
      var4.setImage(new NetworkImage(var2, this.appConfigProvider, this.uploadingImageCache, this.imageListener, this.requestManager));
      var4.setLocalImage(new LocalImage(var2, this.requestManager));
      var4.setButton(var7);
      var4.setAttachmentList(var6);
      var4.setLocalAttachment(var6);
      var4.setTwitterButton(var7);
      var4.setFacebookButton(var7);
      var4.setVideo(new Video(var2, this.api, this.appConfigProvider, this.requestManager));
      var4.setLink(new LinkCard(var2, this.appConfigProvider, this.conversationId, this.requestManager));
      var4.setConversationRating(new ConversationRatingCard(this.api, this.conversationId, this.appConfigProvider, this.requestManager));
      var4.setLinkList(new LinkListRenderer(this.api, this.appConfigProvider, this.conversationId));
      return var4;
   }

   public BlocksViewHolder getAdminHolder() {
      return this.generateHolder(R.layout.intercom_blocks_admin_layout, StyleType.ADMIN);
   }

   public BlocksViewHolder getChatFullHolder() {
      return this.generateHolder(R.layout.intercom_blocks_admin_layout, StyleType.CHAT_FULL);
   }

   public BlocksViewHolder getContainerCardHolder() {
      return this.generateHolder(R.layout.intercom_blocks_container_card_layout, StyleType.CONTAINER_CARD);
   }

   public BlocksViewHolder getConversationRatingHolder() {
      return this.generateHolder(R.layout.intercom_blocks_container_layout, StyleType.CHAT_FULL);
   }

   public BlocksViewHolder getLinkHolder() {
      return this.generateHolder(R.layout.intercom_blocks_container_layout, StyleType.CHAT_FULL);
   }

   public BlocksViewHolder getLinkListHolder() {
      return this.generateHolder(R.layout.intercom_blocks_container_card_layout, StyleType.CHAT_FULL);
   }

   public BlocksViewHolder getNoteHolder() {
      return this.generateHolder(R.layout.intercom_blocks_note_layout, StyleType.NOTE);
   }

   public BlocksViewHolder getPostHolder() {
      return this.generateHolder(R.layout.intercom_blocks_container_layout, StyleType.POST);
   }

   public BlocksViewHolder getPreviewHolder() {
      return this.generateHolder(R.layout.intercom_blocks_user_layout, StyleType.PREVIEW);
   }

   public BlocksViewHolder getUserHolder() {
      return this.generateHolder(R.layout.intercom_blocks_user_layout, StyleType.USER);
   }
}
