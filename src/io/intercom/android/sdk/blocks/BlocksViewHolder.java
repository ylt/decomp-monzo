package io.intercom.android.sdk.blocks;

import io.intercom.android.sdk.blocks.blockInterfaces.AttachmentListBlock;
import io.intercom.android.sdk.blocks.blockInterfaces.ButtonBlock;
import io.intercom.android.sdk.blocks.blockInterfaces.CodeBlock;
import io.intercom.android.sdk.blocks.blockInterfaces.ConversationRatingBlock;
import io.intercom.android.sdk.blocks.blockInterfaces.FacebookBlock;
import io.intercom.android.sdk.blocks.blockInterfaces.HeadingBlock;
import io.intercom.android.sdk.blocks.blockInterfaces.ImageBlock;
import io.intercom.android.sdk.blocks.blockInterfaces.LightWeightReplyBlock;
import io.intercom.android.sdk.blocks.blockInterfaces.LinkBlock;
import io.intercom.android.sdk.blocks.blockInterfaces.LinkListBlock;
import io.intercom.android.sdk.blocks.blockInterfaces.LocalAttachmentBlock;
import io.intercom.android.sdk.blocks.blockInterfaces.LocalImageBlock;
import io.intercom.android.sdk.blocks.blockInterfaces.NotificationChannelsCardBlock;
import io.intercom.android.sdk.blocks.blockInterfaces.OrderedListBlock;
import io.intercom.android.sdk.blocks.blockInterfaces.ParagraphBlock;
import io.intercom.android.sdk.blocks.blockInterfaces.SubheadingBlock;
import io.intercom.android.sdk.blocks.blockInterfaces.TwitterBlock;
import io.intercom.android.sdk.blocks.blockInterfaces.UnorderedListBlock;
import io.intercom.android.sdk.blocks.blockInterfaces.VideoBlock;

public class BlocksViewHolder {
   private AttachmentListBlock attachmentList;
   private ButtonBlock button;
   private CodeBlock code;
   private ConversationRatingBlock conversationRating;
   private FacebookBlock facebookButton;
   private HeadingBlock heading;
   private ImageBlock image;
   private int layout;
   private LinkBlock link;
   private LinkListBlock linkList;
   private LocalAttachmentBlock localAttachment;
   private LocalImageBlock localImage;
   private LightWeightReplyBlock lwr;
   private NotificationChannelsCardBlock notificationChannelsCard;
   private OrderedListBlock orderedList;
   private ParagraphBlock paragraph;
   private SubheadingBlock subheading;
   private TwitterBlock twitterButton;
   private UnorderedListBlock unorderedList;
   private VideoBlock video;

   public AttachmentListBlock getAttachmentList() {
      return this.attachmentList;
   }

   public ButtonBlock getButton() {
      return this.button;
   }

   public CodeBlock getCode() {
      return this.code;
   }

   public ConversationRatingBlock getConversationRating() {
      return this.conversationRating;
   }

   public FacebookBlock getFacebookButton() {
      return this.facebookButton;
   }

   public HeadingBlock getHeading() {
      return this.heading;
   }

   public ImageBlock getImage() {
      return this.image;
   }

   public int getLayout() {
      return this.layout;
   }

   public LinkBlock getLink() {
      return this.link;
   }

   public LinkListBlock getLinkList() {
      return this.linkList;
   }

   public LocalAttachmentBlock getLocalAttachment() {
      return this.localAttachment;
   }

   public LocalImageBlock getLocalImage() {
      return this.localImage;
   }

   public LightWeightReplyBlock getLwr() {
      return this.lwr;
   }

   public NotificationChannelsCardBlock getNotificationChannelsCard() {
      return this.notificationChannelsCard;
   }

   public OrderedListBlock getOrderedList() {
      return this.orderedList;
   }

   public ParagraphBlock getParagraph() {
      return this.paragraph;
   }

   public SubheadingBlock getSubheading() {
      return this.subheading;
   }

   public TwitterBlock getTwitterButton() {
      return this.twitterButton;
   }

   public UnorderedListBlock getUnorderedList() {
      return this.unorderedList;
   }

   public VideoBlock getVideo() {
      return this.video;
   }

   public void setAttachmentList(AttachmentListBlock var1) {
      this.attachmentList = var1;
   }

   public void setButton(ButtonBlock var1) {
      this.button = var1;
   }

   public void setCode(CodeBlock var1) {
      this.code = var1;
   }

   public void setConversationRating(ConversationRatingBlock var1) {
      this.conversationRating = var1;
   }

   public void setFacebookButton(FacebookBlock var1) {
      this.facebookButton = var1;
   }

   public void setHeading(HeadingBlock var1) {
      this.heading = var1;
   }

   public void setImage(ImageBlock var1) {
      this.image = var1;
   }

   public void setLayout(int var1) {
      this.layout = var1;
   }

   public void setLink(LinkBlock var1) {
      this.link = var1;
   }

   public void setLinkList(LinkListBlock var1) {
      this.linkList = var1;
   }

   public void setLocalAttachment(LocalAttachmentBlock var1) {
      this.localAttachment = var1;
   }

   public void setLocalImage(LocalImageBlock var1) {
      this.localImage = var1;
   }

   public void setLwr(LightWeightReplyBlock var1) {
      this.lwr = var1;
   }

   public void setNotificationChannelsCard(NotificationChannelsCardBlock var1) {
      this.notificationChannelsCard = var1;
   }

   public void setOrderedList(OrderedListBlock var1) {
      this.orderedList = var1;
   }

   public void setParagraph(ParagraphBlock var1) {
      this.paragraph = var1;
   }

   public void setSubheading(SubheadingBlock var1) {
      this.subheading = var1;
   }

   public void setTwitterButton(TwitterBlock var1) {
      this.twitterButton = var1;
   }

   public void setUnorderedList(UnorderedListBlock var1) {
      this.unorderedList = var1;
   }

   public void setVideo(VideoBlock var1) {
      this.video = var1;
   }
}
