package io.intercom.android.sdk.blocks;

import android.net.Uri;
import com.intercom.input.gallery.c;
import io.intercom.android.sdk.models.Upload;
import java.util.HashMap;
import java.util.Map;

public class UploadingImageCache {
   private final Map localImagePaths;

   public UploadingImageCache() {
      this(new HashMap());
   }

   UploadingImageCache(Map var1) {
      this.localImagePaths = var1;
   }

   public String getLocalImagePathForRemoteUrl(String var1) {
      return (String)this.localImagePaths.get(var1);
   }

   public void put(Upload var1, c var2) {
      Uri var3 = Uri.parse(var1.getUploadDestination()).buildUpon().path(var1.getKey()).build();
      this.localImagePaths.put(var3.toString(), var2.a());
   }
}
