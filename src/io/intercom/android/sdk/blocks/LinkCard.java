package io.intercom.android.sdk.blocks;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build.VERSION;
import android.support.v4.app.b;
import android.support.v4.content.a;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import io.intercom.android.sdk.Provider;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.activities.IntercomArticleActivity;
import io.intercom.android.sdk.blocks.blockInterfaces.LinkBlock;
import io.intercom.android.sdk.blocks.models.Link;
import io.intercom.android.sdk.blocks.views.ParagraphView;
import io.intercom.android.sdk.commons.utilities.HtmlCompat;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.android.sdk.models.Avatar;
import io.intercom.android.sdk.utilities.AvatarUtils;
import io.intercom.android.sdk.utilities.BackgroundUtils;
import io.intercom.android.sdk.utilities.BlockUtils;
import io.intercom.com.bumptech.glide.i;

class LinkCard implements LinkBlock {
   private final Provider appConfigProvider;
   private final String conversationId;
   private final i requestManager;
   private final StyleType style;

   LinkCard(StyleType var1, Provider var2, String var3, i var4) {
      this.style = var1;
      this.appConfigProvider = var2;
      this.conversationId = var3;
      this.requestManager = var4;
   }

   @SuppressLint({"SetTextI18n"})
   private void addAuthorDetails(Link var1, ImageView var2, TextView var3, String var4) {
      var2.setVisibility(0);
      var3.setVisibility(0);
      SpannableString var6 = new SpannableString(var4);
      var6.setSpan(new StyleSpan(1), 0, var4.length(), 33);
      var3.setText("Written by " + var6);
      int var5 = var2.getResources().getDimensionPixelSize(R.dimen.intercom_avatar_size);
      AvatarUtils.createAvatar(Avatar.create(var1.getAuthor().getAvatar(), ""), var2, var5, (AppConfig)this.appConfigProvider.get(), this.requestManager);
   }

   private View createLinkBlock(final Link var1, ViewGroup var2, boolean var3, boolean var4) {
      int var5 = ((AppConfig)this.appConfigProvider.get()).getBaseColor();
      final Context var6 = var2.getContext();
      final LinearLayout var10 = (LinearLayout)LayoutInflater.from(var6).inflate(R.layout.intercom_link_block, var2, false);
      TextView var7 = (TextView)var10.findViewById(R.id.title);
      var7.setTextColor(var5);
      var7.setText(var1.getTitle());
      this.setUpDescription(var1, (TextView)var10.findViewById(R.id.description));
      ImageView var9 = (ImageView)var10.findViewById(R.id.avatar);
      TextView var8 = (TextView)var10.findViewById(R.id.author);
      String var11 = var1.getAuthor().getFirstName();
      if(TextUtils.isEmpty(var11)) {
         var9.setVisibility(8);
         var8.setVisibility(8);
      } else {
         this.addAuthorDetails(var1, var9, var8, var11);
      }

      if(!var3) {
         if("educate.article".equals(var1.getLinkType())) {
            var10.setOnClickListener(new OnClickListener() {
               public void onClick(View var1x) {
                  if(var6 instanceof Activity) {
                     Activity var2 = (Activity)var6;
                     Intent var3 = IntercomArticleActivity.buildIntent(var2, var1.getArticleId(), LinkCard.this.conversationId);
                     if(VERSION.SDK_INT >= 16) {
                        var2.startActivity(var3, b.a(var2, var10, "link_background").a());
                     } else {
                        var2.startActivity(var3);
                     }
                  }

               }
            });
         }

         BackgroundUtils.setBackground(var10, a.a(var6, R.drawable.intercom_conversation_card_background));
         BlockUtils.setMarginBottom(var10, 8);
         var5 = var6.getResources().getDimensionPixelSize(R.dimen.intercom_link_padding);
         var10.setPadding(var5, var5, var5, var5);
         BlockUtils.setLayoutMarginsAndGravity(var10, 3, var4);
      }

      return var10;
   }

   private boolean isOnlyBlock(boolean var1, boolean var2) {
      if(var1 && var2) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   private void setUpDescription(Link var1, TextView var2) {
      String var3 = var1.getDescription();
      if(TextUtils.isEmpty(var3)) {
         var2.setVisibility(8);
      } else {
         var2.setVisibility(0);
         var2.setText(var3);
      }

   }

   private boolean shouldDisplayLink() {
      boolean var1;
      if(!StyleType.ADMIN.equals(this.style) && !StyleType.CHAT_FULL.equals(this.style)) {
         var1 = false;
      } else {
         var1 = true;
      }

      return var1;
   }

   public View addLinkBlock(Link var1, boolean var2, boolean var3, ViewGroup var4) {
      var2 = this.isOnlyBlock(var2, var3);
      Object var6;
      if(this.shouldDisplayLink()) {
         var6 = this.createLinkBlock(var1, var4, var2, var3);
      } else {
         int var5 = ((AppConfig)this.appConfigProvider.get()).getBaseColor();
         ParagraphView var7 = new ParagraphView(var4.getContext(), StyleType.ADMIN, BlockAlignment.LEFT, var5);
         var7.setText(HtmlCompat.fromHtml(var1.getText()));
         var6 = var7;
      }

      return (View)var6;
   }
}
