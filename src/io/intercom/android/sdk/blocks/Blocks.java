package io.intercom.android.sdk.blocks;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import io.intercom.android.sdk.blocks.models.Block;
import io.intercom.android.sdk.twig.Twig;
import java.util.List;

public class Blocks {
   private final LayoutInflater inflater;
   private final Twig twig;

   public Blocks(Context var1, Twig var2) {
      this.inflater = LayoutInflater.from(var1);
      this.twig = var2;
   }

   public LinearLayout createBlocks(List var1, BlocksViewHolder var2) {
      LinearLayout var6 = (LinearLayout)this.inflater.inflate(var2.getLayout(), (ViewGroup)null);
      if(var1 != null) {
         for(int var3 = 0; var3 < var1.size(); ++var3) {
            Block var8 = (Block)var1.get(var3);
            boolean var4;
            if(var3 == 0) {
               var4 = true;
            } else {
               var4 = false;
            }

            boolean var5;
            if(var3 == var1.size() - 1) {
               var5 = true;
            } else {
               var5 = false;
            }

            try {
               var6.addView(var8.getType().getView(var2, var8, var6, var4, var5));
            } catch (BlockTypeNotImplementedException var9) {
               this.twig.e(var9, "Error creating view for block with type %s ", new Object[]{var8.getType()});
            }
         }
      }

      return var6;
   }
}
