package io.intercom.android.sdk.blocks;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff.Mode;
import android.support.v7.app.d;
import android.support.v7.app.d.a;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.animation.OvershootInterpolator;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;
import io.intercom.android.sdk.Provider;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.api.Api;
import io.intercom.android.sdk.blocks.blockInterfaces.ConversationRatingBlock;
import io.intercom.android.sdk.blocks.models.ConversationRating;
import io.intercom.android.sdk.blocks.models.ConversationRatingOption;
import io.intercom.android.sdk.commons.utilities.ScreenUtils;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.android.sdk.imageloader.LongTermImageLoader;
import io.intercom.android.sdk.utilities.BackgroundUtils;
import io.intercom.android.sdk.utilities.ColorUtils;
import io.intercom.android.sdk.utilities.FontUtils;
import io.intercom.android.sdk.views.ButtonSelector;
import io.intercom.com.bumptech.glide.i;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ConversationRatingCard implements ConversationRatingBlock {
   private static final long ANIMATION_DURATION_MS = 200L;
   private static final float DESELECTED_RATING_SCALE = 1.0F;
   private static final float DIALOG_HORIZONTAL_MARGIN = 16.0F;
   private static final float RATING_HORIZONTAL_PADDING = 17.0F;
   private static final float RATING_VERTICAL_PADDING = 14.0F;
   private static final float SELECTED_RATING_SCALE = 1.2F;
   private final Api api;
   private final Provider appConfigProvider;
   private final String conversationId;
   private ConversationRating conversationRating;
   private final ColorFilter deselectedFilter;
   private LongTermImageLoader longTermImageLoader;
   private final OnClickListener ratingClickListener = new OnClickListener() {
      public void onClick(View var1) {
         int var2 = ConversationRatingCard.this.ratingViews.indexOf(var1);
         if(var2 != -1 && ConversationRatingCard.this.ratingViews.size() == ConversationRatingCard.this.conversationRating.getOptions().size()) {
            ConversationRatingCard.this.rateConversation(ConversationRatingCard.this.conversationRating, (ConversationRatingOption)ConversationRatingCard.this.conversationRating.getOptions().get(var2));
            ConversationRatingCard.this.updateSelectedRating();
         }

      }
   };
   private final List ratingViews;
   private final i requestManager;
   private LinearLayout rootLayout;

   ConversationRatingCard(Api var1, String var2, Provider var3, i var4) {
      this.api = var1;
      this.conversationId = var2;
      this.appConfigProvider = var3;
      this.requestManager = var4;
      this.ratingViews = new ArrayList();
      this.deselectedFilter = ColorUtils.newGreyscaleFilter();
   }

   private View createConversationRatingBlock(ConversationRating var1, ViewGroup var2) {
      Context var4 = var2.getContext();
      this.ratingViews.clear();
      this.conversationRating = var1;
      this.longTermImageLoader = LongTermImageLoader.newInstance(var4);
      this.rootLayout = (LinearLayout)LayoutInflater.from(var4).inflate(R.layout.intercom_conversation_rating_block, var2, false);
      this.updateViewVisibility();
      android.widget.Button var5 = (android.widget.Button)this.rootLayout.findViewById(R.id.intercom_rating_tell_us_more_button);
      int var3 = ((AppConfig)this.appConfigProvider.get()).getBaseColor();
      BackgroundUtils.setBackground(var5, new ButtonSelector(var4, R.drawable.intercom_border, var3));
      FontUtils.setRobotoMediumTypeface(var5);
      var5.setOnClickListener(new OnClickListener() {
         public void onClick(View var1) {
            ConversationRatingCard.this.showRemarkDialog(var1.getContext());
         }
      });
      this.setupRatingsLayout();
      this.updateSelectedRating();
      return this.rootLayout;
   }

   private void deselectView(ImageView var1) {
      var1.animate().setInterpolator(new OvershootInterpolator(0.6F)).scaleX(1.0F).scaleY(1.0F).setDuration(200L).start();
      var1.setColorFilter(this.deselectedFilter);
   }

   private ConversationRatingOption findSelectedOption() {
      Iterator var2 = this.conversationRating.getOptions().iterator();

      ConversationRatingOption var1;
      do {
         if(!var2.hasNext()) {
            var1 = null;
            break;
         }

         var1 = (ConversationRatingOption)var2.next();
      } while(!var1.getIndex().equals(this.conversationRating.getRatingIndex()));

      return var1;
   }

   private static String imageUrlForUnicode(String var0) {
      return "https://js.intercomcdn.com/images/stickers/" + var0 + ".png";
   }

   private void selectView(float var1, ImageView var2) {
      var2.animate().setInterpolator(new OvershootInterpolator(2.0F)).scaleX(var1).scaleY(var1).setDuration(200L).start();
      var2.clearColorFilter();
   }

   private void setupRatingsLayout() {
      LinearLayout var7 = (LinearLayout)this.rootLayout.findViewById(R.id.intercom_rating_options_layout);
      Context var6 = this.rootLayout.getContext();
      int var3 = ScreenUtils.dpToPx(14.0F, var6);
      int var2 = ScreenUtils.dpToPx(17.0F, var6);

      for(int var1 = 0; var1 < this.conversationRating.getOptions().size(); ++var1) {
         ConversationRatingOption var10 = (ConversationRatingOption)this.conversationRating.getOptions().get(var1);
         FrameLayout var8 = new FrameLayout(var6);
         var8.setClipChildren(false);
         var8.setClipToPadding(false);
         var8.setLayoutParams(new LayoutParams(0, -1, 1.0F));
         final ImageView var5 = new ImageView(var6);
         int var4 = var6.getResources().getDimensionPixelSize(R.dimen.intercom_conversation_rating_size);
         android.widget.FrameLayout.LayoutParams var9 = new android.widget.FrameLayout.LayoutParams(var4, var4, 17);
         var9.setMargins(var2, var3, var2, var3);
         var5.setLayoutParams(var9);
         var5.setLongClickable(false);
         String var11 = imageUrlForUnicode(var10.getUnicode());
         this.longTermImageLoader.loadImage(var11, new LongTermImageLoader.OnImageReadyListener() {
            public void onImageReady(Bitmap var1) {
               var5.setImageBitmap(var1);
            }
         }, this.requestManager);
         var5.setOnClickListener(this.ratingClickListener);
         var8.addView(var5);
         var7.addView(var8);
         this.ratingViews.add(var5);
      }

   }

   private void showRemarkDialog(Context var1) {
      a var5 = new a(var1);
      var5.a(R.string.intercom_tell_us_more);
      LinearLayout var4 = new LinearLayout(var1);
      var4.setOrientation(1);
      LayoutParams var3 = new LayoutParams(-1, -2);
      int var2 = ScreenUtils.dpToPx(16.0F, var1);
      var3.setMargins(var2, 0, var2, 0);
      final EditText var6 = new EditText(var1);
      var6.getBackground().mutate().setColorFilter(((AppConfig)this.appConfigProvider.get()).getBaseColor(), Mode.SRC_ATOP);
      var6.setLayoutParams(var3);
      var4.addView(var6);
      var5.b(var4);
      var5.a(17039370, new android.content.DialogInterface.OnClickListener() {
         public void onClick(DialogInterface var1, int var2) {
            String var3 = var6.getText().toString();
            ConversationRatingCard.this.addRemarkToConversation(ConversationRatingCard.this.conversationRating, var3);
         }
      });
      var5.b(17039360, new android.content.DialogInterface.OnClickListener() {
         public void onClick(DialogInterface var1, int var2) {
            var1.cancel();
         }
      });
      final d var7 = var5.b();
      var7.show();
      var6.setOnFocusChangeListener(new OnFocusChangeListener() {
         public void onFocusChange(View var1, boolean var2) {
            if(var2 && var7.getWindow() != null) {
               var7.getWindow().setSoftInputMode(5);
            }

         }
      });
      var7.a(-1).setTextColor(((AppConfig)this.appConfigProvider.get()).getBaseColor());
      var7.a(-2).setTextColor(((AppConfig)this.appConfigProvider.get()).getBaseColor());
   }

   private void updateSelectedRating() {
      if(this.conversationRating.getOptions().size() == this.ratingViews.size()) {
         for(int var1 = 0; var1 < this.conversationRating.getOptions().size(); ++var1) {
            ConversationRatingOption var3 = (ConversationRatingOption)this.conversationRating.getOptions().get(var1);
            ImageView var2 = (ImageView)this.ratingViews.get(var1);
            if(this.conversationRating.getRatingIndex().intValue() == -1) {
               this.selectView(1.0F, var2);
            } else if(this.conversationRating.getRatingIndex().equals(var3.getIndex())) {
               this.selectView(1.2F, var2);
            } else {
               this.deselectView(var2);
            }
         }
      }

   }

   private void updateViewVisibility() {
      byte var4 = 0;
      if(this.rootLayout != null) {
         boolean var2;
         if(this.conversationRating.getRatingIndex().intValue() != -1) {
            var2 = true;
         } else {
            var2 = false;
         }

         boolean var1;
         if(!TextUtils.isEmpty(this.conversationRating.getRemark())) {
            var1 = true;
         } else {
            var1 = false;
         }

         TextView var5 = (TextView)this.rootLayout.findViewById(R.id.rate_your_conversation_text_view);
         byte var3;
         if(var1) {
            var3 = 8;
         } else {
            var3 = 0;
         }

         var5.setVisibility(var3);
         LinearLayout var9 = (LinearLayout)this.rootLayout.findViewById(R.id.intercom_rating_options_layout);
         if(var1) {
            var3 = 8;
         } else {
            var3 = 0;
         }

         var9.setVisibility(var3);
         android.widget.Button var10 = (android.widget.Button)this.rootLayout.findViewById(R.id.intercom_rating_tell_us_more_button);
         byte var8;
         if(var2 && !var1) {
            var8 = 0;
         } else {
            var8 = 8;
         }

         var10.setVisibility(var8);
         var9 = (LinearLayout)this.rootLayout.findViewById(R.id.intercom_you_rated_layout);
         byte var7;
         if(var1) {
            var7 = var4;
         } else {
            var7 = 8;
         }

         var9.setVisibility(var7);
         ConversationRatingOption var6 = this.findSelectedOption();
         if(var6 != null) {
            final ImageView var11 = (ImageView)this.rootLayout.findViewById(R.id.intercom_you_rated_image_view);
            String var12 = imageUrlForUnicode(var6.getUnicode());
            this.longTermImageLoader.loadImage(var12, new LongTermImageLoader.OnImageReadyListener() {
               public void onImageReady(Bitmap var1) {
                  var11.setImageBitmap(var1);
               }
            }, this.requestManager);
         }
      }

   }

   public View addConversationRatingBlock(ConversationRating var1, boolean var2, boolean var3, ViewGroup var4) {
      return this.createConversationRatingBlock(var1, var4);
   }

   void addRemarkToConversation(ConversationRating var1, String var2) {
      if(TextUtils.isEmpty(var1.getRemark())) {
         var1.setRemark(var2);
         this.api.addConversationRatingRemark(this.conversationId, var2);
         this.updateViewVisibility();
      }

   }

   void rateConversation(ConversationRating var1, ConversationRatingOption var2) {
      if(var1.getRatingIndex().intValue() == -1) {
         var1.setRatingIndex(var2.getIndex().intValue());
         this.api.rateConversation(this.conversationId, var2.getIndex().intValue());
         this.updateViewVisibility();
      }

   }
}
