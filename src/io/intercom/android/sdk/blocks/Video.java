package io.intercom.android.sdk.blocks;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout.LayoutParams;
import io.intercom.android.sdk.Provider;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.api.Api;
import io.intercom.android.sdk.blocks.blockInterfaces.VideoBlock;
import io.intercom.android.sdk.commons.utilities.ScreenUtils;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.android.sdk.logger.LumberMill;
import io.intercom.android.sdk.twig.Twig;
import io.intercom.android.sdk.utilities.BackgroundUtils;
import io.intercom.android.sdk.utilities.BlockUtils;
import io.intercom.android.sdk.utilities.ColorUtils;
import io.intercom.android.sdk.utilities.IntentUtils;
import io.intercom.com.bumptech.glide.i;
import io.intercom.com.bumptech.glide.j;
import io.intercom.com.bumptech.glide.f.e;
import io.intercom.com.bumptech.glide.f.f;
import io.intercom.com.bumptech.glide.f.a.h;
import io.intercom.com.bumptech.glide.load.a;
import io.intercom.com.bumptech.glide.load.engine.GlideException;
import io.intercom.com.bumptech.glide.load.resource.b.b;
import io.intercom.okhttp3.Call;
import io.intercom.okhttp3.Callback;
import io.intercom.okhttp3.Response;
import java.io.IOException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class Video implements VideoBlock {
   private static final int PLAY_BUTTON_DIAMETER_DP = 48;
   private final Api api;
   private final Provider appConfigProvider;
   private final i requestManager;
   private final StyleType style;
   private final Twig twig = LumberMill.getLogger();

   Video(StyleType var1, Api var2, Provider var3, i var4) {
      this.style = var1;
      this.api = var2;
      this.appConfigProvider = var3;
      this.requestManager = var4;
   }

   private RelativeLayout getBaseLayout(Context var1) {
      RelativeLayout var2 = new RelativeLayout(var1);
      BlockUtils.createLayoutParams(var2, -2, -2);
      BlockUtils.setDefaultMarginBottom(var2);
      return var2;
   }

   private ImageView getPlayButtonView(Context var1) {
      int var2 = ScreenUtils.dpToPx(48.0F, var1);
      LayoutParams var3 = new LayoutParams(var2, var2);
      var3.addRule(13);
      ImageView var4 = new ImageView(var1);
      var4.setLayoutParams(var3);
      var4.setScaleType(ScaleType.CENTER);
      var4.setImageResource(R.drawable.intercom_play_arrow);
      var4.setBackgroundResource(R.drawable.intercom_solid_circle);
      var4.setVisibility(8);
      return var4;
   }

   private ImageView getVideoImageView(Context var1) {
      LayoutParams var2 = new LayoutParams(-2, -2);
      ImageView var3 = new ImageView(var1);
      var3.setLayoutParams(var2);
      var3.setAdjustViewBounds(true);
      var3.setScaleType(ScaleType.FIT_START);
      return var3;
   }

   public View addVideo(String var1, VideoProvider var2, String var3, boolean var4, boolean var5, ViewGroup var6) {
      Context var9 = var6.getContext();
      RelativeLayout var10 = this.getBaseLayout(var9);
      ImageView var11 = this.getVideoImageView(var9);
      ImageView var12 = this.getPlayButtonView(var9);
      var10.addView(var11);
      var10.addView(var12);
      int var8 = ((AppConfig)this.appConfigProvider.get()).getBaseColor();
      int var7 = var8;
      if(this.style == StyleType.POST) {
         var7 = ColorUtils.lightenColor(var8);
      }

      var12.setColorFilter(var7, Mode.SRC_ATOP);
      this.fetchThumbnail(var2, var3, var11, var12);
      BlockUtils.setLayoutMarginsAndGravity(var10, 3, var5);
      return var10;
   }

   void createThumbnail(final ImageView var1, final String var2, String var3, final ImageView var4) {
      final Context var5 = var1.getContext();
      this.requestManager.a((Object)var3).a((new f()).a(R.drawable.intercom_video_thumbnail_fallback).b(R.drawable.intercom_video_thumbnail_fallback)).a((j)b.c()).a(new e() {
         public boolean onLoadFailed(GlideException var1x, Object var2, h var3, boolean var4x) {
            var4.setVisibility(8);
            return false;
         }

         public boolean onResourceReady(Drawable var1x, Object var2, h var3, a var4x, boolean var5x) {
            int var6 = android.support.v4.content.a.c(var5, R.color.intercom_semi_transparent);
            var1.setColorFilter(var6, Mode.DARKEN);
            var4.setVisibility(0);
            return false;
         }
      }).a(var1);
      if(this.style != StyleType.CHAT_FULL) {
         var1.setOnClickListener(new OnClickListener() {
            public void onClick(View var1) {
               Intent var2x = new Intent("android.intent.action.VIEW", Uri.parse(var2));
               var2x.setFlags(268435456);
               IntentUtils.safelyOpenIntent(var5, var2x);
            }
         });
      }

   }

   void fetchThumbnail(VideoProvider var1, final String var2, final ImageView var3, final ImageView var4) {
      switch(null.$SwitchMap$io$intercom$android$blocks$VideoProvider[var1.ordinal()]) {
      case 1:
         this.api.getVideo("http://fast.wistia.com/oembed?url=http://home.wistia.com/medias/" + var2, new Callback() {
            public void onFailure(Call var1, IOException var2x) {
               Video.this.setFailedImage(var3);
            }

            public void onResponse(Call var1, Response var2x) throws IOException {
               if(var2x.isSuccessful()) {
                  final String var5 = "http://fast.wistia.net/embed/iframe/" + var2;
                  JSONObject var14 = new JSONObject();

                  label68: {
                     JSONObject var17;
                     try {
                        var17 = new JSONObject(var2x.body().string());
                     } catch (JSONException var11) {
                        var11.printStackTrace();
                        break label68;
                     } catch (IOException var12) {
                        Twig var4x = Video.this.twig;
                        StringBuilder var6 = new StringBuilder();
                        var4x.internal("ErrorObject", var6.append("Couldn't read response body: ").append(var12.getMessage()).toString());
                        break label68;
                     } finally {
                        var2x.body().close();
                     }

                     var14 = var17;
                  }

                  String var15 = var14.optString("thumbnail_url");
                  int var3x = var15.indexOf("?image_crop_resized");
                  final String var16 = var15;
                  if(var3x > 0) {
                     var16 = var15.substring(0, var3x);
                  }

                  var3.post(new Runnable() {
                     public void run() {
                        Video.this.createThumbnail(var3, var5, var16, var4);
                     }
                  });
               } else {
                  Video.this.setFailedImage(var3);
               }

            }
         });
         break;
      case 2:
         this.createThumbnail(var3, "http://www.youtube.com/watch?v=" + var2, "http://img.youtube.com/vi/" + var2 + "/default.jpg", var4);
         break;
      case 3:
         String var5 = "http://vimeo.com/api/v2/video/" + var2 + ".json";
         this.api.getVideo(var5, new Callback() {
            public void onFailure(Call var1, IOException var2x) {
               Video.this.setFailedImage(var3);
            }

            public void onResponse(Call var1, Response var2x) throws IOException {
               if(var2x.isSuccessful()) {
                  JSONObject var13 = new JSONObject();
                  boolean var9 = false;

                  label65: {
                     JSONObject var15;
                     label64: {
                        label63: {
                           try {
                              var9 = true;
                              JSONArray var3x = new JSONArray(var2x.body().string());
                              var15 = var3x.optJSONObject(0);
                              var9 = false;
                              break label64;
                           } catch (JSONException var10) {
                              var10.printStackTrace();
                              var9 = false;
                              break label63;
                           } catch (IOException var11) {
                              Twig var5 = Video.this.twig;
                              StringBuilder var4x = new StringBuilder();
                              var5.internal(var4x.append("Couldn't read response body: ").append(var11.getMessage()).toString());
                              var9 = false;
                           } finally {
                              if(var9) {
                                 var2x.body().close();
                              }
                           }

                           var2x.body().close();
                           break label65;
                        }

                        var2x.body().close();
                        break label65;
                     }

                     var13 = var15;
                     var2x.body().close();
                  }

                  final String var14 = var13.optString("thumbnail_large");
                  var3.post(new Runnable() {
                     public void run() {
                        Video.this.createThumbnail(var3, "http://player.vimeo.com/video/" + var2, var14, var4);
                     }
                  });
               }

            }
         });
      }

   }

   void setFailedImage(final ImageView var1) {
      var1.post(new Runnable() {
         public void run() {
            BackgroundUtils.setBackground(var1, android.support.v4.content.a.a(var1.getContext(), R.drawable.intercom_video_thumbnail_fallback));
         }
      });
   }
}
