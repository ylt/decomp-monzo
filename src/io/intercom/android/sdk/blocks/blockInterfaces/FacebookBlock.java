package io.intercom.android.sdk.blocks.blockInterfaces;

import android.view.View;
import android.view.ViewGroup;
import io.intercom.android.sdk.blocks.BlockAlignment;

public interface FacebookBlock {
   View addFacebookButton(String var1, BlockAlignment var2, boolean var3, boolean var4, ViewGroup var5);
}
