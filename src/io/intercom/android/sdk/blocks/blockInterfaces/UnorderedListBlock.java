package io.intercom.android.sdk.blocks.blockInterfaces;

import android.view.View;
import android.view.ViewGroup;
import java.util.List;

public interface UnorderedListBlock {
   View addUnorderedList(List var1, boolean var2, boolean var3, ViewGroup var4);
}
