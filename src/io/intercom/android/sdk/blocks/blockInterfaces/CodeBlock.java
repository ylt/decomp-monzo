package io.intercom.android.sdk.blocks.blockInterfaces;

import android.text.Spanned;
import android.view.View;
import android.view.ViewGroup;

public interface CodeBlock {
   View addCode(Spanned var1, boolean var2, boolean var3, ViewGroup var4);
}
