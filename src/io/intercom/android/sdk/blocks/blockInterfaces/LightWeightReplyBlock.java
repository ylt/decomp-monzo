package io.intercom.android.sdk.blocks.blockInterfaces;

import android.view.View;
import android.view.ViewGroup;

public interface LightWeightReplyBlock {
   View addLWR(String var1, boolean var2, boolean var3, ViewGroup var4);
}
