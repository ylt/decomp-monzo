package io.intercom.android.sdk.blocks.blockInterfaces;

import android.view.View;
import android.view.ViewGroup;
import io.intercom.android.sdk.blocks.models.Link;

public interface LinkBlock {
   View addLinkBlock(Link var1, boolean var2, boolean var3, ViewGroup var4);
}
