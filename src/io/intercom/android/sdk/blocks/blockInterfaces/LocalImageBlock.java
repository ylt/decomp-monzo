package io.intercom.android.sdk.blocks.blockInterfaces;

import android.view.View;
import android.view.ViewGroup;
import io.intercom.android.sdk.blocks.BlockAlignment;

public interface LocalImageBlock {
   View addImage(String var1, int var2, int var3, BlockAlignment var4, boolean var5, boolean var6, ViewGroup var7);
}
