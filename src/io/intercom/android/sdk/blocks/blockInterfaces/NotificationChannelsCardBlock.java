package io.intercom.android.sdk.blocks.blockInterfaces;

import android.view.View;
import android.view.ViewGroup;
import io.intercom.android.sdk.blocks.models.NotificationChannelsCard;

public interface NotificationChannelsCardBlock {
   View addNotificationChannelsCardBlock(NotificationChannelsCard var1, boolean var2, boolean var3, ViewGroup var4);
}
