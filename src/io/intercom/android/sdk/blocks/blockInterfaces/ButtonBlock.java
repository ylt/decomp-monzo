package io.intercom.android.sdk.blocks.blockInterfaces;

import android.view.View;
import android.view.ViewGroup;
import io.intercom.android.sdk.blocks.BlockAlignment;

public interface ButtonBlock {
   View addButton(String var1, String var2, BlockAlignment var3, boolean var4, boolean var5, ViewGroup var6);
}
