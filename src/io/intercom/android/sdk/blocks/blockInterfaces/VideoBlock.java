package io.intercom.android.sdk.blocks.blockInterfaces;

import android.view.View;
import android.view.ViewGroup;
import io.intercom.android.sdk.blocks.VideoProvider;

public interface VideoBlock {
   View addVideo(String var1, VideoProvider var2, String var3, boolean var4, boolean var5, ViewGroup var6);
}
