package io.intercom.android.sdk.blocks.blockInterfaces;

import android.view.View;
import android.view.ViewGroup;
import io.intercom.android.sdk.blocks.BlockAlignment;

public interface ImageBlock {
   View addImage(String var1, String var2, int var3, int var4, BlockAlignment var5, boolean var6, boolean var7, ViewGroup var8);
}
