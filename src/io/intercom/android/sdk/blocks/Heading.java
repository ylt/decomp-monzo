package io.intercom.android.sdk.blocks;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.content.a;
import android.text.Spanned;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import io.intercom.android.sdk.Provider;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.blocks.blockInterfaces.HeadingBlock;
import io.intercom.android.sdk.blocks.blockInterfaces.SubheadingBlock;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.android.sdk.utilities.BlockUtils;
import io.intercom.android.sdk.utilities.ColorUtils;
import io.intercom.android.sdk.utilities.FontUtils;
import io.intercom.android.sdk.utilities.TrackingLinkMovementMethod;

class Heading implements HeadingBlock, SubheadingBlock {
   private static final int LARGE_MARGIN_BOTTOM_DP = 32;
   private static final int LARGE_TEXT_SP = 24;
   private static final int MARGIN_BOTTOM_DP = 16;
   private static final int MEDIUM_TEXT_SP = 20;
   private static final int SMALL_TEXT_SP = 15;
   private final Provider appConfigProvider;
   private final StyleType style;

   Heading(StyleType var1, Provider var2) {
      this.style = var1;
      this.appConfigProvider = var2;
   }

   private void styleAnnouncementHeading(TextView var1, int var2, int var3, int var4) {
      var1.setTextSize((float)var3);
      var1.setTextColor(var2);
      var1.setLinkTextColor(var2);
      var1.setMovementMethod(new TrackingLinkMovementMethod());
      BlockUtils.setLargeLineSpacing(var1);
      BlockUtils.setMarginBottom(var1, var4);
   }

   private void styleChatHeading(TextView var1, int var2) {
      var1.setTextSize(15.0F);
      var1.setTextColor(var2);
      var1.setLinkTextColor(var2);
      BlockUtils.setSmallLineSpacing(var1);
      BlockUtils.setDefaultMarginBottom(var1);
   }

   public View addHeading(Spanned var1, BlockAlignment var2, boolean var3, boolean var4, ViewGroup var5) {
      Context var7 = var5.getContext();
      TextView var8 = new TextView(var7);
      BlockUtils.createLayoutParams(var8, -2, -2);
      int var6 = ((AppConfig)this.appConfigProvider.get()).getBaseColor();
      switch(null.$SwitchMap$io$intercom$android$sdk$blocks$StyleType[this.style.ordinal()]) {
      case 1:
         this.styleAnnouncementHeading(var8, ColorUtils.lightenColor(var6), 24, 32);
         FontUtils.setRobotoLightTypeface(var8);
         break;
      case 2:
         this.styleAnnouncementHeading(var8, var6, 24, 32);
         break;
      case 3:
         this.styleAnnouncementHeading(var8, var6, 20, 32);
         break;
      case 4:
         this.styleChatHeading(var8, a.c(var7, R.color.intercom_grey_800));
         var8.setTypeface((Typeface)null, 1);
         break;
      default:
         this.styleChatHeading(var8, a.c(var7, R.color.intercom_grey_800));
         var8.setTypeface((Typeface)null, 1);
         var8.setMovementMethod(new TrackingLinkMovementMethod());
      }

      var8.setText(var1);
      var8.setGravity(var2.getGravity());
      BlockUtils.setLayoutMarginsAndGravity(var8, var2.getGravity(), var4);
      return var8;
   }

   public View addSubheading(Spanned var1, BlockAlignment var2, boolean var3, boolean var4, ViewGroup var5) {
      Context var7 = var5.getContext();
      TextView var8 = new TextView(var7);
      BlockUtils.createLayoutParams(var8, -2, -2);
      int var6 = ((AppConfig)this.appConfigProvider.get()).getBaseColor();
      switch(null.$SwitchMap$io$intercom$android$sdk$blocks$StyleType[this.style.ordinal()]) {
      case 1:
         this.styleAnnouncementHeading(var8, ColorUtils.lightenColor(var6), 15, 16);
         FontUtils.setRobotoLightTypeface(var8);
         break;
      case 2:
      case 3:
         this.styleAnnouncementHeading(var8, var6, 15, 16);
         break;
      case 4:
         this.styleChatHeading(var8, a.c(var7, R.color.intercom_grey_800));
         break;
      default:
         this.styleChatHeading(var8, var6);
         var8.setMovementMethod(new TrackingLinkMovementMethod());
      }

      var8.setText(var1);
      var8.setGravity(var2.getGravity());
      BlockUtils.setLayoutMarginsAndGravity(var8, var2.getGravity(), var4);
      return var8;
   }
}
