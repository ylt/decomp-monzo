package io.intercom.android.sdk.blocks;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView.ScaleType;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.blocks.blockInterfaces.LocalImageBlock;
import io.intercom.android.sdk.commons.utilities.ScreenUtils;
import io.intercom.android.sdk.logger.LumberMill;
import io.intercom.android.sdk.transforms.RoundedCornersTransform;
import io.intercom.android.sdk.twig.Twig;
import io.intercom.android.sdk.utilities.BlockUtils;
import io.intercom.android.sdk.utilities.ImageUtils;
import io.intercom.android.sdk.views.ProgressFrameLayout;
import io.intercom.android.sdk.views.ResizableImageView;
import io.intercom.android.sdk.views.UploadProgressBar;
import io.intercom.com.bumptech.glide.h;
import io.intercom.com.bumptech.glide.i;
import io.intercom.com.bumptech.glide.j;
import io.intercom.com.bumptech.glide.f.e;
import io.intercom.com.bumptech.glide.f.f;
import io.intercom.com.bumptech.glide.load.a;
import io.intercom.com.bumptech.glide.load.l;
import io.intercom.com.bumptech.glide.load.engine.GlideException;
import io.intercom.com.bumptech.glide.load.resource.b.b;

class LocalImage extends Image implements LocalImageBlock {
   private final i requestManager;
   private final Twig twig = LumberMill.getLogger();

   LocalImage(StyleType var1, i var2) {
      super(var1);
      this.requestManager = var2;
   }

   public View addImage(String var1, int var2, int var3, BlockAlignment var4, boolean var5, boolean var6, ViewGroup var7) {
      Context var10 = var7.getContext();
      var2 = ScreenUtils.dpToPx((float)var2, var10);
      var3 = ScreenUtils.dpToPx((float)var3, var10);
      ProgressFrameLayout var14 = new ProgressFrameLayout(var10);
      BlockUtils.createLayoutParams(var14, -2, -2);
      BlockUtils.setDefaultMarginBottom(var14);
      final ResizableImageView var9 = new ResizableImageView(var10);
      BlockUtils.createLayoutParams(var9, -2, -2);
      var9.setAdjustViewBounds(true);
      var9.setScaleType(ScaleType.FIT_START);
      var14.addView(var9);
      h var8 = this.requestManager.a((Object)var1);
      this.setImageViewBounds(var2, var3, var9, var8);
      View var11 = var14.getChildAt(0);
      Resources var12 = var10.getResources();
      if(var11 instanceof UploadProgressBar) {
         var2 = var12.getDimensionPixelSize(R.dimen.intercom_local_image_upload_size);
         var11.setLayoutParams(new LayoutParams(var2, var2, 17));
         var11.bringToFront();
         var14.uploadStarted();
      }

      this.setBackground(var9);
      ColorMatrix var13 = new ColorMatrix();
      var13.setSaturation(0.0F);
      var9.setColorFilter(new ColorMatrixColorFilter(var13));
      var8.a((new f()).a((l)(new RoundedCornersTransform(var12.getDimensionPixelSize(R.dimen.intercom_image_rounded_corners)))).b(ImageUtils.getDiskCacheStrategy(var1))).a((j)b.c()).a(new e() {
         public boolean onLoadFailed(GlideException var1, Object var2, io.intercom.com.bumptech.glide.f.a.h var3, boolean var4) {
            LocalImage.this.twig.internal("images", "FAILURE");
            return false;
         }

         public boolean onResourceReady(Drawable var1, Object var2, io.intercom.com.bumptech.glide.f.a.h var3, a var4, boolean var5) {
            LocalImage.this.twig.internal("images", "SUCCESS");
            var9.setBackgroundResource(17170445);
            return false;
         }
      }).a((ImageView)var9);
      BlockUtils.setLayoutMarginsAndGravity(var14, var4.getGravity(), var6);
      return var14;
   }
}
