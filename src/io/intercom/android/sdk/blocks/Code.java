package io.intercom.android.sdk.blocks;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.content.a;
import android.text.Spanned;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.blocks.blockInterfaces.CodeBlock;
import io.intercom.android.sdk.commons.utilities.ScreenUtils;
import io.intercom.android.sdk.utilities.BlockUtils;

class Code implements CodeBlock {
   private TextView getCodeBlockView(Spanned var1, Context var2) {
      TextView var3 = new TextView(var2);
      var3.setTextColor(a.c(var2, R.color.intercom_white));
      var3.setTextSize(14.0F);
      var3.setText(var1);
      var3.setPadding(ScreenUtils.dpToPx(14.0F, var2), ScreenUtils.dpToPx(12.0F, var2), ScreenUtils.dpToPx(14.0F, var2), ScreenUtils.dpToPx(14.0F, var2));
      var3.setBackgroundResource(R.color.intercom_slate_grey_two);
      var3.setTypeface(Typeface.MONOSPACE);
      BlockUtils.createLayoutParams(var3, -2, -2);
      BlockUtils.setDefaultMarginBottom(var3);
      return var3;
   }

   public View addCode(Spanned var1, boolean var2, boolean var3, ViewGroup var4) {
      TextView var5 = this.getCodeBlockView(var1, var4.getContext());
      BlockUtils.setLayoutMarginsAndGravity(var5, 3, var3);
      return var5;
   }
}
