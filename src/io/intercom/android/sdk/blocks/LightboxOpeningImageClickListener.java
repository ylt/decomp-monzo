package io.intercom.android.sdk.blocks;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build.VERSION;
import android.support.v4.app.b;
import android.text.TextUtils;
import android.widget.ImageView;
import io.intercom.android.sdk.api.Api;
import io.intercom.android.sdk.lightbox.LightBoxActivity;
import io.intercom.android.sdk.utilities.LinkOpener;

public class LightboxOpeningImageClickListener implements ImageClickListener {
   private final Api api;

   public LightboxOpeningImageClickListener(Api var1) {
      this.api = var1;
   }

   public void onImageClicked(String var1, String var2, ImageView var3, int var4, int var5) {
      boolean var6 = false;
      Context var7 = var3.getContext();
      if(!TextUtils.isEmpty(var2)) {
         LinkOpener.handleUrl(var2, var7, this.api);
      } else if(var7 instanceof Activity) {
         Activity var9 = (Activity)var7;
         if((var9.getWindow().getAttributes().flags & 1024) != 0) {
            var6 = true;
         }

         Intent var8 = LightBoxActivity.imageIntent(var9, var1, var6, var4, var5);
         if(VERSION.SDK_INT >= 16) {
            var9.startActivity(var8, b.a(var9, var3, "lightbox_image").a());
         } else {
            var9.startActivity(var8);
         }
      } else {
         var7.startActivity(LightBoxActivity.imageIntent(var7, var1, false, var4, var5));
      }

   }
}
