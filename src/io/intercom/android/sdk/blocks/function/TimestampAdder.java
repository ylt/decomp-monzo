package io.intercom.android.sdk.blocks.function;

import io.intercom.android.sdk.models.Part;
import java.util.Calendar;
import java.util.List;

public class TimestampAdder {
   private final Calendar currentDate;
   private final Calendar nextDate;

   TimestampAdder(Calendar var1, Calendar var2) {
      this.currentDate = var1;
      this.nextDate = var2;
   }

   public static TimestampAdder create() {
      return new TimestampAdder(Calendar.getInstance(), Calendar.getInstance());
   }

   private boolean isDivider(Part var1) {
      return "day_divider_style".equals(var1.getMessageStyle());
   }

   public void addDayDividers(List var1) {
      this.currentDate.setTimeInMillis(0L);
      this.nextDate.setTimeInMillis(0L);

      for(int var2 = 0; var2 < var1.size(); ++var2) {
         if(!this.isDivider((Part)var1.get(var2))) {
            long var4 = ((Part)var1.get(var2)).getCreatedAt();
            this.nextDate.setTimeInMillis(1000L * var4);
            int var3 = var2;
            if(this.datesAreFromDifferentDays()) {
               label21: {
                  if(var2 != 0) {
                     var3 = var2;
                     if(this.isDivider((Part)var1.get(var2 - 1))) {
                        break label21;
                     }
                  }

                  var1.add(var2, (new Part.Builder()).withStyle("day_divider_style").withCreatedAt(var4).build());
                  var3 = var2 + 1;
               }
            }

            this.currentDate.setTimeInMillis(this.nextDate.getTimeInMillis());
            var2 = var3;
         }
      }

   }

   boolean datesAreFromDifferentDays() {
      boolean var2 = true;
      boolean var1 = var2;
      if(this.currentDate.get(6) == this.nextDate.get(6)) {
         if(this.currentDate.get(1) != this.nextDate.get(1)) {
            var1 = var2;
         } else {
            var1 = false;
         }
      }

      return var1;
   }
}
