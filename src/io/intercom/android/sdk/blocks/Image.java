package io.intercom.android.sdk.blocks;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Point;
import android.widget.ImageView;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.commons.utilities.ScreenUtils;
import io.intercom.android.sdk.views.ResizableImageView;
import io.intercom.com.bumptech.glide.h;
import io.intercom.com.bumptech.glide.f.f;

class Image {
   private final StyleType style;

   Image(StyleType var1) {
      this.style = var1;
   }

   private int calculateChatFullPadding(Context var1) {
      Resources var7 = var1.getResources();
      float var4 = var7.getDimension(R.dimen.intercom_chat_overlay_width);
      float var2 = var7.getDimension(R.dimen.intercom_chat_overlay_padding_right);
      float var3 = var7.getDimension(R.dimen.intercom_chat_overlay_text_margin_left);
      float var5 = var7.getDimension(R.dimen.intercom_chat_overlay_text_padding_left);
      float var6 = var7.getDimension(R.dimen.intercom_chat_overlay_text_padding_right);
      return (int)((float)ScreenUtils.getScreenDimensions(var1).x - (var4 - (var6 + var2 + var3 + var5)));
   }

   private int getTotalViewPadding(Context var1) {
      Resources var6 = var1.getResources();
      float var2;
      float var3;
      int var5;
      switch(null.$SwitchMap$io$intercom$android$sdk$blocks$StyleType[this.style.ordinal()]) {
      case 1:
      case 2:
      case 3:
         var2 = var6.getDimension(R.dimen.intercom_avatar_size);
         float var4 = var6.getDimension(R.dimen.intercom_conversation_row_icon_spacer);
         var3 = var6.getDimension(R.dimen.intercom_conversation_row_margin);
         var5 = (int)(var6.getDimension(R.dimen.intercom_cell_horizontal_padding) * 3.0F + var2 + var4 + var3);
         break;
      case 4:
         var2 = var6.getDimension(R.dimen.intercom_cell_content_padding);
         var5 = (int)(var6.getDimension(R.dimen.intercom_post_cell_padding) + var2) * 2;
         break;
      case 5:
         var2 = var6.getDimension(R.dimen.intercom_cell_content_padding);
         var3 = var6.getDimension(R.dimen.intercom_note_cell_padding);
         var5 = (int)(var6.getDimension(R.dimen.intercom_note_layout_margin) + var2 + var3) * 2;
         break;
      case 6:
         var5 = (int)var6.getDimension(R.dimen.intercom_cell_horizontal_padding) * 2;
         break;
      case 7:
         var5 = this.calculateChatFullPadding(var1);
         break;
      default:
         var5 = 0;
      }

      return var5;
   }

   protected StyleType getStyle() {
      return this.style;
   }

   protected void setBackground(ImageView var1) {
      var1.setBackgroundResource(R.drawable.intercom_rounded_image_preview);
   }

   protected void setImageViewBounds(int var1, int var2, ResizableImageView var3, h var4) {
      var3.setTotalViewPadding(this.getTotalViewPadding(var3.getContext()));
      var3.setDisplayImageDimensions(var1, var2);
      Point var5 = var3.getImageDimens();
      if(var5.x > 0 && var5.y > 0) {
         var4.a((new f()).a(var5.x, var5.y));
      }

   }
}
