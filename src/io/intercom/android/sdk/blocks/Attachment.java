package io.intercom.android.sdk.blocks;

import android.content.Context;
import android.support.v4.content.a;
import android.text.TextUtils.TruncateAt;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.FrameLayout.LayoutParams;
import io.intercom.android.sdk.Provider;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.blocks.blockInterfaces.AttachmentListBlock;
import io.intercom.android.sdk.blocks.blockInterfaces.LocalAttachmentBlock;
import io.intercom.android.sdk.blocks.models.BlockAttachment;
import io.intercom.android.sdk.blocks.views.AttachmentView;
import io.intercom.android.sdk.commons.utilities.ScreenUtils;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.android.sdk.utilities.BlockUtils;
import io.intercom.android.sdk.views.ProgressLinearLayout;
import io.intercom.android.sdk.views.UploadProgressBar;
import java.util.List;

class Attachment implements AttachmentListBlock, LocalAttachmentBlock {
   private static final int ATTACHMENT_LINE_SPACING_DP = 12;
   private static final int DIVIDER_MARGIN_DP = 6;
   private static final int DIVIDER_WIDTH_DP = 1;
   private static final int LOCAL_ICON_SIZE_DP = 20;
   private final Provider appConfigProvider;
   private final StyleType style;

   public Attachment(StyleType var1, Provider var2) {
      this.style = var1;
      this.appConfigProvider = var2;
   }

   private ImageView getAttachmentIcon(Context var1) {
      ImageView var2 = new ImageView(var1);
      var2.setLayoutParams(new LayoutParams(-1, -1));
      var2.setVisibility(8);
      var2.setImageResource(R.drawable.intercom_icn_attachment);
      return var2;
   }

   private LinearLayout getAttachmentListView(Context var1) {
      LinearLayout var2 = new LinearLayout(var1);
      var2.setOrientation(1);
      var2.setBackgroundResource(R.color.intercom_full_transparent_full_black);
      BlockUtils.createLayoutParams(var2, -2, -2);
      BlockUtils.setDefaultMarginBottom(var2);
      return var2;
   }

   private View getDivider(Context var1) {
      int var3 = ScreenUtils.dpToPx(6.0F, var1);
      int var2 = ScreenUtils.dpToPx(1.0F, var1);
      View var4 = new View(var1);
      var4.setBackgroundResource(R.color.intercom_white);
      BlockUtils.createLayoutParams(var4, var2, -1);
      android.widget.LinearLayout.LayoutParams var5 = (android.widget.LinearLayout.LayoutParams)var4.getLayoutParams();
      var5.leftMargin = var3;
      var5.rightMargin = var3;
      return var4;
   }

   private FrameLayout getIconHolder(Context var1) {
      int var2 = ScreenUtils.dpToPx(20.0F, var1);
      FrameLayout var3 = new FrameLayout(var1);
      BlockUtils.createLayoutParams(var3, var2, var2);
      ((android.widget.LinearLayout.LayoutParams)var3.getLayoutParams()).gravity = 19;
      return var3;
   }

   private ProgressLinearLayout getLocalAttachmentView(Context var1) {
      ProgressLinearLayout var2 = new ProgressLinearLayout(var1);
      BlockUtils.createLayoutParams(var2, -2, -2);
      BlockUtils.setDefaultMarginBottom(var2);
      var2.setOrientation(0);
      return var2;
   }

   private TextView getTextView(String var1, Context var2) {
      TextView var3 = new TextView(var2);
      BlockUtils.createLayoutParams(var3, -2, -2);
      BlockUtils.setSmallLineSpacing(var3);
      var3.setGravity(19);
      var3.setEllipsize(TruncateAt.END);
      var3.setTextSize(15.0F);
      var3.setTextColor(a.c(var2, R.color.intercom_white));
      var3.setBackgroundResource(R.color.intercom_full_transparent_full_black);
      var3.setText(var1);
      return var3;
   }

   private UploadProgressBar getUploadProgressBar(Context var1) {
      UploadProgressBar var2 = new UploadProgressBar(var1);
      var2.setLayoutParams(new LayoutParams(-1, -1));
      return var2;
   }

   public View addAttachment(BlockAttachment var1, boolean var2, boolean var3, ViewGroup var4) {
      Context var9 = var4.getContext();
      ProgressLinearLayout var11 = this.getLocalAttachmentView(var9);
      FrameLayout var8 = this.getIconHolder(var9);
      ImageView var6 = this.getAttachmentIcon(var9);
      UploadProgressBar var7 = this.getUploadProgressBar(var9);
      View var5 = this.getDivider(var9);
      TextView var10 = this.getTextView(var1.getName(), var9);
      var8.addView(var7);
      var8.addView(var6);
      var11.setUploadProgressBar(var7);
      var11.setAttachmentIcon(var6);
      var11.addView(var8);
      var11.addView(var5);
      var11.addView(var10);
      BlockUtils.setLayoutMarginsAndGravity(var11, 3, var3);
      return var11;
   }

   public View addAttachmentList(List var1, boolean var2, boolean var3, ViewGroup var4) {
      int var6 = ((AppConfig)this.appConfigProvider.get()).getBaseColor();
      Context var10 = var4.getContext();
      LinearLayout var9 = this.getAttachmentListView(var10);
      int var7 = var1.size();

      for(int var5 = 0; var5 < var7; ++var5) {
         AttachmentView var8 = new AttachmentView(var10, this.style, var6, (BlockAttachment)var1.get(var5));
         if(var5 < var7 - 1) {
            var8.setPadding(var8.getPaddingLeft(), var8.getPaddingTop(), var8.getPaddingRight(), ScreenUtils.dpToPx(12.0F, var10));
         }

         var9.addView(var8);
      }

      BlockUtils.setLayoutMarginsAndGravity(var9, 3, var3);
      return var9;
   }
}
