package io.intercom.android.sdk.blocks.logic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class TextSplittingStrategy {
   private static final String NEW_LINE = "\n";
   private static final String NEW_LINE_REPLACEMENT = "<br>";
   private static final String NEW_PARAGRAPH_DELIMETER = "\n\n";

   public List apply(String var1) {
      String[] var3 = var1.split("\n\n");

      for(int var2 = 0; var2 < var3.length; ++var2) {
         var3[var2] = var3[var2].replace("\n", "<br>");
      }

      ArrayList var4 = new ArrayList(Arrays.asList(var3));
      var4.removeAll(Collections.singleton(""));
      return var4;
   }
}
