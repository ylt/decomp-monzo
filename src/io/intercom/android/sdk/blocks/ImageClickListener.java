package io.intercom.android.sdk.blocks;

import android.widget.ImageView;

public interface ImageClickListener {
   void onImageClicked(String var1, String var2, ImageView var3, int var4, int var5);
}
