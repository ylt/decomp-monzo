package io.intercom.android.sdk.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.t;
import android.support.v4.content.a;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.view.inputmethod.InputMethodManager;
import io.intercom.android.sdk.Injector;
import io.intercom.android.sdk.Provider;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.conversation.ConversationFragment;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.android.sdk.inbox.InboxFragment;
import io.intercom.android.sdk.logger.LumberMill;
import io.intercom.android.sdk.metrics.MetricTracker;
import io.intercom.android.sdk.models.Conversation;
import io.intercom.android.sdk.models.LastParticipatingAdmin;
import io.intercom.android.sdk.models.Part;
import io.intercom.android.sdk.state.InboxState;
import io.intercom.android.sdk.state.State;
import io.intercom.android.sdk.twig.Twig;
import io.intercom.android.sdk.utilities.ActivityFinisher;
import io.intercom.android.sdk.utilities.BackgroundUtils;
import io.intercom.android.sdk.utilities.TimeFormatter;
import io.intercom.android.sdk.views.StatusBarThemer;
import java.util.Collections;
import java.util.List;

public class IntercomMessengerActivity extends IntercomBaseActivity implements ConversationFragment.Listener, InboxFragment.Listener {
   static final String LAST_PARTICIPANT = "last_participant";
   static final String PARCEL_CONVERSATION_ID = "parcel_conversation_id";
   static final String PARCEL_INITIAL_MESSAGE = "parcel_initial_message";
   static final String SHOW_INBOX = "showInbox";
   private ActivityFinisher activityFinisher;
   private Provider appConfigProvider;
   View background;
   ValueAnimator colorAnimation;
   private View containerView;
   private InboxFragment inboxFragment;
   private boolean isTwoPane;
   private MetricTracker metricTracker;
   private final Twig twig = LumberMill.getLogger();

   private void applyBackOrCloseMessenger() {
      if(this.getSupportFragmentManager().d() == 0) {
         this.metricTracker.closedMessengerBackButton();
         this.closeMessenger();
      } else {
         this.trackMoveToConversationListMetric();
         super.onBackPressed();
      }

   }

   private void closeMessenger() {
      this.hideKeyboard();
      this.showBackgroundColour();
      this.containerView.animate().y((float)this.getWindow().getDecorView().getHeight()).setInterpolator(new OvershootInterpolator()).setDuration(600L).start();
      this.fadeOutBackground();
   }

   private void displayConversation(String var1, LastParticipatingAdmin var2, boolean var3, boolean var4, String var5) {
      this.displayConversation(var1, var2, var3, var4, var5, Collections.emptyList());
   }

   private void displayConversation(String var1, LastParticipatingAdmin var2, boolean var3, boolean var4, String var5, List var6) {
      this.displayFragment(ConversationFragment.newInstance(var1, var2, var3, this.isTwoPane, var5, var6), ConversationFragment.class.getName(), var4);
   }

   private void displayFragment(Fragment var1, String var2, boolean var3) {
      this.twig.internal("frag", "displaying " + var1);
      t var5 = this.getSupportFragmentManager().a();
      int var4;
      if(var1 instanceof ConversationFragment && this.isTwoPane) {
         var4 = R.id.conversation_fragment;
      } else {
         var4 = R.id.inbox_fragment;
      }

      var5.b(var4, var1, var2);
      if(var3) {
         this.twig.internal("frag", "adding " + var1 + " to the back stack");
         var5.a(var2);
      }

      var5.d();
   }

   private void fadeOutBackground() {
      int var2 = a.c(this, 17170445);
      int var1 = a.c(this, R.color.intercom_transparent_black);
      this.colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), new Object[]{Integer.valueOf(var1), Integer.valueOf(var2)});
      this.colorAnimation.setDuration(300L);
      this.colorAnimation.setStartDelay(100L);
      this.colorAnimation.addUpdateListener(new AnimatorUpdateListener() {
         public void onAnimationUpdate(ValueAnimator var1) {
            IntercomMessengerActivity.this.background.setBackgroundColor(((Integer)var1.getAnimatedValue()).intValue());
         }
      });
      this.colorAnimation.addListener(new AnimatorListenerAdapter() {
         public void onAnimationEnd(Animator var1) {
            IntercomMessengerActivity.this.finish();
            IntercomMessengerActivity.this.overridePendingTransition(0, 0);
         }
      });
      this.colorAnimation.start();
   }

   private int getScreenHeight() {
      return this.findViewById(16908290).getMeasuredHeight();
   }

   private boolean hasLoadedFragment(String var1) {
      boolean var2;
      if(this.getSupportFragmentManager().a(var1) != null) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   private void hideKeyboard() {
      View var1 = this.getCurrentFocus();
      if(var1 != null) {
         ((InputMethodManager)this.getSystemService("input_method")).hideSoftInputFromWindow(var1.getWindowToken(), 0);
      }

   }

   public static Intent openComposer(Context var0, String var1) {
      Intent var2 = (new Intent(var0, IntercomMessengerActivity.class)).setFlags(268435456).putExtra("showInbox", false);
      if(!TextUtils.isEmpty(var1)) {
         var2.putExtra("parcel_initial_message", var1);
      }

      return var2;
   }

   public static Intent openConversation(Context var0, String var1, LastParticipatingAdmin var2) {
      Intent var3 = (new Intent(var0, IntercomMessengerActivity.class)).setFlags(268435456).putExtra("showInbox", false);
      if(!TextUtils.isEmpty(var1)) {
         var3.putExtra("parcel_conversation_id", var1);
      }

      if(!LastParticipatingAdmin.isNull(var2)) {
         var3.putExtra("last_participant", var2);
      }

      var3.setExtrasClassLoader(LastParticipatingAdmin.class.getClassLoader());
      return var3;
   }

   public static Intent openInbox(Context var0) {
      return (new Intent(var0, IntercomMessengerActivity.class)).setFlags(268435456).putExtra("showInbox", true);
   }

   private void showBackgroundColour() {
      this.background.setBackgroundResource(R.color.intercom_transparent_black);
   }

   private void showMessenger() {
      boolean var2 = true;
      Bundle var7 = this.getIntent().getExtras();
      String var4 = "";
      String var6 = "";
      LastParticipatingAdmin var5 = LastParticipatingAdmin.NULL;
      boolean var3;
      if(var7 != null) {
         var7.setClassLoader(Part.class.getClassLoader());
         var4 = var7.getString("parcel_conversation_id", "");
         var6 = var7.getString("parcel_initial_message", "");
         var3 = var7.getBoolean("showInbox");
         if(var7.containsKey("last_participant")) {
            var5 = (LastParticipatingAdmin)var7.getParcelable("last_participant");
         }
      } else {
         var3 = false;
      }

      if(this.isTwoPane) {
         this.displayFragment(this.inboxFragment, InboxFragment.class.getName(), false);
         boolean var1;
         if(!var4.isEmpty()) {
            var1 = true;
         } else {
            var1 = false;
         }

         if(var6.isEmpty()) {
            var2 = false;
         }

         if(var1 || var2) {
            this.displayConversation(var4, var5, false, false, var6);
         }
      } else if(var3) {
         this.displayFragment(this.inboxFragment, InboxFragment.class.getName(), false);
      } else {
         this.displayConversation(var4, var5, false, false, var6);
      }

      this.background = this.getWindow().getDecorView();
      BackgroundUtils.animateBackground(a.c(this, 17170445), a.c(this, R.color.intercom_transparent_black), 200, this.background, new AnimatorListenerAdapter() {
         public void onAnimationEnd(Animator var1) {
            IntercomMessengerActivity.this.background.post(new Runnable() {
               public void run() {
                  IntercomMessengerActivity.this.animateSdkWindowIn();
               }
            });
         }
      });
   }

   private void trackMoveToConversationListMetric() {
      ConversationFragment var1 = this.getExistingConversationFragment();
      if(var1 != null) {
         String var2 = var1.getConversationId();
         if(TextUtils.isEmpty(var2)) {
            this.metricTracker.openConversationsListFromNewConversation();
         } else {
            this.metricTracker.openConversationsListFromConversation(var2);
         }
      }

   }

   void animateSdkWindowIn() {
      this.containerView.setVisibility(0);
      this.containerView.setY((float)this.getScreenHeight());
      this.containerView.animate().y(0.0F).setInterpolator(new OvershootInterpolator(0.6F)).setDuration(350L).setListener(new AnimatorListenerAdapter() {
         public void onAnimationEnd(Animator var1) {
            IntercomMessengerActivity.this.hideBackgroundColor();
            ConversationFragment var2 = IntercomMessengerActivity.this.getExistingConversationFragment();
            if(var2 != null) {
               var2.sdkWindowFinishedAnimating();
            }

         }
      }).start();
   }

   ConversationFragment getExistingConversationFragment() {
      String var1 = ConversationFragment.class.getName();
      return (ConversationFragment)this.getSupportFragmentManager().a(var1);
   }

   void hideBackgroundColor() {
      this.background.setBackgroundResource(R.color.intercom_full_transparent_full_black);
   }

   public void onBackPressed() {
      ConversationFragment var1 = this.getExistingConversationFragment();
      if(var1 != null && var1.shouldHandleOnBackPressed()) {
         var1.handleOnBackPressed();
      } else {
         this.applyBackOrCloseMessenger();
      }

   }

   public void onBackToInboxClicked() {
      if(this.hasLoadedFragment(InboxFragment.class.getName())) {
         this.applyBackOrCloseMessenger();
      } else {
         this.trackMoveToConversationListMetric();
         this.displayFragment(this.inboxFragment, InboxFragment.class.getName(), false);
      }

   }

   public void onComposerSelected() {
      LastParticipatingAdmin var2 = LastParticipatingAdmin.NONE;
      boolean var1;
      if(!this.isTwoPane) {
         var1 = true;
      } else {
         var1 = false;
      }

      this.displayConversation("", var2, true, var1, "");
      this.metricTracker.newConversationFromComposeButton(((State)Injector.get().getStore().state()).teamPresence().getOfficeHours().isEmpty());
   }

   public void onConfigurationChanged(Configuration var1) {
      super.onConfigurationChanged(var1);
      this.containerView.setY(0.0F);
      this.containerView.getLayoutParams().height = -1;
   }

   public void onConversationSelected(Conversation var1) {
      LastParticipatingAdmin var5 = var1.getLastParticipatingAdmin();
      LastParticipatingAdmin var4 = var5;
      if(LastParticipatingAdmin.isNull(var5)) {
         var4 = LastParticipatingAdmin.NONE;
      }

      TimeFormatter var7 = new TimeFormatter(this, Injector.get().getTimeProvider());
      String var6 = var1.getId();
      boolean var3 = var1.isRead();
      boolean var2;
      if(!this.isTwoPane) {
         var2 = true;
      } else {
         var2 = false;
      }

      this.displayConversation(var6, var4, var3, var2, "", var1.getGroupConversationParticipants());
      this.metricTracker.openConversationFromConversationList(var1.getId(), ((State)Injector.get().getStore().state()).teamPresence().getOfficeHours().isEmpty(), var4.isActive(), var7.getLastActiveMinutes(var4.getLastActiveAt()));
   }

   public void onConversationsLoaded(List var1, InboxState.Status var2) {
      if(this.isTwoPane && var2 == InboxState.Status.SUCCESS && this.getExistingConversationFragment() == null) {
         boolean var3;
         if(!var1.isEmpty()) {
            var3 = true;
         } else {
            var3 = false;
         }

         if(var3) {
            this.onConversationSelected((Conversation)var1.get(0));
         } else if(((AppConfig)this.appConfigProvider.get()).isInboundMessages()) {
            this.onComposerSelected();
         }
      }

   }

   protected void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.setContentView(R.layout.intercom_messenger_activity_layout);
      this.isTwoPane = this.getResources().getBoolean(R.bool.intercom_is_two_pane);
      this.inboxFragment = InboxFragment.newInstance(this.isTwoPane);
      this.containerView = this.findViewById(R.id.messenger_container);
      if(this.containerView != null) {
         this.containerView.setVisibility(8);
      }

      this.getFragmentManager().executePendingTransactions();
      this.showMessenger();
      Injector var2 = Injector.get();
      this.activityFinisher = var2.getActivityFinisher();
      this.activityFinisher.register(this);
      this.metricTracker = var2.getMetricTracker();
      this.appConfigProvider = var2.getAppConfigProvider();
   }

   protected void onDestroy() {
      this.activityFinisher.unregister(this);
      super.onDestroy();
   }

   public void onToolbarCloseClicked() {
      this.metricTracker.closedMessengerCloseButton();
      this.closeMessenger();
   }

   public void setStatusBarColor() {
      StatusBarThemer.setStatusBarColor(this.getWindow(), (AppConfig)this.appConfigProvider.get());
   }
}
