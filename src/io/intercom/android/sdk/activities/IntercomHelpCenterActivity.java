package io.intercom.android.sdk.activities;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff.Mode;
import android.os.Bundle;
import android.os.Build.VERSION;
import android.support.v4.view.b.b;
import android.text.TextUtils;
import android.transition.ChangeBounds;
import android.transition.Transition;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import io.intercom.android.sdk.Injector;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.android.sdk.metrics.MetricTracker;

public class IntercomHelpCenterActivity extends IntercomBaseActivity {
   private static final int ENTRANCE_ANIMATION_TIME_MS = 300;
   private static final int EXIT_ANIMATION_TIME_MS = 150;
   public static final String LINK_TRANSITION_KEY = "link_background";
   private static final String PARCEL_CONVERSATION_ID = "parcel_conversation_id";
   private static final String PARCEL_HELP_CENTER_URL = "parcel_help_center_url";
   String conversationId = "";
   ProgressBar loadingView;
   MetricTracker metricTracker;
   boolean titleBarEnabled = false;
   WebView webView;

   public static Intent buildIntent(Context var0, String var1, String var2) {
      Intent var3 = new Intent(var0, IntercomHelpCenterActivity.class);
      var3.putExtra("parcel_help_center_url", var1);
      var3.putExtra("parcel_conversation_id", var2);
      var3.setFlags(268435456);
      return var3;
   }

   @TargetApi(19)
   private Transition enterTransition() {
      ChangeBounds var1 = new ChangeBounds();
      var1.setInterpolator(new b());
      var1.setDuration(300L);
      return var1;
   }

   private void fadeOutView(View var1) {
      var1.animate().alpha(0.0F).setDuration(150L).start();
   }

   @TargetApi(19)
   private Transition returnTransition() {
      ChangeBounds var1 = new ChangeBounds();
      var1.setInterpolator(new b());
      var1.setDuration(150L);
      return var1;
   }

   void closeHelpCenter() {
      this.titleBarEnabled = false;
      this.fadeOutView(this.webView);
      this.supportFinishAfterTransition();
   }

   public void onBackPressed() {
      this.metricTracker.closedHelpCenter(this.conversationId, 1);
      this.closeHelpCenter();
   }

   protected void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.setContentView(R.layout.intercom_activity_help_center);
      Injector var4 = Injector.get();
      this.metricTracker = var4.getMetricTracker();
      AppConfig var3 = (AppConfig)var4.getAppConfigProvider().get();
      String var5 = "";
      Bundle var2 = this.getIntent().getExtras();
      if(var2 != null) {
         var5 = var2.getString("parcel_help_center_url", "");
         this.conversationId = var2.getString("parcel_conversation_id", "");
      }

      if(TextUtils.isEmpty(var5)) {
         this.closeHelpCenter();
      }

      this.loadingView = (ProgressBar)this.findViewById(R.id.loading_view);
      this.loadingView.getIndeterminateDrawable().setColorFilter(var3.getBaseColor(), Mode.SRC_IN);
      this.webView = (WebView)this.findViewById(R.id.help_center_web_view);
      this.webView.setVerticalScrollBarEnabled(false);
      this.webView.setWebViewClient(new WebViewClient() {
         public void onPageFinished(WebView var1, String var2) {
            IntercomHelpCenterActivity.this.webView.animate().alpha(1.0F).setDuration(300L).start();
            IntercomHelpCenterActivity.this.loadingView.setVisibility(8);
            IntercomHelpCenterActivity.this.metricTracker.viewedHelpCenter(IntercomHelpCenterActivity.this.conversationId);
            IntercomHelpCenterActivity.this.webView.loadUrl("javascript:Intercom('shutdown')");
         }
      });
      if(VERSION.SDK_INT >= 21) {
         this.webView.setClipToOutline(true);
      }

      this.webView.getSettings().setJavaScriptEnabled(true);
      this.webView.loadUrl(var5);
      this.findViewById(R.id.dismiss).setOnClickListener(new OnClickListener() {
         public void onClick(View var1) {
            IntercomHelpCenterActivity.this.metricTracker.closedHelpCenter(IntercomHelpCenterActivity.this.conversationId, 0);
            IntercomHelpCenterActivity.this.closeHelpCenter();
         }
      });
      if(VERSION.SDK_INT >= 21) {
         this.getWindow().setSharedElementEnterTransition(this.enterTransition());
         this.getWindow().setSharedElementReturnTransition(this.returnTransition());
         this.findViewById(R.id.link_view).setTransitionName("link_background");
      }

   }
}
