package io.intercom.android.sdk.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.j;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.OvershootInterpolator;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import io.intercom.android.sdk.Injector;
import io.intercom.android.sdk.Provider;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.actions.Actions;
import io.intercom.android.sdk.api.Api;
import io.intercom.android.sdk.blocks.BlocksViewHolder;
import io.intercom.android.sdk.blocks.LightboxOpeningImageClickListener;
import io.intercom.android.sdk.blocks.LinkOpeningButtonClickListener;
import io.intercom.android.sdk.blocks.UploadingImageCache;
import io.intercom.android.sdk.blocks.ViewHolderGenerator;
import io.intercom.android.sdk.commons.utilities.ScreenUtils;
import io.intercom.android.sdk.conversation.ReactionInputView;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.android.sdk.metrics.MetricTracker;
import io.intercom.android.sdk.models.LastParticipatingAdmin;
import io.intercom.android.sdk.models.Part;
import io.intercom.android.sdk.models.ReactionReply;
import io.intercom.android.sdk.utilities.BlockUtils;
import io.intercom.android.sdk.utilities.Phrase;
import io.intercom.android.sdk.utilities.TimeFormatter;
import io.intercom.android.sdk.views.ActiveStatePresenter;
import io.intercom.android.sdk.views.ContentAwareScrollView;
import io.intercom.android.sdk.views.IntercomToolbar;
import io.intercom.com.bumptech.glide.c;
import io.intercom.com.bumptech.glide.i;

public class IntercomPostActivity extends IntercomBaseActivity implements OnClickListener {
   private static final String LAST_PARTICIPANT = "last_participant";
   private static final String PARCEL_CONVERSATION_ID = "parcel_conversation_id";
   private static final String PARCEL_PART = "parcel_part";
   private static final String POST_PREVIEW = "is_post_preview";
   View composerLayout;
   String conversationId;
   private boolean isPreview = false;
   private LastParticipatingAdmin lastParticipatingAdmin;
   MetricTracker metricTracker;
   Part part;
   private FrameLayout postContainer;
   ContentAwareScrollView postView;
   private final FullScreenInAppPresenter presenter = new FullScreenInAppPresenter();
   ReactionInputView reactionComposer;
   private i requestManager;
   private EditText textComposer;

   public static Intent buildPostIntent(Context var0, Part var1, String var2, LastParticipatingAdmin var3, boolean var4) {
      Intent var5 = new Intent(var0, IntercomPostActivity.class);
      var5.putExtra("parcel_part", var1);
      if(!TextUtils.isEmpty(var2)) {
         var5.putExtra("parcel_conversation_id", var2);
      }

      var5.putExtra("last_participant", var3);
      var5.putExtra("is_post_preview", var4);
      var5.setFlags(268435456);
      var5.setExtrasClassLoader(Part.class.getClassLoader());
      return var5;
   }

   void animateComposer() {
      this.composerLayout.setY(this.composerLayout.getY() + (float)ScreenUtils.dpToPx(156.0F, this));
      this.composerLayout.setVisibility(0);
      this.textComposer.setVisibility(0);
      this.composerLayout.animate().setInterpolator(new OvershootInterpolator(0.6F)).translationY(0.0F).setDuration(300L).start();
   }

   void animateContent() {
      this.postContainer.setY(this.postContainer.getY() + (float)ScreenUtils.dpToPx(400.0F, this));
      this.postContainer.setVisibility(0);
      this.postContainer.animate().setInterpolator(new OvershootInterpolator(0.6F)).translationY(0.0F).setDuration(300L).start();
   }

   void animateToolbar(IntercomToolbar var1) {
      var1.setY(var1.getY() + (float)ScreenUtils.dpToPx(200.0F, this));
      var1.setVisibility(0);
      var1.animate().setInterpolator(new OvershootInterpolator(0.6F)).translationY(0.0F).setDuration(300L).start();
   }

   public void onBackPressed() {
      this.presenter.closeWindow(this);
      if(this.isPreview) {
         this.metricTracker.closedInAppFromFull(this.conversationId, this.part.getId());
      }

   }

   public void onClick(View var1) {
      if(var1.getId() == R.id.post_touch_target) {
         this.presenter.closeWindow(this);
         if(this.isPreview) {
            this.metricTracker.openedConversationFromFull(this.conversationId, this.part.getId());
            this.startActivity(IntercomMessengerActivity.openConversation(this, this.conversationId, this.lastParticipatingAdmin));
         }
      }

   }

   protected void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.setContentView(R.layout.intercom_activity_post);
      this.requestManager = c.a((j)this);
      Injector var6 = Injector.get();
      Api var4 = var6.getApi();
      this.metricTracker = var6.getMetricTracker();
      Provider var5 = var6.getAppConfigProvider();
      final IntercomToolbar var3 = (IntercomToolbar)this.findViewById(R.id.intercom_toolbar);
      var3.setListener(new InAppToolbarListener(this));
      this.part = new Part();
      this.lastParticipatingAdmin = LastParticipatingAdmin.NULL;
      TimeFormatter var7 = new TimeFormatter(this, var6.getTimeProvider());
      Bundle var8 = this.getIntent().getExtras();
      if(var8 != null) {
         var8.setClassLoader(Part.class.getClassLoader());
         if(var8.containsKey("parcel_part")) {
            this.part = (Part)var8.getParcelable("parcel_part");
            String var10;
            if(this.part != null && this.part.getParticipant() != null) {
               var10 = this.part.getParticipant().getForename();
            } else {
               var10 = "";
            }

            String var9 = ((AppConfig)var5.get()).getName();
            var3.setTitle(Phrase.from((Context)this, R.string.intercom_teammate_from_company).put("name", var10).put("company", var9).format());
         }

         if(var8.containsKey("parcel_conversation_id")) {
            this.conversationId = var8.getString("parcel_conversation_id", "");
            var6.getStore().dispatch(Actions.conversationMarkedAsRead(this.conversationId));
            var4.markConversationAsRead(this.conversationId);
         }

         if(var8.containsKey("is_post_preview")) {
            this.isPreview = var8.getBoolean("is_post_preview", false);
         }

         if(var8.containsKey("last_participant")) {
            this.lastParticipatingAdmin = (LastParticipatingAdmin)var8.getParcelable("last_participant");
            if(!LastParticipatingAdmin.isNull(this.lastParticipatingAdmin)) {
               var3.setSubtitle(var7.getAdminActiveStatus(this.lastParticipatingAdmin, var5));
            }
         }
      }

      var3.setVisibility(8);
      var3.setUpPostToolbar(this.part.getParticipant(), this.lastParticipatingAdmin.isActive(), new ActiveStatePresenter(), (AppConfig)var5.get(), this.requestManager);
      int var2 = ((AppConfig)var5.get()).getBaseColor();
      View var11 = this.getWindow().getDecorView();
      var11.setBackgroundColor(Color.argb(153, Color.red(var2), Color.green(var2), Color.blue(var2)));
      BlocksViewHolder var12 = (new ViewHolderGenerator(new UploadingImageCache(), var4, var5, this.conversationId, new LightboxOpeningImageClickListener(var4), new LinkOpeningButtonClickListener(var4), this.requestManager)).getPostHolder();
      this.postContainer = (FrameLayout)this.findViewById(R.id.post_container);
      this.composerLayout = this.findViewById(R.id.conversation_coordinator);
      this.textComposer = (EditText)this.findViewById(R.id.composer_input_view);
      this.reactionComposer = (ReactionInputView)this.findViewById(R.id.reaction_input_view);
      this.postView = (ContentAwareScrollView)this.findViewById(R.id.post_view);
      LinearLayout var13 = ViewHolderGenerator.createPartsLayout(var12, this.part, this);
      this.postView.addView(BlockUtils.getBlockView(this.postView, var13, this));
      if(this.isPreview && !ReactionReply.isNull(this.part.getReactionReply())) {
         final ConversationReactionListener var14 = new ConversationReactionListener(MetricTracker.ReactionLocation.IN_APP, this.part.getId(), this.conversationId, Injector.get().getApi(), this.metricTracker);
         this.postView.setListener(new ContentAwareScrollView.Listener() {
            public void onBottomReached() {
               IntercomPostActivity.this.postView.setListener((ContentAwareScrollView.Listener)null);
               if(IntercomPostActivity.this.composerLayout.getVisibility() != 0) {
                  IntercomPostActivity.this.composerLayout.setVisibility(0);
                  IntercomPostActivity.this.reactionComposer.setUpReactions(IntercomPostActivity.this.part.getReactionReply(), true, var14, IntercomPostActivity.this.requestManager);
               }

            }

            public void onScrollChanged(int var1) {
            }
         });
         this.reactionComposer.preloadReactionImages(this.part.getReactionReply(), this.requestManager);
      }

      if(this.openedFromConversation()) {
         this.metricTracker.viewedInAppFromMessenger(this.part.getMessageStyle(), this.conversationId, this.part.getId());
      }

      var11.setAlpha(0.0F);
      var11.animate().alpha(1.0F).setDuration(200L).setListener(new AnimatorListenerAdapter() {
         public void onAnimationEnd(Animator var1) {
            IntercomPostActivity.this.animateToolbar(var3);
            IntercomPostActivity.this.animateContent();
            if(IntercomPostActivity.this.openedFromConversation()) {
               IntercomPostActivity.this.composerLayout.setVisibility(8);
            } else if(ReactionReply.isNull(IntercomPostActivity.this.part.getReactionReply())) {
               IntercomPostActivity.this.findViewById(R.id.post_touch_target).setOnClickListener(IntercomPostActivity.this);
               IntercomPostActivity.this.animateComposer();
            }

         }
      }).start();
   }

   boolean openedFromConversation() {
      boolean var1;
      if(!this.isPreview) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }
}
