package io.intercom.android.sdk.activities;

import android.content.Context;
import android.support.v7.app.e;
import io.intercom.android.sdk.Injector;
import io.intercom.android.sdk.utilities.ContextLocaliser;

public abstract class IntercomBaseActivity extends e {
   protected void attachBaseContext(Context var1) {
      super.attachBaseContext(var1);
      (new ContextLocaliser(Injector.get().getAppConfigProvider())).applyOverrideConfiguration(this, var1);
   }
}
