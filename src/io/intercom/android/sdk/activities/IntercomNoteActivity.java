package io.intercom.android.sdk.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.j;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import io.intercom.android.sdk.Injector;
import io.intercom.android.sdk.Provider;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.actions.Actions;
import io.intercom.android.sdk.api.Api;
import io.intercom.android.sdk.blocks.BlocksViewHolder;
import io.intercom.android.sdk.blocks.LightboxOpeningImageClickListener;
import io.intercom.android.sdk.blocks.LinkOpeningButtonClickListener;
import io.intercom.android.sdk.blocks.UploadingImageCache;
import io.intercom.android.sdk.blocks.ViewHolderGenerator;
import io.intercom.android.sdk.conversation.ReactionInputView;
import io.intercom.android.sdk.conversation.ReactionListener;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.android.sdk.metrics.MetricTracker;
import io.intercom.android.sdk.models.LastParticipatingAdmin;
import io.intercom.android.sdk.models.Part;
import io.intercom.android.sdk.models.ReactionReply;
import io.intercom.android.sdk.utilities.BlockUtils;
import io.intercom.android.sdk.utilities.Phrase;
import io.intercom.android.sdk.utilities.TimeFormatter;
import io.intercom.android.sdk.views.ActiveStatePresenter;
import io.intercom.android.sdk.views.ContentAwareScrollView;
import io.intercom.android.sdk.views.IntercomToolbar;
import io.intercom.com.bumptech.glide.c;
import io.intercom.com.bumptech.glide.i;

public class IntercomNoteActivity extends IntercomBaseActivity implements OnClickListener {
   private static final String LAST_PARTICIPANT = "last_participant";
   private static final String PARCEL_CONVERSATION_ID = "parcel_conversation_id";
   private static final String PARCEL_PART = "parcel_part";
   View composerLayout;
   String conversationId;
   private LastParticipatingAdmin lastParticipatingAdmin;
   MetricTracker metricTracker;
   private LinearLayout noteLayout;
   Part part;
   private final FullScreenInAppPresenter presenter = new FullScreenInAppPresenter();
   ReactionInputView reactionComposer;
   private i requestManager;

   public static Intent buildNoteIntent(Context var0, Part var1, String var2, LastParticipatingAdmin var3) {
      Intent var4 = new Intent(var0, IntercomNoteActivity.class);
      var4.putExtra("parcel_part", var1);
      if(!TextUtils.isEmpty(var2)) {
         var4.putExtra("parcel_conversation_id", var2);
      }

      var4.putExtra("last_participant", var3);
      var4.setFlags(268435456);
      var4.setExtrasClassLoader(Part.class.getClassLoader());
      return var4;
   }

   void animateContent() {
      this.noteLayout.setScaleX(0.9F);
      this.noteLayout.setScaleY(0.9F);
      this.noteLayout.setAlpha(0.0F);
      this.noteLayout.setVisibility(0);
      this.noteLayout.animate().scaleX(1.0F).scaleY(1.0F).alpha(1.0F).setDuration(200L).start();
   }

   public void onBackPressed() {
      this.presenter.closeWindow(this);
      this.metricTracker.closedInAppFromFull(this.conversationId, this.part.getId());
   }

   public void onClick(View var1) {
      if(var1.getId() == R.id.note_touch_target) {
         this.presenter.closeWindow(this);
         this.metricTracker.closedInAppFromFull(this.conversationId, this.part.getId());
         this.startActivity(IntercomMessengerActivity.openConversation(this, this.conversationId, this.lastParticipatingAdmin));
      }

   }

   protected void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.setContentView(R.layout.intercom_activity_note);
      this.requestManager = c.a((j)this);
      this.noteLayout = (LinearLayout)this.findViewById(R.id.note_layout);
      this.noteLayout.setVisibility(4);
      this.reactionComposer = (ReactionInputView)this.findViewById(R.id.reaction_input_view);
      IntercomToolbar var2 = (IntercomToolbar)this.findViewById(R.id.intercom_toolbar);
      var2.setListener(new InAppToolbarListener(this));
      this.part = new Part();
      this.lastParticipatingAdmin = LastParticipatingAdmin.NULL;
      Injector var3 = Injector.get();
      Api var4 = var3.getApi();
      this.metricTracker = var3.getMetricTracker();
      Provider var6 = var3.getAppConfigProvider();
      TimeFormatter var7 = new TimeFormatter(this, var3.getTimeProvider());
      Bundle var5 = this.getIntent().getExtras();
      if(var5 != null) {
         var5.setClassLoader(Part.class.getClassLoader());
         if(var5.containsKey("parcel_part")) {
            this.part = (Part)var5.getParcelable("parcel_part");
            String var9;
            if(this.part != null && this.part.getParticipant() != null) {
               var9 = this.part.getParticipant().getForename();
            } else {
               var9 = "";
            }

            String var8 = ((AppConfig)var6.get()).getName();
            var2.setTitle(Phrase.from((Context)this, R.string.intercom_teammate_from_company).put("name", var9).put("company", var8).format());
         }

         if(var5.containsKey("parcel_conversation_id")) {
            this.conversationId = var5.getString("parcel_conversation_id");
            var3.getStore().dispatch(Actions.conversationMarkedAsRead(this.conversationId));
            var4.markConversationAsRead(this.conversationId);
         }

         if(var5.containsKey("last_participant")) {
            this.lastParticipatingAdmin = (LastParticipatingAdmin)var5.getParcelable("last_participant");
            if(!LastParticipatingAdmin.isNull(this.lastParticipatingAdmin)) {
               var2.setSubtitle(var7.getAdminActiveStatus(this.lastParticipatingAdmin, var6));
            }
         }
      }

      var2.setUpNoteToolbar(this.part.getParticipant(), this.lastParticipatingAdmin.isActive(), new ActiveStatePresenter(), (AppConfig)var6.get(), this.requestManager);
      BlocksViewHolder var10 = (new ViewHolderGenerator(new UploadingImageCache(), var4, var6, this.conversationId, new LightboxOpeningImageClickListener(var4), new LinkOpeningButtonClickListener(var4), this.requestManager)).getNoteHolder();
      this.composerLayout = this.findViewById(R.id.note_composer_container);
      final ContentAwareScrollView var11 = (ContentAwareScrollView)this.findViewById(R.id.note_view);
      var11.addView(BlockUtils.getBlockView(var11, ViewHolderGenerator.createPartsLayout(var10, this.part, this), this));
      if(!ReactionReply.isNull(this.part.getReactionReply())) {
         var11.setListener(new ContentAwareScrollView.Listener(new ConversationReactionListener(MetricTracker.ReactionLocation.IN_APP, this.part.getId(), this.conversationId, Injector.get().getApi(), this.metricTracker)) {
            // $FF: synthetic field
            final ReactionListener val$reactionListener;

            {
               this.val$reactionListener = var3;
            }

            public void onBottomReached() {
               var11.setListener((ContentAwareScrollView.Listener)null);
               IntercomNoteActivity.this.composerLayout.setVisibility(0);
               IntercomNoteActivity.this.reactionComposer.setUpReactions(IntercomNoteActivity.this.part.getReactionReply(), true, this.val$reactionListener, IntercomNoteActivity.this.requestManager);
            }

            public void onScrollChanged(int var1) {
            }
         });
         this.reactionComposer.preloadReactionImages(this.part.getReactionReply(), this.requestManager);
      }

      View var12 = this.getWindow().getDecorView();
      var12.setAlpha(0.0F);
      var12.animate().alpha(1.0F).setDuration(200L).setListener(new AnimatorListenerAdapter() {
         public void onAnimationEnd(Animator var1) {
            IntercomNoteActivity.this.animateContent();
            if(ReactionReply.isNull(IntercomNoteActivity.this.part.getReactionReply())) {
               IntercomNoteActivity.this.composerLayout.setVisibility(0);
               IntercomNoteActivity.this.findViewById(R.id.note_touch_target).setOnClickListener(IntercomNoteActivity.this);
               IntercomNoteActivity.this.findViewById(R.id.composer_input_view).setVisibility(0);
            }

         }
      }).start();
   }
}
