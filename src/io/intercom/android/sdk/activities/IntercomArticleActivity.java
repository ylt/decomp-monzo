package io.intercom.android.sdk.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff.Mode;
import android.os.Bundle;
import android.os.Build.VERSION;
import android.support.v4.app.j;
import android.support.v4.view.b.b;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.StyleSpan;
import android.transition.ChangeBounds;
import android.transition.Transition;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.animation.OvershootInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import io.intercom.android.sdk.Injector;
import io.intercom.android.sdk.Provider;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.api.Api;
import io.intercom.android.sdk.api.BaseCallback;
import io.intercom.android.sdk.api.ErrorObject;
import io.intercom.android.sdk.blocks.BlocksViewHolder;
import io.intercom.android.sdk.blocks.LightboxOpeningImageClickListener;
import io.intercom.android.sdk.blocks.LinkOpeningButtonClickListener;
import io.intercom.android.sdk.blocks.UploadingImageCache;
import io.intercom.android.sdk.blocks.ViewHolderGenerator;
import io.intercom.android.sdk.conversation.ReactionInputView;
import io.intercom.android.sdk.conversation.ReactionListener;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.android.sdk.metrics.MetricTracker;
import io.intercom.android.sdk.models.Avatar;
import io.intercom.android.sdk.models.Link;
import io.intercom.android.sdk.models.LinkResponse;
import io.intercom.android.sdk.models.Reaction;
import io.intercom.android.sdk.models.ReactionReply;
import io.intercom.android.sdk.utilities.AvatarUtils;
import io.intercom.android.sdk.utilities.BlockUtils;
import io.intercom.android.sdk.utilities.TimeFormatter;
import io.intercom.android.sdk.utilities.ViewUtils;
import io.intercom.android.sdk.views.ContentAwareScrollView;
import io.intercom.android.sdk.views.IntercomErrorView;
import io.intercom.com.bumptech.glide.c;
import io.intercom.com.bumptech.glide.i;

public class IntercomArticleActivity extends IntercomBaseActivity {
   private static final int ENTRANCE_ANIMATION_TIME_MS = 300;
   private static final int EXIT_ANIMATION_TIME_MS = 150;
   public static final String LINK_TRANSITION_KEY = "link_background";
   private static final String PARCEL_CONVERSATION_ID = "parcel_conversation_id";
   private static final String PARCEL_LINK_ID = "parcel_link_id";
   private static final String PARCEL_PART_ID = "parcel_part_id";
   Api api;
   private final BaseCallback apiCallback = new BaseCallback() {
      protected void onError(ErrorObject var1) {
         IntercomArticleActivity.this.intercomErrorView.setVisibility(0);
         IntercomArticleActivity.this.loadingView.setVisibility(8);
         IntercomArticleActivity.this.scrollView.setVisibility(8);
      }

      protected void onSuccess(LinkResponse.Builder var1) {
         IntercomArticleActivity.this.scrollView.setVisibility(0);
         IntercomArticleActivity.this.loadingView.setVisibility(8);
         IntercomArticleActivity.this.intercomErrorView.setVisibility(8);
         IntercomArticleActivity.this.updateContent(var1.build().getLink());
         IntercomArticleActivity.this.metricTracker.viewedArticle(IntercomArticleActivity.this.conversationId, IntercomArticleActivity.this.linkId);
      }
   };
   private Provider appConfigProvider;
   private TextView author;
   private ImageView avatar;
   private int avatarSize;
   private View composerLayout;
   String conversationId = "";
   private TextView description;
   private IntercomErrorView intercomErrorView;
   private LinearLayout linkContainer;
   String linkId = "";
   FrameLayout linkView;
   ProgressBar loadingView;
   MetricTracker metricTracker;
   private BlocksViewHolder noteHolder;
   private String partId = "";
   private ReactionInputView reactionComposer;
   private i requestManager;
   ContentAwareScrollView scrollView;
   private TimeFormatter timeFormatter;
   private TextView title;
   FrameLayout titleBar;
   boolean titleBarEnabled = false;
   TextView titleBarText;
   int titleSize = 0;
   private TextView updated;

   public static Intent buildIntent(Context var0, String var1, String var2) {
      return buildIntent(var0, var1, (String)null, var2);
   }

   public static Intent buildIntent(Context var0, String var1, String var2, String var3) {
      Intent var4 = new Intent(var0, IntercomArticleActivity.class);
      var4.putExtra("parcel_link_id", var1);
      if(var2 != null) {
         var4.putExtra("parcel_part_id", var2);
      }

      var4.putExtra("parcel_conversation_id", var3);
      var4.setFlags(268435456);
      return var4;
   }

   @TargetApi(19)
   private Transition enterTransition() {
      ChangeBounds var1 = new ChangeBounds();
      var1.setInterpolator(new b());
      var1.setDuration(300L);
      return var1;
   }

   private void fadeOutView(View var1) {
      var1.animate().alpha(0.0F).setDuration(150L).start();
   }

   @TargetApi(19)
   private Transition returnTransition() {
      ChangeBounds var1 = new ChangeBounds();
      var1.setInterpolator(new b());
      var1.setDuration(150L);
      return var1;
   }

   private void setAuthorSpannable(String var1) {
      SpannableStringBuilder var4 = new SpannableStringBuilder("Written by " + var1);
      int var2 = var4.length();
      int var3 = var1.length();
      var4.setSpan(new StyleSpan(1), var2 - var3, var2, 33);
      this.author.setText(var4);
   }

   private void updateContent(final Link var1) {
      AvatarUtils.createAvatar(Avatar.create(var1.getAuthor().getAvatar(), ""), this.avatar, this.avatarSize, (AppConfig)this.appConfigProvider.get(), this.requestManager);
      String var4 = var1.getDescription();
      if(TextUtils.isEmpty(var4)) {
         this.description.setVisibility(8);
      } else {
         this.description.setVisibility(0);
         this.description.setText(var4);
      }

      var4 = var1.getTitle();
      this.title.setText(var4);
      this.titleBarText.setText(var4);
      this.setAuthorSpannable(var1.getAuthor().getFirstName());
      this.updated.setText(this.timeFormatter.getUpdated(var1.getUpdatedAt()));
      LinearLayout var6 = ViewHolderGenerator.createLayoutFromBlocks(this.noteHolder, var1.getBlocks(), this);
      this.linkContainer.addView(BlockUtils.getBlockView(this.linkContainer, var6, this));
      this.titleSize = this.title.getMeasuredHeight();
      this.scrollView.setListener(new ContentAwareScrollView.Listener() {
         public void onBottomReached() {
         }

         public void onScrollChanged(int var1) {
            if(IntercomArticleActivity.this.titleBarEnabled) {
               var1 -= IntercomArticleActivity.this.titleSize;
               IntercomArticleActivity.this.titleBar.setAlpha((float)var1 / 100.0F);
               IntercomArticleActivity.this.titleBarText.setAlpha((float)(var1 - IntercomArticleActivity.this.titleBar.getHeight()) / 100.0F);
            }

         }
      });
      this.scrollView.setAlpha(0.0F);
      this.scrollView.animate().alpha(1.0F).setDuration(300L).start();
      if(!ReactionReply.isNull(var1.getReactionReply())) {
         ReactionReply var7 = var1.getReactionReply();
         int var3 = this.getResources().getDimensionPixelSize(R.dimen.intercom_link_reaction_height);
         this.scrollView.setPadding(this.scrollView.getPaddingLeft(), this.scrollView.getPaddingTop(), this.scrollView.getPaddingRight(), var3);
         this.reactionComposer.preloadReactionImages(var7, this.requestManager);
         this.reactionComposer.setUpReactions(var7, false, new ReactionListener() {
            public void onReactionSelected(Reaction var1x) {
               Injector.get().getApi().reactToLink(var1.getId(), var1x.getIndex());
               IntercomArticleActivity.this.metricTracker.sentReaction(IntercomArticleActivity.this.conversationId, IntercomArticleActivity.this.partId, var1x.getIndex(), MetricTracker.ReactionLocation.LINK);
               ((TextView)IntercomArticleActivity.this.findViewById(R.id.reaction_text)).setText(R.string.intercom_article_response);
            }
         }, this.requestManager);
         this.composerLayout.setVisibility(0);
         View var5 = this.composerLayout;
         float var2 = this.composerLayout.getY();
         var5.setY((float)var3 + var2);
         this.composerLayout.animate().setInterpolator(new OvershootInterpolator(1.0F)).translationY(0.0F).setDuration(300L).setListener(new AnimatorListenerAdapter() {
            public void onAnimationEnd(Animator var1) {
               IntercomArticleActivity.this.linkView.setClipChildren(false);
            }
         }).start();
         if(var7.getReactionIndex() != null) {
            ((TextView)this.findViewById(R.id.reaction_text)).setText(R.string.intercom_article_response);
         }
      }

      this.scrollView.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
         public void onGlobalLayout() {
            ViewUtils.removeGlobalLayoutListener(IntercomArticleActivity.this.scrollView, this);
            float var1 = (float)IntercomArticleActivity.this.linkView.getMeasuredHeight();
            if((float)IntercomArticleActivity.this.scrollView.getChildAt(0).getMeasuredHeight() > var1 * 1.5F) {
               IntercomArticleActivity.this.titleBarEnabled = true;
            }

         }
      });
   }

   void closeLink() {
      this.titleBarEnabled = false;
      this.fadeOutView(this.scrollView);
      this.fadeOutView(this.composerLayout);
      this.fadeOutView(this.titleBar);
      this.supportFinishAfterTransition();
   }

   public void onBackPressed() {
      this.metricTracker.closedArticle(this.conversationId, this.linkId, 1);
      this.closeLink();
   }

   protected void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.setContentView(R.layout.intercom_activity_article);
      var1 = this.getIntent().getExtras();
      if(var1 != null) {
         this.conversationId = var1.getString("parcel_conversation_id", "");
         this.partId = var1.getString("parcel_part_id", "");
         this.linkId = var1.getString("parcel_link_id", "");
      }

      this.requestManager = c.a((j)this);
      Injector var3 = Injector.get();
      this.appConfigProvider = var3.getAppConfigProvider();
      this.api = var3.getApi();
      this.api.getLink(this.linkId, this.apiCallback);
      this.metricTracker = var3.getMetricTracker();
      int var2 = ((AppConfig)this.appConfigProvider.get()).getBaseColor();
      this.timeFormatter = new TimeFormatter(this, var3.getTimeProvider());
      this.linkView = (FrameLayout)this.findViewById(R.id.link_view);
      this.titleBar = (FrameLayout)this.findViewById(R.id.link_title_bar);
      this.titleBarText = (TextView)this.findViewById(R.id.title_bar_text);
      this.titleBarText.setTextColor(var2);
      this.loadingView = (ProgressBar)this.findViewById(R.id.loading_view);
      this.loadingView.getIndeterminateDrawable().setColorFilter(var2, Mode.SRC_IN);
      this.title = (TextView)this.findViewById(R.id.title);
      this.title.setTextColor(var2);
      this.description = (TextView)this.findViewById(R.id.description);
      this.reactionComposer = (ReactionInputView)this.findViewById(R.id.reaction_input_view);
      this.composerLayout = this.findViewById(R.id.link_composer_container);
      this.author = (TextView)this.findViewById(R.id.author);
      this.updated = (TextView)this.findViewById(R.id.updated);
      this.avatarSize = this.getResources().getDimensionPixelSize(R.dimen.intercom_avatar_size);
      this.avatar = (ImageView)this.findViewById(R.id.avatar_view);
      this.noteHolder = (new ViewHolderGenerator(new UploadingImageCache(), this.api, this.appConfigProvider, this.conversationId, new LightboxOpeningImageClickListener(this.api), new LinkOpeningButtonClickListener(this.api), this.requestManager)).getNoteHolder();
      this.intercomErrorView = (IntercomErrorView)this.findViewById(R.id.error_layout_article);
      this.intercomErrorView.setActionButtonTextColor(var2);
      this.intercomErrorView.setActionButtonClickListener(new OnClickListener() {
         public void onClick(View var1) {
            IntercomArticleActivity.this.loadingView.setVisibility(0);
            IntercomArticleActivity.this.api.getLink(IntercomArticleActivity.this.linkId, IntercomArticleActivity.this.apiCallback);
         }
      });
      this.linkContainer = (LinearLayout)this.findViewById(R.id.link_container);
      this.scrollView = (ContentAwareScrollView)this.findViewById(R.id.scroll_view);
      this.findViewById(R.id.dismiss).setOnClickListener(new OnClickListener() {
         public void onClick(View var1) {
            IntercomArticleActivity.this.metricTracker.closedArticle(IntercomArticleActivity.this.conversationId, IntercomArticleActivity.this.linkId, 0);
            IntercomArticleActivity.this.closeLink();
         }
      });
      if(VERSION.SDK_INT >= 21) {
         this.getWindow().setSharedElementEnterTransition(this.enterTransition());
         this.getWindow().setSharedElementReturnTransition(this.returnTransition());
         this.findViewById(R.id.link_view).setTransitionName("link_background");
      }

   }
}
