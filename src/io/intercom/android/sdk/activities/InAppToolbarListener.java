package io.intercom.android.sdk.activities;

import android.app.Activity;
import io.intercom.android.sdk.views.IntercomToolbar;

class InAppToolbarListener implements IntercomToolbar.Listener {
   private final Activity activity;

   InAppToolbarListener(Activity var1) {
      this.activity = var1;
   }

   public void onCloseClicked() {
      this.activity.onBackPressed();
   }

   public void onInboxClicked() {
   }

   public void onToolbarClicked() {
   }
}
