package io.intercom.android.sdk.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.view.View;

class FullScreenInAppPresenter {
   void closeWindow(final Activity var1) {
      View var2 = var1.getWindow().getDecorView();
      var2.setAlpha(1.0F);
      var2.animate().alpha(0.0F).setDuration(200L).setListener(new AnimatorListenerAdapter() {
         public void onAnimationEnd(Animator var1x) {
            var1.finish();
            var1.overridePendingTransition(0, 0);
         }
      }).start();
   }
}
