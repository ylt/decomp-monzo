package io.intercom.android.sdk.errorreporting;

import io.intercom.com.google.gson.a.c;
import java.util.List;

public class ErrorReport {
   @c(
      a = "exception_reports"
   )
   private final List exceptionReports;
   private final long timestamp;

   ErrorReport(List var1, long var2) {
      this.exceptionReports = var1;
      this.timestamp = var2;
   }

   public List getExceptionReports() {
      return this.exceptionReports;
   }

   public long getTimestamp() {
      return this.timestamp;
   }
}
