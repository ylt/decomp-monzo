package io.intercom.android.sdk.errorreporting;

import android.content.Context;
import io.intercom.android.sdk.Provider;
import io.intercom.com.google.gson.e;

public class ErrorReporter {
   private final Provider apiProvider;
   private final ExceptionParser exceptionParser;
   private final ErrorReportStore reportStore;

   ErrorReporter(ErrorReportStore var1, Provider var2, ExceptionParser var3) {
      this.reportStore = var1;
      this.apiProvider = var2;
      this.exceptionParser = var3;
   }

   public static ErrorReporter create(Context var0, e var1, Provider var2) {
      return new ErrorReporter(ErrorReportStore.create(var0.getApplicationContext(), var1), var2, new ExceptionParser());
   }

   public void disableExceptionHandler() {
      IntercomExceptionHandler.disable();
   }

   public void enableExceptionHandler() {
      IntercomExceptionHandler.enable(this);
   }

   void saveReport(Throwable var1) {
      if(this.exceptionParser.containsIntercomMethod(var1)) {
         this.reportStore.saveToDisk(this.exceptionParser.createReportFrom(var1));
      }

   }

   public void sendSavedReport() {
      this.reportStore.sendSavedReport(this.apiProvider);
   }
}
