package io.intercom.android.sdk.errorreporting;

import io.intercom.com.google.gson.a.c;

public class ExceptionReport {
   @c(
      a = "class_name"
   )
   private final String className;
   private final String message;
   private final String stacktrace;

   ExceptionReport(String var1, String var2, String var3) {
      this.className = var1;
      this.message = var2;
      this.stacktrace = var3;
   }

   public String getClassName() {
      return this.className;
   }

   public String getMessage() {
      return this.message;
   }

   public String getStacktrace() {
      return this.stacktrace;
   }
}
