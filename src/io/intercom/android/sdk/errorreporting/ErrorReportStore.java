package io.intercom.android.sdk.errorreporting;

import android.content.Context;
import android.os.AsyncTask;
import io.intercom.android.sdk.Provider;
import io.intercom.android.sdk.api.Api;
import io.intercom.android.sdk.logger.LumberMill;
import io.intercom.android.sdk.twig.Twig;
import io.intercom.android.sdk.utilities.IoUtils;
import io.intercom.com.google.gson.e;
import java.io.File;
import java.io.FileReader;
import java.io.Reader;
import java.util.concurrent.RejectedExecutionException;

class ErrorReportStore {
   private static final String REPORT_FILE_PATH = "/intercom-error.json";
   private static final Twig TWIG = LumberMill.getLogger();
   private final e gson;
   private final File reportFile;

   ErrorReportStore(File var1, e var2) {
      this.reportFile = var1;
      this.gson = var2;
   }

   public static ErrorReportStore create(Context var0, e var1) {
      return new ErrorReportStore(new File(var0.getCacheDir().getAbsolutePath() + "/intercom-error.json"), var1);
   }

   private void readAndSendReport(Api var1) {
      try {
         FileReader var2 = new FileReader(this.reportFile);
         var1.sendErrorReport((ErrorReport)this.gson.a((Reader)var2, (Class)ErrorReport.class));
         IoUtils.safelyDelete(this.reportFile);
      } catch (Exception var3) {
         IoUtils.safelyDelete(this.reportFile);
      }

   }

   void saveToDisk(ErrorReport param1) {
      // $FF: Couldn't be decompiled
   }

   void sendSavedReport(final Provider var1) {
      try {
         Runnable var2 = new Runnable() {
            public void run() {
               if(ErrorReportStore.this.reportFile.exists()) {
                  ErrorReportStore.this.readAndSendReport((Api)var1.get());
               }

            }
         };
         AsyncTask.execute(var2);
      } catch (RejectedExecutionException var3) {
         TWIG.internal("Couldn't queue up sending of event: " + var3);
      }

   }
}
