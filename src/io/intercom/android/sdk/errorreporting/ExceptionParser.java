package io.intercom.android.sdk.errorreporting;

import io.intercom.android.sdk.commons.utilities.TimeProvider;
import java.util.ArrayList;
import java.util.List;

class ExceptionParser {
   private static final String[] ALLOWED_PACKAGES = new String[]{"io.intercom.android.sdk.", "java.", "android.", "com.android.", "com.google."};
   private static final String INTERCOM_SDK_PACKAGE = "io.intercom.android.sdk.";
   private static final String REDACTION_LINE = "[Non Intercom/OS method]";

   private List createExceptionReports(Throwable var1) {
      ArrayList var2;
      for(var2 = new ArrayList(); var1 != null; var1 = var1.getCause()) {
         var2.add(new ExceptionReport(var1.getClass().getName(), var1.getLocalizedMessage(), this.getStacktraceString(var1.getStackTrace())));
      }

      return var2;
   }

   private String getStacktraceString(StackTraceElement[] var1) {
      StringBuilder var4 = new StringBuilder();

      for(int var2 = 0; var2 < var1.length; ++var2) {
         StackTraceElement var3 = var1[var2];
         String var5;
         if(this.isFromAllowedPackage(var3)) {
            var5 = var3.toString();
         } else {
            var5 = "[Non Intercom/OS method]";
         }

         var4.append(var5);
         if(var2 < var1.length - 1) {
            var4.append("\n");
         }
      }

      return var4.toString();
   }

   private boolean isFromAllowedPackage(StackTraceElement var1) {
      boolean var5 = false;
      String var7 = var1.getClassName();
      String[] var6 = ALLOWED_PACKAGES;
      int var3 = var6.length;
      int var2 = 0;

      boolean var4;
      while(true) {
         var4 = var5;
         if(var2 >= var3) {
            break;
         }

         if(var7.startsWith(var6[var2])) {
            var4 = true;
            break;
         }

         ++var2;
      }

      return var4;
   }

   boolean containsIntercomMethod(Throwable var1) {
      boolean var5 = false;

      while(true) {
         boolean var4 = var5;
         if(var1 == null) {
            return var4;
         }

         StackTraceElement[] var6 = var1.getStackTrace();
         int var3 = var6.length;

         for(int var2 = 0; var2 < var3; ++var2) {
            if(var6[var2].getClassName().startsWith("io.intercom.android.sdk.")) {
               var4 = true;
               return var4;
            }
         }

         var1 = var1.getCause();
      }
   }

   ErrorReport createReportFrom(Throwable var1) {
      return new ErrorReport(this.createExceptionReports(var1), TimeProvider.SYSTEM.currentTimeMillis());
   }
}
