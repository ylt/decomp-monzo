package io.intercom.android.sdk;

public interface UnreadConversationCountListener {
   void onCountUpdate(int var1);
}
