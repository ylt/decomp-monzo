package io.intercom.android.sdk;

import android.app.Application;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.os.Build.VERSION;
import android.text.TextUtils;
import io.intercom.android.sdk.actions.Actions;
import io.intercom.android.sdk.activities.IntercomMessengerActivity;
import io.intercom.android.sdk.api.Api;
import io.intercom.android.sdk.api.DeDuper;
import io.intercom.android.sdk.api.UserUpdateBatcher;
import io.intercom.android.sdk.api.UserUpdateRequest;
import io.intercom.android.sdk.exceptions.IntercomIntegrationException;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.android.sdk.identity.Registration;
import io.intercom.android.sdk.identity.UserIdentity;
import io.intercom.android.sdk.logger.LumberMill;
import io.intercom.android.sdk.metrics.MetricTracker;
import io.intercom.android.sdk.models.LastParticipatingAdmin;
import io.intercom.android.sdk.nexus.NexusClient;
import io.intercom.android.sdk.overlay.LauncherOpenBehaviour;
import io.intercom.android.sdk.overlay.OverlayManager;
import io.intercom.android.sdk.push.SystemNotificationManager;
import io.intercom.android.sdk.state.State;
import io.intercom.android.sdk.store.Selectors;
import io.intercom.android.sdk.store.Store;
import io.intercom.android.sdk.store.UnreadCountTracker;
import io.intercom.android.sdk.twig.Twig;
import io.intercom.android.sdk.user.DeviceData;
import io.intercom.android.sdk.utilities.ActivityFinisher;
import io.intercom.android.sdk.utilities.AttributeSanitiser;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

class RealIntercom extends Intercom {
   private final ActivityFinisher activityFinisher;
   private final Provider apiProvider;
   private final Provider appConfigProvider;
   private final Context context;
   private final MetricTracker metricTracker;
   private final NexusClient nexusClient;
   private final OverlayManager overlayManager;
   private final ResetManager resetManager;
   private final Store store;
   private final DeDuper superDeDuper;
   private final SystemNotificationManager systemNotificationManager;
   private final Twig twig;
   private final UnreadCountTracker unreadCountTracker;
   private final UserIdentity userIdentity;
   private final Provider userUpdateBatcher;

   RealIntercom(DeDuper var1, Provider var2, NexusClient var3, Store var4, UnreadCountTracker var5, MetricTracker var6, final Context var7, OverlayManager var8, Provider var9, UserIdentity var10, SystemNotificationManager var11, Provider var12, ResetManager var13, Twig var14, ActivityFinisher var15) {
      this.superDeDuper = var1;
      this.apiProvider = var2;
      this.nexusClient = var3;
      this.store = var4;
      this.unreadCountTracker = var5;
      this.metricTracker = var6;
      this.context = var7;
      this.overlayManager = var8;
      this.appConfigProvider = var9;
      this.userIdentity = var10;
      this.systemNotificationManager = var11;
      this.userUpdateBatcher = var12;
      this.resetManager = var13;
      this.twig = var14;
      this.activityFinisher = var15;
      switch(null.$SwitchMap$io$intercom$android$sdk$IntercomPushManager$IntercomPushIntegrationType[IntercomPushManager.getInstalledModuleType().ordinal()]) {
      case 1:
         throw new IntercomIntegrationException("Both Intercom FCM and GCM modules were included. Please include only one of these dependencies in your project.");
      case 2:
         var14.i("Enabling FCM for cloud messaging", new Object[0]);
         var11.setUpNotificationChannelsIfSupported();
         break;
      case 3:
         var14.i("Enabling GCM for cloud messaging", new Object[0]);
         var11.setUpNotificationChannelsIfSupported();
         this.setGcmSenderId();
         AsyncTask.execute(new Runnable() {
            public void run() {
               IntercomPushManager.registerGcmToken(var7);
            }
         });
         break;
      default:
         var11.deleteNotificationChannels();
         var14.internal("No push integration detected");
      }

   }

   static Intercom create(Application var0, String var1, String var2) {
      Injector.initWithAppCredentials(var0, var1, var2);
      Injector var5 = Injector.get();
      UserIdentity var3 = var5.getUserIdentity();
      Store var4 = var5.getStore();
      return new RealIntercom(var5.getDeDuper(), var5.getApiProvider(), var5.getNexusClient(), var4, new UnreadCountTracker(var4), var5.getMetricTracker(), var0, var5.getOverlayManager(), var5.getAppConfigProvider(), var3, var5.getSystemNotificationManager(), var5.getUserUpdateBatcherProvider(), var5.getResetManager(), LumberMill.getLogger(), var5.getActivityFinisher());
   }

   private void logErrorAndOpenInbox(String var1) {
      this.twig.e(var1, new Object[0]);
      this.displayConversationsList();
   }

   private void logEventWithValidation(String var1, Map var2) {
      if(TextUtils.isEmpty(var1)) {
         this.twig.e("The event name is null or empty. We can't log an event with this string.", new Object[0]);
      } else {
         ((UserUpdateBatcher)this.userUpdateBatcher.get()).submitPendingUpdate();
         ((Api)this.apiProvider.get()).logEvent(var1, var2);
      }

   }

   private boolean noUserRegistered() {
      boolean var1;
      if(this.userIdentity.identityExists() && !this.userIdentity.isSoftReset()) {
         var1 = false;
      } else {
         var1 = true;
      }

      return var1;
   }

   private void openIntercomChatPush(String var1, TaskStackBuilder var2) {
      Intent var3;
      if(var1.equals("multiple_notifications")) {
         var3 = IntercomMessengerActivity.openInbox(this.context);
      } else {
         var3 = IntercomMessengerActivity.openConversation(this.context, var1, LastParticipatingAdmin.NULL);
      }

      if(var2 != null && VERSION.SDK_INT >= 16) {
         var2.addNextIntent(var3);
         this.context.startActivities(var2.getIntents());
      } else {
         this.context.startActivity(var3);
      }

      this.metricTracker.viewedPushNotification(var1);
      this.systemNotificationManager.clear();
   }

   private void performUpdate(Map var1) {
      if(this.userIdentity.isUnidentified()) {
         AttributeSanitiser.anonymousSanitisation(var1);
      }

      if(this.superDeDuper.shouldUpdateUser(var1)) {
         this.superDeDuper.update(var1);
         Boolean var2 = (Boolean)this.store.select(Selectors.APP_IS_BACKGROUNDED);
         ((UserUpdateBatcher)this.userUpdateBatcher.get()).updateUser(new UserUpdateRequest(false, var2.booleanValue(), var1, false));
         this.twig.internal("dupe", "updated user");
      } else {
         this.twig.internal("dupe", "dropped dupe");
      }

   }

   private void setGcmSenderId() {
      String var1 = this.context.getString(R.string.intercom_gcm_sender_id);
      if(!TextUtils.isEmpty(var1)) {
         IntercomPushManager.cacheSenderId(this.context, var1);
      }

   }

   private void softRegister() {
      String var1 = DeviceData.getDeviceToken(this.context);
      if(!TextUtils.isEmpty(var1)) {
         ((Api)this.apiProvider.get()).setDeviceToken(var1);
      }

      this.nexusClient.connect(((AppConfig)this.appConfigProvider.get()).getRealTimeConfig(), true);
      this.userIdentity.softRestart();
      this.resetManager.clear();
      Runnable var2 = new Runnable() {
         public void run() {
            RealIntercom.this.overlayManager.refreshStateBecauseUserIdentityIsNotInStoreYet();
         }
      };
      if(Looper.myLooper() == Looper.getMainLooper()) {
         var2.run();
      } else {
         (new Handler(Looper.getMainLooper())).post(var2);
      }

   }

   private boolean userIsRegistered(Registration var1) {
      boolean var2;
      if(this.userIdentity.isSoftReset() && this.userIdentity.isSameUser(var1)) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   public void addUnreadConversationCountListener(UnreadConversationCountListener var1) {
      if(var1 != null) {
         this.unreadCountTracker.addListener(var1);
      }

   }

   public void displayConversationsList() {
      this.metricTracker.openedMessengerConversationList(LauncherOpenBehaviour.LauncherType.CUSTOM);
      this.context.startActivity(IntercomMessengerActivity.openInbox(this.context));
   }

   public void displayMessageComposer() {
      this.displayMessageComposer("");
   }

   public void displayMessageComposer(String var1) {
      if(this.noUserRegistered()) {
         this.logErrorAndOpenInbox("The messenger was opened but there was no user registered on this device. Please call registerUnidentifiedUser() or registerIdentifiedUser(Registration).");
      } else if(!((AppConfig)this.appConfigProvider.get()).isReceivedFromServer()) {
         this.logErrorAndOpenInbox("It appears your app has not received a successful response from Intercom. Please check you are using the correct Android app ID and API Key from the Intercom settings.");
      } else if(!((AppConfig)this.appConfigProvider.get()).isInboundMessages()) {
         this.logErrorAndOpenInbox("It appears your app is not on a plan that allows message composing. As a fallback we are calling displayConversationsList()");
      } else {
         this.metricTracker.openedMessengerNewConversation(LauncherOpenBehaviour.LauncherType.CUSTOM);
         this.context.startActivity(IntercomMessengerActivity.openComposer(this.context, var1));
      }

   }

   public void displayMessenger() {
      if(this.noUserRegistered()) {
         this.logErrorAndOpenInbox("The messenger was opened but there was no user registered on this device.Please call registerUnidentifiedUser() or registerIdentifiedUser(Registration).");
      } else {
         (new LauncherOpenBehaviour(this.appConfigProvider, this.store, LauncherOpenBehaviour.LauncherType.CUSTOM, this.metricTracker)).openMessenger(this.context);
      }

   }

   public int getUnreadConversationCount() {
      return ((State)this.store.state()).unreadConversationIds().size();
   }

   public void handlePushMessage() {
      this.handlePushMessage((TaskStackBuilder)null);
   }

   public void handlePushMessage(TaskStackBuilder var1) {
      SharedPreferences var2 = this.context.getSharedPreferences("INTERCOM_SDK_PUSH_PREFS", 0);
      String var3 = var2.getString("intercom_push_notification_path", "");
      if(TextUtils.isEmpty(var3)) {
         this.twig.internal("No Uri found");
      } else {
         this.openIntercomChatPush(var3, var1);
         var2.edit().clear().apply();
      }

   }

   public void hideMessenger() {
      this.activityFinisher.finishActivities();
   }

   public void logEvent(String var1) {
      this.logEventWithValidation(var1, Collections.emptyMap());
   }

   public void logEvent(String var1, Map var2) {
      Object var3;
      if(var2 == null) {
         this.twig.i("The metadata provided is null, logging event with no metadata", new Object[0]);
         var3 = new HashMap();
      } else {
         var3 = var2;
         if(var2.isEmpty()) {
            this.twig.i("The metadata provided is empty, logging event with no metadata", new Object[0]);
            var3 = var2;
         }
      }

      this.logEventWithValidation(var1, (Map)var3);
   }

   public void registerIdentifiedUser(Registration var1) {
      if(var1 == null) {
         this.twig.e("The registration object you passed to is null. An example successful call is registerIdentifiedUser(Registration.create().withEmail(email));", new Object[0]);
      } else if(this.userIsRegistered(var1)) {
         this.softRegister();
      } else {
         this.resetManager.hardReset();
         if(this.userIdentity.canRegisterIdentifiedUser(var1)) {
            this.userIdentity.registerIdentifiedUser(var1);
            this.nexusClient.disconnect();
            boolean var2;
            if(!((Boolean)this.store.select(Selectors.SESSION_STARTED_SINCE_LAST_BACKGROUNDED)).booleanValue()) {
               var2 = true;
            } else {
               var2 = false;
            }

            Boolean var3 = Boolean.valueOf(var2);
            Boolean var4 = (Boolean)this.store.select(Selectors.APP_IS_BACKGROUNDED);
            UserUpdateRequest var5;
            if(var1.getAttributes() != null) {
               var5 = new UserUpdateRequest(var3.booleanValue(), var4.booleanValue(), var1.getAttributes().toMap(), true);
            } else {
               var5 = new UserUpdateRequest(var3.booleanValue(), var4.booleanValue(), true);
            }

            ((UserUpdateBatcher)this.userUpdateBatcher.get()).updateUser(var5);
         } else if(this.userIdentity.registrationHasAttributes(var1)) {
            this.twig.i("We already have a registered user. Updating this user with the attributes provided.", new Object[0]);
            this.updateUser(var1.getAttributes());
         } else {
            this.twig.i("Failed to register user. We already have a registered user. If you are attempting to register a new user, call reset() before this.", new Object[0]);
         }
      }

   }

   public void registerUnidentifiedUser() {
      if(this.userIdentity.canRegisterUnidentifiedUser()) {
         this.resetManager.hardReset();
         this.userIdentity.registerUnidentifiedUser();
         this.nexusClient.disconnect();
         Boolean var2 = (Boolean)this.store.select(Selectors.APP_IS_BACKGROUNDED);
         boolean var1;
         if(!((Boolean)this.store.select(Selectors.SESSION_STARTED_SINCE_LAST_BACKGROUNDED)).booleanValue()) {
            var1 = true;
         } else {
            var1 = false;
         }

         ((UserUpdateBatcher)this.userUpdateBatcher.get()).updateUser(new UserUpdateRequest(Boolean.valueOf(var1).booleanValue(), var2.booleanValue(), true));
      } else {
         this.twig.i("Failed to register user. We already have a registered user. If you are attempting to register a new user, call reset() before this. If you are attempting to register an identified user call: registerIdentifiedUser(Registration)", new Object[0]);
      }

   }

   public void removeUnreadConversationCountListener(UnreadConversationCountListener var1) {
      this.unreadCountTracker.removeListener(var1);
   }

   public void reset() {
      if(!this.userIdentity.isSoftReset()) {
         this.resetManager.softReset();
      }

   }

   public void setBottomPadding(int var1) {
      this.store.dispatch(Actions.setBottomPadding(var1));
   }

   public void setInAppMessageVisibility(Intercom.Visibility var1) {
      this.store.dispatch(Actions.setInAppNotificationVisibility(var1));
   }

   public void setLauncherVisibility(Intercom.Visibility var1) {
      this.store.dispatch(Actions.setLauncherVisibility(var1));
   }

   public void setUserHash(String var1) {
      if(TextUtils.isEmpty(var1)) {
         this.twig.w("The user hash you sent us to verify was either null or empty, we will not be able to authenticate your requests without a valid user hash.", new Object[0]);
      } else if(this.userIdentity.getHmac().equals(var1)) {
         this.twig.i("The user hash set matches the existing user identity hash value", new Object[0]);
      } else {
         if(this.userIdentity.softUserIdentityHmacDiffers(var1)) {
            this.resetManager.hardReset();
         }

         this.userIdentity.setUserHash(var1);
      }

   }

   public void updateUser(UserAttributes var1) {
      if(var1 == null) {
         this.twig.e("updateUser method failed: the UserAttributes object provided is null", new Object[0]);
      } else {
         this.performUpdate(var1.toMap());
      }

   }
}
