package io.intercom.android.sdk.user;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Build.VERSION;
import io.intercom.android.sdk.commons.utilities.DeviceUtils;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class DeviceData {
   private static final String PREFS_DEVICE_ID = "DeviceId";
   private static final String PREFS_PUSH_TOKEN = "push_token";

   public static void cacheDeviceToken(Context var0, String var1) {
      getPreferences(var0).edit().putString("push_token", var1).apply();
   }

   public static Map generateDeviceData(Context var0) {
      HashMap var1 = new HashMap();
      var1.put("platform_version", VERSION.RELEASE);
      var1.put("platform", Build.MODEL);
      var1.put("browser", "Intercom-Android-SDK");
      var1.put("version", DeviceUtils.getAppVersion(var0));
      var1.put("application", DeviceUtils.getAppName(var0));
      var1.put("language", Locale.getDefault().getDisplayLanguage());
      var1.put("device_id", getCachedOrNewId(var0));
      String var2 = getPreferences(var0).getString("push_token", "");
      if(!var2.isEmpty()) {
         var1.put("device_token", var2);
      }

      return var1;
   }

   private static String getCachedOrNewId(Context var0) {
      SharedPreferences var3 = getPreferences(var0);
      String var2 = var3.getString("DeviceId", "");
      String var1 = var2;
      if(var2.isEmpty()) {
         var1 = DeviceUtils.generateDeviceId(var0);
         var3.edit().putString("DeviceId", var1).apply();
      }

      return var1;
   }

   public static String getDeviceToken(Context var0) {
      return getPreferences(var0).getString("push_token", "");
   }

   private static SharedPreferences getPreferences(Context var0) {
      return var0.getSharedPreferences("INTERCOM_SDK_DATA", 0);
   }

   public static boolean hasCachedDeviceToken(Context var0, String var1) {
      return var1.equals(getDeviceToken(var0));
   }
}
