package io.intercom.android.sdk.push;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build.VERSION;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.android.sdk.logger.LumberMill;
import io.intercom.android.sdk.twig.Twig;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class SystemNotificationManager {
   static final String ACTIONS_CHANNEL = "intercom_actions_channel";
   static final String CHAT_REPLIES_CHANNEL = "intercom_chat_replies_channel";
   static final String NEW_CHATS_CHANNEL = "intercom_new_chats_channel";
   private static final int NOTIFICATION_ID = 9999997;
   private final NotificationManager androidNotificationManager;
   final List receivedPayloads;
   private final SystemNotificationFactory systemNotificationFactory;
   private final Twig twig;

   public SystemNotificationManager(NotificationManager var1) {
      this(var1, new SystemNotificationFactory());
   }

   SystemNotificationManager(NotificationManager var1, SystemNotificationFactory var2) {
      this.receivedPayloads = Collections.synchronizedList(new ArrayList());
      this.twig = LumberMill.getLogger();
      this.androidNotificationManager = var1;
      this.systemNotificationFactory = var2;
   }

   @TargetApi(26)
   private void setUpNotificationChannels() {
      NotificationChannel var3 = new NotificationChannel("intercom_chat_replies_channel", "Chat replies", 4);
      var3.setDescription("Reply notifications from chats in this app");
      NotificationChannel var1 = new NotificationChannel("intercom_new_chats_channel", "New chats", 4);
      var1.setDescription("New chat messages sent from this app");
      NotificationChannel var2 = new NotificationChannel("intercom_actions_channel", "Actions", 4);
      var2.setDescription("Contain links to take an action in this app");
      this.androidNotificationManager.createNotificationChannels(Arrays.asList(new NotificationChannel[]{var3, var1, var2}));
   }

   public void clear() {
      if(!this.receivedPayloads.isEmpty()) {
         this.twig.i("Removing Intercom push notifications.", new Object[0]);
      }

      this.androidNotificationManager.cancel(9999997);
      this.receivedPayloads.clear();
   }

   void createNotification(PushPayload var1, Context var2, AppConfig var3) {
      if(!this.receivedPayloads.contains(var1)) {
         this.receivedPayloads.add(var1);
         Notification var4;
         if(this.receivedPayloads.size() == 1) {
            var4 = this.systemNotificationFactory.createSingleNotification(var1, var2, var3);
         } else {
            var4 = this.systemNotificationFactory.createGroupedNotification(this.receivedPayloads, var2);
         }

         this.androidNotificationManager.notify(9999997, var4);
      }

   }

   void createPushOnlyNotification(PushPayload var1, Context var2) {
      Notification var4 = this.systemNotificationFactory.createPushOnlyNotification(var1, var2);
      String var3 = var1.getPushOnlyConversationId();
      this.androidNotificationManager.notify(var3, var3.hashCode(), var4);
   }

   public void deleteNotificationChannels() {
      if(VERSION.SDK_INT >= 26) {
         this.androidNotificationManager.deleteNotificationChannel("intercom_chat_replies_channel");
         this.androidNotificationManager.deleteNotificationChannel("intercom_new_chats_channel");
         this.androidNotificationManager.deleteNotificationChannel("intercom_actions_channel");
      }

   }

   public void setUpNotificationChannelsIfSupported() {
      if(VERSION.SDK_INT >= 26) {
         this.setUpNotificationChannels();
      }

   }
}
