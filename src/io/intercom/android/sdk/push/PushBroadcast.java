package io.intercom.android.sdk.push;

import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import io.intercom.android.sdk.logger.LumberMill;
import io.intercom.android.sdk.twig.Twig;

class PushBroadcast {
   private final Uri pushData;
   private final String pushOnlyId;
   private final Twig twig = LumberMill.getLogger();

   PushBroadcast() {
      this.pushData = Uri.EMPTY;
      this.pushOnlyId = "";
   }

   PushBroadcast(Intent var1) {
      String var2 = var1.getStringExtra("push_only_convo_id");
      if(TextUtils.isEmpty(var2)) {
         this.twig.internal("The uri had no push only id");
         this.pushOnlyId = "";
      } else {
         this.pushOnlyId = var2;
      }

      Uri var3 = var1.getData();
      if(var3 == null) {
         this.twig.internal("The uri had no push data");
         this.pushData = Uri.EMPTY;
      } else {
         this.pushData = var3;
      }

   }

   Uri getPushData() {
      return this.pushData;
   }

   String getPushOnlyId() {
      return this.pushOnlyId;
   }

   boolean hasPushOnlyId() {
      boolean var1;
      if(!TextUtils.isEmpty(this.pushOnlyId)) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   boolean isEmpty() {
      boolean var1;
      if(Uri.EMPTY.equals(this.pushData) && this.pushOnlyId.isEmpty()) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }
}
