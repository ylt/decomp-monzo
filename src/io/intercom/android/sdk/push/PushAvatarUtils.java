package io.intercom.android.sdk.push;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Bitmap.Config;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import io.intercom.android.sdk.commons.utilities.ScreenUtils;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.android.sdk.utilities.AvatarUtils;
import io.intercom.android.sdk.utilities.NameUtils;

class PushAvatarUtils {
   private static final int LARGE_ICON_SIZE_DP = 48;

   private static Bitmap drawableToBitmap(Context var0, Drawable var1) {
      Bitmap var4;
      if(var1 instanceof BitmapDrawable) {
         BitmapDrawable var3 = (BitmapDrawable)var1;
         if(var3.getBitmap() != null) {
            var4 = var3.getBitmap();
            return var4;
         }
      }

      if(var1.getIntrinsicWidth() > 0 && var1.getIntrinsicHeight() > 0) {
         var4 = Bitmap.createBitmap(var1.getIntrinsicWidth(), var1.getIntrinsicHeight(), Config.ARGB_8888);
      } else {
         int var2 = ScreenUtils.dpToPx(48.0F, var0);
         var4 = Bitmap.createBitmap(var2, var2, Config.ARGB_8888);
      }

      Canvas var5 = new Canvas(var4);
      var1.setBounds(0, 0, var5.getWidth(), var5.getHeight());
      var1.draw(var5);
      return var4;
   }

   public static Bitmap getNotificationDefaultBitmap(Context var0, AppConfig var1) {
      return drawableToBitmap(var0, AvatarUtils.getDefaultDrawable(var0, var1));
   }

   public static Bitmap getNotificationInitialsBitmap(Context var0, String var1, AppConfig var2) {
      return drawableToBitmap(var0, AvatarUtils.getInitialsDrawable(NameUtils.getInitial(var1), var2));
   }
}
