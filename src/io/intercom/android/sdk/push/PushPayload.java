package io.intercom.android.sdk.push;

import android.os.Bundle;

public abstract class PushPayload {
   private static final String MESSAGE_TYPE = "message";
   private static final String PUSH_ONLY = "push_only";

   public static PushPayload create(Bundle var0) {
      int var1 = var0.getInt("intercom_priority_type", 1);
      return new AutoValue_PushPayload(stringOrEmpty(var0, "conversation_id"), stringOrEmpty(var0, "message"), stringOrEmpty(var0, "body"), stringOrEmpty(var0, "author_name"), stringOrEmpty(var0, "image_url"), stringOrEmpty(var0, "app_name"), stringOrEmpty(var0, "receiver"), stringOrEmpty(var0, "conversation_part_type"), stringOrEmpty(var0, "intercom_push_type"), stringOrEmpty(var0, "uri"), stringOrEmpty(var0, "push_only_conv_id"), stringOrEmpty(var0, "title"), var1);
   }

   private static String stringOrEmpty(Bundle var0, String var1) {
      return var0.getString(var1, "");
   }

   public abstract String getAppName();

   public abstract String getAuthorName();

   public abstract String getBody();

   public abstract String getConversationId();

   public abstract String getConversationPartType();

   public abstract String getImageUrl();

   public abstract String getIntercomPushType();

   public abstract String getMessage();

   public abstract int getPriority();

   public abstract String getPushOnlyConversationId();

   public abstract String getReceiver();

   public abstract String getTitle();

   public abstract String getUri();

   boolean isFirstPart() {
      return "message".equals(this.getConversationPartType());
   }

   boolean isIntercomPush() {
      boolean var1;
      if(!this.getIntercomPushType().isEmpty() && "intercom_sdk".equals(this.getReceiver())) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   boolean isPushOnly() {
      return "push_only".equals(this.getIntercomPushType());
   }

   String messageOrBody() {
      String var1;
      if(this.getMessage().isEmpty()) {
         var1 = this.getBody();
      } else {
         var1 = this.getMessage();
      }

      return var1;
   }
}
