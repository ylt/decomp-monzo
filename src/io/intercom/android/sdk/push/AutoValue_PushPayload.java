package io.intercom.android.sdk.push;

final class AutoValue_PushPayload extends PushPayload {
   private final String appName;
   private final String authorName;
   private final String body;
   private final String conversationId;
   private final String conversationPartType;
   private final String imageUrl;
   private final String intercomPushType;
   private final String message;
   private final int priority;
   private final String pushOnlyConversationId;
   private final String receiver;
   private final String title;
   private final String uri;

   AutoValue_PushPayload(String var1, String var2, String var3, String var4, String var5, String var6, String var7, String var8, String var9, String var10, String var11, String var12, int var13) {
      if(var1 == null) {
         throw new NullPointerException("Null conversationId");
      } else {
         this.conversationId = var1;
         if(var2 == null) {
            throw new NullPointerException("Null message");
         } else {
            this.message = var2;
            if(var3 == null) {
               throw new NullPointerException("Null body");
            } else {
               this.body = var3;
               if(var4 == null) {
                  throw new NullPointerException("Null authorName");
               } else {
                  this.authorName = var4;
                  if(var5 == null) {
                     throw new NullPointerException("Null imageUrl");
                  } else {
                     this.imageUrl = var5;
                     if(var6 == null) {
                        throw new NullPointerException("Null appName");
                     } else {
                        this.appName = var6;
                        if(var7 == null) {
                           throw new NullPointerException("Null receiver");
                        } else {
                           this.receiver = var7;
                           if(var8 == null) {
                              throw new NullPointerException("Null conversationPartType");
                           } else {
                              this.conversationPartType = var8;
                              if(var9 == null) {
                                 throw new NullPointerException("Null intercomPushType");
                              } else {
                                 this.intercomPushType = var9;
                                 if(var10 == null) {
                                    throw new NullPointerException("Null uri");
                                 } else {
                                    this.uri = var10;
                                    if(var11 == null) {
                                       throw new NullPointerException("Null pushOnlyConversationId");
                                    } else {
                                       this.pushOnlyConversationId = var11;
                                       if(var12 == null) {
                                          throw new NullPointerException("Null title");
                                       } else {
                                          this.title = var12;
                                          this.priority = var13;
                                       }
                                    }
                                 }
                              }
                           }
                        }
                     }
                  }
               }
            }
         }
      }
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(var1 != this) {
         if(var1 instanceof PushPayload) {
            PushPayload var3 = (PushPayload)var1;
            if(!this.conversationId.equals(var3.getConversationId()) || !this.message.equals(var3.getMessage()) || !this.body.equals(var3.getBody()) || !this.authorName.equals(var3.getAuthorName()) || !this.imageUrl.equals(var3.getImageUrl()) || !this.appName.equals(var3.getAppName()) || !this.receiver.equals(var3.getReceiver()) || !this.conversationPartType.equals(var3.getConversationPartType()) || !this.intercomPushType.equals(var3.getIntercomPushType()) || !this.uri.equals(var3.getUri()) || !this.pushOnlyConversationId.equals(var3.getPushOnlyConversationId()) || !this.title.equals(var3.getTitle()) || this.priority != var3.getPriority()) {
               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public String getAppName() {
      return this.appName;
   }

   public String getAuthorName() {
      return this.authorName;
   }

   public String getBody() {
      return this.body;
   }

   public String getConversationId() {
      return this.conversationId;
   }

   public String getConversationPartType() {
      return this.conversationPartType;
   }

   public String getImageUrl() {
      return this.imageUrl;
   }

   public String getIntercomPushType() {
      return this.intercomPushType;
   }

   public String getMessage() {
      return this.message;
   }

   public int getPriority() {
      return this.priority;
   }

   public String getPushOnlyConversationId() {
      return this.pushOnlyConversationId;
   }

   public String getReceiver() {
      return this.receiver;
   }

   public String getTitle() {
      return this.title;
   }

   public String getUri() {
      return this.uri;
   }

   public int hashCode() {
      return ((((((((((((this.conversationId.hashCode() ^ 1000003) * 1000003 ^ this.message.hashCode()) * 1000003 ^ this.body.hashCode()) * 1000003 ^ this.authorName.hashCode()) * 1000003 ^ this.imageUrl.hashCode()) * 1000003 ^ this.appName.hashCode()) * 1000003 ^ this.receiver.hashCode()) * 1000003 ^ this.conversationPartType.hashCode()) * 1000003 ^ this.intercomPushType.hashCode()) * 1000003 ^ this.uri.hashCode()) * 1000003 ^ this.pushOnlyConversationId.hashCode()) * 1000003 ^ this.title.hashCode()) * 1000003 ^ this.priority;
   }

   public String toString() {
      return "PushPayload{conversationId=" + this.conversationId + ", message=" + this.message + ", body=" + this.body + ", authorName=" + this.authorName + ", imageUrl=" + this.imageUrl + ", appName=" + this.appName + ", receiver=" + this.receiver + ", conversationPartType=" + this.conversationPartType + ", intercomPushType=" + this.intercomPushType + ", uri=" + this.uri + ", pushOnlyConversationId=" + this.pushOnlyConversationId + ", title=" + this.title + ", priority=" + this.priority + "}";
   }
}
