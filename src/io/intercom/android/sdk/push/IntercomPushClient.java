package io.intercom.android.sdk.push;

import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import io.intercom.android.sdk.Injector;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.android.sdk.logger.LumberMill;
import io.intercom.android.sdk.state.State;
import io.intercom.android.sdk.twig.Twig;
import io.intercom.android.sdk.utilities.ContextLocaliser;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class IntercomPushClient {
   private final PushHandler pushHandler;
   private final Twig twig;

   public IntercomPushClient() {
      this(LumberMill.getLogger(), new PushHandler());
   }

   IntercomPushClient(Twig var1, PushHandler var2) {
      this.twig = var1;
      this.pushHandler = var2;
   }

   private Bundle convertMessageMapToBundle(Map var1) {
      Bundle var2 = new Bundle(var1.size());
      Iterator var3 = var1.entrySet().iterator();

      while(var3.hasNext()) {
         Entry var4 = (Entry)var3.next();
         var2.putString((String)var4.getKey(), (String)var4.getValue());
      }

      return var2;
   }

   public void handlePush(Application var1, Bundle var2) {
      if(!this.isIntercomPush(var2)) {
         this.twig.i("The message passed to handlePush was not an Intercom push message.", new Object[0]);
      } else {
         Injector.initIfCachedCredentials(var1);
         if(Injector.isNotInitialised()) {
            this.twig.w("Push not handled because Intercom is not initialised", new Object[0]);
         } else {
            Injector var4 = Injector.get();
            Context var6 = (new ContextLocaliser(var4.getAppConfigProvider())).createLocalisedContext(var1);
            boolean var3 = ((State)var4.getStore().state()).hostAppState().isBackgrounded();
            SystemNotificationManager var5 = var4.getSystemNotificationManager();
            this.pushHandler.handlePush(var2, var4.getUserIdentity(), var5, var3, var6, var4.getMetricTracker(), (AppConfig)var4.getAppConfigProvider().get());
         }
      }

   }

   public void handlePush(Application var1, Map var2) {
      this.handlePush(var1, this.convertMessageMapToBundle(var2));
   }

   public boolean isIntercomPush(Bundle var1) {
      return this.pushHandler.isIntercomPush(var1);
   }

   public boolean isIntercomPush(Map var1) {
      return this.isIntercomPush(this.convertMessageMapToBundle(var1));
   }

   public void sendTokenToIntercom(Application var1, String var2) {
      if(TextUtils.isEmpty(var2)) {
         this.twig.e("sendTokenToIntercom() was called with a null or empty token. This user will not receive push notifications until a valid device token is sent.", new Object[0]);
      } else {
         Injector.initIfCachedCredentials(var1);
         if(Injector.isNotInitialised()) {
            this.twig.w("Token not sent because Intercom is not initialised", new Object[0]);
         } else {
            Injector var3 = Injector.get();
            if(this.pushHandler.shouldSendDeviceToken(var1, var2)) {
               this.pushHandler.sendTokenToIntercom(var1, var2, var3.getApi());
            }
         }
      }

   }
}
