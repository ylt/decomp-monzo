package io.intercom.android.sdk.push;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v4.app.ab.b;
import android.support.v4.app.ab.c;
import android.support.v4.app.ab.e;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.StyleSpan;
import io.intercom.android.sdk.Injector;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.commons.utilities.DeviceUtils;
import io.intercom.android.sdk.commons.utilities.ScreenUtils;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.android.sdk.logger.LumberMill;
import io.intercom.android.sdk.transforms.RoundTransform;
import io.intercom.android.sdk.twig.Twig;
import io.intercom.android.sdk.utilities.Phrase;
import io.intercom.com.bumptech.glide.h;
import io.intercom.com.bumptech.glide.f.f;
import io.intercom.com.bumptech.glide.load.l;
import java.util.Iterator;
import java.util.List;

class SystemNotificationFactory {
   private static final String CONVERSATION_URI = "conversation_id=%s";
   private static final String INTERCOM_SDK_GCM = "intercom_sdk_gcm";
   private static final int LARGE_ICON_SIZE_DP = 48;
   private static final Twig twig = LumberMill.getLogger();

   private static c createBuilder(Context var0, Uri var1, String var2, int var3) {
      byte var4 = 0;
      ComponentName var5 = new ComponentName(var0, IntercomPushBroadcastReceiver.class);
      Intent var7 = (new Intent("io.intercom.android.sdk.INTERCOM_PUSH_OPENED")).setComponent(var5);
      Intent var6 = (new Intent("io.intercom.android.sdk.INTERCOM_PUSH_DISMISSED")).setComponent(var5);
      String var8 = var0.getPackageName();
      Intent var13 = var0.getPackageManager().getLaunchIntentForPackage(var8);
      Intent var9;
      if(var13 == null) {
         var9 = new Intent();
         twig.e("Couldn't get launch Intent for package '" + var8 + "' - tapping on notification will do nothing", new Object[0]);
      } else {
         var13.addCategory("android.intent.category.LAUNCHER");
         var13.setData(var1);
         var13.putExtra("intercom_push_key", "intercom_sdk_gcm");
         var9 = var13;
         if(var2 != null) {
            var13.putExtra("push_only_convo_id", var2);
            var9 = var13;
         }
      }

      var7.putExtra("host_app_intent", var9);
      var6.putExtra("host_app_intent", var9);
      PendingIntent var11 = PendingIntent.getBroadcast(var0, var3, var7, 134217728);
      PendingIntent var10 = PendingIntent.getBroadcast(var0, var3, var6, 134217728);
      byte var12 = var4;
      if(DeviceUtils.hasPermission(var0, "android.permission.VIBRATE")) {
         var12 = 2;
      }

      return (new c(var0)).a(R.drawable.intercom_push_icon).a("msg").a(true).e(((AppConfig)Injector.get().getAppConfigProvider().get()).getBaseColor()).a(((AppConfig)Injector.get().getAppConfigProvider().get()).getBaseColor(), 500, 2000).a(var11).b(var10).c(var12 | 1);
   }

   private Bitmap generateAvatar(PushPayload var1, Context var2, AppConfig var3) {
      Bitmap var8;
      if(TextUtils.isEmpty(var1.getImageUrl()) && !TextUtils.isEmpty(var1.getAuthorName())) {
         var8 = PushAvatarUtils.getNotificationInitialsBitmap(var2, var1.getAuthorName(), var3);
      } else {
         BitmapDrawable var10 = new BitmapDrawable(var2.getResources(), PushAvatarUtils.getNotificationDefaultBitmap(var2, var3));

         try {
            int var4 = ScreenUtils.dpToPx(48.0F, var2);
            h var9 = io.intercom.com.bumptech.glide.c.b(var2.getApplicationContext()).c();
            f var5 = new f();
            f var6 = var5.b((Drawable)var10);
            RoundTransform var11 = new RoundTransform();
            var8 = (Bitmap)var9.a(var6.a((l)var11)).a(var1.getImageUrl()).a(var4, var4).get();
         } catch (Exception var7) {
            twig.d("Failed to retrieve the notification image", new Object[0]);
            var8 = var10.getBitmap();
         }
      }

      return var8;
   }

   private String getConversationChannelId(PushPayload var1) {
      String var2;
      if(var1.isFirstPart()) {
         var2 = "intercom_new_chats_channel";
      } else {
         var2 = "intercom_chat_replies_channel";
      }

      return var2;
   }

   Notification createGroupedNotification(List var1, Context var2) {
      Uri var4 = Uri.parse("intercom_sdk/multiple_notifications");
      Object var3;
      if(var1.size() == 1) {
         var3 = var2.getString(R.string.intercom_one_new_message);
      } else {
         var3 = Phrase.from(var2, R.string.intercom_new_messages).put("n", var1.size()).format();
      }

      e var6 = new e();
      var6.a(var2.getString(R.string.intercom_new_notifications));
      Iterator var5 = var1.iterator();

      while(var5.hasNext()) {
         PushPayload var7 = (PushPayload)var5.next();
         SpannableString var8 = new SpannableString(var7.getAuthorName() + ": " + var7.messageOrBody());
         var8.setSpan(new StyleSpan(1), 0, var7.getAuthorName().length(), 33);
         var6.b(var8);
      }

      return createBuilder(var2, var4, (String)null, -1).a(var2.getString(R.string.intercom_new_notifications)).b((CharSequence)var3).c(this.getConversationChannelId((PushPayload)var1.get(var1.size() - 1))).d(((PushPayload)var1.get(var1.size() - 1)).getPriority()).a(var6).a();
   }

   Notification createPushOnlyNotification(PushPayload var1, Context var2) {
      String var3 = var1.getPushOnlyConversationId();
      return createBuilder(var2, Uri.parse(var1.getUri()), var3, var3.hashCode()).a(this.getTitle(var2, var1)).b(var1.messageOrBody()).d(var1.getPriority()).c("intercom_actions_channel").a((new b()).a(var1.messageOrBody())).a();
   }

   Notification createSingleNotification(PushPayload var1, Context var2, AppConfig var3) {
      return createBuilder(var2, Uri.parse(String.format("intercom_sdk/conversation_id=%s", new Object[]{var1.getConversationId()})), (String)null, -1).a(this.getTitle(var2, var1)).b(var1.messageOrBody()).c(this.getConversationChannelId(var1)).d(var1.getPriority()).a(this.generateAvatar(var1, var2, var3)).a((new b()).a(var1.messageOrBody())).a();
   }

   @SuppressLint({"PrivateResource"})
   CharSequence getTitle(Context var1, PushPayload var2) {
      String var3 = var2.getTitle();
      Object var4;
      if(!var3.isEmpty()) {
         var4 = var3;
      } else {
         var3 = var2.getAuthorName();
         String var5 = var2.getAppName();
         if(!var3.isEmpty() && !var5.isEmpty()) {
            var4 = Phrase.from(var1, R.string.intercom_teammate_from_company).put("name", var3).put("company", var5).format();
         } else {
            var4 = var5;
            if(!var3.isEmpty()) {
               var4 = var3;
            }
         }
      }

      return (CharSequence)var4;
   }
}
