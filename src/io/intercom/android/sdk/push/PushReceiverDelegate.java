package io.intercom.android.sdk.push;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import io.intercom.android.sdk.api.Api;
import io.intercom.android.sdk.identity.UserIdentity;
import io.intercom.android.sdk.logger.LumberMill;
import io.intercom.android.sdk.metrics.MetricTracker;
import io.intercom.android.sdk.twig.Twig;
import io.intercom.android.sdk.utilities.IntentUtils;
import java.util.List;

class PushReceiverDelegate {
   private static final String CONSUMED_BY_INTERCOM = "consumed_by_intercom";
   private final Twig twig = LumberMill.getLogger();

   private PushBroadcast createPushFromIntent(Intent var1) {
      PushBroadcast var3 = new PushBroadcast();
      PushBroadcast var2;
      if(var1 == null) {
         this.twig.internal("Host app intent was null.");
         var2 = var3;
      } else {
         var2 = var3;
         if(this.notLaunchedFromHistory(var1)) {
            var2 = var3;
            if(this.notConsumedByIntercom(var1)) {
               var2 = var3;
               if(this.isIntercomIntent(var1.getExtras())) {
                  var2 = new PushBroadcast(var1);
                  var1.putExtra("consumed_by_intercom", true);
               }
            }
         }
      }

      return var2;
   }

   private String extractNotificationId(Uri var1) {
      List var2 = var1.getPathSegments();
      String var3;
      if(var2.isEmpty()) {
         var3 = "";
      } else {
         var3 = (String)var2.get(1);
         if(var3.equals("multiple_notifications")) {
            var3 = "multiple_notifications";
         } else {
            var3 = var3.substring(var3.lastIndexOf(61) + 1);
         }
      }

      return var3;
   }

   private Intent getLaunchActivityIntent(Context param1) {
      // $FF: Couldn't be decompiled
   }

   private void handlePushMessage(Context var1, Api var2, Uri var3, String var4, MetricTracker var5) {
      var2.markConversationAsRead(var4);
      Intent var6;
      if(Uri.EMPTY.equals(var3)) {
         this.twig.i("There was no URI in the push message. Defaulting to launch activity", new Object[0]);
         var6 = this.getLaunchActivityIntent(var1);
      } else {
         var6 = new Intent("android.intent.action.VIEW", var3);
      }

      var6.addFlags(268435456);
      var5.openedPushOnlyNotification(var4);
      IntentUtils.safelyOpenIntent(var1, var6);
   }

   private boolean isIntercomIntent(Bundle var1) {
      boolean var2;
      if(var1 != null && var1.containsKey("intercom_push_key")) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   private boolean notConsumedByIntercom(Intent var1) {
      boolean var2 = false;
      if(!var1.getBooleanExtra("consumed_by_intercom", false)) {
         var2 = true;
      }

      return var2;
   }

   private boolean notLaunchedFromHistory(Intent var1) {
      boolean var2;
      if((var1.getFlags() & 1048576) == 0) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   private void openHostAppIntent(Context var1, Intent var2, String var3) {
      var1.getSharedPreferences("INTERCOM_SDK_PUSH_PREFS", 0).edit().putString("intercom_push_notification_path", var3).apply();
      IntentUtils.safelyOpenIntent(var1, var2);
   }

   void handlePushTap(Context var1, Intent var2, Api var3, UserIdentity var4, MetricTracker var5) {
      if(var4.identityExists()) {
         Intent var7 = (Intent)var2.getParcelableExtra("host_app_intent");
         if(var7 != null) {
            PushBroadcast var9 = this.createPushFromIntent(var7);
            if(var9.isEmpty()) {
               this.twig.internal("Could not open push. No Uri and conversation id found.");
            } else {
               Uri var8 = var9.getPushData();
               String var6 = var9.getPushOnlyId();
               if(var9.hasPushOnlyId()) {
                  this.handlePushMessage(var1, var3, var8, var6, var5);
               } else {
                  this.openHostAppIntent(var1, var7, this.extractNotificationId(var8));
               }
            }
         }
      }

   }

   void trackDismiss(Intent var1, UserIdentity var2, MetricTracker var3) {
      if(var2.identityExists()) {
         var1 = (Intent)var1.getParcelableExtra("host_app_intent");
         if(var1 != null) {
            PushBroadcast var5 = new PushBroadcast(var1);
            if(var5.isEmpty()) {
               this.twig.internal("Could not track push dismiss. No Uri found");
            } else {
               boolean var4 = var5.hasPushOnlyId();
               String var6;
               if(var4) {
                  var6 = var5.getPushOnlyId();
               } else {
                  var6 = this.extractNotificationId(var5.getPushData());
               }

               var3.dismissedPushNotification(var6, var4);
            }
         }
      }

   }
}
