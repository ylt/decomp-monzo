package io.intercom.android.sdk.push;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import io.intercom.android.sdk.Injector;
import io.intercom.android.sdk.api.Api;
import io.intercom.android.sdk.identity.UserIdentity;
import io.intercom.android.sdk.logger.LumberMill;
import io.intercom.android.sdk.metrics.MetricTracker;
import io.intercom.android.sdk.twig.Twig;

public class IntercomPushBroadcastReceiver extends BroadcastReceiver {
   static final String INTERCOM_PUSH_DISMISSED = "io.intercom.android.sdk.INTERCOM_PUSH_DISMISSED";
   static final String INTERCOM_PUSH_OPENED = "io.intercom.android.sdk.INTERCOM_PUSH_OPENED";
   private static final Twig TWIG = LumberMill.getLogger();
   private final PushReceiverDelegate pushReceiverDelegate = new PushReceiverDelegate();

   public void onReceive(Context var1, Intent var2) {
      Injector.initIfCachedCredentials((Application)var1.getApplicationContext());
      if(Injector.isNotInitialised()) {
         TWIG.w("Push not handled because Intercom is not initialised", new Object[0]);
      } else {
         Injector var7 = Injector.get();
         UserIdentity var4 = var7.getUserIdentity();
         MetricTracker var5 = var7.getMetricTracker();
         String var6 = var2.getAction();
         byte var3 = -1;
         switch(var6.hashCode()) {
         case -1466442296:
            if(var6.equals("io.intercom.android.sdk.INTERCOM_PUSH_OPENED")) {
               var3 = 2;
            }
            break;
         case 233030346:
            if(var6.equals("io.intercom.android.sdk.INTERCOM_PUSH_DISMISSED")) {
               var3 = 0;
            }
         }

         switch(var3) {
         case 0:
            this.pushReceiverDelegate.trackDismiss(var2, var4, var5);
            break;
         default:
            Api var8 = var7.getApi();
            this.pushReceiverDelegate.handlePushTap(var1, var2, var8, var4, var5);
         }
      }

   }
}
