package io.intercom.android.sdk.push;

import android.content.Context;
import android.os.Bundle;
import io.intercom.android.sdk.api.Api;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.android.sdk.identity.UserIdentity;
import io.intercom.android.sdk.logger.LumberMill;
import io.intercom.android.sdk.metrics.MetricTracker;
import io.intercom.android.sdk.twig.Twig;
import io.intercom.android.sdk.user.DeviceData;

class PushHandler {
   private static final String TAG = "PushHandler";
   private final Twig twig = LumberMill.getLogger();

   private void sendNotification(Bundle var1, SystemNotificationManager var2, boolean var3, Context var4, MetricTracker var5, AppConfig var6) {
      if(var1 != null && !var1.isEmpty()) {
         this.twig.internal("PushHandler", "Notification Data Json :" + var1.getString("message"));
         PushPayload var7 = PushPayload.create(var1);
         if(var7.isIntercomPush()) {
            if(var7.isPushOnly()) {
               var5.receivedPushOnlyNotification(var7.getPushOnlyConversationId());
               var2.createPushOnlyNotification(var7, var4);
            } else if(var3) {
               var5.receivedPushNotification(var7.getConversationId());
               var2.createNotification(var7, var4, var6);
            } else {
               this.twig.i("Intercom message received but not displayed in notification bar. This happened because the host app was in the foreground.", new Object[0]);
            }
         } else {
            this.twig.i("This is not a Intercom push message", new Object[0]);
         }
      } else {
         this.twig.e("The push intent contained no data", new Object[0]);
      }

   }

   void handlePush(Bundle var1, UserIdentity var2, SystemNotificationManager var3, boolean var4, Context var5, MetricTracker var6, AppConfig var7) {
      if(var2.identityExists()) {
         this.sendNotification(var1, var3, var4, var5, var6, var7);
      } else {
         var3.clear();
         this.twig.i("Can't create push message as we have no user identity. This can be caused by messages being sent to a logged out user.", new Object[0]);
      }

   }

   boolean isIntercomPush(Bundle var1) {
      return PushPayload.create(var1).isIntercomPush();
   }

   void sendTokenToIntercom(Context var1, String var2, Api var3) {
      var3.setDeviceToken(var2);
      DeviceData.cacheDeviceToken(var1, var2);
   }

   boolean shouldSendDeviceToken(Context var1, String var2) {
      boolean var3;
      if(!DeviceData.hasCachedDeviceToken(var1, var2)) {
         var3 = true;
      } else {
         var3 = false;
      }

      return var3;
   }
}
