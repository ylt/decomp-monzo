package io.intercom.android.sdk.profile;

import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.AppBarLayout.b;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnPreDrawListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import io.intercom.android.sdk.Provider;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.android.sdk.metrics.MetricTracker;
import io.intercom.android.sdk.models.LastParticipatingAdmin;
import io.intercom.android.sdk.models.TeamPresence;
import io.intercom.com.bumptech.glide.i;
import java.util.List;

public class ProfilePresenter {
   final AppBarLayout appBarLayout;
   private final Provider appConfigProvider;
   private boolean autoOpened;
   private String conversationId = "";
   ProfilePresenter.ProfileState currentState;
   private boolean didShowUnknown;
   boolean isAnimating;
   private final MetricTracker metricTracker;
   private final b offsetListener;
   private final TeamProfilePresenter teamProfilePresenter;
   private final TeammateProfilePresenter teammateProfilePresenter;
   private final ViewGroup titleLayoutCoordinator;
   private final View toolbar;
   private boolean wasClicked;

   public ProfilePresenter(CoordinatorLayout var1, MetricTracker var2, Provider var3, i var4) {
      this.currentState = ProfilePresenter.ProfileState.IDLE;
      this.isAnimating = false;
      this.didShowUnknown = false;
      this.wasClicked = false;
      this.autoOpened = false;
      this.offsetListener = new b() {
         public void onOffsetChanged(AppBarLayout var1, int var2) {
            if(var2 == 0) {
               if(ProfilePresenter.this.currentState != ProfilePresenter.ProfileState.EXPANDED) {
                  ProfilePresenter.this.trackOpenMetric();
                  ((InputMethodManager)var1.getContext().getSystemService("input_method")).hideSoftInputFromWindow(var1.getWindowToken(), 0);
               }

               ProfilePresenter.this.isAnimating = false;
               ProfilePresenter.this.currentState = ProfilePresenter.ProfileState.EXPANDED;
            } else if(Math.abs(var2) >= var1.getTotalScrollRange()) {
               ProfilePresenter.this.isAnimating = false;
               ProfilePresenter.this.currentState = ProfilePresenter.ProfileState.COLLAPSED;
            } else {
               ProfilePresenter.this.currentState = ProfilePresenter.ProfileState.IDLE;
            }

         }
      };
      this.appConfigProvider = var3;
      this.metricTracker = var2;
      this.appBarLayout = (AppBarLayout)var1.findViewById(R.id.app_bar_layout);
      this.toolbar = var1.findViewById(R.id.profile_toolbar);
      this.titleLayoutCoordinator = (FrameLayout)var1.findViewById(R.id.profile_toolbar_coordinator);
      int var5 = ((AppConfig)var3.get()).getBaseColor();
      this.teammateProfilePresenter = new TeammateProfilePresenter(var1, var5, this, var3, var4);
      this.teamProfilePresenter = new TeamProfilePresenter(var1, var5, this, var3);
   }

   private void openProfile() {
      this.appBarLayout.a(true, true);
   }

   public void addListener(b var1) {
      this.appBarLayout.a(var1);
   }

   void applyOffsetChangedListener(final b var1) {
      this.appBarLayout.a(var1);
      this.appBarLayout.getViewTreeObserver().addOnPreDrawListener(new OnPreDrawListener() {
         public boolean onPreDraw() {
            ProfilePresenter.this.appBarLayout.getViewTreeObserver().removeOnPreDrawListener(this);
            var1.onOffsetChanged(ProfilePresenter.this.appBarLayout, 0);
            return false;
         }
      });
   }

   public void closeProfile() {
      this.appBarLayout.a(false, true);
   }

   boolean isDidShowUnknown() {
      return this.didShowUnknown;
   }

   public boolean isExpanded() {
      boolean var1;
      if(this.currentState == ProfilePresenter.ProfileState.EXPANDED) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public void onStop() {
      this.teammateProfilePresenter.stopUpdatingTime();
   }

   public void profileAutoOpened() {
      this.autoOpened = true;
      this.openProfile();
   }

   public void profileClicked() {
      this.wasClicked = true;
      this.openProfile();
   }

   public void setConversationId(String var1) {
      this.conversationId = var1;
   }

   void setDidShowUnknown(boolean var1) {
      this.didShowUnknown = var1;
   }

   public void setTeamPresence(TeamPresence var1, int var2, i var3) {
      this.teamProfilePresenter.setTeamCollapsingTitle(((AppConfig)this.appConfigProvider.get()).getName());
      if(var1.isEmpty()) {
         this.teammateProfilePresenter.setTeammateSubtitle((CharSequence)null);
      } else {
         this.toolbar.setBackgroundColor(0);
         this.appBarLayout.b(this.teammateProfilePresenter.getToolbarBehavior());
         this.teamProfilePresenter.setPresence(var1, var2, this.appBarLayout, this.teammateProfilePresenter.getRootLayout(), this.titleLayoutCoordinator, var3);
      }

   }

   public void setTeammatePresence(LastParticipatingAdmin var1, List var2, CharSequence var3, int var4) {
      this.toolbar.setBackgroundColor(0);
      this.appBarLayout.b(this.teamProfilePresenter.getToolbarBehavior());
      this.teammateProfilePresenter.setPresence(var1, var2, var3, var4, this.appBarLayout, this.currentState, this.teamProfilePresenter.getRootLayout(), this.titleLayoutCoordinator);
   }

   public void setUnknownPresence() {
      this.didShowUnknown = true;
      this.teammateProfilePresenter.getRootLayout().setAlpha(0.0F);
      this.teamProfilePresenter.getRootLayout().setAlpha(0.0F);
      this.toolbar.setBackgroundColor(((AppConfig)this.appConfigProvider.get()).getBaseColor());
      this.appBarLayout.b(this.teamProfilePresenter.getToolbarBehavior());
      this.appBarLayout.b(this.teammateProfilePresenter.getToolbarBehavior());
   }

   public void startOffsetListener() {
      this.appBarLayout.a(this.offsetListener);
   }

   void trackOpenMetric() {
      ProfilePresenter.ProfileType var1;
      if(this.teamProfilePresenter.getRootLayout().getAlpha() == 0.0F) {
         var1 = ProfilePresenter.ProfileType.TEAMMATE;
      } else {
         var1 = ProfilePresenter.ProfileType.TEAM;
      }

      if(this.wasClicked) {
         this.metricTracker.profileClickedOpen(this.conversationId, var1);
      } else if(this.autoOpened) {
         this.metricTracker.profileAutoOpen(this.conversationId, var1);
      } else {
         this.metricTracker.profileScrolledOpen(this.conversationId, var1);
      }

      this.wasClicked = false;
      this.autoOpened = false;
   }

   static enum ProfileState {
      COLLAPSED,
      EXPANDED,
      IDLE;
   }

   public static enum ProfileType {
      TEAM,
      TEAMMATE;
   }
}
