package io.intercom.android.sdk.profile;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.content.a;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Space;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;
import io.intercom.android.sdk.Provider;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.commons.utilities.ScreenUtils;
import io.intercom.android.sdk.commons.utilities.TimeProvider;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.android.sdk.models.Avatar;
import io.intercom.android.sdk.models.LastParticipatingAdmin;
import io.intercom.android.sdk.models.Location;
import io.intercom.android.sdk.models.Participant;
import io.intercom.android.sdk.models.SocialAccount;
import io.intercom.android.sdk.utilities.AvatarUtils;
import io.intercom.android.sdk.utilities.BackgroundUtils;
import io.intercom.android.sdk.utilities.GroupConversationTextFormatter;
import io.intercom.android.sdk.utilities.ImageUtils;
import io.intercom.android.sdk.utilities.IntentUtils;
import io.intercom.android.sdk.utilities.LocationFormatter;
import io.intercom.android.sdk.views.ActiveStatePresenter;
import io.intercom.com.bumptech.glide.i;
import io.intercom.com.bumptech.glide.j;
import io.intercom.com.bumptech.glide.f.f;
import io.intercom.com.bumptech.glide.load.resource.b.b;
import java.util.Date;
import java.util.List;

class TeammateProfilePresenter {
   private static final int FADE_DURATION_MS = 150;
   private final ActiveStatePresenter activeStatePresenter;
   private final View activeStateView;
   private final Provider appConfigProvider;
   private final LinearLayout avatarHolder;
   private final Drawable avatarStroke;
   private final ImageView avatarView1;
   private final ImageView avatarView2;
   private final ImageView avatarView3;
   private final ImageView backgroundImageView;
   private final TextView bioView;
   private final int borderSize;
   private final Space bottomSpacer;
   private final LinearLayout groupConversationBanner;
   Location lastAdminLocation;
   private final ImageButton linkedInButton;
   final TextView locationView;
   private final ProfilePresenter profilePresenter;
   private final i requestManager;
   private final TextView roleView;
   private final ViewGroup rootLayout;
   private final LinearLayout socialLayout;
   private final TextView subtitleView;
   private final Runnable timeUpdate = new Runnable() {
      public void run() {
         TeammateProfilePresenter.this.locationView.removeCallbacks(this);
         CharSequence var1 = LocationFormatter.getLocationString(TeammateProfilePresenter.this.locationView.getContext(), TeammateProfilePresenter.this.lastAdminLocation, new Date());
         TeammateProfilePresenter.this.setTextAndVisibility(TeammateProfilePresenter.this.locationView, var1);
         LocationFormatter.postOnNextMinute(TeammateProfilePresenter.this.locationView, this, TimeProvider.SYSTEM);
      }
   };
   private final TextView titleView;
   private final TextView titleViewNameOnly;
   private final ProfileToolbarBehavior toolbarBehavior;
   private final Space topSpacer;
   private final ImageButton twitterButton;

   TeammateProfilePresenter(CoordinatorLayout var1, int var2, ProfilePresenter var3, Provider var4, i var5) {
      this.profilePresenter = var3;
      this.appConfigProvider = var4;
      this.requestManager = var5;
      this.rootLayout = (ViewGroup)var1.findViewById(R.id.intercom_teammate_profile_container_view);
      this.rootLayout.setBackgroundColor(var2);
      this.topSpacer = (Space)this.rootLayout.findViewById(R.id.intercom_avatar_spacer);
      this.bottomSpacer = (Space)this.rootLayout.findViewById(R.id.intercom_bottom_spacer);
      this.titleView = (TextView)this.rootLayout.findViewById(R.id.intercom_collapsing_title);
      this.titleViewNameOnly = (TextView)this.rootLayout.findViewById(R.id.intercom_collapsing_title_name_only);
      this.subtitleView = (TextView)this.rootLayout.findViewById(R.id.intercom_collapsing_subtitle);
      this.roleView = (TextView)this.rootLayout.findViewById(R.id.intercom_collapsing_role);
      this.locationView = (TextView)this.rootLayout.findViewById(R.id.intercom_collapsing_location);
      this.bioView = (TextView)this.rootLayout.findViewById(R.id.intercom_collapsing_bio);
      this.avatarView1 = (ImageView)this.rootLayout.findViewById(R.id.intercom_collapsing_teammate_avatar1);
      this.avatarView2 = (ImageView)this.rootLayout.findViewById(R.id.intercom_collapsing_teammate_avatar2);
      this.avatarView3 = (ImageView)this.rootLayout.findViewById(R.id.intercom_collapsing_teammate_avatar3);
      this.activeStateView = this.rootLayout.findViewById(R.id.intercom_collapsing_teammate_active_state);
      this.twitterButton = (ImageButton)this.rootLayout.findViewById(R.id.twitter_button);
      this.linkedInButton = (ImageButton)this.rootLayout.findViewById(R.id.linkedin_button);
      this.backgroundImageView = (ImageView)var1.findViewById(R.id.collapsing_background_image);
      this.socialLayout = (LinearLayout)var1.findViewById(R.id.social_button_layout);
      this.groupConversationBanner = (LinearLayout)var1.findViewById(R.id.intercom_group_conversations_banner);
      this.avatarHolder = (LinearLayout)var1.findViewById(R.id.intercom_group_avatar_holder);
      this.toolbarBehavior = new TeammateProfileToolbarBehavior(this.rootLayout);
      this.activeStatePresenter = new ActiveStatePresenter();
      this.borderSize = ScreenUtils.dpToPx(2.0F, var1.getContext());
      this.avatarStroke = a.a(var1.getContext(), R.drawable.intercom_solid_circle);
      this.avatarStroke.setColorFilter(((AppConfig)var4.get()).getBaseColor(), Mode.MULTIPLY);
      BackgroundUtils.setBackground(this.avatarView1, this.avatarStroke);
      BackgroundUtils.setBackground(this.avatarView2, this.avatarStroke);
      BackgroundUtils.setBackground(this.avatarView3, this.avatarStroke);
      this.avatarView1.setPadding(this.borderSize, this.borderSize, this.borderSize, this.borderSize);
      this.avatarView2.setPadding(this.borderSize, this.borderSize, this.borderSize, this.borderSize);
      this.avatarView3.setPadding(this.borderSize, this.borderSize, this.borderSize, this.borderSize);
   }

   private void setGroupConversationParticipants(List var1, int var2) {
      if(var1.size() > 0) {
         this.groupConversationBanner.setVisibility(0);
      } else {
         this.groupConversationBanner.setVisibility(8);
      }

      this.avatarHolder.removeAllViews();
      Context var7 = this.avatarHolder.getContext();
      int var5 = ScreenUtils.dpToPx(8.0F, var7);
      int var3 = var7.getResources().getDimensionPixelSize(R.dimen.intercom_group_conversations_banner_padding);
      int var4 = var7.getResources().getDimensionPixelSize(R.dimen.intercom_teammate_avatar_size);
      var2 = (var2 - var3 * 2) / (var4 + var5);
      if(var2 > var1.size()) {
         var2 = var1.size();
      }

      for(var3 = 0; var3 < var2; ++var3) {
         if(var3 == var2 - 1 && var2 < var1.size()) {
            TextView var11 = new TextView(var7);
            var11.setLayoutParams(new LayoutParams(var4, var4));
            var11.setTextColor(-1);
            var11.setGravity(17);
            int var6 = var1.size();
            var11.setText("+" + (var6 - var2 + 1));
            this.avatarHolder.addView(var11);
         } else {
            Participant var10 = (Participant)var1.get(var3);
            if(var10 != Participant.NULL) {
               ImageView var8 = new ImageView(var7);
               LayoutParams var9 = new LayoutParams(var4, var4);
               var9.setMargins(0, 0, var5, 0);
               var8.setLayoutParams(var9);
               AvatarUtils.loadAvatarIntoView(var10.getAvatar(), var8, (AppConfig)this.appConfigProvider.get(), this.requestManager);
               this.avatarHolder.addView(var8);
            }
         }
      }

   }

   private void setSocialAccounts(final SocialAccount var1, final SocialAccount var2, final Context var3) {
      if(var1 == SocialAccount.NULL) {
         this.twitterButton.setVisibility(8);
      } else {
         this.twitterButton.setVisibility(0);
         this.twitterButton.setOnClickListener(new OnClickListener() {
            public void onClick(View var1x) {
               Intent var2 = new Intent("android.intent.action.VIEW");
               var2.setData(Uri.parse(var1.getProfileUrl()));
               IntentUtils.safelyOpenIntent(var3, var2);
            }
         });
      }

      if(var2 == SocialAccount.NULL) {
         this.linkedInButton.setVisibility(8);
      } else {
         this.linkedInButton.setVisibility(0);
         this.linkedInButton.setOnClickListener(new OnClickListener() {
            public void onClick(View var1) {
               Intent var2x = new Intent("android.intent.action.VIEW");
               var2x.setData(Uri.parse(var2.getProfileUrl()));
               IntentUtils.safelyOpenIntent(var3, var2x);
            }
         });
      }

      if(this.linkedInButton.getVisibility() == 8 && this.twitterButton.getVisibility() == 8) {
         this.socialLayout.setVisibility(8);
      } else {
         this.socialLayout.setVisibility(0);
      }

   }

   private void setToolbarBackground(Avatar var1, int var2) {
      String var3 = var1.getImageUrl();
      if(!TextUtils.isEmpty(var3)) {
         this.requestManager.a((Object)var3).a((j)b.c()).a((new f()).b(ImageUtils.getDiskCacheStrategy(var3)).a(this.rootLayout.getMeasuredWidth(), var2)).a(this.backgroundImageView);
      }

   }

   ViewGroup getRootLayout() {
      return this.rootLayout;
   }

   ProfileToolbarBehavior getToolbarBehavior() {
      return this.toolbarBehavior;
   }

   void setPresence(LastParticipatingAdmin var1, List var2, CharSequence var3, int var4, AppBarLayout var5, ProfilePresenter.ProfileState var6, View var7, View var8) {
      var5.b(this.toolbarBehavior);
      if(this.rootLayout.getAlpha() < 1.0F) {
         if(this.profilePresenter.isDidShowUnknown()) {
            this.rootLayout.setAlpha(1.0F);
            var7.setAlpha(0.0F);
            boolean var10;
            if(var6 == ProfilePresenter.ProfileState.EXPANDED) {
               var10 = true;
            } else {
               var10 = false;
            }

            var5.a(var10, true);
            this.profilePresenter.setDidShowUnknown(false);
         } else {
            this.rootLayout.animate().alpha(1.0F).setDuration(150L).start();
            var7.animate().alpha(0.0F).setDuration(150L).start();
         }
      }

      Context var12 = this.rootLayout.getContext();
      this.titleView.setText(GroupConversationTextFormatter.groupConversationTitle(var1.getFirstName(), var2.size(), var12));
      this.titleViewNameOnly.setText(var1.getFirstName());
      this.setTeammateSubtitle(var3);
      int var9 = var2.size();
      AvatarUtils.loadAvatarIntoView(var1.getAvatar(), this.avatarView1, (AppConfig)this.appConfigProvider.get(), this.requestManager);
      if(var9 > 0) {
         AvatarUtils.loadAvatarIntoView(((Participant)var2.get(var9 - 1)).getAvatar(), this.avatarView2, (AppConfig)this.appConfigProvider.get(), this.requestManager);
      }

      if(var9 > 1) {
         AvatarUtils.loadAvatarIntoView(((Participant)var2.get(var9 - 2)).getAvatar(), this.avatarView3, (AppConfig)this.appConfigProvider.get(), this.requestManager);
      }

      this.activeStateView.setVisibility(0);
      this.activeStatePresenter.presentStateDot(var1.isActive(), this.activeStateView, (AppConfig)this.appConfigProvider.get());
      this.setTextAndVisibility(this.roleView, var1.getJobTitle());
      this.lastAdminLocation = var1.getLocation();
      this.timeUpdate.run();
      this.setTextAndVisibility(this.bioView, var1.getIntro());
      this.setSocialAccounts(var1.getTwitter(), var1.getLinkedIn(), var12);
      var9 = var4;
      if(var4 == 0) {
         var9 = ScreenUtils.getScreenDimensions(var12).x;
      }

      this.setGroupConversationParticipants(var2, var9);
      var4 = MeasureSpec.makeMeasureSpec(var9, 1073741824);
      this.rootLayout.measure(var4, 0);
      var4 = this.rootLayout.getMeasuredHeight();
      var8.getLayoutParams().height = var4;
      var8.requestLayout();
      this.backgroundImageView.getLayoutParams().height = var4;
      Resources var11 = var12.getResources();
      this.rootLayout.getLayoutParams().height = var11.getDimensionPixelSize(R.dimen.intercom_toolbar_height);
      this.rootLayout.requestLayout();
      this.profilePresenter.applyOffsetChangedListener(this.toolbarBehavior);
      this.setToolbarBackground(var1.getAvatar(), var4);
   }

   void setTeammateSubtitle(CharSequence var1) {
      this.subtitleView.setText(var1);
   }

   void setTextAndVisibility(TextView var1, CharSequence var2) {
      if(TextUtils.isEmpty(var2)) {
         var1.setVisibility(8);
      } else {
         var1.setVisibility(0);
         var1.setText(var2);
         this.topSpacer.setVisibility(0);
      }

   }

   void stopUpdatingTime() {
      this.locationView.removeCallbacks(this.timeUpdate);
   }
}
