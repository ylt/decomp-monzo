package io.intercom.android.sdk.profile;

import android.support.design.widget.AppBarLayout;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.commons.utilities.ScreenUtils;
import io.intercom.android.sdk.utilities.ViewUtils;

class TeamProfileToolbarBehavior extends ProfileToolbarBehavior {
   private static final float CHANGE_IN_SCALE = 0.3F;
   private ImageView avatarMini1;
   private ImageView avatarMini2;
   private ImageView avatarMini3;
   private TextView bio;
   private TextView officeHours;
   private View officeHoursBanner;
   private final ViewGroup rootLayout;
   private View separator;
   private LinearLayout teamProfiles;
   private final float translate16dp;

   public TeamProfileToolbarBehavior(ViewGroup var1) {
      super(var1.getContext());
      this.rootLayout = var1;
      this.translate16dp = (float)ScreenUtils.dpToPx(16.0F, var1.getContext());
   }

   private void updateCollapsedAvatars(ViewGroup var1, float var2) {
      if(this.avatarMini1 == null) {
         this.avatarMini1 = (ImageView)var1.findViewById(R.id.intercom_collapsing_team_avatar1);
      }

      if(this.avatarMini2 == null) {
         this.avatarMini2 = (ImageView)var1.findViewById(R.id.intercom_collapsing_team_avatar2);
      }

      if(this.avatarMini3 == null) {
         this.avatarMini3 = (ImageView)var1.findViewById(R.id.intercom_collapsing_team_avatar3);
      }

      var2 = 0.7F + 0.003F * var2;
      this.avatarMini1.setScaleX(var2);
      this.avatarMini2.setScaleX(var2);
      this.avatarMini3.setScaleX(var2);
      this.avatarMini1.setScaleY(var2);
      this.avatarMini2.setScaleY(var2);
      this.avatarMini3.setScaleY(var2);
   }

   private void updateCollapsedBio(ViewGroup var1, View var2, float var3) {
      if(this.bio == null) {
         this.bio = (TextView)var1.findViewById(R.id.intercom_collapsing_team_bio);
      }

      this.setAlphaAsPercentageOfScroll(this.bio, var2, 0.7F, false);
      float var4 = this.translate16dp / 100.0F;
      this.bio.setTranslationY((float)((int)Math.max(0.0F, var4 * var3)));
   }

   private void updateOfficeHours(ViewGroup var1, View var2, float var3) {
      if(this.officeHours == null) {
         this.officeHours = (TextView)var1.findViewById(R.id.intercom_collapsing_office_hours);
      }

      if(this.separator == null) {
         this.separator = var1.findViewById(R.id.intercom_team_profile_separator);
      }

      if(this.officeHoursBanner == null) {
         this.officeHoursBanner = var1.findViewById(R.id.intercom_office_hours_banner);
      }

      this.setAlphaAsPercentageOfScroll(this.officeHours, var2, 1.0F, false);
      this.setAlphaAsPercentageOfScroll(this.separator, var2, 0.1F, false);
      this.setAlphaAsPercentageOfScroll(this.officeHoursBanner, var2, 0.2F, false);
      var3 = this.translate16dp / 100.0F * var3;
      this.officeHours.setTranslationY((float)((int)Math.max(0.0F, var3)));
      this.separator.setTranslationY((float)((int)Math.max(0.0F, var3)));
      this.officeHoursBanner.setTranslationY((float)((int)Math.max(0.0F, var3)));
   }

   private void updateTeamProfile(ViewGroup var1, View var2, float var3) {
      if(this.teamProfiles == null) {
         this.teamProfiles = (LinearLayout)var1.findViewById(R.id.intercom_team_profiles_layout);
      }

      this.setAlphaAsPercentageOfScroll(this.teamProfiles, var2, 1.0F, false);
      float var4 = this.translate16dp / 100.0F;
      this.teamProfiles.setTranslationY((float)((int)Math.max(0.0F, var4 * var3)));
   }

   public void onOffsetChanged(AppBarLayout var1, int var2) {
      if((float)var1.getHeight() > this.toolbarHeight) {
         float var3 = this.getScrollPercentage(var1);
         this.rootLayout.getLayoutParams().height = var1.getBottom();
         this.updateCollapsedAvatars(this.rootLayout, var3);
         this.updateTeamProfile(this.rootLayout, var1, var3);
         this.updateCollapsedBio(this.rootLayout, var1, var3);
         this.updateOfficeHours(this.rootLayout, var1, var3);
         ViewUtils.requestLayoutIfPossible(this.rootLayout);
      }

   }
}
