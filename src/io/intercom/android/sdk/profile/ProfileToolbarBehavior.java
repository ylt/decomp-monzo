package io.intercom.android.sdk.profile;

import android.content.Context;
import android.support.design.widget.AppBarLayout.b;
import android.view.View;
import io.intercom.android.sdk.R;

abstract class ProfileToolbarBehavior implements b {
   final float toolbarHeight;

   public ProfileToolbarBehavior(Context var1) {
      this.toolbarHeight = var1.getResources().getDimension(R.dimen.intercom_toolbar_height);
   }

   protected float getScrollPercentage(View var1) {
      float var2 = (float)var1.getHeight();
      float var3 = this.toolbarHeight;
      return ((float)var1.getBottom() - this.toolbarHeight) / (var2 - var3) * 100.0F;
   }

   protected void setAlphaAsPercentageOfScroll(View var1, View var2, float var3, boolean var4) {
      float var6 = this.getScrollPercentage(var2);
      float var5 = var6;
      if(var4) {
         var5 = 100.0F - var6;
      }

      var1.setAlpha(var5 * (var3 / 100.0F));
   }
}
