package io.intercom.android.sdk.profile;

import android.content.Context;
import android.content.res.Resources;
import android.os.Build.VERSION;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.MeasureSpec;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import io.intercom.android.sdk.Provider;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.commons.utilities.ScreenUtils;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.android.sdk.models.Avatar;
import io.intercom.android.sdk.models.Participant;
import io.intercom.android.sdk.models.TeamPresence;
import io.intercom.android.sdk.utilities.AvatarUtils;
import io.intercom.com.bumptech.glide.i;
import java.util.Arrays;
import java.util.List;

class TeamProfilePresenter {
   private final List adminNameViews;
   private final Provider appConfigProvider;
   private final LinearLayout avatarLayout;
   private final List avatarViews;
   private final TextView bioView;
   private final TextView collapsedOfficeHours;
   private final TextView collapsedToolbarTitle;
   private final ProfilePresenter profilePresenter;
   private final ViewGroup rootLayout;
   private final ProfileToolbarBehavior toolbarBehavior;

   TeamProfilePresenter(CoordinatorLayout var1, int var2, ProfilePresenter var3, Provider var4) {
      this.profilePresenter = var3;
      this.rootLayout = (ViewGroup)var1.findViewById(R.id.intercom_team_profile);
      this.appConfigProvider = var4;
      this.rootLayout.setBackgroundColor(var2);
      this.rootLayout.setAlpha(0.0F);
      this.collapsedToolbarTitle = (TextView)this.rootLayout.findViewById(R.id.intercom_collapsing_title);
      this.collapsedOfficeHours = (TextView)this.rootLayout.findViewById(R.id.intercom_collapsing_office_hours);
      this.avatarLayout = (LinearLayout)this.rootLayout.findViewById(R.id.intercom_team_profiles_layout);
      this.avatarViews = Arrays.asList(new ImageView[]{(ImageView)this.rootLayout.findViewById(R.id.intercom_collapsing_team_avatar1), (ImageView)this.rootLayout.findViewById(R.id.intercom_collapsing_team_avatar2), (ImageView)this.rootLayout.findViewById(R.id.intercom_collapsing_team_avatar3)});
      this.adminNameViews = Arrays.asList(new TextView[]{(TextView)this.rootLayout.findViewById(R.id.intercom_collapsing_team_name_1), (TextView)this.rootLayout.findViewById(R.id.intercom_collapsing_team_name_2), (TextView)this.rootLayout.findViewById(R.id.intercom_collapsing_team_name_3)});
      this.bioView = (TextView)this.rootLayout.findViewById(R.id.intercom_collapsing_team_bio);
      this.toolbarBehavior = new TeamProfileToolbarBehavior(this.rootLayout);
   }

   private void setAvatar(Avatar var1, ImageView var2, i var3) {
      var2.setVisibility(0);
      AvatarUtils.loadAvatarIntoView(var1, var2, (AppConfig)this.appConfigProvider.get(), var3);
   }

   private void setProfileTextView(TextView var1, CharSequence var2) {
      if(TextUtils.isEmpty(var2)) {
         var1.setVisibility(8);
      } else {
         var1.setVisibility(0);
         var1.setText(var2);
      }

   }

   ViewGroup getRootLayout() {
      return this.rootLayout;
   }

   ProfileToolbarBehavior getToolbarBehavior() {
      return this.toolbarBehavior;
   }

   void setPresence(TeamPresence var1, int var2, AppBarLayout var3, View var4, View var5, i var6) {
      var3.b(this.toolbarBehavior);
      if(this.rootLayout.getAlpha() < 1.0F || this.avatarLayout.getVisibility() == 8) {
         var4.setAlpha(0.0F);
         this.rootLayout.setAlpha(1.0F);
         if(this.profilePresenter.isDidShowUnknown()) {
            var5.requestLayout();
            var3.a(true, true);
            this.profilePresenter.setDidShowUnknown(false);
         }
      }

      List var12 = var1.getActiveAdmins();
      int var7;
      if(var12.isEmpty()) {
         this.avatarLayout.setVisibility(8);
      } else {
         this.avatarLayout.setVisibility(0);
         if(TextUtils.isEmpty(var1.getOfficeHours())) {
            this.rootLayout.findViewById(R.id.intercom_office_hours_banner).setVisibility(8);
            this.collapsedOfficeHours.setText(var1.getExpectedResponseDelay());
         } else {
            this.rootLayout.findViewById(R.id.intercom_team_profile_separator).setVisibility(8);
            this.collapsedOfficeHours.setText(var1.getOfficeHours());
            this.collapsedOfficeHours.setCompoundDrawablesWithIntrinsicBounds(R.drawable.intercom_snooze, 0, 0, 0);
            this.collapsedOfficeHours.setCompoundDrawablePadding(ScreenUtils.dpToPx(6.0F, this.rootLayout.getContext()));
         }

         for(var7 = 0; var7 < this.avatarViews.size() && var7 < var12.size(); ++var7) {
            ImageView var8 = (ImageView)this.avatarViews.get(var7);
            TextView var9 = (TextView)this.adminNameViews.get(var7);
            Participant var13 = (Participant)var12.get(var7);
            this.setAvatar(var13.getAvatar(), var8, var6);
            var8.setVisibility(0);
            var9.setText(var13.getForename());
            var9.setVisibility(0);
         }
      }

      this.setProfileTextView(this.bioView, ((AppConfig)this.appConfigProvider.get()).getTeamProfileBio());
      Context var10 = this.rootLayout.getContext();
      var7 = var2;
      if(var2 == 0) {
         var7 = ScreenUtils.getScreenDimensions(var10).x;
      }

      if(VERSION.SDK_INT >= 16) {
         var2 = this.collapsedOfficeHours.getMaxLines();
      } else {
         var2 = 1;
      }

      var7 = MeasureSpec.makeMeasureSpec(var7, 1073741824);
      this.rootLayout.measure(var7, 0);
      Resources var11 = var10.getResources();
      var5.getLayoutParams().height = this.rootLayout.getMeasuredHeight() + var11.getDimensionPixelSize(R.dimen.intercom_team_profile_translation_y);
      this.rootLayout.getLayoutParams().height = var11.getDimensionPixelSize(R.dimen.intercom_toolbar_height);
      var5.requestLayout();
      this.collapsedOfficeHours.setMaxLines(var2);
      this.rootLayout.requestLayout();
      this.profilePresenter.applyOffsetChangedListener(this.toolbarBehavior);
   }

   void setTeamCollapsingTitle(CharSequence var1) {
      this.collapsedToolbarTitle.setText(var1);
   }
}
