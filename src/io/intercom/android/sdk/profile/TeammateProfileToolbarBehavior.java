package io.intercom.android.sdk.profile;

import android.content.Context;
import android.content.res.Resources;
import android.support.design.widget.AppBarLayout;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.commons.utilities.ScreenUtils;
import io.intercom.android.sdk.utilities.ViewUtils;

class TeammateProfileToolbarBehavior extends ProfileToolbarBehavior {
   private static final float CHANGE_IN_AVATAR_SCALE = 0.5F;
   private static final float CHANGE_IN_TITLE_SCALE = 0.1F;
   private View activeStateCircle;
   private View avatar1;
   private View avatar2;
   private View avatar3;
   private ImageView backgroundView;
   private View bio;
   private View location;
   private final float maxAlpha;
   private final float profileTranslationY;
   private View role;
   private final ViewGroup rootLayout;
   private LinearLayout socialLayout;
   private View subtitle;
   private View title;
   private View titleNameOnly;
   private final float translate18dp;
   private final float translate8dp;

   TeammateProfileToolbarBehavior(ViewGroup var1) {
      super(var1.getContext());
      this.rootLayout = var1;
      Context var3 = var1.getContext();
      this.translate8dp = (float)ScreenUtils.dpToPx(8.0F, var3);
      this.profileTranslationY = (float)var3.getResources().getDimensionPixelSize(R.dimen.intercom_team_profile_translation_y);
      this.translate18dp = (float)ScreenUtils.dpToPx(18.0F, var3);
      TypedValue var2 = new TypedValue();
      var3.getResources().getValue(R.dimen.intercom_toolbar_image_background_alpha, var2, true);
      this.maxAlpha = var2.getFloat();
   }

   private void translateY(View var1, View var2, float var3) {
      var1.setTranslationY((float)((int)Math.max(0.0F, var3 / 100.0F * this.getScrollPercentage(var2))));
   }

   private void updateAvatar(ViewGroup var1, View var2, float var3) {
      if(this.avatar1 == null) {
         this.avatar1 = var1.findViewById(R.id.intercom_collapsing_teammate_avatar1);
      }

      if(this.avatar2 == null) {
         this.avatar2 = var1.findViewById(R.id.intercom_collapsing_teammate_avatar2);
      }

      if(this.avatar3 == null) {
         this.avatar3 = var1.findViewById(R.id.intercom_collapsing_teammate_avatar3);
      }

      if(this.activeStateCircle == null) {
         this.activeStateCircle = var1.findViewById(R.id.intercom_collapsing_teammate_active_state);
      }

      Resources var7 = var1.getContext().getResources();
      float var6 = (float)var7.getDimensionPixelSize(R.dimen.intercom_teammate_avatar_size);
      float var5 = (float)var7.getDimensionPixelSize(R.dimen.intercom_teammate_active_state_size);
      float var4 = var5 / var6;
      var5 /= var6;
      var6 = this.translate8dp / 100.0F * var3;
      this.avatar1.setTranslationX((float)((int)Math.max(var6, 0.0F)));
      this.activeStateCircle.setTranslationX((float)((int)Math.max(var6 * (1.0F - var4 + 1.0F), 0.0F)));
      var4 = this.profileTranslationY / 100.0F * var3;
      this.avatar1.setTranslationY((float)((int)Math.max(0.0F, var4)));
      this.activeStateCircle.setTranslationY((float)((int)Math.max(0.0F, (var5 + 1.0F) * var4)));
      var3 = 0.005F * var3 + 1.0F;
      this.avatar1.setScaleX(var3);
      this.avatar1.setScaleY(var3);
      this.activeStateCircle.setScaleX(var3);
      this.activeStateCircle.setScaleY(var3);
      this.setAlphaAsPercentageOfScroll(this.avatar2, var2, 1.0F, true);
      this.setAlphaAsPercentageOfScroll(this.avatar3, var2, 1.0F, true);
   }

   private void updateBackgroundImage(ViewGroup var1, View var2) {
      if(this.backgroundView == null) {
         this.backgroundView = (ImageView)var1.findViewById(R.id.collapsing_background_image);
      }

      this.setAlphaAsPercentageOfScroll(this.backgroundView, var2, this.maxAlpha, false);
   }

   private void updateBio(ViewGroup var1, View var2) {
      if(this.bio == null) {
         this.bio = var1.findViewById(R.id.intercom_collapsing_bio);
      }

      this.translateY(this.bio, var2, this.profileTranslationY);
      this.setAlphaAsPercentageOfScroll(this.bio, var2, 0.7F, false);
   }

   private void updateLocation(ViewGroup var1, View var2) {
      if(this.location == null) {
         this.location = var1.findViewById(R.id.intercom_collapsing_location);
      }

      this.translateY(this.location, var2, this.profileTranslationY);
      this.setAlphaAsPercentageOfScroll(this.location, var2, 1.0F, false);
   }

   private void updateRole(ViewGroup var1, View var2) {
      if(this.role == null) {
         this.role = var1.findViewById(R.id.intercom_collapsing_role);
      }

      this.translateY(this.role, var2, this.profileTranslationY);
      this.setAlphaAsPercentageOfScroll(this.role, var2, 1.0F, false);
   }

   private void updateSocialButtons(ViewGroup var1, View var2) {
      if(this.socialLayout == null) {
         this.socialLayout = (LinearLayout)var1.findViewById(R.id.social_button_layout);
      }

      this.translateY(this.socialLayout, var2, this.profileTranslationY);
      this.setAlphaAsPercentageOfScroll(this.socialLayout, var2, 1.0F, false);
   }

   private void updateTitle(ViewGroup var1, View var2, float var3) {
      if(this.title == null) {
         this.title = var1.findViewById(R.id.intercom_collapsing_title);
      }

      if(this.titleNameOnly == null) {
         this.titleNameOnly = var1.findViewById(R.id.intercom_collapsing_title_name_only);
      }

      if(this.subtitle == null) {
         this.subtitle = var1.findViewById(R.id.intercom_collapsing_subtitle);
      }

      this.setAlphaAsPercentageOfScroll(this.subtitle, var2, 1.0F, false);
      float var4 = (float)((int)Math.max(0.0F, (float)(ScreenUtils.dpToPx(7.0F, var2.getContext()) / 100) * var3));
      if(var4 > 0.0F) {
         this.title.setTranslationY(var4);
         this.titleNameOnly.setTranslationY(var4);
      }

      var4 = (float)((int)Math.max(0.0F, this.translate18dp / 100.0F * var3));
      if(var4 > 0.0F) {
         this.subtitle.setTranslationY(var4);
      }

      var3 = 0.001F * var3 + 1.0F;
      this.title.setPivotX(0.0F);
      this.title.setPivotY(0.0F);
      this.title.setScaleX(var3);
      this.title.setScaleY(var3);
      this.titleNameOnly.setPivotX(0.0F);
      this.titleNameOnly.setPivotY(0.0F);
      this.titleNameOnly.setScaleX(var3);
      this.titleNameOnly.setScaleY(var3);
      this.setAlphaAsPercentageOfScroll(this.title, var2, 1.0F, true);
      this.setAlphaAsPercentageOfScroll(this.titleNameOnly, var2, 1.0F, false);
   }

   public void onOffsetChanged(AppBarLayout var1, int var2) {
      if((float)var1.getHeight() > this.toolbarHeight) {
         float var3 = this.getScrollPercentage(var1);
         this.rootLayout.getLayoutParams().height = var1.getBottom();
         this.updateAvatar(this.rootLayout, var1, var3);
         this.updateTitle(this.rootLayout, var1, var3);
         this.updateRole(this.rootLayout, var1);
         this.updateLocation(this.rootLayout, var1);
         this.updateBio(this.rootLayout, var1);
         this.updateSocialButtons(this.rootLayout, var1);
         this.updateBackgroundImage(this.rootLayout, var1);
         ViewUtils.requestLayoutIfPossible(this.rootLayout);
      }

   }
}
