package io.intercom.android.sdk.profile;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.view.View.MeasureSpec;

public class TeammateProfileBackgroundImageView extends AppCompatImageView {
   public TeammateProfileBackgroundImageView(Context var1) {
      super(var1);
   }

   public TeammateProfileBackgroundImageView(Context var1, AttributeSet var2) {
      super(var1, var2);
   }

   public TeammateProfileBackgroundImageView(Context var1, AttributeSet var2, int var3) {
      super(var1, var2, var3);
   }

   protected void onMeasure(int var1, int var2) {
      Drawable var5 = this.getDrawable();
      if(var5 != null) {
         int var3 = MeasureSpec.getSize(var1);
         int var4 = var5.getIntrinsicWidth();
         if(var4 > 0) {
            this.setMeasuredDimension(var3, var5.getIntrinsicHeight() * var3 / var4);
         } else {
            super.onMeasure(var1, var2);
         }
      } else {
         super.onMeasure(var1, var2);
      }

   }
}
