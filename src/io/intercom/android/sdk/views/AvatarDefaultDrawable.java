package io.intercom.android.sdk.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Paint.Style;
import android.graphics.drawable.Drawable;
import android.support.v4.content.a;
import io.intercom.android.sdk.R;

public class AvatarDefaultDrawable extends Drawable {
   private final Paint avatarBackground;
   private final Drawable companyIcon;
   private final RectF roundRect = new RectF();

   public AvatarDefaultDrawable(Context var1, int var2) {
      this.companyIcon = a.a(var1, R.drawable.intercom_default_avatar_icon);
      this.avatarBackground = new Paint();
      this.avatarBackground.setAntiAlias(true);
      this.avatarBackground.setColor(var2);
      this.avatarBackground.setStyle(Style.FILL);
   }

   public void draw(Canvas var1) {
      this.roundRect.set(this.getBounds());
      var1.drawRoundRect(this.roundRect, (float)(var1.getHeight() / 2), (float)(var1.getHeight() / 2), this.avatarBackground);
      int var2 = (var1.getWidth() - this.companyIcon.getIntrinsicWidth()) / 2;
      int var3 = (var1.getHeight() - this.companyIcon.getIntrinsicHeight()) / 2;
      this.companyIcon.setBounds(var2, var3, this.companyIcon.getIntrinsicWidth() + var2, this.companyIcon.getIntrinsicHeight() + var3);
      this.companyIcon.draw(var1);
   }

   public int getOpacity() {
      return -3;
   }

   public void setAlpha(int var1) {
   }

   public void setColorFilter(ColorFilter var1) {
   }
}
