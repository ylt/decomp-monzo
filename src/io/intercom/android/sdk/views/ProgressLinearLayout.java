package io.intercom.android.sdk.views;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.Animator.AnimatorListener;
import android.content.Context;
import android.util.AttributeSet;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import io.intercom.android.sdk.conversation.UploadProgressListener;
import io.intercom.android.sdk.logger.LumberMill;
import io.intercom.android.sdk.twig.Twig;

public class ProgressLinearLayout extends LinearLayout implements UploadProgressListener {
   ImageView attachmentIcon;
   private final Twig twig;
   UploadProgressBar uploadProgressBar;

   public ProgressLinearLayout(Context var1) {
      this(var1, (AttributeSet)null);
   }

   public ProgressLinearLayout(Context var1, AttributeSet var2) {
      super(var1, var2);
      this.twig = LumberMill.getLogger();
   }

   public void setAttachmentIcon(ImageView var1) {
      this.attachmentIcon = var1;
   }

   public void setUploadProgressBar(UploadProgressBar var1) {
      this.uploadProgressBar = var1;
   }

   public void uploadNotice(final byte var1) {
      this.twig.internal("progress", "" + var1);
      this.post(new Runnable() {
         public void run() {
            if(ProgressLinearLayout.this.uploadProgressBar != null) {
               ProgressLinearLayout.this.uploadProgressBar.setProgress(var1);
               if(var1 == 90) {
                  ProgressLinearLayout.this.uploadProgressBar.smoothEndAnimation(new AnimatorListener() {
                     public void onAnimationCancel(Animator var1x) {
                     }

                     public void onAnimationEnd(Animator var1x) {
                        ProgressLinearLayout.this.uploadProgressBar.hideBar();
                        if(ProgressLinearLayout.this.attachmentIcon != null) {
                           ProgressLinearLayout.this.attachmentIcon.setVisibility(0);
                           ObjectAnimator var2 = ObjectAnimator.ofFloat(ProgressLinearLayout.this.attachmentIcon, "alpha", new float[]{0.0F, 1.0F});
                           var2.setDuration(300L);
                           var2.setInterpolator(new DecelerateInterpolator());
                           var2.start();
                        }

                     }

                     public void onAnimationRepeat(Animator var1x) {
                     }

                     public void onAnimationStart(Animator var1x) {
                     }
                  });
               }
            }

         }
      });
   }

   public void uploadStarted() {
      this.uploadProgressBar.smoothStartAnimation();
   }

   public void uploadStopped() {
      this.uploadProgressBar.hideBar();
   }
}
