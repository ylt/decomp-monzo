package io.intercom.android.sdk.views;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import io.intercom.android.sdk.R;
import java.util.concurrent.TimeUnit;

public class AdminIsTypingView extends LinearLayout {
   private static final int ANIMATION_DELAY_MS = 100;
   private static final int ANIMATION_DURATION_MS = 200;
   private static final float FADED_ALPHA = 0.7F;
   private static final int IS_TYPING_DURATION_SECONDS = 10;
   private static final float SMALL_SCALE = 0.4F;
   final Runnable animateDots;
   boolean animating;
   final ImageView[] dots;
   final Animator[] dotsAnimations;
   final Runnable endAnimation;
   private AdminIsTypingView.Listener listener;

   public AdminIsTypingView(Context var1) {
      this(var1, (AttributeSet)null);
   }

   public AdminIsTypingView(Context var1, AttributeSet var2) {
      super(var1, var2);
      this.dots = new ImageView[3];
      this.dotsAnimations = new Animator[3];
      this.animating = false;
      this.animateDots = new Runnable() {
         public void run() {
            for(int var1 = 0; var1 < AdminIsTypingView.this.dots.length; ++var1) {
               AdminIsTypingView.this.dotsAnimations[var1].start();
            }

            AdminIsTypingView.this.postDelayed(AdminIsTypingView.this.animateDots, TimeUnit.SECONDS.toMillis(1L));
         }
      };
      this.endAnimation = new Runnable() {
         public void run() {
            int var1 = 0;
            AdminIsTypingView.this.animating = false;
            AdminIsTypingView.this.removeCallbacks(AdminIsTypingView.this.animateDots);
            AdminIsTypingView.this.removeCallbacks(AdminIsTypingView.this.endAnimation);
            if(AdminIsTypingView.this.listener != null) {
               AdminIsTypingView.this.listener.onAdminTypingAnimationEnded(AdminIsTypingView.this);
            }

            Animator[] var3 = AdminIsTypingView.this.dotsAnimations;

            for(int var2 = var3.length; var1 < var2; ++var1) {
               var3[var1].cancel();
            }

         }
      };
      inflate(this.getContext(), R.layout.intercom_admin_is_typing, this);
      this.dots[0] = (ImageView)this.findViewById(R.id.dot1);
      this.dots[1] = (ImageView)this.findViewById(R.id.dot2);
      this.dots[2] = (ImageView)this.findViewById(R.id.dot3);
      PropertyValuesHolder var7 = PropertyValuesHolder.ofFloat(View.SCALE_X, new float[]{0.4F, 1.0F});
      PropertyValuesHolder var4 = PropertyValuesHolder.ofFloat(View.SCALE_Y, new float[]{0.4F, 1.0F});
      PropertyValuesHolder var6 = PropertyValuesHolder.ofFloat(View.ALPHA, new float[]{0.7F, 1.0F});

      for(int var3 = 0; var3 < this.dotsAnimations.length; ++var3) {
         ObjectAnimator var5 = ObjectAnimator.ofPropertyValuesHolder(this.dots[var3], new PropertyValuesHolder[]{var6, var7, var4});
         var5.setRepeatCount(1);
         var5.setRepeatMode(2);
         var5.setDuration(200L);
         var5.setStartDelay((long)(var3 * 100));
         this.dotsAnimations[var3] = var5;
      }

      this.setupEndCondition();
   }

   private void setupEndCondition() {
      this.postDelayed(this.endAnimation, TimeUnit.SECONDS.toMillis(10L));
   }

   public void beginAnimation() {
      if(!this.animating) {
         this.animating = true;
         this.animateDots.run();
      }

   }

   public void cancelTypingAnimation() {
      this.endAnimation.run();
   }

   public void renewTypingAnimation() {
      this.removeCallbacks(this.endAnimation);
      this.setupEndCondition();
   }

   public void setListener(AdminIsTypingView.Listener var1) {
      this.listener = var1;
   }

   public interface Listener {
      void onAdminTypingAnimationEnded(AdminIsTypingView var1);
   }
}
