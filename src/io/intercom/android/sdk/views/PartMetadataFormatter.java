package io.intercom.android.sdk.views;

import android.content.res.Resources;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.models.Part;
import io.intercom.android.sdk.utilities.TimeFormatter;

public class PartMetadataFormatter {
   private final TimeFormatter timeFormatter;

   public PartMetadataFormatter(TimeFormatter var1) {
      this.timeFormatter = var1;
   }

   private String formatLastPart(Part var1, Resources var2) {
      StringBuilder var3 = new StringBuilder();
      if(var1.isDisplayDelivered()) {
         var3.append(var2.getString(R.string.intercom_delivered));
      } else {
         var3.append(this.timeFormatter.getFormattedTime(var1.getCreatedAt()));
      }

      if("seen".equalsIgnoreCase(var1.getSeenByAdmin())) {
         var3.append(". ").append(var2.getString(R.string.intercom_message_seen));
      } else if("unseen".equalsIgnoreCase(var1.getSeenByAdmin())) {
         var3.append(". ").append(var2.getString(R.string.intercom_message_unseen));
      }

      return var3.toString();
   }

   private String formatPart(Part var1) {
      String var2;
      if(var1.isDisplayDelivered()) {
         var2 = "";
      } else {
         var2 = this.timeFormatter.getAbsoluteTime(var1.getCreatedAt());
      }

      return var2;
   }

   public String getMetadataString(Part var1, boolean var2, Resources var3) {
      String var4;
      switch(null.$SwitchMap$io$intercom$android$sdk$models$Part$MessageState[var1.getMessageState().ordinal()]) {
      case 1:
         var4 = var3.getString(R.string.intercom_message_state_sending);
         break;
      case 2:
      case 3:
         var4 = var3.getString(R.string.intercom_message_failed_try_again);
         break;
      default:
         if(var2) {
            var4 = this.formatLastPart(var1, var3);
         } else {
            var4 = this.formatPart(var1);
         }
      }

      return var4;
   }
}
