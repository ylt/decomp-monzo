package io.intercom.android.sdk.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.utilities.FontUtils;

public class IntercomErrorView extends RelativeLayout {
   private Button actionButton;
   private TextView subtitle;
   private TextView titleView;

   public IntercomErrorView(Context var1) {
      this(var1, (AttributeSet)null);
   }

   public IntercomErrorView(Context var1, AttributeSet var2) {
      super(var1, var2);
   }

   protected void onFinishInflate() {
      super.onFinishInflate();
      this.titleView = (TextView)this.findViewById(R.id.empty_text_title);
      FontUtils.setRobotoMediumTypeface(this.titleView);
      this.subtitle = (TextView)this.findViewById(R.id.empty_text_subtitle);
      this.actionButton = (Button)this.findViewById(R.id.action_button);
   }

   public void setActionButtonClickListener(OnClickListener var1) {
      this.actionButton.setOnClickListener(var1);
   }

   public void setActionButtonText(int var1) {
      this.actionButton.setText(var1);
   }

   public void setActionButtonTextColor(int var1) {
      this.actionButton.setTextColor(var1);
   }

   public void setActionButtonVisibility(int var1) {
      this.actionButton.setVisibility(var1);
   }

   public void setSubtitle(int var1) {
      this.subtitle.setText(var1);
   }

   public void setSubtitle(CharSequence var1) {
      this.subtitle.setText(var1);
   }

   public void setTitle(int var1) {
      this.titleView.setText(var1);
   }
}
