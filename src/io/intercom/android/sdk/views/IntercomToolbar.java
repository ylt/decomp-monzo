package io.intercom.android.sdk.views;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.support.v4.content.a;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.android.sdk.models.Participant;
import io.intercom.android.sdk.utilities.AvatarUtils;
import io.intercom.com.bumptech.glide.i;

public class IntercomToolbar extends Toolbar implements OnClickListener, OnTouchListener {
   private static final int TITLE_FADE_DURATION_MS = 150;
   private final View activeStateView;
   private final ImageView avatar;
   private final ImageButton backButton;
   private final BackButtonCountDrawable backButtonCountDrawable;
   private final ImageButton close;
   private IntercomToolbar.Listener listener;
   private final TextView subtitle;
   final TextView title;

   public IntercomToolbar(Context var1) {
      this(var1, (AttributeSet)null);
   }

   public IntercomToolbar(Context var1, AttributeSet var2) {
      super(var1, var2);
      View var3 = inflate(var1, R.layout.intercom_toolbar, this);
      var3.setOnTouchListener(this);
      this.title = (TextView)var3.findViewById(R.id.intercom_toolbar_title);
      this.subtitle = (TextView)var3.findViewById(R.id.intercom_toolbar_subtitle);
      this.close = (ImageButton)var3.findViewById(R.id.intercom_toolbar_close);
      this.backButton = (ImageButton)var3.findViewById(R.id.intercom_toolbar_inbox);
      this.avatar = (ImageView)var3.findViewById(R.id.intercom_toolbar_avatar);
      this.activeStateView = var3.findViewById(R.id.intercom_toolbar_avatar_active_state);
      this.backButtonCountDrawable = new BackButtonCountDrawable(this.getContext(), (String)null);
      this.backButton.setImageDrawable(this.backButtonCountDrawable);
      this.backButton.setOnClickListener(this);
      this.close.setOnClickListener(this);
   }

   public void fadeOutTitle(int var1) {
      this.title.animate().alpha(0.0F).setDuration((long)var1).setListener(new AnimatorListenerAdapter() {
         public void onAnimationEnd(Animator var1) {
            IntercomToolbar.this.title.setText((CharSequence)null);
         }
      }).start();
   }

   public void onClick(View var1) {
      if(this.listener != null) {
         int var2 = var1.getId();
         if(var2 == R.id.intercom_toolbar_close) {
            this.listener.onCloseClicked();
         } else if(var2 == R.id.intercom_toolbar_inbox) {
            this.listener.onInboxClicked();
         }
      }

   }

   public boolean onTouch(View var1, MotionEvent var2) {
      if(this.listener != null && var2.getAction() == 1 && var1.getId() == R.id.intercom_toolbar) {
         this.listener.onToolbarClicked();
      }

      return false;
   }

   public void setCloseButtonVisibility(int var1) {
      this.close.setVisibility(var1);
   }

   public void setInboxButtonVisibility(int var1) {
      this.backButton.setVisibility(var1);
   }

   public void setListener(IntercomToolbar.Listener var1) {
      this.listener = var1;
   }

   public void setSubtitle(CharSequence var1) {
      this.subtitle.setText(var1);
      if(!TextUtils.isEmpty(var1)) {
         this.subtitle.setAlpha(1.0F);
      }

   }

   public void setSubtitleVisibility(int var1) {
      this.subtitle.setVisibility(var1);
   }

   public void setTitle(CharSequence var1) {
      if(TextUtils.isEmpty(var1)) {
         this.fadeOutTitle(150);
      } else {
         this.title.setAlpha(1.0F);
         this.title.setText(var1);
      }

   }

   public void setUnreadCount(Integer var1) {
      BackButtonCountDrawable var2 = this.backButtonCountDrawable;
      String var3;
      if(var1.intValue() > 0) {
         var3 = Integer.toString(var1.intValue());
      } else {
         var3 = null;
      }

      var2.setText(var3);
   }

   public void setUpNoteToolbar(Participant var1, boolean var2, ActiveStatePresenter var3, AppConfig var4, i var5) {
      this.title.setTextColor(a.c(this.getContext(), R.color.intercom_note_title_grey));
      this.subtitle.setTextColor(a.c(this.getContext(), R.color.intercom_note_grey));
      this.close.setColorFilter(a.c(this.getContext(), R.color.intercom_grey_500));
      this.backButton.setVisibility(8);
      this.avatar.setVisibility(0);
      this.activeStateView.setVisibility(0);
      this.title.setTextSize(14.0F);
      this.subtitle.setAlpha(1.0F);
      View var6 = this.findViewById(R.id.intercom_toolbar_divider);
      var6.setVisibility(0);
      var6.setBackgroundColor(-16777216);
      AvatarUtils.loadAvatarIntoView(var1.getAvatar(), this.avatar, var4, var5);
      var3.presentStateDot(var2, this.activeStateView, var4);
   }

   public void setUpPostToolbar(Participant var1, boolean var2, ActiveStatePresenter var3, AppConfig var4, i var5) {
      this.setBackgroundColor(0);
      this.title.setTextColor(-1);
      this.close.setColorFilter(-1);
      this.backButton.setVisibility(8);
      this.avatar.setVisibility(0);
      this.activeStateView.setVisibility(0);
      this.title.setTextSize(14.0F);
      this.subtitle.setAlpha(0.7F);
      this.findViewById(R.id.intercom_toolbar_divider).setVisibility(0);
      AvatarUtils.loadAvatarIntoView(var1.getAvatar(), this.avatar, var4, var5);
      var3.presentStateDot(var2, this.activeStateView, var4);
   }

   public interface Listener {
      void onCloseClicked();

      void onInboxClicked();

      void onToolbarClicked();
   }
}
