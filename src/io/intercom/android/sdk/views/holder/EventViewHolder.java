package io.intercom.android.sdk.views.holder;

import android.support.v7.widget.RecyclerView.w;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import io.intercom.android.sdk.Provider;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.android.sdk.models.Avatar;
import io.intercom.android.sdk.models.EventData;
import io.intercom.android.sdk.models.EventParticipant;
import io.intercom.android.sdk.models.Part;
import io.intercom.android.sdk.utilities.AvatarUtils;
import io.intercom.com.bumptech.glide.i;

public class EventViewHolder extends w implements ConversationPartViewHolder {
   private final Provider appConfigProvider;
   private final ImageView avatar;
   private final TextView eventTitle;
   private final i requestManager;

   public EventViewHolder(View var1, Provider var2, i var3) {
      super(var1);
      this.appConfigProvider = var2;
      this.requestManager = var3;
      this.eventTitle = (TextView)var1.findViewById(R.id.event_name);
      this.avatar = (ImageView)var1.findViewById(R.id.avatar);
   }

   public void bind(Part var1, ViewGroup var2) {
      EventData var4 = var1.getEventData();
      EventParticipant var3 = var4.getParticipant();
      this.eventTitle.setText(var4.getEventAsPlainText());
      AvatarUtils.loadAvatarIntoView(Avatar.create(var3.getAvatar().getImageUrl(), var3.getInitial()), this.avatar, (AppConfig)this.appConfigProvider.get(), this.requestManager);
   }
}
