package io.intercom.android.sdk.views.holder;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.support.v7.widget.RecyclerView.w;
import android.text.SpannableStringBuilder;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.ImageView;
import android.widget.Toast;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.blocks.models.Block;
import io.intercom.android.sdk.commons.utilities.HtmlCompat;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.android.sdk.logger.LumberMill;
import io.intercom.android.sdk.models.Part;
import io.intercom.android.sdk.models.Participant;
import io.intercom.android.sdk.twig.Twig;
import io.intercom.android.sdk.utilities.AvatarUtils;
import io.intercom.android.sdk.views.AdminIsTypingView;
import io.intercom.android.sdk.views.ExpandableLayout;
import io.intercom.com.bumptech.glide.i;
import java.util.Iterator;

abstract class BlocksPartViewHolder extends w implements OnClickListener, OnLongClickListener, ConversationPartViewHolder {
   final ExpandableLayout bubble;
   protected final ViewGroup cellLayout;
   private final ClipboardManager clipboardManager;
   private final String clipboardMessage;
   final ConversationListener conversationListener;
   final ImageView networkAvatar;
   private final Twig twig = LumberMill.getLogger();

   BlocksPartViewHolder(View var1, ConversationListener var2, ClipboardManager var3) {
      super(var1);
      this.conversationListener = var2;
      this.clipboardManager = var3;
      this.clipboardMessage = var1.getContext().getString(R.string.intercom_copied_to_clipboard);
      this.networkAvatar = (ImageView)var1.findViewById(R.id.avatarView);
      this.cellLayout = (ViewGroup)var1.findViewById(R.id.cellLayout);
      this.bubble = (ExpandableLayout)var1.findViewById(R.id.intercom_bubble);
      var1.setOnClickListener(this);
      var1.setOnLongClickListener(this);
   }

   private static void appendLine(SpannableStringBuilder var0, CharSequence var1) {
      if(var1 != null && var1.length() != 0) {
         if(var0.length() > 0) {
            var0.append('\n');
         }

         var0.append(var1);
      }

   }

   static String getPartText(Part var0) {
      SpannableStringBuilder var1 = new SpannableStringBuilder();
      Iterator var2 = var0.getBlocks().iterator();

      while(true) {
         while(var2.hasNext()) {
            Block var3 = (Block)var2.next();
            switch(null.$SwitchMap$io$intercom$android$blocks$BlockType[var3.getType().ordinal()]) {
            case 1:
            case 2:
            case 3:
            case 4:
               appendLine(var1, HtmlCompat.fromHtml(var3.getText()));
               break;
            case 5:
               appendLine(var1, var3.getUrl());
               break;
            case 6:
            case 7:
               Iterator var5 = var3.getItems().iterator();

               while(var5.hasNext()) {
                  appendLine(var1, (String)var5.next());
               }
            }
         }

         String var4;
         if(var1.length() == 0) {
            var4 = var0.getSummary();
         } else {
            var4 = var1.toString();
         }

         return var4;
      }
   }

   private void setUpRowFocusRules(View var1, Part var2, ExpandableLayout var3) {
      if(var3 != null) {
         var3.setAlpha(1.0F);
         if(Part.MessageState.FAILED == var2.getMessageState() || Part.MessageState.UPLOAD_FAILED == var2.getMessageState()) {
            ((ViewGroup)var1).setDescendantFocusability(393216);
         }
      }

   }

   void checkForEntranceAnimation(int var1, Part var2, ImageView var3, View var4, ViewGroup var5) {
      if(var2.hasEntranceAnimation()) {
         if(var1 == 5) {
            var2.setEntranceAnimation(false);
            var3.setAlpha(0.0F);
            var3.setScaleX(0.5F);
            var3.setScaleY(0.5F);
            var3.animate().alpha(1.0F).scaleX(1.0F).scaleY(1.0F).setDuration(300L).setStartDelay(100L).start();
            var4.setAlpha(0.0F);
            var4.setScaleX(0.5F);
            var4.setScaleY(0.5F);
            var4.animate().alpha(1.0F).scaleX(1.0F).scaleY(1.0F).setDuration(300L).setStartDelay(150L).start();
            ((AdminIsTypingView)var5.getChildAt(0)).beginAnimation();
         } else if(Part.MessageState.SENDING == var2.getMessageState()) {
            var2.setEntranceAnimation(false);
            var4.setAlpha(0.0F);
            var4.setTranslationY(var4.getTranslationY() + 100.0F);
            var4.animate().setStartDelay(100L).alpha(1.0F).translationYBy(-100.0F).start();
         }
      }

   }

   public boolean onLongClick(View var1) {
      boolean var4 = false;
      boolean var3;
      if(this.clipboardManager == null) {
         var3 = var4;
      } else {
         int var2 = this.getAdapterPosition();
         var3 = var4;
         if(var2 != -1) {
            try {
               Part var6 = this.conversationListener.getPart(var2);
               this.clipboardManager.setPrimaryClip(ClipData.newPlainText("message", getPartText(var6)));
               Toast.makeText(this.itemView.getContext(), this.clipboardMessage, 0).show();
            } catch (Exception var5) {
               this.twig.internal(var5.getMessage());
               var3 = var4;
               return var3;
            }

            var3 = true;
         }
      }

      return var3;
   }

   ViewGroup setUpHolderBlocks(Part var1, ViewGroup var2, ExpandableLayout var3, ViewGroup var4) {
      var2.setVisibility(0);
      if(var4.getParent() != null) {
         ((ViewGroup)var4.getParent()).removeView(var4);
      }

      if(var2.getChildCount() > 0) {
         var2.removeAllViews();
      }

      var2.addView(var4, 0);
      this.setUpRowFocusRules(this.itemView, var1, var3);
      return var4;
   }

   void showAvatar(Participant var1, ImageView var2, AppConfig var3, i var4) {
      var2.setVisibility(0);
      AvatarUtils.loadAvatarIntoView(var1.getAvatar(), var2, var3, var4);
   }
}
