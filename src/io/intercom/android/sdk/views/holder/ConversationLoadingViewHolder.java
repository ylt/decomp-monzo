package io.intercom.android.sdk.views.holder;

import android.graphics.PorterDuff.Mode;
import android.support.v7.widget.RecyclerView.w;
import android.view.View;
import android.widget.ProgressBar;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.identity.AppConfig;

public class ConversationLoadingViewHolder extends w {
   public ConversationLoadingViewHolder(View var1, AppConfig var2) {
      super(var1);
      ((ProgressBar)var1.findViewById(R.id.progressBar)).getIndeterminateDrawable().setColorFilter(var2.getBaseColor(), Mode.SRC_IN);
   }
}
