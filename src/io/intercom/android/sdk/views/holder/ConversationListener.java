package io.intercom.android.sdk.views.holder;

import android.view.View;
import io.intercom.android.sdk.models.Part;

public interface ConversationListener {
   int getCount();

   Part getPart(int var1);

   Part getSelectedPart();

   void onContainerCardClicked(int var1, ContainerCardViewHolder var2);

   void onLinkClicked(int var1, View var2);

   void onPartClicked(int var1, PartViewHolder var2);
}
