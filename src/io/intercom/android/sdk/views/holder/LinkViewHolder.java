package io.intercom.android.sdk.views.holder;

import android.content.ClipboardManager;
import android.os.Build.VERSION;
import android.view.View;
import android.view.ViewGroup;
import io.intercom.android.sdk.Provider;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.android.sdk.models.Part;
import io.intercom.com.bumptech.glide.i;

public class LinkViewHolder extends BlocksPartViewHolder {
   private final Provider appConfigProvider;
   private final i requestManager;
   private final int viewType;

   public LinkViewHolder(View var1, int var2, ConversationListener var3, ClipboardManager var4, Provider var5, i var6) {
      super(var1, var3, var4);
      this.viewType = var2;
      this.appConfigProvider = var5;
      this.requestManager = var6;
      if(VERSION.SDK_INT >= 21) {
         this.cellLayout.setTransitionName("link_background");
      }

   }

   public void bind(Part var1, ViewGroup var2) {
      var2 = this.setUpHolderBlocks(var1, this.cellLayout, this.bubble, var2);
      this.checkForEntranceAnimation(this.viewType, var1, this.networkAvatar, this.cellLayout, var2);
      if(this.networkAvatar != null) {
         this.showAvatar(var1.getParticipant(), this.networkAvatar, (AppConfig)this.appConfigProvider.get(), this.requestManager);
      }

   }

   public void onClick(View var1) {
      int var2 = this.getAdapterPosition();
      if(var2 != -1) {
         this.conversationListener.onLinkClicked(var2, this.cellLayout);
      }

   }
}
