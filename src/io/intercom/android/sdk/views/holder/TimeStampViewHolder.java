package io.intercom.android.sdk.views.holder;

import android.support.v7.widget.RecyclerView.w;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import io.intercom.android.sdk.models.Part;
import io.intercom.android.sdk.utilities.TimeFormatter;

public class TimeStampViewHolder extends w implements ConversationPartViewHolder {
   private final TimeFormatter timeFormatter;

   public TimeStampViewHolder(View var1, TimeFormatter var2) {
      super(var1);
      this.timeFormatter = var2;
   }

   public void bind(Part var1, ViewGroup var2) {
      ((TextView)this.itemView).setText(this.timeFormatter.getAbsoluteDate(var1.getCreatedAt()));
   }
}
