package io.intercom.android.sdk.views.holder;

import android.content.ClipboardManager;
import android.view.View;
import android.view.ViewGroup;
import io.intercom.android.sdk.Provider;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.android.sdk.models.Part;
import io.intercom.com.bumptech.glide.i;

public class ConversationRatingViewHolder extends BlocksPartViewHolder {
   private final Provider appConfigProvider;
   private final i requestManager;

   public ConversationRatingViewHolder(View var1, ConversationListener var2, Provider var3, i var4) {
      super(var1, var2, (ClipboardManager)null);
      this.appConfigProvider = var3;
      this.requestManager = var4;
   }

   public void bind(Part var1, ViewGroup var2) {
      this.setUpHolderBlocks(var1, this.cellLayout, this.bubble, var2);
      if(this.networkAvatar != null) {
         this.showAvatar(var1.getParticipant(), this.networkAvatar, (AppConfig)this.appConfigProvider.get(), this.requestManager);
      }

   }

   public void onClick(View var1) {
   }
}
