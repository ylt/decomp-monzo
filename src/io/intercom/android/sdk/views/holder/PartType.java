package io.intercom.android.sdk.views.holder;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.SOURCE)
public @interface PartType {
   int ADMIN_IS_TYPING = 5;
   int ADMIN_MESSAGE = 1;
   int ALT_USER_MESSAGE = 4;
   int CONVERSATION_RATING = 10;
   int DAY_DIVIDER = 6;
   int EVENT = 12;
   int LINK = 8;
   int LINK_LIST = 13;
   int LINK_REPLY = 9;
   int LOADING_LAYOUT = 7;
   int NOTE_CARD = 3;
   int POST_CARD = 2;
   int USER_MESSAGE = 0;
}
