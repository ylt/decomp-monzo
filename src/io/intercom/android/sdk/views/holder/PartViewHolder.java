package io.intercom.android.sdk.views.holder;

import android.content.ClipboardManager;
import android.content.Context;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.support.v4.content.a;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import io.intercom.android.sdk.Provider;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.blocks.BlockType;
import io.intercom.android.sdk.blocks.models.Block;
import io.intercom.android.sdk.commons.utilities.ScreenUtils;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.android.sdk.models.Part;
import io.intercom.android.sdk.utilities.BackgroundUtils;
import io.intercom.android.sdk.utilities.Phrase;
import io.intercom.android.sdk.views.PartMetadataFormatter;
import io.intercom.com.bumptech.glide.i;
import java.util.List;

public class PartViewHolder extends BlocksPartViewHolder {
   private final Provider appConfigProvider;
   private final TextView attribution;
   private ViewGroup blocks;
   private final TextView metaData;
   private final PartMetadataFormatter partMetadataFormatter;
   private final i requestManager;
   private final int viewType;

   public PartViewHolder(View var1, int var2, ConversationListener var3, ClipboardManager var4, PartMetadataFormatter var5, Provider var6, i var7) {
      super(var1, var3, var4);
      this.viewType = var2;
      this.partMetadataFormatter = var5;
      this.appConfigProvider = var6;
      this.requestManager = var7;
      this.metaData = (TextView)var1.findViewById(R.id.metadata);
      this.attribution = (TextView)var1.findViewById(R.id.attribution);
   }

   private boolean isSingleImagePart(Part var1) {
      boolean var2 = false;
      List var3 = var1.getBlocks();
      if(var3.size() == 1) {
         BlockType var4 = ((Block)var3.get(0)).getType();
         if(var4 != BlockType.LOCALIMAGE && var4 != BlockType.IMAGE) {
            var2 = false;
         } else {
            var2 = true;
         }
      }

      return var2;
   }

   private void setBubbleBackground(Part var1, View var2, int var3) {
      int var4 = 0;
      int var8 = var2.getPaddingLeft();
      int var7 = var2.getPaddingRight();
      int var5 = var2.getPaddingTop();
      int var6 = var2.getPaddingBottom();
      if(this.isSingleImagePart(var1)) {
         this.cellLayout.setBackgroundColor(0);
         this.cellLayout.setPadding(0, 0, 0, 0);
         var3 = 0;
      } else {
         Context var9 = this.cellLayout.getContext();
         var4 = (int)var9.getResources().getDimension(R.dimen.intercom_cell_horizontal_padding);
         this.cellLayout.setPadding(var4, 0, var4, 0);
         Drawable var10 = a.a(var9, R.drawable.intercom_bubble_background);
         if(!var1.isAdmin() && var3 != 4) {
            var10.setColorFilter(((AppConfig)this.appConfigProvider.get()).getBaseColor(), Mode.SRC_IN);
         }

         BackgroundUtils.setBackground(this.cellLayout, var10);
         var4 = var6;
         var3 = var5;
      }

      var2.setPadding(var8, var3, var7, var4);
   }

   private void setupAttribution(Part var1) {
      if(this.attribution != null) {
         if(this.shouldShowAttribution(var1)) {
            CharSequence var2 = Phrase.from(this.metaData.getContext(), R.string.intercom_gif_attribution).put("providername", ((Block)var1.getBlocks().get(0)).getAttribution()).format();
            this.attribution.setVisibility(0);
            this.attribution.setText(var2);
         } else {
            this.attribution.setVisibility(8);
         }
      }

   }

   private void setupHolderBackground(int var1, Part var2, boolean var3, i var4) {
      this.setBubbleBackground(var2, this.blocks, var1);
      if(this.networkAvatar != null) {
         if(var3) {
            this.networkAvatar.setVisibility(4);
         } else if(var1 == 1 || var1 == 5 || var1 == 4) {
            this.showAvatar(var2.getParticipant(), this.networkAvatar, (AppConfig)this.appConfigProvider.get(), var4);
         }
      }

   }

   private void setupMetaData(int var1, Part var2) {
      if(this.metaData != null) {
         boolean var3;
         if(var2 == this.conversationListener.getPart(this.conversationListener.getCount() - 1)) {
            var3 = true;
         } else {
            var3 = false;
         }

         if(var1 != 2 && var1 != 3) {
            this.metaData.setText(this.partMetadataFormatter.getMetadataString(var2, var3, this.metaData.getResources()));
            if(Part.MessageState.UPLOAD_FAILED == var2.getMessageState()) {
               this.metaData.setCompoundDrawablesWithIntrinsicBounds(R.drawable.intercom_message_error, 0, 0, 0);
            } else {
               this.metaData.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            }
         } else {
            this.metaData.setVisibility(4);
            this.metaData.setHeight(ScreenUtils.dpToPx(10.0F, this.metaData.getContext()));
         }
      }

   }

   private boolean shouldConcatenate(Part var1, int var2) {
      boolean var3 = false;
      if(var2 != -1 && var2 + 1 < this.conversationListener.getCount()) {
         var3 = Part.shouldConcatenate(var1, this.conversationListener.getPart(var2 + 1));
      }

      return var3;
   }

   private boolean shouldExpand(Part var1) {
      boolean var2;
      if(!this.isSelectedPart(var1) && !this.isLastPart(var1) && !this.isNextPartTyping(this.getAdapterPosition())) {
         var2 = false;
      } else {
         var2 = true;
      }

      return var2;
   }

   private boolean shouldShowAttribution(Part var1) {
      boolean var2;
      if(this.isSingleImagePart(var1) && !TextUtils.isEmpty(((Block)var1.getBlocks().get(0)).getAttribution()) && var1.getMessageState().equals(Part.MessageState.NORMAL)) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   public void bind(Part var1, ViewGroup var2) {
      boolean var3 = this.shouldConcatenate(var1, this.getAdapterPosition());
      this.blocks = this.setUpHolderBlocks(var1, this.cellLayout, this.bubble, var2);
      this.setupHolderBackground(this.viewType, var1, var3, this.requestManager);
      this.setupMetaData(this.viewType, var1);
      this.setupAttribution(var1);
      this.checkForEntranceAnimation(this.viewType, var1, this.networkAvatar, this.cellLayout, this.blocks);
      if(this.bubble != null) {
         this.bubble.setExpanded(this.shouldExpand(var1), false);
      }

   }

   boolean isLastPart(Part var1) {
      boolean var2;
      if(var1 == this.conversationListener.getPart(this.conversationListener.getCount() - 1) && !"admin_is_typing_style".equals(var1.getMessageStyle())) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   boolean isNextPartTyping(int var1) {
      boolean var3 = false;
      boolean var2;
      if(var1 == -1) {
         var2 = var3;
      } else {
         ++var1;
         var2 = var3;
         if(var1 <= this.conversationListener.getCount() - 1) {
            var2 = var3;
            if("admin_is_typing_style".equals(this.conversationListener.getPart(var1).getMessageStyle())) {
               var2 = true;
            }
         }
      }

      return var2;
   }

   boolean isSelectedPart(Part var1) {
      boolean var2;
      if(var1 == this.conversationListener.getSelectedPart()) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   public void onClick(View var1) {
      int var2 = this.getAdapterPosition();
      if(var2 != -1) {
         this.conversationListener.onPartClicked(var2, this);
      }

   }

   public void setExpanded(boolean var1) {
      if(this.bubble != null) {
         this.bubble.setExpanded(var1, true);
      }

   }
}
