package io.intercom.android.sdk.views.holder;

import android.content.ClipboardManager;
import android.os.Looper;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnPreDrawListener;
import android.widget.ImageView;
import android.widget.TextView;
import io.intercom.android.sdk.Provider;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.android.sdk.models.Part;
import io.intercom.android.sdk.utilities.Phrase;
import io.intercom.android.sdk.views.LockableScrollView;
import io.intercom.com.bumptech.glide.i;

public class ContainerCardViewHolder extends BlocksPartViewHolder {
   private Provider appConfigProvider;
   ImageView arrowExpander;
   private final View fade;
   final LockableScrollView lockableScrollView;
   private final i requestManager;
   private final TextView title;
   private final int viewType;

   public ContainerCardViewHolder(final View var1, int var2, ConversationListener var3, ClipboardManager var4, boolean var5, Provider var6, i var7) {
      super(var1, var3, var4);
      this.viewType = var2;
      this.appConfigProvider = var6;
      this.requestManager = var7;
      this.title = (TextView)var1.findViewById(R.id.intercom_container_card_title);
      this.fade = var1.findViewById(R.id.intercom_container_fade_view);
      this.lockableScrollView = (LockableScrollView)var1.findViewById(R.id.cell_content);
      this.lockableScrollView.setScrollingEnabled(false);
      if(var2 == 3) {
         if(var5) {
            this.arrowExpander = (ImageView)var1.findViewById(R.id.expand_arrow);
         }

         LockableScrollView var8 = this.lockableScrollView;
         if(!var5) {
            var5 = true;
         } else {
            var5 = false;
         }

         var8.setExpanded(var5);
      } else {
         this.lockableScrollView.setExpanded(false);
      }

      var1 = var1.findViewById(R.id.cellLayout);
      var1.getViewTreeObserver().addOnPreDrawListener(new OnPreDrawListener() {
         public boolean onPreDraw() {
            var1.getViewTreeObserver().removeOnPreDrawListener(this);
            ContainerCardViewHolder.this.setupViews();
            return false;
         }
      });
   }

   private boolean contentIsOverflowing() {
      boolean var1 = false;
      if(this.lockableScrollView.getChildAt(0).getMeasuredHeight() > this.lockableScrollView.getMaxHeight()) {
         var1 = true;
      }

      return var1;
   }

   private void layoutForCollapsedNoteWithArrow() {
      byte var2 = 0;
      byte var1;
      if(this.arrowExpander != null) {
         ImageView var3 = this.arrowExpander;
         if(this.contentIsOverflowing()) {
            var1 = 0;
         } else {
            var1 = 8;
         }

         var3.setVisibility(var1);
         this.arrowExpander.setScaleY(1.0F);
      }

      View var4 = this.fade;
      if(this.contentIsOverflowing()) {
         var1 = var2;
      } else {
         var1 = 4;
      }

      var4.setVisibility(var1);
   }

   private void layoutForExpandedNoteWithArrow() {
      if(this.arrowExpander != null) {
         ImageView var2 = this.arrowExpander;
         byte var1;
         if(this.contentIsOverflowing()) {
            var1 = 0;
         } else {
            var1 = 8;
         }

         var2.setVisibility(var1);
         this.arrowExpander.setScaleY(-1.0F);
      }

      this.fade.setVisibility(4);
   }

   private void layoutForExpandedNoteWithoutArrow() {
      if(this.arrowExpander != null) {
         this.arrowExpander.setVisibility(8);
      }

      this.fade.setVisibility(4);
   }

   private void layoutForPost() {
      if(this.arrowExpander != null) {
         this.arrowExpander.setVisibility(8);
      }

      View var2 = this.fade;
      byte var1;
      if(this.contentIsOverflowing()) {
         var1 = 0;
      } else {
         var1 = 4;
      }

      var2.setVisibility(var1);
   }

   private void runOnMainThread(Runnable var1) {
      if(Looper.myLooper() == Looper.getMainLooper()) {
         var1.run();
      } else {
         this.lockableScrollView.post(var1);
      }

   }

   public void bind(Part var1, ViewGroup var2) {
      ViewGroup var3 = this.setUpHolderBlocks(var1, this.cellLayout, this.bubble, var2);
      this.showAvatar(var1.getParticipant(), this.networkAvatar, (AppConfig)this.appConfigProvider.get(), this.requestManager);
      CharSequence var4 = Phrase.from(this.title.getContext(), R.string.intercom_teammate_from_company).put("name", var1.getParticipant().getForename()).put("company", ((AppConfig)this.appConfigProvider.get()).getName()).format();
      this.title.setText(var4);
      this.checkForEntranceAnimation(this.viewType, var1, this.networkAvatar, this.cellLayout, var3);
   }

   public void onClick(View var1) {
      int var2 = this.getAdapterPosition();
      if(var2 != -1) {
         this.conversationListener.onContainerCardClicked(var2, this);
      }

   }

   void setupViews() {
      if(this.viewType == 2) {
         this.layoutForPost();
      } else if(this.arrowExpander == null) {
         this.layoutForExpandedNoteWithoutArrow();
      } else if(this.lockableScrollView.isExpanded()) {
         this.layoutForExpandedNoteWithArrow();
      } else {
         this.layoutForCollapsedNoteWithArrow();
      }

   }

   public void toggleExpanded() {
      this.runOnMainThread(new Runnable() {
         public void run() {
            if(ContainerCardViewHolder.this.arrowExpander != null && ContainerCardViewHolder.this.arrowExpander.getVisibility() == 0) {
               ContainerCardViewHolder.this.lockableScrollView.toggleExpanded();
               ContainerCardViewHolder.this.setupViews();
            }

         }
      });
   }
}
