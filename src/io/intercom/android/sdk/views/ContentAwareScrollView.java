package io.intercom.android.sdk.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ScrollView;

public class ContentAwareScrollView extends ScrollView {
   private ContentAwareScrollView.Listener listener;

   public ContentAwareScrollView(Context var1) {
      super(var1);
   }

   public ContentAwareScrollView(Context var1, AttributeSet var2) {
      super(var1, var2);
   }

   public ContentAwareScrollView(Context var1, AttributeSet var2, int var3) {
      super(var1, var2, var3);
   }

   private boolean isAtBottom() {
      boolean var2 = false;
      int var1 = this.getChildAt(0).getBottom();
      if(var1 != 0 && this.getBottom() + this.getScrollY() >= var1) {
         var2 = true;
      }

      return var2;
   }

   public ContentAwareScrollView.Listener getListener() {
      return this.listener;
   }

   protected void notifyListenerIfAtBottom() {
      if(this.listener != null && this.isAtBottom()) {
         this.listener.onBottomReached();
      }

   }

   protected void notifyListenerScrollChanged(int var1) {
      if(this.listener != null) {
         this.listener.onScrollChanged(var1);
      }

   }

   protected void onMeasure(int var1, int var2) {
      super.onMeasure(var1, var2);
      this.notifyListenerIfAtBottom();
   }

   protected void onScrollChanged(int var1, int var2, int var3, int var4) {
      super.onScrollChanged(var1, var2, var3, var4);
      this.notifyListenerIfAtBottom();
      this.notifyListenerScrollChanged(var2);
   }

   public void setListener(ContentAwareScrollView.Listener var1) {
      this.listener = var1;
      this.notifyListenerIfAtBottom();
   }

   public interface Listener {
      void onBottomReached();

      void onScrollChanged(int var1);
   }
}
