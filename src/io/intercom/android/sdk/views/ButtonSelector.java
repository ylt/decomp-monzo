package io.intercom.android.sdk.views;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;

public class ButtonSelector extends StateListDrawable {
   private final int color;

   public ButtonSelector(Context var1, int var2, int var3) {
      this.color = var3;
      Drawable var4 = var1.getResources().getDrawable(var2);
      this.addState(new int[]{16842910}, var4);
      var4 = var1.getResources().getDrawable(var2);
      this.addState(new int[]{16842908}, var4);
      Drawable var5 = var1.getResources().getDrawable(var2);
      this.addState(new int[]{16842919}, var5);
   }

   private static int darken(int var0, double var1) {
      return Color.argb(255, (int)((double)Color.red(var0) * var1), (int)((double)Color.green(var0) * var1), (int)((double)Color.blue(var0) * var1));
   }

   protected boolean onStateChange(int[] var1) {
      boolean var3 = false;
      int var4 = var1.length;

      for(int var2 = 0; var2 < var4; ++var2) {
         int var5 = var1[var2];
         if(var5 == 16842919 || var5 == 16842908) {
            var3 = true;
         }
      }

      if(var3) {
         this.setColorFilter(darken(this.color, 0.9D), Mode.SRC);
      } else {
         this.setColorFilter(this.color, Mode.SRC);
      }

      return super.onStateChange(var1);
   }
}
