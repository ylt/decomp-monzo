package io.intercom.android.sdk.views;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.BaseSavedState;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.LinearLayout;
import io.intercom.android.sdk.R;

public class ExpandableLayout extends LinearLayout {
   private static final long ANIMATION_DURATION = 100L;
   private boolean attachedToWindow;
   private ObjectAnimator expandAnimator;
   private ObjectAnimator fadeAnimator;
   private boolean firstLayout = true;
   private int heightMeasureSpec;
   private boolean inLayout;
   private int widthMeasureSpec;

   public ExpandableLayout(Context var1) {
      super(var1);
   }

   public ExpandableLayout(Context var1, AttributeSet var2) {
      super(var1, var2);
   }

   public ExpandableLayout(Context var1, AttributeSet var2, int var3) {
      super(var1, var2, var3);
   }

   private boolean collapse(View var1, boolean var2) {
      if(!this.checkExpandableView(var1)) {
         throw new IllegalArgumentException("collapse(), View is not expandableView");
      } else {
         ExpandableLayout.LayoutParams var3 = (ExpandableLayout.LayoutParams)var1.getLayoutParams();
         if(!this.firstLayout && this.attachedToWindow && var2) {
            if(var3.isExpanded && !var3.isExpanding) {
               this.playCollapseAnimation(var1);
               var2 = true;
            } else {
               var2 = false;
            }
         } else {
            var3.isExpanded = false;
            var3.isExpanding = false;
            var3.height = var3.originalHeight;
            var1.setVisibility(8);
            var2 = true;
         }

         return var2;
      }
   }

   private boolean expand(View var1, boolean var2) {
      if(!this.checkExpandableView(var1)) {
         throw new IllegalArgumentException("expand(), View is not expandableView");
      } else {
         ExpandableLayout.LayoutParams var3 = (ExpandableLayout.LayoutParams)var1.getLayoutParams();
         if(!this.firstLayout && this.attachedToWindow && var2) {
            if(!var3.isExpanded && !var3.isExpanding) {
               this.playExpandAnimation(var1);
               var2 = true;
            } else {
               var2 = false;
            }
         } else {
            var3.isExpanded = true;
            var3.isExpanding = false;
            var3.height = var3.originalHeight;
            var1.setVisibility(0);
            var2 = true;
         }

         return var2;
      }
   }

   private void playCollapseAnimation(final View var1) {
      ExpandableLayout.LayoutParams var2 = (ExpandableLayout.LayoutParams)var1.getLayoutParams();
      if(!var2.isExpanding) {
         var1.setVisibility(0);
         var2.isExpanding = true;
         this.measure(this.widthMeasureSpec, this.heightMeasureSpec);
         this.expandAnimator = ObjectAnimator.ofInt(var2, "height", new int[]{var1.getMeasuredHeight(), 0});
         this.expandAnimator.setDuration(100L);
         this.expandAnimator.addUpdateListener(new AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator var1x) {
               var1.requestLayout();
            }
         });
         this.expandAnimator.addListener(new AnimatorEndListener() {
            public void onAnimationEnd(Animator var1x) {
               ExpandableLayout.this.performToggleState(var1);
            }
         });
         var1.setAlpha(0.0F);
         this.fadeAnimator = ObjectAnimator.ofFloat(var1, View.ALPHA, new float[]{1.0F, 0.0F});
         this.fadeAnimator.setDuration(100L);
         this.fadeAnimator.addListener(new AnimatorEndListener() {
            public void onAnimationEnd(Animator var1x) {
               var1.setAlpha(0.0F);
            }
         });
         this.expandAnimator.setStartDelay(200L);
         this.fadeAnimator.start();
         this.expandAnimator.start();
      }

   }

   private void playExpandAnimation(final View var1) {
      ExpandableLayout.LayoutParams var3 = (ExpandableLayout.LayoutParams)var1.getLayoutParams();
      if(!var3.isExpanding) {
         var1.setVisibility(0);
         var3.isExpanding = true;
         this.measure(this.widthMeasureSpec, this.heightMeasureSpec);
         int var2 = var1.getMeasuredHeight();
         var3.height = 0;
         this.expandAnimator = ObjectAnimator.ofInt(var3, "height", new int[]{0, var2});
         this.expandAnimator.setDuration(100L);
         this.expandAnimator.addUpdateListener(new AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator var1x) {
               var1.requestLayout();
            }
         });
         this.expandAnimator.addListener(new AnimatorEndListener() {
            public void onAnimationEnd(Animator var1x) {
               ExpandableLayout.this.performToggleState(var1);
            }
         });
         var1.setAlpha(0.0F);
         this.fadeAnimator = ObjectAnimator.ofFloat(var1, View.ALPHA, new float[]{0.0F, 1.0F});
         this.fadeAnimator.setDuration(100L);
         this.fadeAnimator.addListener(new AnimatorEndListener() {
            public void onAnimationEnd(Animator var1x) {
               var1.setAlpha(1.0F);
            }
         });
         this.fadeAnimator.setStartDelay(200L);
         this.expandAnimator.start();
         this.fadeAnimator.start();
      }

   }

   private void safeReleaseAnimator(ObjectAnimator var1) {
      if(var1 != null && var1.isRunning()) {
         var1.end();
      }

   }

   boolean checkExpandableView(View var1) {
      return ((ExpandableLayout.LayoutParams)var1.getLayoutParams()).canExpand;
   }

   protected boolean checkLayoutParams(android.view.ViewGroup.LayoutParams var1) {
      boolean var2;
      if(super.checkLayoutParams(var1) && var1 instanceof ExpandableLayout.LayoutParams) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   public View findExpandableView() {
      int var1 = 0;

      View var2;
      while(true) {
         if(var1 >= this.getChildCount()) {
            var2 = null;
            break;
         }

         if(((ExpandableLayout.LayoutParams)this.getChildAt(var1).getLayoutParams()).canExpand) {
            var2 = this.getChildAt(var1);
            break;
         }

         ++var1;
      }

      return var2;
   }

   protected ExpandableLayout.LayoutParams generateDefaultLayoutParams() {
      return new ExpandableLayout.LayoutParams(-1, -2);
   }

   public ExpandableLayout.LayoutParams generateLayoutParams(AttributeSet var1) {
      return new ExpandableLayout.LayoutParams(this.getContext(), var1);
   }

   protected ExpandableLayout.LayoutParams generateLayoutParams(android.view.ViewGroup.LayoutParams var1) {
      return new ExpandableLayout.LayoutParams(var1);
   }

   public boolean isExpanded() {
      View var2 = this.findExpandableView();
      boolean var1;
      if(var2 != null && ((ExpandableLayout.LayoutParams)var2.getLayoutParams()).isExpanded) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   protected void onAttachedToWindow() {
      super.onAttachedToWindow();
      this.attachedToWindow = true;
   }

   protected void onDetachedFromWindow() {
      super.onDetachedFromWindow();
      this.attachedToWindow = false;
      View var1 = this.findExpandableView();
      this.safeReleaseAnimator(this.expandAnimator);
      this.safeReleaseAnimator(this.fadeAnimator);
      if(var1 != null) {
         ExpandableLayout.LayoutParams var2 = (ExpandableLayout.LayoutParams)var1.getLayoutParams();
         if(var2.isExpanded) {
            var2.height = var2.originalHeight;
            var1.setVisibility(0);
         } else {
            var2.height = var2.originalHeight;
            var1.setVisibility(8);
         }

         var2.isExpanding = false;
      }

   }

   protected void onLayout(boolean var1, int var2, int var3, int var4, int var5) {
      this.inLayout = true;
      super.onLayout(var1, var2, var3, var4, var5);
      this.inLayout = false;
      this.firstLayout = false;
   }

   protected void onMeasure(int var1, int var2) {
      this.widthMeasureSpec = var1;
      this.heightMeasureSpec = var2;
      View var3 = this.findExpandableView();
      if(var3 != null) {
         ExpandableLayout.LayoutParams var4 = (ExpandableLayout.LayoutParams)var3.getLayoutParams();
         if(var4.weight != 0.0F) {
            throw new IllegalArgumentException("ExpandableView can't use weight");
         }

         if(!var4.isExpanded && !var4.isExpanding) {
            var3.setVisibility(8);
         } else {
            var3.setVisibility(0);
         }
      }

      super.onMeasure(var1, var2);
   }

   protected void onRestoreInstanceState(Parcelable var1) {
      ExpandableLayout.SavedState var2 = (ExpandableLayout.SavedState)var1;
      super.onRestoreInstanceState(var2.getSuperState());
      if(var2.isExpanded && this.findExpandableView() != null) {
         this.setExpanded(true);
      }

   }

   protected Parcelable onSaveInstanceState() {
      ExpandableLayout.SavedState var1 = new ExpandableLayout.SavedState(super.onSaveInstanceState());
      if(this.isExpanded()) {
         var1.isExpanded = true;
      }

      return var1;
   }

   void performToggleState(View var1) {
      ExpandableLayout.LayoutParams var2 = (ExpandableLayout.LayoutParams)var1.getLayoutParams();
      if(var2.isExpanded) {
         var2.isExpanded = false;
         var1.setVisibility(8);
         var2.height = var2.originalHeight;
      } else {
         var2.isExpanded = true;
      }

      var2.isExpanding = false;
   }

   public void requestLayout() {
      if(!this.inLayout) {
         super.requestLayout();
      }

   }

   public boolean setExpanded(boolean var1) {
      return this.setExpanded(var1, false);
   }

   public boolean setExpanded(boolean var1, boolean var2) {
      boolean var4 = false;
      View var5 = this.findExpandableView();
      boolean var3 = var4;
      if(var5 != null) {
         var3 = var4;
         if(var1 != this.isExpanded()) {
            if(var1) {
               var3 = this.expand(var5, var2);
            } else {
               var3 = this.collapse(var5, var2);
            }
         }
      }

      this.requestLayout();
      return var3;
   }

   private static class LayoutParams extends android.widget.LinearLayout.LayoutParams {
      private static final int NO_MEASURED_HEIGHT = -10;
      boolean canExpand;
      boolean isExpanded;
      boolean isExpanding;
      int originalHeight = -10;

      public LayoutParams(int var1, int var2) {
         super(var1, var2);
         this.originalHeight = this.height;
      }

      public LayoutParams(int var1, int var2, float var3) {
         super(var1, var2, var3);
         this.originalHeight = this.height;
      }

      public LayoutParams(Context var1, AttributeSet var2) {
         super(var1, var2);
         TypedArray var3 = var1.obtainStyledAttributes(var2, R.styleable.ExpandableLayout);
         this.canExpand = var3.getBoolean(R.styleable.ExpandableLayout_intercomCanExpand, false);
         this.originalHeight = this.height;
         var3.recycle();
      }

      public LayoutParams(android.view.ViewGroup.LayoutParams var1) {
         super(var1);
         this.originalHeight = this.height;
      }

      public LayoutParams(MarginLayoutParams var1) {
         super(var1);
         this.originalHeight = this.height;
      }

      @TargetApi(19)
      public LayoutParams(android.widget.LinearLayout.LayoutParams var1) {
         super(var1);
         this.originalHeight = this.height;
      }

      public void setHeight(int var1) {
         this.height = var1;
      }
   }

   private static class SavedState extends BaseSavedState {
      public static final Creator CREATOR = new Creator() {
         public ExpandableLayout.SavedState createFromParcel(Parcel var1) {
            return new ExpandableLayout.SavedState(var1);
         }

         public ExpandableLayout.SavedState[] newArray(int var1) {
            return new ExpandableLayout.SavedState[var1];
         }
      };
      boolean isExpanded;

      public SavedState(Parcel var1) {
         boolean var2 = true;
         super(var1);
         if(var1.readInt() != 1) {
            var2 = false;
         }

         this.isExpanded = var2;
      }

      public SavedState(Parcelable var1) {
         super(var1);
      }

      public void writeToParcel(Parcel var1, int var2) {
         super.writeToParcel(var1, var2);
         byte var3;
         if(this.isExpanded) {
            var3 = 1;
         } else {
            var3 = 0;
         }

         var1.writeInt(var3);
      }
   }
}
