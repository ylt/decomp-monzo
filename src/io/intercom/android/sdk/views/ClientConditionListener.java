package io.intercom.android.sdk.views;

public interface ClientConditionListener {
   void conditionSatisfied(String var1, String var2);
}
