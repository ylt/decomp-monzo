package io.intercom.android.sdk.views;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.drawable.Drawable;

public class AvatarInitialsDrawable extends Drawable {
   private final Paint avatarBackground;
   private final String text;
   private final Rect textBounds = new Rect();
   private final Paint textPaint;

   public AvatarInitialsDrawable(String var1, int var2) {
      this.text = var1;
      this.avatarBackground = new Paint();
      this.avatarBackground.setAntiAlias(true);
      this.avatarBackground.setColor(var2);
      this.avatarBackground.setStyle(Style.FILL);
      this.textPaint = new Paint();
      this.textPaint.setAntiAlias(true);
      this.textPaint.setColor(-1);
      this.textPaint.setFakeBoldText(true);
      this.textPaint.setTextAlign(Align.LEFT);
   }

   public void draw(Canvas var1) {
      Rect var2 = this.getBounds();
      this.textPaint.setTextSize((float)(var2.height() / 3));
      this.textPaint.getTextBounds(this.text, 0, this.text.length(), this.textBounds);
      var1.drawCircle((float)var2.centerX(), (float)var2.centerY(), (float)(var2.height() / 2), this.avatarBackground);
      var1.drawText(this.text, (float)var2.centerX() - this.textBounds.exactCenterX(), (float)var2.centerY() - this.textBounds.exactCenterY(), this.textPaint);
   }

   public int getOpacity() {
      return -3;
   }

   public void setAlpha(int var1) {
      this.textPaint.setAlpha(var1);
      this.avatarBackground.setAlpha(var1);
   }

   public void setColorFilter(ColorFilter var1) {
   }
}
