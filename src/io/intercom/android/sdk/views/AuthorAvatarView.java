package io.intercom.android.sdk.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.FrameLayout.LayoutParams;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.commons.utilities.ScreenUtils;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.android.sdk.models.Avatar;
import io.intercom.android.sdk.models.Participant;
import io.intercom.android.sdk.utilities.AvatarUtils;
import io.intercom.com.bumptech.glide.i;
import java.util.List;

public class AuthorAvatarView extends FrameLayout {
   private final ActiveStatePresenter activeStatePresenter;
   private final int activeStateSize;
   private final View activeStateView;
   private final int imageViewSize;
   private final ImageView leftImageView;
   private final ImageView rightImageView;
   private final int teamAvatarPadding;
   private final ImageView topImageView;

   public AuthorAvatarView(Context var1) {
      this(var1, (AttributeSet)null);
   }

   public AuthorAvatarView(Context var1, AttributeSet var2) {
      super(var1, var2);
      this.activeStatePresenter = new ActiveStatePresenter();
      this.rightImageView = new ImageView(var1);
      this.topImageView = new ImageView(var1);
      this.leftImageView = new ImageView(var1);
      this.activeStateView = new View(var1);
      this.addView(this.rightImageView);
      this.addView(this.topImageView);
      this.addView(this.leftImageView);
      this.addView(this.activeStateView);
      this.leftImageView.setBackgroundResource(R.drawable.intercom_solid_circle);
      this.rightImageView.setBackgroundResource(R.drawable.intercom_solid_circle);
      this.topImageView.setBackgroundResource(R.drawable.intercom_solid_circle);
      this.activeStateView.setVisibility(8);
      this.teamAvatarPadding = ScreenUtils.dpToPx(1.0F, var1);
      this.leftImageView.setPadding(this.teamAvatarPadding, this.teamAvatarPadding, this.teamAvatarPadding, this.teamAvatarPadding);
      this.rightImageView.setPadding(this.teamAvatarPadding, this.teamAvatarPadding, this.teamAvatarPadding, this.teamAvatarPadding);
      this.topImageView.setPadding(this.teamAvatarPadding, this.teamAvatarPadding, this.teamAvatarPadding, this.teamAvatarPadding);
      TypedArray var5 = var1.getTheme().obtainStyledAttributes(var2, R.styleable.AuthorAvatarView, 0, 0);

      try {
         this.imageViewSize = var5.getDimensionPixelSize(R.styleable.AuthorAvatarView_avatarSize, 0);
         this.activeStateSize = var5.getDimensionPixelSize(R.styleable.AuthorAvatarView_activeStateSize, 0);
      } finally {
         var5.recycle();
      }

      this.activeStateView.setLayoutParams(new LayoutParams(this.activeStateSize, this.activeStateSize, 85));
   }

   private void loadDefaultDrawable(AppConfig var1) {
      this.leftImageView.setLayoutParams(new LayoutParams(-1, -1));
      this.leftImageView.setPadding(0, 0, 0, 0);
      this.setNumberOfVisibleAvatars(1);
      this.activeStateView.setVisibility(8);
      this.requestLayout();
      this.leftImageView.setImageDrawable(AvatarUtils.getDefaultDrawable(this.leftImageView.getContext(), var1));
   }

   private void loadThreeAvatars(Participant var1, Participant var2, Participant var3, AppConfig var4, i var5) {
      this.leftImageView.setLayoutParams(new LayoutParams(this.imageViewSize, this.imageViewSize, 83));
      this.leftImageView.setPadding(this.teamAvatarPadding, this.teamAvatarPadding, this.teamAvatarPadding, this.teamAvatarPadding);
      this.rightImageView.setLayoutParams(new LayoutParams(this.imageViewSize, this.imageViewSize, 85));
      this.topImageView.setLayoutParams(new LayoutParams(this.imageViewSize, this.imageViewSize, 49));
      this.activeStateView.setVisibility(8);
      this.setNumberOfVisibleAvatars(3);
      this.requestLayout();
      AvatarUtils.loadAvatarIntoView(var1.getAvatar(), this.leftImageView, var4, var5);
      AvatarUtils.loadAvatarIntoView(var2.getAvatar(), this.rightImageView, var4, var5);
      AvatarUtils.loadAvatarIntoView(var3.getAvatar(), this.topImageView, var4, var5);
   }

   private void loadTwoAvatars(Participant var1, Participant var2, AppConfig var3, i var4) {
      this.leftImageView.setLayoutParams(new LayoutParams(this.imageViewSize, this.imageViewSize, 19));
      this.leftImageView.setPadding(this.teamAvatarPadding, this.teamAvatarPadding, this.teamAvatarPadding, this.teamAvatarPadding);
      this.rightImageView.setLayoutParams(new LayoutParams(this.imageViewSize, this.imageViewSize, 21));
      this.setNumberOfVisibleAvatars(2);
      this.activeStateView.setVisibility(8);
      this.requestLayout();
      AvatarUtils.loadAvatarIntoView(var1.getAvatar(), this.leftImageView, var3, var4);
      AvatarUtils.loadAvatarIntoView(var2.getAvatar(), this.rightImageView, var3, var4);
   }

   private void setNumberOfVisibleAvatars(int var1) {
      byte var3 = 0;
      ImageView var4 = this.leftImageView;
      byte var2;
      if(var1 >= 1) {
         var2 = 0;
      } else {
         var2 = 8;
      }

      var4.setVisibility(var2);
      var4 = this.rightImageView;
      if(var1 >= 2) {
         var2 = 0;
      } else {
         var2 = 8;
      }

      var4.setVisibility(var2);
      var4 = this.topImageView;
      byte var5;
      if(var1 >= 3) {
         var5 = var3;
      } else {
         var5 = 8;
      }

      var4.setVisibility(var5);
   }

   public void loadAvatar(Avatar var1, AppConfig var2, i var3) {
      this.leftImageView.setLayoutParams(new LayoutParams(-1, -1));
      this.leftImageView.setPadding(0, 0, 0, 0);
      this.setNumberOfVisibleAvatars(1);
      this.activeStateView.setVisibility(8);
      this.requestLayout();
      AvatarUtils.loadAvatarIntoView(var1, this.leftImageView, var2, var3);
   }

   public void loadAvatarWithActiveState(Avatar var1, boolean var2, AppConfig var3, i var4) {
      this.leftImageView.setLayoutParams(new LayoutParams(-1, -1));
      this.leftImageView.setPadding(0, 0, 0, 0);
      this.setNumberOfVisibleAvatars(1);
      this.activeStatePresenter.presentStateDot(var2, this.activeStateView, var3);
      this.activeStateView.setVisibility(0);
      this.requestLayout();
      AvatarUtils.loadAvatarIntoView(var1, this.leftImageView, var3, var4);
   }

   public void loadAvatars(List var1, AppConfig var2, i var3) {
      switch(var1.size()) {
      case 0:
         this.loadDefaultDrawable(var2);
         break;
      case 1:
         this.loadAvatar(((Participant)var1.get(0)).getAvatar(), var2, var3);
         break;
      case 2:
         this.loadTwoAvatars((Participant)var1.get(0), (Participant)var1.get(1), var2, var3);
         break;
      default:
         this.loadThreeAvatars((Participant)var1.get(0), (Participant)var1.get(1), (Participant)var1.get(2), var2, var3);
      }

   }
}
