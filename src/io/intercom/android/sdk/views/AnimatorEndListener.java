package io.intercom.android.sdk.views;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;

abstract class AnimatorEndListener implements AnimatorListener {
   public void onAnimationCancel(Animator var1) {
   }

   public void onAnimationRepeat(Animator var1) {
   }

   public void onAnimationStart(Animator var1) {
   }
}
