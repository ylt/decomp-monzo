package io.intercom.android.sdk.views;

import android.annotation.TargetApi;
import android.os.Build.VERSION;
import android.view.Window;
import io.intercom.android.sdk.identity.AppConfig;

public class StatusBarThemer {
   @TargetApi(21)
   static void setStatusBarColor(int var0, Window var1, int var2) {
      if(var2 >= 21) {
         var1.addFlags(Integer.MIN_VALUE);
         var1.setStatusBarColor(var0);
      }

   }

   public static void setStatusBarColor(Window var0, AppConfig var1) {
      setStatusBarColor(var1.getBaseColorDark(), var0, VERSION.SDK_INT);
   }
}
