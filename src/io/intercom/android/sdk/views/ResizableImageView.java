package io.intercom.android.sdk.views;

import android.content.Context;
import android.graphics.Point;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.util.TypedValue;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.commons.utilities.ScreenUtils;
import io.intercom.android.sdk.utilities.ImageUtils;

public class ResizableImageView extends AppCompatImageView {
   private int imageHeight;
   private int imageWidth;
   private int totalViewPadding;

   public ResizableImageView(Context var1) {
      this(var1, (AttributeSet)null);
   }

   public ResizableImageView(Context var1, AttributeSet var2) {
      this(var1, var2, 0);
   }

   public ResizableImageView(Context var1, AttributeSet var2, int var3) {
      super(var1, var2, var3);
   }

   private int calculateContainerWidth() {
      Point var4 = ScreenUtils.getScreenDimensions(this.getContext());
      int var2;
      if(this.getResources().getBoolean(R.bool.intercom_is_two_pane)) {
         TypedValue var3 = new TypedValue();
         this.getResources().getValue(R.dimen.intercom_two_pane_conversation_percentage, var3, true);
         float var1 = var3.getFloat();
         var2 = (int)((float)var4.x * var1);
      } else {
         var2 = var4.x;
      }

      return var2;
   }

   public Point getImageDimens() {
      double var1 = ImageUtils.getAspectRatio(this.imageWidth, this.imageHeight);
      int var4 = this.calculateContainerWidth();
      int var3 = this.totalViewPadding;
      var3 = Math.min(this.imageWidth, var4 - var3);
      return new Point(var3, ImageUtils.getAspectHeight(var3, var1));
   }

   protected void onMeasure(int var1, int var2) {
      Point var3 = this.getImageDimens();
      if(this.imageWidth > 0 && this.imageHeight > 0) {
         this.setMeasuredDimension(var3.x, var3.y);
      } else {
         super.onMeasure(var1, var2);
      }

   }

   public void setDisplayImageDimensions(int var1, int var2) {
      this.imageHeight = var2;
      this.imageWidth = var1;
   }

   public void setTotalViewPadding(int var1) {
      this.totalViewPadding = var1;
   }
}
