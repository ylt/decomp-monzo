package io.intercom.android.sdk.views.decoration;

import android.content.Context;
import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.g;
import android.support.v7.widget.RecyclerView.t;
import android.view.View;
import io.intercom.android.sdk.commons.utilities.ScreenUtils;
import io.intercom.android.sdk.models.Part;
import java.util.List;

public class ConversationItemDecoration extends g {
   private static final int CONCAT_SPACING = 4;
   private static final int DIVIDER_BOTTOM_SPACING = 10;
   private static final int DIVIDER_TOP_SPACING = 16;
   private static final int SPACING = 24;
   private final int concatSpacing;
   private final int headerBottomSpacing;
   private final int headerTopSpacing;
   private final List parts;
   private final int spacing;

   public ConversationItemDecoration(Context var1, List var2) {
      this.parts = var2;
      this.spacing = ScreenUtils.dpToPx(24.0F, var1);
      this.concatSpacing = ScreenUtils.dpToPx(4.0F, var1);
      this.headerTopSpacing = ScreenUtils.dpToPx(16.0F, var1);
      this.headerBottomSpacing = ScreenUtils.dpToPx(10.0F, var1);
   }

   private boolean nextPartIsDivider(int var1) {
      boolean var2;
      if(var1 + 1 < this.parts.size() && "day_divider_style".equals(((Part)this.parts.get(var1 + 1)).getMessageStyle())) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   private int topSpacingForPartAtPosition(int var1) {
      if(var1 == 0) {
         var1 = this.spacing;
      } else {
         var1 = 0;
      }

      return var1;
   }

   public void getItemOffsets(Rect var1, View var2, RecyclerView var3, t var4) {
      int var5 = var3.g(var2);
      if(var5 != -1 && var5 < this.parts.size()) {
         Part var6 = (Part)this.parts.get(var5);
         if("day_divider_style".equals(var6.getMessageStyle())) {
            var1.set(0, this.headerTopSpacing, 0, this.headerBottomSpacing);
         } else if(this.shouldConcatenate(var6, var5)) {
            var1.set(0, this.topSpacingForPartAtPosition(var5), 0, this.concatSpacing);
         } else if(this.nextPartIsDivider(var5)) {
            var1.set(0, this.topSpacingForPartAtPosition(var5), 0, 0);
         } else {
            var1.set(0, this.topSpacingForPartAtPosition(var5), 0, this.spacing);
         }
      }

   }

   boolean shouldConcatenate(Part var1, int var2) {
      boolean var3;
      if(var2 + 1 < this.parts.size()) {
         var3 = Part.shouldConcatenate(var1, (Part)this.parts.get(var2 + 1));
      } else {
         var3 = false;
      }

      return var3;
   }
}
