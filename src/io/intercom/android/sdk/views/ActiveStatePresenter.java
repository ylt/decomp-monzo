package io.intercom.android.sdk.views;

import android.content.Context;
import android.support.v4.content.a;
import android.view.View;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.android.sdk.utilities.BackgroundUtils;

public class ActiveStatePresenter {
   private static final String ENGLISH_LOCALE = "en";

   public void presentStateDot(boolean var1, View var2, AppConfig var3) {
      if(!"en".equals(var3.getLocale())) {
         var2.setBackgroundResource(0);
      } else {
         Context var5 = var2.getContext();
         int var4;
         if(var1) {
            var4 = R.color.intercom_active_state;
         } else {
            var4 = R.color.intercom_away_state;
         }

         BackgroundUtils.setBackground(var2, new ActiveStateDrawable(a.c(var5, var4), a.c(var2.getContext(), R.color.intercom_white), 1.0F * var2.getResources().getDisplayMetrics().density));
      }

   }
}
