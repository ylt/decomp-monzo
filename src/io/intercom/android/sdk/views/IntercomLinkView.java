package io.intercom.android.sdk.views;

import android.content.Context;
import android.support.v7.widget.aa;
import android.util.AttributeSet;
import android.view.View;
import io.intercom.android.sdk.Provider;
import io.intercom.android.sdk.api.Api;
import io.intercom.android.sdk.commons.utilities.ScreenUtils;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.android.sdk.logger.LumberMill;
import io.intercom.android.sdk.models.Part;
import io.intercom.android.sdk.twig.Twig;
import io.intercom.android.sdk.utilities.LinkOpener;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class IntercomLinkView extends aa {
   private static final int INTERCOM_LINK_ANIMATION_TIME_MS = 100;
   private final int conversationBottomPaddingPx = ScreenUtils.dpToPx(8.0F, this.getContext());
   private final int[] intercomLinkPosition = new int[2];
   private int lastBottomPosition = 0;
   private final int[] lastChildPosition = new int[2];
   private final Twig twig = LumberMill.getLogger();

   public IntercomLinkView(Context var1, AttributeSet var2) {
      super(var1, var2);
   }

   private String getCompanyForUrl(String var1) {
      try {
         var1 = URLEncoder.encode(var1, "UTF-8");
      } catch (UnsupportedEncodingException var2) {
         this.twig.i("Could not url encode the app name", new Object[0]);
         var1 = "";
      }

      return var1;
   }

   private static String getProductForUrl(Part var0) {
      String var1;
      if(Part.NULL != var0 && !var0.isUser()) {
         if(var0.isLinkCard() && "educate.article".equals(var0.getLinkBlock().getLinkType())) {
            var1 = "educate";
         } else {
            var1 = "engage";
         }
      } else {
         var1 = "resolve";
      }

      return var1;
   }

   String createIntercomLinkUrl(Provider var1, Part var2) {
      return "http://www.intercom.io/intercom-link?company=" + this.getCompanyForUrl(((AppConfig)var1.get()).getName()) + "&product=" + getProductForUrl(var2) + "&utm_source=android-sdk&utm_campaign=intercom-link&utm_content=we-run-on-intercom&utm_medium=messenger";
   }

   public void followIntercomLink(Provider var1, Part var2, Api var3) {
      LinkOpener.handleUrl(this.createIntercomLinkUrl(var1, var2), this.getContext(), var3);
   }

   public void hide() {
      this.animate().alpha(0.0F).setDuration(100L);
   }

   public void hideIfIntersectedOrShow(View var1) {
      var1.getLocationOnScreen(this.lastChildPosition);
      int var2 = this.lastChildPosition[1] + var1.getHeight() + this.conversationBottomPaddingPx;
      this.getLocationOnScreen(this.intercomLinkPosition);
      if(var2 >= this.intercomLinkPosition[1] && this.lastBottomPosition < this.intercomLinkPosition[1]) {
         this.hide();
      } else if(var2 < this.intercomLinkPosition[1] && this.lastBottomPosition >= this.intercomLinkPosition[1]) {
         this.show();
      }

      this.lastBottomPosition = var2;
   }

   public void show() {
      this.animate().alpha(1.0F).setDuration(100L);
   }
}
