package io.intercom.android.sdk.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.drawable.Drawable;
import android.support.v4.content.a;
import android.text.TextUtils;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.commons.utilities.ScreenUtils;

class BackButtonCountDrawable extends Drawable {
   private static final String COUNT_BACKGROUND_COLOR = "#FE536C";
   private static final int COUNT_BACKGROUND_RADIUS_DP = 8;
   private static final int COUNT_TEXT_PADDING_TOP = 4;
   private static final int COUNT_TEXT_SIZE_DP = 11;
   private static final int ICON_PADDING_DP = 16;
   private static final int ICON_SIZE_DP = 24;
   private final Drawable backIcon;
   private final Paint countBackgroundPaint;
   private final int countBackgroundRadius;
   private final int iconPadding;
   private final int iconSize;
   private String text;
   private final Paint textPaint;
   private final int textTopPadding;

   BackButtonCountDrawable(Context var1, String var2) {
      this.text = var2;
      this.backIcon = a.a(var1, R.drawable.intercom_back);
      this.iconPadding = ScreenUtils.dpToPx(16.0F, var1);
      this.iconSize = ScreenUtils.dpToPx(24.0F, var1);
      this.countBackgroundPaint = new Paint();
      this.countBackgroundPaint.setAntiAlias(true);
      this.countBackgroundPaint.setColor(Color.parseColor("#FE536C"));
      this.countBackgroundPaint.setStyle(Style.FILL);
      this.countBackgroundRadius = ScreenUtils.dpToPx(8.0F, var1);
      this.textPaint = new Paint();
      this.textPaint.setAntiAlias(true);
      this.textPaint.setColor(-1);
      this.textPaint.setTextSize((float)ScreenUtils.dpToPx(11.0F, var1));
      this.textPaint.setFakeBoldText(true);
      this.textPaint.setStyle(Style.FILL);
      this.textPaint.setTextAlign(Align.CENTER);
      this.textTopPadding = ScreenUtils.dpToPx(4.0F, var1);
   }

   public void draw(Canvas var1) {
      Rect var6 = this.getBounds();
      int var5 = var6.left + this.iconPadding;
      int var3 = var6.top + this.iconPadding;
      int var4 = this.iconSize + var5;
      int var2 = this.iconSize;
      this.backIcon.setBounds(var5, var3, var4, var2 + var3);
      this.backIcon.draw(var1);
      if(!TextUtils.isEmpty(this.text)) {
         var1.drawCircle((float)var4, (float)var3, (float)this.countBackgroundRadius, this.countBackgroundPaint);
         var1.drawText(this.text, (float)var4, (float)(var3 + this.textTopPadding), this.textPaint);
      }

   }

   public int getOpacity() {
      return -3;
   }

   public void setAlpha(int var1) {
      this.textPaint.setAlpha(var1);
      this.countBackgroundPaint.setAlpha(var1);
      this.backIcon.setAlpha(var1);
   }

   public void setColorFilter(ColorFilter var1) {
      this.backIcon.setColorFilter(var1);
   }

   public void setText(String var1) {
      this.text = var1;
      this.invalidateSelf();
   }
}
