package io.intercom.android.sdk.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import io.intercom.android.sdk.conversation.UploadProgressListener;
import io.intercom.android.sdk.logger.LumberMill;
import io.intercom.android.sdk.twig.Twig;

public class ProgressFrameLayout extends FrameLayout implements UploadProgressListener {
   private final Twig twig;
   final UploadProgressBar uploadProgressBar;

   public ProgressFrameLayout(Context var1) {
      this(var1, (AttributeSet)null);
   }

   public ProgressFrameLayout(Context var1, AttributeSet var2) {
      super(var1, var2);
      this.twig = LumberMill.getLogger();
      this.uploadProgressBar = new UploadProgressBar(var1, var2);
      this.addView(this.uploadProgressBar);
   }

   public void uploadNotice(final byte var1) {
      this.twig.internal("progress", "" + var1);
      this.post(new Runnable() {
         public void run() {
            ProgressFrameLayout.this.uploadProgressBar.setProgress(var1);
            if(var1 == 90) {
               ProgressFrameLayout.this.uploadProgressBar.smoothEndAnimation();
            }

         }
      });
   }

   public void uploadStarted() {
      this.uploadProgressBar.smoothStartAnimation();
   }

   public void uploadStopped() {
      this.uploadProgressBar.hideBar();
   }
}
