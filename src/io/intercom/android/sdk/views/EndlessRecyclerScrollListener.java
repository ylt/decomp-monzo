package io.intercom.android.sdk.views;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.m;

public class EndlessRecyclerScrollListener extends m {
   final EndlessScrollListener endlessScrollListener;
   private final LinearLayoutManager layoutManager;
   private boolean morePagesAvailable = true;

   public EndlessRecyclerScrollListener(LinearLayoutManager var1, EndlessScrollListener var2) {
      this.layoutManager = var1;
      this.endlessScrollListener = var2;
   }

   public void onScrollStateChanged(RecyclerView var1, int var2) {
      super.onScrollStateChanged(var1, var2);
      this.endlessScrollListener.setOverScrollColour();
   }

   public void onScrolled(RecyclerView var1, int var2, int var3) {
      super.onScrolled(var1, var2, var3);
      var2 = var1.getChildCount();
      if(this.layoutManager.F() - var2 <= this.layoutManager.n() && this.morePagesAvailable) {
         var1.post(new Runnable() {
            public void run() {
               EndlessRecyclerScrollListener.this.endlessScrollListener.onLoadMore();
            }
         });
      }

   }

   public void setMorePagesAvailable(boolean var1) {
      this.morePagesAvailable = var1;
   }
}
