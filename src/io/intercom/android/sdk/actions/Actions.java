package io.intercom.android.sdk.actions;

import android.app.Activity;
import android.net.Uri;
import io.intercom.android.sdk.Intercom;
import io.intercom.android.sdk.models.BaseResponse;
import io.intercom.android.sdk.models.Conversation;
import io.intercom.android.sdk.models.ConversationsResponse;
import io.intercom.android.sdk.models.TeamPresence;
import io.intercom.android.sdk.models.UsersResponse;
import io.intercom.android.sdk.state.ReplyPart;

public class Actions {
   private static final Object NONE = "No Value";

   public static Action activityPaused(Activity var0) {
      return new Action(Action.Type.ACTIVITY_PAUSED, var0);
   }

   public static Action activityReadyForViewAttachment(Activity var0) {
      return new Action(Action.Type.ACTIVITY_READY_FOR_VIEW_ATTACHMENT, var0);
   }

   public static Action activityStopped(Activity var0) {
      return new Action(Action.Type.ACTIVITY_STOPPED, var0);
   }

   public static Action appEnteredBackground(long var0) {
      return new Action(Action.Type.APP_ENTERED_BACKGROUND, Long.valueOf(var0));
   }

   public static Action appEnteredForeground(long var0) {
      return new Action(Action.Type.APP_ENTERED_FOREGROUND, Long.valueOf(var0));
   }

   public static Action baseResponseReceived(BaseResponse var0) {
      return new Action(Action.Type.BASE_RESPONSE_RECEIVED, var0);
   }

   public static Action composerCleared(String var0) {
      return new Action(Action.Type.COMPOSER_CLEARED, var0);
   }

   public static Action composerInputChanged() {
      return noValueAction(Action.Type.COMPOSER_INPUT_CHANGED);
   }

   public static Action composerOpened() {
      return noValueAction(Action.Type.NEW_CONVERSATION_SCREEN_OPENED);
   }

   public static Action composerSendButtonPressed() {
      return noValueAction(Action.Type.COMPOSER_SEND_BUTTON_PRESSED);
   }

   public static Action composerTypedIn(String var0) {
      return new Action(Action.Type.COMPOSER_TYPED_IN, var0);
   }

   public static Action conversationClosed() {
      return noValueAction(Action.Type.CONVERSATION_CLOSED);
   }

   public static Action conversationMarkedAsDismissed(Conversation var0) {
      return new Action(Action.Type.CONVERSATION_MARKED_AS_DISMISSED, var0);
   }

   public static Action conversationMarkedAsRead(String var0) {
      return new Action(Action.Type.CONVERSATION_MARKED_AS_READ, var0);
   }

   public static Action conversationOpened(String var0) {
      return new Action(Action.Type.CONVERSATION_OPENED, var0);
   }

   public static Action conversationReplySuccess(ReplyPart var0) {
      return new Action(Action.Type.CONVERSATION_REPLY_SUCCESS, var0);
   }

   public static Action fetchConversationSuccess(Conversation var0) {
      return new Action(Action.Type.FETCH_CONVERSATION_SUCCESS, var0);
   }

   public static Action fetchInboxBeforeDateRequest(long var0) {
      return new Action(Action.Type.FETCH_INBOX_BEFORE_DATE_REQUEST, Long.valueOf(var0));
   }

   public static Action fetchInboxFailed() {
      return noValueAction(Action.Type.FETCH_INBOX_FAILED);
   }

   public static Action fetchInboxRequest() {
      return noValueAction(Action.Type.FETCH_INBOX_REQUEST);
   }

   public static Action fetchInboxSuccess(ConversationsResponse var0) {
      return new Action(Action.Type.FETCH_INBOX_SUCCESS, var0);
   }

   public static Action hardReset() {
      return noValueAction(Action.Type.HARD_RESET);
   }

   public static Action inboxOpened() {
      return noValueAction(Action.Type.INBOX_OPENED);
   }

   public static Action newCommentEventReceived(String var0) {
      return new Action(Action.Type.NEW_COMMENT_EVENT_RECEIVED, var0);
   }

   public static Action newConversationSuccess(Conversation var0) {
      return new Action(Action.Type.NEW_CONVERSATION_SUCCESS, var0);
   }

   private static Action noValueAction(Action.Type var0) {
      return new Action(var0, NONE);
   }

   public static Action screenshotDeleted() {
      return noValueAction(Action.Type.SCREENSHOT_DELETED);
   }

   public static Action screenshotLightboxOpened() {
      return noValueAction(Action.Type.SCREENSHOT_LIGHTBOX_OPENED);
   }

   public static Action screenshotTaken(Uri var0) {
      return new Action(Action.Type.SCREENSHOT_TAKEN, var0);
   }

   public static Action sessionStarted() {
      return noValueAction(Action.Type.SESSION_STARTED);
   }

   public static Action setBottomPadding(int var0) {
      return new Action(Action.Type.SET_BOTTOM_PADDING, Integer.valueOf(var0));
   }

   public static Action setInAppNotificationVisibility(Intercom.Visibility var0) {
      return new Action(Action.Type.SET_IN_APP_NOTIFICATION_VISIBILITY, var0);
   }

   public static Action setLauncherVisibility(Intercom.Visibility var0) {
      return new Action(Action.Type.SET_LAUNCHER_VISIBILITY, var0);
   }

   public static Action softReset() {
      return noValueAction(Action.Type.SOFT_RESET);
   }

   public static Action teamPresenceUpdated(TeamPresence var0) {
      return new Action(Action.Type.TEAM_PRESENCE_UPDATED, var0);
   }

   public static Action unreadConversationsSuccess(UsersResponse var0) {
      return new Action(Action.Type.UNREAD_CONVERSATIONS_SUCCESS, var0);
   }
}
