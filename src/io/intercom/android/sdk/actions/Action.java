package io.intercom.android.sdk.actions;

public final class Action {
   private final Action.Type type;
   private final Object value;

   Action(Action.Type var1, Object var2) {
      this.type = var1;
      this.value = var2;
   }

   public boolean equals(Object var1) {
      boolean var3 = false;
      boolean var2;
      if(this == var1) {
         var2 = true;
      } else {
         var2 = var3;
         if(var1 != null) {
            var2 = var3;
            if(this.getClass() == var1.getClass()) {
               Action var4 = (Action)var1;
               var2 = var3;
               if(this.type == var4.type) {
                  var2 = this.value.equals(var4.value);
               }
            }
         }
      }

      return var2;
   }

   public int hashCode() {
      return this.type.hashCode() * 31 + this.value.hashCode();
   }

   public String toString() {
      return this.type.toString() + ": " + this.value.toString();
   }

   public Action.Type type() {
      return this.type;
   }

   public Object value() {
      return this.value;
   }

   public static enum Type {
      ACTIVITY_PAUSED,
      ACTIVITY_READY_FOR_VIEW_ATTACHMENT,
      ACTIVITY_STOPPED,
      APP_ENTERED_BACKGROUND,
      APP_ENTERED_FOREGROUND,
      BASE_RESPONSE_RECEIVED,
      COMPOSER_CLEARED,
      COMPOSER_INPUT_CHANGED,
      COMPOSER_SEND_BUTTON_PRESSED,
      COMPOSER_TYPED_IN,
      CONVERSATION_CLOSED,
      CONVERSATION_MARKED_AS_DISMISSED,
      CONVERSATION_MARKED_AS_READ,
      CONVERSATION_OPENED,
      CONVERSATION_REPLY_SUCCESS,
      FETCH_CONVERSATION_SUCCESS,
      FETCH_INBOX_BEFORE_DATE_REQUEST,
      FETCH_INBOX_FAILED,
      FETCH_INBOX_REQUEST,
      FETCH_INBOX_SUCCESS,
      HARD_RESET,
      INBOX_OPENED,
      NEW_COMMENT_EVENT_RECEIVED,
      NEW_CONVERSATION_SCREEN_OPENED,
      NEW_CONVERSATION_SUCCESS,
      SCREENSHOT_DELETED,
      SCREENSHOT_LIGHTBOX_OPENED,
      SCREENSHOT_TAKEN,
      SESSION_STARTED,
      SET_BOTTOM_PADDING,
      SET_IN_APP_NOTIFICATION_VISIBILITY,
      SET_LAUNCHER_VISIBILITY,
      SOFT_RESET,
      TEAM_PRESENCE_UPDATED,
      UNREAD_CONVERSATIONS_SUCCESS;
   }
}
