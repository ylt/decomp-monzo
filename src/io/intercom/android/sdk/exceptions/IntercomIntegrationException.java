package io.intercom.android.sdk.exceptions;

public class IntercomIntegrationException extends RuntimeException {
   public IntercomIntegrationException(String var1) {
      super(var1);
   }
}
