package io.intercom.android.sdk.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.SOURCE)
public @interface SeenState {
   String HIDE = "hide";
   String SEEN = "seen";
   String UNSEEN = "unseen";
}
