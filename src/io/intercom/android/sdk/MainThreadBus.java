package io.intercom.android.sdk;

import android.os.Handler;
import android.os.Looper;
import io.intercom.com.a.a.b;
import io.intercom.com.a.a.i;

class MainThreadBus extends b {
   private final Handler mainThread = new Handler(Looper.getMainLooper());

   MainThreadBus(i var1) {
      super(var1);
   }

   public void post(final Object var1) {
      if(Looper.myLooper() == Looper.getMainLooper()) {
         super.post(var1);
      } else {
         this.mainThread.post(new Runnable() {
            public void run() {
               MainThreadBus.super.post(var1);
            }
         });
      }

   }

   public void register(Object var1) {
      try {
         super.register(var1);
      } catch (Exception var2) {
         ;
      }

   }

   public void unregister(Object var1) {
      try {
         super.unregister(var1);
      } catch (Exception var2) {
         ;
      }

   }
}
