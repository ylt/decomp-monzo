package io.intercom.android.sdk.transforms;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import io.intercom.android.sdk.commons.utilities.BitmapUtils;
import io.intercom.com.bumptech.glide.load.engine.a.e;
import io.intercom.com.bumptech.glide.load.resource.bitmap.f;
import java.security.MessageDigest;

public class RoundedCornersTransform extends f {
   private static final int VERSION = 1;
   private final String id;
   private final byte[] idBytes;
   private final int radius;

   public RoundedCornersTransform(int var1) {
      this.radius = var1;
      this.id = "io.intercom.android.sdk.transforms.RoundedCornersTransform.(radius=" + var1 + ")" + 1;
      this.idBytes = this.id.getBytes(a);
   }

   protected Bitmap transform(final e var1, Bitmap var2, int var3, int var4) {
      return BitmapUtils.transformRoundCorners(var2, new BitmapUtils.BitmapCache() {
         public Bitmap get(int var1x, int var2, Config var3) {
            return var1.a(var1x, var2, var3);
         }
      }, this.radius);
   }

   public void updateDiskCacheKey(MessageDigest var1) {
      var1.update(this.idBytes);
   }
}
