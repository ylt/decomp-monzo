package io.intercom.android.sdk.transforms;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import io.intercom.android.sdk.commons.utilities.BitmapUtils;
import io.intercom.com.bumptech.glide.load.engine.a.e;
import io.intercom.com.bumptech.glide.load.resource.bitmap.f;
import java.security.MessageDigest;

public class RoundTransform extends f {
   private static final String ID = "io.intercom.android.sdk.transforms.RoundTransform.1";
   private static final byte[] ID_BYTES;
   private static final int VERSION = 1;

   static {
      ID_BYTES = "io.intercom.android.sdk.transforms.RoundTransform.1".getBytes(a);
   }

   protected Bitmap transform(final e var1, Bitmap var2, int var3, int var4) {
      return BitmapUtils.transformRound(var2, new BitmapUtils.BitmapCache() {
         public Bitmap get(int var1x, int var2, Config var3) {
            return var1.a(var1x, var2, var3);
         }
      });
   }

   public void updateDiskCacheKey(MessageDigest var1) {
      var1.update(ID_BYTES);
   }
}
