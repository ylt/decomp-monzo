package io.intercom.android.sdk.middleware;

import android.app.Activity;
import io.intercom.android.sdk.Provider;
import io.intercom.android.sdk.actions.Action;
import io.intercom.android.sdk.models.UsersResponse;
import io.intercom.android.sdk.overlay.OverlayManager;
import io.intercom.android.sdk.state.OverlayState;
import io.intercom.android.sdk.store.Selectors;
import io.intercom.android.sdk.store.Store;
import java.util.List;

public class OverlayManagerMiddleware implements Store.Middleware {
   private final Provider managerProvider;

   public OverlayManagerMiddleware(Provider var1) {
      this.managerProvider = var1;
   }

   private OverlayManager manager() {
      return (OverlayManager)this.managerProvider.get();
   }

   private void removeOverlays(Activity var1) {
      if(var1 != null) {
         this.manager().removeOverlaysIfPresent(var1);
      }

   }

   public void dispatch(Store var1, Action var2, Store.NextDispatcher var3) {
      OverlayState var4 = (OverlayState)var1.select(Selectors.OVERLAY);
      Activity var5 = var4.pausedHostActivity();
      Activity var7 = var4.resumedHostActivity();
      switch(null.$SwitchMap$io$intercom$android$sdk$actions$Action$Type[var2.type().ordinal()]) {
      case 1:
         if((Activity)var2.value() != var5) {
            this.removeOverlays(var5);
         }
         break;
      case 2:
         this.removeOverlays(var5);
         break;
      case 3:
         if((Activity)var2.value() == var5) {
            this.removeOverlays(var5);
         }
         break;
      case 4:
         this.manager().cancelAnimations();
         this.removeOverlays(var5);
         this.removeOverlays(var7);
         break;
      case 5:
         List var6 = ((UsersResponse)var2.value()).getUnreadConversations().getConversations();
         this.manager().sendUnreadConversationsReceivedMetric(var6);
      }

      var3.dispatch(var2);
   }
}
