package io.intercom.android.sdk.middleware;

import android.support.v4.g.m;
import io.intercom.android.sdk.Intercom;
import io.intercom.android.sdk.Provider;
import io.intercom.android.sdk.actions.Action;
import io.intercom.android.sdk.conversation.SoundPlayer;
import io.intercom.android.sdk.identity.UserIdentity;
import io.intercom.android.sdk.models.Conversation;
import io.intercom.android.sdk.models.Part;
import io.intercom.android.sdk.state.OverlayState;
import io.intercom.android.sdk.store.Selectors;
import io.intercom.android.sdk.store.Store;
import java.util.Iterator;
import java.util.Set;

public class AudioMiddleware implements Store.Middleware {
   private final m playedPartIdsForConversations = new m();
   private final SoundPlayer soundPlayer;
   private final Provider userIdentityProvider;

   public AudioMiddleware(SoundPlayer var1, Provider var2) {
      this.soundPlayer = var1;
      this.userIdentityProvider = var2;
   }

   private boolean neverPlayedAudioFor(Conversation var1, Part var2) {
      String var4 = (String)this.playedPartIdsForConversations.get(var1.getId());
      boolean var3;
      if(!var2.getId().equals(var4)) {
         var3 = true;
      } else {
         var3 = false;
      }

      return var3;
   }

   private static boolean noActivityForOverlays(OverlayState var0) {
      boolean var1;
      if(var0.resumedHostActivity() == null) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   private static boolean notificationsDisabled(OverlayState var0) {
      boolean var1;
      if(var0.notificationVisibility() != Intercom.Visibility.VISIBLE) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public void dispatch(Store var1, Action var2, Store.NextDispatcher var3) {
      var3.dispatch(var2);
      if(!((UserIdentity)this.userIdentityProvider.get()).isSoftReset()) {
         switch(null.$SwitchMap$io$intercom$android$sdk$actions$Action$Type[var2.type().ordinal()]) {
         case 1:
         case 2:
         case 3:
            OverlayState var7 = (OverlayState)var1.select(Selectors.OVERLAY);
            if(!notificationsDisabled(var7) && !noActivityForOverlays(var7)) {
               Set var6 = var7.dismissedPartIds();
               Iterator var5 = var7.conversations().iterator();
               boolean var4 = false;

               while(true) {
                  Conversation var8;
                  Part var9;
                  do {
                     do {
                        if(!var5.hasNext()) {
                           if(var4) {
                              this.soundPlayer.playMessageReceivedSound();
                           }

                           return;
                        }

                        var8 = (Conversation)var5.next();
                        var9 = var8.getLastPart();
                     } while(var9 == Part.NULL);
                  } while(var6.contains(var9.getId()));

                  if(!var4 && !this.neverPlayedAudioFor(var8, var9)) {
                     var4 = false;
                  } else {
                     var4 = true;
                  }

                  this.playedPartIdsForConversations.put(var8.getId(), var9.getId());
               }
            }
            break;
         case 4:
            this.playedPartIdsForConversations.clear();
         }
      }

   }
}
