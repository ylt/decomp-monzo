package io.intercom.android.sdk.middleware;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.actions.Action;
import io.intercom.android.sdk.models.BaseResponse;
import io.intercom.android.sdk.store.Store;
import io.intercom.android.sdk.twig.Twig;

public class FirstMessageMiddleware implements Store.Middleware {
   private final Context context;
   private final Twig twig;

   public FirstMessageMiddleware(Twig var1, Context var2) {
      this.twig = var1;
      this.context = var2;
   }

   private void showFirstMessageToast() {
      View var2 = LayoutInflater.from(this.context).inflate(R.layout.intercom_onboarding_layout, (ViewGroup)null);
      Toast var1 = new Toast(this.context);
      var1.setGravity(17, 0, 0);
      var1.setDuration(1);
      var1.setView(var2);
      var1.show();
   }

   public void dispatch(Store var1, Action var2, Store.NextDispatcher var3) {
      var3.dispatch(var2);
      switch(null.$SwitchMap$io$intercom$android$sdk$actions$Action$Type[var2.type().ordinal()]) {
      case 1:
         if(((BaseResponse)var2.value()).getConfig().isFirstRequest()) {
            this.twig.i(this.context.getString(R.string.intercom_android_activated_message), new Object[0]);

            try {
               this.showFirstMessageToast();
            } catch (Exception var4) {
               this.twig.internal("Couldn't show first message toast: " + var4.getMessage());
            }
         }
      default:
      }
   }
}
