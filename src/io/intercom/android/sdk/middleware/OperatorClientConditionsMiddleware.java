package io.intercom.android.sdk.middleware;

import android.os.Handler;
import io.intercom.android.sdk.Provider;
import io.intercom.android.sdk.actions.Action;
import io.intercom.android.sdk.api.Api;
import io.intercom.android.sdk.models.Conversation;
import io.intercom.android.sdk.models.OperatorClientCondition;
import io.intercom.android.sdk.operator.OperatorClientConditionTimer;
import io.intercom.android.sdk.state.ActiveConversationState;
import io.intercom.android.sdk.state.State;
import io.intercom.android.sdk.store.Store;
import io.intercom.android.sdk.views.ClientConditionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class OperatorClientConditionsMiddleware implements Store.Middleware, ClientConditionListener {
   private static final String USER_INTERACTION_CONDITION = "user_interaction_condition";
   private final Provider apiProvider;
   final Map conversationConditions = new HashMap();
   private final Handler handler;

   public OperatorClientConditionsMiddleware(Provider var1, Handler var2) {
      this.apiProvider = var1;
      this.handler = var2;
   }

   private void addClientConditionsForConversation(String var1, List var2, ActiveConversationState var3) {
      if(!var2.isEmpty()) {
         Object var4 = (List)this.conversationConditions.get(var1);
         if(var4 == null) {
            var4 = new ArrayList();
            this.conversationConditions.put(var1, var4);
         }

         Iterator var6 = var2.iterator();

         while(var6.hasNext()) {
            OperatorClientCondition var5 = (OperatorClientCondition)var6.next();
            if("user_interaction_condition".equals(var5.conditionId())) {
               this.createOperatorClientConditionTimer(var1, var3, (List)var4, var5);
            }
         }
      }

   }

   private void createOperatorClientConditionTimer(String var1, ActiveConversationState var2, List var3, OperatorClientCondition var4) {
      OperatorClientConditionTimer var5 = new OperatorClientConditionTimer(var1, var4.id(), this.handler, this);
      var3.add(var5);
      var5.beginCountdown();
      if(this.userHasInteractedWithConversation(var1, var2)) {
         var5.interrupt();
      }

   }

   private void interruptCondition(String var1) {
      List var2 = (List)this.conversationConditions.get(var1);
      if(var2 != null) {
         Iterator var3 = var2.iterator();

         while(var3.hasNext()) {
            ((OperatorClientConditionTimer)var3.next()).interrupt();
         }
      }

   }

   private boolean userHasInteractedWithConversation(String var1, ActiveConversationState var2) {
      boolean var3;
      if(!var2.getConversationId().equals(var1) || !var2.hasSwitchedInputType() && !var2.hasTextInComposer()) {
         var3 = false;
      } else {
         var3 = true;
      }

      return var3;
   }

   public void conditionSatisfied(String var1, String var2) {
      Iterator var3 = ((List)this.conversationConditions.get(var1)).iterator();

      while(var3.hasNext()) {
         if(((OperatorClientConditionTimer)var3.next()).getCondition().equals(var2)) {
            var3.remove();
         }
      }

      ((Api)this.apiProvider.get()).satisfyOperatorCondition(var1, var2);
   }

   public void dispatch(Store var1, Action var2, Store.NextDispatcher var3) {
      var3.dispatch(var2);
      switch(null.$SwitchMap$io$intercom$android$sdk$actions$Action$Type[var2.type().ordinal()]) {
      case 1:
      case 2:
      case 3:
         this.interruptCondition(((State)var1.state()).activeConversationState().getConversationId());
         break;
      case 4:
      case 5:
         Conversation var4 = (Conversation)var2.value();
         this.addClientConditionsForConversation(var4.getId(), var4.getOperatorClientConditions(), ((State)var1.state()).activeConversationState());
      }

   }
}
