package io.intercom.android.sdk.middleware;

import io.intercom.android.sdk.Provider;
import io.intercom.android.sdk.actions.Action;
import io.intercom.android.sdk.api.Api;
import io.intercom.android.sdk.models.Conversation;
import io.intercom.android.sdk.state.OverlayState;
import io.intercom.android.sdk.store.Selectors;
import io.intercom.android.sdk.store.Store;

public class ApiMiddleware implements Store.Middleware {
   private final Provider apiProvider;

   public ApiMiddleware(Provider var1) {
      this.apiProvider = var1;
   }

   private Api api() {
      return (Api)this.apiProvider.get();
   }

   private void fetchDataForCurrentScreen(Store var1, String var2) {
      if(((OverlayState)var1.select(Selectors.OVERLAY)).resumedHostActivity() == null && !((Boolean)var1.select(Selectors.APP_IS_BACKGROUNDED)).booleanValue()) {
         this.api().getConversation(var2);
      } else {
         this.api().getUnreadConversations();
      }

   }

   public void dispatch(Store var1, Action var2, Store.NextDispatcher var3) {
      var3.dispatch(var2);
      switch(null.$SwitchMap$io$intercom$android$sdk$actions$Action$Type[var2.type().ordinal()]) {
      case 1:
         Conversation var5 = (Conversation)var2.value();
         this.api().markConversationAsDismissed(var5.getId());
         break;
      case 2:
         int var4 = ((OverlayState)var1.select(Selectors.OVERLAY)).conversations().size();
         if(var4 == 1 && ((Integer)var1.select(Selectors.UNREAD_COUNT)).intValue() > var4) {
            this.api().getUnreadConversations();
         }
         break;
      case 3:
         this.api().getInbox();
         break;
      case 4:
         this.api().getInboxBefore(((Long)var2.value()).longValue());
         break;
      case 5:
         this.fetchDataForCurrentScreen(var1, (String)var2.value());
      }

   }
}
