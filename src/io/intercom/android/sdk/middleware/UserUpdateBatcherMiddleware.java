package io.intercom.android.sdk.middleware;

import io.intercom.android.sdk.Provider;
import io.intercom.android.sdk.actions.Action;
import io.intercom.android.sdk.api.UserUpdateBatcher;
import io.intercom.android.sdk.api.UserUpdateRequest;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.android.sdk.identity.UserIdentity;
import io.intercom.android.sdk.state.State;
import io.intercom.android.sdk.store.Selectors;
import io.intercom.android.sdk.store.Store;

public class UserUpdateBatcherMiddleware implements Store.Middleware {
   private final Provider appConfigProvider;
   private final Provider userIdentityProvider;
   private final Provider userUpdateBatcher;

   public UserUpdateBatcherMiddleware(Provider var1, Provider var2, Provider var3) {
      this.appConfigProvider = var2;
      this.userUpdateBatcher = var1;
      this.userIdentityProvider = var3;
   }

   public void dispatch(Store var1, Action var2, Store.NextDispatcher var3) {
      var3.dispatch(var2);
      switch(null.$SwitchMap$io$intercom$android$sdk$actions$Action$Type[var2.type().ordinal()]) {
      case 1:
         if(((Long)var2.value()).longValue() - ((State)var1.state()).hostAppState().backgroundedTimestamp() > ((AppConfig)this.appConfigProvider.get()).getNewSessionThresholdMs() && ((UserIdentity)this.userIdentityProvider.get()).identityExists() && !((Boolean)var1.select(Selectors.SESSION_STARTED_SINCE_LAST_BACKGROUNDED)).booleanValue()) {
            Boolean var4 = (Boolean)var1.select(Selectors.APP_IS_BACKGROUNDED);
            ((UserUpdateBatcher)this.userUpdateBatcher.get()).updateUser(new UserUpdateRequest(true, var4.booleanValue(), true));
         }
      default:
      }
   }
}
