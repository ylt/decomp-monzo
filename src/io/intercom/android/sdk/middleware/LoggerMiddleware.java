package io.intercom.android.sdk.middleware;

import android.app.Activity;
import io.intercom.android.sdk.actions.Action;
import io.intercom.android.sdk.store.Store;
import io.intercom.android.sdk.twig.Twig;

public class LoggerMiddleware implements Store.Middleware {
   private final Twig twig;

   public LoggerMiddleware(Twig var1) {
      this.twig = var1;
   }

   public void dispatch(Store var1, Action var2, Store.NextDispatcher var3) {
      var3.dispatch(var2);
      this.twig.i("Dispatched Action: " + var2.toString(), new Object[0]);
      Activity var4;
      switch(null.$SwitchMap$io$intercom$android$sdk$actions$Action$Type[var2.type().ordinal()]) {
      case 1:
         var4 = (Activity)var2.value();
         this.twig.i("Ready to show in-app messages in " + var4, new Object[0]);
         break;
      case 2:
         var4 = (Activity)var2.value();
         this.twig.i(var4 + " was paused", new Object[0]);
      }

   }
}
