package io.intercom.android.sdk.middleware;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.net.Uri;
import android.provider.MediaStore.Images.Media;
import android.support.v7.a.a.i;
import io.intercom.android.sdk.Provider;
import io.intercom.android.sdk.actions.Action;
import io.intercom.android.sdk.activities.IntercomMessengerActivity;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.android.sdk.overlay.ScreenshotContentObserver;
import io.intercom.android.sdk.state.OverlayState;
import io.intercom.android.sdk.state.State;
import io.intercom.android.sdk.store.Selectors;
import io.intercom.android.sdk.store.Store;
import io.intercom.android.sdk.utilities.ActivityUtils;
import io.intercom.android.sdk.utilities.StoreUtils;

public class ScreenshotMiddleware implements Store.Middleware {
   private final Provider appConfigProvider;
   private final Context context;
   private Runnable dialogDismiss;
   private Store.Subscription observerSubscription;

   public ScreenshotMiddleware(Provider var1, Context var2) {
      this.appConfigProvider = var1;
      this.context = var2;
   }

   private void showShareDialog(Store var1) {
      final Activity var2 = ((OverlayState)var1.select(Selectors.OVERLAY)).resumedHostActivity();
      if(var2 != null) {
         var2.runOnUiThread(new Runnable(((AppConfig)this.appConfigProvider.get()).getName()) {
            // $FF: synthetic field
            final String val$appName;

            {
               this.val$appName = var3;
            }

            public void run() {
               final AlertDialog var1 = (new Builder(var2, i.Theme_AppCompat_Light_Dialog_Alert)).setTitle("Share screenshot?").setMessage("Edit and share this screenshot with " + this.val$appName + "?").setPositiveButton("Share", new OnClickListener() {
                  public void onClick(DialogInterface var1, int var2x) {
                     if(!ActivityUtils.isNotActive(var2)) {
                        var2.startActivity(IntercomMessengerActivity.openComposer(var2, ""));
                     }

                  }
               }).create();
               if(ScreenshotMiddleware.this.dialogDismiss != null) {
                  ScreenshotMiddleware.this.dialogDismiss.run();
               }

               ScreenshotMiddleware.this.dialogDismiss = new Runnable() {
                  public void run() {
                     var1.dismiss();
                  }
               };
               var1.show();
            }
         });
      }

   }

   private void startDetector(Store var1) {
      StoreUtils.safeUnsubscribe(this.observerSubscription);
      final ContentResolver var2 = this.context.getContentResolver();
      final ScreenshotContentObserver var3 = new ScreenshotContentObserver(var1, var2);
      var2.registerContentObserver(Media.EXTERNAL_CONTENT_URI, true, var3);
      this.observerSubscription = new Store.Subscription() {
         public void unsubscribe() {
            var2.unregisterContentObserver(var3);
         }
      };
   }

   public void dispatch(Store var1, Action var2, Store.NextDispatcher var3) {
      var3.dispatch(var2);
      if(((AppConfig)this.appConfigProvider.get()).hasFeature("screenshot-sharing")) {
         switch(null.$SwitchMap$io$intercom$android$sdk$actions$Action$Type[var2.type().ordinal()]) {
         case 1:
            this.startDetector(var1);
            break;
         case 2:
            this.showShareDialog(var1);
            break;
         case 3:
            if(((State)var1.state()).lastScreenshot() == Uri.EMPTY && this.dialogDismiss != null) {
               this.dialogDismiss.run();
            }
            break;
         case 4:
            StoreUtils.safeUnsubscribe(this.observerSubscription);
         }
      }

   }
}
