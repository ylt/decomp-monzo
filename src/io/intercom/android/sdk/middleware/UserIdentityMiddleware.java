package io.intercom.android.sdk.middleware;

import io.intercom.android.sdk.Provider;
import io.intercom.android.sdk.actions.Action;
import io.intercom.android.sdk.identity.UserIdentity;
import io.intercom.android.sdk.models.BaseResponse;
import io.intercom.android.sdk.store.Store;

public class UserIdentityMiddleware implements Store.Middleware {
   private final Provider userIdentityProvider;

   public UserIdentityMiddleware(Provider var1) {
      this.userIdentityProvider = var1;
   }

   public void dispatch(Store var1, Action var2, Store.NextDispatcher var3) {
      var3.dispatch(var2);
      switch(null.$SwitchMap$io$intercom$android$sdk$actions$Action$Type[var2.type().ordinal()]) {
      case 1:
         BaseResponse var4 = (BaseResponse)var2.value();
         ((UserIdentity)this.userIdentityProvider.get()).update(var4.getUser());
         break;
      case 2:
         ((UserIdentity)this.userIdentityProvider.get()).softReset();
         break;
      case 3:
         ((UserIdentity)this.userIdentityProvider.get()).hardReset();
      }

   }
}
