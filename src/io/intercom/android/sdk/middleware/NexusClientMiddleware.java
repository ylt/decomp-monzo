package io.intercom.android.sdk.middleware;

import android.os.Handler;
import io.intercom.android.sdk.Provider;
import io.intercom.android.sdk.actions.Action;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.android.sdk.models.BaseResponse;
import io.intercom.android.sdk.nexus.NexusClient;
import io.intercom.android.sdk.nexus.NexusConfig;
import io.intercom.android.sdk.store.Selectors;
import io.intercom.android.sdk.store.Store;

public class NexusClientMiddleware implements Store.Middleware {
   private final Provider appConfigProvider;
   private final Runnable disconnectRunnable = new Runnable() {
      public void run() {
         NexusClientMiddleware.this.client().disconnect();
      }
   };
   private final Handler handler;
   private final Provider nexusClient;

   public NexusClientMiddleware(Provider var1, Provider var2, Handler var3) {
      this.nexusClient = var1;
      this.appConfigProvider = var2;
      this.handler = var3;
   }

   private void connectWithConfig(NexusConfig var1) {
      this.handler.removeCallbacks(this.disconnectRunnable);
      this.client().connect(var1, true);
   }

   private void disconnectImmediately() {
      this.handler.removeCallbacks(this.disconnectRunnable);
      this.client().disconnect();
   }

   private void scheduleDisconnect() {
      this.handler.removeCallbacks(this.disconnectRunnable);
      this.handler.postDelayed(this.disconnectRunnable, ((AppConfig)this.appConfigProvider.get()).getNewSessionThresholdMs());
   }

   NexusClient client() {
      return (NexusClient)this.nexusClient.get();
   }

   public void dispatch(Store var1, Action var2, Store.NextDispatcher var3) {
      var3.dispatch(var2);
      switch(null.$SwitchMap$io$intercom$android$sdk$actions$Action$Type[var2.type().ordinal()]) {
      case 1:
         this.connectWithConfig(((AppConfig)this.appConfigProvider.get()).getRealTimeConfig());
         break;
      case 2:
         this.scheduleDisconnect();
         break;
      case 3:
         this.disconnectImmediately();
         break;
      case 4:
         NexusConfig var4 = ((BaseResponse)var2.value()).getConfig().getRealTimeConfig();
         if(var4.getEndpoints().isEmpty()) {
            this.disconnectImmediately();
         } else if(!((Boolean)var1.select(Selectors.APP_IS_BACKGROUNDED)).booleanValue()) {
            this.connectWithConfig(var4);
         }
      }

   }
}
