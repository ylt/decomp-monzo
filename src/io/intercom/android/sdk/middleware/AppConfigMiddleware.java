package io.intercom.android.sdk.middleware;

import io.intercom.android.sdk.Provider;
import io.intercom.android.sdk.actions.Action;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.android.sdk.models.BaseResponse;
import io.intercom.android.sdk.store.Store;
import io.intercom.com.a.a.b;

public class AppConfigMiddleware implements Store.Middleware {
   private final Provider appConfigProvider;
   private final b bus;

   public AppConfigMiddleware(Provider var1, b var2) {
      this.appConfigProvider = var1;
      this.bus = var2;
   }

   public void dispatch(Store var1, Action var2, Store.NextDispatcher var3) {
      var3.dispatch(var2);
      switch(null.$SwitchMap$io$intercom$android$sdk$actions$Action$Type[var2.type().ordinal()]) {
      case 1:
         BaseResponse var4 = (BaseResponse)var2.value();
         ((AppConfig)this.appConfigProvider.get()).update(var4.getConfig(), this.bus);
         break;
      case 2:
         ((AppConfig)this.appConfigProvider.get()).resetRealTimeConfig();
      }

   }
}
