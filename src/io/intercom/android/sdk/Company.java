package io.intercom.android.sdk;

import io.intercom.android.sdk.logger.LumberMill;
import io.intercom.android.sdk.twig.Twig;
import io.intercom.android.sdk.utilities.CustomAttributeValidator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class Company {
   private static final String COMPANY_ID = "id";
   private static final String CREATED_AT = "created_at";
   private static final String CUSTOM_ATTRIBUTES = "custom_attributes";
   private static final String MONTHLY_SPEND = "monthly_spend";
   private static final String NAME = "name";
   private static final String PLAN = "plan";
   private static final Twig TWIG = LumberMill.getLogger();
   private final Map attributes;
   private final Map customAttributes;

   Company(Company.Builder var1) {
      this.attributes = var1.attributes;
      this.customAttributes = var1.customAttributes;
   }

   Map getAttributes() {
      if(!this.customAttributes.isEmpty()) {
         this.attributes.put("custom_attributes", this.customAttributes);
      }

      return this.attributes;
   }

   public static final class Builder {
      final Map attributes = new HashMap();
      final Map customAttributes = new HashMap();

      public Company build() {
         return new Company(this);
      }

      public Company.Builder withCompanyId(String var1) {
         this.attributes.put("id", var1);
         return this;
      }

      public Company.Builder withCreatedAt(Long var1) {
         this.attributes.put("created_at", var1);
         return this;
      }

      public Company.Builder withCustomAttribute(String var1, Object var2) {
         if(var1 == null) {
            Company.TWIG.w("The key you provided was null for the attribute " + var2, new Object[0]);
         } else if(CustomAttributeValidator.isValid(var2)) {
            this.customAttributes.put(var1, var2);
         } else {
            Company.TWIG.w("The custom company attribute " + var1 + " was of type " + var2.getClass().getSimpleName() + " We only accept the following types: " + CustomAttributeValidator.getAcceptedTypes(), new Object[0]);
         }

         return this;
      }

      public Company.Builder withCustomAttributes(Map var1) {
         if(var1 == null) {
            Company.TWIG.w("The map of attributes you provided was null.", new Object[0]);
         } else {
            Iterator var3 = var1.entrySet().iterator();

            while(var3.hasNext()) {
               Entry var2 = (Entry)var3.next();
               if(CustomAttributeValidator.isValid(var2.getValue())) {
                  this.customAttributes.put(var2.getKey(), var2.getValue());
               } else {
                  Company.TWIG.w("The custom company attribute " + (String)var2.getKey() + " was of type " + var2.getClass().getSimpleName() + " We only accept the following types: " + CustomAttributeValidator.getAcceptedTypes(), new Object[0]);
               }
            }
         }

         return this;
      }

      public Company.Builder withMonthlySpend(Integer var1) {
         this.attributes.put("monthly_spend", var1);
         return this;
      }

      public Company.Builder withName(String var1) {
         this.attributes.put("name", var1);
         return this;
      }

      public Company.Builder withPlan(String var1) {
         this.attributes.put("plan", var1);
         return this;
      }
   }
}
