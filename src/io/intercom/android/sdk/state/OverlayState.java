package io.intercom.android.sdk.state;

import android.app.Activity;
import io.intercom.android.sdk.Intercom;
import java.util.List;
import java.util.Set;

public abstract class OverlayState {
   public static OverlayState.Builder builder() {
      return new AutoValue_OverlayState.Builder();
   }

   public static OverlayState create(List var0, Set var1, Intercom.Visibility var2, Intercom.Visibility var3, Activity var4, Activity var5, int var6) {
      return builder().conversations(var0).dismissedPartIds(var1).notificationVisibility(var2).launcherVisibility(var3).bottomPadding(var6).resumedHostActivity(var4).pausedHostActivity(var5).build();
   }

   public abstract int bottomPadding();

   public abstract List conversations();

   public abstract Set dismissedPartIds();

   public abstract Intercom.Visibility launcherVisibility();

   public abstract Intercom.Visibility notificationVisibility();

   public abstract Activity pausedHostActivity();

   public abstract Activity resumedHostActivity();

   public abstract OverlayState.Builder toBuilder();

   public abstract static class Builder {
      public abstract OverlayState.Builder bottomPadding(int var1);

      public abstract OverlayState build();

      public abstract OverlayState.Builder conversations(List var1);

      public abstract OverlayState.Builder dismissedPartIds(Set var1);

      public abstract OverlayState.Builder launcherVisibility(Intercom.Visibility var1);

      public abstract OverlayState.Builder notificationVisibility(Intercom.Visibility var1);

      public abstract OverlayState.Builder pausedHostActivity(Activity var1);

      public abstract OverlayState.Builder resumedHostActivity(Activity var1);
   }
}
