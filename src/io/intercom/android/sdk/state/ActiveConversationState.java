package io.intercom.android.sdk.state;

public abstract class ActiveConversationState {
   public static ActiveConversationState create(String var0, boolean var1, boolean var2) {
      return new AutoValue_ActiveConversationState(var0, var1, var2);
   }

   public abstract String getConversationId();

   public abstract boolean hasSwitchedInputType();

   public abstract boolean hasTextInComposer();
}
