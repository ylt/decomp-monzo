package io.intercom.android.sdk.state;

import io.intercom.android.sdk.models.Part;

public class ReplyPart {
   private final String conversationId;
   private final Part part;

   public ReplyPart(Part var1, String var2) {
      this.part = var1;
      this.conversationId = var2;
   }

   public String getConversationId() {
      return this.conversationId;
   }

   public Part getPart() {
      return this.part;
   }
}
