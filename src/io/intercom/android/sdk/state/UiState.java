package io.intercom.android.sdk.state;

public abstract class UiState {
   public static UiState create(UiState.Screen var0, String var1) {
      return new AutoValue_UiState(var0, var1);
   }

   public abstract String conversationId();

   public abstract UiState.Screen screen();

   public static enum Screen {
      COMPOSER,
      CONVERSATION,
      INBOX,
      NONE;
   }
}
