package io.intercom.android.sdk.state;

import java.util.List;

final class AutoValue_InboxState extends InboxState {
   private final List conversations;
   private final boolean hasMorePages;
   private final InboxState.Status status;

   AutoValue_InboxState(List var1, InboxState.Status var2, boolean var3) {
      if(var1 == null) {
         throw new NullPointerException("Null conversations");
      } else {
         this.conversations = var1;
         if(var2 == null) {
            throw new NullPointerException("Null status");
         } else {
            this.status = var2;
            this.hasMorePages = var3;
         }
      }
   }

   public List conversations() {
      return this.conversations;
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(var1 != this) {
         if(var1 instanceof InboxState) {
            InboxState var3 = (InboxState)var1;
            if(!this.conversations.equals(var3.conversations()) || !this.status.equals(var3.status()) || this.hasMorePages != var3.hasMorePages()) {
               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public boolean hasMorePages() {
      return this.hasMorePages;
   }

   public int hashCode() {
      int var2 = this.conversations.hashCode();
      int var3 = this.status.hashCode();
      short var1;
      if(this.hasMorePages) {
         var1 = 1231;
      } else {
         var1 = 1237;
      }

      return var1 ^ ((var2 ^ 1000003) * 1000003 ^ var3) * 1000003;
   }

   public InboxState.Status status() {
      return this.status;
   }

   public String toString() {
      return "InboxState{conversations=" + this.conversations + ", status=" + this.status + ", hasMorePages=" + this.hasMorePages + "}";
   }
}
