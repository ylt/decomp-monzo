package io.intercom.android.sdk.state;

public abstract class HostAppState {
   public static HostAppState create(boolean var0, boolean var1, long var2) {
      return new AutoValue_HostAppState(var0, var1, var2);
   }

   public abstract long backgroundedTimestamp();

   public abstract boolean isBackgrounded();

   public abstract boolean sessionStartedSinceLastBackgrounded();
}
