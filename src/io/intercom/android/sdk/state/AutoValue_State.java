package io.intercom.android.sdk.state;

import android.net.Uri;
import io.intercom.android.sdk.models.TeamPresence;
import java.util.Set;

final class AutoValue_State extends State {
   private final ActiveConversationState activeConversationState;
   private final boolean hasConversations;
   private final HostAppState hostAppState;
   private final InboxState inboxState;
   private final Uri lastScreenshot;
   private final OverlayState overlayState;
   private final TeamPresence teamPresence;
   private final UiState uiState;
   private final Set unreadConversationIds;

   AutoValue_State(boolean var1, UiState var2, TeamPresence var3, Set var4, InboxState var5, HostAppState var6, OverlayState var7, Uri var8, ActiveConversationState var9) {
      this.hasConversations = var1;
      if(var2 == null) {
         throw new NullPointerException("Null uiState");
      } else {
         this.uiState = var2;
         if(var3 == null) {
            throw new NullPointerException("Null teamPresence");
         } else {
            this.teamPresence = var3;
            if(var4 == null) {
               throw new NullPointerException("Null unreadConversationIds");
            } else {
               this.unreadConversationIds = var4;
               if(var5 == null) {
                  throw new NullPointerException("Null inboxState");
               } else {
                  this.inboxState = var5;
                  if(var6 == null) {
                     throw new NullPointerException("Null hostAppState");
                  } else {
                     this.hostAppState = var6;
                     if(var7 == null) {
                        throw new NullPointerException("Null overlayState");
                     } else {
                        this.overlayState = var7;
                        if(var8 == null) {
                           throw new NullPointerException("Null lastScreenshot");
                        } else {
                           this.lastScreenshot = var8;
                           if(var9 == null) {
                              throw new NullPointerException("Null activeConversationState");
                           } else {
                              this.activeConversationState = var9;
                           }
                        }
                     }
                  }
               }
            }
         }
      }
   }

   public ActiveConversationState activeConversationState() {
      return this.activeConversationState;
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(var1 != this) {
         if(var1 instanceof State) {
            State var3 = (State)var1;
            if(this.hasConversations != var3.hasConversations() || !this.uiState.equals(var3.uiState()) || !this.teamPresence.equals(var3.teamPresence()) || !this.unreadConversationIds.equals(var3.unreadConversationIds()) || !this.inboxState.equals(var3.inboxState()) || !this.hostAppState.equals(var3.hostAppState()) || !this.overlayState.equals(var3.overlayState()) || !this.lastScreenshot.equals(var3.lastScreenshot()) || !this.activeConversationState.equals(var3.activeConversationState())) {
               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public boolean hasConversations() {
      return this.hasConversations;
   }

   public int hashCode() {
      short var1;
      if(this.hasConversations) {
         var1 = 1231;
      } else {
         var1 = 1237;
      }

      return ((((((((var1 ^ 1000003) * 1000003 ^ this.uiState.hashCode()) * 1000003 ^ this.teamPresence.hashCode()) * 1000003 ^ this.unreadConversationIds.hashCode()) * 1000003 ^ this.inboxState.hashCode()) * 1000003 ^ this.hostAppState.hashCode()) * 1000003 ^ this.overlayState.hashCode()) * 1000003 ^ this.lastScreenshot.hashCode()) * 1000003 ^ this.activeConversationState.hashCode();
   }

   public HostAppState hostAppState() {
      return this.hostAppState;
   }

   public InboxState inboxState() {
      return this.inboxState;
   }

   public Uri lastScreenshot() {
      return this.lastScreenshot;
   }

   public OverlayState overlayState() {
      return this.overlayState;
   }

   public TeamPresence teamPresence() {
      return this.teamPresence;
   }

   public String toString() {
      return "State{hasConversations=" + this.hasConversations + ", uiState=" + this.uiState + ", teamPresence=" + this.teamPresence + ", unreadConversationIds=" + this.unreadConversationIds + ", inboxState=" + this.inboxState + ", hostAppState=" + this.hostAppState + ", overlayState=" + this.overlayState + ", lastScreenshot=" + this.lastScreenshot + ", activeConversationState=" + this.activeConversationState + "}";
   }

   public UiState uiState() {
      return this.uiState;
   }

   public Set unreadConversationIds() {
      return this.unreadConversationIds;
   }
}
