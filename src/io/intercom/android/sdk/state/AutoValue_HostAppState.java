package io.intercom.android.sdk.state;

final class AutoValue_HostAppState extends HostAppState {
   private final long backgroundedTimestamp;
   private final boolean isBackgrounded;
   private final boolean sessionStartedSinceLastBackgrounded;

   AutoValue_HostAppState(boolean var1, boolean var2, long var3) {
      this.isBackgrounded = var1;
      this.sessionStartedSinceLastBackgrounded = var2;
      this.backgroundedTimestamp = var3;
   }

   public long backgroundedTimestamp() {
      return this.backgroundedTimestamp;
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(var1 != this) {
         if(var1 instanceof HostAppState) {
            HostAppState var3 = (HostAppState)var1;
            if(this.isBackgrounded != var3.isBackgrounded() || this.sessionStartedSinceLastBackgrounded != var3.sessionStartedSinceLastBackgrounded() || this.backgroundedTimestamp != var3.backgroundedTimestamp()) {
               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public int hashCode() {
      short var2 = 1231;
      short var1;
      if(this.isBackgrounded) {
         var1 = 1231;
      } else {
         var1 = 1237;
      }

      if(!this.sessionStartedSinceLastBackgrounded) {
         var2 = 1237;
      }

      return (int)((long)(((var1 ^ 1000003) * 1000003 ^ var2) * 1000003) ^ this.backgroundedTimestamp >>> 32 ^ this.backgroundedTimestamp);
   }

   public boolean isBackgrounded() {
      return this.isBackgrounded;
   }

   public boolean sessionStartedSinceLastBackgrounded() {
      return this.sessionStartedSinceLastBackgrounded;
   }

   public String toString() {
      return "HostAppState{isBackgrounded=" + this.isBackgrounded + ", sessionStartedSinceLastBackgrounded=" + this.sessionStartedSinceLastBackgrounded + ", backgroundedTimestamp=" + this.backgroundedTimestamp + "}";
   }
}
