package io.intercom.android.sdk.state;

final class AutoValue_UiState extends UiState {
   private final String conversationId;
   private final UiState.Screen screen;

   AutoValue_UiState(UiState.Screen var1, String var2) {
      if(var1 == null) {
         throw new NullPointerException("Null screen");
      } else {
         this.screen = var1;
         this.conversationId = var2;
      }
   }

   public String conversationId() {
      return this.conversationId;
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(var1 != this) {
         if(var1 instanceof UiState) {
            UiState var3 = (UiState)var1;
            if(this.screen.equals(var3.screen())) {
               if(this.conversationId == null) {
                  if(var3.conversationId() == null) {
                     return var2;
                  }
               } else if(this.conversationId.equals(var3.conversationId())) {
                  return var2;
               }
            }

            var2 = false;
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public int hashCode() {
      int var2 = this.screen.hashCode();
      int var1;
      if(this.conversationId == null) {
         var1 = 0;
      } else {
         var1 = this.conversationId.hashCode();
      }

      return var1 ^ 1000003 * (var2 ^ 1000003);
   }

   public UiState.Screen screen() {
      return this.screen;
   }

   public String toString() {
      return "UiState{screen=" + this.screen + ", conversationId=" + this.conversationId + "}";
   }
}
