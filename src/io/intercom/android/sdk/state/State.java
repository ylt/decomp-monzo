package io.intercom.android.sdk.state;

import android.net.Uri;
import io.intercom.android.sdk.models.TeamPresence;
import java.util.Set;

public abstract class State {
   public static State create(boolean var0, UiState var1, TeamPresence var2, Set var3, InboxState var4, HostAppState var5, OverlayState var6, Uri var7, ActiveConversationState var8) {
      return new AutoValue_State(var0, var1, var2, var3, var4, var5, var6, var7, var8);
   }

   public abstract ActiveConversationState activeConversationState();

   public abstract boolean hasConversations();

   public abstract HostAppState hostAppState();

   public abstract InboxState inboxState();

   public abstract Uri lastScreenshot();

   public abstract OverlayState overlayState();

   public abstract TeamPresence teamPresence();

   public abstract UiState uiState();

   public abstract Set unreadConversationIds();
}
