package io.intercom.android.sdk.state;

final class AutoValue_ActiveConversationState extends ActiveConversationState {
   private final String getConversationId;
   private final boolean hasSwitchedInputType;
   private final boolean hasTextInComposer;

   AutoValue_ActiveConversationState(String var1, boolean var2, boolean var3) {
      if(var1 == null) {
         throw new NullPointerException("Null getConversationId");
      } else {
         this.getConversationId = var1;
         this.hasSwitchedInputType = var2;
         this.hasTextInComposer = var3;
      }
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(var1 != this) {
         if(var1 instanceof ActiveConversationState) {
            ActiveConversationState var3 = (ActiveConversationState)var1;
            if(!this.getConversationId.equals(var3.getConversationId()) || this.hasSwitchedInputType != var3.hasSwitchedInputType() || this.hasTextInComposer != var3.hasTextInComposer()) {
               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public String getConversationId() {
      return this.getConversationId;
   }

   public boolean hasSwitchedInputType() {
      return this.hasSwitchedInputType;
   }

   public boolean hasTextInComposer() {
      return this.hasTextInComposer;
   }

   public int hashCode() {
      short var2 = 1231;
      int var3 = this.getConversationId.hashCode();
      short var1;
      if(this.hasSwitchedInputType) {
         var1 = 1231;
      } else {
         var1 = 1237;
      }

      if(!this.hasTextInComposer) {
         var2 = 1237;
      }

      return (var1 ^ (var3 ^ 1000003) * 1000003) * 1000003 ^ var2;
   }

   public String toString() {
      return "ActiveConversationState{getConversationId=" + this.getConversationId + ", hasSwitchedInputType=" + this.hasSwitchedInputType + ", hasTextInComposer=" + this.hasTextInComposer + "}";
   }
}
