package io.intercom.android.sdk.state;

import android.app.Activity;
import io.intercom.android.sdk.Intercom;
import java.util.List;
import java.util.Set;

final class AutoValue_OverlayState extends OverlayState {
   private final int bottomPadding;
   private final List conversations;
   private final Set dismissedPartIds;
   private final Intercom.Visibility launcherVisibility;
   private final Intercom.Visibility notificationVisibility;
   private final Activity pausedHostActivity;
   private final Activity resumedHostActivity;

   private AutoValue_OverlayState(List var1, Set var2, Intercom.Visibility var3, Intercom.Visibility var4, int var5, Activity var6, Activity var7) {
      this.conversations = var1;
      this.dismissedPartIds = var2;
      this.notificationVisibility = var3;
      this.launcherVisibility = var4;
      this.bottomPadding = var5;
      this.resumedHostActivity = var6;
      this.pausedHostActivity = var7;
   }

   // $FF: synthetic method
   AutoValue_OverlayState(List var1, Set var2, Intercom.Visibility var3, Intercom.Visibility var4, int var5, Activity var6, Activity var7, Object var8) {
      this(var1, var2, var3, var4, var5, var6, var7);
   }

   public int bottomPadding() {
      return this.bottomPadding;
   }

   public List conversations() {
      return this.conversations;
   }

   public Set dismissedPartIds() {
      return this.dismissedPartIds;
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(var1 != this) {
         if(var1 instanceof OverlayState) {
            OverlayState var3 = (OverlayState)var1;
            if(this.conversations.equals(var3.conversations()) && this.dismissedPartIds.equals(var3.dismissedPartIds()) && this.notificationVisibility.equals(var3.notificationVisibility()) && this.launcherVisibility.equals(var3.launcherVisibility()) && this.bottomPadding == var3.bottomPadding()) {
               label30: {
                  if(this.resumedHostActivity == null) {
                     if(var3.resumedHostActivity() != null) {
                        break label30;
                     }
                  } else if(!this.resumedHostActivity.equals(var3.resumedHostActivity())) {
                     break label30;
                  }

                  if(this.pausedHostActivity == null) {
                     if(var3.pausedHostActivity() == null) {
                        return var2;
                     }
                  } else if(this.pausedHostActivity.equals(var3.pausedHostActivity())) {
                     return var2;
                  }
               }
            }

            var2 = false;
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public int hashCode() {
      int var2 = 0;
      int var3 = this.conversations.hashCode();
      int var5 = this.dismissedPartIds.hashCode();
      int var6 = this.notificationVisibility.hashCode();
      int var4 = this.launcherVisibility.hashCode();
      int var7 = this.bottomPadding;
      int var1;
      if(this.resumedHostActivity == null) {
         var1 = 0;
      } else {
         var1 = this.resumedHostActivity.hashCode();
      }

      if(this.pausedHostActivity != null) {
         var2 = this.pausedHostActivity.hashCode();
      }

      return (var1 ^ (((((var3 ^ 1000003) * 1000003 ^ var5) * 1000003 ^ var6) * 1000003 ^ var4) * 1000003 ^ var7) * 1000003) * 1000003 ^ var2;
   }

   public Intercom.Visibility launcherVisibility() {
      return this.launcherVisibility;
   }

   public Intercom.Visibility notificationVisibility() {
      return this.notificationVisibility;
   }

   public Activity pausedHostActivity() {
      return this.pausedHostActivity;
   }

   public Activity resumedHostActivity() {
      return this.resumedHostActivity;
   }

   public OverlayState.Builder toBuilder() {
      return new AutoValue_OverlayState.Builder(this);
   }

   public String toString() {
      return "OverlayState{conversations=" + this.conversations + ", dismissedPartIds=" + this.dismissedPartIds + ", notificationVisibility=" + this.notificationVisibility + ", launcherVisibility=" + this.launcherVisibility + ", bottomPadding=" + this.bottomPadding + ", resumedHostActivity=" + this.resumedHostActivity + ", pausedHostActivity=" + this.pausedHostActivity + "}";
   }

   static final class Builder extends OverlayState.Builder {
      private Integer bottomPadding;
      private List conversations;
      private Set dismissedPartIds;
      private Intercom.Visibility launcherVisibility;
      private Intercom.Visibility notificationVisibility;
      private Activity pausedHostActivity;
      private Activity resumedHostActivity;

      Builder() {
      }

      private Builder(OverlayState var1) {
         this.conversations = var1.conversations();
         this.dismissedPartIds = var1.dismissedPartIds();
         this.notificationVisibility = var1.notificationVisibility();
         this.launcherVisibility = var1.launcherVisibility();
         this.bottomPadding = Integer.valueOf(var1.bottomPadding());
         this.resumedHostActivity = var1.resumedHostActivity();
         this.pausedHostActivity = var1.pausedHostActivity();
      }

      // $FF: synthetic method
      Builder(OverlayState var1, Object var2) {
         this(var1);
      }

      public OverlayState.Builder bottomPadding(int var1) {
         this.bottomPadding = Integer.valueOf(var1);
         return this;
      }

      public OverlayState build() {
         String var2 = "";
         if(this.conversations == null) {
            var2 = "" + " conversations";
         }

         String var1 = var2;
         if(this.dismissedPartIds == null) {
            var1 = var2 + " dismissedPartIds";
         }

         var2 = var1;
         if(this.notificationVisibility == null) {
            var2 = var1 + " notificationVisibility";
         }

         var1 = var2;
         if(this.launcherVisibility == null) {
            var1 = var2 + " launcherVisibility";
         }

         var2 = var1;
         if(this.bottomPadding == null) {
            var2 = var1 + " bottomPadding";
         }

         if(!var2.isEmpty()) {
            throw new IllegalStateException("Missing required properties:" + var2);
         } else {
            return new AutoValue_OverlayState(this.conversations, this.dismissedPartIds, this.notificationVisibility, this.launcherVisibility, this.bottomPadding.intValue(), this.resumedHostActivity, this.pausedHostActivity);
         }
      }

      public OverlayState.Builder conversations(List var1) {
         if(var1 == null) {
            throw new NullPointerException("Null conversations");
         } else {
            this.conversations = var1;
            return this;
         }
      }

      public OverlayState.Builder dismissedPartIds(Set var1) {
         if(var1 == null) {
            throw new NullPointerException("Null dismissedPartIds");
         } else {
            this.dismissedPartIds = var1;
            return this;
         }
      }

      public OverlayState.Builder launcherVisibility(Intercom.Visibility var1) {
         if(var1 == null) {
            throw new NullPointerException("Null launcherVisibility");
         } else {
            this.launcherVisibility = var1;
            return this;
         }
      }

      public OverlayState.Builder notificationVisibility(Intercom.Visibility var1) {
         if(var1 == null) {
            throw new NullPointerException("Null notificationVisibility");
         } else {
            this.notificationVisibility = var1;
            return this;
         }
      }

      public OverlayState.Builder pausedHostActivity(Activity var1) {
         this.pausedHostActivity = var1;
         return this;
      }

      public OverlayState.Builder resumedHostActivity(Activity var1) {
         this.resumedHostActivity = var1;
         return this;
      }
   }
}
