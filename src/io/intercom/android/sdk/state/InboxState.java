package io.intercom.android.sdk.state;

import java.util.List;

public abstract class InboxState {
   public static InboxState create(List var0, InboxState.Status var1, boolean var2) {
      return new AutoValue_InboxState(var0, var1, var2);
   }

   public abstract List conversations();

   public abstract boolean hasMorePages();

   public abstract InboxState.Status status();

   public static enum Status {
      FAILED,
      INITIAL,
      LOADING,
      SUCCESS;
   }
}
