package io.intercom.android.sdk;

import android.annotation.SuppressLint;
import android.app.Application;
import android.app.TaskStackBuilder;
import io.intercom.android.sdk.identity.Registration;
import io.intercom.android.sdk.logger.LumberMill;
import io.intercom.android.sdk.twig.Twig;
import io.intercom.android.sdk.utilities.ValidatorUtil;
import java.util.Map;

public abstract class Intercom {
   public static final String GCM_RECEIVER = "intercom_sdk";
   public static final Intercom.Visibility GONE;
   private static final Twig TWIG;
   public static final Intercom.Visibility VISIBLE;
   private static Intercom instance;
   @SuppressLint({"StaticFieldLeak"})
   private static final LateInitializationPreparer lateInitializationPreparer;

   static {
      VISIBLE = Intercom.Visibility.VISIBLE;
      GONE = Intercom.Visibility.GONE;
      TWIG = LumberMill.getLogger();
      lateInitializationPreparer = new LateInitializationPreparer();
   }

   public static Intercom client() {
      synchronized(Intercom.class){}

      Intercom var0;
      try {
         if(instance == null) {
            IllegalStateException var3 = new IllegalStateException("Please call Intercom.initialize() before requesting the client.");
            throw var3;
         }

         var0 = instance;
      } finally {
         ;
      }

      return var0;
   }

   public static void initialize(Application var0, String var1, String var2) {
      synchronized(Intercom.class){}

      try {
         if(instance != null) {
            TWIG.i("Intercom has already been initialized", new Object[0]);
         } else if(ValidatorUtil.isValidConstructorParams(var0, var1, var2)) {
            TWIG.i("Intercom has already been initialized", new Object[0]);
            instance = RealIntercom.create(var0, var1, var2);
            lateInitializationPreparer.handlePastLifecycleEvents(var0, Injector.get());
         } else {
            InvalidIntercom var5 = new InvalidIntercom();
            instance = var5;
         }
      } finally {
         ;
      }

   }

   public static void registerForLaterInitialisation(Application param0) {
      // $FF: Couldn't be decompiled
   }

   public static void setLogLevel(@Intercom.LogLevel int var0) {
      LumberMill.setLogLevel(var0);
   }

   public static void unregisterForLateInitialisation(Application var0) {
      if(var0 == null) {
         throw new NullPointerException("Cannot call registerForLaterInitialisation() with a null Application");
      } else {
         lateInitializationPreparer.unregister(var0);
      }
   }

   public abstract void addUnreadConversationCountListener(UnreadConversationCountListener var1);

   public abstract void displayConversationsList();

   public abstract void displayMessageComposer();

   public abstract void displayMessageComposer(String var1);

   public abstract void displayMessenger();

   public abstract int getUnreadConversationCount();

   public abstract void handlePushMessage();

   public abstract void handlePushMessage(TaskStackBuilder var1);

   public abstract void hideMessenger();

   public abstract void logEvent(String var1);

   public abstract void logEvent(String var1, Map var2);

   public abstract void registerIdentifiedUser(Registration var1);

   public abstract void registerUnidentifiedUser();

   public abstract void removeUnreadConversationCountListener(UnreadConversationCountListener var1);

   public abstract void reset();

   public abstract void setBottomPadding(int var1);

   public abstract void setInAppMessageVisibility(Intercom.Visibility var1);

   public abstract void setLauncherVisibility(Intercom.Visibility var1);

   public abstract void setUserHash(String var1);

   public abstract void updateUser(UserAttributes var1);

   public @interface LogLevel {
      int ASSERT = 7;
      int DEBUG = 3;
      int DISABLED = 8;
      int ERROR = 6;
      int INFO = 4;
      int VERBOSE = 2;
      int WARN = 5;
   }

   public static enum Visibility {
      GONE,
      VISIBLE;
   }
}
