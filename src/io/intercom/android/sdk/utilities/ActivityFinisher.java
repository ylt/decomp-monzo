package io.intercom.android.sdk.utilities;

import android.app.Activity;
import android.support.v4.g.b;
import java.util.Iterator;
import java.util.Set;

public class ActivityFinisher {
   private final Set activities = new b();

   public void finishActivities() {
      Iterator var1 = this.activities.iterator();

      while(var1.hasNext()) {
         ((Activity)var1.next()).finish();
      }

      this.activities.clear();
   }

   public void register(Activity var1) {
      this.activities.add(var1);
   }

   public void unregister(Activity var1) {
      this.activities.remove(var1);
   }
}
