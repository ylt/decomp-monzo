package io.intercom.android.sdk.utilities;

import android.text.Layout;
import android.text.Selection;
import android.text.Spannable;
import android.text.method.LinkMovementMethod;
import android.text.style.URLSpan;
import android.view.MotionEvent;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.TextView;
import io.intercom.android.sdk.Injector;
import io.intercom.android.sdk.commons.utilities.TimeProvider;

public class TrackingLinkMovementMethod extends LinkMovementMethod {
   private boolean longClickActive = false;
   private boolean longClickPerformed = false;
   private long startClickTime;

   private int getPosition(MotionEvent var1, TextView var2) {
      int var7 = (int)var1.getX();
      int var8 = (int)var1.getY();
      int var4 = var2.getTotalPaddingLeft();
      int var5 = var2.getTotalPaddingTop();
      int var3 = var2.getScrollX();
      int var6 = var2.getScrollY();
      Layout var9 = var2.getLayout();
      return var9.getOffsetForHorizontal(var9.getLineForVertical(var8 - var5 + var6), (float)(var7 - var4 + var3));
   }

   private void performClick(ViewGroup var1) {
      while(!var1.performClick() && var1.getParent() instanceof ViewGroup) {
         var1 = (ViewGroup)var1.getParent();
      }

   }

   private void performLongClick(ViewGroup var1) {
      while(!var1.performLongClick() && var1.getParent() instanceof ViewGroup) {
         var1 = (ViewGroup)var1.getParent();
      }

   }

   public boolean onTouchEvent(TextView var1, Spannable var2, MotionEvent var3) {
      boolean var7 = true;
      boolean var6 = false;
      int var4 = this.getPosition(var3, var1);
      URLSpan[] var8 = (URLSpan[])var2.getSpans(var4, var4, URLSpan.class);
      boolean var5;
      switch(var3.getAction()) {
      case 0:
         this.startClickTime = TimeProvider.SYSTEM.currentTimeMillis();
         if(!this.longClickActive) {
            this.longClickActive = true;
         }

         var5 = var6;
         if(var8.length != 0) {
            Selection.setSelection(var2, var2.getSpanStart(var8[0]), var2.getSpanEnd(var8[0]));
            var5 = var6;
         }
         break;
      case 1:
         this.longClickActive = false;
         if(!this.longClickPerformed) {
            if(var8.length != 0) {
               LinkOpener.handleUrl(var8[0].getURL(), var1.getContext(), Injector.get().getApi());
            } else {
               Selection.removeSelection(var2);
               this.performClick((ViewGroup)var1.getParent());
            }

            var5 = true;
         } else {
            var5 = false;
         }

         this.longClickPerformed = false;
         break;
      case 2:
         var5 = var6;
         if(this.longClickActive) {
            if(TimeProvider.SYSTEM.currentTimeMillis() - this.startClickTime >= (long)ViewConfiguration.getLongPressTimeout()) {
               this.longClickActive = false;
               this.performLongClick((ViewGroup)var1.getParent());
               this.longClickPerformed = true;
               var5 = var7;
            } else {
               var5 = false;
            }
         }
         break;
      default:
         var5 = var6;
      }

      return var5;
   }
}
