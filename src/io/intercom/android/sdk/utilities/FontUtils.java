package io.intercom.android.sdk.utilities;

import android.graphics.Typeface;
import android.os.Build.VERSION;
import android.widget.TextView;
import io.intercom.android.sdk.logger.LumberMill;
import io.intercom.android.sdk.twig.Twig;

public class FontUtils {
   private static final String ROBOTO_MEDIUM = "fonts/intercom_roboto_medium.ttf";
   private static final Twig twig = LumberMill.getLogger();

   public static void setRobotoLightTypeface(TextView var0) {
      if(VERSION.SDK_INT >= 16) {
         var0.setTypeface(Typeface.create("sans-serif-light", 0));
      } else {
         var0.setTypeface(Typeface.SANS_SERIF);
      }

   }

   public static void setRobotoMediumTypeface(TextView var0) {
      if(VERSION.SDK_INT >= 21) {
         var0.setTypeface(Typeface.create("sans-serif-medium", 0));
      } else {
         try {
            var0.setTypeface(Typeface.createFromAsset(var0.getContext().getAssets(), "fonts/intercom_roboto_medium.ttf"));
         } catch (RuntimeException var1) {
            twig.e("We could not load our custom font, using the default system font as a backup.", new Object[0]);
         }
      }

   }
}
