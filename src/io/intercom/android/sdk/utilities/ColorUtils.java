package io.intercom.android.sdk.utilities;

import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;

public class ColorUtils {
   public static int darkenColor(int var0) {
      float[] var1 = new float[3];
      Color.colorToHSV(var0, var1);
      var1[2] *= 0.79F;
      return Color.HSVToColor(var1);
   }

   public static int lightenColor(int var0) {
      int var1 = (Color.red(var0) + 255) / 2;
      int var2 = (Color.green(var0) + 255) / 2;
      int var3 = (Color.blue(var0) + 255) / 2;
      return Color.argb(Color.alpha(var0), var1, var2, var3);
   }

   public static ColorFilter newGreyscaleFilter() {
      ColorMatrix var0 = new ColorMatrix();
      var0.setSaturation(0.0F);
      return new ColorMatrixColorFilter(var0);
   }
}
