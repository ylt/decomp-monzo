package io.intercom.android.sdk.utilities;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Build.VERSION;
import android.view.ContextThemeWrapper;
import io.intercom.android.sdk.Provider;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.android.sdk.logger.LumberMill;
import io.intercom.android.sdk.twig.Twig;
import java.util.Locale;

public class ContextLocaliser {
   private static final String FAKE_FORMAL_COUNTRY = "FO";
   private static final String FORMAL_TAG = "form";
   private static final String SIMPLIFIED_CHINESE_COUNTRY = "CN";
   private static final String SIMPLIFIED_CHINESE_ISO_15924 = "hans";
   private static final String TRADITIONAL_CHINESE_COUNTRY = "TW";
   private static final String TRADITIONAL_CHINESE_ISO_15924 = "hant";
   private static final Twig twig = LumberMill.getLogger();
   private final Provider appConfigProvider;

   public ContextLocaliser(Provider var1) {
      this.appConfigProvider = var1;
   }

   static Locale convertToLocale(String var0) {
      Locale var3;
      if(var0.length() <= 2) {
         var3 = new Locale(var0);
      } else {
         String var2 = var0.substring(0, 2);
         String var1 = var0.substring(3);
         if("hans".equalsIgnoreCase(var1)) {
            var0 = "CN";
         } else if("hant".equalsIgnoreCase(var1)) {
            var0 = "TW";
         } else {
            var0 = var1;
            if("form".equalsIgnoreCase(var1)) {
               var0 = "FO";
            }
         }

         var3 = new Locale(var2, var0);
      }

      return var3;
   }

   public void applyOverrideConfiguration(ContextThemeWrapper var1, Context var2) {
      if(VERSION.SDK_INT >= 17) {
         Configuration var3 = new Configuration(var2.getResources().getConfiguration());
         var3.setLocale(convertToLocale(((AppConfig)this.appConfigProvider.get()).getLocale()));
         var1.applyOverrideConfiguration(var3);
      }

   }

   public Context createLocalisedContext(Context var1) {
      if(VERSION.SDK_INT >= 17) {
         Configuration var2 = new Configuration(var1.getResources().getConfiguration());
         var2.setLocale(convertToLocale(((AppConfig)this.appConfigProvider.get()).getLocale()));
         var1 = var1.createConfigurationContext(var2);
      } else {
         twig.d("Localisation outside the Messenger is unsupported on Android 4.0 and 4.1, text may be localised incorrectly", new Object[0]);
      }

      return var1;
   }
}
