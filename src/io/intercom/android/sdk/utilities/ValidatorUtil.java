package io.intercom.android.sdk.utilities;

import android.app.Application;
import io.intercom.android.sdk.api.ApiFactory;
import io.intercom.android.sdk.logger.LumberMill;
import io.intercom.android.sdk.twig.Twig;

public class ValidatorUtil {
   private static final String API_PREFIX = "android_sdk-";
   private static final Twig twig = LumberMill.getLogger();

   static boolean apiKeyIsMissingPrefix(String var0) {
      boolean var1;
      if(var0 != null && var0.startsWith("android_sdk-")) {
         var1 = false;
      } else {
         var1 = true;
      }

      return var1;
   }

   static boolean apiKeyIsTooShort(String var0) {
      boolean var1;
      if(var0 != null && var0.length() >= 52) {
         var1 = false;
      } else {
         var1 = true;
      }

      return var1;
   }

   private static boolean appIdIsInvalid(String var0) {
      boolean var1;
      if(var0 != null && !ApiFactory.removeInvalidCharacters(var0).isEmpty()) {
         var1 = false;
      } else {
         var1 = true;
      }

      return var1;
   }

   public static boolean isValidConstructorParams(Application var0, String var1, String var2) {
      boolean var4 = false;
      boolean var3 = true;
      if(var0 == null) {
         twig.e("The Application passed in was null.We need an Application to enable Intercom in your app", new Object[0]);
         var3 = false;
      }

      if(appIdIsInvalid(var2)) {
         twig.e("The app ID (" + var2 + ") you provided is either null or empty. We need a valid app ID to enable Intercom in your app", new Object[0]);
         var3 = false;
      }

      if(apiKeyIsTooShort(var1)) {
         twig.e("The API key provided (" + var1 + ") is too short.\nPlease check that you are using an Intercom Android SDK key and have not passed the appId into the apiKey field\n", new Object[0]);
         var3 = false;
      }

      if(apiKeyIsMissingPrefix(var1)) {
         twig.e("The API key provided (" + var1 + ") does not begin with 'android_sdk-'.\nPlease check that you are using an Intercom Android SDK key and have not passed the appId into the apiKey field\n", new Object[0]);
         var3 = var4;
      }

      return var3;
   }
}
