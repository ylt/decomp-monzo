package io.intercom.android.sdk.utilities;

import java.io.Closeable;
import java.io.File;

public class IoUtils {
   public static void closeQuietly(Closeable var0) {
      if(var0 != null) {
         try {
            var0.close();
         } catch (Exception var1) {
            ;
         }
      }

   }

   public static void safelyDelete(File var0) {
      try {
         if(!var0.delete()) {
            var0.deleteOnExit();
         }
      } catch (Exception var1) {
         ;
      }

   }
}
