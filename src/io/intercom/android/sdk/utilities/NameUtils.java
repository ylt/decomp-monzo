package io.intercom.android.sdk.utilities;

public class NameUtils {
   public static String getInitial(String var0) {
      var0 = var0.trim();
      if(var0.isEmpty()) {
         var0 = "";
      } else {
         var0 = String.valueOf(var0.charAt(0));
      }

      return var0;
   }
}
