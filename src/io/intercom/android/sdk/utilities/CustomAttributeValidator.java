package io.intercom.android.sdk.utilities;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class CustomAttributeValidator {
   private static final List ACCEPTED_CUSTOM_ATTRIBUTE_TYPES = Arrays.asList(new Class[]{String.class, Character.class, Short.class, Long.class, Float.class, Double.class, Integer.class, Byte.class, Boolean.class});

   public static String getAcceptedTypes() {
      StringBuilder var0 = new StringBuilder();

      Class var2;
      for(Iterator var1 = ACCEPTED_CUSTOM_ATTRIBUTE_TYPES.iterator(); var1.hasNext(); var0.append(var2.getSimpleName())) {
         var2 = (Class)var1.next();
         if(var0.length() != 0) {
            var0.append(", ");
         }
      }

      return var0.toString();
   }

   public static boolean isValid(Object var0) {
      boolean var1;
      if(var0 != null && !ACCEPTED_CUSTOM_ATTRIBUTE_TYPES.contains(var0.getClass())) {
         var1 = false;
      } else {
         var1 = true;
      }

      return var1;
   }
}
