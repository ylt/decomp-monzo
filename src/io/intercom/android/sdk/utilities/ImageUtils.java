package io.intercom.android.sdk.utilities;

import android.text.TextUtils;
import io.intercom.com.bumptech.glide.load.engine.h;

public class ImageUtils {
   public static int getAspectHeight(int var0, double var1) {
      return (int)((double)var0 * var1);
   }

   public static double getAspectRatio(int var0, int var1) {
      double var4 = 1.0D * (double)var1 / (double)var0;
      double var2 = var4;
      if(Double.isNaN(var4)) {
         var2 = 0.0D;
      }

      return var2;
   }

   public static h getDiskCacheStrategy(String var0) {
      h var1;
      if(isGif(var0)) {
         var1 = h.c;
      } else {
         var1 = h.d;
      }

      return var1;
   }

   public static boolean isGif(String var0) {
      boolean var1;
      if(!TextUtils.isEmpty(var0) && var0.endsWith(".gif")) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }
}
