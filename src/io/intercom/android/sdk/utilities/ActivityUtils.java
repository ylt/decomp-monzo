package io.intercom.android.sdk.utilities;

import android.app.Activity;
import android.os.Build.VERSION;
import io.intercom.android.sdk.Intercom;
import javax.annotation.Nullable;

public class ActivityUtils {
   private static final String COMPOSER_INPUT_PACKAGE_NAME = "io.intercom.android.sdk.input";
   private static final String COMPOSER_PACKAGE_NAME = "io.intercom.android.sdk.composer";
   private static final String[] INTERCOM_PACKAGES;
   private static final String PACKAGE_NAME;

   static {
      boolean var2 = false;

      String var0;
      try {
         var2 = true;
         var0 = Intercom.class.getPackage().getName();
         var2 = false;
      } finally {
         if(var2) {
            PACKAGE_NAME = "io.intercom.android.sdk.sdk";
         }
      }

      if(var0 == null) {
         var0 = "io.intercom.android.sdk.sdk";
      }

      PACKAGE_NAME = var0;
      INTERCOM_PACKAGES = new String[]{PACKAGE_NAME, "io.intercom.android.sdk.composer", "io.intercom.android.sdk.input"};
   }

   public static boolean isHostActivity(@Nullable Activity var0) {
      boolean var1;
      if(var0 != null && isInHostAppPackage(var0.getClass().getName())) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   private static boolean isInHostAppPackage(String var0) {
      boolean var3 = false;
      String[] var4 = INTERCOM_PACKAGES;
      int var2 = var4.length;
      int var1 = 0;

      while(true) {
         if(var1 >= var2) {
            var3 = true;
            break;
         }

         if(var0.startsWith(var4[var1])) {
            break;
         }

         ++var1;
      }

      return var3;
   }

   public static boolean isNotActive(Activity var0) {
      boolean var1;
      if(!var0.isFinishing() && (VERSION.SDK_INT < 17 || !var0.isDestroyed())) {
         var1 = false;
      } else {
         var1 = true;
      }

      return var1;
   }
}
