package io.intercom.android.sdk.utilities;

import android.os.Build.VERSION;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import io.intercom.android.sdk.logger.LumberMill;
import io.intercom.android.sdk.twig.Twig;

public class ViewUtils {
   private static final Twig twig = LumberMill.getLogger();

   public static void removeGlobalLayoutListener(View var0, OnGlobalLayoutListener var1) {
      if(VERSION.SDK_INT >= 16) {
         var0.getViewTreeObserver().removeOnGlobalLayoutListener(var1);
      } else {
         var0.getViewTreeObserver().removeGlobalOnLayoutListener(var1);
      }

   }

   public static void requestLayoutIfPossible(View var0) {
      if(VERSION.SDK_INT < 18 || !var0.isInLayout()) {
         var0.requestLayout();
      }

   }

   public static void setOverScrollColour(RecyclerView param0, int param1) {
      // $FF: Couldn't be decompiled
   }

   public static void waitForViewAttachment(final View var0, final Runnable var1) {
      if(var0.getHeight() == 0) {
         var0.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
            public void onGlobalLayout() {
               ViewUtils.removeGlobalLayoutListener(var0, this);
               var1.run();
            }
         });
      } else {
         var1.run();
      }

   }
}
