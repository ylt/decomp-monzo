package io.intercom.android.sdk.utilities;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import io.intercom.android.sdk.api.Api;
import io.intercom.android.sdk.logger.LumberMill;
import io.intercom.android.sdk.twig.Twig;
import java.util.Locale;

public class LinkOpener {
   private static final Twig twig = LumberMill.getLogger();

   static Uri getTargetUriFromTrackingUrl(String var0) {
      Uri var1 = Uri.parse(var0);
      String var2;
      if(var1.isHierarchical()) {
         var2 = var1.getQueryParameter("url");
      } else {
         var2 = var0;
      }

      if(var2 != null) {
         var0 = var2;
      }

      return parseUrl(var0);
   }

   public static void handleUrl(String var0, Context var1, Api var2) {
      if(!TextUtils.isEmpty(var0)) {
         Uri var3;
         if(isTrackingUrl(var0)) {
            var2.hitTrackingUrl(var0);
            var3 = getTargetUriFromTrackingUrl(var0);
         } else {
            var3 = parseUrl(var0);
         }

         openUrl(var3, var1);
      }

   }

   private static boolean isTrackingUrl(String var0) {
      return var0.contains("via.intercom.io");
   }

   private static Uri normalizeScheme(Uri var0) {
      String var2 = var0.getScheme();
      Uri var1;
      if(var2 == null) {
         var1 = var0;
      } else {
         String var3 = var2.toLowerCase(Locale.ROOT);
         var1 = var0;
         if(!var2.equals(var3)) {
            var1 = var0.buildUpon().scheme(var3).build();
         }
      }

      return var1;
   }

   private static void openUrl(Uri var0, Context var1) {
      String var2;
      if("mailto".equals(var0.getScheme())) {
         var2 = "android.intent.action.SENDTO";
      } else {
         var2 = "android.intent.action.VIEW";
      }

      Intent var5 = new Intent(var2, var0);
      var5.addFlags(268435456);

      try {
         IntentUtils.safelyOpenIntent(var1, var5);
      } catch (ActivityNotFoundException var3) {
         twig.e("No Activity found to handle the URL '" + var0.toString() + "'", new Object[0]);
      } catch (SecurityException var4) {
         twig.e("Couldn't open link because of error: " + var4.getMessage(), new Object[0]);
      }

   }

   private static Uri parseUrl(String var0) {
      Uri var2 = Uri.parse(var0);
      Uri var1 = var2;
      if(var2.getScheme() == null) {
         var1 = Uri.parse("http://" + var0);
      }

      Uri var3;
      if(!var1.getScheme().equalsIgnoreCase("http")) {
         var3 = var1;
         if(!var1.getScheme().equalsIgnoreCase("https")) {
            return var3;
         }
      }

      var3 = normalizeScheme(var1);
      return var3;
   }
}
