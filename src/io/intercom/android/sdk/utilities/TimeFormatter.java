package io.intercom.android.sdk.utilities;

import android.content.Context;
import android.os.Build.VERSION;
import android.text.format.DateFormat;
import io.intercom.android.sdk.Provider;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.commons.utilities.TimeProvider;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.android.sdk.models.LastParticipatingAdmin;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class TimeFormatter {
   private SimpleDateFormat absoluteDateFormatter;
   private SimpleDateFormat absoluteTimeFormatter;
   private final Context context;
   private final TimeProvider timeProvider;

   public TimeFormatter(Context var1, TimeProvider var2) {
      this.context = var1;
      this.timeProvider = var2;
   }

   private static SimpleDateFormat createDateFormatter(Locale var0, String var1) {
      SimpleDateFormat var2;
      if(VERSION.SDK_INT >= 18) {
         var2 = new SimpleDateFormat(DateFormat.getBestDateTimePattern(var0, var1), var0);
      } else {
         var2 = new SimpleDateFormat(var1, var0);
      }

      return var2;
   }

   private CharSequence getAdminActiveStatus(Date var1) {
      long var2 = this.getDifferenceInMinutes(var1);
      CharSequence var4;
      if(var2 > TimeUnit.DAYS.toMinutes(6L)) {
         var4 = this.context.getText(R.string.intercom_active_week_ago);
      } else {
         Phrase var5;
         if(var2 >= TimeUnit.HOURS.toMinutes(23L) + 31L) {
            var5 = Phrase.from(this.context, R.string.intercom_active_day_ago).put("days", Long.toString((var2 / 60L - 13L) / 24L + 1L));
         } else if(var2 >= 53L) {
            var5 = Phrase.from(this.context, R.string.intercom_active_hour_ago).put("hours", Long.toString((var2 - 31L) / 60L + 1L));
         } else if(var2 >= 38L) {
            var5 = Phrase.from(this.context, R.string.intercom_active_minute_ago).put("minutes", Long.toString(45L));
         } else if(var2 >= 16L) {
            var5 = Phrase.from(this.context, R.string.intercom_active_minute_ago).put("minutes", Long.toString(30L));
         } else {
            var5 = Phrase.from(this.context, R.string.intercom_active_15m_ago).put("minutes", Long.toString(15L));
         }

         var4 = var5.format();
      }

      return var4;
   }

   private Date getDateFromTimeStamp(long var1) {
      return new Date(1000L * var1);
   }

   private long getDifferenceInDays(Date var1) {
      return TimeUnit.MILLISECONDS.toDays(this.timeProvider.currentTimeMillis() - var1.getTime());
   }

   private long getDifferenceInHours(Date var1) {
      return TimeUnit.MILLISECONDS.toHours(this.timeProvider.currentTimeMillis() - var1.getTime());
   }

   private long getDifferenceInMinutes(Date var1) {
      return TimeUnit.MILLISECONDS.toMinutes(this.timeProvider.currentTimeMillis() - var1.getTime());
   }

   private CharSequence getFormattedTime(Date var1) {
      long var4 = this.getDifferenceInMinutes(var1);
      long var6 = this.getDifferenceInHours(var1);
      long var2 = this.getDifferenceInDays(var1);
      long var8 = var2 / 7L;
      Phrase var10;
      CharSequence var11;
      if(var8 > 0L) {
         var10 = Phrase.from(this.context, R.string.intercom_time_week_ago).put("delta", Long.toString(var8));
      } else if(var2 > 0L) {
         var10 = Phrase.from(this.context, R.string.intercom_time_day_ago).put("delta", Long.toString(var2));
      } else if(var6 > 0L) {
         var10 = Phrase.from(this.context, R.string.intercom_time_hour_ago).put("delta", Long.toString(var6));
      } else {
         if(var4 < 1L) {
            var11 = this.context.getText(R.string.intercom_time_just_now);
            return var11;
         }

         var10 = Phrase.from(this.context, R.string.intercom_time_minute_ago).put("delta", Long.toString(var4));
      }

      var11 = var10.format();
      return var11;
   }

   public String getAbsoluteDate(long var1) {
      return this.getAbsoluteDate(1000L * var1, this.context.getResources().getConfiguration().locale);
   }

   String getAbsoluteDate(long var1, Locale var3) {
      if(this.absoluteDateFormatter == null) {
         this.absoluteDateFormatter = createDateFormatter(var3, "MMMM d");
      }

      return this.absoluteDateFormatter.format(new Date(var1));
   }

   public String getAbsoluteTime(long var1) {
      return this.getAbsoluteTime(1000L * var1, this.context.getResources().getConfiguration().locale);
   }

   String getAbsoluteTime(long var1, Locale var3) {
      if(this.absoluteTimeFormatter == null) {
         this.absoluteTimeFormatter = createDateFormatter(var3, "hh:mm");
      }

      return this.absoluteTimeFormatter.format(new Date(var1));
   }

   public CharSequence getAdminActiveStatus(LastParticipatingAdmin var1, Provider var2) {
      Object var3;
      if(this.shouldShowActiveOrAwayState(((AppConfig)var2.get()).getLocale())) {
         if(var1.isActive()) {
            var3 = this.context.getString(R.string.intercom_active_state);
         } else {
            var3 = this.context.getString(R.string.intercom_away_state);
         }
      } else if(var1.getLastActiveAt() <= 0L) {
         var3 = "";
      } else {
         var3 = this.getAdminActiveStatus(this.getDateFromTimeStamp(var1.getLastActiveAt()));
      }

      return (CharSequence)var3;
   }

   public CharSequence getFormattedTime(long var1) {
      Object var3;
      if(var1 <= 0L) {
         var3 = "";
      } else {
         var3 = this.getFormattedTime(new Date(1000L * var1));
      }

      return (CharSequence)var3;
   }

   public String getLastActiveMinutes(long var1) {
      Date var3 = new Date(1000L * var1);
      return this.getDifferenceInMinutes(var3) + " minutes";
   }

   public CharSequence getUpdated(long var1) {
      Date var9 = this.getDateFromTimeStamp(var1);
      long var5 = this.getDifferenceInMinutes(var9);
      long var7 = this.getDifferenceInHours(var9);
      var1 = this.getDifferenceInDays(var9);
      long var3 = var1 / 7L;
      Object var10;
      if(var3 > 0L) {
         var10 = "Updated " + var3 + " weeks ago";
      } else if(var1 > 0L) {
         var10 = "Updated " + var1 + " days ago";
      } else if(var7 > 0L) {
         var10 = "Updated " + var7 + " hours ago";
      } else if(var5 >= 1L) {
         var10 = "Updated " + var5 + " minutes ago";
      } else {
         var10 = this.context.getText(R.string.intercom_time_just_now);
      }

      return (CharSequence)var10;
   }

   boolean shouldShowActiveOrAwayState(String var1) {
      return var1.equals("en");
   }
}
