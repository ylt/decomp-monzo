package io.intercom.android.sdk.utilities;

public class NullSafety {
   public static boolean valueOrDefault(Boolean var0, boolean var1) {
      if(var0 != null) {
         var1 = var0.booleanValue();
      }

      return var1;
   }

   public static String valueOrEmpty(String var0) {
      String var1 = var0;
      if(var0 == null) {
         var1 = "";
      }

      return var1;
   }
}
