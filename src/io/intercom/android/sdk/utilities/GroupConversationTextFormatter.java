package io.intercom.android.sdk.utilities;

import android.content.Context;
import io.intercom.android.sdk.R;

public class GroupConversationTextFormatter {
   public static CharSequence groupConversationTitle(String var0, int var1, Context var2) {
      Object var3;
      if(var1 == 1) {
         var3 = Phrase.from(var2, R.string.intercom_name_and_1_other).put("name", var0).format();
      } else {
         var3 = var0;
         if(var1 > 1) {
            var3 = Phrase.from(var2, R.string.intercom_name_and_x_others).put("name", var0).put("count", var1).format();
         }
      }

      return (CharSequence)var3;
   }
}
