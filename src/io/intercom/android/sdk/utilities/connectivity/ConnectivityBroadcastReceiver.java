package io.intercom.android.sdk.utilities.connectivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

class ConnectivityBroadcastReceiver extends BroadcastReceiver {
   private final ConnectivityBroadcastReceiver.ConnectivityUpdateListener connectivityUpdateListener;

   ConnectivityBroadcastReceiver(ConnectivityBroadcastReceiver.ConnectivityUpdateListener var1) {
      this.connectivityUpdateListener = var1;
   }

   public void onReceive(Context var1, Intent var2) {
      if(var2 != null && "android.net.conn.CONNECTIVITY_CHANGE".equals(var2.getAction())) {
         NetworkState var3;
         if(var2.getBooleanExtra("noConnectivity", false)) {
            var3 = NetworkState.NOT_CONNECTED;
         } else {
            var3 = NetworkState.CONNECTED;
         }

         this.connectivityUpdateListener.onUpdate(var3);
      }

   }

   interface ConnectivityUpdateListener {
      void onUpdate(NetworkState var1);
   }
}
