package io.intercom.android.sdk.utilities.connectivity;

import android.content.Context;
import android.content.IntentFilter;

public class NetworkConnectivityMonitor implements ConnectivityBroadcastReceiver.ConnectivityUpdateListener {
   private boolean didRegister;
   private NetworkState lastState;
   private NetworkConnectivityMonitor.ConnectivityEventListener listener;
   private final ConnectivityBroadcastReceiver receiver;

   public NetworkConnectivityMonitor() {
      this.lastState = NetworkState.UNKNOWN;
      this.didRegister = false;
      this.receiver = new ConnectivityBroadcastReceiver(this);
   }

   NetworkConnectivityMonitor.ConnectivityEventListener getListener() {
      return this.listener;
   }

   public void onUpdate(NetworkState var1) {
      if(var1 != this.lastState) {
         if(this.listener != null) {
            if(var1 == NetworkState.NOT_CONNECTED) {
               this.listener.onDisconnect();
            } else if(var1 == NetworkState.CONNECTED && this.lastState == NetworkState.NOT_CONNECTED) {
               this.listener.onReconnect();
            }
         }

         this.lastState = var1;
      }

   }

   public void setListener(NetworkConnectivityMonitor.ConnectivityEventListener var1) {
      this.listener = var1;
   }

   public void startListening(Context var1) {
      synchronized(this){}

      try {
         if(!this.didRegister) {
            ConnectivityBroadcastReceiver var3 = this.receiver;
            IntentFilter var2 = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
            var1.registerReceiver(var3, var2);
            this.didRegister = true;
         }
      } finally {
         ;
      }

   }

   public void stopListening(Context var1) {
      synchronized(this){}

      try {
         if(this.didRegister) {
            var1.unregisterReceiver(this.receiver);
            this.didRegister = false;
         }
      } finally {
         ;
      }

   }

   public interface ConnectivityEventListener {
      void onDisconnect();

      void onReconnect();
   }
}
