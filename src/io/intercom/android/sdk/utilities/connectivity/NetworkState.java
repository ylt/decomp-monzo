package io.intercom.android.sdk.utilities.connectivity;

enum NetworkState {
   CONNECTED,
   NOT_CONNECTED,
   UNKNOWN;
}
