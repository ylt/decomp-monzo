package io.intercom.android.sdk.utilities;

import io.intercom.android.sdk.store.Store;
import javax.annotation.Nullable;

public class StoreUtils {
   public static void safeUnsubscribe(@Nullable Store.Subscription var0) {
      if(var0 != null) {
         var0.unsubscribe();
      }

   }
}
