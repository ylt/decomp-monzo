package io.intercom.android.sdk.utilities;

import android.content.Context;
import android.view.View;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.commons.utilities.TimeProvider;
import io.intercom.android.sdk.models.Location;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class LocationFormatter {
   private static final String[] LOWER_CASE_ENGLISH_AM_PM = new String[]{"am", "pm"};
   private static final long ONE_MINUTE_IN_MILLIS;
   private static final String[] UPPER_CASE_EN_GB_AM_PM = new String[]{"a.m.", "p.m."};
   private static final String[] UPPER_CASE_EN_US_AM_PM = new String[]{"AM", "PM"};

   static {
      ONE_MINUTE_IN_MILLIS = TimeUnit.MINUTES.toMillis(1L);
   }

   public static CharSequence getLocationString(Context var0, Location var1, Date var2) {
      Object var6;
      if(var1.getTimezoneOffset() == null) {
         var6 = "";
      } else {
         Locale var4 = var0.getResources().getConfiguration().locale;
         SimpleDateFormat var3 = new SimpleDateFormat("h:mma", var4);
         TimeZone var5 = TimeZone.getDefault();
         var5.setRawOffset((int)TimeUnit.SECONDS.toMillis((long)var1.getTimezoneOffset().intValue()));
         var3.setTimeZone(var5);
         DateFormatSymbols var7 = new DateFormatSymbols(var4);
         if(Arrays.equals(var7.getAmPmStrings(), UPPER_CASE_EN_GB_AM_PM) || Arrays.equals(var7.getAmPmStrings(), UPPER_CASE_EN_US_AM_PM)) {
            var7.setAmPmStrings(LOWER_CASE_ENGLISH_AM_PM);
         }

         var3.setDateFormatSymbols(var7);
         var6 = Phrase.from(var0, R.string.intercom_profile_location).put("time", var3.format(var2)).put("location", var1.getCityName() + ", " + var1.getCountryName()).format();
      }

      return (CharSequence)var6;
   }

   public static void postOnNextMinute(View var0, Runnable var1, TimeProvider var2) {
      long var5 = var2.currentTimeMillis();
      long var3 = ONE_MINUTE_IN_MILLIS;
      var0.postDelayed(var1, ONE_MINUTE_IN_MILLIS - var5 % var3);
   }
}
