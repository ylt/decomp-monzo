package io.intercom.android.sdk.utilities;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.commons.utilities.ScreenUtils;

public class BlockUtils {
   private static final int DEFAULT_MARGIN_BOTTOM_DP = 10;
   private static final int LARGE_LINE_SPACING_DP = 4;
   private static final int SMALL_LINE_SPACING_DP = 2;

   public static void createLayoutParams(View var0, int var1, int var2) {
      var0.setLayoutParams(new LayoutParams(var1, var2));
   }

   public static View getBlockView(ViewGroup var0, LinearLayout var1, Context var2) {
      View var4 = LayoutInflater.from(var2).inflate(R.layout.intercom_container_layout, var0, false);
      LinearLayout var3 = (LinearLayout)var4.findViewById(R.id.cellLayout);
      if(var1.getParent() != null) {
         ((LinearLayout)var1.getParent()).removeView(var1);
      }

      if(var3.getChildCount() > 0) {
         var3.removeAllViews();
      }

      var3.addView(var1, 0);
      var4.setFocusable(false);
      var3.setVisibility(0);
      return var4;
   }

   public static void setDefaultMarginBottom(View var0) {
      setMarginBottom(var0, 10);
   }

   public static void setLargeLineSpacing(TextView var0) {
      setLineSpacing(var0, 4);
   }

   public static void setLayoutMarginsAndGravity(View var0, int var1, boolean var2) {
      LayoutParams var3 = (LayoutParams)var0.getLayoutParams();
      if(var2) {
         var3.setMargins(var3.leftMargin, var3.topMargin, var3.rightMargin, 0);
      }

      var3.gravity = var1;
   }

   private static void setLineSpacing(TextView var0, int var1) {
      var0.setLineSpacing((float)ScreenUtils.dpToPx((float)var1, var0.getContext()), 1.0F);
   }

   public static void setMarginBottom(View var0, int var1) {
      ((MarginLayoutParams)var0.getLayoutParams()).bottomMargin = ScreenUtils.dpToPx((float)var1, var0.getContext());
   }

   public static void setMarginLeft(View var0, int var1) {
      ((MarginLayoutParams)var0.getLayoutParams()).leftMargin = ScreenUtils.dpToPx((float)var1, var0.getContext());
   }

   public static void setSmallLineSpacing(TextView var0) {
      setLineSpacing(var0, 2);
   }
}
