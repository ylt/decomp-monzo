package io.intercom.android.sdk.utilities;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.android.sdk.models.Avatar;
import io.intercom.android.sdk.transforms.RoundTransform;
import io.intercom.android.sdk.views.AvatarDefaultDrawable;
import io.intercom.android.sdk.views.AvatarInitialsDrawable;
import io.intercom.com.bumptech.glide.i;
import io.intercom.com.bumptech.glide.j;
import io.intercom.com.bumptech.glide.f.f;
import io.intercom.com.bumptech.glide.f.a.h;
import io.intercom.com.bumptech.glide.f.b.d;
import io.intercom.com.bumptech.glide.load.l;
import io.intercom.com.bumptech.glide.load.resource.b.b;
import java.io.File;

public class AvatarUtils {
   public static void createAvatar(Avatar var0, ImageView var1, int var2, AppConfig var3, i var4) {
      Context var5 = var1.getContext();
      Object var7;
      if(var0.getInitials().isEmpty()) {
         var7 = getDefaultDrawable(var5, var3);
      } else {
         var7 = getInitialsDrawable(var0.getInitials(), var3);
      }

      f var6 = (new f()).a((Drawable)var7).b((Drawable)var7).b(ImageUtils.getDiskCacheStrategy(var0.getImageUrl())).a((l)(new RoundTransform()));
      f var8 = var6;
      if(var2 > 0) {
         var8 = var6.a(var2, var2);
      }

      var4.a((Object)var0.getImageUrl()).a((j)b.c()).a(var8).a(var1);
   }

   public static AvatarDefaultDrawable getDefaultDrawable(Context var0, AppConfig var1) {
      return new AvatarDefaultDrawable(var0, var1.getBaseColorDark());
   }

   public static AvatarInitialsDrawable getInitialsDrawable(String var0, AppConfig var1) {
      return new AvatarInitialsDrawable(var0.toUpperCase(), var1.getBaseColorDark());
   }

   public static void loadAvatarIntoView(Avatar var0, ImageView var1, AppConfig var2, i var3) {
      createAvatar(var0, var1, 0, var2, var3);
   }

   public static void preloadAvatar(Avatar var0, final Runnable var1, i var2) {
      if(var0.getImageUrl().isEmpty()) {
         var1.run();
      } else {
         var2.e().a(var0.getImageUrl()).a((h)(new io.intercom.com.bumptech.glide.f.a.f() {
            public void onLoadFailed(Drawable var1x) {
               var1.run();
            }

            public void onResourceReady(File var1x, d var2) {
               var1.run();
            }
         }));
      }

   }
}
