package io.intercom.android.sdk.utilities;

import android.content.Context;
import android.os.Build.VERSION;
import android.provider.Settings.Global;
import io.intercom.android.sdk.logger.LumberMill;
import io.intercom.android.sdk.twig.Twig;

public class SystemSettings {
   private static final Twig twig = LumberMill.getLogger();

   public static float getTransitionScale(Context var0) {
      float var2 = 1.0F;
      float var1 = var2;
      if(VERSION.SDK_INT >= 17) {
         try {
            var1 = Global.getFloat(var0.getContentResolver(), "transition_animation_scale");
         } catch (Exception var3) {
            twig.internal("Couldn't get animation scale: " + var3.getMessage());
            var1 = var2;
         }
      }

      return var1;
   }
}
