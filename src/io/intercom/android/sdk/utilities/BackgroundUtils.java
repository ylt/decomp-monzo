package io.intercom.android.sdk.utilities;

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.animation.Animator.AnimatorListener;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.view.View;

public class BackgroundUtils {
   public static void animateBackground(int var0, int var1, int var2, final View var3, AnimatorListener var4) {
      ValueAnimator var5 = ValueAnimator.ofObject(new ArgbEvaluator(), new Object[]{Integer.valueOf(var0), Integer.valueOf(var1)});
      var5.setDuration((long)var2);
      var5.addUpdateListener(new AnimatorUpdateListener() {
         public void onAnimationUpdate(ValueAnimator var1) {
            var3.setBackgroundColor(((Integer)var1.getAnimatedValue()).intValue());
         }
      });
      if(var4 != null) {
         var5.addListener(var4);
      }

      var5.start();
   }

   public static void setBackground(View var0, Drawable var1) {
      if(VERSION.SDK_INT >= 16) {
         var0.setBackground(var1);
      } else {
         var0.setBackgroundDrawable(var1);
      }

   }
}
