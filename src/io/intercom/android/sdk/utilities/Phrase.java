package io.intercom.android.sdk.utilities;

import android.app.Fragment;
import android.content.Context;
import android.content.res.Resources;
import android.text.SpannableStringBuilder;
import android.view.View;
import android.widget.TextView;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public final class Phrase {
   private static final int EOF = 0;
   private char curChar;
   private int curCharIndex;
   private CharSequence formatted;
   private Phrase.Token head;
   private final Set keys;
   private final Map keysToValues;
   private final CharSequence pattern;

   private Phrase(CharSequence var1) {
      char var2 = 0;
      super();
      this.keys = new HashSet();
      this.keysToValues = new HashMap();
      if(var1.length() > 0) {
         var2 = var1.charAt(0);
      }

      this.curChar = var2;
      this.pattern = var1;
      Phrase.Token var4 = null;

      while(true) {
         Phrase.Token var3 = this.token(var4);
         if(var3 == null) {
            return;
         }

         var4 = var3;
         if(this.head == null) {
            this.head = var3;
            var4 = var3;
         }
      }
   }

   private void consume() {
      ++this.curCharIndex;
      char var1;
      if(this.curCharIndex == this.pattern.length()) {
         var1 = 0;
      } else {
         var1 = this.pattern.charAt(this.curCharIndex);
      }

      this.curChar = var1;
   }

   public static Phrase from(Fragment var0, int var1) {
      return from(var0.getResources(), var1);
   }

   public static Phrase from(Context var0, int var1) {
      return from(var0.getResources(), var1);
   }

   public static Phrase from(Resources var0, int var1) {
      return from(var0.getText(var1));
   }

   public static Phrase from(View var0, int var1) {
      return from(var0.getResources(), var1);
   }

   public static Phrase from(CharSequence var0) {
      return new Phrase(var0);
   }

   private Phrase.KeyToken key(Phrase.Token var1) {
      StringBuilder var2 = new StringBuilder();
      this.consume();

      while(this.curChar >= 97 && this.curChar <= 122 || this.curChar >= 65 && this.curChar <= 90 || this.curChar == 95) {
         var2.append(this.curChar);
         this.consume();
      }

      if(this.curChar != 125) {
         throw new IllegalArgumentException("Missing closing brace: } in '" + this.pattern + "'");
      } else {
         this.consume();
         if(var2.length() == 0) {
            throw new IllegalArgumentException("Empty key: {} in '" + this.pattern + "'");
         } else {
            String var3 = var2.toString();
            this.keys.add(var3);
            return new Phrase.KeyToken(var1, var3);
         }
      }
   }

   private Phrase.LeftCurlyBracketToken leftCurlyBracket(Phrase.Token var1) {
      this.consume();
      this.consume();
      return new Phrase.LeftCurlyBracketToken(var1);
   }

   private char lookahead() {
      char var1;
      if(this.curCharIndex < this.pattern.length() - 1) {
         var1 = this.pattern.charAt(this.curCharIndex + 1);
      } else {
         var1 = 0;
      }

      return var1;
   }

   private Phrase.TextToken text(Phrase.Token var1) {
      int var2 = this.curCharIndex;

      while(this.curChar != 123 && this.curChar != 0) {
         this.consume();
      }

      return new Phrase.TextToken(var1, this.curCharIndex - var2);
   }

   private Phrase.Token token(Phrase.Token var1) {
      Object var3;
      if(this.curChar == 0) {
         var3 = null;
      } else if(this.curChar == 123) {
         char var2 = this.lookahead();
         if(var2 == 123) {
            var3 = this.leftCurlyBracket(var1);
         } else {
            if(var2 < 97 || var2 > 122) {
               throw new IllegalArgumentException("Unexpected character '" + var2 + "'; expected key in '" + this.pattern + "'");
            }

            var3 = this.key(var1);
         }
      } else {
         var3 = this.text(var1);
      }

      return (Phrase.Token)var3;
   }

   public CharSequence format() {
      if(this.formatted == null) {
         if(!this.keysToValues.keySet().containsAll(this.keys)) {
            HashSet var3 = new HashSet(this.keys);
            var3.removeAll(this.keysToValues.keySet());
            throw new IllegalArgumentException("Missing keys: " + var3 + " in '" + this.pattern + "'");
         }

         SpannableStringBuilder var2 = new SpannableStringBuilder(this.pattern);

         for(Phrase.Token var1 = this.head; var1 != null; var1 = var1.next) {
            var1.expand(var2, this.keysToValues);
         }

         this.formatted = var2;
      }

      return this.formatted;
   }

   public void into(TextView var1) {
      if(var1 == null) {
         throw new IllegalArgumentException("TextView must not be null.");
      } else {
         var1.setText(this.format());
      }
   }

   public Phrase put(String var1, int var2) {
      return this.put(var1, Integer.toString(var2));
   }

   public Phrase put(String var1, CharSequence var2) {
      if(!this.keys.contains(var1)) {
         throw new IllegalArgumentException("Key '" + var1 + "' not found in '" + this.pattern + "'");
      } else if(var2 == null) {
         throw new IllegalArgumentException("Null value for '" + var1 + "' in '" + this.pattern + "'");
      } else {
         this.keysToValues.put(var1, var2);
         this.formatted = null;
         return this;
      }
   }

   public Phrase putOptional(String var1, int var2) {
      Phrase var3 = this;
      if(this.keys.contains(var1)) {
         var3 = this.put(var1, var2);
      }

      return var3;
   }

   public Phrase putOptional(String var1, CharSequence var2) {
      Phrase var3 = this;
      if(this.keys.contains(var1)) {
         var3 = this.put(var1, var2);
      }

      return var3;
   }

   public String toString() {
      return this.pattern.toString();
   }

   private static class KeyToken extends Phrase.Token {
      private final String key;
      private CharSequence value;

      KeyToken(Phrase.Token var1, String var2) {
         super(var1);
         this.key = var2;
      }

      void expand(SpannableStringBuilder var1, Map var2) {
         this.value = (CharSequence)var2.get(this.key);
         int var3 = this.getFormattedStart();
         var1.replace(var3, this.key.length() + var3 + 2, this.value);
      }

      int getFormattedLength() {
         return this.value.length();
      }
   }

   private static class LeftCurlyBracketToken extends Phrase.Token {
      LeftCurlyBracketToken(Phrase.Token var1) {
         super(var1);
      }

      void expand(SpannableStringBuilder var1, Map var2) {
         int var3 = this.getFormattedStart();
         var1.replace(var3, var3 + 2, "{");
      }

      int getFormattedLength() {
         return 1;
      }
   }

   private static class TextToken extends Phrase.Token {
      private final int textLength;

      TextToken(Phrase.Token var1, int var2) {
         super(var1);
         this.textLength = var2;
      }

      void expand(SpannableStringBuilder var1, Map var2) {
      }

      int getFormattedLength() {
         return this.textLength;
      }
   }

   private abstract static class Token {
      Phrase.Token next;
      private final Phrase.Token prev;

      protected Token(Phrase.Token var1) {
         this.prev = var1;
         if(var1 != null) {
            var1.next = this;
         }

      }

      abstract void expand(SpannableStringBuilder var1, Map var2);

      abstract int getFormattedLength();

      final int getFormattedStart() {
         int var1;
         if(this.prev == null) {
            var1 = 0;
         } else {
            var1 = this.prev.getFormattedStart() + this.prev.getFormattedLength();
         }

         return var1;
      }
   }
}
