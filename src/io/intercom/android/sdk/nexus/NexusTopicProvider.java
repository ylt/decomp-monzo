package io.intercom.android.sdk.nexus;

import java.util.List;

interface NexusTopicProvider {
   List getTopics();
}
