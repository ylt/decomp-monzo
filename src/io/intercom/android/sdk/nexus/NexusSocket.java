package io.intercom.android.sdk.nexus;

import io.intercom.a.f;
import io.intercom.android.sdk.twig.Twig;
import io.intercom.okhttp3.OkHttpClient;
import io.intercom.okhttp3.Request;
import io.intercom.okhttp3.Response;
import io.intercom.okhttp3.WebSocket;
import io.intercom.okhttp3.WebSocketListener;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import org.json.JSONException;
import org.json.JSONObject;

class NexusSocket {
   private static final WebSocket CLOSED_SOCKET = new WebSocket() {
      public void cancel() {
      }

      public boolean close(int var1, String var2) {
         return false;
      }

      public long queueSize() {
         return 0L;
      }

      public Request request() {
         throw new NullPointerException("ClosedSocket has no request");
      }

      public boolean send(f var1) {
         return false;
      }

      public boolean send(String var1) {
         return false;
      }
   };
   private static final String HEADER = "?X-Nexus-Version=android.5.1.0";
   private static final int MAX_RECONNECT_TIME_SECONDS = 900;
   private static final int N_TIMEOUT_DISCONNECT = 4001;
   private static final int OK_CLIENT_DISCONNECT = 4000;
   private final OkHttpClient client;
   private final long connectionTimeoutSeconds;
   private long lastReconnectAt;
   private final NexusListener listener;
   private int reconnectAttempts;
   private ScheduledFuture reconnectFuture;
   private final boolean shouldSendPresence;
   private WebSocket socket;
   private final ScheduledExecutorService timeoutExecutor;
   private ScheduledFuture timeoutFuture;
   private final Runnable timeoutRunnable;
   private final NexusTopicProvider topicProvider;
   private final Twig twig;
   private final String url;
   private final WebSocketListener webSocketListener;

   NexusSocket(String var1, int var2, boolean var3, Twig var4, ScheduledExecutorService var5, OkHttpClient var6, NexusListener var7, NexusTopicProvider var8) {
      this.socket = CLOSED_SOCKET;
      this.timeoutRunnable = new Runnable() {
         public void run() {
            NexusSocket.this.timedOut();
         }
      };
      this.lastReconnectAt = 0L;
      this.reconnectAttempts = 0;
      this.webSocketListener = new WebSocketListener() {
         private void parseJsonString(String var1) {
            if(!var1.isEmpty() && !var1.equals(" ") && !var1.endsWith("|")) {
               try {
                  JSONObject var2 = new JSONObject(var1);
                  String var3 = var2.optString("eventName");
                  Twig var7;
                  if(!var3.isEmpty() && !var3.equals("ACK")) {
                     var7 = NexusSocket.this.twig;
                     StringBuilder var4 = new StringBuilder();
                     var7.internal(var4.append("onMessage TEXT: ").append(var1).toString());
                     NexusListener var9 = NexusSocket.this.listener;
                     NexusEvent var8 = new NexusEvent(var2);
                     var9.notifyEvent(var8);
                  } else {
                     var7 = NexusSocket.this.twig;
                     StringBuilder var6 = new StringBuilder();
                     var7.internal(var6.append("onMessage ACK: ").append(var1).toString());
                  }
               } catch (JSONException var5) {
                  NexusSocket.this.twig.internal("onMessage: json parse exception for message: '" + var1 + " " + var5);
               }
            }

         }

         public void onClosed(WebSocket var1, int var2, String var3) {
            switch(var2) {
            case 4000:
               NexusSocket.this.shutdown();
               break;
            default:
               NexusSocket.this.scheduleReconnect();
            }

            NexusSocket.this.twig.internal("onClose code: " + var2 + " reason: " + var3);
         }

         public void onClosing(WebSocket var1, int var2, String var3) {
            NexusSocket.this.twig.internal("Server requested close:  " + var2 + " - '" + var3 + "'");
            var1.close(var2, var3);
         }

         public void onFailure(WebSocket var1, Throwable var2, Response var3) {
            if(NexusSocket.shouldReconnectFromFailure(var3)) {
               NexusSocket.this.scheduleReconnect();
            } else {
               NexusSocket.this.shutdown();
            }

            NexusSocket.this.twig.internal("onFailure: " + var2.getMessage());
            NexusSocket.this.listener.onConnectFailed();
         }

         public void onMessage(WebSocket var1, f var2) {
            NexusSocket.this.twig.internal("Received bytes message " + var2 + ", resetting timeout");
            NexusSocket.this.resetTimeout();
         }

         public void onMessage(WebSocket var1, String var2) {
            NexusSocket.this.resetTimeout();
            this.parseJsonString(var2);
         }

         public void onOpen(WebSocket var1, Response var2) {
            NexusSocket.this.twig.internal("onOpen: " + var2.message());
            NexusSocket.this.socket = var1;
            NexusSocket.this.resetTimeout();
            List var3 = NexusSocket.this.topicProvider.getTopics();
            if(!var3.isEmpty()) {
               NexusSocket.this.fire(NexusEvent.getSubscribeEvent(var3).toStringEncodedJsonObject());
            }

            if(NexusSocket.this.shouldSendPresence) {
               NexusSocket.this.fire(NexusEvent.getUserPresenceEvent().toStringEncodedJsonObject());
            }

            NexusSocket.this.listener.onConnect();
         }
      };
      this.url = var1;
      this.connectionTimeoutSeconds = (long)var2;
      this.shouldSendPresence = var3;
      this.twig = var4;
      this.listener = var7;
      this.topicProvider = var8;
      this.client = var6;
      this.timeoutExecutor = var5;
   }

   static long calculateReconnectTimerInSeconds(int var0) {
      var0 = (int)Math.min(Math.pow(2.0D, (double)var0), 900.0D);
      return (long)(var0 + (new Random()).nextInt(var0 + 1));
   }

   private void disconnect(int var1, String var2) {
      if(!this.socket.close(var1, var2)) {
         this.twig.internal("Could not close socket while disconnecting, it may be already closed");
      }

   }

   private void modifyReconnectAttempts() {
      if(System.currentTimeMillis() - this.lastReconnectAt > TimeUnit.SECONDS.toMillis(900L) * 2L) {
         this.twig.d("resetting reconnection attempts", new Object[0]);
         this.reconnectAttempts = 1;
      } else {
         this.twig.d("incrementing reconnection attempts", new Object[0]);
         ++this.reconnectAttempts;
      }

      this.lastReconnectAt = System.currentTimeMillis();
   }

   private void resetTimeout() {
      if(this.timeoutFuture != null) {
         this.timeoutFuture.cancel(true);
      }

      this.timeoutFuture = this.timeoutExecutor.schedule(this.timeoutRunnable, this.connectionTimeoutSeconds, TimeUnit.SECONDS);
   }

   private void scheduleReconnect() {
      if(this.reconnectFuture == null) {
         this.modifyReconnectAttempts();
         long var1 = calculateReconnectTimerInSeconds(this.reconnectAttempts);
         this.twig.internal("Scheduling reconnect in: " + var1 + " for attempt: " + this.reconnectAttempts);
         this.reconnectFuture = this.timeoutExecutor.schedule(new Runnable() {
            public void run() {
               NexusSocket.this.connect();
               NexusSocket.this.reconnectFuture = null;
            }
         }, var1, TimeUnit.SECONDS);
      }

   }

   static boolean shouldReconnectFromFailure(Response var0) {
      boolean var3 = true;
      boolean var2 = var3;
      if(var0 != null) {
         int var1 = var0.code();
         if(var1 >= 500 && var1 <= 599) {
            var2 = var3;
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   private void shutdown() {
      this.socket = CLOSED_SOCKET;
      if(this.timeoutFuture != null) {
         this.timeoutFuture.cancel(true);
      }

      this.listener.onShutdown();
   }

   private void timedOut() {
      if(this.socket == CLOSED_SOCKET) {
         this.scheduleReconnect();
      } else {
         this.disconnect(4001, "Socket timed out");
      }

      this.listener.onConnectFailed();
   }

   void connect() {
      this.twig.d("connecting to a socket...", new Object[0]);
      Request var1 = (new Request.Builder()).url(this.url + "?X-Nexus-Version=android.5.1.0").build();
      this.client.newWebSocket(var1, this.webSocketListener);
      this.timeoutFuture = this.timeoutExecutor.schedule(this.timeoutRunnable, this.connectionTimeoutSeconds, TimeUnit.SECONDS);
   }

   void disconnect() {
      this.disconnect(4000, "Disconnect called by client");
   }

   void fire(String var1) {
      if(!var1.isEmpty()) {
         try {
            Twig var3 = this.twig;
            StringBuilder var2 = new StringBuilder();
            var3.internal(var2.append("firing: ").append(var1).toString());
            this.socket.send(var1);
         } catch (IllegalStateException var4) {
            this.twig.internal("Error when firing '" + var1 + "': " + var4);
         }
      }

   }

   boolean isConnected() {
      boolean var1;
      if(this.socket != CLOSED_SOCKET) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }
}
