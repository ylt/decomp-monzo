package io.intercom.android.sdk.nexus;

import io.intercom.android.sdk.twig.Twig;
import io.intercom.okhttp3.OkHttpClient;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

public class NexusClient implements NexusTopicProvider {
   private ScheduledExecutorService backgroundTaskExecutor;
   private final OkHttpClient client;
   private final NexusEventPropagator eventPropagator;
   private ScheduledFuture future;
   private long presenceInterval;
   private final List sockets;
   private final List topics;
   private final Twig twig;

   public NexusClient(Twig var1) {
      this(var1, new NexusEventPropagator(var1));
   }

   NexusClient(Twig var1, NexusEventPropagator var2) {
      this.sockets = new ArrayList();
      this.topics = new ArrayList();
      this.twig = var1;
      this.eventPropagator = var2;
      this.client = (new OkHttpClient.Builder()).readTimeout(130L, TimeUnit.SECONDS).writeTimeout(130L, TimeUnit.SECONDS).connectTimeout(20L, TimeUnit.SECONDS).build();
   }

   private void schedulePresence() {
      if(this.presenceInterval > 0L) {
         this.future = this.backgroundTaskExecutor.schedule(new Runnable() {
            public void run() {
               NexusClient.this.fire(NexusEvent.getUserPresenceEvent());
               NexusClient.this.schedulePresence();
            }
         }, this.presenceInterval, TimeUnit.SECONDS);
      }

   }

   private void subscribeToTopics(List var1) {
      if(!var1.isEmpty()) {
         this.fire(NexusEvent.getSubscribeEvent(var1));
      }

   }

   private void unSubscribeFromTopics(List var1) {
      if(!var1.isEmpty()) {
         this.fire(NexusEvent.getUnsubscribeEvent(var1));
      }

   }

   public void addEventListener(NexusListener var1) {
      this.eventPropagator.addListener(var1);
   }

   public void addTopics(List var1) {
      synchronized(this){}

      try {
         ArrayList var2 = new ArrayList(var1);
         var2.removeAll(this.topics);
         this.subscribeToTopics(var2);
         this.topics.addAll(var2);
      } finally {
         ;
      }

   }

   public void clearTopics() {
      synchronized(this){}

      try {
         this.unSubscribeFromTopics(this.topics);
         this.topics.clear();
      } finally {
         ;
      }

   }

   public void connect(NexusConfig var1, boolean var2) {
      if(var1.getEndpoints().isEmpty()) {
         this.twig.e("No endpoints present", new Object[0]);
      } else {
         if(this.backgroundTaskExecutor == null) {
            NexusClient.NexusThreadFactory var3 = new NexusClient.NexusThreadFactory(null);
            this.backgroundTaskExecutor = Executors.newScheduledThreadPool(var1.getEndpoints().size() + 1, var3);
         }

         Iterator var5 = var1.getEndpoints().iterator();

         while(var5.hasNext()) {
            String var4 = (String)var5.next();
            this.twig.i("Adding socket", new Object[0]);
            NexusSocket var6 = new NexusSocket(var4, var1.getConnectionTimeout(), var2, this.twig, this.backgroundTaskExecutor, this.client, this.eventPropagator, this);
            var6.connect();
            this.sockets.add(var6);
         }

         this.presenceInterval = (long)var1.getPresenceHeartbeatInterval();
         if(var2) {
            this.schedulePresence();
         }
      }

   }

   public void disconnect() {
      // $FF: Couldn't be decompiled
   }

   public void fire(NexusEvent param1) {
      // $FF: Couldn't be decompiled
   }

   public List getTopics() {
      synchronized(this){}

      List var1;
      try {
         var1 = this.topics;
      } finally {
         ;
      }

      return var1;
   }

   public boolean isConnected() {
      // $FF: Couldn't be decompiled
   }

   public void localUpdate(NexusEvent var1) {
      synchronized(this){}

      try {
         this.eventPropagator.notifyEvent(var1);
      } finally {
         ;
      }

   }

   public void removeEventListener(NexusListener var1) {
      this.eventPropagator.removeListener(var1);
   }

   public void removeTopics(List param1) {
      // $FF: Couldn't be decompiled
   }

   public void setTopics(List var1) {
      synchronized(this){}

      try {
         ArrayList var3 = new ArrayList(var1);
         var3.removeAll(this.topics);
         ArrayList var2 = new ArrayList(this.topics);
         var2.removeAll(var1);
         this.subscribeToTopics(var3);
         this.unSubscribeFromTopics(var2);
         this.topics.clear();
         this.topics.addAll(var1);
      } finally {
         ;
      }

   }

   private static class NexusThreadFactory implements ThreadFactory {
      private final ThreadFactory defaultFactory;
      private int threadCount;

      private NexusThreadFactory() {
         this.defaultFactory = Executors.defaultThreadFactory();
         this.threadCount = 0;
      }

      // $FF: synthetic method
      NexusThreadFactory(Object var1) {
         this();
      }

      public Thread newThread(Runnable var1) {
         Thread var2 = this.defaultFactory.newThread(var1);
         ++this.threadCount;
         var2.setName("IntercomNexus-" + this.threadCount);
         return var2;
      }
   }
}
