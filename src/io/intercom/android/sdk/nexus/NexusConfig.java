package io.intercom.android.sdk.nexus;

import java.util.ArrayList;
import java.util.List;

public class NexusConfig {
   private final int connectionTimeout;
   private final List endpoints;
   private final int presenceHeartbeatInterval;

   public NexusConfig() {
      this(new NexusConfig.Builder());
   }

   public NexusConfig(NexusConfig.Builder var1) {
      Object var2;
      if(var1.endpoints == null) {
         var2 = new ArrayList();
      } else {
         var2 = var1.endpoints;
      }

      this.endpoints = (List)var2;
      this.presenceHeartbeatInterval = var1.presence_heartbeat_interval;
      this.connectionTimeout = var1.connection_timeout;
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(this != var1) {
         if(var1 != null && this.getClass() == var1.getClass()) {
            NexusConfig var3 = (NexusConfig)var1;
            if(this.connectionTimeout != var3.connectionTimeout || this.presenceHeartbeatInterval != var3.presenceHeartbeatInterval || !this.endpoints.equals(var3.endpoints)) {
               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public int getConnectionTimeout() {
      return this.connectionTimeout;
   }

   public List getEndpoints() {
      return this.endpoints;
   }

   public int getPresenceHeartbeatInterval() {
      return this.presenceHeartbeatInterval;
   }

   public int hashCode() {
      return (this.endpoints.hashCode() * 31 + this.presenceHeartbeatInterval) * 31 + this.connectionTimeout;
   }

   public static class Builder {
      int connection_timeout;
      List endpoints;
      int presence_heartbeat_interval;

      public NexusConfig build() {
         return new NexusConfig(this);
      }

      public NexusConfig.Builder withConnectionTimeout(int var1) {
         this.connection_timeout = var1;
         return this;
      }

      public NexusConfig.Builder withEndpoints(List var1) {
         this.endpoints = var1;
         return this;
      }

      public NexusConfig.Builder withPresenceHeartbeatInterval(int var1) {
         this.presence_heartbeat_interval = var1;
         return this;
      }
   }
}
