package io.intercom.android.sdk.nexus;

import android.util.LruCache;
import io.intercom.android.sdk.twig.Twig;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

class NexusEventPropagator implements NexusListener {
   private final LruCache cache = new LruCache(100);
   private final List listeners = new CopyOnWriteArrayList();
   private final Twig twig;

   public NexusEventPropagator(Twig var1) {
      this.twig = var1;
   }

   void addListener(NexusListener var1) {
      this.listeners.add(var1);
   }

   void cacheEvent(NexusEvent var1) {
      this.cache.put(var1.getGuid(), Boolean.valueOf(true));
   }

   public void notifyEvent(NexusEvent param1) {
      // $FF: Couldn't be decompiled
   }

   public void onConnect() {
      this.twig.d("notifying listeners that a connection opened", new Object[0]);
      Iterator var1 = this.listeners.iterator();

      while(var1.hasNext()) {
         ((NexusListener)var1.next()).onConnect();
      }

   }

   public void onConnectFailed() {
      this.twig.d("notifying listeners that a connection failed to open", new Object[0]);
      Iterator var1 = this.listeners.iterator();

      while(var1.hasNext()) {
         ((NexusListener)var1.next()).onConnectFailed();
      }

   }

   public void onShutdown() {
      this.twig.d("notifying listeners that a connection has shutdown", new Object[0]);
      Iterator var1 = this.listeners.iterator();

      while(var1.hasNext()) {
         ((NexusListener)var1.next()).onShutdown();
      }

   }

   void removeListener(NexusListener var1) {
      this.listeners.remove(var1);
   }
}
