package io.intercom.android.sdk.nexus;

import java.util.HashMap;

public class EventData extends HashMap {
   public EventData() {
   }

   public EventData(int var1) {
      super(var1);
   }

   public long optLong(String var1) {
      return this.optLong(var1, -1L);
   }

   public long optLong(String var1, long var2) {
      Object var4 = this.get(var1);
      if(var4 instanceof Long) {
         var2 = ((Long)var4).longValue();
      }

      return var2;
   }

   public String optString(String var1) {
      return this.optString(var1, "");
   }

   public String optString(String var1, String var2) {
      Object var3 = this.get(var1);
      if(var3 instanceof String) {
         var1 = (String)var3;
      } else {
         var1 = var2;
      }

      return var1;
   }
}
