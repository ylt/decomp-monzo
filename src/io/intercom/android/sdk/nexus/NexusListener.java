package io.intercom.android.sdk.nexus;

public interface NexusListener {
   void notifyEvent(NexusEvent var1);

   void onConnect();

   void onConnectFailed();

   void onShutdown();
}
