package io.intercom.android.sdk.nexus;

import android.text.TextUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import org.json.JSONArray;
import org.json.JSONObject;

public class NexusEvent {
   static final String ADMIN_AVATAR = "adminAvatar";
   static final String ADMIN_ID = "adminId";
   static final String ADMIN_NAME = "adminName";
   static final String ADMIN_TIMESTAMP = "adminTimestamp";
   static final String ASSIGNEE_ID = "assigneeId";
   static final String CONVERSATION_ID = "conversationId";
   static final String EVENT_DATA = "eventData";
   static final String EVENT_GUID = "eventGuid";
   static final String EVENT_NAME = "eventName";
   static final String NX_FROM_USER = "nx.FromUser";
   static final String NX_TOPICS = "nx.Topics";
   static final String NX_TO_USER = "nx.ToUser";
   static final String TOPIC_PREFIX_CONVERSATION = "conversation/";
   private final EventData eventData;
   private final NexusEventType eventType;
   private final String guid;
   private final List topics;
   private final String userId;

   public NexusEvent(NexusEvent.Builder var1) {
      this.eventType = var1.eventName;
      this.eventData = new EventData();
      String var2;
      if(var1.eventData != null) {
         Iterator var4 = var1.eventData.keySet().iterator();

         while(var4.hasNext()) {
            var2 = (String)var4.next();
            Object var3 = var1.eventData.get(var2);
            if(var3 != null) {
               this.eventData.put(var2, var3);
            }
         }
      }

      this.topics = new ArrayList();
      if(var1.topics != null) {
         Iterator var6 = var1.topics.iterator();

         while(var6.hasNext()) {
            var2 = (String)var6.next();
            if(!TextUtils.isEmpty(var2)) {
               this.topics.add(var2);
            }
         }
      }

      this.guid = UUID.randomUUID().toString();
      String var5;
      if(var1.userId != null) {
         var5 = var1.userId;
      } else {
         var5 = "";
      }

      this.userId = var5;
   }

   public NexusEvent(JSONObject var1) {
      this.eventType = NexusEventType.safeValueOf(var1.optString("eventName"));
      JSONObject var5 = var1.optJSONObject("eventData");
      String var3;
      if(var5 != null && var5.length() > 0) {
         this.eventData = new EventData(var5.length());
         Iterator var6 = var5.keys();

         while(var6.hasNext()) {
            var3 = (String)var6.next();
            Object var4 = var5.opt(var3);
            if(var4 != null) {
               this.eventData.put(var3, var4);
            }
         }
      } else {
         this.eventData = new EventData();
      }

      this.topics = new ArrayList();
      JSONArray var7 = var1.optJSONArray("nx.Topics");
      if(var7 != null) {
         for(int var2 = 0; var2 < var7.length(); ++var2) {
            var3 = var7.optString(var2);
            if(!TextUtils.isEmpty(var3)) {
               this.topics.add(var3);
            }
         }
      }

      this.guid = var1.optString("eventGuid");
      if(var1.has("nx.ToUser")) {
         this.userId = var1.optString("nx.ToUser");
      } else {
         this.userId = var1.optString("nx.FromUser");
      }

   }

   private static List conversationTopics(String var0) {
      return Collections.singletonList("conversation/" + var0);
   }

   public static NexusEvent getAdminIsTypingEvent(String var0, String var1, String var2, String var3, String var4) {
      EventData var5 = new EventData(4);
      var5.put("conversationId", var0);
      var5.put("adminId", var1);
      var5.put("adminName", var2);
      var5.put("adminAvatar", var3);
      return (new NexusEvent.Builder(NexusEventType.AdminIsTyping)).withEventData(var5).withUserId(var4).withTopics(conversationTopics(var0)).build();
   }

   public static NexusEvent getAdminIsTypingNoteEvent(String var0, String var1, String var2, String var3, String var4) {
      EventData var5 = new EventData(4);
      var5.put("adminId", var1);
      var5.put("conversationId", var0);
      var5.put("adminName", var2);
      var5.put("adminAvatar", var3);
      return (new NexusEvent.Builder(NexusEventType.AdminIsTypingANote)).withEventData(var5).withTopics(conversationTopics(var0)).withUserId(var4).build();
   }

   public static NexusEvent getConversationAssignedEvent(String var0, String var1, String var2) {
      EventData var3 = new EventData(3);
      var3.put("adminId", var1);
      var3.put("conversationId", var0);
      var3.put("assigneeId", var2);
      return (new NexusEvent.Builder(NexusEventType.ConversationAssigned)).withEventData(var3).build();
   }

   public static NexusEvent getConversationClosedEvent(String var0, String var1) {
      EventData var2 = new EventData(2);
      var2.put("adminId", var1);
      var2.put("conversationId", var0);
      return (new NexusEvent.Builder(NexusEventType.ConversationClosed)).withEventData(var2).build();
   }

   public static NexusEvent getConversationReopenedEvent(String var0, String var1) {
      EventData var2 = new EventData(2);
      var2.put("adminId", var1);
      var2.put("conversationId", var0);
      return (new NexusEvent.Builder(NexusEventType.ConversationReopened)).withEventData(var2).build();
   }

   public static NexusEvent getConversationSeenAdminEvent(String var0, String var1) {
      EventData var2 = new EventData(1);
      var2.put("conversationId", var0);
      return (new NexusEvent.Builder(NexusEventType.UserContentSeenByAdmin)).withEventData(var2).withTopics(conversationTopics(var0)).withUserId(var1).build();
   }

   public static NexusEvent getConversationSeenEvent(String var0, String var1) {
      EventData var2 = new EventData(1);
      var2.put("conversationId", var0);
      return (new NexusEvent.Builder(NexusEventType.ConversationSeen)).withEventData(var2).withTopics(conversationTopics(var0)).withUserId(var1).build();
   }

   public static NexusEvent getNewCommentEvent(String var0, String var1) {
      EventData var2 = new EventData(1);
      var2.put("conversationId", var0);
      return (new NexusEvent.Builder(NexusEventType.NewComment)).withEventData(var2).withTopics(conversationTopics(var0)).withUserId(var1).build();
   }

   public static NexusEvent getNewNoteEvent(String var0, String var1) {
      EventData var2 = new EventData(2);
      var2.put("adminId", var1);
      var2.put("conversationId", var0);
      return (new NexusEvent.Builder(NexusEventType.NewNote)).withEventData(var2).build();
   }

   public static NexusEvent getSubscribeEvent(List var0) {
      return (new NexusEvent.Builder(NexusEventType.Subscribe)).withTopics(var0).build();
   }

   public static NexusEvent getUnsubscribeEvent(List var0) {
      return (new NexusEvent.Builder(NexusEventType.Unsubscribe)).withTopics(var0).build();
   }

   public static NexusEvent getUserIsTypingEvent(String var0, String var1) {
      EventData var2 = new EventData(1);
      var2.put("conversationId", var0);
      return (new NexusEvent.Builder(NexusEventType.UserIsTyping)).withEventData(var2).withTopics(conversationTopics(var0)).withUserId(var1).build();
   }

   public static NexusEvent getUserPresenceEvent() {
      return (new NexusEvent.Builder(NexusEventType.UserPresence)).build();
   }

   public EventData getEventData() {
      return this.eventData;
   }

   public NexusEventType getEventType() {
      return this.eventType;
   }

   public String getGuid() {
      return this.guid;
   }

   public List getTopics() {
      return this.topics;
   }

   public String getUserId() {
      return this.userId;
   }

   public String toStringEncodedJsonObject() {
      return this.eventType.toStringEncodedJsonObject(this);
   }

   static class Builder {
      EventData eventData;
      final NexusEventType eventName;
      List topics;
      String userId;

      Builder(NexusEventType var1) {
         this.eventName = var1;
      }

      public NexusEvent build() {
         return new NexusEvent(this);
      }

      public NexusEvent.Builder withEventData(EventData var1) {
         this.eventData = var1;
         return this;
      }

      public NexusEvent.Builder withTopics(List var1) {
         this.topics = var1;
         return this;
      }

      public NexusEvent.Builder withUserId(String var1) {
         this.userId = var1;
         return this;
      }
   }
}
