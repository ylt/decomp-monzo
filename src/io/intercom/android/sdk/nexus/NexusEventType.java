package io.intercom.android.sdk.nexus;

import java.util.List;
import java.util.UUID;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public enum NexusEventType {
   AdminIsTyping {
      protected JSONObject toJsonObject(NexusEvent var1) throws JSONException {
         JSONObject var3 = super.toJsonObject(var1);
         JSONObject var2 = var3.optJSONObject("eventData");
         var2.put("adminName", var1.getEventData().optString("adminName"));
         var2.put("adminId", var1.getEventData().optString("adminId"));
         var2.put("adminAvatar", var1.getEventData().optString("adminAvatar"));
         var3.put("nx.ToUser", var1.getUserId());
         return var3;
      }
   },
   AdminIsTypingANote {
      protected JSONObject toJsonObject(NexusEvent var1) throws JSONException {
         JSONObject var2 = super.toJsonObject(var1);
         JSONObject var3 = var2.optJSONObject("eventData");
         var3.put("adminName", var1.getEventData().optString("adminName"));
         var3.put("adminId", var1.getEventData().optString("adminId"));
         var3.put("adminAvatar", var1.getEventData().optString("adminAvatar"));
         var2.put("nx.ToUser", var1.getUserId());
         return var2;
      }
   },
   ConversationAssigned {
      protected JSONObject toJsonObject(NexusEvent var1) throws JSONException {
         JSONObject var2 = super.toJsonObject(var1);
         JSONObject var3 = var2.optJSONObject("eventData");
         var3.put("adminId", var1.getEventData().optString("adminId"));
         var3.put("assigneeId", var1.getEventData().optString("assigneeId"));
         return var2;
      }
   },
   ConversationClosed {
      protected JSONObject toJsonObject(NexusEvent var1) throws JSONException {
         JSONObject var2 = super.toJsonObject(var1);
         var2.optJSONObject("eventData").put("adminId", var1.getEventData().optString("adminId"));
         return var2;
      }
   },
   ConversationReopened {
      protected JSONObject toJsonObject(NexusEvent var1) throws JSONException {
         JSONObject var2 = super.toJsonObject(var1);
         var2.getJSONObject("eventData").put("adminId", var1.getEventData().optString("adminId"));
         return var2;
      }
   },
   ConversationSeen {
      protected JSONObject toJsonObject(NexusEvent var1) throws JSONException {
         JSONObject var2 = super.toJsonObject(var1);
         var2.put("nx.FromUser", var1.getUserId());
         return var2;
      }
   },
   NewComment {
      protected JSONObject toJsonObject(NexusEvent var1) throws JSONException {
         JSONObject var2 = super.toJsonObject(var1);
         var2.put("nx.ToUser", var1.getUserId());
         return var2;
      }
   },
   NewNote {
      protected JSONObject toJsonObject(NexusEvent var1) throws JSONException {
         JSONObject var2 = super.toJsonObject(var1);
         var2.optJSONObject("eventData").put("adminId", var1.getEventData().optString("adminId"));
         return var2;
      }
   },
   Subscribe {
      protected JSONObject toJsonObject(NexusEvent var1) throws JSONException {
         JSONObject var2 = super.toJsonObject(var1);
         var2.put("eventName", "nx." + this.name());
         return var2;
      }
   },
   ThreadAssigned {
      protected JSONObject toJsonObject(NexusEvent var1) throws JSONException {
         JSONObject var3 = super.toJsonObject(var1);
         JSONObject var2 = var3.optJSONObject("eventData");
         var2.put("adminId", var1.getEventData().optString("adminId"));
         var2.put("assigneeId", var1.getEventData().optString("assigneeId"));
         return var3;
      }
   },
   ThreadClosed {
      protected JSONObject toJsonObject(NexusEvent var1) throws JSONException {
         JSONObject var2 = super.toJsonObject(var1);
         var2.optJSONObject("eventData").put("adminId", var1.getEventData().optString("adminId"));
         return var2;
      }
   },
   ThreadCreated {
      protected JSONObject toJsonObject(NexusEvent var1) throws JSONException {
         JSONObject var2 = super.toJsonObject(var1);
         var2.put("nx.ToUser", var1.getUserId());
         return var2;
      }
   },
   ThreadReopened {
      protected JSONObject toJsonObject(NexusEvent var1) throws JSONException {
         JSONObject var2 = super.toJsonObject(var1);
         var2.getJSONObject("eventData").put("adminId", var1.getEventData().optString("adminId"));
         return var2;
      }
   },
   ThreadUpdated {
      protected JSONObject toJsonObject(NexusEvent var1) throws JSONException {
         JSONObject var2 = super.toJsonObject(var1);
         var2.getJSONObject("eventData").put("adminId", var1.getEventData().optString("adminId"));
         return var2;
      }
   },
   UNKNOWN {
      protected String toStringEncodedJsonObject(NexusEvent var1) {
         return "";
      }
   },
   Unsubscribe {
      protected JSONObject toJsonObject(NexusEvent var1) throws JSONException {
         JSONObject var2 = super.toJsonObject(var1);
         var2.put("eventName", "nx." + this.name());
         return var2;
      }
   },
   UserContentSeenByAdmin {
      protected JSONObject toJsonObject(NexusEvent var1) throws JSONException {
         JSONObject var2 = super.toJsonObject(var1);
         var2.put("nx.ToUser", var1.getUserId());
         return var2;
      }
   },
   UserIsTyping {
      protected JSONObject toJsonObject(NexusEvent var1) throws JSONException {
         JSONObject var2 = super.toJsonObject(var1);
         var2.put("nx.FromUser", var1.getUserId());
         return var2;
      }
   },
   UserPresence {
      protected JSONObject toJsonObject(NexusEvent var1) throws JSONException {
         JSONObject var2 = new JSONObject();
         JSONObject var3 = new JSONObject();
         var2.put("eventGuid", UUID.randomUUID().toString());
         var2.put("eventName", "nx." + this.name());
         var2.put("eventData", var3);
         return var2;
      }
   };

   private NexusEventType() {
   }

   // $FF: synthetic method
   NexusEventType(Object var3) {
      this();
   }

   public static NexusEventType safeValueOf(String var0) {
      NexusEventType var2;
      try {
         var2 = valueOf(var0);
      } catch (Exception var1) {
         var2 = UNKNOWN;
      }

      return var2;
   }

   protected JSONObject toJsonObject(NexusEvent var1) throws JSONException {
      JSONObject var2 = new JSONObject();
      JSONObject var3 = new JSONObject();
      var3.put("conversationId", var1.getEventData().optString("conversationId"));
      var2.put("eventGuid", var1.getGuid());
      var2.put("eventName", this.name());
      var2.put("eventData", var3);
      List var4 = var1.getTopics();
      if(!var4.isEmpty()) {
         var2.put("nx.Topics", new JSONArray(var4));
      }

      return var2;
   }

   protected String toStringEncodedJsonObject(NexusEvent var1) {
      String var3;
      try {
         var3 = this.toJsonObject(var1).toString();
      } catch (JSONException var2) {
         var3 = "";
      }

      return var3;
   }
}
