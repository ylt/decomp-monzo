package io.intercom.android.sdk.metrics.ops;

import io.intercom.android.sdk.commons.utilities.TimeProvider;
import io.intercom.android.sdk.metrics.MetricsStore;
import java.util.ArrayList;
import java.util.List;

public class OpsMetricTracker {
   public static final String FINISH = "finish";
   public static final String LOAD_CONVERSATION_TIME_TO_COMPLETE_REQUEST = "time-to-complete-request-load-conversation-ms";
   public static final String LOAD_CONVERSATION_TIME_TO_PROCESS_ACTION = "time-to-process-action-load-conversation-ms";
   public static final String LOAD_CONVERSATION_TIME_TO_RENDER_RESULT = "time-to-render-result-load-conversation-ms";
   public static final String SEND_PART_TIME_TO_COMPLETE_REQUEST = "time-to-complete-request-send-part-ms";
   public static final String SEND_PART_TIME_TO_PROCESS_ACTION = "time-to-process-action-send-part-ms";
   public static final String SEND_PART_TIME_TO_RENDER_RESULT = "time-to-render-result-send-part-ms";
   public static final String START = "start";
   public static final String TIMING_TYPE = "timing";
   final List events = new ArrayList();
   private final MetricsStore store;
   private final TimeProvider timeProvider;

   public OpsMetricTracker(MetricsStore var1, TimeProvider var2) {
      this.store = var1;
      this.timeProvider = var2;
   }

   private OpsEvent findStartEvent(String var1) {
      int var2 = this.events.size() - 1;

      OpsEvent var4;
      while(true) {
         if(var2 < 0) {
            var4 = null;
            break;
         }

         OpsEvent var3 = (OpsEvent)this.events.get(var2);
         if("start".equals(var3.getEventType()) && var1.equals(var3.getName())) {
            var4 = var3;
            break;
         }

         --var2;
      }

      return var4;
   }

   private void trackOpsEvent(OpsEvent var1) {
      OpsEvent var2 = this.findStartEvent(var1.getName());
      if(var2 != null) {
         this.events.remove(var2);
         this.store.track(new OpsMetricObject("timing", var2.getName(), var1.getTimestamp() - var2.getTimestamp()));
      }

   }

   public void clear() {
      this.events.clear();
   }

   public void trackEvent(String var1, String var2) {
      OpsEvent var3 = new OpsEvent(var1, var2, this.timeProvider.currentTimeMillis());
      if("finish".equals(var1)) {
         this.trackOpsEvent(var3);
      } else {
         this.events.add(var3);
      }

   }
}
