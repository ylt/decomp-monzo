package io.intercom.android.sdk.metrics.ops;

import io.intercom.android.sdk.metrics.MetricInterface;
import io.intercom.com.google.gson.e;

public class OpsMetricObject implements MetricInterface {
   private final String name;
   private final String type;
   private final long value;

   public OpsMetricObject(String var1, String var2, long var3) {
      this.type = var1;
      this.name = var2;
      this.value = var3;
   }

   public boolean equals(Object var1) {
      boolean var4 = true;
      boolean var3 = false;
      boolean var2;
      if(this == var1) {
         var2 = true;
      } else {
         var2 = var3;
         if(var1 != null) {
            var2 = var3;
            if(this.getClass() == var1.getClass()) {
               OpsMetricObject var5 = (OpsMetricObject)var1;
               var2 = var3;
               if(this.value == var5.value) {
                  if(this.type != null) {
                     var2 = var3;
                     if(!this.type.equals(var5.type)) {
                        return var2;
                     }
                  } else if(var5.type != null) {
                     var2 = var3;
                     return var2;
                  }

                  if(this.name != null) {
                     var2 = this.name.equals(var5.name);
                  } else {
                     var2 = var4;
                     if(var5.name != null) {
                        var2 = false;
                     }
                  }
               }
            }
         }
      }

      return var2;
   }

   public int hashCode() {
      int var2 = 0;
      int var1;
      if(this.type != null) {
         var1 = this.type.hashCode();
      } else {
         var1 = 0;
      }

      if(this.name != null) {
         var2 = this.name.hashCode();
      }

      return (var1 * 31 + var2) * 31 + (int)(this.value ^ this.value >>> 32);
   }

   public String toJson(e var1) {
      return var1.a((Object)this);
   }

   public String toString() {
      return "OpsMetricObject{type='" + this.type + '\'' + ", name='" + this.name + '\'' + ", value=" + this.value + '}';
   }
}
