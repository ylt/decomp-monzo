package io.intercom.android.sdk.metrics;

import android.content.Context;
import io.intercom.android.sdk.Provider;
import io.intercom.android.sdk.api.Api;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.android.sdk.logger.LumberMill;
import io.intercom.android.sdk.metrics.ops.OpsMetricObject;
import io.intercom.android.sdk.twig.Twig;
import io.intercom.com.google.gson.e;
import io.intercom.retrofit2.Call;
import io.intercom.retrofit2.Callback;
import io.intercom.retrofit2.Response;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class MetricsStore {
   private static final String DELIMITER = "~";
   private static final Executor EXECUTOR = Executors.newSingleThreadExecutor();
   private static final String METRICS_DISK_CACHE = "intercomMetricsDiskCache";
   private static final String OPS_METRICS_DISK_CACHE = "intercomOpsMetricsDiskCache";
   final Provider apiProvider;
   private final Provider appConfigProvider;
   final Context context;
   private final Executor executor;
   final e gson;
   final String installerPackageName;
   final boolean isDebugBuild;
   final List metrics;
   final List opsMetrics;
   private final Twig twig;

   public MetricsStore(Context var1, Provider var2, Provider var3) {
      this(var1, var2, var3, EXECUTOR);
   }

   MetricsStore(Context var1, Provider var2, Provider var3, Executor var4) {
      this.metrics = new ArrayList();
      this.opsMetrics = new ArrayList();
      this.gson = new e();
      this.twig = LumberMill.getLogger();
      this.context = var1;
      this.apiProvider = var2;
      this.appConfigProvider = var3;
      this.executor = var4;
      this.isDebugBuild = AppTypeDetector.isDebugBuild(var1);
      this.installerPackageName = AppTypeDetector.getInstallerPackageName(var1);
   }

   private void getMetrics(String[] param1, Class param2) {
      // $FF: Couldn't be decompiled
   }

   private boolean isDisabled() {
      boolean var1;
      if(!((AppConfig)this.appConfigProvider.get()).isMetricsEnabled()) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   private String[] loadObjectsAsJson(String param1) throws IOException {
      // $FF: Couldn't be decompiled
   }

   void cleanUpMetrics(List var1, List var2) {
      this.metrics.removeAll(var1);
      this.opsMetrics.removeAll(var2);
      this.context.deleteFile("intercomMetricsDiskCache");
      this.context.deleteFile("intercomOpsMetricsDiskCache");
      Iterator var3;
      if(!this.metrics.isEmpty()) {
         var3 = this.metrics.iterator();

         while(var3.hasNext()) {
            this.persistMetric((MetricObject)var3.next(), "intercomMetricsDiskCache");
         }
      }

      if(!this.opsMetrics.isEmpty()) {
         var3 = this.opsMetrics.iterator();

         while(var3.hasNext()) {
            this.persistMetric((OpsMetricObject)var3.next(), "intercomOpsMetricsDiskCache");
         }
      }

   }

   public void loadAndSendCachedMetrics() {
      this.executor.execute(new Runnable() {
         public void run() {
            try {
               MetricsStore.this.readMetricsFromDisk();
               MetricsStore.this.sendMetrics();
            } catch (IOException var2) {
               MetricsStore.this.twig.internal("Couldn't read metric from disk: " + var2.getMessage());
            }

         }
      });
   }

   void persistMetric(MetricInterface param1, String param2) {
      // $FF: Couldn't be decompiled
   }

   void readMetricsFromDisk() throws IOException {
      this.getMetrics(this.loadObjectsAsJson("intercomMetricsDiskCache"), MetricObject.class);
      this.getMetrics(this.loadObjectsAsJson("intercomOpsMetricsDiskCache"), OpsMetricObject.class);
   }

   public void sendMetrics() {
      this.executor.execute(new Runnable() {
         public void run() {
            if(!MetricsStore.this.metrics.isEmpty() || !MetricsStore.this.opsMetrics.isEmpty()) {
               final ArrayList var1 = new ArrayList(MetricsStore.this.metrics);
               final ArrayList var2 = new ArrayList(MetricsStore.this.opsMetrics);
               ((Api)MetricsStore.this.apiProvider.get()).sendMetrics(var1, var2, new Callback() {
                  public void onFailure(Call var1x, Throwable var2x) {
                  }

                  public void onResponse(Call var1x, Response var2x) {
                     if(var2x.isSuccessful()) {
                        MetricsStore.this.executor.execute(new Runnable() {
                           public void run() {
                              MetricsStore.this.cleanUpMetrics(var1, var2);
                           }
                        });
                     }

                  }
               });
            }

         }
      });
   }

   public void track(final MetricObject var1) {
      if(this.isDisabled()) {
         this.twig.internal("Metrics have been remotely disabled");
      } else {
         this.executor.execute(new Runnable() {
            public void run() {
               MetricsStore.this.metrics.add(var1.addInstallerPackageName(MetricsStore.this.installerPackageName).addIsDebugBuild(MetricsStore.this.isDebugBuild));
               MetricsStore.this.persistMetric(var1, "intercomMetricsDiskCache");
            }
         });
      }

   }

   public void track(final OpsMetricObject var1) {
      if(this.isDisabled()) {
         this.twig.internal("Metrics have been remotely disabled");
      } else {
         this.executor.execute(new Runnable() {
            public void run() {
               MetricsStore.this.opsMetrics.add(var1);
               MetricsStore.this.persistMetric(var1, "intercomOpsMetricsDiskCache");
            }
         });
      }

   }
}
