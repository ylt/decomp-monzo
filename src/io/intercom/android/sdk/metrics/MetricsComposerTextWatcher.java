package io.intercom.android.sdk.metrics;

import android.text.Editable;
import android.text.TextUtils;
import io.intercom.android.sdk.utilities.SimpleTextWatcher;

public class MetricsComposerTextWatcher extends SimpleTextWatcher {
   private String conversationId;
   private boolean hasTrackedEvent;
   private final MetricTracker metricTracker;

   public MetricsComposerTextWatcher(MetricTracker var1) {
      this.metricTracker = var1;
   }

   public void afterTextChanged(Editable var1) {
      if(!this.hasTrackedEvent && var1.length() != 0) {
         if(TextUtils.isEmpty(this.conversationId)) {
            this.metricTracker.typeInNewConversation();
         } else {
            this.metricTracker.typeInConversation(this.conversationId);
         }

         this.hasTrackedEvent = true;
      }

   }

   public void reset() {
      this.hasTrackedEvent = false;
   }

   public void setConversationId(String var1) {
      this.conversationId = var1;
   }
}
