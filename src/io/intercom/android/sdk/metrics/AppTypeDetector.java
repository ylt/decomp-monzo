package io.intercom.android.sdk.metrics;

import android.content.Context;
import android.text.TextUtils;
import io.intercom.android.sdk.logger.LumberMill;
import io.intercom.android.sdk.twig.Twig;
import javax.security.auth.x500.X500Principal;

class AppTypeDetector {
   private static final X500Principal DEBUG_DN = new X500Principal("CN=Android Debug,O=Android,C=US");
   private static final Twig twig = LumberMill.getLogger();

   static String getInstallerPackageName(Context var0) {
      String var1 = null;

      String var3;
      try {
         var3 = var0.getPackageManager().getInstallerPackageName(var0.getPackageName());
      } catch (Exception var2) {
         twig.internal("Package name is unknown, error: " + var2.getMessage());
         var3 = var1;
      }

      var1 = var3;
      if(TextUtils.isEmpty(var3)) {
         var1 = "unknown";
      }

      return var1;
   }

   static boolean isDebugBuild(Context param0) {
      // $FF: Couldn't be decompiled
   }
}
