package io.intercom.android.sdk.metrics;

import io.intercom.android.sdk.commons.utilities.TimeProvider;
import io.intercom.android.sdk.identity.UserIdentity;
import io.intercom.android.sdk.overlay.LauncherOpenBehaviour;
import io.intercom.android.sdk.profile.ProfilePresenter;
import java.util.UUID;

public class MetricTracker {
   static final String ACTION_CLICKED = "clicked";
   static final String ACTION_CLOSED = "closed";
   static final String ACTION_DISMISSED = "dismissed";
   static final String ACTION_OPENED = "opened";
   static final String ACTION_RECEIVED = "received";
   static final String ACTION_SEARCHED = "searched";
   static final String ACTION_SENT = "sent";
   static final String ACTION_TYPED = "typed";
   static final String ACTION_VIEWED = "viewed";
   static final String CONTEXT_FROM_ANDROID_BACK_BUTTON = "from_android_back_button";
   static final String CONTEXT_FROM_AUTO = "from_auto";
   static final String CONTEXT_FROM_CLICKING = "from_clicking";
   static final String CONTEXT_FROM_CLOSE_BUTTON = "from_close_button";
   static final String CONTEXT_FROM_CONVERSATION = "from_conversation";
   static final String CONTEXT_FROM_CONVERSATION_LIST = "from_conversation_list";
   static final String CONTEXT_FROM_CUSTOM_LAUNCHER = "from_custom_launcher";
   static final String CONTEXT_FROM_FULL = "from_full";
   static final String CONTEXT_FROM_LAUNCHER = "from_launcher";
   static final String CONTEXT_FROM_NEW_CONVERSATION = "from_new_conversation";
   static final String CONTEXT_FROM_PUSH = "from_push";
   static final String CONTEXT_FROM_SCROLLING = "from_scrolling";
   static final String CONTEXT_FROM_SNIPPET = "from_snippet";
   static final String CONTEXT_IN_CONVERSATION = "in_conversation";
   static final String CONTEXT_IN_NEW_CONVERSATION = "in_new_conversation";
   static final String CONTEXT_ON_ARTICLE = "on_article";
   static final String METADATA_BADGE_VALUE = "badge_value";
   static final String METADATA_COMMENT_ID = "comment_id";
   static final String METADATA_CONTENT = "content";
   static final String METADATA_CONVERSATION_ID = "conversation_id";
   static final String METADATA_HAS_ARTICLE_CARD = "has_article_card";
   static final String METADATA_IS_ANNOTATED = "is_annotated";
   static final String METADATA_IS_ATTACHMENT = "is_attachment";
   static final String METADATA_IS_GIF = "is_gif";
   static final String METADATA_MESSAGE_ID = "message_id";
   static final String METADATA_MESSAGE_TYPE = "message_type";
   static final String METADATA_PUSH_TYPE = "push_type";
   static final String METADATA_QUESTION = "question";
   static final String METADATA_REACTION_INDEX = "reaction_index";
   static final String METADATA_SEARCH_QUERY = "search_query";
   static final String METADATA_SUGGESTIONS = "suggestions";
   static final String METADATA_TEAMMATE_STATUS = "teammate_status";
   static final String METADATA_TIME_SINCE_LAST_ACTIVE = "time_since_last_active";
   static final String METADATA_WITHIN_OFFICE_HOURS = "within_office_hours";
   static final String NAME_EDUCATE_METRIC = "educate_event";
   static final String NAME_MV3_METRIC = "mv3_metric";
   static final String OBJECT_ARTICLE = "article";
   static final String OBJECT_BADGE = "badge";
   static final String OBJECT_CONVERSATION = "conversation";
   static final String OBJECT_CONVERSATION_LIST = "conversation_list";
   static final String OBJECT_EXPANDED_GIF_INPUT = "expanded_gif_input";
   static final String OBJECT_EXPANDED_IMAGE_INPUT = "expanded_image_input";
   static final String OBJECT_GIF_INPUT = "gif_input";
   static final String OBJECT_HELP_CENTER = "help_center";
   static final String OBJECT_IMAGE_INPUT = "image_input";
   static final String OBJECT_MESSAGE = "message";
   static final String OBJECT_MESSENGER = "messenger";
   static final String OBJECT_NEW_CONVERSATION = "new_conversation";
   static final String OBJECT_OPERATOR_REPLY = "operator_reply";
   static final String OBJECT_REACTION = "reaction";
   static final String OBJECT_REPLY = "reply";
   static final String OBJECT_TEAMMATE_PROFILE = "teammate_profile";
   static final String OBJECT_TEAM_PROFILE = "team_profile";
   static final String OBJECT_TEXT_INPUT = "text_input";
   static final String OWNER_EDUCATE = "educate";
   static final String OWNER_MESSENGER = "messenger";
   static final String PLACE_IN_APP = "in_app";
   static final String PLACE_MESSENGER = "messenger";
   static final String PLACE_PUSH = "push";
   static final String VALUE_ACTIVE = "active";
   static final String VALUE_AWAY = "away";
   static final String VALUE_MESSAGE = "message";
   static final String VALUE_NOTIFICATION = "notification";
   private final MetricsStore store;
   private final TimeProvider timeProvider;
   private final UserIdentity userIdentity;

   public MetricTracker(UserIdentity var1, MetricsStore var2) {
      this(var1, var2, TimeProvider.SYSTEM);
   }

   MetricTracker(UserIdentity var1, MetricsStore var2, TimeProvider var3) {
      this.userIdentity = var1;
      this.timeProvider = var3;
      this.store = var2;
   }

   private MetricObject newMetric(String var1, String var2, String var3, String var4, String var5, String var6) {
      return new MetricObject(var1, var2, UUID.randomUUID().toString(), this.userIdentity.getIntercomId(), var3, var4, var5, var6, this.timeProvider);
   }

   public void clickedInput(String var1, String var2) {
      this.store.track(this.newMetric("mv3_metric", "messenger", "clicked", var2, "messenger", "in_conversation").addMetaData("conversation_id", var1));
   }

   public void closedArticle(String var1, String var2, @MetricTracker.CloseActionContext int var3) {
      String var4;
      if(var3 == 0) {
         var4 = "from_close_button";
      } else {
         var4 = "from_android_back_button";
      }

      this.store.track(this.newMetric("educate_event", "educate", "closed", "article", "messenger", var4).addMetaData("message_id", var2).addMetaData("conversation_id", var1));
   }

   public void closedHelpCenter(String var1, @MetricTracker.CloseActionContext int var2) {
      String var3;
      if(var2 == 0) {
         var3 = "from_close_button";
      } else {
         var3 = "from_android_back_button";
      }

      this.store.track(this.newMetric("educate_event", "educate", "closed", "help_center", "messenger", var3).addMetaData("conversation_id", var1));
   }

   public void closedInAppFromFull(String var1, String var2) {
      this.store.track(this.newMetric("mv3_metric", "messenger", "closed", "message", "in_app", "from_full").addMetaData("message_id", var2).addMetaData("conversation_id", var1));
   }

   public void closedMessengerBackButton() {
      this.store.track(this.newMetric("mv3_metric", "messenger", "closed", "messenger", "messenger", "from_android_back_button"));
   }

   public void closedMessengerCloseButton() {
      this.store.track(this.newMetric("mv3_metric", "messenger", "closed", "messenger", "messenger", "from_close_button"));
   }

   public void dismissInAppCommentSnippet(String var1, String var2) {
      this.store.track(this.newMetric("mv3_metric", "messenger", "dismissed", "reply", "in_app", "from_snippet").addMetaData("conversation_id", var1).addMetaData("comment_id", var2));
   }

   public void dismissInAppMessageSnippet(String var1, String var2) {
      this.store.track(this.newMetric("mv3_metric", "messenger", "dismissed", "message", "in_app", "from_snippet").addMetaData("conversation_id", var1).addMetaData("message_id", var2));
   }

   public void dismissedPushNotification(String var1, boolean var2) {
      String var3;
      if(var2) {
         var3 = "message";
      } else {
         var3 = "notification";
      }

      this.store.track(this.newMetric("mv3_metric", "messenger", "dismissed", "message", "in_app", "from_push").addMetaData("conversation_id", var1).addMetaData("push_type", var3));
   }

   public void expandedInput(String var1, String var2) {
      this.store.track(this.newMetric("mv3_metric", "messenger", "opened", "expanded_gif_input", "messenger", "in_conversation").addMetaData("conversation_id", var1));
   }

   public void newConversationFromComposeButton(boolean var1) {
      this.store.track(this.newMetric("mv3_metric", "messenger", "opened", "new_conversation", "messenger", "from_conversation_list").addMetaData("within_office_hours", Boolean.valueOf(var1)));
   }

   public void openConversationFromConversationList(String var1, boolean var2, boolean var3, String var4) {
      MetricsStore var5 = this.store;
      MetricObject var6 = this.newMetric("mv3_metric", "messenger", "opened", "conversation", "messenger", "from_conversation_list").addMetaData("conversation_id", var1).addMetaData("within_office_hours", Boolean.valueOf(var2));
      if(var3) {
         var1 = "active";
      } else {
         var1 = "away";
      }

      var5.track(var6.addMetaData("teammate_status", var1).addMetaData("time_since_last_active", var4));
   }

   public void openConversationsListFromConversation(String var1) {
      this.store.track(this.newMetric("mv3_metric", "messenger", "opened", "conversation_list", "messenger", "from_conversation").addMetaData("conversation_id", var1));
   }

   public void openConversationsListFromNewConversation() {
      this.store.track(this.newMetric("mv3_metric", "messenger", "opened", "conversation_list", "messenger", "from_new_conversation"));
   }

   public void openedConversationFromFull(String var1, String var2) {
      this.store.track(this.newMetric("mv3_metric", "messenger", "opened", "conversation", "messenger", "from_full").addMetaData("comment_id", var2).addMetaData("conversation_id", var1));
   }

   public void openedConversationFromSnippet(String var1, String var2) {
      this.store.track(this.newMetric("mv3_metric", "messenger", "opened", "conversation", "messenger", "from_snippet").addMetaData("comment_id", var2).addMetaData("conversation_id", var1));
   }

   public void openedMessengerConversation(String var1, LauncherOpenBehaviour.LauncherType var2) {
      String var3;
      if(LauncherOpenBehaviour.LauncherType.DEFAULT == var2) {
         var3 = "from_launcher";
      } else {
         var3 = "from_custom_launcher";
      }

      this.store.track(this.newMetric("mv3_metric", "messenger", "opened", "conversation", "messenger", var3).addMetaData("conversation_id", var1));
   }

   public void openedMessengerConversationList(LauncherOpenBehaviour.LauncherType var1) {
      String var2;
      if(LauncherOpenBehaviour.LauncherType.DEFAULT == var1) {
         var2 = "from_launcher";
      } else {
         var2 = "from_custom_launcher";
      }

      this.store.track(this.newMetric("mv3_metric", "messenger", "opened", "conversation_list", "messenger", var2));
   }

   public void openedMessengerNewConversation(LauncherOpenBehaviour.LauncherType var1) {
      String var2;
      if(LauncherOpenBehaviour.LauncherType.DEFAULT == var1) {
         var2 = "from_launcher";
      } else {
         var2 = "from_custom_launcher";
      }

      this.store.track(this.newMetric("mv3_metric", "messenger", "opened", "new_conversation", "messenger", var2));
   }

   public void openedPushOnlyNotification(String var1) {
      this.store.track(this.newMetric("mv3_metric", "messenger", "opened", "message", "in_app", "from_push").addMetaData("conversation_id", var1).addMetaData("push_type", "message"));
   }

   public void profileAutoOpen(String var1, ProfilePresenter.ProfileType var2) {
      String var3;
      if(ProfilePresenter.ProfileType.TEAMMATE == var2) {
         var3 = "teammate_profile";
      } else {
         var3 = "team_profile";
      }

      this.store.track(this.newMetric("mv3_metric", "messenger", "opened", var3, "messenger", "from_auto").addMetaData("conversation_id", var1));
   }

   public void profileClickedOpen(String var1, ProfilePresenter.ProfileType var2) {
      String var3;
      if(ProfilePresenter.ProfileType.TEAMMATE == var2) {
         var3 = "teammate_profile";
      } else {
         var3 = "team_profile";
      }

      this.store.track(this.newMetric("mv3_metric", "messenger", "opened", var3, "messenger", "from_clicking").addMetaData("conversation_id", var1));
   }

   public void profileScrolledOpen(String var1, ProfilePresenter.ProfileType var2) {
      String var3;
      if(ProfilePresenter.ProfileType.TEAMMATE == var2) {
         var3 = "teammate_profile";
      } else {
         var3 = "team_profile";
      }

      this.store.track(this.newMetric("mv3_metric", "messenger", "opened", var3, "messenger", "from_scrolling").addMetaData("conversation_id", var1));
   }

   public void receivedMessageFromFullWhenClosed(boolean var1, boolean var2, String var3, String var4, String var5) {
      this.store.track(this.newMetric("mv3_metric", "messenger", "received", "message", "in_app", "from_full").addMetaData("conversation_id", var3).addMetaData("message_id", var4).addMetaData("message_type", var5).addMetaData("is_attachment", Boolean.valueOf(var1)).addMetaData("has_article_card", Boolean.valueOf(var2)));
   }

   public void receivedMessageFromSnippetWhenClosed(boolean var1, boolean var2, String var3, String var4, String var5) {
      this.store.track(this.newMetric("mv3_metric", "messenger", "received", "message", "in_app", "from_snippet").addMetaData("conversation_id", var3).addMetaData("message_id", var4).addMetaData("message_type", var5).addMetaData("is_attachment", Boolean.valueOf(var1)).addMetaData("has_article_card", Boolean.valueOf(var2)));
   }

   public void receivedNotificationFromBadgeWhenMessengerClosed(String var1) {
      this.store.track(this.newMetric("mv3_metric", "messenger", "received", "badge", "in_app", "from_launcher").addMetaData("badge_value", var1));
   }

   public void receivedOperatorReply(String var1) {
      this.store.track(this.newMetric("educate_event", "educate", "received", "operator_reply", "messenger", "from_conversation").addMetaData("conversation_id", var1));
   }

   public void receivedPushNotification(String var1) {
      this.store.track(this.newMetric("mv3_metric", "messenger", "received", "message", "push", "from_push").addMetaData("conversation_id", var1).addMetaData("push_type", "notification"));
   }

   public void receivedPushOnlyNotification(String var1) {
      this.store.track(this.newMetric("mv3_metric", "messenger", "received", "message", "push", "from_push").addMetaData("conversation_id", var1).addMetaData("push_type", "message"));
   }

   public void receivedReply(boolean var1, boolean var2, String var3, String var4) {
      this.store.track(this.newMetric("mv3_metric", "messenger", "received", "reply", "messenger", "in_conversation").addMetaData("is_attachment", Boolean.valueOf(var1)).addMetaData("has_article_card", Boolean.valueOf(var2)).addMetaData("comment_id", var3).addMetaData("conversation_id", var4));
   }

   public void receivedReplyFromSnippetWhenClosed(boolean var1, boolean var2, String var3, String var4) {
      this.store.track(this.newMetric("mv3_metric", "messenger", "received", "reply", "in_app", "from_snippet").addMetaData("conversation_id", var3).addMetaData("comment_id", var4).addMetaData("is_attachment", Boolean.valueOf(var1)).addMetaData("has_article_card", Boolean.valueOf(var2)));
   }

   public void searchedGifInput(String var1) {
      this.store.track(this.newMetric("mv3_metric", "messenger", "searched", "expanded_gif_input", "messenger", "in_conversation").addMetaData("search_query", var1));
   }

   public void sentInConversation(boolean var1, boolean var2, boolean var3, String var4, String var5, boolean var6, boolean var7, String var8) {
      MetricsStore var9 = this.store;
      MetricObject var10 = this.newMetric("mv3_metric", "messenger", "sent", "reply", "messenger", "in_conversation").addMetaData("is_attachment", Boolean.valueOf(var1)).addMetaData("is_annotated", Boolean.valueOf(var2)).addMetaData("is_gif", Boolean.valueOf(var3)).addMetaData("message_id", var4).addMetaData("conversation_id", var5).addMetaData("within_office_hours", Boolean.valueOf(var6));
      if(var7) {
         var4 = "active";
      } else {
         var4 = "away";
      }

      var9.track(var10.addMetaData("teammate_status", var4).addMetaData("time_since_last_active", var8));
   }

   public void sentInNewConversation(boolean var1, boolean var2, boolean var3, String var4, String var5, boolean var6) {
      this.store.track(this.newMetric("mv3_metric", "messenger", "sent", "message", "messenger", "in_new_conversation").addMetaData("is_attachment", Boolean.valueOf(var1)).addMetaData("is_annotated", Boolean.valueOf(var2)).addMetaData("is_gif", Boolean.valueOf(var3)).addMetaData("message_id", var4).addMetaData("conversation_id", var5).addMetaData("within_office_hours", Boolean.valueOf(var6)));
   }

   public void sentReaction(String var1, String var2, int var3, MetricTracker.ReactionLocation var4) {
      if(MetricTracker.ReactionLocation.CONVERSATION == var4) {
         this.store.track(this.newMetric("mv3_metric", "messenger", "sent", "reaction", "messenger", "in_conversation").addMetaData("message_id", var2).addMetaData("reaction_index", Integer.valueOf(var3)).addMetaData("conversation_id", var1));
      } else if(MetricTracker.ReactionLocation.LINK == var4) {
         this.store.track(this.newMetric("educate_event", "educate", "sent", "reaction", "messenger", "on_article").addMetaData("message_id", var2).addMetaData("conversation_id", var1));
      } else {
         this.store.track(this.newMetric("mv3_metric", "messenger", "sent", "reaction", "in_app", "from_full").addMetaData("message_id", var2).addMetaData("conversation_id", var1));
      }

   }

   public void startConversation(String var1) {
      this.store.track(this.newMetric("mv3_metric", "messenger", "opened", "conversation", "messenger", "from_new_conversation").addMetaData("conversation_id", var1));
   }

   void typeInConversation(String var1) {
      this.store.track(this.newMetric("mv3_metric", "messenger", "typed", "reply", "messenger", "in_conversation").addMetaData("conversation_id", var1));
   }

   void typeInNewConversation() {
      this.store.track(this.newMetric("mv3_metric", "messenger", "typed", "message", "messenger", "in_new_conversation"));
   }

   public void viewedArticle(String var1, String var2) {
      this.store.track(this.newMetric("educate_event", "educate", "viewed", "article", "messenger", "from_conversation").addMetaData("message_id", var2).addMetaData("conversation_id", var1));
   }

   public void viewedHelpCenter(String var1) {
      this.store.track(this.newMetric("educate_event", "educate", "viewed", "help_center", "messenger", "from_conversation").addMetaData("conversation_id", var1));
   }

   public void viewedInAppFromFull(String var1, String var2) {
      this.store.track(this.newMetric("mv3_metric", "messenger", "viewed", "message", "in_app", "from_full").addMetaData("message_id", var2).addMetaData("conversation_id", var1));
   }

   public void viewedInAppFromMessenger(String var1, String var2, String var3) {
      this.store.track(this.newMetric("mv3_metric", "messenger", "viewed", "message", "messenger", "from_full").addMetaData("message_type", var3).addMetaData("message_id", var2).addMetaData("conversation_id", var1));
   }

   public void viewedInAppFromSnippet(String var1, String var2) {
      this.store.track(this.newMetric("mv3_metric", "messenger", "viewed", "message", "in_app", "from_snippet").addMetaData("message_id", var2).addMetaData("conversation_id", var1));
   }

   public void viewedPushNotification(String var1) {
      this.store.track(this.newMetric("mv3_metric", "messenger", "viewed", "message", "in_app", "from_push").addMetaData("conversation_id", var1).addMetaData("push_type", "notification"));
   }

   public void viewedReply(boolean var1, boolean var2, String var3, String var4) {
      this.store.track(this.newMetric("mv3_metric", "messenger", "viewed", "reply", "messenger", "from_snippet").addMetaData("is_attachment", Boolean.valueOf(var1)).addMetaData("has_article_card", Boolean.valueOf(var2)).addMetaData("comment_id", var3).addMetaData("conversation_id", var4));
   }

   public @interface CloseActionContext {
      int BACK_BUTTON = 1;
      int CLOSE_BUTTON = 0;
   }

   public static enum ReactionLocation {
      CONVERSATION,
      IN_APP,
      LINK;
   }
}
