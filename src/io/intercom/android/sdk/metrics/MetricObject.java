package io.intercom.android.sdk.metrics;

import android.text.TextUtils;
import io.intercom.android.sdk.commons.utilities.TimeProvider;
import io.intercom.com.google.gson.e;
import io.intercom.com.google.gson.a.c;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class MetricObject implements MetricInterface {
   static final String KEY_ACTION = "action";
   private static final String KEY_ANDROID_INSTALLER_PACKAGE_NAME = "android_installer_package_name";
   private static final String KEY_ANDROID_IS_DEBUG_BUILD = "android_is_debug_build";
   static final String KEY_CONTEXT = "context";
   static final String KEY_OBJECT = "object";
   static final String KEY_OWNER = "owner";
   static final String KEY_PLACE = "place";
   static final String KEY_USER_ID = "user_id";
   @c(
      a = "created_at"
   )
   private final long createdAt;
   private final String id;
   private final Map metadata = new HashMap();
   private final String name;

   MetricObject(String var1, String var2, String var3, String var4, String var5, String var6, String var7, String var8, TimeProvider var9) {
      this.name = var1;
      this.createdAt = TimeUnit.MILLISECONDS.toSeconds(var9.currentTimeMillis());
      this.id = var3;
      this.metadata.put("action", var5);
      this.metadata.put("object", var6);
      this.metadata.put("place", var7);
      this.metadata.put("context", var8);
      this.metadata.put("owner", var2);
      if(!TextUtils.isEmpty(var4)) {
         this.metadata.put("user_id", var4);
      }

   }

   MetricObject addInstallerPackageName(String var1) {
      String var2 = var1;
      if(TextUtils.isEmpty(var1)) {
         var2 = "unknown";
      }

      this.addMetaData("android_installer_package_name", var2);
      return this;
   }

   MetricObject addIsDebugBuild(boolean var1) {
      this.addMetaData("android_is_debug_build", Boolean.valueOf(var1));
      return this;
   }

   MetricObject addMetaData(String var1, Object var2) {
      this.metadata.put(var1, var2);
      return this;
   }

   public boolean equals(Object var1) {
      boolean var4 = true;
      boolean var3 = false;
      boolean var2;
      if(this == var1) {
         var2 = true;
      } else {
         var2 = var3;
         if(var1 != null) {
            var2 = var3;
            if(this.getClass() == var1.getClass()) {
               MetricObject var5 = (MetricObject)var1;
               var2 = var3;
               if(this.createdAt == var5.createdAt) {
                  if(this.metadata != null) {
                     var2 = var3;
                     if(!this.metadata.equals(var5.metadata)) {
                        return var2;
                     }
                  } else if(var5.metadata != null) {
                     var2 = var3;
                     return var2;
                  }

                  if(this.id != null) {
                     var2 = this.id.equals(var5.id);
                  } else {
                     var2 = var4;
                     if(var5.id != null) {
                        var2 = false;
                     }
                  }
               }
            }
         }
      }

      return var2;
   }

   public long getCreatedAt() {
      return this.createdAt;
   }

   public Map getMetadata() {
      return this.metadata;
   }

   public int hashCode() {
      int var2 = 0;
      int var1;
      if(this.metadata != null) {
         var1 = this.metadata.hashCode();
      } else {
         var1 = 0;
      }

      if(this.id != null) {
         var2 = this.id.hashCode();
      }

      return (var1 * 31 + var2) * 31 + (int)(this.createdAt ^ this.createdAt >>> 32);
   }

   public String toJson(e var1) {
      return var1.a((Object)this);
   }

   public String toString() {
      return "MetricObject{metadata=" + this.metadata + ", id='" + this.id + '\'' + ", createdAt=" + this.createdAt + '}';
   }
}
