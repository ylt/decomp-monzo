package io.intercom.android.sdk.overlay;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import io.intercom.android.sdk.Provider;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.android.sdk.models.Conversation;
import io.intercom.android.sdk.models.Part;
import io.intercom.android.sdk.utilities.AvatarUtils;
import io.intercom.com.bumptech.glide.i;

class ChatSnippet extends ChatNotification {
   private TextView contentBody;
   private final i requestManager;

   public ChatSnippet(Context var1, Conversation var2, int var3, int var4, InAppNotification.Listener var5, Provider var6, i var7) {
      super(var1, var2, var3, var4, var5, var6, var7);
      this.requestManager = var7;
   }

   private void setBodyText(Part var1) {
      if(var1.getSummary().isEmpty()) {
         this.contentBody.setText(this.localisedContext.getString(R.string.intercom_image_attached));
      } else {
         this.contentBody.setText(var1.getSummary());
      }

   }

   protected View getContentContainer() {
      return this.overlayRoot.findViewById(R.id.chathead_text_body);
   }

   protected ViewGroup inflateChatRootView(ViewGroup var1, LayoutInflater var2) {
      var1 = (ViewGroup)var2.inflate(R.layout.intercom_preview_chat_snippet_overlay, var1, false);
      this.contentBody = (TextView)var2.inflate(R.layout.intercom_preview_chat_snippet_body, var1, false);
      ((ViewGroup)var1.findViewById(R.id.chathead_text_container)).addView(this.contentBody);
      return var1;
   }

   public void update(Conversation var1, Runnable var2) {
      this.conversation = var1;
      View var4 = this.overlayRoot.findViewById(R.id.chathead_text_body);
      View var3 = this.overlayRoot.findViewById(R.id.chathead_text_container);
      var3.setPivotX(0.0F);
      this.performReplyPulse(var3, var4, var2);
   }

   protected void updateContentContainer(Part var1) {
      this.setBodyText(var1);
   }

   protected void updateViewDataDuringReplyPulse(int var1) {
      ImageView var2 = (ImageView)this.overlayRoot.findViewById(R.id.chathead_avatar);
      AvatarUtils.loadAvatarIntoView(this.conversation.getLastAdmin().getAvatar(), var2, (AppConfig)this.appConfigProvider.get(), this.requestManager);
      ((TextView)this.overlayRoot.findViewById(R.id.chathead_text_header)).setText(this.getHeaderText());
      this.setBodyText(this.conversation.getLastPart());
   }
}
