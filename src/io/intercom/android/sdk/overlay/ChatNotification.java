package io.intercom.android.sdk.overlay;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.animation.Animator.AnimatorListener;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.FrameLayout.LayoutParams;
import io.intercom.android.sdk.Provider;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.commons.utilities.ScreenUtils;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.android.sdk.models.Conversation;
import io.intercom.android.sdk.models.Part;
import io.intercom.android.sdk.utilities.AvatarUtils;
import io.intercom.android.sdk.utilities.FontUtils;
import io.intercom.com.bumptech.glide.i;

abstract class ChatNotification extends InAppNotification {
   private static final int ANIMATION_DURATION = 170;
   private static final int GROWTH_WIDTH = 355;
   private final i requestManager;

   protected ChatNotification(Context var1, Conversation var2, int var3, int var4, InAppNotification.Listener var5, Provider var6, i var7) {
      super(var1, var2, var3, var4, var5, var6);
      this.requestManager = var7;
   }

   private void expandChat(ViewGroup var1, AnimatorListenerAdapter var2) {
      ViewGroup var5 = (ViewGroup)var1.findViewById(R.id.chathead_text_container);
      if(var5 != null) {
         TextView var8 = (TextView)var1.findViewById(R.id.chathead_text_header);
         View var6 = this.getContentContainer();
         LayoutParams var7 = (LayoutParams)var5.getLayoutParams();
         View var9 = var1.findViewById(R.id.chat_avatar_container);
         float var3 = var9.getTranslationX();
         float var4 = var9.getTranslationY();
         ObjectAnimator.ofPropertyValuesHolder(var9, new PropertyValuesHolder[]{PropertyValuesHolder.ofFloat(View.ALPHA, new float[]{1.0F, 0.8F}), PropertyValuesHolder.ofFloat("translationX", new float[]{var3, var3 + 100.0F}), PropertyValuesHolder.ofFloat("translationY", new float[]{var4, var4 - 40.0F}), PropertyValuesHolder.ofFloat(View.SCALE_X, new float[]{1.0F, 0.8F}), PropertyValuesHolder.ofFloat(View.SCALE_Y, new float[]{1.0F, 0.8F})}).setDuration(170L).start();
         ObjectAnimator.ofFloat(var8, View.ALPHA, new float[]{1.0F, 0.0F}).setDuration(170L).start();
         ObjectAnimator.ofFloat(var6, View.ALPHA, new float[]{1.0F, 0.0F}).setDuration(170L).start();
         this.getWidthAnimator(var5, var2).start();
         this.getWidthAnimator(this.overlayRoot).start();
         this.getMarginAnimator(var5, var7).start();
      }

   }

   private Animator getMarginAnimator(final ViewGroup var1, LayoutParams var2) {
      ValueAnimator var3 = ValueAnimator.ofInt(new int[]{var2.leftMargin, 0});
      var3.addUpdateListener(new AnimatorUpdateListener() {
         public void onAnimationUpdate(ValueAnimator var1x) {
            int var2 = ((Integer)var1x.getAnimatedValue()).intValue();
            LayoutParams var3 = (LayoutParams)var1.getLayoutParams();
            var3.leftMargin = var2;
            var1.setLayoutParams(var3);
         }
      });
      var3.setDuration(170L);
      return var3;
   }

   private Animator getWidthAnimator(ViewGroup var1, final AnimatorListener var2) {
      final int var3 = Math.abs(var1.getMeasuredWidth() - ScreenUtils.dpToPx(355.0F, var1.getContext()));
      ValueAnimator var4 = this.getWidthAnimator(var1);
      var4.addUpdateListener(new AnimatorUpdateListener() {
         boolean hasSentUpdate = false;

         public void onAnimationUpdate(ValueAnimator var1) {
            int var2x = ((Integer)var1.getAnimatedValue()).intValue();
            if(var3 > 0) {
               var2x = var2x / var3 * 100;
            } else {
               var2x = 100;
            }

            if(var2x > 80 && !this.hasSentUpdate) {
               this.hasSentUpdate = true;
               var2.onAnimationEnd((Animator)null);
            }

         }
      });
      return var4;
   }

   private ValueAnimator getWidthAnimator(final ViewGroup var1) {
      ValueAnimator var2 = ValueAnimator.ofInt(new int[]{var1.getMeasuredWidth(), ScreenUtils.dpToPx(355.0F, var1.getContext())});
      var2.addUpdateListener(new AnimatorUpdateListener() {
         public void onAnimationUpdate(ValueAnimator var1x) {
            int var2 = ((Integer)var1x.getAnimatedValue()).intValue();
            LayoutParams var3 = (LayoutParams)var1.getLayoutParams();
            var3.width = var2;
            var1.setLayoutParams(var3);
         }
      });
      var2.setDuration(170L);
      return var2;
   }

   private void performEntranceAnimation() {
      View var2 = this.overlayRoot.findViewById(R.id.chat_avatar_container);
      final ViewGroup var1 = (ViewGroup)this.overlayRoot.findViewById(R.id.chat_full_body);
      var1.setVisibility(4);
      ObjectAnimator var3 = ObjectAnimator.ofPropertyValuesHolder(var2, new PropertyValuesHolder[]{PropertyValuesHolder.ofFloat(View.ALPHA, new float[]{0.0F, 1.0F})}).setDuration(400L);
      var3.addListener(new AnimatorListenerAdapter() {
         public void onAnimationEnd(Animator var1x) {
            ChatNotification.this.animateTextContainer(var1);
         }
      });
      var3.start();
   }

   private void populateViewsWithData(i var1) {
      View var3 = this.overlayRoot.findViewById(R.id.chathead_root);
      ImageView var2 = (ImageView)var3.findViewById(R.id.chathead_avatar);
      TextView var4 = (TextView)var3.findViewById(R.id.chathead_text_header);
      var4.setTextColor(((AppConfig)this.appConfigProvider.get()).getBaseColor());
      FontUtils.setRobotoMediumTypeface(var4);
      AvatarUtils.loadAvatarIntoView(this.conversation.getLastAdmin().getAvatar(), var2, (AppConfig)this.appConfigProvider.get(), var1);
      var4.setText(this.getHeaderText());
      this.updateContentContainer(this.conversation.getLastPart());
   }

   void animateTextContainer(ViewGroup var1) {
      int var3 = (int)var1.getX();
      int var2 = (int)var1.getX();
      var1.setScaleX(0.8F);
      var1.setScaleY(0.8F);
      var1.setAlpha(0.8F);
      var1.setX((float)(var3 - 150));
      var1.setVisibility(0);
      var1.animate().setInterpolator(new OvershootInterpolator(0.6F)).scaleX(1.0F).scaleY(1.0F).alphaBy(1.0F).translationX((float)var2).setDuration(300L).setListener(new AnimatorListenerAdapter() {
         public void onAnimationEnd(Animator var1) {
            ChatNotification.this.beginListeningForTouchEvents();
         }
      }).start();
   }

   public void display(ViewGroup var1, LayoutInflater var2, boolean var3, int var4) {
      if(this.overlayRoot == null) {
         this.overlayRoot = this.inflateChatRootView(var1, var2);
      }

      if(!this.isAttached()) {
         var1.addView(this.overlayRoot, 0);
      }

      LayoutParams var5 = (LayoutParams)this.overlayRoot.getLayoutParams();
      var5.setMargins(var5.leftMargin, var5.topMargin, var5.rightMargin, var5.bottomMargin + this.overlayRoot.getResources().getDimensionPixelSize(R.dimen.intercom_bottom_padding) + var4);
      this.overlayRoot.setLayoutParams(var5);
      this.populateViewsWithData(this.requestManager);
      if(var3) {
         this.performEntranceAnimation();
      } else {
         this.overlayRoot.setVisibility(0);
         this.beginListeningForTouchEvents();
      }

   }

   protected abstract View getContentContainer();

   protected abstract ViewGroup inflateChatRootView(ViewGroup var1, LayoutInflater var2);

   public void moveBackward(ViewGroup var1, AnimatorListenerAdapter var2) {
      ++this.position;
      this.animateToPosition(var1.getContext());
      this.expandChat(var1, var2);
   }

   protected void onNotificationPressed(View var1) {
      var1.animate().scaleX(0.9F).scaleY(0.9F).alpha(0.9F).setDuration(50L).start();
   }

   protected void onNotificationReleased(View var1) {
      var1.animate().scaleX(1.0F).scaleY(1.0F).alpha(1.0F).setDuration(50L).start();
   }

   protected abstract void updateContentContainer(Part var1);

   protected abstract void updateViewDataDuringReplyPulse(int var1);
}
