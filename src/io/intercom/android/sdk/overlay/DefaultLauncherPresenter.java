package io.intercom.android.sdk.overlay;

import android.animation.Animator.AnimatorListener;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.metrics.MetricTracker;

class DefaultLauncherPresenter implements DefaultLauncher.Listener {
   private int bottomPadding = 0;
   DefaultLauncher defaultLauncher;
   private final LayoutInflater inflater;
   private final MetricTracker metricTracker;
   private final LauncherOpenBehaviour openBehaviour;
   private int previousUnreadCount = 0;

   DefaultLauncherPresenter(LayoutInflater var1, LauncherOpenBehaviour var2, MetricTracker var3) {
      this.inflater = var1;
      this.openBehaviour = var2;
      this.metricTracker = var3;
      this.bottomPadding = this.getDefaultPadding(var1.getContext().getResources());
   }

   private int getDefaultPadding(Resources var1) {
      return var1.getDimensionPixelSize(R.dimen.intercom_launcher_padding_bottom) + var1.getDimensionPixelSize(R.dimen.intercom_bottom_padding);
   }

   void displayLauncherOnAttachedRoot(ViewGroup var1, int var2) {
      if(this.defaultLauncher != null && !this.defaultLauncher.isAttachedToRoot(var1)) {
         this.defaultLauncher.removeView();
         this.defaultLauncher = null;
      }

      if(this.defaultLauncher == null) {
         this.defaultLauncher = new DefaultLauncher(var1, this.inflater, this, this.bottomPadding);
         this.defaultLauncher.fadeOnScreen();
      }

      this.setLauncherBackgroundColor(var2);
      this.setUnreadCount(this.previousUnreadCount);
   }

   DefaultLauncher getAndUnsetLauncher() {
      DefaultLauncher var1 = this.defaultLauncher;
      this.defaultLauncher = null;
      return var1;
   }

   public boolean isDisplaying() {
      boolean var1;
      if(this.defaultLauncher != null) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public void onLauncherClicked(Context var1) {
      this.openBehaviour.openMessenger(var1);
   }

   void removeLauncher() {
      if(this.defaultLauncher != null) {
         this.defaultLauncher.fadeOffScreen((AnimatorListener)null);
         this.defaultLauncher = null;
      }

   }

   public void setBottomPadding(int var1) {
      this.bottomPadding = this.getDefaultPadding(this.inflater.getContext().getResources()) + var1;
      if(this.isDisplaying()) {
         this.defaultLauncher.updateBottomPadding(this.bottomPadding);
      }

   }

   void setLauncherBackgroundColor(int var1) {
      if(this.defaultLauncher != null) {
         this.defaultLauncher.setBackgroundColor(var1);
      }

   }

   public void setUnreadCount(int var1) {
      if(this.isDisplaying()) {
         String var2 = String.valueOf(var1);
         if(var1 > this.previousUnreadCount) {
            this.metricTracker.receivedNotificationFromBadgeWhenMessengerClosed(var2);
         }

         if(var1 > 0) {
            this.defaultLauncher.setBadgeCount(var2);
         } else {
            this.defaultLauncher.hideBadgeCount();
         }
      }

      this.previousUnreadCount = var1;
   }
}
