package io.intercom.android.sdk.overlay;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.content.Context;
import android.content.res.Resources;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.animation.OvershootInterpolator;
import android.widget.TextView;
import io.intercom.android.sdk.Provider;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.activities.IntercomMessengerActivity;
import io.intercom.android.sdk.commons.utilities.ScreenUtils;
import io.intercom.android.sdk.logger.LumberMill;
import io.intercom.android.sdk.metrics.MetricTracker;
import io.intercom.android.sdk.models.Conversation;
import io.intercom.android.sdk.models.Part;
import io.intercom.android.sdk.twig.Twig;
import io.intercom.android.sdk.utilities.ContextLocaliser;
import io.intercom.android.sdk.utilities.Phrase;
import io.intercom.android.sdk.utilities.SystemSettings;
import io.intercom.android.sdk.utilities.ViewUtils;
import io.intercom.com.bumptech.glide.i;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

class InAppNotificationManager implements OnClickListener, InAppNotification.Listener {
   private static final int MAX_DISPLAYED_NOTIFICATIONS = 3;
   private static final int PILL_START_OFFSET_DP = 66;
   private final Provider appConfigProvider;
   private int bottomPadding = 0;
   private final ContextLocaliser contextLocaliser;
   final Handler handler;
   private boolean hasAnimated = false;
   private final LayoutInflater inflater;
   private final List lastDisplayedConversations = new ArrayList();
   private final InAppNotificationManager.Listener listener;
   private final MetricTracker metricTracker;
   private final Map notifications = new HashMap();
   private float pillPosition;
   private final i requestManager;
   private final Twig twig = LumberMill.getLogger();

   public InAppNotificationManager(LayoutInflater var1, Handler var2, InAppNotificationManager.Listener var3, MetricTracker var4, Provider var5, ContextLocaliser var6, i var7) {
      this.inflater = var1;
      this.listener = var3;
      this.handler = var2;
      this.metricTracker = var4;
      this.appConfigProvider = var5;
      this.contextLocaliser = var6;
      this.requestManager = var7;
   }

   private void addNewNotifications(List var1, ViewGroup var2) {
      int var4 = var1.size();

      for(int var3 = 0; var3 < var4; ++var3) {
         Conversation var7 = (Conversation)var1.get(var3);
         Context var8 = this.contextLocaliser.createLocalisedContext(var2.getContext());
         Part var6 = var7.getLastPart();
         Object var9;
         if(var4 < 2 && "chat".equals(var6.getMessageStyle())) {
            if(var6.getDeliveryOption() == Part.DeliveryOption.SUMMARY) {
               var9 = new ChatSnippet(var8, var7, var3, var2.getHeight(), this, this.appConfigProvider, this.requestManager);
            } else {
               var9 = new ChatFull(var8, var7, var3, var2.getHeight(), this, this.metricTracker, this.appConfigProvider, this.requestManager);
            }
         } else {
            var9 = new StackableSnippet(var8, var7, var3, this.handler, var2.getHeight(), this, this.appConfigProvider, this.requestManager);
         }

         this.notifications.put(var7.getId(), var9);
         if(var3 < 3) {
            LayoutInflater var10 = this.inflater;
            boolean var5;
            if(!this.hasAnimated) {
               var5 = true;
            } else {
               var5 = false;
            }

            ((InAppNotification)var9).display(var2, var10, var5, this.bottomPadding);
         }
      }

   }

   private void animatePill(final View var1) {
      this.handler.postDelayed(new Runnable() {
         public void run() {
            var1.setY(InAppNotificationManager.this.pillPosition + (float)ScreenUtils.dpToPx(66.0F, var1.getContext()));
            var1.animate().setInterpolator(new OvershootInterpolator()).y(InAppNotificationManager.this.pillPosition).alpha(1.0F).scaleX(1.0F).scaleY(1.0F).start();
         }
      }, (long)(500.0F * SystemSettings.getTransitionScale(var1.getContext())));
   }

   private void displayPill(LayoutInflater var1, ViewGroup var2) {
      TextView var4 = (TextView)var2.findViewById(R.id.notification_pill);
      if(this.notifications.size() <= 3) {
         if(var4 != null) {
            var2.removeView(var4);
         }
      } else {
         TextView var3 = var4;
         if(var4 == null) {
            var1.inflate(R.layout.intercom_notification_pill, var2, true);
            var3 = (TextView)var2.findViewById(R.id.notification_pill);
            var3.setAlpha(0.0F);
            var3.setScaleX(0.4F);
            var3.setScaleY(0.4F);
            var3.setY(this.pillPosition);
            var3.setOnClickListener(this);
            this.animatePill(var3);
         }

         Phrase.from(this.contextLocaliser.createLocalisedContext(var3.getContext()), R.string.intercom_plus_x_more).put("n", this.notifications.size() - 3).into(var3);
      }

   }

   private boolean isReply(Conversation var1) {
      boolean var2;
      if(!this.lastDisplayedConversations.isEmpty() && var1.getId().equals(((Conversation)this.lastDisplayedConversations.get(0)).getId())) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   private void updateNotifications(final List var1, final Map var2, final ViewGroup var3) {
      final Conversation var6 = (Conversation)var1.get(0);
      Iterator var7 = var2.values().iterator();
      InAppNotification var4 = (InAppNotification)var7.next();

      while(var7.hasNext()) {
         InAppNotification var5 = (InAppNotification)var7.next();
         if(var5.getPosition() < var4.getPosition()) {
            var4 = var5;
         }
      }

      if(this.isReply(var6)) {
         var4.update(var6, new Runnable() {
            public void run() {
               InAppNotificationManager.this.syncUpdates(var1, var2, var3);
            }
         });
      } else if(var2.size() == 1 && var4 instanceof ChatSnippet) {
         var4.moveBackward(var3, new AnimatorListenerAdapter() {
            public void onAnimationEnd(Animator var1x) {
               InAppNotificationManager.this.addNewView(var6, var3, var2);
               InAppNotificationManager.this.handler.postDelayed(new Runnable() {
                  public void run() {
                     InAppNotificationManager.this.syncUpdates(var1, var2, var3);
                  }
               }, 220L);
            }
         });
      } else {
         Iterator var8 = var2.values().iterator();

         while(var8.hasNext()) {
            var4 = (InAppNotification)var8.next();
            if(var4.getPosition() < 3) {
               var4.moveBackward(var3);
            } else {
               var4.setPosition(var4.getPosition() + 1);
            }
         }

         this.addNewView(var6, var3, var2);
         this.syncUpdates(var1, var2, var3);
      }

   }

   private void updatePillPosition(View var1) {
      ObjectAnimator.ofPropertyValuesHolder(var1, new PropertyValuesHolder[]{PropertyValuesHolder.ofFloat("y", new float[]{var1.getY(), this.pillPosition})}).setDuration(300L).start();
   }

   void addNewView(Conversation var1, ViewGroup var2, Map var3) {
      StackableSnippet var4 = new StackableSnippet(this.contextLocaliser.createLocalisedContext(var2.getContext()), var1, 0, this.handler, var2.getHeight(), this, this.appConfigProvider, this.requestManager);
      var4.display(var2, this.inflater, true, this.bottomPadding);
      if(var3.containsKey(var1.getId())) {
         ((InAppNotification)var3.get(var1.getId())).removeView();
      }

      var3.put(var1.getId(), var4);
   }

   public void displayNotifications(final ViewGroup var1, final List var2) {
      ViewUtils.waitForViewAttachment(var1, new Runnable() {
         public void run() {
            InAppNotificationManager.this.displayNotificationsAfterAttach(var2, var1);
         }
      });
   }

   void displayNotificationsAfterAttach(List var1, ViewGroup var2) {
      boolean var3;
      if(!this.lastDisplayedConversations.equals(var1) && !this.notifications.isEmpty()) {
         var3 = true;
      } else {
         var3 = false;
      }

      HashMap var4 = new HashMap(this.notifications);
      if(var3) {
         this.updateNotifications(var1, var4, var2);
      } else if(!this.isDisplaying()) {
         this.addNewNotifications(var1, var2);
      }

      this.hasAnimated = true;
      Resources var5 = var2.getResources();
      this.pillPosition = (float)(var2.getHeight() - var5.getDimensionPixelSize(R.dimen.intercom_overlay_pill_bottom_margin) - var5.getDimensionPixelSize(R.dimen.intercom_bottom_padding) - this.bottomPadding);
      this.displayPill(this.inflater, var2);
      this.lastDisplayedConversations.clear();
      this.lastDisplayedConversations.addAll(var1);
   }

   public boolean isDisplaying() {
      boolean var1;
      if(!this.notifications.isEmpty()) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public void onClick(View var1) {
      var1.getContext().startActivity(IntercomMessengerActivity.openInbox(var1.getContext()));
   }

   public void onInAppNotificationDismiss(InAppNotification var1) {
      ViewGroup var2;
      try {
         var2 = this.listener.getRootView();
      } catch (Exception var4) {
         this.twig.internal("Couldn't get root view: " + var4.getMessage());
         var2 = null;
      }

      Conversation var3 = var1.getConversation();
      if(var2 != null) {
         this.listener.markAsDismissed(var3);
         var2.removeView(var1.getRootView());
         this.notifications.remove(var3.getId());
         this.lastDisplayedConversations.remove(var3);
         Iterator var5 = this.notifications.values().iterator();

         while(var5.hasNext()) {
            ((InAppNotification)var5.next()).moveForward(var2, this.inflater);
         }

         this.displayPill(this.inflater, var2);
         if(this.notifications.isEmpty()) {
            this.hasAnimated = false;
         }
      }

      Part var6 = var3.getLastAdminPart();
      if("chat".equals(var6.getMessageStyle()) && Part.DeliveryOption.FULL == var6.getDeliveryOption()) {
         this.metricTracker.closedInAppFromFull(var3.getId(), var6.getId());
      } else if(var6.isInitialMessage()) {
         this.metricTracker.dismissInAppMessageSnippet(var3.getId(), var6.getId());
      } else {
         this.metricTracker.dismissInAppCommentSnippet(var3.getId(), var6.getId());
      }

   }

   public void onInAppNotificationTap(Conversation var1) {
      this.listener.openNotification(var1);
   }

   public void reset(ViewGroup var1) {
      Iterator var2 = this.notifications.values().iterator();

      while(var2.hasNext()) {
         ((InAppNotification)var2.next()).removeView();
      }

      View var3 = var1.findViewById(R.id.notification_pill);
      if(var3 != null) {
         var1.removeView(var3);
      }

      this.notifications.clear();
   }

   public void setBottomPadding(int var1) {
      this.bottomPadding = var1;
      if(this.isDisplaying()) {
         Iterator var2 = this.notifications.values().iterator();

         while(var2.hasNext()) {
            ((InAppNotification)var2.next()).updateBottomPadding(this.inflater.getContext(), this.bottomPadding);
         }

         ViewGroup var5;
         try {
            var5 = this.listener.getRootView();
         } catch (Exception var4) {
            this.twig.internal("Couldn't get root view: " + var4.getMessage());
            var5 = null;
         }

         if(var5 != null) {
            Resources var3 = var5.getResources();
            this.pillPosition = (float)(var5.getHeight() - var3.getDimensionPixelSize(R.dimen.intercom_overlay_pill_bottom_margin) - var3.getDimensionPixelSize(R.dimen.intercom_bottom_padding) - var1);
            View var6 = var5.findViewById(R.id.notification_pill);
            if(var6 != null) {
               this.updatePillPosition(var6);
            }
         }
      }

   }

   void syncUpdates(List var1, Map var2, ViewGroup var3) {
      for(int var4 = 0; var4 < var1.size(); ++var4) {
         Conversation var6 = (Conversation)var1.get(var4);
         Object var5 = (InAppNotification)var2.remove(var6.getId());
         if(var5 instanceof ChatSnippet && var4 > 0 || var5 == null) {
            if(var5 != null) {
               ((InAppNotification)var5).removeView();
            }

            var5 = new StackableSnippet(this.contextLocaliser.createLocalisedContext(var3.getContext()), var6, var4, this.handler, var3.getHeight(), this, this.appConfigProvider, this.requestManager);
         }

         if(var4 < 3) {
            ((InAppNotification)var5).display(var3, var6, this.inflater, false, this.bottomPadding);
         } else {
            ((InAppNotification)var5).removeView();
         }

         this.notifications.put(var6.getId(), var5);
      }

      Iterator var7 = var2.values().iterator();

      while(var7.hasNext()) {
         ((InAppNotification)var7.next()).removeView();
      }

   }

   interface Listener {
      ViewGroup getRootView() throws Exception;

      void markAsDismissed(Conversation var1);

      void openNotification(Conversation var1);
   }
}
