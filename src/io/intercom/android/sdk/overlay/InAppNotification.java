package io.intercom.android.sdk.overlay;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.View.OnTouchListener;
import android.view.animation.OvershootInterpolator;
import android.widget.FrameLayout.LayoutParams;
import io.intercom.android.sdk.Provider;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.commons.utilities.ScreenUtils;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.android.sdk.models.Conversation;
import io.intercom.android.sdk.utilities.Phrase;

abstract class InAppNotification implements OnTouchListener {
   private static final int ANIMATE_OFFSCREEN_OFFSET = 200;
   private static final int BASE_MARGIN_DP = 16;
   private static final int DISMISS_DISTANCE_DP = 80;
   protected static final int MARGIN_OFFSET_DP = 8;
   private static final float RESISTANCE = 2.0F;
   protected static final float SCALE_OFFSET = 0.05F;
   protected final Provider appConfigProvider;
   private int bottomPadding = 0;
   protected Conversation conversation;
   private float initialTouchX;
   private float initialViewX;
   final InAppNotification.Listener listener;
   protected final Context localisedContext;
   protected ViewGroup overlayRoot;
   protected int position;
   protected final int screenHeight;

   protected InAppNotification(Context var1, Conversation var2, int var3, int var4, InAppNotification.Listener var5, Provider var6) {
      this.conversation = var2;
      this.listener = var5;
      this.position = var3;
      this.screenHeight = var4;
      this.localisedContext = var1;
      this.appConfigProvider = var6;
   }

   private void animateOffScreen(View var1) {
      int var3 = ScreenUtils.dpToPx(200.0F, var1.getContext());
      float var2;
      if(var1.getX() > this.initialViewX) {
         var2 = (float)(var3 + this.getParentOrScreenWidth(var1));
      } else {
         var2 = (float)(var1.getWidth() * -1 - var3);
      }

      var1.animate().setInterpolator(new OvershootInterpolator(0.6F)).translationX(var2).setDuration(300L).setListener(new AnimatorListenerAdapter() {
         public void onAnimationEnd(Animator var1) {
            InAppNotification.this.listener.onInAppNotificationDismiss(InAppNotification.this);
         }
      }).start();
   }

   private void animateToOriginalPosition(View var1) {
      var1.animate().setInterpolator(new OvershootInterpolator(0.6F)).translationX(0.0F).setDuration(300L).start();
   }

   private int getParentOrScreenWidth(View var1) {
      ViewParent var3 = var1.getParent();
      int var2;
      if(var3 instanceof View) {
         var2 = ((View)var3).getWidth();
      } else {
         var2 = var1.getResources().getDisplayMetrics().widthPixels;
      }

      return var2;
   }

   protected void animateToBackOfStack(Context var1) {
      Resources var4 = var1.getResources();
      int var3 = (int)var4.getDimension(R.dimen.intercom_notification_preview_height);
      int var2 = ScreenUtils.dpToPx((float)(this.position * 8 + 16), var1);
      var3 = this.screenHeight - (var4.getDimensionPixelSize(R.dimen.intercom_bottom_padding) + var3 + var2 + this.bottomPadding);
      var2 = ScreenUtils.dpToPx(8.0F, var1);
      PropertyValuesHolder var5 = PropertyValuesHolder.ofFloat(View.Y, new float[]{(float)(var2 + var3), (float)var3});
      PropertyValuesHolder var6 = PropertyValuesHolder.ofFloat(View.ALPHA, new float[]{this.overlayRoot.getAlpha(), 1.0F});
      PropertyValuesHolder var7 = PropertyValuesHolder.ofFloat(View.SCALE_X, new float[]{0.85F, 1.0F - (float)this.position * 0.05F});
      ObjectAnimator.ofPropertyValuesHolder(this.overlayRoot, new PropertyValuesHolder[]{var5, var6, var7}).setDuration(200L).start();
   }

   protected void animateToPosition(Context var1) {
      Resources var7 = var1.getResources();
      int var2 = (int)var7.getDimension(R.dimen.intercom_notification_preview_height);
      int var6 = ScreenUtils.dpToPx((float)(this.position * 8 + 16), var1);
      int var3 = this.screenHeight;
      int var4 = var7.getDimensionPixelSize(R.dimen.intercom_bottom_padding);
      int var5 = this.bottomPadding;
      PropertyValuesHolder var8 = PropertyValuesHolder.ofFloat("y", new float[]{this.overlayRoot.getY(), (float)(var3 - (var4 + var2 + var6 + var5))});
      PropertyValuesHolder var9 = PropertyValuesHolder.ofFloat("scaleX", new float[]{this.overlayRoot.getScaleX(), 1.0F - (float)this.position * 0.05F});
      ObjectAnimator.ofPropertyValuesHolder(this.overlayRoot, new PropertyValuesHolder[]{var8, var9}).setDuration(200L).start();
   }

   protected void beginListeningForTouchEvents() {
      this.overlayRoot.setOnTouchListener(this);
   }

   public abstract void display(ViewGroup var1, LayoutInflater var2, boolean var3, int var4);

   public void display(ViewGroup var1, Conversation var2, LayoutInflater var3, boolean var4, int var5) {
      this.conversation = var2;
      this.display(var1, var3, var4, var5);
   }

   public Conversation getConversation() {
      return this.conversation;
   }

   protected CharSequence getHeaderText() {
      CharSequence var1;
      if(this.conversation.isAdminReply()) {
         var1 = Phrase.from(this.localisedContext, R.string.intercom_reply_from_admin).put("name", this.conversation.getLastAdmin().getForename()).format();
      } else {
         var1 = Phrase.from(this.localisedContext, R.string.intercom_teammate_from_company).put("name", this.conversation.getLastAdmin().getForename()).put("company", ((AppConfig)this.appConfigProvider.get()).getName()).format();
      }

      return var1;
   }

   public int getPosition() {
      return this.position;
   }

   protected View getRootView() {
      return this.overlayRoot;
   }

   public boolean isAttached() {
      boolean var1;
      if(this.overlayRoot.getParent() != null) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public void moveBackward(ViewGroup var1) {
      this.moveBackward(var1, new AnimatorListenerAdapter() {
      });
   }

   public abstract void moveBackward(ViewGroup var1, AnimatorListenerAdapter var2);

   public void moveForward(ViewGroup var1, LayoutInflater var2) {
      --this.position;
      if(this.position < 0) {
         if(this.overlayRoot != null && this.overlayRoot.getParent() instanceof ViewGroup) {
            ((ViewGroup)this.overlayRoot.getParent()).removeView(this.overlayRoot);
         }
      } else if(this.position == 0) {
         this.animateToPosition(var1.getContext());
         this.beginListeningForTouchEvents();
      } else if(this.position == 1) {
         this.animateToPosition(var1.getContext());
      } else if(this.position == 2) {
         this.display(var1, var2, false, this.bottomPadding);
         this.animateToBackOfStack(var1.getContext());
      }

   }

   protected abstract void onNotificationPressed(View var1);

   protected abstract void onNotificationReleased(View var1);

   public boolean onTouch(View var1, MotionEvent var2) {
      float var3 = (var2.getRawX() - this.initialTouchX) / 2.0F;
      switch(var2.getAction()) {
      case 0:
         this.initialViewX = var1.getX();
         this.initialTouchX = var2.getRawX();
         this.onNotificationPressed(this.getRootView());
         break;
      case 1:
         var3 = Math.abs(var3);
         if(var3 < 5.0F) {
            this.listener.onInAppNotificationTap(this.conversation);
         } else if(var3 > (float)ScreenUtils.dpToPx(80.0F, this.localisedContext)) {
            this.stopListeningForTouchEvents();
            this.animateOffScreen(var1);
         } else {
            this.onNotificationReleased(this.getRootView());
            this.animateToOriginalPosition(var1);
         }
         break;
      case 2:
         var1.setX(var3 + this.initialViewX);
      }

      return true;
   }

   protected void performReplyPulse(View var1, final View var2, final Runnable var3) {
      ObjectAnimator var4 = ObjectAnimator.ofPropertyValuesHolder(var1, new PropertyValuesHolder[]{PropertyValuesHolder.ofFloat(View.SCALE_X, new float[]{1.0F, 1.05F}), PropertyValuesHolder.ofFloat(View.SCALE_Y, new float[]{1.0F, 1.05F})}).setDuration(150L);
      var4.setRepeatCount(1);
      var4.setRepeatMode(2);
      var4.start();
      var4 = ObjectAnimator.ofFloat(var2, View.ALPHA, new float[]{1.0F, 0.0F}).setDuration(200L);
      var4.addListener(new AnimatorListenerAdapter() {
         public void onAnimationEnd(Animator var1) {
            InAppNotification.this.updateViewDataDuringReplyPulse(InAppNotification.this.bottomPadding);
            ObjectAnimator var2x = ObjectAnimator.ofFloat(var2, View.ALPHA, new float[]{0.0F, 1.0F}).setDuration(200L);
            var2x.setStartDelay(100L);
            var2x.start();
            var2x.addListener(new AnimatorListenerAdapter() {
               public void onAnimationEnd(Animator var1) {
                  var3.run();
               }
            });
         }
      });
      var4.start();
   }

   public void removeView() {
      View var1 = this.getRootView();
      if(var1 != null && var1.getParent() != null) {
         ((ViewGroup)var1.getParent()).removeView(var1);
      }

   }

   public void setPosition(int var1) {
      this.position = var1;
   }

   protected void stopListeningForTouchEvents() {
      this.overlayRoot.setOnTouchListener((OnTouchListener)null);
   }

   public abstract void update(Conversation var1, Runnable var2);

   public void updateBottomPadding(Context var1, int var2) {
      if(this.bottomPadding != var2 && this.overlayRoot != null) {
         this.bottomPadding = var2;
         Resources var5 = var1.getResources();
         int var4 = ScreenUtils.dpToPx((float)(this.position * 8 + 16), var1);
         int var3 = var5.getDimensionPixelSize(R.dimen.intercom_bottom_padding);
         final LayoutParams var6 = (LayoutParams)this.overlayRoot.getLayoutParams();
         ValueAnimator var7 = ValueAnimator.ofInt(new int[]{var6.bottomMargin, var3 + var4 + var2});
         var7.setDuration((long)300);
         var7.addUpdateListener(new AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator var1) {
               var6.setMargins(var6.leftMargin, var6.topMargin, var6.rightMargin, ((Integer)var1.getAnimatedValue()).intValue());
               InAppNotification.this.overlayRoot.setLayoutParams(var6);
            }
         });
         var7.start();
      }

   }

   protected abstract void updateViewDataDuringReplyPulse(int var1);

   interface Listener {
      void onInAppNotificationDismiss(InAppNotification var1);

      void onInAppNotificationTap(Conversation var1);
   }
}
