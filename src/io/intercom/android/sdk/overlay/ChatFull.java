package io.intercom.android.sdk.overlay;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.ViewTreeObserver.OnPreDrawListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import io.intercom.android.sdk.Injector;
import io.intercom.android.sdk.Provider;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.api.Api;
import io.intercom.android.sdk.blocks.Blocks;
import io.intercom.android.sdk.blocks.BlocksViewHolder;
import io.intercom.android.sdk.blocks.ButtonClickListener;
import io.intercom.android.sdk.blocks.ImageClickListener;
import io.intercom.android.sdk.blocks.UploadingImageCache;
import io.intercom.android.sdk.blocks.ViewHolderGenerator;
import io.intercom.android.sdk.logger.LumberMill;
import io.intercom.android.sdk.metrics.MetricTracker;
import io.intercom.android.sdk.models.Conversation;
import io.intercom.android.sdk.models.Part;
import io.intercom.com.bumptech.glide.i;

class ChatFull extends ChatNotification {
   private final MetricTracker metricTracker;
   private final i requestManager;

   ChatFull(Context var1, Conversation var2, int var3, int var4, InAppNotification.Listener var5, MetricTracker var6, Provider var7, i var8) {
      super(var1, var2, var3, var4, var5, var7, var8);
      this.metricTracker = var6;
      this.requestManager = var8;
   }

   protected View getContentContainer() {
      return ((ViewGroup)this.overlayRoot.findViewById(R.id.chathead_text_container)).getChildAt(1);
   }

   protected ViewGroup inflateChatRootView(final ViewGroup var1, LayoutInflater var2) {
      var1 = (ViewGroup)var2.inflate(R.layout.intercom_preview_chat_full_overlay, var1, false);
      Blocks var3 = new Blocks(this.localisedContext, LumberMill.getBlocksTwig());
      Api var5 = Injector.get().getApi();
      BlocksViewHolder var4 = (new ViewHolderGenerator(new UploadingImageCache(), var5, this.appConfigProvider, this.conversation.getId(), new ChatFull.ChatFullImageClickListener(null), new ChatFull.ChatFullButtonClickListener(null), this.requestManager)).getChatFullHolder();
      LinearLayout var7 = var3.createBlocks(this.conversation.getLastPart().getBlocks(), var4);
      final ViewGroup var6 = (ViewGroup)var1.findViewById(R.id.chathead_text_container);
      var6.addView(var7);
      var6.getViewTreeObserver().addOnPreDrawListener(new OnPreDrawListener() {
         public boolean onPreDraw() {
            var6.getViewTreeObserver().removeOnPreDrawListener(this);
            int var3 = var6.getMeasuredHeight();
            int var1x = ((MarginLayoutParams)var6.getLayoutParams()).bottomMargin;
            Resources var6x = ChatFull.this.localisedContext.getResources();
            int var2 = var6x.getDimensionPixelSize(R.dimen.intercom_chat_full_top_margin);
            int var4 = var6x.getDimensionPixelSize(R.dimen.intercom_bottom_padding);
            boolean var5;
            if(var3 >= ChatFull.this.screenHeight - var2 - var1x - var4) {
               var1.findViewById(R.id.chat_overlay_overflow_fade).setVisibility(0);
               var5 = false;
            } else {
               var5 = true;
            }

            return var5;
         }
      });
      var5.markConversationAsRead(this.conversation.getId());
      this.metricTracker.viewedInAppFromFull(this.conversation.getId(), this.conversation.getLastPart().getId());
      return var1;
   }

   public void update(Conversation var1, Runnable var2) {
   }

   protected void updateContentContainer(Part var1) {
   }

   protected void updateViewDataDuringReplyPulse(int var1) {
   }

   private static class ChatFullButtonClickListener implements ButtonClickListener {
      private ChatFullButtonClickListener() {
      }

      // $FF: synthetic method
      ChatFullButtonClickListener(Object var1) {
         this();
      }

      public void onButtonClicked(View var1, String var2) {
      }

      public boolean shouldHandleClicks() {
         return false;
      }
   }

   private static class ChatFullImageClickListener implements ImageClickListener {
      private ChatFullImageClickListener() {
      }

      // $FF: synthetic method
      ChatFullImageClickListener(Object var1) {
         this();
      }

      public void onImageClicked(String var1, String var2, ImageView var3, int var4, int var5) {
      }
   }
}
