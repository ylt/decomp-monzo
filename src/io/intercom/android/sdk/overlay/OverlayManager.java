package io.intercom.android.sdk.overlay;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.app.Application;
import android.os.Handler;
import android.os.Looper;
import android.os.Build.VERSION;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import io.intercom.android.sdk.Intercom;
import io.intercom.android.sdk.Provider;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.actions.Actions;
import io.intercom.android.sdk.activities.IntercomMessengerActivity;
import io.intercom.android.sdk.activities.IntercomNoteActivity;
import io.intercom.android.sdk.activities.IntercomPostActivity;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.android.sdk.identity.UserIdentity;
import io.intercom.android.sdk.logger.LumberMill;
import io.intercom.android.sdk.metrics.MetricTracker;
import io.intercom.android.sdk.models.Conversation;
import io.intercom.android.sdk.models.Part;
import io.intercom.android.sdk.models.events.ConfigUpdateEvent;
import io.intercom.android.sdk.state.OverlayState;
import io.intercom.android.sdk.store.Selectors;
import io.intercom.android.sdk.store.Store;
import io.intercom.android.sdk.twig.Twig;
import io.intercom.android.sdk.utilities.AvatarUtils;
import io.intercom.android.sdk.utilities.ContextLocaliser;
import io.intercom.android.sdk.utilities.ViewUtils;
import io.intercom.com.a.a.b;
import io.intercom.com.a.a.h;
import io.intercom.com.bumptech.glide.i;
import java.util.Iterator;
import java.util.List;

public class OverlayManager implements InAppNotificationManager.Listener, Store.Subscriber2 {
   private final Provider appConfigProvider;
   private final Application application;
   private final Handler handler;
   final InAppNotificationManager inAppNotificationManager;
   final DefaultLauncherPresenter launcherPresenter;
   private final MetricTracker metricTracker;
   private final i requestManager;
   private final Store store;
   private final Twig twig = LumberMill.getLogger();
   private final UserIdentity userIdentity;

   public OverlayManager(Application var1, b var2, Store var3, Provider var4, MetricTracker var5, UserIdentity var6, i var7) {
      this.application = var1;
      this.appConfigProvider = var4;
      this.store = var3;
      this.metricTracker = var5;
      this.userIdentity = var6;
      this.requestManager = var7;
      LayoutInflater var8 = LayoutInflater.from(var1);
      this.handler = new Handler(Looper.getMainLooper());
      this.inAppNotificationManager = new InAppNotificationManager(var8, this.handler, this, var5, var4, new ContextLocaliser(var4), var7);
      this.launcherPresenter = new DefaultLauncherPresenter(var8, new LauncherOpenBehaviour(var4, var3, LauncherOpenBehaviour.LauncherType.DEFAULT, var5), var5);
      var2.register(this);
      var3.subscribeToChanges(Selectors.UNREAD_COUNT, Selectors.OVERLAY, this);
   }

   private ViewGroup getRootView(Activity var1) {
      ViewGroup var3 = (ViewGroup)var1.findViewById(R.id.intercom_overlay_root);
      Object var2 = var3;
      if(var3 == null) {
         var2 = new FrameLayout(var1);
         ((ViewGroup)var2).setClipChildren(false);
         ((ViewGroup)var2).setClipToPadding(false);
         ((ViewGroup)var2).setFitsSystemWindows(true);
         ((ViewGroup)var2).setId(R.id.intercom_overlay_root);
         var1.addContentView((View)var2, new LayoutParams(-1, -1, 80));
      }

      return (ViewGroup)var2;
   }

   private void openMessenger(Conversation var1) {
      this.application.startActivity(IntercomMessengerActivity.openConversation(this.application, var1.getId(), var1.getLastParticipatingAdmin()));
   }

   private void openNote(Conversation var1, boolean var2) {
      Part var3 = var1.getLastPart();
      if(var2) {
         this.metricTracker.viewedInAppFromSnippet(var1.getId(), var3.getId());
      } else {
         this.metricTracker.viewedInAppFromFull(var1.getId(), var3.getId());
      }

      try {
         this.application.startActivity(IntercomNoteActivity.buildNoteIntent(this.application, var3, var1.getId(), var1.getLastParticipatingAdmin()));
      } catch (IllegalArgumentException var4) {
         this.twig.internal("Overlay", "Error loading the note " + var4.getMessage());
      }

   }

   private void openPost(Conversation var1, boolean var2) {
      Part var3 = var1.getLastPart();
      if(var2) {
         this.metricTracker.viewedInAppFromSnippet(var1.getId(), var3.getId());
      } else {
         this.metricTracker.viewedInAppFromFull(var1.getId(), var3.getId());
      }

      this.application.startActivity(IntercomPostActivity.buildPostIntent(this.application, var1.getLastPart(), var1.getId(), var1.getLastParticipatingAdmin(), true));
   }

   public void cancelAnimations() {
      this.handler.removeCallbacksAndMessages((Object)null);
   }

   @h
   public void configUpdated(ConfigUpdateEvent var1) {
      OverlayState var3 = (OverlayState)this.store.select(Selectors.OVERLAY);
      Activity var2 = var3.resumedHostActivity();
      if(this.shouldDisplayLauncher(var3.conversations(), var3.notificationVisibility(), var3.launcherVisibility(), var2)) {
         final ViewGroup var4 = this.getRootView(var2);
         ViewUtils.waitForViewAttachment(var4, new Runnable() {
            public void run() {
               int var1 = ((AppConfig)OverlayManager.this.appConfigProvider.get()).getBaseColor();
               OverlayManager.this.launcherPresenter.displayLauncherOnAttachedRoot(var4, var1);
            }
         });
      } else {
         this.launcherPresenter.setLauncherBackgroundColor(((AppConfig)this.appConfigProvider.get()).getBaseColor());
      }

   }

   void displayNotifications(final List var1, Activity var2) {
      final ViewGroup var6 = this.getRootView(var2);
      Conversation var5 = (Conversation)var1.get(0);
      String var3 = var5.getLastPart().getMessageStyle();
      Part.DeliveryOption var4 = var5.getLastPart().getDeliveryOption();
      if(var1.size() > 1 || this.inAppNotificationManager.isDisplaying() || var4 == Part.DeliveryOption.SUMMARY || var4 == Part.DeliveryOption.FULL && "chat".equals(var3)) {
         if(this.launcherPresenter.isDisplaying()) {
            final DefaultLauncher var7 = this.launcherPresenter.getAndUnsetLauncher();
            if(var1.size() == 1) {
               var7.pulseForTransformation(new AnimatorListenerAdapter() {
                  public void onAnimationEnd(Animator var1x) {
                     var7.fadeOffScreen(new AnimatorListenerAdapter() {
                        public void onAnimationEnd(Animator var1x) {
                           var7.removeView();
                        }
                     });
                     OverlayManager.this.inAppNotificationManager.displayNotifications(var6, var1);
                  }
               });
            } else {
               var7.fadeOffScreen(new AnimatorListenerAdapter() {
                  public void onAnimationEnd(Animator var1x) {
                     var7.removeView();
                     OverlayManager.this.inAppNotificationManager.displayNotifications(var6, var1);
                  }
               });
            }
         } else {
            this.inAppNotificationManager.displayNotifications(var6, var1);
         }
      } else if(var4 == Part.DeliveryOption.FULL) {
         if("post".equals(var3)) {
            this.openPost(var5, false);
         } else if("note".equals(var3)) {
            this.openNote(var5, false);
         }
      }

   }

   public ViewGroup getRootView() throws Exception {
      Activity var1 = ((OverlayState)this.store.select(Selectors.OVERLAY)).resumedHostActivity();
      if(var1 == null) {
         throw new NullPointerException();
      } else {
         return this.getRootView(var1);
      }
   }

   public void markAsDismissed(Conversation var1) {
      this.store.dispatch(Actions.conversationMarkedAsDismissed(var1));
   }

   public void onStateChange(final Integer var1, OverlayState var2) {
      Intercom.Visibility var3 = var2.notificationVisibility();
      Activity var4 = var2.resumedHostActivity();
      List var5 = var2.conversations();
      this.launcherPresenter.setBottomPadding(var2.bottomPadding());
      this.inAppNotificationManager.setBottomPadding(var2.bottomPadding());
      if(var4 != null && !var4.isFinishing() && (VERSION.SDK_INT < 17 || !var4.isDestroyed())) {
         if(this.shouldDisplayNotifications(var5, var3, var4)) {
            this.preloadAvatarThenDisplayNotifications(var5, var3, var4, this.userIdentity.getFingerprint());
         } else if(this.shouldDisplayLauncher(var5, var3, var2.launcherVisibility(), var4)) {
            final ViewGroup var6 = this.getRootView(var4);
            ViewUtils.waitForViewAttachment(var6, new Runnable() {
               public void run() {
                  OverlayManager.this.inAppNotificationManager.reset(var6);
                  OverlayManager.this.launcherPresenter.setUnreadCount(var1.intValue());
                  int var1x = ((AppConfig)OverlayManager.this.appConfigProvider.get()).getBaseColor();
                  OverlayManager.this.launcherPresenter.displayLauncherOnAttachedRoot(var6, var1x);
               }
            });
         } else {
            this.removeOverlaysIfPresent(var4);
         }
      }

   }

   public void openNotification(Conversation var1) {
      Part var3 = var1.getLastAdminPart();
      String var2 = var3.getMessageStyle();
      if("post".equals(var2)) {
         this.openPost(var1, true);
      } else if("note".equals(var2)) {
         this.openNote(var1, true);
      } else {
         this.openMessenger(var1);
         if("chat".equals(var2) && Part.DeliveryOption.FULL == var3.getDeliveryOption()) {
            this.metricTracker.openedConversationFromFull(var1.getId(), var3.getId());
         } else {
            this.metricTracker.openedConversationFromSnippet(var1.getId(), var3.getId());
         }
      }

   }

   void preloadAvatarThenDisplayNotifications(final List var1, final Intercom.Visibility var2, final Activity var3, final String var4) {
      AvatarUtils.preloadAvatar(((Conversation)var1.get(0)).getLastAdmin().getAvatar(), new Runnable() {
         public void run() {
            if(var4.equals(OverlayManager.this.userIdentity.getFingerprint()) && OverlayManager.this.shouldDisplayNotifications(var1, var2, var3)) {
               OverlayManager.this.displayNotifications(var1, var3);
            }

         }
      }, this.requestManager);
   }

   public void refreshStateBecauseUserIdentityIsNotInStoreYet() {
      this.onStateChange((Integer)this.store.select(Selectors.UNREAD_COUNT), (OverlayState)this.store.select(Selectors.OVERLAY));
   }

   public void removeOverlaysIfPresent(Activity var1) {
      var1.runOnUiThread(new Runnable(var1.findViewById(R.id.intercom_overlay_root)) {
         // $FF: synthetic field
         final View val$root;

         {
            this.val$root = var2;
         }

         public void run() {
            if(this.val$root != null) {
               OverlayManager.this.launcherPresenter.removeLauncher();
               OverlayManager.this.inAppNotificationManager.reset((ViewGroup)this.val$root);
               if(this.val$root.getParent() != null) {
                  ((ViewGroup)this.val$root.getParent()).removeView(this.val$root);
               }
            }

         }
      });
   }

   public void sendUnreadConversationsReceivedMetric(List var1) {
      Iterator var3 = var1.iterator();

      while(var3.hasNext()) {
         Conversation var2 = (Conversation)var3.next();
         Part var4 = var2.getLastPart();
         if(Part.DeliveryOption.SUMMARY == var4.getDeliveryOption()) {
            if(var4.isInitialMessage()) {
               this.metricTracker.receivedMessageFromSnippetWhenClosed(var4.hasAttachments(), var4.isLinkCard(), var2.getId(), var4.getId(), var4.getMessageStyle());
            } else {
               this.metricTracker.receivedReplyFromSnippetWhenClosed(var4.hasAttachments(), var4.isLinkCard(), var2.getId(), var4.getId());
            }
         } else {
            this.metricTracker.receivedMessageFromFullWhenClosed(var4.hasAttachments(), var4.isLinkCard(), var2.getId(), var4.getId(), var4.getMessageStyle());
         }
      }

   }

   boolean shouldDisplayLauncher(List var1, Intercom.Visibility var2, Intercom.Visibility var3, Activity var4) {
      boolean var5 = false;
      if(var2 == Intercom.Visibility.VISIBLE) {
         Iterator var7 = var1.iterator();

         while(var7.hasNext()) {
            Conversation var6 = (Conversation)var7.next();
            if(Part.DeliveryOption.BADGE != var6.getLastPart().getDeliveryOption()) {
               return var5;
            }
         }
      }

      if(var3 == Intercom.Visibility.VISIBLE && !this.userIdentity.isSoftReset() && this.userIdentity.identityExists() && ((AppConfig)this.appConfigProvider.get()).isReceivedFromServer() && var4 != null) {
         var5 = true;
      } else {
         var5 = false;
      }

      return var5;
   }

   boolean shouldDisplayNotifications(List var1, Intercom.Visibility var2, Activity var3) {
      boolean var4;
      if(var2 == Intercom.Visibility.VISIBLE && !this.userIdentity.isSoftReset() && !var1.isEmpty() && var3 != null) {
         var4 = true;
      } else {
         var4 = false;
      }

      return var4;
   }

   public void softReset() {
      this.cancelAnimations();
      Activity var1 = ((OverlayState)this.store.select(Selectors.OVERLAY)).resumedHostActivity();
      if(var1 != null) {
         this.removeOverlaysIfPresent(var1);
      }

   }
}
