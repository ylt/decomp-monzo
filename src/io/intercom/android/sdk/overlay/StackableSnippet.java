package io.intercom.android.sdk.overlay;

import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.os.Handler;
import android.os.Build.VERSION;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnTouchListener;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.FrameLayout.LayoutParams;
import io.intercom.android.sdk.Provider;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.commons.utilities.ScreenUtils;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.android.sdk.models.Conversation;
import io.intercom.android.sdk.utilities.AvatarUtils;
import io.intercom.android.sdk.utilities.FontUtils;
import io.intercom.com.bumptech.glide.i;

class StackableSnippet extends InAppNotification implements OnTouchListener {
   private static final int BASE_ELEVATION = 18;
   private static final int BASE_MARGIN_DP = 16;
   private static final int ELEVATION_STEP = 3;
   private final Handler handler;
   private final i requestManager;

   public StackableSnippet(Context var1, Conversation var2, int var3, Handler var4, int var5, InAppNotification.Listener var6, Provider var7, i var8) {
      super(var1, var2, var3, var5, var6, var7);
      this.handler = var4;
      this.requestManager = var8;
   }

   private void animateOnScreen() {
      this.handler.postDelayed(new Runnable() {
         public void run() {
            StackableSnippet.this.overlayRoot.setVisibility(0);
            StackableSnippet.this.overlayRoot.setY((float)StackableSnippet.this.screenHeight);
            StackableSnippet.this.overlayRoot.animate().setInterpolator(new OvershootInterpolator(0.6F)).translationY(0.0F).setDuration(300L).start();
         }
      }, (long)(this.position * 70));
   }

   private void setViewData(int var1) {
      Context var2 = this.overlayRoot.getContext();
      AppConfig var3 = (AppConfig)this.appConfigProvider.get();
      TextView var4 = (TextView)this.overlayRoot.findViewById(R.id.preview_name);
      FontUtils.setRobotoMediumTypeface(var4);
      var4.setTextColor(var3.getBaseColor());
      var4.setText(this.getHeaderText());
      ((TextView)this.overlayRoot.findViewById(R.id.preview_summary)).setText(this.conversation.getLastPart().getSummary());
      ImageView var5 = (ImageView)this.overlayRoot.findViewById(R.id.preview_avatar);
      AvatarUtils.loadAvatarIntoView(this.conversation.getLastAdmin().getAvatar(), var5, var3, this.requestManager);
      ((LayoutParams)this.overlayRoot.getLayoutParams()).bottomMargin = ScreenUtils.dpToPx((float)(this.position * 8 + 16), var2) + var2.getResources().getDimensionPixelSize(R.dimen.intercom_bottom_padding) + var1;
      if(VERSION.SDK_INT >= 21) {
         this.overlayRoot.setElevation((float)(18 - this.position * 3));
      }

      this.overlayRoot.setScaleX(1.0F - (float)this.position * 0.05F);
      if(this.position == 0) {
         this.beginListeningForTouchEvents();
      }

   }

   private void setupView(ViewGroup var1, LayoutInflater var2) {
      if(this.overlayRoot == null) {
         this.overlayRoot = (LinearLayout)var2.inflate(R.layout.intercom_preview_notification, var1, false);
      }

      if(!this.isAttached()) {
         var1.addView(this.overlayRoot, 0);
      }

   }

   public void display(ViewGroup var1, LayoutInflater var2, boolean var3, int var4) {
      this.setupView(var1, var2);
      this.setViewData(var4);
      if(var3) {
         this.animateOnScreen();
      } else {
         this.overlayRoot.setVisibility(0);
      }

   }

   public void moveBackward(ViewGroup var1, AnimatorListenerAdapter var2) {
      ++this.position;
      this.animateToPosition(var1.getContext());
   }

   protected void onNotificationPressed(View var1) {
      var1.animate().scaleX(0.95F).scaleY(0.95F).setDuration(50L).start();
   }

   protected void onNotificationReleased(View var1) {
      var1.animate().scaleX(1.0F).scaleY(1.0F).setDuration(50L).start();
   }

   public void update(Conversation var1, Runnable var2) {
      this.conversation = var1;
      this.performReplyPulse(this.overlayRoot.findViewById(R.id.notification_root), this.overlayRoot.findViewById(R.id.preview_summary), var2);
   }

   protected void updateViewDataDuringReplyPulse(int var1) {
      this.setViewData(var1);
   }
}
