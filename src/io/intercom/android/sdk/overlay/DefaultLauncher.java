package io.intercom.android.sdk.overlay;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.animation.Animator.AnimatorListener;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.support.v4.content.a;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.TextView;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.utilities.BackgroundUtils;

class DefaultLauncher implements OnTouchListener {
   private static final int ANIMATION_DURATION_MS = 300;
   private final View badge;
   private final TextView badgeCount;
   final View launcherRoot;
   final DefaultLauncher.Listener listener;

   public DefaultLauncher(ViewGroup var1, LayoutInflater var2, DefaultLauncher.Listener var3, int var4) {
      this.listener = var3;
      var2.inflate(R.layout.intercom_default_launcher, var1, true);
      this.launcherRoot = var1.findViewById(R.id.launcher_root);
      MarginLayoutParams var5 = (MarginLayoutParams)this.launcherRoot.getLayoutParams();
      var5.setMargins(var5.leftMargin, var5.topMargin, var5.rightMargin, var4);
      this.launcherRoot.setLayoutParams(var5);
      this.badge = this.launcherRoot.findViewById(R.id.launcher_icon);
      this.badgeCount = (TextView)this.launcherRoot.findViewById(R.id.launcher_badge_count);
      this.launcherRoot.setOnTouchListener(this);
   }

   private void callListenerWithFadeOut() {
      this.launcherRoot.setAlpha(1.0F);
      this.launcherRoot.animate().alpha(0.0F).setDuration(50L).setListener(new AnimatorListenerAdapter() {
         public void onAnimationEnd(Animator var1) {
            DefaultLauncher.this.listener.onLauncherClicked(DefaultLauncher.this.launcherRoot.getContext());
         }
      }).start();
   }

   public void fadeOffScreen(AnimatorListener var1) {
      this.launcherRoot.animate().alpha(0.0F).setDuration(100L).setListener(var1).start();
   }

   public void fadeOnScreen() {
      this.launcherRoot.setAlpha(0.0F);
      this.launcherRoot.animate().alpha(1.0F).setDuration(100L).start();
   }

   public void hideBadgeCount() {
      this.badgeCount.setVisibility(8);
   }

   public boolean isAttachedToRoot(ViewGroup var1) {
      boolean var2;
      if(this.launcherRoot.getParent() == var1) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   public boolean onTouch(View var1, MotionEvent var2) {
      switch(var2.getAction()) {
      case 0:
         var1.setScaleX(0.9F);
         var1.setScaleY(0.9F);
         break;
      case 1:
         this.callListenerWithFadeOut();
      }

      return true;
   }

   public void pulseForTransformation(final AnimatorListener var1) {
      this.launcherRoot.animate().scaleX(1.1F).scaleY(1.1F).setDuration(100L).setListener(new AnimatorListenerAdapter() {
         public void onAnimationEnd(Animator var1x) {
            var1.onAnimationEnd(var1x);
            DefaultLauncher.this.launcherRoot.animate().scaleX(1.0F).scaleY(1.0F).setDuration(100L).start();
         }
      }).start();
   }

   public void removeView() {
      if(this.launcherRoot.getParent() != null) {
         ((ViewGroup)this.launcherRoot.getParent()).removeView(this.launcherRoot);
      }

   }

   public void setBackgroundColor(int var1) {
      Context var3 = this.badge.getContext();
      Drawable var2 = a.a(var3, R.drawable.intercom_solid_circle);
      var2.setColorFilter(var1, Mode.SRC_IN);
      Drawable var4 = var2.getConstantState().newDrawable();
      var4.setColorFilter(a.c(var3, R.color.intercom_inbox_count_background), Mode.SRC_IN);
      BackgroundUtils.setBackground(this.badgeCount, var4);
      BackgroundUtils.setBackground(this.badge, var2);
   }

   public void setBadgeCount(String var1) {
      this.badgeCount.setVisibility(0);
      this.badgeCount.setText(var1);
   }

   public void updateBottomPadding(int var1) {
      final MarginLayoutParams var3 = (MarginLayoutParams)this.launcherRoot.getLayoutParams();
      ValueAnimator var2 = ValueAnimator.ofInt(new int[]{var3.bottomMargin, var1});
      var2.setDuration(300L);
      var2.addUpdateListener(new AnimatorUpdateListener() {
         public void onAnimationUpdate(ValueAnimator var1) {
            var3.setMargins(var3.leftMargin, var3.topMargin, var3.rightMargin, ((Integer)var1.getAnimatedValue()).intValue());
            DefaultLauncher.this.launcherRoot.setLayoutParams(var3);
         }
      });
      var2.start();
   }

   public interface Listener {
      void onLauncherClicked(Context var1);
   }
}
