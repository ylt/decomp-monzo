package io.intercom.android.sdk.overlay;

import android.net.Uri;

public interface ScreenshotEventListener {
   void onScreenshotDeleted(Uri var1);

   void onScreenshotFolderDeleted();

   void onScreenshotTaken(Uri var1);
}
