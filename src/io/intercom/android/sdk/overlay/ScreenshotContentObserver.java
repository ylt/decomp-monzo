package io.intercom.android.sdk.overlay;

import android.content.ContentResolver;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.provider.MediaStore.Images.Media;
import io.intercom.android.sdk.commons.utilities.TimeProvider;
import io.intercom.android.sdk.logger.LumberMill;
import io.intercom.android.sdk.store.Store;
import io.intercom.android.sdk.twig.Twig;

public class ScreenshotContentObserver extends ContentObserver {
   private static final long DEFAULT_DETECT_WINDOW_SECONDS = 5L;
   private static final String EXTERNAL_CONTENT_URI_STRING;
   private static final String[] PROJECTION;
   private static final String ROOT_FOLDER_URI_STRING = "content://media/external";
   private static final String SORT_ORDER = "date_added DESC";
   private final ContentResolver contentResolver;
   private long lastScreenshotTimestamp;
   private final Store store;
   private final Twig twig = LumberMill.getLogger();

   static {
      EXTERNAL_CONTENT_URI_STRING = Media.EXTERNAL_CONTENT_URI.toString();
      PROJECTION = new String[]{"_display_name", "_data", "date_added"};
   }

   public ScreenshotContentObserver(Store var1, ContentResolver var2) {
      super((Handler)null);
      this.store = var1;
      this.contentResolver = var2;
   }

   private Uri getLatestScreenshot(Cursor var1) {
      Uri var5;
      if(var1 != null && var1.moveToFirst()) {
         String var4 = var1.getString(var1.getColumnIndex("_data"));
         long var2 = var1.getLong(var1.getColumnIndex("date_added"));
         if(this.isInScreenshotFolder(var4) && this.isRecentEnough(var2)) {
            this.lastScreenshotTimestamp = var2;
            var5 = Uri.parse(var4);
            return var5;
         }
      }

      var5 = Uri.EMPTY;
      return var5;
   }

   private boolean isInScreenshotFolder(String var1) {
      return var1.contains("Screenshot");
   }

   private boolean isRecentEnough(long var1) {
      boolean var3 = false;
      long var4 = TimeProvider.SYSTEM.currentTimeMillis() / 1000L;
      if(var1 >= this.lastScreenshotTimestamp && Math.abs(var4 - var1) <= 5L) {
         var3 = true;
      }

      return var3;
   }

   private void queryContentForUri(Uri param1) {
      // $FF: Couldn't be decompiled
   }

   public void onChange(boolean var1, Uri var2) {
      String var3 = var2.toString();
      if("content://media/external".equals(var3)) {
         this.queryContentForUri(Media.EXTERNAL_CONTENT_URI);
      } else if(var3.startsWith(EXTERNAL_CONTENT_URI_STRING)) {
         this.queryContentForUri(var2);
      }

      super.onChange(var1, var2);
   }
}
