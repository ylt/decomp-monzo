package io.intercom.android.sdk.overlay;

import android.content.Context;
import android.text.TextUtils;
import io.intercom.android.sdk.Provider;
import io.intercom.android.sdk.activities.IntercomMessengerActivity;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.android.sdk.metrics.MetricTracker;
import io.intercom.android.sdk.models.LastParticipatingAdmin;
import io.intercom.android.sdk.state.State;
import io.intercom.android.sdk.state.UiState;
import io.intercom.android.sdk.store.Store;
import java.util.Set;

public class LauncherOpenBehaviour {
   private final Provider appConfigProvider;
   private final LauncherOpenBehaviour.LauncherType launcherType;
   private final MetricTracker metricTracker;
   private final Store store;

   public LauncherOpenBehaviour(Provider var1, Store var2, LauncherOpenBehaviour.LauncherType var3, MetricTracker var4) {
      this.appConfigProvider = var1;
      this.store = var2;
      this.launcherType = var3;
      this.metricTracker = var4;
   }

   private boolean hasEmptyInbox() {
      State var2 = (State)this.store.state();
      boolean var1;
      if(Boolean.FALSE.equals(Boolean.valueOf(var2.hasConversations())) && var2.unreadConversationIds().isEmpty()) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   private void openLastOrDefaultState(Context var1) {
      UiState var2 = ((State)this.store.state()).uiState();
      switch(null.$SwitchMap$io$intercom$android$sdk$state$UiState$Screen[var2.screen().ordinal()]) {
      case 1:
         String var3 = var2.conversationId();
         if(TextUtils.isEmpty(var3)) {
            this.presentInbox(var1);
         } else {
            this.presentConversation(var1, var3);
         }
         break;
      case 2:
      default:
         this.presentInbox(var1);
         break;
      case 3:
         this.presentComposer(var1);
         break;
      case 4:
         if(this.hasEmptyInbox() && ((AppConfig)this.appConfigProvider.get()).isInboundMessages()) {
            this.presentComposer(var1);
         } else {
            this.presentInbox(var1);
         }
      }

   }

   private void presentComposer(Context var1) {
      this.metricTracker.openedMessengerNewConversation(this.launcherType);
      var1.startActivity(IntercomMessengerActivity.openComposer(var1, ""));
   }

   private void presentConversation(Context var1, String var2) {
      this.metricTracker.openedMessengerConversation(var2, this.launcherType);
      var1.startActivity(IntercomMessengerActivity.openConversation(var1, var2, LastParticipatingAdmin.NULL));
   }

   private void presentInbox(Context var1) {
      this.metricTracker.openedMessengerConversationList(this.launcherType);
      var1.startActivity(IntercomMessengerActivity.openInbox(var1));
   }

   public void openMessenger(Context var1) {
      Set var2 = ((State)this.store.state()).unreadConversationIds();
      if(var2.size() == 1) {
         this.presentConversation(var1, (String)var2.iterator().next());
      } else if(var2.size() > 1) {
         this.presentInbox(var1);
      } else {
         this.openLastOrDefaultState(var1);
      }

   }

   public static enum LauncherType {
      CUSTOM,
      DEFAULT;
   }
}
