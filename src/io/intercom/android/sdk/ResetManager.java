package io.intercom.android.sdk;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import io.intercom.android.sdk.actions.Actions;
import io.intercom.android.sdk.api.Api;
import io.intercom.android.sdk.api.UserUpdateBatcher;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.android.sdk.identity.UserIdentity;
import io.intercom.android.sdk.logger.LumberMill;
import io.intercom.android.sdk.overlay.OverlayManager;
import io.intercom.android.sdk.store.Store;
import io.intercom.android.sdk.twig.Twig;
import io.intercom.android.sdk.user.DeviceData;
import io.intercom.android.sdk.utilities.ActivityFinisher;

class ResetManager {
   private final ActivityFinisher activityFinisher;
   private final Provider apiProvider;
   private final Provider appConfigProvider;
   private final Context context;
   private final Handler handler = new Handler(Looper.getMainLooper());
   private final Runnable hardResetTask = new Runnable() {
      public void run() {
         ResetManager.this.hardReset();
      }
   };
   private final OverlayManager overlayManager;
   private final Store store;
   private final Twig twig = LumberMill.getLogger();
   private final UserIdentity userIdentity;
   private final UserUpdateBatcher userUpdateBatcher;

   ResetManager(Provider var1, UserIdentity var2, OverlayManager var3, Provider var4, Store var5, UserUpdateBatcher var6, Context var7, ActivityFinisher var8) {
      this.apiProvider = var1;
      this.userIdentity = var2;
      this.overlayManager = var3;
      this.appConfigProvider = var4;
      this.store = var5;
      this.userUpdateBatcher = var6;
      this.context = var7;
      this.activityFinisher = var8;
   }

   public void clear() {
      this.handler.removeCallbacks(this.hardResetTask);
   }

   void hardReset() {
      this.handler.removeCallbacks(this.hardResetTask);
      if(this.isSoftReset()) {
         String var1 = DeviceData.getDeviceToken(this.context);
         if(TextUtils.isEmpty(var1)) {
            this.twig.internal("There is no device token to remove.");
         } else {
            ((Api)this.apiProvider.get()).removeDeviceToken(var1);
         }

         this.store.dispatch(Actions.hardReset());
      }

   }

   boolean isSoftReset() {
      return this.userIdentity.isSoftReset();
   }

   void softReset() {
      this.userUpdateBatcher.submitPendingUpdate();
      this.activityFinisher.finishActivities();
      this.overlayManager.softReset();
      this.handler.postDelayed(this.hardResetTask, ((AppConfig)this.appConfigProvider.get()).getSoftResetTimeoutMs());
      this.store.dispatch(Actions.softReset());
      this.twig.i("Successfully reset the user. To resume communicating with Intercom, you can register a user", new Object[0]);
   }
}
