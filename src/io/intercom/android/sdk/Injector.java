package io.intercom.android.sdk;

import android.annotation.SuppressLint;
import android.app.Application;
import android.app.NotificationManager;
import android.content.Context;
import android.content.SharedPreferences;
import io.intercom.android.sdk.api.Api;
import io.intercom.android.sdk.api.ApiFactory;
import io.intercom.android.sdk.api.DeDuper;
import io.intercom.android.sdk.api.UserUpdateBatcher;
import io.intercom.android.sdk.commons.utilities.TimeProvider;
import io.intercom.android.sdk.conversation.SoundPlayer;
import io.intercom.android.sdk.errorreporting.ErrorReporter;
import io.intercom.android.sdk.exceptions.IntercomIntegrationException;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.android.sdk.identity.AppIdentity;
import io.intercom.android.sdk.identity.UserIdentity;
import io.intercom.android.sdk.logger.LumberMill;
import io.intercom.android.sdk.metrics.MetricTracker;
import io.intercom.android.sdk.metrics.MetricsStore;
import io.intercom.android.sdk.metrics.ops.OpsMetricTracker;
import io.intercom.android.sdk.nexus.NexusClient;
import io.intercom.android.sdk.overlay.OverlayManager;
import io.intercom.android.sdk.push.SystemNotificationManager;
import io.intercom.android.sdk.store.Store;
import io.intercom.android.sdk.store.StoreFactory;
import io.intercom.android.sdk.twig.Twig;
import io.intercom.android.sdk.utilities.ActivityFinisher;
import io.intercom.android.sdk.utilities.SystemSettings;
import io.intercom.com.a.a.b;
import io.intercom.com.a.a.i;
import io.intercom.com.bumptech.glide.c;
import io.intercom.com.google.gson.e;
import java.util.concurrent.TimeUnit;
import javax.annotation.Nullable;

public class Injector {
   private static final Twig TWIG = LumberMill.getLogger();
   @Nullable
   @SuppressLint({"StaticFieldLeak"})
   private static Injector instance;
   private final ActivityFinisher activityFinisher = new ActivityFinisher();
   @Nullable
   private Api api;
   private final Provider apiProvider = new Provider() {
      public Api get() {
         return Injector.this.getApi();
      }
   };
   private final AppConfig appConfig;
   private final Provider appConfigProvider = new Provider() {
      public AppConfig get() {
         return Injector.this.appConfig;
      }
   };
   private final AppIdentity appIdentity;
   private final Application application;
   @Nullable
   private MainThreadBus bus;
   @Nullable
   private ErrorReporter errorReporter;
   @Nullable
   private e gson;
   @Nullable
   private LifecycleTracker lifecycleTracker;
   @Nullable
   private MetricTracker metricTracker;
   @Nullable
   private MetricsStore metricsStore;
   @Nullable
   private NexusWrapper nexusClient;
   private final Provider nexusClientProvider = new Provider() {
      public NexusClient get() {
         return Injector.this.getNexusClient();
      }
   };
   @Nullable
   private OpsMetricTracker opsMetricTracker;
   @Nullable
   private OverlayManager overlayManager;
   private final Provider overlayManagerProvider = new Provider() {
      public OverlayManager get() {
         return Injector.this.getOverlayManager();
      }
   };
   @Nullable
   private ResetManager resetManager;
   @Nullable
   private Store store;
   @Nullable
   private DeDuper superDeDuper;
   @Nullable
   private SystemNotificationManager systemNotificationManager;
   private final UserIdentity userIdentity;
   private final Provider userIdentityProvider = new Provider() {
      public UserIdentity get() {
         return Injector.this.getUserIdentity();
      }
   };
   @Nullable
   private UserUpdateBatcher userUpdateBatcher;
   private final Provider userUpdateBatcherProvider = new Provider() {
      public UserUpdateBatcher get() {
         return Injector.this.getUserUpdateBatcher();
      }
   };

   protected Injector(Application var1, AppIdentity var2, AppConfig var3, UserIdentity var4) {
      this.application = var1;
      this.appIdentity = var2;
      this.appConfig = var3;
      this.userIdentity = var4;
   }

   public static Injector get() {
      synchronized(Injector.class){}

      Injector var0;
      try {
         if(instance == null) {
            IntercomIntegrationException var3 = new IntercomIntegrationException("Intercom was not initialized correctly, Intercom.initialize() needs to be called in onCreate() in your Application class.");
            throw var3;
         }

         var0 = instance;
      } finally {
         ;
      }

      return var0;
   }

   public static void initIfCachedCredentials(Application param0) {
      // $FF: Couldn't be decompiled
   }

   static void initWithAppCredentials(Application param0, String param1, String param2) {
      // $FF: Couldn't be decompiled
   }

   public static boolean isNotInitialised() {
      synchronized(Injector.class){}
      boolean var3 = false;

      Injector var1;
      try {
         var3 = true;
         var1 = instance;
         var3 = false;
      } finally {
         if(var3) {
            ;
         }
      }

      boolean var0;
      if(var1 == null) {
         var0 = true;
      } else {
         var0 = false;
      }

      return var0;
   }

   static void setSharedInstance(Injector var0) {
      instance = var0;
   }

   public ActivityFinisher getActivityFinisher() {
      return this.activityFinisher;
   }

   public Api getApi() {
      synchronized(this){}

      Api var4;
      try {
         if(this.api == null) {
            String var1 = ApiFactory.getHostname(this.appIdentity);
            this.api = ApiFactory.create(this.getApplication(), this.appIdentity, this.userIdentity, this.getBus(), this.getStore(), var1, this.appConfigProvider, this.getGson());
         }

         this.api.updateMaxRequests();
         var4 = this.api;
      } finally {
         ;
      }

      return var4;
   }

   public Provider getApiProvider() {
      return this.apiProvider;
   }

   public Provider getAppConfigProvider() {
      return this.appConfigProvider;
   }

   public AppIdentity getAppIdentity() {
      return this.appIdentity;
   }

   public Application getApplication() {
      return this.application;
   }

   public b getBus() {
      synchronized(this){}

      MainThreadBus var1;
      try {
         if(this.bus == null) {
            var1 = new MainThreadBus(i.a);
            this.bus = var1;
         }

         var1 = this.bus;
      } finally {
         ;
      }

      return var1;
   }

   public DeDuper getDeDuper() {
      synchronized(this){}

      DeDuper var5;
      try {
         if(this.superDeDuper == null) {
            SharedPreferences var1 = this.application.getSharedPreferences("INTERCOM_DEDUPER_PREFS", 0);
            DeDuper var2 = new DeDuper(this.appConfigProvider, var1);
            this.superDeDuper = var2;
            this.superDeDuper.readPersistedCachedAttributes();
         }

         var5 = this.superDeDuper;
      } finally {
         ;
      }

      return var5;
   }

   public ErrorReporter getErrorReporter() {
      synchronized(this){}

      ErrorReporter var1;
      try {
         if(this.errorReporter == null) {
            this.errorReporter = ErrorReporter.create(this.application, this.getGson(), this.apiProvider);
         }

         var1 = this.errorReporter;
      } finally {
         ;
      }

      return var1;
   }

   public e getGson() {
      synchronized(this){}

      e var1;
      try {
         if(this.gson == null) {
            var1 = new e();
            this.gson = var1;
         }

         var1 = this.gson;
      } finally {
         ;
      }

      return var1;
   }

   public LifecycleTracker getLifecycleTracker() {
      synchronized(this){}

      LifecycleTracker var1;
      try {
         if(this.lifecycleTracker == null) {
            this.lifecycleTracker = LifecycleTracker.create(this.getSystemNotificationManager(), this.getMetricsStore(), this.getDeDuper(), this.getTimeProvider(), this.getUserUpdateBatcher(), this.getStore(), this.getResetManager(), SystemSettings.getTransitionScale(this.application));
         }

         var1 = this.lifecycleTracker;
      } finally {
         ;
      }

      return var1;
   }

   public MetricTracker getMetricTracker() {
      synchronized(this){}

      MetricTracker var1;
      try {
         if(this.metricTracker == null) {
            var1 = new MetricTracker(this.userIdentity, this.getMetricsStore());
            this.metricTracker = var1;
         }

         var1 = this.metricTracker;
      } finally {
         ;
      }

      return var1;
   }

   public MetricsStore getMetricsStore() {
      synchronized(this){}

      MetricsStore var1;
      try {
         if(this.metricsStore == null) {
            var1 = new MetricsStore(this.getApplication(), this.getApiProvider(), this.appConfigProvider);
            this.metricsStore = var1;
         }

         var1 = this.metricsStore;
      } finally {
         ;
      }

      return var1;
   }

   public NexusClient getNexusClient() {
      synchronized(this){}

      NexusWrapper var1;
      try {
         if(this.nexusClient == null) {
            var1 = new NexusWrapper(LumberMill.getNexusTwig(), this.getBus(), this.getStore(), this.getNexusDebouncePeriod());
            this.nexusClient = var1;
         }

         var1 = this.nexusClient;
      } finally {
         ;
      }

      return var1;
   }

   protected long getNexusDebouncePeriod() {
      return TimeUnit.SECONDS.toMillis(1L);
   }

   public OpsMetricTracker getOpsMetricTracker() {
      synchronized(this){}

      OpsMetricTracker var1;
      try {
         if(this.opsMetricTracker == null) {
            var1 = new OpsMetricTracker(this.getMetricsStore(), this.getTimeProvider());
            this.opsMetricTracker = var1;
         }

         var1 = this.opsMetricTracker;
      } finally {
         ;
      }

      return var1;
   }

   public OverlayManager getOverlayManager() {
      synchronized(this){}

      OverlayManager var1;
      try {
         if(this.overlayManager == null) {
            var1 = new OverlayManager(this.getApplication(), this.getBus(), this.getStore(), this.appConfigProvider, this.getMetricTracker(), this.userIdentity, c.b((Context)this.application));
            this.overlayManager = var1;
         }

         var1 = this.overlayManager;
      } finally {
         ;
      }

      return var1;
   }

   public ResetManager getResetManager() {
      synchronized(this){}

      ResetManager var1;
      try {
         if(this.resetManager == null) {
            var1 = new ResetManager(this.getApiProvider(), this.getUserIdentity(), this.getOverlayManager(), this.appConfigProvider, this.getStore(), this.getUserUpdateBatcher(), this.application, this.activityFinisher);
            this.resetManager = var1;
         }

         var1 = this.resetManager;
      } finally {
         ;
      }

      return var1;
   }

   public Store getStore() {
      synchronized(this){}

      Store var9;
      try {
         if(this.store == null) {
            Provider var2 = this.apiProvider;
            Provider var1 = this.appConfigProvider;
            Provider var6 = this.nexusClientProvider;
            Provider var3 = this.overlayManagerProvider;
            Provider var4 = this.userUpdateBatcherProvider;
            SoundPlayer var5 = new SoundPlayer(this.application, this.appConfigProvider);
            this.store = StoreFactory.createStore(var2, var1, var6, var3, var4, var5, this.userIdentityProvider, this.application, LumberMill.getLogger(), this.getBus());
         }

         var9 = this.store;
      } finally {
         ;
      }

      return var9;
   }

   public SystemNotificationManager getSystemNotificationManager() {
      synchronized(this){}

      SystemNotificationManager var1;
      try {
         if(this.systemNotificationManager == null) {
            NotificationManager var2 = (NotificationManager)this.application.getSystemService("notification");
            var1 = new SystemNotificationManager(var2);
            this.systemNotificationManager = var1;
         }

         var1 = this.systemNotificationManager;
      } finally {
         ;
      }

      return var1;
   }

   public TimeProvider getTimeProvider() {
      return TimeProvider.SYSTEM;
   }

   public UserIdentity getUserIdentity() {
      return this.userIdentity;
   }

   public UserUpdateBatcher getUserUpdateBatcher() {
      synchronized(this){}

      UserUpdateBatcher var1;
      try {
         if(this.userUpdateBatcher == null) {
            this.userUpdateBatcher = UserUpdateBatcher.create(this.getApiProvider(), this.appConfigProvider, this.getStore());
         }

         var1 = this.userUpdateBatcher;
      } finally {
         ;
      }

      return var1;
   }

   public Provider getUserUpdateBatcherProvider() {
      return this.userUpdateBatcherProvider;
   }
}
