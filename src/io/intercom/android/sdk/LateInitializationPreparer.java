package io.intercom.android.sdk;

import android.app.Activity;
import android.app.Application;
import io.intercom.android.sdk.logger.LumberMill;
import io.intercom.android.sdk.twig.Twig;
import io.intercom.android.sdk.utilities.SimpleActivityLifecycleCallbacks;
import java.util.HashSet;
import java.util.Set;

class LateInitializationPreparer extends SimpleActivityLifecycleCallbacks {
   private boolean hasPaused;
   private boolean isRegistered;
   private Activity lastResumedActivity;
   private final Set startedActivities = new HashSet();
   private final Twig twig = LumberMill.getLogger();

   void handlePastLifecycleEvents(Application var1, Injector var2) {
      Activity var5 = this.lastResumedActivity;
      boolean var3 = this.hasPaused;
      Set var4 = this.startedActivities;
      this.unregister(var1);
      if(var5 != null) {
         this.twig.i("Handling lifecycle events for " + var5 + " during late initialisation", new Object[0]);
         LifecycleTracker var6 = var2.getLifecycleTracker();
         var6.onActivityStarted(var5);
         var6.onActivityResumed(var5);
         if(var3) {
            var6.onActivityPaused(var5);
         }
      }

      if(!var4.isEmpty()) {
         var2.getLifecycleTracker().registerActivities(var4);
         this.twig.i("Observed Activities with hashcodes " + var4 + " during late initialization", new Object[0]);
      }

   }

   boolean hasPaused() {
      return this.hasPaused;
   }

   Activity lastResumedActivity() {
      return this.lastResumedActivity;
   }

   public void onActivityPaused(Activity var1) {
      this.startedActivities.add(Integer.valueOf(System.identityHashCode(var1)));
      this.hasPaused = true;
   }

   public void onActivityResumed(Activity var1) {
      this.startedActivities.add(Integer.valueOf(System.identityHashCode(var1)));
      this.lastResumedActivity = var1;
      this.hasPaused = false;
   }

   public void onActivityStarted(Activity var1) {
      this.startedActivities.add(Integer.valueOf(System.identityHashCode(var1)));
   }

   public void onActivityStopped(Activity var1) {
      this.startedActivities.remove(Integer.valueOf(System.identityHashCode(var1)));
      if(var1 == this.lastResumedActivity) {
         this.lastResumedActivity = null;
      }

   }

   void register(Application param1) {
      // $FF: Couldn't be decompiled
   }

   Set startedActivities() {
      return this.startedActivities;
   }

   void unregister(Application var1) {
      this.twig.i("Unregistering for later initialization", new Object[0]);
      var1.unregisterActivityLifecycleCallbacks(this);
      this.isRegistered = false;
      this.lastResumedActivity = null;
      this.hasPaused = false;
   }
}
