package io.intercom.android.sdk.imageloader;

import android.graphics.Bitmap;
import io.intercom.android.sdk.logger.LumberMill;
import io.intercom.android.sdk.twig.Twig;
import io.intercom.com.bumptech.glide.a.a;
import io.intercom.com.bumptech.glide.h.e;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

class LongTermImageDiskCache implements Closeable {
   private static final int APP_VERSION = 1;
   private static final int VALUE_COUNT = 1;
   private final File directory;
   private a diskLruCache;
   private final int maxSize;
   private final LongTermImageDiskCache.SafeKeyGenerator safeKeyGenerator;
   private final Twig twig = LumberMill.getLogger();
   private final LongTermImageDiskCache.DiskCacheWriteLocker writeLocker = new LongTermImageDiskCache.DiskCacheWriteLocker();

   public LongTermImageDiskCache(File var1, int var2) {
      this.directory = var1;
      this.maxSize = var2;
      this.safeKeyGenerator = new LongTermImageDiskCache.SafeKeyGenerator();
   }

   private a getDiskCache() throws IOException {
      synchronized(this){}

      a var1;
      try {
         if(this.diskLruCache == null) {
            this.diskLruCache = a.a(this.directory, 1, 1, (long)this.maxSize);
         }

         var1 = this.diskLruCache;
      } finally {
         ;
      }

      return var1;
   }

   private void resetDiskCache() {
      synchronized(this){}

      try {
         this.diskLruCache = null;
      } finally {
         ;
      }

   }

   public void clear() {
      synchronized(this){}

      try {
         this.getDiskCache().b();
         this.resetDiskCache();
      } catch (IOException var4) {
         this.twig.e(var4.getMessage(), new Object[0]);
      } finally {
         ;
      }

   }

   public void close() throws IOException {
      if(this.diskLruCache != null) {
         this.diskLruCache.close();
      }

   }

   public void delete(String var1) {
      var1 = this.safeKeyGenerator.getSafeKey(var1);

      try {
         this.getDiskCache().c(var1);
      } catch (IOException var2) {
         this.twig.e(var2.getMessage(), new Object[0]);
      }

   }

   public File get(String param1) {
      // $FF: Couldn't be decompiled
   }

   public boolean isClosed() {
      boolean var1;
      if(this.diskLruCache != null && !this.diskLruCache.a()) {
         var1 = false;
      } else {
         var1 = true;
      }

      return var1;
   }

   public void put(String param1, Bitmap param2) {
      // $FF: Couldn't be decompiled
   }

   public void write(File param1, Bitmap param2) {
      // $FF: Couldn't be decompiled
   }

   private static class DiskCacheWriteLocker {
      private final Map locks = new HashMap();
      private final LongTermImageDiskCache.WriteLockPool writeLockPool = new LongTermImageDiskCache.WriteLockPool();

      void acquire(String param1) {
         // $FF: Couldn't be decompiled
      }

      void release(String param1) {
         // $FF: Couldn't be decompiled
      }
   }

   private class WriteLock {
      int interestedThreads;
      final Lock lock = new ReentrantLock();
   }

   private class WriteLockPool {
      private static final int MAX_POOL_SIZE = 10;
      private final Queue pool = new ArrayDeque();

      LongTermImageDiskCache.WriteLock obtain() {
         // $FF: Couldn't be decompiled
      }

      void offer(LongTermImageDiskCache.WriteLock param1) {
         // $FF: Couldn't be decompiled
      }
   }

   private static class SafeKeyGenerator {
      private final e loadIdToSafeHash = new e(1000);

      public String getSafeKey(String param1) {
         // $FF: Couldn't be decompiled
      }
   }
}
