package io.intercom.android.sdk.imageloader;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import io.intercom.android.sdk.logger.LumberMill;
import io.intercom.android.sdk.twig.Twig;
import io.intercom.com.bumptech.glide.i;
import io.intercom.com.bumptech.glide.f.f;
import io.intercom.com.bumptech.glide.f.b.d;
import io.intercom.com.bumptech.glide.load.engine.h;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;

public class LongTermImageLoader implements Closeable {
   private static final int CACHE_SIZE = 26214400;
   private static final Twig twig = LumberMill.getLogger();
   private final LongTermImageDiskCache diskCache;
   private final LongTermImageLoader.Fetcher imageFetcher;

   LongTermImageLoader(LongTermImageDiskCache var1, LongTermImageLoader.Fetcher var2) {
      this.diskCache = var1;
      this.imageFetcher = var2;
   }

   public static LongTermImageLoader newInstance(Context var0) {
      File var1 = new File(var0.getCacheDir(), "intercom-glide");
      if(!var1.exists() && !var1.mkdir()) {
         twig.e("Could not create directory: " + var1.getAbsolutePath(), new Object[0]);
      }

      LongTermImageDiskCache var2 = new LongTermImageDiskCache(var1, 26214400);
      return new LongTermImageLoader(var2, new LongTermImageLoader.Fetcher(var2));
   }

   public void close() throws IOException {
      this.diskCache.close();
   }

   public void loadImage(String var1, LongTermImageLoader.OnImageReadyListener var2, i var3) {
      File var5 = this.diskCache.get(var1);
      LongTermImageLoader.OnImageReadyListener var4 = var2;
      if(var2 == null) {
         var4 = LongTermImageLoader.OnImageReadyListener.NULL;
      }

      if(var5 != null && var5.exists()) {
         this.imageFetcher.loadImageFromFile(var1, var5, var4, var3);
      } else {
         this.imageFetcher.fetchImageFromWeb(var1, var4, var3);
      }

   }

   static class Fetcher {
      final LongTermImageDiskCache diskCache;

      Fetcher(LongTermImageDiskCache var1) {
         this.diskCache = var1;
      }

      void fetchImageFromWeb(final String var1, final LongTermImageLoader.OnImageReadyListener var2, i var3) {
         var3.c().a(var1).a((new f()).b(h.b)).a((io.intercom.com.bumptech.glide.f.a.h)(new io.intercom.com.bumptech.glide.f.a.f(Integer.MIN_VALUE, Integer.MIN_VALUE) {
            public void onResourceReady(Bitmap var1x, d var2x) {
               var2.onImageReady(var1x);
               if(!Fetcher.this.diskCache.isClosed()) {
                  Fetcher.this.diskCache.put(var1, var1x);
               }

            }
         }));
      }

      void loadImageFromFile(final String var1, File var2, final LongTermImageLoader.OnImageReadyListener var3, final i var4) {
         var4.c().a(var2).a((new f()).b(h.b)).a((io.intercom.com.bumptech.glide.f.a.h)(new io.intercom.com.bumptech.glide.f.a.f(Integer.MIN_VALUE, Integer.MIN_VALUE) {
            public void onLoadFailed(Drawable var1x) {
               Fetcher.this.fetchImageFromWeb(var1, var3, var4);
            }

            public void onResourceReady(Bitmap var1x, d var2) {
               var3.onImageReady(var1x);
            }
         }));
      }
   }

   public interface OnImageReadyListener {
      LongTermImageLoader.OnImageReadyListener NULL = new LongTermImageLoader.OnImageReadyListener() {
         public void onImageReady(Bitmap var1) {
         }
      };

      void onImageReady(Bitmap var1);
   }
}
