package io.intercom.android.sdk.imageloader;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Shader.TileMode;
import android.graphics.drawable.BitmapDrawable;
import android.text.TextUtils;
import android.widget.ImageView;
import io.intercom.android.sdk.Provider;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.com.bumptech.glide.i;
import java.io.IOException;

public class WallpaperLoader {
   private final Provider appConfigProvider;
   private final LongTermImageLoader imageLoader;
   private final i requestManager;

   WallpaperLoader(LongTermImageLoader var1, Provider var2, i var3) {
      this.imageLoader = var1;
      this.appConfigProvider = var2;
      this.requestManager = var3;
   }

   public static WallpaperLoader create(Context var0, Provider var1, i var2) {
      return new WallpaperLoader(LongTermImageLoader.newInstance(var0), var1, var2);
   }

   public void close() throws IOException {
      this.imageLoader.close();
   }

   public void loadWallpaperInto(final ImageView var1, final WallpaperLoader.Listener var2) {
      String var3 = ((AppConfig)this.appConfigProvider.get()).getWallpaper();
      if(!TextUtils.isEmpty(var3)) {
         this.imageLoader.loadImage(var3, new LongTermImageLoader.OnImageReadyListener() {
            public void onImageReady(Bitmap var1x) {
               Context var2x = var1.getContext();
               if(var2x != null) {
                  var1x.setDensity(160);
                  BitmapDrawable var3 = new BitmapDrawable(var2x.getResources(), var1x);
                  var3.setTileModeXY(TileMode.REPEAT, TileMode.REPEAT);
                  var1.setImageDrawable(var3);
                  var2.onLoadComplete();
               }

            }
         }, this.requestManager);
      }

   }

   public interface Listener {
      void onLoadComplete();
   }
}
