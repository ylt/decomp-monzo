package io.intercom.android.sdk;

import io.intercom.android.sdk.logger.LumberMill;
import io.intercom.android.sdk.twig.Twig;
import io.intercom.android.sdk.utilities.CustomAttributeValidator;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

public class UserAttributes {
   private static final String COMPANIES = "companies";
   private static final String CUSTOM_ATTRIBUTES = "custom_attributes";
   private static final String EMAIL = "email";
   private static final String LANGUAGE_OVERRIDE = "language_override";
   private static final String NAME = "name";
   private static final String PHONE = "phone";
   private static final String SIGNED_UP_AT = "signed_up_at";
   private static final Twig TWIG = LumberMill.getLogger();
   private static final String UNSUBSCRIBED_FROM_EMAILS = "unsubscribed_from_emails";
   private static final String USER_ID = "user_id";
   private final Map attributes;
   private final List companies;
   private final Map customAttributes;

   UserAttributes(UserAttributes.Builder var1) {
      this.attributes = var1.attributes;
      this.customAttributes = var1.customAttributes;
      this.companies = var1.companies;
   }

   public boolean isEmpty() {
      boolean var1;
      if(this.attributes.isEmpty() && this.customAttributes.isEmpty() && this.companies.isEmpty()) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   Map toMap() {
      if(!this.customAttributes.isEmpty()) {
         this.attributes.put("custom_attributes", this.customAttributes);
      }

      if(!this.companies.isEmpty()) {
         this.attributes.put("companies", this.companies);
      }

      return this.attributes;
   }

   public static final class Builder {
      final Map attributes = new HashMap();
      final List companies = new ArrayList();
      final Map customAttributes = new HashMap();

      public UserAttributes build() {
         return new UserAttributes(this);
      }

      public UserAttributes.Builder withCompany(Company var1) {
         if(var1 == null) {
            UserAttributes.TWIG.w("The company you provided was null", new Object[0]);
         } else {
            this.companies.add(var1.getAttributes());
         }

         return this;
      }

      public UserAttributes.Builder withCustomAttribute(String var1, Object var2) {
         if(var1 == null) {
            UserAttributes.TWIG.w("The key you provided was null for the attribute " + var2, new Object[0]);
         } else if(CustomAttributeValidator.isValid(var2)) {
            this.customAttributes.put(var1, var2);
         } else {
            UserAttributes.TWIG.w("The custom user attribute " + var1 + " was of type " + var2.getClass().getSimpleName() + " We only accept the following types: " + CustomAttributeValidator.getAcceptedTypes(), new Object[0]);
         }

         return this;
      }

      public UserAttributes.Builder withCustomAttributes(Map var1) {
         if(var1 == null) {
            UserAttributes.TWIG.w("The map of attributes you provided was null.", new Object[0]);
         } else {
            Iterator var2 = var1.entrySet().iterator();

            while(var2.hasNext()) {
               Entry var3 = (Entry)var2.next();
               Object var4 = var3.getValue();
               if(CustomAttributeValidator.isValid(var4)) {
                  this.customAttributes.put(var3.getKey(), var4);
               } else {
                  UserAttributes.TWIG.w("The custom user attribute " + (String)var3.getKey() + " was of type " + var4.getClass().getSimpleName() + " We only accept the following types: " + CustomAttributeValidator.getAcceptedTypes(), new Object[0]);
               }
            }
         }

         return this;
      }

      public UserAttributes.Builder withEmail(String var1) {
         this.attributes.put("email", var1);
         return this;
      }

      public UserAttributes.Builder withLanguageOverride(String var1) {
         this.attributes.put("language_override", var1);
         return this;
      }

      public UserAttributes.Builder withName(String var1) {
         this.attributes.put("name", var1);
         return this;
      }

      public UserAttributes.Builder withPhone(String var1) {
         this.attributes.put("phone", var1);
         return this;
      }

      public UserAttributes.Builder withSignedUpAt(Long var1) {
         this.attributes.put("signed_up_at", var1);
         return this;
      }

      public UserAttributes.Builder withSignedUpAt(Date var1) {
         Long var2;
         if(var1 == null) {
            var2 = null;
         } else {
            var2 = Long.valueOf(TimeUnit.MILLISECONDS.toSeconds(var1.getTime()));
         }

         return this.withSignedUpAt(var2);
      }

      public UserAttributes.Builder withUnsubscribedFromEmails(Boolean var1) {
         this.attributes.put("unsubscribed_from_emails", var1);
         return this;
      }

      public UserAttributes.Builder withUserId(String var1) {
         this.attributes.put("user_id", var1);
         return this;
      }
   }
}
