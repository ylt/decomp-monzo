package io.intercom.android.sdk.twig;

import android.util.Log;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class Twig {
   public static final int DISABLED = 8;
   private static final int MAX_LOG_LENGTH = 4000;
   private final boolean internalLoggingEnabled;
   private int logLevel;
   private final String tag;

   public Twig(int var1, String var2, boolean var3) {
      this.logLevel = var1;
      if(var2 == null) {
         var2 = "Twig";
      }

      this.tag = var2;
      this.internalLoggingEnabled = var3;
   }

   private String getStackTraceString(Throwable var1) {
      StringWriter var2 = new StringWriter(256);
      PrintWriter var3 = new PrintWriter(var2, false);
      var1.printStackTrace(var3);
      var3.flush();
      return var2.toString();
   }

   private void log(int var1, String var2, String var3) {
      if(var3.length() < 4000) {
         this.printLog(var1, var2, var3);
      } else {
         int var4 = 0;

         int var7;
         for(int var6 = var3.length(); var4 < var6; var4 = var7 + 1) {
            int var5 = var3.indexOf(10, var4);
            if(var5 == -1) {
               var5 = var6;
            }

            while(true) {
               var7 = Math.min(var5, var4 + 4000);
               this.printLog(var1, var2, var3.substring(var4, var7));
               if(var7 >= var5) {
                  break;
               }

               var4 = var7;
            }
         }
      }

   }

   private void prepareLog(int var1, Throwable var2, String var3, Object... var4) {
      if(var1 >= this.logLevel) {
         String var6 = this.getTag();
         String var5;
         if(var3 != null && var3.length() == 0) {
            var5 = null;
         } else {
            var5 = var3;
         }

         String var7;
         if(var5 == null) {
            if(var2 == null) {
               return;
            }

            var7 = this.getStackTraceString(var2);
         } else {
            var3 = var5;
            if(var4.length > 0) {
               var3 = String.format(var5, var4);
            }

            var7 = var3;
            if(var2 != null) {
               var7 = var3 + "\n" + this.getStackTraceString(var2);
            }
         }

         this.log(var1, var6, var7);
      }

   }

   private void printLog(int var1, String var2, String var3) {
      if(var1 == 7) {
         Log.wtf(var2, var3);
      } else {
         Log.println(var1, var2, var3);
      }

   }

   public void d(String var1, Object... var2) {
      this.prepareLog(3, (Throwable)null, var1, var2);
   }

   public void d(Throwable var1) {
      this.prepareLog(3, var1, (String)null, new Object[0]);
   }

   public void d(Throwable var1, String var2, Object... var3) {
      this.prepareLog(3, var1, var2, var3);
   }

   public void e(String var1, Object... var2) {
      this.prepareLog(6, (Throwable)null, var1, var2);
   }

   public void e(Throwable var1) {
      this.prepareLog(6, var1, (String)null, new Object[0]);
   }

   public void e(Throwable var1, String var2, Object... var3) {
      this.prepareLog(6, var1, var2, var3);
   }

   int getLogLevel() {
      return this.logLevel;
   }

   public String getTag() {
      return this.tag;
   }

   public void i(String var1, Object... var2) {
      this.prepareLog(4, (Throwable)null, var1, var2);
   }

   public void i(Throwable var1) {
      this.prepareLog(4, var1, (String)null, new Object[0]);
   }

   public void i(Throwable var1, String var2, Object... var3) {
      this.prepareLog(4, var1, var2, var3);
   }

   public void internal(String var1) {
      this.internal(this.tag, var1);
   }

   public void internal(String var1, String var2) {
      if(this.internalLoggingEnabled) {
         Log.d(var1, "INTERNAL: " + var2);
      }

   }

   void log(int var1, String var2, Object... var3) {
      this.prepareLog(var1, (Throwable)null, var2, var3);
   }

   public void setLogLevel(int var1) {
      this.logLevel = var1;
   }

   public void v(String var1, Object... var2) {
      this.prepareLog(2, (Throwable)null, var1, var2);
   }

   public void v(Throwable var1) {
      this.prepareLog(2, var1, (String)null, new Object[0]);
   }

   public void v(Throwable var1, String var2, Object... var3) {
      this.prepareLog(2, var1, var2, var3);
   }

   public void w(String var1, Object... var2) {
      this.prepareLog(5, (Throwable)null, var1, var2);
   }

   public void w(Throwable var1) {
      this.prepareLog(5, var1, (String)null, new Object[0]);
   }

   public void w(Throwable var1, String var2, Object... var3) {
      this.prepareLog(5, var1, var2, var3);
   }

   public void wtf(String var1, Object... var2) {
      this.prepareLog(7, (Throwable)null, var1, var2);
   }

   public void wtf(Throwable var1) {
      this.prepareLog(7, var1, (String)null, new Object[0]);
   }

   public void wtf(Throwable var1, String var2, Object... var3) {
      this.prepareLog(7, var1, var2, var3);
   }

   @Retention(RetentionPolicy.SOURCE)
   public @interface LogLevel {
   }
}
