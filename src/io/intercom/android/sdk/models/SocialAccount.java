package io.intercom.android.sdk.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import io.intercom.android.sdk.utilities.NullSafety;

public abstract class SocialAccount implements Parcelable {
   public static final Creator CREATOR = new Creator() {
      public SocialAccount createFromParcel(Parcel var1) {
         return SocialAccount.create(var1.readString(), var1.readString());
      }

      public SocialAccount[] newArray(int var1) {
         return new SocialAccount[var1];
      }
   };
   public static final SocialAccount NULL = create("", "");

   public static SocialAccount create(String var0, String var1) {
      return new AutoValue_SocialAccount(var0, var1);
   }

   public int describeContents() {
      return 0;
   }

   public abstract String getProfileUrl();

   public abstract String getProvider();

   public void writeToParcel(Parcel var1, int var2) {
      var1.writeString(this.getProvider());
      var1.writeString(this.getProfileUrl());
   }

   public static final class Builder {
      String profile_url;
      String provider;

      public SocialAccount build() {
         return SocialAccount.create(NullSafety.valueOrEmpty(this.provider), NullSafety.valueOrEmpty(this.profile_url));
      }
   }
}
