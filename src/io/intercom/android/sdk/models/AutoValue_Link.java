package io.intercom.android.sdk.models;

import java.util.List;

final class AutoValue_Link extends Link {
   private final List blocks;
   private final Card card;
   private final long createdAt;
   private final String id;
   private final ReactionReply reactionReply;
   private final long updatedAt;

   AutoValue_Link(String var1, Card var2, List var3, ReactionReply var4, long var5, long var7) {
      if(var1 == null) {
         throw new NullPointerException("Null id");
      } else {
         this.id = var1;
         if(var2 == null) {
            throw new NullPointerException("Null card");
         } else {
            this.card = var2;
            if(var3 == null) {
               throw new NullPointerException("Null blocks");
            } else {
               this.blocks = var3;
               if(var4 == null) {
                  throw new NullPointerException("Null reactionReply");
               } else {
                  this.reactionReply = var4;
                  this.createdAt = var5;
                  this.updatedAt = var7;
               }
            }
         }
      }
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(var1 != this) {
         if(var1 instanceof Link) {
            Link var3 = (Link)var1;
            if(!this.id.equals(var3.getId()) || !this.card.equals(var3.getCard()) || !this.blocks.equals(var3.getBlocks()) || !this.reactionReply.equals(var3.getReactionReply()) || this.createdAt != var3.getCreatedAt() || this.updatedAt != var3.getUpdatedAt()) {
               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public List getBlocks() {
      return this.blocks;
   }

   public Card getCard() {
      return this.card;
   }

   public long getCreatedAt() {
      return this.createdAt;
   }

   public String getId() {
      return this.id;
   }

   public ReactionReply getReactionReply() {
      return this.reactionReply;
   }

   public long getUpdatedAt() {
      return this.updatedAt;
   }

   public int hashCode() {
      return (int)((long)((int)((long)(((((this.id.hashCode() ^ 1000003) * 1000003 ^ this.card.hashCode()) * 1000003 ^ this.blocks.hashCode()) * 1000003 ^ this.reactionReply.hashCode()) * 1000003) ^ this.createdAt >>> 32 ^ this.createdAt) * 1000003) ^ this.updatedAt >>> 32 ^ this.updatedAt);
   }

   public String toString() {
      return "Link{id=" + this.id + ", card=" + this.card + ", blocks=" + this.blocks + ", reactionReply=" + this.reactionReply + ", createdAt=" + this.createdAt + ", updatedAt=" + this.updatedAt + "}";
   }
}
