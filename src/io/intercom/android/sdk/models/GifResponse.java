package io.intercom.android.sdk.models;

import java.util.List;

public class GifResponse {
   private final List results;

   GifResponse(List var1) {
      this.results = var1;
   }

   public List results() {
      return this.results;
   }

   public static final class Builder {
      List results;

      public GifResponse build() {
         return new GifResponse(this.results);
      }
   }
}
