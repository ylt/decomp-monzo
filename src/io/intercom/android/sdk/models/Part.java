package io.intercom.android.sdk.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import com.intercom.input.gallery.c;
import io.intercom.android.sdk.blocks.BlockType;
import io.intercom.android.sdk.blocks.models.Block;
import io.intercom.android.sdk.utilities.NullSafety;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class Part implements Parcelable {
   public static final String ADMIN_IS_TYPING_STYLE = "admin_is_typing_style";
   public static final String CHAT_MESSAGE_STYLE = "chat";
   public static final Creator CREATOR = new Creator() {
      public Part createFromParcel(Parcel var1) {
         return new Part(var1, null);
      }

      public Part[] newArray(int var1) {
         return new Part[var1];
      }
   };
   public static final String DAY_DIVIDER_STYLE = "day_divider_style";
   public static final String LEGACY_ANNOUNCEMENT_STYLE = "announcement";
   public static final String LEGACY_SMALL_ANNOUNCEMENT_STYLE = "small-announcement";
   public static final String LOADING_LAYOUT = "loading_layout_style";
   public static final String NOTE_MESSAGE_STYLE = "note";
   public static final Part NULL = new Part(new Part.Builder());
   public static final String POST_MESSAGE_STYLE = "post";
   private final List attachments;
   private final List blocks;
   private final long createdAt;
   private final Part.DeliveryOption deliveryOption;
   private boolean displayDelivered;
   private boolean entranceAnimation;
   private final EventData eventData;
   private final String id;
   private final boolean isInitialMessage;
   private Part.MessageState messageState;
   private final String messageStyle;
   private Participant participant;
   private String participantId;
   private final boolean participantIsAdmin;
   private final ReactionReply reactionReply;
   private String seenByAdmin;
   private final String summary;
   private c uploadImage;

   public Part() {
      this(new Part.Builder());
   }

   private Part(Parcel var1) {
      boolean var4 = true;
      super();
      this.id = var1.readString();
      this.participantId = var1.readString();
      boolean var3;
      if(var1.readByte() != 0) {
         var3 = true;
      } else {
         var3 = false;
      }

      this.participantIsAdmin = var3;
      this.blocks = new ArrayList();
      var1.readList(this.blocks, Block.class.getClassLoader());
      this.attachments = new ArrayList();
      var1.readList(this.attachments, Attachments.class.getClassLoader());
      this.messageStyle = var1.readString();
      this.createdAt = var1.readLong();
      this.summary = var1.readString();
      boolean var2;
      if(var1.readByte() == 0) {
         var2 = true;
      } else {
         var2 = false;
      }

      if(var2) {
         this.reactionReply = ReactionReply.NULL;
      } else {
         this.reactionReply = (ReactionReply)var1.readValue(ReactionReply.class.getClassLoader());
      }

      this.seenByAdmin = var1.readString();
      this.participant = (Participant)var1.readValue(Participant.class.getClassLoader());
      this.eventData = (EventData)var1.readValue(EventData.class.getClassLoader());
      if(var1.readByte() != 0) {
         var3 = var4;
      } else {
         var3 = false;
      }

      this.isInitialMessage = var3;
      this.deliveryOption = convertDeliveryOption(var1.readString());
   }

   // $FF: synthetic method
   Part(Parcel var1, Object var2) {
      this(var1);
   }

   private Part(Part.Builder var1) {
      String var2;
      if(var1.id == null) {
         var2 = UUID.randomUUID().toString();
      } else {
         var2 = var1.id;
      }

      this.id = var2;
      this.participantId = NullSafety.valueOrEmpty(var1.participant_id);
      this.participantIsAdmin = var1.participant_is_admin;
      this.summary = NullSafety.valueOrEmpty(var1.summary);
      this.createdAt = var1.created_at;
      this.messageStyle = convertLegacyMessageStyle(var1.message_style);
      this.deliveryOption = convertDeliveryOption(var1.delivery_option);
      this.blocks = new ArrayList();
      Iterator var4;
      if(var1.body != null) {
         var4 = var1.body.iterator();

         while(var4.hasNext()) {
            Block.Builder var3 = (Block.Builder)var4.next();
            this.blocks.add(var3.build());
         }
      }

      ReactionReply var5;
      if(var1.reactions_reply == null) {
         var5 = ReactionReply.NULL;
      } else {
         var5 = var1.reactions_reply.build();
      }

      this.reactionReply = var5;
      if(var1.seen_by_admin == null) {
         var2 = "hide";
      } else {
         var2 = var1.seen_by_admin;
      }

      this.seenByAdmin = var2;
      this.attachments = new ArrayList();
      if(var1.attachments != null) {
         var4 = var1.attachments.iterator();

         while(var4.hasNext()) {
            Attachments.Builder var6 = (Attachments.Builder)var4.next();
            this.attachments.add(var6.build());
         }
      }

      EventData var7;
      if(var1.event_data == null) {
         var7 = EventData.NULL;
      } else {
         var7 = var1.event_data.build();
      }

      this.eventData = var7;
      this.participant = Participant.NULL;
      this.displayDelivered = false;
      this.entranceAnimation = false;
      this.isInitialMessage = var1.is_initial_message;
      this.messageState = Part.MessageState.NORMAL;
   }

   // $FF: synthetic method
   Part(Part.Builder var1, Object var2) {
      this(var1);
   }

   private static Part.DeliveryOption convertDeliveryOption(String var0) {
      Part.DeliveryOption var2;
      if(var0 == null) {
         var2 = Part.DeliveryOption.SUMMARY;
      } else {
         try {
            var2 = Part.DeliveryOption.valueOf(var0.toUpperCase());
         } catch (IllegalArgumentException var1) {
            var2 = Part.DeliveryOption.SUMMARY;
         }
      }

      return var2;
   }

   static String convertLegacyMessageStyle(String var0) {
      if("announcement".equals(var0)) {
         var0 = "post";
      } else if("small-announcement".equals(var0)) {
         var0 = "note";
      } else if("admin_is_typing_style".equals(var0)) {
         var0 = "admin_is_typing_style";
      } else if("day_divider_style".equals(var0)) {
         var0 = "day_divider_style";
      } else if("loading_layout_style".equals(var0)) {
         var0 = "loading_layout_style";
      } else {
         var0 = "chat";
      }

      return var0;
   }

   private static boolean nextPartFromSameParticipant(Part var0, Part var1) {
      return var0.getParticipantId().equals(var1.getParticipantId());
   }

   public static boolean shouldConcatenate(Part var0, Part var1) {
      boolean var3 = false;
      boolean var2 = var3;
      if(!"day_divider_style".equals(var0.getMessageStyle())) {
         var2 = var3;
         if(!var0.isLinkCard()) {
            var2 = var3;
            if(!var0.isEvent().booleanValue()) {
               if(var1.isEvent().booleanValue()) {
                  var2 = var3;
               } else {
                  var2 = var3;
                  if(Math.abs(var1.getCreatedAt() - var0.getCreatedAt()) < TimeUnit.MINUTES.toSeconds(3L)) {
                     var2 = var3;
                     if(nextPartFromSameParticipant(var0, var1)) {
                        var2 = var3;
                        if(var1.getCreatedAt() != 0L) {
                           var2 = true;
                        }
                     }
                  }
               }
            }
         }
      }

      return var2;
   }

   public int describeContents() {
      return 0;
   }

   public boolean equals(Object var1) {
      boolean var2;
      if(var1 instanceof Part) {
         Part var3 = (Part)var1;
         var2 = this.id.equals(var3.id);
      } else {
         var2 = false;
      }

      return var2;
   }

   public List getAttachments() {
      return this.attachments;
   }

   public List getBlocks() {
      return this.blocks;
   }

   public long getCreatedAt() {
      return this.createdAt;
   }

   public Part.DeliveryOption getDeliveryOption() {
      return this.deliveryOption;
   }

   public EventData getEventData() {
      return this.eventData;
   }

   public String getId() {
      return this.id;
   }

   public Block getLinkBlock() {
      return (Block)this.blocks.get(0);
   }

   public Part.MessageState getMessageState() {
      return this.messageState;
   }

   public String getMessageStyle() {
      return this.messageStyle;
   }

   public Participant getParticipant() {
      return this.participant;
   }

   public String getParticipantId() {
      return this.participantId;
   }

   public ReactionReply getReactionReply() {
      return this.reactionReply;
   }

   public String getSeenByAdmin() {
      String var1;
      if(this.participantIsAdmin) {
         var1 = "hide";
      } else {
         var1 = this.seenByAdmin;
      }

      return var1;
   }

   public String getSummary() {
      return this.summary;
   }

   public c getUpload() {
      return this.uploadImage;
   }

   public boolean hasAttachments() {
      boolean var1;
      if(!this.attachments.isEmpty()) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public boolean hasEntranceAnimation() {
      return this.entranceAnimation;
   }

   public int hashCode() {
      return this.id.hashCode();
   }

   public boolean isAdmin() {
      return this.participantIsAdmin;
   }

   public boolean isDisplayDelivered() {
      return this.displayDelivered;
   }

   public Boolean isEvent() {
      boolean var1;
      if(!this.eventData.getEventAsPlainText().isEmpty()) {
         var1 = true;
      } else {
         var1 = false;
      }

      return Boolean.valueOf(var1);
   }

   public boolean isGifOnlyPart() {
      boolean var1;
      if(this.blocks.size() == 1 && !TextUtils.isEmpty(((Block)this.blocks.get(0)).getAttribution())) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public boolean isInitialMessage() {
      return this.isInitialMessage;
   }

   public boolean isLinkCard() {
      boolean var1;
      if(this.isSingleBlockPartOfType(BlockType.LINK) && "chat".equals(this.messageStyle)) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public boolean isLinkList() {
      boolean var1;
      if(this.isSingleBlockPartOfType(BlockType.LINKLIST) && "chat".equals(this.messageStyle)) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public boolean isMessagePart() {
      boolean var1;
      if(!"post".equals(this.messageStyle) && !"note".equals(this.messageStyle) && !"chat".equals(this.messageStyle)) {
         var1 = false;
      } else {
         var1 = true;
      }

      return var1;
   }

   public boolean isReply() {
      boolean var1;
      if(!this.isInitialMessage) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public boolean isSingleBlockPartOfType(BlockType var1) {
      boolean var2;
      if(this.blocks.size() == 1 && ((Block)this.blocks.get(0)).getType() == var1) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   public boolean isUser() {
      boolean var1;
      if(!this.isAdmin()) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public void setDisplayDelivered(boolean var1) {
      this.displayDelivered = var1;
   }

   public void setEntranceAnimation(boolean var1) {
      this.entranceAnimation = var1;
   }

   public void setMessageState(Part.MessageState var1) {
      this.messageState = var1;
   }

   public void setParticipant(Participant var1) {
      this.participant = var1;
      this.participantId = var1.getId();
   }

   public void setSeenByAdmin(String var1) {
      this.seenByAdmin = var1;
   }

   public void setUpload(c var1) {
      this.uploadImage = var1;
   }

   public void writeToParcel(Parcel var1, int var2) {
      byte var3 = 1;
      var1.writeString(this.id);
      var1.writeString(this.participantId);
      byte var4;
      if(this.participantIsAdmin) {
         var4 = 1;
      } else {
         var4 = 0;
      }

      var1.writeByte((byte)var4);
      var1.writeList(this.blocks);
      var1.writeList(this.attachments);
      var1.writeString(this.messageStyle);
      var1.writeLong(this.createdAt);
      var1.writeString(this.summary);
      if(ReactionReply.isNull(this.reactionReply)) {
         var1.writeByte(0);
      } else {
         var1.writeByte(1);
         var1.writeValue(this.reactionReply);
      }

      var1.writeString(this.seenByAdmin);
      var1.writeValue(this.participant);
      var1.writeValue(this.eventData);
      if(this.isInitialMessage) {
         var4 = var3;
      } else {
         var4 = 0;
      }

      var1.writeByte((byte)var4);
      var1.writeString(this.deliveryOption.name());
   }

   public static final class Builder {
      List attachments;
      List body;
      long created_at;
      String delivery_option;
      EventData.Builder event_data;
      String id;
      boolean is_initial_message;
      String message_style;
      String participant_id;
      boolean participant_is_admin;
      ReactionReply.Builder reactions_reply;
      String seen_by_admin;
      String summary;

      public Part build() {
         return new Part(this, null);
      }

      public Part.Builder withBlocks(List var1) {
         this.body = var1;
         return this;
      }

      public Part.Builder withCreatedAt(long var1) {
         this.created_at = var1;
         return this;
      }

      public Part.Builder withParticipantIsAdmin(boolean var1) {
         this.participant_is_admin = var1;
         return this;
      }

      public Part.Builder withStyle(String var1) {
         this.message_style = var1;
         return this;
      }
   }

   public static enum DeliveryOption {
      BADGE,
      FULL,
      SUMMARY;
   }

   public static enum MessageState {
      FAILED,
      NORMAL,
      SENDING,
      UPLOAD_FAILED;
   }
}
