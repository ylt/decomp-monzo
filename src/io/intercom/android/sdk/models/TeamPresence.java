package io.intercom.android.sdk.models;

import io.intercom.android.sdk.commons.utilities.CollectionUtils;
import io.intercom.android.sdk.utilities.NullSafety;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public abstract class TeamPresence {
   public static TeamPresence create(List var0, String var1, String var2) {
      return new AutoValue_TeamPresence(var0, var1, var2);
   }

   public abstract List getActiveAdmins();

   public abstract String getExpectedResponseDelay();

   public abstract String getOfficeHours();

   public boolean isEmpty() {
      boolean var1;
      if(this.getActiveAdmins().isEmpty() && this.getExpectedResponseDelay().isEmpty()) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public static final class Builder {
      List active_admins;
      String expected_response_delay;
      String office_hours;

      public TeamPresence build() {
         ArrayList var3 = new ArrayList(CollectionUtils.capacityFor(this.active_admins));
         if(this.active_admins != null) {
            Iterator var2 = this.active_admins.iterator();

            while(var2.hasNext()) {
               Participant.Builder var1 = (Participant.Builder)var2.next();
               if(var1 != null) {
                  var3.add(var1.build());
               }
            }
         }

         return TeamPresence.create(var3, NullSafety.valueOrEmpty(this.expected_response_delay), NullSafety.valueOrEmpty(this.office_hours));
      }
   }
}
