package io.intercom.android.sdk.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import io.intercom.android.sdk.utilities.NullSafety;

public class EventParticipant implements Parcelable {
   public static final Creator CREATOR = new Creator() {
      public EventParticipant createFromParcel(Parcel var1) {
         return new EventParticipant(var1);
      }

      public EventParticipant[] newArray(int var1) {
         return new EventParticipant[var1];
      }
   };
   public static final EventParticipant NULL = new EventParticipant(new EventParticipant.Builder());
   private final Avatar avatar;
   private final String id;
   private final String initial;
   private final String label;
   private final String type;

   protected EventParticipant(Parcel var1) {
      this.id = var1.readString();
      this.initial = var1.readString();
      this.label = var1.readString();
      this.type = var1.readString();
      this.avatar = (Avatar)var1.readValue(Avatar.class.getClassLoader());
   }

   private EventParticipant(EventParticipant.Builder var1) {
      this.id = NullSafety.valueOrEmpty(var1.id);
      this.initial = NullSafety.valueOrEmpty(var1.initial);
      this.label = NullSafety.valueOrEmpty(var1.label);
      this.type = NullSafety.valueOrEmpty(var1.type);
      Avatar var2;
      if(var1.avatar == null) {
         var2 = (new Avatar.Builder()).build();
      } else {
         var2 = var1.avatar.build();
      }

      this.avatar = var2;
   }

   // $FF: synthetic method
   EventParticipant(EventParticipant.Builder var1, Object var2) {
      this(var1);
   }

   public int describeContents() {
      return 0;
   }

   public Avatar getAvatar() {
      return this.avatar;
   }

   public String getId() {
      return this.id;
   }

   public String getInitial() {
      return this.initial;
   }

   public String getLabel() {
      return this.label;
   }

   public String getType() {
      return this.type;
   }

   public void writeToParcel(Parcel var1, int var2) {
      var1.writeString(this.id);
      var1.writeString(this.initial);
      var1.writeString(this.label);
      var1.writeString(this.type);
      var1.writeValue(this.avatar);
   }

   public static final class Builder {
      Avatar.Builder avatar;
      String id;
      String initial;
      String label;
      String type;

      public EventParticipant build() {
         return new EventParticipant(this, null);
      }
   }
}
