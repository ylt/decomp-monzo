package io.intercom.android.sdk.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import io.intercom.android.sdk.utilities.NullSafety;

public abstract class Reaction implements Parcelable {
   public static final Creator CREATOR = new Creator() {
      public Reaction createFromParcel(Parcel var1) {
         return Reaction.create(var1.readInt(), var1.readString());
      }

      public Reaction[] newArray(int var1) {
         return new Reaction[var1];
      }
   };

   public static Reaction create(int var0, String var1) {
      return new AutoValue_Reaction(var0, var1);
   }

   public int describeContents() {
      return 0;
   }

   public abstract String getImageUrl();

   public abstract int getIndex();

   public void writeToParcel(Parcel var1, int var2) {
      var1.writeInt(this.getIndex());
      var1.writeString(this.getImageUrl());
   }

   public static final class Builder {
      String image_url;
      int index;

      public Reaction build() {
         return Reaction.create(this.index, NullSafety.valueOrEmpty(this.image_url));
      }
   }
}
