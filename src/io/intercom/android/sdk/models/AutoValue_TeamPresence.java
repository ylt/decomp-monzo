package io.intercom.android.sdk.models;

import java.util.List;

final class AutoValue_TeamPresence extends TeamPresence {
   private final List activeAdmins;
   private final String expectedResponseDelay;
   private final String officeHours;

   AutoValue_TeamPresence(List var1, String var2, String var3) {
      if(var1 == null) {
         throw new NullPointerException("Null activeAdmins");
      } else {
         this.activeAdmins = var1;
         if(var2 == null) {
            throw new NullPointerException("Null expectedResponseDelay");
         } else {
            this.expectedResponseDelay = var2;
            if(var3 == null) {
               throw new NullPointerException("Null officeHours");
            } else {
               this.officeHours = var3;
            }
         }
      }
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(var1 != this) {
         if(var1 instanceof TeamPresence) {
            TeamPresence var3 = (TeamPresence)var1;
            if(!this.activeAdmins.equals(var3.getActiveAdmins()) || !this.expectedResponseDelay.equals(var3.getExpectedResponseDelay()) || !this.officeHours.equals(var3.getOfficeHours())) {
               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public List getActiveAdmins() {
      return this.activeAdmins;
   }

   public String getExpectedResponseDelay() {
      return this.expectedResponseDelay;
   }

   public String getOfficeHours() {
      return this.officeHours;
   }

   public int hashCode() {
      return ((this.activeAdmins.hashCode() ^ 1000003) * 1000003 ^ this.expectedResponseDelay.hashCode()) * 1000003 ^ this.officeHours.hashCode();
   }

   public String toString() {
      return "TeamPresence{activeAdmins=" + this.activeAdmins + ", expectedResponseDelay=" + this.expectedResponseDelay + ", officeHours=" + this.officeHours + "}";
   }
}
