package io.intercom.android.sdk.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class EventData implements Parcelable {
   public static final Creator CREATOR = new Creator() {
      public EventData createFromParcel(Parcel var1) {
         return new EventData(var1);
      }

      public EventData[] newArray(int var1) {
         return new EventData[var1];
      }
   };
   public static final EventData NULL = new EventData(new EventData.Builder());
   private final String eventAsPlainText;
   private final EventParticipant participant;

   protected EventData(Parcel var1) {
      this.participant = (EventParticipant)var1.readValue(EventParticipant.class.getClassLoader());
      this.eventAsPlainText = var1.readString();
   }

   private EventData(EventData.Builder var1) {
      EventParticipant var2;
      if(var1.participant == null) {
         var2 = EventParticipant.NULL;
      } else {
         var2 = var1.participant.build();
      }

      this.participant = var2;
      String var3;
      if(var1.eventAsPlainText == null) {
         var3 = "";
      } else {
         var3 = var1.eventAsPlainText;
      }

      this.eventAsPlainText = var3;
   }

   // $FF: synthetic method
   EventData(EventData.Builder var1, Object var2) {
      this(var1);
   }

   public int describeContents() {
      return 0;
   }

   public String getEventAsPlainText() {
      return this.eventAsPlainText;
   }

   public EventParticipant getParticipant() {
      return this.participant;
   }

   public void writeToParcel(Parcel var1, int var2) {
      var1.writeValue(this.participant);
      var1.writeString(this.eventAsPlainText);
   }

   public static final class Builder {
      String eventAsPlainText;
      EventParticipant.Builder participant;

      public EventData build() {
         return new EventData(this, null);
      }
   }
}
