package io.intercom.android.sdk.models;

final class AutoValue_Participant extends Participant {
   private final Avatar avatar;
   private final String email;
   private final String id;
   private final String name;
   private final String type;

   AutoValue_Participant(String var1, String var2, String var3, String var4, Avatar var5) {
      if(var1 == null) {
         throw new NullPointerException("Null id");
      } else {
         this.id = var1;
         if(var2 == null) {
            throw new NullPointerException("Null name");
         } else {
            this.name = var2;
            if(var3 == null) {
               throw new NullPointerException("Null type");
            } else {
               this.type = var3;
               if(var4 == null) {
                  throw new NullPointerException("Null email");
               } else {
                  this.email = var4;
                  if(var5 == null) {
                     throw new NullPointerException("Null avatar");
                  } else {
                     this.avatar = var5;
                  }
               }
            }
         }
      }
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(var1 != this) {
         if(var1 instanceof Participant) {
            Participant var3 = (Participant)var1;
            if(!this.id.equals(var3.getId()) || !this.name.equals(var3.getName()) || !this.type.equals(var3.getType()) || !this.email.equals(var3.getEmail()) || !this.avatar.equals(var3.getAvatar())) {
               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public Avatar getAvatar() {
      return this.avatar;
   }

   public String getEmail() {
      return this.email;
   }

   public String getId() {
      return this.id;
   }

   public String getName() {
      return this.name;
   }

   public String getType() {
      return this.type;
   }

   public int hashCode() {
      return ((((this.id.hashCode() ^ 1000003) * 1000003 ^ this.name.hashCode()) * 1000003 ^ this.type.hashCode()) * 1000003 ^ this.email.hashCode()) * 1000003 ^ this.avatar.hashCode();
   }

   public String toString() {
      return "Participant{id=" + this.id + ", name=" + this.name + ", type=" + this.type + ", email=" + this.email + ", avatar=" + this.avatar + "}";
   }
}
