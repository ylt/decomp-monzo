package io.intercom.android.sdk.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import io.intercom.android.sdk.utilities.NullSafety;
import java.util.Iterator;
import java.util.List;

public class LastParticipatingAdmin implements Parcelable {
   public static final Creator CREATOR = new Creator() {
      public LastParticipatingAdmin createFromParcel(Parcel var1) {
         return new LastParticipatingAdmin(var1);
      }

      public LastParticipatingAdmin[] newArray(int var1) {
         return new LastParticipatingAdmin[var1];
      }
   };
   private static final String LINKED_IN = "linkedin";
   public static final LastParticipatingAdmin NONE = new LastParticipatingAdmin(new LastParticipatingAdmin.Builder());
   public static final LastParticipatingAdmin NULL = new LastParticipatingAdmin(new LastParticipatingAdmin.Builder());
   private static final String TWITTER = "twitter";
   private final Avatar avatar;
   private final String firstName;
   private final String intro;
   private final boolean isActive;
   private final String jobTitle;
   private final long lastActiveAt;
   private final SocialAccount linkedIn;
   private final Location location;
   private final SocialAccount twitter;

   LastParticipatingAdmin(Parcel var1) {
      this.lastActiveAt = var1.readLong();
      boolean var2;
      if(var1.readByte() != 0) {
         var2 = true;
      } else {
         var2 = false;
      }

      this.isActive = var2;
      this.avatar = (Avatar)var1.readValue(Avatar.class.getClassLoader());
      this.firstName = var1.readString();
      this.intro = var1.readString();
      this.jobTitle = var1.readString();
      this.location = (Location)var1.readValue(Location.class.getClassLoader());
      this.twitter = (SocialAccount)var1.readValue(SocialAccount.class.getClassLoader());
      this.linkedIn = (SocialAccount)var1.readValue(SocialAccount.class.getClassLoader());
   }

   public LastParticipatingAdmin(LastParticipatingAdmin.Builder var1) {
      Avatar var2;
      if(var1.avatar == null) {
         var2 = (new Avatar.Builder()).build();
      } else {
         var2 = var1.avatar.build();
      }

      this.avatar = var2;
      this.firstName = NullSafety.valueOrEmpty(var1.first_name);
      this.intro = NullSafety.valueOrEmpty(var1.intro);
      this.jobTitle = NullSafety.valueOrEmpty(var1.job_title);
      Location var7;
      if(var1.location == null) {
         var7 = (new Location.Builder()).build();
      } else {
         var7 = var1.location.build();
      }

      this.location = var7;
      this.lastActiveAt = var1.last_active_at;
      this.isActive = var1.is_active;
      SocialAccount var3 = SocialAccount.NULL;
      SocialAccount var8 = SocialAccount.NULL;
      SocialAccount var4;
      if(var1.social_accounts != null) {
         Iterator var5 = var1.social_accounts.iterator();
         SocialAccount var6 = var8;
         var8 = var3;

         while(true) {
            var4 = var6;
            var3 = var8;
            if(!var5.hasNext()) {
               break;
            }

            var3 = ((SocialAccount.Builder)var5.next()).build();
            if("twitter".equals(var3.getProvider())) {
               var8 = var3;
            } else if("linkedin".equals(var3.getProvider())) {
               var6 = var3;
            }
         }
      } else {
         var4 = var8;
      }

      this.twitter = var3;
      this.linkedIn = var4;
   }

   public static boolean isNull(LastParticipatingAdmin var0) {
      boolean var1;
      if(var0 == NONE || !NULL.equals(var0) && var0 != null) {
         var1 = false;
      } else {
         var1 = true;
      }

      return var1;
   }

   public int describeContents() {
      return 0;
   }

   public Avatar getAvatar() {
      return this.avatar;
   }

   public String getFirstName() {
      return this.firstName;
   }

   public String getIntro() {
      return this.intro;
   }

   public String getJobTitle() {
      return this.jobTitle;
   }

   public long getLastActiveAt() {
      return this.lastActiveAt;
   }

   public SocialAccount getLinkedIn() {
      return this.linkedIn;
   }

   public Location getLocation() {
      return this.location;
   }

   public SocialAccount getTwitter() {
      return this.twitter;
   }

   public boolean isActive() {
      return this.isActive;
   }

   public void writeToParcel(Parcel var1, int var2) {
      var1.writeLong(this.lastActiveAt);
      byte var3;
      if(this.isActive) {
         var3 = 1;
      } else {
         var3 = 0;
      }

      var1.writeByte((byte)var3);
      var1.writeValue(this.avatar);
      var1.writeString(this.firstName);
      var1.writeString(this.intro);
      var1.writeString(this.jobTitle);
      var1.writeValue(this.location);
      var1.writeValue(this.twitter);
      var1.writeValue(this.linkedIn);
   }

   public static final class Builder {
      Avatar.Builder avatar;
      String first_name;
      String intro;
      boolean is_active;
      String job_title;
      long last_active_at;
      Location.Builder location;
      List social_accounts;

      public LastParticipatingAdmin build() {
         return new LastParticipatingAdmin(this);
      }

      public LastParticipatingAdmin.Builder withFirstName(String var1) {
         this.first_name = var1;
         return this;
      }

      public LastParticipatingAdmin.Builder withIntro(String var1) {
         this.intro = var1;
         return this;
      }

      public LastParticipatingAdmin.Builder withJobTitle(String var1) {
         this.job_title = var1;
         return this;
      }
   }
}
