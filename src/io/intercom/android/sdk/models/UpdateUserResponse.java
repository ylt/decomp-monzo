package io.intercom.android.sdk.models;

public class UpdateUserResponse extends UsersResponse {
   private final TeamPresence teamPresence;

   UpdateUserResponse(UpdateUserResponse.Builder var1) {
      super(var1);
      TeamPresence var2;
      if(var1.team_presence == null) {
         var2 = (new TeamPresence.Builder()).build();
      } else {
         var2 = var1.team_presence.build();
      }

      this.teamPresence = var2;
   }

   public TeamPresence getTeamPresence() {
      return this.teamPresence;
   }

   public static final class Builder extends UsersResponse.Builder {
      TeamPresence.Builder team_presence;

      public UpdateUserResponse build() {
         return new UpdateUserResponse(this);
      }
   }
}
