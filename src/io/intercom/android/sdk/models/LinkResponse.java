package io.intercom.android.sdk.models;

public class LinkResponse extends BaseResponse {
   private final Link link;

   LinkResponse(LinkResponse.Builder var1) {
      super(var1);
      Link var2;
      if(var1.article == null) {
         var2 = (new Link.Builder()).build();
      } else {
         var2 = var1.article.build();
      }

      this.link = var2;
   }

   public Link getLink() {
      return this.link;
   }

   public static final class Builder extends BaseResponse.Builder {
      Link.Builder article;

      public LinkResponse build() {
         return new LinkResponse(this);
      }
   }
}
