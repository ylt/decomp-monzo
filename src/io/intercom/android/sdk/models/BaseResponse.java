package io.intercom.android.sdk.models;

public abstract class BaseResponse {
   private final Config config;
   private final boolean hasConversations;
   private final User user;

   protected BaseResponse(BaseResponse.Builder var1) {
      Config var2;
      if(var1.config == null) {
         var2 = Config.NULL;
      } else {
         var2 = var1.config.build();
      }

      this.config = var2;
      this.hasConversations = var1.has_conversations;
      User var3;
      if(var1.user == null) {
         var3 = User.NULL;
      } else {
         var3 = var1.user.build();
      }

      this.user = var3;
   }

   public Config getConfig() {
      return this.config;
   }

   public User getUser() {
      return this.user;
   }

   public boolean hasConversations() {
      return this.hasConversations;
   }

   abstract static class Builder {
      Config.Builder config;
      boolean has_conversations;
      User.Builder user;

      public abstract BaseResponse build();
   }
}
