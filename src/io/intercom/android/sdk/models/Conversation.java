package io.intercom.android.sdk.models;

import io.intercom.android.sdk.utilities.NullSafety;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

public class Conversation {
   private final List conversationParts;
   private final List groupConversationParticipants;
   private final String id;
   private final LastParticipatingAdmin lastParticipatingAdmin;
   private final List operatorClientConditions;
   private final Map participants;
   private final boolean read;
   private final boolean userParticipated;

   public Conversation() {
      this(new Conversation.Builder());
   }

   Conversation(Conversation.Builder var1) {
      this.id = NullSafety.valueOrEmpty(var1.id);
      this.read = var1.read;
      this.userParticipated = var1.user_participated;
      this.participants = new LinkedHashMap();
      Iterator var3;
      if(var1.participants != null) {
         var3 = var1.participants.iterator();

         while(var3.hasNext()) {
            Participant var2 = ((Participant.Builder)var3.next()).build();
            this.participants.put(var2.getId(), var2);
         }
      }

      this.conversationParts = new ArrayList();
      Iterator var5;
      if(var1.conversation_parts != null) {
         var5 = var1.conversation_parts.iterator();

         while(var5.hasNext()) {
            Part var7 = ((Part.Builder)var5.next()).build();
            var7.setParticipant(this.getParticipant(var7.getParticipantId()));
            this.conversationParts.add(var7);
         }
      }

      this.groupConversationParticipants = new ArrayList();
      if(var1.group_conversation_participant_ids != null) {
         var5 = var1.group_conversation_participant_ids.iterator();

         while(var5.hasNext()) {
            String var8 = (String)var5.next();
            if(var8 != null) {
               this.groupConversationParticipants.add(this.getParticipant(var8));
            }
         }
      }

      this.operatorClientConditions = new ArrayList();
      if(var1.operator_client_conditions != null) {
         var3 = var1.operator_client_conditions.iterator();

         while(var3.hasNext()) {
            OperatorClientCondition.Builder var6 = (OperatorClientCondition.Builder)var3.next();
            if(var6 != null) {
               this.operatorClientConditions.add(var6.build());
            }
         }
      }

      LastParticipatingAdmin var4;
      if(var1.last_participating_admin != null) {
         var4 = var1.last_participating_admin.build();
      } else {
         var4 = LastParticipatingAdmin.NULL;
      }

      this.lastParticipatingAdmin = var4;
   }

   private Conversation(Map var1, List var2, List var3, List var4, LastParticipatingAdmin var5, String var6, boolean var7, boolean var8) {
      this.participants = var1;
      this.conversationParts = var2;
      this.groupConversationParticipants = var3;
      this.operatorClientConditions = var4;
      this.lastParticipatingAdmin = var5;
      this.id = var6;
      this.userParticipated = var7;
      this.read = var8;
   }

   public boolean equals(Object var1) {
      boolean var3 = false;
      boolean var2;
      if(this == var1) {
         var2 = true;
      } else {
         var2 = var3;
         if(var1 != null) {
            var2 = var3;
            if(this.getClass() == var1.getClass()) {
               Conversation var4 = (Conversation)var1;
               var2 = var3;
               if(this.userParticipated == var4.userParticipated) {
                  var2 = var3;
                  if(this.read == var4.read) {
                     var2 = var3;
                     if(this.participants.equals(var4.participants)) {
                        var2 = var3;
                        if(this.conversationParts.equals(var4.conversationParts)) {
                           var2 = var3;
                           if(this.groupConversationParticipants.equals(var4.groupConversationParticipants)) {
                              var2 = var3;
                              if(this.operatorClientConditions.equals(var4.operatorClientConditions)) {
                                 var2 = var3;
                                 if(this.lastParticipatingAdmin.equals(var4.lastParticipatingAdmin)) {
                                    var2 = this.id.equals(var4.id);
                                 }
                              }
                           }
                        }
                     }
                  }
               }
            }
         }
      }

      return var2;
   }

   public List getGroupConversationParticipants() {
      return this.groupConversationParticipants;
   }

   public String getId() {
      return this.id;
   }

   public Participant getLastAdmin() {
      ListIterator var2 = (new ArrayList(this.participants.values())).listIterator(this.participants.values().size());

      Participant var1;
      do {
         if(!var2.hasPrevious()) {
            var1 = Participant.NULL;
            break;
         }

         var1 = (Participant)var2.previous();
      } while(!var1.isAdmin());

      return var1;
   }

   public Part getLastAdminPart() {
      int var1 = this.conversationParts.size() - 1;

      Part var2;
      while(true) {
         if(var1 < 0) {
            var2 = Part.NULL;
            break;
         }

         var2 = (Part)this.conversationParts.get(var1);
         if(var2.isAdmin()) {
            break;
         }

         --var1;
      }

      return var2;
   }

   public Part getLastPart() {
      Part var1;
      if(this.conversationParts.isEmpty()) {
         var1 = Part.NULL;
      } else {
         var1 = (Part)this.conversationParts.get(this.conversationParts.size() - 1);
      }

      return var1;
   }

   public LastParticipatingAdmin getLastParticipatingAdmin() {
      return this.lastParticipatingAdmin;
   }

   public Part getLastUserVisiblePart() {
      int var1 = this.conversationParts.size() - 1;

      Part var2;
      while(true) {
         if(var1 < 0) {
            var2 = Part.NULL;
            break;
         }

         var2 = (Part)this.conversationParts.get(var1);
         if(!var2.isEvent().booleanValue()) {
            break;
         }

         --var1;
      }

      return var2;
   }

   public List getOperatorClientConditions() {
      return this.operatorClientConditions;
   }

   public Participant getParticipant(String var1) {
      Participant var2 = (Participant)this.participants.get(var1);
      Participant var3 = var2;
      if(var2 == null) {
         var3 = Participant.NULL;
      }

      return var3;
   }

   public Map getParticipants() {
      return this.participants;
   }

   public List getParts() {
      return this.conversationParts;
   }

   public int hashCode() {
      byte var2 = 1;
      int var6 = this.participants.hashCode();
      int var8 = this.conversationParts.hashCode();
      int var7 = this.groupConversationParticipants.hashCode();
      int var3 = this.operatorClientConditions.hashCode();
      int var4 = this.lastParticipatingAdmin.hashCode();
      int var5 = this.id.hashCode();
      byte var1;
      if(this.userParticipated) {
         var1 = 1;
      } else {
         var1 = 0;
      }

      if(!this.read) {
         var2 = 0;
      }

      return (var1 + (((((var6 * 31 + var8) * 31 + var7) * 31 + var3) * 31 + var4) * 31 + var5) * 31) * 31 + var2;
   }

   public boolean isAdminReply() {
      boolean var1 = true;
      if(this.participants.size() <= 1) {
         var1 = false;
      }

      return var1;
   }

   public boolean isRead() {
      return this.read;
   }

   public boolean isUserParticipated() {
      return this.userParticipated;
   }

   public Conversation withRead(boolean var1) {
      return new Conversation(this.participants, this.conversationParts, this.groupConversationParticipants, this.operatorClientConditions, this.lastParticipatingAdmin, this.id, this.userParticipated, var1);
   }

   public static final class Builder {
      List conversation_parts;
      List group_conversation_participant_ids;
      String id;
      LastParticipatingAdmin.Builder last_participating_admin;
      List operator_client_conditions;
      List participants;
      boolean read;
      boolean user_participated;

      public Conversation build() {
         return new Conversation(this);
      }

      public Conversation.Builder withId(String var1) {
         this.id = var1;
         return this;
      }

      public Conversation.Builder withParticipants(List var1) {
         this.participants = var1;
         return this;
      }

      public Conversation.Builder withParts(List var1) {
         this.conversation_parts = var1;
         return this;
      }
   }
}
