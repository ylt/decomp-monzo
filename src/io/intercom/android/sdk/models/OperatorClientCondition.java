package io.intercom.android.sdk.models;

import io.intercom.android.sdk.utilities.NullSafety;

public abstract class OperatorClientCondition {
   public static OperatorClientCondition create(String var0, String var1) {
      return new AutoValue_OperatorClientCondition(var0, var1);
   }

   public abstract String conditionId();

   public abstract String id();

   public static final class Builder {
      String condition_id;
      String id;

      public OperatorClientCondition build() {
         return OperatorClientCondition.create(NullSafety.valueOrEmpty(this.id), NullSafety.valueOrEmpty(this.condition_id));
      }
   }
}
