package io.intercom.android.sdk.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import io.intercom.android.sdk.utilities.NameUtils;
import io.intercom.android.sdk.utilities.NullSafety;

public abstract class Participant implements Parcelable {
   public static final String ADMIN_TYPE = "admin";
   public static final Creator CREATOR = new Creator() {
      public Participant createFromParcel(Parcel var1) {
         return Participant.create(var1.readString(), var1.readString(), var1.readString(), var1.readString(), (Avatar)var1.readValue(Avatar.class.getClassLoader()));
      }

      public Participant[] newArray(int var1) {
         return new Participant[var1];
      }
   };
   public static final Participant NULL = create("", "", "", "", Avatar.create("", ""));
   static final String USER_TYPE = "user";

   public static Participant create(String var0, String var1, String var2, String var3, Avatar var4) {
      return new AutoValue_Participant(var0, var1, var2, var3, var4);
   }

   public int describeContents() {
      return 0;
   }

   public abstract Avatar getAvatar();

   public abstract String getEmail();

   public String getForename() {
      return this.nameOrEmail().trim().split(" ")[0];
   }

   public abstract String getId();

   public abstract String getName();

   public abstract String getType();

   public boolean isAdmin() {
      return "admin".equals(this.getType());
   }

   public boolean isUserWithId(String var1) {
      boolean var2;
      if("user".equals(this.getType()) && this.getId().equals(var1)) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   String nameOrEmail() {
      String var1;
      if(this.getName().isEmpty()) {
         var1 = this.getEmail();
      } else {
         var1 = this.getName();
      }

      return var1;
   }

   public void writeToParcel(Parcel var1, int var2) {
      var1.writeString(this.getId());
      var1.writeString(this.getName());
      var1.writeString(this.getType());
      var1.writeString(this.getEmail());
      var1.writeValue(this.getAvatar());
   }

   public static final class Builder {
      Avatar.Builder avatar;
      String email;
      String id;
      String name;
      String type;

      public Participant build() {
         String var1;
         if(this.type == null) {
            var1 = "user";
         } else {
            var1 = this.type;
         }

         String var4 = NullSafety.valueOrEmpty(this.name);
         String var3 = NullSafety.valueOrEmpty(this.email);
         String var2;
         if(var4.isEmpty()) {
            var2 = var3;
         } else {
            var2 = var4;
         }

         var2 = NameUtils.getInitial(var2);
         Avatar var5;
         if(this.avatar == null) {
            var5 = Avatar.create("", var2);
         } else {
            var5 = this.avatar.withInitials(var2).build();
         }

         return Participant.create(NullSafety.valueOrEmpty(this.id), var4, var1, var3, var5);
      }

      public Participant.Builder withEmail(String var1) {
         this.email = var1;
         return this;
      }

      public Participant.Builder withId(String var1) {
         this.id = var1;
         return this;
      }

      public Participant.Builder withName(String var1) {
         this.name = var1;
         return this;
      }

      public Participant.Builder withType(String var1) {
         this.type = var1;
         return this;
      }
   }
}
