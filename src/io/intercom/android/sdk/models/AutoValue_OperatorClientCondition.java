package io.intercom.android.sdk.models;

final class AutoValue_OperatorClientCondition extends OperatorClientCondition {
   private final String conditionId;
   private final String id;

   AutoValue_OperatorClientCondition(String var1, String var2) {
      if(var1 == null) {
         throw new NullPointerException("Null id");
      } else {
         this.id = var1;
         if(var2 == null) {
            throw new NullPointerException("Null conditionId");
         } else {
            this.conditionId = var2;
         }
      }
   }

   public String conditionId() {
      return this.conditionId;
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(var1 != this) {
         if(var1 instanceof OperatorClientCondition) {
            OperatorClientCondition var3 = (OperatorClientCondition)var1;
            if(!this.id.equals(var3.id()) || !this.conditionId.equals(var3.conditionId())) {
               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public int hashCode() {
      return (this.id.hashCode() ^ 1000003) * 1000003 ^ this.conditionId.hashCode();
   }

   public String id() {
      return this.id;
   }

   public String toString() {
      return "OperatorClientCondition{id=" + this.id + ", conditionId=" + this.conditionId + "}";
   }
}
