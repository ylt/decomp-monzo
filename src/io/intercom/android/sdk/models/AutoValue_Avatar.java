package io.intercom.android.sdk.models;

final class AutoValue_Avatar extends Avatar {
   private final String imageUrl;
   private final String initials;

   AutoValue_Avatar(String var1, String var2) {
      if(var1 == null) {
         throw new NullPointerException("Null imageUrl");
      } else {
         this.imageUrl = var1;
         if(var2 == null) {
            throw new NullPointerException("Null initials");
         } else {
            this.initials = var2;
         }
      }
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(var1 != this) {
         if(var1 instanceof Avatar) {
            Avatar var3 = (Avatar)var1;
            if(!this.imageUrl.equals(var3.getImageUrl()) || !this.initials.equals(var3.getInitials())) {
               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public String getImageUrl() {
      return this.imageUrl;
   }

   public String getInitials() {
      return this.initials;
   }

   public int hashCode() {
      return (this.imageUrl.hashCode() ^ 1000003) * 1000003 ^ this.initials.hashCode();
   }

   public String toString() {
      return "Avatar{imageUrl=" + this.imageUrl + ", initials=" + this.initials + "}";
   }
}
