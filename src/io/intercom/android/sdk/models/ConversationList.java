package io.intercom.android.sdk.models;

import io.intercom.android.sdk.commons.utilities.CollectionUtils;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public abstract class ConversationList {
   public static ConversationList create(List var0, Set var1, boolean var2) {
      return new AutoValue_ConversationList(var0, var1, var2);
   }

   public abstract List getConversations();

   public abstract Set getUnreadConversationIds();

   public abstract boolean hasMorePages();

   public static final class Builder {
      List conversations;
      boolean more_pages_available;
      List unread_conversation_ids;

      public ConversationList build() {
         ArrayList var1 = new ArrayList(CollectionUtils.capacityFor(this.conversations));
         Iterator var2;
         if(this.conversations != null) {
            var2 = this.conversations.iterator();

            while(var2.hasNext()) {
               Conversation.Builder var3 = (Conversation.Builder)var2.next();
               if(var3 != null) {
                  var1.add(var3.build());
               }
            }
         }

         HashSet var4 = new HashSet(CollectionUtils.capacityFor(this.unread_conversation_ids));
         if(this.unread_conversation_ids != null) {
            var2 = this.unread_conversation_ids.iterator();

            while(var2.hasNext()) {
               String var5 = (String)var2.next();
               if(var5 != null) {
                  var4.add(var5);
               }
            }
         }

         return ConversationList.create(var1, var4, this.more_pages_available);
      }
   }
}
