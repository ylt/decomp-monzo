package io.intercom.android.sdk.models.events.realtime;

public class UserContentSeenByAdminEvent {
   private final String conversationId;

   public UserContentSeenByAdminEvent(String var1) {
      this.conversationId = var1;
   }

   public String getConversationId() {
      return this.conversationId;
   }
}
