package io.intercom.android.sdk.models.events.realtime;

public class NewCommentEvent {
   private final String conversationId;

   public NewCommentEvent(String var1) {
      this.conversationId = var1;
   }

   public String getConversationId() {
      return this.conversationId;
   }
}
