package io.intercom.android.sdk.models.events;

import io.intercom.android.sdk.models.Part;

public class ReplyEvent {
   private final String conversationId;
   private final boolean isAnnotated;
   private final String partId;
   private final int position;
   private final Part response;

   public ReplyEvent(Part var1, int var2, String var3, String var4, boolean var5) {
      this.response = var1;
      this.partId = var3;
      this.position = var2;
      this.conversationId = var4;
      this.isAnnotated = var5;
   }

   public String getConversationId() {
      return this.conversationId;
   }

   public String getPartId() {
      return this.partId;
   }

   public int getPosition() {
      return this.position;
   }

   public Part getResponse() {
      return this.response;
   }

   public boolean isAnnotated() {
      return this.isAnnotated;
   }
}
