package io.intercom.android.sdk.models.events;

import io.intercom.android.sdk.models.Conversation;

public class ConversationEvent {
   private final Conversation response;

   public ConversationEvent(Conversation var1) {
      this.response = var1;
   }

   public Conversation getResponse() {
      return this.response;
   }
}
