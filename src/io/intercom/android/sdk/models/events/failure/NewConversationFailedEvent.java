package io.intercom.android.sdk.models.events.failure;

public class NewConversationFailedEvent {
   private final String partId;
   private final int position;

   public NewConversationFailedEvent(int var1, String var2) {
      this.position = var1;
      this.partId = var2;
   }

   public String getPartId() {
      return this.partId;
   }

   public int getPosition() {
      return this.position;
   }
}
