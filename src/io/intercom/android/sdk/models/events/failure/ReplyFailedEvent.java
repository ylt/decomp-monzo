package io.intercom.android.sdk.models.events.failure;

public class ReplyFailedEvent {
   private final boolean isUpload;
   private final String partId;
   private final int position;

   public ReplyFailedEvent(int var1, boolean var2, String var3) {
      this.position = var1;
      this.partId = var3;
      this.isUpload = var2;
   }

   public String getPartId() {
      return this.partId;
   }

   public int getPosition() {
      return this.position;
   }

   public boolean isUpload() {
      return this.isUpload;
   }
}
