package io.intercom.android.sdk.models.events;

import io.intercom.android.sdk.models.Upload;

public class UploadEvent {
   private final boolean isAnnotated;
   private final long size;
   private final String tempPartId;
   private final int tempPartPosition;
   private final Upload upload;

   public UploadEvent(Upload var1, long var2, int var4, String var5, boolean var6) {
      this.upload = var1;
      this.size = var2;
      this.tempPartPosition = var4;
      this.tempPartId = var5;
      this.isAnnotated = var6;
   }

   public long getSize() {
      return this.size;
   }

   public String getTempPartId() {
      return this.tempPartId;
   }

   public int getTempPartPosition() {
      return this.tempPartPosition;
   }

   public Upload getUpload() {
      return this.upload;
   }

   public boolean isAnnotatedImage() {
      return this.isAnnotated;
   }
}
