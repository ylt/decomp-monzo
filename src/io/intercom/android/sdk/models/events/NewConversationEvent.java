package io.intercom.android.sdk.models.events;

import io.intercom.android.sdk.models.Conversation;

public class NewConversationEvent {
   private final Conversation conversation;
   private final boolean isAnnotatedImage;
   private final String partId;

   public NewConversationEvent(Conversation var1, String var2, boolean var3) {
      this.conversation = var1;
      this.partId = var2;
      this.isAnnotatedImage = var3;
   }

   public Conversation getConversation() {
      return this.conversation;
   }

   public String getPartId() {
      return this.partId;
   }

   public boolean isAnnotatedImage() {
      return this.isAnnotatedImage;
   }
}
