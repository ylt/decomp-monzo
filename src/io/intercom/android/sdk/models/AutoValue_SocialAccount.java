package io.intercom.android.sdk.models;

final class AutoValue_SocialAccount extends SocialAccount {
   private final String profileUrl;
   private final String provider;

   AutoValue_SocialAccount(String var1, String var2) {
      if(var1 == null) {
         throw new NullPointerException("Null provider");
      } else {
         this.provider = var1;
         if(var2 == null) {
            throw new NullPointerException("Null profileUrl");
         } else {
            this.profileUrl = var2;
         }
      }
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(var1 != this) {
         if(var1 instanceof SocialAccount) {
            SocialAccount var3 = (SocialAccount)var1;
            if(!this.provider.equals(var3.getProvider()) || !this.profileUrl.equals(var3.getProfileUrl())) {
               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public String getProfileUrl() {
      return this.profileUrl;
   }

   public String getProvider() {
      return this.provider;
   }

   public int hashCode() {
      return (this.provider.hashCode() ^ 1000003) * 1000003 ^ this.profileUrl.hashCode();
   }

   public String toString() {
      return "SocialAccount{provider=" + this.provider + ", profileUrl=" + this.profileUrl + "}";
   }
}
