package io.intercom.android.sdk.models;

public class ConversationsResponse extends BaseResponse {
   private final ConversationList conversationPage;

   ConversationsResponse(ConversationsResponse.Builder var1) {
      super(var1);
      ConversationList var2;
      if(var1.conversation_page == null) {
         var2 = (new ConversationList.Builder()).build();
      } else {
         var2 = var1.conversation_page.build();
      }

      this.conversationPage = var2;
   }

   public ConversationList getConversationPage() {
      return this.conversationPage;
   }

   public static final class Builder extends BaseResponse.Builder {
      ConversationList.Builder conversation_page;

      public ConversationsResponse build() {
         return new ConversationsResponse(this);
      }
   }
}
