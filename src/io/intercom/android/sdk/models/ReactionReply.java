package io.intercom.android.sdk.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import io.intercom.android.sdk.commons.utilities.CollectionUtils;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ReactionReply implements Parcelable {
   public static final Creator CREATOR = new Creator() {
      public ReactionReply createFromParcel(Parcel var1) {
         return new ReactionReply(var1);
      }

      public ReactionReply[] newArray(int var1) {
         return new ReactionReply[var1];
      }
   };
   public static final ReactionReply NULL = new ReactionReply(new ReactionReply.Builder());
   private Integer reactionIndex;
   private final List reactionSet;

   ReactionReply(Parcel var1) {
      Integer var2;
      if(var1.readByte() == 0) {
         var2 = null;
      } else {
         var2 = Integer.valueOf(var1.readInt());
      }

      this.reactionIndex = var2;
      this.reactionSet = new ArrayList();
      var1.readList(this.reactionSet, Reaction.class.getClassLoader());
   }

   ReactionReply(ReactionReply.Builder var1) {
      this.reactionIndex = var1.reaction_index;
      this.reactionSet = new ArrayList(CollectionUtils.capacityFor(var1.reaction_set));
      if(var1.reaction_set != null) {
         Iterator var2 = var1.reaction_set.iterator();

         while(var2.hasNext()) {
            Reaction.Builder var3 = (Reaction.Builder)var2.next();
            this.reactionSet.add(var3.build());
         }
      }

   }

   public static boolean isNull(ReactionReply var0) {
      boolean var1;
      if(!NULL.equals(var0) && var0 != null) {
         var1 = false;
      } else {
         var1 = true;
      }

      return var1;
   }

   public int describeContents() {
      return 0;
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(this != var1) {
         if(var1 != null && this.getClass() == var1.getClass()) {
            ReactionReply var3 = (ReactionReply)var1;
            if(!this.reactionSet.equals(var3.reactionSet)) {
               var2 = false;
            } else if(this.reactionIndex != null) {
               var2 = this.reactionIndex.equals(var3.reactionIndex);
            } else if(var3.reactionIndex != null) {
               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public Integer getReactionIndex() {
      return this.reactionIndex;
   }

   public List getReactionSet() {
      return this.reactionSet;
   }

   public int hashCode() {
      int var2 = this.reactionSet.hashCode();
      int var1;
      if(this.reactionIndex != null) {
         var1 = this.reactionIndex.hashCode();
      } else {
         var1 = 0;
      }

      return var1 + var2 * 31;
   }

   public void setReactionIndex(int var1) {
      this.reactionIndex = Integer.valueOf(var1);
   }

   public void writeToParcel(Parcel var1, int var2) {
      if(this.reactionIndex == null) {
         var1.writeByte(0);
      } else {
         var1.writeByte(1);
         var1.writeInt(this.reactionIndex.intValue());
      }

      var1.writeList(this.reactionSet);
   }

   public static class Builder {
      Integer reaction_index;
      List reaction_set;

      public ReactionReply build() {
         return new ReactionReply(this);
      }
   }
}
