package io.intercom.android.sdk.models;

import io.intercom.android.sdk.nexus.NexusConfig;
import io.intercom.android.sdk.utilities.NullSafety;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public abstract class Config {
   public static final long DEFAULT_BATCH_USER_UPDATE_PERIOD_MS;
   public static final long DEFAULT_CACHE_MAX_AGE_MS;
   public static final long DEFAULT_PING_DELAY_MS;
   public static final int DEFAULT_RATE_LIMIT_COUNT = 100;
   public static final long DEFAULT_RATE_LIMIT_PERIOD_MS;
   public static final long DEFAULT_SESSION_TIMEOUT_MS;
   public static final long DEFAULT_SOFT_RESET_TIMEOUT_MS;
   public static final Config NULL;

   static {
      DEFAULT_RATE_LIMIT_PERIOD_MS = TimeUnit.MINUTES.toMillis(1L);
      DEFAULT_CACHE_MAX_AGE_MS = TimeUnit.MINUTES.toMillis(5L);
      DEFAULT_SESSION_TIMEOUT_MS = TimeUnit.SECONDS.toMillis(20L);
      DEFAULT_SOFT_RESET_TIMEOUT_MS = TimeUnit.SECONDS.toMillis(1L);
      DEFAULT_BATCH_USER_UPDATE_PERIOD_MS = TimeUnit.SECONDS.toMillis(1L);
      DEFAULT_PING_DELAY_MS = TimeUnit.SECONDS.toMillis(1L);
      NULL = (new Config.Builder()).build();
   }

   public abstract String getBaseColor();

   public abstract long getBatchUserUpdatePeriod();

   public abstract Set getFeatures();

   public abstract String getLocale();

   public abstract String getMessengerBackground();

   public abstract String getName();

   public abstract long getNewSessionThreshold();

   public abstract long getPingDelayMs();

   public abstract int getRateLimitCount();

   public abstract long getRateLimitPeriod();

   public abstract NexusConfig getRealTimeConfig();

   public abstract long getSoftResetTimeout();

   public abstract long getUserUpdateCacheMaxAge();

   public abstract String getWelcomeMessage();

   public abstract boolean isAudioEnabled();

   public abstract boolean isBackgroundRequestsEnabled();

   public abstract boolean isFirstRequest();

   public abstract boolean isInboundMessages();

   public abstract boolean isMetricsEnabled();

   public abstract boolean isShowPoweredBy();

   public static final class Builder {
      Boolean audio_enabled;
      Boolean background_requests_enabled;
      String base_color;
      Float batch_user_update_period;
      Set features;
      Boolean inbound_messages;
      Boolean is_first_request;
      Integer local_rate_limit;
      Long local_rate_limit_period;
      String locale;
      String messenger_background;
      Boolean metrics_enabled;
      String name;
      Long new_session_threshold;
      Float ping_delay;
      NexusConfig.Builder real_time_config;
      Boolean show_powered_by;
      Long soft_reset_timeout;
      Long user_update_dup_cache_max_age;
      String welcome_message_plain_text;

      public Config build() {
         long var2;
         if(this.local_rate_limit_period == null) {
            var2 = Config.DEFAULT_RATE_LIMIT_PERIOD_MS;
         } else {
            var2 = TimeUnit.SECONDS.toMillis(this.local_rate_limit_period.longValue());
         }

         int var1;
         if(this.local_rate_limit == null) {
            var1 = 100;
         } else {
            var1 = this.local_rate_limit.intValue();
         }

         long var4;
         if(this.batch_user_update_period == null) {
            var4 = Config.DEFAULT_BATCH_USER_UPDATE_PERIOD_MS;
         } else {
            var4 = (long)(this.batch_user_update_period.floatValue() * 1000.0F);
         }

         long var6;
         if(this.user_update_dup_cache_max_age == null) {
            var6 = Config.DEFAULT_CACHE_MAX_AGE_MS;
         } else {
            var6 = TimeUnit.SECONDS.toMillis(this.user_update_dup_cache_max_age.longValue());
         }

         long var8;
         if(this.soft_reset_timeout == null) {
            var8 = Config.DEFAULT_SOFT_RESET_TIMEOUT_MS;
         } else {
            var8 = TimeUnit.SECONDS.toMillis(this.soft_reset_timeout.longValue());
         }

         long var10;
         if(this.new_session_threshold == null) {
            var10 = Config.DEFAULT_SESSION_TIMEOUT_MS;
         } else {
            var10 = TimeUnit.SECONDS.toMillis(this.new_session_threshold.longValue());
         }

         long var12;
         if(this.ping_delay == null) {
            var12 = Config.DEFAULT_PING_DELAY_MS;
         } else {
            var12 = (long)(this.ping_delay.floatValue() * 1000.0F);
         }

         NexusConfig var14;
         if(this.real_time_config == null) {
            var14 = new NexusConfig();
         } else {
            var14 = this.real_time_config.build();
         }

         Set var15;
         if(this.features == null) {
            var15 = Collections.emptySet();
         } else {
            var15 = this.features;
         }

         return new AutoValue_Config(NullSafety.valueOrEmpty(this.name), this.base_color, NullSafety.valueOrEmpty(this.welcome_message_plain_text), NullSafety.valueOrEmpty(this.messenger_background), NullSafety.valueOrEmpty(this.locale), NullSafety.valueOrDefault(this.is_first_request, false), NullSafety.valueOrDefault(this.inbound_messages, false), NullSafety.valueOrDefault(this.show_powered_by, true), NullSafety.valueOrDefault(this.audio_enabled, true), NullSafety.valueOrDefault(this.metrics_enabled, true), NullSafety.valueOrDefault(this.background_requests_enabled, true), var2, var1, var4, var6, var8, var10, var12, var14, var15);
      }
   }
}
