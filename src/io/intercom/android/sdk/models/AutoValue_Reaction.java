package io.intercom.android.sdk.models;

final class AutoValue_Reaction extends Reaction {
   private final String imageUrl;
   private final int index;

   AutoValue_Reaction(int var1, String var2) {
      this.index = var1;
      if(var2 == null) {
         throw new NullPointerException("Null imageUrl");
      } else {
         this.imageUrl = var2;
      }
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(var1 != this) {
         if(var1 instanceof Reaction) {
            Reaction var3 = (Reaction)var1;
            if(this.index != var3.getIndex() || !this.imageUrl.equals(var3.getImageUrl())) {
               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public String getImageUrl() {
      return this.imageUrl;
   }

   public int getIndex() {
      return this.index;
   }

   public int hashCode() {
      return (this.index ^ 1000003) * 1000003 ^ this.imageUrl.hashCode();
   }

   public String toString() {
      return "Reaction{index=" + this.index + ", imageUrl=" + this.imageUrl + "}";
   }
}
