package io.intercom.android.sdk.models;

import io.intercom.android.sdk.utilities.NullSafety;
import io.intercom.com.google.gson.a.c;

public class User {
   public static final User NULL = new User();
   @c(
      a = "anonymous_id"
   )
   private final String anonymousId;
   private final String email;
   @c(
      a = "intercom_id"
   )
   private final String intercomId;
   @c(
      a = "user_id"
   )
   private final String userId;

   public User() {
      this.intercomId = "";
      this.anonymousId = "";
      this.userId = "";
      this.email = "";
   }

   User(User.Builder var1) {
      this.intercomId = NullSafety.valueOrEmpty(var1.intercom_id);
      this.anonymousId = NullSafety.valueOrEmpty(var1.anonymous_id);
      this.userId = NullSafety.valueOrEmpty(var1.user_id);
      this.email = NullSafety.valueOrEmpty(var1.email);
   }

   public boolean equals(Object var1) {
      boolean var3 = false;
      boolean var2;
      if(this == var1) {
         var2 = true;
      } else {
         var2 = var3;
         if(var1 != null) {
            var2 = var3;
            if(this.getClass() == var1.getClass()) {
               User var4 = (User)var1;
               var2 = var3;
               if(this.intercomId.equals(var4.intercomId)) {
                  var2 = var3;
                  if(this.anonymousId.equals(var4.anonymousId)) {
                     var2 = var3;
                     if(this.userId.equals(var4.userId)) {
                        var2 = this.email.equals(var4.email);
                     }
                  }
               }
            }
         }
      }

      return var2;
   }

   public String getAnonymousId() {
      return this.anonymousId;
   }

   public String getEmail() {
      return this.email;
   }

   public String getIntercomId() {
      return this.intercomId;
   }

   public String getUserId() {
      return this.userId;
   }

   public int hashCode() {
      return ((this.intercomId.hashCode() * 31 + this.anonymousId.hashCode()) * 31 + this.userId.hashCode()) * 31 + this.email.hashCode();
   }

   public static final class Builder {
      String anonymous_id;
      private Avatar.Builder avatar;
      String email;
      String intercom_id;
      String user_id;

      public User build() {
         return new User(this);
      }

      public User.Builder withAnonymousId(String var1) {
         this.anonymous_id = var1;
         return this;
      }

      public User.Builder withAvatar(Avatar.Builder var1) {
         this.avatar = var1;
         return this;
      }

      public User.Builder withEmail(String var1) {
         this.email = var1;
         return this;
      }

      public User.Builder withIntercomId(String var1) {
         this.intercom_id = var1;
         return this;
      }

      public User.Builder withUserId(String var1) {
         this.user_id = var1;
         return this;
      }
   }
}
