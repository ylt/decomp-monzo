package io.intercom.android.sdk.models;

final class AutoValue_Upload extends Upload {
   private final String acl;
   private final String awsAccessKey;
   private final String contentType;
   private final int id;
   private final String key;
   private final String policy;
   private final String publicUrl;
   private final String signature;
   private final String successActionStatus;
   private final String uploadDestination;

   AutoValue_Upload(int var1, String var2, String var3, String var4, String var5, String var6, String var7, String var8, String var9, String var10) {
      this.id = var1;
      if(var2 == null) {
         throw new NullPointerException("Null acl");
      } else {
         this.acl = var2;
         if(var3 == null) {
            throw new NullPointerException("Null awsAccessKey");
         } else {
            this.awsAccessKey = var3;
            if(var4 == null) {
               throw new NullPointerException("Null contentType");
            } else {
               this.contentType = var4;
               if(var5 == null) {
                  throw new NullPointerException("Null key");
               } else {
                  this.key = var5;
                  if(var6 == null) {
                     throw new NullPointerException("Null policy");
                  } else {
                     this.policy = var6;
                     if(var7 == null) {
                        throw new NullPointerException("Null publicUrl");
                     } else {
                        this.publicUrl = var7;
                        if(var8 == null) {
                           throw new NullPointerException("Null signature");
                        } else {
                           this.signature = var8;
                           if(var9 == null) {
                              throw new NullPointerException("Null successActionStatus");
                           } else {
                              this.successActionStatus = var9;
                              if(var10 == null) {
                                 throw new NullPointerException("Null uploadDestination");
                              } else {
                                 this.uploadDestination = var10;
                              }
                           }
                        }
                     }
                  }
               }
            }
         }
      }
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(var1 != this) {
         if(var1 instanceof Upload) {
            Upload var3 = (Upload)var1;
            if(this.id != var3.getId() || !this.acl.equals(var3.getAcl()) || !this.awsAccessKey.equals(var3.getAwsAccessKey()) || !this.contentType.equals(var3.getContentType()) || !this.key.equals(var3.getKey()) || !this.policy.equals(var3.getPolicy()) || !this.publicUrl.equals(var3.getPublicUrl()) || !this.signature.equals(var3.getSignature()) || !this.successActionStatus.equals(var3.getSuccessActionStatus()) || !this.uploadDestination.equals(var3.getUploadDestination())) {
               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public String getAcl() {
      return this.acl;
   }

   public String getAwsAccessKey() {
      return this.awsAccessKey;
   }

   public String getContentType() {
      return this.contentType;
   }

   public int getId() {
      return this.id;
   }

   public String getKey() {
      return this.key;
   }

   public String getPolicy() {
      return this.policy;
   }

   public String getPublicUrl() {
      return this.publicUrl;
   }

   public String getSignature() {
      return this.signature;
   }

   public String getSuccessActionStatus() {
      return this.successActionStatus;
   }

   public String getUploadDestination() {
      return this.uploadDestination;
   }

   public int hashCode() {
      return (((((((((this.id ^ 1000003) * 1000003 ^ this.acl.hashCode()) * 1000003 ^ this.awsAccessKey.hashCode()) * 1000003 ^ this.contentType.hashCode()) * 1000003 ^ this.key.hashCode()) * 1000003 ^ this.policy.hashCode()) * 1000003 ^ this.publicUrl.hashCode()) * 1000003 ^ this.signature.hashCode()) * 1000003 ^ this.successActionStatus.hashCode()) * 1000003 ^ this.uploadDestination.hashCode();
   }

   public String toString() {
      return "Upload{id=" + this.id + ", acl=" + this.acl + ", awsAccessKey=" + this.awsAccessKey + ", contentType=" + this.contentType + ", key=" + this.key + ", policy=" + this.policy + ", publicUrl=" + this.publicUrl + ", signature=" + this.signature + ", successActionStatus=" + this.successActionStatus + ", uploadDestination=" + this.uploadDestination + "}";
   }
}
