package io.intercom.android.sdk.models;

final class AutoValue_Location extends Location {
   private final String cityName;
   private final String countryName;
   private final Integer timezoneOffset;

   AutoValue_Location(String var1, String var2, Integer var3) {
      if(var1 == null) {
         throw new NullPointerException("Null cityName");
      } else {
         this.cityName = var1;
         if(var2 == null) {
            throw new NullPointerException("Null countryName");
         } else {
            this.countryName = var2;
            this.timezoneOffset = var3;
         }
      }
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(var1 != this) {
         if(var1 instanceof Location) {
            Location var3 = (Location)var1;
            if(this.cityName.equals(var3.getCityName()) && this.countryName.equals(var3.getCountryName())) {
               if(this.timezoneOffset == null) {
                  if(var3.getTimezoneOffset() == null) {
                     return var2;
                  }
               } else if(this.timezoneOffset.equals(var3.getTimezoneOffset())) {
                  return var2;
               }
            }

            var2 = false;
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public String getCityName() {
      return this.cityName;
   }

   public String getCountryName() {
      return this.countryName;
   }

   public Integer getTimezoneOffset() {
      return this.timezoneOffset;
   }

   public int hashCode() {
      int var3 = this.cityName.hashCode();
      int var2 = this.countryName.hashCode();
      int var1;
      if(this.timezoneOffset == null) {
         var1 = 0;
      } else {
         var1 = this.timezoneOffset.hashCode();
      }

      return var1 ^ ((var3 ^ 1000003) * 1000003 ^ var2) * 1000003;
   }

   public String toString() {
      return "Location{cityName=" + this.cityName + ", countryName=" + this.countryName + ", timezoneOffset=" + this.timezoneOffset + "}";
   }
}
