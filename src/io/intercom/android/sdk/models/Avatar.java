package io.intercom.android.sdk.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import io.intercom.android.sdk.utilities.NullSafety;

public abstract class Avatar implements Parcelable {
   public static final Creator CREATOR = new Creator() {
      public Avatar createFromParcel(Parcel var1) {
         return Avatar.create(var1.readString(), var1.readString());
      }

      public Avatar[] newArray(int var1) {
         return new Avatar[var1];
      }
   };

   public static Avatar create(String var0, String var1) {
      return new AutoValue_Avatar(var0, var1);
   }

   public int describeContents() {
      return 0;
   }

   public abstract String getImageUrl();

   public abstract String getInitials();

   public void writeToParcel(Parcel var1, int var2) {
      var1.writeString(this.getImageUrl());
      var1.writeString(this.getInitials());
   }

   public static final class Builder {
      String image_url;
      String initials;

      public Avatar build() {
         return Avatar.create(NullSafety.valueOrEmpty(this.image_url), NullSafety.valueOrEmpty(this.initials));
      }

      Avatar.Builder withInitials(String var1) {
         this.initials = var1;
         return this;
      }
   }
}
