package io.intercom.android.sdk.models;

import io.intercom.android.sdk.nexus.NexusConfig;
import java.util.Set;

final class AutoValue_Config extends Config {
   private final boolean audioEnabled;
   private final boolean backgroundRequestsEnabled;
   private final String baseColor;
   private final long batchUserUpdatePeriod;
   private final Set features;
   private final boolean firstRequest;
   private final boolean inboundMessages;
   private final String locale;
   private final String messengerBackground;
   private final boolean metricsEnabled;
   private final String name;
   private final long newSessionThreshold;
   private final long pingDelayMs;
   private final int rateLimitCount;
   private final long rateLimitPeriod;
   private final NexusConfig realTimeConfig;
   private final boolean showPoweredBy;
   private final long softResetTimeout;
   private final long userUpdateCacheMaxAge;
   private final String welcomeMessage;

   AutoValue_Config(String var1, String var2, String var3, String var4, String var5, boolean var6, boolean var7, boolean var8, boolean var9, boolean var10, boolean var11, long var12, int var14, long var15, long var17, long var19, long var21, long var23, NexusConfig var25, Set var26) {
      if(var1 == null) {
         throw new NullPointerException("Null name");
      } else {
         this.name = var1;
         this.baseColor = var2;
         if(var3 == null) {
            throw new NullPointerException("Null welcomeMessage");
         } else {
            this.welcomeMessage = var3;
            if(var4 == null) {
               throw new NullPointerException("Null messengerBackground");
            } else {
               this.messengerBackground = var4;
               if(var5 == null) {
                  throw new NullPointerException("Null locale");
               } else {
                  this.locale = var5;
                  this.firstRequest = var6;
                  this.inboundMessages = var7;
                  this.showPoweredBy = var8;
                  this.metricsEnabled = var9;
                  this.backgroundRequestsEnabled = var10;
                  this.audioEnabled = var11;
                  this.rateLimitPeriod = var12;
                  this.rateLimitCount = var14;
                  this.batchUserUpdatePeriod = var15;
                  this.userUpdateCacheMaxAge = var17;
                  this.softResetTimeout = var19;
                  this.newSessionThreshold = var21;
                  this.pingDelayMs = var23;
                  if(var25 == null) {
                     throw new NullPointerException("Null realTimeConfig");
                  } else {
                     this.realTimeConfig = var25;
                     if(var26 == null) {
                        throw new NullPointerException("Null features");
                     } else {
                        this.features = var26;
                     }
                  }
               }
            }
         }
      }
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(var1 != this) {
         if(var1 instanceof Config) {
            Config var3 = (Config)var1;
            if(this.name.equals(var3.getName())) {
               label55: {
                  if(this.baseColor == null) {
                     if(var3.getBaseColor() != null) {
                        break label55;
                     }
                  } else if(!this.baseColor.equals(var3.getBaseColor())) {
                     break label55;
                  }

                  if(this.welcomeMessage.equals(var3.getWelcomeMessage()) && this.messengerBackground.equals(var3.getMessengerBackground()) && this.locale.equals(var3.getLocale()) && this.firstRequest == var3.isFirstRequest() && this.inboundMessages == var3.isInboundMessages() && this.showPoweredBy == var3.isShowPoweredBy() && this.metricsEnabled == var3.isMetricsEnabled() && this.backgroundRequestsEnabled == var3.isBackgroundRequestsEnabled() && this.audioEnabled == var3.isAudioEnabled() && this.rateLimitPeriod == var3.getRateLimitPeriod() && this.rateLimitCount == var3.getRateLimitCount() && this.batchUserUpdatePeriod == var3.getBatchUserUpdatePeriod() && this.userUpdateCacheMaxAge == var3.getUserUpdateCacheMaxAge() && this.softResetTimeout == var3.getSoftResetTimeout() && this.newSessionThreshold == var3.getNewSessionThreshold() && this.pingDelayMs == var3.getPingDelayMs() && this.realTimeConfig.equals(var3.getRealTimeConfig()) && this.features.equals(var3.getFeatures())) {
                     return var2;
                  }
               }
            }

            var2 = false;
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public String getBaseColor() {
      return this.baseColor;
   }

   public long getBatchUserUpdatePeriod() {
      return this.batchUserUpdatePeriod;
   }

   public Set getFeatures() {
      return this.features;
   }

   public String getLocale() {
      return this.locale;
   }

   public String getMessengerBackground() {
      return this.messengerBackground;
   }

   public String getName() {
      return this.name;
   }

   public long getNewSessionThreshold() {
      return this.newSessionThreshold;
   }

   public long getPingDelayMs() {
      return this.pingDelayMs;
   }

   public int getRateLimitCount() {
      return this.rateLimitCount;
   }

   public long getRateLimitPeriod() {
      return this.rateLimitPeriod;
   }

   public NexusConfig getRealTimeConfig() {
      return this.realTimeConfig;
   }

   public long getSoftResetTimeout() {
      return this.softResetTimeout;
   }

   public long getUserUpdateCacheMaxAge() {
      return this.userUpdateCacheMaxAge;
   }

   public String getWelcomeMessage() {
      return this.welcomeMessage;
   }

   public int hashCode() {
      short var7 = 1231;
      int var8 = this.name.hashCode();
      int var1;
      if(this.baseColor == null) {
         var1 = 0;
      } else {
         var1 = this.baseColor.hashCode();
      }

      int var9 = this.welcomeMessage.hashCode();
      int var11 = this.messengerBackground.hashCode();
      int var10 = this.locale.hashCode();
      short var2;
      if(this.firstRequest) {
         var2 = 1231;
      } else {
         var2 = 1237;
      }

      short var3;
      if(this.inboundMessages) {
         var3 = 1231;
      } else {
         var3 = 1237;
      }

      short var4;
      if(this.showPoweredBy) {
         var4 = 1231;
      } else {
         var4 = 1237;
      }

      short var5;
      if(this.metricsEnabled) {
         var5 = 1231;
      } else {
         var5 = 1237;
      }

      short var6;
      if(this.backgroundRequestsEnabled) {
         var6 = 1231;
      } else {
         var6 = 1237;
      }

      if(!this.audioEnabled) {
         var7 = 1237;
      }

      return ((int)((long)((int)((long)((int)((long)((int)((long)((int)((long)(((int)((long)(((var6 ^ (var5 ^ (var4 ^ (var3 ^ (var2 ^ ((((var1 ^ (var8 ^ 1000003) * 1000003) * 1000003 ^ var9) * 1000003 ^ var11) * 1000003 ^ var10) * 1000003) * 1000003) * 1000003) * 1000003) * 1000003) * 1000003 ^ var7) * 1000003) ^ this.rateLimitPeriod >>> 32 ^ this.rateLimitPeriod) * 1000003 ^ this.rateLimitCount) * 1000003) ^ this.batchUserUpdatePeriod >>> 32 ^ this.batchUserUpdatePeriod) * 1000003) ^ this.userUpdateCacheMaxAge >>> 32 ^ this.userUpdateCacheMaxAge) * 1000003) ^ this.softResetTimeout >>> 32 ^ this.softResetTimeout) * 1000003) ^ this.newSessionThreshold >>> 32 ^ this.newSessionThreshold) * 1000003) ^ this.pingDelayMs >>> 32 ^ this.pingDelayMs) * 1000003 ^ this.realTimeConfig.hashCode()) * 1000003 ^ this.features.hashCode();
   }

   public boolean isAudioEnabled() {
      return this.audioEnabled;
   }

   public boolean isBackgroundRequestsEnabled() {
      return this.backgroundRequestsEnabled;
   }

   public boolean isFirstRequest() {
      return this.firstRequest;
   }

   public boolean isInboundMessages() {
      return this.inboundMessages;
   }

   public boolean isMetricsEnabled() {
      return this.metricsEnabled;
   }

   public boolean isShowPoweredBy() {
      return this.showPoweredBy;
   }

   public String toString() {
      return "Config{name=" + this.name + ", baseColor=" + this.baseColor + ", welcomeMessage=" + this.welcomeMessage + ", messengerBackground=" + this.messengerBackground + ", locale=" + this.locale + ", firstRequest=" + this.firstRequest + ", inboundMessages=" + this.inboundMessages + ", showPoweredBy=" + this.showPoweredBy + ", metricsEnabled=" + this.metricsEnabled + ", backgroundRequestsEnabled=" + this.backgroundRequestsEnabled + ", audioEnabled=" + this.audioEnabled + ", rateLimitPeriod=" + this.rateLimitPeriod + ", rateLimitCount=" + this.rateLimitCount + ", batchUserUpdatePeriod=" + this.batchUserUpdatePeriod + ", userUpdateCacheMaxAge=" + this.userUpdateCacheMaxAge + ", softResetTimeout=" + this.softResetTimeout + ", newSessionThreshold=" + this.newSessionThreshold + ", pingDelayMs=" + this.pingDelayMs + ", realTimeConfig=" + this.realTimeConfig + ", features=" + this.features + "}";
   }
}
