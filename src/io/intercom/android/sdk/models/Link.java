package io.intercom.android.sdk.models;

import io.intercom.android.sdk.blocks.models.Author;
import io.intercom.android.sdk.blocks.models.Block;
import io.intercom.android.sdk.commons.utilities.CollectionUtils;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

public abstract class Link {
   public static final String ARTICLE_TYPE = "educate.article";
   public static final String HELP_CENTER_TYPE = "educate.help_center";
   public static final String SUGGESTION_TYPE = "educate.suggestion";

   public Author getAuthor() {
      return this.getCard().getAuthor();
   }

   public abstract List getBlocks();

   public abstract Card getCard();

   public abstract long getCreatedAt();

   public String getDescription() {
      return this.getCard().getDescription();
   }

   public abstract String getId();

   public abstract ReactionReply getReactionReply();

   public String getText() {
      return this.getCard().getText();
   }

   public String getTitle() {
      return this.getCard().getTitle();
   }

   public String getType() {
      return this.getCard().getType();
   }

   public abstract long getUpdatedAt();

   public static final class Builder {
      List blocks;
      Card.Builder card;
      long created_at;
      String id;
      ReactionReply.Builder reactions_reply;
      long updated_at;

      public Link build() {
         String var1;
         if(this.id == null) {
            var1 = UUID.randomUUID().toString();
         } else {
            var1 = this.id;
         }

         ArrayList var4 = new ArrayList(CollectionUtils.capacityFor(this.blocks));
         if(this.blocks != null) {
            Iterator var3 = this.blocks.iterator();

            while(var3.hasNext()) {
               Block.Builder var2 = (Block.Builder)var3.next();
               if(var2 != null) {
                  var4.add(var2.build());
               }
            }
         }

         ReactionReply var5;
         if(this.reactions_reply == null) {
            var5 = ReactionReply.NULL;
         } else {
            var5 = this.reactions_reply.build();
         }

         Card var6;
         if(this.card == null) {
            var6 = (new Card.Builder()).build();
         } else {
            var6 = this.card.build();
         }

         return new AutoValue_Link(var1, var6, var4, var5, this.created_at, this.updated_at);
      }
   }
}
