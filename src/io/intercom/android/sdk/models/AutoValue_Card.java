package io.intercom.android.sdk.models;

import io.intercom.android.sdk.blocks.models.Author;

final class AutoValue_Card extends Card {
   private final Author author;
   private final String description;
   private final String text;
   private final String title;
   private final String type;

   AutoValue_Card(String var1, String var2, String var3, String var4, Author var5) {
      if(var1 == null) {
         throw new NullPointerException("Null type");
      } else {
         this.type = var1;
         if(var2 == null) {
            throw new NullPointerException("Null text");
         } else {
            this.text = var2;
            if(var3 == null) {
               throw new NullPointerException("Null title");
            } else {
               this.title = var3;
               if(var4 == null) {
                  throw new NullPointerException("Null description");
               } else {
                  this.description = var4;
                  if(var5 == null) {
                     throw new NullPointerException("Null author");
                  } else {
                     this.author = var5;
                  }
               }
            }
         }
      }
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(var1 != this) {
         if(var1 instanceof Card) {
            Card var3 = (Card)var1;
            if(!this.type.equals(var3.getType()) || !this.text.equals(var3.getText()) || !this.title.equals(var3.getTitle()) || !this.description.equals(var3.getDescription()) || !this.author.equals(var3.getAuthor())) {
               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public Author getAuthor() {
      return this.author;
   }

   public String getDescription() {
      return this.description;
   }

   public String getText() {
      return this.text;
   }

   public String getTitle() {
      return this.title;
   }

   public String getType() {
      return this.type;
   }

   public int hashCode() {
      return ((((this.type.hashCode() ^ 1000003) * 1000003 ^ this.text.hashCode()) * 1000003 ^ this.title.hashCode()) * 1000003 ^ this.description.hashCode()) * 1000003 ^ this.author.hashCode();
   }

   public String toString() {
      return "Card{type=" + this.type + ", text=" + this.text + ", title=" + this.title + ", description=" + this.description + ", author=" + this.author + "}";
   }
}
