package io.intercom.android.sdk.models;

final class AutoValue_Attachments extends Attachments {
   private final String contentType;
   private final String name;
   private final String url;

   AutoValue_Attachments(String var1, String var2, String var3) {
      if(var1 == null) {
         throw new NullPointerException("Null name");
      } else {
         this.name = var1;
         if(var2 == null) {
            throw new NullPointerException("Null url");
         } else {
            this.url = var2;
            if(var3 == null) {
               throw new NullPointerException("Null contentType");
            } else {
               this.contentType = var3;
            }
         }
      }
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(var1 != this) {
         if(var1 instanceof Attachments) {
            Attachments var3 = (Attachments)var1;
            if(!this.name.equals(var3.getName()) || !this.url.equals(var3.getUrl()) || !this.contentType.equals(var3.getContentType())) {
               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public String getContentType() {
      return this.contentType;
   }

   public String getName() {
      return this.name;
   }

   public String getUrl() {
      return this.url;
   }

   public int hashCode() {
      return ((this.name.hashCode() ^ 1000003) * 1000003 ^ this.url.hashCode()) * 1000003 ^ this.contentType.hashCode();
   }

   public String toString() {
      return "Attachments{name=" + this.name + ", url=" + this.url + ", contentType=" + this.contentType + "}";
   }
}
