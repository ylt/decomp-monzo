package io.intercom.android.sdk.models;

import java.util.List;
import java.util.Set;

final class AutoValue_ConversationList extends ConversationList {
   private final List getConversations;
   private final Set getUnreadConversationIds;
   private final boolean hasMorePages;

   AutoValue_ConversationList(List var1, Set var2, boolean var3) {
      if(var1 == null) {
         throw new NullPointerException("Null getConversations");
      } else {
         this.getConversations = var1;
         if(var2 == null) {
            throw new NullPointerException("Null getUnreadConversationIds");
         } else {
            this.getUnreadConversationIds = var2;
            this.hasMorePages = var3;
         }
      }
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(var1 != this) {
         if(var1 instanceof ConversationList) {
            ConversationList var3 = (ConversationList)var1;
            if(!this.getConversations.equals(var3.getConversations()) || !this.getUnreadConversationIds.equals(var3.getUnreadConversationIds()) || this.hasMorePages != var3.hasMorePages()) {
               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public List getConversations() {
      return this.getConversations;
   }

   public Set getUnreadConversationIds() {
      return this.getUnreadConversationIds;
   }

   public boolean hasMorePages() {
      return this.hasMorePages;
   }

   public int hashCode() {
      int var3 = this.getConversations.hashCode();
      int var2 = this.getUnreadConversationIds.hashCode();
      short var1;
      if(this.hasMorePages) {
         var1 = 1231;
      } else {
         var1 = 1237;
      }

      return var1 ^ ((var3 ^ 1000003) * 1000003 ^ var2) * 1000003;
   }

   public String toString() {
      return "ConversationList{getConversations=" + this.getConversations + ", getUnreadConversationIds=" + this.getUnreadConversationIds + ", hasMorePages=" + this.hasMorePages + "}";
   }
}
