package io.intercom.android.sdk.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import io.intercom.android.sdk.utilities.NullSafety;

public abstract class Location implements Parcelable {
   public static final Creator CREATOR = new Creator() {
      public Location createFromParcel(Parcel var1) {
         String var3 = var1.readString();
         String var2 = var1.readString();
         Integer var4;
         if(var1.readByte() == 0) {
            var4 = null;
         } else {
            var4 = Integer.valueOf(var1.readInt());
         }

         return Location.create(var3, var2, var4);
      }

      public Location[] newArray(int var1) {
         return new Location[var1];
      }
   };

   public static Location create(String var0, String var1, Integer var2) {
      return new AutoValue_Location(var0, var1, var2);
   }

   public int describeContents() {
      return 0;
   }

   public abstract String getCityName();

   public abstract String getCountryName();

   public abstract Integer getTimezoneOffset();

   public void writeToParcel(Parcel var1, int var2) {
      var1.writeString(this.getCityName());
      var1.writeString(this.getCountryName());
      Integer var3 = this.getTimezoneOffset();
      if(var3 == null) {
         var1.writeByte(0);
      } else {
         var1.writeByte(1);
         var1.writeInt(var3.intValue());
      }

   }

   public static final class Builder {
      String city_name;
      String country_name;
      Integer timezone_offset;

      public Location build() {
         return Location.create(NullSafety.valueOrEmpty(this.city_name), NullSafety.valueOrEmpty(this.country_name), this.timezone_offset);
      }
   }
}
