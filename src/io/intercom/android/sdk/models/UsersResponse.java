package io.intercom.android.sdk.models;

public class UsersResponse extends BaseResponse {
   private final ConversationList unreadConversations;

   UsersResponse(UsersResponse.Builder var1) {
      super(var1);
      ConversationList var2;
      if(var1.unread_conversations == null) {
         var2 = (new ConversationList.Builder()).build();
      } else {
         var2 = var1.unread_conversations.build();
      }

      this.unreadConversations = var2;
   }

   public ConversationList getUnreadConversations() {
      return this.unreadConversations;
   }

   public static class Builder extends BaseResponse.Builder {
      ConversationList.Builder unread_conversations;

      public UsersResponse build() {
         return new UsersResponse(this);
      }
   }
}
