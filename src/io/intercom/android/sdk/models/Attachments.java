package io.intercom.android.sdk.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import io.intercom.android.sdk.utilities.NullSafety;

public abstract class Attachments implements Parcelable {
   public static final Creator CREATOR = new Creator() {
      public Attachments createFromParcel(Parcel var1) {
         return Attachments.create(var1.readString(), var1.readString(), var1.readString());
      }

      public Attachments[] newArray(int var1) {
         return new Attachments[var1];
      }
   };

   static Attachments create(String var0, String var1, String var2) {
      return new AutoValue_Attachments(var0, var1, var2);
   }

   public int describeContents() {
      return 0;
   }

   public abstract String getContentType();

   public abstract String getName();

   public abstract String getUrl();

   public void writeToParcel(Parcel var1, int var2) {
      var1.writeString(this.getName());
      var1.writeString(this.getUrl());
      var1.writeString(this.getContentType());
   }

   public static final class Builder {
      String content_type;
      String name;
      String url;

      public Attachments build() {
         return Attachments.create(NullSafety.valueOrEmpty(this.name), NullSafety.valueOrEmpty(this.url), NullSafety.valueOrEmpty(this.content_type));
      }
   }
}
