package io.intercom.android.sdk.models;

import io.intercom.android.sdk.blocks.models.Author;
import io.intercom.android.sdk.utilities.NullSafety;

public abstract class Card {
   public static Card create(String var0, String var1, String var2, String var3, Author var4) {
      return new AutoValue_Card(var0, var1, var2, var3, var4);
   }

   public abstract Author getAuthor();

   public abstract String getDescription();

   public abstract String getText();

   public abstract String getTitle();

   public abstract String getType();

   public static final class Builder {
      String articleId;
      Author.Builder author;
      String description;
      String linkType;
      String text;
      String title;
      String type;

      public Card build() {
         Author var1;
         if(this.author == null) {
            var1 = (new Author.Builder()).build();
         } else {
            var1 = this.author.build();
         }

         return Card.create(NullSafety.valueOrEmpty(this.type), NullSafety.valueOrEmpty(this.text), NullSafety.valueOrEmpty(this.title), NullSafety.valueOrEmpty(this.description), var1);
      }
   }
}
