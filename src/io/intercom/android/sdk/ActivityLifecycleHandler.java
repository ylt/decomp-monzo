package io.intercom.android.sdk;

import android.app.Activity;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import io.intercom.android.sdk.actions.Actions;
import io.intercom.android.sdk.store.Store;

class ActivityLifecycleHandler extends Handler {
   private static final int ESTIMATED_ACTIVITY_TRANSITION_DURATION_MS = 500;
   static final int PAUSED_CODE = 2;
   static final int READY_FOR_VIEW_CODE = 1;
   static final int RESUMED_CODE = 0;
   static final int STOPPED_CODE = 3;
   private final long readyForViewDelayMs;
   private final Store store;

   ActivityLifecycleHandler(Looper var1, Store var2, float var3) {
      super(var1);
      this.store = var2;
      this.readyForViewDelayMs = (long)(500.0F * var3);
   }

   static Message getMessage(int var0, Activity var1) {
      Message var2 = Message.obtain();
      var2.what = var0;
      var2.obj = var1;
      return var2;
   }

   public void handleMessage(Message var1) {
      Activity var2 = (Activity)var1.obj;
      switch(var1.what) {
      case 0:
         this.sendMessageAfterDelay(getMessage(1, var2), this.readyForViewDelayMs);
         break;
      case 1:
         this.store.dispatch(Actions.activityReadyForViewAttachment(var2));
         break;
      case 2:
         this.store.dispatch(Actions.activityPaused(var2));
         break;
      case 3:
         this.store.dispatch(Actions.activityStopped(var2));
      }

   }

   void sendMessageAfterDelay(Message var1, long var2) {
      this.sendMessageDelayed(var1, var2);
   }
}
