package io.intercom.android.sdk.commons;

public final class BuildConfig {
   public static final String APPLICATION_ID = "io.intercom.android.sdk.commons";
   public static final String BUILD_TYPE = "release";
   public static final boolean DEBUG = false;
   public static final String FLAVOR = "";
   public static final int VERSION_CODE = 12;
   public static final String VERSION_NAME = "1.0.2";
}
