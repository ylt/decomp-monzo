package io.intercom.android.sdk.commons.utilities;

import android.content.Context;
import android.graphics.Point;
import android.view.WindowManager;

public class ScreenUtils {
   @Deprecated
   public static int convertDpToPixel(float var0, Context var1) {
      return dpToPx(var0, var1);
   }

   @Deprecated
   public static int convertPixelsToDp(float var0, Context var1) {
      return pxToDp(var0, var1);
   }

   public static int dpToPx(float var0, Context var1) {
      return (int)(var1.getResources().getDisplayMetrics().density * var0);
   }

   public static Point getScreenDimensions(Context var0) {
      Point var1 = new Point();
      ((WindowManager)var0.getSystemService("window")).getDefaultDisplay().getSize(var1);
      return var1;
   }

   public static int pxToDp(float var0, Context var1) {
      return (int)(var0 / var1.getResources().getDisplayMetrics().density);
   }
}
