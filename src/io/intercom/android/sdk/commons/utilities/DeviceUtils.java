package io.intercom.android.sdk.commons.utilities;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import java.util.UUID;

public class DeviceUtils {
   public static String generateDeviceId(Context var0) {
      return generateDeviceId(getTelephonyDeviceId(var0), getWifiMacAddress(var0), getAndroidId(var0));
   }

   public static String generateDeviceId(String var0, String var1, String var2) {
      if(TextUtils.isEmpty(var0)) {
         String var3;
         if(!TextUtils.isEmpty(var1) && !TextUtils.isEmpty(var2)) {
            var3 = (new UUID((long)var2.hashCode(), (long)var1.hashCode())).toString();
         } else {
            var3 = var2;
            if(TextUtils.isEmpty(var2)) {
               if(!TextUtils.isEmpty(var1)) {
                  var3 = var1;
               } else {
                  var3 = var0;
               }
            }
         }

         var0 = var3;
         if(TextUtils.isEmpty(var3)) {
            var0 = UUID.randomUUID().toString();
         }
      }

      return var0;
   }

   static String getAndroidId(Context var0) {
      String var1 = Secure.getString(var0.getContentResolver(), "android_id");
      String var2;
      if(var1 != null) {
         var2 = var1;
         if(!var1.equalsIgnoreCase("9774d56d682e549c")) {
            return var2;
         }
      }

      var2 = "";
      return var2;
   }

   public static String getAppName(Context var0) {
      String var1 = var0.getPackageName();
      String var2 = var1;
      if(var1 == null) {
         var2 = "";
      }

      return var2;
   }

   public static String getAppVersion(Context param0) {
      // $FF: Couldn't be decompiled
   }

   static String getTelephonyDeviceId(Context var0) {
      String var3;
      if(hasPermission(var0, "android.permission.READ_PHONE_STATE")) {
         try {
            var3 = ((TelephonyManager)var0.getSystemService("phone")).getDeviceId();
         } catch (Exception var2) {
            var3 = null;
         }
      } else {
         var3 = null;
      }

      String var1 = var3;
      if(var3 == null) {
         var1 = "";
      }

      return var1;
   }

   static String getWifiMacAddress(Context var0) {
      String var4;
      label18: {
         if(hasPermission(var0, "android.permission.ACCESS_WIFI_STATE")) {
            WifiManager var2 = (WifiManager)var0.getSystemService("wifi");
            if(var2 != null) {
               WifiInfo var3 = var2.getConnectionInfo();
               if(var3 != null) {
                  var4 = var3.getMacAddress();
                  break label18;
               }
            }
         }

         var4 = null;
      }

      String var1 = var4;
      if(var4 == null) {
         var1 = "";
      }

      return var1;
   }

   public static boolean hasPermission(Context var0, String var1) {
      boolean var2;
      if(var0.checkCallingOrSelfPermission(var1) == 0) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }
}
