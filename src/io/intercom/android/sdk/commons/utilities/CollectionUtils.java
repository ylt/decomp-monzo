package io.intercom.android.sdk.commons.utilities;

import java.util.Collection;

public class CollectionUtils {
   public static int capacityFor(Collection var0) {
      int var1;
      if(var0 == null) {
         var1 = 0;
      } else {
         var1 = var0.size();
      }

      return var1;
   }
}
