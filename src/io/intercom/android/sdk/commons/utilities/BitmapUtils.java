package io.intercom.android.sdk.commons.utilities;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Bitmap.Config;
import android.graphics.Paint.Style;
import android.graphics.Shader.TileMode;

public class BitmapUtils {
   public static Bitmap transformRound(Bitmap var0, BitmapUtils.BitmapCache var1) {
      int var3 = Math.min(var0.getWidth(), var0.getHeight());
      int var5 = (var0.getWidth() - var3) / 2;
      int var4 = (var0.getHeight() - var3) / 2;
      Bitmap var6 = var1.get(var3, var3, Config.ARGB_8888);
      Bitmap var10 = var6;
      if(var6 == null) {
         var10 = Bitmap.createBitmap(var3, var3, Config.ARGB_8888);
      }

      Canvas var7 = new Canvas(var10);
      Paint var11 = new Paint();
      BitmapShader var9 = new BitmapShader(var0, TileMode.CLAMP, TileMode.CLAMP);
      if(var5 != 0 || var4 != 0) {
         Matrix var8 = new Matrix();
         var8.setTranslate((float)(-var5), (float)(-var4));
         var9.setLocalMatrix(var8);
      }

      var11.setShader(var9);
      var11.setAntiAlias(true);
      float var2 = (float)var3 / 2.0F;
      var7.drawCircle(var2, var2, var2, var11);
      return var10;
   }

   public static Bitmap transformRound(Bitmap var0, BitmapUtils.BitmapCache var1, int var2, int var3) {
      int var7 = Math.min(var0.getWidth(), var0.getHeight());
      float var4 = (float)var7 / 2.0F;
      Bitmap var8 = var1.get(var7, var7, Config.ARGB_8888);
      Bitmap var11 = var8;
      if(var8 == null) {
         var11 = Bitmap.createBitmap(var7, var7, Config.ARGB_8888);
      }

      Paint var9 = new Paint();
      var9.setAntiAlias(true);
      var9.setShader(new BitmapShader(var0, TileMode.CLAMP, TileMode.CLAMP));
      Canvas var12 = new Canvas(var11);
      float var5 = (float)((var0.getWidth() - var3) / 2 + var3 / 2);
      float var6 = (float)((var0.getHeight() - var3) / 2 + var3 / 2);
      var12.drawCircle(var5, var6, var4 - (float)var3, var9);
      Paint var10 = new Paint();
      var10.setColor(var2);
      var10.setStyle(Style.STROKE);
      var10.setAntiAlias(true);
      var10.setStrokeWidth(2.0F);
      var12.drawCircle(var5, var6, var4 - (float)var3, var10);
      return var11;
   }

   public static Bitmap transformRoundCorners(Bitmap var0, BitmapUtils.BitmapCache var1, int var2) {
      int var4 = var0.getWidth();
      int var3 = var0.getHeight();
      Bitmap var5 = var1.get(var4, var3, Config.ARGB_8888);
      Bitmap var8 = var5;
      if(var5 == null) {
         var8 = Bitmap.createBitmap(var4, var3, Config.ARGB_8888);
      }

      Canvas var6 = new Canvas(var8);
      Paint var7 = new Paint();
      var7.setAntiAlias(true);
      var7.setShader(new BitmapShader(var0, TileMode.CLAMP, TileMode.CLAMP));
      var6.drawRoundRect(new RectF(0.0F, 0.0F, (float)var4, (float)var3), (float)var2, (float)var2, var7);
      return var8;
   }

   public static Bitmap transformRoundCorners(Bitmap var0, BitmapUtils.BitmapCache var1, int var2, int var3, int var4, int var5, int var6) {
      Bitmap var7 = var1.get(var2, var3, Config.ARGB_8888);
      Bitmap var10 = var7;
      if(var7 == null) {
         var10 = Bitmap.createBitmap(var2, var3, Config.ARGB_8888);
      }

      Canvas var11 = new Canvas(var10);
      Paint var8 = new Paint();
      var8.setAntiAlias(true);
      var8.setShader(new BitmapShader(Bitmap.createScaledBitmap(var0, var2, var3, false), TileMode.CLAMP, TileMode.CLAMP));
      RectF var9 = new RectF((float)var6, (float)var6, (float)(var2 - var6), (float)(var3 - var6));
      var11.drawRoundRect(var9, (float)var4, (float)var4, var8);
      var8 = new Paint();
      var8.setColor(var5);
      var8.setStyle(Style.STROKE);
      var8.setAntiAlias(true);
      var8.setStrokeWidth((float)var6);
      var11.drawRoundRect(var9, (float)var4, (float)var4, var8);
      return var10;
   }

   public interface BitmapCache {
      Bitmap get(int var1, int var2, Config var3);
   }
}
