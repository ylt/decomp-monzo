package io.intercom.android.sdk.commons.utilities;

import java.util.ArrayList;
import java.util.List;

public class StringUtils {
   public static List splitOnEmpty(String var0) {
      ArrayList var3 = new ArrayList(var0.length());
      String[] var4 = var0.split("");
      int var2 = var4.length;

      for(int var1 = 0; var1 < var2; ++var1) {
         var0 = var4[var1];
         if(!var0.isEmpty()) {
            var3.add(var0);
         }
      }

      return var3;
   }
}
