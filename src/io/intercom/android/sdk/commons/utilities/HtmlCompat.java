package io.intercom.android.sdk.commons.utilities;

import android.os.Build.VERSION;
import android.text.Html;
import android.text.Spanned;

public class HtmlCompat {
   public static Spanned fromHtml(String var0) {
      Spanned var1;
      if(VERSION.SDK_INT >= 24) {
         var1 = Html.fromHtml(var0, 0);
      } else {
         var1 = Html.fromHtml(var0);
      }

      return var1;
   }
}
