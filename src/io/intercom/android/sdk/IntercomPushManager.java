package io.intercom.android.sdk;

import android.app.Application;
import android.content.Context;
import io.intercom.android.sdk.logger.LumberMill;
import io.intercom.android.sdk.twig.Twig;

public class IntercomPushManager {
   public static final String HOST_APP_INTENT = "host_app_intent";
   public static final String INTERCOM_PUSH_KEY = "intercom_push_key";
   public static final String INTERCOM_PUSH_PATH = "intercom_push_notification_path";
   public static final String MULTIPLE_NOTIFICATIONS = "multiple_notifications";
   private static final String PREFS_SENDER_ID = "intercom_sender_id";
   public static final String PUSH_ONLY_ID = "push_only_convo_id";
   private static final Twig TWIG = LumberMill.getLogger();
   public static IntercomPushManager.GcmImplementation gcmImplementation = new IntercomPushManager.GcmImplementation() {
      public void registerToken(Application var1) {
      }
   };

   static void cacheSenderId(Context var0, String var1) {
      var0.getSharedPreferences("INTERCOM_SDK_PREFS", 0).edit().putString("intercom_sender_id", var1).apply();
   }

   private static boolean fcmModuleInstalled() {
      boolean var0;
      if(getFcmMessengerClass() != null && getFcmInstanceIdClass() != null) {
         var0 = true;
      } else {
         var0 = false;
      }

      return var0;
   }

   private static boolean gcmModuleInstalled() {
      boolean var0;
      if(getGcmListenerClass() != null && getGcmImplementationClass() != null) {
         var0 = true;
      } else {
         var0 = false;
      }

      return var0;
   }

   private static Class getFcmInstanceIdClass() {
      Class var0;
      try {
         var0 = Class.forName("io.intercom.android.sdk.fcm.IntercomFcmInstanceIdService");
      } catch (ClassNotFoundException var1) {
         var0 = null;
      }

      return var0;
   }

   private static Class getFcmMessengerClass() {
      Class var0;
      try {
         var0 = Class.forName("io.intercom.android.sdk.fcm.IntercomFcmMessengerService");
      } catch (ClassNotFoundException var1) {
         var0 = null;
      }

      return var0;
   }

   private static Class getGcmImplementationClass() {
      Class var0;
      try {
         var0 = Class.forName("io.intercom.android.sdk.gcm.IntercomGcmImplementation");
      } catch (ClassNotFoundException var1) {
         var0 = null;
      }

      return var0;
   }

   private static Class getGcmListenerClass() {
      Class var0;
      try {
         var0 = Class.forName("io.intercom.android.sdk.gcm.IntercomGcmListenerService");
      } catch (ClassNotFoundException var1) {
         var0 = null;
      }

      return var0;
   }

   static IntercomPushManager.IntercomPushIntegrationType getInstalledModuleType() {
      IntercomPushManager.IntercomPushIntegrationType var2 = IntercomPushManager.IntercomPushIntegrationType.NONE;
      boolean var0 = fcmModuleInstalled();
      boolean var1 = gcmModuleInstalled();
      if(var0 && var1) {
         TWIG.internal("Both FCM and GCM are installed");
         var2 = IntercomPushManager.IntercomPushIntegrationType.BOTH;
      } else if(var0) {
         TWIG.internal("FCM is installed");
         var2 = IntercomPushManager.IntercomPushIntegrationType.FCM;
      } else if(var1) {
         TWIG.internal("GCM is installed");
         var2 = IntercomPushManager.IntercomPushIntegrationType.GCM;
      }

      return var2;
   }

   public static String getSenderId(Context var0) {
      return var0.getSharedPreferences("INTERCOM_SDK_PREFS", 0).getString("intercom_sender_id", "");
   }

   static void registerGcmToken(Context var0) {
      gcmImplementation.registerToken((Application)var0.getApplicationContext());
   }

   public interface GcmImplementation {
      void registerToken(Application var1);
   }

   static enum IntercomPushIntegrationType {
      BOTH,
      FCM,
      GCM,
      NONE;
   }
}
