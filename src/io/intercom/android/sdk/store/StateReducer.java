package io.intercom.android.sdk.store;

import android.net.Uri;
import io.intercom.android.sdk.actions.Action;
import io.intercom.android.sdk.models.TeamPresence;
import io.intercom.android.sdk.state.ActiveConversationState;
import io.intercom.android.sdk.state.HostAppState;
import io.intercom.android.sdk.state.InboxState;
import io.intercom.android.sdk.state.OverlayState;
import io.intercom.android.sdk.state.State;
import io.intercom.android.sdk.state.UiState;
import java.util.Set;

class StateReducer implements Store.Reducer {
   private final Store.Reducer activeConversationStateReducer;
   private final Store.Reducer hasConversationsReducer;
   private final Store.Reducer hostAppStateReducer;
   private final Store.Reducer inboxStateReducer;
   private final Store.Reducer overlayStateReducer;
   private final Store.Reducer screenshotReducer;
   private final Store.Reducer teamPresenceReducer;
   private final Store.Reducer uiStateReducer;
   private final Store.Reducer unreadConversationIdsReducer;

   StateReducer(Store.Reducer var1, Store.Reducer var2, Store.Reducer var3, Store.Reducer var4, Store.Reducer var5, Store.Reducer var6, Store.Reducer var7, Store.Reducer var8, Store.Reducer var9) {
      this.hasConversationsReducer = var1;
      this.uiStateReducer = var2;
      this.teamPresenceReducer = var3;
      this.unreadConversationIdsReducer = var4;
      this.inboxStateReducer = var5;
      this.hostAppStateReducer = var6;
      this.overlayStateReducer = var7;
      this.screenshotReducer = var8;
      this.activeConversationStateReducer = var9;
   }

   public State reduce(Action var1, State var2) {
      return State.create(((Boolean)this.hasConversationsReducer.reduce(var1, Boolean.valueOf(var2.hasConversations()))).booleanValue(), (UiState)this.uiStateReducer.reduce(var1, var2.uiState()), (TeamPresence)this.teamPresenceReducer.reduce(var1, var2.teamPresence()), (Set)this.unreadConversationIdsReducer.reduce(var1, var2.unreadConversationIds()), (InboxState)this.inboxStateReducer.reduce(var1, var2.inboxState()), (HostAppState)this.hostAppStateReducer.reduce(var1, var2.hostAppState()), (OverlayState)this.overlayStateReducer.reduce(var1, var2.overlayState()), (Uri)this.screenshotReducer.reduce(var1, var2.lastScreenshot()), (ActiveConversationState)this.activeConversationStateReducer.reduce(var1, var2.activeConversationState()));
   }
}
