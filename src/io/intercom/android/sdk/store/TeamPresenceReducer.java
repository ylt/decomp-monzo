package io.intercom.android.sdk.store;

import io.intercom.android.sdk.actions.Action;
import io.intercom.android.sdk.models.TeamPresence;
import java.util.Collections;

class TeamPresenceReducer implements Store.Reducer {
   static final TeamPresence INITIAL_STATE = TeamPresence.create(Collections.emptyList(), "", "");

   public TeamPresence reduce(Action var1, TeamPresence var2) {
      TeamPresence var3;
      switch(null.$SwitchMap$io$intercom$android$sdk$actions$Action$Type[var1.type().ordinal()]) {
      case 1:
         var3 = (TeamPresence)var1.value();
         break;
      default:
         var3 = var2;
      }

      return var3;
   }
}
