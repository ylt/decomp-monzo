package io.intercom.android.sdk.store;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import io.intercom.android.sdk.Provider;
import io.intercom.android.sdk.conversation.SoundPlayer;
import io.intercom.android.sdk.middleware.ApiMiddleware;
import io.intercom.android.sdk.middleware.AppConfigMiddleware;
import io.intercom.android.sdk.middleware.AudioMiddleware;
import io.intercom.android.sdk.middleware.FirstMessageMiddleware;
import io.intercom.android.sdk.middleware.LoggerMiddleware;
import io.intercom.android.sdk.middleware.NexusClientMiddleware;
import io.intercom.android.sdk.middleware.OperatorClientConditionsMiddleware;
import io.intercom.android.sdk.middleware.OverlayManagerMiddleware;
import io.intercom.android.sdk.middleware.ScreenshotMiddleware;
import io.intercom.android.sdk.middleware.UserIdentityMiddleware;
import io.intercom.android.sdk.middleware.UserUpdateBatcherMiddleware;
import io.intercom.android.sdk.state.State;
import io.intercom.android.sdk.twig.Twig;
import io.intercom.com.a.a.b;

public class StoreFactory {
   public static Store createStore(Provider var0, Provider var1, Provider var2, Provider var3, Provider var4, SoundPlayer var5, Provider var6, Context var7, Twig var8, b var9) {
      return new Store(new StateReducer(new HasConversationsReducer(), new LastViewReducer(), new TeamPresenceReducer(), new UnreadConversationsReducer(), new InboxStateReducer(), new HostAppStateReducer(), new OverlayStateReducer(), new ScreenshotReducer(), new ActiveConversationStateReducer()), State.create(false, LastViewReducer.INITIAL_STATE, TeamPresenceReducer.INITIAL_STATE, UnreadConversationsReducer.INITIAL_STATE, InboxStateReducer.INITIAL_STATE, HostAppStateReducer.INITIAL_STATE, OverlayStateReducer.INITIAL_STATE, ScreenshotReducer.INITIAL_STATE, ActiveConversationStateReducer.INITIAL_STATE), new Store.Middleware[]{new LoggerMiddleware(var8), new ApiMiddleware(var0), new UserUpdateBatcherMiddleware(var4, var1, var6), new NexusClientMiddleware(var2, var1, new Handler(Looper.getMainLooper())), new OverlayManagerMiddleware(var3), new AudioMiddleware(var5, var6), new FirstMessageMiddleware(var8, var7), new ScreenshotMiddleware(var1, var7), new UserIdentityMiddleware(var6), new AppConfigMiddleware(var1, var9), new OperatorClientConditionsMiddleware(var0, new Handler(Looper.getMainLooper()))});
   }
}
