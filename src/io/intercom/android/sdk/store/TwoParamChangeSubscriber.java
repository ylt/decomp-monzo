package io.intercom.android.sdk.store;

import android.os.Handler;
import android.os.Looper;

class TwoParamChangeSubscriber implements Store.Subscriber {
   private Object last1;
   private Object last2;
   private final Handler mainHandler;
   private final Store.Selector selector1;
   private final Store.Selector selector2;
   final Store.Subscriber2 subscriber;

   TwoParamChangeSubscriber(Store.Selector var1, Store.Selector var2, Store.Subscriber2 var3) {
      this(var1, var2, var3, new Handler(Looper.getMainLooper()));
   }

   TwoParamChangeSubscriber(Store.Selector var1, Store.Selector var2, Store.Subscriber2 var3, Handler var4) {
      this.last1 = null;
      this.last2 = null;
      this.selector1 = var1;
      this.selector2 = var2;
      this.subscriber = var3;
      this.mainHandler = var4;
   }

   public void onStateChange(final Object var1) {
      final Object var2 = this.selector1.transform(var1);
      var1 = this.selector2.transform(var1);
      if(this.last1 != var2 || this.last2 != var1) {
         this.last1 = var2;
         this.last2 = var1;
         ChangeSubscriber.runOnMainThread(new Runnable() {
            public void run() {
               TwoParamChangeSubscriber.this.subscriber.onStateChange(var2, var1);
            }
         }, this.mainHandler);
      }

   }
}
