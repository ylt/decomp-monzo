package io.intercom.android.sdk.store;

import io.intercom.android.sdk.actions.Action;
import io.intercom.android.sdk.models.Conversation;
import io.intercom.android.sdk.state.UiState;

class LastViewReducer implements Store.Reducer {
   static final UiState INITIAL_STATE;

   static {
      INITIAL_STATE = UiState.create(UiState.Screen.NONE, (String)null);
   }

   public UiState reduce(Action var1, UiState var2) {
      switch(null.$SwitchMap$io$intercom$android$sdk$actions$Action$Type[var1.type().ordinal()]) {
      case 1:
         var2 = UiState.create(UiState.Screen.INBOX, (String)null);
         break;
      case 2:
         var2 = UiState.create(UiState.Screen.COMPOSER, (String)null);
         break;
      case 3:
         var2 = UiState.create(UiState.Screen.CONVERSATION, (String)var1.value());
         break;
      case 4:
         var2 = UiState.create(UiState.Screen.CONVERSATION, ((Conversation)var1.value()).getId());
         break;
      case 5:
      case 6:
         var2 = UiState.create(UiState.Screen.NONE, (String)null);
      }

      return var2;
   }
}
