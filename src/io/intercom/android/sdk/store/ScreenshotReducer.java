package io.intercom.android.sdk.store;

import android.net.Uri;
import io.intercom.android.sdk.actions.Action;

class ScreenshotReducer implements Store.Reducer {
   static final Uri INITIAL_STATE;

   static {
      INITIAL_STATE = Uri.EMPTY;
   }

   public Uri reduce(Action var1, Uri var2) {
      Uri var3;
      switch(null.$SwitchMap$io$intercom$android$sdk$actions$Action$Type[var1.type().ordinal()]) {
      case 1:
         var3 = (Uri)var1.value();
         break;
      case 2:
      case 3:
         var3 = INITIAL_STATE;
         break;
      default:
         var3 = var2;
      }

      return var3;
   }
}
