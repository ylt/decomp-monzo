package io.intercom.android.sdk.store;

import io.intercom.android.sdk.actions.Action;
import io.intercom.android.sdk.models.Conversation;
import io.intercom.android.sdk.state.ActiveConversationState;

public class ActiveConversationStateReducer implements Store.Reducer {
   public static final ActiveConversationState INITIAL_STATE = ActiveConversationState.create("", false, false);

   public ActiveConversationState reduce(Action var1, ActiveConversationState var2) {
      switch(null.$SwitchMap$io$intercom$android$sdk$actions$Action$Type[var1.type().ordinal()]) {
      case 1:
         var2 = ActiveConversationState.create(var2.getConversationId(), var2.hasSwitchedInputType(), true);
         break;
      case 2:
         var2 = ActiveConversationState.create(var2.getConversationId(), var2.hasSwitchedInputType(), false);
         break;
      case 3:
         var2 = ActiveConversationState.create(var2.getConversationId(), true, var2.hasTextInComposer());
         break;
      case 4:
         var2 = ActiveConversationState.create(var2.getConversationId(), INITIAL_STATE.hasSwitchedInputType(), INITIAL_STATE.hasTextInComposer());
         break;
      case 5:
      case 6:
         var2 = ActiveConversationState.create(((Conversation)var1.value()).getId(), var2.hasSwitchedInputType(), var2.hasTextInComposer());
         break;
      case 7:
         var2 = ActiveConversationState.create((String)var1.value(), var2.hasSwitchedInputType(), var2.hasTextInComposer());
         break;
      case 8:
         var2 = INITIAL_STATE;
      }

      return var2;
   }
}
