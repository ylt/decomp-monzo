package io.intercom.android.sdk.store;

import io.intercom.android.sdk.actions.Action;
import io.intercom.android.sdk.models.Conversation;
import io.intercom.android.sdk.models.ConversationsResponse;
import io.intercom.android.sdk.models.UsersResponse;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

class UnreadConversationsReducer implements Store.Reducer {
   static final Set INITIAL_STATE = Collections.emptySet();

   public Set reduce(Action var1, Set var2) {
      switch(null.$SwitchMap$io$intercom$android$sdk$actions$Action$Type[var1.type().ordinal()]) {
      case 1:
         var2 = new HashSet((Collection)var2);
         ((HashSet)var2).remove((String)var1.value());
         break;
      case 2:
         var2 = new HashSet(((UsersResponse)var1.value()).getUnreadConversations().getUnreadConversationIds());
         break;
      case 3:
         var2 = new HashSet(((ConversationsResponse)var1.value()).getConversationPage().getUnreadConversationIds());
         break;
      case 4:
         var2 = new HashSet((Collection)var2);
         Conversation var3 = (Conversation)var1.value();
         if(var3.isRead()) {
            ((HashSet)var2).remove(var3.getId());
         } else {
            ((HashSet)var2).add(var3.getId());
         }
         break;
      case 5:
         var2 = INITIAL_STATE;
      }

      return (Set)var2;
   }
}
