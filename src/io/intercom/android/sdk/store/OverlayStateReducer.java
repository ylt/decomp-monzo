package io.intercom.android.sdk.store;

import android.annotation.SuppressLint;
import android.app.Activity;
import io.intercom.android.sdk.Intercom;
import io.intercom.android.sdk.actions.Action;
import io.intercom.android.sdk.models.Conversation;
import io.intercom.android.sdk.models.Part;
import io.intercom.android.sdk.models.UsersResponse;
import io.intercom.android.sdk.state.OverlayState;
import io.intercom.android.sdk.utilities.ActivityUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

class OverlayStateReducer implements Store.Reducer {
   @SuppressLint({"StaticFieldLeak"})
   static final OverlayState INITIAL_STATE;

   static {
      INITIAL_STATE = OverlayState.create(Collections.emptyList(), Collections.emptySet(), Intercom.Visibility.VISIBLE, Intercom.Visibility.GONE, (Activity)null, (Activity)null, 0);
   }

   private static OverlayState newStateIfNewHostActivity(OverlayState var0, Activity var1, Activity var2) {
      if(var0.resumedHostActivity() != var1 || var0.pausedHostActivity() != var2) {
         var0 = var0.toBuilder().resumedHostActivity(var1).pausedHostActivity(var2).build();
      }

      return var0;
   }

   private static void removeBadgeConversations(List var0) {
      for(int var1 = var0.size() - 1; var1 >= 0; --var1) {
         if(((Conversation)var0.get(var1)).getLastPart().getDeliveryOption() == Part.DeliveryOption.BADGE) {
            var0.remove(var1);
         }
      }

   }

   private static void removeConversationWithId(List var0, String var1) {
      Iterator var2 = var0.iterator();

      while(var2.hasNext()) {
         Conversation var3 = (Conversation)var2.next();
         if(var3.getId().equals(var1)) {
            var0.remove(var3);
            break;
         }
      }

   }

   private static void removeDismissedConversations(List var0, Set var1) {
      for(int var2 = var0.size() - 1; var2 >= 0; --var2) {
         if(var1.contains(((Conversation)var0.get(var2)).getLastPart().getId())) {
            var0.remove(var2);
         }
      }

   }

   public OverlayState reduce(Action var1, OverlayState var2) {
      Conversation var3 = null;
      Activity var5;
      ArrayList var6;
      switch(null.$SwitchMap$io$intercom$android$sdk$actions$Action$Type[var1.type().ordinal()]) {
      case 1:
         var2 = var2.toBuilder().notificationVisibility((Intercom.Visibility)var1.value()).build();
         break;
      case 2:
         var2 = var2.toBuilder().launcherVisibility((Intercom.Visibility)var1.value()).build();
         break;
      case 3:
         var2 = var2.toBuilder().bottomPadding(((Integer)var1.value()).intValue()).build();
         break;
      case 4:
         var2 = var2.toBuilder().conversations(Collections.emptyList()).dismissedPartIds(Collections.emptySet()).build();
         break;
      case 5:
         ArrayList var7 = new ArrayList(var2.conversations());
         removeConversationWithId(var7, (String)var1.value());
         var2 = var2.toBuilder().conversations(var7).build();
         break;
      case 6:
         var3 = (Conversation)var1.value();
         var6 = new ArrayList(var2.conversations());
         HashSet var4 = new HashSet(var2.dismissedPartIds());
         var4.add(var3.getLastPart().getId());
         removeDismissedConversations(var6, var4);
         var2 = var2.toBuilder().conversations(var6).dismissedPartIds(var4).build();
         break;
      case 7:
         var6 = new ArrayList(((UsersResponse)var1.value()).getUnreadConversations().getConversations());
         removeDismissedConversations(var6, var2.dismissedPartIds());
         removeBadgeConversations(var6);
         var2 = var2.toBuilder().conversations(var6).build();
         break;
      case 8:
         var5 = (Activity)var1.value();
         if(!ActivityUtils.isHostActivity(var5)) {
            var5 = null;
         }

         var2 = newStateIfNewHostActivity(var2, var5, (Activity)null);
         break;
      case 9:
         var2 = newStateIfNewHostActivity(var2, (Activity)null, (Activity)var1.value());
         break;
      case 10:
         if((Activity)var1.value() == var2.pausedHostActivity()) {
            var5 = var3;
         } else {
            var5 = var2.pausedHostActivity();
         }

         var2 = newStateIfNewHostActivity(var2, var2.resumedHostActivity(), var5);
         break;
      case 11:
         var2 = newStateIfNewHostActivity(var2, (Activity)null, (Activity)null);
      }

      return var2;
   }
}
