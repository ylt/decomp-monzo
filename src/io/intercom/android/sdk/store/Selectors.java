package io.intercom.android.sdk.store;

import io.intercom.android.sdk.models.TeamPresence;
import io.intercom.android.sdk.state.ActiveConversationState;
import io.intercom.android.sdk.state.InboxState;
import io.intercom.android.sdk.state.OverlayState;
import io.intercom.android.sdk.state.State;

public class Selectors {
   public static final Store.Selector ACTIVE_CONVERSATION = new Store.Selector() {
      public ActiveConversationState transform(State var1) {
         return var1.activeConversationState();
      }
   };
   public static final Store.Selector APP_IS_BACKGROUNDED = new Store.Selector() {
      public Boolean transform(State var1) {
         return Boolean.valueOf(var1.hostAppState().isBackgrounded());
      }
   };
   public static final Store.Selector INBOX = new Store.Selector() {
      public InboxState transform(State var1) {
         return var1.inboxState();
      }
   };
   public static final Store.Selector OVERLAY = new Store.Selector() {
      public OverlayState transform(State var1) {
         return var1.overlayState();
      }
   };
   public static final Store.Selector SESSION_STARTED_SINCE_LAST_BACKGROUNDED = new Store.Selector() {
      public Boolean transform(State var1) {
         return Boolean.valueOf(var1.hostAppState().sessionStartedSinceLastBackgrounded());
      }
   };
   public static final Store.Selector TEAM_PRESENCE = new Store.Selector() {
      public TeamPresence transform(State var1) {
         return var1.teamPresence();
      }
   };
   public static final Store.Selector UNREAD_COUNT = new Store.Selector() {
      public Integer transform(State var1) {
         return Integer.valueOf(var1.unreadConversationIds().size());
      }
   };
}
