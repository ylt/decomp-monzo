package io.intercom.android.sdk.store;

import io.intercom.android.sdk.actions.Action;
import io.intercom.android.sdk.state.HostAppState;

class HostAppStateReducer implements Store.Reducer {
   static final HostAppState INITIAL_STATE = HostAppState.create(true, false, 0L);

   public HostAppState reduce(Action var1, HostAppState var2) {
      switch(null.$SwitchMap$io$intercom$android$sdk$actions$Action$Type[var1.type().ordinal()]) {
      case 1:
         var2 = HostAppState.create(false, false, var2.backgroundedTimestamp());
         break;
      case 2:
         var2 = HostAppState.create(true, var2.sessionStartedSinceLastBackgrounded(), ((Long)var1.value()).longValue());
         break;
      case 3:
         var2 = HostAppState.create(var2.isBackgrounded(), true, var2.backgroundedTimestamp());
         break;
      case 4:
         var2 = HostAppState.create(var2.isBackgrounded(), false, var2.backgroundedTimestamp());
      }

      return var2;
   }
}
