package io.intercom.android.sdk.store;

import io.intercom.android.sdk.actions.Action;
import io.intercom.android.sdk.models.Conversation;
import io.intercom.android.sdk.models.ConversationsResponse;
import io.intercom.android.sdk.state.InboxState;
import io.intercom.android.sdk.state.ReplyPart;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

class InboxStateReducer implements Store.Reducer {
   static final InboxState INITIAL_STATE;

   static {
      INITIAL_STATE = InboxState.create(Collections.emptyList(), InboxState.Status.INITIAL, true);
   }

   private void addConversationAtTop(List var1, Conversation var2) {
      var1.add(0, var2);
   }

   private void markConversationAsRead(String var1, List var2) {
      int var4 = var2.size();

      for(int var3 = 0; var3 < var4; ++var3) {
         Conversation var5 = (Conversation)var2.get(var3);
         if(var1.equals(var5.getId())) {
            var2.set(var3, var5.withRead(true));
         }
      }

   }

   private List mergeConversationLists(List var1, List var2) {
      ArrayList var3 = new ArrayList(var1.size() + var2.size());
      HashSet var4 = new HashSet(var2.size());
      Iterator var7 = var2.iterator();

      while(var7.hasNext()) {
         Conversation var5 = (Conversation)var7.next();
         var4.add(var5.getId());
         var3.add(var5);
      }

      var7 = var1.iterator();

      while(var7.hasNext()) {
         Conversation var6 = (Conversation)var7.next();
         if(!var4.contains(var6.getId())) {
            var3.add(var6);
         }
      }

      return var3;
   }

   private void sortByLastPartDate(List var1) {
      Collections.sort(var1, new Comparator() {
         public int compare(Conversation var1, Conversation var2) {
            return (int)(var2.getLastPart().getCreatedAt() - var1.getLastPart().getCreatedAt());
         }
      });
   }

   private void updateConversation(Conversation var1, List var2) {
      for(int var3 = 0; var3 < var2.size(); ++var3) {
         Conversation var4 = (Conversation)var2.get(var3);
         if(var1.getId().equals(var4.getId())) {
            var2.set(var3, var1);
         }
      }

   }

   private void updateRepliedConversationAndMoveToTop(ReplyPart var1, List var2) {
      for(int var3 = 0; var3 < var2.size(); ++var3) {
         Conversation var4 = (Conversation)var2.get(var3);
         if(var1.getConversationId().equals(var4.getId())) {
            var4.getParts().add(var1.getPart());
            var2.remove(var4);
            this.addConversationAtTop(var2, var4);
         }
      }

   }

   public InboxState reduce(Action var1, InboxState var2) {
      ArrayList var3;
      InboxState var4;
      switch(null.$SwitchMap$io$intercom$android$sdk$actions$Action$Type[var1.type().ordinal()]) {
      case 1:
      case 2:
         var4 = var2;
         if(var2.status() != InboxState.Status.LOADING) {
            var4 = InboxState.create(var2.conversations(), InboxState.Status.LOADING, var2.hasMorePages());
         }
         break;
      case 3:
         ConversationsResponse var6 = (ConversationsResponse)var1.value();
         List var7 = var6.getConversationPage().getConversations();
         List var5 = this.mergeConversationLists(var2.conversations(), var7);
         this.sortByLastPartDate(var5);
         var4 = InboxState.create(var5, InboxState.Status.SUCCESS, var6.getConversationPage().hasMorePages());
         break;
      case 4:
         var4 = InboxState.create(var2.conversations(), InboxState.Status.FAILED, var2.hasMorePages());
         break;
      case 5:
         var3 = new ArrayList(var2.conversations());
         this.markConversationAsRead((String)var1.value(), var3);
         var4 = InboxState.create(var3, InboxState.Status.SUCCESS, var2.hasMorePages());
         break;
      case 6:
         var3 = new ArrayList(var2.conversations());
         this.updateRepliedConversationAndMoveToTop((ReplyPart)var1.value(), var3);
         var4 = InboxState.create(var3, InboxState.Status.SUCCESS, var2.hasMorePages());
         break;
      case 7:
         var3 = new ArrayList(var2.conversations());
         this.updateConversation((Conversation)var1.value(), var3);
         this.sortByLastPartDate(var3);
         var4 = InboxState.create(var3, InboxState.Status.SUCCESS, var2.hasMorePages());
         break;
      case 8:
         var3 = new ArrayList(var2.conversations());
         this.addConversationAtTop(var3, (Conversation)var1.value());
         var4 = InboxState.create(var3, InboxState.Status.SUCCESS, var2.hasMorePages());
         break;
      case 9:
      case 10:
         var4 = INITIAL_STATE;
         break;
      default:
         var4 = var2;
      }

      return var4;
   }
}
