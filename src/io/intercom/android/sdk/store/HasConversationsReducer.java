package io.intercom.android.sdk.store;

import io.intercom.android.sdk.actions.Action;
import io.intercom.android.sdk.models.BaseResponse;

class HasConversationsReducer implements Store.Reducer {
   static final boolean INITIAL_STATE = false;

   public Boolean reduce(Action var1, Boolean var2) {
      switch(null.$SwitchMap$io$intercom$android$sdk$actions$Action$Type[var1.type().ordinal()]) {
      case 1:
         var2 = Boolean.valueOf(((BaseResponse)var1.value()).hasConversations());
         break;
      case 2:
         var2 = Boolean.valueOf(false);
      }

      return var2;
   }
}
