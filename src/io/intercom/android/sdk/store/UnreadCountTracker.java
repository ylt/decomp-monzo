package io.intercom.android.sdk.store;

import io.intercom.android.sdk.UnreadConversationCountListener;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class UnreadCountTracker {
   final Map listeners = new ConcurrentHashMap();
   private final Store store;

   public UnreadCountTracker(Store var1) {
      this.store = var1;
   }

   public void addListener(final UnreadConversationCountListener var1) {
      Store.Subscriber var2 = new Store.Subscriber() {
         public void onStateChange(Integer var1x) {
            var1.onCountUpdate(var1x.intValue());
         }
      };
      Store.Subscription var3 = this.store.subscribeToChanges(Selectors.UNREAD_COUNT, var2);
      this.listeners.put(var1, var3);
   }

   public void removeListener(UnreadConversationCountListener var1) {
      if(this.listeners.containsKey(var1)) {
         ((Store.Subscription)this.listeners.remove(var1)).unsubscribe();
      }

   }
}
