package io.intercom.android.sdk.store;

import io.intercom.android.sdk.actions.Action;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class Store {
   Object currentState;
   final Store.Middleware dispatcher = new Store.Middleware() {
      public void dispatch(Store param1, Action param2, Store.NextDispatcher param3) {
         // $FF: Couldn't be decompiled
      }
   };
   private final List next = new ArrayList();
   final Store.Reducer reducer;
   final List subscribers = new CopyOnWriteArrayList();

   @SafeVarargs
   public Store(Store.Reducer var1, Object var2, Store.Middleware... var3) {
      this.reducer = var1;
      this.currentState = var2;
      this.next.add(new Store.NextDispatcher() {
         public void dispatch(Action var1) {
            Store.this.dispatcher.dispatch(Store.this, var1, (Store.NextDispatcher)null);
         }
      });

      for(int var4 = var3.length - 1; var4 >= 0; --var4) {
         final Store.Middleware var6 = var3[var4];
         final Store.NextDispatcher var5 = (Store.NextDispatcher)this.next.get(0);
         this.next.add(0, new Store.NextDispatcher() {
            public void dispatch(Action var1) {
               var6.dispatch(Store.this, var1, var5);
            }
         });
      }

   }

   public Object dispatch(Action var1) {
      ((Store.NextDispatcher)this.next.get(0)).dispatch(var1);
      return this.state();
   }

   public Object select(Store.Selector var1) {
      return var1.transform(this.state());
   }

   public Object state() {
      return this.currentState;
   }

   public Store.Subscription subscribe(final Store.Subscriber var1) {
      this.subscribers.add(var1);
      var1.onStateChange(this.state());
      return new Store.Subscription() {
         public void unsubscribe() {
            Store.this.subscribers.remove(var1);
         }
      };
   }

   public Store.Subscription subscribeToChanges(Store.Selector var1, Store.Selector var2, Store.Subscriber2 var3) {
      return this.subscribe(new TwoParamChangeSubscriber(var1, var2, var3));
   }

   public Store.Subscription subscribeToChanges(Store.Selector var1, Store.Subscriber var2) {
      return this.subscribe(new ChangeSubscriber(var1, var2));
   }

   public interface Middleware {
      void dispatch(Store var1, Action var2, Store.NextDispatcher var3);
   }

   public interface NextDispatcher {
      void dispatch(Action var1);
   }

   public interface Reducer {
      Object reduce(Action var1, Object var2);
   }

   public interface Selector {
      Object transform(Object var1);
   }

   public interface Subscriber {
      void onStateChange(Object var1);
   }

   public interface Subscriber2 {
      void onStateChange(Object var1, Object var2);
   }

   public interface Subscription {
      void unsubscribe();
   }
}
