package io.intercom.android.sdk.store;

import android.os.Handler;
import android.os.Looper;

class ChangeSubscriber implements Store.Subscriber {
   private Object last;
   private final Handler mainHandler;
   private final Store.Selector selector;
   final Store.Subscriber subscriber;

   ChangeSubscriber(Store.Selector var1, Store.Subscriber var2) {
      this(var1, var2, new Handler(Looper.getMainLooper()));
   }

   ChangeSubscriber(Store.Selector var1, Store.Subscriber var2, Handler var3) {
      this.last = null;
      this.selector = var1;
      this.subscriber = var2;
      this.mainHandler = var3;
   }

   static void runOnMainThread(Runnable var0, Handler var1) {
      if(Looper.myLooper() == Looper.getMainLooper()) {
         var0.run();
      } else {
         var1.post(var0);
      }

   }

   public void onStateChange(final Object var1) {
      var1 = this.selector.transform(var1);
      if(this.last != var1) {
         this.last = var1;
         runOnMainThread(new Runnable() {
            public void run() {
               ChangeSubscriber.this.subscriber.onStateChange(var1);
            }
         }, this.mainHandler);
      }

   }
}
