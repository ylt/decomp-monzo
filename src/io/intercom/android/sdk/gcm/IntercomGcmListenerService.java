package io.intercom.android.sdk.gcm;

import android.os.Bundle;
import com.google.android.gms.gcm.GcmListenerService;
import io.intercom.android.sdk.push.IntercomPushClient;

public class IntercomGcmListenerService extends GcmListenerService {
   private final IntercomPushClient pushClient = new IntercomPushClient();

   public void onMessageReceived(String var1, Bundle var2) {
      this.pushClient.handlePush(this.getApplication(), var2);
   }
}
