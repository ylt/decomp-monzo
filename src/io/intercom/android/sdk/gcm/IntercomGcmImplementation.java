package io.intercom.android.sdk.gcm;

import android.app.Application;
import android.os.Bundle;
import android.text.TextUtils;
import com.google.android.gms.iid.InstanceID;
import io.intercom.android.sdk.IntercomPushManager;
import io.intercom.android.sdk.logger.LumberMill;
import io.intercom.android.sdk.push.IntercomPushClient;
import io.intercom.android.sdk.twig.Twig;
import java.io.IOException;

public class IntercomGcmImplementation implements IntercomPushManager.GcmImplementation {
   private final IntercomPushClient pushClient = new IntercomPushClient();
   private final Twig twig = LumberMill.getLogger();

   static {
      IntercomPushManager.gcmImplementation = new IntercomGcmImplementation();
   }

   private void printTokenError() {
      this.twig.e("Intercom push registration failed. Please make sure the following string is in your strings.xml file: <string name=\"intercom_gcm_sender_id\">YOUR_SENDER_ID</string>", new Object[0]);
   }

   public void registerToken(Application var1) {
      InstanceID var3 = InstanceID.getInstance(var1);
      String var2 = IntercomPushManager.getSenderId(var1);
      if(TextUtils.isEmpty(var2)) {
         this.printTokenError();
      } else {
         try {
            var2 = var3.getToken(var2, "GCM", (Bundle)null);
            this.pushClient.sendTokenToIntercom(var1, var2);
         } catch (IOException var4) {
            this.twig.e("Upload failed: " + var4.getMessage(), new Object[0]);
         }
      }

   }
}
