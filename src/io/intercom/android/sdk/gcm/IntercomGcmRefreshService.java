package io.intercom.android.sdk.gcm;

import android.app.Application;
import com.google.android.gms.iid.InstanceIDListenerService;

public class IntercomGcmRefreshService extends InstanceIDListenerService {
   private IntercomGcmImplementation implementation = new IntercomGcmImplementation();

   public void onTokenRefresh() {
      this.implementation.registerToken((Application)this.getApplicationContext());
   }
}
