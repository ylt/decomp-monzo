package io.intercom.android.sdk.inbox;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.Animator.AnimatorListener;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff.Mode;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.j;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ProgressBar;
import io.intercom.android.sdk.Injector;
import io.intercom.android.sdk.Provider;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.actions.Actions;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.android.sdk.logger.LumberMill;
import io.intercom.android.sdk.models.Conversation;
import io.intercom.android.sdk.state.InboxState;
import io.intercom.android.sdk.state.State;
import io.intercom.android.sdk.store.Selectors;
import io.intercom.android.sdk.store.Store;
import io.intercom.android.sdk.twig.Twig;
import io.intercom.android.sdk.utilities.Phrase;
import io.intercom.android.sdk.utilities.StoreUtils;
import io.intercom.android.sdk.utilities.TimeFormatter;
import io.intercom.android.sdk.utilities.ViewUtils;
import io.intercom.android.sdk.views.EndlessRecyclerScrollListener;
import io.intercom.android.sdk.views.EndlessScrollListener;
import io.intercom.android.sdk.views.IntercomErrorView;
import io.intercom.android.sdk.views.IntercomToolbar;
import io.intercom.com.bumptech.glide.c;
import io.intercom.com.bumptech.glide.i;
import java.util.List;

public class InboxFragment extends Fragment implements OnClickListener, ConversationClickListener, Store.Subscriber, EndlessScrollListener, IntercomToolbar.Listener {
   private static final String ARG_IS_TWO_PANE = "is_two_pane";
   private static final int FADE_DURATION_MS = 150;
   private InboxAdapter adapter;
   private Provider appConfigProvider;
   private FloatingActionButton composerButton;
   private EndlessRecyclerScrollListener endlessRecyclerScrollListener;
   private IntercomErrorView inboxErrorView;
   RecyclerView inboxView;
   private IntercomToolbar intercomToolbar;
   private boolean isTwoPane;
   private LinearLayoutManager layoutManager;
   InboxFragment.Listener listener;
   private ProgressBar progressBar;
   private i requestManager;
   private View rootView;
   private Store store;
   private Store.Subscription subscription;
   private final Twig twig;

   public InboxFragment() {
      this.listener = InboxFragment.Listener.EMPTY;
      this.isTwoPane = false;
      this.twig = LumberMill.getLogger();
   }

   private void displayEmptyView() {
      this.inboxErrorView.setTitle(R.string.intercom_no_conversations);
      CharSequence var1 = Phrase.from(this.getContext(), R.string.intercom_empty_conversations).put("name", ((AppConfig)this.appConfigProvider.get()).getName()).format();
      this.inboxErrorView.setSubtitle(var1);
      this.inboxErrorView.setActionButtonVisibility(8);
      this.inboxErrorView.setVisibility(0);
      this.showComposerButtonIfEnabled();
      this.inboxView.setVisibility(8);
      this.progressBar.setVisibility(8);
   }

   private void displayErrorView() {
      this.inboxErrorView.setTitle(R.string.intercom_inbox_error_state_title);
      this.inboxErrorView.setSubtitle(R.string.intercom_failed_to_load_conversation);
      this.inboxErrorView.setActionButtonText(R.string.intercom_retry);
      this.inboxErrorView.setActionButtonVisibility(0);
      this.inboxErrorView.setVisibility(0);
      this.inboxView.setVisibility(8);
      this.progressBar.setVisibility(8);
      this.composerButton.setVisibility(8);
   }

   private void displayInbox() {
      this.inboxView.setVisibility(0);
      this.showComposerButtonIfEnabled();
      this.inboxErrorView.setVisibility(8);
      this.progressBar.setVisibility(8);
   }

   private void displayLoadingView() {
      this.inboxErrorView.setVisibility(8);
      this.inboxView.setVisibility(8);
      this.composerButton.setVisibility(8);
      this.progressBar.setVisibility(0);
   }

   private void fadeOutInbox(AnimatorListener var1) {
      this.intercomToolbar.fadeOutTitle(150);
      this.inboxView.animate().alpha(0.0F).setDuration(150L).setListener(var1).start();
   }

   private boolean isInboundMessageEnabled() {
      return ((AppConfig)this.appConfigProvider.get()).isInboundMessages();
   }

   public static InboxFragment newInstance(boolean var0) {
      InboxFragment var1 = new InboxFragment();
      Bundle var2 = new Bundle();
      var2.putBoolean("is_two_pane", var0);
      var1.setArguments(var2);
      return var1;
   }

   private void setColorScheme() {
      int var1 = ((AppConfig)this.appConfigProvider.get()).getBaseColor();
      this.progressBar.getIndeterminateDrawable().setColorFilter(var1, Mode.SRC_IN);
      this.composerButton.setBackgroundTintList(ColorStateList.valueOf(var1));
      this.listener.setStatusBarColor();
      this.intercomToolbar.setBackgroundColor(var1);
   }

   private void setToolbarTitle() {
      String var1 = ((AppConfig)this.appConfigProvider.get()).getName();
      if(TextUtils.isEmpty(var1)) {
         this.intercomToolbar.setTitle(this.getString(R.string.intercom_conversations));
      } else {
         CharSequence var2 = Phrase.from(this.getContext(), R.string.intercom_conversations_with_app).put("name", var1).format();
         this.intercomToolbar.setTitle(var2);
      }

   }

   private void showComposerButtonIfEnabled() {
      FloatingActionButton var2 = this.composerButton;
      byte var1;
      if(this.isInboundMessageEnabled()) {
         var1 = 0;
      } else {
         var1 = 8;
      }

      var2.setVisibility(var1);
   }

   public void onAttach(Context var1) {
      super.onAttach(var1);

      try {
         this.listener = (InboxFragment.Listener)var1;
      } catch (ClassCastException var3) {
         throw new ClassCastException(var1 + " must implement InboxFragment.Listener");
      }
   }

   public void onClick(View var1) {
      if(var1.getId() == R.id.compose_action_button) {
         if(this.isTwoPane) {
            this.listener.onComposerSelected();
         } else {
            this.fadeOutInbox(new AnimatorListenerAdapter() {
               public void onAnimationEnd(Animator var1) {
                  InboxFragment.this.listener.onComposerSelected();
               }
            });
         }
      }

   }

   public void onCloseClicked() {
      this.listener.onToolbarCloseClicked();
   }

   public void onConversationClicked(int var1) {
      final Conversation var2 = (Conversation)((State)this.store.state()).inboxState().conversations().get(var1);
      if(this.isTwoPane) {
         this.listener.onConversationSelected(var2);
      } else {
         this.fadeOutInbox(new AnimatorListenerAdapter() {
            public void onAnimationEnd(Animator var1) {
               InboxFragment.this.listener.onConversationSelected(var2);
            }
         });
      }

   }

   public void onCreate(Bundle var1) {
      super.onCreate(var1);
      Injector var2 = Injector.get();
      this.requestManager = c.a((Fragment)this);
      this.store = var2.getStore();
      this.appConfigProvider = var2.getAppConfigProvider();
      j var3 = this.getActivity();
      TimeFormatter var4 = new TimeFormatter(var3, var2.getTimeProvider());
      this.adapter = new InboxAdapter(LayoutInflater.from(var3), this, this.store, var4, this.appConfigProvider, var2.getUserIdentity(), this.requestManager);
      this.layoutManager = new LinearLayoutManager(var3);
      this.endlessRecyclerScrollListener = new EndlessRecyclerScrollListener(this.layoutManager, this);
      var1 = this.getArguments();
      if(var1 != null) {
         this.isTwoPane = var1.getBoolean("is_two_pane", false);
      }

   }

   public View onCreateView(LayoutInflater var1, ViewGroup var2, Bundle var3) {
      this.twig.internal("inbox frag", "creating view for fragment");
      if(this.rootView == null) {
         this.rootView = var1.inflate(R.layout.intercom_fragment_inbox, var2, false);
         this.progressBar = (ProgressBar)this.rootView.findViewById(R.id.progress_bar);
         this.inboxView = (RecyclerView)this.rootView.findViewById(R.id.inbox_recycler_view);
         this.inboxView.setLayoutManager(this.layoutManager);
         this.inboxView.a(this.endlessRecyclerScrollListener);
         this.inboxView.setAdapter(this.adapter);
         this.composerButton = (FloatingActionButton)this.rootView.findViewById(R.id.compose_action_button);
         this.composerButton.setOnClickListener(this);
         this.inboxErrorView = (IntercomErrorView)this.rootView.findViewById(R.id.error_layout_inbox);
         this.inboxErrorView.setActionButtonTextColor(((AppConfig)this.appConfigProvider.get()).getBaseColor());
         this.inboxErrorView.setActionButtonClickListener(new OnClickListener() {
            public void onClick(View var1) {
               InboxFragment.this.store.dispatch(Actions.fetchInboxRequest());
            }
         });
         this.intercomToolbar = (IntercomToolbar)this.rootView.findViewById(R.id.intercom_toolbar);
         this.intercomToolbar.setListener(this);
         this.intercomToolbar.setSubtitleVisibility(8);
         if(this.isTwoPane) {
            this.intercomToolbar.setCloseButtonVisibility(8);
         }

         AsyncTask.THREAD_POOL_EXECUTOR.execute(new Runnable() {
            public void run() {
               InboxFragment.this.store.dispatch(Actions.fetchInboxRequest());
            }
         });
      } else {
         ViewGroup var4 = (ViewGroup)this.rootView.getParent();
         if(var4 != null) {
            var4.removeView(this.rootView);
         }
      }

      return this.rootView;
   }

   public void onDetach() {
      super.onDetach();
      this.listener = InboxFragment.Listener.EMPTY;
   }

   public void onInboxClicked() {
   }

   public void onLoadMore() {
      InboxState var4 = ((State)this.store.state()).inboxState();
      List var3 = var4.conversations();
      if(var4.status() != InboxState.Status.LOADING && !var3.isEmpty()) {
         long var1 = ((Conversation)var3.get(var3.size() - 1)).getLastPart().getCreatedAt();
         this.store.dispatch(Actions.fetchInboxBeforeDateRequest(var1));
      }

   }

   public void onResume() {
      this.store.dispatch(Actions.inboxOpened());
      if(!this.isTwoPane) {
         this.inboxView.setAlpha(1.0F);
      }

      this.setToolbarTitle();
      this.setColorScheme();
      super.onResume();
   }

   public void onStart() {
      super.onStart();
      this.subscription = this.store.subscribeToChanges(Selectors.INBOX, this);
   }

   public void onStateChange(InboxState var1) {
      this.adapter.setInboxState(var1);
      this.adapter.notifyDataSetChanged();
      this.endlessRecyclerScrollListener.setMorePagesAvailable(var1.hasMorePages());
      if(this.isAdded() && this.getView() != null) {
         switch(null.$SwitchMap$io$intercom$android$sdk$state$InboxState$Status[var1.status().ordinal()]) {
         case 1:
            this.displayLoadingView();
            break;
         case 2:
            if(var1.conversations().isEmpty()) {
               this.displayLoadingView();
            } else {
               this.displayInbox();
            }
            break;
         case 3:
            if(this.layoutManager.o() == 0) {
               this.layoutManager.e(0);
            }

            if(var1.conversations().isEmpty()) {
               this.displayEmptyView();
            } else {
               this.displayInbox();
            }

            this.listener.onConversationsLoaded(var1.conversations(), var1.status());
            break;
         default:
            this.displayErrorView();
         }
      }

   }

   public void onStop() {
      StoreUtils.safeUnsubscribe(this.subscription);
      super.onStop();
   }

   public void onToolbarClicked() {
   }

   public void setOverScrollColour() {
      ViewUtils.setOverScrollColour(this.inboxView, ((AppConfig)this.appConfigProvider.get()).getBaseColor());
   }

   public interface Listener {
      InboxFragment.Listener EMPTY = new InboxFragment.Listener() {
         public void onComposerSelected() {
         }

         public void onConversationSelected(Conversation var1) {
         }

         public void onConversationsLoaded(List var1, InboxState.Status var2) {
         }

         public void onToolbarCloseClicked() {
         }

         public void setStatusBarColor() {
         }
      };

      void onComposerSelected();

      void onConversationSelected(Conversation var1);

      void onConversationsLoaded(List var1, InboxState.Status var2);

      void onToolbarCloseClicked();

      void setStatusBarColor();
   }
}
