package io.intercom.android.sdk.inbox;

import android.support.v7.widget.RecyclerView.a;
import android.support.v7.widget.RecyclerView.w;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import io.intercom.android.sdk.Provider;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.android.sdk.identity.UserIdentity;
import io.intercom.android.sdk.models.Conversation;
import io.intercom.android.sdk.models.TeamPresence;
import io.intercom.android.sdk.state.InboxState;
import io.intercom.android.sdk.store.Selectors;
import io.intercom.android.sdk.store.Store;
import io.intercom.android.sdk.utilities.TimeFormatter;
import io.intercom.com.bumptech.glide.i;
import java.util.Locale;

class InboxAdapter extends a {
   static final int CONVERSATION = 0;
   static final int LOADING = 1;
   private final Provider appConfigProvider;
   private final ConversationClickListener conversationClickListener;
   private InboxState inboxState;
   private final LayoutInflater inflater;
   private final i requestManager;
   private final Store store;
   private final TimeFormatter timeFormatter;
   private UserIdentity userIdentity;

   InboxAdapter(LayoutInflater var1, ConversationClickListener var2, Store var3, TimeFormatter var4, Provider var5, UserIdentity var6, i var7) {
      this.inflater = var1;
      this.conversationClickListener = var2;
      this.store = var3;
      this.timeFormatter = var4;
      this.appConfigProvider = var5;
      this.userIdentity = var6;
      this.requestManager = var7;
      this.setHasStableIds(true);
   }

   private int conversationCount() {
      return this.inboxState.conversations().size();
   }

   public int getItemCount() {
      int var2 = this.conversationCount();
      byte var1;
      if(this.inboxState.status() == InboxState.Status.LOADING) {
         var1 = 1;
      } else {
         var1 = 0;
      }

      return var1 + var2;
   }

   public long getItemId(int var1) {
      long var2;
      if(var1 == this.conversationCount()) {
         var2 = -1L;
      } else {
         String var4 = ((Conversation)this.inboxState.conversations().get(var1)).getId();
         if(!TextUtils.isEmpty(var4) && TextUtils.isDigitsOnly(var4)) {
            var2 = Long.valueOf(var4).longValue();
         } else {
            var2 = (long)var4.hashCode();
         }
      }

      return var2;
   }

   public int getItemViewType(int var1) {
      byte var2;
      if(var1 == this.conversationCount()) {
         var2 = 1;
      } else {
         var2 = 0;
      }

      return var2;
   }

   public void onBindViewHolder(w var1, int var2) {
      if(var1 instanceof InboxRowViewHolder) {
         ((InboxRowViewHolder)var1).bindData((Conversation)this.inboxState.conversations().get(var2), this.userIdentity, (AppConfig)this.appConfigProvider.get(), (TeamPresence)this.store.select(Selectors.TEAM_PRESENCE));
      }

   }

   public w onCreateViewHolder(ViewGroup var1, int var2) {
      Object var3;
      if(var2 == 0) {
         var3 = new InboxRowViewHolder(this.inflater.inflate(R.layout.intercom_row_inbox, var1, false), this.conversationClickListener, this.timeFormatter, this.requestManager);
      } else {
         if(var2 != 1) {
            throw new RuntimeException(String.format(Locale.getDefault(), "View type %d not supported", new Object[]{Integer.valueOf(var2)}));
         }

         var3 = new LoadingViewHolder(this.inflater.inflate(R.layout.intercom_row_loading, var1, false), ((AppConfig)this.appConfigProvider.get()).getBaseColor());
      }

      return (w)var3;
   }

   void setInboxState(InboxState var1) {
      this.inboxState = var1;
   }
}
