package io.intercom.android.sdk.inbox;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.support.v4.content.a;
import android.support.v7.widget.RecyclerView.w;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.android.sdk.identity.UserIdentity;
import io.intercom.android.sdk.models.Conversation;
import io.intercom.android.sdk.models.LastParticipatingAdmin;
import io.intercom.android.sdk.models.Part;
import io.intercom.android.sdk.models.TeamPresence;
import io.intercom.android.sdk.utilities.FontUtils;
import io.intercom.android.sdk.utilities.GroupConversationTextFormatter;
import io.intercom.android.sdk.utilities.TimeFormatter;
import io.intercom.android.sdk.views.AuthorAvatarView;
import io.intercom.com.bumptech.glide.i;

class InboxRowViewHolder extends w implements OnClickListener {
   private final AuthorAvatarView authorAvatar;
   private final TextView authorName;
   private final ConversationClickListener conversationClickListener;
   private final ImageView conversationIndicator;
   private final i requestManager;
   private final TextView summary;
   private final TimeFormatter timeFormatter;
   private final TextView timeStamp;

   InboxRowViewHolder(View var1, ConversationClickListener var2, TimeFormatter var3, i var4) {
      super(var1);
      this.conversationClickListener = var2;
      this.timeFormatter = var3;
      this.requestManager = var4;
      var1.setOnClickListener(this);
      this.authorAvatar = (AuthorAvatarView)var1.findViewById(R.id.intercom_author_avatar);
      this.authorName = (TextView)var1.findViewById(R.id.intercom_user_name);
      this.timeStamp = (TextView)var1.findViewById(R.id.intercom_time_stamp);
      this.summary = (TextView)var1.findViewById(R.id.intercom_message_summary);
      this.conversationIndicator = (ImageView)var1.findViewById(R.id.intercom_conversation_indicator);
   }

   private void setRowParticipantDetails(Conversation var1, String var2, TeamPresence var3, AppConfig var4) {
      LastParticipatingAdmin var5 = var1.getLastParticipatingAdmin();
      if(!TextUtils.isEmpty(var5.getFirstName())) {
         this.authorAvatar.loadAvatarWithActiveState(var5.getAvatar(), var5.isActive(), var4, this.requestManager);
         this.authorName.setText(GroupConversationTextFormatter.groupConversationTitle(var5.getFirstName(), var1.getGroupConversationParticipants().size(), this.authorName.getContext()));
      } else if(var3.isEmpty()) {
         this.authorAvatar.loadAvatar(var5.getAvatar(), var4, this.requestManager);
         this.authorName.setText(var2);
      } else {
         this.authorAvatar.loadAvatars(var3.getActiveAdmins(), var4, this.requestManager);
         this.authorName.setText(var2);
      }

   }

   void bindData(Conversation var1, UserIdentity var2, AppConfig var3, TeamPresence var4) {
      Part var6 = var1.getLastUserVisiblePart();
      Context var7 = this.itemView.getContext();
      if(var6.getParticipant().isUserWithId(var2.getIntercomId())) {
         this.summary.setText(var7.getString(R.string.intercom_you) + ": " + var6.getSummary());
      } else {
         this.summary.setText(var6.getSummary());
      }

      this.timeStamp.setText(this.timeFormatter.getFormattedTime(var6.getCreatedAt()));
      this.setRowParticipantDetails(var1, var3.getName(), var4, var3);
      if(!var1.isRead()) {
         FontUtils.setRobotoMediumTypeface(this.authorName);
         this.summary.setTextColor(a.c(var7, R.color.intercom_grey_800));
         Drawable var8 = a.a(var7, R.drawable.intercom_unread_dot);
         int var5 = var3.getBaseColor();
         ((GradientDrawable)var8).setColor(var5);
         this.timeStamp.setCompoundDrawablesWithIntrinsicBounds(var8, (Drawable)null, (Drawable)null, (Drawable)null);
         this.timeStamp.setTextColor(var5);
      } else {
         this.authorName.setTypeface(Typeface.SANS_SERIF);
         this.summary.setTextColor(a.c(var7, R.color.intercom_grey_600));
         this.timeStamp.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
         this.timeStamp.setTextColor(a.c(var7, R.color.intercom_grey_500));
      }

      if(var1.isUserParticipated()) {
         this.conversationIndicator.setVisibility(0);
      } else {
         this.conversationIndicator.setVisibility(4);
      }

   }

   public void onClick(View var1) {
      int var2 = this.getAdapterPosition();
      if(var2 != -1) {
         this.conversationClickListener.onConversationClicked(var2);
      }

   }
}
