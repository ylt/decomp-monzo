package io.intercom.android.sdk.inbox;

import android.content.res.Resources;
import android.graphics.PorterDuff.Mode;
import android.support.v7.widget.RecyclerView.w;
import android.view.View;
import android.widget.ProgressBar;
import io.intercom.android.sdk.R;

class LoadingViewHolder extends w {
   LoadingViewHolder(View var1, int var2) {
      super(var1);
      Resources var3 = var1.getResources();
      var1.getLayoutParams().height = var3.getDimensionPixelSize(R.dimen.intercom_inbox_row_height);
      ((ProgressBar)var1.findViewById(R.id.progressBar)).getIndeterminateDrawable().setColorFilter(var2, Mode.SRC_IN);
   }
}
