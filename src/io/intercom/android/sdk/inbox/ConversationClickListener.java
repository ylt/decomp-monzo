package io.intercom.android.sdk.inbox;

interface ConversationClickListener {
   void onConversationClicked(int var1);
}
