package io.intercom.android.sdk.inbox;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Paint.Style;
import android.support.v4.content.a;
import android.util.AttributeSet;
import android.view.View;
import io.intercom.android.sdk.R;

public class InboxEmptyRowsView extends View {
   private static final int CIRCLE_RADIUS = 24;
   private static final int CIRCLE_TOP_PADDING = 20;
   private static final int ITEM_HEIGHT = 68;
   private static final int LINE_CORNER_RADIUS = 4;
   private static final int LINE_LEFT_X = 80;
   private static final int LOWER_LINE_BOTTOM_Y = 64;
   private static final int LOWER_LINE_TOP_Y = 52;
   private static final int NUMBER_OF_ROWS = 3;
   private static final int PADDING_X = 16;
   private static final int UPPER_LINE_BOTTOM_Y = 40;
   private static final int UPPER_LINE_TOP_Y = 28;
   private static final int UPPER_LINE_WIDTH = 116;
   private final float density;
   private final Paint paint;
   private final RectF rect;

   public InboxEmptyRowsView(Context var1) {
      this(var1, (AttributeSet)null);
   }

   public InboxEmptyRowsView(Context var1, AttributeSet var2) {
      super(var1, var2);
      this.paint = new Paint();
      this.rect = new RectF();
      this.paint.setStyle(Style.FILL);
      this.paint.setColor(a.c(var1, R.color.intercom_error_state_empty_avatar));
      this.density = this.getResources().getDisplayMetrics().density;
   }

   private float dpToPx(int var1) {
      return (float)var1 * this.density;
   }

   protected void onDraw(Canvas var1) {
      super.onDraw(var1);

      for(int var2 = 0; var2 < 3; ++var2) {
         int var3 = var2 * 68;
         var1.drawCircle(this.dpToPx(40), this.dpToPx(var3 + 44), this.dpToPx(24), this.paint);
         this.rect.set(this.dpToPx(80), this.dpToPx(var3 + 28), this.dpToPx(196), this.dpToPx(var3 + 40));
         var1.drawRoundRect(this.rect, this.dpToPx(4), this.dpToPx(4), this.paint);
         this.rect.set(this.dpToPx(80), this.dpToPx(var3 + 52), (float)this.getWidth() - this.dpToPx(16), this.dpToPx(var3 + 64));
         var1.drawRoundRect(this.rect, this.dpToPx(4), this.dpToPx(4), this.paint);
      }

   }
}
