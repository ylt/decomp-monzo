package io.intercom.android.sdk.api;

import android.content.Context;
import android.text.TextUtils;
import com.intercom.input.gallery.c;
import io.intercom.android.sdk.Provider;
import io.intercom.android.sdk.actions.Actions;
import io.intercom.android.sdk.blocks.UploadingImageCache;
import io.intercom.android.sdk.conversation.UploadProgressListener;
import io.intercom.android.sdk.errorreporting.ErrorReport;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.android.sdk.identity.AppIdentity;
import io.intercom.android.sdk.identity.UserIdentity;
import io.intercom.android.sdk.logger.LumberMill;
import io.intercom.android.sdk.models.TeamPresence;
import io.intercom.android.sdk.models.UpdateUserResponse;
import io.intercom.android.sdk.models.Upload;
import io.intercom.android.sdk.models.events.UploadEvent;
import io.intercom.android.sdk.models.events.failure.UploadFailedEvent;
import io.intercom.android.sdk.store.Selectors;
import io.intercom.android.sdk.store.Store;
import io.intercom.android.sdk.twig.Twig;
import io.intercom.android.sdk.user.DeviceData;
import io.intercom.com.a.a.b;
import io.intercom.okhttp3.Dispatcher;
import io.intercom.okhttp3.MediaType;
import io.intercom.okhttp3.MultipartBody;
import io.intercom.okhttp3.OkHttpClient;
import io.intercom.okhttp3.Request;
import io.intercom.retrofit2.Call;
import io.intercom.retrofit2.Callback;
import io.intercom.retrofit2.Response;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;

public class Api {
   private static final String BATCH_SIZE = "batch_size";
   private static final String DATA = "data";
   private static final String DEVICE_DATA = "device_data";
   private static final String DEVICE_TOKEN = "device_token";
   private static final String HMAC = "hmac";
   private static final String NEW_SESSION = "new_session";
   private static final String SENT_FROM_BACKGROUND = "sent_from_background";
   private static final Twig TWIG = LumberMill.getLogger();
   private static final String UPLOAD = "upload";
   private static final String USER = "user";
   private static final String USER_ATTRIBUTES = "user_attributes";
   private final OkHttpClient apiHttpClient;
   private final Provider appConfigProvider;
   private final AppIdentity appIdentity;
   final b bus;
   final CallbackHolder callbacks;
   private final Context context;
   private final int defaultOkHttpMaxRequests;
   private final Callback emptyCallback = new Callback() {
      public void onFailure(Call var1, Throwable var2) {
      }

      public void onResponse(Call var1, Response var2) {
      }
   };
   final OkHttpClient httpClient = new OkHttpClient();
   private final MessengerApi messengerApi;
   private final RateLimiter rateLimiter;
   private final Store store;
   final UserIdentity userIdentity;

   public Api(Context var1, AppIdentity var2, UserIdentity var3, b var4, OkHttpClient var5, MessengerApi var6, CallbackHolder var7, RateLimiter var8, Store var9, Provider var10) {
      this.context = var1;
      this.appIdentity = var2;
      this.userIdentity = var3;
      this.bus = var4;
      this.messengerApi = var6;
      this.callbacks = var7;
      this.rateLimiter = var8;
      this.store = var9;
      this.appConfigProvider = var10;
      this.apiHttpClient = var5;
      this.defaultOkHttpMaxRequests = var5.dispatcher().getMaxRequests();
      this.updateMaxRequests();
   }

   private void addSecureHash(Map var1) {
      String var2 = this.userIdentity.getData();
      String var3 = this.userIdentity.getHmac();
      if(!TextUtils.isEmpty(var2)) {
         var1.put("data", var2);
      }

      if(!TextUtils.isEmpty(var3)) {
         var1.put("hmac", var3);
      }

   }

   private Map baseNewConversationParams() {
      HashMap var1 = new HashMap();
      var1.put("app_id", this.appIdentity.appId());
      var1.put("user", this.userIdentity.toMap());
      this.addSecureHash(var1);
      return var1;
   }

   private Map createBaseReplyParams() {
      HashMap var1 = new HashMap();
      var1.put("app_id", this.appIdentity.appId());
      var1.put("type", "user");
      var1.put("message_type", "comment");
      var1.put("user", this.userIdentity.toMap());
      this.addSecureHash(var1);
      return var1;
   }

   private Map generateUpdateUserParams(UserUpdateRequest var1) {
      HashMap var2 = new HashMap();
      var2.put("user", this.userIdentity.toMap());
      var2.put("device_data", DeviceData.generateDeviceData(this.context));
      var2.put("new_session", Boolean.valueOf(var1.isNewSession()));
      var2.put("sent_from_background", Boolean.valueOf(var1.isSentFromBackground()));
      var2.put("batch_size", Integer.valueOf(var1.getBatchSize()));
      var2.put("user_attributes", var1.getAttributes());
      this.addSecureHash(var2);
      return var2;
   }

   protected static boolean isUserNotFound(ErrorObject var0, Map var1) {
      boolean var2 = true;
      if(var0.hasErrorBody() && var0.getStatusCode() == 404 && var1 != null && var1.get("intercom_id") != null && var1.size() > 1) {
         label20: {
            boolean var3;
            try {
               JSONObject var5 = new JSONObject(var0.getErrorBody());
               var3 = var5.getJSONArray("errors").getJSONObject(0).getString("code").equals("not_found");
            } catch (Exception var4) {
               TWIG.internal("Could not parse error response");
               break label20;
            }

            if(var3) {
               return var2;
            }
         }
      }

      var2 = false;
      return var2;
   }

   private void logBackgroundDisabledError() {
      TWIG.e("Your request was not sent because the app is in the background. Please contact Intercom to enable background requests.", new Object[0]);
   }

   private void retriableUpdateUser(final Map var1, final String var2) {
      this.messengerApi.updateUser(var1).enqueue(new BaseCallback() {
         void logFailure(String var1x, ErrorObject var2x) {
            super.logFailure("Failed to register or update user", var2x);
         }

         public void onError(ErrorObject var1x) {
            Map var2x = (Map)var1.get("user");
            if(Api.isUserNotFound(var1x, var2x)) {
               var2x.remove("intercom_id");
               var1.put("user", var2x);
               Api.this.retriableUpdateUser(var1, var2);
            }

         }

         public void onSuccess(UpdateUserResponse.Builder var1x) {
            if(var2.equals(Api.this.userIdentity.getFingerprint())) {
               Api.TWIG.i("Successfully registered or updated user", new Object[0]);
               Api.this.callbacks.unreadCallback().onSuccess(var1x);
               TeamPresence var2x = var1x.build().getTeamPresence();
               Api.this.store.dispatch(Actions.teamPresenceUpdated(var2x));
            }

         }
      });
   }

   private boolean shouldStopBackgroundRequest(boolean var1) {
      if(var1 && ((AppConfig)this.appConfigProvider.get()).backgroundRequestsDisabled()) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public void addConversationRatingRemark(String var1, String var2) {
      HashMap var3 = new HashMap();
      var3.put("user", this.userIdentity.toMap());
      var3.put("remark", var2);
      this.addSecureHash(var3);
      this.messengerApi.addConversationRatingRemark(var1, var3).enqueue(this.callbacks.loggingCallback("adding remark to conversation"));
   }

   public void fetchDefaultGifs(Callback var1) {
      this.messengerApi.getGifs(Collections.emptyMap()).enqueue(var1);
   }

   public void fetchGifs(String var1, Callback var2) {
      Map var3 = Collections.singletonMap("query", var1);
      this.messengerApi.getGifs(var3).enqueue(var2);
   }

   public void getConversation(String var1) {
      Map var2 = this.userIdentity.toMap();
      this.addSecureHash(var2);
      this.messengerApi.getConversation(var1, var2).enqueue(this.callbacks.conversationCallback());
   }

   public void getInbox() {
      Map var1 = this.userIdentity.toMap();
      var1.put("per_page", "20");
      this.addSecureHash(var1);
      this.messengerApi.getConversations(var1).enqueue(this.callbacks.inboxCallback());
   }

   public void getInboxBefore(long var1) {
      Map var3 = this.userIdentity.toMap();
      var3.put("before", String.valueOf(var1));
      var3.put("per_page", "20");
      this.addSecureHash(var3);
      this.messengerApi.getConversations(var3).enqueue(this.callbacks.inboxCallback());
   }

   public void getLink(String var1, Callback var2) {
      Map var3 = this.userIdentity.toMap();
      this.addSecureHash(var3);
      this.messengerApi.getLink(var1, var3).enqueue(var2);
   }

   public void getUnreadConversations() {
      Map var1 = this.userIdentity.toMap();
      var1.put("per_page", "20");
      this.addSecureHash(var1);
      this.messengerApi.getUnreadConversations(var1).enqueue(this.callbacks.unreadCallback());
   }

   public void getVideo(String var1, io.intercom.okhttp3.Callback var2) {
      this.httpClient.newCall((new Request.Builder()).url(var1).build()).enqueue(var2);
   }

   public void hitTrackingUrl(String var1) {
      this.httpClient.newCall((new Request.Builder()).url(var1).build()).enqueue(new io.intercom.okhttp3.Callback() {
         public void onFailure(io.intercom.okhttp3.Call var1, IOException var2) {
            Api.TWIG.internal("Tracking Url", "Failed tracking url request");
         }

         public void onResponse(io.intercom.okhttp3.Call var1, io.intercom.okhttp3.Response var2) throws IOException {
            Api.TWIG.internal("Tracking Url", "success");
            var2.body().close();
         }
      });
   }

   public boolean isIdle() {
      boolean var1;
      if(this.apiHttpClient.dispatcher().runningCallsCount() == 0) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   boolean isSynchronous() {
      boolean var1 = true;
      if(this.apiHttpClient.dispatcher().getMaxRequests() != 1) {
         var1 = false;
      }

      return var1;
   }

   public void logEvent(String var1, Map var2) {
      if(this.rateLimiter.isLimited()) {
         this.rateLimiter.logError();
      } else {
         boolean var3 = ((Boolean)this.store.select(Selectors.APP_IS_BACKGROUNDED)).booleanValue();
         if(this.shouldStopBackgroundRequest(var3)) {
            this.logBackgroundDisabledError();
         } else {
            this.rateLimiter.recordRequest();
            HashMap var4 = new HashMap();
            var4.put("event_name", var1);
            if(!var2.isEmpty()) {
               var4.put("metadata", var2);
            }

            HashMap var5 = new HashMap();
            var5.put("event", var4);
            var5.put("sent_from_background", Boolean.valueOf(var3));
            var5.put("user", this.userIdentity.toMap());
            this.addSecureHash(var5);
            this.messengerApi.logEvent(var5).enqueue(this.callbacks.unreadCallback());
         }
      }

   }

   public void markConversationAsDismissed(String var1) {
      HashMap var2 = new HashMap();
      var2.put("user", this.userIdentity.toMap());
      var2.put("conversation_ids", new String[]{var1});
      this.addSecureHash(var2);
      this.messengerApi.markAsDismissed(var2).enqueue(this.emptyCallback);
   }

   public void markConversationAsRead(String var1) {
      HashMap var2 = new HashMap();
      var2.put("app_id", this.appIdentity.appId());
      var2.put("user", this.userIdentity.toMap());
      this.addSecureHash(var2);
      this.messengerApi.markAsRead(var1, var2).enqueue(this.emptyCallback);
   }

   public void rateConversation(String var1, int var2) {
      HashMap var3 = new HashMap();
      var3.put("user", this.userIdentity.toMap());
      var3.put("rating_index", Integer.valueOf(var2));
      this.addSecureHash(var3);
      this.messengerApi.rateConversation(var1, var3).enqueue(this.callbacks.loggingCallback("conversation rating"));
   }

   public void reactToConversation(String var1, int var2) {
      HashMap var3 = new HashMap();
      var3.put("user", this.userIdentity.toMap());
      var3.put("reaction_index", Integer.valueOf(var2));
      this.addSecureHash(var3);
      this.messengerApi.reactToConversation(var1, var3).enqueue(this.callbacks.loggingCallback("add reaction to conversation"));
   }

   public void reactToLink(String var1, int var2) {
      HashMap var3 = new HashMap();
      var3.put("user", this.userIdentity.toMap());
      var3.put("reaction_index", Integer.valueOf(var2));
      this.addSecureHash(var3);
      this.messengerApi.reactToLink(var1, var3).enqueue(this.callbacks.loggingCallback("add reaction to link"));
   }

   public void removeDeviceToken(String var1) {
      HashMap var2 = new HashMap();
      var2.put("user", this.userIdentity.toMap());
      var2.put("device_token", var1);
      this.addSecureHash(var2);
      this.messengerApi.deleteDeviceToken(var2).enqueue(this.emptyCallback);
   }

   public void replyToConversation(String var1, List var2, int var3, String var4, boolean var5, boolean var6) {
      Map var7 = this.createBaseReplyParams();
      var7.put("blocks", var2);
      this.messengerApi.replyToConversation(var1, var7).enqueue(this.callbacks.replyCallback(var3, var5, var4, var1, var6));
   }

   void retriableUpdateUser(Map var1) {
      this.retriableUpdateUser(var1, this.userIdentity.getFingerprint());
   }

   public void satisfyOperatorCondition(String var1, String var2) {
      HashMap var3 = new HashMap();
      var3.put("user", this.userIdentity.toMap());
      var3.put("transition_id", var2);
      this.addSecureHash(var3);
      this.messengerApi.satisfyCondition(var1, var3).enqueue(this.emptyCallback);
   }

   public void sendErrorReport(ErrorReport var1) {
      HashMap var2 = new HashMap();
      var2.put("user", this.userIdentity.toMap());
      var2.put("error_report", var1);
      this.addSecureHash(var2);
      this.messengerApi.reportError(var2).enqueue(this.callbacks.loggingCallback("report error"));
   }

   public void sendMetrics(List var1, List var2, Callback var3) {
      HashMap var4 = new HashMap();
      var4.put("metrics", var1);
      var4.put("op_metrics", var2);
      this.messengerApi.sendMetrics(var4).enqueue(var3);
   }

   public void setDeviceToken(String var1) {
      Map var2 = DeviceData.generateDeviceData(this.context);
      var2.put("device_token", var1);
      HashMap var3 = new HashMap();
      var3.put("user", this.userIdentity.toMap());
      var3.put("device_data", var2);
      this.addSecureHash(var3);
      this.messengerApi.setDeviceToken(var3).enqueue(this.emptyCallback);
   }

   public void setIdleCallback(Runnable var1) {
      this.apiHttpClient.dispatcher().setIdleCallback(var1);
   }

   public void startNewConversation(List var1, int var2, String var3, boolean var4) {
      Map var5 = this.baseNewConversationParams();
      var5.put("blocks", var1);
      this.messengerApi.startNewConversation(var5).enqueue(this.callbacks.newConversationCallback(var2, var3, var4));
   }

   public void updateMaxRequests() {
      int var1;
      if(this.userIdentity.hasIntercomId()) {
         var1 = this.defaultOkHttpMaxRequests;
      } else {
         var1 = 1;
      }

      Dispatcher var2 = this.apiHttpClient.dispatcher();
      if(var2.getMaxRequests() != var1) {
         var2.setMaxRequests(var1);
      }

   }

   public void updateUser(UserUpdateRequest var1) {
      boolean var2;
      if(!var1.isInternalUpdate()) {
         var2 = true;
      } else {
         var2 = false;
      }

      if(var2) {
         if(this.rateLimiter.isLimited()) {
            this.rateLimiter.logError();
            return;
         }

         if(this.shouldStopBackgroundRequest(var1.isSentFromBackground())) {
            this.logBackgroundDisabledError();
            return;
         }

         this.rateLimiter.recordRequest();
      }

      this.retriableUpdateUser(this.generateUpdateUserParams(var1));
   }

   public void uploadFile(final c var1, final int var2, final String var3, final UploadingImageCache var4, final UploadProgressListener var5) {
      final String var8 = var1.b();
      final String var7 = var1.c();
      HashMap var6 = new HashMap();
      HashMap var9 = new HashMap();
      var9.put("original_filename", var8);
      var9.put("size_in_bytes", Integer.valueOf(var1.h()));
      var9.put("content_type", var7);
      var9.put("width", Integer.valueOf(var1.f()));
      var9.put("height", Integer.valueOf(var1.g()));
      var6.put("upload", var9);
      var6.put("user", this.userIdentity.toMap());
      this.addSecureHash(var6);
      this.messengerApi.uploadFile(var6).enqueue(new BaseCallback() {
         void logFailure(String var1x, ErrorObject var2x) {
            super.logFailure("Upload failed", var2x);
         }

         public void onError(ErrorObject var1x) {
            Api.this.bus.post(new UploadFailedEvent(var2, var3));
         }

         public void onSuccess(Upload.Builder var1x) {
            final Upload var2x = var1x.build();
            var4.put(var2x, var1);
            MultipartBody var3x = (new MultipartBody.Builder()).setType(MultipartBody.FORM).addFormDataPart("key", var2x.getKey()).addFormDataPart("acl", var2x.getAcl()).addFormDataPart("Content-Type", var2x.getContentType()).addFormDataPart("AWSAccessKeyId", var2x.getAwsAccessKey()).addFormDataPart("policy", var2x.getPolicy()).addFormDataPart("signature", var2x.getSignature()).addFormDataPart("success_action_status", var2x.getSuccessActionStatus()).addFormDataPart("file", var8, new ProgressRequestBody(MediaType.parse(var7), var1.j(), var5)).build();
            Api.this.httpClient.newCall((new Request.Builder()).url(var2x.getUploadDestination()).post(var3x).build()).enqueue(new io.intercom.okhttp3.Callback() {
               public void onFailure(io.intercom.okhttp3.Call var1x, IOException var2xx) {
                  Api.TWIG.e("Upload failed: " + var2xx.getMessage(), new Object[0]);
                  Api.this.bus.post(new UploadFailedEvent(var2, var3));
               }

               public void onResponse(io.intercom.okhttp3.Call var1x, io.intercom.okhttp3.Response var2xx) throws IOException {
                  Api.TWIG.internal("API Success", "Successfully uploaded");
                  if(var2xx.isSuccessful()) {
                     Api.this.bus.post(new UploadEvent(var2x, (long)var1.h(), var2, var3, var1.i()));
                  } else {
                     Api.TWIG.e("Upload failed: request body " + var2xx.body(), new Object[0]);
                     Api.this.bus.post(new UploadFailedEvent(var2, var3));
                  }

                  var2xx.body().close();
               }
            });
            Api.TWIG.internal("API Success", "Successfully uploaded");
         }
      });
   }
}
