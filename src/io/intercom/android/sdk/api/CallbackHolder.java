package io.intercom.android.sdk.api;

import io.intercom.android.sdk.actions.Actions;
import io.intercom.android.sdk.logger.LumberMill;
import io.intercom.android.sdk.models.Conversation;
import io.intercom.android.sdk.models.ConversationsResponse;
import io.intercom.android.sdk.models.Part;
import io.intercom.android.sdk.models.UsersResponse;
import io.intercom.android.sdk.models.events.ConversationEvent;
import io.intercom.android.sdk.models.events.NewConversationEvent;
import io.intercom.android.sdk.models.events.ReplyEvent;
import io.intercom.android.sdk.models.events.failure.ConversationFailedEvent;
import io.intercom.android.sdk.models.events.failure.NewConversationFailedEvent;
import io.intercom.android.sdk.models.events.failure.ReplyFailedEvent;
import io.intercom.android.sdk.state.ReplyPart;
import io.intercom.android.sdk.store.Store;
import io.intercom.android.sdk.twig.Twig;
import io.intercom.com.a.a.b;
import io.intercom.retrofit2.Call;
import io.intercom.retrofit2.Callback;
import io.intercom.retrofit2.Response;

class CallbackHolder {
   private static final Twig TWIG = LumberMill.getLogger();
   final b bus;
   final Store store;

   CallbackHolder(b var1, Store var2) {
      this.bus = var1;
      this.store = var2;
   }

   Callback conversationCallback() {
      return new BaseCallback() {
         public void onError(ErrorObject var1) {
            CallbackHolder.this.bus.post(new ConversationFailedEvent());
         }

         public void onSuccess(Conversation.Builder var1) {
            Conversation var2 = var1.build();
            CallbackHolder.this.store.dispatch(Actions.fetchConversationSuccess(var2));
            CallbackHolder.this.bus.post(new ConversationEvent(var2));
         }
      };
   }

   Callback inboxCallback() {
      return new BaseCallback() {
         public void onError(ErrorObject var1) {
            CallbackHolder.this.store.dispatch(Actions.fetchInboxFailed());
         }

         public void onSuccess(ConversationsResponse.Builder var1) {
            ConversationsResponse var2 = var1.build();
            CallbackHolder.this.store.dispatch(Actions.baseResponseReceived(var2));
            CallbackHolder.this.store.dispatch(Actions.fetchInboxSuccess(var2));
         }
      };
   }

   Callback loggingCallback(final String var1) {
      return new Callback() {
         public void onFailure(Call var1x, Throwable var2) {
            CallbackHolder.TWIG.internal(var1 + " failure");
         }

         public void onResponse(Call var1x, Response var2) {
            CallbackHolder.TWIG.internal(var1 + " success");
         }
      };
   }

   Callback newConversationCallback(final int var1, final String var2, final boolean var3) {
      return new BaseCallback() {
         protected void onError(ErrorObject var1x) {
            CallbackHolder.this.bus.post(new NewConversationFailedEvent(var1, var2));
         }

         public void onSuccess(Conversation.Builder var1x) {
            Conversation var2x = var1x.build();
            CallbackHolder.this.store.dispatch(Actions.newConversationSuccess(var2x));
            CallbackHolder.this.bus.post(new NewConversationEvent(var2x, var2, var3));
         }
      };
   }

   Callback replyCallback(final int var1, final boolean var2, final String var3, final String var4, final boolean var5) {
      return new BaseCallback() {
         public void onError(ErrorObject var1x) {
            CallbackHolder.this.bus.post(new ReplyFailedEvent(var1, var2, var3));
         }

         public void onSuccess(Part.Builder var1x) {
            Part var2x = var1x.build();
            CallbackHolder.this.store.dispatch(Actions.conversationReplySuccess(new ReplyPart(var2x, var4)));
            CallbackHolder.this.bus.post(new ReplyEvent(var2x, var1, var3, var4, var5));
         }
      };
   }

   BaseCallback unreadCallback() {
      return new BaseCallback() {
         public void onSuccess(UsersResponse.Builder var1) {
            UsersResponse var2 = var1.build();
            CallbackHolder.this.store.dispatch(Actions.baseResponseReceived(var2));
            CallbackHolder.this.store.dispatch(Actions.unreadConversationsSuccess(var2));
         }
      };
   }
}
