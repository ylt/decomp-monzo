package io.intercom.android.sdk.api;

import android.content.SharedPreferences;
import io.intercom.android.sdk.Provider;
import io.intercom.android.sdk.commons.utilities.TimeProvider;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.com.google.gson.JsonSyntaxException;
import io.intercom.com.google.gson.e;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class DeDuper {
   private static final String CUSTOM_ATTRIBUTES = "custom_attributes";
   private static final String EMPTY_JSON = "{}";
   private static final String PREFS_CACHED_ATTRIBUTES = "CachedAttributes";
   private static final String PREFS_EARLIEST_UPDATE_AT = "EarliestUpdateAt";
   private final Provider appConfigProvider;
   private final Map cachedAttributes = new HashMap();
   private long earliestUpdateAt = 0L;
   private final e gson = new e();
   private final SharedPreferences prefs;

   public DeDuper(Provider var1, SharedPreferences var2) {
      this.appConfigProvider = var1;
      this.prefs = var2;
   }

   private static boolean containsOnlyEmptyCustomAttributes(Map var0) {
      boolean var1 = true;
      if(var0.size() != 1 || !Collections.EMPTY_MAP.equals(var0.get("custom_attributes"))) {
         var1 = false;
      }

      return var1;
   }

   private static Map getCustomAttributes(Map var0) {
      Object var1 = var0.get("custom_attributes");
      if(var1 instanceof Map) {
         var0 = (Map)var1;
      } else {
         var0 = null;
      }

      return var0;
   }

   private boolean hasExpiredCache() {
      boolean var1 = false;
      if(this.earliestUpdateAt != 0L) {
         long var2 = ((AppConfig)this.appConfigProvider.get()).getUserUpdateCacheMaxAgeMs();
         if(TimeProvider.SYSTEM.currentTimeMillis() - this.earliestUpdateAt > var2) {
            var1 = true;
         } else {
            var1 = false;
         }
      }

      return var1;
   }

   private boolean hasNewAttributeValues(Map var1) {
      Map var3 = getCustomAttributes(this.cachedAttributes);
      Map var4 = getCustomAttributes(var1);
      boolean var2;
      Entry var9;
      if(var4 != null) {
         if(var3 == null) {
            var2 = true;
            return var2;
         }

         Iterator var5 = var4.entrySet().iterator();

         while(var5.hasNext()) {
            var9 = (Entry)var5.next();
            Object var6 = var9.getValue();
            if(var6 != null && !var6.equals(var3.get(var9.getKey()))) {
               var2 = true;
               return var2;
            }
         }
      }

      Iterator var7 = var1.entrySet().iterator();

      while(true) {
         if(!var7.hasNext()) {
            var2 = false;
            break;
         }

         var9 = (Entry)var7.next();
         if(!"custom_attributes".equals(var9.getKey())) {
            Object var8 = var9.getValue();
            if(var8 != null && !var8.equals(this.cachedAttributes.get(var9.getKey()))) {
               var2 = true;
               break;
            }
         }
      }

      return var2;
   }

   private boolean isEmpty() {
      return this.cachedAttributes.isEmpty();
   }

   private static Map mergeMaps(Map var0, Map var1) {
      HashMap var2 = new HashMap(var0);
      var2.putAll(var1);
      return var2;
   }

   private void persistCachedAttributes() {
      HashMap var1 = new HashMap(this.cachedAttributes);
      this.prefs.edit().putString("CachedAttributes", this.gson.a((Object)var1)).putLong("EarliestUpdateAt", this.earliestUpdateAt).apply();
   }

   long getEarliestUpdateAt() {
      return this.earliestUpdateAt;
   }

   Map getMap() {
      return this.cachedAttributes;
   }

   public void readPersistedCachedAttributes() {
      if(this.prefs.contains("CachedAttributes") && this.prefs.contains("EarliestUpdateAt")) {
         String var1 = this.prefs.getString("CachedAttributes", "{}");

         Map var3;
         try {
            var3 = (Map)this.gson.a(var1, Map.class);
         } catch (JsonSyntaxException var2) {
            return;
         }

         this.cachedAttributes.putAll(var3);
         this.earliestUpdateAt = this.prefs.getLong("EarliestUpdateAt", 0L);
      }

   }

   public void reset() {
      this.cachedAttributes.clear();
      this.earliestUpdateAt = 0L;
      this.persistCachedAttributes();
   }

   void setEarliestUpdateAt(long var1) {
      this.earliestUpdateAt = var1;
   }

   public boolean shouldUpdateUser(Map var1) {
      boolean var3 = false;
      boolean var2 = var3;
      if(!var1.isEmpty()) {
         if(containsOnlyEmptyCustomAttributes(var1)) {
            var2 = var3;
         } else {
            if(!this.hasNewAttributeValues(var1) && !this.isEmpty()) {
               var2 = var3;
               if(!this.hasExpiredCache()) {
                  return var2;
               }
            }

            var2 = true;
         }
      }

      return var2;
   }

   public void update(Map var1) {
      if(this.hasExpiredCache()) {
         this.cachedAttributes.clear();
         this.setEarliestUpdateAt(TimeProvider.SYSTEM.currentTimeMillis());
      }

      if(this.earliestUpdateAt == 0L) {
         this.setEarliestUpdateAt(TimeProvider.SYSTEM.currentTimeMillis());
      }

      Map var2 = getCustomAttributes(this.cachedAttributes);
      Map var3 = getCustomAttributes(var1);
      this.cachedAttributes.putAll(var1);
      if(var2 != null && var3 != null) {
         this.cachedAttributes.put("custom_attributes", mergeMaps(var2, var3));
      }

      this.persistCachedAttributes();
   }
}
