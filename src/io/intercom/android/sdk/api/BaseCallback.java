package io.intercom.android.sdk.api;

import io.intercom.android.sdk.logger.LumberMill;
import io.intercom.android.sdk.twig.Twig;
import io.intercom.retrofit2.Call;
import io.intercom.retrofit2.Callback;
import io.intercom.retrofit2.Response;

public abstract class BaseCallback implements Callback {
   private final Twig twig = LumberMill.getLogger();

   static String getDetails(ErrorObject var0) {
      String var2;
      if(var0.hasErrorBody()) {
         var2 = var0.getErrorBody();
      } else {
         var2 = var0.getThrowable().getMessage();
      }

      String var1 = var2;
      if(var2 == null) {
         var1 = "unknown error";
      }

      return var1;
   }

   private void handleError(ErrorObject var1) {
      this.logFailure("Api call failed", var1);
      this.onError(var1);
   }

   void logFailure(String var1, ErrorObject var2) {
      this.twig.e(var1 + ": " + getDetails(var2), new Object[0]);
   }

   protected void onError(ErrorObject var1) {
   }

   public final void onFailure(Call var1, Throwable var2) {
      this.handleError(new ErrorObject(var2, (Response)null));
   }

   public final void onResponse(Call var1, Response var2) {
      if(var2 == null) {
         this.handleError(new ErrorObject(new IllegalStateException("No body returned from the server"), (Response)null));
      } else if(var2.body() == null) {
         this.handleError(new ErrorObject(new IllegalStateException("No body returned from the server"), var2));
      } else if(!var2.isSuccessful()) {
         this.handleError(new ErrorObject(new Exception("Status code outside the 200-300 range"), var2));
      } else {
         this.onSuccess(var2.body());
      }

   }

   protected abstract void onSuccess(Object var1);
}
