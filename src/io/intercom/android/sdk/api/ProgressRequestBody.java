package io.intercom.android.sdk.api;

import io.intercom.a.d;
import io.intercom.android.sdk.conversation.UploadProgressListener;
import io.intercom.okhttp3.MediaType;
import io.intercom.okhttp3.RequestBody;
import java.io.File;
import java.io.IOException;

class ProgressRequestBody extends RequestBody {
   private static final int SEGMENT_SIZE = 2048;
   private final MediaType contentType;
   private final File file;
   private final UploadProgressListener listener;

   public ProgressRequestBody(MediaType var1, File var2, UploadProgressListener var3) {
      this.contentType = var1;
      this.file = var2;
      this.listener = var3;
   }

   public long contentLength() {
      return this.file.length();
   }

   public MediaType contentType() {
      return this.contentType;
   }

   public void writeTo(d param1) throws IOException {
      // $FF: Couldn't be decompiled
   }
}
