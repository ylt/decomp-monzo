package io.intercom.android.sdk.api;

import io.intercom.okhttp3.Interceptor;
import io.intercom.okhttp3.Response;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

class RetryInterceptor implements Interceptor {
   private static final int MAX_RETRIES = 3;
   private final RetryInterceptor.Sleeper sleeper;

   public RetryInterceptor(RetryInterceptor.Sleeper var1) {
      this.sleeper = var1;
   }

   static int getRetryTimer(int var0) {
      return (int)Math.pow(2.0D, (double)var0);
   }

   public Response intercept(Interceptor.Chain var1) throws IOException {
      int var2 = 0;

      while(var2 <= 3) {
         try {
            Response var3 = var1.proceed(var1.request());
            return var3;
         } catch (IOException var4) {
            if(var2 == 3) {
               throw var4;
            }

            this.sleeper.sleep(getRetryTimer(var2 + 1));
            ++var2;
         }
      }

      throw new IOException("request failed due to network errors");
   }

   public static class Sleeper {
      public void sleep(int var1) {
         try {
            TimeUnit.SECONDS.sleep((long)var1);
         } catch (InterruptedException var3) {
            Thread.currentThread().interrupt();
         }

      }
   }
}
