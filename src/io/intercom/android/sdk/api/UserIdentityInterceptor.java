package io.intercom.android.sdk.api;

import io.intercom.android.sdk.identity.UserIdentity;
import io.intercom.android.sdk.logger.LumberMill;
import io.intercom.android.sdk.twig.Twig;
import io.intercom.okhttp3.Interceptor;
import io.intercom.okhttp3.Response;
import java.io.IOException;

class UserIdentityInterceptor implements Interceptor {
   private static final String NO_USER_IDENTITY = "A network request was made with no user registered on this device.Please call registerUnidentifiedUser() or registerIdentifiedUser(Registration).";
   private static final String USER_IDENTITY_CHANGED = "registered user changed while this request was in flight";
   private final Twig twig = LumberMill.getLogger();
   private final UserIdentity userIdentity;

   public UserIdentityInterceptor(UserIdentity var1) {
      this.userIdentity = var1;
   }

   public Response intercept(Interceptor.Chain var1) throws IOException {
      if(this.userIdentity.identityExists()) {
         String var2 = this.userIdentity.getFingerprint();
         Response var3 = var1.proceed(var1.request());
         if(var2.equals(this.userIdentity.getFingerprint())) {
            this.twig.internal("interceptor", "proceeding");
            return var3;
         } else {
            this.twig.internal("interceptor", "halting: user identity changed");
            if(var3 != null) {
               var3.body().close();
            }

            throw new IOException("registered user changed while this request was in flight");
         }
      } else {
         throw new IOException("A network request was made with no user registered on this device.Please call registerUnidentifiedUser() or registerIdentifiedUser(Registration).");
      }
   }
}
