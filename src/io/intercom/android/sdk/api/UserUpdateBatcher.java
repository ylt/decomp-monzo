package io.intercom.android.sdk.api;

import android.os.Handler;
import android.os.Looper;
import io.intercom.android.sdk.Provider;
import io.intercom.android.sdk.actions.Actions;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.android.sdk.store.Store;
import java.util.UUID;

public class UserUpdateBatcher {
   private final Provider apiProvider;
   private final Provider appConfigProvider;
   private final Handler handler;
   private UserUpdateRequest pendingUpdate;
   private String pendingUpdateIdentifier;
   private final Store store;

   UserUpdateBatcher(Handler var1, Provider var2, Provider var3, Store var4) {
      this.apiProvider = var2;
      this.handler = var1;
      this.appConfigProvider = var3;
      this.store = var4;
      this.resetPendingUpdate();
   }

   public static UserUpdateBatcher create(Provider var0, Provider var1, Store var2) {
      return new UserUpdateBatcher(new Handler(Looper.getMainLooper()), var0, var1, var2);
   }

   private long maxDelay(UserUpdateRequest var1) {
      AppConfig var4 = (AppConfig)this.appConfigProvider.get();
      long var2;
      if(var1.isInternalUpdate()) {
         var2 = var4.getPingDelayMs();
      } else {
         var2 = var4.getBatchUserUpdatePeriodMs();
      }

      return var2;
   }

   private void resetPendingUpdate() {
      this.pendingUpdate = new UserUpdateRequest();
      this.pendingUpdateIdentifier = UUID.randomUUID().toString();
   }

   private void submitPendingUpdate(UserUpdateRequest var1) {
      if(var1.isValidUpdate()) {
         if(var1.isNewSession()) {
            this.store.dispatch(Actions.sessionStarted());
         }

         ((Api)this.apiProvider.get()).updateUser(var1);
         this.resetPendingUpdate();
      }

   }

   private void submitPendingUpdate(String param1) {
      // $FF: Couldn't be decompiled
   }

   private void submitPendingUpdateWithDelay(long var1) {
      if(var1 > 0L) {
         final String var3 = this.pendingUpdateIdentifier;
         this.handler.postDelayed(new Runnable() {
            public void run() {
               UserUpdateBatcher.this.submitPendingUpdate(var3);
            }
         }, var1);
      } else {
         this.submitPendingUpdate();
      }

   }

   public void submitPendingUpdate() {
      synchronized(this){}

      try {
         this.submitPendingUpdate(this.pendingUpdate);
      } finally {
         ;
      }

   }

   public void updateUser(UserUpdateRequest param1) {
      // $FF: Couldn't be decompiled
   }
}
