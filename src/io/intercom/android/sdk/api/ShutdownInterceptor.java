package io.intercom.android.sdk.api;

import io.intercom.android.sdk.logger.LumberMill;
import io.intercom.android.sdk.twig.Twig;
import io.intercom.okhttp3.Interceptor;
import io.intercom.okhttp3.Response;
import java.io.IOException;

public class ShutdownInterceptor implements Interceptor {
   private static final String ERROR = "error";
   private static final String MESSAGE = "message";
   private static final String MESSENGER_SHUTDOWN_RESPONSE = "messenger_shutdown_response";
   private static final String SHUTDOWN_PERIOD = "shutdown_period";
   private static final String TYPE = "type";
   private final ShutdownState shutdownState;
   private final Twig twig = LumberMill.getLogger();

   public ShutdownInterceptor(ShutdownState var1) {
      this.shutdownState = var1;
   }

   public Response intercept(Interceptor.Chain param1) throws IOException {
      // $FF: Couldn't be decompiled
   }
}
