package io.intercom.android.sdk.api;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Base64;
import io.intercom.android.sdk.commons.utilities.DeviceUtils;
import io.intercom.android.sdk.identity.AppIdentity;
import io.intercom.okhttp3.Interceptor;
import io.intercom.okhttp3.Request;
import io.intercom.okhttp3.Response;
import java.io.IOException;
import java.util.Locale;

class HeaderInterceptor implements Interceptor {
   private static final String ACCEPT_LANGUAGE = "Accept-Language";
   private static final String ANDROID_HEADER = "intercom-android-sdk/";
   private static final String AUTHORIZATION = "Authorization";
   private static final String CONTENT_TYPE_KEY = "Content-Type";
   private static final String CONTENT_TYPE_VALUE = "application/json";
   private static final String CORDOVA_HEADER = "intercom-sdk-cordova/";
   private static final String CORDOVA_PREFS = "intercomsdk_cordova_prefs";
   private static final String CORDOVA_VERSION = "cordova_version";
   private static final String HOST_APP_VERSION_KEY = "X-INTERCOM-HOST-APP-VERSION";
   private static final String INTERCOM_AGENT = "X-INTERCOM-AGENT";
   private static final String INTERCOM_AGENT_WRAPPER = "X-INTERCOM-AGENT-WRAPPER";
   private static final String SUPPORTED_LANGUAGES_KEY = "X-INTERCOM-SUPPORTED-LANGUAGES";
   private static final String SUPPORTED_LANGUAGE_LIST = "ar,bg,bs,ca,cs,da,de,de-form,el,es,et,fi,fr,he,hr,hu,id,it,ja,ko,lt,lv,mn,nb,nl,pl,pt,pt-BR,ro,ru,sl,sr,sv,tr,vi,zh-Hant,zh-Hans";
   private final AppIdentity appIdentity;
   private final String appVersion;
   private final String cordovaVersion;
   private final Locale userLocale;
   private final String versionName;

   HeaderInterceptor(String var1, String var2, AppIdentity var3, Locale var4, String var5) {
      this.cordovaVersion = var1;
      this.versionName = var2;
      this.appIdentity = var3;
      this.userLocale = var4;
      this.appVersion = var5;
   }

   public static HeaderInterceptor create(Context var0, AppIdentity var1) {
      return new HeaderInterceptor(var0.getSharedPreferences("intercomsdk_cordova_prefs", 0).getString("cordova_version", ""), "4.0.5", var1, var0.getResources().getConfiguration().locale, DeviceUtils.getAppVersion(var0));
   }

   private String getBasicAuth() {
      String var1 = this.appIdentity.appId() + ":" + this.appIdentity.apiKey();
      return "Basic " + Base64.encodeToString(var1.getBytes(), 2);
   }

   @SuppressLint({"CommitPrefEdits"})
   public static void setCordovaVersion(Context var0, String var1) {
      var0.getSharedPreferences("intercomsdk_cordova_prefs", 0).edit().putString("cordova_version", var1).apply();
   }

   public Response intercept(Interceptor.Chain var1) throws IOException {
      Request.Builder var2 = var1.request().newBuilder().header("Content-Type", "application/json").header("Authorization", this.getBasicAuth()).header("X-INTERCOM-AGENT", "intercom-android-sdk/" + this.versionName).header("Accept-Language", this.userLocale.getLanguage() + "-" + this.userLocale.getCountry()).header("X-INTERCOM-SUPPORTED-LANGUAGES", "ar,bg,bs,ca,cs,da,de,de-form,el,es,et,fi,fr,he,hr,hu,id,it,ja,ko,lt,lv,mn,nb,nl,pl,pt,pt-BR,ro,ru,sl,sr,sv,tr,vi,zh-Hant,zh-Hans").header("X-INTERCOM-HOST-APP-VERSION", this.appVersion);
      if(!this.cordovaVersion.isEmpty()) {
         var2.header("X-INTERCOM-AGENT-WRAPPER", "intercom-sdk-cordova/" + this.cordovaVersion);
      }

      return var1.proceed(var2.build());
   }
}
