package io.intercom.android.sdk.api;

import android.content.Context;
import android.content.SharedPreferences;
import io.intercom.android.sdk.commons.utilities.DeviceUtils;
import io.intercom.android.sdk.commons.utilities.TimeProvider;
import io.intercom.android.sdk.identity.AppIdentity;

public class ShutdownState {
   private static final String PREFS_SHUTDOWN_EXPIRY = "ShutdownExpiry";
   private static final String PREFS_SHUTDOWN_FINGERPRINT = "ShutdownFingerprint";
   private static final String PREFS_SHUTDOWN_REASON = "ShutdownReason";
   private final AppIdentity appIdentity;
   private final Context context;
   private final SharedPreferences prefs;
   long shutdownExpiry;
   String shutdownFingerprint;
   private String shutdownReason;
   private final TimeProvider timeProvider;

   public ShutdownState(Context var1, AppIdentity var2, TimeProvider var3) {
      this.context = var1;
      this.appIdentity = var2;
      this.prefs = var1.getSharedPreferences("INTERCOM_SHUTDOWN_PREFS", 0);
      this.timeProvider = var3;
      this.shutdownExpiry = this.prefs.getLong("ShutdownExpiry", var3.currentTimeMillis());
      this.shutdownReason = this.prefs.getString("ShutdownReason", "");
      this.shutdownFingerprint = this.prefs.getString("ShutdownFingerprint", generateAppFingerprint(var1, var2));
   }

   private static String generateAppFingerprint(Context var0, AppIdentity var1) {
      return var0.getPackageName() + "-" + DeviceUtils.getAppVersion(var0) + "-" + var1.appId() + "-" + "4.0.5";
   }

   private void persistCachedAttributes() {
      this.prefs.edit().putString("ShutdownFingerprint", this.shutdownFingerprint).putString("ShutdownReason", this.shutdownReason).putLong("ShutdownExpiry", this.shutdownExpiry).apply();
   }

   public boolean canSendNetworkRequests() {
      boolean var2 = false;
      boolean var1;
      if(this.shutdownExpiry <= this.timeProvider.currentTimeMillis()) {
         var1 = true;
      } else {
         var1 = false;
      }

      boolean var3 = this.shutdownFingerprint.equals(generateAppFingerprint(this.context, this.appIdentity));
      if(var1 || !var3) {
         var2 = true;
      }

      return var2;
   }

   public String getShutdownReason() {
      return this.shutdownReason;
   }

   public void updateShutdownState(long var1, String var3) {
      this.shutdownExpiry = this.timeProvider.currentTimeMillis() + var1;
      this.shutdownReason = var3;
      this.shutdownFingerprint = generateAppFingerprint(this.context, this.appIdentity);
      this.persistCachedAttributes();
   }
}
