package io.intercom.android.sdk.api;

import io.intercom.android.sdk.logger.LumberMill;
import io.intercom.android.sdk.twig.Twig;
import io.intercom.retrofit2.Response;
import java.io.IOException;

public class ErrorObject {
   private final String errorBody;
   private final int statusCode;
   private final Throwable throwable;
   private final Twig twig = LumberMill.getLogger();

   ErrorObject(Throwable var1, Response var2) {
      this.throwable = var1;
      this.errorBody = this.parseErrorBody(var2);
      this.statusCode = this.parseStatusCode(var2);
   }

   private String parseErrorBody(Response var1) {
      String var3;
      if(var1 != null && var1.errorBody() != null) {
         try {
            var3 = var1.errorBody().string();
            return var3;
         } catch (IOException var2) {
            this.twig.internal("Couldn't parse error body: " + var2.getMessage());
         }
      }

      var3 = null;
      return var3;
   }

   private int parseStatusCode(Response var1) {
      int var2;
      if(var1 != null) {
         var2 = var1.code();
      } else {
         var2 = -1;
      }

      return var2;
   }

   String getErrorBody() {
      return this.errorBody;
   }

   int getStatusCode() {
      return this.statusCode;
   }

   public Throwable getThrowable() {
      return this.throwable;
   }

   boolean hasErrorBody() {
      boolean var1;
      if(this.errorBody != null) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }
}
