package io.intercom.android.sdk.api;

import android.content.Context;
import io.intercom.android.sdk.Provider;
import io.intercom.android.sdk.commons.utilities.TimeProvider;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.android.sdk.identity.AppIdentity;
import io.intercom.android.sdk.identity.UserIdentity;
import io.intercom.android.sdk.store.Store;
import io.intercom.com.a.a.b;
import io.intercom.com.google.gson.e;
import io.intercom.okhttp3.Cache;
import io.intercom.okhttp3.CertificatePinner;
import io.intercom.okhttp3.OkHttpClient;
import io.intercom.retrofit2.Retrofit;
import io.intercom.retrofit2.converter.gson.GsonConverterFactory;
import java.io.File;
import java.util.concurrent.TimeUnit;

public class ApiFactory {
   private static final String CACHE_NAME = "Intercom_SDK/HttpCache";
   private static final int CACHE_SIZE = 10485760;
   private static final String ENDPOINT = "/messenger/mobile/";
   private static final int MAX_DNS_SEGMENT_SIZE = 63;
   private static final String PARTIAL_HOSTNAME = ".mobile-sdk-api.intercom.io";
   private static final String PROTOCOL = "https://";

   static String convertHostnameToUrl(String var0) {
      return "https://" + var0 + "/messenger/mobile/";
   }

   public static Api create(Context var0, AppIdentity var1, UserIdentity var2, b var3, Store var4, String var5, Provider var6, e var7) {
      return createWithNetworkClient(var0, var1, var2, var3, createConfigurableHttpClient(var0, var1, var2).build(), var4, var5, var6, var7);
   }

   public static OkHttpClient.Builder createConfigurableHttpClient(Context var0, AppIdentity var1, UserIdentity var2) {
      OkHttpClient.Builder var5 = (new OkHttpClient.Builder()).readTimeout(2L, TimeUnit.MINUTES).connectTimeout(2L, TimeUnit.MINUTES).writeTimeout(2L, TimeUnit.MINUTES).addInterceptor(new UserIdentityInterceptor(var2)).addInterceptor(new RetryInterceptor(new RetryInterceptor.Sleeper())).addInterceptor(new ShutdownInterceptor(new ShutdownState(var0, var1, TimeProvider.SYSTEM))).addNetworkInterceptor(HeaderInterceptor.create(var0, var1));
      File var3 = var0.getCacheDir();
      if(var3 != null) {
         var5.cache(new Cache(new File(var3.getAbsolutePath(), "Intercom_SDK/HttpCache"), 10485760L));
      }

      String var4 = getFullHostname(var1.appId());
      var5.certificatePinner((new CertificatePinner.Builder()).add(var4, new String[]{"sha1/BiCgk94N+oILO/VULX+wYS6gWKU="}).build());
      return var5;
   }

   private static MessengerApi createRetrofitClient(OkHttpClient var0, String var1, e var2) {
      return (MessengerApi)(new Retrofit.Builder()).baseUrl(var1).client(var0).addConverterFactory(GsonConverterFactory.create(var2)).build().create(MessengerApi.class);
   }

   static String createUniqueIdentifier(String var0) {
      byte var1 = 62;
      var0 = removeInvalidCharacters(var0) + "-android";
      if(var0.length() > 63) {
         if(var0.charAt(62) != 45) {
            var1 = 63;
         }

         var0 = var0.substring(0, var1);
      }

      return var0;
   }

   public static Api createWithNetworkClient(Context var0, AppIdentity var1, UserIdentity var2, b var3, OkHttpClient var4, Store var5, String var6, Provider var7, e var8) {
      return new Api(var0, var1, var2, var3, var4, createRetrofitClient(var4, var6, var8), new CallbackHolder(var3, var5), new RateLimiter((AppConfig)var7.get()), var5, var7);
   }

   static String getFullHostname(String var0) {
      return createUniqueIdentifier(var0) + ".mobile-sdk-api.intercom.io";
   }

   public static String getHostname(AppIdentity var0) {
      return convertHostnameToUrl(getFullHostname(var0.appId()));
   }

   public static String removeInvalidCharacters(String var0) {
      return var0.replaceAll("[^A-Za-z0-9\\-$]", "");
   }
}
