package io.intercom.android.sdk.api;

import io.intercom.retrofit2.Call;
import io.intercom.retrofit2.http.Body;
import io.intercom.retrofit2.http.POST;
import io.intercom.retrofit2.http.PUT;
import io.intercom.retrofit2.http.Path;
import java.util.Map;

interface MessengerApi {
   @POST("conversations/{conversationId}/remark")
   Call addConversationRatingRemark(@Path("conversationId") String var1, @Body Map var2);

   @PUT("device_tokens")
   Call deleteDeviceToken(@Body Map var1);

   @POST("conversations/{conversationId}")
   Call getConversation(@Path("conversationId") String var1, @Body Map var2);

   @POST("conversations/inbox")
   Call getConversations(@Body Map var1);

   @POST("gifs")
   Call getGifs(@Body Map var1);

   @POST("articles/{articleId}")
   Call getLink(@Path("articleId") String var1, @Body Map var2);

   @POST("conversations/unread")
   Call getUnreadConversations(@Body Map var1);

   @POST("events")
   Call logEvent(@Body Map var1);

   @POST("conversations/dismiss")
   Call markAsDismissed(@Body Map var1);

   @POST("conversations/{conversationId}/read")
   Call markAsRead(@Path("conversationId") String var1, @Body Map var2);

   @POST("conversations/{conversationId}/rate")
   Call rateConversation(@Path("conversationId") String var1, @Body Map var2);

   @POST("conversations/{conversationId}/react")
   Call reactToConversation(@Path("conversationId") String var1, @Body Map var2);

   @POST("articles/{articleId}/react")
   Call reactToLink(@Path("articleId") String var1, @Body Map var2);

   @POST("conversations/{conversationId}/reply")
   Call replyToConversation(@Path("conversationId") String var1, @Body Map var2);

   @POST("error_reports")
   Call reportError(@Body Map var1);

   @POST("conversations/{conversationId}/conditions_satisfied")
   Call satisfyCondition(@Path("conversationId") String var1, @Body Map var2);

   @POST("metrics")
   Call sendMetrics(@Body Map var1);

   @POST("device_tokens")
   Call setDeviceToken(@Body Map var1);

   @POST("conversations")
   Call startNewConversation(@Body Map var1);

   @POST("users")
   Call updateUser(@Body Map var1);

   @POST("uploads")
   Call uploadFile(@Body Map var1);
}
