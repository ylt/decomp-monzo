package io.intercom.android.sdk.api;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class UserUpdateRequest {
   private static final String CUSTOM_ATTRIBUTES = "custom_attributes";
   private final Map attributes;
   private final int batchSize;
   private final boolean internalUpdate;
   private final boolean newSession;
   private final boolean sentFromBackground;

   public UserUpdateRequest() {
      this(false, true, (Map)null, 0, false);
   }

   UserUpdateRequest(boolean var1, boolean var2, Map var3, int var4, boolean var5) {
      this.attributes = new HashMap();
      if(var3 != null) {
         this.attributes.putAll(var3);
      }

      this.sentFromBackground = var2;
      this.newSession = var1;
      this.batchSize = var4;
      this.internalUpdate = var5;
   }

   public UserUpdateRequest(boolean var1, boolean var2, Map var3, boolean var4) {
      this(var1, var2, var3, 1, var4);
   }

   public UserUpdateRequest(boolean var1, boolean var2, boolean var3) {
      this(var1, var2, (Map)null, 1, var3);
   }

   private boolean attributesWillOverwriteExistingAttributes(Map var1) {
      Map var4 = getCustomAttributes(this.attributes);
      Map var3 = getCustomAttributes(var1);
      boolean var2;
      Iterator var5;
      Object var8;
      if(var4 != null && var3 != null) {
         var5 = var4.entrySet().iterator();

         while(var5.hasNext()) {
            Entry var6 = (Entry)var5.next();
            var8 = var3.get(var6.getKey());
            if(var8 != null && !var8.equals(var6.getValue())) {
               var2 = true;
               return var2;
            }
         }
      }

      var5 = this.attributes.entrySet().iterator();

      while(true) {
         if(!var5.hasNext()) {
            var2 = false;
            break;
         }

         Entry var7 = (Entry)var5.next();
         if(!"custom_attributes".equals(var7.getKey())) {
            var8 = var1.get(var7.getKey());
            if(var8 != null && !var8.equals(var7.getValue())) {
               var2 = true;
               break;
            }
         }
      }

      return var2;
   }

   private static Map getCustomAttributes(Map var0) {
      Object var1 = var0.get("custom_attributes");
      if(var1 instanceof Map) {
         var0 = (Map)var1;
      } else {
         var0 = null;
      }

      return var0;
   }

   private static Map mergeAttributes(Map var0, Map var1) {
      HashMap var3 = new HashMap(var0);
      Map var2 = getCustomAttributes(var3);
      Map var4 = getCustomAttributes(var1);
      var3.putAll(var1);
      if(var4 != null && !var4.isEmpty()) {
         Object var5 = var2;
         if(var2 == null) {
            var5 = new HashMap();
         }

         ((Map)var5).putAll(var4);
         var3.put("custom_attributes", var5);
      } else if(var2 != null) {
         var3.put("custom_attributes", var2);
      } else {
         var3.remove("custom_attributes");
      }

      return var3;
   }

   public boolean canMergeUpdate(UserUpdateRequest var1) {
      boolean var2;
      if(this.attributesWillOverwriteExistingAttributes(var1.attributes) || this.newSession && var1.newSession) {
         var2 = false;
      } else {
         var2 = true;
      }

      return var2;
   }

   public boolean equals(Object var1) {
      boolean var3 = false;
      boolean var2;
      if(this == var1) {
         var2 = true;
      } else {
         var2 = var3;
         if(var1 != null) {
            var2 = var3;
            if(this.getClass() == var1.getClass()) {
               UserUpdateRequest var4 = (UserUpdateRequest)var1;
               var2 = var3;
               if(this.newSession == var4.newSession) {
                  var2 = var3;
                  if(this.batchSize == var4.batchSize) {
                     var2 = var3;
                     if(this.sentFromBackground == var4.sentFromBackground) {
                        var2 = var3;
                        if(this.internalUpdate == var4.internalUpdate) {
                           var2 = this.attributes.equals(var4.attributes);
                        }
                     }
                  }
               }
            }
         }
      }

      return var2;
   }

   public Map getAttributes() {
      return this.attributes;
   }

   public int getBatchSize() {
      return this.batchSize;
   }

   public int hashCode() {
      byte var3 = 1;
      int var4 = this.attributes.hashCode();
      byte var1;
      if(this.newSession) {
         var1 = 1;
      } else {
         var1 = 0;
      }

      int var5 = this.batchSize;
      byte var2;
      if(this.sentFromBackground) {
         var2 = 1;
      } else {
         var2 = 0;
      }

      if(!this.internalUpdate) {
         var3 = 0;
      }

      return (var2 + ((var1 + var4 * 31) * 31 + var5) * 31) * 31 + var3;
   }

   public boolean isInternalUpdate() {
      return this.internalUpdate;
   }

   public boolean isNewSession() {
      return this.newSession;
   }

   public boolean isSentFromBackground() {
      return this.sentFromBackground;
   }

   public boolean isValidUpdate() {
      boolean var1;
      if(!this.internalUpdate && this.attributes.isEmpty()) {
         var1 = false;
      } else {
         var1 = true;
      }

      return var1;
   }

   public UserUpdateRequest merge(UserUpdateRequest var1) {
      boolean var5 = true;
      boolean var3;
      if(!this.internalUpdate && !var1.internalUpdate) {
         var3 = false;
      } else {
         var3 = true;
      }

      boolean var4;
      if(!this.newSession && !var1.newSession) {
         var4 = false;
      } else {
         var4 = true;
      }

      Map var6 = mergeAttributes(this.attributes, var1.attributes);
      if(!this.sentFromBackground || !var1.sentFromBackground) {
         var5 = false;
      }

      int var2 = this.batchSize;
      return new UserUpdateRequest(var4, var5, var6, var1.batchSize + var2, var3);
   }
}
