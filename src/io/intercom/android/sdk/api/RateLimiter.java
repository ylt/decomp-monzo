package io.intercom.android.sdk.api;

import io.intercom.android.sdk.commons.utilities.TimeProvider;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.android.sdk.logger.LumberMill;
import io.intercom.android.sdk.twig.Twig;

class RateLimiter {
   private final AppConfig appConfig;
   private int limitedRequestCount;
   private long periodStartTimestamp;
   private final TimeProvider timeProvider;
   private final Twig twig;

   RateLimiter(AppConfig var1) {
      this(var1, TimeProvider.SYSTEM);
   }

   RateLimiter(AppConfig var1, TimeProvider var2) {
      this.twig = LumberMill.getLogger();
      this.appConfig = var1;
      this.timeProvider = var2;
   }

   private boolean hasReachedMaxCount() {
      boolean var1;
      if(this.limitedRequestCount >= this.appConfig.getRateLimitCount()) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   private boolean isInsideCurrentTimePeriod() {
      boolean var1;
      if(this.timeProvider.currentTimeMillis() - this.periodStartTimestamp < this.appConfig.getRateLimitPeriodMs()) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   boolean isLimited() {
      boolean var1;
      if(this.isInsideCurrentTimePeriod() && this.hasReachedMaxCount()) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   void logError() {
      this.twig.e("Your app is being rate limited because you're performing too many requests per minute", new Object[0]);
   }

   void recordRequest() {
      if(!this.isInsideCurrentTimePeriod()) {
         this.periodStartTimestamp = this.timeProvider.currentTimeMillis();
         this.limitedRequestCount = 0;
      }

      ++this.limitedRequestCount;
   }
}
