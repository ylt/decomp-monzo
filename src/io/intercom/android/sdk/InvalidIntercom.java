package io.intercom.android.sdk;

import android.app.TaskStackBuilder;
import io.intercom.android.sdk.identity.Registration;
import io.intercom.android.sdk.logger.LumberMill;
import io.intercom.android.sdk.twig.Twig;
import java.util.Map;

class InvalidIntercom extends Intercom {
   private final Twig twig = LumberMill.getLogger();

   private void logIncorrectInitialisationWarning() {
      this.twig.e("Intercom has been initialized incorrectly. Please make sure the first Intercom method you call is initialize() and that you're passing in the correct app ID and API key", new Object[0]);
   }

   public void addUnreadConversationCountListener(UnreadConversationCountListener var1) {
      this.logIncorrectInitialisationWarning();
   }

   public void displayConversationsList() {
      this.logIncorrectInitialisationWarning();
   }

   public void displayMessageComposer() {
      this.logIncorrectInitialisationWarning();
   }

   public void displayMessageComposer(String var1) {
      this.logIncorrectInitialisationWarning();
   }

   public void displayMessenger() {
      this.logIncorrectInitialisationWarning();
   }

   public int getUnreadConversationCount() {
      this.logIncorrectInitialisationWarning();
      return 0;
   }

   public void handlePushMessage() {
      this.logIncorrectInitialisationWarning();
   }

   public void handlePushMessage(TaskStackBuilder var1) {
      this.logIncorrectInitialisationWarning();
   }

   public void hideMessenger() {
      this.logIncorrectInitialisationWarning();
   }

   public void logEvent(String var1) {
      this.logIncorrectInitialisationWarning();
   }

   public void logEvent(String var1, Map var2) {
      this.logIncorrectInitialisationWarning();
   }

   public void registerIdentifiedUser(Registration var1) {
      this.logIncorrectInitialisationWarning();
   }

   public void registerUnidentifiedUser() {
      this.logIncorrectInitialisationWarning();
   }

   public void removeUnreadConversationCountListener(UnreadConversationCountListener var1) {
      this.logIncorrectInitialisationWarning();
   }

   public void reset() {
      this.logIncorrectInitialisationWarning();
   }

   public void setBottomPadding(int var1) {
      this.logIncorrectInitialisationWarning();
   }

   public void setInAppMessageVisibility(Intercom.Visibility var1) {
      this.logIncorrectInitialisationWarning();
   }

   public void setLauncherVisibility(Intercom.Visibility var1) {
      this.logIncorrectInitialisationWarning();
   }

   public void setUserHash(String var1) {
      this.logIncorrectInitialisationWarning();
   }

   public void updateUser(UserAttributes var1) {
      this.logIncorrectInitialisationWarning();
   }
}
