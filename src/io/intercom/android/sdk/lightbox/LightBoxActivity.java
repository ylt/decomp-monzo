package io.intercom.android.sdk.lightbox;

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Build.VERSION;
import android.support.v4.app.j;
import android.support.v4.content.a;
import android.support.v7.app.e;
import android.view.ViewGroup;
import android.widget.ImageView;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.transforms.RoundedCornersTransform;
import io.intercom.android.sdk.utilities.ImageUtils;
import io.intercom.com.bumptech.glide.c;
import io.intercom.com.bumptech.glide.f.f;
import io.intercom.com.bumptech.glide.h.i;
import io.intercom.com.bumptech.glide.load.l;
import io.intercom.com.bumptech.glide.load.resource.b.b;

public class LightBoxActivity extends e implements LightBoxListener {
   private static final int ANIMATION_TIME_MS = 300;
   private static final String CACHE_HEIGHT = "cache_height";
   private static final String CACHE_WIDTH = "cache_width";
   private static final String EXTRA_ACTIVITY_FULLSCREEN = "extra_activity_fullscreen";
   private static final String EXTRA_IMAGE_URL = "extra_image_url";
   public static final String TRANSITION_KEY = "lightbox_image";
   private String imageUrl = "";
   ViewGroup rootView;

   private void fadeIn() {
      int var1 = a.c(this, R.color.intercom_full_transparent_full_black);
      int var2 = a.c(this, R.color.intercom_transparent_black_lightbox);
      ValueAnimator var3 = ValueAnimator.ofObject(new ArgbEvaluator(), new Object[]{Integer.valueOf(var1), Integer.valueOf(var2)});
      var3.setDuration(300L);
      var3.addUpdateListener(new AnimatorUpdateListener() {
         public void onAnimationUpdate(ValueAnimator var1) {
            LightBoxActivity.this.rootView.setBackgroundColor(((Integer)var1.getAnimatedValue()).intValue());
         }
      });
      var3.start();
   }

   private void fadeOut() {
      int var1 = a.c(this, R.color.intercom_transparent_black_lightbox);
      int var2 = a.c(this, R.color.intercom_full_transparent_full_black);
      ValueAnimator var3 = ValueAnimator.ofObject(new ArgbEvaluator(), new Object[]{Integer.valueOf(var1), Integer.valueOf(var2)});
      var3.setDuration(300L);
      var3.addUpdateListener(new AnimatorUpdateListener() {
         public void onAnimationUpdate(ValueAnimator var1) {
            LightBoxActivity.this.rootView.setBackgroundColor(((Integer)var1.getAnimatedValue()).intValue());
         }
      });
      var3.start();
   }

   public static Intent imageIntent(Context var0, String var1, boolean var2, int var3, int var4) {
      return (new Intent(var0, LightBoxActivity.class)).setFlags(268435456).putExtra("extra_image_url", var1).putExtra("cache_width", var3).putExtra("cache_height", var4).putExtra("extra_activity_fullscreen", var2);
   }

   public void closeLightBox() {
      this.fadeOut();
      this.supportFinishAfterTransition();
   }

   public void onBackPressed() {
      this.closeLightBox();
   }

   protected void onCreate(Bundle var1) {
      int var2 = 0;
      Bundle var4 = this.getIntent().getExtras();
      int var3;
      if(var4 != null) {
         this.imageUrl = var4.getString("extra_image_url", "");
         var3 = var4.getInt("cache_width");
         var2 = var4.getInt("cache_height");
         if(var4.getBoolean("extra_activity_fullscreen", false)) {
            this.requestWindowFeature(1);
            this.getWindow().setFlags(1024, 1024);
         }
      } else {
         var3 = 0;
      }

      super.onCreate(var1);
      this.setContentView(R.layout.intercom_activity_lightbox);
      this.rootView = (ViewGroup)this.findViewById(R.id.root_view);
      LightBoxImageView var6 = (LightBoxImageView)this.findViewById(R.id.full_image);
      if(VERSION.SDK_INT >= 21) {
         var6.setTransitionName("lightbox_image");
      }

      f var5 = (new f()).a((l)(new RoundedCornersTransform(this.getResources().getDimensionPixelSize(R.dimen.intercom_image_rounded_corners)))).b(R.drawable.intercom_error).b(ImageUtils.getDiskCacheStrategy(this.imageUrl));
      if(i.a(var3, var2)) {
         var5 = var5.a(var3, var2);
      }

      c.a((j)this).a((Object)this.imageUrl).a((io.intercom.com.bumptech.glide.j)b.c()).a(var5).a((ImageView)var6);
      var6.setLightBoxListener(this);
      this.fadeIn();
   }
}
