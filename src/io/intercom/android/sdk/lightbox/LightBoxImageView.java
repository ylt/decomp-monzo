package io.intercom.android.sdk.lightbox;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.View.OnTouchListener;

public class LightBoxImageView extends AppCompatImageView {
   GestureDetector gestureDetector;
   LightBoxListener lightBoxListener;

   public LightBoxImageView(Context var1) {
      super(var1);
      this.init();
   }

   public LightBoxImageView(Context var1, AttributeSet var2) {
      super(var1, var2);
      this.init();
   }

   private void init() {
      this.gestureDetector = new GestureDetector(this.getContext(), new LightBoxImageView.GestureListener());
      this.setOnTouchListener(new OnTouchListener() {
         public boolean onTouch(View var1, MotionEvent var2) {
            return LightBoxImageView.this.gestureDetector.onTouchEvent(var2);
         }
      });
   }

   void setLightBoxListener(LightBoxListener var1) {
      this.lightBoxListener = var1;
   }

   private class GestureListener extends SimpleOnGestureListener {
      public boolean onDown(MotionEvent var1) {
         return true;
      }

      public boolean onSingleTapConfirmed(MotionEvent var1) {
         if(LightBoxImageView.this.lightBoxListener != null) {
            LightBoxImageView.this.lightBoxListener.closeLightBox();
         }

         return super.onSingleTapConfirmed(var1);
      }
   }
}
