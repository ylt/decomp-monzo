package io.intercom.android.sdk.lightbox;

public interface LightBoxListener {
   void closeLightBox();
}
