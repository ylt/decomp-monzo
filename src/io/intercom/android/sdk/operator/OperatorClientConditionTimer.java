package io.intercom.android.sdk.operator;

import android.os.Handler;
import io.intercom.android.sdk.views.ClientConditionListener;

public class OperatorClientConditionTimer {
   private static final int LONG_DELAY_MILLIS = 10000;
   private static final int SHORT_DELAY_MILLIS = 2000;
   private final String condition;
   private final String conversationId;
   private final Handler handler;
   private final Runnable runnable;

   public OperatorClientConditionTimer(final String var1, final String var2, Handler var3, final ClientConditionListener var4) {
      this.conversationId = var1;
      this.condition = var2;
      this.handler = var3;
      this.runnable = new Runnable() {
         public void run() {
            var4.conditionSatisfied(var1, var2);
         }
      };
   }

   public void beginCountdown() {
      this.handler.postDelayed(this.runnable, 2000L);
   }

   public String getCondition() {
      return this.condition;
   }

   public String getConversationId() {
      return this.conversationId;
   }

   public void interrupt() {
      this.handler.removeCallbacks(this.runnable);
      this.handler.postDelayed(this.runnable, 10000L);
   }
}
