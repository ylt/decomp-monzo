package io.intercom.android.sdk;

import io.intercom.android.sdk.actions.Actions;
import io.intercom.android.sdk.conversation.events.AdminIsTypingEvent;
import io.intercom.android.sdk.models.events.realtime.NewCommentEvent;
import io.intercom.android.sdk.models.events.realtime.UserContentSeenByAdminEvent;
import io.intercom.android.sdk.nexus.NexusClient;
import io.intercom.android.sdk.nexus.NexusConfig;
import io.intercom.android.sdk.nexus.NexusEvent;
import io.intercom.android.sdk.nexus.NexusListener;
import io.intercom.android.sdk.store.Store;
import io.intercom.android.sdk.twig.Twig;
import io.intercom.com.a.a.b;
import java.util.Collections;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

class NexusWrapper extends NexusClient implements NexusListener {
   private static final String ADMIN_AVATAR = "adminAvatar";
   private static final String ADMIN_ID = "adminId";
   private static final String ADMIN_NAME = "adminName";
   private static final String CONVERSATION_ID = "conversationId";
   private ScheduledFuture actionFuture;
   private final b bus;
   private final long debouncePeriodMs;
   private final ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
   private final Store store;
   private final Twig twig;

   NexusWrapper(Twig var1, b var2, Store var3, long var4) {
      super(var1);
      this.twig = var1;
      this.bus = var2;
      this.store = var3;
      this.debouncePeriodMs = var4;
   }

   private void logKnownEvent(NexusEvent var1) {
      this.twig.internal("Nexus", "Received " + var1.getEventType() + " event");
   }

   private void removeCallbacks() {
      if(this.actionFuture != null) {
         this.actionFuture.cancel(false);
      }

   }

   public void connect(final NexusConfig var1, final boolean var2) {
      if(var1.getEndpoints().isEmpty()) {
         this.twig.w("No realtime endpoints present so we can't connect", new Object[0]);
      } else {
         this.removeCallbacks();
         this.actionFuture = this.executor.schedule(new Runnable() {
            public void run() {
               NexusWrapper.this.connectNow(var1, var2);
            }
         }, this.debouncePeriodMs, TimeUnit.MILLISECONDS);
      }

   }

   void connectNow(NexusConfig var1, boolean var2) {
      if(!this.isConnected()) {
         super.connect(var1, var2);
         this.setTopics(Collections.singletonList("*"));
         this.addEventListener(this);
      }

   }

   public void disconnect() {
      this.removeCallbacks();
      this.actionFuture = this.executor.schedule(new Runnable() {
         public void run() {
            NexusWrapper.this.disconnectNow();
         }
      }, this.debouncePeriodMs, TimeUnit.MILLISECONDS);
   }

   void disconnectNow() {
      this.removeEventListener(this);
      super.disconnect();
   }

   public void notifyEvent(NexusEvent var1) {
      String var4 = var1.getEventData().optString("conversationId");
      switch(null.$SwitchMap$io$intercom$android$nexus$NexusEventType[var1.getEventType().ordinal()]) {
      case 1:
         this.logKnownEvent(var1);
         String var3 = var1.getEventData().optString("adminId");
         String var2 = var1.getEventData().optString("adminName");
         String var5 = var1.getEventData().optString("adminAvatar");
         this.bus.post(new AdminIsTypingEvent(var3, var4, var2, var5));
         break;
      case 2:
         this.logKnownEvent(var1);
         this.bus.post(new NewCommentEvent(var4));
         this.store.dispatch(Actions.newCommentEventReceived(var4));
         break;
      case 3:
         this.logKnownEvent(var1);
         this.bus.post(new UserContentSeenByAdminEvent(var4));
         break;
      case 4:
         this.logKnownEvent(var1);
         this.store.dispatch(Actions.conversationMarkedAsRead(var4));
         break;
      default:
         this.twig.internal("Nexus", "Unexpected event: " + var1.getEventType());
      }

   }

   public void onConnect() {
   }

   public void onConnectFailed() {
   }

   public void onShutdown() {
   }
}
