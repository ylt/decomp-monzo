package io.intercom.android.sdk.conversation.events;

public class AdminIsTypingEvent {
   private final String adminAvatarUrl;
   private final String adminId;
   private final String adminName;
   private final String conversationId;

   public AdminIsTypingEvent(String var1, String var2, String var3, String var4) {
      this.adminId = var1;
      this.conversationId = var2;
      this.adminName = var3;
      this.adminAvatarUrl = var4;
   }

   public String getAdminAvatarUrl() {
      return this.adminAvatarUrl;
   }

   public String getAdminId() {
      return this.adminId;
   }

   public String getAdminName() {
      return this.adminName;
   }

   public String getConversationId() {
      return this.conversationId;
   }
}
