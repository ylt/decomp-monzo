package io.intercom.android.sdk.conversation;

import android.webkit.WebView;
import java.util.ArrayList;
import java.util.Collection;

public class JavascriptRunner {
   private final Collection actionsAfterLoad;
   private boolean hasLoaded;
   private final WebView webView;

   public JavascriptRunner(WebView var1) {
      this(var1, new ArrayList());
   }

   JavascriptRunner(WebView var1, Collection var2) {
      this.hasLoaded = false;
      this.webView = var1;
      this.actionsAfterLoad = var2;
   }

   public void clearPendingScripts() {
      synchronized(this){}

      try {
         this.actionsAfterLoad.clear();
      } finally {
         ;
      }

   }

   void reset() {
      synchronized(this){}

      try {
         this.hasLoaded = false;
         this.clearPendingScripts();
      } finally {
         ;
      }

   }

   public void run(final String var1) {
      synchronized(this){}

      try {
         Runnable var2 = new Runnable() {
            public void run() {
               JavascriptRunner.this.webView.loadUrl("javascript:" + var1);
            }
         };
         if(this.hasLoaded) {
            this.webView.post(var2);
         } else {
            this.actionsAfterLoad.add(var2);
         }
      } finally {
         ;
      }

   }

   public void runPendingScripts() {
      // $FF: Couldn't be decompiled
   }
}
