package io.intercom.android.sdk.conversation;

import io.intercom.android.sdk.models.Conversation;
import io.intercom.android.sdk.models.Part;
import java.util.Iterator;

class ProfileExpansionLogic {
   private static boolean hasNoUserReplies(Conversation var0) {
      boolean var3 = false;
      Iterator var4 = var0.getParts().iterator();
      boolean var2 = false;

      while(true) {
         if(!var4.hasNext()) {
            var3 = true;
            break;
         }

         Part var5 = (Part)var4.next();
         boolean var1 = var2;
         if(var5.isAdmin()) {
            var1 = true;
         }

         var2 = var1;
         if(var1) {
            var2 = var1;
            if(!var5.isAdmin()) {
               break;
            }
         }
      }

      return var3;
   }

   private static boolean hasOnlyUserParts(Conversation var0) {
      Iterator var2 = var0.getParts().iterator();

      boolean var1;
      while(true) {
         if(var2.hasNext()) {
            if(!((Part)var2.next()).isAdmin()) {
               continue;
            }

            var1 = false;
            break;
         }

         var1 = true;
         break;
      }

      return var1;
   }

   boolean shouldExpandProfile(Conversation var1) {
      boolean var2;
      if(!hasOnlyUserParts(var1) && !hasNoUserReplies(var1)) {
         var2 = false;
      } else {
         var2 = true;
      }

      return var2;
   }
}
