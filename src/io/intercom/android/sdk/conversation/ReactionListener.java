package io.intercom.android.sdk.conversation;

import io.intercom.android.sdk.models.Reaction;

public interface ReactionListener {
   void onReactionSelected(Reaction var1);
}
