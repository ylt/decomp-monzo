package io.intercom.android.sdk.conversation;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnClickListener;
import android.content.res.Configuration;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.d.a;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import com.intercom.input.gallery.GalleryLightBoxActivity;
import com.intercom.input.gallery.c;
import io.intercom.android.sdk.Injector;
import io.intercom.android.sdk.Provider;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.actions.Actions;
import io.intercom.android.sdk.activities.ConversationReactionListener;
import io.intercom.android.sdk.activities.IntercomPostActivity;
import io.intercom.android.sdk.api.Api;
import io.intercom.android.sdk.blocks.BlockFactory;
import io.intercom.android.sdk.blocks.BlockType;
import io.intercom.android.sdk.blocks.Blocks;
import io.intercom.android.sdk.blocks.logic.TextSplittingStrategy;
import io.intercom.android.sdk.blocks.models.Block;
import io.intercom.android.sdk.blocks.models.BlockAttachment;
import io.intercom.android.sdk.conversation.composer.ComposerPresenter;
import io.intercom.android.sdk.conversation.composer.galleryinput.LocalGalleryLightBoxFragment;
import io.intercom.android.sdk.conversation.events.AdminIsTypingEvent;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.android.sdk.identity.AppIdentity;
import io.intercom.android.sdk.identity.UserIdentity;
import io.intercom.android.sdk.imageloader.WallpaperLoader;
import io.intercom.android.sdk.logger.LumberMill;
import io.intercom.android.sdk.metrics.MetricTracker;
import io.intercom.android.sdk.metrics.ops.OpsMetricTracker;
import io.intercom.android.sdk.models.Conversation;
import io.intercom.android.sdk.models.LastParticipatingAdmin;
import io.intercom.android.sdk.models.Part;
import io.intercom.android.sdk.models.Participant;
import io.intercom.android.sdk.models.ReactionReply;
import io.intercom.android.sdk.models.TeamPresence;
import io.intercom.android.sdk.models.events.ConversationEvent;
import io.intercom.android.sdk.models.events.ReplyEvent;
import io.intercom.android.sdk.models.events.failure.ConversationFailedEvent;
import io.intercom.android.sdk.models.events.realtime.NewCommentEvent;
import io.intercom.android.sdk.nexus.NexusClient;
import io.intercom.android.sdk.nexus.NexusEvent;
import io.intercom.android.sdk.profile.ProfilePresenter;
import io.intercom.android.sdk.state.State;
import io.intercom.android.sdk.store.Selectors;
import io.intercom.android.sdk.store.Store;
import io.intercom.android.sdk.twig.Twig;
import io.intercom.android.sdk.utilities.BackgroundUtils;
import io.intercom.android.sdk.utilities.ContextLocaliser;
import io.intercom.android.sdk.utilities.Phrase;
import io.intercom.android.sdk.utilities.StoreUtils;
import io.intercom.android.sdk.utilities.TimeFormatter;
import io.intercom.android.sdk.utilities.ViewUtils;
import io.intercom.android.sdk.utilities.connectivity.NetworkConnectivityMonitor;
import io.intercom.android.sdk.views.IntercomErrorView;
import io.intercom.android.sdk.views.IntercomLinkView;
import io.intercom.android.sdk.views.IntercomToolbar;
import io.intercom.com.a.a.b;
import io.intercom.com.a.a.h;
import io.intercom.com.bumptech.glide.i;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class ConversationFragment extends Fragment implements ConversationContentPresenter.Host, ConversationPartAdapter.Listener, Store.Subscriber2, IntercomToolbar.Listener {
   private static final String ARG_CONVERSATION_ID = "conversationId";
   private static final String ARG_GROUP_PARTICIPANTS = "group_participants";
   private static final String ARG_INITIAL_MESSAGE = "initial_message";
   private static final String ARG_IS_READ = "intercomsdk-isRead";
   private static final String ARG_IS_TWO_PANE = "is_two_pane";
   private static final String ARG_LAST_PARTICIPANT = "last_participant";
   private static final int COMPOSER_LIGHTBOX_REQUEST_CODE = 25;
   private static final String EXTRA_GALLERY_IMAGE = "gallery_image";
   private static final String IMAGE_MIME_TYPE = "image";
   private static final int MAX_FILE_SIZE_BYTES = 41943040;
   Provider appConfigProvider;
   private b bus;
   private boolean canOpenProfile = true;
   private FrameLayout composerHolder;
   private final ComposerPresenter.Listener composerListener = new ComposerPresenter.Listener() {
      private final BlockFactory blockFactory = new BlockFactory(new TextSplittingStrategy());

      private List createBlocks(c var1) {
         ArrayList var2 = new ArrayList();
         if(var1.c().contains("image")) {
            var2.add((new Block.Builder()).withUrl(var1.a()).withType(BlockType.LOCALIMAGE.name()).withWidth(var1.f()).withHeight(var1.g()));
         } else {
            BlockAttachment var3 = (new BlockAttachment.Builder()).withName(var1.b()).withUrl(var1.a()).withContentType(var1.c()).build();
            var2.add((new Block.Builder()).withAttachments(Collections.singletonList(var3)).withType(BlockType.LOCAL_ATTACHMENT.name()));
         }

         return var2;
      }

      private void showUploadError() {
         (new a(ConversationFragment.this.getActivity())).a(R.string.intercom_failed_to_send).b(R.string.intercom_file_too_big).a(17039370, new OnClickListener() {
            public void onClick(DialogInterface var1, int var2) {
            }
         }).c();
      }

      public void onRemoteImageSelected(c var1) {
         ConversationFragment.this.contentPresenter.sendPart(Collections.singletonList((new Block.Builder()).withType("image").withUrl(var1.a()).withAttribution(var1.e()).withHeight(var1.g()).withWidth(var1.f())));
         ConversationFragment.this.composerPresenter.returnToDefaultInput();
      }

      public void onSendButtonPressed(CharSequence var1) {
         String var2 = var1.toString().trim();
         if(!var2.isEmpty()) {
            ConversationFragment.this.opsMetricTracker.trackEvent("start", "time-to-process-action-send-part-ms");
            ConversationFragment.this.contentPresenter.sendPart(this.blockFactory.getBlocksForText(var2));
         }

      }

      public void onUploadImageSelected(c var1) {
         if(var1.h() > 41943040) {
            this.showUploadError();
         } else {
            ConversationFragment.this.contentPresenter.uploadImage(this.createBlocks(var1), var1);
         }

      }
   };
   ComposerPresenter composerPresenter;
   private final NetworkConnectivityMonitor.ConnectivityEventListener connectivityEventListener = new NetworkConnectivityMonitor.ConnectivityEventListener() {
      public void onDisconnect() {
         Snackbar.a(ConversationFragment.this.rootView, R.string.intercom_no_network_connection, -2).c();
      }

      public void onReconnect() {
         Snackbar.a(ConversationFragment.this.rootView, R.string.intercom_connected, 0).e(((AppConfig)ConversationFragment.this.appConfigProvider.get()).getBaseColor()).a(R.string.intercom_dismiss, new android.view.View.OnClickListener() {
            public void onClick(View var1) {
            }
         }).c();
      }
   };
   ConversationContentPresenter contentPresenter;
   Conversation conversation;
   String conversationId;
   private int currentOrientation;
   private final OnGlobalLayoutListener globalLayoutListener = new OnGlobalLayoutListener() {
      private int previousHeight;

      public void onGlobalLayout() {
         int var1 = ConversationFragment.this.rootView.getHeight();
         if(this.previousHeight != 0 && !ConversationFragment.this.orientationChanged && this.previousHeight > var1) {
            ConversationFragment.this.profilePresenter.closeProfile();
            if(ConversationFragment.this.shouldStayAtBottom) {
               ConversationFragment.this.contentPresenter.scrollToBottom();
               ConversationFragment.this.shouldStayAtBottom = false;
            }
         }

         if(ConversationFragment.this.orientationChanged && this.previousHeight != var1) {
            ConversationFragment.this.orientationChanged = false;
         }

         this.previousHeight = var1;
         ConversationFragment.this.contentPresenter.onGlobalLayout();
      }
   };
   private List groupParticipants;
   private boolean hasLoadedConversation = false;
   private String initialMessage = "";
   private IntercomErrorView intercomErrorView;
   private IntercomToolbar intercomToolbar;
   private boolean isTwoPane = false;
   private CharSequence lastActiveTime;
   private LastParticipatingAdmin lastParticipant;
   private ConversationFragment.Listener listener;
   private MetricTracker metricTracker;
   final NetworkConnectivityMonitor networkConnectivityMonitor = new NetworkConnectivityMonitor();
   private NexusClient nexusClient;
   private OpsMetricTracker opsMetricTracker;
   boolean orientationChanged = false;
   private final ProfileExpansionLogic profileExpansionLogic = new ProfileExpansionLogic();
   ProfilePresenter profilePresenter;
   private ReactionInputView reactionComposer;
   private i requestManager;
   View rootView;
   boolean shouldStayAtBottom = false;
   SoundPlayer soundPlayer;
   private Store store;
   private Store.Subscription subscription;
   private final Twig twig = LumberMill.getLogger();
   private UserIdentity userIdentity;
   private WallpaperLoader wallpaperLoader;

   private void configureInputView(Conversation var1) {
      Part var2 = var1.getLastPart();
      if(ReactionReply.isNull(var2.getReactionReply())) {
         this.composerPresenter.showComposer();
         this.composerPresenter.requestFocus();
         this.reactionComposer.setVisibility(8);
      } else {
         ConversationReactionListener var3 = new ConversationReactionListener(MetricTracker.ReactionLocation.CONVERSATION, var2.getId(), var1.getId(), Injector.get().getApi(), this.metricTracker);
         this.reactionComposer.setUpReactions(var2.getReactionReply(), true, var3, this.requestManager);
         this.composerPresenter.hideComposer();
      }

   }

   private static c createGalleryImageForUri(Uri var0) {
      String var1 = var0.getPath();
      Options var2 = new Options();
      var2.inJustDecodeBounds = true;
      BitmapFactory.decodeFile(var1, var2);
      return (new c.a()).c(var1).a(var0.getLastPathSegment()).d(var1).c((int)(new File(var1)).length()).b(var2.outHeight).a(var2.outWidth).b(var2.outMimeType).a();
   }

   private ConversationContentPresenter createNativeContentPresenter(View var1) {
      Injector var3 = Injector.get();
      IntercomLinkView var4 = (IntercomLinkView)var1.findViewById(R.id.intercom_link);
      TextView var2 = (TextView)var1.findViewById(R.id.pill);
      RecyclerView var6 = (RecyclerView)var1.findViewById(R.id.conversation_list);
      Blocks var5 = new Blocks(this.getActivity(), LumberMill.getBlocksTwig());
      Api var9 = var3.getApi();
      ArrayList var8 = new ArrayList();
      ContextLocaliser var7 = new ContextLocaliser(this.appConfigProvider);
      return NativeConversationContentPresenter.create(this, var6, ConversationPartAdapter.create(this.getActivity(), var8, this, var9, this.conversationId, this.appConfigProvider, this.userIdentity, var3.getTimeProvider(), var7, this.requestManager), var4, this.appConfigProvider, var9, var8, this.conversationId, var2, this.soundPlayer, var5, this.userIdentity, this.opsMetricTracker, this.requestManager);
   }

   private ConversationContentPresenter createWebViewContentPresenter(View var1) {
      ViewGroup var2 = (ViewGroup)var1.findViewById(R.id.conversation_web_view_layout);
      var2.setVisibility(0);
      IntercomLinkView var7 = (IntercomLinkView)var1.findViewById(R.id.intercom_link);
      Injector var9 = Injector.get();
      Api var8 = var9.getApi();
      ArrayList var3 = new ArrayList();
      ObservableWebView var4 = new ObservableWebView(this.getActivity());
      var2.addView(var4, new LayoutParams(-1, -1));
      AppIdentity var6 = var9.getAppIdentity();
      View var5 = LayoutInflater.from(var2.getContext()).inflate(R.layout.intercom_row_loading, var2, false);
      var5.setVisibility(8);
      var2.addView(var5);
      return WebViewConversationContentPresenter.create(var4, var5, var7, var6, this.userIdentity, this.appConfigProvider, var8, var3, this.conversationId, this, var9.getGson(), new JavascriptRunner(var4), this.store, this.bus, this.opsMetricTracker, this.profilePresenter);
   }

   private void displayErrorView() {
      this.intercomErrorView.setVisibility(0);
      this.composerHolder.setVisibility(8);
      this.contentPresenter.showErrorView();
   }

   public static ConversationFragment newInstance(String var0, LastParticipatingAdmin var1, boolean var2, boolean var3, String var4, List var5) {
      ConversationFragment var7 = new ConversationFragment();
      Bundle var6 = new Bundle();
      var6.putString("conversationId", var0);
      var6.putString("initial_message", var4);
      var6.putParcelable("last_participant", var1);
      var6.putParcelableArrayList("group_participants", new ArrayList(var5));
      var6.putBoolean("intercomsdk-isRead", var2);
      var6.putBoolean("is_two_pane", var3);
      var6.setClassLoader(Part.class.getClassLoader());
      var7.setArguments(var6);
      return var7;
   }

   private void toggleProfile() {
      if(this.profilePresenter.isExpanded()) {
         this.profilePresenter.closeProfile();
      } else {
         this.profilePresenter.profileClicked();
         this.contentPresenter.smoothScrollToTop();
      }

   }

   private void trackLastPart(Part var1) {
      if(var1.isLinkList()) {
         this.metricTracker.receivedOperatorReply(this.conversationId);
      } else {
         this.metricTracker.receivedReply(var1.hasAttachments(), var1.isLinkCard(), var1.getId(), this.conversationId);
      }

   }

   private void updateLastActiveTime() {
      if(this.conversation.getLastParticipatingAdmin() != null && this.getActivity() != null) {
         this.lastActiveTime = (new TimeFormatter(this.getActivity(), Injector.get().getTimeProvider())).getAdminActiveStatus(this.lastParticipant, this.appConfigProvider);
      }

   }

   private void updateProfileToolbar(TeamPresence var1) {
      if(!LastParticipatingAdmin.isNull(this.lastParticipant) && this.lastParticipant != LastParticipatingAdmin.NONE) {
         this.updateLastActiveTime();
         this.profilePresenter.setTeammatePresence(this.lastParticipant, this.groupParticipants, this.lastActiveTime, this.rootView.getWidth());
      } else {
         int var2 = this.getActivity().getWindow().getDecorView().getMeasuredWidth();
         this.profilePresenter.setTeamPresence(var1, var2, this.requestManager);
      }

   }

   @h
   public void adminIsTyping(AdminIsTypingEvent var1) {
      if(this.conversationId.equals(var1.getConversationId())) {
         this.contentPresenter.onAdminStartedTyping(var1);
      }

   }

   @h
   public void conversationFailure(ConversationFailedEvent var1) {
      if(this.isAdded()) {
         this.displayErrorView();
      }

   }

   @h
   public void conversationSuccess(ConversationEvent var1) {
      this.composerHolder.setVisibility(0);
      int var3 = this.conversation.getParts().size();
      List var5 = var1.getResponse().getParts();
      if(var1.getResponse().getId().equals(this.conversationId) && var5.size() > var3) {
         this.conversation = var1.getResponse();
         if(this.isAdded()) {
            this.contentPresenter.onConversationFetched(var1);
            this.displayConversation();
         }

         boolean var2;
         if(!this.conversation.isRead()) {
            var2 = true;
         } else {
            var2 = false;
         }

         if(var2) {
            Injector.get().getApi().markConversationAsRead(this.conversationId);
            this.store.dispatch(Actions.conversationMarkedAsRead(this.conversationId));
            this.nexusClient.fire(NexusEvent.getConversationSeenEvent(this.conversationId, this.userIdentity.getIntercomId()));
         }

         if(var3 == 0) {
            Part var6 = (Part)var5.get(var5.size() - 1);
            if(var6.isReply() && var2) {
               this.trackLastPart(var6);
            }

            if(this.canOpenProfile) {
               View var7 = this.getView();
               if(var7 != null) {
                  final boolean var4 = this.profileExpansionLogic.shouldExpandProfile(this.conversation);
                  var7.postDelayed(new Runnable() {
                     public void run() {
                        if(ConversationFragment.this.getContext() != null) {
                           if(var4) {
                              ConversationFragment.this.profilePresenter.profileAutoOpened();
                              ConversationFragment.this.profilePresenter.startOffsetListener();
                              ConversationFragment.this.contentPresenter.smoothScrollToTop();
                           } else {
                              ConversationFragment.this.profilePresenter.startOffsetListener();
                           }
                        }

                     }
                  }, 50L);
                  if(var4) {
                     this.contentPresenter.scrollToTop();
                  } else {
                     this.contentPresenter.scrollToBottom();
                  }

                  this.canOpenProfile = false;
               }
            }
         } else {
            var5 = var5.subList(var3, var5.size());
            Iterator var8 = var5.iterator();

            while(var8.hasNext()) {
               if(((Part)var8.next()).isAdmin()) {
                  this.soundPlayer.playMessageReceivedSound();
                  break;
               }
            }

            this.contentPresenter.onNewPartReceived();
            this.trackLastPart((Part)var5.get(var5.size() - 1));
         }
      }

      this.lastParticipant = this.conversation.getLastParticipatingAdmin();
      this.groupParticipants = this.conversation.getGroupConversationParticipants();
      if(this.lastParticipant != null) {
         this.updateLastActiveTime();
      }

      this.updateProfileToolbar((TeamPresence)this.store.select(Selectors.TEAM_PRESENCE));
      this.hasLoadedConversation = true;
   }

   void displayConversation() {
      if(this.isAdded()) {
         this.intercomErrorView.setVisibility(8);
         this.contentPresenter.showContentView();
         this.configureInputView(this.conversation);
      }

   }

   void displayLoadingView() {
      if(this.isAdded()) {
         this.intercomErrorView.setVisibility(8);
         this.composerPresenter.hideComposer();
         this.contentPresenter.showLoadingView();
      }

   }

   public Conversation getConversation() {
      return this.conversation;
   }

   public String getConversationId() {
      return this.conversationId;
   }

   public void handleOnBackPressed() {
      this.composerPresenter.onBackPressed();
   }

   boolean hasNotLoadedLastAdminForExistingConversation() {
      boolean var1;
      if(!this.hasLoadedConversation && !this.conversationId.isEmpty() && LastParticipatingAdmin.isNull(this.lastParticipant)) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   @h
   public void newComment(NewCommentEvent var1) {
      if(this.conversationId.equals(var1.getConversationId())) {
         this.contentPresenter.onNewCommentEventReceived(var1);
      }

   }

   public void onActivityResult(int var1, int var2, Intent var3) {
      switch(var1) {
      case 25:
         if(var3 != null && var3.hasExtra("gallery_image")) {
            c var4 = (c)var3.getParcelableExtra("gallery_image");
            this.composerListener.onUploadImageSelected(var4);
         }
      default:
      }
   }

   public void onAttach(Context var1) {
      super.onAttach(var1);

      try {
         this.listener = (ConversationFragment.Listener)var1;
      } catch (ClassCastException var3) {
         throw new ClassCastException(var1 + " must implement ConversationFragment.Listener");
      }
   }

   public void onCloseClicked() {
      this.listener.onToolbarCloseClicked();
   }

   public void onConfigurationChanged(Configuration var1) {
      super.onConfigurationChanged(var1);
      if(var1.orientation != this.currentOrientation) {
         this.orientationChanged = true;
         this.composerPresenter.updateMaxLines();
      }

      this.currentOrientation = var1.orientation;
   }

   public void onConversationCreated(Conversation var1, boolean var2) {
      this.conversation = var1;
      this.conversationId = var1.getId();
      this.profilePresenter.setConversationId(this.conversationId);
      this.composerPresenter.setConversationId(this.conversationId);
      this.composerPresenter.setHint(R.string.intercom_reply_to_conversation);
      this.nexusClient.fire(NexusEvent.getNewCommentEvent(this.conversationId, this.userIdentity.getIntercomId()));
      Part var3 = (Part)var1.getParts().get(0);
      this.metricTracker.sentInNewConversation(var3.hasAttachments(), var2, var3.isGifOnlyPart(), var3.getId(), this.conversationId, ((TeamPresence)this.store.select(Selectors.TEAM_PRESENCE)).getOfficeHours().isEmpty());
      this.metricTracker.startConversation(this.conversationId);
      this.hasLoadedConversation = true;
   }

   public void onConversationUpdated(Conversation var1) {
      this.conversationSuccess(new ConversationEvent(var1));
   }

   public void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.requestManager = io.intercom.com.bumptech.glide.c.a((Fragment)this);
      Injector var3 = Injector.get();
      this.bus = var3.getBus();
      this.nexusClient = var3.getNexusClient();
      this.appConfigProvider = var3.getAppConfigProvider();
      this.userIdentity = var3.getUserIdentity();
      this.store = var3.getStore();
      Uri var2 = ((State)this.store.state()).lastScreenshot();
      if(var2 != Uri.EMPTY) {
         this.store.dispatch(Actions.screenshotLightboxOpened());
         c var4 = createGalleryImageForUri(var2);
         this.startActivityForResult(GalleryLightBoxActivity.a(this.getActivity(), var4, LocalGalleryLightBoxFragment.class), 25);
      }

      this.metricTracker = var3.getMetricTracker();
      this.opsMetricTracker = var3.getOpsMetricTracker();
      this.opsMetricTracker.trackEvent("start", "time-to-process-action-load-conversation-ms");
      var1 = this.getArguments();
      if(var1 != null) {
         var1.setClassLoader(Part.class.getClassLoader());
         this.conversationId = var1.getString("conversationId", "");
         this.initialMessage = var1.getString("initial_message", "");
         this.lastParticipant = (LastParticipatingAdmin)var1.getParcelable("last_participant");
         if(this.lastParticipant == null) {
            this.lastParticipant = LastParticipatingAdmin.NULL;
         }

         this.groupParticipants = var1.getParcelableArrayList("group_participants");
         if(this.groupParticipants == null) {
            this.groupParticipants = new ArrayList();
         }

         if(!var1.getBoolean("intercomsdk-isRead", false)) {
            this.store.dispatch(Actions.conversationMarkedAsRead(this.conversationId));
         }

         this.isTwoPane = var1.getBoolean("is_two_pane", false);
         if(TextUtils.isEmpty(this.conversationId)) {
            this.store.dispatch(Actions.composerOpened());
         } else {
            this.store.dispatch(Actions.conversationOpened(this.conversationId));
         }
      }

      this.conversation = new Conversation();
      this.soundPlayer = new SoundPlayer(this.getActivity(), this.appConfigProvider);
      this.currentOrientation = this.getResources().getConfiguration().orientation;
   }

   public View onCreateView(LayoutInflater var1, ViewGroup var2, Bundle var3) {
      if(this.rootView == null) {
         this.rootView = var1.inflate(R.layout.intercom_fragment_conversation, var2, false);
      } else {
         ViewGroup var5 = (ViewGroup)this.rootView.getParent();
         if(var5 != null) {
            var5.removeView(this.rootView);
         }
      }

      this.composerHolder = (FrameLayout)this.rootView.findViewById(R.id.composer_holder);
      this.profilePresenter = new ProfilePresenter((CoordinatorLayout)this.rootView.findViewById(R.id.conversation_coordinator_layout), this.metricTracker, this.appConfigProvider, this.requestManager);
      this.profilePresenter.setConversationId(this.conversationId);
      this.intercomErrorView = (IntercomErrorView)this.rootView.findViewById(R.id.error_layout_conversation);
      this.intercomErrorView.setActionButtonTextColor(((AppConfig)this.appConfigProvider.get()).getBaseColor());
      this.intercomErrorView.setActionButtonClickListener(new android.view.View.OnClickListener() {
         public void onClick(View var1) {
            ConversationFragment.this.displayLoadingView();
            ConversationFragment.this.contentPresenter.fetchConversation(ConversationFragment.this.conversationId);
         }
      });
      this.profilePresenter.addListener(new android.support.design.widget.AppBarLayout.b() {
         public void onOffsetChanged(AppBarLayout var1, int var2) {
            ConversationFragment.this.contentPresenter.onProfileScrolled();
         }
      });
      ImageView var6 = (ImageView)this.rootView.findViewById(R.id.wallpaper);
      this.wallpaperLoader = WallpaperLoader.create(this.getContext(), this.appConfigProvider, this.requestManager);
      this.wallpaperLoader.loadWallpaperInto(var6, new WallpaperLoader.Listener() {
         public void onLoadComplete() {
            BackgroundUtils.setBackground(ConversationFragment.this.rootView, (Drawable)null);
         }
      });
      this.reactionComposer = (ReactionInputView)this.rootView.findViewById(R.id.reaction_input_view);
      this.intercomToolbar = (IntercomToolbar)this.rootView.findViewById(R.id.intercom_toolbar);
      this.intercomToolbar.setListener(this);
      IntercomToolbar var7 = this.intercomToolbar;
      byte var4;
      if(this.isTwoPane) {
         var4 = 8;
      } else {
         var4 = 0;
      }

      var7.setInboxButtonVisibility(var4);
      this.listener.setStatusBarColor();
      this.rootView.getViewTreeObserver().addOnGlobalLayoutListener(this.globalLayoutListener);
      return this.rootView;
   }

   public void onDestroy() {
      this.store.dispatch(Actions.conversationClosed());
      super.onDestroy();
   }

   public void onDestroyView() {
      ViewUtils.removeGlobalLayoutListener(this.rootView, this.globalLayoutListener);
      this.composerPresenter.cleanup();
      this.contentPresenter.cleanup();

      try {
         this.wallpaperLoader.close();
      } catch (IOException var2) {
         this.twig.d("Couldn't close LongTermImageLoader: " + var2.getMessage(), new Object[0]);
      }

      super.onDestroyView();
   }

   public void onInboxClicked() {
      this.composerPresenter.cleanup();
      this.listener.onBackToInboxClicked();
      this.intercomToolbar.setInboxButtonVisibility(8);
      CharSequence var1 = Phrase.from(this.intercomToolbar.getContext(), R.string.intercom_conversations_with_app).put("name", ((AppConfig)this.appConfigProvider.get()).getName()).format();
      this.intercomToolbar.setTitle(var1);
      this.intercomToolbar.setSubtitle("");
   }

   public void onPartClicked(Part var1) {
      this.contentPresenter.onPartClicked(var1);
   }

   public void onPostCardClicked() {
      List var1 = this.conversation.getParts();
      if(!var1.isEmpty() && ((Part)var1.get(0)).getMessageStyle() == "post") {
         this.onPostCardClicked((Part)var1.get(0));
      }

   }

   public void onPostCardClicked(Part var1) {
      this.startActivity(IntercomPostActivity.buildPostIntent(this.getContext(), var1, "", this.lastParticipant, false));
   }

   public void onStart() {
      super.onStart();
      this.networkConnectivityMonitor.startListening(this.getActivity());
      this.networkConnectivityMonitor.setListener(this.connectivityEventListener);
      this.subscription = this.store.subscribeToChanges(Selectors.UNREAD_COUNT, Selectors.TEAM_PRESENCE, this);
      this.bus.register(this);
      this.bus.register(this.contentPresenter);
      this.contentPresenter.fetchConversation(this.conversationId);
   }

   public void onStateChange(Integer var1, TeamPresence var2) {
      this.intercomToolbar.setUnreadCount(var1);
      if(this.hasNotLoadedLastAdminForExistingConversation()) {
         this.profilePresenter.setUnknownPresence();
      } else {
         this.updateProfileToolbar(var2);
      }

   }

   public void onStop() {
      StoreUtils.safeUnsubscribe(this.subscription);
      this.bus.unregister(this);
      this.bus.unregister(this.contentPresenter);
      this.networkConnectivityMonitor.setListener((NetworkConnectivityMonitor.ConnectivityEventListener)null);
      this.networkConnectivityMonitor.stopListening(this.getActivity());
      this.opsMetricTracker.clear();
      this.profilePresenter.onStop();
      super.onStop();
   }

   public void onToolbarClicked() {
      this.toggleProfile();
   }

   public void onViewCreated(View var1, Bundle var2) {
      super.onViewCreated(var1, var2);
      if(((AppConfig)this.appConfigProvider.get()).hasFeature("conversation-web-view")) {
         try {
            this.contentPresenter = this.createWebViewContentPresenter(var1);
         } catch (Exception var3) {
            this.contentPresenter = this.createNativeContentPresenter(var1);
         }
      } else {
         this.contentPresenter = this.createNativeContentPresenter(var1);
      }

      FrameLayout var4 = (FrameLayout)this.rootView.findViewById(R.id.composer_container);
      var4.setOnClickListener(new android.view.View.OnClickListener() {
         public void onClick(View var1) {
            if(ConversationFragment.this.contentPresenter.isAtBottom()) {
               ConversationFragment.this.shouldStayAtBottom = true;
            }

         }
      });
      this.composerPresenter = new ComposerPresenter(this.composerListener, var4, this.getChildFragmentManager(), LayoutInflater.from(var1.getContext()), this.nexusClient, this.userIdentity, this.appConfigProvider, this.metricTracker, this.conversationId, this.initialMessage, this.store, this.getActivity(), ((AppConfig)this.appConfigProvider.get()).getBaseColor());
      this.contentPresenter.setup();
      this.composerPresenter.requestFocus();
      this.configureInputView(this.conversation);
      if(this.conversationId.isEmpty()) {
         this.composerHolder.setVisibility(0);
         this.displayConversation();
      } else {
         this.composerPresenter.setConversationId(this.conversationId);
         this.displayLoadingView();
      }

      if(this.conversationId.isEmpty()) {
         this.canOpenProfile = false;
         var1.postDelayed(new Runnable() {
            public void run() {
               if(ConversationFragment.this.getActivity() != null) {
                  ConversationFragment.this.profilePresenter.profileAutoOpened();
                  ConversationFragment.this.profilePresenter.startOffsetListener();
                  ConversationFragment.this.contentPresenter.smoothScrollToTop();
               }

            }
         }, 50L);
      }

   }

   @h
   public void replySuccess(ReplyEvent var1) {
      if(var1.getConversationId().equals(this.conversationId)) {
         this.nexusClient.fire(NexusEvent.getNewCommentEvent(this.conversationId, this.userIdentity.getIntercomId()));
         Part var4 = var1.getResponse();
         Participant var3 = this.conversation.getParticipant(var4.getParticipantId());
         Participant var2 = var3;
         if(var3 == Participant.NULL) {
            var2 = (new Participant.Builder()).withId(var4.getParticipantId()).build();
            this.conversation.getParticipants().put(var4.getParticipantId(), var2);
         }

         var4.setParticipant(var2);
         this.contentPresenter.onReplyDelivered(var1);
         TimeFormatter var5 = new TimeFormatter(this.getActivity(), Injector.get().getTimeProvider());
         this.metricTracker.sentInConversation(var4.hasAttachments(), var1.isAnnotated(), var4.isGifOnlyPart(), var4.getId(), this.conversationId, ((TeamPresence)this.store.select(Selectors.TEAM_PRESENCE)).getOfficeHours().isEmpty(), this.lastParticipant.isActive(), var5.getLastActiveMinutes(this.lastParticipant.getLastActiveAt()));
      }

   }

   public void sdkWindowFinishedAnimating() {
      if(this.getView() != null && this.profileExpansionLogic.shouldExpandProfile(this.conversation)) {
         this.profilePresenter.profileAutoOpened();
         this.contentPresenter.smoothScrollToTop();
      }

   }

   public boolean shouldHandleOnBackPressed() {
      boolean var1;
      if(this.composerPresenter != null && this.composerPresenter.isOpen()) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public interface Listener {
      void onBackToInboxClicked();

      void onToolbarCloseClicked();

      void setStatusBarColor();
   }
}
