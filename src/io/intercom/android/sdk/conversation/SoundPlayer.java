package io.intercom.android.sdk.conversation;

import android.content.Context;
import android.content.res.Resources.NotFoundException;
import android.media.SoundPool;
import android.media.SoundPool.OnLoadCompleteListener;
import io.intercom.android.sdk.Provider;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.android.sdk.logger.LumberMill;
import io.intercom.android.sdk.twig.Twig;

public class SoundPlayer {
   static final int NOT_FOUND_ID = -1;
   private static final Twig TWIG = LumberMill.getLogger();
   private final Provider appConfigProvider;
   final int messageReceivedId;
   boolean messageReceivedLoaded;
   final int replyDeliveredId;
   boolean replyDeliveredLoaded;
   final int replyFailedId;
   boolean replyFailedLoaded;
   final int replySentId;
   boolean replySentLoaded;
   private final SoundPool soundPool;

   public SoundPlayer(Context var1, Provider var2) {
      this(var1, var2, new SoundPool(1, 5, 0));
   }

   SoundPlayer(Context var1, Provider var2, SoundPool var3) {
      this.appConfigProvider = var2;
      this.soundPool = var3;
      this.soundPool.setOnLoadCompleteListener(new OnLoadCompleteListener() {
         public void onLoadComplete(SoundPool var1, int var2, int var3) {
            if(var3 == 0) {
               if(var2 == SoundPlayer.this.messageReceivedId) {
                  SoundPlayer.this.messageReceivedLoaded = true;
               } else if(var2 == SoundPlayer.this.replyFailedId) {
                  SoundPlayer.this.replyFailedLoaded = true;
               } else if(var2 == SoundPlayer.this.replySentId) {
                  SoundPlayer.this.replySentLoaded = true;
               } else if(var2 == SoundPlayer.this.replyDeliveredId) {
                  SoundPlayer.this.replyDeliveredLoaded = true;
               }
            }

         }
      });
      this.messageReceivedId = this.loadSound(var1, R.raw.intercom_birdy_done_1);
      this.replyFailedId = this.loadSound(var1, R.raw.intercom_wood_done_1);
      this.replySentId = this.loadSound(var1, R.raw.intercom_wood_done_2);
      this.replyDeliveredId = this.loadSound(var1, R.raw.intercom_wood_done_3);
   }

   private int loadSound(Context var1, int var2) {
      try {
         var2 = this.soundPool.load(var1, var2, 1);
      } catch (NotFoundException var3) {
         TWIG.e(var3, "Could not play sound", new Object[0]);
         var2 = -1;
      }

      return var2;
   }

   void playIfLoaded(boolean var1, int var2) {
      if(var1 && var2 != -1 && ((AppConfig)this.appConfigProvider.get()).isAudioEnabled()) {
         this.soundPool.play(var2, 1.0F, 1.0F, 1, 0, 1.0F);
      }

   }

   public void playMessageReceivedSound() {
      this.playIfLoaded(this.messageReceivedLoaded, this.messageReceivedId);
   }

   public void playReplyDeliveredSound() {
      this.playIfLoaded(this.replyDeliveredLoaded, this.replyDeliveredId);
   }

   public void playReplyFailedSound() {
      this.playIfLoaded(this.replyFailedLoaded, this.replyFailedId);
   }

   public void playReplySentSound() {
      this.playIfLoaded(this.replySentLoaded, this.replySentId);
   }
}
