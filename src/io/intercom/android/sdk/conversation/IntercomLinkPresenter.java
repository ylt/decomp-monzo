package io.intercom.android.sdk.conversation;

import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import io.intercom.android.sdk.Provider;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.api.Api;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.android.sdk.models.Part;
import io.intercom.android.sdk.views.IntercomLinkView;
import java.util.Iterator;
import java.util.List;

class IntercomLinkPresenter {
   private final Api api;
   private final Provider appConfigProvider;
   private final String conversationId;
   private final List conversationParts;
   final IntercomLinkView intercomLinkView;
   private boolean wasAtBottom = false;

   IntercomLinkPresenter(IntercomLinkView var1, Provider var2, Api var3, List var4, String var5) {
      this.intercomLinkView = var1;
      this.appConfigProvider = var2;
      this.api = var3;
      this.conversationParts = var4;
      this.conversationId = var5;
   }

   private Part getFirstMessagePart() {
      Iterator var2 = this.conversationParts.iterator();

      Part var1;
      do {
         if(!var2.hasNext()) {
            var1 = Part.NULL;
            break;
         }

         var1 = (Part)var2.next();
      } while(!var1.isMessagePart());

      return var1;
   }

   private boolean shouldShowIntercomLink() {
      return ((AppConfig)this.appConfigProvider.get()).shouldShowIntercomLink();
   }

   void onProfileScrolled(View var1) {
      if(this.shouldShowIntercomLink() && var1 != null) {
         this.intercomLinkView.hideIfIntersectedOrShow(var1);
      }

   }

   void setup(IntercomLinkPresenter.IntercomLinkHost var1) {
      this.intercomLinkView.setAlpha(0.0F);
      if(this.shouldShowIntercomLink()) {
         var1.addBottomPadding(this.intercomLinkView.getResources().getDimensionPixelSize(R.dimen.intercom_link_height));
         this.intercomLinkView.setOnTouchListener(new OnTouchListener() {
            public boolean onTouch(View var1, MotionEvent var2) {
               boolean var3;
               if(IntercomLinkPresenter.this.intercomLinkView.getAlpha() == 1.0F) {
                  var3 = true;
               } else {
                  var3 = false;
               }

               if(var3 && 1 == var2.getActionMasked()) {
                  IntercomLinkPresenter.this.intercomLinkView.followIntercomLink(IntercomLinkPresenter.this.appConfigProvider, IntercomLinkPresenter.this.getFirstMessagePart(), IntercomLinkPresenter.this.api);
               }

               return var3;
            }
         });
         if(this.conversationId.isEmpty()) {
            this.intercomLinkView.show();
         }
      }

   }

   void updateIntercomLink(IntercomLinkPresenter.IntercomLinkHost var1) {
      if(this.shouldShowIntercomLink()) {
         boolean var2 = var1.isAtBottom();
         if(var2 && !this.wasAtBottom) {
            this.intercomLinkView.show();
         } else if(this.wasAtBottom && !var2) {
            this.intercomLinkView.hide();
         }

         this.wasAtBottom = var1.isAtBottom();
      }

   }

   interface IntercomLinkHost {
      void addBottomPadding(int var1);

      boolean isAtBottom();
   }
}
