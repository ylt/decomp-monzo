package io.intercom.android.sdk.conversation;

import android.content.Context;
import android.webkit.WebView;

public class ObservableWebView extends WebView {
   private ObservableWebView.OnScrollChangeListener onScrollChangeListener;

   public ObservableWebView(Context var1) {
      super(var1);
   }

   protected void onScrollChanged(int var1, int var2, int var3, int var4) {
      super.onScrollChanged(var1, var2, var3, var4);
      if(this.onScrollChangeListener != null) {
         this.onScrollChangeListener.onScrollChange(this, var1, var2, var3, var4);
      }

   }

   public void setOnScrollChangeListener(ObservableWebView.OnScrollChangeListener var1) {
      this.onScrollChangeListener = var1;
   }

   public interface OnScrollChangeListener {
      void onScrollChange(WebView var1, int var2, int var3, int var4, int var5);
   }
}
