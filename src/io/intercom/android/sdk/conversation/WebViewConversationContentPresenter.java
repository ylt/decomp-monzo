package io.intercom.android.sdk.conversation;

import android.annotation.SuppressLint;
import android.graphics.PorterDuff.Mode;
import android.view.View;
import android.webkit.WebView;
import android.widget.ProgressBar;
import com.intercom.input.gallery.c;
import io.intercom.android.sdk.Provider;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.api.Api;
import io.intercom.android.sdk.conversation.events.AdminIsTypingEvent;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.android.sdk.identity.AppIdentity;
import io.intercom.android.sdk.identity.UserIdentity;
import io.intercom.android.sdk.metrics.ops.OpsMetricTracker;
import io.intercom.android.sdk.models.Part;
import io.intercom.android.sdk.models.events.ConversationEvent;
import io.intercom.android.sdk.models.events.NewConversationEvent;
import io.intercom.android.sdk.models.events.ReplyEvent;
import io.intercom.android.sdk.models.events.realtime.NewCommentEvent;
import io.intercom.android.sdk.profile.ProfilePresenter;
import io.intercom.android.sdk.store.Store;
import io.intercom.android.sdk.views.IntercomLinkView;
import io.intercom.com.a.a.b;
import io.intercom.com.google.gson.e;
import java.util.List;

class WebViewConversationContentPresenter implements ConversationContentPresenter, IntercomLinkPresenter.IntercomLinkHost {
   private static final int DIRECTION_DOWN = 1;
   private static final String WEB_PAGE_URL = "https://js.intercomcdn.com/mobile.html";
   private final Provider appConfigProvider;
   private final AppIdentity appIdentity;
   private final b bus;
   private boolean fetchedConversation = false;
   private final e gson;
   private final ConversationContentPresenter.Host host;
   private WebViewHostWrapper hostWrapper;
   private final IntercomLinkPresenter intercomLinkPresenter;
   private final JavascriptRunner jsRunner;
   private final View loadingView;
   private final OpsMetricTracker opsMetricTracker;
   private final List parts;
   private final ProfilePresenter profilePresenter;
   private final Store store;
   private final UserIdentity userIdentity;
   private final ObservableWebView webView;

   WebViewConversationContentPresenter(ObservableWebView var1, View var2, IntercomLinkPresenter var3, AppIdentity var4, UserIdentity var5, Provider var6, List var7, ConversationContentPresenter.Host var8, e var9, JavascriptRunner var10, Store var11, b var12, OpsMetricTracker var13, ProfilePresenter var14) {
      this.webView = var1;
      this.appIdentity = var4;
      this.userIdentity = var5;
      this.appConfigProvider = var6;
      this.host = var8;
      this.gson = var9;
      this.jsRunner = var10;
      this.loadingView = var2;
      this.store = var11;
      this.bus = var12;
      this.intercomLinkPresenter = var3;
      this.parts = var7;
      this.opsMetricTracker = var13;
      this.profilePresenter = var14;
   }

   static WebViewConversationContentPresenter create(ObservableWebView var0, View var1, IntercomLinkView var2, AppIdentity var3, UserIdentity var4, Provider var5, Api var6, List var7, String var8, ConversationContentPresenter.Host var9, e var10, JavascriptRunner var11, Store var12, b var13, OpsMetricTracker var14, ProfilePresenter var15) {
      return new WebViewConversationContentPresenter(var0, var1, new IntercomLinkPresenter(var2, var5, var6, var7, var8), var3, var4, var5, var7, var9, var10, var11, var12, var13, var14, var15);
   }

   private void loadBundle() {
      this.jsRunner.reset();
      this.jsRunner.run("conversationApp.sendAction = function(action) {AndroidHost.handleAction(JSON.stringify(action));};");
      this.webView.loadUrl("https://js.intercomcdn.com/mobile.html");
   }

   public void addBottomPadding(int var1) {
   }

   public void cleanup() {
      this.jsRunner.clearPendingScripts();
   }

   public void fetchConversation(String var1) {
      if(!this.fetchedConversation) {
         this.fetchedConversation = true;
         this.loadBundle();
         this.sendWebViewAction(WebViewConversationAction.setConversation(this.appIdentity, this.userIdentity, this.appConfigProvider, var1));
      }

   }

   public boolean isAtBottom() {
      boolean var1 = true;
      if(this.webView.canScrollVertically(1)) {
         var1 = false;
      }

      return var1;
   }

   public void onAdminStartedTyping(AdminIsTypingEvent var1) {
      this.sendWebViewAction(WebViewConversationAction.adminIsTyping(var1.getAdminId(), var1.getAdminName(), var1.getAdminAvatarUrl()));
   }

   public void onConversationFetched(ConversationEvent var1) {
   }

   public void onGlobalLayout() {
      this.intercomLinkPresenter.updateIntercomLink(this);
   }

   public void onNewCommentEventReceived(NewCommentEvent var1) {
      this.sendWebViewAction(WebViewConversationAction.partCreated());
   }

   public void onNewConversation(NewConversationEvent var1) {
   }

   public void onNewPartReceived() {
   }

   public void onPartClicked(Part var1) {
   }

   public void onProfileScrolled() {
      if(this.profilePresenter.isExpanded()) {
         this.scrollToTop();
      }

      this.intercomLinkPresenter.updateIntercomLink(this);
   }

   public void onReplyDelivered(ReplyEvent var1) {
   }

   public void scrollToBottom() {
      this.webView.scrollTo(0, (int)((float)this.webView.getContentHeight() * this.webView.getScale()));
   }

   public void scrollToTop() {
      this.webView.scrollTo(0, 0);
   }

   public void sendPart(List var1) {
      this.sendWebViewAction(WebViewConversationAction.createPart(var1));
   }

   void sendWebViewAction(WebViewConversationAction var1) {
      String var2 = this.gson.a((Object)var1);
      this.jsRunner.run("conversationApp.handleAction(" + var2 + ");");
   }

   @SuppressLint({"SetJavaScriptEnabled"})
   public void setup() {
      this.webView.setOnScrollChangeListener(new ObservableWebView.OnScrollChangeListener() {
         public void onScrollChange(WebView var1, int var2, int var3, int var4, int var5) {
            if(var3 > 0 && WebViewConversationContentPresenter.this.profilePresenter.isExpanded()) {
               WebViewConversationContentPresenter.this.profilePresenter.closeProfile();
            }

            WebViewConversationContentPresenter.this.intercomLinkPresenter.updateIntercomLink(WebViewConversationContentPresenter.this);
         }
      });
      AppConfig var2 = (AppConfig)this.appConfigProvider.get();
      ProgressBar var1 = (ProgressBar)this.loadingView.findViewById(R.id.progressBar);
      if(var1 != null) {
         var1.getIndeterminateDrawable().setColorFilter(var2.getBaseColor(), Mode.SRC_IN);
      }

      this.webView.getSettings().setJavaScriptEnabled(true);
      this.webView.setWebViewClient(new ConversationWebViewClient(this.jsRunner, "https://js.intercomcdn.com/mobile.html", this.bus));
      this.hostWrapper = new WebViewHostWrapper(this.host, this.webView, this.gson, this.store, this.parts, this.opsMetricTracker);
      this.webView.addJavascriptInterface(this.hostWrapper, "AndroidHost");
      this.webView.setBackgroundColor(0);
      this.intercomLinkPresenter.setup(this);
   }

   public void showContentView() {
      this.webView.setVisibility(0);
      this.loadingView.setVisibility(8);
   }

   public void showErrorView() {
      this.loadingView.setVisibility(8);
      this.webView.setVisibility(4);
      this.fetchedConversation = false;
   }

   public void showLoadingView() {
      this.loadingView.setVisibility(0);
      this.webView.setVisibility(4);
   }

   public void smoothScrollToTop() {
      this.scrollToTop();
   }

   public void uploadImage(List var1, c var2) {
      this.sendWebViewAction(WebViewConversationAction.createUpload(var2.b(), var2.a(), var2.c(), var2.f(), var2.g(), var2.i()));
   }
}
