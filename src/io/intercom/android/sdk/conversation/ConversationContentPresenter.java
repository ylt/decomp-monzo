package io.intercom.android.sdk.conversation;

import com.intercom.input.gallery.c;
import io.intercom.android.sdk.conversation.events.AdminIsTypingEvent;
import io.intercom.android.sdk.models.Conversation;
import io.intercom.android.sdk.models.Part;
import io.intercom.android.sdk.models.events.ConversationEvent;
import io.intercom.android.sdk.models.events.NewConversationEvent;
import io.intercom.android.sdk.models.events.ReplyEvent;
import io.intercom.android.sdk.models.events.realtime.NewCommentEvent;
import java.util.List;

interface ConversationContentPresenter {
   void cleanup();

   void fetchConversation(String var1);

   boolean isAtBottom();

   void onAdminStartedTyping(AdminIsTypingEvent var1);

   void onConversationFetched(ConversationEvent var1);

   void onGlobalLayout();

   void onNewCommentEventReceived(NewCommentEvent var1);

   void onNewConversation(NewConversationEvent var1);

   void onNewPartReceived();

   void onPartClicked(Part var1);

   void onProfileScrolled();

   void onReplyDelivered(ReplyEvent var1);

   void scrollToBottom();

   void scrollToTop();

   void sendPart(List var1);

   void setup();

   void showContentView();

   void showErrorView();

   void showLoadingView();

   void smoothScrollToTop();

   void uploadImage(List var1, c var2);

   public interface Host {
      Conversation getConversation();

      String getConversationId();

      void onConversationCreated(Conversation var1, boolean var2);

      void onConversationUpdated(Conversation var1);

      void onPostCardClicked();
   }
}
