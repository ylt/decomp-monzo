package io.intercom.android.sdk.conversation;

import android.annotation.TargetApi;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import io.intercom.android.sdk.models.events.failure.ConversationFailedEvent;
import io.intercom.com.a.a.b;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

class ConversationWebViewClient extends WebViewClient {
   public static final String UPLOAD_FILE_PATH = "filePath";
   public static final String UPLOAD_MIME_TYPE = "mimeType";
   private final b bus;
   private final JavascriptRunner jsRunner;
   private final Uri webPageUri;

   ConversationWebViewClient(JavascriptRunner var1, String var2, b var3) {
      this.jsRunner = var1;
      this.webPageUri = Uri.parse(var2);
      this.bus = var3;
   }

   private boolean isRequestForFavicon(String var1) {
      return var1.contains("/favicon.ico");
   }

   public void onPageFinished(WebView var1, String var2) {
      this.jsRunner.runPendingScripts();
   }

   public void onReceivedError(WebView var1, int var2, String var3, String var4) {
      if(!this.isRequestForFavicon(var4) && Uri.parse(var4).getHost().equals(this.webPageUri.getHost())) {
         this.bus.post(new ConversationFailedEvent());
         this.jsRunner.clearPendingScripts();
      }

   }

   @TargetApi(23)
   public void onReceivedError(WebView var1, WebResourceRequest var2, WebResourceError var3) {
      this.onReceivedError(var1, var3.getErrorCode(), var3.getDescription().toString(), var2.getUrl().toString());
   }

   @TargetApi(21)
   public WebResourceResponse shouldInterceptRequest(WebView var1, WebResourceRequest var2) {
      return this.shouldInterceptRequest(var1, var2.getUrl().toString());
   }

   public WebResourceResponse shouldInterceptRequest(WebView var1, String var2) {
      WebResourceResponse var7;
      if(this.isRequestForFavicon(var2)) {
         var7 = new WebResourceResponse((String)null, (String)null, (InputStream)null);
      } else {
         Uri var8 = Uri.parse(var2);
         if(var8.getHost().equals(this.webPageUri.getHost()) && !TextUtils.isEmpty(var8.getQueryParameter("filePath")) && !TextUtils.isEmpty(var8.getQueryParameter("mimeType"))) {
            try {
               String var9 = var8.getQueryParameter("filePath");
               String var3 = var8.getQueryParameter("mimeType");
               File var10 = new File(var9);
               FileInputStream var4 = new FileInputStream(var10);
               BufferedInputStream var5 = new BufferedInputStream(var4);
               var7 = new WebResourceResponse(var3, "UTF-8", var5);
            } catch (FileNotFoundException var6) {
               var7 = new WebResourceResponse("text/plain", "UTF-8", (InputStream)null);
            }
         } else {
            var7 = null;
         }
      }

      return var7;
   }

   @TargetApi(21)
   public boolean shouldOverrideUrlLoading(WebView var1, WebResourceRequest var2) {
      return this.shouldOverrideUrlLoading(var1, var2.getUrl().toString());
   }

   public boolean shouldOverrideUrlLoading(WebView var1, String var2) {
      boolean var3;
      if(!TextUtils.isEmpty(var2) && !this.webPageUri.toString().equals(var2)) {
         var1.getContext().startActivity(new Intent("android.intent.action.VIEW", Uri.parse(var2)));
         var3 = true;
      } else {
         var3 = false;
      }

      return var3;
   }
}
