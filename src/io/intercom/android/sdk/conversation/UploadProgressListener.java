package io.intercom.android.sdk.conversation;

public interface UploadProgressListener {
   void uploadNotice(byte var1);

   void uploadStarted();

   void uploadStopped();
}
