package io.intercom.android.sdk.conversation;

import android.content.Context;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import io.intercom.android.sdk.actions.Actions;
import io.intercom.android.sdk.activities.IntercomArticleActivity;
import io.intercom.android.sdk.lightbox.LightBoxActivity;
import io.intercom.android.sdk.metrics.ops.OpsMetricTracker;
import io.intercom.android.sdk.models.Conversation;
import io.intercom.android.sdk.store.Store;
import io.intercom.com.google.gson.e;
import java.util.List;

public class WebViewHostWrapper {
   private static final String ARTICLE_CARD_CLICKED = "ARTICLE_CARD_CLICKED";
   private static final String CONVERSATION_CHANGED = "CONVERSATION_CHANGED";
   private static final String IMAGE_CLICKED = "IMAGE_CLICKED";
   private static final String OPS_METRIC_EVENT = "OPS_METRIC_EVENT";
   private static final String POST_CARD_CLICKED = "POST_CARD_CLICKED";
   private final e gson;
   private final ConversationContentPresenter.Host host;
   private final OpsMetricTracker opsMetricTracker;
   private final List parts;
   private final Store store;
   private final WebView webView;

   WebViewHostWrapper(ConversationContentPresenter.Host var1, WebView var2, e var3, Store var4, List var5, OpsMetricTracker var6) {
      this.host = var1;
      this.webView = var2;
      this.gson = var3;
      this.store = var4;
      this.parts = var5;
      this.opsMetricTracker = var6;
   }

   @JavascriptInterface
   public void handleAction(String var1) {
      final WebViewConversationAction var4 = (WebViewConversationAction)this.gson.a(var1, WebViewConversationAction.class);
      String var3 = var4.getType();
      byte var2 = -1;
      switch(var3.hashCode()) {
      case -1547946056:
         if(var3.equals("OPS_METRIC_EVENT")) {
            var2 = 4;
         }
         break;
      case -1466318175:
         if(var3.equals("ARTICLE_CARD_CLICKED")) {
            var2 = 2;
         }
         break;
      case -1017968381:
         if(var3.equals("IMAGE_CLICKED")) {
            var2 = 1;
         }
         break;
      case -509335848:
         if(var3.equals("CONVERSATION_CHANGED")) {
            var2 = 0;
         }
         break;
      case -259325161:
         if(var3.equals("POST_CARD_CLICKED")) {
            var2 = 3;
         }
      }

      switch(var2) {
      case 0:
         var1 = this.gson.a(var4.getValue().get("conversation"));
         final Conversation var5 = ((Conversation.Builder)this.gson.a(var1, Conversation.Builder.class)).build();
         this.parts.clear();
         this.parts.addAll(var5.getParts());
         this.webView.post(new Runnable() {
            public void run() {
               if(!WebViewHostWrapper.this.host.getConversationId().isEmpty()) {
                  WebViewHostWrapper.this.store.dispatch(Actions.fetchConversationSuccess(var5));
               }

               if(WebViewHostWrapper.this.host.getConversationId().isEmpty() && !var5.getParts().isEmpty()) {
                  WebViewHostWrapper.this.store.dispatch(Actions.newConversationSuccess(var5));
                  WebViewHostWrapper.this.host.onConversationCreated(var5, false);
               } else {
                  WebViewHostWrapper.this.host.onConversationUpdated(var5);
               }

            }
         });
         break;
      case 1:
         final Context var6 = this.webView.getContext();
         this.webView.post(new Runnable() {
            public void run() {
               String var1 = (String)var4.getValue().get("src");
               if(var1 != null) {
                  var6.startActivity(LightBoxActivity.imageIntent(var6, var1, false, 0, 0));
               }

            }
         });
         break;
      case 2:
         this.webView.post(new Runnable() {
            public void run() {
               String var1 = (String)var4.getValue().get("articleId");
               if(var1 != null) {
                  Context var2 = WebViewHostWrapper.this.webView.getContext();
                  var2.startActivity(IntercomArticleActivity.buildIntent(var2, var1, WebViewHostWrapper.this.host.getConversationId()));
               }

            }
         });
         break;
      case 3:
         this.webView.post(new Runnable() {
            public void run() {
               WebViewHostWrapper.this.host.onPostCardClicked();
            }
         });
         break;
      case 4:
         this.webView.post(new Runnable() {
            public void run() {
               String var1 = (String)var4.getValue().get("eventType");
               String var2 = (String)var4.getValue().get("event");
               WebViewHostWrapper.this.opsMetricTracker.trackEvent(var1, var2);
            }
         });
      }

   }
}
