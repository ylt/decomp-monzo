package io.intercom.android.sdk.conversation;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.ColorFilter;
import android.graphics.Rect;
import android.os.Vibrator;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.animation.OvershootInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.commons.utilities.DeviceUtils;
import io.intercom.android.sdk.commons.utilities.ScreenUtils;
import io.intercom.android.sdk.imageloader.LongTermImageLoader;
import io.intercom.android.sdk.logger.LumberMill;
import io.intercom.android.sdk.models.Reaction;
import io.intercom.android.sdk.models.ReactionReply;
import io.intercom.android.sdk.twig.Twig;
import io.intercom.android.sdk.utilities.ColorUtils;
import io.intercom.com.bumptech.glide.i;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ReactionInputView extends LinearLayout {
   private static final float REACTION_SCALE = 2.5F;
   private static final int VIBRATION_DURATION_MS = 10;
   private final ColorFilter deselectedFilter;
   Integer highlightedViewIndex;
   private ReactionListener listener;
   private final LongTermImageLoader longTermImageLoader;
   private ReactionReply reactionReply;
   private final List reactionViews;
   int reactionsLoaded;
   private final OnTouchListener touchListener;
   private final Twig twig;
   private final Vibrator vibrator;

   public ReactionInputView(Context var1, AttributeSet var2) {
      this(var1, var2, 0);
   }

   public ReactionInputView(Context var1, AttributeSet var2, int var3) {
      super(var1, var2, var3);
      this.twig = LumberMill.getLogger();
      this.highlightedViewIndex = null;
      this.reactionsLoaded = 0;
      this.touchListener = new OnTouchListener() {
         private final Rect touchRect = new Rect();

         private void handleTouchMove(MotionEvent var1) {
            ReactionInputView.this.getHitRect(this.touchRect);
            if(this.touchRect.contains((int)var1.getX(), (int)var1.getY())) {
               for(int var2 = 0; var2 < ReactionInputView.this.getChildCount(); ++var2) {
                  ReactionInputView.this.getChildAt(var2).getHitRect(this.touchRect);
                  if(this.touchRect.contains((int)var1.getX(), (int)var1.getY())) {
                     if(ReactionInputView.this.highlightedViewIndex == null || ReactionInputView.this.highlightedViewIndex.intValue() != var2) {
                        ReactionInputView.this.vibrateForSelection();
                        ReactionInputView.this.selectViewAtIndex(2.5F, var2);
                     }

                     if(ReactionInputView.this.highlightedViewIndex == null) {
                        Integer var3 = ReactionInputView.this.getCurrentSelectedIndex();
                        if(var3 != null && var3.intValue() != var2) {
                           ReactionInputView.this.deselectViewAtIndex(var3.intValue());
                        }
                     } else if(ReactionInputView.this.highlightedViewIndex.intValue() != var2) {
                        ReactionInputView.this.deselectViewAtIndex(ReactionInputView.this.highlightedViewIndex.intValue());
                     }

                     ReactionInputView.this.highlightedViewIndex = Integer.valueOf(var2);
                  }
               }
            } else {
               if(ReactionInputView.this.highlightedViewIndex != null) {
                  ReactionInputView.this.highlightSelectedReaction();
               }

               ReactionInputView.this.highlightedViewIndex = null;
            }

         }

         public boolean onTouch(View var1, MotionEvent var2) {
            switch(var2.getActionMasked()) {
            case 0:
            case 2:
               this.handleTouchMove(var2);
               break;
            case 1:
               ReactionInputView.this.handleTouchUp();
            }

            return true;
         }
      };
      this.reactionViews = new ArrayList();
      this.setVisibility(8);
      if(this.isInEditMode()) {
         this.longTermImageLoader = null;
         this.vibrator = null;
      } else {
         this.longTermImageLoader = LongTermImageLoader.newInstance(var1);
         this.vibrator = (Vibrator)var1.getSystemService("vibrator");
      }

      this.deselectedFilter = ColorUtils.newGreyscaleFilter();
   }

   void deselectViewAtIndex(int var1) {
      ImageView var2 = (ImageView)this.reactionViews.get(var1);
      var2.animate().setInterpolator(new OvershootInterpolator(0.6F)).scaleX(1.0F).scaleY(1.0F).setDuration(200L).start();
      var2.setColorFilter(this.deselectedFilter);
   }

   Integer getCurrentSelectedIndex() {
      int var1 = 0;

      Integer var2;
      while(true) {
         if(var1 < this.getChildCount()) {
            Integer var3 = this.reactionReply.getReactionIndex();
            Reaction var4 = (Reaction)this.reactionReply.getReactionSet().get(var1);
            if(var3 != null && !var3.equals(Integer.valueOf(var4.getIndex()))) {
               ++var1;
               continue;
            }

            var2 = Integer.valueOf(var1);
            break;
         }

         var2 = null;
         break;
      }

      return var2;
   }

   void handleTouchUp() {
      if(this.highlightedViewIndex != null && this.highlightedViewIndex.intValue() >= 0 && this.highlightedViewIndex.intValue() < this.reactionReply.getReactionSet().size()) {
         Reaction var1 = (Reaction)this.reactionReply.getReactionSet().get(this.highlightedViewIndex.intValue());
         if(var1 != null && (this.reactionReply.getReactionIndex() == null || var1.getIndex() != this.reactionReply.getReactionIndex().intValue())) {
            this.reactionReply.setReactionIndex(var1.getIndex());
            if(this.listener != null) {
               this.listener.onReactionSelected(var1);
            }
         }
      }

      this.highlightSelectedReaction();
      this.highlightedViewIndex = null;
   }

   void highlightSelectedReaction() {
      for(int var1 = 0; var1 < this.getChildCount(); ++var1) {
         Integer var3 = this.reactionReply.getReactionIndex();
         Reaction var2 = (Reaction)this.reactionReply.getReactionSet().get(var1);
         if(var3 != null && !var3.equals(Integer.valueOf(var2.getIndex()))) {
            this.deselectViewAtIndex(var1);
         } else {
            this.selectViewAtIndex(1.0F, var1);
         }
      }

   }

   protected void onAttachedToWindow() {
      super.onAttachedToWindow();
      this.setOnTouchListener(this.touchListener);
   }

   protected void onDetachedFromWindow() {
      super.onDetachedFromWindow();
      this.setOnTouchListener((OnTouchListener)null);
      if(!this.isInEditMode()) {
         try {
            this.longTermImageLoader.close();
         } catch (IOException var2) {
            this.twig.d("Couldn't close LongTermImageLoader: " + var2.getMessage(), new Object[0]);
         }
      }

   }

   public void preloadReactionImages(ReactionReply var1, i var2) {
      Iterator var4 = var1.getReactionSet().iterator();

      while(var4.hasNext()) {
         Reaction var3 = (Reaction)var4.next();
         this.longTermImageLoader.loadImage(var3.getImageUrl(), (LongTermImageLoader.OnImageReadyListener)null, var2);
      }

   }

   void selectViewAtIndex(float var1, int var2) {
      ImageView var3 = (ImageView)this.reactionViews.get(var2);
      var3.animate().setInterpolator(new OvershootInterpolator(2.0F)).scaleX(var1).scaleY(var1).setDuration(200L).start();
      var3.clearColorFilter();
   }

   public void setUpReactions(ReactionReply var1, final boolean var2, ReactionListener var3, i var4) {
      final Context var8 = this.getContext();
      this.reactionReply = var1;
      this.listener = var3;
      List var13 = var1.getReactionSet();
      final int var6 = var13.size();

      for(int var5 = 0; var5 < var13.size(); ++var5) {
         FrameLayout var9 = new FrameLayout(var8);
         var9.setClipChildren(false);
         var9.setClipToPadding(false);
         var9.setLayoutParams(new LayoutParams(0, -1, 1.0F));
         final ImageView var11 = new ImageView(var8);
         int var7 = this.getResources().getDimensionPixelSize(R.dimen.intercom_reaction_size);
         var11.setLayoutParams(new android.widget.FrameLayout.LayoutParams(var7, var7, 17));
         var11.setPivotY((float)this.getResources().getDimensionPixelSize(R.dimen.intercom_reaction_offset));
         var11.setPivotX((float)(var7 / 2));
         this.reactionViews.add(var11);
         var9.addView(var11);
         Reaction var10 = (Reaction)var13.get(var5);
         this.longTermImageLoader.loadImage(var10.getImageUrl(), new LongTermImageLoader.OnImageReadyListener() {
            public void onImageReady(Bitmap var1) {
               var11.setImageBitmap(var1);
               ReactionInputView var2x = ReactionInputView.this;
               ++var2x.reactionsLoaded;
               if(ReactionInputView.this.reactionsLoaded == var6) {
                  if(var2) {
                     ReactionInputView.this.setVisibility(0);
                     ReactionInputView.this.setY((float)(ReactionInputView.this.getHeight() + ScreenUtils.dpToPx(60.0F, var8)));
                     ReactionInputView.this.animate().setInterpolator(new OvershootInterpolator(0.6F)).translationY(0.0F).setDuration(300L).start();
                  } else {
                     ReactionInputView.this.setVisibility(0);
                  }
               }

            }
         }, var4);
         Integer var12 = var1.getReactionIndex();
         if(var12 != null && !var12.equals(Integer.valueOf(var10.getIndex()))) {
            this.deselectViewAtIndex(var5);
         }

         this.addView(var9);
      }

   }

   void vibrateForSelection() {
      if(DeviceUtils.hasPermission(this.getContext(), "android.permission.VIBRATE")) {
         this.vibrator.vibrate(10L);
      }

   }
}
