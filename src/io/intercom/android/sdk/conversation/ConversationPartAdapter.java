package io.intercom.android.sdk.conversation;

import android.app.Activity;
import android.content.ClipboardManager;
import android.content.Intent;
import android.os.Build.VERSION;
import android.support.v4.app.b;
import android.support.v7.widget.RecyclerView.a;
import android.support.v7.widget.RecyclerView.w;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import io.intercom.android.sdk.Provider;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.activities.IntercomArticleActivity;
import io.intercom.android.sdk.api.Api;
import io.intercom.android.sdk.blocks.BlockType;
import io.intercom.android.sdk.blocks.models.Block;
import io.intercom.android.sdk.commons.utilities.TimeProvider;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.android.sdk.identity.UserIdentity;
import io.intercom.android.sdk.models.Part;
import io.intercom.android.sdk.models.Participant;
import io.intercom.android.sdk.utilities.ContextLocaliser;
import io.intercom.android.sdk.utilities.LinkOpener;
import io.intercom.android.sdk.utilities.TimeFormatter;
import io.intercom.android.sdk.views.PartMetadataFormatter;
import io.intercom.android.sdk.views.holder.ContainerCardViewHolder;
import io.intercom.android.sdk.views.holder.ConversationListener;
import io.intercom.android.sdk.views.holder.ConversationLoadingViewHolder;
import io.intercom.android.sdk.views.holder.ConversationPartViewHolder;
import io.intercom.android.sdk.views.holder.ConversationRatingViewHolder;
import io.intercom.android.sdk.views.holder.EventViewHolder;
import io.intercom.android.sdk.views.holder.LinkListViewHolder;
import io.intercom.android.sdk.views.holder.LinkViewHolder;
import io.intercom.android.sdk.views.holder.PartViewHolder;
import io.intercom.android.sdk.views.holder.TimeStampViewHolder;
import io.intercom.com.bumptech.glide.i;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class ConversationPartAdapter extends a implements ConversationListener {
   private boolean allowExpansionArrow = false;
   private final Api api;
   private final Provider appConfigProvider;
   private final ClipboardManager clipboardManager;
   private final String conversationId;
   private final ConversationPartAdapter.Listener listener;
   private final List partList;
   private final PartMetadataFormatter partMetadataFormatter;
   private final Map partViewMap;
   private final i requestManager;
   private final Activity rootActivity;
   private Part selectedPart;
   private final TimeFormatter timeFormatter;
   private final UserIdentity userIdentity;

   ConversationPartAdapter(List var1, ClipboardManager var2, ConversationPartAdapter.Listener var3, PartMetadataFormatter var4, TimeFormatter var5, Map var6, Api var7, Activity var8, String var9, Provider var10, UserIdentity var11, i var12) {
      this.partList = var1;
      this.clipboardManager = var2;
      this.listener = var3;
      this.partMetadataFormatter = var4;
      this.timeFormatter = var5;
      this.partViewMap = var6;
      this.api = var7;
      this.rootActivity = var8;
      this.conversationId = var9;
      this.appConfigProvider = var10;
      this.userIdentity = var11;
      this.requestManager = var12;
   }

   public static ConversationPartAdapter create(Activity var0, List var1, ConversationPartAdapter.Listener var2, Api var3, String var4, Provider var5, UserIdentity var6, TimeProvider var7, ContextLocaliser var8, i var9) {
      ClipboardManager var10 = (ClipboardManager)var0.getSystemService("clipboard");
      TimeFormatter var11 = new TimeFormatter(var8.createLocalisedContext(var0), var7);
      return new ConversationPartAdapter(var1, var10, var2, new PartMetadataFormatter(var11), var11, new HashMap(), var3, var0, var4, var5, var6, var9);
   }

   private void openArticleActivity(Block var1, Part var2, View var3) {
      Intent var4 = IntercomArticleActivity.buildIntent(this.rootActivity, var1.getArticleId(), var2.getId(), this.conversationId);
      if(VERSION.SDK_INT >= 16) {
         b var5 = b.a(this.rootActivity, var3, "link_background");
         this.rootActivity.startActivity(var4, var5.a());
      } else {
         this.rootActivity.startActivity(var4);
      }

   }

   private void startAllowingExpansionArrowIfPositionIsNotLast(int var1) {
      boolean var2 = this.allowExpansionArrow;
      boolean var3;
      if(var1 != this.partList.size() - 1) {
         var3 = true;
      } else {
         var3 = false;
      }

      this.allowExpansionArrow = var3 | var2;
   }

   public int getCount() {
      return this.getItemCount();
   }

   public int getItemCount() {
      return this.partList.size();
   }

   public int getItemViewType(int var1) {
      Part var4 = (Part)this.partList.get(var1);
      String var3 = var4.getMessageStyle();
      byte var5;
      if(var4.isEvent().booleanValue()) {
         var5 = 12;
      } else if("admin_is_typing_style".equals(var3)) {
         var5 = 5;
      } else if("day_divider_style".equals(var3)) {
         var5 = 6;
      } else if("loading_layout_style".equals(var3)) {
         var5 = 7;
      } else {
         Participant var2 = var4.getParticipant();
         if(var2.isAdmin()) {
            if(var4.isLinkCard()) {
               if(var4.isInitialMessage()) {
                  var5 = 8;
               } else {
                  var5 = 9;
               }
            } else if(var4.isSingleBlockPartOfType(BlockType.CONVERSATIONRATING)) {
               var5 = 10;
            } else if(var4.isSingleBlockPartOfType(BlockType.LINKLIST)) {
               var5 = 13;
            } else if("post".equals(var3)) {
               var5 = 2;
            } else if("note".equals(var3)) {
               this.startAllowingExpansionArrowIfPositionIsNotLast(var1);
               var5 = 3;
            } else {
               var5 = 1;
            }
         } else if(var2.isUserWithId(this.userIdentity.getIntercomId())) {
            var5 = 0;
         } else {
            var5 = 4;
         }
      }

      return var5;
   }

   public Part getPart(int var1) {
      return (Part)this.partList.get(var1);
   }

   public Part getSelectedPart() {
      return this.selectedPart;
   }

   ViewGroup getViewForPart(Part var1) {
      return (ViewGroup)this.partViewMap.get(var1);
   }

   public void onBindViewHolder(w var1, int var2) {
      if(var1 instanceof ConversationPartViewHolder) {
         Part var5 = this.getPart(var2);
         ViewGroup var4 = (ViewGroup)this.partViewMap.get(var5);
         Object var3 = var4;
         if(var4 == null) {
            var3 = new FrameLayout(var1.itemView.getContext());
         }

         ((ConversationPartViewHolder)var1).bind(var5, (ViewGroup)var3);
      }

   }

   public void onContainerCardClicked(int var1, ContainerCardViewHolder var2) {
      Part var3 = (Part)this.partList.get(var1);
      switch(this.getItemViewType(var1)) {
      case 2:
         this.listener.onPostCardClicked(var3);
         break;
      case 3:
         if(this.allowExpansionArrow) {
            var2.toggleExpanded();
         }
      }

   }

   public w onCreateViewHolder(ViewGroup var1, int var2) {
      LayoutInflater var3 = LayoutInflater.from(var1.getContext());
      Object var4;
      switch(var2) {
      case 1:
      case 4:
      case 5:
         var4 = new PartViewHolder(var3.inflate(R.layout.intercom_row_admin_part, var1, false), var2, this, this.clipboardManager, this.partMetadataFormatter, this.appConfigProvider, this.requestManager);
         break;
      case 2:
         var4 = new ContainerCardViewHolder(var3.inflate(R.layout.intercom_row_post, var1, false), var2, this, this.clipboardManager, false, this.appConfigProvider, this.requestManager);
         break;
      case 3:
         var4 = new ContainerCardViewHolder(var3.inflate(R.layout.intercom_row_note, var1, false), var2, this, this.clipboardManager, this.allowExpansionArrow, this.appConfigProvider, this.requestManager);
         break;
      case 6:
         var4 = new TimeStampViewHolder(var3.inflate(R.layout.intercom_day_divider, var1, false), this.timeFormatter);
         break;
      case 7:
         var4 = new ConversationLoadingViewHolder(var3.inflate(R.layout.intercom_row_loading, var1, false), (AppConfig)this.appConfigProvider.get());
         break;
      case 8:
         var4 = new LinkViewHolder(var3.inflate(R.layout.intercom_row_link, var1, false), var2, this, this.clipboardManager, this.appConfigProvider, this.requestManager);
         break;
      case 9:
         var4 = new LinkViewHolder(var3.inflate(R.layout.intercom_row_link_reply, var1, false), var2, this, this.clipboardManager, this.appConfigProvider, this.requestManager);
         break;
      case 10:
         var4 = new ConversationRatingViewHolder(var3.inflate(R.layout.intercom_row_conversation_rating, var1, false), this, this.appConfigProvider, this.requestManager);
         break;
      case 11:
      default:
         var4 = new PartViewHolder(var3.inflate(R.layout.intercom_row_user_part, var1, false), var2, this, this.clipboardManager, this.partMetadataFormatter, this.appConfigProvider, this.requestManager);
         break;
      case 12:
         var4 = new EventViewHolder(var3.inflate(R.layout.intercom_row_event, var1, false), this.appConfigProvider, this.requestManager);
         break;
      case 13:
         var4 = new LinkListViewHolder(var3.inflate(R.layout.intercom_row_link_list, var1, false), this, this.appConfigProvider, this.requestManager);
      }

      return (w)var4;
   }

   public void onLinkClicked(int var1, View var2) {
      Part var4 = (Part)this.partList.get(var1);
      Block var3 = var4.getLinkBlock();
      if("educate.article".equals(var3.getLinkType())) {
         this.openArticleActivity(var3, var4, var2);
      } else {
         LinkOpener.handleUrl(var3.getUrl(), this.rootActivity, this.api);
      }

   }

   public void onPartClicked(int var1, PartViewHolder var2) {
      Part var3 = (Part)this.partList.get(var1);
      if(var3.getMessageState() != Part.MessageState.FAILED && var3.getMessageState() != Part.MessageState.UPLOAD_FAILED) {
         if(var1 < this.getCount() - 1) {
            if(var3 == this.selectedPart) {
               this.selectedPart = null;
               var2.setExpanded(false);
            } else {
               this.notifyItemChanged(this.partList.indexOf(this.selectedPart));
               this.selectedPart = var3;
               var2.setExpanded(true);
            }
         }
      } else {
         this.listener.onPartClicked(var3);
      }

   }

   void setViewForPart(Part var1, ViewGroup var2) {
      this.partViewMap.put(var1, var2);
   }

   interface Listener {
      void onPartClicked(Part var1);

      void onPostCardClicked(Part var1);
   }
}
