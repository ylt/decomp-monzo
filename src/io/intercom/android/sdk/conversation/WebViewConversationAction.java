package io.intercom.android.sdk.conversation;

import android.net.Uri.Builder;
import android.text.TextUtils;
import io.intercom.android.sdk.Provider;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.android.sdk.identity.AppIdentity;
import io.intercom.android.sdk.identity.UserIdentity;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WebViewConversationAction {
   private static final String ADMIN_IS_TYPING = "ADMIN_IS_TYPING";
   private static final String CREATE_PART = "CREATE_PART";
   private static final String CREATE_UPLOAD = "CREATE_UPLOAD";
   private static final String HOST_USER_AGENT = "intercom-android-sdk/4.0.5";
   private static final String PART_CREATED = "PART_CREATED";
   private static final String SET_CONVERSATION = "SET_CONVERSATION";
   private final String type;
   private final Map value;

   public WebViewConversationAction(String var1, Map var2) {
      this.type = var1;
      this.value = var2;
   }

   public static WebViewConversationAction adminIsTyping(String var0, String var1, String var2) {
      HashMap var3 = new HashMap();
      var3.put("adminId", var0);
      var3.put("firstName", var1);
      var3.put("avatarUrl", var2);
      return new WebViewConversationAction("ADMIN_IS_TYPING", var3);
   }

   public static WebViewConversationAction createPart(List var0) {
      HashMap var1 = new HashMap();
      var1.put("body", var0);
      return new WebViewConversationAction("CREATE_PART", var1);
   }

   private static Map createSession(AppIdentity var0, UserIdentity var1) {
      HashMap var2 = new HashMap();
      var2.put("appId", var0.appId());
      var2.put("hostUserAgent", "intercom-android-sdk/4.0.5");
      if(!TextUtils.isEmpty(var1.getUserId())) {
         var2.put("userId", var1.getUserId());
      }

      if(!TextUtils.isEmpty(var1.getEmail())) {
         var2.put("email", var1.getEmail());
      }

      if(!TextUtils.isEmpty(var1.getAnonymousId())) {
         var2.put("userId", var1.getAnonymousId());
      }

      if(!TextUtils.isEmpty(var1.getHmac())) {
         var2.put("userHash", var1.getHmac());
      }

      return var2;
   }

   public static WebViewConversationAction createUpload(String var0, String var1, String var2, int var3, int var4, boolean var5) {
      Builder var6 = new Builder();
      var6.appendQueryParameter("filePath", var1);
      var6.appendQueryParameter("mimeType", var2);
      HashMap var7 = new HashMap(5);
      var7.put("mimeType", var2);
      var7.put("fileName", var0);
      var7.put("fileUrl", var6.build().toString());
      var7.put("width", Integer.valueOf(var3));
      var7.put("height", Integer.valueOf(var4));
      var7.put("isAnnotatedImage", Boolean.valueOf(var5));
      return new WebViewConversationAction("CREATE_UPLOAD", var7);
   }

   public static WebViewConversationAction partCreated() {
      return new WebViewConversationAction("PART_CREATED", Collections.emptyMap());
   }

   public static WebViewConversationAction setConversation(AppIdentity var0, UserIdentity var1, Provider var2, String var3) {
      HashMap var4 = new HashMap(5);
      var4.put("session", createSession(var0, var1));
      var4.put("conversationId", var3);
      var4.put("locale", ((AppConfig)var2.get()).getLocale());
      var4.put("appColor", String.format("#%06X", new Object[]{Integer.valueOf(((AppConfig)var2.get()).getBaseColor() & 16777215)}));
      var4.put("appName", ((AppConfig)var2.get()).getName());
      return new WebViewConversationAction("SET_CONVERSATION", var4);
   }

   public String getType() {
      return this.type;
   }

   public Map getValue() {
      return this.value;
   }
}
