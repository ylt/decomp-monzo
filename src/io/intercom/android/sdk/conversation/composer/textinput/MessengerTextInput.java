package io.intercom.android.sdk.conversation.composer.textinput;

import android.widget.EditText;
import com.intercom.composer.b.a;
import com.intercom.composer.b.c.b;
import java.util.List;

public class MessengerTextInput extends b {
   private final EditText editText;

   public MessengerTextInput(String var1, a var2, CharSequence var3, CharSequence var4, com.intercom.composer.b.c.a var5, EditText var6) {
      super(var1, var2, var5, (List)null);
      this.editText = var6;
      var6.setHint(var3);
      var6.setText(var4);
   }

   public EditText createEditText() {
      return this.editText;
   }

   public com.intercom.composer.b.a.a createFragment() {
      return new com.intercom.composer.b.a.a();
   }
}
