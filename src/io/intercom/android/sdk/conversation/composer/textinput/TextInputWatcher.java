package io.intercom.android.sdk.conversation.composer.textinput;

import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import io.intercom.android.sdk.actions.Actions;
import io.intercom.android.sdk.nexus.NexusClient;
import io.intercom.android.sdk.nexus.NexusEvent;
import io.intercom.android.sdk.store.Store;

class TextInputWatcher implements TextWatcher {
   private static final long IS_TYPING_DELAY = 1000L;
   private String conversationId = "";
   private final Handler handler;
   private final NexusClient nexusClient;
   boolean shouldSendIsTyping = true;
   private final Store store;
   private final String userId;

   TextInputWatcher(String var1, NexusClient var2, Store var3, Handler var4) {
      this.userId = var1;
      this.nexusClient = var2;
      this.store = var3;
      this.handler = var4;
   }

   public void afterTextChanged(Editable var1) {
      if(this.shouldSendIsTyping) {
         this.shouldSendIsTyping = false;
         this.handler.postDelayed(new Runnable() {
            public void run() {
               TextInputWatcher.this.shouldSendIsTyping = true;
            }
         }, 1000L);
         if(!this.conversationId.isEmpty()) {
            this.nexusClient.fire(NexusEvent.getUserIsTypingEvent(this.conversationId, this.userId));
         }

         if(var1.length() == 0) {
            this.store.dispatch(Actions.composerCleared(this.conversationId));
         } else {
            this.store.dispatch(Actions.composerTypedIn(this.conversationId));
         }
      }

   }

   public void beforeTextChanged(CharSequence var1, int var2, int var3, int var4) {
   }

   public void onTextChanged(CharSequence var1, int var2, int var3, int var4) {
   }

   protected void setConversationId(String var1) {
      this.conversationId = var1;
   }
}
