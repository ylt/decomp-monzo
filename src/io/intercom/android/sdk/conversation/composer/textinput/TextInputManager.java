package io.intercom.android.sdk.conversation.composer.textinput;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.EditText;
import com.intercom.composer.b.a;
import com.intercom.composer.b.b;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.actions.Actions;
import io.intercom.android.sdk.conversation.composer.ComposerPresenter;
import io.intercom.android.sdk.conversation.composer.InputDrawableManager;
import io.intercom.android.sdk.identity.UserIdentity;
import io.intercom.android.sdk.metrics.MetricTracker;
import io.intercom.android.sdk.metrics.MetricsComposerTextWatcher;
import io.intercom.android.sdk.nexus.NexusClient;
import io.intercom.android.sdk.store.Store;

public class TextInputManager {
   private final EditText composerText;
   private final Context context;
   private String conversationId;
   private String initialMessage;
   private final ComposerPresenter.Listener listener;
   private final MetricsComposerTextWatcher metricsTextWatcher;
   private final Store store;
   private final Drawable textIcon;
   private final TextInputWatcher textWatcher;

   public TextInputManager(Context var1, LayoutInflater var2, UserIdentity var3, NexusClient var4, MetricTracker var5, String var6, String var7, InputDrawableManager var8, Store var9, ComposerPresenter.Listener var10) {
      this.context = var1;
      this.listener = var10;
      this.conversationId = var6;
      this.initialMessage = var7;
      this.store = var9;
      this.textIcon = var8.createDrawable(var1, R.drawable.intercom_input_text);
      this.composerText = (EditText)var2.inflate(R.layout.intercom_composer_edit_text, (ViewGroup)null);
      this.textWatcher = new TextInputWatcher(var3.getIntercomId(), var4, var9, new Handler(Looper.getMainLooper()));
      this.metricsTextWatcher = new MetricsComposerTextWatcher(var5);
      this.metricsTextWatcher.setConversationId(var6);
      this.composerText.addTextChangedListener(this.textWatcher);
      this.composerText.addTextChangedListener(this.metricsTextWatcher);
   }

   public void cleanup() {
      this.store.dispatch(Actions.composerCleared(this.conversationId));
      this.composerText.removeTextChangedListener(this.textWatcher);
      this.composerText.removeTextChangedListener(this.metricsTextWatcher);
      this.composerText.clearFocus();
      this.metricsTextWatcher.reset();
   }

   public b createInput() {
      String var1;
      if(TextUtils.isEmpty(this.conversationId)) {
         var1 = this.context.getResources().getString(R.string.intercom_start_conversation);
      } else {
         var1 = this.context.getResources().getString(R.string.intercom_reply_to_conversation);
      }

      return new MessengerTextInput("text_input", new a() {
         public Drawable getIconDrawable(String var1, Context var2) {
            return TextInputManager.this.textIcon;
         }
      }, var1, this.initialMessage, new com.intercom.composer.b.c.a() {
         public void textToBeSent(b var1, CharSequence var2) {
            TextInputManager.this.listener.onSendButtonPressed(var2);
            TextInputManager.this.store.dispatch(Actions.composerSendButtonPressed());
         }
      }, this.composerText);
   }

   public void requestFocus() {
      this.composerText.requestFocus();
   }

   public void setConversationId(String var1) {
      this.textWatcher.setConversationId(var1);
      this.metricsTextWatcher.setConversationId(var1);
   }

   public void setHint(int var1) {
      this.composerText.setHint(var1);
   }

   public void updateMaxLines() {
      this.composerText.setMaxLines(this.context.getResources().getInteger(R.integer.intercom_max_composer_lines));
   }
}
