package io.intercom.android.sdk.conversation.composer;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.SOURCE)
public @interface ComposerInputIndentifier {
   String GALLERY = "gallery_input";
   String GIF = "gif_input";
   String TEXT = "text_input";
}
