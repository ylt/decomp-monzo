package io.intercom.android.sdk.conversation.composer;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.support.v4.content.a;
import io.intercom.android.sdk.R;

public class InputDrawableManager {
   private static final int[][] STATES;
   private final ColorStateList colorStateList;

   static {
      int[] var0 = new int[0];
      STATES = new int[][]{{16842919}, {16842913}, var0};
   }

   public InputDrawableManager(Context var1, int var2) {
      int var3 = a.c(var1, R.color.intercom_input_default_color);
      this.colorStateList = new ColorStateList(STATES, new int[]{var2, var2, var3});
   }

   public Drawable createDrawable(Context var1, int var2) {
      Drawable var3 = android.support.v4.a.a.a.g(a.a(var1, var2));
      android.support.v4.a.a.a.a(var3, this.colorStateList);
      return var3;
   }
}
