package io.intercom.android.sdk.conversation.composer;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Build.VERSION;
import android.support.v4.app.Fragment;
import android.support.v4.app.n;
import android.support.v4.content.a;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import com.intercom.composer.b;
import com.intercom.composer.c;
import com.intercom.composer.f;
import io.intercom.android.sdk.Provider;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.actions.Actions;
import io.intercom.android.sdk.conversation.composer.galleryinput.GalleryInputManager;
import io.intercom.android.sdk.conversation.composer.textinput.TextInputManager;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.android.sdk.identity.UserIdentity;
import io.intercom.android.sdk.metrics.MetricTracker;
import io.intercom.android.sdk.nexus.NexusClient;
import io.intercom.android.sdk.store.Store;
import java.util.ArrayList;
import java.util.List;

public class ComposerPresenter implements f {
   private static final int SELECT_INPUT_DELAY_MS = 100;
   private final b composerFragment;
   private final FrameLayout composerLayout;
   private String conversationId;
   private final Runnable defaultInputRunnable = new Runnable() {
      public void run() {
         ComposerPresenter.this.composerFragment.a("text_input", true);
      }
   };
   private final GalleryInputManager galleryInputManager;
   private final Handler handler = new Handler(Looper.getMainLooper());
   private final InputDrawableManager inputDrawableManager;
   private InputProvider inputProvider;
   private final MetricTracker metricTracker;
   private final Store store;
   private final TextInputManager textInputManager;

   public ComposerPresenter(ComposerPresenter.Listener var1, FrameLayout var2, n var3, LayoutInflater var4, NexusClient var5, UserIdentity var6, Provider var7, MetricTracker var8, String var9, String var10, Store var11, Context var12, int var13) {
      this.store = var11;
      this.composerLayout = var2;
      this.metricTracker = var8;
      this.conversationId = var9;
      this.inputDrawableManager = new InputDrawableManager(var12, ((AppConfig)var7.get()).getBaseColor());
      Fragment var16 = var3.a(b.class.getName());
      if(var16 instanceof b) {
         this.composerFragment = (b)var16;
      } else {
         this.composerFragment = b.a((String)null, false, var13);
         var3.a().b(R.id.composer_container, this.composerFragment, b.class.getName()).c();
      }

      this.composerFragment.a(new c() {
         public List getInputs() {
            return ComposerPresenter.this.inputProvider.getInputs();
         }
      });
      this.composerFragment.a((f)this);
      this.textInputManager = new TextInputManager(var12, var4, var6, var5, var8, var9, var10, this.inputDrawableManager, var11, var1);
      this.galleryInputManager = new GalleryInputManager(var12, this.inputDrawableManager, var1, var8, var9);
      boolean var15 = true;
      boolean var14 = var15;
      if(VERSION.SDK_INT >= 16) {
         var14 = var15;
         if(VERSION.SDK_INT < 23) {
            if(a.b(var12, "android.permission.READ_EXTERNAL_STORAGE") == 0) {
               var14 = true;
            } else {
               var14 = false;
            }
         }
      }

      this.setUpInputs(var3, var14);
   }

   private void addInputsToProvider(List var1) {
      this.inputProvider.addInputs(var1);
   }

   private void setUpInputs(n var1, boolean var2) {
      ArrayList var3 = new ArrayList();
      var3.add(this.textInputManager.createInput());
      var3.add(this.galleryInputManager.createGifInput());
      if(var2) {
         var3.add(this.galleryInputManager.createGalleryInput());
      }

      Fragment var4 = var1.a(InputProvider.class.getName());
      if(var4 == null) {
         this.inputProvider = new InputProvider();
         this.addInputsToProvider(var3);
         var1.a().a(this.inputProvider, InputProvider.class.getName()).c();
      } else {
         this.inputProvider = (InputProvider)var4;
         if(this.inputProvider.getInputs().isEmpty()) {
            this.addInputsToProvider(var3);
         }
      }

   }

   public void cleanup() {
      this.textInputManager.cleanup();
   }

   public void hideComposer() {
      this.composerLayout.setVisibility(8);
   }

   public boolean isOpen() {
      return this.composerFragment.d();
   }

   public void onBackPressed() {
      this.composerFragment.b();
   }

   public void onInputSelected(com.intercom.composer.b.b var1) {
      this.store.dispatch(Actions.composerInputChanged());
      this.metricTracker.clickedInput(this.conversationId, var1.getUniqueIdentifier());
   }

   public void requestFocus() {
      this.textInputManager.requestFocus();
   }

   public void returnToDefaultInput() {
      this.handler.postDelayed(this.defaultInputRunnable, 100L);
   }

   public void setConversationId(String var1) {
      this.conversationId = var1;
      this.textInputManager.setConversationId(var1);
      this.galleryInputManager.setConversationId(var1);
   }

   public void setHint(int var1) {
      this.textInputManager.setHint(var1);
   }

   public void showComposer() {
      this.composerLayout.setVisibility(0);
   }

   public void updateMaxLines() {
      this.textInputManager.updateMaxLines();
   }

   public interface Listener {
      void onRemoteImageSelected(com.intercom.input.gallery.c var1);

      void onSendButtonPressed(CharSequence var1);

      void onUploadImageSelected(com.intercom.input.gallery.c var1);
   }
}
