package io.intercom.android.sdk.conversation.composer.galleryinput;

import android.text.TextUtils;
import com.intercom.input.gallery.c;
import com.intercom.input.gallery.e;
import io.intercom.android.sdk.api.Api;
import io.intercom.android.sdk.blocks.models.Block;
import io.intercom.android.sdk.metrics.MetricTracker;
import io.intercom.android.sdk.models.GifResponse;
import io.intercom.retrofit2.Call;
import io.intercom.retrofit2.Callback;
import io.intercom.retrofit2.Response;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

class GifInputDataSource implements e {
   private static final long DEBOUNCE_DELAY_MS = 300L;
   private final Api api;
   private int currentCount = 0;
   private final Debouncer debouncer = new Debouncer();
   private String lastQuery;
   private e.a listener;
   private boolean loading;
   private final MetricTracker metricTracker;

   GifInputDataSource(Api var1, MetricTracker var2) {
      this.api = var1;
      this.metricTracker = var2;
   }

   private List getImagesFromResponse(List var1) {
      this.currentCount = var1.size();
      ArrayList var2 = new ArrayList(this.currentCount);
      Iterator var3 = var1.iterator();

      while(var3.hasNext()) {
         Block var4 = (Block)var3.next();
         var2.add((new c.a()).c(var4.getUrl()).d(var4.getPreviewUrl()).e(var4.getAttribution()).b(var4.getHeight()).a(var4.getWidth()).a());
      }

      return var2;
   }

   private void loadDefaultGifs() {
      this.api.fetchDefaultGifs(new Callback() {
         public void onFailure(Call var1, Throwable var2) {
            GifInputDataSource.this.loading = false;
            GifInputDataSource.this.listener.a();
         }

         public void onResponse(Call var1, Response var2) {
            List var3 = GifInputDataSource.this.getImagesFromResponse(((GifResponse)var2.body()).results());
            GifInputDataSource.this.loading = false;
            GifInputDataSource.this.listener.a(var3);
         }
      });
   }

   private void searchGifs(final String var1) {
      this.metricTracker.searchedGifInput(var1);
      this.debouncer.call(new Runnable() {
         public void run() {
            GifInputDataSource.this.api.fetchGifs(var1, new Callback() {
               public void onFailure(Call var1x, Throwable var2) {
                  GifInputDataSource.this.loading = false;
                  GifInputDataSource.this.listener.a();
               }

               public void onResponse(Call var1x, Response var2) {
                  GifInputDataSource.this.loading = false;
                  if(var1.equals(GifInputDataSource.this.lastQuery)) {
                     List var3 = GifInputDataSource.this.getImagesFromResponse(((GifResponse)var2.body()).results());
                     GifInputDataSource.this.listener.a(var3);
                  }

               }
            });
         }
      }, 300L);
   }

   public int getCount() {
      return this.currentCount;
   }

   public void getImages(int var1, String var2) {
      this.loading = true;
      this.lastQuery = var2;
      if(TextUtils.isEmpty(var2)) {
         this.loadDefaultGifs();
      } else {
         this.searchGifs(var2);
      }

   }

   public int getPermissionStatus() {
      return 0;
   }

   public boolean isLoading() {
      return this.loading;
   }

   public void requestPermission() {
   }

   public void setListener(e.a var1) {
      this.listener = var1;
   }
}
