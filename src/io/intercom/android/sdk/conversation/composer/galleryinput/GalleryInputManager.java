package io.intercom.android.sdk.conversation.composer.galleryinput;

import android.content.Context;
import android.graphics.drawable.Drawable;
import com.intercom.composer.b.a;
import com.intercom.composer.b.b;
import com.intercom.input.gallery.c;
import com.intercom.input.gallery.d;
import com.intercom.input.gallery.f;
import com.intercom.input.gallery.g;
import com.intercom.input.gallery.j;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.conversation.composer.ComposerPresenter;
import io.intercom.android.sdk.conversation.composer.InputDrawableManager;
import io.intercom.android.sdk.metrics.MetricTracker;

public class GalleryInputManager {
   private static final String INPUT_EXPANDED_PREFIX = "expanded_";
   private String conversationId;
   private final Drawable galleryIcon;
   private final Drawable gifIcon;
   private final ComposerPresenter.Listener listener;
   private final MetricTracker metricTracker;

   public GalleryInputManager(Context var1, InputDrawableManager var2, ComposerPresenter.Listener var3, MetricTracker var4, String var5) {
      this.listener = var3;
      this.metricTracker = var4;
      this.conversationId = var5;
      this.galleryIcon = var2.createDrawable(var1, R.drawable.intercom_input_gallery);
      this.gifIcon = var2.createDrawable(var1, R.drawable.intercom_input_gif);
   }

   public b createGalleryInput() {
      return new d("gallery_input", new a() {
         public Drawable getIconDrawable(String var1, Context var2) {
            return GalleryInputManager.this.galleryIcon;
         }
      }, new j() {
         public void onGalleryOutputReceived(c var1) {
            GalleryInputManager.this.listener.onUploadImageSelected(var1);
         }
      }, new f() {
         public void onInputExpanded() {
            GalleryInputManager.this.metricTracker.expandedInput(GalleryInputManager.this.conversationId, "expanded_gallery_input");
         }
      }, new com.intercom.composer.d() {
         public g create() {
            return new LocalGalleryInputFragment();
         }
      });
   }

   public b createGifInput() {
      return new d("gif_input", new a() {
         public Drawable getIconDrawable(String var1, Context var2) {
            return GalleryInputManager.this.gifIcon;
         }
      }, new j() {
         public void onGalleryOutputReceived(c var1) {
            GalleryInputManager.this.listener.onRemoteImageSelected(var1);
         }
      }, new f() {
         public void onInputExpanded() {
            GalleryInputManager.this.metricTracker.expandedInput(GalleryInputManager.this.conversationId, "expanded_gif_input");
         }
      }, new com.intercom.composer.d() {
         public g create() {
            return new GifInputFragment();
         }
      });
   }

   public void setConversationId(String var1) {
      this.conversationId = var1;
   }
}
