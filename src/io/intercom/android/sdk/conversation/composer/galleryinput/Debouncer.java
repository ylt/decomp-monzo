package io.intercom.android.sdk.conversation.composer.galleryinput;

import android.os.Handler;
import android.os.Looper;

class Debouncer {
   private final Handler handler = new Handler(Looper.getMainLooper());

   void call(Runnable var1, long var2) {
      this.handler.removeCallbacksAndMessages((Object)null);
      this.handler.postDelayed(var1, var2);
   }
}
