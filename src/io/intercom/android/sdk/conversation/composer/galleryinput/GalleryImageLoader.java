package io.intercom.android.sdk.conversation.composer.galleryinput;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.support.v4.content.a;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import com.intercom.composer.e;
import com.intercom.input.gallery.c;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.logger.LumberMill;
import io.intercom.android.sdk.twig.Twig;
import io.intercom.android.sdk.utilities.ImageUtils;
import io.intercom.com.bumptech.glide.i;
import io.intercom.com.bumptech.glide.j;
import io.intercom.com.bumptech.glide.load.l;
import io.intercom.com.bumptech.glide.load.engine.GlideException;
import io.intercom.com.bumptech.glide.load.engine.h;
import io.intercom.com.bumptech.glide.load.resource.b.b;
import io.intercom.com.bumptech.glide.load.resource.bitmap.f;

class GalleryImageLoader implements e {
   private static final float GIF_SIZE_MULTIPLIER = 0.5F;
   private final h diskCacheStrategy;
   private final i requestManager;
   private final f transformation;

   GalleryImageLoader(h var1, f var2, i var3) {
      this.diskCacheStrategy = var1;
      this.transformation = var2;
      this.requestManager = var3;
   }

   static GalleryImageLoader create(h var0, f var1, i var2) {
      return new GalleryImageLoader(var0, var1, var2);
   }

   private Bitmap getBitmapFromDrawable(Drawable var1) {
      Bitmap var2;
      if(var1 instanceof BitmapDrawable) {
         var2 = ((BitmapDrawable)var1).getBitmap();
      } else {
         var2 = null;
      }

      return var2;
   }

   private void logErrorMessageForUrl(Exception var1, String var2) {
      Twig var3 = LumberMill.getLogger();
      var2 = "Failed to load image for URL: " + var2 + " - ";
      if(var1 == null) {
         var3.e(var2 + "no error message, data probably failed to decode", new Object[0]);
      } else {
         var3.e(var2 + var1.getMessage(), new Object[0]);
      }

   }

   public void clear(ImageView var1) {
      this.requestManager.a((View)var1);
   }

   public Bitmap getBitmapFromView(ImageView var1) {
      Drawable var4 = var1.getDrawable();
      Bitmap var5;
      if(var4 instanceof TransitionDrawable) {
         TransitionDrawable var3 = (TransitionDrawable)var4;
         int var2 = var3.getNumberOfLayers() - 1;

         while(true) {
            if(var2 < 0) {
               var5 = null;
               break;
            }

            var5 = this.getBitmapFromDrawable(var3.getDrawable(var2));
            if(var5 != null) {
               break;
            }

            --var2;
         }
      } else {
         var5 = this.getBitmapFromDrawable(var4);
      }

      return var5;
   }

   public void loadImageIntoView(c var1, ImageView var2) {
      final String var3;
      if(TextUtils.isEmpty(var1.d())) {
         var3 = var1.a();
      } else {
         var3 = var1.d();
      }

      Context var6 = var2.getContext();
      io.intercom.com.bumptech.glide.f.f var4 = (new io.intercom.com.bumptech.glide.f.f()).b(this.diskCacheStrategy).a((Drawable)(new ColorDrawable(a.c(var6, R.color.intercom_search_bg_grey))));
      io.intercom.com.bumptech.glide.f.f var7 = var4;
      if(this.transformation != null) {
         var7 = var4.a((l)this.transformation);
      }

      io.intercom.com.bumptech.glide.h var5 = this.requestManager.a((Object)var3);
      var4 = var7;
      if(ImageUtils.isGif(var3)) {
         var4 = var7.a(0.5F);
      }

      var5.a(var4).a((j)b.c()).a(new io.intercom.com.bumptech.glide.f.e() {
         public boolean onLoadFailed(GlideException var1, Object var2, io.intercom.com.bumptech.glide.f.a.h var3x, boolean var4) {
            GalleryImageLoader.this.logErrorMessageForUrl(var1, var3);
            return false;
         }

         public boolean onResourceReady(Drawable var1, Object var2, io.intercom.com.bumptech.glide.f.a.h var3x, io.intercom.com.bumptech.glide.load.a var4, boolean var5) {
            return false;
         }
      }).a(var2);
   }
}
