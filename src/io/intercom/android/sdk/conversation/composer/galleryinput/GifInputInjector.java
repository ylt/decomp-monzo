package io.intercom.android.sdk.conversation.composer.galleryinput;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import com.intercom.input.gallery.e;
import com.intercom.input.gallery.g;
import io.intercom.android.sdk.Injector;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.com.bumptech.glide.i;
import io.intercom.com.bumptech.glide.load.engine.h;

class GifInputInjector implements g.a {
   private final GifInputDataSource dataSource = new GifInputDataSource(Injector.get().getApi(), Injector.get().getMetricTracker());
   private final i requestManager;

   public GifInputInjector(i var1) {
      this.requestManager = var1;
   }

   public e getDataSource(g var1) {
      return this.dataSource;
   }

   public String getEmptyViewSubtitle(Resources var1) {
      return var1.getString(R.string.intercom_no_gifs_matching_query);
   }

   public String getEmptyViewTitle(Resources var1) {
      return var1.getString(R.string.intercom_no_gifs_found);
   }

   public String getErrorViewSubtitle(Resources var1) {
      return var1.getString(R.string.intercom_try_again_minute);
   }

   public String getErrorViewTitle(Resources var1) {
      return var1.getString(R.string.intercom_gifs_load_error);
   }

   public View getExpanderButton(ViewGroup var1) {
      return null;
   }

   public com.intercom.composer.e getImageLoader(g var1) {
      DownscaleOnlyCenterCrop var2 = new DownscaleOnlyCenterCrop(var1.getContext());
      return GalleryImageLoader.create(h.c, var2, this.requestManager);
   }

   public Class getLightBoxFragmentClass(g var1) {
      return GifLightBoxFragment.class;
   }

   public View getSearchView(ViewGroup var1) {
      return LayoutInflater.from(var1.getContext()).inflate(R.layout.intercom_gif_input_search, var1, false);
   }

   public int getThemeColor(Context var1) {
      return ((AppConfig)Injector.get().getAppConfigProvider().get()).getBaseColor();
   }

   public Toolbar getToolbar(ViewGroup var1) {
      Toolbar var4 = (Toolbar)LayoutInflater.from(var1.getContext()).inflate(R.layout.intercom_gif_input_toolbar, var1, false);
      final ImageButton var3 = (ImageButton)var4.findViewById(R.id.clear_search);
      final EditText var2 = (EditText)var4.findViewById(R.id.search_bar);
      var2.requestFocus();
      var2.addTextChangedListener(new TextWatcher() {
         public void afterTextChanged(Editable var1) {
            ImageButton var3x = var3;
            byte var2;
            if(TextUtils.isEmpty(var1)) {
               var2 = 8;
            } else {
               var2 = 0;
            }

            var3x.setVisibility(var2);
            GifInputInjector.this.dataSource.getImages(0, var1.toString());
         }

         public void beforeTextChanged(CharSequence var1, int var2, int var3x, int var4) {
         }

         public void onTextChanged(CharSequence var1, int var2, int var3x, int var4) {
         }
      });
      var3.setOnClickListener(new OnClickListener() {
         public void onClick(View var1) {
            var2.setText((CharSequence)null);
         }
      });
      return var4;
   }
}
