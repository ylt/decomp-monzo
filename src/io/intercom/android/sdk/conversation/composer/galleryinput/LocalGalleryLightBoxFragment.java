package io.intercom.android.sdk.conversation.composer.galleryinput;

import android.support.v4.app.Fragment;
import com.intercom.composer.e;
import com.intercom.input.gallery.i;
import io.intercom.android.sdk.Injector;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.com.bumptech.glide.c;
import io.intercom.com.bumptech.glide.load.engine.h;
import io.intercom.com.bumptech.glide.load.resource.bitmap.f;
import java.util.Arrays;
import java.util.List;

public class LocalGalleryLightBoxFragment extends i {
   private static final List ANNOTATION_COLORS = Arrays.asList(new Integer[]{Integer.valueOf(-171118), Integer.valueOf(-3737543), Integer.valueOf(-12265987)});

   protected i.a getInjector(i var1) {
      return new i.a() {
         private Injector rootInjector() {
            return Injector.get();
         }

         public List getAnnotationColors(i var1) {
            return LocalGalleryLightBoxFragment.ANNOTATION_COLORS;
         }

         public e getImageLoader(i var1) {
            return GalleryImageLoader.create(h.b, (f)null, c.a((Fragment)var1));
         }

         public boolean isAnnotationEnabled(i var1) {
            return ((AppConfig)this.rootInjector().getAppConfigProvider().get()).hasFeature("image-annotation");
         }
      };
   }
}
