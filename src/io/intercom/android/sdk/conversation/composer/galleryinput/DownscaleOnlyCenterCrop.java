package io.intercom.android.sdk.conversation.composer.galleryinput;

import android.content.Context;
import android.graphics.Bitmap;
import io.intercom.com.bumptech.glide.load.engine.a.e;
import io.intercom.com.bumptech.glide.load.resource.bitmap.h;

class DownscaleOnlyCenterCrop extends h {
   DownscaleOnlyCenterCrop(Context var1) {
      super(var1);
   }

   protected Bitmap transform(e var1, Bitmap var2, int var3, int var4) {
      Bitmap var5;
      if(var2.getHeight() <= var4) {
         var5 = var2;
         if(var2.getWidth() <= var3) {
            return var5;
         }
      }

      var5 = super.transform(var1, var2, var3, var4);
      return var5;
   }
}
