package io.intercom.android.sdk.conversation.composer.galleryinput;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import com.intercom.input.gallery.e;
import com.intercom.input.gallery.g;
import com.intercom.input.gallery.m;
import io.intercom.android.sdk.Injector;
import io.intercom.android.sdk.R;
import io.intercom.android.sdk.identity.AppConfig;
import io.intercom.com.bumptech.glide.i;
import io.intercom.com.bumptech.glide.load.engine.h;

class LocalGalleryInputInjector implements g.a {
   private final i requestManager;

   public LocalGalleryInputInjector(i var1) {
      this.requestManager = var1;
   }

   public e getDataSource(g var1) {
      return m.a(var1);
   }

   public String getEmptyViewSubtitle(Resources var1) {
      return var1.getString(R.string.intercom_no_photos_on_device);
   }

   public String getEmptyViewTitle(Resources var1) {
      return var1.getString(R.string.intercom_no_photos);
   }

   public String getErrorViewSubtitle(Resources var1) {
      return null;
   }

   public String getErrorViewTitle(Resources var1) {
      return null;
   }

   public View getExpanderButton(ViewGroup var1) {
      ImageButton var2 = (ImageButton)LayoutInflater.from(var1.getContext()).inflate(R.layout.intercom_expander_button, var1, false);
      var2.setColorFilter(((AppConfig)Injector.get().getAppConfigProvider().get()).getBaseColor());
      return var2;
   }

   public com.intercom.composer.e getImageLoader(g var1) {
      DownscaleOnlyCenterCrop var2 = new DownscaleOnlyCenterCrop(var1.getContext());
      return GalleryImageLoader.create(h.b, var2, this.requestManager);
   }

   public Class getLightBoxFragmentClass(g var1) {
      return LocalGalleryLightBoxFragment.class;
   }

   public View getSearchView(ViewGroup var1) {
      return null;
   }

   public int getThemeColor(Context var1) {
      return ((AppConfig)Injector.get().getAppConfigProvider().get()).getBaseColor();
   }

   public Toolbar getToolbar(ViewGroup var1) {
      return (Toolbar)LayoutInflater.from(var1.getContext()).inflate(R.layout.intercom_gallery_input_toolbar, var1, false);
   }
}
