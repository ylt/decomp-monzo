package io.intercom.android.sdk.conversation.composer.galleryinput;

import android.support.v4.app.Fragment;
import com.intercom.composer.e;
import com.intercom.input.gallery.i;
import io.intercom.com.bumptech.glide.c;
import io.intercom.com.bumptech.glide.load.engine.h;
import io.intercom.com.bumptech.glide.load.resource.bitmap.f;
import java.util.Collections;
import java.util.List;

public class GifLightBoxFragment extends i {
   protected i.a getInjector(i var1) {
      return new i.a() {
         public List getAnnotationColors(i var1) {
            return Collections.emptyList();
         }

         public e getImageLoader(i var1) {
            return GalleryImageLoader.create(h.c, (f)null, c.a((Fragment)var1));
         }

         public boolean isAnnotationEnabled(i var1) {
            return false;
         }
      };
   }
}
