package io.intercom.android.sdk.conversation.composer.galleryinput;

import android.support.v4.app.Fragment;
import com.intercom.input.gallery.g;
import io.intercom.com.bumptech.glide.c;

public class LocalGalleryInputFragment extends g {
   protected g.a getInjector(g var1) {
      return new LocalGalleryInputInjector(c.a((Fragment)this));
   }
}
