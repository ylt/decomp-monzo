package io.intercom.android.sdk.conversation.composer;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import java.util.ArrayList;
import java.util.List;

public class InputProvider extends Fragment {
   private final List inputs = new ArrayList();

   public void addInputs(List var1) {
      this.inputs.addAll(var1);
   }

   public List getInputs() {
      return this.inputs;
   }

   public void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.setRetainInstance(true);
   }
}
