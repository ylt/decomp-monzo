package io.intercom.android;

public final class BuildConfig {
   public static final String APPLICATION_ID = "io.intercom.android";
   public static final String BUILD_TYPE = "release";
   public static final boolean DEBUG = false;
   public static final String FLAVOR = "";
   public static final int VERSION_CODE = -1;
   public static final String VERSION_NAME = "4.0.5";
   public static final boolean isDebug = false;
}
