package io.intercom.a;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

public interface e extends s {
   int a(byte[] var1, int var2, int var3) throws IOException;

   long a(byte var1) throws IOException;

   long a(r var1) throws IOException;

   String a(Charset var1) throws IOException;

   void a(long var1) throws IOException;

   void a(c var1, long var2) throws IOException;

   void a(byte[] var1) throws IOException;

   boolean a(long var1, f var3) throws IOException;

   c b();

   boolean b(long var1) throws IOException;

   f d(long var1) throws IOException;

   boolean f() throws IOException;

   InputStream g();

   byte[] h(long var1) throws IOException;

   byte i() throws IOException;

   void i(long var1) throws IOException;

   short j() throws IOException;

   int k() throws IOException;

   long l() throws IOException;

   short m() throws IOException;

   int n() throws IOException;

   long o() throws IOException;

   long p() throws IOException;

   String r() throws IOException;

   String s() throws IOException;

   byte[] t() throws IOException;
}
