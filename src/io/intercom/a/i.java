package io.intercom.a;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class i extends t {
   private t a;

   public i(t var1) {
      if(var1 == null) {
         throw new IllegalArgumentException("delegate == null");
      } else {
         this.a = var1;
      }
   }

   public final i a(t var1) {
      if(var1 == null) {
         throw new IllegalArgumentException("delegate == null");
      } else {
         this.a = var1;
         return this;
      }
   }

   public final t a() {
      return this.a;
   }

   public t clearDeadline() {
      return this.a.clearDeadline();
   }

   public t clearTimeout() {
      return this.a.clearTimeout();
   }

   public long deadlineNanoTime() {
      return this.a.deadlineNanoTime();
   }

   public t deadlineNanoTime(long var1) {
      return this.a.deadlineNanoTime(var1);
   }

   public boolean hasDeadline() {
      return this.a.hasDeadline();
   }

   public void throwIfReached() throws IOException {
      this.a.throwIfReached();
   }

   public t timeout(long var1, TimeUnit var3) {
      return this.a.timeout(var1, var3);
   }

   public long timeoutNanos() {
      return this.a.timeoutNanos();
   }
}
