package io.intercom.a;

import java.io.IOException;

public interface d extends r {
   long a(s var1) throws IOException;

   c b();

   d b(f var1) throws IOException;

   d b(String var1) throws IOException;

   d c(byte[] var1) throws IOException;

   d c(byte[] var1, int var2, int var3) throws IOException;

   d e() throws IOException;

   void flush() throws IOException;

   d g(int var1) throws IOException;

   d h(int var1) throws IOException;

   d i(int var1) throws IOException;

   d m(long var1) throws IOException;

   d n(long var1) throws IOException;

   d o(long var1) throws IOException;

   d x() throws IOException;
}
