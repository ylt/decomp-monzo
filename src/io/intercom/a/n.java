package io.intercom.a;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

final class n implements e {
   public final c a = new c();
   public final s b;
   boolean c;

   n(s var1) {
      if(var1 == null) {
         throw new NullPointerException("source == null");
      } else {
         this.b = var1;
      }
   }

   public int a(byte[] var1, int var2, int var3) throws IOException {
      u.a((long)var1.length, (long)var2, (long)var3);
      if(this.a.b == 0L && this.b.read(this.a, 8192L) == -1L) {
         var2 = -1;
      } else {
         var3 = (int)Math.min((long)var3, this.a.b);
         var2 = this.a.a(var1, var2, var3);
      }

      return var2;
   }

   public long a(byte var1) throws IOException {
      return this.a(var1, 0L, Long.MAX_VALUE);
   }

   public long a(byte var1, long var2, long var4) throws IOException {
      if(this.c) {
         throw new IllegalStateException("closed");
      } else if(var2 >= 0L && var4 >= var2) {
         while(true) {
            if(var2 < var4) {
               long var6 = this.a.a(var1, var2, var4);
               if(var6 != -1L) {
                  var2 = var6;
                  break;
               }

               var6 = this.a.b;
               if(var6 < var4 && this.b.read(this.a, 8192L) != -1L) {
                  var2 = Math.max(var2, var6);
                  continue;
               }

               var2 = -1L;
               break;
            }

            var2 = -1L;
            break;
         }

         return var2;
      } else {
         throw new IllegalArgumentException(String.format("fromIndex=%s toIndex=%s", new Object[]{Long.valueOf(var2), Long.valueOf(var4)}));
      }
   }

   public long a(r var1) throws IOException {
      if(var1 == null) {
         throw new IllegalArgumentException("sink == null");
      } else {
         long var2 = 0L;

         long var4;
         while(this.b.read(this.a, 8192L) != -1L) {
            var4 = this.a.h();
            if(var4 > 0L) {
               var2 += var4;
               var1.write(this.a, var4);
            }
         }

         var4 = var2;
         if(this.a.a() > 0L) {
            var4 = var2 + this.a.a();
            var1.write(this.a, this.a.a());
         }

         return var4;
      }
   }

   public String a(Charset var1) throws IOException {
      if(var1 == null) {
         throw new IllegalArgumentException("charset == null");
      } else {
         this.a.a(this.b);
         return this.a.a(var1);
      }
   }

   public void a(long var1) throws IOException {
      if(!this.b(var1)) {
         throw new EOFException();
      }
   }

   public void a(c var1, long var2) throws IOException {
      try {
         this.a(var2);
      } catch (EOFException var5) {
         var1.a((s)this.a);
         throw var5;
      }

      this.a.a(var1, var2);
   }

   public void a(byte[] var1) throws IOException {
      try {
         this.a((long)var1.length);
      } catch (EOFException var5) {
         int var3;
         for(int var2 = 0; this.a.b > 0L; var2 += var3) {
            var3 = this.a.a(var1, var2, (int)this.a.b);
            if(var3 == -1) {
               throw new AssertionError();
            }
         }

         throw var5;
      }

      this.a.a(var1);
   }

   public boolean a(long var1, f var3) throws IOException {
      return this.a(var1, var3, 0, var3.h());
   }

   public boolean a(long var1, f var3, int var4, int var5) throws IOException {
      boolean var10 = false;
      if(this.c) {
         throw new IllegalStateException("closed");
      } else {
         boolean var9 = var10;
         if(var1 >= 0L) {
            var9 = var10;
            if(var4 >= 0) {
               var9 = var10;
               if(var5 >= 0) {
                  if(var3.h() - var4 < var5) {
                     var9 = var10;
                  } else {
                     int var6 = 0;

                     while(true) {
                        if(var6 >= var5) {
                           var9 = true;
                           break;
                        }

                        long var7 = (long)var6 + var1;
                        var9 = var10;
                        if(!this.b(1L + var7)) {
                           break;
                        }

                        var9 = var10;
                        if(this.a.c(var7) != var3.a(var4 + var6)) {
                           break;
                        }

                        ++var6;
                     }
                  }
               }
            }
         }

         return var9;
      }
   }

   public c b() {
      return this.a;
   }

   public boolean b(long var1) throws IOException {
      if(var1 < 0L) {
         throw new IllegalArgumentException("byteCount < 0: " + var1);
      } else if(this.c) {
         throw new IllegalStateException("closed");
      } else {
         boolean var3;
         while(true) {
            if(this.a.b < var1) {
               if(this.b.read(this.a, 8192L) != -1L) {
                  continue;
               }

               var3 = false;
               break;
            }

            var3 = true;
            break;
         }

         return var3;
      }
   }

   public String c(long var1) throws IOException {
      if(var1 < 0L) {
         throw new IllegalArgumentException("limit < 0: " + var1);
      } else {
         long var3;
         if(var1 == Long.MAX_VALUE) {
            var3 = Long.MAX_VALUE;
         } else {
            var3 = var1 + 1L;
         }

         long var5 = this.a(10, 0L, var3);
         String var7;
         if(var5 != -1L) {
            var7 = this.a.g(var5);
         } else {
            if(var3 >= Long.MAX_VALUE || !this.b(var3) || this.a.c(var3 - 1L) != 13 || !this.b(1L + var3) || this.a.c(var3) != 10) {
               c var8 = new c();
               this.a.a(var8, 0L, Math.min(32L, this.a.a()));
               throw new EOFException("\\n not found: limit=" + Math.min(this.a.a(), var1) + " content=" + var8.q().f() + '…');
            }

            var7 = this.a.g(var3);
         }

         return var7;
      }
   }

   public void close() throws IOException {
      if(!this.c) {
         this.c = true;
         this.b.close();
         this.a.u();
      }

   }

   public f d(long var1) throws IOException {
      this.a(var1);
      return this.a.d(var1);
   }

   public boolean f() throws IOException {
      if(this.c) {
         throw new IllegalStateException("closed");
      } else {
         boolean var1;
         if(this.a.f() && this.b.read(this.a, 8192L) == -1L) {
            var1 = true;
         } else {
            var1 = false;
         }

         return var1;
      }
   }

   public InputStream g() {
      return new InputStream() {
         public int available() throws IOException {
            if(n.this.c) {
               throw new IOException("closed");
            } else {
               return (int)Math.min(n.this.a.b, 2147483647L);
            }
         }

         public void close() throws IOException {
            n.this.close();
         }

         public int read() throws IOException {
            if(n.this.c) {
               throw new IOException("closed");
            } else {
               int var1;
               if(n.this.a.b == 0L && n.this.b.read(n.this.a, 8192L) == -1L) {
                  var1 = -1;
               } else {
                  var1 = n.this.a.i() & 255;
               }

               return var1;
            }
         }

         public int read(byte[] var1, int var2, int var3) throws IOException {
            if(n.this.c) {
               throw new IOException("closed");
            } else {
               u.a((long)var1.length, (long)var2, (long)var3);
               if(n.this.a.b == 0L && n.this.b.read(n.this.a, 8192L) == -1L) {
                  var2 = -1;
               } else {
                  var2 = n.this.a.a(var1, var2, var3);
               }

               return var2;
            }
         }

         public String toString() {
            return n.this + ".inputStream()";
         }
      };
   }

   public byte[] h(long var1) throws IOException {
      this.a(var1);
      return this.a.h(var1);
   }

   public byte i() throws IOException {
      this.a(1L);
      return this.a.i();
   }

   public void i(long var1) throws IOException {
      if(this.c) {
         throw new IllegalStateException("closed");
      } else {
         while(var1 > 0L) {
            if(this.a.b == 0L && this.b.read(this.a, 8192L) == -1L) {
               throw new EOFException();
            }

            long var3 = Math.min(var1, this.a.a());
            this.a.i(var3);
            var1 -= var3;
         }

      }
   }

   public short j() throws IOException {
      this.a(2L);
      return this.a.j();
   }

   public int k() throws IOException {
      this.a(4L);
      return this.a.k();
   }

   public long l() throws IOException {
      this.a(8L);
      return this.a.l();
   }

   public short m() throws IOException {
      this.a(2L);
      return this.a.m();
   }

   public int n() throws IOException {
      this.a(4L);
      return this.a.n();
   }

   public long o() throws IOException {
      this.a(1L);

      for(int var2 = 0; this.b((long)(var2 + 1)); ++var2) {
         byte var1 = this.a.c((long)var2);
         if((var1 < 48 || var1 > 57) && (var2 != 0 || var1 != 45)) {
            if(var2 == 0) {
               throw new NumberFormatException(String.format("Expected leading [0-9] or '-' character but was %#x", new Object[]{Byte.valueOf(var1)}));
            }
            break;
         }
      }

      return this.a.o();
   }

   public long p() throws IOException {
      this.a(1L);

      for(int var2 = 0; this.b((long)(var2 + 1)); ++var2) {
         byte var1 = this.a.c((long)var2);
         if((var1 < 48 || var1 > 57) && (var1 < 97 || var1 > 102) && (var1 < 65 || var1 > 70)) {
            if(var2 == 0) {
               throw new NumberFormatException(String.format("Expected leading [0-9a-fA-F] character but was %#x", new Object[]{Byte.valueOf(var1)}));
            }
            break;
         }
      }

      return this.a.p();
   }

   public String r() throws IOException {
      this.a.a(this.b);
      return this.a.r();
   }

   public long read(c var1, long var2) throws IOException {
      long var4 = -1L;
      if(var1 == null) {
         throw new IllegalArgumentException("sink == null");
      } else if(var2 < 0L) {
         throw new IllegalArgumentException("byteCount < 0: " + var2);
      } else if(this.c) {
         throw new IllegalStateException("closed");
      } else {
         if(this.a.b == 0L && this.b.read(this.a, 8192L) == -1L) {
            var2 = var4;
         } else {
            var2 = Math.min(var2, this.a.b);
            var2 = this.a.read(var1, var2);
         }

         return var2;
      }
   }

   public String s() throws IOException {
      return this.c(Long.MAX_VALUE);
   }

   public byte[] t() throws IOException {
      this.a.a(this.b);
      return this.a.t();
   }

   public t timeout() {
      return this.b.timeout();
   }

   public String toString() {
      return "buffer(" + this.b + ")";
   }
}
