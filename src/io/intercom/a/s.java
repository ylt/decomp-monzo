package io.intercom.a;

import java.io.Closeable;
import java.io.IOException;

public interface s extends Closeable {
   void close() throws IOException;

   long read(c var1, long var2) throws IOException;

   t timeout();
}
