package io.intercom.a;

import java.io.IOException;

public abstract class g implements r {
   private final r delegate;

   public g(r var1) {
      if(var1 == null) {
         throw new IllegalArgumentException("delegate == null");
      } else {
         this.delegate = var1;
      }
   }

   public void close() throws IOException {
      this.delegate.close();
   }

   public final r delegate() {
      return this.delegate;
   }

   public void flush() throws IOException {
      this.delegate.flush();
   }

   public t timeout() {
      return this.delegate.timeout();
   }

   public String toString() {
      return this.getClass().getSimpleName() + "(" + this.delegate.toString() + ")";
   }

   public void write(c var1, long var2) throws IOException {
      this.delegate.write(var1, var2);
   }
}
