package io.intercom.a;

import java.io.Closeable;
import java.io.Flushable;
import java.io.IOException;

public interface r extends Closeable, Flushable {
   void close() throws IOException;

   void flush() throws IOException;

   t timeout();

   void write(c var1, long var2) throws IOException;
}
