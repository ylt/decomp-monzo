package io.intercom.a;

import java.util.Arrays;

final class q extends f {
   final transient byte[][] f;
   final transient int[] g;

   q(c var1, int var2) {
      byte var5 = 0;
      super((byte[])null);
      u.a(var1.b, 0L, (long)var2);
      o var6 = var1.a;
      int var4 = 0;

      int var3;
      for(var3 = 0; var3 < var2; var6 = var6.f) {
         if(var6.c == var6.b) {
            throw new AssertionError("s.limit == s.pos");
         }

         var3 += var6.c - var6.b;
         ++var4;
      }

      this.f = new byte[var4][];
      this.g = new int[var4 * 2];
      o var7 = var1.a;
      var4 = 0;

      for(var3 = var5; var3 < var2; var7 = var7.f) {
         this.f[var4] = var7.a;
         int var8 = var7.c - var7.b + var3;
         var3 = var8;
         if(var8 > var2) {
            var3 = var2;
         }

         this.g[var4] = var3;
         this.g[this.f.length + var4] = var7.b;
         var7.d = true;
         ++var4;
      }

   }

   private int b(int var1) {
      var1 = Arrays.binarySearch(this.g, 0, this.f.length, var1 + 1);
      if(var1 < 0) {
         var1 = ~var1;
      }

      return var1;
   }

   private f j() {
      return new f(this.i());
   }

   private Object writeReplace() {
      return this.j();
   }

   public byte a(int var1) {
      u.a((long)this.g[this.f.length - 1], (long)var1, 1L);
      int var3 = this.b(var1);
      int var2;
      if(var3 == 0) {
         var2 = 0;
      } else {
         var2 = this.g[var3 - 1];
      }

      int var4 = this.g[this.f.length + var3];
      return this.f[var3][var1 - var2 + var4];
   }

   public f a(int var1, int var2) {
      return this.j().a(var1, var2);
   }

   public String a() {
      return this.j().a();
   }

   void a(c var1) {
      int var3 = 0;
      int var5 = this.f.length;

      int var2;
      int var4;
      for(var2 = 0; var3 < var5; var2 = var4) {
         int var6 = this.g[var5 + var3];
         var4 = this.g[var3];
         o var9 = new o(this.f[var3], var6, var6 + var4 - var2);
         if(var1.a == null) {
            var9.g = var9;
            var9.f = var9;
            var1.a = var9;
         } else {
            var1.a.g.a(var9);
         }

         ++var3;
      }

      long var7 = var1.b;
      var1.b = (long)var2 + var7;
   }

   public boolean a(int var1, f var2, int var3, int var4) {
      boolean var10 = false;
      boolean var9 = var10;
      if(var1 >= 0) {
         if(var1 > this.h() - var4) {
            var9 = var10;
         } else {
            for(int var5 = this.b(var1); var4 > 0; ++var5) {
               int var6;
               if(var5 == 0) {
                  var6 = 0;
               } else {
                  var6 = this.g[var5 - 1];
               }

               int var7 = Math.min(var4, this.g[var5] - var6 + var6 - var1);
               int var8 = this.g[this.f.length + var5];
               var9 = var10;
               if(!var2.a(var3, this.f[var5], var1 - var6 + var8, var7)) {
                  return var9;
               }

               var1 += var7;
               var3 += var7;
               var4 -= var7;
            }

            var9 = true;
         }
      }

      return var9;
   }

   public boolean a(int var1, byte[] var2, int var3, int var4) {
      boolean var10 = false;
      boolean var9 = var10;
      if(var1 >= 0) {
         var9 = var10;
         if(var1 <= this.h() - var4) {
            var9 = var10;
            if(var3 >= 0) {
               if(var3 > var2.length - var4) {
                  var9 = var10;
               } else {
                  for(int var5 = this.b(var1); var4 > 0; ++var5) {
                     int var6;
                     if(var5 == 0) {
                        var6 = 0;
                     } else {
                        var6 = this.g[var5 - 1];
                     }

                     int var7 = Math.min(var4, this.g[var5] - var6 + var6 - var1);
                     int var8 = this.g[this.f.length + var5];
                     var9 = var10;
                     if(!u.a(this.f[var5], var1 - var6 + var8, var2, var3, var7)) {
                        return var9;
                     }

                     var1 += var7;
                     var3 += var7;
                     var4 -= var7;
                  }

                  var9 = true;
               }
            }
         }
      }

      return var9;
   }

   public String b() {
      return this.j().b();
   }

   public f c() {
      return this.j().c();
   }

   public f d() {
      return this.j().d();
   }

   public f e() {
      return this.j().e();
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(var1 != this) {
         if(var1 instanceof f && ((f)var1).h() == this.h() && this.a(0, (f)((f)var1), 0, this.h())) {
            var2 = true;
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public String f() {
      return this.j().f();
   }

   public f g() {
      return this.j().g();
   }

   public int h() {
      return this.g[this.f.length - 1];
   }

   public int hashCode() {
      int var1 = this.d;
      if(var1 == 0) {
         var1 = 1;
         int var7 = this.f.length;
         int var2 = 0;

         int var5;
         for(int var3 = 0; var2 < var7; var3 = var5) {
            byte[] var8 = this.f[var2];
            int var6 = this.g[var7 + var2];
            var5 = this.g[var2];

            for(int var4 = var6; var4 < var5 - var3 + var6; ++var4) {
               var1 = var1 * 31 + var8[var4];
            }

            ++var2;
         }

         this.d = var1;
      }

      return var1;
   }

   public byte[] i() {
      int var2 = 0;
      byte[] var6 = new byte[this.g[this.f.length - 1]];
      int var4 = this.f.length;

      int var3;
      for(int var1 = 0; var2 < var4; var1 = var3) {
         int var5 = this.g[var4 + var2];
         var3 = this.g[var2];
         System.arraycopy(this.f[var2], var5, var6, var1, var3 - var1);
         ++var2;
      }

      return var6;
   }

   public String toString() {
      return this.j().toString();
   }
}
