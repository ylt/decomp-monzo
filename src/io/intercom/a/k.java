package io.intercom.a;

import java.io.IOException;
import java.util.zip.Inflater;

public final class k implements s {
   private final e a;
   private final Inflater b;
   private int c;
   private boolean d;

   k(e var1, Inflater var2) {
      if(var1 == null) {
         throw new IllegalArgumentException("source == null");
      } else if(var2 == null) {
         throw new IllegalArgumentException("inflater == null");
      } else {
         this.a = var1;
         this.b = var2;
      }
   }

   private void b() throws IOException {
      if(this.c != 0) {
         int var1 = this.c - this.b.getRemaining();
         this.c -= var1;
         this.a.i((long)var1);
      }

   }

   public boolean a() throws IOException {
      boolean var1 = false;
      if(this.b.needsInput()) {
         this.b();
         if(this.b.getRemaining() != 0) {
            throw new IllegalStateException("?");
         }

         if(this.a.f()) {
            var1 = true;
         } else {
            o var2 = this.a.b().a;
            this.c = var2.c - var2.b;
            this.b.setInput(var2.a, var2.b, this.c);
         }
      }

      return var1;
   }

   public void close() throws IOException {
      if(!this.d) {
         this.b.end();
         this.d = true;
         this.a.close();
      }

   }

   public long read(c param1, long param2) throws IOException {
      // $FF: Couldn't be decompiled
   }

   public t timeout() {
      return this.a.timeout();
   }
}
