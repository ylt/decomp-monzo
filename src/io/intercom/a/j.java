package io.intercom.a;

import java.io.EOFException;
import java.io.IOException;
import java.util.zip.CRC32;
import java.util.zip.Inflater;

public final class j implements s {
   private int a = 0;
   private final e b;
   private final Inflater c;
   private final k d;
   private final CRC32 e = new CRC32();

   public j(s var1) {
      if(var1 == null) {
         throw new IllegalArgumentException("source == null");
      } else {
         this.c = new Inflater(true);
         this.b = l.a(var1);
         this.d = new k(this.b, this.c);
      }
   }

   private void a() throws IOException {
      this.b.a(10L);
      byte var2 = this.b.b().c(3L);
      boolean var1;
      if((var2 >> 1 & 1) == 1) {
         var1 = true;
      } else {
         var1 = false;
      }

      if(var1) {
         this.a(this.b.b(), 0L, 10L);
      }

      this.a("ID1ID2", 8075, this.b.j());
      this.b.i(8L);
      if((var2 >> 2 & 1) == 1) {
         this.b.a(2L);
         if(var1) {
            this.a(this.b.b(), 0L, 2L);
         }

         short var3 = this.b.b().m();
         this.b.a((long)var3);
         if(var1) {
            this.a(this.b.b(), 0L, (long)var3);
         }

         this.b.i((long)var3);
      }

      long var4;
      if((var2 >> 3 & 1) == 1) {
         var4 = this.b.a(0);
         if(var4 == -1L) {
            throw new EOFException();
         }

         if(var1) {
            this.a(this.b.b(), 0L, 1L + var4);
         }

         this.b.i(1L + var4);
      }

      if((var2 >> 4 & 1) == 1) {
         var4 = this.b.a(0);
         if(var4 == -1L) {
            throw new EOFException();
         }

         if(var1) {
            this.a(this.b.b(), 0L, 1L + var4);
         }

         this.b.i(1L + var4);
      }

      if(var1) {
         this.a("FHCRC", this.b.m(), (short)((int)this.e.getValue()));
         this.e.reset();
      }

   }

   private void a(c var1, long var2, long var4) {
      o var13 = var1.a;

      while(true) {
         o var12 = var13;
         long var8 = var2;
         long var10 = var4;
         if(var2 < (long)(var13.c - var13.b)) {
            while(var10 > 0L) {
               int var7 = (int)((long)var12.b + var8);
               int var6 = (int)Math.min((long)(var12.c - var7), var10);
               this.e.update(var12.a, var7, var6);
               var10 -= (long)var6;
               var12 = var12.f;
               var8 = 0L;
            }

            return;
         }

         var2 -= (long)(var13.c - var13.b);
         var13 = var13.f;
      }
   }

   private void a(String var1, int var2, int var3) throws IOException {
      if(var3 != var2) {
         throw new IOException(String.format("%s: actual 0x%08x != expected 0x%08x", new Object[]{var1, Integer.valueOf(var3), Integer.valueOf(var2)}));
      }
   }

   private void b() throws IOException {
      this.a("CRC", this.b.n(), (int)this.e.getValue());
      this.a("ISIZE", this.b.n(), (int)this.c.getBytesWritten());
   }

   public void close() throws IOException {
      this.d.close();
   }

   public long read(c var1, long var2) throws IOException {
      long var4 = 0L;
      if(var2 < 0L) {
         throw new IllegalArgumentException("byteCount < 0: " + var2);
      } else {
         if(var2 == 0L) {
            var2 = var4;
         } else {
            if(this.a == 0) {
               this.a();
               this.a = 1;
            }

            if(this.a == 1) {
               var4 = var1.b;
               var2 = this.d.read(var1, var2);
               if(var2 != -1L) {
                  this.a(var1, var4, var2);
                  return var2;
               }

               this.a = 2;
            }

            if(this.a == 2) {
               this.b();
               this.a = 3;
               if(!this.b.f()) {
                  throw new IOException("gzip finished without exhausting source");
               }
            }

            var2 = -1L;
         }

         return var2;
      }
   }

   public t timeout() {
      return this.b.timeout();
   }
}
