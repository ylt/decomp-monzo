package io.intercom.a;

import java.io.IOException;

public abstract class h implements s {
   private final s delegate;

   public h(s var1) {
      if(var1 == null) {
         throw new IllegalArgumentException("delegate == null");
      } else {
         this.delegate = var1;
      }
   }

   public void close() throws IOException {
      this.delegate.close();
   }

   public final s delegate() {
      return this.delegate;
   }

   public long read(c var1, long var2) throws IOException {
      return this.delegate.read(var1, var2);
   }

   public t timeout() {
      return this.delegate.timeout();
   }

   public String toString() {
      return this.getClass().getSimpleName() + "(" + this.delegate.toString() + ")";
   }
}
