package io.intercom.okhttp3;

import io.intercom.okhttp3.internal.http.HttpHeaders;
import java.util.concurrent.TimeUnit;
import javax.annotation.Nullable;

public final class CacheControl {
   public static final CacheControl FORCE_CACHE;
   public static final CacheControl FORCE_NETWORK = (new CacheControl.Builder()).noCache().build();
   @Nullable
   String headerValue;
   private final boolean immutable;
   private final boolean isPrivate;
   private final boolean isPublic;
   private final int maxAgeSeconds;
   private final int maxStaleSeconds;
   private final int minFreshSeconds;
   private final boolean mustRevalidate;
   private final boolean noCache;
   private final boolean noStore;
   private final boolean noTransform;
   private final boolean onlyIfCached;
   private final int sMaxAgeSeconds;

   static {
      FORCE_CACHE = (new CacheControl.Builder()).onlyIfCached().maxStale(Integer.MAX_VALUE, TimeUnit.SECONDS).build();
   }

   CacheControl(CacheControl.Builder var1) {
      this.noCache = var1.noCache;
      this.noStore = var1.noStore;
      this.maxAgeSeconds = var1.maxAgeSeconds;
      this.sMaxAgeSeconds = -1;
      this.isPrivate = false;
      this.isPublic = false;
      this.mustRevalidate = false;
      this.maxStaleSeconds = var1.maxStaleSeconds;
      this.minFreshSeconds = var1.minFreshSeconds;
      this.onlyIfCached = var1.onlyIfCached;
      this.noTransform = var1.noTransform;
      this.immutable = var1.immutable;
   }

   private CacheControl(boolean var1, boolean var2, int var3, int var4, boolean var5, boolean var6, boolean var7, int var8, int var9, boolean var10, boolean var11, boolean var12, @Nullable String var13) {
      this.noCache = var1;
      this.noStore = var2;
      this.maxAgeSeconds = var3;
      this.sMaxAgeSeconds = var4;
      this.isPrivate = var5;
      this.isPublic = var6;
      this.mustRevalidate = var7;
      this.maxStaleSeconds = var8;
      this.minFreshSeconds = var9;
      this.onlyIfCached = var10;
      this.noTransform = var11;
      this.immutable = var12;
      this.headerValue = var13;
   }

   private String headerValue() {
      StringBuilder var1 = new StringBuilder();
      if(this.noCache) {
         var1.append("no-cache, ");
      }

      if(this.noStore) {
         var1.append("no-store, ");
      }

      if(this.maxAgeSeconds != -1) {
         var1.append("max-age=").append(this.maxAgeSeconds).append(", ");
      }

      if(this.sMaxAgeSeconds != -1) {
         var1.append("s-maxage=").append(this.sMaxAgeSeconds).append(", ");
      }

      if(this.isPrivate) {
         var1.append("private, ");
      }

      if(this.isPublic) {
         var1.append("public, ");
      }

      if(this.mustRevalidate) {
         var1.append("must-revalidate, ");
      }

      if(this.maxStaleSeconds != -1) {
         var1.append("max-stale=").append(this.maxStaleSeconds).append(", ");
      }

      if(this.minFreshSeconds != -1) {
         var1.append("min-fresh=").append(this.minFreshSeconds).append(", ");
      }

      if(this.onlyIfCached) {
         var1.append("only-if-cached, ");
      }

      if(this.noTransform) {
         var1.append("no-transform, ");
      }

      if(this.immutable) {
         var1.append("immutable, ");
      }

      String var2;
      if(var1.length() == 0) {
         var2 = "";
      } else {
         var1.delete(var1.length() - 2, var1.length());
         var2 = var1.toString();
      }

      return var2;
   }

   public static CacheControl parse(Headers var0) {
      boolean var20 = false;
      int var6 = -1;
      int var5 = -1;
      boolean var21 = false;
      boolean var19 = false;
      boolean var18 = false;
      int var4 = -1;
      int var3 = -1;
      boolean var17 = false;
      boolean var16 = false;
      boolean var15 = false;
      boolean var1 = true;
      int var13 = var0.size();
      int var7 = 0;
      String var30 = null;

      boolean var2;
      boolean var14;
      for(var14 = false; var7 < var13; var1 = var2) {
         int var9;
         int var10;
         int var11;
         int var12;
         boolean var22;
         boolean var23;
         boolean var24;
         boolean var25;
         boolean var26;
         boolean var27;
         boolean var28;
         boolean var29;
         String var31;
         label90: {
            var31 = var0.name(var7);
            String var32 = var0.value(var7);
            if(var31.equalsIgnoreCase("Cache-Control")) {
               if(var30 != null) {
                  var1 = false;
               } else {
                  var30 = var32;
               }
            } else {
               if(!var31.equalsIgnoreCase("Pragma")) {
                  var22 = var14;
                  var2 = var1;
                  var23 = var15;
                  var24 = var16;
                  var25 = var17;
                  var9 = var3;
                  var10 = var4;
                  var26 = var18;
                  var27 = var19;
                  var28 = var21;
                  var11 = var5;
                  var12 = var6;
                  var29 = var20;
                  var31 = var30;
                  break label90;
               }

               var1 = false;
            }

            int var8 = 0;

            while(true) {
               var31 = var30;
               var29 = var20;
               var12 = var6;
               var11 = var5;
               var28 = var21;
               var27 = var19;
               var26 = var18;
               var10 = var4;
               var9 = var3;
               var25 = var17;
               var24 = var16;
               var23 = var15;
               var2 = var1;
               var22 = var14;
               if(var8 >= var32.length()) {
                  break;
               }

               int var35 = HttpHeaders.skipUntil(var32, var8, "=,;");
               String var33 = var32.substring(var8, var35).trim();
               if(var35 != var32.length() && var32.charAt(var35) != 44 && var32.charAt(var35) != 59) {
                  var8 = HttpHeaders.skipWhitespace(var32, var35 + 1);
                  if(var8 < var32.length() && var32.charAt(var8) == 34) {
                     var35 = var8 + 1;
                     var8 = HttpHeaders.skipUntil(var32, var35, "\"");
                     var31 = var32.substring(var35, var8);
                     var35 = var8 + 1;
                  } else {
                     var35 = HttpHeaders.skipUntil(var32, var8, ",;");
                     var31 = var32.substring(var8, var35).trim();
                  }
               } else {
                  ++var35;
                  var31 = null;
               }

               if("no-cache".equalsIgnoreCase(var33)) {
                  var14 = true;
                  var8 = var35;
               } else if("no-store".equalsIgnoreCase(var33)) {
                  var20 = true;
                  var8 = var35;
               } else if("max-age".equalsIgnoreCase(var33)) {
                  var6 = HttpHeaders.parseSeconds(var31, -1);
                  var8 = var35;
               } else if("s-maxage".equalsIgnoreCase(var33)) {
                  var5 = HttpHeaders.parseSeconds(var31, -1);
                  var8 = var35;
               } else if("private".equalsIgnoreCase(var33)) {
                  var21 = true;
                  var8 = var35;
               } else if("public".equalsIgnoreCase(var33)) {
                  var19 = true;
                  var8 = var35;
               } else if("must-revalidate".equalsIgnoreCase(var33)) {
                  var18 = true;
                  var8 = var35;
               } else if("max-stale".equalsIgnoreCase(var33)) {
                  var4 = HttpHeaders.parseSeconds(var31, Integer.MAX_VALUE);
                  var8 = var35;
               } else if("min-fresh".equalsIgnoreCase(var33)) {
                  var3 = HttpHeaders.parseSeconds(var31, -1);
                  var8 = var35;
               } else if("only-if-cached".equalsIgnoreCase(var33)) {
                  var17 = true;
                  var8 = var35;
               } else if("no-transform".equalsIgnoreCase(var33)) {
                  var16 = true;
                  var8 = var35;
               } else {
                  var8 = var35;
                  if("immutable".equalsIgnoreCase(var33)) {
                     var15 = true;
                     var8 = var35;
                  }
               }
            }
         }

         ++var7;
         var14 = var22;
         var30 = var31;
         var20 = var29;
         var6 = var12;
         var5 = var11;
         var21 = var28;
         var19 = var27;
         var18 = var26;
         var4 = var10;
         var3 = var9;
         var17 = var25;
         var16 = var24;
         var15 = var23;
      }

      String var34;
      if(!var1) {
         var34 = null;
      } else {
         var34 = var30;
      }

      return new CacheControl(var14, var20, var6, var5, var21, var19, var18, var4, var3, var17, var16, var15, var34);
   }

   public boolean immutable() {
      return this.immutable;
   }

   public boolean isPrivate() {
      return this.isPrivate;
   }

   public boolean isPublic() {
      return this.isPublic;
   }

   public int maxAgeSeconds() {
      return this.maxAgeSeconds;
   }

   public int maxStaleSeconds() {
      return this.maxStaleSeconds;
   }

   public int minFreshSeconds() {
      return this.minFreshSeconds;
   }

   public boolean mustRevalidate() {
      return this.mustRevalidate;
   }

   public boolean noCache() {
      return this.noCache;
   }

   public boolean noStore() {
      return this.noStore;
   }

   public boolean noTransform() {
      return this.noTransform;
   }

   public boolean onlyIfCached() {
      return this.onlyIfCached;
   }

   public int sMaxAgeSeconds() {
      return this.sMaxAgeSeconds;
   }

   public String toString() {
      String var1 = this.headerValue;
      if(var1 == null) {
         var1 = this.headerValue();
         this.headerValue = var1;
      }

      return var1;
   }

   public static final class Builder {
      boolean immutable;
      int maxAgeSeconds = -1;
      int maxStaleSeconds = -1;
      int minFreshSeconds = -1;
      boolean noCache;
      boolean noStore;
      boolean noTransform;
      boolean onlyIfCached;

      public CacheControl build() {
         return new CacheControl(this);
      }

      public CacheControl.Builder immutable() {
         this.immutable = true;
         return this;
      }

      public CacheControl.Builder maxAge(int var1, TimeUnit var2) {
         if(var1 < 0) {
            throw new IllegalArgumentException("maxAge < 0: " + var1);
         } else {
            long var3 = var2.toSeconds((long)var1);
            if(var3 > 2147483647L) {
               var1 = Integer.MAX_VALUE;
            } else {
               var1 = (int)var3;
            }

            this.maxAgeSeconds = var1;
            return this;
         }
      }

      public CacheControl.Builder maxStale(int var1, TimeUnit var2) {
         if(var1 < 0) {
            throw new IllegalArgumentException("maxStale < 0: " + var1);
         } else {
            long var3 = var2.toSeconds((long)var1);
            if(var3 > 2147483647L) {
               var1 = Integer.MAX_VALUE;
            } else {
               var1 = (int)var3;
            }

            this.maxStaleSeconds = var1;
            return this;
         }
      }

      public CacheControl.Builder minFresh(int var1, TimeUnit var2) {
         if(var1 < 0) {
            throw new IllegalArgumentException("minFresh < 0: " + var1);
         } else {
            long var3 = var2.toSeconds((long)var1);
            if(var3 > 2147483647L) {
               var1 = Integer.MAX_VALUE;
            } else {
               var1 = (int)var3;
            }

            this.minFreshSeconds = var1;
            return this;
         }
      }

      public CacheControl.Builder noCache() {
         this.noCache = true;
         return this;
      }

      public CacheControl.Builder noStore() {
         this.noStore = true;
         return this;
      }

      public CacheControl.Builder noTransform() {
         this.noTransform = true;
         return this;
      }

      public CacheControl.Builder onlyIfCached() {
         this.onlyIfCached = true;
         return this;
      }
   }
}
