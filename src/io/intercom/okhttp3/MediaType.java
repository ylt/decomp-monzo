package io.intercom.okhttp3;

import java.nio.charset.Charset;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.Nullable;

public final class MediaType {
   private static final Pattern PARAMETER = Pattern.compile(";\\s*(?:([a-zA-Z0-9-!#$%&'*+.^_`{|}~]+)=(?:([a-zA-Z0-9-!#$%&'*+.^_`{|}~]+)|\"([^\"]*)\"))?");
   private static final String QUOTED = "\"([^\"]*)\"";
   private static final String TOKEN = "([a-zA-Z0-9-!#$%&'*+.^_`{|}~]+)";
   private static final Pattern TYPE_SUBTYPE = Pattern.compile("([a-zA-Z0-9-!#$%&'*+.^_`{|}~]+)/([a-zA-Z0-9-!#$%&'*+.^_`{|}~]+)");
   @Nullable
   private final String charset;
   private final String mediaType;
   private final String subtype;
   private final String type;

   private MediaType(String var1, String var2, String var3, @Nullable String var4) {
      this.mediaType = var1;
      this.type = var2;
      this.subtype = var3;
      this.charset = var4;
   }

   @Nullable
   public static MediaType parse(String var0) {
      Object var5 = null;
      Matcher var2 = TYPE_SUBTYPE.matcher(var0);
      MediaType var4;
      if(!var2.lookingAt()) {
         var4 = (MediaType)var5;
      } else {
         String var6 = var2.group(1).toLowerCase(Locale.US);
         String var7 = var2.group(2).toLowerCase(Locale.US);
         Matcher var8 = PARAMETER.matcher(var0);
         int var1 = var2.end();
         String var3 = null;

         while(true) {
            if(var1 >= var0.length()) {
               var4 = new MediaType(var0, var6, var7, var3);
               break;
            }

            var8.region(var1, var0.length());
            var4 = (MediaType)var5;
            if(!var8.lookingAt()) {
               break;
            }

            String var10 = var8.group(1);
            String var9 = var3;
            if(var10 != null) {
               if(!var10.equalsIgnoreCase("charset")) {
                  var9 = var3;
               } else {
                  var10 = var8.group(2);
                  if(var10 != null) {
                     var9 = var10;
                     if(var10.startsWith("'")) {
                        var9 = var10;
                        if(var10.endsWith("'")) {
                           var9 = var10;
                           if(var10.length() > 2) {
                              var9 = var10.substring(1, var10.length() - 1);
                           }
                        }
                     }
                  } else {
                     var9 = var8.group(3);
                  }

                  if(var3 != null) {
                     var4 = (MediaType)var5;
                     if(!var9.equalsIgnoreCase(var3)) {
                        break;
                     }
                  }
               }
            }

            var1 = var8.end();
            var3 = var9;
         }
      }

      return var4;
   }

   @Nullable
   public Charset charset() {
      return this.charset((Charset)null);
   }

   @Nullable
   public Charset charset(@Nullable Charset var1) {
      Charset var2 = var1;

      try {
         if(this.charset != null) {
            var2 = Charset.forName(this.charset);
         }
      } catch (IllegalArgumentException var3) {
         var2 = var1;
      }

      return var2;
   }

   public boolean equals(@Nullable Object var1) {
      boolean var2;
      if(var1 instanceof MediaType && ((MediaType)var1).mediaType.equals(this.mediaType)) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   public int hashCode() {
      return this.mediaType.hashCode();
   }

   public String subtype() {
      return this.subtype;
   }

   public String toString() {
      return this.mediaType;
   }

   public String type() {
      return this.type;
   }
}
