package io.intercom.okhttp3;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import javax.annotation.Nullable;

public interface Interceptor {
   Response intercept(Interceptor.Chain var1) throws IOException;

   public interface Chain {
      Call call();

      int connectTimeoutMillis();

      @Nullable
      Connection connection();

      Response proceed(Request var1) throws IOException;

      int readTimeoutMillis();

      Request request();

      Interceptor.Chain withConnectTimeout(int var1, TimeUnit var2);

      Interceptor.Chain withReadTimeout(int var1, TimeUnit var2);

      Interceptor.Chain withWriteTimeout(int var1, TimeUnit var2);

      int writeTimeoutMillis();
   }
}
