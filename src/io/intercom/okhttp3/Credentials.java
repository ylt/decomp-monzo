package io.intercom.okhttp3;

import io.intercom.a.f;
import io.intercom.okhttp3.internal.Util;
import java.nio.charset.Charset;

public final class Credentials {
   public static String basic(String var0, String var1) {
      return basic(var0, var1, Util.ISO_8859_1);
   }

   public static String basic(String var0, String var1, Charset var2) {
      var0 = f.a(var0 + ":" + var1, var2).b();
      return "Basic " + var0;
   }
}
