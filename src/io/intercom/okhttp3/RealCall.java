package io.intercom.okhttp3;

import io.intercom.okhttp3.internal.NamedRunnable;
import io.intercom.okhttp3.internal.cache.CacheInterceptor;
import io.intercom.okhttp3.internal.connection.ConnectInterceptor;
import io.intercom.okhttp3.internal.connection.RealConnection;
import io.intercom.okhttp3.internal.connection.StreamAllocation;
import io.intercom.okhttp3.internal.http.BridgeInterceptor;
import io.intercom.okhttp3.internal.http.CallServerInterceptor;
import io.intercom.okhttp3.internal.http.HttpCodec;
import io.intercom.okhttp3.internal.http.RealInterceptorChain;
import io.intercom.okhttp3.internal.http.RetryAndFollowUpInterceptor;
import io.intercom.okhttp3.internal.platform.Platform;
import java.io.IOException;
import java.util.ArrayList;

final class RealCall implements Call {
   final OkHttpClient client;
   private EventListener eventListener;
   private boolean executed;
   final boolean forWebSocket;
   final Request originalRequest;
   final RetryAndFollowUpInterceptor retryAndFollowUpInterceptor;

   private RealCall(OkHttpClient var1, Request var2, boolean var3) {
      this.client = var1;
      this.originalRequest = var2;
      this.forWebSocket = var3;
      this.retryAndFollowUpInterceptor = new RetryAndFollowUpInterceptor(var1, var3);
   }

   // $FF: synthetic method
   static EventListener access$000(RealCall var0) {
      return var0.eventListener;
   }

   private void captureCallStackTrace() {
      Object var1 = Platform.get().getStackTraceForCloseable("response.body().close()");
      this.retryAndFollowUpInterceptor.setCallStackTrace(var1);
   }

   static RealCall newRealCall(OkHttpClient var0, Request var1, boolean var2) {
      RealCall var3 = new RealCall(var0, var1, var2);
      var3.eventListener = var0.eventListenerFactory().create(var3);
      return var3;
   }

   public void cancel() {
      this.retryAndFollowUpInterceptor.cancel();
   }

   public RealCall clone() {
      return newRealCall(this.client, this.originalRequest, this.forWebSocket);
   }

   public void enqueue(Callback param1) {
      // $FF: Couldn't be decompiled
   }

   public Response execute() throws IOException {
      // $FF: Couldn't be decompiled
   }

   Response getResponseWithInterceptorChain() throws IOException {
      ArrayList var1 = new ArrayList();
      var1.addAll(this.client.interceptors());
      var1.add(this.retryAndFollowUpInterceptor);
      var1.add(new BridgeInterceptor(this.client.cookieJar()));
      var1.add(new CacheInterceptor(this.client.internalCache()));
      var1.add(new ConnectInterceptor(this.client));
      if(!this.forWebSocket) {
         var1.addAll(this.client.networkInterceptors());
      }

      var1.add(new CallServerInterceptor(this.forWebSocket));
      return (new RealInterceptorChain(var1, (StreamAllocation)null, (HttpCodec)null, (RealConnection)null, 0, this.originalRequest, this, this.eventListener, this.client.connectTimeoutMillis(), this.client.readTimeoutMillis(), this.client.writeTimeoutMillis())).proceed(this.originalRequest);
   }

   public boolean isCanceled() {
      return this.retryAndFollowUpInterceptor.isCanceled();
   }

   public boolean isExecuted() {
      synchronized(this){}

      boolean var1;
      try {
         var1 = this.executed;
      } finally {
         ;
      }

      return var1;
   }

   String redactedUrl() {
      return this.originalRequest.url().redact();
   }

   public Request request() {
      return this.originalRequest;
   }

   StreamAllocation streamAllocation() {
      return this.retryAndFollowUpInterceptor.streamAllocation();
   }

   String toLoggableString() {
      StringBuilder var2 = new StringBuilder();
      String var1;
      if(this.isCanceled()) {
         var1 = "canceled ";
      } else {
         var1 = "";
      }

      var2 = var2.append(var1);
      if(this.forWebSocket) {
         var1 = "web socket";
      } else {
         var1 = "call";
      }

      return var2.append(var1).append(" to ").append(this.redactedUrl()).toString();
   }

   final class AsyncCall extends NamedRunnable {
      private final Callback responseCallback;

      AsyncCall(Callback var2) {
         super("OkHttp %s", new Object[]{RealCall.this.redactedUrl()});
         this.responseCallback = var2;
      }

      protected void execute() {
         // $FF: Couldn't be decompiled
      }

      RealCall get() {
         return RealCall.this;
      }

      String host() {
         return RealCall.this.originalRequest.url().host();
      }

      Request request() {
         return RealCall.this.originalRequest;
      }
   }
}
