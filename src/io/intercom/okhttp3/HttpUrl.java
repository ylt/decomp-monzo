package io.intercom.okhttp3;

import io.intercom.a.c;
import io.intercom.okhttp3.internal.Util;
import io.intercom.okhttp3.internal.publicsuffix.PublicSuffixDatabase;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.Nullable;

public final class HttpUrl {
   static final String FORM_ENCODE_SET = " \"':;<=>@[]^`{}|/\\?#&!$(),~";
   static final String FRAGMENT_ENCODE_SET = "";
   static final String FRAGMENT_ENCODE_SET_URI = " \"#<>\\^`{|}";
   private static final char[] HEX_DIGITS = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
   static final String PASSWORD_ENCODE_SET = " \"':;<=>@[]^`{}|/\\?#";
   static final String PATH_SEGMENT_ENCODE_SET = " \"<>^`{}|/\\?#";
   static final String PATH_SEGMENT_ENCODE_SET_URI = "[]";
   static final String QUERY_COMPONENT_ENCODE_SET = " \"'<>#&=";
   static final String QUERY_COMPONENT_ENCODE_SET_URI = "\\^`{|}";
   static final String QUERY_ENCODE_SET = " \"'<>#";
   static final String USERNAME_ENCODE_SET = " \"':;<=>@[]^`{}|/\\?#";
   @Nullable
   private final String fragment;
   final String host;
   private final String password;
   private final List pathSegments;
   final int port;
   @Nullable
   private final List queryNamesAndValues;
   final String scheme;
   private final String url;
   private final String username;

   HttpUrl(HttpUrl.Builder var1) {
      Object var3 = null;
      super();
      this.scheme = var1.scheme;
      this.username = percentDecode(var1.encodedUsername, false);
      this.password = percentDecode(var1.encodedPassword, false);
      this.host = var1.host;
      this.port = var1.effectivePort();
      this.pathSegments = this.percentDecode(var1.encodedPathSegments, false);
      List var2;
      if(var1.encodedQueryNamesAndValues != null) {
         var2 = this.percentDecode(var1.encodedQueryNamesAndValues, true);
      } else {
         var2 = null;
      }

      this.queryNamesAndValues = var2;
      String var4 = (String)var3;
      if(var1.encodedFragment != null) {
         var4 = percentDecode(var1.encodedFragment, false);
      }

      this.fragment = var4;
      this.url = var1.toString();
   }

   static String canonicalize(String var0, int var1, int var2, String var3, boolean var4, boolean var5, boolean var6, boolean var7, Charset var8) {
      int var9 = var1;

      while(true) {
         if(var9 >= var2) {
            var0 = var0.substring(var1, var2);
            break;
         }

         int var10 = var0.codePointAt(var9);
         if(var10 < 32 || var10 == 127 || var10 >= 128 && var7 || var3.indexOf(var10) != -1 || var10 == 37 && (!var4 || var5 && !percentEncoded(var0, var9, var2)) || var10 == 43 && var6) {
            c var11 = new c();
            var11.a(var0, var1, var9);
            canonicalize(var11, var0, var9, var2, var3, var4, var5, var6, var7, var8);
            var0 = var11.r();
            break;
         }

         var9 += Character.charCount(var10);
      }

      return var0;
   }

   static String canonicalize(String var0, String var1, boolean var2, boolean var3, boolean var4, boolean var5) {
      return canonicalize(var0, 0, var0.length(), var1, var2, var3, var4, var5, (Charset)null);
   }

   static String canonicalize(String var0, String var1, boolean var2, boolean var3, boolean var4, boolean var5, Charset var6) {
      return canonicalize(var0, 0, var0.length(), var1, var2, var3, var4, var5, var6);
   }

   static void canonicalize(c var0, String var1, int var2, int var3, String var4, boolean var5, boolean var6, boolean var7, boolean var8, Charset var9) {
      c var14;
      for(c var12 = null; var2 < var3; var12 = var14) {
         int var10;
         label88: {
            var10 = var1.codePointAt(var2);
            if(var5) {
               var14 = var12;
               if(var10 == 9) {
                  break label88;
               }

               var14 = var12;
               if(var10 == 10) {
                  break label88;
               }

               var14 = var12;
               if(var10 == 12) {
                  break label88;
               }

               if(var10 == 13) {
                  var14 = var12;
                  break label88;
               }
            }

            if(var10 == 43 && var7) {
               String var15;
               if(var5) {
                  var15 = "+";
               } else {
                  var15 = "%2B";
               }

               var0.a(var15);
               var14 = var12;
            } else if(var10 < 32 || var10 == 127 || var10 >= 128 && var8 || var4.indexOf(var10) != -1 || var10 == 37 && (!var5 || var6 && !percentEncoded(var1, var2, var3))) {
               c var13 = var12;
               if(var12 == null) {
                  var13 = new c();
               }

               if(var9 != null && !var9.equals(Util.UTF_8)) {
                  var13.a(var1, var2, Character.charCount(var10) + var2, var9);
               } else {
                  var13.a(var10);
               }

               while(true) {
                  var14 = var13;
                  if(var13.f()) {
                     break;
                  }

                  int var11 = var13.i() & 255;
                  var0.b(37);
                  var0.b(HEX_DIGITS[var11 >> 4 & 15]);
                  var0.b(HEX_DIGITS[var11 & 15]);
               }
            } else {
               var0.a(var10);
               var14 = var12;
            }
         }

         var2 += Character.charCount(var10);
      }

   }

   public static int defaultPort(String var0) {
      short var1;
      if(var0.equals("http")) {
         var1 = 80;
      } else if(var0.equals("https")) {
         var1 = 443;
      } else {
         var1 = -1;
      }

      return var1;
   }

   @Nullable
   public static HttpUrl get(URI var0) {
      return parse(var0.toString());
   }

   @Nullable
   public static HttpUrl get(URL var0) {
      return parse(var0.toString());
   }

   static HttpUrl getChecked(String var0) throws MalformedURLException, UnknownHostException {
      HttpUrl.Builder var1 = new HttpUrl.Builder();
      HttpUrl.ParseResult var2 = var1.parse((HttpUrl)null, var0);
      switch(null.$SwitchMap$okhttp3$HttpUrl$Builder$ParseResult[var2.ordinal()]) {
      case 1:
         return var1.build();
      case 2:
         throw new UnknownHostException("Invalid host: " + var0);
      default:
         throw new MalformedURLException("Invalid URL: " + var2 + " for " + var0);
      }
   }

   static void namesAndValuesToQueryString(StringBuilder var0, List var1) {
      int var3 = var1.size();

      for(int var2 = 0; var2 < var3; var2 += 2) {
         String var5 = (String)var1.get(var2);
         String var4 = (String)var1.get(var2 + 1);
         if(var2 > 0) {
            var0.append('&');
         }

         var0.append(var5);
         if(var4 != null) {
            var0.append('=');
            var0.append(var4);
         }
      }

   }

   @Nullable
   public static HttpUrl parse(String var0) {
      HttpUrl var1 = null;
      HttpUrl.Builder var2 = new HttpUrl.Builder();
      if(var2.parse((HttpUrl)null, var0) == HttpUrl.ParseResult.SUCCESS) {
         var1 = var2.build();
      }

      return var1;
   }

   static void pathSegmentsToString(StringBuilder var0, List var1) {
      int var3 = var1.size();

      for(int var2 = 0; var2 < var3; ++var2) {
         var0.append('/');
         var0.append((String)var1.get(var2));
      }

   }

   static String percentDecode(String var0, int var1, int var2, boolean var3) {
      int var4 = var1;

      while(true) {
         if(var4 >= var2) {
            var0 = var0.substring(var1, var2);
            break;
         }

         char var5 = var0.charAt(var4);
         if(var5 == 37 || var5 == 43 && var3) {
            c var6 = new c();
            var6.a(var0, var1, var4);
            percentDecode(var6, var0, var4, var2, var3);
            var0 = var6.r();
            break;
         }

         ++var4;
      }

      return var0;
   }

   static String percentDecode(String var0, boolean var1) {
      return percentDecode(var0, 0, var0.length(), var1);
   }

   private List percentDecode(List var1, boolean var2) {
      int var4 = var1.size();
      ArrayList var6 = new ArrayList(var4);

      for(int var3 = 0; var3 < var4; ++var3) {
         String var5 = (String)var1.get(var3);
         if(var5 != null) {
            var5 = percentDecode(var5, var2);
         } else {
            var5 = null;
         }

         var6.add(var5);
      }

      return Collections.unmodifiableList(var6);
   }

   static void percentDecode(c var0, String var1, int var2, int var3, boolean var4) {
      int var5;
      for(; var2 < var3; var2 += Character.charCount(var5)) {
         var5 = var1.codePointAt(var2);
         if(var5 == 37 && var2 + 2 < var3) {
            int var6 = Util.decodeHexDigit(var1.charAt(var2 + 1));
            int var7 = Util.decodeHexDigit(var1.charAt(var2 + 2));
            if(var6 != -1 && var7 != -1) {
               var0.b((var6 << 4) + var7);
               var2 += 2;
               continue;
            }
         } else if(var5 == 43 && var4) {
            var0.b(32);
            continue;
         }

         var0.a(var5);
      }

   }

   static boolean percentEncoded(String var0, int var1, int var2) {
      boolean var3;
      if(var1 + 2 < var2 && var0.charAt(var1) == 37 && Util.decodeHexDigit(var0.charAt(var1 + 1)) != -1 && Util.decodeHexDigit(var0.charAt(var1 + 2)) != -1) {
         var3 = true;
      } else {
         var3 = false;
      }

      return var3;
   }

   static List queryStringToNamesAndValues(String var0) {
      ArrayList var4 = new ArrayList();

      int var2;
      for(int var1 = 0; var1 <= var0.length(); var1 = var2 + 1) {
         int var3 = var0.indexOf(38, var1);
         var2 = var3;
         if(var3 == -1) {
            var2 = var0.length();
         }

         var3 = var0.indexOf(61, var1);
         if(var3 != -1 && var3 <= var2) {
            var4.add(var0.substring(var1, var3));
            var4.add(var0.substring(var3 + 1, var2));
         } else {
            var4.add(var0.substring(var1, var2));
            var4.add((Object)null);
         }
      }

      return var4;
   }

   @Nullable
   public String encodedFragment() {
      String var2;
      if(this.fragment == null) {
         var2 = null;
      } else {
         int var1 = this.url.indexOf(35);
         var2 = this.url.substring(var1 + 1);
      }

      return var2;
   }

   public String encodedPassword() {
      String var3;
      if(this.password.isEmpty()) {
         var3 = "";
      } else {
         int var2 = this.url.indexOf(58, this.scheme.length() + 3);
         int var1 = this.url.indexOf(64);
         var3 = this.url.substring(var2 + 1, var1);
      }

      return var3;
   }

   public String encodedPath() {
      int var2 = this.url.indexOf(47, this.scheme.length() + 3);
      int var1 = Util.delimiterOffset(this.url, var2, this.url.length(), "?#");
      return this.url.substring(var2, var1);
   }

   public List encodedPathSegments() {
      int var1 = this.url.indexOf(47, this.scheme.length() + 3);
      int var2 = Util.delimiterOffset(this.url, var1, this.url.length(), "?#");
      ArrayList var4 = new ArrayList();

      while(var1 < var2) {
         int var3 = var1 + 1;
         var1 = Util.delimiterOffset(this.url, var3, var2, '/');
         var4.add(this.url.substring(var3, var1));
      }

      return var4;
   }

   @Nullable
   public String encodedQuery() {
      String var3;
      if(this.queryNamesAndValues == null) {
         var3 = null;
      } else {
         int var1 = this.url.indexOf(63) + 1;
         int var2 = Util.delimiterOffset(this.url, var1 + 1, this.url.length(), '#');
         var3 = this.url.substring(var1, var2);
      }

      return var3;
   }

   public String encodedUsername() {
      String var3;
      if(this.username.isEmpty()) {
         var3 = "";
      } else {
         int var1 = this.scheme.length() + 3;
         int var2 = Util.delimiterOffset(this.url, var1, this.url.length(), ":@");
         var3 = this.url.substring(var1, var2);
      }

      return var3;
   }

   public boolean equals(@Nullable Object var1) {
      boolean var2;
      if(var1 instanceof HttpUrl && ((HttpUrl)var1).url.equals(this.url)) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   @Nullable
   public String fragment() {
      return this.fragment;
   }

   public int hashCode() {
      return this.url.hashCode();
   }

   public String host() {
      return this.host;
   }

   public boolean isHttps() {
      return this.scheme.equals("https");
   }

   public HttpUrl.Builder newBuilder() {
      HttpUrl.Builder var2 = new HttpUrl.Builder();
      var2.scheme = this.scheme;
      var2.encodedUsername = this.encodedUsername();
      var2.encodedPassword = this.encodedPassword();
      var2.host = this.host;
      int var1;
      if(this.port != defaultPort(this.scheme)) {
         var1 = this.port;
      } else {
         var1 = -1;
      }

      var2.port = var1;
      var2.encodedPathSegments.clear();
      var2.encodedPathSegments.addAll(this.encodedPathSegments());
      var2.encodedQuery(this.encodedQuery());
      var2.encodedFragment = this.encodedFragment();
      return var2;
   }

   @Nullable
   public HttpUrl.Builder newBuilder(String var1) {
      HttpUrl.Builder var2 = new HttpUrl.Builder();
      HttpUrl.Builder var3;
      if(var2.parse(this, var1) == HttpUrl.ParseResult.SUCCESS) {
         var3 = var2;
      } else {
         var3 = null;
      }

      return var3;
   }

   public String password() {
      return this.password;
   }

   public List pathSegments() {
      return this.pathSegments;
   }

   public int pathSize() {
      return this.pathSegments.size();
   }

   public int port() {
      return this.port;
   }

   @Nullable
   public String query() {
      String var1;
      if(this.queryNamesAndValues == null) {
         var1 = null;
      } else {
         StringBuilder var2 = new StringBuilder();
         namesAndValuesToQueryString(var2, this.queryNamesAndValues);
         var1 = var2.toString();
      }

      return var1;
   }

   @Nullable
   public String queryParameter(String var1) {
      Object var5 = null;
      String var4;
      if(this.queryNamesAndValues == null) {
         var4 = (String)var5;
      } else {
         int var2 = 0;
         int var3 = this.queryNamesAndValues.size();

         while(true) {
            var4 = (String)var5;
            if(var2 >= var3) {
               break;
            }

            if(var1.equals(this.queryNamesAndValues.get(var2))) {
               var4 = (String)this.queryNamesAndValues.get(var2 + 1);
               break;
            }

            var2 += 2;
         }
      }

      return var4;
   }

   public String queryParameterName(int var1) {
      if(this.queryNamesAndValues == null) {
         throw new IndexOutOfBoundsException();
      } else {
         return (String)this.queryNamesAndValues.get(var1 * 2);
      }
   }

   public Set queryParameterNames() {
      Set var3;
      if(this.queryNamesAndValues == null) {
         var3 = Collections.emptySet();
      } else {
         LinkedHashSet var4 = new LinkedHashSet();
         int var1 = 0;

         for(int var2 = this.queryNamesAndValues.size(); var1 < var2; var1 += 2) {
            var4.add(this.queryNamesAndValues.get(var1));
         }

         var3 = Collections.unmodifiableSet(var4);
      }

      return var3;
   }

   public String queryParameterValue(int var1) {
      if(this.queryNamesAndValues == null) {
         throw new IndexOutOfBoundsException();
      } else {
         return (String)this.queryNamesAndValues.get(var1 * 2 + 1);
      }
   }

   public List queryParameterValues(String var1) {
      List var5;
      if(this.queryNamesAndValues == null) {
         var5 = Collections.emptyList();
      } else {
         ArrayList var4 = new ArrayList();
         int var2 = 0;

         for(int var3 = this.queryNamesAndValues.size(); var2 < var3; var2 += 2) {
            if(var1.equals(this.queryNamesAndValues.get(var2))) {
               var4.add(this.queryNamesAndValues.get(var2 + 1));
            }
         }

         var5 = Collections.unmodifiableList(var4);
      }

      return var5;
   }

   public int querySize() {
      int var1;
      if(this.queryNamesAndValues != null) {
         var1 = this.queryNamesAndValues.size() / 2;
      } else {
         var1 = 0;
      }

      return var1;
   }

   public String redact() {
      return this.newBuilder("/...").username("").password("").build().toString();
   }

   @Nullable
   public HttpUrl resolve(String var1) {
      HttpUrl.Builder var2 = this.newBuilder(var1);
      HttpUrl var3;
      if(var2 != null) {
         var3 = var2.build();
      } else {
         var3 = null;
      }

      return var3;
   }

   public String scheme() {
      return this.scheme;
   }

   public String toString() {
      return this.url;
   }

   @Nullable
   public String topPrivateDomain() {
      String var1;
      if(Util.verifyAsIpAddress(this.host)) {
         var1 = null;
      } else {
         var1 = PublicSuffixDatabase.get().getEffectiveTldPlusOne(this.host);
      }

      return var1;
   }

   public URI uri() {
      String var3 = this.newBuilder().reencodeForUri().toString();

      URI var1;
      try {
         var1 = new URI(var3);
      } catch (URISyntaxException var5) {
         try {
            var1 = URI.create(var3.replaceAll("[\\u0000-\\u001F\\u007F-\\u009F\\p{javaWhitespace}]", ""));
         } catch (Exception var4) {
            throw new RuntimeException(var5);
         }
      }

      return var1;
   }

   public URL url() {
      try {
         URL var1 = new URL(this.url);
         return var1;
      } catch (MalformedURLException var2) {
         throw new RuntimeException(var2);
      }
   }

   public String username() {
      return this.username;
   }

   public static final class Builder {
      @Nullable
      String encodedFragment;
      String encodedPassword = "";
      final List encodedPathSegments = new ArrayList();
      @Nullable
      List encodedQueryNamesAndValues;
      String encodedUsername = "";
      @Nullable
      String host;
      int port = -1;
      @Nullable
      String scheme;

      public Builder() {
         this.encodedPathSegments.add("");
      }

      private HttpUrl.Builder addPathSegments(String var1, boolean var2) {
         int var3 = 0;

         int var4;
         do {
            var4 = Util.delimiterOffset(var1, var3, var1.length(), "/\\");
            boolean var5;
            if(var4 < var1.length()) {
               var5 = true;
            } else {
               var5 = false;
            }

            this.push(var1, var3, var4, var5, var2);
            ++var4;
            var3 = var4;
         } while(var4 <= var1.length());

         return this;
      }

      private static String canonicalizeHost(String var0, int var1, int var2) {
         return Util.canonicalizeHost(HttpUrl.percentDecode(var0, var1, var2, false));
      }

      private boolean isDot(String var1) {
         boolean var2;
         if(!var1.equals(".") && !var1.equalsIgnoreCase("%2e")) {
            var2 = false;
         } else {
            var2 = true;
         }

         return var2;
      }

      private boolean isDotDot(String var1) {
         boolean var2;
         if(!var1.equals("..") && !var1.equalsIgnoreCase("%2e.") && !var1.equalsIgnoreCase(".%2e") && !var1.equalsIgnoreCase("%2e%2e")) {
            var2 = false;
         } else {
            var2 = true;
         }

         return var2;
      }

      private static int parsePort(String var0, int var1, int var2) {
         try {
            var1 = Integer.parseInt(HttpUrl.canonicalize(var0, var1, var2, "", false, false, false, true, (Charset)null));
         } catch (NumberFormatException var3) {
            var1 = -1;
            return var1;
         }

         if(var1 <= 0 || var1 > '\uffff') {
            var1 = -1;
         }

         return var1;
      }

      private void pop() {
         if(((String)this.encodedPathSegments.remove(this.encodedPathSegments.size() - 1)).isEmpty() && !this.encodedPathSegments.isEmpty()) {
            this.encodedPathSegments.set(this.encodedPathSegments.size() - 1, "");
         } else {
            this.encodedPathSegments.add("");
         }

      }

      private static int portColonOffset(String var0, int var1, int var2) {
         while(true) {
            int var3;
            label22: {
               int var4;
               if(var1 < var2) {
                  var3 = var1;
                  var4 = var1;
                  switch(var0.charAt(var1)) {
                  case ':':
                     break;
                  case '[':
                     do {
                        var1 = var3 + 1;
                        var3 = var1;
                        if(var1 >= var2) {
                           break label22;
                        }

                        var3 = var1;
                     } while(var0.charAt(var1) != 93);

                     var3 = var1;
                     break label22;
                  default:
                     var3 = var1;
                     break label22;
                  }
               } else {
                  var4 = var2;
               }

               return var4;
            }

            var1 = var3 + 1;
         }
      }

      private void push(String var1, int var2, int var3, boolean var4, boolean var5) {
         var1 = HttpUrl.canonicalize(var1, var2, var3, " \"<>^`{}|/\\?#", var5, false, false, true, (Charset)null);
         if(!this.isDot(var1)) {
            if(this.isDotDot(var1)) {
               this.pop();
            } else {
               if(((String)this.encodedPathSegments.get(this.encodedPathSegments.size() - 1)).isEmpty()) {
                  this.encodedPathSegments.set(this.encodedPathSegments.size() - 1, var1);
               } else {
                  this.encodedPathSegments.add(var1);
               }

               if(var4) {
                  this.encodedPathSegments.add("");
               }
            }
         }

      }

      private void removeAllCanonicalQueryParameters(String var1) {
         for(int var2 = this.encodedQueryNamesAndValues.size() - 2; var2 >= 0; var2 -= 2) {
            if(var1.equals(this.encodedQueryNamesAndValues.get(var2))) {
               this.encodedQueryNamesAndValues.remove(var2 + 1);
               this.encodedQueryNamesAndValues.remove(var2);
               if(this.encodedQueryNamesAndValues.isEmpty()) {
                  this.encodedQueryNamesAndValues = null;
                  break;
               }
            }
         }

      }

      private void resolvePath(String var1, int var2, int var3) {
         if(var2 != var3) {
            char var4 = var1.charAt(var2);
            if(var4 != 47 && var4 != 92) {
               this.encodedPathSegments.set(this.encodedPathSegments.size() - 1, "");
            } else {
               this.encodedPathSegments.clear();
               this.encodedPathSegments.add("");
               ++var2;
            }

            while(var2 < var3) {
               int var6 = Util.delimiterOffset(var1, var2, var3, "/\\");
               boolean var5;
               if(var6 < var3) {
                  var5 = true;
               } else {
                  var5 = false;
               }

               this.push(var1, var2, var6, var5, true);
               var2 = var6;
               if(var5) {
                  var2 = var6 + 1;
               }
            }
         }

      }

      private static int schemeDelimiterOffset(String var0, int var1, int var2) {
         byte var4 = -1;
         int var3;
         if(var2 - var1 < 2) {
            var3 = var4;
         } else {
            char var5 = var0.charAt(var1);
            if(var5 < 97 || var5 > 122) {
               var3 = var4;
               if(var5 < 65) {
                  return var3;
               }

               var3 = var4;
               if(var5 > 90) {
                  return var3;
               }
            }

            ++var1;

            while(true) {
               var3 = var4;
               if(var1 >= var2) {
                  break;
               }

               var5 = var0.charAt(var1);
               if((var5 < 97 || var5 > 122) && (var5 < 65 || var5 > 90) && (var5 < 48 || var5 > 57) && var5 != 43 && var5 != 45 && var5 != 46) {
                  var3 = var4;
                  if(var5 == 58) {
                     var3 = var1;
                  }
                  break;
               }

               ++var1;
            }
         }

         return var3;
      }

      private static int slashCount(String var0, int var1, int var2) {
         int var3;
         for(var3 = 0; var1 < var2; ++var1) {
            char var4 = var0.charAt(var1);
            if(var4 != 92 && var4 != 47) {
               break;
            }

            ++var3;
         }

         return var3;
      }

      public HttpUrl.Builder addEncodedPathSegment(String var1) {
         if(var1 == null) {
            throw new NullPointerException("encodedPathSegment == null");
         } else {
            this.push(var1, 0, var1.length(), false, true);
            return this;
         }
      }

      public HttpUrl.Builder addEncodedPathSegments(String var1) {
         if(var1 == null) {
            throw new NullPointerException("encodedPathSegments == null");
         } else {
            return this.addPathSegments(var1, true);
         }
      }

      public HttpUrl.Builder addEncodedQueryParameter(String var1, @Nullable String var2) {
         if(var1 == null) {
            throw new NullPointerException("encodedName == null");
         } else {
            if(this.encodedQueryNamesAndValues == null) {
               this.encodedQueryNamesAndValues = new ArrayList();
            }

            this.encodedQueryNamesAndValues.add(HttpUrl.canonicalize(var1, " \"'<>#&=", true, false, true, true));
            List var3 = this.encodedQueryNamesAndValues;
            if(var2 != null) {
               var1 = HttpUrl.canonicalize(var2, " \"'<>#&=", true, false, true, true);
            } else {
               var1 = null;
            }

            var3.add(var1);
            return this;
         }
      }

      public HttpUrl.Builder addPathSegment(String var1) {
         if(var1 == null) {
            throw new NullPointerException("pathSegment == null");
         } else {
            this.push(var1, 0, var1.length(), false, false);
            return this;
         }
      }

      public HttpUrl.Builder addPathSegments(String var1) {
         if(var1 == null) {
            throw new NullPointerException("pathSegments == null");
         } else {
            return this.addPathSegments(var1, false);
         }
      }

      public HttpUrl.Builder addQueryParameter(String var1, @Nullable String var2) {
         if(var1 == null) {
            throw new NullPointerException("name == null");
         } else {
            if(this.encodedQueryNamesAndValues == null) {
               this.encodedQueryNamesAndValues = new ArrayList();
            }

            this.encodedQueryNamesAndValues.add(HttpUrl.canonicalize(var1, " \"'<>#&=", false, false, true, true));
            List var3 = this.encodedQueryNamesAndValues;
            if(var2 != null) {
               var1 = HttpUrl.canonicalize(var2, " \"'<>#&=", false, false, true, true);
            } else {
               var1 = null;
            }

            var3.add(var1);
            return this;
         }
      }

      public HttpUrl build() {
         if(this.scheme == null) {
            throw new IllegalStateException("scheme == null");
         } else if(this.host == null) {
            throw new IllegalStateException("host == null");
         } else {
            return new HttpUrl(this);
         }
      }

      int effectivePort() {
         int var1;
         if(this.port != -1) {
            var1 = this.port;
         } else {
            var1 = HttpUrl.defaultPort(this.scheme);
         }

         return var1;
      }

      public HttpUrl.Builder encodedFragment(@Nullable String var1) {
         if(var1 != null) {
            var1 = HttpUrl.canonicalize(var1, "", true, false, false, false);
         } else {
            var1 = null;
         }

         this.encodedFragment = var1;
         return this;
      }

      public HttpUrl.Builder encodedPassword(String var1) {
         if(var1 == null) {
            throw new NullPointerException("encodedPassword == null");
         } else {
            this.encodedPassword = HttpUrl.canonicalize(var1, " \"':;<=>@[]^`{}|/\\?#", true, false, false, true);
            return this;
         }
      }

      public HttpUrl.Builder encodedPath(String var1) {
         if(var1 == null) {
            throw new NullPointerException("encodedPath == null");
         } else if(!var1.startsWith("/")) {
            throw new IllegalArgumentException("unexpected encodedPath: " + var1);
         } else {
            this.resolvePath(var1, 0, var1.length());
            return this;
         }
      }

      public HttpUrl.Builder encodedQuery(@Nullable String var1) {
         List var2;
         if(var1 != null) {
            var2 = HttpUrl.queryStringToNamesAndValues(HttpUrl.canonicalize(var1, " \"'<>#", true, false, true, true));
         } else {
            var2 = null;
         }

         this.encodedQueryNamesAndValues = var2;
         return this;
      }

      public HttpUrl.Builder encodedUsername(String var1) {
         if(var1 == null) {
            throw new NullPointerException("encodedUsername == null");
         } else {
            this.encodedUsername = HttpUrl.canonicalize(var1, " \"':;<=>@[]^`{}|/\\?#", true, false, false, true);
            return this;
         }
      }

      public HttpUrl.Builder fragment(@Nullable String var1) {
         if(var1 != null) {
            var1 = HttpUrl.canonicalize(var1, "", false, false, false, false);
         } else {
            var1 = null;
         }

         this.encodedFragment = var1;
         return this;
      }

      public HttpUrl.Builder host(String var1) {
         if(var1 == null) {
            throw new NullPointerException("host == null");
         } else {
            String var2 = canonicalizeHost(var1, 0, var1.length());
            if(var2 == null) {
               throw new IllegalArgumentException("unexpected host: " + var1);
            } else {
               this.host = var2;
               return this;
            }
         }
      }

      HttpUrl.ParseResult parse(@Nullable HttpUrl var1, String var2) {
         int var3 = Util.skipLeadingAsciiWhitespace(var2, 0, var2.length());
         int var8 = Util.skipTrailingAsciiWhitespace(var2, var3, var2.length());
         HttpUrl.ParseResult var10;
         if(schemeDelimiterOffset(var2, var3, var8) != -1) {
            if(var2.regionMatches(true, var3, "https:", 0, 6)) {
               this.scheme = "https";
               var3 += "https:".length();
            } else {
               if(!var2.regionMatches(true, var3, "http:", 0, 5)) {
                  var10 = HttpUrl.ParseResult.UNSUPPORTED_SCHEME;
                  return var10;
               }

               this.scheme = "http";
               var3 += "http:".length();
            }
         } else {
            if(var1 == null) {
               var10 = HttpUrl.ParseResult.MISSING_SCHEME;
               return var10;
            }

            this.scheme = var1.scheme;
         }

         int var6 = slashCount(var2, var3, var8);
         int var13;
         if(var6 < 2 && var1 != null && var1.scheme.equals(this.scheme)) {
            label80: {
               this.encodedUsername = var1.encodedUsername();
               this.encodedPassword = var1.encodedPassword();
               this.host = var1.host;
               this.port = var1.port;
               this.encodedPathSegments.clear();
               this.encodedPathSegments.addAll(var1.encodedPathSegments());
               if(var3 != var8) {
                  var13 = var3;
                  if(var2.charAt(var3) != 35) {
                     break label80;
                  }
               }

               this.encodedQuery(var1.encodedQuery());
               var13 = var3;
            }
         } else {
            boolean var5 = false;
            boolean var4 = false;
            var6 += var3;
            boolean var12 = var5;
            int var14 = var6;

            label74:
            while(true) {
               int var7 = Util.delimiterOffset(var2, var14, var8, "@/\\?#");
               if(var7 != var8) {
                  var6 = var2.charAt(var7);
               } else {
                  var6 = -1;
               }

               boolean var15;
               switch(var6) {
               case -1:
               case 35:
               case 47:
               case 63:
               case 92:
                  var3 = portColonOffset(var2, var14, var7);
                  if(var3 + 1 < var7) {
                     this.host = canonicalizeHost(var2, var14, var3);
                     this.port = parsePort(var2, var3 + 1, var7);
                     if(this.port == -1) {
                        var10 = HttpUrl.ParseResult.INVALID_PORT;
                        return var10;
                     }
                  } else {
                     this.host = canonicalizeHost(var2, var14, var3);
                     this.port = HttpUrl.defaultPort(this.scheme);
                  }

                  if(this.host == null) {
                     var10 = HttpUrl.ParseResult.INVALID_HOST;
                     return var10;
                  }

                  var13 = var7;
                  break label74;
               case 64:
                  if(!var12) {
                     var6 = Util.delimiterOffset(var2, var14, var7, ':');
                     String var9 = HttpUrl.canonicalize(var2, var14, var6, " \"':;<=>@[]^`{}|/\\?#", true, false, false, true, (Charset)null);
                     String var11 = var9;
                     if(var4) {
                        var11 = this.encodedUsername + "%40" + var9;
                     }

                     this.encodedUsername = var11;
                     if(var6 != var7) {
                        var12 = true;
                        this.encodedPassword = HttpUrl.canonicalize(var2, var6 + 1, var7, " \"':;<=>@[]^`{}|/\\?#", true, false, false, true, (Charset)null);
                     }

                     var4 = true;
                  } else {
                     this.encodedPassword = this.encodedPassword + "%40" + HttpUrl.canonicalize(var2, var14, var7, " \"':;<=>@[]^`{}|/\\?#", true, false, false, true, (Charset)null);
                  }

                  var6 = var7 + 1;
                  var5 = var12;
                  var3 = var6;
                  break;
               default:
                  var15 = var12;
                  var3 = var14;
                  var5 = var15;
               }

               var15 = var5;
               var14 = var3;
               var12 = var15;
            }
         }

         var3 = Util.delimiterOffset(var2, var13, var8, "?#");
         this.resolvePath(var2, var13, var3);
         if(var3 < var8 && var2.charAt(var3) == 63) {
            var13 = Util.delimiterOffset(var2, var3, var8, '#');
            this.encodedQueryNamesAndValues = HttpUrl.queryStringToNamesAndValues(HttpUrl.canonicalize(var2, var3 + 1, var13, " \"'<>#", true, false, true, true, (Charset)null));
            var3 = var13;
         }

         if(var3 < var8 && var2.charAt(var3) == 35) {
            this.encodedFragment = HttpUrl.canonicalize(var2, var3 + 1, var8, "", true, false, false, false, (Charset)null);
         }

         var10 = HttpUrl.ParseResult.SUCCESS;
         return var10;
      }

      public HttpUrl.Builder password(String var1) {
         if(var1 == null) {
            throw new NullPointerException("password == null");
         } else {
            this.encodedPassword = HttpUrl.canonicalize(var1, " \"':;<=>@[]^`{}|/\\?#", false, false, false, true);
            return this;
         }
      }

      public HttpUrl.Builder port(int var1) {
         if(var1 > 0 && var1 <= '\uffff') {
            this.port = var1;
            return this;
         } else {
            throw new IllegalArgumentException("unexpected port: " + var1);
         }
      }

      public HttpUrl.Builder query(@Nullable String var1) {
         List var2;
         if(var1 != null) {
            var2 = HttpUrl.queryStringToNamesAndValues(HttpUrl.canonicalize(var1, " \"'<>#", false, false, true, true));
         } else {
            var2 = null;
         }

         this.encodedQueryNamesAndValues = var2;
         return this;
      }

      HttpUrl.Builder reencodeForUri() {
         int var2 = this.encodedPathSegments.size();

         int var1;
         String var3;
         for(var1 = 0; var1 < var2; ++var1) {
            var3 = (String)this.encodedPathSegments.get(var1);
            this.encodedPathSegments.set(var1, HttpUrl.canonicalize(var3, "[]", true, true, false, true));
         }

         if(this.encodedQueryNamesAndValues != null) {
            var2 = this.encodedQueryNamesAndValues.size();

            for(var1 = 0; var1 < var2; ++var1) {
               var3 = (String)this.encodedQueryNamesAndValues.get(var1);
               if(var3 != null) {
                  this.encodedQueryNamesAndValues.set(var1, HttpUrl.canonicalize(var3, "\\^`{|}", true, true, true, true));
               }
            }
         }

         if(this.encodedFragment != null) {
            this.encodedFragment = HttpUrl.canonicalize(this.encodedFragment, " \"#<>\\^`{|}", true, true, false, false);
         }

         return this;
      }

      public HttpUrl.Builder removeAllEncodedQueryParameters(String var1) {
         if(var1 == null) {
            throw new NullPointerException("encodedName == null");
         } else {
            if(this.encodedQueryNamesAndValues != null) {
               this.removeAllCanonicalQueryParameters(HttpUrl.canonicalize(var1, " \"'<>#&=", true, false, true, true));
            }

            return this;
         }
      }

      public HttpUrl.Builder removeAllQueryParameters(String var1) {
         if(var1 == null) {
            throw new NullPointerException("name == null");
         } else {
            if(this.encodedQueryNamesAndValues != null) {
               this.removeAllCanonicalQueryParameters(HttpUrl.canonicalize(var1, " \"'<>#&=", false, false, true, true));
            }

            return this;
         }
      }

      public HttpUrl.Builder removePathSegment(int var1) {
         this.encodedPathSegments.remove(var1);
         if(this.encodedPathSegments.isEmpty()) {
            this.encodedPathSegments.add("");
         }

         return this;
      }

      public HttpUrl.Builder scheme(String var1) {
         if(var1 == null) {
            throw new NullPointerException("scheme == null");
         } else {
            if(var1.equalsIgnoreCase("http")) {
               this.scheme = "http";
            } else {
               if(!var1.equalsIgnoreCase("https")) {
                  throw new IllegalArgumentException("unexpected scheme: " + var1);
               }

               this.scheme = "https";
            }

            return this;
         }
      }

      public HttpUrl.Builder setEncodedPathSegment(int var1, String var2) {
         if(var2 == null) {
            throw new NullPointerException("encodedPathSegment == null");
         } else {
            String var3 = HttpUrl.canonicalize(var2, 0, var2.length(), " \"<>^`{}|/\\?#", true, false, false, true, (Charset)null);
            this.encodedPathSegments.set(var1, var3);
            if(!this.isDot(var3) && !this.isDotDot(var3)) {
               return this;
            } else {
               throw new IllegalArgumentException("unexpected path segment: " + var2);
            }
         }
      }

      public HttpUrl.Builder setEncodedQueryParameter(String var1, @Nullable String var2) {
         this.removeAllEncodedQueryParameters(var1);
         this.addEncodedQueryParameter(var1, var2);
         return this;
      }

      public HttpUrl.Builder setPathSegment(int var1, String var2) {
         if(var2 == null) {
            throw new NullPointerException("pathSegment == null");
         } else {
            String var3 = HttpUrl.canonicalize(var2, 0, var2.length(), " \"<>^`{}|/\\?#", false, false, false, true, (Charset)null);
            if(!this.isDot(var3) && !this.isDotDot(var3)) {
               this.encodedPathSegments.set(var1, var3);
               return this;
            } else {
               throw new IllegalArgumentException("unexpected path segment: " + var2);
            }
         }
      }

      public HttpUrl.Builder setQueryParameter(String var1, @Nullable String var2) {
         this.removeAllQueryParameters(var1);
         this.addQueryParameter(var1, var2);
         return this;
      }

      public String toString() {
         StringBuilder var2 = new StringBuilder();
         var2.append(this.scheme);
         var2.append("://");
         if(!this.encodedUsername.isEmpty() || !this.encodedPassword.isEmpty()) {
            var2.append(this.encodedUsername);
            if(!this.encodedPassword.isEmpty()) {
               var2.append(':');
               var2.append(this.encodedPassword);
            }

            var2.append('@');
         }

         if(this.host.indexOf(58) != -1) {
            var2.append('[');
            var2.append(this.host);
            var2.append(']');
         } else {
            var2.append(this.host);
         }

         int var1 = this.effectivePort();
         if(var1 != HttpUrl.defaultPort(this.scheme)) {
            var2.append(':');
            var2.append(var1);
         }

         HttpUrl.pathSegmentsToString(var2, this.encodedPathSegments);
         if(this.encodedQueryNamesAndValues != null) {
            var2.append('?');
            HttpUrl.namesAndValuesToQueryString(var2, this.encodedQueryNamesAndValues);
         }

         if(this.encodedFragment != null) {
            var2.append('#');
            var2.append(this.encodedFragment);
         }

         return var2.toString();
      }

      public HttpUrl.Builder username(String var1) {
         if(var1 == null) {
            throw new NullPointerException("username == null");
         } else {
            this.encodedUsername = HttpUrl.canonicalize(var1, " \"':;<=>@[]^`{}|/\\?#", false, false, false, true);
            return this;
         }
      }
   }

   static enum ParseResult {
      INVALID_HOST,
      INVALID_PORT,
      MISSING_SCHEME,
      SUCCESS,
      UNSUPPORTED_SCHEME;
   }
}
