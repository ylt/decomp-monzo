package io.intercom.okhttp3;

import io.intercom.a.c;
import io.intercom.a.d;
import io.intercom.a.f;
import io.intercom.okhttp3.internal.Util;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.annotation.Nullable;

public final class MultipartBody extends RequestBody {
   public static final MediaType ALTERNATIVE = MediaType.parse("multipart/alternative");
   private static final byte[] COLONSPACE = new byte[]{58, 32};
   private static final byte[] CRLF = new byte[]{13, 10};
   private static final byte[] DASHDASH = new byte[]{45, 45};
   public static final MediaType DIGEST = MediaType.parse("multipart/digest");
   public static final MediaType FORM = MediaType.parse("multipart/form-data");
   public static final MediaType MIXED = MediaType.parse("multipart/mixed");
   public static final MediaType PARALLEL = MediaType.parse("multipart/parallel");
   private final f boundary;
   private long contentLength = -1L;
   private final MediaType contentType;
   private final MediaType originalType;
   private final List parts;

   MultipartBody(f var1, MediaType var2, List var3) {
      this.boundary = var1;
      this.originalType = var2;
      this.contentType = MediaType.parse(var2 + "; boundary=" + var1.a());
      this.parts = Util.immutableList(var3);
   }

   static StringBuilder appendQuotedString(StringBuilder var0, String var1) {
      var0.append('"');
      int var3 = 0;

      for(int var4 = var1.length(); var3 < var4; ++var3) {
         char var2 = var1.charAt(var3);
         switch(var2) {
         case '\n':
            var0.append("%0A");
            break;
         case '\r':
            var0.append("%0D");
            break;
         case '"':
            var0.append("%22");
            break;
         default:
            var0.append(var2);
         }
      }

      var0.append('"');
      return var0;
   }

   private long writeOrCountBytes(@Nullable d var1, boolean var2) throws IOException {
      long var7 = 0L;
      Object var11;
      if(var2) {
         var1 = new c();
         var11 = var1;
      } else {
         var11 = null;
      }

      int var5 = this.parts.size();
      int var3 = 0;

      long var9;
      while(true) {
         if(var3 >= var5) {
            ((d)var1).c(DASHDASH);
            ((d)var1).b(this.boundary);
            ((d)var1).c(DASHDASH);
            ((d)var1).c(CRLF);
            var9 = var7;
            if(var2) {
               var9 = var7 + ((c)var11).a();
               ((c)var11).u();
            }
            break;
         }

         MultipartBody.Part var12 = (MultipartBody.Part)this.parts.get(var3);
         Headers var13 = var12.headers;
         RequestBody var14 = var12.body;
         ((d)var1).c(DASHDASH);
         ((d)var1).b(this.boundary);
         ((d)var1).c(CRLF);
         if(var13 != null) {
            int var4 = 0;

            for(int var6 = var13.size(); var4 < var6; ++var4) {
               ((d)var1).b(var13.name(var4)).c(COLONSPACE).b(var13.value(var4)).c(CRLF);
            }
         }

         MediaType var15 = var14.contentType();
         if(var15 != null) {
            ((d)var1).b("Content-Type: ").b(var15.toString()).c(CRLF);
         }

         var9 = var14.contentLength();
         if(var9 != -1L) {
            ((d)var1).b("Content-Length: ").n(var9).c(CRLF);
         } else if(var2) {
            ((c)var11).u();
            var9 = -1L;
            break;
         }

         ((d)var1).c(CRLF);
         if(var2) {
            var7 += var9;
         } else {
            var14.writeTo((d)var1);
         }

         ((d)var1).c(CRLF);
         ++var3;
      }

      return var9;
   }

   public String boundary() {
      return this.boundary.a();
   }

   public long contentLength() throws IOException {
      long var1 = this.contentLength;
      if(var1 == -1L) {
         var1 = this.writeOrCountBytes((d)null, true);
         this.contentLength = var1;
      }

      return var1;
   }

   public MediaType contentType() {
      return this.contentType;
   }

   public MultipartBody.Part part(int var1) {
      return (MultipartBody.Part)this.parts.get(var1);
   }

   public List parts() {
      return this.parts;
   }

   public int size() {
      return this.parts.size();
   }

   public MediaType type() {
      return this.originalType;
   }

   public void writeTo(d var1) throws IOException {
      this.writeOrCountBytes(var1, false);
   }

   public static final class Builder {
      private final f boundary;
      private final List parts;
      private MediaType type;

      public Builder() {
         this(UUID.randomUUID().toString());
      }

      public Builder(String var1) {
         this.type = MultipartBody.MIXED;
         this.parts = new ArrayList();
         this.boundary = f.a(var1);
      }

      public MultipartBody.Builder addFormDataPart(String var1, String var2) {
         return this.addPart(MultipartBody.Part.createFormData(var1, var2));
      }

      public MultipartBody.Builder addFormDataPart(String var1, @Nullable String var2, RequestBody var3) {
         return this.addPart(MultipartBody.Part.createFormData(var1, var2, var3));
      }

      public MultipartBody.Builder addPart(@Nullable Headers var1, RequestBody var2) {
         return this.addPart(MultipartBody.Part.create(var1, var2));
      }

      public MultipartBody.Builder addPart(MultipartBody.Part var1) {
         if(var1 == null) {
            throw new NullPointerException("part == null");
         } else {
            this.parts.add(var1);
            return this;
         }
      }

      public MultipartBody.Builder addPart(RequestBody var1) {
         return this.addPart(MultipartBody.Part.create(var1));
      }

      public MultipartBody build() {
         if(this.parts.isEmpty()) {
            throw new IllegalStateException("Multipart body must have at least one part.");
         } else {
            return new MultipartBody(this.boundary, this.type, this.parts);
         }
      }

      public MultipartBody.Builder setType(MediaType var1) {
         if(var1 == null) {
            throw new NullPointerException("type == null");
         } else if(!var1.type().equals("multipart")) {
            throw new IllegalArgumentException("multipart != " + var1);
         } else {
            this.type = var1;
            return this;
         }
      }
   }

   public static final class Part {
      final RequestBody body;
      @Nullable
      final Headers headers;

      private Part(@Nullable Headers var1, RequestBody var2) {
         this.headers = var1;
         this.body = var2;
      }

      public static MultipartBody.Part create(@Nullable Headers var0, RequestBody var1) {
         if(var1 == null) {
            throw new NullPointerException("body == null");
         } else if(var0 != null && var0.get("Content-Type") != null) {
            throw new IllegalArgumentException("Unexpected header: Content-Type");
         } else if(var0 != null && var0.get("Content-Length") != null) {
            throw new IllegalArgumentException("Unexpected header: Content-Length");
         } else {
            return new MultipartBody.Part(var0, var1);
         }
      }

      public static MultipartBody.Part create(RequestBody var0) {
         return create((Headers)null, var0);
      }

      public static MultipartBody.Part createFormData(String var0, String var1) {
         return createFormData(var0, (String)null, RequestBody.create((MediaType)null, (String)var1));
      }

      public static MultipartBody.Part createFormData(String var0, @Nullable String var1, RequestBody var2) {
         if(var0 == null) {
            throw new NullPointerException("name == null");
         } else {
            StringBuilder var3 = new StringBuilder("form-data; name=");
            MultipartBody.appendQuotedString(var3, var0);
            if(var1 != null) {
               var3.append("; filename=");
               MultipartBody.appendQuotedString(var3, var1);
            }

            return create(Headers.of(new String[]{"Content-Disposition", var3.toString()}), var2);
         }
      }

      public RequestBody body() {
         return this.body;
      }

      @Nullable
      public Headers headers() {
         return this.headers;
      }
   }
}
