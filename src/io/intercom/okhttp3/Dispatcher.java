package io.intercom.okhttp3;

import io.intercom.okhttp3.internal.Util;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import javax.annotation.Nullable;

public final class Dispatcher {
   @Nullable
   private ExecutorService executorService;
   @Nullable
   private Runnable idleCallback;
   private int maxRequests = 64;
   private int maxRequestsPerHost = 5;
   private final Deque readyAsyncCalls = new ArrayDeque();
   private final Deque runningAsyncCalls = new ArrayDeque();
   private final Deque runningSyncCalls = new ArrayDeque();

   public Dispatcher() {
   }

   public Dispatcher(ExecutorService var1) {
      this.executorService = var1;
   }

   private void finished(Deque param1, Object param2, boolean param3) {
      // $FF: Couldn't be decompiled
   }

   private void promoteCalls() {
      if(this.runningAsyncCalls.size() < this.maxRequests && !this.readyAsyncCalls.isEmpty()) {
         Iterator var2 = this.readyAsyncCalls.iterator();

         while(var2.hasNext()) {
            RealCall.AsyncCall var1 = (RealCall.AsyncCall)var2.next();
            if(this.runningCallsForHost(var1) < this.maxRequestsPerHost) {
               var2.remove();
               this.runningAsyncCalls.add(var1);
               this.executorService().execute(var1);
            }

            if(this.runningAsyncCalls.size() >= this.maxRequests) {
               break;
            }
         }
      }

   }

   private int runningCallsForHost(RealCall.AsyncCall var1) {
      Iterator var3 = this.runningAsyncCalls.iterator();
      int var2 = 0;

      while(var3.hasNext()) {
         if(((RealCall.AsyncCall)var3.next()).host().equals(var1.host())) {
            ++var2;
         }
      }

      return var2;
   }

   public void cancelAll() {
      // $FF: Couldn't be decompiled
   }

   void enqueue(RealCall.AsyncCall var1) {
      synchronized(this){}

      try {
         if(this.runningAsyncCalls.size() < this.maxRequests && this.runningCallsForHost(var1) < this.maxRequestsPerHost) {
            this.runningAsyncCalls.add(var1);
            this.executorService().execute(var1);
         } else {
            this.readyAsyncCalls.add(var1);
         }
      } finally {
         ;
      }

   }

   void executed(RealCall var1) {
      synchronized(this){}

      try {
         this.runningSyncCalls.add(var1);
      } finally {
         ;
      }

   }

   public ExecutorService executorService() {
      synchronized(this){}

      ExecutorService var6;
      try {
         if(this.executorService == null) {
            TimeUnit var2 = TimeUnit.SECONDS;
            SynchronousQueue var3 = new SynchronousQueue();
            ThreadPoolExecutor var1 = new ThreadPoolExecutor(0, Integer.MAX_VALUE, 60L, var2, var3, Util.threadFactory("OkHttp Dispatcher", false));
            this.executorService = var1;
         }

         var6 = this.executorService;
      } finally {
         ;
      }

      return var6;
   }

   void finished(RealCall.AsyncCall var1) {
      this.finished(this.runningAsyncCalls, var1, true);
   }

   void finished(RealCall var1) {
      this.finished(this.runningSyncCalls, var1, false);
   }

   public int getMaxRequests() {
      synchronized(this){}

      int var1;
      try {
         var1 = this.maxRequests;
      } finally {
         ;
      }

      return var1;
   }

   public int getMaxRequestsPerHost() {
      synchronized(this){}

      int var1;
      try {
         var1 = this.maxRequestsPerHost;
      } finally {
         ;
      }

      return var1;
   }

   public List queuedCalls() {
      // $FF: Couldn't be decompiled
   }

   public int queuedCallsCount() {
      synchronized(this){}

      int var1;
      try {
         var1 = this.readyAsyncCalls.size();
      } finally {
         ;
      }

      return var1;
   }

   public List runningCalls() {
      // $FF: Couldn't be decompiled
   }

   public int runningCallsCount() {
      synchronized(this){}

      int var1;
      int var2;
      try {
         var2 = this.runningAsyncCalls.size();
         var1 = this.runningSyncCalls.size();
      } finally {
         ;
      }

      return var2 + var1;
   }

   public void setIdleCallback(@Nullable Runnable var1) {
      synchronized(this){}

      try {
         this.idleCallback = var1;
      } finally {
         ;
      }

   }

   public void setMaxRequests(int param1) {
      // $FF: Couldn't be decompiled
   }

   public void setMaxRequestsPerHost(int param1) {
      // $FF: Couldn't be decompiled
   }
}
