package io.intercom.okhttp3;

import io.intercom.okhttp3.internal.Util;
import java.nio.charset.Charset;
import javax.annotation.Nullable;

public final class Challenge {
   private final Charset charset;
   private final String realm;
   private final String scheme;

   public Challenge(String var1, String var2) {
      this(var1, var2, Util.ISO_8859_1);
   }

   private Challenge(String var1, String var2, Charset var3) {
      if(var1 == null) {
         throw new NullPointerException("scheme == null");
      } else if(var2 == null) {
         throw new NullPointerException("realm == null");
      } else if(var3 == null) {
         throw new NullPointerException("charset == null");
      } else {
         this.scheme = var1;
         this.realm = var2;
         this.charset = var3;
      }
   }

   public Charset charset() {
      return this.charset;
   }

   public boolean equals(@Nullable Object var1) {
      boolean var2;
      if(var1 instanceof Challenge && ((Challenge)var1).scheme.equals(this.scheme) && ((Challenge)var1).realm.equals(this.realm) && ((Challenge)var1).charset.equals(this.charset)) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   public int hashCode() {
      return ((this.realm.hashCode() + 899) * 31 + this.scheme.hashCode()) * 31 + this.charset.hashCode();
   }

   public String realm() {
      return this.realm;
   }

   public String scheme() {
      return this.scheme;
   }

   public String toString() {
      return this.scheme + " realm=\"" + this.realm + "\" charset=\"" + this.charset + "\"";
   }

   public Challenge withCharset(Charset var1) {
      return new Challenge(this.scheme, this.realm, var1);
   }
}
