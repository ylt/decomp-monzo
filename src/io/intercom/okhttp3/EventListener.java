package io.intercom.okhttp3;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.List;
import javax.annotation.Nullable;

public abstract class EventListener {
   public static final EventListener NONE = new EventListener() {
   };

   static EventListener.Factory factory(final EventListener var0) {
      return new EventListener.Factory() {
         public EventListener create(Call var1) {
            return var0;
         }
      };
   }

   public void callEnd(Call var1) {
   }

   public void callFailed(Call var1, IOException var2) {
   }

   public void callStart(Call var1) {
   }

   public void connectEnd(Call var1, InetSocketAddress var2, @Nullable Proxy var3, @Nullable Protocol var4) {
   }

   public void connectFailed(Call var1, InetSocketAddress var2, @Nullable Proxy var3, @Nullable Protocol var4, @Nullable IOException var5) {
   }

   public void connectStart(Call var1, InetSocketAddress var2, Proxy var3) {
   }

   public void connectionAcquired(Call var1, Connection var2) {
   }

   public void connectionReleased(Call var1, Connection var2) {
   }

   public void dnsEnd(Call var1, String var2, @Nullable List var3) {
   }

   public void dnsStart(Call var1, String var2) {
   }

   public void requestBodyEnd(Call var1, long var2) {
   }

   public void requestBodyStart(Call var1) {
   }

   public void requestHeadersEnd(Call var1, Request var2) {
   }

   public void requestHeadersStart(Call var1) {
   }

   public void responseBodyEnd(Call var1, long var2) {
   }

   public void responseBodyStart(Call var1) {
   }

   public void responseHeadersEnd(Call var1, Response var2) {
   }

   public void responseHeadersStart(Call var1) {
   }

   public void secureConnectEnd(Call var1, @Nullable Handshake var2) {
   }

   public void secureConnectStart(Call var1) {
   }

   public interface Factory {
      EventListener create(Call var1);
   }
}
