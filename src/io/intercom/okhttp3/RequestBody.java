package io.intercom.okhttp3;

import io.intercom.a.d;
import io.intercom.a.f;
import io.intercom.okhttp3.internal.Util;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import javax.annotation.Nullable;

public abstract class RequestBody {
   public static RequestBody create(@Nullable final MediaType var0, final f var1) {
      return new RequestBody() {
         public long contentLength() throws IOException {
            return (long)var1.h();
         }

         @Nullable
         public MediaType contentType() {
            return var0;
         }

         public void writeTo(d var1x) throws IOException {
            var1x.b(var1);
         }
      };
   }

   public static RequestBody create(@Nullable final MediaType var0, final File var1) {
      if(var1 == null) {
         throw new NullPointerException("content == null");
      } else {
         return new RequestBody() {
            public long contentLength() {
               return var1.length();
            }

            @Nullable
            public MediaType contentType() {
               return var0;
            }

            public void writeTo(d param1) throws IOException {
               // $FF: Couldn't be decompiled
            }
         };
      }
   }

   public static RequestBody create(@Nullable MediaType var0, String var1) {
      Charset var2 = Util.UTF_8;
      MediaType var3 = var0;
      if(var0 != null) {
         Charset var4 = var0.charset();
         var2 = var4;
         var3 = var0;
         if(var4 == null) {
            var2 = Util.UTF_8;
            var3 = MediaType.parse(var0 + "; charset=utf-8");
         }
      }

      return create(var3, var1.getBytes(var2));
   }

   public static RequestBody create(@Nullable MediaType var0, byte[] var1) {
      return create(var0, var1, 0, var1.length);
   }

   public static RequestBody create(@Nullable final MediaType var0, final byte[] var1, final int var2, final int var3) {
      if(var1 == null) {
         throw new NullPointerException("content == null");
      } else {
         Util.checkOffsetAndCount((long)var1.length, (long)var2, (long)var3);
         return new RequestBody() {
            public long contentLength() {
               return (long)var3;
            }

            @Nullable
            public MediaType contentType() {
               return var0;
            }

            public void writeTo(d var1x) throws IOException {
               var1x.c(var1, var2, var3);
            }
         };
      }
   }

   public long contentLength() throws IOException {
      return -1L;
   }

   @Nullable
   public abstract MediaType contentType();

   public abstract void writeTo(d var1) throws IOException;
}
