package io.intercom.okhttp3;

import io.intercom.a.f;
import javax.annotation.Nullable;

public interface WebSocket {
   void cancel();

   boolean close(int var1, @Nullable String var2);

   long queueSize();

   Request request();

   boolean send(f var1);

   boolean send(String var1);

   public interface Factory {
      WebSocket newWebSocket(Request var1, WebSocketListener var2);
   }
}
