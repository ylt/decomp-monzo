package io.intercom.okhttp3.internal.cache;

import io.intercom.a.r;
import java.io.IOException;

public interface CacheRequest {
   void abort();

   r body() throws IOException;
}
