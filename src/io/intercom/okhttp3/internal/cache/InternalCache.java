package io.intercom.okhttp3.internal.cache;

import io.intercom.okhttp3.Request;
import io.intercom.okhttp3.Response;
import java.io.IOException;

public interface InternalCache {
   Response get(Request var1) throws IOException;

   CacheRequest put(Response var1) throws IOException;

   void remove(Request var1) throws IOException;

   void trackConditionalCacheHit();

   void trackResponse(CacheStrategy var1);

   void update(Response var1, Response var2);
}
