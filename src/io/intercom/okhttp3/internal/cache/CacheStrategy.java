package io.intercom.okhttp3.internal.cache;

import io.intercom.okhttp3.CacheControl;
import io.intercom.okhttp3.Headers;
import io.intercom.okhttp3.Request;
import io.intercom.okhttp3.Response;
import io.intercom.okhttp3.internal.Internal;
import io.intercom.okhttp3.internal.http.HttpDate;
import io.intercom.okhttp3.internal.http.HttpHeaders;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import javax.annotation.Nullable;

public final class CacheStrategy {
   @Nullable
   public final Response cacheResponse;
   @Nullable
   public final Request networkRequest;

   CacheStrategy(Request var1, Response var2) {
      this.networkRequest = var1;
      this.cacheResponse = var2;
   }

   public static boolean isCacheable(Response var0, Request var1) {
      boolean var3 = false;
      boolean var2;
      switch(var0.code()) {
      case 302:
      case 307:
         if(var0.header("Expires") == null && var0.cacheControl().maxAgeSeconds() == -1 && !var0.cacheControl().isPublic()) {
            var2 = var3;
            if(!var0.cacheControl().isPrivate()) {
               break;
            }
         }
      case 200:
      case 203:
      case 204:
      case 300:
      case 301:
      case 308:
      case 404:
      case 405:
      case 410:
      case 414:
      case 501:
         var2 = var3;
         if(!var0.cacheControl().noStore()) {
            var2 = var3;
            if(!var1.cacheControl().noStore()) {
               var2 = true;
            }
         }
         break;
      default:
         var2 = var3;
      }

      return var2;
   }

   public static class Factory {
      private int ageSeconds = -1;
      final Response cacheResponse;
      private String etag;
      private Date expires;
      private Date lastModified;
      private String lastModifiedString;
      final long nowMillis;
      private long receivedResponseMillis;
      final Request request;
      private long sentRequestMillis;
      private Date servedDate;
      private String servedDateString;

      public Factory(long var1, Request var3, Response var4) {
         this.nowMillis = var1;
         this.request = var3;
         this.cacheResponse = var4;
         if(var4 != null) {
            this.sentRequestMillis = var4.sentRequestAtMillis();
            this.receivedResponseMillis = var4.receivedResponseAtMillis();
            Headers var9 = var4.headers();
            int var5 = 0;

            for(int var6 = var9.size(); var5 < var6; ++var5) {
               String var8 = var9.name(var5);
               String var7 = var9.value(var5);
               if("Date".equalsIgnoreCase(var8)) {
                  this.servedDate = HttpDate.parse(var7);
                  this.servedDateString = var7;
               } else if("Expires".equalsIgnoreCase(var8)) {
                  this.expires = HttpDate.parse(var7);
               } else if("Last-Modified".equalsIgnoreCase(var8)) {
                  this.lastModified = HttpDate.parse(var7);
                  this.lastModifiedString = var7;
               } else if("ETag".equalsIgnoreCase(var8)) {
                  this.etag = var7;
               } else if("Age".equalsIgnoreCase(var8)) {
                  this.ageSeconds = HttpHeaders.parseSeconds(var7, -1);
               }
            }
         }

      }

      private long cacheResponseAge() {
         long var1 = 0L;
         if(this.servedDate != null) {
            var1 = Math.max(0L, this.receivedResponseMillis - this.servedDate.getTime());
         }

         long var3 = var1;
         if(this.ageSeconds != -1) {
            var3 = Math.max(var1, TimeUnit.SECONDS.toMillis((long)this.ageSeconds));
         }

         return var3 + (this.receivedResponseMillis - this.sentRequestMillis) + (this.nowMillis - this.receivedResponseMillis);
      }

      private long computeFreshnessLifetime() {
         long var3 = 0L;
         CacheControl var7 = this.cacheResponse.cacheControl();
         long var1;
         if(var7.maxAgeSeconds() != -1) {
            var1 = TimeUnit.SECONDS.toMillis((long)var7.maxAgeSeconds());
         } else if(this.expires != null) {
            if(this.servedDate != null) {
               var1 = this.servedDate.getTime();
            } else {
               var1 = this.receivedResponseMillis;
            }

            var1 = this.expires.getTime() - var1;
            if(var1 <= 0L) {
               var1 = 0L;
            }
         } else {
            var1 = var3;
            if(this.lastModified != null) {
               var1 = var3;
               if(this.cacheResponse.request().url().query() == null) {
                  if(this.servedDate != null) {
                     var1 = this.servedDate.getTime();
                  } else {
                     var1 = this.sentRequestMillis;
                  }

                  long var5 = var1 - this.lastModified.getTime();
                  var1 = var3;
                  if(var5 > 0L) {
                     var1 = var5 / 10L;
                  }
               }
            }
         }

         return var1;
      }

      private CacheStrategy getCandidate() {
         long var7 = 0L;
         CacheStrategy var11;
         if(this.cacheResponse == null) {
            var11 = new CacheStrategy(this.request, (Response)null);
         } else if(this.request.isHttps() && this.cacheResponse.handshake() == null) {
            var11 = new CacheStrategy(this.request, (Response)null);
         } else if(!CacheStrategy.isCacheable(this.cacheResponse, this.request)) {
            var11 = new CacheStrategy(this.request, (Response)null);
         } else {
            CacheControl var12 = this.request.cacheControl();
            if(!var12.noCache() && !hasConditions(this.request)) {
               CacheControl var14 = this.cacheResponse.cacheControl();
               if(var14.immutable()) {
                  var11 = new CacheStrategy((Request)null, this.cacheResponse);
               } else {
                  long var9 = this.cacheResponseAge();
                  long var3 = this.computeFreshnessLifetime();
                  long var1 = var3;
                  if(var12.maxAgeSeconds() != -1) {
                     var1 = Math.min(var3, TimeUnit.SECONDS.toMillis((long)var12.maxAgeSeconds()));
                  }

                  if(var12.minFreshSeconds() != -1) {
                     var3 = TimeUnit.SECONDS.toMillis((long)var12.minFreshSeconds());
                  } else {
                     var3 = 0L;
                  }

                  long var5 = var7;
                  if(!var14.mustRevalidate()) {
                     var5 = var7;
                     if(var12.maxStaleSeconds() != -1) {
                        var5 = TimeUnit.SECONDS.toMillis((long)var12.maxStaleSeconds());
                     }
                  }

                  if(!var14.noCache() && var9 + var3 < var5 + var1) {
                     Response.Builder var17 = this.cacheResponse.newBuilder();
                     if(var3 + var9 >= var1) {
                        var17.addHeader("Warning", "110 HttpURLConnection \"Response is stale\"");
                     }

                     if(var9 > 86400000L && this.isFreshnessLifetimeHeuristic()) {
                        var17.addHeader("Warning", "113 HttpURLConnection \"Heuristic expiration\"");
                     }

                     var11 = new CacheStrategy((Request)null, var17.build());
                  } else {
                     String var15;
                     String var16;
                     if(this.etag != null) {
                        var15 = "If-None-Match";
                        var16 = this.etag;
                     } else if(this.lastModified != null) {
                        var15 = "If-Modified-Since";
                        var16 = this.lastModifiedString;
                     } else {
                        if(this.servedDate == null) {
                           var11 = new CacheStrategy(this.request, (Response)null);
                           return var11;
                        }

                        var15 = "If-Modified-Since";
                        var16 = this.servedDateString;
                     }

                     Headers.Builder var13 = this.request.headers().newBuilder();
                     Internal.instance.addLenient(var13, var15, var16);
                     var11 = new CacheStrategy(this.request.newBuilder().headers(var13.build()).build(), this.cacheResponse);
                  }
               }
            } else {
               var11 = new CacheStrategy(this.request, (Response)null);
            }
         }

         return var11;
      }

      private static boolean hasConditions(Request var0) {
         boolean var1;
         if(var0.header("If-Modified-Since") == null && var0.header("If-None-Match") == null) {
            var1 = false;
         } else {
            var1 = true;
         }

         return var1;
      }

      private boolean isFreshnessLifetimeHeuristic() {
         boolean var1;
         if(this.cacheResponse.cacheControl().maxAgeSeconds() == -1 && this.expires == null) {
            var1 = true;
         } else {
            var1 = false;
         }

         return var1;
      }

      public CacheStrategy get() {
         CacheStrategy var2 = this.getCandidate();
         CacheStrategy var1 = var2;
         if(var2.networkRequest != null) {
            var1 = var2;
            if(this.request.cacheControl().onlyIfCached()) {
               var1 = new CacheStrategy((Request)null, (Response)null);
            }
         }

         return var1;
      }
   }
}
