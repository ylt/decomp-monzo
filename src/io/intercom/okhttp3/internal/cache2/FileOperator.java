package io.intercom.okhttp3.internal.cache2;

import io.intercom.a.c;
import java.io.EOFException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

final class FileOperator {
   private static final int BUFFER_SIZE = 8192;
   private final byte[] byteArray = new byte[8192];
   private final ByteBuffer byteBuffer;
   private final FileChannel fileChannel;

   FileOperator(FileChannel var1) {
      this.byteBuffer = ByteBuffer.wrap(this.byteArray);
      this.fileChannel = var1;
   }

   public void read(long var1, c var3, long var4) throws IOException {
      long var7 = var1;
      var1 = var4;
      if(var4 < 0L) {
         throw new IndexOutOfBoundsException();
      } else {
         while(var1 > 0L) {
            boolean var10 = false;

            int var6;
            try {
               var10 = true;
               this.byteBuffer.limit((int)Math.min(8192L, var1));
               if(this.fileChannel.read(this.byteBuffer, var7) == -1) {
                  EOFException var12 = new EOFException();
                  throw var12;
               }

               var6 = this.byteBuffer.position();
               var3.b(this.byteArray, 0, var6);
               var10 = false;
            } finally {
               if(var10) {
                  this.byteBuffer.clear();
               }
            }

            var7 += (long)var6;
            var1 -= (long)var6;
            this.byteBuffer.clear();
         }

      }
   }

   public void write(long param1, c param3, long param4) throws IOException {
      // $FF: Couldn't be decompiled
   }
}
