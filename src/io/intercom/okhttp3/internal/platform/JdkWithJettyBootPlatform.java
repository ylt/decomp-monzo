package io.intercom.okhttp3.internal.platform;

import io.intercom.okhttp3.internal.Util;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.List;
import javax.net.ssl.SSLSocket;

class JdkWithJettyBootPlatform extends Platform {
   private final Class clientProviderClass;
   private final Method getMethod;
   private final Method putMethod;
   private final Method removeMethod;
   private final Class serverProviderClass;

   JdkWithJettyBootPlatform(Method var1, Method var2, Method var3, Class var4, Class var5) {
      this.putMethod = var1;
      this.getMethod = var2;
      this.removeMethod = var3;
      this.clientProviderClass = var4;
      this.serverProviderClass = var5;
   }

   public static Platform buildIfSupported() {
      JdkWithJettyBootPlatform var0;
      try {
         Class var8 = Class.forName("org.eclipse.jetty.alpn.ALPN");
         StringBuilder var1 = new StringBuilder();
         Class var3 = Class.forName(var1.append("org.eclipse.jetty.alpn.ALPN").append("$Provider").toString());
         var1 = new StringBuilder();
         Class var9 = Class.forName(var1.append("org.eclipse.jetty.alpn.ALPN").append("$ClientProvider").toString());
         StringBuilder var2 = new StringBuilder();
         Class var10 = Class.forName(var2.append("org.eclipse.jetty.alpn.ALPN").append("$ServerProvider").toString());
         Method var11 = var8.getMethod("put", new Class[]{SSLSocket.class, var3});
         Method var4 = var8.getMethod("get", new Class[]{SSLSocket.class});
         Method var5 = var8.getMethod("remove", new Class[]{SSLSocket.class});
         var0 = new JdkWithJettyBootPlatform(var11, var4, var5, var9, var10);
         return var0;
      } catch (ClassNotFoundException var6) {
         ;
      } catch (NoSuchMethodException var7) {
         ;
      }

      var0 = null;
      return var0;
   }

   public void afterHandshake(SSLSocket var1) {
      Object var4;
      try {
         this.removeMethod.invoke((Object)null, new Object[]{var1});
         return;
      } catch (IllegalAccessException var2) {
         var4 = var2;
      } catch (InvocationTargetException var3) {
         var4 = var3;
      }

      throw Util.assertionError("unable to remove alpn", (Exception)var4);
   }

   public void configureTlsExtensions(SSLSocket var1, String var2, List var3) {
      var3 = alpnProtocolNames(var3);

      Object var9;
      try {
         ClassLoader var10 = Platform.class.getClassLoader();
         Class var5 = this.clientProviderClass;
         Class var4 = this.serverProviderClass;
         JdkWithJettyBootPlatform.JettyNegoProvider var6 = new JdkWithJettyBootPlatform.JettyNegoProvider(var3);
         Object var11 = Proxy.newProxyInstance(var10, new Class[]{var5, var4}, var6);
         this.putMethod.invoke((Object)null, new Object[]{var1, var11});
         return;
      } catch (InvocationTargetException var7) {
         var9 = var7;
      } catch (IllegalAccessException var8) {
         var9 = var8;
      }

      throw Util.assertionError("unable to set alpn", (Exception)var9);
   }

   public String getSelectedProtocol(SSLSocket var1) {
      Object var2 = null;

      String var7;
      label49: {
         label35: {
            Object var5;
            try {
               JdkWithJettyBootPlatform.JettyNegoProvider var6 = (JdkWithJettyBootPlatform.JettyNegoProvider)Proxy.getInvocationHandler(this.getMethod.invoke((Object)null, new Object[]{var1}));
               if(!var6.unsupported && var6.selected == null) {
                  Platform.get().log(4, "ALPN callback dropped: HTTP/2 is disabled. Is alpn-boot on the boot class path?", (Throwable)null);
                  break label49;
               }

               if(!var6.unsupported) {
                  var7 = var6.selected;
                  return var7;
               }
               break label35;
            } catch (InvocationTargetException var3) {
               var5 = var3;
            } catch (IllegalAccessException var4) {
               var5 = var4;
            }

            throw Util.assertionError("unable to get selected protocol", (Exception)var5);
         }

         var7 = null;
         return var7;
      }

      var7 = (String)var2;
      return var7;
   }

   private static class JettyNegoProvider implements InvocationHandler {
      private final List protocols;
      String selected;
      boolean unsupported;

      JettyNegoProvider(List var1) {
         this.protocols = var1;
      }

      public Object invoke(Object var1, Method var2, Object[] var3) throws Throwable {
         String var6 = var2.getName();
         Class var7 = var2.getReturnType();
         var1 = var3;
         if(var3 == null) {
            var1 = Util.EMPTY_STRING_ARRAY;
         }

         if(var6.equals("supports") && Boolean.TYPE == var7) {
            var1 = Boolean.valueOf(true);
         } else if(var6.equals("unsupported") && Void.TYPE == var7) {
            this.unsupported = true;
            var1 = null;
         } else if(var6.equals("protocols") && ((Object[])var1).length == 0) {
            var1 = this.protocols;
         } else if((var6.equals("selectProtocol") || var6.equals("select")) && String.class == var7 && ((Object[])var1).length == 1 && ((Object[])var1)[0] instanceof List) {
            List var8 = (List)((Object[])var1)[0];
            int var5 = var8.size();
            int var4 = 0;

            while(true) {
               if(var4 >= var5) {
                  var1 = (String)this.protocols.get(0);
                  this.selected = (String)var1;
                  break;
               }

               if(this.protocols.contains(var8.get(var4))) {
                  var1 = (String)var8.get(var4);
                  this.selected = (String)var1;
                  break;
               }

               ++var4;
            }
         } else if((var6.equals("protocolSelected") || var6.equals("selected")) && ((Object[])var1).length == 1) {
            this.selected = (String)((Object[])var1)[0];
            var1 = null;
         } else {
            var1 = var2.invoke(this, (Object[])var1);
         }

         return var1;
      }
   }
}
