package io.intercom.okhttp3.internal.platform;

import io.intercom.a.c;
import io.intercom.okhttp3.OkHttpClient;
import io.intercom.okhttp3.Protocol;
import io.intercom.okhttp3.internal.tls.BasicCertificateChainCleaner;
import io.intercom.okhttp3.internal.tls.BasicTrustRootIndex;
import io.intercom.okhttp3.internal.tls.CertificateChainCleaner;
import io.intercom.okhttp3.internal.tls.TrustRootIndex;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.X509TrustManager;

public class Platform {
   public static final int INFO = 4;
   private static final Platform PLATFORM = findPlatform();
   public static final int WARN = 5;
   private static final Logger logger = Logger.getLogger(OkHttpClient.class.getName());

   public static List alpnProtocolNames(List var0) {
      ArrayList var3 = new ArrayList(var0.size());
      int var2 = var0.size();

      for(int var1 = 0; var1 < var2; ++var1) {
         Protocol var4 = (Protocol)var0.get(var1);
         if(var4 != Protocol.HTTP_1_0) {
            var3.add(var4.toString());
         }
      }

      return var3;
   }

   static byte[] concatLengthPrefixed(List var0) {
      c var4 = new c();
      int var2 = var0.size();

      for(int var1 = 0; var1 < var2; ++var1) {
         Protocol var3 = (Protocol)var0.get(var1);
         if(var3 != Protocol.HTTP_1_0) {
            var4.b(var3.toString().length());
            var4.a(var3.toString());
         }
      }

      return var4.t();
   }

   private static Platform findPlatform() {
      Object var0 = AndroidPlatform.buildIfSupported();
      if(var0 == null) {
         Jdk9Platform var1 = Jdk9Platform.buildIfSupported();
         var0 = var1;
         if(var1 == null) {
            Platform var2 = JdkWithJettyBootPlatform.buildIfSupported();
            var0 = var2;
            if(var2 == null) {
               var0 = new Platform();
            }
         }
      }

      return (Platform)var0;
   }

   public static Platform get() {
      return PLATFORM;
   }

   static Object readFieldOrNull(Object param0, Class param1, String param2) {
      // $FF: Couldn't be decompiled
   }

   public void afterHandshake(SSLSocket var1) {
   }

   public CertificateChainCleaner buildCertificateChainCleaner(X509TrustManager var1) {
      return new BasicCertificateChainCleaner(this.buildTrustRootIndex(var1));
   }

   public TrustRootIndex buildTrustRootIndex(X509TrustManager var1) {
      return new BasicTrustRootIndex(var1.getAcceptedIssuers());
   }

   public void configureTlsExtensions(SSLSocket var1, String var2, List var3) {
   }

   public void connectSocket(Socket var1, InetSocketAddress var2, int var3) throws IOException {
      var1.connect(var2, var3);
   }

   public String getPrefix() {
      return "OkHttp";
   }

   public String getSelectedProtocol(SSLSocket var1) {
      return null;
   }

   public Object getStackTraceForCloseable(String var1) {
      Throwable var2;
      if(logger.isLoggable(Level.FINE)) {
         var2 = new Throwable(var1);
      } else {
         var2 = null;
      }

      return var2;
   }

   public boolean isCleartextTrafficPermitted(String var1) {
      return true;
   }

   public void log(int var1, String var2, Throwable var3) {
      Level var4;
      if(var1 == 5) {
         var4 = Level.WARNING;
      } else {
         var4 = Level.INFO;
      }

      logger.log(var4, var2, var3);
   }

   public void logCloseableLeak(String var1, Object var2) {
      String var3 = var1;
      if(var2 == null) {
         var3 = var1 + " To see where this was allocated, set the OkHttpClient logger level to FINE: Logger.getLogger(OkHttpClient.class.getName()).setLevel(Level.FINE);";
      }

      this.log(5, var3, (Throwable)var2);
   }

   public X509TrustManager trustManager(SSLSocketFactory param1) {
      // $FF: Couldn't be decompiled
   }
}
