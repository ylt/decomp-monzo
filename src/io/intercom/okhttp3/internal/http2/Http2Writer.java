package io.intercom.okhttp3.internal.http2;

import io.intercom.a.c;
import io.intercom.a.d;
import java.io.Closeable;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

final class Http2Writer implements Closeable {
   private static final Logger logger = Logger.getLogger(Http2.class.getName());
   private final boolean client;
   private boolean closed;
   private final c hpackBuffer;
   final Hpack.Writer hpackWriter;
   private int maxFrameSize;
   private final d sink;

   Http2Writer(d var1, boolean var2) {
      this.sink = var1;
      this.client = var2;
      this.hpackBuffer = new c();
      this.hpackWriter = new Hpack.Writer(this.hpackBuffer);
      this.maxFrameSize = 16384;
   }

   private void writeContinuationFrames(int var1, long var2) throws IOException {
      while(var2 > 0L) {
         int var5 = (int)Math.min((long)this.maxFrameSize, var2);
         var2 -= (long)var5;
         byte var4;
         if(var2 == 0L) {
            var4 = 4;
         } else {
            var4 = 0;
         }

         this.frameHeader(var1, var5, 9, var4);
         this.sink.write(this.hpackBuffer, (long)var5);
      }

   }

   private static void writeMedium(d var0, int var1) throws IOException {
      var0.i(var1 >>> 16 & 255);
      var0.i(var1 >>> 8 & 255);
      var0.i(var1 & 255);
   }

   public void applyAndAckSettings(Settings var1) throws IOException {
      synchronized(this){}

      try {
         if(this.closed) {
            IOException var4 = new IOException("closed");
            throw var4;
         }

         this.maxFrameSize = var1.getMaxFrameSize(this.maxFrameSize);
         if(var1.getHeaderTableSize() != -1) {
            this.hpackWriter.setHeaderTableSizeSetting(var1.getHeaderTableSize());
         }

         this.frameHeader(0, 0, 4, 1);
         this.sink.flush();
      } finally {
         ;
      }

   }

   public void close() throws IOException {
      synchronized(this){}

      try {
         this.closed = true;
         this.sink.close();
      } finally {
         ;
      }

   }

   public void connectionPreface() throws IOException {
      // $FF: Couldn't be decompiled
   }

   public void data(boolean param1, int param2, c param3, int param4) throws IOException {
      // $FF: Couldn't be decompiled
   }

   void dataFrame(int var1, byte var2, c var3, int var4) throws IOException {
      this.frameHeader(var1, var4, 0, var2);
      if(var4 > 0) {
         this.sink.write(var3, (long)var4);
      }

   }

   public void flush() throws IOException {
      synchronized(this){}

      try {
         if(this.closed) {
            IOException var1 = new IOException("closed");
            throw var1;
         }

         this.sink.flush();
      } finally {
         ;
      }

   }

   public void frameHeader(int var1, int var2, byte var3, byte var4) throws IOException {
      if(logger.isLoggable(Level.FINE)) {
         logger.fine(Http2.frameLog(false, var1, var2, var3, var4));
      }

      if(var2 > this.maxFrameSize) {
         throw Http2.illegalArgument("FRAME_SIZE_ERROR length > %d: %d", new Object[]{Integer.valueOf(this.maxFrameSize), Integer.valueOf(var2)});
      } else if((Integer.MIN_VALUE & var1) != 0) {
         throw Http2.illegalArgument("reserved bit set: %s", new Object[]{Integer.valueOf(var1)});
      } else {
         writeMedium(this.sink, var2);
         this.sink.i(var3 & 255);
         this.sink.i(var4 & 255);
         this.sink.g(Integer.MAX_VALUE & var1);
      }
   }

   public void goAway(int var1, ErrorCode var2, byte[] var3) throws IOException {
      synchronized(this){}

      try {
         if(this.closed) {
            IOException var6 = new IOException("closed");
            throw var6;
         }

         if(var2.httpCode == -1) {
            throw Http2.illegalArgument("errorCode.httpCode == -1", new Object[0]);
         }

         this.frameHeader(0, var3.length + 8, 7, 0);
         this.sink.g(var1);
         this.sink.g(var2.httpCode);
         if(var3.length > 0) {
            this.sink.c(var3);
         }

         this.sink.flush();
      } finally {
         ;
      }

   }

   public void headers(int var1, List var2) throws IOException {
      synchronized(this){}

      try {
         if(this.closed) {
            IOException var5 = new IOException("closed");
            throw var5;
         }

         this.headers(false, var1, var2);
      } finally {
         ;
      }

   }

   void headers(boolean var1, int var2, List var3) throws IOException {
      if(this.closed) {
         throw new IOException("closed");
      } else {
         this.hpackWriter.writeHeaders(var3);
         long var7 = this.hpackBuffer.a();
         int var6 = (int)Math.min((long)this.maxFrameSize, var7);
         byte var4;
         if(var7 == (long)var6) {
            var4 = 4;
         } else {
            var4 = 0;
         }

         byte var5 = var4;
         if(var1) {
            var5 = (byte)(var4 | 1);
         }

         this.frameHeader(var2, var6, 1, var5);
         this.sink.write(this.hpackBuffer, (long)var6);
         if(var7 > (long)var6) {
            this.writeContinuationFrames(var2, var7 - (long)var6);
         }

      }
   }

   public int maxDataLength() {
      return this.maxFrameSize;
   }

   public void ping(boolean param1, int param2, int param3) throws IOException {
      // $FF: Couldn't be decompiled
   }

   public void pushPromise(int param1, int param2, List param3) throws IOException {
      // $FF: Couldn't be decompiled
   }

   public void rstStream(int var1, ErrorCode var2) throws IOException {
      synchronized(this){}

      try {
         if(this.closed) {
            IOException var6 = new IOException("closed");
            throw var6;
         }

         if(var2.httpCode == -1) {
            IllegalArgumentException var5 = new IllegalArgumentException();
            throw var5;
         }

         this.frameHeader(var1, 4, 3, 0);
         this.sink.g(var2.httpCode);
         this.sink.flush();
      } finally {
         ;
      }

   }

   public void settings(Settings param1) throws IOException {
      // $FF: Couldn't be decompiled
   }

   public void synReply(boolean var1, int var2, List var3) throws IOException {
      synchronized(this){}

      try {
         if(this.closed) {
            IOException var6 = new IOException("closed");
            throw var6;
         }

         this.headers(var1, var2, var3);
      } finally {
         ;
      }

   }

   public void synStream(boolean var1, int var2, int var3, List var4) throws IOException {
      synchronized(this){}

      try {
         if(this.closed) {
            IOException var7 = new IOException("closed");
            throw var7;
         }

         this.headers(var1, var2, var4);
      } finally {
         ;
      }

   }

   public void windowUpdate(int param1, long param2) throws IOException {
      // $FF: Couldn't be decompiled
   }
}
