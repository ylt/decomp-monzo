package io.intercom.okhttp3.internal.http2;

import io.intercom.a.e;
import java.io.IOException;
import java.util.List;

public interface PushObserver {
   PushObserver CANCEL = new PushObserver() {
      public boolean onData(int var1, e var2, int var3, boolean var4) throws IOException {
         var2.i((long)var3);
         return true;
      }

      public boolean onHeaders(int var1, List var2, boolean var3) {
         return true;
      }

      public boolean onRequest(int var1, List var2) {
         return true;
      }

      public void onReset(int var1, ErrorCode var2) {
      }
   };

   boolean onData(int var1, e var2, int var3, boolean var4) throws IOException;

   boolean onHeaders(int var1, List var2, boolean var3);

   boolean onRequest(int var1, List var2);

   void onReset(int var1, ErrorCode var2);
}
