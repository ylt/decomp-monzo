package io.intercom.okhttp3.internal;

import io.intercom.a.c;
import io.intercom.a.e;
import io.intercom.a.f;
import io.intercom.a.s;
import io.intercom.okhttp3.HttpUrl;
import io.intercom.okhttp3.MediaType;
import io.intercom.okhttp3.RequestBody;
import io.intercom.okhttp3.ResponseBody;
import java.io.Closeable;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
import javax.annotation.Nullable;

public final class Util {
   public static final byte[] EMPTY_BYTE_ARRAY = new byte[0];
   public static final RequestBody EMPTY_REQUEST;
   public static final ResponseBody EMPTY_RESPONSE;
   public static final String[] EMPTY_STRING_ARRAY = new String[0];
   public static final Charset ISO_8859_1;
   public static final Comparator NATURAL_ORDER;
   public static final TimeZone UTC;
   private static final Charset UTF_16_BE;
   private static final f UTF_16_BE_BOM;
   private static final Charset UTF_16_LE;
   private static final f UTF_16_LE_BOM;
   private static final Charset UTF_32_BE;
   private static final f UTF_32_BE_BOM;
   private static final Charset UTF_32_LE;
   private static final f UTF_32_LE_BOM;
   public static final Charset UTF_8;
   private static final f UTF_8_BOM;
   private static final Pattern VERIFY_AS_IP_ADDRESS;

   static {
      EMPTY_RESPONSE = ResponseBody.create((MediaType)null, (byte[])EMPTY_BYTE_ARRAY);
      EMPTY_REQUEST = RequestBody.create((MediaType)null, (byte[])EMPTY_BYTE_ARRAY);
      UTF_8_BOM = f.c("efbbbf");
      UTF_16_BE_BOM = f.c("feff");
      UTF_16_LE_BOM = f.c("fffe");
      UTF_32_BE_BOM = f.c("0000ffff");
      UTF_32_LE_BOM = f.c("ffff0000");
      UTF_8 = Charset.forName("UTF-8");
      ISO_8859_1 = Charset.forName("ISO-8859-1");
      UTF_16_BE = Charset.forName("UTF-16BE");
      UTF_16_LE = Charset.forName("UTF-16LE");
      UTF_32_BE = Charset.forName("UTF-32BE");
      UTF_32_LE = Charset.forName("UTF-32LE");
      UTC = TimeZone.getTimeZone("GMT");
      NATURAL_ORDER = new Comparator() {
         public int compare(String var1, String var2) {
            return var1.compareTo(var2);
         }
      };
      VERIFY_AS_IP_ADDRESS = Pattern.compile("([0-9a-fA-F]*:[0-9a-fA-F:.]*)|([\\d.]+)");
   }

   public static AssertionError assertionError(String var0, Exception var1) {
      return (AssertionError)(new AssertionError(var0)).initCause(var1);
   }

   public static Charset bomAwareCharset(e var0, Charset var1) throws IOException {
      if(var0.a(0L, UTF_8_BOM)) {
         var0.i((long)UTF_8_BOM.h());
         var1 = UTF_8;
      } else if(var0.a(0L, UTF_16_BE_BOM)) {
         var0.i((long)UTF_16_BE_BOM.h());
         var1 = UTF_16_BE;
      } else if(var0.a(0L, UTF_16_LE_BOM)) {
         var0.i((long)UTF_16_LE_BOM.h());
         var1 = UTF_16_LE;
      } else if(var0.a(0L, UTF_32_BE_BOM)) {
         var0.i((long)UTF_32_BE_BOM.h());
         var1 = UTF_32_BE;
      } else if(var0.a(0L, UTF_32_LE_BOM)) {
         var0.i((long)UTF_32_LE_BOM.h());
         var1 = UTF_32_LE;
      }

      return var1;
   }

   public static String canonicalizeHost(String param0) {
      // $FF: Couldn't be decompiled
   }

   public static int checkDuration(String var0, long var1, TimeUnit var3) {
      if(var1 < 0L) {
         throw new IllegalArgumentException(var0 + " < 0");
      } else if(var3 == null) {
         throw new NullPointerException("unit == null");
      } else {
         long var4 = var3.toMillis(var1);
         if(var4 > 2147483647L) {
            throw new IllegalArgumentException(var0 + " too large.");
         } else if(var4 == 0L && var1 > 0L) {
            throw new IllegalArgumentException(var0 + " too small.");
         } else {
            return (int)var4;
         }
      }
   }

   public static void checkOffsetAndCount(long var0, long var2, long var4) {
      if((var2 | var4) < 0L || var2 > var0 || var0 - var2 < var4) {
         throw new ArrayIndexOutOfBoundsException();
      }
   }

   public static void closeQuietly(Closeable var0) {
      if(var0 != null) {
         try {
            var0.close();
         } catch (RuntimeException var1) {
            throw var1;
         } catch (Exception var2) {
            ;
         }
      }

   }

   public static void closeQuietly(ServerSocket var0) {
      if(var0 != null) {
         try {
            var0.close();
         } catch (RuntimeException var1) {
            throw var1;
         } catch (Exception var2) {
            ;
         }
      }

   }

   public static void closeQuietly(Socket var0) {
      if(var0 != null) {
         try {
            var0.close();
         } catch (AssertionError var1) {
            if(!isAndroidGetsocknameError(var1)) {
               throw var1;
            }
         } catch (RuntimeException var2) {
            throw var2;
         } catch (Exception var3) {
            ;
         }
      }

   }

   public static String[] concat(String[] var0, String var1) {
      String[] var2 = new String[var0.length + 1];
      System.arraycopy(var0, 0, var2, 0, var0.length);
      var2[var2.length - 1] = var1;
      return var2;
   }

   private static boolean containsInvalidHostnameAsciiCodes(String var0) {
      boolean var4 = false;
      int var1 = 0;

      boolean var3;
      while(true) {
         var3 = var4;
         if(var1 >= var0.length()) {
            break;
         }

         char var2 = var0.charAt(var1);
         if(var2 <= 31 || var2 >= 127) {
            var3 = true;
            break;
         }

         if(" #%/:?@[\\]".indexOf(var2) != -1) {
            var3 = true;
            break;
         }

         ++var1;
      }

      return var3;
   }

   public static int decodeHexDigit(char var0) {
      int var1;
      if(var0 >= 48 && var0 <= 57) {
         var1 = var0 - 48;
      } else if(var0 >= 97 && var0 <= 102) {
         var1 = var0 - 97 + 10;
      } else if(var0 >= 65 && var0 <= 70) {
         var1 = var0 - 65 + 10;
      } else {
         var1 = -1;
      }

      return var1;
   }

   private static boolean decodeIpv4Suffix(String var0, int var1, int var2, byte[] var3, int var4) {
      int var6 = var4;
      int var5 = var1;

      boolean var9;
      while(true) {
         if(var5 >= var2) {
            if(var6 != var4 + 4) {
               var9 = false;
            } else {
               var9 = true;
            }
            break;
         }

         if(var6 == var3.length) {
            var9 = false;
            break;
         }

         var1 = var5;
         if(var6 != var4) {
            if(var0.charAt(var5) != 46) {
               var9 = false;
               break;
            }

            var1 = var5 + 1;
         }

         int var7 = 0;

         for(var5 = var1; var5 < var2; ++var5) {
            char var8 = var0.charAt(var5);
            if(var8 < 48 || var8 > 57) {
               break;
            }

            if(var7 == 0 && var1 != var5) {
               var9 = false;
               return var9;
            }

            var7 = var7 * 10 + var8 - 48;
            if(var7 > 255) {
               var9 = false;
               return var9;
            }
         }

         if(var5 - var1 == 0) {
            var9 = false;
            break;
         }

         var3[var6] = (byte)var7;
         ++var6;
      }

      return var9;
   }

   @Nullable
   private static InetAddress decodeIpv6(String var0, int var1, int var2) {
      byte[] var9 = new byte[16];
      int var8 = -1;
      int var4 = -1;
      int var3 = 0;
      int var5 = var1;

      InetAddress var11;
      while(true) {
         int var6;
         int var7;
         label92: {
            var6 = var4;
            var1 = var3;
            if(var5 < var2) {
               if(var3 == var9.length) {
                  var11 = null;
                  break;
               }

               if(var5 + 2 <= var2 && var0.regionMatches(var5, "::", 0, 2)) {
                  if(var4 != -1) {
                     var11 = null;
                     break;
                  }

                  var1 = var5 + 2;
                  var3 += 2;
                  if(var1 != var2) {
                     var7 = var3;
                     var6 = var3;
                     break label92;
                  }

                  var1 = var3;
                  var6 = var3;
               } else {
                  var1 = var5;
                  var6 = var4;
                  var7 = var3;
                  if(var3 == 0) {
                     break label92;
                  }

                  if(var0.regionMatches(var5, ":", 0, 1)) {
                     var1 = var5 + 1;
                     var7 = var3;
                     var6 = var4;
                     break label92;
                  }

                  if(!var0.regionMatches(var5, ".", 0, 1)) {
                     var11 = null;
                     break;
                  }

                  if(!decodeIpv4Suffix(var0, var8, var2, var9, var3 - 2)) {
                     var11 = null;
                     break;
                  }

                  var1 = var3 + 2;
                  var6 = var4;
               }
            }

            if(var1 != var9.length) {
               if(var6 == -1) {
                  var11 = null;
                  break;
               }

               System.arraycopy(var9, var6, var9, var9.length - (var1 - var6), var1 - var6);
               Arrays.fill(var9, var6, var9.length - var1 + var6, 0);
            }

            try {
               var11 = InetAddress.getByAddress(var9);
               break;
            } catch (UnknownHostException var10) {
               throw new AssertionError();
            }
         }

         var4 = 0;

         for(var3 = var1; var3 < var2; ++var3) {
            var5 = decodeHexDigit(var0.charAt(var3));
            if(var5 == -1) {
               break;
            }

            var4 = (var4 << 4) + var5;
         }

         var5 = var3 - var1;
         if(var5 == 0 || var5 > 4) {
            var11 = null;
            break;
         }

         var5 = var7 + 1;
         var9[var7] = (byte)(var4 >>> 8 & 255);
         var7 = var5 + 1;
         var9[var5] = (byte)(var4 & 255);
         var5 = var3;
         var4 = var6;
         var3 = var7;
         var8 = var1;
      }

      return var11;
   }

   public static int delimiterOffset(String var0, int var1, int var2, char var3) {
      while(true) {
         int var4 = var2;
         if(var1 < var2) {
            if(var0.charAt(var1) != var3) {
               ++var1;
               continue;
            }

            var4 = var1;
         }

         return var4;
      }
   }

   public static int delimiterOffset(String var0, int var1, int var2, String var3) {
      while(true) {
         int var4 = var2;
         if(var1 < var2) {
            if(var3.indexOf(var0.charAt(var1)) == -1) {
               ++var1;
               continue;
            }

            var4 = var1;
         }

         return var4;
      }
   }

   public static boolean discard(s var0, int var1, TimeUnit var2) {
      boolean var3;
      try {
         var3 = skipAll(var0, var1, var2);
      } catch (IOException var4) {
         var3 = false;
      }

      return var3;
   }

   public static boolean equal(Object var0, Object var1) {
      boolean var2;
      if(var0 != var1 && (var0 == null || !var0.equals(var1))) {
         var2 = false;
      } else {
         var2 = true;
      }

      return var2;
   }

   public static String format(String var0, Object... var1) {
      return String.format(Locale.US, var0, var1);
   }

   public static String hostHeader(HttpUrl var0, boolean var1) {
      String var2;
      if(var0.host().contains(":")) {
         var2 = "[" + var0.host() + "]";
      } else {
         var2 = var0.host();
      }

      String var3;
      if(!var1) {
         var3 = var2;
         if(var0.port() == HttpUrl.defaultPort(var0.scheme())) {
            return var3;
         }
      }

      var3 = var2 + ":" + var0.port();
      return var3;
   }

   public static List immutableList(List var0) {
      return Collections.unmodifiableList(new ArrayList(var0));
   }

   public static List immutableList(Object... var0) {
      return Collections.unmodifiableList(Arrays.asList((Object[])var0.clone()));
   }

   public static int indexOf(Comparator var0, String[] var1, String var2) {
      int var3 = 0;
      int var4 = var1.length;

      while(true) {
         if(var3 >= var4) {
            var3 = -1;
            break;
         }

         if(var0.compare(var1[var3], var2) == 0) {
            break;
         }

         ++var3;
      }

      return var3;
   }

   public static int indexOfControlOrNonAscii(String var0) {
      int var1 = 0;
      int var3 = var0.length();

      int var2;
      while(true) {
         if(var1 >= var3) {
            var2 = -1;
            break;
         }

         char var4 = var0.charAt(var1);
         var2 = var1;
         if(var4 <= 31) {
            break;
         }

         if(var4 >= 127) {
            var2 = var1;
            break;
         }

         ++var1;
      }

      return var2;
   }

   private static String inet6AddressToAscii(byte[] var0) {
      byte var7 = 0;
      int var3 = 0;
      int var2 = -1;

      int var1;
      int var4;
      for(var1 = 0; var1 < var0.length; var2 = var4) {
         int var5;
         for(var5 = var1; var5 < 16 && var0[var5] == 0 && var0[var5 + 1] == 0; var5 += 2) {
            ;
         }

         int var8 = var5 - var1;
         int var6 = var3;
         var4 = var2;
         if(var8 > var3) {
            var6 = var3;
            var4 = var2;
            if(var8 >= 4) {
               var6 = var8;
               var4 = var1;
            }
         }

         var1 = var5 + 2;
         var3 = var6;
      }

      c var9 = new c();
      var1 = var7;

      while(var1 < var0.length) {
         if(var1 == var2) {
            var9.b(58);
            var4 = var1 + var3;
            var1 = var4;
            if(var4 == 16) {
               var9.b(58);
               var1 = var4;
            }
         } else {
            if(var1 > 0) {
               var9.b(58);
            }

            var9.l((long)((var0[var1] & 255) << 8 | var0[var1 + 1] & 255));
            var1 += 2;
         }
      }

      return var9.r();
   }

   public static String[] intersect(Comparator var0, String[] var1, String[] var2) {
      ArrayList var7 = new ArrayList();
      int var5 = var1.length;

      for(int var3 = 0; var3 < var5; ++var3) {
         String var8 = var1[var3];
         int var6 = var2.length;

         for(int var4 = 0; var4 < var6; ++var4) {
            if(var0.compare(var8, var2[var4]) == 0) {
               var7.add(var8);
               break;
            }
         }
      }

      return (String[])var7.toArray(new String[var7.size()]);
   }

   public static boolean isAndroidGetsocknameError(AssertionError var0) {
      boolean var1;
      if(var0.getCause() != null && var0.getMessage() != null && var0.getMessage().contains("getsockname failed")) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public static boolean nonEmptyIntersection(Comparator var0, String[] var1, String[] var2) {
      boolean var8 = false;
      boolean var7 = var8;
      if(var1 != null) {
         var7 = var8;
         if(var2 != null) {
            var7 = var8;
            if(var1.length != 0) {
               if(var2.length == 0) {
                  var7 = var8;
               } else {
                  int var5 = var1.length;
                  int var3 = 0;

                  while(true) {
                     var7 = var8;
                     if(var3 >= var5) {
                        break;
                     }

                     String var9 = var1[var3];
                     int var6 = var2.length;

                     for(int var4 = 0; var4 < var6; ++var4) {
                        if(var0.compare(var9, var2[var4]) == 0) {
                           var7 = true;
                           return var7;
                        }
                     }

                     ++var3;
                  }
               }
            }
         }
      }

      return var7;
   }

   public static boolean skipAll(s param0, int param1, TimeUnit param2) throws IOException {
      // $FF: Couldn't be decompiled
   }

   public static int skipLeadingAsciiWhitespace(String var0, int var1, int var2) {
      while(true) {
         int var3 = var2;
         if(var1 < var2) {
            switch(var0.charAt(var1)) {
            case '\t':
            case '\n':
            case '\f':
            case '\r':
            case ' ':
               ++var1;
               continue;
            default:
               var3 = var1;
            }
         }

         return var3;
      }
   }

   public static int skipTrailingAsciiWhitespace(String var0, int var1, int var2) {
      int var3 = var2 - 1;

      while(true) {
         var2 = var1;
         if(var3 < var1) {
            return var2;
         }

         switch(var0.charAt(var3)) {
         case '\t':
         case '\n':
         case '\f':
         case '\r':
         case ' ':
            --var3;
            break;
         default:
            var2 = var3 + 1;
            return var2;
         }
      }
   }

   public static ThreadFactory threadFactory(final String var0, final boolean var1) {
      return new ThreadFactory() {
         public Thread newThread(Runnable var1x) {
            Thread var2 = new Thread(var1x, var0);
            var2.setDaemon(var1);
            return var2;
         }
      };
   }

   public static String toHumanReadableAscii(String var0) {
      int var4 = var0.length();
      int var1 = 0;

      String var5;
      while(true) {
         var5 = var0;
         if(var1 >= var4) {
            break;
         }

         int var2 = var0.codePointAt(var1);
         if(var2 <= 31 || var2 >= 127) {
            c var6 = new c();
            var6.a((String)var0, 0, var1);

            while(var1 < var4) {
               int var3 = var0.codePointAt(var1);
               if(var3 > 31 && var3 < 127) {
                  var2 = var3;
               } else {
                  var2 = 63;
               }

               var6.a(var2);
               var1 += Character.charCount(var3);
            }

            var5 = var6.r();
            break;
         }

         var1 += Character.charCount(var2);
      }

      return var5;
   }

   public static String trimSubstring(String var0, int var1, int var2) {
      var1 = skipLeadingAsciiWhitespace(var0, var1, var2);
      return var0.substring(var1, skipTrailingAsciiWhitespace(var0, var1, var2));
   }

   public static boolean verifyAsIpAddress(String var0) {
      return VERIFY_AS_IP_ADDRESS.matcher(var0).matches();
   }
}
