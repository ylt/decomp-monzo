package io.intercom.okhttp3.internal.tls;

import java.security.PublicKey;
import java.security.cert.X509Certificate;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import javax.security.auth.x500.X500Principal;

public final class BasicTrustRootIndex implements TrustRootIndex {
   private final Map subjectToCaCerts = new LinkedHashMap();

   public BasicTrustRootIndex(X509Certificate... var1) {
      int var3 = var1.length;

      for(int var2 = 0; var2 < var3; ++var2) {
         X509Certificate var6 = var1[var2];
         X500Principal var7 = var6.getSubjectX500Principal();
         Set var5 = (Set)this.subjectToCaCerts.get(var7);
         Object var4 = var5;
         if(var5 == null) {
            var4 = new LinkedHashSet(1);
            this.subjectToCaCerts.put(var7, var4);
         }

         ((Set)var4).add(var6);
      }

   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(var1 != this && (!(var1 instanceof BasicTrustRootIndex) || !((BasicTrustRootIndex)var1).subjectToCaCerts.equals(this.subjectToCaCerts))) {
         var2 = false;
      }

      return var2;
   }

   public X509Certificate findByIssuerAndSignature(X509Certificate var1) {
      X500Principal var2 = var1.getIssuerX500Principal();
      Set var6 = (Set)this.subjectToCaCerts.get(var2);
      if(var6 == null) {
         var1 = null;
      } else {
         Iterator var3 = var6.iterator();

         while(true) {
            if(var3.hasNext()) {
               X509Certificate var7 = (X509Certificate)var3.next();
               PublicKey var4 = var7.getPublicKey();

               try {
                  var1.verify(var4);
               } catch (Exception var5) {
                  continue;
               }

               var1 = var7;
               break;
            }

            var1 = null;
            break;
         }
      }

      return var1;
   }

   public int hashCode() {
      return this.subjectToCaCerts.hashCode();
   }
}
