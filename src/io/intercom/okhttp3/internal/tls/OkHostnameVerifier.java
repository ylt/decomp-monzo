package io.intercom.okhttp3.internal.tls;

import io.intercom.okhttp3.internal.Util;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLSession;

public final class OkHostnameVerifier implements HostnameVerifier {
   private static final int ALT_DNS_NAME = 2;
   private static final int ALT_IPA_NAME = 7;
   public static final OkHostnameVerifier INSTANCE = new OkHostnameVerifier();

   public static List allSubjectAltNames(X509Certificate var0) {
      List var1 = getSubjectAltNames(var0, 7);
      List var2 = getSubjectAltNames(var0, 2);
      ArrayList var3 = new ArrayList(var1.size() + var2.size());
      var3.addAll(var1);
      var3.addAll(var2);
      return var3;
   }

   private static List getSubjectAltNames(X509Certificate param0, int param1) {
      // $FF: Couldn't be decompiled
   }

   private boolean verifyHostname(String var1, X509Certificate var2) {
      var1 = var1.toLowerCase(Locale.US);
      List var7 = getSubjectAltNames(var2, 2);
      int var5 = var7.size();
      int var4 = 0;
      boolean var3 = false;

      boolean var6;
      while(true) {
         if(var4 >= var5) {
            if(!var3) {
               String var8 = (new DistinguishedNameParser(var2.getSubjectX500Principal())).findMostSpecific("cn");
               if(var8 != null) {
                  var6 = this.verifyHostname(var1, var8);
                  break;
               }
            }

            var6 = false;
            break;
         }

         if(this.verifyHostname(var1, (String)var7.get(var4))) {
            var6 = true;
            break;
         }

         ++var4;
         var3 = true;
      }

      return var6;
   }

   private boolean verifyIpAddress(String var1, X509Certificate var2) {
      List var6 = getSubjectAltNames(var2, 7);
      int var4 = var6.size();
      int var3 = 0;

      boolean var5;
      while(true) {
         if(var3 >= var4) {
            var5 = false;
            break;
         }

         if(var1.equalsIgnoreCase((String)var6.get(var3))) {
            var5 = true;
            break;
         }

         ++var3;
      }

      return var5;
   }

   public boolean verify(String var1, X509Certificate var2) {
      boolean var3;
      if(Util.verifyAsIpAddress(var1)) {
         var3 = this.verifyIpAddress(var1, var2);
      } else {
         var3 = this.verifyHostname(var1, var2);
      }

      return var3;
   }

   public boolean verify(String var1, SSLSession var2) {
      boolean var3;
      try {
         var3 = this.verify(var1, (X509Certificate)var2.getPeerCertificates()[0]);
      } catch (SSLException var4) {
         var3 = false;
      }

      return var3;
   }

   public boolean verifyHostname(String var1, String var2) {
      boolean var5 = false;
      boolean var4 = var5;
      if(var1 != null) {
         var4 = var5;
         if(var1.length() != 0) {
            var4 = var5;
            if(!var1.startsWith(".")) {
               if(var1.endsWith("..")) {
                  var4 = var5;
               } else {
                  var4 = var5;
                  if(var2 != null) {
                     var4 = var5;
                     if(var2.length() != 0) {
                        var4 = var5;
                        if(!var2.startsWith(".")) {
                           var4 = var5;
                           if(!var2.endsWith("..")) {
                              String var6 = var1;
                              if(!var1.endsWith(".")) {
                                 var6 = var1 + '.';
                              }

                              var1 = var2;
                              if(!var2.endsWith(".")) {
                                 var1 = var2 + '.';
                              }

                              var1 = var1.toLowerCase(Locale.US);
                              if(!var1.contains("*")) {
                                 var4 = var6.equals(var1);
                              } else {
                                 var4 = var5;
                                 if(var1.startsWith("*.")) {
                                    var4 = var5;
                                    if(var1.indexOf(42, 1) == -1) {
                                       var4 = var5;
                                       if(var6.length() >= var1.length()) {
                                          var4 = var5;
                                          if(!"*.".equals(var1)) {
                                             var1 = var1.substring(1);
                                             var4 = var5;
                                             if(var6.endsWith(var1)) {
                                                int var3 = var6.length() - var1.length();
                                                if(var3 > 0) {
                                                   var4 = var5;
                                                   if(var6.lastIndexOf(46, var3 - 1) != -1) {
                                                      return var4;
                                                   }
                                                }

                                                var4 = true;
                                             }
                                          }
                                       }
                                    }
                                 }
                              }
                           }
                        }
                     }
                  }
               }
            }
         }
      }

      return var4;
   }
}
