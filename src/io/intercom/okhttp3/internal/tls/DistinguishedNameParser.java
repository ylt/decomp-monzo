package io.intercom.okhttp3.internal.tls;

import javax.security.auth.x500.X500Principal;

final class DistinguishedNameParser {
   private int beg;
   private char[] chars;
   private int cur;
   private final String dn;
   private int end;
   private final int length;
   private int pos;

   DistinguishedNameParser(X500Principal var1) {
      this.dn = var1.getName("RFC2253");
      this.length = this.dn.length();
   }

   private String escapedAV() {
      this.beg = this.pos;
      this.end = this.pos;

      String var3;
      while(this.pos < this.length) {
         int var1;
         char[] var2;
         switch(this.chars[this.pos]) {
         case ' ':
            this.cur = this.end;
            ++this.pos;
            var2 = this.chars;
            var1 = this.end;
            this.end = var1 + 1;

            for(var2[var1] = 32; this.pos < this.length && this.chars[this.pos] == 32; ++this.pos) {
               var2 = this.chars;
               var1 = this.end;
               this.end = var1 + 1;
               var2[var1] = 32;
            }

            if(this.pos == this.length || this.chars[this.pos] == 44 || this.chars[this.pos] == 43 || this.chars[this.pos] == 59) {
               var3 = new String(this.chars, this.beg, this.cur - this.beg);
               return var3;
            }
            break;
         case '+':
         case ',':
         case ';':
            var3 = new String(this.chars, this.beg, this.end - this.beg);
            return var3;
         case '\\':
            var2 = this.chars;
            var1 = this.end;
            this.end = var1 + 1;
            var2[var1] = this.getEscaped();
            ++this.pos;
            break;
         default:
            var2 = this.chars;
            var1 = this.end;
            this.end = var1 + 1;
            var2[var1] = this.chars[this.pos];
            ++this.pos;
         }
      }

      var3 = new String(this.chars, this.beg, this.end - this.beg);
      return var3;
   }

   private int getByte(int var1) {
      if(var1 + 1 >= this.length) {
         throw new IllegalStateException("Malformed DN: " + this.dn);
      } else {
         char var2 = this.chars[var1];
         int var4;
         if(var2 >= 48 && var2 <= 57) {
            var4 = var2 - 48;
         } else if(var2 >= 97 && var2 <= 102) {
            var4 = var2 - 87;
         } else {
            if(var2 < 65 || var2 > 70) {
               throw new IllegalStateException("Malformed DN: " + this.dn);
            }

            var4 = var2 - 55;
         }

         char var3 = this.chars[var1 + 1];
         if(var3 >= 48 && var3 <= 57) {
            var1 = var3 - 48;
         } else if(var3 >= 97 && var3 <= 102) {
            var1 = var3 - 87;
         } else {
            if(var3 < 65 || var3 > 70) {
               throw new IllegalStateException("Malformed DN: " + this.dn);
            }

            var1 = var3 - 55;
         }

         return (var4 << 4) + var1;
      }
   }

   private char getEscaped() {
      ++this.pos;
      if(this.pos == this.length) {
         throw new IllegalStateException("Unexpected end of DN: " + this.dn);
      } else {
         char var1;
         switch(this.chars[this.pos]) {
         case ' ':
         case '"':
         case '#':
         case '%':
         case '*':
         case '+':
         case ',':
         case ';':
         case '<':
         case '=':
         case '>':
         case '\\':
         case '_':
            var1 = this.chars[this.pos];
            break;
         default:
            var1 = this.getUTF8();
         }

         return var1;
      }
   }

   private char getUTF8() {
      int var2 = this.getByte(this.pos);
      ++this.pos;
      char var1;
      if(var2 < 128) {
         var1 = (char)var2;
      } else if(var2 >= 192 && var2 <= 247) {
         byte var3;
         if(var2 <= 223) {
            var3 = 1;
            var2 &= 31;
         } else if(var2 <= 239) {
            var3 = 2;
            var2 &= 15;
         } else {
            var3 = 3;
            var2 &= 7;
         }

         int var4 = 0;

         while(true) {
            if(var4 >= var3) {
               var1 = (char)var2;
               break;
            }

            ++this.pos;
            if(this.pos == this.length || this.chars[this.pos] != 92) {
               var1 = 63;
               break;
            }

            ++this.pos;
            int var5 = this.getByte(this.pos);
            ++this.pos;
            if((var5 & 192) != 128) {
               var1 = 63;
               break;
            }

            var2 = (var2 << 6) + (var5 & 63);
            ++var4;
         }
      } else {
         var1 = 63;
      }

      return var1;
   }

   private String hexAV() {
      if(this.pos + 4 >= this.length) {
         throw new IllegalStateException("Unexpected end of DN: " + this.dn);
      } else {
         this.beg = this.pos++;

         int var1;
         label57:
         while(true) {
            if(this.pos == this.length || this.chars[this.pos] == 43 || this.chars[this.pos] == 44 || this.chars[this.pos] == 59) {
               this.end = this.pos;
               break;
            }

            if(this.chars[this.pos] == 32) {
               this.end = this.pos++;

               while(true) {
                  if(this.pos >= this.length || this.chars[this.pos] != 32) {
                     break label57;
                  }

                  ++this.pos;
               }
            }

            if(this.chars[this.pos] >= 65 && this.chars[this.pos] <= 70) {
               char[] var4 = this.chars;
               var1 = this.pos;
               var4[var1] = (char)(var4[var1] + 32);
            }

            ++this.pos;
         }

         int var3 = this.end - this.beg;
         if(var3 >= 5 && (var3 & 1) != 0) {
            byte[] var5 = new byte[var3 / 2];
            var1 = 0;

            for(int var2 = this.beg + 1; var1 < var5.length; ++var1) {
               var5[var1] = (byte)this.getByte(var2);
               var2 += 2;
            }

            return new String(this.chars, this.beg, var3);
         } else {
            throw new IllegalStateException("Unexpected end of DN: " + this.dn);
         }
      }
   }

   private String nextAT() {
      while(this.pos < this.length && this.chars[this.pos] == 32) {
         ++this.pos;
      }

      String var1;
      if(this.pos == this.length) {
         var1 = null;
      } else {
         for(this.beg = this.pos++; this.pos < this.length && this.chars[this.pos] != 61 && this.chars[this.pos] != 32; ++this.pos) {
            ;
         }

         if(this.pos >= this.length) {
            throw new IllegalStateException("Unexpected end of DN: " + this.dn);
         }

         this.end = this.pos;
         if(this.chars[this.pos] == 32) {
            while(true) {
               if(this.pos >= this.length || this.chars[this.pos] == 61 || this.chars[this.pos] != 32) {
                  if(this.chars[this.pos] != 61 || this.pos == this.length) {
                     throw new IllegalStateException("Unexpected end of DN: " + this.dn);
                  }
                  break;
               }

               ++this.pos;
            }
         }

         ++this.pos;

         while(this.pos < this.length && this.chars[this.pos] == 32) {
            ++this.pos;
         }

         if(this.end - this.beg > 4 && this.chars[this.beg + 3] == 46 && (this.chars[this.beg] == 79 || this.chars[this.beg] == 111) && (this.chars[this.beg + 1] == 73 || this.chars[this.beg + 1] == 105) && (this.chars[this.beg + 2] == 68 || this.chars[this.beg + 2] == 100)) {
            this.beg += 4;
         }

         var1 = new String(this.chars, this.beg, this.end - this.beg);
      }

      return var1;
   }

   private String quotedAV() {
      ++this.pos;
      this.beg = this.pos;

      for(this.end = this.beg; this.pos != this.length; ++this.end) {
         if(this.chars[this.pos] == 34) {
            ++this.pos;

            while(this.pos < this.length && this.chars[this.pos] == 32) {
               ++this.pos;
            }

            return new String(this.chars, this.beg, this.end - this.beg);
         }

         if(this.chars[this.pos] == 92) {
            this.chars[this.end] = this.getEscaped();
         } else {
            this.chars[this.end] = this.chars[this.pos];
         }

         ++this.pos;
      }

      throw new IllegalStateException("Unexpected end of DN: " + this.dn);
   }

   public String findMostSpecific(String var1) {
      this.pos = 0;
      this.beg = 0;
      this.end = 0;
      this.cur = 0;
      this.chars = this.dn.toCharArray();
      String var2 = this.nextAT();
      String var3 = var2;
      if(var2 == null) {
         var2 = null;
      } else {
         while(true) {
            var2 = "";
            if(this.pos == this.length) {
               var2 = null;
               break;
            }

            switch(this.chars[this.pos]) {
            case '"':
               var2 = this.quotedAV();
               break;
            case '#':
               var2 = this.hexAV();
            case '+':
            case ',':
            case ';':
               break;
            default:
               var2 = this.escapedAV();
            }

            if(var1.equalsIgnoreCase(var3)) {
               break;
            }

            if(this.pos >= this.length) {
               var2 = null;
               break;
            }

            if(this.chars[this.pos] != 44 && this.chars[this.pos] != 59 && this.chars[this.pos] != 43) {
               throw new IllegalStateException("Malformed DN: " + this.dn);
            }

            ++this.pos;
            var2 = this.nextAT();
            var3 = var2;
            if(var2 == null) {
               throw new IllegalStateException("Malformed DN: " + this.dn);
            }
         }
      }

      return var2;
   }
}
