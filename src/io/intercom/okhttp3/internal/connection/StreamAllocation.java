package io.intercom.okhttp3.internal.connection;

import io.intercom.okhttp3.Address;
import io.intercom.okhttp3.Call;
import io.intercom.okhttp3.ConnectionPool;
import io.intercom.okhttp3.EventListener;
import io.intercom.okhttp3.Interceptor;
import io.intercom.okhttp3.OkHttpClient;
import io.intercom.okhttp3.Route;
import io.intercom.okhttp3.internal.Internal;
import io.intercom.okhttp3.internal.http.HttpCodec;
import java.io.IOException;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.net.Socket;

public final class StreamAllocation {
   // $FF: synthetic field
   static final boolean $assertionsDisabled;
   public final Address address;
   public final Call call;
   private final Object callStackTrace;
   private boolean canceled;
   private HttpCodec codec;
   private RealConnection connection;
   private final ConnectionPool connectionPool;
   public final EventListener eventListener;
   private int refusedStreamCount;
   private boolean released;
   private boolean reportedAcquired;
   private Route route;
   private RouteSelector.Selection routeSelection;
   private final RouteSelector routeSelector;

   static {
      boolean var0;
      if(!StreamAllocation.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      $assertionsDisabled = var0;
   }

   public StreamAllocation(ConnectionPool var1, Address var2, Call var3, EventListener var4, Object var5) {
      this.connectionPool = var1;
      this.address = var2;
      this.call = var3;
      this.eventListener = var4;
      this.routeSelector = new RouteSelector(var2, this.routeDatabase(), var3, var4);
      this.callStackTrace = var5;
   }

   private Socket deallocate(boolean var1, boolean var2, boolean var3) {
      Object var5 = null;
      if(!$assertionsDisabled && !Thread.holdsLock(this.connectionPool)) {
         throw new AssertionError();
      } else {
         if(var3) {
            this.codec = null;
         }

         if(var2) {
            this.released = true;
         }

         Socket var4 = (Socket)var5;
         if(this.connection != null) {
            if(var1) {
               this.connection.noNewStreams = true;
            }

            var4 = (Socket)var5;
            if(this.codec == null) {
               if(!this.released) {
                  var4 = (Socket)var5;
                  if(!this.connection.noNewStreams) {
                     return var4;
                  }
               }

               label30: {
                  this.release(this.connection);
                  if(this.connection.allocations.isEmpty()) {
                     this.connection.idleAtNanos = System.nanoTime();
                     if(Internal.instance.connectionBecameIdle(this.connectionPool, this.connection)) {
                        var4 = this.connection.socket();
                        break label30;
                     }
                  }

                  var4 = null;
               }

               this.connection = null;
            }
         }

         return var4;
      }
   }

   private RealConnection findConnection(int param1, int param2, int param3, boolean param4) throws IOException {
      // $FF: Couldn't be decompiled
   }

   private RealConnection findHealthyConnection(int param1, int param2, int param3, boolean param4, boolean param5) throws IOException {
      // $FF: Couldn't be decompiled
   }

   private void release(RealConnection var1) {
      int var3 = var1.allocations.size();

      for(int var2 = 0; var2 < var3; ++var2) {
         if(((Reference)var1.allocations.get(var2)).get() == this) {
            var1.allocations.remove(var2);
            return;
         }
      }

      throw new IllegalStateException();
   }

   private Socket releaseIfNoNewStreams() {
      if(!$assertionsDisabled && !Thread.holdsLock(this.connectionPool)) {
         throw new AssertionError();
      } else {
         RealConnection var1 = this.connection;
         Socket var2;
         if(var1 != null && var1.noNewStreams) {
            var2 = this.deallocate(false, false, true);
         } else {
            var2 = null;
         }

         return var2;
      }
   }

   private RouteDatabase routeDatabase() {
      return Internal.instance.routeDatabase(this.connectionPool);
   }

   public void acquire(RealConnection var1, boolean var2) {
      if(!$assertionsDisabled && !Thread.holdsLock(this.connectionPool)) {
         throw new AssertionError();
      } else if(this.connection != null) {
         throw new IllegalStateException();
      } else {
         this.connection = var1;
         this.reportedAcquired = var2;
         var1.allocations.add(new StreamAllocation.StreamAllocationReference(this, this.callStackTrace));
      }
   }

   public void cancel() {
      // $FF: Couldn't be decompiled
   }

   public HttpCodec codec() {
      // $FF: Couldn't be decompiled
   }

   public RealConnection connection() {
      synchronized(this){}

      RealConnection var1;
      try {
         var1 = this.connection;
      } finally {
         ;
      }

      return var1;
   }

   public boolean hasMoreRoutes() {
      boolean var1;
      if(this.route == null && (this.routeSelection == null || !this.routeSelection.hasNext()) && !this.routeSelector.hasNext()) {
         var1 = false;
      } else {
         var1 = true;
      }

      return var1;
   }

   public HttpCodec newStream(OkHttpClient param1, Interceptor.Chain param2, boolean param3) {
      // $FF: Couldn't be decompiled
   }

   public void noNewStreams() {
      // $FF: Couldn't be decompiled
   }

   public void release() {
      // $FF: Couldn't be decompiled
   }

   public Socket releaseAndAcquire(RealConnection var1) {
      if(!$assertionsDisabled && !Thread.holdsLock(this.connectionPool)) {
         throw new AssertionError();
      } else if(this.codec == null && this.connection.allocations.size() == 1) {
         Reference var3 = (Reference)this.connection.allocations.get(0);
         Socket var2 = this.deallocate(true, false, false);
         this.connection = var1;
         var1.allocations.add(var3);
         return var2;
      } else {
         throw new IllegalStateException();
      }
   }

   public void streamFailed(IOException param1) {
      // $FF: Couldn't be decompiled
   }

   public void streamFinished(boolean param1, HttpCodec param2, long param3, IOException param5) {
      // $FF: Couldn't be decompiled
   }

   public String toString() {
      RealConnection var1 = this.connection();
      String var2;
      if(var1 != null) {
         var2 = var1.toString();
      } else {
         var2 = this.address.toString();
      }

      return var2;
   }

   public static final class StreamAllocationReference extends WeakReference {
      public final Object callStackTrace;

      StreamAllocationReference(StreamAllocation var1, Object var2) {
         super(var1);
         this.callStackTrace = var2;
      }
   }
}
