package io.intercom.okhttp3.internal.connection;

import io.intercom.okhttp3.ConnectionSpec;
import io.intercom.okhttp3.internal.Internal;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.ProtocolException;
import java.net.UnknownServiceException;
import java.security.cert.CertificateException;
import java.util.Arrays;
import java.util.List;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLProtocolException;
import javax.net.ssl.SSLSocket;

public final class ConnectionSpecSelector {
   private final List connectionSpecs;
   private boolean isFallback;
   private boolean isFallbackPossible;
   private int nextModeIndex = 0;

   public ConnectionSpecSelector(List var1) {
      this.connectionSpecs = var1;
   }

   private boolean isFallbackPossible(SSLSocket var1) {
      int var2 = this.nextModeIndex;

      boolean var3;
      while(true) {
         if(var2 >= this.connectionSpecs.size()) {
            var3 = false;
            break;
         }

         if(((ConnectionSpec)this.connectionSpecs.get(var2)).isCompatible(var1)) {
            var3 = true;
            break;
         }

         ++var2;
      }

      return var3;
   }

   public ConnectionSpec configureSecureSocket(SSLSocket var1) throws IOException {
      int var2 = this.nextModeIndex;
      int var3 = this.connectionSpecs.size();

      ConnectionSpec var4;
      while(true) {
         if(var2 >= var3) {
            var4 = null;
            break;
         }

         var4 = (ConnectionSpec)this.connectionSpecs.get(var2);
         if(var4.isCompatible(var1)) {
            this.nextModeIndex = var2 + 1;
            break;
         }

         ++var2;
      }

      if(var4 == null) {
         throw new UnknownServiceException("Unable to find acceptable protocols. isFallback=" + this.isFallback + ", modes=" + this.connectionSpecs + ", supported protocols=" + Arrays.toString(var1.getEnabledProtocols()));
      } else {
         this.isFallbackPossible = this.isFallbackPossible(var1);
         Internal.instance.apply(var4, var1, this.isFallback);
         return var4;
      }
   }

   public boolean connectionFailed(IOException var1) {
      boolean var3 = false;
      this.isFallback = true;
      boolean var2;
      if(!this.isFallbackPossible) {
         var2 = var3;
      } else {
         var2 = var3;
         if(!(var1 instanceof ProtocolException)) {
            var2 = var3;
            if(!(var1 instanceof InterruptedIOException)) {
               if(var1 instanceof SSLHandshakeException) {
                  var2 = var3;
                  if(var1.getCause() instanceof CertificateException) {
                     return var2;
                  }
               }

               var2 = var3;
               if(!(var1 instanceof SSLPeerUnverifiedException)) {
                  if(!(var1 instanceof SSLHandshakeException)) {
                     var2 = var3;
                     if(!(var1 instanceof SSLProtocolException)) {
                        return var2;
                     }
                  }

                  var2 = true;
               }
            }
         }
      }

      return var2;
   }
}
