package io.intercom.okhttp3.internal.connection;

import io.intercom.a.d;
import io.intercom.a.e;
import io.intercom.a.l;
import io.intercom.a.s;
import io.intercom.okhttp3.Address;
import io.intercom.okhttp3.Call;
import io.intercom.okhttp3.Connection;
import io.intercom.okhttp3.ConnectionPool;
import io.intercom.okhttp3.EventListener;
import io.intercom.okhttp3.Handshake;
import io.intercom.okhttp3.HttpUrl;
import io.intercom.okhttp3.Interceptor;
import io.intercom.okhttp3.OkHttpClient;
import io.intercom.okhttp3.Protocol;
import io.intercom.okhttp3.Request;
import io.intercom.okhttp3.Response;
import io.intercom.okhttp3.Route;
import io.intercom.okhttp3.internal.Internal;
import io.intercom.okhttp3.internal.Util;
import io.intercom.okhttp3.internal.Version;
import io.intercom.okhttp3.internal.http.HttpCodec;
import io.intercom.okhttp3.internal.http.HttpHeaders;
import io.intercom.okhttp3.internal.http1.Http1Codec;
import io.intercom.okhttp3.internal.http2.ErrorCode;
import io.intercom.okhttp3.internal.http2.Http2Codec;
import io.intercom.okhttp3.internal.http2.Http2Connection;
import io.intercom.okhttp3.internal.http2.Http2Stream;
import io.intercom.okhttp3.internal.platform.Platform;
import io.intercom.okhttp3.internal.tls.OkHostnameVerifier;
import io.intercom.okhttp3.internal.ws.RealWebSocket;
import java.io.IOException;
import java.net.ConnectException;
import java.net.Proxy;
import java.net.Socket;
import java.net.SocketException;
import java.net.Proxy.Type;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.annotation.Nullable;
import javax.net.ssl.SSLPeerUnverifiedException;

public final class RealConnection extends Http2Connection.Listener implements Connection {
   private static final int MAX_TUNNEL_ATTEMPTS = 21;
   private static final String NPE_THROW_WITH_NULL = "throw with null exception";
   public int allocationLimit = 1;
   public final List allocations = new ArrayList();
   private final ConnectionPool connectionPool;
   private Handshake handshake;
   private Http2Connection http2Connection;
   public long idleAtNanos = Long.MAX_VALUE;
   public boolean noNewStreams;
   private Protocol protocol;
   private Socket rawSocket;
   private final Route route;
   private d sink;
   private Socket socket;
   private e source;
   public int successCount;

   public RealConnection(ConnectionPool var1, Route var2) {
      this.connectionPool = var1;
      this.route = var2;
   }

   private void connectSocket(int var1, int var2, Call var3, EventListener var4) throws IOException {
      Proxy var6 = this.route.proxy();
      Address var5 = this.route.address();
      Socket var10;
      if(var6.type() != Type.DIRECT && var6.type() != Type.HTTP) {
         var10 = new Socket(var6);
      } else {
         var10 = var5.socketFactory().createSocket();
      }

      this.rawSocket = var10;
      var4.connectStart(var3, this.route.socketAddress(), var6);
      this.rawSocket.setSoTimeout(var2);

      try {
         Platform.get().connectSocket(this.rawSocket, this.route.socketAddress(), var1);
      } catch (ConnectException var7) {
         ConnectException var9 = new ConnectException("Failed to connect to " + this.route.socketAddress());
         var9.initCause(var7);
         throw var9;
      }

      try {
         this.source = l.a(l.b(this.rawSocket));
         this.sink = l.a(l.a(this.rawSocket));
      } catch (NullPointerException var8) {
         if("throw with null exception".equals(var8.getMessage())) {
            throw new IOException(var8);
         }
      }

   }

   private void connectTls(ConnectionSpecSelector param1) throws IOException {
      // $FF: Couldn't be decompiled
   }

   private void connectTunnel(int var1, int var2, int var3, Call var4, EventListener var5) throws IOException {
      Request var7 = this.createTunnelRequest();
      HttpUrl var8 = var7.url();

      for(int var6 = 0; var6 < 21; ++var6) {
         this.connectSocket(var1, var2, var4, var5);
         var7 = this.createTunnel(var2, var3, var7, var8);
         if(var7 == null) {
            break;
         }

         Util.closeQuietly(this.rawSocket);
         this.rawSocket = null;
         this.sink = null;
         this.source = null;
         var5.connectEnd(var4, this.route.socketAddress(), this.route.proxy(), (Protocol)null);
      }

   }

   private Request createTunnel(int var1, int var2, Request var3, HttpUrl var4) throws IOException {
      String var9 = "CONNECT " + Util.hostHeader(var4, true) + " HTTP/1.1";

      while(true) {
         Http1Codec var12 = new Http1Codec((OkHttpClient)null, (StreamAllocation)null, this.source, this.sink);
         this.source.timeout().timeout((long)var1, TimeUnit.MILLISECONDS);
         this.sink.timeout().timeout((long)var2, TimeUnit.MILLISECONDS);
         var12.writeRequest(var3.headers(), var9);
         var12.finishRequest();
         Response var10 = var12.readResponseHeaders(false).request(var3).build();
         long var7 = HttpHeaders.contentLength(var10);
         long var5 = var7;
         if(var7 == -1L) {
            var5 = 0L;
         }

         s var11 = var12.newFixedLengthSource(var5);
         Util.skipAll(var11, Integer.MAX_VALUE, TimeUnit.MILLISECONDS);
         var11.close();
         Request var13;
         switch(var10.code()) {
         case 200:
            if(!this.source.b().f() || !this.sink.b().f()) {
               throw new IOException("TLS tunnel buffered too many bytes!");
            }

            var13 = null;
            break;
         case 407:
            var13 = this.route.address().proxyAuthenticator().authenticate(this.route, var10);
            if(var13 == null) {
               throw new IOException("Failed to authenticate with proxy");
            }

            var3 = var13;
            if(!"close".equalsIgnoreCase(var10.header("Connection"))) {
               continue;
            }
            break;
         default:
            throw new IOException("Unexpected response code for CONNECT: " + var10.code());
         }

         return var13;
      }
   }

   private Request createTunnelRequest() {
      return (new Request.Builder()).url(this.route.address().url()).header("Host", Util.hostHeader(this.route.address().url(), true)).header("Proxy-Connection", "Keep-Alive").header("User-Agent", Version.userAgent()).build();
   }

   private void establishProtocol(ConnectionSpecSelector var1, Call var2, EventListener var3) throws IOException {
      if(this.route.address().sslSocketFactory() == null) {
         this.protocol = Protocol.HTTP_1_1;
         this.socket = this.rawSocket;
      } else {
         var3.secureConnectStart(var2);
         this.connectTls(var1);
         var3.secureConnectEnd(var2, this.handshake);
         if(this.protocol == Protocol.HTTP_2) {
            this.socket.setSoTimeout(0);
            this.http2Connection = (new Http2Connection.Builder(true)).socket(this.socket, this.route.address().url().host(), this.source, this.sink).listener(this).build();
            this.http2Connection.start();
         }
      }

   }

   public static RealConnection testConnection(ConnectionPool var0, Route var1, Socket var2, long var3) {
      RealConnection var5 = new RealConnection(var0, var1);
      var5.socket = var2;
      var5.idleAtNanos = var3;
      return var5;
   }

   public void cancel() {
      Util.closeQuietly(this.rawSocket);
   }

   public void connect(int param1, int param2, int param3, boolean param4, Call param5, EventListener param6) {
      // $FF: Couldn't be decompiled
   }

   public Handshake handshake() {
      return this.handshake;
   }

   public boolean isEligible(Address var1, @Nullable Route var2) {
      boolean var4 = false;
      boolean var3 = var4;
      if(this.allocations.size() < this.allocationLimit) {
         if(this.noNewStreams) {
            var3 = var4;
         } else {
            var3 = var4;
            if(Internal.instance.equalsNonHost(this.route.address(), var1)) {
               if(var1.url().host().equals(this.route().address().url().host())) {
                  var3 = true;
               } else {
                  var3 = var4;
                  if(this.http2Connection != null) {
                     var3 = var4;
                     if(var2 != null) {
                        var3 = var4;
                        if(var2.proxy().type() == Type.DIRECT) {
                           var3 = var4;
                           if(this.route.proxy().type() == Type.DIRECT) {
                              var3 = var4;
                              if(this.route.socketAddress().equals(var2.socketAddress())) {
                                 var3 = var4;
                                 if(var2.address().hostnameVerifier() == OkHostnameVerifier.INSTANCE) {
                                    var3 = var4;
                                    if(this.supportsUrl(var1.url())) {
                                       try {
                                          var1.certificatePinner().check(var1.url().host(), this.handshake().peerCertificates());
                                       } catch (SSLPeerUnverifiedException var5) {
                                          var3 = var4;
                                          return var3;
                                       }

                                       var3 = true;
                                    }
                                 }
                              }
                           }
                        }
                     }
                  }
               }
            }
         }
      }

      return var3;
   }

   public boolean isHealthy(boolean param1) {
      // $FF: Couldn't be decompiled
   }

   public boolean isMultiplexed() {
      boolean var1;
      if(this.http2Connection != null) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public HttpCodec newCodec(OkHttpClient var1, Interceptor.Chain var2, StreamAllocation var3) throws SocketException {
      Object var4;
      if(this.http2Connection != null) {
         var4 = new Http2Codec(var1, var2, var3, this.http2Connection);
      } else {
         this.socket.setSoTimeout(var2.readTimeoutMillis());
         this.source.timeout().timeout((long)var2.readTimeoutMillis(), TimeUnit.MILLISECONDS);
         this.sink.timeout().timeout((long)var2.writeTimeoutMillis(), TimeUnit.MILLISECONDS);
         var4 = new Http1Codec(var1, var3, this.source, this.sink);
      }

      return (HttpCodec)var4;
   }

   public RealWebSocket.Streams newWebSocketStreams(final StreamAllocation var1) {
      return new RealWebSocket.Streams(true, this.source, this.sink) {
         public void close() throws IOException {
            var1.streamFinished(true, var1.codec(), -1L, (IOException)null);
         }
      };
   }

   public void onSettings(Http2Connection param1) {
      // $FF: Couldn't be decompiled
   }

   public void onStream(Http2Stream var1) throws IOException {
      var1.close(ErrorCode.REFUSED_STREAM);
   }

   public Protocol protocol() {
      return this.protocol;
   }

   public Route route() {
      return this.route;
   }

   public Socket socket() {
      return this.socket;
   }

   public boolean supportsUrl(HttpUrl var1) {
      boolean var2 = false;
      if(var1.port() == this.route.address().url().port()) {
         if(!var1.host().equals(this.route.address().url().host())) {
            if(this.handshake != null && OkHostnameVerifier.INSTANCE.verify(var1.host(), (X509Certificate)this.handshake.peerCertificates().get(0))) {
               var2 = true;
            } else {
               var2 = false;
            }
         } else {
            var2 = true;
         }
      }

      return var2;
   }

   public String toString() {
      StringBuilder var2 = (new StringBuilder()).append("Connection{").append(this.route.address().url().host()).append(":").append(this.route.address().url().port()).append(", proxy=").append(this.route.proxy()).append(" hostAddress=").append(this.route.socketAddress()).append(" cipherSuite=");
      Object var1;
      if(this.handshake != null) {
         var1 = this.handshake.cipherSuite();
      } else {
         var1 = "none";
      }

      return var2.append(var1).append(" protocol=").append(this.protocol).append('}').toString();
   }
}
