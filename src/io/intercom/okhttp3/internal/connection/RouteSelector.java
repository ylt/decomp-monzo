package io.intercom.okhttp3.internal.connection;

import io.intercom.okhttp3.Address;
import io.intercom.okhttp3.Call;
import io.intercom.okhttp3.EventListener;
import io.intercom.okhttp3.HttpUrl;
import io.intercom.okhttp3.Route;
import io.intercom.okhttp3.internal.Util;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.SocketAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.net.Proxy.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;

public final class RouteSelector {
   private final Address address;
   private final Call call;
   private final EventListener eventListener;
   private List inetSocketAddresses = Collections.emptyList();
   private int nextProxyIndex;
   private final List postponedRoutes = new ArrayList();
   private List proxies = Collections.emptyList();
   private final RouteDatabase routeDatabase;

   public RouteSelector(Address var1, RouteDatabase var2, Call var3, EventListener var4) {
      this.address = var1;
      this.routeDatabase = var2;
      this.call = var3;
      this.eventListener = var4;
      this.resetNextProxy(var1.url(), var1.proxy());
   }

   static String getHostString(InetSocketAddress var0) {
      InetAddress var1 = var0.getAddress();
      String var2;
      if(var1 == null) {
         var2 = var0.getHostName();
      } else {
         var2 = var1.getHostAddress();
      }

      return var2;
   }

   private boolean hasNextProxy() {
      boolean var1;
      if(this.nextProxyIndex < this.proxies.size()) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   private Proxy nextProxy() throws IOException {
      if(!this.hasNextProxy()) {
         throw new SocketException("No route to " + this.address.url().host() + "; exhausted proxy configurations: " + this.proxies);
      } else {
         List var2 = this.proxies;
         int var1 = this.nextProxyIndex;
         this.nextProxyIndex = var1 + 1;
         Proxy var3 = (Proxy)var2.get(var1);
         this.resetNextInetSocketAddress(var3);
         return var3;
      }
   }

   private void resetNextInetSocketAddress(Proxy var1) throws IOException {
      this.inetSocketAddresses = new ArrayList();
      int var2;
      String var5;
      if(var1.type() != Type.DIRECT && var1.type() != Type.SOCKS) {
         SocketAddress var8 = var1.address();
         if(!(var8 instanceof InetSocketAddress)) {
            throw new IllegalArgumentException("Proxy.address() is not an InetSocketAddress: " + var8.getClass());
         }

         InetSocketAddress var6 = (InetSocketAddress)var8;
         var5 = getHostString(var6);
         var2 = var6.getPort();
      } else {
         var5 = this.address.url().host();
         var2 = this.address.url().port();
      }

      if(var2 >= 1 && var2 <= '\uffff') {
         if(var1.type() == Type.SOCKS) {
            this.inetSocketAddresses.add(InetSocketAddress.createUnresolved(var5, var2));
         } else {
            this.eventListener.dnsStart(this.call, var5);
            List var7 = this.address.dns().lookup(var5);
            if(var7.isEmpty()) {
               throw new UnknownHostException(this.address.dns() + " returned no addresses for " + var5);
            }

            this.eventListener.dnsEnd(this.call, var5, var7);
            int var4 = var7.size();

            for(int var3 = 0; var3 < var4; ++var3) {
               InetAddress var9 = (InetAddress)var7.get(var3);
               this.inetSocketAddresses.add(new InetSocketAddress(var9, var2));
            }
         }

      } else {
         throw new SocketException("No route to " + var5 + ":" + var2 + "; port is out of range");
      }
   }

   private void resetNextProxy(HttpUrl var1, Proxy var2) {
      if(var2 != null) {
         this.proxies = Collections.singletonList(var2);
      } else {
         List var3 = this.address.proxySelector().select(var1.uri());
         if(var3 != null && !var3.isEmpty()) {
            var3 = Util.immutableList(var3);
         } else {
            var3 = Util.immutableList((Object[])(new Proxy[]{Proxy.NO_PROXY}));
         }

         this.proxies = var3;
      }

      this.nextProxyIndex = 0;
   }

   public void connectFailed(Route var1, IOException var2) {
      if(var1.proxy().type() != Type.DIRECT && this.address.proxySelector() != null) {
         this.address.proxySelector().connectFailed(this.address.url().uri(), var1.proxy().address(), var2);
      }

      this.routeDatabase.failed(var1);
   }

   public boolean hasNext() {
      boolean var1;
      if(!this.hasNextProxy() && this.postponedRoutes.isEmpty()) {
         var1 = false;
      } else {
         var1 = true;
      }

      return var1;
   }

   public RouteSelector.Selection next() throws IOException {
      if(!this.hasNext()) {
         throw new NoSuchElementException();
      } else {
         ArrayList var4 = new ArrayList();

         while(this.hasNextProxy()) {
            Proxy var5 = this.nextProxy();
            int var2 = this.inetSocketAddresses.size();

            for(int var1 = 0; var1 < var2; ++var1) {
               Route var3 = new Route(this.address, var5, (InetSocketAddress)this.inetSocketAddresses.get(var1));
               if(this.routeDatabase.shouldPostpone(var3)) {
                  this.postponedRoutes.add(var3);
               } else {
                  var4.add(var3);
               }
            }

            if(!var4.isEmpty()) {
               break;
            }
         }

         if(var4.isEmpty()) {
            var4.addAll(this.postponedRoutes);
            this.postponedRoutes.clear();
         }

         return new RouteSelector.Selection(var4);
      }
   }

   public static final class Selection {
      private int nextRouteIndex = 0;
      private final List routes;

      Selection(List var1) {
         this.routes = var1;
      }

      public List getAll() {
         return new ArrayList(this.routes);
      }

      public boolean hasNext() {
         boolean var1;
         if(this.nextRouteIndex < this.routes.size()) {
            var1 = true;
         } else {
            var1 = false;
         }

         return var1;
      }

      public Route next() {
         if(!this.hasNext()) {
            throw new NoSuchElementException();
         } else {
            List var2 = this.routes;
            int var1 = this.nextRouteIndex;
            this.nextRouteIndex = var1 + 1;
            return (Route)var2.get(var1);
         }
      }
   }
}
