package io.intercom.okhttp3.internal.connection;

import io.intercom.okhttp3.Interceptor;
import io.intercom.okhttp3.OkHttpClient;
import io.intercom.okhttp3.Request;
import io.intercom.okhttp3.Response;
import io.intercom.okhttp3.internal.http.RealInterceptorChain;
import java.io.IOException;

public final class ConnectInterceptor implements Interceptor {
   public final OkHttpClient client;

   public ConnectInterceptor(OkHttpClient var1) {
      this.client = var1;
   }

   public Response intercept(Interceptor.Chain var1) throws IOException {
      RealInterceptorChain var5 = (RealInterceptorChain)var1;
      Request var4 = var5.request();
      StreamAllocation var3 = var5.streamAllocation();
      boolean var2;
      if(!var4.method().equals("GET")) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var5.proceed(var4, var3, var3.newStream(this.client, var1, var2), var3.connection());
   }
}
