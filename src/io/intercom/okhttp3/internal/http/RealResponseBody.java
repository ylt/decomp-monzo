package io.intercom.okhttp3.internal.http;

import io.intercom.a.e;
import io.intercom.okhttp3.MediaType;
import io.intercom.okhttp3.ResponseBody;
import javax.annotation.Nullable;

public final class RealResponseBody extends ResponseBody {
   private final long contentLength;
   @Nullable
   private final String contentTypeString;
   private final e source;

   public RealResponseBody(@Nullable String var1, long var2, e var4) {
      this.contentTypeString = var1;
      this.contentLength = var2;
      this.source = var4;
   }

   public long contentLength() {
      return this.contentLength;
   }

   public MediaType contentType() {
      MediaType var1;
      if(this.contentTypeString != null) {
         var1 = MediaType.parse(this.contentTypeString);
      } else {
         var1 = null;
      }

      return var1;
   }

   public e source() {
      return this.source;
   }
}
