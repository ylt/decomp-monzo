package io.intercom.okhttp3.internal.http;

import io.intercom.a.r;
import io.intercom.okhttp3.Request;
import io.intercom.okhttp3.Response;
import io.intercom.okhttp3.ResponseBody;
import java.io.IOException;

public interface HttpCodec {
   int DISCARD_STREAM_TIMEOUT_MILLIS = 100;

   void cancel();

   r createRequestBody(Request var1, long var2);

   void finishRequest() throws IOException;

   void flushRequest() throws IOException;

   ResponseBody openResponseBody(Response var1) throws IOException;

   Response.Builder readResponseHeaders(boolean var1) throws IOException;

   void writeRequestHeaders(Request var1) throws IOException;
}
