package io.intercom.okhttp3.internal.http;

import io.intercom.okhttp3.Call;
import io.intercom.okhttp3.Connection;
import io.intercom.okhttp3.EventListener;
import io.intercom.okhttp3.Interceptor;
import io.intercom.okhttp3.Request;
import io.intercom.okhttp3.Response;
import io.intercom.okhttp3.internal.Util;
import io.intercom.okhttp3.internal.connection.RealConnection;
import io.intercom.okhttp3.internal.connection.StreamAllocation;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

public final class RealInterceptorChain implements Interceptor.Chain {
   private final Call call;
   private int calls;
   private final int connectTimeout;
   private final RealConnection connection;
   private final EventListener eventListener;
   private final HttpCodec httpCodec;
   private final int index;
   private final List interceptors;
   private final int readTimeout;
   private final Request request;
   private final StreamAllocation streamAllocation;
   private final int writeTimeout;

   public RealInterceptorChain(List var1, StreamAllocation var2, HttpCodec var3, RealConnection var4, int var5, Request var6, Call var7, EventListener var8, int var9, int var10, int var11) {
      this.interceptors = var1;
      this.connection = var4;
      this.streamAllocation = var2;
      this.httpCodec = var3;
      this.index = var5;
      this.request = var6;
      this.call = var7;
      this.eventListener = var8;
      this.connectTimeout = var9;
      this.readTimeout = var10;
      this.writeTimeout = var11;
   }

   public Call call() {
      return this.call;
   }

   public int connectTimeoutMillis() {
      return this.connectTimeout;
   }

   public Connection connection() {
      return this.connection;
   }

   public EventListener eventListener() {
      return this.eventListener;
   }

   public HttpCodec httpStream() {
      return this.httpCodec;
   }

   public Response proceed(Request var1) throws IOException {
      return this.proceed(var1, this.streamAllocation, this.httpCodec, this.connection);
   }

   public Response proceed(Request var1, StreamAllocation var2, HttpCodec var3, RealConnection var4) throws IOException {
      if(this.index >= this.interceptors.size()) {
         throw new AssertionError();
      } else {
         ++this.calls;
         if(this.httpCodec != null && !this.connection.supportsUrl(var1.url())) {
            throw new IllegalStateException("network interceptor " + this.interceptors.get(this.index - 1) + " must retain the same host and port");
         } else if(this.httpCodec != null && this.calls > 1) {
            throw new IllegalStateException("network interceptor " + this.interceptors.get(this.index - 1) + " must call proceed() exactly once");
         } else {
            RealInterceptorChain var6 = new RealInterceptorChain(this.interceptors, var2, var3, var4, this.index + 1, var1, this.call, this.eventListener, this.connectTimeout, this.readTimeout, this.writeTimeout);
            Interceptor var7 = (Interceptor)this.interceptors.get(this.index);
            Response var5 = var7.intercept(var6);
            if(var3 != null && this.index + 1 < this.interceptors.size() && var6.calls != 1) {
               throw new IllegalStateException("network interceptor " + var7 + " must call proceed() exactly once");
            } else if(var5 == null) {
               throw new NullPointerException("interceptor " + var7 + " returned null");
            } else if(var5.body() == null) {
               throw new IllegalStateException("interceptor " + var7 + " returned a response with no body");
            } else {
               return var5;
            }
         }
      }
   }

   public int readTimeoutMillis() {
      return this.readTimeout;
   }

   public Request request() {
      return this.request;
   }

   public StreamAllocation streamAllocation() {
      return this.streamAllocation;
   }

   public Interceptor.Chain withConnectTimeout(int var1, TimeUnit var2) {
      var1 = Util.checkDuration("timeout", (long)var1, var2);
      return new RealInterceptorChain(this.interceptors, this.streamAllocation, this.httpCodec, this.connection, this.index, this.request, this.call, this.eventListener, var1, this.readTimeout, this.writeTimeout);
   }

   public Interceptor.Chain withReadTimeout(int var1, TimeUnit var2) {
      var1 = Util.checkDuration("timeout", (long)var1, var2);
      return new RealInterceptorChain(this.interceptors, this.streamAllocation, this.httpCodec, this.connection, this.index, this.request, this.call, this.eventListener, this.connectTimeout, var1, this.writeTimeout);
   }

   public Interceptor.Chain withWriteTimeout(int var1, TimeUnit var2) {
      var1 = Util.checkDuration("timeout", (long)var1, var2);
      return new RealInterceptorChain(this.interceptors, this.streamAllocation, this.httpCodec, this.connection, this.index, this.request, this.call, this.eventListener, this.connectTimeout, this.readTimeout, var1);
   }

   public int writeTimeoutMillis() {
      return this.writeTimeout;
   }
}
