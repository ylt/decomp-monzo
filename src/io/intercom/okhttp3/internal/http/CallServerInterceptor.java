package io.intercom.okhttp3.internal.http;

import io.intercom.a.c;
import io.intercom.a.d;
import io.intercom.a.g;
import io.intercom.a.l;
import io.intercom.a.r;
import io.intercom.okhttp3.Interceptor;
import io.intercom.okhttp3.Request;
import io.intercom.okhttp3.Response;
import io.intercom.okhttp3.internal.Util;
import io.intercom.okhttp3.internal.connection.RealConnection;
import io.intercom.okhttp3.internal.connection.StreamAllocation;
import java.io.IOException;
import java.net.ProtocolException;

public final class CallServerInterceptor implements Interceptor {
   private final boolean forWebSocket;

   public CallServerInterceptor(boolean var1) {
      this.forWebSocket = var1;
   }

   public Response intercept(Interceptor.Chain var1) throws IOException {
      RealInterceptorChain var8 = (RealInterceptorChain)var1;
      HttpCodec var7 = var8.httpStream();
      StreamAllocation var6 = var8.streamAllocation();
      RealConnection var5 = (RealConnection)var8.connection();
      Request var9 = var8.request();
      long var3 = System.currentTimeMillis();
      var8.eventListener().requestHeadersStart(var8.call());
      var7.writeRequestHeaders(var9);
      var8.eventListener().requestHeadersEnd(var8.call(), var9);
      Response.Builder var11 = null;
      if(HttpMethod.permitsRequestBody(var9.method()) && var9.body() != null) {
         if("100-continue".equalsIgnoreCase(var9.header("Expect"))) {
            var7.flushRequest();
            var8.eventListener().responseHeadersStart(var8.call());
            var11 = var7.readResponseHeaders(true);
         }

         if(var11 == null) {
            var8.eventListener().requestBodyStart(var8.call());
            CallServerInterceptor.CountingSink var13 = new CallServerInterceptor.CountingSink(var7.createRequestBody(var9, var9.body().contentLength()));
            d var10 = l.a((r)var13);
            var9.body().writeTo(var10);
            var10.close();
            var8.eventListener().requestBodyEnd(var8.call(), var13.successfulCount);
         } else if(!var5.isMultiplexed()) {
            var6.noNewStreams();
         }
      } else {
         var11 = null;
      }

      var7.finishRequest();
      Response.Builder var14 = var11;
      if(var11 == null) {
         var8.eventListener().responseHeadersStart(var8.call());
         var14 = var7.readResponseHeaders(false);
      }

      Response var12 = var14.request(var9).handshake(var6.connection().handshake()).sentRequestAtMillis(var3).receivedResponseAtMillis(System.currentTimeMillis()).build();
      var8.eventListener().responseHeadersEnd(var8.call(), var12);
      int var2 = var12.code();
      if(this.forWebSocket && var2 == 101) {
         var12 = var12.newBuilder().body(Util.EMPTY_RESPONSE).build();
      } else {
         var12 = var12.newBuilder().body(var7.openResponseBody(var12)).build();
      }

      if("close".equalsIgnoreCase(var12.request().header("Connection")) || "close".equalsIgnoreCase(var12.header("Connection"))) {
         var6.noNewStreams();
      }

      if((var2 == 204 || var2 == 205) && var12.body().contentLength() > 0L) {
         throw new ProtocolException("HTTP " + var2 + " had non-zero Content-Length: " + var12.body().contentLength());
      } else {
         return var12;
      }
   }

   static final class CountingSink extends g {
      long successfulCount;

      CountingSink(r var1) {
         super(var1);
      }

      public void write(c var1, long var2) throws IOException {
         super.write(var1, var2);
         this.successfulCount += var2;
      }
   }
}
