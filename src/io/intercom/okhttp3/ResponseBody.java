package io.intercom.okhttp3;

import io.intercom.a.c;
import io.intercom.a.e;
import io.intercom.okhttp3.internal.Util;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import javax.annotation.Nullable;

public abstract class ResponseBody implements Closeable {
   private Reader reader;

   private Charset charset() {
      MediaType var1 = this.contentType();
      Charset var2;
      if(var1 != null) {
         var2 = var1.charset(Util.UTF_8);
      } else {
         var2 = Util.UTF_8;
      }

      return var2;
   }

   public static ResponseBody create(@Nullable final MediaType var0, final long var1, final e var3) {
      if(var3 == null) {
         throw new NullPointerException("source == null");
      } else {
         return new ResponseBody() {
            public long contentLength() {
               return var1;
            }

            @Nullable
            public MediaType contentType() {
               return var0;
            }

            public e source() {
               return var3;
            }
         };
      }
   }

   public static ResponseBody create(@Nullable MediaType var0, String var1) {
      Charset var2 = Util.UTF_8;
      MediaType var3 = var0;
      if(var0 != null) {
         Charset var4 = var0.charset();
         var2 = var4;
         var3 = var0;
         if(var4 == null) {
            var2 = Util.UTF_8;
            var3 = MediaType.parse(var0 + "; charset=utf-8");
         }
      }

      c var5 = (new c()).a(var1, var2);
      return create(var3, var5.a(), var5);
   }

   public static ResponseBody create(@Nullable MediaType var0, byte[] var1) {
      c var2 = (new c()).b(var1);
      return create(var0, (long)var1.length, var2);
   }

   public final InputStream byteStream() {
      return this.source().g();
   }

   public final byte[] bytes() throws IOException {
      long var1 = this.contentLength();
      if(var1 > 2147483647L) {
         throw new IOException("Cannot buffer entire body for content length: " + var1);
      } else {
         e var3 = this.source();

         byte[] var4;
         try {
            var4 = var3.t();
         } finally {
            Util.closeQuietly((Closeable)var3);
         }

         if(var1 != -1L && var1 != (long)var4.length) {
            throw new IOException("Content-Length (" + var1 + ") and stream length (" + var4.length + ") disagree");
         } else {
            return var4;
         }
      }
   }

   public final Reader charStream() {
      Object var1 = this.reader;
      if(var1 == null) {
         var1 = new ResponseBody.BomAwareReader(this.source(), this.charset());
         this.reader = (Reader)var1;
      }

      return (Reader)var1;
   }

   public void close() {
      Util.closeQuietly((Closeable)this.source());
   }

   public abstract long contentLength();

   @Nullable
   public abstract MediaType contentType();

   public abstract e source();

   public final String string() throws IOException {
      e var1 = this.source();

      String var2;
      try {
         var2 = var1.a(Util.bomAwareCharset(var1, this.charset()));
      } finally {
         Util.closeQuietly((Closeable)var1);
      }

      return var2;
   }

   static final class BomAwareReader extends Reader {
      private final Charset charset;
      private boolean closed;
      private Reader delegate;
      private final e source;

      BomAwareReader(e var1, Charset var2) {
         this.source = var1;
         this.charset = var2;
      }

      public void close() throws IOException {
         this.closed = true;
         if(this.delegate != null) {
            this.delegate.close();
         } else {
            this.source.close();
         }

      }

      public int read(char[] var1, int var2, int var3) throws IOException {
         if(this.closed) {
            throw new IOException("Stream closed");
         } else {
            Reader var5 = this.delegate;
            Object var4 = var5;
            if(var5 == null) {
               Charset var6 = Util.bomAwareCharset(this.source, this.charset);
               var4 = new InputStreamReader(this.source.g(), var6);
               this.delegate = (Reader)var4;
            }

            return ((Reader)var4).read(var1, var2, var3);
         }
      }
   }
}
