package io.intercom.com.a.a;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

class d {
   private final Object a;
   private final Method b;
   private final int c;
   private boolean d = true;

   d(Object var1, Method var2) {
      if(var1 == null) {
         throw new NullPointerException("EventHandler target cannot be null.");
      } else if(var2 == null) {
         throw new NullPointerException("EventHandler method cannot be null.");
      } else {
         this.a = var1;
         this.b = var2;
         var2.setAccessible(true);
         this.c = (var2.hashCode() + 31) * 31 + var1.hashCode();
      }
   }

   public void a(Object var1) throws InvocationTargetException {
      if(!this.d) {
         throw new IllegalStateException(this.toString() + " has been invalidated and can no longer handle events.");
      } else {
         try {
            this.b.invoke(this.a, new Object[]{var1});
         } catch (IllegalAccessException var2) {
            throw new AssertionError(var2);
         } catch (InvocationTargetException var3) {
            if(var3.getCause() instanceof Error) {
               throw (Error)var3.getCause();
            } else {
               throw var3;
            }
         }
      }
   }

   public boolean a() {
      return this.d;
   }

   public void b() {
      this.d = false;
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(this != var1) {
         if(var1 == null) {
            var2 = false;
         } else if(this.getClass() != var1.getClass()) {
            var2 = false;
         } else {
            d var3 = (d)var1;
            if(!this.b.equals(var3.b) || this.a != var3.a) {
               var2 = false;
            }
         }
      }

      return var2;
   }

   public int hashCode() {
      return this.c;
   }

   public String toString() {
      return "[EventHandler " + this.b + "]";
   }
}
