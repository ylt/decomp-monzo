package io.intercom.com.a.a;

import android.os.Looper;

public interface i {
   i a = new i() {
      public void a(b var1) {
      }
   };
   i b = new i() {
      public void a(b var1) {
         if(Looper.myLooper() != Looper.getMainLooper()) {
            throw new IllegalStateException("Event bus " + var1 + " accessed from non-main thread " + Looper.myLooper());
         }
      }
   };

   void a(b var1);
}
