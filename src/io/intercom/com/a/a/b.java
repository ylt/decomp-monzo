package io.intercom.com.a.a;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CopyOnWriteArraySet;

public class b {
   public static final String DEFAULT_IDENTIFIER = "default";
   private final i enforcer;
   private final ThreadLocal eventsToDispatch;
   private final ConcurrentMap flattenHierarchyCache;
   private final f handlerFinder;
   private final ConcurrentMap handlersByType;
   private final String identifier;
   private final ThreadLocal isDispatching;
   private final ConcurrentMap producersByType;

   public b() {
      this("default");
   }

   public b(i var1) {
      this(var1, "default");
   }

   public b(i var1, String var2) {
      this(var1, var2, f.a);
   }

   b(i var1, String var2, f var3) {
      this.handlersByType = new ConcurrentHashMap();
      this.producersByType = new ConcurrentHashMap();
      this.eventsToDispatch = new ThreadLocal() {
         protected ConcurrentLinkedQueue a() {
            return new ConcurrentLinkedQueue();
         }

         // $FF: synthetic method
         protected Object initialValue() {
            return this.a();
         }
      };
      this.isDispatching = new ThreadLocal() {
         protected Boolean a() {
            return Boolean.valueOf(false);
         }

         // $FF: synthetic method
         protected Object initialValue() {
            return this.a();
         }
      };
      this.flattenHierarchyCache = new ConcurrentHashMap();
      this.enforcer = var1;
      this.identifier = var2;
      this.handlerFinder = var3;
   }

   public b(String var1) {
      this(i.b, var1);
   }

   private void dispatchProducerResultToHandler(d var1, e var2) {
      Object var3 = null;

      Object var6;
      label17: {
         Object var4;
         try {
            var4 = var2.c();
         } catch (InvocationTargetException var5) {
            throwRuntimeException("Producer " + var2 + " threw an exception.", var5);
            var6 = var3;
            break label17;
         }

         var6 = var4;
      }

      if(var6 != null) {
         this.dispatch(var6, var1);
      }

   }

   private Set getClassesFor(Class var1) {
      LinkedList var2 = new LinkedList();
      HashSet var3 = new HashSet();
      var2.add(var1);

      while(!var2.isEmpty()) {
         var1 = (Class)var2.remove(0);
         var3.add(var1);
         var1 = var1.getSuperclass();
         if(var1 != null) {
            var2.add(var1);
         }
      }

      return var3;
   }

   private static void throwRuntimeException(String var0, InvocationTargetException var1) {
      Throwable var2 = var1.getCause();
      if(var2 != null) {
         throw new RuntimeException(var0 + ": " + var2.getMessage(), var2);
      } else {
         throw new RuntimeException(var0 + ": " + var1.getMessage(), var1);
      }
   }

   protected void dispatch(Object var1, d var2) {
      try {
         var2.a(var1);
      } catch (InvocationTargetException var4) {
         throwRuntimeException("Could not dispatch event: " + var1.getClass() + " to handler " + var2, var4);
      }

   }

   protected void dispatchQueuedEvents() {
      // $FF: Couldn't be decompiled
   }

   protected void enqueueEvent(Object var1, d var2) {
      ((ConcurrentLinkedQueue)this.eventsToDispatch.get()).offer(new b.a(var1, var2));
   }

   Set flattenHierarchy(Class var1) {
      Set var3 = (Set)this.flattenHierarchyCache.get(var1);
      Set var2 = var3;
      if(var3 == null) {
         var3 = this.getClassesFor(var1);
         Set var4 = (Set)this.flattenHierarchyCache.putIfAbsent(var1, var3);
         var2 = var4;
         if(var4 == null) {
            var2 = var3;
         }
      }

      return var2;
   }

   Set getHandlersForEventType(Class var1) {
      return (Set)this.handlersByType.get(var1);
   }

   e getProducerForEventType(Class var1) {
      return (e)this.producersByType.get(var1);
   }

   public void post(Object var1) {
      if(var1 == null) {
         throw new NullPointerException("Event to post must not be null.");
      } else {
         this.enforcer.a(this);
         Iterator var4 = this.flattenHierarchy(var1.getClass()).iterator();

         boolean var2;
         boolean var3;
         for(var3 = false; var4.hasNext(); var3 = var2) {
            Set var5 = this.getHandlersForEventType((Class)var4.next());
            var2 = var3;
            if(var5 != null) {
               var2 = var3;
               if(!var5.isEmpty()) {
                  var3 = true;
                  Iterator var6 = var5.iterator();

                  while(true) {
                     var2 = var3;
                     if(!var6.hasNext()) {
                        break;
                     }

                     this.enqueueEvent(var1, (d)var6.next());
                  }
               }
            }
         }

         if(!var3 && !(var1 instanceof c)) {
            this.post(new c(this, var1));
         }

         this.dispatchQueuedEvents();
      }
   }

   public void register(Object var1) {
      if(var1 == null) {
         throw new NullPointerException("Object to register must not be null.");
      } else {
         this.enforcer.a(this);
         Map var4 = this.handlerFinder.a(var1);
         Iterator var2 = var4.keySet().iterator();

         Iterator var18;
         while(var2.hasNext()) {
            Class var6 = (Class)var2.next();
            e var3 = (e)var4.get(var6);
            e var5 = (e)this.producersByType.putIfAbsent(var6, var3);
            if(var5 != null) {
               throw new IllegalArgumentException("Producer method for type " + var6 + " found on type " + var3.a.getClass() + ", but already registered by type " + var5.a.getClass() + ".");
            }

            Set var17 = (Set)this.handlersByType.get(var6);
            if(var17 != null && !var17.isEmpty()) {
               var18 = var17.iterator();

               while(var18.hasNext()) {
                  this.dispatchProducerResultToHandler((d)var18.next(), var3);
               }
            }
         }

         Map var19 = this.handlerFinder.b(var1);
         var18 = var19.keySet().iterator();

         while(var18.hasNext()) {
            Class var15 = (Class)var18.next();
            Set var7 = (Set)this.handlersByType.get(var15);
            var1 = var7;
            if(var7 == null) {
               CopyOnWriteArraySet var9 = new CopyOnWriteArraySet();
               Set var11 = (Set)this.handlersByType.putIfAbsent(var15, var9);
               var1 = var11;
               if(var11 == null) {
                  var1 = var9;
               }
            }

            if(!((Set)var1).addAll((Set)var19.get(var15))) {
               throw new IllegalArgumentException("Object already registered.");
            }
         }

         Iterator var8 = var19.entrySet().iterator();

         while(true) {
            e var12;
            Entry var13;
            do {
               do {
                  if(!var8.hasNext()) {
                     return;
                  }

                  var13 = (Entry)var8.next();
                  Class var10 = (Class)var13.getKey();
                  var12 = (e)this.producersByType.get(var10);
               } while(var12 == null);
            } while(!var12.a());

            Iterator var16 = ((Set)var13.getValue()).iterator();

            while(var16.hasNext()) {
               d var14 = (d)var16.next();
               if(!var12.a()) {
                  break;
               }

               if(var14.a()) {
                  this.dispatchProducerResultToHandler(var14, var12);
               }
            }
         }
      }
   }

   public String toString() {
      return "[Bus \"" + this.identifier + "\"]";
   }

   public void unregister(Object var1) {
      if(var1 == null) {
         throw new NullPointerException("Object to unregister must not be null.");
      } else {
         this.enforcer.a(this);
         Iterator var3 = this.handlerFinder.a(var1).entrySet().iterator();

         while(var3.hasNext()) {
            Entry var5 = (Entry)var3.next();
            Class var4 = (Class)var5.getKey();
            e var2 = this.getProducerForEventType(var4);
            e var10 = (e)var5.getValue();
            if(var10 == null || !var10.equals(var2)) {
               throw new IllegalArgumentException("Missing event producer for an annotated method. Is " + var1.getClass() + " registered?");
            }

            ((e)this.producersByType.remove(var4)).b();
         }

         var3 = this.handlerFinder.b(var1).entrySet().iterator();

         while(var3.hasNext()) {
            Entry var8 = (Entry)var3.next();
            Set var7 = this.getHandlersForEventType((Class)var8.getKey());
            Collection var11 = (Collection)var8.getValue();
            if(var7 == null || !var7.containsAll(var11)) {
               throw new IllegalArgumentException("Missing event handler for an annotated method. Is " + var1.getClass() + " registered?");
            }

            Iterator var6 = var7.iterator();

            while(var6.hasNext()) {
               d var9 = (d)var6.next();
               if(var11.contains(var9)) {
                  var9.b();
               }
            }

            var7.removeAll(var11);
         }

      }
   }

   static class a {
      final Object a;
      final d b;

      public a(Object var1, d var2) {
         this.a = var1;
         this.b = var2;
      }
   }
}
