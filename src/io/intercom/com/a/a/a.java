package io.intercom.com.a.a;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

final class a {
   private static final ConcurrentMap a = new ConcurrentHashMap();
   private static final ConcurrentMap b = new ConcurrentHashMap();

   static Map a(Object var0) {
      Class var4 = var0.getClass();
      HashMap var3 = new HashMap();
      Map var2 = (Map)a.get(var4);
      Object var1 = var2;
      if(var2 == null) {
         var1 = new HashMap();
         a(var4, (Map)var1);
      }

      if(!((Map)var1).isEmpty()) {
         Iterator var5 = ((Map)var1).entrySet().iterator();

         while(var5.hasNext()) {
            Entry var7 = (Entry)var5.next();
            e var6 = new e(var0, (Method)var7.getValue());
            var3.put(var7.getKey(), var6);
         }
      }

      return var3;
   }

   private static void a(Class var0, Map var1) {
      a(var0, var1, new HashMap());
   }

   private static void a(Class var0, Map var1, Map var2) {
      Method[] var7 = var0.getDeclaredMethods();
      int var4 = var7.length;

      for(int var3 = 0; var3 < var4; ++var3) {
         Method var8 = var7[var3];
         if(!var8.isBridge()) {
            Class[] var5;
            if(var8.isAnnotationPresent(h.class)) {
               var5 = var8.getParameterTypes();
               if(var5.length != 1) {
                  throw new IllegalArgumentException("Method " + var8 + " has @Subscribe annotation but requires " + var5.length + " arguments.  Methods must require a single argument.");
               }

               Class var9 = var5[0];
               if(var9.isInterface()) {
                  throw new IllegalArgumentException("Method " + var8 + " has @Subscribe annotation on " + var9 + " which is an interface.  Subscription must be on a concrete class type.");
               }

               if((var8.getModifiers() & 1) == 0) {
                  throw new IllegalArgumentException("Method " + var8 + " has @Subscribe annotation on " + var9 + " but is not 'public'.");
               }

               Set var6 = (Set)var2.get(var9);
               Object var10 = var6;
               if(var6 == null) {
                  var10 = new HashSet();
                  var2.put(var9, var10);
               }

               ((Set)var10).add(var8);
            } else if(var8.isAnnotationPresent(g.class)) {
               var5 = var8.getParameterTypes();
               if(var5.length != 0) {
                  throw new IllegalArgumentException("Method " + var8 + "has @Produce annotation but requires " + var5.length + " arguments.  Methods must require zero arguments.");
               }

               if(var8.getReturnType() == Void.class) {
                  throw new IllegalArgumentException("Method " + var8 + " has a return type of void.  Must declare a non-void type.");
               }

               Class var11 = var8.getReturnType();
               if(var11.isInterface()) {
                  throw new IllegalArgumentException("Method " + var8 + " has @Produce annotation on " + var11 + " which is an interface.  Producers must return a concrete class type.");
               }

               if(var11.equals(Void.TYPE)) {
                  throw new IllegalArgumentException("Method " + var8 + " has @Produce annotation but has no return type.");
               }

               if((var8.getModifiers() & 1) == 0) {
                  throw new IllegalArgumentException("Method " + var8 + " has @Produce annotation on " + var11 + " but is not 'public'.");
               }

               if(var1.containsKey(var11)) {
                  throw new IllegalArgumentException("Producer for type " + var11 + " has already been registered.");
               }

               var1.put(var11, var8);
            }
         }
      }

      a.put(var0, var1);
      b.put(var0, var2);
   }

   static Map b(Object var0) {
      Class var4 = var0.getClass();
      HashMap var3 = new HashMap();
      Map var2 = (Map)b.get(var4);
      Object var1 = var2;
      if(var2 == null) {
         var1 = new HashMap();
         b(var4, (Map)var1);
      }

      if(!((Map)var1).isEmpty()) {
         Iterator var8 = ((Map)var1).entrySet().iterator();

         while(var8.hasNext()) {
            Entry var5 = (Entry)var8.next();
            HashSet var7 = new HashSet();
            Iterator var6 = ((Set)var5.getValue()).iterator();

            while(var6.hasNext()) {
               var7.add(new d(var0, (Method)var6.next()));
            }

            var3.put(var5.getKey(), var7);
         }
      }

      return var3;
   }

   private static void b(Class var0, Map var1) {
      a(var0, new HashMap(), var1);
   }
}
