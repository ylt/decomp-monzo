package io.intercom.com.bumptech.glide;

import android.annotation.TargetApi;
import android.content.ComponentCallbacks2;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.support.v4.app.Fragment;
import android.util.Log;
import io.intercom.com.bumptech.glide.c.l;
import io.intercom.com.bumptech.glide.load.k;
import io.intercom.com.bumptech.glide.load.b.n;
import io.intercom.com.bumptech.glide.load.b.s;
import io.intercom.com.bumptech.glide.load.b.t;
import io.intercom.com.bumptech.glide.load.b.u;
import io.intercom.com.bumptech.glide.load.b.v;
import io.intercom.com.bumptech.glide.load.b.w;
import io.intercom.com.bumptech.glide.load.resource.bitmap.p;
import io.intercom.com.bumptech.glide.load.resource.bitmap.r;
import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

@TargetApi(14)
public class c implements ComponentCallbacks2 {
   private static volatile c a;
   private static volatile boolean b;
   private final io.intercom.com.bumptech.glide.load.engine.i c;
   private final io.intercom.com.bumptech.glide.load.engine.a.e d;
   private final io.intercom.com.bumptech.glide.load.engine.b.h e;
   private final io.intercom.com.bumptech.glide.load.engine.d.a f;
   private final e g;
   private final Registry h;
   private final io.intercom.com.bumptech.glide.load.engine.a.b i;
   private final l j;
   private final io.intercom.com.bumptech.glide.c.d k;
   private final List l = new ArrayList();
   private f m;

   @TargetApi(14)
   c(Context var1, io.intercom.com.bumptech.glide.load.engine.i var2, io.intercom.com.bumptech.glide.load.engine.b.h var3, io.intercom.com.bumptech.glide.load.engine.a.e var4, io.intercom.com.bumptech.glide.load.engine.a.b var5, l var6, io.intercom.com.bumptech.glide.c.d var7, int var8, io.intercom.com.bumptech.glide.f.f var9, Map var10) {
      this.m = f.b;
      this.c = var2;
      this.d = var4;
      this.i = var5;
      this.e = var3;
      this.j = var6;
      this.k = var7;
      this.f = new io.intercom.com.bumptech.glide.load.engine.d.a(var3, var4, (io.intercom.com.bumptech.glide.load.b)var9.m().a(io.intercom.com.bumptech.glide.load.resource.bitmap.l.a));
      Resources var11 = var1.getResources();
      this.h = new Registry();
      this.h.a((io.intercom.com.bumptech.glide.load.e)(new io.intercom.com.bumptech.glide.load.resource.bitmap.j()));
      io.intercom.com.bumptech.glide.load.resource.bitmap.l var14 = new io.intercom.com.bumptech.glide.load.resource.bitmap.l(this.h.a(), var11.getDisplayMetrics(), var4, var5);
      io.intercom.com.bumptech.glide.load.resource.d.a var13 = new io.intercom.com.bumptech.glide.load.resource.d.a(var1, this.h.a(), var4, var5);
      this.h.a(ByteBuffer.class, (io.intercom.com.bumptech.glide.load.d)(new io.intercom.com.bumptech.glide.load.b.c())).a(InputStream.class, (io.intercom.com.bumptech.glide.load.d)(new s(var5))).a(ByteBuffer.class, Bitmap.class, (io.intercom.com.bumptech.glide.load.j)(new io.intercom.com.bumptech.glide.load.resource.bitmap.g(var14))).a(InputStream.class, Bitmap.class, (io.intercom.com.bumptech.glide.load.j)(new p(var14, var5))).a(ParcelFileDescriptor.class, Bitmap.class, (io.intercom.com.bumptech.glide.load.j)(new r(var4))).a(Bitmap.class, (k)(new io.intercom.com.bumptech.glide.load.resource.bitmap.d())).a(ByteBuffer.class, BitmapDrawable.class, (io.intercom.com.bumptech.glide.load.j)(new io.intercom.com.bumptech.glide.load.resource.bitmap.a(var11, var4, new io.intercom.com.bumptech.glide.load.resource.bitmap.g(var14)))).a(InputStream.class, BitmapDrawable.class, (io.intercom.com.bumptech.glide.load.j)(new io.intercom.com.bumptech.glide.load.resource.bitmap.a(var11, var4, new p(var14, var5)))).a(ParcelFileDescriptor.class, BitmapDrawable.class, (io.intercom.com.bumptech.glide.load.j)(new io.intercom.com.bumptech.glide.load.resource.bitmap.a(var11, var4, new r(var4)))).a(BitmapDrawable.class, (k)(new io.intercom.com.bumptech.glide.load.resource.bitmap.b(var4, new io.intercom.com.bumptech.glide.load.resource.bitmap.d()))).b(InputStream.class, io.intercom.com.bumptech.glide.load.resource.d.c.class, (io.intercom.com.bumptech.glide.load.j)(new io.intercom.com.bumptech.glide.load.resource.d.i(this.h.a(), var13, var5))).b(ByteBuffer.class, io.intercom.com.bumptech.glide.load.resource.d.c.class, (io.intercom.com.bumptech.glide.load.j)var13).a(io.intercom.com.bumptech.glide.load.resource.d.c.class, (k)(new io.intercom.com.bumptech.glide.load.resource.d.d())).a(io.intercom.com.bumptech.glide.b.a.class, io.intercom.com.bumptech.glide.b.a.class, (n)(new u.a())).a(io.intercom.com.bumptech.glide.b.a.class, Bitmap.class, (io.intercom.com.bumptech.glide.load.j)(new io.intercom.com.bumptech.glide.load.resource.d.h(var4))).a((io.intercom.com.bumptech.glide.load.a.c.a)(new io.intercom.com.bumptech.glide.load.resource.a.a.a())).a(File.class, ByteBuffer.class, (n)(new io.intercom.com.bumptech.glide.load.b.d.b())).a(File.class, InputStream.class, (n)(new io.intercom.com.bumptech.glide.load.b.f.e())).a(File.class, File.class, (io.intercom.com.bumptech.glide.load.j)(new io.intercom.com.bumptech.glide.load.resource.c.a())).a(File.class, ParcelFileDescriptor.class, (n)(new io.intercom.com.bumptech.glide.load.b.f.b())).a(File.class, File.class, (n)(new u.a())).a((io.intercom.com.bumptech.glide.load.a.c.a)(new io.intercom.com.bumptech.glide.load.a.i.a(var5))).a(Integer.TYPE, InputStream.class, (n)(new io.intercom.com.bumptech.glide.load.b.r.b(var11))).a(Integer.TYPE, ParcelFileDescriptor.class, (n)(new io.intercom.com.bumptech.glide.load.b.r.a(var11))).a(Integer.class, InputStream.class, (n)(new io.intercom.com.bumptech.glide.load.b.r.b(var11))).a(Integer.class, ParcelFileDescriptor.class, (n)(new io.intercom.com.bumptech.glide.load.b.r.a(var11))).a(String.class, InputStream.class, (n)(new io.intercom.com.bumptech.glide.load.b.e.c())).a(String.class, InputStream.class, (n)(new t.b())).a(String.class, ParcelFileDescriptor.class, (n)(new t.a())).a(Uri.class, InputStream.class, (n)(new io.intercom.com.bumptech.glide.load.b.a.b.a())).a(Uri.class, InputStream.class, (n)(new io.intercom.com.bumptech.glide.load.b.a.c(var1.getAssets()))).a(Uri.class, ParcelFileDescriptor.class, (n)(new io.intercom.com.bumptech.glide.load.b.a.b(var1.getAssets()))).a(Uri.class, InputStream.class, (n)(new io.intercom.com.bumptech.glide.load.b.a.c.a(var1))).a(Uri.class, InputStream.class, (n)(new io.intercom.com.bumptech.glide.load.b.a.d.a(var1))).a(Uri.class, InputStream.class, (n)(new v.c(var1.getContentResolver()))).a(Uri.class, ParcelFileDescriptor.class, (n)(new v.a(var1.getContentResolver()))).a(Uri.class, InputStream.class, (n)(new w.a())).a(URL.class, InputStream.class, (n)(new io.intercom.com.bumptech.glide.load.b.a.e.a())).a(Uri.class, File.class, (n)(new io.intercom.com.bumptech.glide.load.b.k.a(var1))).a(io.intercom.com.bumptech.glide.load.b.g.class, InputStream.class, (n)(new io.intercom.com.bumptech.glide.load.b.a.a.a())).a(byte[].class, ByteBuffer.class, (n)(new io.intercom.com.bumptech.glide.load.b.b.a())).a(byte[].class, InputStream.class, (n)(new io.intercom.com.bumptech.glide.load.b.b.d())).a(Bitmap.class, BitmapDrawable.class, (io.intercom.com.bumptech.glide.load.resource.e.d)(new io.intercom.com.bumptech.glide.load.resource.e.b(var11, var4))).a(Bitmap.class, byte[].class, (io.intercom.com.bumptech.glide.load.resource.e.d)(new io.intercom.com.bumptech.glide.load.resource.e.a())).a(io.intercom.com.bumptech.glide.load.resource.d.c.class, byte[].class, (io.intercom.com.bumptech.glide.load.resource.e.d)(new io.intercom.com.bumptech.glide.load.resource.e.c()));
      io.intercom.com.bumptech.glide.f.a.e var12 = new io.intercom.com.bumptech.glide.f.a.e();
      this.g = new e(var1, this.h, var12, var9, var10, var2, var8);
   }

   public static c a(Context param0) {
      // $FF: Couldn't be decompiled
   }

   public static i a(Fragment var0) {
      return e(var0.getActivity()).a(var0);
   }

   public static i a(android.support.v4.app.j var0) {
      return e(var0).a(var0);
   }

   public static i b(Context var0) {
      return e(var0).a(var0);
   }

   private static void c(Context var0) {
      if(b) {
         throw new IllegalStateException("You cannot call Glide.get() in registerComponents(), use the provided Glide instance instead");
      } else {
         b = true;
         d(var0);
         b = false;
      }
   }

   private static void d(Context var0) {
      Context var4 = var0.getApplicationContext();
      a var3 = i();
      List var1 = Collections.emptyList();
      if(var3 == null || var3.c()) {
         var1 = (new io.intercom.com.bumptech.glide.d.e(var4)).a();
      }

      Iterator var5;
      if(var3 != null && !var3.a().isEmpty()) {
         Set var2 = var3.a();
         var5 = var1.iterator();

         while(var5.hasNext()) {
            io.intercom.com.bumptech.glide.d.c var6 = (io.intercom.com.bumptech.glide.d.c)var5.next();
            if(var2.contains(var6.getClass())) {
               if(Log.isLoggable("Glide", 3)) {
                  Log.d("Glide", "AppGlideModule excludes manifest GlideModule: " + var6);
               }

               var5.remove();
            }
         }
      }

      if(Log.isLoggable("Glide", 3)) {
         var5 = var1.iterator();

         while(var5.hasNext()) {
            io.intercom.com.bumptech.glide.d.c var7 = (io.intercom.com.bumptech.glide.d.c)var5.next();
            Log.d("Glide", "Discovered GlideModule from manifest: " + var7.getClass());
         }
      }

      l.a var8;
      if(var3 != null) {
         var8 = var3.b();
      } else {
         var8 = null;
      }

      d var10 = (new d()).a(var8);
      var5 = var1.iterator();

      while(var5.hasNext()) {
         ((io.intercom.com.bumptech.glide.d.c)var5.next()).a(var4, var10);
      }

      if(var3 != null) {
         var3.a(var4, var10);
      }

      c var11 = var10.a(var4);
      Iterator var9 = var1.iterator();

      while(var9.hasNext()) {
         ((io.intercom.com.bumptech.glide.d.c)var9.next()).a(var4, var11, var11.h);
      }

      if(var3 != null) {
         var3.a(var4, var11, var11.h);
      }

      var0.getApplicationContext().registerComponentCallbacks(var11);
      a = var11;
   }

   private static l e(Context var0) {
      io.intercom.com.bumptech.glide.h.h.a(var0, "You cannot start a load on a not yet attached View or a  Fragment where getActivity() returns null (which usually occurs when getActivity() is called before the Fragment is attached or after the Fragment is destroyed).");
      return a(var0).g();
   }

   private static a i() {
      a var0;
      try {
         var0 = (a)Class.forName("io.intercom.com.bumptech.glide.GeneratedAppGlideModuleImpl").newInstance();
      } catch (ClassNotFoundException var1) {
         if(Log.isLoggable("Glide", 5)) {
            Log.w("Glide", "Failed to find GeneratedAppGlideModule. You should include an annotationProcessor compile dependency on com.github.bumptech.glide:glide:compiler in your application and a @GlideModule annotated AppGlideModule implementation or LibraryGlideModules will be silently ignored");
         }

         var0 = null;
      } catch (InstantiationException var2) {
         throw new IllegalStateException("GeneratedAppGlideModuleImpl is implemented incorrectly. If you've manually implemented this class, remove your implementation. The Annotation processor will generate a correct implementation.", var2);
      } catch (IllegalAccessException var3) {
         throw new IllegalStateException("GeneratedAppGlideModuleImpl is implemented incorrectly. If you've manually implemented this class, remove your implementation. The Annotation processor will generate a correct implementation.", var3);
      }

      return var0;
   }

   public io.intercom.com.bumptech.glide.load.engine.a.e a() {
      return this.d;
   }

   public void a(int var1) {
      io.intercom.com.bumptech.glide.h.i.a();
      this.e.a(var1);
      this.d.a(var1);
      this.i.a(var1);
   }

   void a(io.intercom.com.bumptech.glide.f.a.h param1) {
      // $FF: Couldn't be decompiled
   }

   void a(i param1) {
      // $FF: Couldn't be decompiled
   }

   public io.intercom.com.bumptech.glide.load.engine.a.b b() {
      return this.i;
   }

   void b(i param1) {
      // $FF: Couldn't be decompiled
   }

   public Context c() {
      return this.g.getBaseContext();
   }

   io.intercom.com.bumptech.glide.c.d d() {
      return this.k;
   }

   e e() {
      return this.g;
   }

   public void f() {
      io.intercom.com.bumptech.glide.h.i.a();
      this.e.a();
      this.d.a();
      this.i.a();
   }

   public l g() {
      return this.j;
   }

   public Registry h() {
      return this.h;
   }

   public void onConfigurationChanged(Configuration var1) {
   }

   public void onLowMemory() {
      this.f();
   }

   public void onTrimMemory(int var1) {
      this.a(var1);
   }
}
