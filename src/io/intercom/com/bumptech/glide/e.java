package io.intercom.com.bumptech.glide;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Handler;
import android.os.Looper;
import android.widget.ImageView;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

@TargetApi(14)
public class e extends ContextWrapper {
   static final j a = new b();
   private final Handler b;
   private final Registry c;
   private final io.intercom.com.bumptech.glide.f.a.e d;
   private final io.intercom.com.bumptech.glide.f.f e;
   private final Map f;
   private final io.intercom.com.bumptech.glide.load.engine.i g;
   private final int h;

   public e(Context var1, Registry var2, io.intercom.com.bumptech.glide.f.a.e var3, io.intercom.com.bumptech.glide.f.f var4, Map var5, io.intercom.com.bumptech.glide.load.engine.i var6, int var7) {
      super(var1.getApplicationContext());
      this.c = var2;
      this.d = var3;
      this.e = var4;
      this.f = var5;
      this.g = var6;
      this.h = var7;
      this.b = new Handler(Looper.getMainLooper());
   }

   public io.intercom.com.bumptech.glide.f.a.h a(ImageView var1, Class var2) {
      return this.d.a(var1, var2);
   }

   public io.intercom.com.bumptech.glide.f.f a() {
      return this.e;
   }

   public j a(Class var1) {
      j var3 = (j)this.f.get(var1);
      j var2 = var3;
      if(var3 == null) {
         Iterator var4 = this.f.entrySet().iterator();
         var2 = var3;

         while(var4.hasNext()) {
            Entry var6 = (Entry)var4.next();
            if(((Class)var6.getKey()).isAssignableFrom(var1)) {
               var2 = (j)var6.getValue();
            }
         }
      }

      j var5 = var2;
      if(var2 == null) {
         var5 = a;
      }

      return var5;
   }

   public Handler b() {
      return this.b;
   }

   public io.intercom.com.bumptech.glide.load.engine.i c() {
      return this.g;
   }

   public Registry d() {
      return this.c;
   }

   public int e() {
      return this.h;
   }
}
