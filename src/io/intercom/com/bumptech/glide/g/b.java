package io.intercom.com.bumptech.glide.g;

import io.intercom.com.bumptech.glide.h.h;
import io.intercom.com.bumptech.glide.load.g;
import java.security.MessageDigest;

public final class b implements g {
   private final Object b;

   public b(Object var1) {
      this.b = h.a(var1);
   }

   public boolean equals(Object var1) {
      boolean var2;
      if(var1 instanceof b) {
         b var3 = (b)var1;
         var2 = this.b.equals(var3.b);
      } else {
         var2 = false;
      }

      return var2;
   }

   public int hashCode() {
      return this.b.hashCode();
   }

   public String toString() {
      return "ObjectKey{object=" + this.b + '}';
   }

   public void updateDiskCacheKey(MessageDigest var1) {
      var1.update(this.b.toString().getBytes(a));
   }
}
