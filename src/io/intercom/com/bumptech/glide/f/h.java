package io.intercom.com.bumptech.glide.f;

import android.graphics.drawable.Drawable;
import android.util.Log;
import io.intercom.com.bumptech.glide.load.engine.GlideException;
import io.intercom.com.bumptech.glide.load.engine.r;

public final class h implements io.intercom.com.bumptech.glide.f.a.g, b, g, io.intercom.com.bumptech.glide.h.a.a.c {
   private static final android.support.v4.g.k.a a = io.intercom.com.bumptech.glide.h.a.a.a(150, new io.intercom.com.bumptech.glide.h.a.a.a() {
      public h a() {
         return new h();
      }

      // $FF: synthetic method
      public Object b() {
         return this.a();
      }
   });
   private static boolean y = true;
   private final String b = String.valueOf(super.hashCode());
   private final io.intercom.com.bumptech.glide.h.a.b c = io.intercom.com.bumptech.glide.h.a.b.a();
   private c d;
   private io.intercom.com.bumptech.glide.e e;
   private Object f;
   private Class g;
   private f h;
   private int i;
   private int j;
   private io.intercom.com.bumptech.glide.g k;
   private io.intercom.com.bumptech.glide.f.a.h l;
   private e m;
   private io.intercom.com.bumptech.glide.load.engine.i n;
   private io.intercom.com.bumptech.glide.f.b.e o;
   private r p;
   private io.intercom.com.bumptech.glide.load.engine.i.d q;
   private long r;
   private h.a s;
   private Drawable t;
   private Drawable u;
   private Drawable v;
   private int w;
   private int x;

   private static int a(int var0, float var1) {
      if(var0 != Integer.MIN_VALUE) {
         var0 = Math.round((float)var0 * var1);
      }

      return var0;
   }

   private Drawable a(int var1) {
      Drawable var2;
      if(y) {
         var2 = this.b(var1);
      } else {
         var2 = this.c(var1);
      }

      return var2;
   }

   public static h a(io.intercom.com.bumptech.glide.e var0, Object var1, Class var2, f var3, int var4, int var5, io.intercom.com.bumptech.glide.g var6, io.intercom.com.bumptech.glide.f.a.h var7, e var8, c var9, io.intercom.com.bumptech.glide.load.engine.i var10, io.intercom.com.bumptech.glide.f.b.e var11) {
      h var13 = (h)a.a();
      h var12 = var13;
      if(var13 == null) {
         var12 = new h();
      }

      var12.b(var0, var1, var2, var3, var4, var5, var6, var7, var8, var9, var10, var11);
      return var12;
   }

   private void a(GlideException var1, int var2) {
      this.c.b();
      int var3 = this.e.e();
      if(var3 <= var2) {
         Log.w("Glide", "Load failed for " + this.f + " with size [" + this.w + "x" + this.x + "]", var1);
         if(var3 <= 4) {
            var1.a("Glide");
         }
      }

      this.q = null;
      this.s = h.a.e;
      if(this.m == null || !this.m.onLoadFailed(var1, this.f, this.l, this.q())) {
         this.n();
      }

   }

   private void a(r var1) {
      this.n.a(var1);
      this.p = null;
   }

   private void a(r var1, Object var2, io.intercom.com.bumptech.glide.load.a var3) {
      boolean var4 = this.q();
      this.s = h.a.d;
      this.p = var1;
      if(this.e.e() <= 3) {
         Log.d("Glide", "Finished loading " + var2.getClass().getSimpleName() + " from " + var3 + " for " + this.f + " with size [" + this.w + "x" + this.x + "] in " + io.intercom.com.bumptech.glide.h.d.a(this.r) + " ms");
      }

      if(this.m == null || !this.m.onResourceReady(var2, this.f, this.l, var3, var4)) {
         io.intercom.com.bumptech.glide.f.b.d var5 = this.o.a(var3, var4);
         this.l.onResourceReady(var2, var5);
      }

      this.r();
   }

   private void a(String var1) {
      Log.v("Request", var1 + " this: " + this.b);
   }

   private Drawable b(int var1) {
      Drawable var2;
      try {
         var2 = android.support.v7.c.a.b.b(this.e, var1);
      } catch (NoClassDefFoundError var3) {
         y = false;
         var2 = this.c(var1);
      }

      return var2;
   }

   private void b(io.intercom.com.bumptech.glide.e var1, Object var2, Class var3, f var4, int var5, int var6, io.intercom.com.bumptech.glide.g var7, io.intercom.com.bumptech.glide.f.a.h var8, e var9, c var10, io.intercom.com.bumptech.glide.load.engine.i var11, io.intercom.com.bumptech.glide.f.b.e var12) {
      this.e = var1;
      this.f = var2;
      this.g = var3;
      this.h = var4;
      this.i = var5;
      this.j = var6;
      this.k = var7;
      this.l = var8;
      this.m = var9;
      this.d = var10;
      this.n = var11;
      this.o = var12;
      this.s = h.a.a;
   }

   private Drawable c(int var1) {
      return android.support.v4.content.a.b.a(this.e.getResources(), var1, this.h.v());
   }

   private Drawable k() {
      if(this.t == null) {
         this.t = this.h.p();
         if(this.t == null && this.h.q() > 0) {
            this.t = this.a(this.h.q());
         }
      }

      return this.t;
   }

   private Drawable l() {
      if(this.u == null) {
         this.u = this.h.s();
         if(this.u == null && this.h.r() > 0) {
            this.u = this.a(this.h.r());
         }
      }

      return this.u;
   }

   private Drawable m() {
      if(this.v == null) {
         this.v = this.h.u();
         if(this.v == null && this.h.t() > 0) {
            this.v = this.a(this.h.t());
         }
      }

      return this.v;
   }

   private void n() {
      if(this.p()) {
         Drawable var2 = null;
         if(this.f == null) {
            var2 = this.m();
         }

         Drawable var1 = var2;
         if(var2 == null) {
            var1 = this.k();
         }

         var2 = var1;
         if(var1 == null) {
            var2 = this.l();
         }

         this.l.onLoadFailed(var2);
      }

   }

   private boolean o() {
      boolean var1;
      if(this.d != null && !this.d.b(this)) {
         var1 = false;
      } else {
         var1 = true;
      }

      return var1;
   }

   private boolean p() {
      boolean var1;
      if(this.d != null && !this.d.c(this)) {
         var1 = false;
      } else {
         var1 = true;
      }

      return var1;
   }

   private boolean q() {
      boolean var1;
      if(this.d != null && this.d.d()) {
         var1 = false;
      } else {
         var1 = true;
      }

      return var1;
   }

   private void r() {
      if(this.d != null) {
         this.d.d(this);
      }

   }

   public void a() {
      this.c.b();
      this.r = io.intercom.com.bumptech.glide.h.d.a();
      if(this.f == null) {
         if(io.intercom.com.bumptech.glide.h.i.a(this.i, this.j)) {
            this.w = this.i;
            this.x = this.j;
         }

         byte var1;
         if(this.m() == null) {
            var1 = 5;
         } else {
            var1 = 3;
         }

         this.a(new GlideException("Received null model"), var1);
      } else {
         if(this.s == h.a.b) {
            throw new IllegalArgumentException("Cannot restart a running request");
         }

         if(this.s == h.a.d) {
            this.a(this.p, io.intercom.com.bumptech.glide.load.a.e);
         } else {
            this.s = h.a.c;
            if(io.intercom.com.bumptech.glide.h.i.a(this.i, this.j)) {
               this.a(this.i, this.j);
            } else {
               this.l.getSize(this);
            }

            if((this.s == h.a.b || this.s == h.a.c) && this.p()) {
               this.l.onLoadStarted(this.l());
            }

            if(Log.isLoggable("Request", 2)) {
               this.a("finished run method in " + io.intercom.com.bumptech.glide.h.d.a(this.r));
            }
         }
      }

   }

   public void a(int var1, int var2) {
      this.c.b();
      if(Log.isLoggable("Request", 2)) {
         this.a("Got onSizeReady in " + io.intercom.com.bumptech.glide.h.d.a(this.r));
      }

      if(this.s == h.a.c) {
         this.s = h.a.b;
         float var3 = this.h.D();
         this.w = a(var1, var3);
         this.x = a(var2, var3);
         if(Log.isLoggable("Request", 2)) {
            this.a("finished setup for calling load in " + io.intercom.com.bumptech.glide.h.d.a(this.r));
         }

         this.q = this.n.a(this.e, this.f, this.h.x(), this.w, this.x, this.h.n(), this.g, this.k, this.h.o(), this.h.k(), this.h.l(), this.h.E(), this.h.m(), this.h.w(), this.h.F(), this.h.G(), this);
         if(Log.isLoggable("Request", 2)) {
            this.a("finished onSizeReady in " + io.intercom.com.bumptech.glide.h.d.a(this.r));
         }
      }

   }

   public void a(GlideException var1) {
      this.a(var1, 5);
   }

   public void a(r var1, io.intercom.com.bumptech.glide.load.a var2) {
      this.c.b();
      this.q = null;
      if(var1 == null) {
         this.a(new GlideException("Expected to receive a Resource<R> with an object of " + this.g + " inside, but instead got null."));
      } else {
         Object var3 = var1.c();
         if(var3 != null && this.g.isAssignableFrom(var3.getClass())) {
            if(!this.o()) {
               this.a(var1);
               this.s = h.a.d;
            } else {
               this.a(var1, var3, var2);
            }
         } else {
            this.a(var1);
            StringBuilder var4 = (new StringBuilder()).append("Expected to receive an object of ").append(this.g).append(" but instead got ");
            Object var6;
            if(var3 != null) {
               var6 = var3.getClass();
            } else {
               var6 = "";
            }

            StringBuilder var7 = var4.append(var6).append("{").append(var3).append("} inside Resource{").append(var1).append("}.");
            String var5;
            if(var3 != null) {
               var5 = "";
            } else {
               var5 = " To indicate failure return a null Resource object, rather than a Resource object containing null data.";
            }

            this.a(new GlideException(var7.append(var5).toString()));
         }
      }

   }

   public boolean a(b var1) {
      boolean var3 = false;
      boolean var2 = var3;
      if(var1 instanceof h) {
         h var4 = (h)var1;
         var2 = var3;
         if(this.i == var4.i) {
            var2 = var3;
            if(this.j == var4.j) {
               var2 = var3;
               if(io.intercom.com.bumptech.glide.h.i.a(this.f, var4.f)) {
                  var2 = var3;
                  if(this.g.equals(var4.g)) {
                     var2 = var3;
                     if(this.h.equals(var4.h)) {
                        var2 = var3;
                        if(this.k == var4.k) {
                           var2 = true;
                        }
                     }
                  }
               }
            }
         }
      }

      return var2;
   }

   public void b() {
      this.c();
      this.s = h.a.h;
   }

   public void c() {
      io.intercom.com.bumptech.glide.h.i.a();
      if(this.s != h.a.g) {
         this.j();
         if(this.p != null) {
            this.a(this.p);
         }

         if(this.p()) {
            this.l.onLoadCleared(this.l());
         }

         this.s = h.a.g;
      }

   }

   public boolean e() {
      boolean var1;
      if(this.s != h.a.b && this.s != h.a.c) {
         var1 = false;
      } else {
         var1 = true;
      }

      return var1;
   }

   public boolean f() {
      boolean var1;
      if(this.s == h.a.d) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public boolean g() {
      return this.f();
   }

   public boolean h() {
      boolean var1;
      if(this.s != h.a.f && this.s != h.a.g) {
         var1 = false;
      } else {
         var1 = true;
      }

      return var1;
   }

   public void i() {
      this.e = null;
      this.f = null;
      this.g = null;
      this.h = null;
      this.i = -1;
      this.j = -1;
      this.l = null;
      this.m = null;
      this.d = null;
      this.o = null;
      this.q = null;
      this.t = null;
      this.u = null;
      this.v = null;
      this.w = -1;
      this.x = -1;
      a.a(this);
   }

   void j() {
      this.c.b();
      this.l.removeCallback(this);
      this.s = h.a.f;
      if(this.q != null) {
         this.q.a();
         this.q = null;
      }

   }

   public io.intercom.com.bumptech.glide.h.a.b m_() {
      return this.c;
   }

   private static enum a {
      a,
      b,
      c,
      d,
      e,
      f,
      g,
      h;
   }
}
