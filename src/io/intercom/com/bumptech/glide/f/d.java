package io.intercom.com.bumptech.glide.f;

import android.graphics.drawable.Drawable;
import android.os.Handler;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class d implements a, Runnable {
   private static final d.a a = new d.a();
   private final Handler b;
   private final int c;
   private final int d;
   private final boolean e;
   private final d.a f;
   private Object g;
   private b h;
   private boolean i;
   private boolean j;
   private boolean k;

   public d(Handler var1, int var2, int var3) {
      this(var1, var2, var3, true, a);
   }

   d(Handler var1, int var2, int var3, boolean var4, d.a var5) {
      this.b = var1;
      this.c = var2;
      this.d = var3;
      this.e = var4;
      this.f = var5;
   }

   private Object a(Long param1) throws ExecutionException, InterruptedException, TimeoutException {
      // $FF: Couldn't be decompiled
   }

   private void a() {
      this.b.post(this);
   }

   public boolean cancel(boolean param1) {
      // $FF: Couldn't be decompiled
   }

   public Object get() throws InterruptedException, ExecutionException {
      try {
         Object var1 = this.a((Long)null);
         return var1;
      } catch (TimeoutException var2) {
         throw new AssertionError(var2);
      }
   }

   public Object get(long var1, TimeUnit var3) throws InterruptedException, ExecutionException, TimeoutException {
      return this.a(Long.valueOf(var3.toMillis(var1)));
   }

   public b getRequest() {
      return this.h;
   }

   public void getSize(io.intercom.com.bumptech.glide.f.a.g var1) {
      var1.a(this.c, this.d);
   }

   public boolean isCancelled() {
      synchronized(this){}

      boolean var1;
      try {
         var1 = this.i;
      } finally {
         ;
      }

      return var1;
   }

   public boolean isDone() {
      synchronized(this){}
      boolean var4 = false;

      boolean var1;
      label45: {
         try {
            var4 = true;
            if(this.i) {
               var4 = false;
               break label45;
            }

            var1 = this.j;
            var4 = false;
         } finally {
            if(var4) {
               ;
            }
         }

         if(!var1) {
            var1 = false;
            return var1;
         }
      }

      var1 = true;
      return var1;
   }

   public void onDestroy() {
   }

   public void onLoadCleared(Drawable var1) {
   }

   public void onLoadFailed(Drawable var1) {
      synchronized(this){}

      try {
         this.k = true;
         this.f.a(this);
      } finally {
         ;
      }

   }

   public void onLoadStarted(Drawable var1) {
   }

   public void onResourceReady(Object var1, io.intercom.com.bumptech.glide.f.b.d var2) {
      synchronized(this){}

      try {
         this.j = true;
         this.g = var1;
         this.f.a(this);
      } finally {
         ;
      }

   }

   public void onStart() {
   }

   public void onStop() {
   }

   public void removeCallback(io.intercom.com.bumptech.glide.f.a.g var1) {
   }

   public void run() {
      if(this.h != null) {
         this.h.c();
         this.h = null;
      }

   }

   public void setRequest(b var1) {
      this.h = var1;
   }

   static class a {
      public void a(Object var1) {
         var1.notifyAll();
      }

      public void a(Object var1, long var2) throws InterruptedException {
         var1.wait(var2);
      }
   }
}
