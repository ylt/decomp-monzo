package io.intercom.com.bumptech.glide.f;

import io.intercom.com.bumptech.glide.load.engine.GlideException;

public interface e {
   boolean onLoadFailed(GlideException var1, Object var2, io.intercom.com.bumptech.glide.f.a.h var3, boolean var4);

   boolean onResourceReady(Object var1, Object var2, io.intercom.com.bumptech.glide.f.a.h var3, io.intercom.com.bumptech.glide.load.a var4, boolean var5);
}
