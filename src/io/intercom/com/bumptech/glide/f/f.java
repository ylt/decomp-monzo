package io.intercom.com.bumptech.glide.f;

import android.content.res.Resources.Theme;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import io.intercom.com.bumptech.glide.load.l;
import io.intercom.com.bumptech.glide.load.resource.bitmap.k;
import io.intercom.com.bumptech.glide.load.resource.bitmap.m;
import java.util.HashMap;
import java.util.Map;

public class f implements Cloneable {
   private int a;
   private float b = 1.0F;
   private io.intercom.com.bumptech.glide.load.engine.h c;
   private io.intercom.com.bumptech.glide.g d;
   private Drawable e;
   private int f;
   private Drawable g;
   private int h;
   private boolean i;
   private int j;
   private int k;
   private io.intercom.com.bumptech.glide.load.g l;
   private boolean m;
   private boolean n;
   private Drawable o;
   private int p;
   private io.intercom.com.bumptech.glide.load.i q;
   private Map r;
   private Class s;
   private boolean t;
   private Theme u;
   private boolean v;
   private boolean w;
   private boolean x;
   private boolean y;

   public f() {
      this.c = io.intercom.com.bumptech.glide.load.engine.h.e;
      this.d = io.intercom.com.bumptech.glide.g.c;
      this.i = true;
      this.j = -1;
      this.k = -1;
      this.l = io.intercom.com.bumptech.glide.g.a.a();
      this.n = true;
      this.q = new io.intercom.com.bumptech.glide.load.i();
      this.r = new HashMap();
      this.s = Object.class;
      this.y = true;
   }

   private f H() {
      if(this.t) {
         throw new IllegalStateException("You cannot modify locked RequestOptions, consider clone()");
      } else {
         return this;
      }
   }

   public static f a(io.intercom.com.bumptech.glide.load.engine.h var0) {
      return (new f()).b(var0);
   }

   public static f a(io.intercom.com.bumptech.glide.load.g var0) {
      return (new f()).b(var0);
   }

   private f a(k var1, l var2, boolean var3) {
      f var4;
      if(var3) {
         var4 = this.b(var1, var2);
      } else {
         var4 = this.a(var1, var2);
      }

      var4.y = true;
      return var4;
   }

   public static f a(Class var0) {
      return (new f()).b(var0);
   }

   private static boolean b(int var0, int var1) {
      boolean var2;
      if((var0 & var1) != 0) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   private f c(k var1, l var2) {
      return this.a(var1, var2, false);
   }

   private boolean c(int var1) {
      return b(this.a, var1);
   }

   public final int A() {
      return this.k;
   }

   public final boolean B() {
      return io.intercom.com.bumptech.glide.h.i.a(this.k, this.j);
   }

   public final int C() {
      return this.j;
   }

   public final float D() {
      return this.b;
   }

   public boolean E() {
      return this.y;
   }

   public final boolean F() {
      return this.w;
   }

   public final boolean G() {
      return this.x;
   }

   public f a() {
      try {
         f var1 = (f)super.clone();
         io.intercom.com.bumptech.glide.load.i var2 = new io.intercom.com.bumptech.glide.load.i();
         var1.q = var2;
         var1.q.a(this.q);
         HashMap var4 = new HashMap();
         var1.r = var4;
         var1.r.putAll(this.r);
         var1.t = false;
         var1.v = false;
         return var1;
      } catch (CloneNotSupportedException var3) {
         throw new RuntimeException(var3);
      }
   }

   public f a(float var1) {
      f var2;
      if(this.v) {
         var2 = this.a().a(var1);
      } else {
         if(var1 < 0.0F || var1 > 1.0F) {
            throw new IllegalArgumentException("sizeMultiplier must be between 0 and 1");
         }

         this.b = var1;
         this.a |= 2;
         var2 = this.H();
      }

      return var2;
   }

   public f a(int var1) {
      f var2;
      if(this.v) {
         var2 = this.a().a(var1);
      } else {
         this.h = var1;
         this.a |= 128;
         var2 = this.H();
      }

      return var2;
   }

   public f a(int var1, int var2) {
      f var3;
      if(this.v) {
         var3 = this.a().a(var1, var2);
      } else {
         this.k = var1;
         this.j = var2;
         this.a |= 512;
         var3 = this.H();
      }

      return var3;
   }

   public f a(Drawable var1) {
      f var2;
      if(this.v) {
         var2 = this.a().a(var1);
      } else {
         this.g = var1;
         this.a |= 64;
         var2 = this.H();
      }

      return var2;
   }

   public f a(f var1) {
      if(this.v) {
         var1 = this.a().a(var1);
      } else {
         if(b(var1.a, 2)) {
            this.b = var1.b;
         }

         if(b(var1.a, 262144)) {
            this.w = var1.w;
         }

         if(b(var1.a, 4)) {
            this.c = var1.c;
         }

         if(b(var1.a, 8)) {
            this.d = var1.d;
         }

         if(b(var1.a, 16)) {
            this.e = var1.e;
         }

         if(b(var1.a, 32)) {
            this.f = var1.f;
         }

         if(b(var1.a, 64)) {
            this.g = var1.g;
         }

         if(b(var1.a, 128)) {
            this.h = var1.h;
         }

         if(b(var1.a, 256)) {
            this.i = var1.i;
         }

         if(b(var1.a, 512)) {
            this.k = var1.k;
            this.j = var1.j;
         }

         if(b(var1.a, 1024)) {
            this.l = var1.l;
         }

         if(b(var1.a, 4096)) {
            this.s = var1.s;
         }

         if(b(var1.a, 8192)) {
            this.o = var1.o;
         }

         if(b(var1.a, 16384)) {
            this.p = var1.p;
         }

         if(b(var1.a, '耀')) {
            this.u = var1.u;
         }

         if(b(var1.a, 65536)) {
            this.n = var1.n;
         }

         if(b(var1.a, 131072)) {
            this.m = var1.m;
         }

         if(b(var1.a, 2048)) {
            this.r.putAll(var1.r);
            this.y = var1.y;
         }

         if(b(var1.a, 524288)) {
            this.x = var1.x;
         }

         if(!this.n) {
            this.r.clear();
            this.a &= -2049;
            this.m = false;
            this.a &= -131073;
            this.y = true;
         }

         this.a |= var1.a;
         this.q.a(var1.q);
         var1 = this.H();
      }

      return var1;
   }

   public f a(io.intercom.com.bumptech.glide.g var1) {
      f var2;
      if(this.v) {
         var2 = this.a().a(var1);
      } else {
         this.d = (io.intercom.com.bumptech.glide.g)io.intercom.com.bumptech.glide.h.h.a((Object)var1);
         this.a |= 8;
         var2 = this.H();
      }

      return var2;
   }

   public f a(io.intercom.com.bumptech.glide.load.h var1, Object var2) {
      f var3;
      if(this.v) {
         var3 = this.a().a(var1, var2);
      } else {
         io.intercom.com.bumptech.glide.h.h.a((Object)var1);
         io.intercom.com.bumptech.glide.h.h.a(var2);
         this.q.a(var1, var2);
         var3 = this.H();
      }

      return var3;
   }

   public f a(l var1) {
      f var2;
      if(this.v) {
         var2 = this.a().a(var1);
      } else {
         this.b(var1);
         this.m = true;
         this.a |= 131072;
         var2 = this.H();
      }

      return var2;
   }

   public f a(k var1) {
      return this.a(io.intercom.com.bumptech.glide.load.resource.bitmap.l.b, io.intercom.com.bumptech.glide.h.h.a((Object)var1));
   }

   final f a(k var1, l var2) {
      f var3;
      if(this.v) {
         var3 = this.a().a(var1, var2);
      } else {
         this.a(var1);
         var3 = this.b(var2);
      }

      return var3;
   }

   public f a(Class var1, l var2) {
      f var3;
      if(this.v) {
         var3 = this.a().a(var1, var2);
      } else {
         io.intercom.com.bumptech.glide.h.h.a((Object)var1);
         io.intercom.com.bumptech.glide.h.h.a((Object)var2);
         this.r.put(var1, var2);
         this.a |= 2048;
         this.n = true;
         this.a |= 65536;
         this.y = false;
         var3 = this.H();
      }

      return var3;
   }

   public f a(boolean var1) {
      boolean var2 = true;
      f var3;
      if(this.v) {
         var3 = this.a().a(true);
      } else {
         if(!var1) {
            var1 = var2;
         } else {
            var1 = false;
         }

         this.i = var1;
         this.a |= 256;
         var3 = this.H();
      }

      return var3;
   }

   public f b(int var1) {
      f var2;
      if(this.v) {
         var2 = this.a().b(var1);
      } else {
         this.f = var1;
         this.a |= 32;
         var2 = this.H();
      }

      return var2;
   }

   public f b(Drawable var1) {
      f var2;
      if(this.v) {
         var2 = this.a().b(var1);
      } else {
         this.e = var1;
         this.a |= 16;
         var2 = this.H();
      }

      return var2;
   }

   public f b(io.intercom.com.bumptech.glide.load.engine.h var1) {
      f var2;
      if(this.v) {
         var2 = this.a().b(var1);
      } else {
         this.c = (io.intercom.com.bumptech.glide.load.engine.h)io.intercom.com.bumptech.glide.h.h.a((Object)var1);
         this.a |= 4;
         var2 = this.H();
      }

      return var2;
   }

   public f b(io.intercom.com.bumptech.glide.load.g var1) {
      f var2;
      if(this.v) {
         var2 = this.a().b(var1);
      } else {
         this.l = (io.intercom.com.bumptech.glide.load.g)io.intercom.com.bumptech.glide.h.h.a((Object)var1);
         this.a |= 1024;
         var2 = this.H();
      }

      return var2;
   }

   public f b(l var1) {
      f var2;
      if(this.v) {
         var2 = this.a().b(var1);
      } else {
         this.a(Bitmap.class, var1);
         this.a((Class)BitmapDrawable.class, (l)(new io.intercom.com.bumptech.glide.load.resource.bitmap.c(var1)));
         this.a((Class)io.intercom.com.bumptech.glide.load.resource.d.c.class, (l)(new io.intercom.com.bumptech.glide.load.resource.d.f(var1)));
         var2 = this.H();
      }

      return var2;
   }

   final f b(k var1, l var2) {
      f var3;
      if(this.v) {
         var3 = this.a().b(var1, var2);
      } else {
         this.a(var1);
         var3 = this.a(var2);
      }

      return var3;
   }

   public f b(Class var1) {
      f var2;
      if(this.v) {
         var2 = this.a().b(var1);
      } else {
         this.s = (Class)io.intercom.com.bumptech.glide.h.h.a((Object)var1);
         this.a |= 4096;
         var2 = this.H();
      }

      return var2;
   }

   public final boolean b() {
      return this.n;
   }

   public final boolean c() {
      return this.c(2048);
   }

   // $FF: synthetic method
   public Object clone() throws CloneNotSupportedException {
      return this.a();
   }

   public final boolean d() {
      return this.t;
   }

   public f e() {
      return this.a((k)k.b, (l)(new io.intercom.com.bumptech.glide.load.resource.bitmap.h()));
   }

   public boolean equals(Object var1) {
      boolean var3 = false;
      boolean var2 = var3;
      if(var1 instanceof f) {
         f var4 = (f)var1;
         var2 = var3;
         if(Float.compare(var4.b, this.b) == 0) {
            var2 = var3;
            if(this.f == var4.f) {
               var2 = var3;
               if(io.intercom.com.bumptech.glide.h.i.a((Object)this.e, (Object)var4.e)) {
                  var2 = var3;
                  if(this.h == var4.h) {
                     var2 = var3;
                     if(io.intercom.com.bumptech.glide.h.i.a((Object)this.g, (Object)var4.g)) {
                        var2 = var3;
                        if(this.p == var4.p) {
                           var2 = var3;
                           if(io.intercom.com.bumptech.glide.h.i.a((Object)this.o, (Object)var4.o)) {
                              var2 = var3;
                              if(this.i == var4.i) {
                                 var2 = var3;
                                 if(this.j == var4.j) {
                                    var2 = var3;
                                    if(this.k == var4.k) {
                                       var2 = var3;
                                       if(this.m == var4.m) {
                                          var2 = var3;
                                          if(this.n == var4.n) {
                                             var2 = var3;
                                             if(this.w == var4.w) {
                                                var2 = var3;
                                                if(this.x == var4.x) {
                                                   var2 = var3;
                                                   if(this.c.equals(var4.c)) {
                                                      var2 = var3;
                                                      if(this.d == var4.d) {
                                                         var2 = var3;
                                                         if(this.q.equals(var4.q)) {
                                                            var2 = var3;
                                                            if(this.r.equals(var4.r)) {
                                                               var2 = var3;
                                                               if(this.s.equals(var4.s)) {
                                                                  var2 = var3;
                                                                  if(io.intercom.com.bumptech.glide.h.i.a((Object)this.l, (Object)var4.l)) {
                                                                     var2 = var3;
                                                                     if(io.intercom.com.bumptech.glide.h.i.a((Object)this.u, (Object)var4.u)) {
                                                                        var2 = true;
                                                                     }
                                                                  }
                                                               }
                                                            }
                                                         }
                                                      }
                                                   }
                                                }
                                             }
                                          }
                                       }
                                    }
                                 }
                              }
                           }
                        }
                     }
                  }
               }
            }
         }
      }

      return var2;
   }

   public f f() {
      return this.c(k.a, new m());
   }

   public f g() {
      return this.c(k.e, new io.intercom.com.bumptech.glide.load.resource.bitmap.i());
   }

   public f h() {
      f var1;
      if(this.v) {
         var1 = this.a().h();
      } else {
         this.a((io.intercom.com.bumptech.glide.load.h)io.intercom.com.bumptech.glide.load.resource.d.a.a, (Object)Boolean.valueOf(true));
         this.a((io.intercom.com.bumptech.glide.load.h)io.intercom.com.bumptech.glide.load.resource.d.i.a, (Object)Boolean.valueOf(true));
         var1 = this.H();
      }

      return var1;
   }

   public int hashCode() {
      int var1 = io.intercom.com.bumptech.glide.h.i.a(this.b);
      var1 = io.intercom.com.bumptech.glide.h.i.b(this.f, var1);
      var1 = io.intercom.com.bumptech.glide.h.i.a(this.e, var1);
      var1 = io.intercom.com.bumptech.glide.h.i.b(this.h, var1);
      var1 = io.intercom.com.bumptech.glide.h.i.a(this.g, var1);
      var1 = io.intercom.com.bumptech.glide.h.i.b(this.p, var1);
      var1 = io.intercom.com.bumptech.glide.h.i.a(this.o, var1);
      var1 = io.intercom.com.bumptech.glide.h.i.a(this.i, var1);
      var1 = io.intercom.com.bumptech.glide.h.i.b(this.j, var1);
      var1 = io.intercom.com.bumptech.glide.h.i.b(this.k, var1);
      var1 = io.intercom.com.bumptech.glide.h.i.a(this.m, var1);
      var1 = io.intercom.com.bumptech.glide.h.i.a(this.n, var1);
      var1 = io.intercom.com.bumptech.glide.h.i.a(this.w, var1);
      var1 = io.intercom.com.bumptech.glide.h.i.a(this.x, var1);
      var1 = io.intercom.com.bumptech.glide.h.i.a(this.c, var1);
      var1 = io.intercom.com.bumptech.glide.h.i.a(this.d, var1);
      var1 = io.intercom.com.bumptech.glide.h.i.a(this.q, var1);
      var1 = io.intercom.com.bumptech.glide.h.i.a(this.r, var1);
      var1 = io.intercom.com.bumptech.glide.h.i.a(this.s, var1);
      var1 = io.intercom.com.bumptech.glide.h.i.a(this.l, var1);
      return io.intercom.com.bumptech.glide.h.i.a(this.u, var1);
   }

   public f i() {
      this.t = true;
      return this;
   }

   public f j() {
      if(this.t && !this.v) {
         throw new IllegalStateException("You cannot auto lock an already locked options object, try clone() first");
      } else {
         this.v = true;
         return this.i();
      }
   }

   public final Map k() {
      return this.r;
   }

   public final boolean l() {
      return this.m;
   }

   public final io.intercom.com.bumptech.glide.load.i m() {
      return this.q;
   }

   public final Class n() {
      return this.s;
   }

   public final io.intercom.com.bumptech.glide.load.engine.h o() {
      return this.c;
   }

   public final Drawable p() {
      return this.e;
   }

   public final int q() {
      return this.f;
   }

   public final int r() {
      return this.h;
   }

   public final Drawable s() {
      return this.g;
   }

   public final int t() {
      return this.p;
   }

   public final Drawable u() {
      return this.o;
   }

   public final Theme v() {
      return this.u;
   }

   public final boolean w() {
      return this.i;
   }

   public final io.intercom.com.bumptech.glide.load.g x() {
      return this.l;
   }

   public final boolean y() {
      return this.c(8);
   }

   public final io.intercom.com.bumptech.glide.g z() {
      return this.d;
   }
}
