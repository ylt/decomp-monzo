package io.intercom.com.bumptech.glide.f.b;

public class a implements e {
   private final int a;
   private final boolean b;
   private b c;

   protected a(int var1, boolean var2) {
      this.a = var1;
      this.b = var2;
   }

   private d a() {
      if(this.c == null) {
         this.c = new b(this.a, this.b);
      }

      return this.c;
   }

   public d a(io.intercom.com.bumptech.glide.load.a var1, boolean var2) {
      d var3;
      if(var1 == io.intercom.com.bumptech.glide.load.a.e) {
         var3 = c.b();
      } else {
         var3 = this.a();
      }

      return var3;
   }

   public static class a {
      private int a;
      private boolean b;

      public a() {
         this(300);
      }

      public a(int var1) {
         this.a = var1;
      }

      public a a() {
         return new a(this.a, this.b);
      }
   }
}
