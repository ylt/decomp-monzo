package io.intercom.com.bumptech.glide.f.b;

import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;

public class b implements d {
   private final int a;
   private final boolean b;

   public b(int var1, boolean var2) {
      this.a = var1;
      this.b = var2;
   }

   public boolean a(Drawable var1, d.a var2) {
      Drawable var4 = var2.a();
      Object var3 = var4;
      if(var4 == null) {
         var3 = new ColorDrawable(0);
      }

      TransitionDrawable var5 = new TransitionDrawable(new Drawable[]{(Drawable)var3, var1});
      var5.setCrossFadeEnabled(this.b);
      var5.startTransition(this.a);
      var2.b(var5);
      return true;
   }
}
