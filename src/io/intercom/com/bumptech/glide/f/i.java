package io.intercom.com.bumptech.glide.f;

public class i implements b, c {
   private b a;
   private b b;
   private c c;
   private boolean d;

   public i() {
      this((c)null);
   }

   public i(c var1) {
      this.c = var1;
   }

   private boolean j() {
      boolean var1;
      if(this.c != null && !this.c.b(this)) {
         var1 = false;
      } else {
         var1 = true;
      }

      return var1;
   }

   private boolean k() {
      boolean var1;
      if(this.c != null && !this.c.c(this)) {
         var1 = false;
      } else {
         var1 = true;
      }

      return var1;
   }

   private boolean l() {
      boolean var1;
      if(this.c != null && this.c.d()) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public void a() {
      this.d = true;
      if(!this.b.e()) {
         this.b.a();
      }

      if(this.d && !this.a.e()) {
         this.a.a();
      }

   }

   public void a(b var1, b var2) {
      this.a = var1;
      this.b = var2;
   }

   public boolean a(b var1) {
      boolean var3 = false;
      boolean var2 = var3;
      if(var1 instanceof i) {
         i var4 = (i)var1;
         if(this.a == null) {
            var2 = var3;
            if(var4.a != null) {
               return var2;
            }
         } else {
            var2 = var3;
            if(!this.a.a(var4.a)) {
               return var2;
            }
         }

         if(this.b == null) {
            var2 = var3;
            if(var4.b != null) {
               return var2;
            }
         } else {
            var2 = var3;
            if(!this.b.a(var4.b)) {
               return var2;
            }
         }

         var2 = true;
      }

      return var2;
   }

   public void b() {
      this.d = false;
      this.a.b();
      this.b.b();
   }

   public boolean b(b var1) {
      boolean var2;
      if(!this.j() || !var1.equals(this.a) && this.a.g()) {
         var2 = false;
      } else {
         var2 = true;
      }

      return var2;
   }

   public void c() {
      this.d = false;
      this.b.c();
      this.a.c();
   }

   public boolean c(b var1) {
      boolean var2;
      if(this.k() && var1.equals(this.a) && !this.d()) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   public void d(b var1) {
      if(!var1.equals(this.b)) {
         if(this.c != null) {
            this.c.d(this);
         }

         if(!this.b.f()) {
            this.b.c();
         }
      }

   }

   public boolean d() {
      boolean var1;
      if(!this.l() && !this.g()) {
         var1 = false;
      } else {
         var1 = true;
      }

      return var1;
   }

   public boolean e() {
      return this.a.e();
   }

   public boolean f() {
      boolean var1;
      if(!this.a.f() && !this.b.f()) {
         var1 = false;
      } else {
         var1 = true;
      }

      return var1;
   }

   public boolean g() {
      boolean var1;
      if(!this.a.g() && !this.b.g()) {
         var1 = false;
      } else {
         var1 = true;
      }

      return var1;
   }

   public boolean h() {
      return this.a.h();
   }

   public void i() {
      this.a.i();
      this.b.i();
   }
}
