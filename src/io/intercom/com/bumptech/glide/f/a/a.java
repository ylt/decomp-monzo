package io.intercom.com.bumptech.glide.f.a;

import android.graphics.drawable.Drawable;

public abstract class a implements h {
   private io.intercom.com.bumptech.glide.f.b request;

   public io.intercom.com.bumptech.glide.f.b getRequest() {
      return this.request;
   }

   public void onDestroy() {
   }

   public void onLoadCleared(Drawable var1) {
   }

   public void onLoadFailed(Drawable var1) {
   }

   public void onLoadStarted(Drawable var1) {
   }

   public void onStart() {
   }

   public void onStop() {
   }

   public void setRequest(io.intercom.com.bumptech.glide.f.b var1) {
      this.request = var1;
   }
}
