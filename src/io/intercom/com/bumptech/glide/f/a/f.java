package io.intercom.com.bumptech.glide.f.a;

public abstract class f extends a {
   private final int height;
   private final int width;

   public f() {
      this(Integer.MIN_VALUE, Integer.MIN_VALUE);
   }

   public f(int var1, int var2) {
      this.width = var1;
      this.height = var2;
   }

   public final void getSize(g var1) {
      if(!io.intercom.com.bumptech.glide.h.i.a(this.width, this.height)) {
         throw new IllegalArgumentException("Width and height must both be > 0 or Target#SIZE_ORIGINAL, but given width: " + this.width + " and height: " + this.height + ", either provide dimensions in the constructor or call override()");
      } else {
         var1.a(this.width, this.height);
      }
   }

   public void removeCallback(g var1) {
   }
}
