package io.intercom.com.bumptech.glide.f.a;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

public class e {
   public h a(ImageView var1, Class var2) {
      Object var3;
      if(Bitmap.class.equals(var2)) {
         var3 = new b(var1);
      } else {
         if(!Drawable.class.isAssignableFrom(var2)) {
            throw new IllegalArgumentException("Unhandled class: " + var2 + ", try .as*(Class).transcode(ResourceTranscoder)");
         }

         var3 = new c(var1);
      }

      return (h)var3;
   }
}
