package io.intercom.com.bumptech.glide.f.a;

import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

public abstract class d extends i implements io.intercom.com.bumptech.glide.f.b.d.a {
   private Animatable b;

   public d(ImageView var1) {
      super(var1);
   }

   private void b(Object var1) {
      this.c(var1);
      this.a(var1);
   }

   private void c(Object var1) {
      if(var1 instanceof Animatable) {
         this.b = (Animatable)var1;
         this.b.start();
      } else {
         this.b = null;
      }

   }

   public Drawable a() {
      return ((ImageView)this.a).getDrawable();
   }

   protected abstract void a(Object var1);

   public void b(Drawable var1) {
      ((ImageView)this.a).setImageDrawable(var1);
   }

   public void onLoadCleared(Drawable var1) {
      super.onLoadCleared(var1);
      this.b((Object)null);
      this.b(var1);
   }

   public void onLoadFailed(Drawable var1) {
      super.onLoadFailed(var1);
      this.b((Object)null);
      this.b(var1);
   }

   public void onLoadStarted(Drawable var1) {
      super.onLoadStarted(var1);
      this.b((Object)null);
      this.b(var1);
   }

   public void onResourceReady(Object var1, io.intercom.com.bumptech.glide.f.b.d var2) {
      if(var2 != null && var2.a(var1, this)) {
         this.c(var1);
      } else {
         this.b(var1);
      }

   }

   public void onStart() {
      if(this.b != null) {
         this.b.start();
      }

   }

   public void onStop() {
      if(this.b != null) {
         this.b.stop();
      }

   }
}
