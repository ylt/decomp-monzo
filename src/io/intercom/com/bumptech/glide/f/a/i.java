package io.intercom.com.bumptech.glide.f.a;

import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewTreeObserver.OnPreDrawListener;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public abstract class i extends a {
   private static boolean b = false;
   private static Integer c = null;
   protected final View a;
   private final i.a d;

   public i(View var1) {
      this.a = (View)io.intercom.com.bumptech.glide.h.h.a((Object)var1);
      this.d = new i.a(var1);
   }

   private void a(Object var1) {
      if(c == null) {
         b = true;
         this.a.setTag(var1);
      } else {
         this.a.setTag(c.intValue(), var1);
      }

   }

   private Object b() {
      Object var1;
      if(c == null) {
         var1 = this.a.getTag();
      } else {
         var1 = this.a.getTag(c.intValue());
      }

      return var1;
   }

   public io.intercom.com.bumptech.glide.f.b getRequest() {
      Object var1 = this.b();
      io.intercom.com.bumptech.glide.f.b var2;
      if(var1 != null) {
         if(!(var1 instanceof io.intercom.com.bumptech.glide.f.b)) {
            throw new IllegalArgumentException("You must not call setTag() on a view Glide is targeting");
         }

         var2 = (io.intercom.com.bumptech.glide.f.b)var1;
      } else {
         var2 = null;
      }

      return var2;
   }

   public void getSize(g var1) {
      this.d.a(var1);
   }

   public void onLoadCleared(Drawable var1) {
      super.onLoadCleared(var1);
      this.d.b();
   }

   public void removeCallback(g var1) {
      this.d.b(var1);
   }

   public void setRequest(io.intercom.com.bumptech.glide.f.b var1) {
      this.a(var1);
   }

   public String toString() {
      return "Target for: " + this.a;
   }

   private static class a {
      private final View a;
      private final List b = new ArrayList();
      private i.a c;

      a(View var1) {
         this.a = var1;
      }

      private int a(int var1, int var2, int var3) {
         var1 -= var3;
         if(!this.a(var1)) {
            if(var2 == 0) {
               var1 = 0;
            } else if(var2 == -2) {
               var1 = Integer.MIN_VALUE;
            } else if(var2 > 0) {
               var1 = var2 - var3;
            } else {
               var1 = 0;
            }
         }

         return var1;
      }

      private void a(int var1, int var2) {
         Iterator var3 = (new ArrayList(this.b)).iterator();

         while(var3.hasNext()) {
            ((g)var3.next()).a(var1, var2);
         }

      }

      private boolean a(int var1) {
         boolean var2;
         if(var1 <= 0 && var1 != Integer.MIN_VALUE) {
            var2 = false;
         } else {
            var2 = true;
         }

         return var2;
      }

      private boolean b(int var1, int var2) {
         boolean var3;
         if(this.c() && this.a(var1) && this.a(var2)) {
            var3 = true;
         } else {
            var3 = false;
         }

         return var3;
      }

      private boolean c() {
         boolean var1 = true;
         if((this.a.getLayoutParams() == null || this.a.getLayoutParams().width <= 0 || this.a.getLayoutParams().height <= 0) && this.a.isLayoutRequested()) {
            var1 = false;
         }

         return var1;
      }

      private int d() {
         int var2 = this.a.getPaddingTop();
         int var3 = this.a.getPaddingBottom();
         LayoutParams var4 = this.a.getLayoutParams();
         int var1;
         if(var4 != null) {
            var1 = var4.height;
         } else {
            var1 = 0;
         }

         return this.a(this.a.getHeight(), var1, var3 + var2);
      }

      private int e() {
         int var3 = this.a.getPaddingLeft();
         int var2 = this.a.getPaddingRight();
         LayoutParams var4 = this.a.getLayoutParams();
         int var1;
         if(var4 != null) {
            var1 = var4.width;
         } else {
            var1 = 0;
         }

         return this.a(this.a.getWidth(), var1, var2 + var3);
      }

      void a() {
         if(!this.b.isEmpty()) {
            int var2 = this.e();
            int var1 = this.d();
            if(this.b(var2, var1)) {
               this.a(var2, var1);
               this.b();
            }
         }

      }

      void a(g var1) {
         int var2 = this.e();
         int var3 = this.d();
         if(this.b(var2, var3)) {
            var1.a(var2, var3);
         } else {
            if(!this.b.contains(var1)) {
               this.b.add(var1);
            }

            if(this.c == null) {
               ViewTreeObserver var4 = this.a.getViewTreeObserver();
               this.c = new i.a(this);
               var4.addOnPreDrawListener(this.c);
            }
         }

      }

      void b() {
         ViewTreeObserver var1 = this.a.getViewTreeObserver();
         if(var1.isAlive()) {
            var1.removeOnPreDrawListener(this.c);
         }

         this.c = null;
         this.b.clear();
      }

      void b(g var1) {
         this.b.remove(var1);
      }
   }

   private static class a implements OnPreDrawListener {
      private final WeakReference a;

      a(i.a var1) {
         this.a = new WeakReference(var1);
      }

      public boolean onPreDraw() {
         if(Log.isLoggable("ViewTarget", 2)) {
            Log.v("ViewTarget", "OnGlobalLayoutListener called listener=" + this);
         }

         i.a var1 = (i.a)this.a.get();
         if(var1 != null) {
            var1.a();
         }

         return true;
      }
   }
}
