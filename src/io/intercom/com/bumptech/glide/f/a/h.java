package io.intercom.com.bumptech.glide.f.a;

import android.graphics.drawable.Drawable;

public interface h extends io.intercom.com.bumptech.glide.c.i {
   io.intercom.com.bumptech.glide.f.b getRequest();

   void getSize(g var1);

   void onLoadCleared(Drawable var1);

   void onLoadFailed(Drawable var1);

   void onLoadStarted(Drawable var1);

   void onResourceReady(Object var1, io.intercom.com.bumptech.glide.f.b.d var2);

   void removeCallback(g var1);

   void setRequest(io.intercom.com.bumptech.glide.f.b var1);
}
