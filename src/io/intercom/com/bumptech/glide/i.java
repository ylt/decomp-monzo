package io.intercom.com.bumptech.glide;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import io.intercom.com.bumptech.glide.c.m;
import io.intercom.com.bumptech.glide.c.n;
import io.intercom.com.bumptech.glide.c.p;
import java.io.File;
import java.util.Iterator;

public class i implements io.intercom.com.bumptech.glide.c.i {
   private static final io.intercom.com.bumptech.glide.f.f c = io.intercom.com.bumptech.glide.f.f.a(Bitmap.class).i();
   private static final io.intercom.com.bumptech.glide.f.f d = io.intercom.com.bumptech.glide.f.f.a(io.intercom.com.bumptech.glide.load.resource.d.c.class).i();
   private static final io.intercom.com.bumptech.glide.f.f e;
   protected final c a;
   final io.intercom.com.bumptech.glide.c.h b;
   private final n f;
   private final m g;
   private final p h;
   private final Runnable i;
   private final Handler j;
   private final io.intercom.com.bumptech.glide.c.c k;
   private io.intercom.com.bumptech.glide.f.f l;

   static {
      e = io.intercom.com.bumptech.glide.f.f.a(io.intercom.com.bumptech.glide.load.engine.h.c).a(g.d).a(true);
   }

   public i(c var1, io.intercom.com.bumptech.glide.c.h var2, m var3) {
      this(var1, var2, var3, new n(), var1.d());
   }

   i(c var1, io.intercom.com.bumptech.glide.c.h var2, m var3, n var4, io.intercom.com.bumptech.glide.c.d var5) {
      this.h = new p();
      this.i = new Runnable() {
         public void run() {
            i.this.b.a(i.this);
         }
      };
      this.j = new Handler(Looper.getMainLooper());
      this.a = var1;
      this.b = var2;
      this.g = var3;
      this.f = var4;
      this.k = var5.a(var1.e().getBaseContext(), new i.b(var4));
      if(io.intercom.com.bumptech.glide.h.i.d()) {
         this.j.post(this.i);
      } else {
         var2.a(this);
      }

      var2.a(this.k);
      this.a(var1.e().a());
      var1.a(this);
   }

   private void c(io.intercom.com.bumptech.glide.f.a.h var1) {
      if(!this.b(var1)) {
         this.a.a(var1);
      }

   }

   public h a(Class var1) {
      return new h(this.a, this, var1);
   }

   public h a(Object var1) {
      return this.d().a(var1);
   }

   public void a() {
      io.intercom.com.bumptech.glide.h.i.a();
      this.f.a();
   }

   public void a(View var1) {
      this.a((io.intercom.com.bumptech.glide.f.a.h)(new i.a(var1)));
   }

   public void a(final io.intercom.com.bumptech.glide.f.a.h var1) {
      if(var1 != null) {
         if(io.intercom.com.bumptech.glide.h.i.c()) {
            this.c(var1);
         } else {
            this.j.post(new Runnable() {
               public void run() {
                  i.this.a(var1);
               }
            });
         }
      }

   }

   void a(io.intercom.com.bumptech.glide.f.a.h var1, io.intercom.com.bumptech.glide.f.b var2) {
      this.h.a(var1);
      this.f.a(var2);
   }

   protected void a(io.intercom.com.bumptech.glide.f.f var1) {
      this.l = var1.a().j();
   }

   j b(Class var1) {
      return this.a.e().a(var1);
   }

   public void b() {
      io.intercom.com.bumptech.glide.h.i.a();
      this.f.b();
   }

   boolean b(io.intercom.com.bumptech.glide.f.a.h var1) {
      boolean var2 = true;
      io.intercom.com.bumptech.glide.f.b var3 = var1.getRequest();
      if(var3 != null) {
         if(this.f.b(var3)) {
            this.h.b(var1);
            var1.setRequest((io.intercom.com.bumptech.glide.f.b)null);
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public h c() {
      return this.a(Bitmap.class).a(c);
   }

   public h d() {
      return this.a(Drawable.class);
   }

   public h e() {
      return this.a(File.class).a(e);
   }

   io.intercom.com.bumptech.glide.f.f f() {
      return this.l;
   }

   public void onDestroy() {
      this.h.onDestroy();
      Iterator var1 = this.h.a().iterator();

      while(var1.hasNext()) {
         this.a((io.intercom.com.bumptech.glide.f.a.h)var1.next());
      }

      this.h.b();
      this.f.c();
      this.b.b(this);
      this.b.b(this.k);
      this.j.removeCallbacks(this.i);
      this.a.b(this);
   }

   public void onStart() {
      this.b();
      this.h.onStart();
   }

   public void onStop() {
      this.a();
      this.h.onStop();
   }

   public String toString() {
      return super.toString() + "{tracker=" + this.f + ", treeNode=" + this.g + "}";
   }

   private static class a extends io.intercom.com.bumptech.glide.f.a.i {
      public a(View var1) {
         super(var1);
      }

      public void onResourceReady(Object var1, io.intercom.com.bumptech.glide.f.b.d var2) {
      }
   }

   private static class b implements io.intercom.com.bumptech.glide.c.c.a {
      private final n a;

      public b(n var1) {
         this.a = var1;
      }

      public void a(boolean var1) {
         if(var1) {
            this.a.d();
         }

      }
   }
}
