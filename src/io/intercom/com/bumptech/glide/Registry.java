package io.intercom.com.bumptech.glide;

import io.intercom.com.bumptech.glide.load.k;
import io.intercom.com.bumptech.glide.load.b.n;
import io.intercom.com.bumptech.glide.load.b.o;
import io.intercom.com.bumptech.glide.load.engine.p;
import io.intercom.com.bumptech.glide.load.engine.r;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class Registry {
   private final o a;
   private final io.intercom.com.bumptech.glide.e.a b;
   private final io.intercom.com.bumptech.glide.e.e c;
   private final io.intercom.com.bumptech.glide.e.f d;
   private final io.intercom.com.bumptech.glide.load.a.d e;
   private final io.intercom.com.bumptech.glide.load.resource.e.e f;
   private final io.intercom.com.bumptech.glide.e.b g;
   private final io.intercom.com.bumptech.glide.e.d h = new io.intercom.com.bumptech.glide.e.d();
   private final io.intercom.com.bumptech.glide.e.c i = new io.intercom.com.bumptech.glide.e.c();
   private final android.support.v4.g.k.a j = io.intercom.com.bumptech.glide.h.a.a.a();

   public Registry() {
      this.a = new o(this.j);
      this.b = new io.intercom.com.bumptech.glide.e.a();
      this.c = new io.intercom.com.bumptech.glide.e.e();
      this.d = new io.intercom.com.bumptech.glide.e.f();
      this.e = new io.intercom.com.bumptech.glide.load.a.d();
      this.f = new io.intercom.com.bumptech.glide.load.resource.e.e();
      this.g = new io.intercom.com.bumptech.glide.e.b();
   }

   private List c(Class var1, Class var2, Class var3) {
      ArrayList var4 = new ArrayList();
      Iterator var6 = this.c.b(var1, var2).iterator();

      while(var6.hasNext()) {
         Class var5 = (Class)var6.next();
         Iterator var7 = this.f.b(var5, var3).iterator();

         while(var7.hasNext()) {
            var2 = (Class)var7.next();
            var4.add(new io.intercom.com.bumptech.glide.load.engine.g(var1, var5, var2, this.c.a(var1, var5), this.f.a(var5, var2), this.j));
         }
      }

      return var4;
   }

   public Registry a(io.intercom.com.bumptech.glide.load.a.c.a var1) {
      this.e.a(var1);
      return this;
   }

   public Registry a(io.intercom.com.bumptech.glide.load.e var1) {
      this.g.a(var1);
      return this;
   }

   public Registry a(Class var1, io.intercom.com.bumptech.glide.load.d var2) {
      this.b.a(var1, var2);
      return this;
   }

   public Registry a(Class var1, k var2) {
      this.d.a(var1, var2);
      return this;
   }

   public Registry a(Class var1, Class var2, n var3) {
      this.a.a(var1, var2, var3);
      return this;
   }

   public Registry a(Class var1, Class var2, io.intercom.com.bumptech.glide.load.j var3) {
      this.c.a(var3, var1, var2);
      return this;
   }

   public Registry a(Class var1, Class var2, io.intercom.com.bumptech.glide.load.resource.e.d var3) {
      this.f.a(var1, var2, var3);
      return this;
   }

   public io.intercom.com.bumptech.glide.load.d a(Object var1) throws Registry.NoSourceEncoderAvailableException {
      io.intercom.com.bumptech.glide.load.d var2 = this.b.a(var1.getClass());
      if(var2 != null) {
         return var2;
      } else {
         throw new Registry.NoSourceEncoderAvailableException(var1.getClass());
      }
   }

   public p a(Class var1, Class var2, Class var3) {
      p var5 = this.i.b(var1, var2, var3);
      p var4 = var5;
      if(var5 == null) {
         var4 = var5;
         if(!this.i.a(var1, var2, var3)) {
            List var6 = this.c(var1, var2, var3);
            if(var6.isEmpty()) {
               var4 = null;
            } else {
               var4 = new p(var1, var2, var3, var6, this.j);
            }

            this.i.a(var1, var2, var3, var4);
         }
      }

      return var4;
   }

   public List a() {
      List var1 = this.g.a();
      if(var1.isEmpty()) {
         throw new Registry.NoImageHeaderParserException();
      } else {
         return var1;
      }
   }

   public boolean a(r var1) {
      boolean var2;
      if(this.d.a(var1.b()) != null) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   public Registry b(Class var1, Class var2, io.intercom.com.bumptech.glide.load.j var3) {
      this.c.b(var3, var1, var2);
      return this;
   }

   public io.intercom.com.bumptech.glide.load.a.c b(Object var1) {
      return this.e.a(var1);
   }

   public k b(r var1) throws Registry.NoResultEncoderAvailableException {
      k var2 = this.d.a(var1.b());
      if(var2 != null) {
         return var2;
      } else {
         throw new Registry.NoResultEncoderAvailableException(var1.b());
      }
   }

   public List b(Class var1, Class var2, Class var3) {
      List var5 = this.h.a(var1, var2);
      Object var4 = var5;
      if(var5 == null) {
         var4 = new ArrayList();
         Iterator var8 = this.a.a(var1).iterator();

         while(var8.hasNext()) {
            Class var6 = (Class)var8.next();
            Iterator var7 = this.c.b(var6, var2).iterator();

            while(var7.hasNext()) {
               var6 = (Class)var7.next();
               if(!this.f.b(var6, var3).isEmpty() && !((List)var4).contains(var6)) {
                  ((List)var4).add(var6);
               }
            }
         }

         this.h.a(var1, var2, Collections.unmodifiableList((List)var4));
      }

      return (List)var4;
   }

   public List c(Object var1) {
      List var2 = this.a.a(var1);
      if(var2.isEmpty()) {
         throw new Registry.NoModelLoaderAvailableException(var1);
      } else {
         return var2;
      }
   }

   public static class MissingComponentException extends RuntimeException {
      public MissingComponentException(String var1) {
         super(var1);
      }
   }

   public static final class NoImageHeaderParserException extends Registry.MissingComponentException {
      public NoImageHeaderParserException() {
         super("Failed to find image header parser.");
      }
   }

   public static class NoModelLoaderAvailableException extends Registry.MissingComponentException {
      public NoModelLoaderAvailableException(Class var1, Class var2) {
         super("Failed to find any ModelLoaders for model: " + var1 + " and data: " + var2);
      }

      public NoModelLoaderAvailableException(Object var1) {
         super("Failed to find any ModelLoaders for model: " + var1);
      }
   }

   public static class NoResultEncoderAvailableException extends Registry.MissingComponentException {
      public NoResultEncoderAvailableException(Class var1) {
         super("Failed to find result encoder for resource class: " + var1);
      }
   }

   public static class NoSourceEncoderAvailableException extends Registry.MissingComponentException {
      public NoSourceEncoderAvailableException(Class var1) {
         super("Failed to find source encoder for data class: " + var1);
      }
   }
}
