package io.intercom.com.bumptech.glide;

public abstract class j implements Cloneable {
   private io.intercom.com.bumptech.glide.f.b.e a = io.intercom.com.bumptech.glide.f.b.c.a();

   private j c() {
      return this;
   }

   protected final j a() {
      try {
         j var1 = (j)super.clone();
         return var1;
      } catch (CloneNotSupportedException var2) {
         throw new RuntimeException(var2);
      }
   }

   public final j a(io.intercom.com.bumptech.glide.f.b.e var1) {
      this.a = (io.intercom.com.bumptech.glide.f.b.e)io.intercom.com.bumptech.glide.h.h.a((Object)var1);
      return this.c();
   }

   final io.intercom.com.bumptech.glide.f.b.e b() {
      return this.a;
   }

   // $FF: synthetic method
   protected Object clone() throws CloneNotSupportedException {
      return this.a();
   }
}
