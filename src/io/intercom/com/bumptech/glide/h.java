package io.intercom.com.bumptech.glide;

import android.widget.ImageView;
import java.io.File;

public class h implements Cloneable {
   protected static final io.intercom.com.bumptech.glide.f.f a;
   protected io.intercom.com.bumptech.glide.f.f b;
   private final e c;
   private final i d;
   private final Class e;
   private final io.intercom.com.bumptech.glide.f.f f;
   private final c g;
   private j h;
   private Object i;
   private io.intercom.com.bumptech.glide.f.e j;
   private h k;
   private Float l;
   private boolean m = true;
   private boolean n;
   private boolean o;

   static {
      a = (new io.intercom.com.bumptech.glide.f.f()).b(io.intercom.com.bumptech.glide.load.engine.h.c).a(g.d).a(true);
   }

   protected h(c var1, i var2, Class var3) {
      this.g = var1;
      this.d = var2;
      this.c = var1.e();
      this.e = var3;
      this.f = var2.f();
      this.h = var2.b(var3);
      this.b = this.f;
   }

   private io.intercom.com.bumptech.glide.f.b a(io.intercom.com.bumptech.glide.f.a.h var1, io.intercom.com.bumptech.glide.f.f var2, io.intercom.com.bumptech.glide.f.c var3, j var4, g var5, int var6, int var7) {
      var2.i();
      return io.intercom.com.bumptech.glide.f.h.a(this.c, this.i, this.e, var2, var6, var7, var5, var1, this.j, var3, this.c.c(), var4.b());
   }

   private io.intercom.com.bumptech.glide.f.b a(io.intercom.com.bumptech.glide.f.a.h var1, io.intercom.com.bumptech.glide.f.i var2, j var3, g var4, int var5, int var6) {
      Object var12;
      if(this.k != null) {
         if(this.o) {
            throw new IllegalStateException("You cannot use a request as both the main request and a thumbnail, consider using clone() on the request(s) passed to thumbnail()");
         }

         j var9 = this.k.h;
         if(this.k.m) {
            var9 = var3;
         }

         g var10;
         if(this.k.b.y()) {
            var10 = this.k.b.z();
         } else {
            var10 = this.a(var4);
         }

         int var8 = this.k.b.A();
         int var7 = this.k.b.C();
         if(io.intercom.com.bumptech.glide.h.i.a(var5, var6) && !this.k.b.B()) {
            var8 = this.b.A();
            var7 = this.b.C();
         }

         var2 = new io.intercom.com.bumptech.glide.f.i(var2);
         io.intercom.com.bumptech.glide.f.b var13 = this.a(var1, this.b, var2, var3, var4, var5, var6);
         this.o = true;
         io.intercom.com.bumptech.glide.f.b var11 = this.k.a(var1, var2, var9, var10, var8, var7);
         this.o = false;
         var2.a(var13, var11);
         var12 = var2;
      } else if(this.l != null) {
         var2 = new io.intercom.com.bumptech.glide.f.i(var2);
         var2.a(this.a(var1, this.b, var2, var3, var4, var5, var6), this.a(var1, this.b.a().a(this.l.floatValue()), var2, var3, this.a(var4), var5, var6));
         var12 = var2;
      } else {
         var12 = this.a(var1, this.b, var2, var3, var4, var5, var6);
      }

      return (io.intercom.com.bumptech.glide.f.b)var12;
   }

   private g a(g var1) {
      switch(null.b[var1.ordinal()]) {
      case 1:
         var1 = g.c;
         break;
      case 2:
         var1 = g.b;
         break;
      case 3:
      case 4:
         var1 = g.a;
         break;
      default:
         throw new IllegalArgumentException("unknown priority: " + this.b.z());
      }

      return var1;
   }

   private io.intercom.com.bumptech.glide.f.b b(io.intercom.com.bumptech.glide.f.a.h var1) {
      return this.a(var1, (io.intercom.com.bumptech.glide.f.i)null, this.h, this.b.z(), this.b.A(), this.b.C());
   }

   private h b(Object var1) {
      this.i = var1;
      this.n = true;
      return this;
   }

   public io.intercom.com.bumptech.glide.f.a.h a(ImageView var1) {
      io.intercom.com.bumptech.glide.h.i.a();
      io.intercom.com.bumptech.glide.h.h.a((Object)var1);
      if(!this.b.c() && this.b.b() && var1.getScaleType() != null) {
         if(this.b.d()) {
            this.b = this.b.a();
         }

         switch(null.a[var1.getScaleType().ordinal()]) {
         case 1:
            this.b.e();
            break;
         case 2:
            this.b.g();
            break;
         case 3:
         case 4:
         case 5:
            this.b.f();
            break;
         case 6:
            this.b.g();
         }
      }

      return this.a(this.c.a(var1, this.e));
   }

   public io.intercom.com.bumptech.glide.f.a.h a(io.intercom.com.bumptech.glide.f.a.h var1) {
      io.intercom.com.bumptech.glide.h.i.a();
      io.intercom.com.bumptech.glide.h.h.a((Object)var1);
      if(!this.n) {
         throw new IllegalArgumentException("You must call #load() before calling #into()");
      } else {
         this.b.i();
         io.intercom.com.bumptech.glide.f.b var3 = this.b(var1);
         io.intercom.com.bumptech.glide.f.b var2 = var1.getRequest();
         if(var3.a(var2) && (((io.intercom.com.bumptech.glide.f.b)io.intercom.com.bumptech.glide.h.h.a((Object)var2)).f() || ((io.intercom.com.bumptech.glide.f.b)io.intercom.com.bumptech.glide.h.h.a((Object)var2)).e())) {
            var3.i();
            if(!((io.intercom.com.bumptech.glide.f.b)io.intercom.com.bumptech.glide.h.h.a((Object)var2)).e()) {
               var2.a();
            }
         } else {
            this.d.a(var1);
            var1.setRequest(var3);
            this.d.a(var1, var3);
         }

         return var1;
      }
   }

   public io.intercom.com.bumptech.glide.f.a a(int var1, int var2) {
      final io.intercom.com.bumptech.glide.f.d var3 = new io.intercom.com.bumptech.glide.f.d(this.c.b(), var1, var2);
      if(io.intercom.com.bumptech.glide.h.i.d()) {
         this.c.b().post(new Runnable() {
            public void run() {
               if(!var3.isCancelled()) {
                  h.this.a((io.intercom.com.bumptech.glide.f.a.h)var3);
               }

            }
         });
      } else {
         this.a((io.intercom.com.bumptech.glide.f.a.h)var3);
      }

      return var3;
   }

   protected io.intercom.com.bumptech.glide.f.f a() {
      io.intercom.com.bumptech.glide.f.f var1;
      if(this.f == this.b) {
         var1 = this.b.a();
      } else {
         var1 = this.b;
      }

      return var1;
   }

   public h a(io.intercom.com.bumptech.glide.f.e var1) {
      this.j = var1;
      return this;
   }

   public h a(io.intercom.com.bumptech.glide.f.f var1) {
      io.intercom.com.bumptech.glide.h.h.a((Object)var1);
      this.b = this.a().a(var1);
      return this;
   }

   public h a(j var1) {
      this.h = (j)io.intercom.com.bumptech.glide.h.h.a((Object)var1);
      this.m = false;
      return this;
   }

   public h a(File var1) {
      return this.b((Object)var1);
   }

   public h a(Object var1) {
      return this.b(var1);
   }

   public h a(String var1) {
      return this.b((Object)var1);
   }

   public h b() {
      try {
         h var1 = (h)super.clone();
         var1.b = var1.b.a();
         var1.h = var1.h.a();
         return var1;
      } catch (CloneNotSupportedException var2) {
         throw new RuntimeException(var2);
      }
   }

   // $FF: synthetic method
   public Object clone() throws CloneNotSupportedException {
      return this.b();
   }
}
