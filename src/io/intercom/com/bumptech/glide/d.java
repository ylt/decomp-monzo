package io.intercom.com.bumptech.glide;

import android.content.Context;
import io.intercom.com.bumptech.glide.c.l;
import io.intercom.com.bumptech.glide.load.engine.a.k;
import java.util.Map;

public final class d {
   private final Map a = new android.support.v4.g.a();
   private io.intercom.com.bumptech.glide.load.engine.i b;
   private io.intercom.com.bumptech.glide.load.engine.a.e c;
   private io.intercom.com.bumptech.glide.load.engine.a.b d;
   private io.intercom.com.bumptech.glide.load.engine.b.h e;
   private io.intercom.com.bumptech.glide.load.engine.c.a f;
   private io.intercom.com.bumptech.glide.load.engine.c.a g;
   private io.intercom.com.bumptech.glide.load.engine.b.a.a h;
   private io.intercom.com.bumptech.glide.load.engine.b.i i;
   private io.intercom.com.bumptech.glide.c.d j;
   private int k = 4;
   private io.intercom.com.bumptech.glide.f.f l = new io.intercom.com.bumptech.glide.f.f();
   private l.a m;

   public c a(Context var1) {
      if(this.f == null) {
         this.f = io.intercom.com.bumptech.glide.load.engine.c.a.b();
      }

      if(this.g == null) {
         this.g = io.intercom.com.bumptech.glide.load.engine.c.a.a();
      }

      if(this.i == null) {
         this.i = (new io.intercom.com.bumptech.glide.load.engine.b.i.a(var1)).a();
      }

      if(this.j == null) {
         this.j = new io.intercom.com.bumptech.glide.c.f();
      }

      if(this.c == null) {
         int var2 = this.i.b();
         if(var2 > 0) {
            this.c = new k(var2);
         } else {
            this.c = new io.intercom.com.bumptech.glide.load.engine.a.f();
         }
      }

      if(this.d == null) {
         this.d = new io.intercom.com.bumptech.glide.load.engine.a.j(this.i.c());
      }

      if(this.e == null) {
         this.e = new io.intercom.com.bumptech.glide.load.engine.b.g(this.i.a());
      }

      if(this.h == null) {
         this.h = new io.intercom.com.bumptech.glide.load.engine.b.f(var1);
      }

      if(this.b == null) {
         this.b = new io.intercom.com.bumptech.glide.load.engine.i(this.e, this.h, this.g, this.f, io.intercom.com.bumptech.glide.load.engine.c.a.c());
      }

      l var3 = new l(this.m);
      return new c(var1, this.b, this.e, this.c, this.d, var3, this.j, this.k, this.l.i(), this.a);
   }

   d a(l.a var1) {
      this.m = var1;
      return this;
   }
}
