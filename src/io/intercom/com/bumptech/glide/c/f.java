package io.intercom.com.bumptech.glide.c;

import android.content.Context;

public class f implements d {
   public c a(Context var1, c.a var2) {
      boolean var3;
      if(android.support.v4.content.a.b(var1, "android.permission.ACCESS_NETWORK_STATE") == 0) {
         var3 = true;
      } else {
         var3 = false;
      }

      Object var4;
      if(var3) {
         var4 = new e(var1, var2);
      } else {
         var4 = new j();
      }

      return (c)var4;
   }
}
