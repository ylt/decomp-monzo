package io.intercom.com.bumptech.glide.c;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.WeakHashMap;

public class n {
   private final Set a = Collections.newSetFromMap(new WeakHashMap());
   private final List b = new ArrayList();
   private boolean c;

   public void a() {
      this.c = true;
      Iterator var1 = io.intercom.com.bumptech.glide.h.i.a((Collection)this.a).iterator();

      while(var1.hasNext()) {
         io.intercom.com.bumptech.glide.f.b var2 = (io.intercom.com.bumptech.glide.f.b)var1.next();
         if(var2.e()) {
            var2.b();
            this.b.add(var2);
         }
      }

   }

   public void a(io.intercom.com.bumptech.glide.f.b var1) {
      this.a.add(var1);
      if(!this.c) {
         var1.a();
      } else {
         this.b.add(var1);
      }

   }

   public void b() {
      this.c = false;
      Iterator var1 = io.intercom.com.bumptech.glide.h.i.a((Collection)this.a).iterator();

      while(var1.hasNext()) {
         io.intercom.com.bumptech.glide.f.b var2 = (io.intercom.com.bumptech.glide.f.b)var1.next();
         if(!var2.f() && !var2.h() && !var2.e()) {
            var2.a();
         }
      }

      this.b.clear();
   }

   public boolean b(io.intercom.com.bumptech.glide.f.b var1) {
      boolean var2 = false;
      boolean var3 = false;
      if(var1 != null) {
         var3 = this.a.remove(var1);
         if(this.b.remove(var1) || var3) {
            var2 = true;
         }

         var3 = var2;
         if(var2) {
            var1.c();
            var1.i();
            var3 = var2;
         }
      }

      return var3;
   }

   public void c() {
      Iterator var1 = io.intercom.com.bumptech.glide.h.i.a((Collection)this.a).iterator();

      while(var1.hasNext()) {
         this.b((io.intercom.com.bumptech.glide.f.b)var1.next());
      }

      this.b.clear();
   }

   public void d() {
      Iterator var1 = io.intercom.com.bumptech.glide.h.i.a((Collection)this.a).iterator();

      while(var1.hasNext()) {
         io.intercom.com.bumptech.glide.f.b var2 = (io.intercom.com.bumptech.glide.f.b)var1.next();
         if(!var2.f() && !var2.h()) {
            var2.b();
            if(!this.c) {
               var2.a();
            } else {
               this.b.add(var2);
            }
         }
      }

   }

   public String toString() {
      return super.toString() + "{numRequests=" + this.a.size() + ", isPaused=" + this.c + "}";
   }
}
