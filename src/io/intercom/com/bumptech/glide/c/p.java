package io.intercom.com.bumptech.glide.c;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.WeakHashMap;

public final class p implements i {
   private final Set a = Collections.newSetFromMap(new WeakHashMap());

   public List a() {
      return new ArrayList(this.a);
   }

   public void a(io.intercom.com.bumptech.glide.f.a.h var1) {
      this.a.add(var1);
   }

   public void b() {
      this.a.clear();
   }

   public void b(io.intercom.com.bumptech.glide.f.a.h var1) {
      this.a.remove(var1);
   }

   public void onDestroy() {
      Iterator var1 = io.intercom.com.bumptech.glide.h.i.a((Collection)this.a).iterator();

      while(var1.hasNext()) {
         ((io.intercom.com.bumptech.glide.f.a.h)var1.next()).onDestroy();
      }

   }

   public void onStart() {
      Iterator var1 = io.intercom.com.bumptech.glide.h.i.a((Collection)this.a).iterator();

      while(var1.hasNext()) {
         ((io.intercom.com.bumptech.glide.f.a.h)var1.next()).onStart();
      }

   }

   public void onStop() {
      Iterator var1 = io.intercom.com.bumptech.glide.h.i.a((Collection)this.a).iterator();

      while(var1.hasNext()) {
         ((io.intercom.com.bumptech.glide.f.a.h)var1.next()).onStop();
      }

   }
}
