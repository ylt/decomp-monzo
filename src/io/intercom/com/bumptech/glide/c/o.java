package io.intercom.com.bumptech.glide.c;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.util.Log;
import java.util.HashSet;

public class o extends Fragment {
   private final a a;
   private final m b;
   private final HashSet c;
   private o d;
   private io.intercom.com.bumptech.glide.i e;
   private Fragment f;

   public o() {
      this(new a());
   }

   @SuppressLint({"ValidFragment"})
   public o(a var1) {
      this.b = new o.a();
      this.c = new HashSet();
      this.a = var1;
   }

   private void a(android.support.v4.app.j var1) {
      this.e();
      this.d = io.intercom.com.bumptech.glide.c.a((Context)var1).g().a((android.support.v4.app.n)var1.getSupportFragmentManager(), (Fragment)null);
      if(this.d != this) {
         this.d.a(this);
      }

   }

   private void a(o var1) {
      this.c.add(var1);
   }

   private void b(o var1) {
      this.c.remove(var1);
   }

   private Fragment d() {
      Fragment var1 = this.getParentFragment();
      if(var1 == null) {
         var1 = this.f;
      }

      return var1;
   }

   private void e() {
      if(this.d != null) {
         this.d.b(this);
         this.d = null;
      }

   }

   a a() {
      return this.a;
   }

   void a(Fragment var1) {
      this.f = var1;
      if(var1 != null && var1.getActivity() != null) {
         this.a(var1.getActivity());
      }

   }

   public void a(io.intercom.com.bumptech.glide.i var1) {
      this.e = var1;
   }

   public io.intercom.com.bumptech.glide.i b() {
      return this.e;
   }

   public m c() {
      return this.b;
   }

   public void onAttach(Context var1) {
      super.onAttach(var1);

      try {
         this.a(this.getActivity());
      } catch (IllegalStateException var2) {
         if(Log.isLoggable("SupportRMFragment", 5)) {
            Log.w("SupportRMFragment", "Unable to register fragment with root", var2);
         }
      }

   }

   public void onDestroy() {
      super.onDestroy();
      this.a.c();
      this.e();
   }

   public void onDetach() {
      super.onDetach();
      this.f = null;
      this.e();
   }

   public void onStart() {
      super.onStart();
      this.a.a();
   }

   public void onStop() {
      super.onStop();
      this.a.b();
   }

   public String toString() {
      return super.toString() + "{parent=" + this.d() + "}";
   }

   private class a implements m {
      public String toString() {
         return super.toString() + "{fragment=" + o.this + "}";
      }
   }
}
