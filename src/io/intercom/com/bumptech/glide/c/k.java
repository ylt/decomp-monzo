package io.intercom.com.bumptech.glide.c;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.os.Build.VERSION;
import android.util.Log;
import java.util.HashSet;

public class k extends Fragment {
   private final a a;
   private final m b;
   private final HashSet c;
   private io.intercom.com.bumptech.glide.i d;
   private k e;
   private Fragment f;

   public k() {
      this(new a());
   }

   @SuppressLint({"ValidFragment"})
   k(a var1) {
      this.b = new k.a();
      this.c = new HashSet();
      this.a = var1;
   }

   private void a(Activity var1) {
      this.e();
      this.e = io.intercom.com.bumptech.glide.c.a((Context)var1).g().a((FragmentManager)var1.getFragmentManager(), (Fragment)null);
      if(this.e != this) {
         this.e.a(this);
      }

   }

   private void a(k var1) {
      this.c.add(var1);
   }

   private void b(k var1) {
      this.c.remove(var1);
   }

   @TargetApi(17)
   private Fragment d() {
      Fragment var1;
      if(VERSION.SDK_INT >= 17) {
         var1 = this.getParentFragment();
      } else {
         var1 = null;
      }

      if(var1 == null) {
         var1 = this.f;
      }

      return var1;
   }

   private void e() {
      if(this.e != null) {
         this.e.b(this);
         this.e = null;
      }

   }

   a a() {
      return this.a;
   }

   void a(Fragment var1) {
      this.f = var1;
      if(var1 != null && var1.getActivity() != null) {
         this.a(var1.getActivity());
      }

   }

   public void a(io.intercom.com.bumptech.glide.i var1) {
      this.d = var1;
   }

   public io.intercom.com.bumptech.glide.i b() {
      return this.d;
   }

   public m c() {
      return this.b;
   }

   public void onAttach(Activity var1) {
      super.onAttach(var1);

      try {
         this.a(var1);
      } catch (IllegalStateException var2) {
         if(Log.isLoggable("RMFragment", 5)) {
            Log.w("RMFragment", "Unable to register fragment with root", var2);
         }
      }

   }

   public void onDestroy() {
      super.onDestroy();
      this.a.c();
      this.e();
   }

   public void onDetach() {
      super.onDetach();
      this.e();
   }

   public void onStart() {
      super.onStart();
      this.a.a();
   }

   public void onStop() {
      super.onStop();
      this.a.b();
   }

   public String toString() {
      return super.toString() + "{parent=" + this.d() + "}";
   }

   private class a implements m {
      public String toString() {
         return super.toString() + "{fragment=" + k.this + "}";
      }
   }
}
