package io.intercom.com.bumptech.glide.c;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

class e implements c {
   final c.a a;
   boolean b;
   private final Context c;
   private boolean d;
   private final BroadcastReceiver e = new BroadcastReceiver() {
      public void onReceive(Context var1, Intent var2) {
         boolean var3 = e.this.b;
         e.this.b = e.this.a(var1);
         if(var3 != e.this.b) {
            e.this.a.a(e.this.b);
         }

      }
   };

   public e(Context var1, c.a var2) {
      this.c = var1.getApplicationContext();
      this.a = var2;
   }

   private void a() {
      if(!this.d) {
         this.b = this.a(this.c);
         this.c.registerReceiver(this.e, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
         this.d = true;
      }

   }

   private void b() {
      if(this.d) {
         this.c.unregisterReceiver(this.e);
         this.d = false;
      }

   }

   boolean a(Context var1) {
      NetworkInfo var3 = ((ConnectivityManager)var1.getSystemService("connectivity")).getActiveNetworkInfo();
      boolean var2;
      if(var3 != null && var3.isConnected()) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   public void onDestroy() {
   }

   public void onStart() {
      this.a();
   }

   public void onStop() {
      this.b();
   }
}
