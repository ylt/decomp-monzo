package io.intercom.com.bumptech.glide.c;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Build.VERSION;
import android.os.Handler.Callback;
import android.util.Log;
import java.util.HashMap;
import java.util.Map;

public class l implements Callback {
   private static final l.a i = new l.a() {
      public io.intercom.com.bumptech.glide.i a(io.intercom.com.bumptech.glide.c var1, h var2, m var3) {
         return new io.intercom.com.bumptech.glide.i(var1, var2, var3);
      }
   };
   final Map a = new HashMap();
   final Map b = new HashMap();
   private volatile io.intercom.com.bumptech.glide.i c;
   private final Handler d;
   private final l.a e;
   private final android.support.v4.g.a f = new android.support.v4.g.a();
   private final android.support.v4.g.a g = new android.support.v4.g.a();
   private final Bundle h = new Bundle();

   public l(l.a var1) {
      if(var1 == null) {
         var1 = i;
      }

      this.e = var1;
      this.d = new Handler(Looper.getMainLooper(), this);
   }

   private io.intercom.com.bumptech.glide.i a(Context var1, FragmentManager var2, Fragment var3) {
      k var4 = this.a(var2, var3);
      io.intercom.com.bumptech.glide.i var7 = var4.b();
      io.intercom.com.bumptech.glide.i var6 = var7;
      if(var7 == null) {
         io.intercom.com.bumptech.glide.c var5 = io.intercom.com.bumptech.glide.c.a(var1);
         var6 = this.e.a(var5, var4.a(), var4.c());
         var4.a(var6);
      }

      return var6;
   }

   private io.intercom.com.bumptech.glide.i a(Context var1, android.support.v4.app.n var2, android.support.v4.app.Fragment var3) {
      o var4 = this.a(var2, var3);
      io.intercom.com.bumptech.glide.i var7 = var4.b();
      io.intercom.com.bumptech.glide.i var6 = var7;
      if(var7 == null) {
         io.intercom.com.bumptech.glide.c var5 = io.intercom.com.bumptech.glide.c.a(var1);
         var6 = this.e.a(var5, var4.a(), var4.c());
         var4.a(var6);
      }

      return var6;
   }

   private io.intercom.com.bumptech.glide.i b(Context param1) {
      // $FF: Couldn't be decompiled
   }

   @TargetApi(17)
   private static void b(Activity var0) {
      if(VERSION.SDK_INT >= 17 && var0.isDestroyed()) {
         throw new IllegalArgumentException("You cannot start a load for a destroyed activity");
      }
   }

   @TargetApi(17)
   k a(FragmentManager var1, Fragment var2) {
      k var4 = (k)var1.findFragmentByTag("io.intercom.com.bumptech.glide.manager");
      k var3 = var4;
      if(var4 == null) {
         var4 = (k)this.a.get(var1);
         var3 = var4;
         if(var4 == null) {
            var3 = new k();
            var3.a(var2);
            this.a.put(var1, var3);
            var1.beginTransaction().add(var3, "io.intercom.com.bumptech.glide.manager").commitAllowingStateLoss();
            this.d.obtainMessage(1, var1).sendToTarget();
         }
      }

      return var3;
   }

   o a(android.support.v4.app.n var1, android.support.v4.app.Fragment var2) {
      o var4 = (o)var1.a("io.intercom.com.bumptech.glide.manager");
      o var3 = var4;
      if(var4 == null) {
         var4 = (o)this.b.get(var1);
         var3 = var4;
         if(var4 == null) {
            var3 = new o();
            var3.a(var2);
            this.b.put(var1, var3);
            var1.a().a(var3, "io.intercom.com.bumptech.glide.manager").d();
            this.d.obtainMessage(2, var1).sendToTarget();
         }
      }

      return var3;
   }

   public io.intercom.com.bumptech.glide.i a(Activity var1) {
      io.intercom.com.bumptech.glide.i var2;
      if(io.intercom.com.bumptech.glide.h.i.d()) {
         var2 = this.a(var1.getApplicationContext());
      } else {
         b(var1);
         var2 = this.a(var1, (FragmentManager)var1.getFragmentManager(), (Fragment)null);
      }

      return var2;
   }

   public io.intercom.com.bumptech.glide.i a(Context var1) {
      if(var1 == null) {
         throw new IllegalArgumentException("You cannot start a load on a null Context");
      } else {
         io.intercom.com.bumptech.glide.i var2;
         if(io.intercom.com.bumptech.glide.h.i.c() && !(var1 instanceof Application)) {
            if(var1 instanceof android.support.v4.app.j) {
               var2 = this.a((android.support.v4.app.j)var1);
               return var2;
            }

            if(var1 instanceof Activity) {
               var2 = this.a((Activity)var1);
               return var2;
            }

            if(var1 instanceof ContextWrapper) {
               var2 = this.a(((ContextWrapper)var1).getBaseContext());
               return var2;
            }
         }

         var2 = this.b(var1);
         return var2;
      }
   }

   public io.intercom.com.bumptech.glide.i a(android.support.v4.app.Fragment var1) {
      io.intercom.com.bumptech.glide.h.h.a(var1.getActivity(), "You cannot start a load on a fragment before it is attached or after it is destroyed");
      io.intercom.com.bumptech.glide.i var3;
      if(io.intercom.com.bumptech.glide.h.i.d()) {
         var3 = this.a(var1.getActivity().getApplicationContext());
      } else {
         android.support.v4.app.n var2 = var1.getChildFragmentManager();
         var3 = this.a(var1.getActivity(), (android.support.v4.app.n)var2, (android.support.v4.app.Fragment)var1);
      }

      return var3;
   }

   public io.intercom.com.bumptech.glide.i a(android.support.v4.app.j var1) {
      io.intercom.com.bumptech.glide.i var2;
      if(io.intercom.com.bumptech.glide.h.i.d()) {
         var2 = this.a(var1.getApplicationContext());
      } else {
         b((Activity)var1);
         var2 = this.a(var1, (android.support.v4.app.n)var1.getSupportFragmentManager(), (android.support.v4.app.Fragment)null);
      }

      return var2;
   }

   public boolean handleMessage(Message var1) {
      Object var4 = null;
      boolean var2 = true;
      Object var3;
      Object var5;
      switch(var1.what) {
      case 1:
         var5 = (FragmentManager)var1.obj;
         var3 = this.a.remove(var5);
         break;
      case 2:
         var5 = (android.support.v4.app.n)var1.obj;
         var3 = this.b.remove(var5);
         break;
      default:
         var2 = false;
         var3 = null;
         var5 = var4;
      }

      if(var2 && var3 == null && Log.isLoggable("RMRetriever", 5)) {
         Log.w("RMRetriever", "Failed to remove expected request manager fragment, manager: " + var5);
      }

      return var2;
   }

   public interface a {
      io.intercom.com.bumptech.glide.i a(io.intercom.com.bumptech.glide.c var1, h var2, m var3);
   }
}
