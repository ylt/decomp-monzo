package io.intercom.com.bumptech.glide.c;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Set;
import java.util.WeakHashMap;

class a implements h {
   private final Set a = Collections.newSetFromMap(new WeakHashMap());
   private boolean b;
   private boolean c;

   void a() {
      this.b = true;
      Iterator var1 = io.intercom.com.bumptech.glide.h.i.a((Collection)this.a).iterator();

      while(var1.hasNext()) {
         ((i)var1.next()).onStart();
      }

   }

   public void a(i var1) {
      this.a.add(var1);
      if(this.c) {
         var1.onDestroy();
      } else if(this.b) {
         var1.onStart();
      } else {
         var1.onStop();
      }

   }

   void b() {
      this.b = false;
      Iterator var1 = io.intercom.com.bumptech.glide.h.i.a((Collection)this.a).iterator();

      while(var1.hasNext()) {
         ((i)var1.next()).onStop();
      }

   }

   public void b(i var1) {
      this.a.remove(var1);
   }

   void c() {
      this.c = true;
      Iterator var1 = io.intercom.com.bumptech.glide.h.i.a((Collection)this.a).iterator();

      while(var1.hasNext()) {
         ((i)var1.next()).onDestroy();
      }

   }
}
