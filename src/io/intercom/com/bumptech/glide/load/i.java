package io.intercom.com.bumptech.glide.load;

import java.security.MessageDigest;
import java.util.Iterator;
import java.util.Map.Entry;

public final class i implements g {
   private final android.support.v4.g.a b = new android.support.v4.g.a();

   private static void a(h var0, Object var1, MessageDigest var2) {
      var0.a(var1, var2);
   }

   public i a(h var1, Object var2) {
      this.b.put(var1, var2);
      return this;
   }

   public Object a(h var1) {
      Object var2;
      if(this.b.containsKey(var1)) {
         var2 = this.b.get(var1);
      } else {
         var2 = var1.a();
      }

      return var2;
   }

   public void a(i var1) {
      this.b.a(var1.b);
   }

   public boolean equals(Object var1) {
      boolean var2;
      if(var1 instanceof i) {
         i var3 = (i)var1;
         var2 = this.b.equals(var3.b);
      } else {
         var2 = false;
      }

      return var2;
   }

   public int hashCode() {
      return this.b.hashCode();
   }

   public String toString() {
      return "Options{values=" + this.b + '}';
   }

   public void updateDiskCacheKey(MessageDigest var1) {
      Iterator var3 = this.b.entrySet().iterator();

      while(var3.hasNext()) {
         Entry var2 = (Entry)var3.next();
         a((h)var2.getKey(), var2.getValue(), var1);
      }

   }
}
