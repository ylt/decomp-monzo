package io.intercom.com.bumptech.glide.load.a;

import io.intercom.com.bumptech.glide.load.resource.bitmap.RecyclableBufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

public final class i implements c {
   private final RecyclableBufferedInputStream a;

   i(InputStream var1, io.intercom.com.bumptech.glide.load.engine.a.b var2) {
      this.a = new RecyclableBufferedInputStream(var1, var2);
      this.a.mark(5242880);
   }

   // $FF: synthetic method
   public Object a() throws IOException {
      return this.c();
   }

   public void b() {
      this.a.b();
   }

   public InputStream c() throws IOException {
      this.a.reset();
      return this.a;
   }

   public static final class a implements c.a {
      private final io.intercom.com.bumptech.glide.load.engine.a.b a;

      public a(io.intercom.com.bumptech.glide.load.engine.a.b var1) {
         this.a = var1;
      }

      public c a(InputStream var1) {
         return new i(var1, this.a);
      }

      public Class a() {
         return InputStream.class;
      }
   }
}
