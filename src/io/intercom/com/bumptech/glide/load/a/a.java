package io.intercom.com.bumptech.glide.load.a;

import android.content.res.AssetManager;
import android.util.Log;
import java.io.IOException;

public abstract class a implements b {
   private final String a;
   private final AssetManager b;
   private Object c;

   public a(AssetManager var1, String var2) {
      this.b = var1;
      this.a = var2;
   }

   protected abstract Object a(AssetManager var1, String var2) throws IOException;

   public void a() {
      if(this.c != null) {
         try {
            this.a(this.c);
         } catch (IOException var2) {
            ;
         }
      }

   }

   public void a(io.intercom.com.bumptech.glide.g var1, b.a var2) {
      try {
         this.c = this.a(this.b, this.a);
      } catch (IOException var3) {
         if(Log.isLoggable("AssetPathFetcher", 3)) {
            Log.d("AssetPathFetcher", "Failed to load data from asset manager", var3);
         }

         var2.a((Exception)var3);
         return;
      }

      var2.a(this.c);
   }

   protected abstract void a(Object var1) throws IOException;

   public void b() {
   }

   public io.intercom.com.bumptech.glide.load.a c() {
      return io.intercom.com.bumptech.glide.load.a.a;
   }
}
