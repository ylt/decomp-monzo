package io.intercom.com.bumptech.glide.load.a;

import android.content.ContentResolver;
import android.content.res.AssetFileDescriptor;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import java.io.FileNotFoundException;
import java.io.IOException;

public class g extends j {
   public g(ContentResolver var1, Uri var2) {
      super(var1, var2);
   }

   protected ParcelFileDescriptor a(Uri var1, ContentResolver var2) throws FileNotFoundException {
      AssetFileDescriptor var3 = var2.openAssetFileDescriptor(var1, "r");
      if(var3 == null) {
         throw new FileNotFoundException("FileDescriptor is null for: " + var1);
      } else {
         return var3.getParcelFileDescriptor();
      }
   }

   protected void a(ParcelFileDescriptor var1) throws IOException {
      var1.close();
   }

   // $FF: synthetic method
   protected Object b(Uri var1, ContentResolver var2) throws FileNotFoundException {
      return this.a(var1, var2);
   }

   public Class d() {
      return ParcelFileDescriptor.class;
   }
}
