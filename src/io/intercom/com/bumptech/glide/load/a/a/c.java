package io.intercom.com.bumptech.glide.load.a.a;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore.Images.Thumbnails;
import android.util.Log;
import io.intercom.com.bumptech.glide.g;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class c implements io.intercom.com.bumptech.glide.load.a.b {
   private final Uri a;
   private final e b;
   private InputStream c;

   c(Uri var1, e var2) {
      this.a = var1;
      this.b = var2;
   }

   public static c a(Context var0, Uri var1) {
      return a(var0, var1, new c.a(var0.getContentResolver()));
   }

   private static c a(Context var0, Uri var1, d var2) {
      io.intercom.com.bumptech.glide.load.engine.a.b var3 = io.intercom.com.bumptech.glide.c.a(var0).b();
      return new c(var1, new e(io.intercom.com.bumptech.glide.c.a(var0).h().a(), var2, var3, var0.getContentResolver()));
   }

   public static c b(Context var0, Uri var1) {
      return a(var0, var1, new c.b(var0.getContentResolver()));
   }

   private InputStream e() throws FileNotFoundException {
      Object var2 = this.b.b(this.a);
      int var1;
      if(var2 != null) {
         var1 = this.b.a(this.a);
      } else {
         var1 = -1;
      }

      if(var1 != -1) {
         var2 = new io.intercom.com.bumptech.glide.load.a.e((InputStream)var2, var1);
      }

      return (InputStream)var2;
   }

   public void a() {
      if(this.c != null) {
         try {
            this.c.close();
         } catch (IOException var2) {
            ;
         }
      }

   }

   public void a(g var1, io.intercom.com.bumptech.glide.load.a.b.a var2) {
      try {
         this.c = this.e();
      } catch (FileNotFoundException var3) {
         if(Log.isLoggable("MediaStoreThumbFetcher", 3)) {
            Log.d("MediaStoreThumbFetcher", "Failed to find thumbnail file", var3);
         }

         var2.a((Exception)var3);
         return;
      }

      var2.a((Object)this.c);
   }

   public void b() {
   }

   public io.intercom.com.bumptech.glide.load.a c() {
      return io.intercom.com.bumptech.glide.load.a.a;
   }

   public Class d() {
      return InputStream.class;
   }

   static class a implements d {
      private static final String[] b = new String[]{"_data"};
      private final ContentResolver a;

      a(ContentResolver var1) {
         this.a = var1;
      }

      public Cursor a(Uri var1) {
         String var2 = var1.getLastPathSegment();
         return this.a.query(Thumbnails.EXTERNAL_CONTENT_URI, b, "kind = 1 AND image_id = ?", new String[]{var2}, (String)null);
      }
   }

   static class b implements d {
      private static final String[] b = new String[]{"_data"};
      private final ContentResolver a;

      b(ContentResolver var1) {
         this.a = var1;
      }

      public Cursor a(Uri var1) {
         String var2 = var1.getLastPathSegment();
         return this.a.query(android.provider.MediaStore.Video.Thumbnails.EXTERNAL_CONTENT_URI, b, "kind = 1 AND video_id = ?", new String[]{var2}, (String)null);
      }
   }
}
