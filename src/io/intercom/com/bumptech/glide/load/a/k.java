package io.intercom.com.bumptech.glide.load.a;

import android.content.res.AssetManager;
import java.io.IOException;
import java.io.InputStream;

public class k extends a {
   public k(AssetManager var1, String var2) {
      super(var1, var2);
   }

   // $FF: synthetic method
   protected Object a(AssetManager var1, String var2) throws IOException {
      return this.b(var1, var2);
   }

   protected void a(InputStream var1) throws IOException {
      var1.close();
   }

   protected InputStream b(AssetManager var1, String var2) throws IOException {
      return var1.open(var2);
   }

   public Class d() {
      return InputStream.class;
   }
}
