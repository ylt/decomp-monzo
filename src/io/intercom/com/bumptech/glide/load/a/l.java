package io.intercom.com.bumptech.glide.load.a;

import android.annotation.TargetApi;
import android.content.ContentResolver;
import android.content.UriMatcher;
import android.net.Uri;
import android.provider.ContactsContract.Contacts;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class l extends j {
   private static final UriMatcher a = new UriMatcher(-1);

   static {
      a.addURI("com.android.contacts", "contacts/lookup/*/#", 1);
      a.addURI("com.android.contacts", "contacts/lookup/*", 1);
      a.addURI("com.android.contacts", "contacts/#/photo", 2);
      a.addURI("com.android.contacts", "contacts/#", 3);
      a.addURI("com.android.contacts", "contacts/#/display_photo", 4);
      a.addURI("com.android.contacts", "phone_lookup/*", 5);
   }

   public l(ContentResolver var1, Uri var2) {
      super(var1, var2);
   }

   @TargetApi(14)
   private InputStream a(ContentResolver var1, Uri var2) {
      return Contacts.openContactPhotoInputStream(var1, var2, true);
   }

   private InputStream c(Uri var1, ContentResolver var2) throws FileNotFoundException {
      InputStream var3;
      switch(a.match(var1)) {
      case 1:
      case 5:
         var1 = Contacts.lookupContact(var2, var1);
         if(var1 == null) {
            throw new FileNotFoundException("Contact cannot be found");
         }

         var3 = this.a(var2, var1);
         break;
      case 2:
      case 4:
      default:
         var3 = var2.openInputStream(var1);
         break;
      case 3:
         var3 = this.a(var2, var1);
      }

      return var3;
   }

   protected InputStream a(Uri var1, ContentResolver var2) throws FileNotFoundException {
      InputStream var3 = this.c(var1, var2);
      if(var3 == null) {
         throw new FileNotFoundException("InputStream is null for " + var1);
      } else {
         return var3;
      }
   }

   protected void a(InputStream var1) throws IOException {
      var1.close();
   }

   // $FF: synthetic method
   protected Object b(Uri var1, ContentResolver var2) throws FileNotFoundException {
      return this.a(var1, var2);
   }

   public Class d() {
      return InputStream.class;
   }
}
