package io.intercom.com.bumptech.glide.load.a;

import android.content.ContentResolver;
import android.net.Uri;
import android.util.Log;
import java.io.FileNotFoundException;
import java.io.IOException;

public abstract class j implements b {
   private final Uri a;
   private final ContentResolver b;
   private Object c;

   public j(ContentResolver var1, Uri var2) {
      this.b = var1;
      this.a = var2;
   }

   public void a() {
      if(this.c != null) {
         try {
            this.a(this.c);
         } catch (IOException var2) {
            ;
         }
      }

   }

   public final void a(io.intercom.com.bumptech.glide.g var1, b.a var2) {
      try {
         this.c = this.b(this.a, this.b);
      } catch (FileNotFoundException var3) {
         if(Log.isLoggable("LocalUriFetcher", 3)) {
            Log.d("LocalUriFetcher", "Failed to open Uri", var3);
         }

         var2.a((Exception)var3);
         return;
      }

      var2.a(this.c);
   }

   protected abstract void a(Object var1) throws IOException;

   protected abstract Object b(Uri var1, ContentResolver var2) throws FileNotFoundException;

   public void b() {
   }

   public io.intercom.com.bumptech.glide.load.a c() {
      return io.intercom.com.bumptech.glide.load.a.a;
   }
}
