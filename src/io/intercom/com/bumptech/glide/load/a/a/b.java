package io.intercom.com.bumptech.glide.load.a.a;

import android.net.Uri;

public final class b {
   public static boolean a(int var0, int var1) {
      boolean var2;
      if(var0 <= 512 && var1 <= 384) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   public static boolean a(Uri var0) {
      boolean var1;
      if(var0 != null && "content".equals(var0.getScheme()) && "media".equals(var0.getAuthority())) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public static boolean b(Uri var0) {
      boolean var1;
      if(a(var0) && d(var0)) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public static boolean c(Uri var0) {
      boolean var1;
      if(a(var0) && !d(var0)) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   private static boolean d(Uri var0) {
      return var0.getPathSegments().contains("video");
   }
}
