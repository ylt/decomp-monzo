package io.intercom.com.bumptech.glide.load.a;

import android.text.TextUtils;
import android.util.Log;
import io.intercom.com.bumptech.glide.load.HttpException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class h implements b {
   static final h.b a = new h.a();
   private final io.intercom.com.bumptech.glide.load.b.g b;
   private final int c;
   private final h.b d;
   private HttpURLConnection e;
   private InputStream f;
   private volatile boolean g;

   public h(io.intercom.com.bumptech.glide.load.b.g var1, int var2) {
      this(var1, var2, a);
   }

   h(io.intercom.com.bumptech.glide.load.b.g var1, int var2, h.b var3) {
      this.b = var1;
      this.c = var2;
      this.d = var3;
   }

   private InputStream a(HttpURLConnection var1) throws IOException {
      if(TextUtils.isEmpty(var1.getContentEncoding())) {
         int var2 = var1.getContentLength();
         this.f = io.intercom.com.bumptech.glide.h.b.a(var1.getInputStream(), (long)var2);
      } else {
         if(Log.isLoggable("HttpUrlFetcher", 3)) {
            Log.d("HttpUrlFetcher", "Got non empty content encoding: " + var1.getContentEncoding());
         }

         this.f = var1.getInputStream();
      }

      return this.f;
   }

   private InputStream a(URL var1, int var2, URL var3, Map var4) throws IOException {
      if(var2 >= 5) {
         throw new HttpException("Too many (> 5) redirects!");
      } else {
         if(var3 != null) {
            try {
               if(var1.toURI().equals(var3.toURI())) {
                  HttpException var11 = new HttpException("In re-direct loop");
                  throw var11;
               }
            } catch (URISyntaxException var7) {
               ;
            }
         }

         this.e = this.d.a(var1);
         Iterator var9 = var4.entrySet().iterator();

         while(var9.hasNext()) {
            Entry var6 = (Entry)var9.next();
            this.e.addRequestProperty((String)var6.getKey(), (String)var6.getValue());
         }

         this.e.setConnectTimeout(this.c);
         this.e.setReadTimeout(this.c);
         this.e.setUseCaches(false);
         this.e.setDoInput(true);
         this.e.setInstanceFollowRedirects(false);
         this.e.connect();
         InputStream var8;
         if(this.g) {
            var8 = null;
         } else {
            int var5 = this.e.getResponseCode();
            if(var5 / 100 == 2) {
               var8 = this.a(this.e);
            } else {
               if(var5 / 100 != 3) {
                  if(var5 == -1) {
                     throw new HttpException(var5);
                  }

                  throw new HttpException(this.e.getResponseMessage(), var5);
               }

               String var10 = this.e.getHeaderField("Location");
               if(TextUtils.isEmpty(var10)) {
                  throw new HttpException("Received empty or null redirect url");
               }

               var8 = this.a(new URL(var1, var10), var2 + 1, var1, var4);
            }
         }

         return var8;
      }
   }

   public void a() {
      if(this.f != null) {
         try {
            this.f.close();
         } catch (IOException var2) {
            ;
         }
      }

      if(this.e != null) {
         this.e.disconnect();
      }

   }

   public void a(io.intercom.com.bumptech.glide.g var1, b.a var2) {
      long var3 = io.intercom.com.bumptech.glide.h.d.a();

      InputStream var6;
      try {
         var6 = this.a(this.b.a(), 0, (URL)null, this.b.b());
      } catch (IOException var5) {
         if(Log.isLoggable("HttpUrlFetcher", 3)) {
            Log.d("HttpUrlFetcher", "Failed to load data for url", var5);
         }

         var2.a((Exception)var5);
         return;
      }

      if(Log.isLoggable("HttpUrlFetcher", 2)) {
         Log.v("HttpUrlFetcher", "Finished http url fetcher fetch in " + io.intercom.com.bumptech.glide.h.d.a(var3) + " ms and loaded " + var6);
      }

      var2.a((Object)var6);
   }

   public void b() {
      this.g = true;
   }

   public io.intercom.com.bumptech.glide.load.a c() {
      return io.intercom.com.bumptech.glide.load.a.b;
   }

   public Class d() {
      return InputStream.class;
   }

   private static class a implements h.b {
      public HttpURLConnection a(URL var1) throws IOException {
         return (HttpURLConnection)var1.openConnection();
      }
   }

   interface b {
      HttpURLConnection a(URL var1) throws IOException;
   }
}
