package io.intercom.com.bumptech.glide.load.a;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public final class e extends FilterInputStream {
   private static final byte[] a = new byte[]{-1, -31, 0, 28, 69, 120, 105, 102, 0, 0, 77, 77, 0, 0, 0, 0, 0, 8, 0, 1, 1, 18, 0, 2, 0, 0, 0, 1, 0};
   private static final int b;
   private static final int c;
   private final byte d;
   private int e;

   static {
      b = a.length;
      c = b + 2;
   }

   public e(InputStream var1, int var2) {
      super(var1);
      if(var2 >= -1 && var2 <= 8) {
         this.d = (byte)var2;
      } else {
         throw new IllegalArgumentException("Cannot add invalid orientation: " + var2);
      }
   }

   public void mark(int var1) {
      throw new UnsupportedOperationException();
   }

   public boolean markSupported() {
      return false;
   }

   public int read() throws IOException {
      int var1;
      if(this.e >= 2 && this.e <= c) {
         if(this.e == c) {
            var1 = this.d;
         } else {
            var1 = a[this.e - 2] & 255;
         }
      } else {
         var1 = super.read();
      }

      if(var1 != -1) {
         ++this.e;
      }

      return var1;
   }

   public int read(byte[] var1, int var2, int var3) throws IOException {
      if(this.e > c) {
         var2 = super.read(var1, var2, var3);
      } else if(this.e == c) {
         var1[var2] = this.d;
         var2 = 1;
      } else if(this.e < 2) {
         var2 = super.read(var1, var2, 2 - this.e);
      } else {
         var3 = Math.min(c - this.e, var3);
         System.arraycopy(a, this.e - 2, var1, var2, var3);
         var2 = var3;
      }

      if(var2 > 0) {
         this.e += var2;
      }

      return var2;
   }

   public void reset() throws IOException {
      throw new UnsupportedOperationException();
   }

   public long skip(long var1) throws IOException {
      var1 = super.skip(var1);
      if(var1 > 0L) {
         this.e = (int)((long)this.e + var1);
      }

      return var1;
   }
}
