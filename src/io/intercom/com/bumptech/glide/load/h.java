package io.intercom.com.bumptech.glide.load;

import java.security.MessageDigest;

public final class h {
   private static final h.a a = new h.a() {
      public void a(byte[] var1, Object var2, MessageDigest var3) {
      }
   };
   private final Object b;
   private final h.a c;
   private final String d;
   private volatile byte[] e;

   h(String var1, Object var2, h.a var3) {
      this.d = io.intercom.com.bumptech.glide.h.h.a(var1);
      this.b = var2;
      this.c = (h.a)io.intercom.com.bumptech.glide.h.h.a((Object)var3);
   }

   public static h a(String var0) {
      return new h(var0, (Object)null, c());
   }

   public static h a(String var0, Object var1) {
      return new h(var0, var1, c());
   }

   public static h a(String var0, Object var1, h.a var2) {
      return new h(var0, var1, var2);
   }

   private byte[] b() {
      if(this.e == null) {
         this.e = this.d.getBytes(g.a);
      }

      return this.e;
   }

   private static h.a c() {
      return a;
   }

   public Object a() {
      return this.b;
   }

   public void a(Object var1, MessageDigest var2) {
      this.c.a(this.b(), var1, var2);
   }

   public boolean equals(Object var1) {
      boolean var2;
      if(var1 instanceof h) {
         h var3 = (h)var1;
         var2 = this.d.equals(var3.d);
      } else {
         var2 = false;
      }

      return var2;
   }

   public int hashCode() {
      return this.d.hashCode();
   }

   public String toString() {
      return "Option{key='" + this.d + '\'' + '}';
   }

   public interface a {
      void a(byte[] var1, Object var2, MessageDigest var3);
   }
}
