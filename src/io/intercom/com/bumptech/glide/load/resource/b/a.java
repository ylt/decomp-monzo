package io.intercom.com.bumptech.glide.load.resource.b;

import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import io.intercom.com.bumptech.glide.h.h;
import io.intercom.com.bumptech.glide.load.engine.o;
import io.intercom.com.bumptech.glide.load.engine.r;
import io.intercom.com.bumptech.glide.load.resource.d.c;

public abstract class a implements o, r {
   protected final Drawable a;

   public a(Drawable var1) {
      this.a = (Drawable)h.a((Object)var1);
   }

   public void a() {
      if(this.a instanceof BitmapDrawable) {
         ((BitmapDrawable)this.a).getBitmap().prepareToDraw();
      } else if(this.a instanceof c) {
         ((c)this.a).b().prepareToDraw();
      }

   }

   // $FF: synthetic method
   public Object c() {
      return this.f();
   }

   public final Drawable f() {
      return this.a.getConstantState().newDrawable();
   }
}
