package io.intercom.com.bumptech.glide.load.resource.d;

import io.intercom.com.bumptech.glide.load.j;
import io.intercom.com.bumptech.glide.load.engine.r;

public final class h implements j {
   private final io.intercom.com.bumptech.glide.load.engine.a.e a;

   public h(io.intercom.com.bumptech.glide.load.engine.a.e var1) {
      this.a = var1;
   }

   public r a(io.intercom.com.bumptech.glide.b.a var1, int var2, int var3, io.intercom.com.bumptech.glide.load.i var4) {
      return io.intercom.com.bumptech.glide.load.resource.bitmap.e.a(var1.h(), this.a);
   }

   public boolean a(io.intercom.com.bumptech.glide.b.a var1, io.intercom.com.bumptech.glide.load.i var2) {
      return true;
   }
}
