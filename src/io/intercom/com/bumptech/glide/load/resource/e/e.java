package io.intercom.com.bumptech.glide.load.resource.e;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class e {
   private final List a = new ArrayList();

   public d a(Class var1, Class var2) {
      synchronized(this){}

      d var7;
      try {
         if(var2.isAssignableFrom(var1)) {
            var7 = f.a();
         } else {
            Iterator var3 = this.a.iterator();

            e.a var4;
            do {
               if(!var3.hasNext()) {
                  StringBuilder var8 = new StringBuilder();
                  IllegalArgumentException var9 = new IllegalArgumentException(var8.append("No transcoder registered to transcode from ").append(var1).append(" to ").append(var2).toString());
                  throw var9;
               }

               var4 = (e.a)var3.next();
            } while(!var4.a(var1, var2));

            var7 = var4.a;
         }
      } finally {
         ;
      }

      return var7;
   }

   public void a(Class var1, Class var2, d var3) {
      synchronized(this){}

      try {
         List var5 = this.a;
         e.a var4 = new e.a(var1, var2, var3);
         var5.add(var4);
      } finally {
         ;
      }

   }

   public List b(Class param1, Class param2) {
      // $FF: Couldn't be decompiled
   }

   private static final class a {
      final d a;
      private final Class b;
      private final Class c;

      a(Class var1, Class var2, d var3) {
         this.b = var1;
         this.c = var2;
         this.a = var3;
      }

      public boolean a(Class var1, Class var2) {
         boolean var3;
         if(this.b.isAssignableFrom(var1) && var2.isAssignableFrom(this.c)) {
            var3 = true;
         } else {
            var3 = false;
         }

         return var3;
      }
   }
}
