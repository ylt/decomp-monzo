package io.intercom.com.bumptech.glide.load.resource.e;

import android.content.res.Resources;
import android.graphics.Bitmap;
import io.intercom.com.bumptech.glide.h.h;
import io.intercom.com.bumptech.glide.load.engine.r;
import io.intercom.com.bumptech.glide.load.resource.bitmap.o;

public class b implements d {
   private final Resources a;
   private final io.intercom.com.bumptech.glide.load.engine.a.e b;

   public b(Resources var1, io.intercom.com.bumptech.glide.load.engine.a.e var2) {
      this.a = (Resources)h.a((Object)var1);
      this.b = (io.intercom.com.bumptech.glide.load.engine.a.e)h.a((Object)var2);
   }

   public r a(r var1) {
      return o.a(this.a, this.b, (Bitmap)var1.c());
   }
}
