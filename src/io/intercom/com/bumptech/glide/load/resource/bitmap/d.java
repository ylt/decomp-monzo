package io.intercom.com.bumptech.glide.load.resource.bitmap;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import java.io.File;

public class d implements io.intercom.com.bumptech.glide.load.k {
   public static final io.intercom.com.bumptech.glide.load.h a = io.intercom.com.bumptech.glide.load.h.a((String)"io.intercom.com.bumptech.glide.load.resource.bitmap.BitmapEncoder.CompressionQuality", (Object)Integer.valueOf(90));
   public static final io.intercom.com.bumptech.glide.load.h b = io.intercom.com.bumptech.glide.load.h.a("io.intercom.com.bumptech.glide.load.resource.bitmap.BitmapEncoder.CompressionFormat");

   private CompressFormat a(Bitmap var1, io.intercom.com.bumptech.glide.load.i var2) {
      CompressFormat var4 = (CompressFormat)var2.a(b);
      CompressFormat var3;
      if(var4 != null) {
         var3 = var4;
      } else if(var1.hasAlpha()) {
         var3 = CompressFormat.PNG;
      } else {
         var3 = CompressFormat.JPEG;
      }

      return var3;
   }

   public io.intercom.com.bumptech.glide.load.c a(io.intercom.com.bumptech.glide.load.i var1) {
      return io.intercom.com.bumptech.glide.load.c.b;
   }

   public boolean a(io.intercom.com.bumptech.glide.load.engine.r param1, File param2, io.intercom.com.bumptech.glide.load.i param3) {
      // $FF: Couldn't be decompiled
   }
}
