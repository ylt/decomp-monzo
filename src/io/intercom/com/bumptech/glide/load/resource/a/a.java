package io.intercom.com.bumptech.glide.load.resource.a;

import io.intercom.com.bumptech.glide.load.a.c;
import java.io.IOException;
import java.nio.ByteBuffer;

public class a implements c {
   private final ByteBuffer a;

   public a(ByteBuffer var1) {
      this.a = var1;
   }

   // $FF: synthetic method
   public Object a() throws IOException {
      return this.c();
   }

   public void b() {
   }

   public ByteBuffer c() throws IOException {
      this.a.position(0);
      return this.a;
   }

   public static class a implements c.a {
      public c a(ByteBuffer var1) {
         return new a(var1);
      }

      public Class a() {
         return ByteBuffer.class;
      }
   }
}
