package io.intercom.com.bumptech.glide.load.resource.bitmap;

import android.annotation.TargetApi;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory.Options;
import android.os.Build.VERSION;
import java.io.File;

final class n {
   private static final File a = new File("/proc/self/fd");
   private static volatile n d;
   private volatile int b;
   private volatile boolean c = true;

   static n a() {
      // $FF: Couldn't be decompiled
   }

   private boolean b() {
      // $FF: Couldn't be decompiled
   }

   @TargetApi(26)
   boolean a(int var1, int var2, Options var3, io.intercom.com.bumptech.glide.load.b var4, boolean var5, boolean var6) {
      if(var5 && VERSION.SDK_INT >= 26 && var4 != io.intercom.com.bumptech.glide.load.b.b && !var6) {
         if(var1 >= 128 && var2 >= 128 && this.b()) {
            var5 = true;
         } else {
            var5 = false;
         }

         var6 = var5;
         if(var5) {
            var3.inPreferredConfig = Config.HARDWARE;
            var3.inMutable = false;
            var6 = var5;
         }
      } else {
         var6 = false;
      }

      return var6;
   }
}
