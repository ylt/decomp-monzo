package io.intercom.com.bumptech.glide.load.resource.e;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import io.intercom.com.bumptech.glide.load.engine.r;
import java.io.ByteArrayOutputStream;

public class a implements d {
   private final CompressFormat a;
   private final int b;

   public a() {
      this(CompressFormat.JPEG, 100);
   }

   public a(CompressFormat var1, int var2) {
      this.a = var1;
      this.b = var2;
   }

   public r a(r var1) {
      ByteArrayOutputStream var2 = new ByteArrayOutputStream();
      ((Bitmap)var1.c()).compress(this.a, this.b, var2);
      var1.e();
      return new io.intercom.com.bumptech.glide.load.resource.a.b(var2.toByteArray());
   }
}
