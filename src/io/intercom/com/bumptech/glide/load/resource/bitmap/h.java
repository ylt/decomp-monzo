package io.intercom.com.bumptech.glide.load.resource.bitmap;

import android.content.Context;
import android.graphics.Bitmap;
import java.security.MessageDigest;

public class h extends f {
   private static final String ID = "io.intercom.com.bumptech.glide.load.resource.bitmap.CenterCrop";
   private static final byte[] ID_BYTES;

   static {
      ID_BYTES = "io.intercom.com.bumptech.glide.load.resource.bitmap.CenterCrop".getBytes(a);
   }

   public h() {
   }

   @Deprecated
   public h(Context var1) {
      this();
   }

   @Deprecated
   public h(io.intercom.com.bumptech.glide.load.engine.a.e var1) {
      this();
   }

   public boolean equals(Object var1) {
      return var1 instanceof h;
   }

   public int hashCode() {
      return "io.intercom.com.bumptech.glide.load.resource.bitmap.CenterCrop".hashCode();
   }

   protected Bitmap transform(io.intercom.com.bumptech.glide.load.engine.a.e var1, Bitmap var2, int var3, int var4) {
      return q.a(var1, var2, var3, var4);
   }

   public void updateDiskCacheKey(MessageDigest var1) {
      var1.update(ID_BYTES);
   }
}
