package io.intercom.com.bumptech.glide.load.resource.bitmap;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;

public class o implements io.intercom.com.bumptech.glide.load.engine.o, io.intercom.com.bumptech.glide.load.engine.r {
   private final Bitmap a;
   private final Resources b;
   private final io.intercom.com.bumptech.glide.load.engine.a.e c;

   o(Resources var1, io.intercom.com.bumptech.glide.load.engine.a.e var2, Bitmap var3) {
      this.b = (Resources)io.intercom.com.bumptech.glide.h.h.a((Object)var1);
      this.c = (io.intercom.com.bumptech.glide.load.engine.a.e)io.intercom.com.bumptech.glide.h.h.a((Object)var2);
      this.a = (Bitmap)io.intercom.com.bumptech.glide.h.h.a((Object)var3);
   }

   public static o a(Context var0, Bitmap var1) {
      return a(var0.getResources(), io.intercom.com.bumptech.glide.c.a(var0).a(), var1);
   }

   public static o a(Resources var0, io.intercom.com.bumptech.glide.load.engine.a.e var1, Bitmap var2) {
      return new o(var0, var1, var2);
   }

   public void a() {
      this.a.prepareToDraw();
   }

   public Class b() {
      return BitmapDrawable.class;
   }

   // $FF: synthetic method
   public Object c() {
      return this.f();
   }

   public int d() {
      return io.intercom.com.bumptech.glide.h.i.a(this.a);
   }

   public void e() {
      this.c.a(this.a);
   }

   public BitmapDrawable f() {
      return new BitmapDrawable(this.b, this.a);
   }
}
