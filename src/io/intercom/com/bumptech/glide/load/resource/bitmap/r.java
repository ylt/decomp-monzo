package io.intercom.com.bumptech.glide.load.resource.bitmap;

import android.media.MediaMetadataRetriever;
import android.os.ParcelFileDescriptor;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.security.MessageDigest;

public class r implements io.intercom.com.bumptech.glide.load.j {
   public static final io.intercom.com.bumptech.glide.load.h a = io.intercom.com.bumptech.glide.load.h.a("io.intercom.com.bumptech.glide.load.resource.bitmap.VideoBitmapDecode.TargetFrame", Long.valueOf(-1L), new io.intercom.com.bumptech.glide.load.h.a() {
      private final ByteBuffer a = ByteBuffer.allocate(8);

      public void a(byte[] param1, Long param2, MessageDigest param3) {
         // $FF: Couldn't be decompiled
      }
   });
   public static final io.intercom.com.bumptech.glide.load.h b = io.intercom.com.bumptech.glide.load.h.a("io.intercom.com.bumptech.glide.load.resource.bitmap.VideoBitmapDecode.FrameOption", (Object)null, new io.intercom.com.bumptech.glide.load.h.a() {
      private final ByteBuffer a = ByteBuffer.allocate(4);

      public void a(byte[] param1, Integer param2, MessageDigest param3) {
         // $FF: Couldn't be decompiled
      }
   });
   private static final r.a c = new r.a();
   private final io.intercom.com.bumptech.glide.load.engine.a.e d;
   private final r.a e;

   public r(io.intercom.com.bumptech.glide.load.engine.a.e var1) {
      this(var1, c);
   }

   r(io.intercom.com.bumptech.glide.load.engine.a.e var1, r.a var2) {
      this.d = var1;
      this.e = var2;
   }

   public io.intercom.com.bumptech.glide.load.engine.r a(ParcelFileDescriptor param1, int param2, int param3, io.intercom.com.bumptech.glide.load.i param4) throws IOException {
      // $FF: Couldn't be decompiled
   }

   public boolean a(ParcelFileDescriptor var1, io.intercom.com.bumptech.glide.load.i var2) {
      MediaMetadataRetriever var9 = this.e.a();
      boolean var6 = false;

      boolean var3;
      label48: {
         try {
            var6 = true;
            var9.setDataSource(var1.getFileDescriptor());
            var6 = false;
            break label48;
         } catch (RuntimeException var7) {
            var6 = false;
         } finally {
            if(var6) {
               var9.release();
            }
         }

         var3 = false;
         var9.release();
         return var3;
      }

      var3 = true;
      var9.release();
      return var3;
   }

   static class a {
      public MediaMetadataRetriever a() {
         return new MediaMetadataRetriever();
      }
   }
}
