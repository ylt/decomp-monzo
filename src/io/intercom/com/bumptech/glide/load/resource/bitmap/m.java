package io.intercom.com.bumptech.glide.load.resource.bitmap;

import android.graphics.Bitmap;
import java.security.MessageDigest;

public class m extends f {
   private static final byte[] b;

   static {
      b = "io.intercom.com.bumptech.glide.load.resource.bitmap.FitCenter".getBytes(a);
   }

   public boolean equals(Object var1) {
      return var1 instanceof m;
   }

   public int hashCode() {
      return "io.intercom.com.bumptech.glide.load.resource.bitmap.FitCenter".hashCode();
   }

   protected Bitmap transform(io.intercom.com.bumptech.glide.load.engine.a.e var1, Bitmap var2, int var3, int var4) {
      return q.b(var1, var2, var3, var4);
   }

   public void updateDiskCacheKey(MessageDigest var1) {
      var1.update(b);
   }
}
