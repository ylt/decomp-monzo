package io.intercom.com.bumptech.glide.load.resource.bitmap;

import android.graphics.drawable.BitmapDrawable;
import java.io.File;

public class b implements io.intercom.com.bumptech.glide.load.k {
   private final io.intercom.com.bumptech.glide.load.engine.a.e a;
   private final io.intercom.com.bumptech.glide.load.k b;

   public b(io.intercom.com.bumptech.glide.load.engine.a.e var1, io.intercom.com.bumptech.glide.load.k var2) {
      this.a = var1;
      this.b = var2;
   }

   public io.intercom.com.bumptech.glide.load.c a(io.intercom.com.bumptech.glide.load.i var1) {
      return this.b.a(var1);
   }

   public boolean a(io.intercom.com.bumptech.glide.load.engine.r var1, File var2, io.intercom.com.bumptech.glide.load.i var3) {
      return this.b.a(new e(((BitmapDrawable)var1.c()).getBitmap(), this.a), var2, var3);
   }
}
