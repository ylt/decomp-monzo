package io.intercom.com.bumptech.glide.load.resource.bitmap;

import android.util.Log;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;

public final class j implements io.intercom.com.bumptech.glide.load.e {
   static final byte[] a = "Exif\u0000\u0000".getBytes(Charset.forName("UTF-8"));
   private static final int[] b = new int[]{0, 1, 1, 2, 4, 8, 1, 1, 2, 4, 8, 4, 8};

   private static int a(int var0, int var1) {
      return var0 + 2 + var1 * 12;
   }

   private static int a(j.b var0) {
      int var2 = "Exif\u0000\u0000".length();
      short var1 = var0.b(var2);
      ByteOrder var8;
      if(var1 == 19789) {
         var8 = ByteOrder.BIG_ENDIAN;
      } else if(var1 == 18761) {
         var8 = ByteOrder.LITTLE_ENDIAN;
      } else {
         if(Log.isLoggable("DfltImageHeaderParser", 3)) {
            Log.d("DfltImageHeaderParser", "Unknown endianness = " + var1);
         }

         var8 = ByteOrder.BIG_ENDIAN;
      }

      var0.a(var8);
      int var3 = var2 + var0.a(var2 + 4);
      short var10 = var0.b(var3);
      int var9 = 0;

      while(true) {
         if(var9 >= var10) {
            var1 = -1;
            break;
         }

         int var5 = a(var3, var9);
         short var4 = var0.b(var5);
         if(var4 == 274) {
            short var6 = var0.b(var5 + 2);
            if(var6 >= 1 && var6 <= 12) {
               int var7 = var0.a(var5 + 4);
               if(var7 < 0) {
                  if(Log.isLoggable("DfltImageHeaderParser", 3)) {
                     Log.d("DfltImageHeaderParser", "Negative tiff component count");
                  }
               } else {
                  if(Log.isLoggable("DfltImageHeaderParser", 3)) {
                     Log.d("DfltImageHeaderParser", "Got tagIndex=" + var9 + " tagType=" + var4 + " formatCode=" + var6 + " componentCount=" + var7);
                  }

                  var7 += b[var6];
                  if(var7 > 4) {
                     if(Log.isLoggable("DfltImageHeaderParser", 3)) {
                        Log.d("DfltImageHeaderParser", "Got byte count > 4, not orientation, continuing, formatCode=" + var6);
                     }
                  } else {
                     var5 += 8;
                     if(var5 >= 0 && var5 <= var0.a()) {
                        if(var7 >= 0 && var5 + var7 <= var0.a()) {
                           var1 = var0.b(var5);
                           break;
                        }

                        if(Log.isLoggable("DfltImageHeaderParser", 3)) {
                           Log.d("DfltImageHeaderParser", "Illegal number of bytes for TI tag data tagType=" + var4);
                        }
                     } else if(Log.isLoggable("DfltImageHeaderParser", 3)) {
                        Log.d("DfltImageHeaderParser", "Illegal tagValueOffset=" + var5 + " tagType=" + var4);
                     }
                  }
               }
            } else if(Log.isLoggable("DfltImageHeaderParser", 3)) {
               Log.d("DfltImageHeaderParser", "Got invalid format code = " + var6);
            }
         }

         ++var9;
      }

      return var1;
   }

   private int a(j.c var1, io.intercom.com.bumptech.glide.load.engine.a.b var2) throws IOException {
      byte var4 = -1;
      int var5 = var1.a();
      int var3;
      if(!a(var5)) {
         var3 = var4;
         if(Log.isLoggable("DfltImageHeaderParser", 3)) {
            Log.d("DfltImageHeaderParser", "Parser doesn't handle magic number: " + var5);
            var3 = var4;
         }
      } else {
         var3 = this.b(var1);
         if(var3 == -1) {
            var3 = var4;
            if(Log.isLoggable("DfltImageHeaderParser", 3)) {
               Log.d("DfltImageHeaderParser", "Failed to parse exif segment length, or exif segment not found");
               var3 = var4;
            }
         } else {
            byte[] var6 = (byte[])var2.a(var3, byte[].class);

            try {
               var3 = this.a(var1, var6, var3);
            } finally {
               var2.a(var6, byte[].class);
            }
         }
      }

      return var3;
   }

   private int a(j.c var1, byte[] var2, int var3) throws IOException {
      byte var5 = -1;
      int var6 = var1.a(var2, var3);
      int var4;
      if(var6 != var3) {
         var4 = var5;
         if(Log.isLoggable("DfltImageHeaderParser", 3)) {
            Log.d("DfltImageHeaderParser", "Unable to read exif segment data, length: " + var3 + ", actually read: " + var6);
            var4 = var5;
         }
      } else if(this.a(var2, var3)) {
         var4 = a(new j.b(var2, var3));
      } else {
         var4 = var5;
         if(Log.isLoggable("DfltImageHeaderParser", 3)) {
            Log.d("DfltImageHeaderParser", "Missing jpeg exif preamble");
            var4 = var5;
         }
      }

      return var4;
   }

   private io.intercom.com.bumptech.glide.load.e.a a(j.c var1) throws IOException {
      int var2 = var1.a();
      io.intercom.com.bumptech.glide.load.e.a var3;
      if(var2 == '\uffd8') {
         var3 = io.intercom.com.bumptech.glide.load.e.a.b;
      } else {
         var2 = var2 << 16 & -65536 | var1.a() & '\uffff';
         if(var2 == -1991225785) {
            var1.a(21L);
            if(var1.c() >= 3) {
               var3 = io.intercom.com.bumptech.glide.load.e.a.d;
            } else {
               var3 = io.intercom.com.bumptech.glide.load.e.a.e;
            }
         } else if(var2 >> 8 == 4671814) {
            var3 = io.intercom.com.bumptech.glide.load.e.a.a;
         } else if(var2 != 1380533830) {
            var3 = io.intercom.com.bumptech.glide.load.e.a.h;
         } else {
            var1.a(4L);
            if((var1.a() << 16 & -65536 | var1.a() & '\uffff') != 1464156752) {
               var3 = io.intercom.com.bumptech.glide.load.e.a.h;
            } else {
               var2 = var1.a() << 16 & -65536 | var1.a() & '\uffff';
               if((var2 & -256) != 1448097792) {
                  var3 = io.intercom.com.bumptech.glide.load.e.a.h;
               } else if((var2 & 255) == 88) {
                  var1.a(4L);
                  if((var1.c() & 16) != 0) {
                     var3 = io.intercom.com.bumptech.glide.load.e.a.f;
                  } else {
                     var3 = io.intercom.com.bumptech.glide.load.e.a.g;
                  }
               } else if((var2 & 255) == 76) {
                  var1.a(4L);
                  if((var1.c() & 8) != 0) {
                     var3 = io.intercom.com.bumptech.glide.load.e.a.f;
                  } else {
                     var3 = io.intercom.com.bumptech.glide.load.e.a.g;
                  }
               } else {
                  var3 = io.intercom.com.bumptech.glide.load.e.a.g;
               }
            }
         }
      }

      return var3;
   }

   private static boolean a(int var0) {
      boolean var1;
      if((var0 & '\uffd8') != '\uffd8' && var0 != 19789 && var0 != 18761) {
         var1 = false;
      } else {
         var1 = true;
      }

      return var1;
   }

   private boolean a(byte[] var1, int var2) {
      boolean var4 = false;
      boolean var3;
      if(var1 != null && var2 > a.length) {
         var3 = true;
      } else {
         var3 = false;
      }

      if(var3) {
         for(var2 = 0; var2 < a.length; ++var2) {
            if(var1[var2] != a[var2]) {
               var3 = var4;
               break;
            }
         }
      }

      return var3;
   }

   private int b(j.c var1) throws IOException {
      byte var3 = -1;

      int var2;
      while(true) {
         short var4 = var1.b();
         if(var4 != 255) {
            var2 = var3;
            if(Log.isLoggable("DfltImageHeaderParser", 3)) {
               Log.d("DfltImageHeaderParser", "Unknown segmentId=" + var4);
               var2 = var3;
            }
            break;
         }

         short var5 = var1.b();
         var2 = var3;
         if(var5 == 218) {
            break;
         }

         if(var5 == 217) {
            var2 = var3;
            if(Log.isLoggable("DfltImageHeaderParser", 3)) {
               Log.d("DfltImageHeaderParser", "Found MARKER_EOI in exif segment");
               var2 = var3;
            }
            break;
         }

         int var8 = var1.a() - 2;
         if(var5 != 225) {
            long var6 = var1.a((long)var8);
            if(var6 == (long)var8) {
               continue;
            }

            var2 = var3;
            if(Log.isLoggable("DfltImageHeaderParser", 3)) {
               Log.d("DfltImageHeaderParser", "Unable to skip enough data, type: " + var5 + ", wanted to skip: " + var8 + ", but actually skipped: " + var6);
               var2 = var3;
            }
            break;
         }

         var2 = var8;
         break;
      }

      return var2;
   }

   public int a(InputStream var1, io.intercom.com.bumptech.glide.load.engine.a.b var2) throws IOException {
      return this.a((j.c)(new j.d((InputStream)io.intercom.com.bumptech.glide.h.h.a((Object)var1))), (io.intercom.com.bumptech.glide.load.engine.a.b)io.intercom.com.bumptech.glide.h.h.a((Object)var2));
   }

   public io.intercom.com.bumptech.glide.load.e.a a(InputStream var1) throws IOException {
      return this.a((j.c)(new j.d((InputStream)io.intercom.com.bumptech.glide.h.h.a((Object)var1))));
   }

   public io.intercom.com.bumptech.glide.load.e.a a(ByteBuffer var1) throws IOException {
      return this.a((j.c)(new j.a((ByteBuffer)io.intercom.com.bumptech.glide.h.h.a((Object)var1))));
   }

   private static final class a implements j.c {
      private final ByteBuffer a;

      a(ByteBuffer var1) {
         this.a = var1;
         var1.order(ByteOrder.BIG_ENDIAN);
      }

      public int a() throws IOException {
         return this.c() << 8 & '\uff00' | this.c() & 255;
      }

      public int a(byte[] var1, int var2) throws IOException {
         var2 = Math.min(var2, this.a.remaining());
         if(var2 == 0) {
            var2 = -1;
         } else {
            this.a.get(var1, 0, var2);
         }

         return var2;
      }

      public long a(long var1) throws IOException {
         int var3 = (int)Math.min((long)this.a.remaining(), var1);
         this.a.position(this.a.position() + var3);
         return (long)var3;
      }

      public short b() throws IOException {
         return (short)(this.c() & 255);
      }

      public int c() throws IOException {
         byte var1;
         if(this.a.remaining() < 1) {
            var1 = -1;
         } else {
            var1 = this.a.get();
         }

         return var1;
      }
   }

   private static final class b {
      private final ByteBuffer a;

      b(byte[] var1, int var2) {
         this.a = (ByteBuffer)ByteBuffer.wrap(var1).order(ByteOrder.BIG_ENDIAN).limit(var2);
      }

      private boolean a(int var1, int var2) {
         boolean var3;
         if(this.a.remaining() - var1 >= var2) {
            var3 = true;
         } else {
            var3 = false;
         }

         return var3;
      }

      int a() {
         return this.a.remaining();
      }

      int a(int var1) {
         if(this.a(var1, 4)) {
            var1 = this.a.getInt(var1);
         } else {
            var1 = -1;
         }

         return var1;
      }

      void a(ByteOrder var1) {
         this.a.order(var1);
      }

      short b(int var1) {
         short var2;
         if(this.a(var1, 2)) {
            var2 = this.a.getShort(var1);
         } else {
            var2 = -1;
         }

         return var2;
      }
   }

   private interface c {
      int a() throws IOException;

      int a(byte[] var1, int var2) throws IOException;

      long a(long var1) throws IOException;

      short b() throws IOException;

      int c() throws IOException;
   }

   private static final class d implements j.c {
      private final InputStream a;

      d(InputStream var1) {
         this.a = var1;
      }

      public int a() throws IOException {
         return this.a.read() << 8 & '\uff00' | this.a.read() & 255;
      }

      public int a(byte[] var1, int var2) throws IOException {
         int var3;
         int var4;
         for(var3 = var2; var3 > 0; var3 -= var4) {
            var4 = this.a.read(var1, var2 - var3, var3);
            if(var4 == -1) {
               break;
            }
         }

         return var2 - var3;
      }

      public long a(long var1) throws IOException {
         if(var1 < 0L) {
            var1 = 0L;
         } else {
            long var3 = var1;

            while(var3 > 0L) {
               long var5 = this.a.skip(var3);
               if(var5 > 0L) {
                  var3 -= var5;
               } else {
                  if(this.a.read() == -1) {
                     break;
                  }

                  --var3;
               }
            }

            var1 -= var3;
         }

         return var1;
      }

      public short b() throws IOException {
         return (short)(this.a.read() & 255);
      }

      public int c() throws IOException {
         return this.a.read();
      }
   }
}
