package io.intercom.com.bumptech.glide.load.resource.bitmap;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.graphics.Bitmap.Config;
import android.graphics.PorterDuff.Mode;
import android.os.Build;
import android.os.Build.VERSION;
import android.util.Log;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public final class q {
   private static final Paint a = new Paint(6);
   private static final Paint b = new Paint(7);
   private static final Paint c;
   private static final List d = Arrays.asList(new String[]{"XT1097", "XT1085"});
   private static final Lock e;

   static {
      Object var0;
      if(d.contains(Build.MODEL) && VERSION.SDK_INT == 22) {
         var0 = new ReentrantLock();
      } else {
         var0 = new q.a();
      }

      e = (Lock)var0;
      c = new Paint(7);
      c.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
   }

   public static int a(int var0) {
      short var1;
      switch(var0) {
      case 3:
      case 4:
         var1 = 180;
         break;
      case 5:
      case 6:
         var1 = 90;
         break;
      case 7:
      case 8:
         var1 = 270;
         break;
      default:
         var1 = 0;
      }

      return var1;
   }

   private static Config a(Bitmap var0) {
      Config var1;
      if(var0.getConfig() != null) {
         var1 = var0.getConfig();
      } else {
         var1 = Config.ARGB_8888;
      }

      return var1;
   }

   public static Bitmap a(io.intercom.com.bumptech.glide.load.engine.a.e var0, Bitmap var1, int var2) {
      Bitmap var5;
      if(!b(var2)) {
         var5 = var1;
      } else {
         Matrix var4 = new Matrix();
         a(var2, var4);
         RectF var3 = new RectF(0.0F, 0.0F, (float)var1.getWidth(), (float)var1.getHeight());
         var4.mapRect(var3);
         var5 = var0.a(Math.round(var3.width()), Math.round(var3.height()), a(var1));
         var4.postTranslate(-var3.left, -var3.top);
         a(var1, var5, var4);
      }

      return var5;
   }

   public static Bitmap a(io.intercom.com.bumptech.glide.load.engine.a.e var0, Bitmap var1, int var2, int var3) {
      float var6 = 0.0F;
      Bitmap var9;
      if(var1.getWidth() == var2 && var1.getHeight() == var3) {
         var9 = var1;
      } else {
         Matrix var8 = new Matrix();
         float var4;
         float var5;
         if(var1.getWidth() * var3 > var1.getHeight() * var2) {
            var4 = (float)var3 / (float)var1.getHeight();
            var5 = ((float)var2 - (float)var1.getWidth() * var4) * 0.5F;
         } else {
            var4 = (float)var2 / (float)var1.getWidth();
            float var7 = (float)var3;
            var6 = (float)var1.getHeight();
            var5 = 0.0F;
            var6 = (var7 - var6 * var4) * 0.5F;
         }

         var8.setScale(var4, var4);
         var8.postTranslate((float)((int)(var5 + 0.5F)), (float)((int)(var6 + 0.5F)));
         var9 = var0.a(var2, var3, a(var1));
         a(var1, var9);
         a(var1, var9, var8);
      }

      return var9;
   }

   public static Lock a() {
      return e;
   }

   static void a(int var0, Matrix var1) {
      switch(var0) {
      case 2:
         var1.setScale(-1.0F, 1.0F);
         break;
      case 3:
         var1.setRotate(180.0F);
         break;
      case 4:
         var1.setRotate(180.0F);
         var1.postScale(-1.0F, 1.0F);
         break;
      case 5:
         var1.setRotate(90.0F);
         var1.postScale(-1.0F, 1.0F);
         break;
      case 6:
         var1.setRotate(90.0F);
         break;
      case 7:
         var1.setRotate(-90.0F);
         var1.postScale(-1.0F, 1.0F);
         break;
      case 8:
         var1.setRotate(-90.0F);
      }

   }

   public static void a(Bitmap var0, Bitmap var1) {
      var1.setHasAlpha(var0.hasAlpha());
   }

   private static void a(Bitmap var0, Bitmap var1, Matrix var2) {
      e.lock();

      try {
         Canvas var3 = new Canvas(var1);
         var3.drawBitmap(var0, var2, a);
         a(var3);
      } finally {
         e.unlock();
      }

   }

   private static void a(Canvas var0) {
      var0.setBitmap((Bitmap)null);
   }

   public static Bitmap b(io.intercom.com.bumptech.glide.load.engine.a.e var0, Bitmap var1, int var2, int var3) {
      Bitmap var8;
      if(var1.getWidth() == var2 && var1.getHeight() == var3) {
         var8 = var1;
         if(Log.isLoggable("TransformationUtils", 2)) {
            Log.v("TransformationUtils", "requested target size matches input, returning input");
            var8 = var1;
         }
      } else {
         float var4 = Math.min((float)var2 / (float)var1.getWidth(), (float)var3 / (float)var1.getHeight());
         int var6 = (int)((float)var1.getWidth() * var4);
         int var5 = (int)((float)var1.getHeight() * var4);
         if(var1.getWidth() == var6 && var1.getHeight() == var5) {
            var8 = var1;
            if(Log.isLoggable("TransformationUtils", 2)) {
               Log.v("TransformationUtils", "adjusted target size matches input, returning input");
               var8 = var1;
            }
         } else {
            var8 = var0.a(var6, var5, a(var1));
            a(var1, var8);
            if(Log.isLoggable("TransformationUtils", 2)) {
               Log.v("TransformationUtils", "request: " + var2 + "x" + var3);
               Log.v("TransformationUtils", "toFit:   " + var1.getWidth() + "x" + var1.getHeight());
               Log.v("TransformationUtils", "toReuse: " + var8.getWidth() + "x" + var8.getHeight());
               Log.v("TransformationUtils", "minPct:   " + var4);
            }

            Matrix var7 = new Matrix();
            var7.setScale(var4, var4);
            a(var1, var8, var7);
         }
      }

      return var8;
   }

   public static boolean b(int var0) {
      boolean var1;
      switch(var0) {
      case 2:
      case 3:
      case 4:
      case 5:
      case 6:
      case 7:
      case 8:
         var1 = true;
         break;
      default:
         var1 = false;
      }

      return var1;
   }

   public static Bitmap c(io.intercom.com.bumptech.glide.load.engine.a.e var0, Bitmap var1, int var2, int var3) {
      Bitmap var4;
      if(var1.getWidth() <= var2 && var1.getHeight() <= var3) {
         var4 = var1;
         if(Log.isLoggable("TransformationUtils", 2)) {
            Log.v("TransformationUtils", "requested target size larger or equal to input, returning input");
            var4 = var1;
         }
      } else {
         if(Log.isLoggable("TransformationUtils", 2)) {
            Log.v("TransformationUtils", "requested target size too big for input, fit centering instead");
         }

         var4 = b(var0, var1, var2, var3);
      }

      return var4;
   }

   private static final class a implements Lock {
      public void lock() {
      }

      public void lockInterruptibly() throws InterruptedException {
      }

      public Condition newCondition() {
         throw new UnsupportedOperationException("Should not be called");
      }

      public boolean tryLock() {
         return true;
      }

      public boolean tryLock(long var1, TimeUnit var3) throws InterruptedException {
         return true;
      }

      public void unlock() {
      }
   }
}
