package io.intercom.com.bumptech.glide.load.resource.d;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.os.Handler.Callback;
import io.intercom.com.bumptech.glide.load.l;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

class g {
   final io.intercom.com.bumptech.glide.i a;
   private final io.intercom.com.bumptech.glide.b.a b;
   private final Handler c;
   private final List d;
   private final io.intercom.com.bumptech.glide.load.engine.a.e e;
   private boolean f;
   private boolean g;
   private boolean h;
   private io.intercom.com.bumptech.glide.h i;
   private g.a j;
   private boolean k;
   private g.a l;
   private Bitmap m;
   private l n;

   public g(io.intercom.com.bumptech.glide.c var1, io.intercom.com.bumptech.glide.b.a var2, int var3, int var4, l var5, Bitmap var6) {
      this(var1.a(), io.intercom.com.bumptech.glide.c.b(var1.c()), var2, (Handler)null, a(io.intercom.com.bumptech.glide.c.b(var1.c()), var3, var4), var5, var6);
   }

   g(io.intercom.com.bumptech.glide.load.engine.a.e var1, io.intercom.com.bumptech.glide.i var2, io.intercom.com.bumptech.glide.b.a var3, Handler var4, io.intercom.com.bumptech.glide.h var5, l var6, Bitmap var7) {
      this.d = new ArrayList();
      this.f = false;
      this.g = false;
      this.h = false;
      this.a = var2;
      Handler var8 = var4;
      if(var4 == null) {
         var8 = new Handler(Looper.getMainLooper(), new g.c());
      }

      this.e = var1;
      this.c = var8;
      this.i = var5;
      this.b = var3;
      this.a(var6, var7);
   }

   private static io.intercom.com.bumptech.glide.h a(io.intercom.com.bumptech.glide.i var0, int var1, int var2) {
      return var0.c().a(io.intercom.com.bumptech.glide.f.f.a(io.intercom.com.bumptech.glide.load.engine.h.b).a(true).a(var1, var2));
   }

   private int j() {
      return io.intercom.com.bumptech.glide.h.i.a(this.i().getWidth(), this.i().getHeight(), this.i().getConfig());
   }

   private void k() {
      if(!this.f) {
         this.f = true;
         this.k = false;
         this.m();
      }

   }

   private void l() {
      this.f = false;
   }

   private void m() {
      if(this.f && !this.g) {
         if(this.h) {
            this.b.f();
            this.h = false;
         }

         this.g = true;
         int var1 = this.b.c();
         long var4 = SystemClock.uptimeMillis();
         long var2 = (long)var1;
         this.b.b();
         this.l = new g.a(this.c, this.b.e(), var2 + var4);
         this.i.b().a(io.intercom.com.bumptech.glide.f.f.a((io.intercom.com.bumptech.glide.load.g)(new g.d()))).a((Object)this.b).a((io.intercom.com.bumptech.glide.f.a.h)this.l);
      }

   }

   private void n() {
      if(this.m != null) {
         this.e.a(this.m);
         this.m = null;
      }

   }

   Bitmap a() {
      return this.m;
   }

   void a(l var1, Bitmap var2) {
      this.n = (l)io.intercom.com.bumptech.glide.h.h.a((Object)var1);
      this.m = (Bitmap)io.intercom.com.bumptech.glide.h.h.a((Object)var2);
      this.i = this.i.a((new io.intercom.com.bumptech.glide.f.f()).a(var1));
   }

   void a(g.a var1) {
      if(this.k) {
         this.c.obtainMessage(2, var1).sendToTarget();
      } else {
         if(var1.a() != null) {
            this.n();
            g.a var3 = this.j;
            this.j = var1;

            for(int var2 = this.d.size() - 1; var2 >= 0; --var2) {
               ((g.b)this.d.get(var2)).f();
            }

            if(var3 != null) {
               this.c.obtainMessage(2, var3).sendToTarget();
            }
         }

         this.g = false;
         this.m();
      }

   }

   void a(g.b var1) {
      if(this.k) {
         throw new IllegalStateException("Cannot subscribe to a cleared frame loader");
      } else {
         boolean var2 = this.d.isEmpty();
         if(this.d.contains(var1)) {
            throw new IllegalStateException("Cannot subscribe twice in a row");
         } else {
            this.d.add(var1);
            if(var2) {
               this.k();
            }

         }
      }
   }

   int b() {
      return this.i().getWidth();
   }

   void b(g.b var1) {
      this.d.remove(var1);
      if(this.d.isEmpty()) {
         this.l();
      }

   }

   int c() {
      return this.i().getHeight();
   }

   int d() {
      return this.b.g() + this.j();
   }

   int e() {
      int var1;
      if(this.j != null) {
         var1 = this.j.a;
      } else {
         var1 = -1;
      }

      return var1;
   }

   ByteBuffer f() {
      return this.b.a().asReadOnlyBuffer();
   }

   int g() {
      return this.b.d();
   }

   void h() {
      this.d.clear();
      this.n();
      this.l();
      if(this.j != null) {
         this.a.a((io.intercom.com.bumptech.glide.f.a.h)this.j);
         this.j = null;
      }

      if(this.l != null) {
         this.a.a((io.intercom.com.bumptech.glide.f.a.h)this.l);
         this.l = null;
      }

      this.b.i();
      this.k = true;
   }

   Bitmap i() {
      Bitmap var1;
      if(this.j != null) {
         var1 = this.j.a();
      } else {
         var1 = this.m;
      }

      return var1;
   }

   static class a extends io.intercom.com.bumptech.glide.f.a.f {
      final int a;
      private final Handler b;
      private final long c;
      private Bitmap d;

      a(Handler var1, int var2, long var3) {
         this.b = var1;
         this.a = var2;
         this.c = var3;
      }

      Bitmap a() {
         return this.d;
      }

      public void a(Bitmap var1, io.intercom.com.bumptech.glide.f.b.d var2) {
         this.d = var1;
         Message var3 = this.b.obtainMessage(1, this);
         this.b.sendMessageAtTime(var3, this.c);
      }

      // $FF: synthetic method
      public void onResourceReady(Object var1, io.intercom.com.bumptech.glide.f.b.d var2) {
         this.a((Bitmap)var1, var2);
      }
   }

   public interface b {
      void f();
   }

   private class c implements Callback {
      public boolean handleMessage(Message var1) {
         boolean var2;
         g.a var3;
         if(var1.what == 1) {
            var3 = (g.a)var1.obj;
            g.this.a(var3);
            var2 = true;
         } else {
            if(var1.what == 2) {
               var3 = (g.a)var1.obj;
               g.this.a.a((io.intercom.com.bumptech.glide.f.a.h)var3);
            }

            var2 = false;
         }

         return var2;
      }
   }

   static class d implements io.intercom.com.bumptech.glide.load.g {
      private final UUID b;

      public d() {
         this(UUID.randomUUID());
      }

      d(UUID var1) {
         this.b = var1;
      }

      public boolean equals(Object var1) {
         boolean var2;
         if(var1 instanceof g.d) {
            var2 = ((g.d)var1).b.equals(this.b);
         } else {
            var2 = false;
         }

         return var2;
      }

      public int hashCode() {
         return this.b.hashCode();
      }

      public void updateDiskCacheKey(MessageDigest var1) {
         throw new UnsupportedOperationException("Not implemented");
      }
   }
}
