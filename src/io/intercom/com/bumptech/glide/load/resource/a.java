package io.intercom.com.bumptech.glide.load.resource;

import io.intercom.com.bumptech.glide.h.h;
import io.intercom.com.bumptech.glide.load.engine.r;

public class a implements r {
   protected final Object a;

   public a(Object var1) {
      this.a = h.a(var1);
   }

   public Class b() {
      return this.a.getClass();
   }

   public final Object c() {
      return this.a;
   }

   public final int d() {
      return 1;
   }

   public void e() {
   }
}
