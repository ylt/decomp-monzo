package io.intercom.com.bumptech.glide.load.resource.bitmap;

public abstract class k {
   public static final k a = new k.e();
   public static final k b = new k.d();
   public static final k c = new k.a();
   public static final k d = new k.b();
   public static final k e = new k.c();
   public static final k f = new k.f();
   public static final k g;

   static {
      g = b;
   }

   public abstract float a(int var1, int var2, int var3, int var4);

   public abstract k.g b(int var1, int var2, int var3, int var4);

   private static class a extends k {
      public float a(int var1, int var2, int var3, int var4) {
         float var5 = 1.0F;
         var1 = Math.min(var2 / var4, var1 / var3);
         if(var1 != 0) {
            var5 = 1.0F / (float)Integer.highestOneBit(var1);
         }

         return var5;
      }

      public k.g b(int var1, int var2, int var3, int var4) {
         return k.g.b;
      }
   }

   private static class b extends k {
      public float a(int var1, int var2, int var3, int var4) {
         byte var5 = 1;
         var1 = (int)Math.ceil((double)Math.max((float)var2 / (float)var4, (float)var1 / (float)var3));
         var2 = Math.max(1, Integer.highestOneBit(var1));
         byte var6;
         if(var2 < var1) {
            var6 = var5;
         } else {
            var6 = 0;
         }

         return 1.0F / (float)(var2 << var6);
      }

      public k.g b(int var1, int var2, int var3, int var4) {
         return k.g.a;
      }
   }

   private static class c extends k {
      public float a(int var1, int var2, int var3, int var4) {
         return Math.min(1.0F, a.a(var1, var2, var3, var4));
      }

      public k.g b(int var1, int var2, int var3, int var4) {
         return k.g.b;
      }
   }

   private static class d extends k {
      public float a(int var1, int var2, int var3, int var4) {
         return Math.max((float)var3 / (float)var1, (float)var4 / (float)var2);
      }

      public k.g b(int var1, int var2, int var3, int var4) {
         return k.g.b;
      }
   }

   private static class e extends k {
      public float a(int var1, int var2, int var3, int var4) {
         return Math.min((float)var3 / (float)var1, (float)var4 / (float)var2);
      }

      public k.g b(int var1, int var2, int var3, int var4) {
         return k.g.b;
      }
   }

   private static class f extends k {
      public float a(int var1, int var2, int var3, int var4) {
         return 1.0F;
      }

      public k.g b(int var1, int var2, int var3, int var4) {
         return k.g.b;
      }
   }

   public static enum g {
      a,
      b;
   }
}
