package io.intercom.com.bumptech.glide.load.resource.bitmap;

import android.graphics.Bitmap;
import java.io.IOException;
import java.io.InputStream;

public class p implements io.intercom.com.bumptech.glide.load.j {
   private final l a;
   private final io.intercom.com.bumptech.glide.load.engine.a.b b;

   public p(l var1, io.intercom.com.bumptech.glide.load.engine.a.b var2) {
      this.a = var1;
      this.b = var2;
   }

   public io.intercom.com.bumptech.glide.load.engine.r a(InputStream var1, int var2, int var3, io.intercom.com.bumptech.glide.load.i var4) throws IOException {
      boolean var5;
      RecyclableBufferedInputStream var11;
      if(var1 instanceof RecyclableBufferedInputStream) {
         var11 = (RecyclableBufferedInputStream)var1;
         var5 = false;
      } else {
         var11 = new RecyclableBufferedInputStream(var1, this.b);
         var5 = true;
      }

      io.intercom.com.bumptech.glide.h.c var6 = io.intercom.com.bumptech.glide.h.c.a(var11);
      io.intercom.com.bumptech.glide.h.f var7 = new io.intercom.com.bumptech.glide.h.f(var6);
      p.a var8 = new p.a(var11, var6);

      io.intercom.com.bumptech.glide.load.engine.r var12;
      try {
         var12 = this.a.a((InputStream)var7, var2, var3, (io.intercom.com.bumptech.glide.load.i)var4, (l.a)var8);
      } finally {
         var6.b();
         if(var5) {
            var11.b();
         }

      }

      return var12;
   }

   public boolean a(InputStream var1, io.intercom.com.bumptech.glide.load.i var2) throws IOException {
      return this.a.a(var1);
   }

   static class a implements l.a {
      private final RecyclableBufferedInputStream a;
      private final io.intercom.com.bumptech.glide.h.c b;

      public a(RecyclableBufferedInputStream var1, io.intercom.com.bumptech.glide.h.c var2) {
         this.a = var1;
         this.b = var2;
      }

      public void a() {
         this.a.a();
      }

      public void a(io.intercom.com.bumptech.glide.load.engine.a.e var1, Bitmap var2) throws IOException {
         IOException var3 = this.b.a();
         if(var3 != null) {
            if(var2 != null) {
               var1.a(var2);
            }

            throw var3;
         }
      }
   }
}
