package io.intercom.com.bumptech.glide.load.resource.d;

import io.intercom.com.bumptech.glide.load.j;
import io.intercom.com.bumptech.glide.load.engine.r;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;

public class i implements j {
   public static final io.intercom.com.bumptech.glide.load.h a = io.intercom.com.bumptech.glide.load.h.a((String)"io.intercom.com.bumptech.glide.load.resource.gif.ByteBufferGifDecoder.DisableAnimation", (Object)Boolean.valueOf(false));
   private final List b;
   private final j c;
   private final io.intercom.com.bumptech.glide.load.engine.a.b d;

   public i(List var1, j var2, io.intercom.com.bumptech.glide.load.engine.a.b var3) {
      this.b = var1;
      this.c = var2;
      this.d = var3;
   }

   private static byte[] a(InputStream param0) {
      // $FF: Couldn't be decompiled
   }

   public r a(InputStream var1, int var2, int var3, io.intercom.com.bumptech.glide.load.i var4) throws IOException {
      byte[] var5 = a(var1);
      r var6;
      if(var5 == null) {
         var6 = null;
      } else {
         ByteBuffer var7 = ByteBuffer.wrap(var5);
         var6 = this.c.a(var7, var2, var3, var4);
      }

      return var6;
   }

   public boolean a(InputStream var1, io.intercom.com.bumptech.glide.load.i var2) throws IOException {
      boolean var3;
      if(!((Boolean)var2.a(a)).booleanValue() && io.intercom.com.bumptech.glide.load.f.a(this.b, var1, this.d) == io.intercom.com.bumptech.glide.load.e.a.a) {
         var3 = true;
      } else {
         var3 = false;
      }

      return var3;
   }
}
