package io.intercom.com.bumptech.glide.load.resource.bitmap;

import android.graphics.Bitmap;

public class e implements io.intercom.com.bumptech.glide.load.engine.o, io.intercom.com.bumptech.glide.load.engine.r {
   private final Bitmap a;
   private final io.intercom.com.bumptech.glide.load.engine.a.e b;

   public e(Bitmap var1, io.intercom.com.bumptech.glide.load.engine.a.e var2) {
      this.a = (Bitmap)io.intercom.com.bumptech.glide.h.h.a(var1, "Bitmap must not be null");
      this.b = (io.intercom.com.bumptech.glide.load.engine.a.e)io.intercom.com.bumptech.glide.h.h.a(var2, "BitmapPool must not be null");
   }

   public static e a(Bitmap var0, io.intercom.com.bumptech.glide.load.engine.a.e var1) {
      e var2;
      if(var0 == null) {
         var2 = null;
      } else {
         var2 = new e(var0, var1);
      }

      return var2;
   }

   public void a() {
      this.a.prepareToDraw();
   }

   public Class b() {
      return Bitmap.class;
   }

   // $FF: synthetic method
   public Object c() {
      return this.f();
   }

   public int d() {
      return io.intercom.com.bumptech.glide.h.i.a(this.a);
   }

   public void e() {
      this.b.a(this.a);
   }

   public Bitmap f() {
      return this.a;
   }
}
