package io.intercom.com.bumptech.glide.load.resource.bitmap;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public class RecyclableBufferedInputStream extends FilterInputStream {
   private volatile byte[] a;
   private int b;
   private int c;
   private int d;
   private int e;
   private final io.intercom.com.bumptech.glide.load.engine.a.b f;

   public RecyclableBufferedInputStream(InputStream var1, io.intercom.com.bumptech.glide.load.engine.a.b var2) {
      this(var1, var2, 65536);
   }

   RecyclableBufferedInputStream(InputStream var1, io.intercom.com.bumptech.glide.load.engine.a.b var2, int var3) {
      super(var1);
      this.d = -1;
      this.f = var2;
      this.a = (byte[])var2.a(var3, byte[].class);
   }

   private int a(InputStream var1, byte[] var2) throws IOException {
      int var3;
      int var4;
      if(this.d != -1 && this.e - this.d < this.c) {
         byte[] var5;
         if(this.d == 0 && this.c > var2.length && this.b == var2.length) {
            var4 = var2.length * 2;
            var3 = var4;
            if(var4 > this.c) {
               var3 = this.c;
            }

            var5 = (byte[])this.f.a(var3, byte[].class);
            System.arraycopy(var2, 0, var5, 0, var2.length);
            this.a = var5;
            this.f.a(var2, byte[].class);
         } else {
            var5 = var2;
            if(this.d > 0) {
               System.arraycopy(var2, this.d, var2, 0, var2.length - this.d);
               var5 = var2;
            }
         }

         this.e -= this.d;
         this.d = 0;
         this.b = 0;
         var4 = var1.read(var5, this.e, var5.length - this.e);
         if(var4 <= 0) {
            var3 = this.e;
         } else {
            var3 = this.e + var4;
         }

         this.b = var3;
         var3 = var4;
      } else {
         var4 = var1.read(var2);
         var3 = var4;
         if(var4 > 0) {
            this.d = -1;
            this.e = 0;
            this.b = var4;
            var3 = var4;
         }
      }

      return var3;
   }

   private static IOException c() throws IOException {
      throw new IOException("BufferedInputStream is closed");
   }

   public void a() {
      synchronized(this){}

      try {
         this.c = this.a.length;
      } finally {
         ;
      }

   }

   public int available() throws IOException {
      // $FF: Couldn't be decompiled
   }

   public void b() {
      synchronized(this){}

      try {
         if(this.a != null) {
            this.f.a(this.a, byte[].class);
            this.a = null;
         }
      } finally {
         ;
      }

   }

   public void close() throws IOException {
      if(this.a != null) {
         this.f.a(this.a, byte[].class);
         this.a = null;
      }

      InputStream var1 = this.in;
      this.in = null;
      if(var1 != null) {
         var1.close();
      }

   }

   public void mark(int var1) {
      synchronized(this){}

      try {
         this.c = Math.max(this.c, var1);
         this.d = this.e;
      } finally {
         ;
      }

   }

   public boolean markSupported() {
      return true;
   }

   public int read() throws IOException {
      // $FF: Couldn't be decompiled
   }

   public int read(byte[] param1, int param2, int param3) throws IOException {
      // $FF: Couldn't be decompiled
   }

   public void reset() throws IOException {
      synchronized(this){}

      try {
         if(this.a == null) {
            IOException var5 = new IOException("Stream is closed");
            throw var5;
         }

         if(-1 == this.d) {
            StringBuilder var2 = new StringBuilder();
            RecyclableBufferedInputStream.InvalidMarkException var1 = new RecyclableBufferedInputStream.InvalidMarkException(var2.append("Mark has been invalidated, pos: ").append(this.e).append(" markLimit: ").append(this.c).toString());
            throw var1;
         }

         this.e = this.d;
      } finally {
         ;
      }

   }

   public long skip(long param1) throws IOException {
      // $FF: Couldn't be decompiled
   }

   public static class InvalidMarkException extends IOException {
      public InvalidMarkException(String var1) {
         super(var1);
      }
   }
}
