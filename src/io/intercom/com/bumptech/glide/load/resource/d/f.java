package io.intercom.com.bumptech.glide.load.resource.d;

import android.content.Context;
import android.graphics.Bitmap;
import io.intercom.com.bumptech.glide.load.l;
import io.intercom.com.bumptech.glide.load.engine.r;
import java.security.MessageDigest;

public class f implements l {
   private final l b;

   public f(l var1) {
      this.b = (l)io.intercom.com.bumptech.glide.h.h.a((Object)var1);
   }

   public boolean equals(Object var1) {
      boolean var2;
      if(var1 instanceof f) {
         f var3 = (f)var1;
         var2 = this.b.equals(var3.b);
      } else {
         var2 = false;
      }

      return var2;
   }

   public int hashCode() {
      return this.b.hashCode();
   }

   public r transform(Context var1, r var2, int var3, int var4) {
      c var5 = (c)var2.c();
      io.intercom.com.bumptech.glide.load.engine.a.e var6 = io.intercom.com.bumptech.glide.c.a(var1).a();
      io.intercom.com.bumptech.glide.load.resource.bitmap.e var7 = new io.intercom.com.bumptech.glide.load.resource.bitmap.e(var5.b(), var6);
      r var8 = this.b.transform(var1, var7, var3, var4);
      if(!var7.equals(var8)) {
         var7.e();
      }

      Bitmap var9 = (Bitmap)var8.c();
      var5.a(this.b, var9);
      return var2;
   }

   public void updateDiskCacheKey(MessageDigest var1) {
      this.b.updateDiskCacheKey(var1);
   }
}
