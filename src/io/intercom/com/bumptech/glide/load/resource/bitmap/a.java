package io.intercom.com.bumptech.glide.load.resource.bitmap;

import android.content.res.Resources;
import android.graphics.Bitmap;
import java.io.IOException;

public class a implements io.intercom.com.bumptech.glide.load.j {
   private final io.intercom.com.bumptech.glide.load.j a;
   private final Resources b;
   private final io.intercom.com.bumptech.glide.load.engine.a.e c;

   public a(Resources var1, io.intercom.com.bumptech.glide.load.engine.a.e var2, io.intercom.com.bumptech.glide.load.j var3) {
      this.b = (Resources)io.intercom.com.bumptech.glide.h.h.a((Object)var1);
      this.c = (io.intercom.com.bumptech.glide.load.engine.a.e)io.intercom.com.bumptech.glide.h.h.a((Object)var2);
      this.a = (io.intercom.com.bumptech.glide.load.j)io.intercom.com.bumptech.glide.h.h.a((Object)var3);
   }

   public io.intercom.com.bumptech.glide.load.engine.r a(Object var1, int var2, int var3, io.intercom.com.bumptech.glide.load.i var4) throws IOException {
      io.intercom.com.bumptech.glide.load.engine.r var5 = this.a.a(var1, var2, var3, var4);
      o var6;
      if(var5 == null) {
         var6 = null;
      } else {
         var6 = o.a(this.b, this.c, (Bitmap)var5.c());
      }

      return var6;
   }

   public boolean a(Object var1, io.intercom.com.bumptech.glide.load.i var2) throws IOException {
      return this.a.a(var1, var2);
   }
}
