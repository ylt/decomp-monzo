package io.intercom.com.bumptech.glide.load.resource.d;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Drawable.ConstantState;
import android.view.Gravity;
import io.intercom.com.bumptech.glide.load.l;
import java.nio.ByteBuffer;

public class c extends Drawable implements Animatable, g.b {
   private final c.a a;
   private boolean b;
   private boolean c;
   private boolean d;
   private boolean e;
   private int f;
   private int g;
   private boolean h;
   private Paint i;
   private Rect j;

   public c(Context var1, io.intercom.com.bumptech.glide.b.a var2, io.intercom.com.bumptech.glide.load.engine.a.e var3, l var4, int var5, int var6, Bitmap var7) {
      this(new c.a(var3, new g(io.intercom.com.bumptech.glide.c.a(var1), var2, var5, var6, var4, var7)));
   }

   c(c.a var1) {
      this.e = true;
      this.g = -1;
      this.a = (c.a)io.intercom.com.bumptech.glide.h.h.a((Object)var1);
   }

   private void h() {
      this.f = 0;
   }

   private void i() {
      boolean var1;
      if(!this.d) {
         var1 = true;
      } else {
         var1 = false;
      }

      io.intercom.com.bumptech.glide.h.h.a(var1, "You cannot start a recycled Drawable. Ensure thatyou clear any references to the Drawable when clearing the corresponding request.");
      if(this.a.b.g() == 1) {
         this.invalidateSelf();
      } else if(!this.b) {
         this.b = true;
         this.a.b.a((g.b)this);
         this.invalidateSelf();
      }

   }

   private void j() {
      this.b = false;
      this.a.b.b(this);
   }

   private Rect k() {
      if(this.j == null) {
         this.j = new Rect();
      }

      return this.j;
   }

   private Paint l() {
      if(this.i == null) {
         this.i = new Paint(2);
      }

      return this.i;
   }

   public int a() {
      return this.a.b.d();
   }

   public void a(l var1, Bitmap var2) {
      this.a.b.a(var1, var2);
   }

   public Bitmap b() {
      return this.a.b.a();
   }

   public ByteBuffer c() {
      return this.a.b.f();
   }

   public int d() {
      return this.a.b.g();
   }

   public void draw(Canvas var1) {
      if(!this.d) {
         if(this.h) {
            Gravity.apply(119, this.getIntrinsicWidth(), this.getIntrinsicHeight(), this.getBounds(), this.k());
            this.h = false;
         }

         var1.drawBitmap(this.a.b.i(), (Rect)null, this.k(), this.l());
      }

   }

   public int e() {
      return this.a.b.e();
   }

   public void f() {
      if(this.getCallback() == null) {
         this.stop();
         this.invalidateSelf();
      } else {
         this.invalidateSelf();
         if(this.e() == this.d() - 1) {
            ++this.f;
         }

         if(this.g != -1 && this.f >= this.g) {
            this.stop();
         }
      }

   }

   public void g() {
      this.d = true;
      this.a.b.h();
   }

   public ConstantState getConstantState() {
      return this.a;
   }

   public int getIntrinsicHeight() {
      return this.a.b.c();
   }

   public int getIntrinsicWidth() {
      return this.a.b.b();
   }

   public int getOpacity() {
      return -2;
   }

   public boolean isRunning() {
      return this.b;
   }

   protected void onBoundsChange(Rect var1) {
      super.onBoundsChange(var1);
      this.h = true;
   }

   public void setAlpha(int var1) {
      this.l().setAlpha(var1);
   }

   public void setColorFilter(ColorFilter var1) {
      this.l().setColorFilter(var1);
   }

   public boolean setVisible(boolean var1, boolean var2) {
      boolean var3;
      if(!this.d) {
         var3 = true;
      } else {
         var3 = false;
      }

      io.intercom.com.bumptech.glide.h.h.a(var3, "Cannot change the visibility of a recycled resource. Ensure that you unset the Drawable from your View before changing the View's visibility.");
      this.e = var1;
      if(!var1) {
         this.j();
      } else if(this.c) {
         this.i();
      }

      return super.setVisible(var1, var2);
   }

   public void start() {
      this.c = true;
      this.h();
      if(this.e) {
         this.i();
      }

   }

   public void stop() {
      this.c = false;
      this.j();
   }

   static class a extends ConstantState {
      final io.intercom.com.bumptech.glide.load.engine.a.e a;
      final g b;

      public a(io.intercom.com.bumptech.glide.load.engine.a.e var1, g var2) {
         this.a = var1;
         this.b = var2;
      }

      public int getChangingConfigurations() {
         return 0;
      }

      public Drawable newDrawable() {
         return new c(this);
      }

      public Drawable newDrawable(Resources var1) {
         return this.newDrawable();
      }
   }
}
