package io.intercom.com.bumptech.glide.load.resource.d;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import io.intercom.com.bumptech.glide.load.j;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Queue;

public class a implements j {
   public static final io.intercom.com.bumptech.glide.load.h a = io.intercom.com.bumptech.glide.load.h.a((String)"io.intercom.com.bumptech.glide.load.resource.gif.ByteBufferGifDecoder.DisableAnimation", (Object)Boolean.valueOf(false));
   private static final a.a b = new a.a();
   private static final a.b c = new a.b();
   private final Context d;
   private final List e;
   private final a.b f;
   private final io.intercom.com.bumptech.glide.load.engine.a.e g;
   private final a.a h;
   private final b i;

   public a(Context var1, List var2, io.intercom.com.bumptech.glide.load.engine.a.e var3, io.intercom.com.bumptech.glide.load.engine.a.b var4) {
      this(var1, var2, var3, var4, c, b);
   }

   a(Context var1, List var2, io.intercom.com.bumptech.glide.load.engine.a.e var3, io.intercom.com.bumptech.glide.load.engine.a.b var4, a.b var5, a.a var6) {
      this.d = var1.getApplicationContext();
      this.e = var2;
      this.g = var3;
      this.h = var6;
      this.i = new b(var3, var4);
      this.f = var5;
   }

   private static int a(io.intercom.com.bumptech.glide.b.c var0, int var1, int var2) {
      int var3 = Math.min(var0.a() / var2, var0.b() / var1);
      if(var3 == 0) {
         var3 = 0;
      } else {
         var3 = Integer.highestOneBit(var3);
      }

      var3 = Math.max(1, var3);
      if(Log.isLoggable("BufferGifDecoder", 2)) {
         Log.v("BufferGifDecoder", "Downsampling GIF, sampleSize: " + var3 + ", target dimens: [" + var1 + "x" + var2 + "], actual dimens: [" + var0.b() + "x" + var0.a() + "]");
      }

      return var3;
   }

   private e a(ByteBuffer var1, int var2, int var3, io.intercom.com.bumptech.glide.b.d var4) {
      Object var8 = null;
      long var6 = io.intercom.com.bumptech.glide.h.d.a();
      io.intercom.com.bumptech.glide.b.c var9 = var4.b();
      e var12 = (e)var8;
      if(var9.c() > 0) {
         if(var9.d() != 0) {
            var12 = (e)var8;
         } else {
            int var5 = a(var9, var2, var3);
            io.intercom.com.bumptech.glide.b.a var10 = this.h.a(this.i, var9, var1, var5);
            var10.b();
            Bitmap var14 = var10.h();
            var12 = (e)var8;
            if(var14 != null) {
               io.intercom.com.bumptech.glide.load.resource.b var13 = io.intercom.com.bumptech.glide.load.resource.b.a();
               c var11 = new c(this.d, var10, this.g, var13, var2, var3, var14);
               if(Log.isLoggable("BufferGifDecoder", 2)) {
                  Log.v("BufferGifDecoder", "Decoded GIF from stream in " + io.intercom.com.bumptech.glide.h.d.a(var6));
               }

               var12 = new e(var11);
            }
         }
      }

      return var12;
   }

   public e a(ByteBuffer var1, int var2, int var3, io.intercom.com.bumptech.glide.load.i var4) {
      io.intercom.com.bumptech.glide.b.d var8 = this.f.a(var1);

      e var7;
      try {
         var7 = this.a(var1, var2, var3, var8);
      } finally {
         this.f.a(var8);
      }

      return var7;
   }

   public boolean a(ByteBuffer var1, io.intercom.com.bumptech.glide.load.i var2) throws IOException {
      boolean var3;
      if(!((Boolean)var2.a(a)).booleanValue() && io.intercom.com.bumptech.glide.load.f.a(this.e, var1) == io.intercom.com.bumptech.glide.load.e.a.a) {
         var3 = true;
      } else {
         var3 = false;
      }

      return var3;
   }

   static class a {
      public io.intercom.com.bumptech.glide.b.a a(io.intercom.com.bumptech.glide.b.a.a var1, io.intercom.com.bumptech.glide.b.c var2, ByteBuffer var3, int var4) {
         return new io.intercom.com.bumptech.glide.b.e(var1, var2, var3, var4);
      }
   }

   static class b {
      private final Queue a = io.intercom.com.bumptech.glide.h.i.a(0);

      public io.intercom.com.bumptech.glide.b.d a(ByteBuffer param1) {
         // $FF: Couldn't be decompiled
      }

      public void a(io.intercom.com.bumptech.glide.b.d var1) {
         synchronized(this){}

         try {
            var1.a();
            this.a.offer(var1);
         } finally {
            ;
         }

      }
   }
}
