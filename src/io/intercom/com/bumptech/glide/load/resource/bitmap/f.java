package io.intercom.com.bumptech.glide.load.resource.bitmap;

import android.content.Context;
import android.graphics.Bitmap;

public abstract class f implements io.intercom.com.bumptech.glide.load.l {
   public f() {
   }

   @Deprecated
   public f(Context var1) {
      this();
   }

   @Deprecated
   public f(io.intercom.com.bumptech.glide.load.engine.a.e var1) {
      this();
   }

   protected abstract Bitmap transform(io.intercom.com.bumptech.glide.load.engine.a.e var1, Bitmap var2, int var3, int var4);

   public final io.intercom.com.bumptech.glide.load.engine.r transform(Context var1, io.intercom.com.bumptech.glide.load.engine.r var2, int var3, int var4) {
      if(!io.intercom.com.bumptech.glide.h.i.a(var3, var4)) {
         throw new IllegalArgumentException("Cannot apply transformation on width: " + var3 + " or height: " + var4 + " less than or equal to zero and not Target.SIZE_ORIGINAL");
      } else {
         io.intercom.com.bumptech.glide.load.engine.a.e var8 = io.intercom.com.bumptech.glide.c.a(var1).a();
         Bitmap var6 = (Bitmap)((io.intercom.com.bumptech.glide.load.engine.r)var2).c();
         int var5 = var3;
         if(var3 == Integer.MIN_VALUE) {
            var5 = var6.getWidth();
         }

         var3 = var4;
         if(var4 == Integer.MIN_VALUE) {
            var3 = var6.getHeight();
         }

         Bitmap var7 = this.transform(var8, var6, var5, var3);
         if(!var6.equals(var7)) {
            var2 = e.a(var7, var8);
         }

         return (io.intercom.com.bumptech.glide.load.engine.r)var2;
      }
   }
}
