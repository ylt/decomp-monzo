package io.intercom.com.bumptech.glide.load.resource.d;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;

public final class b implements io.intercom.com.bumptech.glide.b.a.a {
   private final io.intercom.com.bumptech.glide.load.engine.a.e a;
   private final io.intercom.com.bumptech.glide.load.engine.a.b b;

   public b(io.intercom.com.bumptech.glide.load.engine.a.e var1, io.intercom.com.bumptech.glide.load.engine.a.b var2) {
      this.a = var1;
      this.b = var2;
   }

   public Bitmap a(int var1, int var2, Config var3) {
      return this.a.b(var1, var2, var3);
   }

   public void a(Bitmap var1) {
      this.a.a(var1);
   }

   public void a(byte[] var1) {
      if(this.b != null) {
         this.b.a(var1, byte[].class);
      }

   }

   public void a(int[] var1) {
      if(this.b != null) {
         this.b.a(var1, int[].class);
      }

   }

   public byte[] a(int var1) {
      byte[] var2;
      if(this.b == null) {
         var2 = new byte[var1];
      } else {
         var2 = (byte[])this.b.a(var1, byte[].class);
      }

      return var2;
   }

   public int[] b(int var1) {
      int[] var2;
      if(this.b == null) {
         var2 = new int[var1];
      } else {
         var2 = (int[])this.b.a(var1, int[].class);
      }

      return var2;
   }
}
