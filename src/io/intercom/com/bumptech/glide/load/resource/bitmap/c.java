package io.intercom.com.bumptech.glide.load.resource.bitmap;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import java.security.MessageDigest;

public class c implements io.intercom.com.bumptech.glide.load.l {
   private final io.intercom.com.bumptech.glide.load.l b;

   public c(io.intercom.com.bumptech.glide.load.l var1) {
      this.b = (io.intercom.com.bumptech.glide.load.l)io.intercom.com.bumptech.glide.h.h.a((Object)var1);
   }

   public boolean equals(Object var1) {
      boolean var2;
      if(var1 instanceof c) {
         c var3 = (c)var1;
         var2 = this.b.equals(var3.b);
      } else {
         var2 = false;
      }

      return var2;
   }

   public int hashCode() {
      return this.b.hashCode();
   }

   public io.intercom.com.bumptech.glide.load.engine.r transform(Context var1, io.intercom.com.bumptech.glide.load.engine.r var2, int var3, int var4) {
      e var6 = e.a(((BitmapDrawable)((io.intercom.com.bumptech.glide.load.engine.r)var2).c()).getBitmap(), io.intercom.com.bumptech.glide.c.a(var1).a());
      io.intercom.com.bumptech.glide.load.engine.r var5 = this.b.transform(var1, var6, var3, var4);
      if(!var5.equals(var6)) {
         var2 = o.a(var1, (Bitmap)var5.c());
      }

      return (io.intercom.com.bumptech.glide.load.engine.r)var2;
   }

   public void updateDiskCacheKey(MessageDigest var1) {
      this.b.updateDiskCacheKey(var1);
   }
}
