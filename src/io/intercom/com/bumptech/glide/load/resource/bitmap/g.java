package io.intercom.com.bumptech.glide.load.resource.bitmap;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

public class g implements io.intercom.com.bumptech.glide.load.j {
   private final l a;

   public g(l var1) {
      this.a = var1;
   }

   public io.intercom.com.bumptech.glide.load.engine.r a(ByteBuffer var1, int var2, int var3, io.intercom.com.bumptech.glide.load.i var4) throws IOException {
      InputStream var5 = io.intercom.com.bumptech.glide.h.a.b(var1);
      return this.a.a(var5, var2, var3, var4);
   }

   public boolean a(ByteBuffer var1, io.intercom.com.bumptech.glide.load.i var2) throws IOException {
      return this.a.a(var1);
   }
}
