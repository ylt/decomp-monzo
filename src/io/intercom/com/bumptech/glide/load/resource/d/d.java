package io.intercom.com.bumptech.glide.load.resource.d;

import android.util.Log;
import io.intercom.com.bumptech.glide.load.k;
import io.intercom.com.bumptech.glide.load.engine.r;
import java.io.File;
import java.io.IOException;

public class d implements k {
   public io.intercom.com.bumptech.glide.load.c a(io.intercom.com.bumptech.glide.load.i var1) {
      return io.intercom.com.bumptech.glide.load.c.a;
   }

   public boolean a(r var1, File var2, io.intercom.com.bumptech.glide.load.i var3) {
      c var6 = (c)var1.c();

      boolean var4;
      try {
         io.intercom.com.bumptech.glide.h.a.a(var6.c(), var2);
      } catch (IOException var5) {
         if(Log.isLoggable("GifEncoder", 5)) {
            Log.w("GifEncoder", "Failed to encode GIF drawable data", var5);
         }

         var4 = false;
         return var4;
      }

      var4 = true;
      return var4;
   }
}
