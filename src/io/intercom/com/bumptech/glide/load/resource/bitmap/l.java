package io.intercom.com.bumptech.glide.load.resource.bitmap;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory.Options;
import android.os.Build.VERSION;
import android.util.DisplayMetrics;
import android.util.Log;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Queue;
import java.util.Set;

public final class l {
   public static final io.intercom.com.bumptech.glide.load.h a;
   public static final io.intercom.com.bumptech.glide.load.h b;
   public static final io.intercom.com.bumptech.glide.load.h c;
   public static final io.intercom.com.bumptech.glide.load.h d;
   private static final Set e;
   private static final l.a f;
   private static final Set g;
   private static final Queue h;
   private final io.intercom.com.bumptech.glide.load.engine.a.e i;
   private final DisplayMetrics j;
   private final io.intercom.com.bumptech.glide.load.engine.a.b k;
   private final List l;
   private final n m = n.a();

   static {
      a = io.intercom.com.bumptech.glide.load.h.a((String)"io.intercom.com.bumptech.glide.load.resource.bitmap.Downsampler.DecodeFormat", (Object)io.intercom.com.bumptech.glide.load.b.d);
      b = io.intercom.com.bumptech.glide.load.h.a((String)"io.intercom.com.bumptech.glide.load.resource.bitmap.Downsampler.DownsampleStrategy", (Object)k.c);
      c = io.intercom.com.bumptech.glide.load.h.a((String)"io.intercom.com.bumptech.glide.load.resource.bitmap.Downsampler.FixBitmapSize", (Object)Boolean.valueOf(false));
      d = io.intercom.com.bumptech.glide.load.h.a((String)"com.bumtpech.glide.load.resource.bitmap.Downsampler.AllowHardwareDecode", (Object)null);
      e = Collections.unmodifiableSet(new HashSet(Arrays.asList(new String[]{"image/vnd.wap.wbmp", "image/x-ico"})));
      f = new l.a() {
         public void a() {
         }

         public void a(io.intercom.com.bumptech.glide.load.engine.a.e var1, Bitmap var2) throws IOException {
         }
      };
      g = Collections.unmodifiableSet(EnumSet.of(io.intercom.com.bumptech.glide.load.e.a.b, io.intercom.com.bumptech.glide.load.e.a.d, io.intercom.com.bumptech.glide.load.e.a.e));
      h = io.intercom.com.bumptech.glide.h.i.a(0);
   }

   public l(List var1, DisplayMetrics var2, io.intercom.com.bumptech.glide.load.engine.a.e var3, io.intercom.com.bumptech.glide.load.engine.a.b var4) {
      this.l = var1;
      this.j = (DisplayMetrics)io.intercom.com.bumptech.glide.h.h.a((Object)var2);
      this.i = (io.intercom.com.bumptech.glide.load.engine.a.e)io.intercom.com.bumptech.glide.h.h.a((Object)var3);
      this.k = (io.intercom.com.bumptech.glide.load.engine.a.b)io.intercom.com.bumptech.glide.h.h.a((Object)var4);
   }

   private Bitmap a(InputStream var1, Options var2, k var3, io.intercom.com.bumptech.glide.load.b var4, boolean var5, int var6, int var7, boolean var8, l.a var9) throws IOException {
      int[] var20 = a(var1, var2, var9, this.i);
      int var15 = var20[0];
      int var14 = var20[1];
      String var25 = var2.outMimeType;
      int var17 = io.intercom.com.bumptech.glide.load.f.b(this.l, var1, this.k);
      int var13 = q.a(var17);
      boolean var19 = q.b(var17);
      int var11;
      if(var6 == Integer.MIN_VALUE) {
         var11 = var15;
      } else {
         var11 = var6;
      }

      int var12;
      if(var7 == Integer.MIN_VALUE) {
         var12 = var14;
      } else {
         var12 = var7;
      }

      a(var3, var13, var15, var14, var11, var12, var2);
      this.a(var1, var4, var5, var19, var2, var11, var12);
      boolean var24;
      if(VERSION.SDK_INT >= 19) {
         var24 = true;
      } else {
         var24 = false;
      }

      if((var2.inSampleSize == 1 || var24) && this.b(var1)) {
         if(!var8 || !var24) {
            float var10;
            if(a(var2)) {
               var10 = (float)var2.inTargetDensity / (float)var2.inDensity;
            } else {
               var10 = 1.0F;
            }

            int var18 = var2.inSampleSize;
            var12 = (int)Math.ceil((double)((float)var15 / (float)var18));
            var11 = (int)Math.ceil((double)((float)var14 / (float)var18));
            var13 = Math.round((float)var12 * var10);
            int var16 = Math.round((float)var11 * var10);
            var11 = var13;
            var12 = var16;
            if(Log.isLoggable("Downsampler", 2)) {
               Log.v("Downsampler", "Calculated target [" + var13 + "x" + var16 + "] for source [" + var15 + "x" + var14 + "], sampleSize: " + var18 + ", targetDensity: " + var2.inTargetDensity + ", density: " + var2.inDensity + ", density multiplier: " + var10);
               var11 = var13;
               var12 = var16;
            }
         }

         if(var11 > 0 && var12 > 0) {
            a(var2, this.i, var11, var12);
         }
      }

      Bitmap var23 = b(var1, var2, var9, this.i);
      var9.a(this.i, var23);
      if(Log.isLoggable("Downsampler", 2)) {
         a(var15, var14, var25, var2, var23, var6, var7);
      }

      Bitmap var21 = null;
      if(var23 != null) {
         var23.setDensity(this.j.densityDpi);
         Bitmap var22 = q.a(this.i, var23, var17);
         var21 = var22;
         if(!var23.equals(var22)) {
            this.i.a(var23);
            var21 = var22;
         }
      }

      return var21;
   }

   private static Options a() {
      // $FF: Couldn't be decompiled
   }

   private static IOException a(IllegalArgumentException var0, int var1, int var2, String var3, Options var4) {
      return new IOException("Exception decoding bitmap, outWidth: " + var1 + ", outHeight: " + var2 + ", outMimeType: " + var3 + ", inBitmap: " + b(var4), var0);
   }

   @TargetApi(19)
   private static String a(Bitmap var0) {
      String var2;
      if(var0 == null) {
         var2 = null;
      } else {
         String var1;
         if(VERSION.SDK_INT >= 19) {
            var1 = " (" + var0.getAllocationByteCount() + ")";
         } else {
            var1 = "";
         }

         var2 = "[" + var0.getWidth() + "x" + var0.getHeight() + "] " + var0.getConfig() + var1;
      }

      return var2;
   }

   private static void a(int var0, int var1, String var2, Options var3, Bitmap var4, int var5, int var6) {
      Log.v("Downsampler", "Decoded " + a(var4) + " from [" + var0 + "x" + var1 + "] " + var2 + " with inBitmap " + b(var3) + " for [" + var5 + "x" + var6 + "], sample size: " + var3.inSampleSize + ", density: " + var3.inDensity + ", target density: " + var3.inTargetDensity + ", thread: " + Thread.currentThread().getName());
   }

   @TargetApi(26)
   private static void a(Options var0, io.intercom.com.bumptech.glide.load.engine.a.e var1, int var2, int var3) {
      if(VERSION.SDK_INT < 26 || var0.inPreferredConfig != Config.HARDWARE) {
         var0.inBitmap = var1.b(var2, var3, var0.inPreferredConfig);
      }

   }

   static void a(k var0, int var1, int var2, int var3, int var4, int var5, Options var6) {
      if(var2 > 0 && var3 > 0) {
         float var7;
         if(var1 != 90 && var1 != 270) {
            var7 = var0.a(var2, var3, var4, var5);
         } else {
            var7 = var0.a(var3, var2, var4, var5);
         }

         if(var7 <= 0.0F) {
            throw new IllegalArgumentException("Cannot scale with factor: " + var7 + " from: " + var0);
         }

         k.g var10 = var0.b(var2, var3, var4, var5);
         if(var10 == null) {
            throw new IllegalArgumentException("Cannot round with null rounding");
         }

         int var9 = (int)((float)var2 * var7 + 0.5F);
         var1 = (int)((float)var3 * var7 + 0.5F);
         var9 = var2 / var9;
         var1 = var3 / var1;
         if(var10 == k.g.a) {
            var1 = Math.max(var9, var1);
         } else {
            var1 = Math.min(var9, var1);
         }

         if(VERSION.SDK_INT <= 23 && e.contains(var6.outMimeType)) {
            var1 = 1;
         } else {
            var9 = Math.max(1, Integer.highestOneBit(var1));
            var1 = var9;
            if(var10 == k.g.a) {
               var1 = var9;
               if((float)var9 < 1.0F / var7) {
                  var1 = var9 << 1;
               }
            }
         }

         float var8 = (float)var1 * var7;
         var6.inSampleSize = var1;
         if(VERSION.SDK_INT >= 19) {
            var6.inTargetDensity = (int)(1000.0F * var8 + 0.5F);
            var6.inDensity = 1000;
         }

         if(a(var6)) {
            var6.inScaled = true;
         } else {
            var6.inTargetDensity = 0;
            var6.inDensity = 0;
         }

         if(Log.isLoggable("Downsampler", 2)) {
            Log.v("Downsampler", "Calculate scaling, source: [" + var2 + "x" + var3 + "], target: [" + var4 + "x" + var5 + "], exact scale factor: " + var7 + ", power of 2 sample size: " + var1 + ", adjusted scale factor: " + var8 + ", target density: " + var6.inTargetDensity + ", density: " + var6.inDensity);
         }
      }

   }

   private void a(InputStream var1, io.intercom.com.bumptech.glide.load.b var2, boolean var3, boolean var4, Options var5, int var6, int var7) throws IOException {
      if(!this.m.a(var6, var7, var5, var2, var3, var4)) {
         if(var2 != io.intercom.com.bumptech.glide.load.b.a && var2 != io.intercom.com.bumptech.glide.load.b.b && VERSION.SDK_INT != 16) {
            try {
               var3 = io.intercom.com.bumptech.glide.load.f.a(this.l, var1, this.k).a();
            } catch (IOException var8) {
               if(Log.isLoggable("Downsampler", 3)) {
                  Log.d("Downsampler", "Cannot determine whether the image has alpha or not from header, format " + var2, var8);
               }

               var3 = false;
            }

            Config var9;
            if(var3) {
               var9 = Config.ARGB_8888;
            } else {
               var9 = Config.RGB_565;
            }

            var5.inPreferredConfig = var9;
            if(var5.inPreferredConfig == Config.RGB_565 || var5.inPreferredConfig == Config.ARGB_4444 || var5.inPreferredConfig == Config.ALPHA_8) {
               var5.inDither = true;
            }
         } else {
            var5.inPreferredConfig = Config.ARGB_8888;
         }
      }

   }

   private static boolean a(Options var0) {
      boolean var1;
      if(var0.inTargetDensity > 0 && var0.inDensity > 0 && var0.inTargetDensity != var0.inDensity) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   private static int[] a(InputStream var0, Options var1, l.a var2, io.intercom.com.bumptech.glide.load.engine.a.e var3) throws IOException {
      var1.inJustDecodeBounds = true;
      b(var0, var1, var2, var3);
      var1.inJustDecodeBounds = false;
      return new int[]{var1.outWidth, var1.outHeight};
   }

   private static Bitmap b(InputStream param0, Options param1, l.a param2, io.intercom.com.bumptech.glide.load.engine.a.e param3) throws IOException {
      // $FF: Couldn't be decompiled
   }

   private static String b(Options var0) {
      return a(var0.inBitmap);
   }

   private boolean b(InputStream var1) throws IOException {
      boolean var2;
      if(VERSION.SDK_INT >= 19) {
         var2 = true;
      } else {
         try {
            io.intercom.com.bumptech.glide.load.e.a var4 = io.intercom.com.bumptech.glide.load.f.a(this.l, var1, this.k);
            var2 = g.contains(var4);
         } catch (IOException var3) {
            if(Log.isLoggable("Downsampler", 3)) {
               Log.d("Downsampler", "Cannot determine the image type from header", var3);
            }

            var2 = false;
         }
      }

      return var2;
   }

   private static void c(Options param0) {
      // $FF: Couldn't be decompiled
   }

   private static void d(Options var0) {
      var0.inTempStorage = null;
      var0.inDither = false;
      var0.inScaled = false;
      var0.inSampleSize = 1;
      var0.inPreferredConfig = null;
      var0.inJustDecodeBounds = false;
      var0.inDensity = 0;
      var0.inTargetDensity = 0;
      var0.outWidth = 0;
      var0.outHeight = 0;
      var0.outMimeType = null;
      var0.inBitmap = null;
      var0.inMutable = true;
   }

   public io.intercom.com.bumptech.glide.load.engine.r a(InputStream var1, int var2, int var3, io.intercom.com.bumptech.glide.load.i var4) throws IOException {
      return this.a(var1, var2, var3, var4, f);
   }

   public io.intercom.com.bumptech.glide.load.engine.r a(InputStream var1, int var2, int var3, io.intercom.com.bumptech.glide.load.i var4, l.a var5) throws IOException {
      io.intercom.com.bumptech.glide.h.h.a(var1.markSupported(), "You must provide an InputStream that supports mark()");
      byte[] var8 = (byte[])this.k.a(65536, byte[].class);
      Options var9 = a();
      var9.inTempStorage = var8;
      io.intercom.com.bumptech.glide.load.b var11 = (io.intercom.com.bumptech.glide.load.b)var4.a(a);
      k var10 = (k)var4.a(b);
      boolean var7 = ((Boolean)var4.a(c)).booleanValue();
      boolean var6;
      if(var4.a(d) != null && ((Boolean)var4.a(d)).booleanValue()) {
         var6 = true;
      } else {
         var6 = false;
      }

      if(var11 == io.intercom.com.bumptech.glide.load.b.b) {
         var6 = false;
      }

      e var14;
      try {
         var14 = e.a(this.a(var1, var9, var10, var11, var6, var2, var3, var7, var5), this.i);
      } finally {
         c(var9);
         this.k.a(var8, byte[].class);
      }

      return var14;
   }

   public boolean a(InputStream var1) {
      return true;
   }

   public boolean a(ByteBuffer var1) {
      return true;
   }

   public interface a {
      void a();

      void a(io.intercom.com.bumptech.glide.load.engine.a.e var1, Bitmap var2) throws IOException;
   }
}
