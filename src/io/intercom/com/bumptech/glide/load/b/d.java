package io.intercom.com.bumptech.glide.load.b;

import android.util.Log;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;

public class d implements m {
   public m.a a(File var1, int var2, int var3, io.intercom.com.bumptech.glide.load.i var4) {
      return new m.a(new io.intercom.com.bumptech.glide.g.b(var1), new d.a(var1));
   }

   public boolean a(File var1) {
      return true;
   }

   private static class a implements io.intercom.com.bumptech.glide.load.a.b {
      private final File a;

      public a(File var1) {
         this.a = var1;
      }

      public void a() {
      }

      public void a(io.intercom.com.bumptech.glide.g var1, io.intercom.com.bumptech.glide.load.a.b.a var2) {
         ByteBuffer var4;
         try {
            var4 = io.intercom.com.bumptech.glide.h.a.a(this.a);
         } catch (IOException var3) {
            if(Log.isLoggable("ByteBufferFileLoader", 3)) {
               Log.d("ByteBufferFileLoader", "Failed to obtain ByteBuffer for file", var3);
            }

            var2.a((Exception)var3);
            return;
         }

         var2.a((Object)var4);
      }

      public void b() {
      }

      public io.intercom.com.bumptech.glide.load.a c() {
         return io.intercom.com.bumptech.glide.load.a.a;
      }

      public Class d() {
         return ByteBuffer.class;
      }
   }

   public static class b implements n {
      public m a(q var1) {
         return new d();
      }
   }
}
