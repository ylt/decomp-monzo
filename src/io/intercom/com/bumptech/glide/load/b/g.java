package io.intercom.com.bumptech.glide.load.b;

import android.net.Uri;
import android.text.TextUtils;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.util.Map;

public class g implements io.intercom.com.bumptech.glide.load.g {
   private final h b;
   private final URL c;
   private final String d;
   private String e;
   private URL f;
   private volatile byte[] g;
   private int h;

   public g(String var1) {
      this(var1, h.b);
   }

   public g(String var1, h var2) {
      this.c = null;
      this.d = io.intercom.com.bumptech.glide.h.h.a(var1);
      this.b = (h)io.intercom.com.bumptech.glide.h.h.a((Object)var2);
   }

   public g(URL var1) {
      this(var1, h.b);
   }

   public g(URL var1, h var2) {
      this.c = (URL)io.intercom.com.bumptech.glide.h.h.a((Object)var1);
      this.d = null;
      this.b = (h)io.intercom.com.bumptech.glide.h.h.a((Object)var2);
   }

   private URL d() throws MalformedURLException {
      if(this.f == null) {
         this.f = new URL(this.e());
      }

      return this.f;
   }

   private String e() {
      if(TextUtils.isEmpty(this.e)) {
         String var2 = this.d;
         String var1 = var2;
         if(TextUtils.isEmpty(var2)) {
            var1 = this.c.toString();
         }

         this.e = Uri.encode(var1, "@#&=*+-_.,:!?()/~'%");
      }

      return this.e;
   }

   private byte[] f() {
      if(this.g == null) {
         this.g = this.c().getBytes(a);
      }

      return this.g;
   }

   public URL a() throws MalformedURLException {
      return this.d();
   }

   public Map b() {
      return this.b.a();
   }

   public String c() {
      String var1;
      if(this.d != null) {
         var1 = this.d;
      } else {
         var1 = this.c.toString();
      }

      return var1;
   }

   public boolean equals(Object var1) {
      boolean var3 = false;
      boolean var2 = var3;
      if(var1 instanceof g) {
         g var4 = (g)var1;
         var2 = var3;
         if(this.c().equals(var4.c())) {
            var2 = var3;
            if(this.b.equals(var4.b)) {
               var2 = true;
            }
         }
      }

      return var2;
   }

   public int hashCode() {
      if(this.h == 0) {
         this.h = this.c().hashCode();
         this.h = this.h * 31 + this.b.hashCode();
      }

      return this.h;
   }

   public String toString() {
      return this.c();
   }

   public void updateDiskCacheKey(MessageDigest var1) {
      var1.update(this.f());
   }
}
