package io.intercom.com.bumptech.glide.load.b;

import android.util.Log;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;

public class c implements io.intercom.com.bumptech.glide.load.d {
   public boolean a(ByteBuffer var1, File var2, io.intercom.com.bumptech.glide.load.i var3) {
      boolean var5 = false;

      boolean var4;
      try {
         io.intercom.com.bumptech.glide.h.a.a(var1, var2);
      } catch (IOException var6) {
         var4 = var5;
         if(Log.isLoggable("ByteBufferEncoder", 3)) {
            Log.d("ByteBufferEncoder", "Failed to write data", var6);
            var4 = var5;
         }

         return var4;
      }

      var4 = true;
      return var4;
   }
}
