package io.intercom.com.bumptech.glide.load.b.a;

import android.content.Context;
import android.net.Uri;
import io.intercom.com.bumptech.glide.load.i;
import io.intercom.com.bumptech.glide.load.b.m;
import io.intercom.com.bumptech.glide.load.b.n;
import io.intercom.com.bumptech.glide.load.b.q;
import io.intercom.com.bumptech.glide.load.resource.bitmap.r;

public class d implements m {
   private final Context a;

   d(Context var1) {
      this.a = var1.getApplicationContext();
   }

   private boolean a(i var1) {
      Long var3 = (Long)var1.a(r.a);
      boolean var2;
      if(var3 != null && var3.longValue() == -1L) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   public m.a a(Uri var1, int var2, int var3, i var4) {
      m.a var5;
      if(io.intercom.com.bumptech.glide.load.a.a.b.a(var2, var3) && this.a(var4)) {
         var5 = new m.a(new io.intercom.com.bumptech.glide.g.b(var1), io.intercom.com.bumptech.glide.load.a.a.c.b(this.a, var1));
      } else {
         var5 = null;
      }

      return var5;
   }

   public boolean a(Uri var1) {
      return io.intercom.com.bumptech.glide.load.a.a.b.b(var1);
   }

   public static class a implements n {
      private final Context a;

      public a(Context var1) {
         this.a = var1;
      }

      public m a(q var1) {
         return new d(this.a);
      }
   }
}
