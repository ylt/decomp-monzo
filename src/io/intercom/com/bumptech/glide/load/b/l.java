package io.intercom.com.bumptech.glide.load.b;

import java.util.Queue;

public class l {
   private final io.intercom.com.bumptech.glide.h.e a;

   public l() {
      this(250);
   }

   public l(int var1) {
      this.a = new io.intercom.com.bumptech.glide.h.e(var1) {
         protected void a(l.a var1, Object var2) {
            var1.a();
         }
      };
   }

   public Object a(Object var1, int var2, int var3) {
      l.a var4 = l.a.a(var1, var2, var3);
      var1 = this.a.b(var4);
      var4.a();
      return var1;
   }

   public void a(Object var1, int var2, int var3, Object var4) {
      l.a var5 = l.a.a(var1, var2, var3);
      this.a.b(var5, var4);
   }

   static final class a {
      private static final Queue a = io.intercom.com.bumptech.glide.h.i.a(0);
      private int b;
      private int c;
      private Object d;

      static l.a a(Object param0, int param1, int param2) {
         // $FF: Couldn't be decompiled
      }

      private void b(Object var1, int var2, int var3) {
         this.d = var1;
         this.c = var2;
         this.b = var3;
      }

      public void a() {
         // $FF: Couldn't be decompiled
      }

      public boolean equals(Object var1) {
         boolean var3 = false;
         boolean var2 = var3;
         if(var1 instanceof l.a) {
            l.a var4 = (l.a)var1;
            var2 = var3;
            if(this.c == var4.c) {
               var2 = var3;
               if(this.b == var4.b) {
                  var2 = var3;
                  if(this.d.equals(var4.d)) {
                     var2 = true;
                  }
               }
            }
         }

         return var2;
      }

      public int hashCode() {
         return (this.b * 31 + this.c) * 31 + this.d.hashCode();
      }
   }
}
