package io.intercom.com.bumptech.glide.load.b.a;

import android.content.Context;
import android.net.Uri;
import io.intercom.com.bumptech.glide.load.i;
import io.intercom.com.bumptech.glide.load.b.m;
import io.intercom.com.bumptech.glide.load.b.n;
import io.intercom.com.bumptech.glide.load.b.q;

public class c implements m {
   public final Context a;

   public c(Context var1) {
      this.a = var1.getApplicationContext();
   }

   public m.a a(Uri var1, int var2, int var3, i var4) {
      m.a var5;
      if(io.intercom.com.bumptech.glide.load.a.a.b.a(var2, var3)) {
         var5 = new m.a(new io.intercom.com.bumptech.glide.g.b(var1), io.intercom.com.bumptech.glide.load.a.a.c.a(this.a, var1));
      } else {
         var5 = null;
      }

      return var5;
   }

   public boolean a(Uri var1) {
      return io.intercom.com.bumptech.glide.load.a.a.b.c(var1);
   }

   public static class a implements n {
      private final Context a;

      public a(Context var1) {
         this.a = var1;
      }

      public m a(q var1) {
         return new c(this.a);
      }
   }
}
