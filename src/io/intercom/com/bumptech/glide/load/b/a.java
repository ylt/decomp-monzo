package io.intercom.com.bumptech.glide.load.b;

import android.content.res.AssetManager;
import android.net.Uri;

public class a implements m {
   private static final int a = "file:///android_asset/".length();
   private final AssetManager b;
   private final a.a c;

   public a(AssetManager var1, a.a var2) {
      this.b = var1;
      this.c = var2;
   }

   public m.a a(Uri var1, int var2, int var3, io.intercom.com.bumptech.glide.load.i var4) {
      String var5 = var1.toString().substring(a);
      return new m.a(new io.intercom.com.bumptech.glide.g.b(var1), this.c.a(this.b, var5));
   }

   public boolean a(Uri var1) {
      boolean var3 = false;
      boolean var2 = var3;
      if("file".equals(var1.getScheme())) {
         var2 = var3;
         if(!var1.getPathSegments().isEmpty()) {
            var2 = var3;
            if("android_asset".equals(var1.getPathSegments().get(0))) {
               var2 = true;
            }
         }
      }

      return var2;
   }

   public interface a {
      io.intercom.com.bumptech.glide.load.a.b a(AssetManager var1, String var2);
   }

   public static class b implements a.a, n {
      private final AssetManager a;

      public b(AssetManager var1) {
         this.a = var1;
      }

      public io.intercom.com.bumptech.glide.load.a.b a(AssetManager var1, String var2) {
         return new io.intercom.com.bumptech.glide.load.a.f(var1, var2);
      }

      public m a(q var1) {
         return new a(this.a, this);
      }
   }

   public static class c implements a.a, n {
      private final AssetManager a;

      public c(AssetManager var1) {
         this.a = var1;
      }

      public io.intercom.com.bumptech.glide.load.a.b a(AssetManager var1, String var2) {
         return new io.intercom.com.bumptech.glide.load.a.k(var1, var2);
      }

      public m a(q var1) {
         return new a(this.a, this);
      }
   }
}
