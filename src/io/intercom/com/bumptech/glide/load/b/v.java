package io.intercom.com.bumptech.glide.load.b;

import android.content.ContentResolver;
import android.net.Uri;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class v implements m {
   private static final Set a = Collections.unmodifiableSet(new HashSet(Arrays.asList(new String[]{"file", "android.resource", "content"})));
   private final v.b b;

   public v(v.b var1) {
      this.b = var1;
   }

   public m.a a(Uri var1, int var2, int var3, io.intercom.com.bumptech.glide.load.i var4) {
      return new m.a(new io.intercom.com.bumptech.glide.g.b(var1), this.b.a(var1));
   }

   public boolean a(Uri var1) {
      return a.contains(var1.getScheme());
   }

   public static class a implements n, v.b {
      private final ContentResolver a;

      public a(ContentResolver var1) {
         this.a = var1;
      }

      public io.intercom.com.bumptech.glide.load.a.b a(Uri var1) {
         return new io.intercom.com.bumptech.glide.load.a.g(this.a, var1);
      }

      public m a(q var1) {
         return new v(this);
      }
   }

   public interface b {
      io.intercom.com.bumptech.glide.load.a.b a(Uri var1);
   }

   public static class c implements n, v.b {
      private final ContentResolver a;

      public c(ContentResolver var1) {
         this.a = var1;
      }

      public io.intercom.com.bumptech.glide.load.a.b a(Uri var1) {
         return new io.intercom.com.bumptech.glide.load.a.l(this.a, var1);
      }

      public m a(q var1) {
         return new v(this);
      }
   }
}
