package io.intercom.com.bumptech.glide.load.b;

import android.net.Uri;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class w implements m {
   private static final Set a = Collections.unmodifiableSet(new HashSet(Arrays.asList(new String[]{"http", "https"})));
   private final m b;

   public w(m var1) {
      this.b = var1;
   }

   public m.a a(Uri var1, int var2, int var3, io.intercom.com.bumptech.glide.load.i var4) {
      g var5 = new g(var1.toString());
      return this.b.a(var5, var2, var3, var4);
   }

   public boolean a(Uri var1) {
      return a.contains(var1.getScheme());
   }

   public static class a implements n {
      public m a(q var1) {
         return new w(var1.a(g.class, InputStream.class));
      }
   }
}
