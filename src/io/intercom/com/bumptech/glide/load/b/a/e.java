package io.intercom.com.bumptech.glide.load.b.a;

import io.intercom.com.bumptech.glide.load.i;
import io.intercom.com.bumptech.glide.load.b.g;
import io.intercom.com.bumptech.glide.load.b.m;
import io.intercom.com.bumptech.glide.load.b.n;
import io.intercom.com.bumptech.glide.load.b.q;
import java.io.InputStream;
import java.net.URL;

public class e implements m {
   private final m a;

   public e(m var1) {
      this.a = var1;
   }

   public m.a a(URL var1, int var2, int var3, i var4) {
      return this.a.a(new g(var1), var2, var3, var4);
   }

   public boolean a(URL var1) {
      return true;
   }

   public static class a implements n {
      public m a(q var1) {
         return new e(var1.a(g.class, InputStream.class));
      }
   }
}
