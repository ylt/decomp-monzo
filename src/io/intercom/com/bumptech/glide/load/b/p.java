package io.intercom.com.bumptech.glide.load.b;

import io.intercom.com.bumptech.glide.load.engine.GlideException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

class p implements m {
   private final List a;
   private final android.support.v4.g.k.a b;

   p(List var1, android.support.v4.g.k.a var2) {
      this.a = var1;
      this.b = var2;
   }

   public m.a a(Object var1, int var2, int var3, io.intercom.com.bumptech.glide.load.i var4) {
      int var6 = this.a.size();
      ArrayList var8 = new ArrayList(var6);
      int var5 = 0;

      io.intercom.com.bumptech.glide.load.g var7;
      for(var7 = null; var5 < var6; ++var5) {
         m var9 = (m)this.a.get(var5);
         if(var9.a(var1)) {
            m.a var11 = var9.a(var1, var2, var3, var4);
            if(var11 != null) {
               var7 = var11.a;
               var8.add(var11.c);
            }
         }
      }

      m.a var10;
      if(!var8.isEmpty()) {
         var10 = new m.a(var7, new p.a(var8, this.b));
      } else {
         var10 = null;
      }

      return var10;
   }

   public boolean a(Object var1) {
      Iterator var3 = this.a.iterator();

      boolean var2;
      while(true) {
         if(var3.hasNext()) {
            if(!((m)var3.next()).a(var1)) {
               continue;
            }

            var2 = true;
            break;
         }

         var2 = false;
         break;
      }

      return var2;
   }

   public String toString() {
      return "MultiModelLoader{modelLoaders=" + Arrays.toString(this.a.toArray(new m[this.a.size()])) + '}';
   }

   static class a implements io.intercom.com.bumptech.glide.load.a.b, io.intercom.com.bumptech.glide.load.a.b.a {
      private final List a;
      private final android.support.v4.g.k.a b;
      private int c;
      private io.intercom.com.bumptech.glide.g d;
      private io.intercom.com.bumptech.glide.load.a.b.a e;
      private List f;

      a(List var1, android.support.v4.g.k.a var2) {
         this.b = var2;
         io.intercom.com.bumptech.glide.h.h.a((Collection)var1);
         this.a = var1;
         this.c = 0;
      }

      private void e() {
         if(this.c < this.a.size() - 1) {
            ++this.c;
            this.a(this.d, this.e);
         } else {
            this.e.a((Exception)(new GlideException("Fetch failed", new ArrayList(this.f))));
         }

      }

      public void a() {
         if(this.f != null) {
            this.b.a(this.f);
         }

         this.f = null;
         Iterator var1 = this.a.iterator();

         while(var1.hasNext()) {
            ((io.intercom.com.bumptech.glide.load.a.b)var1.next()).a();
         }

      }

      public void a(io.intercom.com.bumptech.glide.g var1, io.intercom.com.bumptech.glide.load.a.b.a var2) {
         this.d = var1;
         this.e = var2;
         this.f = (List)this.b.a();
         ((io.intercom.com.bumptech.glide.load.a.b)this.a.get(this.c)).a(var1, this);
      }

      public void a(Exception var1) {
         this.f.add(var1);
         this.e();
      }

      public void a(Object var1) {
         if(var1 != null) {
            this.e.a(var1);
         } else {
            this.e();
         }

      }

      public void b() {
         Iterator var1 = this.a.iterator();

         while(var1.hasNext()) {
            ((io.intercom.com.bumptech.glide.load.a.b)var1.next()).b();
         }

      }

      public io.intercom.com.bumptech.glide.load.a c() {
         return ((io.intercom.com.bumptech.glide.load.a.b)this.a.get(0)).c();
      }

      public Class d() {
         return ((io.intercom.com.bumptech.glide.load.a.b)this.a.get(0)).d();
      }
   }
}
