package io.intercom.com.bumptech.glide.load.b;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.ByteBuffer;

public class b implements m {
   private final b.b a;

   public b(b.b var1) {
      this.a = var1;
   }

   public m.a a(byte[] var1, int var2, int var3, io.intercom.com.bumptech.glide.load.i var4) {
      return new m.a(io.intercom.com.bumptech.glide.g.a.a(), new b.c(var1, this.a));
   }

   public boolean a(byte[] var1) {
      return true;
   }

   public static class a implements n {
      public m a(q var1) {
         return new b(new b.b() {
            public Class a() {
               return ByteBuffer.class;
            }

            public ByteBuffer a(byte[] var1) {
               return ByteBuffer.wrap(var1);
            }

            // $FF: synthetic method
            public Object b(byte[] var1) {
               return this.a(var1);
            }
         });
      }
   }

   public interface b {
      Class a();

      Object b(byte[] var1);
   }

   private static class c implements io.intercom.com.bumptech.glide.load.a.b {
      private final byte[] a;
      private final b.b b;

      public c(byte[] var1, b.b var2) {
         this.a = var1;
         this.b = var2;
      }

      public void a() {
      }

      public void a(io.intercom.com.bumptech.glide.g var1, io.intercom.com.bumptech.glide.load.a.b.a var2) {
         var2.a(this.b.b(this.a));
      }

      public void b() {
      }

      public io.intercom.com.bumptech.glide.load.a c() {
         return io.intercom.com.bumptech.glide.load.a.a;
      }

      public Class d() {
         return this.b.a();
      }
   }

   public static class d implements n {
      public m a(q var1) {
         return new b(new b.b() {
            public InputStream a(byte[] var1) {
               return new ByteArrayInputStream(var1);
            }

            public Class a() {
               return InputStream.class;
            }

            // $FF: synthetic method
            public Object b(byte[] var1) {
               return this.a(var1);
            }
         });
      }
   }
}
