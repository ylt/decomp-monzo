package io.intercom.com.bumptech.glide.load.b.a;

import android.net.Uri;
import io.intercom.com.bumptech.glide.load.i;
import io.intercom.com.bumptech.glide.load.b.g;
import io.intercom.com.bumptech.glide.load.b.m;
import io.intercom.com.bumptech.glide.load.b.n;
import io.intercom.com.bumptech.glide.load.b.q;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class b implements m {
   private static final Set a = Collections.unmodifiableSet(new HashSet(Arrays.asList(new String[]{"http", "https"})));
   private final m b;

   public b(m var1) {
      this.b = var1;
   }

   public m.a a(Uri var1, int var2, int var3, i var4) {
      return this.b.a(new g(var1.toString()), var2, var3, var4);
   }

   public boolean a(Uri var1) {
      return a.contains(var1.getScheme());
   }

   public static class a implements n {
      public m a(q var1) {
         return new b(var1.a(g.class, InputStream.class));
      }
   }
}
