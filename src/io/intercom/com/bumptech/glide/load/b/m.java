package io.intercom.com.bumptech.glide.load.b;

import java.util.Collections;
import java.util.List;

public interface m {
   m.a a(Object var1, int var2, int var3, io.intercom.com.bumptech.glide.load.i var4);

   boolean a(Object var1);

   public static class a {
      public final io.intercom.com.bumptech.glide.load.g a;
      public final List b;
      public final io.intercom.com.bumptech.glide.load.a.b c;

      public a(io.intercom.com.bumptech.glide.load.g var1, io.intercom.com.bumptech.glide.load.a.b var2) {
         this(var1, Collections.emptyList(), var2);
      }

      public a(io.intercom.com.bumptech.glide.load.g var1, List var2, io.intercom.com.bumptech.glide.load.a.b var3) {
         this.a = (io.intercom.com.bumptech.glide.load.g)io.intercom.com.bumptech.glide.h.h.a((Object)var1);
         this.b = (List)io.intercom.com.bumptech.glide.h.h.a((Object)var2);
         this.c = (io.intercom.com.bumptech.glide.load.a.b)io.intercom.com.bumptech.glide.h.h.a((Object)var3);
      }
   }
}
