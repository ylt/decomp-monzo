package io.intercom.com.bumptech.glide.load.b;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class q {
   private static final q.c a = new q.c();
   private static final m b = new q.a();
   private final List c;
   private final q.c d;
   private final Set e;
   private final android.support.v4.g.k.a f;

   public q(android.support.v4.g.k.a var1) {
      this(var1, a);
   }

   q(android.support.v4.g.k.a var1, q.c var2) {
      this.c = new ArrayList();
      this.e = new HashSet();
      this.f = var1;
      this.d = var2;
   }

   private static m a() {
      return b;
   }

   private m a(q.b var1) {
      return (m)io.intercom.com.bumptech.glide.h.h.a((Object)var1.b.a(this));
   }

   private void a(Class var1, Class var2, n var3, boolean var4) {
      q.b var6 = new q.b(var1, var2, var3);
      List var7 = this.c;
      int var5;
      if(var4) {
         var5 = this.c.size();
      } else {
         var5 = 0;
      }

      var7.add(var5, var6);
   }

   public m a(Class param1, Class param2) {
      // $FF: Couldn't be decompiled
   }

   List a(Class param1) {
      // $FF: Couldn't be decompiled
   }

   void a(Class var1, Class var2, n var3) {
      synchronized(this){}

      try {
         this.a(var1, var2, var3, true);
      } finally {
         ;
      }

   }

   List b(Class param1) {
      // $FF: Couldn't be decompiled
   }

   private static class a implements m {
      public m.a a(Object var1, int var2, int var3, io.intercom.com.bumptech.glide.load.i var4) {
         return null;
      }

      public boolean a(Object var1) {
         return false;
      }
   }

   private static class b {
      final Class a;
      final n b;
      private final Class c;

      public b(Class var1, Class var2, n var3) {
         this.c = var1;
         this.a = var2;
         this.b = var3;
      }

      public boolean a(Class var1) {
         return this.c.isAssignableFrom(var1);
      }

      public boolean a(Class var1, Class var2) {
         boolean var3;
         if(this.a(var1) && this.a.isAssignableFrom(var2)) {
            var3 = true;
         } else {
            var3 = false;
         }

         return var3;
      }
   }

   static class c {
      public p a(List var1, android.support.v4.g.k.a var2) {
         return new p(var1, var2);
      }
   }
}
