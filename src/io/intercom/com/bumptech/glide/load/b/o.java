package io.intercom.com.bumptech.glide.load.b;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class o {
   private final q a;
   private final o.a b;

   public o(android.support.v4.g.k.a var1) {
      this(new q(var1));
   }

   o(q var1) {
      this.b = new o.a();
      this.a = var1;
   }

   private static Class b(Object var0) {
      return var0.getClass();
   }

   private List b(Class var1) {
      List var3 = this.b.a(var1);
      List var2 = var3;
      if(var3 == null) {
         var2 = Collections.unmodifiableList(this.a.a(var1));
         this.b.a(var1, var2);
      }

      return var2;
   }

   public List a(Class var1) {
      synchronized(this){}

      List var4;
      try {
         var4 = this.a.b(var1);
      } finally {
         ;
      }

      return var4;
   }

   public List a(Object param1) {
      // $FF: Couldn't be decompiled
   }

   public void a(Class var1, Class var2, n var3) {
      synchronized(this){}

      try {
         this.a.a(var1, var2, var3);
         this.b.a();
      } finally {
         ;
      }

   }

   private static class a {
      private final Map a = new HashMap();

      public List a(Class var1) {
         o.a var2 = (o.a)this.a.get(var1);
         List var3;
         if(var2 == null) {
            var3 = null;
         } else {
            var3 = var2.a;
         }

         return var3;
      }

      public void a() {
         this.a.clear();
      }

      public void a(Class var1, List var2) {
         if((o.a)this.a.put(var1, new o.a(var2)) != null) {
            throw new IllegalStateException("Already cached loaders for model: " + var1);
         }
      }
   }

   private static class a {
      final List a;

      public a(List var1) {
         this.a = var1;
      }
   }
}
