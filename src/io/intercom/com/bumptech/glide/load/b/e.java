package io.intercom.com.bumptech.glide.load.b;

import android.util.Base64;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

public final class e implements m {
   private final e.a a;

   public e(e.a var1) {
      this.a = var1;
   }

   public m.a a(String var1, int var2, int var3, io.intercom.com.bumptech.glide.load.i var4) {
      return new m.a(new io.intercom.com.bumptech.glide.g.b(var1), new e.b(var1, this.a));
   }

   public boolean a(String var1) {
      return var1.startsWith("data:image");
   }

   public interface a {
      Class a();

      Object a(String var1) throws IllegalArgumentException;

      void a(Object var1) throws IOException;
   }

   private static final class b implements io.intercom.com.bumptech.glide.load.a.b {
      private final String a;
      private final e.a b;
      private Object c;

      public b(String var1, e.a var2) {
         this.a = var1;
         this.b = var2;
      }

      public void a() {
         try {
            this.b.a(this.c);
         } catch (IOException var2) {
            ;
         }

      }

      public void a(io.intercom.com.bumptech.glide.g var1, io.intercom.com.bumptech.glide.load.a.b.a var2) {
         try {
            this.c = this.b.a(this.a);
            var2.a(this.c);
         } catch (IllegalArgumentException var3) {
            var2.a((Exception)var3);
         }

      }

      public void b() {
      }

      public io.intercom.com.bumptech.glide.load.a c() {
         return io.intercom.com.bumptech.glide.load.a.a;
      }

      public Class d() {
         return this.b.a();
      }
   }

   public static final class c implements n {
      private final e.a a = new e.a() {
         public Class a() {
            return InputStream.class;
         }

         // $FF: synthetic method
         public Object a(String var1) throws IllegalArgumentException {
            return this.b(var1);
         }

         public void a(InputStream var1) throws IOException {
            var1.close();
         }

         public InputStream b(String var1) {
            if(!var1.startsWith("data:image")) {
               throw new IllegalArgumentException("Not a valid image data URL.");
            } else {
               int var2 = var1.indexOf(44);
               if(var2 == -1) {
                  throw new IllegalArgumentException("Missing comma in data URL.");
               } else if(!var1.substring(0, var2).endsWith(";base64")) {
                  throw new IllegalArgumentException("Not a base64 image data URL.");
               } else {
                  return new ByteArrayInputStream(Base64.decode(var1.substring(var2 + 1), 0));
               }
            }
         }
      };

      public final m a(q var1) {
         return new e(this.a);
      }
   }
}
