package io.intercom.com.bumptech.glide.load.b;

import android.os.ParcelFileDescriptor;
import android.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class f implements m {
   private final f.d a;

   public f(f.d var1) {
      this.a = var1;
   }

   public m.a a(File var1, int var2, int var3, io.intercom.com.bumptech.glide.load.i var4) {
      return new m.a(new io.intercom.com.bumptech.glide.g.b(var1), new f.c(var1, this.a));
   }

   public boolean a(File var1) {
      return true;
   }

   public static class a implements n {
      private final f.d a;

      public a(f.d var1) {
         this.a = var1;
      }

      public final m a(q var1) {
         return new f(this.a);
      }
   }

   public static class b extends f.a {
      public b() {
         super(new f.d() {
            public ParcelFileDescriptor a(File var1) throws FileNotFoundException {
               return ParcelFileDescriptor.open(var1, 268435456);
            }

            public Class a() {
               return ParcelFileDescriptor.class;
            }

            public void a(ParcelFileDescriptor var1) throws IOException {
               var1.close();
            }

            // $FF: synthetic method
            public Object b(File var1) throws FileNotFoundException {
               return this.a(var1);
            }
         });
      }
   }

   private static class c implements io.intercom.com.bumptech.glide.load.a.b {
      private final File a;
      private final f.d b;
      private Object c;

      public c(File var1, f.d var2) {
         this.a = var1;
         this.b = var2;
      }

      public void a() {
         if(this.c != null) {
            try {
               this.b.a(this.c);
            } catch (IOException var2) {
               ;
            }
         }

      }

      public void a(io.intercom.com.bumptech.glide.g var1, io.intercom.com.bumptech.glide.load.a.b.a var2) {
         try {
            this.c = this.b.b(this.a);
         } catch (FileNotFoundException var3) {
            if(Log.isLoggable("FileLoader", 3)) {
               Log.d("FileLoader", "Failed to open file", var3);
            }

            var2.a((Exception)var3);
            return;
         }

         var2.a(this.c);
      }

      public void b() {
      }

      public io.intercom.com.bumptech.glide.load.a c() {
         return io.intercom.com.bumptech.glide.load.a.a;
      }

      public Class d() {
         return this.b.a();
      }
   }

   public interface d {
      Class a();

      void a(Object var1) throws IOException;

      Object b(File var1) throws FileNotFoundException;
   }

   public static class e extends f.a {
      public e() {
         super(new f.d() {
            public InputStream a(File var1) throws FileNotFoundException {
               return new FileInputStream(var1);
            }

            public Class a() {
               return InputStream.class;
            }

            public void a(InputStream var1) throws IOException {
               var1.close();
            }

            // $FF: synthetic method
            public Object b(File var1) throws FileNotFoundException {
               return this.a(var1);
            }
         });
      }
   }
}
