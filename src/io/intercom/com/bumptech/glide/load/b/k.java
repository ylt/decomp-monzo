package io.intercom.com.bumptech.glide.load.b;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;
import java.io.File;
import java.io.FileNotFoundException;

public final class k implements m {
   private final Context a;

   k(Context var1) {
      this.a = var1;
   }

   public m.a a(Uri var1, int var2, int var3, io.intercom.com.bumptech.glide.load.i var4) {
      return new m.a(new io.intercom.com.bumptech.glide.g.b(var1), new k.b(this.a, var1));
   }

   public boolean a(Uri var1) {
      return io.intercom.com.bumptech.glide.load.a.a.b.a(var1);
   }

   public static final class a implements n {
      private final Context a;

      public a(Context var1) {
         this.a = var1;
      }

      public m a(q var1) {
         return new k(this.a);
      }
   }

   private static class b implements io.intercom.com.bumptech.glide.load.a.b {
      private static final String[] a = new String[]{"_data"};
      private final Context b;
      private final Uri c;

      b(Context var1, Uri var2) {
         this.b = var1;
         this.c = var2;
      }

      public void a() {
      }

      public void a(io.intercom.com.bumptech.glide.g var1, io.intercom.com.bumptech.glide.load.a.b.a var2) {
         String var7 = null;
         Object var3 = null;
         Cursor var4 = this.b.getContentResolver().query(this.c, a, (String)null, (String[])null, (String)null);
         if(var4 != null) {
            var7 = (String)var3;

            try {
               if(var4.moveToFirst()) {
                  var7 = var4.getString(var4.getColumnIndexOrThrow("_data"));
               }
            } finally {
               var4.close();
            }
         }

         if(TextUtils.isEmpty(var7)) {
            var2.a((Exception)(new FileNotFoundException("Failed to find file path for: " + this.c)));
         } else {
            var2.a((Object)(new File(var7)));
         }

      }

      public void b() {
      }

      public io.intercom.com.bumptech.glide.load.a c() {
         return io.intercom.com.bumptech.glide.load.a.a;
      }

      public Class d() {
         return File.class;
      }
   }
}
