package io.intercom.com.bumptech.glide.load.b;

public class u implements m {
   public m.a a(Object var1, int var2, int var3, io.intercom.com.bumptech.glide.load.i var4) {
      return new m.a(new io.intercom.com.bumptech.glide.g.b(var1), new u.b(var1));
   }

   public boolean a(Object var1) {
      return true;
   }

   public static class a implements n {
      public m a(q var1) {
         return new u();
      }
   }

   private static class b implements io.intercom.com.bumptech.glide.load.a.b {
      private final Object a;

      public b(Object var1) {
         this.a = var1;
      }

      public void a() {
      }

      public void a(io.intercom.com.bumptech.glide.g var1, io.intercom.com.bumptech.glide.load.a.b.a var2) {
         var2.a(this.a);
      }

      public void b() {
      }

      public io.intercom.com.bumptech.glide.load.a c() {
         return io.intercom.com.bumptech.glide.load.a.a;
      }

      public Class d() {
         return this.a.getClass();
      }
   }
}
