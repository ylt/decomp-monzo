package io.intercom.com.bumptech.glide.load.b;

import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.text.TextUtils;
import java.io.File;
import java.io.InputStream;

public class t implements m {
   private final m a;

   public t(m var1) {
      this.a = var1;
   }

   private static Uri b(String var0) {
      Uri var1;
      if(TextUtils.isEmpty(var0)) {
         var1 = null;
      } else if(var0.startsWith("/")) {
         var1 = c(var0);
      } else {
         Uri var2 = Uri.parse(var0);
         var1 = var2;
         if(var2.getScheme() == null) {
            var1 = c(var0);
         }
      }

      return var1;
   }

   private static Uri c(String var0) {
      return Uri.fromFile(new File(var0));
   }

   public m.a a(String var1, int var2, int var3, io.intercom.com.bumptech.glide.load.i var4) {
      Uri var5 = b(var1);
      m.a var6;
      if(var5 == null) {
         var6 = null;
      } else {
         var6 = this.a.a(var5, var2, var3, var4);
      }

      return var6;
   }

   public boolean a(String var1) {
      return true;
   }

   public static class a implements n {
      public m a(q var1) {
         return new t(var1.a(Uri.class, ParcelFileDescriptor.class));
      }
   }

   public static class b implements n {
      public m a(q var1) {
         return new t(var1.a(Uri.class, InputStream.class));
      }
   }
}
