package io.intercom.com.bumptech.glide.load.b.a;

import io.intercom.com.bumptech.glide.load.h;
import io.intercom.com.bumptech.glide.load.i;
import io.intercom.com.bumptech.glide.load.b.g;
import io.intercom.com.bumptech.glide.load.b.l;
import io.intercom.com.bumptech.glide.load.b.m;
import io.intercom.com.bumptech.glide.load.b.n;
import io.intercom.com.bumptech.glide.load.b.q;

public class a implements m {
   public static final h a = h.a((String)"io.intercom.com.bumptech.glide.load.model.stream.HttpGlideUrlLoader.Timeout", (Object)Integer.valueOf(2500));
   private final l b;

   public a() {
      this((l)null);
   }

   public a(l var1) {
      this.b = var1;
   }

   public m.a a(g var1, int var2, int var3, i var4) {
      g var5 = var1;
      if(this.b != null) {
         var5 = (g)this.b.a(var1, 0, 0);
         if(var5 == null) {
            this.b.a(var1, 0, 0, var1);
            var5 = var1;
         }
      }

      return new m.a(var5, new io.intercom.com.bumptech.glide.load.a.h(var5, ((Integer)var4.a(a)).intValue()));
   }

   public boolean a(g var1) {
      return true;
   }

   public static class a implements n {
      private final l a = new l(500);

      public m a(q var1) {
         return new a(this.a);
      }
   }
}
