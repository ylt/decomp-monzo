package io.intercom.com.bumptech.glide.load.b;

import android.content.res.Resources;
import android.content.res.Resources.NotFoundException;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import java.io.InputStream;

public class r implements m {
   private final m a;
   private final Resources b;

   public r(Resources var1, m var2) {
      this.b = var1;
      this.a = var2;
   }

   private Uri b(Integer var1) {
      Uri var4;
      Uri var5;
      try {
         StringBuilder var2 = new StringBuilder();
         var5 = Uri.parse(var2.append("android.resource://").append(this.b.getResourcePackageName(var1.intValue())).append('/').append(this.b.getResourceTypeName(var1.intValue())).append('/').append(this.b.getResourceEntryName(var1.intValue())).toString());
      } catch (NotFoundException var3) {
         if(Log.isLoggable("ResourceLoader", 5)) {
            Log.w("ResourceLoader", "Received invalid resource id: " + var1, var3);
         }

         var4 = null;
         return var4;
      }

      var4 = var5;
      return var4;
   }

   public m.a a(Integer var1, int var2, int var3, io.intercom.com.bumptech.glide.load.i var4) {
      Uri var5 = this.b(var1);
      m.a var6;
      if(var5 == null) {
         var6 = null;
      } else {
         var6 = this.a.a(var5, var2, var3, var4);
      }

      return var6;
   }

   public boolean a(Integer var1) {
      return true;
   }

   public static class a implements n {
      private final Resources a;

      public a(Resources var1) {
         this.a = var1;
      }

      public m a(q var1) {
         return new r(this.a, var1.a(Uri.class, ParcelFileDescriptor.class));
      }
   }

   public static class b implements n {
      private final Resources a;

      public b(Resources var1) {
         this.a = var1;
      }

      public m a(q var1) {
         return new r(this.a, var1.a(Uri.class, InputStream.class));
      }
   }
}
