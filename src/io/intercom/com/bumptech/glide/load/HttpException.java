package io.intercom.com.bumptech.glide.load;

import java.io.IOException;

public final class HttpException extends IOException {
   private final int a;

   public HttpException(int var1) {
      this("Http request failed with status code: " + var1, var1);
   }

   public HttpException(String var1) {
      this(var1, -1);
   }

   public HttpException(String var1, int var2) {
      this(var1, var2, (Throwable)null);
   }

   public HttpException(String var1, int var2, Throwable var3) {
      super(var1, var3);
      this.a = var2;
   }
}
