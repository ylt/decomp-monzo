package io.intercom.com.bumptech.glide.load;

import android.content.Context;
import io.intercom.com.bumptech.glide.load.engine.r;

public interface l extends g {
   boolean equals(Object var1);

   int hashCode();

   r transform(Context var1, r var2, int var3, int var4);
}
