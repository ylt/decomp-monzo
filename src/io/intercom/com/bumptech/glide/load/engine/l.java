package io.intercom.com.bumptech.glide.load.engine;

import java.security.MessageDigest;
import java.util.Map;

class l implements io.intercom.com.bumptech.glide.load.g {
   private final Object b;
   private final int c;
   private final int d;
   private final Class e;
   private final Class f;
   private final io.intercom.com.bumptech.glide.load.g g;
   private final Map h;
   private final io.intercom.com.bumptech.glide.load.i i;
   private int j;

   public l(Object var1, io.intercom.com.bumptech.glide.load.g var2, int var3, int var4, Map var5, Class var6, Class var7, io.intercom.com.bumptech.glide.load.i var8) {
      this.b = io.intercom.com.bumptech.glide.h.h.a(var1);
      this.g = (io.intercom.com.bumptech.glide.load.g)io.intercom.com.bumptech.glide.h.h.a(var2, "Signature must not be null");
      this.c = var3;
      this.d = var4;
      this.h = (Map)io.intercom.com.bumptech.glide.h.h.a((Object)var5);
      this.e = (Class)io.intercom.com.bumptech.glide.h.h.a(var6, "Resource class must not be null");
      this.f = (Class)io.intercom.com.bumptech.glide.h.h.a(var7, "Transcode class must not be null");
      this.i = (io.intercom.com.bumptech.glide.load.i)io.intercom.com.bumptech.glide.h.h.a((Object)var8);
   }

   public boolean equals(Object var1) {
      boolean var3 = false;
      boolean var2 = var3;
      if(var1 instanceof l) {
         l var4 = (l)var1;
         var2 = var3;
         if(this.b.equals(var4.b)) {
            var2 = var3;
            if(this.g.equals(var4.g)) {
               var2 = var3;
               if(this.d == var4.d) {
                  var2 = var3;
                  if(this.c == var4.c) {
                     var2 = var3;
                     if(this.h.equals(var4.h)) {
                        var2 = var3;
                        if(this.e.equals(var4.e)) {
                           var2 = var3;
                           if(this.f.equals(var4.f)) {
                              var2 = var3;
                              if(this.i.equals(var4.i)) {
                                 var2 = true;
                              }
                           }
                        }
                     }
                  }
               }
            }
         }
      }

      return var2;
   }

   public int hashCode() {
      if(this.j == 0) {
         this.j = this.b.hashCode();
         this.j = this.j * 31 + this.g.hashCode();
         this.j = this.j * 31 + this.c;
         this.j = this.j * 31 + this.d;
         this.j = this.j * 31 + this.h.hashCode();
         this.j = this.j * 31 + this.e.hashCode();
         this.j = this.j * 31 + this.f.hashCode();
         this.j = this.j * 31 + this.i.hashCode();
      }

      return this.j;
   }

   public String toString() {
      return "EngineKey{model=" + this.b + ", width=" + this.c + ", height=" + this.d + ", resourceClass=" + this.e + ", transcodeClass=" + this.f + ", signature=" + this.g + ", hashCode=" + this.j + ", transformations=" + this.h + ", options=" + this.i + '}';
   }

   public void updateDiskCacheKey(MessageDigest var1) {
      throw new UnsupportedOperationException();
   }
}
