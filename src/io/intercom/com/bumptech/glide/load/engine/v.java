package io.intercom.com.bumptech.glide.load.engine;

import android.util.Log;
import java.util.Collections;
import java.util.List;

class v implements io.intercom.com.bumptech.glide.load.a.b.a, d, d.a {
   private final e a;
   private final d.a b;
   private int c;
   private a d;
   private Object e;
   private volatile io.intercom.com.bumptech.glide.load.b.m.a f;
   private b g;

   public v(e var1, d.a var2) {
      this.a = var1;
      this.b = var2;
   }

   private void b(Object var1) {
      long var2 = io.intercom.com.bumptech.glide.h.d.a();

      try {
         io.intercom.com.bumptech.glide.load.d var4 = this.a.a(var1);
         c var5 = new c(var4, var1, this.a.e());
         b var6 = new b(this.f.a, this.a.f());
         this.g = var6;
         this.a.b().a(this.g, var5);
         if(Log.isLoggable("SourceGenerator", 2)) {
            StringBuilder var9 = new StringBuilder();
            Log.v("SourceGenerator", var9.append("Finished encoding source to cache, key: ").append(this.g).append(", data: ").append(var1).append(", encoder: ").append(var4).append(", duration: ").append(io.intercom.com.bumptech.glide.h.d.a(var2)).toString());
         }
      } finally {
         this.f.c.a();
      }

      this.d = new a(Collections.singletonList(this.f.a), this.a, this);
   }

   private boolean d() {
      boolean var1;
      if(this.c < this.a.k().size()) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public void a(io.intercom.com.bumptech.glide.load.g var1, Exception var2, io.intercom.com.bumptech.glide.load.a.b var3, io.intercom.com.bumptech.glide.load.a var4) {
      this.b.a(var1, var2, var3, this.f.c.c());
   }

   public void a(io.intercom.com.bumptech.glide.load.g var1, Object var2, io.intercom.com.bumptech.glide.load.a.b var3, io.intercom.com.bumptech.glide.load.a var4, io.intercom.com.bumptech.glide.load.g var5) {
      this.b.a(var1, var2, var3, this.f.c.c(), var1);
   }

   public void a(Exception var1) {
      this.b.a(this.g, var1, this.f.c, this.f.c.c());
   }

   public void a(Object var1) {
      h var2 = this.a.c();
      if(var1 != null && var2.a(this.f.c.c())) {
         this.e = var1;
         this.b.c();
      } else {
         this.b.a(this.f.a, var1, this.f.c, this.f.c.c(), this.g);
      }

   }

   public boolean a() {
      if(this.e != null) {
         Object var3 = this.e;
         this.e = null;
         this.b(var3);
      }

      boolean var2;
      if(this.d != null && this.d.a()) {
         var2 = true;
         return var2;
      } else {
         this.d = null;
         this.f = null;
         var2 = false;

         while(true) {
            do {
               do {
                  if(var2 || !this.d()) {
                     return var2;
                  }

                  List var4 = this.a.k();
                  int var1 = this.c;
                  this.c = var1 + 1;
                  this.f = (io.intercom.com.bumptech.glide.load.b.m.a)var4.get(var1);
               } while(this.f == null);
            } while(!this.a.c().a(this.f.c.c()) && !this.a.a(this.f.c.d()));

            this.f.c.a(this.a.d(), this);
            var2 = true;
         }
      }
   }

   public void b() {
      io.intercom.com.bumptech.glide.load.b.m.a var1 = this.f;
      if(var1 != null) {
         var1.c.b();
      }

   }

   public void c() {
      throw new UnsupportedOperationException();
   }
}
