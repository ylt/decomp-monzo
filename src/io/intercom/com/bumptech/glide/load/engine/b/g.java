package io.intercom.com.bumptech.glide.load.engine.b;

import android.annotation.SuppressLint;
import io.intercom.com.bumptech.glide.load.engine.r;

public class g extends io.intercom.com.bumptech.glide.h.e implements h {
   private h.a a;

   public g(int var1) {
      super(var1);
   }

   protected int a(r var1) {
      return var1.d();
   }

   // $FF: synthetic method
   public r a(io.intercom.com.bumptech.glide.load.g var1) {
      return (r)super.c(var1);
   }

   @SuppressLint({"InlinedApi"})
   public void a(int var1) {
      if(var1 >= 40) {
         this.a();
      } else if(var1 >= 20) {
         this.b(this.b() / 2);
      }

   }

   public void a(h.a var1) {
      this.a = var1;
   }

   protected void a(io.intercom.com.bumptech.glide.load.g var1, r var2) {
      if(this.a != null) {
         this.a.b(var2);
      }

   }
}
