package io.intercom.com.bumptech.glide.load.engine.a;

import android.util.Log;
import java.util.HashMap;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;

public final class j implements b {
   private final h a = new h();
   private final j.b b = new j.b();
   private final Map c = new HashMap();
   private final Map d = new HashMap();
   private final int e;
   private int f;

   public j() {
      this.e = 4194304;
   }

   public j(int var1) {
      this.e = var1;
   }

   private a a(Object var1) {
      return this.b(var1.getClass());
   }

   private Object a(j.a var1) {
      return this.a.a((m)var1);
   }

   private NavigableMap a(Class var1) {
      NavigableMap var3 = (NavigableMap)this.c.get(var1);
      Object var2 = var3;
      if(var3 == null) {
         var2 = new TreeMap();
         this.c.put(var1, var2);
      }

      return (NavigableMap)var2;
   }

   private boolean a(int var1, Integer var2) {
      boolean var3;
      if(var2 == null || !this.b() && var2.intValue() > var1 * 8) {
         var3 = false;
      } else {
         var3 = true;
      }

      return var3;
   }

   private a b(Class var1) {
      a var3 = (a)this.d.get(var1);
      Object var2 = var3;
      if(var3 == null) {
         if(var1.equals(int[].class)) {
            var2 = new i();
         } else {
            if(!var1.equals(byte[].class)) {
               throw new IllegalArgumentException("No array pool found for: " + var1.getSimpleName());
            }

            var2 = new g();
         }

         this.d.put(var1, var2);
      }

      return (a)var2;
   }

   private void b(int var1, Class var2) {
      NavigableMap var3 = this.a(var2);
      Integer var4 = (Integer)var3.get(Integer.valueOf(var1));
      if(var4 == null) {
         throw new NullPointerException("Tried to decrement empty size, size: " + var1 + ", this: " + this);
      } else {
         if(var4.intValue() == 1) {
            var3.remove(Integer.valueOf(var1));
         } else {
            var3.put(Integer.valueOf(var1), Integer.valueOf(var4.intValue() - 1));
         }

      }
   }

   private boolean b() {
      boolean var1;
      if(this.f != 0 && this.e / this.f < 2) {
         var1 = false;
      } else {
         var1 = true;
      }

      return var1;
   }

   private boolean b(int var1) {
      boolean var2;
      if(var1 <= this.e / 2) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   private void c() {
      this.c(this.e);
   }

   private void c(int var1) {
      while(this.f > var1) {
         Object var3 = this.a.a();
         io.intercom.com.bumptech.glide.h.h.a(var3);
         a var2 = this.a(var3);
         this.f -= var2.a(var3) * var2.b();
         this.b(var2.a(var3), var3.getClass());
         if(Log.isLoggable(var2.a(), 2)) {
            Log.v(var2.a(), "evicted: " + var2.a(var3));
         }
      }

   }

   public Object a(int param1, Class param2) {
      // $FF: Couldn't be decompiled
   }

   public void a() {
      synchronized(this){}

      try {
         this.c(0);
      } finally {
         ;
      }

   }

   public void a(int param1) {
      // $FF: Couldn't be decompiled
   }

   public void a(Object param1, Class param2) {
      // $FF: Couldn't be decompiled
   }

   private static final class a implements m {
      int a;
      private final j.b b;
      private Class c;

      a(j.b var1) {
         this.b = var1;
      }

      public void a() {
         this.b.a(this);
      }

      void a(int var1, Class var2) {
         this.a = var1;
         this.c = var2;
      }

      public boolean equals(Object var1) {
         boolean var3 = false;
         boolean var2 = var3;
         if(var1 instanceof j.a) {
            j.a var4 = (j.a)var1;
            var2 = var3;
            if(this.a == var4.a) {
               var2 = var3;
               if(this.c == var4.c) {
                  var2 = true;
               }
            }
         }

         return var2;
      }

      public int hashCode() {
         int var2 = this.a;
         int var1;
         if(this.c != null) {
            var1 = this.c.hashCode();
         } else {
            var1 = 0;
         }

         return var1 + var2 * 31;
      }

      public String toString() {
         return "Key{size=" + this.a + "array=" + this.c + '}';
      }
   }

   private static final class b extends d {
      protected j.a a() {
         return new j.a(this);
      }

      j.a a(int var1, Class var2) {
         j.a var3 = (j.a)this.c();
         var3.a(var1, var2);
         return var3;
      }

      // $FF: synthetic method
      protected m b() {
         return this.a();
      }
   }
}
