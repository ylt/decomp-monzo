package io.intercom.com.bumptech.glide.load.engine.a;

import java.util.Queue;

abstract class d {
   private final Queue a = io.intercom.com.bumptech.glide.h.i.a(20);

   public void a(m var1) {
      if(this.a.size() < 20) {
         this.a.offer(var1);
      }

   }

   protected abstract m b();

   protected m c() {
      m var2 = (m)this.a.poll();
      m var1 = var2;
      if(var2 == null) {
         var1 = this.b();
      }

      return var1;
   }
}
