package io.intercom.com.bumptech.glide.load.engine;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Handler.Callback;

class u {
   private boolean a;
   private final Handler b = new Handler(Looper.getMainLooper(), new u.a());

   public void a(r var1) {
      io.intercom.com.bumptech.glide.h.i.a();
      if(this.a) {
         this.b.obtainMessage(1, var1).sendToTarget();
      } else {
         this.a = true;
         var1.e();
         this.a = false;
      }

   }

   private static class a implements Callback {
      public boolean handleMessage(Message var1) {
         boolean var2;
         if(var1.what == 1) {
            ((r)var1.obj).e();
            var2 = true;
         } else {
            var2 = false;
         }

         return var2;
      }
   }
}
