package io.intercom.com.bumptech.glide.load.engine;

import java.io.File;
import java.util.List;

class a implements io.intercom.com.bumptech.glide.load.a.b.a, d {
   private List a;
   private final e b;
   private final d.a c;
   private int d;
   private io.intercom.com.bumptech.glide.load.g e;
   private List f;
   private int g;
   private volatile io.intercom.com.bumptech.glide.load.b.m.a h;
   private File i;

   a(e var1, d.a var2) {
      this(var1.l(), var1, var2);
   }

   a(List var1, e var2, d.a var3) {
      this.d = -1;
      this.a = var1;
      this.b = var2;
      this.c = var3;
   }

   private boolean c() {
      boolean var1;
      if(this.g < this.f.size()) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public void a(Exception var1) {
      this.c.a(this.e, var1, this.h.c, io.intercom.com.bumptech.glide.load.a.c);
   }

   public void a(Object var1) {
      this.c.a(this.e, var1, this.h.c, io.intercom.com.bumptech.glide.load.a.c, this.e);
   }

   public boolean a() {
      boolean var2 = false;
      boolean var3 = false;

      while(this.f == null || !this.c()) {
         ++this.d;
         if(this.d >= this.a.size()) {
            return var3;
         }

         io.intercom.com.bumptech.glide.load.g var5 = (io.intercom.com.bumptech.glide.load.g)this.a.get(this.d);
         b var4 = new b(var5, this.b.f());
         this.i = this.b.b().a(var4);
         if(this.i != null) {
            this.e = var5;
            this.f = this.b.a(this.i);
            this.g = 0;
         }
      }

      this.h = null;

      while(true) {
         var3 = var2;
         if(var2) {
            return var3;
         }

         var3 = var2;
         if(!this.c()) {
            return var3;
         }

         List var6 = this.f;
         int var1 = this.g;
         this.g = var1 + 1;
         this.h = ((io.intercom.com.bumptech.glide.load.b.m)var6.get(var1)).a(this.i, this.b.g(), this.b.h(), this.b.e());
         if(this.h != null && this.b.a(this.h.c.d())) {
            var2 = true;
            this.h.c.a(this.b.d(), this);
         }
      }
   }

   public void b() {
      io.intercom.com.bumptech.glide.load.b.m.a var1 = this.h;
      if(var1 != null) {
         var1.c.b();
      }

   }
}
