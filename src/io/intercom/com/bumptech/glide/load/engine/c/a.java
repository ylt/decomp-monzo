package io.intercom.com.bumptech.glide.load.engine.c;

import android.os.Process;
import android.os.StrictMode;
import android.os.StrictMode.ThreadPolicy;
import android.os.StrictMode.ThreadPolicy.Builder;
import android.util.Log;
import java.io.File;
import java.io.FilenameFilter;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

public final class a extends ThreadPoolExecutor {
   private static final long b;
   private final boolean a;

   static {
      b = TimeUnit.SECONDS.toMillis(10L);
   }

   a(int var1, int var2, long var3, String var5, a.b var6, boolean var7, boolean var8) {
      this(var1, var2, var3, var5, var6, var7, var8, new PriorityBlockingQueue());
   }

   a(int var1, int var2, long var3, String var5, a.b var6, boolean var7, boolean var8, BlockingQueue var9) {
      super(var1, var2, var3, TimeUnit.MILLISECONDS, var9, new a.a(var5, var6, var7));
      this.a = var8;
   }

   a(int var1, String var2, a.b var3, boolean var4, boolean var5) {
      this(var1, var1, 0L, var2, var3, var4, var5);
   }

   public static a a() {
      return a(1, "disk-cache", a.b.d);
   }

   public static a a(int var0, String var1, a.b var2) {
      return new a(var0, var1, var2, true, false);
   }

   private Future a(Future param1) {
      // $FF: Couldn't be decompiled
   }

   public static a b() {
      return b(d(), "source", a.b.d);
   }

   public static a b(int var0, String var1, a.b var2) {
      return new a(var0, var1, var2, false, false);
   }

   public static a c() {
      return new a(0, Integer.MAX_VALUE, b, "source-unlimited", a.b.d, false, false, new SynchronousQueue());
   }

   public static int d() {
      ThreadPolicy var2 = StrictMode.allowThreadDiskReads();

      File[] var9;
      label60: {
         try {
            File var1 = new File("/sys/devices/system/cpu/");
            final Pattern var4 = Pattern.compile("cpu[0-9]+");
            FilenameFilter var3 = new FilenameFilter() {
               public boolean accept(File var1, String var2) {
                  return var4.matcher(var2).matches();
               }
            };
            var9 = var1.listFiles(var3);
            break label60;
         } catch (Throwable var7) {
            if(Log.isLoggable("GlideExecutor", 6)) {
               Log.e("GlideExecutor", "Failed to calculate accurate cpu count", var7);
            }
         } finally {
            StrictMode.setThreadPolicy(var2);
         }

         var9 = null;
      }

      int var0;
      if(var9 != null) {
         var0 = var9.length;
      } else {
         var0 = 0;
      }

      return Math.min(4, Math.max(Math.max(1, Runtime.getRuntime().availableProcessors()), var0));
   }

   public void execute(Runnable var1) {
      if(this.a) {
         var1.run();
      } else {
         super.execute(var1);
      }

   }

   public Future submit(Runnable var1) {
      return this.a(super.submit(var1));
   }

   public Future submit(Runnable var1, Object var2) {
      return this.a(super.submit(var1, var2));
   }

   public Future submit(Callable var1) {
      return this.a(super.submit(var1));
   }

   private static final class a implements ThreadFactory {
      final a.b a;
      final boolean b;
      private final String c;
      private int d;

      a(String var1, a.b var2, boolean var3) {
         this.c = var1;
         this.a = var2;
         this.b = var3;
      }

      public Thread newThread(Runnable var1) {
         synchronized(this){}

         Thread var2;
         try {
            StringBuilder var3 = new StringBuilder();
            var2 = new Thread(var1, var3.append("glide-").append(this.c).append("-thread-").append(this.d).toString()) {
               public void run() {
                  Process.setThreadPriority(9);
                  if(a.this.b) {
                     StrictMode.setThreadPolicy((new Builder()).detectNetwork().penaltyDeath().build());
                  }

                  try {
                     super.run();
                  } catch (Throwable var2) {
                     a.this.a.a(var2);
                  }

               }
            };
            ++this.d;
         } finally {
            ;
         }

         return var2;
      }
   }

   public static enum b {
      a,
      b {
         protected void a(Throwable var1) {
            if(var1 != null && Log.isLoggable("GlideExecutor", 6)) {
               Log.e("GlideExecutor", "Request threw uncaught throwable", var1);
            }

         }
      },
      c {
         protected void a(Throwable var1) {
            super.a(var1);
            if(var1 != null) {
               throw new RuntimeException("Request threw uncaught throwable", var1);
            }
         }
      };

      public static final a.b d = b;

      private b() {
      }

      // $FF: synthetic method
      b(Object var3) {
         this();
      }

      protected void a(Throwable var1) {
      }
   }
}
