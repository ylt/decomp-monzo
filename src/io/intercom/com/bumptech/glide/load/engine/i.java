package io.intercom.com.bumptech.glide.load.engine;

import android.os.Looper;
import android.os.MessageQueue.IdleHandler;
import android.util.Log;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

public class i implements io.intercom.com.bumptech.glide.load.engine.b.h.a, k, n.a {
   private final Map a;
   private final m b;
   private final io.intercom.com.bumptech.glide.load.engine.b.h c;
   private final i.b d;
   private final Map e;
   private final u f;
   private final i.c g;
   private final i.a h;
   private ReferenceQueue i;

   public i(io.intercom.com.bumptech.glide.load.engine.b.h var1, io.intercom.com.bumptech.glide.load.engine.b.a.a var2, io.intercom.com.bumptech.glide.load.engine.c.a var3, io.intercom.com.bumptech.glide.load.engine.c.a var4, io.intercom.com.bumptech.glide.load.engine.c.a var5) {
      this(var1, var2, var3, var4, var5, (Map)null, (m)null, (Map)null, (i.b)null, (i.a)null, (u)null);
   }

   i(io.intercom.com.bumptech.glide.load.engine.b.h var1, io.intercom.com.bumptech.glide.load.engine.b.a.a var2, io.intercom.com.bumptech.glide.load.engine.c.a var3, io.intercom.com.bumptech.glide.load.engine.c.a var4, io.intercom.com.bumptech.glide.load.engine.c.a var5, Map var6, m var7, Map var8, i.b var9, i.a var10, u var11) {
      this.c = var1;
      this.g = new i.c(var2);
      Object var12 = var8;
      if(var8 == null) {
         var12 = new HashMap();
      }

      this.e = (Map)var12;
      m var13 = var7;
      if(var7 == null) {
         var13 = new m();
      }

      this.b = var13;
      var12 = var6;
      if(var6 == null) {
         var12 = new HashMap();
      }

      this.a = (Map)var12;
      i.b var14 = var9;
      if(var9 == null) {
         var14 = new i.b(var3, var4, var5, this);
      }

      this.d = var14;
      i.a var15 = var10;
      if(var10 == null) {
         var15 = new i.a(this.g);
      }

      this.h = var15;
      u var16 = var11;
      if(var11 == null) {
         var16 = new u();
      }

      this.f = var16;
      var1.a((io.intercom.com.bumptech.glide.load.engine.b.h.a)this);
   }

   private n a(io.intercom.com.bumptech.glide.load.g var1) {
      r var2 = this.c.a(var1);
      n var3;
      if(var2 == null) {
         var3 = null;
      } else if(var2 instanceof n) {
         var3 = (n)var2;
      } else {
         var3 = new n(var2, true);
      }

      return var3;
   }

   private n a(io.intercom.com.bumptech.glide.load.g var1, boolean var2) {
      WeakReference var3 = null;
      n var4;
      if(!var2) {
         var4 = var3;
      } else {
         var3 = (WeakReference)this.e.get(var1);
         if(var3 != null) {
            n var5 = (n)var3.get();
            if(var5 != null) {
               var5.f();
               var4 = var5;
            } else {
               this.e.remove(var1);
               var4 = var5;
            }
         } else {
            var4 = null;
         }
      }

      return var4;
   }

   private ReferenceQueue a() {
      if(this.i == null) {
         this.i = new ReferenceQueue();
         Looper.myQueue().addIdleHandler(new i.e(this.e, this.i));
      }

      return this.i;
   }

   private static void a(String var0, long var1, io.intercom.com.bumptech.glide.load.g var3) {
      Log.v("Engine", var0 + " in " + io.intercom.com.bumptech.glide.h.d.a(var1) + "ms, key: " + var3);
   }

   private n b(io.intercom.com.bumptech.glide.load.g var1, boolean var2) {
      n var3;
      if(!var2) {
         var3 = null;
      } else {
         n var4 = this.a(var1);
         var3 = var4;
         if(var4 != null) {
            var4.f();
            this.e.put(var1, new i.f(var1, var4, this.a()));
            var3 = var4;
         }
      }

      return var3;
   }

   public i.d a(io.intercom.com.bumptech.glide.e var1, Object var2, io.intercom.com.bumptech.glide.load.g var3, int var4, int var5, Class var6, Class var7, io.intercom.com.bumptech.glide.g var8, h var9, Map var10, boolean var11, boolean var12, io.intercom.com.bumptech.glide.load.i var13, boolean var14, boolean var15, boolean var16, io.intercom.com.bumptech.glide.f.g var17) {
      io.intercom.com.bumptech.glide.h.i.a();
      long var18 = io.intercom.com.bumptech.glide.h.d.a();
      l var20 = this.b.a(var2, var3, var4, var5, var10, var6, var7, var13);
      n var21 = this.b(var20, var14);
      i.d var23;
      if(var21 != null) {
         var17.a(var21, io.intercom.com.bumptech.glide.load.a.e);
         if(Log.isLoggable("Engine", 2)) {
            a("Loaded resource from cache", var18, var20);
         }

         var23 = null;
      } else {
         var21 = this.a(var20, var14);
         if(var21 != null) {
            var17.a(var21, io.intercom.com.bumptech.glide.load.a.e);
            if(Log.isLoggable("Engine", 2)) {
               a("Loaded resource from active resources", var18, var20);
            }

            var23 = null;
         } else {
            j var22 = (j)this.a.get(var20);
            if(var22 != null) {
               var22.a(var17);
               if(Log.isLoggable("Engine", 2)) {
                  a("Added to existing load", var18, var20);
               }

               var23 = new i.d(var17, var22);
            } else {
               var22 = this.d.a(var20, var14, var15);
               f var24 = this.h.a(var1, var2, var20, var3, var4, var5, var6, var7, var8, var9, var10, var11, var12, var16, var13, var22);
               this.a.put(var20, var22);
               var22.a(var17);
               var22.b(var24);
               if(Log.isLoggable("Engine", 2)) {
                  a("Started new load", var18, var20);
               }

               var23 = new i.d(var17, var22);
            }
         }
      }

      return var23;
   }

   public void a(j var1, io.intercom.com.bumptech.glide.load.g var2) {
      io.intercom.com.bumptech.glide.h.i.a();
      if(var1.equals((j)this.a.get(var2))) {
         this.a.remove(var2);
      }

   }

   public void a(r var1) {
      io.intercom.com.bumptech.glide.h.i.a();
      if(var1 instanceof n) {
         ((n)var1).g();
      } else {
         throw new IllegalArgumentException("Cannot release anything but an EngineResource");
      }
   }

   public void a(io.intercom.com.bumptech.glide.load.g var1, n var2) {
      io.intercom.com.bumptech.glide.h.i.a();
      if(var2 != null) {
         var2.a(var1, this);
         if(var2.a()) {
            this.e.put(var1, new i.f(var1, var2, this.a()));
         }
      }

      this.a.remove(var1);
   }

   public void b(r var1) {
      io.intercom.com.bumptech.glide.h.i.a();
      this.f.a(var1);
   }

   public void b(io.intercom.com.bumptech.glide.load.g var1, n var2) {
      io.intercom.com.bumptech.glide.h.i.a();
      this.e.remove(var1);
      if(var2.a()) {
         this.c.b(var1, var2);
      } else {
         this.f.a(var2);
      }

   }

   static class a {
      final f.d a;
      final android.support.v4.g.k.a b = io.intercom.com.bumptech.glide.h.a.a.a(150, new io.intercom.com.bumptech.glide.h.a.a.a() {
         public f a() {
            return new f(a.this.a, a.this.b);
         }

         // $FF: synthetic method
         public Object b() {
            return this.a();
         }
      });
      private int c;

      a(f.d var1) {
         this.a = var1;
      }

      f a(io.intercom.com.bumptech.glide.e var1, Object var2, l var3, io.intercom.com.bumptech.glide.load.g var4, int var5, int var6, Class var7, Class var8, io.intercom.com.bumptech.glide.g var9, h var10, Map var11, boolean var12, boolean var13, boolean var14, io.intercom.com.bumptech.glide.load.i var15, f.a var16) {
         f var18 = (f)this.b.a();
         int var17 = this.c;
         this.c = var17 + 1;
         return var18.a(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10, var11, var12, var13, var14, var15, var16, var17);
      }
   }

   static class b {
      final io.intercom.com.bumptech.glide.load.engine.c.a a;
      final io.intercom.com.bumptech.glide.load.engine.c.a b;
      final io.intercom.com.bumptech.glide.load.engine.c.a c;
      final k d;
      final android.support.v4.g.k.a e = io.intercom.com.bumptech.glide.h.a.a.a(150, new io.intercom.com.bumptech.glide.h.a.a.a() {
         public j a() {
            return new j(b.this.a, b.this.b, b.this.c, b.this.d, b.this.e);
         }

         // $FF: synthetic method
         public Object b() {
            return this.a();
         }
      });

      b(io.intercom.com.bumptech.glide.load.engine.c.a var1, io.intercom.com.bumptech.glide.load.engine.c.a var2, io.intercom.com.bumptech.glide.load.engine.c.a var3, k var4) {
         this.a = var1;
         this.b = var2;
         this.c = var3;
         this.d = var4;
      }

      j a(io.intercom.com.bumptech.glide.load.g var1, boolean var2, boolean var3) {
         return ((j)this.e.a()).a(var1, var2, var3);
      }
   }

   private static class c implements f.d {
      private final io.intercom.com.bumptech.glide.load.engine.b.a.a a;
      private volatile io.intercom.com.bumptech.glide.load.engine.b.a b;

      public c(io.intercom.com.bumptech.glide.load.engine.b.a.a var1) {
         this.a = var1;
      }

      public io.intercom.com.bumptech.glide.load.engine.b.a a() {
         // $FF: Couldn't be decompiled
      }
   }

   public static class d {
      private final j a;
      private final io.intercom.com.bumptech.glide.f.g b;

      public d(io.intercom.com.bumptech.glide.f.g var1, j var2) {
         this.b = var1;
         this.a = var2;
      }

      public void a() {
         this.a.b(this.b);
      }
   }

   private static class e implements IdleHandler {
      private final Map a;
      private final ReferenceQueue b;

      public e(Map var1, ReferenceQueue var2) {
         this.a = var1;
         this.b = var2;
      }

      public boolean queueIdle() {
         i.f var1 = (i.f)this.b.poll();
         if(var1 != null) {
            this.a.remove(var1.a);
         }

         return true;
      }
   }

   private static class f extends WeakReference {
      final io.intercom.com.bumptech.glide.load.g a;

      public f(io.intercom.com.bumptech.glide.load.g var1, n var2, ReferenceQueue var3) {
         super(var2, var3);
         this.a = var1;
      }
   }
}
