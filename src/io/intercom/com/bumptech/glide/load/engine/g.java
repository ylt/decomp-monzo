package io.intercom.com.bumptech.glide.load.engine;

import android.util.Log;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class g {
   private final Class a;
   private final List b;
   private final io.intercom.com.bumptech.glide.load.resource.e.d c;
   private final android.support.v4.g.k.a d;
   private final String e;

   public g(Class var1, Class var2, Class var3, List var4, io.intercom.com.bumptech.glide.load.resource.e.d var5, android.support.v4.g.k.a var6) {
      this.a = var1;
      this.b = var4;
      this.c = var5;
      this.d = var6;
      this.e = "Failed DecodePath{" + var1.getSimpleName() + "->" + var2.getSimpleName() + "->" + var3.getSimpleName() + "}";
   }

   private r a(io.intercom.com.bumptech.glide.load.a.c var1, int var2, int var3, io.intercom.com.bumptech.glide.load.i var4) throws GlideException {
      List var5 = (List)this.d.a();

      r var8;
      try {
         var8 = this.a(var1, var2, var3, var4, var5);
      } finally {
         this.d.a(var5);
      }

      return var8;
   }

   private r a(io.intercom.com.bumptech.glide.load.a.c var1, int var2, int var3, io.intercom.com.bumptech.glide.load.i var4, List var5) throws GlideException {
      r var8 = null;
      int var7 = this.b.size();
      int var6 = 0;

      r var9;
      while(true) {
         var9 = var8;
         if(var6 >= var7) {
            break;
         }

         io.intercom.com.bumptech.glide.load.j var10 = (io.intercom.com.bumptech.glide.load.j)this.b.get(var6);

         label37: {
            try {
               if(!var10.a(var1.a(), var4)) {
                  break label37;
               }

               var9 = var10.a(var1.a(), var2, var3, var4);
            } catch (IOException var11) {
               if(Log.isLoggable("DecodePath", 2)) {
                  Log.v("DecodePath", "Failed to decode data for " + var10, var11);
               }

               var5.add(var11);
               break label37;
            }

            var8 = var9;
         }

         if(var8 != null) {
            var9 = var8;
            break;
         }

         ++var6;
      }

      if(var9 == null) {
         throw new GlideException(this.e, new ArrayList(var5));
      } else {
         return var9;
      }
   }

   public r a(io.intercom.com.bumptech.glide.load.a.c var1, int var2, int var3, io.intercom.com.bumptech.glide.load.i var4, g.a var5) throws GlideException {
      r var6 = var5.a(this.a(var1, var2, var3, var4));
      return this.c.a(var6);
   }

   public String toString() {
      return "DecodePath{ dataClass=" + this.a + ", decoders=" + this.b + ", transcoder=" + this.c + '}';
   }

   interface a {
      r a(r var1);
   }
}
