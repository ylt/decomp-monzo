package io.intercom.com.bumptech.glide.load.engine;

import android.util.Log;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public final class GlideException extends Exception {
   private static final StackTraceElement[] a = new StackTraceElement[0];
   private final List b;
   private io.intercom.com.bumptech.glide.load.g c;
   private io.intercom.com.bumptech.glide.load.a d;
   private Class e;

   public GlideException(String var1) {
      this(var1, Collections.emptyList());
   }

   public GlideException(String var1, Exception var2) {
      this(var1, Collections.singletonList(var2));
   }

   public GlideException(String var1, List var2) {
      super(var1);
      this.setStackTrace(a);
      this.b = var2;
   }

   private void a(Appendable var1) {
      a((Exception)this, (Appendable)var1);
      a((List)this.a(), (Appendable)(new GlideException.a(var1)));
   }

   private static void a(Exception var0, Appendable var1) {
      try {
         var1.append(var0.getClass().toString()).append(": ").append(var0.getMessage()).append('\n');
      } catch (IOException var2) {
         throw new RuntimeException(var0);
      }
   }

   private void a(Exception var1, List var2) {
      if(var1 instanceof GlideException) {
         Iterator var3 = ((GlideException)var1).a().iterator();

         while(var3.hasNext()) {
            this.a((Exception)var3.next(), var2);
         }
      } else {
         var2.add(var1);
      }

   }

   private static void a(List var0, Appendable var1) {
      try {
         b(var0, var1);
      } catch (IOException var2) {
         throw new RuntimeException(var2);
      }
   }

   private static void b(List var0, Appendable var1) throws IOException {
      int var3 = var0.size();

      for(int var2 = 0; var2 < var3; ++var2) {
         var1.append("Cause (").append(String.valueOf(var2 + 1)).append(" of ").append(String.valueOf(var3)).append("): ");
         Exception var4 = (Exception)var0.get(var2);
         if(var4 instanceof GlideException) {
            ((GlideException)var4).a(var1);
         } else {
            a(var4, var1);
         }
      }

   }

   public List a() {
      return this.b;
   }

   void a(io.intercom.com.bumptech.glide.load.g var1, io.intercom.com.bumptech.glide.load.a var2) {
      this.a(var1, var2, (Class)null);
   }

   void a(io.intercom.com.bumptech.glide.load.g var1, io.intercom.com.bumptech.glide.load.a var2, Class var3) {
      this.c = var1;
      this.d = var2;
      this.e = var3;
   }

   public void a(String var1) {
      List var4 = this.b();
      int var3 = var4.size();

      for(int var2 = 0; var2 < var3; ++var2) {
         Log.i(var1, "Root cause (" + (var2 + 1) + " of " + var3 + ")", (Throwable)var4.get(var2));
      }

   }

   public List b() {
      ArrayList var1 = new ArrayList();
      this.a((Exception)this, (List)var1);
      return var1;
   }

   public Throwable fillInStackTrace() {
      return this;
   }

   public String getMessage() {
      StringBuilder var2 = (new StringBuilder()).append(super.getMessage());
      String var1;
      if(this.e != null) {
         var1 = ", " + this.e;
      } else {
         var1 = "";
      }

      var2 = var2.append(var1);
      if(this.d != null) {
         var1 = ", " + this.d;
      } else {
         var1 = "";
      }

      var2 = var2.append(var1);
      if(this.c != null) {
         var1 = ", " + this.c;
      } else {
         var1 = "";
      }

      return var2.append(var1).toString();
   }

   public void printStackTrace() {
      this.printStackTrace(System.err);
   }

   public void printStackTrace(PrintStream var1) {
      this.a((Appendable)var1);
   }

   public void printStackTrace(PrintWriter var1) {
      this.a((Appendable)var1);
   }

   private static final class a implements Appendable {
      private final Appendable a;
      private boolean b = true;

      a(Appendable var1) {
         this.a = var1;
      }

      private CharSequence a(CharSequence var1) {
         Object var2 = var1;
         if(var1 == null) {
            var2 = "";
         }

         return (CharSequence)var2;
      }

      public Appendable append(char var1) throws IOException {
         boolean var2 = false;
         if(this.b) {
            this.b = false;
            this.a.append("  ");
         }

         if(var1 == 10) {
            var2 = true;
         }

         this.b = var2;
         this.a.append(var1);
         return this;
      }

      public Appendable append(CharSequence var1) throws IOException {
         var1 = this.a(var1);
         return this.append(var1, 0, var1.length());
      }

      public Appendable append(CharSequence var1, int var2, int var3) throws IOException {
         boolean var5 = false;
         var1 = this.a(var1);
         if(this.b) {
            this.b = false;
            this.a.append("  ");
         }

         boolean var4 = var5;
         if(var1.length() > 0) {
            var4 = var5;
            if(var1.charAt(var3 - 1) == 10) {
               var4 = true;
            }
         }

         this.b = var4;
         this.a.append(var1, var2, var3);
         return this;
      }
   }
}
