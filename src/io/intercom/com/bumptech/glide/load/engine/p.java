package io.intercom.com.bumptech.glide.load.engine;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class p {
   private final Class a;
   private final android.support.v4.g.k.a b;
   private final List c;
   private final String d;

   public p(Class var1, Class var2, Class var3, List var4, android.support.v4.g.k.a var5) {
      this.a = var1;
      this.b = var5;
      this.c = (List)io.intercom.com.bumptech.glide.h.h.a((Collection)var4);
      this.d = "Failed LoadPath{" + var1.getSimpleName() + "->" + var2.getSimpleName() + "->" + var3.getSimpleName() + "}";
   }

   private r a(io.intercom.com.bumptech.glide.load.a.c var1, io.intercom.com.bumptech.glide.load.i var2, int var3, int var4, g.a var5, List var6) throws GlideException {
      int var8 = this.c.size();
      r var9 = null;

      for(int var7 = 0; var7 < var8; ++var7) {
         g var10 = (g)this.c.get(var7);

         label27: {
            r var12;
            try {
               var12 = var10.a(var1, var3, var4, var2, var5);
            } catch (GlideException var11) {
               var6.add(var11);
               break label27;
            }

            var9 = var12;
         }

         if(var9 != null) {
            break;
         }
      }

      if(var9 == null) {
         throw new GlideException(this.d, new ArrayList(var6));
      } else {
         return var9;
      }
   }

   public r a(io.intercom.com.bumptech.glide.load.a.c var1, io.intercom.com.bumptech.glide.load.i var2, int var3, int var4, g.a var5) throws GlideException {
      List var6 = (List)this.b.a();

      r var9;
      try {
         var9 = this.a(var1, var2, var3, var4, var5, var6);
      } finally {
         this.b.a(var6);
      }

      return var9;
   }

   public String toString() {
      return "LoadPath{decodePaths=" + Arrays.toString(this.c.toArray(new g[this.c.size()])) + '}';
   }
}
