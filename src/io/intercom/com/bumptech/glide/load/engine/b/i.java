package io.intercom.com.bumptech.glide.load.engine.b;

import android.app.ActivityManager;
import android.content.Context;
import android.os.Build.VERSION;
import android.text.format.Formatter;
import android.util.DisplayMetrics;
import android.util.Log;

public final class i {
   private final int a;
   private final int b;
   private final Context c;
   private final int d;

   i(i.a var1) {
      this.c = var1.b;
      int var3;
      if(b(var1.c)) {
         var3 = var1.i / 2;
      } else {
         var3 = var1.i;
      }

      this.d = var3;
      int var4 = a(var1.c, var1.g, var1.h);
      int var5 = var1.d.a() * var1.d.b() * 4;
      var3 = Math.round((float)var5 * var1.f);
      var5 = Math.round((float)var5 * var1.e);
      int var6 = var4 - this.d;
      if(var5 + var3 <= var6) {
         this.b = var5;
         this.a = var3;
      } else {
         float var2 = (float)var6 / (var1.f + var1.e);
         this.b = Math.round(var1.e * var2);
         this.a = Math.round(var2 * var1.f);
      }

      if(Log.isLoggable("MemorySizeCalculator", 3)) {
         StringBuilder var8 = (new StringBuilder()).append("Calculation complete, Calculated memory cache size: ").append(this.a(this.b)).append(", pool size: ").append(this.a(this.a)).append(", byte array size: ").append(this.a(this.d)).append(", memory class limited? ");
         boolean var7;
         if(var5 + var3 > var4) {
            var7 = true;
         } else {
            var7 = false;
         }

         Log.d("MemorySizeCalculator", var8.append(var7).append(", max size: ").append(this.a(var4)).append(", memoryClass: ").append(var1.c.getMemoryClass()).append(", isLowMemoryDevice: ").append(b(var1.c)).toString());
      }

   }

   private static int a(ActivityManager var0, float var1, float var2) {
      int var4 = var0.getMemoryClass();
      boolean var5 = b(var0);
      float var3 = (float)(var4 * 1024 * 1024);
      if(!var5) {
         var2 = var1;
      }

      return Math.round(var3 * var2);
   }

   private String a(int var1) {
      return Formatter.formatFileSize(this.c, (long)var1);
   }

   private static boolean b(ActivityManager var0) {
      boolean var1;
      if(VERSION.SDK_INT >= 19) {
         var1 = var0.isLowRamDevice();
      } else {
         var1 = false;
      }

      return var1;
   }

   public int a() {
      return this.b;
   }

   public int b() {
      return this.a;
   }

   public int c() {
      return this.d;
   }

   public static final class a {
      static final int a;
      private final Context b;
      private ActivityManager c;
      private i.c d;
      private float e = 2.0F;
      private float f;
      private float g;
      private float h;
      private int i;

      static {
         byte var0;
         if(VERSION.SDK_INT > 26) {
            var0 = 4;
         } else {
            var0 = 1;
         }

         a = var0;
      }

      public a(Context var1) {
         this.f = (float)a;
         this.g = 0.4F;
         this.h = 0.33F;
         this.i = 4194304;
         this.b = var1;
         this.c = (ActivityManager)var1.getSystemService("activity");
         this.d = new i.b(var1.getResources().getDisplayMetrics());
         if(VERSION.SDK_INT >= 26 && i.b(this.c)) {
            this.f = 0.0F;
         }

      }

      public i a() {
         return new i(this);
      }
   }

   private static final class b implements i.c {
      private final DisplayMetrics a;

      public b(DisplayMetrics var1) {
         this.a = var1;
      }

      public int a() {
         return this.a.widthPixels;
      }

      public int b() {
         return this.a.heightPixels;
      }
   }

   interface c {
      int a();

      int b();
   }
}
