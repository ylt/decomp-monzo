package io.intercom.com.bumptech.glide.load.engine.b;

import android.content.Context;
import java.io.File;

public final class f extends d {
   public f(Context var1) {
      this(var1, "image_manager_disk_cache", 262144000);
   }

   public f(final Context var1, final String var2, int var3) {
      super(new d.a() {
         public File a() {
            File var1x = var1.getCacheDir();
            if(var1x == null) {
               var1x = null;
            } else if(var2 != null) {
               var1x = new File(var1x, var2);
            }

            return var1x;
         }
      }, var3);
   }
}
