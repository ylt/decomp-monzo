package io.intercom.com.bumptech.glide.load.engine.b;

import java.io.File;

public class d implements a.a {
   private final int a;
   private final d.a b;

   public d(d.a var1, int var2) {
      this.a = var2;
      this.b = var1;
   }

   public a a() {
      Object var2 = null;
      File var3 = this.b.a();
      a var1;
      if(var3 == null) {
         var1 = (a)var2;
      } else {
         if(!var3.mkdirs()) {
            var1 = (a)var2;
            if(!var3.exists()) {
               return var1;
            }

            var1 = (a)var2;
            if(!var3.isDirectory()) {
               return var1;
            }
         }

         var1 = e.a(var3, this.a);
      }

      return var1;
   }

   public interface a {
      File a();
   }
}
