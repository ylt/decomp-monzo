package io.intercom.com.bumptech.glide.load.engine;

import java.nio.ByteBuffer;
import java.security.MessageDigest;

final class t implements io.intercom.com.bumptech.glide.load.g {
   private static final io.intercom.com.bumptech.glide.h.e b = new io.intercom.com.bumptech.glide.h.e(50);
   private final io.intercom.com.bumptech.glide.load.g c;
   private final io.intercom.com.bumptech.glide.load.g d;
   private final int e;
   private final int f;
   private final Class g;
   private final io.intercom.com.bumptech.glide.load.i h;
   private final io.intercom.com.bumptech.glide.load.l i;

   public t(io.intercom.com.bumptech.glide.load.g var1, io.intercom.com.bumptech.glide.load.g var2, int var3, int var4, io.intercom.com.bumptech.glide.load.l var5, Class var6, io.intercom.com.bumptech.glide.load.i var7) {
      this.c = var1;
      this.d = var2;
      this.e = var3;
      this.f = var4;
      this.i = var5;
      this.g = var6;
      this.h = var7;
   }

   private byte[] a() {
      byte[] var2 = (byte[])b.b(this.g);
      byte[] var1 = var2;
      if(var2 == null) {
         var1 = this.g.getName().getBytes(a);
         b.b(this.g, var1);
      }

      return var1;
   }

   public boolean equals(Object var1) {
      boolean var3 = false;
      boolean var2 = var3;
      if(var1 instanceof t) {
         t var4 = (t)var1;
         var2 = var3;
         if(this.f == var4.f) {
            var2 = var3;
            if(this.e == var4.e) {
               var2 = var3;
               if(io.intercom.com.bumptech.glide.h.i.a((Object)this.i, (Object)var4.i)) {
                  var2 = var3;
                  if(this.g.equals(var4.g)) {
                     var2 = var3;
                     if(this.c.equals(var4.c)) {
                        var2 = var3;
                        if(this.d.equals(var4.d)) {
                           var2 = var3;
                           if(this.h.equals(var4.h)) {
                              var2 = true;
                           }
                        }
                     }
                  }
               }
            }
         }
      }

      return var2;
   }

   public int hashCode() {
      int var2 = ((this.c.hashCode() * 31 + this.d.hashCode()) * 31 + this.e) * 31 + this.f;
      int var1 = var2;
      if(this.i != null) {
         var1 = var2 * 31 + this.i.hashCode();
      }

      return (var1 * 31 + this.g.hashCode()) * 31 + this.h.hashCode();
   }

   public String toString() {
      return "ResourceCacheKey{sourceKey=" + this.c + ", signature=" + this.d + ", width=" + this.e + ", height=" + this.f + ", decodedResourceClass=" + this.g + ", transformation='" + this.i + '\'' + ", options=" + this.h + '}';
   }

   public void updateDiskCacheKey(MessageDigest var1) {
      byte[] var2 = ByteBuffer.allocate(8).putInt(this.e).putInt(this.f).array();
      this.d.updateDiskCacheKey(var1);
      this.c.updateDiskCacheKey(var1);
      var1.update(var2);
      if(this.i != null) {
         this.i.updateDiskCacheKey(var1);
      }

      this.h.updateDiskCacheKey(var1);
      var1.update(this.a());
   }
}
