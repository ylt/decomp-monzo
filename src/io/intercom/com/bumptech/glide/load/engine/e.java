package io.intercom.com.bumptech.glide.load.engine;

import io.intercom.com.bumptech.glide.Registry;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

final class e {
   private final List a = new ArrayList();
   private final List b = new ArrayList();
   private io.intercom.com.bumptech.glide.e c;
   private Object d;
   private int e;
   private int f;
   private Class g;
   private f.d h;
   private io.intercom.com.bumptech.glide.load.i i;
   private Map j;
   private Class k;
   private boolean l;
   private boolean m;
   private io.intercom.com.bumptech.glide.load.g n;
   private io.intercom.com.bumptech.glide.g o;
   private h p;
   private boolean q;
   private boolean r;

   io.intercom.com.bumptech.glide.load.d a(Object var1) throws Registry.NoSourceEncoderAvailableException {
      return this.c.d().a(var1);
   }

   e a(io.intercom.com.bumptech.glide.e var1, Object var2, io.intercom.com.bumptech.glide.load.g var3, int var4, int var5, h var6, Class var7, Class var8, io.intercom.com.bumptech.glide.g var9, io.intercom.com.bumptech.glide.load.i var10, Map var11, boolean var12, boolean var13, f.d var14) {
      this.c = var1;
      this.d = var2;
      this.n = var3;
      this.e = var4;
      this.f = var5;
      this.p = var6;
      this.g = var7;
      this.h = var14;
      this.k = var8;
      this.o = var9;
      this.i = var10;
      this.j = var11;
      this.q = var12;
      this.r = var13;
      return this;
   }

   List a(File var1) throws Registry.NoModelLoaderAvailableException {
      return this.c.d().c(var1);
   }

   void a() {
      this.c = null;
      this.d = null;
      this.n = null;
      this.g = null;
      this.k = null;
      this.i = null;
      this.o = null;
      this.j = null;
      this.p = null;
      this.a.clear();
      this.l = false;
      this.b.clear();
      this.m = false;
   }

   boolean a(r var1) {
      return this.c.d().a(var1);
   }

   boolean a(io.intercom.com.bumptech.glide.load.g var1) {
      List var5 = this.k();
      int var3 = var5.size();
      int var2 = 0;

      boolean var4;
      while(true) {
         if(var2 >= var3) {
            var4 = false;
            break;
         }

         if(((io.intercom.com.bumptech.glide.load.b.m.a)var5.get(var2)).a.equals(var1)) {
            var4 = true;
            break;
         }

         ++var2;
      }

      return var4;
   }

   boolean a(Class var1) {
      boolean var2;
      if(this.b(var1) != null) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   io.intercom.com.bumptech.glide.load.engine.b.a b() {
      return this.h.a();
   }

   p b(Class var1) {
      return this.c.d().a(var1, this.g, this.k);
   }

   io.intercom.com.bumptech.glide.load.k b(r var1) {
      return this.c.d().b(var1);
   }

   h c() {
      return this.p;
   }

   io.intercom.com.bumptech.glide.load.l c(Class var1) {
      io.intercom.com.bumptech.glide.load.l var3 = (io.intercom.com.bumptech.glide.load.l)this.j.get(var1);
      Object var2 = var3;
      if(var3 == null) {
         if(this.j.isEmpty() && this.q) {
            throw new IllegalArgumentException("Missing transformation for " + var1 + ". If you wish to ignore unknown resource types, use the optional transformation methods.");
         }

         var2 = io.intercom.com.bumptech.glide.load.resource.b.a();
      }

      return (io.intercom.com.bumptech.glide.load.l)var2;
   }

   io.intercom.com.bumptech.glide.g d() {
      return this.o;
   }

   io.intercom.com.bumptech.glide.load.i e() {
      return this.i;
   }

   io.intercom.com.bumptech.glide.load.g f() {
      return this.n;
   }

   int g() {
      return this.e;
   }

   int h() {
      return this.f;
   }

   List i() {
      return this.c.d().b(this.d.getClass(), this.g, this.k);
   }

   boolean j() {
      return this.r;
   }

   List k() {
      if(!this.l) {
         this.l = true;
         this.a.clear();
         List var4 = this.c.d().c(this.d);
         int var2 = var4.size();

         for(int var1 = 0; var1 < var2; ++var1) {
            io.intercom.com.bumptech.glide.load.b.m.a var3 = ((io.intercom.com.bumptech.glide.load.b.m)var4.get(var1)).a(this.d, this.e, this.f, this.i);
            if(var3 != null) {
               this.a.add(var3);
            }
         }
      }

      return this.a;
   }

   List l() {
      if(!this.m) {
         this.m = true;
         this.b.clear();
         List var5 = this.k();
         int var3 = var5.size();

         for(int var1 = 0; var1 < var3; ++var1) {
            io.intercom.com.bumptech.glide.load.b.m.a var4 = (io.intercom.com.bumptech.glide.load.b.m.a)var5.get(var1);
            if(!this.b.contains(var4.a)) {
               this.b.add(var4.a);
            }

            for(int var2 = 0; var2 < var4.b.size(); ++var2) {
               if(!this.b.contains(var4.b.get(var2))) {
                  this.b.add(var4.b.get(var2));
               }
            }
         }
      }

      return this.b;
   }
}
