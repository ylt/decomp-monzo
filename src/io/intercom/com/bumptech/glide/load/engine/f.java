package io.intercom.com.bumptech.glide.load.engine;

import android.os.Build.VERSION;
import android.util.Log;
import io.intercom.com.bumptech.glide.Registry;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

class f implements io.intercom.com.bumptech.glide.h.a.a.c, d.a, Comparable, Runnable {
   private io.intercom.com.bumptech.glide.load.a.b A;
   private volatile d B;
   private volatile boolean C;
   private volatile boolean D;
   final e a = new e();
   final f.c b = new f.c();
   io.intercom.com.bumptech.glide.load.g c;
   int d;
   int e;
   h f;
   io.intercom.com.bumptech.glide.load.i g;
   io.intercom.com.bumptech.glide.load.g h;
   private final List i = new ArrayList();
   private final io.intercom.com.bumptech.glide.h.a.b j = io.intercom.com.bumptech.glide.h.a.b.a();
   private final f.d k;
   private final android.support.v4.g.k.a l;
   private final f.e m = new f.e();
   private io.intercom.com.bumptech.glide.e n;
   private io.intercom.com.bumptech.glide.g o;
   private l p;
   private f.a q;
   private int r;
   private f.g s;
   private f.f t;
   private long u;
   private boolean v;
   private Thread w;
   private io.intercom.com.bumptech.glide.load.g x;
   private Object y;
   private io.intercom.com.bumptech.glide.load.a z;

   f(f.d var1, android.support.v4.g.k.a var2) {
      this.k = var1;
      this.l = var2;
   }

   private f.g a(f.g var1) {
      switch(null.b[var1.ordinal()]) {
      case 1:
         if(this.f.b()) {
            var1 = f.g.c;
         } else {
            var1 = this.a(f.g.c);
         }
         break;
      case 2:
         if(this.v) {
            var1 = f.g.f;
         } else {
            var1 = f.g.d;
         }
         break;
      case 3:
      case 4:
         var1 = f.g.f;
         break;
      case 5:
         if(this.f.a()) {
            var1 = f.g.b;
         } else {
            var1 = this.a(f.g.b);
         }
         break;
      default:
         throw new IllegalArgumentException("Unrecognized stage: " + var1);
      }

      return var1;
   }

   private r a(io.intercom.com.bumptech.glide.load.a.b var1, Object var2, io.intercom.com.bumptech.glide.load.a var3) throws GlideException {
      r var8;
      if(var2 == null) {
         var2 = null;
         var1.a();
         var8 = (r)var2;
      } else {
         r var9;
         try {
            long var4 = io.intercom.com.bumptech.glide.h.d.a();
            var9 = this.a(var2, var3);
            if(Log.isLoggable("DecodeJob", 2)) {
               StringBuilder var10 = new StringBuilder();
               this.a(var10.append("Decoded result ").append(var9).toString(), var4);
            }
         } finally {
            var1.a();
         }

         var8 = var9;
      }

      return var8;
   }

   private r a(Object var1, io.intercom.com.bumptech.glide.load.a var2) throws GlideException {
      return this.a(var1, var2, this.a.b(var1.getClass()));
   }

   private r a(Object var1, io.intercom.com.bumptech.glide.load.a var2, p var3) throws GlideException {
      io.intercom.com.bumptech.glide.load.i var6 = this.a(var2);
      io.intercom.com.bumptech.glide.load.a.c var10 = this.n.d().b(var1);

      r var11;
      try {
         int var4 = this.d;
         int var5 = this.e;
         f.b var7 = new f.b(var2);
         var11 = var3.a(var10, var6, var4, var5, var7);
      } finally {
         var10.b();
      }

      return var11;
   }

   private io.intercom.com.bumptech.glide.load.i a(io.intercom.com.bumptech.glide.load.a var1) {
      io.intercom.com.bumptech.glide.load.i var3 = this.g;
      io.intercom.com.bumptech.glide.load.i var2;
      if(VERSION.SDK_INT < 26) {
         var2 = var3;
      } else {
         var2 = var3;
         if(var3.a(io.intercom.com.bumptech.glide.load.resource.bitmap.l.d) == null) {
            if(var1 != io.intercom.com.bumptech.glide.load.a.d) {
               var2 = var3;
               if(!this.a.j()) {
                  return var2;
               }
            }

            var2 = new io.intercom.com.bumptech.glide.load.i();
            var2.a(this.g);
            var2.a(io.intercom.com.bumptech.glide.load.resource.bitmap.l.d, Boolean.valueOf(true));
         }
      }

      return var2;
   }

   private void a(r var1, io.intercom.com.bumptech.glide.load.a var2) {
      this.m();
      this.q.a(var1, var2);
   }

   private void a(String var1, long var2) {
      this.a(var1, var2, (String)null);
   }

   private void a(String var1, long var2, String var4) {
      StringBuilder var5 = (new StringBuilder()).append(var1).append(" in ").append(io.intercom.com.bumptech.glide.h.d.a(var2)).append(", load key: ").append(this.p);
      if(var4 != null) {
         var1 = ", " + var4;
      } else {
         var1 = "";
      }

      Log.v("DecodeJob", var5.append(var1).append(", thread: ").append(Thread.currentThread().getName()).toString());
   }

   private void b(r var1, io.intercom.com.bumptech.glide.load.a var2) {
      if(var1 instanceof o) {
         ((o)var1).a();
      }

      q var3 = null;
      Object var4 = var1;
      if(this.b.a()) {
         var3 = q.a(var1);
         var4 = var3;
      }

      this.a((r)var4, var2);
      this.s = f.g.e;

      try {
         if(this.b.a()) {
            this.b.a(this.k, this.g);
         }
      } finally {
         if(var3 != null) {
            var3.a();
         }

         this.e();
      }

   }

   private void e() {
      if(this.m.a()) {
         this.g();
      }

   }

   private void f() {
      if(this.m.b()) {
         this.g();
      }

   }

   private void g() {
      this.m.c();
      this.b.b();
      this.a.a();
      this.C = false;
      this.n = null;
      this.c = null;
      this.g = null;
      this.o = null;
      this.p = null;
      this.q = null;
      this.s = null;
      this.B = null;
      this.w = null;
      this.h = null;
      this.y = null;
      this.z = null;
      this.A = null;
      this.u = 0L;
      this.D = false;
      this.i.clear();
      this.l.a(this);
   }

   private int h() {
      return this.o.ordinal();
   }

   private void i() {
      switch(null.a[this.t.ordinal()]) {
      case 1:
         this.s = this.a(f.g.a);
         this.B = this.j();
         this.k();
         break;
      case 2:
         this.k();
         break;
      case 3:
         this.n();
         break;
      default:
         throw new IllegalStateException("Unrecognized run reason: " + this.t);
      }

   }

   private d j() {
      Object var1;
      switch(null.b[this.s.ordinal()]) {
      case 1:
         var1 = new s(this.a, this);
         break;
      case 2:
         var1 = new a(this.a, this);
         break;
      case 3:
         var1 = new v(this.a, this);
         break;
      case 4:
         var1 = null;
         break;
      default:
         throw new IllegalStateException("Unrecognized stage: " + this.s);
      }

      return (d)var1;
   }

   private void k() {
      this.w = Thread.currentThread();
      this.u = io.intercom.com.bumptech.glide.h.d.a();
      boolean var1 = false;

      while(true) {
         boolean var2 = var1;
         if(!this.D) {
            var2 = var1;
            if(this.B != null) {
               var1 = this.B.a();
               var2 = var1;
               if(!var1) {
                  this.s = this.a(this.s);
                  this.B = this.j();
                  if(this.s != f.g.d) {
                     continue;
                  }

                  this.c();
                  break;
               }
            }
         }

         if((this.s == f.g.f || this.D) && !var2) {
            this.l();
         }
         break;
      }

   }

   private void l() {
      this.m();
      GlideException var1 = new GlideException("Failed to load resource", new ArrayList(this.i));
      this.q.a(var1);
      this.f();
   }

   private void m() {
      this.j.b();
      if(this.C) {
         throw new IllegalStateException("Already notified");
      } else {
         this.C = true;
      }
   }

   private void n() {
      if(Log.isLoggable("DecodeJob", 2)) {
         this.a("Retrieved data", this.u, "data: " + this.y + ", cache key: " + this.h + ", fetcher: " + this.A);
      }

      r var1;
      try {
         var1 = this.a(this.A, this.y, this.z);
      } catch (GlideException var2) {
         var2.a(this.x, this.z);
         this.i.add(var2);
         var1 = null;
      }

      if(var1 != null) {
         this.b(var1, this.z);
      } else {
         this.k();
      }

   }

   public int a(f var1) {
      int var3 = this.h() - var1.h();
      int var2 = var3;
      if(var3 == 0) {
         var2 = this.r - var1.r;
      }

      return var2;
   }

   f a(io.intercom.com.bumptech.glide.e var1, Object var2, l var3, io.intercom.com.bumptech.glide.load.g var4, int var5, int var6, Class var7, Class var8, io.intercom.com.bumptech.glide.g var9, h var10, Map var11, boolean var12, boolean var13, boolean var14, io.intercom.com.bumptech.glide.load.i var15, f.a var16, int var17) {
      this.a.a(var1, var2, var4, var5, var6, var10, var7, var8, var9, var15, var11, var12, var13, this.k);
      this.n = var1;
      this.c = var4;
      this.o = var9;
      this.p = var3;
      this.d = var5;
      this.e = var6;
      this.f = var10;
      this.v = var14;
      this.g = var15;
      this.q = var16;
      this.r = var17;
      this.t = f.f.a;
      return this;
   }

   public void a(io.intercom.com.bumptech.glide.load.g var1, Exception var2, io.intercom.com.bumptech.glide.load.a.b var3, io.intercom.com.bumptech.glide.load.a var4) {
      var3.a();
      GlideException var5 = new GlideException("Fetching data failed", var2);
      var5.a(var1, var4, var3.d());
      this.i.add(var5);
      if(Thread.currentThread() != this.w) {
         this.t = f.f.b;
         this.q.a(this);
      } else {
         this.k();
      }

   }

   public void a(io.intercom.com.bumptech.glide.load.g var1, Object var2, io.intercom.com.bumptech.glide.load.a.b var3, io.intercom.com.bumptech.glide.load.a var4, io.intercom.com.bumptech.glide.load.g var5) {
      this.h = var1;
      this.y = var2;
      this.A = var3;
      this.z = var4;
      this.x = var5;
      if(Thread.currentThread() != this.w) {
         this.t = f.f.c;
         this.q.a(this);
      } else {
         android.support.v4.os.e.a("DecodeJob.decodeFromRetrievedData");

         try {
            this.n();
         } finally {
            android.support.v4.os.e.a();
         }
      }

   }

   void a(boolean var1) {
      if(this.m.a(var1)) {
         this.g();
      }

   }

   boolean a() {
      f.g var2 = this.a(f.g.a);
      boolean var1;
      if(var2 != f.g.b && var2 != f.g.c) {
         var1 = false;
      } else {
         var1 = true;
      }

      return var1;
   }

   public void b() {
      this.D = true;
      d var1 = this.B;
      if(var1 != null) {
         var1.b();
      }

   }

   public void c() {
      this.t = f.f.b;
      this.q.a(this);
   }

   // $FF: synthetic method
   public int compareTo(Object var1) {
      return this.a((f)var1);
   }

   public io.intercom.com.bumptech.glide.h.a.b m_() {
      return this.j;
   }

   public void run() {
      android.support.v4.os.e.a("DecodeJob#run");

      try {
         if(this.D) {
            this.l();
         } else {
            this.i();
         }
      } catch (RuntimeException var5) {
         if(Log.isLoggable("DecodeJob", 3)) {
            StringBuilder var1 = new StringBuilder();
            Log.d("DecodeJob", var1.append("DecodeJob threw unexpectedly, isCancelled: ").append(this.D).append(", stage: ").append(this.s).toString(), var5);
         }

         if(this.s != f.g.e) {
            this.l();
         }

         if(!this.D) {
            throw var5;
         }
      } finally {
         if(this.A != null) {
            this.A.a();
         }

         android.support.v4.os.e.a();
      }

   }

   interface a {
      void a(GlideException var1);

      void a(f var1);

      void a(r var1, io.intercom.com.bumptech.glide.load.a var2);
   }

   private final class b implements g.a {
      private final io.intercom.com.bumptech.glide.load.a b;

      b(io.intercom.com.bumptech.glide.load.a var2) {
         this.b = var2;
      }

      private Class b(r var1) {
         return var1.c().getClass();
      }

      public r a(r var1) {
         Class var7 = this.b(var1);
         r var3;
         io.intercom.com.bumptech.glide.load.l var4;
         if(this.b != io.intercom.com.bumptech.glide.load.a.d) {
            var4 = f.this.a.c(var7);
            var3 = var4.transform(f.this.n, var1, f.this.d, f.this.e);
         } else {
            var3 = var1;
            var4 = null;
         }

         if(!var1.equals(var3)) {
            var1.e();
         }

         io.intercom.com.bumptech.glide.load.c var5;
         io.intercom.com.bumptech.glide.load.k var8;
         if(f.this.a.a(var3)) {
            var8 = f.this.a.b(var3);
            var5 = var8.a(f.this.g);
         } else {
            var5 = io.intercom.com.bumptech.glide.load.c.c;
            var8 = null;
         }

         boolean var2;
         if(!f.this.a.a(f.this.h)) {
            var2 = true;
         } else {
            var2 = false;
         }

         Object var6 = var3;
         if(f.this.f.a(var2, this.b, var5)) {
            if(var8 == null) {
               throw new Registry.NoResultEncoderAvailableException(var3.c().getClass());
            }

            Object var9;
            if(var5 == io.intercom.com.bumptech.glide.load.c.a) {
               var9 = new b(f.this.h, f.this.c);
            } else {
               if(var5 != io.intercom.com.bumptech.glide.load.c.b) {
                  throw new IllegalArgumentException("Unknown strategy: " + var5);
               }

               var9 = new t(f.this.h, f.this.c, f.this.d, f.this.e, var4, var7, f.this.g);
            }

            var6 = q.a(var3);
            f.this.b.a((io.intercom.com.bumptech.glide.load.g)var9, var8, (q)var6);
         }

         return (r)var6;
      }
   }

   private static class c {
      private io.intercom.com.bumptech.glide.load.g a;
      private io.intercom.com.bumptech.glide.load.k b;
      private q c;

      void a(f.d var1, io.intercom.com.bumptech.glide.load.i var2) {
         android.support.v4.os.e.a("DecodeJob.encode");

         try {
            io.intercom.com.bumptech.glide.load.engine.b.a var7 = var1.a();
            io.intercom.com.bumptech.glide.load.g var3 = this.a;
            c var4 = new c(this.b, this.c, var2);
            var7.a(var3, var4);
         } finally {
            this.c.a();
            android.support.v4.os.e.a();
         }

      }

      void a(io.intercom.com.bumptech.glide.load.g var1, io.intercom.com.bumptech.glide.load.k var2, q var3) {
         this.a = var1;
         this.b = var2;
         this.c = var3;
      }

      boolean a() {
         boolean var1;
         if(this.c != null) {
            var1 = true;
         } else {
            var1 = false;
         }

         return var1;
      }

      void b() {
         this.a = null;
         this.b = null;
         this.c = null;
      }
   }

   interface d {
      io.intercom.com.bumptech.glide.load.engine.b.a a();
   }

   private static class e {
      private boolean a;
      private boolean b;
      private boolean c;

      private boolean b(boolean var1) {
         if((this.c || var1 || this.b) && this.a) {
            var1 = true;
         } else {
            var1 = false;
         }

         return var1;
      }

      boolean a() {
         synchronized(this){}

         boolean var1;
         try {
            this.b = true;
            var1 = this.b(false);
         } finally {
            ;
         }

         return var1;
      }

      boolean a(boolean var1) {
         synchronized(this){}

         try {
            this.a = true;
            var1 = this.b(var1);
         } finally {
            ;
         }

         return var1;
      }

      boolean b() {
         synchronized(this){}

         boolean var1;
         try {
            this.c = true;
            var1 = this.b(false);
         } finally {
            ;
         }

         return var1;
      }

      void c() {
         synchronized(this){}

         try {
            this.b = false;
            this.a = false;
            this.c = false;
         } finally {
            ;
         }

      }
   }

   private static enum f {
      a,
      b,
      c;
   }

   private static enum g {
      a,
      b,
      c,
      d,
      e,
      f;
   }
}
