package io.intercom.com.bumptech.glide.load.engine;

public abstract class h {
   public static final h a = new h() {
      public boolean a() {
         return true;
      }

      public boolean a(io.intercom.com.bumptech.glide.load.a var1) {
         boolean var2;
         if(var1 == io.intercom.com.bumptech.glide.load.a.b) {
            var2 = true;
         } else {
            var2 = false;
         }

         return var2;
      }

      public boolean a(boolean var1, io.intercom.com.bumptech.glide.load.a var2, io.intercom.com.bumptech.glide.load.c var3) {
         if(var2 != io.intercom.com.bumptech.glide.load.a.d && var2 != io.intercom.com.bumptech.glide.load.a.e) {
            var1 = true;
         } else {
            var1 = false;
         }

         return var1;
      }

      public boolean b() {
         return true;
      }
   };
   public static final h b = new h() {
      public boolean a() {
         return false;
      }

      public boolean a(io.intercom.com.bumptech.glide.load.a var1) {
         return false;
      }

      public boolean a(boolean var1, io.intercom.com.bumptech.glide.load.a var2, io.intercom.com.bumptech.glide.load.c var3) {
         return false;
      }

      public boolean b() {
         return false;
      }
   };
   public static final h c = new h() {
      public boolean a() {
         return false;
      }

      public boolean a(io.intercom.com.bumptech.glide.load.a var1) {
         boolean var2;
         if(var1 != io.intercom.com.bumptech.glide.load.a.c && var1 != io.intercom.com.bumptech.glide.load.a.e) {
            var2 = true;
         } else {
            var2 = false;
         }

         return var2;
      }

      public boolean a(boolean var1, io.intercom.com.bumptech.glide.load.a var2, io.intercom.com.bumptech.glide.load.c var3) {
         return false;
      }

      public boolean b() {
         return true;
      }
   };
   public static final h d = new h() {
      public boolean a() {
         return true;
      }

      public boolean a(io.intercom.com.bumptech.glide.load.a var1) {
         return false;
      }

      public boolean a(boolean var1, io.intercom.com.bumptech.glide.load.a var2, io.intercom.com.bumptech.glide.load.c var3) {
         if(var2 != io.intercom.com.bumptech.glide.load.a.d && var2 != io.intercom.com.bumptech.glide.load.a.e) {
            var1 = true;
         } else {
            var1 = false;
         }

         return var1;
      }

      public boolean b() {
         return false;
      }
   };
   public static final h e = new h() {
      public boolean a() {
         return true;
      }

      public boolean a(io.intercom.com.bumptech.glide.load.a var1) {
         boolean var2;
         if(var1 == io.intercom.com.bumptech.glide.load.a.b) {
            var2 = true;
         } else {
            var2 = false;
         }

         return var2;
      }

      public boolean a(boolean var1, io.intercom.com.bumptech.glide.load.a var2, io.intercom.com.bumptech.glide.load.c var3) {
         if((var1 && var2 == io.intercom.com.bumptech.glide.load.a.c || var2 == io.intercom.com.bumptech.glide.load.a.a) && var3 == io.intercom.com.bumptech.glide.load.c.b) {
            var1 = true;
         } else {
            var1 = false;
         }

         return var1;
      }

      public boolean b() {
         return true;
      }
   };

   public abstract boolean a();

   public abstract boolean a(io.intercom.com.bumptech.glide.load.a var1);

   public abstract boolean a(boolean var1, io.intercom.com.bumptech.glide.load.a var2, io.intercom.com.bumptech.glide.load.c var3);

   public abstract boolean b();
}
