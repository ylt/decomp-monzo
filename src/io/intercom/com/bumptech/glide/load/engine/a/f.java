package io.intercom.com.bumptech.glide.load.engine.a;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;

public class f implements e {
   public Bitmap a(int var1, int var2, Config var3) {
      return Bitmap.createBitmap(var1, var2, var3);
   }

   public void a() {
   }

   public void a(int var1) {
   }

   public void a(Bitmap var1) {
      var1.recycle();
   }

   public Bitmap b(int var1, int var2, Config var3) {
      return this.a(var1, var2, var3);
   }
}
