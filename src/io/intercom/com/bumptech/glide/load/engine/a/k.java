package io.intercom.com.bumptech.glide.load.engine.a;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.os.Build.VERSION;
import android.util.Log;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class k implements e {
   private static final Config a;
   private final l b;
   private final Set c;
   private final int d;
   private final k.a e;
   private int f;
   private int g;
   private int h;
   private int i;
   private int j;
   private int k;

   static {
      a = Config.ARGB_8888;
   }

   public k(int var1) {
      this(var1, e(), f());
   }

   k(int var1, l var2, Set var3) {
      this.d = var1;
      this.f = var1;
      this.b = var2;
      this.c = var3;
      this.e = new k.b();
   }

   @TargetApi(26)
   private static void a(Config var0) {
      if(VERSION.SDK_INT >= 26 && var0 == Config.HARDWARE) {
         throw new IllegalArgumentException("Cannot create a mutable Bitmap with config: " + var0 + ". Consider setting Downsampler#ALLOW_HARDWARE_CONFIG to false in your RequestOptions and/or in GlideBuilder.setDefaultRequestOptions");
      }
   }

   private void b() {
      this.b(this.f);
   }

   private void b(int param1) {
      // $FF: Couldn't be decompiled
   }

   private static void b(Bitmap var0) {
      var0.setHasAlpha(true);
      c(var0);
   }

   private Bitmap c(int param1, int param2, Config param3) {
      // $FF: Couldn't be decompiled
   }

   private void c() {
      if(Log.isLoggable("LruBitmapPool", 2)) {
         this.d();
      }

   }

   @TargetApi(19)
   private static void c(Bitmap var0) {
      if(VERSION.SDK_INT >= 19) {
         var0.setPremultiplied(true);
      }

   }

   private void d() {
      Log.v("LruBitmapPool", "Hits=" + this.h + ", misses=" + this.i + ", puts=" + this.j + ", evictions=" + this.k + ", currentSize=" + this.g + ", maxSize=" + this.f + "\nStrategy=" + this.b);
   }

   private static l e() {
      Object var0;
      if(VERSION.SDK_INT >= 19) {
         var0 = new n();
      } else {
         var0 = new c();
      }

      return (l)var0;
   }

   @TargetApi(26)
   private static Set f() {
      HashSet var0 = new HashSet();
      var0.addAll(Arrays.asList(Config.values()));
      if(VERSION.SDK_INT >= 19) {
         var0.add((Object)null);
      }

      if(VERSION.SDK_INT >= 26) {
         var0.remove(Config.HARDWARE);
      }

      return Collections.unmodifiableSet(var0);
   }

   public Bitmap a(int var1, int var2, Config var3) {
      Bitmap var4 = this.c(var1, var2, var3);
      Bitmap var5;
      if(var4 != null) {
         var4.eraseColor(0);
         var5 = var4;
      } else {
         var5 = Bitmap.createBitmap(var1, var2, var3);
      }

      return var5;
   }

   public void a() {
      if(Log.isLoggable("LruBitmapPool", 3)) {
         Log.d("LruBitmapPool", "clearMemory");
      }

      this.b(0);
   }

   @SuppressLint({"InlinedApi"})
   public void a(int var1) {
      if(Log.isLoggable("LruBitmapPool", 3)) {
         Log.d("LruBitmapPool", "trimMemory, level=" + var1);
      }

      if(var1 >= 40) {
         this.a();
      } else if(var1 >= 20) {
         this.b(this.f / 2);
      }

   }

   public void a(Bitmap param1) {
      // $FF: Couldn't be decompiled
   }

   public Bitmap b(int var1, int var2, Config var3) {
      Bitmap var5 = this.c(var1, var2, var3);
      Bitmap var4 = var5;
      if(var5 == null) {
         var4 = Bitmap.createBitmap(var1, var2, var3);
      }

      return var4;
   }

   private interface a {
      void a(Bitmap var1);

      void b(Bitmap var1);
   }

   private static class b implements k.a {
      public void a(Bitmap var1) {
      }

      public void b(Bitmap var1) {
      }
   }
}
