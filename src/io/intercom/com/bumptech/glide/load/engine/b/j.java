package io.intercom.com.bumptech.glide.load.engine.b;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class j {
   private final io.intercom.com.bumptech.glide.h.e a = new io.intercom.com.bumptech.glide.h.e(1000);
   private final android.support.v4.g.k.a b = io.intercom.com.bumptech.glide.h.a.a.b(10, new io.intercom.com.bumptech.glide.h.a.a.a() {
      public j.a a() {
         try {
            j.a var1 = new j.a(MessageDigest.getInstance("SHA-256"));
            return var1;
         } catch (NoSuchAlgorithmException var2) {
            throw new RuntimeException(var2);
         }
      }

      // $FF: synthetic method
      public Object b() {
         return this.a();
      }
   });

   private String b(io.intercom.com.bumptech.glide.load.g var1) {
      j.a var2 = (j.a)this.b.a();

      String var5;
      try {
         var1.updateDiskCacheKey(var2.a);
         var5 = io.intercom.com.bumptech.glide.h.i.a(var2.a.digest());
      } finally {
         this.b.a(var2);
      }

      return var5;
   }

   public String a(io.intercom.com.bumptech.glide.load.g param1) {
      // $FF: Couldn't be decompiled
   }

   private static final class a implements io.intercom.com.bumptech.glide.h.a.a.c {
      final MessageDigest a;
      private final io.intercom.com.bumptech.glide.h.a.b b = io.intercom.com.bumptech.glide.h.a.b.a();

      a(MessageDigest var1) {
         this.a = var1;
      }

      public io.intercom.com.bumptech.glide.h.a.b m_() {
         return this.b;
      }
   }
}
