package io.intercom.com.bumptech.glide.load.engine;

import android.os.Looper;

class n implements r {
   private final boolean a;
   private n.a b;
   private io.intercom.com.bumptech.glide.load.g c;
   private int d;
   private boolean e;
   private final r f;

   n(r var1, boolean var2) {
      this.f = (r)io.intercom.com.bumptech.glide.h.h.a((Object)var1);
      this.a = var2;
   }

   void a(io.intercom.com.bumptech.glide.load.g var1, n.a var2) {
      this.c = var1;
      this.b = var2;
   }

   boolean a() {
      return this.a;
   }

   public Class b() {
      return this.f.b();
   }

   public Object c() {
      return this.f.c();
   }

   public int d() {
      return this.f.d();
   }

   public void e() {
      if(this.d > 0) {
         throw new IllegalStateException("Cannot recycle a resource while it is still acquired");
      } else if(this.e) {
         throw new IllegalStateException("Cannot recycle a resource that has already been recycled");
      } else {
         this.e = true;
         this.f.e();
      }
   }

   void f() {
      if(this.e) {
         throw new IllegalStateException("Cannot acquire a recycled resource");
      } else if(!Looper.getMainLooper().equals(Looper.myLooper())) {
         throw new IllegalThreadStateException("Must call acquire on the main thread");
      } else {
         ++this.d;
      }
   }

   void g() {
      if(this.d <= 0) {
         throw new IllegalStateException("Cannot release a recycled or not yet acquired resource");
      } else if(!Looper.getMainLooper().equals(Looper.myLooper())) {
         throw new IllegalThreadStateException("Must call release on the main thread");
      } else {
         int var1 = this.d - 1;
         this.d = var1;
         if(var1 == 0) {
            this.b.b(this.c, this);
         }

      }
   }

   public String toString() {
      return "EngineResource{isCacheable=" + this.a + ", listener=" + this.b + ", key=" + this.c + ", acquired=" + this.d + ", isRecycled=" + this.e + ", resource=" + this.f + '}';
   }

   interface a {
      void b(io.intercom.com.bumptech.glide.load.g var1, n var2);
   }
}
