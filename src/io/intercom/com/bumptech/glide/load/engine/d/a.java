package io.intercom.com.bumptech.glide.load.engine.d;

import android.os.Handler;
import android.os.Looper;
import io.intercom.com.bumptech.glide.load.b;
import io.intercom.com.bumptech.glide.load.engine.a.e;
import io.intercom.com.bumptech.glide.load.engine.b.h;

public final class a {
   private final h a;
   private final e b;
   private final b c;
   private final Handler d = new Handler(Looper.getMainLooper());

   public a(h var1, e var2, b var3) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
   }
}
