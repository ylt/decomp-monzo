package io.intercom.com.bumptech.glide.load.engine;

import java.io.File;

class c implements io.intercom.com.bumptech.glide.load.engine.b.a.b {
   private final io.intercom.com.bumptech.glide.load.d a;
   private final Object b;
   private final io.intercom.com.bumptech.glide.load.i c;

   c(io.intercom.com.bumptech.glide.load.d var1, Object var2, io.intercom.com.bumptech.glide.load.i var3) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
   }

   public boolean a(File var1) {
      return this.a.a(this.b, var1, this.c);
   }
}
