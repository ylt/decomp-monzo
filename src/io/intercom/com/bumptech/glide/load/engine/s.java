package io.intercom.com.bumptech.glide.load.engine;

import java.io.File;
import java.util.List;

class s implements io.intercom.com.bumptech.glide.load.a.b.a, d {
   private final d.a a;
   private final e b;
   private int c = 0;
   private int d = -1;
   private io.intercom.com.bumptech.glide.load.g e;
   private List f;
   private int g;
   private volatile io.intercom.com.bumptech.glide.load.b.m.a h;
   private File i;
   private t j;

   public s(e var1, d.a var2) {
      this.b = var1;
      this.a = var2;
   }

   private boolean c() {
      boolean var1;
      if(this.g < this.f.size()) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public void a(Exception var1) {
      this.a.a(this.j, var1, this.h.c, io.intercom.com.bumptech.glide.load.a.d);
   }

   public void a(Object var1) {
      this.a.a(this.e, var1, this.h.c, io.intercom.com.bumptech.glide.load.a.d, this.j);
   }

   public boolean a() {
      List var8 = this.b.l();
      boolean var3;
      if(var8.isEmpty()) {
         var3 = false;
      } else {
         List var6 = this.b.i();

         while(true) {
            if(this.f != null && this.c()) {
               this.h = null;
               boolean var2 = false;

               while(true) {
                  var3 = var2;
                  if(var2) {
                     return var3;
                  }

                  var3 = var2;
                  if(!this.c()) {
                     return var3;
                  }

                  List var9 = this.f;
                  int var1 = this.g;
                  this.g = var1 + 1;
                  this.h = ((io.intercom.com.bumptech.glide.load.b.m)var9.get(var1)).a(this.i, this.b.g(), this.b.h(), this.b.e());
                  if(this.h != null && this.b.a(this.h.c.d())) {
                     var2 = true;
                     this.h.c.a(this.b.d(), this);
                  }
               }
            }

            ++this.d;
            if(this.d >= var6.size()) {
               ++this.c;
               if(this.c >= var8.size()) {
                  var3 = false;
                  break;
               }

               this.d = 0;
            }

            io.intercom.com.bumptech.glide.load.g var5 = (io.intercom.com.bumptech.glide.load.g)var8.get(this.c);
            Class var7 = (Class)var6.get(this.d);
            io.intercom.com.bumptech.glide.load.l var4 = this.b.c(var7);
            this.j = new t(var5, this.b.f(), this.b.g(), this.b.h(), var4, var7, this.b.e());
            this.i = this.b.b().a(this.j);
            if(this.i != null) {
               this.e = var5;
               this.f = this.b.a(this.i);
               this.g = 0;
            }
         }
      }

      return var3;
   }

   public void b() {
      io.intercom.com.bumptech.glide.load.b.m.a var1 = this.h;
      if(var1 != null) {
         var1.c.b();
      }

   }
}
