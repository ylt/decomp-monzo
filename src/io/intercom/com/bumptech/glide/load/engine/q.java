package io.intercom.com.bumptech.glide.load.engine;

final class q implements io.intercom.com.bumptech.glide.h.a.a.c, r {
   private static final android.support.v4.g.k.a a = io.intercom.com.bumptech.glide.h.a.a.b(20, new io.intercom.com.bumptech.glide.h.a.a.a() {
      public q a() {
         return new q();
      }

      // $FF: synthetic method
      public Object b() {
         return this.a();
      }
   });
   private final io.intercom.com.bumptech.glide.h.a.b b = io.intercom.com.bumptech.glide.h.a.b.a();
   private r c;
   private boolean d;
   private boolean e;

   static q a(r var0) {
      q var1 = (q)a.a();
      var1.b(var0);
      return var1;
   }

   private void b(r var1) {
      this.e = false;
      this.d = true;
      this.c = var1;
   }

   private void f() {
      this.c = null;
      a.a(this);
   }

   public void a() {
      synchronized(this){}

      try {
         this.b.b();
         if(!this.d) {
            IllegalStateException var1 = new IllegalStateException("Already unlocked");
            throw var1;
         }

         this.d = false;
         if(this.e) {
            this.e();
         }
      } finally {
         ;
      }

   }

   public Class b() {
      return this.c.b();
   }

   public Object c() {
      return this.c.c();
   }

   public int d() {
      return this.c.d();
   }

   public void e() {
      synchronized(this){}

      try {
         this.b.b();
         this.e = true;
         if(!this.d) {
            this.c.e();
            this.f();
         }
      } finally {
         ;
      }

   }

   public io.intercom.com.bumptech.glide.h.a.b m_() {
      return this.b;
   }
}
