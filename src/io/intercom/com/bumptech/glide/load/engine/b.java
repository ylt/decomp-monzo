package io.intercom.com.bumptech.glide.load.engine;

import java.security.MessageDigest;

final class b implements io.intercom.com.bumptech.glide.load.g {
   private final io.intercom.com.bumptech.glide.load.g b;
   private final io.intercom.com.bumptech.glide.load.g c;

   public b(io.intercom.com.bumptech.glide.load.g var1, io.intercom.com.bumptech.glide.load.g var2) {
      this.b = var1;
      this.c = var2;
   }

   public boolean equals(Object var1) {
      boolean var3 = false;
      boolean var2 = var3;
      if(var1 instanceof b) {
         b var4 = (b)var1;
         var2 = var3;
         if(this.b.equals(var4.b)) {
            var2 = var3;
            if(this.c.equals(var4.c)) {
               var2 = true;
            }
         }
      }

      return var2;
   }

   public int hashCode() {
      return this.b.hashCode() * 31 + this.c.hashCode();
   }

   public String toString() {
      return "DataCacheKey{sourceKey=" + this.b + ", signature=" + this.c + '}';
   }

   public void updateDiskCacheKey(MessageDigest var1) {
      this.b.updateDiskCacheKey(var1);
      this.c.updateDiskCacheKey(var1);
   }
}
