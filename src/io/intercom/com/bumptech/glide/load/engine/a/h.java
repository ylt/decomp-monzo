package io.intercom.com.bumptech.glide.load.engine.a;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class h {
   private final h.a a = new h.a();
   private final Map b = new HashMap();

   private void a(h.a var1) {
      d(var1);
      var1.c = this.a;
      var1.b = this.a.b;
      c(var1);
   }

   private void b(h.a var1) {
      d(var1);
      var1.c = this.a.c;
      var1.b = this.a;
      c(var1);
   }

   private static void c(h.a var0) {
      var0.b.c = var0;
      var0.c.b = var0;
   }

   private static void d(h.a var0) {
      var0.c.b = var0.b;
      var0.b.c = var0.c;
   }

   public Object a() {
      h.a var1 = this.a.c;

      Object var3;
      while(true) {
         if(var1.equals(this.a)) {
            var3 = null;
            break;
         }

         Object var2 = var1.a();
         if(var2 != null) {
            var3 = var2;
            break;
         }

         d(var1);
         this.b.remove(var1.a);
         ((m)var1.a).a();
         var1 = var1.c;
      }

      return var3;
   }

   public Object a(m var1) {
      h.a var2 = (h.a)this.b.get(var1);
      h.a var3;
      if(var2 == null) {
         var2 = new h.a(var1);
         this.b.put(var1, var2);
         var3 = var2;
      } else {
         var1.a();
         var3 = var2;
      }

      this.a(var3);
      return var3.a();
   }

   public void a(m var1, Object var2) {
      h.a var3 = (h.a)this.b.get(var1);
      h.a var4;
      if(var3 == null) {
         var3 = new h.a(var1);
         this.b(var3);
         this.b.put(var1, var3);
         var4 = var3;
      } else {
         var1.a();
         var4 = var3;
      }

      var4.a(var2);
   }

   public String toString() {
      StringBuilder var3 = new StringBuilder("GroupedLinkedMap( ");
      h.a var2 = this.a.b;

      boolean var1;
      for(var1 = false; !var2.equals(this.a); var2 = var2.b) {
         var1 = true;
         var3.append('{').append(var2.a).append(':').append(var2.b()).append("}, ");
      }

      if(var1) {
         var3.delete(var3.length() - 2, var3.length());
      }

      return var3.append(" )").toString();
   }

   private static class a {
      final Object a;
      h.a b;
      h.a c;
      private List d;

      public a() {
         this((Object)null);
      }

      public a(Object var1) {
         this.c = this;
         this.b = this;
         this.a = var1;
      }

      public Object a() {
         int var1 = this.b();
         Object var2;
         if(var1 > 0) {
            var2 = this.d.remove(var1 - 1);
         } else {
            var2 = null;
         }

         return var2;
      }

      public void a(Object var1) {
         if(this.d == null) {
            this.d = new ArrayList();
         }

         this.d.add(var1);
      }

      public int b() {
         int var1;
         if(this.d != null) {
            var1 = this.d.size();
         } else {
            var1 = 0;
         }

         return var1;
      }
   }
}
