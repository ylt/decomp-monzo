package io.intercom.com.bumptech.glide.load.engine.a;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;
import java.util.Map.Entry;

@TargetApi(19)
public class n implements l {
   private static final Config[] a;
   private static final Config[] b;
   private static final Config[] c;
   private static final Config[] d;
   private final n.b e = new n.b();
   private final h f = new h();
   private final Map g = new HashMap();

   static {
      a = new Config[]{Config.ARGB_8888, null};
      b = new Config[]{Config.RGB_565};
      c = new Config[]{Config.ARGB_4444};
      d = new Config[]{Config.ALPHA_8};
   }

   static String a(int var0, Config var1) {
      return "[" + var0 + "](" + var1 + ")";
   }

   private NavigableMap a(Config var1) {
      NavigableMap var3 = (NavigableMap)this.g.get(var1);
      Object var2 = var3;
      if(var3 == null) {
         var2 = new TreeMap();
         this.g.put(var1, var2);
      }

      return (NavigableMap)var2;
   }

   private void a(Integer var1, Bitmap var2) {
      NavigableMap var3 = this.a(var2.getConfig());
      Integer var4 = (Integer)var3.get(var1);
      if(var4 == null) {
         throw new NullPointerException("Tried to decrement empty size, size: " + var1 + ", removed: " + this.b(var2) + ", this: " + this);
      } else {
         if(var4.intValue() == 1) {
            var3.remove(var1);
         } else {
            var3.put(var1, Integer.valueOf(var4.intValue() - 1));
         }

      }
   }

   private n.a b(int var1, Config var2) {
      n.a var5 = this.e.a(var1, var2);
      Config[] var8 = b(var2);
      int var4 = var8.length;

      Integer var6;
      Config var7;
      n.a var9;
      label40: {
         for(int var3 = 0; var3 < var4; ++var3) {
            var7 = var8[var3];
            var6 = (Integer)this.a(var7).ceilingKey(Integer.valueOf(var1));
            if(var6 != null && var6.intValue() <= var1 * 8) {
               if(var6.intValue() != var1) {
                  break label40;
               }

               if(var7 == null) {
                  if(var2 != null) {
                     break label40;
                  }
               } else if(!var7.equals(var2)) {
                  break label40;
               }
               break;
            }
         }

         var9 = var5;
         return var9;
      }

      this.e.a(var5);
      var9 = this.e.a(var6.intValue(), var7);
      return var9;
   }

   private static Config[] b(Config var0) {
      Config[] var2;
      switch(null.a[var0.ordinal()]) {
      case 1:
         var2 = a;
         break;
      case 2:
         var2 = b;
         break;
      case 3:
         var2 = c;
         break;
      case 4:
         var2 = d;
         break;
      default:
         Config[] var1 = new Config[]{var0};
         var2 = var1;
      }

      return var2;
   }

   public Bitmap a() {
      Bitmap var1 = (Bitmap)this.f.a();
      if(var1 != null) {
         this.a(Integer.valueOf(io.intercom.com.bumptech.glide.h.i.a(var1)), var1);
      }

      return var1;
   }

   public Bitmap a(int var1, int var2, Config var3) {
      n.a var5 = this.b(io.intercom.com.bumptech.glide.h.i.a(var1, var2, var3), var3);
      Bitmap var4 = (Bitmap)this.f.a((m)var5);
      if(var4 != null) {
         this.a(Integer.valueOf(var5.a), var4);
         if(var4.getConfig() != null) {
            var3 = var4.getConfig();
         } else {
            var3 = Config.ARGB_8888;
         }

         var4.reconfigure(var1, var2, var3);
      }

      return var4;
   }

   public void a(Bitmap var1) {
      int var2 = io.intercom.com.bumptech.glide.h.i.a(var1);
      n.a var4 = this.e.a(var2, var1.getConfig());
      this.f.a(var4, var1);
      NavigableMap var5 = this.a(var1.getConfig());
      Integer var6 = (Integer)var5.get(Integer.valueOf(var4.a));
      int var3 = var4.a;
      if(var6 == null) {
         var2 = 1;
      } else {
         var2 = var6.intValue() + 1;
      }

      var5.put(Integer.valueOf(var3), Integer.valueOf(var2));
   }

   public String b(int var1, int var2, Config var3) {
      return a(io.intercom.com.bumptech.glide.h.i.a(var1, var2, var3), var3);
   }

   public String b(Bitmap var1) {
      return a(io.intercom.com.bumptech.glide.h.i.a(var1), var1.getConfig());
   }

   public int c(Bitmap var1) {
      return io.intercom.com.bumptech.glide.h.i.a(var1);
   }

   public String toString() {
      StringBuilder var2 = (new StringBuilder()).append("SizeConfigStrategy{groupedMap=").append(this.f).append(", sortedSizes=(");
      Iterator var3 = this.g.entrySet().iterator();

      while(var3.hasNext()) {
         Entry var1 = (Entry)var3.next();
         var2.append(var1.getKey()).append('[').append(var1.getValue()).append("], ");
      }

      if(!this.g.isEmpty()) {
         var2.replace(var2.length() - 2, var2.length(), "");
      }

      return var2.append(")}").toString();
   }

   static final class a implements m {
      int a;
      private final n.b b;
      private Config c;

      public a(n.b var1) {
         this.b = var1;
      }

      public void a() {
         this.b.a(this);
      }

      public void a(int var1, Config var2) {
         this.a = var1;
         this.c = var2;
      }

      public boolean equals(Object var1) {
         boolean var3 = false;
         boolean var2 = var3;
         if(var1 instanceof n.a) {
            n.a var4 = (n.a)var1;
            var2 = var3;
            if(this.a == var4.a) {
               var2 = var3;
               if(io.intercom.com.bumptech.glide.h.i.a((Object)this.c, (Object)var4.c)) {
                  var2 = true;
               }
            }
         }

         return var2;
      }

      public int hashCode() {
         int var2 = this.a;
         int var1;
         if(this.c != null) {
            var1 = this.c.hashCode();
         } else {
            var1 = 0;
         }

         return var1 + var2 * 31;
      }

      public String toString() {
         return n.a(this.a, this.c);
      }
   }

   static class b extends d {
      protected n.a a() {
         return new n.a(this);
      }

      public n.a a(int var1, Config var2) {
         n.a var3 = (n.a)this.c();
         var3.a(var1, var2);
         return var3;
      }

      // $FF: synthetic method
      protected m b() {
         return this.a();
      }
   }
}
