package io.intercom.com.bumptech.glide.load.engine;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Handler.Callback;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

class j implements io.intercom.com.bumptech.glide.h.a.a.c, f.a {
   private static final j.a a = new j.a();
   private static final Handler b = new Handler(Looper.getMainLooper(), new j.b());
   private final List c;
   private final io.intercom.com.bumptech.glide.h.a.b d;
   private final android.support.v4.g.k.a e;
   private final j.a f;
   private final k g;
   private final io.intercom.com.bumptech.glide.load.engine.c.a h;
   private final io.intercom.com.bumptech.glide.load.engine.c.a i;
   private final io.intercom.com.bumptech.glide.load.engine.c.a j;
   private io.intercom.com.bumptech.glide.load.g k;
   private boolean l;
   private boolean m;
   private r n;
   private io.intercom.com.bumptech.glide.load.a o;
   private boolean p;
   private GlideException q;
   private boolean r;
   private List s;
   private n t;
   private f u;
   private volatile boolean v;

   j(io.intercom.com.bumptech.glide.load.engine.c.a var1, io.intercom.com.bumptech.glide.load.engine.c.a var2, io.intercom.com.bumptech.glide.load.engine.c.a var3, k var4, android.support.v4.g.k.a var5) {
      this(var1, var2, var3, var4, var5, a);
   }

   j(io.intercom.com.bumptech.glide.load.engine.c.a var1, io.intercom.com.bumptech.glide.load.engine.c.a var2, io.intercom.com.bumptech.glide.load.engine.c.a var3, k var4, android.support.v4.g.k.a var5, j.a var6) {
      this.c = new ArrayList(2);
      this.d = io.intercom.com.bumptech.glide.h.a.b.a();
      this.h = var1;
      this.i = var2;
      this.j = var3;
      this.g = var4;
      this.e = var5;
      this.f = var6;
   }

   private void a(boolean var1) {
      io.intercom.com.bumptech.glide.h.i.a();
      this.c.clear();
      this.k = null;
      this.t = null;
      this.n = null;
      if(this.s != null) {
         this.s.clear();
      }

      this.r = false;
      this.v = false;
      this.p = false;
      this.u.a(var1);
      this.u = null;
      this.q = null;
      this.o = null;
      this.e.a(this);
   }

   private void c(io.intercom.com.bumptech.glide.f.g var1) {
      if(this.s == null) {
         this.s = new ArrayList(2);
      }

      if(!this.s.contains(var1)) {
         this.s.add(var1);
      }

   }

   private boolean d(io.intercom.com.bumptech.glide.f.g var1) {
      boolean var2;
      if(this.s != null && this.s.contains(var1)) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   private io.intercom.com.bumptech.glide.load.engine.c.a f() {
      io.intercom.com.bumptech.glide.load.engine.c.a var1;
      if(this.m) {
         var1 = this.j;
      } else {
         var1 = this.i;
      }

      return var1;
   }

   j a(io.intercom.com.bumptech.glide.load.g var1, boolean var2, boolean var3) {
      this.k = var1;
      this.l = var2;
      this.m = var3;
      return this;
   }

   void a() {
      if(!this.r && !this.p && !this.v) {
         this.v = true;
         this.u.b();
         this.g.a(this, this.k);
      }

   }

   public void a(io.intercom.com.bumptech.glide.f.g var1) {
      io.intercom.com.bumptech.glide.h.i.a();
      this.d.b();
      if(this.p) {
         var1.a(this.t, this.o);
      } else if(this.r) {
         var1.a(this.q);
      } else {
         this.c.add(var1);
      }

   }

   public void a(GlideException var1) {
      this.q = var1;
      b.obtainMessage(2, this).sendToTarget();
   }

   public void a(f var1) {
      this.f().execute(var1);
   }

   public void a(r var1, io.intercom.com.bumptech.glide.load.a var2) {
      this.n = var1;
      this.o = var2;
      b.obtainMessage(1, this).sendToTarget();
   }

   void b() {
      this.d.b();
      if(this.v) {
         this.n.e();
         this.a(false);
      } else {
         if(this.c.isEmpty()) {
            throw new IllegalStateException("Received a resource without any callbacks to notify");
         }

         if(this.p) {
            throw new IllegalStateException("Already have resource");
         }

         this.t = this.f.a(this.n, this.l);
         this.p = true;
         this.t.f();
         this.g.a(this.k, this.t);
         Iterator var2 = this.c.iterator();

         while(var2.hasNext()) {
            io.intercom.com.bumptech.glide.f.g var1 = (io.intercom.com.bumptech.glide.f.g)var2.next();
            if(!this.d(var1)) {
               this.t.f();
               var1.a(this.t, this.o);
            }
         }

         this.t.g();
         this.a(false);
      }

   }

   public void b(io.intercom.com.bumptech.glide.f.g var1) {
      io.intercom.com.bumptech.glide.h.i.a();
      this.d.b();
      if(!this.p && !this.r) {
         this.c.remove(var1);
         if(this.c.isEmpty()) {
            this.a();
         }
      } else {
         this.c(var1);
      }

   }

   public void b(f var1) {
      this.u = var1;
      io.intercom.com.bumptech.glide.load.engine.c.a var2;
      if(var1.a()) {
         var2 = this.h;
      } else {
         var2 = this.f();
      }

      var2.execute(var1);
   }

   void c() {
      this.d.b();
      if(!this.v) {
         throw new IllegalStateException("Not cancelled");
      } else {
         this.g.a(this, this.k);
         this.a(false);
      }
   }

   void e() {
      this.d.b();
      if(this.v) {
         this.a(false);
      } else {
         if(this.c.isEmpty()) {
            throw new IllegalStateException("Received an exception without any callbacks to notify");
         }

         if(this.r) {
            throw new IllegalStateException("Already failed once");
         }

         this.r = true;
         this.g.a((io.intercom.com.bumptech.glide.load.g)this.k, (n)null);
         Iterator var2 = this.c.iterator();

         while(var2.hasNext()) {
            io.intercom.com.bumptech.glide.f.g var1 = (io.intercom.com.bumptech.glide.f.g)var2.next();
            if(!this.d(var1)) {
               var1.a(this.q);
            }
         }

         this.a(false);
      }

   }

   public io.intercom.com.bumptech.glide.h.a.b m_() {
      return this.d;
   }

   static class a {
      public n a(r var1, boolean var2) {
         return new n(var1, var2);
      }
   }

   private static class b implements Callback {
      public boolean handleMessage(Message var1) {
         j var2 = (j)var1.obj;
         switch(var1.what) {
         case 1:
            var2.b();
            break;
         case 2:
            var2.e();
            break;
         case 3:
            var2.c();
            break;
         default:
            throw new IllegalStateException("Unrecognized message: " + var1.what);
         }

         return true;
      }
   }
}
