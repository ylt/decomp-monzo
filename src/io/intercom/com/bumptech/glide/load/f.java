package io.intercom.com.bumptech.glide.load;

import io.intercom.com.bumptech.glide.load.resource.bitmap.RecyclableBufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Iterator;
import java.util.List;

public final class f {
   public static e.a a(List var0, InputStream var1, io.intercom.com.bumptech.glide.load.engine.a.b var2) throws IOException {
      e.a var7;
      if(var1 == null) {
         var7 = e.a.h;
      } else {
         Object var3 = var1;
         if(!var1.markSupported()) {
            var3 = new RecyclableBufferedInputStream(var1, var2);
         }

         ((InputStream)var3).mark(5242880);
         Iterator var9 = var0.iterator();

         while(var9.hasNext()) {
            e var8 = (e)var9.next();
            boolean var5 = false;

            e.a var10;
            try {
               var5 = true;
               var7 = var8.a((InputStream)var3);
               var10 = e.a.h;
               var5 = false;
            } finally {
               if(var5) {
                  ((InputStream)var3).reset();
               }
            }

            if(var7 != var10) {
               ((InputStream)var3).reset();
               return var7;
            }

            ((InputStream)var3).reset();
         }

         var7 = e.a.h;
      }

      return var7;
   }

   public static e.a a(List var0, ByteBuffer var1) throws IOException {
      e.a var3;
      if(var1 == null) {
         var3 = e.a.h;
      } else {
         Iterator var2 = var0.iterator();

         do {
            if(!var2.hasNext()) {
               var3 = e.a.h;
               break;
            }

            var3 = ((e)var2.next()).a(var1);
         } while(var3 == e.a.h);
      }

      return var3;
   }

   public static int b(List var0, InputStream var1, io.intercom.com.bumptech.glide.load.engine.a.b var2) throws IOException {
      int var3;
      if(var1 == null) {
         var3 = -1;
      } else {
         Object var4 = var1;
         if(!var1.markSupported()) {
            var4 = new RecyclableBufferedInputStream(var1, var2);
         }

         ((InputStream)var4).mark(5242880);
         Iterator var9 = var0.iterator();

         while(var9.hasNext()) {
            e var8 = (e)var9.next();
            boolean var6 = false;

            try {
               var6 = true;
               var3 = var8.a((InputStream)var4, var2);
               var6 = false;
            } finally {
               if(var6) {
                  ((InputStream)var4).reset();
               }
            }

            if(var3 != -1) {
               ((InputStream)var4).reset();
               return var3;
            }

            ((InputStream)var4).reset();
         }

         var3 = -1;
      }

      return var3;
   }
}
