package io.intercom.com.bumptech.glide.load;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

public interface e {
   int a(InputStream var1, io.intercom.com.bumptech.glide.load.engine.a.b var2) throws IOException;

   e.a a(InputStream var1) throws IOException;

   e.a a(ByteBuffer var1) throws IOException;

   public static enum a {
      a(true),
      b(false),
      c(false),
      d(true),
      e(false),
      f(true),
      g(false),
      h(false);

      private final boolean i;

      private a(boolean var3) {
         this.i = var3;
      }

      public boolean a() {
         return this.i;
      }
   }
}
