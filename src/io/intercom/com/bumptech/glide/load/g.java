package io.intercom.com.bumptech.glide.load;

import java.nio.charset.Charset;
import java.security.MessageDigest;

public interface g {
   Charset a = Charset.forName("UTF-8");

   boolean equals(Object var1);

   int hashCode();

   void updateDiskCacheKey(MessageDigest var1);
}
