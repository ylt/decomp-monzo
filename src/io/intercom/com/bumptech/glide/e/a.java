package io.intercom.com.bumptech.glide.e;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class a {
   private final List a = new ArrayList();

   public io.intercom.com.bumptech.glide.load.d a(Class var1) {
      synchronized(this){}
      boolean var5 = false;

      io.intercom.com.bumptech.glide.load.d var7;
      try {
         var5 = true;
         Iterator var3 = this.a.iterator();

         while(true) {
            if(!var3.hasNext()) {
               var5 = false;
               break;
            }

            a.a var2 = (a.a)var3.next();
            if(var2.a(var1)) {
               var7 = var2.a;
               var5 = false;
               return var7;
            }
         }
      } finally {
         if(var5) {
            ;
         }
      }

      var7 = null;
      return var7;
   }

   public void a(Class var1, io.intercom.com.bumptech.glide.load.d var2) {
      synchronized(this){}

      try {
         List var3 = this.a;
         a.a var4 = new a.a(var1, var2);
         var3.add(var4);
      } finally {
         ;
      }

   }

   private static final class a {
      final io.intercom.com.bumptech.glide.load.d a;
      private final Class b;

      public a(Class var1, io.intercom.com.bumptech.glide.load.d var2) {
         this.b = var1;
         this.a = var2;
      }

      public boolean a(Class var1) {
         return this.b.isAssignableFrom(var1);
      }
   }
}
