package io.intercom.com.bumptech.glide.b;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import java.nio.ByteBuffer;
import java.util.Arrays;

public class e implements a {
   private static final String a = e.class.getSimpleName();
   private int[] b;
   private final int[] c;
   private ByteBuffer d;
   private byte[] e;
   private byte[] f;
   private int g;
   private int h;
   private short[] i;
   private byte[] j;
   private byte[] k;
   private byte[] l;
   private int[] m;
   private int n;
   private c o;
   private a.a p;
   private Bitmap q;
   private boolean r;
   private int s;
   private int t;
   private int u;
   private int v;
   private boolean w;

   public e(a.a var1) {
      this.c = new int[256];
      this.g = 0;
      this.h = 0;
      this.p = var1;
      this.o = new c();
   }

   public e(a.a var1, c var2, ByteBuffer var3, int var4) {
      this(var1);
      this.a(var2, var3, var4);
   }

   private int a(int var1, int var2, int var3) {
      byte var15 = 0;
      int var9 = var1;
      int var4 = 0;
      int var5 = 0;
      int var6 = 0;
      int var7 = 0;

      int var8;
      byte var10;
      int var11;
      int var12;
      int var13;
      int var14;
      int var16;
      int var17;
      for(var8 = 0; var9 < this.t + var1 && var9 < this.l.length && var9 < var2; var8 = var17) {
         var10 = this.l[var9];
         var16 = this.b[var10 & 255];
         var14 = var4;
         var13 = var5;
         var12 = var6;
         var11 = var7;
         var17 = var8;
         if(var16 != 0) {
            var17 = var8 + (var16 >> 24 & 255);
            var11 = var7 + (var16 >> 16 & 255);
            var12 = var6 + (var16 >> 8 & 255);
            var13 = var5 + (var16 & 255);
            var14 = var4 + 1;
         }

         ++var9;
         var4 = var14;
         var5 = var13;
         var6 = var12;
         var7 = var11;
      }

      var17 = var1 + var3;
      var9 = var8;
      var8 = var6;
      var6 = var4;

      for(var4 = var17; var4 < var1 + var3 + this.t && var4 < this.l.length && var4 < var2; var9 = var17) {
         var10 = this.l[var4];
         var16 = this.b[var10 & 255];
         var14 = var6;
         var13 = var5;
         var12 = var8;
         var11 = var7;
         var17 = var9;
         if(var16 != 0) {
            var17 = var9 + (var16 >> 24 & 255);
            var11 = var7 + (var16 >> 16 & 255);
            var12 = var8 + (var16 >> 8 & 255);
            var13 = var5 + (var16 & 255);
            var14 = var6 + 1;
         }

         ++var4;
         var6 = var14;
         var5 = var13;
         var8 = var12;
         var7 = var11;
      }

      if(var6 == 0) {
         var1 = var15;
      } else {
         var1 = var9 / var6 << 24 | var7 / var6 << 16 | var8 / var6 << 8 | var5 / var6;
      }

      return var1;
   }

   private Bitmap a(b var1, b var2) {
      int[] var19 = this.m;
      if(var2 == null) {
         Arrays.fill(var19, 0);
      }

      int var3;
      byte var4;
      int var5;
      int var6;
      int var7;
      int var8;
      int var9;
      int var21;
      if(var2 != null && var2.g > 0) {
         if(var2.g == 2) {
            var4 = 0;
            if(!var1.f) {
               var21 = this.o.l;
               var3 = var21;
               if(var1.k != null) {
                  var3 = var21;
                  if(this.o.j == var1.h) {
                     var3 = 0;
                  }
               }
            } else {
               var3 = var4;
               if(this.n == 0) {
                  this.w = true;
                  var3 = var4;
               }
            }

            var8 = var2.d / this.t;
            var21 = var2.b / this.t;
            var7 = var2.c / this.t;
            var5 = var2.a / this.t;
            var6 = var21 * this.v + var5;
            var9 = this.v;

            for(var21 = var6; var21 < var6 + var8 * var9; var21 += this.v) {
               for(var5 = var21; var5 < var21 + var7; ++var5) {
                  var19[var5] = var3;
               }
            }
         } else if(var2.g == 3 && this.q != null) {
            this.q.getPixels(var19, 0, this.v, 0, 0, this.v, this.u);
         }
      }

      this.a(var1);
      int var17 = var1.d / this.t;
      int var15 = var1.b / this.t;
      int var16 = var1.c / this.t;
      int var14 = var1.a / this.t;
      var7 = 1;
      var4 = 8;
      var5 = 0;
      boolean var10;
      if(this.n == 0) {
         var10 = true;
      } else {
         var10 = false;
      }

      byte var22;
      for(var8 = 0; var8 < var17; var4 = var22) {
         if(var1.e) {
            var3 = var5;
            byte var23 = var4;
            var9 = var7;
            if(var5 >= var17) {
               var9 = var7 + 1;
               switch(var9) {
               case 2:
                  var3 = 4;
                  var23 = var4;
                  break;
               case 3:
                  var3 = 2;
                  var23 = 4;
                  break;
               case 4:
                  var3 = 1;
                  var23 = 2;
                  break;
               default:
                  var23 = var4;
                  var3 = var5;
               }
            }

            var21 = var9;
            var22 = var23;
            var6 = var3 + var23;
         } else {
            var3 = var8;
            var6 = var5;
            var22 = var4;
            var21 = var7;
         }

         var3 += var15;
         if(var3 < this.u) {
            var9 = this.v * var3;
            int var12 = var9 + var14;
            var7 = var12 + var16;
            var3 = var7;
            if(this.v + var9 < var7) {
               var3 = this.v + var9;
            }

            int var13 = this.t * var8 * var1.c;
            int var18 = this.t;
            var7 = var12;

            int var25;
            for(var9 = var13; var7 < var3; var9 += var25) {
               if(this.t == 1) {
                  byte var11 = this.l[var9];
                  var25 = this.b[var11 & 255];
               } else {
                  var25 = this.a(var9, var13 + (var3 - var12) * var18, var1.c);
               }

               if(var25 != 0) {
                  var19[var7] = var25;
               } else if(!this.w && var10) {
                  this.w = true;
               }

               var25 = this.t;
               ++var7;
            }
         }

         ++var8;
         var7 = var21;
         var5 = var6;
      }

      if(this.r && (var1.g == 0 || var1.g == 1)) {
         if(this.q == null) {
            this.q = this.m();
         }

         this.q.setPixels(var19, 0, this.v, 0, 0, this.v, this.u);
      }

      Bitmap var20 = this.m();
      var20.setPixels(var19, 0, this.v, 0, 0, this.v, this.u);
      return var20;
   }

   private void a(b var1) {
      this.g = 0;
      this.h = 0;
      if(var1 != null) {
         this.d.position(var1.j);
      }

      int var15;
      if(var1 == null) {
         var15 = this.o.f * this.o.g;
      } else {
         var15 = var1.c * var1.d;
      }

      if(this.l == null || this.l.length < var15) {
         this.l = this.p.a(var15);
      }

      if(this.i == null) {
         this.i = new short[4096];
      }

      if(this.j == null) {
         this.j = new byte[4096];
      }

      if(this.k == null) {
         this.k = new byte[4097];
      }

      int var22 = this.k();
      int var23 = 1 << var22;
      int var6 = var22 + 1;

      int var2;
      for(var2 = 0; var2 < var23; ++var2) {
         this.i[var2] = 0;
         this.j[var2] = (byte)var2;
      }

      int var16 = 0;
      int var12 = 0;
      int var10 = 0;
      int var9 = 0;
      int var11 = 0;
      int var5 = (1 << var6) - 1;
      int var8 = var23 + 2;
      int var3 = 0;
      var2 = 0;
      int var4 = 0;
      int var7 = -1;

      label105:
      while(var16 < var15) {
         int var14 = var3;
         if(var3 == 0) {
            var14 = this.l();
            if(var14 <= 0) {
               this.s = 3;
               break;
            }

            var4 = 0;
         }

         var12 += (this.e[var4] & 255) << var10;
         int var13 = var4 + 1;
         --var14;
         var4 = var6;
         int var17 = var10 + 8;
         var10 = var7;
         var7 = var2;
         var3 = var8;
         var6 = var9;
         var2 = var10;
         var10 = var17;
         var8 = var12;

         while(true) {
            int var18;
            while(var10 >= var4) {
               var12 = var8 & var5;
               var9 = var8 >> var4;
               var8 = var10 - var4;
               if(var12 != var23) {
                  if(var12 > var3) {
                     this.s = 3;
                     var12 = var9;
                     var9 = var6;
                     var10 = var14;
                     var14 = var2;
                     var6 = var4;
                     var4 = var13;
                     var13 = var3;
                     var2 = var7;
                     var3 = var10;
                     var10 = var8;
                     var7 = var14;
                     var8 = var13;
                     continue label105;
                  }

                  if(var12 == var23 + 1) {
                     var12 = var9;
                     var9 = var6;
                     var10 = var14;
                     var14 = var2;
                     var6 = var4;
                     var4 = var13;
                     var13 = var3;
                     var2 = var7;
                     var3 = var10;
                     var10 = var8;
                     var7 = var14;
                     var8 = var13;
                     continue label105;
                  }

                  if(var2 == -1) {
                     this.k[var11] = this.j[var12];
                     var10 = var8;
                     var6 = var12;
                     ++var11;
                     var2 = var12;
                     var8 = var9;
                  } else {
                     if(var12 >= var3) {
                        this.k[var11] = (byte)var6;
                        ++var11;
                        var6 = var2;
                     } else {
                        var6 = var12;
                     }

                     while(var6 >= var23) {
                        this.k[var11] = this.j[var6];
                        var6 = this.i[var6];
                        ++var11;
                     }

                     int var20 = this.j[var6] & 255;
                     byte[] var25 = this.k;
                     int var21 = var11 + 1;
                     var25[var11] = (byte)var20;
                     int var19 = var5;
                     var17 = var4;
                     var10 = var3;
                     var18 = var16;
                     var6 = var21;
                     var11 = var7;
                     if(var3 < 4096) {
                        this.i[var3] = (short)var2;
                        this.j[var3] = (byte)var20;
                        var2 = var3 + 1;
                        var19 = var5;
                        var17 = var4;
                        var10 = var2;
                        var18 = var16;
                        var6 = var21;
                        var11 = var7;
                        if((var2 & var5) == 0) {
                           var19 = var5;
                           var17 = var4;
                           var10 = var2;
                           var18 = var16;
                           var6 = var21;
                           var11 = var7;
                           if(var2 < 4096) {
                              var17 = var4 + 1;
                              var19 = var5 + var2;
                              var11 = var7;
                              var6 = var21;
                              var18 = var16;
                              var10 = var2;
                           }
                        }
                     }

                     while(var6 > 0) {
                        var25 = this.l;
                        byte[] var24 = this.k;
                        --var6;
                        var25[var11] = var24[var6];
                        ++var18;
                        ++var11;
                     }

                     var2 = var12;
                     var12 = var8;
                     var8 = var9;
                     var5 = var19;
                     var4 = var17;
                     var3 = var10;
                     var16 = var18;
                     var7 = var11;
                     var11 = var6;
                     var10 = var12;
                     var6 = var20;
                  }
               } else {
                  var4 = var22 + 1;
                  var5 = (1 << var4) - 1;
                  var3 = var23 + 2;
                  var10 = var8;
                  var2 = -1;
                  var8 = var9;
               }
            }

            var9 = var6;
            var17 = var2;
            var18 = var3;
            var2 = var7;
            var6 = var4;
            var4 = var13;
            var3 = var14;
            var12 = var8;
            var7 = var17;
            var8 = var18;
            break;
         }
      }

      while(var2 < var15) {
         this.l[var2] = 0;
         ++var2;
      }

   }

   private void j() {
      if(this.g <= this.h) {
         if(this.f == null) {
            this.f = this.p.a(16384);
         }

         this.h = 0;
         this.g = Math.min(this.d.remaining(), 16384);
         this.d.get(this.f, 0, this.g);
      }

   }

   private int k() {
      int var1;
      byte[] var2;
      try {
         this.j();
         var2 = this.f;
         var1 = this.h;
         this.h = var1 + 1;
      } catch (Exception var3) {
         this.s = 1;
         var1 = 0;
         return var1;
      }

      byte var4 = var2[var1];
      var1 = var4 & 255;
      return var1;
   }

   private int l() {
      // $FF: Couldn't be decompiled
   }

   private Bitmap m() {
      Config var1;
      if(this.w) {
         var1 = Config.ARGB_8888;
      } else {
         var1 = Config.RGB_565;
      }

      Bitmap var2 = this.p.a(this.v, this.u, var1);
      var2.setHasAlpha(true);
      return var2;
   }

   public int a(int var1) {
      byte var3 = -1;
      int var2 = var3;
      if(var1 >= 0) {
         var2 = var3;
         if(var1 < this.o.c) {
            var2 = ((b)this.o.e.get(var1)).i;
         }
      }

      return var2;
   }

   public ByteBuffer a() {
      return this.d;
   }

   public void a(c param1, ByteBuffer param2, int param3) {
      // $FF: Couldn't be decompiled
   }

   public void b() {
      this.n = (this.n + 1) % this.o.c;
   }

   public int c() {
      int var1;
      if(this.o.c > 0 && this.n >= 0) {
         var1 = this.a(this.n);
      } else {
         var1 = 0;
      }

      return var1;
   }

   public int d() {
      return this.o.c;
   }

   public int e() {
      return this.n;
   }

   public void f() {
      this.n = -1;
   }

   public int g() {
      return this.d.limit() + this.l.length + this.m.length * 4;
   }

   public Bitmap h() {
      // $FF: Couldn't be decompiled
   }

   public void i() {
      this.o = null;
      if(this.l != null) {
         this.p.a(this.l);
      }

      if(this.m != null) {
         this.p.a(this.m);
      }

      if(this.q != null) {
         this.p.a(this.q);
      }

      this.q = null;
      this.d = null;
      this.w = false;
      if(this.e != null) {
         this.p.a(this.e);
      }

      if(this.f != null) {
         this.p.a(this.f);
      }

   }
}
