package io.intercom.com.bumptech.glide.b;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import java.nio.ByteBuffer;

public interface a {
   ByteBuffer a();

   void b();

   int c();

   int d();

   int e();

   void f();

   int g();

   Bitmap h();

   void i();

   public interface a {
      Bitmap a(int var1, int var2, Config var3);

      void a(Bitmap var1);

      void a(byte[] var1);

      void a(int[] var1);

      byte[] a(int var1);

      int[] b(int var1);
   }
}
