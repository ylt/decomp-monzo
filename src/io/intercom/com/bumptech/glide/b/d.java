package io.intercom.com.bumptech.glide.b;

import android.util.Log;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

public class d {
   private final byte[] a = new byte[256];
   private ByteBuffer b;
   private c c;
   private int d = 0;

   private void a(int var1) {
      boolean var2 = false;

      while(!var2 && !this.o() && this.c.c <= var1) {
         switch(this.m()) {
         case 33:
            switch(this.m()) {
            case 1:
               this.k();
               continue;
            case 249:
               this.c.d = new b();
               this.e();
               continue;
            case 254:
               this.k();
               continue;
            case 255:
               this.l();
               String var4 = "";

               for(int var3 = 0; var3 < 11; ++var3) {
                  var4 = var4 + (char)this.a[var3];
               }

               if(var4.equals("NETSCAPE2.0")) {
                  this.g();
               } else {
                  this.k();
               }
               continue;
            default:
               this.k();
               continue;
            }
         case 44:
            if(this.c.d == null) {
               this.c.d = new b();
            }

            this.f();
            break;
         case 59:
            var2 = true;
            break;
         default:
            this.c.b = 1;
         }
      }

   }

   private int[] b(int var1) {
      int var2 = 0;
      byte[] var9 = new byte[var1 * 3];

      int[] var7;
      int[] var8;
      try {
         this.b.get(var9);
         var8 = new int[256];
      } catch (BufferUnderflowException var10) {
         var7 = null;
         if(Log.isLoggable("GifHeaderParser", 3)) {
            Log.d("GifHeaderParser", "Format Error Reading Color Table", var10);
         }

         this.c.b = 1;
         return var7;
      }

      int var3 = 0;

      while(true) {
         var7 = var8;
         if(var2 >= var1) {
            return var7;
         }

         int var6 = var3 + 1;
         byte var4 = var9[var3];
         int var5 = var6 + 1;
         byte var11 = var9[var6];
         var3 = var5 + 1;
         var8[var2] = (var4 & 255) << 16 | -16777216 | (var11 & 255) << 8 | var9[var5] & 255;
         ++var2;
      }
   }

   private void c() {
      this.b = null;
      Arrays.fill(this.a, 0);
      this.c = new c();
      this.d = 0;
   }

   private void d() {
      this.a(Integer.MAX_VALUE);
   }

   private void e() {
      boolean var3 = true;
      this.m();
      int var1 = this.m();
      this.c.d.g = (var1 & 28) >> 2;
      if(this.c.d.g == 0) {
         this.c.d.g = 1;
      }

      b var4 = this.c.d;
      if((var1 & 1) == 0) {
         var3 = false;
      }

      var4.f = var3;
      int var2 = this.n();
      var1 = var2;
      if(var2 < 2) {
         var1 = 10;
      }

      this.c.d.i = var1 * 10;
      this.c.d.h = this.m();
      this.m();
   }

   private void f() {
      boolean var4 = true;
      this.c.d.a = this.n();
      this.c.d.b = this.n();
      this.c.d.c = this.n();
      this.c.d.d = this.n();
      int var2 = this.m();
      boolean var1;
      if((var2 & 128) != 0) {
         var1 = true;
      } else {
         var1 = false;
      }

      int var3 = (int)Math.pow(2.0D, (double)((var2 & 7) + 1));
      b var5 = this.c.d;
      if((var2 & 64) == 0) {
         var4 = false;
      }

      var5.e = var4;
      if(var1) {
         this.c.d.k = this.b(var3);
      } else {
         this.c.d.k = null;
      }

      this.c.d.j = this.b.position();
      this.j();
      if(!this.o()) {
         c var6 = this.c;
         ++var6.c;
         this.c.e.add(this.c.d);
      }

   }

   private void g() {
      do {
         this.l();
         if(this.a[0] == 1) {
            byte var1 = this.a[1];
            byte var2 = this.a[2];
            this.c.m = var1 & 255 | (var2 & 255) << 8;
         }
      } while(this.d > 0 && !this.o());

   }

   private void h() {
      String var2 = "";

      for(int var1 = 0; var1 < 6; ++var1) {
         var2 = var2 + (char)this.m();
      }

      if(!var2.startsWith("GIF")) {
         this.c.b = 1;
      } else {
         this.i();
         if(this.c.h && !this.o()) {
            this.c.a = this.b(this.c.i);
            this.c.l = this.c.a[this.c.j];
         }
      }

   }

   private void i() {
      this.c.f = this.n();
      this.c.g = this.n();
      int var1 = this.m();
      c var3 = this.c;
      boolean var2;
      if((var1 & 128) != 0) {
         var2 = true;
      } else {
         var2 = false;
      }

      var3.h = var2;
      this.c.i = (int)Math.pow(2.0D, (double)((var1 & 7) + 1));
      this.c.j = this.m();
      this.c.k = this.m();
   }

   private void j() {
      this.m();
      this.k();
   }

   private void k() {
      int var2;
      do {
         var2 = this.m();
         int var1 = Math.min(this.b.position() + var2, this.b.limit());
         this.b.position(var1);
      } while(var2 > 0);

   }

   private int l() {
      // $FF: Couldn't be decompiled
   }

   private int m() {
      int var1 = 0;

      byte var2;
      try {
         var2 = this.b.get();
      } catch (Exception var4) {
         this.c.b = 1;
         return var1;
      }

      var1 = var2 & 255;
      return var1;
   }

   private int n() {
      return this.b.getShort();
   }

   private boolean o() {
      boolean var1;
      if(this.c.b != 0) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public d a(ByteBuffer var1) {
      this.c();
      this.b = var1.asReadOnlyBuffer();
      this.b.position(0);
      this.b.order(ByteOrder.LITTLE_ENDIAN);
      return this;
   }

   public void a() {
      this.b = null;
      this.c = null;
   }

   public c b() {
      if(this.b == null) {
         throw new IllegalStateException("You must call setData() before parseHeader()");
      } else {
         c var1;
         if(this.o()) {
            var1 = this.c;
         } else {
            this.h();
            if(!this.o()) {
               this.d();
               if(this.c.c < 0) {
                  this.c.b = 1;
               }
            }

            var1 = this.c;
         }

         return var1;
      }
   }
}
