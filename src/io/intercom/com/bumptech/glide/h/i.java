package io.intercom.com.bumptech.glide.h;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.os.Looper;
import android.os.Build.VERSION;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Queue;

public final class i {
   private static final char[] a = "0123456789abcdef".toCharArray();
   private static final char[] b = new char[64];

   public static int a(float var0) {
      return a(var0, 17);
   }

   public static int a(float var0, int var1) {
      return b(Float.floatToIntBits(var0), var1);
   }

   public static int a(int var0, int var1, Config var2) {
      return var0 * var1 * a(var2);
   }

   private static int a(Config var0) {
      Config var2 = var0;
      if(var0 == null) {
         var2 = Config.ARGB_8888;
      }

      byte var1;
      switch(null.a[var2.ordinal()]) {
      case 1:
         var1 = 1;
         break;
      case 2:
      case 3:
         var1 = 2;
         break;
      default:
         var1 = 4;
      }

      return var1;
   }

   @TargetApi(19)
   public static int a(Bitmap var0) {
      if(var0.isRecycled()) {
         throw new IllegalStateException("Cannot obtain size for recycled Bitmap: " + var0 + "[" + var0.getWidth() + "x" + var0.getHeight() + "] " + var0.getConfig());
      } else {
         int var1;
         if(VERSION.SDK_INT >= 19) {
            try {
               var1 = var0.getAllocationByteCount();
               return var1;
            } catch (NullPointerException var3) {
               ;
            }
         }

         var1 = var0.getHeight() * var0.getRowBytes();
         return var1;
      }
   }

   public static int a(Object var0, int var1) {
      int var2;
      if(var0 == null) {
         var2 = 0;
      } else {
         var2 = var0.hashCode();
      }

      return b(var2, var1);
   }

   public static int a(boolean var0, int var1) {
      byte var2;
      if(var0) {
         var2 = 1;
      } else {
         var2 = 0;
      }

      return b(var2, var1);
   }

   public static String a(byte[] param0) {
      // $FF: Couldn't be decompiled
   }

   private static String a(byte[] var0, char[] var1) {
      for(int var2 = 0; var2 < var0.length; ++var2) {
         int var3 = var0[var2] & 255;
         var1[var2 * 2] = a[var3 >>> 4];
         var1[var2 * 2 + 1] = a[var3 & 15];
      }

      return new String(var1);
   }

   public static List a(Collection var0) {
      ArrayList var1 = new ArrayList(var0.size());
      Iterator var2 = var0.iterator();

      while(var2.hasNext()) {
         var1.add(var2.next());
      }

      return var1;
   }

   public static Queue a(int var0) {
      return new ArrayDeque(var0);
   }

   public static void a() {
      if(!c()) {
         throw new IllegalArgumentException("You must call this method on the main thread");
      }
   }

   public static boolean a(int var0, int var1) {
      boolean var2;
      if(b(var0) && b(var1)) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   public static boolean a(Object var0, Object var1) {
      boolean var2;
      if(var0 == null) {
         if(var1 == null) {
            var2 = true;
         } else {
            var2 = false;
         }
      } else {
         var2 = var0.equals(var1);
      }

      return var2;
   }

   public static int b(int var0, int var1) {
      return var1 * 31 + var0;
   }

   public static void b() {
      if(!d()) {
         throw new IllegalArgumentException("You must call this method on a background thread");
      }
   }

   private static boolean b(int var0) {
      boolean var1;
      if(var0 <= 0 && var0 != Integer.MIN_VALUE) {
         var1 = false;
      } else {
         var1 = true;
      }

      return var1;
   }

   public static boolean c() {
      boolean var0;
      if(Looper.myLooper() == Looper.getMainLooper()) {
         var0 = true;
      } else {
         var0 = false;
      }

      return var0;
   }

   public static boolean d() {
      boolean var0;
      if(!c()) {
         var0 = true;
      } else {
         var0 = false;
      }

      return var0;
   }
}
