package io.intercom.com.bumptech.glide.h;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.concurrent.atomic.AtomicReference;

public final class a {
   private static final AtomicReference a = new AtomicReference();

   public static ByteBuffer a(File param0) throws IOException {
      // $FF: Couldn't be decompiled
   }

   public static void a(ByteBuffer param0, File param1) throws IOException {
      // $FF: Couldn't be decompiled
   }

   public static byte[] a(ByteBuffer var0) {
      a.b var1 = c(var0);
      byte[] var2;
      if(var1 != null && var1.a == 0 && var1.b == var1.c.length) {
         var2 = var0.array();
      } else {
         ByteBuffer var3 = var0.asReadOnlyBuffer();
         var2 = new byte[var3.limit()];
         var3.position(0);
         var3.get(var2);
      }

      return var2;
   }

   public static InputStream b(ByteBuffer var0) {
      return new a.a(var0);
   }

   private static a.b c(ByteBuffer var0) {
      a.b var1;
      if(!var0.isReadOnly() && var0.hasArray()) {
         var1 = new a.b(var0.array(), var0.arrayOffset(), var0.limit());
      } else {
         var1 = null;
      }

      return var1;
   }

   private static class a extends InputStream {
      private final ByteBuffer a;
      private int b = -1;

      public a(ByteBuffer var1) {
         this.a = var1;
      }

      public int available() throws IOException {
         return this.a.remaining();
      }

      public void mark(int var1) {
         synchronized(this){}

         try {
            this.b = this.a.position();
         } finally {
            ;
         }

      }

      public boolean markSupported() {
         return true;
      }

      public int read() throws IOException {
         byte var1;
         if(!this.a.hasRemaining()) {
            var1 = -1;
         } else {
            var1 = this.a.get();
         }

         return var1;
      }

      public int read(byte[] var1, int var2, int var3) throws IOException {
         if(!this.a.hasRemaining()) {
            var2 = -1;
         } else {
            var3 = Math.min(var3, this.available());
            this.a.get(var1, var2, var3);
            var2 = var3;
         }

         return var2;
      }

      public void reset() throws IOException {
         synchronized(this){}

         try {
            if(this.b == -1) {
               IOException var1 = new IOException("Cannot reset to unset mark position");
               throw var1;
            }

            this.a.position(this.b);
         } finally {
            ;
         }

      }

      public long skip(long var1) throws IOException {
         if(!this.a.hasRemaining()) {
            var1 = -1L;
         } else {
            var1 = Math.min(var1, (long)this.available());
            this.a.position((int)((long)this.a.position() + var1));
         }

         return var1;
      }
   }

   static final class b {
      final int a;
      final int b;
      final byte[] c;

      public b(byte[] var1, int var2, int var3) {
         this.c = var1;
         this.a = var2;
         this.b = var3;
      }
   }
}
