package io.intercom.com.bumptech.glide.h;

public class g {
   private Class a;
   private Class b;
   private Class c;

   public g() {
   }

   public g(Class var1, Class var2) {
      this.a(var1, var2);
   }

   public g(Class var1, Class var2, Class var3) {
      this.a(var1, var2, var3);
   }

   public void a(Class var1, Class var2) {
      this.a(var1, var2, (Class)null);
   }

   public void a(Class var1, Class var2, Class var3) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(this != var1) {
         if(var1 != null && this.getClass() == var1.getClass()) {
            g var3 = (g)var1;
            if(!this.a.equals(var3.a)) {
               var2 = false;
            } else if(!this.b.equals(var3.b)) {
               var2 = false;
            } else if(!i.a((Object)this.c, (Object)var3.c)) {
               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public int hashCode() {
      int var3 = this.a.hashCode();
      int var2 = this.b.hashCode();
      int var1;
      if(this.c != null) {
         var1 = this.c.hashCode();
      } else {
         var1 = 0;
      }

      return var1 + (var3 * 31 + var2) * 31;
   }

   public String toString() {
      return "MultiClassKey{first=" + this.a + ", second=" + this.b + '}';
   }
}
