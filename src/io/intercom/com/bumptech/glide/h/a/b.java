package io.intercom.com.bumptech.glide.h.a;

public abstract class b {
   private b() {
   }

   // $FF: synthetic method
   b(Object var1) {
      this();
   }

   public static b a() {
      return new b.a();
   }

   abstract void a(boolean var1);

   public abstract void b();

   private static class a extends b {
      private volatile boolean a;

      a() {
         super(null);
      }

      public void a(boolean var1) {
         this.a = var1;
      }

      public void b() {
         if(this.a) {
            throw new IllegalStateException("Already released");
         }
      }
   }
}
