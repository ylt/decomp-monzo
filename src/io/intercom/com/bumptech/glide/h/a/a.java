package io.intercom.com.bumptech.glide.h.a;

import android.util.Log;
import java.util.ArrayList;
import java.util.List;

public final class a {
   private static final a.d a = new a.d() {
      public void a(Object var1) {
      }
   };

   public static android.support.v4.g.k.a a() {
      return a(20);
   }

   public static android.support.v4.g.k.a a(int var0) {
      return a(new android.support.v4.g.k.c(var0), new a.a() {
         public List a() {
            return new ArrayList();
         }

         // $FF: synthetic method
         public Object b() {
            return this.a();
         }
      }, new a.d() {
         public void a(List var1) {
            var1.clear();
         }
      });
   }

   public static android.support.v4.g.k.a a(int var0, a.a var1) {
      return a(new android.support.v4.g.k.b(var0), var1);
   }

   private static android.support.v4.g.k.a a(android.support.v4.g.k.a var0, a.a var1) {
      return a(var0, var1, b());
   }

   private static android.support.v4.g.k.a a(android.support.v4.g.k.a var0, a.a var1, a.d var2) {
      return new a.b(var0, var1, var2);
   }

   public static android.support.v4.g.k.a b(int var0, a.a var1) {
      return a(new android.support.v4.g.k.c(var0), var1);
   }

   private static a.d b() {
      return a;
   }

   public interface a {
      Object b();
   }

   private static final class b implements android.support.v4.g.k.a {
      private final a.a a;
      private final a.d b;
      private final android.support.v4.g.k.a c;

      b(android.support.v4.g.k.a var1, a.a var2, a.d var3) {
         this.c = var1;
         this.a = var2;
         this.b = var3;
      }

      public Object a() {
         Object var2 = this.c.a();
         Object var1 = var2;
         if(var2 == null) {
            var2 = this.a.b();
            var1 = var2;
            if(Log.isLoggable("FactoryPools", 2)) {
               Log.v("FactoryPools", "Created new " + var2.getClass());
               var1 = var2;
            }
         }

         if(var1 instanceof a.c) {
            ((a.c)var1).m_().a(false);
         }

         return var1;
      }

      public boolean a(Object var1) {
         if(var1 instanceof a.c) {
            ((a.c)var1).m_().a(true);
         }

         this.b.a(var1);
         return this.c.a(var1);
      }
   }

   public interface c {
      b m_();
   }

   public interface d {
      void a(Object var1);
   }
}
