package io.intercom.com.bumptech.glide.h;

import android.text.TextUtils;
import java.util.Collection;

public final class h {
   public static Object a(Object var0) {
      return a(var0, "Argument must not be null");
   }

   public static Object a(Object var0, String var1) {
      if(var0 == null) {
         throw new NullPointerException(var1);
      } else {
         return var0;
      }
   }

   public static String a(String var0) {
      if(TextUtils.isEmpty(var0)) {
         throw new IllegalArgumentException("Must not be null or empty");
      } else {
         return var0;
      }
   }

   public static Collection a(Collection var0) {
      if(var0.isEmpty()) {
         throw new IllegalArgumentException("Must not be empty.");
      } else {
         return var0;
      }
   }

   public static void a(boolean var0, String var1) {
      if(!var0) {
         throw new IllegalArgumentException(var1);
      }
   }
}
