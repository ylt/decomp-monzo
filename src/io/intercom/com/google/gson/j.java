package io.intercom.com.google.gson;

import java.io.IOException;
import java.io.StringWriter;

public abstract class j {
   public Number a() {
      throw new UnsupportedOperationException(this.getClass().getSimpleName());
   }

   public String b() {
      throw new UnsupportedOperationException(this.getClass().getSimpleName());
   }

   public double c() {
      throw new UnsupportedOperationException(this.getClass().getSimpleName());
   }

   public long d() {
      throw new UnsupportedOperationException(this.getClass().getSimpleName());
   }

   public int e() {
      throw new UnsupportedOperationException(this.getClass().getSimpleName());
   }

   public boolean f() {
      throw new UnsupportedOperationException(this.getClass().getSimpleName());
   }

   public boolean g() {
      return this instanceof g;
   }

   public boolean h() {
      return this instanceof l;
   }

   public boolean i() {
      return this instanceof m;
   }

   public boolean j() {
      return this instanceof k;
   }

   public l k() {
      if(this.h()) {
         return (l)this;
      } else {
         throw new IllegalStateException("Not a JSON Object: " + this);
      }
   }

   public g l() {
      if(this.g()) {
         return (g)this;
      } else {
         throw new IllegalStateException("Not a JSON Array: " + this);
      }
   }

   public m m() {
      if(this.i()) {
         return (m)this;
      } else {
         throw new IllegalStateException("Not a JSON Primitive: " + this);
      }
   }

   Boolean n() {
      throw new UnsupportedOperationException(this.getClass().getSimpleName());
   }

   public String toString() {
      try {
         StringWriter var2 = new StringWriter();
         io.intercom.com.google.gson.stream.c var1 = new io.intercom.com.google.gson.stream.c(var2);
         var1.b(true);
         io.intercom.com.google.gson.b.j.a(this, var1);
         String var4 = var2.toString();
         return var4;
      } catch (IOException var3) {
         throw new AssertionError(var3);
      }
   }
}
