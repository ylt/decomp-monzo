package io.intercom.com.google.gson;

public class JsonParseException extends RuntimeException {
   public JsonParseException(String var1) {
      super(var1);
   }

   public JsonParseException(String var1, Throwable var2) {
      super(var1, var2);
   }

   public JsonParseException(Throwable var1) {
      super(var1);
   }
}
