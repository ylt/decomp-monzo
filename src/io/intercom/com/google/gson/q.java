package io.intercom.com.google.gson;

import java.io.IOException;

public abstract class q {
   public final j a(Object var1) {
      try {
         io.intercom.com.google.gson.b.a.f var2 = new io.intercom.com.google.gson.b.a.f();
         this.a(var2, var1);
         j var4 = var2.a();
         return var4;
      } catch (IOException var3) {
         throw new JsonIOException(var3);
      }
   }

   public final q a() {
      return new q() {
         public void a(io.intercom.com.google.gson.stream.c var1, Object var2) throws IOException {
            if(var2 == null) {
               var1.f();
            } else {
               q.this.a(var1, var2);
            }

         }

         public Object b(io.intercom.com.google.gson.stream.a var1) throws IOException {
            Object var2;
            if(var1.f() == io.intercom.com.google.gson.stream.b.i) {
               var1.j();
               var2 = null;
            } else {
               var2 = q.this.b(var1);
            }

            return var2;
         }
      };
   }

   public abstract void a(io.intercom.com.google.gson.stream.c var1, Object var2) throws IOException;

   public abstract Object b(io.intercom.com.google.gson.stream.a var1) throws IOException;
}
