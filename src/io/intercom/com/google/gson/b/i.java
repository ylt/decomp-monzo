package io.intercom.com.google.gson.b;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public final class i {
   private static final Map a;
   private static final Map b;

   static {
      HashMap var1 = new HashMap(16);
      HashMap var0 = new HashMap(16);
      a(var1, var0, Boolean.TYPE, Boolean.class);
      a(var1, var0, Byte.TYPE, Byte.class);
      a(var1, var0, Character.TYPE, Character.class);
      a(var1, var0, Double.TYPE, Double.class);
      a(var1, var0, Float.TYPE, Float.class);
      a(var1, var0, Integer.TYPE, Integer.class);
      a(var1, var0, Long.TYPE, Long.class);
      a(var1, var0, Short.TYPE, Short.class);
      a(var1, var0, Void.TYPE, Void.class);
      a = Collections.unmodifiableMap(var1);
      b = Collections.unmodifiableMap(var0);
   }

   public static Class a(Class var0) {
      Class var1 = (Class)a.get(a.a(var0));
      if(var1 != null) {
         var0 = var1;
      }

      return var0;
   }

   private static void a(Map var0, Map var1, Class var2, Class var3) {
      var0.put(var2, var3);
      var1.put(var3, var2);
   }

   public static boolean a(Type var0) {
      return a.containsKey(var0);
   }
}
