package io.intercom.com.google.gson.b.a;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

public final class f extends io.intercom.com.google.gson.stream.c {
   private static final Writer a = new Writer() {
      public void close() throws IOException {
         throw new AssertionError();
      }

      public void flush() throws IOException {
         throw new AssertionError();
      }

      public void write(char[] var1, int var2, int var3) {
         throw new AssertionError();
      }
   };
   private static final io.intercom.com.google.gson.m b = new io.intercom.com.google.gson.m("closed");
   private final List c = new ArrayList();
   private String d;
   private io.intercom.com.google.gson.j e;

   public f() {
      super(a);
      this.e = io.intercom.com.google.gson.k.a;
   }

   private void a(io.intercom.com.google.gson.j var1) {
      if(this.d != null) {
         if(!var1.j() || this.i()) {
            ((io.intercom.com.google.gson.l)this.j()).a(this.d, var1);
         }

         this.d = null;
      } else if(this.c.isEmpty()) {
         this.e = var1;
      } else {
         io.intercom.com.google.gson.j var2 = this.j();
         if(!(var2 instanceof io.intercom.com.google.gson.g)) {
            throw new IllegalStateException();
         }

         ((io.intercom.com.google.gson.g)var2).a(var1);
      }

   }

   private io.intercom.com.google.gson.j j() {
      return (io.intercom.com.google.gson.j)this.c.get(this.c.size() - 1);
   }

   public io.intercom.com.google.gson.j a() {
      if(!this.c.isEmpty()) {
         throw new IllegalStateException("Expected one JSON element but was " + this.c);
      } else {
         return this.e;
      }
   }

   public io.intercom.com.google.gson.stream.c a(long var1) throws IOException {
      this.a((io.intercom.com.google.gson.j)(new io.intercom.com.google.gson.m(Long.valueOf(var1))));
      return this;
   }

   public io.intercom.com.google.gson.stream.c a(Boolean var1) throws IOException {
      Object var2;
      if(var1 == null) {
         var2 = this.f();
      } else {
         this.a((io.intercom.com.google.gson.j)(new io.intercom.com.google.gson.m(var1)));
         var2 = this;
      }

      return (io.intercom.com.google.gson.stream.c)var2;
   }

   public io.intercom.com.google.gson.stream.c a(Number var1) throws IOException {
      Object var4;
      if(var1 == null) {
         var4 = this.f();
      } else {
         if(!this.g()) {
            double var2 = var1.doubleValue();
            if(Double.isNaN(var2) || Double.isInfinite(var2)) {
               throw new IllegalArgumentException("JSON forbids NaN and infinities: " + var1);
            }
         }

         this.a((io.intercom.com.google.gson.j)(new io.intercom.com.google.gson.m(var1)));
         var4 = this;
      }

      return (io.intercom.com.google.gson.stream.c)var4;
   }

   public io.intercom.com.google.gson.stream.c a(String var1) throws IOException {
      if(!this.c.isEmpty() && this.d == null) {
         if(this.j() instanceof io.intercom.com.google.gson.l) {
            this.d = var1;
            return this;
         } else {
            throw new IllegalStateException();
         }
      } else {
         throw new IllegalStateException();
      }
   }

   public io.intercom.com.google.gson.stream.c a(boolean var1) throws IOException {
      this.a((io.intercom.com.google.gson.j)(new io.intercom.com.google.gson.m(Boolean.valueOf(var1))));
      return this;
   }

   public io.intercom.com.google.gson.stream.c b() throws IOException {
      io.intercom.com.google.gson.g var1 = new io.intercom.com.google.gson.g();
      this.a((io.intercom.com.google.gson.j)var1);
      this.c.add(var1);
      return this;
   }

   public io.intercom.com.google.gson.stream.c b(String var1) throws IOException {
      Object var2;
      if(var1 == null) {
         var2 = this.f();
      } else {
         this.a((io.intercom.com.google.gson.j)(new io.intercom.com.google.gson.m(var1)));
         var2 = this;
      }

      return (io.intercom.com.google.gson.stream.c)var2;
   }

   public io.intercom.com.google.gson.stream.c c() throws IOException {
      if(!this.c.isEmpty() && this.d == null) {
         if(this.j() instanceof io.intercom.com.google.gson.g) {
            this.c.remove(this.c.size() - 1);
            return this;
         } else {
            throw new IllegalStateException();
         }
      } else {
         throw new IllegalStateException();
      }
   }

   public void close() throws IOException {
      if(!this.c.isEmpty()) {
         throw new IOException("Incomplete document");
      } else {
         this.c.add(b);
      }
   }

   public io.intercom.com.google.gson.stream.c d() throws IOException {
      io.intercom.com.google.gson.l var1 = new io.intercom.com.google.gson.l();
      this.a((io.intercom.com.google.gson.j)var1);
      this.c.add(var1);
      return this;
   }

   public io.intercom.com.google.gson.stream.c e() throws IOException {
      if(!this.c.isEmpty() && this.d == null) {
         if(this.j() instanceof io.intercom.com.google.gson.l) {
            this.c.remove(this.c.size() - 1);
            return this;
         } else {
            throw new IllegalStateException();
         }
      } else {
         throw new IllegalStateException();
      }
   }

   public io.intercom.com.google.gson.stream.c f() throws IOException {
      this.a((io.intercom.com.google.gson.j)io.intercom.com.google.gson.k.a);
      return this;
   }

   public void flush() throws IOException {
   }
}
