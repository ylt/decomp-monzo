package io.intercom.com.google.gson.b.a;

import io.intercom.com.google.gson.JsonSyntaxException;
import io.intercom.com.google.gson.q;
import io.intercom.com.google.gson.r;
import java.io.IOException;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public final class j extends q {
   public static final r a = new r() {
      public q a(io.intercom.com.google.gson.e var1, io.intercom.com.google.gson.c.a var2) {
         j var3;
         if(var2.a() == Date.class) {
            var3 = new j();
         } else {
            var3 = null;
         }

         return var3;
      }
   };
   private final DateFormat b = new SimpleDateFormat("MMM d, yyyy");

   public Date a(io.intercom.com.google.gson.stream.a var1) throws IOException {
      synchronized(this){}
      boolean var5 = false;

      Date var9;
      try {
         var5 = true;
         if(var1.f() != io.intercom.com.google.gson.stream.b.i) {
            try {
               var9 = new Date(this.b.parse(var1.h()).getTime());
               var5 = false;
               return var9;
            } catch (ParseException var6) {
               JsonSyntaxException var8 = new JsonSyntaxException(var6);
               throw var8;
            }
         }

         var1.j();
         var5 = false;
      } finally {
         if(var5) {
            ;
         }
      }

      var9 = null;
      return var9;
   }

   public void a(io.intercom.com.google.gson.stream.c param1, Date param2) throws IOException {
      // $FF: Couldn't be decompiled
   }

   // $FF: synthetic method
   public Object b(io.intercom.com.google.gson.stream.a var1) throws IOException {
      return this.a(var1);
   }
}
