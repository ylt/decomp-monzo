package io.intercom.com.google.gson.b.a;

import io.intercom.com.google.gson.q;
import io.intercom.com.google.gson.r;
import java.io.IOException;
import java.lang.reflect.Array;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.Type;
import java.util.ArrayList;

public final class a extends q {
   public static final r a = new r() {
      public q a(io.intercom.com.google.gson.e var1, io.intercom.com.google.gson.c.a var2) {
         Type var4 = var2.b();
         a var3;
         if(var4 instanceof GenericArrayType || var4 instanceof Class && ((Class)var4).isArray()) {
            var4 = io.intercom.com.google.gson.b.b.g(var4);
            var3 = new a(var1, var1.a(io.intercom.com.google.gson.c.a.a(var4)), io.intercom.com.google.gson.b.b.e(var4));
         } else {
            var3 = null;
         }

         return var3;
      }
   };
   private final Class b;
   private final q c;

   public a(io.intercom.com.google.gson.e var1, q var2, Class var3) {
      this.c = new m(var1, var2, var3);
      this.b = var3;
   }

   public void a(io.intercom.com.google.gson.stream.c var1, Object var2) throws IOException {
      if(var2 == null) {
         var1.f();
      } else {
         var1.b();
         int var3 = 0;

         for(int var4 = Array.getLength(var2); var3 < var4; ++var3) {
            Object var5 = Array.get(var2, var3);
            this.c.a(var1, var5);
         }

         var1.c();
      }

   }

   public Object b(io.intercom.com.google.gson.stream.a var1) throws IOException {
      Object var6;
      if(var1.f() == io.intercom.com.google.gson.stream.b.i) {
         var1.j();
         var6 = null;
      } else {
         ArrayList var5 = new ArrayList();
         var1.a();

         while(var1.e()) {
            var5.add(this.c.b(var1));
         }

         var1.b();
         int var3 = var5.size();
         Object var4 = Array.newInstance(this.b, var3);
         int var2 = 0;

         while(true) {
            var6 = var4;
            if(var2 >= var3) {
               break;
            }

            Array.set(var4, var2, var5.get(var2));
            ++var2;
         }
      }

      return var6;
   }
}
