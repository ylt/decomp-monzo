package io.intercom.com.google.gson.b.a;

import io.intercom.com.google.gson.JsonSyntaxException;
import io.intercom.com.google.gson.q;
import io.intercom.com.google.gson.r;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public final class g implements r {
   final boolean a;
   private final io.intercom.com.google.gson.b.c b;

   public g(io.intercom.com.google.gson.b.c var1, boolean var2) {
      this.b = var1;
      this.a = var2;
   }

   private q a(io.intercom.com.google.gson.e var1, Type var2) {
      q var3;
      if(var2 != Boolean.TYPE && var2 != Boolean.class) {
         var3 = var1.a(io.intercom.com.google.gson.c.a.a(var2));
      } else {
         var3 = n.f;
      }

      return var3;
   }

   public q a(io.intercom.com.google.gson.e var1, io.intercom.com.google.gson.c.a var2) {
      Type var3 = var2.b();
      g.a var6;
      if(!Map.class.isAssignableFrom(var2.a())) {
         var6 = null;
      } else {
         Type[] var8 = io.intercom.com.google.gson.b.b.b(var3, io.intercom.com.google.gson.b.b.e(var3));
         q var4 = this.a(var1, var8[0]);
         q var5 = var1.a(io.intercom.com.google.gson.c.a.a(var8[1]));
         io.intercom.com.google.gson.b.h var7 = this.b.a(var2);
         var6 = new g.a(var1, var8[0], var4, var8[1], var5, var7);
      }

      return var6;
   }

   private final class a extends q {
      private final q b;
      private final q c;
      private final io.intercom.com.google.gson.b.h d;

      public a(io.intercom.com.google.gson.e var2, Type var3, q var4, Type var5, q var6, io.intercom.com.google.gson.b.h var7) {
         this.b = new m(var2, var4, var3);
         this.c = new m(var2, var6, var5);
         this.d = var7;
      }

      private String a(io.intercom.com.google.gson.j var1) {
         String var3;
         if(var1.i()) {
            io.intercom.com.google.gson.m var2 = var1.m();
            if(var2.p()) {
               var3 = String.valueOf(var2.a());
            } else if(var2.o()) {
               var3 = Boolean.toString(var2.f());
            } else {
               if(!var2.q()) {
                  throw new AssertionError();
               }

               var3 = var2.b();
            }
         } else {
            if(!var1.j()) {
               throw new AssertionError();
            }

            var3 = "null";
         }

         return var3;
      }

      public Map a(io.intercom.com.google.gson.stream.a var1) throws IOException {
         io.intercom.com.google.gson.stream.b var3 = var1.f();
         Map var4;
         if(var3 == io.intercom.com.google.gson.stream.b.i) {
            var1.j();
            var4 = null;
         } else {
            Map var2 = (Map)this.d.a();
            Object var5;
            if(var3 == io.intercom.com.google.gson.stream.b.a) {
               var1.a();

               while(var1.e()) {
                  var1.a();
                  var5 = this.b.b(var1);
                  if(var2.put(var5, this.c.b(var1)) != null) {
                     throw new JsonSyntaxException("duplicate key: " + var5);
                  }

                  var1.b();
               }

               var1.b();
               var4 = var2;
            } else {
               var1.c();

               while(var1.e()) {
                  io.intercom.com.google.gson.b.e.a.a(var1);
                  var5 = this.b.b(var1);
                  if(var2.put(var5, this.c.b(var1)) != null) {
                     throw new JsonSyntaxException("duplicate key: " + var5);
                  }
               }

               var1.d();
               var4 = var2;
            }
         }

         return var4;
      }

      public void a(io.intercom.com.google.gson.stream.c var1, Map var2) throws IOException {
         byte var6 = 0;
         byte var5 = 0;
         if(var2 == null) {
            var1.f();
         } else if(!g.this.a) {
            var1.d();
            Iterator var7 = var2.entrySet().iterator();

            while(var7.hasNext()) {
               Entry var11 = (Entry)var7.next();
               var1.a(String.valueOf(var11.getKey()));
               this.c.a(var1, var11.getValue());
            }

            var1.e();
         } else {
            ArrayList var8 = new ArrayList(var2.size());
            ArrayList var15 = new ArrayList(var2.size());
            Iterator var12 = var2.entrySet().iterator();

            boolean var3;
            boolean var4;
            for(var3 = false; var12.hasNext(); var3 |= var4) {
               Entry var10 = (Entry)var12.next();
               io.intercom.com.google.gson.j var9 = this.b.a(var10.getKey());
               var8.add(var9);
               var15.add(var10.getValue());
               if(!var9.g() && !var9.h()) {
                  var4 = false;
               } else {
                  var4 = true;
               }
            }

            int var13;
            int var14;
            if(var3) {
               var1.b();
               var14 = var8.size();

               for(var13 = var5; var13 < var14; ++var13) {
                  var1.b();
                  io.intercom.com.google.gson.b.j.a((io.intercom.com.google.gson.j)var8.get(var13), var1);
                  this.c.a(var1, var15.get(var13));
                  var1.c();
               }

               var1.c();
            } else {
               var1.d();
               var14 = var8.size();

               for(var13 = var6; var13 < var14; ++var13) {
                  var1.a(this.a((io.intercom.com.google.gson.j)var8.get(var13)));
                  this.c.a(var1, var15.get(var13));
               }

               var1.e();
            }
         }

      }

      // $FF: synthetic method
      public Object b(io.intercom.com.google.gson.stream.a var1) throws IOException {
         return this.a(var1);
      }
   }
}
