package io.intercom.com.google.gson.b.a;

import java.io.IOException;
import java.io.Reader;
import java.util.Iterator;
import java.util.Map.Entry;

public final class e extends io.intercom.com.google.gson.stream.a {
   private static final Reader b = new Reader() {
      public void close() throws IOException {
         throw new AssertionError();
      }

      public int read(char[] var1, int var2, int var3) throws IOException {
         throw new AssertionError();
      }
   };
   private static final Object c = new Object();
   private Object[] d;
   private int e;
   private String[] f;
   private int[] g;

   private void a(io.intercom.com.google.gson.stream.b var1) throws IOException {
      if(this.f() != var1) {
         throw new IllegalStateException("Expected " + var1 + " but was " + this.f() + this.v());
      }
   }

   private void a(Object var1) {
      Object[] var3;
      if(this.e == this.d.length) {
         var3 = new Object[this.e * 2];
         int[] var5 = new int[this.e * 2];
         String[] var4 = new String[this.e * 2];
         System.arraycopy(this.d, 0, var3, 0, this.e);
         System.arraycopy(this.g, 0, var5, 0, this.e);
         System.arraycopy(this.f, 0, var4, 0, this.e);
         this.d = var3;
         this.g = var5;
         this.f = var4;
      }

      var3 = this.d;
      int var2 = this.e;
      this.e = var2 + 1;
      var3[var2] = var1;
   }

   private Object t() {
      return this.d[this.e - 1];
   }

   private Object u() {
      Object[] var2 = this.d;
      int var1 = this.e - 1;
      this.e = var1;
      Object var3 = var2[var1];
      this.d[this.e] = null;
      return var3;
   }

   private String v() {
      return " at path " + this.p();
   }

   public void a() throws IOException {
      this.a(io.intercom.com.google.gson.stream.b.a);
      this.a((Object)((io.intercom.com.google.gson.g)this.t()).iterator());
      this.g[this.e - 1] = 0;
   }

   public void b() throws IOException {
      this.a(io.intercom.com.google.gson.stream.b.b);
      this.u();
      this.u();
      if(this.e > 0) {
         int[] var2 = this.g;
         int var1 = this.e - 1;
         ++var2[var1];
      }

   }

   public void c() throws IOException {
      this.a(io.intercom.com.google.gson.stream.b.c);
      this.a((Object)((io.intercom.com.google.gson.l)this.t()).o().iterator());
   }

   public void close() throws IOException {
      this.d = new Object[]{c};
      this.e = 1;
   }

   public void d() throws IOException {
      this.a(io.intercom.com.google.gson.stream.b.d);
      this.u();
      this.u();
      if(this.e > 0) {
         int[] var2 = this.g;
         int var1 = this.e - 1;
         ++var2[var1];
      }

   }

   public boolean e() throws IOException {
      io.intercom.com.google.gson.stream.b var2 = this.f();
      boolean var1;
      if(var2 != io.intercom.com.google.gson.stream.b.d && var2 != io.intercom.com.google.gson.stream.b.b) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public io.intercom.com.google.gson.stream.b f() throws IOException {
      io.intercom.com.google.gson.stream.b var2;
      if(this.e == 0) {
         var2 = io.intercom.com.google.gson.stream.b.j;
      } else {
         Object var3 = this.t();
         if(var3 instanceof Iterator) {
            boolean var1 = this.d[this.e - 2] instanceof io.intercom.com.google.gson.l;
            Iterator var4 = (Iterator)var3;
            if(var4.hasNext()) {
               if(var1) {
                  var2 = io.intercom.com.google.gson.stream.b.e;
               } else {
                  this.a(var4.next());
                  var2 = this.f();
               }
            } else if(var1) {
               var2 = io.intercom.com.google.gson.stream.b.d;
            } else {
               var2 = io.intercom.com.google.gson.stream.b.b;
            }
         } else if(var3 instanceof io.intercom.com.google.gson.l) {
            var2 = io.intercom.com.google.gson.stream.b.c;
         } else if(var3 instanceof io.intercom.com.google.gson.g) {
            var2 = io.intercom.com.google.gson.stream.b.a;
         } else if(var3 instanceof io.intercom.com.google.gson.m) {
            io.intercom.com.google.gson.m var5 = (io.intercom.com.google.gson.m)var3;
            if(var5.q()) {
               var2 = io.intercom.com.google.gson.stream.b.f;
            } else if(var5.o()) {
               var2 = io.intercom.com.google.gson.stream.b.h;
            } else {
               if(!var5.p()) {
                  throw new AssertionError();
               }

               var2 = io.intercom.com.google.gson.stream.b.g;
            }
         } else {
            if(!(var3 instanceof io.intercom.com.google.gson.k)) {
               if(var3 == c) {
                  throw new IllegalStateException("JsonReader is closed");
               }

               throw new AssertionError();
            }

            var2 = io.intercom.com.google.gson.stream.b.i;
         }
      }

      return var2;
   }

   public String g() throws IOException {
      this.a(io.intercom.com.google.gson.stream.b.e);
      Entry var1 = (Entry)((Iterator)this.t()).next();
      String var2 = (String)var1.getKey();
      this.f[this.e - 1] = var2;
      this.a(var1.getValue());
      return var2;
   }

   public String h() throws IOException {
      io.intercom.com.google.gson.stream.b var2 = this.f();
      if(var2 != io.intercom.com.google.gson.stream.b.f && var2 != io.intercom.com.google.gson.stream.b.g) {
         throw new IllegalStateException("Expected " + io.intercom.com.google.gson.stream.b.f + " but was " + var2 + this.v());
      } else {
         String var3 = ((io.intercom.com.google.gson.m)this.u()).b();
         if(this.e > 0) {
            int[] var4 = this.g;
            int var1 = this.e - 1;
            ++var4[var1];
         }

         return var3;
      }
   }

   public boolean i() throws IOException {
      this.a(io.intercom.com.google.gson.stream.b.h);
      boolean var2 = ((io.intercom.com.google.gson.m)this.u()).f();
      if(this.e > 0) {
         int[] var3 = this.g;
         int var1 = this.e - 1;
         ++var3[var1];
      }

      return var2;
   }

   public void j() throws IOException {
      this.a(io.intercom.com.google.gson.stream.b.i);
      this.u();
      if(this.e > 0) {
         int[] var2 = this.g;
         int var1 = this.e - 1;
         ++var2[var1];
      }

   }

   public double k() throws IOException {
      io.intercom.com.google.gson.stream.b var4 = this.f();
      if(var4 != io.intercom.com.google.gson.stream.b.g && var4 != io.intercom.com.google.gson.stream.b.f) {
         throw new IllegalStateException("Expected " + io.intercom.com.google.gson.stream.b.g + " but was " + var4 + this.v());
      } else {
         double var1 = ((io.intercom.com.google.gson.m)this.t()).c();
         if(this.q() || !Double.isNaN(var1) && !Double.isInfinite(var1)) {
            this.u();
            if(this.e > 0) {
               int[] var5 = this.g;
               int var3 = this.e - 1;
               ++var5[var3];
            }

            return var1;
         } else {
            throw new NumberFormatException("JSON forbids NaN and infinities: " + var1);
         }
      }
   }

   public long l() throws IOException {
      io.intercom.com.google.gson.stream.b var4 = this.f();
      if(var4 != io.intercom.com.google.gson.stream.b.g && var4 != io.intercom.com.google.gson.stream.b.f) {
         throw new IllegalStateException("Expected " + io.intercom.com.google.gson.stream.b.g + " but was " + var4 + this.v());
      } else {
         long var2 = ((io.intercom.com.google.gson.m)this.t()).d();
         this.u();
         if(this.e > 0) {
            int[] var5 = this.g;
            int var1 = this.e - 1;
            ++var5[var1];
         }

         return var2;
      }
   }

   public int m() throws IOException {
      io.intercom.com.google.gson.stream.b var3 = this.f();
      if(var3 != io.intercom.com.google.gson.stream.b.g && var3 != io.intercom.com.google.gson.stream.b.f) {
         throw new IllegalStateException("Expected " + io.intercom.com.google.gson.stream.b.g + " but was " + var3 + this.v());
      } else {
         int var1 = ((io.intercom.com.google.gson.m)this.t()).e();
         this.u();
         if(this.e > 0) {
            int[] var4 = this.g;
            int var2 = this.e - 1;
            ++var4[var2];
         }

         return var1;
      }
   }

   public void n() throws IOException {
      if(this.f() == io.intercom.com.google.gson.stream.b.e) {
         this.g();
         this.f[this.e - 2] = "null";
      } else {
         this.u();
         if(this.e > 0) {
            this.f[this.e - 1] = "null";
         }
      }

      if(this.e > 0) {
         int[] var2 = this.g;
         int var1 = this.e - 1;
         ++var2[var1];
      }

   }

   public void o() throws IOException {
      this.a(io.intercom.com.google.gson.stream.b.e);
      Entry var1 = (Entry)((Iterator)this.t()).next();
      this.a(var1.getValue());
      this.a((Object)(new io.intercom.com.google.gson.m((String)var1.getKey())));
   }

   public String p() {
      StringBuilder var3 = (new StringBuilder()).append('$');

      int var1;
      for(int var2 = 0; var2 < this.e; var2 = var1 + 1) {
         Object[] var4;
         if(this.d[var2] instanceof io.intercom.com.google.gson.g) {
            var4 = this.d;
            ++var2;
            var1 = var2;
            if(var4[var2] instanceof Iterator) {
               var3.append('[').append(this.g[var2]).append(']');
               var1 = var2;
            }
         } else {
            var1 = var2;
            if(this.d[var2] instanceof io.intercom.com.google.gson.l) {
               var4 = this.d;
               ++var2;
               var1 = var2;
               if(var4[var2] instanceof Iterator) {
                  var3.append('.');
                  var1 = var2;
                  if(this.f[var2] != null) {
                     var3.append(this.f[var2]);
                     var1 = var2;
                  }
               }
            }
         }
      }

      return var3.toString();
   }

   public String toString() {
      return this.getClass().getSimpleName();
   }
}
