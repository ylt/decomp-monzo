package io.intercom.com.google.gson.b.a;

import io.intercom.com.google.gson.q;
import io.intercom.com.google.gson.r;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.Iterator;

public final class b implements r {
   private final io.intercom.com.google.gson.b.c a;

   public b(io.intercom.com.google.gson.b.c var1) {
      this.a = var1;
   }

   public q a(io.intercom.com.google.gson.e var1, io.intercom.com.google.gson.c.a var2) {
      Type var4 = var2.b();
      Class var3 = var2.a();
      b.a var5;
      if(!Collection.class.isAssignableFrom(var3)) {
         var5 = null;
      } else {
         Type var6 = io.intercom.com.google.gson.b.b.a(var4, var3);
         var5 = new b.a(var1, var6, var1.a(io.intercom.com.google.gson.c.a.a(var6)), this.a.a(var2));
      }

      return var5;
   }

   private static final class a extends q {
      private final q a;
      private final io.intercom.com.google.gson.b.h b;

      public a(io.intercom.com.google.gson.e var1, Type var2, q var3, io.intercom.com.google.gson.b.h var4) {
         this.a = new m(var1, var3, var2);
         this.b = var4;
      }

      public Collection a(io.intercom.com.google.gson.stream.a var1) throws IOException {
         Collection var3;
         if(var1.f() == io.intercom.com.google.gson.stream.b.i) {
            var1.j();
            var3 = null;
         } else {
            Collection var2 = (Collection)this.b.a();
            var1.a();

            while(var1.e()) {
               var2.add(this.a.b(var1));
            }

            var1.b();
            var3 = var2;
         }

         return var3;
      }

      public void a(io.intercom.com.google.gson.stream.c var1, Collection var2) throws IOException {
         if(var2 == null) {
            var1.f();
         } else {
            var1.b();
            Iterator var3 = var2.iterator();

            while(var3.hasNext()) {
               Object var4 = var3.next();
               this.a.a(var1, var4);
            }

            var1.c();
         }

      }

      // $FF: synthetic method
      public Object b(io.intercom.com.google.gson.stream.a var1) throws IOException {
         return this.a(var1);
      }
   }
}
