package io.intercom.com.google.gson.b.a;

import io.intercom.com.google.gson.JsonSyntaxException;
import io.intercom.com.google.gson.q;
import io.intercom.com.google.gson.r;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.util.Date;
import java.util.Locale;

public final class c extends q {
   public static final r a = new r() {
      public q a(io.intercom.com.google.gson.e var1, io.intercom.com.google.gson.c.a var2) {
         c var3;
         if(var2.a() == Date.class) {
            var3 = new c();
         } else {
            var3 = null;
         }

         return var3;
      }
   };
   private final DateFormat b;
   private final DateFormat c;

   public c() {
      this.b = DateFormat.getDateTimeInstance(2, 2, Locale.US);
      this.c = DateFormat.getDateTimeInstance(2, 2);
   }

   private Date a(String var1) {
      synchronized(this){}
      boolean var8 = false;

      Date var13;
      Date var15;
      label84: {
         label85: {
            try {
               try {
                  var8 = true;
                  var15 = this.c.parse(var1);
                  var8 = false;
                  break label84;
               } catch (ParseException var10) {
                  ;
               }

               try {
                  var15 = this.b.parse(var1);
                  var8 = false;
                  break label85;
               } catch (ParseException var11) {
                  try {
                     ParsePosition var14 = new ParsePosition(0);
                     var15 = io.intercom.com.google.gson.b.a.a.a.a(var1, var14);
                     var8 = false;
                  } catch (ParseException var9) {
                     JsonSyntaxException var2 = new JsonSyntaxException(var1, var9);
                     throw var2;
                  }
               }
            } finally {
               if(var8) {
                  ;
               }
            }

            var13 = var15;
            return var13;
         }

         var13 = var15;
         return var13;
      }

      var13 = var15;
      return var13;
   }

   public Date a(io.intercom.com.google.gson.stream.a var1) throws IOException {
      Date var2;
      if(var1.f() == io.intercom.com.google.gson.stream.b.i) {
         var1.j();
         var2 = null;
      } else {
         var2 = this.a(var1.h());
      }

      return var2;
   }

   public void a(io.intercom.com.google.gson.stream.c param1, Date param2) throws IOException {
      // $FF: Couldn't be decompiled
   }

   // $FF: synthetic method
   public Object b(io.intercom.com.google.gson.stream.a var1) throws IOException {
      return this.a(var1);
   }
}
