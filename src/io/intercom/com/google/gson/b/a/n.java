package io.intercom.com.google.gson.b.a;

import io.intercom.com.google.gson.JsonSyntaxException;
import io.intercom.com.google.gson.q;
import io.intercom.com.google.gson.r;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.URI;
import java.net.URL;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Calendar;
import java.util.Currency;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.UUID;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicIntegerArray;

public final class n {
   public static final q A;
   public static final q B;
   public static final q C;
   public static final r D;
   public static final q E;
   public static final r F;
   public static final q G;
   public static final r H;
   public static final q I;
   public static final r J;
   public static final q K;
   public static final r L;
   public static final q M;
   public static final r N;
   public static final q O;
   public static final r P;
   public static final q Q;
   public static final r R;
   public static final r S;
   public static final q T;
   public static final r U;
   public static final q V;
   public static final r W;
   public static final q X;
   public static final r Y;
   public static final r Z;
   public static final q a = (new q() {
      public Class a(io.intercom.com.google.gson.stream.a var1) throws IOException {
         throw new UnsupportedOperationException("Attempted to deserialize a java.lang.Class. Forgot to register a type adapter?");
      }

      public void a(io.intercom.com.google.gson.stream.c var1, Class var2) throws IOException {
         throw new UnsupportedOperationException("Attempted to serialize java.lang.Class: " + var2.getName() + ". Forgot to register a type adapter?");
      }

      // $FF: synthetic method
      public Object b(io.intercom.com.google.gson.stream.a var1) throws IOException {
         return this.a(var1);
      }
   }).a();
   public static final r b;
   public static final q c;
   public static final r d;
   public static final q e;
   public static final q f;
   public static final r g;
   public static final q h;
   public static final r i;
   public static final q j;
   public static final r k;
   public static final q l;
   public static final r m;
   public static final q n;
   public static final r o;
   public static final q p;
   public static final r q;
   public static final q r;
   public static final r s;
   public static final q t;
   public static final q u;
   public static final q v;
   public static final q w;
   public static final r x;
   public static final q y;
   public static final r z;

   static {
      b = a(Class.class, a);
      c = (new q() {
         public BitSet a(io.intercom.com.google.gson.stream.a var1) throws IOException {
            BitSet var6 = new BitSet();
            var1.a();
            io.intercom.com.google.gson.stream.b var5 = var1.f();

            for(int var2 = 0; var5 != io.intercom.com.google.gson.stream.b.b; var5 = var1.f()) {
               boolean var4;
               switch(null.a[var5.ordinal()]) {
               case 1:
                  if(var1.m() != 0) {
                     var4 = true;
                  } else {
                     var4 = false;
                  }
                  break;
               case 2:
                  var4 = var1.i();
                  break;
               case 3:
                  String var8 = var1.h();

                  int var3;
                  try {
                     var3 = Integer.parseInt(var8);
                  } catch (NumberFormatException var7) {
                     throw new JsonSyntaxException("Error: Expecting: bitset number value (1, 0), Found: " + var8);
                  }

                  if(var3 != 0) {
                     var4 = true;
                  } else {
                     var4 = false;
                  }
                  break;
               default:
                  throw new JsonSyntaxException("Invalid bitset value type: " + var5);
               }

               if(var4) {
                  var6.set(var2);
               }

               ++var2;
            }

            var1.b();
            return var6;
         }

         public void a(io.intercom.com.google.gson.stream.c var1, BitSet var2) throws IOException {
            var1.b();
            int var5 = var2.length();

            for(int var3 = 0; var3 < var5; ++var3) {
               byte var4;
               if(var2.get(var3)) {
                  var4 = 1;
               } else {
                  var4 = 0;
               }

               var1.a((long)var4);
            }

            var1.c();
         }

         // $FF: synthetic method
         public Object b(io.intercom.com.google.gson.stream.a var1) throws IOException {
            return this.a(var1);
         }
      }).a();
      d = a(BitSet.class, c);
      e = new q() {
         public Boolean a(io.intercom.com.google.gson.stream.a var1) throws IOException {
            Boolean var2;
            if(var1.f() == io.intercom.com.google.gson.stream.b.i) {
               var1.j();
               var2 = null;
            } else if(var1.f() == io.intercom.com.google.gson.stream.b.f) {
               var2 = Boolean.valueOf(Boolean.parseBoolean(var1.h()));
            } else {
               var2 = Boolean.valueOf(var1.i());
            }

            return var2;
         }

         public void a(io.intercom.com.google.gson.stream.c var1, Boolean var2) throws IOException {
            var1.a(var2);
         }

         // $FF: synthetic method
         public Object b(io.intercom.com.google.gson.stream.a var1) throws IOException {
            return this.a(var1);
         }
      };
      f = new q() {
         public Boolean a(io.intercom.com.google.gson.stream.a var1) throws IOException {
            Boolean var2;
            if(var1.f() == io.intercom.com.google.gson.stream.b.i) {
               var1.j();
               var2 = null;
            } else {
               var2 = Boolean.valueOf(var1.h());
            }

            return var2;
         }

         public void a(io.intercom.com.google.gson.stream.c var1, Boolean var2) throws IOException {
            String var3;
            if(var2 == null) {
               var3 = "null";
            } else {
               var3 = var2.toString();
            }

            var1.b(var3);
         }

         // $FF: synthetic method
         public Object b(io.intercom.com.google.gson.stream.a var1) throws IOException {
            return this.a(var1);
         }
      };
      g = a(Boolean.TYPE, Boolean.class, e);
      h = new q() {
         public Number a(io.intercom.com.google.gson.stream.a var1) throws IOException {
            Byte var4;
            if(var1.f() == io.intercom.com.google.gson.stream.b.i) {
               var1.j();
               var4 = null;
            } else {
               byte var2;
               try {
                  var2 = (byte)var1.m();
               } catch (NumberFormatException var3) {
                  throw new JsonSyntaxException(var3);
               }

               var4 = Byte.valueOf(var2);
            }

            return var4;
         }

         public void a(io.intercom.com.google.gson.stream.c var1, Number var2) throws IOException {
            var1.a(var2);
         }

         // $FF: synthetic method
         public Object b(io.intercom.com.google.gson.stream.a var1) throws IOException {
            return this.a(var1);
         }
      };
      i = a(Byte.TYPE, Byte.class, h);
      j = new q() {
         public Number a(io.intercom.com.google.gson.stream.a var1) throws IOException {
            Short var4;
            if(var1.f() == io.intercom.com.google.gson.stream.b.i) {
               var1.j();
               var4 = null;
            } else {
               short var2;
               try {
                  var2 = (short)var1.m();
               } catch (NumberFormatException var3) {
                  throw new JsonSyntaxException(var3);
               }

               var4 = Short.valueOf(var2);
            }

            return var4;
         }

         public void a(io.intercom.com.google.gson.stream.c var1, Number var2) throws IOException {
            var1.a(var2);
         }

         // $FF: synthetic method
         public Object b(io.intercom.com.google.gson.stream.a var1) throws IOException {
            return this.a(var1);
         }
      };
      k = a(Short.TYPE, Short.class, j);
      l = new q() {
         public Number a(io.intercom.com.google.gson.stream.a var1) throws IOException {
            Integer var4;
            if(var1.f() == io.intercom.com.google.gson.stream.b.i) {
               var1.j();
               var4 = null;
            } else {
               int var2;
               try {
                  var2 = var1.m();
               } catch (NumberFormatException var3) {
                  throw new JsonSyntaxException(var3);
               }

               var4 = Integer.valueOf(var2);
            }

            return var4;
         }

         public void a(io.intercom.com.google.gson.stream.c var1, Number var2) throws IOException {
            var1.a(var2);
         }

         // $FF: synthetic method
         public Object b(io.intercom.com.google.gson.stream.a var1) throws IOException {
            return this.a(var1);
         }
      };
      m = a(Integer.TYPE, Integer.class, l);
      n = (new q() {
         public AtomicInteger a(io.intercom.com.google.gson.stream.a var1) throws IOException {
            try {
               AtomicInteger var3 = new AtomicInteger(var1.m());
               return var3;
            } catch (NumberFormatException var2) {
               throw new JsonSyntaxException(var2);
            }
         }

         public void a(io.intercom.com.google.gson.stream.c var1, AtomicInteger var2) throws IOException {
            var1.a((long)var2.get());
         }

         // $FF: synthetic method
         public Object b(io.intercom.com.google.gson.stream.a var1) throws IOException {
            return this.a(var1);
         }
      }).a();
      o = a(AtomicInteger.class, n);
      p = (new q() {
         public AtomicBoolean a(io.intercom.com.google.gson.stream.a var1) throws IOException {
            return new AtomicBoolean(var1.i());
         }

         public void a(io.intercom.com.google.gson.stream.c var1, AtomicBoolean var2) throws IOException {
            var1.a(var2.get());
         }

         // $FF: synthetic method
         public Object b(io.intercom.com.google.gson.stream.a var1) throws IOException {
            return this.a(var1);
         }
      }).a();
      q = a(AtomicBoolean.class, p);
      r = (new q() {
         public AtomicIntegerArray a(io.intercom.com.google.gson.stream.a var1) throws IOException {
            ArrayList var4 = new ArrayList();
            var1.a();

            while(var1.e()) {
               try {
                  var4.add(Integer.valueOf(var1.m()));
               } catch (NumberFormatException var5) {
                  throw new JsonSyntaxException(var5);
               }
            }

            var1.b();
            int var3 = var4.size();
            AtomicIntegerArray var6 = new AtomicIntegerArray(var3);

            for(int var2 = 0; var2 < var3; ++var2) {
               var6.set(var2, ((Integer)var4.get(var2)).intValue());
            }

            return var6;
         }

         public void a(io.intercom.com.google.gson.stream.c var1, AtomicIntegerArray var2) throws IOException {
            var1.b();
            int var3 = 0;

            for(int var4 = var2.length(); var3 < var4; ++var3) {
               var1.a((long)var2.get(var3));
            }

            var1.c();
         }

         // $FF: synthetic method
         public Object b(io.intercom.com.google.gson.stream.a var1) throws IOException {
            return this.a(var1);
         }
      }).a();
      s = a(AtomicIntegerArray.class, r);
      t = new q() {
         public Number a(io.intercom.com.google.gson.stream.a var1) throws IOException {
            Long var5;
            if(var1.f() == io.intercom.com.google.gson.stream.b.i) {
               var1.j();
               var5 = null;
            } else {
               long var2;
               try {
                  var2 = var1.l();
               } catch (NumberFormatException var4) {
                  throw new JsonSyntaxException(var4);
               }

               var5 = Long.valueOf(var2);
            }

            return var5;
         }

         public void a(io.intercom.com.google.gson.stream.c var1, Number var2) throws IOException {
            var1.a(var2);
         }

         // $FF: synthetic method
         public Object b(io.intercom.com.google.gson.stream.a var1) throws IOException {
            return this.a(var1);
         }
      };
      u = new q() {
         public Number a(io.intercom.com.google.gson.stream.a var1) throws IOException {
            Float var2;
            if(var1.f() == io.intercom.com.google.gson.stream.b.i) {
               var1.j();
               var2 = null;
            } else {
               var2 = Float.valueOf((float)var1.k());
            }

            return var2;
         }

         public void a(io.intercom.com.google.gson.stream.c var1, Number var2) throws IOException {
            var1.a(var2);
         }

         // $FF: synthetic method
         public Object b(io.intercom.com.google.gson.stream.a var1) throws IOException {
            return this.a(var1);
         }
      };
      v = new q() {
         public Number a(io.intercom.com.google.gson.stream.a var1) throws IOException {
            Double var2;
            if(var1.f() == io.intercom.com.google.gson.stream.b.i) {
               var1.j();
               var2 = null;
            } else {
               var2 = Double.valueOf(var1.k());
            }

            return var2;
         }

         public void a(io.intercom.com.google.gson.stream.c var1, Number var2) throws IOException {
            var1.a(var2);
         }

         // $FF: synthetic method
         public Object b(io.intercom.com.google.gson.stream.a var1) throws IOException {
            return this.a(var1);
         }
      };
      w = new q() {
         public Number a(io.intercom.com.google.gson.stream.a var1) throws IOException {
            io.intercom.com.google.gson.stream.b var2 = var1.f();
            io.intercom.com.google.gson.b.f var3;
            switch(null.a[var2.ordinal()]) {
            case 1:
            case 3:
               var3 = new io.intercom.com.google.gson.b.f(var1.h());
               break;
            case 2:
            default:
               throw new JsonSyntaxException("Expecting number, got: " + var2);
            case 4:
               var1.j();
               var3 = null;
            }

            return var3;
         }

         public void a(io.intercom.com.google.gson.stream.c var1, Number var2) throws IOException {
            var1.a(var2);
         }

         // $FF: synthetic method
         public Object b(io.intercom.com.google.gson.stream.a var1) throws IOException {
            return this.a(var1);
         }
      };
      x = a(Number.class, w);
      y = new q() {
         public Character a(io.intercom.com.google.gson.stream.a var1) throws IOException {
            Character var2;
            if(var1.f() == io.intercom.com.google.gson.stream.b.i) {
               var1.j();
               var2 = null;
            } else {
               String var3 = var1.h();
               if(var3.length() != 1) {
                  throw new JsonSyntaxException("Expecting character, got: " + var3);
               }

               var2 = Character.valueOf(var3.charAt(0));
            }

            return var2;
         }

         public void a(io.intercom.com.google.gson.stream.c var1, Character var2) throws IOException {
            String var3;
            if(var2 == null) {
               var3 = null;
            } else {
               var3 = String.valueOf(var2);
            }

            var1.b(var3);
         }

         // $FF: synthetic method
         public Object b(io.intercom.com.google.gson.stream.a var1) throws IOException {
            return this.a(var1);
         }
      };
      z = a(Character.TYPE, Character.class, y);
      A = new q() {
         public String a(io.intercom.com.google.gson.stream.a var1) throws IOException {
            io.intercom.com.google.gson.stream.b var2 = var1.f();
            String var3;
            if(var2 == io.intercom.com.google.gson.stream.b.i) {
               var1.j();
               var3 = null;
            } else if(var2 == io.intercom.com.google.gson.stream.b.h) {
               var3 = Boolean.toString(var1.i());
            } else {
               var3 = var1.h();
            }

            return var3;
         }

         public void a(io.intercom.com.google.gson.stream.c var1, String var2) throws IOException {
            var1.b(var2);
         }

         // $FF: synthetic method
         public Object b(io.intercom.com.google.gson.stream.a var1) throws IOException {
            return this.a(var1);
         }
      };
      B = new q() {
         public BigDecimal a(io.intercom.com.google.gson.stream.a var1) throws IOException {
            BigDecimal var3;
            if(var1.f() == io.intercom.com.google.gson.stream.b.i) {
               var1.j();
               var3 = null;
            } else {
               try {
                  var3 = new BigDecimal(var1.h());
               } catch (NumberFormatException var2) {
                  throw new JsonSyntaxException(var2);
               }
            }

            return var3;
         }

         public void a(io.intercom.com.google.gson.stream.c var1, BigDecimal var2) throws IOException {
            var1.a((Number)var2);
         }

         // $FF: synthetic method
         public Object b(io.intercom.com.google.gson.stream.a var1) throws IOException {
            return this.a(var1);
         }
      };
      C = new q() {
         public BigInteger a(io.intercom.com.google.gson.stream.a var1) throws IOException {
            BigInteger var3;
            if(var1.f() == io.intercom.com.google.gson.stream.b.i) {
               var1.j();
               var3 = null;
            } else {
               try {
                  var3 = new BigInteger(var1.h());
               } catch (NumberFormatException var2) {
                  throw new JsonSyntaxException(var2);
               }
            }

            return var3;
         }

         public void a(io.intercom.com.google.gson.stream.c var1, BigInteger var2) throws IOException {
            var1.a((Number)var2);
         }

         // $FF: synthetic method
         public Object b(io.intercom.com.google.gson.stream.a var1) throws IOException {
            return this.a(var1);
         }
      };
      D = a(String.class, A);
      E = new q() {
         public StringBuilder a(io.intercom.com.google.gson.stream.a var1) throws IOException {
            StringBuilder var2;
            if(var1.f() == io.intercom.com.google.gson.stream.b.i) {
               var1.j();
               var2 = null;
            } else {
               var2 = new StringBuilder(var1.h());
            }

            return var2;
         }

         public void a(io.intercom.com.google.gson.stream.c var1, StringBuilder var2) throws IOException {
            String var3;
            if(var2 == null) {
               var3 = null;
            } else {
               var3 = var2.toString();
            }

            var1.b(var3);
         }

         // $FF: synthetic method
         public Object b(io.intercom.com.google.gson.stream.a var1) throws IOException {
            return this.a(var1);
         }
      };
      F = a(StringBuilder.class, E);
      G = new q() {
         public StringBuffer a(io.intercom.com.google.gson.stream.a var1) throws IOException {
            StringBuffer var2;
            if(var1.f() == io.intercom.com.google.gson.stream.b.i) {
               var1.j();
               var2 = null;
            } else {
               var2 = new StringBuffer(var1.h());
            }

            return var2;
         }

         public void a(io.intercom.com.google.gson.stream.c var1, StringBuffer var2) throws IOException {
            String var3;
            if(var2 == null) {
               var3 = null;
            } else {
               var3 = var2.toString();
            }

            var1.b(var3);
         }

         // $FF: synthetic method
         public Object b(io.intercom.com.google.gson.stream.a var1) throws IOException {
            return this.a(var1);
         }
      };
      H = a(StringBuffer.class, G);
      I = new q() {
         public URL a(io.intercom.com.google.gson.stream.a var1) throws IOException {
            Object var2 = null;
            URL var4;
            if(var1.f() == io.intercom.com.google.gson.stream.b.i) {
               var1.j();
               var4 = (URL)var2;
            } else {
               String var3 = var1.h();
               var4 = (URL)var2;
               if(!"null".equals(var3)) {
                  var4 = new URL(var3);
               }
            }

            return var4;
         }

         public void a(io.intercom.com.google.gson.stream.c var1, URL var2) throws IOException {
            String var3;
            if(var2 == null) {
               var3 = null;
            } else {
               var3 = var2.toExternalForm();
            }

            var1.b(var3);
         }

         // $FF: synthetic method
         public Object b(io.intercom.com.google.gson.stream.a var1) throws IOException {
            return this.a(var1);
         }
      };
      J = a(URL.class, I);
      K = new q() {
         public URI a(io.intercom.com.google.gson.stream.a param1) throws IOException {
            // $FF: Couldn't be decompiled
         }

         public void a(io.intercom.com.google.gson.stream.c var1, URI var2) throws IOException {
            String var3;
            if(var2 == null) {
               var3 = null;
            } else {
               var3 = var2.toASCIIString();
            }

            var1.b(var3);
         }

         // $FF: synthetic method
         public Object b(io.intercom.com.google.gson.stream.a var1) throws IOException {
            return this.a(var1);
         }
      };
      L = a(URI.class, K);
      M = new q() {
         public InetAddress a(io.intercom.com.google.gson.stream.a var1) throws IOException {
            InetAddress var2;
            if(var1.f() == io.intercom.com.google.gson.stream.b.i) {
               var1.j();
               var2 = null;
            } else {
               var2 = InetAddress.getByName(var1.h());
            }

            return var2;
         }

         public void a(io.intercom.com.google.gson.stream.c var1, InetAddress var2) throws IOException {
            String var3;
            if(var2 == null) {
               var3 = null;
            } else {
               var3 = var2.getHostAddress();
            }

            var1.b(var3);
         }

         // $FF: synthetic method
         public Object b(io.intercom.com.google.gson.stream.a var1) throws IOException {
            return this.a(var1);
         }
      };
      N = b(InetAddress.class, M);
      O = new q() {
         public UUID a(io.intercom.com.google.gson.stream.a var1) throws IOException {
            UUID var2;
            if(var1.f() == io.intercom.com.google.gson.stream.b.i) {
               var1.j();
               var2 = null;
            } else {
               var2 = UUID.fromString(var1.h());
            }

            return var2;
         }

         public void a(io.intercom.com.google.gson.stream.c var1, UUID var2) throws IOException {
            String var3;
            if(var2 == null) {
               var3 = null;
            } else {
               var3 = var2.toString();
            }

            var1.b(var3);
         }

         // $FF: synthetic method
         public Object b(io.intercom.com.google.gson.stream.a var1) throws IOException {
            return this.a(var1);
         }
      };
      P = a(UUID.class, O);
      Q = (new q() {
         public Currency a(io.intercom.com.google.gson.stream.a var1) throws IOException {
            return Currency.getInstance(var1.h());
         }

         public void a(io.intercom.com.google.gson.stream.c var1, Currency var2) throws IOException {
            var1.b(var2.getCurrencyCode());
         }

         // $FF: synthetic method
         public Object b(io.intercom.com.google.gson.stream.a var1) throws IOException {
            return this.a(var1);
         }
      }).a();
      R = a(Currency.class, Q);
      S = new r() {
         public q a(io.intercom.com.google.gson.e var1, io.intercom.com.google.gson.c.a var2) {
            q var3;
            if(var2.a() != Timestamp.class) {
               var3 = null;
            } else {
               var3 = new q(var1.a(Date.class)) {
                  // $FF: synthetic field
                  final q a;

                  {
                     this.a = var2;
                  }

                  public Timestamp a(io.intercom.com.google.gson.stream.a var1) throws IOException {
                     Date var2 = (Date)this.a.b(var1);
                     Timestamp var3;
                     if(var2 != null) {
                        var3 = new Timestamp(var2.getTime());
                     } else {
                        var3 = null;
                     }

                     return var3;
                  }

                  public void a(io.intercom.com.google.gson.stream.c var1, Timestamp var2) throws IOException {
                     this.a.a(var1, var2);
                  }

                  // $FF: synthetic method
                  public Object b(io.intercom.com.google.gson.stream.a var1) throws IOException {
                     return this.a(var1);
                  }
               };
            }

            return var3;
         }
      };
      T = new q() {
         public Calendar a(io.intercom.com.google.gson.stream.a var1) throws IOException {
            int var3 = 0;
            GregorianCalendar var10;
            if(var1.f() == io.intercom.com.google.gson.stream.b.i) {
               var1.j();
               var10 = null;
            } else {
               var1.c();
               int var4 = 0;
               int var5 = 0;
               int var6 = 0;
               int var7 = 0;
               int var8 = 0;

               while(var1.f() != io.intercom.com.google.gson.stream.b.d) {
                  String var9 = var1.g();
                  int var2 = var1.m();
                  if("year".equals(var9)) {
                     var8 = var2;
                  } else if("month".equals(var9)) {
                     var7 = var2;
                  } else if("dayOfMonth".equals(var9)) {
                     var6 = var2;
                  } else if("hourOfDay".equals(var9)) {
                     var5 = var2;
                  } else if("minute".equals(var9)) {
                     var4 = var2;
                  } else if("second".equals(var9)) {
                     var3 = var2;
                  }
               }

               var1.d();
               var10 = new GregorianCalendar(var8, var7, var6, var5, var4, var3);
            }

            return var10;
         }

         public void a(io.intercom.com.google.gson.stream.c var1, Calendar var2) throws IOException {
            if(var2 == null) {
               var1.f();
            } else {
               var1.d();
               var1.a("year");
               var1.a((long)var2.get(1));
               var1.a("month");
               var1.a((long)var2.get(2));
               var1.a("dayOfMonth");
               var1.a((long)var2.get(5));
               var1.a("hourOfDay");
               var1.a((long)var2.get(11));
               var1.a("minute");
               var1.a((long)var2.get(12));
               var1.a("second");
               var1.a((long)var2.get(13));
               var1.e();
            }

         }

         // $FF: synthetic method
         public Object b(io.intercom.com.google.gson.stream.a var1) throws IOException {
            return this.a(var1);
         }
      };
      U = b(Calendar.class, GregorianCalendar.class, T);
      V = new q() {
         public Locale a(io.intercom.com.google.gson.stream.a var1) throws IOException {
            String var2 = null;
            Locale var4;
            if(var1.f() == io.intercom.com.google.gson.stream.b.i) {
               var1.j();
               var4 = var2;
            } else {
               StringTokenizer var3 = new StringTokenizer(var1.h(), "_");
               String var5;
               if(var3.hasMoreElements()) {
                  var5 = var3.nextToken();
               } else {
                  var5 = null;
               }

               if(var3.hasMoreElements()) {
                  var2 = var3.nextToken();
               } else {
                  var2 = null;
               }

               String var6;
               if(var3.hasMoreElements()) {
                  var6 = var3.nextToken();
               } else {
                  var6 = null;
               }

               if(var2 == null && var6 == null) {
                  var4 = new Locale(var5);
               } else if(var6 == null) {
                  var4 = new Locale(var5, var2);
               } else {
                  var4 = new Locale(var5, var2, var6);
               }
            }

            return var4;
         }

         public void a(io.intercom.com.google.gson.stream.c var1, Locale var2) throws IOException {
            String var3;
            if(var2 == null) {
               var3 = null;
            } else {
               var3 = var2.toString();
            }

            var1.b(var3);
         }

         // $FF: synthetic method
         public Object b(io.intercom.com.google.gson.stream.a var1) throws IOException {
            return this.a(var1);
         }
      };
      W = a(Locale.class, V);
      X = new q() {
         public io.intercom.com.google.gson.j a(io.intercom.com.google.gson.stream.a var1) throws IOException {
            Object var3;
            switch(null.a[var1.f().ordinal()]) {
            case 1:
               var3 = new io.intercom.com.google.gson.m(new io.intercom.com.google.gson.b.f(var1.h()));
               break;
            case 2:
               var3 = new io.intercom.com.google.gson.m(Boolean.valueOf(var1.i()));
               break;
            case 3:
               var3 = new io.intercom.com.google.gson.m(var1.h());
               break;
            case 4:
               var1.j();
               var3 = io.intercom.com.google.gson.k.a;
               break;
            case 5:
               io.intercom.com.google.gson.g var4 = new io.intercom.com.google.gson.g();
               var1.a();

               while(var1.e()) {
                  var4.a(this.a(var1));
               }

               var1.b();
               var3 = var4;
               break;
            case 6:
               io.intercom.com.google.gson.l var2 = new io.intercom.com.google.gson.l();
               var1.c();

               while(var1.e()) {
                  var2.a(var1.g(), this.a(var1));
               }

               var1.d();
               var3 = var2;
               break;
            default:
               throw new IllegalArgumentException();
            }

            return (io.intercom.com.google.gson.j)var3;
         }

         public void a(io.intercom.com.google.gson.stream.c var1, io.intercom.com.google.gson.j var2) throws IOException {
            if(var2 != null && !var2.j()) {
               if(var2.i()) {
                  io.intercom.com.google.gson.m var4 = var2.m();
                  if(var4.p()) {
                     var1.a(var4.a());
                  } else if(var4.o()) {
                     var1.a(var4.f());
                  } else {
                     var1.b(var4.b());
                  }
               } else {
                  Iterator var5;
                  if(var2.g()) {
                     var1.b();
                     var5 = var2.l().iterator();

                     while(var5.hasNext()) {
                        this.a(var1, (io.intercom.com.google.gson.j)var5.next());
                     }

                     var1.c();
                  } else {
                     if(!var2.h()) {
                        throw new IllegalArgumentException("Couldn't write " + var2.getClass());
                     }

                     var1.d();
                     var5 = var2.k().o().iterator();

                     while(var5.hasNext()) {
                        Entry var3 = (Entry)var5.next();
                        var1.a((String)var3.getKey());
                        this.a(var1, (io.intercom.com.google.gson.j)var3.getValue());
                     }

                     var1.e();
                  }
               }
            } else {
               var1.f();
            }

         }

         // $FF: synthetic method
         public Object b(io.intercom.com.google.gson.stream.a var1) throws IOException {
            return this.a(var1);
         }
      };
      Y = b(io.intercom.com.google.gson.j.class, X);
      Z = new r() {
         public q a(io.intercom.com.google.gson.e var1, io.intercom.com.google.gson.c.a var2) {
            Class var5 = var2.a();
            n.a var3;
            if(Enum.class.isAssignableFrom(var5) && var5 != Enum.class) {
               Class var4 = var5;
               if(!var5.isEnum()) {
                  var4 = var5.getSuperclass();
               }

               var3 = new n.a(var4);
            } else {
               var3 = null;
            }

            return var3;
         }
      };
   }

   public static r a(final Class var0, final q var1) {
      return new r() {
         public q a(io.intercom.com.google.gson.e var1x, io.intercom.com.google.gson.c.a var2) {
            q var3;
            if(var2.a() == var0) {
               var3 = var1;
            } else {
               var3 = null;
            }

            return var3;
         }

         public String toString() {
            return "Factory[type=" + var0.getName() + ",adapter=" + var1 + "]";
         }
      };
   }

   public static r a(final Class var0, final Class var1, final q var2) {
      return new r() {
         public q a(io.intercom.com.google.gson.e var1x, io.intercom.com.google.gson.c.a var2x) {
            Class var3 = var2x.a();
            q var4;
            if(var3 != var0 && var3 != var1) {
               var4 = null;
            } else {
               var4 = var2;
            }

            return var4;
         }

         public String toString() {
            return "Factory[type=" + var1.getName() + "+" + var0.getName() + ",adapter=" + var2 + "]";
         }
      };
   }

   public static r b(final Class var0, final q var1) {
      return new r() {
         public q a(io.intercom.com.google.gson.e var1x, io.intercom.com.google.gson.c.a var2) {
            final Class var3 = var2.a();
            q var4;
            if(!var0.isAssignableFrom(var3)) {
               var4 = null;
            } else {
               var4 = new q() {
                  public void a(io.intercom.com.google.gson.stream.c var1x, Object var2) throws IOException {
                     var1.a(var1x, var2);
                  }

                  public Object b(io.intercom.com.google.gson.stream.a var1x) throws IOException {
                     Object var2 = var1.b(var1x);
                     if(var2 != null && !var3.isInstance(var2)) {
                        throw new JsonSyntaxException("Expected a " + var3.getName() + " but was " + var2.getClass().getName());
                     } else {
                        return var2;
                     }
                  }
               };
            }

            return var4;
         }

         public String toString() {
            return "Factory[typeHierarchy=" + var0.getName() + ",adapter=" + var1 + "]";
         }
      };
   }

   public static r b(final Class var0, final Class var1, final q var2) {
      return new r() {
         public q a(io.intercom.com.google.gson.e var1x, io.intercom.com.google.gson.c.a var2x) {
            Class var3 = var2x.a();
            q var4;
            if(var3 != var0 && var3 != var1) {
               var4 = null;
            } else {
               var4 = var2;
            }

            return var4;
         }

         public String toString() {
            return "Factory[type=" + var0.getName() + "+" + var1.getName() + ",adapter=" + var2 + "]";
         }
      };
   }

   private static final class a extends q {
      private final Map a;
      private final Map b;

      public a(Class param1) {
         // $FF: Couldn't be decompiled
      }

      public Enum a(io.intercom.com.google.gson.stream.a var1) throws IOException {
         Enum var2;
         if(var1.f() == io.intercom.com.google.gson.stream.b.i) {
            var1.j();
            var2 = null;
         } else {
            var2 = (Enum)this.a.get(var1.h());
         }

         return var2;
      }

      public void a(io.intercom.com.google.gson.stream.c var1, Enum var2) throws IOException {
         String var3;
         if(var2 == null) {
            var3 = null;
         } else {
            var3 = (String)this.b.get(var2);
         }

         var1.b(var3);
      }

      // $FF: synthetic method
      public Object b(io.intercom.com.google.gson.stream.a var1) throws IOException {
         return this.a(var1);
      }
   }
}
