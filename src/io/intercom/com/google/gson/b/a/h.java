package io.intercom.com.google.gson.b.a;

import io.intercom.com.google.gson.q;
import io.intercom.com.google.gson.r;
import java.io.IOException;
import java.util.ArrayList;

public final class h extends q {
   public static final r a = new r() {
      public q a(io.intercom.com.google.gson.e var1, io.intercom.com.google.gson.c.a var2) {
         h var3;
         if(var2.a() == Object.class) {
            var3 = new h(var1);
         } else {
            var3 = null;
         }

         return var3;
      }
   };
   private final io.intercom.com.google.gson.e b;

   h(io.intercom.com.google.gson.e var1) {
      this.b = var1;
   }

   public void a(io.intercom.com.google.gson.stream.c var1, Object var2) throws IOException {
      if(var2 == null) {
         var1.f();
      } else {
         q var3 = this.b.a(var2.getClass());
         if(var3 instanceof h) {
            var1.d();
            var1.e();
         } else {
            var3.a(var1, var2);
         }
      }

   }

   public Object b(io.intercom.com.google.gson.stream.a var1) throws IOException {
      io.intercom.com.google.gson.stream.b var2 = var1.f();
      Object var3;
      switch(null.a[var2.ordinal()]) {
      case 1:
         ArrayList var5 = new ArrayList();
         var1.a();

         while(var1.e()) {
            var5.add(this.b(var1));
         }

         var1.b();
         var3 = var5;
         break;
      case 2:
         io.intercom.com.google.gson.b.g var4 = new io.intercom.com.google.gson.b.g();
         var1.c();

         while(var1.e()) {
            var4.put(var1.g(), this.b(var1));
         }

         var1.d();
         var3 = var4;
         break;
      case 3:
         var3 = var1.h();
         break;
      case 4:
         var3 = Double.valueOf(var1.k());
         break;
      case 5:
         var3 = Boolean.valueOf(var1.i());
         break;
      case 6:
         var1.j();
         var3 = null;
         break;
      default:
         throw new IllegalStateException();
      }

      return var3;
   }
}
