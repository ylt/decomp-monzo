package io.intercom.com.google.gson.b.a;

import io.intercom.com.google.gson.q;
import java.io.IOException;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;

final class m extends q {
   private final io.intercom.com.google.gson.e a;
   private final q b;
   private final Type c;

   m(io.intercom.com.google.gson.e var1, q var2, Type var3) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
   }

   private Type a(Type var1, Object var2) {
      Object var3 = var1;
      if(var2 != null) {
         if(var1 != Object.class && !(var1 instanceof TypeVariable)) {
            var3 = var1;
            if(!(var1 instanceof Class)) {
               return (Type)var3;
            }
         }

         var3 = var2.getClass();
      }

      return (Type)var3;
   }

   public void a(io.intercom.com.google.gson.stream.c var1, Object var2) throws IOException {
      q var3 = this.b;
      Type var4 = this.a(this.c, var2);
      if(var4 != this.c) {
         var3 = this.a.a(io.intercom.com.google.gson.c.a.a(var4));
         if(var3 instanceof i.a && !(this.b instanceof i.a)) {
            var3 = this.b;
         }
      }

      var3.a(var1, var2);
   }

   public Object b(io.intercom.com.google.gson.stream.a var1) throws IOException {
      return this.b.b(var1);
   }
}
