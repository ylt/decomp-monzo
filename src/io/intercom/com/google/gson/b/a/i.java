package io.intercom.com.google.gson.b.a;

import io.intercom.com.google.gson.q;
import io.intercom.com.google.gson.r;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public final class i implements r {
   private final io.intercom.com.google.gson.b.c a;
   private final io.intercom.com.google.gson.d b;
   private final io.intercom.com.google.gson.b.d c;
   private final d d;

   public i(io.intercom.com.google.gson.b.c var1, io.intercom.com.google.gson.d var2, io.intercom.com.google.gson.b.d var3, d var4) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
      this.d = var4;
   }

   private i.b a(final io.intercom.com.google.gson.e var1, final Field var2, String var3, final io.intercom.com.google.gson.c.a var4, boolean var5, boolean var6) {
      final boolean var8 = io.intercom.com.google.gson.b.i.a((Type)var4.a());
      io.intercom.com.google.gson.a.b var10 = (io.intercom.com.google.gson.a.b)var2.getAnnotation(io.intercom.com.google.gson.a.b.class);
      q var9 = null;
      if(var10 != null) {
         var9 = this.d.a(this.a, var1, var4, var10);
      }

      final boolean var7;
      if(var9 != null) {
         var7 = true;
      } else {
         var7 = false;
      }

      final q var11 = var9;
      if(var9 == null) {
         var11 = var1.a(var4);
      }

      return new i.b(var3, var5, var6) {
         void a(io.intercom.com.google.gson.stream.a var1x, Object var2x) throws IOException, IllegalAccessException {
            Object var3 = var11.b(var1x);
            if(var3 != null || !var8) {
               var2.set(var2x, var3);
            }

         }

         void a(io.intercom.com.google.gson.stream.c var1x, Object var2x) throws IOException, IllegalAccessException {
            Object var3 = var2.get(var2x);
            if(var7) {
               var2x = var11;
            } else {
               var2x = new m(var1, var11, var4.b());
            }

            ((q)var2x).a(var1x, var3);
         }

         public boolean a(Object var1x) throws IOException, IllegalAccessException {
            boolean var2x = false;
            if(this.i && var2.get(var1x) != var1x) {
               var2x = true;
            }

            return var2x;
         }
      };
   }

   private List a(Field var1) {
      io.intercom.com.google.gson.a.c var4 = (io.intercom.com.google.gson.a.c)var1.getAnnotation(io.intercom.com.google.gson.a.c.class);
      Object var6;
      if(var4 == null) {
         var6 = Collections.singletonList(this.b.a(var1));
      } else {
         String var7 = var4.a();
         String[] var5 = var4.b();
         if(var5.length == 0) {
            var6 = Collections.singletonList(var7);
         } else {
            ArrayList var8 = new ArrayList(var5.length + 1);
            var8.add(var7);
            int var3 = var5.length;
            int var2 = 0;

            while(true) {
               var6 = var8;
               if(var2 >= var3) {
                  break;
               }

               var8.add(var5[var2]);
               ++var2;
            }
         }
      }

      return (List)var6;
   }

   private Map a(io.intercom.com.google.gson.e var1, io.intercom.com.google.gson.c.a var2, Class var3) {
      LinkedHashMap var12 = new LinkedHashMap();
      if(!var3.isInterface()) {
         Type var13 = var2.b();
         Class var10 = var3;

         for(io.intercom.com.google.gson.c.a var19 = var2; var10 != Object.class; var10 = var19.a()) {
            Field[] var15 = var10.getDeclaredFields();
            int var6 = var15.length;

            for(int var4 = 0; var4 < var6; ++var4) {
               Field var16 = var15[var4];
               boolean var8 = this.a(var16, true);
               boolean var9 = this.a(var16, false);
               if(var8 || var9) {
                  var16.setAccessible(true);
                  Type var14 = io.intercom.com.google.gson.b.b.a(var19.b(), var10, var16.getGenericType());
                  List var17 = this.a(var16);
                  i.b var18 = null;
                  int var7 = var17.size();

                  for(int var5 = 0; var5 < var7; ++var5) {
                     String var11 = (String)var17.get(var5);
                     if(var5 != 0) {
                        var8 = false;
                     }

                     i.b var20 = (i.b)var12.put(var11, this.a(var1, var16, var11, io.intercom.com.google.gson.c.a.a(var14), var8, var9));
                     if(var18 == null) {
                        var18 = var20;
                     }
                  }

                  if(var18 != null) {
                     throw new IllegalArgumentException(var13 + " declares multiple JSON fields named " + var18.h);
                  }
               }
            }

            var19 = io.intercom.com.google.gson.c.a.a(io.intercom.com.google.gson.b.b.a(var19.b(), var10, var10.getGenericSuperclass()));
         }
      }

      return var12;
   }

   static boolean a(Field var0, boolean var1, io.intercom.com.google.gson.b.d var2) {
      if(!var2.a(var0.getType(), var1) && !var2.a(var0, var1)) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public q a(io.intercom.com.google.gson.e var1, io.intercom.com.google.gson.c.a var2) {
      Class var3 = var2.a();
      i.a var4;
      if(!Object.class.isAssignableFrom(var3)) {
         var4 = null;
      } else {
         var4 = new i.a(this.a.a(var2), this.a(var1, var2, var3));
      }

      return var4;
   }

   public boolean a(Field var1, boolean var2) {
      return a(var1, var2, this.c);
   }

   public static final class a extends q {
      private final io.intercom.com.google.gson.b.h a;
      private final Map b;

      a(io.intercom.com.google.gson.b.h var1, Map var2) {
         this.a = var1;
         this.b = var2;
      }

      public void a(io.intercom.com.google.gson.stream.c param1, Object param2) throws IOException {
         // $FF: Couldn't be decompiled
      }

      public Object b(io.intercom.com.google.gson.stream.a param1) throws IOException {
         // $FF: Couldn't be decompiled
      }
   }

   abstract static class b {
      final String h;
      final boolean i;
      final boolean j;

      protected b(String var1, boolean var2, boolean var3) {
         this.h = var1;
         this.i = var2;
         this.j = var3;
      }

      abstract void a(io.intercom.com.google.gson.stream.a var1, Object var2) throws IOException, IllegalAccessException;

      abstract void a(io.intercom.com.google.gson.stream.c var1, Object var2) throws IOException, IllegalAccessException;

      abstract boolean a(Object var1) throws IOException, IllegalAccessException;
   }
}
