package io.intercom.com.google.gson.b.a;

import io.intercom.com.google.gson.o;
import io.intercom.com.google.gson.q;
import io.intercom.com.google.gson.r;
import java.io.IOException;

public final class l extends q {
   final io.intercom.com.google.gson.e a;
   private final o b;
   private final io.intercom.com.google.gson.i c;
   private final io.intercom.com.google.gson.c.a d;
   private final r e;
   private final l.a f = new l.a();
   private q g;

   public l(o var1, io.intercom.com.google.gson.i var2, io.intercom.com.google.gson.e var3, io.intercom.com.google.gson.c.a var4, r var5) {
      this.b = var1;
      this.c = var2;
      this.a = var3;
      this.d = var4;
      this.e = var5;
   }

   private q b() {
      q var1 = this.g;
      if(var1 == null) {
         var1 = this.a.a(this.e, this.d);
         this.g = var1;
      }

      return var1;
   }

   public void a(io.intercom.com.google.gson.stream.c var1, Object var2) throws IOException {
      if(this.b == null) {
         this.b().a(var1, var2);
      } else if(var2 == null) {
         var1.f();
      } else {
         io.intercom.com.google.gson.b.j.a(this.b.a(var2, this.d.b(), this.f), var1);
      }

   }

   public Object b(io.intercom.com.google.gson.stream.a var1) throws IOException {
      Object var2;
      if(this.c == null) {
         var2 = this.b().b(var1);
      } else {
         io.intercom.com.google.gson.j var3 = io.intercom.com.google.gson.b.j.a(var1);
         if(var3.j()) {
            var2 = null;
         } else {
            var2 = this.c.a(var3, this.d.b(), this.f);
         }
      }

      return var2;
   }

   private final class a implements io.intercom.com.google.gson.h, io.intercom.com.google.gson.n {
      private a() {
      }

      // $FF: synthetic method
      a(Object var2) {
         this();
      }
   }
}
