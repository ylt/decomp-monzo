package io.intercom.com.google.gson.b.a;

import io.intercom.com.google.gson.o;
import io.intercom.com.google.gson.q;
import io.intercom.com.google.gson.r;

public final class d implements r {
   private final io.intercom.com.google.gson.b.c a;

   public d(io.intercom.com.google.gson.b.c var1) {
      this.a = var1;
   }

   q a(io.intercom.com.google.gson.b.c var1, io.intercom.com.google.gson.e var2, io.intercom.com.google.gson.c.a var3, io.intercom.com.google.gson.a.b var4) {
      Object var5 = var1.a(io.intercom.com.google.gson.c.a.b(var4.a())).a();
      Object var6;
      if(var5 instanceof q) {
         var6 = (q)var5;
      } else if(var5 instanceof r) {
         var6 = ((r)var5).a(var2, var3);
      } else {
         if(!(var5 instanceof o) && !(var5 instanceof io.intercom.com.google.gson.i)) {
            throw new IllegalArgumentException("Invalid attempt to bind an instance of " + var5.getClass().getName() + " as a @JsonAdapter for " + var3.toString() + ". @JsonAdapter value must be a TypeAdapter, TypeAdapterFactory," + " JsonSerializer or JsonDeserializer.");
         }

         o var7;
         if(var5 instanceof o) {
            var7 = (o)var5;
         } else {
            var7 = null;
         }

         io.intercom.com.google.gson.i var9;
         if(var5 instanceof io.intercom.com.google.gson.i) {
            var9 = (io.intercom.com.google.gson.i)var5;
         } else {
            var9 = null;
         }

         var6 = new l(var7, var9, var2, var3, (r)null);
      }

      Object var8 = var6;
      if(var6 != null) {
         var8 = var6;
         if(var4.b()) {
            var8 = ((q)var6).a();
         }
      }

      return (q)var8;
   }

   public q a(io.intercom.com.google.gson.e var1, io.intercom.com.google.gson.c.a var2) {
      io.intercom.com.google.gson.a.b var3 = (io.intercom.com.google.gson.a.b)var2.a().getAnnotation(io.intercom.com.google.gson.a.b.class);
      q var4;
      if(var3 == null) {
         var4 = null;
      } else {
         var4 = this.a(this.a, var1, var2, var3);
      }

      return var4;
   }
}
