package io.intercom.com.google.gson.b;

import java.io.ObjectStreamException;
import java.io.Serializable;
import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.Map.Entry;

public final class g extends AbstractMap implements Serializable {
   // $FF: synthetic field
   static final boolean f;
   private static final Comparator g;
   Comparator a;
   g.d b;
   int c;
   int d;
   final g.d e;
   private g.a h;
   private g.b i;

   static {
      boolean var0;
      if(!g.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      f = var0;
      g = new Comparator() {
         public int a(Comparable var1, Comparable var2) {
            return var1.compareTo(var2);
         }

         // $FF: synthetic method
         public int compare(Object var1, Object var2) {
            return this.a((Comparable)var1, (Comparable)var2);
         }
      };
   }

   public g() {
      this(g);
   }

   public g(Comparator var1) {
      this.c = 0;
      this.d = 0;
      this.e = new g.d();
      if(var1 == null) {
         var1 = g;
      }

      this.a = var1;
   }

   private void a(g.d var1) {
      byte var4 = 0;
      g.d var6 = var1.b;
      g.d var7 = var1.c;
      g.d var5 = var7.b;
      g.d var8 = var7.c;
      var1.c = var5;
      if(var5 != null) {
         var5.a = var1;
      }

      this.a(var1, var7);
      var7.b = var1;
      var1.a = var7;
      int var2;
      if(var6 != null) {
         var2 = var6.h;
      } else {
         var2 = 0;
      }

      int var3;
      if(var5 != null) {
         var3 = var5.h;
      } else {
         var3 = 0;
      }

      var1.h = Math.max(var2, var3) + 1;
      var3 = var1.h;
      var2 = var4;
      if(var8 != null) {
         var2 = var8.h;
      }

      var7.h = Math.max(var3, var2) + 1;
   }

   private void a(g.d var1, g.d var2) {
      g.d var3 = var1.a;
      var1.a = null;
      if(var2 != null) {
         var2.a = var3;
      }

      if(var3 != null) {
         if(var3.b == var1) {
            var3.b = var2;
         } else {
            if(!f && var3.c != var1) {
               throw new AssertionError();
            }

            var3.c = var2;
         }
      } else {
         this.b = var2;
      }

   }

   private boolean a(Object var1, Object var2) {
      boolean var3;
      if(var1 != var2 && (var1 == null || !var1.equals(var2))) {
         var3 = false;
      } else {
         var3 = true;
      }

      return var3;
   }

   private void b(g.d var1) {
      byte var4 = 0;
      g.d var8 = var1.b;
      g.d var5 = var1.c;
      g.d var6 = var8.b;
      g.d var7 = var8.c;
      var1.b = var7;
      if(var7 != null) {
         var7.a = var1;
      }

      this.a(var1, var8);
      var8.c = var1;
      var1.a = var8;
      int var2;
      if(var5 != null) {
         var2 = var5.h;
      } else {
         var2 = 0;
      }

      int var3;
      if(var7 != null) {
         var3 = var7.h;
      } else {
         var3 = 0;
      }

      var1.h = Math.max(var2, var3) + 1;
      var3 = var1.h;
      var2 = var4;
      if(var6 != null) {
         var2 = var6.h;
      }

      var8.h = Math.max(var3, var2) + 1;
   }

   private void b(g.d var1, boolean var2) {
      for(; var1 != null; var1 = var1.a) {
         g.d var6 = var1.b;
         g.d var7 = var1.c;
         int var3;
         if(var6 != null) {
            var3 = var6.h;
         } else {
            var3 = 0;
         }

         int var4;
         if(var7 != null) {
            var4 = var7.h;
         } else {
            var4 = 0;
         }

         int var5 = var3 - var4;
         g.d var8;
         if(var5 == -2) {
            var8 = var7.b;
            var6 = var7.c;
            if(var6 != null) {
               var3 = var6.h;
            } else {
               var3 = 0;
            }

            if(var8 != null) {
               var4 = var8.h;
            } else {
               var4 = 0;
            }

            var3 = var4 - var3;
            if(var3 == -1 || var3 == 0 && !var2) {
               this.a(var1);
            } else {
               if(!f && var3 != 1) {
                  throw new AssertionError();
               }

               this.b(var7);
               this.a(var1);
            }

            if(var2) {
               break;
            }
         } else if(var5 == 2) {
            var7 = var6.b;
            var8 = var6.c;
            if(var8 != null) {
               var3 = var8.h;
            } else {
               var3 = 0;
            }

            if(var7 != null) {
               var4 = var7.h;
            } else {
               var4 = 0;
            }

            var3 = var4 - var3;
            if(var3 != 1 && (var3 != 0 || var2)) {
               if(!f && var3 != -1) {
                  throw new AssertionError();
               }

               this.a(var6);
               this.b(var1);
            } else {
               this.b(var1);
            }

            if(var2) {
               break;
            }
         } else if(var5 == 0) {
            var1.h = var3 + 1;
            if(var2) {
               break;
            }
         } else {
            if(!f && var5 != -1 && var5 != 1) {
               throw new AssertionError();
            }

            var1.h = Math.max(var3, var4) + 1;
            if(!var2) {
               break;
            }
         }
      }

   }

   private Object writeReplace() throws ObjectStreamException {
      return new LinkedHashMap(this);
   }

   g.d a(Object var1) {
      Object var3 = null;
      g.d var2 = (g.d)var3;
      if(var1 != null) {
         try {
            var2 = this.a(var1, false);
         } catch (ClassCastException var4) {
            var2 = (g.d)var3;
         }
      }

      return var2;
   }

   g.d a(Object var1, boolean var2) {
      Object var7 = null;
      Comparator var8 = this.a;
      g.d var4 = this.b;
      int var3;
      g.d var5;
      if(var4 != null) {
         Comparable var6;
         if(var8 == g) {
            var6 = (Comparable)var1;
         } else {
            var6 = null;
         }

         while(true) {
            if(var6 != null) {
               var3 = var6.compareTo(var4.f);
            } else {
               var3 = var8.compare(var1, var4.f);
            }

            if(var3 == 0) {
               return var4;
            }

            if(var3 < 0) {
               var5 = var4.b;
            } else {
               var5 = var4.c;
            }

            if(var5 == null) {
               var5 = var4;
               break;
            }

            var4 = var5;
         }
      } else {
         var3 = 0;
         var5 = var4;
      }

      var4 = (g.d)var7;
      if(var2) {
         var4 = this.e;
         g.d var9;
         if(var5 == null) {
            if(var8 == g && !(var1 instanceof Comparable)) {
               throw new ClassCastException(var1.getClass().getName() + " is not Comparable");
            }

            var9 = new g.d(var5, var1, var4, var4.e);
            this.b = var9;
         } else {
            var9 = new g.d(var5, var1, var4, var4.e);
            if(var3 < 0) {
               var5.b = var9;
            } else {
               var5.c = var9;
            }

            this.b(var5, true);
         }

         ++this.c;
         ++this.d;
         var4 = var9;
      }

      return var4;
   }

   g.d a(Entry var1) {
      g.d var3 = this.a(var1.getKey());
      boolean var2;
      if(var3 != null && this.a(var3.g, var1.getValue())) {
         var2 = true;
      } else {
         var2 = false;
      }

      g.d var4;
      if(var2) {
         var4 = var3;
      } else {
         var4 = null;
      }

      return var4;
   }

   void a(g.d var1, boolean var2) {
      int var4 = 0;
      if(var2) {
         var1.e.d = var1.d;
         var1.d.e = var1.e;
      }

      g.d var5 = var1.b;
      g.d var7 = var1.c;
      g.d var6 = var1.a;
      if(var5 != null && var7 != null) {
         if(var5.h > var7.h) {
            var5 = var5.b();
         } else {
            var5 = var7.a();
         }

         this.a(var5, false);
         var6 = var1.b;
         int var3;
         if(var6 != null) {
            var3 = var6.h;
            var5.b = var6;
            var6.a = var5;
            var1.b = null;
         } else {
            var3 = 0;
         }

         var6 = var1.c;
         if(var6 != null) {
            var4 = var6.h;
            var5.c = var6;
            var6.a = var5;
            var1.c = null;
         }

         var5.h = Math.max(var3, var4) + 1;
         this.a(var1, var5);
      } else {
         if(var5 != null) {
            this.a(var1, var5);
            var1.b = null;
         } else if(var7 != null) {
            this.a(var1, var7);
            var1.c = null;
         } else {
            this.a((g.d)var1, (g.d)null);
         }

         this.b(var6, false);
         --this.c;
         ++this.d;
      }

   }

   g.d b(Object var1) {
      g.d var2 = this.a(var1);
      if(var2 != null) {
         this.a(var2, true);
      }

      return var2;
   }

   public void clear() {
      this.b = null;
      this.c = 0;
      ++this.d;
      g.d var1 = this.e;
      var1.e = var1;
      var1.d = var1;
   }

   public boolean containsKey(Object var1) {
      boolean var2;
      if(this.a(var1) != null) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   public Set entrySet() {
      g.a var1 = this.h;
      if(var1 == null) {
         var1 = new g.a();
         this.h = var1;
      }

      return var1;
   }

   public Object get(Object var1) {
      g.d var2 = this.a(var1);
      if(var2 != null) {
         var1 = var2.g;
      } else {
         var1 = null;
      }

      return var1;
   }

   public Set keySet() {
      g.b var1 = this.i;
      if(var1 == null) {
         var1 = new g.b();
         this.i = var1;
      }

      return var1;
   }

   public Object put(Object var1, Object var2) {
      if(var1 == null) {
         throw new NullPointerException("key == null");
      } else {
         g.d var4 = this.a(var1, true);
         Object var3 = var4.g;
         var4.g = var2;
         return var3;
      }
   }

   public Object remove(Object var1) {
      g.d var2 = this.b(var1);
      if(var2 != null) {
         var1 = var2.g;
      } else {
         var1 = null;
      }

      return var1;
   }

   public int size() {
      return this.c;
   }

   class a extends AbstractSet {
      public void clear() {
         g.this.clear();
      }

      public boolean contains(Object var1) {
         boolean var2;
         if(var1 instanceof Entry && g.this.a((Entry)var1) != null) {
            var2 = true;
         } else {
            var2 = false;
         }

         return var2;
      }

      public Iterator iterator() {
         return new g.c() {
            public Entry a() {
               return this.b();
            }

            // $FF: synthetic method
            public Object next() {
               return this.a();
            }
         };
      }

      public boolean remove(Object var1) {
         boolean var2 = false;
         if(var1 instanceof Entry) {
            g.d var3 = g.this.a((Entry)var1);
            if(var3 != null) {
               g.this.a(var3, true);
               var2 = true;
            }
         }

         return var2;
      }

      public int size() {
         return g.this.c;
      }
   }

   final class b extends AbstractSet {
      public void clear() {
         g.this.clear();
      }

      public boolean contains(Object var1) {
         return g.this.containsKey(var1);
      }

      public Iterator iterator() {
         return new g.c() {
            public Object next() {
               return this.b().f;
            }
         };
      }

      public boolean remove(Object var1) {
         boolean var2;
         if(g.this.b(var1) != null) {
            var2 = true;
         } else {
            var2 = false;
         }

         return var2;
      }

      public int size() {
         return g.this.c;
      }
   }

   private abstract class c implements Iterator {
      g.d b;
      g.d c;
      int d;

      c() {
         this.b = g.this.e.d;
         this.c = null;
         this.d = g.this.d;
      }

      final g.d b() {
         g.d var1 = this.b;
         if(var1 == g.this.e) {
            throw new NoSuchElementException();
         } else if(g.this.d != this.d) {
            throw new ConcurrentModificationException();
         } else {
            this.b = var1.d;
            this.c = var1;
            return var1;
         }
      }

      public final boolean hasNext() {
         boolean var1;
         if(this.b != g.this.e) {
            var1 = true;
         } else {
            var1 = false;
         }

         return var1;
      }

      public final void remove() {
         if(this.c == null) {
            throw new IllegalStateException();
         } else {
            g.this.a(this.c, true);
            this.c = null;
            this.d = g.this.d;
         }
      }
   }

   static final class d implements Entry {
      g.d a;
      g.d b;
      g.d c;
      g.d d;
      g.d e;
      final Object f;
      Object g;
      int h;

      d() {
         this.f = null;
         this.e = this;
         this.d = this;
      }

      d(g.d var1, Object var2, g.d var3, g.d var4) {
         this.a = var1;
         this.f = var2;
         this.h = 1;
         this.d = var3;
         this.e = var4;
         var4.d = this;
         var3.e = this;
      }

      public g.d a() {
         g.d var1 = this.b;

         g.d var2;
         g.d var3;
         for(var2 = this; var1 != null; var1 = var3) {
            var3 = var1.b;
            var2 = var1;
         }

         return var2;
      }

      public g.d b() {
         g.d var1 = this.c;

         g.d var2;
         g.d var3;
         for(var2 = this; var1 != null; var1 = var3) {
            var3 = var1.c;
            var2 = var1;
         }

         return var2;
      }

      public boolean equals(Object var1) {
         boolean var3 = false;
         boolean var2 = var3;
         if(var1 instanceof Entry) {
            Entry var4 = (Entry)var1;
            if(this.f == null) {
               var2 = var3;
               if(var4.getKey() != null) {
                  return var2;
               }
            } else {
               var2 = var3;
               if(!this.f.equals(var4.getKey())) {
                  return var2;
               }
            }

            if(this.g == null) {
               var2 = var3;
               if(var4.getValue() != null) {
                  return var2;
               }
            } else {
               var2 = var3;
               if(!this.g.equals(var4.getValue())) {
                  return var2;
               }
            }

            var2 = true;
         }

         return var2;
      }

      public Object getKey() {
         return this.f;
      }

      public Object getValue() {
         return this.g;
      }

      public int hashCode() {
         int var2 = 0;
         int var1;
         if(this.f == null) {
            var1 = 0;
         } else {
            var1 = this.f.hashCode();
         }

         if(this.g != null) {
            var2 = this.g.hashCode();
         }

         return var1 ^ var2;
      }

      public Object setValue(Object var1) {
         Object var2 = this.g;
         this.g = var1;
         return var2;
      }

      public String toString() {
         return this.f + "=" + this.g;
      }
   }
}
