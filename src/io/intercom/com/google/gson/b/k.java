package io.intercom.com.google.gson.b;

import java.io.ObjectInputStream;
import java.io.ObjectStreamClass;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public abstract class k {
   public static k a() {
      k var1;
      try {
         Class var8 = Class.forName("sun.misc.Unsafe");
         Field var9 = var8.getDeclaredField("theUnsafe");
         var9.setAccessible(true);
         final Object var10 = var9.get((Object)null);
         final Method var3 = var8.getMethod("allocateInstance", new Class[]{Class.class});
         var1 = new k() {
            public Object a(Class var1) throws Exception {
               b(var1);
               return var3.invoke(var10, new Object[]{var1});
            }
         };
      } catch (Exception var6) {
         final Method var2;
         try {
            Method var7 = ObjectStreamClass.class.getDeclaredMethod("getConstructorId", new Class[]{Class.class});
            var7.setAccessible(true);
            final int var0 = ((Integer)var7.invoke((Object)null, new Object[]{Object.class})).intValue();
            var2 = ObjectStreamClass.class.getDeclaredMethod("newInstance", new Class[]{Class.class, Integer.TYPE});
            var2.setAccessible(true);
            var1 = new k() {
               public Object a(Class var1) throws Exception {
                  b(var1);
                  return var2.invoke((Object)null, new Object[]{var1, Integer.valueOf(var0)});
               }
            };
         } catch (Exception var5) {
            try {
               var2 = ObjectInputStream.class.getDeclaredMethod("newInstance", new Class[]{Class.class, Class.class});
               var2.setAccessible(true);
               var1 = new k() {
                  public Object a(Class var1) throws Exception {
                     b(var1);
                     return var2.invoke((Object)null, new Object[]{var1, Object.class});
                  }
               };
            } catch (Exception var4) {
               var1 = new k() {
                  public Object a(Class var1) {
                     throw new UnsupportedOperationException("Cannot allocate " + var1);
                  }
               };
            }
         }
      }

      return var1;
   }

   static void b(Class var0) {
      int var1 = var0.getModifiers();
      if(Modifier.isInterface(var1)) {
         throw new UnsupportedOperationException("Interface can't be instantiated! Interface name: " + var0.getName());
      } else if(Modifier.isAbstract(var1)) {
         throw new UnsupportedOperationException("Abstract class can't be instantiated! Class name: " + var0.getName());
      }
   }

   public abstract Object a(Class var1) throws Exception;
}
