package io.intercom.com.google.gson.b;

import java.io.ObjectStreamException;
import java.math.BigDecimal;

public final class f extends Number {
   private final String a;

   public f(String var1) {
      this.a = var1;
   }

   private Object writeReplace() throws ObjectStreamException {
      return new BigDecimal(this.a);
   }

   public double doubleValue() {
      return Double.parseDouble(this.a);
   }

   public boolean equals(Object var1) {
      boolean var3 = false;
      boolean var2;
      if(this == var1) {
         var2 = true;
      } else {
         var2 = var3;
         if(var1 instanceof f) {
            f var4 = (f)var1;
            if(this.a != var4.a) {
               var2 = var3;
               if(!this.a.equals(var4.a)) {
                  return var2;
               }
            }

            var2 = true;
         }
      }

      return var2;
   }

   public float floatValue() {
      return Float.parseFloat(this.a);
   }

   public int hashCode() {
      return this.a.hashCode();
   }

   public int intValue() {
      int var1;
      try {
         var1 = Integer.parseInt(this.a);
      } catch (NumberFormatException var6) {
         long var2;
         try {
            var2 = Long.parseLong(this.a);
         } catch (NumberFormatException var5) {
            var1 = (new BigDecimal(this.a)).intValue();
            return var1;
         }

         var1 = (int)var2;
      }

      return var1;
   }

   public long longValue() {
      long var1;
      try {
         var1 = Long.parseLong(this.a);
      } catch (NumberFormatException var4) {
         var1 = (new BigDecimal(this.a)).longValue();
      }

      return var1;
   }

   public String toString() {
      return this.a;
   }
}
