package io.intercom.com.google.gson.b;

import io.intercom.com.google.gson.JsonParseException;
import io.intercom.com.google.gson.b.a.n;
import java.io.IOException;
import java.io.Writer;

public final class j {
   public static io.intercom.com.google.gson.j a(io.intercom.com.google.gson.stream.a param0) throws JsonParseException {
      // $FF: Couldn't be decompiled
   }

   public static Writer a(Appendable var0) {
      Object var1;
      if(var0 instanceof Writer) {
         var1 = (Writer)var0;
      } else {
         var1 = new j.a(var0);
      }

      return (Writer)var1;
   }

   public static void a(io.intercom.com.google.gson.j var0, io.intercom.com.google.gson.stream.c var1) throws IOException {
      n.X.a(var1, var0);
   }

   private static final class a extends Writer {
      private final Appendable a;
      private final j.a b = new j.a();

      a(Appendable var1) {
         this.a = var1;
      }

      public void close() {
      }

      public void flush() {
      }

      public void write(int var1) throws IOException {
         this.a.append((char)var1);
      }

      public void write(char[] var1, int var2, int var3) throws IOException {
         this.b.a = var1;
         this.a.append(this.b, var2, var2 + var3);
      }
   }

   static class a implements CharSequence {
      char[] a;

      public char charAt(int var1) {
         return this.a[var1];
      }

      public int length() {
         return this.a.length;
      }

      public CharSequence subSequence(int var1, int var2) {
         return new String(this.a, var1, var2 - var1);
      }
   }
}
