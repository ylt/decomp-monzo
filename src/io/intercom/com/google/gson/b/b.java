package io.intercom.com.google.gson.b;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.GenericDeclaration;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Properties;

public final class b {
   static final Type[] a = new Type[0];

   static int a(Object var0) {
      int var1;
      if(var0 != null) {
         var1 = var0.hashCode();
      } else {
         var1 = 0;
      }

      return var1;
   }

   private static int a(Object[] var0, Object var1) {
      int var2 = 0;

      for(int var3 = var0.length; var2 < var3; ++var2) {
         if(var1.equals(var0[var2])) {
            return var2;
         }
      }

      throw new NoSuchElementException();
   }

   private static Class a(TypeVariable var0) {
      GenericDeclaration var1 = var0.getGenericDeclaration();
      Class var2;
      if(var1 instanceof Class) {
         var2 = (Class)var1;
      } else {
         var2 = null;
      }

      return var2;
   }

   public static GenericArrayType a(Type var0) {
      return new b.a(var0);
   }

   public static ParameterizedType a(Type var0, Type var1, Type... var2) {
      return new b.b(var0, var1, var2);
   }

   public static Type a(Type var0, Class var1) {
      Type var2 = b(var0, var1, Collection.class);
      var0 = var2;
      if(var2 instanceof WildcardType) {
         var0 = ((WildcardType)var2).getUpperBounds()[0];
      }

      Object var3;
      if(var0 instanceof ParameterizedType) {
         var3 = ((ParameterizedType)var0).getActualTypeArguments()[0];
      } else {
         var3 = Object.class;
      }

      return (Type)var3;
   }

   static Type a(Type var0, Class var1, Class var2) {
      if(var2 != var1) {
         if(var2.isInterface()) {
            Class[] var5 = var1.getInterfaces();
            int var3 = 0;

            for(int var4 = var5.length; var3 < var4; ++var3) {
               if(var5[var3] == var2) {
                  var0 = var1.getGenericInterfaces()[var3];
                  return (Type)var0;
               }

               if(var2.isAssignableFrom(var5[var3])) {
                  var0 = a(var1.getGenericInterfaces()[var3], var5[var3], var2);
                  return (Type)var0;
               }
            }
         }

         if(!var1.isInterface()) {
            while(var1 != Object.class) {
               Class var6 = var1.getSuperclass();
               if(var6 == var2) {
                  var0 = var1.getGenericSuperclass();
                  return (Type)var0;
               }

               if(var2.isAssignableFrom(var6)) {
                  var0 = a(var1.getGenericSuperclass(), var6, var2);
                  return (Type)var0;
               }

               var1 = var6;
            }
         }

         var0 = var2;
      }

      return (Type)var0;
   }

   public static Type a(Type var0, Class var1, Type var2) {
      Object var7 = var2;

      Object var11;
      while(true) {
         if(!(var7 instanceof TypeVariable)) {
            if(var7 instanceof Class && ((Class)var7).isArray()) {
               var11 = (Class)var7;
               Class var18 = ((Class)var11).getComponentType();
               var0 = a(var0, (Class)var1, (Type)var18);
               if(var18 != var0) {
                  var11 = a(var0);
               }
               break;
            }

            if(var7 instanceof GenericArrayType) {
               var11 = (GenericArrayType)var7;
               Type var14 = ((GenericArrayType)var11).getGenericComponentType();
               var0 = a(var0, var1, var14);
               if(var14 != var0) {
                  var11 = a(var0);
               }
            } else if(var7 instanceof ParameterizedType) {
               ParameterizedType var8 = (ParameterizedType)var7;
               var2 = var8.getOwnerType();
               Type var9 = a(var0, var1, var2);
               boolean var3;
               if(var9 != var2) {
                  var3 = true;
               } else {
                  var3 = false;
               }

               Type[] var16 = var8.getActualTypeArguments();
               int var6 = var16.length;

               boolean var4;
               for(int var5 = 0; var5 < var6; var3 = var4) {
                  Type var10 = a(var0, var1, var16[var5]);
                  Type[] var12 = var16;
                  var4 = var3;
                  if(var10 != var16[var5]) {
                     var12 = var16;
                     var4 = var3;
                     if(!var3) {
                        var12 = (Type[])var16.clone();
                        var4 = true;
                     }

                     var12[var5] = var10;
                  }

                  ++var5;
                  var16 = var12;
               }

               var11 = var8;
               if(var3) {
                  var11 = a(var9, var8.getRawType(), var16);
               }
            } else {
               var11 = var7;
               if(var7 instanceof WildcardType) {
                  WildcardType var17 = (WildcardType)var7;
                  Type[] var19 = var17.getLowerBounds();
                  Type[] var15 = var17.getUpperBounds();
                  if(var19.length == 1) {
                     var0 = a(var0, var1, var19[0]);
                     var11 = var17;
                     if(var0 != var19[0]) {
                        var11 = c(var0);
                     }
                  } else {
                     var11 = var17;
                     if(var15.length == 1) {
                        var0 = a(var0, var1, var15[0]);
                        var11 = var17;
                        if(var0 != var15[0]) {
                           var11 = b(var0);
                        }
                     }
                  }
               }
            }
            break;
         }

         TypeVariable var13 = (TypeVariable)var7;
         var11 = a(var0, var1, var13);
         if(var11 == var13) {
            break;
         }

         var7 = var11;
      }

      return (Type)var11;
   }

   static Type a(Type var0, Class var1, TypeVariable var2) {
      Class var4 = a(var2);
      Object var5;
      if(var4 == null) {
         var5 = var2;
      } else {
         Type var6 = a(var0, var1, var4);
         var5 = var2;
         if(var6 instanceof ParameterizedType) {
            int var3 = a((Object[])var4.getTypeParameters(), (Object)var2);
            var5 = ((ParameterizedType)var6).getActualTypeArguments()[var3];
         }
      }

      return (Type)var5;
   }

   static boolean a(Object var0, Object var1) {
      boolean var2;
      if(var0 != var1 && (var0 == null || !var0.equals(var1))) {
         var2 = false;
      } else {
         var2 = true;
      }

      return var2;
   }

   public static boolean a(Type var0, Type var1) {
      boolean var5 = true;
      boolean var6 = true;
      boolean var4 = true;
      boolean var3 = false;
      boolean var2;
      if(var0 == var1) {
         var2 = true;
      } else if(var0 instanceof Class) {
         var2 = var0.equals(var1);
      } else if(var0 instanceof ParameterizedType) {
         var2 = var3;
         if(var1 instanceof ParameterizedType) {
            ParameterizedType var7 = (ParameterizedType)var0;
            ParameterizedType var10 = (ParameterizedType)var1;
            if(a((Object)var7.getOwnerType(), (Object)var10.getOwnerType()) && var7.getRawType().equals(var10.getRawType()) && Arrays.equals(var7.getActualTypeArguments(), var10.getActualTypeArguments())) {
               var2 = var4;
            } else {
               var2 = false;
            }
         }
      } else if(var0 instanceof GenericArrayType) {
         var2 = var3;
         if(var1 instanceof GenericArrayType) {
            GenericArrayType var8 = (GenericArrayType)var0;
            GenericArrayType var12 = (GenericArrayType)var1;
            var2 = a(var8.getGenericComponentType(), var12.getGenericComponentType());
         }
      } else if(var0 instanceof WildcardType) {
         var2 = var3;
         if(var1 instanceof WildcardType) {
            WildcardType var9 = (WildcardType)var0;
            WildcardType var13 = (WildcardType)var1;
            if(Arrays.equals(var9.getUpperBounds(), var13.getUpperBounds()) && Arrays.equals(var9.getLowerBounds(), var13.getLowerBounds())) {
               var2 = var5;
            } else {
               var2 = false;
            }
         }
      } else {
         var2 = var3;
         if(var0 instanceof TypeVariable) {
            var2 = var3;
            if(var1 instanceof TypeVariable) {
               TypeVariable var11 = (TypeVariable)var0;
               TypeVariable var14 = (TypeVariable)var1;
               if(var11.getGenericDeclaration() == var14.getGenericDeclaration() && var11.getName().equals(var14.getName())) {
                  var2 = var6;
               } else {
                  var2 = false;
               }
            }
         }
      }

      return var2;
   }

   static Type b(Type var0, Class var1, Class var2) {
      a.a(var2.isAssignableFrom(var1));
      return a(var0, var1, a(var0, var1, var2));
   }

   public static WildcardType b(Type var0) {
      Type[] var2;
      if(var0 instanceof WildcardType) {
         var2 = ((WildcardType)var0).getUpperBounds();
      } else {
         Type[] var1 = new Type[]{var0};
         var2 = var1;
      }

      return new b.c(var2, a);
   }

   public static Type[] b(Type var0, Class var1) {
      Type[] var2;
      if(var0 == Properties.class) {
         var2 = new Type[]{String.class, String.class};
      } else {
         var0 = b(var0, var1, Map.class);
         if(var0 instanceof ParameterizedType) {
            var2 = ((ParameterizedType)var0).getActualTypeArguments();
         } else {
            var2 = new Type[]{Object.class, Object.class};
         }
      }

      return var2;
   }

   public static WildcardType c(Type var0) {
      Type[] var2;
      if(var0 instanceof WildcardType) {
         var2 = ((WildcardType)var0).getLowerBounds();
      } else {
         Type[] var1 = new Type[]{var0};
         var2 = var1;
      }

      return new b.c(new Type[]{Object.class}, var2);
   }

   public static Type d(Type var0) {
      if(var0 instanceof Class) {
         var0 = (Class)var0;
         if(((Class)var0).isArray()) {
            var0 = new b.a(d(((Class)var0).getComponentType()));
         }

         var0 = (Type)var0;
      } else if(var0 instanceof ParameterizedType) {
         ParameterizedType var1 = (ParameterizedType)var0;
         var0 = new b.b(var1.getOwnerType(), var1.getRawType(), var1.getActualTypeArguments());
      } else if(var0 instanceof GenericArrayType) {
         var0 = new b.a(((GenericArrayType)var0).getGenericComponentType());
      } else if(var0 instanceof WildcardType) {
         WildcardType var2 = (WildcardType)var0;
         var0 = new b.c(var2.getUpperBounds(), var2.getLowerBounds());
      }

      return (Type)var0;
   }

   public static Class e(Type var0) {
      Class var2;
      if(var0 instanceof Class) {
         var2 = (Class)var0;
      } else if(var0 instanceof ParameterizedType) {
         var0 = ((ParameterizedType)var0).getRawType();
         a.a(var0 instanceof Class);
         var2 = (Class)var0;
      } else if(var0 instanceof GenericArrayType) {
         var2 = Array.newInstance(e(((GenericArrayType)var0).getGenericComponentType()), 0).getClass();
      } else if(var0 instanceof TypeVariable) {
         var2 = Object.class;
      } else {
         if(!(var0 instanceof WildcardType)) {
            String var1;
            if(var0 == null) {
               var1 = "null";
            } else {
               var1 = var0.getClass().getName();
            }

            throw new IllegalArgumentException("Expected a Class, ParameterizedType, or GenericArrayType, but <" + var0 + "> is of type " + var1);
         }

         var2 = e(((WildcardType)var0).getUpperBounds()[0]);
      }

      return var2;
   }

   public static String f(Type var0) {
      String var1;
      if(var0 instanceof Class) {
         var1 = ((Class)var0).getName();
      } else {
         var1 = var0.toString();
      }

      return var1;
   }

   public static Type g(Type var0) {
      Object var1;
      if(var0 instanceof GenericArrayType) {
         var1 = ((GenericArrayType)var0).getGenericComponentType();
      } else {
         var1 = ((Class)var0).getComponentType();
      }

      return (Type)var1;
   }

   static void h(Type var0) {
      boolean var1;
      if(var0 instanceof Class && ((Class)var0).isPrimitive()) {
         var1 = false;
      } else {
         var1 = true;
      }

      a.a(var1);
   }

   private static final class a implements Serializable, GenericArrayType {
      private final Type a;

      public a(Type var1) {
         this.a = b.d(var1);
      }

      public boolean equals(Object var1) {
         boolean var2;
         if(var1 instanceof GenericArrayType && b.a((Type)this, (Type)((GenericArrayType)var1))) {
            var2 = true;
         } else {
            var2 = false;
         }

         return var2;
      }

      public Type getGenericComponentType() {
         return this.a;
      }

      public int hashCode() {
         return this.a.hashCode();
      }

      public String toString() {
         return b.f(this.a) + "[]";
      }
   }

   private static final class b implements Serializable, ParameterizedType {
      private final Type a;
      private final Type b;
      private final Type[] c;

      public b(Type var1, Type var2, Type... var3) {
         byte var5 = 0;
         super();
         if(var2 instanceof Class) {
            Class var8 = (Class)var2;
            boolean var4;
            if(!Modifier.isStatic(var8.getModifiers()) && var8.getEnclosingClass() != null) {
               var4 = false;
            } else {
               var4 = true;
            }

            boolean var7;
            if(var1 == null && !var4) {
               var7 = false;
            } else {
               var7 = true;
            }

            a.a(var7);
         }

         if(var1 == null) {
            var1 = null;
         } else {
            var1 = b.d(var1);
         }

         this.a = var1;
         this.b = b.d(var2);
         this.c = (Type[])var3.clone();
         int var6 = this.c.length;

         for(int var9 = var5; var9 < var6; ++var9) {
            a.a(this.c[var9]);
            b.h(this.c[var9]);
            this.c[var9] = b.d(this.c[var9]);
         }

      }

      public boolean equals(Object var1) {
         boolean var2;
         if(var1 instanceof ParameterizedType && b.a((Type)this, (Type)((ParameterizedType)var1))) {
            var2 = true;
         } else {
            var2 = false;
         }

         return var2;
      }

      public Type[] getActualTypeArguments() {
         return (Type[])this.c.clone();
      }

      public Type getOwnerType() {
         return this.a;
      }

      public Type getRawType() {
         return this.b;
      }

      public int hashCode() {
         return Arrays.hashCode(this.c) ^ this.b.hashCode() ^ b.a((Object)this.a);
      }

      public String toString() {
         int var2 = this.c.length;
         String var3;
         if(var2 == 0) {
            var3 = b.f(this.b);
         } else {
            StringBuilder var4 = new StringBuilder((var2 + 1) * 30);
            var4.append(b.f(this.b)).append("<").append(b.f(this.c[0]));

            for(int var1 = 1; var1 < var2; ++var1) {
               var4.append(", ").append(b.f(this.c[var1]));
            }

            var3 = var4.append(">").toString();
         }

         return var3;
      }
   }

   private static final class c implements Serializable, WildcardType {
      private final Type a;
      private final Type b;

      public c(Type[] var1, Type[] var2) {
         boolean var4 = true;
         super();
         boolean var3;
         if(var2.length <= 1) {
            var3 = true;
         } else {
            var3 = false;
         }

         a.a(var3);
         if(var1.length == 1) {
            var3 = true;
         } else {
            var3 = false;
         }

         a.a(var3);
         if(var2.length == 1) {
            a.a(var2[0]);
            b.h(var2[0]);
            if(var1[0] == Object.class) {
               var3 = var4;
            } else {
               var3 = false;
            }

            a.a(var3);
            this.b = b.d(var2[0]);
            this.a = Object.class;
         } else {
            a.a(var1[0]);
            b.h(var1[0]);
            this.b = null;
            this.a = b.d(var1[0]);
         }

      }

      public boolean equals(Object var1) {
         boolean var2;
         if(var1 instanceof WildcardType && b.a((Type)this, (Type)((WildcardType)var1))) {
            var2 = true;
         } else {
            var2 = false;
         }

         return var2;
      }

      public Type[] getLowerBounds() {
         Type[] var1;
         if(this.b != null) {
            var1 = new Type[]{this.b};
         } else {
            var1 = b.a;
         }

         return var1;
      }

      public Type[] getUpperBounds() {
         return new Type[]{this.a};
      }

      public int hashCode() {
         int var1;
         if(this.b != null) {
            var1 = this.b.hashCode() + 31;
         } else {
            var1 = 1;
         }

         return var1 ^ this.a.hashCode() + 31;
      }

      public String toString() {
         String var1;
         if(this.b != null) {
            var1 = "? super " + b.f(this.b);
         } else if(this.a == Object.class) {
            var1 = "?";
         } else {
            var1 = "? extends " + b.f(this.a);
         }

         return var1;
      }
   }
}
