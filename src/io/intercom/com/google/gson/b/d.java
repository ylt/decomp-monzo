package io.intercom.com.google.gson.b;

import io.intercom.com.google.gson.q;
import io.intercom.com.google.gson.r;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public final class d implements r, Cloneable {
   public static final d a = new d();
   private double b = -1.0D;
   private int c = 136;
   private boolean d = true;
   private boolean e;
   private List f = Collections.emptyList();
   private List g = Collections.emptyList();

   private boolean a(io.intercom.com.google.gson.a.d var1) {
      boolean var2;
      if(var1 != null && var1.a() > this.b) {
         var2 = false;
      } else {
         var2 = true;
      }

      return var2;
   }

   private boolean a(io.intercom.com.google.gson.a.d var1, io.intercom.com.google.gson.a.e var2) {
      boolean var3;
      if(this.a(var1) && this.a(var2)) {
         var3 = true;
      } else {
         var3 = false;
      }

      return var3;
   }

   private boolean a(io.intercom.com.google.gson.a.e var1) {
      boolean var2;
      if(var1 != null && var1.a() <= this.b) {
         var2 = false;
      } else {
         var2 = true;
      }

      return var2;
   }

   private boolean a(Class var1) {
      boolean var2;
      if(Enum.class.isAssignableFrom(var1) || !var1.isAnonymousClass() && !var1.isLocalClass()) {
         var2 = false;
      } else {
         var2 = true;
      }

      return var2;
   }

   private boolean b(Class var1) {
      boolean var2;
      if(var1.isMemberClass() && !this.c(var1)) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   private boolean c(Class var1) {
      boolean var2;
      if((var1.getModifiers() & 8) != 0) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   protected d a() {
      try {
         d var1 = (d)super.clone();
         return var1;
      } catch (CloneNotSupportedException var2) {
         throw new AssertionError(var2);
      }
   }

   public q a(final io.intercom.com.google.gson.e var1, final io.intercom.com.google.gson.c.a var2) {
      Class var5 = var2.a();
      final boolean var3 = this.a(var5, true);
      final boolean var4 = this.a(var5, false);
      q var6;
      if(!var3 && !var4) {
         var6 = null;
      } else {
         var6 = new q() {
            private q f;

            private q b() {
               q var1x = this.f;
               if(var1x == null) {
                  var1x = var1.a((r)d.this, (io.intercom.com.google.gson.c.a)var2);
                  this.f = var1x;
               }

               return var1x;
            }

            public void a(io.intercom.com.google.gson.stream.c var1x, Object var2x) throws IOException {
               if(var3) {
                  var1x.f();
               } else {
                  this.b().a(var1x, var2x);
               }

            }

            public Object b(io.intercom.com.google.gson.stream.a var1x) throws IOException {
               Object var2x;
               if(var4) {
                  var1x.n();
                  var2x = null;
               } else {
                  var2x = this.b().b(var1x);
               }

               return var2x;
            }
         };
      }

      return var6;
   }

   public boolean a(Class var1, boolean var2) {
      if(this.b != -1.0D && !this.a((io.intercom.com.google.gson.a.d)var1.getAnnotation(io.intercom.com.google.gson.a.d.class), (io.intercom.com.google.gson.a.e)var1.getAnnotation(io.intercom.com.google.gson.a.e.class))) {
         var2 = true;
      } else if(!this.d && this.b(var1)) {
         var2 = true;
      } else if(this.a(var1)) {
         var2 = true;
      } else {
         List var3;
         if(var2) {
            var3 = this.f;
         } else {
            var3 = this.g;
         }

         Iterator var4 = var3.iterator();

         while(true) {
            if(!var4.hasNext()) {
               var2 = false;
               break;
            }

            if(((io.intercom.com.google.gson.a)var4.next()).a(var1)) {
               var2 = true;
               break;
            }
         }
      }

      return var2;
   }

   public boolean a(Field var1, boolean var2) {
      if((this.c & var1.getModifiers()) != 0) {
         var2 = true;
      } else if(this.b != -1.0D && !this.a((io.intercom.com.google.gson.a.d)var1.getAnnotation(io.intercom.com.google.gson.a.d.class), (io.intercom.com.google.gson.a.e)var1.getAnnotation(io.intercom.com.google.gson.a.e.class))) {
         var2 = true;
      } else if(var1.isSynthetic()) {
         var2 = true;
      } else {
         label76: {
            if(this.e) {
               io.intercom.com.google.gson.a.a var3 = (io.intercom.com.google.gson.a.a)var1.getAnnotation(io.intercom.com.google.gson.a.a.class);
               if(var3 == null) {
                  break label76;
               }

               if(var2) {
                  if(!var3.a()) {
                     break label76;
                  }
               } else if(!var3.b()) {
                  break label76;
               }
            }

            if(!this.d && this.b(var1.getType())) {
               var2 = true;
               return var2;
            }

            if(this.a(var1.getType())) {
               var2 = true;
            } else {
               List var5;
               if(var2) {
                  var5 = this.f;
               } else {
                  var5 = this.g;
               }

               if(!var5.isEmpty()) {
                  io.intercom.com.google.gson.b var4 = new io.intercom.com.google.gson.b(var1);
                  Iterator var6 = var5.iterator();

                  while(var6.hasNext()) {
                     if(((io.intercom.com.google.gson.a)var6.next()).a(var4)) {
                        var2 = true;
                        return var2;
                     }
                  }
               }

               var2 = false;
            }

            return var2;
         }

         var2 = true;
      }

      return var2;
   }

   // $FF: synthetic method
   protected Object clone() throws CloneNotSupportedException {
      return this.a();
   }
}
