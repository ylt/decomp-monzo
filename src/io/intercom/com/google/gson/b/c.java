package io.intercom.com.google.gson.b;

import io.intercom.com.google.gson.JsonIOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;

public final class c {
   private final Map a;

   public c(Map var1) {
      this.a = var1;
   }

   private h a(Class var1) {
      h var4;
      try {
         final Constructor var2 = var1.getDeclaredConstructor(new Class[0]);
         if(!var2.isAccessible()) {
            var2.setAccessible(true);
         }

         var4 = new h() {
            public Object a() {
               try {
                  Object var1 = var2.newInstance((Object[])null);
                  return var1;
               } catch (InstantiationException var2x) {
                  throw new RuntimeException("Failed to invoke " + var2 + " with no args", var2x);
               } catch (InvocationTargetException var3) {
                  throw new RuntimeException("Failed to invoke " + var2 + " with no args", var3.getTargetException());
               } catch (IllegalAccessException var4) {
                  throw new AssertionError(var4);
               }
            }
         };
      } catch (NoSuchMethodException var3) {
         var4 = null;
      }

      return var4;
   }

   private h a(final Type var1, Class var2) {
      h var3;
      if(Collection.class.isAssignableFrom(var2)) {
         if(SortedSet.class.isAssignableFrom(var2)) {
            var3 = new h() {
               public Object a() {
                  return new TreeSet();
               }
            };
         } else if(EnumSet.class.isAssignableFrom(var2)) {
            var3 = new h() {
               public Object a() {
                  if(var1 instanceof ParameterizedType) {
                     Type var1x = ((ParameterizedType)var1).getActualTypeArguments()[0];
                     if(var1x instanceof Class) {
                        return EnumSet.noneOf((Class)var1x);
                     } else {
                        throw new JsonIOException("Invalid EnumSet type: " + var1.toString());
                     }
                  } else {
                     throw new JsonIOException("Invalid EnumSet type: " + var1.toString());
                  }
               }
            };
         } else if(Set.class.isAssignableFrom(var2)) {
            var3 = new h() {
               public Object a() {
                  return new LinkedHashSet();
               }
            };
         } else if(Queue.class.isAssignableFrom(var2)) {
            var3 = new h() {
               public Object a() {
                  return new ArrayDeque();
               }
            };
         } else {
            var3 = new h() {
               public Object a() {
                  return new ArrayList();
               }
            };
         }
      } else if(Map.class.isAssignableFrom(var2)) {
         if(ConcurrentNavigableMap.class.isAssignableFrom(var2)) {
            var3 = new h() {
               public Object a() {
                  return new ConcurrentSkipListMap();
               }
            };
         } else if(ConcurrentMap.class.isAssignableFrom(var2)) {
            var3 = new h() {
               public Object a() {
                  return new ConcurrentHashMap();
               }
            };
         } else if(SortedMap.class.isAssignableFrom(var2)) {
            var3 = new h() {
               public Object a() {
                  return new TreeMap();
               }
            };
         } else if(var1 instanceof ParameterizedType && !String.class.isAssignableFrom(io.intercom.com.google.gson.c.a.a(((ParameterizedType)var1).getActualTypeArguments()[0]).a())) {
            var3 = new h() {
               public Object a() {
                  return new LinkedHashMap();
               }
            };
         } else {
            var3 = new h() {
               public Object a() {
                  return new g();
               }
            };
         }
      } else {
         var3 = null;
      }

      return var3;
   }

   private h b(final Type var1, final Class var2) {
      return new h() {
         private final k d = k.a();

         public Object a() {
            try {
               Object var1x = this.d.a(var2);
               return var1x;
            } catch (Exception var2x) {
               throw new RuntimeException("Unable to invoke no-args constructor for " + var1 + ". " + "Register an InstanceCreator with Gson for this type may fix this problem.", var2x);
            }
         }
      };
   }

   public h a(io.intercom.com.google.gson.c.a var1) {
      final Type var3 = var1.b();
      Class var4 = var1.a();
      final io.intercom.com.google.gson.f var5 = (io.intercom.com.google.gson.f)this.a.get(var3);
      h var6;
      if(var5 != null) {
         var6 = new h() {
            public Object a() {
               return var5.a(var3);
            }
         };
      } else {
         var5 = (io.intercom.com.google.gson.f)this.a.get(var4);
         if(var5 != null) {
            var6 = new h() {
               public Object a() {
                  return var5.a(var3);
               }
            };
         } else {
            h var2 = this.a(var4);
            var6 = var2;
            if(var2 == null) {
               var2 = this.a(var3, var4);
               var6 = var2;
               if(var2 == null) {
                  var6 = this.b(var3, var4);
               }
            }
         }
      }

      return var6;
   }

   public String toString() {
      return this.a.toString();
   }
}
