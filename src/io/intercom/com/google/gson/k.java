package io.intercom.com.google.gson;

public final class k extends j {
   public static final k a = new k();

   public boolean equals(Object var1) {
      boolean var2;
      if(this != var1 && !(var1 instanceof k)) {
         var2 = false;
      } else {
         var2 = true;
      }

      return var2;
   }

   public int hashCode() {
      return k.class.hashCode();
   }
}
