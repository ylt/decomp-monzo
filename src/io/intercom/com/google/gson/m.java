package io.intercom.com.google.gson;

import java.math.BigInteger;

public final class m extends j {
   private static final Class[] a;
   private Object b;

   static {
      a = new Class[]{Integer.TYPE, Long.TYPE, Short.TYPE, Float.TYPE, Double.TYPE, Byte.TYPE, Boolean.TYPE, Character.TYPE, Integer.class, Long.class, Short.class, Float.class, Double.class, Byte.class, Boolean.class, Character.class};
   }

   public m(Boolean var1) {
      this.a((Object)var1);
   }

   public m(Number var1) {
      this.a((Object)var1);
   }

   public m(String var1) {
      this.a((Object)var1);
   }

   private static boolean a(m var0) {
      boolean var1;
      if(var0.b instanceof Number) {
         Number var2 = (Number)var0.b;
         if(!(var2 instanceof BigInteger) && !(var2 instanceof Long) && !(var2 instanceof Integer) && !(var2 instanceof Short) && !(var2 instanceof Byte)) {
            var1 = false;
         } else {
            var1 = true;
         }
      } else {
         var1 = false;
      }

      return var1;
   }

   private static boolean b(Object var0) {
      boolean var4 = true;
      boolean var3;
      if(var0 instanceof String) {
         var3 = var4;
      } else {
         Class var6 = var0.getClass();
         Class[] var5 = a;
         int var2 = var5.length;
         int var1 = 0;

         while(true) {
            if(var1 >= var2) {
               var3 = false;
               break;
            }

            var3 = var4;
            if(var5[var1].isAssignableFrom(var6)) {
               break;
            }

            ++var1;
         }
      }

      return var3;
   }

   public Number a() {
      Object var1;
      if(this.b instanceof String) {
         var1 = new io.intercom.com.google.gson.b.f((String)this.b);
      } else {
         var1 = (Number)this.b;
      }

      return (Number)var1;
   }

   void a(Object var1) {
      if(var1 instanceof Character) {
         this.b = String.valueOf(((Character)var1).charValue());
      } else {
         boolean var2;
         if(!(var1 instanceof Number) && !b(var1)) {
            var2 = false;
         } else {
            var2 = true;
         }

         io.intercom.com.google.gson.b.a.a(var2);
         this.b = var1;
      }

   }

   public String b() {
      String var1;
      if(this.p()) {
         var1 = this.a().toString();
      } else if(this.o()) {
         var1 = this.n().toString();
      } else {
         var1 = (String)this.b;
      }

      return var1;
   }

   public double c() {
      double var1;
      if(this.p()) {
         var1 = this.a().doubleValue();
      } else {
         var1 = Double.parseDouble(this.b());
      }

      return var1;
   }

   public long d() {
      long var1;
      if(this.p()) {
         var1 = this.a().longValue();
      } else {
         var1 = Long.parseLong(this.b());
      }

      return var1;
   }

   public int e() {
      int var1;
      if(this.p()) {
         var1 = this.a().intValue();
      } else {
         var1 = Integer.parseInt(this.b());
      }

      return var1;
   }

   public boolean equals(Object var1) {
      boolean var6 = true;
      boolean var7 = false;
      if(this != var1) {
         if(var1 != null && this.getClass() == var1.getClass()) {
            m var8 = (m)var1;
            if(this.b == null) {
               if(var8.b != null) {
                  var6 = false;
               }
            } else if(a(this) && a(var8)) {
               if(this.a().longValue() != var8.a().longValue()) {
                  var6 = false;
               }
            } else if(this.b instanceof Number && var8.b instanceof Number) {
               double var2 = this.a().doubleValue();
               double var4 = var8.a().doubleValue();
               if(var2 != var4) {
                  var6 = var7;
                  if(!Double.isNaN(var2)) {
                     return var6;
                  }

                  var6 = var7;
                  if(!Double.isNaN(var4)) {
                     return var6;
                  }
               }

               var6 = true;
            } else {
               var6 = this.b.equals(var8.b);
            }
         } else {
            var6 = false;
         }
      }

      return var6;
   }

   public boolean f() {
      boolean var1;
      if(this.o()) {
         var1 = this.n().booleanValue();
      } else {
         var1 = Boolean.parseBoolean(this.b());
      }

      return var1;
   }

   public int hashCode() {
      int var1;
      if(this.b == null) {
         var1 = 31;
      } else {
         long var2;
         if(a(this)) {
            var2 = this.a().longValue();
            var1 = (int)(var2 ^ var2 >>> 32);
         } else if(this.b instanceof Number) {
            var2 = Double.doubleToLongBits(this.a().doubleValue());
            var1 = (int)(var2 ^ var2 >>> 32);
         } else {
            var1 = this.b.hashCode();
         }
      }

      return var1;
   }

   Boolean n() {
      return (Boolean)this.b;
   }

   public boolean o() {
      return this.b instanceof Boolean;
   }

   public boolean p() {
      return this.b instanceof Number;
   }

   public boolean q() {
      return this.b instanceof String;
   }
}
