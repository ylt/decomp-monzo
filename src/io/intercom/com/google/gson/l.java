package io.intercom.com.google.gson;

import java.util.Set;

public final class l extends j {
   private final io.intercom.com.google.gson.b.g a = new io.intercom.com.google.gson.b.g();

   public void a(String var1, j var2) {
      Object var3 = var2;
      if(var2 == null) {
         var3 = k.a;
      }

      this.a.put(var1, var3);
   }

   public boolean equals(Object var1) {
      boolean var2;
      if(var1 != this && (!(var1 instanceof l) || !((l)var1).a.equals(this.a))) {
         var2 = false;
      } else {
         var2 = true;
      }

      return var2;
   }

   public int hashCode() {
      return this.a.hashCode();
   }

   public Set o() {
      return this.a.entrySet();
   }
}
