package io.intercom.com.google.gson;

import io.intercom.com.google.gson.stream.MalformedJsonException;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicLongArray;

public final class e {
   private static final io.intercom.com.google.gson.c.a a = io.intercom.com.google.gson.c.a.b(Object.class);
   private final ThreadLocal b;
   private final Map c;
   private final List d;
   private final io.intercom.com.google.gson.b.c e;
   private final io.intercom.com.google.gson.b.d f;
   private final d g;
   private final boolean h;
   private final boolean i;
   private final boolean j;
   private final boolean k;
   private final boolean l;
   private final io.intercom.com.google.gson.b.a.d m;

   public e() {
      this(io.intercom.com.google.gson.b.d.a, c.a, Collections.emptyMap(), false, false, false, true, false, false, false, p.a, Collections.emptyList());
   }

   e(io.intercom.com.google.gson.b.d var1, d var2, Map var3, boolean var4, boolean var5, boolean var6, boolean var7, boolean var8, boolean var9, boolean var10, p var11, List var12) {
      this.b = new ThreadLocal();
      this.c = new ConcurrentHashMap();
      this.e = new io.intercom.com.google.gson.b.c(var3);
      this.f = var1;
      this.g = var2;
      this.h = var4;
      this.j = var6;
      this.i = var7;
      this.k = var8;
      this.l = var9;
      ArrayList var14 = new ArrayList();
      var14.add(io.intercom.com.google.gson.b.a.n.Y);
      var14.add(io.intercom.com.google.gson.b.a.h.a);
      var14.add(var1);
      var14.addAll(var12);
      var14.add(io.intercom.com.google.gson.b.a.n.D);
      var14.add(io.intercom.com.google.gson.b.a.n.m);
      var14.add(io.intercom.com.google.gson.b.a.n.g);
      var14.add(io.intercom.com.google.gson.b.a.n.i);
      var14.add(io.intercom.com.google.gson.b.a.n.k);
      q var13 = a(var11);
      var14.add(io.intercom.com.google.gson.b.a.n.a(Long.TYPE, Long.class, var13));
      var14.add(io.intercom.com.google.gson.b.a.n.a(Double.TYPE, Double.class, this.a(var10)));
      var14.add(io.intercom.com.google.gson.b.a.n.a(Float.TYPE, Float.class, this.b(var10)));
      var14.add(io.intercom.com.google.gson.b.a.n.x);
      var14.add(io.intercom.com.google.gson.b.a.n.o);
      var14.add(io.intercom.com.google.gson.b.a.n.q);
      var14.add(io.intercom.com.google.gson.b.a.n.a(AtomicLong.class, a(var13)));
      var14.add(io.intercom.com.google.gson.b.a.n.a(AtomicLongArray.class, b(var13)));
      var14.add(io.intercom.com.google.gson.b.a.n.s);
      var14.add(io.intercom.com.google.gson.b.a.n.z);
      var14.add(io.intercom.com.google.gson.b.a.n.F);
      var14.add(io.intercom.com.google.gson.b.a.n.H);
      var14.add(io.intercom.com.google.gson.b.a.n.a(BigDecimal.class, io.intercom.com.google.gson.b.a.n.B));
      var14.add(io.intercom.com.google.gson.b.a.n.a(BigInteger.class, io.intercom.com.google.gson.b.a.n.C));
      var14.add(io.intercom.com.google.gson.b.a.n.J);
      var14.add(io.intercom.com.google.gson.b.a.n.L);
      var14.add(io.intercom.com.google.gson.b.a.n.P);
      var14.add(io.intercom.com.google.gson.b.a.n.R);
      var14.add(io.intercom.com.google.gson.b.a.n.W);
      var14.add(io.intercom.com.google.gson.b.a.n.N);
      var14.add(io.intercom.com.google.gson.b.a.n.d);
      var14.add(io.intercom.com.google.gson.b.a.c.a);
      var14.add(io.intercom.com.google.gson.b.a.n.U);
      var14.add(io.intercom.com.google.gson.b.a.k.a);
      var14.add(io.intercom.com.google.gson.b.a.j.a);
      var14.add(io.intercom.com.google.gson.b.a.n.S);
      var14.add(io.intercom.com.google.gson.b.a.a.a);
      var14.add(io.intercom.com.google.gson.b.a.n.b);
      var14.add(new io.intercom.com.google.gson.b.a.b(this.e));
      var14.add(new io.intercom.com.google.gson.b.a.g(this.e, var5));
      this.m = new io.intercom.com.google.gson.b.a.d(this.e);
      var14.add(this.m);
      var14.add(io.intercom.com.google.gson.b.a.n.Z);
      var14.add(new io.intercom.com.google.gson.b.a.i(this.e, var2, var1, this.m));
      this.d = Collections.unmodifiableList(var14);
   }

   private static q a(p var0) {
      q var1;
      if(var0 == p.a) {
         var1 = io.intercom.com.google.gson.b.a.n.t;
      } else {
         var1 = new q() {
            public Number a(io.intercom.com.google.gson.stream.a var1) throws IOException {
               Long var2;
               if(var1.f() == io.intercom.com.google.gson.stream.b.i) {
                  var1.j();
                  var2 = null;
               } else {
                  var2 = Long.valueOf(var1.l());
               }

               return var2;
            }

            public void a(io.intercom.com.google.gson.stream.c var1, Number var2) throws IOException {
               if(var2 == null) {
                  var1.f();
               } else {
                  var1.b(var2.toString());
               }

            }

            // $FF: synthetic method
            public Object b(io.intercom.com.google.gson.stream.a var1) throws IOException {
               return this.a(var1);
            }
         };
      }

      return var1;
   }

   private static q a(final q var0) {
      return (new q() {
         public AtomicLong a(io.intercom.com.google.gson.stream.a var1) throws IOException {
            return new AtomicLong(((Number)var0.b(var1)).longValue());
         }

         public void a(io.intercom.com.google.gson.stream.c var1, AtomicLong var2) throws IOException {
            var0.a(var1, Long.valueOf(var2.get()));
         }

         // $FF: synthetic method
         public Object b(io.intercom.com.google.gson.stream.a var1) throws IOException {
            return this.a(var1);
         }
      }).a();
   }

   private q a(boolean var1) {
      q var2;
      if(var1) {
         var2 = io.intercom.com.google.gson.b.a.n.v;
      } else {
         var2 = new q() {
            public Double a(io.intercom.com.google.gson.stream.a var1) throws IOException {
               Double var2;
               if(var1.f() == io.intercom.com.google.gson.stream.b.i) {
                  var1.j();
                  var2 = null;
               } else {
                  var2 = Double.valueOf(var1.k());
               }

               return var2;
            }

            public void a(io.intercom.com.google.gson.stream.c var1, Number var2) throws IOException {
               if(var2 == null) {
                  var1.f();
               } else {
                  e.a(var2.doubleValue());
                  var1.a(var2);
               }

            }

            // $FF: synthetic method
            public Object b(io.intercom.com.google.gson.stream.a var1) throws IOException {
               return this.a(var1);
            }
         };
      }

      return var2;
   }

   static void a(double var0) {
      if(Double.isNaN(var0) || Double.isInfinite(var0)) {
         throw new IllegalArgumentException(var0 + " is not a valid double value as per JSON specification. To override this" + " behavior, use GsonBuilder.serializeSpecialFloatingPointValues() method.");
      }
   }

   private static void a(Object var0, io.intercom.com.google.gson.stream.a var1) {
      if(var0 != null) {
         try {
            if(var1.f() != io.intercom.com.google.gson.stream.b.j) {
               JsonIOException var4 = new JsonIOException("JSON document was not fully consumed.");
               throw var4;
            }
         } catch (MalformedJsonException var2) {
            throw new JsonSyntaxException(var2);
         } catch (IOException var3) {
            throw new JsonIOException(var3);
         }
      }

   }

   private static q b(final q var0) {
      return (new q() {
         public AtomicLongArray a(io.intercom.com.google.gson.stream.a var1) throws IOException {
            ArrayList var4 = new ArrayList();
            var1.a();

            while(var1.e()) {
               var4.add(Long.valueOf(((Number)var0.b(var1)).longValue()));
            }

            var1.b();
            int var3 = var4.size();
            AtomicLongArray var5 = new AtomicLongArray(var3);

            for(int var2 = 0; var2 < var3; ++var2) {
               var5.set(var2, ((Long)var4.get(var2)).longValue());
            }

            return var5;
         }

         public void a(io.intercom.com.google.gson.stream.c var1, AtomicLongArray var2) throws IOException {
            var1.b();
            int var3 = 0;

            for(int var4 = var2.length(); var3 < var4; ++var3) {
               var0.a(var1, Long.valueOf(var2.get(var3)));
            }

            var1.c();
         }

         // $FF: synthetic method
         public Object b(io.intercom.com.google.gson.stream.a var1) throws IOException {
            return this.a(var1);
         }
      }).a();
   }

   private q b(boolean var1) {
      q var2;
      if(var1) {
         var2 = io.intercom.com.google.gson.b.a.n.u;
      } else {
         var2 = new q() {
            public Float a(io.intercom.com.google.gson.stream.a var1) throws IOException {
               Float var2;
               if(var1.f() == io.intercom.com.google.gson.stream.b.i) {
                  var1.j();
                  var2 = null;
               } else {
                  var2 = Float.valueOf((float)var1.k());
               }

               return var2;
            }

            public void a(io.intercom.com.google.gson.stream.c var1, Number var2) throws IOException {
               if(var2 == null) {
                  var1.f();
               } else {
                  e.a((double)var2.floatValue());
                  var1.a(var2);
               }

            }

            // $FF: synthetic method
            public Object b(io.intercom.com.google.gson.stream.a var1) throws IOException {
               return this.a(var1);
            }
         };
      }

      return var2;
   }

   public q a(io.intercom.com.google.gson.c.a param1) {
      // $FF: Couldn't be decompiled
   }

   public q a(r var1, io.intercom.com.google.gson.c.a var2) {
      Object var4 = var1;
      if(!this.d.contains(var1)) {
         var4 = this.m;
      }

      Iterator var6 = this.d.iterator();
      boolean var3 = false;

      while(var6.hasNext()) {
         r var5 = (r)var6.next();
         if(!var3) {
            if(var5 == var4) {
               var3 = true;
            }
         } else {
            q var7 = var5.a(this, var2);
            if(var7 != null) {
               return var7;
            }
         }
      }

      throw new IllegalArgumentException("GSON cannot serialize " + var2);
   }

   public q a(Class var1) {
      return this.a(io.intercom.com.google.gson.c.a.b(var1));
   }

   public io.intercom.com.google.gson.stream.a a(Reader var1) {
      io.intercom.com.google.gson.stream.a var2 = new io.intercom.com.google.gson.stream.a(var1);
      var2.a(this.l);
      return var2;
   }

   public io.intercom.com.google.gson.stream.c a(Writer var1) throws IOException {
      if(this.j) {
         var1.write(")]}'\n");
      }

      io.intercom.com.google.gson.stream.c var2 = new io.intercom.com.google.gson.stream.c(var1);
      if(this.k) {
         var2.c("  ");
      }

      var2.d(this.h);
      return var2;
   }

   public Object a(io.intercom.com.google.gson.stream.a param1, Type param2) throws JsonIOException, JsonSyntaxException {
      // $FF: Couldn't be decompiled
   }

   public Object a(Reader var1, Class var2) throws JsonSyntaxException, JsonIOException {
      io.intercom.com.google.gson.stream.a var4 = this.a(var1);
      Object var3 = this.a((io.intercom.com.google.gson.stream.a)var4, (Type)var2);
      a(var3, var4);
      return io.intercom.com.google.gson.b.i.a(var2).cast(var3);
   }

   public Object a(Reader var1, Type var2) throws JsonIOException, JsonSyntaxException {
      io.intercom.com.google.gson.stream.a var3 = this.a(var1);
      Object var4 = this.a(var3, var2);
      a(var4, var3);
      return var4;
   }

   public Object a(String var1, Class var2) throws JsonSyntaxException {
      Object var3 = this.a((String)var1, (Type)var2);
      return io.intercom.com.google.gson.b.i.a(var2).cast(var3);
   }

   public Object a(String var1, Type var2) throws JsonSyntaxException {
      Object var3;
      if(var1 == null) {
         var3 = null;
      } else {
         var3 = this.a((Reader)(new StringReader(var1)), (Type)var2);
      }

      return var3;
   }

   public String a(j var1) {
      StringWriter var2 = new StringWriter();
      this.a((j)var1, (Appendable)var2);
      return var2.toString();
   }

   public String a(Object var1) {
      String var2;
      if(var1 == null) {
         var2 = this.a((j)k.a);
      } else {
         var2 = this.a((Object)var1, (Type)var1.getClass());
      }

      return var2;
   }

   public String a(Object var1, Type var2) {
      StringWriter var3 = new StringWriter();
      this.a(var1, var2, (Appendable)var3);
      return var3.toString();
   }

   public void a(j var1, io.intercom.com.google.gson.stream.c var2) throws JsonIOException {
      boolean var3 = var2.g();
      var2.b(true);
      boolean var4 = var2.h();
      var2.c(this.i);
      boolean var5 = var2.i();
      var2.d(this.h);

      try {
         io.intercom.com.google.gson.b.j.a(var1, var2);
      } catch (IOException var9) {
         JsonIOException var11 = new JsonIOException(var9);
         throw var11;
      } finally {
         var2.b(var3);
         var2.c(var4);
         var2.d(var5);
      }

   }

   public void a(j var1, Appendable var2) throws JsonIOException {
      try {
         this.a(var1, this.a(io.intercom.com.google.gson.b.j.a(var2)));
      } catch (IOException var3) {
         throw new JsonIOException(var3);
      }
   }

   public void a(Object var1, Appendable var2) throws JsonIOException {
      if(var1 != null) {
         this.a(var1, var1.getClass(), (Appendable)var2);
      } else {
         this.a((j)k.a, (Appendable)var2);
      }

   }

   public void a(Object var1, Type var2, io.intercom.com.google.gson.stream.c var3) throws JsonIOException {
      q var12 = this.a(io.intercom.com.google.gson.c.a.a(var2));
      boolean var4 = var3.g();
      var3.b(true);
      boolean var6 = var3.h();
      var3.c(this.i);
      boolean var5 = var3.i();
      var3.d(this.h);

      try {
         var12.a(var3, var1);
      } catch (IOException var9) {
         JsonIOException var11 = new JsonIOException(var9);
         throw var11;
      } finally {
         var3.b(var4);
         var3.c(var6);
         var3.d(var5);
      }

   }

   public void a(Object var1, Type var2, Appendable var3) throws JsonIOException {
      try {
         this.a(var1, var2, this.a(io.intercom.com.google.gson.b.j.a(var3)));
      } catch (IOException var4) {
         throw new JsonIOException(var4);
      }
   }

   public String toString() {
      return "{serializeNulls:" + this.h + ",factories:" + this.d + ",instanceCreators:" + this.e + "}";
   }

   static class a extends q {
      private q a;

      public void a(q var1) {
         if(this.a != null) {
            throw new AssertionError();
         } else {
            this.a = var1;
         }
      }

      public void a(io.intercom.com.google.gson.stream.c var1, Object var2) throws IOException {
         if(this.a == null) {
            throw new IllegalStateException();
         } else {
            this.a.a(var1, var2);
         }
      }

      public Object b(io.intercom.com.google.gson.stream.a var1) throws IOException {
         if(this.a == null) {
            throw new IllegalStateException();
         } else {
            return this.a.b(var1);
         }
      }
   }
}
