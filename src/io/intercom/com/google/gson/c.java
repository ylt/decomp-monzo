package io.intercom.com.google.gson;

import java.lang.reflect.Field;
import java.util.Locale;

public enum c implements d {
   a {
      public String a(Field var1) {
         return var1.getName();
      }
   },
   b {
      public String a(Field var1) {
         return a(var1.getName());
      }
   },
   c {
      public String a(Field var1) {
         return a(a(var1.getName(), " "));
      }
   },
   d {
      public String a(Field var1) {
         return a(var1.getName(), "_").toLowerCase(Locale.ENGLISH);
      }
   },
   e {
      public String a(Field var1) {
         return a(var1.getName(), "-").toLowerCase(Locale.ENGLISH);
      }
   };

   private c() {
   }

   // $FF: synthetic method
   c(Object var3) {
      this();
   }

   private static String a(char var0, String var1, int var2) {
      if(var2 < var1.length()) {
         var1 = var0 + var1.substring(var2);
      } else {
         var1 = String.valueOf(var0);
      }

      return var1;
   }

   static String a(String var0) {
      StringBuilder var5 = new StringBuilder();
      int var2 = 0;
      char var1 = var0.charAt(0);

      for(int var3 = var0.length(); var2 < var3 - 1 && !Character.isLetter(var1); var1 = var0.charAt(var2)) {
         var5.append(var1);
         ++var2;
      }

      String var4 = var0;
      if(!Character.isUpperCase(var1)) {
         var4 = var5.append(a(Character.toUpperCase(var1), var0, var2 + 1)).toString();
      }

      return var4;
   }

   static String a(String var0, String var1) {
      StringBuilder var5 = new StringBuilder();
      int var3 = 0;

      for(int var4 = var0.length(); var3 < var4; ++var3) {
         char var2 = var0.charAt(var3);
         if(Character.isUpperCase(var2) && var5.length() != 0) {
            var5.append(var1);
         }

         var5.append(var2);
      }

      return var5.toString();
   }
}
