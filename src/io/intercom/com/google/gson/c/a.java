package io.intercom.com.google.gson.c;

import io.intercom.com.google.gson.b.b;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public class a {
   final Class a;
   final Type b;
   final int c;

   protected a() {
      this.b = a(this.getClass());
      this.a = b.e(this.b);
      this.c = this.b.hashCode();
   }

   a(Type var1) {
      this.b = b.d((Type)io.intercom.com.google.gson.b.a.a(var1));
      this.a = b.e(this.b);
      this.c = this.b.hashCode();
   }

   public static a a(Type var0) {
      return new a(var0);
   }

   static Type a(Class var0) {
      Type var1 = var0.getGenericSuperclass();
      if(var1 instanceof Class) {
         throw new RuntimeException("Missing type parameter.");
      } else {
         return b.d(((ParameterizedType)var1).getActualTypeArguments()[0]);
      }
   }

   public static a b(Class var0) {
      return new a(var0);
   }

   public final Class a() {
      return this.a;
   }

   public final Type b() {
      return this.b;
   }

   public final boolean equals(Object var1) {
      boolean var2;
      if(var1 instanceof a && b.a(this.b, ((a)var1).b)) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   public final int hashCode() {
      return this.c;
   }

   public final String toString() {
      return b.f(this.b);
   }
}
