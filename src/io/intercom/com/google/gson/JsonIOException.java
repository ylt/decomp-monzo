package io.intercom.com.google.gson;

public final class JsonIOException extends JsonParseException {
   public JsonIOException(String var1) {
      super(var1);
   }

   public JsonIOException(Throwable var1) {
      super(var1);
   }
}
