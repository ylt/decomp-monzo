package io.intercom.com.google.gson.stream;

import java.io.IOException;

public final class MalformedJsonException extends IOException {
   public MalformedJsonException(String var1) {
      super(var1);
   }
}
