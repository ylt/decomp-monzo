package io.intercom.com.google.gson;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class g extends j implements Iterable {
   private final List a = new ArrayList();

   public Number a() {
      if(this.a.size() == 1) {
         return ((j)this.a.get(0)).a();
      } else {
         throw new IllegalStateException();
      }
   }

   public void a(j var1) {
      Object var2 = var1;
      if(var1 == null) {
         var2 = k.a;
      }

      this.a.add(var2);
   }

   public String b() {
      if(this.a.size() == 1) {
         return ((j)this.a.get(0)).b();
      } else {
         throw new IllegalStateException();
      }
   }

   public double c() {
      if(this.a.size() == 1) {
         return ((j)this.a.get(0)).c();
      } else {
         throw new IllegalStateException();
      }
   }

   public long d() {
      if(this.a.size() == 1) {
         return ((j)this.a.get(0)).d();
      } else {
         throw new IllegalStateException();
      }
   }

   public int e() {
      if(this.a.size() == 1) {
         return ((j)this.a.get(0)).e();
      } else {
         throw new IllegalStateException();
      }
   }

   public boolean equals(Object var1) {
      boolean var2;
      if(var1 != this && (!(var1 instanceof g) || !((g)var1).a.equals(this.a))) {
         var2 = false;
      } else {
         var2 = true;
      }

      return var2;
   }

   public boolean f() {
      if(this.a.size() == 1) {
         return ((j)this.a.get(0)).f();
      } else {
         throw new IllegalStateException();
      }
   }

   public int hashCode() {
      return this.a.hashCode();
   }

   public Iterator iterator() {
      return this.a.iterator();
   }
}
