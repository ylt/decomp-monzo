package io.reactivex;

import io.reactivex.d.e.d.aa;
import io.reactivex.d.e.d.ab;
import io.reactivex.d.e.d.ac;
import io.reactivex.d.e.d.ad;
import io.reactivex.d.e.d.ae;
import io.reactivex.d.e.d.af;
import io.reactivex.d.e.d.ag;
import io.reactivex.d.e.d.ah;
import io.reactivex.d.e.d.ai;
import io.reactivex.d.e.d.aj;
import io.reactivex.d.e.d.ak;
import io.reactivex.d.e.d.al;
import io.reactivex.d.e.d.am;
import io.reactivex.d.e.d.ao;
import io.reactivex.d.e.d.ap;
import io.reactivex.d.e.d.aq;
import io.reactivex.d.e.d.ar;
import io.reactivex.d.e.d.as;
import io.reactivex.d.e.d.at;
import io.reactivex.d.e.d.av;
import io.reactivex.d.e.d.aw;
import io.reactivex.d.e.d.ax;
import io.reactivex.d.e.d.ay;
import io.reactivex.d.e.d.az;
import io.reactivex.d.e.d.ba;
import io.reactivex.d.e.d.bb;
import io.reactivex.d.e.d.bc;
import io.reactivex.d.e.d.bd;
import io.reactivex.d.e.d.be;
import io.reactivex.d.e.d.bf;
import io.reactivex.d.e.d.bg;
import io.reactivex.d.e.d.bh;
import io.reactivex.d.e.d.bi;
import io.reactivex.d.e.d.bj;
import io.reactivex.d.e.d.bk;
import io.reactivex.d.e.d.bl;
import io.reactivex.d.e.d.bm;
import io.reactivex.d.e.d.bn;
import io.reactivex.d.e.d.bo;
import io.reactivex.d.e.d.bp;
import io.reactivex.d.e.d.bq;
import io.reactivex.d.e.d.br;
import io.reactivex.d.e.d.bs;
import io.reactivex.d.e.d.bt;
import io.reactivex.d.e.d.bu;
import io.reactivex.d.e.d.bv;
import io.reactivex.d.e.d.bw;
import io.reactivex.d.e.d.bx;
import io.reactivex.d.e.d.by;
import io.reactivex.d.e.d.bz;
import io.reactivex.d.e.d.ca;
import io.reactivex.d.e.d.cb;
import io.reactivex.d.e.d.cc;
import io.reactivex.d.e.d.cd;
import io.reactivex.d.e.d.ce;
import io.reactivex.d.e.d.cf;
import io.reactivex.d.e.d.cg;
import io.reactivex.d.e.d.ci;
import io.reactivex.d.e.d.cj;
import io.reactivex.d.e.d.ck;
import io.reactivex.d.e.d.cl;
import io.reactivex.d.e.d.cm;
import io.reactivex.d.e.d.cn;
import io.reactivex.d.e.d.co;
import io.reactivex.d.e.d.cp;
import io.reactivex.d.e.d.cq;
import io.reactivex.d.e.d.cr;
import io.reactivex.d.e.d.cs;
import io.reactivex.d.e.d.ct;
import io.reactivex.d.e.d.cv;
import io.reactivex.d.e.d.cw;
import io.reactivex.d.e.d.cx;
import io.reactivex.d.e.d.cy;
import io.reactivex.d.e.d.cz;
import io.reactivex.d.e.d.da;
import io.reactivex.d.e.d.db;
import io.reactivex.d.e.d.dc;
import io.reactivex.d.e.d.dd;
import io.reactivex.d.e.d.de;
import io.reactivex.d.e.d.df;
import io.reactivex.d.e.d.dg;
import io.reactivex.d.e.d.dh;
import io.reactivex.d.e.d.di;
import io.reactivex.d.e.d.dj;
import io.reactivex.d.e.d.dk;
import io.reactivex.d.e.d.dl;
import io.reactivex.d.e.d.dm;
import io.reactivex.d.e.d.dn;
import io.reactivex.d.e.d.do;
import io.reactivex.d.e.d.dp;
import io.reactivex.d.e.d.dq;
import io.reactivex.d.e.d.dr;
import io.reactivex.d.e.d.ds;
import io.reactivex.d.e.d.dt;
import io.reactivex.d.e.d.du;
import io.reactivex.d.e.d.dv;
import io.reactivex.d.e.d.dw;
import io.reactivex.d.e.d.dx;
import io.reactivex.d.e.d.dy;
import io.reactivex.d.e.d.dz;
import io.reactivex.d.e.d.ea;
import io.reactivex.d.e.d.eb;
import io.reactivex.d.e.d.ec;
import io.reactivex.d.e.d.ed;
import io.reactivex.d.e.d.ee;
import io.reactivex.d.e.d.ef;
import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public abstract class n implements r {
   public static n amb(Iterable var0) {
      io.reactivex.d.b.b.a(var0, (String)"sources is null");
      return io.reactivex.g.a.a((n)(new io.reactivex.d.e.d.h((r[])null, var0)));
   }

   public static n ambArray(r... var0) {
      io.reactivex.d.b.b.a(var0, (String)"sources is null");
      int var1 = var0.length;
      n var2;
      if(var1 == 0) {
         var2 = empty();
      } else if(var1 == 1) {
         var2 = wrap(var0[0]);
      } else {
         var2 = io.reactivex.g.a.a((n)(new io.reactivex.d.e.d.h(var0, (Iterable)null)));
      }

      return var2;
   }

   public static int bufferSize() {
      return f.a();
   }

   public static n combineLatest(io.reactivex.c.h var0, int var1, r... var2) {
      return combineLatest(var2, var0, var1);
   }

   public static n combineLatest(r var0, r var1, io.reactivex.c.c var2) {
      io.reactivex.d.b.b.a(var0, (String)"source1 is null");
      io.reactivex.d.b.b.a(var1, (String)"source2 is null");
      return combineLatest(io.reactivex.d.b.a.a(var2), bufferSize(), new r[]{var0, var1});
   }

   public static n combineLatest(r var0, r var1, r var2, io.reactivex.c.i var3) {
      io.reactivex.d.b.b.a(var0, (String)"source1 is null");
      io.reactivex.d.b.b.a(var1, (String)"source2 is null");
      io.reactivex.d.b.b.a(var2, (String)"source3 is null");
      return combineLatest(io.reactivex.d.b.a.a(var3), bufferSize(), new r[]{var0, var1, var2});
   }

   public static n combineLatest(r var0, r var1, r var2, r var3, io.reactivex.c.j var4) {
      io.reactivex.d.b.b.a(var0, (String)"source1 is null");
      io.reactivex.d.b.b.a(var1, (String)"source2 is null");
      io.reactivex.d.b.b.a(var2, (String)"source3 is null");
      io.reactivex.d.b.b.a(var3, (String)"source4 is null");
      return combineLatest(io.reactivex.d.b.a.a(var4), bufferSize(), new r[]{var0, var1, var2, var3});
   }

   public static n combineLatest(r var0, r var1, r var2, r var3, r var4, io.reactivex.c.k var5) {
      io.reactivex.d.b.b.a(var0, (String)"source1 is null");
      io.reactivex.d.b.b.a(var1, (String)"source2 is null");
      io.reactivex.d.b.b.a(var2, (String)"source3 is null");
      io.reactivex.d.b.b.a(var3, (String)"source4 is null");
      io.reactivex.d.b.b.a(var4, (String)"source5 is null");
      return combineLatest(io.reactivex.d.b.a.a(var5), bufferSize(), new r[]{var0, var1, var2, var3, var4});
   }

   public static n combineLatest(r var0, r var1, r var2, r var3, r var4, r var5, io.reactivex.c.l var6) {
      io.reactivex.d.b.b.a(var0, (String)"source1 is null");
      io.reactivex.d.b.b.a(var1, (String)"source2 is null");
      io.reactivex.d.b.b.a(var2, (String)"source3 is null");
      io.reactivex.d.b.b.a(var3, (String)"source4 is null");
      io.reactivex.d.b.b.a(var4, (String)"source5 is null");
      io.reactivex.d.b.b.a(var5, (String)"source6 is null");
      return combineLatest(io.reactivex.d.b.a.a(var6), bufferSize(), new r[]{var0, var1, var2, var3, var4, var5});
   }

   public static n combineLatest(r var0, r var1, r var2, r var3, r var4, r var5, r var6, io.reactivex.c.m var7) {
      io.reactivex.d.b.b.a(var0, (String)"source1 is null");
      io.reactivex.d.b.b.a(var1, (String)"source2 is null");
      io.reactivex.d.b.b.a(var2, (String)"source3 is null");
      io.reactivex.d.b.b.a(var3, (String)"source4 is null");
      io.reactivex.d.b.b.a(var4, (String)"source5 is null");
      io.reactivex.d.b.b.a(var5, (String)"source6 is null");
      io.reactivex.d.b.b.a(var6, (String)"source7 is null");
      return combineLatest(io.reactivex.d.b.a.a(var7), bufferSize(), new r[]{var0, var1, var2, var3, var4, var5, var6});
   }

   public static n combineLatest(r var0, r var1, r var2, r var3, r var4, r var5, r var6, r var7, io.reactivex.c.n var8) {
      io.reactivex.d.b.b.a(var0, (String)"source1 is null");
      io.reactivex.d.b.b.a(var1, (String)"source2 is null");
      io.reactivex.d.b.b.a(var2, (String)"source3 is null");
      io.reactivex.d.b.b.a(var3, (String)"source4 is null");
      io.reactivex.d.b.b.a(var4, (String)"source5 is null");
      io.reactivex.d.b.b.a(var5, (String)"source6 is null");
      io.reactivex.d.b.b.a(var6, (String)"source7 is null");
      io.reactivex.d.b.b.a(var7, (String)"source8 is null");
      return combineLatest(io.reactivex.d.b.a.a(var8), bufferSize(), new r[]{var0, var1, var2, var3, var4, var5, var6, var7});
   }

   public static n combineLatest(r var0, r var1, r var2, r var3, r var4, r var5, r var6, r var7, r var8, io.reactivex.c.o var9) {
      io.reactivex.d.b.b.a(var0, (String)"source1 is null");
      io.reactivex.d.b.b.a(var1, (String)"source2 is null");
      io.reactivex.d.b.b.a(var2, (String)"source3 is null");
      io.reactivex.d.b.b.a(var3, (String)"source4 is null");
      io.reactivex.d.b.b.a(var4, (String)"source5 is null");
      io.reactivex.d.b.b.a(var5, (String)"source6 is null");
      io.reactivex.d.b.b.a(var6, (String)"source7 is null");
      io.reactivex.d.b.b.a(var7, (String)"source8 is null");
      io.reactivex.d.b.b.a(var8, (String)"source9 is null");
      return combineLatest(io.reactivex.d.b.a.a(var9), bufferSize(), new r[]{var0, var1, var2, var3, var4, var5, var6, var7, var8});
   }

   public static n combineLatest(Iterable var0, io.reactivex.c.h var1) {
      return combineLatest(var0, var1, bufferSize());
   }

   public static n combineLatest(Iterable var0, io.reactivex.c.h var1, int var2) {
      io.reactivex.d.b.b.a(var0, (String)"sources is null");
      io.reactivex.d.b.b.a(var1, (String)"combiner is null");
      io.reactivex.d.b.b.a(var2, "bufferSize");
      return io.reactivex.g.a.a((n)(new io.reactivex.d.e.d.u((r[])null, var0, var1, var2 << 1, false)));
   }

   public static n combineLatest(r[] var0, io.reactivex.c.h var1) {
      return combineLatest(var0, var1, bufferSize());
   }

   public static n combineLatest(r[] var0, io.reactivex.c.h var1, int var2) {
      io.reactivex.d.b.b.a(var0, (String)"sources is null");
      n var3;
      if(var0.length == 0) {
         var3 = empty();
      } else {
         io.reactivex.d.b.b.a(var1, (String)"combiner is null");
         io.reactivex.d.b.b.a(var2, "bufferSize");
         var3 = io.reactivex.g.a.a((n)(new io.reactivex.d.e.d.u(var0, (Iterable)null, var1, var2 << 1, false)));
      }

      return var3;
   }

   public static n combineLatestDelayError(io.reactivex.c.h var0, int var1, r... var2) {
      return combineLatestDelayError(var2, var0, var1);
   }

   public static n combineLatestDelayError(Iterable var0, io.reactivex.c.h var1) {
      return combineLatestDelayError(var0, var1, bufferSize());
   }

   public static n combineLatestDelayError(Iterable var0, io.reactivex.c.h var1, int var2) {
      io.reactivex.d.b.b.a(var0, (String)"sources is null");
      io.reactivex.d.b.b.a(var1, (String)"combiner is null");
      io.reactivex.d.b.b.a(var2, "bufferSize");
      return io.reactivex.g.a.a((n)(new io.reactivex.d.e.d.u((r[])null, var0, var1, var2 << 1, true)));
   }

   public static n combineLatestDelayError(r[] var0, io.reactivex.c.h var1) {
      return combineLatestDelayError(var0, var1, bufferSize());
   }

   public static n combineLatestDelayError(r[] var0, io.reactivex.c.h var1, int var2) {
      io.reactivex.d.b.b.a(var2, "bufferSize");
      io.reactivex.d.b.b.a(var1, (String)"combiner is null");
      n var3;
      if(var0.length == 0) {
         var3 = empty();
      } else {
         var3 = io.reactivex.g.a.a((n)(new io.reactivex.d.e.d.u(var0, (Iterable)null, var1, var2 << 1, true)));
      }

      return var3;
   }

   public static n concat(r var0) {
      return concat(var0, bufferSize());
   }

   public static n concat(r var0, int var1) {
      io.reactivex.d.b.b.a(var0, (String)"sources is null");
      io.reactivex.d.b.b.a(var1, "prefetch");
      return io.reactivex.g.a.a((n)(new io.reactivex.d.e.d.v(var0, io.reactivex.d.b.a.a(), var1, io.reactivex.d.j.i.a)));
   }

   public static n concat(r var0, r var1) {
      io.reactivex.d.b.b.a(var0, (String)"source1 is null");
      io.reactivex.d.b.b.a(var1, (String)"source2 is null");
      return concatArray(new r[]{var0, var1});
   }

   public static n concat(r var0, r var1, r var2) {
      io.reactivex.d.b.b.a(var0, (String)"source1 is null");
      io.reactivex.d.b.b.a(var1, (String)"source2 is null");
      io.reactivex.d.b.b.a(var2, (String)"source3 is null");
      return concatArray(new r[]{var0, var1, var2});
   }

   public static n concat(r var0, r var1, r var2, r var3) {
      io.reactivex.d.b.b.a(var0, (String)"source1 is null");
      io.reactivex.d.b.b.a(var1, (String)"source2 is null");
      io.reactivex.d.b.b.a(var2, (String)"source3 is null");
      io.reactivex.d.b.b.a(var3, (String)"source4 is null");
      return concatArray(new r[]{var0, var1, var2, var3});
   }

   public static n concat(Iterable var0) {
      io.reactivex.d.b.b.a(var0, (String)"sources is null");
      return fromIterable(var0).concatMapDelayError(io.reactivex.d.b.a.a(), bufferSize(), false);
   }

   public static n concatArray(r... var0) {
      n var1;
      if(var0.length == 0) {
         var1 = empty();
      } else if(var0.length == 1) {
         var1 = wrap(var0[0]);
      } else {
         var1 = io.reactivex.g.a.a((n)(new io.reactivex.d.e.d.v(fromArray(var0), io.reactivex.d.b.a.a(), bufferSize(), io.reactivex.d.j.i.b)));
      }

      return var1;
   }

   public static n concatArrayDelayError(r... var0) {
      n var1;
      if(var0.length == 0) {
         var1 = empty();
      } else if(var0.length == 1) {
         var1 = wrap(var0[0]);
      } else {
         var1 = concatDelayError((r)fromArray(var0));
      }

      return var1;
   }

   public static n concatArrayEager(int var0, int var1, r... var2) {
      return fromArray(var2).concatMapEagerDelayError(io.reactivex.d.b.a.a(), var0, var1, false);
   }

   public static n concatArrayEager(r... var0) {
      return concatArrayEager(bufferSize(), bufferSize(), var0);
   }

   public static n concatDelayError(r var0) {
      return concatDelayError(var0, bufferSize(), true);
   }

   public static n concatDelayError(r var0, int var1, boolean var2) {
      io.reactivex.d.b.b.a(var0, (String)"sources is null");
      io.reactivex.d.b.b.a(var1, "prefetch is null");
      io.reactivex.c.h var4 = io.reactivex.d.b.a.a();
      io.reactivex.d.j.i var3;
      if(var2) {
         var3 = io.reactivex.d.j.i.c;
      } else {
         var3 = io.reactivex.d.j.i.b;
      }

      return io.reactivex.g.a.a((n)(new io.reactivex.d.e.d.v(var0, var4, var1, var3)));
   }

   public static n concatDelayError(Iterable var0) {
      io.reactivex.d.b.b.a(var0, (String)"sources is null");
      return concatDelayError((r)fromIterable(var0));
   }

   public static n concatEager(r var0) {
      return concatEager(var0, bufferSize(), bufferSize());
   }

   public static n concatEager(r var0, int var1, int var2) {
      io.reactivex.d.b.b.a(Integer.valueOf(var1), (String)"maxConcurrency is null");
      io.reactivex.d.b.b.a(Integer.valueOf(var2), (String)"prefetch is null");
      return wrap(var0).concatMapEager(io.reactivex.d.b.a.a(), var1, var2);
   }

   public static n concatEager(Iterable var0) {
      return concatEager(var0, bufferSize(), bufferSize());
   }

   public static n concatEager(Iterable var0, int var1, int var2) {
      io.reactivex.d.b.b.a(Integer.valueOf(var1), (String)"maxConcurrency is null");
      io.reactivex.d.b.b.a(Integer.valueOf(var2), (String)"prefetch is null");
      return fromIterable(var0).concatMapEagerDelayError(io.reactivex.d.b.a.a(), var1, var2, false);
   }

   public static n create(p var0) {
      io.reactivex.d.b.b.a(var0, (String)"source is null");
      return io.reactivex.g.a.a((n)(new io.reactivex.d.e.d.z(var0)));
   }

   public static n defer(Callable var0) {
      io.reactivex.d.b.b.a(var0, (String)"supplier is null");
      return io.reactivex.g.a.a((n)(new ac(var0)));
   }

   private n doOnEach(io.reactivex.c.g var1, io.reactivex.c.g var2, io.reactivex.c.a var3, io.reactivex.c.a var4) {
      io.reactivex.d.b.b.a(var1, (String)"onNext is null");
      io.reactivex.d.b.b.a(var2, (String)"onError is null");
      io.reactivex.d.b.b.a(var3, (String)"onComplete is null");
      io.reactivex.d.b.b.a(var4, (String)"onAfterTerminate is null");
      return io.reactivex.g.a.a((n)(new al(this, var1, var2, var3, var4)));
   }

   public static n empty() {
      return io.reactivex.g.a.a(aq.a);
   }

   public static n error(Throwable var0) {
      io.reactivex.d.b.b.a(var0, (String)"e is null");
      return error(io.reactivex.d.b.a.a((Object)var0));
   }

   public static n error(Callable var0) {
      io.reactivex.d.b.b.a(var0, (String)"errorSupplier is null");
      return io.reactivex.g.a.a((n)(new ar(var0)));
   }

   public static n fromArray(Object... var0) {
      io.reactivex.d.b.b.a(var0, (String)"items is null");
      n var1;
      if(var0.length == 0) {
         var1 = empty();
      } else if(var0.length == 1) {
         var1 = just(var0[0]);
      } else {
         var1 = io.reactivex.g.a.a((n)(new az(var0)));
      }

      return var1;
   }

   public static n fromCallable(Callable var0) {
      io.reactivex.d.b.b.a(var0, (String)"supplier is null");
      return io.reactivex.g.a.a((n)(new ba(var0)));
   }

   public static n fromFuture(Future var0) {
      io.reactivex.d.b.b.a(var0, (String)"future is null");
      return io.reactivex.g.a.a((n)(new bb(var0, 0L, (TimeUnit)null)));
   }

   public static n fromFuture(Future var0, long var1, TimeUnit var3) {
      io.reactivex.d.b.b.a(var0, (String)"future is null");
      io.reactivex.d.b.b.a(var3, (String)"unit is null");
      return io.reactivex.g.a.a((n)(new bb(var0, var1, var3)));
   }

   public static n fromFuture(Future var0, long var1, TimeUnit var3, u var4) {
      io.reactivex.d.b.b.a(var4, (String)"scheduler is null");
      return fromFuture(var0, var1, var3).subscribeOn(var4);
   }

   public static n fromFuture(Future var0, u var1) {
      io.reactivex.d.b.b.a(var1, (String)"scheduler is null");
      return fromFuture(var0).subscribeOn(var1);
   }

   public static n fromIterable(Iterable var0) {
      io.reactivex.d.b.b.a(var0, (String)"source is null");
      return io.reactivex.g.a.a((n)(new bc(var0)));
   }

   public static n fromPublisher(org.a.a var0) {
      io.reactivex.d.b.b.a(var0, (String)"publisher is null");
      return io.reactivex.g.a.a((n)(new bd(var0)));
   }

   public static n generate(io.reactivex.c.g var0) {
      io.reactivex.d.b.b.a(var0, (String)"generator  is null");
      return generate(io.reactivex.d.b.a.e(), bl.a(var0), io.reactivex.d.b.a.b());
   }

   public static n generate(Callable var0, io.reactivex.c.b var1) {
      io.reactivex.d.b.b.a(var1, (String)"generator  is null");
      return generate(var0, bl.a(var1), io.reactivex.d.b.a.b());
   }

   public static n generate(Callable var0, io.reactivex.c.b var1, io.reactivex.c.g var2) {
      io.reactivex.d.b.b.a(var1, (String)"generator  is null");
      return generate(var0, bl.a(var1), var2);
   }

   public static n generate(Callable var0, io.reactivex.c.c var1) {
      return generate(var0, var1, io.reactivex.d.b.a.b());
   }

   public static n generate(Callable var0, io.reactivex.c.c var1, io.reactivex.c.g var2) {
      io.reactivex.d.b.b.a(var0, (String)"initialState is null");
      io.reactivex.d.b.b.a(var1, (String)"generator  is null");
      io.reactivex.d.b.b.a(var2, (String)"disposeState is null");
      return io.reactivex.g.a.a((n)(new bf(var0, var1, var2)));
   }

   public static n interval(long var0, long var2, TimeUnit var4) {
      return interval(var0, var2, var4, io.reactivex.h.a.a());
   }

   public static n interval(long var0, long var2, TimeUnit var4, u var5) {
      io.reactivex.d.b.b.a(var4, (String)"unit is null");
      io.reactivex.d.b.b.a(var5, (String)"scheduler is null");
      return io.reactivex.g.a.a((n)(new bm(Math.max(0L, var0), Math.max(0L, var2), var4, var5)));
   }

   public static n interval(long var0, TimeUnit var2) {
      return interval(var0, var0, var2, io.reactivex.h.a.a());
   }

   public static n interval(long var0, TimeUnit var2, u var3) {
      return interval(var0, var0, var2, var3);
   }

   public static n intervalRange(long var0, long var2, long var4, long var6, TimeUnit var8) {
      return intervalRange(var0, var2, var4, var6, var8, io.reactivex.h.a.a());
   }

   public static n intervalRange(long var0, long var2, long var4, long var6, TimeUnit var8, u var9) {
      if(var2 < 0L) {
         throw new IllegalArgumentException("count >= 0 required but it was " + var2);
      } else {
         n var10;
         if(var2 == 0L) {
            var10 = empty().delay(var4, var8, var9);
         } else {
            var2 = var0 + (var2 - 1L);
            if(var0 > 0L && var2 < 0L) {
               throw new IllegalArgumentException("Overflow! start + count is bigger than Long.MAX_VALUE");
            }

            io.reactivex.d.b.b.a(var8, (String)"unit is null");
            io.reactivex.d.b.b.a(var9, (String)"scheduler is null");
            var10 = io.reactivex.g.a.a((n)(new bn(var0, var2, Math.max(0L, var4), Math.max(0L, var6), var8, var9)));
         }

         return var10;
      }
   }

   public static n just(Object var0) {
      io.reactivex.d.b.b.a(var0, "The item is null");
      return io.reactivex.g.a.a((n)(new bp(var0)));
   }

   public static n just(Object var0, Object var1) {
      io.reactivex.d.b.b.a(var0, "The first item is null");
      io.reactivex.d.b.b.a(var1, "The second item is null");
      return fromArray(new Object[]{var0, var1});
   }

   public static n just(Object var0, Object var1, Object var2) {
      io.reactivex.d.b.b.a(var0, "The first item is null");
      io.reactivex.d.b.b.a(var1, "The second item is null");
      io.reactivex.d.b.b.a(var2, "The third item is null");
      return fromArray(new Object[]{var0, var1, var2});
   }

   public static n just(Object var0, Object var1, Object var2, Object var3) {
      io.reactivex.d.b.b.a(var0, "The first item is null");
      io.reactivex.d.b.b.a(var1, "The second item is null");
      io.reactivex.d.b.b.a(var2, "The third item is null");
      io.reactivex.d.b.b.a(var3, "The fourth item is null");
      return fromArray(new Object[]{var0, var1, var2, var3});
   }

   public static n just(Object var0, Object var1, Object var2, Object var3, Object var4) {
      io.reactivex.d.b.b.a(var0, "The first item is null");
      io.reactivex.d.b.b.a(var1, "The second item is null");
      io.reactivex.d.b.b.a(var2, "The third item is null");
      io.reactivex.d.b.b.a(var3, "The fourth item is null");
      io.reactivex.d.b.b.a(var4, "The fifth item is null");
      return fromArray(new Object[]{var0, var1, var2, var3, var4});
   }

   public static n just(Object var0, Object var1, Object var2, Object var3, Object var4, Object var5) {
      io.reactivex.d.b.b.a(var0, "The first item is null");
      io.reactivex.d.b.b.a(var1, "The second item is null");
      io.reactivex.d.b.b.a(var2, "The third item is null");
      io.reactivex.d.b.b.a(var3, "The fourth item is null");
      io.reactivex.d.b.b.a(var4, "The fifth item is null");
      io.reactivex.d.b.b.a(var5, "The sixth item is null");
      return fromArray(new Object[]{var0, var1, var2, var3, var4, var5});
   }

   public static n just(Object var0, Object var1, Object var2, Object var3, Object var4, Object var5, Object var6) {
      io.reactivex.d.b.b.a(var0, "The first item is null");
      io.reactivex.d.b.b.a(var1, "The second item is null");
      io.reactivex.d.b.b.a(var2, "The third item is null");
      io.reactivex.d.b.b.a(var3, "The fourth item is null");
      io.reactivex.d.b.b.a(var4, "The fifth item is null");
      io.reactivex.d.b.b.a(var5, "The sixth item is null");
      io.reactivex.d.b.b.a(var6, "The seventh item is null");
      return fromArray(new Object[]{var0, var1, var2, var3, var4, var5, var6});
   }

   public static n just(Object var0, Object var1, Object var2, Object var3, Object var4, Object var5, Object var6, Object var7) {
      io.reactivex.d.b.b.a(var0, "The first item is null");
      io.reactivex.d.b.b.a(var1, "The second item is null");
      io.reactivex.d.b.b.a(var2, "The third item is null");
      io.reactivex.d.b.b.a(var3, "The fourth item is null");
      io.reactivex.d.b.b.a(var4, "The fifth item is null");
      io.reactivex.d.b.b.a(var5, "The sixth item is null");
      io.reactivex.d.b.b.a(var6, "The seventh item is null");
      io.reactivex.d.b.b.a(var7, "The eighth item is null");
      return fromArray(new Object[]{var0, var1, var2, var3, var4, var5, var6, var7});
   }

   public static n just(Object var0, Object var1, Object var2, Object var3, Object var4, Object var5, Object var6, Object var7, Object var8) {
      io.reactivex.d.b.b.a(var0, "The first item is null");
      io.reactivex.d.b.b.a(var1, "The second item is null");
      io.reactivex.d.b.b.a(var2, "The third item is null");
      io.reactivex.d.b.b.a(var3, "The fourth item is null");
      io.reactivex.d.b.b.a(var4, "The fifth item is null");
      io.reactivex.d.b.b.a(var5, "The sixth item is null");
      io.reactivex.d.b.b.a(var6, "The seventh item is null");
      io.reactivex.d.b.b.a(var7, "The eighth item is null");
      io.reactivex.d.b.b.a(var8, "The ninth item is null");
      return fromArray(new Object[]{var0, var1, var2, var3, var4, var5, var6, var7, var8});
   }

   public static n just(Object var0, Object var1, Object var2, Object var3, Object var4, Object var5, Object var6, Object var7, Object var8, Object var9) {
      io.reactivex.d.b.b.a(var0, "The first item is null");
      io.reactivex.d.b.b.a(var1, "The second item is null");
      io.reactivex.d.b.b.a(var2, "The third item is null");
      io.reactivex.d.b.b.a(var3, "The fourth item is null");
      io.reactivex.d.b.b.a(var4, "The fifth item is null");
      io.reactivex.d.b.b.a(var5, "The sixth item is null");
      io.reactivex.d.b.b.a(var6, "The seventh item is null");
      io.reactivex.d.b.b.a(var7, "The eighth item is null");
      io.reactivex.d.b.b.a(var8, "The ninth item is null");
      io.reactivex.d.b.b.a(var9, "The tenth item is null");
      return fromArray(new Object[]{var0, var1, var2, var3, var4, var5, var6, var7, var8, var9});
   }

   public static n merge(r var0) {
      io.reactivex.d.b.b.a(var0, (String)"sources is null");
      return io.reactivex.g.a.a((n)(new at(var0, io.reactivex.d.b.a.a(), false, Integer.MAX_VALUE, bufferSize())));
   }

   public static n merge(r var0, int var1) {
      io.reactivex.d.b.b.a(var0, (String)"sources is null");
      io.reactivex.d.b.b.a(var1, "maxConcurrency");
      return io.reactivex.g.a.a((n)(new at(var0, io.reactivex.d.b.a.a(), false, var1, bufferSize())));
   }

   public static n merge(r var0, r var1) {
      io.reactivex.d.b.b.a(var0, (String)"source1 is null");
      io.reactivex.d.b.b.a(var1, (String)"source2 is null");
      return fromArray(new r[]{var0, var1}).flatMap(io.reactivex.d.b.a.a(), false, 2);
   }

   public static n merge(r var0, r var1, r var2) {
      io.reactivex.d.b.b.a(var0, (String)"source1 is null");
      io.reactivex.d.b.b.a(var1, (String)"source2 is null");
      io.reactivex.d.b.b.a(var2, (String)"source3 is null");
      return fromArray(new r[]{var0, var1, var2}).flatMap(io.reactivex.d.b.a.a(), false, 3);
   }

   public static n merge(r var0, r var1, r var2, r var3) {
      io.reactivex.d.b.b.a(var0, (String)"source1 is null");
      io.reactivex.d.b.b.a(var1, (String)"source2 is null");
      io.reactivex.d.b.b.a(var2, (String)"source3 is null");
      io.reactivex.d.b.b.a(var3, (String)"source4 is null");
      return fromArray(new r[]{var0, var1, var2, var3}).flatMap(io.reactivex.d.b.a.a(), false, 4);
   }

   public static n merge(Iterable var0) {
      return fromIterable(var0).flatMap(io.reactivex.d.b.a.a());
   }

   public static n merge(Iterable var0, int var1) {
      return fromIterable(var0).flatMap(io.reactivex.d.b.a.a(), var1);
   }

   public static n merge(Iterable var0, int var1, int var2) {
      return fromIterable(var0).flatMap(io.reactivex.d.b.a.a(), false, var1, var2);
   }

   public static n mergeArray(int var0, int var1, r... var2) {
      return fromArray(var2).flatMap(io.reactivex.d.b.a.a(), false, var0, var1);
   }

   public static n mergeArray(r... var0) {
      return fromArray(var0).flatMap(io.reactivex.d.b.a.a(), var0.length);
   }

   public static n mergeArrayDelayError(int var0, int var1, r... var2) {
      return fromArray(var2).flatMap(io.reactivex.d.b.a.a(), true, var0, var1);
   }

   public static n mergeArrayDelayError(r... var0) {
      return fromArray(var0).flatMap(io.reactivex.d.b.a.a(), true, var0.length);
   }

   public static n mergeDelayError(r var0) {
      io.reactivex.d.b.b.a(var0, (String)"sources is null");
      return io.reactivex.g.a.a((n)(new at(var0, io.reactivex.d.b.a.a(), true, Integer.MAX_VALUE, bufferSize())));
   }

   public static n mergeDelayError(r var0, int var1) {
      io.reactivex.d.b.b.a(var0, (String)"sources is null");
      io.reactivex.d.b.b.a(var1, "maxConcurrency");
      return io.reactivex.g.a.a((n)(new at(var0, io.reactivex.d.b.a.a(), true, var1, bufferSize())));
   }

   public static n mergeDelayError(r var0, r var1) {
      io.reactivex.d.b.b.a(var0, (String)"source1 is null");
      io.reactivex.d.b.b.a(var1, (String)"source2 is null");
      return fromArray(new r[]{var0, var1}).flatMap(io.reactivex.d.b.a.a(), true, 2);
   }

   public static n mergeDelayError(r var0, r var1, r var2) {
      io.reactivex.d.b.b.a(var0, (String)"source1 is null");
      io.reactivex.d.b.b.a(var1, (String)"source2 is null");
      io.reactivex.d.b.b.a(var2, (String)"source3 is null");
      return fromArray(new r[]{var0, var1, var2}).flatMap(io.reactivex.d.b.a.a(), true, 3);
   }

   public static n mergeDelayError(r var0, r var1, r var2, r var3) {
      io.reactivex.d.b.b.a(var0, (String)"source1 is null");
      io.reactivex.d.b.b.a(var1, (String)"source2 is null");
      io.reactivex.d.b.b.a(var2, (String)"source3 is null");
      io.reactivex.d.b.b.a(var3, (String)"source4 is null");
      return fromArray(new r[]{var0, var1, var2, var3}).flatMap(io.reactivex.d.b.a.a(), true, 4);
   }

   public static n mergeDelayError(Iterable var0) {
      return fromIterable(var0).flatMap(io.reactivex.d.b.a.a(), true);
   }

   public static n mergeDelayError(Iterable var0, int var1) {
      return fromIterable(var0).flatMap(io.reactivex.d.b.a.a(), true, var1);
   }

   public static n mergeDelayError(Iterable var0, int var1, int var2) {
      return fromIterable(var0).flatMap(io.reactivex.d.b.a.a(), true, var1, var2);
   }

   public static n never() {
      return io.reactivex.g.a.a(bw.a);
   }

   public static n range(int var0, int var1) {
      if(var1 < 0) {
         throw new IllegalArgumentException("count >= 0 required but it was " + var1);
      } else {
         n var2;
         if(var1 == 0) {
            var2 = empty();
         } else if(var1 == 1) {
            var2 = just(Integer.valueOf(var0));
         } else {
            if((long)var0 + (long)(var1 - 1) > 2147483647L) {
               throw new IllegalArgumentException("Integer overflow");
            }

            var2 = io.reactivex.g.a.a((n)(new cc(var0, var1)));
         }

         return var2;
      }
   }

   public static n rangeLong(long var0, long var2) {
      if(var2 < 0L) {
         throw new IllegalArgumentException("count >= 0 required but it was " + var2);
      } else {
         n var4;
         if(var2 == 0L) {
            var4 = empty();
         } else if(var2 == 1L) {
            var4 = just(Long.valueOf(var0));
         } else {
            if(var0 > 0L && var2 - 1L + var0 < 0L) {
               throw new IllegalArgumentException("Overflow! start + count is bigger than Long.MAX_VALUE");
            }

            var4 = io.reactivex.g.a.a((n)(new cd(var0, var2)));
         }

         return var4;
      }
   }

   public static v sequenceEqual(r var0, r var1) {
      return sequenceEqual(var0, var1, io.reactivex.d.b.b.a(), bufferSize());
   }

   public static v sequenceEqual(r var0, r var1, int var2) {
      return sequenceEqual(var0, var1, io.reactivex.d.b.b.a(), var2);
   }

   public static v sequenceEqual(r var0, r var1, io.reactivex.c.d var2) {
      return sequenceEqual(var0, var1, var2, bufferSize());
   }

   public static v sequenceEqual(r var0, r var1, io.reactivex.c.d var2, int var3) {
      io.reactivex.d.b.b.a(var0, (String)"source1 is null");
      io.reactivex.d.b.b.a(var1, (String)"source2 is null");
      io.reactivex.d.b.b.a(var2, (String)"isEqual is null");
      io.reactivex.d.b.b.a(var3, "bufferSize");
      return io.reactivex.g.a.a((v)(new cv(var0, var1, var2, var3)));
   }

   public static n switchOnNext(r var0) {
      return switchOnNext(var0, bufferSize());
   }

   public static n switchOnNext(r var0, int var1) {
      io.reactivex.d.b.b.a(var0, (String)"sources is null");
      io.reactivex.d.b.b.a(var1, "bufferSize");
      return io.reactivex.g.a.a((n)(new dg(var0, io.reactivex.d.b.a.a(), var1, false)));
   }

   public static n switchOnNextDelayError(r var0) {
      return switchOnNextDelayError(var0, bufferSize());
   }

   public static n switchOnNextDelayError(r var0, int var1) {
      io.reactivex.d.b.b.a(var0, (String)"sources is null");
      io.reactivex.d.b.b.a(var1, "prefetch");
      return io.reactivex.g.a.a((n)(new dg(var0, io.reactivex.d.b.a.a(), var1, true)));
   }

   private n timeout0(long var1, TimeUnit var3, r var4, u var5) {
      io.reactivex.d.b.b.a(var3, (String)"timeUnit is null");
      io.reactivex.d.b.b.a(var5, (String)"scheduler is null");
      return io.reactivex.g.a.a((n)(new dr(this, var1, var3, var5, var4)));
   }

   private n timeout0(r var1, io.reactivex.c.h var2, r var3) {
      io.reactivex.d.b.b.a(var2, (String)"itemTimeoutIndicator is null");
      return io.reactivex.g.a.a((n)(new dq(this, var1, var2, var3)));
   }

   public static n timer(long var0, TimeUnit var2) {
      return timer(var0, var2, io.reactivex.h.a.a());
   }

   public static n timer(long var0, TimeUnit var2, u var3) {
      io.reactivex.d.b.b.a(var2, (String)"unit is null");
      io.reactivex.d.b.b.a(var3, (String)"scheduler is null");
      return io.reactivex.g.a.a((n)(new ds(Math.max(var0, 0L), var2, var3)));
   }

   public static n unsafeCreate(r var0) {
      io.reactivex.d.b.b.a(var0, (String)"source is null");
      io.reactivex.d.b.b.a(var0, (String)"onSubscribe is null");
      if(var0 instanceof n) {
         throw new IllegalArgumentException("unsafeCreate(Observable) should be upgraded");
      } else {
         return io.reactivex.g.a.a((n)(new be(var0)));
      }
   }

   public static n using(Callable var0, io.reactivex.c.h var1, io.reactivex.c.g var2) {
      return using(var0, var1, var2, true);
   }

   public static n using(Callable var0, io.reactivex.c.h var1, io.reactivex.c.g var2, boolean var3) {
      io.reactivex.d.b.b.a(var0, (String)"resourceSupplier is null");
      io.reactivex.d.b.b.a(var1, (String)"sourceSupplier is null");
      io.reactivex.d.b.b.a(var2, (String)"disposer is null");
      return io.reactivex.g.a.a((n)(new dw(var0, var1, var2, var3)));
   }

   public static n wrap(r var0) {
      io.reactivex.d.b.b.a(var0, (String)"source is null");
      n var1;
      if(var0 instanceof n) {
         var1 = io.reactivex.g.a.a((n)var0);
      } else {
         var1 = io.reactivex.g.a.a((n)(new be(var0)));
      }

      return var1;
   }

   public static n zip(r var0, io.reactivex.c.h var1) {
      io.reactivex.d.b.b.a(var1, (String)"zipper is null");
      io.reactivex.d.b.b.a(var0, (String)"sources is null");
      return io.reactivex.g.a.a((new dt(var0, 16)).flatMap(bl.c(var1)));
   }

   public static n zip(r var0, r var1, io.reactivex.c.c var2) {
      io.reactivex.d.b.b.a(var0, (String)"source1 is null");
      io.reactivex.d.b.b.a(var1, (String)"source2 is null");
      return zipArray(io.reactivex.d.b.a.a(var2), false, bufferSize(), new r[]{var0, var1});
   }

   public static n zip(r var0, r var1, io.reactivex.c.c var2, boolean var3) {
      io.reactivex.d.b.b.a(var0, (String)"source1 is null");
      io.reactivex.d.b.b.a(var1, (String)"source2 is null");
      return zipArray(io.reactivex.d.b.a.a(var2), var3, bufferSize(), new r[]{var0, var1});
   }

   public static n zip(r var0, r var1, io.reactivex.c.c var2, boolean var3, int var4) {
      io.reactivex.d.b.b.a(var0, (String)"source1 is null");
      io.reactivex.d.b.b.a(var1, (String)"source2 is null");
      return zipArray(io.reactivex.d.b.a.a(var2), var3, var4, new r[]{var0, var1});
   }

   public static n zip(r var0, r var1, r var2, io.reactivex.c.i var3) {
      io.reactivex.d.b.b.a(var0, (String)"source1 is null");
      io.reactivex.d.b.b.a(var1, (String)"source2 is null");
      io.reactivex.d.b.b.a(var2, (String)"source3 is null");
      return zipArray(io.reactivex.d.b.a.a(var3), false, bufferSize(), new r[]{var0, var1, var2});
   }

   public static n zip(r var0, r var1, r var2, r var3, io.reactivex.c.j var4) {
      io.reactivex.d.b.b.a(var0, (String)"source1 is null");
      io.reactivex.d.b.b.a(var1, (String)"source2 is null");
      io.reactivex.d.b.b.a(var2, (String)"source3 is null");
      io.reactivex.d.b.b.a(var3, (String)"source4 is null");
      return zipArray(io.reactivex.d.b.a.a(var4), false, bufferSize(), new r[]{var0, var1, var2, var3});
   }

   public static n zip(r var0, r var1, r var2, r var3, r var4, io.reactivex.c.k var5) {
      io.reactivex.d.b.b.a(var0, (String)"source1 is null");
      io.reactivex.d.b.b.a(var1, (String)"source2 is null");
      io.reactivex.d.b.b.a(var2, (String)"source3 is null");
      io.reactivex.d.b.b.a(var3, (String)"source4 is null");
      io.reactivex.d.b.b.a(var4, (String)"source5 is null");
      return zipArray(io.reactivex.d.b.a.a(var5), false, bufferSize(), new r[]{var0, var1, var2, var3, var4});
   }

   public static n zip(r var0, r var1, r var2, r var3, r var4, r var5, io.reactivex.c.l var6) {
      io.reactivex.d.b.b.a(var0, (String)"source1 is null");
      io.reactivex.d.b.b.a(var1, (String)"source2 is null");
      io.reactivex.d.b.b.a(var2, (String)"source3 is null");
      io.reactivex.d.b.b.a(var3, (String)"source4 is null");
      io.reactivex.d.b.b.a(var4, (String)"source5 is null");
      io.reactivex.d.b.b.a(var5, (String)"source6 is null");
      return zipArray(io.reactivex.d.b.a.a(var6), false, bufferSize(), new r[]{var0, var1, var2, var3, var4, var5});
   }

   public static n zip(r var0, r var1, r var2, r var3, r var4, r var5, r var6, io.reactivex.c.m var7) {
      io.reactivex.d.b.b.a(var0, (String)"source1 is null");
      io.reactivex.d.b.b.a(var1, (String)"source2 is null");
      io.reactivex.d.b.b.a(var2, (String)"source3 is null");
      io.reactivex.d.b.b.a(var3, (String)"source4 is null");
      io.reactivex.d.b.b.a(var4, (String)"source5 is null");
      io.reactivex.d.b.b.a(var5, (String)"source6 is null");
      io.reactivex.d.b.b.a(var6, (String)"source7 is null");
      return zipArray(io.reactivex.d.b.a.a(var7), false, bufferSize(), new r[]{var0, var1, var2, var3, var4, var5, var6});
   }

   public static n zip(r var0, r var1, r var2, r var3, r var4, r var5, r var6, r var7, io.reactivex.c.n var8) {
      io.reactivex.d.b.b.a(var0, (String)"source1 is null");
      io.reactivex.d.b.b.a(var1, (String)"source2 is null");
      io.reactivex.d.b.b.a(var2, (String)"source3 is null");
      io.reactivex.d.b.b.a(var3, (String)"source4 is null");
      io.reactivex.d.b.b.a(var4, (String)"source5 is null");
      io.reactivex.d.b.b.a(var5, (String)"source6 is null");
      io.reactivex.d.b.b.a(var6, (String)"source7 is null");
      io.reactivex.d.b.b.a(var7, (String)"source8 is null");
      return zipArray(io.reactivex.d.b.a.a(var8), false, bufferSize(), new r[]{var0, var1, var2, var3, var4, var5, var6, var7});
   }

   public static n zip(r var0, r var1, r var2, r var3, r var4, r var5, r var6, r var7, r var8, io.reactivex.c.o var9) {
      io.reactivex.d.b.b.a(var0, (String)"source1 is null");
      io.reactivex.d.b.b.a(var1, (String)"source2 is null");
      io.reactivex.d.b.b.a(var2, (String)"source3 is null");
      io.reactivex.d.b.b.a(var3, (String)"source4 is null");
      io.reactivex.d.b.b.a(var4, (String)"source5 is null");
      io.reactivex.d.b.b.a(var5, (String)"source6 is null");
      io.reactivex.d.b.b.a(var6, (String)"source7 is null");
      io.reactivex.d.b.b.a(var7, (String)"source8 is null");
      io.reactivex.d.b.b.a(var8, (String)"source9 is null");
      return zipArray(io.reactivex.d.b.a.a(var9), false, bufferSize(), new r[]{var0, var1, var2, var3, var4, var5, var6, var7, var8});
   }

   public static n zip(Iterable var0, io.reactivex.c.h var1) {
      io.reactivex.d.b.b.a(var1, (String)"zipper is null");
      io.reactivex.d.b.b.a(var0, (String)"sources is null");
      return io.reactivex.g.a.a((n)(new ee((r[])null, var0, var1, bufferSize(), false)));
   }

   public static n zipArray(io.reactivex.c.h var0, boolean var1, int var2, r... var3) {
      n var4;
      if(var3.length == 0) {
         var4 = empty();
      } else {
         io.reactivex.d.b.b.a(var0, (String)"zipper is null");
         io.reactivex.d.b.b.a(var2, "bufferSize");
         var4 = io.reactivex.g.a.a((n)(new ee(var3, (Iterable)null, var0, var2, var1)));
      }

      return var4;
   }

   public static n zipIterable(Iterable var0, io.reactivex.c.h var1, boolean var2, int var3) {
      io.reactivex.d.b.b.a(var1, (String)"zipper is null");
      io.reactivex.d.b.b.a(var0, (String)"sources is null");
      io.reactivex.d.b.b.a(var3, "bufferSize");
      return io.reactivex.g.a.a((n)(new ee((r[])null, var0, var1, var3, var2)));
   }

   public final v all(io.reactivex.c.q var1) {
      io.reactivex.d.b.b.a(var1, (String)"predicate is null");
      return io.reactivex.g.a.a((v)(new io.reactivex.d.e.d.g(this, var1)));
   }

   public final n ambWith(r var1) {
      io.reactivex.d.b.b.a(var1, (String)"other is null");
      return ambArray(new r[]{this, var1});
   }

   public final v any(io.reactivex.c.q var1) {
      io.reactivex.d.b.b.a(var1, (String)"predicate is null");
      return io.reactivex.g.a.a((v)(new io.reactivex.d.e.d.j(this, var1)));
   }

   public final Object blockingFirst() {
      io.reactivex.d.d.e var1 = new io.reactivex.d.d.e();
      this.subscribe((t)var1);
      Object var2 = var1.a();
      if(var2 != null) {
         return var2;
      } else {
         throw new NoSuchElementException();
      }
   }

   public final Object blockingFirst(Object var1) {
      io.reactivex.d.d.e var2 = new io.reactivex.d.d.e();
      this.subscribe((t)var2);
      Object var3 = var2.a();
      if(var3 != null) {
         var1 = var3;
      }

      return var1;
   }

   public final void blockingForEach(io.reactivex.c.g var1) {
      Iterator var2 = this.blockingIterable().iterator();

      while(var2.hasNext()) {
         try {
            var1.a(var2.next());
         } catch (Throwable var3) {
            io.reactivex.exceptions.a.b(var3);
            ((io.reactivex.b.b)var2).dispose();
            throw io.reactivex.d.j.j.a(var3);
         }
      }

   }

   public final Iterable blockingIterable() {
      return this.blockingIterable(bufferSize());
   }

   public final Iterable blockingIterable(int var1) {
      io.reactivex.d.b.b.a(var1, "bufferSize");
      return new io.reactivex.d.e.d.b(this, var1);
   }

   public final Object blockingLast() {
      io.reactivex.d.d.f var1 = new io.reactivex.d.d.f();
      this.subscribe((t)var1);
      Object var2 = var1.a();
      if(var2 != null) {
         return var2;
      } else {
         throw new NoSuchElementException();
      }
   }

   public final Object blockingLast(Object var1) {
      io.reactivex.d.d.f var2 = new io.reactivex.d.d.f();
      this.subscribe((t)var2);
      Object var3 = var2.a();
      if(var3 != null) {
         var1 = var3;
      }

      return var1;
   }

   public final Iterable blockingLatest() {
      return new io.reactivex.d.e.d.c(this);
   }

   public final Iterable blockingMostRecent(Object var1) {
      return new io.reactivex.d.e.d.d(this, var1);
   }

   public final Iterable blockingNext() {
      return new io.reactivex.d.e.d.e(this);
   }

   public final Object blockingSingle() {
      Object var1 = this.singleElement().b();
      if(var1 == null) {
         throw new NoSuchElementException();
      } else {
         return var1;
      }
   }

   public final Object blockingSingle(Object var1) {
      return this.single(var1).b();
   }

   public final void blockingSubscribe() {
      io.reactivex.d.e.d.l.a(this);
   }

   public final void blockingSubscribe(io.reactivex.c.g var1) {
      io.reactivex.d.e.d.l.a(this, var1, io.reactivex.d.b.a.f, io.reactivex.d.b.a.c);
   }

   public final void blockingSubscribe(io.reactivex.c.g var1, io.reactivex.c.g var2) {
      io.reactivex.d.e.d.l.a(this, var1, var2, io.reactivex.d.b.a.c);
   }

   public final void blockingSubscribe(io.reactivex.c.g var1, io.reactivex.c.g var2, io.reactivex.c.a var3) {
      io.reactivex.d.e.d.l.a(this, var1, var2, var3);
   }

   public final void blockingSubscribe(t var1) {
      io.reactivex.d.e.d.l.a(this, var1);
   }

   public final n buffer(int var1) {
      return this.buffer(var1, var1);
   }

   public final n buffer(int var1, int var2) {
      return this.buffer(var1, var2, io.reactivex.d.j.b.a());
   }

   public final n buffer(int var1, int var2, Callable var3) {
      io.reactivex.d.b.b.a(var1, "count");
      io.reactivex.d.b.b.a(var2, "skip");
      io.reactivex.d.b.b.a(var3, (String)"bufferSupplier is null");
      return io.reactivex.g.a.a((n)(new io.reactivex.d.e.d.m(this, var1, var2, var3)));
   }

   public final n buffer(int var1, Callable var2) {
      return this.buffer(var1, var1, var2);
   }

   public final n buffer(long var1, long var3, TimeUnit var5) {
      return this.buffer(var1, var3, var5, io.reactivex.h.a.a(), io.reactivex.d.j.b.a());
   }

   public final n buffer(long var1, long var3, TimeUnit var5, u var6) {
      return this.buffer(var1, var3, var5, var6, io.reactivex.d.j.b.a());
   }

   public final n buffer(long var1, long var3, TimeUnit var5, u var6, Callable var7) {
      io.reactivex.d.b.b.a(var5, (String)"unit is null");
      io.reactivex.d.b.b.a(var6, (String)"scheduler is null");
      io.reactivex.d.b.b.a(var7, (String)"bufferSupplier is null");
      return io.reactivex.g.a.a((n)(new io.reactivex.d.e.d.q(this, var1, var3, var5, var6, var7, Integer.MAX_VALUE, false)));
   }

   public final n buffer(long var1, TimeUnit var3) {
      return this.buffer(var1, var3, io.reactivex.h.a.a(), Integer.MAX_VALUE);
   }

   public final n buffer(long var1, TimeUnit var3, int var4) {
      return this.buffer(var1, var3, io.reactivex.h.a.a(), var4);
   }

   public final n buffer(long var1, TimeUnit var3, u var4) {
      return this.buffer(var1, var3, var4, Integer.MAX_VALUE, io.reactivex.d.j.b.a(), false);
   }

   public final n buffer(long var1, TimeUnit var3, u var4, int var5) {
      return this.buffer(var1, var3, var4, var5, io.reactivex.d.j.b.a(), false);
   }

   public final n buffer(long var1, TimeUnit var3, u var4, int var5, Callable var6, boolean var7) {
      io.reactivex.d.b.b.a(var3, (String)"unit is null");
      io.reactivex.d.b.b.a(var4, (String)"scheduler is null");
      io.reactivex.d.b.b.a(var6, (String)"bufferSupplier is null");
      io.reactivex.d.b.b.a(var5, "count");
      return io.reactivex.g.a.a((n)(new io.reactivex.d.e.d.q(this, var1, var1, var3, var4, var6, var5, var7)));
   }

   public final n buffer(r var1) {
      return this.buffer(var1, io.reactivex.d.j.b.a());
   }

   public final n buffer(r var1, int var2) {
      io.reactivex.d.b.b.a(var2, "initialCapacity");
      return this.buffer(var1, io.reactivex.d.b.a.a(var2));
   }

   public final n buffer(r var1, io.reactivex.c.h var2) {
      return this.buffer(var1, var2, io.reactivex.d.j.b.a());
   }

   public final n buffer(r var1, io.reactivex.c.h var2, Callable var3) {
      io.reactivex.d.b.b.a(var1, (String)"openingIndicator is null");
      io.reactivex.d.b.b.a(var2, (String)"closingIndicator is null");
      io.reactivex.d.b.b.a(var3, (String)"bufferSupplier is null");
      return io.reactivex.g.a.a((n)(new io.reactivex.d.e.d.n(this, var1, var2, var3)));
   }

   public final n buffer(r var1, Callable var2) {
      io.reactivex.d.b.b.a(var1, (String)"boundary is null");
      io.reactivex.d.b.b.a(var2, (String)"bufferSupplier is null");
      return io.reactivex.g.a.a((n)(new io.reactivex.d.e.d.p(this, var1, var2)));
   }

   public final n buffer(Callable var1) {
      return this.buffer(var1, io.reactivex.d.j.b.a());
   }

   public final n buffer(Callable var1, Callable var2) {
      io.reactivex.d.b.b.a(var1, (String)"boundarySupplier is null");
      io.reactivex.d.b.b.a(var2, (String)"bufferSupplier is null");
      return io.reactivex.g.a.a((n)(new io.reactivex.d.e.d.o(this, var1, var2)));
   }

   public final n cache() {
      return io.reactivex.d.e.d.r.a(this);
   }

   public final n cacheWithInitialCapacity(int var1) {
      return io.reactivex.d.e.d.r.a(this, var1);
   }

   public final n cast(Class var1) {
      io.reactivex.d.b.b.a(var1, (String)"clazz is null");
      return this.map(io.reactivex.d.b.a.a(var1));
   }

   public final v collect(Callable var1, io.reactivex.c.b var2) {
      io.reactivex.d.b.b.a(var1, (String)"initialValueSupplier is null");
      io.reactivex.d.b.b.a(var2, (String)"collector is null");
      return io.reactivex.g.a.a((v)(new io.reactivex.d.e.d.t(this, var1, var2)));
   }

   public final v collectInto(Object var1, io.reactivex.c.b var2) {
      io.reactivex.d.b.b.a(var1, "initialValue is null");
      return this.collect(io.reactivex.d.b.a.a(var1), var2);
   }

   public final n compose(s var1) {
      return wrap(((s)io.reactivex.d.b.b.a(var1, (String)"composer is null")).a(this));
   }

   public final n concatMap(io.reactivex.c.h var1) {
      return this.concatMap(var1, 2);
   }

   public final n concatMap(io.reactivex.c.h var1, int var2) {
      io.reactivex.d.b.b.a(var1, (String)"mapper is null");
      io.reactivex.d.b.b.a(var2, "prefetch");
      n var4;
      if(this instanceof io.reactivex.d.c.g) {
         Object var3 = ((io.reactivex.d.c.g)this).call();
         if(var3 == null) {
            var4 = empty();
         } else {
            var4 = cr.a(var3, var1);
         }
      } else {
         var4 = io.reactivex.g.a.a((n)(new io.reactivex.d.e.d.v(this, var1, var2, io.reactivex.d.j.i.a)));
      }

      return var4;
   }

   public final n concatMapDelayError(io.reactivex.c.h var1) {
      return this.concatMapDelayError(var1, bufferSize(), true);
   }

   public final n concatMapDelayError(io.reactivex.c.h var1, int var2, boolean var3) {
      io.reactivex.d.b.b.a(var1, (String)"mapper is null");
      io.reactivex.d.b.b.a(var2, "prefetch");
      n var5;
      if(this instanceof io.reactivex.d.c.g) {
         Object var4 = ((io.reactivex.d.c.g)this).call();
         if(var4 == null) {
            var5 = empty();
         } else {
            var5 = cr.a(var4, var1);
         }
      } else {
         io.reactivex.d.j.i var6;
         if(var3) {
            var6 = io.reactivex.d.j.i.c;
         } else {
            var6 = io.reactivex.d.j.i.b;
         }

         var5 = io.reactivex.g.a.a((n)(new io.reactivex.d.e.d.v(this, var1, var2, var6)));
      }

      return var5;
   }

   public final n concatMapEager(io.reactivex.c.h var1) {
      return this.concatMapEager(var1, Integer.MAX_VALUE, bufferSize());
   }

   public final n concatMapEager(io.reactivex.c.h var1, int var2, int var3) {
      io.reactivex.d.b.b.a(var1, (String)"mapper is null");
      io.reactivex.d.b.b.a(var2, "maxConcurrency");
      io.reactivex.d.b.b.a(var3, "prefetch");
      return io.reactivex.g.a.a((n)(new io.reactivex.d.e.d.w(this, var1, io.reactivex.d.j.i.a, var2, var3)));
   }

   public final n concatMapEagerDelayError(io.reactivex.c.h var1, int var2, int var3, boolean var4) {
      io.reactivex.d.b.b.a(var1, (String)"mapper is null");
      io.reactivex.d.b.b.a(var2, "maxConcurrency");
      io.reactivex.d.b.b.a(var3, "prefetch");
      io.reactivex.d.j.i var5;
      if(var4) {
         var5 = io.reactivex.d.j.i.c;
      } else {
         var5 = io.reactivex.d.j.i.b;
      }

      return io.reactivex.g.a.a((n)(new io.reactivex.d.e.d.w(this, var1, var5, var2, var3)));
   }

   public final n concatMapEagerDelayError(io.reactivex.c.h var1, boolean var2) {
      return this.concatMapEagerDelayError(var1, Integer.MAX_VALUE, bufferSize(), var2);
   }

   public final n concatMapIterable(io.reactivex.c.h var1) {
      io.reactivex.d.b.b.a(var1, (String)"mapper is null");
      return io.reactivex.g.a.a((n)(new ay(this, var1)));
   }

   public final n concatMapIterable(io.reactivex.c.h var1, int var2) {
      io.reactivex.d.b.b.a(var1, (String)"mapper is null");
      io.reactivex.d.b.b.a(var2, "prefetch");
      return this.concatMap(bl.b(var1), var2);
   }

   public final n concatWith(r var1) {
      io.reactivex.d.b.b.a(var1, (String)"other is null");
      return concat(this, var1);
   }

   public final v contains(Object var1) {
      io.reactivex.d.b.b.a(var1, "element is null");
      return this.any(io.reactivex.d.b.a.c(var1));
   }

   public final v count() {
      return io.reactivex.g.a.a((v)(new io.reactivex.d.e.d.y(this)));
   }

   public final n debounce(long var1, TimeUnit var3) {
      return this.debounce(var1, var3, io.reactivex.h.a.a());
   }

   public final n debounce(long var1, TimeUnit var3, u var4) {
      io.reactivex.d.b.b.a(var3, (String)"unit is null");
      io.reactivex.d.b.b.a(var4, (String)"scheduler is null");
      return io.reactivex.g.a.a((n)(new ab(this, var1, var3, var4)));
   }

   public final n debounce(io.reactivex.c.h var1) {
      io.reactivex.d.b.b.a(var1, (String)"debounceSelector is null");
      return io.reactivex.g.a.a((n)(new aa(this, var1)));
   }

   public final n defaultIfEmpty(Object var1) {
      io.reactivex.d.b.b.a(var1, "defaultItem is null");
      return this.switchIfEmpty(just(var1));
   }

   public final n delay(long var1, TimeUnit var3) {
      return this.delay(var1, var3, io.reactivex.h.a.a(), false);
   }

   public final n delay(long var1, TimeUnit var3, u var4) {
      return this.delay(var1, var3, var4, false);
   }

   public final n delay(long var1, TimeUnit var3, u var4, boolean var5) {
      io.reactivex.d.b.b.a(var3, (String)"unit is null");
      io.reactivex.d.b.b.a(var4, (String)"scheduler is null");
      return io.reactivex.g.a.a((n)(new ad(this, var1, var3, var4, var5)));
   }

   public final n delay(long var1, TimeUnit var3, boolean var4) {
      return this.delay(var1, var3, io.reactivex.h.a.a(), var4);
   }

   public final n delay(io.reactivex.c.h var1) {
      io.reactivex.d.b.b.a(var1, (String)"itemDelay is null");
      return this.flatMap(bl.a(var1));
   }

   public final n delay(r var1, io.reactivex.c.h var2) {
      return this.delaySubscription(var1).delay(var2);
   }

   public final n delaySubscription(long var1, TimeUnit var3) {
      return this.delaySubscription(var1, var3, io.reactivex.h.a.a());
   }

   public final n delaySubscription(long var1, TimeUnit var3, u var4) {
      return this.delaySubscription(timer(var1, var3, var4));
   }

   public final n delaySubscription(r var1) {
      io.reactivex.d.b.b.a(var1, (String)"other is null");
      return io.reactivex.g.a.a((n)(new ae(this, var1)));
   }

   public final n dematerialize() {
      return io.reactivex.g.a.a((n)(new af(this)));
   }

   public final n distinct() {
      return this.distinct(io.reactivex.d.b.a.a(), io.reactivex.d.b.a.g());
   }

   public final n distinct(io.reactivex.c.h var1) {
      return this.distinct(var1, io.reactivex.d.b.a.g());
   }

   public final n distinct(io.reactivex.c.h var1, Callable var2) {
      io.reactivex.d.b.b.a(var1, (String)"keySelector is null");
      io.reactivex.d.b.b.a(var2, (String)"collectionSupplier is null");
      return io.reactivex.g.a.a((n)(new ah(this, var1, var2)));
   }

   public final n distinctUntilChanged() {
      return this.distinctUntilChanged(io.reactivex.d.b.a.a());
   }

   public final n distinctUntilChanged(io.reactivex.c.d var1) {
      io.reactivex.d.b.b.a(var1, (String)"comparer is null");
      return io.reactivex.g.a.a((n)(new ai(this, io.reactivex.d.b.a.a(), var1)));
   }

   public final n distinctUntilChanged(io.reactivex.c.h var1) {
      io.reactivex.d.b.b.a(var1, (String)"keySelector is null");
      return io.reactivex.g.a.a((n)(new ai(this, var1, io.reactivex.d.b.b.a())));
   }

   public final n doAfterNext(io.reactivex.c.g var1) {
      io.reactivex.d.b.b.a(var1, (String)"onAfterNext is null");
      return io.reactivex.g.a.a((n)(new aj(this, var1)));
   }

   public final n doAfterTerminate(io.reactivex.c.a var1) {
      io.reactivex.d.b.b.a(var1, (String)"onFinally is null");
      return this.doOnEach(io.reactivex.d.b.a.b(), io.reactivex.d.b.a.b(), io.reactivex.d.b.a.c, var1);
   }

   public final n doFinally(io.reactivex.c.a var1) {
      io.reactivex.d.b.b.a(var1, (String)"onFinally is null");
      return io.reactivex.g.a.a((n)(new ak(this, var1)));
   }

   public final n doOnComplete(io.reactivex.c.a var1) {
      return this.doOnEach(io.reactivex.d.b.a.b(), io.reactivex.d.b.a.b(), var1, io.reactivex.d.b.a.c);
   }

   public final n doOnDispose(io.reactivex.c.a var1) {
      return this.doOnLifecycle(io.reactivex.d.b.a.b(), var1);
   }

   public final n doOnEach(io.reactivex.c.g var1) {
      io.reactivex.d.b.b.a(var1, (String)"consumer is null");
      return this.doOnEach(io.reactivex.d.b.a.a(var1), io.reactivex.d.b.a.b(var1), io.reactivex.d.b.a.c(var1), io.reactivex.d.b.a.c);
   }

   public final n doOnEach(t var1) {
      io.reactivex.d.b.b.a(var1, (String)"observer is null");
      return this.doOnEach(bl.a(var1), bl.b(var1), bl.c(var1), io.reactivex.d.b.a.c);
   }

   public final n doOnError(io.reactivex.c.g var1) {
      return this.doOnEach(io.reactivex.d.b.a.b(), var1, io.reactivex.d.b.a.c, io.reactivex.d.b.a.c);
   }

   public final n doOnLifecycle(io.reactivex.c.g var1, io.reactivex.c.a var2) {
      io.reactivex.d.b.b.a(var1, (String)"onSubscribe is null");
      io.reactivex.d.b.b.a(var2, (String)"onDispose is null");
      return io.reactivex.g.a.a((n)(new am(this, var1, var2)));
   }

   public final n doOnNext(io.reactivex.c.g var1) {
      return this.doOnEach(var1, io.reactivex.d.b.a.b(), io.reactivex.d.b.a.c, io.reactivex.d.b.a.c);
   }

   public final n doOnSubscribe(io.reactivex.c.g var1) {
      return this.doOnLifecycle(var1, io.reactivex.d.b.a.c);
   }

   public final n doOnTerminate(io.reactivex.c.a var1) {
      io.reactivex.d.b.b.a(var1, (String)"onTerminate is null");
      return this.doOnEach(io.reactivex.d.b.a.b(), io.reactivex.d.b.a.a(var1), var1, io.reactivex.d.b.a.c);
   }

   public final h elementAt(long var1) {
      if(var1 < 0L) {
         throw new IndexOutOfBoundsException("index >= 0 required but it was " + var1);
      } else {
         return io.reactivex.g.a.a((h)(new ao(this, var1)));
      }
   }

   public final v elementAt(long var1, Object var3) {
      if(var1 < 0L) {
         throw new IndexOutOfBoundsException("index >= 0 required but it was " + var1);
      } else {
         io.reactivex.d.b.b.a(var3, "defaultItem is null");
         return io.reactivex.g.a.a((v)(new ap(this, var1, var3)));
      }
   }

   public final v elementAtOrError(long var1) {
      if(var1 < 0L) {
         throw new IndexOutOfBoundsException("index >= 0 required but it was " + var1);
      } else {
         return io.reactivex.g.a.a((v)(new ap(this, var1, (Object)null)));
      }
   }

   public final n filter(io.reactivex.c.q var1) {
      io.reactivex.d.b.b.a(var1, (String)"predicate is null");
      return io.reactivex.g.a.a((n)(new as(this, var1)));
   }

   public final v first(Object var1) {
      return this.elementAt(0L, var1);
   }

   public final h firstElement() {
      return this.elementAt(0L);
   }

   public final v firstOrError() {
      return this.elementAtOrError(0L);
   }

   public final n flatMap(io.reactivex.c.h var1) {
      return this.flatMap(var1, false);
   }

   public final n flatMap(io.reactivex.c.h var1, int var2) {
      return this.flatMap(var1, false, var2, bufferSize());
   }

   public final n flatMap(io.reactivex.c.h var1, io.reactivex.c.c var2) {
      return this.flatMap(var1, var2, false, bufferSize(), bufferSize());
   }

   public final n flatMap(io.reactivex.c.h var1, io.reactivex.c.c var2, int var3) {
      return this.flatMap(var1, var2, false, var3, bufferSize());
   }

   public final n flatMap(io.reactivex.c.h var1, io.reactivex.c.c var2, boolean var3) {
      return this.flatMap(var1, var2, var3, bufferSize(), bufferSize());
   }

   public final n flatMap(io.reactivex.c.h var1, io.reactivex.c.c var2, boolean var3, int var4) {
      return this.flatMap(var1, var2, var3, var4, bufferSize());
   }

   public final n flatMap(io.reactivex.c.h var1, io.reactivex.c.c var2, boolean var3, int var4, int var5) {
      io.reactivex.d.b.b.a(var1, (String)"mapper is null");
      io.reactivex.d.b.b.a(var2, (String)"combiner is null");
      return this.flatMap(bl.a(var1, var2), var3, var4, var5);
   }

   public final n flatMap(io.reactivex.c.h var1, io.reactivex.c.h var2, Callable var3) {
      io.reactivex.d.b.b.a(var1, (String)"onNextMapper is null");
      io.reactivex.d.b.b.a(var2, (String)"onErrorMapper is null");
      io.reactivex.d.b.b.a(var3, (String)"onCompleteSupplier is null");
      return merge((r)(new bu(this, var1, var2, var3)));
   }

   public final n flatMap(io.reactivex.c.h var1, io.reactivex.c.h var2, Callable var3, int var4) {
      io.reactivex.d.b.b.a(var1, (String)"onNextMapper is null");
      io.reactivex.d.b.b.a(var2, (String)"onErrorMapper is null");
      io.reactivex.d.b.b.a(var3, (String)"onCompleteSupplier is null");
      return merge((r)(new bu(this, var1, var2, var3)), var4);
   }

   public final n flatMap(io.reactivex.c.h var1, boolean var2) {
      return this.flatMap(var1, var2, Integer.MAX_VALUE);
   }

   public final n flatMap(io.reactivex.c.h var1, boolean var2, int var3) {
      return this.flatMap(var1, var2, var3, bufferSize());
   }

   public final n flatMap(io.reactivex.c.h var1, boolean var2, int var3, int var4) {
      io.reactivex.d.b.b.a(var1, (String)"mapper is null");
      io.reactivex.d.b.b.a(var3, "maxConcurrency");
      io.reactivex.d.b.b.a(var4, "bufferSize");
      n var6;
      if(this instanceof io.reactivex.d.c.g) {
         Object var5 = ((io.reactivex.d.c.g)this).call();
         if(var5 == null) {
            var6 = empty();
         } else {
            var6 = cr.a(var5, var1);
         }
      } else {
         var6 = io.reactivex.g.a.a((n)(new at(this, var1, var2, var3, var4)));
      }

      return var6;
   }

   public final b flatMapCompletable(io.reactivex.c.h var1) {
      return this.flatMapCompletable(var1, false);
   }

   public final b flatMapCompletable(io.reactivex.c.h var1, boolean var2) {
      io.reactivex.d.b.b.a(var1, (String)"mapper is null");
      return io.reactivex.g.a.a((b)(new av(this, var1, var2)));
   }

   public final n flatMapIterable(io.reactivex.c.h var1) {
      io.reactivex.d.b.b.a(var1, (String)"mapper is null");
      return io.reactivex.g.a.a((n)(new ay(this, var1)));
   }

   public final n flatMapIterable(io.reactivex.c.h var1, io.reactivex.c.c var2) {
      io.reactivex.d.b.b.a(var1, (String)"mapper is null");
      io.reactivex.d.b.b.a(var2, (String)"resultSelector is null");
      return this.flatMap(bl.b(var1), var2, false, bufferSize(), bufferSize());
   }

   public final n flatMapMaybe(io.reactivex.c.h var1) {
      return this.flatMapMaybe(var1, false);
   }

   public final n flatMapMaybe(io.reactivex.c.h var1, boolean var2) {
      io.reactivex.d.b.b.a(var1, (String)"mapper is null");
      return io.reactivex.g.a.a((n)(new aw(this, var1, var2)));
   }

   public final n flatMapSingle(io.reactivex.c.h var1) {
      return this.flatMapSingle(var1, false);
   }

   public final n flatMapSingle(io.reactivex.c.h var1, boolean var2) {
      io.reactivex.d.b.b.a(var1, (String)"mapper is null");
      return io.reactivex.g.a.a((n)(new ax(this, var1, var2)));
   }

   public final io.reactivex.b.b forEach(io.reactivex.c.g var1) {
      return this.subscribe(var1);
   }

   public final io.reactivex.b.b forEachWhile(io.reactivex.c.q var1) {
      return this.forEachWhile(var1, io.reactivex.d.b.a.f, io.reactivex.d.b.a.c);
   }

   public final io.reactivex.b.b forEachWhile(io.reactivex.c.q var1, io.reactivex.c.g var2) {
      return this.forEachWhile(var1, var2, io.reactivex.d.b.a.c);
   }

   public final io.reactivex.b.b forEachWhile(io.reactivex.c.q var1, io.reactivex.c.g var2, io.reactivex.c.a var3) {
      io.reactivex.d.b.b.a(var1, (String)"onNext is null");
      io.reactivex.d.b.b.a(var2, (String)"onError is null");
      io.reactivex.d.b.b.a(var3, (String)"onComplete is null");
      io.reactivex.d.d.n var4 = new io.reactivex.d.d.n(var1, var2, var3);
      this.subscribe((t)var4);
      return var4;
   }

   public final n groupBy(io.reactivex.c.h var1) {
      return this.groupBy(var1, io.reactivex.d.b.a.a(), false, bufferSize());
   }

   public final n groupBy(io.reactivex.c.h var1, io.reactivex.c.h var2) {
      return this.groupBy(var1, var2, false, bufferSize());
   }

   public final n groupBy(io.reactivex.c.h var1, io.reactivex.c.h var2, boolean var3) {
      return this.groupBy(var1, var2, var3, bufferSize());
   }

   public final n groupBy(io.reactivex.c.h var1, io.reactivex.c.h var2, boolean var3, int var4) {
      io.reactivex.d.b.b.a(var1, (String)"keySelector is null");
      io.reactivex.d.b.b.a(var2, (String)"valueSelector is null");
      io.reactivex.d.b.b.a(var4, "bufferSize");
      return io.reactivex.g.a.a((n)(new bg(this, var1, var2, var4, var3)));
   }

   public final n groupBy(io.reactivex.c.h var1, boolean var2) {
      return this.groupBy(var1, io.reactivex.d.b.a.a(), var2, bufferSize());
   }

   public final n groupJoin(r var1, io.reactivex.c.h var2, io.reactivex.c.h var3, io.reactivex.c.c var4) {
      io.reactivex.d.b.b.a(var1, (String)"other is null");
      io.reactivex.d.b.b.a(var2, (String)"leftEnd is null");
      io.reactivex.d.b.b.a(var3, (String)"rightEnd is null");
      io.reactivex.d.b.b.a(var4, (String)"resultSelector is null");
      return io.reactivex.g.a.a((n)(new bh(this, var1, var2, var3, var4)));
   }

   public final n hide() {
      return io.reactivex.g.a.a((n)(new bi(this)));
   }

   public final b ignoreElements() {
      return io.reactivex.g.a.a((b)(new bk(this)));
   }

   public final v isEmpty() {
      return this.all(io.reactivex.d.b.a.d());
   }

   public final n join(r var1, io.reactivex.c.h var2, io.reactivex.c.h var3, io.reactivex.c.c var4) {
      io.reactivex.d.b.b.a(var1, (String)"other is null");
      io.reactivex.d.b.b.a(var2, (String)"leftEnd is null");
      io.reactivex.d.b.b.a(var3, (String)"rightEnd is null");
      io.reactivex.d.b.b.a(var4, (String)"resultSelector is null");
      return io.reactivex.g.a.a((n)(new bo(this, var1, var2, var3, var4)));
   }

   public final v last(Object var1) {
      io.reactivex.d.b.b.a(var1, "defaultItem is null");
      return io.reactivex.g.a.a((v)(new br(this, var1)));
   }

   public final h lastElement() {
      return io.reactivex.g.a.a((h)(new bq(this)));
   }

   public final v lastOrError() {
      return io.reactivex.g.a.a((v)(new br(this, (Object)null)));
   }

   public final n lift(q var1) {
      io.reactivex.d.b.b.a(var1, (String)"onLift is null");
      return io.reactivex.g.a.a((n)(new bs(this, var1)));
   }

   public final n map(io.reactivex.c.h var1) {
      io.reactivex.d.b.b.a(var1, (String)"mapper is null");
      return io.reactivex.g.a.a((n)(new bt(this, var1)));
   }

   public final n materialize() {
      return io.reactivex.g.a.a((n)(new bv(this)));
   }

   public final n mergeWith(r var1) {
      io.reactivex.d.b.b.a(var1, (String)"other is null");
      return merge(this, var1);
   }

   public final n observeOn(u var1) {
      return this.observeOn(var1, false, bufferSize());
   }

   public final n observeOn(u var1, boolean var2) {
      return this.observeOn(var1, var2, bufferSize());
   }

   public final n observeOn(u var1, boolean var2, int var3) {
      io.reactivex.d.b.b.a(var1, (String)"scheduler is null");
      io.reactivex.d.b.b.a(var3, "bufferSize");
      return io.reactivex.g.a.a((n)(new bx(this, var1, var2, var3)));
   }

   public final n ofType(Class var1) {
      io.reactivex.d.b.b.a(var1, (String)"clazz is null");
      return this.filter(io.reactivex.d.b.a.b(var1)).cast(var1);
   }

   public final n onErrorResumeNext(io.reactivex.c.h var1) {
      io.reactivex.d.b.b.a(var1, (String)"resumeFunction is null");
      return io.reactivex.g.a.a((n)(new by(this, var1, false)));
   }

   public final n onErrorResumeNext(r var1) {
      io.reactivex.d.b.b.a(var1, (String)"next is null");
      return this.onErrorResumeNext(io.reactivex.d.b.a.b((Object)var1));
   }

   public final n onErrorReturn(io.reactivex.c.h var1) {
      io.reactivex.d.b.b.a(var1, (String)"valueSupplier is null");
      return io.reactivex.g.a.a((n)(new bz(this, var1)));
   }

   public final n onErrorReturnItem(Object var1) {
      io.reactivex.d.b.b.a(var1, "item is null");
      return this.onErrorReturn(io.reactivex.d.b.a.b(var1));
   }

   public final n onExceptionResumeNext(r var1) {
      io.reactivex.d.b.b.a(var1, (String)"next is null");
      return io.reactivex.g.a.a((n)(new by(this, io.reactivex.d.b.a.b((Object)var1), true)));
   }

   public final n onTerminateDetach() {
      return io.reactivex.g.a.a((n)(new ag(this)));
   }

   public final io.reactivex.e.a publish() {
      return ca.a((r)this);
   }

   public final n publish(io.reactivex.c.h var1) {
      io.reactivex.d.b.b.a(var1, (String)"selector is null");
      return io.reactivex.g.a.a((n)(new cb(this, var1)));
   }

   public final h reduce(io.reactivex.c.c var1) {
      io.reactivex.d.b.b.a(var1, (String)"reducer is null");
      return io.reactivex.g.a.a((h)(new ce(this, var1)));
   }

   public final v reduce(Object var1, io.reactivex.c.c var2) {
      io.reactivex.d.b.b.a(var1, "seed is null");
      io.reactivex.d.b.b.a(var2, (String)"reducer is null");
      return io.reactivex.g.a.a((v)(new cf(this, var1, var2)));
   }

   public final v reduceWith(Callable var1, io.reactivex.c.c var2) {
      io.reactivex.d.b.b.a(var1, (String)"seedSupplier is null");
      io.reactivex.d.b.b.a(var2, (String)"reducer is null");
      return io.reactivex.g.a.a((v)(new cg(this, var1, var2)));
   }

   public final n repeat() {
      return this.repeat(Long.MAX_VALUE);
   }

   public final n repeat(long var1) {
      if(var1 < 0L) {
         throw new IllegalArgumentException("times >= 0 required but it was " + var1);
      } else {
         n var3;
         if(var1 == 0L) {
            var3 = empty();
         } else {
            var3 = io.reactivex.g.a.a((n)(new ci(this, var1)));
         }

         return var3;
      }
   }

   public final n repeatUntil(io.reactivex.c.e var1) {
      io.reactivex.d.b.b.a(var1, (String)"stop is null");
      return io.reactivex.g.a.a((n)(new cj(this, var1)));
   }

   public final n repeatWhen(io.reactivex.c.h var1) {
      io.reactivex.d.b.b.a(var1, (String)"handler is null");
      return io.reactivex.g.a.a((n)(new ck(this, var1)));
   }

   public final io.reactivex.e.a replay() {
      return cl.a((r)this);
   }

   public final io.reactivex.e.a replay(int var1) {
      io.reactivex.d.b.b.a(var1, "bufferSize");
      return cl.a(this, var1);
   }

   public final io.reactivex.e.a replay(int var1, long var2, TimeUnit var4) {
      return this.replay(var1, var2, var4, io.reactivex.h.a.a());
   }

   public final io.reactivex.e.a replay(int var1, long var2, TimeUnit var4, u var5) {
      io.reactivex.d.b.b.a(var1, "bufferSize");
      io.reactivex.d.b.b.a(var4, (String)"unit is null");
      io.reactivex.d.b.b.a(var5, (String)"scheduler is null");
      return cl.a(this, var2, var4, var5, var1);
   }

   public final io.reactivex.e.a replay(int var1, u var2) {
      io.reactivex.d.b.b.a(var1, "bufferSize");
      return cl.a(this.replay(var1), var2);
   }

   public final io.reactivex.e.a replay(long var1, TimeUnit var3) {
      return this.replay(var1, var3, io.reactivex.h.a.a());
   }

   public final io.reactivex.e.a replay(long var1, TimeUnit var3, u var4) {
      io.reactivex.d.b.b.a(var3, (String)"unit is null");
      io.reactivex.d.b.b.a(var4, (String)"scheduler is null");
      return cl.a(this, var1, var3, var4);
   }

   public final io.reactivex.e.a replay(u var1) {
      io.reactivex.d.b.b.a(var1, (String)"scheduler is null");
      return cl.a(this.replay(), var1);
   }

   public final n replay(io.reactivex.c.h var1) {
      io.reactivex.d.b.b.a(var1, (String)"selector is null");
      return cl.a(bl.a(this), var1);
   }

   public final n replay(io.reactivex.c.h var1, int var2) {
      io.reactivex.d.b.b.a(var1, (String)"selector is null");
      io.reactivex.d.b.b.a(var2, "bufferSize");
      return cl.a(bl.a(this, var2), var1);
   }

   public final n replay(io.reactivex.c.h var1, int var2, long var3, TimeUnit var5) {
      return this.replay(var1, var2, var3, var5, io.reactivex.h.a.a());
   }

   public final n replay(io.reactivex.c.h var1, int var2, long var3, TimeUnit var5, u var6) {
      io.reactivex.d.b.b.a(var1, (String)"selector is null");
      io.reactivex.d.b.b.a(var2, "bufferSize");
      io.reactivex.d.b.b.a(var5, (String)"unit is null");
      io.reactivex.d.b.b.a(var6, (String)"scheduler is null");
      return cl.a(bl.a(this, var2, var3, var5, var6), var1);
   }

   public final n replay(io.reactivex.c.h var1, int var2, u var3) {
      io.reactivex.d.b.b.a(var1, (String)"selector is null");
      io.reactivex.d.b.b.a(var3, (String)"scheduler is null");
      io.reactivex.d.b.b.a(var2, "bufferSize");
      return cl.a(bl.a(this, var2), bl.a(var1, var3));
   }

   public final n replay(io.reactivex.c.h var1, long var2, TimeUnit var4) {
      return this.replay(var1, var2, var4, io.reactivex.h.a.a());
   }

   public final n replay(io.reactivex.c.h var1, long var2, TimeUnit var4, u var5) {
      io.reactivex.d.b.b.a(var1, (String)"selector is null");
      io.reactivex.d.b.b.a(var4, (String)"unit is null");
      io.reactivex.d.b.b.a(var5, (String)"scheduler is null");
      return cl.a(bl.a(this, var2, var4, var5), var1);
   }

   public final n replay(io.reactivex.c.h var1, u var2) {
      io.reactivex.d.b.b.a(var1, (String)"selector is null");
      io.reactivex.d.b.b.a(var2, (String)"scheduler is null");
      return cl.a(bl.a(this), bl.a(var1, var2));
   }

   public final n retry() {
      return this.retry(Long.MAX_VALUE, io.reactivex.d.b.a.c());
   }

   public final n retry(long var1) {
      return this.retry(var1, io.reactivex.d.b.a.c());
   }

   public final n retry(long var1, io.reactivex.c.q var3) {
      if(var1 < 0L) {
         throw new IllegalArgumentException("times >= 0 required but it was " + var1);
      } else {
         io.reactivex.d.b.b.a(var3, (String)"predicate is null");
         return io.reactivex.g.a.a((n)(new cn(this, var1, var3)));
      }
   }

   public final n retry(io.reactivex.c.d var1) {
      io.reactivex.d.b.b.a(var1, (String)"predicate is null");
      return io.reactivex.g.a.a((n)(new cm(this, var1)));
   }

   public final n retry(io.reactivex.c.q var1) {
      return this.retry(Long.MAX_VALUE, var1);
   }

   public final n retryUntil(io.reactivex.c.e var1) {
      io.reactivex.d.b.b.a(var1, (String)"stop is null");
      return this.retry(Long.MAX_VALUE, io.reactivex.d.b.a.a(var1));
   }

   public final n retryWhen(io.reactivex.c.h var1) {
      io.reactivex.d.b.b.a(var1, (String)"handler is null");
      return io.reactivex.g.a.a((n)(new co(this, var1)));
   }

   public final void safeSubscribe(t var1) {
      io.reactivex.d.b.b.a(var1, (String)"s is null");
      if(var1 instanceof io.reactivex.f.d) {
         this.subscribe(var1);
      } else {
         this.subscribe((t)(new io.reactivex.f.d(var1)));
      }

   }

   public final n sample(long var1, TimeUnit var3) {
      return this.sample(var1, var3, io.reactivex.h.a.a());
   }

   public final n sample(long var1, TimeUnit var3, u var4) {
      io.reactivex.d.b.b.a(var3, (String)"unit is null");
      io.reactivex.d.b.b.a(var4, (String)"scheduler is null");
      return io.reactivex.g.a.a((n)(new cp(this, var1, var3, var4, false)));
   }

   public final n sample(long var1, TimeUnit var3, u var4, boolean var5) {
      io.reactivex.d.b.b.a(var3, (String)"unit is null");
      io.reactivex.d.b.b.a(var4, (String)"scheduler is null");
      return io.reactivex.g.a.a((n)(new cp(this, var1, var3, var4, var5)));
   }

   public final n sample(long var1, TimeUnit var3, boolean var4) {
      return this.sample(var1, var3, io.reactivex.h.a.a(), var4);
   }

   public final n sample(r var1) {
      io.reactivex.d.b.b.a(var1, (String)"sampler is null");
      return io.reactivex.g.a.a((n)(new cq(this, var1, false)));
   }

   public final n sample(r var1, boolean var2) {
      io.reactivex.d.b.b.a(var1, (String)"sampler is null");
      return io.reactivex.g.a.a((n)(new cq(this, var1, var2)));
   }

   public final n scan(io.reactivex.c.c var1) {
      io.reactivex.d.b.b.a(var1, (String)"accumulator is null");
      return io.reactivex.g.a.a((n)(new cs(this, var1)));
   }

   public final n scan(Object var1, io.reactivex.c.c var2) {
      io.reactivex.d.b.b.a(var1, "seed is null");
      return this.scanWith(io.reactivex.d.b.a.a(var1), var2);
   }

   public final n scanWith(Callable var1, io.reactivex.c.c var2) {
      io.reactivex.d.b.b.a(var1, (String)"seedSupplier is null");
      io.reactivex.d.b.b.a(var2, (String)"accumulator is null");
      return io.reactivex.g.a.a((n)(new ct(this, var1, var2)));
   }

   public final n serialize() {
      return io.reactivex.g.a.a((n)(new cw(this)));
   }

   public final n share() {
      return this.publish().a();
   }

   public final v single(Object var1) {
      io.reactivex.d.b.b.a(var1, "defaultItem is null");
      return io.reactivex.g.a.a((v)(new cy(this, var1)));
   }

   public final h singleElement() {
      return io.reactivex.g.a.a((h)(new cx(this)));
   }

   public final v singleOrError() {
      return io.reactivex.g.a.a((v)(new cy(this, (Object)null)));
   }

   public final n skip(long var1) {
      n var3;
      if(var1 <= 0L) {
         var3 = io.reactivex.g.a.a(this);
      } else {
         var3 = io.reactivex.g.a.a((n)(new cz(this, var1)));
      }

      return var3;
   }

   public final n skip(long var1, TimeUnit var3) {
      return this.skipUntil(timer(var1, var3));
   }

   public final n skip(long var1, TimeUnit var3, u var4) {
      return this.skipUntil(timer(var1, var3, var4));
   }

   public final n skipLast(int var1) {
      if(var1 < 0) {
         throw new IndexOutOfBoundsException("count >= 0 required but it was " + var1);
      } else {
         n var2;
         if(var1 == 0) {
            var2 = io.reactivex.g.a.a(this);
         } else {
            var2 = io.reactivex.g.a.a((n)(new da(this, var1)));
         }

         return var2;
      }
   }

   public final n skipLast(long var1, TimeUnit var3) {
      return this.skipLast(var1, var3, io.reactivex.h.a.c(), false, bufferSize());
   }

   public final n skipLast(long var1, TimeUnit var3, u var4) {
      return this.skipLast(var1, var3, var4, false, bufferSize());
   }

   public final n skipLast(long var1, TimeUnit var3, u var4, boolean var5) {
      return this.skipLast(var1, var3, var4, var5, bufferSize());
   }

   public final n skipLast(long var1, TimeUnit var3, u var4, boolean var5, int var6) {
      io.reactivex.d.b.b.a(var3, (String)"unit is null");
      io.reactivex.d.b.b.a(var4, (String)"scheduler is null");
      io.reactivex.d.b.b.a(var6, "bufferSize");
      return io.reactivex.g.a.a((n)(new db(this, var1, var3, var4, var6 << 1, var5)));
   }

   public final n skipLast(long var1, TimeUnit var3, boolean var4) {
      return this.skipLast(var1, var3, io.reactivex.h.a.c(), var4, bufferSize());
   }

   public final n skipUntil(r var1) {
      io.reactivex.d.b.b.a(var1, (String)"other is null");
      return io.reactivex.g.a.a((n)(new dc(this, var1)));
   }

   public final n skipWhile(io.reactivex.c.q var1) {
      io.reactivex.d.b.b.a(var1, (String)"predicate is null");
      return io.reactivex.g.a.a((n)(new dd(this, var1)));
   }

   public final n sorted() {
      return this.toList().f().map(io.reactivex.d.b.a.a(io.reactivex.d.b.a.h())).flatMapIterable(io.reactivex.d.b.a.a());
   }

   public final n sorted(Comparator var1) {
      io.reactivex.d.b.b.a(var1, (String)"sortFunction is null");
      return this.toList().f().map(io.reactivex.d.b.a.a(var1)).flatMapIterable(io.reactivex.d.b.a.a());
   }

   public final n startWith(r var1) {
      io.reactivex.d.b.b.a(var1, (String)"other is null");
      return concatArray(new r[]{var1, this});
   }

   public final n startWith(Iterable var1) {
      return concatArray(new r[]{fromIterable(var1), this});
   }

   public final n startWith(Object var1) {
      io.reactivex.d.b.b.a(var1, "item is null");
      return concatArray(new r[]{just(var1), this});
   }

   public final n startWithArray(Object... var1) {
      n var2 = fromArray(var1);
      if(var2 == empty()) {
         var2 = io.reactivex.g.a.a(this);
      } else {
         var2 = concatArray(new r[]{var2, this});
      }

      return var2;
   }

   public final io.reactivex.b.b subscribe() {
      return this.subscribe(io.reactivex.d.b.a.b(), io.reactivex.d.b.a.f, io.reactivex.d.b.a.c, io.reactivex.d.b.a.b());
   }

   public final io.reactivex.b.b subscribe(io.reactivex.c.g var1) {
      return this.subscribe(var1, io.reactivex.d.b.a.f, io.reactivex.d.b.a.c, io.reactivex.d.b.a.b());
   }

   public final io.reactivex.b.b subscribe(io.reactivex.c.g var1, io.reactivex.c.g var2) {
      return this.subscribe(var1, var2, io.reactivex.d.b.a.c, io.reactivex.d.b.a.b());
   }

   public final io.reactivex.b.b subscribe(io.reactivex.c.g var1, io.reactivex.c.g var2, io.reactivex.c.a var3) {
      return this.subscribe(var1, var2, var3, io.reactivex.d.b.a.b());
   }

   public final io.reactivex.b.b subscribe(io.reactivex.c.g var1, io.reactivex.c.g var2, io.reactivex.c.a var3, io.reactivex.c.g var4) {
      io.reactivex.d.b.b.a(var1, (String)"onNext is null");
      io.reactivex.d.b.b.a(var2, (String)"onError is null");
      io.reactivex.d.b.b.a(var3, (String)"onComplete is null");
      io.reactivex.d.b.b.a(var4, (String)"onSubscribe is null");
      io.reactivex.d.d.s var5 = new io.reactivex.d.d.s(var1, var2, var3, var4);
      this.subscribe((t)var5);
      return var5;
   }

   public final void subscribe(t var1) {
      io.reactivex.d.b.b.a(var1, (String)"observer is null");

      try {
         var1 = io.reactivex.g.a.a(this, var1);
         io.reactivex.d.b.b.a(var1, (String)"Plugin returned null Observer");
         this.subscribeActual(var1);
      } catch (NullPointerException var3) {
         throw var3;
      } catch (Throwable var4) {
         io.reactivex.exceptions.a.b(var4);
         io.reactivex.g.a.a(var4);
         NullPointerException var5 = new NullPointerException("Actually not, but can't throw other exceptions due to RS");
         var5.initCause(var4);
         throw var5;
      }
   }

   protected abstract void subscribeActual(t var1);

   public final n subscribeOn(u var1) {
      io.reactivex.d.b.b.a(var1, (String)"scheduler is null");
      return io.reactivex.g.a.a((n)(new de(this, var1)));
   }

   public final t subscribeWith(t var1) {
      this.subscribe(var1);
      return var1;
   }

   public final n switchIfEmpty(r var1) {
      io.reactivex.d.b.b.a(var1, (String)"other is null");
      return io.reactivex.g.a.a((n)(new df(this, var1)));
   }

   public final n switchMap(io.reactivex.c.h var1) {
      return this.switchMap(var1, bufferSize());
   }

   public final n switchMap(io.reactivex.c.h var1, int var2) {
      io.reactivex.d.b.b.a(var1, (String)"mapper is null");
      io.reactivex.d.b.b.a(var2, "bufferSize");
      n var4;
      if(this instanceof io.reactivex.d.c.g) {
         Object var3 = ((io.reactivex.d.c.g)this).call();
         if(var3 == null) {
            var4 = empty();
         } else {
            var4 = cr.a(var3, var1);
         }
      } else {
         var4 = io.reactivex.g.a.a((n)(new dg(this, var1, var2, false)));
      }

      return var4;
   }

   public final n switchMapDelayError(io.reactivex.c.h var1) {
      return this.switchMapDelayError(var1, bufferSize());
   }

   public final n switchMapDelayError(io.reactivex.c.h var1, int var2) {
      io.reactivex.d.b.b.a(var1, (String)"mapper is null");
      io.reactivex.d.b.b.a(var2, "bufferSize");
      n var4;
      if(this instanceof io.reactivex.d.c.g) {
         Object var3 = ((io.reactivex.d.c.g)this).call();
         if(var3 == null) {
            var4 = empty();
         } else {
            var4 = cr.a(var3, var1);
         }
      } else {
         var4 = io.reactivex.g.a.a((n)(new dg(this, var1, var2, true)));
      }

      return var4;
   }

   public final n switchMapSingle(io.reactivex.c.h var1) {
      return bl.a(this, var1);
   }

   public final n switchMapSingleDelayError(io.reactivex.c.h var1) {
      return bl.b(this, var1);
   }

   public final n take(long var1) {
      if(var1 < 0L) {
         throw new IllegalArgumentException("count >= 0 required but it was " + var1);
      } else {
         return io.reactivex.g.a.a((n)(new dh(this, var1)));
      }
   }

   public final n take(long var1, TimeUnit var3) {
      return this.takeUntil((r)timer(var1, var3));
   }

   public final n take(long var1, TimeUnit var3, u var4) {
      return this.takeUntil((r)timer(var1, var3, var4));
   }

   public final n takeLast(int var1) {
      if(var1 < 0) {
         throw new IndexOutOfBoundsException("count >= 0 required but it was " + var1);
      } else {
         n var2;
         if(var1 == 0) {
            var2 = io.reactivex.g.a.a((n)(new bj(this)));
         } else if(var1 == 1) {
            var2 = io.reactivex.g.a.a((n)(new dj(this)));
         } else {
            var2 = io.reactivex.g.a.a((n)(new di(this, var1)));
         }

         return var2;
      }
   }

   public final n takeLast(long var1, long var3, TimeUnit var5) {
      return this.takeLast(var1, var3, var5, io.reactivex.h.a.c(), false, bufferSize());
   }

   public final n takeLast(long var1, long var3, TimeUnit var5, u var6) {
      return this.takeLast(var1, var3, var5, var6, false, bufferSize());
   }

   public final n takeLast(long var1, long var3, TimeUnit var5, u var6, boolean var7, int var8) {
      io.reactivex.d.b.b.a(var5, (String)"unit is null");
      io.reactivex.d.b.b.a(var6, (String)"scheduler is null");
      io.reactivex.d.b.b.a(var8, "bufferSize");
      if(var1 < 0L) {
         throw new IndexOutOfBoundsException("count >= 0 required but it was " + var1);
      } else {
         return io.reactivex.g.a.a((n)(new dk(this, var1, var3, var5, var6, var8, var7)));
      }
   }

   public final n takeLast(long var1, TimeUnit var3) {
      return this.takeLast(var1, var3, io.reactivex.h.a.c(), false, bufferSize());
   }

   public final n takeLast(long var1, TimeUnit var3, u var4) {
      return this.takeLast(var1, var3, var4, false, bufferSize());
   }

   public final n takeLast(long var1, TimeUnit var3, u var4, boolean var5) {
      return this.takeLast(var1, var3, var4, var5, bufferSize());
   }

   public final n takeLast(long var1, TimeUnit var3, u var4, boolean var5, int var6) {
      return this.takeLast(Long.MAX_VALUE, var1, var3, var4, var5, var6);
   }

   public final n takeLast(long var1, TimeUnit var3, boolean var4) {
      return this.takeLast(var1, var3, io.reactivex.h.a.c(), var4, bufferSize());
   }

   public final n takeUntil(io.reactivex.c.q var1) {
      io.reactivex.d.b.b.a(var1, (String)"predicate is null");
      return io.reactivex.g.a.a((n)(new dm(this, var1)));
   }

   public final n takeUntil(r var1) {
      io.reactivex.d.b.b.a(var1, (String)"other is null");
      return io.reactivex.g.a.a((n)(new dl(this, var1)));
   }

   public final n takeWhile(io.reactivex.c.q var1) {
      io.reactivex.d.b.b.a(var1, (String)"predicate is null");
      return io.reactivex.g.a.a((n)(new dn(this, var1)));
   }

   public final io.reactivex.f.f test() {
      io.reactivex.f.f var1 = new io.reactivex.f.f();
      this.subscribe((t)var1);
      return var1;
   }

   public final io.reactivex.f.f test(boolean var1) {
      io.reactivex.f.f var2 = new io.reactivex.f.f();
      if(var1) {
         var2.dispose();
      }

      this.subscribe((t)var2);
      return var2;
   }

   public final n throttleFirst(long var1, TimeUnit var3) {
      return this.throttleFirst(var1, var3, io.reactivex.h.a.a());
   }

   public final n throttleFirst(long var1, TimeUnit var3, u var4) {
      io.reactivex.d.b.b.a(var3, (String)"unit is null");
      io.reactivex.d.b.b.a(var4, (String)"scheduler is null");
      return io.reactivex.g.a.a((n)(new do(this, var1, var3, var4)));
   }

   public final n throttleLast(long var1, TimeUnit var3) {
      return this.sample(var1, var3);
   }

   public final n throttleLast(long var1, TimeUnit var3, u var4) {
      return this.sample(var1, var3, var4);
   }

   public final n throttleWithTimeout(long var1, TimeUnit var3) {
      return this.debounce(var1, var3);
   }

   public final n throttleWithTimeout(long var1, TimeUnit var3, u var4) {
      return this.debounce(var1, var3, var4);
   }

   public final n timeInterval() {
      return this.timeInterval(TimeUnit.MILLISECONDS, io.reactivex.h.a.a());
   }

   public final n timeInterval(u var1) {
      return this.timeInterval(TimeUnit.MILLISECONDS, var1);
   }

   public final n timeInterval(TimeUnit var1) {
      return this.timeInterval(var1, io.reactivex.h.a.a());
   }

   public final n timeInterval(TimeUnit var1, u var2) {
      io.reactivex.d.b.b.a(var1, (String)"unit is null");
      io.reactivex.d.b.b.a(var2, (String)"scheduler is null");
      return io.reactivex.g.a.a((n)(new dp(this, var1, var2)));
   }

   public final n timeout(long var1, TimeUnit var3) {
      return this.timeout0(var1, var3, (r)null, io.reactivex.h.a.a());
   }

   public final n timeout(long var1, TimeUnit var3, r var4) {
      io.reactivex.d.b.b.a(var4, (String)"other is null");
      return this.timeout0(var1, var3, var4, io.reactivex.h.a.a());
   }

   public final n timeout(long var1, TimeUnit var3, u var4) {
      return this.timeout0(var1, var3, (r)null, var4);
   }

   public final n timeout(long var1, TimeUnit var3, u var4, r var5) {
      io.reactivex.d.b.b.a(var5, (String)"other is null");
      return this.timeout0(var1, var3, var5, var4);
   }

   public final n timeout(io.reactivex.c.h var1) {
      return this.timeout0((r)null, var1, (r)null);
   }

   public final n timeout(io.reactivex.c.h var1, r var2) {
      io.reactivex.d.b.b.a(var2, (String)"other is null");
      return this.timeout0((r)null, var1, var2);
   }

   public final n timeout(r var1, io.reactivex.c.h var2) {
      io.reactivex.d.b.b.a(var1, (String)"firstTimeoutIndicator is null");
      return this.timeout0(var1, var2, (r)null);
   }

   public final n timeout(r var1, io.reactivex.c.h var2, r var3) {
      io.reactivex.d.b.b.a(var1, (String)"firstTimeoutIndicator is null");
      io.reactivex.d.b.b.a(var3, (String)"other is null");
      return this.timeout0(var1, var2, var3);
   }

   public final n timestamp() {
      return this.timestamp(TimeUnit.MILLISECONDS, io.reactivex.h.a.a());
   }

   public final n timestamp(u var1) {
      return this.timestamp(TimeUnit.MILLISECONDS, var1);
   }

   public final n timestamp(TimeUnit var1) {
      return this.timestamp(var1, io.reactivex.h.a.a());
   }

   public final n timestamp(TimeUnit var1, u var2) {
      io.reactivex.d.b.b.a(var1, (String)"unit is null");
      io.reactivex.d.b.b.a(var2, (String)"scheduler is null");
      return this.map(io.reactivex.d.b.a.a(var1, var2));
   }

   public final Object to(io.reactivex.c.h var1) {
      try {
         Object var3 = ((io.reactivex.c.h)io.reactivex.d.b.b.a(var1, (String)"converter is null")).a(this);
         return var3;
      } catch (Throwable var2) {
         io.reactivex.exceptions.a.b(var2);
         throw io.reactivex.d.j.j.a(var2);
      }
   }

   public final f toFlowable(a var1) {
      io.reactivex.d.e.b.b var3 = new io.reactivex.d.e.b.b(this);
      Object var2 = var3;
      switch(null.a[var1.ordinal()]) {
      case 1:
         var2 = var3.c();
         break;
      case 2:
         var2 = var3.d();
      case 3:
         break;
      case 4:
         var2 = io.reactivex.g.a.a((f)(new io.reactivex.d.e.b.e(var3)));
         break;
      default:
         var2 = var3.b();
      }

      return (f)var2;
   }

   public final Future toFuture() {
      return (Future)this.subscribeWith(new io.reactivex.d.d.p());
   }

   public final v toList() {
      return this.toList(16);
   }

   public final v toList(int var1) {
      io.reactivex.d.b.b.a(var1, "capacityHint");
      return io.reactivex.g.a.a((v)(new du(this, var1)));
   }

   public final v toList(Callable var1) {
      io.reactivex.d.b.b.a(var1, (String)"collectionSupplier is null");
      return io.reactivex.g.a.a((v)(new du(this, var1)));
   }

   public final v toMap(io.reactivex.c.h var1) {
      io.reactivex.d.b.b.a(var1, (String)"keySelector is null");
      return this.collect(io.reactivex.d.j.l.a(), io.reactivex.d.b.a.a(var1));
   }

   public final v toMap(io.reactivex.c.h var1, io.reactivex.c.h var2) {
      io.reactivex.d.b.b.a(var1, (String)"keySelector is null");
      io.reactivex.d.b.b.a(var2, (String)"valueSelector is null");
      return this.collect(io.reactivex.d.j.l.a(), io.reactivex.d.b.a.a(var1, var2));
   }

   public final v toMap(io.reactivex.c.h var1, io.reactivex.c.h var2, Callable var3) {
      io.reactivex.d.b.b.a(var1, (String)"keySelector is null");
      io.reactivex.d.b.b.a(var1, (String)"keySelector is null");
      io.reactivex.d.b.b.a(var2, (String)"valueSelector is null");
      io.reactivex.d.b.b.a(var3, (String)"mapSupplier is null");
      return this.collect(var3, io.reactivex.d.b.a.a(var1, var2));
   }

   public final v toMultimap(io.reactivex.c.h var1) {
      return this.toMultimap(var1, io.reactivex.d.b.a.a(), io.reactivex.d.j.l.a(), io.reactivex.d.j.b.b());
   }

   public final v toMultimap(io.reactivex.c.h var1, io.reactivex.c.h var2) {
      return this.toMultimap(var1, var2, io.reactivex.d.j.l.a(), io.reactivex.d.j.b.b());
   }

   public final v toMultimap(io.reactivex.c.h var1, io.reactivex.c.h var2, Callable var3) {
      return this.toMultimap(var1, var2, var3, io.reactivex.d.j.b.b());
   }

   public final v toMultimap(io.reactivex.c.h var1, io.reactivex.c.h var2, Callable var3, io.reactivex.c.h var4) {
      io.reactivex.d.b.b.a(var1, (String)"keySelector is null");
      io.reactivex.d.b.b.a(var2, (String)"valueSelector is null");
      io.reactivex.d.b.b.a(var3, (String)"mapSupplier is null");
      io.reactivex.d.b.b.a(var4, (String)"collectionFactory is null");
      return this.collect(var3, io.reactivex.d.b.a.a(var1, var2, var4));
   }

   public final v toSortedList() {
      return this.toSortedList(io.reactivex.d.b.a.f());
   }

   public final v toSortedList(int var1) {
      return this.toSortedList(io.reactivex.d.b.a.f(), var1);
   }

   public final v toSortedList(Comparator var1) {
      io.reactivex.d.b.b.a(var1, (String)"comparator is null");
      return this.toList().d(io.reactivex.d.b.a.a(var1));
   }

   public final v toSortedList(Comparator var1, int var2) {
      io.reactivex.d.b.b.a(var1, (String)"comparator is null");
      return this.toList(var2).d(io.reactivex.d.b.a.a(var1));
   }

   public final n unsubscribeOn(u var1) {
      io.reactivex.d.b.b.a(var1, (String)"scheduler is null");
      return io.reactivex.g.a.a((n)(new dv(this, var1)));
   }

   public final n window(long var1) {
      return this.window(var1, var1, bufferSize());
   }

   public final n window(long var1, long var3) {
      return this.window(var1, var3, bufferSize());
   }

   public final n window(long var1, long var3, int var5) {
      io.reactivex.d.b.b.a(var1, "count");
      io.reactivex.d.b.b.a(var3, "skip");
      io.reactivex.d.b.b.a(var5, "bufferSize");
      return io.reactivex.g.a.a((n)(new dx(this, var1, var3, var5)));
   }

   public final n window(long var1, long var3, TimeUnit var5) {
      return this.window(var1, var3, var5, io.reactivex.h.a.a(), bufferSize());
   }

   public final n window(long var1, long var3, TimeUnit var5, u var6) {
      return this.window(var1, var3, var5, var6, bufferSize());
   }

   public final n window(long var1, long var3, TimeUnit var5, u var6, int var7) {
      io.reactivex.d.b.b.a(var1, "timespan");
      io.reactivex.d.b.b.a(var3, "timeskip");
      io.reactivex.d.b.b.a(var7, "bufferSize");
      io.reactivex.d.b.b.a(var6, (String)"scheduler is null");
      io.reactivex.d.b.b.a(var5, (String)"unit is null");
      return io.reactivex.g.a.a((n)(new eb(this, var1, var3, var5, var6, Long.MAX_VALUE, var7, false)));
   }

   public final n window(long var1, TimeUnit var3) {
      return this.window(var1, var3, io.reactivex.h.a.a(), Long.MAX_VALUE, false);
   }

   public final n window(long var1, TimeUnit var3, long var4) {
      return this.window(var1, var3, io.reactivex.h.a.a(), var4, false);
   }

   public final n window(long var1, TimeUnit var3, long var4, boolean var6) {
      return this.window(var1, var3, io.reactivex.h.a.a(), var4, var6);
   }

   public final n window(long var1, TimeUnit var3, u var4) {
      return this.window(var1, var3, var4, Long.MAX_VALUE, false);
   }

   public final n window(long var1, TimeUnit var3, u var4, long var5) {
      return this.window(var1, var3, var4, var5, false);
   }

   public final n window(long var1, TimeUnit var3, u var4, long var5, boolean var7) {
      return this.window(var1, var3, var4, var5, var7, bufferSize());
   }

   public final n window(long var1, TimeUnit var3, u var4, long var5, boolean var7, int var8) {
      io.reactivex.d.b.b.a(var8, "bufferSize");
      io.reactivex.d.b.b.a(var4, (String)"scheduler is null");
      io.reactivex.d.b.b.a(var3, (String)"unit is null");
      io.reactivex.d.b.b.a(var5, "count");
      return io.reactivex.g.a.a((n)(new eb(this, var1, var1, var3, var4, var5, var8, var7)));
   }

   public final n window(r var1) {
      return this.window(var1, bufferSize());
   }

   public final n window(r var1, int var2) {
      io.reactivex.d.b.b.a(var1, (String)"boundary is null");
      io.reactivex.d.b.b.a(var2, "bufferSize");
      return io.reactivex.g.a.a((n)(new dy(this, var1, var2)));
   }

   public final n window(r var1, io.reactivex.c.h var2) {
      return this.window(var1, var2, bufferSize());
   }

   public final n window(r var1, io.reactivex.c.h var2, int var3) {
      io.reactivex.d.b.b.a(var1, (String)"openingIndicator is null");
      io.reactivex.d.b.b.a(var2, (String)"closingIndicator is null");
      io.reactivex.d.b.b.a(var3, "bufferSize");
      return io.reactivex.g.a.a((n)(new dz(this, var1, var2, var3)));
   }

   public final n window(Callable var1) {
      return this.window(var1, bufferSize());
   }

   public final n window(Callable var1, int var2) {
      io.reactivex.d.b.b.a(var1, (String)"boundary is null");
      io.reactivex.d.b.b.a(var2, "bufferSize");
      return io.reactivex.g.a.a((n)(new ea(this, var1, var2)));
   }

   public final n withLatestFrom(r var1, io.reactivex.c.c var2) {
      io.reactivex.d.b.b.a(var1, (String)"other is null");
      io.reactivex.d.b.b.a(var2, (String)"combiner is null");
      return io.reactivex.g.a.a((n)(new ec(this, var2, var1)));
   }

   public final n withLatestFrom(r var1, r var2, io.reactivex.c.i var3) {
      io.reactivex.d.b.b.a(var1, (String)"o1 is null");
      io.reactivex.d.b.b.a(var2, (String)"o2 is null");
      io.reactivex.d.b.b.a(var3, (String)"combiner is null");
      io.reactivex.c.h var4 = io.reactivex.d.b.a.a(var3);
      return this.withLatestFrom(new r[]{var1, var2}, var4);
   }

   public final n withLatestFrom(r var1, r var2, r var3, io.reactivex.c.j var4) {
      io.reactivex.d.b.b.a(var1, (String)"o1 is null");
      io.reactivex.d.b.b.a(var2, (String)"o2 is null");
      io.reactivex.d.b.b.a(var3, (String)"o3 is null");
      io.reactivex.d.b.b.a(var4, (String)"combiner is null");
      io.reactivex.c.h var5 = io.reactivex.d.b.a.a(var4);
      return this.withLatestFrom(new r[]{var1, var2, var3}, var5);
   }

   public final n withLatestFrom(r var1, r var2, r var3, r var4, io.reactivex.c.k var5) {
      io.reactivex.d.b.b.a(var1, (String)"o1 is null");
      io.reactivex.d.b.b.a(var2, (String)"o2 is null");
      io.reactivex.d.b.b.a(var3, (String)"o3 is null");
      io.reactivex.d.b.b.a(var4, (String)"o4 is null");
      io.reactivex.d.b.b.a(var5, (String)"combiner is null");
      io.reactivex.c.h var6 = io.reactivex.d.b.a.a(var5);
      return this.withLatestFrom(new r[]{var1, var2, var3, var4}, var6);
   }

   public final n withLatestFrom(Iterable var1, io.reactivex.c.h var2) {
      io.reactivex.d.b.b.a(var1, (String)"others is null");
      io.reactivex.d.b.b.a(var2, (String)"combiner is null");
      return io.reactivex.g.a.a((n)(new ed(this, var1, var2)));
   }

   public final n withLatestFrom(r[] var1, io.reactivex.c.h var2) {
      io.reactivex.d.b.b.a(var1, (String)"others is null");
      io.reactivex.d.b.b.a(var2, (String)"combiner is null");
      return io.reactivex.g.a.a((n)(new ed(this, var1, var2)));
   }

   public final n zipWith(r var1, io.reactivex.c.c var2) {
      io.reactivex.d.b.b.a(var1, (String)"other is null");
      return zip(this, var1, var2);
   }

   public final n zipWith(r var1, io.reactivex.c.c var2, boolean var3) {
      return zip(this, var1, var2, var3);
   }

   public final n zipWith(r var1, io.reactivex.c.c var2, boolean var3, int var4) {
      return zip(this, var1, var2, var3, var4);
   }

   public final n zipWith(Iterable var1, io.reactivex.c.c var2) {
      io.reactivex.d.b.b.a(var1, (String)"other is null");
      io.reactivex.d.b.b.a(var2, (String)"zipper is null");
      return io.reactivex.g.a.a((n)(new ef(this, var1, var2)));
   }
}
