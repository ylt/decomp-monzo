package io.reactivex;

public final class m {
   static final m b = new m((Object)null);
   final Object a;

   private m(Object var1) {
      this.a = var1;
   }

   public static m a(Object var0) {
      io.reactivex.d.b.b.a(var0, "value is null");
      return new m(var0);
   }

   public static m a(Throwable var0) {
      io.reactivex.d.b.b.a(var0, (String)"error is null");
      return new m(io.reactivex.d.j.n.a(var0));
   }

   public static m f() {
      return b;
   }

   public boolean a() {
      boolean var1;
      if(this.a == null) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public boolean b() {
      return io.reactivex.d.j.n.c(this.a);
   }

   public boolean c() {
      Object var2 = this.a;
      boolean var1;
      if(var2 != null && !io.reactivex.d.j.n.c(var2)) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public Object d() {
      Object var1 = this.a;
      if(var1 != null && !io.reactivex.d.j.n.c(var1)) {
         var1 = this.a;
      } else {
         var1 = null;
      }

      return var1;
   }

   public Throwable e() {
      Object var1 = this.a;
      Throwable var2;
      if(io.reactivex.d.j.n.c(var1)) {
         var2 = io.reactivex.d.j.n.f(var1);
      } else {
         var2 = null;
      }

      return var2;
   }

   public boolean equals(Object var1) {
      boolean var2;
      if(var1 instanceof m) {
         m var3 = (m)var1;
         var2 = io.reactivex.d.b.b.a(this.a, var3.a);
      } else {
         var2 = false;
      }

      return var2;
   }

   public int hashCode() {
      Object var2 = this.a;
      int var1;
      if(var2 != null) {
         var1 = var2.hashCode();
      } else {
         var1 = 0;
      }

      return var1;
   }

   public String toString() {
      Object var1 = this.a;
      String var2;
      if(var1 == null) {
         var2 = "OnCompleteNotification";
      } else if(io.reactivex.d.j.n.c(var1)) {
         var2 = "OnErrorNotification[" + io.reactivex.d.j.n.f(var1) + "]";
      } else {
         var2 = "OnNextNotification[" + this.a + "]";
      }

      return var2;
   }
}
