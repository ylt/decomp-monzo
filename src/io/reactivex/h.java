package io.reactivex;

import java.util.concurrent.Callable;

public abstract class h implements l {
   public static h a() {
      return io.reactivex.g.a.a((h)io.reactivex.d.e.c.e.a);
   }

   public static h a(k var0) {
      io.reactivex.d.b.b.a(var0, (String)"onSubscribe is null");
      return io.reactivex.g.a.a((h)(new io.reactivex.d.e.c.c(var0)));
   }

   public static h a(Object var0) {
      io.reactivex.d.b.b.a(var0, "item is null");
      return io.reactivex.g.a.a((h)(new io.reactivex.d.e.c.n(var0)));
   }

   public static h a(Callable var0) {
      io.reactivex.d.b.b.a(var0, (String)"callable is null");
      return io.reactivex.g.a.a((h)(new io.reactivex.d.e.c.h(var0)));
   }

   public final io.reactivex.b.b a(io.reactivex.c.g var1, io.reactivex.c.g var2) {
      return this.a(var1, var2, io.reactivex.d.b.a.c);
   }

   public final io.reactivex.b.b a(io.reactivex.c.g var1, io.reactivex.c.g var2, io.reactivex.c.a var3) {
      io.reactivex.d.b.b.a(var1, (String)"onSuccess is null");
      io.reactivex.d.b.b.a(var2, (String)"onError is null");
      io.reactivex.d.b.b.a(var3, (String)"onComplete is null");
      return (io.reactivex.b.b)this.c((j)(new io.reactivex.d.e.c.b(var1, var2, var3)));
   }

   public final h a(io.reactivex.c.b var1) {
      io.reactivex.d.b.b.a(var1, (String)"onEvent is null");
      return io.reactivex.g.a.a((h)(new io.reactivex.d.e.c.d(this, var1)));
   }

   public final h a(io.reactivex.c.g var1) {
      return io.reactivex.g.a.a((h)(new io.reactivex.d.e.c.s(this, io.reactivex.d.b.a.b(), io.reactivex.d.b.a.b(), (io.reactivex.c.g)io.reactivex.d.b.b.a(var1, (String)"onError is null"), io.reactivex.d.b.a.c, io.reactivex.d.b.a.c, io.reactivex.d.b.a.c)));
   }

   public final h a(io.reactivex.c.q var1) {
      io.reactivex.d.b.b.a(var1, (String)"predicate is null");
      return io.reactivex.g.a.a((h)(new io.reactivex.d.e.c.q(this, var1)));
   }

   public final h a(l var1) {
      io.reactivex.d.b.b.a(var1, (String)"next is null");
      return this.d(io.reactivex.d.b.a.b((Object)var1));
   }

   public final h a(u var1) {
      io.reactivex.d.b.b.a(var1, (String)"scheduler is null");
      return io.reactivex.g.a.a((h)(new io.reactivex.d.e.c.p(this, var1)));
   }

   public final n a(io.reactivex.c.h var1) {
      return this.e().flatMap(var1);
   }

   public final void a(j var1) {
      io.reactivex.d.b.b.a(var1, (String)"observer is null");
      var1 = io.reactivex.g.a.a(this, var1);
      io.reactivex.d.b.b.a(var1, (String)"observer returned by the RxJavaPlugins hook is null");

      try {
         this.b(var1);
      } catch (NullPointerException var3) {
         throw var3;
      } catch (Throwable var4) {
         io.reactivex.exceptions.a.b(var4);
         NullPointerException var5 = new NullPointerException("subscribeActual failed");
         var5.initCause(var4);
         throw var5;
      }
   }

   public final b b(io.reactivex.c.h var1) {
      io.reactivex.d.b.b.a(var1, (String)"mapper is null");
      return io.reactivex.g.a.a((b)(new io.reactivex.d.e.c.g(this, var1)));
   }

   public final h b(io.reactivex.c.g var1) {
      return io.reactivex.g.a.a((h)(new io.reactivex.d.e.c.s(this, (io.reactivex.c.g)io.reactivex.d.b.b.a(var1, (String)"onSubscribe is null"), io.reactivex.d.b.a.b(), io.reactivex.d.b.a.b(), io.reactivex.d.b.a.c, io.reactivex.d.b.a.c, io.reactivex.d.b.a.c)));
   }

   public final h b(u var1) {
      io.reactivex.d.b.b.a(var1, (String)"scheduler is null");
      return io.reactivex.g.a.a((h)(new io.reactivex.d.e.c.t(this, var1)));
   }

   public final Object b() {
      io.reactivex.d.d.g var1 = new io.reactivex.d.d.g();
      this.a((j)var1);
      return var1.b();
   }

   protected abstract void b(j var1);

   public final b c() {
      return io.reactivex.g.a.a((b)(new io.reactivex.d.e.c.k(this)));
   }

   public final h c(io.reactivex.c.g var1) {
      return io.reactivex.g.a.a((h)(new io.reactivex.d.e.c.s(this, io.reactivex.d.b.a.b(), (io.reactivex.c.g)io.reactivex.d.b.b.a(var1, (String)"onSubscribe is null"), io.reactivex.d.b.a.b(), io.reactivex.d.b.a.c, io.reactivex.d.b.a.c, io.reactivex.d.b.a.c)));
   }

   public final h c(io.reactivex.c.h var1) {
      io.reactivex.d.b.b.a(var1, (String)"mapper is null");
      return io.reactivex.g.a.a((h)(new io.reactivex.d.e.c.o(this, var1)));
   }

   public final j c(j var1) {
      this.a(var1);
      return var1;
   }

   public final h d(io.reactivex.c.h var1) {
      io.reactivex.d.b.b.a(var1, (String)"resumeFunction is null");
      return io.reactivex.g.a.a((h)(new io.reactivex.d.e.c.r(this, var1, true)));
   }

   public final v d() {
      return io.reactivex.g.a.a((v)(new io.reactivex.d.e.c.m(this)));
   }

   public final n e() {
      n var1;
      if(this instanceof io.reactivex.d.c.c) {
         var1 = ((io.reactivex.d.c.c)this).q_();
      } else {
         var1 = io.reactivex.g.a.a((n)(new io.reactivex.d.e.c.u(this)));
      }

      return var1;
   }

   public final h f() {
      return this.a(io.reactivex.d.b.a.c());
   }
}
