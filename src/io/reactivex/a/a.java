package io.reactivex.a;

import android.os.Looper;
import io.reactivex.b.b;
import java.util.concurrent.atomic.AtomicBoolean;

public abstract class a implements b {
   private final AtomicBoolean a = new AtomicBoolean();

   protected abstract void a();

   public final void dispose() {
      if(this.a.compareAndSet(false, true)) {
         if(Looper.myLooper() == Looper.getMainLooper()) {
            this.a();
         } else {
            io.reactivex.a.b.a.a().a(new Runnable() {
               public void run() {
                  a.this.a();
               }
            });
         }
      }

   }

   public final boolean isDisposed() {
      return this.a.get();
   }
}
