package io.reactivex.a.b;

import android.os.Handler;
import android.os.Looper;
import io.reactivex.u;
import java.util.concurrent.Callable;

public final class a {
   private static final u a = io.reactivex.a.a.a.a(new Callable() {
      public u a() throws Exception {
         return a.a.a;
      }

      // $FF: synthetic method
      public Object call() throws Exception {
         return this.a();
      }
   });

   public static u a() {
      return io.reactivex.a.a.a.a(a);
   }

   private static final class a {
      static final u a = new b(new Handler(Looper.getMainLooper()));
   }
}
