package io.reactivex.a.b;

import android.os.Handler;
import android.os.Message;
import io.reactivex.u;
import io.reactivex.b.c;
import java.util.concurrent.TimeUnit;

final class b extends u {
   private final Handler b;

   b(Handler var1) {
      this.b = var1;
   }

   public io.reactivex.b.b a(Runnable var1, long var2, TimeUnit var4) {
      if(var1 == null) {
         throw new NullPointerException("run == null");
      } else if(var4 == null) {
         throw new NullPointerException("unit == null");
      } else {
         var1 = io.reactivex.g.a.a(var1);
         b.b var5 = new b.b(this.b, var1);
         this.b.postDelayed(var5, Math.max(0L, var4.toMillis(var2)));
         return var5;
      }
   }

   public u.c a() {
      return new b.a(this.b);
   }

   private static final class a extends u.c {
      private final Handler a;
      private volatile boolean b;

      a(Handler var1) {
         this.a = var1;
      }

      public io.reactivex.b.b a(Runnable var1, long var2, TimeUnit var4) {
         if(var1 == null) {
            throw new NullPointerException("run == null");
         } else if(var4 == null) {
            throw new NullPointerException("unit == null");
         } else {
            Object var6;
            if(this.b) {
               var6 = c.b();
            } else {
               var1 = io.reactivex.g.a.a(var1);
               b.b var5 = new b.b(this.a, var1);
               Message var7 = Message.obtain(this.a, var5);
               var7.obj = this;
               this.a.sendMessageDelayed(var7, Math.max(0L, var4.toMillis(var2)));
               var6 = var5;
               if(this.b) {
                  this.a.removeCallbacks(var5);
                  var6 = c.b();
               }
            }

            return (io.reactivex.b.b)var6;
         }
      }

      public void dispose() {
         this.b = true;
         this.a.removeCallbacksAndMessages(this);
      }

      public boolean isDisposed() {
         return this.b;
      }
   }

   private static final class b implements io.reactivex.b.b, Runnable {
      private final Handler a;
      private final Runnable b;
      private volatile boolean c;

      b(Handler var1, Runnable var2) {
         this.a = var1;
         this.b = var2;
      }

      public void dispose() {
         this.c = true;
         this.a.removeCallbacks(this);
      }

      public boolean isDisposed() {
         return this.c;
      }

      public void run() {
         try {
            this.b.run();
         } catch (Throwable var3) {
            IllegalStateException var1 = new IllegalStateException("Fatal Exception thrown on Scheduler.", var3);
            io.reactivex.g.a.a((Throwable)var1);
            Thread var2 = Thread.currentThread();
            var2.getUncaughtExceptionHandler().uncaughtException(var2, var1);
         }

      }
   }
}
