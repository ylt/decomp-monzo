package io.reactivex.a.a;

import io.reactivex.u;
import io.reactivex.c.h;
import java.util.concurrent.Callable;

public final class a {
   private static volatile h a;
   private static volatile h b;

   static u a(h var0, Callable var1) {
      u var2 = (u)a(var0, (Object)var1);
      if(var2 == null) {
         throw new NullPointerException("Scheduler Callable returned null");
      } else {
         return var2;
      }
   }

   public static u a(u var0) {
      if(var0 == null) {
         throw new NullPointerException("scheduler == null");
      } else {
         h var1 = b;
         if(var1 != null) {
            var0 = (u)a(var1, (Object)var0);
         }

         return var0;
      }
   }

   public static u a(Callable var0) {
      if(var0 == null) {
         throw new NullPointerException("scheduler == null");
      } else {
         h var1 = a;
         u var2;
         if(var1 == null) {
            var2 = b(var0);
         } else {
            var2 = a(var1, var0);
         }

         return var2;
      }
   }

   static Object a(h var0, Object var1) {
      try {
         Object var3 = var0.a(var1);
         return var3;
      } catch (Throwable var2) {
         throw io.reactivex.exceptions.a.a(var2);
      }
   }

   static u b(Callable param0) {
      // $FF: Couldn't be decompiled
   }
}
