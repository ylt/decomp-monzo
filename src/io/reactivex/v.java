package io.reactivex;

import java.util.NoSuchElementException;
import java.util.concurrent.Callable;

public abstract class v implements z {
   public static v a(io.reactivex.c.h var0, z... var1) {
      io.reactivex.d.b.b.a(var0, (String)"zipper is null");
      io.reactivex.d.b.b.a(var1, (String)"sources is null");
      v var2;
      if(var1.length == 0) {
         var2 = a((Throwable)(new NoSuchElementException()));
      } else {
         var2 = io.reactivex.g.a.a((v)(new io.reactivex.d.e.e.w(var1, var0)));
      }

      return var2;
   }

   private static v a(f var0) {
      return io.reactivex.g.a.a((v)(new io.reactivex.d.e.b.j(var0, (Object)null)));
   }

   public static v a(y var0) {
      io.reactivex.d.b.b.a(var0, (String)"source is null");
      return io.reactivex.g.a.a((v)(new io.reactivex.d.e.e.a(var0)));
   }

   public static v a(z var0, z var1, io.reactivex.c.c var2) {
      io.reactivex.d.b.b.a(var0, (String)"source1 is null");
      io.reactivex.d.b.b.a(var1, (String)"source2 is null");
      return a(io.reactivex.d.b.a.a(var2), new z[]{var0, var1});
   }

   public static v a(z var0, z var1, z var2, io.reactivex.c.i var3) {
      io.reactivex.d.b.b.a(var0, (String)"source1 is null");
      io.reactivex.d.b.b.a(var1, (String)"source2 is null");
      io.reactivex.d.b.b.a(var2, (String)"source3 is null");
      return a(io.reactivex.d.b.a.a(var3), new z[]{var0, var1, var2});
   }

   public static v a(Iterable var0, io.reactivex.c.h var1) {
      io.reactivex.d.b.b.a(var1, (String)"zipper is null");
      io.reactivex.d.b.b.a(var0, (String)"sources is null");
      return io.reactivex.g.a.a((v)(new io.reactivex.d.e.e.x(var0, var1)));
   }

   public static v a(Object var0) {
      io.reactivex.d.b.b.a(var0, "value is null");
      return io.reactivex.g.a.a((v)(new io.reactivex.d.e.e.n(var0)));
   }

   public static v a(Throwable var0) {
      io.reactivex.d.b.b.a(var0, (String)"error is null");
      return b(io.reactivex.d.b.a.a((Object)var0));
   }

   public static v a(Callable var0) {
      io.reactivex.d.b.b.a(var0, (String)"singleSupplier is null");
      return io.reactivex.g.a.a((v)(new io.reactivex.d.e.e.b(var0)));
   }

   public static v b(Callable var0) {
      io.reactivex.d.b.b.a(var0, (String)"errorSupplier is null");
      return io.reactivex.g.a.a((v)(new io.reactivex.d.e.e.j(var0)));
   }

   public static v c(Callable var0) {
      io.reactivex.d.b.b.a(var0, (String)"callable is null");
      return io.reactivex.g.a.a((v)(new io.reactivex.d.e.e.m(var0)));
   }

   public static v o_() {
      return io.reactivex.g.a.a(io.reactivex.d.e.e.p.a);
   }

   public final io.reactivex.b.b a(io.reactivex.c.g var1, io.reactivex.c.g var2) {
      io.reactivex.d.b.b.a(var1, (String)"onSuccess is null");
      io.reactivex.d.b.b.a(var2, (String)"onError is null");
      io.reactivex.d.d.j var3 = new io.reactivex.d.d.j(var1, var2);
      this.a((x)var3);
      return var3;
   }

   public final h a(io.reactivex.c.q var1) {
      io.reactivex.d.b.b.a(var1, (String)"predicate is null");
      return io.reactivex.g.a.a((h)(new io.reactivex.d.e.c.f(this, var1)));
   }

   public final v a(long var1) {
      return a(this.d().a(var1));
   }

   public final v a(io.reactivex.c.a var1) {
      io.reactivex.d.b.b.a(var1, (String)"onFinally is null");
      return io.reactivex.g.a.a((v)(new io.reactivex.d.e.e.e(this, var1)));
   }

   public final v a(io.reactivex.c.b var1) {
      io.reactivex.d.b.b.a(var1, (String)"onEvent is null");
      return io.reactivex.g.a.a((v)(new io.reactivex.d.e.e.g(this, var1)));
   }

   public final v a(io.reactivex.c.d var1) {
      return a(this.d().a(var1));
   }

   public final v a(io.reactivex.c.g var1) {
      io.reactivex.d.b.b.a(var1, (String)"doAfterSuccess is null");
      return io.reactivex.g.a.a((v)(new io.reactivex.d.e.e.d(this, var1)));
   }

   public final v a(io.reactivex.c.h var1) {
      io.reactivex.d.b.b.a(var1, (String)"mapper is null");
      return io.reactivex.g.a.a((v)(new io.reactivex.d.e.e.k(this, var1)));
   }

   public final v a(u var1) {
      io.reactivex.d.b.b.a(var1, (String)"scheduler is null");
      return io.reactivex.g.a.a((v)(new io.reactivex.d.e.e.q(this, var1)));
   }

   public final v a(v var1) {
      io.reactivex.d.b.b.a(var1, (String)"resumeSingleInCaseOfError is null");
      return this.f(io.reactivex.d.b.a.b((Object)var1));
   }

   public final v a(z var1, io.reactivex.c.c var2) {
      return a(this, var1, var2);
   }

   public final v a(Class var1) {
      io.reactivex.d.b.b.a(var1, (String)"clazz is null");
      return this.d(io.reactivex.d.b.a.a(var1));
   }

   public final void a(x var1) {
      io.reactivex.d.b.b.a(var1, (String)"subscriber is null");
      var1 = io.reactivex.g.a.a(this, var1);
      io.reactivex.d.b.b.a(var1, (String)"subscriber returned by the RxJavaPlugins hook is null");

      try {
         this.b(var1);
      } catch (NullPointerException var3) {
         throw var3;
      } catch (Throwable var4) {
         io.reactivex.exceptions.a.b(var4);
         NullPointerException var2 = new NullPointerException("subscribeActual failed");
         var2.initCause(var4);
         throw var2;
      }
   }

   public final n b(io.reactivex.c.h var1) {
      return this.f().flatMap(var1);
   }

   public final v b(io.reactivex.c.g var1) {
      io.reactivex.d.b.b.a(var1, (String)"onSubscribe is null");
      return io.reactivex.g.a.a((v)(new io.reactivex.d.e.e.h(this, var1)));
   }

   public final v b(u var1) {
      io.reactivex.d.b.b.a(var1, (String)"scheduler is null");
      return io.reactivex.g.a.a((v)(new io.reactivex.d.e.e.t(this, var1)));
   }

   public final v b(Object var1) {
      io.reactivex.d.b.b.a(var1, "value is null");
      return io.reactivex.g.a.a((v)(new io.reactivex.d.e.e.r(this, (io.reactivex.c.h)null, var1)));
   }

   public final Object b() {
      io.reactivex.d.d.g var1 = new io.reactivex.d.d.g();
      this.a((x)var1);
      return var1.b();
   }

   protected abstract void b(x var1);

   public final b c() {
      return io.reactivex.g.a.a((b)(new io.reactivex.d.e.a.g(this)));
   }

   public final b c(io.reactivex.c.h var1) {
      io.reactivex.d.b.b.a(var1, (String)"mapper is null");
      return io.reactivex.g.a.a((b)(new io.reactivex.d.e.e.l(this, var1)));
   }

   public final v c(io.reactivex.c.g var1) {
      io.reactivex.d.b.b.a(var1, (String)"onSuccess is null");
      return io.reactivex.g.a.a((v)(new io.reactivex.d.e.e.i(this, var1)));
   }

   public final f d() {
      f var1;
      if(this instanceof io.reactivex.d.c.a) {
         var1 = ((io.reactivex.d.c.a)this).a();
      } else {
         var1 = io.reactivex.g.a.a((f)(new io.reactivex.d.e.e.u(this)));
      }

      return var1;
   }

   public final v d(io.reactivex.c.g var1) {
      io.reactivex.d.b.b.a(var1, (String)"onError is null");
      return io.reactivex.g.a.a((v)(new io.reactivex.d.e.e.f(this, var1)));
   }

   public final v d(io.reactivex.c.h var1) {
      io.reactivex.d.b.b.a(var1, (String)"mapper is null");
      return io.reactivex.g.a.a((v)(new io.reactivex.d.e.e.o(this, var1)));
   }

   public final io.reactivex.b.b e(io.reactivex.c.g var1) {
      return this.a(var1, io.reactivex.d.b.a.f);
   }

   public final h e() {
      h var1;
      if(this instanceof io.reactivex.d.c.b) {
         var1 = ((io.reactivex.d.c.b)this).p_();
      } else {
         var1 = io.reactivex.g.a.a((h)(new io.reactivex.d.e.c.i(this)));
      }

      return var1;
   }

   public final v e(io.reactivex.c.h var1) {
      io.reactivex.d.b.b.a(var1, (String)"resumeFunction is null");
      return io.reactivex.g.a.a((v)(new io.reactivex.d.e.e.r(this, var1, (Object)null)));
   }

   public final n f() {
      n var1;
      if(this instanceof io.reactivex.d.c.c) {
         var1 = ((io.reactivex.d.c.c)this).q_();
      } else {
         var1 = io.reactivex.g.a.a((n)(new io.reactivex.d.e.e.v(this)));
      }

      return var1;
   }

   public final v f(io.reactivex.c.h var1) {
      io.reactivex.d.b.b.a(var1, (String)"resumeFunctionInCaseOfError is null");
      return io.reactivex.g.a.a((v)(new io.reactivex.d.e.e.s(this, var1)));
   }
}
