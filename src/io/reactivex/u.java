package io.reactivex;

import java.util.concurrent.TimeUnit;

public abstract class u {
   static final long a;

   static {
      a = TimeUnit.MINUTES.toNanos(Long.getLong("rx2.scheduler.drift-tolerance", 15L).longValue());
   }

   public long a(TimeUnit var1) {
      return var1.convert(System.currentTimeMillis(), TimeUnit.MILLISECONDS);
   }

   public io.reactivex.b.b a(Runnable var1) {
      return this.a(var1, 0L, TimeUnit.NANOSECONDS);
   }

   public io.reactivex.b.b a(Runnable var1, long var2, long var4, TimeUnit var6) {
      u.c var7 = this.a();
      Object var9 = new u.b(io.reactivex.g.a.a(var1), var7);
      io.reactivex.b.b var8 = var7.a((Runnable)var9, var2, var4, var6);
      if(var8 == io.reactivex.d.a.e.a) {
         var9 = var8;
      }

      return (io.reactivex.b.b)var9;
   }

   public io.reactivex.b.b a(Runnable var1, long var2, TimeUnit var4) {
      u.c var5 = this.a();
      u.a var6 = new u.a(io.reactivex.g.a.a(var1), var5);
      var5.a(var6, var2, var4);
      return var6;
   }

   public abstract u.c a();

   public void b() {
   }

   static final class a implements io.reactivex.b.b, Runnable {
      final Runnable a;
      final u.c b;
      Thread c;

      a(Runnable var1, u.c var2) {
         this.a = var1;
         this.b = var2;
      }

      public void dispose() {
         if(this.c == Thread.currentThread() && this.b instanceof io.reactivex.d.g.f) {
            ((io.reactivex.d.g.f)this.b).b();
         } else {
            this.b.dispose();
         }

      }

      public boolean isDisposed() {
         return this.b.isDisposed();
      }

      public void run() {
         this.c = Thread.currentThread();

         try {
            this.a.run();
         } finally {
            this.dispose();
            this.c = null;
         }

      }
   }

   static class b implements io.reactivex.b.b, Runnable {
      final Runnable a;
      final u.c b;
      volatile boolean c;

      b(Runnable var1, u.c var2) {
         this.a = var1;
         this.b = var2;
      }

      public void dispose() {
         this.c = true;
         this.b.dispose();
      }

      public boolean isDisposed() {
         return this.c;
      }

      public void run() {
         if(!this.c) {
            try {
               this.a.run();
            } catch (Throwable var2) {
               io.reactivex.exceptions.a.b(var2);
               this.b.dispose();
               throw io.reactivex.d.j.j.a(var2);
            }
         }

      }
   }

   public abstract static class c implements io.reactivex.b.b {
      public long a(TimeUnit var1) {
         return var1.convert(System.currentTimeMillis(), TimeUnit.MILLISECONDS);
      }

      public io.reactivex.b.b a(Runnable var1) {
         return this.a(var1, 0L, TimeUnit.NANOSECONDS);
      }

      public io.reactivex.b.b a(Runnable var1, long var2, long var4, TimeUnit var6) {
         io.reactivex.d.a.k var10 = new io.reactivex.d.a.k();
         io.reactivex.d.a.k var9 = new io.reactivex.d.a.k(var10);
         var1 = io.reactivex.g.a.a(var1);
         var4 = var6.toNanos(var4);
         long var7 = this.a(TimeUnit.NANOSECONDS);
         Object var11 = this.a(new u.a(var7 + var6.toNanos(var2), var1, var7, var9, var4), var2, var6);
         if(var11 != io.reactivex.d.a.e.a) {
            var10.b((io.reactivex.b.b)var11);
            var11 = var9;
         }

         return (io.reactivex.b.b)var11;
      }

      public abstract io.reactivex.b.b a(Runnable var1, long var2, TimeUnit var4);
   }

   final class a implements Runnable {
      final Runnable a;
      final io.reactivex.d.a.k b;
      final long c;
      long d;
      long e;
      long f;

      a(long var2, Runnable var4, long var5, io.reactivex.d.a.k var7, long var8) {
         this.a = var4;
         this.b = var7;
         this.c = var8;
         this.e = var5;
         this.f = var2;
      }

      public void run() {
         this.a.run();
         if(!this.b.isDisposed()) {
            long var3 = u.this.a(TimeUnit.NANOSECONDS);
            long var1;
            long var5;
            if(u.a + var3 >= this.e && var3 < this.e + this.c + u.a) {
               var1 = this.f;
               var5 = this.d + 1L;
               this.d = var5;
               var1 += var5 * this.c;
            } else {
               var1 = this.c + var3;
               var5 = this.c;
               long var7 = this.d + 1L;
               this.d = var7;
               this.f = var1 - var5 * var7;
            }

            this.e = var3;
            this.b.b(u.this.a(this, var1 - var3, TimeUnit.NANOSECONDS));
         }

      }
   }
}
