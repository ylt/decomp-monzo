package io.reactivex.g;

import io.reactivex.b;
import io.reactivex.f;
import io.reactivex.j;
import io.reactivex.n;
import io.reactivex.t;
import io.reactivex.u;
import io.reactivex.v;
import io.reactivex.x;
import io.reactivex.c.c;
import io.reactivex.c.e;
import io.reactivex.c.g;
import io.reactivex.c.h;
import io.reactivex.exceptions.CompositeException;
import io.reactivex.exceptions.MissingBackpressureException;
import io.reactivex.exceptions.OnErrorNotImplementedException;
import io.reactivex.exceptions.UndeliverableException;
import java.util.concurrent.Callable;

public final class a {
   static volatile g a;
   static volatile h b;
   static volatile h c;
   static volatile h d;
   static volatile h e;
   static volatile h f;
   static volatile h g;
   static volatile h h;
   static volatile h i;
   static volatile h j;
   static volatile h k;
   static volatile h l;
   static volatile h m;
   static volatile h n;
   static volatile c o;
   static volatile c p;
   static volatile c q;
   static volatile c r;
   static volatile c s;
   static volatile e t;
   static volatile boolean u;

   public static b a(b var0) {
      h var1 = n;
      if(var1 != null) {
         var0 = (b)a((h)var1, (Object)var0);
      }

      return var0;
   }

   public static io.reactivex.c a(b var0, io.reactivex.c var1) {
      c var2 = s;
      if(var2 != null) {
         var1 = (io.reactivex.c)a(var2, var0, var1);
      }

      return var1;
   }

   public static io.reactivex.e.a a(io.reactivex.e.a var0) {
      h var1 = k;
      if(var1 != null) {
         var0 = (io.reactivex.e.a)a((h)var1, (Object)var0);
      }

      return var0;
   }

   public static f a(f var0) {
      h var1 = i;
      if(var1 != null) {
         var0 = (f)a((h)var1, (Object)var0);
      }

      return var0;
   }

   public static io.reactivex.h a(io.reactivex.h var0) {
      h var1 = l;
      if(var1 != null) {
         var0 = (io.reactivex.h)a((h)var1, (Object)var0);
      }

      return var0;
   }

   public static j a(io.reactivex.h var0, j var1) {
      c var2 = p;
      j var3;
      if(var2 != null) {
         var3 = (j)a(var2, var0, var1);
      } else {
         var3 = var1;
      }

      return var3;
   }

   public static n a(n var0) {
      h var1 = j;
      if(var1 != null) {
         var0 = (n)a((h)var1, (Object)var0);
      }

      return var0;
   }

   public static t a(n var0, t var1) {
      c var2 = q;
      if(var2 != null) {
         var1 = (t)a(var2, var0, var1);
      }

      return var1;
   }

   static u a(h var0, Callable var1) {
      return (u)io.reactivex.d.b.b.a(a((h)var0, (Object)var1), "Scheduler Callable result can't be null");
   }

   public static u a(u var0) {
      h var1 = g;
      if(var1 != null) {
         var0 = (u)a((h)var1, (Object)var0);
      }

      return var0;
   }

   public static u a(Callable var0) {
      io.reactivex.d.b.b.a(var0, (String)"Scheduler Callable can't be null");
      h var1 = c;
      u var2;
      if(var1 == null) {
         var2 = e(var0);
      } else {
         var2 = a(var1, var0);
      }

      return var2;
   }

   public static v a(v var0) {
      h var1 = m;
      if(var1 != null) {
         var0 = (v)a((h)var1, (Object)var0);
      }

      return var0;
   }

   public static x a(v var0, x var1) {
      c var2 = r;
      x var3;
      if(var2 != null) {
         var3 = (x)a(var2, var0, var1);
      } else {
         var3 = var1;
      }

      return var3;
   }

   static Object a(c var0, Object var1, Object var2) {
      try {
         Object var4 = var0.a(var1, var2);
         return var4;
      } catch (Throwable var3) {
         throw io.reactivex.d.j.j.a(var3);
      }
   }

   static Object a(h var0, Object var1) {
      try {
         Object var3 = var0.a(var1);
         return var3;
      } catch (Throwable var2) {
         throw io.reactivex.d.j.j.a(var2);
      }
   }

   public static Runnable a(Runnable var0) {
      h var1 = b;
      if(var1 != null) {
         var0 = (Runnable)a((h)var1, (Object)var0);
      }

      return var0;
   }

   public static org.a.b a(f var0, org.a.b var1) {
      c var2 = o;
      if(var2 != null) {
         var1 = (org.a.b)a(var2, var0, var1);
      }

      return var1;
   }

   public static void a(Throwable var0) {
      g var2 = a;
      Object var1;
      if(var0 == null) {
         var1 = new NullPointerException("onError called with null. Null values are generally not allowed in 2.x operators and sources.");
      } else {
         var1 = var0;
         if(!b(var0)) {
            var1 = new UndeliverableException(var0);
         }
      }

      if(var2 != null) {
         try {
            var2.a(var1);
            return;
         } catch (Throwable var3) {
            var3.printStackTrace();
            c(var3);
         }
      }

      ((Throwable)var1).printStackTrace();
      c((Throwable)var1);
   }

   public static boolean a() {
      return u;
   }

   public static u b(u var0) {
      h var1 = h;
      if(var1 != null) {
         var0 = (u)a((h)var1, (Object)var0);
      }

      return var0;
   }

   public static u b(Callable var0) {
      io.reactivex.d.b.b.a(var0, (String)"Scheduler Callable can't be null");
      h var1 = e;
      u var2;
      if(var1 == null) {
         var2 = e(var0);
      } else {
         var2 = a(var1, var0);
      }

      return var2;
   }

   public static boolean b() {
      e var1 = t;
      boolean var0;
      if(var1 != null) {
         try {
            var0 = var1.a();
         } catch (Throwable var2) {
            throw io.reactivex.d.j.j.a(var2);
         }
      } else {
         var0 = false;
      }

      return var0;
   }

   static boolean b(Throwable var0) {
      boolean var2 = true;
      boolean var1;
      if(var0 instanceof OnErrorNotImplementedException) {
         var1 = var2;
      } else {
         var1 = var2;
         if(!(var0 instanceof MissingBackpressureException)) {
            var1 = var2;
            if(!(var0 instanceof IllegalStateException)) {
               var1 = var2;
               if(!(var0 instanceof NullPointerException)) {
                  var1 = var2;
                  if(!(var0 instanceof IllegalArgumentException)) {
                     var1 = var2;
                     if(!(var0 instanceof CompositeException)) {
                        var1 = false;
                     }
                  }
               }
            }
         }
      }

      return var1;
   }

   public static u c(Callable var0) {
      io.reactivex.d.b.b.a(var0, (String)"Scheduler Callable can't be null");
      h var1 = f;
      u var2;
      if(var1 == null) {
         var2 = e(var0);
      } else {
         var2 = a(var1, var0);
      }

      return var2;
   }

   static void c(Throwable var0) {
      Thread var1 = Thread.currentThread();
      var1.getUncaughtExceptionHandler().uncaughtException(var1, var0);
   }

   public static u d(Callable var0) {
      io.reactivex.d.b.b.a(var0, (String)"Scheduler Callable can't be null");
      h var1 = d;
      u var2;
      if(var1 == null) {
         var2 = e(var0);
      } else {
         var2 = a(var1, var0);
      }

      return var2;
   }

   static u e(Callable var0) {
      try {
         u var2 = (u)io.reactivex.d.b.b.a(var0.call(), "Scheduler Callable result can't be null");
         return var2;
      } catch (Throwable var1) {
         throw io.reactivex.d.j.j.a(var1);
      }
   }
}
