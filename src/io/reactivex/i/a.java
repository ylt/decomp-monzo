package io.reactivex.i;

import io.reactivex.t;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

public final class a extends c {
   static final a.a[] a = new a.a[0];
   static final a.a[] b = new a.a[0];
   final AtomicReference c;
   Throwable d;

   a() {
      this.c = new AtomicReference(b);
   }

   public static a a() {
      return new a();
   }

   boolean a(a.a var1) {
      while(true) {
         a.a[] var5 = (a.a[])this.c.get();
         boolean var3;
         if(var5 == a) {
            var3 = false;
         } else {
            int var2 = var5.length;
            a.a[] var4 = new a.a[var2 + 1];
            System.arraycopy(var5, 0, var4, 0, var2);
            var4[var2] = var1;
            if(!this.c.compareAndSet(var5, var4)) {
               continue;
            }

            var3 = true;
         }

         return var3;
      }
   }

   void b(a.a var1) {
      while(true) {
         a.a[] var7 = (a.a[])this.c.get();
         if(var7 != a && var7 != b) {
            int var5 = var7.length;
            byte var4 = -1;
            int var2 = 0;

            int var3;
            while(true) {
               var3 = var4;
               if(var2 >= var5) {
                  break;
               }

               if(var7[var2] == var1) {
                  var3 = var2;
                  break;
               }

               ++var2;
            }

            if(var3 >= 0) {
               a.a[] var6;
               if(var5 == 1) {
                  var6 = b;
               } else {
                  var6 = new a.a[var5 - 1];
                  System.arraycopy(var7, 0, var6, 0, var3);
                  System.arraycopy(var7, var3 + 1, var6, var3, var5 - var3 - 1);
               }

               if(!this.c.compareAndSet(var7, var6)) {
                  continue;
               }
            }
         }

         return;
      }
   }

   public void onComplete() {
      if(this.c.get() != a) {
         a.a[] var3 = (a.a[])this.c.getAndSet(a);
         int var2 = var3.length;

         for(int var1 = 0; var1 < var2; ++var1) {
            var3[var1].a();
         }
      }

   }

   public void onError(Throwable var1) {
      if(this.c.get() == a) {
         io.reactivex.g.a.a(var1);
      } else {
         Object var4 = var1;
         if(var1 == null) {
            var4 = new NullPointerException("onError called with null. Null values are generally not allowed in 2.x operators and sources.");
         }

         this.d = (Throwable)var4;
         a.a[] var5 = (a.a[])this.c.getAndSet(a);
         int var3 = var5.length;

         for(int var2 = 0; var2 < var3; ++var2) {
            var5[var2].a((Throwable)var4);
         }
      }

   }

   public void onNext(Object var1) {
      if(this.c.get() != a) {
         if(var1 == null) {
            this.onError(new NullPointerException("onNext called with null. Null values are generally not allowed in 2.x operators and sources."));
         } else {
            a.a[] var4 = (a.a[])this.c.get();
            int var3 = var4.length;

            for(int var2 = 0; var2 < var3; ++var2) {
               var4[var2].a(var1);
            }
         }
      }

   }

   public void onSubscribe(io.reactivex.b.b var1) {
      if(this.c.get() == a) {
         var1.dispose();
      }

   }

   public void subscribeActual(t var1) {
      a.a var2 = new a.a(var1, this);
      var1.onSubscribe(var2);
      if(this.a(var2)) {
         if(var2.isDisposed()) {
            this.b(var2);
         }
      } else {
         Throwable var3 = this.d;
         if(var3 != null) {
            var1.onError(var3);
         } else {
            var1.onComplete();
         }
      }

   }

   static final class a extends AtomicBoolean implements io.reactivex.b.b {
      final t a;
      final a b;

      a(t var1, a var2) {
         this.a = var1;
         this.b = var2;
      }

      public void a() {
         if(!this.get()) {
            this.a.onComplete();
         }

      }

      public void a(Object var1) {
         if(!this.get()) {
            this.a.onNext(var1);
         }

      }

      public void a(Throwable var1) {
         if(this.get()) {
            io.reactivex.g.a.a(var1);
         } else {
            this.a.onError(var1);
         }

      }

      public void dispose() {
         if(this.compareAndSet(false, true)) {
            this.b.b(this);
         }

      }

      public boolean isDisposed() {
         return this.get();
      }
   }
}
