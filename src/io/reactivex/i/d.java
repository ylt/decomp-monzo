package io.reactivex.i;

import io.reactivex.t;
import io.reactivex.d.a.e;
import io.reactivex.d.c.i;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

public final class d extends c {
   final io.reactivex.d.f.c a;
   final AtomicReference b;
   final AtomicReference c;
   final boolean d;
   volatile boolean e;
   volatile boolean f;
   Throwable g;
   final AtomicBoolean h;
   final io.reactivex.d.d.b i;
   boolean j;

   d(int var1, Runnable var2, boolean var3) {
      this.a = new io.reactivex.d.f.c(io.reactivex.d.b.b.a(var1, "capacityHint"));
      this.c = new AtomicReference(io.reactivex.d.b.b.a(var2, (String)"onTerminate"));
      this.d = var3;
      this.b = new AtomicReference();
      this.h = new AtomicBoolean();
      this.i = new d.a();
   }

   d(int var1, boolean var2) {
      this.a = new io.reactivex.d.f.c(io.reactivex.d.b.b.a(var1, "capacityHint"));
      this.c = new AtomicReference();
      this.d = var2;
      this.b = new AtomicReference();
      this.h = new AtomicBoolean();
      this.i = new d.a();
   }

   public static d a() {
      return new d(bufferSize(), true);
   }

   public static d a(int var0) {
      return new d(var0, true);
   }

   public static d a(int var0, Runnable var1) {
      return new d(var0, var1, true);
   }

   void a(t var1) {
      io.reactivex.d.f.c var8 = this.a;
      boolean var4;
      if(!this.d) {
         var4 = true;
      } else {
         var4 = false;
      }

      boolean var2 = true;
      int var5 = 1;

      while(true) {
         if(this.e) {
            this.b.lazySet((Object)null);
            var8.c();
            break;
         }

         boolean var7 = this.f;
         Object var9 = this.a.n_();
         boolean var6;
         if(var9 == null) {
            var6 = true;
         } else {
            var6 = false;
         }

         boolean var3 = var2;
         if(var7) {
            var3 = var2;
            if(var4) {
               var3 = var2;
               if(var2) {
                  if(this.a(var8, var1)) {
                     break;
                  }

                  var3 = false;
               }
            }

            if(var6) {
               this.c(var1);
               break;
            }
         }

         if(var6) {
            int var10 = this.i.addAndGet(-var5);
            var2 = var3;
            var5 = var10;
            if(var10 == 0) {
               break;
            }
         } else {
            var1.onNext(var9);
            var2 = var3;
         }
      }

   }

   boolean a(i var1, t var2) {
      Throwable var4 = this.g;
      boolean var3;
      if(var4 != null) {
         this.b.lazySet((Object)null);
         var1.c();
         var2.onError(var4);
         var3 = true;
      } else {
         var3 = false;
      }

      return var3;
   }

   void b(t var1) {
      int var3 = 1;
      io.reactivex.d.f.c var6 = this.a;
      boolean var2;
      if(!this.d) {
         var2 = true;
      } else {
         var2 = false;
      }

      int var4;
      do {
         if(this.e) {
            this.b.lazySet((Object)null);
            var6.c();
            break;
         }

         boolean var5 = this.f;
         if(var2 && var5 && this.a(var6, var1)) {
            break;
         }

         var1.onNext((Object)null);
         if(var5) {
            this.c(var1);
            break;
         }

         var4 = this.i.addAndGet(-var3);
         var3 = var4;
      } while(var4 != 0);

   }

   void c() {
      Runnable var1 = (Runnable)this.c.get();
      if(var1 != null && this.c.compareAndSet(var1, (Object)null)) {
         var1.run();
      }

   }

   void c(t var1) {
      this.b.lazySet((Object)null);
      Throwable var2 = this.g;
      if(var2 != null) {
         var1.onError(var2);
      } else {
         var1.onComplete();
      }

   }

   void d() {
      if(this.i.getAndIncrement() == 0) {
         t var2 = (t)this.b.get();
         int var1 = 1;

         while(true) {
            if(var2 != null) {
               if(this.j) {
                  this.b(var2);
               } else {
                  this.a(var2);
               }
               break;
            }

            var1 = this.i.addAndGet(-var1);
            if(var1 == 0) {
               break;
            }

            var2 = (t)this.b.get();
         }
      }

   }

   public void onComplete() {
      if(!this.f && !this.e) {
         this.f = true;
         this.c();
         this.d();
      }

   }

   public void onError(Throwable var1) {
      if(!this.f && !this.e) {
         Object var2 = var1;
         if(var1 == null) {
            var2 = new NullPointerException("onError called with null. Null values are generally not allowed in 2.x operators and sources.");
         }

         this.g = (Throwable)var2;
         this.f = true;
         this.c();
         this.d();
      } else {
         io.reactivex.g.a.a(var1);
      }

   }

   public void onNext(Object var1) {
      if(!this.f && !this.e) {
         if(var1 == null) {
            this.onError(new NullPointerException("onNext called with null. Null values are generally not allowed in 2.x operators and sources."));
         } else {
            this.a.a(var1);
            this.d();
         }
      }

   }

   public void onSubscribe(io.reactivex.b.b var1) {
      if(this.f || this.e) {
         var1.dispose();
      }

   }

   protected void subscribeActual(t var1) {
      if(!this.h.get() && this.h.compareAndSet(false, true)) {
         var1.onSubscribe(this.i);
         this.b.lazySet(var1);
         if(this.e) {
            this.b.lazySet((Object)null);
         } else {
            this.d();
         }
      } else {
         e.a(new IllegalStateException("Only a single observer allowed."), (t)var1);
      }

   }

   final class a extends io.reactivex.d.d.b {
      public int a(int var1) {
         byte var2;
         if((var1 & 2) != 0) {
            d.this.j = true;
            var2 = 2;
         } else {
            var2 = 0;
         }

         return var2;
      }

      public boolean b() {
         return d.this.a.b();
      }

      public void c() {
         d.this.a.c();
      }

      public void dispose() {
         if(!d.this.e) {
            d.this.e = true;
            d.this.c();
            d.this.b.lazySet((Object)null);
            if(d.this.i.getAndIncrement() == 0) {
               d.this.b.lazySet((Object)null);
               d.this.a.c();
            }
         }

      }

      public boolean isDisposed() {
         return d.this.e;
      }

      public Object n_() throws Exception {
         return d.this.a.n_();
      }
   }
}
