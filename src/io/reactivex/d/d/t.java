package io.reactivex.d.d;

public abstract class t extends v implements io.reactivex.d.j.o, io.reactivex.t {
   protected final io.reactivex.t a;
   protected final io.reactivex.d.c.h b;
   protected volatile boolean c;
   protected volatile boolean d;
   protected Throwable e;

   public t(io.reactivex.t var1, io.reactivex.d.c.h var2) {
      this.a = var1;
      this.b = var2;
   }

   public final int a(int var1) {
      return this.f.addAndGet(var1);
   }

   public void a(io.reactivex.t var1, Object var2) {
   }

   protected final void a(Object var1, boolean var2, io.reactivex.b.b var3) {
      io.reactivex.t var4 = this.a;
      io.reactivex.d.c.h var5 = this.b;
      if(this.f.get() == 0 && this.f.compareAndSet(0, 1)) {
         this.a(var4, var1);
         if(this.a(-1) == 0) {
            return;
         }
      } else {
         var5.a(var1);
         if(!this.c()) {
            return;
         }
      }

      io.reactivex.d.j.r.a(var5, var4, var2, var3, this);
   }

   public final boolean a() {
      return this.c;
   }

   protected final void b(Object var1, boolean var2, io.reactivex.b.b var3) {
      io.reactivex.t var5 = this.a;
      io.reactivex.d.c.h var4 = this.b;
      if(this.f.get() == 0 && this.f.compareAndSet(0, 1)) {
         if(var4.b()) {
            this.a(var5, var1);
            if(this.a(-1) == 0) {
               return;
            }
         } else {
            var4.a(var1);
         }
      } else {
         var4.a(var1);
         if(!this.c()) {
            return;
         }
      }

      io.reactivex.d.j.r.a(var4, var5, var2, var3, this);
   }

   public final boolean b() {
      return this.d;
   }

   public final boolean c() {
      boolean var1;
      if(this.f.getAndIncrement() == 0) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public final boolean d() {
      boolean var1 = true;
      if(this.f.get() != 0 || !this.f.compareAndSet(0, 1)) {
         var1 = false;
      }

      return var1;
   }

   public final Throwable e() {
      return this.e;
   }
}
