package io.reactivex.d.d;

import java.util.Queue;
import java.util.concurrent.atomic.AtomicReference;

public final class h extends AtomicReference implements io.reactivex.b.b, io.reactivex.t {
   public static final Object a = new Object();
   final Queue b;

   public h(Queue var1) {
      this.b = var1;
   }

   public void dispose() {
      if(io.reactivex.d.a.d.a((AtomicReference)this)) {
         this.b.offer(a);
      }

   }

   public boolean isDisposed() {
      boolean var1;
      if(this.get() == io.reactivex.d.a.d.a) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public void onComplete() {
      this.b.offer(io.reactivex.d.j.n.a());
   }

   public void onError(Throwable var1) {
      this.b.offer(io.reactivex.d.j.n.a(var1));
   }

   public void onNext(Object var1) {
      this.b.offer(io.reactivex.d.j.n.a(var1));
   }

   public void onSubscribe(io.reactivex.b.b var1) {
      io.reactivex.d.a.d.b(this, var1);
   }
}
