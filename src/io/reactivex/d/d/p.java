package io.reactivex.d.d;

import java.util.NoSuchElementException;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicReference;

public final class p extends CountDownLatch implements io.reactivex.b.b, io.reactivex.t, Future {
   Object a;
   Throwable b;
   final AtomicReference c = new AtomicReference();

   public p() {
      super(1);
   }

   public boolean cancel(boolean var1) {
      while(true) {
         io.reactivex.b.b var2 = (io.reactivex.b.b)this.c.get();
         if(var2 != this && var2 != io.reactivex.d.a.d.a) {
            if(!this.c.compareAndSet(var2, io.reactivex.d.a.d.a)) {
               continue;
            }

            if(var2 != null) {
               var2.dispose();
            }

            this.countDown();
            var1 = true;
            break;
         }

         var1 = false;
         break;
      }

      return var1;
   }

   public void dispose() {
   }

   public Object get() throws InterruptedException, ExecutionException {
      if(this.getCount() != 0L) {
         io.reactivex.d.j.e.a();
         this.await();
      }

      if(this.isCancelled()) {
         throw new CancellationException();
      } else {
         Throwable var1 = this.b;
         if(var1 != null) {
            throw new ExecutionException(var1);
         } else {
            return this.a;
         }
      }
   }

   public Object get(long var1, TimeUnit var3) throws InterruptedException, ExecutionException, TimeoutException {
      if(this.getCount() != 0L) {
         io.reactivex.d.j.e.a();
         if(!this.await(var1, var3)) {
            throw new TimeoutException();
         }
      }

      if(this.isCancelled()) {
         throw new CancellationException();
      } else {
         Throwable var4 = this.b;
         if(var4 != null) {
            throw new ExecutionException(var4);
         } else {
            return this.a;
         }
      }
   }

   public boolean isCancelled() {
      return io.reactivex.d.a.d.a((io.reactivex.b.b)this.c.get());
   }

   public boolean isDisposed() {
      return this.isDone();
   }

   public boolean isDone() {
      boolean var1;
      if(this.getCount() == 0L) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public void onComplete() {
      if(this.a == null) {
         this.onError(new NoSuchElementException("The source is empty"));
      } else {
         while(true) {
            io.reactivex.b.b var1 = (io.reactivex.b.b)this.c.get();
            if(var1 == this || var1 == io.reactivex.d.a.d.a) {
               break;
            }

            if(this.c.compareAndSet(var1, this)) {
               this.countDown();
               break;
            }
         }
      }

   }

   public void onError(Throwable var1) {
      if(this.b == null) {
         this.b = var1;

         while(true) {
            io.reactivex.b.b var2 = (io.reactivex.b.b)this.c.get();
            if(var2 == this || var2 == io.reactivex.d.a.d.a) {
               io.reactivex.g.a.a(var1);
               break;
            }

            if(this.c.compareAndSet(var2, this)) {
               this.countDown();
               break;
            }
         }
      } else {
         io.reactivex.g.a.a(var1);
      }

   }

   public void onNext(Object var1) {
      if(this.a != null) {
         ((io.reactivex.b.b)this.c.get()).dispose();
         this.onError(new IndexOutOfBoundsException("More than one element received"));
      } else {
         this.a = var1;
      }

   }

   public void onSubscribe(io.reactivex.b.b var1) {
      io.reactivex.d.a.d.b(this.c, var1);
   }
}
