package io.reactivex.d.d;

import io.reactivex.exceptions.CompositeException;
import java.util.concurrent.atomic.AtomicReference;

public final class j extends AtomicReference implements io.reactivex.b.b, io.reactivex.x {
   final io.reactivex.c.g a;
   final io.reactivex.c.g b;

   public j(io.reactivex.c.g var1, io.reactivex.c.g var2) {
      this.a = var1;
      this.b = var2;
   }

   public void a_(Object var1) {
      this.lazySet(io.reactivex.d.a.d.a);

      try {
         this.a.a(var1);
      } catch (Throwable var2) {
         io.reactivex.exceptions.a.b(var2);
         io.reactivex.g.a.a(var2);
      }

   }

   public void dispose() {
      io.reactivex.d.a.d.a((AtomicReference)this);
   }

   public boolean isDisposed() {
      boolean var1;
      if(this.get() == io.reactivex.d.a.d.a) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public void onError(Throwable var1) {
      this.lazySet(io.reactivex.d.a.d.a);

      try {
         this.b.a(var1);
      } catch (Throwable var3) {
         io.reactivex.exceptions.a.b(var3);
         io.reactivex.g.a.a((Throwable)(new CompositeException(new Throwable[]{var1, var3})));
      }

   }

   public void onSubscribe(io.reactivex.b.b var1) {
      io.reactivex.d.a.d.b(this, var1);
   }
}
