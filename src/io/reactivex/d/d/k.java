package io.reactivex.d.d;

public class k extends b {
   protected final io.reactivex.t a;
   protected Object b;

   public k(io.reactivex.t var1) {
      this.a = var1;
   }

   public final int a(int var1) {
      byte var2;
      if((var1 & 2) != 0) {
         this.lazySet(8);
         var2 = 2;
      } else {
         var2 = 0;
      }

      return var2;
   }

   public final void a(Throwable var1) {
      if((this.get() & 54) != 0) {
         io.reactivex.g.a.a(var1);
      } else {
         this.lazySet(2);
         this.a.onError(var1);
      }

   }

   public final void b(Object var1) {
      int var2 = this.get();
      if((var2 & 54) == 0) {
         if(var2 == 8) {
            this.b = var1;
            this.lazySet(16);
         } else {
            this.lazySet(2);
         }

         io.reactivex.t var3 = this.a;
         var3.onNext(var1);
         if(this.get() != 4) {
            var3.onComplete();
         }
      }

   }

   public final boolean b() {
      boolean var1;
      if(this.get() != 16) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public final void c() {
      this.lazySet(32);
      this.b = null;
   }

   public final void d() {
      if((this.get() & 54) == 0) {
         this.lazySet(2);
         this.a.onComplete();
      }

   }

   public void dispose() {
      this.set(4);
      this.b = null;
   }

   public final boolean isDisposed() {
      boolean var1;
      if(this.get() == 4) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public final Object n_() throws Exception {
      Object var1;
      if(this.get() == 16) {
         var1 = this.b;
         this.b = null;
         this.lazySet(32);
      } else {
         var1 = null;
      }

      return var1;
   }
}
