package io.reactivex.d.d;

import io.reactivex.exceptions.OnErrorNotImplementedException;
import java.util.concurrent.atomic.AtomicReference;

public final class i extends AtomicReference implements io.reactivex.b.b, io.reactivex.c, io.reactivex.c.g {
   final io.reactivex.c.g a;
   final io.reactivex.c.a b;

   public i(io.reactivex.c.g var1, io.reactivex.c.a var2) {
      this.a = var1;
      this.b = var2;
   }

   public void a(Throwable var1) {
      io.reactivex.g.a.a((Throwable)(new OnErrorNotImplementedException(var1)));
   }

   public void dispose() {
      io.reactivex.d.a.d.a((AtomicReference)this);
   }

   public boolean isDisposed() {
      boolean var1;
      if(this.get() == io.reactivex.d.a.d.a) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public void onComplete() {
      try {
         this.b.a();
      } catch (Throwable var2) {
         io.reactivex.exceptions.a.b(var2);
         io.reactivex.g.a.a(var2);
      }

      this.lazySet(io.reactivex.d.a.d.a);
   }

   public void onError(Throwable var1) {
      try {
         this.a.a(var1);
      } catch (Throwable var2) {
         io.reactivex.exceptions.a.b(var2);
         io.reactivex.g.a.a(var2);
      }

      this.lazySet(io.reactivex.d.a.d.a);
   }

   public void onSubscribe(io.reactivex.b.b var1) {
      io.reactivex.d.a.d.b(this, var1);
   }
}
