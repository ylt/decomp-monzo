package io.reactivex.d.d;

import java.util.concurrent.atomic.AtomicReference;

public final class q extends AtomicReference implements io.reactivex.b.b, io.reactivex.t {
   final r a;
   final int b;
   io.reactivex.d.c.i c;
   volatile boolean d;
   int e;

   public q(r var1, int var2) {
      this.a = var1;
      this.b = var2;
   }

   public boolean a() {
      return this.d;
   }

   public void b() {
      this.d = true;
   }

   public io.reactivex.d.c.i c() {
      return this.c;
   }

   public void dispose() {
      io.reactivex.d.a.d.a((AtomicReference)this);
   }

   public boolean isDisposed() {
      return io.reactivex.d.a.d.a((io.reactivex.b.b)this.get());
   }

   public void onComplete() {
      this.a.a(this);
   }

   public void onError(Throwable var1) {
      this.a.a(this, var1);
   }

   public void onNext(Object var1) {
      if(this.e == 0) {
         this.a.a(this, var1);
      } else {
         this.a.a();
      }

   }

   public void onSubscribe(io.reactivex.b.b var1) {
      if(io.reactivex.d.a.d.b(this, var1)) {
         if(var1 instanceof io.reactivex.d.c.d) {
            io.reactivex.d.c.d var3 = (io.reactivex.d.c.d)var1;
            int var2 = var3.a(3);
            if(var2 == 1) {
               this.e = var2;
               this.c = var3;
               this.d = true;
               this.a.a(this);
               return;
            }

            if(var2 == 2) {
               this.e = var2;
               this.c = var3;
               return;
            }
         }

         this.c = io.reactivex.d.j.r.a(-this.b);
      }

   }
}
