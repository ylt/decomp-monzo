package io.reactivex.d.d;

public final class l implements io.reactivex.b.b, io.reactivex.t {
   final io.reactivex.t a;
   final io.reactivex.c.g b;
   final io.reactivex.c.a c;
   io.reactivex.b.b d;

   public l(io.reactivex.t var1, io.reactivex.c.g var2, io.reactivex.c.a var3) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
   }

   public void dispose() {
      try {
         this.c.a();
      } catch (Throwable var2) {
         io.reactivex.exceptions.a.b(var2);
         io.reactivex.g.a.a(var2);
      }

      this.d.dispose();
   }

   public boolean isDisposed() {
      return this.d.isDisposed();
   }

   public void onComplete() {
      if(this.d != io.reactivex.d.a.d.a) {
         this.a.onComplete();
      }

   }

   public void onError(Throwable var1) {
      if(this.d != io.reactivex.d.a.d.a) {
         this.a.onError(var1);
      } else {
         io.reactivex.g.a.a(var1);
      }

   }

   public void onNext(Object var1) {
      this.a.onNext(var1);
   }

   public void onSubscribe(io.reactivex.b.b var1) {
      try {
         this.b.a(var1);
      } catch (Throwable var3) {
         io.reactivex.exceptions.a.b(var3);
         var1.dispose();
         this.d = io.reactivex.d.a.d.a;
         io.reactivex.d.a.e.a(var3, this.a);
         return;
      }

      if(io.reactivex.d.a.d.a(this.d, var1)) {
         this.d = var1;
         this.a.onSubscribe(this);
      }

   }
}
