package io.reactivex.d.d;

import java.util.concurrent.atomic.AtomicReference;

public final class x implements io.reactivex.x {
   final AtomicReference a;
   final io.reactivex.x b;

   public x(AtomicReference var1, io.reactivex.x var2) {
      this.a = var1;
      this.b = var2;
   }

   public void a_(Object var1) {
      this.b.a_(var1);
   }

   public void onError(Throwable var1) {
      this.b.onError(var1);
   }

   public void onSubscribe(io.reactivex.b.b var1) {
      io.reactivex.d.a.d.c(this.a, var1);
   }
}
