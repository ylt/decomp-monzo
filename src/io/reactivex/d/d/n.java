package io.reactivex.d.d;

import io.reactivex.exceptions.CompositeException;
import java.util.concurrent.atomic.AtomicReference;

public final class n extends AtomicReference implements io.reactivex.b.b, io.reactivex.t {
   final io.reactivex.c.q a;
   final io.reactivex.c.g b;
   final io.reactivex.c.a c;
   boolean d;

   public n(io.reactivex.c.q var1, io.reactivex.c.g var2, io.reactivex.c.a var3) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
   }

   public void dispose() {
      io.reactivex.d.a.d.a((AtomicReference)this);
   }

   public boolean isDisposed() {
      return io.reactivex.d.a.d.a((io.reactivex.b.b)this.get());
   }

   public void onComplete() {
      if(!this.d) {
         this.d = true;

         try {
            this.c.a();
         } catch (Throwable var2) {
            io.reactivex.exceptions.a.b(var2);
            io.reactivex.g.a.a(var2);
         }
      }

   }

   public void onError(Throwable var1) {
      if(this.d) {
         io.reactivex.g.a.a(var1);
      } else {
         this.d = true;

         try {
            this.b.a(var1);
         } catch (Throwable var3) {
            io.reactivex.exceptions.a.b(var3);
            io.reactivex.g.a.a((Throwable)(new CompositeException(new Throwable[]{var1, var3})));
         }
      }

   }

   public void onNext(Object var1) {
      if(!this.d) {
         boolean var2;
         try {
            var2 = this.a.a(var1);
         } catch (Throwable var3) {
            io.reactivex.exceptions.a.b(var3);
            this.dispose();
            this.onError(var3);
            return;
         }

         if(!var2) {
            this.dispose();
            this.onComplete();
         }
      }

   }

   public void onSubscribe(io.reactivex.b.b var1) {
      io.reactivex.d.a.d.b(this, var1);
   }
}
