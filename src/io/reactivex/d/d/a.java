package io.reactivex.d.d;

public abstract class a implements io.reactivex.d.c.d, io.reactivex.t {
   protected final io.reactivex.t a;
   protected io.reactivex.b.b b;
   protected io.reactivex.d.c.d c;
   protected boolean d;
   protected int e;

   public a(io.reactivex.t var1) {
      this.a = var1;
   }

   protected final void a(Throwable var1) {
      io.reactivex.exceptions.a.b(var1);
      this.b.dispose();
      this.onError(var1);
   }

   public final boolean a(Object var1) {
      throw new UnsupportedOperationException("Should not be called!");
   }

   protected final int b(int var1) {
      io.reactivex.d.c.d var3 = this.c;
      if(var3 != null && (var1 & 4) == 0) {
         int var2 = var3.a(var1);
         var1 = var2;
         if(var2 != 0) {
            this.e = var2;
            var1 = var2;
         }
      } else {
         var1 = 0;
      }

      return var1;
   }

   public boolean b() {
      return this.c.b();
   }

   public void c() {
      this.c.c();
   }

   protected boolean d() {
      return true;
   }

   public void dispose() {
      this.b.dispose();
   }

   protected void e() {
   }

   public boolean isDisposed() {
      return this.b.isDisposed();
   }

   public void onComplete() {
      if(!this.d) {
         this.d = true;
         this.a.onComplete();
      }

   }

   public void onError(Throwable var1) {
      if(this.d) {
         io.reactivex.g.a.a(var1);
      } else {
         this.d = true;
         this.a.onError(var1);
      }

   }

   public final void onSubscribe(io.reactivex.b.b var1) {
      if(io.reactivex.d.a.d.a(this.b, var1)) {
         this.b = var1;
         if(var1 instanceof io.reactivex.d.c.d) {
            this.c = (io.reactivex.d.c.d)var1;
         }

         if(this.d()) {
            this.a.onSubscribe(this);
            this.e();
         }
      }

   }
}
