package io.reactivex.d.d;

import java.util.concurrent.CountDownLatch;

public abstract class d extends CountDownLatch implements io.reactivex.b.b, io.reactivex.t {
   Object a;
   Throwable b;
   io.reactivex.b.b c;
   volatile boolean d;

   public d() {
      super(1);
   }

   public final Object a() {
      if(this.getCount() != 0L) {
         try {
            io.reactivex.d.j.e.a();
            this.await();
         } catch (InterruptedException var2) {
            this.dispose();
            throw io.reactivex.d.j.j.a((Throwable)var2);
         }
      }

      Throwable var1 = this.b;
      if(var1 != null) {
         throw io.reactivex.d.j.j.a(var1);
      } else {
         return this.a;
      }
   }

   public final void dispose() {
      this.d = true;
      io.reactivex.b.b var1 = this.c;
      if(var1 != null) {
         var1.dispose();
      }

   }

   public final boolean isDisposed() {
      return this.d;
   }

   public final void onComplete() {
      this.countDown();
   }

   public final void onSubscribe(io.reactivex.b.b var1) {
      this.c = var1;
      if(this.d) {
         var1.dispose();
      }

   }
}
