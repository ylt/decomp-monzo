package io.reactivex.d.d;

import java.util.concurrent.CountDownLatch;

public final class g extends CountDownLatch implements io.reactivex.c, io.reactivex.j, io.reactivex.x {
   Object a;
   Throwable b;
   io.reactivex.b.b c;
   volatile boolean d;

   public g() {
      super(1);
   }

   void a() {
      this.d = true;
      io.reactivex.b.b var1 = this.c;
      if(var1 != null) {
         var1.dispose();
      }

   }

   public void a_(Object var1) {
      this.a = var1;
      this.countDown();
   }

   public Object b() {
      if(this.getCount() != 0L) {
         try {
            io.reactivex.d.j.e.a();
            this.await();
         } catch (InterruptedException var2) {
            this.a();
            throw io.reactivex.d.j.j.a((Throwable)var2);
         }
      }

      Throwable var1 = this.b;
      if(var1 != null) {
         throw io.reactivex.d.j.j.a(var1);
      } else {
         return this.a;
      }
   }

   public void onComplete() {
      this.countDown();
   }

   public void onError(Throwable var1) {
      this.b = var1;
      this.countDown();
   }

   public void onSubscribe(io.reactivex.b.b var1) {
      this.c = var1;
      if(this.d) {
         var1.dispose();
      }

   }
}
