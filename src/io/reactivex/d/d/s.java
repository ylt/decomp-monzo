package io.reactivex.d.d;

import io.reactivex.exceptions.CompositeException;
import java.util.concurrent.atomic.AtomicReference;

public final class s extends AtomicReference implements io.reactivex.b.b, io.reactivex.t {
   final io.reactivex.c.g a;
   final io.reactivex.c.g b;
   final io.reactivex.c.a c;
   final io.reactivex.c.g d;

   public s(io.reactivex.c.g var1, io.reactivex.c.g var2, io.reactivex.c.a var3, io.reactivex.c.g var4) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
      this.d = var4;
   }

   public void dispose() {
      io.reactivex.d.a.d.a((AtomicReference)this);
   }

   public boolean isDisposed() {
      boolean var1;
      if(this.get() == io.reactivex.d.a.d.a) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public void onComplete() {
      if(!this.isDisposed()) {
         this.lazySet(io.reactivex.d.a.d.a);

         try {
            this.c.a();
         } catch (Throwable var2) {
            io.reactivex.exceptions.a.b(var2);
            io.reactivex.g.a.a(var2);
         }
      }

   }

   public void onError(Throwable var1) {
      if(!this.isDisposed()) {
         this.lazySet(io.reactivex.d.a.d.a);

         try {
            this.b.a(var1);
         } catch (Throwable var3) {
            io.reactivex.exceptions.a.b(var3);
            io.reactivex.g.a.a((Throwable)(new CompositeException(new Throwable[]{var1, var3})));
         }
      }

   }

   public void onNext(Object var1) {
      if(!this.isDisposed()) {
         try {
            this.a.a(var1);
         } catch (Throwable var2) {
            io.reactivex.exceptions.a.b(var2);
            ((io.reactivex.b.b)this.get()).dispose();
            this.onError(var2);
         }
      }

   }

   public void onSubscribe(io.reactivex.b.b var1) {
      if(io.reactivex.d.a.d.b(this, var1)) {
         try {
            this.d.a(this);
         } catch (Throwable var3) {
            io.reactivex.exceptions.a.b(var3);
            var1.dispose();
            this.onError(var3);
         }
      }

   }
}
