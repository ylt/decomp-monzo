package io.reactivex.d.d;

import io.reactivex.exceptions.OnErrorNotImplementedException;
import java.util.concurrent.atomic.AtomicReference;

public final class m extends AtomicReference implements io.reactivex.b.b, io.reactivex.c {
   public void dispose() {
      io.reactivex.d.a.d.a((AtomicReference)this);
   }

   public boolean isDisposed() {
      boolean var1;
      if(this.get() == io.reactivex.d.a.d.a) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public void onComplete() {
      this.lazySet(io.reactivex.d.a.d.a);
   }

   public void onError(Throwable var1) {
      this.lazySet(io.reactivex.d.a.d.a);
      io.reactivex.g.a.a((Throwable)(new OnErrorNotImplementedException(var1)));
   }

   public void onSubscribe(io.reactivex.b.b var1) {
      io.reactivex.d.a.d.b(this, var1);
   }
}
