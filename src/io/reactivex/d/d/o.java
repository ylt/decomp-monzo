package io.reactivex.d.d;

public final class o implements io.reactivex.t {
   final io.reactivex.d.a.j a;
   io.reactivex.b.b b;

   public o(io.reactivex.d.a.j var1) {
      this.a = var1;
   }

   public void onComplete() {
      this.a.b(this.b);
   }

   public void onError(Throwable var1) {
      this.a.a(var1, this.b);
   }

   public void onNext(Object var1) {
      this.a.a(var1, this.b);
   }

   public void onSubscribe(io.reactivex.b.b var1) {
      if(io.reactivex.d.a.d.a(this.b, var1)) {
         this.b = var1;
         this.a.a(var1);
      }

   }
}
