package io.reactivex.d.j;

import java.util.concurrent.CountDownLatch;

public final class e {
   public static void a() {
      if(io.reactivex.g.a.a() && (Thread.currentThread() instanceof io.reactivex.d.g.g || io.reactivex.g.a.b())) {
         throw new IllegalStateException("Attempt to block on a Scheduler " + Thread.currentThread().getName() + " that doesn't support blocking operators as they may lead to deadlock");
      }
   }

   public static void a(CountDownLatch var0, io.reactivex.b.b var1) {
      if(var0.getCount() != 0L) {
         try {
            a();
            var0.await();
         } catch (InterruptedException var2) {
            var1.dispose();
            Thread.currentThread().interrupt();
            throw new IllegalStateException("Interrupted while waiting for subscription to complete.", var2);
         }
      }

   }
}
