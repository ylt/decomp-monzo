package io.reactivex.d.j;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.RandomAccess;
import java.util.concurrent.atomic.AtomicInteger;

public final class s extends AtomicInteger implements List, RandomAccess {
   final ArrayList a = new ArrayList();

   public void add(int var1, Object var2) {
      this.a.add(var1, var2);
      this.lazySet(this.a.size());
   }

   public boolean add(Object var1) {
      boolean var2 = this.a.add(var1);
      this.lazySet(this.a.size());
      return var2;
   }

   public boolean addAll(int var1, Collection var2) {
      boolean var3 = this.a.addAll(var1, var2);
      this.lazySet(this.a.size());
      return var3;
   }

   public boolean addAll(Collection var1) {
      boolean var2 = this.a.addAll(var1);
      this.lazySet(this.a.size());
      return var2;
   }

   public void clear() {
      this.a.clear();
      this.lazySet(0);
   }

   public boolean contains(Object var1) {
      return this.a.contains(var1);
   }

   public boolean containsAll(Collection var1) {
      return this.a.containsAll(var1);
   }

   public boolean equals(Object var1) {
      boolean var2;
      if(var1 instanceof s) {
         var2 = this.a.equals(((s)var1).a);
      } else {
         var2 = this.a.equals(var1);
      }

      return var2;
   }

   public Object get(int var1) {
      return this.a.get(var1);
   }

   public int hashCode() {
      return this.a.hashCode();
   }

   public int indexOf(Object var1) {
      return this.a.indexOf(var1);
   }

   public boolean isEmpty() {
      boolean var1;
      if(this.get() == 0) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public Iterator iterator() {
      return this.a.iterator();
   }

   public int lastIndexOf(Object var1) {
      return this.a.lastIndexOf(var1);
   }

   public ListIterator listIterator() {
      return this.a.listIterator();
   }

   public ListIterator listIterator(int var1) {
      return this.a.listIterator(var1);
   }

   public Object remove(int var1) {
      Object var2 = this.a.remove(var1);
      this.lazySet(this.a.size());
      return var2;
   }

   public boolean remove(Object var1) {
      boolean var2 = this.a.remove(var1);
      this.lazySet(this.a.size());
      return var2;
   }

   public boolean removeAll(Collection var1) {
      boolean var2 = this.a.removeAll(var1);
      this.lazySet(this.a.size());
      return var2;
   }

   public boolean retainAll(Collection var1) {
      boolean var2 = this.a.retainAll(var1);
      this.lazySet(this.a.size());
      return var2;
   }

   public Object set(int var1, Object var2) {
      return this.a.set(var1, var2);
   }

   public int size() {
      return this.get();
   }

   public List subList(int var1, int var2) {
      return this.a.subList(var1, var2);
   }

   public Object[] toArray() {
      return this.a.toArray();
   }

   public Object[] toArray(Object[] var1) {
      return this.a.toArray(var1);
   }

   public String toString() {
      return this.a.toString();
   }
}
