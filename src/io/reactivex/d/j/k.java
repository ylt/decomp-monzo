package io.reactivex.d.j;

import io.reactivex.t;
import java.util.concurrent.atomic.AtomicInteger;

public final class k {
   public static void a(t var0, Object var1, AtomicInteger var2, c var3) {
      if(var2.get() == 0 && var2.compareAndSet(0, 1)) {
         var0.onNext(var1);
         if(var2.decrementAndGet() != 0) {
            Throwable var4 = var3.a();
            if(var4 != null) {
               var0.onError(var4);
            } else {
               var0.onComplete();
            }
         }
      }

   }

   public static void a(t var0, Throwable var1, AtomicInteger var2, c var3) {
      if(var3.a(var1)) {
         if(var2.getAndIncrement() == 0) {
            var0.onError(var3.a());
         }
      } else {
         io.reactivex.g.a.a(var1);
      }

   }

   public static void a(t var0, AtomicInteger var1, c var2) {
      if(var1.getAndIncrement() == 0) {
         Throwable var3 = var2.a();
         if(var3 != null) {
            var0.onError(var3);
         } else {
            var0.onComplete();
         }
      }

   }

   public static void a(org.a.b var0, Object var1, AtomicInteger var2, c var3) {
      if(var2.get() == 0 && var2.compareAndSet(0, 1)) {
         var0.onNext(var1);
         if(var2.decrementAndGet() != 0) {
            Throwable var4 = var3.a();
            if(var4 != null) {
               var0.onError(var4);
            } else {
               var0.onComplete();
            }
         }
      }

   }

   public static void a(org.a.b var0, Throwable var1, AtomicInteger var2, c var3) {
      if(var3.a(var1)) {
         if(var2.getAndIncrement() == 0) {
            var0.onError(var3.a());
         }
      } else {
         io.reactivex.g.a.a(var1);
      }

   }

   public static void a(org.a.b var0, AtomicInteger var1, c var2) {
      if(var1.getAndIncrement() == 0) {
         Throwable var3 = var2.a();
         if(var3 != null) {
            var0.onError(var3);
         } else {
            var0.onComplete();
         }
      }

   }
}
