package io.reactivex.d.j;

import io.reactivex.exceptions.ProtocolViolationException;
import java.util.concurrent.atomic.AtomicReference;

public final class h {
   public static String a(String var0) {
      return "It is not allowed to subscribe with a(n) " + var0 + " multiple times. Please create a fresh instance of " + var0 + " and subscribe that to the target source instead.";
   }

   public static void a(Class var0) {
      io.reactivex.g.a.a((Throwable)(new ProtocolViolationException(a(var0.getName()))));
   }

   public static boolean a(io.reactivex.b.b var0, io.reactivex.b.b var1, Class var2) {
      io.reactivex.d.b.b.a(var1, (String)"next is null");
      boolean var3;
      if(var0 != null) {
         var1.dispose();
         if(var0 != io.reactivex.d.a.d.a) {
            a(var2);
         }

         var3 = false;
      } else {
         var3 = true;
      }

      return var3;
   }

   public static boolean a(AtomicReference var0, io.reactivex.b.b var1, Class var2) {
      io.reactivex.d.b.b.a(var1, (String)"next is null");
      boolean var3;
      if(!var0.compareAndSet((Object)null, var1)) {
         var1.dispose();
         if(var0.get() != io.reactivex.d.a.d.a) {
            a(var2);
         }

         var3 = false;
      } else {
         var3 = true;
      }

      return var3;
   }
}
