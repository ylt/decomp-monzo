package io.reactivex.d.j;

import java.util.concurrent.CountDownLatch;

public final class f extends CountDownLatch implements io.reactivex.c.a, io.reactivex.c.g {
   public Throwable a;

   public f() {
      super(1);
   }

   public void a() {
      this.countDown();
   }

   public void a(Throwable var1) {
      this.a = var1;
      this.countDown();
   }
}
