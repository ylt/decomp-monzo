package io.reactivex.d.j;

import io.reactivex.exceptions.CompositeException;
import java.util.concurrent.atomic.AtomicReference;

public final class j {
   public static final Throwable a = new j.a();

   public static RuntimeException a(Throwable var0) {
      if(var0 instanceof Error) {
         throw (Error)var0;
      } else {
         RuntimeException var1;
         if(var0 instanceof RuntimeException) {
            var1 = (RuntimeException)var0;
         } else {
            var1 = new RuntimeException(var0);
         }

         return var1;
      }
   }

   public static Throwable a(AtomicReference var0) {
      Throwable var2 = (Throwable)var0.get();
      Throwable var1 = var2;
      if(var2 != a) {
         var1 = (Throwable)var0.getAndSet(a);
      }

      return var1;
   }

   public static boolean a(AtomicReference var0, Throwable var1) {
      while(true) {
         Throwable var4 = (Throwable)var0.get();
         boolean var2;
         if(var4 == a) {
            var2 = false;
         } else {
            Object var3;
            if(var4 == null) {
               var3 = var1;
            } else {
               var3 = new CompositeException(new Throwable[]{var4, var1});
            }

            if(!var0.compareAndSet(var4, var3)) {
               continue;
            }

            var2 = true;
         }

         return var2;
      }
   }

   static final class a extends Throwable {
      a() {
         super("No further exceptions");
      }

      public Throwable fillInStackTrace() {
         return this;
      }
   }
}
