package io.reactivex.d.j;

import io.reactivex.t;

public final class r {
   public static io.reactivex.d.c.i a(int var0) {
      Object var1;
      if(var0 < 0) {
         var1 = new io.reactivex.d.f.c(-var0);
      } else {
         var1 = new io.reactivex.d.f.b(var0);
      }

      return (io.reactivex.d.c.i)var1;
   }

   public static void a(io.reactivex.d.c.h var0, t var1, boolean var2, io.reactivex.b.b var3, o var4) {
      int var5 = 1;

      while(!a(var4.b(), var0.b(), var1, var2, var0, var3, var4)) {
         while(true) {
            boolean var7 = var4.b();
            Object var8 = var0.n_();
            boolean var6;
            if(var8 == null) {
               var6 = true;
            } else {
               var6 = false;
            }

            if(a(var7, var6, var1, var2, var0, var3, var4)) {
               return;
            }

            if(var6) {
               var5 = var4.a(-var5);
               if(var5 == 0) {
                  return;
               }
            } else {
               var4.a(var1, var8);
            }
         }
      }

   }

   public static boolean a(boolean var0, boolean var1, t var2, boolean var3, io.reactivex.d.c.i var4, io.reactivex.b.b var5, o var6) {
      boolean var7 = true;
      if(var6.a()) {
         var4.c();
         var5.dispose();
         var0 = var7;
      } else {
         if(var0) {
            if(var3) {
               if(var1) {
                  var5.dispose();
                  Throwable var8 = var6.e();
                  if(var8 != null) {
                     var2.onError(var8);
                     var0 = var7;
                  } else {
                     var2.onComplete();
                     var0 = var7;
                  }

                  return var0;
               }
            } else {
               Throwable var9 = var6.e();
               if(var9 != null) {
                  var4.c();
                  var5.dispose();
                  var2.onError(var9);
                  var0 = var7;
                  return var0;
               }

               if(var1) {
                  var5.dispose();
                  var2.onComplete();
                  var0 = var7;
                  return var0;
               }
            }
         }

         var0 = false;
      }

      return var0;
   }
}
