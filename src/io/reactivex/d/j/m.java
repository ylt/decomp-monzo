package io.reactivex.d.j;

import java.util.ArrayList;

public class m {
   final int h;
   Object[] i;
   Object[] j;
   volatile int k;
   int l;

   public m(int var1) {
      this.h = var1;
   }

   public void a(Object var1) {
      if(this.k == 0) {
         this.i = new Object[this.h + 1];
         this.j = this.i;
         this.i[0] = var1;
         this.l = 1;
         this.k = 1;
      } else if(this.l == this.h) {
         Object[] var2 = new Object[this.h + 1];
         var2[0] = var1;
         this.j[this.h] = var2;
         this.j = var2;
         this.l = 1;
         ++this.k;
      } else {
         this.j[this.l] = var1;
         ++this.l;
         ++this.k;
      }

   }

   public Object[] b() {
      return this.i;
   }

   public int c() {
      return this.k;
   }

   public String toString() {
      int var6 = this.h;
      int var5 = this.k;
      ArrayList var8 = new ArrayList(var5 + 1);
      Object[] var7 = this.b();
      int var2 = 0;
      int var1 = 0;

      while(var2 < var5) {
         var8.add(var7[var1]);
         int var3 = var2 + 1;
         int var4 = var1 + 1;
         var1 = var4;
         var2 = var3;
         if(var4 == var6) {
            var7 = (Object[])((Object[])var7[var6]);
            var1 = 0;
            var2 = var3;
         }
      }

      return var8.toString();
   }
}
