package io.reactivex.d.j;

import java.util.concurrent.atomic.AtomicReference;

public final class c extends AtomicReference {
   public Throwable a() {
      return j.a((AtomicReference)this);
   }

   public boolean a(Throwable var1) {
      return j.a(this, var1);
   }
}
