package io.reactivex.d.j;

public final class p {
   final float a;
   int b;
   int c;
   int d;
   Object[] e;

   public p() {
      this(16, 0.75F);
   }

   public p(int var1, float var2) {
      this.a = var2;
      var1 = q.a(var1);
      this.b = var1 - 1;
      this.d = (int)((float)var1 * var2);
      this.e = (Object[])(new Object[var1]);
   }

   static int a(int var0) {
      var0 = -1640531527 * var0;
      return var0 ^ var0 >>> 16;
   }

   void a() {
      Object[] var8 = this.e;
      int var1 = var8.length;
      int var5 = var1 << 1;
      int var6 = var5 - 1;
      Object[] var7 = (Object[])(new Object[var5]);

      for(int var2 = this.c; var2 != 0; --var2) {
         int var3 = var1;

         do {
            var1 = var3 - 1;
            var3 = var1;
         } while(var8[var1] == null);

         int var4 = a(var8[var1].hashCode()) & var6;
         var3 = var4;
         if(var7[var4] != null) {
            do {
               var3 = var4 + 1 & var6;
               var4 = var3;
            } while(var7[var3] != null);
         }

         var7[var3] = var8[var1];
      }

      this.b = var6;
      this.d = (int)((float)var5 * this.a);
      this.e = var7;
   }

   boolean a(int var1, Object[] var2, int var3) {
      --this.c;
      int var4 = var1;

      while(true) {
         var1 = var4 + 1 & var3;

         Object var6;
         while(true) {
            var6 = var2[var1];
            if(var6 == null) {
               var2[var4] = null;
               return true;
            }

            int var5 = a(var6.hashCode()) & var3;
            if(var4 <= var1) {
               if(var4 >= var5 || var5 > var1) {
                  break;
               }
            } else if(var4 >= var5 && var5 > var1) {
               break;
            }

            var1 = var1 + 1 & var3;
         }

         var2[var4] = var6;
         var4 = var1;
      }
   }

   public boolean a(Object var1) {
      Object[] var6 = this.e;
      int var4 = this.b;
      int var2 = a(var1.hashCode()) & var4;
      Object var7 = var6[var2];
      int var3 = var2;
      boolean var5;
      if(var7 != null) {
         if(var7.equals(var1)) {
            var5 = false;
            return var5;
         }

         while(true) {
            var3 = var2 + 1 & var4;
            var7 = var6[var3];
            if(var7 == null) {
               break;
            }

            var2 = var3;
            if(var7.equals(var1)) {
               var5 = false;
               return var5;
            }
         }
      }

      var6[var3] = var1;
      var2 = this.c + 1;
      this.c = var2;
      if(var2 >= this.d) {
         this.a();
      }

      var5 = true;
      return var5;
   }

   public boolean b(Object var1) {
      Object[] var6 = this.e;
      int var4 = this.b;
      int var3 = a(var1.hashCode()) & var4;
      Object var7 = var6[var3];
      boolean var5;
      if(var7 == null) {
         var5 = false;
      } else {
         int var2 = var3;
         if(var7.equals(var1)) {
            var5 = this.a(var3, var6, var4);
         } else {
            while(true) {
               var3 = var2 + 1 & var4;
               var7 = var6[var3];
               if(var7 == null) {
                  var5 = false;
                  break;
               }

               var2 = var3;
               if(var7.equals(var1)) {
                  var5 = this.a(var3, var6, var4);
                  break;
               }
            }
         }
      }

      return var5;
   }

   public Object[] b() {
      return this.e;
   }
}
