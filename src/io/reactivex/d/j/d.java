package io.reactivex.d.j;

import java.util.concurrent.atomic.AtomicLong;

public final class d {
   public static long a(long var0, long var2) {
      var2 += var0;
      var0 = var2;
      if(var2 < 0L) {
         var0 = Long.MAX_VALUE;
      }

      return var0;
   }

   public static long a(AtomicLong var0, long var1) {
      long var3 = Long.MAX_VALUE;

      while(true) {
         long var5 = var0.get();
         if(var5 == Long.MAX_VALUE) {
            var1 = var3;
            break;
         }

         if(var0.compareAndSet(var5, a(var5, var1))) {
            var1 = var5;
            break;
         }
      }

      return var1;
   }

   public static long b(AtomicLong var0, long var1) {
      while(true) {
         long var7 = var0.get();
         long var3;
         if(var7 == Long.MAX_VALUE) {
            var3 = Long.MAX_VALUE;
         } else {
            long var5 = var7 - var1;
            var3 = var5;
            if(var5 < 0L) {
               io.reactivex.g.a.a((Throwable)(new IllegalStateException("More produced than requested: " + var5)));
               var3 = 0L;
            }

            if(!var0.compareAndSet(var7, var3)) {
               continue;
            }
         }

         return var3;
      }
   }
}
