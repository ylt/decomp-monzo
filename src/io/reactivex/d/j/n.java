package io.reactivex.d.j;

import io.reactivex.t;
import java.io.Serializable;

public enum n {
   a;

   public static Object a() {
      return a;
   }

   public static Object a(io.reactivex.b.b var0) {
      return new n.a(var0);
   }

   public static Object a(Object var0) {
      return var0;
   }

   public static Object a(Throwable var0) {
      return new n.b(var0);
   }

   public static boolean a(Object var0, t var1) {
      boolean var2 = true;
      if(var0 == a) {
         var1.onComplete();
      } else if(var0 instanceof n.b) {
         var1.onError(((n.b)var0).a);
      } else {
         var1.onNext(var0);
         var2 = false;
      }

      return var2;
   }

   public static boolean b(Object var0) {
      boolean var1;
      if(var0 == a) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public static boolean b(Object var0, t var1) {
      boolean var2 = true;
      if(var0 == a) {
         var1.onComplete();
      } else if(var0 instanceof n.b) {
         var1.onError(((n.b)var0).a);
      } else if(var0 instanceof n.a) {
         var1.onSubscribe(((n.a)var0).a);
         var2 = false;
      } else {
         var1.onNext(var0);
         var2 = false;
      }

      return var2;
   }

   public static boolean c(Object var0) {
      return var0 instanceof n.b;
   }

   public static boolean d(Object var0) {
      return var0 instanceof n.a;
   }

   public static Object e(Object var0) {
      return var0;
   }

   public static Throwable f(Object var0) {
      return ((n.b)var0).a;
   }

   public static io.reactivex.b.b g(Object var0) {
      return ((n.a)var0).a;
   }

   public String toString() {
      return "NotificationLite.Complete";
   }

   static final class a implements Serializable {
      final io.reactivex.b.b a;

      a(io.reactivex.b.b var1) {
         this.a = var1;
      }

      public String toString() {
         return "NotificationLite.Disposable[" + this.a + "]";
      }
   }

   static final class b implements Serializable {
      final Throwable a;

      b(Throwable var1) {
         this.a = var1;
      }

      public boolean equals(Object var1) {
         boolean var2;
         if(var1 instanceof n.b) {
            n.b var3 = (n.b)var1;
            var2 = io.reactivex.d.b.b.a(this.a, (Object)var3.a);
         } else {
            var2 = false;
         }

         return var2;
      }

      public int hashCode() {
         return this.a.hashCode();
      }

      public String toString() {
         return "NotificationLite.Error[" + this.a + "]";
      }
   }
}
