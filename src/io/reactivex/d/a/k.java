package io.reactivex.d.a;

import java.util.concurrent.atomic.AtomicReference;

public final class k extends AtomicReference implements io.reactivex.b.b {
   public k() {
   }

   public k(io.reactivex.b.b var1) {
      this.lazySet(var1);
   }

   public boolean a(io.reactivex.b.b var1) {
      return d.a((AtomicReference)this, var1);
   }

   public boolean b(io.reactivex.b.b var1) {
      return d.c(this, var1);
   }

   public void dispose() {
      d.a((AtomicReference)this);
   }

   public boolean isDisposed() {
      return d.a((io.reactivex.b.b)this.get());
   }
}
