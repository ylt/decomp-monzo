package io.reactivex.d.a;

import io.reactivex.t;
import io.reactivex.x;

public enum e implements io.reactivex.d.c.d {
   a,
   b;

   public static void a(io.reactivex.c var0) {
      var0.onSubscribe(a);
      var0.onComplete();
   }

   public static void a(io.reactivex.j var0) {
      var0.onSubscribe(a);
      var0.onComplete();
   }

   public static void a(t var0) {
      var0.onSubscribe(a);
      var0.onComplete();
   }

   public static void a(Throwable var0, io.reactivex.c var1) {
      var1.onSubscribe(a);
      var1.onError(var0);
   }

   public static void a(Throwable var0, io.reactivex.j var1) {
      var1.onSubscribe(a);
      var1.onError(var0);
   }

   public static void a(Throwable var0, t var1) {
      var1.onSubscribe(a);
      var1.onError(var0);
   }

   public static void a(Throwable var0, x var1) {
      var1.onSubscribe(a);
      var1.onError(var0);
   }

   public int a(int var1) {
      return var1 & 2;
   }

   public boolean a(Object var1) {
      throw new UnsupportedOperationException("Should not be called!");
   }

   public boolean b() {
      return true;
   }

   public void c() {
   }

   public void dispose() {
   }

   public boolean isDisposed() {
      boolean var1;
      if(this == a) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public Object n_() throws Exception {
      return null;
   }
}
