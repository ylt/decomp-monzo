package io.reactivex.d.a;

import java.util.concurrent.atomic.AtomicReference;

public final class b extends AtomicReference implements io.reactivex.b.b {
   public b(io.reactivex.c.f var1) {
      super(var1);
   }

   public void dispose() {
      if(this.get() != null) {
         io.reactivex.c.f var1 = (io.reactivex.c.f)this.getAndSet((Object)null);
         if(var1 != null) {
            try {
               var1.a();
            } catch (Exception var2) {
               io.reactivex.exceptions.a.b(var2);
               io.reactivex.g.a.a((Throwable)var2);
            }
         }
      }

   }

   public boolean isDisposed() {
      boolean var1;
      if(this.get() == null) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }
}
