package io.reactivex.d.a;

import io.reactivex.t;
import io.reactivex.d.j.n;

public final class j extends g implements io.reactivex.b.b {
   final t b;
   final io.reactivex.d.f.c c;
   volatile io.reactivex.b.b d;
   io.reactivex.b.b e;
   volatile boolean f;

   public j(t var1, io.reactivex.b.b var2, int var3) {
      this.b = var1;
      this.e = var2;
      this.c = new io.reactivex.d.f.c(var3);
      this.d = e.a;
   }

   void a() {
      io.reactivex.b.b var1 = this.e;
      this.e = null;
      if(var1 != null) {
         var1.dispose();
      }

   }

   public void a(Throwable var1, io.reactivex.b.b var2) {
      if(this.f) {
         io.reactivex.g.a.a(var1);
      } else {
         this.c.a((Object)var2, (Object)n.a(var1));
         this.b();
      }

   }

   public boolean a(io.reactivex.b.b var1) {
      boolean var2;
      if(this.f) {
         var2 = false;
      } else {
         this.c.a((Object)this.d, (Object)n.a(var1));
         this.b();
         var2 = true;
      }

      return var2;
   }

   public boolean a(Object var1, io.reactivex.b.b var2) {
      boolean var3;
      if(this.f) {
         var3 = false;
      } else {
         this.c.a((Object)var2, (Object)n.a(var1));
         this.b();
         var3 = true;
      }

      return var3;
   }

   void b() {
      if(this.a.getAndIncrement() == 0) {
         io.reactivex.d.f.c var4 = this.c;
         t var3 = this.b;
         int var1 = 1;

         int var2;
         do {
            while(true) {
               Object var5 = var4.n_();
               if(var5 == null) {
                  var2 = this.a.addAndGet(-var1);
                  var1 = var2;
                  break;
               }

               Object var6 = var4.n_();
               if(var5 == this.d) {
                  if(n.d(var6)) {
                     io.reactivex.b.b var7 = n.g(var6);
                     this.d.dispose();
                     if(!this.f) {
                        this.d = var7;
                     } else {
                        var7.dispose();
                     }
                  } else if(n.c(var6)) {
                     var4.c();
                     this.a();
                     Throwable var8 = n.f(var6);
                     if(!this.f) {
                        this.f = true;
                        var3.onError(var8);
                     } else {
                        io.reactivex.g.a.a(var8);
                     }
                  } else if(n.b(var6)) {
                     var4.c();
                     this.a();
                     if(!this.f) {
                        this.f = true;
                        var3.onComplete();
                     }
                  } else {
                     var3.onNext(n.e(var6));
                  }
               }
            }
         } while(var2 != 0);
      }

   }

   public void b(io.reactivex.b.b var1) {
      this.c.a((Object)var1, (Object)n.a());
      this.b();
   }

   public void dispose() {
      if(!this.f) {
         this.f = true;
         this.a();
      }

   }

   public boolean isDisposed() {
      io.reactivex.b.b var2 = this.e;
      boolean var1;
      if(var2 != null) {
         var1 = var2.isDisposed();
      } else {
         var1 = this.f;
      }

      return var1;
   }
}
