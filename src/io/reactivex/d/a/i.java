package io.reactivex.d.a;

import io.reactivex.exceptions.CompositeException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class i implements io.reactivex.b.b, c {
   List a;
   volatile boolean b;

   void a(List var1) {
      if(var1 != null) {
         io.reactivex.b.b var2 = null;
         Iterator var3 = var1.iterator();
         ArrayList var5 = var2;

         while(var3.hasNext()) {
            var2 = (io.reactivex.b.b)var3.next();

            try {
               var2.dispose();
            } catch (Throwable var4) {
               io.reactivex.exceptions.a.b(var4);
               if(var5 == null) {
                  var5 = new ArrayList();
               }

               var5.add(var4);
            }
         }

         if(var5 != null) {
            if(var5.size() == 1) {
               throw io.reactivex.d.j.j.a((Throwable)var5.get(0));
            }

            throw new CompositeException(var5);
         }
      }

   }

   public boolean a(io.reactivex.b.b param1) {
      // $FF: Couldn't be decompiled
   }

   public boolean b(io.reactivex.b.b var1) {
      boolean var2;
      if(this.c(var1)) {
         var1.dispose();
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   public boolean c(io.reactivex.b.b param1) {
      // $FF: Couldn't be decompiled
   }

   public void dispose() {
      // $FF: Couldn't be decompiled
   }

   public boolean isDisposed() {
      return this.b;
   }
}
