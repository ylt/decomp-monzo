package io.reactivex.d.a;

import java.util.concurrent.atomic.AtomicReferenceArray;

public final class a extends AtomicReferenceArray implements io.reactivex.b.b {
   public a(int var1) {
      super(var1);
   }

   public boolean a(int var1, io.reactivex.b.b var2) {
      while(true) {
         io.reactivex.b.b var4 = (io.reactivex.b.b)this.get(var1);
         boolean var3;
         if(var4 == d.a) {
            var2.dispose();
            var3 = false;
         } else {
            if(!this.compareAndSet(var1, var4, var2)) {
               continue;
            }

            if(var4 != null) {
               var4.dispose();
            }

            var3 = true;
         }

         return var3;
      }
   }

   public void dispose() {
      if(this.get(0) != d.a) {
         int var2 = this.length();

         for(int var1 = 0; var1 < var2; ++var1) {
            if((io.reactivex.b.b)this.get(var1) != d.a) {
               io.reactivex.b.b var3 = (io.reactivex.b.b)this.getAndSet(var1, d.a);
               if(var3 != d.a && var3 != null) {
                  var3.dispose();
               }
            }
         }
      }

   }

   public boolean isDisposed() {
      boolean var1 = false;
      if(this.get(0) == d.a) {
         var1 = true;
      }

      return var1;
   }
}
