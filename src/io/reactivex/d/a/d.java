package io.reactivex.d.a;

import io.reactivex.exceptions.ProtocolViolationException;
import java.util.concurrent.atomic.AtomicReference;

public enum d implements io.reactivex.b.b {
   a;

   public static void a() {
      io.reactivex.g.a.a((Throwable)(new ProtocolViolationException("Disposable already set!")));
   }

   public static boolean a(io.reactivex.b.b var0) {
      boolean var1;
      if(var0 == a) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public static boolean a(io.reactivex.b.b var0, io.reactivex.b.b var1) {
      boolean var2 = false;
      if(var1 == null) {
         io.reactivex.g.a.a((Throwable)(new NullPointerException("next is null")));
      } else if(var0 != null) {
         var1.dispose();
         a();
      } else {
         var2 = true;
      }

      return var2;
   }

   public static boolean a(AtomicReference var0) {
      io.reactivex.b.b var3 = (io.reactivex.b.b)var0.get();
      d var2 = a;
      boolean var1;
      if(var3 != var2) {
         io.reactivex.b.b var4 = (io.reactivex.b.b)var0.getAndSet(var2);
         if(var4 != var2) {
            if(var4 != null) {
               var4.dispose();
            }

            var1 = true;
            return var1;
         }
      }

      var1 = false;
      return var1;
   }

   public static boolean a(AtomicReference var0, io.reactivex.b.b var1) {
      while(true) {
         io.reactivex.b.b var3 = (io.reactivex.b.b)var0.get();
         boolean var2;
         if(var3 == a) {
            if(var1 != null) {
               var1.dispose();
            }

            var2 = false;
         } else {
            if(!var0.compareAndSet(var3, var1)) {
               continue;
            }

            if(var3 != null) {
               var3.dispose();
            }

            var2 = true;
         }

         return var2;
      }
   }

   public static boolean b(AtomicReference var0, io.reactivex.b.b var1) {
      io.reactivex.d.b.b.a(var1, (String)"d is null");
      boolean var2;
      if(!var0.compareAndSet((Object)null, var1)) {
         var1.dispose();
         if(var0.get() != a) {
            a();
         }

         var2 = false;
      } else {
         var2 = true;
      }

      return var2;
   }

   public static boolean c(AtomicReference var0, io.reactivex.b.b var1) {
      while(true) {
         io.reactivex.b.b var3 = (io.reactivex.b.b)var0.get();
         boolean var2;
         if(var3 == a) {
            if(var1 != null) {
               var1.dispose();
            }

            var2 = false;
         } else {
            if(!var0.compareAndSet(var3, var1)) {
               continue;
            }

            var2 = true;
         }

         return var2;
      }
   }

   public static boolean d(AtomicReference var0, io.reactivex.b.b var1) {
      boolean var2;
      if(!var0.compareAndSet((Object)null, var1)) {
         if(var0.get() == a) {
            var1.dispose();
         }

         var2 = false;
      } else {
         var2 = true;
      }

      return var2;
   }

   public void dispose() {
   }

   public boolean isDisposed() {
      return true;
   }
}
