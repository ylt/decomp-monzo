package io.reactivex.d.g;

import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.atomic.AtomicReference;

abstract class a extends AtomicReference implements io.reactivex.b.b {
   protected static final FutureTask c;
   protected static final FutureTask d;
   protected final Runnable a;
   protected Thread b;

   static {
      c = new FutureTask(io.reactivex.d.b.a.b, (Object)null);
      d = new FutureTask(io.reactivex.d.b.a.b, (Object)null);
   }

   a(Runnable var1) {
      this.a = var1;
   }

   public final void a(Future var1) {
      while(true) {
         Future var3 = (Future)this.get();
         if(var3 != c) {
            if(var3 == d) {
               boolean var2;
               if(this.b != Thread.currentThread()) {
                  var2 = true;
               } else {
                  var2 = false;
               }

               var1.cancel(var2);
            } else if(!this.compareAndSet(var3, var1)) {
               continue;
            }
         }

         return;
      }
   }

   public final void dispose() {
      Future var2 = (Future)this.get();
      if(var2 != c && var2 != d && this.compareAndSet(var2, d) && var2 != null) {
         boolean var1;
         if(this.b != Thread.currentThread()) {
            var1 = true;
         } else {
            var1 = false;
         }

         var2.cancel(var1);
      }

   }

   public final boolean isDisposed() {
      Future var2 = (Future)this.get();
      boolean var1;
      if(var2 != c && var2 != d) {
         var1 = false;
      } else {
         var1 = true;
      }

      return var1;
   }
}
