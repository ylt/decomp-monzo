package io.reactivex.d.g;

import java.util.concurrent.Callable;

public final class j extends a implements Callable {
   public j(Runnable var1) {
      super(var1);
   }

   public Void a() throws Exception {
      this.b = Thread.currentThread();

      try {
         this.a.run();
      } finally {
         this.lazySet(c);
         this.b = null;
      }

      return null;
   }

   // $FF: synthetic method
   public Object call() throws Exception {
      return this.a();
   }
}
