package io.reactivex.d.g;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.atomic.AtomicReference;

final class c implements io.reactivex.b.b, Callable {
   static final FutureTask f;
   final Runnable a;
   final AtomicReference b;
   final AtomicReference c;
   final ExecutorService d;
   Thread e;

   static {
      f = new FutureTask(io.reactivex.d.b.a.b, (Object)null);
   }

   c(Runnable var1, ExecutorService var2) {
      this.a = var1;
      this.c = new AtomicReference();
      this.b = new AtomicReference();
      this.d = var2;
   }

   public Void a() throws Exception {
      try {
         this.e = Thread.currentThread();

         try {
            this.a.run();
            this.b(this.d.submit(this));
         } catch (Throwable var4) {
            io.reactivex.g.a.a(var4);
         }
      } finally {
         this.e = null;
      }

      return null;
   }

   void a(Future var1) {
      Future var3;
      do {
         var3 = (Future)this.c.get();
         if(var3 == f) {
            boolean var2;
            if(this.e != Thread.currentThread()) {
               var2 = true;
            } else {
               var2 = false;
            }

            var1.cancel(var2);
         }
      } while(!this.c.compareAndSet(var3, var1));

   }

   void b(Future var1) {
      Future var3;
      do {
         var3 = (Future)this.b.get();
         if(var3 == f) {
            boolean var2;
            if(this.e != Thread.currentThread()) {
               var2 = true;
            } else {
               var2 = false;
            }

            var1.cancel(var2);
         }
      } while(!this.b.compareAndSet(var3, var1));

   }

   // $FF: synthetic method
   public Object call() throws Exception {
      return this.a();
   }

   public void dispose() {
      boolean var2 = true;
      Future var3 = (Future)this.c.getAndSet(f);
      boolean var1;
      if(var3 != null && var3 != f) {
         if(this.e != Thread.currentThread()) {
            var1 = true;
         } else {
            var1 = false;
         }

         var3.cancel(var1);
      }

      var3 = (Future)this.b.getAndSet(f);
      if(var3 != null && var3 != f) {
         if(this.e != Thread.currentThread()) {
            var1 = var2;
         } else {
            var1 = false;
         }

         var3.cancel(var1);
      }

   }

   public boolean isDisposed() {
      boolean var1;
      if(this.c.get() == f) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }
}
