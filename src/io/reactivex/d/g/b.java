package io.reactivex.d.g;

import io.reactivex.u;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

public final class b extends u {
   static final b.b b;
   static final h c;
   static final int d = a(Runtime.getRuntime().availableProcessors(), Integer.getInteger("rx2.computation-threads", 0).intValue());
   static final b.c e = new b.c(new h("RxComputationShutdown"));
   final ThreadFactory f;
   final AtomicReference g;

   static {
      e.dispose();
      c = new h("RxComputationThreadPool", Math.max(1, Math.min(10, Integer.getInteger("rx2.computation-priority", 5).intValue())), true);
      b = new b.b(0, c);
      b.b();
   }

   public b() {
      this(c);
   }

   public b(ThreadFactory var1) {
      this.f = var1;
      this.g = new AtomicReference(b);
      this.b();
   }

   static int a(int var0, int var1) {
      int var2;
      if(var1 > 0) {
         var2 = var1;
         if(var1 <= var0) {
            return var2;
         }
      }

      var2 = var0;
      return var2;
   }

   public io.reactivex.b.b a(Runnable var1, long var2, long var4, TimeUnit var6) {
      return ((b.b)this.g.get()).a().b(var1, var2, var4, var6);
   }

   public io.reactivex.b.b a(Runnable var1, long var2, TimeUnit var4) {
      return ((b.b)this.g.get()).a().b(var1, var2, var4);
   }

   public u.c a() {
      return new b.a(((b.b)this.g.get()).a());
   }

   public void b() {
      b.b var1 = new b.b(d, this.f);
      if(!this.g.compareAndSet(b, var1)) {
         var1.b();
      }

   }

   static final class a extends u.c {
      volatile boolean a;
      private final io.reactivex.d.a.i b;
      private final io.reactivex.b.a c;
      private final io.reactivex.d.a.i d;
      private final b.c e;

      a(b.c var1) {
         this.e = var1;
         this.b = new io.reactivex.d.a.i();
         this.c = new io.reactivex.b.a();
         this.d = new io.reactivex.d.a.i();
         this.d.a((io.reactivex.b.b)this.b);
         this.d.a((io.reactivex.b.b)this.c);
      }

      public io.reactivex.b.b a(Runnable var1) {
         Object var2;
         if(this.a) {
            var2 = io.reactivex.d.a.e.a;
         } else {
            var2 = this.e.a(var1, 0L, TimeUnit.MILLISECONDS, this.b);
         }

         return (io.reactivex.b.b)var2;
      }

      public io.reactivex.b.b a(Runnable var1, long var2, TimeUnit var4) {
         Object var5;
         if(this.a) {
            var5 = io.reactivex.d.a.e.a;
         } else {
            var5 = this.e.a(var1, var2, var4, this.c);
         }

         return (io.reactivex.b.b)var5;
      }

      public void dispose() {
         if(!this.a) {
            this.a = true;
            this.d.dispose();
         }

      }

      public boolean isDisposed() {
         return this.a;
      }
   }

   static final class b {
      final int a;
      final b.c[] b;
      long c;

      b(int var1, ThreadFactory var2) {
         this.a = var1;
         this.b = new b.c[var1];

         for(int var3 = 0; var3 < var1; ++var3) {
            this.b[var3] = new b.c(var2);
         }

      }

      public b.c a() {
         int var1 = this.a;
         b.c var4;
         if(var1 == 0) {
            var4 = b.e;
         } else {
            b.c[] var5 = this.b;
            long var2 = this.c;
            this.c = 1L + var2;
            var4 = var5[(int)(var2 % (long)var1)];
         }

         return var4;
      }

      public void b() {
         b.c[] var3 = this.b;
         int var2 = var3.length;

         for(int var1 = 0; var1 < var2; ++var1) {
            var3[var1].dispose();
         }

      }
   }

   static final class c extends f {
      c(ThreadFactory var1) {
         super(var1);
      }
   }
}
