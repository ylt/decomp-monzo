package io.reactivex.d.g;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicLong;

public final class h extends AtomicLong implements ThreadFactory {
   final String a;
   final int b;
   final boolean c;

   public h(String var1) {
      this(var1, 5, false);
   }

   public h(String var1, int var2) {
      this(var1, var2, false);
   }

   public h(String var1, int var2, boolean var3) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
   }

   public Thread newThread(Runnable var1) {
      String var2 = this.a + '-' + this.incrementAndGet();
      Object var3;
      if(this.c) {
         var3 = new h.a(var1, var2);
      } else {
         var3 = new Thread(var1, var2);
      }

      ((Thread)var3).setPriority(this.b);
      ((Thread)var3).setDaemon(true);
      return (Thread)var3;
   }

   public String toString() {
      return "RxThreadFactory[" + this.a + "]";
   }

   static final class a extends Thread implements g {
      a(Runnable var1, String var2) {
         super(var1, var2);
      }
   }
}
