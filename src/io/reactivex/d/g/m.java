package io.reactivex.d.g;

import io.reactivex.u;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

public final class m extends u {
   static final h d;
   static final ScheduledExecutorService e = Executors.newScheduledThreadPool(0);
   final ThreadFactory b;
   final AtomicReference c;

   static {
      e.shutdown();
      d = new h("RxSingleScheduler", Math.max(1, Math.min(10, Integer.getInteger("rx2.single-priority", 5).intValue())), true);
   }

   public m() {
      this(d);
   }

   public m(ThreadFactory var1) {
      this.c = new AtomicReference();
      this.b = var1;
      this.c.lazySet(a(var1));
   }

   static ScheduledExecutorService a(ThreadFactory var0) {
      return l.a(var0);
   }

   public io.reactivex.b.b a(Runnable param1, long param2, long param4, TimeUnit param6) {
      // $FF: Couldn't be decompiled
   }

   public io.reactivex.b.b a(Runnable param1, long param2, TimeUnit param4) {
      // $FF: Couldn't be decompiled
   }

   public u.c a() {
      return new m.a((ScheduledExecutorService)this.c.get());
   }

   public void b() {
      ScheduledExecutorService var1 = null;

      ScheduledExecutorService var2;
      ScheduledExecutorService var3;
      do {
         var3 = (ScheduledExecutorService)this.c.get();
         if(var3 != e) {
            if(var1 != null) {
               var1.shutdown();
            }
            break;
         }

         var2 = var1;
         if(var1 == null) {
            var2 = a(this.b);
         }

         var1 = var2;
      } while(!this.c.compareAndSet(var3, var2));

   }

   static final class a extends u.c {
      final ScheduledExecutorService a;
      final io.reactivex.b.a b;
      volatile boolean c;

      a(ScheduledExecutorService var1) {
         this.a = var1;
         this.b = new io.reactivex.b.a();
      }

      public io.reactivex.b.b a(Runnable param1, long param2, TimeUnit param4) {
         // $FF: Couldn't be decompiled
      }

      public void dispose() {
         if(!this.c) {
            this.c = true;
            this.b.dispose();
         }

      }

      public boolean isDisposed() {
         return this.c;
      }
   }
}
