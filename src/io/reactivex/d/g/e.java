package io.reactivex.d.g;

import io.reactivex.u;
import java.util.concurrent.ThreadFactory;

public final class e extends u {
   private static final h c = new h("RxNewThreadScheduler", Math.max(1, Math.min(10, Integer.getInteger("rx2.newthread-priority", 5).intValue())));
   final ThreadFactory b;

   public e() {
      this(c);
   }

   public e(ThreadFactory var1) {
      this.b = var1;
   }

   public u.c a() {
      return new f(this.b);
   }
}
