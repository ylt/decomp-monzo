package io.reactivex.d.g;

import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

public final class l {
   public static final boolean a;
   public static final int b;
   static final AtomicReference c;
   static final Map d;

   static {
      byte var1 = 1;
      c = new AtomicReference();
      d = new ConcurrentHashMap();
      Properties var3 = System.getProperties();
      boolean var2;
      if(var3.containsKey("rx2.purge-enabled")) {
         var2 = Boolean.getBoolean("rx2.purge-enabled");
      } else {
         var2 = true;
      }

      int var0 = var1;
      if(var2) {
         var0 = var1;
         if(var3.containsKey("rx2.purge-period-seconds")) {
            var0 = Integer.getInteger("rx2.purge-period-seconds", 1).intValue();
         }
      }

      a = var2;
      b = var0;
      a();
   }

   public static ScheduledExecutorService a(ThreadFactory var0) {
      ScheduledExecutorService var2 = Executors.newScheduledThreadPool(1, var0);
      if(a && var2 instanceof ScheduledThreadPoolExecutor) {
         ScheduledThreadPoolExecutor var1 = (ScheduledThreadPoolExecutor)var2;
         d.put(var1, var2);
      }

      return var2;
   }

   public static void a() {
      if(a) {
         while(true) {
            ScheduledExecutorService var0 = (ScheduledExecutorService)c.get();
            if(var0 != null && !var0.isShutdown()) {
               break;
            }

            ScheduledExecutorService var1 = Executors.newScheduledThreadPool(1, new h("RxSchedulerPurge"));
            if(c.compareAndSet(var0, var1)) {
               var1.scheduleAtFixedRate(new l.a(), (long)b, (long)b, TimeUnit.SECONDS);
               break;
            }

            var1.shutdownNow();
         }
      }

   }

   static final class a implements Runnable {
      public void run() {
         // $FF: Couldn't be decompiled
      }
   }
}
