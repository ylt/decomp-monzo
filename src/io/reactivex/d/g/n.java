package io.reactivex.d.g;

import io.reactivex.u;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public final class n extends u {
   private static final n b = new n();

   public static n c() {
      return b;
   }

   public io.reactivex.b.b a(Runnable var1) {
      var1.run();
      return io.reactivex.d.a.e.a;
   }

   public io.reactivex.b.b a(Runnable var1, long var2, TimeUnit var4) {
      try {
         var4.sleep(var2);
         var1.run();
      } catch (InterruptedException var5) {
         Thread.currentThread().interrupt();
         io.reactivex.g.a.a((Throwable)var5);
      }

      return io.reactivex.d.a.e.a;
   }

   public u.c a() {
      return new n.c();
   }

   static final class a implements Runnable {
      private final Runnable a;
      private final n.c b;
      private final long c;

      a(Runnable var1, n.c var2, long var3) {
         this.a = var1;
         this.b = var2;
         this.c = var3;
      }

      public void run() {
         if(!this.b.c) {
            long var1 = this.b.a(TimeUnit.MILLISECONDS);
            if(this.c > var1) {
               var1 = this.c - var1;
               if(var1 > 0L) {
                  try {
                     Thread.sleep(var1);
                  } catch (InterruptedException var4) {
                     Thread.currentThread().interrupt();
                     io.reactivex.g.a.a((Throwable)var4);
                     return;
                  }
               }
            }

            if(!this.b.c) {
               this.a.run();
            }
         }

      }
   }

   static final class b implements Comparable {
      final Runnable a;
      final long b;
      final int c;
      volatile boolean d;

      b(Runnable var1, Long var2, int var3) {
         this.a = var1;
         this.b = var2.longValue();
         this.c = var3;
      }

      public int a(n.b var1) {
         int var3 = io.reactivex.d.b.b.a(this.b, var1.b);
         int var2 = var3;
         if(var3 == 0) {
            var2 = io.reactivex.d.b.b.a(this.c, var1.c);
         }

         return var2;
      }

      // $FF: synthetic method
      public int compareTo(Object var1) {
         return this.a((n.b)var1);
      }
   }

   static final class c extends u.c implements io.reactivex.b.b {
      final PriorityBlockingQueue a = new PriorityBlockingQueue();
      final AtomicInteger b = new AtomicInteger();
      volatile boolean c;
      private final AtomicInteger d = new AtomicInteger();

      public io.reactivex.b.b a(Runnable var1) {
         return this.a(var1, this.a(TimeUnit.MILLISECONDS));
      }

      io.reactivex.b.b a(Runnable var1, long var2) {
         Object var5;
         if(this.c) {
            var5 = io.reactivex.d.a.e.a;
         } else {
            n.b var6 = new n.b(var1, Long.valueOf(var2), this.b.incrementAndGet());
            this.a.add(var6);
            if(this.d.getAndIncrement() == 0) {
               int var4 = 1;

               while(true) {
                  if(this.c) {
                     this.a.clear();
                     var5 = io.reactivex.d.a.e.a;
                     break;
                  }

                  var6 = (n.b)this.a.poll();
                  if(var6 == null) {
                     var4 = this.d.addAndGet(-var4);
                     if(var4 == 0) {
                        var5 = io.reactivex.d.a.e.a;
                        break;
                     }
                  } else if(!var6.d) {
                     var6.a.run();
                  }
               }
            } else {
               var5 = io.reactivex.b.c.a(new n.a(var6));
            }
         }

         return (io.reactivex.b.b)var5;
      }

      public io.reactivex.b.b a(Runnable var1, long var2, TimeUnit var4) {
         var2 = this.a(TimeUnit.MILLISECONDS) + var4.toMillis(var2);
         return this.a(new n.a(var1, this, var2), var2);
      }

      public void dispose() {
         this.c = true;
      }

      public boolean isDisposed() {
         return this.c;
      }
   }

   final class a implements Runnable {
      final n.b a;

      a(n.b var2) {
         this.a = var2;
      }

      public void run() {
         this.a.d = true;
         n.super.a.remove(this.a);
      }
   }
}
