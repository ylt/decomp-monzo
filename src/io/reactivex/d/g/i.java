package io.reactivex.d.g;

public final class i extends a implements Runnable {
   public i(Runnable var1) {
      super(var1);
   }

   public void run() {
      this.b = Thread.currentThread();

      try {
         this.a.run();
      } catch (Throwable var4) {
         this.lazySet(c);
         io.reactivex.g.a.a(var4);
      } finally {
         this.b = null;
      }

   }
}
