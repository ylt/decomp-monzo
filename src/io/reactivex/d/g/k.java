package io.reactivex.d.g;

import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicReferenceArray;

public final class k extends AtomicReferenceArray implements io.reactivex.b.b, Runnable, Callable {
   static final Object b = new Object();
   static final Object c = new Object();
   final Runnable a;

   public k(Runnable var1, io.reactivex.d.a.c var2) {
      super(3);
      this.a = var1;
      this.lazySet(0, var2);
   }

   public void a(Future var1) {
      boolean var2 = true;

      Object var3;
      do {
         var3 = this.get(1);
         if(var3 == c) {
            break;
         }

         if(var3 == b) {
            if(this.get(2) == Thread.currentThread()) {
               var2 = false;
            }

            var1.cancel(var2);
            break;
         }
      } while(!this.compareAndSet(1, var3, var1));

   }

   public Object call() {
      this.run();
      return null;
   }

   public void dispose() {
      boolean var1 = true;

      Object var2;
      while(true) {
         var2 = this.get(1);
         if(var2 == c || var2 == b) {
            break;
         }

         if(this.compareAndSet(1, var2, b)) {
            if(var2 != null) {
               Future var3 = (Future)var2;
               if(this.get(2) == Thread.currentThread()) {
                  var1 = false;
               }

               var3.cancel(var1);
            }
            break;
         }
      }

      while(true) {
         var2 = this.get(0);
         if(var2 == c || var2 == b || var2 == null) {
            break;
         }

         if(this.compareAndSet(0, var2, b)) {
            ((io.reactivex.d.a.c)var2).c(this);
            break;
         }
      }

   }

   public boolean isDisposed() {
      boolean var2 = true;
      Object var3 = this.get(1);
      boolean var1 = var2;
      if(var3 != b) {
         if(var3 == c) {
            var1 = var2;
         } else {
            var1 = false;
         }
      }

      return var1;
   }

   public void run() {
      this.lazySet(2, Thread.currentThread());
      boolean var5 = false;

      try {
         var5 = true;
         this.a.run();
         var5 = false;
      } catch (Throwable var6) {
         io.reactivex.g.a.a(var6);
         var5 = false;
      } finally {
         if(var5) {
            this.lazySet(2, (Object)null);
            Object var2 = this.get(0);
            if(var2 != b && var2 != null && this.compareAndSet(0, var2, c)) {
               ((io.reactivex.d.a.c)var2).c(this);
            }

            do {
               var2 = this.get(1);
            } while(var2 != b && !this.compareAndSet(1, var2, c));

         }
      }

      this.lazySet(2, (Object)null);
      Object var1 = this.get(0);
      if(var1 != b && var1 != null && this.compareAndSet(0, var1, c)) {
         ((io.reactivex.d.a.c)var1).c(this);
      }

      do {
         var1 = this.get(1);
      } while(var1 != b && !this.compareAndSet(1, var1, c));

   }
}
