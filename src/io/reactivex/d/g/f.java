package io.reactivex.d.g;

import io.reactivex.u;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

public class f extends u.c implements io.reactivex.b.b {
   volatile boolean a;
   private final ScheduledExecutorService b;

   public f(ThreadFactory var1) {
      this.b = l.a(var1);
   }

   public io.reactivex.b.b a(Runnable var1) {
      return this.a(var1, 0L, (TimeUnit)null);
   }

   public io.reactivex.b.b a(Runnable var1, long var2, TimeUnit var4) {
      Object var5;
      if(this.a) {
         var5 = io.reactivex.d.a.e.a;
      } else {
         var5 = this.a(var1, var2, var4, (io.reactivex.d.a.c)null);
      }

      return (io.reactivex.b.b)var5;
   }

   public k a(Runnable param1, long param2, TimeUnit param4, io.reactivex.d.a.c param5) {
      // $FF: Couldn't be decompiled
   }

   public io.reactivex.b.b b(Runnable param1, long param2, long param4, TimeUnit param6) {
      // $FF: Couldn't be decompiled
   }

   public io.reactivex.b.b b(Runnable param1, long param2, TimeUnit param4) {
      // $FF: Couldn't be decompiled
   }

   public void b() {
      if(!this.a) {
         this.a = true;
         this.b.shutdown();
      }

   }

   public void dispose() {
      if(!this.a) {
         this.a = true;
         this.b.shutdownNow();
      }

   }

   public boolean isDisposed() {
      return this.a;
   }
}
