package io.reactivex.d.g;

import io.reactivex.u;
import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

public final class d extends u {
   static final h b;
   static final h c;
   static final d.c d;
   static final d.a g;
   private static final TimeUnit h;
   final ThreadFactory e;
   final AtomicReference f;

   static {
      h = TimeUnit.SECONDS;
      d = new d.c(new h("RxCachedThreadSchedulerShutdown"));
      d.dispose();
      int var0 = Math.max(1, Math.min(10, Integer.getInteger("rx2.io-priority", 5).intValue()));
      b = new h("RxCachedThreadScheduler", var0);
      c = new h("RxCachedWorkerPoolEvictor", var0);
      g = new d.a(0L, (TimeUnit)null, b);
      g.d();
   }

   public d() {
      this(b);
   }

   public d(ThreadFactory var1) {
      this.e = var1;
      this.f = new AtomicReference(g);
      this.b();
   }

   public u.c a() {
      return new d.b((d.a)this.f.get());
   }

   public void b() {
      d.a var1 = new d.a(60L, h, this.e);
      if(!this.f.compareAndSet(g, var1)) {
         var1.d();
      }

   }

   static final class a implements Runnable {
      final io.reactivex.b.a a;
      private final long b;
      private final ConcurrentLinkedQueue c;
      private final ScheduledExecutorService d;
      private final Future e;
      private final ThreadFactory f;

      a(long var1, TimeUnit var3, ThreadFactory var4) {
         Object var5 = null;
         super();
         if(var3 != null) {
            var1 = var3.toNanos(var1);
         } else {
            var1 = 0L;
         }

         this.b = var1;
         this.c = new ConcurrentLinkedQueue();
         this.a = new io.reactivex.b.a();
         this.f = var4;
         ScheduledExecutorService var6;
         ScheduledFuture var7;
         if(var3 != null) {
            var6 = Executors.newScheduledThreadPool(1, d.c);
            var7 = var6.scheduleWithFixedDelay(this, this.b, this.b, TimeUnit.NANOSECONDS);
         } else {
            var7 = null;
            var6 = (ScheduledExecutorService)var5;
         }

         this.d = var6;
         this.e = var7;
      }

      d.c a() {
         d.c var1;
         if(this.a.isDisposed()) {
            var1 = d.d;
         } else {
            do {
               if(this.c.isEmpty()) {
                  var1 = new d.c(this.f);
                  this.a.a((io.reactivex.b.b)var1);
                  break;
               }

               var1 = (d.c)this.c.poll();
            } while(var1 == null);
         }

         return var1;
      }

      void a(d.c var1) {
         var1.a(this.c() + this.b);
         this.c.offer(var1);
      }

      void b() {
         if(!this.c.isEmpty()) {
            long var1 = this.c();
            Iterator var4 = this.c.iterator();

            while(var4.hasNext()) {
               d.c var3 = (d.c)var4.next();
               if(var3.a() > var1) {
                  break;
               }

               if(this.c.remove(var3)) {
                  this.a.b(var3);
               }
            }
         }

      }

      long c() {
         return System.nanoTime();
      }

      void d() {
         this.a.dispose();
         if(this.e != null) {
            this.e.cancel(true);
         }

         if(this.d != null) {
            this.d.shutdownNow();
         }

      }

      public void run() {
         this.b();
      }
   }

   static final class b extends u.c {
      final AtomicBoolean a = new AtomicBoolean();
      private final io.reactivex.b.a b;
      private final d.a c;
      private final d.c d;

      b(d.a var1) {
         this.c = var1;
         this.b = new io.reactivex.b.a();
         this.d = var1.a();
      }

      public io.reactivex.b.b a(Runnable var1, long var2, TimeUnit var4) {
         Object var5;
         if(this.b.isDisposed()) {
            var5 = io.reactivex.d.a.e.a;
         } else {
            var5 = this.d.a(var1, var2, var4, this.b);
         }

         return (io.reactivex.b.b)var5;
      }

      public void dispose() {
         if(this.a.compareAndSet(false, true)) {
            this.b.dispose();
            this.c.a(this.d);
         }

      }

      public boolean isDisposed() {
         return this.a.get();
      }
   }

   static final class c extends f {
      private long b = 0L;

      c(ThreadFactory var1) {
         super(var1);
      }

      public long a() {
         return this.b;
      }

      public void a(long var1) {
         this.b = var1;
      }
   }
}
