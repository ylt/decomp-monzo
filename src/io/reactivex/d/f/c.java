package io.reactivex.d.f;

import io.reactivex.d.c.h;
import io.reactivex.d.j.q;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReferenceArray;

public final class c implements h {
   static final int a = Integer.getInteger("jctools.spsc.max.lookahead.step", 4096).intValue();
   private static final Object j = new Object();
   final AtomicLong b = new AtomicLong();
   int c;
   long d;
   final int e;
   AtomicReferenceArray f;
   final int g;
   AtomicReferenceArray h;
   final AtomicLong i = new AtomicLong();

   public c(int var1) {
      int var2 = q.a(Math.max(8, var1));
      var1 = var2 - 1;
      AtomicReferenceArray var3 = new AtomicReferenceArray(var2 + 1);
      this.f = var3;
      this.e = var1;
      this.a(var2);
      this.h = var3;
      this.g = var1;
      this.d = (long)(var1 - 1);
      this.a(0L);
   }

   private static int a(long var0, int var2) {
      return b((int)var0 & var2);
   }

   private Object a(AtomicReferenceArray var1, long var2, int var4) {
      this.h = var1;
      var4 = a(var2, var4);
      Object var5 = b(var1, var4);
      if(var5 != null) {
         a(var1, var4, (Object)null);
         this.b(1L + var2);
      }

      return var5;
   }

   private AtomicReferenceArray a(AtomicReferenceArray var1, int var2) {
      var2 = b(var2);
      AtomicReferenceArray var3 = (AtomicReferenceArray)b(var1, var2);
      a(var1, var2, (Object)null);
      return var3;
   }

   private void a(int var1) {
      this.c = Math.min(var1 / 4, a);
   }

   private void a(long var1) {
      this.b.lazySet(var1);
   }

   private static void a(AtomicReferenceArray var0, int var1, Object var2) {
      var0.lazySet(var1, var2);
   }

   private void a(AtomicReferenceArray var1, long var2, int var4, Object var5, long var6) {
      AtomicReferenceArray var8 = new AtomicReferenceArray(var1.length());
      this.f = var8;
      this.d = var2 + var6 - 1L;
      a(var8, var4, var5);
      this.a(var1, var8);
      a(var1, var4, j);
      this.a(var2 + 1L);
   }

   private void a(AtomicReferenceArray var1, AtomicReferenceArray var2) {
      a(var1, b(var1.length() - 1), var2);
   }

   private boolean a(AtomicReferenceArray var1, Object var2, long var3, int var5) {
      a(var1, var5, var2);
      this.a(1L + var3);
      return true;
   }

   private static int b(int var0) {
      return var0;
   }

   private static Object b(AtomicReferenceArray var0, int var1) {
      return var0.get(var1);
   }

   private Object b(AtomicReferenceArray var1, long var2, int var4) {
      this.h = var1;
      return b(var1, a(var2, var4));
   }

   private void b(long var1) {
      this.i.lazySet(var1);
   }

   private long f() {
      return this.b.get();
   }

   private long g() {
      return this.i.get();
   }

   private long h() {
      return this.b.get();
   }

   private long i() {
      return this.i.get();
   }

   public boolean a(Object var1) {
      if(var1 == null) {
         throw new NullPointerException("Null is not a valid element");
      } else {
         AtomicReferenceArray var8 = this.f;
         long var6 = this.h();
         int var3 = this.e;
         int var4 = a(var6, var3);
         boolean var5;
         if(var6 < this.d) {
            var5 = this.a(var8, var1, var6, var4);
         } else {
            int var2 = this.c;
            if(b(var8, a((long)var2 + var6, var3)) == null) {
               this.d = (long)var2 + var6 - 1L;
               var5 = this.a(var8, var1, var6, var4);
            } else if(b(var8, a(1L + var6, var3)) == null) {
               var5 = this.a(var8, var1, var6, var4);
            } else {
               this.a(var8, var6, var4, var1, (long)var3);
               var5 = true;
            }
         }

         return var5;
      }
   }

   public boolean a(Object var1, Object var2) {
      AtomicReferenceArray var6 = this.f;
      long var4 = this.f();
      int var3 = this.e;
      if(b(var6, a(var4 + 2L, var3)) == null) {
         var3 = a(var4, var3);
         a(var6, var3 + 1, var2);
         a(var6, var3, var1);
         this.a(var4 + 2L);
      } else {
         AtomicReferenceArray var7 = new AtomicReferenceArray(var6.length());
         this.f = var7;
         var3 = a(var4, var3);
         a(var7, var3 + 1, var2);
         a(var7, var3, var1);
         this.a(var6, var7);
         a(var6, var3, j);
         this.a(var4 + 2L);
      }

      return true;
   }

   public boolean b() {
      boolean var1;
      if(this.f() == this.g()) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public void c() {
      while(this.n_() != null || !this.b()) {
         ;
      }

   }

   public Object d() {
      AtomicReferenceArray var6 = this.h;
      long var2 = this.i();
      int var1 = this.g;
      Object var5 = b(var6, a(var2, var1));
      Object var4 = var5;
      if(var5 == j) {
         var4 = this.b(this.a(var6, var1 + 1), var2, var1);
      }

      return var4;
   }

   public int e() {
      long var1 = this.g();

      while(true) {
         long var5 = this.f();
         long var3 = this.g();
         if(var1 == var3) {
            return (int)(var5 - var3);
         }

         var1 = var3;
      }
   }

   public Object n_() {
      AtomicReferenceArray var7 = this.h;
      long var4 = this.i();
      int var2 = this.g;
      int var3 = a(var4, var2);
      Object var6 = b(var7, var3);
      boolean var1;
      if(var6 == j) {
         var1 = true;
      } else {
         var1 = false;
      }

      if(var6 != null && !var1) {
         a(var7, var3, (Object)null);
         this.b(1L + var4);
      } else if(var1) {
         var6 = this.a(this.a(var7, var2 + 1), var4, var2);
      } else {
         var6 = null;
      }

      return var6;
   }
}
