package io.reactivex.d.f;

import io.reactivex.d.c.h;
import java.util.concurrent.atomic.AtomicReference;

public final class a implements h {
   private final AtomicReference a = new AtomicReference();
   private final AtomicReference b = new AtomicReference();

   public a() {
      a.a var1 = new a.a();
      this.b(var1);
      this.a(var1);
   }

   a.a a(a.a var1) {
      return (a.a)this.a.getAndSet(var1);
   }

   public boolean a(Object var1) {
      if(var1 == null) {
         throw new NullPointerException("Null is not a valid element");
      } else {
         a.a var2 = new a.a(var1);
         this.a(var2).a(var2);
         return true;
      }
   }

   void b(a.a var1) {
      this.b.lazySet(var1);
   }

   public boolean b() {
      boolean var1;
      if(this.e() == this.d()) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public void c() {
      while(this.n_() != null && !this.b()) {
         ;
      }

   }

   a.a d() {
      return (a.a)this.a.get();
   }

   a.a e() {
      return (a.a)this.b.get();
   }

   a.a f() {
      return (a.a)this.b.get();
   }

   public Object n_() {
      a.a var1 = this.f();
      a.a var2 = var1.c();
      Object var3;
      if(var2 != null) {
         var3 = var2.a();
         this.b(var2);
      } else if(var1 != this.d()) {
         do {
            var2 = var1.c();
         } while(var2 == null);

         var3 = var2.a();
         this.b(var2);
      } else {
         var3 = null;
      }

      return var3;
   }

   static final class a extends AtomicReference {
      private Object a;

      a() {
      }

      a(Object var1) {
         this.a(var1);
      }

      public Object a() {
         Object var1 = this.b();
         this.a((Object)null);
         return var1;
      }

      public void a(a.a var1) {
         this.lazySet(var1);
      }

      public void a(Object var1) {
         this.a = var1;
      }

      public Object b() {
         return this.a;
      }

      public a.a c() {
         return (a.a)this.get();
      }
   }
}
