package io.reactivex.d.f;

import io.reactivex.d.c.h;
import io.reactivex.d.j.q;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReferenceArray;

public final class b extends AtomicReferenceArray implements h {
   private static final Integer f = Integer.getInteger("jctools.spsc.max.lookahead.step", 4096);
   final int a = this.length() - 1;
   final AtomicLong b = new AtomicLong();
   long c;
   final AtomicLong d = new AtomicLong();
   final int e;

   public b(int var1) {
      super(q.a(var1));
      this.e = Math.min(var1 / 4, f.intValue());
   }

   int a(long var1, int var3) {
      return (int)var1 & var3;
   }

   Object a(int var1) {
      return this.get(var1);
   }

   void a(int var1, Object var2) {
      this.lazySet(var1, var2);
   }

   void a(long var1) {
      this.b.lazySet(var1);
   }

   public boolean a(Object var1) {
      if(var1 == null) {
         throw new NullPointerException("Null is not a valid element");
      } else {
         int var4 = this.a;
         long var5 = this.b.get();
         int var3 = this.a(var5, var4);
         boolean var7;
         if(var5 >= this.c) {
            int var2 = this.e;
            if(this.a(this.a((long)var2 + var5, var4)) == null) {
               this.c = (long)var2 + var5;
            } else if(this.a(var3) != null) {
               var7 = false;
               return var7;
            }
         }

         this.a(var3, var1);
         this.a(1L + var5);
         var7 = true;
         return var7;
      }
   }

   void b(long var1) {
      this.d.lazySet(var1);
   }

   public boolean b() {
      boolean var1;
      if(this.b.get() == this.d.get()) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   int c(long var1) {
      return (int)var1 & this.a;
   }

   public void c() {
      while(this.n_() != null || !this.b()) {
         ;
      }

   }

   public Object n_() {
      Object var5 = null;
      long var2 = this.d.get();
      int var1 = this.c(var2);
      Object var4 = this.a(var1);
      if(var4 == null) {
         var4 = var5;
      } else {
         this.b(var2 + 1L);
         this.a(var1, (Object)null);
      }

      return var4;
   }
}
