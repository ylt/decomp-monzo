package io.reactivex.d.e.c;

public final class n extends io.reactivex.h implements io.reactivex.d.c.g {
   final Object a;

   public n(Object var1) {
      this.a = var1;
   }

   protected void b(io.reactivex.j var1) {
      var1.onSubscribe(io.reactivex.b.c.b());
      var1.a_(this.a);
   }

   public Object call() {
      return this.a;
   }
}
