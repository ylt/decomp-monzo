package io.reactivex.d.e.c;

import io.reactivex.v;
import io.reactivex.x;

public final class m extends v implements io.reactivex.d.c.b {
   final io.reactivex.l a;

   public m(io.reactivex.l var1) {
      this.a = var1;
   }

   protected void b(x var1) {
      this.a.a(new m.a(var1));
   }

   public io.reactivex.h p_() {
      return io.reactivex.g.a.a((io.reactivex.h)(new l(this.a)));
   }

   static final class a implements io.reactivex.b.b, io.reactivex.j {
      final x a;
      io.reactivex.b.b b;

      a(x var1) {
         this.a = var1;
      }

      public void a_(Object var1) {
         this.b = io.reactivex.d.a.d.a;
         this.a.a_(Boolean.valueOf(false));
      }

      public void dispose() {
         this.b.dispose();
         this.b = io.reactivex.d.a.d.a;
      }

      public boolean isDisposed() {
         return this.b.isDisposed();
      }

      public void onComplete() {
         this.b = io.reactivex.d.a.d.a;
         this.a.a_(Boolean.valueOf(true));
      }

      public void onError(Throwable var1) {
         this.b = io.reactivex.d.a.d.a;
         this.a.onError(var1);
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.b, var1)) {
            this.b = var1;
            this.a.onSubscribe(this);
         }

      }
   }
}
