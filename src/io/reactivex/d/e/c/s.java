package io.reactivex.d.e.c;

import io.reactivex.exceptions.CompositeException;

public final class s extends a {
   final io.reactivex.c.g b;
   final io.reactivex.c.g c;
   final io.reactivex.c.g d;
   final io.reactivex.c.a e;
   final io.reactivex.c.a f;
   final io.reactivex.c.a g;

   public s(io.reactivex.l var1, io.reactivex.c.g var2, io.reactivex.c.g var3, io.reactivex.c.g var4, io.reactivex.c.a var5, io.reactivex.c.a var6, io.reactivex.c.a var7) {
      super(var1);
      this.b = var2;
      this.c = var3;
      this.d = var4;
      this.e = var5;
      this.f = var6;
      this.g = var7;
   }

   protected void b(io.reactivex.j var1) {
      this.a.a(new s.a(var1, this));
   }

   static final class a implements io.reactivex.b.b, io.reactivex.j {
      final io.reactivex.j a;
      final s b;
      io.reactivex.b.b c;

      a(io.reactivex.j var1, s var2) {
         this.a = var1;
         this.b = var2;
      }

      void a() {
         try {
            this.b.f.a();
         } catch (Throwable var2) {
            io.reactivex.exceptions.a.b(var2);
            io.reactivex.g.a.a(var2);
         }

      }

      void a(Throwable var1) {
         try {
            this.b.d.a(var1);
         } catch (Throwable var3) {
            io.reactivex.exceptions.a.b(var3);
            var1 = new CompositeException(new Throwable[]{(Throwable)var1, var3});
         }

         this.c = io.reactivex.d.a.d.a;
         this.a.onError((Throwable)var1);
         this.a();
      }

      public void a_(Object var1) {
         if(this.c != io.reactivex.d.a.d.a) {
            try {
               this.b.c.a(var1);
            } catch (Throwable var2) {
               io.reactivex.exceptions.a.b(var2);
               this.a(var2);
               return;
            }

            this.c = io.reactivex.d.a.d.a;
            this.a.a_(var1);
            this.a();
         }

      }

      public void dispose() {
         try {
            this.b.g.a();
         } catch (Throwable var2) {
            io.reactivex.exceptions.a.b(var2);
            io.reactivex.g.a.a(var2);
         }

         this.c.dispose();
         this.c = io.reactivex.d.a.d.a;
      }

      public boolean isDisposed() {
         return this.c.isDisposed();
      }

      public void onComplete() {
         if(this.c != io.reactivex.d.a.d.a) {
            try {
               this.b.e.a();
            } catch (Throwable var2) {
               io.reactivex.exceptions.a.b(var2);
               this.a(var2);
               return;
            }

            this.c = io.reactivex.d.a.d.a;
            this.a.onComplete();
            this.a();
         }

      }

      public void onError(Throwable var1) {
         if(this.c == io.reactivex.d.a.d.a) {
            io.reactivex.g.a.a(var1);
         } else {
            this.a(var1);
         }

      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.c, var1)) {
            try {
               this.b.b.a(var1);
            } catch (Throwable var3) {
               io.reactivex.exceptions.a.b(var3);
               var1.dispose();
               this.c = io.reactivex.d.a.d.a;
               io.reactivex.d.a.e.a(var3, this.a);
               return;
            }

            this.c = var1;
            this.a.onSubscribe(this);
         }

      }
   }
}
