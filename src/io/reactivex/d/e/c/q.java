package io.reactivex.d.e.c;

import io.reactivex.exceptions.CompositeException;

public final class q extends a {
   final io.reactivex.c.q b;

   public q(io.reactivex.l var1, io.reactivex.c.q var2) {
      super(var1);
      this.b = var2;
   }

   protected void b(io.reactivex.j var1) {
      this.a.a(new q.a(var1, this.b));
   }

   static final class a implements io.reactivex.b.b, io.reactivex.j {
      final io.reactivex.j a;
      final io.reactivex.c.q b;
      io.reactivex.b.b c;

      a(io.reactivex.j var1, io.reactivex.c.q var2) {
         this.a = var1;
         this.b = var2;
      }

      public void a_(Object var1) {
         this.a.a_(var1);
      }

      public void dispose() {
         this.c.dispose();
      }

      public boolean isDisposed() {
         return this.c.isDisposed();
      }

      public void onComplete() {
         this.a.onComplete();
      }

      public void onError(Throwable var1) {
         boolean var2;
         try {
            var2 = this.b.a(var1);
         } catch (Throwable var4) {
            io.reactivex.exceptions.a.b(var4);
            this.a.onError(new CompositeException(new Throwable[]{var1, var4}));
            return;
         }

         if(var2) {
            this.a.onComplete();
         } else {
            this.a.onError(var1);
         }

      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.c, var1)) {
            this.c = var1;
            this.a.onSubscribe(this);
         }

      }
   }
}
