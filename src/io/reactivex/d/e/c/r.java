package io.reactivex.d.e.c;

import io.reactivex.exceptions.CompositeException;
import java.util.concurrent.atomic.AtomicReference;

public final class r extends a {
   final io.reactivex.c.h b;
   final boolean c;

   public r(io.reactivex.l var1, io.reactivex.c.h var2, boolean var3) {
      super(var1);
      this.b = var2;
      this.c = var3;
   }

   protected void b(io.reactivex.j var1) {
      this.a.a(new r.a(var1, this.b, this.c));
   }

   static final class a extends AtomicReference implements io.reactivex.b.b, io.reactivex.j {
      final io.reactivex.j a;
      final io.reactivex.c.h b;
      final boolean c;

      a(io.reactivex.j var1, io.reactivex.c.h var2, boolean var3) {
         this.a = var1;
         this.b = var2;
         this.c = var3;
      }

      public void a_(Object var1) {
         this.a.a_(var1);
      }

      public void dispose() {
         io.reactivex.d.a.d.a((AtomicReference)this);
      }

      public boolean isDisposed() {
         return io.reactivex.d.a.d.a((io.reactivex.b.b)this.get());
      }

      public void onComplete() {
         this.a.onComplete();
      }

      public void onError(Throwable var1) {
         if(!this.c && !(var1 instanceof Exception)) {
            this.a.onError(var1);
         } else {
            io.reactivex.l var2;
            try {
               var2 = (io.reactivex.l)io.reactivex.d.b.b.a(this.b.a(var1), "The resumeFunction returned a null MaybeSource");
            } catch (Throwable var3) {
               io.reactivex.exceptions.a.b(var3);
               this.a.onError(new CompositeException(new Throwable[]{var1, var3}));
               return;
            }

            io.reactivex.d.a.d.c(this, (io.reactivex.b.b)null);
            var2.a(new r.a(this.a, this));
         }

      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.b(this, var1)) {
            this.a.onSubscribe(this);
         }

      }
   }

   static final class a implements io.reactivex.j {
      final io.reactivex.j a;
      final AtomicReference b;

      a(io.reactivex.j var1, AtomicReference var2) {
         this.a = var1;
         this.b = var2;
      }

      public void a_(Object var1) {
         this.a.a_(var1);
      }

      public void onComplete() {
         this.a.onComplete();
      }

      public void onError(Throwable var1) {
         this.a.onError(var1);
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         io.reactivex.d.a.d.b(this.b, var1);
      }
   }
}
