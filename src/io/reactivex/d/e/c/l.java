package io.reactivex.d.e.c;

public final class l extends a {
   public l(io.reactivex.l var1) {
      super(var1);
   }

   protected void b(io.reactivex.j var1) {
      this.a.a(new l.a(var1));
   }

   static final class a implements io.reactivex.b.b, io.reactivex.j {
      final io.reactivex.j a;
      io.reactivex.b.b b;

      a(io.reactivex.j var1) {
         this.a = var1;
      }

      public void a_(Object var1) {
         this.a.a_(Boolean.valueOf(false));
      }

      public void dispose() {
         this.b.dispose();
      }

      public boolean isDisposed() {
         return this.b.isDisposed();
      }

      public void onComplete() {
         this.a.a_(Boolean.valueOf(true));
      }

      public void onError(Throwable var1) {
         this.a.onError(var1);
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.b, var1)) {
            this.b = var1;
            this.a.onSubscribe(this);
         }

      }
   }
}
