package io.reactivex.d.e.c;

import io.reactivex.x;
import io.reactivex.z;

public final class f extends io.reactivex.h {
   final z a;
   final io.reactivex.c.q b;

   public f(z var1, io.reactivex.c.q var2) {
      this.a = var1;
      this.b = var2;
   }

   protected void b(io.reactivex.j var1) {
      this.a.a(new f.a(var1, this.b));
   }

   static final class a implements io.reactivex.b.b, x {
      final io.reactivex.j a;
      final io.reactivex.c.q b;
      io.reactivex.b.b c;

      a(io.reactivex.j var1, io.reactivex.c.q var2) {
         this.a = var1;
         this.b = var2;
      }

      public void a_(Object var1) {
         boolean var2;
         try {
            var2 = this.b.a(var1);
         } catch (Throwable var3) {
            io.reactivex.exceptions.a.b(var3);
            this.a.onError(var3);
            return;
         }

         if(var2) {
            this.a.a_(var1);
         } else {
            this.a.onComplete();
         }

      }

      public void dispose() {
         io.reactivex.b.b var1 = this.c;
         this.c = io.reactivex.d.a.d.a;
         var1.dispose();
      }

      public boolean isDisposed() {
         return this.c.isDisposed();
      }

      public void onError(Throwable var1) {
         this.a.onError(var1);
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.c, var1)) {
            this.c = var1;
            this.a.onSubscribe(this);
         }

      }
   }
}
