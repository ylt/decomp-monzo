package io.reactivex.d.e.c;

import java.util.concurrent.atomic.AtomicReference;

public final class t extends a {
   final io.reactivex.u b;

   public t(io.reactivex.l var1, io.reactivex.u var2) {
      super(var1);
      this.b = var2;
   }

   protected void b(io.reactivex.j var1) {
      t.a var2 = new t.a(var1);
      var1.onSubscribe(var2);
      var2.a.b(this.b.a((Runnable)(new t.b(var2, this.a))));
   }

   static final class a extends AtomicReference implements io.reactivex.b.b, io.reactivex.j {
      final io.reactivex.d.a.k a;
      final io.reactivex.j b;

      a(io.reactivex.j var1) {
         this.b = var1;
         this.a = new io.reactivex.d.a.k();
      }

      public void a_(Object var1) {
         this.b.a_(var1);
      }

      public void dispose() {
         io.reactivex.d.a.d.a((AtomicReference)this);
         this.a.dispose();
      }

      public boolean isDisposed() {
         return io.reactivex.d.a.d.a((io.reactivex.b.b)this.get());
      }

      public void onComplete() {
         this.b.onComplete();
      }

      public void onError(Throwable var1) {
         this.b.onError(var1);
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         io.reactivex.d.a.d.b(this, var1);
      }
   }

   static final class b implements Runnable {
      final io.reactivex.j a;
      final io.reactivex.l b;

      b(io.reactivex.j var1, io.reactivex.l var2) {
         this.a = var1;
         this.b = var2;
      }

      public void run() {
         this.b.a(this.a);
      }
   }
}
