package io.reactivex.d.e.c;

import java.util.concurrent.atomic.AtomicReference;

public final class p extends a {
   final io.reactivex.u b;

   public p(io.reactivex.l var1, io.reactivex.u var2) {
      super(var1);
      this.b = var2;
   }

   protected void b(io.reactivex.j var1) {
      this.a.a(new p.a(var1, this.b));
   }

   static final class a extends AtomicReference implements io.reactivex.b.b, io.reactivex.j, Runnable {
      final io.reactivex.j a;
      final io.reactivex.u b;
      Object c;
      Throwable d;

      a(io.reactivex.j var1, io.reactivex.u var2) {
         this.a = var1;
         this.b = var2;
      }

      public void a_(Object var1) {
         this.c = var1;
         io.reactivex.d.a.d.c(this, this.b.a((Runnable)this));
      }

      public void dispose() {
         io.reactivex.d.a.d.a((AtomicReference)this);
      }

      public boolean isDisposed() {
         return io.reactivex.d.a.d.a((io.reactivex.b.b)this.get());
      }

      public void onComplete() {
         io.reactivex.d.a.d.c(this, this.b.a((Runnable)this));
      }

      public void onError(Throwable var1) {
         this.d = var1;
         io.reactivex.d.a.d.c(this, this.b.a((Runnable)this));
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.b(this, var1)) {
            this.a.onSubscribe(this);
         }

      }

      public void run() {
         Throwable var1 = this.d;
         if(var1 != null) {
            this.d = null;
            this.a.onError(var1);
         } else {
            Object var2 = this.c;
            if(var2 != null) {
               this.c = null;
               this.a.a_(var2);
            } else {
               this.a.onComplete();
            }
         }

      }
   }
}
