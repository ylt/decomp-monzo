package io.reactivex.d.e.c;

import io.reactivex.x;
import io.reactivex.z;

public final class i extends io.reactivex.h {
   final z a;

   public i(z var1) {
      this.a = var1;
   }

   protected void b(io.reactivex.j var1) {
      this.a.a(new i.a(var1));
   }

   static final class a implements io.reactivex.b.b, x {
      final io.reactivex.j a;
      io.reactivex.b.b b;

      a(io.reactivex.j var1) {
         this.a = var1;
      }

      public void a_(Object var1) {
         this.b = io.reactivex.d.a.d.a;
         this.a.a_(var1);
      }

      public void dispose() {
         this.b.dispose();
         this.b = io.reactivex.d.a.d.a;
      }

      public boolean isDisposed() {
         return this.b.isDisposed();
      }

      public void onError(Throwable var1) {
         this.b = io.reactivex.d.a.d.a;
         this.a.onError(var1);
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.b, var1)) {
            this.b = var1;
            this.a.onSubscribe(this);
         }

      }
   }
}
