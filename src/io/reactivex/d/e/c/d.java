package io.reactivex.d.e.c;

import io.reactivex.exceptions.CompositeException;

public final class d extends a {
   final io.reactivex.c.b b;

   public d(io.reactivex.l var1, io.reactivex.c.b var2) {
      super(var1);
      this.b = var2;
   }

   protected void b(io.reactivex.j var1) {
      this.a.a(new d.a(var1, this.b));
   }

   static final class a implements io.reactivex.b.b, io.reactivex.j {
      final io.reactivex.j a;
      final io.reactivex.c.b b;
      io.reactivex.b.b c;

      a(io.reactivex.j var1, io.reactivex.c.b var2) {
         this.a = var1;
         this.b = var2;
      }

      public void a_(Object var1) {
         this.c = io.reactivex.d.a.d.a;

         try {
            this.b.a(var1, (Object)null);
         } catch (Throwable var2) {
            io.reactivex.exceptions.a.b(var2);
            this.a.onError(var2);
            return;
         }

         this.a.a_(var1);
      }

      public void dispose() {
         this.c.dispose();
         this.c = io.reactivex.d.a.d.a;
      }

      public boolean isDisposed() {
         return this.c.isDisposed();
      }

      public void onComplete() {
         this.c = io.reactivex.d.a.d.a;

         try {
            this.b.a((Object)null, (Object)null);
         } catch (Throwable var2) {
            io.reactivex.exceptions.a.b(var2);
            this.a.onError(var2);
            return;
         }

         this.a.onComplete();
      }

      public void onError(Throwable var1) {
         this.c = io.reactivex.d.a.d.a;

         try {
            this.b.a((Object)null, var1);
         } catch (Throwable var3) {
            io.reactivex.exceptions.a.b(var3);
            var1 = new CompositeException(new Throwable[]{(Throwable)var1, var3});
         }

         this.a.onError((Throwable)var1);
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.c, var1)) {
            this.c = var1;
            this.a.onSubscribe(this);
         }

      }
   }
}
