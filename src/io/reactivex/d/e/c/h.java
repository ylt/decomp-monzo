package io.reactivex.d.e.c;

import java.util.concurrent.Callable;

public final class h extends io.reactivex.h implements Callable {
   final Callable a;

   public h(Callable var1) {
      this.a = var1;
   }

   protected void b(io.reactivex.j var1) {
      io.reactivex.b.b var2 = io.reactivex.b.c.a();
      var1.onSubscribe(var2);
      if(!var2.isDisposed()) {
         Object var3;
         try {
            var3 = this.a.call();
         } catch (Throwable var4) {
            io.reactivex.exceptions.a.b(var4);
            if(!var2.isDisposed()) {
               var1.onError(var4);
            } else {
               io.reactivex.g.a.a(var4);
            }

            return;
         }

         if(!var2.isDisposed()) {
            if(var3 == null) {
               var1.onComplete();
            } else {
               var1.a_(var3);
            }
         }
      }

   }

   public Object call() throws Exception {
      return this.a.call();
   }
}
