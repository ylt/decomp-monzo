package io.reactivex.d.e.c;

public final class u extends io.reactivex.n {
   final io.reactivex.l a;

   public u(io.reactivex.l var1) {
      this.a = var1;
   }

   protected void subscribeActual(io.reactivex.t var1) {
      this.a.a(new u.a(var1));
   }

   static final class a extends io.reactivex.d.d.k implements io.reactivex.j {
      io.reactivex.b.b c;

      a(io.reactivex.t var1) {
         super(var1);
      }

      public void a_(Object var1) {
         this.b(var1);
      }

      public void dispose() {
         super.dispose();
         this.c.dispose();
      }

      public void onComplete() {
         this.d();
      }

      public void onError(Throwable var1) {
         this.a(var1);
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.c, var1)) {
            this.c = var1;
            this.a.onSubscribe(this);
         }

      }
   }
}
