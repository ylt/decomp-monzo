package io.reactivex.d.e.c;

public final class k extends io.reactivex.b implements io.reactivex.d.c.b {
   final io.reactivex.l a;

   public k(io.reactivex.l var1) {
      this.a = var1;
   }

   protected void b(io.reactivex.c var1) {
      this.a.a(new k.a(var1));
   }

   public io.reactivex.h p_() {
      return io.reactivex.g.a.a((io.reactivex.h)(new j(this.a)));
   }

   static final class a implements io.reactivex.b.b, io.reactivex.j {
      final io.reactivex.c a;
      io.reactivex.b.b b;

      a(io.reactivex.c var1) {
         this.a = var1;
      }

      public void a_(Object var1) {
         this.b = io.reactivex.d.a.d.a;
         this.a.onComplete();
      }

      public void dispose() {
         this.b.dispose();
         this.b = io.reactivex.d.a.d.a;
      }

      public boolean isDisposed() {
         return this.b.isDisposed();
      }

      public void onComplete() {
         this.b = io.reactivex.d.a.d.a;
         this.a.onComplete();
      }

      public void onError(Throwable var1) {
         this.b = io.reactivex.d.a.d.a;
         this.a.onError(var1);
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.b, var1)) {
            this.b = var1;
            this.a.onSubscribe(this);
         }

      }
   }
}
