package io.reactivex.d.e.c;

import java.util.concurrent.atomic.AtomicReference;

public final class g extends io.reactivex.b {
   final io.reactivex.l a;
   final io.reactivex.c.h b;

   public g(io.reactivex.l var1, io.reactivex.c.h var2) {
      this.a = var1;
      this.b = var2;
   }

   protected void b(io.reactivex.c var1) {
      g.a var2 = new g.a(var1, this.b);
      var1.onSubscribe(var2);
      this.a.a(var2);
   }

   static final class a extends AtomicReference implements io.reactivex.b.b, io.reactivex.c, io.reactivex.j {
      final io.reactivex.c a;
      final io.reactivex.c.h b;

      a(io.reactivex.c var1, io.reactivex.c.h var2) {
         this.a = var1;
         this.b = var2;
      }

      public void a_(Object var1) {
         io.reactivex.d var3;
         try {
            var3 = (io.reactivex.d)io.reactivex.d.b.b.a(this.b.a(var1), "The mapper returned a null CompletableSource");
         } catch (Throwable var2) {
            io.reactivex.exceptions.a.b(var2);
            this.onError(var2);
            return;
         }

         if(!this.isDisposed()) {
            var3.a(this);
         }

      }

      public void dispose() {
         io.reactivex.d.a.d.a((AtomicReference)this);
      }

      public boolean isDisposed() {
         return io.reactivex.d.a.d.a((io.reactivex.b.b)this.get());
      }

      public void onComplete() {
         this.a.onComplete();
      }

      public void onError(Throwable var1) {
         this.a.onError(var1);
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         io.reactivex.d.a.d.c(this, var1);
      }
   }
}
