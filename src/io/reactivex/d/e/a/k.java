package io.reactivex.d.e.a;

import java.util.Iterator;
import java.util.concurrent.atomic.AtomicInteger;

public final class k extends io.reactivex.b {
   final Iterable a;

   public k(Iterable var1) {
      this.a = var1;
   }

   public void b(io.reactivex.c var1) {
      io.reactivex.b.a var7 = new io.reactivex.b.a();
      var1.onSubscribe(var7);

      Iterator var6;
      try {
         var6 = (Iterator)io.reactivex.d.b.b.a(this.a.iterator(), (String)"The source iterator returned is null");
      } catch (Throwable var10) {
         io.reactivex.exceptions.a.b(var10);
         var1.onError(var10);
         return;
      }

      AtomicInteger var3 = new AtomicInteger(1);
      io.reactivex.d.j.c var4 = new io.reactivex.d.j.c();

      while(true) {
         if(var7.isDisposed()) {
            return;
         }

         boolean var2;
         try {
            var2 = var6.hasNext();
         } catch (Throwable var9) {
            io.reactivex.exceptions.a.b(var9);
            var4.a(var9);
            break;
         }

         if(!var2) {
            break;
         }

         if(var7.isDisposed()) {
            return;
         }

         io.reactivex.d var5;
         try {
            var5 = (io.reactivex.d)io.reactivex.d.b.b.a(var6.next(), "The iterator returned a null CompletableSource");
         } catch (Throwable var8) {
            io.reactivex.exceptions.a.b(var8);
            var4.a(var8);
            break;
         }

         if(var7.isDisposed()) {
            return;
         }

         var3.getAndIncrement();
         var5.a(new j.a(var1, var7, var4, var3));
      }

      if(var3.decrementAndGet() == 0) {
         Throwable var11 = var4.a();
         if(var11 == null) {
            var1.onComplete();
         } else {
            var1.onError(var11);
         }
      }

   }
}
