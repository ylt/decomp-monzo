package io.reactivex.d.e.a;

import java.util.concurrent.atomic.AtomicInteger;

public final class a extends io.reactivex.b {
   final io.reactivex.d[] a;

   public a(io.reactivex.d[] var1) {
      this.a = var1;
   }

   public void b(io.reactivex.c var1) {
      a.a var2 = new a.a(var1, this.a);
      var1.onSubscribe(var2.d);
      var2.a();
   }

   static final class a extends AtomicInteger implements io.reactivex.c {
      final io.reactivex.c a;
      final io.reactivex.d[] b;
      int c;
      final io.reactivex.d.a.k d;

      a(io.reactivex.c var1, io.reactivex.d[] var2) {
         this.a = var1;
         this.b = var2;
         this.d = new io.reactivex.d.a.k();
      }

      void a() {
         if(!this.d.isDisposed() && this.getAndIncrement() == 0) {
            io.reactivex.d[] var2 = this.b;

            while(!this.d.isDisposed()) {
               int var1 = this.c;
               this.c = var1 + 1;
               if(var1 == var2.length) {
                  this.a.onComplete();
                  break;
               }

               var2[var1].a(this);
               if(this.decrementAndGet() == 0) {
                  break;
               }
            }
         }

      }

      public void onComplete() {
         this.a();
      }

      public void onError(Throwable var1) {
         this.a.onError(var1);
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         this.d.a(var1);
      }
   }
}
