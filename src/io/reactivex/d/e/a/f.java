package io.reactivex.d.e.a;

public final class f extends io.reactivex.b {
   final io.reactivex.c.a a;

   public f(io.reactivex.c.a var1) {
      this.a = var1;
   }

   protected void b(io.reactivex.c var1) {
      io.reactivex.b.b var3 = io.reactivex.b.c.a();
      var1.onSubscribe(var3);

      try {
         this.a.a();
      } catch (Throwable var4) {
         io.reactivex.exceptions.a.b(var4);
         if(!var3.isDisposed()) {
            var1.onError(var4);
         }

         return;
      }

      if(!var3.isDisposed()) {
         var1.onComplete();
      }

   }
}
