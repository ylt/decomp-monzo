package io.reactivex.d.e.a;

import io.reactivex.exceptions.CompositeException;

public final class n extends io.reactivex.b {
   final io.reactivex.d a;
   final io.reactivex.c.g b;
   final io.reactivex.c.g c;
   final io.reactivex.c.a d;
   final io.reactivex.c.a e;
   final io.reactivex.c.a f;
   final io.reactivex.c.a g;

   public n(io.reactivex.d var1, io.reactivex.c.g var2, io.reactivex.c.g var3, io.reactivex.c.a var4, io.reactivex.c.a var5, io.reactivex.c.a var6, io.reactivex.c.a var7) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
      this.d = var4;
      this.e = var5;
      this.f = var6;
      this.g = var7;
   }

   protected void b(io.reactivex.c var1) {
      this.a.a(new n.a(var1));
   }

   final class a implements io.reactivex.b.b, io.reactivex.c {
      final io.reactivex.c a;
      io.reactivex.b.b b;

      a(io.reactivex.c var2) {
         this.a = var2;
      }

      void a() {
         try {
            n.this.f.a();
         } catch (Throwable var2) {
            io.reactivex.exceptions.a.b(var2);
            io.reactivex.g.a.a(var2);
         }

      }

      public void dispose() {
         try {
            n.this.g.a();
         } catch (Throwable var2) {
            io.reactivex.exceptions.a.b(var2);
            io.reactivex.g.a.a(var2);
         }

         this.b.dispose();
      }

      public boolean isDisposed() {
         return this.b.isDisposed();
      }

      public void onComplete() {
         if(this.b != io.reactivex.d.a.d.a) {
            try {
               n.this.d.a();
               n.this.e.a();
            } catch (Throwable var2) {
               io.reactivex.exceptions.a.b(var2);
               this.a.onError(var2);
               return;
            }

            this.a.onComplete();
            this.a();
         }

      }

      public void onError(Throwable var1) {
         if(this.b == io.reactivex.d.a.d.a) {
            io.reactivex.g.a.a((Throwable)var1);
         } else {
            try {
               n.this.c.a(var1);
               n.this.e.a();
            } catch (Throwable var3) {
               io.reactivex.exceptions.a.b(var3);
               var1 = new CompositeException(new Throwable[]{(Throwable)var1, var3});
            }

            this.a.onError((Throwable)var1);
            this.a();
         }

      }

      public void onSubscribe(io.reactivex.b.b var1) {
         try {
            n.this.b.a(var1);
         } catch (Throwable var3) {
            io.reactivex.exceptions.a.b(var3);
            var1.dispose();
            this.b = io.reactivex.d.a.d.a;
            io.reactivex.d.a.e.a(var3, this.a);
            return;
         }

         if(io.reactivex.d.a.d.a(this.b, var1)) {
            this.b = var1;
            this.a.onSubscribe(this);
         }

      }
   }
}
