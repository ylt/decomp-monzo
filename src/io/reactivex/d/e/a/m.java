package io.reactivex.d.e.a;

import io.reactivex.exceptions.CompositeException;

public final class m extends io.reactivex.b {
   final io.reactivex.d a;
   final io.reactivex.c.q b;

   public m(io.reactivex.d var1, io.reactivex.c.q var2) {
      this.a = var1;
      this.b = var2;
   }

   protected void b(io.reactivex.c var1) {
      this.a.a(new m.a(var1));
   }

   final class a implements io.reactivex.c {
      private final io.reactivex.c b;

      a(io.reactivex.c var2) {
         this.b = var2;
      }

      public void onComplete() {
         this.b.onComplete();
      }

      public void onError(Throwable var1) {
         boolean var2;
         try {
            var2 = m.this.b.a(var1);
         } catch (Throwable var4) {
            io.reactivex.exceptions.a.b(var4);
            this.b.onError(new CompositeException(new Throwable[]{var1, var4}));
            return;
         }

         if(var2) {
            this.b.onComplete();
         } else {
            this.b.onError(var1);
         }

      }

      public void onSubscribe(io.reactivex.b.b var1) {
         this.b.onSubscribe(var1);
      }
   }
}
