package io.reactivex.d.e.a;

import io.reactivex.t;

public final class p extends io.reactivex.n {
   final io.reactivex.d a;

   public p(io.reactivex.d var1) {
      this.a = var1;
   }

   protected void subscribeActual(t var1) {
      this.a.a(new p.a(var1));
   }

   static final class a implements io.reactivex.c {
      private final t a;

      a(t var1) {
         this.a = var1;
      }

      public void onComplete() {
         this.a.onComplete();
      }

      public void onError(Throwable var1) {
         this.a.onError(var1);
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         this.a.onSubscribe(var1);
      }
   }
}
