package io.reactivex.d.e.a;

import java.util.concurrent.Callable;

public final class b extends io.reactivex.b {
   final Callable a;

   public b(Callable var1) {
      this.a = var1;
   }

   protected void b(io.reactivex.c var1) {
      io.reactivex.d var2;
      try {
         var2 = (io.reactivex.d)io.reactivex.d.b.b.a(this.a.call(), "The completableSupplier returned a null CompletableSource");
      } catch (Throwable var3) {
         io.reactivex.exceptions.a.b(var3);
         io.reactivex.d.a.e.a(var3, var1);
         return;
      }

      var2.a(var1);
   }
}
