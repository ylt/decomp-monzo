package io.reactivex.d.e.a;

import io.reactivex.v;
import io.reactivex.x;
import java.util.concurrent.Callable;

public final class q extends v {
   final io.reactivex.d a;
   final Callable b;
   final Object c;

   public q(io.reactivex.d var1, Callable var2, Object var3) {
      this.a = var1;
      this.c = var3;
      this.b = var2;
   }

   protected void b(x var1) {
      this.a.a(new q.a(var1));
   }

   final class a implements io.reactivex.c {
      private final x b;

      a(x var2) {
         this.b = var2;
      }

      public void onComplete() {
         Object var1;
         if(q.this.b != null) {
            try {
               var1 = q.this.b.call();
            } catch (Throwable var2) {
               io.reactivex.exceptions.a.b(var2);
               this.b.onError(var2);
               return;
            }
         } else {
            var1 = q.this.c;
         }

         if(var1 == null) {
            this.b.onError(new NullPointerException("The value supplied is null"));
         } else {
            this.b.a_(var1);
         }

      }

      public void onError(Throwable var1) {
         this.b.onError(var1);
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         this.b.onSubscribe(var1);
      }
   }
}
