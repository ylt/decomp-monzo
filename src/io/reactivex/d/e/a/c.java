package io.reactivex.d.e.a;

import io.reactivex.exceptions.CompositeException;

public final class c extends io.reactivex.b {
   final io.reactivex.d a;
   final io.reactivex.c.g b;

   public c(io.reactivex.d var1, io.reactivex.c.g var2) {
      this.a = var1;
      this.b = var2;
   }

   protected void b(io.reactivex.c var1) {
      this.a.a(new c.a(var1));
   }

   final class a implements io.reactivex.c {
      private final io.reactivex.c b;

      a(io.reactivex.c var2) {
         this.b = var2;
      }

      public void onComplete() {
         try {
            c.this.b.a((Object)null);
         } catch (Throwable var2) {
            io.reactivex.exceptions.a.b(var2);
            this.b.onError(var2);
            return;
         }

         this.b.onComplete();
      }

      public void onError(Throwable var1) {
         try {
            c.this.b.a(var1);
         } catch (Throwable var3) {
            io.reactivex.exceptions.a.b(var3);
            var1 = new CompositeException(new Throwable[]{(Throwable)var1, var3});
         }

         this.b.onError((Throwable)var1);
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         this.b.onSubscribe(var1);
      }
   }
}
