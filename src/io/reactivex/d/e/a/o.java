package io.reactivex.d.e.a;

import io.reactivex.u;
import java.util.concurrent.atomic.AtomicReference;

public final class o extends io.reactivex.b {
   final io.reactivex.d a;
   final u b;

   public o(io.reactivex.d var1, u var2) {
      this.a = var1;
      this.b = var2;
   }

   protected void b(io.reactivex.c var1) {
      o.a var2 = new o.a(var1, this.a);
      var1.onSubscribe(var2);
      io.reactivex.b.b var3 = this.b.a((Runnable)var2);
      var2.b.b(var3);
   }

   static final class a extends AtomicReference implements io.reactivex.b.b, io.reactivex.c, Runnable {
      final io.reactivex.c a;
      final io.reactivex.d.a.k b;
      final io.reactivex.d c;

      a(io.reactivex.c var1, io.reactivex.d var2) {
         this.a = var1;
         this.c = var2;
         this.b = new io.reactivex.d.a.k();
      }

      public void dispose() {
         io.reactivex.d.a.d.a((AtomicReference)this);
         this.b.dispose();
      }

      public boolean isDisposed() {
         return io.reactivex.d.a.d.a((io.reactivex.b.b)this.get());
      }

      public void onComplete() {
         this.a.onComplete();
      }

      public void onError(Throwable var1) {
         this.a.onError(var1);
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         io.reactivex.d.a.d.b(this, var1);
      }

      public void run() {
         this.c.a(this);
      }
   }
}
