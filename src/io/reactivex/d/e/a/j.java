package io.reactivex.d.e.a;

import java.util.concurrent.atomic.AtomicInteger;

public final class j extends io.reactivex.b {
   final io.reactivex.d[] a;

   public void b(io.reactivex.c var1) {
      io.reactivex.b.a var8 = new io.reactivex.b.a();
      AtomicInteger var4 = new AtomicInteger(this.a.length + 1);
      io.reactivex.d.j.c var6 = new io.reactivex.d.j.c();
      var1.onSubscribe(var8);
      io.reactivex.d[] var5 = this.a;
      int var3 = var5.length;
      int var2 = 0;

      while(true) {
         if(var2 >= var3) {
            if(var4.decrementAndGet() == 0) {
               Throwable var9 = var6.a();
               if(var9 == null) {
                  var1.onComplete();
               } else {
                  var1.onError(var9);
               }
            }
            break;
         }

         io.reactivex.d var7 = var5[var2];
         if(var8.isDisposed()) {
            break;
         }

         if(var7 == null) {
            var6.a(new NullPointerException("A completable source is null"));
            var4.decrementAndGet();
         } else {
            var7.a(new j.a(var1, var8, var6, var4));
         }

         ++var2;
      }

   }

   static final class a implements io.reactivex.c {
      final io.reactivex.c a;
      final io.reactivex.b.a b;
      final io.reactivex.d.j.c c;
      final AtomicInteger d;

      a(io.reactivex.c var1, io.reactivex.b.a var2, io.reactivex.d.j.c var3, AtomicInteger var4) {
         this.a = var1;
         this.b = var2;
         this.c = var3;
         this.d = var4;
      }

      void a() {
         if(this.d.decrementAndGet() == 0) {
            Throwable var1 = this.c.a();
            if(var1 == null) {
               this.a.onComplete();
            } else {
               this.a.onError(var1);
            }
         }

      }

      public void onComplete() {
         this.a();
      }

      public void onError(Throwable var1) {
         if(this.c.a(var1)) {
            this.a();
         } else {
            io.reactivex.g.a.a(var1);
         }

      }

      public void onSubscribe(io.reactivex.b.b var1) {
         this.b.a(var1);
      }
   }
}
