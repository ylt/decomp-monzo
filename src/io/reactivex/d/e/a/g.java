package io.reactivex.d.e.a;

import io.reactivex.x;
import io.reactivex.z;

public final class g extends io.reactivex.b {
   final z a;

   public g(z var1) {
      this.a = var1;
   }

   protected void b(io.reactivex.c var1) {
      this.a.a(new g.a(var1));
   }

   static final class a implements x {
      final io.reactivex.c a;

      a(io.reactivex.c var1) {
         this.a = var1;
      }

      public void a_(Object var1) {
         this.a.onComplete();
      }

      public void onError(Throwable var1) {
         this.a.onError(var1);
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         this.a.onSubscribe(var1);
      }
   }
}
