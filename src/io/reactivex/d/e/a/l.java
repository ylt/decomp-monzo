package io.reactivex.d.e.a;

import io.reactivex.u;
import java.util.concurrent.atomic.AtomicReference;

public final class l extends io.reactivex.b {
   final io.reactivex.d a;
   final u b;

   public l(io.reactivex.d var1, u var2) {
      this.a = var1;
      this.b = var2;
   }

   protected void b(io.reactivex.c var1) {
      this.a.a(new l.a(var1, this.b));
   }

   static final class a extends AtomicReference implements io.reactivex.b.b, io.reactivex.c, Runnable {
      final io.reactivex.c a;
      final u b;
      Throwable c;

      a(io.reactivex.c var1, u var2) {
         this.a = var1;
         this.b = var2;
      }

      public void dispose() {
         io.reactivex.d.a.d.a((AtomicReference)this);
      }

      public boolean isDisposed() {
         return io.reactivex.d.a.d.a((io.reactivex.b.b)this.get());
      }

      public void onComplete() {
         io.reactivex.d.a.d.c(this, this.b.a((Runnable)this));
      }

      public void onError(Throwable var1) {
         this.c = var1;
         io.reactivex.d.a.d.c(this, this.b.a((Runnable)this));
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.b(this, var1)) {
            this.a.onSubscribe(this);
         }

      }

      public void run() {
         Throwable var1 = this.c;
         if(var1 != null) {
            this.c = null;
            this.a.onError(var1);
         } else {
            this.a.onComplete();
         }

      }
   }
}
