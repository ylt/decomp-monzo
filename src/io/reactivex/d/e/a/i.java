package io.reactivex.d.e.a;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public final class i extends io.reactivex.b {
   final io.reactivex.d[] a;

   public i(io.reactivex.d[] var1) {
      this.a = var1;
   }

   public void b(io.reactivex.c var1) {
      io.reactivex.b.a var5 = new io.reactivex.b.a();
      i.a var4 = new i.a(var1, new AtomicBoolean(), var5, this.a.length + 1);
      var1.onSubscribe(var5);
      io.reactivex.d[] var7 = this.a;
      int var3 = var7.length;
      int var2 = 0;

      while(true) {
         if(var2 >= var3) {
            var4.onComplete();
            break;
         }

         io.reactivex.d var6 = var7[var2];
         if(var5.isDisposed()) {
            break;
         }

         if(var6 == null) {
            var5.dispose();
            var4.onError(new NullPointerException("A completable source is null"));
            break;
         }

         var6.a(var4);
         ++var2;
      }

   }

   static final class a extends AtomicInteger implements io.reactivex.c {
      final io.reactivex.c a;
      final AtomicBoolean b;
      final io.reactivex.b.a c;

      a(io.reactivex.c var1, AtomicBoolean var2, io.reactivex.b.a var3, int var4) {
         this.a = var1;
         this.b = var2;
         this.c = var3;
         this.lazySet(var4);
      }

      public void onComplete() {
         if(this.decrementAndGet() == 0 && this.b.compareAndSet(false, true)) {
            this.a.onComplete();
         }

      }

      public void onError(Throwable var1) {
         this.c.dispose();
         if(this.b.compareAndSet(false, true)) {
            this.a.onError(var1);
         } else {
            io.reactivex.g.a.a(var1);
         }

      }

      public void onSubscribe(io.reactivex.b.b var1) {
         this.c.a(var1);
      }
   }
}
