package io.reactivex.d.e.d;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public final class at extends a {
   final io.reactivex.c.h b;
   final boolean c;
   final int d;
   final int e;

   public at(io.reactivex.r var1, io.reactivex.c.h var2, boolean var3, int var4, int var5) {
      super(var1);
      this.b = var2;
      this.c = var3;
      this.d = var4;
      this.e = var5;
   }

   public void subscribeActual(io.reactivex.t var1) {
      if(!cr.a(this.a, var1, this.b)) {
         this.a.subscribe(new at.b(var1, this.b, this.c, this.d, this.e));
      }

   }

   static final class a extends AtomicReference implements io.reactivex.t {
      final long a;
      final at.b b;
      volatile boolean c;
      volatile io.reactivex.d.c.i d;
      int e;

      a(at.b var1, long var2) {
         this.a = var2;
         this.b = var1;
      }

      public void a() {
         io.reactivex.d.a.d.a((AtomicReference)this);
      }

      public void onComplete() {
         this.c = true;
         this.b.a();
      }

      public void onError(Throwable var1) {
         if(this.b.h.a(var1)) {
            if(!this.b.c) {
               this.b.d();
            }

            this.c = true;
            this.b.a();
         } else {
            io.reactivex.g.a.a(var1);
         }

      }

      public void onNext(Object var1) {
         if(this.e == 0) {
            this.b.a(var1, this);
         } else {
            this.b.a();
         }

      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.b(this, var1) && var1 instanceof io.reactivex.d.c.d) {
            io.reactivex.d.c.d var3 = (io.reactivex.d.c.d)var1;
            int var2 = var3.a(7);
            if(var2 == 1) {
               this.e = var2;
               this.d = var3;
               this.c = true;
               this.b.a();
            } else if(var2 == 2) {
               this.e = var2;
               this.d = var3;
            }
         }

      }
   }

   static final class b extends AtomicInteger implements io.reactivex.b.b, io.reactivex.t {
      static final at.a[] k = new at.a[0];
      static final at.a[] l = new at.a[0];
      final io.reactivex.t a;
      final io.reactivex.c.h b;
      final boolean c;
      final int d;
      final int e;
      volatile io.reactivex.d.c.h f;
      volatile boolean g;
      final io.reactivex.d.j.c h = new io.reactivex.d.j.c();
      volatile boolean i;
      final AtomicReference j;
      io.reactivex.b.b m;
      long n;
      long o;
      int p;
      Queue q;
      int r;

      b(io.reactivex.t var1, io.reactivex.c.h var2, boolean var3, int var4, int var5) {
         this.a = var1;
         this.b = var2;
         this.c = var3;
         this.d = var4;
         this.e = var5;
         if(var4 != Integer.MAX_VALUE) {
            this.q = new ArrayDeque(var4);
         }

         this.j = new AtomicReference(k);
      }

      void a() {
         if(this.getAndIncrement() == 0) {
            this.b();
         }

      }

      void a(io.reactivex.r param1) {
         // $FF: Couldn't be decompiled
      }

      void a(Object var1, at.a var2) {
         if(this.get() == 0 && this.compareAndSet(0, 1)) {
            this.a.onNext(var1);
            if(this.decrementAndGet() == 0) {
               return;
            }
         } else {
            io.reactivex.d.c.i var4 = var2.d;
            Object var3 = var4;
            if(var4 == null) {
               var3 = new io.reactivex.d.f.c(this.e);
               var2.d = (io.reactivex.d.c.i)var3;
            }

            ((io.reactivex.d.c.i)var3).a(var1);
            if(this.getAndIncrement() != 0) {
               return;
            }
         }

         this.b();
      }

      void a(Callable var1) {
         Object var3;
         try {
            var3 = var1.call();
         } catch (Throwable var4) {
            io.reactivex.exceptions.a.b(var4);
            this.h.a(var4);
            this.a();
            return;
         }

         if(var3 != null) {
            if(this.get() == 0 && this.compareAndSet(0, 1)) {
               this.a.onNext(var3);
               if(this.decrementAndGet() == 0) {
                  return;
               }
            } else {
               io.reactivex.d.c.h var2 = this.f;
               Object var5 = var2;
               if(var2 == null) {
                  if(this.d == Integer.MAX_VALUE) {
                     var5 = new io.reactivex.d.f.c(this.e);
                  } else {
                     var5 = new io.reactivex.d.f.b(this.d);
                  }

                  this.f = (io.reactivex.d.c.h)var5;
               }

               if(!((io.reactivex.d.c.h)var5).a(var3)) {
                  this.onError(new IllegalStateException("Scalar queue full?!"));
                  return;
               }

               if(this.getAndIncrement() != 0) {
                  return;
               }
            }

            this.b();
         }

      }

      boolean a(at.a var1) {
         while(true) {
            at.a[] var5 = (at.a[])this.j.get();
            boolean var3;
            if(var5 == l) {
               var1.a();
               var3 = false;
            } else {
               int var2 = var5.length;
               at.a[] var4 = new at.a[var2 + 1];
               System.arraycopy(var5, 0, var4, 0, var2);
               var4[var2] = var1;
               if(!this.j.compareAndSet(var5, var4)) {
                  continue;
               }

               var3 = true;
            }

            return var3;
         }
      }

      void b() {
         // $FF: Couldn't be decompiled
      }

      void b(at.a var1) {
         while(true) {
            at.a[] var7 = (at.a[])this.j.get();
            int var5 = var7.length;
            if(var5 != 0) {
               byte var4 = -1;
               int var2 = 0;

               int var3;
               while(true) {
                  var3 = var4;
                  if(var2 >= var5) {
                     break;
                  }

                  if(var7[var2] == var1) {
                     var3 = var2;
                     break;
                  }

                  ++var2;
               }

               if(var3 >= 0) {
                  at.a[] var6;
                  if(var5 == 1) {
                     var6 = k;
                  } else {
                     var6 = new at.a[var5 - 1];
                     System.arraycopy(var7, 0, var6, 0, var3);
                     System.arraycopy(var7, var3 + 1, var6, var3, var5 - var3 - 1);
                  }

                  if(!this.j.compareAndSet(var7, var6)) {
                     continue;
                  }
               }
            }

            return;
         }
      }

      boolean c() {
         boolean var1;
         if(this.i) {
            var1 = true;
         } else {
            Throwable var2 = (Throwable)this.h.get();
            if(!this.c && var2 != null) {
               this.d();
               var2 = this.h.a();
               if(var2 != io.reactivex.d.j.j.a) {
                  this.a.onError(var2);
               }

               var1 = true;
            } else {
               var1 = false;
            }
         }

         return var1;
      }

      boolean d() {
         int var1 = 0;
         this.m.dispose();
         boolean var3;
         if((at.a[])this.j.get() != l) {
            at.a[] var4 = (at.a[])this.j.getAndSet(l);
            if(var4 != l) {
               for(int var2 = var4.length; var1 < var2; ++var1) {
                  var4[var1].a();
               }

               var3 = true;
               return var3;
            }
         }

         var3 = false;
         return var3;
      }

      public void dispose() {
         if(!this.i) {
            this.i = true;
            if(this.d()) {
               Throwable var1 = this.h.a();
               if(var1 != null && var1 != io.reactivex.d.j.j.a) {
                  io.reactivex.g.a.a(var1);
               }
            }
         }

      }

      public boolean isDisposed() {
         return this.i;
      }

      public void onComplete() {
         if(!this.g) {
            this.g = true;
            this.a();
         }

      }

      public void onError(Throwable var1) {
         if(this.g) {
            io.reactivex.g.a.a(var1);
         } else if(this.h.a(var1)) {
            this.g = true;
            this.a();
         } else {
            io.reactivex.g.a.a(var1);
         }

      }

      public void onNext(Object param1) {
         // $FF: Couldn't be decompiled
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.m, var1)) {
            this.m = var1;
            this.a.onSubscribe(this);
         }

      }
   }
}
