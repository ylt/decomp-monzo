package io.reactivex.d.e.d;

import io.reactivex.exceptions.CompositeException;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicBoolean;

public final class dw extends io.reactivex.n {
   final Callable a;
   final io.reactivex.c.h b;
   final io.reactivex.c.g c;
   final boolean d;

   public dw(Callable var1, io.reactivex.c.h var2, io.reactivex.c.g var3, boolean var4) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
      this.d = var4;
   }

   public void subscribeActual(io.reactivex.t var1) {
      Object var3;
      try {
         var3 = this.a.call();
      } catch (Throwable var6) {
         io.reactivex.exceptions.a.b(var6);
         io.reactivex.d.a.e.a(var6, var1);
         return;
      }

      io.reactivex.r var2;
      try {
         var2 = (io.reactivex.r)io.reactivex.d.b.b.a(this.b.a(var3), "The sourceSupplier returned a null ObservableSource");
      } catch (Throwable var5) {
         io.reactivex.exceptions.a.b(var5);

         try {
            this.c.a(var3);
         } catch (Throwable var4) {
            io.reactivex.exceptions.a.b(var4);
            io.reactivex.d.a.e.a(new CompositeException(new Throwable[]{var5, var4}), (io.reactivex.t)var1);
            return;
         }

         io.reactivex.d.a.e.a(var5, var1);
         return;
      }

      var2.subscribe(new dw.a(var1, var3, this.c, this.d));
   }

   static final class a extends AtomicBoolean implements io.reactivex.b.b, io.reactivex.t {
      final io.reactivex.t a;
      final Object b;
      final io.reactivex.c.g c;
      final boolean d;
      io.reactivex.b.b e;

      a(io.reactivex.t var1, Object var2, io.reactivex.c.g var3, boolean var4) {
         this.a = var1;
         this.b = var2;
         this.c = var3;
         this.d = var4;
      }

      void a() {
         if(this.compareAndSet(false, true)) {
            try {
               this.c.a(this.b);
            } catch (Throwable var2) {
               io.reactivex.exceptions.a.b(var2);
               io.reactivex.g.a.a(var2);
            }
         }

      }

      public void dispose() {
         this.a();
         this.e.dispose();
      }

      public boolean isDisposed() {
         return this.get();
      }

      public void onComplete() {
         if(this.d) {
            if(this.compareAndSet(false, true)) {
               try {
                  this.c.a(this.b);
               } catch (Throwable var2) {
                  io.reactivex.exceptions.a.b(var2);
                  this.a.onError(var2);
                  return;
               }
            }

            this.e.dispose();
            this.a.onComplete();
         } else {
            this.a.onComplete();
            this.e.dispose();
            this.a();
         }

      }

      public void onError(Throwable var1) {
         if(this.d) {
            Object var2 = var1;
            if(this.compareAndSet(false, true)) {
               label23: {
                  try {
                     this.c.a(this.b);
                  } catch (Throwable var3) {
                     io.reactivex.exceptions.a.b(var3);
                     var2 = new CompositeException(new Throwable[]{var1, var3});
                     break label23;
                  }

                  var2 = var1;
               }
            }

            this.e.dispose();
            this.a.onError((Throwable)var2);
         } else {
            this.a.onError(var1);
            this.e.dispose();
            this.a();
         }

      }

      public void onNext(Object var1) {
         this.a.onNext(var1);
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.e, var1)) {
            this.e = var1;
            this.a.onSubscribe(this);
         }

      }
   }
}
