package io.reactivex.d.e.d;

import java.util.Arrays;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public final class ee extends io.reactivex.n {
   final io.reactivex.r[] a;
   final Iterable b;
   final io.reactivex.c.h c;
   final int d;
   final boolean e;

   public ee(io.reactivex.r[] var1, Iterable var2, io.reactivex.c.h var3, int var4, boolean var5) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
      this.d = var4;
      this.e = var5;
   }

   public void subscribeActual(io.reactivex.t var1) {
      Object var5 = this.a;
      int var3;
      if(var5 == null) {
         Object var4 = new io.reactivex.n[8];
         Iterator var6 = this.b.iterator();
         int var2 = 0;

         while(true) {
            var5 = var4;
            var3 = var2;
            if(!var6.hasNext()) {
               break;
            }

            io.reactivex.r var7 = (io.reactivex.r)var6.next();
            if(var2 == ((Object[])var4).length) {
               io.reactivex.r[] var8 = new io.reactivex.r[(var2 >> 2) + var2];
               System.arraycopy(var4, 0, var8, 0, var2);
               var4 = var8;
            }

            ((Object[])var4)[var2] = var7;
            ++var2;
         }
      } else {
         var3 = ((Object[])var5).length;
      }

      if(var3 == 0) {
         io.reactivex.d.a.e.a(var1);
      } else {
         (new ee.a(var1, this.c, var3, this.e)).a((io.reactivex.r[])var5, this.d);
      }

   }

   static final class a extends AtomicInteger implements io.reactivex.b.b {
      final io.reactivex.t a;
      final io.reactivex.c.h b;
      final ee.b[] c;
      final Object[] d;
      final boolean e;
      volatile boolean f;

      a(io.reactivex.t var1, io.reactivex.c.h var2, int var3, boolean var4) {
         this.a = var1;
         this.b = var2;
         this.c = new ee.b[var3];
         this.d = (Object[])(new Object[var3]);
         this.e = var4;
      }

      void a() {
         this.c();
         this.b();
      }

      public void a(io.reactivex.r[] var1, int var2) {
         byte var4 = 0;
         ee.b[] var6 = this.c;
         int var5 = var6.length;

         for(int var3 = 0; var3 < var5; ++var3) {
            var6[var3] = new ee.b(this, var2);
         }

         this.lazySet(0);
         this.a.onSubscribe(this);

         for(var2 = var4; var2 < var5 && !this.f; ++var2) {
            var1[var2].subscribe(var6[var2]);
         }

      }

      boolean a(boolean var1, boolean var2, io.reactivex.t var3, boolean var4, ee.b var5) {
         boolean var6 = true;
         if(this.f) {
            this.a();
            var1 = var6;
         } else {
            if(var1) {
               Throwable var7;
               if(var4) {
                  if(var2) {
                     var7 = var5.d;
                     this.a();
                     if(var7 != null) {
                        var3.onError(var7);
                        var1 = var6;
                     } else {
                        var3.onComplete();
                        var1 = var6;
                     }

                     return var1;
                  }
               } else {
                  var7 = var5.d;
                  if(var7 != null) {
                     this.a();
                     var3.onError(var7);
                     var1 = var6;
                     return var1;
                  }

                  if(var2) {
                     this.a();
                     var3.onComplete();
                     var1 = var6;
                     return var1;
                  }
               }
            }

            var1 = false;
         }

         return var1;
      }

      void b() {
         ee.b[] var3 = this.c;
         int var2 = var3.length;

         for(int var1 = 0; var1 < var2; ++var1) {
            var3[var1].a();
         }

      }

      void c() {
         ee.b[] var3 = this.c;
         int var2 = var3.length;

         for(int var1 = 0; var1 < var2; ++var1) {
            var3[var1].b.c();
         }

      }

      public void d() {
         if(this.getAndIncrement() == 0) {
            ee.b[] var12 = this.c;
            io.reactivex.t var10 = this.a;
            Object[] var11 = this.d;
            boolean var9 = this.e;
            int var2 = 1;

            while(true) {
               int var1 = 0;
               int var6 = var12.length;
               int var4 = 0;

               int var5;
               for(int var3 = 0; var4 < var6; var1 = var5) {
                  ee.b var13 = var12[var4];
                  if(var11[var3] == null) {
                     boolean var8 = var13.c;
                     Object var14 = var13.b.n_();
                     boolean var7;
                     if(var14 == null) {
                        var7 = true;
                     } else {
                        var7 = false;
                     }

                     if(this.a(var8, var7, var10, var9, var13)) {
                        return;
                     }

                     if(!var7) {
                        var11[var3] = var14;
                     } else {
                        ++var1;
                     }

                     var5 = var1;
                  } else {
                     var5 = var1;
                     if(var13.c) {
                        var5 = var1;
                        if(!var9) {
                           Throwable var16 = var13.d;
                           var5 = var1;
                           if(var16 != null) {
                              this.a();
                              var10.onError(var16);
                              return;
                           }
                        }
                     }
                  }

                  ++var4;
                  ++var3;
               }

               if(var1 != 0) {
                  var2 = this.addAndGet(-var2);
                  if(var2 == 0) {
                     break;
                  }
               } else {
                  Object var17;
                  try {
                     var17 = io.reactivex.d.b.b.a(this.b.a(var11.clone()), "The zipper returned a null value");
                  } catch (Throwable var15) {
                     io.reactivex.exceptions.a.b(var15);
                     this.a();
                     var10.onError(var15);
                     break;
                  }

                  var10.onNext(var17);
                  Arrays.fill(var11, (Object)null);
               }
            }
         }

      }

      public void dispose() {
         if(!this.f) {
            this.f = true;
            this.b();
            if(this.getAndIncrement() == 0) {
               this.c();
            }
         }

      }

      public boolean isDisposed() {
         return this.f;
      }
   }

   static final class b implements io.reactivex.t {
      final ee.a a;
      final io.reactivex.d.f.c b;
      volatile boolean c;
      Throwable d;
      final AtomicReference e = new AtomicReference();

      b(ee.a var1, int var2) {
         this.a = var1;
         this.b = new io.reactivex.d.f.c(var2);
      }

      public void a() {
         io.reactivex.d.a.d.a(this.e);
      }

      public void onComplete() {
         this.c = true;
         this.a.d();
      }

      public void onError(Throwable var1) {
         this.d = var1;
         this.c = true;
         this.a.d();
      }

      public void onNext(Object var1) {
         this.b.a(var1);
         this.a.d();
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         io.reactivex.d.a.d.b(this.e, var1);
      }
   }
}
