package io.reactivex.d.e.d;

public final class aj extends a {
   final io.reactivex.c.g b;

   public aj(io.reactivex.r var1, io.reactivex.c.g var2) {
      super(var1);
      this.b = var2;
   }

   protected void subscribeActual(io.reactivex.t var1) {
      this.a.subscribe(new aj.a(var1, this.b));
   }

   static final class a extends io.reactivex.d.d.a {
      final io.reactivex.c.g f;

      a(io.reactivex.t var1, io.reactivex.c.g var2) {
         super(var1);
         this.f = var2;
      }

      public int a(int var1) {
         return this.b(var1);
      }

      public Object n_() throws Exception {
         Object var1 = this.c.n_();
         if(var1 != null) {
            this.f.a(var1);
         }

         return var1;
      }

      public void onNext(Object var1) {
         this.a.onNext(var1);
         if(this.e == 0) {
            try {
               this.f.a(var1);
            } catch (Throwable var2) {
               this.a(var2);
            }
         }

      }
   }
}
