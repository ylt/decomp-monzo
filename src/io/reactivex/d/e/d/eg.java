package io.reactivex.d.e.d;

import java.util.concurrent.atomic.AtomicReference;

public final class eg extends AtomicReference implements io.reactivex.b.b, io.reactivex.t {
   final io.reactivex.t a;
   final AtomicReference b = new AtomicReference();

   public eg(io.reactivex.t var1) {
      this.a = var1;
   }

   public void a(io.reactivex.b.b var1) {
      io.reactivex.d.a.d.a((AtomicReference)this, var1);
   }

   public void dispose() {
      io.reactivex.d.a.d.a(this.b);
      io.reactivex.d.a.d.a((AtomicReference)this);
   }

   public boolean isDisposed() {
      boolean var1;
      if(this.b.get() == io.reactivex.d.a.d.a) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public void onComplete() {
      this.dispose();
      this.a.onComplete();
   }

   public void onError(Throwable var1) {
      this.dispose();
      this.a.onError(var1);
   }

   public void onNext(Object var1) {
      this.a.onNext(var1);
   }

   public void onSubscribe(io.reactivex.b.b var1) {
      if(io.reactivex.d.a.d.b(this.b, var1)) {
         this.a.onSubscribe(this);
      }

   }
}
