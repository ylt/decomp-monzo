package io.reactivex.d.e.d;

import io.reactivex.exceptions.CompositeException;
import java.util.concurrent.atomic.AtomicInteger;

public final class cm extends a {
   final io.reactivex.c.d b;

   public cm(io.reactivex.n var1, io.reactivex.c.d var2) {
      super(var1);
      this.b = var2;
   }

   public void subscribeActual(io.reactivex.t var1) {
      io.reactivex.d.a.k var2 = new io.reactivex.d.a.k();
      var1.onSubscribe(var2);
      (new cm.a(var1, this.b, var2, this.a)).a();
   }

   static final class a extends AtomicInteger implements io.reactivex.t {
      final io.reactivex.t a;
      final io.reactivex.d.a.k b;
      final io.reactivex.r c;
      final io.reactivex.c.d d;
      int e;

      a(io.reactivex.t var1, io.reactivex.c.d var2, io.reactivex.d.a.k var3, io.reactivex.r var4) {
         this.a = var1;
         this.b = var3;
         this.c = var4;
         this.d = var2;
      }

      void a() {
         if(this.getAndIncrement() == 0) {
            int var1 = 1;

            while(!this.b.isDisposed()) {
               this.c.subscribe(this);
               int var2 = this.addAndGet(-var1);
               var1 = var2;
               if(var2 == 0) {
                  break;
               }
            }
         }

      }

      public void onComplete() {
         this.a.onComplete();
      }

      public void onError(Throwable var1) {
         boolean var3;
         try {
            io.reactivex.c.d var4 = this.d;
            int var2 = this.e + 1;
            this.e = var2;
            var3 = var4.a(Integer.valueOf(var2), var1);
         } catch (Throwable var5) {
            io.reactivex.exceptions.a.b(var5);
            this.a.onError(new CompositeException(new Throwable[]{var1, var5}));
            return;
         }

         if(!var3) {
            this.a.onError(var1);
         } else {
            this.a();
         }

      }

      public void onNext(Object var1) {
         this.a.onNext(var1);
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         this.b.a(var1);
      }
   }
}
