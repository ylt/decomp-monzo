package io.reactivex.d.e.d;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicInteger;

public final class n extends a {
   final Callable b;
   final io.reactivex.r c;
   final io.reactivex.c.h d;

   public n(io.reactivex.r var1, io.reactivex.r var2, io.reactivex.c.h var3, Callable var4) {
      super(var1);
      this.c = var2;
      this.d = var3;
      this.b = var4;
   }

   protected void subscribeActual(io.reactivex.t var1) {
      this.a.subscribe(new n.a(new io.reactivex.f.e(var1), this.c, this.d, this.b));
   }

   static final class a extends io.reactivex.d.d.t implements io.reactivex.b.b {
      final io.reactivex.r g;
      final io.reactivex.c.h h;
      final Callable i;
      final io.reactivex.b.a j;
      io.reactivex.b.b k;
      final List l;
      final AtomicInteger m = new AtomicInteger();

      a(io.reactivex.t var1, io.reactivex.r var2, io.reactivex.c.h var3, Callable var4) {
         super(var1, new io.reactivex.d.f.a());
         this.g = var2;
         this.h = var3;
         this.i = var4;
         this.l = new LinkedList();
         this.j = new io.reactivex.b.a();
      }

      void a(io.reactivex.b.b var1) {
         if(this.j.b(var1) && this.m.decrementAndGet() == 0) {
            this.f();
         }

      }

      public void a(io.reactivex.t var1, Collection var2) {
         var1.onNext(var2);
      }

      void a(Object param1) {
         // $FF: Couldn't be decompiled
      }

      void a(Collection param1, io.reactivex.b.b param2) {
         // $FF: Couldn't be decompiled
      }

      public void dispose() {
         if(!this.c) {
            this.c = true;
            this.j.dispose();
         }

      }

      void f() {
         // $FF: Couldn't be decompiled
      }

      public boolean isDisposed() {
         return this.c;
      }

      public void onComplete() {
         if(this.m.decrementAndGet() == 0) {
            this.f();
         }

      }

      public void onError(Throwable param1) {
         // $FF: Couldn't be decompiled
      }

      public void onNext(Object param1) {
         // $FF: Couldn't be decompiled
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.k, var1)) {
            this.k = var1;
            n.c var2 = new n.c(this);
            this.j.a((io.reactivex.b.b)var2);
            this.a.onSubscribe(this);
            this.m.lazySet(1);
            this.g.subscribe(var2);
         }

      }
   }

   static final class b extends io.reactivex.f.c {
      final n.a a;
      final Collection b;
      boolean c;

      b(Collection var1, n.a var2) {
         this.a = var2;
         this.b = var1;
      }

      public void onComplete() {
         if(!this.c) {
            this.c = true;
            this.a.a((Collection)this.b, (io.reactivex.b.b)this);
         }

      }

      public void onError(Throwable var1) {
         if(this.c) {
            io.reactivex.g.a.a(var1);
         } else {
            this.a.onError(var1);
         }

      }

      public void onNext(Object var1) {
         this.onComplete();
      }
   }

   static final class c extends io.reactivex.f.c {
      final n.a a;
      boolean b;

      c(n.a var1) {
         this.a = var1;
      }

      public void onComplete() {
         if(!this.b) {
            this.b = true;
            this.a.a((io.reactivex.b.b)this);
         }

      }

      public void onError(Throwable var1) {
         if(this.b) {
            io.reactivex.g.a.a(var1);
         } else {
            this.b = true;
            this.a.onError(var1);
         }

      }

      public void onNext(Object var1) {
         if(!this.b) {
            this.a.a(var1);
         }

      }
   }
}
