package io.reactivex.d.e.d;

public final class dj extends a {
   public dj(io.reactivex.r var1) {
      super(var1);
   }

   public void subscribeActual(io.reactivex.t var1) {
      this.a.subscribe(new dj.a(var1));
   }

   static final class a implements io.reactivex.b.b, io.reactivex.t {
      final io.reactivex.t a;
      io.reactivex.b.b b;
      Object c;

      a(io.reactivex.t var1) {
         this.a = var1;
      }

      void a() {
         Object var1 = this.c;
         if(var1 != null) {
            this.c = null;
            this.a.onNext(var1);
         }

         this.a.onComplete();
      }

      public void dispose() {
         this.c = null;
         this.b.dispose();
      }

      public boolean isDisposed() {
         return this.b.isDisposed();
      }

      public void onComplete() {
         this.a();
      }

      public void onError(Throwable var1) {
         this.c = null;
         this.a.onError(var1);
      }

      public void onNext(Object var1) {
         this.c = var1;
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.b, var1)) {
            this.b = var1;
            this.a.onSubscribe(this);
         }

      }
   }
}
