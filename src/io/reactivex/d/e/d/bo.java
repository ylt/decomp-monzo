package io.reactivex.d.e.d;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public final class bo extends a {
   final io.reactivex.r b;
   final io.reactivex.c.h c;
   final io.reactivex.c.h d;
   final io.reactivex.c.c e;

   public bo(io.reactivex.r var1, io.reactivex.r var2, io.reactivex.c.h var3, io.reactivex.c.h var4, io.reactivex.c.c var5) {
      super(var1);
      this.b = var2;
      this.c = var3;
      this.d = var4;
      this.e = var5;
   }

   protected void subscribeActual(io.reactivex.t var1) {
      bo.a var2 = new bo.a(var1, this.c, this.d, this.e);
      var1.onSubscribe(var2);
      bh.d var4 = new bh.d(var2, true);
      var2.c.a((io.reactivex.b.b)var4);
      bh.d var3 = new bh.d(var2, false);
      var2.c.a((io.reactivex.b.b)var3);
      this.a.subscribe(var4);
      this.b.subscribe(var3);
   }

   static final class a extends AtomicInteger implements io.reactivex.b.b, bh.b {
      static final Integer n = Integer.valueOf(1);
      static final Integer o = Integer.valueOf(2);
      static final Integer p = Integer.valueOf(3);
      static final Integer q = Integer.valueOf(4);
      final io.reactivex.t a;
      final io.reactivex.d.f.c b;
      final io.reactivex.b.a c;
      final Map d;
      final Map e;
      final AtomicReference f;
      final io.reactivex.c.h g;
      final io.reactivex.c.h h;
      final io.reactivex.c.c i;
      final AtomicInteger j;
      int k;
      int l;
      volatile boolean m;

      a(io.reactivex.t var1, io.reactivex.c.h var2, io.reactivex.c.h var3, io.reactivex.c.c var4) {
         this.a = var1;
         this.c = new io.reactivex.b.a();
         this.b = new io.reactivex.d.f.c(io.reactivex.n.bufferSize());
         this.d = new LinkedHashMap();
         this.e = new LinkedHashMap();
         this.f = new AtomicReference();
         this.g = var2;
         this.h = var3;
         this.i = var4;
         this.j = new AtomicInteger(2);
      }

      void a() {
         this.c.dispose();
      }

      public void a(bh.d var1) {
         this.c.c(var1);
         this.j.decrementAndGet();
         this.b();
      }

      void a(io.reactivex.t var1) {
         Throwable var2 = io.reactivex.d.j.j.a(this.f);
         this.d.clear();
         this.e.clear();
         var1.onError(var2);
      }

      public void a(Throwable var1) {
         if(io.reactivex.d.j.j.a(this.f, var1)) {
            this.j.decrementAndGet();
            this.b();
         } else {
            io.reactivex.g.a.a(var1);
         }

      }

      void a(Throwable var1, io.reactivex.t var2, io.reactivex.d.f.c var3) {
         io.reactivex.exceptions.a.b(var1);
         io.reactivex.d.j.j.a(this.f, var1);
         var3.c();
         this.a();
         this.a(var2);
      }

      public void a(boolean param1, bh.c param2) {
         // $FF: Couldn't be decompiled
      }

      public void a(boolean param1, Object param2) {
         // $FF: Couldn't be decompiled
      }

      void b() {
         if(this.getAndIncrement() == 0) {
            io.reactivex.d.f.c var4 = this.b;
            io.reactivex.t var5 = this.a;
            int var1 = 1;

            while(true) {
               if(this.m) {
                  var4.c();
                  break;
               }

               if((Throwable)this.f.get() != null) {
                  var4.c();
                  this.a();
                  this.a(var5);
                  break;
               }

               boolean var2;
               if(this.j.get() == 0) {
                  var2 = true;
               } else {
                  var2 = false;
               }

               Integer var7 = (Integer)var4.n_();
               boolean var3;
               if(var7 == null) {
                  var3 = true;
               } else {
                  var3 = false;
               }

               if(var2 && var3) {
                  this.d.clear();
                  this.e.clear();
                  this.c.dispose();
                  var5.onComplete();
                  break;
               }

               if(var3) {
                  var1 = this.addAndGet(-var1);
                  if(var1 == 0) {
                     break;
                  }
               } else {
                  Object var6 = var4.n_();
                  int var13;
                  Iterator var16;
                  Object var18;
                  if(var7 == n) {
                     var13 = this.k;
                     this.k = var13 + 1;
                     this.d.put(Integer.valueOf(var13), var6);

                     io.reactivex.r var19;
                     try {
                        var19 = (io.reactivex.r)io.reactivex.d.b.b.a(this.g.a(var6), "The leftEnd returned a null ObservableSource");
                     } catch (Throwable var12) {
                        this.a(var12, var5, var4);
                        break;
                     }

                     bh.c var17 = new bh.c(this, true, var13);
                     this.c.a((io.reactivex.b.b)var17);
                     var19.subscribe(var17);
                     if((Throwable)this.f.get() != null) {
                        var4.c();
                        this.a();
                        this.a(var5);
                        break;
                     }

                     for(var16 = this.e.values().iterator(); var16.hasNext(); var5.onNext(var18)) {
                        var18 = var16.next();

                        try {
                           var18 = io.reactivex.d.b.b.a(this.i.a(var6, var18), "The resultSelector returned a null value");
                        } catch (Throwable var11) {
                           this.a(var11, var5, var4);
                           return;
                        }
                     }
                  } else if(var7 == o) {
                     var13 = this.l;
                     this.l = var13 + 1;
                     this.e.put(Integer.valueOf(var13), var6);

                     io.reactivex.r var15;
                     try {
                        var15 = (io.reactivex.r)io.reactivex.d.b.b.a(this.h.a(var6), "The rightEnd returned a null ObservableSource");
                     } catch (Throwable var10) {
                        this.a(var10, var5, var4);
                        break;
                     }

                     bh.c var8 = new bh.c(this, false, var13);
                     this.c.a((io.reactivex.b.b)var8);
                     var15.subscribe(var8);
                     if((Throwable)this.f.get() != null) {
                        var4.c();
                        this.a();
                        this.a(var5);
                        break;
                     }

                     for(var16 = this.d.values().iterator(); var16.hasNext(); var5.onNext(var18)) {
                        var18 = var16.next();

                        try {
                           var18 = io.reactivex.d.b.b.a(this.i.a(var18, var6), "The resultSelector returned a null value");
                        } catch (Throwable var9) {
                           this.a(var9, var5, var4);
                           return;
                        }
                     }
                  } else {
                     bh.c var14;
                     if(var7 == p) {
                        var14 = (bh.c)var6;
                        this.d.remove(Integer.valueOf(var14.c));
                        this.c.b(var14);
                     } else {
                        var14 = (bh.c)var6;
                        this.e.remove(Integer.valueOf(var14.c));
                        this.c.b(var14);
                     }
                  }
               }
            }
         }

      }

      public void b(Throwable var1) {
         if(io.reactivex.d.j.j.a(this.f, var1)) {
            this.b();
         } else {
            io.reactivex.g.a.a(var1);
         }

      }

      public void dispose() {
         if(!this.m) {
            this.m = true;
            this.a();
            if(this.getAndIncrement() == 0) {
               this.b.c();
            }
         }

      }

      public boolean isDisposed() {
         return this.m;
      }
   }
}
