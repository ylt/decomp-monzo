package io.reactivex.d.e.d;

import java.util.Collection;
import java.util.concurrent.Callable;

public final class p extends a {
   final io.reactivex.r b;
   final Callable c;

   public p(io.reactivex.r var1, io.reactivex.r var2, Callable var3) {
      super(var1);
      this.b = var2;
      this.c = var3;
   }

   protected void subscribeActual(io.reactivex.t var1) {
      this.a.subscribe(new p.b(new io.reactivex.f.e(var1), this.c, this.b));
   }

   static final class a extends io.reactivex.f.c {
      final p.b a;

      a(p.b var1) {
         this.a = var1;
      }

      public void onComplete() {
         this.a.onComplete();
      }

      public void onError(Throwable var1) {
         this.a.onError(var1);
      }

      public void onNext(Object var1) {
         this.a.f();
      }
   }

   static final class b extends io.reactivex.d.d.t implements io.reactivex.b.b, io.reactivex.t {
      final Callable g;
      final io.reactivex.r h;
      io.reactivex.b.b i;
      io.reactivex.b.b j;
      Collection k;

      b(io.reactivex.t var1, Callable var2, io.reactivex.r var3) {
         super(var1, new io.reactivex.d.f.a());
         this.g = var2;
         this.h = var3;
      }

      public void a(io.reactivex.t var1, Collection var2) {
         this.a.onNext(var2);
      }

      public void dispose() {
         if(!this.c) {
            this.c = true;
            this.j.dispose();
            this.i.dispose();
            if(this.c()) {
               this.b.c();
            }
         }

      }

      void f() {
         // $FF: Couldn't be decompiled
      }

      public boolean isDisposed() {
         return this.c;
      }

      public void onComplete() {
         // $FF: Couldn't be decompiled
      }

      public void onError(Throwable var1) {
         this.dispose();
         this.a.onError(var1);
      }

      public void onNext(Object param1) {
         // $FF: Couldn't be decompiled
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.i, var1)) {
            this.i = var1;

            Collection var2;
            try {
               var2 = (Collection)io.reactivex.d.b.b.a(this.g.call(), "The buffer supplied is null");
            } catch (Throwable var3) {
               io.reactivex.exceptions.a.b(var3);
               this.c = true;
               var1.dispose();
               io.reactivex.d.a.e.a(var3, this.a);
               return;
            }

            this.k = var2;
            p.a var4 = new p.a(this);
            this.j = var4;
            this.a.onSubscribe(this);
            if(!this.c) {
               this.h.subscribe(var4);
            }
         }

      }
   }
}
