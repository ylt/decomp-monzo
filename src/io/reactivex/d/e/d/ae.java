package io.reactivex.d.e.d;

public final class ae extends io.reactivex.n {
   final io.reactivex.r a;
   final io.reactivex.r b;

   public ae(io.reactivex.r var1, io.reactivex.r var2) {
      this.a = var1;
      this.b = var2;
   }

   public void subscribeActual(io.reactivex.t var1) {
      io.reactivex.d.a.k var2 = new io.reactivex.d.a.k();
      var1.onSubscribe(var2);
      ae.a var3 = new ae.a(var2, var1);
      this.b.subscribe(var3);
   }

   final class a implements io.reactivex.t {
      final io.reactivex.d.a.k a;
      final io.reactivex.t b;
      boolean c;

      a(io.reactivex.d.a.k var2, io.reactivex.t var3) {
         this.a = var2;
         this.b = var3;
      }

      public void onComplete() {
         if(!this.c) {
            this.c = true;
            ae.this.a.subscribe(new ae.a());
         }

      }

      public void onError(Throwable var1) {
         if(this.c) {
            io.reactivex.g.a.a(var1);
         } else {
            this.c = true;
            this.b.onError(var1);
         }

      }

      public void onNext(Object var1) {
         this.onComplete();
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         this.a.a(var1);
      }
   }

   final class a implements io.reactivex.t {
      public void onComplete() {
         ae.super.b.onComplete();
      }

      public void onError(Throwable var1) {
         ae.super.b.onError(var1);
      }

      public void onNext(Object var1) {
         ae.super.b.onNext(var1);
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         ae.super.a.a(var1);
      }
   }
}
