package io.reactivex.d.e.d;

import io.reactivex.exceptions.CompositeException;

public final class by extends a {
   final io.reactivex.c.h b;
   final boolean c;

   public by(io.reactivex.r var1, io.reactivex.c.h var2, boolean var3) {
      super(var1);
      this.b = var2;
      this.c = var3;
   }

   public void subscribeActual(io.reactivex.t var1) {
      by.a var2 = new by.a(var1, this.b, this.c);
      var1.onSubscribe(var2.d);
      this.a.subscribe(var2);
   }

   static final class a implements io.reactivex.t {
      final io.reactivex.t a;
      final io.reactivex.c.h b;
      final boolean c;
      final io.reactivex.d.a.k d;
      boolean e;
      boolean f;

      a(io.reactivex.t var1, io.reactivex.c.h var2, boolean var3) {
         this.a = var1;
         this.b = var2;
         this.c = var3;
         this.d = new io.reactivex.d.a.k();
      }

      public void onComplete() {
         if(!this.f) {
            this.f = true;
            this.e = true;
            this.a.onComplete();
         }

      }

      public void onError(Throwable var1) {
         if(this.e) {
            if(this.f) {
               io.reactivex.g.a.a(var1);
            } else {
               this.a.onError(var1);
            }
         } else {
            this.e = true;
            if(this.c && !(var1 instanceof Exception)) {
               this.a.onError(var1);
            } else {
               io.reactivex.r var2;
               try {
                  var2 = (io.reactivex.r)this.b.a(var1);
               } catch (Throwable var3) {
                  io.reactivex.exceptions.a.b(var3);
                  this.a.onError(new CompositeException(new Throwable[]{var1, var3}));
                  return;
               }

               if(var2 == null) {
                  NullPointerException var4 = new NullPointerException("Observable is null");
                  var4.initCause(var1);
                  this.a.onError(var4);
               } else {
                  var2.subscribe(this);
               }
            }
         }

      }

      public void onNext(Object var1) {
         if(!this.f) {
            this.a.onNext(var1);
         }

      }

      public void onSubscribe(io.reactivex.b.b var1) {
         this.d.b(var1);
      }
   }
}
