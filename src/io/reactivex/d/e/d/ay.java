package io.reactivex.d.e.d;

import java.util.Iterator;

public final class ay extends a {
   final io.reactivex.c.h b;

   public ay(io.reactivex.r var1, io.reactivex.c.h var2) {
      super(var1);
      this.b = var2;
   }

   protected void subscribeActual(io.reactivex.t var1) {
      this.a.subscribe(new ay.a(var1, this.b));
   }

   static final class a implements io.reactivex.b.b, io.reactivex.t {
      final io.reactivex.t a;
      final io.reactivex.c.h b;
      io.reactivex.b.b c;

      a(io.reactivex.t var1, io.reactivex.c.h var2) {
         this.a = var1;
         this.b = var2;
      }

      public void dispose() {
         this.c.dispose();
         this.c = io.reactivex.d.a.d.a;
      }

      public boolean isDisposed() {
         return this.c.isDisposed();
      }

      public void onComplete() {
         if(this.c != io.reactivex.d.a.d.a) {
            this.c = io.reactivex.d.a.d.a;
            this.a.onComplete();
         }

      }

      public void onError(Throwable var1) {
         if(this.c == io.reactivex.d.a.d.a) {
            io.reactivex.g.a.a(var1);
         } else {
            this.c = io.reactivex.d.a.d.a;
            this.a.onError(var1);
         }

      }

      public void onNext(Object var1) {
         if(this.c != io.reactivex.d.a.d.a) {
            Iterator var3;
            try {
               var3 = ((Iterable)this.b.a(var1)).iterator();
            } catch (Throwable var7) {
               io.reactivex.exceptions.a.b(var7);
               this.c.dispose();
               this.onError(var7);
               return;
            }

            io.reactivex.t var8 = this.a;

            while(true) {
               boolean var2;
               try {
                  var2 = var3.hasNext();
               } catch (Throwable var6) {
                  io.reactivex.exceptions.a.b(var6);
                  this.c.dispose();
                  this.onError(var6);
                  break;
               }

               if(!var2) {
                  break;
               }

               Object var4;
               try {
                  var4 = io.reactivex.d.b.b.a(var3.next(), "The iterator returned a null value");
               } catch (Throwable var5) {
                  io.reactivex.exceptions.a.b(var5);
                  this.c.dispose();
                  this.onError(var5);
                  break;
               }

               var8.onNext(var4);
            }
         }

      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.c, var1)) {
            this.c = var1;
            this.a.onSubscribe(this);
         }

      }
   }
}
