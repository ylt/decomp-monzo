package io.reactivex.d.e.d;

import java.util.concurrent.LinkedBlockingQueue;

public final class l {
   public static void a(io.reactivex.r var0) {
      io.reactivex.d.j.f var1 = new io.reactivex.d.j.f();
      io.reactivex.d.d.s var2 = new io.reactivex.d.d.s(io.reactivex.d.b.a.b(), var1, var1, io.reactivex.d.b.a.b());
      var0.subscribe(var2);
      io.reactivex.d.j.e.a(var1, var2);
      Throwable var3 = var1.a;
      if(var3 != null) {
         throw io.reactivex.d.j.j.a(var3);
      }
   }

   public static void a(io.reactivex.r var0, io.reactivex.c.g var1, io.reactivex.c.g var2, io.reactivex.c.a var3) {
      io.reactivex.d.b.b.a(var1, (String)"onNext is null");
      io.reactivex.d.b.b.a(var2, (String)"onError is null");
      io.reactivex.d.b.b.a(var3, (String)"onComplete is null");
      a(var0, new io.reactivex.d.d.s(var1, var2, var3, io.reactivex.d.b.a.b()));
   }

   public static void a(io.reactivex.r var0, io.reactivex.t var1) {
      LinkedBlockingQueue var5 = new LinkedBlockingQueue();
      io.reactivex.d.d.h var4 = new io.reactivex.d.d.h(var5);
      var1.onSubscribe(var4);
      var0.subscribe(var4);

      while(!var4.isDisposed()) {
         Object var3 = var5.poll();
         Object var2 = var3;
         if(var3 == null) {
            try {
               var2 = var5.take();
            } catch (InterruptedException var6) {
               var4.dispose();
               var1.onError(var6);
               break;
            }
         }

         if(var4.isDisposed() || var0 == io.reactivex.d.d.h.a || io.reactivex.d.j.n.b(var2, var1)) {
            break;
         }
      }

   }
}
