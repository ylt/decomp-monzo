package io.reactivex.d.e.d;

import java.util.ArrayDeque;

public final class di extends a {
   final int b;

   public di(io.reactivex.r var1, int var2) {
      super(var1);
      this.b = var2;
   }

   public void subscribeActual(io.reactivex.t var1) {
      this.a.subscribe(new di.a(var1, this.b));
   }

   static final class a extends ArrayDeque implements io.reactivex.b.b, io.reactivex.t {
      final io.reactivex.t a;
      final int b;
      io.reactivex.b.b c;
      volatile boolean d;

      a(io.reactivex.t var1, int var2) {
         this.a = var1;
         this.b = var2;
      }

      public void dispose() {
         if(!this.d) {
            this.d = true;
            this.c.dispose();
         }

      }

      public boolean isDisposed() {
         return this.d;
      }

      public void onComplete() {
         io.reactivex.t var2 = this.a;

         while(!this.d) {
            Object var1 = this.poll();
            if(var1 == null) {
               if(!this.d) {
                  var2.onComplete();
               }
               break;
            }

            var2.onNext(var1);
         }

      }

      public void onError(Throwable var1) {
         this.a.onError(var1);
      }

      public void onNext(Object var1) {
         if(this.b == this.size()) {
            this.poll();
         }

         this.offer(var1);
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.c, var1)) {
            this.c = var1;
            this.a.onSubscribe(this);
         }

      }
   }
}
