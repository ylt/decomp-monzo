package io.reactivex.d.e.d;

import java.util.ArrayDeque;
import java.util.concurrent.atomic.AtomicInteger;

public final class w extends a {
   final io.reactivex.c.h b;
   final io.reactivex.d.j.i c;
   final int d;
   final int e;

   public w(io.reactivex.r var1, io.reactivex.c.h var2, io.reactivex.d.j.i var3, int var4, int var5) {
      super(var1);
      this.b = var2;
      this.c = var3;
      this.d = var4;
      this.e = var5;
   }

   protected void subscribeActual(io.reactivex.t var1) {
      this.a.subscribe(new w.a(var1, this.b, this.d, this.e, this.c));
   }

   static final class a extends AtomicInteger implements io.reactivex.b.b, io.reactivex.d.d.r, io.reactivex.t {
      final io.reactivex.t a;
      final io.reactivex.c.h b;
      final int c;
      final int d;
      final io.reactivex.d.j.i e;
      final io.reactivex.d.j.c f;
      final ArrayDeque g;
      io.reactivex.d.c.i h;
      io.reactivex.b.b i;
      volatile boolean j;
      int k;
      volatile boolean l;
      io.reactivex.d.d.q m;
      int n;

      a(io.reactivex.t var1, io.reactivex.c.h var2, int var3, int var4, io.reactivex.d.j.i var5) {
         this.a = var1;
         this.b = var2;
         this.c = var3;
         this.d = var4;
         this.e = var5;
         this.f = new io.reactivex.d.j.c();
         this.g = new ArrayDeque();
      }

      public void a() {
         // $FF: Couldn't be decompiled
      }

      public void a(io.reactivex.d.d.q var1) {
         var1.b();
         this.a();
      }

      public void a(io.reactivex.d.d.q var1, Object var2) {
         var1.c().a(var2);
         this.a();
      }

      public void a(io.reactivex.d.d.q var1, Throwable var2) {
         if(this.f.a(var2)) {
            if(this.e == io.reactivex.d.j.i.a) {
               this.i.dispose();
            }

            var1.b();
            this.a();
         } else {
            io.reactivex.g.a.a(var2);
         }

      }

      void b() {
         io.reactivex.d.d.q var1 = this.m;
         if(var1 != null) {
            var1.dispose();
         }

         while(true) {
            var1 = (io.reactivex.d.d.q)this.g.poll();
            if(var1 == null) {
               return;
            }

            var1.dispose();
         }
      }

      public void dispose() {
         this.l = true;
         if(this.getAndIncrement() == 0) {
            this.h.c();
            this.b();
         }

      }

      public boolean isDisposed() {
         return this.l;
      }

      public void onComplete() {
         this.j = true;
         this.a();
      }

      public void onError(Throwable var1) {
         if(this.f.a(var1)) {
            this.j = true;
            this.a();
         } else {
            io.reactivex.g.a.a(var1);
         }

      }

      public void onNext(Object var1) {
         if(this.k == 0) {
            this.h.a(var1);
         }

         this.a();
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.i, var1)) {
            this.i = var1;
            if(var1 instanceof io.reactivex.d.c.d) {
               io.reactivex.d.c.d var3 = (io.reactivex.d.c.d)var1;
               int var2 = var3.a(3);
               if(var2 == 1) {
                  this.k = var2;
                  this.h = var3;
                  this.j = true;
                  this.a.onSubscribe(this);
                  this.a();
                  return;
               }

               if(var2 == 2) {
                  this.k = var2;
                  this.h = var3;
                  this.a.onSubscribe(this);
                  return;
               }
            }

            this.h = io.reactivex.d.j.r.a(this.d);
            this.a.onSubscribe(this);
         }

      }
   }
}
