package io.reactivex.d.e.d;

public final class bx extends a {
   final io.reactivex.u b;
   final boolean c;
   final int d;

   public bx(io.reactivex.r var1, io.reactivex.u var2, boolean var3, int var4) {
      super(var1);
      this.b = var2;
      this.c = var3;
      this.d = var4;
   }

   protected void subscribeActual(io.reactivex.t var1) {
      if(this.b instanceof io.reactivex.d.g.n) {
         this.a.subscribe(var1);
      } else {
         io.reactivex.u.c var2 = this.b.a();
         this.a.subscribe(new bx.a(var1, var2, this.c, this.d));
      }

   }

   static final class a extends io.reactivex.d.d.b implements io.reactivex.t, Runnable {
      final io.reactivex.t a;
      final io.reactivex.u.c b;
      final boolean c;
      final int d;
      io.reactivex.d.c.i e;
      io.reactivex.b.b f;
      Throwable g;
      volatile boolean h;
      volatile boolean i;
      int j;
      boolean k;

      a(io.reactivex.t var1, io.reactivex.u.c var2, boolean var3, int var4) {
         this.a = var1;
         this.b = var2;
         this.c = var3;
         this.d = var4;
      }

      public int a(int var1) {
         byte var2;
         if((var1 & 2) != 0) {
            this.k = true;
            var2 = 2;
         } else {
            var2 = 0;
         }

         return var2;
      }

      boolean a(boolean var1, boolean var2, io.reactivex.t var3) {
         boolean var4 = true;
         if(this.i) {
            this.e.c();
            var1 = var4;
         } else {
            if(var1) {
               Throwable var5 = this.g;
               if(this.c) {
                  if(var2) {
                     if(var5 != null) {
                        var3.onError(var5);
                     } else {
                        var3.onComplete();
                     }

                     this.b.dispose();
                     var1 = var4;
                     return var1;
                  }
               } else {
                  if(var5 != null) {
                     this.e.c();
                     var3.onError(var5);
                     this.b.dispose();
                     var1 = var4;
                     return var1;
                  }

                  if(var2) {
                     var3.onComplete();
                     this.b.dispose();
                     var1 = var4;
                     return var1;
                  }
               }
            }

            var1 = false;
         }

         return var1;
      }

      public boolean b() {
         return this.e.b();
      }

      public void c() {
         this.e.c();
      }

      void d() {
         if(this.getAndIncrement() == 0) {
            this.b.a((Runnable)this);
         }

      }

      public void dispose() {
         if(!this.i) {
            this.i = true;
            this.f.dispose();
            this.b.dispose();
            if(this.getAndIncrement() == 0) {
               this.e.c();
            }
         }

      }

      void e() {
         io.reactivex.d.c.i var5 = this.e;
         io.reactivex.t var6 = this.a;
         int var1 = 1;

         while(!this.a(this.h, var5.b(), var6)) {
            while(true) {
               boolean var4 = this.h;

               Object var7;
               try {
                  var7 = var5.n_();
               } catch (Throwable var8) {
                  io.reactivex.exceptions.a.b(var8);
                  this.f.dispose();
                  var5.c();
                  var6.onError(var8);
                  this.b.dispose();
                  return;
               }

               boolean var3;
               if(var7 == null) {
                  var3 = true;
               } else {
                  var3 = false;
               }

               if(this.a(var4, var3, var6)) {
                  return;
               }

               if(var3) {
                  int var2 = this.addAndGet(-var1);
                  var1 = var2;
                  if(var2 == 0) {
                     return;
                  }
               } else {
                  var6.onNext(var7);
               }
            }
         }

      }

      void f() {
         int var1 = 1;

         while(!this.i) {
            boolean var3 = this.h;
            Throwable var4 = this.g;
            if(!this.c && var3 && var4 != null) {
               this.a.onError(this.g);
               this.b.dispose();
               break;
            }

            this.a.onNext((Object)null);
            if(var3) {
               var4 = this.g;
               if(var4 != null) {
                  this.a.onError(var4);
               } else {
                  this.a.onComplete();
               }

               this.b.dispose();
               break;
            }

            int var2 = this.addAndGet(-var1);
            var1 = var2;
            if(var2 == 0) {
               break;
            }
         }

      }

      public boolean isDisposed() {
         return this.i;
      }

      public Object n_() throws Exception {
         return this.e.n_();
      }

      public void onComplete() {
         if(!this.h) {
            this.h = true;
            this.d();
         }

      }

      public void onError(Throwable var1) {
         if(this.h) {
            io.reactivex.g.a.a(var1);
         } else {
            this.g = var1;
            this.h = true;
            this.d();
         }

      }

      public void onNext(Object var1) {
         if(!this.h) {
            if(this.j != 2) {
               this.e.a(var1);
            }

            this.d();
         }

      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.f, var1)) {
            this.f = var1;
            if(var1 instanceof io.reactivex.d.c.d) {
               io.reactivex.d.c.d var3 = (io.reactivex.d.c.d)var1;
               int var2 = var3.a(7);
               if(var2 == 1) {
                  this.j = var2;
                  this.e = var3;
                  this.h = true;
                  this.a.onSubscribe(this);
                  this.d();
                  return;
               }

               if(var2 == 2) {
                  this.j = var2;
                  this.e = var3;
                  this.a.onSubscribe(this);
                  return;
               }
            }

            this.e = new io.reactivex.d.f.c(this.d);
            this.a.onSubscribe(this);
         }

      }

      public void run() {
         if(this.k) {
            this.f();
         } else {
            this.e();
         }

      }
   }
}
