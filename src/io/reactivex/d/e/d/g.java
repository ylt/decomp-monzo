package io.reactivex.d.e.d;

public final class g extends io.reactivex.v implements io.reactivex.d.c.c {
   final io.reactivex.r a;
   final io.reactivex.c.q b;

   public g(io.reactivex.r var1, io.reactivex.c.q var2) {
      this.a = var1;
      this.b = var2;
   }

   protected void b(io.reactivex.x var1) {
      this.a.subscribe(new g.a(var1, this.b));
   }

   public io.reactivex.n q_() {
      return io.reactivex.g.a.a((io.reactivex.n)(new f(this.a, this.b)));
   }

   static final class a implements io.reactivex.b.b, io.reactivex.t {
      final io.reactivex.x a;
      final io.reactivex.c.q b;
      io.reactivex.b.b c;
      boolean d;

      a(io.reactivex.x var1, io.reactivex.c.q var2) {
         this.a = var1;
         this.b = var2;
      }

      public void dispose() {
         this.c.dispose();
      }

      public boolean isDisposed() {
         return this.c.isDisposed();
      }

      public void onComplete() {
         if(!this.d) {
            this.d = true;
            this.a.a_(Boolean.valueOf(true));
         }

      }

      public void onError(Throwable var1) {
         if(this.d) {
            io.reactivex.g.a.a(var1);
         } else {
            this.d = true;
            this.a.onError(var1);
         }

      }

      public void onNext(Object var1) {
         if(!this.d) {
            boolean var2;
            try {
               var2 = this.b.a(var1);
            } catch (Throwable var3) {
               io.reactivex.exceptions.a.b(var3);
               this.c.dispose();
               this.onError(var3);
               return;
            }

            if(!var2) {
               this.d = true;
               this.c.dispose();
               this.a.a_(Boolean.valueOf(false));
            }
         }

      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.c, var1)) {
            this.c = var1;
            this.a.onSubscribe(this);
         }

      }
   }
}
