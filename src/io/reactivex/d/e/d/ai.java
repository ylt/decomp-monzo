package io.reactivex.d.e.d;

public final class ai extends a {
   final io.reactivex.c.h b;
   final io.reactivex.c.d c;

   public ai(io.reactivex.r var1, io.reactivex.c.h var2, io.reactivex.c.d var3) {
      super(var1);
      this.b = var2;
      this.c = var3;
   }

   protected void subscribeActual(io.reactivex.t var1) {
      this.a.subscribe(new ai.a(var1, this.b, this.c));
   }

   static final class a extends io.reactivex.d.d.a {
      final io.reactivex.c.h f;
      final io.reactivex.c.d g;
      Object h;
      boolean i;

      a(io.reactivex.t var1, io.reactivex.c.h var2, io.reactivex.c.d var3) {
         super(var1);
         this.f = var2;
         this.g = var3;
      }

      public int a(int var1) {
         return this.b(var1);
      }

      public Object n_() throws Exception {
         while(true) {
            Object var1 = this.c.n_();
            if(var1 == null) {
               var1 = null;
            } else {
               Object var2 = this.f.a(var1);
               if(!this.i) {
                  this.i = true;
                  this.h = var2;
               } else {
                  if(this.g.a(this.h, var2)) {
                     this.h = var2;
                     continue;
                  }

                  this.h = var2;
               }
            }

            return var1;
         }
      }

      public void onNext(Object var1) {
         if(!this.d) {
            if(this.e != 0) {
               this.a.onNext(var1);
            } else {
               label25: {
                  boolean var2;
                  try {
                     Object var3 = this.f.a(var1);
                     if(!this.i) {
                        this.i = true;
                        this.h = var3;
                        break label25;
                     }

                     var2 = this.g.a(this.h, var3);
                     this.h = var3;
                  } catch (Throwable var4) {
                     this.a(var4);
                     return;
                  }

                  if(var2) {
                     return;
                  }
               }

               this.a.onNext(var1);
            }
         }

      }
   }
}
