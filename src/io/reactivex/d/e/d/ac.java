package io.reactivex.d.e.d;

import java.util.concurrent.Callable;

public final class ac extends io.reactivex.n {
   final Callable a;

   public ac(Callable var1) {
      this.a = var1;
   }

   public void subscribeActual(io.reactivex.t var1) {
      io.reactivex.r var2;
      try {
         var2 = (io.reactivex.r)io.reactivex.d.b.b.a(this.a.call(), "null ObservableSource supplied");
      } catch (Throwable var3) {
         io.reactivex.exceptions.a.b(var3);
         io.reactivex.d.a.e.a(var3, var1);
         return;
      }

      var2.subscribe(var1);
   }
}
