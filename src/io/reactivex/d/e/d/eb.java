package io.reactivex.d.e.d;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

public final class eb extends a {
   final long b;
   final long c;
   final TimeUnit d;
   final io.reactivex.u e;
   final long f;
   final int g;
   final boolean h;

   public eb(io.reactivex.r var1, long var2, long var4, TimeUnit var6, io.reactivex.u var7, long var8, int var10, boolean var11) {
      super(var1);
      this.b = var2;
      this.c = var4;
      this.d = var6;
      this.e = var7;
      this.f = var8;
      this.g = var10;
      this.h = var11;
   }

   public void subscribeActual(io.reactivex.t var1) {
      io.reactivex.f.e var2 = new io.reactivex.f.e(var1);
      if(this.b == this.c) {
         if(this.f == Long.MAX_VALUE) {
            this.a.subscribe(new eb.b(var2, this.b, this.d, this.e, this.g));
         } else {
            this.a.subscribe(new eb.a(var2, this.b, this.d, this.e, this.g, this.f, this.h));
         }
      } else {
         this.a.subscribe(new eb.c(var2, this.b, this.c, this.d, this.e.a(), this.g));
      }

   }

   static final class a extends io.reactivex.d.d.t implements io.reactivex.b.b {
      final long g;
      final TimeUnit h;
      final io.reactivex.u i;
      final int j;
      final boolean k;
      final long l;
      final io.reactivex.u.c m;
      long n;
      long o;
      io.reactivex.b.b p;
      io.reactivex.i.d q;
      volatile boolean r;
      final AtomicReference s = new AtomicReference();

      a(io.reactivex.t var1, long var2, TimeUnit var4, io.reactivex.u var5, int var6, long var7, boolean var9) {
         super(var1, new io.reactivex.d.f.a());
         this.g = var2;
         this.h = var4;
         this.i = var5;
         this.j = var6;
         this.l = var7;
         this.k = var9;
         if(var9) {
            this.m = var5.a();
         } else {
            this.m = null;
         }

      }

      public void dispose() {
         this.c = true;
      }

      void f() {
         io.reactivex.d.a.d.a(this.s);
         io.reactivex.u.c var1 = this.m;
         if(var1 != null) {
            var1.dispose();
         }

      }

      void g() {
         io.reactivex.d.f.a var9 = (io.reactivex.d.f.a)this.b;
         io.reactivex.t var8 = this.a;
         io.reactivex.i.d var7 = this.q;
         int var1 = 1;

         while(true) {
            if(this.r) {
               this.p.dispose();
               var9.c();
               this.f();
               break;
            }

            boolean var3 = this.d;
            Object var10 = var9.n_();
            boolean var2;
            if(var10 == null) {
               var2 = true;
            } else {
               var2 = false;
            }

            boolean var4 = var10 instanceof eb.a;
            if(var3 && (var2 || var4)) {
               this.q = null;
               var9.c();
               this.f();
               Throwable var12 = this.e;
               if(var12 != null) {
                  var7.onError(var12);
               } else {
                  var7.onComplete();
               }
               break;
            }

            if(var2) {
               var1 = this.a(-var1);
               if(var1 != 0) {
                  continue;
               }
               break;
            } else if(var4) {
               eb.a var14 = (eb.a)var10;
               if(this.k || this.o == var14.a) {
                  var7.onComplete();
                  this.n = 0L;
                  var7 = io.reactivex.i.d.a(this.j);
                  this.q = var7;
                  var8.onNext(var7);
               }
            } else {
               var7.onNext(io.reactivex.d.j.n.e(var10));
               long var5 = this.n + 1L;
               if(var5 >= this.l) {
                  ++this.o;
                  this.n = 0L;
                  var7.onComplete();
                  var7 = io.reactivex.i.d.a(this.j);
                  this.q = var7;
                  this.a.onNext(var7);
                  if(this.k) {
                     io.reactivex.b.b var13 = (io.reactivex.b.b)this.s.get();
                     var13.dispose();
                     io.reactivex.b.b var11 = this.m.a(new eb.a(this.o, this), this.g, this.g, this.h);
                     if(!this.s.compareAndSet(var13, var11)) {
                        var11.dispose();
                     }
                  }
               } else {
                  this.n = var5;
               }
            }
         }

      }

      public boolean isDisposed() {
         return this.c;
      }

      public void onComplete() {
         this.d = true;
         if(this.c()) {
            this.g();
         }

         this.a.onComplete();
         this.f();
      }

      public void onError(Throwable var1) {
         this.e = var1;
         this.d = true;
         if(this.c()) {
            this.g();
         }

         this.a.onError(var1);
         this.f();
      }

      public void onNext(Object var1) {
         if(!this.r) {
            if(this.d()) {
               io.reactivex.i.d var4 = this.q;
               var4.onNext(var1);
               long var2 = this.n + 1L;
               if(var2 >= this.l) {
                  ++this.o;
                  this.n = 0L;
                  var4.onComplete();
                  io.reactivex.i.d var5 = io.reactivex.i.d.a(this.j);
                  this.q = var5;
                  this.a.onNext(var5);
                  if(this.k) {
                     ((io.reactivex.b.b)this.s.get()).dispose();
                     io.reactivex.b.b var6 = this.m.a(new eb.a(this.o, this), this.g, this.g, this.h);
                     io.reactivex.d.a.d.c(this.s, var6);
                  }
               } else {
                  this.n = var2;
               }

               if(this.a(-1) == 0) {
                  return;
               }
            } else {
               this.b.a(io.reactivex.d.j.n.a(var1));
               if(!this.c()) {
                  return;
               }
            }

            this.g();
         }

      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.p, var1)) {
            this.p = var1;
            io.reactivex.t var2 = this.a;
            var2.onSubscribe(this);
            if(!this.c) {
               io.reactivex.i.d var3 = io.reactivex.i.d.a(this.j);
               this.q = var3;
               var2.onNext(var3);
               eb.a var4 = new eb.a(this.o, this);
               if(this.k) {
                  var1 = this.m.a(var4, this.g, this.g, this.h);
               } else {
                  var1 = this.i.a(var4, this.g, this.g, this.h);
               }

               io.reactivex.d.a.d.c(this.s, var1);
            }
         }

      }
   }

   static final class a implements Runnable {
      final long a;
      final eb.a b;

      a(long var1, eb.a var3) {
         this.a = var1;
         this.b = var3;
      }

      public void run() {
         eb.a var1 = this.b;
         if(!var1.c) {
            var1.b.a(this);
         } else {
            var1.r = true;
            var1.f();
         }

         if(var1.c()) {
            var1.g();
         }

      }
   }

   static final class b extends io.reactivex.d.d.t implements io.reactivex.b.b, io.reactivex.t, Runnable {
      static final Object n = new Object();
      final long g;
      final TimeUnit h;
      final io.reactivex.u i;
      final int j;
      io.reactivex.b.b k;
      io.reactivex.i.d l;
      final AtomicReference m = new AtomicReference();
      volatile boolean o;

      b(io.reactivex.t var1, long var2, TimeUnit var4, io.reactivex.u var5, int var6) {
         super(var1, new io.reactivex.d.f.a());
         this.g = var2;
         this.h = var4;
         this.i = var5;
         this.j = var6;
      }

      public void dispose() {
         this.c = true;
      }

      void f() {
         io.reactivex.d.a.d.a(this.m);
      }

      void g() {
         io.reactivex.d.f.a var6 = (io.reactivex.d.f.a)this.b;
         io.reactivex.t var7 = this.a;
         io.reactivex.i.d var5 = this.l;
         int var1 = 1;

         while(true) {
            boolean var4 = this.o;
            boolean var3 = this.d;
            Object var8 = var6.n_();
            if(var3 && (var8 == null || var8 == n)) {
               this.l = null;
               var6.c();
               this.f();
               Throwable var9 = this.e;
               if(var9 != null) {
                  var5.onError(var9);
               } else {
                  var5.onComplete();
               }
               break;
            }

            if(var8 == null) {
               int var2 = this.a(-var1);
               var1 = var2;
               if(var2 == 0) {
                  break;
               }
            } else if(var8 == n) {
               var5.onComplete();
               if(!var4) {
                  var5 = io.reactivex.i.d.a(this.j);
                  this.l = var5;
                  var7.onNext(var5);
               } else {
                  this.k.dispose();
               }
            } else {
               var5.onNext(io.reactivex.d.j.n.e(var8));
            }
         }

      }

      public boolean isDisposed() {
         return this.c;
      }

      public void onComplete() {
         this.d = true;
         if(this.c()) {
            this.g();
         }

         this.f();
         this.a.onComplete();
      }

      public void onError(Throwable var1) {
         this.e = var1;
         this.d = true;
         if(this.c()) {
            this.g();
         }

         this.f();
         this.a.onError(var1);
      }

      public void onNext(Object var1) {
         if(!this.o) {
            if(this.d()) {
               this.l.onNext(var1);
               if(this.a(-1) == 0) {
                  return;
               }
            } else {
               this.b.a(io.reactivex.d.j.n.a(var1));
               if(!this.c()) {
                  return;
               }
            }

            this.g();
         }

      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.k, var1)) {
            this.k = var1;
            this.l = io.reactivex.i.d.a(this.j);
            io.reactivex.t var2 = this.a;
            var2.onSubscribe(this);
            var2.onNext(this.l);
            if(!this.c) {
               var1 = this.i.a(this, this.g, this.g, this.h);
               io.reactivex.d.a.d.c(this.m, var1);
            }
         }

      }

      public void run() {
         if(this.c) {
            this.o = true;
            this.f();
         }

         this.b.a(n);
         if(this.c()) {
            this.g();
         }

      }
   }

   static final class c extends io.reactivex.d.d.t implements io.reactivex.b.b, Runnable {
      final long g;
      final long h;
      final TimeUnit i;
      final io.reactivex.u.c j;
      final int k;
      final List l;
      io.reactivex.b.b m;
      volatile boolean n;

      c(io.reactivex.t var1, long var2, long var4, TimeUnit var6, io.reactivex.u.c var7, int var8) {
         super(var1, new io.reactivex.d.f.a());
         this.g = var2;
         this.h = var4;
         this.i = var6;
         this.j = var7;
         this.k = var8;
         this.l = new LinkedList();
      }

      void a(io.reactivex.i.d var1) {
         this.b.a(new eb.b(var1, false));
         if(this.c()) {
            this.g();
         }

      }

      public void dispose() {
         this.c = true;
      }

      void f() {
         this.j.dispose();
      }

      void g() {
         io.reactivex.d.f.a var7 = (io.reactivex.d.f.a)this.b;
         io.reactivex.t var6 = this.a;
         List var5 = this.l;
         int var1 = 1;

         label73:
         while(true) {
            if(this.n) {
               this.m.dispose();
               this.f();
               var7.c();
               var5.clear();
               break;
            }

            boolean var3 = this.d;
            Object var8 = var7.n_();
            boolean var2;
            if(var8 == null) {
               var2 = true;
            } else {
               var2 = false;
            }

            boolean var4 = var8 instanceof eb.b;
            if(!var3 || !var2 && !var4) {
               if(var2) {
                  var1 = this.a(-var1);
                  if(var1 != 0) {
                     continue;
                  }
                  break;
               }

               if(var4) {
                  eb.b var12 = (eb.b)var8;
                  if(var12.b) {
                     if(!this.c) {
                        io.reactivex.i.d var13 = io.reactivex.i.d.a(this.k);
                        var5.add(var13);
                        var6.onNext(var13);
                        this.j.a(new eb.a(var13), this.g, this.i);
                     }
                  } else {
                     var5.remove(var12.a);
                     var12.a.onComplete();
                     if(var5.isEmpty() && this.c) {
                        this.n = true;
                     }
                  }
                  continue;
               }

               Iterator var9 = var5.iterator();

               while(true) {
                  if(!var9.hasNext()) {
                     continue label73;
                  }

                  ((io.reactivex.i.d)var9.next()).onNext(var8);
               }
            }

            var7.c();
            Throwable var11 = this.e;
            Iterator var10;
            if(var11 != null) {
               var10 = var5.iterator();

               while(var10.hasNext()) {
                  ((io.reactivex.i.d)var10.next()).onError(var11);
               }
            } else {
               var10 = var5.iterator();

               while(var10.hasNext()) {
                  ((io.reactivex.i.d)var10.next()).onComplete();
               }
            }

            this.f();
            var5.clear();
            break;
         }

      }

      public boolean isDisposed() {
         return this.c;
      }

      public void onComplete() {
         this.d = true;
         if(this.c()) {
            this.g();
         }

         this.a.onComplete();
         this.f();
      }

      public void onError(Throwable var1) {
         this.e = var1;
         this.d = true;
         if(this.c()) {
            this.g();
         }

         this.a.onError(var1);
         this.f();
      }

      public void onNext(Object var1) {
         if(this.d()) {
            Iterator var2 = this.l.iterator();

            while(var2.hasNext()) {
               ((io.reactivex.i.d)var2.next()).onNext(var1);
            }

            if(this.a(-1) == 0) {
               return;
            }
         } else {
            this.b.a(var1);
            if(!this.c()) {
               return;
            }
         }

         this.g();
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.m, var1)) {
            this.m = var1;
            this.a.onSubscribe(this);
            if(!this.c) {
               io.reactivex.i.d var2 = io.reactivex.i.d.a(this.k);
               this.l.add(var2);
               this.a.onNext(var2);
               this.j.a(new eb.a(var2), this.g, this.i);
               this.j.a(this, this.h, this.h, this.i);
            }
         }

      }

      public void run() {
         eb.b var1 = new eb.b(io.reactivex.i.d.a(this.k), true);
         if(!this.c) {
            this.b.a(var1);
         }

         if(this.c()) {
            this.g();
         }

      }
   }

   final class a implements Runnable {
      private final io.reactivex.i.d b;

      a(io.reactivex.i.d var2) {
         this.b = var2;
      }

      public void run() {
         eb.this.a(this.b);
      }
   }

   static final class b {
      final io.reactivex.i.d a;
      final boolean b;

      b(io.reactivex.i.d var1, boolean var2) {
         this.a = var1;
         this.b = var2;
      }
   }
}
