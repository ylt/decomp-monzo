package io.reactivex.d.e.d;

import java.util.concurrent.Callable;

public final class ct extends a {
   final io.reactivex.c.c b;
   final Callable c;

   public ct(io.reactivex.r var1, Callable var2, io.reactivex.c.c var3) {
      super(var1);
      this.b = var3;
      this.c = var2;
   }

   public void subscribeActual(io.reactivex.t var1) {
      Object var2;
      try {
         var2 = io.reactivex.d.b.b.a(this.c.call(), "The seed supplied is null");
      } catch (Throwable var3) {
         io.reactivex.exceptions.a.b(var3);
         io.reactivex.d.a.e.a(var3, var1);
         return;
      }

      this.a.subscribe(new ct.a(var1, this.b, var2));
   }

   static final class a implements io.reactivex.b.b, io.reactivex.t {
      final io.reactivex.t a;
      final io.reactivex.c.c b;
      Object c;
      io.reactivex.b.b d;
      boolean e;

      a(io.reactivex.t var1, io.reactivex.c.c var2, Object var3) {
         this.a = var1;
         this.b = var2;
         this.c = var3;
      }

      public void dispose() {
         this.d.dispose();
      }

      public boolean isDisposed() {
         return this.d.isDisposed();
      }

      public void onComplete() {
         if(!this.e) {
            this.e = true;
            this.a.onComplete();
         }

      }

      public void onError(Throwable var1) {
         if(this.e) {
            io.reactivex.g.a.a(var1);
         } else {
            this.e = true;
            this.a.onError(var1);
         }

      }

      public void onNext(Object var1) {
         if(!this.e) {
            Object var2 = this.c;

            try {
               var1 = io.reactivex.d.b.b.a(this.b.a(var2, var1), "The accumulator returned a null value");
            } catch (Throwable var3) {
               io.reactivex.exceptions.a.b(var3);
               this.d.dispose();
               this.onError(var3);
               return;
            }

            this.c = var1;
            this.a.onNext(var1);
         }

      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.d, var1)) {
            this.d = var1;
            this.a.onSubscribe(this);
            this.a.onNext(this.c);
         }

      }
   }
}
