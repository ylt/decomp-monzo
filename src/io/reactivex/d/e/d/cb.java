package io.reactivex.d.e.d;

import java.util.concurrent.atomic.AtomicReference;

public final class cb extends a {
   final io.reactivex.c.h b;

   public cb(io.reactivex.r var1, io.reactivex.c.h var2) {
      super(var1);
      this.b = var2;
   }

   protected void subscribeActual(io.reactivex.t var1) {
      io.reactivex.i.a var3 = io.reactivex.i.a.a();

      io.reactivex.r var2;
      try {
         var2 = (io.reactivex.r)io.reactivex.d.b.b.a(this.b.a(var3), "The selector returned a null ObservableSource");
      } catch (Throwable var4) {
         io.reactivex.exceptions.a.b(var4);
         io.reactivex.d.a.e.a(var4, var1);
         return;
      }

      cb.b var5 = new cb.b(var1);
      var2.subscribe(var5);
      this.a.subscribe(new cb.a(var3, var5));
   }

   static final class a implements io.reactivex.t {
      final io.reactivex.i.a a;
      final AtomicReference b;

      a(io.reactivex.i.a var1, AtomicReference var2) {
         this.a = var1;
         this.b = var2;
      }

      public void onComplete() {
         this.a.onComplete();
      }

      public void onError(Throwable var1) {
         this.a.onError(var1);
      }

      public void onNext(Object var1) {
         this.a.onNext(var1);
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         io.reactivex.d.a.d.b(this.b, var1);
      }
   }

   static final class b extends AtomicReference implements io.reactivex.b.b, io.reactivex.t {
      final io.reactivex.t a;
      io.reactivex.b.b b;

      b(io.reactivex.t var1) {
         this.a = var1;
      }

      public void dispose() {
         this.b.dispose();
         io.reactivex.d.a.d.a((AtomicReference)this);
      }

      public boolean isDisposed() {
         return this.b.isDisposed();
      }

      public void onComplete() {
         io.reactivex.d.a.d.a((AtomicReference)this);
         this.a.onComplete();
      }

      public void onError(Throwable var1) {
         io.reactivex.d.a.d.a((AtomicReference)this);
         this.a.onError(var1);
      }

      public void onNext(Object var1) {
         this.a.onNext(var1);
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.b, var1)) {
            this.b = var1;
            this.a.onSubscribe(this);
         }

      }
   }
}
