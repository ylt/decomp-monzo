package io.reactivex.d.e.d;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public final class bh extends a {
   final io.reactivex.r b;
   final io.reactivex.c.h c;
   final io.reactivex.c.h d;
   final io.reactivex.c.c e;

   public bh(io.reactivex.r var1, io.reactivex.r var2, io.reactivex.c.h var3, io.reactivex.c.h var4, io.reactivex.c.c var5) {
      super(var1);
      this.b = var2;
      this.c = var3;
      this.d = var4;
      this.e = var5;
   }

   protected void subscribeActual(io.reactivex.t var1) {
      bh.a var2 = new bh.a(var1, this.c, this.d, this.e);
      var1.onSubscribe(var2);
      bh.d var4 = new bh.d(var2, true);
      var2.c.a((io.reactivex.b.b)var4);
      bh.d var3 = new bh.d(var2, false);
      var2.c.a((io.reactivex.b.b)var3);
      this.a.subscribe(var4);
      this.b.subscribe(var3);
   }

   static final class a extends AtomicInteger implements io.reactivex.b.b, bh.b {
      static final Integer n = Integer.valueOf(1);
      static final Integer o = Integer.valueOf(2);
      static final Integer p = Integer.valueOf(3);
      static final Integer q = Integer.valueOf(4);
      final io.reactivex.t a;
      final io.reactivex.d.f.c b;
      final io.reactivex.b.a c;
      final Map d;
      final Map e;
      final AtomicReference f;
      final io.reactivex.c.h g;
      final io.reactivex.c.h h;
      final io.reactivex.c.c i;
      final AtomicInteger j;
      int k;
      int l;
      volatile boolean m;

      a(io.reactivex.t var1, io.reactivex.c.h var2, io.reactivex.c.h var3, io.reactivex.c.c var4) {
         this.a = var1;
         this.c = new io.reactivex.b.a();
         this.b = new io.reactivex.d.f.c(io.reactivex.n.bufferSize());
         this.d = new LinkedHashMap();
         this.e = new LinkedHashMap();
         this.f = new AtomicReference();
         this.g = var2;
         this.h = var3;
         this.i = var4;
         this.j = new AtomicInteger(2);
      }

      void a() {
         this.c.dispose();
      }

      public void a(bh.d var1) {
         this.c.c(var1);
         this.j.decrementAndGet();
         this.b();
      }

      void a(io.reactivex.t var1) {
         Throwable var2 = io.reactivex.d.j.j.a(this.f);
         Iterator var3 = this.d.values().iterator();

         while(var3.hasNext()) {
            ((io.reactivex.i.d)var3.next()).onError(var2);
         }

         this.d.clear();
         this.e.clear();
         var1.onError(var2);
      }

      public void a(Throwable var1) {
         if(io.reactivex.d.j.j.a(this.f, var1)) {
            this.j.decrementAndGet();
            this.b();
         } else {
            io.reactivex.g.a.a(var1);
         }

      }

      void a(Throwable var1, io.reactivex.t var2, io.reactivex.d.f.c var3) {
         io.reactivex.exceptions.a.b(var1);
         io.reactivex.d.j.j.a(this.f, var1);
         var3.c();
         this.a();
         this.a(var2);
      }

      public void a(boolean param1, bh.c param2) {
         // $FF: Couldn't be decompiled
      }

      public void a(boolean param1, Object param2) {
         // $FF: Couldn't be decompiled
      }

      void b() {
         if(this.getAndIncrement() == 0) {
            io.reactivex.d.f.c var5 = this.b;
            io.reactivex.t var4 = this.a;
            int var1 = 1;

            while(true) {
               if(this.m) {
                  var5.c();
                  break;
               }

               if((Throwable)this.f.get() != null) {
                  var5.c();
                  this.a();
                  this.a(var4);
                  break;
               }

               boolean var2;
               if(this.j.get() == 0) {
                  var2 = true;
               } else {
                  var2 = false;
               }

               Integer var7 = (Integer)var5.n_();
               boolean var3;
               if(var7 == null) {
                  var3 = true;
               } else {
                  var3 = false;
               }

               if(var2 && var3) {
                  Iterator var14 = this.d.values().iterator();

                  while(var14.hasNext()) {
                     ((io.reactivex.i.d)var14.next()).onComplete();
                  }

                  this.d.clear();
                  this.e.clear();
                  this.c.dispose();
                  var4.onComplete();
                  break;
               }

               if(var3) {
                  var1 = this.addAndGet(-var1);
                  if(var1 == 0) {
                     break;
                  }
               } else {
                  Object var6 = var5.n_();
                  int var13;
                  io.reactivex.i.d var16;
                  if(var7 == n) {
                     var16 = io.reactivex.i.d.a();
                     var13 = this.k;
                     this.k = var13 + 1;
                     this.d.put(Integer.valueOf(var13), var16);

                     io.reactivex.r var9;
                     try {
                        var9 = (io.reactivex.r)io.reactivex.d.b.b.a(this.g.a(var6), "The leftEnd returned a null ObservableSource");
                     } catch (Throwable var10) {
                        this.a(var10, var4, var5);
                        break;
                     }

                     bh.c var20 = new bh.c(this, true, var13);
                     this.c.a((io.reactivex.b.b)var20);
                     var9.subscribe(var20);
                     if((Throwable)this.f.get() != null) {
                        var5.c();
                        this.a();
                        this.a(var4);
                        break;
                     }

                     try {
                        var6 = io.reactivex.d.b.b.a(this.i.a(var6, var16), "The resultSelector returned a null value");
                     } catch (Throwable var12) {
                        this.a(var12, var4, var5);
                        break;
                     }

                     var4.onNext(var6);
                     Iterator var17 = this.e.values().iterator();

                     while(var17.hasNext()) {
                        var16.onNext(var17.next());
                     }
                  } else if(var7 == o) {
                     var13 = this.l;
                     this.l = var13 + 1;
                     this.e.put(Integer.valueOf(var13), var6);

                     io.reactivex.r var8;
                     try {
                        var8 = (io.reactivex.r)io.reactivex.d.b.b.a(this.h.a(var6), "The rightEnd returned a null ObservableSource");
                     } catch (Throwable var11) {
                        this.a(var11, var4, var5);
                        break;
                     }

                     bh.c var18 = new bh.c(this, false, var13);
                     this.c.a((io.reactivex.b.b)var18);
                     var8.subscribe(var18);
                     if((Throwable)this.f.get() != null) {
                        var5.c();
                        this.a();
                        this.a(var4);
                        break;
                     }

                     Iterator var19 = this.d.values().iterator();

                     while(var19.hasNext()) {
                        ((io.reactivex.i.d)var19.next()).onNext(var6);
                     }
                  } else {
                     bh.c var15;
                     if(var7 == p) {
                        var15 = (bh.c)var6;
                        var16 = (io.reactivex.i.d)this.d.remove(Integer.valueOf(var15.c));
                        this.c.b(var15);
                        if(var16 != null) {
                           var16.onComplete();
                        }
                     } else if(var7 == q) {
                        var15 = (bh.c)var6;
                        this.e.remove(Integer.valueOf(var15.c));
                        this.c.b(var15);
                     }
                  }
               }
            }
         }

      }

      public void b(Throwable var1) {
         if(io.reactivex.d.j.j.a(this.f, var1)) {
            this.b();
         } else {
            io.reactivex.g.a.a(var1);
         }

      }

      public void dispose() {
         if(!this.m) {
            this.m = true;
            this.a();
            if(this.getAndIncrement() == 0) {
               this.b.c();
            }
         }

      }

      public boolean isDisposed() {
         return this.m;
      }
   }

   interface b {
      void a(bh.d var1);

      void a(Throwable var1);

      void a(boolean var1, bh.c var2);

      void a(boolean var1, Object var2);

      void b(Throwable var1);
   }

   static final class c extends AtomicReference implements io.reactivex.b.b, io.reactivex.t {
      final bh.b a;
      final boolean b;
      final int c;

      c(bh.b var1, boolean var2, int var3) {
         this.a = var1;
         this.b = var2;
         this.c = var3;
      }

      public void dispose() {
         io.reactivex.d.a.d.a((AtomicReference)this);
      }

      public boolean isDisposed() {
         return io.reactivex.d.a.d.a((io.reactivex.b.b)this.get());
      }

      public void onComplete() {
         this.a.a(this.b, this);
      }

      public void onError(Throwable var1) {
         this.a.b(var1);
      }

      public void onNext(Object var1) {
         if(io.reactivex.d.a.d.a((AtomicReference)this)) {
            this.a.a(this.b, this);
         }

      }

      public void onSubscribe(io.reactivex.b.b var1) {
         io.reactivex.d.a.d.b(this, var1);
      }
   }

   static final class d extends AtomicReference implements io.reactivex.b.b, io.reactivex.t {
      final bh.b a;
      final boolean b;

      d(bh.b var1, boolean var2) {
         this.a = var1;
         this.b = var2;
      }

      public void dispose() {
         io.reactivex.d.a.d.a((AtomicReference)this);
      }

      public boolean isDisposed() {
         return io.reactivex.d.a.d.a((io.reactivex.b.b)this.get());
      }

      public void onComplete() {
         this.a.a(this);
      }

      public void onError(Throwable var1) {
         this.a.a(var1);
      }

      public void onNext(Object var1) {
         this.a.a(this.b, var1);
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         io.reactivex.d.a.d.b(this, var1);
      }
   }
}
