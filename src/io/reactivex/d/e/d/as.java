package io.reactivex.d.e.d;

public final class as extends a {
   final io.reactivex.c.q b;

   public as(io.reactivex.r var1, io.reactivex.c.q var2) {
      super(var1);
      this.b = var2;
   }

   public void subscribeActual(io.reactivex.t var1) {
      this.a.subscribe(new as.a(var1, this.b));
   }

   static final class a extends io.reactivex.d.d.a {
      final io.reactivex.c.q f;

      a(io.reactivex.t var1, io.reactivex.c.q var2) {
         super(var1);
         this.f = var2;
      }

      public int a(int var1) {
         return this.b(var1);
      }

      public Object n_() throws Exception {
         Object var1;
         do {
            var1 = this.c.n_();
         } while(var1 != null && !this.f.a(var1));

         return var1;
      }

      public void onNext(Object var1) {
         if(this.e == 0) {
            boolean var2;
            try {
               var2 = this.f.a(var1);
            } catch (Throwable var3) {
               this.a(var3);
               return;
            }

            if(var2) {
               this.a.onNext(var1);
            }
         } else {
            this.a.onNext((Object)null);
         }

      }
   }
}
