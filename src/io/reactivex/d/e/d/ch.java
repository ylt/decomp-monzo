package io.reactivex.d.e.d;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.ReentrantLock;

public final class ch extends a {
   final io.reactivex.e.a b;
   volatile io.reactivex.b.a c = new io.reactivex.b.a();
   final AtomicInteger d = new AtomicInteger();
   final ReentrantLock e = new ReentrantLock();

   public ch(io.reactivex.e.a var1) {
      super(var1);
      this.b = var1;
   }

   private io.reactivex.b.b a(io.reactivex.b.a var1) {
      return io.reactivex.b.c.a(new ch.c(var1));
   }

   private io.reactivex.c.g a(io.reactivex.t var1, AtomicBoolean var2) {
      return new ch.b(var1, var2);
   }

   void a(io.reactivex.t var1, io.reactivex.b.a var2) {
      ch.a var3 = new ch.a(var1, var2, this.a(var2));
      var1.onSubscribe(var3);
      this.b.subscribe(var3);
   }

   public void subscribeActual(io.reactivex.t var1) {
      this.e.lock();
      if(this.d.incrementAndGet() == 1) {
         AtomicBoolean var2 = new AtomicBoolean(true);

         try {
            this.b.a(this.a(var1, var2));
         } finally {
            if(var2.get()) {
               this.e.unlock();
            }

         }
      } else {
         try {
            this.a(var1, this.c);
         } finally {
            this.e.unlock();
         }
      }

   }

   final class a extends AtomicReference implements io.reactivex.b.b, io.reactivex.t {
      final io.reactivex.t a;
      final io.reactivex.b.a b;
      final io.reactivex.b.b c;

      a(io.reactivex.t var2, io.reactivex.b.a var3, io.reactivex.b.b var4) {
         this.a = var2;
         this.b = var3;
         this.c = var4;
      }

      void a() {
         ch.this.e.lock();

         try {
            if(ch.this.c == this.b) {
               if(ch.this.b instanceof io.reactivex.b.b) {
                  ((io.reactivex.b.b)ch.this.b).dispose();
               }

               ch.this.c.dispose();
               ch var2 = ch.this;
               io.reactivex.b.a var1 = new io.reactivex.b.a();
               var2.c = var1;
               ch.this.d.set(0);
            }
         } finally {
            ch.this.e.unlock();
         }

      }

      public void dispose() {
         io.reactivex.d.a.d.a((AtomicReference)this);
         this.c.dispose();
      }

      public boolean isDisposed() {
         return io.reactivex.d.a.d.a((io.reactivex.b.b)this.get());
      }

      public void onComplete() {
         this.a();
         this.a.onComplete();
      }

      public void onError(Throwable var1) {
         this.a();
         this.a.onError(var1);
      }

      public void onNext(Object var1) {
         this.a.onNext(var1);
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         io.reactivex.d.a.d.b(this, var1);
      }
   }

   final class b implements io.reactivex.c.g {
      private final io.reactivex.t b;
      private final AtomicBoolean c;

      b(io.reactivex.t var2, AtomicBoolean var3) {
         this.b = var2;
         this.c = var3;
      }

      public void a(io.reactivex.b.b var1) {
         try {
            ch.this.c.a(var1);
            ch.this.a(this.b, ch.this.c);
         } finally {
            ch.this.e.unlock();
            this.c.set(false);
         }

      }
   }

   final class c implements Runnable {
      private final io.reactivex.b.a b;

      c(io.reactivex.b.a var2) {
         this.b = var2;
      }

      public void run() {
         ch.this.e.lock();

         try {
            if(ch.this.c == this.b && ch.this.d.decrementAndGet() == 0) {
               if(ch.this.b instanceof io.reactivex.b.b) {
                  ((io.reactivex.b.b)ch.this.b).dispose();
               }

               ch.this.c.dispose();
               ch var1 = ch.this;
               io.reactivex.b.a var2 = new io.reactivex.b.a();
               var1.c = var2;
            }
         } finally {
            ch.this.e.unlock();
         }

      }
   }
}
