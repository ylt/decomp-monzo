package io.reactivex.d.e.d;

import java.util.Collection;
import java.util.concurrent.Callable;

public final class du extends io.reactivex.v implements io.reactivex.d.c.c {
   final io.reactivex.r a;
   final Callable b;

   public du(io.reactivex.r var1, int var2) {
      this.a = var1;
      this.b = io.reactivex.d.b.a.a(var2);
   }

   public du(io.reactivex.r var1, Callable var2) {
      this.a = var1;
      this.b = var2;
   }

   public void b(io.reactivex.x var1) {
      Collection var2;
      try {
         var2 = (Collection)io.reactivex.d.b.b.a(this.b.call(), "The collectionSupplier returned a null collection. Null values are generally not allowed in 2.x operators and sources.");
      } catch (Throwable var3) {
         io.reactivex.exceptions.a.b(var3);
         io.reactivex.d.a.e.a(var3, var1);
         return;
      }

      this.a.subscribe(new du.a(var1, var2));
   }

   public io.reactivex.n q_() {
      return io.reactivex.g.a.a((io.reactivex.n)(new dt(this.a, this.b)));
   }

   static final class a implements io.reactivex.b.b, io.reactivex.t {
      final io.reactivex.x a;
      Collection b;
      io.reactivex.b.b c;

      a(io.reactivex.x var1, Collection var2) {
         this.a = var1;
         this.b = var2;
      }

      public void dispose() {
         this.c.dispose();
      }

      public boolean isDisposed() {
         return this.c.isDisposed();
      }

      public void onComplete() {
         Collection var1 = this.b;
         this.b = null;
         this.a.a_(var1);
      }

      public void onError(Throwable var1) {
         this.b = null;
         this.a.onError(var1);
      }

      public void onNext(Object var1) {
         this.b.add(var1);
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.c, var1)) {
            this.c = var1;
            this.a.onSubscribe(this);
         }

      }
   }
}
