package io.reactivex.d.e.d;

import java.util.concurrent.atomic.AtomicInteger;

public final class cj extends a {
   final io.reactivex.c.e b;

   public cj(io.reactivex.n var1, io.reactivex.c.e var2) {
      super(var1);
      this.b = var2;
   }

   public void subscribeActual(io.reactivex.t var1) {
      io.reactivex.d.a.k var2 = new io.reactivex.d.a.k();
      var1.onSubscribe(var2);
      (new cj.a(var1, this.b, var2, this.a)).a();
   }

   static final class a extends AtomicInteger implements io.reactivex.t {
      final io.reactivex.t a;
      final io.reactivex.d.a.k b;
      final io.reactivex.r c;
      final io.reactivex.c.e d;

      a(io.reactivex.t var1, io.reactivex.c.e var2, io.reactivex.d.a.k var3, io.reactivex.r var4) {
         this.a = var1;
         this.b = var3;
         this.c = var4;
         this.d = var2;
      }

      void a() {
         if(this.getAndIncrement() == 0) {
            int var1 = 1;

            int var2;
            do {
               this.c.subscribe(this);
               var2 = this.addAndGet(-var1);
               var1 = var2;
            } while(var2 != 0);
         }

      }

      public void onComplete() {
         boolean var1;
         try {
            var1 = this.d.a();
         } catch (Throwable var3) {
            io.reactivex.exceptions.a.b(var3);
            this.a.onError(var3);
            return;
         }

         if(var1) {
            this.a.onComplete();
         } else {
            this.a();
         }

      }

      public void onError(Throwable var1) {
         this.a.onError(var1);
      }

      public void onNext(Object var1) {
         this.a.onNext(var1);
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         this.b.b(var1);
      }
   }
}
