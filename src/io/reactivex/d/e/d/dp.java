package io.reactivex.d.e.d;

import java.util.concurrent.TimeUnit;

public final class dp extends a {
   final io.reactivex.u b;
   final TimeUnit c;

   public dp(io.reactivex.r var1, TimeUnit var2, io.reactivex.u var3) {
      super(var1);
      this.b = var3;
      this.c = var2;
   }

   public void subscribeActual(io.reactivex.t var1) {
      this.a.subscribe(new dp.a(var1, this.c, this.b));
   }

   static final class a implements io.reactivex.b.b, io.reactivex.t {
      final io.reactivex.t a;
      final TimeUnit b;
      final io.reactivex.u c;
      long d;
      io.reactivex.b.b e;

      a(io.reactivex.t var1, TimeUnit var2, io.reactivex.u var3) {
         this.a = var1;
         this.c = var3;
         this.b = var2;
      }

      public void dispose() {
         this.e.dispose();
      }

      public boolean isDisposed() {
         return this.e.isDisposed();
      }

      public void onComplete() {
         this.a.onComplete();
      }

      public void onError(Throwable var1) {
         this.a.onError(var1);
      }

      public void onNext(Object var1) {
         long var2 = this.c.a(this.b);
         long var4 = this.d;
         this.d = var2;
         this.a.onNext(new io.reactivex.h.b(var1, var2 - var4, this.b));
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.e, var1)) {
            this.e = var1;
            this.d = this.c.a(this.b);
            this.a.onSubscribe(this);
         }

      }
   }
}
