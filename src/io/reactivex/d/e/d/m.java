package io.reactivex.d.e.d;

import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Iterator;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicBoolean;

public final class m extends a {
   final int b;
   final int c;
   final Callable d;

   public m(io.reactivex.r var1, int var2, int var3, Callable var4) {
      super(var1);
      this.b = var2;
      this.c = var3;
      this.d = var4;
   }

   protected void subscribeActual(io.reactivex.t var1) {
      if(this.c == this.b) {
         m.a var2 = new m.a(var1, this.b, this.d);
         if(var2.a()) {
            this.a.subscribe(var2);
         }
      } else {
         this.a.subscribe(new m.b(var1, this.b, this.c, this.d));
      }

   }

   static final class a implements io.reactivex.b.b, io.reactivex.t {
      final io.reactivex.t a;
      final int b;
      final Callable c;
      Collection d;
      int e;
      io.reactivex.b.b f;

      a(io.reactivex.t var1, int var2, Callable var3) {
         this.a = var1;
         this.b = var2;
         this.c = var3;
      }

      boolean a() {
         boolean var1;
         Collection var2;
         try {
            var2 = (Collection)io.reactivex.d.b.b.a(this.c.call(), "Empty buffer supplied");
         } catch (Throwable var3) {
            io.reactivex.exceptions.a.b(var3);
            this.d = null;
            if(this.f == null) {
               io.reactivex.d.a.e.a(var3, this.a);
            } else {
               this.f.dispose();
               this.a.onError(var3);
            }

            var1 = false;
            return var1;
         }

         this.d = var2;
         var1 = true;
         return var1;
      }

      public void dispose() {
         this.f.dispose();
      }

      public boolean isDisposed() {
         return this.f.isDisposed();
      }

      public void onComplete() {
         Collection var1 = this.d;
         this.d = null;
         if(var1 != null && !var1.isEmpty()) {
            this.a.onNext(var1);
         }

         this.a.onComplete();
      }

      public void onError(Throwable var1) {
         this.d = null;
         this.a.onError(var1);
      }

      public void onNext(Object var1) {
         Collection var3 = this.d;
         if(var3 != null) {
            var3.add(var1);
            int var2 = this.e + 1;
            this.e = var2;
            if(var2 >= this.b) {
               this.a.onNext(var3);
               this.e = 0;
               this.a();
            }
         }

      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.f, var1)) {
            this.f = var1;
            this.a.onSubscribe(this);
         }

      }
   }

   static final class b extends AtomicBoolean implements io.reactivex.b.b, io.reactivex.t {
      final io.reactivex.t a;
      final int b;
      final int c;
      final Callable d;
      io.reactivex.b.b e;
      final ArrayDeque f;
      long g;

      b(io.reactivex.t var1, int var2, int var3, Callable var4) {
         this.a = var1;
         this.b = var2;
         this.c = var3;
         this.d = var4;
         this.f = new ArrayDeque();
      }

      public void dispose() {
         this.e.dispose();
      }

      public boolean isDisposed() {
         return this.e.isDisposed();
      }

      public void onComplete() {
         while(!this.f.isEmpty()) {
            this.a.onNext(this.f.poll());
         }

         this.a.onComplete();
      }

      public void onError(Throwable var1) {
         this.f.clear();
         this.a.onError(var1);
      }

      public void onNext(Object var1) {
         long var2 = this.g;
         this.g = 1L + var2;
         if(var2 % (long)this.c == 0L) {
            Collection var4;
            try {
               var4 = (Collection)io.reactivex.d.b.b.a(this.d.call(), "The bufferSupplier returned a null collection. Null values are generally not allowed in 2.x operators and sources.");
            } catch (Throwable var6) {
               this.f.clear();
               this.e.dispose();
               this.a.onError(var6);
               return;
            }

            this.f.offer(var4);
         }

         Iterator var7 = this.f.iterator();

         while(var7.hasNext()) {
            Collection var5 = (Collection)var7.next();
            var5.add(var1);
            if(this.b <= var5.size()) {
               var7.remove();
               this.a.onNext(var5);
            }
         }

      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.e, var1)) {
            this.e = var1;
            this.a.onSubscribe(this);
         }

      }
   }
}
