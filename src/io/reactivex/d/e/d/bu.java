package io.reactivex.d.e.d;

import io.reactivex.exceptions.CompositeException;
import java.util.concurrent.Callable;

public final class bu extends a {
   final io.reactivex.c.h b;
   final io.reactivex.c.h c;
   final Callable d;

   public bu(io.reactivex.r var1, io.reactivex.c.h var2, io.reactivex.c.h var3, Callable var4) {
      super(var1);
      this.b = var2;
      this.c = var3;
      this.d = var4;
   }

   public void subscribeActual(io.reactivex.t var1) {
      this.a.subscribe(new bu.a(var1, this.b, this.c, this.d));
   }

   static final class a implements io.reactivex.b.b, io.reactivex.t {
      final io.reactivex.t a;
      final io.reactivex.c.h b;
      final io.reactivex.c.h c;
      final Callable d;
      io.reactivex.b.b e;

      a(io.reactivex.t var1, io.reactivex.c.h var2, io.reactivex.c.h var3, Callable var4) {
         this.a = var1;
         this.b = var2;
         this.c = var3;
         this.d = var4;
      }

      public void dispose() {
         this.e.dispose();
      }

      public boolean isDisposed() {
         return this.e.isDisposed();
      }

      public void onComplete() {
         io.reactivex.r var1;
         try {
            var1 = (io.reactivex.r)io.reactivex.d.b.b.a(this.d.call(), "The onComplete ObservableSource returned is null");
         } catch (Throwable var2) {
            io.reactivex.exceptions.a.b(var2);
            this.a.onError(var2);
            return;
         }

         this.a.onNext(var1);
         this.a.onComplete();
      }

      public void onError(Throwable var1) {
         io.reactivex.r var2;
         try {
            var2 = (io.reactivex.r)io.reactivex.d.b.b.a(this.c.a(var1), "The onError ObservableSource returned is null");
         } catch (Throwable var3) {
            io.reactivex.exceptions.a.b(var3);
            this.a.onError(new CompositeException(new Throwable[]{var1, var3}));
            return;
         }

         this.a.onNext(var2);
         this.a.onComplete();
      }

      public void onNext(Object var1) {
         io.reactivex.r var3;
         try {
            var3 = (io.reactivex.r)io.reactivex.d.b.b.a(this.b.a(var1), "The onNext ObservableSource returned is null");
         } catch (Throwable var2) {
            io.reactivex.exceptions.a.b(var2);
            this.a.onError(var2);
            return;
         }

         this.a.onNext(var3);
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.e, var1)) {
            this.e = var1;
            this.a.onSubscribe(this);
         }

      }
   }
}
