package io.reactivex.d.e.d;

public final class bk extends io.reactivex.b implements io.reactivex.d.c.c {
   final io.reactivex.r a;

   public bk(io.reactivex.r var1) {
      this.a = var1;
   }

   public void b(io.reactivex.c var1) {
      this.a.subscribe(new bk.a(var1));
   }

   public io.reactivex.n q_() {
      return io.reactivex.g.a.a((io.reactivex.n)(new bj(this.a)));
   }

   static final class a implements io.reactivex.b.b, io.reactivex.t {
      final io.reactivex.c a;
      io.reactivex.b.b b;

      a(io.reactivex.c var1) {
         this.a = var1;
      }

      public void dispose() {
         this.b.dispose();
      }

      public boolean isDisposed() {
         return this.b.isDisposed();
      }

      public void onComplete() {
         this.a.onComplete();
      }

      public void onError(Throwable var1) {
         this.a.onError(var1);
      }

      public void onNext(Object var1) {
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         this.b = var1;
         this.a.onSubscribe(this);
      }
   }
}
