package io.reactivex.d.e.d;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

public final class aa extends a {
   final io.reactivex.c.h b;

   public aa(io.reactivex.r var1, io.reactivex.c.h var2) {
      super(var1);
      this.b = var2;
   }

   public void subscribeActual(io.reactivex.t var1) {
      this.a.subscribe(new aa.a(new io.reactivex.f.e(var1), this.b));
   }

   static final class a implements io.reactivex.b.b, io.reactivex.t {
      final io.reactivex.t a;
      final io.reactivex.c.h b;
      io.reactivex.b.b c;
      final AtomicReference d = new AtomicReference();
      volatile long e;
      boolean f;

      a(io.reactivex.t var1, io.reactivex.c.h var2) {
         this.a = var1;
         this.b = var2;
      }

      void a(long var1, Object var3) {
         if(var1 == this.e) {
            this.a.onNext(var3);
         }

      }

      public void dispose() {
         this.c.dispose();
         io.reactivex.d.a.d.a(this.d);
      }

      public boolean isDisposed() {
         return this.c.isDisposed();
      }

      public void onComplete() {
         if(!this.f) {
            this.f = true;
            io.reactivex.b.b var1 = (io.reactivex.b.b)this.d.get();
            if(var1 != io.reactivex.d.a.d.a) {
               ((aa.a)var1).a();
               io.reactivex.d.a.d.a(this.d);
               this.a.onComplete();
            }
         }

      }

      public void onError(Throwable var1) {
         io.reactivex.d.a.d.a(this.d);
         this.a.onError(var1);
      }

      public void onNext(Object var1) {
         if(!this.f) {
            long var2 = 1L + this.e;
            this.e = var2;
            io.reactivex.b.b var4 = (io.reactivex.b.b)this.d.get();
            if(var4 != null) {
               var4.dispose();
            }

            io.reactivex.r var5;
            try {
               var5 = (io.reactivex.r)io.reactivex.d.b.b.a(this.b.a(var1), "The ObservableSource supplied is null");
            } catch (Throwable var6) {
               io.reactivex.exceptions.a.b(var6);
               this.dispose();
               this.a.onError(var6);
               return;
            }

            aa.a var7 = new aa.a(this, var2, var1);
            if(this.d.compareAndSet(var4, var7)) {
               var5.subscribe(var7);
            }
         }

      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.c, var1)) {
            this.c = var1;
            this.a.onSubscribe(this);
         }

      }
   }

   static final class a extends io.reactivex.f.c {
      final aa.a a;
      final long b;
      final Object c;
      boolean d;
      final AtomicBoolean e = new AtomicBoolean();

      a(aa.a var1, long var2, Object var4) {
         this.a = var1;
         this.b = var2;
         this.c = var4;
      }

      void a() {
         if(this.e.compareAndSet(false, true)) {
            this.a.a(this.b, this.c);
         }

      }

      public void onComplete() {
         if(!this.d) {
            this.d = true;
            this.a();
         }

      }

      public void onError(Throwable var1) {
         if(this.d) {
            io.reactivex.g.a.a(var1);
         } else {
            this.d = true;
            this.a.onError(var1);
         }

      }

      public void onNext(Object var1) {
         if(!this.d) {
            this.d = true;
            this.dispose();
            this.a();
         }

      }
   }
}
