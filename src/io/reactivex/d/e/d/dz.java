package io.reactivex.d.e.d;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

public final class dz extends a {
   final io.reactivex.r b;
   final io.reactivex.c.h c;
   final int d;

   public dz(io.reactivex.r var1, io.reactivex.r var2, io.reactivex.c.h var3, int var4) {
      super(var1);
      this.b = var2;
      this.c = var3;
      this.d = var4;
   }

   public void subscribeActual(io.reactivex.t var1) {
      this.a.subscribe(new dz.c(new io.reactivex.f.e(var1), this.b, this.c, this.d));
   }

   static final class a extends io.reactivex.f.c {
      final dz.c a;
      final io.reactivex.i.d b;
      boolean c;

      a(dz.c var1, io.reactivex.i.d var2) {
         this.a = var1;
         this.b = var2;
      }

      public void onComplete() {
         if(!this.c) {
            this.c = true;
            this.a.a(this);
         }

      }

      public void onError(Throwable var1) {
         if(this.c) {
            io.reactivex.g.a.a(var1);
         } else {
            this.c = true;
            this.a.a(var1);
         }

      }

      public void onNext(Object var1) {
         if(!this.c) {
            this.c = true;
            this.dispose();
            this.a.a(this);
         }

      }
   }

   static final class b extends io.reactivex.f.c {
      final dz.c a;

      b(dz.c var1) {
         this.a = var1;
      }

      public void onComplete() {
         this.a.onComplete();
      }

      public void onError(Throwable var1) {
         this.a.a(var1);
      }

      public void onNext(Object var1) {
         this.a.a(var1);
      }
   }

   static final class c extends io.reactivex.d.d.t implements io.reactivex.b.b {
      final io.reactivex.r g;
      final io.reactivex.c.h h;
      final int i;
      final io.reactivex.b.a j;
      io.reactivex.b.b k;
      final AtomicReference l = new AtomicReference();
      final List m;
      final AtomicLong n = new AtomicLong();

      c(io.reactivex.t var1, io.reactivex.r var2, io.reactivex.c.h var3, int var4) {
         super(var1, new io.reactivex.d.f.a());
         this.g = var2;
         this.h = var3;
         this.i = var4;
         this.j = new io.reactivex.b.a();
         this.m = new ArrayList();
         this.n.lazySet(1L);
      }

      void a(dz.a var1) {
         this.j.c(var1);
         this.b.a(new dz.d(var1.b, (Object)null));
         if(this.c()) {
            this.g();
         }

      }

      public void a(io.reactivex.t var1, Object var2) {
      }

      void a(Object var1) {
         this.b.a(new dz.d((io.reactivex.i.d)null, var1));
         if(this.c()) {
            this.g();
         }

      }

      void a(Throwable var1) {
         this.k.dispose();
         this.j.dispose();
         this.onError(var1);
      }

      public void dispose() {
         this.c = true;
      }

      void f() {
         this.j.dispose();
         io.reactivex.d.a.d.a(this.l);
      }

      void g() {
         io.reactivex.d.f.a var6 = (io.reactivex.d.f.a)this.b;
         io.reactivex.t var5 = this.a;
         List var4 = this.m;
         int var1 = 1;

         while(true) {
            boolean var3 = this.d;
            Object var8 = var6.n_();
            boolean var2;
            if(var8 == null) {
               var2 = true;
            } else {
               var2 = false;
            }

            if(var3 && var2) {
               this.f();
               Throwable var10 = this.e;
               if(var10 != null) {
                  Iterator var12 = var4.iterator();

                  while(var12.hasNext()) {
                     ((io.reactivex.i.d)var12.next()).onError(var10);
                  }
               } else {
                  Iterator var11 = var4.iterator();

                  while(var11.hasNext()) {
                     ((io.reactivex.i.d)var11.next()).onComplete();
                  }
               }

               var4.clear();
               break;
            }

            if(var2) {
               var1 = this.a(-var1);
               if(var1 != 0) {
                  continue;
               }
               break;
            } else if(var8 instanceof dz.d) {
               dz.d var13 = (dz.d)var8;
               if(var13.a != null) {
                  if(var4.remove(var13.a)) {
                     var13.a.onComplete();
                     if(this.n.decrementAndGet() == 0L) {
                        this.f();
                        break;
                     }
                  }
               } else if(!this.c) {
                  io.reactivex.i.d var15 = io.reactivex.i.d.a(this.i);
                  var4.add(var15);
                  var5.onNext(var15);

                  io.reactivex.r var14;
                  try {
                     var14 = (io.reactivex.r)io.reactivex.d.b.b.a(this.h.a(var13.b), "The ObservableSource supplied is null");
                  } catch (Throwable var9) {
                     io.reactivex.exceptions.a.b(var9);
                     this.c = true;
                     var5.onError(var9);
                     continue;
                  }

                  dz.a var16 = new dz.a(this, var15);
                  if(this.j.a((io.reactivex.b.b)var16)) {
                     this.n.getAndIncrement();
                     var14.subscribe(var16);
                  }
               }
            } else {
               Iterator var7 = var4.iterator();

               while(var7.hasNext()) {
                  ((io.reactivex.i.d)var7.next()).onNext(io.reactivex.d.j.n.e(var8));
               }
            }
         }

      }

      public boolean isDisposed() {
         return this.c;
      }

      public void onComplete() {
         if(!this.d) {
            this.d = true;
            if(this.c()) {
               this.g();
            }

            if(this.n.decrementAndGet() == 0L) {
               this.j.dispose();
            }

            this.a.onComplete();
         }

      }

      public void onError(Throwable var1) {
         if(this.d) {
            io.reactivex.g.a.a(var1);
         } else {
            this.e = var1;
            this.d = true;
            if(this.c()) {
               this.g();
            }

            if(this.n.decrementAndGet() == 0L) {
               this.j.dispose();
            }

            this.a.onError(var1);
         }

      }

      public void onNext(Object var1) {
         if(this.d()) {
            Iterator var2 = this.m.iterator();

            while(var2.hasNext()) {
               ((io.reactivex.i.d)var2.next()).onNext(var1);
            }

            if(this.a(-1) == 0) {
               return;
            }
         } else {
            this.b.a(io.reactivex.d.j.n.a(var1));
            if(!this.c()) {
               return;
            }
         }

         this.g();
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.k, var1)) {
            this.k = var1;
            this.a.onSubscribe(this);
            if(!this.c) {
               dz.b var2 = new dz.b(this);
               if(this.l.compareAndSet((Object)null, var2)) {
                  this.n.getAndIncrement();
                  this.g.subscribe(var2);
               }
            }
         }

      }
   }

   static final class d {
      final io.reactivex.i.d a;
      final Object b;

      d(io.reactivex.i.d var1, Object var2) {
         this.a = var1;
         this.b = var2;
      }
   }
}
