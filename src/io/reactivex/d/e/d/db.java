package io.reactivex.d.e.d;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public final class db extends a {
   final long b;
   final TimeUnit c;
   final io.reactivex.u d;
   final int e;
   final boolean f;

   public db(io.reactivex.r var1, long var2, TimeUnit var4, io.reactivex.u var5, int var6, boolean var7) {
      super(var1);
      this.b = var2;
      this.c = var4;
      this.d = var5;
      this.e = var6;
      this.f = var7;
   }

   public void subscribeActual(io.reactivex.t var1) {
      this.a.subscribe(new db.a(var1, this.b, this.c, this.d, this.e, this.f));
   }

   static final class a extends AtomicInteger implements io.reactivex.b.b, io.reactivex.t {
      final io.reactivex.t a;
      final long b;
      final TimeUnit c;
      final io.reactivex.u d;
      final io.reactivex.d.f.c e;
      final boolean f;
      io.reactivex.b.b g;
      volatile boolean h;
      volatile boolean i;
      Throwable j;

      a(io.reactivex.t var1, long var2, TimeUnit var4, io.reactivex.u var5, int var6, boolean var7) {
         this.a = var1;
         this.b = var2;
         this.c = var4;
         this.d = var5;
         this.e = new io.reactivex.d.f.c(var6);
         this.f = var7;
      }

      void a() {
         if(this.getAndIncrement() == 0) {
            io.reactivex.t var10 = this.a;
            io.reactivex.d.f.c var13 = this.e;
            boolean var4 = this.f;
            TimeUnit var12 = this.c;
            io.reactivex.u var11 = this.d;
            long var8 = this.b;
            int var2 = 1;

            while(!this.h) {
               boolean var5 = this.i;
               Long var14 = (Long)var13.d();
               boolean var1;
               if(var14 == null) {
                  var1 = true;
               } else {
                  var1 = false;
               }

               long var6 = var11.a(var12);
               boolean var3 = var1;
               if(!var1) {
                  var3 = var1;
                  if(var14.longValue() > var6 - var8) {
                     var3 = true;
                  }
               }

               if(var5) {
                  if(var4) {
                     if(var3) {
                        Throwable var15 = this.j;
                        if(var15 != null) {
                           var10.onError(var15);
                        } else {
                           var10.onComplete();
                        }

                        return;
                     }
                  } else {
                     Throwable var16 = this.j;
                     if(var16 != null) {
                        this.e.c();
                        var10.onError(var16);
                        return;
                     }

                     if(var3) {
                        var10.onComplete();
                        return;
                     }
                  }
               }

               if(var3) {
                  var2 = this.addAndGet(-var2);
                  if(var2 == 0) {
                     return;
                  }
               } else {
                  var13.n_();
                  var10.onNext(var13.n_());
               }
            }

            this.e.c();
         }

      }

      public void dispose() {
         if(!this.h) {
            this.h = true;
            this.g.dispose();
            if(this.getAndIncrement() == 0) {
               this.e.c();
            }
         }

      }

      public boolean isDisposed() {
         return this.h;
      }

      public void onComplete() {
         this.i = true;
         this.a();
      }

      public void onError(Throwable var1) {
         this.j = var1;
         this.i = true;
         this.a();
      }

      public void onNext(Object var1) {
         this.e.a((Object)Long.valueOf(this.d.a(this.c)), (Object)var1);
         this.a();
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.g, var1)) {
            this.g = var1;
            this.a.onSubscribe(this);
         }

      }
   }
}
