package io.reactivex.d.e.d;

public final class cz extends a {
   final long b;

   public cz(io.reactivex.r var1, long var2) {
      super(var1);
      this.b = var2;
   }

   public void subscribeActual(io.reactivex.t var1) {
      this.a.subscribe(new cz.a(var1, this.b));
   }

   static final class a implements io.reactivex.b.b, io.reactivex.t {
      final io.reactivex.t a;
      long b;
      io.reactivex.b.b c;

      a(io.reactivex.t var1, long var2) {
         this.a = var1;
         this.b = var2;
      }

      public void dispose() {
         this.c.dispose();
      }

      public boolean isDisposed() {
         return this.c.isDisposed();
      }

      public void onComplete() {
         this.a.onComplete();
      }

      public void onError(Throwable var1) {
         this.a.onError(var1);
      }

      public void onNext(Object var1) {
         if(this.b != 0L) {
            --this.b;
         } else {
            this.a.onNext(var1);
         }

      }

      public void onSubscribe(io.reactivex.b.b var1) {
         this.c = var1;
         this.a.onSubscribe(this);
      }
   }
}
