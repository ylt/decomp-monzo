package io.reactivex.d.e.d;

public final class x extends a {
   public x(io.reactivex.r var1) {
      super(var1);
   }

   public void subscribeActual(io.reactivex.t var1) {
      this.a.subscribe(new x.a(var1));
   }

   static final class a implements io.reactivex.b.b, io.reactivex.t {
      final io.reactivex.t a;
      io.reactivex.b.b b;
      long c;

      a(io.reactivex.t var1) {
         this.a = var1;
      }

      public void dispose() {
         this.b.dispose();
      }

      public boolean isDisposed() {
         return this.b.isDisposed();
      }

      public void onComplete() {
         this.a.onNext(Long.valueOf(this.c));
         this.a.onComplete();
      }

      public void onError(Throwable var1) {
         this.a.onError(var1);
      }

      public void onNext(Object var1) {
         ++this.c;
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.b, var1)) {
            this.b = var1;
            this.a.onSubscribe(this);
         }

      }
   }
}
