package io.reactivex.d.e.d;

import io.reactivex.exceptions.CompositeException;

public final class al extends a {
   final io.reactivex.c.g b;
   final io.reactivex.c.g c;
   final io.reactivex.c.a d;
   final io.reactivex.c.a e;

   public al(io.reactivex.r var1, io.reactivex.c.g var2, io.reactivex.c.g var3, io.reactivex.c.a var4, io.reactivex.c.a var5) {
      super(var1);
      this.b = var2;
      this.c = var3;
      this.d = var4;
      this.e = var5;
   }

   public void subscribeActual(io.reactivex.t var1) {
      this.a.subscribe(new al.a(var1, this.b, this.c, this.d, this.e));
   }

   static final class a implements io.reactivex.b.b, io.reactivex.t {
      final io.reactivex.t a;
      final io.reactivex.c.g b;
      final io.reactivex.c.g c;
      final io.reactivex.c.a d;
      final io.reactivex.c.a e;
      io.reactivex.b.b f;
      boolean g;

      a(io.reactivex.t var1, io.reactivex.c.g var2, io.reactivex.c.g var3, io.reactivex.c.a var4, io.reactivex.c.a var5) {
         this.a = var1;
         this.b = var2;
         this.c = var3;
         this.d = var4;
         this.e = var5;
      }

      public void dispose() {
         this.f.dispose();
      }

      public boolean isDisposed() {
         return this.f.isDisposed();
      }

      public void onComplete() {
         if(!this.g) {
            try {
               this.d.a();
            } catch (Throwable var3) {
               io.reactivex.exceptions.a.b(var3);
               this.onError(var3);
               return;
            }

            this.g = true;
            this.a.onComplete();

            try {
               this.e.a();
            } catch (Throwable var2) {
               io.reactivex.exceptions.a.b(var2);
               io.reactivex.g.a.a(var2);
            }
         }

      }

      public void onError(Throwable var1) {
         if(this.g) {
            io.reactivex.g.a.a((Throwable)var1);
         } else {
            this.g = true;

            try {
               this.c.a(var1);
            } catch (Throwable var4) {
               io.reactivex.exceptions.a.b(var4);
               var1 = new CompositeException(new Throwable[]{(Throwable)var1, var4});
            }

            this.a.onError((Throwable)var1);

            try {
               this.e.a();
            } catch (Throwable var3) {
               io.reactivex.exceptions.a.b(var3);
               io.reactivex.g.a.a(var3);
            }
         }

      }

      public void onNext(Object var1) {
         if(!this.g) {
            try {
               this.b.a(var1);
            } catch (Throwable var2) {
               io.reactivex.exceptions.a.b(var2);
               this.f.dispose();
               this.onError(var2);
               return;
            }

            this.a.onNext(var1);
         }

      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.f, var1)) {
            this.f = var1;
            this.a.onSubscribe(this);
         }

      }
   }
}
