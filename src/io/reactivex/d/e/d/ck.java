package io.reactivex.d.e.d;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public final class ck extends a {
   final io.reactivex.c.h b;

   public ck(io.reactivex.r var1, io.reactivex.c.h var2) {
      super(var1);
      this.b = var2;
   }

   protected void subscribeActual(io.reactivex.t var1) {
      io.reactivex.i.c var3 = io.reactivex.i.a.a().b();

      io.reactivex.r var2;
      try {
         var2 = (io.reactivex.r)io.reactivex.d.b.b.a(this.b.a(var3), "The handler returned a null ObservableSource");
      } catch (Throwable var4) {
         io.reactivex.exceptions.a.b(var4);
         io.reactivex.d.a.e.a(var4, var1);
         return;
      }

      ck.a var5 = new ck.a(var1, var3, this.a);
      var1.onSubscribe(var5);
      var2.subscribe(var5.e);
      var5.c();
   }

   static final class a extends AtomicInteger implements io.reactivex.b.b, io.reactivex.t {
      final io.reactivex.t a;
      final AtomicInteger b;
      final io.reactivex.d.j.c c;
      final io.reactivex.i.c d;
      final ck.a e;
      final AtomicReference f;
      final io.reactivex.r g;
      volatile boolean h;

      a(io.reactivex.t var1, io.reactivex.i.c var2, io.reactivex.r var3) {
         this.a = var1;
         this.d = var2;
         this.g = var3;
         this.b = new AtomicInteger();
         this.c = new io.reactivex.d.j.c();
         this.e = new ck.a();
         this.f = new AtomicReference();
      }

      void a() {
         this.c();
      }

      void a(Throwable var1) {
         io.reactivex.d.a.d.a(this.f);
         io.reactivex.d.j.k.a((io.reactivex.t)this.a, (Throwable)var1, this, this.c);
      }

      void b() {
         io.reactivex.d.a.d.a(this.f);
         io.reactivex.d.j.k.a((io.reactivex.t)this.a, this, this.c);
      }

      void c() {
         if(this.b.getAndIncrement() == 0) {
            while(!this.isDisposed()) {
               if(!this.h) {
                  this.h = true;
                  this.g.subscribe(this);
               }

               if(this.b.decrementAndGet() == 0) {
                  break;
               }
            }
         }

      }

      public void dispose() {
         io.reactivex.d.a.d.a(this.f);
         io.reactivex.d.a.d.a((AtomicReference)this.e);
      }

      public boolean isDisposed() {
         return io.reactivex.d.a.d.a((io.reactivex.b.b)this.f.get());
      }

      public void onComplete() {
         this.h = false;
         this.d.onNext(Integer.valueOf(0));
      }

      public void onError(Throwable var1) {
         io.reactivex.d.a.d.a((AtomicReference)this.e);
         io.reactivex.d.j.k.a((io.reactivex.t)this.a, (Throwable)var1, this, this.c);
      }

      public void onNext(Object var1) {
         io.reactivex.d.j.k.a((io.reactivex.t)this.a, (Object)var1, this, this.c);
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         io.reactivex.d.a.d.c(this.f, var1);
      }
   }

   final class a extends AtomicReference implements io.reactivex.t {
      public void onComplete() {
         ck.this.b();
      }

      public void onError(Throwable var1) {
         ck.this.a(var1);
      }

      public void onNext(Object var1) {
         ck.this.a();
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         io.reactivex.d.a.d.b(this, var1);
      }
   }
}
