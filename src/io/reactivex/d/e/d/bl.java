package io.reactivex.d.e.d;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

public final class bl {
   public static io.reactivex.c.c a(io.reactivex.c.b var0) {
      return new bl.m(var0);
   }

   public static io.reactivex.c.c a(io.reactivex.c.g var0) {
      return new bl.n(var0);
   }

   public static io.reactivex.c.g a(io.reactivex.t var0) {
      return new bl.j(var0);
   }

   public static io.reactivex.c.h a(io.reactivex.c.h var0) {
      return new bl.f(var0);
   }

   public static io.reactivex.c.h a(io.reactivex.c.h var0, io.reactivex.c.c var1) {
      return new bl.e(var1, var0);
   }

   public static io.reactivex.c.h a(io.reactivex.c.h var0, io.reactivex.u var1) {
      return new bl.l(var0, var1);
   }

   public static io.reactivex.n a(io.reactivex.n var0, io.reactivex.c.h var1) {
      return var0.switchMap(d(var1), 1);
   }

   public static Callable a(io.reactivex.n var0) {
      return new bl.k(var0);
   }

   public static Callable a(io.reactivex.n var0, int var1) {
      return new bl.a(var0, var1);
   }

   public static Callable a(io.reactivex.n var0, int var1, long var2, TimeUnit var4, io.reactivex.u var5) {
      return new bl.b(var0, var1, var2, var4, var5);
   }

   public static Callable a(io.reactivex.n var0, long var1, TimeUnit var3, io.reactivex.u var4) {
      return new bl.o(var0, var1, var3, var4);
   }

   public static io.reactivex.c.g b(io.reactivex.t var0) {
      return new bl.i(var0);
   }

   public static io.reactivex.c.h b(io.reactivex.c.h var0) {
      return new bl.c(var0);
   }

   public static io.reactivex.n b(io.reactivex.n var0, io.reactivex.c.h var1) {
      return var0.switchMapDelayError(d(var1), 1);
   }

   public static io.reactivex.c.a c(io.reactivex.t var0) {
      return new bl.h(var0);
   }

   public static io.reactivex.c.h c(io.reactivex.c.h var0) {
      return new bl.p(var0);
   }

   private static io.reactivex.c.h d(io.reactivex.c.h var0) {
      io.reactivex.d.b.b.a(var0, (String)"mapper is null");
      return new bl.g(var0);
   }

   static final class a implements Callable {
      private final io.reactivex.n a;
      private final int b;

      a(io.reactivex.n var1, int var2) {
         this.a = var1;
         this.b = var2;
      }

      public io.reactivex.e.a a() {
         return this.a.replay(this.b);
      }

      // $FF: synthetic method
      public Object call() throws Exception {
         return this.a();
      }
   }

   static final class b implements Callable {
      private final io.reactivex.n a;
      private final int b;
      private final long c;
      private final TimeUnit d;
      private final io.reactivex.u e;

      b(io.reactivex.n var1, int var2, long var3, TimeUnit var5, io.reactivex.u var6) {
         this.a = var1;
         this.b = var2;
         this.c = var3;
         this.d = var5;
         this.e = var6;
      }

      public io.reactivex.e.a a() {
         return this.a.replay(this.b, this.c, this.d, this.e);
      }

      // $FF: synthetic method
      public Object call() throws Exception {
         return this.a();
      }
   }

   static final class c implements io.reactivex.c.h {
      private final io.reactivex.c.h a;

      c(io.reactivex.c.h var1) {
         this.a = var1;
      }

      // $FF: synthetic method
      public Object a(Object var1) throws Exception {
         return this.b(var1);
      }

      public io.reactivex.r b(Object var1) throws Exception {
         return new bc((Iterable)io.reactivex.d.b.b.a(this.a.a(var1), "The mapper returned a null Iterable"));
      }
   }

   static final class d implements io.reactivex.c.h {
      private final io.reactivex.c.c a;
      private final Object b;

      d(io.reactivex.c.c var1, Object var2) {
         this.a = var1;
         this.b = var2;
      }

      public Object a(Object var1) throws Exception {
         return this.a.a(this.b, var1);
      }
   }

   static final class e implements io.reactivex.c.h {
      private final io.reactivex.c.c a;
      private final io.reactivex.c.h b;

      e(io.reactivex.c.c var1, io.reactivex.c.h var2) {
         this.a = var1;
         this.b = var2;
      }

      // $FF: synthetic method
      public Object a(Object var1) throws Exception {
         return this.b(var1);
      }

      public io.reactivex.r b(Object var1) throws Exception {
         return new bt((io.reactivex.r)io.reactivex.d.b.b.a(this.b.a(var1), "The mapper returned a null ObservableSource"), new bl.d(this.a, var1));
      }
   }

   static final class f implements io.reactivex.c.h {
      final io.reactivex.c.h a;

      f(io.reactivex.c.h var1) {
         this.a = var1;
      }

      // $FF: synthetic method
      public Object a(Object var1) throws Exception {
         return this.b(var1);
      }

      public io.reactivex.r b(Object var1) throws Exception {
         return (new dh((io.reactivex.r)io.reactivex.d.b.b.a(this.a.a(var1), "The itemDelay returned a null ObservableSource"), 1L)).map(io.reactivex.d.b.a.b(var1)).defaultIfEmpty(var1);
      }
   }

   static final class g implements io.reactivex.c.h {
      final io.reactivex.c.h a;

      g(io.reactivex.c.h var1) {
         this.a = var1;
      }

      // $FF: synthetic method
      public Object a(Object var1) throws Exception {
         return this.b(var1);
      }

      public io.reactivex.n b(Object var1) throws Exception {
         return io.reactivex.g.a.a((io.reactivex.n)(new io.reactivex.d.e.e.v((io.reactivex.z)io.reactivex.d.b.b.a(this.a.a(var1), "The mapper returned a null SingleSource"))));
      }
   }

   static final class h implements io.reactivex.c.a {
      final io.reactivex.t a;

      h(io.reactivex.t var1) {
         this.a = var1;
      }

      public void a() throws Exception {
         this.a.onComplete();
      }
   }

   static final class i implements io.reactivex.c.g {
      final io.reactivex.t a;

      i(io.reactivex.t var1) {
         this.a = var1;
      }

      public void a(Throwable var1) throws Exception {
         this.a.onError(var1);
      }
   }

   static final class j implements io.reactivex.c.g {
      final io.reactivex.t a;

      j(io.reactivex.t var1) {
         this.a = var1;
      }

      public void a(Object var1) throws Exception {
         this.a.onNext(var1);
      }
   }

   static final class k implements Callable {
      private final io.reactivex.n a;

      k(io.reactivex.n var1) {
         this.a = var1;
      }

      public io.reactivex.e.a a() {
         return this.a.replay();
      }

      // $FF: synthetic method
      public Object call() throws Exception {
         return this.a();
      }
   }

   static final class l implements io.reactivex.c.h {
      private final io.reactivex.c.h a;
      private final io.reactivex.u b;

      l(io.reactivex.c.h var1, io.reactivex.u var2) {
         this.a = var1;
         this.b = var2;
      }

      public io.reactivex.r a(io.reactivex.n var1) throws Exception {
         return io.reactivex.n.wrap((io.reactivex.r)io.reactivex.d.b.b.a(this.a.a(var1), "The selector returned a null ObservableSource")).observeOn(this.b);
      }
   }

   static final class m implements io.reactivex.c.c {
      final io.reactivex.c.b a;

      m(io.reactivex.c.b var1) {
         this.a = var1;
      }

      public Object a(Object var1, io.reactivex.e var2) throws Exception {
         this.a.a(var1, var2);
         return var1;
      }
   }

   static final class n implements io.reactivex.c.c {
      final io.reactivex.c.g a;

      n(io.reactivex.c.g var1) {
         this.a = var1;
      }

      public Object a(Object var1, io.reactivex.e var2) throws Exception {
         this.a.a(var2);
         return var1;
      }
   }

   static final class o implements Callable {
      private final io.reactivex.n a;
      private final long b;
      private final TimeUnit c;
      private final io.reactivex.u d;

      o(io.reactivex.n var1, long var2, TimeUnit var4, io.reactivex.u var5) {
         this.a = var1;
         this.b = var2;
         this.c = var4;
         this.d = var5;
      }

      public io.reactivex.e.a a() {
         return this.a.replay(this.b, this.c, this.d);
      }

      // $FF: synthetic method
      public Object call() throws Exception {
         return this.a();
      }
   }

   static final class p implements io.reactivex.c.h {
      private final io.reactivex.c.h a;

      p(io.reactivex.c.h var1) {
         this.a = var1;
      }

      public io.reactivex.r a(List var1) {
         return io.reactivex.n.zipIterable(var1, this.a, false, io.reactivex.n.bufferSize());
      }
   }
}
