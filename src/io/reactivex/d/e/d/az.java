package io.reactivex.d.e.d;

public final class az extends io.reactivex.n {
   final Object[] a;

   public az(Object[] var1) {
      this.a = var1;
   }

   public void subscribeActual(io.reactivex.t var1) {
      az.a var2 = new az.a(var1, this.a);
      var1.onSubscribe(var2);
      if(!var2.d) {
         var2.d();
      }

   }

   static final class a extends io.reactivex.d.d.c {
      final io.reactivex.t a;
      final Object[] b;
      int c;
      boolean d;
      volatile boolean e;

      a(io.reactivex.t var1, Object[] var2) {
         this.a = var1;
         this.b = var2;
      }

      public int a(int var1) {
         byte var2 = 1;
         byte var3;
         if((var1 & 1) != 0) {
            this.d = true;
            var3 = var2;
         } else {
            var3 = 0;
         }

         return var3;
      }

      public boolean b() {
         boolean var1;
         if(this.c == this.b.length) {
            var1 = true;
         } else {
            var1 = false;
         }

         return var1;
      }

      public void c() {
         this.c = this.b.length;
      }

      void d() {
         Object[] var3 = this.b;
         int var2 = var3.length;
         int var1 = 0;

         while(true) {
            if(var1 >= var2 || this.isDisposed()) {
               if(!this.isDisposed()) {
                  this.a.onComplete();
               }
               break;
            }

            Object var4 = var3[var1];
            if(var4 == null) {
               this.a.onError(new NullPointerException("The " + var1 + "th element is null"));
               break;
            }

            this.a.onNext(var4);
            ++var1;
         }

      }

      public void dispose() {
         this.e = true;
      }

      public boolean isDisposed() {
         return this.e;
      }

      public Object n_() {
         int var1 = this.c;
         Object[] var2 = this.b;
         Object var3;
         if(var1 != var2.length) {
            this.c = var1 + 1;
            var3 = io.reactivex.d.b.b.a(var2[var1], "The array element is null");
         } else {
            var3 = null;
         }

         return var3;
      }
   }
}
