package io.reactivex.d.e.d;

public final class ao extends io.reactivex.h implements io.reactivex.d.c.c {
   final io.reactivex.r a;
   final long b;

   public ao(io.reactivex.r var1, long var2) {
      this.a = var1;
      this.b = var2;
   }

   public void b(io.reactivex.j var1) {
      this.a.subscribe(new ao.a(var1, this.b));
   }

   public io.reactivex.n q_() {
      return io.reactivex.g.a.a((io.reactivex.n)(new an(this.a, this.b, (Object)null, false)));
   }

   static final class a implements io.reactivex.b.b, io.reactivex.t {
      final io.reactivex.j a;
      final long b;
      io.reactivex.b.b c;
      long d;
      boolean e;

      a(io.reactivex.j var1, long var2) {
         this.a = var1;
         this.b = var2;
      }

      public void dispose() {
         this.c.dispose();
      }

      public boolean isDisposed() {
         return this.c.isDisposed();
      }

      public void onComplete() {
         if(!this.e) {
            this.e = true;
            this.a.onComplete();
         }

      }

      public void onError(Throwable var1) {
         if(this.e) {
            io.reactivex.g.a.a(var1);
         } else {
            this.e = true;
            this.a.onError(var1);
         }

      }

      public void onNext(Object var1) {
         if(!this.e) {
            long var2 = this.d;
            if(var2 == this.b) {
               this.e = true;
               this.c.dispose();
               this.a.a_(var1);
            } else {
               this.d = var2 + 1L;
            }
         }

      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.c, var1)) {
            this.c = var1;
            this.a.onSubscribe(this);
         }

      }
   }
}
