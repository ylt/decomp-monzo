package io.reactivex.d.e.d;

import java.util.Collection;
import java.util.concurrent.Callable;

public final class ah extends a {
   final io.reactivex.c.h b;
   final Callable c;

   public ah(io.reactivex.r var1, io.reactivex.c.h var2, Callable var3) {
      super(var1);
      this.b = var2;
      this.c = var3;
   }

   protected void subscribeActual(io.reactivex.t var1) {
      Collection var2;
      try {
         var2 = (Collection)io.reactivex.d.b.b.a(this.c.call(), "The collectionSupplier returned a null collection. Null values are generally not allowed in 2.x operators and sources.");
      } catch (Throwable var3) {
         io.reactivex.exceptions.a.b(var3);
         io.reactivex.d.a.e.a(var3, var1);
         return;
      }

      this.a.subscribe(new ah.a(var1, this.b, var2));
   }

   static final class a extends io.reactivex.d.d.a {
      final Collection f;
      final io.reactivex.c.h g;

      a(io.reactivex.t var1, io.reactivex.c.h var2, Collection var3) {
         super(var1);
         this.g = var2;
         this.f = var3;
      }

      public int a(int var1) {
         return this.b(var1);
      }

      public void c() {
         this.f.clear();
         super.c();
      }

      public Object n_() throws Exception {
         Object var1;
         do {
            var1 = this.c.n_();
         } while(var1 != null && !this.f.add(io.reactivex.d.b.b.a(this.g.a(var1), "The keySelector returned a null key")));

         return var1;
      }

      public void onComplete() {
         if(!this.d) {
            this.d = true;
            this.f.clear();
            this.a.onComplete();
         }

      }

      public void onError(Throwable var1) {
         if(this.d) {
            io.reactivex.g.a.a(var1);
         } else {
            this.d = true;
            this.f.clear();
            this.a.onError(var1);
         }

      }

      public void onNext(Object var1) {
         if(!this.d) {
            if(this.e == 0) {
               boolean var2;
               try {
                  Object var3 = io.reactivex.d.b.b.a(this.g.a(var1), "The keySelector returned a null key");
                  var2 = this.f.add(var3);
               } catch (Throwable var4) {
                  this.a(var4);
                  return;
               }

               if(var2) {
                  this.a.onNext(var1);
               }
            } else {
               this.a.onNext((Object)null);
            }
         }

      }
   }
}
