package io.reactivex.d.e.d;

public final class ce extends io.reactivex.h {
   final io.reactivex.r a;
   final io.reactivex.c.c b;

   public ce(io.reactivex.r var1, io.reactivex.c.c var2) {
      this.a = var1;
      this.b = var2;
   }

   protected void b(io.reactivex.j var1) {
      this.a.subscribe(new ce.a(var1, this.b));
   }

   static final class a implements io.reactivex.b.b, io.reactivex.t {
      final io.reactivex.j a;
      final io.reactivex.c.c b;
      boolean c;
      Object d;
      io.reactivex.b.b e;

      a(io.reactivex.j var1, io.reactivex.c.c var2) {
         this.a = var1;
         this.b = var2;
      }

      public void dispose() {
         this.e.dispose();
      }

      public boolean isDisposed() {
         return this.e.isDisposed();
      }

      public void onComplete() {
         if(!this.c) {
            this.c = true;
            Object var1 = this.d;
            this.d = null;
            if(var1 != null) {
               this.a.a_(var1);
            } else {
               this.a.onComplete();
            }
         }

      }

      public void onError(Throwable var1) {
         if(this.c) {
            io.reactivex.g.a.a(var1);
         } else {
            this.c = true;
            this.d = null;
            this.a.onError(var1);
         }

      }

      public void onNext(Object var1) {
         if(!this.c) {
            Object var2 = this.d;
            if(var2 == null) {
               this.d = var1;
            } else {
               try {
                  this.d = io.reactivex.d.b.b.a(this.b.a(var2, var1), "The reducer returned a null value");
               } catch (Throwable var3) {
                  io.reactivex.exceptions.a.b(var3);
                  this.e.dispose();
                  this.onError(var3);
               }
            }
         }

      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.e, var1)) {
            this.e = var1;
            this.a.onSubscribe(this);
         }

      }
   }
}
