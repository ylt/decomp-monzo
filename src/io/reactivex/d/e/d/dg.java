package io.reactivex.d.e.d;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public final class dg extends a {
   final io.reactivex.c.h b;
   final int c;
   final boolean d;

   public dg(io.reactivex.r var1, io.reactivex.c.h var2, int var3, boolean var4) {
      super(var1);
      this.b = var2;
      this.c = var3;
      this.d = var4;
   }

   public void subscribeActual(io.reactivex.t var1) {
      if(!cr.a(this.a, var1, this.b)) {
         this.a.subscribe(new dg.b(var1, this.b, this.c, this.d));
      }

   }

   static final class a extends AtomicReference implements io.reactivex.t {
      final dg.b a;
      final long b;
      final io.reactivex.d.f.c c;
      volatile boolean d;

      a(dg.b var1, long var2, int var4) {
         this.a = var1;
         this.b = var2;
         this.c = new io.reactivex.d.f.c(var4);
      }

      public void a() {
         io.reactivex.d.a.d.a((AtomicReference)this);
      }

      public void onComplete() {
         if(this.b == this.a.k) {
            this.d = true;
            this.a.b();
         }

      }

      public void onError(Throwable var1) {
         this.a.a(this, var1);
      }

      public void onNext(Object var1) {
         if(this.b == this.a.k) {
            this.c.a(var1);
            this.a.b();
         }

      }

      public void onSubscribe(io.reactivex.b.b var1) {
         io.reactivex.d.a.d.b(this, var1);
      }
   }

   static final class b extends AtomicInteger implements io.reactivex.b.b, io.reactivex.t {
      static final dg.a j = new dg.a((dg.b)null, -1L, 1);
      final io.reactivex.t a;
      final io.reactivex.c.h b;
      final int c;
      final boolean d;
      final io.reactivex.d.j.c e;
      volatile boolean f;
      volatile boolean g;
      io.reactivex.b.b h;
      final AtomicReference i = new AtomicReference();
      volatile long k;

      static {
         j.a();
      }

      b(io.reactivex.t var1, io.reactivex.c.h var2, int var3, boolean var4) {
         this.a = var1;
         this.b = var2;
         this.c = var3;
         this.d = var4;
         this.e = new io.reactivex.d.j.c();
      }

      void a() {
         if((dg.a)this.i.get() != j) {
            dg.a var1 = (dg.a)this.i.getAndSet(j);
            if(var1 != j && var1 != null) {
               var1.a();
            }
         }

      }

      void a(dg.a var1, Throwable var2) {
         if(var1.b == this.k && this.e.a(var2)) {
            if(!this.d) {
               this.h.dispose();
            }

            var1.d = true;
            this.b();
         } else {
            io.reactivex.g.a.a(var2);
         }

      }

      void b() {
         if(this.getAndIncrement() == 0) {
            io.reactivex.t var4 = this.a;
            int var2 = 1;

            while(true) {
               while(!this.g) {
                  boolean var1;
                  if(this.f) {
                     if(this.i.get() == null) {
                        var1 = true;
                     } else {
                        var1 = false;
                     }

                     if(this.d) {
                        if(var1) {
                           Throwable var5 = (Throwable)this.e.get();
                           if(var5 != null) {
                              var4.onError(var5);
                           } else {
                              var4.onComplete();
                           }

                           return;
                        }
                     } else {
                        if((Throwable)this.e.get() != null) {
                           var4.onError(this.e.a());
                           return;
                        }

                        if(var1) {
                           var4.onComplete();
                           return;
                        }
                     }
                  }

                  dg.a var7 = (dg.a)this.i.get();
                  if(var7 != null) {
                     io.reactivex.d.f.c var8 = var7.c;
                     boolean var3;
                     if(var7.d) {
                        var3 = var8.b();
                        if(this.d) {
                           if(var3) {
                              this.i.compareAndSet(var7, (Object)null);
                              continue;
                           }
                        } else {
                           if((Throwable)this.e.get() != null) {
                              var4.onError(this.e.a());
                              return;
                           }

                           if(var3) {
                              this.i.compareAndSet(var7, (Object)null);
                              continue;
                           }
                        }
                     }

                     while(true) {
                        if(this.g) {
                           return;
                        }

                        if(var7 != this.i.get()) {
                           var1 = true;
                           break;
                        }

                        if(!this.d && (Throwable)this.e.get() != null) {
                           var4.onError(this.e.a());
                           return;
                        }

                        var3 = var7.d;
                        Object var6 = var8.n_();
                        if(var6 == null) {
                           var1 = true;
                        } else {
                           var1 = false;
                        }

                        if(var3 && var1) {
                           this.i.compareAndSet(var7, (Object)null);
                           var1 = true;
                           break;
                        }

                        if(var1) {
                           var1 = false;
                           break;
                        }

                        var4.onNext(var6);
                     }

                     if(var1) {
                        continue;
                     }
                  }

                  var2 = this.addAndGet(-var2);
                  if(var2 == 0) {
                     return;
                  }
               }

               return;
            }
         }
      }

      public void dispose() {
         if(!this.g) {
            this.g = true;
            this.h.dispose();
            this.a();
         }

      }

      public boolean isDisposed() {
         return this.g;
      }

      public void onComplete() {
         if(!this.f) {
            this.f = true;
            this.b();
         }

      }

      public void onError(Throwable var1) {
         if(!this.f && this.e.a(var1)) {
            this.f = true;
            this.b();
         } else {
            if(!this.d) {
               this.a();
            }

            io.reactivex.g.a.a(var1);
         }

      }

      public void onNext(Object var1) {
         long var2 = 1L + this.k;
         this.k = var2;
         dg.a var4 = (dg.a)this.i.get();
         if(var4 != null) {
            var4.a();
         }

         io.reactivex.r var7;
         try {
            var7 = (io.reactivex.r)io.reactivex.d.b.b.a(this.b.a(var1), "The ObservableSource returned is null");
         } catch (Throwable var6) {
            io.reactivex.exceptions.a.b(var6);
            this.h.dispose();
            this.onError(var6);
            return;
         }

         var4 = new dg.a(this, var2, this.c);

         while(true) {
            dg.a var5 = (dg.a)this.i.get();
            if(var5 == j) {
               break;
            }

            if(this.i.compareAndSet(var5, var4)) {
               var7.subscribe(var4);
               break;
            }
         }

      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.h, var1)) {
            this.h = var1;
            this.a.onSubscribe(this);
         }

      }
   }
}
