package io.reactivex.d.e.d;

import io.reactivex.exceptions.CompositeException;
import java.util.concurrent.atomic.AtomicInteger;

public final class cn extends a {
   final io.reactivex.c.q b;
   final long c;

   public cn(io.reactivex.n var1, long var2, io.reactivex.c.q var4) {
      super(var1);
      this.b = var4;
      this.c = var2;
   }

   public void subscribeActual(io.reactivex.t var1) {
      io.reactivex.d.a.k var2 = new io.reactivex.d.a.k();
      var1.onSubscribe(var2);
      (new cn.a(var1, this.c, this.b, var2, this.a)).a();
   }

   static final class a extends AtomicInteger implements io.reactivex.t {
      final io.reactivex.t a;
      final io.reactivex.d.a.k b;
      final io.reactivex.r c;
      final io.reactivex.c.q d;
      long e;

      a(io.reactivex.t var1, long var2, io.reactivex.c.q var4, io.reactivex.d.a.k var5, io.reactivex.r var6) {
         this.a = var1;
         this.b = var5;
         this.c = var6;
         this.d = var4;
         this.e = var2;
      }

      void a() {
         if(this.getAndIncrement() == 0) {
            int var1 = 1;

            while(!this.b.isDisposed()) {
               this.c.subscribe(this);
               int var2 = this.addAndGet(-var1);
               var1 = var2;
               if(var2 == 0) {
                  break;
               }
            }
         }

      }

      public void onComplete() {
         this.a.onComplete();
      }

      public void onError(Throwable var1) {
         long var3 = this.e;
         if(var3 != Long.MAX_VALUE) {
            this.e = var3 - 1L;
         }

         if(var3 == 0L) {
            this.a.onError(var1);
         } else {
            boolean var2;
            try {
               var2 = this.d.a(var1);
            } catch (Throwable var6) {
               io.reactivex.exceptions.a.b(var6);
               this.a.onError(new CompositeException(new Throwable[]{var1, var6}));
               return;
            }

            if(!var2) {
               this.a.onError(var1);
            } else {
               this.a();
            }
         }

      }

      public void onNext(Object var1) {
         this.a.onNext(var1);
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         this.b.a(var1);
      }
   }
}
