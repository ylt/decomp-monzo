package io.reactivex.d.e.d;

import java.util.NoSuchElementException;

public final class ap extends io.reactivex.v implements io.reactivex.d.c.c {
   final io.reactivex.r a;
   final long b;
   final Object c;

   public ap(io.reactivex.r var1, long var2, Object var4) {
      this.a = var1;
      this.b = var2;
      this.c = var4;
   }

   public void b(io.reactivex.x var1) {
      this.a.subscribe(new ap.a(var1, this.b, this.c));
   }

   public io.reactivex.n q_() {
      return io.reactivex.g.a.a((io.reactivex.n)(new an(this.a, this.b, this.c, true)));
   }

   static final class a implements io.reactivex.b.b, io.reactivex.t {
      final io.reactivex.x a;
      final long b;
      final Object c;
      io.reactivex.b.b d;
      long e;
      boolean f;

      a(io.reactivex.x var1, long var2, Object var4) {
         this.a = var1;
         this.b = var2;
         this.c = var4;
      }

      public void dispose() {
         this.d.dispose();
      }

      public boolean isDisposed() {
         return this.d.isDisposed();
      }

      public void onComplete() {
         if(!this.f) {
            this.f = true;
            Object var1 = this.c;
            if(var1 != null) {
               this.a.a_(var1);
            } else {
               this.a.onError(new NoSuchElementException());
            }
         }

      }

      public void onError(Throwable var1) {
         if(this.f) {
            io.reactivex.g.a.a(var1);
         } else {
            this.f = true;
            this.a.onError(var1);
         }

      }

      public void onNext(Object var1) {
         if(!this.f) {
            long var2 = this.e;
            if(var2 == this.b) {
               this.f = true;
               this.d.dispose();
               this.a.a_(var1);
            } else {
               this.e = var2 + 1L;
            }
         }

      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.d, var1)) {
            this.d = var1;
            this.a.onSubscribe(this);
         }

      }
   }
}
