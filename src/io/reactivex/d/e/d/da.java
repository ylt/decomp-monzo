package io.reactivex.d.e.d;

import java.util.ArrayDeque;

public final class da extends a {
   final int b;

   public da(io.reactivex.r var1, int var2) {
      super(var1);
      this.b = var2;
   }

   public void subscribeActual(io.reactivex.t var1) {
      this.a.subscribe(new da.a(var1, this.b));
   }

   static final class a extends ArrayDeque implements io.reactivex.b.b, io.reactivex.t {
      final io.reactivex.t a;
      final int b;
      io.reactivex.b.b c;

      a(io.reactivex.t var1, int var2) {
         super(var2);
         this.a = var1;
         this.b = var2;
      }

      public void dispose() {
         this.c.dispose();
      }

      public boolean isDisposed() {
         return this.c.isDisposed();
      }

      public void onComplete() {
         this.a.onComplete();
      }

      public void onError(Throwable var1) {
         this.a.onError(var1);
      }

      public void onNext(Object var1) {
         if(this.b == this.size()) {
            this.a.onNext(this.poll());
         }

         this.offer(var1);
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.c, var1)) {
            this.c = var1;
            this.a.onSubscribe(this);
         }

      }
   }
}
