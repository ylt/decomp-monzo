package io.reactivex.d.e.d;

import java.util.Iterator;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public final class u extends io.reactivex.n {
   final io.reactivex.r[] a;
   final Iterable b;
   final io.reactivex.c.h c;
   final int d;
   final boolean e;

   public u(io.reactivex.r[] var1, Iterable var2, io.reactivex.c.h var3, int var4, boolean var5) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
      this.d = var4;
      this.e = var5;
   }

   public void subscribeActual(io.reactivex.t var1) {
      Object var3 = this.a;
      int var2;
      if(var3 == null) {
         var3 = new io.reactivex.n[8];
         Iterator var5 = this.b.iterator();

         for(var2 = 0; var5.hasNext(); ++var2) {
            io.reactivex.r var6 = (io.reactivex.r)var5.next();
            if(var2 == ((Object[])var3).length) {
               io.reactivex.r[] var4 = new io.reactivex.r[(var2 >> 2) + var2];
               System.arraycopy(var3, 0, var4, 0, var2);
               var3 = var4;
            }

            ((Object[])var3)[var2] = var6;
         }
      } else {
         var2 = ((Object[])var3).length;
      }

      if(var2 == 0) {
         io.reactivex.d.a.e.a(var1);
      } else {
         (new u.b(var1, this.c, var2, this.d, this.e)).a((io.reactivex.r[])var3);
      }

   }

   static final class a implements io.reactivex.t {
      final u.b a;
      final int b;
      final AtomicReference c = new AtomicReference();

      a(u.b var1, int var2) {
         this.a = var1;
         this.b = var2;
      }

      public void a() {
         io.reactivex.d.a.d.a(this.c);
      }

      public void onComplete() {
         this.a.a((Object)null, this.b);
      }

      public void onError(Throwable var1) {
         this.a.a(var1);
         this.a.a((Object)null, this.b);
      }

      public void onNext(Object var1) {
         this.a.a(var1, this.b);
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         io.reactivex.d.a.d.b(this.c, var1);
      }
   }

   static final class b extends AtomicInteger implements io.reactivex.b.b {
      final io.reactivex.t a;
      final io.reactivex.c.h b;
      final u.a[] c;
      final Object[] d;
      final io.reactivex.d.f.c e;
      final boolean f;
      volatile boolean g;
      volatile boolean h;
      final io.reactivex.d.j.c i = new io.reactivex.d.j.c();
      int j;
      int k;

      b(io.reactivex.t var1, io.reactivex.c.h var2, int var3, int var4, boolean var5) {
         this.a = var1;
         this.b = var2;
         this.f = var5;
         this.d = (Object[])(new Object[var3]);
         this.c = new u.a[var3];
         this.e = new io.reactivex.d.f.c(var4);
      }

      void a() {
         u.a[] var3 = this.c;
         int var2 = var3.length;

         for(int var1 = 0; var1 < var2; ++var1) {
            var3[var1].a();
         }

      }

      void a(io.reactivex.d.f.c var1) {
         this.b(var1);
         this.a();
      }

      void a(Object param1, int param2) {
         // $FF: Couldn't be decompiled
      }

      void a(Throwable var1) {
         if(!this.i.a(var1)) {
            io.reactivex.g.a.a(var1);
         }

      }

      public void a(io.reactivex.r[] var1) {
         byte var3 = 0;
         u.a[] var5 = this.c;
         int var4 = var5.length;

         int var2;
         for(var2 = 0; var2 < var4; ++var2) {
            var5[var2] = new u.a(this, var2);
         }

         this.lazySet(0);
         this.a.onSubscribe(this);

         for(var2 = var3; var2 < var4 && !this.h && !this.g; ++var2) {
            var1[var2].subscribe(var5[var2]);
         }

      }

      boolean a(boolean var1, boolean var2, io.reactivex.t var3, io.reactivex.d.f.c var4, boolean var5) {
         if(this.g) {
            this.a(var4);
            var1 = true;
         } else {
            if(var1) {
               if(var5) {
                  if(var2) {
                     this.a(var4);
                     Throwable var6 = this.i.a();
                     if(var6 != null) {
                        var3.onError(var6);
                     } else {
                        var3.onComplete();
                     }

                     var1 = true;
                     return var1;
                  }
               } else {
                  if((Throwable)this.i.get() != null) {
                     this.a(var4);
                     var3.onError(this.i.a());
                     var1 = true;
                     return var1;
                  }

                  if(var2) {
                     this.b(this.e);
                     var3.onComplete();
                     var1 = true;
                     return var1;
                  }
               }
            }

            var1 = false;
         }

         return var1;
      }

      void b() {
         if(this.getAndIncrement() == 0) {
            io.reactivex.d.f.c var5 = this.e;
            io.reactivex.t var6 = this.a;
            boolean var4 = this.f;
            int var1 = 1;

            while(!this.a(this.h, var5.b(), var6, var5, var4)) {
               while(true) {
                  boolean var3 = this.h;
                  boolean var2;
                  if((u.a)var5.n_() == null) {
                     var2 = true;
                  } else {
                     var2 = false;
                  }

                  if(this.a(var3, var2, var6, var5, var4)) {
                     return;
                  }

                  if(var2) {
                     var1 = this.addAndGet(-var1);
                     if(var1 == 0) {
                        return;
                     }
                  } else {
                     Object[] var7 = (Object[])((Object[])var5.n_());

                     Object var9;
                     try {
                        var9 = io.reactivex.d.b.b.a(this.b.a(var7), "The combiner returned a null");
                     } catch (Throwable var8) {
                        io.reactivex.exceptions.a.b(var8);
                        this.g = true;
                        this.a(var5);
                        var6.onError(var8);
                        return;
                     }

                     var6.onNext(var9);
                  }
               }
            }
         }

      }

      void b(io.reactivex.d.f.c param1) {
         // $FF: Couldn't be decompiled
      }

      public void dispose() {
         if(!this.g) {
            this.g = true;
            this.a();
            if(this.getAndIncrement() == 0) {
               this.b(this.e);
            }
         }

      }

      public boolean isDisposed() {
         return this.g;
      }
   }
}
