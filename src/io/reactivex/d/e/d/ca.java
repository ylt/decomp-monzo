package io.reactivex.d.e.d;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

public final class ca extends io.reactivex.e.a {
   final io.reactivex.r a;
   final AtomicReference b;
   final io.reactivex.r c;

   private ca(io.reactivex.r var1, io.reactivex.r var2, AtomicReference var3) {
      this.c = var1;
      this.a = var2;
      this.b = var3;
   }

   public static io.reactivex.e.a a(io.reactivex.r var0) {
      AtomicReference var1 = new AtomicReference();
      return io.reactivex.g.a.a((io.reactivex.e.a)(new ca(new ca.c(var1), var0, var1)));
   }

   public void a(io.reactivex.c.g var1) {
      ca.b var3;
      ca.b var4;
      do {
         var4 = (ca.b)this.b.get();
         if(var4 != null) {
            var3 = var4;
            if(!var4.isDisposed()) {
               break;
            }
         }

         var3 = new ca.b(this.b);
      } while(!this.b.compareAndSet(var4, var3));

      boolean var2;
      if(!var3.e.get() && var3.e.compareAndSet(false, true)) {
         var2 = true;
      } else {
         var2 = false;
      }

      try {
         var1.a(var3);
      } catch (Throwable var5) {
         io.reactivex.exceptions.a.b(var5);
         throw io.reactivex.d.j.j.a(var5);
      }

      if(var2) {
         this.a.subscribe(var3);
      }

   }

   protected void subscribeActual(io.reactivex.t var1) {
      this.c.subscribe(var1);
   }

   static final class a extends AtomicReference implements io.reactivex.b.b {
      final io.reactivex.t a;

      a(io.reactivex.t var1) {
         this.a = var1;
      }

      void a(ca.b var1) {
         if(!this.compareAndSet((Object)null, var1)) {
            var1.b(this);
         }

      }

      public void dispose() {
         Object var1 = this.getAndSet(this);
         if(var1 != null && var1 != this) {
            ((ca.b)var1).b(this);
         }

      }

      public boolean isDisposed() {
         boolean var1;
         if(this.get() == this) {
            var1 = true;
         } else {
            var1 = false;
         }

         return var1;
      }
   }

   static final class b implements io.reactivex.b.b, io.reactivex.t {
      static final ca.a[] b = new ca.a[0];
      static final ca.a[] c = new ca.a[0];
      final AtomicReference a;
      final AtomicReference d;
      final AtomicBoolean e;
      final AtomicReference f = new AtomicReference();

      b(AtomicReference var1) {
         this.d = new AtomicReference(b);
         this.a = var1;
         this.e = new AtomicBoolean();
      }

      boolean a(ca.a var1) {
         while(true) {
            ca.a[] var5 = (ca.a[])this.d.get();
            boolean var3;
            if(var5 == c) {
               var3 = false;
            } else {
               int var2 = var5.length;
               ca.a[] var4 = new ca.a[var2 + 1];
               System.arraycopy(var5, 0, var4, 0, var2);
               var4[var2] = var1;
               if(!this.d.compareAndSet(var5, var4)) {
                  continue;
               }

               var3 = true;
            }

            return var3;
         }
      }

      void b(ca.a var1) {
         while(true) {
            ca.a[] var7 = (ca.a[])this.d.get();
            int var5 = var7.length;
            if(var5 != 0) {
               byte var4 = -1;
               int var2 = 0;

               int var3;
               while(true) {
                  var3 = var4;
                  if(var2 >= var5) {
                     break;
                  }

                  if(var7[var2].equals(var1)) {
                     var3 = var2;
                     break;
                  }

                  ++var2;
               }

               if(var3 >= 0) {
                  ca.a[] var6;
                  if(var5 == 1) {
                     var6 = b;
                  } else {
                     var6 = new ca.a[var5 - 1];
                     System.arraycopy(var7, 0, var6, 0, var3);
                     System.arraycopy(var7, var3 + 1, var6, var3, var5 - var3 - 1);
                  }

                  if(!this.d.compareAndSet(var7, var6)) {
                     continue;
                  }
               }
            }

            return;
         }
      }

      public void dispose() {
         if(this.d.get() != c && (ca.a[])this.d.getAndSet(c) != c) {
            this.a.compareAndSet(this, (Object)null);
            io.reactivex.d.a.d.a(this.f);
         }

      }

      public boolean isDisposed() {
         boolean var1;
         if(this.d.get() == c) {
            var1 = true;
         } else {
            var1 = false;
         }

         return var1;
      }

      public void onComplete() {
         this.a.compareAndSet(this, (Object)null);
         ca.a[] var3 = (ca.a[])this.d.getAndSet(c);
         int var2 = var3.length;

         for(int var1 = 0; var1 < var2; ++var1) {
            var3[var1].a.onComplete();
         }

      }

      public void onError(Throwable var1) {
         this.a.compareAndSet(this, (Object)null);
         ca.a[] var4 = (ca.a[])this.d.getAndSet(c);
         if(var4.length != 0) {
            int var3 = var4.length;

            for(int var2 = 0; var2 < var3; ++var2) {
               var4[var2].a.onError(var1);
            }
         } else {
            io.reactivex.g.a.a(var1);
         }

      }

      public void onNext(Object var1) {
         ca.a[] var4 = (ca.a[])this.d.get();
         int var3 = var4.length;

         for(int var2 = 0; var2 < var3; ++var2) {
            var4[var2].a.onNext(var1);
         }

      }

      public void onSubscribe(io.reactivex.b.b var1) {
         io.reactivex.d.a.d.b(this.f, var1);
      }
   }

   static final class c implements io.reactivex.r {
      private final AtomicReference a;

      c(AtomicReference var1) {
         this.a = var1;
      }

      public void subscribe(io.reactivex.t var1) {
         ca.a var3 = new ca.a(var1);
         var1.onSubscribe(var3);

         ca.b var2;
         ca.b var4;
         do {
            do {
               var2 = (ca.b)this.a.get();
               if(var2 != null) {
                  var4 = var2;
                  if(!var2.isDisposed()) {
                     break;
                  }
               }

               var4 = new ca.b(this.a);
            } while(!this.a.compareAndSet(var2, var4));
         } while(!var4.a(var3));

         var3.a(var4);
      }
   }
}
