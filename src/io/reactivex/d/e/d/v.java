package io.reactivex.d.e.d;

import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicInteger;

public final class v extends a {
   final io.reactivex.c.h b;
   final int c;
   final io.reactivex.d.j.i d;

   public v(io.reactivex.r var1, io.reactivex.c.h var2, int var3, io.reactivex.d.j.i var4) {
      super(var1);
      this.b = var2;
      this.d = var4;
      this.c = Math.max(8, var3);
   }

   public void subscribeActual(io.reactivex.t var1) {
      if(!cr.a(this.a, var1, this.b)) {
         if(this.d == io.reactivex.d.j.i.a) {
            io.reactivex.f.e var6 = new io.reactivex.f.e(var1);
            this.a.subscribe(new v.b(var6, this.b, this.c));
         } else {
            io.reactivex.r var5 = this.a;
            io.reactivex.c.h var4 = this.b;
            int var2 = this.c;
            boolean var3;
            if(this.d == io.reactivex.d.j.i.c) {
               var3 = true;
            } else {
               var3 = false;
            }

            var5.subscribe(new v.a(var1, var4, var2, var3));
         }
      }

   }

   static final class a extends AtomicInteger implements io.reactivex.b.b, io.reactivex.t {
      final io.reactivex.t a;
      final io.reactivex.c.h b;
      final int c;
      final io.reactivex.d.j.c d;
      final v.a e;
      final io.reactivex.d.a.k f;
      final boolean g;
      io.reactivex.d.c.i h;
      io.reactivex.b.b i;
      volatile boolean j;
      volatile boolean k;
      volatile boolean l;
      int m;

      a(io.reactivex.t var1, io.reactivex.c.h var2, int var3, boolean var4) {
         this.a = var1;
         this.b = var2;
         this.c = var3;
         this.g = var4;
         this.d = new io.reactivex.d.j.c();
         this.e = new v.a(var1, this);
         this.f = new io.reactivex.d.a.k();
      }

      void a() {
         if(this.getAndIncrement() == 0) {
            io.reactivex.t var3 = this.a;
            io.reactivex.d.c.i var5 = this.h;
            io.reactivex.d.j.c var4 = this.d;

            while(true) {
               while(true) {
                  if(!this.j) {
                     if(this.l) {
                        var5.c();
                        return;
                     }

                     if(!this.g && (Throwable)var4.get() != null) {
                        var5.c();
                        this.l = true;
                        var3.onError(var4.a());
                        return;
                     }

                     boolean var2 = this.k;

                     Object var6;
                     try {
                        var6 = var5.n_();
                     } catch (Throwable var8) {
                        io.reactivex.exceptions.a.b(var8);
                        this.l = true;
                        this.i.dispose();
                        var4.a(var8);
                        var3.onError(var4.a());
                        return;
                     }

                     boolean var1;
                     if(var6 == null) {
                        var1 = true;
                     } else {
                        var1 = false;
                     }

                     if(var2 && var1) {
                        this.l = true;
                        Throwable var10 = var4.a();
                        if(var10 != null) {
                           var3.onError(var10);
                        } else {
                           var3.onComplete();
                        }

                        return;
                     }

                     if(!var1) {
                        io.reactivex.r var11;
                        try {
                           var11 = (io.reactivex.r)io.reactivex.d.b.b.a(this.b.a(var6), "The mapper returned a null ObservableSource");
                        } catch (Throwable var7) {
                           io.reactivex.exceptions.a.b(var7);
                           this.l = true;
                           this.i.dispose();
                           var5.c();
                           var4.a(var7);
                           var3.onError(var4.a());
                           return;
                        }

                        if(var11 instanceof Callable) {
                           try {
                              var6 = ((Callable)var11).call();
                           } catch (Throwable var9) {
                              io.reactivex.exceptions.a.b(var9);
                              var4.a(var9);
                              continue;
                           }

                           if(var6 != null && !this.l) {
                              var3.onNext(var6);
                           }
                           continue;
                        }

                        this.j = true;
                        var11.subscribe(this.e);
                     }
                  }

                  if(this.decrementAndGet() == 0) {
                     return;
                  }
               }
            }
         }
      }

      public void dispose() {
         this.l = true;
         this.i.dispose();
         this.f.dispose();
      }

      public boolean isDisposed() {
         return this.l;
      }

      public void onComplete() {
         this.k = true;
         this.a();
      }

      public void onError(Throwable var1) {
         if(this.d.a(var1)) {
            this.k = true;
            this.a();
         } else {
            io.reactivex.g.a.a(var1);
         }

      }

      public void onNext(Object var1) {
         if(this.m == 0) {
            this.h.a(var1);
         }

         this.a();
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.i, var1)) {
            this.i = var1;
            if(var1 instanceof io.reactivex.d.c.d) {
               io.reactivex.d.c.d var3 = (io.reactivex.d.c.d)var1;
               int var2 = var3.a(3);
               if(var2 == 1) {
                  this.m = var2;
                  this.h = var3;
                  this.k = true;
                  this.a.onSubscribe(this);
                  this.a();
                  return;
               }

               if(var2 == 2) {
                  this.m = var2;
                  this.h = var3;
                  this.a.onSubscribe(this);
                  return;
               }
            }

            this.h = new io.reactivex.d.f.c(this.c);
            this.a.onSubscribe(this);
         }

      }
   }

   static final class a implements io.reactivex.t {
      final io.reactivex.t a;
      final v.a b;

      a(io.reactivex.t var1, v.a var2) {
         this.a = var1;
         this.b = var2;
      }

      public void onComplete() {
         v.a var1 = this.b;
         var1.j = false;
         var1.a();
      }

      public void onError(Throwable var1) {
         v.a var2 = this.b;
         if(var2.d.a(var1)) {
            if(!var2.g) {
               var2.i.dispose();
            }

            var2.j = false;
            var2.a();
         } else {
            io.reactivex.g.a.a(var1);
         }

      }

      public void onNext(Object var1) {
         this.a.onNext(var1);
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         this.b.f.b(var1);
      }
   }

   static final class b extends AtomicInteger implements io.reactivex.b.b, io.reactivex.t {
      final io.reactivex.t a;
      final io.reactivex.d.a.k b;
      final io.reactivex.c.h c;
      final io.reactivex.t d;
      final int e;
      io.reactivex.d.c.i f;
      io.reactivex.b.b g;
      volatile boolean h;
      volatile boolean i;
      volatile boolean j;
      int k;

      b(io.reactivex.t var1, io.reactivex.c.h var2, int var3) {
         this.a = var1;
         this.c = var2;
         this.e = var3;
         this.d = new v.a(var1, this);
         this.b = new io.reactivex.d.a.k();
      }

      void a() {
         this.h = false;
         this.b();
      }

      void a(io.reactivex.b.b var1) {
         this.b.a(var1);
      }

      void b() {
         if(this.getAndIncrement() == 0) {
            do {
               if(this.i) {
                  this.f.c();
                  break;
               }

               if(!this.h) {
                  boolean var2 = this.j;

                  Object var3;
                  try {
                     var3 = this.f.n_();
                  } catch (Throwable var5) {
                     io.reactivex.exceptions.a.b(var5);
                     this.dispose();
                     this.f.c();
                     this.a.onError(var5);
                     break;
                  }

                  boolean var1;
                  if(var3 == null) {
                     var1 = true;
                  } else {
                     var1 = false;
                  }

                  if(var2 && var1) {
                     this.i = true;
                     this.a.onComplete();
                     break;
                  }

                  if(!var1) {
                     io.reactivex.r var6;
                     try {
                        var6 = (io.reactivex.r)io.reactivex.d.b.b.a(this.c.a(var3), "The mapper returned a null ObservableSource");
                     } catch (Throwable var4) {
                        io.reactivex.exceptions.a.b(var4);
                        this.dispose();
                        this.f.c();
                        this.a.onError(var4);
                        break;
                     }

                     this.h = true;
                     var6.subscribe(this.d);
                  }
               }
            } while(this.decrementAndGet() != 0);
         }

      }

      public void dispose() {
         this.i = true;
         this.b.dispose();
         this.g.dispose();
         if(this.getAndIncrement() == 0) {
            this.f.c();
         }

      }

      public boolean isDisposed() {
         return this.i;
      }

      public void onComplete() {
         if(!this.j) {
            this.j = true;
            this.b();
         }

      }

      public void onError(Throwable var1) {
         if(this.j) {
            io.reactivex.g.a.a(var1);
         } else {
            this.j = true;
            this.dispose();
            this.a.onError(var1);
         }

      }

      public void onNext(Object var1) {
         if(!this.j) {
            if(this.k == 0) {
               this.f.a(var1);
            }

            this.b();
         }

      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.g, var1)) {
            this.g = var1;
            if(var1 instanceof io.reactivex.d.c.d) {
               io.reactivex.d.c.d var3 = (io.reactivex.d.c.d)var1;
               int var2 = var3.a(3);
               if(var2 == 1) {
                  this.k = var2;
                  this.f = var3;
                  this.j = true;
                  this.a.onSubscribe(this);
                  this.b();
                  return;
               }

               if(var2 == 2) {
                  this.k = var2;
                  this.f = var3;
                  this.a.onSubscribe(this);
                  return;
               }
            }

            this.f = new io.reactivex.d.f.c(this.e);
            this.a.onSubscribe(this);
         }

      }
   }

   static final class a implements io.reactivex.t {
      final io.reactivex.t a;
      final v.b b;

      a(io.reactivex.t var1, v.b var2) {
         this.a = var1;
         this.b = var2;
      }

      public void onComplete() {
         this.b.a();
      }

      public void onError(Throwable var1) {
         this.b.dispose();
         this.a.onError(var1);
      }

      public void onNext(Object var1) {
         this.a.onNext(var1);
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         this.b.a(var1);
      }
   }
}
