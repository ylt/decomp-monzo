package io.reactivex.d.e.d;

import java.util.concurrent.atomic.AtomicReference;

public final class z extends io.reactivex.n {
   final io.reactivex.p a;

   public z(io.reactivex.p var1) {
      this.a = var1;
   }

   protected void subscribeActual(io.reactivex.t var1) {
      z.a var2 = new z.a(var1);
      var1.onSubscribe(var2);

      try {
         this.a.a(var2);
      } catch (Throwable var3) {
         io.reactivex.exceptions.a.b(var3);
         var2.a(var3);
      }

   }

   static final class a extends AtomicReference implements io.reactivex.b.b, io.reactivex.o {
      final io.reactivex.t a;

      a(io.reactivex.t var1) {
         this.a = var1;
      }

      public void a() {
         if(!this.isDisposed()) {
            try {
               this.a.onComplete();
            } finally {
               this.dispose();
            }
         }

      }

      public void a(io.reactivex.b.b var1) {
         io.reactivex.d.a.d.a((AtomicReference)this, var1);
      }

      public void a(io.reactivex.c.f var1) {
         this.a((io.reactivex.b.b)(new io.reactivex.d.a.b(var1)));
      }

      public void a(Object var1) {
         if(var1 == null) {
            this.a((Throwable)(new NullPointerException("onNext called with null. Null values are generally not allowed in 2.x operators and sources.")));
         } else if(!this.isDisposed()) {
            this.a.onNext(var1);
         }

      }

      public void a(Throwable var1) {
         if(!this.b(var1)) {
            io.reactivex.g.a.a(var1);
         }

      }

      public boolean b(Throwable var1) {
         Object var3 = var1;
         if(var1 == null) {
            var3 = new NullPointerException("onError called with null. Null values are generally not allowed in 2.x operators and sources.");
         }

         boolean var2;
         if(!this.isDisposed()) {
            try {
               this.a.onError((Throwable)var3);
            } finally {
               this.dispose();
            }

            var2 = true;
         } else {
            var2 = false;
         }

         return var2;
      }

      public void dispose() {
         io.reactivex.d.a.d.a((AtomicReference)this);
      }

      public boolean isDisposed() {
         return io.reactivex.d.a.d.a((io.reactivex.b.b)this.get());
      }
   }
}
