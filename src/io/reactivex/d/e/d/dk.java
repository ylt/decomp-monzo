package io.reactivex.d.e.d;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public final class dk extends a {
   final long b;
   final long c;
   final TimeUnit d;
   final io.reactivex.u e;
   final int f;
   final boolean g;

   public dk(io.reactivex.r var1, long var2, long var4, TimeUnit var6, io.reactivex.u var7, int var8, boolean var9) {
      super(var1);
      this.b = var2;
      this.c = var4;
      this.d = var6;
      this.e = var7;
      this.f = var8;
      this.g = var9;
   }

   public void subscribeActual(io.reactivex.t var1) {
      this.a.subscribe(new dk.a(var1, this.b, this.c, this.d, this.e, this.f, this.g));
   }

   static final class a extends AtomicBoolean implements io.reactivex.b.b, io.reactivex.t {
      final io.reactivex.t a;
      final long b;
      final long c;
      final TimeUnit d;
      final io.reactivex.u e;
      final io.reactivex.d.f.c f;
      final boolean g;
      io.reactivex.b.b h;
      volatile boolean i;
      Throwable j;

      a(io.reactivex.t var1, long var2, long var4, TimeUnit var6, io.reactivex.u var7, int var8, boolean var9) {
         this.a = var1;
         this.b = var2;
         this.c = var4;
         this.d = var6;
         this.e = var7;
         this.f = new io.reactivex.d.f.c(var8);
         this.g = var9;
      }

      void a() {
         if(this.compareAndSet(false, true)) {
            io.reactivex.t var3 = this.a;
            io.reactivex.d.f.c var4 = this.f;
            boolean var2 = this.g;

            while(true) {
               if(this.i) {
                  var4.c();
                  break;
               }

               if(!var2) {
                  Throwable var5 = this.j;
                  if(var5 != null) {
                     var4.c();
                     var3.onError(var5);
                     break;
                  }
               }

               Object var8 = var4.n_();
               boolean var1;
               if(var8 == null) {
                  var1 = true;
               } else {
                  var1 = false;
               }

               if(var1) {
                  Throwable var7 = this.j;
                  if(var7 != null) {
                     var3.onError(var7);
                  } else {
                     var3.onComplete();
                  }
                  break;
               }

               Object var6 = var4.n_();
               if(((Long)var8).longValue() >= this.e.a(this.d) - this.c) {
                  var3.onNext(var6);
               }
            }
         }

      }

      public void dispose() {
         if(!this.i) {
            this.i = true;
            this.h.dispose();
            if(this.compareAndSet(false, true)) {
               this.f.c();
            }
         }

      }

      public boolean isDisposed() {
         return this.i;
      }

      public void onComplete() {
         this.a();
      }

      public void onError(Throwable var1) {
         this.j = var1;
         this.a();
      }

      public void onNext(Object var1) {
         io.reactivex.d.f.c var9 = this.f;
         long var7 = this.e.a(this.d);
         long var5 = this.c;
         long var3 = this.b;
         boolean var2;
         if(var3 == Long.MAX_VALUE) {
            var2 = true;
         } else {
            var2 = false;
         }

         var9.a((Object)Long.valueOf(var7), (Object)var1);

         while(!var9.b() && (((Long)var9.d()).longValue() <= var7 - var5 || !var2 && (long)(var9.e() >> 1) > var3)) {
            var9.n_();
            var9.n_();
         }

      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.h, var1)) {
            this.h = var1;
            this.a.onSubscribe(this);
         }

      }
   }
}
