package io.reactivex.d.e.d;

import java.util.Iterator;
import java.util.NoSuchElementException;

public final class d implements Iterable {
   final io.reactivex.r a;
   final Object b;

   public d(io.reactivex.r var1, Object var2) {
      this.a = var1;
      this.b = var2;
   }

   public Iterator iterator() {
      d.a var1 = new d.a(this.b);
      this.a.subscribe(var1);
      return var1.a();
   }

   static final class a extends io.reactivex.f.b {
      volatile Object a;

      a(Object var1) {
         this.a = io.reactivex.d.j.n.a(var1);
      }

      public d.a a() {
         return new d.a();
      }

      public void onComplete() {
         this.a = io.reactivex.d.j.n.a();
      }

      public void onError(Throwable var1) {
         this.a = io.reactivex.d.j.n.a(var1);
      }

      public void onNext(Object var1) {
         this.a = io.reactivex.d.j.n.a(var1);
      }
   }

   final class a implements Iterator {
      private Object b;

      public boolean hasNext() {
         this.b = d.super.a;
         boolean var1;
         if(!io.reactivex.d.j.n.b(this.b)) {
            var1 = true;
         } else {
            var1 = false;
         }

         return var1;
      }

      public Object next() {
         Object var1;
         try {
            if(this.b == null) {
               this.b = d.super.a;
            }

            if(io.reactivex.d.j.n.b(this.b)) {
               NoSuchElementException var4 = new NoSuchElementException();
               throw var4;
            }

            if(io.reactivex.d.j.n.c(this.b)) {
               throw io.reactivex.d.j.j.a(io.reactivex.d.j.n.f(this.b));
            }

            var1 = io.reactivex.d.j.n.e(this.b);
         } finally {
            this.b = null;
         }

         return var1;
      }

      public void remove() {
         throw new UnsupportedOperationException("Read only iterator");
      }
   }
}
