package io.reactivex.d.e.d;

import java.util.concurrent.atomic.AtomicReference;

public final class au extends a {
   final io.reactivex.c.h b;
   final boolean c;

   public au(io.reactivex.r var1, io.reactivex.c.h var2, boolean var3) {
      super(var1);
      this.b = var2;
      this.c = var3;
   }

   protected void subscribeActual(io.reactivex.t var1) {
      this.a.subscribe(new au.a(var1, this.b, this.c));
   }

   static final class a extends io.reactivex.d.d.b implements io.reactivex.t {
      final io.reactivex.t a;
      final io.reactivex.d.j.c b;
      final io.reactivex.c.h c;
      final boolean d;
      final io.reactivex.b.a e;
      io.reactivex.b.b f;
      volatile boolean g;

      a(io.reactivex.t var1, io.reactivex.c.h var2, boolean var3) {
         this.a = var1;
         this.c = var2;
         this.d = var3;
         this.b = new io.reactivex.d.j.c();
         this.e = new io.reactivex.b.a();
         this.lazySet(1);
      }

      public int a(int var1) {
         return var1 & 2;
      }

      void a(au.a var1) {
         this.e.c(var1);
         this.onComplete();
      }

      void a(au.a var1, Throwable var2) {
         this.e.c(var1);
         this.onError(var2);
      }

      public boolean b() {
         return true;
      }

      public void c() {
      }

      public void dispose() {
         this.g = true;
         this.f.dispose();
         this.e.dispose();
      }

      public boolean isDisposed() {
         return this.f.isDisposed();
      }

      public Object n_() throws Exception {
         return null;
      }

      public void onComplete() {
         if(this.decrementAndGet() == 0) {
            Throwable var1 = this.b.a();
            if(var1 != null) {
               this.a.onError(var1);
            } else {
               this.a.onComplete();
            }
         }

      }

      public void onError(Throwable var1) {
         if(this.b.a(var1)) {
            if(this.d) {
               if(this.decrementAndGet() == 0) {
                  var1 = this.b.a();
                  this.a.onError(var1);
               }
            } else {
               this.dispose();
               if(this.getAndSet(0) > 0) {
                  var1 = this.b.a();
                  this.a.onError(var1);
               }
            }
         } else {
            io.reactivex.g.a.a(var1);
         }

      }

      public void onNext(Object var1) {
         io.reactivex.d var4;
         try {
            var4 = (io.reactivex.d)io.reactivex.d.b.b.a(this.c.a(var1), "The mapper returned a null CompletableSource");
         } catch (Throwable var3) {
            io.reactivex.exceptions.a.b(var3);
            this.f.dispose();
            this.onError(var3);
            return;
         }

         this.getAndIncrement();
         au.a var2 = new au.a();
         if(!this.g && this.e.a((io.reactivex.b.b)var2)) {
            var4.a(var2);
         }

      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.f, var1)) {
            this.f = var1;
            this.a.onSubscribe(this);
         }

      }
   }

   final class a extends AtomicReference implements io.reactivex.b.b, io.reactivex.c {
      public void dispose() {
         io.reactivex.d.a.d.a((AtomicReference)this);
      }

      public boolean isDisposed() {
         return io.reactivex.d.a.d.a((io.reactivex.b.b)this.get());
      }

      public void onComplete() {
         au.this.a(this);
      }

      public void onError(Throwable var1) {
         au.this.a(this, var1);
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         io.reactivex.d.a.d.b(this, var1);
      }
   }
}
