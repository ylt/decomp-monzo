package io.reactivex.d.e.d;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public final class ax extends a {
   final io.reactivex.c.h b;
   final boolean c;

   public ax(io.reactivex.r var1, io.reactivex.c.h var2, boolean var3) {
      super(var1);
      this.b = var2;
      this.c = var3;
   }

   protected void subscribeActual(io.reactivex.t var1) {
      this.a.subscribe(new ax.a(var1, this.b, this.c));
   }

   static final class a extends AtomicInteger implements io.reactivex.b.b, io.reactivex.t {
      final io.reactivex.t a;
      final boolean b;
      final io.reactivex.b.a c;
      final AtomicInteger d;
      final io.reactivex.d.j.c e;
      final io.reactivex.c.h f;
      final AtomicReference g;
      io.reactivex.b.b h;
      volatile boolean i;

      a(io.reactivex.t var1, io.reactivex.c.h var2, boolean var3) {
         this.a = var1;
         this.f = var2;
         this.b = var3;
         this.c = new io.reactivex.b.a();
         this.e = new io.reactivex.d.j.c();
         this.d = new AtomicInteger(1);
         this.g = new AtomicReference();
      }

      io.reactivex.d.f.c a() {
         while(true) {
            io.reactivex.d.f.c var1 = (io.reactivex.d.f.c)this.g.get();
            if(var1 == null) {
               var1 = new io.reactivex.d.f.c(io.reactivex.n.bufferSize());
               if(!this.g.compareAndSet((Object)null, var1)) {
                  continue;
               }
            }

            return var1;
         }
      }

      void a(ax.a param1, Object param2) {
         // $FF: Couldn't be decompiled
      }

      void a(ax.a var1, Throwable var2) {
         this.c.c(var1);
         if(this.e.a(var2)) {
            if(!this.b) {
               this.h.dispose();
               this.c.dispose();
            }

            this.d.decrementAndGet();
            this.b();
         } else {
            io.reactivex.g.a.a(var2);
         }

      }

      void b() {
         if(this.getAndIncrement() == 0) {
            this.d();
         }

      }

      void c() {
         io.reactivex.d.f.c var1 = (io.reactivex.d.f.c)this.g.get();
         if(var1 != null) {
            var1.c();
         }

      }

      void d() {
         io.reactivex.t var5 = this.a;
         AtomicInteger var7 = this.d;
         AtomicReference var6 = this.g;
         int var1 = 1;

         while(true) {
            if(this.i) {
               this.c();
               break;
            }

            Throwable var9;
            if(!this.b && (Throwable)this.e.get() != null) {
               var9 = this.e.a();
               this.c();
               var5.onError(var9);
               break;
            }

            boolean var2;
            if(var7.get() == 0) {
               var2 = true;
            } else {
               var2 = false;
            }

            io.reactivex.d.f.c var4 = (io.reactivex.d.f.c)var6.get();
            Object var8;
            if(var4 != null) {
               var8 = var4.n_();
            } else {
               var8 = null;
            }

            boolean var3;
            if(var8 == null) {
               var3 = true;
            } else {
               var3 = false;
            }

            if(var2 && var3) {
               var9 = this.e.a();
               if(var9 != null) {
                  var5.onError(var9);
               } else {
                  var5.onComplete();
               }
               break;
            }

            if(var3) {
               var1 = this.addAndGet(-var1);
               if(var1 != 0) {
                  continue;
               }
               break;
            } else {
               var5.onNext(var8);
            }
         }

      }

      public void dispose() {
         this.i = true;
         this.h.dispose();
         this.c.dispose();
      }

      public boolean isDisposed() {
         return this.i;
      }

      public void onComplete() {
         this.d.decrementAndGet();
         this.b();
      }

      public void onError(Throwable var1) {
         this.d.decrementAndGet();
         if(this.e.a(var1)) {
            if(!this.b) {
               this.c.dispose();
            }

            this.b();
         } else {
            io.reactivex.g.a.a(var1);
         }

      }

      public void onNext(Object var1) {
         io.reactivex.z var4;
         try {
            var4 = (io.reactivex.z)io.reactivex.d.b.b.a(this.f.a(var1), "The mapper returned a null SingleSource");
         } catch (Throwable var3) {
            io.reactivex.exceptions.a.b(var3);
            this.h.dispose();
            this.onError(var3);
            return;
         }

         this.d.getAndIncrement();
         ax.a var2 = new ax.a();
         if(!this.i && this.c.a((io.reactivex.b.b)var2)) {
            var4.a(var2);
         }

      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.h, var1)) {
            this.h = var1;
            this.a.onSubscribe(this);
         }

      }
   }

   final class a extends AtomicReference implements io.reactivex.b.b, io.reactivex.x {
      public void a_(Object var1) {
         ax.this.a(this, var1);
      }

      public void dispose() {
         io.reactivex.d.a.d.a((AtomicReference)this);
      }

      public boolean isDisposed() {
         return io.reactivex.d.a.d.a((io.reactivex.b.b)this.get());
      }

      public void onError(Throwable var1) {
         ax.this.a(this, var1);
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         io.reactivex.d.a.d.b(this, var1);
      }
   }
}
