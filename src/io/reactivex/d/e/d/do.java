package io.reactivex.d.e.d;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

public final class do extends a {
   final long b;
   final TimeUnit c;
   final io.reactivex.u d;

   public do(io.reactivex.r var1, long var2, TimeUnit var4, io.reactivex.u var5) {
      super(var1);
      this.b = var2;
      this.c = var4;
      this.d = var5;
   }

   public void subscribeActual(io.reactivex.t var1) {
      this.a.subscribe(new do.a(new io.reactivex.f.e(var1), this.b, this.c, this.d.a()));
   }

   static final class a extends AtomicReference implements io.reactivex.b.b, io.reactivex.t, Runnable {
      final io.reactivex.t a;
      final long b;
      final TimeUnit c;
      final io.reactivex.u.c d;
      io.reactivex.b.b e;
      volatile boolean f;
      boolean g;

      a(io.reactivex.t var1, long var2, TimeUnit var4, io.reactivex.u.c var5) {
         this.a = var1;
         this.b = var2;
         this.c = var4;
         this.d = var5;
      }

      public void dispose() {
         this.e.dispose();
         this.d.dispose();
      }

      public boolean isDisposed() {
         return this.d.isDisposed();
      }

      public void onComplete() {
         if(!this.g) {
            this.g = true;
            this.a.onComplete();
            this.d.dispose();
         }

      }

      public void onError(Throwable var1) {
         if(this.g) {
            io.reactivex.g.a.a(var1);
         } else {
            this.g = true;
            this.a.onError(var1);
            this.d.dispose();
         }

      }

      public void onNext(Object var1) {
         if(!this.f && !this.g) {
            this.f = true;
            this.a.onNext(var1);
            io.reactivex.b.b var2 = (io.reactivex.b.b)this.get();
            if(var2 != null) {
               var2.dispose();
            }

            io.reactivex.d.a.d.c(this, this.d.a(this, this.b, this.c));
         }

      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.e, var1)) {
            this.e = var1;
            this.a.onSubscribe(this);
         }

      }

      public void run() {
         this.f = false;
      }
   }
}
