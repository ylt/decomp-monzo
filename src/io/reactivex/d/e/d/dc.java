package io.reactivex.d.e.d;

public final class dc extends a {
   final io.reactivex.r b;

   public dc(io.reactivex.r var1, io.reactivex.r var2) {
      super(var1);
      this.b = var2;
   }

   public void subscribeActual(io.reactivex.t var1) {
      io.reactivex.f.e var3 = new io.reactivex.f.e(var1);
      io.reactivex.d.a.a var2 = new io.reactivex.d.a.a(2);
      var3.onSubscribe(var2);
      dc.b var4 = new dc.b(var3, var2);
      this.b.subscribe(new dc.a(var2, var4, var3));
      this.a.subscribe(var4);
   }

   final class a implements io.reactivex.t {
      io.reactivex.b.b a;
      private final io.reactivex.d.a.a c;
      private final dc.b d;
      private final io.reactivex.f.e e;

      a(io.reactivex.d.a.a var2, dc.b var3, io.reactivex.f.e var4) {
         this.c = var2;
         this.d = var3;
         this.e = var4;
      }

      public void onComplete() {
         this.d.d = true;
      }

      public void onError(Throwable var1) {
         this.c.dispose();
         this.e.onError(var1);
      }

      public void onNext(Object var1) {
         this.a.dispose();
         this.d.d = true;
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.a, var1)) {
            this.a = var1;
            this.c.a(1, var1);
         }

      }
   }

   static final class b implements io.reactivex.t {
      final io.reactivex.t a;
      final io.reactivex.d.a.a b;
      io.reactivex.b.b c;
      volatile boolean d;
      boolean e;

      b(io.reactivex.t var1, io.reactivex.d.a.a var2) {
         this.a = var1;
         this.b = var2;
      }

      public void onComplete() {
         this.b.dispose();
         this.a.onComplete();
      }

      public void onError(Throwable var1) {
         this.b.dispose();
         this.a.onError(var1);
      }

      public void onNext(Object var1) {
         if(this.e) {
            this.a.onNext(var1);
         } else if(this.d) {
            this.e = true;
            this.a.onNext(var1);
         }

      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.c, var1)) {
            this.c = var1;
            this.b.a(0, var1);
         }

      }
   }
}
