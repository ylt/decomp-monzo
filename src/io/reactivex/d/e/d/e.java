package io.reactivex.d.e.d;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public final class e implements Iterable {
   final io.reactivex.r a;

   public e(io.reactivex.r var1) {
      this.a = var1;
   }

   public Iterator iterator() {
      e.b var1 = new e.b();
      return new e.a(this.a, var1);
   }

   static final class a implements Iterator {
      private final e.b a;
      private final io.reactivex.r b;
      private Object c;
      private boolean d = true;
      private boolean e = true;
      private Throwable f;
      private boolean g;

      a(io.reactivex.r var1, e.b var2) {
         this.b = var1;
         this.a = var2;
      }

      private boolean a() {
         boolean var1 = true;
         if(!this.g) {
            this.g = true;
            this.a.b();
            (new bv(this.b)).subscribe(this.a);
         }

         io.reactivex.m var2;
         try {
            var2 = this.a.a();
         } catch (InterruptedException var3) {
            this.a.dispose();
            this.f = var3;
            throw io.reactivex.d.j.j.a((Throwable)var3);
         }

         if(var2.c()) {
            this.e = false;
            this.c = var2.d();
         } else {
            this.d = false;
            if(!var2.a()) {
               this.f = var2.e();
               throw io.reactivex.d.j.j.a(this.f);
            }

            var1 = false;
         }

         return var1;
      }

      public boolean hasNext() {
         boolean var1 = false;
         if(this.f != null) {
            throw io.reactivex.d.j.j.a(this.f);
         } else {
            if(this.d && (!this.e || this.a())) {
               var1 = true;
            }

            return var1;
         }
      }

      public Object next() {
         if(this.f != null) {
            throw io.reactivex.d.j.j.a(this.f);
         } else if(this.hasNext()) {
            this.e = true;
            return this.c;
         } else {
            throw new NoSuchElementException("No more elements");
         }
      }

      public void remove() {
         throw new UnsupportedOperationException("Read only iterator");
      }
   }

   static final class b extends io.reactivex.f.c {
      final AtomicInteger a = new AtomicInteger();
      private final BlockingQueue b = new ArrayBlockingQueue(1);

      public io.reactivex.m a() throws InterruptedException {
         this.b();
         io.reactivex.d.j.e.a();
         return (io.reactivex.m)this.b.take();
      }

      public void a(io.reactivex.m var1) {
         io.reactivex.m var2 = var1;
         if(this.a.getAndSet(0) != 1) {
            if(var1.c()) {
               return;
            }

            var2 = var1;
         }

         while(!this.b.offer(var2)) {
            var1 = (io.reactivex.m)this.b.poll();
            if(var1 != null && !var1.c()) {
               var2 = var1;
            }
         }

      }

      void b() {
         this.a.set(1);
      }

      public void onComplete() {
      }

      public void onError(Throwable var1) {
         io.reactivex.g.a.a(var1);
      }

      // $FF: synthetic method
      public void onNext(Object var1) {
         this.a((io.reactivex.m)var1);
      }
   }
}
