package io.reactivex.d.e.d;

import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicReference;

public final class dq extends a {
   final io.reactivex.r b;
   final io.reactivex.c.h c;
   final io.reactivex.r d;

   public dq(io.reactivex.r var1, io.reactivex.r var2, io.reactivex.c.h var3, io.reactivex.r var4) {
      super(var1);
      this.b = var2;
      this.c = var3;
      this.d = var4;
   }

   public void subscribeActual(io.reactivex.t var1) {
      if(this.d == null) {
         this.a.subscribe(new dq.c(new io.reactivex.f.e(var1), this.b, this.c));
      } else {
         this.a.subscribe(new dq.d(var1, this.b, this.c, this.d));
      }

   }

   interface a {
      void a(long var1);

      void a(Throwable var1);
   }

   static final class b extends io.reactivex.f.c {
      final dq.a a;
      final long b;
      boolean c;

      b(dq.a var1, long var2) {
         this.a = var1;
         this.b = var2;
      }

      public void onComplete() {
         if(!this.c) {
            this.c = true;
            this.a.a(this.b);
         }

      }

      public void onError(Throwable var1) {
         if(this.c) {
            io.reactivex.g.a.a(var1);
         } else {
            this.c = true;
            this.a.a(var1);
         }

      }

      public void onNext(Object var1) {
         if(!this.c) {
            this.c = true;
            this.dispose();
            this.a.a(this.b);
         }

      }
   }

   static final class c extends AtomicReference implements io.reactivex.b.b, dq.a, io.reactivex.t {
      final io.reactivex.t a;
      final io.reactivex.r b;
      final io.reactivex.c.h c;
      io.reactivex.b.b d;
      volatile long e;

      c(io.reactivex.t var1, io.reactivex.r var2, io.reactivex.c.h var3) {
         this.a = var1;
         this.b = var2;
         this.c = var3;
      }

      public void a(long var1) {
         if(var1 == this.e) {
            this.dispose();
            this.a.onError(new TimeoutException());
         }

      }

      public void a(Throwable var1) {
         this.d.dispose();
         this.a.onError(var1);
      }

      public void dispose() {
         if(io.reactivex.d.a.d.a((AtomicReference)this)) {
            this.d.dispose();
         }

      }

      public boolean isDisposed() {
         return this.d.isDisposed();
      }

      public void onComplete() {
         io.reactivex.d.a.d.a((AtomicReference)this);
         this.a.onComplete();
      }

      public void onError(Throwable var1) {
         io.reactivex.d.a.d.a((AtomicReference)this);
         this.a.onError(var1);
      }

      public void onNext(Object var1) {
         long var2 = 1L + this.e;
         this.e = var2;
         this.a.onNext(var1);
         io.reactivex.b.b var4 = (io.reactivex.b.b)this.get();
         if(var4 != null) {
            var4.dispose();
         }

         io.reactivex.r var5;
         try {
            var5 = (io.reactivex.r)io.reactivex.d.b.b.a(this.c.a(var1), "The ObservableSource returned is null");
         } catch (Throwable var6) {
            io.reactivex.exceptions.a.b(var6);
            this.dispose();
            this.a.onError(var6);
            return;
         }

         dq.b var7 = new dq.b(this, var2);
         if(this.compareAndSet(var4, var7)) {
            var5.subscribe(var7);
         }

      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.d, var1)) {
            this.d = var1;
            io.reactivex.t var2 = this.a;
            io.reactivex.r var3 = this.b;
            if(var3 != null) {
               dq.b var4 = new dq.b(this, 0L);
               if(this.compareAndSet((Object)null, var4)) {
                  var2.onSubscribe(this);
                  var3.subscribe(var4);
               }
            } else {
               var2.onSubscribe(this);
            }
         }

      }
   }

   static final class d extends AtomicReference implements io.reactivex.b.b, dq.a, io.reactivex.t {
      final io.reactivex.t a;
      final io.reactivex.r b;
      final io.reactivex.c.h c;
      final io.reactivex.r d;
      final io.reactivex.d.a.j e;
      io.reactivex.b.b f;
      boolean g;
      volatile long h;

      d(io.reactivex.t var1, io.reactivex.r var2, io.reactivex.c.h var3, io.reactivex.r var4) {
         this.a = var1;
         this.b = var2;
         this.c = var3;
         this.d = var4;
         this.e = new io.reactivex.d.a.j(var1, this, 8);
      }

      public void a(long var1) {
         if(var1 == this.h) {
            this.dispose();
            this.d.subscribe(new io.reactivex.d.d.o(this.e));
         }

      }

      public void a(Throwable var1) {
         this.f.dispose();
         this.a.onError(var1);
      }

      public void dispose() {
         if(io.reactivex.d.a.d.a((AtomicReference)this)) {
            this.f.dispose();
         }

      }

      public boolean isDisposed() {
         return this.f.isDisposed();
      }

      public void onComplete() {
         if(!this.g) {
            this.g = true;
            this.dispose();
            this.e.b(this.f);
         }

      }

      public void onError(Throwable var1) {
         if(this.g) {
            io.reactivex.g.a.a(var1);
         } else {
            this.g = true;
            this.dispose();
            this.e.a(var1, this.f);
         }

      }

      public void onNext(Object var1) {
         if(!this.g) {
            long var2 = 1L + this.h;
            this.h = var2;
            if(this.e.a(var1, this.f)) {
               io.reactivex.b.b var4 = (io.reactivex.b.b)this.get();
               if(var4 != null) {
                  var4.dispose();
               }

               io.reactivex.r var5;
               try {
                  var5 = (io.reactivex.r)io.reactivex.d.b.b.a(this.c.a(var1), "The ObservableSource returned is null");
               } catch (Throwable var6) {
                  io.reactivex.exceptions.a.b(var6);
                  this.a.onError(var6);
                  return;
               }

               dq.b var7 = new dq.b(this, var2);
               if(this.compareAndSet(var4, var7)) {
                  var5.subscribe(var7);
               }
            }
         }

      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.f, var1)) {
            this.f = var1;
            this.e.a(var1);
            io.reactivex.t var2 = this.a;
            io.reactivex.r var4 = this.b;
            if(var4 != null) {
               dq.b var3 = new dq.b(this, 0L);
               if(this.compareAndSet((Object)null, var3)) {
                  var2.onSubscribe(this.e);
                  var4.subscribe(var3);
               }
            } else {
               var2.onSubscribe(this.e);
            }
         }

      }
   }
}
