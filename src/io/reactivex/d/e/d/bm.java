package io.reactivex.d.e.d;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

public final class bm extends io.reactivex.n {
   final io.reactivex.u a;
   final long b;
   final long c;
   final TimeUnit d;

   public bm(long var1, long var3, TimeUnit var5, io.reactivex.u var6) {
      this.b = var1;
      this.c = var3;
      this.d = var5;
      this.a = var6;
   }

   public void subscribeActual(io.reactivex.t var1) {
      bm.a var2 = new bm.a(var1);
      var1.onSubscribe(var2);
      io.reactivex.u var3 = this.a;
      if(var3 instanceof io.reactivex.d.g.n) {
         io.reactivex.u.c var4 = var3.a();
         var2.a(var4);
         var4.a(var2, this.b, this.c, this.d);
      } else {
         var2.a(var3.a(var2, this.b, this.c, this.d));
      }

   }

   static final class a extends AtomicReference implements io.reactivex.b.b, Runnable {
      final io.reactivex.t a;
      long b;

      a(io.reactivex.t var1) {
         this.a = var1;
      }

      public void a(io.reactivex.b.b var1) {
         io.reactivex.d.a.d.b(this, var1);
      }

      public void dispose() {
         io.reactivex.d.a.d.a((AtomicReference)this);
      }

      public boolean isDisposed() {
         boolean var1;
         if(this.get() == io.reactivex.d.a.d.a) {
            var1 = true;
         } else {
            var1 = false;
         }

         return var1;
      }

      public void run() {
         if(this.get() != io.reactivex.d.a.d.a) {
            io.reactivex.t var3 = this.a;
            long var1 = this.b;
            this.b = 1L + var1;
            var3.onNext(Long.valueOf(var1));
         }

      }
   }
}
