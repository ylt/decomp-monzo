package io.reactivex.d.e.d;

import java.util.NoSuchElementException;

public final class br extends io.reactivex.v {
   final io.reactivex.r a;
   final Object b;

   public br(io.reactivex.r var1, Object var2) {
      this.a = var1;
      this.b = var2;
   }

   protected void b(io.reactivex.x var1) {
      this.a.subscribe(new br.a(var1, this.b));
   }

   static final class a implements io.reactivex.b.b, io.reactivex.t {
      final io.reactivex.x a;
      final Object b;
      io.reactivex.b.b c;
      Object d;

      a(io.reactivex.x var1, Object var2) {
         this.a = var1;
         this.b = var2;
      }

      public void dispose() {
         this.c.dispose();
         this.c = io.reactivex.d.a.d.a;
      }

      public boolean isDisposed() {
         boolean var1;
         if(this.c == io.reactivex.d.a.d.a) {
            var1 = true;
         } else {
            var1 = false;
         }

         return var1;
      }

      public void onComplete() {
         this.c = io.reactivex.d.a.d.a;
         Object var1 = this.d;
         if(var1 != null) {
            this.d = null;
            this.a.a_(var1);
         } else {
            var1 = this.b;
            if(var1 != null) {
               this.a.a_(var1);
            } else {
               this.a.onError(new NoSuchElementException());
            }
         }

      }

      public void onError(Throwable var1) {
         this.c = io.reactivex.d.a.d.a;
         this.d = null;
         this.a.onError(var1);
      }

      public void onNext(Object var1) {
         this.d = var1;
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.c, var1)) {
            this.c = var1;
            this.a.onSubscribe(this);
         }

      }
   }
}
