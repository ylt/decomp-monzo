package io.reactivex.d.e.d;

import java.util.concurrent.TimeUnit;

public final class ad extends a {
   final long b;
   final TimeUnit c;
   final io.reactivex.u d;
   final boolean e;

   public ad(io.reactivex.r var1, long var2, TimeUnit var4, io.reactivex.u var5, boolean var6) {
      super(var1);
      this.b = var2;
      this.c = var4;
      this.d = var5;
      this.e = var6;
   }

   public void subscribeActual(io.reactivex.t var1) {
      if(!this.e) {
         var1 = new io.reactivex.f.e((io.reactivex.t)var1);
      }

      io.reactivex.u.c var2 = this.d.a();
      this.a.subscribe(new ad.a((io.reactivex.t)var1, this.b, this.c, var2, this.e));
   }

   static final class a implements io.reactivex.b.b, io.reactivex.t {
      final io.reactivex.t a;
      final long b;
      final TimeUnit c;
      final io.reactivex.u.c d;
      final boolean e;
      io.reactivex.b.b f;

      a(io.reactivex.t var1, long var2, TimeUnit var4, io.reactivex.u.c var5, boolean var6) {
         this.a = var1;
         this.b = var2;
         this.c = var4;
         this.d = var5;
         this.e = var6;
      }

      public void dispose() {
         this.f.dispose();
         this.d.dispose();
      }

      public boolean isDisposed() {
         return this.d.isDisposed();
      }

      public void onComplete() {
         this.d.a(new ad.a(), this.b, this.c);
      }

      public void onError(Throwable var1) {
         io.reactivex.u.c var4 = this.d;
         ad.b var5 = new ad.b(var1);
         long var2;
         if(this.e) {
            var2 = this.b;
         } else {
            var2 = 0L;
         }

         var4.a(var5, var2, this.c);
      }

      public void onNext(Object var1) {
         this.d.a(new ad.c(var1), this.b, this.c);
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.f, var1)) {
            this.f = var1;
            this.a.onSubscribe(this);
         }

      }
   }

   final class a implements Runnable {
      public void run() {
         try {
            ad.super.a.onComplete();
         } finally {
            ad.super.d.dispose();
         }

      }
   }

   final class b implements Runnable {
      private final Throwable b;

      b(Throwable var2) {
         this.b = var2;
      }

      public void run() {
         try {
            ad.super.a.onError(this.b);
         } finally {
            ad.super.d.dispose();
         }

      }
   }

   final class c implements Runnable {
      private final Object b;

      c(Object var2) {
         this.b = var2;
      }

      public void run() {
         ad.super.a.onNext(this.b);
      }
   }
}
