package io.reactivex.d.e.d;

public final class bd extends io.reactivex.n {
   final org.a.a a;

   public bd(org.a.a var1) {
      this.a = var1;
   }

   protected void subscribeActual(io.reactivex.t var1) {
      this.a.a(new bd.a(var1));
   }

   static final class a implements io.reactivex.b.b, io.reactivex.g {
      final io.reactivex.t a;
      org.a.c b;

      a(io.reactivex.t var1) {
         this.a = var1;
      }

      public void a(org.a.c var1) {
         if(io.reactivex.d.i.d.a(this.b, var1)) {
            this.b = var1;
            this.a.onSubscribe(this);
            var1.a(Long.MAX_VALUE);
         }

      }

      public void dispose() {
         this.b.a();
         this.b = io.reactivex.d.i.d.a;
      }

      public boolean isDisposed() {
         boolean var1;
         if(this.b == io.reactivex.d.i.d.a) {
            var1 = true;
         } else {
            var1 = false;
         }

         return var1;
      }

      public void onComplete() {
         this.a.onComplete();
      }

      public void onError(Throwable var1) {
         this.a.onError(var1);
      }

      public void onNext(Object var1) {
         this.a.onNext(var1);
      }
   }
}
