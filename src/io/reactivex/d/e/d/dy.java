package io.reactivex.d.e.d;

import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

public final class dy extends a {
   final io.reactivex.r b;
   final int c;

   public dy(io.reactivex.r var1, io.reactivex.r var2, int var3) {
      super(var1);
      this.b = var2;
      this.c = var3;
   }

   public void subscribeActual(io.reactivex.t var1) {
      this.a.subscribe(new dy.b(new io.reactivex.f.e(var1), this.b, this.c));
   }

   static final class a extends io.reactivex.f.c {
      final dy.b a;
      boolean b;

      a(dy.b var1) {
         this.a = var1;
      }

      public void onComplete() {
         if(!this.b) {
            this.b = true;
            this.a.onComplete();
         }

      }

      public void onError(Throwable var1) {
         if(this.b) {
            io.reactivex.g.a.a(var1);
         } else {
            this.b = true;
            this.a.onError(var1);
         }

      }

      public void onNext(Object var1) {
         if(!this.b) {
            this.a.g();
         }

      }
   }

   static final class b extends io.reactivex.d.d.t implements io.reactivex.b.b {
      static final Object l = new Object();
      final io.reactivex.r g;
      final int h;
      io.reactivex.b.b i;
      final AtomicReference j = new AtomicReference();
      io.reactivex.i.d k;
      final AtomicLong m = new AtomicLong();

      b(io.reactivex.t var1, io.reactivex.r var2, int var3) {
         super(var1, new io.reactivex.d.f.a());
         this.g = var2;
         this.h = var3;
         this.m.lazySet(1L);
      }

      public void dispose() {
         this.c = true;
      }

      void f() {
         io.reactivex.d.f.a var6 = (io.reactivex.d.f.a)this.b;
         io.reactivex.t var5 = this.a;
         io.reactivex.i.d var4 = this.k;
         int var1 = 1;

         while(true) {
            boolean var3 = this.d;
            Object var7 = var6.n_();
            boolean var2;
            if(var7 == null) {
               var2 = true;
            } else {
               var2 = false;
            }

            if(var3 && var2) {
               io.reactivex.d.a.d.a(this.j);
               Throwable var9 = this.e;
               if(var9 != null) {
                  var4.onError(var9);
               } else {
                  var4.onComplete();
               }
               break;
            }

            if(var2) {
               int var8 = this.a(-var1);
               var1 = var8;
               if(var8 == 0) {
                  break;
               }
            } else if(var7 == l) {
               var4.onComplete();
               if(this.m.decrementAndGet() == 0L) {
                  io.reactivex.d.a.d.a(this.j);
                  break;
               }

               if(!this.c) {
                  var4 = io.reactivex.i.d.a(this.h);
                  this.m.getAndIncrement();
                  this.k = var4;
                  var5.onNext(var4);
               }
            } else {
               var4.onNext(io.reactivex.d.j.n.e(var7));
            }
         }

      }

      void g() {
         this.b.a(l);
         if(this.c()) {
            this.f();
         }

      }

      public boolean isDisposed() {
         return this.c;
      }

      public void onComplete() {
         if(!this.d) {
            this.d = true;
            if(this.c()) {
               this.f();
            }

            if(this.m.decrementAndGet() == 0L) {
               io.reactivex.d.a.d.a(this.j);
            }

            this.a.onComplete();
         }

      }

      public void onError(Throwable var1) {
         if(this.d) {
            io.reactivex.g.a.a(var1);
         } else {
            this.e = var1;
            this.d = true;
            if(this.c()) {
               this.f();
            }

            if(this.m.decrementAndGet() == 0L) {
               io.reactivex.d.a.d.a(this.j);
            }

            this.a.onError(var1);
         }

      }

      public void onNext(Object var1) {
         if(this.d()) {
            this.k.onNext(var1);
            if(this.a(-1) == 0) {
               return;
            }
         } else {
            this.b.a(io.reactivex.d.j.n.a(var1));
            if(!this.c()) {
               return;
            }
         }

         this.f();
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.i, var1)) {
            this.i = var1;
            io.reactivex.t var3 = this.a;
            var3.onSubscribe(this);
            if(!this.c) {
               io.reactivex.i.d var2 = io.reactivex.i.d.a(this.h);
               this.k = var2;
               var3.onNext(var2);
               dy.a var4 = new dy.a(this);
               if(this.j.compareAndSet((Object)null, var4)) {
                  this.m.getAndIncrement();
                  this.g.subscribe(var4);
               }
            }
         }

      }
   }
}
