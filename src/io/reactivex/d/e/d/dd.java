package io.reactivex.d.e.d;

public final class dd extends a {
   final io.reactivex.c.q b;

   public dd(io.reactivex.r var1, io.reactivex.c.q var2) {
      super(var1);
      this.b = var2;
   }

   public void subscribeActual(io.reactivex.t var1) {
      this.a.subscribe(new dd.a(var1, this.b));
   }

   static final class a implements io.reactivex.b.b, io.reactivex.t {
      final io.reactivex.t a;
      final io.reactivex.c.q b;
      io.reactivex.b.b c;
      boolean d;

      a(io.reactivex.t var1, io.reactivex.c.q var2) {
         this.a = var1;
         this.b = var2;
      }

      public void dispose() {
         this.c.dispose();
      }

      public boolean isDisposed() {
         return this.c.isDisposed();
      }

      public void onComplete() {
         this.a.onComplete();
      }

      public void onError(Throwable var1) {
         this.a.onError(var1);
      }

      public void onNext(Object var1) {
         if(this.d) {
            this.a.onNext(var1);
         } else {
            boolean var2;
            try {
               var2 = this.b.a(var1);
            } catch (Throwable var3) {
               io.reactivex.exceptions.a.b(var3);
               this.c.dispose();
               this.a.onError(var3);
               return;
            }

            if(!var2) {
               this.d = true;
               this.a.onNext(var1);
            }
         }

      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.c, var1)) {
            this.c = var1;
            this.a.onSubscribe(this);
         }

      }
   }
}
