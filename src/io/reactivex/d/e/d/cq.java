package io.reactivex.d.e.d;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public final class cq extends a {
   final io.reactivex.r b;
   final boolean c;

   public cq(io.reactivex.r var1, io.reactivex.r var2, boolean var3) {
      super(var1);
      this.b = var2;
      this.c = var3;
   }

   public void subscribeActual(io.reactivex.t var1) {
      io.reactivex.f.e var2 = new io.reactivex.f.e(var1);
      if(this.c) {
         this.a.subscribe(new cq.a(var2, this.b));
      } else {
         this.a.subscribe(new cq.b(var2, this.b));
      }

   }

   static final class a extends cq.c {
      final AtomicInteger a = new AtomicInteger();
      volatile boolean b;

      a(io.reactivex.t var1, io.reactivex.r var2) {
         super(var1, var2);
      }

      void a() {
         this.b = true;
         if(this.a.getAndIncrement() == 0) {
            this.e();
            this.c.onComplete();
         }

      }

      void b() {
         this.b = true;
         if(this.a.getAndIncrement() == 0) {
            this.e();
            this.c.onComplete();
         }

      }

      void c() {
         if(this.a.getAndIncrement() == 0) {
            do {
               boolean var1 = this.b;
               this.e();
               if(var1) {
                  this.c.onComplete();
                  break;
               }
            } while(this.a.decrementAndGet() != 0);
         }

      }
   }

   static final class b extends cq.c {
      b(io.reactivex.t var1, io.reactivex.r var2) {
         super(var1, var2);
      }

      void a() {
         this.c.onComplete();
      }

      void b() {
         this.c.onComplete();
      }

      void c() {
         this.e();
      }
   }

   abstract static class c extends AtomicReference implements io.reactivex.b.b, io.reactivex.t {
      final io.reactivex.t c;
      final io.reactivex.r d;
      final AtomicReference e = new AtomicReference();
      io.reactivex.b.b f;

      c(io.reactivex.t var1, io.reactivex.r var2) {
         this.c = var1;
         this.d = var2;
      }

      abstract void a();

      public void a(Throwable var1) {
         this.f.dispose();
         this.c.onError(var1);
      }

      boolean a(io.reactivex.b.b var1) {
         return io.reactivex.d.a.d.b(this.e, var1);
      }

      abstract void b();

      abstract void c();

      public void d() {
         this.f.dispose();
         this.b();
      }

      public void dispose() {
         io.reactivex.d.a.d.a(this.e);
         this.f.dispose();
      }

      void e() {
         Object var1 = this.getAndSet((Object)null);
         if(var1 != null) {
            this.c.onNext(var1);
         }

      }

      public boolean isDisposed() {
         boolean var1;
         if(this.e.get() == io.reactivex.d.a.d.a) {
            var1 = true;
         } else {
            var1 = false;
         }

         return var1;
      }

      public void onComplete() {
         io.reactivex.d.a.d.a(this.e);
         this.a();
      }

      public void onError(Throwable var1) {
         io.reactivex.d.a.d.a(this.e);
         this.c.onError(var1);
      }

      public void onNext(Object var1) {
         this.lazySet(var1);
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.f, var1)) {
            this.f = var1;
            this.c.onSubscribe(this);
            if(this.e.get() == null) {
               this.d.subscribe(new cq.d(this));
            }
         }

      }
   }

   static final class d implements io.reactivex.t {
      final cq.c a;

      d(cq.c var1) {
         this.a = var1;
      }

      public void onComplete() {
         this.a.d();
      }

      public void onError(Throwable var1) {
         this.a.a(var1);
      }

      public void onNext(Object var1) {
         this.a.c();
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         this.a.a(var1);
      }
   }
}
