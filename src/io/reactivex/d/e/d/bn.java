package io.reactivex.d.e.d;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

public final class bn extends io.reactivex.n {
   final io.reactivex.u a;
   final long b;
   final long c;
   final long d;
   final long e;
   final TimeUnit f;

   public bn(long var1, long var3, long var5, long var7, TimeUnit var9, io.reactivex.u var10) {
      this.d = var5;
      this.e = var7;
      this.f = var9;
      this.a = var10;
      this.b = var1;
      this.c = var3;
   }

   public void subscribeActual(io.reactivex.t var1) {
      bn.a var2 = new bn.a(var1, this.b, this.c);
      var1.onSubscribe(var2);
      io.reactivex.u var3 = this.a;
      if(var3 instanceof io.reactivex.d.g.n) {
         io.reactivex.u.c var4 = var3.a();
         var2.a(var4);
         var4.a(var2, this.d, this.e, this.f);
      } else {
         var2.a(var3.a(var2, this.d, this.e, this.f));
      }

   }

   static final class a extends AtomicReference implements io.reactivex.b.b, Runnable {
      final io.reactivex.t a;
      final long b;
      long c;

      a(io.reactivex.t var1, long var2, long var4) {
         this.a = var1;
         this.c = var2;
         this.b = var4;
      }

      public void a(io.reactivex.b.b var1) {
         io.reactivex.d.a.d.b(this, var1);
      }

      public void dispose() {
         io.reactivex.d.a.d.a((AtomicReference)this);
      }

      public boolean isDisposed() {
         boolean var1;
         if(this.get() == io.reactivex.d.a.d.a) {
            var1 = true;
         } else {
            var1 = false;
         }

         return var1;
      }

      public void run() {
         if(!this.isDisposed()) {
            long var1 = this.c;
            this.a.onNext(Long.valueOf(var1));
            if(var1 == this.b) {
               io.reactivex.d.a.d.a((AtomicReference)this);
               this.a.onComplete();
            } else {
               this.c = var1 + 1L;
            }
         }

      }
   }
}
