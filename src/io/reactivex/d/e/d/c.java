package io.reactivex.d.e.d;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicReference;

public final class c implements Iterable {
   final io.reactivex.r a;

   public c(io.reactivex.r var1) {
      this.a = var1;
   }

   public Iterator iterator() {
      c.a var1 = new c.a();
      io.reactivex.n.wrap(this.a).materialize().subscribe((io.reactivex.t)var1);
      return var1;
   }

   static final class a extends io.reactivex.f.c implements Iterator {
      io.reactivex.m a;
      final Semaphore b = new Semaphore(0);
      final AtomicReference c = new AtomicReference();

      public void a(io.reactivex.m var1) {
         boolean var2;
         if(this.c.getAndSet(var1) == null) {
            var2 = true;
         } else {
            var2 = false;
         }

         if(var2) {
            this.b.release();
         }

      }

      public boolean hasNext() {
         if(this.a != null && this.a.b()) {
            throw io.reactivex.d.j.j.a(this.a.e());
         } else {
            if(this.a == null) {
               try {
                  io.reactivex.d.j.e.a();
                  this.b.acquire();
               } catch (InterruptedException var2) {
                  this.dispose();
                  this.a = io.reactivex.m.a((Throwable)var2);
                  throw io.reactivex.d.j.j.a((Throwable)var2);
               }

               io.reactivex.m var1 = (io.reactivex.m)this.c.getAndSet((Object)null);
               this.a = var1;
               if(var1.b()) {
                  throw io.reactivex.d.j.j.a(var1.e());
               }
            }

            return this.a.c();
         }
      }

      public Object next() {
         if(this.hasNext()) {
            Object var1 = this.a.d();
            this.a = null;
            return var1;
         } else {
            throw new NoSuchElementException();
         }
      }

      public void onComplete() {
      }

      public void onError(Throwable var1) {
         io.reactivex.g.a.a(var1);
      }

      // $FF: synthetic method
      public void onNext(Object var1) {
         this.a((io.reactivex.m)var1);
      }

      public void remove() {
         throw new UnsupportedOperationException("Read-only iterator.");
      }
   }
}
