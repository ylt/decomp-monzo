package io.reactivex.d.e.d;

import java.util.ArrayDeque;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public final class dx extends a {
   final long b;
   final long c;
   final int d;

   public dx(io.reactivex.r var1, long var2, long var4, int var6) {
      super(var1);
      this.b = var2;
      this.c = var4;
      this.d = var6;
   }

   public void subscribeActual(io.reactivex.t var1) {
      if(this.b == this.c) {
         this.a.subscribe(new dx.a(var1, this.b, this.d));
      } else {
         this.a.subscribe(new dx.b(var1, this.b, this.c, this.d));
      }

   }

   static final class a extends AtomicInteger implements io.reactivex.b.b, io.reactivex.t, Runnable {
      final io.reactivex.t a;
      final long b;
      final int c;
      long d;
      io.reactivex.b.b e;
      io.reactivex.i.d f;
      volatile boolean g;

      a(io.reactivex.t var1, long var2, int var4) {
         this.a = var1;
         this.b = var2;
         this.c = var4;
      }

      public void dispose() {
         this.g = true;
      }

      public boolean isDisposed() {
         return this.g;
      }

      public void onComplete() {
         io.reactivex.i.d var1 = this.f;
         if(var1 != null) {
            this.f = null;
            var1.onComplete();
         }

         this.a.onComplete();
      }

      public void onError(Throwable var1) {
         io.reactivex.i.d var2 = this.f;
         if(var2 != null) {
            this.f = null;
            var2.onError(var1);
         }

         this.a.onError(var1);
      }

      public void onNext(Object var1) {
         io.reactivex.i.d var5 = this.f;
         io.reactivex.i.d var4 = var5;
         if(var5 == null) {
            var4 = var5;
            if(!this.g) {
               var4 = io.reactivex.i.d.a(this.c, this);
               this.f = var4;
               this.a.onNext(var4);
            }
         }

         if(var4 != null) {
            var4.onNext(var1);
            long var2 = this.d + 1L;
            this.d = var2;
            if(var2 >= this.b) {
               this.d = 0L;
               this.f = null;
               var4.onComplete();
               if(this.g) {
                  this.e.dispose();
               }
            }
         }

      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.e, var1)) {
            this.e = var1;
            this.a.onSubscribe(this);
         }

      }

      public void run() {
         if(this.g) {
            this.e.dispose();
         }

      }
   }

   static final class b extends AtomicBoolean implements io.reactivex.b.b, io.reactivex.t, Runnable {
      final io.reactivex.t a;
      final long b;
      final long c;
      final int d;
      final ArrayDeque e;
      long f;
      volatile boolean g;
      long h;
      io.reactivex.b.b i;
      final AtomicInteger j = new AtomicInteger();

      b(io.reactivex.t var1, long var2, long var4, int var6) {
         this.a = var1;
         this.b = var2;
         this.c = var4;
         this.d = var6;
         this.e = new ArrayDeque();
      }

      public void dispose() {
         this.g = true;
      }

      public boolean isDisposed() {
         return this.g;
      }

      public void onComplete() {
         ArrayDeque var1 = this.e;

         while(!var1.isEmpty()) {
            ((io.reactivex.i.d)var1.poll()).onComplete();
         }

         this.a.onComplete();
      }

      public void onError(Throwable var1) {
         ArrayDeque var2 = this.e;

         while(!var2.isEmpty()) {
            ((io.reactivex.i.d)var2.poll()).onError(var1);
         }

         this.a.onError(var1);
      }

      public void onNext(Object var1) {
         ArrayDeque var8 = this.e;
         long var6 = this.f;
         long var4 = this.c;
         if(var6 % var4 == 0L && !this.g) {
            this.j.getAndIncrement();
            io.reactivex.i.d var9 = io.reactivex.i.d.a(this.d, this);
            var8.offer(var9);
            this.a.onNext(var9);
         }

         long var2 = this.h + 1L;
         Iterator var10 = var8.iterator();

         while(var10.hasNext()) {
            ((io.reactivex.i.d)var10.next()).onNext(var1);
         }

         if(var2 >= this.b) {
            ((io.reactivex.i.d)var8.poll()).onComplete();
            if(var8.isEmpty() && this.g) {
               this.i.dispose();
               return;
            }

            this.h = var2 - var4;
         } else {
            this.h = var2;
         }

         this.f = var6 + 1L;
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.i, var1)) {
            this.i = var1;
            this.a.onSubscribe(this);
         }

      }

      public void run() {
         if(this.j.decrementAndGet() == 0 && this.g) {
            this.i.dispose();
         }

      }
   }
}
