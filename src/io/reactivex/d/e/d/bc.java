package io.reactivex.d.e.d;

import java.util.Iterator;

public final class bc extends io.reactivex.n {
   final Iterable a;

   public bc(Iterable var1) {
      this.a = var1;
   }

   public void subscribeActual(io.reactivex.t var1) {
      Iterator var3;
      try {
         var3 = this.a.iterator();
      } catch (Throwable var5) {
         io.reactivex.exceptions.a.b(var5);
         io.reactivex.d.a.e.a(var5, var1);
         return;
      }

      boolean var2;
      try {
         var2 = var3.hasNext();
      } catch (Throwable var4) {
         io.reactivex.exceptions.a.b(var4);
         io.reactivex.d.a.e.a(var4, var1);
         return;
      }

      if(!var2) {
         io.reactivex.d.a.e.a(var1);
      } else {
         bc.a var6 = new bc.a(var1, var3);
         var1.onSubscribe(var6);
         if(!var6.d) {
            var6.d();
         }
      }

   }

   static final class a extends io.reactivex.d.d.c {
      final io.reactivex.t a;
      final Iterator b;
      volatile boolean c;
      boolean d;
      boolean e;
      boolean f;

      a(io.reactivex.t var1, Iterator var2) {
         this.a = var1;
         this.b = var2;
      }

      public int a(int var1) {
         byte var2 = 1;
         byte var3;
         if((var1 & 1) != 0) {
            this.d = true;
            var3 = var2;
         } else {
            var3 = 0;
         }

         return var3;
      }

      public boolean b() {
         return this.e;
      }

      public void c() {
         this.e = true;
      }

      void d() {
         while(true) {
            if(!this.isDisposed()) {
               Object var2;
               try {
                  var2 = io.reactivex.d.b.b.a(this.b.next(), "The iterator returned a null value");
               } catch (Throwable var4) {
                  io.reactivex.exceptions.a.b(var4);
                  this.a.onError(var4);
                  return;
               }

               this.a.onNext(var2);
               if(!this.isDisposed()) {
                  boolean var1;
                  try {
                     var1 = this.b.hasNext();
                  } catch (Throwable var3) {
                     io.reactivex.exceptions.a.b(var3);
                     this.a.onError(var3);
                     return;
                  }

                  if(var1) {
                     continue;
                  }

                  if(!this.isDisposed()) {
                     this.a.onComplete();
                  }
               }
            }

            return;
         }
      }

      public void dispose() {
         this.c = true;
      }

      public boolean isDisposed() {
         return this.c;
      }

      public Object n_() {
         Object var1 = null;
         if(!this.e) {
            if(this.f) {
               if(!this.b.hasNext()) {
                  this.e = true;
                  return var1;
               }
            } else {
               this.f = true;
            }

            var1 = io.reactivex.d.b.b.a(this.b.next(), "The iterator returned a null value");
         }

         return var1;
      }
   }
}
