package io.reactivex.d.e.d;

public final class bs extends a {
   final io.reactivex.q b;

   public bs(io.reactivex.r var1, io.reactivex.q var2) {
      super(var1);
      this.b = var2;
   }

   public void subscribeActual(io.reactivex.t var1) {
      try {
         io.reactivex.t var2 = this.b.a(var1);
         StringBuilder var6 = new StringBuilder();
         var1 = (io.reactivex.t)io.reactivex.d.b.b.a(var2, (String)var6.append("Operator ").append(this.b).append(" returned a null Observer").toString());
      } catch (NullPointerException var3) {
         throw var3;
      } catch (Throwable var4) {
         io.reactivex.exceptions.a.b(var4);
         io.reactivex.g.a.a(var4);
         NullPointerException var5 = new NullPointerException("Actually not, but can't throw other exceptions due to RS");
         var5.initCause(var4);
         throw var5;
      }

      this.a.subscribe(var1);
   }
}
