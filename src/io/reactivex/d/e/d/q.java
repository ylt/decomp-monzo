package io.reactivex.d.e.d;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

public final class q extends a {
   final long b;
   final long c;
   final TimeUnit d;
   final io.reactivex.u e;
   final Callable f;
   final int g;
   final boolean h;

   public q(io.reactivex.r var1, long var2, long var4, TimeUnit var6, io.reactivex.u var7, Callable var8, int var9, boolean var10) {
      super(var1);
      this.b = var2;
      this.c = var4;
      this.d = var6;
      this.e = var7;
      this.f = var8;
      this.g = var9;
      this.h = var10;
   }

   protected void subscribeActual(io.reactivex.t var1) {
      if(this.b == this.c && this.g == Integer.MAX_VALUE) {
         this.a.subscribe(new q.b(new io.reactivex.f.e(var1), this.f, this.b, this.d, this.e));
      } else {
         io.reactivex.u.c var2 = this.e.a();
         if(this.b == this.c) {
            this.a.subscribe(new q.a(new io.reactivex.f.e(var1), this.f, this.b, this.d, this.g, this.h, var2));
         } else {
            this.a.subscribe(new q.c(new io.reactivex.f.e(var1), this.f, this.b, this.c, this.d, var2));
         }
      }

   }

   static final class a extends io.reactivex.d.d.t implements io.reactivex.b.b, Runnable {
      final Callable g;
      final long h;
      final TimeUnit i;
      final int j;
      final boolean k;
      final io.reactivex.u.c l;
      Collection m;
      io.reactivex.b.b n;
      io.reactivex.b.b o;
      long p;
      long q;

      a(io.reactivex.t var1, Callable var2, long var3, TimeUnit var5, int var6, boolean var7, io.reactivex.u.c var8) {
         super(var1, new io.reactivex.d.f.a());
         this.g = var2;
         this.h = var3;
         this.i = var5;
         this.j = var6;
         this.k = var7;
         this.l = var8;
      }

      public void a(io.reactivex.t var1, Collection var2) {
         var1.onNext(var2);
      }

      public void dispose() {
         // $FF: Couldn't be decompiled
      }

      public boolean isDisposed() {
         return this.c;
      }

      public void onComplete() {
         // $FF: Couldn't be decompiled
      }

      public void onError(Throwable param1) {
         // $FF: Couldn't be decompiled
      }

      public void onNext(Object param1) {
         // $FF: Couldn't be decompiled
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.o, var1)) {
            this.o = var1;

            Collection var2;
            try {
               var2 = (Collection)io.reactivex.d.b.b.a(this.g.call(), "The buffer supplied is null");
            } catch (Throwable var3) {
               io.reactivex.exceptions.a.b(var3);
               var1.dispose();
               io.reactivex.d.a.e.a(var3, this.a);
               this.l.dispose();
               return;
            }

            this.m = var2;
            this.a.onSubscribe(this);
            this.n = this.l.a(this, this.h, this.h, this.i);
         }

      }

      public void run() {
         // $FF: Couldn't be decompiled
      }
   }

   static final class b extends io.reactivex.d.d.t implements io.reactivex.b.b, Runnable {
      final Callable g;
      final long h;
      final TimeUnit i;
      final io.reactivex.u j;
      io.reactivex.b.b k;
      Collection l;
      final AtomicReference m = new AtomicReference();

      b(io.reactivex.t var1, Callable var2, long var3, TimeUnit var5, io.reactivex.u var6) {
         super(var1, new io.reactivex.d.f.a());
         this.g = var2;
         this.h = var3;
         this.i = var5;
         this.j = var6;
      }

      public void a(io.reactivex.t var1, Collection var2) {
         this.a.onNext(var2);
      }

      public void dispose() {
         io.reactivex.d.a.d.a(this.m);
         this.k.dispose();
      }

      public boolean isDisposed() {
         boolean var1;
         if(this.m.get() == io.reactivex.d.a.d.a) {
            var1 = true;
         } else {
            var1 = false;
         }

         return var1;
      }

      public void onComplete() {
         // $FF: Couldn't be decompiled
      }

      public void onError(Throwable param1) {
         // $FF: Couldn't be decompiled
      }

      public void onNext(Object param1) {
         // $FF: Couldn't be decompiled
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.k, var1)) {
            this.k = var1;

            Collection var3;
            try {
               var3 = (Collection)io.reactivex.d.b.b.a(this.g.call(), "The buffer supplied is null");
            } catch (Throwable var2) {
               io.reactivex.exceptions.a.b(var2);
               this.dispose();
               io.reactivex.d.a.e.a(var2, this.a);
               return;
            }

            this.l = var3;
            this.a.onSubscribe(this);
            if(!this.c) {
               var1 = this.j.a(this, this.h, this.h, this.i);
               if(!this.m.compareAndSet((Object)null, var1)) {
                  var1.dispose();
               }
            }
         }

      }

      public void run() {
         // $FF: Couldn't be decompiled
      }
   }

   static final class c extends io.reactivex.d.d.t implements io.reactivex.b.b, Runnable {
      final Callable g;
      final long h;
      final long i;
      final TimeUnit j;
      final io.reactivex.u.c k;
      final List l;
      io.reactivex.b.b m;

      c(io.reactivex.t var1, Callable var2, long var3, long var5, TimeUnit var7, io.reactivex.u.c var8) {
         super(var1, new io.reactivex.d.f.a());
         this.g = var2;
         this.h = var3;
         this.i = var5;
         this.j = var7;
         this.k = var8;
         this.l = new LinkedList();
      }

      // $FF: synthetic method
      static void a(q.c var0, Object var1, boolean var2, io.reactivex.b.b var3) {
         var0.b(var1, var2, var3);
      }

      // $FF: synthetic method
      static void b(q.c var0, Object var1, boolean var2, io.reactivex.b.b var3) {
         var0.b(var1, var2, var3);
      }

      public void a(io.reactivex.t var1, Collection var2) {
         var1.onNext(var2);
      }

      public void dispose() {
         if(!this.c) {
            this.c = true;
            this.f();
            this.m.dispose();
            this.k.dispose();
         }

      }

      void f() {
         // $FF: Couldn't be decompiled
      }

      public boolean isDisposed() {
         return this.c;
      }

      public void onComplete() {
         // $FF: Couldn't be decompiled
      }

      public void onError(Throwable var1) {
         this.d = true;
         this.f();
         this.a.onError(var1);
         this.k.dispose();
      }

      public void onNext(Object param1) {
         // $FF: Couldn't be decompiled
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.m, var1)) {
            this.m = var1;

            Collection var2;
            try {
               var2 = (Collection)io.reactivex.d.b.b.a(this.g.call(), "The buffer supplied is null");
            } catch (Throwable var3) {
               io.reactivex.exceptions.a.b(var3);
               var1.dispose();
               io.reactivex.d.a.e.a(var3, this.a);
               this.k.dispose();
               return;
            }

            this.l.add(var2);
            this.a.onSubscribe(this);
            this.k.a(this, this.i, this.i, this.j);
            this.k.a(new q.b(var2), this.h, this.j);
         }

      }

      public void run() {
         // $FF: Couldn't be decompiled
      }
   }

   final class a implements Runnable {
      private final Collection b;

      a(Collection var2) {
         this.b = var2;
      }

      public void run() {
         // $FF: Couldn't be decompiled
      }
   }

   final class b implements Runnable {
      private final Collection b;

      b(Collection var2) {
         this.b = var2;
      }

      public void run() {
         // $FF: Couldn't be decompiled
      }
   }
}
