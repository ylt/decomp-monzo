package io.reactivex.d.e.d;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public final class b implements Iterable {
   final io.reactivex.r a;
   final int b;

   public b(io.reactivex.r var1, int var2) {
      this.a = var1;
      this.b = var2;
   }

   public Iterator iterator() {
      b.a var1 = new b.a(this.b);
      this.a.subscribe(var1);
      return var1;
   }

   static final class a extends AtomicReference implements io.reactivex.b.b, io.reactivex.t, Iterator {
      final io.reactivex.d.f.c a;
      final Lock b;
      final Condition c;
      volatile boolean d;
      Throwable e;

      a(int var1) {
         this.a = new io.reactivex.d.f.c(var1);
         this.b = new ReentrantLock();
         this.c = this.b.newCondition();
      }

      void a() {
         this.b.lock();

         try {
            this.c.signalAll();
         } finally {
            this.b.unlock();
         }

      }

      public void dispose() {
         io.reactivex.d.a.d.a((AtomicReference)this);
      }

      public boolean hasNext() {
         // $FF: Couldn't be decompiled
      }

      public boolean isDisposed() {
         return io.reactivex.d.a.d.a((io.reactivex.b.b)this.get());
      }

      public Object next() {
         if(this.hasNext()) {
            return this.a.n_();
         } else {
            throw new NoSuchElementException();
         }
      }

      public void onComplete() {
         this.d = true;
         this.a();
      }

      public void onError(Throwable var1) {
         this.e = var1;
         this.d = true;
         this.a();
      }

      public void onNext(Object var1) {
         this.a.a(var1);
         this.a();
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         io.reactivex.d.a.d.b(this, var1);
      }

      public void remove() {
         throw new UnsupportedOperationException("remove");
      }
   }
}
