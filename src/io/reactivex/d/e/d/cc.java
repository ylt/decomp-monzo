package io.reactivex.d.e.d;

public final class cc extends io.reactivex.n {
   private final int a;
   private final long b;

   public cc(int var1, int var2) {
      this.a = var1;
      this.b = (long)var1 + (long)var2;
   }

   protected void subscribeActual(io.reactivex.t var1) {
      cc.a var2 = new cc.a(var1, (long)this.a, this.b);
      var1.onSubscribe(var2);
      var2.d();
   }

   static final class a extends io.reactivex.d.d.b {
      final io.reactivex.t a;
      final long b;
      long c;
      boolean d;

      a(io.reactivex.t var1, long var2, long var4) {
         this.a = var1;
         this.c = var2;
         this.b = var4;
      }

      public int a(int var1) {
         byte var2 = 1;
         byte var3;
         if((var1 & 1) != 0) {
            this.d = true;
            var3 = var2;
         } else {
            var3 = 0;
         }

         return var3;
      }

      public boolean b() {
         boolean var1;
         if(this.c == this.b) {
            var1 = true;
         } else {
            var1 = false;
         }

         return var1;
      }

      public void c() {
         this.c = this.b;
         this.lazySet(1);
      }

      void d() {
         if(!this.d) {
            io.reactivex.t var5 = this.a;
            long var3 = this.b;

            for(long var1 = this.c; var1 != var3 && this.get() == 0; ++var1) {
               var5.onNext(Integer.valueOf((int)var1));
            }

            if(this.get() == 0) {
               this.lazySet(1);
               var5.onComplete();
            }
         }

      }

      public void dispose() {
         this.set(1);
      }

      public Integer e() throws Exception {
         long var1 = this.c;
         Integer var3;
         if(var1 != this.b) {
            this.c = 1L + var1;
            var3 = Integer.valueOf((int)var1);
         } else {
            this.lazySet(1);
            var3 = null;
         }

         return var3;
      }

      public boolean isDisposed() {
         boolean var1;
         if(this.get() != 0) {
            var1 = true;
         } else {
            var1 = false;
         }

         return var1;
      }

      // $FF: synthetic method
      public Object n_() throws Exception {
         return this.e();
      }
   }
}
