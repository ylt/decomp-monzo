package io.reactivex.d.e.d;

public final class bv extends a {
   public bv(io.reactivex.r var1) {
      super(var1);
   }

   public void subscribeActual(io.reactivex.t var1) {
      this.a.subscribe(new bv.a(var1));
   }

   static final class a implements io.reactivex.b.b, io.reactivex.t {
      final io.reactivex.t a;
      io.reactivex.b.b b;

      a(io.reactivex.t var1) {
         this.a = var1;
      }

      public void dispose() {
         this.b.dispose();
      }

      public boolean isDisposed() {
         return this.b.isDisposed();
      }

      public void onComplete() {
         io.reactivex.m var1 = io.reactivex.m.f();
         this.a.onNext(var1);
         this.a.onComplete();
      }

      public void onError(Throwable var1) {
         io.reactivex.m var2 = io.reactivex.m.a(var1);
         this.a.onNext(var2);
         this.a.onComplete();
      }

      public void onNext(Object var1) {
         this.a.onNext(io.reactivex.m.a(var1));
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.b, var1)) {
            this.b = var1;
            this.a.onSubscribe(this);
         }

      }
   }
}
