package io.reactivex.d.e.d;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public final class h extends io.reactivex.n {
   final io.reactivex.r[] a;
   final Iterable b;

   public h(io.reactivex.r[] var1, Iterable var2) {
      this.a = var1;
      this.b = var2;
   }

   public void subscribeActual(io.reactivex.t param1) {
      // $FF: Couldn't be decompiled
   }

   static final class a implements io.reactivex.b.b {
      final io.reactivex.t a;
      final h.b[] b;
      final AtomicInteger c = new AtomicInteger();

      a(io.reactivex.t var1, int var2) {
         this.a = var1;
         this.b = new h.b[var2];
      }

      public void a(io.reactivex.r[] var1) {
         byte var3 = 0;
         h.b[] var5 = this.b;
         int var4 = var5.length;

         int var2;
         for(var2 = 0; var2 < var4; ++var2) {
            var5[var2] = new h.b(this, var2 + 1, this.a);
         }

         this.c.lazySet(0);
         this.a.onSubscribe(this);

         for(var2 = var3; var2 < var4 && this.c.get() == 0; ++var2) {
            var1[var2].subscribe(var5[var2]);
         }

      }

      public boolean a(int var1) {
         boolean var5 = true;
         int var2 = 0;
         int var3 = this.c.get();
         boolean var4;
         if(var3 == 0) {
            if(this.c.compareAndSet(0, var1)) {
               h.b[] var6 = this.b;
               var3 = var6.length;

               while(true) {
                  var4 = var5;
                  if(var2 >= var3) {
                     break;
                  }

                  if(var2 + 1 != var1) {
                     var6[var2].a();
                  }

                  ++var2;
               }
            } else {
               var4 = false;
            }
         } else {
            var4 = var5;
            if(var3 != var1) {
               var4 = false;
            }
         }

         return var4;
      }

      public void dispose() {
         if(this.c.get() != -1) {
            this.c.lazySet(-1);
            h.b[] var3 = this.b;
            int var2 = var3.length;

            for(int var1 = 0; var1 < var2; ++var1) {
               var3[var1].a();
            }
         }

      }

      public boolean isDisposed() {
         boolean var1;
         if(this.c.get() == -1) {
            var1 = true;
         } else {
            var1 = false;
         }

         return var1;
      }
   }

   static final class b extends AtomicReference implements io.reactivex.t {
      final h.a a;
      final int b;
      final io.reactivex.t c;
      boolean d;

      b(h.a var1, int var2, io.reactivex.t var3) {
         this.a = var1;
         this.b = var2;
         this.c = var3;
      }

      public void a() {
         io.reactivex.d.a.d.a((AtomicReference)this);
      }

      public void onComplete() {
         if(this.d) {
            this.c.onComplete();
         } else if(this.a.a(this.b)) {
            this.d = true;
            this.c.onComplete();
         }

      }

      public void onError(Throwable var1) {
         if(this.d) {
            this.c.onError(var1);
         } else if(this.a.a(this.b)) {
            this.d = true;
            this.c.onError(var1);
         } else {
            io.reactivex.g.a.a(var1);
         }

      }

      public void onNext(Object var1) {
         if(this.d) {
            this.c.onNext(var1);
         } else if(this.a.a(this.b)) {
            this.d = true;
            this.c.onNext(var1);
         } else {
            ((io.reactivex.b.b)this.get()).dispose();
         }

      }

      public void onSubscribe(io.reactivex.b.b var1) {
         io.reactivex.d.a.d.b(this, var1);
      }
   }
}
