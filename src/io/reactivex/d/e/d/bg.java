package io.reactivex.d.e.d;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public final class bg extends a {
   final io.reactivex.c.h b;
   final io.reactivex.c.h c;
   final int d;
   final boolean e;

   public bg(io.reactivex.r var1, io.reactivex.c.h var2, io.reactivex.c.h var3, int var4, boolean var5) {
      super(var1);
      this.b = var2;
      this.c = var3;
      this.d = var4;
      this.e = var5;
   }

   public void subscribeActual(io.reactivex.t var1) {
      this.a.subscribe(new bg.a(var1, this.b, this.c, this.d, this.e));
   }

   public static final class a extends AtomicInteger implements io.reactivex.b.b, io.reactivex.t {
      static final Object g = new Object();
      final io.reactivex.t a;
      final io.reactivex.c.h b;
      final io.reactivex.c.h c;
      final int d;
      final boolean e;
      final Map f;
      io.reactivex.b.b h;
      final AtomicBoolean i = new AtomicBoolean();

      public a(io.reactivex.t var1, io.reactivex.c.h var2, io.reactivex.c.h var3, int var4, boolean var5) {
         this.a = var1;
         this.b = var2;
         this.c = var3;
         this.d = var4;
         this.e = var5;
         this.f = new ConcurrentHashMap();
         this.lazySet(1);
      }

      public void a(Object var1) {
         if(var1 == null) {
            var1 = g;
         }

         this.f.remove(var1);
         if(this.decrementAndGet() == 0) {
            this.h.dispose();
         }

      }

      public void dispose() {
         if(this.i.compareAndSet(false, true) && this.decrementAndGet() == 0) {
            this.h.dispose();
         }

      }

      public boolean isDisposed() {
         return this.i.get();
      }

      public void onComplete() {
         ArrayList var1 = new ArrayList(this.f.values());
         this.f.clear();
         Iterator var2 = var1.iterator();

         while(var2.hasNext()) {
            ((bg.b)var2.next()).a();
         }

         this.a.onComplete();
      }

      public void onError(Throwable var1) {
         ArrayList var2 = new ArrayList(this.f.values());
         this.f.clear();
         Iterator var3 = var2.iterator();

         while(var3.hasNext()) {
            ((bg.b)var3.next()).a(var1);
         }

         this.a.onError(var1);
      }

      public void onNext(Object var1) {
         Object var4;
         try {
            var4 = this.b.a(var1);
         } catch (Throwable var7) {
            io.reactivex.exceptions.a.b(var7);
            this.h.dispose();
            this.onError(var7);
            return;
         }

         Object var2;
         if(var4 != null) {
            var2 = var4;
         } else {
            var2 = g;
         }

         bg.b var5 = (bg.b)this.f.get(var2);
         bg.b var3 = var5;
         if(var5 == null) {
            if(this.i.get()) {
               return;
            }

            var3 = bg.b.a(var4, this.d, this, this.e);
            this.f.put(var2, var3);
            this.getAndIncrement();
            this.a.onNext(var3);
         }

         try {
            var1 = io.reactivex.d.b.b.a(this.c.a(var1), "The value supplied is null");
         } catch (Throwable var6) {
            io.reactivex.exceptions.a.b(var6);
            this.h.dispose();
            this.onError(var6);
            return;
         }

         var3.a(var1);
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.h, var1)) {
            this.h = var1;
            this.a.onSubscribe(this);
         }

      }
   }

   static final class b extends io.reactivex.e.b {
      final bg.c a;

      protected b(Object var1, bg.c var2) {
         super(var1);
         this.a = var2;
      }

      public static bg.b a(Object var0, int var1, bg.a var2, boolean var3) {
         return new bg.b(var0, new bg.c(var1, var2, var0, var3));
      }

      public void a() {
         this.a.a();
      }

      public void a(Object var1) {
         this.a.a(var1);
      }

      public void a(Throwable var1) {
         this.a.a(var1);
      }

      protected void subscribeActual(io.reactivex.t var1) {
         this.a.subscribe(var1);
      }
   }

   static final class c extends AtomicInteger implements io.reactivex.b.b, io.reactivex.r {
      final Object a;
      final io.reactivex.d.f.c b;
      final bg.a c;
      final boolean d;
      volatile boolean e;
      Throwable f;
      final AtomicBoolean g = new AtomicBoolean();
      final AtomicBoolean h = new AtomicBoolean();
      final AtomicReference i = new AtomicReference();

      c(int var1, bg.a var2, Object var3, boolean var4) {
         this.b = new io.reactivex.d.f.c(var1);
         this.c = var2;
         this.a = var3;
         this.d = var4;
      }

      public void a() {
         this.e = true;
         this.b();
      }

      public void a(Object var1) {
         this.b.a(var1);
         this.b();
      }

      public void a(Throwable var1) {
         this.f = var1;
         this.e = true;
         this.b();
      }

      boolean a(boolean var1, boolean var2, io.reactivex.t var3, boolean var4) {
         boolean var5 = true;
         if(this.g.get()) {
            this.b.c();
            this.c.a(this.a);
            this.i.lazySet((Object)null);
            var1 = var5;
         } else {
            if(var1) {
               Throwable var6;
               if(var4) {
                  if(var2) {
                     var6 = this.f;
                     this.i.lazySet((Object)null);
                     if(var6 != null) {
                        var3.onError(var6);
                        var1 = var5;
                     } else {
                        var3.onComplete();
                        var1 = var5;
                     }

                     return var1;
                  }
               } else {
                  var6 = this.f;
                  if(var6 != null) {
                     this.b.c();
                     this.i.lazySet((Object)null);
                     var3.onError(var6);
                     var1 = var5;
                     return var1;
                  }

                  if(var2) {
                     this.i.lazySet((Object)null);
                     var3.onComplete();
                     var1 = var5;
                     return var1;
                  }
               }
            }

            var1 = false;
         }

         return var1;
      }

      void b() {
         if(this.getAndIncrement() == 0) {
            io.reactivex.d.f.c var7 = this.b;
            boolean var5 = this.d;
            io.reactivex.t var6 = (io.reactivex.t)this.i.get();
            int var1 = 1;

            while(true) {
               if(var6 != null) {
                  while(true) {
                     boolean var4 = this.e;
                     Object var8 = var7.n_();
                     boolean var3;
                     if(var8 == null) {
                        var3 = true;
                     } else {
                        var3 = false;
                     }

                     if(this.a(var4, var3, var6, var5)) {
                        return;
                     }

                     if(var3) {
                        break;
                     }

                     var6.onNext(var8);
                  }
               }

               int var2 = this.addAndGet(-var1);
               if(var2 == 0) {
                  break;
               }

               var1 = var2;
               if(var6 == null) {
                  var6 = (io.reactivex.t)this.i.get();
                  var1 = var2;
               }
            }
         }

      }

      public void dispose() {
         if(this.g.compareAndSet(false, true) && this.getAndIncrement() == 0) {
            this.i.lazySet((Object)null);
            this.c.a(this.a);
         }

      }

      public boolean isDisposed() {
         return this.g.get();
      }

      public void subscribe(io.reactivex.t var1) {
         if(this.h.compareAndSet(false, true)) {
            var1.onSubscribe(this);
            this.i.lazySet(var1);
            if(this.g.get()) {
               this.i.lazySet((Object)null);
            } else {
               this.b();
            }
         } else {
            io.reactivex.d.a.e.a(new IllegalStateException("Only one Observer allowed!"), (io.reactivex.t)var1);
         }

      }
   }
}
