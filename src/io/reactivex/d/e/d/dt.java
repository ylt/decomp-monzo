package io.reactivex.d.e.d;

import java.util.Collection;
import java.util.concurrent.Callable;

public final class dt extends a {
   final Callable b;

   public dt(io.reactivex.r var1, int var2) {
      super(var1);
      this.b = io.reactivex.d.b.a.a(var2);
   }

   public dt(io.reactivex.r var1, Callable var2) {
      super(var1);
      this.b = var2;
   }

   public void subscribeActual(io.reactivex.t var1) {
      Collection var2;
      try {
         var2 = (Collection)io.reactivex.d.b.b.a(this.b.call(), "The collectionSupplier returned a null collection. Null values are generally not allowed in 2.x operators and sources.");
      } catch (Throwable var3) {
         io.reactivex.exceptions.a.b(var3);
         io.reactivex.d.a.e.a(var3, var1);
         return;
      }

      this.a.subscribe(new dt.a(var1, var2));
   }

   static final class a implements io.reactivex.b.b, io.reactivex.t {
      Collection a;
      final io.reactivex.t b;
      io.reactivex.b.b c;

      a(io.reactivex.t var1, Collection var2) {
         this.b = var1;
         this.a = var2;
      }

      public void dispose() {
         this.c.dispose();
      }

      public boolean isDisposed() {
         return this.c.isDisposed();
      }

      public void onComplete() {
         Collection var1 = this.a;
         this.a = null;
         this.b.onNext(var1);
         this.b.onComplete();
      }

      public void onError(Throwable var1) {
         this.a = null;
         this.b.onError(var1);
      }

      public void onNext(Object var1) {
         this.a.add(var1);
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.c, var1)) {
            this.c = var1;
            this.b.onSubscribe(this);
         }

      }
   }
}
