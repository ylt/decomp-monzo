package io.reactivex.d.e.d;

import java.util.Collection;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicReference;

public final class o extends a {
   final Callable b;
   final Callable c;

   public o(io.reactivex.r var1, Callable var2, Callable var3) {
      super(var1);
      this.b = var2;
      this.c = var3;
   }

   protected void subscribeActual(io.reactivex.t var1) {
      this.a.subscribe(new o.b(new io.reactivex.f.e(var1), this.c, this.b));
   }

   static final class a extends io.reactivex.f.c {
      final o.b a;
      boolean b;

      a(o.b var1) {
         this.a = var1;
      }

      public void onComplete() {
         if(!this.b) {
            this.b = true;
            this.a.g();
         }

      }

      public void onError(Throwable var1) {
         if(this.b) {
            io.reactivex.g.a.a(var1);
         } else {
            this.b = true;
            this.a.onError(var1);
         }

      }

      public void onNext(Object var1) {
         if(!this.b) {
            this.b = true;
            this.dispose();
            this.a.g();
         }

      }
   }

   static final class b extends io.reactivex.d.d.t implements io.reactivex.b.b, io.reactivex.t {
      final Callable g;
      final Callable h;
      io.reactivex.b.b i;
      final AtomicReference j = new AtomicReference();
      Collection k;

      b(io.reactivex.t var1, Callable var2, Callable var3) {
         super(var1, new io.reactivex.d.f.a());
         this.g = var2;
         this.h = var3;
      }

      public void a(io.reactivex.t var1, Collection var2) {
         this.a.onNext(var2);
      }

      public void dispose() {
         if(!this.c) {
            this.c = true;
            this.i.dispose();
            this.f();
            if(this.c()) {
               this.b.c();
            }
         }

      }

      void f() {
         io.reactivex.d.a.d.a(this.j);
      }

      void g() {
         // $FF: Couldn't be decompiled
      }

      public boolean isDisposed() {
         return this.c;
      }

      public void onComplete() {
         // $FF: Couldn't be decompiled
      }

      public void onError(Throwable var1) {
         this.dispose();
         this.a.onError(var1);
      }

      public void onNext(Object param1) {
         // $FF: Couldn't be decompiled
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.i, var1)) {
            this.i = var1;
            io.reactivex.t var2 = this.a;

            Collection var3;
            try {
               var3 = (Collection)io.reactivex.d.b.b.a(this.g.call(), "The buffer supplied is null");
            } catch (Throwable var5) {
               io.reactivex.exceptions.a.b(var5);
               this.c = true;
               var1.dispose();
               io.reactivex.d.a.e.a(var5, var2);
               return;
            }

            this.k = var3;

            io.reactivex.r var7;
            try {
               var7 = (io.reactivex.r)io.reactivex.d.b.b.a(this.h.call(), "The boundary ObservableSource supplied is null");
            } catch (Throwable var4) {
               io.reactivex.exceptions.a.b(var4);
               this.c = true;
               var1.dispose();
               io.reactivex.d.a.e.a(var4, var2);
               return;
            }

            o.a var6 = new o.a(this);
            this.j.set(var6);
            var2.onSubscribe(this);
            if(!this.c) {
               var7.subscribe(var6);
            }
         }

      }
   }
}
