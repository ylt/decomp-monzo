package io.reactivex.d.e.d;

import java.util.concurrent.atomic.AtomicInteger;

public final class cu extends io.reactivex.n {
   final io.reactivex.r a;
   final io.reactivex.r b;
   final io.reactivex.c.d c;
   final int d;

   public cu(io.reactivex.r var1, io.reactivex.r var2, io.reactivex.c.d var3, int var4) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
      this.d = var4;
   }

   public void subscribeActual(io.reactivex.t var1) {
      cu.a var2 = new cu.a(var1, this.d, this.a, this.b, this.c);
      var1.onSubscribe(var2);
      var2.a();
   }

   static final class a extends AtomicInteger implements io.reactivex.b.b {
      final io.reactivex.t a;
      final io.reactivex.c.d b;
      final io.reactivex.d.a.a c;
      final io.reactivex.r d;
      final io.reactivex.r e;
      final cu.b[] f;
      volatile boolean g;
      Object h;
      Object i;

      a(io.reactivex.t var1, int var2, io.reactivex.r var3, io.reactivex.r var4, io.reactivex.c.d var5) {
         this.a = var1;
         this.d = var3;
         this.e = var4;
         this.b = var5;
         cu.b[] var6 = new cu.b[2];
         this.f = var6;
         var6[0] = new cu.b(this, 0, var2);
         var6[1] = new cu.b(this, 1, var2);
         this.c = new io.reactivex.d.a.a(2);
      }

      void a() {
         cu.b[] var1 = this.f;
         this.d.subscribe(var1[0]);
         this.e.subscribe(var1[1]);
      }

      void a(io.reactivex.d.f.c var1, io.reactivex.d.f.c var2) {
         this.g = true;
         var1.c();
         var2.c();
      }

      boolean a(io.reactivex.b.b var1, int var2) {
         return this.c.a(var2, var1);
      }

      void b() {
         if(this.getAndIncrement() == 0) {
            cu.b[] var7 = this.f;
            cu.b var8 = var7[0];
            io.reactivex.d.f.c var6 = var8.b;
            cu.b var9 = var7[1];
            io.reactivex.d.f.c var13 = var9.b;
            int var1 = 1;

            int var12;
            do {
               boolean var2;
               boolean var3;
               do {
                  if(this.g) {
                     var6.c();
                     var13.c();
                     return;
                  }

                  boolean var4 = var8.d;
                  Throwable var10;
                  if(var4) {
                     var10 = var8.e;
                     if(var10 != null) {
                        this.a(var6, var13);
                        this.a.onError(var10);
                        return;
                     }
                  }

                  boolean var5 = var9.d;
                  if(var5) {
                     var10 = var9.e;
                     if(var10 != null) {
                        this.a(var6, var13);
                        this.a.onError(var10);
                        return;
                     }
                  }

                  if(this.h == null) {
                     this.h = var6.n_();
                  }

                  if(this.h == null) {
                     var2 = true;
                  } else {
                     var2 = false;
                  }

                  if(this.i == null) {
                     this.i = var13.n_();
                  }

                  if(this.i == null) {
                     var3 = true;
                  } else {
                     var3 = false;
                  }

                  if(var4 && var5 && var2 && var3) {
                     this.a.onNext(Boolean.valueOf(true));
                     this.a.onComplete();
                     return;
                  }

                  if(var4 && var5 && var2 != var3) {
                     this.a(var6, var13);
                     this.a.onNext(Boolean.valueOf(false));
                     this.a.onComplete();
                     return;
                  }

                  if(!var2 && !var3) {
                     try {
                        var4 = this.b.a(this.h, this.i);
                     } catch (Throwable var11) {
                        io.reactivex.exceptions.a.b(var11);
                        this.a(var6, var13);
                        this.a.onError(var11);
                        return;
                     }

                     if(!var4) {
                        this.a(var6, var13);
                        this.a.onNext(Boolean.valueOf(false));
                        this.a.onComplete();
                        return;
                     }

                     this.h = null;
                     this.i = null;
                  }
               } while(!var2 && !var3);

               var12 = this.addAndGet(-var1);
               var1 = var12;
            } while(var12 != 0);
         }

      }

      public void dispose() {
         if(!this.g) {
            this.g = true;
            this.c.dispose();
            if(this.getAndIncrement() == 0) {
               cu.b[] var1 = this.f;
               var1[0].b.c();
               var1[1].b.c();
            }
         }

      }

      public boolean isDisposed() {
         return this.g;
      }
   }

   static final class b implements io.reactivex.t {
      final cu.a a;
      final io.reactivex.d.f.c b;
      final int c;
      volatile boolean d;
      Throwable e;

      b(cu.a var1, int var2, int var3) {
         this.a = var1;
         this.c = var2;
         this.b = new io.reactivex.d.f.c(var3);
      }

      public void onComplete() {
         this.d = true;
         this.a.b();
      }

      public void onError(Throwable var1) {
         this.e = var1;
         this.d = true;
         this.a.b();
      }

      public void onNext(Object var1) {
         this.b.a(var1);
         this.a.b();
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         this.a.a(var1, this.c);
      }
   }
}
