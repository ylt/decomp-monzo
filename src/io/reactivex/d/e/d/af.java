package io.reactivex.d.e.d;

public final class af extends a {
   public af(io.reactivex.r var1) {
      super(var1);
   }

   public void subscribeActual(io.reactivex.t var1) {
      this.a.subscribe(new af.a(var1));
   }

   static final class a implements io.reactivex.b.b, io.reactivex.t {
      final io.reactivex.t a;
      boolean b;
      io.reactivex.b.b c;

      a(io.reactivex.t var1) {
         this.a = var1;
      }

      public void a(io.reactivex.m var1) {
         if(this.b) {
            if(var1.b()) {
               io.reactivex.g.a.a(var1.e());
            }
         } else if(var1.b()) {
            this.c.dispose();
            this.onError(var1.e());
         } else if(var1.a()) {
            this.c.dispose();
            this.onComplete();
         } else {
            this.a.onNext(var1.d());
         }

      }

      public void dispose() {
         this.c.dispose();
      }

      public boolean isDisposed() {
         return this.c.isDisposed();
      }

      public void onComplete() {
         if(!this.b) {
            this.b = true;
            this.a.onComplete();
         }

      }

      public void onError(Throwable var1) {
         if(this.b) {
            io.reactivex.g.a.a(var1);
         } else {
            this.b = true;
            this.a.onError(var1);
         }

      }

      // $FF: synthetic method
      public void onNext(Object var1) {
         this.a((io.reactivex.m)var1);
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.c, var1)) {
            this.c = var1;
            this.a.onSubscribe(this);
         }

      }
   }
}
