package io.reactivex.d.e.d;

public final class ag extends a {
   public ag(io.reactivex.r var1) {
      super(var1);
   }

   protected void subscribeActual(io.reactivex.t var1) {
      this.a.subscribe(new ag.a(var1));
   }

   static final class a implements io.reactivex.b.b, io.reactivex.t {
      io.reactivex.t a;
      io.reactivex.b.b b;

      a(io.reactivex.t var1) {
         this.a = var1;
      }

      public void dispose() {
         io.reactivex.b.b var1 = this.b;
         this.b = io.reactivex.d.j.g.a;
         this.a = io.reactivex.d.j.g.b();
         var1.dispose();
      }

      public boolean isDisposed() {
         return this.b.isDisposed();
      }

      public void onComplete() {
         io.reactivex.t var1 = this.a;
         this.b = io.reactivex.d.j.g.a;
         this.a = io.reactivex.d.j.g.b();
         var1.onComplete();
      }

      public void onError(Throwable var1) {
         io.reactivex.t var2 = this.a;
         this.b = io.reactivex.d.j.g.a;
         this.a = io.reactivex.d.j.g.b();
         var2.onError(var1);
      }

      public void onNext(Object var1) {
         this.a.onNext(var1);
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.b, var1)) {
            this.b = var1;
            this.a.onSubscribe(this);
         }

      }
   }
}
