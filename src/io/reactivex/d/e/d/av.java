package io.reactivex.d.e.d;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public final class av extends io.reactivex.b implements io.reactivex.d.c.c {
   final io.reactivex.r a;
   final io.reactivex.c.h b;
   final boolean c;

   public av(io.reactivex.r var1, io.reactivex.c.h var2, boolean var3) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
   }

   protected void b(io.reactivex.c var1) {
      this.a.subscribe(new av.a(var1, this.b, this.c));
   }

   public io.reactivex.n q_() {
      return io.reactivex.g.a.a((io.reactivex.n)(new au(this.a, this.b, this.c)));
   }

   static final class a extends AtomicInteger implements io.reactivex.b.b, io.reactivex.t {
      final io.reactivex.c a;
      final io.reactivex.d.j.c b;
      final io.reactivex.c.h c;
      final boolean d;
      final io.reactivex.b.a e;
      io.reactivex.b.b f;
      volatile boolean g;

      a(io.reactivex.c var1, io.reactivex.c.h var2, boolean var3) {
         this.a = var1;
         this.c = var2;
         this.d = var3;
         this.b = new io.reactivex.d.j.c();
         this.e = new io.reactivex.b.a();
         this.lazySet(1);
      }

      void a(av.a var1) {
         this.e.c(var1);
         this.onComplete();
      }

      void a(av.a var1, Throwable var2) {
         this.e.c(var1);
         this.onError(var2);
      }

      public void dispose() {
         this.g = true;
         this.f.dispose();
         this.e.dispose();
      }

      public boolean isDisposed() {
         return this.f.isDisposed();
      }

      public void onComplete() {
         if(this.decrementAndGet() == 0) {
            Throwable var1 = this.b.a();
            if(var1 != null) {
               this.a.onError(var1);
            } else {
               this.a.onComplete();
            }
         }

      }

      public void onError(Throwable var1) {
         if(this.b.a(var1)) {
            if(this.d) {
               if(this.decrementAndGet() == 0) {
                  var1 = this.b.a();
                  this.a.onError(var1);
               }
            } else {
               this.dispose();
               if(this.getAndSet(0) > 0) {
                  var1 = this.b.a();
                  this.a.onError(var1);
               }
            }
         } else {
            io.reactivex.g.a.a(var1);
         }

      }

      public void onNext(Object var1) {
         io.reactivex.d var4;
         try {
            var4 = (io.reactivex.d)io.reactivex.d.b.b.a(this.c.a(var1), "The mapper returned a null CompletableSource");
         } catch (Throwable var3) {
            io.reactivex.exceptions.a.b(var3);
            this.f.dispose();
            this.onError(var3);
            return;
         }

         this.getAndIncrement();
         av.a var2 = new av.a();
         if(!this.g && this.e.a((io.reactivex.b.b)var2)) {
            var4.a(var2);
         }

      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.f, var1)) {
            this.f = var1;
            this.a.onSubscribe(this);
         }

      }
   }

   final class a extends AtomicReference implements io.reactivex.b.b, io.reactivex.c {
      public void dispose() {
         io.reactivex.d.a.d.a((AtomicReference)this);
      }

      public boolean isDisposed() {
         return io.reactivex.d.a.d.a((io.reactivex.b.b)this.get());
      }

      public void onComplete() {
         av.this.a(this);
      }

      public void onError(Throwable var1) {
         av.this.a(this, var1);
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         io.reactivex.d.a.d.b(this, var1);
      }
   }
}
