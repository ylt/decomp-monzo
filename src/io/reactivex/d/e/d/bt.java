package io.reactivex.d.e.d;

public final class bt extends a {
   final io.reactivex.c.h b;

   public bt(io.reactivex.r var1, io.reactivex.c.h var2) {
      super(var1);
      this.b = var2;
   }

   public void subscribeActual(io.reactivex.t var1) {
      this.a.subscribe(new bt.a(var1, this.b));
   }

   static final class a extends io.reactivex.d.d.a {
      final io.reactivex.c.h f;

      a(io.reactivex.t var1, io.reactivex.c.h var2) {
         super(var1);
         this.f = var2;
      }

      public int a(int var1) {
         return this.b(var1);
      }

      public Object n_() throws Exception {
         Object var1 = this.c.n_();
         if(var1 != null) {
            var1 = io.reactivex.d.b.b.a(this.f.a(var1), "The mapper function returned a null value.");
         } else {
            var1 = null;
         }

         return var1;
      }

      public void onNext(Object var1) {
         if(!this.d) {
            if(this.e != 0) {
               this.a.onNext((Object)null);
            } else {
               try {
                  var1 = io.reactivex.d.b.b.a(this.f.a(var1), "The mapper function returned a null value.");
               } catch (Throwable var2) {
                  this.a(var2);
                  return;
               }

               this.a.onNext(var1);
            }
         }

      }
   }
}
