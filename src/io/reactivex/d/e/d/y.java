package io.reactivex.d.e.d;

public final class y extends io.reactivex.v implements io.reactivex.d.c.c {
   final io.reactivex.r a;

   public y(io.reactivex.r var1) {
      this.a = var1;
   }

   public void b(io.reactivex.x var1) {
      this.a.subscribe(new y.a(var1));
   }

   public io.reactivex.n q_() {
      return io.reactivex.g.a.a((io.reactivex.n)(new x(this.a)));
   }

   static final class a implements io.reactivex.b.b, io.reactivex.t {
      final io.reactivex.x a;
      io.reactivex.b.b b;
      long c;

      a(io.reactivex.x var1) {
         this.a = var1;
      }

      public void dispose() {
         this.b.dispose();
         this.b = io.reactivex.d.a.d.a;
      }

      public boolean isDisposed() {
         return this.b.isDisposed();
      }

      public void onComplete() {
         this.b = io.reactivex.d.a.d.a;
         this.a.a_(Long.valueOf(this.c));
      }

      public void onError(Throwable var1) {
         this.b = io.reactivex.d.a.d.a;
         this.a.onError(var1);
      }

      public void onNext(Object var1) {
         ++this.c;
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.b, var1)) {
            this.b = var1;
            this.a.onSubscribe(this);
         }

      }
   }
}
