package io.reactivex.d.e.d;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicReference;

public final class dr extends a {
   static final io.reactivex.b.b f = new dr.a();
   final long b;
   final TimeUnit c;
   final io.reactivex.u d;
   final io.reactivex.r e;

   public dr(io.reactivex.r var1, long var2, TimeUnit var4, io.reactivex.u var5, io.reactivex.r var6) {
      super(var1);
      this.b = var2;
      this.c = var4;
      this.d = var5;
      this.e = var6;
   }

   public void subscribeActual(io.reactivex.t var1) {
      if(this.e == null) {
         this.a.subscribe(new dr.b(new io.reactivex.f.e(var1), this.b, this.c, this.d.a()));
      } else {
         this.a.subscribe(new dr.c(var1, this.b, this.c, this.d.a(), this.e));
      }

   }

   static final class a implements io.reactivex.b.b {
      public void dispose() {
      }

      public boolean isDisposed() {
         return true;
      }
   }

   static final class b extends AtomicReference implements io.reactivex.b.b, io.reactivex.t {
      final io.reactivex.t a;
      final long b;
      final TimeUnit c;
      final io.reactivex.u.c d;
      io.reactivex.b.b e;
      volatile long f;
      volatile boolean g;

      b(io.reactivex.t var1, long var2, TimeUnit var4, io.reactivex.u.c var5) {
         this.a = var1;
         this.b = var2;
         this.c = var4;
         this.d = var5;
      }

      void a(long var1) {
         io.reactivex.b.b var3 = (io.reactivex.b.b)this.get();
         if(var3 != null) {
            var3.dispose();
         }

         if(this.compareAndSet(var3, dr.f)) {
            io.reactivex.d.a.d.c(this, this.d.a(new dr.a(var1), this.b, this.c));
         }

      }

      public void dispose() {
         this.e.dispose();
         this.d.dispose();
      }

      public boolean isDisposed() {
         return this.d.isDisposed();
      }

      public void onComplete() {
         if(!this.g) {
            this.g = true;
            this.a.onComplete();
            this.dispose();
         }

      }

      public void onError(Throwable var1) {
         if(this.g) {
            io.reactivex.g.a.a(var1);
         } else {
            this.g = true;
            this.a.onError(var1);
            this.dispose();
         }

      }

      public void onNext(Object var1) {
         if(!this.g) {
            long var2 = this.f + 1L;
            this.f = var2;
            this.a.onNext(var1);
            this.a(var2);
         }

      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.e, var1)) {
            this.e = var1;
            this.a.onSubscribe(this);
            this.a(0L);
         }

      }
   }

   final class a implements Runnable {
      private final long b;

      a(long var2) {
         this.b = var2;
      }

      public void run() {
         if(this.b == dr.super.f) {
            dr.super.g = true;
            dr.super.e.dispose();
            io.reactivex.d.a.d.a((AtomicReference)dr.this);
            dr.super.a.onError(new TimeoutException());
            dr.super.d.dispose();
         }

      }
   }

   static final class c extends AtomicReference implements io.reactivex.b.b, io.reactivex.t {
      final io.reactivex.t a;
      final long b;
      final TimeUnit c;
      final io.reactivex.u.c d;
      final io.reactivex.r e;
      io.reactivex.b.b f;
      final io.reactivex.d.a.j g;
      volatile long h;
      volatile boolean i;

      c(io.reactivex.t var1, long var2, TimeUnit var4, io.reactivex.u.c var5, io.reactivex.r var6) {
         this.a = var1;
         this.b = var2;
         this.c = var4;
         this.d = var5;
         this.e = var6;
         this.g = new io.reactivex.d.a.j(var1, this, 8);
      }

      void a() {
         this.e.subscribe(new io.reactivex.d.d.o(this.g));
      }

      void a(long var1) {
         io.reactivex.b.b var3 = (io.reactivex.b.b)this.get();
         if(var3 != null) {
            var3.dispose();
         }

         if(this.compareAndSet(var3, dr.f)) {
            io.reactivex.d.a.d.c(this, this.d.a(new dr.a(var1), this.b, this.c));
         }

      }

      public void dispose() {
         this.f.dispose();
         this.d.dispose();
      }

      public boolean isDisposed() {
         return this.d.isDisposed();
      }

      public void onComplete() {
         if(!this.i) {
            this.i = true;
            this.g.b(this.f);
            this.d.dispose();
         }

      }

      public void onError(Throwable var1) {
         if(this.i) {
            io.reactivex.g.a.a(var1);
         } else {
            this.i = true;
            this.g.a(var1, this.f);
            this.d.dispose();
         }

      }

      public void onNext(Object var1) {
         if(!this.i) {
            long var2 = this.h + 1L;
            this.h = var2;
            if(this.g.a(var1, this.f)) {
               this.a(var2);
            }
         }

      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.f, var1)) {
            this.f = var1;
            if(this.g.a(var1)) {
               this.a.onSubscribe(this.g);
               this.a(0L);
            }
         }

      }
   }

   final class a implements Runnable {
      private final long b;

      a(long var2) {
         this.b = var2;
      }

      public void run() {
         if(this.b == dr.super.h) {
            dr.super.i = true;
            dr.super.f.dispose();
            io.reactivex.d.a.d.a((AtomicReference)dr.this);
            dr.this.a();
            dr.super.d.dispose();
         }

      }
   }
}
