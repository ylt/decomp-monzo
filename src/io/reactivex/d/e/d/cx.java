package io.reactivex.d.e.d;

public final class cx extends io.reactivex.h {
   final io.reactivex.r a;

   public cx(io.reactivex.r var1) {
      this.a = var1;
   }

   public void b(io.reactivex.j var1) {
      this.a.subscribe(new cx.a(var1));
   }

   static final class a implements io.reactivex.b.b, io.reactivex.t {
      final io.reactivex.j a;
      io.reactivex.b.b b;
      Object c;
      boolean d;

      a(io.reactivex.j var1) {
         this.a = var1;
      }

      public void dispose() {
         this.b.dispose();
      }

      public boolean isDisposed() {
         return this.b.isDisposed();
      }

      public void onComplete() {
         if(!this.d) {
            this.d = true;
            Object var1 = this.c;
            this.c = null;
            if(var1 == null) {
               this.a.onComplete();
            } else {
               this.a.a_(var1);
            }
         }

      }

      public void onError(Throwable var1) {
         if(this.d) {
            io.reactivex.g.a.a(var1);
         } else {
            this.d = true;
            this.a.onError(var1);
         }

      }

      public void onNext(Object var1) {
         if(!this.d) {
            if(this.c != null) {
               this.d = true;
               this.b.dispose();
               this.a.onError(new IllegalArgumentException("Sequence contains more than one element!"));
            } else {
               this.c = var1;
            }
         }

      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.b, var1)) {
            this.b = var1;
            this.a.onSubscribe(this);
         }

      }
   }
}
