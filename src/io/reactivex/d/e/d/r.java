package io.reactivex.d.e.d;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public final class r extends a {
   final r.a b;
   final AtomicBoolean c;

   private r(io.reactivex.n var1, r.a var2) {
      super(var1);
      this.b = var2;
      this.c = new AtomicBoolean();
   }

   public static io.reactivex.n a(io.reactivex.n var0) {
      return a(var0, 16);
   }

   public static io.reactivex.n a(io.reactivex.n var0, int var1) {
      io.reactivex.d.b.b.a(var1, "capacityHint");
      return io.reactivex.g.a.a((io.reactivex.n)(new r(var0, new r.a(var0, var1))));
   }

   protected void subscribeActual(io.reactivex.t var1) {
      r.b var2 = new r.b(var1, this.b);
      var1.onSubscribe(var2);
      this.b.a(var2);
      if(!this.c.get() && this.c.compareAndSet(false, true)) {
         this.b.a();
      }

      var2.a();
   }

   static final class a extends io.reactivex.d.j.m implements io.reactivex.t {
      static final r.b[] d = new r.b[0];
      static final r.b[] e = new r.b[0];
      final io.reactivex.n a;
      final io.reactivex.d.a.k b;
      final AtomicReference c;
      volatile boolean f;
      boolean g;

      a(io.reactivex.n var1, int var2) {
         super(var2);
         this.a = var1;
         this.c = new AtomicReference(d);
         this.b = new io.reactivex.d.a.k();
      }

      public void a() {
         this.a.subscribe((io.reactivex.t)this);
         this.f = true;
      }

      public boolean a(r.b var1) {
         while(true) {
            r.b[] var4 = (r.b[])this.c.get();
            boolean var3;
            if(var4 == e) {
               var3 = false;
            } else {
               int var2 = var4.length;
               r.b[] var5 = new r.b[var2 + 1];
               System.arraycopy(var4, 0, var5, 0, var2);
               var5[var2] = var1;
               if(!this.c.compareAndSet(var4, var5)) {
                  continue;
               }

               var3 = true;
            }

            return var3;
         }
      }

      public void b(r.b var1) {
         while(true) {
            r.b[] var7 = (r.b[])this.c.get();
            int var5 = var7.length;
            if(var5 != 0) {
               byte var4 = -1;
               int var2 = 0;

               int var3;
               while(true) {
                  var3 = var4;
                  if(var2 >= var5) {
                     break;
                  }

                  if(var7[var2].equals(var1)) {
                     var3 = var2;
                     break;
                  }

                  ++var2;
               }

               if(var3 >= 0) {
                  r.b[] var6;
                  if(var5 == 1) {
                     var6 = d;
                  } else {
                     var6 = new r.b[var5 - 1];
                     System.arraycopy(var7, 0, var6, 0, var3);
                     System.arraycopy(var7, var3 + 1, var6, var3, var5 - var3 - 1);
                  }

                  if(!this.c.compareAndSet(var7, var6)) {
                     continue;
                  }
               }
            }

            return;
         }
      }

      public void onComplete() {
         if(!this.g) {
            this.g = true;
            this.a(io.reactivex.d.j.n.a());
            this.b.dispose();
            r.b[] var3 = (r.b[])this.c.getAndSet(e);
            int var2 = var3.length;

            for(int var1 = 0; var1 < var2; ++var1) {
               var3[var1].a();
            }
         }

      }

      public void onError(Throwable var1) {
         if(!this.g) {
            this.g = true;
            this.a(io.reactivex.d.j.n.a(var1));
            this.b.dispose();
            r.b[] var4 = (r.b[])this.c.getAndSet(e);
            int var3 = var4.length;

            for(int var2 = 0; var2 < var3; ++var2) {
               var4[var2].a();
            }
         }

      }

      public void onNext(Object var1) {
         if(!this.g) {
            this.a(io.reactivex.d.j.n.a(var1));
            r.b[] var4 = (r.b[])this.c.get();
            int var3 = var4.length;

            for(int var2 = 0; var2 < var3; ++var2) {
               var4[var2].a();
            }
         }

      }

      public void onSubscribe(io.reactivex.b.b var1) {
         this.b.a(var1);
      }
   }

   static final class b extends AtomicInteger implements io.reactivex.b.b {
      final io.reactivex.t a;
      final r.a b;
      Object[] c;
      int d;
      int e;
      volatile boolean f;

      b(io.reactivex.t var1, r.a var2) {
         this.a = var1;
         this.b = var2;
      }

      public void a() {
         if(this.getAndIncrement() == 0) {
            io.reactivex.t var9 = this.a;
            int var2 = 1;

            while(!this.f) {
               int var6 = this.b.c();
               if(var6 != 0) {
                  Object[] var8 = this.c;
                  Object[] var7 = var8;
                  if(var8 == null) {
                     var7 = this.b.b();
                     this.c = var7;
                  }

                  int var5 = var7.length - 1;
                  int var3 = this.e;
                  int var1 = this.d;

                  for(var8 = var7; var3 < var6; var8 = var7) {
                     if(this.f) {
                        return;
                     }

                     int var4 = var1;
                     var7 = var8;
                     if(var1 == var5) {
                        var7 = (Object[])((Object[])var8[var5]);
                        var4 = 0;
                     }

                     if(io.reactivex.d.j.n.a(var7[var4], var9)) {
                        return;
                     }

                     var1 = var4 + 1;
                     ++var3;
                  }

                  if(this.f) {
                     break;
                  }

                  this.e = var3;
                  this.d = var1;
                  this.c = var8;
               }

               var2 = this.addAndGet(-var2);
               if(var2 == 0) {
                  break;
               }
            }
         }

      }

      public void dispose() {
         if(!this.f) {
            this.f = true;
            this.b.b(this);
         }

      }

      public boolean isDisposed() {
         return this.f;
      }
   }
}
