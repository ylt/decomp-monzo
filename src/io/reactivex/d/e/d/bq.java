package io.reactivex.d.e.d;

public final class bq extends io.reactivex.h {
   final io.reactivex.r a;

   public bq(io.reactivex.r var1) {
      this.a = var1;
   }

   protected void b(io.reactivex.j var1) {
      this.a.subscribe(new bq.a(var1));
   }

   static final class a implements io.reactivex.b.b, io.reactivex.t {
      final io.reactivex.j a;
      io.reactivex.b.b b;
      Object c;

      a(io.reactivex.j var1) {
         this.a = var1;
      }

      public void dispose() {
         this.b.dispose();
         this.b = io.reactivex.d.a.d.a;
      }

      public boolean isDisposed() {
         boolean var1;
         if(this.b == io.reactivex.d.a.d.a) {
            var1 = true;
         } else {
            var1 = false;
         }

         return var1;
      }

      public void onComplete() {
         this.b = io.reactivex.d.a.d.a;
         Object var1 = this.c;
         if(var1 != null) {
            this.c = null;
            this.a.a_(var1);
         } else {
            this.a.onComplete();
         }

      }

      public void onError(Throwable var1) {
         this.b = io.reactivex.d.a.d.a;
         this.c = null;
         this.a.onError(var1);
      }

      public void onNext(Object var1) {
         this.c = var1;
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.b, var1)) {
            this.b = var1;
            this.a.onSubscribe(this);
         }

      }
   }
}
