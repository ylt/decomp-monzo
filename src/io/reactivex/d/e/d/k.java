package io.reactivex.d.e.d;

import java.util.concurrent.atomic.AtomicInteger;

public final class k extends io.reactivex.n {
   final io.reactivex.e.a a;
   final int b;
   final io.reactivex.c.g c;
   final AtomicInteger d;

   public k(io.reactivex.e.a var1, int var2, io.reactivex.c.g var3) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
      this.d = new AtomicInteger();
   }

   public void subscribeActual(io.reactivex.t var1) {
      this.a.subscribe(var1);
      if(this.d.incrementAndGet() == this.b) {
         this.a.a(this.c);
      }

   }
}
