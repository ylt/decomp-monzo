package io.reactivex.d.e.d;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

public final class ab extends a {
   final long b;
   final TimeUnit c;
   final io.reactivex.u d;

   public ab(io.reactivex.r var1, long var2, TimeUnit var4, io.reactivex.u var5) {
      super(var1);
      this.b = var2;
      this.c = var4;
      this.d = var5;
   }

   public void subscribeActual(io.reactivex.t var1) {
      this.a.subscribe(new ab.b(new io.reactivex.f.e(var1), this.b, this.c, this.d.a()));
   }

   static final class a extends AtomicReference implements io.reactivex.b.b, Runnable {
      final Object a;
      final long b;
      final ab.b c;
      final AtomicBoolean d = new AtomicBoolean();

      a(Object var1, long var2, ab.b var4) {
         this.a = var1;
         this.b = var2;
         this.c = var4;
      }

      public void a(io.reactivex.b.b var1) {
         io.reactivex.d.a.d.c(this, var1);
      }

      public void dispose() {
         io.reactivex.d.a.d.a((AtomicReference)this);
      }

      public boolean isDisposed() {
         boolean var1;
         if(this.get() == io.reactivex.d.a.d.a) {
            var1 = true;
         } else {
            var1 = false;
         }

         return var1;
      }

      public void run() {
         if(this.d.compareAndSet(false, true)) {
            this.c.a(this.b, this.a, this);
         }

      }
   }

   static final class b implements io.reactivex.b.b, io.reactivex.t {
      final io.reactivex.t a;
      final long b;
      final TimeUnit c;
      final io.reactivex.u.c d;
      io.reactivex.b.b e;
      final AtomicReference f = new AtomicReference();
      volatile long g;
      boolean h;

      b(io.reactivex.t var1, long var2, TimeUnit var4, io.reactivex.u.c var5) {
         this.a = var1;
         this.b = var2;
         this.c = var4;
         this.d = var5;
      }

      void a(long var1, Object var3, ab.a var4) {
         if(var1 == this.g) {
            this.a.onNext(var3);
            var4.dispose();
         }

      }

      public void dispose() {
         this.e.dispose();
         this.d.dispose();
      }

      public boolean isDisposed() {
         return this.d.isDisposed();
      }

      public void onComplete() {
         if(!this.h) {
            this.h = true;
            io.reactivex.b.b var1 = (io.reactivex.b.b)this.f.get();
            if(var1 != io.reactivex.d.a.d.a) {
               ab.a var2 = (ab.a)var1;
               if(var2 != null) {
                  var2.run();
               }

               this.a.onComplete();
               this.d.dispose();
            }
         }

      }

      public void onError(Throwable var1) {
         if(this.h) {
            io.reactivex.g.a.a(var1);
         } else {
            this.h = true;
            this.a.onError(var1);
            this.d.dispose();
         }

      }

      public void onNext(Object var1) {
         if(!this.h) {
            long var2 = 1L + this.g;
            this.g = var2;
            io.reactivex.b.b var4 = (io.reactivex.b.b)this.f.get();
            if(var4 != null) {
               var4.dispose();
            }

            ab.a var5 = new ab.a(var1, var2, this);
            if(this.f.compareAndSet(var4, var5)) {
               var5.a(this.d.a(var5, this.b, this.c));
            }
         }

      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.e, var1)) {
            this.e = var1;
            this.a.onSubscribe(this);
         }

      }
   }
}
