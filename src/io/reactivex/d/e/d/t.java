package io.reactivex.d.e.d;

import java.util.concurrent.Callable;

public final class t extends io.reactivex.v implements io.reactivex.d.c.c {
   final io.reactivex.r a;
   final Callable b;
   final io.reactivex.c.b c;

   public t(io.reactivex.r var1, Callable var2, io.reactivex.c.b var3) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
   }

   protected void b(io.reactivex.x var1) {
      Object var2;
      try {
         var2 = io.reactivex.d.b.b.a(this.b.call(), "The initialSupplier returned a null value");
      } catch (Throwable var3) {
         io.reactivex.d.a.e.a(var3, var1);
         return;
      }

      this.a.subscribe(new t.a(var1, var2, this.c));
   }

   public io.reactivex.n q_() {
      return io.reactivex.g.a.a((io.reactivex.n)(new s(this.a, this.b, this.c)));
   }

   static final class a implements io.reactivex.b.b, io.reactivex.t {
      final io.reactivex.x a;
      final io.reactivex.c.b b;
      final Object c;
      io.reactivex.b.b d;
      boolean e;

      a(io.reactivex.x var1, Object var2, io.reactivex.c.b var3) {
         this.a = var1;
         this.b = var3;
         this.c = var2;
      }

      public void dispose() {
         this.d.dispose();
      }

      public boolean isDisposed() {
         return this.d.isDisposed();
      }

      public void onComplete() {
         if(!this.e) {
            this.e = true;
            this.a.a_(this.c);
         }

      }

      public void onError(Throwable var1) {
         if(this.e) {
            io.reactivex.g.a.a(var1);
         } else {
            this.e = true;
            this.a.onError(var1);
         }

      }

      public void onNext(Object var1) {
         if(!this.e) {
            try {
               this.b.a(this.c, var1);
            } catch (Throwable var2) {
               this.d.dispose();
               this.onError(var2);
            }
         }

      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.d, var1)) {
            this.d = var1;
            this.a.onSubscribe(this);
         }

      }
   }
}
