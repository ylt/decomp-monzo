package io.reactivex.d.e.d;

import java.util.concurrent.atomic.AtomicReference;

public final class de extends a {
   final io.reactivex.u b;

   public de(io.reactivex.r var1, io.reactivex.u var2) {
      super(var1);
      this.b = var2;
   }

   public void subscribeActual(io.reactivex.t var1) {
      de.a var2 = new de.a(var1);
      var1.onSubscribe(var2);
      var2.a(this.b.a((Runnable)(new de.b(var2))));
   }

   static final class a extends AtomicReference implements io.reactivex.b.b, io.reactivex.t {
      final io.reactivex.t a;
      final AtomicReference b;

      a(io.reactivex.t var1) {
         this.a = var1;
         this.b = new AtomicReference();
      }

      void a(io.reactivex.b.b var1) {
         io.reactivex.d.a.d.b(this, var1);
      }

      public void dispose() {
         io.reactivex.d.a.d.a(this.b);
         io.reactivex.d.a.d.a((AtomicReference)this);
      }

      public boolean isDisposed() {
         return io.reactivex.d.a.d.a((io.reactivex.b.b)this.get());
      }

      public void onComplete() {
         this.a.onComplete();
      }

      public void onError(Throwable var1) {
         this.a.onError(var1);
      }

      public void onNext(Object var1) {
         this.a.onNext(var1);
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         io.reactivex.d.a.d.b(this.b, var1);
      }
   }

   final class b implements Runnable {
      private final de.a b;

      b(de.a var2) {
         this.b = var2;
      }

      public void run() {
         de.this.a.subscribe(this.b);
      }
   }
}
