package io.reactivex.d.e.d;

import java.util.concurrent.atomic.AtomicInteger;

public final class ci extends a {
   final long b;

   public ci(io.reactivex.n var1, long var2) {
      super(var1);
      this.b = var2;
   }

   public void subscribeActual(io.reactivex.t var1) {
      long var2 = Long.MAX_VALUE;
      io.reactivex.d.a.k var4 = new io.reactivex.d.a.k();
      var1.onSubscribe(var4);
      if(this.b != Long.MAX_VALUE) {
         var2 = this.b - 1L;
      }

      (new ci.a(var1, var2, var4, this.a)).a();
   }

   static final class a extends AtomicInteger implements io.reactivex.t {
      final io.reactivex.t a;
      final io.reactivex.d.a.k b;
      final io.reactivex.r c;
      long d;

      a(io.reactivex.t var1, long var2, io.reactivex.d.a.k var4, io.reactivex.r var5) {
         this.a = var1;
         this.b = var4;
         this.c = var5;
         this.d = var2;
      }

      void a() {
         if(this.getAndIncrement() == 0) {
            int var1 = 1;

            while(!this.b.isDisposed()) {
               this.c.subscribe(this);
               int var2 = this.addAndGet(-var1);
               var1 = var2;
               if(var2 == 0) {
                  break;
               }
            }
         }

      }

      public void onComplete() {
         long var1 = this.d;
         if(var1 != Long.MAX_VALUE) {
            this.d = var1 - 1L;
         }

         if(var1 != 0L) {
            this.a();
         } else {
            this.a.onComplete();
         }

      }

      public void onError(Throwable var1) {
         this.a.onError(var1);
      }

      public void onNext(Object var1) {
         this.a.onNext(var1);
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         this.b.b(var1);
      }
   }
}
