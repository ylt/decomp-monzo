package io.reactivex.d.e.d;

import java.util.concurrent.Callable;

public final class bf extends io.reactivex.n {
   final Callable a;
   final io.reactivex.c.c b;
   final io.reactivex.c.g c;

   public bf(Callable var1, io.reactivex.c.c var2, io.reactivex.c.g var3) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
   }

   public void subscribeActual(io.reactivex.t var1) {
      Object var2;
      try {
         var2 = this.a.call();
      } catch (Throwable var3) {
         io.reactivex.exceptions.a.b(var3);
         io.reactivex.d.a.e.a(var3, var1);
         return;
      }

      bf.a var4 = new bf.a(var1, this.b, this.c, var2);
      var1.onSubscribe(var4);
      var4.b();
   }

   static final class a implements io.reactivex.b.b, io.reactivex.e {
      final io.reactivex.t a;
      final io.reactivex.c.c b;
      final io.reactivex.c.g c;
      Object d;
      volatile boolean e;
      boolean f;
      boolean g;

      a(io.reactivex.t var1, io.reactivex.c.c var2, io.reactivex.c.g var3, Object var4) {
         this.a = var1;
         this.b = var2;
         this.c = var3;
         this.d = var4;
      }

      private void b(Object var1) {
         try {
            this.c.a(var1);
         } catch (Throwable var2) {
            io.reactivex.exceptions.a.b(var2);
            io.reactivex.g.a.a(var2);
         }

      }

      public void a() {
         if(!this.f) {
            this.f = true;
            this.a.onComplete();
         }

      }

      public void a(Object var1) {
         if(!this.f) {
            if(this.g) {
               this.a((Throwable)(new IllegalStateException("onNext already called in this generate turn")));
            } else if(var1 == null) {
               this.a((Throwable)(new NullPointerException("onNext called with null. Null values are generally not allowed in 2.x operators and sources.")));
            } else {
               this.g = true;
               this.a.onNext(var1);
            }
         }

      }

      public void a(Throwable var1) {
         if(this.f) {
            io.reactivex.g.a.a(var1);
         } else {
            Object var2 = var1;
            if(var1 == null) {
               var2 = new NullPointerException("onError called with null. Null values are generally not allowed in 2.x operators and sources.");
            }

            this.f = true;
            this.a.onError((Throwable)var2);
         }

      }

      public void b() {
         Object var1 = this.d;
         if(this.e) {
            this.d = null;
            this.b(var1);
         } else {
            io.reactivex.c.c var3 = this.b;

            while(!this.e) {
               this.g = false;

               Object var2;
               try {
                  var2 = var3.a(var1, this);
               } catch (Throwable var4) {
                  io.reactivex.exceptions.a.b(var4);
                  this.d = null;
                  this.e = true;
                  this.a(var4);
                  this.b(var1);
                  return;
               }

               var1 = var2;
               if(this.f) {
                  this.e = true;
                  this.d = null;
                  this.b(var2);
                  return;
               }
            }

            this.d = null;
            this.b(var1);
         }

      }

      public void dispose() {
         this.e = true;
      }

      public boolean isDisposed() {
         return this.e;
      }
   }
}
