package io.reactivex.d.e.d;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.atomic.AtomicReferenceArray;

public final class ed extends a {
   final io.reactivex.r[] b;
   final Iterable c;
   final io.reactivex.c.h d;

   public ed(io.reactivex.r var1, Iterable var2, io.reactivex.c.h var3) {
      super(var1);
      this.b = null;
      this.c = var2;
      this.d = var3;
   }

   public ed(io.reactivex.r var1, io.reactivex.r[] var2, io.reactivex.c.h var3) {
      super(var1);
      this.b = var2;
      this.c = null;
      this.d = var3;
   }

   protected void subscribeActual(io.reactivex.t param1) {
      // $FF: Couldn't be decompiled
   }

   final class a implements io.reactivex.c.h {
      public Object a(Object var1) throws Exception {
         return io.reactivex.d.b.b.a(ed.this.d.a(new Object[]{var1}), "The combiner returned a null value");
      }
   }

   static final class b extends AtomicInteger implements io.reactivex.b.b, io.reactivex.t {
      final io.reactivex.t a;
      final io.reactivex.c.h b;
      final ed.c[] c;
      final AtomicReferenceArray d;
      final AtomicReference e;
      final io.reactivex.d.j.c f;
      volatile boolean g;

      b(io.reactivex.t var1, io.reactivex.c.h var2, int var3) {
         this.a = var1;
         this.b = var2;
         ed.c[] var5 = new ed.c[var3];

         for(int var4 = 0; var4 < var3; ++var4) {
            var5[var4] = new ed.c(this, var4);
         }

         this.c = var5;
         this.d = new AtomicReferenceArray(var3);
         this.e = new AtomicReference();
         this.f = new io.reactivex.d.j.c();
      }

      void a(int var1) {
         ed.c[] var3 = this.c;

         for(int var2 = 0; var2 < var3.length; ++var2) {
            if(var2 != var1) {
               var3[var2].a();
            }
         }

      }

      void a(int var1, Object var2) {
         this.d.set(var1, var2);
      }

      void a(int var1, Throwable var2) {
         this.g = true;
         io.reactivex.d.a.d.a(this.e);
         this.a(var1);
         io.reactivex.d.j.k.a((io.reactivex.t)this.a, (Throwable)var2, this, this.f);
      }

      void a(int var1, boolean var2) {
         if(!var2) {
            this.g = true;
            this.a(var1);
            io.reactivex.d.j.k.a((io.reactivex.t)this.a, this, this.f);
         }

      }

      void a(io.reactivex.r[] var1, int var2) {
         ed.c[] var4 = this.c;
         AtomicReference var5 = this.e;

         for(int var3 = 0; var3 < var2 && !io.reactivex.d.a.d.a((io.reactivex.b.b)var5.get()) && !this.g; ++var3) {
            var1[var3].subscribe(var4[var3]);
         }

      }

      public void dispose() {
         io.reactivex.d.a.d.a(this.e);
         ed.c[] var3 = this.c;
         int var2 = var3.length;

         for(int var1 = 0; var1 < var2; ++var1) {
            var3[var1].a();
         }

      }

      public boolean isDisposed() {
         return io.reactivex.d.a.d.a((io.reactivex.b.b)this.e.get());
      }

      public void onComplete() {
         if(!this.g) {
            this.g = true;
            this.a(-1);
            io.reactivex.d.j.k.a((io.reactivex.t)this.a, this, this.f);
         }

      }

      public void onError(Throwable var1) {
         if(this.g) {
            io.reactivex.g.a.a(var1);
         } else {
            this.g = true;
            this.a(-1);
            io.reactivex.d.j.k.a((io.reactivex.t)this.a, (Throwable)var1, this, this.f);
         }

      }

      public void onNext(Object var1) {
         int var2 = 0;
         if(!this.g) {
            AtomicReferenceArray var5 = this.d;
            int var3 = var5.length();
            Object[] var4 = new Object[var3 + 1];
            var4[0] = var1;

            while(true) {
               if(var2 >= var3) {
                  try {
                     var1 = io.reactivex.d.b.b.a(this.b.a(var4), "combiner returned a null value");
                  } catch (Throwable var6) {
                     io.reactivex.exceptions.a.b(var6);
                     this.dispose();
                     this.onError(var6);
                     break;
                  }

                  io.reactivex.d.j.k.a((io.reactivex.t)this.a, (Object)var1, this, this.f);
                  break;
               }

               var1 = var5.get(var2);
               if(var1 == null) {
                  break;
               }

               var4[var2 + 1] = var1;
               ++var2;
            }
         }

      }

      public void onSubscribe(io.reactivex.b.b var1) {
         io.reactivex.d.a.d.b(this.e, var1);
      }
   }

   static final class c extends AtomicReference implements io.reactivex.t {
      final ed.b a;
      final int b;
      boolean c;

      c(ed.b var1, int var2) {
         this.a = var1;
         this.b = var2;
      }

      public void a() {
         io.reactivex.d.a.d.a((AtomicReference)this);
      }

      public void onComplete() {
         this.a.a(this.b, this.c);
      }

      public void onError(Throwable var1) {
         this.a.a(this.b, var1);
      }

      public void onNext(Object var1) {
         if(!this.c) {
            this.c = true;
         }

         this.a.a(this.b, var1);
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         io.reactivex.d.a.d.b(this, var1);
      }
   }
}
