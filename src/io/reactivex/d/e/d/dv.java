package io.reactivex.d.e.d;

import java.util.concurrent.atomic.AtomicBoolean;

public final class dv extends a {
   final io.reactivex.u b;

   public dv(io.reactivex.r var1, io.reactivex.u var2) {
      super(var1);
      this.b = var2;
   }

   public void subscribeActual(io.reactivex.t var1) {
      this.a.subscribe(new dv.a(var1, this.b));
   }

   static final class a extends AtomicBoolean implements io.reactivex.b.b, io.reactivex.t {
      final io.reactivex.t a;
      final io.reactivex.u b;
      io.reactivex.b.b c;

      a(io.reactivex.t var1, io.reactivex.u var2) {
         this.a = var1;
         this.b = var2;
      }

      public void dispose() {
         if(this.compareAndSet(false, true)) {
            this.b.a((Runnable)(new dv.a()));
         }

      }

      public boolean isDisposed() {
         return this.get();
      }

      public void onComplete() {
         if(!this.get()) {
            this.a.onComplete();
         }

      }

      public void onError(Throwable var1) {
         if(this.get()) {
            io.reactivex.g.a.a(var1);
         } else {
            this.a.onError(var1);
         }

      }

      public void onNext(Object var1) {
         if(!this.get()) {
            this.a.onNext(var1);
         }

      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.c, var1)) {
            this.c = var1;
            this.a.onSubscribe(this);
         }

      }
   }

   final class a implements Runnable {
      public void run() {
         dv.super.c.dispose();
      }
   }
}
