package io.reactivex.d.e.d;

public final class cf extends io.reactivex.v {
   final io.reactivex.r a;
   final Object b;
   final io.reactivex.c.c c;

   public cf(io.reactivex.r var1, Object var2, io.reactivex.c.c var3) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
   }

   protected void b(io.reactivex.x var1) {
      this.a.subscribe(new cf.a(var1, this.c, this.b));
   }

   static final class a implements io.reactivex.b.b, io.reactivex.t {
      final io.reactivex.x a;
      final io.reactivex.c.c b;
      Object c;
      io.reactivex.b.b d;

      a(io.reactivex.x var1, io.reactivex.c.c var2, Object var3) {
         this.a = var1;
         this.c = var3;
         this.b = var2;
      }

      public void dispose() {
         this.d.dispose();
      }

      public boolean isDisposed() {
         return this.d.isDisposed();
      }

      public void onComplete() {
         Object var1 = this.c;
         this.c = null;
         if(var1 != null) {
            this.a.a_(var1);
         }

      }

      public void onError(Throwable var1) {
         Object var2 = this.c;
         this.c = null;
         if(var2 != null) {
            this.a.onError(var1);
         } else {
            io.reactivex.g.a.a(var1);
         }

      }

      public void onNext(Object var1) {
         Object var2 = this.c;
         if(var2 != null) {
            try {
               this.c = io.reactivex.d.b.b.a(this.b.a(var2, var1), "The reducer returned a null value");
            } catch (Throwable var3) {
               io.reactivex.exceptions.a.b(var3);
               this.d.dispose();
               this.onError(var3);
            }
         }

      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.d, var1)) {
            this.d = var1;
            this.a.onSubscribe(this);
         }

      }
   }
}
