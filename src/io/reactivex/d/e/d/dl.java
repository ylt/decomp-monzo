package io.reactivex.d.e.d;

import java.util.concurrent.atomic.AtomicBoolean;

public final class dl extends a {
   final io.reactivex.r b;

   public dl(io.reactivex.r var1, io.reactivex.r var2) {
      super(var1);
      this.b = var2;
   }

   public void subscribeActual(io.reactivex.t var1) {
      io.reactivex.f.e var4 = new io.reactivex.f.e(var1);
      io.reactivex.d.a.a var3 = new io.reactivex.d.a.a(2);
      dl.b var2 = new dl.b(var4, var3);
      var1.onSubscribe(var3);
      this.b.subscribe(new dl.a(var3, var4));
      this.a.subscribe(var2);
   }

   final class a implements io.reactivex.t {
      private final io.reactivex.d.a.a b;
      private final io.reactivex.f.e c;

      a(io.reactivex.d.a.a var2, io.reactivex.f.e var3) {
         this.b = var2;
         this.c = var3;
      }

      public void onComplete() {
         this.b.dispose();
         this.c.onComplete();
      }

      public void onError(Throwable var1) {
         this.b.dispose();
         this.c.onError(var1);
      }

      public void onNext(Object var1) {
         this.b.dispose();
         this.c.onComplete();
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         this.b.a(1, var1);
      }
   }

   static final class b extends AtomicBoolean implements io.reactivex.t {
      final io.reactivex.t a;
      final io.reactivex.d.a.a b;
      io.reactivex.b.b c;

      b(io.reactivex.t var1, io.reactivex.d.a.a var2) {
         this.a = var1;
         this.b = var2;
      }

      public void onComplete() {
         this.b.dispose();
         this.a.onComplete();
      }

      public void onError(Throwable var1) {
         this.b.dispose();
         this.a.onError(var1);
      }

      public void onNext(Object var1) {
         this.a.onNext(var1);
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.c, var1)) {
            this.c = var1;
            this.b.a(0, var1);
         }

      }
   }
}
