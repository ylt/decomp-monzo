package io.reactivex.d.e.d;

public final class df extends a {
   final io.reactivex.r b;

   public df(io.reactivex.r var1, io.reactivex.r var2) {
      super(var1);
      this.b = var2;
   }

   public void subscribeActual(io.reactivex.t var1) {
      df.a var2 = new df.a(var1, this.b);
      var1.onSubscribe(var2.c);
      this.a.subscribe(var2);
   }

   static final class a implements io.reactivex.t {
      final io.reactivex.t a;
      final io.reactivex.r b;
      final io.reactivex.d.a.k c;
      boolean d;

      a(io.reactivex.t var1, io.reactivex.r var2) {
         this.a = var1;
         this.b = var2;
         this.d = true;
         this.c = new io.reactivex.d.a.k();
      }

      public void onComplete() {
         if(this.d) {
            this.d = false;
            this.b.subscribe(this);
         } else {
            this.a.onComplete();
         }

      }

      public void onError(Throwable var1) {
         this.a.onError(var1);
      }

      public void onNext(Object var1) {
         if(this.d) {
            this.d = false;
         }

         this.a.onNext(var1);
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         this.c.a(var1);
      }
   }
}
