package io.reactivex.d.e.d;

import java.util.NoSuchElementException;

public final class an extends a {
   final long b;
   final Object c;
   final boolean d;

   public an(io.reactivex.r var1, long var2, Object var4, boolean var5) {
      super(var1);
      this.b = var2;
      this.c = var4;
      this.d = var5;
   }

   public void subscribeActual(io.reactivex.t var1) {
      this.a.subscribe(new an.a(var1, this.b, this.c, this.d));
   }

   static final class a implements io.reactivex.b.b, io.reactivex.t {
      final io.reactivex.t a;
      final long b;
      final Object c;
      final boolean d;
      io.reactivex.b.b e;
      long f;
      boolean g;

      a(io.reactivex.t var1, long var2, Object var4, boolean var5) {
         this.a = var1;
         this.b = var2;
         this.c = var4;
         this.d = var5;
      }

      public void dispose() {
         this.e.dispose();
      }

      public boolean isDisposed() {
         return this.e.isDisposed();
      }

      public void onComplete() {
         if(!this.g) {
            this.g = true;
            Object var1 = this.c;
            if(var1 == null && this.d) {
               this.a.onError(new NoSuchElementException());
            } else {
               if(var1 != null) {
                  this.a.onNext(var1);
               }

               this.a.onComplete();
            }
         }

      }

      public void onError(Throwable var1) {
         if(this.g) {
            io.reactivex.g.a.a(var1);
         } else {
            this.g = true;
            this.a.onError(var1);
         }

      }

      public void onNext(Object var1) {
         if(!this.g) {
            long var2 = this.f;
            if(var2 == this.b) {
               this.g = true;
               this.e.dispose();
               this.a.onNext(var1);
               this.a.onComplete();
            } else {
               this.f = var2 + 1L;
            }
         }

      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.e, var1)) {
            this.e = var1;
            this.a.onSubscribe(this);
         }

      }
   }
}
