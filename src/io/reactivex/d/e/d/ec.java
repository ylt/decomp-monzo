package io.reactivex.d.e.d;

import java.util.concurrent.atomic.AtomicReference;

public final class ec extends a {
   final io.reactivex.c.c b;
   final io.reactivex.r c;

   public ec(io.reactivex.r var1, io.reactivex.c.c var2, io.reactivex.r var3) {
      super(var1);
      this.b = var2;
      this.c = var3;
   }

   public void subscribeActual(io.reactivex.t var1) {
      io.reactivex.f.e var2 = new io.reactivex.f.e(var1);
      ec.b var3 = new ec.b(var2, this.b);
      var2.onSubscribe(var3);
      this.c.subscribe(new ec.a(var3));
      this.a.subscribe(var3);
   }

   final class a implements io.reactivex.t {
      private final ec.b b;

      a(ec.b var2) {
         this.b = var2;
      }

      public void onComplete() {
      }

      public void onError(Throwable var1) {
         this.b.a(var1);
      }

      public void onNext(Object var1) {
         this.b.lazySet(var1);
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         this.b.a(var1);
      }
   }

   static final class b extends AtomicReference implements io.reactivex.b.b, io.reactivex.t {
      final io.reactivex.t a;
      final io.reactivex.c.c b;
      final AtomicReference c = new AtomicReference();
      final AtomicReference d = new AtomicReference();

      b(io.reactivex.t var1, io.reactivex.c.c var2) {
         this.a = var1;
         this.b = var2;
      }

      public void a(Throwable var1) {
         io.reactivex.d.a.d.a(this.c);
         this.a.onError(var1);
      }

      public boolean a(io.reactivex.b.b var1) {
         return io.reactivex.d.a.d.b(this.d, var1);
      }

      public void dispose() {
         io.reactivex.d.a.d.a(this.c);
         io.reactivex.d.a.d.a(this.d);
      }

      public boolean isDisposed() {
         return io.reactivex.d.a.d.a((io.reactivex.b.b)this.c.get());
      }

      public void onComplete() {
         io.reactivex.d.a.d.a(this.d);
         this.a.onComplete();
      }

      public void onError(Throwable var1) {
         io.reactivex.d.a.d.a(this.d);
         this.a.onError(var1);
      }

      public void onNext(Object var1) {
         Object var2 = this.get();
         if(var2 != null) {
            try {
               var1 = io.reactivex.d.b.b.a(this.b.a(var1, var2), "The combiner returned a null value");
            } catch (Throwable var3) {
               io.reactivex.exceptions.a.b(var3);
               this.dispose();
               this.a.onError(var3);
               return;
            }

            this.a.onNext(var1);
         }

      }

      public void onSubscribe(io.reactivex.b.b var1) {
         io.reactivex.d.a.d.b(this.c, var1);
      }
   }
}
