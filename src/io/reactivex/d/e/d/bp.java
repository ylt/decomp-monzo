package io.reactivex.d.e.d;

public final class bp extends io.reactivex.n implements io.reactivex.d.c.g {
   private final Object a;

   public bp(Object var1) {
      this.a = var1;
   }

   public Object call() {
      return this.a;
   }

   protected void subscribeActual(io.reactivex.t var1) {
      cr.a var2 = new cr.a(var1, this.a);
      var1.onSubscribe(var2);
      var2.run();
   }
}
