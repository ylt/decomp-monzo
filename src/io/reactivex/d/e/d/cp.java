package io.reactivex.d.e.d;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public final class cp extends a {
   final long b;
   final TimeUnit c;
   final io.reactivex.u d;
   final boolean e;

   public cp(io.reactivex.r var1, long var2, TimeUnit var4, io.reactivex.u var5, boolean var6) {
      super(var1);
      this.b = var2;
      this.c = var4;
      this.d = var5;
      this.e = var6;
   }

   public void subscribeActual(io.reactivex.t var1) {
      io.reactivex.f.e var2 = new io.reactivex.f.e(var1);
      if(this.e) {
         this.a.subscribe(new cp.a(var2, this.b, this.c, this.d));
      } else {
         this.a.subscribe(new cp.b(var2, this.b, this.c, this.d));
      }

   }

   static final class a extends cp.c {
      final AtomicInteger a = new AtomicInteger(1);

      a(io.reactivex.t var1, long var2, TimeUnit var4, io.reactivex.u var5) {
         super(var1, var2, var4, var5);
      }

      void a() {
         this.c();
         if(this.a.decrementAndGet() == 0) {
            this.b.onComplete();
         }

      }

      public void run() {
         if(this.a.incrementAndGet() == 2) {
            this.c();
            if(this.a.decrementAndGet() == 0) {
               this.b.onComplete();
            }
         }

      }
   }

   static final class b extends cp.c {
      b(io.reactivex.t var1, long var2, TimeUnit var4, io.reactivex.u var5) {
         super(var1, var2, var4, var5);
      }

      void a() {
         this.b.onComplete();
      }

      public void run() {
         this.c();
      }
   }

   abstract static class c extends AtomicReference implements io.reactivex.b.b, io.reactivex.t, Runnable {
      final io.reactivex.t b;
      final long c;
      final TimeUnit d;
      final io.reactivex.u e;
      final AtomicReference f = new AtomicReference();
      io.reactivex.b.b g;

      c(io.reactivex.t var1, long var2, TimeUnit var4, io.reactivex.u var5) {
         this.b = var1;
         this.c = var2;
         this.d = var4;
         this.e = var5;
      }

      abstract void a();

      void b() {
         io.reactivex.d.a.d.a(this.f);
      }

      void c() {
         Object var1 = this.getAndSet((Object)null);
         if(var1 != null) {
            this.b.onNext(var1);
         }

      }

      public void dispose() {
         this.b();
         this.g.dispose();
      }

      public boolean isDisposed() {
         return this.g.isDisposed();
      }

      public void onComplete() {
         this.b();
         this.a();
      }

      public void onError(Throwable var1) {
         this.b();
         this.b.onError(var1);
      }

      public void onNext(Object var1) {
         this.lazySet(var1);
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.g, var1)) {
            this.g = var1;
            this.b.onSubscribe(this);
            var1 = this.e.a(this, this.c, this.c, this.d);
            io.reactivex.d.a.d.c(this.f, var1);
         }

      }
   }
}
