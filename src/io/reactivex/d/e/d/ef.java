package io.reactivex.d.e.d;

import java.util.Iterator;

public final class ef extends io.reactivex.n {
   final io.reactivex.n a;
   final Iterable b;
   final io.reactivex.c.c c;

   public ef(io.reactivex.n var1, Iterable var2, io.reactivex.c.c var3) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
   }

   public void subscribeActual(io.reactivex.t var1) {
      Iterator var3;
      try {
         var3 = (Iterator)io.reactivex.d.b.b.a(this.b.iterator(), (String)"The iterator returned by other is null");
      } catch (Throwable var5) {
         io.reactivex.exceptions.a.b(var5);
         io.reactivex.d.a.e.a(var5, var1);
         return;
      }

      boolean var2;
      try {
         var2 = var3.hasNext();
      } catch (Throwable var4) {
         io.reactivex.exceptions.a.b(var4);
         io.reactivex.d.a.e.a(var4, var1);
         return;
      }

      if(!var2) {
         io.reactivex.d.a.e.a(var1);
      } else {
         this.a.subscribe((io.reactivex.t)(new ef.a(var1, var3, this.c)));
      }

   }

   static final class a implements io.reactivex.b.b, io.reactivex.t {
      final io.reactivex.t a;
      final Iterator b;
      final io.reactivex.c.c c;
      io.reactivex.b.b d;
      boolean e;

      a(io.reactivex.t var1, Iterator var2, io.reactivex.c.c var3) {
         this.a = var1;
         this.b = var2;
         this.c = var3;
      }

      void a(Throwable var1) {
         this.e = true;
         this.d.dispose();
         this.a.onError(var1);
      }

      public void dispose() {
         this.d.dispose();
      }

      public boolean isDisposed() {
         return this.d.isDisposed();
      }

      public void onComplete() {
         if(!this.e) {
            this.e = true;
            this.a.onComplete();
         }

      }

      public void onError(Throwable var1) {
         if(this.e) {
            io.reactivex.g.a.a(var1);
         } else {
            this.e = true;
            this.a.onError(var1);
         }

      }

      public void onNext(Object var1) {
         if(!this.e) {
            Object var3;
            try {
               var3 = io.reactivex.d.b.b.a(this.b.next(), "The iterator returned a null value");
            } catch (Throwable var6) {
               io.reactivex.exceptions.a.b(var6);
               this.a(var6);
               return;
            }

            try {
               var1 = io.reactivex.d.b.b.a(this.c.a(var1, var3), "The zipper function returned a null value");
            } catch (Throwable var5) {
               io.reactivex.exceptions.a.b(var5);
               this.a(var5);
               return;
            }

            this.a.onNext(var1);

            boolean var2;
            try {
               var2 = this.b.hasNext();
            } catch (Throwable var4) {
               io.reactivex.exceptions.a.b(var4);
               this.a(var4);
               return;
            }

            if(!var2) {
               this.e = true;
               this.d.dispose();
               this.a.onComplete();
            }
         }

      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.d, var1)) {
            this.d = var1;
            this.a.onSubscribe(this);
         }

      }
   }
}
