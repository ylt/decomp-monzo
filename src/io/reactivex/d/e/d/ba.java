package io.reactivex.d.e.d;

import java.util.concurrent.Callable;

public final class ba extends io.reactivex.n implements Callable {
   final Callable a;

   public ba(Callable var1) {
      this.a = var1;
   }

   public Object call() throws Exception {
      return io.reactivex.d.b.b.a(this.a.call(), "The callable returned a null value");
   }

   public void subscribeActual(io.reactivex.t var1) {
      io.reactivex.d.d.k var2 = new io.reactivex.d.d.k(var1);
      var1.onSubscribe(var2);
      if(!var2.isDisposed()) {
         Object var3;
         try {
            var3 = io.reactivex.d.b.b.a(this.a.call(), "Callable returned null");
         } catch (Throwable var4) {
            io.reactivex.exceptions.a.b(var4);
            if(!var2.isDisposed()) {
               var1.onError(var4);
            } else {
               io.reactivex.g.a.a(var4);
            }

            return;
         }

         var2.b(var3);
      }

   }
}
