package io.reactivex.d.e.d;

import io.reactivex.exceptions.CompositeException;

public final class bz extends a {
   final io.reactivex.c.h b;

   public bz(io.reactivex.r var1, io.reactivex.c.h var2) {
      super(var1);
      this.b = var2;
   }

   public void subscribeActual(io.reactivex.t var1) {
      this.a.subscribe(new bz.a(var1, this.b));
   }

   static final class a implements io.reactivex.b.b, io.reactivex.t {
      final io.reactivex.t a;
      final io.reactivex.c.h b;
      io.reactivex.b.b c;

      a(io.reactivex.t var1, io.reactivex.c.h var2) {
         this.a = var1;
         this.b = var2;
      }

      public void dispose() {
         this.c.dispose();
      }

      public boolean isDisposed() {
         return this.c.isDisposed();
      }

      public void onComplete() {
         this.a.onComplete();
      }

      public void onError(Throwable var1) {
         Object var2;
         try {
            var2 = this.b.a(var1);
         } catch (Throwable var3) {
            io.reactivex.exceptions.a.b(var3);
            this.a.onError(new CompositeException(new Throwable[]{var1, var3}));
            return;
         }

         if(var2 == null) {
            NullPointerException var4 = new NullPointerException("The supplied value is null");
            var4.initCause(var1);
            this.a.onError(var4);
         } else {
            this.a.onNext(var2);
            this.a.onComplete();
         }

      }

      public void onNext(Object var1) {
         this.a.onNext(var1);
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.c, var1)) {
            this.c = var1;
            this.a.onSubscribe(this);
         }

      }
   }
}
