package io.reactivex.d.e.d;

import java.util.NoSuchElementException;

public final class cy extends io.reactivex.v {
   final io.reactivex.r a;
   final Object b;

   public cy(io.reactivex.r var1, Object var2) {
      this.a = var1;
      this.b = var2;
   }

   public void b(io.reactivex.x var1) {
      this.a.subscribe(new cy.a(var1, this.b));
   }

   static final class a implements io.reactivex.b.b, io.reactivex.t {
      final io.reactivex.x a;
      final Object b;
      io.reactivex.b.b c;
      Object d;
      boolean e;

      a(io.reactivex.x var1, Object var2) {
         this.a = var1;
         this.b = var2;
      }

      public void dispose() {
         this.c.dispose();
      }

      public boolean isDisposed() {
         return this.c.isDisposed();
      }

      public void onComplete() {
         if(!this.e) {
            this.e = true;
            Object var2 = this.d;
            this.d = null;
            Object var1 = var2;
            if(var2 == null) {
               var1 = this.b;
            }

            if(var1 != null) {
               this.a.a_(var1);
            } else {
               this.a.onError(new NoSuchElementException());
            }
         }

      }

      public void onError(Throwable var1) {
         if(this.e) {
            io.reactivex.g.a.a(var1);
         } else {
            this.e = true;
            this.a.onError(var1);
         }

      }

      public void onNext(Object var1) {
         if(!this.e) {
            if(this.d != null) {
               this.e = true;
               this.c.dispose();
               this.a.onError(new IllegalArgumentException("Sequence contains more than one element!"));
            } else {
               this.d = var1;
            }
         }

      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.c, var1)) {
            this.c = var1;
            this.a.onSubscribe(this);
         }

      }
   }
}
