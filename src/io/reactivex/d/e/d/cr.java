package io.reactivex.d.e.d;

import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicInteger;

public final class cr {
   public static io.reactivex.n a(Object var0, io.reactivex.c.h var1) {
      return io.reactivex.g.a.a((io.reactivex.n)(new cr.b(var0, var1)));
   }

   public static boolean a(io.reactivex.r var0, io.reactivex.t var1, io.reactivex.c.h var2) {
      boolean var3;
      if(var0 instanceof Callable) {
         Object var7;
         try {
            var7 = ((Callable)var0).call();
         } catch (Throwable var6) {
            io.reactivex.exceptions.a.b(var6);
            io.reactivex.d.a.e.a(var6, var1);
            var3 = true;
            return var3;
         }

         if(var7 == null) {
            io.reactivex.d.a.e.a(var1);
            var3 = true;
         } else {
            try {
               var0 = (io.reactivex.r)io.reactivex.d.b.b.a(var2.a(var7), "The mapper returned a null ObservableSource");
            } catch (Throwable var5) {
               io.reactivex.exceptions.a.b(var5);
               io.reactivex.d.a.e.a(var5, var1);
               var3 = true;
               return var3;
            }

            if(var0 instanceof Callable) {
               try {
                  var7 = ((Callable)var0).call();
               } catch (Throwable var4) {
                  io.reactivex.exceptions.a.b(var4);
                  io.reactivex.d.a.e.a(var4, var1);
                  var3 = true;
                  return var3;
               }

               if(var7 == null) {
                  io.reactivex.d.a.e.a(var1);
                  var3 = true;
                  return var3;
               }

               cr.a var8 = new cr.a(var1, var7);
               var1.onSubscribe(var8);
               var8.run();
            } else {
               var0.subscribe(var1);
            }

            var3 = true;
         }
      } else {
         var3 = false;
      }

      return var3;
   }

   public static final class a extends AtomicInteger implements io.reactivex.d.c.d, Runnable {
      final io.reactivex.t a;
      final Object b;

      public a(io.reactivex.t var1, Object var2) {
         this.a = var1;
         this.b = var2;
      }

      public int a(int var1) {
         byte var2 = 1;
         byte var3;
         if((var1 & 1) != 0) {
            this.lazySet(1);
            var3 = var2;
         } else {
            var3 = 0;
         }

         return var3;
      }

      public boolean a(Object var1) {
         throw new UnsupportedOperationException("Should not be called!");
      }

      public boolean b() {
         boolean var1 = true;
         if(this.get() == 1) {
            var1 = false;
         }

         return var1;
      }

      public void c() {
         this.lazySet(3);
      }

      public void dispose() {
         this.set(3);
      }

      public boolean isDisposed() {
         boolean var1;
         if(this.get() == 3) {
            var1 = true;
         } else {
            var1 = false;
         }

         return var1;
      }

      public Object n_() throws Exception {
         Object var1;
         if(this.get() == 1) {
            this.lazySet(3);
            var1 = this.b;
         } else {
            var1 = null;
         }

         return var1;
      }

      public void run() {
         if(this.get() == 0 && this.compareAndSet(0, 2)) {
            this.a.onNext(this.b);
            if(this.get() == 2) {
               this.lazySet(3);
               this.a.onComplete();
            }
         }

      }
   }

   static final class b extends io.reactivex.n {
      final Object a;
      final io.reactivex.c.h b;

      b(Object var1, io.reactivex.c.h var2) {
         this.a = var1;
         this.b = var2;
      }

      public void subscribeActual(io.reactivex.t var1) {
         io.reactivex.r var2;
         try {
            var2 = (io.reactivex.r)io.reactivex.d.b.b.a(this.b.a(this.a), "The mapper returned a null ObservableSource");
         } catch (Throwable var4) {
            io.reactivex.d.a.e.a(var4, var1);
            return;
         }

         if(var2 instanceof Callable) {
            Object var5;
            try {
               var5 = ((Callable)var2).call();
            } catch (Throwable var3) {
               io.reactivex.exceptions.a.b(var3);
               io.reactivex.d.a.e.a(var3, var1);
               return;
            }

            if(var5 == null) {
               io.reactivex.d.a.e.a(var1);
            } else {
               cr.a var6 = new cr.a(var1, var5);
               var1.onSubscribe(var6);
               var6.run();
            }
         } else {
            var2.subscribe(var1);
         }

      }
   }
}
