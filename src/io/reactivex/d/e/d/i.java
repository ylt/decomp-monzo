package io.reactivex.d.e.d;

public final class i extends a {
   final io.reactivex.c.q b;

   public i(io.reactivex.r var1, io.reactivex.c.q var2) {
      super(var1);
      this.b = var2;
   }

   protected void subscribeActual(io.reactivex.t var1) {
      this.a.subscribe(new i.a(var1, this.b));
   }

   static final class a implements io.reactivex.b.b, io.reactivex.t {
      final io.reactivex.t a;
      final io.reactivex.c.q b;
      io.reactivex.b.b c;
      boolean d;

      a(io.reactivex.t var1, io.reactivex.c.q var2) {
         this.a = var1;
         this.b = var2;
      }

      public void dispose() {
         this.c.dispose();
      }

      public boolean isDisposed() {
         return this.c.isDisposed();
      }

      public void onComplete() {
         if(!this.d) {
            this.d = true;
            this.a.onNext(Boolean.valueOf(false));
            this.a.onComplete();
         }

      }

      public void onError(Throwable var1) {
         if(this.d) {
            io.reactivex.g.a.a(var1);
         } else {
            this.d = true;
            this.a.onError(var1);
         }

      }

      public void onNext(Object var1) {
         if(!this.d) {
            boolean var2;
            try {
               var2 = this.b.a(var1);
            } catch (Throwable var3) {
               io.reactivex.exceptions.a.b(var3);
               this.c.dispose();
               this.onError(var3);
               return;
            }

            if(var2) {
               this.d = true;
               this.c.dispose();
               this.a.onNext(Boolean.valueOf(true));
               this.a.onComplete();
            }
         }

      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.c, var1)) {
            this.c = var1;
            this.a.onSubscribe(this);
         }

      }
   }
}
