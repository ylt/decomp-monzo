package io.reactivex.d.e.d;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

public final class ds extends io.reactivex.n {
   final io.reactivex.u a;
   final long b;
   final TimeUnit c;

   public ds(long var1, TimeUnit var3, io.reactivex.u var4) {
      this.b = var1;
      this.c = var3;
      this.a = var4;
   }

   public void subscribeActual(io.reactivex.t var1) {
      ds.a var2 = new ds.a(var1);
      var1.onSubscribe(var2);
      var2.a(this.a.a(var2, this.b, this.c));
   }

   static final class a extends AtomicReference implements io.reactivex.b.b, Runnable {
      final io.reactivex.t a;

      a(io.reactivex.t var1) {
         this.a = var1;
      }

      public void a(io.reactivex.b.b var1) {
         io.reactivex.d.a.d.d(this, var1);
      }

      public void dispose() {
         io.reactivex.d.a.d.a((AtomicReference)this);
      }

      public boolean isDisposed() {
         boolean var1;
         if(this.get() == io.reactivex.d.a.d.a) {
            var1 = true;
         } else {
            var1 = false;
         }

         return var1;
      }

      public void run() {
         if(!this.isDisposed()) {
            this.a.onNext(Long.valueOf(0L));
            this.lazySet(io.reactivex.d.a.e.a);
            this.a.onComplete();
         }

      }
   }
}
