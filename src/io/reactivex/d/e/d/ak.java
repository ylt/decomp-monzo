package io.reactivex.d.e.d;

public final class ak extends a {
   final io.reactivex.c.a b;

   public ak(io.reactivex.r var1, io.reactivex.c.a var2) {
      super(var1);
      this.b = var2;
   }

   protected void subscribeActual(io.reactivex.t var1) {
      this.a.subscribe(new ak.a(var1, this.b));
   }

   static final class a extends io.reactivex.d.d.b implements io.reactivex.t {
      final io.reactivex.t a;
      final io.reactivex.c.a b;
      io.reactivex.b.b c;
      io.reactivex.d.c.d d;
      boolean e;

      a(io.reactivex.t var1, io.reactivex.c.a var2) {
         this.a = var1;
         this.b = var2;
      }

      public int a(int var1) {
         boolean var4 = true;
         byte var3 = 0;
         io.reactivex.d.c.d var5 = this.d;
         int var2 = var3;
         if(var5 != null) {
            var2 = var3;
            if((var1 & 4) == 0) {
               var2 = var5.a(var1);
               if(var2 != 0) {
                  if(var2 != 1) {
                     var4 = false;
                  }

                  this.e = var4;
               }
            }
         }

         return var2;
      }

      public boolean b() {
         return this.d.b();
      }

      public void c() {
         this.d.c();
      }

      void d() {
         if(this.compareAndSet(0, 1)) {
            try {
               this.b.a();
            } catch (Throwable var2) {
               io.reactivex.exceptions.a.b(var2);
               io.reactivex.g.a.a(var2);
            }
         }

      }

      public void dispose() {
         this.c.dispose();
         this.d();
      }

      public boolean isDisposed() {
         return this.c.isDisposed();
      }

      public Object n_() throws Exception {
         Object var1 = this.d.n_();
         if(var1 == null && this.e) {
            this.d();
         }

         return var1;
      }

      public void onComplete() {
         this.a.onComplete();
         this.d();
      }

      public void onError(Throwable var1) {
         this.a.onError(var1);
         this.d();
      }

      public void onNext(Object var1) {
         this.a.onNext(var1);
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.c, var1)) {
            this.c = var1;
            if(var1 instanceof io.reactivex.d.c.d) {
               this.d = (io.reactivex.d.c.d)var1;
            }

            this.a.onSubscribe(this);
         }

      }
   }
}
