package io.reactivex.d.e.d;

public final class dh extends a {
   final long b;

   public dh(io.reactivex.r var1, long var2) {
      super(var1);
      this.b = var2;
   }

   protected void subscribeActual(io.reactivex.t var1) {
      this.a.subscribe(new dh.a(var1, this.b));
   }

   static final class a implements io.reactivex.b.b, io.reactivex.t {
      final io.reactivex.t a;
      boolean b;
      io.reactivex.b.b c;
      long d;

      a(io.reactivex.t var1, long var2) {
         this.a = var1;
         this.d = var2;
      }

      public void dispose() {
         this.c.dispose();
      }

      public boolean isDisposed() {
         return this.c.isDisposed();
      }

      public void onComplete() {
         if(!this.b) {
            this.b = true;
            this.c.dispose();
            this.a.onComplete();
         }

      }

      public void onError(Throwable var1) {
         if(this.b) {
            io.reactivex.g.a.a(var1);
         } else {
            this.b = true;
            this.c.dispose();
            this.a.onError(var1);
         }

      }

      public void onNext(Object var1) {
         if(!this.b) {
            long var3 = this.d;
            this.d = var3 - 1L;
            if(var3 > 0L) {
               boolean var2;
               if(this.d == 0L) {
                  var2 = true;
               } else {
                  var2 = false;
               }

               this.a.onNext(var1);
               if(var2) {
                  this.onComplete();
               }
            }
         }

      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.c, var1)) {
            this.c = var1;
            if(this.d == 0L) {
               this.b = true;
               var1.dispose();
               io.reactivex.d.a.e.a(this.a);
            } else {
               this.a.onSubscribe(this);
            }
         }

      }
   }
}
