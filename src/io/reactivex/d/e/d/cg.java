package io.reactivex.d.e.d;

import java.util.concurrent.Callable;

public final class cg extends io.reactivex.v {
   final io.reactivex.r a;
   final Callable b;
   final io.reactivex.c.c c;

   public cg(io.reactivex.r var1, Callable var2, io.reactivex.c.c var3) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
   }

   protected void b(io.reactivex.x var1) {
      Object var2;
      try {
         var2 = io.reactivex.d.b.b.a(this.b.call(), "The seedSupplier returned a null value");
      } catch (Throwable var3) {
         io.reactivex.exceptions.a.b(var3);
         io.reactivex.d.a.e.a(var3, var1);
         return;
      }

      this.a.subscribe(new cf.a(var1, this.c, var2));
   }
}
