package io.reactivex.d.e.d;

import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public final class cl extends io.reactivex.e.a implements io.reactivex.b.b {
   static final cl.b e = new cl.o();
   final io.reactivex.r a;
   final AtomicReference b;
   final cl.b c;
   final io.reactivex.r d;

   private cl(io.reactivex.r var1, io.reactivex.r var2, AtomicReference var3, cl.b var4) {
      this.d = var1;
      this.a = var2;
      this.b = var3;
      this.c = var4;
   }

   public static io.reactivex.e.a a(io.reactivex.e.a var0, io.reactivex.u var1) {
      return io.reactivex.g.a.a((io.reactivex.e.a)(new cl.g(var0, var0.observeOn(var1))));
   }

   public static io.reactivex.e.a a(io.reactivex.r var0) {
      return a(var0, e);
   }

   public static io.reactivex.e.a a(io.reactivex.r var0, int var1) {
      io.reactivex.e.a var2;
      if(var1 == Integer.MAX_VALUE) {
         var2 = a(var0);
      } else {
         var2 = a((io.reactivex.r)var0, (cl.b)(new cl.i(var1)));
      }

      return var2;
   }

   public static io.reactivex.e.a a(io.reactivex.r var0, long var1, TimeUnit var3, io.reactivex.u var4) {
      return a(var0, var1, var3, var4, Integer.MAX_VALUE);
   }

   public static io.reactivex.e.a a(io.reactivex.r var0, long var1, TimeUnit var3, io.reactivex.u var4, int var5) {
      return a((io.reactivex.r)var0, (cl.b)(new cl.l(var5, var1, var3, var4)));
   }

   static io.reactivex.e.a a(io.reactivex.r var0, cl.b var1) {
      AtomicReference var2 = new AtomicReference();
      return io.reactivex.g.a.a((io.reactivex.e.a)(new cl(new cl.k(var2, var1), var0, var2, var1)));
   }

   public static io.reactivex.n a(Callable var0, io.reactivex.c.h var1) {
      return io.reactivex.g.a.a((io.reactivex.n)(new cl.e(var0, var1)));
   }

   public void a(io.reactivex.c.g var1) {
      cl.j var3;
      cl.j var4;
      do {
         var4 = (cl.j)this.b.get();
         if(var4 != null) {
            var3 = var4;
            if(!var4.isDisposed()) {
               break;
            }
         }

         var3 = new cl.j(this.c.a());
      } while(!this.b.compareAndSet(var4, var3));

      boolean var2;
      if(!var3.f.get() && var3.f.compareAndSet(false, true)) {
         var2 = true;
      } else {
         var2 = false;
      }

      try {
         var1.a(var3);
      } catch (Throwable var5) {
         if(var2) {
            var3.f.compareAndSet(true, false);
         }

         io.reactivex.exceptions.a.b(var5);
         throw io.reactivex.d.j.j.a(var5);
      }

      if(var2) {
         this.a.subscribe(var3);
      }

   }

   public void dispose() {
      this.b.lazySet((Object)null);
   }

   public boolean isDisposed() {
      io.reactivex.b.b var2 = (io.reactivex.b.b)this.b.get();
      boolean var1;
      if(var2 != null && !var2.isDisposed()) {
         var1 = false;
      } else {
         var1 = true;
      }

      return var1;
   }

   protected void subscribeActual(io.reactivex.t var1) {
      this.d.subscribe(var1);
   }

   abstract static class a extends AtomicReference implements cl.h {
      cl.f a;
      int b;

      a() {
         cl.f var1 = new cl.f((Object)null);
         this.a = var1;
         this.set(var1);
      }

      final void a() {
         cl.f var1 = (cl.f)((cl.f)this.get()).get();
         --this.b;
         this.b(var1);
      }

      public final void a(cl.d var1) {
         if(var1.getAndIncrement() == 0) {
            int var2 = 1;

            label28:
            do {
               cl.f var4 = (cl.f)var1.a();
               cl.f var3 = var4;
               if(var4 == null) {
                  var3 = this.e();
                  var1.c = var3;
               }

               while(!var1.isDisposed()) {
                  var4 = (cl.f)var3.get();
                  if(var4 == null) {
                     var1.c = var3;
                     var2 = var1.addAndGet(-var2);
                     continue label28;
                  }

                  if(io.reactivex.d.j.n.a(this.c(var4.a), var1.b)) {
                     var1.c = null;
                     return;
                  }

                  var3 = var4;
               }

               return;
            } while(var2 != 0);
         }

      }

      final void a(cl.f var1) {
         this.a.set(var1);
         this.a = var1;
         ++this.b;
      }

      public final void a(Object var1) {
         this.a(new cl.f(this.b(io.reactivex.d.j.n.a(var1))));
         this.c();
      }

      public final void a(Throwable var1) {
         this.a(new cl.f(this.b(io.reactivex.d.j.n.a(var1))));
         this.d();
      }

      Object b(Object var1) {
         return var1;
      }

      public final void b() {
         this.a(new cl.f(this.b(io.reactivex.d.j.n.a())));
         this.d();
      }

      final void b(cl.f var1) {
         this.set(var1);
      }

      Object c(Object var1) {
         return var1;
      }

      abstract void c();

      void d() {
      }

      cl.f e() {
         return (cl.f)this.get();
      }
   }

   interface b {
      cl.h a();
   }

   static final class c implements io.reactivex.c.g {
      private final eg a;

      c(eg var1) {
         this.a = var1;
      }

      public void a(io.reactivex.b.b var1) {
         this.a.a(var1);
      }
   }

   static final class d extends AtomicInteger implements io.reactivex.b.b {
      final cl.j a;
      final io.reactivex.t b;
      Object c;
      volatile boolean d;

      d(cl.j var1, io.reactivex.t var2) {
         this.a = var1;
         this.b = var2;
      }

      Object a() {
         return this.c;
      }

      public void dispose() {
         if(!this.d) {
            this.d = true;
            this.a.b(this);
         }

      }

      public boolean isDisposed() {
         return this.d;
      }
   }

   static final class e extends io.reactivex.n {
      private final Callable a;
      private final io.reactivex.c.h b;

      e(Callable var1, io.reactivex.c.h var2) {
         this.a = var1;
         this.b = var2;
      }

      protected void subscribeActual(io.reactivex.t var1) {
         io.reactivex.r var2;
         io.reactivex.e.a var3;
         try {
            var3 = (io.reactivex.e.a)io.reactivex.d.b.b.a(this.a.call(), "The connectableFactory returned a null ConnectableObservable");
            var2 = (io.reactivex.r)io.reactivex.d.b.b.a(this.b.a(var3), "The selector returned a null ObservableSource");
         } catch (Throwable var4) {
            io.reactivex.exceptions.a.b(var4);
            io.reactivex.d.a.e.a(var4, var1);
            return;
         }

         eg var5 = new eg(var1);
         var2.subscribe(var5);
         var3.a(new cl.c(var5));
      }
   }

   static final class f extends AtomicReference {
      final Object a;

      f(Object var1) {
         this.a = var1;
      }
   }

   static final class g extends io.reactivex.e.a {
      private final io.reactivex.e.a a;
      private final io.reactivex.n b;

      g(io.reactivex.e.a var1, io.reactivex.n var2) {
         this.a = var1;
         this.b = var2;
      }

      public void a(io.reactivex.c.g var1) {
         this.a.a(var1);
      }

      protected void subscribeActual(io.reactivex.t var1) {
         this.b.subscribe(var1);
      }
   }

   interface h {
      void a(cl.d var1);

      void a(Object var1);

      void a(Throwable var1);

      void b();
   }

   static final class i implements cl.b {
      private final int a;

      i(int var1) {
         this.a = var1;
      }

      public cl.h a() {
         return new cl.n(this.a);
      }
   }

   static final class j extends AtomicReference implements io.reactivex.b.b, io.reactivex.t {
      static final cl.d[] c = new cl.d[0];
      static final cl.d[] d = new cl.d[0];
      final cl.h a;
      boolean b;
      final AtomicReference e;
      final AtomicBoolean f;

      j(cl.h var1) {
         this.a = var1;
         this.e = new AtomicReference(c);
         this.f = new AtomicBoolean();
      }

      void a() {
         cl.d[] var4 = (cl.d[])this.e.get();
         int var2 = var4.length;

         for(int var1 = 0; var1 < var2; ++var1) {
            cl.d var3 = var4[var1];
            this.a.a(var3);
         }

      }

      boolean a(cl.d var1) {
         while(true) {
            cl.d[] var5 = (cl.d[])this.e.get();
            boolean var3;
            if(var5 == d) {
               var3 = false;
            } else {
               int var2 = var5.length;
               cl.d[] var4 = new cl.d[var2 + 1];
               System.arraycopy(var5, 0, var4, 0, var2);
               var4[var2] = var1;
               if(!this.e.compareAndSet(var5, var4)) {
                  continue;
               }

               var3 = true;
            }

            return var3;
         }
      }

      void b() {
         cl.d[] var3 = (cl.d[])this.e.getAndSet(d);
         int var2 = var3.length;

         for(int var1 = 0; var1 < var2; ++var1) {
            cl.d var4 = var3[var1];
            this.a.a(var4);
         }

      }

      void b(cl.d var1) {
         while(true) {
            cl.d[] var7 = (cl.d[])this.e.get();
            int var5 = var7.length;
            if(var5 != 0) {
               byte var4 = -1;
               int var2 = 0;

               int var3;
               while(true) {
                  var3 = var4;
                  if(var2 >= var5) {
                     break;
                  }

                  if(var7[var2].equals(var1)) {
                     var3 = var2;
                     break;
                  }

                  ++var2;
               }

               if(var3 >= 0) {
                  cl.d[] var6;
                  if(var5 == 1) {
                     var6 = c;
                  } else {
                     var6 = new cl.d[var5 - 1];
                     System.arraycopy(var7, 0, var6, 0, var3);
                     System.arraycopy(var7, var3 + 1, var6, var3, var5 - var3 - 1);
                  }

                  if(!this.e.compareAndSet(var7, var6)) {
                     continue;
                  }
               }
            }

            return;
         }
      }

      public void dispose() {
         this.e.set(d);
         io.reactivex.d.a.d.a((AtomicReference)this);
      }

      public boolean isDisposed() {
         boolean var1;
         if(this.e.get() == d) {
            var1 = true;
         } else {
            var1 = false;
         }

         return var1;
      }

      public void onComplete() {
         if(!this.b) {
            this.b = true;
            this.a.b();
            this.b();
         }

      }

      public void onError(Throwable var1) {
         if(!this.b) {
            this.b = true;
            this.a.a(var1);
            this.b();
         } else {
            io.reactivex.g.a.a(var1);
         }

      }

      public void onNext(Object var1) {
         if(!this.b) {
            this.a.a(var1);
            this.a();
         }

      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.b(this, var1)) {
            this.a();
         }

      }
   }

   static final class k implements io.reactivex.r {
      private final AtomicReference a;
      private final cl.b b;

      k(AtomicReference var1, cl.b var2) {
         this.a = var1;
         this.b = var2;
      }

      public void subscribe(io.reactivex.t var1) {
         while(true) {
            cl.j var3 = (cl.j)this.a.get();
            cl.j var2 = var3;
            if(var3 == null) {
               var2 = new cl.j(this.b.a());
               if(!this.a.compareAndSet((Object)null, var2)) {
                  continue;
               }
            }

            cl.d var4 = new cl.d(var2, var1);
            var1.onSubscribe(var4);
            var2.a(var4);
            if(var4.isDisposed()) {
               var2.b(var4);
            } else {
               var2.a.a(var4);
            }

            return;
         }
      }
   }

   static final class l implements cl.b {
      private final int a;
      private final long b;
      private final TimeUnit c;
      private final io.reactivex.u d;

      l(int var1, long var2, TimeUnit var4, io.reactivex.u var5) {
         this.a = var1;
         this.b = var2;
         this.c = var4;
         this.d = var5;
      }

      public cl.h a() {
         return new cl.m(this.a, this.b, this.c, this.d);
      }
   }

   static final class m extends cl.a {
      final io.reactivex.u c;
      final long d;
      final TimeUnit e;
      final int f;

      m(int var1, long var2, TimeUnit var4, io.reactivex.u var5) {
         this.c = var5;
         this.f = var1;
         this.d = var2;
         this.e = var4;
      }

      Object b(Object var1) {
         return new io.reactivex.h.b(var1, this.c.a(this.e), this.e);
      }

      Object c(Object var1) {
         return ((io.reactivex.h.b)var1).a();
      }

      void c() {
         long var4 = this.c.a(this.e);
         long var2 = this.d;
         cl.f var7 = (cl.f)this.get();
         cl.f var6 = (cl.f)var7.get();
         int var1 = 0;

         while(var6 != null) {
            cl.f var8;
            if(this.b > this.f) {
               ++var1;
               --this.b;
               var8 = (cl.f)var6.get();
               var7 = var6;
               var6 = var8;
            } else {
               if(((io.reactivex.h.b)var6.a).b() > var4 - var2) {
                  break;
               }

               ++var1;
               --this.b;
               var8 = (cl.f)var6.get();
               var7 = var6;
               var6 = var8;
            }
         }

         if(var1 != 0) {
            this.b(var7);
         }

      }

      void d() {
         long var2 = this.c.a(this.e);
         long var4 = this.d;
         cl.f var7 = (cl.f)this.get();
         cl.f var6 = (cl.f)var7.get();

         int var1;
         cl.f var8;
         for(var1 = 0; var6 != null && this.b > 1 && ((io.reactivex.h.b)var6.a).b() <= var2 - var4; var6 = var8) {
            ++var1;
            --this.b;
            var8 = (cl.f)var6.get();
            var7 = var6;
         }

         if(var1 != 0) {
            this.b(var7);
         }

      }

      cl.f e() {
         long var1 = this.c.a(this.e);
         long var3 = this.d;
         cl.f var6 = (cl.f)this.get();

         cl.f var8;
         for(cl.f var5 = (cl.f)var6.get(); var5 != null; var5 = var8) {
            io.reactivex.h.b var7 = (io.reactivex.h.b)var5.a;
            if(io.reactivex.d.j.n.b(var7.a()) || io.reactivex.d.j.n.c(var7.a()) || var7.b() > var1 - var3) {
               break;
            }

            var8 = (cl.f)var5.get();
            var6 = var5;
         }

         return var6;
      }
   }

   static final class n extends cl.a {
      final int c;

      n(int var1) {
         this.c = var1;
      }

      void c() {
         if(this.b > this.c) {
            this.a();
         }

      }
   }

   static final class o implements cl.b {
      public cl.h a() {
         return new cl.p(16);
      }
   }

   static final class p extends ArrayList implements cl.h {
      volatile int a;

      p(int var1) {
         super(var1);
      }

      public void a(cl.d var1) {
         if(var1.getAndIncrement() == 0) {
            io.reactivex.t var5 = var1.b;
            int var3 = 1;

            while(!var1.isDisposed()) {
               int var4 = this.a;
               Integer var6 = (Integer)var1.a();
               int var2;
               if(var6 != null) {
                  var2 = var6.intValue();
               } else {
                  var2 = 0;
               }

               while(var2 < var4) {
                  if(io.reactivex.d.j.n.a(this.get(var2), var5) || var1.isDisposed()) {
                     return;
                  }

                  ++var2;
               }

               var1.c = Integer.valueOf(var2);
               var3 = var1.addAndGet(-var3);
               if(var3 == 0) {
                  break;
               }
            }
         }

      }

      public void a(Object var1) {
         this.add(io.reactivex.d.j.n.a(var1));
         ++this.a;
      }

      public void a(Throwable var1) {
         this.add(io.reactivex.d.j.n.a(var1));
         ++this.a;
      }

      public void b() {
         this.add(io.reactivex.d.j.n.a());
         ++this.a;
      }
   }
}
