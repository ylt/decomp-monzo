package io.reactivex.d.e.d;

public final class am extends a {
   private final io.reactivex.c.g b;
   private final io.reactivex.c.a c;

   public am(io.reactivex.n var1, io.reactivex.c.g var2, io.reactivex.c.a var3) {
      super(var1);
      this.b = var2;
      this.c = var3;
   }

   protected void subscribeActual(io.reactivex.t var1) {
      this.a.subscribe(new io.reactivex.d.d.l(var1, this.b, this.c));
   }
}
