package io.reactivex.d.e.d;

import java.util.concurrent.Callable;

public final class ar extends io.reactivex.n {
   final Callable a;

   public ar(Callable var1) {
      this.a = var1;
   }

   public void subscribeActual(io.reactivex.t var1) {
      Throwable var2;
      try {
         var2 = (Throwable)io.reactivex.d.b.b.a(this.a.call(), "Callable returned null throwable. Null values are generally not allowed in 2.x operators and sources.");
      } catch (Throwable var3) {
         var2 = var3;
         io.reactivex.exceptions.a.b(var3);
      }

      io.reactivex.d.a.e.a(var2, var1);
   }
}
