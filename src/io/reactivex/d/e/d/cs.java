package io.reactivex.d.e.d;

public final class cs extends a {
   final io.reactivex.c.c b;

   public cs(io.reactivex.r var1, io.reactivex.c.c var2) {
      super(var1);
      this.b = var2;
   }

   public void subscribeActual(io.reactivex.t var1) {
      this.a.subscribe(new cs.a(var1, this.b));
   }

   static final class a implements io.reactivex.b.b, io.reactivex.t {
      final io.reactivex.t a;
      final io.reactivex.c.c b;
      io.reactivex.b.b c;
      Object d;
      boolean e;

      a(io.reactivex.t var1, io.reactivex.c.c var2) {
         this.a = var1;
         this.b = var2;
      }

      public void dispose() {
         this.c.dispose();
      }

      public boolean isDisposed() {
         return this.c.isDisposed();
      }

      public void onComplete() {
         if(!this.e) {
            this.e = true;
            this.a.onComplete();
         }

      }

      public void onError(Throwable var1) {
         if(this.e) {
            io.reactivex.g.a.a(var1);
         } else {
            this.e = true;
            this.a.onError(var1);
         }

      }

      public void onNext(Object var1) {
         if(!this.e) {
            io.reactivex.t var2 = this.a;
            Object var3 = this.d;
            if(var3 == null) {
               this.d = var1;
               var2.onNext(var1);
            } else {
               try {
                  var1 = io.reactivex.d.b.b.a(this.b.a(var3, var1), "The value returned by the accumulator is null");
               } catch (Throwable var4) {
                  io.reactivex.exceptions.a.b(var4);
                  this.c.dispose();
                  this.onError(var4);
                  return;
               }

               this.d = var1;
               var2.onNext(var1);
            }
         }

      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.c, var1)) {
            this.c = var1;
            this.a.onSubscribe(this);
         }

      }
   }
}
