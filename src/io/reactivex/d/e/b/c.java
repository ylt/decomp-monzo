package io.reactivex.d.e.b;

import io.reactivex.exceptions.MissingBackpressureException;
import java.util.concurrent.atomic.AtomicLong;

public final class c extends a {
   final int c;
   final boolean d;
   final boolean e;
   final io.reactivex.c.a f;

   public c(io.reactivex.f var1, int var2, boolean var3, boolean var4, io.reactivex.c.a var5) {
      super(var1);
      this.c = var2;
      this.d = var3;
      this.e = var4;
      this.f = var5;
   }

   protected void b(org.a.b var1) {
      this.b.a((io.reactivex.g)(new c.a(var1, this.c, this.d, this.e, this.f)));
   }

   static final class a extends io.reactivex.d.i.a implements io.reactivex.g {
      final org.a.b a;
      final io.reactivex.d.c.h b;
      final boolean c;
      final io.reactivex.c.a d;
      org.a.c e;
      volatile boolean f;
      volatile boolean g;
      Throwable h;
      final AtomicLong i = new AtomicLong();
      boolean j;

      a(org.a.b var1, int var2, boolean var3, boolean var4, io.reactivex.c.a var5) {
         this.a = var1;
         this.d = var5;
         this.c = var4;
         Object var6;
         if(var3) {
            var6 = new io.reactivex.d.f.c(var2);
         } else {
            var6 = new io.reactivex.d.f.b(var2);
         }

         this.b = (io.reactivex.d.c.h)var6;
      }

      public int a(int var1) {
         byte var2;
         if((var1 & 2) != 0) {
            this.j = true;
            var2 = 2;
         } else {
            var2 = 0;
         }

         return var2;
      }

      public void a() {
         if(!this.f) {
            this.f = true;
            this.e.a();
            if(this.getAndIncrement() == 0) {
               this.b.c();
            }
         }

      }

      public void a(long var1) {
         if(!this.j && io.reactivex.d.i.d.b(var1)) {
            io.reactivex.d.j.d.a(this.i, var1);
            this.d();
         }

      }

      public void a(org.a.c var1) {
         if(io.reactivex.d.i.d.a(this.e, var1)) {
            this.e = var1;
            this.a.a(this);
            var1.a(Long.MAX_VALUE);
         }

      }

      boolean a(boolean var1, boolean var2, org.a.b var3) {
         boolean var4 = true;
         if(this.f) {
            this.b.c();
            var1 = var4;
         } else {
            if(var1) {
               Throwable var5;
               if(this.c) {
                  if(var2) {
                     var5 = this.h;
                     if(var5 != null) {
                        var3.onError(var5);
                        var1 = var4;
                     } else {
                        var3.onComplete();
                        var1 = var4;
                     }

                     return var1;
                  }
               } else {
                  var5 = this.h;
                  if(var5 != null) {
                     this.b.c();
                     var3.onError(var5);
                     var1 = var4;
                     return var1;
                  }

                  if(var2) {
                     var3.onComplete();
                     var1 = var4;
                     return var1;
                  }
               }
            }

            var1 = false;
         }

         return var1;
      }

      public boolean b() {
         return this.b.b();
      }

      public void c() {
         this.b.c();
      }

      void d() {
         if(this.getAndIncrement() == 0) {
            io.reactivex.d.c.h var11 = this.b;
            org.a.b var9 = this.a;
            int var1 = 1;

            while(!this.a(this.g, var11.b(), var9)) {
               long var5 = this.i.get();

               long var3;
               for(var3 = 0L; var3 != var5; ++var3) {
                  boolean var8 = this.g;
                  Object var10 = var11.n_();
                  boolean var7;
                  if(var10 == null) {
                     var7 = true;
                  } else {
                     var7 = false;
                  }

                  if(this.a(var8, var7, var9)) {
                     return;
                  }

                  if(var7) {
                     break;
                  }

                  var9.onNext(var10);
               }

               if(var3 == var5 && this.a(this.g, var11.b(), var9)) {
                  break;
               }

               if(var3 != 0L && var5 != Long.MAX_VALUE) {
                  this.i.addAndGet(-var3);
               }

               int var2 = this.addAndGet(-var1);
               var1 = var2;
               if(var2 == 0) {
                  break;
               }
            }
         }

      }

      public Object n_() throws Exception {
         return this.b.n_();
      }

      public void onComplete() {
         this.g = true;
         if(this.j) {
            this.a.onComplete();
         } else {
            this.d();
         }

      }

      public void onError(Throwable var1) {
         this.h = var1;
         this.g = true;
         if(this.j) {
            this.a.onError(var1);
         } else {
            this.d();
         }

      }

      public void onNext(Object var1) {
         if(!this.b.a(var1)) {
            this.e.a();
            MissingBackpressureException var2 = new MissingBackpressureException("Buffer is full");

            try {
               this.d.a();
            } catch (Throwable var3) {
               io.reactivex.exceptions.a.b(var3);
               var2.initCause(var3);
            }

            this.onError(var2);
         } else if(this.j) {
            this.a.onNext((Object)null);
         } else {
            this.d();
         }

      }
   }
}
