package io.reactivex.d.e.b;

import io.reactivex.exceptions.CompositeException;
import java.util.concurrent.atomic.AtomicInteger;

public final class g extends a {
   final io.reactivex.c.d c;

   public g(io.reactivex.f var1, io.reactivex.c.d var2) {
      super(var1);
      this.c = var2;
   }

   public void b(org.a.b var1) {
      io.reactivex.d.i.c var2 = new io.reactivex.d.i.c();
      var1.a(var2);
      (new g.a(var1, this.c, var2, this.b)).a();
   }

   static final class a extends AtomicInteger implements io.reactivex.g {
      final org.a.b a;
      final io.reactivex.d.i.c b;
      final org.a.a c;
      final io.reactivex.c.d d;
      int e;

      a(org.a.b var1, io.reactivex.c.d var2, io.reactivex.d.i.c var3, org.a.a var4) {
         this.a = var1;
         this.b = var3;
         this.c = var4;
         this.d = var2;
      }

      void a() {
         if(this.getAndIncrement() == 0) {
            int var1 = 1;

            while(!this.b.d()) {
               this.c.a(this);
               int var2 = this.addAndGet(-var1);
               var1 = var2;
               if(var2 == 0) {
                  break;
               }
            }
         }

      }

      public void a(org.a.c var1) {
         this.b.a(var1);
      }

      public void onComplete() {
         this.a.onComplete();
      }

      public void onError(Throwable var1) {
         boolean var3;
         try {
            io.reactivex.c.d var4 = this.d;
            int var2 = this.e + 1;
            this.e = var2;
            var3 = var4.a(Integer.valueOf(var2), var1);
         } catch (Throwable var5) {
            io.reactivex.exceptions.a.b(var5);
            this.a.onError(new CompositeException(new Throwable[]{var1, var5}));
            return;
         }

         if(!var3) {
            this.a.onError(var1);
         } else {
            this.a();
         }

      }

      public void onNext(Object var1) {
         this.a.onNext(var1);
         this.b.b(1L);
      }
   }
}
