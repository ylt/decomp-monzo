package io.reactivex.d.e.b;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

public final class f extends a {
   public f(io.reactivex.f var1) {
      super(var1);
   }

   protected void b(org.a.b var1) {
      this.b.a((io.reactivex.g)(new f.a(var1)));
   }

   static final class a extends AtomicInteger implements io.reactivex.g, org.a.c {
      final org.a.b a;
      org.a.c b;
      volatile boolean c;
      Throwable d;
      volatile boolean e;
      final AtomicLong f = new AtomicLong();
      final AtomicReference g = new AtomicReference();

      a(org.a.b var1) {
         this.a = var1;
      }

      public void a() {
         if(!this.e) {
            this.e = true;
            this.b.a();
            if(this.getAndIncrement() == 0) {
               this.g.lazySet((Object)null);
            }
         }

      }

      public void a(long var1) {
         if(io.reactivex.d.i.d.b(var1)) {
            io.reactivex.d.j.d.a(this.f, var1);
            this.b();
         }

      }

      public void a(org.a.c var1) {
         if(io.reactivex.d.i.d.a(this.b, var1)) {
            this.b = var1;
            this.a.a(this);
            var1.a(Long.MAX_VALUE);
         }

      }

      boolean a(boolean var1, boolean var2, org.a.b var3, AtomicReference var4) {
         boolean var5 = true;
         if(this.e) {
            var4.lazySet((Object)null);
            var1 = var5;
         } else {
            if(var1) {
               Throwable var6 = this.d;
               if(var6 != null) {
                  var4.lazySet((Object)null);
                  var3.onError(var6);
                  var1 = var5;
                  return var1;
               }

               if(var2) {
                  var3.onComplete();
                  var1 = var5;
                  return var1;
               }
            }

            var1 = false;
         }

         return var1;
      }

      void b() {
         if(this.getAndIncrement() == 0) {
            org.a.b var7 = this.a;
            AtomicLong var9 = this.f;
            AtomicReference var8 = this.g;
            int var1 = 1;

            int var2;
            do {
               boolean var3;
               boolean var4;
               long var5;
               for(var5 = 0L; var5 != var9.get(); ++var5) {
                  var4 = this.c;
                  Object var10 = var8.getAndSet((Object)null);
                  if(var10 == null) {
                     var3 = true;
                  } else {
                     var3 = false;
                  }

                  if(this.a(var4, var3, var7, var8)) {
                     return;
                  }

                  if(var3) {
                     break;
                  }

                  var7.onNext(var10);
               }

               if(var5 == var9.get()) {
                  var4 = this.c;
                  if(var8.get() == null) {
                     var3 = true;
                  } else {
                     var3 = false;
                  }

                  if(this.a(var4, var3, var7, var8)) {
                     break;
                  }
               }

               if(var5 != 0L) {
                  io.reactivex.d.j.d.b(var9, var5);
               }

               var2 = this.addAndGet(-var1);
               var1 = var2;
            } while(var2 != 0);
         }

      }

      public void onComplete() {
         this.c = true;
         this.b();
      }

      public void onError(Throwable var1) {
         this.d = var1;
         this.c = true;
         this.b();
      }

      public void onNext(Object var1) {
         this.g.lazySet(var1);
         this.b();
      }
   }
}
