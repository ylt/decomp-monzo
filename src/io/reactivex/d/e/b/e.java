package io.reactivex.d.e.b;

import io.reactivex.exceptions.MissingBackpressureException;
import java.util.concurrent.atomic.AtomicLong;

public final class e extends a {
   public e(io.reactivex.f var1) {
      super(var1);
   }

   protected void b(org.a.b var1) {
      this.b.a((io.reactivex.g)(new e.a(var1)));
   }

   static final class a extends AtomicLong implements io.reactivex.g, org.a.c {
      final org.a.b a;
      org.a.c b;
      boolean c;

      a(org.a.b var1) {
         this.a = var1;
      }

      public void a() {
         this.b.a();
      }

      public void a(long var1) {
         if(io.reactivex.d.i.d.b(var1)) {
            io.reactivex.d.j.d.a(this, var1);
         }

      }

      public void a(org.a.c var1) {
         if(io.reactivex.d.i.d.a(this.b, var1)) {
            this.b = var1;
            this.a.a(this);
            var1.a(Long.MAX_VALUE);
         }

      }

      public void onComplete() {
         if(!this.c) {
            this.c = true;
            this.a.onComplete();
         }

      }

      public void onError(Throwable var1) {
         if(this.c) {
            io.reactivex.g.a.a(var1);
         } else {
            this.c = true;
            this.a.onError(var1);
         }

      }

      public void onNext(Object var1) {
         if(!this.c) {
            if(this.get() != 0L) {
               this.a.onNext(var1);
               io.reactivex.d.j.d.b(this, 1L);
            } else {
               this.onError(new MissingBackpressureException("could not emit value due to lack of requests"));
            }
         }

      }
   }
}
