package io.reactivex.d.e.b;

import java.util.concurrent.atomic.AtomicLong;

public final class d extends a implements io.reactivex.c.g {
   final io.reactivex.c.g c = this;

   public d(io.reactivex.f var1) {
      super(var1);
   }

   public void a(Object var1) {
   }

   protected void b(org.a.b var1) {
      this.b.a((io.reactivex.g)(new d.a(var1, this.c)));
   }

   static final class a extends AtomicLong implements io.reactivex.g, org.a.c {
      final org.a.b a;
      final io.reactivex.c.g b;
      org.a.c c;
      boolean d;

      a(org.a.b var1, io.reactivex.c.g var2) {
         this.a = var1;
         this.b = var2;
      }

      public void a() {
         this.c.a();
      }

      public void a(long var1) {
         if(io.reactivex.d.i.d.b(var1)) {
            io.reactivex.d.j.d.a(this, var1);
         }

      }

      public void a(org.a.c var1) {
         if(io.reactivex.d.i.d.a(this.c, var1)) {
            this.c = var1;
            this.a.a(this);
            var1.a(Long.MAX_VALUE);
         }

      }

      public void onComplete() {
         if(!this.d) {
            this.d = true;
            this.a.onComplete();
         }

      }

      public void onError(Throwable var1) {
         if(this.d) {
            io.reactivex.g.a.a(var1);
         } else {
            this.d = true;
            this.a.onError(var1);
         }

      }

      public void onNext(Object var1) {
         if(!this.d) {
            if(this.get() != 0L) {
               this.a.onNext(var1);
               io.reactivex.d.j.d.b(this, 1L);
            } else {
               try {
                  this.b.a(var1);
               } catch (Throwable var2) {
                  io.reactivex.exceptions.a.b(var2);
                  this.a();
                  this.onError(var2);
               }
            }
         }

      }
   }
}
