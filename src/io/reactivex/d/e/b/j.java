package io.reactivex.d.e.b;

import io.reactivex.v;
import io.reactivex.x;
import java.util.NoSuchElementException;

public final class j extends v implements io.reactivex.d.c.a {
   final io.reactivex.f a;
   final Object b;

   public j(io.reactivex.f var1, Object var2) {
      this.a = var1;
      this.b = var2;
   }

   public io.reactivex.f a() {
      return io.reactivex.g.a.a((io.reactivex.f)(new i(this.a, this.b)));
   }

   protected void b(x var1) {
      this.a.a((io.reactivex.g)(new j.a(var1, this.b)));
   }

   static final class a implements io.reactivex.b.b, io.reactivex.g {
      final x a;
      final Object b;
      org.a.c c;
      boolean d;
      Object e;

      a(x var1, Object var2) {
         this.a = var1;
         this.b = var2;
      }

      public void a(org.a.c var1) {
         if(io.reactivex.d.i.d.a(this.c, var1)) {
            this.c = var1;
            this.a.onSubscribe(this);
            var1.a(Long.MAX_VALUE);
         }

      }

      public void dispose() {
         this.c.a();
         this.c = io.reactivex.d.i.d.a;
      }

      public boolean isDisposed() {
         boolean var1;
         if(this.c == io.reactivex.d.i.d.a) {
            var1 = true;
         } else {
            var1 = false;
         }

         return var1;
      }

      public void onComplete() {
         if(!this.d) {
            this.d = true;
            this.c = io.reactivex.d.i.d.a;
            Object var2 = this.e;
            this.e = null;
            Object var1 = var2;
            if(var2 == null) {
               var1 = this.b;
            }

            if(var1 != null) {
               this.a.a_(var1);
            } else {
               this.a.onError(new NoSuchElementException());
            }
         }

      }

      public void onError(Throwable var1) {
         if(this.d) {
            io.reactivex.g.a.a(var1);
         } else {
            this.d = true;
            this.c = io.reactivex.d.i.d.a;
            this.a.onError(var1);
         }

      }

      public void onNext(Object var1) {
         if(!this.d) {
            if(this.e != null) {
               this.d = true;
               this.c.a();
               this.c = io.reactivex.d.i.d.a;
               this.a.onError(new IllegalArgumentException("Sequence contains more than one element!"));
            } else {
               this.e = var1;
            }
         }

      }
   }
}
