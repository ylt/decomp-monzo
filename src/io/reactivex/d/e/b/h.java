package io.reactivex.d.e.b;

import io.reactivex.c.q;
import io.reactivex.exceptions.CompositeException;
import java.util.concurrent.atomic.AtomicInteger;

public final class h extends a {
   final q c;
   final long d;

   public h(io.reactivex.f var1, long var2, q var4) {
      super(var1);
      this.c = var4;
      this.d = var2;
   }

   public void b(org.a.b var1) {
      io.reactivex.d.i.c var2 = new io.reactivex.d.i.c();
      var1.a(var2);
      (new h.a(var1, this.d, this.c, var2, this.b)).a();
   }

   static final class a extends AtomicInteger implements io.reactivex.g {
      final org.a.b a;
      final io.reactivex.d.i.c b;
      final org.a.a c;
      final q d;
      long e;

      a(org.a.b var1, long var2, q var4, io.reactivex.d.i.c var5, org.a.a var6) {
         this.a = var1;
         this.b = var5;
         this.c = var6;
         this.d = var4;
         this.e = var2;
      }

      void a() {
         if(this.getAndIncrement() == 0) {
            int var1 = 1;

            while(!this.b.d()) {
               this.c.a(this);
               int var2 = this.addAndGet(-var1);
               var1 = var2;
               if(var2 == 0) {
                  break;
               }
            }
         }

      }

      public void a(org.a.c var1) {
         this.b.a(var1);
      }

      public void onComplete() {
         this.a.onComplete();
      }

      public void onError(Throwable var1) {
         long var2 = this.e;
         if(var2 != Long.MAX_VALUE) {
            this.e = var2 - 1L;
         }

         if(var2 == 0L) {
            this.a.onError(var1);
         } else {
            boolean var4;
            try {
               var4 = this.d.a(var1);
            } catch (Throwable var6) {
               io.reactivex.exceptions.a.b(var6);
               this.a.onError(new CompositeException(new Throwable[]{var1, var6}));
               return;
            }

            if(!var4) {
               this.a.onError(var1);
            } else {
               this.a();
            }
         }

      }

      public void onNext(Object var1) {
         this.a.onNext(var1);
         this.b.b(1L);
      }
   }
}
