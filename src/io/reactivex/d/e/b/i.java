package io.reactivex.d.e.b;

public final class i extends a {
   final Object c;

   public i(io.reactivex.f var1, Object var2) {
      super(var1);
      this.c = var2;
   }

   protected void b(org.a.b var1) {
      this.b.a((io.reactivex.g)(new i.a(var1, this.c)));
   }

   static final class a extends io.reactivex.d.i.b implements io.reactivex.g {
      final Object a;
      org.a.c b;
      boolean c;

      a(org.a.b var1, Object var2) {
         super(var1);
         this.a = var2;
      }

      public void a() {
         super.a();
         this.b.a();
      }

      public void a(org.a.c var1) {
         if(io.reactivex.d.i.d.a(this.b, var1)) {
            this.b = var1;
            this.d.a(this);
            var1.a(Long.MAX_VALUE);
         }

      }

      public void onComplete() {
         if(!this.c) {
            this.c = true;
            Object var2 = this.e;
            this.e = null;
            Object var1 = var2;
            if(var2 == null) {
               var1 = this.a;
            }

            if(var1 == null) {
               this.d.onComplete();
            } else {
               this.b(var1);
            }
         }

      }

      public void onError(Throwable var1) {
         if(this.c) {
            io.reactivex.g.a.a(var1);
         } else {
            this.c = true;
            this.d.onError(var1);
         }

      }

      public void onNext(Object var1) {
         if(!this.c) {
            if(this.e != null) {
               this.c = true;
               this.b.a();
               this.d.onError(new IllegalArgumentException("Sequence contains more than one element!"));
            } else {
               this.e = var1;
            }
         }

      }
   }
}
