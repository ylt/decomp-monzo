package io.reactivex.d.e.b;

import io.reactivex.n;
import io.reactivex.t;

public final class b extends io.reactivex.f {
   private final n b;

   public b(n var1) {
      this.b = var1;
   }

   protected void b(org.a.b var1) {
      this.b.subscribe((t)(new b.a(var1)));
   }

   static class a implements t, org.a.c {
      private final org.a.b a;
      private io.reactivex.b.b b;

      a(org.a.b var1) {
         this.a = var1;
      }

      public void a() {
         this.b.dispose();
      }

      public void a(long var1) {
      }

      public void onComplete() {
         this.a.onComplete();
      }

      public void onError(Throwable var1) {
         this.a.onError(var1);
      }

      public void onNext(Object var1) {
         this.a.onNext(var1);
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         this.b = var1;
         this.a.a(this);
      }
   }
}
