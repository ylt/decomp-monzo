package io.reactivex.d.e.e;

import io.reactivex.z;
import java.util.concurrent.atomic.AtomicReference;

public final class c extends io.reactivex.v {
   final z a;
   final io.reactivex.d b;

   public c(z var1, io.reactivex.d var2) {
      this.a = var1;
      this.b = var2;
   }

   protected void b(io.reactivex.x var1) {
      this.b.a(new c.a(var1, this.a));
   }

   static final class a extends AtomicReference implements io.reactivex.b.b, io.reactivex.c {
      final io.reactivex.x a;
      final z b;

      a(io.reactivex.x var1, z var2) {
         this.a = var1;
         this.b = var2;
      }

      public void dispose() {
         io.reactivex.d.a.d.a((AtomicReference)this);
      }

      public boolean isDisposed() {
         return io.reactivex.d.a.d.a((io.reactivex.b.b)this.get());
      }

      public void onComplete() {
         this.b.a(new io.reactivex.d.d.x(this, this.a));
      }

      public void onError(Throwable var1) {
         this.a.onError(var1);
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.b(this, var1)) {
            this.a.onSubscribe(this);
         }

      }
   }
}
