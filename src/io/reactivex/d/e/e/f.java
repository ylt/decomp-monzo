package io.reactivex.d.e.e;

import io.reactivex.z;
import io.reactivex.exceptions.CompositeException;

public final class f extends io.reactivex.v {
   final z a;
   final io.reactivex.c.g b;

   public f(z var1, io.reactivex.c.g var2) {
      this.a = var1;
      this.b = var2;
   }

   protected void b(io.reactivex.x var1) {
      this.a.a(new f.a(var1));
   }

   final class a implements io.reactivex.x {
      private final io.reactivex.x b;

      a(io.reactivex.x var2) {
         this.b = var2;
      }

      public void a_(Object var1) {
         this.b.a_(var1);
      }

      public void onError(Throwable var1) {
         try {
            f.this.b.a(var1);
         } catch (Throwable var3) {
            io.reactivex.exceptions.a.b(var3);
            var1 = new CompositeException(new Throwable[]{(Throwable)var1, var3});
         }

         this.b.onError((Throwable)var1);
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         this.b.onSubscribe(var1);
      }
   }
}
