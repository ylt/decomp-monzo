package io.reactivex.d.e.e;

import io.reactivex.z;
import io.reactivex.exceptions.CompositeException;
import java.util.concurrent.atomic.AtomicReference;

public final class s extends io.reactivex.v {
   final z a;
   final io.reactivex.c.h b;

   public s(z var1, io.reactivex.c.h var2) {
      this.a = var1;
      this.b = var2;
   }

   protected void b(io.reactivex.x var1) {
      this.a.a(new s.a(var1, this.b));
   }

   static final class a extends AtomicReference implements io.reactivex.b.b, io.reactivex.x {
      final io.reactivex.x a;
      final io.reactivex.c.h b;

      a(io.reactivex.x var1, io.reactivex.c.h var2) {
         this.a = var1;
         this.b = var2;
      }

      public void a_(Object var1) {
         this.a.a_(var1);
      }

      public void dispose() {
         io.reactivex.d.a.d.a((AtomicReference)this);
      }

      public boolean isDisposed() {
         return io.reactivex.d.a.d.a((io.reactivex.b.b)this.get());
      }

      public void onError(Throwable var1) {
         z var2;
         try {
            var2 = (z)io.reactivex.d.b.b.a(this.b.a(var1), "The nextFunction returned a null SingleSource.");
         } catch (Throwable var3) {
            io.reactivex.exceptions.a.b(var3);
            this.a.onError(new CompositeException(new Throwable[]{var1, var3}));
            return;
         }

         var2.a(new io.reactivex.d.d.x(this, this.a));
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.b(this, var1)) {
            this.a.onSubscribe(this);
         }

      }
   }
}
