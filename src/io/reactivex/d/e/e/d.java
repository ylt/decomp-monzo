package io.reactivex.d.e.e;

import io.reactivex.z;

public final class d extends io.reactivex.v {
   final z a;
   final io.reactivex.c.g b;

   public d(z var1, io.reactivex.c.g var2) {
      this.a = var1;
      this.b = var2;
   }

   protected void b(io.reactivex.x var1) {
      this.a.a(new d.a(var1, this.b));
   }

   static final class a implements io.reactivex.b.b, io.reactivex.x {
      final io.reactivex.x a;
      final io.reactivex.c.g b;
      io.reactivex.b.b c;

      a(io.reactivex.x var1, io.reactivex.c.g var2) {
         this.a = var1;
         this.b = var2;
      }

      public void a_(Object var1) {
         this.a.a_(var1);

         try {
            this.b.a(var1);
         } catch (Throwable var2) {
            io.reactivex.exceptions.a.b(var2);
            io.reactivex.g.a.a(var2);
         }

      }

      public void dispose() {
         this.c.dispose();
      }

      public boolean isDisposed() {
         return this.c.isDisposed();
      }

      public void onError(Throwable var1) {
         this.a.onError(var1);
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.c, var1)) {
            this.c = var1;
            this.a.onSubscribe(this);
         }

      }
   }
}
