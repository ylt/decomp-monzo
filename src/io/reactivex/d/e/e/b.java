package io.reactivex.d.e.e;

import io.reactivex.z;
import java.util.concurrent.Callable;

public final class b extends io.reactivex.v {
   final Callable a;

   public b(Callable var1) {
      this.a = var1;
   }

   protected void b(io.reactivex.x var1) {
      z var2;
      try {
         var2 = (z)io.reactivex.d.b.b.a(this.a.call(), "The singleSupplier returned a null SingleSource");
      } catch (Throwable var3) {
         io.reactivex.exceptions.a.b(var3);
         io.reactivex.d.a.e.a(var3, var1);
         return;
      }

      var2.a(var1);
   }
}
