package io.reactivex.d.e.e;

import io.reactivex.z;
import java.util.concurrent.atomic.AtomicReference;

public final class t extends io.reactivex.v {
   final z a;
   final io.reactivex.u b;

   public t(z var1, io.reactivex.u var2) {
      this.a = var1;
      this.b = var2;
   }

   protected void b(io.reactivex.x var1) {
      t.a var2 = new t.a(var1, this.a);
      var1.onSubscribe(var2);
      io.reactivex.b.b var3 = this.b.a((Runnable)var2);
      var2.b.b(var3);
   }

   static final class a extends AtomicReference implements io.reactivex.b.b, io.reactivex.x, Runnable {
      final io.reactivex.x a;
      final io.reactivex.d.a.k b;
      final z c;

      a(io.reactivex.x var1, z var2) {
         this.a = var1;
         this.c = var2;
         this.b = new io.reactivex.d.a.k();
      }

      public void a_(Object var1) {
         this.a.a_(var1);
      }

      public void dispose() {
         io.reactivex.d.a.d.a((AtomicReference)this);
         this.b.dispose();
      }

      public boolean isDisposed() {
         return io.reactivex.d.a.d.a((io.reactivex.b.b)this.get());
      }

      public void onError(Throwable var1) {
         this.a.onError(var1);
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         io.reactivex.d.a.d.b(this, var1);
      }

      public void run() {
         this.c.a(this);
      }
   }
}
