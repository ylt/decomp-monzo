package io.reactivex.d.e.e;

import io.reactivex.z;

public final class i extends io.reactivex.v {
   final z a;
   final io.reactivex.c.g b;

   public i(z var1, io.reactivex.c.g var2) {
      this.a = var1;
      this.b = var2;
   }

   protected void b(io.reactivex.x var1) {
      this.a.a(new i.a(var1));
   }

   final class a implements io.reactivex.x {
      private final io.reactivex.x b;

      a(io.reactivex.x var2) {
         this.b = var2;
      }

      public void a_(Object var1) {
         try {
            i.this.b.a(var1);
         } catch (Throwable var2) {
            io.reactivex.exceptions.a.b(var2);
            this.b.onError(var2);
            return;
         }

         this.b.a_(var1);
      }

      public void onError(Throwable var1) {
         this.b.onError(var1);
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         this.b.onSubscribe(var1);
      }
   }
}
