package io.reactivex.d.e.e;

import io.reactivex.z;
import java.util.concurrent.atomic.AtomicInteger;

public final class e extends io.reactivex.v {
   final z a;
   final io.reactivex.c.a b;

   public e(z var1, io.reactivex.c.a var2) {
      this.a = var1;
      this.b = var2;
   }

   protected void b(io.reactivex.x var1) {
      this.a.a(new e.a(var1, this.b));
   }

   static final class a extends AtomicInteger implements io.reactivex.b.b, io.reactivex.x {
      final io.reactivex.x a;
      final io.reactivex.c.a b;
      io.reactivex.b.b c;

      a(io.reactivex.x var1, io.reactivex.c.a var2) {
         this.a = var1;
         this.b = var2;
      }

      void a() {
         if(this.compareAndSet(0, 1)) {
            try {
               this.b.a();
            } catch (Throwable var2) {
               io.reactivex.exceptions.a.b(var2);
               io.reactivex.g.a.a(var2);
            }
         }

      }

      public void a_(Object var1) {
         this.a.a_(var1);
         this.a();
      }

      public void dispose() {
         this.c.dispose();
         this.a();
      }

      public boolean isDisposed() {
         return this.c.isDisposed();
      }

      public void onError(Throwable var1) {
         this.a.onError(var1);
         this.a();
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.c, var1)) {
            this.c = var1;
            this.a.onSubscribe(this);
         }

      }
   }
}
