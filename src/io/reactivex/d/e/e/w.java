package io.reactivex.d.e.e;

import io.reactivex.z;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public final class w extends io.reactivex.v {
   final z[] a;
   final io.reactivex.c.h b;

   public w(z[] var1, io.reactivex.c.h var2) {
      this.a = var1;
      this.b = var2;
   }

   protected void b(io.reactivex.x var1) {
      int var2 = 0;
      z[] var4 = this.a;
      int var3 = var4.length;
      if(var3 == 1) {
         var4[0].a(new o.a(var1, new w.a()));
      } else {
         w.b var5 = new w.b(var1, var3, this.b);
         var1.onSubscribe(var5);

         while(var2 < var3 && !var5.isDisposed()) {
            z var6 = var4[var2];
            if(var6 == null) {
               var5.a((Throwable)(new NullPointerException("One of the sources is null")), var2);
               break;
            }

            var6.a(var5.c[var2]);
            ++var2;
         }
      }

   }

   final class a implements io.reactivex.c.h {
      public Object a(Object var1) throws Exception {
         return io.reactivex.d.b.b.a(w.this.b.a(new Object[]{var1}), "The zipper returned a null value");
      }
   }

   static final class b extends AtomicInteger implements io.reactivex.b.b {
      final io.reactivex.x a;
      final io.reactivex.c.h b;
      final w.c[] c;
      final Object[] d;

      b(io.reactivex.x var1, int var2, io.reactivex.c.h var3) {
         super(var2);
         this.a = var1;
         this.b = var3;
         w.c[] var5 = new w.c[var2];

         for(int var4 = 0; var4 < var2; ++var4) {
            var5[var4] = new w.c(this, var4);
         }

         this.c = var5;
         this.d = new Object[var2];
      }

      void a(int var1) {
         w.c[] var4 = this.c;
         int var3 = var4.length;

         for(int var2 = 0; var2 < var1; ++var2) {
            var4[var2].a();
         }

         ++var1;

         while(var1 < var3) {
            var4[var1].a();
            ++var1;
         }

      }

      void a(Object var1, int var2) {
         this.d[var2] = var1;
         if(this.decrementAndGet() == 0) {
            try {
               var1 = io.reactivex.d.b.b.a(this.b.a(this.d), "The zipper returned a null value");
            } catch (Throwable var3) {
               io.reactivex.exceptions.a.b(var3);
               this.a.onError(var3);
               return;
            }

            this.a.a_(var1);
         }

      }

      void a(Throwable var1, int var2) {
         if(this.getAndSet(0) > 0) {
            this.a(var2);
            this.a.onError(var1);
         } else {
            io.reactivex.g.a.a(var1);
         }

      }

      public void dispose() {
         int var1 = 0;
         if(this.getAndSet(0) > 0) {
            w.c[] var3 = this.c;

            for(int var2 = var3.length; var1 < var2; ++var1) {
               var3[var1].a();
            }
         }

      }

      public boolean isDisposed() {
         boolean var1;
         if(this.get() <= 0) {
            var1 = true;
         } else {
            var1 = false;
         }

         return var1;
      }
   }

   static final class c extends AtomicReference implements io.reactivex.x {
      final w.b a;
      final int b;

      c(w.b var1, int var2) {
         this.a = var1;
         this.b = var2;
      }

      public void a() {
         io.reactivex.d.a.d.a((AtomicReference)this);
      }

      public void a_(Object var1) {
         this.a.a(var1, this.b);
      }

      public void onError(Throwable var1) {
         this.a.a(var1, this.b);
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         io.reactivex.d.a.d.b(this, var1);
      }
   }
}
