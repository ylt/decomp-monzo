package io.reactivex.d.e.e;

import io.reactivex.z;

public final class v extends io.reactivex.n {
   final z a;

   public v(z var1) {
      this.a = var1;
   }

   public void subscribeActual(io.reactivex.t var1) {
      this.a.a(new v.a(var1));
   }

   static final class a implements io.reactivex.b.b, io.reactivex.x {
      final io.reactivex.t a;
      io.reactivex.b.b b;

      a(io.reactivex.t var1) {
         this.a = var1;
      }

      public void a_(Object var1) {
         this.a.onNext(var1);
         this.a.onComplete();
      }

      public void dispose() {
         this.b.dispose();
      }

      public boolean isDisposed() {
         return this.b.isDisposed();
      }

      public void onError(Throwable var1) {
         this.a.onError(var1);
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.b, var1)) {
            this.b = var1;
            this.a.onSubscribe(this);
         }

      }
   }
}
