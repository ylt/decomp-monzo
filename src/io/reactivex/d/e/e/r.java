package io.reactivex.d.e.e;

import io.reactivex.z;
import io.reactivex.exceptions.CompositeException;

public final class r extends io.reactivex.v {
   final z a;
   final io.reactivex.c.h b;
   final Object c;

   public r(z var1, io.reactivex.c.h var2, Object var3) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
   }

   protected void b(io.reactivex.x var1) {
      this.a.a(new r.a(var1));
   }

   final class a implements io.reactivex.x {
      private final io.reactivex.x b;

      a(io.reactivex.x var2) {
         this.b = var2;
      }

      public void a_(Object var1) {
         this.b.a_(var1);
      }

      public void onError(Throwable var1) {
         Object var2;
         if(r.this.b != null) {
            try {
               var2 = r.this.b.a(var1);
            } catch (Throwable var3) {
               io.reactivex.exceptions.a.b(var3);
               this.b.onError(new CompositeException(new Throwable[]{var1, var3}));
               return;
            }
         } else {
            var2 = r.this.c;
         }

         if(var2 == null) {
            NullPointerException var4 = new NullPointerException("Value supplied was null");
            var4.initCause(var1);
            this.b.onError(var4);
         } else {
            this.b.a_(var2);
         }

      }

      public void onSubscribe(io.reactivex.b.b var1) {
         this.b.onSubscribe(var1);
      }
   }
}
