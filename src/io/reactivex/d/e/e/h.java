package io.reactivex.d.e.e;

import io.reactivex.z;

public final class h extends io.reactivex.v {
   final z a;
   final io.reactivex.c.g b;

   public h(z var1, io.reactivex.c.g var2) {
      this.a = var1;
      this.b = var2;
   }

   protected void b(io.reactivex.x var1) {
      this.a.a(new h.a(var1, this.b));
   }

   static final class a implements io.reactivex.x {
      final io.reactivex.x a;
      final io.reactivex.c.g b;
      boolean c;

      a(io.reactivex.x var1, io.reactivex.c.g var2) {
         this.a = var1;
         this.b = var2;
      }

      public void a_(Object var1) {
         if(!this.c) {
            this.a.a_(var1);
         }

      }

      public void onError(Throwable var1) {
         if(this.c) {
            io.reactivex.g.a.a(var1);
         } else {
            this.a.onError(var1);
         }

      }

      public void onSubscribe(io.reactivex.b.b var1) {
         try {
            this.b.a(var1);
         } catch (Throwable var3) {
            io.reactivex.exceptions.a.b(var3);
            this.c = true;
            var1.dispose();
            io.reactivex.d.a.e.a(var3, this.a);
            return;
         }

         this.a.onSubscribe(var1);
      }
   }
}
