package io.reactivex.d.e.e;

import io.reactivex.z;
import java.util.concurrent.atomic.AtomicReference;

public final class k extends io.reactivex.v {
   final z a;
   final io.reactivex.c.h b;

   public k(z var1, io.reactivex.c.h var2) {
      this.b = var2;
      this.a = var1;
   }

   protected void b(io.reactivex.x var1) {
      this.a.a(new k.a(var1, this.b));
   }

   static final class a extends AtomicReference implements io.reactivex.b.b, io.reactivex.x {
      final io.reactivex.x a;
      final io.reactivex.c.h b;

      a(io.reactivex.x var1, io.reactivex.c.h var2) {
         this.a = var1;
         this.b = var2;
      }

      public void a_(Object var1) {
         z var3;
         try {
            var3 = (z)io.reactivex.d.b.b.a(this.b.a(var1), "The single returned by the mapper is null");
         } catch (Throwable var2) {
            io.reactivex.exceptions.a.b(var2);
            this.a.onError(var2);
            return;
         }

         if(!this.isDisposed()) {
            var3.a(new k.a(this, this.a));
         }

      }

      public void dispose() {
         io.reactivex.d.a.d.a((AtomicReference)this);
      }

      public boolean isDisposed() {
         return io.reactivex.d.a.d.a((io.reactivex.b.b)this.get());
      }

      public void onError(Throwable var1) {
         this.a.onError(var1);
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.b(this, var1)) {
            this.a.onSubscribe(this);
         }

      }
   }

   static final class a implements io.reactivex.x {
      final AtomicReference a;
      final io.reactivex.x b;

      a(AtomicReference var1, io.reactivex.x var2) {
         this.a = var1;
         this.b = var2;
      }

      public void a_(Object var1) {
         this.b.a_(var1);
      }

      public void onError(Throwable var1) {
         this.b.onError(var1);
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         io.reactivex.d.a.d.c(this.a, var1);
      }
   }
}
