package io.reactivex.d.e.e;

import io.reactivex.z;

public final class o extends io.reactivex.v {
   final z a;
   final io.reactivex.c.h b;

   public o(z var1, io.reactivex.c.h var2) {
      this.a = var1;
      this.b = var2;
   }

   protected void b(io.reactivex.x var1) {
      this.a.a(new o.a(var1, this.b));
   }

   static final class a implements io.reactivex.x {
      final io.reactivex.x a;
      final io.reactivex.c.h b;

      a(io.reactivex.x var1, io.reactivex.c.h var2) {
         this.a = var1;
         this.b = var2;
      }

      public void a_(Object var1) {
         try {
            var1 = io.reactivex.d.b.b.a(this.b.a(var1), "The mapper function returned a null value.");
         } catch (Throwable var2) {
            io.reactivex.exceptions.a.b(var2);
            this.onError(var2);
            return;
         }

         this.a.a_(var1);
      }

      public void onError(Throwable var1) {
         this.a.onError(var1);
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         this.a.onSubscribe(var1);
      }
   }
}
