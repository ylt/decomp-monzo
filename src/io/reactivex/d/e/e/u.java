package io.reactivex.d.e.e;

import io.reactivex.z;

public final class u extends io.reactivex.f {
   final z b;

   public u(z var1) {
      this.b = var1;
   }

   public void b(org.a.b var1) {
      this.b.a(new u.a(var1));
   }

   static final class a extends io.reactivex.d.i.b implements io.reactivex.x {
      io.reactivex.b.b a;

      a(org.a.b var1) {
         super(var1);
      }

      public void a() {
         super.a();
         this.a.dispose();
      }

      public void a_(Object var1) {
         this.b(var1);
      }

      public void onError(Throwable var1) {
         this.d.onError(var1);
      }

      public void onSubscribe(io.reactivex.b.b var1) {
         if(io.reactivex.d.a.d.a(this.a, var1)) {
            this.a = var1;
            this.d.a(this);
         }

      }
   }
}
