package io.reactivex.d.e.e;

import io.reactivex.y;
import java.util.concurrent.atomic.AtomicReference;

public final class a extends io.reactivex.v {
   final y a;

   public a(y var1) {
      this.a = var1;
   }

   protected void b(io.reactivex.x var1) {
      a.a var2 = new a.a(var1);
      var1.onSubscribe(var2);

      try {
         this.a.a(var2);
      } catch (Throwable var3) {
         io.reactivex.exceptions.a.b(var3);
         var2.a(var3);
      }

   }

   static final class a extends AtomicReference implements io.reactivex.b.b, io.reactivex.w {
      final io.reactivex.x a;

      a(io.reactivex.x var1) {
         this.a = var1;
      }

      public void a(io.reactivex.b.b var1) {
         io.reactivex.d.a.d.a((AtomicReference)this, var1);
      }

      public void a(io.reactivex.c.f var1) {
         this.a((io.reactivex.b.b)(new io.reactivex.d.a.b(var1)));
      }

      public void a(Object param1) {
         // $FF: Couldn't be decompiled
      }

      public void a(Throwable var1) {
         if(!this.b(var1)) {
            io.reactivex.g.a.a(var1);
         }

      }

      public boolean b(Throwable var1) {
         Object var3 = var1;
         if(var1 == null) {
            var3 = new NullPointerException("onError called with null. Null values are generally not allowed in 2.x operators and sources.");
         }

         boolean var2;
         if(this.get() != io.reactivex.d.a.d.a) {
            io.reactivex.b.b var6 = (io.reactivex.b.b)this.getAndSet(io.reactivex.d.a.d.a);
            if(var6 != io.reactivex.d.a.d.a) {
               try {
                  this.a.onError((Throwable)var3);
               } finally {
                  if(var6 != null) {
                     var6.dispose();
                  }

               }

               var2 = true;
               return var2;
            }
         }

         var2 = false;
         return var2;
      }

      public void dispose() {
         io.reactivex.d.a.d.a((AtomicReference)this);
      }

      public boolean isDisposed() {
         return io.reactivex.d.a.d.a((io.reactivex.b.b)this.get());
      }
   }
}
