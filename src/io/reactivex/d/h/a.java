package io.reactivex.d.h;

import io.reactivex.g;
import io.reactivex.d.i.d;
import io.reactivex.d.j.k;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import org.a.b;
import org.a.c;

public class a extends AtomicInteger implements g, c {
   final b a;
   final io.reactivex.d.j.c b;
   final AtomicLong c;
   final AtomicReference d;
   final AtomicBoolean e;
   volatile boolean f;

   public a(b var1) {
      this.a = var1;
      this.b = new io.reactivex.d.j.c();
      this.c = new AtomicLong();
      this.d = new AtomicReference();
      this.e = new AtomicBoolean();
   }

   public void a() {
      if(!this.f) {
         d.a(this.d);
      }

   }

   public void a(long var1) {
      if(var1 <= 0L) {
         this.a();
         this.onError(new IllegalArgumentException("§3.9 violated: positive request amount required but it was " + var1));
      } else {
         d.a(this.d, this.c, var1);
      }

   }

   public void a(c var1) {
      if(this.e.compareAndSet(false, true)) {
         this.a.a(this);
         d.a(this.d, this.c, var1);
      } else {
         var1.a();
         this.a();
         this.onError(new IllegalStateException("§2.12 violated: onSubscribe must be called at most once"));
      }

   }

   public void onComplete() {
      this.f = true;
      k.a((b)this.a, this, this.b);
   }

   public void onError(Throwable var1) {
      this.f = true;
      k.a((b)this.a, (Throwable)var1, this, this.b);
   }

   public void onNext(Object var1) {
      k.a((b)this.a, (Object)var1, this, this.b);
   }
}
