package io.reactivex.d.b;

import io.reactivex.c.d;

public final class b {
   static final d a = new b.a();

   public static int a(int var0, int var1) {
      byte var2;
      if(var0 < var1) {
         var2 = -1;
      } else if(var0 > var1) {
         var2 = 1;
      } else {
         var2 = 0;
      }

      return var2;
   }

   public static int a(int var0, String var1) {
      if(var0 <= 0) {
         throw new IllegalArgumentException(var1 + " > 0 required but it was " + var0);
      } else {
         return var0;
      }
   }

   public static int a(long var0, long var2) {
      byte var4;
      if(var0 < var2) {
         var4 = -1;
      } else if(var0 > var2) {
         var4 = 1;
      } else {
         var4 = 0;
      }

      return var4;
   }

   public static long a(long var0, String var2) {
      if(var0 <= 0L) {
         throw new IllegalArgumentException(var2 + " > 0 required but it was " + var0);
      } else {
         return var0;
      }
   }

   public static d a() {
      return a;
   }

   public static Object a(Object var0, String var1) {
      if(var0 == null) {
         throw new NullPointerException(var1);
      } else {
         return var0;
      }
   }

   public static boolean a(Object var0, Object var1) {
      boolean var2;
      if(var0 != var1 && (var0 == null || !var0.equals(var1))) {
         var2 = false;
      } else {
         var2 = true;
      }

      return var2;
   }

   static final class a implements d {
      public boolean a(Object var1, Object var2) {
         return b.a(var1, var2);
      }
   }
}
