package io.reactivex.d.b;

import io.reactivex.exceptions.OnErrorNotImplementedException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

public final class a {
   static final io.reactivex.c.h a = new a.v();
   public static final Runnable b = new a.q();
   public static final io.reactivex.c.a c = new a.n();
   static final io.reactivex.c.g d = new a.o();
   public static final io.reactivex.c.g e = new a.s();
   public static final io.reactivex.c.g f = new a.af();
   public static final io.reactivex.c.p g = new a.p();
   static final io.reactivex.c.q h = new a.ak();
   static final io.reactivex.c.q i = new a.t();
   static final Callable j = new a.ae();
   static final Comparator k = new a.aa();
   public static final io.reactivex.c.g l = new a.y();

   public static io.reactivex.c.b a(io.reactivex.c.h var0) {
      return new a.ah(var0);
   }

   public static io.reactivex.c.b a(io.reactivex.c.h var0, io.reactivex.c.h var1) {
      return new a.ai(var1, var0);
   }

   public static io.reactivex.c.b a(io.reactivex.c.h var0, io.reactivex.c.h var1, io.reactivex.c.h var2) {
      return new a.aj(var2, var1, var0);
   }

   public static io.reactivex.c.g a(io.reactivex.c.a var0) {
      return new a.a(var0);
   }

   public static io.reactivex.c.g a(io.reactivex.c.g var0) {
      return new a.ad(var0);
   }

   public static io.reactivex.c.h a() {
      return a;
   }

   public static io.reactivex.c.h a(io.reactivex.c.c var0) {
      b.a(var0, (String)"f is null");
      return new a.b(var0);
   }

   public static io.reactivex.c.h a(io.reactivex.c.i var0) {
      b.a(var0, (String)"f is null");
      return new a.c(var0);
   }

   public static io.reactivex.c.h a(io.reactivex.c.j var0) {
      b.a(var0, (String)"f is null");
      return new a.d(var0);
   }

   public static io.reactivex.c.h a(io.reactivex.c.k var0) {
      b.a(var0, (String)"f is null");
      return new a.e(var0);
   }

   public static io.reactivex.c.h a(io.reactivex.c.l var0) {
      b.a(var0, (String)"f is null");
      return new a.f(var0);
   }

   public static io.reactivex.c.h a(io.reactivex.c.m var0) {
      b.a(var0, (String)"f is null");
      return new a.g(var0);
   }

   public static io.reactivex.c.h a(io.reactivex.c.n var0) {
      b.a(var0, (String)"f is null");
      return new a.h(var0);
   }

   public static io.reactivex.c.h a(io.reactivex.c.o var0) {
      b.a(var0, (String)"f is null");
      return new a.i(var0);
   }

   public static io.reactivex.c.h a(Class var0) {
      return new a.l(var0);
   }

   public static io.reactivex.c.h a(Comparator var0) {
      return new a.x(var0);
   }

   public static io.reactivex.c.h a(TimeUnit var0, io.reactivex.u var1) {
      return new a.ag(var0, var1);
   }

   public static io.reactivex.c.q a(io.reactivex.c.e var0) {
      return new a.k(var0);
   }

   public static Callable a(int var0) {
      return new a.j(var0);
   }

   public static Callable a(Object var0) {
      return new a.w(var0);
   }

   public static io.reactivex.c.g b() {
      return d;
   }

   public static io.reactivex.c.g b(io.reactivex.c.g var0) {
      return new a.ac(var0);
   }

   public static io.reactivex.c.h b(Object var0) {
      return new a.w(var0);
   }

   public static io.reactivex.c.q b(Class var0) {
      return new a.m(var0);
   }

   public static io.reactivex.c.a c(io.reactivex.c.g var0) {
      return new a.ab(var0);
   }

   public static io.reactivex.c.q c() {
      return h;
   }

   public static io.reactivex.c.q c(Object var0) {
      return new a.r(var0);
   }

   public static io.reactivex.c.q d() {
      return i;
   }

   public static Callable e() {
      return j;
   }

   public static Comparator f() {
      return k;
   }

   public static Callable g() {
      return a.u.a;
   }

   public static Comparator h() {
      return a.z.a;
   }

   static final class a implements io.reactivex.c.g {
      final io.reactivex.c.a a;

      a(io.reactivex.c.a var1) {
         this.a = var1;
      }

      public void a(Object var1) throws Exception {
         this.a.a();
      }
   }

   static final class aa implements Comparator {
      public int compare(Object var1, Object var2) {
         return ((Comparable)var1).compareTo(var2);
      }
   }

   static final class ab implements io.reactivex.c.a {
      final io.reactivex.c.g a;

      ab(io.reactivex.c.g var1) {
         this.a = var1;
      }

      public void a() throws Exception {
         this.a.a(io.reactivex.m.f());
      }
   }

   static final class ac implements io.reactivex.c.g {
      final io.reactivex.c.g a;

      ac(io.reactivex.c.g var1) {
         this.a = var1;
      }

      public void a(Throwable var1) throws Exception {
         this.a.a(io.reactivex.m.a(var1));
      }
   }

   static final class ad implements io.reactivex.c.g {
      final io.reactivex.c.g a;

      ad(io.reactivex.c.g var1) {
         this.a = var1;
      }

      public void a(Object var1) throws Exception {
         this.a.a(io.reactivex.m.a(var1));
      }
   }

   static final class ae implements Callable {
      public Object call() {
         return null;
      }
   }

   static final class af implements io.reactivex.c.g {
      public void a(Throwable var1) {
         io.reactivex.g.a.a((Throwable)(new OnErrorNotImplementedException(var1)));
      }
   }

   static final class ag implements io.reactivex.c.h {
      final TimeUnit a;
      final io.reactivex.u b;

      ag(TimeUnit var1, io.reactivex.u var2) {
         this.a = var1;
         this.b = var2;
      }

      // $FF: synthetic method
      public Object a(Object var1) throws Exception {
         return this.b(var1);
      }

      public io.reactivex.h.b b(Object var1) throws Exception {
         return new io.reactivex.h.b(var1, this.b.a(this.a), this.a);
      }
   }

   static final class ah implements io.reactivex.c.b {
      private final io.reactivex.c.h a;

      ah(io.reactivex.c.h var1) {
         this.a = var1;
      }

      public void a(Map var1, Object var2) throws Exception {
         var1.put(this.a.a(var2), var2);
      }
   }

   static final class ai implements io.reactivex.c.b {
      private final io.reactivex.c.h a;
      private final io.reactivex.c.h b;

      ai(io.reactivex.c.h var1, io.reactivex.c.h var2) {
         this.a = var1;
         this.b = var2;
      }

      public void a(Map var1, Object var2) throws Exception {
         var1.put(this.b.a(var2), this.a.a(var2));
      }
   }

   static final class aj implements io.reactivex.c.b {
      private final io.reactivex.c.h a;
      private final io.reactivex.c.h b;
      private final io.reactivex.c.h c;

      aj(io.reactivex.c.h var1, io.reactivex.c.h var2, io.reactivex.c.h var3) {
         this.a = var1;
         this.b = var2;
         this.c = var3;
      }

      public void a(Map var1, Object var2) throws Exception {
         Object var5 = this.c.a(var2);
         Collection var4 = (Collection)var1.get(var5);
         Collection var3 = var4;
         if(var4 == null) {
            var3 = (Collection)this.a.a(var5);
            var1.put(var5, var3);
         }

         var3.add(this.b.a(var2));
      }
   }

   static final class ak implements io.reactivex.c.q {
      public boolean a(Object var1) {
         return true;
      }
   }

   static final class b implements io.reactivex.c.h {
      final io.reactivex.c.c a;

      b(io.reactivex.c.c var1) {
         this.a = var1;
      }

      public Object a(Object[] var1) throws Exception {
         if(var1.length != 2) {
            throw new IllegalArgumentException("Array of size 2 expected but got " + var1.length);
         } else {
            return this.a.a(var1[0], var1[1]);
         }
      }
   }

   static final class c implements io.reactivex.c.h {
      final io.reactivex.c.i a;

      c(io.reactivex.c.i var1) {
         this.a = var1;
      }

      public Object a(Object[] var1) throws Exception {
         if(var1.length != 3) {
            throw new IllegalArgumentException("Array of size 3 expected but got " + var1.length);
         } else {
            return this.a.a(var1[0], var1[1], var1[2]);
         }
      }
   }

   static final class d implements io.reactivex.c.h {
      final io.reactivex.c.j a;

      d(io.reactivex.c.j var1) {
         this.a = var1;
      }

      public Object a(Object[] var1) throws Exception {
         if(var1.length != 4) {
            throw new IllegalArgumentException("Array of size 4 expected but got " + var1.length);
         } else {
            return this.a.a(var1[0], var1[1], var1[2], var1[3]);
         }
      }
   }

   static final class e implements io.reactivex.c.h {
      private final io.reactivex.c.k a;

      e(io.reactivex.c.k var1) {
         this.a = var1;
      }

      public Object a(Object[] var1) throws Exception {
         if(var1.length != 5) {
            throw new IllegalArgumentException("Array of size 5 expected but got " + var1.length);
         } else {
            return this.a.a(var1[0], var1[1], var1[2], var1[3], var1[4]);
         }
      }
   }

   static final class f implements io.reactivex.c.h {
      final io.reactivex.c.l a;

      f(io.reactivex.c.l var1) {
         this.a = var1;
      }

      public Object a(Object[] var1) throws Exception {
         if(var1.length != 6) {
            throw new IllegalArgumentException("Array of size 6 expected but got " + var1.length);
         } else {
            return this.a.a(var1[0], var1[1], var1[2], var1[3], var1[4], var1[5]);
         }
      }
   }

   static final class g implements io.reactivex.c.h {
      final io.reactivex.c.m a;

      g(io.reactivex.c.m var1) {
         this.a = var1;
      }

      public Object a(Object[] var1) throws Exception {
         if(var1.length != 7) {
            throw new IllegalArgumentException("Array of size 7 expected but got " + var1.length);
         } else {
            return this.a.a(var1[0], var1[1], var1[2], var1[3], var1[4], var1[5], var1[6]);
         }
      }
   }

   static final class h implements io.reactivex.c.h {
      final io.reactivex.c.n a;

      h(io.reactivex.c.n var1) {
         this.a = var1;
      }

      public Object a(Object[] var1) throws Exception {
         if(var1.length != 8) {
            throw new IllegalArgumentException("Array of size 8 expected but got " + var1.length);
         } else {
            return this.a.a(var1[0], var1[1], var1[2], var1[3], var1[4], var1[5], var1[6], var1[7]);
         }
      }
   }

   static final class i implements io.reactivex.c.h {
      final io.reactivex.c.o a;

      i(io.reactivex.c.o var1) {
         this.a = var1;
      }

      public Object a(Object[] var1) throws Exception {
         if(var1.length != 9) {
            throw new IllegalArgumentException("Array of size 9 expected but got " + var1.length);
         } else {
            return this.a.a(var1[0], var1[1], var1[2], var1[3], var1[4], var1[5], var1[6], var1[7], var1[8]);
         }
      }
   }

   static final class j implements Callable {
      final int a;

      j(int var1) {
         this.a = var1;
      }

      public List a() throws Exception {
         return new ArrayList(this.a);
      }

      // $FF: synthetic method
      public Object call() throws Exception {
         return this.a();
      }
   }

   static final class k implements io.reactivex.c.q {
      final io.reactivex.c.e a;

      k(io.reactivex.c.e var1) {
         this.a = var1;
      }

      public boolean a(Object var1) throws Exception {
         boolean var2;
         if(!this.a.a()) {
            var2 = true;
         } else {
            var2 = false;
         }

         return var2;
      }
   }

   static final class l implements io.reactivex.c.h {
      final Class a;

      l(Class var1) {
         this.a = var1;
      }

      public Object a(Object var1) throws Exception {
         return this.a.cast(var1);
      }
   }

   static final class m implements io.reactivex.c.q {
      final Class a;

      m(Class var1) {
         this.a = var1;
      }

      public boolean a(Object var1) throws Exception {
         return this.a.isInstance(var1);
      }
   }

   static final class n implements io.reactivex.c.a {
      public void a() {
      }

      public String toString() {
         return "EmptyAction";
      }
   }

   static final class o implements io.reactivex.c.g {
      public void a(Object var1) {
      }

      public String toString() {
         return "EmptyConsumer";
      }
   }

   static final class p implements io.reactivex.c.p {
   }

   static final class q implements Runnable {
      public void run() {
      }

      public String toString() {
         return "EmptyRunnable";
      }
   }

   static final class r implements io.reactivex.c.q {
      final Object a;

      r(Object var1) {
         this.a = var1;
      }

      public boolean a(Object var1) throws Exception {
         return b.a(var1, this.a);
      }
   }

   static final class s implements io.reactivex.c.g {
      public void a(Throwable var1) {
         io.reactivex.g.a.a(var1);
      }
   }

   static final class t implements io.reactivex.c.q {
      public boolean a(Object var1) {
         return false;
      }
   }

   static enum u implements Callable {
      a;

      public Set a() throws Exception {
         return new HashSet();
      }

      // $FF: synthetic method
      public Object call() throws Exception {
         return this.a();
      }
   }

   static final class v implements io.reactivex.c.h {
      public Object a(Object var1) {
         return var1;
      }

      public String toString() {
         return "IdentityFunction";
      }
   }

   static final class w implements io.reactivex.c.h, Callable {
      final Object a;

      w(Object var1) {
         this.a = var1;
      }

      public Object a(Object var1) throws Exception {
         return this.a;
      }

      public Object call() throws Exception {
         return this.a;
      }
   }

   static final class x implements io.reactivex.c.h {
      final Comparator a;

      x(Comparator var1) {
         this.a = var1;
      }

      public List a(List var1) {
         Collections.sort(var1, this.a);
         return var1;
      }
   }

   static final class y implements io.reactivex.c.g {
      public void a(org.a.c var1) throws Exception {
         var1.a(Long.MAX_VALUE);
      }
   }

   static enum z implements Comparator {
      a;

      public int compare(Object var1, Object var2) {
         return ((Comparable)var1).compareTo(var2);
      }
   }
}
