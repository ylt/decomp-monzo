package io.reactivex.d.i;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

public class c extends AtomicInteger implements org.a.c {
   org.a.c a;
   long b;
   final AtomicReference c = new AtomicReference();
   final AtomicLong d = new AtomicLong();
   final AtomicLong e = new AtomicLong();
   volatile boolean f;
   protected boolean g;

   public void a() {
      if(!this.f) {
         this.f = true;
         this.b();
      }

   }

   public final void a(long var1) {
      if(d.b(var1) && !this.g) {
         if(this.get() == 0 && this.compareAndSet(0, 1)) {
            long var3 = this.b;
            if(var3 != Long.MAX_VALUE) {
               var3 = io.reactivex.d.j.d.a(var3, var1);
               this.b = var3;
               if(var3 == Long.MAX_VALUE) {
                  this.g = true;
               }
            }

            org.a.c var5 = this.a;
            if(this.decrementAndGet() != 0) {
               this.c();
            }

            if(var5 != null) {
               var5.a(var1);
            }
         } else {
            io.reactivex.d.j.d.a(this.d, var1);
            this.b();
         }
      }

   }

   public final void a(org.a.c var1) {
      if(this.f) {
         var1.a();
      } else {
         io.reactivex.d.b.b.a(var1, (String)"s is null");
         if(this.get() == 0 && this.compareAndSet(0, 1)) {
            org.a.c var4 = this.a;
            if(var4 != null) {
               var4.a();
            }

            this.a = var1;
            long var2 = this.b;
            if(this.decrementAndGet() != 0) {
               this.c();
            }

            if(var2 != 0L) {
               var1.a(var2);
            }
         } else {
            var1 = (org.a.c)this.c.getAndSet(var1);
            if(var1 != null) {
               var1.a();
            }

            this.b();
         }
      }

   }

   final void b() {
      if(this.getAndIncrement() == 0) {
         this.c();
      }

   }

   public final void b(long var1) {
      long var3 = 0L;
      if(!this.g) {
         if(this.get() == 0 && this.compareAndSet(0, 1)) {
            long var5 = this.b;
            if(var5 != Long.MAX_VALUE) {
               var1 = var5 - var1;
               if(var1 < 0L) {
                  d.c(var1);
                  var1 = var3;
               }

               this.b = var1;
            }

            if(this.decrementAndGet() != 0) {
               this.c();
            }
         } else {
            io.reactivex.d.j.d.a(this.e, var1);
            this.b();
         }
      }

   }

   final void c() {
      long var2 = 0L;
      org.a.c var10 = null;
      int var1 = 1;

      do {
         org.a.c var12 = (org.a.c)this.c.get();
         org.a.c var11 = var12;
         if(var12 != null) {
            var11 = (org.a.c)this.c.getAndSet((Object)null);
         }

         long var6 = this.d.get();
         if(var6 != 0L) {
            var6 = this.d.getAndSet(0L);
         }

         long var4 = this.e.get();
         if(var4 != 0L) {
            var4 = this.e.getAndSet(0L);
         }

         var12 = this.a;
         if(this.f) {
            if(var12 != null) {
               var12.a();
               this.a = null;
            }

            if(var11 != null) {
               var11.a();
            }
         } else {
            long var8 = this.b;
            if(var8 != Long.MAX_VALUE) {
               var8 = io.reactivex.d.j.d.a(var8, var6);
               if(var8 != Long.MAX_VALUE) {
                  var8 -= var4;
                  var4 = var8;
                  if(var8 < 0L) {
                     d.c(var8);
                     var4 = 0L;
                  }
               } else {
                  var4 = var8;
               }

               this.b = var4;
            } else {
               var4 = var8;
            }

            if(var11 != null) {
               if(var12 != null) {
                  var12.a();
               }

               this.a = var11;
               if(var4 != 0L) {
                  var2 = io.reactivex.d.j.d.a(var2, var4);
                  var10 = var11;
               }
            } else if(var12 != null && var6 != 0L) {
               var2 = io.reactivex.d.j.d.a(var2, var6);
               var10 = var12;
            }
         }

         var1 = this.addAndGet(-var1);
      } while(var1 != 0);

      if(var2 != 0L) {
         var10.a(var2);
      }

   }

   public final boolean d() {
      return this.f;
   }
}
