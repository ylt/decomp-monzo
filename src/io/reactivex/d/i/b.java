package io.reactivex.d.i;

public class b extends a {
   protected final org.a.b d;
   protected Object e;

   public b(org.a.b var1) {
      this.d = var1;
   }

   public final int a(int var1) {
      byte var2;
      if((var1 & 2) != 0) {
         this.lazySet(8);
         var2 = 2;
      } else {
         var2 = 0;
      }

      return var2;
   }

   public void a() {
      this.set(4);
      this.e = null;
   }

   public final void a(long var1) {
      if(d.b(var1)) {
         do {
            int var3 = this.get();
            if((var3 & -2) != 0) {
               break;
            }

            if(var3 == 1) {
               if(this.compareAndSet(1, 3)) {
                  Object var4 = this.e;
                  if(var4 != null) {
                     this.e = null;
                     org.a.b var5 = this.d;
                     var5.onNext(var4);
                     if(this.get() != 4) {
                        var5.onComplete();
                     }
                  }
               }
               break;
            }
         } while(!this.compareAndSet(0, 2));
      }

   }

   public final void b(Object var1) {
      int var2 = this.get();

      while(true) {
         org.a.b var4;
         if(var2 == 8) {
            this.e = var1;
            this.lazySet(16);
            var4 = this.d;
            var4.onNext(var1);
            if(this.get() != 4) {
               var4.onComplete();
            }
            break;
         }

         if((var2 & -3) != 0) {
            break;
         }

         if(var2 == 2) {
            this.lazySet(3);
            var4 = this.d;
            var4.onNext(var1);
            if(this.get() != 4) {
               var4.onComplete();
            }
            break;
         }

         this.e = var1;
         if(this.compareAndSet(0, 1)) {
            break;
         }

         int var3 = this.get();
         var2 = var3;
         if(var3 == 4) {
            this.e = null;
            break;
         }
      }

   }

   public final boolean b() {
      boolean var1;
      if(this.get() != 16) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public final void c() {
      this.lazySet(32);
      this.e = null;
   }

   public final Object n_() {
      Object var1;
      if(this.get() == 16) {
         this.lazySet(32);
         var1 = this.e;
         this.e = null;
      } else {
         var1 = null;
      }

      return var1;
   }
}
