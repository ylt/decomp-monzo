package io.reactivex.d.i;

import io.reactivex.exceptions.ProtocolViolationException;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

public enum d implements org.a.c {
   a;

   public static void a(AtomicReference var0, AtomicLong var1, long var2) {
      org.a.c var4 = (org.a.c)var0.get();
      if(var4 != null) {
         var4.a(var2);
      } else if(b(var2)) {
         io.reactivex.d.j.d.a(var1, var2);
         org.a.c var5 = (org.a.c)var0.get();
         if(var5 != null) {
            var2 = var1.getAndSet(0L);
            if(var2 != 0L) {
               var5.a(var2);
            }
         }
      }

   }

   public static boolean a(AtomicReference var0) {
      boolean var1;
      if((org.a.c)var0.get() != a) {
         org.a.c var2 = (org.a.c)var0.getAndSet(a);
         if(var2 != a) {
            if(var2 != null) {
               var2.a();
            }

            var1 = true;
            return var1;
         }
      }

      var1 = false;
      return var1;
   }

   public static boolean a(AtomicReference var0, AtomicLong var1, org.a.c var2) {
      boolean var5;
      if(a(var0, var2)) {
         long var3 = var1.getAndSet(0L);
         if(var3 != 0L) {
            var2.a(var3);
         }

         var5 = true;
      } else {
         var5 = false;
      }

      return var5;
   }

   public static boolean a(AtomicReference var0, org.a.c var1) {
      io.reactivex.d.b.b.a(var1, (String)"d is null");
      boolean var2;
      if(!var0.compareAndSet((Object)null, var1)) {
         var1.a();
         if(var0.get() != a) {
            b();
         }

         var2 = false;
      } else {
         var2 = true;
      }

      return var2;
   }

   public static boolean a(org.a.c var0, org.a.c var1) {
      boolean var2 = false;
      if(var1 == null) {
         io.reactivex.g.a.a((Throwable)(new NullPointerException("next is null")));
      } else if(var0 != null) {
         var1.a();
         b();
      } else {
         var2 = true;
      }

      return var2;
   }

   public static void b() {
      io.reactivex.g.a.a((Throwable)(new ProtocolViolationException("Subscription already set!")));
   }

   public static boolean b(long var0) {
      boolean var2;
      if(var0 <= 0L) {
         io.reactivex.g.a.a((Throwable)(new IllegalArgumentException("n > 0 required but it was " + var0)));
         var2 = false;
      } else {
         var2 = true;
      }

      return var2;
   }

   public static void c(long var0) {
      io.reactivex.g.a.a((Throwable)(new ProtocolViolationException("More produced than requested: " + var0)));
   }

   public void a() {
   }

   public void a(long var1) {
   }
}
