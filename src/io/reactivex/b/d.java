package io.reactivex.b;

import java.util.concurrent.atomic.AtomicReference;

abstract class d extends AtomicReference implements b {
   d(Object var1) {
      super(io.reactivex.d.b.b.a(var1, "value is null"));
   }

   protected abstract void a(Object var1);

   public final void dispose() {
      if(this.get() != null) {
         Object var1 = this.getAndSet((Object)null);
         if(var1 != null) {
            this.a(var1);
         }
      }

   }

   public final boolean isDisposed() {
      boolean var1;
      if(this.get() == null) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }
}
