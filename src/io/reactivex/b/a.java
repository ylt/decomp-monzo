package io.reactivex.b;

import io.reactivex.d.j.j;
import io.reactivex.d.j.p;
import io.reactivex.exceptions.CompositeException;
import java.util.ArrayList;

public final class a implements b, io.reactivex.d.a.c {
   p a;
   volatile boolean b;

   public void a() {
      // $FF: Couldn't be decompiled
   }

   void a(p var1) {
      if(var1 != null) {
         ArrayList var4 = null;
         Object[] var5 = var1.b();
         int var3 = var5.length;
         int var2 = 0;

         ArrayList var8;
         for(var8 = var4; var2 < var3; var8 = var4) {
            Object var6 = var5[var2];
            var4 = var8;
            if(var6 instanceof b) {
               label45: {
                  try {
                     ((b)var6).dispose();
                  } catch (Throwable var7) {
                     io.reactivex.exceptions.a.b(var7);
                     if(var8 == null) {
                        var8 = new ArrayList();
                     }

                     var8.add(var7);
                     var4 = var8;
                     break label45;
                  }

                  var4 = var8;
               }
            }

            ++var2;
         }

         if(var8 != null) {
            if(var8.size() == 1) {
               throw j.a((Throwable)var8.get(0));
            }

            throw new CompositeException(var8);
         }
      }

   }

   public boolean a(b param1) {
      // $FF: Couldn't be decompiled
   }

   public boolean b(b var1) {
      boolean var2;
      if(this.c(var1)) {
         var1.dispose();
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   public boolean c(b param1) {
      // $FF: Couldn't be decompiled
   }

   public void dispose() {
      // $FF: Couldn't be decompiled
   }

   public boolean isDisposed() {
      return this.b;
   }
}
