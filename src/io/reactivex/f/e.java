package io.reactivex.f;

import io.reactivex.t;

public final class e implements io.reactivex.b.b, t {
   final t a;
   final boolean b;
   io.reactivex.b.b c;
   boolean d;
   io.reactivex.d.j.a e;
   volatile boolean f;

   public e(t var1) {
      this(var1, false);
   }

   public e(t var1, boolean var2) {
      this.a = var1;
      this.b = var2;
   }

   void a() {
      // $FF: Couldn't be decompiled
   }

   public void dispose() {
      this.c.dispose();
   }

   public boolean isDisposed() {
      return this.c.isDisposed();
   }

   public void onComplete() {
      // $FF: Couldn't be decompiled
   }

   public void onError(Throwable param1) {
      // $FF: Couldn't be decompiled
   }

   public void onNext(Object param1) {
      // $FF: Couldn't be decompiled
   }

   public void onSubscribe(io.reactivex.b.b var1) {
      if(io.reactivex.d.a.d.a(this.c, var1)) {
         this.c = var1;
         this.a.onSubscribe(this);
      }

   }
}
