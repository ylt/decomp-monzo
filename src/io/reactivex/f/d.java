package io.reactivex.f;

import io.reactivex.t;
import io.reactivex.exceptions.CompositeException;

public final class d implements io.reactivex.b.b, t {
   final t a;
   io.reactivex.b.b b;
   boolean c;

   public d(t var1) {
      this.a = var1;
   }

   void a() {
      this.c = true;
      NullPointerException var1 = new NullPointerException("Subscription not set!");

      try {
         this.a.onSubscribe(io.reactivex.d.a.e.a);
      } catch (Throwable var4) {
         io.reactivex.exceptions.a.b(var4);
         io.reactivex.g.a.a((Throwable)(new CompositeException(new Throwable[]{var1, var4})));
         return;
      }

      try {
         this.a.onError(var1);
      } catch (Throwable var3) {
         io.reactivex.exceptions.a.b(var3);
         io.reactivex.g.a.a((Throwable)(new CompositeException(new Throwable[]{var1, var3})));
      }

   }

   void b() {
      NullPointerException var1 = new NullPointerException("Subscription not set!");

      try {
         this.a.onSubscribe(io.reactivex.d.a.e.a);
      } catch (Throwable var4) {
         io.reactivex.exceptions.a.b(var4);
         io.reactivex.g.a.a((Throwable)(new CompositeException(new Throwable[]{var1, var4})));
         return;
      }

      try {
         this.a.onError(var1);
      } catch (Throwable var3) {
         io.reactivex.exceptions.a.b(var3);
         io.reactivex.g.a.a((Throwable)(new CompositeException(new Throwable[]{var1, var3})));
      }

   }

   public void dispose() {
      this.b.dispose();
   }

   public boolean isDisposed() {
      return this.b.isDisposed();
   }

   public void onComplete() {
      if(!this.c) {
         this.c = true;
         if(this.b == null) {
            this.b();
         } else {
            try {
               this.a.onComplete();
            } catch (Throwable var2) {
               io.reactivex.exceptions.a.b(var2);
               io.reactivex.g.a.a(var2);
            }
         }
      }

   }

   public void onError(Throwable var1) {
      if(this.c) {
         io.reactivex.g.a.a(var1);
      } else {
         this.c = true;
         if(this.b == null) {
            NullPointerException var2 = new NullPointerException("Subscription not set!");

            try {
               this.a.onSubscribe(io.reactivex.d.a.e.a);
            } catch (Throwable var7) {
               io.reactivex.exceptions.a.b(var7);
               io.reactivex.g.a.a((Throwable)(new CompositeException(new Throwable[]{var1, var2, var7})));
               return;
            }

            try {
               t var4 = this.a;
               CompositeException var3 = new CompositeException(new Throwable[]{var1, var2});
               var4.onError(var3);
            } catch (Throwable var6) {
               io.reactivex.exceptions.a.b(var6);
               io.reactivex.g.a.a((Throwable)(new CompositeException(new Throwable[]{var1, var2, var6})));
            }
         } else {
            Object var8 = var1;
            if(var1 == null) {
               var8 = new NullPointerException("onError called with null. Null values are generally not allowed in 2.x operators and sources.");
            }

            try {
               this.a.onError((Throwable)var8);
            } catch (Throwable var5) {
               io.reactivex.exceptions.a.b(var5);
               io.reactivex.g.a.a((Throwable)(new CompositeException(new Throwable[]{(Throwable)var8, var5})));
            }
         }
      }

   }

   public void onNext(Object var1) {
      if(!this.c) {
         if(this.b == null) {
            this.a();
         } else if(var1 == null) {
            NullPointerException var6 = new NullPointerException("onNext called with null. Null values are generally not allowed in 2.x operators and sources.");

            try {
               this.b.dispose();
            } catch (Throwable var5) {
               io.reactivex.exceptions.a.b(var5);
               this.onError(new CompositeException(new Throwable[]{var6, var5}));
               return;
            }

            this.onError(var6);
         } else {
            try {
               this.a.onNext(var1);
            } catch (Throwable var4) {
               io.reactivex.exceptions.a.b(var4);

               try {
                  this.b.dispose();
               } catch (Throwable var3) {
                  io.reactivex.exceptions.a.b(var3);
                  this.onError(new CompositeException(new Throwable[]{var4, var3}));
                  return;
               }

               this.onError(var4);
            }
         }
      }

   }

   public void onSubscribe(io.reactivex.b.b var1) {
      if(io.reactivex.d.a.d.a(this.b, var1)) {
         this.b = var1;

         try {
            this.a.onSubscribe(this);
         } catch (Throwable var4) {
            io.reactivex.exceptions.a.b(var4);
            this.c = true;

            try {
               var1.dispose();
            } catch (Throwable var3) {
               io.reactivex.exceptions.a.b(var3);
               io.reactivex.g.a.a((Throwable)(new CompositeException(new Throwable[]{var4, var3})));
               return;
            }

            io.reactivex.g.a.a(var4);
         }
      }

   }
}
