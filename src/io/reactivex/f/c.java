package io.reactivex.f;

import io.reactivex.t;
import io.reactivex.d.j.h;
import java.util.concurrent.atomic.AtomicReference;

public abstract class c implements io.reactivex.b.b, t {
   final AtomicReference f = new AtomicReference();

   protected void c() {
   }

   public final void dispose() {
      io.reactivex.d.a.d.a(this.f);
   }

   public final boolean isDisposed() {
      boolean var1;
      if(this.f.get() == io.reactivex.d.a.d.a) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public final void onSubscribe(io.reactivex.b.b var1) {
      if(h.a(this.f, var1, this.getClass())) {
         this.c();
      }

   }
}
