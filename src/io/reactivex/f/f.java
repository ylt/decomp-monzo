package io.reactivex.f;

import io.reactivex.j;
import io.reactivex.t;
import io.reactivex.x;
import java.util.concurrent.atomic.AtomicReference;

public class f extends a implements io.reactivex.b.b, io.reactivex.c, j, t, x {
   private final t i;
   private final AtomicReference j;
   private io.reactivex.d.c.d k;

   public f() {
      this(f.a.a);
   }

   public f(t var1) {
      this.j = new AtomicReference();
      this.i = var1;
   }

   public void a_(Object var1) {
      this.onNext(var1);
      this.onComplete();
   }

   public final void dispose() {
      io.reactivex.d.a.d.a(this.j);
   }

   public final boolean isDisposed() {
      return io.reactivex.d.a.d.a((io.reactivex.b.b)this.j.get());
   }

   public void onComplete() {
      if(!this.f) {
         this.f = true;
         if(this.j.get() == null) {
            this.c.add(new IllegalStateException("onSubscribe not called in proper order"));
         }
      }

      try {
         this.e = Thread.currentThread();
         ++this.d;
         this.i.onComplete();
      } finally {
         this.a.countDown();
      }

   }

   public void onError(Throwable param1) {
      // $FF: Couldn't be decompiled
   }

   public void onNext(Object param1) {
      // $FF: Couldn't be decompiled
   }

   public void onSubscribe(io.reactivex.b.b param1) {
      // $FF: Couldn't be decompiled
   }

   static enum a implements t {
      a;

      public void onComplete() {
      }

      public void onError(Throwable var1) {
      }

      public void onNext(Object var1) {
      }

      public void onSubscribe(io.reactivex.b.b var1) {
      }
   }
}
