package io.reactivex;

public abstract class f implements org.a.a {
   static final int a = Math.max(1, Integer.getInteger("rx2.buffer-size", 128).intValue());

   public static int a() {
      return a;
   }

   public final f a(int var1, boolean var2, boolean var3) {
      io.reactivex.d.b.b.a(var1, "bufferSize");
      return io.reactivex.g.a.a((f)(new io.reactivex.d.e.b.c(this, var1, var3, var2, io.reactivex.d.b.a.c)));
   }

   public final f a(long var1) {
      return this.a(var1, io.reactivex.d.b.a.c());
   }

   public final f a(long var1, io.reactivex.c.q var3) {
      if(var1 < 0L) {
         throw new IllegalArgumentException("times >= 0 required but it was " + var1);
      } else {
         io.reactivex.d.b.b.a(var3, (String)"predicate is null");
         return io.reactivex.g.a.a((f)(new io.reactivex.d.e.b.h(this, var1, var3)));
      }
   }

   public final f a(io.reactivex.c.d var1) {
      io.reactivex.d.b.b.a(var1, (String)"predicate is null");
      return io.reactivex.g.a.a((f)(new io.reactivex.d.e.b.g(this, var1)));
   }

   public final void a(g var1) {
      io.reactivex.d.b.b.a(var1, (String)"s is null");

      try {
         org.a.b var5 = io.reactivex.g.a.a((f)this, (org.a.b)var1);
         io.reactivex.d.b.b.a(var5, (String)"Plugin returned null Subscriber");
         this.b(var5);
      } catch (NullPointerException var3) {
         throw var3;
      } catch (Throwable var4) {
         io.reactivex.exceptions.a.b(var4);
         io.reactivex.g.a.a(var4);
         NullPointerException var2 = new NullPointerException("Actually not, but can't throw other exceptions due to RS");
         var2.initCause(var4);
         throw var2;
      }
   }

   public final void a(org.a.b var1) {
      if(var1 instanceof g) {
         this.a((g)var1);
      } else {
         io.reactivex.d.b.b.a(var1, (String)"s is null");
         this.a((g)(new io.reactivex.d.h.a(var1)));
      }

   }

   public final f b() {
      return this.a(a(), false, true);
   }

   protected abstract void b(org.a.b var1);

   public final f c() {
      return io.reactivex.g.a.a((f)(new io.reactivex.d.e.b.d(this)));
   }

   public final f d() {
      return io.reactivex.g.a.a((f)(new io.reactivex.d.e.b.f(this)));
   }
}
