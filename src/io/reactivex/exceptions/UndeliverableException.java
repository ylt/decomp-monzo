package io.reactivex.exceptions;

public final class UndeliverableException extends IllegalStateException {
   public UndeliverableException(Throwable var1) {
      super(var1);
   }
}
