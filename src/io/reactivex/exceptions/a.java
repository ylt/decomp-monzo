package io.reactivex.exceptions;

import io.reactivex.d.j.j;

public final class a {
   public static RuntimeException a(Throwable var0) {
      throw j.a(var0);
   }

   public static void b(Throwable var0) {
      if(var0 instanceof VirtualMachineError) {
         throw (VirtualMachineError)var0;
      } else if(var0 instanceof ThreadDeath) {
         throw (ThreadDeath)var0;
      } else if(var0 instanceof LinkageError) {
         throw (LinkageError)var0;
      }
   }
}
