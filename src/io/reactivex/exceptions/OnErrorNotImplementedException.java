package io.reactivex.exceptions;

public final class OnErrorNotImplementedException extends RuntimeException {
   public OnErrorNotImplementedException(Throwable var1) {
      String var2;
      if(var1 != null) {
         var2 = ((Throwable)var1).getMessage();
      } else {
         var2 = null;
      }

      if(var1 == null) {
         var1 = new NullPointerException();
      }

      super(var2, (Throwable)var1);
   }
}
