package io.reactivex.exceptions;

public final class MissingBackpressureException extends RuntimeException {
   public MissingBackpressureException() {
   }

   public MissingBackpressureException(String var1) {
      super(var1);
   }
}
