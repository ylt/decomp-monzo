package io.reactivex.exceptions;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;

public final class CompositeException extends RuntimeException {
   private final List a;
   private final String b;
   private Throwable c;

   public CompositeException(Iterable var1) {
      LinkedHashSet var3 = new LinkedHashSet();
      ArrayList var2 = new ArrayList();
      if(var1 != null) {
         Iterator var5 = var1.iterator();

         while(var5.hasNext()) {
            Throwable var4 = (Throwable)var5.next();
            if(var4 instanceof CompositeException) {
               var3.addAll(((CompositeException)var4).a());
            } else if(var4 != null) {
               var3.add(var4);
            } else {
               var3.add(new NullPointerException("Throwable was null!"));
            }
         }
      } else {
         var3.add(new NullPointerException("errors was null"));
      }

      if(var3.isEmpty()) {
         throw new IllegalArgumentException("errors is empty");
      } else {
         var2.addAll(var3);
         this.a = Collections.unmodifiableList(var2);
         this.b = this.a.size() + " exceptions occurred. ";
      }
   }

   public CompositeException(Throwable... var1) {
      List var2;
      if(var1 == null) {
         var2 = Collections.singletonList(new NullPointerException("exceptions was null"));
      } else {
         var2 = Arrays.asList(var1);
      }

      this((Iterable)var2);
   }

   private List a(Throwable var1) {
      ArrayList var4 = new ArrayList();
      Throwable var3 = var1.getCause();
      if(var3 != null) {
         Throwable var2 = var3;
         if(var3 != var1) {
            while(true) {
               var4.add(var2);
               var1 = var2.getCause();
               if(var1 == null || var1 == var2) {
                  break;
               }

               var2 = var1;
            }
         }
      }

      return var4;
   }

   private void a(CompositeException.b var1) {
      StringBuilder var4 = new StringBuilder(128);
      var4.append(this).append('\n');
      StackTraceElement[] var5 = this.getStackTrace();
      int var3 = var5.length;

      int var2;
      for(var2 = 0; var2 < var3; ++var2) {
         StackTraceElement var6 = var5[var2];
         var4.append("\tat ").append(var6).append('\n');
      }

      Iterator var7 = this.a.iterator();

      for(var2 = 1; var7.hasNext(); ++var2) {
         Throwable var8 = (Throwable)var7.next();
         var4.append("  ComposedException ").append(var2).append(" :\n");
         this.a(var4, var8, "\t");
      }

      var1.a(var4.toString());
   }

   private void a(StringBuilder var1, Throwable var2, String var3) {
      var1.append(var3).append(var2).append('\n');
      StackTraceElement[] var7 = var2.getStackTrace();
      int var5 = var7.length;

      for(int var4 = 0; var4 < var5; ++var4) {
         StackTraceElement var6 = var7[var4];
         var1.append("\t\tat ").append(var6).append('\n');
      }

      if(var2.getCause() != null) {
         var1.append("\tCaused by: ");
         this.a(var1, var2.getCause(), "");
      }

   }

   private Throwable b(Throwable var1) {
      Throwable var3 = var1.getCause();
      if(var3 != null) {
         Throwable var2 = var3;
         if(this.c != var3) {
            while(true) {
               var3 = var2.getCause();
               var1 = var2;
               if(var3 == null) {
                  break;
               }

               if(var3 == var2) {
                  var1 = var2;
                  break;
               }

               var2 = var3;
            }
         }
      }

      return var1;
   }

   public List a() {
      return this.a;
   }

   public Throwable getCause() {
      // $FF: Couldn't be decompiled
   }

   public String getMessage() {
      return this.b;
   }

   public void printStackTrace() {
      this.printStackTrace(System.err);
   }

   public void printStackTrace(PrintStream var1) {
      this.a((CompositeException.b)(new CompositeException.c(var1)));
   }

   public void printStackTrace(PrintWriter var1) {
      this.a((CompositeException.b)(new CompositeException.d(var1)));
   }

   static final class a extends RuntimeException {
      public String getMessage() {
         return "Chain of Causes for CompositeException In Order Received =>";
      }
   }

   abstract static class b {
      abstract void a(Object var1);
   }

   static final class c extends CompositeException.b {
      private final PrintStream a;

      c(PrintStream var1) {
         this.a = var1;
      }

      void a(Object var1) {
         this.a.println(var1);
      }
   }

   static final class d extends CompositeException.b {
      private final PrintWriter a;

      d(PrintWriter var1) {
         this.a = var1;
      }

      void a(Object var1) {
         this.a.println(var1);
      }
   }
}
