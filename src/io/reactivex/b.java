package io.reactivex;

import io.reactivex.d.e.d.ae;
import java.util.concurrent.Callable;

public abstract class b implements d {
   public static b a() {
      return io.reactivex.g.a.a(io.reactivex.d.e.a.d.a);
   }

   public static b a(io.reactivex.c.a var0) {
      io.reactivex.d.b.b.a(var0, (String)"run is null");
      return io.reactivex.g.a.a((b)(new io.reactivex.d.e.a.f(var0)));
   }

   private b a(io.reactivex.c.g var1, io.reactivex.c.g var2, io.reactivex.c.a var3, io.reactivex.c.a var4, io.reactivex.c.a var5, io.reactivex.c.a var6) {
      io.reactivex.d.b.b.a(var1, (String)"onSubscribe is null");
      io.reactivex.d.b.b.a(var2, (String)"onError is null");
      io.reactivex.d.b.b.a(var3, (String)"onComplete is null");
      io.reactivex.d.b.b.a(var4, (String)"onTerminate is null");
      io.reactivex.d.b.b.a(var5, (String)"onAfterTerminate is null");
      io.reactivex.d.b.b.a(var6, (String)"onDispose is null");
      return io.reactivex.g.a.a((b)(new io.reactivex.d.e.a.n(this, var1, var2, var3, var4, var5, var6)));
   }

   public static b a(d var0) {
      io.reactivex.d.b.b.a(var0, (String)"source is null");
      b var1;
      if(var0 instanceof b) {
         var1 = io.reactivex.g.a.a((b)var0);
      } else {
         var1 = io.reactivex.g.a.a((b)(new io.reactivex.d.e.a.h(var0)));
      }

      return var1;
   }

   public static b a(Iterable var0) {
      io.reactivex.d.b.b.a(var0, (String)"sources is null");
      return io.reactivex.g.a.a((b)(new io.reactivex.d.e.a.k(var0)));
   }

   public static b a(Throwable var0) {
      io.reactivex.d.b.b.a(var0, (String)"error is null");
      return io.reactivex.g.a.a((b)(new io.reactivex.d.e.a.e(var0)));
   }

   public static b a(Callable var0) {
      io.reactivex.d.b.b.a(var0, (String)"completableSupplier");
      return io.reactivex.g.a.a((b)(new io.reactivex.d.e.a.b(var0)));
   }

   public static b a(d... var0) {
      io.reactivex.d.b.b.a(var0, (String)"sources is null");
      b var1;
      if(var0.length == 0) {
         var1 = a();
      } else if(var0.length == 1) {
         var1 = a(var0[0]);
      } else {
         var1 = io.reactivex.g.a.a((b)(new io.reactivex.d.e.a.a(var0)));
      }

      return var1;
   }

   public static b b(d... var0) {
      io.reactivex.d.b.b.a(var0, (String)"sources is null");
      b var1;
      if(var0.length == 0) {
         var1 = a();
      } else if(var0.length == 1) {
         var1 = a(var0[0]);
      } else {
         var1 = io.reactivex.g.a.a((b)(new io.reactivex.d.e.a.i(var0)));
      }

      return var1;
   }

   private static NullPointerException b(Throwable var0) {
      NullPointerException var1 = new NullPointerException("Actually not, but can't pass out an exception otherwise...");
      var1.initCause(var0);
      return var1;
   }

   public final io.reactivex.b.b a(io.reactivex.c.a var1, io.reactivex.c.g var2) {
      io.reactivex.d.b.b.a(var2, (String)"onError is null");
      io.reactivex.d.b.b.a(var1, (String)"onComplete is null");
      io.reactivex.d.d.i var3 = new io.reactivex.d.d.i(var2, var1);
      this.a((c)var3);
      return var3;
   }

   public final b a(io.reactivex.c.g var1) {
      return this.a(io.reactivex.d.b.a.b(), var1, io.reactivex.d.b.a.c, io.reactivex.d.b.a.c, io.reactivex.d.b.a.c, io.reactivex.d.b.a.c);
   }

   public final b a(io.reactivex.c.q var1) {
      io.reactivex.d.b.b.a(var1, (String)"predicate is null");
      return io.reactivex.g.a.a((b)(new io.reactivex.d.e.a.m(this, var1)));
   }

   public final b a(u var1) {
      io.reactivex.d.b.b.a(var1, (String)"scheduler is null");
      return io.reactivex.g.a.a((b)(new io.reactivex.d.e.a.l(this, var1)));
   }

   public final n a(r var1) {
      io.reactivex.d.b.b.a(var1, (String)"next is null");
      return io.reactivex.g.a.a((n)(new ae(var1, this.d())));
   }

   public final v a(z var1) {
      io.reactivex.d.b.b.a(var1, (String)"next is null");
      return io.reactivex.g.a.a((v)(new io.reactivex.d.e.e.c(var1, this)));
   }

   public final v a(Object var1) {
      io.reactivex.d.b.b.a(var1, "completionValue is null");
      return io.reactivex.g.a.a((v)(new io.reactivex.d.e.a.q(this, (Callable)null, var1)));
   }

   public final void a(c var1) {
      io.reactivex.d.b.b.a(var1, (String)"s is null");

      try {
         this.b(io.reactivex.g.a.a(this, var1));
      } catch (NullPointerException var2) {
         throw var2;
      } catch (Throwable var3) {
         io.reactivex.exceptions.a.b(var3);
         io.reactivex.g.a.a(var3);
         throw b(var3);
      }
   }

   public final b b() {
      return this.a(io.reactivex.d.b.a.c());
   }

   public final b b(io.reactivex.c.a var1) {
      return this.a(io.reactivex.d.b.a.b(), io.reactivex.d.b.a.b(), var1, io.reactivex.d.b.a.c, io.reactivex.d.b.a.c, io.reactivex.d.b.a.c);
   }

   public final b b(io.reactivex.c.g var1) {
      io.reactivex.d.b.b.a(var1, (String)"onEvent is null");
      return io.reactivex.g.a.a((b)(new io.reactivex.d.e.a.c(this, var1)));
   }

   public final b b(d var1) {
      return this.c(var1);
   }

   public final b b(u var1) {
      io.reactivex.d.b.b.a(var1, (String)"scheduler is null");
      return io.reactivex.g.a.a((b)(new io.reactivex.d.e.a.o(this, var1)));
   }

   public final v b(Callable var1) {
      io.reactivex.d.b.b.a(var1, (String)"completionValueSupplier is null");
      return io.reactivex.g.a.a((v)(new io.reactivex.d.e.a.q(this, var1, (Object)null)));
   }

   protected abstract void b(c var1);

   public final io.reactivex.b.b c() {
      io.reactivex.d.d.m var1 = new io.reactivex.d.d.m();
      this.a((c)var1);
      return var1;
   }

   public final b c(io.reactivex.c.g var1) {
      return this.a(var1, io.reactivex.d.b.a.b(), io.reactivex.d.b.a.c, io.reactivex.d.b.a.c, io.reactivex.d.b.a.c, io.reactivex.d.b.a.c);
   }

   public final b c(d var1) {
      io.reactivex.d.b.b.a(var1, (String)"other is null");
      return a(new d[]{this, var1});
   }

   public final b d(d var1) {
      io.reactivex.d.b.b.a(var1, (String)"other is null");
      return b(new d[]{this, var1});
   }

   public final n d() {
      n var1;
      if(this instanceof io.reactivex.d.c.c) {
         var1 = ((io.reactivex.d.c.c)this).q_();
      } else {
         var1 = io.reactivex.g.a.a((n)(new io.reactivex.d.e.a.p(this)));
      }

      return var1;
   }
}
