package io.reactivex.h;

import java.util.concurrent.TimeUnit;

public final class b {
   final Object a;
   final long b;
   final TimeUnit c;

   public b(Object var1, long var2, TimeUnit var4) {
      this.a = var1;
      this.b = var2;
      this.c = (TimeUnit)io.reactivex.d.b.b.a(var4, (String)"unit is null");
   }

   public Object a() {
      return this.a;
   }

   public long b() {
      return this.b;
   }

   public boolean equals(Object var1) {
      boolean var3 = false;
      boolean var2 = var3;
      if(var1 instanceof b) {
         b var4 = (b)var1;
         var2 = var3;
         if(io.reactivex.d.b.b.a(this.a, var4.a)) {
            var2 = var3;
            if(this.b == var4.b) {
               var2 = var3;
               if(io.reactivex.d.b.b.a(this.c, (Object)var4.c)) {
                  var2 = true;
               }
            }
         }
      }

      return var2;
   }

   public int hashCode() {
      int var1;
      if(this.a != null) {
         var1 = this.a.hashCode();
      } else {
         var1 = 0;
      }

      return (var1 * 31 + (int)(this.b >>> 31 ^ this.b)) * 31 + this.c.hashCode();
   }

   public String toString() {
      return "Timed[time=" + this.b + ", unit=" + this.c + ", value=" + this.a + "]";
   }
}
