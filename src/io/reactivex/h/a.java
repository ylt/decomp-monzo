package io.reactivex.h;

import io.reactivex.u;
import io.reactivex.d.g.m;
import io.reactivex.d.g.n;
import java.util.concurrent.Callable;

public final class a {
   static final u a = io.reactivex.g.a.d(new a.h());
   static final u b = io.reactivex.g.a.a((Callable)(new a.b()));
   static final u c = io.reactivex.g.a.b((Callable)(new a.c()));
   static final u d = n.c();
   static final u e = io.reactivex.g.a.c((Callable)(new a.f()));

   public static u a() {
      return io.reactivex.g.a.a(b);
   }

   public static u b() {
      return io.reactivex.g.a.b(c);
   }

   public static u c() {
      return d;
   }

   static final class a {
      static final u a = new io.reactivex.d.g.b();
   }

   static final class b implements Callable {
      public u a() throws Exception {
         return a.a.a;
      }

      // $FF: synthetic method
      public Object call() throws Exception {
         return this.a();
      }
   }

   static final class c implements Callable {
      public u a() throws Exception {
         return a.d.a;
      }

      // $FF: synthetic method
      public Object call() throws Exception {
         return this.a();
      }
   }

   static final class d {
      static final u a = new io.reactivex.d.g.d();
   }

   static final class e {
      static final u a = new io.reactivex.d.g.e();
   }

   static final class f implements Callable {
      public u a() throws Exception {
         return a.e.a;
      }

      // $FF: synthetic method
      public Object call() throws Exception {
         return this.a();
      }
   }

   static final class g {
      static final u a = new m();
   }

   static final class h implements Callable {
      public u a() throws Exception {
         return a.g.a;
      }

      // $FF: synthetic method
      public Object call() throws Exception {
         return this.a();
      }
   }
}
