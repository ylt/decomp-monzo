package io.reactivex;

public interface t {
   void onComplete();

   void onError(Throwable var1);

   void onNext(Object var1);

   void onSubscribe(io.reactivex.b.b var1);
}
