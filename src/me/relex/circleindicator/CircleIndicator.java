package me.relex.circleindicator;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.f;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Interpolator;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

public class CircleIndicator extends LinearLayout {
   private ViewPager a;
   private int b = -1;
   private int c = -1;
   private int d = -1;
   private int e;
   private int f;
   private int g;
   private int h;
   private Animator i;
   private Animator j;
   private Animator k;
   private Animator l;
   private int m;
   private final f n;
   private DataSetObserver o;

   public CircleIndicator(Context var1) {
      super(var1);
      this.e = a.a.scale_with_alpha;
      this.f = 0;
      this.g = a.b.white_radius;
      this.h = a.b.white_radius;
      this.m = -1;
      this.n = new f() {
         public void a(int var1) {
         }

         public void a(int var1, float var2, int var3) {
         }

         public void b(int var1) {
            if(CircleIndicator.this.a.getAdapter() != null && CircleIndicator.this.a.getAdapter().b() > 0) {
               if(CircleIndicator.this.j.isRunning()) {
                  CircleIndicator.this.j.end();
                  CircleIndicator.this.j.cancel();
               }

               if(CircleIndicator.this.i.isRunning()) {
                  CircleIndicator.this.i.end();
                  CircleIndicator.this.i.cancel();
               }

               View var2;
               if(CircleIndicator.this.m >= 0) {
                  var2 = CircleIndicator.this.getChildAt(CircleIndicator.this.m);
                  if(var2 != null) {
                     var2.setBackgroundResource(CircleIndicator.this.h);
                     CircleIndicator.this.j.setTarget(var2);
                     CircleIndicator.this.j.start();
                  }
               }

               var2 = CircleIndicator.this.getChildAt(var1);
               if(var2 != null) {
                  var2.setBackgroundResource(CircleIndicator.this.g);
                  CircleIndicator.this.i.setTarget(var2);
                  CircleIndicator.this.i.start();
               }

               CircleIndicator.this.m = var1;
            }

         }
      };
      this.o = new DataSetObserver() {
         public void onChanged() {
            super.onChanged();
            if(CircleIndicator.this.a != null) {
               int var1 = CircleIndicator.this.a.getAdapter().b();
               if(var1 != CircleIndicator.this.getChildCount()) {
                  if(CircleIndicator.this.m < var1) {
                     CircleIndicator.this.m = CircleIndicator.this.a.getCurrentItem();
                  } else {
                     CircleIndicator.this.m = -1;
                  }

                  CircleIndicator.this.a();
               }
            }

         }
      };
      this.a(var1, (AttributeSet)null);
   }

   public CircleIndicator(Context var1, AttributeSet var2) {
      super(var1, var2);
      this.e = a.a.scale_with_alpha;
      this.f = 0;
      this.g = a.b.white_radius;
      this.h = a.b.white_radius;
      this.m = -1;
      this.n = new f() {
         public void a(int var1) {
         }

         public void a(int var1, float var2, int var3) {
         }

         public void b(int var1) {
            if(CircleIndicator.this.a.getAdapter() != null && CircleIndicator.this.a.getAdapter().b() > 0) {
               if(CircleIndicator.this.j.isRunning()) {
                  CircleIndicator.this.j.end();
                  CircleIndicator.this.j.cancel();
               }

               if(CircleIndicator.this.i.isRunning()) {
                  CircleIndicator.this.i.end();
                  CircleIndicator.this.i.cancel();
               }

               View var2;
               if(CircleIndicator.this.m >= 0) {
                  var2 = CircleIndicator.this.getChildAt(CircleIndicator.this.m);
                  if(var2 != null) {
                     var2.setBackgroundResource(CircleIndicator.this.h);
                     CircleIndicator.this.j.setTarget(var2);
                     CircleIndicator.this.j.start();
                  }
               }

               var2 = CircleIndicator.this.getChildAt(var1);
               if(var2 != null) {
                  var2.setBackgroundResource(CircleIndicator.this.g);
                  CircleIndicator.this.i.setTarget(var2);
                  CircleIndicator.this.i.start();
               }

               CircleIndicator.this.m = var1;
            }

         }
      };
      this.o = new DataSetObserver() {
         public void onChanged() {
            super.onChanged();
            if(CircleIndicator.this.a != null) {
               int var1 = CircleIndicator.this.a.getAdapter().b();
               if(var1 != CircleIndicator.this.getChildCount()) {
                  if(CircleIndicator.this.m < var1) {
                     CircleIndicator.this.m = CircleIndicator.this.a.getCurrentItem();
                  } else {
                     CircleIndicator.this.m = -1;
                  }

                  CircleIndicator.this.a();
               }
            }

         }
      };
      this.a(var1, var2);
   }

   public CircleIndicator(Context var1, AttributeSet var2, int var3) {
      super(var1, var2, var3);
      this.e = a.a.scale_with_alpha;
      this.f = 0;
      this.g = a.b.white_radius;
      this.h = a.b.white_radius;
      this.m = -1;
      this.n = new f() {
         public void a(int var1) {
         }

         public void a(int var1, float var2, int var3) {
         }

         public void b(int var1) {
            if(CircleIndicator.this.a.getAdapter() != null && CircleIndicator.this.a.getAdapter().b() > 0) {
               if(CircleIndicator.this.j.isRunning()) {
                  CircleIndicator.this.j.end();
                  CircleIndicator.this.j.cancel();
               }

               if(CircleIndicator.this.i.isRunning()) {
                  CircleIndicator.this.i.end();
                  CircleIndicator.this.i.cancel();
               }

               View var2;
               if(CircleIndicator.this.m >= 0) {
                  var2 = CircleIndicator.this.getChildAt(CircleIndicator.this.m);
                  if(var2 != null) {
                     var2.setBackgroundResource(CircleIndicator.this.h);
                     CircleIndicator.this.j.setTarget(var2);
                     CircleIndicator.this.j.start();
                  }
               }

               var2 = CircleIndicator.this.getChildAt(var1);
               if(var2 != null) {
                  var2.setBackgroundResource(CircleIndicator.this.g);
                  CircleIndicator.this.i.setTarget(var2);
                  CircleIndicator.this.i.start();
               }

               CircleIndicator.this.m = var1;
            }

         }
      };
      this.o = new DataSetObserver() {
         public void onChanged() {
            super.onChanged();
            if(CircleIndicator.this.a != null) {
               int var1 = CircleIndicator.this.a.getAdapter().b();
               if(var1 != CircleIndicator.this.getChildCount()) {
                  if(CircleIndicator.this.m < var1) {
                     CircleIndicator.this.m = CircleIndicator.this.a.getCurrentItem();
                  } else {
                     CircleIndicator.this.m = -1;
                  }

                  CircleIndicator.this.a();
               }
            }

         }
      };
      this.a(var1, var2);
   }

   @TargetApi(21)
   public CircleIndicator(Context var1, AttributeSet var2, int var3, int var4) {
      super(var1, var2, var3, var4);
      this.e = a.a.scale_with_alpha;
      this.f = 0;
      this.g = a.b.white_radius;
      this.h = a.b.white_radius;
      this.m = -1;
      this.n = new f() {
         public void a(int var1) {
         }

         public void a(int var1, float var2, int var3) {
         }

         public void b(int var1) {
            if(CircleIndicator.this.a.getAdapter() != null && CircleIndicator.this.a.getAdapter().b() > 0) {
               if(CircleIndicator.this.j.isRunning()) {
                  CircleIndicator.this.j.end();
                  CircleIndicator.this.j.cancel();
               }

               if(CircleIndicator.this.i.isRunning()) {
                  CircleIndicator.this.i.end();
                  CircleIndicator.this.i.cancel();
               }

               View var2;
               if(CircleIndicator.this.m >= 0) {
                  var2 = CircleIndicator.this.getChildAt(CircleIndicator.this.m);
                  if(var2 != null) {
                     var2.setBackgroundResource(CircleIndicator.this.h);
                     CircleIndicator.this.j.setTarget(var2);
                     CircleIndicator.this.j.start();
                  }
               }

               var2 = CircleIndicator.this.getChildAt(var1);
               if(var2 != null) {
                  var2.setBackgroundResource(CircleIndicator.this.g);
                  CircleIndicator.this.i.setTarget(var2);
                  CircleIndicator.this.i.start();
               }

               CircleIndicator.this.m = var1;
            }

         }
      };
      this.o = new DataSetObserver() {
         public void onChanged() {
            super.onChanged();
            if(CircleIndicator.this.a != null) {
               int var1 = CircleIndicator.this.a.getAdapter().b();
               if(var1 != CircleIndicator.this.getChildCount()) {
                  if(CircleIndicator.this.m < var1) {
                     CircleIndicator.this.m = CircleIndicator.this.a.getCurrentItem();
                  } else {
                     CircleIndicator.this.m = -1;
                  }

                  CircleIndicator.this.a();
               }
            }

         }
      };
      this.a(var1, var2);
   }

   private void a() {
      this.removeAllViews();
      int var2 = this.a.getAdapter().b();
      if(var2 > 0) {
         int var4 = this.a.getCurrentItem();
         int var3 = this.getOrientation();

         for(int var1 = 0; var1 < var2; ++var1) {
            if(var4 == var1) {
               this.a(var3, this.g, this.k);
            } else {
               this.a(var3, this.h, this.l);
            }
         }
      }

   }

   private void a(int var1, int var2, Animator var3) {
      if(var3.isRunning()) {
         var3.end();
         var3.cancel();
      }

      View var4 = new View(this.getContext());
      var4.setBackgroundResource(var2);
      this.addView(var4, this.c, this.d);
      LayoutParams var5 = (LayoutParams)var4.getLayoutParams();
      if(var1 == 0) {
         var5.leftMargin = this.b;
         var5.rightMargin = this.b;
      } else {
         var5.topMargin = this.b;
         var5.bottomMargin = this.b;
      }

      var4.setLayoutParams(var5);
      var3.setTarget(var4);
      var3.start();
   }

   private void a(Context var1) {
      int var2;
      if(this.c < 0) {
         var2 = this.a(5.0F);
      } else {
         var2 = this.c;
      }

      this.c = var2;
      if(this.d < 0) {
         var2 = this.a(5.0F);
      } else {
         var2 = this.d;
      }

      this.d = var2;
      if(this.b < 0) {
         var2 = this.a(5.0F);
      } else {
         var2 = this.b;
      }

      this.b = var2;
      if(this.e == 0) {
         var2 = a.a.scale_with_alpha;
      } else {
         var2 = this.e;
      }

      this.e = var2;
      this.i = this.b(var1);
      this.k = this.b(var1);
      this.k.setDuration(0L);
      this.j = this.c(var1);
      this.l = this.c(var1);
      this.l.setDuration(0L);
      if(this.g == 0) {
         var2 = a.b.white_radius;
      } else {
         var2 = this.g;
      }

      this.g = var2;
      if(this.h == 0) {
         var2 = this.g;
      } else {
         var2 = this.h;
      }

      this.h = var2;
   }

   private void a(Context var1, AttributeSet var2) {
      this.b(var1, var2);
      this.a(var1);
   }

   private Animator b(Context var1) {
      return AnimatorInflater.loadAnimator(var1, this.e);
   }

   private void b(Context var1, AttributeSet var2) {
      byte var3 = 1;
      if(var2 != null) {
         TypedArray var4 = var1.obtainStyledAttributes(var2, a.c.CircleIndicator);
         this.c = var4.getDimensionPixelSize(a.c.CircleIndicator_ci_width, -1);
         this.d = var4.getDimensionPixelSize(a.c.CircleIndicator_ci_height, -1);
         this.b = var4.getDimensionPixelSize(a.c.CircleIndicator_ci_margin, -1);
         this.e = var4.getResourceId(a.c.CircleIndicator_ci_animator, a.a.scale_with_alpha);
         this.f = var4.getResourceId(a.c.CircleIndicator_ci_animator_reverse, 0);
         this.g = var4.getResourceId(a.c.CircleIndicator_ci_drawable, a.b.white_radius);
         this.h = var4.getResourceId(a.c.CircleIndicator_ci_drawable_unselected, this.g);
         if(var4.getInt(a.c.CircleIndicator_ci_orientation, -1) != 1) {
            var3 = 0;
         }

         this.setOrientation(var3);
         int var5 = var4.getInt(a.c.CircleIndicator_ci_gravity, -1);
         if(var5 < 0) {
            var5 = 17;
         }

         this.setGravity(var5);
         var4.recycle();
      }

   }

   private Animator c(Context var1) {
      Animator var2;
      if(this.f == 0) {
         var2 = AnimatorInflater.loadAnimator(var1, this.e);
         var2.setInterpolator(new CircleIndicator.a(null));
      } else {
         var2 = AnimatorInflater.loadAnimator(var1, this.f);
      }

      return var2;
   }

   public int a(float var1) {
      return (int)(this.getResources().getDisplayMetrics().density * var1 + 0.5F);
   }

   public DataSetObserver getDataSetObserver() {
      return this.o;
   }

   @Deprecated
   public void setOnPageChangeListener(f var1) {
      if(this.a == null) {
         throw new NullPointerException("can not find Viewpager , setViewPager first");
      } else {
         this.a.b(var1);
         this.a.a(var1);
      }
   }

   public void setViewPager(ViewPager var1) {
      this.a = var1;
      if(this.a != null && this.a.getAdapter() != null) {
         this.m = -1;
         this.a();
         this.a.b(this.n);
         this.a.a(this.n);
         this.n.b(this.a.getCurrentItem());
      }

   }

   private class a implements Interpolator {
      private a() {
      }

      // $FF: synthetic method
      a(Object var2) {
         this();
      }

      public float getInterpolation(float var1) {
         return Math.abs(1.0F - var1);
      }
   }
}
