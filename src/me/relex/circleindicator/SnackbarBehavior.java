package me.relex.circleindicator;

import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar.SnackbarLayout;
import android.support.v4.view.s;
import android.util.AttributeSet;
import android.view.View;
import java.util.List;

public class SnackbarBehavior extends android.support.design.widget.CoordinatorLayout.a {
   public SnackbarBehavior() {
   }

   public SnackbarBehavior(Context var1, AttributeSet var2) {
      super(var1, var2);
   }

   private float a(CoordinatorLayout var1, CircleIndicator var2) {
      float var3 = 0.0F;
      List var7 = var1.c(var2);
      int var5 = var7.size();

      for(int var4 = 0; var4 < var5; ++var4) {
         View var6 = (View)var7.get(var4);
         if(var6 instanceof SnackbarLayout && var1.a(var2, var6)) {
            var3 = Math.min(var3, s.i(var6) - (float)var6.getHeight());
         }
      }

      return var3;
   }

   public boolean a(CoordinatorLayout var1, CircleIndicator var2, View var3) {
      return var3 instanceof SnackbarLayout;
   }

   public boolean b(CoordinatorLayout var1, CircleIndicator var2, View var3) {
      var2.setTranslationY(this.a(var1, var2));
      return true;
   }
}
