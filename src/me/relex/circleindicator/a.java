package me.relex.circleindicator;

public final class a {
   public static final class a {
      public static final int scale_with_alpha = 2131165186;
   }

   public static final class b {
      public static final int white_radius = 2130838035;
   }

   public static final class c {
      public static final int[] CircleIndicator = new int[]{2130772252, 2130772253, 2130772254, 2130772255, 2130772256, 2130772257, 2130772258, 2130772259, 2130772260};
      public static final int CircleIndicator_ci_animator = 3;
      public static final int CircleIndicator_ci_animator_reverse = 4;
      public static final int CircleIndicator_ci_drawable = 5;
      public static final int CircleIndicator_ci_drawable_unselected = 6;
      public static final int CircleIndicator_ci_gravity = 8;
      public static final int CircleIndicator_ci_height = 1;
      public static final int CircleIndicator_ci_margin = 2;
      public static final int CircleIndicator_ci_orientation = 7;
      public static final int CircleIndicator_ci_width = 0;
   }
}
