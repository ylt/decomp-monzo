package d.a;

import android.util.Log;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class a {
   static volatile a.b[] a;
   private static final a.b[] b = new a.b[0];
   private static final List c = new ArrayList();
   private static final a.b d;

   static {
      a = b;
      d = new a.b() {
         protected void a(int var1, String var2, String var3, Throwable var4) {
            throw new AssertionError("Missing override for log method.");
         }

         public void a(String var1, Object... var2) {
            a.b[] var5 = a.a;
            int var3 = 0;

            for(int var4 = var5.length; var3 < var4; ++var3) {
               var5[var3].a(var1, var2);
            }

         }

         public void a(Throwable var1) {
            a.b[] var4 = a.a;
            int var2 = 0;

            for(int var3 = var4.length; var2 < var3; ++var2) {
               var4[var2].a(var1);
            }

         }

         public void a(Throwable var1, String var2, Object... var3) {
            a.b[] var6 = a.a;
            int var4 = 0;

            for(int var5 = var6.length; var4 < var5; ++var4) {
               var6[var4].a(var1, var2, var3);
            }

         }

         public void b(String var1, Object... var2) {
            a.b[] var5 = a.a;
            int var3 = 0;

            for(int var4 = var5.length; var3 < var4; ++var3) {
               var5[var3].b(var1, var2);
            }

         }

         public void c(String var1, Object... var2) {
            a.b[] var5 = a.a;
            int var3 = 0;

            for(int var4 = var5.length; var3 < var4; ++var3) {
               var5[var3].c(var1, var2);
            }

         }

         public void d(String var1, Object... var2) {
            a.b[] var5 = a.a;
            int var3 = 0;

            for(int var4 = var5.length; var3 < var4; ++var3) {
               var5[var3].d(var1, var2);
            }

         }
      };
   }

   private a() {
      throw new AssertionError("No instances.");
   }

   public static void a(a.b param0) {
      // $FF: Couldn't be decompiled
   }

   public static void a(String var0, Object... var1) {
      d.d(var0, var1);
   }

   public static void a(Throwable var0) {
      d.a(var0);
   }

   public static void a(Throwable var0, String var1, Object... var2) {
      d.a(var0, var1, var2);
   }

   public static void b(String var0, Object... var1) {
      d.a(var0, var1);
   }

   public static void c(String var0, Object... var1) {
      d.b(var0, var1);
   }

   public static void d(String var0, Object... var1) {
      d.c(var0, var1);
   }

   public static class a extends a.b {
      private static final Pattern b = Pattern.compile("(\\$\\d+)+$");

      final String a() {
         String var1 = super.a();
         if(var1 == null) {
            StackTraceElement[] var2 = (new Throwable()).getStackTrace();
            if(var2.length <= 5) {
               throw new IllegalStateException("Synthetic stacktrace didn't have enough elements: are you using proguard?");
            }

            var1 = this.a(var2[5]);
         }

         return var1;
      }

      protected String a(StackTraceElement var1) {
         String var3 = var1.getClassName();
         Matcher var2 = b.matcher(var3);
         if(var2.find()) {
            var3 = var2.replaceAll("");
         }

         String var4 = var3.substring(var3.lastIndexOf(46) + 1);
         var3 = var4;
         if(var4.length() > 23) {
            var3 = var4.substring(0, 23);
         }

         return var3;
      }

      protected void a(int var1, String var2, String var3, Throwable var4) {
         if(var3.length() < 4000) {
            if(var1 == 7) {
               Log.wtf(var2, var3);
            } else {
               Log.println(var1, var2, var3);
            }
         } else {
            int var5 = 0;

            int var8;
            for(int var7 = var3.length(); var5 < var7; var5 = var8 + 1) {
               int var6 = var3.indexOf(10, var5);
               if(var6 == -1) {
                  var6 = var7;
               }

               while(true) {
                  var8 = Math.min(var6, var5 + 4000);
                  String var9 = var3.substring(var5, var8);
                  if(var1 == 7) {
                     Log.wtf(var2, var9);
                  } else {
                     Log.println(var1, var2, var9);
                  }

                  if(var8 >= var6) {
                     break;
                  }

                  var5 = var8;
               }
            }
         }

      }
   }

   public abstract static class b {
      final ThreadLocal a = new ThreadLocal();

      private void a(int var1, Throwable var2, String var3, Object... var4) {
         String var6 = this.a();
         if(this.a(var6, var1)) {
            String var5;
            if(var3 != null && var3.length() == 0) {
               var5 = null;
            } else {
               var5 = var3;
            }

            String var7;
            if(var5 == null) {
               if(var2 == null) {
                  return;
               }

               var7 = this.b(var2);
            } else {
               var3 = var5;
               if(var4.length > 0) {
                  var3 = this.e(var5, var4);
               }

               var7 = var3;
               if(var2 != null) {
                  var7 = var3 + "\n" + this.b(var2);
               }
            }

            this.a(var1, var6, var7, var2);
         }

      }

      private String b(Throwable var1) {
         StringWriter var3 = new StringWriter(256);
         PrintWriter var2 = new PrintWriter(var3, false);
         var1.printStackTrace(var2);
         var2.flush();
         return var3.toString();
      }

      String a() {
         String var1 = (String)this.a.get();
         if(var1 != null) {
            this.a.remove();
         }

         return var1;
      }

      protected abstract void a(int var1, String var2, String var3, Throwable var4);

      public void a(String var1, Object... var2) {
         this.a(4, (Throwable)null, var1, (Object[])var2);
      }

      public void a(Throwable var1) {
         this.a(6, (Throwable)var1, (String)null, (Object[])(new Object[0]));
      }

      public void a(Throwable var1, String var2, Object... var3) {
         this.a(6, (Throwable)var1, var2, (Object[])var3);
      }

      @Deprecated
      protected boolean a(int var1) {
         return true;
      }

      protected boolean a(String var1, int var2) {
         return this.a(var2);
      }

      public void b(String var1, Object... var2) {
         this.a(5, (Throwable)null, var1, (Object[])var2);
      }

      public void c(String var1, Object... var2) {
         this.a(6, (Throwable)null, var1, (Object[])var2);
      }

      public void d(String var1, Object... var2) {
         this.a(3, (Throwable)null, var1, (Object[])var2);
      }

      protected String e(String var1, Object[] var2) {
         return String.format(var1, var2);
      }
   }
}
