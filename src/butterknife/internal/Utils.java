package butterknife.internal;

import android.content.Context;
import android.content.res.Resources.NotFoundException;
import android.graphics.drawable.Drawable;
import android.support.v4.a.a.a;
import android.util.TypedValue;
import android.view.View;
import java.lang.reflect.Array;
import java.util.List;

public final class Utils {
   private static final TypedValue VALUE = new TypedValue();

   private Utils() {
      throw new AssertionError("No instances.");
   }

   @SafeVarargs
   public static Object[] arrayOf(Object... var0) {
      return filterNull(var0);
   }

   public static Object castParam(Object var0, String var1, int var2, String var3, int var4, Class var5) {
      try {
         var0 = var5.cast(var0);
         return var0;
      } catch (ClassCastException var6) {
         throw new IllegalStateException("Parameter #" + (var2 + 1) + " of method '" + var1 + "' was of the wrong type for parameter #" + (var4 + 1) + " of method '" + var3 + "'. See cause for more info.", var6);
      }
   }

   public static Object castView(View var0, int var1, String var2, Class var3) {
      try {
         Object var6 = var3.cast(var0);
         return var6;
      } catch (ClassCastException var4) {
         String var5 = getResourceEntryName(var0, var1);
         throw new IllegalStateException("View '" + var5 + "' with ID " + var1 + " for " + var2 + " was of the wrong type. See cause for more info.", var4);
      }
   }

   private static Object[] filterNull(Object[] var0) {
      int var4 = var0.length;
      int var2 = 0;

      int var1;
      for(var1 = 0; var2 < var4; ++var2) {
         Object var5 = var0[var2];
         if(var5 != null) {
            int var3 = var1 + 1;
            var0[var1] = var5;
            var1 = var3;
         }
      }

      if(var1 != var4) {
         Object[] var6 = (Object[])((Object[])Array.newInstance(var0.getClass().getComponentType(), var1));
         System.arraycopy(var0, 0, var6, 0, var1);
         var0 = var6;
      }

      return var0;
   }

   public static Object findOptionalViewAsType(View var0, int var1, String var2, Class var3) {
      return castView(var0.findViewById(var1), var1, var2, var3);
   }

   public static View findRequiredView(View var0, int var1, String var2) {
      View var3 = var0.findViewById(var1);
      if(var3 != null) {
         return var3;
      } else {
         String var4 = getResourceEntryName(var0, var1);
         throw new IllegalStateException("Required view '" + var4 + "' with ID " + var1 + " for " + var2 + " was not found. If this view is optional add '@Nullable' (fields) or '@Optional' (methods) annotation.");
      }
   }

   public static Object findRequiredViewAsType(View var0, int var1, String var2, Class var3) {
      return castView(findRequiredView(var0, var1, var2), var1, var2, var3);
   }

   public static float getFloat(Context var0, int var1) {
      TypedValue var2 = VALUE;
      var0.getResources().getValue(var1, var2, true);
      if(var2.type == 4) {
         return var2.getFloat();
      } else {
         throw new NotFoundException("Resource ID #0x" + Integer.toHexString(var1) + " type #0x" + Integer.toHexString(var2.type) + " is not valid");
      }
   }

   private static String getResourceEntryName(View var0, int var1) {
      String var2;
      if(var0.isInEditMode()) {
         var2 = "<unavailable while editing>";
      } else {
         var2 = var0.getContext().getResources().getResourceEntryName(var1);
      }

      return var2;
   }

   public static Drawable getTintedDrawable(Context var0, int var1, int var2) {
      if(!var0.getTheme().resolveAttribute(var2, VALUE, true)) {
         throw new NotFoundException("Required tint color attribute with name " + var0.getResources().getResourceEntryName(var2) + " and attribute ID " + var2 + " was not found.");
      } else {
         Drawable var3 = a.g(android.support.v4.content.a.a(var0, var1).mutate());
         a.a(var3, android.support.v4.content.a.c(var0, VALUE.resourceId));
         return var3;
      }
   }

   @SafeVarargs
   public static List listOf(Object... var0) {
      return new ImmutableList(filterNull(var0));
   }
}
