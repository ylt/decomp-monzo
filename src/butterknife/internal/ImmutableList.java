package butterknife.internal;

import java.util.AbstractList;
import java.util.RandomAccess;

final class ImmutableList extends AbstractList implements RandomAccess {
   private final Object[] views;

   ImmutableList(Object[] var1) {
      this.views = var1;
   }

   public boolean contains(Object var1) {
      boolean var5 = false;
      Object[] var6 = this.views;
      int var3 = var6.length;
      int var2 = 0;

      boolean var4;
      while(true) {
         var4 = var5;
         if(var2 >= var3) {
            break;
         }

         if(var6[var2] == var1) {
            var4 = true;
            break;
         }

         ++var2;
      }

      return var4;
   }

   public Object get(int var1) {
      return this.views[var1];
   }

   public int size() {
      return this.views.length;
   }
}
