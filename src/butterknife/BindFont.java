package butterknife;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.CLASS)
@Target({ElementType.FIELD})
public @interface BindFont {
   @BindFont.TypefaceStyle
   int style() default 0;

   int value();

   public @interface TypefaceStyle {
   }
}
