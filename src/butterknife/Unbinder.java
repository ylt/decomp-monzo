package butterknife;

public interface Unbinder {
   Unbinder EMPTY = new Unbinder() {
      public void unbind() {
      }
   };

   void unbind();
}
