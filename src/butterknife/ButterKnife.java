package butterknife;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.util.Log;
import android.util.Property;
import android.view.View;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public final class ButterKnife {
   static final Map BINDINGS = new LinkedHashMap();
   private static final String TAG = "ButterKnife";
   private static boolean debug = false;

   private ButterKnife() {
      throw new AssertionError("No instances.");
   }

   @TargetApi(14)
   public static void apply(View var0, Property var1, Object var2) {
      var1.set(var0, var2);
   }

   public static void apply(View var0, ButterKnife.Action var1) {
      var1.apply(var0, 0);
   }

   public static void apply(View var0, ButterKnife.Setter var1, Object var2) {
      var1.set(var0, var2, 0);
   }

   @SafeVarargs
   public static void apply(View var0, ButterKnife.Action... var1) {
      int var3 = var1.length;

      for(int var2 = 0; var2 < var3; ++var2) {
         var1[var2].apply(var0, 0);
      }

   }

   @TargetApi(14)
   public static void apply(List var0, Property var1, Object var2) {
      int var3 = 0;

      for(int var4 = var0.size(); var3 < var4; ++var3) {
         var1.set(var0.get(var3), var2);
      }

   }

   public static void apply(List var0, ButterKnife.Action var1) {
      int var3 = var0.size();

      for(int var2 = 0; var2 < var3; ++var2) {
         var1.apply((View)var0.get(var2), var2);
      }

   }

   public static void apply(List var0, ButterKnife.Setter var1, Object var2) {
      int var4 = var0.size();

      for(int var3 = 0; var3 < var4; ++var3) {
         var1.set((View)var0.get(var3), var2, var3);
      }

   }

   @SafeVarargs
   public static void apply(List var0, ButterKnife.Action... var1) {
      int var4 = var0.size();

      for(int var2 = 0; var2 < var4; ++var2) {
         int var5 = var1.length;

         for(int var3 = 0; var3 < var5; ++var3) {
            var1[var3].apply((View)var0.get(var2), var2);
         }
      }

   }

   @TargetApi(14)
   public static void apply(View[] var0, Property var1, Object var2) {
      int var3 = 0;

      for(int var4 = var0.length; var3 < var4; ++var3) {
         var1.set(var0[var3], var2);
      }

   }

   public static void apply(View[] var0, ButterKnife.Action var1) {
      int var2 = 0;

      for(int var3 = var0.length; var2 < var3; ++var2) {
         var1.apply(var0[var2], var2);
      }

   }

   public static void apply(View[] var0, ButterKnife.Setter var1, Object var2) {
      int var3 = 0;

      for(int var4 = var0.length; var3 < var4; ++var3) {
         var1.set(var0[var3], var2, var3);
      }

   }

   @SafeVarargs
   public static void apply(View[] var0, ButterKnife.Action... var1) {
      int var4 = var0.length;

      for(int var2 = 0; var2 < var4; ++var2) {
         int var5 = var1.length;

         for(int var3 = 0; var3 < var5; ++var3) {
            var1[var3].apply(var0[var2], var2);
         }
      }

   }

   public static Unbinder bind(Activity var0) {
      return createBinding(var0, var0.getWindow().getDecorView());
   }

   public static Unbinder bind(Dialog var0) {
      return createBinding(var0, var0.getWindow().getDecorView());
   }

   public static Unbinder bind(View var0) {
      return createBinding(var0, var0);
   }

   public static Unbinder bind(Object var0, Activity var1) {
      return createBinding(var0, var1.getWindow().getDecorView());
   }

   public static Unbinder bind(Object var0, Dialog var1) {
      return createBinding(var0, var1.getWindow().getDecorView());
   }

   public static Unbinder bind(Object var0, View var1) {
      return createBinding(var0, var1);
   }

   private static Unbinder createBinding(Object var0, View var1) {
      Class var2 = var0.getClass();
      if(debug) {
         Log.d("ButterKnife", "Looking up binding for " + var2.getName());
      }

      Constructor var8 = findBindingConstructorForClass(var2);
      Unbinder var6;
      if(var8 == null) {
         var6 = Unbinder.EMPTY;
      } else {
         try {
            var6 = (Unbinder)var8.newInstance(new Object[]{var0, var1});
         } catch (IllegalAccessException var3) {
            throw new RuntimeException("Unable to invoke " + var8, var3);
         } catch (InstantiationException var4) {
            throw new RuntimeException("Unable to invoke " + var8, var4);
         } catch (InvocationTargetException var5) {
            Throwable var7 = var5.getCause();
            if(var7 instanceof RuntimeException) {
               throw (RuntimeException)var7;
            }

            if(var7 instanceof Error) {
               throw (Error)var7;
            }

            throw new RuntimeException("Unable to create binding instance.", var7);
         }
      }

      return var6;
   }

   private static Constructor findBindingConstructorForClass(Class param0) {
      // $FF: Couldn't be decompiled
   }

   @Deprecated
   public static View findById(Activity var0, int var1) {
      return var0.findViewById(var1);
   }

   @Deprecated
   public static View findById(Dialog var0, int var1) {
      return var0.findViewById(var1);
   }

   @Deprecated
   public static View findById(View var0, int var1) {
      return var0.findViewById(var1);
   }

   public static void setDebug(boolean var0) {
      debug = var0;
   }

   public interface Action {
      void apply(View var1, int var2);
   }

   public interface Setter {
      void set(View var1, Object var2, int var3);
   }
}
