package e.a.a.a;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.Matrix.ScaleToFit;
import android.graphics.drawable.Drawable;
import android.support.v4.view.h;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewParent;
import android.view.ViewTreeObserver;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import java.lang.ref.WeakReference;

public class d implements OnTouchListener, OnGlobalLayoutListener, e.a.a.a.a.e, c {
   static int b = 1;
   private static final boolean c = Log.isLoggable("PhotoViewAttacher", 3);
   private int A;
   private d.b B;
   private int C;
   private float D;
   private boolean E;
   private ScaleType F;
   int a;
   private Interpolator d;
   private float e;
   private float f;
   private float g;
   private boolean h;
   private boolean i;
   private WeakReference j;
   private GestureDetector k;
   private e.a.a.a.a.d l;
   private final Matrix m;
   private final Matrix n;
   private final Matrix o;
   private final RectF p;
   private final float[] q;
   private d.c r;
   private d.d s;
   private d.g t;
   private OnLongClickListener u;
   private d.e v;
   private d.f w;
   private int x;
   private int y;
   private int z;

   public d(ImageView var1) {
      this(var1, true);
   }

   public d(ImageView var1, boolean var2) {
      this.d = new AccelerateDecelerateInterpolator();
      this.a = 200;
      this.e = 1.0F;
      this.f = 1.75F;
      this.g = 3.0F;
      this.h = true;
      this.i = false;
      this.m = new Matrix();
      this.n = new Matrix();
      this.o = new Matrix();
      this.p = new RectF();
      this.q = new float[9];
      this.C = 2;
      this.F = ScaleType.FIT_CENTER;
      this.j = new WeakReference(var1);
      var1.setDrawingCacheEnabled(true);
      var1.setOnTouchListener(this);
      ViewTreeObserver var3 = var1.getViewTreeObserver();
      if(var3 != null) {
         var3.addOnGlobalLayoutListener(this);
      }

      b(var1);
      if(!var1.isInEditMode()) {
         this.l = e.a.a.a.a.f.a(var1.getContext(), this);
         this.k = new GestureDetector(var1.getContext(), new SimpleOnGestureListener() {
            public boolean onFling(MotionEvent var1, MotionEvent var2, float var3, float var4) {
               boolean var6 = false;
               boolean var5 = var6;
               if(d.this.w != null) {
                  if(d.this.g() > 1.0F) {
                     var5 = var6;
                  } else {
                     var5 = var6;
                     if(h.b(var1) <= d.b) {
                        var5 = var6;
                        if(h.b(var2) <= d.b) {
                           var5 = d.this.w.a(var1, var2, var3, var4);
                        }
                     }
                  }
               }

               return var5;
            }

            public void onLongPress(MotionEvent var1) {
               if(d.this.u != null) {
                  d.this.u.onLongClick(d.this.c());
               }

            }
         });
         this.k.setOnDoubleTapListener(new b(this));
         this.D = 0.0F;
         this.a(var2);
      }

   }

   private float a(Matrix var1, int var2) {
      var1.getValues(this.q);
      return this.q[var2];
   }

   private RectF a(Matrix var1) {
      ImageView var2 = this.c();
      RectF var3;
      if(var2 != null) {
         Drawable var4 = var2.getDrawable();
         if(var4 != null) {
            this.p.set(0.0F, 0.0F, (float)var4.getIntrinsicWidth(), (float)var4.getIntrinsicHeight());
            var1.mapRect(this.p);
            var3 = this.p;
            return var3;
         }
      }

      var3 = null;
      return var3;
   }

   private void a(Drawable var1) {
      ImageView var8 = this.c();
      if(var8 != null && var1 != null) {
         float var3 = (float)this.c(var8);
         float var2 = (float)this.d(var8);
         int var6 = var1.getIntrinsicWidth();
         int var7 = var1.getIntrinsicHeight();
         this.m.reset();
         float var5 = var3 / (float)var6;
         float var4 = var2 / (float)var7;
         if(this.F == ScaleType.CENTER) {
            this.m.postTranslate((var3 - (float)var6) / 2.0F, (var2 - (float)var7) / 2.0F);
         } else if(this.F == ScaleType.CENTER_CROP) {
            var4 = Math.max(var5, var4);
            this.m.postScale(var4, var4);
            this.m.postTranslate((var3 - (float)var6 * var4) / 2.0F, (var2 - var4 * (float)var7) / 2.0F);
         } else if(this.F == ScaleType.CENTER_INSIDE) {
            var4 = Math.min(1.0F, Math.min(var5, var4));
            this.m.postScale(var4, var4);
            this.m.postTranslate((var3 - (float)var6 * var4) / 2.0F, (var2 - var4 * (float)var7) / 2.0F);
         } else {
            RectF var9 = new RectF(0.0F, 0.0F, (float)var6, (float)var7);
            RectF var10 = new RectF(0.0F, 0.0F, var3, var2);
            if((int)this.D % 180 != 0) {
               var9 = new RectF(0.0F, 0.0F, (float)var7, (float)var6);
            }

            switch(null.a[this.F.ordinal()]) {
            case 2:
               this.m.setRectToRect(var9, var10, ScaleToFit.START);
               break;
            case 3:
               this.m.setRectToRect(var9, var10, ScaleToFit.END);
               break;
            case 4:
               this.m.setRectToRect(var9, var10, ScaleToFit.CENTER);
               break;
            case 5:
               this.m.setRectToRect(var9, var10, ScaleToFit.FILL);
            }
         }

         this.q();
      }

   }

   private static boolean a(ImageView var0) {
      boolean var1;
      if(var0 != null && var0.getDrawable() != null) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   private void b(Matrix var1) {
      ImageView var2 = this.c();
      if(var2 != null) {
         this.o();
         var2.setImageMatrix(var1);
         if(this.r != null) {
            RectF var3 = this.a(var1);
            if(var3 != null) {
               this.r.a(var3);
            }
         }
      }

   }

   private static void b(ImageView var0) {
      if(var0 != null && !(var0 instanceof c) && !ScaleType.MATRIX.equals(var0.getScaleType())) {
         var0.setScaleType(ScaleType.MATRIX);
      }

   }

   private static boolean b(ScaleType var0) {
      boolean var1;
      if(var0 == null) {
         var1 = false;
      } else {
         switch(null.a[var0.ordinal()]) {
         case 1:
            throw new IllegalArgumentException(var0.name() + " is not supported in PhotoView");
         default:
            var1 = true;
         }
      }

      return var1;
   }

   private int c(ImageView var1) {
      int var2;
      if(var1 == null) {
         var2 = 0;
      } else {
         var2 = var1.getWidth() - var1.getPaddingLeft() - var1.getPaddingRight();
      }

      return var2;
   }

   private int d(ImageView var1) {
      int var2;
      if(var1 == null) {
         var2 = 0;
      } else {
         var2 = var1.getHeight() - var1.getPaddingTop() - var1.getPaddingBottom();
      }

      return var2;
   }

   private Matrix l() {
      this.n.set(this.m);
      this.n.postConcat(this.o);
      return this.n;
   }

   private void m() {
      if(this.B != null) {
         this.B.a();
         this.B = null;
      }

   }

   private void n() {
      if(this.p()) {
         this.b(this.l());
      }

   }

   private void o() {
      ImageView var1 = this.c();
      if(var1 != null && !(var1 instanceof c) && !ScaleType.MATRIX.equals(var1.getScaleType())) {
         throw new IllegalStateException("The ImageView's ScaleType has been changed since attaching a PhotoViewAttacher. You should call setScaleType on the PhotoViewAttacher instead of on the ImageView");
      }
   }

   private boolean p() {
      float var2 = 0.0F;
      ImageView var7 = this.c();
      boolean var5;
      if(var7 == null) {
         var5 = false;
      } else {
         RectF var6 = this.a(this.l());
         if(var6 == null) {
            var5 = false;
         } else {
            float var1 = var6.height();
            float var3 = var6.width();
            int var4 = this.d(var7);
            if(var1 <= (float)var4) {
               switch(null.a[this.F.ordinal()]) {
               case 2:
                  var1 = -var6.top;
                  break;
               case 3:
                  var1 = (float)var4 - var1 - var6.top;
                  break;
               default:
                  var1 = ((float)var4 - var1) / 2.0F - var6.top;
               }
            } else if(var6.top > 0.0F) {
               var1 = -var6.top;
            } else if(var6.bottom < (float)var4) {
               var1 = (float)var4 - var6.bottom;
            } else {
               var1 = 0.0F;
            }

            var4 = this.c(var7);
            if(var3 <= (float)var4) {
               switch(null.a[this.F.ordinal()]) {
               case 2:
                  var2 = -var6.left;
                  break;
               case 3:
                  var2 = (float)var4 - var3 - var6.left;
                  break;
               default:
                  var2 = ((float)var4 - var3) / 2.0F - var6.left;
               }

               this.C = 2;
            } else if(var6.left > 0.0F) {
               this.C = 0;
               var2 = -var6.left;
            } else if(var6.right < (float)var4) {
               var2 = (float)var4 - var6.right;
               this.C = 1;
            } else {
               this.C = -1;
            }

            this.o.postTranslate(var2, var1);
            var5 = true;
         }
      }

      return var5;
   }

   private void q() {
      this.o.reset();
      this.a(this.D);
      this.b(this.l());
      this.p();
   }

   public void a() {
      if(this.j != null) {
         ImageView var1 = (ImageView)this.j.get();
         if(var1 != null) {
            ViewTreeObserver var2 = var1.getViewTreeObserver();
            if(var2 != null && var2.isAlive()) {
               var2.removeGlobalOnLayoutListener(this);
            }

            var1.setOnTouchListener((OnTouchListener)null);
            this.m();
         }

         if(this.k != null) {
            this.k.setOnDoubleTapListener((OnDoubleTapListener)null);
         }

         this.r = null;
         this.s = null;
         this.t = null;
         this.j = null;
      }

   }

   public void a(float var1) {
      this.o.postRotate(var1 % 360.0F);
      this.n();
   }

   public void a(float var1, float var2) {
      if(!this.l.a()) {
         if(c) {
            e.a.a.a.b.a.a().a("PhotoViewAttacher", String.format("onDrag: dx: %.2f. dy: %.2f", new Object[]{Float.valueOf(var1), Float.valueOf(var2)}));
         }

         ImageView var3 = this.c();
         this.o.postTranslate(var1, var2);
         this.n();
         ViewParent var4 = var3.getParent();
         if(this.h && !this.l.a() && !this.i) {
            if((this.C == 2 || this.C == 0 && var1 >= 1.0F || this.C == 1 && var1 <= -1.0F) && var4 != null) {
               var4.requestDisallowInterceptTouchEvent(false);
            }
         } else if(var4 != null) {
            var4.requestDisallowInterceptTouchEvent(true);
         }
      }

   }

   public void a(float var1, float var2, float var3) {
      if(c) {
         e.a.a.a.b.a.a().a("PhotoViewAttacher", String.format("onScale: scale: %.2f. fX: %.2f. fY: %.2f", new Object[]{Float.valueOf(var1), Float.valueOf(var2), Float.valueOf(var3)}));
      }

      if((this.g() < this.g || var1 < 1.0F) && (this.g() > this.e || var1 > 1.0F)) {
         if(this.v != null) {
            this.v.a(var1, var2, var3);
         }

         this.o.postScale(var1, var1, var2, var3);
         this.n();
      }

   }

   public void a(float var1, float var2, float var3, float var4) {
      if(c) {
         e.a.a.a.b.a.a().a("PhotoViewAttacher", "onFling. sX: " + var1 + " sY: " + var2 + " Vx: " + var3 + " Vy: " + var4);
      }

      ImageView var5 = this.c();
      this.B = new d.b(var5.getContext());
      this.B.a(this.c(var5), this.d(var5), (int)var3, (int)var4);
      var5.post(this.B);
   }

   public void a(float var1, float var2, float var3, boolean var4) {
      ImageView var5 = this.c();
      if(var5 != null) {
         if(var1 >= this.e && var1 <= this.g) {
            if(var4) {
               var5.post(new d.a(this.g(), var1, var2, var3));
            } else {
               this.o.setScale(var1, var1, var2, var3);
               this.n();
            }
         } else {
            e.a.a.a.b.a.a().b("PhotoViewAttacher", "Scale must be within the range of minScale and maxScale");
         }
      }

   }

   public void a(ScaleType var1) {
      if(b(var1) && var1 != this.F) {
         this.F = var1;
         this.j();
      }

   }

   public void a(boolean var1) {
      this.E = var1;
      this.j();
   }

   public RectF b() {
      this.p();
      return this.a(this.l());
   }

   public ImageView c() {
      ImageView var1 = null;
      if(this.j != null) {
         var1 = (ImageView)this.j.get();
      }

      if(var1 == null) {
         this.a();
         e.a.a.a.b.a.a().b("PhotoViewAttacher", "ImageView no longer exists. You should not use this PhotoViewAttacher any more.");
      }

      return var1;
   }

   public float d() {
      return this.e;
   }

   public float e() {
      return this.f;
   }

   public float f() {
      return this.g;
   }

   public float g() {
      return (float)Math.sqrt((double)((float)Math.pow((double)this.a(this.o, 0), 2.0D) + (float)Math.pow((double)this.a(this.o, 3), 2.0D)));
   }

   d.d h() {
      return this.s;
   }

   d.g i() {
      return this.t;
   }

   public void j() {
      ImageView var1 = this.c();
      if(var1 != null) {
         if(this.E) {
            b(var1);
            this.a(var1.getDrawable());
         } else {
            this.q();
         }
      }

   }

   public void onGlobalLayout() {
      ImageView var5 = this.c();
      if(var5 != null) {
         if(this.E) {
            int var3 = var5.getTop();
            int var4 = var5.getRight();
            int var1 = var5.getBottom();
            int var2 = var5.getLeft();
            if(var3 != this.x || var1 != this.z || var2 != this.A || var4 != this.y) {
               this.a(var5.getDrawable());
               this.x = var3;
               this.y = var4;
               this.z = var1;
               this.A = var2;
            }
         } else {
            this.a(var5.getDrawable());
         }
      }

   }

   @SuppressLint({"ClickableViewAccessibility"})
   public boolean onTouch(View var1, MotionEvent var2) {
      boolean var6 = false;
      if(this.E && a((ImageView)var1)) {
         ViewParent var9 = var1.getParent();
         boolean var5;
         switch(var2.getAction()) {
         case 0:
            if(var9 != null) {
               var9.requestDisallowInterceptTouchEvent(true);
            } else {
               e.a.a.a.b.a.a().b("PhotoViewAttacher", "onTouch getParent() returned null");
            }

            this.m();
            var5 = false;
            break;
         case 1:
         case 3:
            if(this.g() < this.e) {
               RectF var10 = this.b();
               if(var10 != null) {
                  var1.post(new d.a(this.g(), this.e, var10.centerX(), var10.centerY()));
                  var5 = true;
                  break;
               }
            }
         case 2:
         default:
            var5 = false;
         }

         if(this.l != null) {
            boolean var8 = this.l.a();
            var5 = this.l.b();
            boolean var7 = this.l.c(var2);
            boolean var3;
            if(!var8 && !this.l.a()) {
               var3 = true;
            } else {
               var3 = false;
            }

            boolean var4;
            if(!var5 && !this.l.b()) {
               var4 = true;
            } else {
               var4 = false;
            }

            var5 = var6;
            if(var3) {
               var5 = var6;
               if(var4) {
                  var5 = true;
               }
            }

            this.i = var5;
            var5 = var7;
         }

         var6 = var5;
         if(this.k != null) {
            var6 = var5;
            if(this.k.onTouchEvent(var2)) {
               var6 = true;
            }
         }
      } else {
         var6 = false;
      }

      return var6;
   }

   private class a implements Runnable {
      private final float b;
      private final float c;
      private final long d;
      private final float e;
      private final float f;

      public a(float var2, float var3, float var4, float var5) {
         this.b = var4;
         this.c = var5;
         this.d = System.currentTimeMillis();
         this.e = var2;
         this.f = var3;
      }

      private float a() {
         float var1 = Math.min(1.0F, (float)(System.currentTimeMillis() - this.d) * 1.0F / (float)d.this.a);
         return d.this.d.getInterpolation(var1);
      }

      public void run() {
         ImageView var3 = d.this.c();
         if(var3 != null) {
            float var1 = this.a();
            float var2 = (this.e + (this.f - this.e) * var1) / d.this.g();
            d.this.a(var2, this.b, this.c);
            if(var1 < 1.0F) {
               a.a(var3, this);
            }
         }

      }
   }

   private class b implements Runnable {
      private final e.a.a.a.c.d b;
      private int c;
      private int d;

      public b(Context var2) {
         this.b = e.a.a.a.c.d.a(var2);
      }

      public void a() {
         if(d.c) {
            e.a.a.a.b.a.a().a("PhotoViewAttacher", "Cancel Fling");
         }

         this.b.a(true);
      }

      public void a(int var1, int var2, int var3, int var4) {
         RectF var9 = d.this.b();
         if(var9 != null) {
            int var5 = Math.round(-var9.left);
            int var6;
            if((float)var1 < var9.width()) {
               var1 = Math.round(var9.width() - (float)var1);
               var6 = 0;
            } else {
               var1 = var5;
               var6 = var5;
            }

            int var7 = Math.round(-var9.top);
            int var8;
            if((float)var2 < var9.height()) {
               var2 = Math.round(var9.height() - (float)var2);
               var8 = 0;
            } else {
               var2 = var7;
               var8 = var7;
            }

            this.c = var5;
            this.d = var7;
            if(d.c) {
               e.a.a.a.b.a.a().a("PhotoViewAttacher", "fling. StartX:" + var5 + " StartY:" + var7 + " MaxX:" + var1 + " MaxY:" + var2);
            }

            if(var5 != var1 || var7 != var2) {
               this.b.a(var5, var7, var3, var4, var6, var1, var8, var2, 0, 0);
            }
         }

      }

      public void run() {
         if(!this.b.b()) {
            ImageView var3 = d.this.c();
            if(var3 != null && this.b.a()) {
               int var2 = this.b.c();
               int var1 = this.b.d();
               if(d.c) {
                  e.a.a.a.b.a.a().a("PhotoViewAttacher", "fling run(). CurrentX:" + this.c + " CurrentY:" + this.d + " NewX:" + var2 + " NewY:" + var1);
               }

               d.this.o.postTranslate((float)(this.c - var2), (float)(this.d - var1));
               d.this.b(d.this.l());
               this.c = var2;
               this.d = var1;
               a.a(var3, this);
            }
         }

      }
   }

   public interface c {
      void a(RectF var1);
   }

   public interface d {
      void a();

      void a(View var1, float var2, float var3);
   }

   public interface e {
      void a(float var1, float var2, float var3);
   }

   public interface f {
      boolean a(MotionEvent var1, MotionEvent var2, float var3, float var4);
   }

   public interface g {
      void a(View var1, float var2, float var3);
   }
}
