package e.a.a.a;

import android.annotation.TargetApi;
import android.os.Build.VERSION;
import android.view.View;

public class a {
   public static int a(int var0) {
      if(VERSION.SDK_INT >= 11) {
         var0 = c(var0);
      } else {
         var0 = b(var0);
      }

      return var0;
   }

   public static void a(View var0, Runnable var1) {
      if(VERSION.SDK_INT >= 16) {
         b(var0, var1);
      } else {
         var0.postDelayed(var1, 16L);
      }

   }

   @TargetApi(5)
   private static int b(int var0) {
      return ('\uff00' & var0) >> 8;
   }

   @TargetApi(16)
   private static void b(View var0, Runnable var1) {
      var0.postOnAnimation(var1);
   }

   @TargetApi(11)
   private static int c(int var0) {
      return ('\uff00' & var0) >> 8;
   }
}
