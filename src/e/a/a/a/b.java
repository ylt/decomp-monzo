package e.a.a.a;

import android.graphics.RectF;
import android.view.MotionEvent;
import android.view.GestureDetector.OnDoubleTapListener;
import android.widget.ImageView;

public class b implements OnDoubleTapListener {
   private d a;

   public b(d var1) {
      this.a(var1);
   }

   public void a(d var1) {
      this.a = var1;
   }

   public boolean onDoubleTap(MotionEvent var1) {
      boolean var5 = true;
      if(this.a == null) {
         var5 = false;
      } else {
         try {
            float var3 = this.a.g();
            float var2 = var1.getX();
            float var4 = var1.getY();
            if(var3 < this.a.e()) {
               this.a.a(this.a.e(), var2, var4, true);
            } else if(var3 >= this.a.e() && var3 < this.a.f()) {
               this.a.a(this.a.f(), var2, var4, true);
            } else {
               this.a.a(this.a.d(), var2, var4, true);
            }
         } catch (ArrayIndexOutOfBoundsException var6) {
            ;
         }
      }

      return var5;
   }

   public boolean onDoubleTapEvent(MotionEvent var1) {
      return false;
   }

   public boolean onSingleTapConfirmed(MotionEvent var1) {
      boolean var5 = false;
      boolean var4;
      if(this.a == null) {
         var4 = var5;
      } else {
         ImageView var6 = this.a.c();
         if(this.a.h() != null) {
            RectF var7 = this.a.b();
            if(var7 != null) {
               float var2 = var1.getX();
               float var3 = var1.getY();
               if(var7.contains(var2, var3)) {
                  var2 = (var2 - var7.left) / var7.width();
                  var3 = (var3 - var7.top) / var7.height();
                  this.a.h().a(var6, var2, var3);
                  var4 = true;
                  return var4;
               }

               this.a.h().a();
            }
         }

         var4 = var5;
         if(this.a.i() != null) {
            this.a.i().a(var6, var1.getX(), var1.getY());
            var4 = var5;
         }
      }

      return var4;
   }
}
