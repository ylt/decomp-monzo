package e.a.a.a.c;

import android.content.Context;
import android.widget.Scroller;

public class c extends d {
   private final Scroller a;

   public c(Context var1) {
      this.a = new Scroller(var1);
   }

   public void a(int var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9, int var10) {
      this.a.fling(var1, var2, var3, var4, var5, var6, var7, var8);
   }

   public void a(boolean var1) {
      this.a.forceFinished(var1);
   }

   public boolean a() {
      return this.a.computeScrollOffset();
   }

   public boolean b() {
      return this.a.isFinished();
   }

   public int c() {
      return this.a.getCurrX();
   }

   public int d() {
      return this.a.getCurrY();
   }
}
