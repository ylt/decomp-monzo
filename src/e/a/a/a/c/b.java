package e.a.a.a.c;

import android.annotation.TargetApi;
import android.content.Context;

@TargetApi(14)
public class b extends a {
   public b(Context var1) {
      super(var1);
   }

   public boolean a() {
      return this.a.computeScrollOffset();
   }
}
