package e.a.a.a.c;

import android.content.Context;
import android.os.Build.VERSION;

public abstract class d {
   public static d a(Context var0) {
      Object var1;
      if(VERSION.SDK_INT < 9) {
         var1 = new c(var0);
      } else if(VERSION.SDK_INT < 14) {
         var1 = new a(var0);
      } else {
         var1 = new b(var0);
      }

      return (d)var1;
   }

   public abstract void a(int var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9, int var10);

   public abstract void a(boolean var1);

   public abstract boolean a();

   public abstract boolean b();

   public abstract int c();

   public abstract int d();
}
