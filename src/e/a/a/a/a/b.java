package e.a.a.a.a;

import android.annotation.TargetApi;
import android.content.Context;
import android.view.MotionEvent;

@TargetApi(5)
public class b extends a {
   private int f = -1;
   private int g = 0;

   public b(Context var1) {
      super(var1);
   }

   float a(MotionEvent var1) {
      float var2;
      try {
         var2 = var1.getX(this.g);
      } catch (Exception var4) {
         var2 = var1.getX();
      }

      return var2;
   }

   float b(MotionEvent var1) {
      float var2;
      try {
         var2 = var1.getY(this.g);
      } catch (Exception var4) {
         var2 = var1.getY();
      }

      return var2;
   }

   public boolean c(MotionEvent var1) {
      boolean var5 = true;
      byte var3 = 0;
      int var2;
      switch(var1.getAction() & 255) {
      case 0:
         this.f = var1.getPointerId(0);
         break;
      case 1:
      case 3:
         this.f = -1;
      case 2:
      case 4:
      case 5:
      default:
         break;
      case 6:
         var2 = e.a.a.a.a.a(var1.getAction());
         if(var1.getPointerId(var2) == this.f) {
            byte var7;
            if(var2 == 0) {
               var7 = 1;
            } else {
               var7 = 0;
            }

            this.f = var1.getPointerId(var7);
            this.b = var1.getX(var7);
            this.c = var1.getY(var7);
         }
      }

      var2 = var3;
      if(this.f != -1) {
         var2 = this.f;
      }

      this.g = var1.findPointerIndex(var2);

      boolean var4;
      try {
         var4 = super.c(var1);
      } catch (IllegalArgumentException var6) {
         var4 = var5;
      }

      return var4;
   }
}
