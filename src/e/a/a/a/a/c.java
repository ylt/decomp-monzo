package e.a.a.a.a;

import android.annotation.TargetApi;
import android.content.Context;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.ScaleGestureDetector.OnScaleGestureListener;

@TargetApi(8)
public class c extends b {
   protected final ScaleGestureDetector f;

   public c(Context var1) {
      super(var1);
      this.f = new ScaleGestureDetector(var1, new OnScaleGestureListener() {
         public boolean onScale(ScaleGestureDetector var1) {
            float var2 = var1.getScaleFactor();
            boolean var3;
            if(!Float.isNaN(var2) && !Float.isInfinite(var2)) {
               c.this.a.a(var2, var1.getFocusX(), var1.getFocusY());
               var3 = true;
            } else {
               var3 = false;
            }

            return var3;
         }

         public boolean onScaleBegin(ScaleGestureDetector var1) {
            return true;
         }

         public void onScaleEnd(ScaleGestureDetector var1) {
         }
      });
   }

   public boolean a() {
      return this.f.isInProgress();
   }

   public boolean c(MotionEvent var1) {
      boolean var2;
      try {
         this.f.onTouchEvent(var1);
         var2 = super.c(var1);
      } catch (IllegalArgumentException var3) {
         var2 = true;
      }

      return var2;
   }
}
