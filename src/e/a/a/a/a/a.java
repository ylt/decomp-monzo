package e.a.a.a.a;

import android.content.Context;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.ViewConfiguration;

public class a implements d {
   protected e a;
   float b;
   float c;
   final float d;
   final float e;
   private VelocityTracker f;
   private boolean g;

   public a(Context var1) {
      ViewConfiguration var2 = ViewConfiguration.get(var1);
      this.e = (float)var2.getScaledMinimumFlingVelocity();
      this.d = (float)var2.getScaledTouchSlop();
   }

   float a(MotionEvent var1) {
      return var1.getX();
   }

   public void a(e var1) {
      this.a = var1;
   }

   public boolean a() {
      return false;
   }

   float b(MotionEvent var1) {
      return var1.getY();
   }

   public boolean b() {
      return this.g;
   }

   public boolean c(MotionEvent var1) {
      boolean var6 = false;
      float var2;
      float var3;
      switch(var1.getAction()) {
      case 0:
         this.f = VelocityTracker.obtain();
         if(this.f != null) {
            this.f.addMovement(var1);
         } else {
            e.a.a.a.b.a.a().b("CupcakeGestureDetector", "Velocity tracker is null");
         }

         this.b = this.a(var1);
         this.c = this.b(var1);
         this.g = false;
         break;
      case 1:
         if(this.g && this.f != null) {
            this.b = this.a(var1);
            this.c = this.b(var1);
            this.f.addMovement(var1);
            this.f.computeCurrentVelocity(1000);
            var3 = this.f.getXVelocity();
            var2 = this.f.getYVelocity();
            if(Math.max(Math.abs(var3), Math.abs(var2)) >= this.e) {
               this.a.a(this.b, this.c, -var3, -var2);
            }
         }

         if(this.f != null) {
            this.f.recycle();
            this.f = null;
         }
         break;
      case 2:
         var3 = this.a(var1);
         var2 = this.b(var1);
         float var4 = var3 - this.b;
         float var5 = var2 - this.c;
         if(!this.g) {
            if(Math.sqrt((double)(var4 * var4 + var5 * var5)) >= (double)this.d) {
               var6 = true;
            }

            this.g = var6;
         }

         if(this.g) {
            this.a.a(var4, var5);
            this.b = var3;
            this.c = var2;
            if(this.f != null) {
               this.f.addMovement(var1);
            }
         }
         break;
      case 3:
         if(this.f != null) {
            this.f.recycle();
            this.f = null;
         }
      }

      return true;
   }
}
