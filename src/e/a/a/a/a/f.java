package e.a.a.a.a;

import android.content.Context;
import android.os.Build.VERSION;

public final class f {
   public static d a(Context var0, e var1) {
      int var2 = VERSION.SDK_INT;
      Object var3;
      if(var2 < 5) {
         var3 = new a(var0);
      } else if(var2 < 8) {
         var3 = new b(var0);
      } else {
         var3 = new c(var0);
      }

      ((d)var3).a(var1);
      return (d)var3;
   }
}
