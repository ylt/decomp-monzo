package c;

import java.nio.charset.Charset;

final class v {
   public static final Charset a = Charset.forName("UTF-8");

   public static int a(int var0) {
      return (-16777216 & var0) >>> 24 | (16711680 & var0) >>> 8 | ('\uff00' & var0) << 8 | (var0 & 255) << 24;
   }

   public static short a(short var0) {
      int var1 = '\uffff' & var0;
      return (short)((var1 & 255) << 8 | ('\uff00' & var1) >>> 8);
   }

   public static void a(long var0, long var2, long var4) {
      if((var2 | var4) < 0L || var2 > var0 || var0 - var2 < var4) {
         throw new ArrayIndexOutOfBoundsException(String.format("size=%s offset=%s byteCount=%s", new Object[]{Long.valueOf(var0), Long.valueOf(var2), Long.valueOf(var4)}));
      }
   }

   public static void a(Throwable var0) {
      b(var0);
   }

   public static boolean a(byte[] var0, int var1, byte[] var2, int var3, int var4) {
      boolean var6 = false;
      int var5 = 0;

      while(true) {
         if(var5 >= var4) {
            var6 = true;
            break;
         }

         if(var0[var5 + var1] != var2[var5 + var3]) {
            break;
         }

         ++var5;
      }

      return var6;
   }

   private static void b(Throwable var0) throws Throwable {
      throw var0;
   }
}
