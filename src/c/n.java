package c;

import java.io.IOException;

final class n implements d {
   public final c a = new c();
   public final s b;
   boolean c;

   n(s var1) {
      if(var1 == null) {
         throw new NullPointerException("sink == null");
      } else {
         this.b = var1;
      }
   }

   public long a(t var1) throws IOException {
      if(var1 == null) {
         throw new IllegalArgumentException("source == null");
      } else {
         long var2 = 0L;

         while(true) {
            long var4 = var1.read(this.a, 8192L);
            if(var4 == -1L) {
               return var2;
            }

            var2 += var4;
            this.y();
         }
      }
   }

   public c b() {
      return this.a;
   }

   public d b(String var1) throws IOException {
      if(this.c) {
         throw new IllegalStateException("closed");
      } else {
         this.a.a(var1);
         return this.y();
      }
   }

   public d b(String var1, int var2, int var3) throws IOException {
      if(this.c) {
         throw new IllegalStateException("closed");
      } else {
         this.a.a(var1, var2, var3);
         return this.y();
      }
   }

   public d c(f var1) throws IOException {
      if(this.c) {
         throw new IllegalStateException("closed");
      } else {
         this.a.a(var1);
         return this.y();
      }
   }

   public d c(byte[] var1) throws IOException {
      if(this.c) {
         throw new IllegalStateException("closed");
      } else {
         this.a.b(var1);
         return this.y();
      }
   }

   public d c(byte[] var1, int var2, int var3) throws IOException {
      if(this.c) {
         throw new IllegalStateException("closed");
      } else {
         this.a.b(var1, var2, var3);
         return this.y();
      }
   }

   public void close() throws IOException {
      if(!this.c) {
         Throwable var2 = null;
         Throwable var1 = var2;

         label31: {
            try {
               if(this.a.b <= 0L) {
                  break label31;
               }

               this.b.write(this.a, this.a.b);
            } catch (Throwable var5) {
               var1 = var5;
               break label31;
            }

            var1 = var2;
         }

         label25: {
            try {
               this.b.close();
            } catch (Throwable var4) {
               var2 = var1;
               if(var1 == null) {
                  var2 = var4;
               }
               break label25;
            }

            var2 = var1;
         }

         this.c = true;
         if(var2 != null) {
            v.a(var2);
         }
      }

   }

   public d e() throws IOException {
      if(this.c) {
         throw new IllegalStateException("closed");
      } else {
         long var1 = this.a.a();
         if(var1 > 0L) {
            this.b.write(this.a, var1);
         }

         return this;
      }
   }

   public void flush() throws IOException {
      if(this.c) {
         throw new IllegalStateException("closed");
      } else {
         if(this.a.b > 0L) {
            this.b.write(this.a, this.a.b);
         }

         this.b.flush();
      }
   }

   public d g(int var1) throws IOException {
      if(this.c) {
         throw new IllegalStateException("closed");
      } else {
         this.a.d(var1);
         return this.y();
      }
   }

   public d h(int var1) throws IOException {
      if(this.c) {
         throw new IllegalStateException("closed");
      } else {
         this.a.c(var1);
         return this.y();
      }
   }

   public d i(int var1) throws IOException {
      if(this.c) {
         throw new IllegalStateException("closed");
      } else {
         this.a.b(var1);
         return this.y();
      }
   }

   public d m(long var1) throws IOException {
      if(this.c) {
         throw new IllegalStateException("closed");
      } else {
         this.a.l(var1);
         return this.y();
      }
   }

   public d n(long var1) throws IOException {
      if(this.c) {
         throw new IllegalStateException("closed");
      } else {
         this.a.k(var1);
         return this.y();
      }
   }

   public d o(long var1) throws IOException {
      if(this.c) {
         throw new IllegalStateException("closed");
      } else {
         this.a.j(var1);
         return this.y();
      }
   }

   public u timeout() {
      return this.b.timeout();
   }

   public String toString() {
      return "buffer(" + this.b + ")";
   }

   public void write(c var1, long var2) throws IOException {
      if(this.c) {
         throw new IllegalStateException("closed");
      } else {
         this.a.write(var1, var2);
         this.y();
      }
   }

   public d y() throws IOException {
      if(this.c) {
         throw new IllegalStateException("closed");
      } else {
         long var1 = this.a.h();
         if(var1 > 0L) {
            this.b.write(this.a, var1);
         }

         return this;
      }
   }
}
