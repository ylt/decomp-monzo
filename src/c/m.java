package c;

import java.util.AbstractList;
import java.util.RandomAccess;

public final class m extends AbstractList implements RandomAccess {
   final f[] a;

   private m(f[] var1) {
      this.a = var1;
   }

   public static m a(f... var0) {
      return new m((f[])var0.clone());
   }

   public f a(int var1) {
      return this.a[var1];
   }

   // $FF: synthetic method
   public Object get(int var1) {
      return this.a(var1);
   }

   public int size() {
      return this.a.length;
   }
}
