package c;

import java.io.IOException;

public abstract class h implements t {
   private final t delegate;

   public h(t var1) {
      if(var1 == null) {
         throw new IllegalArgumentException("delegate == null");
      } else {
         this.delegate = var1;
      }
   }

   public void close() throws IOException {
      this.delegate.close();
   }

   public final t delegate() {
      return this.delegate;
   }

   public long read(c var1, long var2) throws IOException {
      return this.delegate.read(var1, var2);
   }

   public u timeout() {
      return this.delegate.timeout();
   }

   public String toString() {
      return this.getClass().getSimpleName() + "(" + this.delegate.toString() + ")";
   }
}
