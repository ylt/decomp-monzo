package c;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.util.concurrent.TimeUnit;

public class u {
   public static final u NONE = new u() {
      public u deadlineNanoTime(long var1) {
         return this;
      }

      public void throwIfReached() throws IOException {
      }

      public u timeout(long var1, TimeUnit var3) {
         return this;
      }
   };
   private long deadlineNanoTime;
   private boolean hasDeadline;
   private long timeoutNanos;

   public u clearDeadline() {
      this.hasDeadline = false;
      return this;
   }

   public u clearTimeout() {
      this.timeoutNanos = 0L;
      return this;
   }

   public final u deadline(long var1, TimeUnit var3) {
      if(var1 <= 0L) {
         throw new IllegalArgumentException("duration <= 0: " + var1);
      } else if(var3 == null) {
         throw new IllegalArgumentException("unit == null");
      } else {
         return this.deadlineNanoTime(System.nanoTime() + var3.toNanos(var1));
      }
   }

   public long deadlineNanoTime() {
      if(!this.hasDeadline) {
         throw new IllegalStateException("No deadline");
      } else {
         return this.deadlineNanoTime;
      }
   }

   public u deadlineNanoTime(long var1) {
      this.hasDeadline = true;
      this.deadlineNanoTime = var1;
      return this;
   }

   public boolean hasDeadline() {
      return this.hasDeadline;
   }

   public void throwIfReached() throws IOException {
      if(Thread.interrupted()) {
         throw new InterruptedIOException("thread interrupted");
      } else if(this.hasDeadline && this.deadlineNanoTime - System.nanoTime() <= 0L) {
         throw new InterruptedIOException("deadline reached");
      }
   }

   public u timeout(long var1, TimeUnit var3) {
      if(var1 < 0L) {
         throw new IllegalArgumentException("timeout < 0: " + var1);
      } else if(var3 == null) {
         throw new IllegalArgumentException("unit == null");
      } else {
         this.timeoutNanos = var3.toNanos(var1);
         return this;
      }
   }

   public long timeoutNanos() {
      return this.timeoutNanos;
   }

   public final void waitUntilNotified(Object param1) throws InterruptedIOException {
      // $FF: Couldn't be decompiled
   }
}
