package c;

import java.io.UnsupportedEncodingException;

final class b {
   private static final byte[] a = new byte[]{65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 43, 47};
   private static final byte[] b = new byte[]{65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 45, 95};

   public static String a(byte[] var0) {
      return a(var0, a);
   }

   private static String a(byte[] var0, byte[] var1) {
      int var3 = 0;
      byte[] var6 = new byte[(var0.length + 2) / 3 * 4];
      int var4 = var0.length - var0.length % 3;

      int var2;
      for(var2 = 0; var3 < var4; var3 += 3) {
         int var5 = var2 + 1;
         var6[var2] = var1[(var0[var3] & 255) >> 2];
         var2 = var5 + 1;
         var6[var5] = var1[(var0[var3] & 3) << 4 | (var0[var3 + 1] & 255) >> 4];
         var5 = var2 + 1;
         var6[var2] = var1[(var0[var3 + 1] & 15) << 2 | (var0[var3 + 2] & 255) >> 6];
         var2 = var5 + 1;
         var6[var5] = var1[var0[var3 + 2] & 63];
      }

      switch(var0.length % 3) {
      case 1:
         var3 = var2 + 1;
         var6[var2] = var1[(var0[var4] & 255) >> 2];
         var2 = var3 + 1;
         var6[var3] = var1[(var0[var4] & 3) << 4];
         var3 = var2 + 1;
         var6[var2] = 61;
         var6[var3] = 61;
         break;
      case 2:
         var3 = var2 + 1;
         var6[var2] = var1[(var0[var4] & 255) >> 2];
         var2 = var3 + 1;
         var6[var3] = var1[(var0[var4] & 3) << 4 | (var0[var4 + 1] & 255) >> 4];
         var3 = var2 + 1;
         var6[var2] = var1[(var0[var4 + 1] & 15) << 2];
         var6[var3] = 61;
      }

      try {
         String var8 = new String(var6, "US-ASCII");
         return var8;
      } catch (UnsupportedEncodingException var7) {
         throw new AssertionError(var7);
      }
   }

   public static byte[] a(String var0) {
      int var5;
      for(var5 = var0.length(); var5 > 0; --var5) {
         char var1 = var0.charAt(var5 - 1);
         if(var1 != 61 && var1 != 10 && var1 != 13 && var1 != 32 && var1 != 9) {
            break;
         }
      }

      byte[] var8 = new byte[(int)((long)var5 * 6L / 8L)];
      int var6 = 0;
      int var4 = 0;
      int var3 = 0;
      int var10 = 0;

      byte[] var9;
      while(true) {
         int var11;
         if(var6 >= var5) {
            var3 %= 4;
            if(var3 == 1) {
               var9 = null;
            } else {
               if(var3 == 2) {
                  var8[var10] = (byte)(var4 << 12 >> 16);
                  var11 = var10 + 1;
               } else {
                  var11 = var10;
                  if(var3 == 3) {
                     var4 <<= 6;
                     var3 = var10 + 1;
                     var8[var10] = (byte)(var4 >> 16);
                     var11 = var3 + 1;
                     var8[var3] = (byte)(var4 >> 8);
                  }
               }

               if(var11 == var8.length) {
                  var9 = var8;
               } else {
                  var9 = new byte[var11];
                  System.arraycopy(var8, 0, var9, 0, var11);
               }
            }
            break;
         }

         label110: {
            char var2 = var0.charAt(var6);
            if(var2 >= 65 && var2 <= 90) {
               var11 = var2 - 65;
            } else if(var2 >= 97 && var2 <= 122) {
               var11 = var2 - 71;
            } else if(var2 >= 48 && var2 <= 57) {
               var11 = var2 + 4;
            } else if(var2 != 43 && var2 != 45) {
               if(var2 != 47 && var2 != 95) {
                  if(var2 != 10 && var2 != 13 && var2 != 32) {
                     if(var2 != 9) {
                        var9 = null;
                        break;
                     }

                     var11 = var4;
                  } else {
                     var11 = var4;
                  }
                  break label110;
               }

               var11 = 63;
            } else {
               var11 = 62;
            }

            var11 = (byte)var11 | var4 << 6;
            ++var3;
            if(var3 % 4 == 0) {
               int var7 = var10 + 1;
               var8[var10] = (byte)(var11 >> 16);
               var4 = var7 + 1;
               var8[var7] = (byte)(var11 >> 8);
               var10 = var4 + 1;
               var8[var4] = (byte)var11;
            }
         }

         ++var6;
         var4 = var11;
      }

      return var9;
   }
}
