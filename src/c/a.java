package c;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.util.concurrent.TimeUnit;
import javax.annotation.Nullable;

public class a extends u {
   private static final long IDLE_TIMEOUT_MILLIS;
   private static final long IDLE_TIMEOUT_NANOS;
   private static final int TIMEOUT_WRITE_SIZE = 65536;
   @Nullable
   static a head;
   private boolean inQueue;
   @Nullable
   private a next;
   private long timeoutAt;

   static {
      IDLE_TIMEOUT_MILLIS = TimeUnit.SECONDS.toMillis(60L);
      IDLE_TIMEOUT_NANOS = TimeUnit.MILLISECONDS.toNanos(IDLE_TIMEOUT_MILLIS);
   }

   @Nullable
   static a awaitTimeout() throws InterruptedException {
      Object var5 = null;
      a var4 = head.next;
      long var0;
      if(var4 == null) {
         var0 = System.nanoTime();
         a.class.wait(IDLE_TIMEOUT_MILLIS);
         var4 = (a)var5;
         if(head.next == null) {
            var4 = (a)var5;
            if(System.nanoTime() - var0 >= IDLE_TIMEOUT_NANOS) {
               var4 = head;
            }
         }
      } else {
         var0 = var4.remainingNanos(System.nanoTime());
         if(var0 > 0L) {
            long var2 = var0 / 1000000L;
            a.class.wait(var2, (int)(var0 - 1000000L * var2));
            var4 = (a)var5;
         } else {
            head.next = var4.next;
            var4.next = null;
         }
      }

      return var4;
   }

   private static boolean cancelScheduledTimeout(a param0) {
      // $FF: Couldn't be decompiled
   }

   private long remainingNanos(long var1) {
      return this.timeoutAt - var1;
   }

   private static void scheduleTimeout(a param0, long param1, boolean param3) {
      // $FF: Couldn't be decompiled
   }

   public final void enter() {
      if(this.inQueue) {
         throw new IllegalStateException("Unbalanced enter/exit");
      } else {
         long var1 = this.timeoutNanos();
         boolean var3 = this.hasDeadline();
         if(var1 != 0L || var3) {
            this.inQueue = true;
            scheduleTimeout(this, var1, var3);
         }

      }
   }

   final IOException exit(IOException var1) throws IOException {
      if(this.exit()) {
         var1 = this.newTimeoutException(var1);
      }

      return var1;
   }

   final void exit(boolean var1) throws IOException {
      if(this.exit() && var1) {
         throw this.newTimeoutException((IOException)null);
      }
   }

   public final boolean exit() {
      boolean var1 = false;
      if(this.inQueue) {
         this.inQueue = false;
         var1 = cancelScheduledTimeout(this);
      }

      return var1;
   }

   protected IOException newTimeoutException(@Nullable IOException var1) {
      InterruptedIOException var2 = new InterruptedIOException("timeout");
      if(var1 != null) {
         var2.initCause(var1);
      }

      return var2;
   }

   public final s sink(final s var1) {
      return new s() {
         public void close() throws IOException {
            a.this.enter();
            boolean var4 = false;

            try {
               var4 = true;
               var1.close();
               var4 = false;
            } catch (IOException var5) {
               throw a.this.exit(var5);
            } finally {
               if(var4) {
                  a.this.exit(false);
               }
            }

            a.this.exit(true);
         }

         public void flush() throws IOException {
            a.this.enter();
            boolean var4 = false;

            try {
               var4 = true;
               var1.flush();
               var4 = false;
            } catch (IOException var5) {
               throw a.this.exit(var5);
            } finally {
               if(var4) {
                  a.this.exit(false);
               }
            }

            a.this.exit(true);
         }

         public u timeout() {
            return a.this;
         }

         public String toString() {
            return "AsyncTimeout.sink(" + var1 + ")";
         }

         public void write(c var1x, long var2) throws IOException {
            v.a(var1x.b, 0L, var2);

            while(var2 > 0L) {
               p var8 = var1x.a;
               long var4 = 0L;

               long var6;
               while(true) {
                  var6 = var4;
                  if(var4 >= 65536L) {
                     break;
                  }

                  var4 += (long)(var1x.a.c - var1x.a.b);
                  if(var4 >= var2) {
                     var6 = var2;
                     break;
                  }

                  var8 = var8.f;
               }

               a.this.enter();
               boolean var11 = false;

               try {
                  var11 = true;
                  var1.write(var1x, var6);
                  var11 = false;
               } catch (IOException var12) {
                  throw a.this.exit(var12);
               } finally {
                  if(var11) {
                     a.this.exit(false);
                  }
               }

               var2 -= var6;
               a.this.exit(true);
            }

         }
      };
   }

   public final t source(final t var1) {
      return new t() {
         public void close() throws IOException {
            boolean var4 = false;

            try {
               var4 = true;
               var1.close();
               var4 = false;
            } catch (IOException var5) {
               throw a.this.exit(var5);
            } finally {
               if(var4) {
                  a.this.exit(false);
               }
            }

            a.this.exit(true);
         }

         public long read(c var1x, long var2) throws IOException {
            a.this.enter();
            boolean var6 = false;

            try {
               var6 = true;
               var2 = var1.read(var1x, var2);
               var6 = false;
            } catch (IOException var7) {
               throw a.this.exit(var7);
            } finally {
               if(var6) {
                  a.this.exit(false);
               }
            }

            a.this.exit(true);
            return var2;
         }

         public u timeout() {
            return a.this;
         }

         public String toString() {
            return "AsyncTimeout.source(" + var1 + ")";
         }
      };
   }

   protected void timedOut() {
   }

   private static final class a extends Thread {
      a() {
         super("Okio Watchdog");
         this.setDaemon(true);
      }

      public void run() {
         // $FF: Couldn't be decompiled
      }
   }
}
