package c;

import java.io.Closeable;
import java.io.IOException;

public interface t extends Closeable {
   void close() throws IOException;

   long read(c var1, long var2) throws IOException;

   u timeout();
}
