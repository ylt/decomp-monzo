package c;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class i extends u {
   private u a;

   public i(u var1) {
      if(var1 == null) {
         throw new IllegalArgumentException("delegate == null");
      } else {
         this.a = var1;
      }
   }

   public final i a(u var1) {
      if(var1 == null) {
         throw new IllegalArgumentException("delegate == null");
      } else {
         this.a = var1;
         return this;
      }
   }

   public final u a() {
      return this.a;
   }

   public u clearDeadline() {
      return this.a.clearDeadline();
   }

   public u clearTimeout() {
      return this.a.clearTimeout();
   }

   public long deadlineNanoTime() {
      return this.a.deadlineNanoTime();
   }

   public u deadlineNanoTime(long var1) {
      return this.a.deadlineNanoTime(var1);
   }

   public boolean hasDeadline() {
      return this.a.hasDeadline();
   }

   public void throwIfReached() throws IOException {
      this.a.throwIfReached();
   }

   public u timeout(long var1, TimeUnit var3) {
      return this.a.timeout(var1, var3);
   }

   public long timeoutNanos() {
      return this.a.timeoutNanos();
   }
}
