package c;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Nullable;

public final class l {
   static final Logger a = Logger.getLogger(l.class.getName());

   public static d a(s var0) {
      return new n(var0);
   }

   public static e a(t var0) {
      return new o(var0);
   }

   public static s a() {
      return new s() {
         public void close() throws IOException {
         }

         public void flush() throws IOException {
         }

         public u timeout() {
            return u.NONE;
         }

         public void write(c var1, long var2) throws IOException {
            var1.i(var2);
         }
      };
   }

   public static s a(OutputStream var0) {
      return a(var0, new u());
   }

   private static s a(final OutputStream var0, final u var1) {
      if(var0 == null) {
         throw new IllegalArgumentException("out == null");
      } else if(var1 == null) {
         throw new IllegalArgumentException("timeout == null");
      } else {
         return new s() {
            public void close() throws IOException {
               var0.close();
            }

            public void flush() throws IOException {
               var0.flush();
            }

            public u timeout() {
               return var1;
            }

            public String toString() {
               return "sink(" + var0 + ")";
            }

            public void write(c var1x, long var2) throws IOException {
               v.a(var1x.b, 0L, var2);

               while(var2 > 0L) {
                  var1.throwIfReached();
                  p var7 = var1x.a;
                  int var4 = (int)Math.min(var2, (long)(var7.c - var7.b));
                  var0.write(var7.a, var7.b, var4);
                  var7.b += var4;
                  long var5 = var2 - (long)var4;
                  var1x.b -= (long)var4;
                  var2 = var5;
                  if(var7.b == var7.c) {
                     var1x.a = var7.a();
                     q.a(var7);
                     var2 = var5;
                  }
               }

            }
         };
      }
   }

   public static s a(Socket var0) throws IOException {
      if(var0 == null) {
         throw new IllegalArgumentException("socket == null");
      } else {
         a var1 = c(var0);
         return var1.sink(a((OutputStream)var0.getOutputStream(), var1));
      }
   }

   public static t a(File var0) throws FileNotFoundException {
      if(var0 == null) {
         throw new IllegalArgumentException("file == null");
      } else {
         return a((InputStream)(new FileInputStream(var0)));
      }
   }

   public static t a(InputStream var0) {
      return a(var0, new u());
   }

   private static t a(final InputStream var0, final u var1) {
      if(var0 == null) {
         throw new IllegalArgumentException("in == null");
      } else if(var1 == null) {
         throw new IllegalArgumentException("timeout == null");
      } else {
         return new t() {
            public void close() throws IOException {
               var0.close();
            }

            public long read(c param1, long param2) throws IOException {
               // $FF: Couldn't be decompiled
            }

            public u timeout() {
               return var1;
            }

            public String toString() {
               return "source(" + var0 + ")";
            }
         };
      }
   }

   static boolean a(AssertionError var0) {
      boolean var1;
      if(var0.getCause() != null && var0.getMessage() != null && var0.getMessage().contains("getsockname failed")) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public static s b(File var0) throws FileNotFoundException {
      if(var0 == null) {
         throw new IllegalArgumentException("file == null");
      } else {
         return a((OutputStream)(new FileOutputStream(var0)));
      }
   }

   public static t b(Socket var0) throws IOException {
      if(var0 == null) {
         throw new IllegalArgumentException("socket == null");
      } else {
         a var1 = c(var0);
         return var1.source(a((InputStream)var0.getInputStream(), var1));
      }
   }

   private static a c(final Socket var0) {
      return new a() {
         protected IOException newTimeoutException(@Nullable IOException var1) {
            SocketTimeoutException var2 = new SocketTimeoutException("timeout");
            if(var1 != null) {
               var2.initCause(var1);
            }

            return var2;
         }

         protected void timedOut() {
            try {
               var0.close();
            } catch (Exception var2) {
               l.a.log(Level.WARNING, "Failed to close timed out socket " + var0, var2);
            } catch (AssertionError var3) {
               if(!l.a(var3)) {
                  throw var3;
               }

               l.a.log(Level.WARNING, "Failed to close timed out socket " + var0, var3);
            }

         }
      };
   }

   public static s c(File var0) throws FileNotFoundException {
      if(var0 == null) {
         throw new IllegalArgumentException("file == null");
      } else {
         return a((OutputStream)(new FileOutputStream(var0, true)));
      }
   }
}
