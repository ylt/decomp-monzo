package c;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import javax.annotation.Nullable;

public final class c implements d, e, Cloneable {
   private static final byte[] c = new byte[]{48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 97, 98, 99, 100, 101, 102};
   @Nullable
   p a;
   long b;

   private boolean a(p var1, int var2, f var3, int var4, int var5) {
      int var7 = var1.c;
      byte[] var11 = var1.a;
      p var10 = var1;

      boolean var9;
      while(true) {
         if(var4 >= var5) {
            var9 = true;
            break;
         }

         int var6 = var7;
         int var8 = var2;
         var1 = var10;
         if(var2 == var7) {
            var1 = var10.f;
            var11 = var1.a;
            var8 = var1.b;
            var6 = var1.c;
         }

         if(var11[var8] != var3.a(var4)) {
            var9 = false;
            break;
         }

         var2 = var8 + 1;
         ++var4;
         var7 = var6;
         var10 = var1;
      }

      return var9;
   }

   public int a(m var1) {
      p var4 = this.a;
      int var2;
      if(var4 == null) {
         var2 = var1.indexOf(f.b);
      } else {
         f[] var7 = var1.a;
         int var3 = var7.length;
         var2 = 0;

         while(true) {
            if(var2 >= var3) {
               var2 = -1;
               break;
            }

            f var5 = var7[var2];
            if(this.b >= (long)var5.h() && this.a(var4, var4.b, var5, 0, var5.h())) {
               try {
                  this.i((long)var5.h());
                  break;
               } catch (EOFException var6) {
                  throw new AssertionError(var6);
               }
            }

            ++var2;
         }
      }

      return var2;
   }

   public int a(byte[] var1, int var2, int var3) {
      v.a((long)var1.length, (long)var2, (long)var3);
      p var4 = this.a;
      if(var4 == null) {
         var2 = -1;
      } else {
         var3 = Math.min(var3, var4.c - var4.b);
         System.arraycopy(var4.a, var4.b, var1, var2, var3);
         var4.b += var3;
         this.b -= (long)var3;
         var2 = var3;
         if(var4.b == var4.c) {
            this.a = var4.a();
            q.a(var4);
            var2 = var3;
         }
      }

      return var2;
   }

   public long a() {
      return this.b;
   }

   public long a(byte var1) {
      return this.a(var1, 0L, Long.MAX_VALUE);
   }

   public long a(byte var1, long var2, long var4) {
      if(var2 >= 0L && var4 >= var2) {
         long var10 = var4;
         if(var4 > this.b) {
            var10 = this.b;
         }

         if(var2 == var10) {
            var2 = -1L;
         } else {
            p var13 = this.a;
            if(var13 == null) {
               var2 = -1L;
            } else {
               long var8;
               p var12;
               if(this.b - var2 < var2) {
                  var8 = this.b;

                  while(true) {
                     var4 = var8;
                     var12 = var13;
                     if(var8 <= var2) {
                        break;
                     }

                     var13 = var13.g;
                     var8 -= (long)(var13.c - var13.b);
                  }
               } else {
                  var4 = 0L;

                  while(true) {
                     var8 = (long)(var13.c - var13.b) + var4;
                     var12 = var13;
                     if(var8 >= var2) {
                        break;
                     }

                     var13 = var13.f;
                     var4 = var8;
                  }
               }

               var8 = var2;

               while(true) {
                  if(var4 >= var10) {
                     var2 = -1L;
                     break;
                  }

                  byte[] var14 = var12.a;
                  int var7 = (int)Math.min((long)var12.c, (long)var12.b + var10 - var4);

                  for(int var6 = (int)((long)var12.b + var8 - var4); var6 < var7; ++var6) {
                     if(var14[var6] == var1) {
                        var2 = (long)(var6 - var12.b) + var4;
                        return var2;
                     }
                  }

                  var8 = (long)(var12.c - var12.b) + var4;
                  var12 = var12.f;
                  var4 = var8;
               }
            }
         }

         return var2;
      } else {
         throw new IllegalArgumentException(String.format("size=%s fromIndex=%s toIndex=%s", new Object[]{Long.valueOf(this.b), Long.valueOf(var2), Long.valueOf(var4)}));
      }
   }

   public long a(f var1, long var2) {
      if(var2 < 0L) {
         throw new IllegalArgumentException("fromIndex < 0");
      } else {
         p var14 = this.a;
         if(var14 == null) {
            var2 = -1L;
         } else {
            long var9;
            long var11;
            p var13;
            if(this.b - var2 < var2) {
               var11 = this.b;

               while(true) {
                  var9 = var11;
                  var13 = var14;
                  if(var11 <= var2) {
                     break;
                  }

                  var14 = var14.g;
                  var11 -= (long)(var14.c - var14.b);
               }
            } else {
               var9 = 0L;

               while(true) {
                  var11 = (long)(var14.c - var14.b) + var9;
                  var13 = var14;
                  if(var11 >= var2) {
                     break;
                  }

                  var14 = var14.f;
                  var9 = var11;
               }
            }

            int var4;
            int var7;
            byte var8;
            byte[] var15;
            if(var1.h() != 2) {
               for(var15 = var1.j(); var9 < this.b; var2 = var9) {
                  byte[] var18 = var13.a;
                  var4 = (int)((long)var13.b + var2 - var9);

                  for(int var17 = var13.c; var4 < var17; ++var4) {
                     var8 = var18[var4];
                     var7 = var15.length;

                     for(int var16 = 0; var16 < var7; ++var16) {
                        if(var8 == var15[var16]) {
                           var2 = var9 + (long)(var4 - var13.b);
                           return var2;
                        }
                     }
                  }

                  var9 += (long)(var13.c - var13.b);
                  var13 = var13.f;
               }
            } else {
               byte var6 = var1.a((int)0);

               for(byte var5 = var1.a((int)1); var9 < this.b; var2 = var9) {
                  var15 = var13.a;
                  var4 = (int)((long)var13.b + var2 - var9);

                  for(var7 = var13.c; var4 < var7; ++var4) {
                     var8 = var15[var4];
                     if(var8 == var6 || var8 == var5) {
                        var2 = var9 + (long)(var4 - var13.b);
                        return var2;
                     }
                  }

                  var9 += (long)(var13.c - var13.b);
                  var13 = var13.f;
               }
            }

            var2 = -1L;
         }

         return var2;
      }
   }

   public long a(s var1) throws IOException {
      long var2 = this.b;
      if(var2 > 0L) {
         var1.write(this, var2);
      }

      return var2;
   }

   public long a(t var1) throws IOException {
      if(var1 == null) {
         throw new IllegalArgumentException("source == null");
      } else {
         long var2 = 0L;

         while(true) {
            long var4 = var1.read(this, 8192L);
            if(var4 == -1L) {
               return var2;
            }

            var2 += var4;
         }
      }
   }

   public c a(int var1) {
      if(var1 < 128) {
         this.b(var1);
      } else if(var1 < 2048) {
         this.b(var1 >> 6 | 192);
         this.b(var1 & 63 | 128);
      } else if(var1 < 65536) {
         if(var1 >= '\ud800' && var1 <= '\udfff') {
            this.b(63);
         } else {
            this.b(var1 >> 12 | 224);
            this.b(var1 >> 6 & 63 | 128);
            this.b(var1 & 63 | 128);
         }
      } else {
         if(var1 > 1114111) {
            throw new IllegalArgumentException("Unexpected code point: " + Integer.toHexString(var1));
         }

         this.b(var1 >> 18 | 240);
         this.b(var1 >> 12 & 63 | 128);
         this.b(var1 >> 6 & 63 | 128);
         this.b(var1 & 63 | 128);
      }

      return this;
   }

   public c a(c var1, long var2, long var4) {
      if(var1 == null) {
         throw new IllegalArgumentException("out == null");
      } else {
         v.a(this.b, var2, var4);
         if(var4 != 0L) {
            var1.b += var4;
            p var10 = this.a;

            while(true) {
               p var11 = var10;
               long var6 = var2;
               long var8 = var4;
               if(var2 < (long)(var10.c - var10.b)) {
                  while(var8 > 0L) {
                     var10 = new p(var11);
                     var10.b = (int)((long)var10.b + var6);
                     var10.c = Math.min(var10.b + (int)var8, var10.c);
                     if(var1.a == null) {
                        var10.g = var10;
                        var10.f = var10;
                        var1.a = var10;
                     } else {
                        var1.a.g.a(var10);
                     }

                     var8 -= (long)(var10.c - var10.b);
                     var11 = var11.f;
                     var6 = 0L;
                  }
                  break;
               }

               var2 -= (long)(var10.c - var10.b);
               var10 = var10.f;
            }
         }

         return this;
      }
   }

   public c a(f var1) {
      if(var1 == null) {
         throw new IllegalArgumentException("byteString == null");
      } else {
         var1.a(this);
         return this;
      }
   }

   public c a(String var1) {
      return this.a((String)var1, 0, var1.length());
   }

   public c a(String var1, int var2, int var3) {
      if(var1 == null) {
         throw new IllegalArgumentException("string == null");
      } else if(var2 < 0) {
         throw new IllegalArgumentException("beginIndex < 0: " + var2);
      } else if(var3 < var2) {
         throw new IllegalArgumentException("endIndex < beginIndex: " + var3 + " < " + var2);
      } else if(var3 > var1.length()) {
         throw new IllegalArgumentException("endIndex > string.length: " + var3 + " > " + var1.length());
      } else {
         while(true) {
            while(var2 < var3) {
               char var6 = var1.charAt(var2);
               char var4;
               int var10;
               if(var6 < 128) {
                  p var8 = this.e(1);
                  byte[] var9 = var8.a;
                  int var5 = var8.c - var2;
                  int var7 = Math.min(var3, 8192 - var5);
                  var10 = var2 + 1;
                  var9[var5 + var2] = (byte)var6;

                  for(var2 = var10; var2 < var7; ++var2) {
                     var4 = var1.charAt(var2);
                     if(var4 >= 128) {
                        break;
                     }

                     var9[var2 + var5] = (byte)var4;
                  }

                  var10 = var2 + var5 - var8.c;
                  var8.c += var10;
                  this.b += (long)var10;
               } else if(var6 < 2048) {
                  this.b(var6 >> 6 | 192);
                  this.b(var6 & 63 | 128);
                  ++var2;
               } else if(var6 >= '\ud800' && var6 <= '\udfff') {
                  if(var2 + 1 < var3) {
                     var4 = var1.charAt(var2 + 1);
                  } else {
                     var4 = 0;
                  }

                  if(var6 <= '\udbff' && var4 >= '\udc00' && var4 <= '\udfff') {
                     var10 = (var4 & -56321 | (var6 & -55297) << 10) + 65536;
                     this.b(var10 >> 18 | 240);
                     this.b(var10 >> 12 & 63 | 128);
                     this.b(var10 >> 6 & 63 | 128);
                     this.b(var10 & 63 | 128);
                     var2 += 2;
                  } else {
                     this.b(63);
                     ++var2;
                  }
               } else {
                  this.b(var6 >> 12 | 224);
                  this.b(var6 >> 6 & 63 | 128);
                  this.b(var6 & 63 | 128);
                  ++var2;
               }
            }

            return this;
         }
      }
   }

   public c a(String var1, int var2, int var3, Charset var4) {
      if(var1 == null) {
         throw new IllegalArgumentException("string == null");
      } else if(var2 < 0) {
         throw new IllegalAccessError("beginIndex < 0: " + var2);
      } else if(var3 < var2) {
         throw new IllegalArgumentException("endIndex < beginIndex: " + var3 + " < " + var2);
      } else if(var3 > var1.length()) {
         throw new IllegalArgumentException("endIndex > string.length: " + var3 + " > " + var1.length());
      } else if(var4 == null) {
         throw new IllegalArgumentException("charset == null");
      } else {
         c var5;
         if(var4.equals(v.a)) {
            var5 = this.a(var1, var2, var3);
         } else {
            byte[] var6 = var1.substring(var2, var3).getBytes(var4);
            var5 = this.b((byte[])var6, 0, var6.length);
         }

         return var5;
      }
   }

   public c a(String var1, Charset var2) {
      return this.a(var1, 0, var1.length(), var2);
   }

   public String a(long var1, Charset var3) throws EOFException {
      v.a(this.b, 0L, var1);
      if(var3 == null) {
         throw new IllegalArgumentException("charset == null");
      } else if(var1 > 2147483647L) {
         throw new IllegalArgumentException("byteCount > Integer.MAX_VALUE: " + var1);
      } else {
         String var6;
         if(var1 == 0L) {
            var6 = "";
         } else {
            p var5 = this.a;
            if((long)var5.b + var1 > (long)var5.c) {
               var6 = new String(this.h(var1), var3);
            } else {
               String var4 = new String(var5.a, var5.b, (int)var1, var3);
               var5.b = (int)((long)var5.b + var1);
               this.b -= var1;
               var6 = var4;
               if(var5.b == var5.c) {
                  this.a = var5.a();
                  q.a(var5);
                  var6 = var4;
               }
            }
         }

         return var6;
      }
   }

   public String a(Charset var1) {
      try {
         String var3 = this.a(this.b, var1);
         return var3;
      } catch (EOFException var2) {
         throw new AssertionError(var2);
      }
   }

   public void a(long var1) throws EOFException {
      if(this.b < var1) {
         throw new EOFException();
      }
   }

   public void a(c var1, long var2) throws EOFException {
      if(this.b < var2) {
         var1.write(this, this.b);
         throw new EOFException();
      } else {
         var1.write(this, var2);
      }
   }

   public void a(byte[] var1) throws EOFException {
      int var3;
      for(int var2 = 0; var2 < var1.length; var2 += var3) {
         var3 = this.a(var1, var2, var1.length - var2);
         if(var3 == -1) {
            throw new EOFException();
         }
      }

   }

   public boolean a(long var1, f var3) {
      return this.a(var1, var3, 0, var3.h());
   }

   public boolean a(long var1, f var3, int var4, int var5) {
      boolean var8 = false;
      boolean var7 = var8;
      if(var1 >= 0L) {
         var7 = var8;
         if(var4 >= 0) {
            var7 = var8;
            if(var5 >= 0) {
               var7 = var8;
               if(this.b - var1 >= (long)var5) {
                  if(var3.h() - var4 < var5) {
                     var7 = var8;
                  } else {
                     int var6 = 0;

                     while(true) {
                        if(var6 >= var5) {
                           var7 = true;
                           break;
                        }

                        var7 = var8;
                        if(this.c((long)var6 + var1) != var3.a(var4 + var6)) {
                           break;
                        }

                        ++var6;
                     }
                  }
               }
            }
         }
      }

      return var7;
   }

   int b(m var1) {
      p var5 = this.a;
      f[] var7 = var1.a;
      int var3 = var7.length;
      int var2 = 0;

      while(true) {
         if(var2 >= var3) {
            var2 = -1;
            break;
         }

         f var6 = var7[var2];
         int var4 = (int)Math.min(this.b, (long)var6.h());
         if(var4 == 0 || this.a(var5, var5.b, var6, 0, var4)) {
            break;
         }

         ++var2;
      }

      return var2;
   }

   public long b(f var1) {
      return this.a(var1, 0L);
   }

   public c b() {
      return this;
   }

   public c b(int var1) {
      p var3 = this.e(1);
      byte[] var4 = var3.a;
      int var2 = var3.c;
      var3.c = var2 + 1;
      var4[var2] = (byte)var1;
      ++this.b;
      return this;
   }

   public c b(byte[] var1) {
      if(var1 == null) {
         throw new IllegalArgumentException("source == null");
      } else {
         return this.b((byte[])var1, 0, var1.length);
      }
   }

   public c b(byte[] var1, int var2, int var3) {
      if(var1 == null) {
         throw new IllegalArgumentException("source == null");
      } else {
         v.a((long)var1.length, (long)var2, (long)var3);

         int var5;
         p var6;
         for(int var4 = var2 + var3; var2 < var4; var6.c += var5) {
            var6 = this.e(1);
            var5 = Math.min(var4 - var2, 8192 - var6.c);
            System.arraycopy(var1, var2, var6.a, var6.c, var5);
            var2 += var5;
         }

         this.b += (long)var3;
         return this;
      }
   }

   // $FF: synthetic method
   public d b(String var1) throws IOException {
      return this.a(var1);
   }

   // $FF: synthetic method
   public d b(String var1, int var2, int var3) throws IOException {
      return this.a(var1, var2, var3);
   }

   public boolean b(long var1) {
      boolean var3;
      if(this.b >= var1) {
         var3 = true;
      } else {
         var3 = false;
      }

      return var3;
   }

   public byte c(long var1) {
      v.a(this.b, var1, 1L);
      p var4 = this.a;

      while(true) {
         int var3 = var4.c - var4.b;
         if(var1 < (long)var3) {
            return var4.a[var4.b + (int)var1];
         }

         var1 -= (long)var3;
         var4 = var4.f;
      }
   }

   public c c(int var1) {
      p var4 = this.e(2);
      byte[] var5 = var4.a;
      int var3 = var4.c;
      int var2 = var3 + 1;
      var5[var3] = (byte)(var1 >>> 8 & 255);
      var5[var2] = (byte)(var1 & 255);
      var4.c = var2 + 1;
      this.b += 2L;
      return this;
   }

   // $FF: synthetic method
   public d c(f var1) throws IOException {
      return this.a(var1);
   }

   // $FF: synthetic method
   public d c(byte[] var1) throws IOException {
      return this.b(var1);
   }

   // $FF: synthetic method
   public d c(byte[] var1, int var2, int var3) throws IOException {
      return this.b(var1, var2, var3);
   }

   public OutputStream c() {
      return new OutputStream() {
         public void close() {
         }

         public void flush() {
         }

         public String toString() {
            return c.this + ".outputStream()";
         }

         public void write(int var1) {
            c.this.b((byte)var1);
         }

         public void write(byte[] var1, int var2, int var3) {
            c.this.b(var1, var2, var3);
         }
      };
   }

   // $FF: synthetic method
   public Object clone() throws CloneNotSupportedException {
      return this.w();
   }

   public void close() {
   }

   public c d() {
      return this;
   }

   public c d(int var1) {
      p var4 = this.e(4);
      byte[] var5 = var4.a;
      int var3 = var4.c;
      int var2 = var3 + 1;
      var5[var3] = (byte)(var1 >>> 24 & 255);
      var3 = var2 + 1;
      var5[var2] = (byte)(var1 >>> 16 & 255);
      var2 = var3 + 1;
      var5[var3] = (byte)(var1 >>> 8 & 255);
      var5[var2] = (byte)(var1 & 255);
      var4.c = var2 + 1;
      this.b += 4L;
      return this;
   }

   public f d(long var1) throws EOFException {
      return new f(this.h(var1));
   }

   public d e() {
      return this;
   }

   p e(int var1) {
      if(var1 >= 1 && var1 <= 8192) {
         p var2;
         p var3;
         if(this.a == null) {
            this.a = q.a();
            var3 = this.a;
            p var4 = this.a;
            var2 = this.a;
            var4.g = var2;
            var3.f = var2;
         } else {
            var3 = this.a.g;
            if(var3.c + var1 <= 8192) {
               var2 = var3;
               if(var3.e) {
                  return var2;
               }
            }

            var2 = var3.a(q.a());
         }

         return var2;
      } else {
         throw new IllegalArgumentException();
      }
   }

   public String e(long var1) throws EOFException {
      return this.a(var1, v.a);
   }

   public boolean equals(Object var1) {
      long var6 = 0L;
      boolean var5;
      if(this == var1) {
         var5 = true;
      } else if(!(var1 instanceof c)) {
         var5 = false;
      } else {
         c var12 = (c)var1;
         if(this.b != var12.b) {
            var5 = false;
         } else if(this.b == 0L) {
            var5 = true;
         } else {
            p var11 = this.a;
            p var10 = var12.a;
            int var3 = var11.b;
            int var2 = var10.b;

            while(true) {
               if(var6 >= this.b) {
                  var5 = true;
                  break;
               }

               long var8 = (long)Math.min(var11.c - var3, var10.c - var2);

               int var4;
               for(var4 = 0; (long)var4 < var8; ++var3) {
                  if(var11.a[var3] != var10.a[var2]) {
                     var5 = false;
                     return var5;
                  }

                  ++var4;
                  ++var2;
               }

               var4 = var3;
               p var13 = var11;
               if(var3 == var11.c) {
                  var13 = var11.f;
                  var4 = var13.b;
               }

               var3 = var2;
               var11 = var10;
               if(var2 == var10.c) {
                  var11 = var10.f;
                  var3 = var11.b;
               }

               var6 += var8;
               var2 = var3;
               var3 = var4;
               var10 = var11;
               var11 = var13;
            }
         }
      }

      return var5;
   }

   public f f(int var1) {
      Object var2;
      if(var1 == 0) {
         var2 = f.b;
      } else {
         var2 = new r(this, var1);
      }

      return (f)var2;
   }

   public String f(long var1) throws EOFException {
      long var3 = Long.MAX_VALUE;
      if(var1 < 0L) {
         throw new IllegalArgumentException("limit < 0: " + var1);
      } else {
         if(var1 != Long.MAX_VALUE) {
            var3 = var1 + 1L;
         }

         long var5 = this.a(10, 0L, var3);
         String var7;
         if(var5 != -1L) {
            var7 = this.g(var5);
         } else {
            if(var3 >= this.a() || this.c(var3 - 1L) != 13 || this.c(var3) != 10) {
               c var8 = new c();
               this.a(var8, 0L, Math.min(32L, this.a()));
               throw new EOFException("\\n not found: limit=" + Math.min(this.a(), var1) + " content=" + var8.q().f() + '…');
            }

            var7 = this.g(var3);
         }

         return var7;
      }
   }

   public boolean f() {
      boolean var1;
      if(this.b == 0L) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public void flush() {
   }

   // $FF: synthetic method
   public d g(int var1) throws IOException {
      return this.d(var1);
   }

   public InputStream g() {
      return new InputStream() {
         public int available() {
            return (int)Math.min(c.this.b, 2147483647L);
         }

         public void close() {
         }

         public int read() {
            int var1;
            if(c.this.b > 0L) {
               var1 = c.this.i() & 255;
            } else {
               var1 = -1;
            }

            return var1;
         }

         public int read(byte[] var1, int var2, int var3) {
            return c.this.a(var1, var2, var3);
         }

         public String toString() {
            return c.this + ".inputStream()";
         }
      };
   }

   String g(long var1) throws EOFException {
      String var3;
      if(var1 > 0L && this.c(var1 - 1L) == 13) {
         var3 = this.e(var1 - 1L);
         this.i(2L);
      } else {
         var3 = this.e(var1);
         this.i(1L);
      }

      return var3;
   }

   public long h() {
      long var3 = this.b;
      long var1;
      if(var3 == 0L) {
         var1 = 0L;
      } else {
         p var5 = this.a.g;
         var1 = var3;
         if(var5.c < 8192) {
            var1 = var3;
            if(var5.e) {
               var1 = var3 - (long)(var5.c - var5.b);
            }
         }
      }

      return var1;
   }

   // $FF: synthetic method
   public d h(int var1) throws IOException {
      return this.c(var1);
   }

   public byte[] h(long var1) throws EOFException {
      v.a(this.b, 0L, var1);
      if(var1 > 2147483647L) {
         throw new IllegalArgumentException("byteCount > Integer.MAX_VALUE: " + var1);
      } else {
         byte[] var3 = new byte[(int)var1];
         this.a(var3);
         return var3;
      }
   }

   public int hashCode() {
      p var5 = this.a;
      int var1;
      if(var5 == null) {
         var1 = 0;
      } else {
         int var3 = 1;

         p var6;
         do {
            int var2 = var5.b;
            int var4 = var5.c;

            byte var7;
            for(var1 = var3; var2 < var4; var1 = var7 + var1 * 31) {
               var7 = var5.a[var2];
               ++var2;
            }

            var6 = var5.f;
            var3 = var1;
            var5 = var6;
         } while(var6 != this.a);
      }

      return var1;
   }

   public byte i() {
      if(this.b == 0L) {
         throw new IllegalStateException("size == 0");
      } else {
         p var6 = this.a;
         int var4 = var6.b;
         int var3 = var6.c;
         byte[] var5 = var6.a;
         int var2 = var4 + 1;
         byte var1 = var5[var4];
         --this.b;
         if(var2 == var3) {
            this.a = var6.a();
            q.a(var6);
         } else {
            var6.b = var2;
         }

         return var1;
      }
   }

   // $FF: synthetic method
   public d i(int var1) throws IOException {
      return this.b(var1);
   }

   public void i(long var1) throws EOFException {
      while(var1 > 0L) {
         if(this.a == null) {
            throw new EOFException();
         }

         int var3 = (int)Math.min(var1, (long)(this.a.c - this.a.b));
         this.b -= (long)var3;
         long var4 = var1 - (long)var3;
         p var6 = this.a;
         var6.b += var3;
         var1 = var4;
         if(this.a.b == this.a.c) {
            var6 = this.a;
            this.a = var6.a();
            q.a(var6);
            var1 = var4;
         }
      }

   }

   public c j(long var1) {
      p var6 = this.e(8);
      byte[] var5 = var6.a;
      int var4 = var6.c;
      int var3 = var4 + 1;
      var5[var4] = (byte)((int)(var1 >>> 56 & 255L));
      var4 = var3 + 1;
      var5[var3] = (byte)((int)(var1 >>> 48 & 255L));
      var3 = var4 + 1;
      var5[var4] = (byte)((int)(var1 >>> 40 & 255L));
      var4 = var3 + 1;
      var5[var3] = (byte)((int)(var1 >>> 32 & 255L));
      var3 = var4 + 1;
      var5[var4] = (byte)((int)(var1 >>> 24 & 255L));
      var4 = var3 + 1;
      var5[var3] = (byte)((int)(var1 >>> 16 & 255L));
      var3 = var4 + 1;
      var5[var4] = (byte)((int)(var1 >>> 8 & 255L));
      var5[var3] = (byte)((int)(var1 & 255L));
      var6.c = var3 + 1;
      this.b += 8L;
      return this;
   }

   public short j() {
      if(this.b < 2L) {
         throw new IllegalStateException("size < 2: " + this.b);
      } else {
         p var6 = this.a;
         int var3 = var6.b;
         int var2 = var6.c;
         short var1;
         if(var2 - var3 < 2) {
            var1 = (short)((this.i() & 255) << 8 | this.i() & 255);
         } else {
            byte[] var7 = var6.a;
            int var5 = var3 + 1;
            byte var8 = var7[var3];
            int var4 = var5 + 1;
            byte var9 = var7[var5];
            this.b -= 2L;
            if(var4 == var2) {
               this.a = var6.a();
               q.a(var6);
            } else {
               var6.b = var4;
            }

            var1 = (short)((var8 & 255) << 8 | var9 & 255);
         }

         return var1;
      }
   }

   public int k() {
      if(this.b < 4L) {
         throw new IllegalStateException("size < 4: " + this.b);
      } else {
         p var7 = this.a;
         int var1 = var7.b;
         int var2 = var7.c;
         if(var2 - var1 < 4) {
            var1 = (this.i() & 255) << 24 | (this.i() & 255) << 16 | (this.i() & 255) << 8 | this.i() & 255;
         } else {
            byte[] var8 = var7.a;
            int var3 = var1 + 1;
            byte var9 = var8[var1];
            int var4 = var3 + 1;
            byte var5 = var8[var3];
            var3 = var4 + 1;
            byte var6 = var8[var4];
            var4 = var3 + 1;
            var1 = (var9 & 255) << 24 | (var5 & 255) << 16 | (var6 & 255) << 8 | var8[var3] & 255;
            this.b -= 4L;
            if(var4 == var2) {
               this.a = var7.a();
               q.a(var7);
            } else {
               var7.b = var4;
            }
         }

         return var1;
      }
   }

   public c k(long var1) {
      c var7;
      if(var1 == 0L) {
         var7 = this.b(48);
      } else {
         boolean var4;
         if(var1 < 0L) {
            var1 = -var1;
            if(var1 < 0L) {
               var7 = this.a("-9223372036854775808");
               return var7;
            }

            var4 = true;
         } else {
            var4 = false;
         }

         byte var3;
         if(var1 < 100000000L) {
            if(var1 < 10000L) {
               if(var1 < 100L) {
                  if(var1 < 10L) {
                     var3 = 1;
                  } else {
                     var3 = 2;
                  }
               } else if(var1 < 1000L) {
                  var3 = 3;
               } else {
                  var3 = 4;
               }
            } else if(var1 < 1000000L) {
               if(var1 < 100000L) {
                  var3 = 5;
               } else {
                  var3 = 6;
               }
            } else if(var1 < 10000000L) {
               var3 = 7;
            } else {
               var3 = 8;
            }
         } else if(var1 < 1000000000000L) {
            if(var1 < 10000000000L) {
               if(var1 < 1000000000L) {
                  var3 = 9;
               } else {
                  var3 = 10;
               }
            } else if(var1 < 100000000000L) {
               var3 = 11;
            } else {
               var3 = 12;
            }
         } else if(var1 < 1000000000000000L) {
            if(var1 < 10000000000000L) {
               var3 = 13;
            } else if(var1 < 100000000000000L) {
               var3 = 14;
            } else {
               var3 = 15;
            }
         } else if(var1 < 100000000000000000L) {
            if(var1 < 10000000000000000L) {
               var3 = 16;
            } else {
               var3 = 17;
            }
         } else if(var1 < 1000000000000000000L) {
            var3 = 18;
         } else {
            var3 = 19;
         }

         int var5 = var3;
         if(var4) {
            var5 = var3 + 1;
         }

         p var9 = this.e(var5);
         byte[] var8 = var9.a;

         int var10;
         for(var10 = var9.c + var5; var1 != 0L; var1 /= 10L) {
            int var6 = (int)(var1 % 10L);
            --var10;
            var8[var10] = c[var6];
         }

         if(var4) {
            var8[var10 - 1] = 45;
         }

         var9.c += var5;
         var1 = this.b;
         this.b = (long)var5 + var1;
         var7 = this;
      }

      return var7;
   }

   public long l() {
      if(this.b < 8L) {
         throw new IllegalStateException("size < 8: " + this.b);
      } else {
         p var19 = this.a;
         int var3 = var19.b;
         int var1 = var19.c;
         long var5;
         if(var1 - var3 < 8) {
            var5 = ((long)this.k() & 4294967295L) << 32 | (long)this.k() & 4294967295L;
         } else {
            byte[] var20 = var19.a;
            int var2 = var3 + 1;
            long var11 = (long)var20[var3];
            var3 = var2 + 1;
            long var9 = (long)var20[var2];
            var2 = var3 + 1;
            long var13 = (long)var20[var3];
            var3 = var2 + 1;
            long var7 = (long)var20[var2];
            int var4 = var3 + 1;
            long var17 = (long)var20[var3];
            var2 = var4 + 1;
            long var15 = (long)var20[var4];
            var3 = var2 + 1;
            var5 = (long)var20[var2];
            var2 = var3 + 1;
            var5 = (long)var20[var3] & 255L | (var9 & 255L) << 48 | (var11 & 255L) << 56 | (var13 & 255L) << 40 | (var7 & 255L) << 32 | (var17 & 255L) << 24 | (var15 & 255L) << 16 | (var5 & 255L) << 8;
            this.b -= 8L;
            if(var2 == var1) {
               this.a = var19.a();
               q.a(var19);
            } else {
               var19.b = var2;
            }
         }

         return var5;
      }
   }

   public c l(long var1) {
      c var6;
      if(var1 == 0L) {
         var6 = this.b(48);
      } else {
         int var5 = Long.numberOfTrailingZeros(Long.highestOneBit(var1)) / 4 + 1;
         p var7 = this.e(var5);
         byte[] var8 = var7.a;
         int var3 = var7.c + var5 - 1;

         for(int var4 = var7.c; var3 >= var4; --var3) {
            var8[var3] = c[(int)(15L & var1)];
            var1 >>>= 4;
         }

         var7.c += var5;
         var1 = this.b;
         this.b = (long)var5 + var1;
         var6 = this;
      }

      return var6;
   }

   // $FF: synthetic method
   public d m(long var1) throws IOException {
      return this.l(var1);
   }

   public short m() {
      return v.a(this.j());
   }

   public int n() {
      return v.a(this.k());
   }

   // $FF: synthetic method
   public d n(long var1) throws IOException {
      return this.k(var1);
   }

   public long o() {
      if(this.b == 0L) {
         throw new IllegalStateException("size == 0");
      } else {
         long var10 = 0L;
         int var5 = 0;
         boolean var6 = false;
         boolean var1 = false;
         long var12 = -7L;

         while(true) {
            p var14 = this.a;
            byte[] var15 = var14.a;
            int var3 = var14.b;
            int var7 = var14.c;
            long var8 = var10;
            int var2 = var5;
            boolean var4 = var6;
            var10 = var12;

            boolean var16;
            while(true) {
               var16 = var1;
               if(var3 >= var7) {
                  break;
               }

               byte var17 = var15[var3];
               if(var17 >= 48 && var17 <= 57) {
                  int var18 = 48 - var17;
                  if(var8 < -922337203685477580L || var8 == -922337203685477580L && (long)var18 < var10) {
                     c var19 = (new c()).k(var8).b(var17);
                     if(!var4) {
                        var19.i();
                     }

                     throw new NumberFormatException("Number too large: " + var19.r());
                  }

                  var8 = var8 * 10L + (long)var18;
               } else {
                  if(var17 != 45 || var2 != 0) {
                     if(var2 == 0) {
                        throw new NumberFormatException("Expected leading [0-9] or '-' character but was 0x" + Integer.toHexString(var17));
                     }

                     var16 = true;
                     break;
                  }

                  var4 = true;
                  --var10;
               }

               ++var3;
               ++var2;
            }

            if(var3 == var7) {
               this.a = var14.a();
               q.a(var14);
            } else {
               var14.b = var3;
            }

            if(!var16) {
               var12 = var10;
               var1 = var16;
               var6 = var4;
               var5 = var2;
               var10 = var8;
               if(this.a != null) {
                  continue;
               }
            }

            this.b -= (long)var2;
            if(!var4) {
               var8 = -var8;
            }

            return var8;
         }
      }
   }

   // $FF: synthetic method
   public d o(long var1) throws IOException {
      return this.j(var1);
   }

   public long p() {
      if(this.b == 0L) {
         throw new IllegalStateException("size == 0");
      } else {
         long var9 = 0L;
         int var1 = 0;
         boolean var2 = false;

         int var3;
         long var7;
         do {
            p var12 = this.a;
            byte[] var11 = var12.a;
            int var4 = var12.b;
            int var5 = var12.c;
            var7 = var9;
            var3 = var1;

            boolean var13;
            while(true) {
               var13 = var2;
               if(var4 >= var5) {
                  break;
               }

               byte var6 = var11[var4];
               if(var6 >= 48 && var6 <= 57) {
                  var1 = var6 - 48;
               } else if(var6 >= 97 && var6 <= 102) {
                  var1 = var6 - 97 + 10;
               } else {
                  if(var6 < 65 || var6 > 70) {
                     if(var3 == 0) {
                        throw new NumberFormatException("Expected leading [0-9a-fA-F] character but was 0x" + Integer.toHexString(var6));
                     }

                     var13 = true;
                     break;
                  }

                  var1 = var6 - 65 + 10;
               }

               if((-1152921504606846976L & var7) != 0L) {
                  c var14 = (new c()).l(var7).b(var6);
                  throw new NumberFormatException("Number too large: " + var14.r());
               }

               var9 = (long)var1;
               ++var3;
               ++var4;
               var7 = var9 | var7 << 4;
            }

            if(var4 == var5) {
               this.a = var12.a();
               q.a(var12);
            } else {
               var12.b = var4;
            }

            if(var13) {
               break;
            }

            var2 = var13;
            var1 = var3;
            var9 = var7;
         } while(this.a != null);

         this.b -= (long)var3;
         return var7;
      }
   }

   public f q() {
      return new f(this.u());
   }

   public String r() {
      try {
         String var1 = this.a(this.b, v.a);
         return var1;
      } catch (EOFException var2) {
         throw new AssertionError(var2);
      }
   }

   public long read(c var1, long var2) {
      if(var1 == null) {
         throw new IllegalArgumentException("sink == null");
      } else if(var2 < 0L) {
         throw new IllegalArgumentException("byteCount < 0: " + var2);
      } else {
         long var4;
         if(this.b == 0L) {
            var4 = -1L;
         } else {
            var4 = var2;
            if(var2 > this.b) {
               var4 = this.b;
            }

            var1.write(this, var4);
         }

         return var4;
      }
   }

   public String s() throws EOFException {
      return this.f(Long.MAX_VALUE);
   }

   public int t() throws EOFException {
      if(this.b == 0L) {
         throw new EOFException();
      } else {
         byte var4 = this.c(0L);
         int var1;
         int var2;
         byte var3;
         if((var4 & 128) == 0) {
            var2 = 0;
            var1 = var4 & 127;
            var3 = 1;
         } else if((var4 & 224) == 192) {
            var1 = var4 & 31;
            var3 = 2;
            var2 = 128;
         } else if((var4 & 240) == 224) {
            var1 = var4 & 15;
            var3 = 3;
            var2 = 2048;
         } else {
            if((var4 & 248) != 240) {
               this.i(1L);
               var1 = '�';
               return var1;
            }

            var1 = var4 & 7;
            var3 = 4;
            var2 = 65536;
         }

         if(this.b < (long)var3) {
            throw new EOFException("size < " + var3 + ": " + this.b + " (to read code point prefixed 0x" + Integer.toHexString(var4) + ")");
         } else {
            int var6 = 1;

            while(true) {
               if(var6 >= var3) {
                  this.i((long)var3);
                  if(var1 > 1114111) {
                     var1 = '�';
                  } else if(var1 >= '\ud800' && var1 <= '\udfff') {
                     var1 = '�';
                  } else if(var1 < var2) {
                     var1 = '�';
                  }
                  break;
               }

               byte var5 = this.c((long)var6);
               if((var5 & 192) != 128) {
                  this.i((long)var6);
                  var1 = '�';
                  break;
               }

               ++var6;
               var1 = var5 & 63 | var1 << 6;
            }

            return var1;
         }
      }
   }

   public u timeout() {
      return u.NONE;
   }

   public String toString() {
      return this.x().toString();
   }

   public byte[] u() {
      try {
         byte[] var1 = this.h(this.b);
         return var1;
      } catch (EOFException var2) {
         throw new AssertionError(var2);
      }
   }

   public void v() {
      try {
         this.i(this.b);
      } catch (EOFException var2) {
         throw new AssertionError(var2);
      }
   }

   public c w() {
      c var2 = new c();
      if(this.b != 0L) {
         var2.a = new p(this.a);
         p var3 = var2.a;
         p var4 = var2.a;
         p var1 = var2.a;
         var4.g = var1;
         var3.f = var1;

         for(var1 = this.a.f; var1 != this.a; var1 = var1.f) {
            var2.a.g.a(new p(var1));
         }

         var2.b = this.b;
      }

      return var2;
   }

   public void write(c var1, long var2) {
      if(var1 == null) {
         throw new IllegalArgumentException("source == null");
      } else if(var1 == this) {
         throw new IllegalArgumentException("source == this");
      } else {
         v.a(var1.b, 0L, var2);

         while(var2 > 0L) {
            long var5;
            p var7;
            if(var2 < (long)(var1.a.c - var1.a.b)) {
               if(this.a != null) {
                  var7 = this.a.g;
               } else {
                  var7 = null;
               }

               if(var7 != null && var7.e) {
                  var5 = (long)var7.c;
                  int var4;
                  if(var7.d) {
                     var4 = 0;
                  } else {
                     var4 = var7.b;
                  }

                  if(var5 + var2 - (long)var4 <= 8192L) {
                     var1.a.a(var7, (int)var2);
                     var1.b -= var2;
                     this.b += var2;
                     break;
                  }
               }

               var1.a = var1.a.a((int)var2);
            }

            var7 = var1.a;
            var5 = (long)(var7.c - var7.b);
            var1.a = var7.a();
            if(this.a == null) {
               this.a = var7;
               p var8 = this.a;
               p var9 = this.a;
               var7 = this.a;
               var9.g = var7;
               var8.f = var7;
            } else {
               this.a.g.a(var7).b();
            }

            var1.b -= var5;
            this.b += var5;
            var2 -= var5;
         }

      }
   }

   public f x() {
      if(this.b > 2147483647L) {
         throw new IllegalArgumentException("size > Integer.MAX_VALUE: " + this.b);
      } else {
         return this.f((int)this.b);
      }
   }

   // $FF: synthetic method
   public d y() throws IOException {
      return this.d();
   }
}
