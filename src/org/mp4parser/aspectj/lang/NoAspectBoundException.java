package org.mp4parser.aspectj.lang;

public class NoAspectBoundException extends RuntimeException {
   Throwable a;

   public NoAspectBoundException() {
   }

   public NoAspectBoundException(String var1, Throwable var2) {
      if(var2 != null) {
         var1 = "Exception while initializing " + var1 + ": " + var2;
      }

      super(var1);
      this.a = var2;
   }

   public Throwable getCause() {
      return this.a;
   }
}
