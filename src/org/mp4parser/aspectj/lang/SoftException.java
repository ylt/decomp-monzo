package org.mp4parser.aspectj.lang;

import java.io.PrintStream;
import java.io.PrintWriter;

public class SoftException extends RuntimeException {
   private static final boolean b;
   Throwable a;

   static {
      boolean var0 = false;

      label13: {
         try {
            Class.forName("java.nio.Buffer");
         } catch (Throwable var2) {
            break label13;
         }

         var0 = true;
      }

      b = var0;
   }

   public Throwable getCause() {
      return this.a;
   }

   public void printStackTrace() {
      this.printStackTrace(System.err);
   }

   public void printStackTrace(PrintStream var1) {
      super.printStackTrace(var1);
      Throwable var2 = this.a;
      if(!b && var2 != null) {
         var1.print("Caused by: ");
         var2.printStackTrace(var1);
      }

   }

   public void printStackTrace(PrintWriter var1) {
      super.printStackTrace(var1);
      Throwable var2 = this.a;
      if(!b && var2 != null) {
         var1.print("Caused by: ");
         var2.printStackTrace(var1);
      }

   }
}
