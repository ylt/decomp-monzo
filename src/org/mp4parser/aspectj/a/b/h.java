package org.mp4parser.aspectj.a.b;

import java.lang.reflect.Modifier;

class h {
   static h j = new h();
   static h k;
   static h l;
   boolean a = true;
   boolean b = true;
   boolean c = false;
   boolean d = false;
   boolean e = false;
   boolean f = true;
   boolean g = true;
   boolean h = true;
   int i;

   static {
      j.a = true;
      j.b = false;
      j.c = false;
      j.d = false;
      j.e = true;
      j.f = false;
      j.g = false;
      j.i = 0;
      k = new h();
      k.a = true;
      k.b = true;
      k.c = false;
      k.d = false;
      k.e = false;
      j.i = 1;
      l = new h();
      l.a = false;
      l.b = true;
      l.c = false;
      l.d = true;
      l.e = false;
      l.h = false;
      l.i = 2;
   }

   String a(int var1) {
      String var2;
      if(!this.d) {
         var2 = "";
      } else {
         var2 = Modifier.toString(var1);
         if(var2.length() == 0) {
            var2 = "";
         } else {
            var2 = var2 + " ";
         }
      }

      return var2;
   }

   public String a(Class var1) {
      return this.a(var1, var1.getName(), this.a);
   }

   public String a(Class var1, String var2) {
      return this.a(var1, var2, this.e);
   }

   String a(Class var1, String var2, boolean var3) {
      String var4;
      if(var1 == null) {
         var4 = "ANONYMOUS";
      } else if(var1.isArray()) {
         var1 = var1.getComponentType();
         var4 = this.a(var1, var1.getName(), var3) + "[]";
      } else if(var3) {
         var4 = this.b(var2).replace('$', '.');
      } else {
         var4 = var2.replace('$', '.');
      }

      return var4;
   }

   String a(String var1) {
      int var2 = var1.lastIndexOf(45);
      if(var2 != -1) {
         var1 = var1.substring(var2 + 1);
      }

      return var1;
   }

   public void a(StringBuffer var1, Class[] var2) {
      for(int var3 = 0; var3 < var2.length; ++var3) {
         if(var3 > 0) {
            var1.append(", ");
         }

         var1.append(this.a(var2[var3]));
      }

   }

   String b(String var1) {
      int var2 = var1.lastIndexOf(46);
      if(var2 != -1) {
         var1 = var1.substring(var2 + 1);
      }

      return var1;
   }

   public void b(StringBuffer var1, Class[] var2) {
      if(var2 != null) {
         if(!this.b) {
            if(var2.length == 0) {
               var1.append("()");
            } else {
               var1.append("(..)");
            }
         } else {
            var1.append("(");
            this.a(var1, var2);
            var1.append(")");
         }
      }

   }

   public void c(StringBuffer var1, Class[] var2) {
      if(this.c && var2 != null && var2.length != 0) {
         var1.append(" throws ");
         this.a(var1, var2);
      }

   }
}
