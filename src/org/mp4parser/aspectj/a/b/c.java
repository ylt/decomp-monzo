package org.mp4parser.aspectj.a.b;

class c implements org.mp4parser.aspectj.lang.b {
   Object a;
   Object b;
   Object[] c;
   org.mp4parser.aspectj.lang.a.a d;

   public c(org.mp4parser.aspectj.lang.a.a var1, Object var2, Object var3, Object[] var4) {
      this.d = var1;
      this.a = var2;
      this.b = var3;
      this.c = var4;
   }

   public Object a() {
      return this.b;
   }

   public final String toString() {
      return this.d.toString();
   }

   static class a implements org.mp4parser.aspectj.lang.a.a {
      String a;
      org.mp4parser.aspectj.lang.c b;
      org.mp4parser.aspectj.lang.reflect.d c;
      private int d;

      public a(int var1, String var2, org.mp4parser.aspectj.lang.c var3, org.mp4parser.aspectj.lang.reflect.d var4) {
         this.a = var2;
         this.b = var3;
         this.c = var4;
         this.d = var1;
      }

      public String a() {
         return this.a;
      }

      String a(h var1) {
         StringBuffer var2 = new StringBuffer();
         var2.append(var1.a(this.a()));
         var2.append("(");
         var2.append(((f)this.b()).b(var1));
         var2.append(")");
         return var2.toString();
      }

      public org.mp4parser.aspectj.lang.c b() {
         return this.b;
      }

      public final String toString() {
         return this.a(h.k);
      }
   }
}
