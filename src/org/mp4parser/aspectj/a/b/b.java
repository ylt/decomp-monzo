package org.mp4parser.aspectj.a.b;

import java.util.Hashtable;
import java.util.StringTokenizer;

public final class b {
   static Hashtable e = new Hashtable();
   static Class f;
   private static Object[] g;
   Class a;
   ClassLoader b;
   String c;
   int d;

   static {
      e.put("void", Void.TYPE);
      e.put("boolean", Boolean.TYPE);
      e.put("byte", Byte.TYPE);
      e.put("char", Character.TYPE);
      e.put("short", Short.TYPE);
      e.put("int", Integer.TYPE);
      e.put("long", Long.TYPE);
      e.put("float", Float.TYPE);
      e.put("double", Double.TYPE);
      g = new Object[0];
   }

   public b(String var1, Class var2) {
      this.c = var1;
      this.a = var2;
      this.d = 0;
      this.b = var2.getClassLoader();
   }

   static Class a(String var0) {
      try {
         Class var2 = Class.forName(var0);
         return var2;
      } catch (ClassNotFoundException var1) {
         throw new NoClassDefFoundError(var1.getMessage());
      }
   }

   static Class a(String param0, ClassLoader param1) {
      // $FF: Couldn't be decompiled
   }

   public static org.mp4parser.aspectj.lang.a a(org.mp4parser.aspectj.lang.a.a var0, Object var1, Object var2) {
      return new c(var0, var1, var2, g);
   }

   public static org.mp4parser.aspectj.lang.a a(org.mp4parser.aspectj.lang.a.a var0, Object var1, Object var2, Object var3) {
      return new c(var0, var1, var2, new Object[]{var3});
   }

   public static org.mp4parser.aspectj.lang.a a(org.mp4parser.aspectj.lang.a.a var0, Object var1, Object var2, Object var3, Object var4) {
      return new c(var0, var1, var2, new Object[]{var3, var4});
   }

   public static org.mp4parser.aspectj.lang.a a(org.mp4parser.aspectj.lang.a.a var0, Object var1, Object var2, Object[] var3) {
      return new c(var0, var1, var2, var3);
   }

   public org.mp4parser.aspectj.lang.a.a a(String var1, org.mp4parser.aspectj.lang.c var2, int var3) {
      int var4 = this.d;
      this.d = var4 + 1;
      return new c.a(var4, var1, var2, this.a(var3, -1));
   }

   public org.mp4parser.aspectj.lang.reflect.c a(String var1, String var2, String var3, String var4, String var5, String var6, String var7) {
      int var9 = Integer.parseInt(var1, 16);
      Class var11 = a(var3, this.b);
      StringTokenizer var13 = new StringTokenizer(var4, ":");
      int var10 = var13.countTokens();
      Class[] var12 = new Class[var10];

      int var8;
      for(var8 = 0; var8 < var10; ++var8) {
         var12[var8] = a(var13.nextToken(), this.b);
      }

      StringTokenizer var15 = new StringTokenizer(var5, ":");
      var10 = var15.countTokens();
      String[] var14 = new String[var10];

      for(var8 = 0; var8 < var10; ++var8) {
         var14[var8] = var15.nextToken();
      }

      var15 = new StringTokenizer(var6, ":");
      var10 = var15.countTokens();
      Class[] var16 = new Class[var10];

      for(var8 = 0; var8 < var10; ++var8) {
         var16[var8] = a(var15.nextToken(), this.b);
      }

      return new e(var9, var2, var11, var12, var14, var16, a(var7, this.b));
   }

   public org.mp4parser.aspectj.lang.reflect.d a(int var1, int var2) {
      return new g(this.a, this.c, var1);
   }
}
