package org.mp4parser.aspectj.a.b;

import java.lang.ref.SoftReference;
import java.util.StringTokenizer;

abstract class f implements org.mp4parser.aspectj.lang.c {
   private static boolean a = true;
   static String[] k = new String[0];
   static Class[] l = new Class[0];
   private String b;
   int e = -1;
   String f;
   String g;
   Class h;
   f.a i;
   ClassLoader j = null;

   f(int var1, String var2, Class var3) {
      this.e = var1;
      this.f = var2;
      this.h = var3;
   }

   private ClassLoader a() {
      if(this.j == null) {
         this.j = this.getClass().getClassLoader();
      }

      return this.j;
   }

   String a(int var1) {
      int var3 = 0;
      int var4 = this.b.indexOf(45);
      int var2 = var1;

      for(var1 = var4; var2 > 0; --var2) {
         var3 = var1 + 1;
         var1 = this.b.indexOf(45, var3);
      }

      var2 = var1;
      if(var1 == -1) {
         var2 = this.b.length();
      }

      return this.b.substring(var3, var2);
   }

   protected abstract String a(h var1);

   int b(int var1) {
      return Integer.parseInt(this.a(var1), 16);
   }

   String b(h var1) {
      String var3 = null;
      String var2 = var3;
      if(a) {
         if(this.i == null) {
            label23: {
               try {
                  f.b var5 = new f.b();
                  this.i = var5;
               } catch (Throwable var4) {
                  a = false;
                  var2 = var3;
                  break label23;
               }

               var2 = var3;
            }
         } else {
            var2 = this.i.a(var1.i);
         }
      }

      var3 = var2;
      if(var2 == null) {
         var3 = this.a(var1);
      }

      if(a) {
         this.i.a(var1.i, var3);
      }

      return var3;
   }

   Class c(int var1) {
      return b.a(this.a(var1), this.a());
   }

   public int d() {
      if(this.e == -1) {
         this.e = this.b(0);
      }

      return this.e;
   }

   Class[] d(int var1) {
      StringTokenizer var4 = new StringTokenizer(this.a(var1), ":");
      int var2 = var4.countTokens();
      Class[] var3 = new Class[var2];

      for(var1 = 0; var1 < var2; ++var1) {
         var3[var1] = b.a(var4.nextToken(), this.a());
      }

      return var3;
   }

   public String e() {
      if(this.f == null) {
         this.f = this.a(1);
      }

      return this.f;
   }

   public Class f() {
      if(this.h == null) {
         this.h = this.c(2);
      }

      return this.h;
   }

   public String g() {
      if(this.g == null) {
         this.g = this.f().getName();
      }

      return this.g;
   }

   public final String toString() {
      return this.b(h.k);
   }

   private interface a {
      String a(int var1);

      void a(int var1, String var2);
   }

   private static final class b implements f.a {
      private SoftReference a;

      public b() {
         this.b();
      }

      private String[] a() {
         return (String[])((String[])this.a.get());
      }

      private String[] b() {
         String[] var1 = new String[3];
         this.a = new SoftReference(var1);
         return var1;
      }

      public String a(int var1) {
         String[] var2 = this.a();
         String var3;
         if(var2 == null) {
            var3 = null;
         } else {
            var3 = var2[var1];
         }

         return var3;
      }

      public void a(int var1, String var2) {
         String[] var4 = this.a();
         String[] var3 = var4;
         if(var4 == null) {
            var3 = this.b();
         }

         var3[var1] = var2;
      }
   }
}
