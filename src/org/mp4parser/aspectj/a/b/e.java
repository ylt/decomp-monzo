package org.mp4parser.aspectj.a.b;

class e extends a implements org.mp4parser.aspectj.lang.reflect.c {
   Class d;

   e(int var1, String var2, Class var3, Class[] var4, String[] var5, Class[] var6, Class var7) {
      super(var1, var2, var3, var4, var5, var6);
      this.d = var7;
   }

   protected String a(h var1) {
      StringBuffer var2 = new StringBuffer();
      var2.append(var1.a(this.d()));
      if(var1.b) {
         var2.append(var1.a(this.c()));
      }

      if(var1.b) {
         var2.append(" ");
      }

      var2.append(var1.a(this.f(), this.g()));
      var2.append(".");
      var2.append(this.e());
      var1.b(var2, this.a());
      var1.c(var2, this.b());
      return var2.toString();
   }

   public Class c() {
      if(this.d == null) {
         this.d = this.c(6);
      }

      return this.d;
   }
}
