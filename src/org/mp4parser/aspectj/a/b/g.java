package org.mp4parser.aspectj.a.b;

class g implements org.mp4parser.aspectj.lang.reflect.d {
   Class a;
   String b;
   int c;

   g(Class var1, String var2, int var3) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
   }

   public String a() {
      return this.b;
   }

   public int b() {
      return this.c;
   }

   public String toString() {
      return this.a() + ":" + this.b();
   }
}
