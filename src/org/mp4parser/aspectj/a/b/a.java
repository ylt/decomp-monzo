package org.mp4parser.aspectj.a.b;

abstract class a extends d implements org.mp4parser.aspectj.lang.reflect.a {
   Class[] a;
   String[] b;
   Class[] c;

   a(int var1, String var2, Class var3, Class[] var4, String[] var5, Class[] var6) {
      super(var1, var2, var3);
      this.a = var4;
      this.b = var5;
      this.c = var6;
   }

   public Class[] a() {
      if(this.a == null) {
         this.a = this.d(3);
      }

      return this.a;
   }

   public Class[] b() {
      if(this.c == null) {
         this.c = this.d(5);
      }

      return this.c;
   }
}
