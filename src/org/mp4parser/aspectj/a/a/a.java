package org.mp4parser.aspectj.a.a;

public final class a {
   public static Object a(byte var0) {
      return new Byte(var0);
   }

   public static Object a(double var0) {
      return new Double(var0);
   }

   public static Object a(float var0) {
      return new Float(var0);
   }

   public static Object a(int var0) {
      return new Integer(var0);
   }

   public static Object a(long var0) {
      return new Long(var0);
   }

   public static Object a(short var0) {
      return new Short(var0);
   }

   public static Object a(boolean var0) {
      return new Boolean(var0);
   }
}
