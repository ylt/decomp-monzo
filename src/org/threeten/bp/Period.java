package org.threeten.bp;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;
import org.threeten.bp.chrono.ChronoPeriod;
import org.threeten.bp.jdk8.Jdk8Methods;
import org.threeten.bp.temporal.ChronoUnit;
import org.threeten.bp.temporal.Temporal;
import org.threeten.bp.temporal.TemporalUnit;
import org.threeten.bp.temporal.UnsupportedTemporalTypeException;

public final class Period extends ChronoPeriod implements Serializable {
   public static final Period a = new Period(0, 0, 0);
   private static final Pattern b = Pattern.compile("([-+]?)P(?:([-+]?[0-9]+)Y)?(?:([-+]?[0-9]+)M)?(?:([-+]?[0-9]+)W)?(?:([-+]?[0-9]+)D)?", 2);
   private final int c;
   private final int d;
   private final int e;

   private Period(int var1, int var2, int var3) {
      this.c = var1;
      this.d = var2;
      this.e = var3;
   }

   public static Period a(int var0) {
      return a(0, 0, var0);
   }

   private static Period a(int var0, int var1, int var2) {
      Period var3;
      if((var0 | var1 | var2) == 0) {
         var3 = a;
      } else {
         var3 = new Period(var0, var1, var2);
      }

      return var3;
   }

   private Object readResolve() {
      Period var1 = this;
      if((this.c | this.d | this.e) == 0) {
         var1 = a;
      }

      return var1;
   }

   public long a(TemporalUnit var1) {
      long var2;
      if(var1 == ChronoUnit.k) {
         var2 = (long)this.c;
      } else if(var1 == ChronoUnit.j) {
         var2 = (long)this.d;
      } else {
         if(var1 != ChronoUnit.h) {
            throw new UnsupportedTemporalTypeException("Unsupported unit: " + var1);
         }

         var2 = (long)this.e;
      }

      return var2;
   }

   public List a() {
      return Collections.unmodifiableList(Arrays.asList(new ChronoUnit[]{ChronoUnit.k, ChronoUnit.j, ChronoUnit.h}));
   }

   public Temporal a(Temporal var1) {
      Jdk8Methods.a(var1, (String)"temporal");
      Temporal var2;
      if(this.c != 0) {
         if(this.d != 0) {
            var2 = var1.d(this.c(), ChronoUnit.j);
         } else {
            var2 = var1.d((long)this.c, ChronoUnit.k);
         }
      } else {
         var2 = var1;
         if(this.d != 0) {
            var2 = var1.d((long)this.d, ChronoUnit.j);
         }
      }

      var1 = var2;
      if(this.e != 0) {
         var1 = var2.d((long)this.e, ChronoUnit.h);
      }

      return var1;
   }

   public boolean b() {
      boolean var1;
      if(this == a) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public long c() {
      return (long)this.c * 12L + (long)this.d;
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(this != var1) {
         if(var1 instanceof Period) {
            Period var3 = (Period)var1;
            if(this.c != var3.c || this.d != var3.d || this.e != var3.e) {
               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public int hashCode() {
      return this.c + Integer.rotateLeft(this.d, 8) + Integer.rotateLeft(this.e, 16);
   }

   public String toString() {
      String var1;
      if(this == a) {
         var1 = "P0D";
      } else {
         StringBuilder var2 = new StringBuilder();
         var2.append('P');
         if(this.c != 0) {
            var2.append(this.c).append('Y');
         }

         if(this.d != 0) {
            var2.append(this.d).append('M');
         }

         if(this.e != 0) {
            var2.append(this.e).append('D');
         }

         var1 = var2.toString();
      }

      return var1;
   }
}
