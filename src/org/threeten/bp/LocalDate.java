package org.threeten.bp;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.InvalidObjectException;
import java.io.ObjectStreamException;
import java.io.Serializable;
import org.threeten.bp.chrono.ChronoLocalDate;
import org.threeten.bp.chrono.ChronoLocalDateTime;
import org.threeten.bp.chrono.Chronology;
import org.threeten.bp.chrono.Era;
import org.threeten.bp.chrono.IsoChronology;
import org.threeten.bp.format.DateTimeFormatter;
import org.threeten.bp.jdk8.Jdk8Methods;
import org.threeten.bp.temporal.ChronoField;
import org.threeten.bp.temporal.ChronoUnit;
import org.threeten.bp.temporal.Temporal;
import org.threeten.bp.temporal.TemporalAccessor;
import org.threeten.bp.temporal.TemporalAdjuster;
import org.threeten.bp.temporal.TemporalAmount;
import org.threeten.bp.temporal.TemporalField;
import org.threeten.bp.temporal.TemporalQueries;
import org.threeten.bp.temporal.TemporalQuery;
import org.threeten.bp.temporal.TemporalUnit;
import org.threeten.bp.temporal.UnsupportedTemporalTypeException;
import org.threeten.bp.temporal.ValueRange;

public final class LocalDate extends ChronoLocalDate implements Serializable, Temporal, TemporalAdjuster {
   public static final LocalDate a = a(-999999999, 1, 1);
   public static final LocalDate b = a(999999999, 12, 31);
   public static final TemporalQuery c = new TemporalQuery() {
      public LocalDate a(TemporalAccessor var1) {
         return LocalDate.a(var1);
      }

      // $FF: synthetic method
      public Object b(TemporalAccessor var1) {
         return this.a(var1);
      }
   };
   private final int d;
   private final short e;
   private final short f;

   private LocalDate(int var1, int var2, int var3) {
      this.d = var1;
      this.e = (short)var2;
      this.f = (short)var3;
   }

   public static LocalDate a() {
      return a(Clock.a());
   }

   public static LocalDate a(int var0, int var1) {
      ChronoField.A.a((long)var0);
      ChronoField.t.a((long)var1);
      boolean var2 = IsoChronology.b.a((long)var0);
      if(var1 == 366 && !var2) {
         throw new DateTimeException("Invalid date 'DayOfYear 366' as '" + var0 + "' is not a leap year");
      } else {
         Month var4 = Month.a((var1 - 1) / 31 + 1);
         Month var3 = var4;
         if(var1 > var4.b(var2) + var4.a(var2) - 1) {
            var3 = var4.a(1L);
         }

         return b(var0, var3, var1 - var3.b(var2) + 1);
      }
   }

   public static LocalDate a(int var0, int var1, int var2) {
      ChronoField.A.a((long)var0);
      ChronoField.x.a((long)var1);
      ChronoField.s.a((long)var2);
      return b(var0, Month.a(var1), var2);
   }

   public static LocalDate a(int var0, Month var1, int var2) {
      ChronoField.A.a((long)var0);
      Jdk8Methods.a(var1, (String)"month");
      ChronoField.s.a((long)var2);
      return b(var0, var1, var2);
   }

   public static LocalDate a(long var0) {
      ChronoField.u.a(var0);
      long var7 = 719528L + var0 - 60L;
      var0 = 0L;
      long var5 = var7;
      if(var7 < 0L) {
         var5 = (1L + var7) / 146097L - 1L;
         var0 = 400L * var5;
         var5 = var7 + -var5 * 146097L;
      }

      long var9 = (400L * var5 + 591L) / 146097L;
      var7 = var5 - (365L * var9 + var9 / 4L - var9 / 100L + var9 / 400L);
      if(var7 < 0L) {
         var7 = var9 - 1L;
         var9 = var5 - (365L * var7 + var7 / 4L - var7 / 100L + var7 / 400L);
         var5 = var7;
         var7 = var9;
      } else {
         var5 = var9;
      }

      int var4 = (int)var7;
      int var2 = (var4 * 5 + 2) / 153;
      int var3 = (var2 * 306 + 5) / 10;
      var7 = (long)(var2 / 10);
      return new LocalDate(ChronoField.A.b(var0 + var5 + var7), (var2 + 2) % 12 + 1, var4 - var3 + 1);
   }

   static LocalDate a(DataInput var0) throws IOException {
      return a(var0.readInt(), var0.readByte(), var0.readByte());
   }

   public static LocalDate a(CharSequence var0) {
      return a(var0, DateTimeFormatter.a);
   }

   public static LocalDate a(CharSequence var0, DateTimeFormatter var1) {
      Jdk8Methods.a(var1, (String)"formatter");
      return (LocalDate)var1.a(var0, c);
   }

   public static LocalDate a(Clock var0) {
      Jdk8Methods.a(var0, (String)"clock");
      Instant var3 = var0.d();
      ZoneOffset var4 = var0.b().d().a(var3);
      long var1 = var3.a();
      return a(Jdk8Methods.e((long)var4.f() + var1, 86400L));
   }

   public static LocalDate a(TemporalAccessor var0) {
      LocalDate var1 = (LocalDate)var0.a(TemporalQueries.f());
      if(var1 == null) {
         throw new DateTimeException("Unable to obtain LocalDate from TemporalAccessor: " + var0 + ", type " + var0.getClass().getName());
      } else {
         return var1;
      }
   }

   private static LocalDate b(int var0, int var1, int var2) {
      int var3 = var2;
      switch(var1) {
      case 2:
         byte var4;
         if(IsoChronology.b.a((long)var0)) {
            var4 = 29;
         } else {
            var4 = 28;
         }

         var3 = Math.min(var2, var4);
      case 3:
      case 5:
      case 7:
      case 8:
      case 10:
         break;
      case 4:
      case 6:
      case 9:
      case 11:
         var3 = Math.min(var2, 30);
         break;
      default:
         var3 = var2;
      }

      return a(var0, var1, var3);
   }

   private static LocalDate b(int var0, Month var1, int var2) {
      if(var2 > 28 && var2 > var1.a(IsoChronology.b.a((long)var0))) {
         if(var2 == 29) {
            throw new DateTimeException("Invalid date 'February 29' as '" + var0 + "' is not a leap year");
         } else {
            throw new DateTimeException("Invalid date '" + var1.name() + " " + var2 + "'");
         }
      } else {
         return new LocalDate(var0, var1.a(), var2);
      }
   }

   private long c(LocalDate var1) {
      long var2 = this.p();
      long var4 = (long)this.g();
      return (var1.p() * 32L + (long)var1.g() - (var2 * 32L + var4)) / 32L;
   }

   private int e(TemporalField var1) {
      int var2;
      switch(null.a[((ChronoField)var1).ordinal()]) {
      case 1:
         var2 = this.f;
         break;
      case 2:
         var2 = this.h();
         break;
      case 3:
         var2 = (this.f - 1) / 7 + 1;
         break;
      case 4:
         if(this.d >= 1) {
            var2 = this.d;
         } else {
            var2 = 1 - this.d;
         }
         break;
      case 5:
         var2 = this.i().a();
         break;
      case 6:
         var2 = (this.f - 1) % 7 + 1;
         break;
      case 7:
         var2 = (this.h() - 1) % 7 + 1;
         break;
      case 8:
         throw new DateTimeException("Field too large for an int: " + var1);
      case 9:
         var2 = (this.h() - 1) / 7 + 1;
         break;
      case 10:
         var2 = this.e;
         break;
      case 11:
         throw new DateTimeException("Field too large for an int: " + var1);
      case 12:
         var2 = this.d;
         break;
      case 13:
         if(this.d >= 1) {
            var2 = 1;
         } else {
            var2 = 0;
         }
         break;
      default:
         throw new UnsupportedTemporalTypeException("Unsupported field: " + var1);
      }

      return var2;
   }

   private long p() {
      return (long)this.d * 12L + (long)(this.e - 1);
   }

   private Object readResolve() throws ObjectStreamException {
      throw new InvalidObjectException("Deserialization via serialization delegate");
   }

   private Object writeReplace() {
      return new Ser(3, this);
   }

   public int a(ChronoLocalDate var1) {
      int var2;
      if(var1 instanceof LocalDate) {
         var2 = this.b((LocalDate)var1);
      } else {
         var2 = super.a(var1);
      }

      return var2;
   }

   long a(LocalDate var1) {
      return var1.n() - this.n();
   }

   public long a(Temporal var1, TemporalUnit var2) {
      LocalDate var5 = a((TemporalAccessor)var1);
      long var3;
      if(var2 instanceof ChronoUnit) {
         switch(null.b[((ChronoUnit)var2).ordinal()]) {
         case 1:
            var3 = this.a(var5);
            break;
         case 2:
            var3 = this.a(var5) / 7L;
            break;
         case 3:
            var3 = this.c(var5);
            break;
         case 4:
            var3 = this.c(var5) / 12L;
            break;
         case 5:
            var3 = this.c(var5) / 120L;
            break;
         case 6:
            var3 = this.c(var5) / 1200L;
            break;
         case 7:
            var3 = this.c(var5) / 12000L;
            break;
         case 8:
            var3 = var5.d((TemporalField)ChronoField.B) - this.d((TemporalField)ChronoField.B);
            break;
         default:
            throw new UnsupportedTemporalTypeException("Unsupported unit: " + var2);
         }
      } else {
         var3 = var2.a(this, var5);
      }

      return var3;
   }

   public Object a(TemporalQuery var1) {
      Object var2;
      if(var1 == TemporalQueries.f()) {
         var2 = this;
      } else {
         var2 = super.a(var1);
      }

      return var2;
   }

   public String a(DateTimeFormatter var1) {
      return super.a(var1);
   }

   public LocalDate a(int var1) {
      LocalDate var2;
      if(this.d == var1) {
         var2 = this;
      } else {
         ChronoField.A.a((long)var1);
         var2 = b(var1, this.e, this.f);
      }

      return var2;
   }

   public LocalDate a(long var1, TemporalUnit var3) {
      LocalDate var5;
      if(var3 instanceof ChronoUnit) {
         ChronoUnit var4 = (ChronoUnit)var3;
         switch(null.b[var4.ordinal()]) {
         case 1:
            var5 = this.e(var1);
            break;
         case 2:
            var5 = this.d(var1);
            break;
         case 3:
            var5 = this.c(var1);
            break;
         case 4:
            var5 = this.b(var1);
            break;
         case 5:
            var5 = this.b(Jdk8Methods.a(var1, 10));
            break;
         case 6:
            var5 = this.b(Jdk8Methods.a(var1, 100));
            break;
         case 7:
            var5 = this.b(Jdk8Methods.a(var1, 1000));
            break;
         case 8:
            var5 = this.a(ChronoField.B, Jdk8Methods.b(this.d((TemporalField)ChronoField.B), var1));
            break;
         default:
            throw new UnsupportedTemporalTypeException("Unsupported unit: " + var3);
         }
      } else {
         var5 = (LocalDate)var3.a(this, var1);
      }

      return var5;
   }

   public LocalDate a(TemporalAdjuster var1) {
      LocalDate var2;
      if(var1 instanceof LocalDate) {
         var2 = (LocalDate)var1;
      } else {
         var2 = (LocalDate)var1.a(this);
      }

      return var2;
   }

   public LocalDate a(TemporalAmount var1) {
      return (LocalDate)var1.a(this);
   }

   public LocalDate a(TemporalField var1, long var2) {
      LocalDate var5;
      if(var1 instanceof ChronoField) {
         ChronoField var4 = (ChronoField)var1;
         var4.a(var2);
         switch(null.a[var4.ordinal()]) {
         case 1:
            var5 = this.c((int)var2);
            break;
         case 2:
            var5 = this.d((int)var2);
            break;
         case 3:
            var5 = this.d(var2 - this.d((TemporalField)ChronoField.v));
            break;
         case 4:
            if(this.d < 1) {
               var2 = 1L - var2;
            }

            var5 = this.a((int)var2);
            break;
         case 5:
            var5 = this.e(var2 - (long)this.i().a());
            break;
         case 6:
            var5 = this.e(var2 - this.d((TemporalField)ChronoField.q));
            break;
         case 7:
            var5 = this.e(var2 - this.d((TemporalField)ChronoField.r));
            break;
         case 8:
            var5 = a(var2);
            break;
         case 9:
            var5 = this.d(var2 - this.d((TemporalField)ChronoField.w));
            break;
         case 10:
            var5 = this.b((int)var2);
            break;
         case 11:
            var5 = this.c(var2 - this.d((TemporalField)ChronoField.y));
            break;
         case 12:
            var5 = this.a((int)var2);
            break;
         case 13:
            var5 = this;
            if(this.d((TemporalField)ChronoField.B) != var2) {
               var5 = this.a(1 - this.d);
            }
            break;
         default:
            throw new UnsupportedTemporalTypeException("Unsupported field: " + var1);
         }
      } else {
         var5 = (LocalDate)var1.a(this, var2);
      }

      return var5;
   }

   public LocalDateTime a(LocalTime var1) {
      return LocalDateTime.a(this, var1);
   }

   public Temporal a(Temporal var1) {
      return super.a(var1);
   }

   void a(DataOutput var1) throws IOException {
      var1.writeInt(this.d);
      var1.writeByte(this.e);
      var1.writeByte(this.f);
   }

   public boolean a(TemporalField var1) {
      return super.a(var1);
   }

   int b(LocalDate var1) {
      int var3 = this.d - var1.d;
      int var2 = var3;
      if(var3 == 0) {
         var3 = this.e - var1.e;
         var2 = var3;
         if(var3 == 0) {
            var2 = this.f - var1.f;
         }
      }

      return var2;
   }

   public LocalDate b(int var1) {
      LocalDate var2;
      if(this.e == var1) {
         var2 = this;
      } else {
         ChronoField.x.a((long)var1);
         var2 = b(this.d, var1, this.f);
      }

      return var2;
   }

   public LocalDate b(long var1) {
      LocalDate var3;
      if(var1 == 0L) {
         var3 = this;
      } else {
         var3 = b(ChronoField.A.b((long)this.d + var1), this.e, this.f);
      }

      return var3;
   }

   public LocalDate b(long var1, TemporalUnit var3) {
      LocalDate var4;
      if(var1 == Long.MIN_VALUE) {
         var4 = this.a(Long.MAX_VALUE, var3).a(1L, var3);
      } else {
         var4 = this.a(-var1, var3);
      }

      return var4;
   }

   public LocalDateTime b(int var1, int var2) {
      return this.a(LocalTime.a(var1, var2));
   }

   // $FF: synthetic method
   public ChronoLocalDate b(TemporalAmount var1) {
      return this.a(var1);
   }

   // $FF: synthetic method
   public ChronoLocalDateTime b(LocalTime var1) {
      return this.a(var1);
   }

   public IsoChronology b() {
      return IsoChronology.b;
   }

   // $FF: synthetic method
   public Temporal b(TemporalAdjuster var1) {
      return this.a(var1);
   }

   // $FF: synthetic method
   public Temporal b(TemporalField var1, long var2) {
      return this.a(var1, var2);
   }

   public ValueRange b(TemporalField var1) {
      ValueRange var5;
      if(var1 instanceof ChronoField) {
         ChronoField var4 = (ChronoField)var1;
         if(!var4.b()) {
            throw new UnsupportedTemporalTypeException("Unsupported field: " + var1);
         }

         switch(null.a[var4.ordinal()]) {
         case 1:
            var5 = ValueRange.a(1L, (long)this.k());
            break;
         case 2:
            var5 = ValueRange.a(1L, (long)this.l());
            break;
         case 3:
            long var2;
            if(this.f() == Month.b && !this.j()) {
               var2 = 4L;
            } else {
               var2 = 5L;
            }

            var5 = ValueRange.a(1L, var2);
            break;
         case 4:
            if(this.d() <= 0) {
               var5 = ValueRange.a(1L, 1000000000L);
            } else {
               var5 = ValueRange.a(1L, 999999999L);
            }
            break;
         default:
            var5 = var1.a();
         }
      } else {
         var5 = var1.b(this);
      }

      return var5;
   }

   public boolean b(ChronoLocalDate var1) {
      boolean var2;
      if(var1 instanceof LocalDate) {
         if(this.b((LocalDate)var1) > 0) {
            var2 = true;
         } else {
            var2 = false;
         }
      } else {
         var2 = super.b(var1);
      }

      return var2;
   }

   public int c(TemporalField var1) {
      int var2;
      if(var1 instanceof ChronoField) {
         var2 = this.e(var1);
      } else {
         var2 = super.c((TemporalField)var1);
      }

      return var2;
   }

   public LocalDate c(int var1) {
      LocalDate var2;
      if(this.f == var1) {
         var2 = this;
      } else {
         var2 = a(this.d, this.e, var1);
      }

      return var2;
   }

   public LocalDate c(long var1) {
      LocalDate var3;
      if(var1 == 0L) {
         var3 = this;
      } else {
         var1 += (long)this.d * 12L + (long)(this.e - 1);
         var3 = b(ChronoField.A.b(Jdk8Methods.e(var1, 12L)), Jdk8Methods.b(var1, 12) + 1, this.f);
      }

      return var3;
   }

   // $FF: synthetic method
   public ChronoLocalDate c(TemporalAdjuster var1) {
      return this.a(var1);
   }

   // $FF: synthetic method
   public ChronoLocalDate c(TemporalField var1, long var2) {
      return this.a(var1, var2);
   }

   public Era c() {
      return super.c();
   }

   // $FF: synthetic method
   public Temporal c(long var1, TemporalUnit var3) {
      return this.b(var1, var3);
   }

   // $FF: synthetic method
   public Temporal c(TemporalAmount var1) {
      return this.a(var1);
   }

   public boolean c(ChronoLocalDate var1) {
      boolean var2;
      if(var1 instanceof LocalDate) {
         if(this.b((LocalDate)var1) < 0) {
            var2 = true;
         } else {
            var2 = false;
         }
      } else {
         var2 = super.c(var1);
      }

      return var2;
   }

   // $FF: synthetic method
   public int compareTo(Object var1) {
      return this.a((ChronoLocalDate)var1);
   }

   public int d() {
      return this.d;
   }

   public long d(TemporalField var1) {
      long var2;
      if(var1 instanceof ChronoField) {
         if(var1 == ChronoField.u) {
            var2 = this.n();
         } else if(var1 == ChronoField.y) {
            var2 = this.p();
         } else {
            var2 = (long)this.e(var1);
         }
      } else {
         var2 = var1.c(this);
      }

      return var2;
   }

   public LocalDate d(int var1) {
      LocalDate var2;
      if(this.h() == var1) {
         var2 = this;
      } else {
         var2 = a(this.d, var1);
      }

      return var2;
   }

   public LocalDate d(long var1) {
      return this.e(Jdk8Methods.a(var1, 7));
   }

   // $FF: synthetic method
   public Temporal d(long var1, TemporalUnit var3) {
      return this.a(var1, var3);
   }

   public boolean d(ChronoLocalDate var1) {
      boolean var2;
      if(var1 instanceof LocalDate) {
         if(this.b((LocalDate)var1) == 0) {
            var2 = true;
         } else {
            var2 = false;
         }
      } else {
         var2 = super.d(var1);
      }

      return var2;
   }

   public int e() {
      return this.e;
   }

   public LocalDate e(long var1) {
      LocalDate var3;
      if(var1 == 0L) {
         var3 = this;
      } else {
         var3 = a(Jdk8Methods.b(this.n(), var1));
      }

      return var3;
   }

   // $FF: synthetic method
   public ChronoLocalDate e(long var1, TemporalUnit var3) {
      return this.b(var1, var3);
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(this != var1) {
         if(var1 instanceof LocalDate) {
            if(this.b((LocalDate)var1) != 0) {
               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public LocalDate f(long var1) {
      LocalDate var3;
      if(var1 == Long.MIN_VALUE) {
         var3 = this.b(Long.MAX_VALUE).b(1L);
      } else {
         var3 = this.b(-var1);
      }

      return var3;
   }

   public Month f() {
      return Month.a(this.e);
   }

   // $FF: synthetic method
   public ChronoLocalDate f(long var1, TemporalUnit var3) {
      return this.a(var1, var3);
   }

   public int g() {
      return this.f;
   }

   public LocalDate g(long var1) {
      LocalDate var3;
      if(var1 == Long.MIN_VALUE) {
         var3 = this.e(Long.MAX_VALUE).e(1L);
      } else {
         var3 = this.e(-var1);
      }

      return var3;
   }

   public int h() {
      return this.f().b(this.j()) + this.f - 1;
   }

   public int hashCode() {
      int var1 = this.d;
      return (var1 << 11) + (this.e << 6) + this.f ^ var1 & -2048;
   }

   public DayOfWeek i() {
      return DayOfWeek.a(Jdk8Methods.b(this.n() + 3L, 7) + 1);
   }

   public boolean j() {
      return IsoChronology.b.a((long)this.d);
   }

   public int k() {
      byte var1;
      switch(this.e) {
      case 2:
         if(this.j()) {
            var1 = 29;
         } else {
            var1 = 28;
         }
         break;
      case 3:
      case 5:
      case 7:
      case 8:
      case 10:
      default:
         var1 = 31;
         break;
      case 4:
      case 6:
      case 9:
      case 11:
         var1 = 30;
      }

      return var1;
   }

   public int l() {
      short var1;
      if(this.j()) {
         var1 = 366;
      } else {
         var1 = 365;
      }

      return var1;
   }

   public LocalDateTime m() {
      return LocalDateTime.a(this, LocalTime.c);
   }

   public long n() {
      long var1 = (long)this.d;
      long var5 = (long)this.e;
      long var3 = 365L * var1 + 0L;
      long var7;
      long var9;
      if(var1 >= 0L) {
         var9 = (3L + var1) / 4L;
         var7 = (99L + var1) / 100L;
         var1 = (var1 + 399L) / 400L + (var9 - var7) + var3;
      } else {
         var7 = var1 / -4L;
         var9 = var1 / -100L;
         var1 = var3 - (var1 / -400L + (var7 - var9));
      }

      var3 = var1 + (367L * var5 - 362L) / 12L + (long)(this.f - 1);
      var1 = var3;
      if(var5 > 2L) {
         --var3;
         var1 = var3;
         if(!this.j()) {
            var1 = var3 - 1L;
         }
      }

      return var1 - 719528L;
   }

   // $FF: synthetic method
   public Chronology o() {
      return this.b();
   }

   public String toString() {
      int var2 = this.d;
      short var1 = this.e;
      short var4 = this.f;
      int var3 = Math.abs(var2);
      StringBuilder var6 = new StringBuilder(10);
      if(var3 < 1000) {
         if(var2 < 0) {
            var6.append(var2 - 10000).deleteCharAt(1);
         } else {
            var6.append(var2 + 10000).deleteCharAt(0);
         }
      } else {
         if(var2 > 9999) {
            var6.append('+');
         }

         var6.append(var2);
      }

      String var5;
      if(var1 < 10) {
         var5 = "-0";
      } else {
         var5 = "-";
      }

      var6 = var6.append(var5).append(var1);
      if(var4 < 10) {
         var5 = "-0";
      } else {
         var5 = "-";
      }

      return var6.append(var5).append(var4).toString();
   }
}
