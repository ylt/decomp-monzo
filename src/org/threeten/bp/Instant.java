package org.threeten.bp;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.InvalidObjectException;
import java.io.ObjectStreamException;
import java.io.Serializable;
import org.threeten.bp.format.DateTimeFormatter;
import org.threeten.bp.jdk8.DefaultInterfaceTemporalAccessor;
import org.threeten.bp.jdk8.Jdk8Methods;
import org.threeten.bp.temporal.ChronoField;
import org.threeten.bp.temporal.ChronoUnit;
import org.threeten.bp.temporal.Temporal;
import org.threeten.bp.temporal.TemporalAccessor;
import org.threeten.bp.temporal.TemporalAdjuster;
import org.threeten.bp.temporal.TemporalField;
import org.threeten.bp.temporal.TemporalQueries;
import org.threeten.bp.temporal.TemporalQuery;
import org.threeten.bp.temporal.TemporalUnit;
import org.threeten.bp.temporal.UnsupportedTemporalTypeException;
import org.threeten.bp.temporal.ValueRange;

public final class Instant extends DefaultInterfaceTemporalAccessor implements Serializable, Comparable, Temporal, TemporalAdjuster {
   public static final Instant a = new Instant(0L, 0);
   public static final Instant b = a(-31557014167219200L, 0L);
   public static final Instant c = a(31556889864403199L, 999999999L);
   public static final TemporalQuery d = new TemporalQuery() {
      public Instant a(TemporalAccessor var1) {
         return Instant.a(var1);
      }

      // $FF: synthetic method
      public Object b(TemporalAccessor var1) {
         return this.a(var1);
      }
   };
   private final long e;
   private final int f;

   private Instant(long var1, int var3) {
      this.e = var1;
      this.f = var3;
   }

   public static Instant a(long var0) {
      return a(var0, 0);
   }

   private static Instant a(long var0, int var2) {
      Instant var3;
      if(((long)var2 | var0) == 0L) {
         var3 = a;
      } else {
         if(var0 < -31557014167219200L || var0 > 31556889864403199L) {
            throw new DateTimeException("Instant exceeds minimum or maximum instant");
         }

         var3 = new Instant(var0, var2);
      }

      return var3;
   }

   public static Instant a(long var0, long var2) {
      return a(Jdk8Methods.b(var0, Jdk8Methods.e(var2, 1000000000L)), Jdk8Methods.b(var2, 1000000000));
   }

   static Instant a(DataInput var0) throws IOException {
      return a(var0.readLong(), (long)var0.readInt());
   }

   public static Instant a(TemporalAccessor var0) {
      try {
         Instant var1 = a(var0.d(ChronoField.C), (long)var0.c(ChronoField.a));
         return var1;
      } catch (DateTimeException var2) {
         throw new DateTimeException("Unable to obtain Instant from TemporalAccessor: " + var0 + ", type " + var0.getClass().getName(), var2);
      }
   }

   private long b(Instant var1) {
      return Jdk8Methods.b(Jdk8Methods.a(Jdk8Methods.c(var1.e, this.e), 1000000000), (long)(var1.f - this.f));
   }

   public static Instant b(long var0) {
      return a(Jdk8Methods.e(var0, 1000L), Jdk8Methods.b(var0, 1000) * 1000000);
   }

   private Instant b(long var1, long var3) {
      Instant var5;
      if((var1 | var3) == 0L) {
         var5 = this;
      } else {
         var5 = a(Jdk8Methods.b(Jdk8Methods.b(this.e, var1), var3 / 1000000000L), var3 % 1000000000L + (long)this.f);
      }

      return var5;
   }

   private long c(Instant var1) {
      long var4 = Jdk8Methods.c(var1.e, this.e);
      long var6 = (long)(var1.f - this.f);
      long var2;
      if(var4 > 0L && var6 < 0L) {
         var2 = var4 - 1L;
      } else {
         var2 = var4;
         if(var4 < 0L) {
            var2 = var4;
            if(var6 > 0L) {
               var2 = var4 + 1L;
            }
         }
      }

      return var2;
   }

   private Object readResolve() throws ObjectStreamException {
      throw new InvalidObjectException("Deserialization via serialization delegate");
   }

   private Object writeReplace() {
      return new Ser(2, this);
   }

   public int a(Instant var1) {
      int var2 = Jdk8Methods.a(this.e, var1.e);
      if(var2 == 0) {
         var2 = this.f - var1.f;
      }

      return var2;
   }

   public long a() {
      return this.e;
   }

   public long a(Temporal var1, TemporalUnit var2) {
      Instant var6 = a((TemporalAccessor)var1);
      long var3;
      if(var2 instanceof ChronoUnit) {
         ChronoUnit var5 = (ChronoUnit)var2;
         switch(null.b[var5.ordinal()]) {
         case 1:
            var3 = this.b(var6);
            break;
         case 2:
            var3 = this.b(var6) / 1000L;
            break;
         case 3:
            var3 = Jdk8Methods.c(var6.c(), this.c());
            break;
         case 4:
            var3 = this.c(var6);
            break;
         case 5:
            var3 = this.c(var6) / 60L;
            break;
         case 6:
            var3 = this.c(var6) / 3600L;
            break;
         case 7:
            var3 = this.c(var6) / 43200L;
            break;
         case 8:
            var3 = this.c(var6) / 86400L;
            break;
         default:
            throw new UnsupportedTemporalTypeException("Unsupported unit: " + var2);
         }
      } else {
         var3 = var2.a(this, var6);
      }

      return var3;
   }

   public Object a(TemporalQuery var1) {
      Object var2;
      if(var1 == TemporalQueries.c()) {
         var2 = ChronoUnit.a;
      } else if(var1 != TemporalQueries.f() && var1 != TemporalQueries.g() && var1 != TemporalQueries.b() && var1 != TemporalQueries.a() && var1 != TemporalQueries.d() && var1 != TemporalQueries.e()) {
         var2 = var1.b(this);
      } else {
         var2 = null;
      }

      return var2;
   }

   public Instant a(long var1, TemporalUnit var3) {
      Instant var4;
      if(var3 instanceof ChronoUnit) {
         switch(null.b[((ChronoUnit)var3).ordinal()]) {
         case 1:
            var4 = this.e(var1);
            break;
         case 2:
            var4 = this.b(var1 / 1000000L, var1 % 1000000L * 1000L);
            break;
         case 3:
            var4 = this.d(var1);
            break;
         case 4:
            var4 = this.c(var1);
            break;
         case 5:
            var4 = this.c(Jdk8Methods.a(var1, 60));
            break;
         case 6:
            var4 = this.c(Jdk8Methods.a(var1, 3600));
            break;
         case 7:
            var4 = this.c(Jdk8Methods.a(var1, 'ꣀ'));
            break;
         case 8:
            var4 = this.c(Jdk8Methods.a(var1, 86400));
            break;
         default:
            throw new UnsupportedTemporalTypeException("Unsupported unit: " + var3);
         }
      } else {
         var4 = (Instant)var3.a(this, var1);
      }

      return var4;
   }

   public Instant a(TemporalAdjuster var1) {
      return (Instant)var1.a(this);
   }

   public Instant a(TemporalField var1, long var2) {
      Instant var6;
      if(var1 instanceof ChronoField) {
         ChronoField var5 = (ChronoField)var1;
         var5.a(var2);
         int var4;
         switch(null.a[var5.ordinal()]) {
         case 1:
            var6 = this;
            if(var2 != (long)this.f) {
               var6 = a(this.e, (int)var2);
            }
            break;
         case 2:
            var4 = (int)var2 * 1000;
            var6 = this;
            if(var4 != this.f) {
               var6 = a(this.e, var4);
            }
            break;
         case 3:
            var4 = (int)var2 * 1000000;
            var6 = this;
            if(var4 != this.f) {
               var6 = a(this.e, var4);
            }
            break;
         case 4:
            var6 = this;
            if(var2 != this.e) {
               var6 = a(var2, this.f);
            }
            break;
         default:
            throw new UnsupportedTemporalTypeException("Unsupported field: " + var1);
         }
      } else {
         var6 = (Instant)var1.a(this, var2);
      }

      return var6;
   }

   public Temporal a(Temporal var1) {
      return var1.b(ChronoField.C, this.e).b(ChronoField.a, (long)this.f);
   }

   void a(DataOutput var1) throws IOException {
      var1.writeLong(this.e);
      var1.writeInt(this.f);
   }

   public boolean a(TemporalField var1) {
      boolean var2 = true;
      boolean var3 = false;
      if(var1 instanceof ChronoField) {
         if(var1 != ChronoField.C && var1 != ChronoField.a && var1 != ChronoField.c) {
            var2 = var3;
            if(var1 != ChronoField.e) {
               return var2;
            }
         }

         var2 = true;
      } else if(var1 == null || !var1.a(this)) {
         var2 = false;
      }

      return var2;
   }

   public int b() {
      return this.f;
   }

   public Instant b(long var1, TemporalUnit var3) {
      Instant var4;
      if(var1 == Long.MIN_VALUE) {
         var4 = this.a(Long.MAX_VALUE, var3).a(1L, var3);
      } else {
         var4 = this.a(-var1, var3);
      }

      return var4;
   }

   // $FF: synthetic method
   public Temporal b(TemporalAdjuster var1) {
      return this.a(var1);
   }

   // $FF: synthetic method
   public Temporal b(TemporalField var1, long var2) {
      return this.a(var1, var2);
   }

   public ValueRange b(TemporalField var1) {
      return super.b(var1);
   }

   public int c(TemporalField var1) {
      int var2;
      if(var1 instanceof ChronoField) {
         switch(null.a[((ChronoField)var1).ordinal()]) {
         case 1:
            var2 = this.f;
            break;
         case 2:
            var2 = this.f / 1000;
            break;
         case 3:
            var2 = this.f / 1000000;
            break;
         default:
            throw new UnsupportedTemporalTypeException("Unsupported field: " + var1);
         }
      } else {
         var2 = this.b(var1).b(var1.c(this), var1);
      }

      return var2;
   }

   public long c() {
      long var1;
      if(this.e >= 0L) {
         var1 = Jdk8Methods.b(Jdk8Methods.d(this.e, 1000L), (long)(this.f / 1000000));
      } else {
         var1 = Jdk8Methods.c(Jdk8Methods.d(this.e + 1L, 1000L), 1000L - (long)(this.f / 1000000));
      }

      return var1;
   }

   public Instant c(long var1) {
      return this.b(var1, 0L);
   }

   // $FF: synthetic method
   public Temporal c(long var1, TemporalUnit var3) {
      return this.b(var1, var3);
   }

   // $FF: synthetic method
   public int compareTo(Object var1) {
      return this.a((Instant)var1);
   }

   public long d(TemporalField var1) {
      long var2;
      if(var1 instanceof ChronoField) {
         switch(null.a[((ChronoField)var1).ordinal()]) {
         case 1:
            var2 = (long)this.f;
            break;
         case 2:
            var2 = (long)(this.f / 1000);
            break;
         case 3:
            var2 = (long)(this.f / 1000000);
            break;
         case 4:
            var2 = this.e;
            break;
         default:
            throw new UnsupportedTemporalTypeException("Unsupported field: " + var1);
         }
      } else {
         var2 = var1.c(this);
      }

      return var2;
   }

   public Instant d(long var1) {
      return this.b(var1 / 1000L, var1 % 1000L * 1000000L);
   }

   // $FF: synthetic method
   public Temporal d(long var1, TemporalUnit var3) {
      return this.a(var1, var3);
   }

   public Instant e(long var1) {
      return this.b(0L, var1);
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(this != var1) {
         if(var1 instanceof Instant) {
            Instant var3 = (Instant)var1;
            if(this.e != var3.e || this.f != var3.f) {
               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public int hashCode() {
      return (int)(this.e ^ this.e >>> 32) + this.f * 51;
   }

   public String toString() {
      return DateTimeFormatter.m.a((TemporalAccessor)this);
   }
}
