package org.threeten.bp;

import java.util.Date;
import java.util.TimeZone;

public final class DateTimeUtils {
   public static Date a(Instant var0) {
      try {
         Date var2 = new Date(var0.c());
         return var2;
      } catch (ArithmeticException var1) {
         throw new IllegalArgumentException(var1);
      }
   }

   public static ZoneId a(TimeZone var0) {
      return ZoneId.a(var0.getID(), ZoneId.b);
   }
}
