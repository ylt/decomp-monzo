package org.threeten.bp;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.InvalidObjectException;
import java.io.ObjectStreamException;
import java.io.Serializable;
import java.util.regex.Pattern;
import org.threeten.bp.jdk8.Jdk8Methods;
import org.threeten.bp.zone.ZoneRules;
import org.threeten.bp.zone.ZoneRulesException;
import org.threeten.bp.zone.ZoneRulesProvider;

final class ZoneRegion extends ZoneId implements Serializable {
   private static final Pattern c = Pattern.compile("[A-Za-z][A-Za-z0-9~/._+-]+");
   private final String d;
   private final transient ZoneRules e;

   ZoneRegion(String var1, ZoneRules var2) {
      this.d = var1;
      this.e = var2;
   }

   static ZoneId a(DataInput var0) throws IOException {
      return b(var0.readUTF());
   }

   static ZoneRegion a(String var0, boolean var1) {
      Jdk8Methods.a(var0, (String)"zoneId");
      if(var0.length() >= 2 && c.matcher(var0).matches()) {
         ZoneRules var2 = null;

         ZoneRules var3;
         try {
            var3 = ZoneRulesProvider.b(var0, true);
         } catch (ZoneRulesException var4) {
            if(var0.equals("GMT0")) {
               var2 = ZoneOffset.d.d();
            } else if(var1) {
               throw var4;
            }

            return new ZoneRegion(var0, var2);
         }

         var2 = var3;
         return new ZoneRegion(var0, var2);
      } else {
         throw new DateTimeException("Invalid ID for region-based ZoneId, invalid format: " + var0);
      }
   }

   private static ZoneRegion b(String var0) {
      if(!var0.equals("Z") && !var0.startsWith("+") && !var0.startsWith("-")) {
         ZoneRegion var2;
         if(!var0.equals("UTC") && !var0.equals("GMT") && !var0.equals("UT")) {
            if(!var0.startsWith("UTC+") && !var0.startsWith("GMT+") && !var0.startsWith("UTC-") && !var0.startsWith("GMT-")) {
               if(!var0.startsWith("UT+") && !var0.startsWith("UT-")) {
                  var2 = a(var0, false);
               } else {
                  ZoneOffset var3 = ZoneOffset.b(var0.substring(2));
                  if(var3.f() == 0) {
                     var2 = new ZoneRegion("UT", var3.d());
                  } else {
                     var2 = new ZoneRegion("UT" + var3.c(), var3.d());
                  }
               }
            } else {
               ZoneOffset var1 = ZoneOffset.b(var0.substring(3));
               if(var1.f() == 0) {
                  var2 = new ZoneRegion(var0.substring(0, 3), var1.d());
               } else {
                  var2 = new ZoneRegion(var0.substring(0, 3) + var1.c(), var1.d());
               }
            }
         } else {
            var2 = new ZoneRegion(var0, ZoneOffset.d.d());
         }

         return var2;
      } else {
         throw new DateTimeException("Invalid ID for region-based ZoneId, invalid format: " + var0);
      }
   }

   private Object readResolve() throws ObjectStreamException {
      throw new InvalidObjectException("Deserialization via serialization delegate");
   }

   private Object writeReplace() {
      return new Ser(7, this);
   }

   void a(DataOutput var1) throws IOException {
      var1.writeByte(7);
      this.b(var1);
   }

   void b(DataOutput var1) throws IOException {
      var1.writeUTF(this.d);
   }

   public String c() {
      return this.d;
   }

   public ZoneRules d() {
      ZoneRules var1;
      if(this.e != null) {
         var1 = this.e;
      } else {
         var1 = ZoneRulesProvider.b(this.d, false);
      }

      return var1;
   }
}
