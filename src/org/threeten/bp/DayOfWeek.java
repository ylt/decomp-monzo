package org.threeten.bp;

import java.util.Locale;
import org.threeten.bp.format.DateTimeFormatterBuilder;
import org.threeten.bp.format.TextStyle;
import org.threeten.bp.temporal.ChronoField;
import org.threeten.bp.temporal.ChronoUnit;
import org.threeten.bp.temporal.Temporal;
import org.threeten.bp.temporal.TemporalAccessor;
import org.threeten.bp.temporal.TemporalAdjuster;
import org.threeten.bp.temporal.TemporalField;
import org.threeten.bp.temporal.TemporalQueries;
import org.threeten.bp.temporal.TemporalQuery;
import org.threeten.bp.temporal.UnsupportedTemporalTypeException;
import org.threeten.bp.temporal.ValueRange;

public enum DayOfWeek implements TemporalAccessor, TemporalAdjuster {
   a,
   b,
   c,
   d,
   e,
   f,
   g;

   public static final TemporalQuery h = new TemporalQuery() {
      public DayOfWeek a(TemporalAccessor var1) {
         return DayOfWeek.a(var1);
      }

      // $FF: synthetic method
      public Object b(TemporalAccessor var1) {
         return this.a(var1);
      }
   };
   private static final DayOfWeek[] i = values();

   public static DayOfWeek a(int var0) {
      if(var0 >= 1 && var0 <= 7) {
         return i[var0 - 1];
      } else {
         throw new DateTimeException("Invalid value for DayOfWeek: " + var0);
      }
   }

   public static DayOfWeek a(TemporalAccessor var0) {
      DayOfWeek var3;
      if(var0 instanceof DayOfWeek) {
         var3 = (DayOfWeek)var0;
      } else {
         DayOfWeek var1;
         try {
            var1 = a(var0.c(ChronoField.p));
         } catch (DateTimeException var2) {
            throw new DateTimeException("Unable to obtain DayOfWeek from TemporalAccessor: " + var0 + ", type " + var0.getClass().getName(), var2);
         }

         var3 = var1;
      }

      return var3;
   }

   public int a() {
      return this.ordinal() + 1;
   }

   public Object a(TemporalQuery var1) {
      Object var2;
      if(var1 == TemporalQueries.c()) {
         var2 = ChronoUnit.h;
      } else if(var1 != TemporalQueries.f() && var1 != TemporalQueries.g() && var1 != TemporalQueries.b() && var1 != TemporalQueries.d() && var1 != TemporalQueries.a() && var1 != TemporalQueries.e()) {
         var2 = var1.b(this);
      } else {
         var2 = null;
      }

      return var2;
   }

   public String a(TextStyle var1, Locale var2) {
      return (new DateTimeFormatterBuilder()).a((TemporalField)ChronoField.p, (TextStyle)var1).a(var2).a((TemporalAccessor)this);
   }

   public DayOfWeek a(long var1) {
      int var3 = (int)(var1 % 7L);
      return i[(var3 + 7 + this.ordinal()) % 7];
   }

   public Temporal a(Temporal var1) {
      return var1.b(ChronoField.p, (long)this.a());
   }

   public boolean a(TemporalField var1) {
      boolean var2 = true;
      if(var1 instanceof ChronoField) {
         if(var1 != ChronoField.p) {
            var2 = false;
         }
      } else if(var1 == null || !var1.a(this)) {
         var2 = false;
      }

      return var2;
   }

   public ValueRange b(TemporalField var1) {
      ValueRange var2;
      if(var1 == ChronoField.p) {
         var2 = var1.a();
      } else {
         if(var1 instanceof ChronoField) {
            throw new UnsupportedTemporalTypeException("Unsupported field: " + var1);
         }

         var2 = var1.b(this);
      }

      return var2;
   }

   public int c(TemporalField var1) {
      int var2;
      if(var1 == ChronoField.p) {
         var2 = this.a();
      } else {
         var2 = this.b(var1).b(this.d(var1), var1);
      }

      return var2;
   }

   public long d(TemporalField var1) {
      long var2;
      if(var1 == ChronoField.p) {
         var2 = (long)this.a();
      } else {
         if(var1 instanceof ChronoField) {
            throw new UnsupportedTemporalTypeException("Unsupported field: " + var1);
         }

         var2 = var1.c(this);
      }

      return var2;
   }
}
