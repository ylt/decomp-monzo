package org.threeten.bp.jdk8;

import org.threeten.bp.temporal.Temporal;
import org.threeten.bp.temporal.TemporalAdjuster;
import org.threeten.bp.temporal.TemporalAmount;
import org.threeten.bp.temporal.TemporalUnit;

public abstract class DefaultInterfaceTemporal extends DefaultInterfaceTemporalAccessor implements Temporal {
   public Temporal b(TemporalAdjuster var1) {
      return var1.a(this);
   }

   public Temporal c(long var1, TemporalUnit var3) {
      Temporal var4;
      if(var1 == Long.MIN_VALUE) {
         var4 = this.d(Long.MAX_VALUE, var3).d(1L, var3);
      } else {
         var4 = this.d(-var1, var3);
      }

      return var4;
   }

   public Temporal c(TemporalAmount var1) {
      return var1.a(this);
   }
}
