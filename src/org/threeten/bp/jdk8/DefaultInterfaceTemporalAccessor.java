package org.threeten.bp.jdk8;

import org.threeten.bp.temporal.ChronoField;
import org.threeten.bp.temporal.TemporalAccessor;
import org.threeten.bp.temporal.TemporalField;
import org.threeten.bp.temporal.TemporalQueries;
import org.threeten.bp.temporal.TemporalQuery;
import org.threeten.bp.temporal.UnsupportedTemporalTypeException;
import org.threeten.bp.temporal.ValueRange;

public abstract class DefaultInterfaceTemporalAccessor implements TemporalAccessor {
   public Object a(TemporalQuery var1) {
      Object var2;
      if(var1 != TemporalQueries.a() && var1 != TemporalQueries.b() && var1 != TemporalQueries.c()) {
         var2 = var1.b(this);
      } else {
         var2 = null;
      }

      return var2;
   }

   public ValueRange b(TemporalField var1) {
      ValueRange var2;
      if(var1 instanceof ChronoField) {
         if(!this.a(var1)) {
            throw new UnsupportedTemporalTypeException("Unsupported field: " + var1);
         }

         var2 = var1.a();
      } else {
         var2 = var1.b(this);
      }

      return var2;
   }

   public int c(TemporalField var1) {
      return this.b(var1).b(this.d(var1), var1);
   }
}
