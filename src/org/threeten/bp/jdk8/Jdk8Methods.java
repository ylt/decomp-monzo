package org.threeten.bp.jdk8;

public final class Jdk8Methods {
   public static int a(int var0, int var1) {
      byte var2;
      if(var0 < var1) {
         var2 = -1;
      } else if(var0 > var1) {
         var2 = 1;
      } else {
         var2 = 0;
      }

      return var2;
   }

   public static int a(long var0) {
      if(var0 <= 2147483647L && var0 >= -2147483648L) {
         return (int)var0;
      } else {
         throw new ArithmeticException("Calculation overflows an int: " + var0);
      }
   }

   public static int a(long var0, long var2) {
      byte var4;
      if(var0 < var2) {
         var4 = -1;
      } else if(var0 > var2) {
         var4 = 1;
      } else {
         var4 = 0;
      }

      return var4;
   }

   public static long a(long var0, int var2) {
      long var3 = var0;
      switch(var2) {
      case -1:
         if(var0 == Long.MIN_VALUE) {
            throw new ArithmeticException("Multiplication overflows a long: " + var0 + " * " + var2);
         }

         var3 = -var0;
         break;
      case 0:
         var3 = 0L;
      case 1:
         break;
      default:
         var3 = (long)var2 * var0;
         if(var3 / (long)var2 != var0) {
            throw new ArithmeticException("Multiplication overflows a long: " + var0 + " * " + var2);
         }
      }

      return var3;
   }

   public static Object a(Object var0, String var1) {
      if(var0 == null) {
         throw new NullPointerException(var1 + " must not be null");
      } else {
         return var0;
      }
   }

   public static boolean a(Object var0, Object var1) {
      boolean var2 = false;
      if(var0 == null) {
         if(var1 == null) {
            var2 = true;
         }
      } else if(var1 != null) {
         var2 = var0.equals(var1);
      }

      return var2;
   }

   public static int b(int var0, int var1) {
      int var2 = var0 + var1;
      if((var0 ^ var2) < 0 && (var0 ^ var1) >= 0) {
         throw new ArithmeticException("Addition overflows an int: " + var0 + " + " + var1);
      } else {
         return var2;
      }
   }

   public static int b(long var0, int var2) {
      return (int)((var0 % (long)var2 + (long)var2) % (long)var2);
   }

   public static long b(long var0, long var2) {
      long var4 = var0 + var2;
      if((var0 ^ var4) < 0L && (var0 ^ var2) >= 0L) {
         throw new ArithmeticException("Addition overflows a long: " + var0 + " + " + var2);
      } else {
         return var4;
      }
   }

   public static int c(int var0, int var1) {
      int var2 = var0 - var1;
      if((var0 ^ var2) < 0 && (var0 ^ var1) < 0) {
         throw new ArithmeticException("Subtraction overflows an int: " + var0 + " - " + var1);
      } else {
         return var2;
      }
   }

   public static long c(long var0, long var2) {
      long var4 = var0 - var2;
      if((var0 ^ var4) < 0L && (var0 ^ var2) < 0L) {
         throw new ArithmeticException("Subtraction overflows a long: " + var0 + " - " + var2);
      } else {
         return var4;
      }
   }

   public static int d(int var0, int var1) {
      return (var0 % var1 + var1) % var1;
   }

   public static long d(long var0, long var2) {
      if(var2 != 1L) {
         if(var0 == 1L) {
            var0 = var2;
         } else if(var0 != 0L && var2 != 0L) {
            long var4 = var0 * var2;
            if(var4 / var2 != var0 || var0 == Long.MIN_VALUE && var2 == -1L || var2 == Long.MIN_VALUE && var0 == -1L) {
               throw new ArithmeticException("Multiplication overflows a long: " + var0 + " * " + var2);
            }

            var0 = var4;
         } else {
            var0 = 0L;
         }
      }

      return var0;
   }

   public static long e(long var0, long var2) {
      if(var0 >= 0L) {
         var0 /= var2;
      } else {
         var0 = (var0 + 1L) / var2 - 1L;
      }

      return var0;
   }

   public static long f(long var0, long var2) {
      return (var0 % var2 + var2) % var2;
   }
}
