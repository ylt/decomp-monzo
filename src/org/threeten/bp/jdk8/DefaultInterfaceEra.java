package org.threeten.bp.jdk8;

import org.threeten.bp.chrono.Era;
import org.threeten.bp.temporal.ChronoField;
import org.threeten.bp.temporal.ChronoUnit;
import org.threeten.bp.temporal.Temporal;
import org.threeten.bp.temporal.TemporalField;
import org.threeten.bp.temporal.TemporalQueries;
import org.threeten.bp.temporal.TemporalQuery;
import org.threeten.bp.temporal.UnsupportedTemporalTypeException;

public abstract class DefaultInterfaceEra extends DefaultInterfaceTemporalAccessor implements Era {
   public Object a(TemporalQuery var1) {
      Object var2;
      if(var1 == TemporalQueries.c()) {
         var2 = ChronoUnit.o;
      } else if(var1 != TemporalQueries.b() && var1 != TemporalQueries.d() && var1 != TemporalQueries.a() && var1 != TemporalQueries.e() && var1 != TemporalQueries.f() && var1 != TemporalQueries.g()) {
         var2 = var1.b(this);
      } else {
         var2 = null;
      }

      return var2;
   }

   public Temporal a(Temporal var1) {
      return var1.b(ChronoField.B, (long)this.a());
   }

   public boolean a(TemporalField var1) {
      boolean var2 = true;
      if(var1 instanceof ChronoField) {
         if(var1 != ChronoField.B) {
            var2 = false;
         }
      } else if(var1 == null || !var1.a(this)) {
         var2 = false;
      }

      return var2;
   }

   public int c(TemporalField var1) {
      int var2;
      if(var1 == ChronoField.B) {
         var2 = this.a();
      } else {
         var2 = this.b(var1).b(this.d(var1), var1);
      }

      return var2;
   }

   public long d(TemporalField var1) {
      long var2;
      if(var1 == ChronoField.B) {
         var2 = (long)this.a();
      } else {
         if(var1 instanceof ChronoField) {
            throw new UnsupportedTemporalTypeException("Unsupported field: " + var1);
         }

         var2 = var1.c(this);
      }

      return var2;
   }
}
