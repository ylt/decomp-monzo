package org.threeten.bp;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.InvalidObjectException;
import java.io.ObjectStreamException;
import java.io.Serializable;
import java.util.List;
import org.threeten.bp.chrono.ChronoLocalDate;
import org.threeten.bp.chrono.ChronoLocalDateTime;
import org.threeten.bp.chrono.ChronoZonedDateTime;
import org.threeten.bp.format.DateTimeFormatter;
import org.threeten.bp.jdk8.Jdk8Methods;
import org.threeten.bp.temporal.ChronoField;
import org.threeten.bp.temporal.ChronoUnit;
import org.threeten.bp.temporal.Temporal;
import org.threeten.bp.temporal.TemporalAccessor;
import org.threeten.bp.temporal.TemporalAdjuster;
import org.threeten.bp.temporal.TemporalAmount;
import org.threeten.bp.temporal.TemporalField;
import org.threeten.bp.temporal.TemporalQueries;
import org.threeten.bp.temporal.TemporalQuery;
import org.threeten.bp.temporal.TemporalUnit;
import org.threeten.bp.temporal.ValueRange;
import org.threeten.bp.zone.ZoneOffsetTransition;
import org.threeten.bp.zone.ZoneRules;

public final class ZonedDateTime extends ChronoZonedDateTime implements Serializable, Temporal {
   public static final TemporalQuery a = new TemporalQuery() {
      public ZonedDateTime a(TemporalAccessor var1) {
         return ZonedDateTime.a(var1);
      }

      // $FF: synthetic method
      public Object b(TemporalAccessor var1) {
         return this.a(var1);
      }
   };
   private final LocalDateTime b;
   private final ZoneOffset c;
   private final ZoneId d;

   private ZonedDateTime(LocalDateTime var1, ZoneOffset var2, ZoneId var3) {
      this.b = var1;
      this.c = var2;
      this.d = var3;
   }

   private static ZonedDateTime a(long var0, int var2, ZoneId var3) {
      ZoneOffset var4 = var3.d().a(Instant.a(var0, (long)var2));
      return new ZonedDateTime(LocalDateTime.a(var0, var2, var4), var4, var3);
   }

   static ZonedDateTime a(DataInput var0) throws IOException {
      return b(LocalDateTime.a(var0), ZoneOffset.a(var0), (ZoneId)Ser.a(var0));
   }

   public static ZonedDateTime a(CharSequence var0) {
      return a(var0, DateTimeFormatter.i);
   }

   public static ZonedDateTime a(CharSequence var0, DateTimeFormatter var1) {
      Jdk8Methods.a(var1, (String)"formatter");
      return (ZonedDateTime)var1.a(var0, a);
   }

   public static ZonedDateTime a(Instant var0, ZoneId var1) {
      Jdk8Methods.a(var0, (String)"instant");
      Jdk8Methods.a(var1, (String)"zone");
      return a(var0.a(), var0.b(), var1);
   }

   public static ZonedDateTime a(LocalDate var0, LocalTime var1, ZoneId var2) {
      return a(LocalDateTime.a(var0, var1), var2);
   }

   private ZonedDateTime a(LocalDateTime var1) {
      return a(var1, this.d, this.c);
   }

   public static ZonedDateTime a(LocalDateTime var0, ZoneId var1) {
      return a((LocalDateTime)var0, (ZoneId)var1, (ZoneOffset)null);
   }

   public static ZonedDateTime a(LocalDateTime var0, ZoneId var1, ZoneOffset var2) {
      Jdk8Methods.a(var0, (String)"localDateTime");
      Jdk8Methods.a(var1, (String)"zone");
      ZonedDateTime var5;
      if(var1 instanceof ZoneOffset) {
         var5 = new ZonedDateTime(var0, (ZoneOffset)var1, var1);
      } else {
         ZoneRules var4 = var1.d();
         List var3 = var4.a(var0);
         if(var3.size() == 1) {
            var2 = (ZoneOffset)var3.get(0);
         } else if(var3.size() == 0) {
            ZoneOffsetTransition var6 = var4.b(var0);
            var0 = var0.d(var6.g().a());
            var2 = var6.f();
         } else if(var2 == null || !var3.contains(var2)) {
            var2 = (ZoneOffset)Jdk8Methods.a(var3.get(0), "offset");
         }

         var5 = new ZonedDateTime(var0, var2, var1);
      }

      return var5;
   }

   public static ZonedDateTime a(LocalDateTime var0, ZoneOffset var1, ZoneId var2) {
      Jdk8Methods.a(var0, (String)"localDateTime");
      Jdk8Methods.a(var1, (String)"offset");
      Jdk8Methods.a(var2, (String)"zone");
      return a(var0.c((ZoneOffset)var1), var0.i(), var2);
   }

   private ZonedDateTime a(ZoneOffset var1) {
      ZonedDateTime var2 = this;
      if(!var1.equals(this.c)) {
         var2 = this;
         if(this.d.d().a(this.b, var1)) {
            var2 = new ZonedDateTime(this.b, var1, this.d);
         }
      }

      return var2;
   }

   public static ZonedDateTime a(TemporalAccessor param0) {
      // $FF: Couldn't be decompiled
   }

   private ZonedDateTime b(LocalDateTime var1) {
      return a(var1, this.c, this.d);
   }

   private static ZonedDateTime b(LocalDateTime var0, ZoneOffset var1, ZoneId var2) {
      Jdk8Methods.a(var0, (String)"localDateTime");
      Jdk8Methods.a(var1, (String)"offset");
      Jdk8Methods.a(var2, (String)"zone");
      if(var2 instanceof ZoneOffset && !var1.equals(var2)) {
         throw new IllegalArgumentException("ZoneId must match ZoneOffset");
      } else {
         return new ZonedDateTime(var0, var1, var2);
      }
   }

   private Object readResolve() throws ObjectStreamException {
      throw new InvalidObjectException("Deserialization via serialization delegate");
   }

   private Object writeReplace() {
      return new Ser(6, this);
   }

   public long a(Temporal var1, TemporalUnit var2) {
      ZonedDateTime var5 = a((TemporalAccessor)var1);
      long var3;
      if(var2 instanceof ChronoUnit) {
         var5 = var5.b(this.d);
         if(var2.a()) {
            var3 = this.b.a((Temporal)var5.b, (TemporalUnit)var2);
         } else {
            var3 = this.g().a((Temporal)var5.g(), (TemporalUnit)var2);
         }
      } else {
         var3 = var2.a(this, var5);
      }

      return var3;
   }

   public Object a(TemporalQuery var1) {
      Object var2;
      if(var1 == TemporalQueries.f()) {
         var2 = this.e();
      } else {
         var2 = super.a(var1);
      }

      return var2;
   }

   public String a(DateTimeFormatter var1) {
      return super.a(var1);
   }

   public ZoneOffset a() {
      return this.c;
   }

   public ZonedDateTime a(long var1, TemporalUnit var3) {
      ZonedDateTime var4;
      if(var3 instanceof ChronoUnit) {
         if(var3.a()) {
            var4 = this.a(this.b.a(var1, var3));
         } else {
            var4 = this.b(this.b.a(var1, var3));
         }
      } else {
         var4 = (ZonedDateTime)var3.a(this, var1);
      }

      return var4;
   }

   public ZonedDateTime a(ZoneId var1) {
      Jdk8Methods.a(var1, (String)"zone");
      ZonedDateTime var2;
      if(this.d.equals(var1)) {
         var2 = this;
      } else {
         var2 = a(this.b, var1, this.c);
      }

      return var2;
   }

   public ZonedDateTime a(TemporalAdjuster var1) {
      ZonedDateTime var2;
      if(var1 instanceof LocalDate) {
         var2 = this.a(LocalDateTime.a((LocalDate)var1, this.b.k()));
      } else if(var1 instanceof LocalTime) {
         var2 = this.a(LocalDateTime.a(this.b.j(), (LocalTime)var1));
      } else if(var1 instanceof LocalDateTime) {
         var2 = this.a((LocalDateTime)var1);
      } else if(var1 instanceof Instant) {
         Instant var3 = (Instant)var1;
         var2 = a(var3.a(), var3.b(), this.d);
      } else if(var1 instanceof ZoneOffset) {
         var2 = this.a((ZoneOffset)var1);
      } else {
         var2 = (ZonedDateTime)var1.a(this);
      }

      return var2;
   }

   public ZonedDateTime a(TemporalAmount var1) {
      return (ZonedDateTime)var1.a(this);
   }

   public ZonedDateTime a(TemporalField var1, long var2) {
      ZonedDateTime var5;
      if(var1 instanceof ChronoField) {
         ChronoField var4 = (ChronoField)var1;
         switch(null.a[var4.ordinal()]) {
         case 1:
            var5 = a(var2, this.c(), this.d);
            break;
         case 2:
            var5 = this.a(ZoneOffset.a(var4.b(var2)));
            break;
         default:
            var5 = this.a(this.b.a(var1, var2));
         }
      } else {
         var5 = (ZonedDateTime)var1.a(this, var2);
      }

      return var5;
   }

   void a(DataOutput var1) throws IOException {
      this.b.a(var1);
      this.c.b(var1);
      this.d.a(var1);
   }

   public boolean a(TemporalField var1) {
      boolean var2;
      if(!(var1 instanceof ChronoField) && (var1 == null || !var1.a(this))) {
         var2 = false;
      } else {
         var2 = true;
      }

      return var2;
   }

   public ZoneId b() {
      return this.d;
   }

   public ZonedDateTime b(long var1, TemporalUnit var3) {
      ZonedDateTime var4;
      if(var1 == Long.MIN_VALUE) {
         var4 = this.a(Long.MAX_VALUE, var3).a(1L, var3);
      } else {
         var4 = this.a(-var1, var3);
      }

      return var4;
   }

   public ZonedDateTime b(ZoneId var1) {
      Jdk8Methods.a(var1, (String)"zone");
      ZonedDateTime var2;
      if(this.d.equals(var1)) {
         var2 = this;
      } else {
         var2 = a(this.b.c((ZoneOffset)this.c), this.b.i(), var1);
      }

      return var2;
   }

   // $FF: synthetic method
   public ChronoZonedDateTime b(TemporalAmount var1) {
      return this.a(var1);
   }

   // $FF: synthetic method
   public Temporal b(TemporalAdjuster var1) {
      return this.a(var1);
   }

   // $FF: synthetic method
   public Temporal b(TemporalField var1, long var2) {
      return this.a(var1, var2);
   }

   public ValueRange b(TemporalField var1) {
      ValueRange var2;
      if(var1 instanceof ChronoField) {
         if(var1 != ChronoField.C && var1 != ChronoField.D) {
            var2 = this.b.b(var1);
         } else {
            var2 = var1.a();
         }
      } else {
         var2 = var1.b(this);
      }

      return var2;
   }

   public int c() {
      return this.b.i();
   }

   public int c(TemporalField var1) {
      int var2;
      if(var1 instanceof ChronoField) {
         switch(null.a[((ChronoField)var1).ordinal()]) {
         case 1:
            throw new DateTimeException("Field too large for an int: " + var1);
         case 2:
            var2 = this.a().f();
            break;
         default:
            var2 = this.b.c(var1);
         }
      } else {
         var2 = super.c(var1);
      }

      return var2;
   }

   // $FF: synthetic method
   public ChronoZonedDateTime c(ZoneId var1) {
      return this.b(var1);
   }

   // $FF: synthetic method
   public ChronoZonedDateTime c(TemporalAdjuster var1) {
      return this.a(var1);
   }

   // $FF: synthetic method
   public ChronoZonedDateTime c(TemporalField var1, long var2) {
      return this.a(var1, var2);
   }

   // $FF: synthetic method
   public Temporal c(long var1, TemporalUnit var3) {
      return this.b(var1, var3);
   }

   // $FF: synthetic method
   public Temporal c(TemporalAmount var1) {
      return this.a(var1);
   }

   public long d(TemporalField var1) {
      long var2;
      if(var1 instanceof ChronoField) {
         switch(null.a[((ChronoField)var1).ordinal()]) {
         case 1:
            var2 = this.k();
            break;
         case 2:
            var2 = (long)this.a().f();
            break;
         default:
            var2 = this.b.d(var1);
         }
      } else {
         var2 = var1.c(this);
      }

      return var2;
   }

   public LocalDateTime d() {
      return this.b;
   }

   // $FF: synthetic method
   public ChronoZonedDateTime d(ZoneId var1) {
      return this.a(var1);
   }

   // $FF: synthetic method
   public Temporal d(long var1, TemporalUnit var3) {
      return this.a(var1, var3);
   }

   public LocalDate e() {
      return this.b.j();
   }

   // $FF: synthetic method
   public ChronoZonedDateTime e(long var1, TemporalUnit var3) {
      return this.b(var1, var3);
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(this != var1) {
         if(var1 instanceof ZonedDateTime) {
            ZonedDateTime var3 = (ZonedDateTime)var1;
            if(!this.b.equals(var3.b) || !this.c.equals(var3.c) || !this.d.equals(var3.d)) {
               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public LocalTime f() {
      return this.b.k();
   }

   // $FF: synthetic method
   public ChronoZonedDateTime f(long var1, TemporalUnit var3) {
      return this.a(var1, var3);
   }

   public OffsetDateTime g() {
      return OffsetDateTime.a(this.b, this.c);
   }

   // $FF: synthetic method
   public ChronoLocalDateTime h() {
      return this.d();
   }

   public int hashCode() {
      return this.b.hashCode() ^ this.c.hashCode() ^ Integer.rotateLeft(this.d.hashCode(), 3);
   }

   // $FF: synthetic method
   public ChronoLocalDate i() {
      return this.e();
   }

   public String toString() {
      String var2 = this.b.toString() + this.c.toString();
      String var1 = var2;
      if(this.c != this.d) {
         var1 = var2 + '[' + this.d.toString() + ']';
      }

      return var1;
   }
}
