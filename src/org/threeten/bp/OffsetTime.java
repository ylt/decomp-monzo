package org.threeten.bp;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.InvalidObjectException;
import java.io.ObjectStreamException;
import java.io.Serializable;
import org.threeten.bp.jdk8.DefaultInterfaceTemporalAccessor;
import org.threeten.bp.jdk8.Jdk8Methods;
import org.threeten.bp.temporal.ChronoField;
import org.threeten.bp.temporal.ChronoUnit;
import org.threeten.bp.temporal.Temporal;
import org.threeten.bp.temporal.TemporalAccessor;
import org.threeten.bp.temporal.TemporalAdjuster;
import org.threeten.bp.temporal.TemporalField;
import org.threeten.bp.temporal.TemporalQueries;
import org.threeten.bp.temporal.TemporalQuery;
import org.threeten.bp.temporal.TemporalUnit;
import org.threeten.bp.temporal.UnsupportedTemporalTypeException;
import org.threeten.bp.temporal.ValueRange;

public final class OffsetTime extends DefaultInterfaceTemporalAccessor implements Serializable, Comparable, Temporal, TemporalAdjuster {
   public static final OffsetTime a;
   public static final OffsetTime b;
   public static final TemporalQuery c;
   private final LocalTime d;
   private final ZoneOffset e;

   static {
      a = LocalTime.a.a(ZoneOffset.f);
      b = LocalTime.b.a(ZoneOffset.e);
      c = new TemporalQuery() {
         public OffsetTime a(TemporalAccessor var1) {
            return OffsetTime.a(var1);
         }

         // $FF: synthetic method
         public Object b(TemporalAccessor var1) {
            return this.a(var1);
         }
      };
   }

   private OffsetTime(LocalTime var1, ZoneOffset var2) {
      this.d = (LocalTime)Jdk8Methods.a(var1, (String)"time");
      this.e = (ZoneOffset)Jdk8Methods.a(var2, (String)"offset");
   }

   static OffsetTime a(DataInput var0) throws IOException {
      return a(LocalTime.a(var0), ZoneOffset.a(var0));
   }

   public static OffsetTime a(LocalTime var0, ZoneOffset var1) {
      return new OffsetTime(var0, var1);
   }

   public static OffsetTime a(TemporalAccessor var0) {
      OffsetTime var3;
      if(var0 instanceof OffsetTime) {
         var3 = (OffsetTime)var0;
      } else {
         OffsetTime var1;
         try {
            var1 = new OffsetTime(LocalTime.a(var0), ZoneOffset.b(var0));
         } catch (DateTimeException var2) {
            throw new DateTimeException("Unable to obtain OffsetTime from TemporalAccessor: " + var0 + ", type " + var0.getClass().getName());
         }

         var3 = var1;
      }

      return var3;
   }

   private long b() {
      return this.d.f() - (long)this.e.f() * 1000000000L;
   }

   private OffsetTime b(LocalTime var1, ZoneOffset var2) {
      OffsetTime var3;
      if(this.d == var1 && this.e.equals(var2)) {
         var3 = this;
      } else {
         var3 = new OffsetTime(var1, var2);
      }

      return var3;
   }

   private Object readResolve() throws ObjectStreamException {
      throw new InvalidObjectException("Deserialization via serialization delegate");
   }

   private Object writeReplace() {
      return new Ser(66, this);
   }

   public int a(OffsetTime var1) {
      int var2;
      if(this.e.equals(var1.e)) {
         var2 = this.d.a(var1.d);
      } else {
         int var3 = Jdk8Methods.a(this.b(), var1.b());
         var2 = var3;
         if(var3 == 0) {
            var2 = this.d.a(var1.d);
         }
      }

      return var2;
   }

   public long a(Temporal var1, TemporalUnit var2) {
      OffsetTime var5 = a((TemporalAccessor)var1);
      long var3;
      if(var2 instanceof ChronoUnit) {
         var3 = var5.b() - this.b();
         switch(null.a[((ChronoUnit)var2).ordinal()]) {
         case 1:
            break;
         case 2:
            var3 /= 1000L;
            break;
         case 3:
            var3 /= 1000000L;
            break;
         case 4:
            var3 /= 1000000000L;
            break;
         case 5:
            var3 /= 60000000000L;
            break;
         case 6:
            var3 /= 3600000000000L;
            break;
         case 7:
            var3 /= 43200000000000L;
            break;
         default:
            throw new UnsupportedTemporalTypeException("Unsupported unit: " + var2);
         }
      } else {
         var3 = var2.a(this, var5);
      }

      return var3;
   }

   public Object a(TemporalQuery var1) {
      Object var2;
      if(var1 == TemporalQueries.c()) {
         var2 = ChronoUnit.a;
      } else if(var1 != TemporalQueries.e() && var1 != TemporalQueries.d()) {
         if(var1 == TemporalQueries.g()) {
            var2 = this.d;
         } else if(var1 != TemporalQueries.b() && var1 != TemporalQueries.f() && var1 != TemporalQueries.a()) {
            var2 = super.a(var1);
         } else {
            var2 = null;
         }
      } else {
         var2 = this.a();
      }

      return var2;
   }

   public OffsetTime a(long var1, TemporalUnit var3) {
      OffsetTime var4;
      if(var3 instanceof ChronoUnit) {
         var4 = this.b(this.d.a(var1, var3), this.e);
      } else {
         var4 = (OffsetTime)var3.a(this, var1);
      }

      return var4;
   }

   public OffsetTime a(TemporalAdjuster var1) {
      OffsetTime var2;
      if(var1 instanceof LocalTime) {
         var2 = this.b((LocalTime)var1, this.e);
      } else if(var1 instanceof ZoneOffset) {
         var2 = this.b(this.d, (ZoneOffset)var1);
      } else if(var1 instanceof OffsetTime) {
         var2 = (OffsetTime)var1;
      } else {
         var2 = (OffsetTime)var1.a(this);
      }

      return var2;
   }

   public OffsetTime a(TemporalField var1, long var2) {
      OffsetTime var5;
      if(var1 instanceof ChronoField) {
         if(var1 == ChronoField.D) {
            ChronoField var4 = (ChronoField)var1;
            var5 = this.b(this.d, ZoneOffset.a(var4.b(var2)));
         } else {
            var5 = this.b(this.d.a(var1, var2), this.e);
         }
      } else {
         var5 = (OffsetTime)var1.a(this, var2);
      }

      return var5;
   }

   public ZoneOffset a() {
      return this.e;
   }

   public Temporal a(Temporal var1) {
      return var1.b(ChronoField.b, this.d.f()).b(ChronoField.D, (long)this.a().f());
   }

   void a(DataOutput var1) throws IOException {
      this.d.a(var1);
      this.e.b(var1);
   }

   public boolean a(TemporalField var1) {
      boolean var2 = true;
      boolean var3 = false;
      if(var1 instanceof ChronoField) {
         if(!var1.c()) {
            var2 = var3;
            if(var1 != ChronoField.D) {
               return var2;
            }
         }

         var2 = true;
      } else if(var1 == null || !var1.a(this)) {
         var2 = false;
      }

      return var2;
   }

   public OffsetTime b(long var1, TemporalUnit var3) {
      OffsetTime var4;
      if(var1 == Long.MIN_VALUE) {
         var4 = this.a(Long.MAX_VALUE, var3).a(1L, var3);
      } else {
         var4 = this.a(-var1, var3);
      }

      return var4;
   }

   // $FF: synthetic method
   public Temporal b(TemporalAdjuster var1) {
      return this.a(var1);
   }

   // $FF: synthetic method
   public Temporal b(TemporalField var1, long var2) {
      return this.a(var1, var2);
   }

   public ValueRange b(TemporalField var1) {
      ValueRange var2;
      if(var1 instanceof ChronoField) {
         if(var1 == ChronoField.D) {
            var2 = var1.a();
         } else {
            var2 = this.d.b(var1);
         }
      } else {
         var2 = var1.b(this);
      }

      return var2;
   }

   public int c(TemporalField var1) {
      return super.c(var1);
   }

   // $FF: synthetic method
   public Temporal c(long var1, TemporalUnit var3) {
      return this.b(var1, var3);
   }

   // $FF: synthetic method
   public int compareTo(Object var1) {
      return this.a((OffsetTime)var1);
   }

   public long d(TemporalField var1) {
      long var2;
      if(var1 instanceof ChronoField) {
         if(var1 == ChronoField.D) {
            var2 = (long)this.a().f();
         } else {
            var2 = this.d.d(var1);
         }
      } else {
         var2 = var1.c(this);
      }

      return var2;
   }

   // $FF: synthetic method
   public Temporal d(long var1, TemporalUnit var3) {
      return this.a(var1, var3);
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(this != var1) {
         if(var1 instanceof OffsetTime) {
            OffsetTime var3 = (OffsetTime)var1;
            if(!this.d.equals(var3.d) || !this.e.equals(var3.e)) {
               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public int hashCode() {
      return this.d.hashCode() ^ this.e.hashCode();
   }

   public String toString() {
      return this.d.toString() + this.e.toString();
   }
}
