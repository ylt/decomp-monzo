package org.threeten.bp.chrono;

import java.util.Iterator;
import java.util.List;
import org.threeten.bp.temporal.TemporalAmount;
import org.threeten.bp.temporal.TemporalUnit;

public abstract class ChronoPeriod implements TemporalAmount {
   public abstract long a(TemporalUnit var1);

   public abstract List a();

   public boolean b() {
      Iterator var2 = this.a().iterator();

      boolean var1;
      while(true) {
         if(var2.hasNext()) {
            if(this.a((TemporalUnit)var2.next()) == 0L) {
               continue;
            }

            var1 = false;
            break;
         }

         var1 = true;
         break;
      }

      return var1;
   }
}
