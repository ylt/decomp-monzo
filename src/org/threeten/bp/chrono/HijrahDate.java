package org.threeten.bp.chrono;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.ParseException;
import java.util.HashMap;
import java.util.StringTokenizer;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import org.threeten.bp.DateTimeException;
import org.threeten.bp.DayOfWeek;
import org.threeten.bp.LocalTime;
import org.threeten.bp.jdk8.Jdk8Methods;
import org.threeten.bp.temporal.ChronoField;
import org.threeten.bp.temporal.Temporal;
import org.threeten.bp.temporal.TemporalAdjuster;
import org.threeten.bp.temporal.TemporalAmount;
import org.threeten.bp.temporal.TemporalField;
import org.threeten.bp.temporal.TemporalUnit;
import org.threeten.bp.temporal.UnsupportedTemporalTypeException;
import org.threeten.bp.temporal.ValueRange;

public final class HijrahDate extends ChronoDateImpl implements Serializable {
   private static final int[] a;
   private static final int[] b;
   private static final int[] c;
   private static final int[] d;
   private static final int[] e;
   private static final int[] f;
   private static final int[] g;
   private static final int[] h;
   private static final char i;
   private static final String j;
   private static final String k;
   private static final HashMap l;
   private static final HashMap m;
   private static final HashMap n;
   private static final Long[] o;
   private static final Integer[] p;
   private static final Integer[] q;
   private static final Integer[] r;
   private static final Integer[] s;
   private static final Integer[] t;
   private static final Integer[] u;
   private static final Integer[] v;
   private static final Integer[] w;
   private final transient int A;
   private final transient int B;
   private final transient DayOfWeek C;
   private final long D;
   private final transient boolean E;
   private final transient HijrahEra x;
   private final transient int y;
   private final transient int z;

   static {
      byte var1 = 0;
      a = new int[]{0, 30, 59, 89, 118, 148, 177, 207, 236, 266, 295, 325};
      b = new int[]{0, 30, 59, 89, 118, 148, 177, 207, 236, 266, 295, 325};
      c = new int[]{30, 29, 30, 29, 30, 29, 30, 29, 30, 29, 30, 29};
      d = new int[]{30, 29, 30, 29, 30, 29, 30, 29, 30, 29, 30, 30};
      e = new int[]{0, 1, 0, 1, 0, 1, 1};
      f = new int[]{1, 9999, 11, 51, 5, 29, 354};
      g = new int[]{1, 9999, 11, 52, 6, 30, 355};
      h = new int[]{0, 354, 709, 1063, 1417, 1772, 2126, 2481, 2835, 3189, 3544, 3898, 4252, 4607, 4961, 5315, 5670, 6024, 6379, 6733, 7087, 7442, 7796, 8150, 8505, 8859, 9214, 9568, 9922, 10277};
      i = File.separatorChar;
      j = File.pathSeparator;
      k = "org" + i + "threeten" + i + "bp" + i + "chrono";
      l = new HashMap();
      m = new HashMap();
      n = new HashMap();
      s = new Integer[a.length];

      int var0;
      for(var0 = 0; var0 < a.length; ++var0) {
         s[var0] = new Integer(a[var0]);
      }

      t = new Integer[b.length];

      for(var0 = 0; var0 < b.length; ++var0) {
         t[var0] = new Integer(b[var0]);
      }

      u = new Integer[c.length];

      for(var0 = 0; var0 < c.length; ++var0) {
         u[var0] = new Integer(c[var0]);
      }

      v = new Integer[d.length];

      for(var0 = 0; var0 < d.length; ++var0) {
         v[var0] = new Integer(d[var0]);
      }

      w = new Integer[h.length];

      for(var0 = 0; var0 < h.length; ++var0) {
         w[var0] = new Integer(h[var0]);
      }

      o = new Long[334];

      for(var0 = 0; var0 < o.length; ++var0) {
         o[var0] = new Long((long)(var0 * 10631));
      }

      p = new Integer[e.length];

      for(var0 = 0; var0 < e.length; ++var0) {
         p[var0] = new Integer(e[var0]);
      }

      q = new Integer[f.length];

      for(var0 = 0; var0 < f.length; ++var0) {
         q[var0] = new Integer(f[var0]);
      }

      r = new Integer[g.length];

      for(var0 = var1; var0 < g.length; ++var0) {
         r[var0] = new Integer(g[var0]);
      }

      try {
         g();
      } catch (IOException var3) {
         ;
      } catch (ParseException var4) {
         ;
      }

   }

   private HijrahDate(long var1) {
      int[] var3 = i(var1);
      b(var3[1]);
      d(var3[2]);
      e(var3[3]);
      c(var3[4]);
      this.x = HijrahEra.a(var3[0]);
      this.y = var3[1];
      this.z = var3[2];
      this.A = var3[3];
      this.B = var3[4];
      this.C = DayOfWeek.a(var3[5]);
      this.D = var1;
      this.E = h((long)this.y);
   }

   static int a(int var0) {
      int var1 = (var0 - 1) / 30;

      Integer[] var2;
      try {
         var2 = (Integer[])n.get(Integer.valueOf(var1));
      } catch (ArrayIndexOutOfBoundsException var3) {
         var2 = null;
      }

      if(var2 != null) {
         var0 = (var0 - 1) % 30;
         if(var0 == 29) {
            var0 = o[var1 + 1].intValue() - o[var1].intValue() - var2[var0].intValue();
         } else {
            var0 = var2[var0 + 1].intValue() - var2[var0].intValue();
         }
      } else if(h((long)var0)) {
         var0 = 355;
      } else {
         var0 = 354;
      }

      return var0;
   }

   static int a(int var0, int var1) {
      return i(var1)[var0].intValue();
   }

   private static int a(int var0, long var1) {
      byte var4 = 0;
      byte var5 = 0;
      byte var3 = 0;
      Integer[] var6 = g(var0);
      if(var1 == 0L) {
         var0 = var3;
      } else if(var1 > 0L) {
         var0 = var4;

         while(true) {
            if(var0 >= var6.length) {
               var0 = 29;
               break;
            }

            if(var1 < (long)var6[var0].intValue()) {
               --var0;
               break;
            }

            ++var0;
         }
      } else {
         var1 = -var1;
         var0 = var5;

         while(true) {
            if(var0 >= var6.length) {
               var0 = 29;
               break;
            }

            if(var1 <= (long)var6[var0].intValue()) {
               --var0;
               break;
            }

            ++var0;
         }
      }

      return var0;
   }

   private static int a(long var0, int var2) {
      Long var3;
      try {
         var3 = o[var2];
      } catch (ArrayIndexOutOfBoundsException var5) {
         var3 = null;
      }

      Long var4 = var3;
      if(var3 == null) {
         var4 = new Long((long)(var2 * 10631));
      }

      return (int)(var0 - var4.longValue());
   }

   static ChronoLocalDate a(DataInput var0) throws IOException {
      int var3 = var0.readInt();
      byte var2 = var0.readByte();
      byte var1 = var0.readByte();
      return HijrahChronology.b.b(var3, var2, var1);
   }

   public static HijrahDate a(int var0, int var1, int var2) {
      HijrahDate var3;
      if(var0 >= 1) {
         var3 = a(HijrahEra.b, var0, var1, var2);
      } else {
         var3 = a(HijrahEra.a, 1 - var0, var1, var2);
      }

      return var3;
   }

   static HijrahDate a(HijrahEra var0, int var1, int var2, int var3) {
      Jdk8Methods.a(var0, (String)"era");
      b(var1);
      d(var2);
      e(var3);
      return new HijrahDate(c(var0.b(var1), var2, var3));
   }

   private static void a(int var0, int var1, int var2, int var3, int var4) {
      if(var0 < 1) {
         throw new IllegalArgumentException("startYear < 1");
      } else if(var2 < 1) {
         throw new IllegalArgumentException("endYear < 1");
      } else if(var1 >= 0 && var1 <= 11) {
         if(var3 >= 0 && var3 <= 11) {
            if(var2 > 9999) {
               throw new IllegalArgumentException("endYear > 9999");
            } else if(var2 < var0) {
               throw new IllegalArgumentException("startYear > endYear");
            } else if(var2 == var0 && var3 < var1) {
               throw new IllegalArgumentException("startYear == endYear && endMonth < startMonth");
            } else {
               boolean var7 = h((long)var0);
               Integer[] var9 = (Integer[])l.get(new Integer(var0));
               Integer[] var8 = var9;
               int var5;
               if(var9 == null) {
                  if(var7) {
                     var8 = new Integer[b.length];

                     for(var5 = 0; var5 < b.length; ++var5) {
                        var8[var5] = new Integer(b[var5]);
                     }
                  } else {
                     var8 = new Integer[a.length];

                     for(var5 = 0; var5 < a.length; ++var5) {
                        var8[var5] = new Integer(a[var5]);
                     }
                  }
               }

               var9 = new Integer[var8.length];

               for(var5 = 0; var5 < 12; ++var5) {
                  if(var5 > var1) {
                     var9[var5] = new Integer(var8[var5].intValue() - var4);
                  } else {
                     var9[var5] = new Integer(var8[var5].intValue());
                  }
               }

               l.put(new Integer(var0), var9);
               var9 = (Integer[])m.get(new Integer(var0));
               var8 = var9;
               if(var9 == null) {
                  if(var7) {
                     var8 = new Integer[d.length];

                     for(var5 = 0; var5 < d.length; ++var5) {
                        var8[var5] = new Integer(d[var5]);
                     }
                  } else {
                     var8 = new Integer[c.length];

                     for(var5 = 0; var5 < c.length; ++var5) {
                        var8[var5] = new Integer(c[var5]);
                     }
                  }
               }

               var9 = new Integer[var8.length];

               for(var5 = 0; var5 < 12; ++var5) {
                  if(var5 == var1) {
                     var9[var5] = new Integer(var8[var5].intValue() - var4);
                  } else {
                     var9[var5] = new Integer(var8[var5].intValue());
                  }
               }

               m.put(new Integer(var0), var9);
               int var6;
               if(var0 != var2) {
                  var6 = (var0 - 1) / 30;
                  var9 = (Integer[])n.get(new Integer(var6));
                  var8 = var9;
                  if(var9 == null) {
                     var8 = new Integer[h.length];

                     for(var5 = 0; var5 < var8.length; ++var5) {
                        var8[var5] = new Integer(h[var5]);
                     }
                  }

                  for(var5 = (var0 - 1) % 30 + 1; var5 < h.length; ++var5) {
                     var8[var5] = new Integer(var8[var5].intValue() - var4);
                  }

                  n.put(new Integer(var6), var8);
                  var5 = (var0 - 1) / 30;
                  var6 = (var2 - 1) / 30;
                  if(var5 != var6) {
                     ++var5;

                     while(var5 < o.length) {
                        o[var5] = new Long(o[var5].longValue() - (long)var4);
                        ++var5;
                     }

                     for(var5 = var6 + 1; var5 < o.length; ++var5) {
                        o[var5] = new Long(o[var5].longValue() + (long)var4);
                     }
                  }

                  var6 = (var2 - 1) / 30;
                  var9 = (Integer[])n.get(new Integer(var6));
                  var8 = var9;
                  if(var9 == null) {
                     var8 = new Integer[h.length];

                     for(var5 = 0; var5 < var8.length; ++var5) {
                        var8[var5] = new Integer(h[var5]);
                     }
                  }

                  for(var5 = (var2 - 1) % 30 + 1; var5 < h.length; ++var5) {
                     var8[var5] = new Integer(var8[var5].intValue() + var4);
                  }

                  n.put(new Integer(var6), var8);
               }

               var7 = h((long)var2);
               var9 = (Integer[])l.get(new Integer(var2));
               var8 = var9;
               if(var9 == null) {
                  if(var7) {
                     var8 = new Integer[b.length];

                     for(var5 = 0; var5 < b.length; ++var5) {
                        var8[var5] = new Integer(b[var5]);
                     }
                  } else {
                     var8 = new Integer[a.length];

                     for(var5 = 0; var5 < a.length; ++var5) {
                        var8[var5] = new Integer(a[var5]);
                     }
                  }
               }

               var9 = new Integer[var8.length];

               for(var5 = 0; var5 < 12; ++var5) {
                  if(var5 > var3) {
                     var9[var5] = new Integer(var8[var5].intValue() + var4);
                  } else {
                     var9[var5] = new Integer(var8[var5].intValue());
                  }
               }

               l.put(new Integer(var2), var9);
               var9 = (Integer[])m.get(new Integer(var2));
               var8 = var9;
               if(var9 == null) {
                  if(var7) {
                     var8 = new Integer[d.length];

                     for(var5 = 0; var5 < d.length; ++var5) {
                        var8[var5] = new Integer(d[var5]);
                     }
                  } else {
                     var8 = new Integer[c.length];

                     for(var5 = 0; var5 < c.length; ++var5) {
                        var8[var5] = new Integer(c[var5]);
                     }
                  }
               }

               var9 = new Integer[var8.length];

               for(var5 = 0; var5 < 12; ++var5) {
                  if(var5 == var3) {
                     var9[var5] = new Integer(var8[var5].intValue() + var4);
                  } else {
                     var9[var5] = new Integer(var8[var5].intValue());
                  }
               }

               m.put(new Integer(var2), var9);
               Integer[] var11 = (Integer[])m.get(new Integer(var0));
               Integer[] var10 = (Integer[])m.get(new Integer(var2));
               var8 = (Integer[])l.get(new Integer(var0));
               var9 = (Integer[])l.get(new Integer(var2));
               var4 = var11[var1].intValue();
               var3 = var10[var3].intValue();
               var2 = var8[11].intValue() + var11[11].intValue();
               var0 = var9[11].intValue();
               var1 = var10[11].intValue() + var0;
               var5 = r[5].intValue();
               var6 = q[5].intValue();
               var0 = var5;
               if(var5 < var4) {
                  var0 = var4;
               }

               var5 = var0;
               if(var0 < var3) {
                  var5 = var3;
               }

               r[5] = new Integer(var5);
               if(var6 > var4) {
                  var0 = var4;
               } else {
                  var0 = var6;
               }

               if(var0 <= var3) {
                  var3 = var0;
               }

               q[5] = new Integer(var3);
               var3 = r[6].intValue();
               var4 = q[6].intValue();
               var0 = var3;
               if(var3 < var2) {
                  var0 = var2;
               }

               var3 = var0;
               if(var0 < var1) {
                  var3 = var1;
               }

               r[6] = new Integer(var3);
               if(var4 > var2) {
                  var0 = var2;
               } else {
                  var0 = var4;
               }

               if(var0 > var1) {
                  var0 = var1;
               }

               q[6] = new Integer(var0);
            }
         } else {
            throw new IllegalArgumentException("endMonth < 0 || endMonth > 11");
         }
      } else {
         throw new IllegalArgumentException("startMonth < 0 || startMonth > 11");
      }
   }

   private static void a(String var0, int var1) throws ParseException {
      StringTokenizer var15 = new StringTokenizer(var0, ";");

      while(var15.hasMoreTokens()) {
         String var7 = var15.nextToken();
         int var3 = var7.indexOf(58);
         if(var3 != -1) {
            String var8 = var7.substring(var3 + 1, var7.length());

            int var2;
            try {
               var2 = Integer.parseInt(var8);
            } catch (NumberFormatException var14) {
               throw new ParseException("Offset is not properly set at line " + var1 + ".", var1);
            }

            int var4 = var7.indexOf(45);
            if(var4 == -1) {
               throw new ParseException("Start and end year/month has incorrect format at line " + var1 + ".", var1);
            }

            String var9 = var7.substring(0, var4);
            var7 = var7.substring(var4 + 1, var3);
            var3 = var9.indexOf(47);
            int var5 = var7.indexOf(47);
            if(var3 != -1) {
               var8 = var9.substring(0, var3);
               var9 = var9.substring(var3 + 1, var9.length());

               try {
                  var3 = Integer.parseInt(var8);
               } catch (NumberFormatException var13) {
                  throw new ParseException("Start year is not properly set at line " + var1 + ".", var1);
               }

               try {
                  var4 = Integer.parseInt(var9);
               } catch (NumberFormatException var12) {
                  throw new ParseException("Start month is not properly set at line " + var1 + ".", var1);
               }

               if(var5 != -1) {
                  var8 = var7.substring(0, var5);
                  var7 = var7.substring(var5 + 1, var7.length());

                  try {
                     var5 = Integer.parseInt(var8);
                  } catch (NumberFormatException var11) {
                     throw new ParseException("End year is not properly set at line " + var1 + ".", var1);
                  }

                  int var6;
                  try {
                     var6 = Integer.parseInt(var7);
                  } catch (NumberFormatException var10) {
                     throw new ParseException("End month is not properly set at line " + var1 + ".", var1);
                  }

                  if(var3 != -1 && var4 != -1 && var5 != -1 && var6 != -1) {
                     a(var3, var4, var5, var6, var2);
                     continue;
                  }

                  throw new ParseException("Unknown error at line " + var1 + ".", var1);
               }

               throw new ParseException("End year/month has incorrect format at line " + var1 + ".", var1);
            }

            throw new ParseException("Start year/month has incorrect format at line " + var1 + ".", var1);
         }

         throw new ParseException("Offset has incorrect format at line " + var1 + ".", var1);
      }

   }

   private static int b(int var0, int var1) {
      byte var2 = 0;
      byte var3 = 0;
      Integer[] var4 = h(var1);
      if(var0 >= 0) {
         var1 = var3;

         while(true) {
            if(var1 >= var4.length) {
               var0 = 11;
               break;
            }

            if(var0 < var4[var1].intValue()) {
               var0 = var1 - 1;
               break;
            }

            ++var1;
         }
      } else {
         if(h((long)var1)) {
            var0 += 355;
            var1 = var2;
         } else {
            var0 += 354;
            var1 = var2;
         }

         while(true) {
            if(var1 >= var4.length) {
               var0 = 11;
               break;
            }

            if(var0 < var4[var1].intValue()) {
               var0 = var1 - 1;
               break;
            }

            ++var1;
         }
      }

      return var0;
   }

   private static HijrahDate b(int var0, int var1, int var2) {
      int var4 = c(var1 - 1, var0);
      int var3 = var2;
      if(var2 > var4) {
         var3 = var4;
      }

      return a(var0, var1, var3);
   }

   private static void b(int var0) {
      if(var0 < 1 || var0 > 9999) {
         throw new DateTimeException("Invalid year of Hijrah Era");
      }
   }

   private static int c(int var0, int var1) {
      return h(var1)[var0].intValue();
   }

   private static long c(int var0, int var1, int var2) {
      return f(var0) + (long)c(var1 - 1, var0) + (long)var2;
   }

   private static void c(int var0) {
      if(var0 < 1 || var0 > f()) {
         throw new DateTimeException("Invalid day of year of Hijrah date");
      }
   }

   private static int d(int var0, int var1, int var2) {
      Integer[] var3 = g(var0);
      if(var1 > 0) {
         var0 = var1 - var3[var2].intValue();
      } else {
         var0 = var3[var2].intValue() + var1;
      }

      return var0;
   }

   static HijrahDate d(long var0) {
      return new HijrahDate(var0);
   }

   private static void d(int var0) {
      if(var0 < 1 || var0 > 12) {
         throw new DateTimeException("Invalid month of Hijrah date");
      }
   }

   static int e() {
      return r[5].intValue();
   }

   private static int e(int var0, int var1, int var2) {
      Integer[] var3 = h(var2);
      if(var0 >= 0) {
         var2 = var0;
         if(var1 > 0) {
            var2 = var0 - var3[var1].intValue();
         }
      } else {
         if(h((long)var2)) {
            var0 += 355;
         } else {
            var0 += 354;
         }

         var2 = var0;
         if(var1 > 0) {
            var2 = var0 - var3[var1].intValue();
         }
      }

      return var2;
   }

   private static void e(int var0) {
      if(var0 < 1 || var0 > e()) {
         throw new DateTimeException("Invalid day of month of Hijrah date, day " + var0 + " greater than " + e() + " or less than 1");
      }
   }

   static int f() {
      return r[6].intValue();
   }

   private static long f(int var0) {
      int var1 = (var0 - 1) / 30;
      int var2 = (var0 - 1) % 30;
      var0 = g(var1)[Math.abs(var2)].intValue();
      if(var2 < 0) {
         var0 = -var0;
      }

      Long var5;
      try {
         var5 = o[var1];
      } catch (ArrayIndexOutOfBoundsException var7) {
         var5 = null;
      }

      Long var6 = var5;
      if(var5 == null) {
         var6 = new Long((long)(var1 * 10631));
      }

      long var3 = var6.longValue();
      return (long)var0 + var3 - 492148L - 1L;
   }

   private static void g() throws IOException, ParseException {
      // $FF: Couldn't be decompiled
   }

   private static Integer[] g(int var0) {
      Integer[] var1;
      try {
         HashMap var2 = n;
         Integer var4 = new Integer(var0);
         var1 = (Integer[])var2.get(var4);
      } catch (ArrayIndexOutOfBoundsException var3) {
         var1 = null;
      }

      Integer[] var5 = var1;
      if(var1 == null) {
         var5 = w;
      }

      return var5;
   }

   private static InputStream h() throws IOException {
      String var0 = System.getProperty("org.threeten.bp.i18n.HijrahDate.deviationConfigFile");
      String var1 = var0;
      if(var0 == null) {
         var1 = "hijrah_deviation.cfg";
      }

      String var2 = System.getProperty("org.threeten.bp.i18n.HijrahDate.deviationConfigDir");
      Object var10;
      if(var2 != null) {
         label56: {
            if(var2.length() == 0) {
               var0 = var2;
               if(var2.endsWith(System.getProperty("file.separator"))) {
                  break label56;
               }
            }

            var0 = var2 + System.getProperty("file.separator");
         }

         File var9 = new File(var0 + i + var1);
         if(var9.exists()) {
            try {
               var10 = new FileInputStream(var9);
            } catch (IOException var8) {
               throw var8;
            }
         } else {
            var10 = null;
         }
      } else {
         StringTokenizer var4 = new StringTokenizer(System.getProperty("java.class.path"), j);

         while(true) {
            if(!var4.hasMoreTokens()) {
               var10 = null;
               break;
            }

            var0 = var4.nextToken();
            File var3 = new File(var0);
            if(var3.exists()) {
               if(var3.isDirectory()) {
                  if((new File(var0 + i + k, var1)).exists()) {
                     try {
                        StringBuilder var11 = new StringBuilder();
                        var10 = new FileInputStream(var11.append(var0).append(i).append(k).append(i).append(var1).toString());
                        break;
                     } catch (IOException var6) {
                        throw var6;
                     }
                  }
               } else {
                  ZipFile var12;
                  try {
                     var12 = new ZipFile(var3);
                  } catch (IOException var7) {
                     var12 = null;
                  }

                  if(var12 != null) {
                     String var14 = k + i + var1;
                     ZipEntry var13 = var12.getEntry(var14);
                     if(var13 == null) {
                        if(i == 47) {
                           var0 = var14.replace('/', '\\');
                        } else {
                           var0 = var14;
                           if(i == 92) {
                              var0 = var14.replace('\\', '/');
                           }
                        }

                        var13 = var12.getEntry(var0);
                     }

                     if(var13 != null) {
                        try {
                           var10 = var12.getInputStream(var13);
                           break;
                        } catch (IOException var5) {
                           throw var5;
                        }
                     }
                  }
               }
            }
         }
      }

      return (InputStream)var10;
   }

   static boolean h(long var0) {
      if(var0 <= 0L) {
         var0 = -var0;
      }

      boolean var2;
      if((14L + 11L * var0) % 30L < 11L) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   private static Integer[] h(int var0) {
      Integer[] var1;
      try {
         HashMap var4 = l;
         Integer var2 = new Integer(var0);
         var1 = (Integer[])var4.get(var2);
      } catch (ArrayIndexOutOfBoundsException var3) {
         var1 = null;
      }

      Integer[] var5 = var1;
      if(var1 == null) {
         if(h((long)var0)) {
            var5 = t;
         } else {
            var5 = s;
         }
      }

      return var5;
   }

   private static int[] i(long var0) {
      var0 += 492148L;
      int var2;
      int var3;
      int var4;
      int var5;
      int var6;
      if(var0 >= 0L) {
         var3 = j(var0);
         var4 = a(var0, var3);
         var2 = a(var3, (long)var4);
         var5 = d(var3, var4, var2);
         var3 = var3 * 30 + var2 + 1;
         var2 = b(var5, var3);
         var6 = e(var5, var2, var3) + 1;
         var4 = HijrahEra.b.a();
      } else {
         var4 = (int)var0 / 10631;
         var5 = (int)var0 % 10631;
         var3 = var5;
         var2 = var4;
         if(var5 == 0) {
            var3 = -10631;
            var2 = var4 + 1;
         }

         var4 = a(var2, (long)var3);
         var5 = d(var2, var3, var4);
         var3 = 1 - (var2 * 30 - var4);
         if(h((long)var3)) {
            var2 = var5 + 355;
         } else {
            var2 = var5 + 354;
         }

         int var7 = b(var2, var3);
         var6 = e(var2, var7, var3) + 1;
         var4 = HijrahEra.a.a();
         var5 = var2;
         var2 = var7;
      }

      int var8 = (int)((var0 + 5L) % 7L);
      byte var9;
      if(var8 <= 0) {
         var9 = 7;
      } else {
         var9 = 0;
      }

      return new int[]{var4, var3, var2 + 1, var6, var5 + 1, var9 + var8};
   }

   private static Integer[] i(int var0) {
      Integer[] var1;
      try {
         HashMap var4 = m;
         Integer var2 = new Integer(var0);
         var1 = (Integer[])var4.get(var2);
      } catch (ArrayIndexOutOfBoundsException var3) {
         var1 = null;
      }

      Integer[] var5 = var1;
      if(var1 == null) {
         if(h((long)var0)) {
            var5 = v;
         } else {
            var5 = u;
         }
      }

      return var5;
   }

   private static int j(long var0) {
      Long[] var3 = o;
      int var2 = 0;

      while(true) {
         try {
            if(var2 >= var3.length) {
               var2 = (int)var0 / 10631;
               return var2;
            }

            if(var0 < var3[var2].longValue()) {
               break;
            }
         } catch (ArrayIndexOutOfBoundsException var4) {
            var2 = (int)var0 / 10631;
            return var2;
         }

         ++var2;
      }

      --var2;
      return var2;
   }

   private Object readResolve() {
      return new HijrahDate(this.D);
   }

   private Object writeReplace() {
      return new Ser(3, this);
   }

   // $FF: synthetic method
   ChronoDateImpl a(long var1) {
      return this.e(var1);
   }

   // $FF: synthetic method
   public ChronoDateImpl a(long var1, TemporalUnit var3) {
      return this.b(var1, var3);
   }

   public HijrahChronology a() {
      return HijrahChronology.b;
   }

   public HijrahDate a(TemporalAdjuster var1) {
      return (HijrahDate)super.c(var1);
   }

   public HijrahDate a(TemporalAmount var1) {
      return (HijrahDate)super.b(var1);
   }

   public HijrahDate a(TemporalField var1, long var2) {
      HijrahDate var6;
      if(var1 instanceof ChronoField) {
         ChronoField var5 = (ChronoField)var1;
         var5.a(var2);
         int var4 = (int)var2;
         switch(null.a[var5.ordinal()]) {
         case 1:
            var6 = b(this.y, this.z, var4);
            break;
         case 2:
            var6 = b(this.y, (var4 - 1) / 30 + 1, (var4 - 1) % 30 + 1);
            break;
         case 3:
            var6 = this.g((var2 - this.d(ChronoField.v)) * 7L);
            break;
         case 4:
            if(this.y < 1) {
               var4 = 1 - var4;
            }

            var6 = b(var4, this.z, this.A);
            break;
         case 5:
            var6 = this.g(var2 - (long)this.C.a());
            break;
         case 6:
            var6 = this.g(var2 - this.d(ChronoField.q));
            break;
         case 7:
            var6 = this.g(var2 - this.d(ChronoField.r));
            break;
         case 8:
            var6 = new HijrahDate((long)var4);
            break;
         case 9:
            var6 = this.g((var2 - this.d(ChronoField.w)) * 7L);
            break;
         case 10:
            var6 = b(this.y, var4, this.A);
            break;
         case 11:
            var6 = b(var4, this.z, this.A);
            break;
         case 12:
            var6 = b(1 - this.y, this.z, this.A);
            break;
         default:
            throw new UnsupportedTemporalTypeException("Unsupported field: " + var1);
         }
      } else {
         var6 = (HijrahDate)var1.a(this, var2);
      }

      return var6;
   }

   void a(DataOutput var1) throws IOException {
      var1.writeInt(this.c((TemporalField)ChronoField.A));
      var1.writeByte(this.c((TemporalField)ChronoField.x));
      var1.writeByte(this.c((TemporalField)ChronoField.s));
   }

   // $FF: synthetic method
   ChronoDateImpl b(long var1) {
      return this.f(var1);
   }

   // $FF: synthetic method
   public ChronoLocalDate b(TemporalAmount var1) {
      return this.a(var1);
   }

   public final ChronoLocalDateTime b(LocalTime var1) {
      return super.b(var1);
   }

   public HijrahDate b(long var1, TemporalUnit var3) {
      return (HijrahDate)super.a(var1, var3);
   }

   public HijrahEra b() {
      return this.x;
   }

   // $FF: synthetic method
   public Temporal b(TemporalAdjuster var1) {
      return this.a(var1);
   }

   // $FF: synthetic method
   public Temporal b(TemporalField var1, long var2) {
      return this.a(var1, var2);
   }

   public ValueRange b(TemporalField var1) {
      ValueRange var3;
      if(var1 instanceof ChronoField) {
         if(!this.a((TemporalField)var1)) {
            throw new UnsupportedTemporalTypeException("Unsupported field: " + var1);
         }

         ChronoField var2 = (ChronoField)var1;
         switch(null.a[var2.ordinal()]) {
         case 1:
            var3 = ValueRange.a(1L, (long)this.d());
            break;
         case 2:
            var3 = ValueRange.a(1L, (long)this.l());
            break;
         case 3:
            var3 = ValueRange.a(1L, 5L);
            break;
         case 4:
            var3 = ValueRange.a(1L, 1000L);
            break;
         default:
            var3 = this.a().a(var2);
         }
      } else {
         var3 = var1.b(this);
      }

      return var3;
   }

   // $FF: synthetic method
   ChronoDateImpl c(long var1) {
      return this.g(var1);
   }

   // $FF: synthetic method
   public ChronoLocalDate c(TemporalAdjuster var1) {
      return this.a(var1);
   }

   // $FF: synthetic method
   public ChronoLocalDate c(TemporalField var1, long var2) {
      return this.a(var1, var2);
   }

   // $FF: synthetic method
   public Era c() {
      return this.b();
   }

   // $FF: synthetic method
   public Temporal c(long var1, TemporalUnit var3) {
      return this.g(var1, var3);
   }

   // $FF: synthetic method
   public Temporal c(TemporalAmount var1) {
      return this.a(var1);
   }

   public int d() {
      return a(this.z - 1, this.y);
   }

   public long d(TemporalField var1) {
      long var2;
      if(var1 instanceof ChronoField) {
         switch(null.a[((ChronoField)var1).ordinal()]) {
         case 1:
            var2 = (long)this.A;
            break;
         case 2:
            var2 = (long)this.B;
            break;
         case 3:
            var2 = (long)((this.A - 1) / 7 + 1);
            break;
         case 4:
            var2 = (long)this.y;
            break;
         case 5:
            var2 = (long)this.C.a();
            break;
         case 6:
            var2 = (long)((this.A - 1) % 7 + 1);
            break;
         case 7:
            var2 = (long)((this.B - 1) % 7 + 1);
            break;
         case 8:
            var2 = this.n();
            break;
         case 9:
            var2 = (long)((this.B - 1) / 7 + 1);
            break;
         case 10:
            var2 = (long)this.z;
            break;
         case 11:
            var2 = (long)this.y;
            break;
         case 12:
            var2 = (long)this.x.a();
            break;
         default:
            throw new UnsupportedTemporalTypeException("Unsupported field: " + var1);
         }
      } else {
         var2 = var1.c(this);
      }

      return var2;
   }

   // $FF: synthetic method
   public Temporal d(long var1, TemporalUnit var3) {
      return this.b(var1, var3);
   }

   // $FF: synthetic method
   public ChronoLocalDate e(long var1, TemporalUnit var3) {
      return this.g(var1, var3);
   }

   HijrahDate e(long var1) {
      HijrahDate var4;
      if(var1 == 0L) {
         var4 = this;
      } else {
         int var3 = Jdk8Methods.b(this.y, (int)var1);
         var4 = a(this.x, var3, this.z, this.A);
      }

      return var4;
   }

   // $FF: synthetic method
   public ChronoLocalDate f(long var1, TemporalUnit var3) {
      return this.b(var1, var3);
   }

   HijrahDate f(long var1) {
      HijrahDate var5;
      if(var1 == 0L) {
         var5 = this;
      } else {
         int var3 = this.z;
         var3 = (int)var1 + (var3 - 1);
         int var4 = var3 / 12;

         for(var3 %= 12; var3 < 0; var4 = Jdk8Methods.c(var4, 1)) {
            var3 += 12;
         }

         var4 = Jdk8Methods.b(this.y, var4);
         var5 = a(this.x, var4, var3 + 1, this.A);
      }

      return var5;
   }

   HijrahDate g(long var1) {
      return new HijrahDate(this.D + var1);
   }

   public HijrahDate g(long var1, TemporalUnit var3) {
      return (HijrahDate)super.e(var1, var3);
   }

   public boolean j() {
      return this.E;
   }

   public int l() {
      return a(this.y);
   }

   public long n() {
      return c(this.y, this.z, this.A);
   }

   // $FF: synthetic method
   public Chronology o() {
      return this.a();
   }
}
