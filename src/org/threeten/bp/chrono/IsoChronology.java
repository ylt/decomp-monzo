package org.threeten.bp.chrono;

import java.io.Serializable;
import java.util.Map;
import org.threeten.bp.DateTimeException;
import org.threeten.bp.DayOfWeek;
import org.threeten.bp.Instant;
import org.threeten.bp.LocalDate;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.Month;
import org.threeten.bp.Year;
import org.threeten.bp.ZoneId;
import org.threeten.bp.ZonedDateTime;
import org.threeten.bp.format.ResolverStyle;
import org.threeten.bp.jdk8.Jdk8Methods;
import org.threeten.bp.temporal.ChronoField;
import org.threeten.bp.temporal.TemporalAccessor;
import org.threeten.bp.temporal.TemporalAdjusters;
import org.threeten.bp.temporal.TemporalField;

public final class IsoChronology extends Chronology implements Serializable {
   public static final IsoChronology b = new IsoChronology();

   private Object readResolve() {
      return b;
   }

   public String a() {
      return "ISO";
   }

   public LocalDate a(Map var1, ResolverStyle var2) {
      LocalDate var16;
      if(var1.containsKey(ChronoField.u)) {
         var16 = LocalDate.a(((Long)var1.remove(ChronoField.u)).longValue());
      } else {
         Long var13 = (Long)var1.remove(ChronoField.y);
         if(var13 != null) {
            if(var2 != ResolverStyle.c) {
               ChronoField.y.a(var13.longValue());
            }

            this.a(var1, ChronoField.x, (long)(Jdk8Methods.b(var13.longValue(), 12) + 1));
            this.a(var1, ChronoField.A, Jdk8Methods.e(var13.longValue(), 12L));
         }

         var13 = (Long)var1.remove(ChronoField.z);
         long var7;
         if(var13 != null) {
            if(var2 != ResolverStyle.c) {
               ChronoField.z.a(var13.longValue());
            }

            Long var14 = (Long)var1.remove(ChronoField.B);
            if(var14 == null) {
               var14 = (Long)var1.get(ChronoField.A);
               ChronoField var15;
               if(var2 == ResolverStyle.a) {
                  if(var14 != null) {
                     var15 = ChronoField.A;
                     if(var14.longValue() > 0L) {
                        var7 = var13.longValue();
                     } else {
                        var7 = Jdk8Methods.c(1L, var13.longValue());
                     }

                     this.a(var1, var15, var7);
                  } else {
                     var1.put(ChronoField.z, var13);
                  }
               } else {
                  var15 = ChronoField.A;
                  if(var14 != null && var14.longValue() <= 0L) {
                     var7 = Jdk8Methods.c(1L, var13.longValue());
                  } else {
                     var7 = var13.longValue();
                  }

                  this.a(var1, var15, var7);
               }
            } else if(var14.longValue() == 1L) {
               this.a(var1, ChronoField.A, var13.longValue());
            } else {
               if(var14.longValue() != 0L) {
                  throw new DateTimeException("Invalid value for era: " + var14);
               }

               this.a(var1, ChronoField.A, Jdk8Methods.c(1L, var13.longValue()));
            }
         } else if(var1.containsKey(ChronoField.B)) {
            ChronoField.B.a(((Long)var1.get(ChronoField.B)).longValue());
         }

         if(var1.containsKey(ChronoField.A)) {
            int var3;
            int var4;
            int var5;
            long var9;
            LocalDate var17;
            if(var1.containsKey(ChronoField.x)) {
               int var6;
               if(var1.containsKey(ChronoField.s)) {
                  var5 = ChronoField.A.b(((Long)var1.remove(ChronoField.A)).longValue());
                  var6 = Jdk8Methods.a(((Long)var1.remove(ChronoField.x)).longValue());
                  var4 = Jdk8Methods.a(((Long)var1.remove(ChronoField.s)).longValue());
                  if(var2 == ResolverStyle.c) {
                     var7 = (long)Jdk8Methods.c(var6, 1);
                     var9 = (long)Jdk8Methods.c(var4, 1);
                     var16 = LocalDate.a(var5, 1, 1).c(var7).e(var9);
                     return var16;
                  } else {
                     if(var2 == ResolverStyle.b) {
                        ChronoField.s.a((long)var4);
                        if(var6 != 4 && var6 != 6 && var6 != 9 && var6 != 11) {
                           var3 = var4;
                           if(var6 == 2) {
                              var3 = Math.min(var4, Month.b.a(Year.a((long)var5)));
                           }
                        } else {
                           var3 = Math.min(var4, 30);
                        }

                        var16 = LocalDate.a(var5, var6, var3);
                     } else {
                        var16 = LocalDate.a(var5, var6, var4);
                     }

                     return var16;
                  }
               }

               if(var1.containsKey(ChronoField.v)) {
                  long var11;
                  if(var1.containsKey(ChronoField.q)) {
                     var3 = ChronoField.A.b(((Long)var1.remove(ChronoField.A)).longValue());
                     if(var2 == ResolverStyle.c) {
                        var7 = Jdk8Methods.c(((Long)var1.remove(ChronoField.x)).longValue(), 1L);
                        var11 = Jdk8Methods.c(((Long)var1.remove(ChronoField.v)).longValue(), 1L);
                        var9 = Jdk8Methods.c(((Long)var1.remove(ChronoField.q)).longValue(), 1L);
                        var16 = LocalDate.a(var3, 1, 1).c(var7).d(var11).e(var9);
                     } else {
                        var5 = ChronoField.x.b(((Long)var1.remove(ChronoField.x)).longValue());
                        var6 = ChronoField.v.b(((Long)var1.remove(ChronoField.v)).longValue());
                        var4 = ChronoField.q.b(((Long)var1.remove(ChronoField.q)).longValue());
                        var17 = LocalDate.a(var3, var5, 1).e((long)(var4 - 1 + (var6 - 1) * 7));
                        var16 = var17;
                        if(var2 == ResolverStyle.a) {
                           var16 = var17;
                           if(var17.c((TemporalField)ChronoField.x) != var5) {
                              throw new DateTimeException("Strict mode rejected date parsed to a different month");
                           }

                           return var16;
                        }
                     }

                     return var16;
                  }

                  if(var1.containsKey(ChronoField.p)) {
                     var5 = ChronoField.A.b(((Long)var1.remove(ChronoField.A)).longValue());
                     if(var2 == ResolverStyle.c) {
                        var7 = Jdk8Methods.c(((Long)var1.remove(ChronoField.x)).longValue(), 1L);
                        var11 = Jdk8Methods.c(((Long)var1.remove(ChronoField.v)).longValue(), 1L);
                        var9 = Jdk8Methods.c(((Long)var1.remove(ChronoField.p)).longValue(), 1L);
                        var16 = LocalDate.a(var5, 1, 1).c(var7).d(var11).e(var9);
                     } else {
                        var3 = ChronoField.x.b(((Long)var1.remove(ChronoField.x)).longValue());
                        var6 = ChronoField.v.b(((Long)var1.remove(ChronoField.v)).longValue());
                        var4 = ChronoField.p.b(((Long)var1.remove(ChronoField.p)).longValue());
                        var17 = LocalDate.a(var5, var3, 1).d((long)(var6 - 1)).a(TemporalAdjusters.a(DayOfWeek.a(var4)));
                        var16 = var17;
                        if(var2 == ResolverStyle.a) {
                           var16 = var17;
                           if(var17.c((TemporalField)ChronoField.x) != var3) {
                              throw new DateTimeException("Strict mode rejected date parsed to a different month");
                           }

                           return var16;
                        }
                     }

                     return var16;
                  }
               }
            }

            if(var1.containsKey(ChronoField.t)) {
               var3 = ChronoField.A.b(((Long)var1.remove(ChronoField.A)).longValue());
               if(var2 == ResolverStyle.c) {
                  var7 = Jdk8Methods.c(((Long)var1.remove(ChronoField.t)).longValue(), 1L);
                  var16 = LocalDate.a(var3, 1).e(var7);
               } else {
                  var16 = LocalDate.a(var3, ChronoField.t.b(((Long)var1.remove(ChronoField.t)).longValue()));
               }

               return var16;
            }

            if(var1.containsKey(ChronoField.w)) {
               if(var1.containsKey(ChronoField.r)) {
                  var3 = ChronoField.A.b(((Long)var1.remove(ChronoField.A)).longValue());
                  if(var2 == ResolverStyle.c) {
                     var7 = Jdk8Methods.c(((Long)var1.remove(ChronoField.w)).longValue(), 1L);
                     var9 = Jdk8Methods.c(((Long)var1.remove(ChronoField.r)).longValue(), 1L);
                     var16 = LocalDate.a(var3, 1, 1).d(var7).e(var9);
                  } else {
                     var4 = ChronoField.w.b(((Long)var1.remove(ChronoField.w)).longValue());
                     var5 = ChronoField.r.b(((Long)var1.remove(ChronoField.r)).longValue());
                     var17 = LocalDate.a(var3, 1, 1).e((long)(var5 - 1 + (var4 - 1) * 7));
                     var16 = var17;
                     if(var2 == ResolverStyle.a) {
                        var16 = var17;
                        if(var17.c((TemporalField)ChronoField.A) != var3) {
                           throw new DateTimeException("Strict mode rejected date parsed to a different year");
                        }

                        return var16;
                     }
                  }

                  return var16;
               }

               if(var1.containsKey(ChronoField.p)) {
                  var5 = ChronoField.A.b(((Long)var1.remove(ChronoField.A)).longValue());
                  if(var2 == ResolverStyle.c) {
                     var9 = Jdk8Methods.c(((Long)var1.remove(ChronoField.w)).longValue(), 1L);
                     var7 = Jdk8Methods.c(((Long)var1.remove(ChronoField.p)).longValue(), 1L);
                     var16 = LocalDate.a(var5, 1, 1).d(var9).e(var7);
                  } else {
                     var4 = ChronoField.w.b(((Long)var1.remove(ChronoField.w)).longValue());
                     var3 = ChronoField.p.b(((Long)var1.remove(ChronoField.p)).longValue());
                     var17 = LocalDate.a(var5, 1, 1).d((long)(var4 - 1)).a(TemporalAdjusters.a(DayOfWeek.a(var3)));
                     var16 = var17;
                     if(var2 == ResolverStyle.a) {
                        var16 = var17;
                        if(var17.c((TemporalField)ChronoField.A) != var5) {
                           throw new DateTimeException("Strict mode rejected date parsed to a different month");
                        }

                        return var16;
                     }
                  }

                  return var16;
               }
            }
         }

         var16 = null;
      }

      return var16;
   }

   // $FF: synthetic method
   public ChronoLocalDate a(int var1, int var2, int var3) {
      return this.b(var1, var2, var3);
   }

   // $FF: synthetic method
   public ChronoZonedDateTime a(Instant var1, ZoneId var2) {
      return this.b(var1, var2);
   }

   // $FF: synthetic method
   public Era a(int var1) {
      return this.b(var1);
   }

   public boolean a(long var1) {
      boolean var3;
      if((3L & var1) != 0L || var1 % 100L == 0L && var1 % 400L != 0L) {
         var3 = false;
      } else {
         var3 = true;
      }

      return var3;
   }

   public String b() {
      return "iso8601";
   }

   public LocalDate b(int var1, int var2, int var3) {
      return LocalDate.a(var1, var2, var3);
   }

   public ZonedDateTime b(Instant var1, ZoneId var2) {
      return ZonedDateTime.a(var1, var2);
   }

   // $FF: synthetic method
   public ChronoLocalDate b(TemporalAccessor var1) {
      return this.e(var1);
   }

   public IsoEra b(int var1) {
      return IsoEra.a(var1);
   }

   // $FF: synthetic method
   public ChronoLocalDateTime c(TemporalAccessor var1) {
      return this.f(var1);
   }

   // $FF: synthetic method
   public ChronoZonedDateTime d(TemporalAccessor var1) {
      return this.g(var1);
   }

   public LocalDate e(TemporalAccessor var1) {
      return LocalDate.a(var1);
   }

   public LocalDateTime f(TemporalAccessor var1) {
      return LocalDateTime.a(var1);
   }

   public ZonedDateTime g(TemporalAccessor var1) {
      return ZonedDateTime.a(var1);
   }
}
