package org.threeten.bp.chrono;

import org.threeten.bp.DateTimeException;
import org.threeten.bp.temporal.ChronoField;
import org.threeten.bp.temporal.ChronoUnit;
import org.threeten.bp.temporal.Temporal;
import org.threeten.bp.temporal.TemporalField;
import org.threeten.bp.temporal.TemporalQueries;
import org.threeten.bp.temporal.TemporalQuery;
import org.threeten.bp.temporal.UnsupportedTemporalTypeException;
import org.threeten.bp.temporal.ValueRange;

public enum IsoEra implements Era {
   a,
   b;

   public static IsoEra a(int var0) {
      IsoEra var1;
      switch(var0) {
      case 0:
         var1 = a;
         break;
      case 1:
         var1 = b;
         break;
      default:
         throw new DateTimeException("Invalid era: " + var0);
      }

      return var1;
   }

   public int a() {
      return this.ordinal();
   }

   public Object a(TemporalQuery var1) {
      Object var2;
      if(var1 == TemporalQueries.c()) {
         var2 = ChronoUnit.o;
      } else if(var1 != TemporalQueries.b() && var1 != TemporalQueries.d() && var1 != TemporalQueries.a() && var1 != TemporalQueries.e() && var1 != TemporalQueries.f() && var1 != TemporalQueries.g()) {
         var2 = var1.b(this);
      } else {
         var2 = null;
      }

      return var2;
   }

   public Temporal a(Temporal var1) {
      return var1.b(ChronoField.B, (long)this.a());
   }

   public boolean a(TemporalField var1) {
      boolean var2 = true;
      if(var1 instanceof ChronoField) {
         if(var1 != ChronoField.B) {
            var2 = false;
         }
      } else if(var1 == null || !var1.a(this)) {
         var2 = false;
      }

      return var2;
   }

   public ValueRange b(TemporalField var1) {
      ValueRange var2;
      if(var1 == ChronoField.B) {
         var2 = var1.a();
      } else {
         if(var1 instanceof ChronoField) {
            throw new UnsupportedTemporalTypeException("Unsupported field: " + var1);
         }

         var2 = var1.b(this);
      }

      return var2;
   }

   public int c(TemporalField var1) {
      int var2;
      if(var1 == ChronoField.B) {
         var2 = this.a();
      } else {
         var2 = this.b(var1).b(this.d(var1), var1);
      }

      return var2;
   }

   public long d(TemporalField var1) {
      long var2;
      if(var1 == ChronoField.B) {
         var2 = (long)this.a();
      } else {
         if(var1 instanceof ChronoField) {
            throw new UnsupportedTemporalTypeException("Unsupported field: " + var1);
         }

         var2 = var1.c(this);
      }

      return var2;
   }
}
