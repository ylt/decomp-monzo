package org.threeten.bp.chrono;

import java.util.Comparator;
import org.threeten.bp.Instant;
import org.threeten.bp.LocalDate;
import org.threeten.bp.LocalTime;
import org.threeten.bp.ZoneId;
import org.threeten.bp.ZoneOffset;
import org.threeten.bp.format.DateTimeFormatter;
import org.threeten.bp.jdk8.DefaultInterfaceTemporal;
import org.threeten.bp.jdk8.Jdk8Methods;
import org.threeten.bp.temporal.ChronoField;
import org.threeten.bp.temporal.ChronoUnit;
import org.threeten.bp.temporal.Temporal;
import org.threeten.bp.temporal.TemporalAccessor;
import org.threeten.bp.temporal.TemporalAdjuster;
import org.threeten.bp.temporal.TemporalAmount;
import org.threeten.bp.temporal.TemporalField;
import org.threeten.bp.temporal.TemporalQueries;
import org.threeten.bp.temporal.TemporalQuery;
import org.threeten.bp.temporal.TemporalUnit;
import org.threeten.bp.temporal.UnsupportedTemporalTypeException;
import org.threeten.bp.temporal.ValueRange;

public abstract class ChronoZonedDateTime extends DefaultInterfaceTemporal implements Comparable, Temporal {
   private static Comparator a = new Comparator() {
      public int a(ChronoZonedDateTime var1, ChronoZonedDateTime var2) {
         int var4 = Jdk8Methods.a(var1.k(), var2.k());
         int var3 = var4;
         if(var4 == 0) {
            var3 = Jdk8Methods.a(var1.f().f(), var2.f().f());
         }

         return var3;
      }

      // $FF: synthetic method
      public int compare(Object var1, Object var2) {
         return this.a((ChronoZonedDateTime)var1, (ChronoZonedDateTime)var2);
      }
   };

   public int a(ChronoZonedDateTime var1) {
      int var3 = Jdk8Methods.a(this.k(), var1.k());
      int var2 = var3;
      if(var3 == 0) {
         var3 = this.f().d() - var1.f().d();
         var2 = var3;
         if(var3 == 0) {
            var3 = this.h().a(var1.h());
            var2 = var3;
            if(var3 == 0) {
               var3 = this.b().c().compareTo(var1.b().c());
               var2 = var3;
               if(var3 == 0) {
                  var2 = this.i().o().a(var1.i().o());
               }
            }
         }
      }

      return var2;
   }

   public Object a(TemporalQuery var1) {
      Object var2;
      if(var1 != TemporalQueries.a() && var1 != TemporalQueries.d()) {
         if(var1 == TemporalQueries.b()) {
            var2 = this.i().o();
         } else if(var1 == TemporalQueries.c()) {
            var2 = ChronoUnit.a;
         } else if(var1 == TemporalQueries.e()) {
            var2 = this.a();
         } else if(var1 == TemporalQueries.f()) {
            var2 = LocalDate.a(this.i().n());
         } else if(var1 == TemporalQueries.g()) {
            var2 = this.f();
         } else {
            var2 = super.a(var1);
         }
      } else {
         var2 = this.b();
      }

      return var2;
   }

   public String a(DateTimeFormatter var1) {
      Jdk8Methods.a(var1, (String)"formatter");
      return var1.a((TemporalAccessor)this);
   }

   public abstract ZoneOffset a();

   public abstract ZoneId b();

   public ChronoZonedDateTime b(TemporalAmount var1) {
      return this.i().o().c(super.c(var1));
   }

   // $FF: synthetic method
   public Temporal b(TemporalAdjuster var1) {
      return this.c(var1);
   }

   // $FF: synthetic method
   public Temporal b(TemporalField var1, long var2) {
      return this.c(var1, var2);
   }

   public ValueRange b(TemporalField var1) {
      ValueRange var2;
      if(var1 instanceof ChronoField) {
         if(var1 != ChronoField.C && var1 != ChronoField.D) {
            var2 = this.h().b((TemporalField)var1);
         } else {
            var2 = var1.a();
         }
      } else {
         var2 = var1.b(this);
      }

      return var2;
   }

   public int c(TemporalField var1) {
      int var2;
      if(var1 instanceof ChronoField) {
         switch(null.a[((ChronoField)var1).ordinal()]) {
         case 1:
            throw new UnsupportedTemporalTypeException("Field too large for an int: " + var1);
         case 2:
            var2 = this.a().f();
            break;
         default:
            var2 = this.h().c((TemporalField)var1);
         }
      } else {
         var2 = super.c(var1);
      }

      return var2;
   }

   public abstract ChronoZonedDateTime c(ZoneId var1);

   public ChronoZonedDateTime c(TemporalAdjuster var1) {
      return this.i().o().c(super.b(var1));
   }

   public abstract ChronoZonedDateTime c(TemporalField var1, long var2);

   // $FF: synthetic method
   public Temporal c(long var1, TemporalUnit var3) {
      return this.e(var1, var3);
   }

   // $FF: synthetic method
   public Temporal c(TemporalAmount var1) {
      return this.b(var1);
   }

   // $FF: synthetic method
   public int compareTo(Object var1) {
      return this.a((ChronoZonedDateTime)var1);
   }

   public long d(TemporalField var1) {
      long var2;
      if(var1 instanceof ChronoField) {
         switch(null.a[((ChronoField)var1).ordinal()]) {
         case 1:
            var2 = this.k();
            break;
         case 2:
            var2 = (long)this.a().f();
            break;
         default:
            var2 = this.h().d(var1);
         }
      } else {
         var2 = var1.c(this);
      }

      return var2;
   }

   public abstract ChronoZonedDateTime d(ZoneId var1);

   // $FF: synthetic method
   public Temporal d(long var1, TemporalUnit var3) {
      return this.f(var1, var3);
   }

   public ChronoZonedDateTime e(long var1, TemporalUnit var3) {
      return this.i().o().c(super.c(var1, var3));
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(this != var1) {
         if(var1 instanceof ChronoZonedDateTime) {
            if(this.a((ChronoZonedDateTime)var1) != 0) {
               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public LocalTime f() {
      return this.h().k();
   }

   public abstract ChronoZonedDateTime f(long var1, TemporalUnit var3);

   public abstract ChronoLocalDateTime h();

   public int hashCode() {
      return this.h().hashCode() ^ this.a().hashCode() ^ Integer.rotateLeft(this.b().hashCode(), 3);
   }

   public ChronoLocalDate i() {
      return this.h().l();
   }

   public Instant j() {
      return Instant.a(this.k(), (long)this.f().d());
   }

   public long k() {
      return this.i().n() * 86400L + (long)this.f().e() - (long)this.a().f();
   }

   public String toString() {
      String var2 = this.h().toString() + this.a().toString();
      String var1 = var2;
      if(this.a() != this.b()) {
         var1 = var2 + '[' + this.b().toString() + ']';
      }

      return var1;
   }
}
