package org.threeten.bp.chrono;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.InvalidObjectException;
import java.io.ObjectStreamException;
import java.io.Serializable;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicReference;
import org.threeten.bp.DateTimeException;
import org.threeten.bp.LocalDate;
import org.threeten.bp.jdk8.DefaultInterfaceEra;
import org.threeten.bp.temporal.ChronoField;
import org.threeten.bp.temporal.TemporalField;
import org.threeten.bp.temporal.ValueRange;

public final class JapaneseEra extends DefaultInterfaceEra implements Serializable {
   public static final JapaneseEra a = new JapaneseEra(-1, LocalDate.a(1868, 9, 8), "Meiji");
   public static final JapaneseEra b = new JapaneseEra(0, LocalDate.a(1912, 7, 30), "Taisho");
   public static final JapaneseEra c = new JapaneseEra(1, LocalDate.a(1926, 12, 25), "Showa");
   public static final JapaneseEra d = new JapaneseEra(2, LocalDate.a(1989, 1, 8), "Heisei");
   private static final AtomicReference e;
   private final int f;
   private final transient LocalDate g;
   private final transient String h;

   static {
      e = new AtomicReference(new JapaneseEra[]{a, b, c, d});
   }

   private JapaneseEra(int var1, LocalDate var2, String var3) {
      this.f = var1;
      this.g = var2;
      this.h = var3;
   }

   public static JapaneseEra a(int var0) {
      JapaneseEra[] var1 = (JapaneseEra[])e.get();
      if(var0 >= a.f && var0 <= var1[var1.length - 1].f) {
         return var1[b(var0)];
      } else {
         throw new DateTimeException("japaneseEra is invalid");
      }
   }

   static JapaneseEra a(DataInput var0) throws IOException {
      return a(var0.readByte());
   }

   static JapaneseEra a(LocalDate var0) {
      if(var0.c((ChronoLocalDate)a.g)) {
         throw new DateTimeException("Date too early: " + var0);
      } else {
         JapaneseEra[] var3 = (JapaneseEra[])e.get();
         int var1 = var3.length - 1;

         JapaneseEra var4;
         while(true) {
            if(var1 < 0) {
               var4 = null;
               break;
            }

            JapaneseEra var2 = var3[var1];
            if(var0.a((ChronoLocalDate)var2.g) >= 0) {
               var4 = var2;
               break;
            }

            --var1;
         }

         return var4;
      }
   }

   private static int b(int var0) {
      return var0 + 1;
   }

   public static JapaneseEra[] b() {
      JapaneseEra[] var0 = (JapaneseEra[])e.get();
      return (JapaneseEra[])Arrays.copyOf(var0, var0.length);
   }

   private Object readResolve() throws ObjectStreamException {
      try {
         JapaneseEra var1 = a(this.f);
         return var1;
      } catch (DateTimeException var3) {
         InvalidObjectException var2 = new InvalidObjectException("Invalid era");
         var2.initCause(var3);
         throw var2;
      }
   }

   private Object writeReplace() {
      return new Ser(2, this);
   }

   public int a() {
      return this.f;
   }

   void a(DataOutput var1) throws IOException {
      var1.writeByte(this.a());
   }

   public ValueRange b(TemporalField var1) {
      ValueRange var2;
      if(var1 == ChronoField.B) {
         var2 = JapaneseChronology.c.a(ChronoField.B);
      } else {
         var2 = super.b(var1);
      }

      return var2;
   }

   LocalDate c() {
      return this.g;
   }

   LocalDate d() {
      int var1 = b(this.f);
      JapaneseEra[] var2 = b();
      LocalDate var3;
      if(var1 >= var2.length - 1) {
         var3 = LocalDate.b;
      } else {
         var3 = var2[var1 + 1].c().g(1L);
      }

      return var3;
   }

   public String toString() {
      return this.h;
   }
}
