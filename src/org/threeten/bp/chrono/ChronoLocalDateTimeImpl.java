package org.threeten.bp.chrono;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.io.Serializable;
import org.threeten.bp.LocalTime;
import org.threeten.bp.ZoneId;
import org.threeten.bp.ZoneOffset;
import org.threeten.bp.jdk8.Jdk8Methods;
import org.threeten.bp.temporal.ChronoField;
import org.threeten.bp.temporal.ChronoUnit;
import org.threeten.bp.temporal.Temporal;
import org.threeten.bp.temporal.TemporalAccessor;
import org.threeten.bp.temporal.TemporalAdjuster;
import org.threeten.bp.temporal.TemporalField;
import org.threeten.bp.temporal.TemporalUnit;
import org.threeten.bp.temporal.ValueRange;

final class ChronoLocalDateTimeImpl extends ChronoLocalDateTime implements Serializable, Temporal, TemporalAdjuster {
   private final ChronoLocalDate a;
   private final LocalTime b;

   private ChronoLocalDateTimeImpl(ChronoLocalDate var1, LocalTime var2) {
      Jdk8Methods.a(var1, (String)"date");
      Jdk8Methods.a(var2, (String)"time");
      this.a = var1;
      this.b = var2;
   }

   static ChronoLocalDateTime a(ObjectInput var0) throws IOException, ClassNotFoundException {
      return ((ChronoLocalDate)var0.readObject()).b((LocalTime)var0.readObject());
   }

   private ChronoLocalDateTimeImpl a(ChronoLocalDate var1, long var2, long var4, long var6, long var8) {
      ChronoLocalDateTimeImpl var21;
      if((var2 | var4 | var6 | var8) == 0L) {
         var21 = this.a((Temporal)var1, (LocalTime)this.b);
      } else {
         long var12 = var8 / 86400000000000L;
         long var14 = var6 / 86400L;
         long var16 = var4 / 1440L;
         long var18 = var2 / 24L;
         long var10 = this.b.f();
         var4 = var8 % 86400000000000L + var6 % 86400L * 1000000000L + var4 % 1440L * 60000000000L + var2 % 24L * 3600000000000L + var10;
         var2 = Jdk8Methods.e(var4, 86400000000000L);
         var4 = Jdk8Methods.f(var4, 86400000000000L);
         LocalTime var20;
         if(var4 == var10) {
            var20 = this.b;
         } else {
            var20 = LocalTime.b(var4);
         }

         var21 = this.a((Temporal)var1.f(var2 + var12 + var14 + var16 + var18, ChronoUnit.h), (LocalTime)var20);
      }

      return var21;
   }

   static ChronoLocalDateTimeImpl a(ChronoLocalDate var0, LocalTime var1) {
      return new ChronoLocalDateTimeImpl(var0, var1);
   }

   private ChronoLocalDateTimeImpl a(Temporal var1, LocalTime var2) {
      ChronoLocalDateTimeImpl var3;
      if(this.a == var1 && this.b == var2) {
         var3 = this;
      } else {
         var3 = new ChronoLocalDateTimeImpl(this.a.o().a(var1), var2);
      }

      return var3;
   }

   private ChronoLocalDateTimeImpl b(long var1) {
      return this.a((Temporal)this.a.f(var1, ChronoUnit.h), (LocalTime)this.b);
   }

   private ChronoLocalDateTimeImpl c(long var1) {
      return this.a(this.a, var1, 0L, 0L, 0L);
   }

   private ChronoLocalDateTimeImpl d(long var1) {
      return this.a(this.a, 0L, var1, 0L, 0L);
   }

   private ChronoLocalDateTimeImpl e(long var1) {
      return this.a(this.a, 0L, 0L, 0L, var1);
   }

   private Object writeReplace() {
      return new Ser(12, this);
   }

   public long a(Temporal var1, TemporalUnit var2) {
      ChronoLocalDateTime var6 = this.l().o().c((TemporalAccessor)var1);
      long var3;
      if(var2 instanceof ChronoUnit) {
         ChronoUnit var7 = (ChronoUnit)var2;
         if(var7.b()) {
            var3 = var6.d(ChronoField.u) - this.a.d(ChronoField.u);
            switch(null.a[var7.ordinal()]) {
            case 1:
               var3 = Jdk8Methods.d(var3, 86400000000000L);
               break;
            case 2:
               var3 = Jdk8Methods.d(var3, 86400000000L);
               break;
            case 3:
               var3 = Jdk8Methods.d(var3, 86400000L);
               break;
            case 4:
               var3 = Jdk8Methods.a(var3, 86400);
               break;
            case 5:
               var3 = Jdk8Methods.a(var3, 1440);
               break;
            case 6:
               var3 = Jdk8Methods.a(var3, 24);
               break;
            case 7:
               var3 = Jdk8Methods.a(var3, 2);
            }

            var3 = Jdk8Methods.b(var3, this.b.a(var6.k(), var2));
         } else {
            ChronoLocalDate var5 = var6.l();
            ChronoLocalDate var8 = var5;
            if(var6.k().c(this.b)) {
               var8 = var5.e(1L, ChronoUnit.h);
            }

            var3 = this.a.a(var8, var2);
         }
      } else {
         var3 = var2.a(this, var6);
      }

      return var3;
   }

   ChronoLocalDateTimeImpl a(long var1) {
      return this.a(this.a, 0L, 0L, var1, 0L);
   }

   public ChronoLocalDateTimeImpl a(long var1, TemporalUnit var3) {
      ChronoLocalDateTimeImpl var5;
      if(var3 instanceof ChronoUnit) {
         ChronoUnit var4 = (ChronoUnit)var3;
         switch(null.a[var4.ordinal()]) {
         case 1:
            var5 = this.e(var1);
            break;
         case 2:
            var5 = this.b(var1 / 86400000000L).e(var1 % 86400000000L * 1000L);
            break;
         case 3:
            var5 = this.b(var1 / 86400000L).e(var1 % 86400000L * 1000000L);
            break;
         case 4:
            var5 = this.a(var1);
            break;
         case 5:
            var5 = this.d(var1);
            break;
         case 6:
            var5 = this.c(var1);
            break;
         case 7:
            var5 = this.b(var1 / 256L).c(var1 % 256L * 12L);
            break;
         default:
            var5 = this.a((Temporal)this.a.f(var1, var3), (LocalTime)this.b);
         }
      } else {
         var5 = this.a.o().b(var3.a(this, var1));
      }

      return var5;
   }

   public ChronoLocalDateTimeImpl a(TemporalAdjuster var1) {
      ChronoLocalDateTimeImpl var2;
      if(var1 instanceof ChronoLocalDate) {
         var2 = this.a((Temporal)((ChronoLocalDate)var1), (LocalTime)this.b);
      } else if(var1 instanceof LocalTime) {
         var2 = this.a((Temporal)this.a, (LocalTime)((LocalTime)var1));
      } else if(var1 instanceof ChronoLocalDateTimeImpl) {
         var2 = this.a.o().b((Temporal)((ChronoLocalDateTimeImpl)var1));
      } else {
         var2 = this.a.o().b((Temporal)((ChronoLocalDateTimeImpl)var1.a(this)));
      }

      return var2;
   }

   public ChronoLocalDateTimeImpl a(TemporalField var1, long var2) {
      ChronoLocalDateTimeImpl var4;
      if(var1 instanceof ChronoField) {
         if(var1.c()) {
            var4 = this.a((Temporal)this.a, (LocalTime)this.b.a(var1, var2));
         } else {
            var4 = this.a((Temporal)this.a.c(var1, var2), (LocalTime)this.b);
         }
      } else {
         var4 = this.a.o().b(var1.a(this, var2));
      }

      return var4;
   }

   void a(ObjectOutput var1) throws IOException {
      var1.writeObject(this.a);
      var1.writeObject(this.b);
   }

   public boolean a(TemporalField var1) {
      boolean var2 = true;
      boolean var3 = false;
      if(var1 instanceof ChronoField) {
         if(!var1.b()) {
            var2 = var3;
            if(!var1.c()) {
               return var2;
            }
         }

         var2 = true;
      } else if(var1 == null || !var1.a(this)) {
         var2 = false;
      }

      return var2;
   }

   public ChronoZonedDateTime b(ZoneId var1) {
      return ChronoZonedDateTimeImpl.a((ChronoLocalDateTimeImpl)this, (ZoneId)var1, (ZoneOffset)null);
   }

   // $FF: synthetic method
   public Temporal b(TemporalAdjuster var1) {
      return this.a(var1);
   }

   // $FF: synthetic method
   public Temporal b(TemporalField var1, long var2) {
      return this.a(var1, var2);
   }

   public ValueRange b(TemporalField var1) {
      ValueRange var2;
      if(var1 instanceof ChronoField) {
         if(var1.c()) {
            var2 = this.b.b(var1);
         } else {
            var2 = this.a.b((TemporalField)var1);
         }
      } else {
         var2 = var1.b(this);
      }

      return var2;
   }

   public int c(TemporalField var1) {
      int var2;
      if(var1 instanceof ChronoField) {
         if(var1.c()) {
            var2 = this.b.c(var1);
         } else {
            var2 = this.a.c((TemporalField)var1);
         }
      } else {
         var2 = this.b(var1).b(this.d(var1), var1);
      }

      return var2;
   }

   // $FF: synthetic method
   public ChronoLocalDateTime c(TemporalAdjuster var1) {
      return this.a(var1);
   }

   // $FF: synthetic method
   public ChronoLocalDateTime c(TemporalField var1, long var2) {
      return this.a(var1, var2);
   }

   public long d(TemporalField var1) {
      long var2;
      if(var1 instanceof ChronoField) {
         if(var1.c()) {
            var2 = this.b.d(var1);
         } else {
            var2 = this.a.d(var1);
         }
      } else {
         var2 = var1.c(this);
      }

      return var2;
   }

   // $FF: synthetic method
   public Temporal d(long var1, TemporalUnit var3) {
      return this.a(var1, var3);
   }

   // $FF: synthetic method
   public ChronoLocalDateTime f(long var1, TemporalUnit var3) {
      return this.a(var1, var3);
   }

   public LocalTime k() {
      return this.b;
   }

   public ChronoLocalDate l() {
      return this.a;
   }
}
