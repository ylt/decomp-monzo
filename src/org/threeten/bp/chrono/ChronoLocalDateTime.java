package org.threeten.bp.chrono;

import java.util.Comparator;
import org.threeten.bp.Instant;
import org.threeten.bp.LocalDate;
import org.threeten.bp.LocalTime;
import org.threeten.bp.ZoneId;
import org.threeten.bp.ZoneOffset;
import org.threeten.bp.format.DateTimeFormatter;
import org.threeten.bp.jdk8.DefaultInterfaceTemporal;
import org.threeten.bp.jdk8.Jdk8Methods;
import org.threeten.bp.temporal.ChronoField;
import org.threeten.bp.temporal.ChronoUnit;
import org.threeten.bp.temporal.Temporal;
import org.threeten.bp.temporal.TemporalAccessor;
import org.threeten.bp.temporal.TemporalAdjuster;
import org.threeten.bp.temporal.TemporalAmount;
import org.threeten.bp.temporal.TemporalField;
import org.threeten.bp.temporal.TemporalQueries;
import org.threeten.bp.temporal.TemporalQuery;
import org.threeten.bp.temporal.TemporalUnit;

public abstract class ChronoLocalDateTime extends DefaultInterfaceTemporal implements Comparable, Temporal, TemporalAdjuster {
   private static final Comparator a = new Comparator() {
      public int a(ChronoLocalDateTime var1, ChronoLocalDateTime var2) {
         int var4 = Jdk8Methods.a(var1.l().n(), var2.l().n());
         int var3 = var4;
         if(var4 == 0) {
            var3 = Jdk8Methods.a(var1.k().f(), var2.k().f());
         }

         return var3;
      }

      // $FF: synthetic method
      public int compare(Object var1, Object var2) {
         return this.a((ChronoLocalDateTime)var1, (ChronoLocalDateTime)var2);
      }
   };

   public int a(ChronoLocalDateTime var1) {
      int var3 = this.l().a(var1.l());
      int var2 = var3;
      if(var3 == 0) {
         var3 = this.k().a(var1.k());
         var2 = var3;
         if(var3 == 0) {
            var2 = this.m().a(var1.m());
         }
      }

      return var2;
   }

   public Object a(TemporalQuery var1) {
      Object var2;
      if(var1 == TemporalQueries.b()) {
         var2 = this.m();
      } else if(var1 == TemporalQueries.c()) {
         var2 = ChronoUnit.a;
      } else if(var1 == TemporalQueries.f()) {
         var2 = LocalDate.a(this.l().n());
      } else if(var1 == TemporalQueries.g()) {
         var2 = this.k();
      } else if(var1 != TemporalQueries.d() && var1 != TemporalQueries.a() && var1 != TemporalQueries.e()) {
         var2 = super.a(var1);
      } else {
         var2 = null;
      }

      return var2;
   }

   public String a(DateTimeFormatter var1) {
      Jdk8Methods.a(var1, (String)"formatter");
      return var1.a((TemporalAccessor)this);
   }

   public Temporal a(Temporal var1) {
      return var1.b(ChronoField.u, this.l().n()).b(ChronoField.b, this.k().f());
   }

   public Instant b(ZoneOffset var1) {
      return Instant.a(this.c(var1), (long)this.k().d());
   }

   public ChronoLocalDateTime b(TemporalAmount var1) {
      return this.l().o().b(super.c(var1));
   }

   public abstract ChronoZonedDateTime b(ZoneId var1);

   // $FF: synthetic method
   public Temporal b(TemporalAdjuster var1) {
      return this.c(var1);
   }

   // $FF: synthetic method
   public Temporal b(TemporalField var1, long var2) {
      return this.c(var1, var2);
   }

   public boolean b(ChronoLocalDateTime var1) {
      long var5 = this.l().n();
      long var3 = var1.l().n();
      boolean var2;
      if(var5 <= var3 && (var5 != var3 || this.k().f() <= var1.k().f())) {
         var2 = false;
      } else {
         var2 = true;
      }

      return var2;
   }

   public long c(ZoneOffset var1) {
      Jdk8Methods.a(var1, (String)"offset");
      return this.l().n() * 86400L + (long)this.k().e() - (long)var1.f();
   }

   public ChronoLocalDateTime c(TemporalAdjuster var1) {
      return this.l().o().b(super.b(var1));
   }

   public abstract ChronoLocalDateTime c(TemporalField var1, long var2);

   // $FF: synthetic method
   public Temporal c(long var1, TemporalUnit var3) {
      return this.e(var1, var3);
   }

   // $FF: synthetic method
   public Temporal c(TemporalAmount var1) {
      return this.b(var1);
   }

   public boolean c(ChronoLocalDateTime var1) {
      long var5 = this.l().n();
      long var3 = var1.l().n();
      boolean var2;
      if(var5 >= var3 && (var5 != var3 || this.k().f() >= var1.k().f())) {
         var2 = false;
      } else {
         var2 = true;
      }

      return var2;
   }

   // $FF: synthetic method
   public int compareTo(Object var1) {
      return this.a((ChronoLocalDateTime)var1);
   }

   // $FF: synthetic method
   public Temporal d(long var1, TemporalUnit var3) {
      return this.f(var1, var3);
   }

   public ChronoLocalDateTime e(long var1, TemporalUnit var3) {
      return this.l().o().b(super.c(var1, var3));
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(this != var1) {
         if(var1 instanceof ChronoLocalDateTime) {
            if(this.a((ChronoLocalDateTime)var1) != 0) {
               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public abstract ChronoLocalDateTime f(long var1, TemporalUnit var3);

   public int hashCode() {
      return this.l().hashCode() ^ this.k().hashCode();
   }

   public abstract LocalTime k();

   public abstract ChronoLocalDate l();

   public Chronology m() {
      return this.l().o();
   }

   public String toString() {
      return this.l().toString() + 'T' + this.k().toString();
   }
}
