package org.threeten.bp.chrono;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.Serializable;
import org.threeten.bp.LocalDate;
import org.threeten.bp.LocalTime;
import org.threeten.bp.jdk8.Jdk8Methods;
import org.threeten.bp.temporal.ChronoField;
import org.threeten.bp.temporal.Temporal;
import org.threeten.bp.temporal.TemporalAdjuster;
import org.threeten.bp.temporal.TemporalAmount;
import org.threeten.bp.temporal.TemporalField;
import org.threeten.bp.temporal.TemporalUnit;
import org.threeten.bp.temporal.UnsupportedTemporalTypeException;
import org.threeten.bp.temporal.ValueRange;

public final class MinguoDate extends ChronoDateImpl implements Serializable {
   private final LocalDate a;

   MinguoDate(LocalDate var1) {
      Jdk8Methods.a(var1, (String)"date");
      this.a = var1;
   }

   static ChronoLocalDate a(DataInput var0) throws IOException {
      int var3 = var0.readInt();
      byte var2 = var0.readByte();
      byte var1 = var0.readByte();
      return MinguoChronology.b.b(var3, var2, var1);
   }

   private MinguoDate a(LocalDate var1) {
      MinguoDate var2;
      if(var1.equals(this.a)) {
         var2 = this;
      } else {
         var2 = new MinguoDate(var1);
      }

      return var2;
   }

   private long d() {
      return (long)this.e() * 12L + (long)this.a.e() - 1L;
   }

   private int e() {
      return this.a.d() - 1911;
   }

   private Object writeReplace() {
      return new Ser(5, this);
   }

   // $FF: synthetic method
   ChronoDateImpl a(long var1) {
      return this.d(var1);
   }

   // $FF: synthetic method
   public ChronoDateImpl a(long var1, TemporalUnit var3) {
      return this.b(var1, var3);
   }

   public MinguoChronology a() {
      return MinguoChronology.b;
   }

   public MinguoDate a(TemporalAdjuster var1) {
      return (MinguoDate)super.c(var1);
   }

   public MinguoDate a(TemporalAmount var1) {
      return (MinguoDate)super.b(var1);
   }

   public MinguoDate a(TemporalField var1, long var2) {
      MinguoDate var6;
      if(var1 instanceof ChronoField) {
         ChronoField var5 = (ChronoField)var1;
         if(this.d(var5) == var2) {
            var6 = this;
         } else {
            switch(null.a[var5.ordinal()]) {
            case 4:
            case 6:
            case 7:
               int var4 = this.a().a(var5).b(var2, var5);
               switch(null.a[var5.ordinal()]) {
               case 4:
                  LocalDate var7 = this.a;
                  if(this.e() >= 1) {
                     var4 += 1911;
                  } else {
                     var4 = 1 - var4 + 1911;
                  }

                  var6 = this.a(var7.a(var4));
                  return var6;
               case 5:
               default:
                  break;
               case 6:
                  var6 = this.a(this.a.a(var4 + 1911));
                  return var6;
               case 7:
                  var6 = this.a(this.a.a(1 - this.e() + 1911));
                  return var6;
               }
            default:
               var6 = this.a(this.a.a(var1, var2));
               break;
            case 5:
               this.a().a(var5).a(var2, var5);
               var6 = this.e(var2 - this.d());
            }
         }
      } else {
         var6 = (MinguoDate)var1.a(this, var2);
      }

      return var6;
   }

   void a(DataOutput var1) throws IOException {
      var1.writeInt(this.c((TemporalField)ChronoField.A));
      var1.writeByte(this.c((TemporalField)ChronoField.x));
      var1.writeByte(this.c((TemporalField)ChronoField.s));
   }

   // $FF: synthetic method
   ChronoDateImpl b(long var1) {
      return this.e(var1);
   }

   // $FF: synthetic method
   public ChronoLocalDate b(TemporalAmount var1) {
      return this.a(var1);
   }

   public final ChronoLocalDateTime b(LocalTime var1) {
      return super.b(var1);
   }

   public MinguoDate b(long var1, TemporalUnit var3) {
      return (MinguoDate)super.a(var1, var3);
   }

   public MinguoEra b() {
      return (MinguoEra)super.c();
   }

   // $FF: synthetic method
   public Temporal b(TemporalAdjuster var1) {
      return this.a(var1);
   }

   // $FF: synthetic method
   public Temporal b(TemporalField var1, long var2) {
      return this.a(var1, var2);
   }

   public ValueRange b(TemporalField var1) {
      ValueRange var5;
      if(var1 instanceof ChronoField) {
         if(!this.a((TemporalField)var1)) {
            throw new UnsupportedTemporalTypeException("Unsupported field: " + var1);
         }

         ChronoField var4 = (ChronoField)var1;
         switch(null.a[var4.ordinal()]) {
         case 1:
         case 2:
         case 3:
            var5 = this.a.b(var1);
            break;
         case 4:
            var5 = ChronoField.A.a();
            long var2;
            if(this.e() <= 0) {
               var2 = -var5.b() + 1L + 1911L;
            } else {
               var2 = var5.c() - 1911L;
            }

            var5 = ValueRange.a(1L, var2);
            break;
         default:
            var5 = this.a().a(var4);
         }
      } else {
         var5 = var1.b(this);
      }

      return var5;
   }

   // $FF: synthetic method
   ChronoDateImpl c(long var1) {
      return this.f(var1);
   }

   // $FF: synthetic method
   public ChronoLocalDate c(TemporalAdjuster var1) {
      return this.a(var1);
   }

   // $FF: synthetic method
   public ChronoLocalDate c(TemporalField var1, long var2) {
      return this.a(var1, var2);
   }

   // $FF: synthetic method
   public Era c() {
      return this.b();
   }

   // $FF: synthetic method
   public Temporal c(long var1, TemporalUnit var3) {
      return this.g(var1, var3);
   }

   // $FF: synthetic method
   public Temporal c(TemporalAmount var1) {
      return this.a(var1);
   }

   public long d(TemporalField var1) {
      long var3;
      if(var1 instanceof ChronoField) {
         switch(null.a[((ChronoField)var1).ordinal()]) {
         case 4:
            int var5 = this.e();
            if(var5 < 1) {
               var5 = 1 - var5;
            }

            var3 = (long)var5;
            break;
         case 5:
            var3 = this.d();
            break;
         case 6:
            var3 = (long)this.e();
            break;
         case 7:
            byte var2;
            if(this.e() >= 1) {
               var2 = 1;
            } else {
               var2 = 0;
            }

            var3 = (long)var2;
            break;
         default:
            var3 = this.a.d(var1);
         }
      } else {
         var3 = var1.c(this);
      }

      return var3;
   }

   MinguoDate d(long var1) {
      return this.a(this.a.b(var1));
   }

   // $FF: synthetic method
   public Temporal d(long var1, TemporalUnit var3) {
      return this.b(var1, var3);
   }

   // $FF: synthetic method
   public ChronoLocalDate e(long var1, TemporalUnit var3) {
      return this.g(var1, var3);
   }

   MinguoDate e(long var1) {
      return this.a(this.a.c(var1));
   }

   public boolean equals(Object var1) {
      boolean var2;
      if(this == var1) {
         var2 = true;
      } else if(var1 instanceof MinguoDate) {
         MinguoDate var3 = (MinguoDate)var1;
         var2 = this.a.equals(var3.a);
      } else {
         var2 = false;
      }

      return var2;
   }

   // $FF: synthetic method
   public ChronoLocalDate f(long var1, TemporalUnit var3) {
      return this.b(var1, var3);
   }

   MinguoDate f(long var1) {
      return this.a(this.a.e(var1));
   }

   public MinguoDate g(long var1, TemporalUnit var3) {
      return (MinguoDate)super.e(var1, var3);
   }

   public int hashCode() {
      return this.a().a().hashCode() ^ this.a.hashCode();
   }

   public long n() {
      return this.a.n();
   }

   // $FF: synthetic method
   public Chronology o() {
      return this.a();
   }
}
