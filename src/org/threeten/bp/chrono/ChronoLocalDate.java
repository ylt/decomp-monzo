package org.threeten.bp.chrono;

import java.util.Comparator;
import org.threeten.bp.LocalDate;
import org.threeten.bp.LocalTime;
import org.threeten.bp.format.DateTimeFormatter;
import org.threeten.bp.jdk8.DefaultInterfaceTemporal;
import org.threeten.bp.jdk8.Jdk8Methods;
import org.threeten.bp.temporal.ChronoField;
import org.threeten.bp.temporal.ChronoUnit;
import org.threeten.bp.temporal.Temporal;
import org.threeten.bp.temporal.TemporalAccessor;
import org.threeten.bp.temporal.TemporalAdjuster;
import org.threeten.bp.temporal.TemporalAmount;
import org.threeten.bp.temporal.TemporalField;
import org.threeten.bp.temporal.TemporalQueries;
import org.threeten.bp.temporal.TemporalQuery;
import org.threeten.bp.temporal.TemporalUnit;

public abstract class ChronoLocalDate extends DefaultInterfaceTemporal implements Comparable, Temporal, TemporalAdjuster {
   private static final Comparator a = new Comparator() {
      public int a(ChronoLocalDate var1, ChronoLocalDate var2) {
         return Jdk8Methods.a(var1.n(), var2.n());
      }

      // $FF: synthetic method
      public int compare(Object var1, Object var2) {
         return this.a((ChronoLocalDate)var1, (ChronoLocalDate)var2);
      }
   };

   public int a(ChronoLocalDate var1) {
      int var3 = Jdk8Methods.a(this.n(), var1.n());
      int var2 = var3;
      if(var3 == 0) {
         var2 = this.o().a(var1.o());
      }

      return var2;
   }

   public Object a(TemporalQuery var1) {
      Object var2;
      if(var1 == TemporalQueries.b()) {
         var2 = this.o();
      } else if(var1 == TemporalQueries.c()) {
         var2 = ChronoUnit.h;
      } else if(var1 == TemporalQueries.f()) {
         var2 = LocalDate.a(this.n());
      } else if(var1 != TemporalQueries.g() && var1 != TemporalQueries.d() && var1 != TemporalQueries.a() && var1 != TemporalQueries.e()) {
         var2 = super.a(var1);
      } else {
         var2 = null;
      }

      return var2;
   }

   public String a(DateTimeFormatter var1) {
      Jdk8Methods.a(var1, (String)"formatter");
      return var1.a((TemporalAccessor)this);
   }

   public Temporal a(Temporal var1) {
      return var1.b(ChronoField.u, this.n());
   }

   public boolean a(TemporalField var1) {
      boolean var2;
      if(var1 instanceof ChronoField) {
         var2 = var1.b();
      } else if(var1 != null && var1.a(this)) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   public ChronoLocalDate b(TemporalAmount var1) {
      return this.o().a(super.c(var1));
   }

   public ChronoLocalDateTime b(LocalTime var1) {
      return ChronoLocalDateTimeImpl.a(this, var1);
   }

   // $FF: synthetic method
   public Temporal b(TemporalAdjuster var1) {
      return this.c(var1);
   }

   // $FF: synthetic method
   public Temporal b(TemporalField var1, long var2) {
      return this.c(var1, var2);
   }

   public boolean b(ChronoLocalDate var1) {
      boolean var2;
      if(this.n() > var1.n()) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   public ChronoLocalDate c(TemporalAdjuster var1) {
      return this.o().a(super.b(var1));
   }

   public abstract ChronoLocalDate c(TemporalField var1, long var2);

   public Era c() {
      return this.o().a(this.c((TemporalField)ChronoField.B));
   }

   // $FF: synthetic method
   public Temporal c(long var1, TemporalUnit var3) {
      return this.e(var1, var3);
   }

   // $FF: synthetic method
   public Temporal c(TemporalAmount var1) {
      return this.b(var1);
   }

   public boolean c(ChronoLocalDate var1) {
      boolean var2;
      if(this.n() < var1.n()) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   // $FF: synthetic method
   public int compareTo(Object var1) {
      return this.a((ChronoLocalDate)var1);
   }

   // $FF: synthetic method
   public Temporal d(long var1, TemporalUnit var3) {
      return this.f(var1, var3);
   }

   public boolean d(ChronoLocalDate var1) {
      boolean var2;
      if(this.n() == var1.n()) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   public ChronoLocalDate e(long var1, TemporalUnit var3) {
      return this.o().a(super.c(var1, var3));
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(this != var1) {
         if(var1 instanceof ChronoLocalDate) {
            if(this.a((ChronoLocalDate)var1) != 0) {
               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public abstract ChronoLocalDate f(long var1, TemporalUnit var3);

   public int hashCode() {
      long var2 = this.n();
      int var1 = this.o().hashCode();
      return (int)(var2 ^ var2 >>> 32) ^ var1;
   }

   public boolean j() {
      return this.o().a(this.d(ChronoField.A));
   }

   public int l() {
      short var1;
      if(this.j()) {
         var1 = 366;
      } else {
         var1 = 365;
      }

      return var1;
   }

   public long n() {
      return this.d(ChronoField.u);
   }

   public abstract Chronology o();

   public String toString() {
      long var5 = this.d(ChronoField.z);
      long var3 = this.d(ChronoField.x);
      long var1 = this.d(ChronoField.s);
      StringBuilder var8 = new StringBuilder(30);
      StringBuilder var9 = var8.append(this.o().toString()).append(" ").append(this.c()).append(" ").append(var5);
      String var7;
      if(var3 < 10L) {
         var7 = "-0";
      } else {
         var7 = "-";
      }

      var9 = var9.append(var7).append(var3);
      if(var1 < 10L) {
         var7 = "-0";
      } else {
         var7 = "-";
      }

      var9.append(var7).append(var1);
      return var8.toString();
   }
}
