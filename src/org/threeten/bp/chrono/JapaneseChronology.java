package org.threeten.bp.chrono;

import java.io.Serializable;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import org.threeten.bp.Instant;
import org.threeten.bp.LocalDate;
import org.threeten.bp.ZoneId;
import org.threeten.bp.temporal.ChronoField;
import org.threeten.bp.temporal.TemporalAccessor;
import org.threeten.bp.temporal.ValueRange;

public final class JapaneseChronology extends Chronology implements Serializable {
   static final Locale b = new Locale("ja", "JP", "JP");
   public static final JapaneseChronology c = new JapaneseChronology();
   private static final Map d = new HashMap();
   private static final Map e = new HashMap();
   private static final Map f = new HashMap();

   static {
      d.put("en", new String[]{"Unknown", "K", "M", "T", "S", "H"});
      d.put("ja", new String[]{"Unknown", "K", "M", "T", "S", "H"});
      e.put("en", new String[]{"Unknown", "K", "M", "T", "S", "H"});
      e.put("ja", new String[]{"Unknown", "慶", "明", "大", "昭", "平"});
      f.put("en", new String[]{"Unknown", "Keio", "Meiji", "Taisho", "Showa", "Heisei"});
      f.put("ja", new String[]{"Unknown", "慶応", "明治", "大正", "昭和", "平成"});
   }

   private Object readResolve() {
      return c;
   }

   public int a(Era var1, int var2) {
      if(!(var1 instanceof JapaneseEra)) {
         throw new ClassCastException("Era must be JapaneseEra");
      } else {
         JapaneseEra var4 = (JapaneseEra)var1;
         int var3 = var4.c().d();
         ValueRange.a(1L, (long)(var4.d().d() - var4.c().d() + 1)).a((long)var2, ChronoField.z);
         return var3 + var2 - 1;
      }
   }

   public String a() {
      return "Japanese";
   }

   // $FF: synthetic method
   public ChronoLocalDate a(int var1, int var2, int var3) {
      return this.b(var1, var2, var3);
   }

   public ChronoZonedDateTime a(Instant var1, ZoneId var2) {
      return super.a(var1, var2);
   }

   // $FF: synthetic method
   public Era a(int var1) {
      return this.b(var1);
   }

   public ValueRange a(ChronoField var1) {
      byte var4 = 0;
      int var2 = 0;
      ValueRange var7;
      switch(null.a[var1.ordinal()]) {
      case 1:
      case 2:
      case 3:
      case 4:
      case 5:
      case 6:
      case 7:
      case 8:
      case 9:
      case 10:
      case 11:
      case 12:
      case 13:
      case 14:
      case 15:
      case 16:
      case 17:
      case 18:
         var7 = var1.a();
         break;
      default:
         Calendar var6 = Calendar.getInstance(b);
         int var3;
         JapaneseEra[] var8;
         switch(null.a[var1.ordinal()]) {
         case 19:
            var8 = JapaneseEra.b();
            var7 = ValueRange.a((long)var8[0].a(), (long)var8[var8.length - 1].a());
            break;
         case 20:
            var8 = JapaneseEra.b();
            var7 = ValueRange.a((long)JapaneseDate.a.d(), (long)var8[var8.length - 1].d().d());
            break;
         case 21:
            var8 = JapaneseEra.b();
            int var5 = var8[var8.length - 1].d().d();
            int var9 = var8[var8.length - 1].c().d();

            for(var3 = Integer.MAX_VALUE; var2 < var8.length; ++var2) {
               var3 = Math.min(var3, var8[var2].d().d() - var8[var2].c().d() + 1);
            }

            var7 = ValueRange.a(1L, 6L, (long)var3, (long)(var5 - var9 + 1));
            break;
         case 22:
            var7 = ValueRange.a((long)(var6.getMinimum(2) + 1), (long)(var6.getGreatestMinimum(2) + 1), (long)(var6.getLeastMaximum(2) + 1), (long)(var6.getMaximum(2) + 1));
            break;
         case 23:
            var8 = JapaneseEra.b();
            var3 = 366;

            for(var2 = var4; var2 < var8.length; ++var2) {
               var3 = Math.min(var3, var8[var2].c().l() - var8[var2].c().h() + 1);
            }

            var7 = ValueRange.a(1L, (long)var3, 366L);
            break;
         default:
            throw new UnsupportedOperationException("Unimplementable field: " + var1);
         }
      }

      return var7;
   }

   public boolean a(long var1) {
      return IsoChronology.b.a(var1);
   }

   public String b() {
      return "japanese";
   }

   // $FF: synthetic method
   public ChronoLocalDate b(TemporalAccessor var1) {
      return this.e(var1);
   }

   public JapaneseDate b(int var1, int var2, int var3) {
      return new JapaneseDate(LocalDate.a(var1, var2, var3));
   }

   public JapaneseEra b(int var1) {
      return JapaneseEra.a(var1);
   }

   public ChronoLocalDateTime c(TemporalAccessor var1) {
      return super.c(var1);
   }

   public ChronoZonedDateTime d(TemporalAccessor var1) {
      return super.d(var1);
   }

   public JapaneseDate e(TemporalAccessor var1) {
      JapaneseDate var2;
      if(var1 instanceof JapaneseDate) {
         var2 = (JapaneseDate)var1;
      } else {
         var2 = new JapaneseDate(LocalDate.a(var1));
      }

      return var2;
   }
}
