package org.threeten.bp.chrono;

import java.io.Serializable;
import org.threeten.bp.DateTimeException;
import org.threeten.bp.LocalDate;
import org.threeten.bp.LocalTime;
import org.threeten.bp.jdk8.Jdk8Methods;
import org.threeten.bp.temporal.ChronoUnit;
import org.threeten.bp.temporal.Temporal;
import org.threeten.bp.temporal.TemporalAccessor;
import org.threeten.bp.temporal.TemporalAdjuster;
import org.threeten.bp.temporal.TemporalUnit;

abstract class ChronoDateImpl extends ChronoLocalDate implements Serializable, Temporal, TemporalAdjuster {
   public long a(Temporal var1, TemporalUnit var2) {
      ChronoLocalDate var5 = this.o().b((TemporalAccessor)var1);
      long var3;
      if(var2 instanceof ChronoUnit) {
         var3 = LocalDate.a((TemporalAccessor)this).a((Temporal)var5, (TemporalUnit)var2);
      } else {
         var3 = var2.a(this, var5);
      }

      return var3;
   }

   abstract ChronoDateImpl a(long var1);

   public ChronoDateImpl a(long var1, TemporalUnit var3) {
      ChronoDateImpl var5;
      if(var3 instanceof ChronoUnit) {
         ChronoUnit var4 = (ChronoUnit)var3;
         switch(null.a[var4.ordinal()]) {
         case 1:
            var5 = this.c(var1);
            break;
         case 2:
            var5 = this.c(Jdk8Methods.a(var1, 7));
            break;
         case 3:
            var5 = this.b(var1);
            break;
         case 4:
            var5 = this.a(var1);
            break;
         case 5:
            var5 = this.a(Jdk8Methods.a(var1, 10));
            break;
         case 6:
            var5 = this.a(Jdk8Methods.a(var1, 100));
            break;
         case 7:
            var5 = this.a(Jdk8Methods.a(var1, 1000));
            break;
         default:
            throw new DateTimeException(var3 + " not valid for chronology " + this.o().a());
         }
      } else {
         var5 = (ChronoDateImpl)this.o().a(var3.a(this, var1));
      }

      return var5;
   }

   abstract ChronoDateImpl b(long var1);

   public ChronoLocalDateTime b(LocalTime var1) {
      return ChronoLocalDateTimeImpl.a((ChronoLocalDate)this, (LocalTime)var1);
   }

   abstract ChronoDateImpl c(long var1);

   // $FF: synthetic method
   public Temporal d(long var1, TemporalUnit var3) {
      return this.a(var1, var3);
   }

   // $FF: synthetic method
   public ChronoLocalDate f(long var1, TemporalUnit var3) {
      return this.a(var1, var3);
   }
}
