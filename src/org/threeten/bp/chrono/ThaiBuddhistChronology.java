package org.threeten.bp.chrono;

import java.io.Serializable;
import java.util.HashMap;
import org.threeten.bp.Instant;
import org.threeten.bp.LocalDate;
import org.threeten.bp.ZoneId;
import org.threeten.bp.temporal.ChronoField;
import org.threeten.bp.temporal.TemporalAccessor;
import org.threeten.bp.temporal.ValueRange;

public final class ThaiBuddhistChronology extends Chronology implements Serializable {
   public static final ThaiBuddhistChronology b = new ThaiBuddhistChronology();
   private static final HashMap c = new HashMap();
   private static final HashMap d = new HashMap();
   private static final HashMap e = new HashMap();

   static {
      c.put("en", new String[]{"BB", "BE"});
      c.put("th", new String[]{"BB", "BE"});
      d.put("en", new String[]{"B.B.", "B.E."});
      d.put("th", new String[]{"พ.ศ.", "ปีก่อนคริสต์กาลที่"});
      e.put("en", new String[]{"Before Buddhist", "Budhhist Era"});
      e.put("th", new String[]{"พุทธศักราช", "ปีก่อนคริสต์กาลที่"});
   }

   private Object readResolve() {
      return b;
   }

   public String a() {
      return "ThaiBuddhist";
   }

   // $FF: synthetic method
   public ChronoLocalDate a(int var1, int var2, int var3) {
      return this.b(var1, var2, var3);
   }

   public ChronoZonedDateTime a(Instant var1, ZoneId var2) {
      return super.a(var1, var2);
   }

   // $FF: synthetic method
   public Era a(int var1) {
      return this.b(var1);
   }

   public ValueRange a(ChronoField var1) {
      ValueRange var2;
      switch(null.a[var1.ordinal()]) {
      case 1:
         var2 = ChronoField.y.a();
         var2 = ValueRange.a(var2.b() + 6516L, var2.c() + 6516L);
         break;
      case 2:
         var2 = ChronoField.A.a();
         var2 = ValueRange.a(1L, -(var2.b() + 543L) + 1L, var2.c() + 543L);
         break;
      case 3:
         var2 = ChronoField.A.a();
         var2 = ValueRange.a(var2.b() + 543L, var2.c() + 543L);
         break;
      default:
         var2 = var1.a();
      }

      return var2;
   }

   public boolean a(long var1) {
      return IsoChronology.b.a(var1 - 543L);
   }

   public String b() {
      return "buddhist";
   }

   // $FF: synthetic method
   public ChronoLocalDate b(TemporalAccessor var1) {
      return this.e(var1);
   }

   public ThaiBuddhistDate b(int var1, int var2, int var3) {
      return new ThaiBuddhistDate(LocalDate.a(var1 - 543, var2, var3));
   }

   public ThaiBuddhistEra b(int var1) {
      return ThaiBuddhistEra.a(var1);
   }

   public ChronoLocalDateTime c(TemporalAccessor var1) {
      return super.c(var1);
   }

   public ChronoZonedDateTime d(TemporalAccessor var1) {
      return super.d(var1);
   }

   public ThaiBuddhistDate e(TemporalAccessor var1) {
      ThaiBuddhistDate var2;
      if(var1 instanceof ThaiBuddhistDate) {
         var2 = (ThaiBuddhistDate)var1;
      } else {
         var2 = new ThaiBuddhistDate(LocalDate.a(var1));
      }

      return var2;
   }
}
