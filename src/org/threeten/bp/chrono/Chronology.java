package org.threeten.bp.chrono;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.InvalidObjectException;
import java.io.ObjectStreamException;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.ServiceLoader;
import java.util.concurrent.ConcurrentHashMap;
import org.threeten.bp.DateTimeException;
import org.threeten.bp.Instant;
import org.threeten.bp.LocalTime;
import org.threeten.bp.ZoneId;
import org.threeten.bp.jdk8.Jdk8Methods;
import org.threeten.bp.temporal.ChronoField;
import org.threeten.bp.temporal.Temporal;
import org.threeten.bp.temporal.TemporalAccessor;
import org.threeten.bp.temporal.TemporalQueries;
import org.threeten.bp.temporal.TemporalQuery;

public abstract class Chronology implements Comparable {
   public static final TemporalQuery a = new TemporalQuery() {
      public Chronology a(TemporalAccessor var1) {
         return Chronology.a(var1);
      }

      // $FF: synthetic method
      public Object b(TemporalAccessor var1) {
         return this.a(var1);
      }
   };
   private static final ConcurrentHashMap b = new ConcurrentHashMap();
   private static final ConcurrentHashMap c = new ConcurrentHashMap();
   private static final Method d;

   static {
      Method var0 = null;

      label13: {
         Method var1;
         try {
            var1 = Locale.class.getMethod("getUnicodeLocaleType", new Class[]{String.class});
         } catch (Throwable var2) {
            break label13;
         }

         var0 = var1;
      }

      d = var0;
   }

   static Chronology a(DataInput var0) throws IOException {
      return a(var0.readUTF());
   }

   public static Chronology a(String var0) {
      c();
      Chronology var1 = (Chronology)b.get(var0);
      if(var1 == null) {
         Chronology var2 = (Chronology)c.get(var0);
         var1 = var2;
         if(var2 == null) {
            throw new DateTimeException("Unknown chronology: " + var0);
         }
      }

      return var1;
   }

   public static Chronology a(TemporalAccessor var0) {
      Jdk8Methods.a(var0, (String)"temporal");
      Object var1 = (Chronology)var0.a(TemporalQueries.b());
      if(var1 == null) {
         var1 = IsoChronology.b;
      }

      return (Chronology)var1;
   }

   private static void b(Chronology var0) {
      b.putIfAbsent(var0.a(), var0);
      String var1 = var0.b();
      if(var1 != null) {
         c.putIfAbsent(var1, var0);
      }

   }

   private static void c() {
      if(b.isEmpty()) {
         b((Chronology)IsoChronology.b);
         b((Chronology)ThaiBuddhistChronology.b);
         b((Chronology)MinguoChronology.b);
         b((Chronology)JapaneseChronology.c);
         b((Chronology)HijrahChronology.b);
         b.putIfAbsent("Hijrah", HijrahChronology.b);
         c.putIfAbsent("islamic", HijrahChronology.b);
         Iterator var0 = ServiceLoader.load(Chronology.class, Chronology.class.getClassLoader()).iterator();

         while(var0.hasNext()) {
            Chronology var1 = (Chronology)var0.next();
            b.putIfAbsent(var1.a(), var1);
            String var2 = var1.b();
            if(var2 != null) {
               c.putIfAbsent(var2, var1);
            }
         }
      }

   }

   private Object readResolve() throws ObjectStreamException {
      throw new InvalidObjectException("Deserialization via serialization delegate");
   }

   private Object writeReplace() {
      return new Ser(11, this);
   }

   public int a(Chronology var1) {
      return this.a().compareTo(var1.a());
   }

   public abstract String a();

   public abstract ChronoLocalDate a(int var1, int var2, int var3);

   ChronoLocalDate a(Temporal var1) {
      ChronoLocalDate var2 = (ChronoLocalDate)var1;
      if(!this.equals(var2.o())) {
         throw new ClassCastException("Chrono mismatch, expected: " + this.a() + ", actual: " + var2.o().a());
      } else {
         return var2;
      }
   }

   public ChronoZonedDateTime a(Instant var1, ZoneId var2) {
      return ChronoZonedDateTimeImpl.a(this, var1, var2);
   }

   public abstract Era a(int var1);

   void a(DataOutput var1) throws IOException {
      var1.writeUTF(this.a());
   }

   void a(Map var1, ChronoField var2, long var3) {
      Long var5 = (Long)var1.get(var2);
      if(var5 != null && var5.longValue() != var3) {
         throw new DateTimeException("Invalid state, field: " + var2 + " " + var5 + " conflicts with " + var2 + " " + var3);
      } else {
         var1.put(var2, Long.valueOf(var3));
      }
   }

   public abstract boolean a(long var1);

   public abstract String b();

   public abstract ChronoLocalDate b(TemporalAccessor var1);

   ChronoLocalDateTimeImpl b(Temporal var1) {
      ChronoLocalDateTimeImpl var2 = (ChronoLocalDateTimeImpl)var1;
      if(!this.equals(var2.l().o())) {
         throw new ClassCastException("Chrono mismatch, required: " + this.a() + ", supplied: " + var2.l().o().a());
      } else {
         return var2;
      }
   }

   public ChronoLocalDateTime c(TemporalAccessor var1) {
      try {
         ChronoLocalDateTime var2 = this.b(var1).b(LocalTime.a(var1));
         return var2;
      } catch (DateTimeException var3) {
         throw new DateTimeException("Unable to obtain ChronoLocalDateTime from TemporalAccessor: " + var1.getClass(), var3);
      }
   }

   ChronoZonedDateTimeImpl c(Temporal var1) {
      ChronoZonedDateTimeImpl var2 = (ChronoZonedDateTimeImpl)var1;
      if(!this.equals(var2.i().o())) {
         throw new ClassCastException("Chrono mismatch, required: " + this.a() + ", supplied: " + var2.i().o().a());
      } else {
         return var2;
      }
   }

   // $FF: synthetic method
   public int compareTo(Object var1) {
      return this.a((Chronology)var1);
   }

   public ChronoZonedDateTime d(TemporalAccessor param1) {
      // $FF: Couldn't be decompiled
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(this != var1) {
         if(var1 instanceof Chronology) {
            if(this.a((Chronology)var1) != 0) {
               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public int hashCode() {
      return this.getClass().hashCode() ^ this.a().hashCode();
   }

   public String toString() {
      return this.a();
   }
}
