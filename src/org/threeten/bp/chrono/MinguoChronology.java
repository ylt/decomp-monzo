package org.threeten.bp.chrono;

import java.io.Serializable;
import org.threeten.bp.Instant;
import org.threeten.bp.LocalDate;
import org.threeten.bp.ZoneId;
import org.threeten.bp.temporal.ChronoField;
import org.threeten.bp.temporal.TemporalAccessor;
import org.threeten.bp.temporal.ValueRange;

public final class MinguoChronology extends Chronology implements Serializable {
   public static final MinguoChronology b = new MinguoChronology();

   private Object readResolve() {
      return b;
   }

   public String a() {
      return "Minguo";
   }

   // $FF: synthetic method
   public ChronoLocalDate a(int var1, int var2, int var3) {
      return this.b(var1, var2, var3);
   }

   public ChronoZonedDateTime a(Instant var1, ZoneId var2) {
      return super.a(var1, var2);
   }

   // $FF: synthetic method
   public Era a(int var1) {
      return this.b(var1);
   }

   public ValueRange a(ChronoField var1) {
      ValueRange var2;
      switch(null.a[var1.ordinal()]) {
      case 1:
         var2 = ChronoField.y.a();
         var2 = ValueRange.a(var2.b() - 22932L, var2.c() - 22932L);
         break;
      case 2:
         var2 = ChronoField.A.a();
         var2 = ValueRange.a(1L, var2.c() - 1911L, -var2.b() + 1L + 1911L);
         break;
      case 3:
         var2 = ChronoField.A.a();
         var2 = ValueRange.a(var2.b() - 1911L, var2.c() - 1911L);
         break;
      default:
         var2 = var1.a();
      }

      return var2;
   }

   public boolean a(long var1) {
      return IsoChronology.b.a(1911L + var1);
   }

   public String b() {
      return "roc";
   }

   // $FF: synthetic method
   public ChronoLocalDate b(TemporalAccessor var1) {
      return this.e(var1);
   }

   public MinguoDate b(int var1, int var2, int var3) {
      return new MinguoDate(LocalDate.a(var1 + 1911, var2, var3));
   }

   public MinguoEra b(int var1) {
      return MinguoEra.a(var1);
   }

   public ChronoLocalDateTime c(TemporalAccessor var1) {
      return super.c(var1);
   }

   public ChronoZonedDateTime d(TemporalAccessor var1) {
      return super.d(var1);
   }

   public MinguoDate e(TemporalAccessor var1) {
      MinguoDate var2;
      if(var1 instanceof MinguoDate) {
         var2 = (MinguoDate)var1;
      } else {
         var2 = new MinguoDate(LocalDate.a(var1));
      }

      return var2;
   }
}
