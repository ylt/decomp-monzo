package org.threeten.bp.chrono;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.threeten.bp.DateTimeException;
import org.threeten.bp.temporal.ChronoField;
import org.threeten.bp.temporal.ChronoUnit;
import org.threeten.bp.temporal.Temporal;
import org.threeten.bp.temporal.TemporalField;
import org.threeten.bp.temporal.TemporalQueries;
import org.threeten.bp.temporal.TemporalQuery;
import org.threeten.bp.temporal.UnsupportedTemporalTypeException;
import org.threeten.bp.temporal.ValueRange;

public enum HijrahEra implements Era {
   a,
   b;

   public static HijrahEra a(int var0) {
      HijrahEra var1;
      switch(var0) {
      case 0:
         var1 = a;
         break;
      case 1:
         var1 = b;
         break;
      default:
         throw new DateTimeException("HijrahEra not valid");
      }

      return var1;
   }

   static HijrahEra a(DataInput var0) throws IOException {
      return a(var0.readByte());
   }

   private Object writeReplace() {
      return new Ser(4, this);
   }

   public int a() {
      return this.ordinal();
   }

   public Object a(TemporalQuery var1) {
      Object var2;
      if(var1 == TemporalQueries.c()) {
         var2 = ChronoUnit.o;
      } else if(var1 != TemporalQueries.b() && var1 != TemporalQueries.d() && var1 != TemporalQueries.a() && var1 != TemporalQueries.e() && var1 != TemporalQueries.f() && var1 != TemporalQueries.g()) {
         var2 = var1.b(this);
      } else {
         var2 = null;
      }

      return var2;
   }

   public Temporal a(Temporal var1) {
      return var1.b(ChronoField.B, (long)this.a());
   }

   void a(DataOutput var1) throws IOException {
      var1.writeByte(this.a());
   }

   public boolean a(TemporalField var1) {
      boolean var2 = true;
      if(var1 instanceof ChronoField) {
         if(var1 != ChronoField.B) {
            var2 = false;
         }
      } else if(var1 == null || !var1.a(this)) {
         var2 = false;
      }

      return var2;
   }

   int b(int var1) {
      if(this != b) {
         var1 = 1 - var1;
      }

      return var1;
   }

   public ValueRange b(TemporalField var1) {
      ValueRange var2;
      if(var1 == ChronoField.B) {
         var2 = ValueRange.a(1L, 1L);
      } else {
         if(var1 instanceof ChronoField) {
            throw new UnsupportedTemporalTypeException("Unsupported field: " + var1);
         }

         var2 = var1.b(this);
      }

      return var2;
   }

   public int c(TemporalField var1) {
      int var2;
      if(var1 == ChronoField.B) {
         var2 = this.a();
      } else {
         var2 = this.b(var1).b(this.d(var1), var1);
      }

      return var2;
   }

   public long d(TemporalField var1) {
      long var2;
      if(var1 == ChronoField.B) {
         var2 = (long)this.a();
      } else {
         if(var1 instanceof ChronoField) {
            throw new UnsupportedTemporalTypeException("Unsupported field: " + var1);
         }

         var2 = var1.c(this);
      }

      return var2;
   }
}
