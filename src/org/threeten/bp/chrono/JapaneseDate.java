package org.threeten.bp.chrono;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.Calendar;
import org.threeten.bp.DateTimeException;
import org.threeten.bp.LocalDate;
import org.threeten.bp.LocalTime;
import org.threeten.bp.temporal.ChronoField;
import org.threeten.bp.temporal.Temporal;
import org.threeten.bp.temporal.TemporalAdjuster;
import org.threeten.bp.temporal.TemporalAmount;
import org.threeten.bp.temporal.TemporalField;
import org.threeten.bp.temporal.TemporalUnit;
import org.threeten.bp.temporal.UnsupportedTemporalTypeException;
import org.threeten.bp.temporal.ValueRange;

public final class JapaneseDate extends ChronoDateImpl implements Serializable {
   static final LocalDate a = LocalDate.a(1873, 1, 1);
   private final LocalDate b;
   private transient JapaneseEra c;
   private transient int d;

   JapaneseDate(LocalDate var1) {
      if(var1.c((ChronoLocalDate)a)) {
         throw new DateTimeException("Minimum supported date is January 1st Meiji 6");
      } else {
         this.c = JapaneseEra.a(var1);
         int var2 = this.c.c().d();
         this.d = var1.d() - (var2 - 1);
         this.b = var1;
      }
   }

   static ChronoLocalDate a(DataInput var0) throws IOException {
      int var3 = var0.readInt();
      byte var2 = var0.readByte();
      byte var1 = var0.readByte();
      return JapaneseChronology.c.b(var3, var2, var1);
   }

   private JapaneseDate a(LocalDate var1) {
      JapaneseDate var2;
      if(var1.equals(this.b)) {
         var2 = this;
      } else {
         var2 = new JapaneseDate(var1);
      }

      return var2;
   }

   private JapaneseDate a(JapaneseEra var1, int var2) {
      var2 = JapaneseChronology.c.a(var1, var2);
      return this.a(this.b.a(var2));
   }

   private ValueRange a(int var1) {
      Calendar var2 = Calendar.getInstance(JapaneseChronology.b);
      var2.set(0, this.c.a() + 2);
      var2.set(this.d, this.b.e() - 1, this.b.g());
      return ValueRange.a((long)var2.getActualMinimum(var1), (long)var2.getActualMaximum(var1));
   }

   private JapaneseDate b(int var1) {
      return this.a(this.b(), var1);
   }

   private long d() {
      long var1;
      if(this.d == 1) {
         var1 = (long)(this.b.h() - this.c.c().h() + 1);
      } else {
         var1 = (long)this.b.h();
      }

      return var1;
   }

   private void readObject(ObjectInputStream var1) throws IOException, ClassNotFoundException {
      var1.defaultReadObject();
      this.c = JapaneseEra.a(this.b);
      int var2 = this.c.c().d();
      this.d = this.b.d() - (var2 - 1);
   }

   private Object writeReplace() {
      return new Ser(1, this);
   }

   // $FF: synthetic method
   ChronoDateImpl a(long var1) {
      return this.d(var1);
   }

   // $FF: synthetic method
   public ChronoDateImpl a(long var1, TemporalUnit var3) {
      return this.b(var1, var3);
   }

   public JapaneseChronology a() {
      return JapaneseChronology.c;
   }

   public JapaneseDate a(TemporalAdjuster var1) {
      return (JapaneseDate)super.c(var1);
   }

   public JapaneseDate a(TemporalAmount var1) {
      return (JapaneseDate)super.b(var1);
   }

   public JapaneseDate a(TemporalField var1, long var2) {
      JapaneseDate var6;
      if(var1 instanceof ChronoField) {
         ChronoField var5 = (ChronoField)var1;
         if(this.d(var5) == var2) {
            var6 = this;
         } else {
            switch(null.a[var5.ordinal()]) {
            case 1:
            case 2:
            case 7:
               int var4 = this.a().a(var5).b(var2, var5);
               switch(null.a[var5.ordinal()]) {
               case 1:
                  var6 = this.a(this.b.e((long)var4 - this.d()));
                  return var6;
               case 2:
                  var6 = this.b(var4);
                  return var6;
               case 7:
                  var6 = this.a(JapaneseEra.a(var4), this.d);
                  return var6;
               }
            default:
               var6 = this.a(this.b.a(var1, var2));
            }
         }
      } else {
         var6 = (JapaneseDate)var1.a(this, var2);
      }

      return var6;
   }

   void a(DataOutput var1) throws IOException {
      var1.writeInt(this.c((TemporalField)ChronoField.A));
      var1.writeByte(this.c((TemporalField)ChronoField.x));
      var1.writeByte(this.c((TemporalField)ChronoField.s));
   }

   public boolean a(TemporalField var1) {
      boolean var2;
      if(var1 != ChronoField.q && var1 != ChronoField.r && var1 != ChronoField.v && var1 != ChronoField.w) {
         var2 = super.a(var1);
      } else {
         var2 = false;
      }

      return var2;
   }

   // $FF: synthetic method
   ChronoDateImpl b(long var1) {
      return this.e(var1);
   }

   // $FF: synthetic method
   public ChronoLocalDate b(TemporalAmount var1) {
      return this.a(var1);
   }

   public final ChronoLocalDateTime b(LocalTime var1) {
      return super.b(var1);
   }

   public JapaneseDate b(long var1, TemporalUnit var3) {
      return (JapaneseDate)super.a(var1, var3);
   }

   public JapaneseEra b() {
      return this.c;
   }

   // $FF: synthetic method
   public Temporal b(TemporalAdjuster var1) {
      return this.a(var1);
   }

   // $FF: synthetic method
   public Temporal b(TemporalField var1, long var2) {
      return this.a(var1, var2);
   }

   public ValueRange b(TemporalField var1) {
      ValueRange var3;
      if(var1 instanceof ChronoField) {
         if(!this.a(var1)) {
            throw new UnsupportedTemporalTypeException("Unsupported field: " + var1);
         }

         ChronoField var2 = (ChronoField)var1;
         switch(null.a[var2.ordinal()]) {
         case 1:
            var3 = this.a(6);
            break;
         case 2:
            var3 = this.a(1);
            break;
         default:
            var3 = this.a().a(var2);
         }
      } else {
         var3 = var1.b(this);
      }

      return var3;
   }

   // $FF: synthetic method
   ChronoDateImpl c(long var1) {
      return this.f(var1);
   }

   // $FF: synthetic method
   public ChronoLocalDate c(TemporalAdjuster var1) {
      return this.a(var1);
   }

   // $FF: synthetic method
   public ChronoLocalDate c(TemporalField var1, long var2) {
      return this.a(var1, var2);
   }

   // $FF: synthetic method
   public Era c() {
      return this.b();
   }

   // $FF: synthetic method
   public Temporal c(long var1, TemporalUnit var3) {
      return this.g(var1, var3);
   }

   // $FF: synthetic method
   public Temporal c(TemporalAmount var1) {
      return this.a(var1);
   }

   public long d(TemporalField var1) {
      long var2;
      if(var1 instanceof ChronoField) {
         switch(null.a[((ChronoField)var1).ordinal()]) {
         case 1:
            var2 = this.d();
            break;
         case 2:
            var2 = (long)this.d;
            break;
         case 3:
         case 4:
         case 5:
         case 6:
            throw new UnsupportedTemporalTypeException("Unsupported field: " + var1);
         case 7:
            var2 = (long)this.c.a();
            break;
         default:
            var2 = this.b.d(var1);
         }
      } else {
         var2 = var1.c(this);
      }

      return var2;
   }

   JapaneseDate d(long var1) {
      return this.a(this.b.b(var1));
   }

   // $FF: synthetic method
   public Temporal d(long var1, TemporalUnit var3) {
      return this.b(var1, var3);
   }

   // $FF: synthetic method
   public ChronoLocalDate e(long var1, TemporalUnit var3) {
      return this.g(var1, var3);
   }

   JapaneseDate e(long var1) {
      return this.a(this.b.c(var1));
   }

   public boolean equals(Object var1) {
      boolean var2;
      if(this == var1) {
         var2 = true;
      } else if(var1 instanceof JapaneseDate) {
         JapaneseDate var3 = (JapaneseDate)var1;
         var2 = this.b.equals(var3.b);
      } else {
         var2 = false;
      }

      return var2;
   }

   // $FF: synthetic method
   public ChronoLocalDate f(long var1, TemporalUnit var3) {
      return this.b(var1, var3);
   }

   JapaneseDate f(long var1) {
      return this.a(this.b.e(var1));
   }

   public JapaneseDate g(long var1, TemporalUnit var3) {
      return (JapaneseDate)super.e(var1, var3);
   }

   public int hashCode() {
      return this.a().a().hashCode() ^ this.b.hashCode();
   }

   public int l() {
      Calendar var1 = Calendar.getInstance(JapaneseChronology.b);
      var1.set(0, this.c.a() + 2);
      var1.set(this.d, this.b.e() - 1, this.b.g());
      return var1.getActualMaximum(6);
   }

   public long n() {
      return this.b.n();
   }

   // $FF: synthetic method
   public Chronology o() {
      return this.a();
   }
}
