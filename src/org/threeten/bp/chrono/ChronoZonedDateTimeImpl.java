package org.threeten.bp.chrono;

import java.io.IOException;
import java.io.InvalidObjectException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.io.ObjectStreamException;
import java.io.Serializable;
import java.util.List;
import org.threeten.bp.Instant;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.ZoneId;
import org.threeten.bp.ZoneOffset;
import org.threeten.bp.jdk8.Jdk8Methods;
import org.threeten.bp.temporal.ChronoField;
import org.threeten.bp.temporal.ChronoUnit;
import org.threeten.bp.temporal.Temporal;
import org.threeten.bp.temporal.TemporalAccessor;
import org.threeten.bp.temporal.TemporalField;
import org.threeten.bp.temporal.TemporalUnit;
import org.threeten.bp.zone.ZoneOffsetTransition;
import org.threeten.bp.zone.ZoneRules;

final class ChronoZonedDateTimeImpl extends ChronoZonedDateTime implements Serializable {
   private final ChronoLocalDateTimeImpl a;
   private final ZoneOffset b;
   private final ZoneId c;

   private ChronoZonedDateTimeImpl(ChronoLocalDateTimeImpl var1, ZoneOffset var2, ZoneId var3) {
      this.a = (ChronoLocalDateTimeImpl)Jdk8Methods.a(var1, (String)"dateTime");
      this.b = (ZoneOffset)Jdk8Methods.a(var2, (String)"offset");
      this.c = (ZoneId)Jdk8Methods.a(var3, (String)"zone");
   }

   static ChronoZonedDateTime a(ObjectInput var0) throws IOException, ClassNotFoundException {
      ChronoLocalDateTime var1 = (ChronoLocalDateTime)var0.readObject();
      ZoneOffset var2 = (ZoneOffset)var0.readObject();
      ZoneId var3 = (ZoneId)var0.readObject();
      return var1.b((ZoneId)var2).d(var3);
   }

   static ChronoZonedDateTime a(ChronoLocalDateTimeImpl var0, ZoneId var1, ZoneOffset var2) {
      Jdk8Methods.a(var0, (String)"localDateTime");
      Jdk8Methods.a(var1, (String)"zone");
      ChronoZonedDateTimeImpl var6;
      if(var1 instanceof ZoneOffset) {
         var6 = new ChronoZonedDateTimeImpl(var0, (ZoneOffset)var1, var1);
      } else {
         ZoneRules var5 = var1.d();
         LocalDateTime var4 = LocalDateTime.a((TemporalAccessor)var0);
         List var3 = var5.a(var4);
         if(var3.size() == 1) {
            var2 = (ZoneOffset)var3.get(0);
         } else if(var3.size() == 0) {
            ZoneOffsetTransition var7 = var5.b(var4);
            var0 = var0.a(var7.g().a());
            var2 = var7.f();
         } else if(var2 == null || !var3.contains(var2)) {
            var2 = (ZoneOffset)var3.get(0);
         }

         Jdk8Methods.a(var2, (String)"offset");
         var6 = new ChronoZonedDateTimeImpl(var0, var2, var1);
      }

      return var6;
   }

   private ChronoZonedDateTimeImpl a(Instant var1, ZoneId var2) {
      return a(this.i().o(), var1, var2);
   }

   static ChronoZonedDateTimeImpl a(Chronology var0, Instant var1, ZoneId var2) {
      ZoneOffset var3 = var2.d().a(var1);
      Jdk8Methods.a(var3, (String)"offset");
      return new ChronoZonedDateTimeImpl((ChronoLocalDateTimeImpl)var0.c((TemporalAccessor)LocalDateTime.a(var1.a(), var1.b(), var3)), var3, var2);
   }

   private Object readResolve() throws ObjectStreamException {
      throw new InvalidObjectException("Deserialization via serialization delegate");
   }

   private Object writeReplace() {
      return new Ser(13, this);
   }

   public long a(Temporal var1, TemporalUnit var2) {
      ChronoZonedDateTime var5 = this.i().o().d(var1);
      long var3;
      if(var2 instanceof ChronoUnit) {
         var5 = var5.c((ZoneId)this.b);
         var3 = this.a.a((Temporal)var5.h(), (TemporalUnit)var2);
      } else {
         var3 = var2.a(this, var5);
      }

      return var3;
   }

   public ZoneOffset a() {
      return this.b;
   }

   void a(ObjectOutput var1) throws IOException {
      var1.writeObject(this.a);
      var1.writeObject(this.b);
      var1.writeObject(this.c);
   }

   public boolean a(TemporalField var1) {
      boolean var2;
      if(!(var1 instanceof ChronoField) && (var1 == null || !var1.a(this))) {
         var2 = false;
      } else {
         var2 = true;
      }

      return var2;
   }

   public ZoneId b() {
      return this.c;
   }

   // $FF: synthetic method
   public Temporal b(TemporalField var1, long var2) {
      return this.c(var1, var2);
   }

   public ChronoZonedDateTime c(ZoneId var1) {
      Jdk8Methods.a(var1, (String)"zone");
      ChronoZonedDateTimeImpl var2;
      if(this.c.equals(var1)) {
         var2 = this;
      } else {
         var2 = this.a(this.a.b((ZoneOffset)this.b), var1);
      }

      return var2;
   }

   public ChronoZonedDateTime c(TemporalField var1, long var2) {
      Object var6;
      if(var1 instanceof ChronoField) {
         ChronoField var4 = (ChronoField)var1;
         switch(null.a[var4.ordinal()]) {
         case 1:
            var6 = this.f(var2 - this.k(), ChronoUnit.d);
            break;
         case 2:
            ZoneOffset var5 = ZoneOffset.a(var4.b(var2));
            var6 = this.a(this.a.b((ZoneOffset)var5), this.c);
            break;
         default:
            var6 = a(this.a.a(var1, var2), this.c, this.b);
         }
      } else {
         var6 = this.i().o().c(var1.a(this, var2));
      }

      return (ChronoZonedDateTime)var6;
   }

   public ChronoZonedDateTime d(ZoneId var1) {
      return a(this.a, var1, this.b);
   }

   // $FF: synthetic method
   public Temporal d(long var1, TemporalUnit var3) {
      return this.f(var1, var3);
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(this != var1) {
         if(var1 instanceof ChronoZonedDateTime) {
            if(this.a((ChronoZonedDateTime)((ChronoZonedDateTime)var1)) != 0) {
               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public ChronoZonedDateTime f(long var1, TemporalUnit var3) {
      Object var4;
      if(var3 instanceof ChronoUnit) {
         var4 = this.c(this.a.a(var1, var3));
      } else {
         var4 = this.i().o().c(var3.a(this, var1));
      }

      return (ChronoZonedDateTime)var4;
   }

   public ChronoLocalDateTime h() {
      return this.a;
   }

   public int hashCode() {
      return this.h().hashCode() ^ this.a().hashCode() ^ Integer.rotateLeft(this.b().hashCode(), 3);
   }

   public String toString() {
      String var2 = this.h().toString() + this.a().toString();
      String var1 = var2;
      if(this.a() != this.b()) {
         var1 = var2 + '[' + this.b().toString() + ']';
      }

      return var1;
   }
}
