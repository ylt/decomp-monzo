package org.threeten.bp.chrono;

import java.io.Serializable;
import java.util.HashMap;
import org.threeten.bp.DateTimeException;
import org.threeten.bp.Instant;
import org.threeten.bp.ZoneId;
import org.threeten.bp.temporal.ChronoField;
import org.threeten.bp.temporal.TemporalAccessor;
import org.threeten.bp.temporal.ValueRange;

public final class HijrahChronology extends Chronology implements Serializable {
   public static final HijrahChronology b = new HijrahChronology();
   private static final HashMap c = new HashMap();
   private static final HashMap d = new HashMap();
   private static final HashMap e = new HashMap();

   static {
      c.put("en", new String[]{"BH", "HE"});
      d.put("en", new String[]{"B.H.", "H.E."});
      e.put("en", new String[]{"Before Hijrah", "Hijrah Era"});
   }

   private Object readResolve() {
      return b;
   }

   public String a() {
      return "Hijrah-umalqura";
   }

   // $FF: synthetic method
   public ChronoLocalDate a(int var1, int var2, int var3) {
      return this.b(var1, var2, var3);
   }

   public ChronoZonedDateTime a(Instant var1, ZoneId var2) {
      return super.a(var1, var2);
   }

   // $FF: synthetic method
   public Era a(int var1) {
      return this.b(var1);
   }

   public ValueRange a(ChronoField var1) {
      return var1.a();
   }

   public boolean a(long var1) {
      return HijrahDate.h(var1);
   }

   public String b() {
      return "islamic-umalqura";
   }

   // $FF: synthetic method
   public ChronoLocalDate b(TemporalAccessor var1) {
      return this.e(var1);
   }

   public HijrahDate b(int var1, int var2, int var3) {
      return HijrahDate.a(var1, var2, var3);
   }

   public HijrahEra b(int var1) {
      HijrahEra var2;
      switch(var1) {
      case 0:
         var2 = HijrahEra.a;
         break;
      case 1:
         var2 = HijrahEra.b;
         break;
      default:
         throw new DateTimeException("invalid Hijrah era");
      }

      return var2;
   }

   public ChronoLocalDateTime c(TemporalAccessor var1) {
      return super.c(var1);
   }

   public ChronoZonedDateTime d(TemporalAccessor var1) {
      return super.d(var1);
   }

   public HijrahDate e(TemporalAccessor var1) {
      HijrahDate var2;
      if(var1 instanceof HijrahDate) {
         var2 = (HijrahDate)var1;
      } else {
         var2 = HijrahDate.d(var1.d(ChronoField.u));
      }

      return var2;
   }
}
