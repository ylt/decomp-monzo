package org.threeten.bp.chrono;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.Externalizable;
import java.io.IOException;
import java.io.InvalidClassException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.io.StreamCorruptedException;

final class Ser implements Externalizable {
   private byte a;
   private Object b;

   public Ser() {
   }

   Ser(byte var1, Object var2) {
      this.a = var1;
      this.b = var2;
   }

   private static Object a(byte var0, ObjectInput var1) throws IOException, ClassNotFoundException {
      Object var2;
      switch(var0) {
      case 1:
         var2 = JapaneseDate.a((DataInput)var1);
         break;
      case 2:
         var2 = JapaneseEra.a((DataInput)var1);
         break;
      case 3:
         var2 = HijrahDate.a((DataInput)var1);
         break;
      case 4:
         var2 = HijrahEra.a((DataInput)var1);
         break;
      case 5:
         var2 = MinguoDate.a((DataInput)var1);
         break;
      case 6:
         var2 = MinguoEra.a((DataInput)var1);
         break;
      case 7:
         var2 = ThaiBuddhistDate.a((DataInput)var1);
         break;
      case 8:
         var2 = ThaiBuddhistEra.a((DataInput)var1);
         break;
      case 9:
      case 10:
      default:
         throw new StreamCorruptedException("Unknown serialized type");
      case 11:
         var2 = Chronology.a((DataInput)var1);
         break;
      case 12:
         var2 = ChronoLocalDateTimeImpl.a(var1);
         break;
      case 13:
         var2 = ChronoZonedDateTimeImpl.a(var1);
      }

      return var2;
   }

   private static void a(byte var0, Object var1, ObjectOutput var2) throws IOException {
      var2.writeByte(var0);
      switch(var0) {
      case 1:
         ((JapaneseDate)var1).a((DataOutput)var2);
         break;
      case 2:
         ((JapaneseEra)var1).a((DataOutput)var2);
         break;
      case 3:
         ((HijrahDate)var1).a((DataOutput)var2);
         break;
      case 4:
         ((HijrahEra)var1).a((DataOutput)var2);
         break;
      case 5:
         ((MinguoDate)var1).a((DataOutput)var2);
         break;
      case 6:
         ((MinguoEra)var1).a((DataOutput)var2);
         break;
      case 7:
         ((ThaiBuddhistDate)var1).a((DataOutput)var2);
         break;
      case 8:
         ((ThaiBuddhistEra)var1).a((DataOutput)var2);
         break;
      case 9:
      case 10:
      default:
         throw new InvalidClassException("Unknown serialized type");
      case 11:
         ((Chronology)var1).a((DataOutput)var2);
         break;
      case 12:
         ((ChronoLocalDateTimeImpl)var1).a(var2);
         break;
      case 13:
         ((ChronoZonedDateTimeImpl)var1).a(var2);
      }

   }

   private Object readResolve() {
      return this.b;
   }

   public void readExternal(ObjectInput var1) throws IOException, ClassNotFoundException {
      this.a = var1.readByte();
      this.b = a(this.a, var1);
   }

   public void writeExternal(ObjectOutput var1) throws IOException {
      a(this.a, this.b, var1);
   }
}
