package org.threeten.bp;

public class DateTimeException extends RuntimeException {
   public DateTimeException(String var1) {
      super(var1);
   }

   public DateTimeException(String var1, Throwable var2) {
      super(var1, var2);
   }
}
