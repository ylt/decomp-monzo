package org.threeten.bp.zone;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.Externalizable;
import java.io.IOException;
import java.io.InvalidClassException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.io.StreamCorruptedException;
import org.threeten.bp.ZoneOffset;

final class Ser implements Externalizable {
   private byte a;
   private Object b;

   public Ser() {
   }

   Ser(byte var1, Object var2) {
      this.a = var1;
      this.b = var2;
   }

   private static Object a(byte var0, DataInput var1) throws IOException, ClassNotFoundException {
      Object var2;
      switch(var0) {
      case 1:
         var2 = StandardZoneRules.a(var1);
         break;
      case 2:
         var2 = ZoneOffsetTransition.a(var1);
         break;
      case 3:
         var2 = ZoneOffsetTransitionRule.a(var1);
         break;
      default:
         throw new StreamCorruptedException("Unknown serialized type");
      }

      return var2;
   }

   static Object a(DataInput var0) throws IOException, ClassNotFoundException {
      return a(var0.readByte(), var0);
   }

   private static void a(byte var0, Object var1, DataOutput var2) throws IOException {
      var2.writeByte(var0);
      switch(var0) {
      case 1:
         ((StandardZoneRules)var1).a(var2);
         break;
      case 2:
         ((ZoneOffsetTransition)var1).a(var2);
         break;
      case 3:
         ((ZoneOffsetTransitionRule)var1).a(var2);
         break;
      default:
         throw new InvalidClassException("Unknown serialized type");
      }

   }

   static void a(long var0, DataOutput var2) throws IOException {
      if(var0 >= -4575744000L && var0 < 10413792000L && var0 % 900L == 0L) {
         int var3 = (int)((4575744000L + var0) / 900L);
         var2.writeByte(var3 >>> 16 & 255);
         var2.writeByte(var3 >>> 8 & 255);
         var2.writeByte(var3 & 255);
      } else {
         var2.writeByte(255);
         var2.writeLong(var0);
      }

   }

   static void a(ZoneOffset var0, DataOutput var1) throws IOException {
      int var3 = var0.f();
      int var2;
      if(var3 % 900 == 0) {
         var2 = var3 / 900;
      } else {
         var2 = 127;
      }

      var1.writeByte(var2);
      if(var2 == 127) {
         var1.writeInt(var3);
      }

   }

   static ZoneOffset b(DataInput var0) throws IOException {
      byte var1 = var0.readByte();
      ZoneOffset var2;
      if(var1 == 127) {
         var2 = ZoneOffset.a(var0.readInt());
      } else {
         var2 = ZoneOffset.a(var1 * 900);
      }

      return var2;
   }

   static long c(DataInput var0) throws IOException {
      int var1 = var0.readByte() & 255;
      long var2;
      if(var1 == 255) {
         var2 = var0.readLong();
      } else {
         var2 = (long)((var1 << 16) + ((var0.readByte() & 255) << 8) + (var0.readByte() & 255)) * 900L - 4575744000L;
      }

      return var2;
   }

   private Object readResolve() {
      return this.b;
   }

   public void readExternal(ObjectInput var1) throws IOException, ClassNotFoundException {
      this.a = var1.readByte();
      this.b = a(this.a, var1);
   }

   public void writeExternal(ObjectOutput var1) throws IOException {
      a(this.a, this.b, var1);
   }
}
