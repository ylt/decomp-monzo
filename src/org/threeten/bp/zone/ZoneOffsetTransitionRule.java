package org.threeten.bp.zone;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.Serializable;
import org.threeten.bp.DayOfWeek;
import org.threeten.bp.LocalDate;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.LocalTime;
import org.threeten.bp.Month;
import org.threeten.bp.ZoneOffset;
import org.threeten.bp.chrono.IsoChronology;
import org.threeten.bp.jdk8.Jdk8Methods;
import org.threeten.bp.temporal.TemporalAdjusters;

public final class ZoneOffsetTransitionRule implements Serializable {
   private final Month a;
   private final byte b;
   private final DayOfWeek c;
   private final LocalTime d;
   private final boolean e;
   private final ZoneOffsetTransitionRule.TimeDefinition f;
   private final ZoneOffset g;
   private final ZoneOffset h;
   private final ZoneOffset i;

   ZoneOffsetTransitionRule(Month var1, int var2, DayOfWeek var3, LocalTime var4, boolean var5, ZoneOffsetTransitionRule.TimeDefinition var6, ZoneOffset var7, ZoneOffset var8, ZoneOffset var9) {
      this.a = var1;
      this.b = (byte)var2;
      this.c = var3;
      this.d = var4;
      this.e = var5;
      this.f = var6;
      this.g = var7;
      this.h = var8;
      this.i = var9;
   }

   static ZoneOffsetTransitionRule a(DataInput var0) throws IOException {
      boolean var6 = false;
      int var1 = var0.readInt();
      Month var11 = Month.a(var1 >>> 28);
      int var2 = (3670016 & var1) >>> 19;
      DayOfWeek var7;
      if(var2 == 0) {
         var7 = null;
      } else {
         var7 = DayOfWeek.a(var2);
      }

      int var3 = (507904 & var1) >>> 14;
      ZoneOffsetTransitionRule.TimeDefinition var12 = ZoneOffsetTransitionRule.TimeDefinition.values()[(var1 & 12288) >>> 12];
      var2 = (var1 & 4080) >>> 4;
      int var4 = (var1 & 12) >>> 2;
      int var5 = var1 & 3;
      LocalTime var8;
      if(var3 == 31) {
         var8 = LocalTime.a((long)var0.readInt());
      } else {
         var8 = LocalTime.a(var3 % 24, 0);
      }

      ZoneOffset var9;
      if(var2 == 255) {
         var9 = ZoneOffset.a(var0.readInt());
      } else {
         var9 = ZoneOffset.a((var2 - 128) * 900);
      }

      ZoneOffset var10;
      if(var4 == 3) {
         var10 = ZoneOffset.a(var0.readInt());
      } else {
         var10 = ZoneOffset.a(var4 * 1800 + var9.f());
      }

      ZoneOffset var13;
      if(var5 == 3) {
         var13 = ZoneOffset.a(var0.readInt());
      } else {
         var13 = ZoneOffset.a(var5 * 1800 + var9.f());
      }

      if(var3 == 24) {
         var6 = true;
      }

      return a(var11, ((264241152 & var1) >>> 22) - 32, var7, var8, var6, var12, var9, var10, var13);
   }

   public static ZoneOffsetTransitionRule a(Month var0, int var1, DayOfWeek var2, LocalTime var3, boolean var4, ZoneOffsetTransitionRule.TimeDefinition var5, ZoneOffset var6, ZoneOffset var7, ZoneOffset var8) {
      Jdk8Methods.a(var0, (String)"month");
      Jdk8Methods.a(var3, (String)"time");
      Jdk8Methods.a(var5, (String)"timeDefnition");
      Jdk8Methods.a(var6, (String)"standardOffset");
      Jdk8Methods.a(var7, (String)"offsetBefore");
      Jdk8Methods.a(var8, (String)"offsetAfter");
      if(var1 >= -28 && var1 <= 31 && var1 != 0) {
         if(var4 && !var3.equals(LocalTime.c)) {
            throw new IllegalArgumentException("Time must be midnight when end of day flag is true");
         } else {
            return new ZoneOffsetTransitionRule(var0, var1, var2, var3, var4, var5, var6, var7, var8);
         }
      } else {
         throw new IllegalArgumentException("Day of month indicator must be between -28 and 31 inclusive excluding zero");
      }
   }

   private Object writeReplace() {
      return new Ser(3, this);
   }

   public ZoneOffsetTransition a(int var1) {
      LocalDate var2;
      LocalDate var3;
      if(this.b < 0) {
         var3 = LocalDate.a(var1, this.a, this.a.a(IsoChronology.b.a((long)var1)) + 1 + this.b);
         var2 = var3;
         if(this.c != null) {
            var2 = var3.a(TemporalAdjusters.b(this.c));
         }
      } else {
         var3 = LocalDate.a(var1, this.a, this.b);
         var2 = var3;
         if(this.c != null) {
            var2 = var3.a(TemporalAdjusters.a(this.c));
         }
      }

      var3 = var2;
      if(this.e) {
         var3 = var2.e(1L);
      }

      LocalDateTime var4 = LocalDateTime.a(var3, this.d);
      return new ZoneOffsetTransition(this.f.a(var4, this.g, this.h), this.h, this.i);
   }

   void a(DataOutput var1) throws IOException {
      int var3;
      if(this.e) {
         var3 = 86400;
      } else {
         var3 = this.d.e();
      }

      int var8 = this.g.f();
      int var5 = this.h.f() - var8;
      int var6 = this.i.f() - var8;
      int var2;
      if(var3 % 3600 == 0) {
         if(this.e) {
            var2 = 24;
         } else {
            var2 = this.d.a();
         }
      } else {
         var2 = 31;
      }

      int var4;
      if(var8 % 900 == 0) {
         var4 = var8 / 900 + 128;
      } else {
         var4 = 255;
      }

      if(var5 != 0 && var5 != 1800 && var5 != 3600) {
         var5 = 3;
      } else {
         var5 /= 1800;
      }

      if(var6 != 0 && var6 != 1800 && var6 != 3600) {
         var6 = 3;
      } else {
         var6 /= 1800;
      }

      int var7;
      if(this.c == null) {
         var7 = 0;
      } else {
         var7 = this.c.a();
      }

      var1.writeInt((var7 << 19) + (this.a.a() << 28) + (this.b + 32 << 22) + (var2 << 14) + (this.f.ordinal() << 12) + (var4 << 4) + (var5 << 2) + var6);
      if(var2 == 31) {
         var1.writeInt(var3);
      }

      if(var4 == 255) {
         var1.writeInt(var8);
      }

      if(var5 == 3) {
         var1.writeInt(this.h.f());
      }

      if(var6 == 3) {
         var1.writeInt(this.i.f());
      }

   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(var1 != this) {
         if(var1 instanceof ZoneOffsetTransitionRule) {
            ZoneOffsetTransitionRule var3 = (ZoneOffsetTransitionRule)var1;
            if(this.a != var3.a || this.b != var3.b || this.c != var3.c || this.f != var3.f || !this.d.equals(var3.d) || this.e != var3.e || !this.g.equals(var3.g) || !this.h.equals(var3.h) || !this.i.equals(var3.i)) {
               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public int hashCode() {
      int var3 = this.d.e();
      byte var1;
      if(this.e) {
         var1 = 1;
      } else {
         var1 = 0;
      }

      int var4 = this.a.ordinal();
      byte var5 = this.b;
      int var2;
      if(this.c == null) {
         var2 = 7;
      } else {
         var2 = this.c.ordinal();
      }

      return (var2 << 2) + (var5 + 32 << 5) + (var1 + var3 << 15) + (var4 << 11) + this.f.ordinal() ^ this.g.hashCode() ^ this.h.hashCode() ^ this.i.hashCode();
   }

   public String toString() {
      StringBuilder var2 = new StringBuilder();
      StringBuilder var3 = var2.append("TransitionRule[");
      String var1;
      if(this.h.a(this.i) > 0) {
         var1 = "Gap ";
      } else {
         var1 = "Overlap ";
      }

      var3.append(var1).append(this.h).append(" to ").append(this.i).append(", ");
      if(this.c != null) {
         if(this.b == -1) {
            var2.append(this.c.name()).append(" on or before last day of ").append(this.a.name());
         } else if(this.b < 0) {
            var2.append(this.c.name()).append(" on or before last day minus ").append(-this.b - 1).append(" of ").append(this.a.name());
         } else {
            var2.append(this.c.name()).append(" on or after ").append(this.a.name()).append(' ').append(this.b);
         }
      } else {
         var2.append(this.a.name()).append(' ').append(this.b);
      }

      var3 = var2.append(" at ");
      if(this.e) {
         var1 = "24:00";
      } else {
         var1 = this.d.toString();
      }

      var3.append(var1).append(" ").append(this.f).append(", standard offset ").append(this.g).append(']');
      return var2.toString();
   }

   public static enum TimeDefinition {
      a,
      b,
      c;

      public LocalDateTime a(LocalDateTime var1, ZoneOffset var2, ZoneOffset var3) {
         switch(null.a[this.ordinal()]) {
         case 1:
            var1 = var1.d((long)(var3.f() - ZoneOffset.d.f()));
            break;
         case 2:
            var1 = var1.d((long)(var3.f() - var2.f()));
         }

         return var1;
      }
   }
}
