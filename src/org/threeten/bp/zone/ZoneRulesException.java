package org.threeten.bp.zone;

import org.threeten.bp.DateTimeException;

public class ZoneRulesException extends DateTimeException {
   public ZoneRulesException(String var1) {
      super(var1);
   }

   public ZoneRulesException(String var1, Throwable var2) {
      super(var1, var2);
   }
}
