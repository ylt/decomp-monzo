package org.threeten.bp.zone;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.threeten.bp.Duration;
import org.threeten.bp.Instant;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.ZoneOffset;

public final class ZoneOffsetTransition implements Serializable, Comparable {
   private final LocalDateTime a;
   private final ZoneOffset b;
   private final ZoneOffset c;

   ZoneOffsetTransition(long var1, ZoneOffset var3, ZoneOffset var4) {
      this.a = LocalDateTime.a(var1, 0, var3);
      this.b = var3;
      this.c = var4;
   }

   ZoneOffsetTransition(LocalDateTime var1, ZoneOffset var2, ZoneOffset var3) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
   }

   static ZoneOffsetTransition a(DataInput var0) throws IOException {
      long var1 = Ser.c(var0);
      ZoneOffset var3 = Ser.b(var0);
      ZoneOffset var4 = Ser.b(var0);
      if(var3.equals(var4)) {
         throw new IllegalArgumentException("Offsets must not be equal");
      } else {
         return new ZoneOffsetTransition(var1, var3, var4);
      }
   }

   private int j() {
      return this.f().f() - this.e().f();
   }

   private Object writeReplace() {
      return new Ser(2, this);
   }

   public int a(ZoneOffsetTransition var1) {
      return this.a().a(var1.a());
   }

   public Instant a() {
      return this.a.b((ZoneOffset)this.b);
   }

   void a(DataOutput var1) throws IOException {
      Ser.a(this.b(), var1);
      Ser.a(this.b, var1);
      Ser.a(this.c, var1);
   }

   public long b() {
      return this.a.c((ZoneOffset)this.b);
   }

   public LocalDateTime c() {
      return this.a;
   }

   // $FF: synthetic method
   public int compareTo(Object var1) {
      return this.a((ZoneOffsetTransition)var1);
   }

   public LocalDateTime d() {
      return this.a.d((long)this.j());
   }

   public ZoneOffset e() {
      return this.b;
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(var1 != this) {
         if(var1 instanceof ZoneOffsetTransition) {
            ZoneOffsetTransition var3 = (ZoneOffsetTransition)var1;
            if(!this.a.equals(var3.a) || !this.b.equals(var3.b) || !this.c.equals(var3.c)) {
               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public ZoneOffset f() {
      return this.c;
   }

   public Duration g() {
      return Duration.a((long)this.j());
   }

   public boolean h() {
      boolean var1;
      if(this.f().f() > this.e().f()) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public int hashCode() {
      return this.a.hashCode() ^ this.b.hashCode() ^ Integer.rotateLeft(this.c.hashCode(), 16);
   }

   List i() {
      List var1;
      if(this.h()) {
         var1 = Collections.emptyList();
      } else {
         var1 = Arrays.asList(new ZoneOffset[]{this.e(), this.f()});
      }

      return var1;
   }

   public String toString() {
      StringBuilder var3 = new StringBuilder();
      StringBuilder var2 = var3.append("Transition[");
      String var1;
      if(this.h()) {
         var1 = "Gap";
      } else {
         var1 = "Overlap";
      }

      var2.append(var1).append(" at ").append(this.a).append(this.b).append(" to ").append(this.c).append(']');
      return var3.toString();
   }
}
