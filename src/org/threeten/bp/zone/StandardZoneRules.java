package org.threeten.bp.zone;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import org.threeten.bp.Instant;
import org.threeten.bp.LocalDate;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.ZoneOffset;
import org.threeten.bp.chrono.ChronoLocalDateTime;
import org.threeten.bp.jdk8.Jdk8Methods;

final class StandardZoneRules extends ZoneRules implements Serializable {
   private final long[] a;
   private final ZoneOffset[] b;
   private final long[] c;
   private final LocalDateTime[] d;
   private final ZoneOffset[] e;
   private final ZoneOffsetTransitionRule[] f;
   private final ConcurrentMap g = new ConcurrentHashMap();

   private StandardZoneRules(long[] var1, ZoneOffset[] var2, long[] var3, ZoneOffset[] var4, ZoneOffsetTransitionRule[] var5) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
      this.e = var4;
      this.f = var5;
      ArrayList var7 = new ArrayList();

      for(int var6 = 0; var6 < var3.length; ++var6) {
         ZoneOffset var10 = var4[var6];
         ZoneOffset var8 = var4[var6 + 1];
         ZoneOffsetTransition var9 = new ZoneOffsetTransition(var3[var6], var10, var8);
         if(var9.h()) {
            var7.add(var9.c());
            var7.add(var9.d());
         } else {
            var7.add(var9.d());
            var7.add(var9.c());
         }
      }

      this.d = (LocalDateTime[])var7.toArray(new LocalDateTime[var7.size()]);
   }

   private int a(long var1, ZoneOffset var3) {
      return LocalDate.a(Jdk8Methods.e((long)var3.f() + var1, 86400L)).d();
   }

   private Object a(LocalDateTime var1, ZoneOffsetTransition var2) {
      LocalDateTime var3 = var2.c();
      Object var4;
      if(var2.h()) {
         if(var1.c((ChronoLocalDateTime)var3)) {
            var4 = var2.e();
         } else {
            var4 = var2;
            if(!var1.c((ChronoLocalDateTime)var2.d())) {
               var4 = var2.f();
            }
         }
      } else if(!var1.c((ChronoLocalDateTime)var3)) {
         var4 = var2.f();
      } else {
         var4 = var2;
         if(var1.c((ChronoLocalDateTime)var2.d())) {
            var4 = var2.e();
         }
      }

      return var4;
   }

   static StandardZoneRules a(DataInput var0) throws IOException, ClassNotFoundException {
      byte var2 = 0;
      int var3 = var0.readInt();
      long[] var5 = new long[var3];

      int var1;
      for(var1 = 0; var1 < var3; ++var1) {
         var5[var1] = Ser.c(var0);
      }

      ZoneOffset[] var6 = new ZoneOffset[var3 + 1];

      for(var1 = 0; var1 < var6.length; ++var1) {
         var6[var1] = Ser.b(var0);
      }

      var3 = var0.readInt();
      long[] var7 = new long[var3];

      for(var1 = 0; var1 < var3; ++var1) {
         var7[var1] = Ser.c(var0);
      }

      ZoneOffset[] var4 = new ZoneOffset[var3 + 1];

      for(var1 = 0; var1 < var4.length; ++var1) {
         var4[var1] = Ser.b(var0);
      }

      byte var9 = var0.readByte();
      ZoneOffsetTransitionRule[] var8 = new ZoneOffsetTransitionRule[var9];

      for(var1 = var2; var1 < var9; ++var1) {
         var8[var1] = ZoneOffsetTransitionRule.a(var0);
      }

      return new StandardZoneRules(var5, var6, var7, var4, var8);
   }

   private ZoneOffsetTransition[] a(int var1) {
      Integer var4 = Integer.valueOf(var1);
      ZoneOffsetTransition[] var3 = (ZoneOffsetTransition[])this.g.get(var4);
      if(var3 == null) {
         ZoneOffsetTransitionRule[] var5 = this.f;
         var3 = new ZoneOffsetTransition[var5.length];

         for(int var2 = 0; var2 < var5.length; ++var2) {
            var3[var2] = var5[var2].a(var1);
         }

         if(var1 < 2100) {
            this.g.putIfAbsent(var4, var3);
         }
      }

      return var3;
   }

   private Object c(LocalDateTime var1) {
      int var2 = 0;
      int var3;
      Object var4;
      if(this.f.length > 0 && var1.b((ChronoLocalDateTime)this.d[this.d.length - 1])) {
         ZoneOffsetTransition[] var11 = this.a(var1.b());
         var4 = null;

         Object var10;
         for(var3 = var11.length; var2 < var3; var4 = var10) {
            ZoneOffsetTransition var7 = var11[var2];
            var10 = this.a(var1, var7);
            var4 = var10;
            if(var10 instanceof ZoneOffsetTransition) {
               break;
            }

            if(var10.equals(var7.e())) {
               var4 = var10;
               break;
            }

            ++var2;
         }
      } else {
         var3 = Arrays.binarySearch(this.d, var1);
         if(var3 == -1) {
            var4 = this.e[0];
         } else {
            if(var3 < 0) {
               var2 = -var3 - 2;
            } else {
               var2 = var3;
               if(var3 < this.d.length - 1) {
                  var2 = var3;
                  if(this.d[var3].equals(this.d[var3 + 1])) {
                     var2 = var3 + 1;
                  }
               }
            }

            if((var2 & 1) == 0) {
               LocalDateTime var9 = this.d[var2];
               LocalDateTime var6 = this.d[var2 + 1];
               ZoneOffset var5 = this.e[var2 / 2];
               ZoneOffset var8 = this.e[var2 / 2 + 1];
               if(var8.f() > var5.f()) {
                  var4 = new ZoneOffsetTransition(var9, var5, var8);
               } else {
                  var4 = new ZoneOffsetTransition(var6, var5, var8);
               }
            } else {
               var4 = this.e[var2 / 2 + 1];
            }
         }
      }

      return var4;
   }

   private Object writeReplace() {
      return new Ser(1, this);
   }

   public List a(LocalDateTime var1) {
      Object var2 = this.c(var1);
      List var3;
      if(var2 instanceof ZoneOffsetTransition) {
         var3 = ((ZoneOffsetTransition)var2).i();
      } else {
         var3 = Collections.singletonList((ZoneOffset)var2);
      }

      return var3;
   }

   public ZoneOffset a(Instant var1) {
      long var4 = var1.a();
      int var2;
      ZoneOffset var7;
      if(this.f.length > 0 && var4 > this.c[this.c.length - 1]) {
         ZoneOffsetTransition[] var6 = this.a(this.a(var4, this.e[this.e.length - 1]));
         ZoneOffsetTransition var8 = null;
         var2 = 0;

         while(true) {
            if(var2 >= var6.length) {
               var7 = var8.f();
               break;
            }

            var8 = var6[var2];
            if(var4 < var8.b()) {
               var7 = var8.e();
               break;
            }

            ++var2;
         }
      } else {
         int var3 = Arrays.binarySearch(this.c, var4);
         var2 = var3;
         if(var3 < 0) {
            var2 = -var3 - 2;
         }

         var7 = this.e[var2 + 1];
      }

      return var7;
   }

   void a(DataOutput var1) throws IOException {
      byte var3 = 0;
      var1.writeInt(this.a.length);
      long[] var5 = this.a;
      int var4 = var5.length;

      int var2;
      for(var2 = 0; var2 < var4; ++var2) {
         Ser.a(var5[var2], var1);
      }

      ZoneOffset[] var6 = this.b;
      var4 = var6.length;

      for(var2 = 0; var2 < var4; ++var2) {
         Ser.a(var6[var2], var1);
      }

      var1.writeInt(this.c.length);
      var5 = this.c;
      var4 = var5.length;

      for(var2 = 0; var2 < var4; ++var2) {
         Ser.a(var5[var2], var1);
      }

      var6 = this.e;
      var4 = var6.length;

      for(var2 = 0; var2 < var4; ++var2) {
         Ser.a(var6[var2], var1);
      }

      var1.writeByte(this.f.length);
      ZoneOffsetTransitionRule[] var7 = this.f;
      var4 = var7.length;

      for(var2 = var3; var2 < var4; ++var2) {
         var7[var2].a(var1);
      }

   }

   public boolean a() {
      boolean var1;
      if(this.c.length == 0) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public boolean a(LocalDateTime var1, ZoneOffset var2) {
      return this.a(var1).contains(var2);
   }

   public ZoneOffset b(Instant var1) {
      long var4 = var1.a();
      int var3 = Arrays.binarySearch(this.a, var4);
      int var2 = var3;
      if(var3 < 0) {
         var2 = -var3 - 2;
      }

      return this.b[var2 + 1];
   }

   public ZoneOffsetTransition b(LocalDateTime var1) {
      Object var2 = this.c(var1);
      ZoneOffsetTransition var3;
      if(var2 instanceof ZoneOffsetTransition) {
         var3 = (ZoneOffsetTransition)var2;
      } else {
         var3 = null;
      }

      return var3;
   }

   public boolean c(Instant var1) {
      boolean var2;
      if(!this.b(var1).equals(this.a(var1))) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(this != var1) {
         if(var1 instanceof StandardZoneRules) {
            StandardZoneRules var3 = (StandardZoneRules)var1;
            if(!Arrays.equals(this.a, var3.a) || !Arrays.equals(this.b, var3.b) || !Arrays.equals(this.c, var3.c) || !Arrays.equals(this.e, var3.e) || !Arrays.equals(this.f, var3.f)) {
               var2 = false;
            }
         } else if(var1 instanceof ZoneRules.Fixed) {
            if(!this.a() || !this.a(Instant.a).equals(((ZoneRules.Fixed)var1).a(Instant.a))) {
               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public int hashCode() {
      return Arrays.hashCode(this.a) ^ Arrays.hashCode(this.b) ^ Arrays.hashCode(this.c) ^ Arrays.hashCode(this.e) ^ Arrays.hashCode(this.f);
   }

   public String toString() {
      return "StandardZoneRules[currentStandardOffset=" + this.b[this.b.length - 1] + "]";
   }
}
