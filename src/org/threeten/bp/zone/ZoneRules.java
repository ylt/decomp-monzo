package org.threeten.bp.zone;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import org.threeten.bp.Instant;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.ZoneOffset;
import org.threeten.bp.jdk8.Jdk8Methods;

public abstract class ZoneRules {
   public static ZoneRules a(ZoneOffset var0) {
      Jdk8Methods.a(var0, (String)"offset");
      return new ZoneRules.Fixed(var0);
   }

   public abstract List a(LocalDateTime var1);

   public abstract ZoneOffset a(Instant var1);

   public abstract boolean a();

   public abstract boolean a(LocalDateTime var1, ZoneOffset var2);

   public abstract ZoneOffsetTransition b(LocalDateTime var1);

   public abstract boolean c(Instant var1);

   static final class Fixed extends ZoneRules implements Serializable {
      private final ZoneOffset a;

      Fixed(ZoneOffset var1) {
         this.a = var1;
      }

      public List a(LocalDateTime var1) {
         return Collections.singletonList(this.a);
      }

      public ZoneOffset a(Instant var1) {
         return this.a;
      }

      public boolean a() {
         return true;
      }

      public boolean a(LocalDateTime var1, ZoneOffset var2) {
         return this.a.equals(var2);
      }

      public ZoneOffsetTransition b(LocalDateTime var1) {
         return null;
      }

      public boolean c(Instant var1) {
         return false;
      }

      public boolean equals(Object var1) {
         boolean var2 = true;
         if(this != var1) {
            if(var1 instanceof ZoneRules.Fixed) {
               var2 = this.a.equals(((ZoneRules.Fixed)var1).a);
            } else if(var1 instanceof StandardZoneRules) {
               StandardZoneRules var3 = (StandardZoneRules)var1;
               if(!var3.a() || !this.a.equals(var3.a(Instant.a))) {
                  var2 = false;
               }
            } else {
               var2 = false;
            }
         }

         return var2;
      }

      public int hashCode() {
         return this.a.hashCode() + 31 ^ 1 ^ 1 ^ this.a.hashCode() + 31 ^ 1;
      }

      public String toString() {
         return "FixedRules:" + this.a;
      }
   }
}
