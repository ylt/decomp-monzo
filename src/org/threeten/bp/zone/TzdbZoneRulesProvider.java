package org.threeten.bp.zone;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StreamCorruptedException;
import java.net.URL;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.atomic.AtomicReferenceArray;
import org.threeten.bp.jdk8.Jdk8Methods;

public final class TzdbZoneRulesProvider extends ZoneRulesProvider {
   private List a;
   private final ConcurrentNavigableMap b = new ConcurrentSkipListMap();
   private Set c = new CopyOnWriteArraySet();

   public TzdbZoneRulesProvider() {
      if(!this.a(ZoneRulesProvider.class.getClassLoader())) {
         throw new ZoneRulesException("No time-zone rules found for 'TZDB'");
      }
   }

   public TzdbZoneRulesProvider(InputStream var1) {
      try {
         this.a(var1);
      } catch (Exception var2) {
         throw new ZoneRulesException("Unable to load TZDB time-zone rules", var2);
      }
   }

   private boolean a(InputStream var1) throws IOException, StreamCorruptedException {
      boolean var2 = false;

      for(Iterator var5 = this.b(var1).iterator(); var5.hasNext(); var2 = true) {
         TzdbZoneRulesProvider.Version var3 = (TzdbZoneRulesProvider.Version)var5.next();
         TzdbZoneRulesProvider.Version var4 = (TzdbZoneRulesProvider.Version)this.b.putIfAbsent(var3.a, var3);
         if(var4 != null && !var4.a.equals(var3.a)) {
            throw new ZoneRulesException("Data already loaded for TZDB time-zone rules version: " + var3.a);
         }
      }

      return var2;
   }

   private boolean a(ClassLoader param1) {
      // $FF: Couldn't be decompiled
   }

   private boolean a(URL param1) throws ClassNotFoundException, IOException, ZoneRulesException {
      // $FF: Couldn't be decompiled
   }

   private Iterable b(InputStream var1) throws IOException, StreamCorruptedException {
      DataInputStream var7 = new DataInputStream(var1);
      if(var7.readByte() != 1) {
         throw new StreamCorruptedException("File format not recognised");
      } else if(!"TZDB".equals(var7.readUTF())) {
         throw new StreamCorruptedException("File format not recognised");
      } else {
         short var4 = var7.readShort();
         String[] var6 = new String[var4];

         int var2;
         for(var2 = 0; var2 < var4; ++var2) {
            var6[var2] = var7.readUTF();
         }

         short var3 = var7.readShort();
         String[] var12 = new String[var3];

         for(var2 = 0; var2 < var3; ++var2) {
            var12[var2] = var7.readUTF();
         }

         this.a = Arrays.asList(var12);
         var3 = var7.readShort();
         Object[] var8 = new Object[var3];

         for(var2 = 0; var2 < var3; ++var2) {
            byte[] var9 = new byte[var7.readShort()];
            var7.readFully(var9);
            var8[var2] = var9;
         }

         AtomicReferenceArray var15 = new AtomicReferenceArray(var8);
         HashSet var14 = new HashSet(var4);

         for(var2 = 0; var2 < var4; ++var2) {
            short var5 = var7.readShort();
            String[] var10 = new String[var5];
            short[] var11 = new short[var5];

            for(int var13 = 0; var13 < var5; ++var13) {
               var10[var13] = var12[var7.readShort()];
               var11[var13] = var7.readShort();
            }

            var14.add(new TzdbZoneRulesProvider.Version(var6[var2], var10, var11, var15));
         }

         return var14;
      }
   }

   protected Set a() {
      return new HashSet(this.a);
   }

   protected ZoneRules a(String var1, boolean var2) {
      Jdk8Methods.a(var1, (String)"zoneId");
      ZoneRules var3 = ((TzdbZoneRulesProvider.Version)this.b.lastEntry().getValue()).a(var1);
      if(var3 == null) {
         throw new ZoneRulesException("Unknown time-zone ID: " + var1);
      } else {
         return var3;
      }
   }

   public String toString() {
      return "TZDB";
   }

   static class Version {
      private final String a;
      private final String[] b;
      private final short[] c;
      private final AtomicReferenceArray d;

      Version(String var1, String[] var2, short[] var3, AtomicReferenceArray var4) {
         this.d = var4;
         this.a = var1;
         this.b = var2;
         this.c = var3;
      }

      ZoneRules a(String var1) {
         int var2 = Arrays.binarySearch(this.b, var1);
         ZoneRules var5;
         if(var2 < 0) {
            var5 = null;
         } else {
            ZoneRules var3;
            try {
               var3 = this.a(this.c[var2]);
            } catch (Exception var4) {
               throw new ZoneRulesException("Invalid binary time-zone data: TZDB:" + var1 + ", version: " + this.a, var4);
            }

            var5 = var3;
         }

         return var5;
      }

      ZoneRules a(short var1) throws Exception {
         Object var3 = this.d.get(var1);
         Object var2 = var3;
         if(var3 instanceof byte[]) {
            var2 = Ser.a(new DataInputStream(new ByteArrayInputStream((byte[])((byte[])var3))));
            this.d.set(var1, var2);
         }

         return (ZoneRules)var2;
      }

      public String toString() {
         return this.a;
      }
   }
}
