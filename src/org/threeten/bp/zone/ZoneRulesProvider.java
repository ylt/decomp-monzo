package org.threeten.bp.zone;

import java.util.HashSet;
import java.util.Iterator;
import java.util.ServiceConfigurationError;
import java.util.ServiceLoader;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CopyOnWriteArrayList;
import org.threeten.bp.jdk8.Jdk8Methods;

public abstract class ZoneRulesProvider {
   private static final CopyOnWriteArrayList a = new CopyOnWriteArrayList();
   private static final ConcurrentMap b = new ConcurrentHashMap(512, 0.75F, 2);

   static {
      Iterator var0 = ServiceLoader.load(ZoneRulesProvider.class, ZoneRulesProvider.class.getClassLoader()).iterator();

      while(var0.hasNext()) {
         ZoneRulesProvider var1 = (ZoneRulesProvider)var0.next();

         try {
            b(var1);
         } catch (ServiceConfigurationError var2) {
            if(!(var2.getCause() instanceof SecurityException)) {
               throw var2;
            }
         }
      }

   }

   private static ZoneRulesProvider a(String var0) {
      ZoneRulesProvider var1 = (ZoneRulesProvider)b.get(var0);
      if(var1 == null) {
         if(b.isEmpty()) {
            throw new ZoneRulesException("No time-zone data files registered");
         } else {
            throw new ZoneRulesException("Unknown time-zone ID: " + var0);
         }
      } else {
         return var1;
      }
   }

   public static void a(ZoneRulesProvider var0) {
      Jdk8Methods.a(var0, (String)"provider");
      b(var0);
      a.add(var0);
   }

   public static Set b() {
      return new HashSet(b.keySet());
   }

   public static ZoneRules b(String var0, boolean var1) {
      Jdk8Methods.a(var0, (String)"zoneId");
      return a(var0).a(var0, var1);
   }

   private static void b(ZoneRulesProvider var0) {
      Iterator var1 = var0.a().iterator();

      String var2;
      do {
         if(!var1.hasNext()) {
            return;
         }

         var2 = (String)var1.next();
         Jdk8Methods.a(var2, (String)"zoneId");
      } while((ZoneRulesProvider)b.putIfAbsent(var2, var0) == null);

      throw new ZoneRulesException("Unable to register zone as one already registered with that ID: " + var2 + ", currently loading from provider: " + var0);
   }

   protected abstract Set a();

   protected abstract ZoneRules a(String var1, boolean var2);
}
