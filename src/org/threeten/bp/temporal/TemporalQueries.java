package org.threeten.bp.temporal;

import org.threeten.bp.LocalDate;
import org.threeten.bp.LocalTime;
import org.threeten.bp.ZoneId;
import org.threeten.bp.ZoneOffset;
import org.threeten.bp.chrono.Chronology;

public final class TemporalQueries {
   static final TemporalQuery a = new TemporalQuery() {
      public ZoneId a(TemporalAccessor var1) {
         return (ZoneId)var1.a((TemporalQuery)this);
      }

      // $FF: synthetic method
      public Object b(TemporalAccessor var1) {
         return this.a(var1);
      }
   };
   static final TemporalQuery b = new TemporalQuery() {
      public Chronology a(TemporalAccessor var1) {
         return (Chronology)var1.a((TemporalQuery)this);
      }

      // $FF: synthetic method
      public Object b(TemporalAccessor var1) {
         return this.a(var1);
      }
   };
   static final TemporalQuery c = new TemporalQuery() {
      public TemporalUnit a(TemporalAccessor var1) {
         return (TemporalUnit)var1.a((TemporalQuery)this);
      }

      // $FF: synthetic method
      public Object b(TemporalAccessor var1) {
         return this.a(var1);
      }
   };
   static final TemporalQuery d = new TemporalQuery() {
      public ZoneId a(TemporalAccessor var1) {
         ZoneId var2 = (ZoneId)var1.a(TemporalQueries.a);
         ZoneId var3;
         if(var2 != null) {
            var3 = var2;
         } else {
            var3 = (ZoneId)var1.a(TemporalQueries.e);
         }

         return var3;
      }

      // $FF: synthetic method
      public Object b(TemporalAccessor var1) {
         return this.a(var1);
      }
   };
   static final TemporalQuery e = new TemporalQuery() {
      public ZoneOffset a(TemporalAccessor var1) {
         ZoneOffset var2;
         if(var1.a((TemporalField)ChronoField.D)) {
            var2 = ZoneOffset.a(var1.c(ChronoField.D));
         } else {
            var2 = null;
         }

         return var2;
      }

      // $FF: synthetic method
      public Object b(TemporalAccessor var1) {
         return this.a(var1);
      }
   };
   static final TemporalQuery f = new TemporalQuery() {
      public LocalDate a(TemporalAccessor var1) {
         LocalDate var2;
         if(var1.a((TemporalField)ChronoField.u)) {
            var2 = LocalDate.a(var1.d(ChronoField.u));
         } else {
            var2 = null;
         }

         return var2;
      }

      // $FF: synthetic method
      public Object b(TemporalAccessor var1) {
         return this.a(var1);
      }
   };
   static final TemporalQuery g = new TemporalQuery() {
      public LocalTime a(TemporalAccessor var1) {
         LocalTime var2;
         if(var1.a((TemporalField)ChronoField.b)) {
            var2 = LocalTime.b(var1.d(ChronoField.b));
         } else {
            var2 = null;
         }

         return var2;
      }

      // $FF: synthetic method
      public Object b(TemporalAccessor var1) {
         return this.a(var1);
      }
   };

   public static final TemporalQuery a() {
      return a;
   }

   public static final TemporalQuery b() {
      return b;
   }

   public static final TemporalQuery c() {
      return c;
   }

   public static final TemporalQuery d() {
      return d;
   }

   public static final TemporalQuery e() {
      return e;
   }

   public static final TemporalQuery f() {
      return f;
   }

   public static final TemporalQuery g() {
      return g;
   }
}
