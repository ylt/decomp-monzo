package org.threeten.bp.temporal;

import java.util.Map;
import org.threeten.bp.format.ResolverStyle;

public interface TemporalField {
   Temporal a(Temporal var1, long var2);

   TemporalAccessor a(Map var1, TemporalAccessor var2, ResolverStyle var3);

   ValueRange a();

   boolean a(TemporalAccessor var1);

   ValueRange b(TemporalAccessor var1);

   boolean b();

   long c(TemporalAccessor var1);

   boolean c();
}
