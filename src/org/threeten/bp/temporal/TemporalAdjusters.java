package org.threeten.bp.temporal;

import org.threeten.bp.DayOfWeek;
import org.threeten.bp.jdk8.Jdk8Methods;

public final class TemporalAdjusters {
   public static TemporalAdjuster a(DayOfWeek var0) {
      return new TemporalAdjusters.RelativeDayOfWeek(0, var0);
   }

   public static TemporalAdjuster b(DayOfWeek var0) {
      return new TemporalAdjusters.RelativeDayOfWeek(1, var0);
   }

   private static final class RelativeDayOfWeek implements TemporalAdjuster {
      private final int a;
      private final int b;

      private RelativeDayOfWeek(int var1, DayOfWeek var2) {
         Jdk8Methods.a(var2, (String)"dayOfWeek");
         this.a = var1;
         this.b = var2.a();
      }

      // $FF: synthetic method
      RelativeDayOfWeek(int var1, DayOfWeek var2, Object var3) {
         this(var1, var2);
      }

      public Temporal a(Temporal var1) {
         int var2 = var1.c(ChronoField.p);
         if(this.a >= 2 || var2 != this.b) {
            long var3;
            if((this.a & 1) == 0) {
               var2 -= this.b;
               if(var2 >= 0) {
                  var3 = (long)(7 - var2);
               } else {
                  var3 = (long)(-var2);
               }

               var1 = var1.d(var3, ChronoUnit.h);
            } else {
               var2 = this.b - var2;
               if(var2 >= 0) {
                  var3 = (long)(7 - var2);
               } else {
                  var3 = (long)(-var2);
               }

               var1 = var1.c(var3, ChronoUnit.h);
            }
         }

         return var1;
      }
   }
}
