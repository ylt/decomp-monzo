package org.threeten.bp.temporal;

import java.util.Map;
import org.threeten.bp.DayOfWeek;
import org.threeten.bp.Duration;
import org.threeten.bp.LocalDate;
import org.threeten.bp.chrono.Chronology;
import org.threeten.bp.chrono.IsoChronology;
import org.threeten.bp.format.ResolverStyle;
import org.threeten.bp.jdk8.Jdk8Methods;

public final class IsoFields {
   public static final TemporalField a;
   public static final TemporalField b;
   public static final TemporalField c;
   public static final TemporalField d;
   public static final TemporalUnit e;
   public static final TemporalUnit f;

   static {
      a = IsoFields.Field.a;
      b = IsoFields.Field.b;
      c = IsoFields.Field.c;
      d = IsoFields.Field.d;
      e = IsoFields.Unit.a;
      f = IsoFields.Unit.b;
   }

   private static enum Field implements TemporalField {
      a {
         public Temporal a(Temporal var1, long var2) {
            long var4 = this.c(var1);
            this.a().a(var2, this);
            return var1.b(ChronoField.t, var2 - var4 + var1.d(ChronoField.t));
         }

         public TemporalAccessor a(Map var1, TemporalAccessor var2, ResolverStyle var3) {
            Long var11 = (Long)var1.get(ChronoField.A);
            Long var12 = (Long)var1.get(b);
            LocalDate var13;
            if(var11 != null && var12 != null) {
               int var5 = ChronoField.A.b(var11.longValue());
               long var9 = ((Long)var1.get(a)).longValue();
               if(var3 == ResolverStyle.c) {
                  long var7 = var12.longValue();
                  var13 = LocalDate.a(var5, 1, 1).c(Jdk8Methods.a(Jdk8Methods.c(var7, 1L), 3)).e(Jdk8Methods.c(var9, 1L));
               } else {
                  int var6 = b.a().b(var12.longValue(), b);
                  if(var3 == ResolverStyle.a) {
                     byte var4 = 92;
                     if(var6 == 1) {
                        if(IsoChronology.b.a((long)var5)) {
                           var4 = 91;
                        } else {
                           var4 = 90;
                        }
                     } else if(var6 == 2) {
                        var4 = 91;
                     }

                     ValueRange.a(1L, (long)var4).a(var9, this);
                  } else {
                     this.a().a(var9, this);
                  }

                  var13 = LocalDate.a(var5, (var6 - 1) * 3 + 1, 1).e(var9 - 1L);
               }

               var1.remove(this);
               var1.remove(ChronoField.A);
               var1.remove(b);
            } else {
               var13 = null;
            }

            return var13;
         }

         public ValueRange a() {
            return ValueRange.a(1L, 90L, 92L);
         }

         public boolean a(TemporalAccessor var1) {
            boolean var2;
            if(var1.a((TemporalField)ChronoField.t) && var1.a((TemporalField)ChronoField.x) && var1.a((TemporalField)ChronoField.A) && IsoFields.Field.e(var1)) {
               var2 = true;
            } else {
               var2 = false;
            }

            return var2;
         }

         public ValueRange b(TemporalAccessor var1) {
            if(!var1.a((TemporalField)this)) {
               throw new UnsupportedTemporalTypeException("Unsupported field: DayOfQuarter");
            } else {
               long var2 = var1.d(b);
               ValueRange var4;
               if(var2 == 1L) {
                  var2 = var1.d(ChronoField.A);
                  if(IsoChronology.b.a(var2)) {
                     var4 = ValueRange.a(1L, 91L);
                  } else {
                     var4 = ValueRange.a(1L, 90L);
                  }
               } else if(var2 == 2L) {
                  var4 = ValueRange.a(1L, 91L);
               } else if(var2 != 3L && var2 != 4L) {
                  var4 = this.a();
               } else {
                  var4 = ValueRange.a(1L, 92L);
               }

               return var4;
            }
         }

         public long c(TemporalAccessor var1) {
            if(!var1.a((TemporalField)this)) {
               throw new UnsupportedTemporalTypeException("Unsupported field: DayOfQuarter");
            } else {
               int var3 = var1.c(ChronoField.t);
               int var2 = var1.c(ChronoField.x);
               long var5 = var1.d(ChronoField.A);
               int[] var7 = IsoFields.Field.e;
               int var4 = (var2 - 1) / 3;
               byte var8;
               if(IsoChronology.b.a(var5)) {
                  var8 = 4;
               } else {
                  var8 = 0;
               }

               return (long)(var3 - var7[var8 + var4]);
            }
         }

         public String toString() {
            return "DayOfQuarter";
         }
      },
      b {
         public Temporal a(Temporal var1, long var2) {
            long var4 = this.c(var1);
            this.a().a(var2, this);
            return var1.b(ChronoField.x, (var2 - var4) * 3L + var1.d(ChronoField.x));
         }

         public ValueRange a() {
            return ValueRange.a(1L, 4L);
         }

         public boolean a(TemporalAccessor var1) {
            boolean var2;
            if(var1.a((TemporalField)ChronoField.x) && IsoFields.Field.e(var1)) {
               var2 = true;
            } else {
               var2 = false;
            }

            return var2;
         }

         public ValueRange b(TemporalAccessor var1) {
            return this.a();
         }

         public long c(TemporalAccessor var1) {
            if(!var1.a((TemporalField)this)) {
               throw new UnsupportedTemporalTypeException("Unsupported field: QuarterOfYear");
            } else {
               return (var1.d(ChronoField.x) + 2L) / 3L;
            }
         }

         public String toString() {
            return "QuarterOfYear";
         }
      },
      c {
         public Temporal a(Temporal var1, long var2) {
            this.a().a(var2, this);
            return var1.d(Jdk8Methods.c(var2, this.c(var1)), ChronoUnit.i);
         }

         public TemporalAccessor a(Map var1, TemporalAccessor var2, ResolverStyle var3) {
            Long var15 = (Long)var1.get(d);
            Long var14 = (Long)var1.get(ChronoField.p);
            LocalDate var16;
            if(var15 != null && var14 != null) {
               int var4 = d.a().b(var15.longValue(), d);
               long var12 = ((Long)var1.get(c)).longValue();
               if(var3 == ResolverStyle.c) {
                  long var10 = var14.longValue();
                  long var8 = 0L;
                  long var6;
                  if(var10 > 7L) {
                     var8 = (var10 - 1L) / 7L;
                     var6 = (var10 - 1L) % 7L + 1L;
                  } else {
                     var6 = var10;
                     if(var10 < 1L) {
                        var8 = var10 / 7L - 1L;
                        var6 = var10 % 7L + 7L;
                     }
                  }

                  var16 = LocalDate.a(var4, 1, 4).d(var12 - 1L).d(var8).a(ChronoField.p, var6);
               } else {
                  int var5 = ChronoField.p.b(var14.longValue());
                  if(var3 == ResolverStyle.a) {
                     IsoFields.Field.d(LocalDate.a(var4, 1, 4)).a(var12, this);
                  } else {
                     this.a().a(var12, this);
                  }

                  var16 = LocalDate.a(var4, 1, 4).d(var12 - 1L).a(ChronoField.p, (long)var5);
               }

               var1.remove(this);
               var1.remove(d);
               var1.remove(ChronoField.p);
            } else {
               var16 = null;
            }

            return var16;
         }

         public ValueRange a() {
            return ValueRange.a(1L, 52L, 53L);
         }

         public boolean a(TemporalAccessor var1) {
            boolean var2;
            if(var1.a((TemporalField)ChronoField.u) && IsoFields.Field.e(var1)) {
               var2 = true;
            } else {
               var2 = false;
            }

            return var2;
         }

         public ValueRange b(TemporalAccessor var1) {
            if(!var1.a((TemporalField)this)) {
               throw new UnsupportedTemporalTypeException("Unsupported field: WeekOfWeekBasedYear");
            } else {
               return IsoFields.Field.d(LocalDate.a(var1));
            }
         }

         public long c(TemporalAccessor var1) {
            if(!var1.a((TemporalField)this)) {
               throw new UnsupportedTemporalTypeException("Unsupported field: WeekOfWeekBasedYear");
            } else {
               return (long)IsoFields.Field.e(LocalDate.a(var1));
            }
         }

         public String toString() {
            return "WeekOfWeekBasedYear";
         }
      },
      d {
         public Temporal a(Temporal var1, long var2) {
            byte var5 = 52;
            if(!this.a(var1)) {
               throw new UnsupportedTemporalTypeException("Unsupported field: WeekBasedYear");
            } else {
               int var6 = this.a().b(var2, d);
               LocalDate var8 = LocalDate.a((TemporalAccessor)var1);
               int var7 = var8.c((TemporalField)ChronoField.p);
               int var4 = IsoFields.Field.e(var8);
               if(var4 == 53 && IsoFields.Field.b(var6) == 52) {
                  var4 = var5;
               }

               var8 = LocalDate.a(var6, 1, 4);
               return var1.b(var8.e((long)((var4 - 1) * 7 + (var7 - var8.c((TemporalField)ChronoField.p)))));
            }
         }

         public ValueRange a() {
            return ChronoField.A.a();
         }

         public boolean a(TemporalAccessor var1) {
            boolean var2;
            if(var1.a((TemporalField)ChronoField.u) && IsoFields.Field.e(var1)) {
               var2 = true;
            } else {
               var2 = false;
            }

            return var2;
         }

         public ValueRange b(TemporalAccessor var1) {
            return ChronoField.A.a();
         }

         public long c(TemporalAccessor var1) {
            if(!var1.a((TemporalField)this)) {
               throw new UnsupportedTemporalTypeException("Unsupported field: WeekBasedYear");
            } else {
               return (long)IsoFields.Field.f(LocalDate.a(var1));
            }
         }

         public String toString() {
            return "WeekBasedYear";
         }
      };

      private static final int[] e = new int[]{0, 90, 181, 273, 0, 91, 182, 274};

      private Field() {
      }

      // $FF: synthetic method
      Field(Object var3) {
         this();
      }

      private static int b(int var0) {
         LocalDate var1 = LocalDate.a(var0, 1, 1);
         byte var2;
         if(var1.i() != DayOfWeek.d && (var1.i() != DayOfWeek.c || !var1.j())) {
            var2 = 52;
         } else {
            var2 = 53;
         }

         return var2;
      }

      private static ValueRange d(LocalDate var0) {
         return ValueRange.a(1L, (long)b(f(var0)));
      }

      private static int e(LocalDate var0) {
         byte var3 = 1;
         int var1 = var0.i().ordinal();
         int var2 = var0.h() - 1;
         var1 = 3 - var1 + var2;
         var1 = var1 - var1 / 7 * 7 - 3;
         if(var1 < -3) {
            var1 += 7;
         }

         if(var2 < var1) {
            var1 = (int)d(var0.d(180).f(1L)).c();
         } else {
            int var4 = (var2 - var1) / 7 + 1;
            if(var4 == 53) {
               boolean var5;
               if(var1 != -3 && (var1 != -2 || !var0.j())) {
                  var5 = false;
               } else {
                  var5 = true;
               }

               var1 = var3;
               if(!var5) {
                  return var1;
               }
            }

            var1 = var4;
         }

         return var1;
      }

      private static boolean e(TemporalAccessor var0) {
         return Chronology.a(var0).equals(IsoChronology.b);
      }

      private static int f(LocalDate var0) {
         int var3 = var0.d();
         int var5 = var0.h();
         int var1;
         if(var5 <= 3) {
            var1 = var3;
            if(var5 - var0.i().ordinal() < -2) {
               var1 = var3 - 1;
            }
         } else {
            var1 = var3;
            if(var5 >= 363) {
               int var4 = var0.i().ordinal();
               byte var2;
               if(var0.j()) {
                  var2 = 1;
               } else {
                  var2 = 0;
               }

               var1 = var3;
               if(var5 - 363 - var2 - var4 >= 0) {
                  var1 = var3 + 1;
               }
            }
         }

         return var1;
      }

      public TemporalAccessor a(Map var1, TemporalAccessor var2, ResolverStyle var3) {
         return null;
      }

      public boolean b() {
         return true;
      }

      public boolean c() {
         return false;
      }
   }

   private static enum Unit implements TemporalUnit {
      a("WeekBasedYears", Duration.a(31556952L)),
      b("QuarterYears", Duration.a(7889238L));

      private final String c;
      private final Duration d;

      private Unit(String var3, Duration var4) {
         this.c = var3;
         this.d = var4;
      }

      public long a(Temporal var1, Temporal var2) {
         long var3;
         switch(null.a[this.ordinal()]) {
         case 1:
            var3 = Jdk8Methods.c(var2.d(IsoFields.d), var1.d(IsoFields.d));
            break;
         case 2:
            var3 = var1.a(var2, ChronoUnit.j) / 3L;
            break;
         default:
            throw new IllegalStateException("Unreachable");
         }

         return var3;
      }

      public Temporal a(Temporal var1, long var2) {
         switch(null.a[this.ordinal()]) {
         case 1:
            var2 = Jdk8Methods.b((long)var1.c(IsoFields.d), var2);
            var1 = var1.b(IsoFields.d, var2);
            break;
         case 2:
            var1 = var1.d(var2 / 256L, ChronoUnit.k).d(var2 % 256L * 3L, ChronoUnit.j);
            break;
         default:
            throw new IllegalStateException("Unreachable");
         }

         return var1;
      }

      public boolean a() {
         return true;
      }

      public String toString() {
         return this.c;
      }
   }
}
