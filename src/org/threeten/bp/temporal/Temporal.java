package org.threeten.bp.temporal;

public interface Temporal extends TemporalAccessor {
   long a(Temporal var1, TemporalUnit var2);

   Temporal b(TemporalAdjuster var1);

   Temporal b(TemporalField var1, long var2);

   Temporal c(long var1, TemporalUnit var3);

   Temporal d(long var1, TemporalUnit var3);
}
