package org.threeten.bp.temporal;

public interface TemporalAccessor {
   Object a(TemporalQuery var1);

   boolean a(TemporalField var1);

   ValueRange b(TemporalField var1);

   int c(TemporalField var1);

   long d(TemporalField var1);
}
