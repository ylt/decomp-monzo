package org.threeten.bp.temporal;

import java.util.Map;
import org.threeten.bp.format.ResolverStyle;

public enum ChronoField implements TemporalField {
   A("Year", ChronoUnit.k, ChronoUnit.p, ValueRange.a(-999999999L, 999999999L)),
   B("Era", ChronoUnit.o, ChronoUnit.p, ValueRange.a(0L, 1L)),
   C("InstantSeconds", ChronoUnit.d, ChronoUnit.p, ValueRange.a(Long.MIN_VALUE, Long.MAX_VALUE)),
   D("OffsetSeconds", ChronoUnit.d, ChronoUnit.p, ValueRange.a(-64800L, 64800L)),
   a("NanoOfSecond", ChronoUnit.a, ChronoUnit.d, ValueRange.a(0L, 999999999L)),
   b("NanoOfDay", ChronoUnit.a, ChronoUnit.h, ValueRange.a(0L, 86399999999999L)),
   c("MicroOfSecond", ChronoUnit.b, ChronoUnit.d, ValueRange.a(0L, 999999L)),
   d("MicroOfDay", ChronoUnit.b, ChronoUnit.h, ValueRange.a(0L, 86399999999L)),
   e("MilliOfSecond", ChronoUnit.c, ChronoUnit.d, ValueRange.a(0L, 999L)),
   f("MilliOfDay", ChronoUnit.c, ChronoUnit.h, ValueRange.a(0L, 86399999L)),
   g("SecondOfMinute", ChronoUnit.d, ChronoUnit.e, ValueRange.a(0L, 59L)),
   h("SecondOfDay", ChronoUnit.d, ChronoUnit.h, ValueRange.a(0L, 86399L)),
   i("MinuteOfHour", ChronoUnit.e, ChronoUnit.f, ValueRange.a(0L, 59L)),
   j("MinuteOfDay", ChronoUnit.e, ChronoUnit.h, ValueRange.a(0L, 1439L)),
   k("HourOfAmPm", ChronoUnit.f, ChronoUnit.g, ValueRange.a(0L, 11L)),
   l("ClockHourOfAmPm", ChronoUnit.f, ChronoUnit.g, ValueRange.a(1L, 12L)),
   m("HourOfDay", ChronoUnit.f, ChronoUnit.h, ValueRange.a(0L, 23L)),
   n("ClockHourOfDay", ChronoUnit.f, ChronoUnit.h, ValueRange.a(1L, 24L)),
   o("AmPmOfDay", ChronoUnit.g, ChronoUnit.h, ValueRange.a(0L, 1L)),
   p("DayOfWeek", ChronoUnit.h, ChronoUnit.i, ValueRange.a(1L, 7L)),
   q("AlignedDayOfWeekInMonth", ChronoUnit.h, ChronoUnit.i, ValueRange.a(1L, 7L)),
   r("AlignedDayOfWeekInYear", ChronoUnit.h, ChronoUnit.i, ValueRange.a(1L, 7L)),
   s("DayOfMonth", ChronoUnit.h, ChronoUnit.j, ValueRange.a(1L, 28L, 31L)),
   t("DayOfYear", ChronoUnit.h, ChronoUnit.k, ValueRange.a(1L, 365L, 366L)),
   u("EpochDay", ChronoUnit.h, ChronoUnit.p, ValueRange.a(-365243219162L, 365241780471L)),
   v("AlignedWeekOfMonth", ChronoUnit.i, ChronoUnit.j, ValueRange.a(1L, 4L, 5L)),
   w("AlignedWeekOfYear", ChronoUnit.i, ChronoUnit.k, ValueRange.a(1L, 53L)),
   x("MonthOfYear", ChronoUnit.j, ChronoUnit.k, ValueRange.a(1L, 12L)),
   y("ProlepticMonth", ChronoUnit.j, ChronoUnit.p, ValueRange.a(-11999999988L, 11999999999L)),
   z("YearOfEra", ChronoUnit.k, ChronoUnit.p, ValueRange.a(1L, 999999999L, 1000000000L));

   private final String E;
   private final TemporalUnit F;
   private final TemporalUnit G;
   private final ValueRange H;

   private ChronoField(String var3, TemporalUnit var4, TemporalUnit var5, ValueRange var6) {
      this.E = var3;
      this.F = var4;
      this.G = var5;
      this.H = var6;
   }

   public long a(long var1) {
      return this.a().a(var1, this);
   }

   public Temporal a(Temporal var1, long var2) {
      return var1.b(this, var2);
   }

   public TemporalAccessor a(Map var1, TemporalAccessor var2, ResolverStyle var3) {
      return null;
   }

   public ValueRange a() {
      return this.H;
   }

   public boolean a(TemporalAccessor var1) {
      return var1.a((TemporalField)this);
   }

   public int b(long var1) {
      return this.a().b(var1, this);
   }

   public ValueRange b(TemporalAccessor var1) {
      return var1.b(this);
   }

   public boolean b() {
      boolean var1;
      if(this.ordinal() >= p.ordinal() && this.ordinal() <= B.ordinal()) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public long c(TemporalAccessor var1) {
      return var1.d(this);
   }

   public boolean c() {
      boolean var1;
      if(this.ordinal() < p.ordinal()) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public String toString() {
      return this.E;
   }
}
