package org.threeten.bp.temporal;

import org.threeten.bp.Duration;

public enum ChronoUnit implements TemporalUnit {
   a("Nanos", Duration.b(1L)),
   b("Micros", Duration.b(1000L)),
   c("Millis", Duration.b(1000000L)),
   d("Seconds", Duration.a(1L)),
   e("Minutes", Duration.a(60L)),
   f("Hours", Duration.a(3600L)),
   g("HalfDays", Duration.a(43200L)),
   h("Days", Duration.a(86400L)),
   i("Weeks", Duration.a(604800L)),
   j("Months", Duration.a(2629746L)),
   k("Years", Duration.a(31556952L)),
   l("Decades", Duration.a(315569520L)),
   m("Centuries", Duration.a(3155695200L)),
   n("Millennia", Duration.a(31556952000L)),
   o("Eras", Duration.a(31556952000000000L)),
   p("Forever", Duration.a(Long.MAX_VALUE, 999999999L));

   private final String q;
   private final Duration r;

   private ChronoUnit(String var3, Duration var4) {
      this.q = var3;
      this.r = var4;
   }

   public long a(Temporal var1, Temporal var2) {
      return var1.a(var2, this);
   }

   public Temporal a(Temporal var1, long var2) {
      return var1.d(var2, this);
   }

   public boolean a() {
      boolean var1;
      if(this.compareTo(h) >= 0 && this != p) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public boolean b() {
      boolean var1;
      if(this.compareTo(h) < 0) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public String toString() {
      return this.q;
   }
}
