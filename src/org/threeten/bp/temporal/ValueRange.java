package org.threeten.bp.temporal;

import java.io.Serializable;
import org.threeten.bp.DateTimeException;

public final class ValueRange implements Serializable {
   private final long a;
   private final long b;
   private final long c;
   private final long d;

   private ValueRange(long var1, long var3, long var5, long var7) {
      this.a = var1;
      this.b = var3;
      this.c = var5;
      this.d = var7;
   }

   public static ValueRange a(long var0, long var2) {
      if(var0 > var2) {
         throw new IllegalArgumentException("Minimum value must be less than maximum value");
      } else {
         return new ValueRange(var0, var0, var2, var2);
      }
   }

   public static ValueRange a(long var0, long var2, long var4) {
      return a(var0, var0, var2, var4);
   }

   public static ValueRange a(long var0, long var2, long var4, long var6) {
      if(var0 > var2) {
         throw new IllegalArgumentException("Smallest minimum value must be less than largest minimum value");
      } else if(var4 > var6) {
         throw new IllegalArgumentException("Smallest maximum value must be less than largest maximum value");
      } else if(var2 > var6) {
         throw new IllegalArgumentException("Minimum value must be less than maximum value");
      } else {
         return new ValueRange(var0, var2, var4, var6);
      }
   }

   public long a(long var1, TemporalField var3) {
      if(!this.a(var1)) {
         if(var3 != null) {
            throw new DateTimeException("Invalid value for " + var3 + " (valid values " + this + "): " + var1);
         } else {
            throw new DateTimeException("Invalid value (valid values " + this + "): " + var1);
         }
      } else {
         return var1;
      }
   }

   public boolean a() {
      boolean var1;
      if(this.a == this.b && this.c == this.d) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public boolean a(long var1) {
      boolean var3;
      if(var1 >= this.b() && var1 <= this.c()) {
         var3 = true;
      } else {
         var3 = false;
      }

      return var3;
   }

   public int b(long var1, TemporalField var3) {
      if(!this.b(var1)) {
         throw new DateTimeException("Invalid int value for " + var3 + ": " + var1);
      } else {
         return (int)var1;
      }
   }

   public long b() {
      return this.a;
   }

   public boolean b(long var1) {
      boolean var3;
      if(this.d() && this.a(var1)) {
         var3 = true;
      } else {
         var3 = false;
      }

      return var3;
   }

   public long c() {
      return this.d;
   }

   public boolean d() {
      boolean var1;
      if(this.b() >= -2147483648L && this.c() <= 2147483647L) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(var1 != this) {
         if(var1 instanceof ValueRange) {
            ValueRange var3 = (ValueRange)var1;
            if(this.a != var3.a || this.b != var3.b || this.c != var3.c || this.d != var3.d) {
               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public int hashCode() {
      long var1 = this.a + this.b << (int)(16L + this.b) >> (int)(this.c + 48L) << (int)(this.c + 32L) >> (int)(this.d + 32L) << (int)(this.d + 48L) >> 16;
      return (int)(var1 ^ var1 >>> 32);
   }

   public String toString() {
      StringBuilder var1 = new StringBuilder();
      var1.append(this.a);
      if(this.a != this.b) {
         var1.append('/').append(this.b);
      }

      var1.append(" - ").append(this.c);
      if(this.c != this.d) {
         var1.append('/').append(this.d);
      }

      return var1.toString();
   }
}
