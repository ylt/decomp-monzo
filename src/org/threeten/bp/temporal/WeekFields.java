package org.threeten.bp.temporal;

import java.io.InvalidObjectException;
import java.io.Serializable;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import org.threeten.bp.DateTimeException;
import org.threeten.bp.DayOfWeek;
import org.threeten.bp.Year;
import org.threeten.bp.chrono.ChronoLocalDate;
import org.threeten.bp.chrono.Chronology;
import org.threeten.bp.format.ResolverStyle;
import org.threeten.bp.jdk8.Jdk8Methods;

public final class WeekFields implements Serializable {
   public static final WeekFields a;
   public static final WeekFields b;
   private static final ConcurrentMap c = new ConcurrentHashMap(4, 0.75F, 2);
   private final DayOfWeek d;
   private final int e;
   private final transient TemporalField f = WeekFields.ComputedDayOfField.a(this);
   private final transient TemporalField g = WeekFields.ComputedDayOfField.b(this);
   private final transient TemporalField h = WeekFields.ComputedDayOfField.c(this);
   private final transient TemporalField i = WeekFields.ComputedDayOfField.d(this);
   private final transient TemporalField j = WeekFields.ComputedDayOfField.e(this);

   static {
      a = new WeekFields(DayOfWeek.a, 4);
      b = a(DayOfWeek.g, 1);
   }

   private WeekFields(DayOfWeek var1, int var2) {
      Jdk8Methods.a(var1, (String)"firstDayOfWeek");
      if(var2 >= 1 && var2 <= 7) {
         this.d = var1;
         this.e = var2;
      } else {
         throw new IllegalArgumentException("Minimal number of days is invalid");
      }
   }

   public static WeekFields a(Locale var0) {
      Jdk8Methods.a(var0, (String)"locale");
      GregorianCalendar var2 = new GregorianCalendar(new Locale(var0.getLanguage(), var0.getCountry()));
      int var1 = var2.getFirstDayOfWeek();
      return a(DayOfWeek.g.a((long)(var1 - 1)), var2.getMinimalDaysInFirstWeek());
   }

   public static WeekFields a(DayOfWeek var0, int var1) {
      String var4 = var0.toString() + var1;
      WeekFields var3 = (WeekFields)c.get(var4);
      WeekFields var2 = var3;
      if(var3 == null) {
         WeekFields var5 = new WeekFields(var0, var1);
         c.putIfAbsent(var4, var5);
         var2 = (WeekFields)c.get(var4);
      }

      return var2;
   }

   private Object readResolve() throws InvalidObjectException {
      try {
         WeekFields var1 = a(this.d, this.e);
         return var1;
      } catch (IllegalArgumentException var2) {
         throw new InvalidObjectException("Invalid WeekFields" + var2.getMessage());
      }
   }

   public DayOfWeek a() {
      return this.d;
   }

   public int b() {
      return this.e;
   }

   public TemporalField c() {
      return this.f;
   }

   public TemporalField d() {
      return this.g;
   }

   public TemporalField e() {
      return this.i;
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(this != var1) {
         if(var1 instanceof WeekFields) {
            if(this.hashCode() != var1.hashCode()) {
               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public TemporalField f() {
      return this.j;
   }

   public int hashCode() {
      return this.d.ordinal() * 7 + this.e;
   }

   public String toString() {
      return "WeekFields[" + this.d + ',' + this.e + ']';
   }

   static class ComputedDayOfField implements TemporalField {
      private static final ValueRange f = ValueRange.a(1L, 7L);
      private static final ValueRange g = ValueRange.a(0L, 1L, 4L, 6L);
      private static final ValueRange h = ValueRange.a(0L, 1L, 52L, 54L);
      private static final ValueRange i = ValueRange.a(1L, 52L, 53L);
      private static final ValueRange j;
      private final String a;
      private final WeekFields b;
      private final TemporalUnit c;
      private final TemporalUnit d;
      private final ValueRange e;

      static {
         j = ChronoField.A.a();
      }

      private ComputedDayOfField(String var1, WeekFields var2, TemporalUnit var3, TemporalUnit var4, ValueRange var5) {
         this.a = var1;
         this.b = var2;
         this.c = var3;
         this.d = var4;
         this.e = var5;
      }

      private int a(int var1, int var2) {
         var2 = Jdk8Methods.d(var1 - var2, 7);
         var1 = -var2;
         if(var2 + 1 > this.b.b()) {
            var1 = 7 - var2;
         }

         return var1;
      }

      private int a(TemporalAccessor var1, int var2) {
         return Jdk8Methods.d(var1.c(ChronoField.p) - var2, 7) + 1;
      }

      static WeekFields.ComputedDayOfField a(WeekFields var0) {
         return new WeekFields.ComputedDayOfField("DayOfWeek", var0, ChronoUnit.h, ChronoUnit.i, f);
      }

      private int b(int var1, int var2) {
         return (var1 + 7 + (var2 - 1)) / 7;
      }

      private long b(TemporalAccessor var1, int var2) {
         int var3 = var1.c(ChronoField.s);
         return (long)this.b(this.a(var3, var2), var3);
      }

      static WeekFields.ComputedDayOfField b(WeekFields var0) {
         return new WeekFields.ComputedDayOfField("WeekOfMonth", var0, ChronoUnit.i, ChronoUnit.j, g);
      }

      private long c(TemporalAccessor var1, int var2) {
         int var3 = var1.c(ChronoField.t);
         return (long)this.b(this.a(var3, var2), var3);
      }

      static WeekFields.ComputedDayOfField c(WeekFields var0) {
         return new WeekFields.ComputedDayOfField("WeekOfYear", var0, ChronoUnit.i, ChronoUnit.k, h);
      }

      private int d(TemporalAccessor var1) {
         int var2 = this.b.a().a();
         var2 = Jdk8Methods.d(var1.c(ChronoField.p) - var2, 7) + 1;
         long var4 = this.c(var1, var2);
         if(var4 == 0L) {
            var2 = (int)this.c(Chronology.a(var1).b(var1).e(1L, ChronoUnit.i), var2) + 1;
         } else {
            if(var4 >= 53L) {
               int var3 = this.a(var1.c(ChronoField.t), var2);
               short var6;
               if(Year.a((long)var1.c(ChronoField.A))) {
                  var6 = 366;
               } else {
                  var6 = 365;
               }

               var2 = this.b(var3, var6 + this.b.b());
               if(var4 >= (long)var2) {
                  var2 = (int)(var4 - (long)(var2 - 1));
                  return var2;
               }
            }

            var2 = (int)var4;
         }

         return var2;
      }

      static WeekFields.ComputedDayOfField d(WeekFields var0) {
         return new WeekFields.ComputedDayOfField("WeekOfWeekBasedYear", var0, ChronoUnit.i, IsoFields.e, i);
      }

      private int e(TemporalAccessor var1) {
         int var2 = this.b.a().a();
         int var3 = Jdk8Methods.d(var1.c(ChronoField.p) - var2, 7) + 1;
         var2 = var1.c(ChronoField.A);
         long var5 = this.c(var1, var3);
         if(var5 == 0L) {
            --var2;
         } else if(var5 >= 53L) {
            int var4 = this.a(var1.c(ChronoField.t), var3);
            short var7;
            if(Year.a((long)var2)) {
               var7 = 366;
            } else {
               var7 = 365;
            }

            if(var5 >= (long)this.b(var4, var7 + this.b.b())) {
               ++var2;
            }
         }

         return var2;
      }

      static WeekFields.ComputedDayOfField e(WeekFields var0) {
         return new WeekFields.ComputedDayOfField("WeekBasedYear", var0, IsoFields.e, ChronoUnit.p, j);
      }

      private ValueRange f(TemporalAccessor var1) {
         int var2 = this.b.a().a();
         var2 = Jdk8Methods.d(var1.c(ChronoField.p) - var2, 7) + 1;
         long var4 = this.c(var1, var2);
         ValueRange var6;
         if(var4 == 0L) {
            var6 = this.f(Chronology.a(var1).b(var1).e(2L, ChronoUnit.i));
         } else {
            int var3 = this.a(var1.c(ChronoField.t), var2);
            short var7;
            if(Year.a((long)var1.c(ChronoField.A))) {
               var7 = 366;
            } else {
               var7 = 365;
            }

            var2 = this.b(var3, var7 + this.b.b());
            if(var4 >= (long)var2) {
               var6 = this.f(Chronology.a(var1).b(var1).f(2L, ChronoUnit.i));
            } else {
               var6 = ValueRange.a(1L, (long)(var2 - 1));
            }
         }

         return var6;
      }

      public Temporal a(Temporal var1, long var2) {
         int var4 = this.e.b(var2, this);
         int var5 = var1.c(this);
         if(var4 != var5) {
            if(this.d == ChronoUnit.p) {
               int var6 = var1.c(this.b.i);
               Temporal var7 = var1.d((long)((double)(var2 - (long)var5) * 52.1775D), ChronoUnit.i);
               if(var7.c(this) > var4) {
                  var1 = var7.c((long)var7.c(this.b.i), ChronoUnit.i);
               } else {
                  var1 = var7;
                  if(var7.c(this) < var4) {
                     var1 = var7.d(2L, ChronoUnit.i);
                  }

                  var7 = var1.d((long)(var6 - var1.c(this.b.i)), ChronoUnit.i);
                  var1 = var7;
                  if(var7.c(this) > var4) {
                     var1 = var7.c(1L, ChronoUnit.i);
                  }
               }
            } else {
               var1 = var1.d((long)(var4 - var5), this.c);
            }
         }

         return var1;
      }

      public TemporalAccessor a(Map var1, TemporalAccessor var2, ResolverStyle var3) {
         int var5 = this.b.a().a();
         int var4;
         long var7;
         ChronoLocalDate var11;
         if(this.d == ChronoUnit.i) {
            var7 = ((Long)var1.remove(this)).longValue();
            var4 = Jdk8Methods.d(this.e.b(var7, this) - 1 + (var5 - 1), 7);
            var1.put(ChronoField.p, Long.valueOf((long)(var4 + 1)));
            var11 = null;
         } else if(!var1.containsKey(ChronoField.p)) {
            var11 = null;
         } else {
            int var6;
            Chronology var12;
            ChronoLocalDate var13;
            if(this.d == ChronoUnit.p) {
               if(!var1.containsKey(this.b.i)) {
                  var11 = null;
               } else {
                  var12 = Chronology.a(var2);
                  var4 = Jdk8Methods.d(ChronoField.p.b(((Long)var1.get(ChronoField.p)).longValue()) - var5, 7) + 1;
                  var6 = this.a().b(((Long)var1.get(this)).longValue(), this);
                  if(var3 == ResolverStyle.c) {
                     var13 = var12.a(var6, 1, this.b.b());
                     var7 = ((Long)var1.get(this.b.i)).longValue();
                     var5 = this.a(var13, var5);
                     var7 = (var7 - this.c(var13, var5)) * 7L + (long)(var4 - var5);
                  } else {
                     var13 = var12.a(var6, 1, this.b.b());
                     var7 = (long)this.b.i.a().b(((Long)var1.get(this.b.i)).longValue(), this.b.i);
                     var5 = this.a(var13, var5);
                     var7 = (var7 - this.c(var13, var5)) * 7L + (long)(var4 - var5);
                  }

                  var13 = var13.f(var7, ChronoUnit.h);
                  if(var3 == ResolverStyle.a && var13.d(this) != ((Long)var1.get(this)).longValue()) {
                     throw new DateTimeException("Strict mode rejected date parsed to a different year");
                  }

                  var1.remove(this);
                  var1.remove(this.b.i);
                  var1.remove(ChronoField.p);
                  var11 = var13;
               }
            } else if(!var1.containsKey(ChronoField.A)) {
               var11 = null;
            } else {
               var4 = Jdk8Methods.d(ChronoField.p.b(((Long)var1.get(ChronoField.p)).longValue()) - var5, 7) + 1;
               var6 = ChronoField.A.b(((Long)var1.get(ChronoField.A)).longValue());
               var12 = Chronology.a(var2);
               if(this.d == ChronoUnit.j) {
                  if(!var1.containsKey(ChronoField.x)) {
                     var11 = null;
                  } else {
                     var7 = ((Long)var1.remove(this)).longValue();
                     long var9;
                     if(var3 == ResolverStyle.c) {
                        var9 = ((Long)var1.get(ChronoField.x)).longValue();
                        var13 = var12.a(var6, 1, 1).f(var9 - 1L, ChronoUnit.j);
                        var5 = this.a(var13, var5);
                        var9 = this.b(var13, var5);
                        var7 = (long)(var4 - var5) + (var7 - var9) * 7L;
                     } else {
                        var13 = var12.a(var6, ChronoField.x.b(((Long)var1.get(ChronoField.x)).longValue()), 8);
                        var5 = this.a(var13, var5);
                        var7 = (long)this.e.b(var7, this);
                        var9 = this.b(var13, var5);
                        var7 = (long)(var4 - var5) + (var7 - var9) * 7L;
                     }

                     var13 = var13.f(var7, ChronoUnit.h);
                     if(var3 == ResolverStyle.a && var13.d(ChronoField.x) != ((Long)var1.get(ChronoField.x)).longValue()) {
                        throw new DateTimeException("Strict mode rejected date parsed to a different month");
                     }

                     var1.remove(this);
                     var1.remove(ChronoField.A);
                     var1.remove(ChronoField.x);
                     var1.remove(ChronoField.p);
                     var11 = var13;
                  }
               } else {
                  if(this.d != ChronoUnit.k) {
                     throw new IllegalStateException("unreachable");
                  }

                  var7 = ((Long)var1.remove(this)).longValue();
                  var13 = var12.a(var6, 1, 1);
                  if(var3 == ResolverStyle.c) {
                     var5 = this.a(var13, var5);
                     var7 = (var7 - this.c(var13, var5)) * 7L + (long)(var4 - var5);
                  } else {
                     var5 = this.a(var13, var5);
                     var7 = ((long)this.e.b(var7, this) - this.c(var13, var5)) * 7L + (long)(var4 - var5);
                  }

                  var13 = var13.f(var7, ChronoUnit.h);
                  if(var3 == ResolverStyle.a && var13.d(ChronoField.A) != ((Long)var1.get(ChronoField.A)).longValue()) {
                     throw new DateTimeException("Strict mode rejected date parsed to a different year");
                  }

                  var1.remove(this);
                  var1.remove(ChronoField.A);
                  var1.remove(ChronoField.p);
                  var11 = var13;
               }
            }
         }

         return var11;
      }

      public ValueRange a() {
         return this.e;
      }

      public boolean a(TemporalAccessor var1) {
         boolean var2;
         if(var1.a((TemporalField)ChronoField.p)) {
            if(this.d == ChronoUnit.i) {
               var2 = true;
               return var2;
            }

            if(this.d == ChronoUnit.j) {
               var2 = var1.a((TemporalField)ChronoField.s);
               return var2;
            }

            if(this.d == ChronoUnit.k) {
               var2 = var1.a((TemporalField)ChronoField.t);
               return var2;
            }

            if(this.d == IsoFields.e) {
               var2 = var1.a((TemporalField)ChronoField.u);
               return var2;
            }

            if(this.d == ChronoUnit.p) {
               var2 = var1.a((TemporalField)ChronoField.u);
               return var2;
            }
         }

         var2 = false;
         return var2;
      }

      public ValueRange b(TemporalAccessor var1) {
         ValueRange var4;
         if(this.d == ChronoUnit.i) {
            var4 = this.e;
         } else {
            ChronoField var3;
            if(this.d == ChronoUnit.j) {
               var3 = ChronoField.s;
            } else {
               if(this.d != ChronoUnit.k) {
                  if(this.d == IsoFields.e) {
                     var4 = this.f(var1);
                  } else {
                     if(this.d != ChronoUnit.p) {
                        throw new IllegalStateException("unreachable");
                     }

                     var4 = var1.b(ChronoField.A);
                  }

                  return var4;
               }

               var3 = ChronoField.t;
            }

            int var2 = this.b.a().a();
            var2 = Jdk8Methods.d(var1.c(ChronoField.p) - var2, 7);
            var2 = this.a(var1.c(var3), var2 + 1);
            var4 = var1.b(var3);
            var4 = ValueRange.a((long)this.b(var2, (int)var4.b()), (long)this.b(var2, (int)var4.c()));
         }

         return var4;
      }

      public boolean b() {
         return true;
      }

      public long c(TemporalAccessor var1) {
         int var2 = this.b.a().a();
         var2 = Jdk8Methods.d(var1.c(ChronoField.p) - var2, 7) + 1;
         long var4;
         if(this.d == ChronoUnit.i) {
            var4 = (long)var2;
         } else {
            int var3;
            if(this.d == ChronoUnit.j) {
               var3 = var1.c(ChronoField.s);
               var4 = (long)this.b(this.a(var3, var2), var3);
            } else if(this.d == ChronoUnit.k) {
               var3 = var1.c(ChronoField.t);
               var4 = (long)this.b(this.a(var3, var2), var3);
            } else if(this.d == IsoFields.e) {
               var4 = (long)this.d(var1);
            } else {
               if(this.d != ChronoUnit.p) {
                  throw new IllegalStateException("unreachable");
               }

               var4 = (long)this.e(var1);
            }
         }

         return var4;
      }

      public boolean c() {
         return false;
      }

      public String toString() {
         return this.a + "[" + this.b.toString() + "]";
      }
   }
}
