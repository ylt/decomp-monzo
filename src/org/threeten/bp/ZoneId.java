package org.threeten.bp;

import java.io.DataOutput;
import java.io.IOException;
import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import org.threeten.bp.jdk8.Jdk8Methods;
import org.threeten.bp.temporal.TemporalAccessor;
import org.threeten.bp.temporal.TemporalQueries;
import org.threeten.bp.temporal.TemporalQuery;
import org.threeten.bp.zone.ZoneRules;
import org.threeten.bp.zone.ZoneRulesProvider;

public abstract class ZoneId implements Serializable {
   public static final TemporalQuery a = new TemporalQuery() {
      public ZoneId a(TemporalAccessor var1) {
         return ZoneId.a(var1);
      }

      // $FF: synthetic method
      public Object b(TemporalAccessor var1) {
         return this.a(var1);
      }
   };
   public static final Map b;

   static {
      HashMap var0 = new HashMap();
      var0.put("ACT", "Australia/Darwin");
      var0.put("AET", "Australia/Sydney");
      var0.put("AGT", "America/Argentina/Buenos_Aires");
      var0.put("ART", "Africa/Cairo");
      var0.put("AST", "America/Anchorage");
      var0.put("BET", "America/Sao_Paulo");
      var0.put("BST", "Asia/Dhaka");
      var0.put("CAT", "Africa/Harare");
      var0.put("CNT", "America/St_Johns");
      var0.put("CST", "America/Chicago");
      var0.put("CTT", "Asia/Shanghai");
      var0.put("EAT", "Africa/Addis_Ababa");
      var0.put("ECT", "Europe/Paris");
      var0.put("IET", "America/Indiana/Indianapolis");
      var0.put("IST", "Asia/Kolkata");
      var0.put("JST", "Asia/Tokyo");
      var0.put("MIT", "Pacific/Apia");
      var0.put("NET", "Asia/Yerevan");
      var0.put("NST", "Pacific/Auckland");
      var0.put("PLT", "Asia/Karachi");
      var0.put("PNT", "America/Phoenix");
      var0.put("PRT", "America/Puerto_Rico");
      var0.put("PST", "America/Los_Angeles");
      var0.put("SST", "Pacific/Guadalcanal");
      var0.put("VST", "Asia/Ho_Chi_Minh");
      var0.put("EST", "-05:00");
      var0.put("MST", "-07:00");
      var0.put("HST", "-10:00");
      b = Collections.unmodifiableMap(var0);
   }

   ZoneId() {
      if(this.getClass() != ZoneOffset.class && this.getClass() != ZoneRegion.class) {
         throw new AssertionError("Invalid subclass");
      }
   }

   public static ZoneId a() {
      return a(TimeZone.getDefault().getID(), b);
   }

   public static ZoneId a(String var0) {
      Jdk8Methods.a(var0, (String)"zoneId");
      Object var2;
      if(var0.equals("Z")) {
         var2 = ZoneOffset.d;
      } else {
         if(var0.length() == 1) {
            throw new DateTimeException("Invalid zone: " + var0);
         }

         if(!var0.startsWith("+") && !var0.startsWith("-")) {
            if(!var0.equals("UTC") && !var0.equals("GMT") && !var0.equals("UT")) {
               if(!var0.startsWith("UTC+") && !var0.startsWith("GMT+") && !var0.startsWith("UTC-") && !var0.startsWith("GMT-")) {
                  if(!var0.startsWith("UT+") && !var0.startsWith("UT-")) {
                     var2 = ZoneRegion.a(var0, true);
                  } else {
                     ZoneOffset var3 = ZoneOffset.b(var0.substring(2));
                     if(var3.f() == 0) {
                        var2 = new ZoneRegion("UT", var3.d());
                     } else {
                        var2 = new ZoneRegion("UT" + var3.c(), var3.d());
                     }
                  }
               } else {
                  ZoneOffset var1 = ZoneOffset.b(var0.substring(3));
                  if(var1.f() == 0) {
                     var2 = new ZoneRegion(var0.substring(0, 3), var1.d());
                  } else {
                     var2 = new ZoneRegion(var0.substring(0, 3) + var1.c(), var1.d());
                  }
               }
            } else {
               var2 = new ZoneRegion(var0, ZoneOffset.d.d());
            }
         } else {
            var2 = ZoneOffset.b(var0);
         }
      }

      return (ZoneId)var2;
   }

   public static ZoneId a(String var0, Map var1) {
      Jdk8Methods.a(var0, (String)"zoneId");
      Jdk8Methods.a(var1, (String)"aliasMap");
      String var2 = (String)var1.get(var0);
      if(var2 != null) {
         var0 = var2;
      }

      return a(var0);
   }

   public static ZoneId a(String var0, ZoneOffset var1) {
      Jdk8Methods.a(var0, (String)"prefix");
      Jdk8Methods.a(var1, "offset");
      if(var0.length() != 0) {
         if(!var0.equals("GMT") && !var0.equals("UTC") && !var0.equals("UT")) {
            throw new IllegalArgumentException("Invalid prefix, must be GMT, UTC or UT: " + var0);
         }

         if(((ZoneOffset)var1).f() == 0) {
            var1 = new ZoneRegion(var0, ((ZoneOffset)var1).d());
         } else {
            var1 = new ZoneRegion(var0 + ((ZoneOffset)var1).c(), ((ZoneOffset)var1).d());
         }
      }

      return (ZoneId)var1;
   }

   public static ZoneId a(TemporalAccessor var0) {
      ZoneId var1 = (ZoneId)var0.a(TemporalQueries.d());
      if(var1 == null) {
         throw new DateTimeException("Unable to obtain ZoneId from TemporalAccessor: " + var0 + ", type " + var0.getClass().getName());
      } else {
         return var1;
      }
   }

   public static Set b() {
      return ZoneRulesProvider.b();
   }

   abstract void a(DataOutput var1) throws IOException;

   public abstract String c();

   public abstract ZoneRules d();

   public ZoneId e() {
      // $FF: Couldn't be decompiled
   }

   public boolean equals(Object var1) {
      boolean var2;
      if(this == var1) {
         var2 = true;
      } else if(var1 instanceof ZoneId) {
         ZoneId var3 = (ZoneId)var1;
         var2 = this.c().equals(var3.c());
      } else {
         var2 = false;
      }

      return var2;
   }

   public int hashCode() {
      return this.c().hashCode();
   }

   public String toString() {
      return this.c();
   }
}
