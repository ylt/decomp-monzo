package org.threeten.bp;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.InvalidObjectException;
import java.io.ObjectStreamException;
import java.io.Serializable;
import org.threeten.bp.jdk8.DefaultInterfaceTemporalAccessor;
import org.threeten.bp.jdk8.Jdk8Methods;
import org.threeten.bp.temporal.ChronoField;
import org.threeten.bp.temporal.ChronoUnit;
import org.threeten.bp.temporal.Temporal;
import org.threeten.bp.temporal.TemporalAccessor;
import org.threeten.bp.temporal.TemporalAdjuster;
import org.threeten.bp.temporal.TemporalField;
import org.threeten.bp.temporal.TemporalQueries;
import org.threeten.bp.temporal.TemporalQuery;
import org.threeten.bp.temporal.TemporalUnit;
import org.threeten.bp.temporal.UnsupportedTemporalTypeException;
import org.threeten.bp.temporal.ValueRange;

public final class LocalTime extends DefaultInterfaceTemporalAccessor implements Serializable, Comparable, Temporal, TemporalAdjuster {
   public static final LocalTime a;
   public static final LocalTime b;
   public static final LocalTime c;
   public static final LocalTime d;
   public static final TemporalQuery e = new TemporalQuery() {
      public LocalTime a(TemporalAccessor var1) {
         return LocalTime.a(var1);
      }

      // $FF: synthetic method
      public Object b(TemporalAccessor var1) {
         return this.a(var1);
      }
   };
   private static final LocalTime[] f = new LocalTime[24];
   private final byte g;
   private final byte h;
   private final byte i;
   private final int j;

   static {
      for(int var0 = 0; var0 < f.length; ++var0) {
         f[var0] = new LocalTime(var0, 0, 0, 0);
      }

      c = f[0];
      d = f[12];
      a = f[0];
      b = new LocalTime(23, 59, 59, 999999999);
   }

   private LocalTime(int var1, int var2, int var3, int var4) {
      this.g = (byte)var1;
      this.h = (byte)var2;
      this.i = (byte)var3;
      this.j = var4;
   }

   public static LocalTime a(int var0, int var1) {
      ChronoField.m.a((long)var0);
      LocalTime var2;
      if(var1 == 0) {
         var2 = f[var0];
      } else {
         ChronoField.i.a((long)var1);
         var2 = new LocalTime(var0, var1, 0, 0);
      }

      return var2;
   }

   public static LocalTime a(int var0, int var1, int var2) {
      ChronoField.m.a((long)var0);
      LocalTime var3;
      if((var1 | var2) == 0) {
         var3 = f[var0];
      } else {
         ChronoField.i.a((long)var1);
         ChronoField.g.a((long)var2);
         var3 = new LocalTime(var0, var1, var2, 0);
      }

      return var3;
   }

   public static LocalTime a(int var0, int var1, int var2, int var3) {
      ChronoField.m.a((long)var0);
      ChronoField.i.a((long)var1);
      ChronoField.g.a((long)var2);
      ChronoField.a.a((long)var3);
      return b(var0, var1, var2, var3);
   }

   public static LocalTime a(long var0) {
      ChronoField.h.a(var0);
      int var3 = (int)(var0 / 3600L);
      var0 -= (long)(var3 * 3600);
      int var2 = (int)(var0 / 60L);
      return b(var3, var2, (int)(var0 - (long)(var2 * 60)), 0);
   }

   static LocalTime a(long var0, int var2) {
      ChronoField.h.a(var0);
      ChronoField.a.a((long)var2);
      int var3 = (int)(var0 / 3600L);
      var0 -= (long)(var3 * 3600);
      int var4 = (int)(var0 / 60L);
      return b(var3, var4, (int)(var0 - (long)(var4 * 60)), var2);
   }

   static LocalTime a(DataInput var0) throws IOException {
      int var3 = 0;
      int var4 = var0.readByte();
      int var1;
      int var2;
      if(var4 < 0) {
         var2 = 0;
         var4 = ~var4;
         var1 = 0;
      } else {
         var2 = var0.readByte();
         if(var2 < 0) {
            var2 = ~var2;
            var1 = 0;
         } else {
            var1 = var0.readByte();
            if(var1 < 0) {
               var1 = ~var1;
            } else {
               var3 = var0.readInt();
            }
         }
      }

      return a(var4, var2, var1, var3);
   }

   public static LocalTime a(TemporalAccessor var0) {
      LocalTime var1 = (LocalTime)var0.a(TemporalQueries.g());
      if(var1 == null) {
         throw new DateTimeException("Unable to obtain LocalTime from TemporalAccessor: " + var0 + ", type " + var0.getClass().getName());
      } else {
         return var1;
      }
   }

   private static LocalTime b(int var0, int var1, int var2, int var3) {
      LocalTime var4;
      if((var1 | var2 | var3) == 0) {
         var4 = f[var0];
      } else {
         var4 = new LocalTime(var0, var1, var2, var3);
      }

      return var4;
   }

   public static LocalTime b(long var0) {
      ChronoField.b.a(var0);
      int var3 = (int)(var0 / 3600000000000L);
      var0 -= (long)var3 * 3600000000000L;
      int var4 = (int)(var0 / 60000000000L);
      var0 -= (long)var4 * 60000000000L;
      int var2 = (int)(var0 / 1000000000L);
      return b(var3, var4, var2, (int)(var0 - (long)var2 * 1000000000L));
   }

   private int e(TemporalField var1) {
      int var2;
      switch(null.a[((ChronoField)var1).ordinal()]) {
      case 1:
         var2 = this.j;
         break;
      case 2:
         throw new DateTimeException("Field too large for an int: " + var1);
      case 3:
         var2 = this.j / 1000;
         break;
      case 4:
         throw new DateTimeException("Field too large for an int: " + var1);
      case 5:
         var2 = this.j / 1000000;
         break;
      case 6:
         var2 = (int)(this.f() / 1000000L);
         break;
      case 7:
         var2 = this.i;
         break;
      case 8:
         var2 = this.e();
         break;
      case 9:
         var2 = this.h;
         break;
      case 10:
         var2 = this.g * 60 + this.h;
         break;
      case 11:
         var2 = this.g % 12;
         break;
      case 12:
         int var3 = this.g % 12;
         var2 = var3;
         if(var3 % 12 == 0) {
            var2 = 12;
         }
         break;
      case 13:
         var2 = this.g;
         break;
      case 14:
         if(this.g == 0) {
            var2 = 24;
         } else {
            var2 = this.g;
         }
         break;
      case 15:
         var2 = this.g / 12;
         break;
      default:
         throw new UnsupportedTemporalTypeException("Unsupported field: " + var1);
      }

      return var2;
   }

   private Object readResolve() throws ObjectStreamException {
      throw new InvalidObjectException("Deserialization via serialization delegate");
   }

   private Object writeReplace() {
      return new Ser(5, this);
   }

   public int a() {
      return this.g;
   }

   public int a(LocalTime var1) {
      int var3 = Jdk8Methods.a(this.g, var1.g);
      int var2 = var3;
      if(var3 == 0) {
         var3 = Jdk8Methods.a(this.h, var1.h);
         var2 = var3;
         if(var3 == 0) {
            var3 = Jdk8Methods.a(this.i, var1.i);
            var2 = var3;
            if(var3 == 0) {
               var2 = Jdk8Methods.a(this.j, var1.j);
            }
         }
      }

      return var2;
   }

   public long a(Temporal var1, TemporalUnit var2) {
      LocalTime var5 = a((TemporalAccessor)var1);
      long var3;
      if(var2 instanceof ChronoUnit) {
         var3 = var5.f() - this.f();
         switch(null.b[((ChronoUnit)var2).ordinal()]) {
         case 1:
            break;
         case 2:
            var3 /= 1000L;
            break;
         case 3:
            var3 /= 1000000L;
            break;
         case 4:
            var3 /= 1000000000L;
            break;
         case 5:
            var3 /= 60000000000L;
            break;
         case 6:
            var3 /= 3600000000000L;
            break;
         case 7:
            var3 /= 43200000000000L;
            break;
         default:
            throw new UnsupportedTemporalTypeException("Unsupported unit: " + var2);
         }
      } else {
         var3 = var2.a(this, var5);
      }

      return var3;
   }

   public Object a(TemporalQuery var1) {
      Object var2;
      if(var1 == TemporalQueries.c()) {
         var2 = ChronoUnit.a;
      } else {
         var2 = this;
         if(var1 != TemporalQueries.g()) {
            if(var1 != TemporalQueries.b() && var1 != TemporalQueries.a() && var1 != TemporalQueries.d() && var1 != TemporalQueries.e() && var1 != TemporalQueries.f()) {
               var2 = var1.b(this);
            } else {
               var2 = null;
            }
         }
      }

      return var2;
   }

   public LocalTime a(int var1) {
      LocalTime var2;
      if(this.g == var1) {
         var2 = this;
      } else {
         ChronoField.m.a((long)var1);
         var2 = b(var1, this.h, this.i, this.j);
      }

      return var2;
   }

   public LocalTime a(long var1, TemporalUnit var3) {
      LocalTime var5;
      if(var3 instanceof ChronoUnit) {
         ChronoUnit var4 = (ChronoUnit)var3;
         switch(null.b[var4.ordinal()]) {
         case 1:
            var5 = this.f(var1);
            break;
         case 2:
            var5 = this.f(var1 % 86400000000L * 1000L);
            break;
         case 3:
            var5 = this.f(var1 % 86400000L * 1000000L);
            break;
         case 4:
            var5 = this.e(var1);
            break;
         case 5:
            var5 = this.d(var1);
            break;
         case 6:
            var5 = this.c(var1);
            break;
         case 7:
            var5 = this.c(var1 % 2L * 12L);
            break;
         default:
            throw new UnsupportedTemporalTypeException("Unsupported unit: " + var3);
         }
      } else {
         var5 = (LocalTime)var3.a(this, var1);
      }

      return var5;
   }

   public LocalTime a(TemporalAdjuster var1) {
      LocalTime var2;
      if(var1 instanceof LocalTime) {
         var2 = (LocalTime)var1;
      } else {
         var2 = (LocalTime)var1.a(this);
      }

      return var2;
   }

   public LocalTime a(TemporalField var1, long var2) {
      long var4 = 0L;
      LocalTime var7;
      if(var1 instanceof ChronoField) {
         ChronoField var6 = (ChronoField)var1;
         var6.a(var2);
         switch(null.a[var6.ordinal()]) {
         case 1:
            var7 = this.d((int)var2);
            break;
         case 2:
            var7 = b(var2);
            break;
         case 3:
            var7 = this.d((int)var2 * 1000);
            break;
         case 4:
            var7 = b(1000L * var2);
            break;
         case 5:
            var7 = this.d((int)var2 * 1000000);
            break;
         case 6:
            var7 = b(1000000L * var2);
            break;
         case 7:
            var7 = this.c((int)var2);
            break;
         case 8:
            var7 = this.e(var2 - (long)this.e());
            break;
         case 9:
            var7 = this.b((int)var2);
            break;
         case 10:
            var7 = this.d(var2 - (long)(this.g * 60 + this.h));
            break;
         case 11:
            var7 = this.c(var2 - (long)(this.g % 12));
            break;
         case 12:
            var4 = var2;
            if(var2 == 12L) {
               var4 = 0L;
            }

            var7 = this.c(var4 - (long)(this.g % 12));
            break;
         case 13:
            var7 = this.a((int)var2);
            break;
         case 14:
            if(var2 == 24L) {
               var2 = var4;
            }

            var7 = this.a((int)var2);
            break;
         case 15:
            var7 = this.c((var2 - (long)(this.g / 12)) * 12L);
            break;
         default:
            throw new UnsupportedTemporalTypeException("Unsupported field: " + var1);
         }
      } else {
         var7 = (LocalTime)var1.a(this, var2);
      }

      return var7;
   }

   public OffsetTime a(ZoneOffset var1) {
      return OffsetTime.a(this, var1);
   }

   public Temporal a(Temporal var1) {
      return var1.b(ChronoField.b, this.f());
   }

   void a(DataOutput var1) throws IOException {
      if(this.j == 0) {
         if(this.i == 0) {
            if(this.h == 0) {
               var1.writeByte(~this.g);
            } else {
               var1.writeByte(this.g);
               var1.writeByte(~this.h);
            }
         } else {
            var1.writeByte(this.g);
            var1.writeByte(this.h);
            var1.writeByte(~this.i);
         }
      } else {
         var1.writeByte(this.g);
         var1.writeByte(this.h);
         var1.writeByte(this.i);
         var1.writeInt(this.j);
      }

   }

   public boolean a(TemporalField var1) {
      boolean var2;
      if(var1 instanceof ChronoField) {
         var2 = var1.c();
      } else if(var1 != null && var1.a(this)) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   public int b() {
      return this.h;
   }

   public LocalTime b(int var1) {
      LocalTime var2;
      if(this.h == var1) {
         var2 = this;
      } else {
         ChronoField.i.a((long)var1);
         var2 = b(this.g, var1, this.i, this.j);
      }

      return var2;
   }

   public LocalTime b(long var1, TemporalUnit var3) {
      LocalTime var4;
      if(var1 == Long.MIN_VALUE) {
         var4 = this.a(Long.MAX_VALUE, var3).a(1L, var3);
      } else {
         var4 = this.a(-var1, var3);
      }

      return var4;
   }

   // $FF: synthetic method
   public Temporal b(TemporalAdjuster var1) {
      return this.a(var1);
   }

   // $FF: synthetic method
   public Temporal b(TemporalField var1, long var2) {
      return this.a(var1, var2);
   }

   public ValueRange b(TemporalField var1) {
      return super.b(var1);
   }

   public boolean b(LocalTime var1) {
      boolean var2;
      if(this.a(var1) > 0) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   public int c() {
      return this.i;
   }

   public int c(TemporalField var1) {
      int var2;
      if(var1 instanceof ChronoField) {
         var2 = this.e(var1);
      } else {
         var2 = super.c(var1);
      }

      return var2;
   }

   public LocalTime c(int var1) {
      LocalTime var2;
      if(this.i == var1) {
         var2 = this;
      } else {
         ChronoField.g.a((long)var1);
         var2 = b(this.g, this.h, var1, this.j);
      }

      return var2;
   }

   public LocalTime c(long var1) {
      LocalTime var3;
      if(var1 == 0L) {
         var3 = this;
      } else {
         var3 = b(((int)(var1 % 24L) + this.g + 24) % 24, this.h, this.i, this.j);
      }

      return var3;
   }

   // $FF: synthetic method
   public Temporal c(long var1, TemporalUnit var3) {
      return this.b(var1, var3);
   }

   public boolean c(LocalTime var1) {
      boolean var2;
      if(this.a(var1) < 0) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   // $FF: synthetic method
   public int compareTo(Object var1) {
      return this.a((LocalTime)var1);
   }

   public int d() {
      return this.j;
   }

   public long d(TemporalField var1) {
      long var2;
      if(var1 instanceof ChronoField) {
         if(var1 == ChronoField.b) {
            var2 = this.f();
         } else if(var1 == ChronoField.d) {
            var2 = this.f() / 1000L;
         } else {
            var2 = (long)this.e(var1);
         }
      } else {
         var2 = var1.c(this);
      }

      return var2;
   }

   public LocalTime d(int var1) {
      LocalTime var2;
      if(this.j == var1) {
         var2 = this;
      } else {
         ChronoField.a.a((long)var1);
         var2 = b(this.g, this.h, this.i, var1);
      }

      return var2;
   }

   public LocalTime d(long var1) {
      LocalTime var5;
      if(var1 == 0L) {
         var5 = this;
      } else {
         int var4 = this.g * 60 + this.h;
         int var3 = ((int)(var1 % 1440L) + var4 + 1440) % 1440;
         var5 = this;
         if(var4 != var3) {
            var5 = b(var3 / 60, var3 % 60, this.i, this.j);
         }
      }

      return var5;
   }

   // $FF: synthetic method
   public Temporal d(long var1, TemporalUnit var3) {
      return this.a(var1, var3);
   }

   public int e() {
      return this.g * 3600 + this.h * 60 + this.i;
   }

   public LocalTime e(long var1) {
      LocalTime var5;
      if(var1 == 0L) {
         var5 = this;
      } else {
         int var3 = this.g * 3600 + this.h * 60 + this.i;
         int var4 = ((int)(var1 % 86400L) + var3 + 86400) % 86400;
         var5 = this;
         if(var3 != var4) {
            var5 = b(var4 / 3600, var4 / 60 % 60, var4 % 60, this.j);
         }
      }

      return var5;
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(this != var1) {
         if(var1 instanceof LocalTime) {
            LocalTime var3 = (LocalTime)var1;
            if(this.g != var3.g || this.h != var3.h || this.i != var3.i || this.j != var3.j) {
               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public long f() {
      return (long)this.g * 3600000000000L + (long)this.h * 60000000000L + (long)this.i * 1000000000L + (long)this.j;
   }

   public LocalTime f(long var1) {
      LocalTime var5;
      if(var1 == 0L) {
         var5 = this;
      } else {
         long var3 = this.f();
         var1 = (var1 % 86400000000000L + var3 + 86400000000000L) % 86400000000000L;
         var5 = this;
         if(var3 != var1) {
            var5 = b((int)(var1 / 3600000000000L), (int)(var1 / 60000000000L % 60L), (int)(var1 / 1000000000L % 60L), (int)(var1 % 1000000000L));
         }
      }

      return var5;
   }

   public int hashCode() {
      long var1 = this.f();
      return (int)(var1 ^ var1 >>> 32);
   }

   public String toString() {
      StringBuilder var6 = new StringBuilder(18);
      byte var4 = this.g;
      byte var3 = this.h;
      byte var2 = this.i;
      int var1 = this.j;
      String var5;
      if(var4 < 10) {
         var5 = "0";
      } else {
         var5 = "";
      }

      StringBuilder var7 = var6.append(var5).append(var4);
      if(var3 < 10) {
         var5 = ":0";
      } else {
         var5 = ":";
      }

      var7.append(var5).append(var3);
      if(var2 > 0 || var1 > 0) {
         if(var2 < 10) {
            var5 = ":0";
         } else {
            var5 = ":";
         }

         var6.append(var5).append(var2);
         if(var1 > 0) {
            var6.append('.');
            if(var1 % 1000000 == 0) {
               var6.append(Integer.toString(var1 / 1000000 + 1000).substring(1));
            } else if(var1 % 1000 == 0) {
               var6.append(Integer.toString(var1 / 1000 + 1000000).substring(1));
            } else {
               var6.append(Integer.toString(1000000000 + var1).substring(1));
            }
         }
      }

      return var6.toString();
   }
}
