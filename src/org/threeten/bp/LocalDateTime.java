package org.threeten.bp;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.InvalidObjectException;
import java.io.ObjectStreamException;
import java.io.Serializable;
import org.threeten.bp.chrono.ChronoLocalDate;
import org.threeten.bp.chrono.ChronoLocalDateTime;
import org.threeten.bp.chrono.ChronoZonedDateTime;
import org.threeten.bp.format.DateTimeFormatter;
import org.threeten.bp.jdk8.Jdk8Methods;
import org.threeten.bp.temporal.ChronoField;
import org.threeten.bp.temporal.ChronoUnit;
import org.threeten.bp.temporal.Temporal;
import org.threeten.bp.temporal.TemporalAccessor;
import org.threeten.bp.temporal.TemporalAdjuster;
import org.threeten.bp.temporal.TemporalAmount;
import org.threeten.bp.temporal.TemporalField;
import org.threeten.bp.temporal.TemporalQueries;
import org.threeten.bp.temporal.TemporalQuery;
import org.threeten.bp.temporal.TemporalUnit;
import org.threeten.bp.temporal.UnsupportedTemporalTypeException;
import org.threeten.bp.temporal.ValueRange;

public final class LocalDateTime extends ChronoLocalDateTime implements Serializable, Temporal, TemporalAdjuster {
   public static final LocalDateTime a;
   public static final LocalDateTime b;
   public static final TemporalQuery c;
   private final LocalDate d;
   private final LocalTime e;

   static {
      a = a(LocalDate.a, LocalTime.a);
      b = a(LocalDate.b, LocalTime.b);
      c = new TemporalQuery() {
         public LocalDateTime a(TemporalAccessor var1) {
            return LocalDateTime.a(var1);
         }

         // $FF: synthetic method
         public Object b(TemporalAccessor var1) {
            return this.a(var1);
         }
      };
   }

   private LocalDateTime(LocalDate var1, LocalTime var2) {
      this.d = var1;
      this.e = var2;
   }

   private int a(LocalDateTime var1) {
      int var3 = this.d.b(var1.j());
      int var2 = var3;
      if(var3 == 0) {
         var2 = this.e.a(var1.k());
      }

      return var2;
   }

   public static LocalDateTime a() {
      return a(Clock.a());
   }

   public static LocalDateTime a(int var0, int var1, int var2, int var3, int var4, int var5, int var6) {
      return new LocalDateTime(LocalDate.a(var0, var1, var2), LocalTime.a(var3, var4, var5, var6));
   }

   public static LocalDateTime a(long var0, int var2, ZoneOffset var3) {
      Jdk8Methods.a(var3, (String)"offset");
      long var5 = (long)var3.f() + var0;
      var0 = Jdk8Methods.e(var5, 86400L);
      int var4 = Jdk8Methods.b(var5, 86400);
      return new LocalDateTime(LocalDate.a(var0), LocalTime.a((long)var4, var2));
   }

   static LocalDateTime a(DataInput var0) throws IOException {
      return a(LocalDate.a(var0), LocalTime.a(var0));
   }

   public static LocalDateTime a(CharSequence var0) {
      return a(var0, DateTimeFormatter.g);
   }

   public static LocalDateTime a(CharSequence var0, DateTimeFormatter var1) {
      Jdk8Methods.a(var1, (String)"formatter");
      return (LocalDateTime)var1.a(var0, c);
   }

   public static LocalDateTime a(Clock var0) {
      Jdk8Methods.a(var0, (String)"clock");
      Instant var1 = var0.d();
      ZoneOffset var2 = var0.b().d().a(var1);
      return a(var1.a(), var1.b(), var2);
   }

   public static LocalDateTime a(Instant var0, ZoneId var1) {
      Jdk8Methods.a(var0, (String)"instant");
      Jdk8Methods.a(var1, (String)"zone");
      ZoneOffset var2 = var1.d().a(var0);
      return a(var0.a(), var0.b(), var2);
   }

   private LocalDateTime a(LocalDate var1, long var2, long var4, long var6, long var8, int var10) {
      LocalDateTime var24;
      if((var2 | var4 | var6 | var8) == 0L) {
         var24 = this.b(var1, this.e);
      } else {
         long var19 = var8 / 86400000000000L;
         long var11 = var6 / 86400L;
         long var13 = var4 / 1440L;
         long var15 = var2 / 24L;
         long var17 = (long)var10;
         long var21 = this.e.f();
         var4 = (var8 % 86400000000000L + var6 % 86400L * 1000000000L + var4 % 1440L * 60000000000L + var2 % 24L * 3600000000000L) * (long)var10 + var21;
         var2 = Jdk8Methods.e(var4, 86400000000000L);
         var4 = Jdk8Methods.f(var4, 86400000000000L);
         LocalTime var23;
         if(var4 == var21) {
            var23 = this.e;
         } else {
            var23 = LocalTime.b(var4);
         }

         var24 = this.b(var1.e(var2 + (var19 + var11 + var13 + var15) * var17), var23);
      }

      return var24;
   }

   public static LocalDateTime a(LocalDate var0, LocalTime var1) {
      Jdk8Methods.a(var0, (String)"date");
      Jdk8Methods.a(var1, (String)"time");
      return new LocalDateTime(var0, var1);
   }

   public static LocalDateTime a(TemporalAccessor var0) {
      LocalDateTime var3;
      if(var0 instanceof LocalDateTime) {
         var3 = (LocalDateTime)var0;
      } else if(var0 instanceof ZonedDateTime) {
         var3 = ((ZonedDateTime)var0).d();
      } else {
         LocalDateTime var1;
         try {
            var1 = new LocalDateTime(LocalDate.a(var0), LocalTime.a(var0));
         } catch (DateTimeException var2) {
            throw new DateTimeException("Unable to obtain LocalDateTime from TemporalAccessor: " + var0 + ", type " + var0.getClass().getName());
         }

         var3 = var1;
      }

      return var3;
   }

   private LocalDateTime b(LocalDate var1, LocalTime var2) {
      LocalDateTime var3;
      if(this.d == var1 && this.e == var2) {
         var3 = this;
      } else {
         var3 = new LocalDateTime(var1, var2);
      }

      return var3;
   }

   private Object readResolve() throws ObjectStreamException {
      throw new InvalidObjectException("Deserialization via serialization delegate");
   }

   private Object writeReplace() {
      return new Ser(4, this);
   }

   public int a(ChronoLocalDateTime var1) {
      int var2;
      if(var1 instanceof LocalDateTime) {
         var2 = this.a((LocalDateTime)var1);
      } else {
         var2 = super.a(var1);
      }

      return var2;
   }

   public long a(Temporal var1, TemporalUnit var2) {
      LocalDateTime var12 = a((TemporalAccessor)var1);
      long var3;
      if(var2 instanceof ChronoUnit) {
         ChronoUnit var13 = (ChronoUnit)var2;
         if(var13.b()) {
            long var9 = this.d.a(var12.d);
            long var7 = var12.e.f() - this.e.f();
            long var5;
            if(var9 > 0L && var7 < 0L) {
               var5 = var9 - 1L;
               var3 = var7 + 86400000000000L;
            } else {
               var3 = var7;
               var5 = var9;
               if(var9 < 0L) {
                  var3 = var7;
                  var5 = var9;
                  if(var7 > 0L) {
                     var5 = var9 + 1L;
                     var3 = var7 - 86400000000000L;
                  }
               }
            }

            switch(null.a[var13.ordinal()]) {
            case 1:
               var3 = Jdk8Methods.b(Jdk8Methods.d(var5, 86400000000000L), var3);
               break;
            case 2:
               var3 = Jdk8Methods.b(Jdk8Methods.d(var5, 86400000000L), var3 / 1000L);
               break;
            case 3:
               var3 = Jdk8Methods.b(Jdk8Methods.d(var5, 86400000L), var3 / 1000000L);
               break;
            case 4:
               var3 = Jdk8Methods.b(Jdk8Methods.a(var5, 86400), var3 / 1000000000L);
               break;
            case 5:
               var3 = Jdk8Methods.b(Jdk8Methods.a(var5, 1440), var3 / 60000000000L);
               break;
            case 6:
               var3 = Jdk8Methods.b(Jdk8Methods.a(var5, 24), var3 / 3600000000000L);
               break;
            case 7:
               var3 = Jdk8Methods.b(Jdk8Methods.a(var5, 2), var3 / 43200000000000L);
               break;
            default:
               throw new UnsupportedTemporalTypeException("Unsupported unit: " + var2);
            }
         } else {
            LocalDate var11 = var12.d;
            LocalDate var14;
            if(var11.b((ChronoLocalDate)this.d) && var12.e.c(this.e)) {
               var14 = var11.g(1L);
            } else {
               var14 = var11;
               if(var11.c((ChronoLocalDate)this.d)) {
                  var14 = var11;
                  if(var12.e.b(this.e)) {
                     var14 = var11.e(1L);
                  }
               }
            }

            var3 = this.d.a((Temporal)var14, (TemporalUnit)var2);
         }
      } else {
         var3 = var2.a(this, var12);
      }

      return var3;
   }

   public Object a(TemporalQuery var1) {
      Object var2;
      if(var1 == TemporalQueries.f()) {
         var2 = this.j();
      } else {
         var2 = super.a(var1);
      }

      return var2;
   }

   public String a(DateTimeFormatter var1) {
      return super.a(var1);
   }

   public LocalDateTime a(int var1) {
      LocalTime var2 = this.e.c(var1);
      return this.b(this.d, var2);
   }

   public LocalDateTime a(long var1) {
      return this.b(this.d.e(var1), this.e);
   }

   public LocalDateTime a(long var1, TemporalUnit var3) {
      LocalDateTime var5;
      if(var3 instanceof ChronoUnit) {
         ChronoUnit var4 = (ChronoUnit)var3;
         switch(null.a[var4.ordinal()]) {
         case 1:
            var5 = this.e(var1);
            break;
         case 2:
            var5 = this.a(var1 / 86400000000L).e(var1 % 86400000000L * 1000L);
            break;
         case 3:
            var5 = this.a(var1 / 86400000L).e(var1 % 86400000L * 1000000L);
            break;
         case 4:
            var5 = this.d(var1);
            break;
         case 5:
            var5 = this.c(var1);
            break;
         case 6:
            var5 = this.b(var1);
            break;
         case 7:
            var5 = this.a(var1 / 256L).b(var1 % 256L * 12L);
            break;
         default:
            var5 = this.b(this.d.a(var1, var3), this.e);
         }
      } else {
         var5 = (LocalDateTime)var3.a(this, var1);
      }

      return var5;
   }

   public LocalDateTime a(TemporalAdjuster var1) {
      LocalDateTime var2;
      if(var1 instanceof LocalDate) {
         var2 = this.b((LocalDate)var1, this.e);
      } else if(var1 instanceof LocalTime) {
         var2 = this.b(this.d, (LocalTime)var1);
      } else if(var1 instanceof LocalDateTime) {
         var2 = (LocalDateTime)var1;
      } else {
         var2 = (LocalDateTime)var1.a(this);
      }

      return var2;
   }

   public LocalDateTime a(TemporalAmount var1) {
      return (LocalDateTime)var1.a(this);
   }

   public LocalDateTime a(TemporalField var1, long var2) {
      LocalDateTime var4;
      if(var1 instanceof ChronoField) {
         if(var1.c()) {
            var4 = this.b(this.d, this.e.a(var1, var2));
         } else {
            var4 = this.b(this.d.a(var1, var2), this.e);
         }
      } else {
         var4 = (LocalDateTime)var1.a(this, var2);
      }

      return var4;
   }

   public OffsetDateTime a(ZoneOffset var1) {
      return OffsetDateTime.a(this, var1);
   }

   public ZonedDateTime a(ZoneId var1) {
      return ZonedDateTime.a(this, var1);
   }

   public Temporal a(Temporal var1) {
      return super.a(var1);
   }

   void a(DataOutput var1) throws IOException {
      this.d.a(var1);
      this.e.a(var1);
   }

   public boolean a(TemporalField var1) {
      boolean var3 = true;
      boolean var2 = false;
      if(var1 instanceof ChronoField) {
         if(var1.b() || var1.c()) {
            var2 = true;
         }
      } else if(var1 != null && var1.a(this)) {
         var2 = var3;
      } else {
         var2 = false;
      }

      return var2;
   }

   public int b() {
      return this.d.d();
   }

   public LocalDateTime b(int var1) {
      LocalTime var2 = this.e.d(var1);
      return this.b(this.d, var2);
   }

   public LocalDateTime b(long var1) {
      return this.a(this.d, var1, 0L, 0L, 0L, 1);
   }

   public LocalDateTime b(long var1, TemporalUnit var3) {
      LocalDateTime var4;
      if(var1 == Long.MIN_VALUE) {
         var4 = this.a(Long.MAX_VALUE, var3).a(1L, var3);
      } else {
         var4 = this.a(-var1, var3);
      }

      return var4;
   }

   // $FF: synthetic method
   public ChronoLocalDateTime b(TemporalAmount var1) {
      return this.a(var1);
   }

   // $FF: synthetic method
   public ChronoZonedDateTime b(ZoneId var1) {
      return this.a(var1);
   }

   // $FF: synthetic method
   public Temporal b(TemporalAdjuster var1) {
      return this.a(var1);
   }

   // $FF: synthetic method
   public Temporal b(TemporalField var1, long var2) {
      return this.a(var1, var2);
   }

   public ValueRange b(TemporalField var1) {
      ValueRange var2;
      if(var1 instanceof ChronoField) {
         if(var1.c()) {
            var2 = this.e.b(var1);
         } else {
            var2 = this.d.b(var1);
         }
      } else {
         var2 = var1.b(this);
      }

      return var2;
   }

   public boolean b(ChronoLocalDateTime var1) {
      boolean var2;
      if(var1 instanceof LocalDateTime) {
         if(this.a((LocalDateTime)var1) > 0) {
            var2 = true;
         } else {
            var2 = false;
         }
      } else {
         var2 = super.b(var1);
      }

      return var2;
   }

   public int c() {
      return this.d.e();
   }

   public int c(TemporalField var1) {
      int var2;
      if(var1 instanceof ChronoField) {
         if(var1.c()) {
            var2 = this.e.c(var1);
         } else {
            var2 = this.d.c(var1);
         }
      } else {
         var2 = super.c((TemporalField)var1);
      }

      return var2;
   }

   public LocalDateTime c(long var1) {
      return this.a(this.d, 0L, var1, 0L, 0L, 1);
   }

   // $FF: synthetic method
   public ChronoLocalDateTime c(TemporalAdjuster var1) {
      return this.a(var1);
   }

   // $FF: synthetic method
   public ChronoLocalDateTime c(TemporalField var1, long var2) {
      return this.a(var1, var2);
   }

   // $FF: synthetic method
   public Temporal c(long var1, TemporalUnit var3) {
      return this.b(var1, var3);
   }

   // $FF: synthetic method
   public Temporal c(TemporalAmount var1) {
      return this.a(var1);
   }

   public boolean c(ChronoLocalDateTime var1) {
      boolean var2;
      if(var1 instanceof LocalDateTime) {
         if(this.a((LocalDateTime)var1) < 0) {
            var2 = true;
         } else {
            var2 = false;
         }
      } else {
         var2 = super.c(var1);
      }

      return var2;
   }

   // $FF: synthetic method
   public int compareTo(Object var1) {
      return this.a((ChronoLocalDateTime)var1);
   }

   public long d(TemporalField var1) {
      long var2;
      if(var1 instanceof ChronoField) {
         if(var1.c()) {
            var2 = this.e.d(var1);
         } else {
            var2 = this.d.d(var1);
         }
      } else {
         var2 = var1.c(this);
      }

      return var2;
   }

   public LocalDateTime d(long var1) {
      return this.a(this.d, 0L, 0L, var1, 0L, 1);
   }

   public Month d() {
      return this.d.f();
   }

   // $FF: synthetic method
   public Temporal d(long var1, TemporalUnit var3) {
      return this.a(var1, var3);
   }

   public int e() {
      return this.d.g();
   }

   public LocalDateTime e(long var1) {
      return this.a(this.d, 0L, 0L, 0L, var1, 1);
   }

   // $FF: synthetic method
   public ChronoLocalDateTime e(long var1, TemporalUnit var3) {
      return this.b(var1, var3);
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(this != var1) {
         if(var1 instanceof LocalDateTime) {
            LocalDateTime var3 = (LocalDateTime)var1;
            if(!this.d.equals(var3.d) || !this.e.equals(var3.e)) {
               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public int f() {
      return this.e.a();
   }

   // $FF: synthetic method
   public ChronoLocalDateTime f(long var1, TemporalUnit var3) {
      return this.a(var1, var3);
   }

   public int g() {
      return this.e.b();
   }

   public int h() {
      return this.e.c();
   }

   public int hashCode() {
      return this.d.hashCode() ^ this.e.hashCode();
   }

   public int i() {
      return this.e.d();
   }

   public LocalDate j() {
      return this.d;
   }

   public LocalTime k() {
      return this.e;
   }

   // $FF: synthetic method
   public ChronoLocalDate l() {
      return this.j();
   }

   public String toString() {
      return this.d.toString() + 'T' + this.e.toString();
   }
}
