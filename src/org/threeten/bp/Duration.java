package org.threeten.bp;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.InvalidObjectException;
import java.io.ObjectStreamException;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.regex.Pattern;
import org.threeten.bp.jdk8.Jdk8Methods;
import org.threeten.bp.temporal.ChronoField;
import org.threeten.bp.temporal.ChronoUnit;
import org.threeten.bp.temporal.Temporal;
import org.threeten.bp.temporal.TemporalAmount;

public final class Duration implements Serializable, Comparable, TemporalAmount {
   public static final Duration a = new Duration(0L, 0);
   private static final BigInteger b = BigInteger.valueOf(1000000000L);
   private static final Pattern c = Pattern.compile("([-+]?)P(?:([-+]?[0-9]+)D)?(T(?:([-+]?[0-9]+)H)?(?:([-+]?[0-9]+)M)?(?:([-+]?[0-9]+)(?:[.,]([0-9]{0,9}))?S)?)?", 2);
   private final long d;
   private final int e;

   private Duration(long var1, int var3) {
      this.d = var1;
      this.e = var3;
   }

   public static Duration a(long var0) {
      return a(var0, 0);
   }

   private static Duration a(long var0, int var2) {
      Duration var3;
      if(((long)var2 | var0) == 0L) {
         var3 = a;
      } else {
         var3 = new Duration(var0, var2);
      }

      return var3;
   }

   public static Duration a(long var0, long var2) {
      return a(Jdk8Methods.b(var0, Jdk8Methods.e(var2, 1000000000L)), Jdk8Methods.b(var2, 1000000000));
   }

   static Duration a(DataInput var0) throws IOException {
      return a(var0.readLong(), (long)var0.readInt());
   }

   public static Duration a(Temporal var0, Temporal var1) {
      long var6 = var0.a(var1, ChronoUnit.d);
      long var2;
      long var4;
      if(var0.a(ChronoField.a) && var1.a(ChronoField.a)) {
         long var10;
         try {
            var10 = var0.d(ChronoField.a);
            var2 = var1.d(ChronoField.a);
         } catch (DateTimeException var14) {
            var2 = 0L;
            var4 = var6;
            return a(var4, var2);
         } catch (ArithmeticException var15) {
            var2 = 0L;
            var4 = var6;
            return a(var4, var2);
         }

         long var8 = var2 - var10;
         if(var6 > 0L && var8 < 0L) {
            var2 = var8 + 1000000000L;
            var4 = var6;
         } else if(var6 < 0L && var8 > 0L) {
            var2 = var8 - 1000000000L;
            var4 = var6;
         } else {
            var2 = var8;
            var4 = var6;
            if(var6 == 0L) {
               var2 = var8;
               var4 = var6;
               if(var8 != 0L) {
                  try {
                     var4 = var0.a(var1.b(ChronoField.a, var10), ChronoUnit.d);
                  } catch (DateTimeException var12) {
                     var2 = var8;
                     var4 = var6;
                     return a(var4, var2);
                  } catch (ArithmeticException var13) {
                     var2 = var8;
                     var4 = var6;
                     return a(var4, var2);
                  }

                  var2 = var8;
               }
            }
         }
      } else {
         var2 = 0L;
         var4 = var6;
      }

      return a(var4, var2);
   }

   public static Duration b(long var0) {
      long var4 = var0 / 1000000000L;
      int var3 = (int)(var0 % 1000000000L);
      int var2 = var3;
      var0 = var4;
      if(var3 < 0) {
         var2 = var3 + 1000000000;
         var0 = var4 - 1L;
      }

      return a(var0, var2);
   }

   private Object readResolve() throws ObjectStreamException {
      throw new InvalidObjectException("Deserialization via serialization delegate");
   }

   private Object writeReplace() {
      return new Ser(1, this);
   }

   public int a(Duration var1) {
      int var2 = Jdk8Methods.a(this.d, var1.d);
      if(var2 == 0) {
         var2 = this.e - var1.e;
      }

      return var2;
   }

   public long a() {
      return this.d;
   }

   public Temporal a(Temporal var1) {
      Temporal var2 = var1;
      if(this.d != 0L) {
         var2 = var1.d(this.d, ChronoUnit.d);
      }

      var1 = var2;
      if(this.e != 0) {
         var1 = var2.d((long)this.e, ChronoUnit.a);
      }

      return var1;
   }

   void a(DataOutput var1) throws IOException {
      var1.writeLong(this.d);
      var1.writeInt(this.e);
   }

   public long b() {
      return this.d / 60L;
   }

   // $FF: synthetic method
   public int compareTo(Object var1) {
      return this.a((Duration)var1);
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(this != var1) {
         if(var1 instanceof Duration) {
            Duration var3 = (Duration)var1;
            if(this.d != var3.d || this.e != var3.e) {
               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public int hashCode() {
      return (int)(this.d ^ this.d >>> 32) + this.e * 51;
   }

   public String toString() {
      String var5;
      if(this == a) {
         var5 = "PT0S";
      } else {
         long var3 = this.d / 3600L;
         int var2 = (int)(this.d % 3600L / 60L);
         int var1 = (int)(this.d % 60L);
         StringBuilder var6 = new StringBuilder(24);
         var6.append("PT");
         if(var3 != 0L) {
            var6.append(var3).append('H');
         }

         if(var2 != 0) {
            var6.append(var2).append('M');
         }

         if(var1 == 0 && this.e == 0 && var6.length() > 2) {
            var5 = var6.toString();
         } else {
            if(var1 < 0 && this.e > 0) {
               if(var1 == -1) {
                  var6.append("-0");
               } else {
                  var6.append(var1 + 1);
               }
            } else {
               var6.append(var1);
            }

            if(this.e > 0) {
               var2 = var6.length();
               if(var1 < 0) {
                  var6.append(2000000000 - this.e);
               } else {
                  var6.append(this.e + 1000000000);
               }

               while(var6.charAt(var6.length() - 1) == 48) {
                  var6.setLength(var6.length() - 1);
               }

               var6.setCharAt(var2, '.');
            }

            var6.append('S');
            var5 = var6.toString();
         }
      }

      return var5;
   }
}
