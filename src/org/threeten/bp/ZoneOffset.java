package org.threeten.bp;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.InvalidObjectException;
import java.io.ObjectStreamException;
import java.io.Serializable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import org.threeten.bp.jdk8.Jdk8Methods;
import org.threeten.bp.temporal.ChronoField;
import org.threeten.bp.temporal.Temporal;
import org.threeten.bp.temporal.TemporalAccessor;
import org.threeten.bp.temporal.TemporalAdjuster;
import org.threeten.bp.temporal.TemporalField;
import org.threeten.bp.temporal.TemporalQueries;
import org.threeten.bp.temporal.TemporalQuery;
import org.threeten.bp.temporal.UnsupportedTemporalTypeException;
import org.threeten.bp.temporal.ValueRange;
import org.threeten.bp.zone.ZoneRules;

public final class ZoneOffset extends ZoneId implements Serializable, Comparable, TemporalAccessor, TemporalAdjuster {
   public static final TemporalQuery c = new TemporalQuery() {
      public ZoneOffset a(TemporalAccessor var1) {
         return ZoneOffset.b(var1);
      }

      // $FF: synthetic method
      public Object b(TemporalAccessor var1) {
         return this.a(var1);
      }
   };
   public static final ZoneOffset d = a(0);
   public static final ZoneOffset e = a(-64800);
   public static final ZoneOffset f = a('ﴠ');
   private static final ConcurrentMap g = new ConcurrentHashMap(16, 0.75F, 4);
   private static final ConcurrentMap h = new ConcurrentHashMap(16, 0.75F, 4);
   private final int i;
   private final transient String j;

   private ZoneOffset(int var1) {
      this.i = var1;
      this.j = b(var1);
   }

   private static int a(CharSequence var0, int var1, boolean var2) {
      if(var2 && var0.charAt(var1 - 1) != 58) {
         throw new DateTimeException("Invalid ID for ZoneOffset, colon not found when expected: " + var0);
      } else {
         char var3 = var0.charAt(var1);
         char var4 = var0.charAt(var1 + 1);
         if(var3 >= 48 && var3 <= 57 && var4 >= 48 && var4 <= 57) {
            return (var3 - 48) * 10 + (var4 - 48);
         } else {
            throw new DateTimeException("Invalid ID for ZoneOffset, non numeric characters found: " + var0);
         }
      }
   }

   public static ZoneOffset a(int var0) {
      if(Math.abs(var0) > 'ﴠ') {
         throw new DateTimeException("Zone offset not in valid range: -18:00 to +18:00");
      } else {
         ZoneOffset var1;
         if(var0 % 900 == 0) {
            Integer var3 = Integer.valueOf(var0);
            ZoneOffset var2 = (ZoneOffset)g.get(var3);
            var1 = var2;
            if(var2 == null) {
               var1 = new ZoneOffset(var0);
               g.putIfAbsent(var3, var1);
               var1 = (ZoneOffset)g.get(var3);
               h.putIfAbsent(var1.c(), var1);
            }
         } else {
            var1 = new ZoneOffset(var0);
         }

         return var1;
      }
   }

   public static ZoneOffset a(int var0, int var1, int var2) {
      b(var0, var1, var2);
      return a(c(var0, var1, var2));
   }

   static ZoneOffset a(DataInput var0) throws IOException {
      byte var1 = var0.readByte();
      ZoneOffset var2;
      if(var1 == 127) {
         var2 = a(var0.readInt());
      } else {
         var2 = a(var1 * 900);
      }

      return var2;
   }

   private static String b(int var0) {
      String var4;
      if(var0 == 0) {
         var4 = "Z";
      } else {
         int var1 = Math.abs(var0);
         StringBuilder var5 = new StringBuilder();
         int var2 = var1 / 3600;
         int var3 = var1 / 60 % 60;
         if(var0 < 0) {
            var4 = "-";
         } else {
            var4 = "+";
         }

         StringBuilder var6 = var5.append(var4);
         if(var2 < 10) {
            var4 = "0";
         } else {
            var4 = "";
         }

         var6 = var6.append(var4).append(var2);
         if(var3 < 10) {
            var4 = ":0";
         } else {
            var4 = ":";
         }

         var6.append(var4).append(var3);
         var0 = var1 % 60;
         if(var0 != 0) {
            if(var0 < 10) {
               var4 = ":0";
            } else {
               var4 = ":";
            }

            var5.append(var4).append(var0);
         }

         var4 = var5.toString();
      }

      return var4;
   }

   public static ZoneOffset b(String var0) {
      Jdk8Methods.a(var0, (String)"offsetId");
      ZoneOffset var5 = (ZoneOffset)h.get(var0);
      ZoneOffset var6;
      if(var5 != null) {
         var6 = var5;
      } else {
         String var7 = var0;
         int var1;
         int var2;
         int var3;
         switch(var0.length()) {
         case 2:
            var7 = var0.charAt(0) + "0" + var0.charAt(1);
         case 3:
            var3 = a(var7, 1, false);
            var2 = 0;
            var1 = 0;
            var0 = var7;
            break;
         case 4:
         case 8:
         default:
            throw new DateTimeException("Invalid ID for ZoneOffset, invalid format: " + var0);
         case 5:
            var3 = a(var0, 1, false);
            var2 = a(var0, 3, false);
            var1 = 0;
            break;
         case 6:
            var3 = a(var0, 1, false);
            var2 = a(var0, 4, true);
            var1 = 0;
            break;
         case 7:
            var3 = a(var0, 1, false);
            var2 = a(var0, 3, false);
            var1 = a(var0, 5, false);
            break;
         case 9:
            var3 = a(var0, 1, false);
            var2 = a(var0, 4, true);
            var1 = a(var0, 7, true);
         }

         char var4 = var0.charAt(0);
         if(var4 != 43 && var4 != 45) {
            throw new DateTimeException("Invalid ID for ZoneOffset, plus/minus not found when expected: " + var0);
         }

         if(var4 == 45) {
            var6 = a(-var3, -var2, -var1);
         } else {
            var6 = a(var3, var2, var1);
         }
      }

      return var6;
   }

   public static ZoneOffset b(TemporalAccessor var0) {
      ZoneOffset var1 = (ZoneOffset)var0.a(TemporalQueries.e());
      if(var1 == null) {
         throw new DateTimeException("Unable to obtain ZoneOffset from TemporalAccessor: " + var0 + ", type " + var0.getClass().getName());
      } else {
         return var1;
      }
   }

   private static void b(int var0, int var1, int var2) {
      if(var0 >= -18 && var0 <= 18) {
         if(var0 > 0) {
            if(var1 < 0 || var2 < 0) {
               throw new DateTimeException("Zone offset minutes and seconds must be positive because hours is positive");
            }
         } else if(var0 < 0) {
            if(var1 > 0 || var2 > 0) {
               throw new DateTimeException("Zone offset minutes and seconds must be negative because hours is negative");
            }
         } else if(var1 > 0 && var2 < 0 || var1 < 0 && var2 > 0) {
            throw new DateTimeException("Zone offset minutes and seconds must have the same sign");
         }

         if(Math.abs(var1) > 59) {
            throw new DateTimeException("Zone offset minutes not in valid range: abs(value) " + Math.abs(var1) + " is not in the range 0 to 59");
         } else if(Math.abs(var2) > 59) {
            throw new DateTimeException("Zone offset seconds not in valid range: abs(value) " + Math.abs(var2) + " is not in the range 0 to 59");
         } else if(Math.abs(var0) == 18 && (Math.abs(var1) > 0 || Math.abs(var2) > 0)) {
            throw new DateTimeException("Zone offset not in valid range: -18:00 to +18:00");
         }
      } else {
         throw new DateTimeException("Zone offset hours not in valid range: value " + var0 + " is not in the range -18 to 18");
      }
   }

   private static int c(int var0, int var1, int var2) {
      return var0 * 3600 + var1 * 60 + var2;
   }

   private Object readResolve() throws ObjectStreamException {
      throw new InvalidObjectException("Deserialization via serialization delegate");
   }

   private Object writeReplace() {
      return new Ser(8, this);
   }

   public int a(ZoneOffset var1) {
      return var1.i - this.i;
   }

   public Object a(TemporalQuery var1) {
      Object var2 = this;
      if(var1 != TemporalQueries.e()) {
         if(var1 == TemporalQueries.d()) {
            var2 = this;
         } else if(var1 != TemporalQueries.f() && var1 != TemporalQueries.g() && var1 != TemporalQueries.c() && var1 != TemporalQueries.b() && var1 != TemporalQueries.a()) {
            var2 = var1.b(this);
         } else {
            var2 = null;
         }
      }

      return var2;
   }

   public Temporal a(Temporal var1) {
      return var1.b(ChronoField.D, (long)this.i);
   }

   void a(DataOutput var1) throws IOException {
      var1.writeByte(8);
      this.b(var1);
   }

   public boolean a(TemporalField var1) {
      boolean var2 = true;
      if(var1 instanceof ChronoField) {
         if(var1 != ChronoField.D) {
            var2 = false;
         }
      } else if(var1 == null || !var1.a(this)) {
         var2 = false;
      }

      return var2;
   }

   public ValueRange b(TemporalField var1) {
      ValueRange var2;
      if(var1 == ChronoField.D) {
         var2 = var1.a();
      } else {
         if(var1 instanceof ChronoField) {
            throw new UnsupportedTemporalTypeException("Unsupported field: " + var1);
         }

         var2 = var1.b(this);
      }

      return var2;
   }

   void b(DataOutput var1) throws IOException {
      int var3 = this.i;
      int var2;
      if(var3 % 900 == 0) {
         var2 = var3 / 900;
      } else {
         var2 = 127;
      }

      var1.writeByte(var2);
      if(var2 == 127) {
         var1.writeInt(var3);
      }

   }

   public int c(TemporalField var1) {
      int var2;
      if(var1 == ChronoField.D) {
         var2 = this.i;
      } else {
         if(var1 instanceof ChronoField) {
            throw new UnsupportedTemporalTypeException("Unsupported field: " + var1);
         }

         var2 = this.b(var1).b(this.d(var1), var1);
      }

      return var2;
   }

   public String c() {
      return this.j;
   }

   // $FF: synthetic method
   public int compareTo(Object var1) {
      return this.a((ZoneOffset)var1);
   }

   public long d(TemporalField var1) {
      long var2;
      if(var1 == ChronoField.D) {
         var2 = (long)this.i;
      } else {
         if(var1 instanceof ChronoField) {
            throw new DateTimeException("Unsupported field: " + var1);
         }

         var2 = var1.c(this);
      }

      return var2;
   }

   public ZoneRules d() {
      return ZoneRules.a(this);
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(this != var1) {
         if(var1 instanceof ZoneOffset) {
            if(this.i != ((ZoneOffset)var1).i) {
               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public int f() {
      return this.i;
   }

   public int hashCode() {
      return this.i;
   }

   public String toString() {
      return this.j;
   }
}
