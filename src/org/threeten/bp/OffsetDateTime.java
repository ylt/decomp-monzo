package org.threeten.bp;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.InvalidObjectException;
import java.io.ObjectStreamException;
import java.io.Serializable;
import java.util.Comparator;
import org.threeten.bp.chrono.ChronoLocalDateTime;
import org.threeten.bp.chrono.IsoChronology;
import org.threeten.bp.jdk8.DefaultInterfaceTemporal;
import org.threeten.bp.jdk8.Jdk8Methods;
import org.threeten.bp.temporal.ChronoField;
import org.threeten.bp.temporal.ChronoUnit;
import org.threeten.bp.temporal.Temporal;
import org.threeten.bp.temporal.TemporalAccessor;
import org.threeten.bp.temporal.TemporalAdjuster;
import org.threeten.bp.temporal.TemporalAmount;
import org.threeten.bp.temporal.TemporalField;
import org.threeten.bp.temporal.TemporalQueries;
import org.threeten.bp.temporal.TemporalQuery;
import org.threeten.bp.temporal.TemporalUnit;
import org.threeten.bp.temporal.ValueRange;

public final class OffsetDateTime extends DefaultInterfaceTemporal implements Serializable, Comparable, Temporal, TemporalAdjuster {
   public static final OffsetDateTime a;
   public static final OffsetDateTime b;
   public static final TemporalQuery c;
   private static final Comparator d;
   private final LocalDateTime e;
   private final ZoneOffset f;

   static {
      a = LocalDateTime.a.a(ZoneOffset.f);
      b = LocalDateTime.b.a(ZoneOffset.e);
      c = new TemporalQuery() {
         public OffsetDateTime a(TemporalAccessor var1) {
            return OffsetDateTime.a(var1);
         }

         // $FF: synthetic method
         public Object b(TemporalAccessor var1) {
            return this.a(var1);
         }
      };
      d = new Comparator() {
         public int a(OffsetDateTime var1, OffsetDateTime var2) {
            int var4 = Jdk8Methods.a(var1.f(), var2.f());
            int var3 = var4;
            if(var4 == 0) {
               var3 = Jdk8Methods.a((long)var1.b(), (long)var2.b());
            }

            return var3;
         }

         // $FF: synthetic method
         public int compare(Object var1, Object var2) {
            return this.a((OffsetDateTime)var1, (OffsetDateTime)var2);
         }
      };
   }

   private OffsetDateTime(LocalDateTime var1, ZoneOffset var2) {
      this.e = (LocalDateTime)Jdk8Methods.a(var1, (String)"dateTime");
      this.f = (ZoneOffset)Jdk8Methods.a(var2, (String)"offset");
   }

   static OffsetDateTime a(DataInput var0) throws IOException {
      return a(LocalDateTime.a(var0), ZoneOffset.a(var0));
   }

   public static OffsetDateTime a(Instant var0, ZoneId var1) {
      Jdk8Methods.a(var0, (String)"instant");
      Jdk8Methods.a(var1, (String)"zone");
      ZoneOffset var2 = var1.d().a(var0);
      return new OffsetDateTime(LocalDateTime.a(var0.a(), var0.b(), var2), var2);
   }

   public static OffsetDateTime a(LocalDateTime var0, ZoneOffset var1) {
      return new OffsetDateTime(var0, var1);
   }

   public static OffsetDateTime a(TemporalAccessor param0) {
      // $FF: Couldn't be decompiled
   }

   private OffsetDateTime b(LocalDateTime var1, ZoneOffset var2) {
      OffsetDateTime var3;
      if(this.e == var1 && this.f.equals(var2)) {
         var3 = this;
      } else {
         var3 = new OffsetDateTime(var1, var2);
      }

      return var3;
   }

   private Object readResolve() throws ObjectStreamException {
      throw new InvalidObjectException("Deserialization via serialization delegate");
   }

   private Object writeReplace() {
      return new Ser(69, this);
   }

   public int a(OffsetDateTime var1) {
      int var2;
      if(this.a().equals(var1.a())) {
         var2 = this.c().a((ChronoLocalDateTime)var1.c());
      } else {
         int var3 = Jdk8Methods.a(this.f(), var1.f());
         var2 = var3;
         if(var3 == 0) {
            var3 = this.e().d() - var1.e().d();
            var2 = var3;
            if(var3 == 0) {
               var2 = this.c().a((ChronoLocalDateTime)var1.c());
            }
         }
      }

      return var2;
   }

   public long a(Temporal var1, TemporalUnit var2) {
      OffsetDateTime var5 = a((TemporalAccessor)var1);
      long var3;
      if(var2 instanceof ChronoUnit) {
         var5 = var5.a(this.f);
         var3 = this.e.a((Temporal)var5.e, (TemporalUnit)var2);
      } else {
         var3 = var2.a(this, var5);
      }

      return var3;
   }

   public Object a(TemporalQuery var1) {
      Object var2;
      if(var1 == TemporalQueries.b()) {
         var2 = IsoChronology.b;
      } else if(var1 == TemporalQueries.c()) {
         var2 = ChronoUnit.a;
      } else if(var1 != TemporalQueries.e() && var1 != TemporalQueries.d()) {
         if(var1 == TemporalQueries.f()) {
            var2 = this.d();
         } else if(var1 == TemporalQueries.g()) {
            var2 = this.e();
         } else if(var1 == TemporalQueries.a()) {
            var2 = null;
         } else {
            var2 = super.a(var1);
         }
      } else {
         var2 = this.a();
      }

      return var2;
   }

   public OffsetDateTime a(long var1, TemporalUnit var3) {
      OffsetDateTime var4;
      if(var3 instanceof ChronoUnit) {
         var4 = this.b(this.e.a(var1, var3), this.f);
      } else {
         var4 = (OffsetDateTime)var3.a(this, var1);
      }

      return var4;
   }

   public OffsetDateTime a(ZoneOffset var1) {
      OffsetDateTime var4;
      if(var1.equals(this.f)) {
         var4 = this;
      } else {
         int var3 = var1.f();
         int var2 = this.f.f();
         var4 = new OffsetDateTime(this.e.d((long)(var3 - var2)), var1);
      }

      return var4;
   }

   public OffsetDateTime a(TemporalAdjuster var1) {
      OffsetDateTime var2;
      if(!(var1 instanceof LocalDate) && !(var1 instanceof LocalTime) && !(var1 instanceof LocalDateTime)) {
         if(var1 instanceof Instant) {
            var2 = a((Instant)((Instant)var1), (ZoneId)this.f);
         } else if(var1 instanceof ZoneOffset) {
            var2 = this.b(this.e, (ZoneOffset)var1);
         } else if(var1 instanceof OffsetDateTime) {
            var2 = (OffsetDateTime)var1;
         } else {
            var2 = (OffsetDateTime)var1.a(this);
         }
      } else {
         var2 = this.b(this.e.a(var1), this.f);
      }

      return var2;
   }

   public OffsetDateTime a(TemporalAmount var1) {
      return (OffsetDateTime)var1.a(this);
   }

   public OffsetDateTime a(TemporalField var1, long var2) {
      OffsetDateTime var5;
      if(var1 instanceof ChronoField) {
         ChronoField var4 = (ChronoField)var1;
         switch(null.a[var4.ordinal()]) {
         case 1:
            var5 = a((Instant)Instant.a(var2, (long)this.b()), (ZoneId)this.f);
            break;
         case 2:
            var5 = this.b(this.e, ZoneOffset.a(var4.b(var2)));
            break;
         default:
            var5 = this.b(this.e.a(var1, var2), this.f);
         }
      } else {
         var5 = (OffsetDateTime)var1.a(this, var2);
      }

      return var5;
   }

   public ZoneOffset a() {
      return this.f;
   }

   public Temporal a(Temporal var1) {
      return var1.b(ChronoField.u, this.d().n()).b(ChronoField.b, this.e().f()).b(ChronoField.D, (long)this.a().f());
   }

   void a(DataOutput var1) throws IOException {
      this.e.a(var1);
      this.f.b(var1);
   }

   public boolean a(TemporalField var1) {
      boolean var2;
      if(!(var1 instanceof ChronoField) && (var1 == null || !var1.a(this))) {
         var2 = false;
      } else {
         var2 = true;
      }

      return var2;
   }

   public int b() {
      return this.e.i();
   }

   public OffsetDateTime b(long var1, TemporalUnit var3) {
      OffsetDateTime var4;
      if(var1 == Long.MIN_VALUE) {
         var4 = this.a(Long.MAX_VALUE, var3).a(1L, var3);
      } else {
         var4 = this.a(-var1, var3);
      }

      return var4;
   }

   // $FF: synthetic method
   public Temporal b(TemporalAdjuster var1) {
      return this.a(var1);
   }

   // $FF: synthetic method
   public Temporal b(TemporalField var1, long var2) {
      return this.a(var1, var2);
   }

   public ValueRange b(TemporalField var1) {
      ValueRange var2;
      if(var1 instanceof ChronoField) {
         if(var1 != ChronoField.C && var1 != ChronoField.D) {
            var2 = this.e.b(var1);
         } else {
            var2 = var1.a();
         }
      } else {
         var2 = var1.b(this);
      }

      return var2;
   }

   public int c(TemporalField var1) {
      int var2;
      if(var1 instanceof ChronoField) {
         switch(null.a[((ChronoField)var1).ordinal()]) {
         case 1:
            throw new DateTimeException("Field too large for an int: " + var1);
         case 2:
            var2 = this.a().f();
            break;
         default:
            var2 = this.e.c(var1);
         }
      } else {
         var2 = super.c(var1);
      }

      return var2;
   }

   public LocalDateTime c() {
      return this.e;
   }

   // $FF: synthetic method
   public Temporal c(long var1, TemporalUnit var3) {
      return this.b(var1, var3);
   }

   // $FF: synthetic method
   public Temporal c(TemporalAmount var1) {
      return this.a(var1);
   }

   // $FF: synthetic method
   public int compareTo(Object var1) {
      return this.a((OffsetDateTime)var1);
   }

   public long d(TemporalField var1) {
      long var2;
      if(var1 instanceof ChronoField) {
         switch(null.a[((ChronoField)var1).ordinal()]) {
         case 1:
            var2 = this.f();
            break;
         case 2:
            var2 = (long)this.a().f();
            break;
         default:
            var2 = this.e.d(var1);
         }
      } else {
         var2 = var1.c(this);
      }

      return var2;
   }

   public LocalDate d() {
      return this.e.j();
   }

   // $FF: synthetic method
   public Temporal d(long var1, TemporalUnit var3) {
      return this.a(var1, var3);
   }

   public LocalTime e() {
      return this.e.k();
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(this != var1) {
         if(var1 instanceof OffsetDateTime) {
            OffsetDateTime var3 = (OffsetDateTime)var1;
            if(!this.e.equals(var3.e) || !this.f.equals(var3.f)) {
               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public long f() {
      return this.e.c((ZoneOffset)this.f);
   }

   public int hashCode() {
      return this.e.hashCode() ^ this.f.hashCode();
   }

   public String toString() {
      return this.e.toString() + this.f.toString();
   }
}
