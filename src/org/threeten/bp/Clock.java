package org.threeten.bp;

import java.io.Serializable;

public abstract class Clock {
   public static Clock a() {
      return new Clock.SystemClock(ZoneId.a());
   }

   public abstract ZoneId b();

   public long c() {
      return this.d().c();
   }

   public abstract Instant d();

   public boolean equals(Object var1) {
      return super.equals(var1);
   }

   public int hashCode() {
      return super.hashCode();
   }

   static final class SystemClock extends Clock implements Serializable {
      private final ZoneId a;

      SystemClock(ZoneId var1) {
         this.a = var1;
      }

      public ZoneId b() {
         return this.a;
      }

      public long c() {
         return System.currentTimeMillis();
      }

      public Instant d() {
         return Instant.b(this.c());
      }

      public boolean equals(Object var1) {
         boolean var2;
         if(var1 instanceof Clock.SystemClock) {
            var2 = this.a.equals(((Clock.SystemClock)var1).a);
         } else {
            var2 = false;
         }

         return var2;
      }

      public int hashCode() {
         return this.a.hashCode() + 1;
      }

      public String toString() {
         return "SystemClock[" + this.a + "]";
      }
   }
}
