package org.threeten.bp;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.InvalidObjectException;
import java.io.ObjectStreamException;
import java.io.Serializable;
import org.threeten.bp.chrono.Chronology;
import org.threeten.bp.chrono.IsoChronology;
import org.threeten.bp.format.DateTimeFormatter;
import org.threeten.bp.format.DateTimeFormatterBuilder;
import org.threeten.bp.format.SignStyle;
import org.threeten.bp.jdk8.DefaultInterfaceTemporalAccessor;
import org.threeten.bp.jdk8.Jdk8Methods;
import org.threeten.bp.temporal.ChronoField;
import org.threeten.bp.temporal.ChronoUnit;
import org.threeten.bp.temporal.Temporal;
import org.threeten.bp.temporal.TemporalAccessor;
import org.threeten.bp.temporal.TemporalAdjuster;
import org.threeten.bp.temporal.TemporalField;
import org.threeten.bp.temporal.TemporalQueries;
import org.threeten.bp.temporal.TemporalQuery;
import org.threeten.bp.temporal.TemporalUnit;
import org.threeten.bp.temporal.UnsupportedTemporalTypeException;
import org.threeten.bp.temporal.ValueRange;

public final class Year extends DefaultInterfaceTemporalAccessor implements Serializable, Comparable, Temporal, TemporalAdjuster {
   public static final TemporalQuery a = new TemporalQuery() {
      public Year a(TemporalAccessor var1) {
         return Year.a(var1);
      }

      // $FF: synthetic method
      public Object b(TemporalAccessor var1) {
         return this.a(var1);
      }
   };
   private static final DateTimeFormatter b;
   private final int c;

   static {
      b = (new DateTimeFormatterBuilder()).a(ChronoField.A, 4, 10, (SignStyle)SignStyle.e).j();
   }

   private Year(int var1) {
      this.c = var1;
   }

   public static Year a() {
      return a(Clock.a());
   }

   public static Year a(int var0) {
      ChronoField.A.a((long)var0);
      return new Year(var0);
   }

   static Year a(DataInput var0) throws IOException {
      return a(var0.readInt());
   }

   public static Year a(Clock var0) {
      return a(LocalDate.a(var0).d());
   }

   public static Year a(TemporalAccessor param0) {
      // $FF: Couldn't be decompiled
   }

   public static boolean a(long var0) {
      boolean var2;
      if((3L & var0) != 0L || var0 % 100L == 0L && var0 % 400L != 0L) {
         var2 = false;
      } else {
         var2 = true;
      }

      return var2;
   }

   private Object readResolve() throws ObjectStreamException {
      throw new InvalidObjectException("Deserialization via serialization delegate");
   }

   private Object writeReplace() {
      return new Ser(67, this);
   }

   public int a(Year var1) {
      return this.c - var1.c;
   }

   public long a(Temporal var1, TemporalUnit var2) {
      Year var5 = a((TemporalAccessor)var1);
      long var3;
      if(var2 instanceof ChronoUnit) {
         var3 = (long)var5.c - (long)this.c;
         switch(null.b[((ChronoUnit)var2).ordinal()]) {
         case 1:
            break;
         case 2:
            var3 /= 10L;
            break;
         case 3:
            var3 /= 100L;
            break;
         case 4:
            var3 /= 1000L;
            break;
         case 5:
            var3 = var5.d(ChronoField.B) - this.d(ChronoField.B);
            break;
         default:
            throw new UnsupportedTemporalTypeException("Unsupported unit: " + var2);
         }
      } else {
         var3 = var2.a(this, var5);
      }

      return var3;
   }

   public Object a(TemporalQuery var1) {
      Object var2;
      if(var1 == TemporalQueries.b()) {
         var2 = IsoChronology.b;
      } else if(var1 == TemporalQueries.c()) {
         var2 = ChronoUnit.k;
      } else if(var1 != TemporalQueries.f() && var1 != TemporalQueries.g() && var1 != TemporalQueries.d() && var1 != TemporalQueries.a() && var1 != TemporalQueries.e()) {
         var2 = super.a(var1);
      } else {
         var2 = null;
      }

      return var2;
   }

   public Year a(long var1, TemporalUnit var3) {
      Year var4;
      if(var3 instanceof ChronoUnit) {
         switch(null.b[((ChronoUnit)var3).ordinal()]) {
         case 1:
            var4 = this.b(var1);
            break;
         case 2:
            var4 = this.b(Jdk8Methods.a(var1, 10));
            break;
         case 3:
            var4 = this.b(Jdk8Methods.a(var1, 100));
            break;
         case 4:
            var4 = this.b(Jdk8Methods.a(var1, 1000));
            break;
         case 5:
            var4 = this.a(ChronoField.B, Jdk8Methods.b(this.d(ChronoField.B), var1));
            break;
         default:
            throw new UnsupportedTemporalTypeException("Unsupported unit: " + var3);
         }
      } else {
         var4 = (Year)var3.a(this, var1);
      }

      return var4;
   }

   public Year a(TemporalAdjuster var1) {
      return (Year)var1.a(this);
   }

   public Year a(TemporalField var1, long var2) {
      Year var7;
      if(var1 instanceof ChronoField) {
         ChronoField var6 = (ChronoField)var1;
         var6.a(var2);
         switch(null.a[var6.ordinal()]) {
         case 1:
            long var4 = var2;
            if(this.c < 1) {
               var4 = 1L - var2;
            }

            var7 = a((int)var4);
            break;
         case 2:
            var7 = a((int)var2);
            break;
         case 3:
            var7 = this;
            if(this.d(ChronoField.B) != var2) {
               var7 = a(1 - this.c);
            }
            break;
         default:
            throw new UnsupportedTemporalTypeException("Unsupported field: " + var1);
         }
      } else {
         var7 = (Year)var1.a(this, var2);
      }

      return var7;
   }

   public Temporal a(Temporal var1) {
      if(!Chronology.a((TemporalAccessor)var1).equals(IsoChronology.b)) {
         throw new DateTimeException("Adjustment only supported on ISO date-time");
      } else {
         return var1.b(ChronoField.A, (long)this.c);
      }
   }

   void a(DataOutput var1) throws IOException {
      var1.writeInt(this.c);
   }

   public boolean a(TemporalField var1) {
      boolean var3 = true;
      boolean var2 = false;
      if(var1 instanceof ChronoField) {
         if(var1 == ChronoField.A || var1 == ChronoField.z || var1 == ChronoField.B) {
            var2 = true;
         }
      } else if(var1 != null && var1.a(this)) {
         var2 = var3;
      } else {
         var2 = false;
      }

      return var2;
   }

   public int b() {
      return this.c;
   }

   public Year b(long var1) {
      Year var3;
      if(var1 == 0L) {
         var3 = this;
      } else {
         var3 = a(ChronoField.A.b((long)this.c + var1));
      }

      return var3;
   }

   public Year b(long var1, TemporalUnit var3) {
      Year var4;
      if(var1 == Long.MIN_VALUE) {
         var4 = this.a(Long.MAX_VALUE, var3).a(1L, var3);
      } else {
         var4 = this.a(-var1, var3);
      }

      return var4;
   }

   // $FF: synthetic method
   public Temporal b(TemporalAdjuster var1) {
      return this.a(var1);
   }

   // $FF: synthetic method
   public Temporal b(TemporalField var1, long var2) {
      return this.a(var1, var2);
   }

   public ValueRange b(TemporalField var1) {
      ValueRange var2;
      if(var1 == ChronoField.z) {
         if(this.c <= 0) {
            var2 = ValueRange.a(1L, 1000000000L);
         } else {
            var2 = ValueRange.a(1L, 999999999L);
         }
      } else {
         var2 = super.b(var1);
      }

      return var2;
   }

   public int c(TemporalField var1) {
      return this.b(var1).b(this.d(var1), var1);
   }

   // $FF: synthetic method
   public Temporal c(long var1, TemporalUnit var3) {
      return this.b(var1, var3);
   }

   // $FF: synthetic method
   public int compareTo(Object var1) {
      return this.a((Year)var1);
   }

   public long d(TemporalField var1) {
      long var3;
      if(var1 instanceof ChronoField) {
         switch(null.a[((ChronoField)var1).ordinal()]) {
         case 1:
            int var5;
            if(this.c < 1) {
               var5 = 1 - this.c;
            } else {
               var5 = this.c;
            }

            var3 = (long)var5;
            break;
         case 2:
            var3 = (long)this.c;
            break;
         case 3:
            byte var2;
            if(this.c < 1) {
               var2 = 0;
            } else {
               var2 = 1;
            }

            var3 = (long)var2;
            break;
         default:
            throw new UnsupportedTemporalTypeException("Unsupported field: " + var1);
         }
      } else {
         var3 = var1.c(this);
      }

      return var3;
   }

   // $FF: synthetic method
   public Temporal d(long var1, TemporalUnit var3) {
      return this.a(var1, var3);
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(this != var1) {
         if(var1 instanceof Year) {
            if(this.c != ((Year)var1).c) {
               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public int hashCode() {
      return this.c;
   }

   public String toString() {
      return Integer.toString(this.c);
   }
}
