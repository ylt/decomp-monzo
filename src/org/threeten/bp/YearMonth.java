package org.threeten.bp;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.InvalidObjectException;
import java.io.ObjectStreamException;
import java.io.Serializable;
import org.threeten.bp.chrono.Chronology;
import org.threeten.bp.chrono.IsoChronology;
import org.threeten.bp.format.DateTimeFormatter;
import org.threeten.bp.format.DateTimeFormatterBuilder;
import org.threeten.bp.format.SignStyle;
import org.threeten.bp.jdk8.DefaultInterfaceTemporalAccessor;
import org.threeten.bp.jdk8.Jdk8Methods;
import org.threeten.bp.temporal.ChronoField;
import org.threeten.bp.temporal.ChronoUnit;
import org.threeten.bp.temporal.Temporal;
import org.threeten.bp.temporal.TemporalAccessor;
import org.threeten.bp.temporal.TemporalAdjuster;
import org.threeten.bp.temporal.TemporalField;
import org.threeten.bp.temporal.TemporalQueries;
import org.threeten.bp.temporal.TemporalQuery;
import org.threeten.bp.temporal.TemporalUnit;
import org.threeten.bp.temporal.UnsupportedTemporalTypeException;
import org.threeten.bp.temporal.ValueRange;

public final class YearMonth extends DefaultInterfaceTemporalAccessor implements Serializable, Comparable, Temporal, TemporalAdjuster {
   public static final TemporalQuery a = new TemporalQuery() {
      public YearMonth a(TemporalAccessor var1) {
         return YearMonth.a(var1);
      }

      // $FF: synthetic method
      public Object b(TemporalAccessor var1) {
         return this.a(var1);
      }
   };
   private static final DateTimeFormatter b;
   private final int c;
   private final int d;

   static {
      b = (new DateTimeFormatterBuilder()).a(ChronoField.A, 4, 10, (SignStyle)SignStyle.e).a('-').a(ChronoField.x, 2).j();
   }

   private YearMonth(int var1, int var2) {
      this.c = var1;
      this.d = var2;
   }

   public static YearMonth a() {
      return a(Clock.a());
   }

   public static YearMonth a(int var0, int var1) {
      ChronoField.A.a((long)var0);
      ChronoField.x.a((long)var1);
      return new YearMonth(var0, var1);
   }

   public static YearMonth a(int var0, Month var1) {
      Jdk8Methods.a(var1, (String)"month");
      return a(var0, var1.a());
   }

   static YearMonth a(DataInput var0) throws IOException {
      return a(var0.readInt(), var0.readByte());
   }

   public static YearMonth a(CharSequence var0) {
      return a(var0, b);
   }

   public static YearMonth a(CharSequence var0, DateTimeFormatter var1) {
      Jdk8Methods.a(var1, (String)"formatter");
      return (YearMonth)var1.a(var0, a);
   }

   public static YearMonth a(Clock var0) {
      LocalDate var1 = LocalDate.a(var0);
      return a(var1.d(), var1.f());
   }

   public static YearMonth a(TemporalAccessor param0) {
      // $FF: Couldn't be decompiled
   }

   private YearMonth b(int var1, int var2) {
      YearMonth var3;
      if(this.c == var1 && this.d == var2) {
         var3 = this;
      } else {
         var3 = new YearMonth(var1, var2);
      }

      return var3;
   }

   private long g() {
      return (long)this.c * 12L + (long)(this.d - 1);
   }

   private Object readResolve() throws ObjectStreamException {
      throw new InvalidObjectException("Deserialization via serialization delegate");
   }

   private Object writeReplace() {
      return new Ser(68, this);
   }

   public int a(YearMonth var1) {
      int var3 = this.c - var1.c;
      int var2 = var3;
      if(var3 == 0) {
         var2 = this.d - var1.d;
      }

      return var2;
   }

   public long a(Temporal var1, TemporalUnit var2) {
      YearMonth var5 = a((TemporalAccessor)var1);
      long var3;
      if(var2 instanceof ChronoUnit) {
         var3 = var5.g() - this.g();
         switch(null.b[((ChronoUnit)var2).ordinal()]) {
         case 1:
            break;
         case 2:
            var3 /= 12L;
            break;
         case 3:
            var3 /= 120L;
            break;
         case 4:
            var3 /= 1200L;
            break;
         case 5:
            var3 /= 12000L;
            break;
         case 6:
            var3 = var5.d(ChronoField.B) - this.d(ChronoField.B);
            break;
         default:
            throw new UnsupportedTemporalTypeException("Unsupported unit: " + var2);
         }
      } else {
         var3 = var2.a(this, var5);
      }

      return var3;
   }

   public Object a(TemporalQuery var1) {
      Object var2;
      if(var1 == TemporalQueries.b()) {
         var2 = IsoChronology.b;
      } else if(var1 == TemporalQueries.c()) {
         var2 = ChronoUnit.j;
      } else if(var1 != TemporalQueries.f() && var1 != TemporalQueries.g() && var1 != TemporalQueries.d() && var1 != TemporalQueries.a() && var1 != TemporalQueries.e()) {
         var2 = super.a(var1);
      } else {
         var2 = null;
      }

      return var2;
   }

   public YearMonth a(int var1) {
      ChronoField.A.a((long)var1);
      return this.b(var1, this.d);
   }

   public YearMonth a(long var1) {
      YearMonth var3;
      if(var1 == 0L) {
         var3 = this;
      } else {
         var3 = this.b(ChronoField.A.b((long)this.c + var1), this.d);
      }

      return var3;
   }

   public YearMonth a(long var1, TemporalUnit var3) {
      YearMonth var4;
      if(var3 instanceof ChronoUnit) {
         switch(null.b[((ChronoUnit)var3).ordinal()]) {
         case 1:
            var4 = this.b(var1);
            break;
         case 2:
            var4 = this.a(var1);
            break;
         case 3:
            var4 = this.a(Jdk8Methods.a(var1, 10));
            break;
         case 4:
            var4 = this.a(Jdk8Methods.a(var1, 100));
            break;
         case 5:
            var4 = this.a(Jdk8Methods.a(var1, 1000));
            break;
         case 6:
            var4 = this.a(ChronoField.B, Jdk8Methods.b(this.d(ChronoField.B), var1));
            break;
         default:
            throw new UnsupportedTemporalTypeException("Unsupported unit: " + var3);
         }
      } else {
         var4 = (YearMonth)var3.a(this, var1);
      }

      return var4;
   }

   public YearMonth a(TemporalAdjuster var1) {
      return (YearMonth)var1.a(this);
   }

   public YearMonth a(TemporalField var1, long var2) {
      YearMonth var7;
      if(var1 instanceof ChronoField) {
         ChronoField var6 = (ChronoField)var1;
         var6.a(var2);
         switch(null.a[var6.ordinal()]) {
         case 1:
            var7 = this.b((int)var2);
            break;
         case 2:
            var7 = this.b(var2 - this.d(ChronoField.y));
            break;
         case 3:
            long var4 = var2;
            if(this.c < 1) {
               var4 = 1L - var2;
            }

            var7 = this.a((int)var4);
            break;
         case 4:
            var7 = this.a((int)var2);
            break;
         case 5:
            var7 = this;
            if(this.d(ChronoField.B) != var2) {
               var7 = this.a(1 - this.c);
            }
            break;
         default:
            throw new UnsupportedTemporalTypeException("Unsupported field: " + var1);
         }
      } else {
         var7 = (YearMonth)var1.a(this, var2);
      }

      return var7;
   }

   public Temporal a(Temporal var1) {
      if(!Chronology.a((TemporalAccessor)var1).equals(IsoChronology.b)) {
         throw new DateTimeException("Adjustment only supported on ISO date-time");
      } else {
         return var1.b(ChronoField.y, this.g());
      }
   }

   void a(DataOutput var1) throws IOException {
      var1.writeInt(this.c);
      var1.writeByte(this.d);
   }

   public boolean a(TemporalField var1) {
      boolean var3 = true;
      boolean var2 = false;
      if(var1 instanceof ChronoField) {
         if(var1 == ChronoField.A || var1 == ChronoField.x || var1 == ChronoField.y || var1 == ChronoField.z || var1 == ChronoField.B) {
            var2 = true;
         }
      } else if(var1 != null && var1.a(this)) {
         var2 = var3;
      } else {
         var2 = false;
      }

      return var2;
   }

   public int b() {
      return this.c;
   }

   public YearMonth b(int var1) {
      ChronoField.x.a((long)var1);
      return this.b(this.c, var1);
   }

   public YearMonth b(long var1) {
      YearMonth var3;
      if(var1 == 0L) {
         var3 = this;
      } else {
         var1 += (long)this.c * 12L + (long)(this.d - 1);
         var3 = this.b(ChronoField.A.b(Jdk8Methods.e(var1, 12L)), Jdk8Methods.b(var1, 12) + 1);
      }

      return var3;
   }

   public YearMonth b(long var1, TemporalUnit var3) {
      YearMonth var4;
      if(var1 == Long.MIN_VALUE) {
         var4 = this.a(Long.MAX_VALUE, var3).a(1L, var3);
      } else {
         var4 = this.a(-var1, var3);
      }

      return var4;
   }

   // $FF: synthetic method
   public Temporal b(TemporalAdjuster var1) {
      return this.a(var1);
   }

   // $FF: synthetic method
   public Temporal b(TemporalField var1, long var2) {
      return this.a(var1, var2);
   }

   public ValueRange b(TemporalField var1) {
      ValueRange var2;
      if(var1 == ChronoField.z) {
         if(this.b() <= 0) {
            var2 = ValueRange.a(1L, 1000000000L);
         } else {
            var2 = ValueRange.a(1L, 999999999L);
         }
      } else {
         var2 = super.b(var1);
      }

      return var2;
   }

   public boolean b(YearMonth var1) {
      boolean var2;
      if(this.a(var1) > 0) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   public int c(TemporalField var1) {
      return this.b(var1).b(this.d(var1), var1);
   }

   public LocalDate c(int var1) {
      return LocalDate.a(this.c, this.d, var1);
   }

   public Month c() {
      return Month.a(this.d);
   }

   // $FF: synthetic method
   public Temporal c(long var1, TemporalUnit var3) {
      return this.b(var1, var3);
   }

   // $FF: synthetic method
   public int compareTo(Object var1) {
      return this.a((YearMonth)var1);
   }

   public long d(TemporalField var1) {
      long var3;
      if(var1 instanceof ChronoField) {
         switch(null.a[((ChronoField)var1).ordinal()]) {
         case 1:
            var3 = (long)this.d;
            break;
         case 2:
            var3 = this.g();
            break;
         case 3:
            int var5;
            if(this.c < 1) {
               var5 = 1 - this.c;
            } else {
               var5 = this.c;
            }

            var3 = (long)var5;
            break;
         case 4:
            var3 = (long)this.c;
            break;
         case 5:
            byte var2;
            if(this.c < 1) {
               var2 = 0;
            } else {
               var2 = 1;
            }

            var3 = (long)var2;
            break;
         default:
            throw new UnsupportedTemporalTypeException("Unsupported field: " + var1);
         }
      } else {
         var3 = var1.c(this);
      }

      return var3;
   }

   // $FF: synthetic method
   public Temporal d(long var1, TemporalUnit var3) {
      return this.a(var1, var3);
   }

   public boolean d() {
      return IsoChronology.b.a((long)this.c);
   }

   public int e() {
      return this.c().a(this.d());
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(this != var1) {
         if(var1 instanceof YearMonth) {
            YearMonth var3 = (YearMonth)var1;
            if(this.c != var3.c || this.d != var3.d) {
               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public LocalDate f() {
      return LocalDate.a(this.c, this.d, this.e());
   }

   public int hashCode() {
      return this.c ^ this.d << 27;
   }

   public String toString() {
      int var1 = Math.abs(this.c);
      StringBuilder var3 = new StringBuilder(9);
      if(var1 < 1000) {
         if(this.c < 0) {
            var3.append(this.c - 10000).deleteCharAt(1);
         } else {
            var3.append(this.c + 10000).deleteCharAt(0);
         }
      } else {
         var3.append(this.c);
      }

      String var2;
      if(this.d < 10) {
         var2 = "-0";
      } else {
         var2 = "-";
      }

      return var3.append(var2).append(this.d).toString();
   }
}
