package org.threeten.bp;

import java.util.Locale;
import org.threeten.bp.chrono.Chronology;
import org.threeten.bp.chrono.IsoChronology;
import org.threeten.bp.format.DateTimeFormatterBuilder;
import org.threeten.bp.format.TextStyle;
import org.threeten.bp.temporal.ChronoField;
import org.threeten.bp.temporal.ChronoUnit;
import org.threeten.bp.temporal.Temporal;
import org.threeten.bp.temporal.TemporalAccessor;
import org.threeten.bp.temporal.TemporalAdjuster;
import org.threeten.bp.temporal.TemporalField;
import org.threeten.bp.temporal.TemporalQueries;
import org.threeten.bp.temporal.TemporalQuery;
import org.threeten.bp.temporal.UnsupportedTemporalTypeException;
import org.threeten.bp.temporal.ValueRange;

public enum Month implements TemporalAccessor, TemporalAdjuster {
   a,
   b,
   c,
   d,
   e,
   f,
   g,
   h,
   i,
   j,
   k,
   l;

   public static final TemporalQuery m = new TemporalQuery() {
      public Month a(TemporalAccessor var1) {
         return Month.a(var1);
      }

      // $FF: synthetic method
      public Object b(TemporalAccessor var1) {
         return this.a(var1);
      }
   };
   private static final Month[] n = values();

   public static Month a(int var0) {
      if(var0 >= 1 && var0 <= 12) {
         return n[var0 - 1];
      } else {
         throw new DateTimeException("Invalid value for MonthOfYear: " + var0);
      }
   }

   public static Month a(TemporalAccessor param0) {
      // $FF: Couldn't be decompiled
   }

   public int a() {
      return this.ordinal() + 1;
   }

   public int a(boolean var1) {
      byte var2;
      switch(null.a[this.ordinal()]) {
      case 1:
         if(var1) {
            var2 = 29;
         } else {
            var2 = 28;
         }
         break;
      case 2:
      case 3:
      case 4:
      case 5:
         var2 = 30;
         break;
      default:
         var2 = 31;
      }

      return var2;
   }

   public Object a(TemporalQuery var1) {
      Object var2;
      if(var1 == TemporalQueries.b()) {
         var2 = IsoChronology.b;
      } else if(var1 == TemporalQueries.c()) {
         var2 = ChronoUnit.j;
      } else if(var1 != TemporalQueries.f() && var1 != TemporalQueries.g() && var1 != TemporalQueries.d() && var1 != TemporalQueries.a() && var1 != TemporalQueries.e()) {
         var2 = var1.b(this);
      } else {
         var2 = null;
      }

      return var2;
   }

   public String a(TextStyle var1, Locale var2) {
      return (new DateTimeFormatterBuilder()).a((TemporalField)ChronoField.x, (TextStyle)var1).a(var2).a((TemporalAccessor)this);
   }

   public Month a(long var1) {
      int var3 = (int)(var1 % 12L);
      return n[(var3 + 12 + this.ordinal()) % 12];
   }

   public Temporal a(Temporal var1) {
      if(!Chronology.a((TemporalAccessor)var1).equals(IsoChronology.b)) {
         throw new DateTimeException("Adjustment only supported on ISO date-time");
      } else {
         return var1.b(ChronoField.x, (long)this.a());
      }
   }

   public boolean a(TemporalField var1) {
      boolean var2 = true;
      if(var1 instanceof ChronoField) {
         if(var1 != ChronoField.x) {
            var2 = false;
         }
      } else if(var1 == null || !var1.a(this)) {
         var2 = false;
      }

      return var2;
   }

   public int b() {
      byte var1;
      switch(null.a[this.ordinal()]) {
      case 1:
         var1 = 28;
         break;
      case 2:
      case 3:
      case 4:
      case 5:
         var1 = 30;
         break;
      default:
         var1 = 31;
      }

      return var1;
   }

   public int b(boolean var1) {
      int var2 = 1;
      byte var3;
      if(var1) {
         var3 = 1;
      } else {
         var3 = 0;
      }

      switch(null.a[this.ordinal()]) {
      case 1:
         var2 = 32;
         break;
      case 2:
         var2 = var3 + 91;
         break;
      case 3:
         var2 = var3 + 152;
         break;
      case 4:
         var2 = var3 + 244;
         break;
      case 5:
         var2 = var3 + 305;
      case 6:
         break;
      case 7:
         var2 = var3 + 60;
         break;
      case 8:
         var2 = var3 + 121;
         break;
      case 9:
         var2 = var3 + 182;
         break;
      case 10:
         var2 = var3 + 213;
         break;
      case 11:
         var2 = var3 + 274;
         break;
      default:
         var2 = var3 + 335;
      }

      return var2;
   }

   public ValueRange b(TemporalField var1) {
      ValueRange var2;
      if(var1 == ChronoField.x) {
         var2 = var1.a();
      } else {
         if(var1 instanceof ChronoField) {
            throw new UnsupportedTemporalTypeException("Unsupported field: " + var1);
         }

         var2 = var1.b(this);
      }

      return var2;
   }

   public int c() {
      byte var1;
      switch(null.a[this.ordinal()]) {
      case 1:
         var1 = 29;
         break;
      case 2:
      case 3:
      case 4:
      case 5:
         var1 = 30;
         break;
      default:
         var1 = 31;
      }

      return var1;
   }

   public int c(TemporalField var1) {
      int var2;
      if(var1 == ChronoField.x) {
         var2 = this.a();
      } else {
         var2 = this.b(var1).b(this.d(var1), var1);
      }

      return var2;
   }

   public long d(TemporalField var1) {
      long var2;
      if(var1 == ChronoField.x) {
         var2 = (long)this.a();
      } else {
         if(var1 instanceof ChronoField) {
            throw new UnsupportedTemporalTypeException("Unsupported field: " + var1);
         }

         var2 = var1.c(this);
      }

      return var2;
   }
}
