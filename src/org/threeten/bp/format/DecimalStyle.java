package org.threeten.bp.format;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public final class DecimalStyle {
   public static final DecimalStyle a = new DecimalStyle('0', '+', '-', '.');
   private static final ConcurrentMap b = new ConcurrentHashMap(16, 0.75F, 2);
   private final char c;
   private final char d;
   private final char e;
   private final char f;

   private DecimalStyle(char var1, char var2, char var3, char var4) {
      this.c = var1;
      this.d = var2;
      this.e = var3;
      this.f = var4;
   }

   public char a() {
      return this.c;
   }

   int a(char var1) {
      int var2 = var1 - this.c;
      if(var2 < 0 || var2 > 9) {
         var2 = -1;
      }

      return var2;
   }

   String a(String var1) {
      if(this.c != 48) {
         char var3 = this.c;
         char[] var4 = var1.toCharArray();

         for(int var2 = 0; var2 < var4.length; ++var2) {
            var4[var2] = (char)(var4[var2] + (var3 - 48));
         }

         var1 = new String(var4);
      }

      return var1;
   }

   public char b() {
      return this.d;
   }

   public char c() {
      return this.e;
   }

   public char d() {
      return this.f;
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(this != var1) {
         if(var1 instanceof DecimalStyle) {
            DecimalStyle var3 = (DecimalStyle)var1;
            if(this.c != var3.c || this.d != var3.d || this.e != var3.e || this.f != var3.f) {
               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public int hashCode() {
      return this.c + this.d + this.e + this.f;
   }

   public String toString() {
      return "DecimalStyle[" + this.c + this.d + this.e + this.f + "]";
   }
}
