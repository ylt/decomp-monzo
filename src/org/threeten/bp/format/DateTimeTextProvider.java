package org.threeten.bp.format;

import java.util.Iterator;
import java.util.Locale;
import org.threeten.bp.temporal.TemporalField;

abstract class DateTimeTextProvider {
   static DateTimeTextProvider a() {
      return new SimpleDateTimeTextProvider();
   }

   public abstract String a(TemporalField var1, long var2, TextStyle var4, Locale var5);

   public abstract Iterator a(TemporalField var1, TextStyle var2, Locale var3);
}
