package org.threeten.bp.format;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.threeten.bp.Period;
import org.threeten.bp.ZoneId;
import org.threeten.bp.chrono.Chronology;
import org.threeten.bp.chrono.IsoChronology;
import org.threeten.bp.jdk8.DefaultInterfaceTemporalAccessor;
import org.threeten.bp.jdk8.Jdk8Methods;
import org.threeten.bp.temporal.TemporalField;
import org.threeten.bp.temporal.TemporalQueries;
import org.threeten.bp.temporal.TemporalQuery;
import org.threeten.bp.temporal.UnsupportedTemporalTypeException;

final class DateTimeParseContext {
   private Locale a;
   private DecimalStyle b;
   private Chronology c;
   private ZoneId d;
   private boolean e = true;
   private boolean f = true;
   private final ArrayList g = new ArrayList();

   DateTimeParseContext(DateTimeFormatter var1) {
      this.a = var1.a();
      this.b = var1.b();
      this.c = var1.c();
      this.d = var1.d();
      this.g.add(new DateTimeParseContext.Parsed());
   }

   DateTimeParseContext(DateTimeParseContext var1) {
      this.a = var1.a;
      this.b = var1.b;
      this.c = var1.c;
      this.d = var1.d;
      this.e = var1.e;
      this.f = var1.f;
      this.g.add(new DateTimeParseContext.Parsed());
   }

   static boolean b(char var0, char var1) {
      boolean var2;
      if(var0 != var1 && Character.toUpperCase(var0) != Character.toUpperCase(var1) && Character.toLowerCase(var0) != Character.toLowerCase(var1)) {
         var2 = false;
      } else {
         var2 = true;
      }

      return var2;
   }

   private DateTimeParseContext.Parsed j() {
      return (DateTimeParseContext.Parsed)this.g.get(this.g.size() - 1);
   }

   int a(TemporalField var1, long var2, int var4, int var5) {
      Jdk8Methods.a(var1, (String)"field");
      Long var7 = (Long)this.j().c.put(var1, Long.valueOf(var2));
      int var6 = var5;
      if(var7 != null) {
         var6 = var5;
         if(var7.longValue() != var2) {
            var6 = ~var4;
         }
      }

      return var6;
   }

   Long a(TemporalField var1) {
      return (Long)this.j().c.get(var1);
   }

   DateTimeParseContext a() {
      return new DateTimeParseContext(this);
   }

   void a(ZoneId var1) {
      Jdk8Methods.a(var1, (String)"zone");
      this.j().b = var1;
   }

   void a(DateTimeFormatterBuilder.ReducedPrinterParser var1, long var2, int var4, int var5) {
      DateTimeParseContext.Parsed var6 = this.j();
      if(var6.f == null) {
         var6.f = new ArrayList(2);
      }

      var6.f.add(new Object[]{var1, Long.valueOf(var2), Integer.valueOf(var4), Integer.valueOf(var5)});
   }

   void a(boolean var1) {
      this.e = var1;
   }

   boolean a(char var1, char var2) {
      boolean var3;
      if(this.e()) {
         if(var1 == var2) {
            var3 = true;
         } else {
            var3 = false;
         }
      } else {
         var3 = b(var1, var2);
      }

      return var3;
   }

   boolean a(CharSequence var1, int var2, CharSequence var3, int var4, int var5) {
      boolean var10 = false;
      boolean var9 = var10;
      if(var2 + var5 <= var1.length()) {
         if(var4 + var5 > var3.length()) {
            var9 = var10;
         } else {
            int var8;
            if(this.e()) {
               for(var8 = 0; var8 < var5; ++var8) {
                  var9 = var10;
                  if(var1.charAt(var2 + var8) != var3.charAt(var4 + var8)) {
                     return var9;
                  }
               }
            } else {
               for(var8 = 0; var8 < var5; ++var8) {
                  char var7 = var1.charAt(var2 + var8);
                  char var6 = var3.charAt(var4 + var8);
                  if(var7 != var6 && Character.toUpperCase(var7) != Character.toUpperCase(var6)) {
                     var9 = var10;
                     if(Character.toLowerCase(var7) != Character.toLowerCase(var6)) {
                        return var9;
                     }
                  }
               }
            }

            var9 = true;
         }
      }

      return var9;
   }

   Locale b() {
      return this.a;
   }

   void b(boolean var1) {
      this.f = var1;
   }

   DecimalStyle c() {
      return this.b;
   }

   void c(boolean var1) {
      if(var1) {
         this.g.remove(this.g.size() - 2);
      } else {
         this.g.remove(this.g.size() - 1);
      }

   }

   Chronology d() {
      Chronology var2 = this.j().a;
      Object var1 = var2;
      if(var2 == null) {
         var2 = this.c;
         var1 = var2;
         if(var2 == null) {
            var1 = IsoChronology.b;
         }
      }

      return (Chronology)var1;
   }

   boolean e() {
      return this.e;
   }

   boolean f() {
      return this.f;
   }

   void g() {
      this.g.add(this.j().a());
   }

   void h() {
      this.j().d = true;
   }

   DateTimeParseContext.Parsed i() {
      return this.j();
   }

   public String toString() {
      return this.j().toString();
   }

   final class Parsed extends DefaultInterfaceTemporalAccessor {
      Chronology a;
      ZoneId b;
      final Map c;
      boolean d;
      Period e;
      List f;

      private Parsed() {
         this.a = null;
         this.b = null;
         this.c = new HashMap();
         this.e = Period.a;
      }

      // $FF: synthetic method
      Parsed(Object var2) {
         this();
      }

      public Object a(TemporalQuery var1) {
         Object var2;
         if(var1 == TemporalQueries.b()) {
            var2 = this.a;
         } else if(var1 != TemporalQueries.a() && var1 != TemporalQueries.d()) {
            var2 = super.a(var1);
         } else {
            var2 = this.b;
         }

         return var2;
      }

      protected DateTimeParseContext.Parsed a() {
         DateTimeParseContext.Parsed var1 = DateTimeParseContext.this.new Parsed();
         var1.a = this.a;
         var1.b = this.b;
         var1.c.putAll(this.c);
         var1.d = this.d;
         return var1;
      }

      public boolean a(TemporalField var1) {
         return this.c.containsKey(var1);
      }

      DateTimeBuilder b() {
         DateTimeBuilder var1 = new DateTimeBuilder();
         var1.a.putAll(this.c);
         var1.b = DateTimeParseContext.this.d();
         if(this.b != null) {
            var1.c = this.b;
         } else {
            var1.c = DateTimeParseContext.this.d;
         }

         var1.f = this.d;
         var1.g = this.e;
         return var1;
      }

      public int c(TemporalField var1) {
         if(!this.c.containsKey(var1)) {
            throw new UnsupportedTemporalTypeException("Unsupported field: " + var1);
         } else {
            return Jdk8Methods.a(((Long)this.c.get(var1)).longValue());
         }
      }

      public long d(TemporalField var1) {
         if(!this.c.containsKey(var1)) {
            throw new UnsupportedTemporalTypeException("Unsupported field: " + var1);
         } else {
            return ((Long)this.c.get(var1)).longValue();
         }
      }

      public String toString() {
         return this.c.toString() + "," + this.a + "," + this.b;
      }
   }
}
