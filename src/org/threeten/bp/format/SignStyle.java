package org.threeten.bp.format;

public enum SignStyle {
   a,
   b,
   c,
   d,
   e;

   boolean a(boolean var1, boolean var2, boolean var3) {
      boolean var5 = false;
      boolean var6 = true;
      boolean var4 = var6;
      switch(this.ordinal()) {
      case 0:
         label17: {
            if(var1) {
               var1 = var5;
               if(var2) {
                  break label17;
               }
            }

            var1 = true;
         }

         var4 = var1;
      case 1:
      case 4:
         break;
      case 2:
      case 3:
      default:
         if(!var2 && !var3) {
            var4 = var6;
         } else {
            var4 = false;
         }
      }

      return var4;
   }
}
