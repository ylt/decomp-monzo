package org.threeten.bp.format;

import java.io.IOException;
import java.text.ParsePosition;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import org.threeten.bp.DateTimeException;
import org.threeten.bp.Period;
import org.threeten.bp.ZoneId;
import org.threeten.bp.chrono.Chronology;
import org.threeten.bp.chrono.IsoChronology;
import org.threeten.bp.jdk8.Jdk8Methods;
import org.threeten.bp.temporal.ChronoField;
import org.threeten.bp.temporal.IsoFields;
import org.threeten.bp.temporal.TemporalAccessor;
import org.threeten.bp.temporal.TemporalField;
import org.threeten.bp.temporal.TemporalQuery;

public final class DateTimeFormatter {
   public static final DateTimeFormatter a;
   public static final DateTimeFormatter b;
   public static final DateTimeFormatter c;
   public static final DateTimeFormatter d;
   public static final DateTimeFormatter e;
   public static final DateTimeFormatter f;
   public static final DateTimeFormatter g;
   public static final DateTimeFormatter h;
   public static final DateTimeFormatter i;
   public static final DateTimeFormatter j;
   public static final DateTimeFormatter k;
   public static final DateTimeFormatter l;
   public static final DateTimeFormatter m;
   public static final DateTimeFormatter n;
   public static final DateTimeFormatter o;
   private static final TemporalQuery p;
   private static final TemporalQuery q;
   private final DateTimeFormatterBuilder.CompositePrinterParser r;
   private final Locale s;
   private final DecimalStyle t;
   private final ResolverStyle u;
   private final Set v;
   private final Chronology w;
   private final ZoneId x;

   static {
      a = (new DateTimeFormatterBuilder()).a(ChronoField.A, 4, 10, (SignStyle)SignStyle.e).a('-').a(ChronoField.x, 2).a('-').a(ChronoField.s, 2).a(ResolverStyle.a).a((Chronology)IsoChronology.b);
      b = (new DateTimeFormatterBuilder()).b().a(a).e().a(ResolverStyle.a).a((Chronology)IsoChronology.b);
      c = (new DateTimeFormatterBuilder()).b().a(a).h().e().a(ResolverStyle.a).a((Chronology)IsoChronology.b);
      d = (new DateTimeFormatterBuilder()).a(ChronoField.m, 2).a(':').a(ChronoField.i, 2).h().a(':').a(ChronoField.g, 2).h().a(ChronoField.a, 0, 9, true).a(ResolverStyle.a);
      e = (new DateTimeFormatterBuilder()).b().a(d).e().a(ResolverStyle.a);
      f = (new DateTimeFormatterBuilder()).b().a(d).h().e().a(ResolverStyle.a);
      g = (new DateTimeFormatterBuilder()).b().a(a).a('T').a(d).a(ResolverStyle.a).a((Chronology)IsoChronology.b);
      h = (new DateTimeFormatterBuilder()).b().a(g).e().a(ResolverStyle.a).a((Chronology)IsoChronology.b);
      i = (new DateTimeFormatterBuilder()).a(h).h().a('[').a().g().a(']').a(ResolverStyle.a).a((Chronology)IsoChronology.b);
      j = (new DateTimeFormatterBuilder()).a(g).h().e().h().a('[').a().g().a(']').a(ResolverStyle.a).a((Chronology)IsoChronology.b);
      k = (new DateTimeFormatterBuilder()).b().a(ChronoField.A, 4, 10, (SignStyle)SignStyle.e).a('-').a(ChronoField.t, 3).h().e().a(ResolverStyle.a).a((Chronology)IsoChronology.b);
      l = (new DateTimeFormatterBuilder()).b().a(IsoFields.d, 4, 10, (SignStyle)SignStyle.e).a("-W").a(IsoFields.c, 2).a('-').a(ChronoField.p, 1).h().e().a(ResolverStyle.a).a((Chronology)IsoChronology.b);
      m = (new DateTimeFormatterBuilder()).b().d().a(ResolverStyle.a);
      n = (new DateTimeFormatterBuilder()).b().a(ChronoField.A, 4).a(ChronoField.x, 2).a(ChronoField.s, 2).h().a("+HHMMss", "Z").a(ResolverStyle.a).a((Chronology)IsoChronology.b);
      HashMap var0 = new HashMap();
      var0.put(Long.valueOf(1L), "Mon");
      var0.put(Long.valueOf(2L), "Tue");
      var0.put(Long.valueOf(3L), "Wed");
      var0.put(Long.valueOf(4L), "Thu");
      var0.put(Long.valueOf(5L), "Fri");
      var0.put(Long.valueOf(6L), "Sat");
      var0.put(Long.valueOf(7L), "Sun");
      HashMap var1 = new HashMap();
      var1.put(Long.valueOf(1L), "Jan");
      var1.put(Long.valueOf(2L), "Feb");
      var1.put(Long.valueOf(3L), "Mar");
      var1.put(Long.valueOf(4L), "Apr");
      var1.put(Long.valueOf(5L), "May");
      var1.put(Long.valueOf(6L), "Jun");
      var1.put(Long.valueOf(7L), "Jul");
      var1.put(Long.valueOf(8L), "Aug");
      var1.put(Long.valueOf(9L), "Sep");
      var1.put(Long.valueOf(10L), "Oct");
      var1.put(Long.valueOf(11L), "Nov");
      var1.put(Long.valueOf(12L), "Dec");
      o = (new DateTimeFormatterBuilder()).b().c().h().a((TemporalField)ChronoField.p, (Map)var0).a(", ").i().a(ChronoField.s, 1, 2, (SignStyle)SignStyle.d).a(' ').a((TemporalField)ChronoField.x, (Map)var1).a(' ').a(ChronoField.A, 4).a(' ').a(ChronoField.m, 2).a(':').a(ChronoField.i, 2).h().a(':').a(ChronoField.g, 2).i().a(' ').a("+HHMM", "GMT").a(ResolverStyle.b).a((Chronology)IsoChronology.b);
      p = new TemporalQuery() {
         public Period a(TemporalAccessor var1) {
            Period var2;
            if(var1 instanceof DateTimeBuilder) {
               var2 = ((DateTimeBuilder)var1).g;
            } else {
               var2 = Period.a;
            }

            return var2;
         }

         // $FF: synthetic method
         public Object b(TemporalAccessor var1) {
            return this.a(var1);
         }
      };
      q = new TemporalQuery() {
         public Boolean a(TemporalAccessor var1) {
            Boolean var2;
            if(var1 instanceof DateTimeBuilder) {
               var2 = Boolean.valueOf(((DateTimeBuilder)var1).f);
            } else {
               var2 = Boolean.FALSE;
            }

            return var2;
         }

         // $FF: synthetic method
         public Object b(TemporalAccessor var1) {
            return this.a(var1);
         }
      };
   }

   DateTimeFormatter(DateTimeFormatterBuilder.CompositePrinterParser var1, Locale var2, DecimalStyle var3, ResolverStyle var4, Set var5, Chronology var6, ZoneId var7) {
      this.r = (DateTimeFormatterBuilder.CompositePrinterParser)Jdk8Methods.a(var1, (String)"printerParser");
      this.s = (Locale)Jdk8Methods.a(var2, (String)"locale");
      this.t = (DecimalStyle)Jdk8Methods.a(var3, (String)"decimalStyle");
      this.u = (ResolverStyle)Jdk8Methods.a(var4, (String)"resolverStyle");
      this.v = var5;
      this.w = var6;
      this.x = var7;
   }

   private DateTimeBuilder a(CharSequence var1, ParsePosition var2) {
      ParsePosition var3;
      if(var2 != null) {
         var3 = var2;
      } else {
         var3 = new ParsePosition(0);
      }

      DateTimeParseContext.Parsed var4 = this.b(var1, var3);
      if(var4 != null && var3.getErrorIndex() < 0 && (var2 != null || var3.getIndex() >= var1.length())) {
         return var4.b();
      } else {
         String var5;
         if(var1.length() > 64) {
            var5 = var1.subSequence(0, 64).toString() + "...";
         } else {
            var5 = var1.toString();
         }

         if(var3.getErrorIndex() >= 0) {
            throw new DateTimeParseException("Text '" + var5 + "' could not be parsed at index " + var3.getErrorIndex(), var1, var3.getErrorIndex());
         } else {
            throw new DateTimeParseException("Text '" + var5 + "' could not be parsed, unparsed text found at index " + var3.getIndex(), var1, var3.getIndex());
         }
      }
   }

   public static DateTimeFormatter a(String var0) {
      return (new DateTimeFormatterBuilder()).b(var0).j();
   }

   public static DateTimeFormatter a(String var0, Locale var1) {
      return (new DateTimeFormatterBuilder()).b(var0).a(var1);
   }

   public static DateTimeFormatter a(FormatStyle var0) {
      Jdk8Methods.a(var0, (String)"dateStyle");
      return (new DateTimeFormatterBuilder()).a((FormatStyle)var0, (FormatStyle)null).j().a((Chronology)IsoChronology.b);
   }

   private DateTimeParseException a(CharSequence var1, RuntimeException var2) {
      String var3;
      if(var1.length() > 64) {
         var3 = var1.subSequence(0, 64).toString() + "...";
      } else {
         var3 = var1.toString();
      }

      return new DateTimeParseException("Text '" + var3 + "' could not be parsed: " + var2.getMessage(), var1, 0, var2);
   }

   private DateTimeParseContext.Parsed b(CharSequence var1, ParsePosition var2) {
      Jdk8Methods.a(var1, (String)"text");
      Jdk8Methods.a(var2, (String)"position");
      DateTimeParseContext var4 = new DateTimeParseContext(this);
      int var3 = var2.getIndex();
      var3 = this.r.a(var4, var1, var3);
      DateTimeParseContext.Parsed var5;
      if(var3 < 0) {
         var2.setErrorIndex(~var3);
         var5 = null;
      } else {
         var2.setIndex(var3);
         var5 = var4.i();
      }

      return var5;
   }

   public Object a(CharSequence var1, TemporalQuery var2) {
      Jdk8Methods.a(var1, (String)"text");
      Jdk8Methods.a(var2, (String)"type");

      try {
         Object var5 = this.a((CharSequence)var1, (ParsePosition)null).a(this.u, this.v).b(var2);
         return var5;
      } catch (DateTimeParseException var3) {
         throw var3;
      } catch (RuntimeException var4) {
         throw this.a(var1, var4);
      }
   }

   public String a(TemporalAccessor var1) {
      StringBuilder var2 = new StringBuilder(32);
      this.a((TemporalAccessor)var1, (Appendable)var2);
      return var2.toString();
   }

   public Locale a() {
      return this.s;
   }

   public DateTimeFormatter a(Chronology var1) {
      DateTimeFormatter var2;
      if(Jdk8Methods.a(this.w, (Object)var1)) {
         var2 = this;
      } else {
         var2 = new DateTimeFormatter(this.r, this.s, this.t, this.u, this.v, var1, this.x);
      }

      return var2;
   }

   public DateTimeFormatter a(ResolverStyle var1) {
      Jdk8Methods.a(var1, (String)"resolverStyle");
      DateTimeFormatter var2;
      if(Jdk8Methods.a(this.u, (Object)var1)) {
         var2 = this;
      } else {
         var2 = new DateTimeFormatter(this.r, this.s, this.t, var1, this.v, this.w, this.x);
      }

      return var2;
   }

   DateTimeFormatterBuilder.CompositePrinterParser a(boolean var1) {
      return this.r.a(var1);
   }

   public void a(TemporalAccessor var1, Appendable var2) {
      Jdk8Methods.a(var1, (String)"temporal");
      Jdk8Methods.a(var2, (String)"appendable");

      try {
         DateTimePrintContext var3 = new DateTimePrintContext(var1, this);
         if(var2 instanceof StringBuilder) {
            this.r.a(var3, (StringBuilder)var2);
         } else {
            StringBuilder var5 = new StringBuilder(32);
            this.r.a(var3, var5);
            var2.append(var5);
         }

      } catch (IOException var4) {
         throw new DateTimeException(var4.getMessage(), var4);
      }
   }

   public DecimalStyle b() {
      return this.t;
   }

   public Chronology c() {
      return this.w;
   }

   public ZoneId d() {
      return this.x;
   }

   public String toString() {
      String var1 = this.r.toString();
      if(!var1.startsWith("[")) {
         var1 = var1.substring(1, var1.length() - 1);
      }

      return var1;
   }
}
