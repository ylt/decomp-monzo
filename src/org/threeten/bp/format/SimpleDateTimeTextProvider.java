package org.threeten.bp.format;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.AbstractMap.SimpleImmutableEntry;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import org.threeten.bp.temporal.ChronoField;
import org.threeten.bp.temporal.IsoFields;
import org.threeten.bp.temporal.TemporalField;

final class SimpleDateTimeTextProvider extends DateTimeTextProvider {
   private static final ConcurrentMap a = new ConcurrentHashMap(16, 0.75F, 2);
   private static final Comparator b = new Comparator() {
      public int a(Entry var1, Entry var2) {
         return ((String)var2.getKey()).length() - ((String)var1.getKey()).length();
      }

      // $FF: synthetic method
      public int compare(Object var1, Object var2) {
         return this.a((Entry)var1, (Entry)var2);
      }
   };

   private Object a(TemporalField var1, Locale var2) {
      Entry var5 = b((Object)var1, (Object)var2);
      Object var4 = a.get(var5);
      Object var3 = var4;
      if(var4 == null) {
         Object var6 = this.b(var1, var2);
         a.putIfAbsent(var5, var6);
         var3 = a.get(var5);
      }

      return var3;
   }

   private static SimpleDateTimeTextProvider.LocaleStore a(Map var0) {
      var0.put(TextStyle.b, var0.get(TextStyle.a));
      var0.put(TextStyle.d, var0.get(TextStyle.c));
      if(var0.containsKey(TextStyle.e) && !var0.containsKey(TextStyle.f)) {
         var0.put(TextStyle.f, var0.get(TextStyle.e));
      }

      return new SimpleDateTimeTextProvider.LocaleStore(var0);
   }

   private Object b(TemporalField var1, Locale var2) {
      HashMap var3;
      Long var4;
      Long var6;
      Long var7;
      Long var8;
      Long var17;
      Object var18;
      Long var19;
      if(var1 == ChronoField.x) {
         DateFormatSymbols var14 = DateFormatSymbols.getInstance(var2);
         var3 = new HashMap();
         var17 = Long.valueOf(1L);
         Long var10 = Long.valueOf(2L);
         var19 = Long.valueOf(3L);
         var6 = Long.valueOf(4L);
         Long var11 = Long.valueOf(5L);
         var8 = Long.valueOf(6L);
         var7 = Long.valueOf(7L);
         Long var9 = Long.valueOf(8L);
         var4 = Long.valueOf(9L);
         Long var12 = Long.valueOf(10L);
         Long var5 = Long.valueOf(11L);
         Long var13 = Long.valueOf(12L);
         String[] var15 = var14.getMonths();
         HashMap var16 = new HashMap();
         var16.put(var17, var15[0]);
         var16.put(var10, var15[1]);
         var16.put(var19, var15[2]);
         var16.put(var6, var15[3]);
         var16.put(var11, var15[4]);
         var16.put(var8, var15[5]);
         var16.put(var7, var15[6]);
         var16.put(var9, var15[7]);
         var16.put(var4, var15[8]);
         var16.put(var12, var15[9]);
         var16.put(var5, var15[10]);
         var16.put(var13, var15[11]);
         var3.put(TextStyle.a, var16);
         var16 = new HashMap();
         var16.put(var17, var15[0].substring(0, 1));
         var16.put(var10, var15[1].substring(0, 1));
         var16.put(var19, var15[2].substring(0, 1));
         var16.put(var6, var15[3].substring(0, 1));
         var16.put(var11, var15[4].substring(0, 1));
         var16.put(var8, var15[5].substring(0, 1));
         var16.put(var7, var15[6].substring(0, 1));
         var16.put(var9, var15[7].substring(0, 1));
         var16.put(var4, var15[8].substring(0, 1));
         var16.put(var12, var15[9].substring(0, 1));
         var16.put(var5, var15[10].substring(0, 1));
         var16.put(var13, var15[11].substring(0, 1));
         var3.put(TextStyle.e, var16);
         String[] var34 = var14.getShortMonths();
         HashMap var35 = new HashMap();
         var35.put(var17, var34[0]);
         var35.put(var10, var34[1]);
         var35.put(var19, var34[2]);
         var35.put(var6, var34[3]);
         var35.put(var11, var34[4]);
         var35.put(var8, var34[5]);
         var35.put(var7, var34[6]);
         var35.put(var9, var34[7]);
         var35.put(var4, var34[8]);
         var35.put(var12, var34[9]);
         var35.put(var5, var34[10]);
         var35.put(var13, var34[11]);
         var3.put(TextStyle.c, var35);
         var18 = a(var3);
      } else if(var1 == ChronoField.p) {
         DateFormatSymbols var29 = DateFormatSymbols.getInstance(var2);
         HashMap var28 = new HashMap();
         var7 = Long.valueOf(1L);
         var19 = Long.valueOf(2L);
         Long var22 = Long.valueOf(3L);
         var6 = Long.valueOf(4L);
         var17 = Long.valueOf(5L);
         var4 = Long.valueOf(6L);
         var8 = Long.valueOf(7L);
         String[] var31 = var29.getWeekdays();
         HashMap var33 = new HashMap();
         var33.put(var7, var31[2]);
         var33.put(var19, var31[3]);
         var33.put(var22, var31[4]);
         var33.put(var6, var31[5]);
         var33.put(var17, var31[6]);
         var33.put(var4, var31[7]);
         var33.put(var8, var31[1]);
         var28.put(TextStyle.a, var33);
         var33 = new HashMap();
         var33.put(var7, var31[2].substring(0, 1));
         var33.put(var19, var31[3].substring(0, 1));
         var33.put(var22, var31[4].substring(0, 1));
         var33.put(var6, var31[5].substring(0, 1));
         var33.put(var17, var31[6].substring(0, 1));
         var33.put(var4, var31[7].substring(0, 1));
         var33.put(var8, var31[1].substring(0, 1));
         var28.put(TextStyle.e, var33);
         String[] var30 = var29.getShortWeekdays();
         HashMap var32 = new HashMap();
         var32.put(var7, var30[2]);
         var32.put(var19, var30[3]);
         var32.put(var22, var30[4]);
         var32.put(var6, var30[5]);
         var32.put(var17, var30[6]);
         var32.put(var4, var30[7]);
         var32.put(var8, var30[1]);
         var28.put(TextStyle.c, var32);
         var18 = a(var28);
      } else {
         HashMap var20;
         if(var1 == ChronoField.o) {
            DateFormatSymbols var21 = DateFormatSymbols.getInstance(var2);
            var20 = new HashMap();
            String[] var23 = var21.getAmPmStrings();
            var3 = new HashMap();
            var3.put(Long.valueOf(0L), var23[0]);
            var3.put(Long.valueOf(1L), var23[1]);
            var20.put(TextStyle.a, var3);
            var20.put(TextStyle.c, var3);
            var18 = a(var20);
         } else {
            HashMap var24;
            if(var1 == ChronoField.B) {
               DateFormatSymbols var25 = DateFormatSymbols.getInstance(var2);
               var20 = new HashMap();
               String[] var26 = var25.getEras();
               HashMap var27 = new HashMap();
               var27.put(Long.valueOf(0L), var26[0]);
               var27.put(Long.valueOf(1L), var26[1]);
               var20.put(TextStyle.c, var27);
               if(var2.getLanguage().equals(Locale.ENGLISH.getLanguage())) {
                  var24 = new HashMap();
                  var24.put(Long.valueOf(0L), "Before Christ");
                  var24.put(Long.valueOf(1L), "Anno Domini");
                  var20.put(TextStyle.a, var24);
               } else {
                  var20.put(TextStyle.a, var27);
               }

               var24 = new HashMap();
               var24.put(Long.valueOf(0L), var26[0].substring(0, 1));
               var24.put(Long.valueOf(1L), var26[1].substring(0, 1));
               var20.put(TextStyle.e, var24);
               var18 = a(var20);
            } else if(var1 == IsoFields.b) {
               var20 = new HashMap();
               var24 = new HashMap();
               var24.put(Long.valueOf(1L), "Q1");
               var24.put(Long.valueOf(2L), "Q2");
               var24.put(Long.valueOf(3L), "Q3");
               var24.put(Long.valueOf(4L), "Q4");
               var20.put(TextStyle.c, var24);
               var24 = new HashMap();
               var24.put(Long.valueOf(1L), "1st quarter");
               var24.put(Long.valueOf(2L), "2nd quarter");
               var24.put(Long.valueOf(3L), "3rd quarter");
               var24.put(Long.valueOf(4L), "4th quarter");
               var20.put(TextStyle.a, var24);
               var18 = a(var20);
            } else {
               var18 = "";
            }
         }
      }

      return var18;
   }

   private static Entry b(Object var0, Object var1) {
      return new SimpleImmutableEntry(var0, var1);
   }

   public String a(TemporalField var1, long var2, TextStyle var4, Locale var5) {
      Object var6 = this.a(var1, var5);
      String var7;
      if(var6 instanceof SimpleDateTimeTextProvider.LocaleStore) {
         var7 = ((SimpleDateTimeTextProvider.LocaleStore)var6).a(var2, var4);
      } else {
         var7 = null;
      }

      return var7;
   }

   public Iterator a(TemporalField var1, TextStyle var2, Locale var3) {
      Object var4 = this.a(var1, var3);
      Iterator var5;
      if(var4 instanceof SimpleDateTimeTextProvider.LocaleStore) {
         var5 = ((SimpleDateTimeTextProvider.LocaleStore)var4).a(var2);
      } else {
         var5 = null;
      }

      return var5;
   }

   static final class LocaleStore {
      private final Map a;
      private final Map b;

      LocaleStore(Map var1) {
         this.a = var1;
         HashMap var3 = new HashMap();
         ArrayList var2 = new ArrayList();
         Iterator var5 = var1.keySet().iterator();

         while(var5.hasNext()) {
            TextStyle var4 = (TextStyle)var5.next();
            HashMap var6 = new HashMap();
            Iterator var7 = ((Map)var1.get(var4)).entrySet().iterator();

            while(var7.hasNext()) {
               Entry var8 = (Entry)var7.next();
               if(var6.put(var8.getValue(), SimpleDateTimeTextProvider.b(var8.getValue(), var8.getKey())) != null) {
                  ;
               }
            }

            ArrayList var9 = new ArrayList(var6.values());
            Collections.sort(var9, SimpleDateTimeTextProvider.b);
            var3.put(var4, var9);
            var2.addAll(var9);
            var3.put((Object)null, var2);
         }

         Collections.sort(var2, SimpleDateTimeTextProvider.b);
         this.b = var3;
      }

      String a(long var1, TextStyle var3) {
         Map var4 = (Map)this.a.get(var3);
         String var5;
         if(var4 != null) {
            var5 = (String)var4.get(Long.valueOf(var1));
         } else {
            var5 = null;
         }

         return var5;
      }

      Iterator a(TextStyle var1) {
         List var2 = (List)this.b.get(var1);
         Iterator var3;
         if(var2 != null) {
            var3 = var2.iterator();
         } else {
            var3 = null;
         }

         return var3;
      }
   }
}
