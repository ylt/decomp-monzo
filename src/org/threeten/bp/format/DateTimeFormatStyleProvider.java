package org.threeten.bp.format;

import java.util.Locale;
import org.threeten.bp.chrono.Chronology;

abstract class DateTimeFormatStyleProvider {
   static DateTimeFormatStyleProvider a() {
      return new SimpleDateTimeFormatStyleProvider();
   }

   public abstract DateTimeFormatter a(FormatStyle var1, FormatStyle var2, Chronology var3, Locale var4);
}
