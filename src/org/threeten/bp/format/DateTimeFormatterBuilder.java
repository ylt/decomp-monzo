package org.threeten.bp.format;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.TreeMap;
import java.util.Map.Entry;
import org.threeten.bp.DateTimeException;
import org.threeten.bp.Instant;
import org.threeten.bp.LocalDate;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.ZoneId;
import org.threeten.bp.ZoneOffset;
import org.threeten.bp.chrono.ChronoLocalDate;
import org.threeten.bp.chrono.Chronology;
import org.threeten.bp.jdk8.Jdk8Methods;
import org.threeten.bp.temporal.ChronoField;
import org.threeten.bp.temporal.IsoFields;
import org.threeten.bp.temporal.TemporalAccessor;
import org.threeten.bp.temporal.TemporalField;
import org.threeten.bp.temporal.TemporalQueries;
import org.threeten.bp.temporal.TemporalQuery;
import org.threeten.bp.temporal.ValueRange;
import org.threeten.bp.temporal.WeekFields;

public final class DateTimeFormatterBuilder {
   static final Comparator a;
   private static final TemporalQuery b = new TemporalQuery() {
      public ZoneId a(TemporalAccessor var1) {
         ZoneId var2 = (ZoneId)var1.a(TemporalQueries.a());
         if(var2 == null || var2 instanceof ZoneOffset) {
            var2 = null;
         }

         return var2;
      }

      // $FF: synthetic method
      public Object b(TemporalAccessor var1) {
         return this.a(var1);
      }
   };
   private static final Map j = new HashMap();
   private DateTimeFormatterBuilder c = this;
   private final DateTimeFormatterBuilder d;
   private final List e = new ArrayList();
   private final boolean f;
   private int g;
   private char h;
   private int i = -1;

   static {
      j.put(Character.valueOf('G'), ChronoField.B);
      j.put(Character.valueOf('y'), ChronoField.z);
      j.put(Character.valueOf('u'), ChronoField.A);
      j.put(Character.valueOf('Q'), IsoFields.b);
      j.put(Character.valueOf('q'), IsoFields.b);
      j.put(Character.valueOf('M'), ChronoField.x);
      j.put(Character.valueOf('L'), ChronoField.x);
      j.put(Character.valueOf('D'), ChronoField.t);
      j.put(Character.valueOf('d'), ChronoField.s);
      j.put(Character.valueOf('F'), ChronoField.q);
      j.put(Character.valueOf('E'), ChronoField.p);
      j.put(Character.valueOf('c'), ChronoField.p);
      j.put(Character.valueOf('e'), ChronoField.p);
      j.put(Character.valueOf('a'), ChronoField.o);
      j.put(Character.valueOf('H'), ChronoField.m);
      j.put(Character.valueOf('k'), ChronoField.n);
      j.put(Character.valueOf('K'), ChronoField.k);
      j.put(Character.valueOf('h'), ChronoField.l);
      j.put(Character.valueOf('m'), ChronoField.i);
      j.put(Character.valueOf('s'), ChronoField.g);
      j.put(Character.valueOf('S'), ChronoField.a);
      j.put(Character.valueOf('A'), ChronoField.f);
      j.put(Character.valueOf('n'), ChronoField.a);
      j.put(Character.valueOf('N'), ChronoField.b);
      a = new Comparator() {
         public int a(String var1, String var2) {
            int var3;
            if(var1.length() == var2.length()) {
               var3 = var1.compareTo(var2);
            } else {
               var3 = var1.length() - var2.length();
            }

            return var3;
         }

         // $FF: synthetic method
         public int compare(Object var1, Object var2) {
            return this.a((String)var1, (String)var2);
         }
      };
   }

   public DateTimeFormatterBuilder() {
      this.d = null;
      this.f = false;
   }

   private DateTimeFormatterBuilder(DateTimeFormatterBuilder var1, boolean var2) {
      this.d = var1;
      this.f = var2;
   }

   private int a(DateTimeFormatterBuilder.DateTimePrinterParser var1) {
      Jdk8Methods.a(var1, (String)"pp");
      Object var2 = var1;
      if(this.c.g > 0) {
         var2 = var1;
         if(var1 != null) {
            var2 = new DateTimeFormatterBuilder.PadPrinterParserDecorator(var1, this.c.g, this.c.h);
         }

         this.c.g = 0;
         this.c.h = 0;
      }

      this.c.e.add(var2);
      this.c.i = -1;
      return this.c.e.size() - 1;
   }

   private DateTimeFormatterBuilder a(DateTimeFormatterBuilder.NumberPrinterParser var1) {
      if(this.c.i >= 0 && this.c.e.get(this.c.i) instanceof DateTimeFormatterBuilder.NumberPrinterParser) {
         int var2 = this.c.i;
         DateTimeFormatterBuilder.NumberPrinterParser var3 = (DateTimeFormatterBuilder.NumberPrinterParser)this.c.e.get(var2);
         if(var1.c == var1.d && var1.e == SignStyle.d) {
            var3 = var3.a(var1.d);
            this.a((DateTimeFormatterBuilder.DateTimePrinterParser)var1.a());
            this.c.i = var2;
            var1 = var3;
         } else {
            var3 = var3.a();
            this.c.i = this.a((DateTimeFormatterBuilder.DateTimePrinterParser)var1);
            var1 = var3;
         }

         this.c.e.set(var2, var1);
      } else {
         this.c.i = this.a((DateTimeFormatterBuilder.DateTimePrinterParser)var1);
      }

      return this;
   }

   private void a(char var1, int var2, TemporalField var3) {
      switch(var1) {
      case 'D':
         if(var2 == 1) {
            this.a(var3);
         } else {
            if(var2 > 3) {
               throw new IllegalArgumentException("Too many pattern letters: " + var1);
            }

            this.a(var3, var2);
         }
         break;
      case 'E':
      case 'G':
         switch(var2) {
         case 1:
         case 2:
         case 3:
            this.a(var3, TextStyle.c);
            return;
         case 4:
            this.a(var3, TextStyle.a);
            return;
         case 5:
            this.a(var3, TextStyle.e);
            return;
         default:
            throw new IllegalArgumentException("Too many pattern letters: " + var1);
         }
      case 'F':
         if(var2 != 1) {
            throw new IllegalArgumentException("Too many pattern letters: " + var1);
         }

         this.a(var3);
         break;
      case 'H':
      case 'K':
      case 'd':
      case 'h':
      case 'k':
      case 'm':
      case 's':
         if(var2 == 1) {
            this.a(var3);
         } else {
            if(var2 != 2) {
               throw new IllegalArgumentException("Too many pattern letters: " + var1);
            }

            this.a(var3, var2);
         }
         break;
      case 'L':
      case 'q':
         switch(var2) {
         case 1:
            this.a(var3);
            return;
         case 2:
            this.a(var3, 2);
            return;
         case 3:
            this.a(var3, TextStyle.d);
            return;
         case 4:
            this.a(var3, TextStyle.b);
            return;
         case 5:
            this.a(var3, TextStyle.f);
            return;
         default:
            throw new IllegalArgumentException("Too many pattern letters: " + var1);
         }
      case 'M':
      case 'Q':
         switch(var2) {
         case 1:
            this.a(var3);
            return;
         case 2:
            this.a(var3, 2);
            return;
         case 3:
            this.a(var3, TextStyle.c);
            return;
         case 4:
            this.a(var3, TextStyle.a);
            return;
         case 5:
            this.a(var3, TextStyle.e);
            return;
         default:
            throw new IllegalArgumentException("Too many pattern letters: " + var1);
         }
      case 'S':
         this.a(ChronoField.a, var2, var2, false);
         break;
      case 'a':
         if(var2 != 1) {
            throw new IllegalArgumentException("Too many pattern letters: " + var1);
         }

         this.a(var3, TextStyle.c);
         break;
      case 'c':
         switch(var2) {
         case 1:
            this.a((DateTimeFormatterBuilder.DateTimePrinterParser)(new DateTimeFormatterBuilder.WeekFieldsPrinterParser('c', var2)));
            return;
         case 2:
            throw new IllegalArgumentException("Invalid number of pattern letters: " + var1);
         case 3:
            this.a(var3, TextStyle.d);
            return;
         case 4:
            this.a(var3, TextStyle.b);
            return;
         case 5:
            this.a(var3, TextStyle.f);
            return;
         default:
            throw new IllegalArgumentException("Too many pattern letters: " + var1);
         }
      case 'e':
         switch(var2) {
         case 1:
         case 2:
            this.a((DateTimeFormatterBuilder.DateTimePrinterParser)(new DateTimeFormatterBuilder.WeekFieldsPrinterParser('e', var2)));
            return;
         case 3:
            this.a(var3, TextStyle.c);
            return;
         case 4:
            this.a(var3, TextStyle.a);
            return;
         case 5:
            this.a(var3, TextStyle.e);
            return;
         default:
            throw new IllegalArgumentException("Too many pattern letters: " + var1);
         }
      case 'u':
      case 'y':
         if(var2 == 2) {
            this.a(var3, 2, 2, (ChronoLocalDate)DateTimeFormatterBuilder.ReducedPrinterParser.g);
         } else if(var2 < 4) {
            this.a(var3, var2, 19, (SignStyle)SignStyle.a);
         } else {
            this.a(var3, var2, 19, (SignStyle)SignStyle.e);
         }
         break;
      default:
         if(var2 == 1) {
            this.a(var3);
         } else {
            this.a(var3, var2);
         }
      }

   }

   private void c(String var1) {
      for(int var5 = 0; var5 < var1.length(); ++var5) {
         char var2 = var1.charAt(var5);
         int var4;
         int var6;
         String var8;
         if(var2 >= 65 && var2 <= 90 || var2 >= 97 && var2 <= 122) {
            for(var4 = var5 + 1; var4 < var1.length() && var1.charAt(var4) == var2; ++var4) {
               ;
            }

            var5 = var4 - var5;
            if(var2 != 112) {
               var6 = var4;
               var4 = var5;
               var5 = var6;
            } else {
               label237: {
                  char var3;
                  label189: {
                     if(var4 < var1.length()) {
                        var3 = var1.charAt(var4);
                        if(var3 >= 65 && var3 <= 90) {
                           break label189;
                        }

                        var2 = var3;
                        if(var3 >= 97) {
                           var2 = var3;
                           if(var3 <= 122) {
                              break label189;
                           }
                        }
                     }

                     var6 = var5;
                     var5 = var4;
                     byte var7 = 0;
                     var4 = var6;
                     var6 = var7;
                     break label237;
                  }

                  for(var6 = var4 + 1; var6 < var1.length() && var1.charAt(var6) == var3; ++var6) {
                     ;
                  }

                  int var10 = var6;
                  var4 = var6 - var4;
                  var2 = var3;
                  var6 = var5;
                  var5 = var10;
               }

               if(var6 == 0) {
                  throw new IllegalArgumentException("Pad letter 'p' must be followed by valid pad pattern: " + var1);
               }

               this.a(var6);
            }

            TemporalField var11 = (TemporalField)j.get(Character.valueOf(var2));
            if(var11 != null) {
               this.a(var2, var4, var11);
            } else if(var2 == 122) {
               if(var4 > 4) {
                  throw new IllegalArgumentException("Too many pattern letters: " + var2);
               }

               if(var4 == 4) {
                  this.b(TextStyle.a);
               } else {
                  this.b(TextStyle.c);
               }
            } else if(var2 == 86) {
               if(var4 != 2) {
                  throw new IllegalArgumentException("Pattern letter count must be 2: " + var2);
               }

               this.f();
            } else if(var2 == 90) {
               if(var4 < 4) {
                  this.a("+HHMM", "+0000");
               } else if(var4 == 4) {
                  this.a(TextStyle.a);
               } else {
                  if(var4 != 5) {
                     throw new IllegalArgumentException("Too many pattern letters: " + var2);
                  }

                  this.a("+HH:MM:ss", "Z");
               }
            } else if(var2 == 79) {
               if(var4 == 1) {
                  this.a(TextStyle.c);
               } else {
                  if(var4 != 4) {
                     throw new IllegalArgumentException("Pattern letter count must be 1 or 4: " + var2);
                  }

                  this.a(TextStyle.a);
               }
            } else {
               byte var13;
               if(var2 == 88) {
                  if(var4 > 5) {
                     throw new IllegalArgumentException("Too many pattern letters: " + var2);
                  }

                  String[] var12 = DateTimeFormatterBuilder.OffsetIdPrinterParser.a;
                  if(var4 == 1) {
                     var13 = 0;
                  } else {
                     var13 = 1;
                  }

                  this.a(var12[var13 + var4], "Z");
               } else if(var2 == 120) {
                  if(var4 > 5) {
                     throw new IllegalArgumentException("Too many pattern letters: " + var2);
                  }

                  if(var4 == 1) {
                     var8 = "+00";
                  } else if(var4 % 2 == 0) {
                     var8 = "+0000";
                  } else {
                     var8 = "+00:00";
                  }

                  String[] var9 = DateTimeFormatterBuilder.OffsetIdPrinterParser.a;
                  if(var4 == 1) {
                     var13 = 0;
                  } else {
                     var13 = 1;
                  }

                  this.a(var9[var4 + var13], var8);
               } else if(var2 == 87) {
                  if(var4 > 1) {
                     throw new IllegalArgumentException("Too many pattern letters: " + var2);
                  }

                  this.a((DateTimeFormatterBuilder.DateTimePrinterParser)(new DateTimeFormatterBuilder.WeekFieldsPrinterParser('W', var4)));
               } else if(var2 == 119) {
                  if(var4 > 2) {
                     throw new IllegalArgumentException("Too many pattern letters: " + var2);
                  }

                  this.a((DateTimeFormatterBuilder.DateTimePrinterParser)(new DateTimeFormatterBuilder.WeekFieldsPrinterParser('w', var4)));
               } else {
                  if(var2 != 89) {
                     throw new IllegalArgumentException("Unknown pattern letter: " + var2);
                  }

                  this.a((DateTimeFormatterBuilder.DateTimePrinterParser)(new DateTimeFormatterBuilder.WeekFieldsPrinterParser('Y', var4)));
               }
            }

            --var5;
         } else if(var2 == 39) {
            for(var4 = var5 + 1; var4 < var1.length(); var4 = var6 + 1) {
               var6 = var4;
               if(var1.charAt(var4) == 39) {
                  if(var4 + 1 >= var1.length() || var1.charAt(var4 + 1) != 39) {
                     break;
                  }

                  var6 = var4 + 1;
               }
            }

            if(var4 >= var1.length()) {
               throw new IllegalArgumentException("Pattern ends with an incomplete string literal: " + var1);
            }

            var8 = var1.substring(var5 + 1, var4);
            if(var8.length() == 0) {
               this.a('\'');
            } else {
               this.a(var8.replace("''", "'"));
            }

            var5 = var4;
         } else if(var2 == 91) {
            this.h();
         } else if(var2 == 93) {
            if(this.c.d == null) {
               throw new IllegalArgumentException("Pattern invalid as it contains ] without previous [");
            }

            this.i();
         } else {
            if(var2 == 123 || var2 == 125 || var2 == 35) {
               throw new IllegalArgumentException("Pattern includes reserved character: '" + var2 + "'");
            }

            this.a(var2);
         }
      }

   }

   public DateTimeFormatter a(Locale var1) {
      Jdk8Methods.a(var1, (String)"locale");

      while(this.c.d != null) {
         this.i();
      }

      return new DateTimeFormatter(new DateTimeFormatterBuilder.CompositePrinterParser(this.e, false), var1, DecimalStyle.a, ResolverStyle.b, (Set)null, (Chronology)null, (ZoneId)null);
   }

   DateTimeFormatter a(ResolverStyle var1) {
      return this.j().a(var1);
   }

   public DateTimeFormatterBuilder a() {
      this.a((DateTimeFormatterBuilder.DateTimePrinterParser)DateTimeFormatterBuilder.SettingsParser.a);
      return this;
   }

   public DateTimeFormatterBuilder a(char var1) {
      this.a((DateTimeFormatterBuilder.DateTimePrinterParser)(new DateTimeFormatterBuilder.CharLiteralPrinterParser(var1)));
      return this;
   }

   public DateTimeFormatterBuilder a(int var1) {
      return this.a(var1, ' ');
   }

   public DateTimeFormatterBuilder a(int var1, char var2) {
      if(var1 < 1) {
         throw new IllegalArgumentException("The pad width must be at least one but was " + var1);
      } else {
         this.c.g = var1;
         this.c.h = var2;
         this.c.i = -1;
         return this;
      }
   }

   public DateTimeFormatterBuilder a(String var1) {
      Jdk8Methods.a(var1, (String)"literal");
      if(var1.length() > 0) {
         if(var1.length() == 1) {
            this.a((DateTimeFormatterBuilder.DateTimePrinterParser)(new DateTimeFormatterBuilder.CharLiteralPrinterParser(var1.charAt(0))));
         } else {
            this.a((DateTimeFormatterBuilder.DateTimePrinterParser)(new DateTimeFormatterBuilder.StringLiteralPrinterParser(var1)));
         }
      }

      return this;
   }

   public DateTimeFormatterBuilder a(String var1, String var2) {
      this.a((DateTimeFormatterBuilder.DateTimePrinterParser)(new DateTimeFormatterBuilder.OffsetIdPrinterParser(var2, var1)));
      return this;
   }

   public DateTimeFormatterBuilder a(DateTimeFormatter var1) {
      Jdk8Methods.a(var1, (String)"formatter");
      this.a((DateTimeFormatterBuilder.DateTimePrinterParser)var1.a(false));
      return this;
   }

   public DateTimeFormatterBuilder a(FormatStyle var1, FormatStyle var2) {
      if(var1 == null && var2 == null) {
         throw new IllegalArgumentException("Either the date or time style must be non-null");
      } else {
         this.a((DateTimeFormatterBuilder.DateTimePrinterParser)(new DateTimeFormatterBuilder.LocalizedPrinterParser(var1, var2)));
         return this;
      }
   }

   public DateTimeFormatterBuilder a(TextStyle var1) {
      Jdk8Methods.a(var1, (String)"style");
      if(var1 != TextStyle.a && var1 != TextStyle.c) {
         throw new IllegalArgumentException("Style must be either full or short");
      } else {
         this.a((DateTimeFormatterBuilder.DateTimePrinterParser)(new DateTimeFormatterBuilder.LocalizedOffsetPrinterParser(var1)));
         return this;
      }
   }

   public DateTimeFormatterBuilder a(TemporalField var1) {
      Jdk8Methods.a(var1, (String)"field");
      this.a(new DateTimeFormatterBuilder.NumberPrinterParser(var1, 1, 19, SignStyle.a));
      return this;
   }

   public DateTimeFormatterBuilder a(TemporalField var1, int var2) {
      Jdk8Methods.a(var1, (String)"field");
      if(var2 >= 1 && var2 <= 19) {
         this.a(new DateTimeFormatterBuilder.NumberPrinterParser(var1, var2, var2, SignStyle.d));
         return this;
      } else {
         throw new IllegalArgumentException("The width must be from 1 to 19 inclusive but was " + var2);
      }
   }

   public DateTimeFormatterBuilder a(TemporalField var1, int var2, int var3, ChronoLocalDate var4) {
      Jdk8Methods.a(var1, (String)"field");
      Jdk8Methods.a(var4, (String)"baseDate");
      this.a((DateTimeFormatterBuilder.NumberPrinterParser)(new DateTimeFormatterBuilder.ReducedPrinterParser(var1, var2, var3, 0, var4)));
      return this;
   }

   public DateTimeFormatterBuilder a(TemporalField var1, int var2, int var3, SignStyle var4) {
      DateTimeFormatterBuilder var5;
      if(var2 == var3 && var4 == SignStyle.d) {
         var5 = this.a(var1, var3);
         return var5;
      } else {
         Jdk8Methods.a(var1, (String)"field");
         Jdk8Methods.a(var4, (String)"signStyle");
         if(var2 >= 1 && var2 <= 19) {
            if(var3 >= 1 && var3 <= 19) {
               if(var3 < var2) {
                  throw new IllegalArgumentException("The maximum width must exceed or equal the minimum width but " + var3 + " < " + var2);
               } else {
                  this.a(new DateTimeFormatterBuilder.NumberPrinterParser(var1, var2, var3, var4));
                  var5 = this;
                  return var5;
               }
            } else {
               throw new IllegalArgumentException("The maximum width must be from 1 to 19 inclusive but was " + var3);
            }
         } else {
            throw new IllegalArgumentException("The minimum width must be from 1 to 19 inclusive but was " + var2);
         }
      }
   }

   public DateTimeFormatterBuilder a(TemporalField var1, int var2, int var3, boolean var4) {
      this.a((DateTimeFormatterBuilder.DateTimePrinterParser)(new DateTimeFormatterBuilder.FractionPrinterParser(var1, var2, var3, var4)));
      return this;
   }

   public DateTimeFormatterBuilder a(TemporalField var1, Map var2) {
      Jdk8Methods.a(var1, (String)"field");
      Jdk8Methods.a(var2, (String)"textLookup");
      LinkedHashMap var3 = new LinkedHashMap(var2);
      DateTimeTextProvider var4 = new DateTimeTextProvider(new SimpleDateTimeTextProvider.LocaleStore(Collections.singletonMap(TextStyle.a, var3))) {
         // $FF: synthetic field
         final SimpleDateTimeTextProvider.LocaleStore a;

         {
            this.a = var2;
         }

         public String a(TemporalField var1, long var2, TextStyle var4, Locale var5) {
            return this.a.a(var2, var4);
         }

         public Iterator a(TemporalField var1, TextStyle var2, Locale var3) {
            return this.a.a(var2);
         }
      };
      this.a((DateTimeFormatterBuilder.DateTimePrinterParser)(new DateTimeFormatterBuilder.TextPrinterParser(var1, TextStyle.a, var4)));
      return this;
   }

   public DateTimeFormatterBuilder a(TemporalField var1, TextStyle var2) {
      Jdk8Methods.a(var1, (String)"field");
      Jdk8Methods.a(var2, (String)"textStyle");
      this.a((DateTimeFormatterBuilder.DateTimePrinterParser)(new DateTimeFormatterBuilder.TextPrinterParser(var1, var2, DateTimeTextProvider.a())));
      return this;
   }

   public DateTimeFormatterBuilder b() {
      this.a((DateTimeFormatterBuilder.DateTimePrinterParser)DateTimeFormatterBuilder.SettingsParser.b);
      return this;
   }

   public DateTimeFormatterBuilder b(String var1) {
      Jdk8Methods.a(var1, (String)"pattern");
      this.c(var1);
      return this;
   }

   public DateTimeFormatterBuilder b(TextStyle var1) {
      this.a((DateTimeFormatterBuilder.DateTimePrinterParser)(new DateTimeFormatterBuilder.ZoneTextPrinterParser(var1)));
      return this;
   }

   public DateTimeFormatterBuilder c() {
      this.a((DateTimeFormatterBuilder.DateTimePrinterParser)DateTimeFormatterBuilder.SettingsParser.d);
      return this;
   }

   public DateTimeFormatterBuilder d() {
      this.a((DateTimeFormatterBuilder.DateTimePrinterParser)(new DateTimeFormatterBuilder.InstantPrinterParser(-2)));
      return this;
   }

   public DateTimeFormatterBuilder e() {
      this.a((DateTimeFormatterBuilder.DateTimePrinterParser)DateTimeFormatterBuilder.OffsetIdPrinterParser.b);
      return this;
   }

   public DateTimeFormatterBuilder f() {
      this.a((DateTimeFormatterBuilder.DateTimePrinterParser)(new DateTimeFormatterBuilder.ZoneIdPrinterParser(TemporalQueries.a(), "ZoneId()")));
      return this;
   }

   public DateTimeFormatterBuilder g() {
      this.a((DateTimeFormatterBuilder.DateTimePrinterParser)(new DateTimeFormatterBuilder.ZoneIdPrinterParser(b, "ZoneRegionId()")));
      return this;
   }

   public DateTimeFormatterBuilder h() {
      this.c.i = -1;
      this.c = new DateTimeFormatterBuilder(this.c, true);
      return this;
   }

   public DateTimeFormatterBuilder i() {
      if(this.c.d == null) {
         throw new IllegalStateException("Cannot call optionalEnd() as there was no previous call to optionalStart()");
      } else {
         if(this.c.e.size() > 0) {
            DateTimeFormatterBuilder.CompositePrinterParser var1 = new DateTimeFormatterBuilder.CompositePrinterParser(this.c.e, this.c.f);
            this.c = this.c.d;
            this.a((DateTimeFormatterBuilder.DateTimePrinterParser)var1);
         } else {
            this.c = this.c.d;
         }

         return this;
      }
   }

   public DateTimeFormatter j() {
      return this.a(Locale.getDefault());
   }

   static final class CharLiteralPrinterParser implements DateTimeFormatterBuilder.DateTimePrinterParser {
      private final char a;

      CharLiteralPrinterParser(char var1) {
         this.a = var1;
      }

      public int a(DateTimeParseContext var1, CharSequence var2, int var3) {
         if(var3 == var2.length()) {
            var3 = ~var3;
         } else {
            char var4 = var2.charAt(var3);
            if(!var1.a(this.a, var4)) {
               var3 = ~var3;
            } else {
               ++var3;
            }
         }

         return var3;
      }

      public boolean a(DateTimePrintContext var1, StringBuilder var2) {
         var2.append(this.a);
         return true;
      }

      public String toString() {
         String var1;
         if(this.a == 39) {
            var1 = "''";
         } else {
            var1 = "'" + this.a + "'";
         }

         return var1;
      }
   }

   static final class CompositePrinterParser implements DateTimeFormatterBuilder.DateTimePrinterParser {
      private final DateTimeFormatterBuilder.DateTimePrinterParser[] a;
      private final boolean b;

      CompositePrinterParser(List var1, boolean var2) {
         this((DateTimeFormatterBuilder.DateTimePrinterParser[])var1.toArray(new DateTimeFormatterBuilder.DateTimePrinterParser[var1.size()]), var2);
      }

      CompositePrinterParser(DateTimeFormatterBuilder.DateTimePrinterParser[] var1, boolean var2) {
         this.a = var1;
         this.b = var2;
      }

      public int a(DateTimeParseContext var1, CharSequence var2, int var3) {
         int var5 = 0;
         int var4;
         int var6;
         DateTimeFormatterBuilder.DateTimePrinterParser[] var7;
         if(this.b) {
            var1.g();
            var7 = this.a;
            var6 = var7.length;
            var5 = 0;
            var4 = var3;

            while(true) {
               if(var5 >= var6) {
                  var1.c(true);
                  var3 = var4;
                  break;
               }

               var4 = var7[var5].a(var1, var2, var4);
               if(var4 < 0) {
                  var1.c(false);
                  break;
               }

               ++var5;
            }
         } else {
            var7 = this.a;
            var6 = var7.length;

            while(true) {
               var4 = var3;
               if(var5 >= var6) {
                  break;
               }

               var3 = var7[var5].a(var1, var2, var3);
               if(var3 < 0) {
                  var4 = var3;
                  break;
               }

               ++var5;
            }

            var3 = var4;
         }

         return var3;
      }

      public DateTimeFormatterBuilder.CompositePrinterParser a(boolean var1) {
         DateTimeFormatterBuilder.CompositePrinterParser var2;
         if(var1 == this.b) {
            var2 = this;
         } else {
            var2 = new DateTimeFormatterBuilder.CompositePrinterParser(this.a, var1);
         }

         return var2;
      }

      public boolean a(DateTimePrintContext param1, StringBuilder param2) {
         // $FF: Couldn't be decompiled
      }

      public String toString() {
         StringBuilder var4 = new StringBuilder();
         if(this.a != null) {
            String var3;
            if(this.b) {
               var3 = "[";
            } else {
               var3 = "(";
            }

            var4.append(var3);
            DateTimeFormatterBuilder.DateTimePrinterParser[] var5 = this.a;
            int var2 = var5.length;

            for(int var1 = 0; var1 < var2; ++var1) {
               var4.append(var5[var1]);
            }

            if(this.b) {
               var3 = "]";
            } else {
               var3 = ")";
            }

            var4.append(var3);
         }

         return var4.toString();
      }
   }

   interface DateTimePrinterParser {
      int a(DateTimeParseContext var1, CharSequence var2, int var3);

      boolean a(DateTimePrintContext var1, StringBuilder var2);
   }

   static final class FractionPrinterParser implements DateTimeFormatterBuilder.DateTimePrinterParser {
      private final TemporalField a;
      private final int b;
      private final int c;
      private final boolean d;

      FractionPrinterParser(TemporalField var1, int var2, int var3, boolean var4) {
         Jdk8Methods.a(var1, (String)"field");
         if(!var1.a().a()) {
            throw new IllegalArgumentException("Field must have a fixed set of values: " + var1);
         } else if(var2 >= 0 && var2 <= 9) {
            if(var3 >= 1 && var3 <= 9) {
               if(var3 < var2) {
                  throw new IllegalArgumentException("Maximum width must exceed or equal the minimum width but " + var3 + " < " + var2);
               } else {
                  this.a = var1;
                  this.b = var2;
                  this.c = var3;
                  this.d = var4;
               }
            } else {
               throw new IllegalArgumentException("Maximum width must be from 1 to 9 inclusive but was " + var3);
            }
         } else {
            throw new IllegalArgumentException("Minimum width must be from 0 to 9 inclusive but was " + var2);
         }
      }

      private long a(BigDecimal var1) {
         ValueRange var2 = this.a.a();
         BigDecimal var3 = BigDecimal.valueOf(var2.b());
         return var1.multiply(BigDecimal.valueOf(var2.c()).subtract(var3).add(BigDecimal.ONE)).setScale(0, RoundingMode.FLOOR).add(var3).longValueExact();
      }

      private BigDecimal a(long var1) {
         ValueRange var4 = this.a.a();
         var4.a(var1, this.a);
         BigDecimal var3 = BigDecimal.valueOf(var4.b());
         BigDecimal var5 = BigDecimal.valueOf(var4.c()).subtract(var3).add(BigDecimal.ONE);
         var3 = BigDecimal.valueOf(var1).subtract(var3).divide(var5, 9, RoundingMode.FLOOR);
         if(var3.compareTo(BigDecimal.ZERO) == 0) {
            var3 = BigDecimal.ZERO;
         } else {
            var3 = var3.stripTrailingZeros();
         }

         return var3;
      }

      public int a(DateTimeParseContext var1, CharSequence var2, int var3) {
         byte var7 = 0;
         int var6;
         if(var1.f()) {
            var6 = this.b;
         } else {
            var6 = 0;
         }

         int var5;
         if(var1.f()) {
            var5 = this.c;
         } else {
            var5 = 9;
         }

         int var9 = var2.length();
         if(var3 == var9) {
            var5 = var3;
            if(var6 > 0) {
               var5 = ~var3;
            }
         } else {
            if(this.d) {
               if(var2.charAt(var3) != var1.c().d()) {
                  var5 = var3;
                  if(var6 > 0) {
                     var5 = ~var3;
                  }

                  return var5;
               }

               ++var3;
            }

            int var8 = var3 + var6;
            if(var8 > var9) {
               var5 = ~var3;
            } else {
               var9 = Math.min(var3 + var5, var9);
               var5 = var3;
               var6 = var7;

               int var12;
               while(true) {
                  var12 = var5;
                  if(var5 >= var9) {
                     break;
                  }

                  var12 = var5 + 1;
                  char var4 = var2.charAt(var5);
                  var5 = var1.c().a(var4);
                  if(var5 < 0) {
                     if(var12 < var8) {
                        var5 = ~var3;
                        return var5;
                     }

                     --var12;
                     break;
                  }

                  var6 = var6 * 10 + var5;
                  var5 = var12;
               }

               long var10 = this.a((new BigDecimal(var6)).movePointLeft(var12 - var3));
               var5 = var1.a(this.a, var10, var3, var12);
            }
         }

         return var5;
      }

      public boolean a(DateTimePrintContext var1, StringBuilder var2) {
         byte var4 = 0;
         boolean var5 = false;
         Long var6 = var1.a(this.a);
         if(var6 != null) {
            DecimalStyle var7 = var1.c();
            BigDecimal var8 = this.a(var6.longValue());
            if(var8.scale() == 0) {
               if(this.b > 0) {
                  int var3 = var4;
                  if(this.d) {
                     var2.append(var7.d());
                     var3 = var4;
                  }

                  while(var3 < this.b) {
                     var2.append(var7.a());
                     ++var3;
                  }
               }
            } else {
               String var9 = var7.a(var8.setScale(Math.min(Math.max(var8.scale(), this.b), this.c), RoundingMode.FLOOR).toPlainString().substring(2));
               if(this.d) {
                  var2.append(var7.d());
               }

               var2.append(var9);
            }

            var5 = true;
         }

         return var5;
      }

      public String toString() {
         String var1;
         if(this.d) {
            var1 = ",DecimalPoint";
         } else {
            var1 = "";
         }

         return "Fraction(" + this.a + "," + this.b + "," + this.c + var1 + ")";
      }
   }

   static final class InstantPrinterParser implements DateTimeFormatterBuilder.DateTimePrinterParser {
      private final int a;

      InstantPrinterParser(int var1) {
         this.a = var1;
      }

      public int a(DateTimeParseContext var1, CharSequence var2, int var3) {
         DateTimeParseContext var17 = var1.a();
         int var4;
         if(this.a < 0) {
            var4 = 0;
         } else {
            var4 = this.a;
         }

         int var5;
         if(this.a < 0) {
            var5 = 9;
         } else {
            var5 = this.a;
         }

         int var8 = (new DateTimeFormatterBuilder()).a(DateTimeFormatter.a).a('T').a(ChronoField.m, 2).a(':').a(ChronoField.i, 2).a(':').a(ChronoField.g, 2).a(ChronoField.a, var4, var5, true).a('Z').j().a(false).a(var17, var2, var3);
         if(var8 < 0) {
            var3 = var8;
         } else {
            long var15 = var17.a((TemporalField)ChronoField.A).longValue();
            int var9 = var17.a((TemporalField)ChronoField.x).intValue();
            int var10 = var17.a((TemporalField)ChronoField.s).intValue();
            var5 = var17.a((TemporalField)ChronoField.m).intValue();
            int var11 = var17.a((TemporalField)ChronoField.i).intValue();
            Long var19 = var17.a((TemporalField)ChronoField.g);
            Long var20 = var17.a((TemporalField)ChronoField.a);
            if(var19 != null) {
               var4 = var19.intValue();
            } else {
               var4 = 0;
            }

            int var7;
            if(var20 != null) {
               var7 = var20.intValue();
            } else {
               var7 = 0;
            }

            int var12 = (int)var15;
            byte var6;
            if(var5 == 24 && var11 == 0 && var4 == 0 && var7 == 0) {
               var6 = 1;
               var5 = 0;
            } else if(var5 == 23 && var11 == 59 && var4 == 60) {
               var1.h();
               var4 = 59;
               var6 = 0;
            } else {
               var6 = 0;
            }

            long var13;
            try {
               var13 = LocalDateTime.a(var12 % 10000, var9, var10, var5, var11, var4, 0).a((long)var6).c((ZoneOffset)ZoneOffset.d);
               var15 = Jdk8Methods.d(var15 / 10000L, 315569520000L);
            } catch (RuntimeException var18) {
               var3 = ~var3;
               return var3;
            }

            var4 = var1.a((TemporalField)ChronoField.C, var15 + var13, var3, var8);
            var3 = var1.a((TemporalField)ChronoField.a, (long)var7, var3, var4);
         }

         return var3;
      }

      public boolean a(DateTimePrintContext var1, StringBuilder var2) {
         Long var13 = var1.a((TemporalField)ChronoField.C);
         Long var12 = Long.valueOf(0L);
         if(var1.a().a((TemporalField)ChronoField.a)) {
            var12 = Long.valueOf(var1.a().d(ChronoField.a));
         }

         boolean var7;
         if(var13 == null) {
            var7 = false;
         } else {
            long var8 = var13.longValue();
            int var3 = ChronoField.a.b(var12.longValue());
            int var4;
            long var10;
            LocalDateTime var14;
            if(var8 >= -62167219200L) {
               var10 = var8 - 315569520000L + 62167219200L;
               var8 = Jdk8Methods.e(var10, 315569520000L) + 1L;
               var14 = LocalDateTime.a(Jdk8Methods.f(var10, 315569520000L) - 62167219200L, 0, ZoneOffset.d);
               if(var8 > 0L) {
                  var2.append('+').append(var8);
               }

               var2.append(var14);
               if(var14.h() == 0) {
                  var2.append(":00");
               }
            } else {
               var10 = 62167219200L + var8;
               var8 = var10 / 315569520000L;
               var10 %= 315569520000L;
               var14 = LocalDateTime.a(var10 - 62167219200L, 0, ZoneOffset.d);
               var4 = var2.length();
               var2.append(var14);
               if(var14.h() == 0) {
                  var2.append(":00");
               }

               if(var8 < 0L) {
                  if(var14.b() == -10000) {
                     var2.replace(var4, var4 + 2, Long.toString(var8 - 1L));
                  } else if(var10 == 0L) {
                     var2.insert(var4, var8);
                  } else {
                     var2.insert(var4 + 1, Math.abs(var8));
                  }
               }
            }

            if(this.a == -2) {
               if(var3 != 0) {
                  var2.append('.');
                  if(var3 % 1000000 == 0) {
                     var2.append(Integer.toString(var3 / 1000000 + 1000).substring(1));
                  } else if(var3 % 1000 == 0) {
                     var2.append(Integer.toString(var3 / 1000 + 1000000).substring(1));
                  } else {
                     var2.append(Integer.toString(1000000000 + var3).substring(1));
                  }
               }
            } else if(this.a > 0 || this.a == -1 && var3 > 0) {
               var2.append('.');
               var4 = 100000000;

               for(int var5 = 0; this.a == -1 && var3 > 0 || var5 < this.a; ++var5) {
                  int var6 = var3 / var4;
                  var2.append((char)(var6 + 48));
                  var3 -= var6 * var4;
                  var4 /= 10;
               }
            }

            var2.append('Z');
            var7 = true;
         }

         return var7;
      }

      public String toString() {
         return "Instant()";
      }
   }

   static final class LocalizedOffsetPrinterParser implements DateTimeFormatterBuilder.DateTimePrinterParser {
      private final TextStyle a;

      public LocalizedOffsetPrinterParser(TextStyle var1) {
         this.a = var1;
      }

      public int a(DateTimeParseContext var1, CharSequence var2, int var3) {
         if(!var1.a(var2, var3, "GMT", 0, 3)) {
            var3 = ~var3;
         } else {
            int var4 = var3 + 3;
            if(this.a == TextStyle.a) {
               var3 = (new DateTimeFormatterBuilder.OffsetIdPrinterParser("", "+HH:MM:ss")).a(var1, var2, var4);
            } else {
               int var8 = var2.length();
               if(var4 == var8) {
                  var3 = var1.a((TemporalField)ChronoField.D, 0L, var4, var4);
               } else {
                  char var10 = var2.charAt(var4);
                  if(var10 != 43 && var10 != 45) {
                     var3 = var1.a((TemporalField)ChronoField.D, 0L, var4, var4);
                  } else {
                     byte var12;
                     if(var10 == 45) {
                        var12 = -1;
                     } else {
                        var12 = 1;
                     }

                     if(var4 == var8) {
                        var3 = ~var4;
                     } else {
                        int var5 = var4 + 1;
                        char var11 = var2.charAt(var5);
                        if(var11 >= 48 && var11 <= 57) {
                           int var6 = var5 + 1;
                           int var7 = var11 - 48;
                           var4 = var7;
                           var5 = var6;
                           if(var6 != var8) {
                              char var9 = var2.charAt(var6);
                              var4 = var7;
                              var5 = var6;
                              if(var9 >= 48) {
                                 var4 = var7;
                                 var5 = var6;
                                 if(var9 <= 57) {
                                    var4 = var7 * 10 + (var9 - 48);
                                    if(var4 > 23) {
                                       var3 = ~var6;
                                       return var3;
                                    }

                                    var5 = var6 + 1;
                                 }
                              }
                           }

                           if(var5 != var8 && var2.charAt(var5) == 58) {
                              var6 = var5 + 1;
                              if(var6 > var8 - 2) {
                                 var3 = ~var6;
                              } else {
                                 char var13 = var2.charAt(var6);
                                 if(var13 >= 48 && var13 <= 57) {
                                    ++var6;
                                    char var14 = var2.charAt(var6);
                                    if(var14 >= 48 && var14 <= 57) {
                                       ++var6;
                                       var5 = (var13 - 48) * 10 + (var14 - 48);
                                       if(var5 > 59) {
                                          var3 = ~var6;
                                       } else if(var6 != var8 && var2.charAt(var6) == 58) {
                                          var7 = var6 + 1;
                                          if(var7 > var8 - 2) {
                                             var3 = ~var7;
                                          } else {
                                             char var15 = var2.charAt(var7);
                                             if(var15 >= 48 && var15 <= 57) {
                                                ++var7;
                                                char var16 = var2.charAt(var7);
                                                if(var16 >= 48 && var16 <= 57) {
                                                   ++var7;
                                                   var6 = (var15 - 48) * 10 + (var16 - 48);
                                                   if(var6 > 59) {
                                                      var3 = ~var7;
                                                   } else {
                                                      var3 = var1.a((TemporalField)ChronoField.D, (long)((var4 * 3600 + var5 * 60 + var6) * var12), var7, var7);
                                                   }
                                                } else {
                                                   var3 = ~var7;
                                                }
                                             } else {
                                                var3 = ~var7;
                                             }
                                          }
                                       } else {
                                          var3 = var1.a((TemporalField)ChronoField.D, (long)((var4 * 3600 + var5 * 60) * var12), var6, var6);
                                       }
                                    } else {
                                       var3 = ~var6;
                                    }
                                 } else {
                                    var3 = ~var6;
                                 }
                              }
                           } else {
                              var3 = var1.a((TemporalField)ChronoField.D, (long)(var4 * var12 * 3600), var5, var5);
                           }
                        } else {
                           var3 = ~var5;
                        }
                     }
                  }
               }
            }
         }

         return var3;
      }

      public boolean a(DateTimePrintContext var1, StringBuilder var2) {
         Long var8 = var1.a((TemporalField)ChronoField.D);
         boolean var7;
         if(var8 == null) {
            var7 = false;
         } else {
            var2.append("GMT");
            if(this.a == TextStyle.a) {
               var7 = (new DateTimeFormatterBuilder.OffsetIdPrinterParser("", "+HH:MM:ss")).a(var1, var2);
            } else {
               int var4 = Jdk8Methods.a(var8.longValue());
               if(var4 != 0) {
                  int var5 = Math.abs(var4 / 3600 % 100);
                  int var3 = Math.abs(var4 / 60 % 60);
                  int var6 = Math.abs(var4 % 60);
                  String var9;
                  if(var4 < 0) {
                     var9 = "-";
                  } else {
                     var9 = "+";
                  }

                  var2.append(var9).append(var5);
                  if(var3 > 0 || var6 > 0) {
                     var2.append(":").append((char)(var3 / 10 + 48)).append((char)(var3 % 10 + 48));
                     if(var6 > 0) {
                        var2.append(":").append((char)(var6 / 10 + 48)).append((char)(var6 % 10 + 48));
                     }
                  }
               }

               var7 = true;
            }
         }

         return var7;
      }
   }

   static final class LocalizedPrinterParser implements DateTimeFormatterBuilder.DateTimePrinterParser {
      private final FormatStyle a;
      private final FormatStyle b;

      LocalizedPrinterParser(FormatStyle var1, FormatStyle var2) {
         this.a = var1;
         this.b = var2;
      }

      private DateTimeFormatter a(Locale var1, Chronology var2) {
         return DateTimeFormatStyleProvider.a().a(this.a, this.b, var2, var1);
      }

      public int a(DateTimeParseContext var1, CharSequence var2, int var3) {
         Chronology var4 = var1.d();
         return this.a(var1.b(), var4).a(false).a(var1, var2, var3);
      }

      public boolean a(DateTimePrintContext var1, StringBuilder var2) {
         Chronology var3 = Chronology.a(var1.a());
         return this.a(var1.b(), var3).a(false).a(var1, var2);
      }

      public String toString() {
         StringBuilder var2 = (new StringBuilder()).append("Localized(");
         Object var1;
         if(this.a != null) {
            var1 = this.a;
         } else {
            var1 = "";
         }

         var2 = var2.append(var1).append(",");
         if(this.b != null) {
            var1 = this.b;
         } else {
            var1 = "";
         }

         return var2.append(var1).append(")").toString();
      }
   }

   static class NumberPrinterParser implements DateTimeFormatterBuilder.DateTimePrinterParser {
      static final int[] a = new int[]{0, 10, 100, 1000, 10000, 100000, 1000000, 10000000, 100000000, 1000000000};
      final TemporalField b;
      final int c;
      final int d;
      final SignStyle e;
      final int f;

      NumberPrinterParser(TemporalField var1, int var2, int var3, SignStyle var4) {
         this.b = var1;
         this.c = var2;
         this.d = var3;
         this.e = var4;
         this.f = 0;
      }

      private NumberPrinterParser(TemporalField var1, int var2, int var3, SignStyle var4, int var5) {
         this.b = var1;
         this.c = var2;
         this.d = var3;
         this.e = var4;
         this.f = var5;
      }

      // $FF: synthetic method
      NumberPrinterParser(TemporalField var1, int var2, int var3, SignStyle var4, int var5, Object var6) {
         this(var1, var2, var3, var4, var5);
      }

      int a(DateTimeParseContext var1, long var2, int var4, int var5) {
         return var1.a(this.b, var2, var4, var5);
      }

      public int a(DateTimeParseContext var1, CharSequence var2, int var3) {
         int var12 = var2.length();
         if(var3 == var12) {
            var3 = ~var3;
         } else {
            char var5 = var2.charAt(var3);
            boolean var6;
            boolean var14;
            boolean var15;
            SignStyle var20;
            boolean var23;
            if(var5 == var1.c().b()) {
               var20 = this.e;
               var15 = var1.f();
               if(this.c == this.d) {
                  var14 = true;
               } else {
                  var14 = false;
               }

               if(!var20.a(true, var15, var14)) {
                  var3 = ~var3;
                  return var3;
               }

               var23 = true;
               var6 = false;
               ++var3;
            } else if(var5 == var1.c().c()) {
               var20 = this.e;
               var15 = var1.f();
               if(this.c == this.d) {
                  var14 = true;
               } else {
                  var14 = false;
               }

               if(!var20.a(false, var15, var14)) {
                  var3 = ~var3;
                  return var3;
               }

               ++var3;
               var23 = false;
               var6 = true;
            } else {
               if(this.e == SignStyle.b && var1.f()) {
                  var3 = ~var3;
                  return var3;
               }

               var23 = false;
               var6 = false;
            }

            int var9;
            if(!var1.f() && !this.a(var1)) {
               var9 = 1;
            } else {
               var9 = this.c;
            }

            int var13 = var3 + var9;
            if(var13 > var12) {
               var3 = ~var3;
            } else {
               int var7;
               if(!var1.f() && !this.a(var1)) {
                  var7 = 9;
               } else {
                  var7 = this.d;
               }

               int var11 = var7 + Math.max(this.f, 0);
               int var10 = 0;

               int var8;
               long var16;
               BigInteger var26;
               while(true) {
                  long var18 = 0L;
                  var26 = null;
                  BigInteger var21 = null;
                  var8 = var3;
                  var16 = var18;
                  if(var10 >= 2) {
                     break;
                  }

                  var11 = Math.min(var11 + var3, var12);
                  var8 = var3;

                  while(true) {
                     var7 = var8;
                     if(var8 >= var11) {
                        break;
                     }

                     var7 = var8 + 1;
                     char var4 = var2.charAt(var8);
                     var8 = var1.c().a(var4);
                     if(var8 < 0) {
                        var8 = var7 - 1;
                        var7 = var8;
                        if(var8 < var13) {
                           var3 = ~var3;
                           return var3;
                        }
                        break;
                     }

                     if(var7 - var3 > 18) {
                        var26 = var21;
                        if(var21 == null) {
                           var26 = BigInteger.valueOf(var18);
                        }

                        var21 = var26.multiply(BigInteger.TEN).add(BigInteger.valueOf((long)var8));
                     } else {
                        var18 = var18 * 10L + (long)var8;
                     }

                     var8 = var7;
                  }

                  var26 = var21;
                  var8 = var7;
                  var16 = var18;
                  if(this.f <= 0) {
                     break;
                  }

                  var26 = var21;
                  var8 = var7;
                  var16 = var18;
                  if(var10 != 0) {
                     break;
                  }

                  var11 = Math.max(var9, var7 - var3 - this.f);
                  ++var10;
               }

               BigInteger var22;
               if(var6) {
                  if(var26 != null) {
                     if(var26.equals(BigInteger.ZERO) && var1.f()) {
                        var3 = ~(var3 - 1);
                        return var3;
                     }

                     var22 = var26.negate();
                  } else {
                     if(var16 == 0L && var1.f()) {
                        var3 = ~(var3 - 1);
                        return var3;
                     }

                     var16 = -var16;
                     var22 = var26;
                  }
               } else {
                  if(this.e == SignStyle.e && var1.f()) {
                     int var25 = var8 - var3;
                     if(var23) {
                        if(var25 <= this.c) {
                           var3 = ~(var3 - 1);
                           return var3;
                        }
                     } else if(var25 > this.c) {
                        var3 = ~var3;
                        return var3;
                     }
                  }

                  var22 = var26;
               }

               if(var22 != null) {
                  var26 = var22;
                  int var24 = var8;
                  if(var22.bitLength() > 63) {
                     var26 = var22.divide(BigInteger.TEN);
                     var24 = var8 - 1;
                  }

                  var3 = this.a(var1, var26.longValue(), var3, var24);
               } else {
                  var3 = this.a(var1, var16, var3, var8);
               }
            }
         }

         return var3;
      }

      long a(DateTimePrintContext var1, long var2) {
         return var2;
      }

      DateTimeFormatterBuilder.NumberPrinterParser a() {
         DateTimeFormatterBuilder.NumberPrinterParser var1;
         if(this.f == -1) {
            var1 = this;
         } else {
            var1 = new DateTimeFormatterBuilder.NumberPrinterParser(this.b, this.c, this.d, this.e, -1);
         }

         return var1;
      }

      DateTimeFormatterBuilder.NumberPrinterParser a(int var1) {
         return new DateTimeFormatterBuilder.NumberPrinterParser(this.b, this.c, this.d, this.e, this.f + var1);
      }

      boolean a(DateTimeParseContext var1) {
         boolean var2;
         if(this.f != -1 && (this.f <= 0 || this.c != this.d || this.e != SignStyle.d)) {
            var2 = false;
         } else {
            var2 = true;
         }

         return var2;
      }

      public boolean a(DateTimePrintContext var1, StringBuilder var2) {
         boolean var6 = false;
         Long var7 = var1.a(this.b);
         if(var7 != null) {
            long var4 = this.a(var1, var7.longValue());
            DecimalStyle var9 = var1.c();
            String var8;
            if(var4 == Long.MIN_VALUE) {
               var8 = "9223372036854775808";
            } else {
               var8 = Long.toString(Math.abs(var4));
            }

            if(var8.length() > this.d) {
               throw new DateTimeException("Field " + this.b + " cannot be printed as the value " + var4 + " exceeds the maximum print width of " + this.d);
            }

            var8 = var9.a(var8);
            if(var4 >= 0L) {
               switch(null.a[this.e.ordinal()]) {
               case 1:
                  if(this.c < 19 && var4 >= (long)a[this.c]) {
                     var2.append(var9.b());
                  }
                  break;
               case 2:
                  var2.append(var9.b());
               }
            } else {
               switch(null.a[this.e.ordinal()]) {
               case 1:
               case 2:
               case 3:
                  var2.append(var9.c());
                  break;
               case 4:
                  throw new DateTimeException("Field " + this.b + " cannot be printed as the value " + var4 + " cannot be negative according to the SignStyle");
               }
            }

            for(int var3 = 0; var3 < this.c - var8.length(); ++var3) {
               var2.append(var9.a());
            }

            var2.append(var8);
            var6 = true;
         }

         return var6;
      }

      public String toString() {
         String var1;
         if(this.c == 1 && this.d == 19 && this.e == SignStyle.a) {
            var1 = "Value(" + this.b + ")";
         } else if(this.c == this.d && this.e == SignStyle.d) {
            var1 = "Value(" + this.b + "," + this.c + ")";
         } else {
            var1 = "Value(" + this.b + "," + this.c + "," + this.d + "," + this.e + ")";
         }

         return var1;
      }
   }

   static final class OffsetIdPrinterParser implements DateTimeFormatterBuilder.DateTimePrinterParser {
      static final String[] a = new String[]{"+HH", "+HHmm", "+HH:mm", "+HHMM", "+HH:MM", "+HHMMss", "+HH:MM:ss", "+HHMMSS", "+HH:MM:SS"};
      static final DateTimeFormatterBuilder.OffsetIdPrinterParser b = new DateTimeFormatterBuilder.OffsetIdPrinterParser("Z", "+HH:MM:ss");
      private final String c;
      private final int d;

      OffsetIdPrinterParser(String var1, String var2) {
         Jdk8Methods.a(var1, (String)"noOffsetText");
         Jdk8Methods.a(var2, (String)"pattern");
         this.c = var1;
         this.d = this.a(var2);
      }

      private int a(String var1) {
         for(int var2 = 0; var2 < a.length; ++var2) {
            if(a[var2].equals(var1)) {
               return var2;
            }
         }

         throw new IllegalArgumentException("Invalid zone offset pattern: " + var1);
      }

      private boolean a(int[] var1, int var2, CharSequence var3, boolean var4) {
         boolean var8;
         if((this.d + 3) / 2 < var2) {
            var8 = false;
         } else {
            int var6 = var1[0];
            int var5 = var6;
            if(this.d % 2 == 0) {
               var5 = var6;
               if(var2 > 1) {
                  var8 = var4;
                  if(var6 + 1 > var3.length()) {
                     return var8;
                  }

                  var8 = var4;
                  if(var3.charAt(var6) != 58) {
                     return var8;
                  }

                  var5 = var6 + 1;
               }
            }

            var8 = var4;
            if(var5 + 2 <= var3.length()) {
               var6 = var5 + 1;
               char var7 = var3.charAt(var5);
               char var9 = var3.charAt(var6);
               var8 = var4;
               if(var7 >= 48) {
                  var8 = var4;
                  if(var7 <= 57) {
                     var8 = var4;
                     if(var9 >= 48) {
                        var8 = var4;
                        if(var9 <= 57) {
                           var5 = (var7 - 48) * 10 + (var9 - 48);
                           var8 = var4;
                           if(var5 >= 0) {
                              var8 = var4;
                              if(var5 <= 59) {
                                 var1[var2] = var5;
                                 var1[0] = var6 + 1;
                                 var8 = false;
                              }
                           }
                        }
                     }
                  }
               }
            }
         }

         return var8;
      }

      public int a(DateTimeParseContext var1, CharSequence var2, int var3) {
         int var4 = var2.length();
         int var6 = this.c.length();
         if(var6 == 0) {
            if(var3 == var4) {
               var3 = var1.a((TemporalField)ChronoField.D, 0L, var3, var3);
               return var3;
            }
         } else {
            if(var3 == var4) {
               var3 = ~var3;
               return var3;
            }

            if(var1.a(var2, var3, this.c, 0, var6)) {
               var3 = var1.a((TemporalField)ChronoField.D, 0L, var3, var6 + var3);
               return var3;
            }
         }

         char var17 = var2.charAt(var3);
         if(var17 == 43 || var17 == 45) {
            byte var18;
            if(var17 == 45) {
               var18 = -1;
            } else {
               var18 = 1;
            }

            boolean var5;
            int[] var16;
            label40: {
               var16 = new int[4];
               var16[0] = var3 + 1;
               if(!this.a(var16, 1, var2, true)) {
                  boolean var15;
                  if(this.d >= 3) {
                     var15 = true;
                  } else {
                     var15 = false;
                  }

                  if(!this.a(var16, 2, var2, var15) && !this.a(var16, 3, var2, false)) {
                     var5 = false;
                     break label40;
                  }
               }

               var5 = true;
            }

            if(!var5) {
               long var13 = (long)var18;
               long var9 = (long)var16[1];
               long var11 = (long)var16[2];
               long var7 = (long)var16[3];
               var3 = var1.a((TemporalField)ChronoField.D, (var9 * 3600L + var11 * 60L + var7) * var13, var3, var16[0]);
               return var3;
            }
         }

         if(var6 == 0) {
            var3 = var1.a((TemporalField)ChronoField.D, 0L, var3, var6 + var3);
         } else {
            var3 = ~var3;
         }

         return var3;
      }

      public boolean a(DateTimePrintContext var1, StringBuilder var2) {
         Long var9 = var1.a((TemporalField)ChronoField.D);
         boolean var8;
         if(var9 == null) {
            var8 = false;
         } else {
            int var3 = Jdk8Methods.a(var9.longValue());
            if(var3 == 0) {
               var2.append(this.c);
            } else {
               int var4 = Math.abs(var3 / 3600 % 100);
               int var7 = Math.abs(var3 / 60 % 60);
               int var6 = Math.abs(var3 % 60);
               int var5 = var2.length();
               String var10;
               if(var3 < 0) {
                  var10 = "-";
               } else {
                  var10 = "+";
               }

               label55: {
                  var2.append(var10).append((char)(var4 / 10 + 48)).append((char)(var4 % 10 + 48));
                  if(this.d < 3) {
                     var3 = var4;
                     if(this.d < 1) {
                        break label55;
                     }

                     var3 = var4;
                     if(var7 <= 0) {
                        break label55;
                     }
                  }

                  if(this.d % 2 == 0) {
                     var10 = ":";
                  } else {
                     var10 = "";
                  }

                  var2.append(var10).append((char)(var7 / 10 + 48)).append((char)(var7 % 10 + 48));
                  var4 += var7;
                  if(this.d < 7) {
                     var3 = var4;
                     if(this.d < 5) {
                        break label55;
                     }

                     var3 = var4;
                     if(var6 <= 0) {
                        break label55;
                     }
                  }

                  if(this.d % 2 == 0) {
                     var10 = ":";
                  } else {
                     var10 = "";
                  }

                  var2.append(var10).append((char)(var6 / 10 + 48)).append((char)(var6 % 10 + 48));
                  var3 = var4 + var6;
               }

               if(var3 == 0) {
                  var2.setLength(var5);
                  var2.append(this.c);
               }
            }

            var8 = true;
         }

         return var8;
      }

      public String toString() {
         String var1 = this.c.replace("'", "''");
         return "Offset(" + a[this.d] + ",'" + var1 + "')";
      }
   }

   static final class PadPrinterParserDecorator implements DateTimeFormatterBuilder.DateTimePrinterParser {
      private final DateTimeFormatterBuilder.DateTimePrinterParser a;
      private final int b;
      private final char c;

      PadPrinterParserDecorator(DateTimeFormatterBuilder.DateTimePrinterParser var1, int var2, char var3) {
         this.a = var1;
         this.b = var2;
         this.c = var3;
      }

      public int a(DateTimeParseContext var1, CharSequence var2, int var3) {
         boolean var8 = var1.f();
         boolean var7 = var1.e();
         if(var3 > var2.length()) {
            throw new IndexOutOfBoundsException();
         } else {
            if(var3 == var2.length()) {
               var3 = ~var3;
            } else {
               int var5 = this.b + var3;
               int var4 = var5;
               if(var5 > var2.length()) {
                  if(var8) {
                     var3 = ~var3;
                     return var3;
                  }

                  var4 = var2.length();
               }

               for(var5 = var3; var5 < var4; ++var5) {
                  if(var7) {
                     if(var2.charAt(var5) != this.c) {
                        break;
                     }
                  } else if(!var1.a(var2.charAt(var5), this.c)) {
                     break;
                  }
               }

               var2 = var2.subSequence(0, var4);
               int var6 = this.a.a(var1, var2, var5);
               if(var6 != var4 && var8) {
                  var3 = ~(var3 + var5);
               } else {
                  var3 = var6;
               }
            }

            return var3;
         }
      }

      public boolean a(DateTimePrintContext var1, StringBuilder var2) {
         int var3 = 0;
         boolean var6 = false;
         int var4 = var2.length();
         if(this.a.a(var1, var2)) {
            int var5 = var2.length() - var4;
            if(var5 > this.b) {
               throw new DateTimeException("Cannot print as output of " + var5 + " characters exceeds pad width of " + this.b);
            }

            while(var3 < this.b - var5) {
               var2.insert(var4, this.c);
               ++var3;
            }

            var6 = true;
         }

         return var6;
      }

      public String toString() {
         StringBuilder var2 = (new StringBuilder()).append("Pad(").append(this.a).append(",").append(this.b);
         String var1;
         if(this.c == 32) {
            var1 = ")";
         } else {
            var1 = ",'" + this.c + "')";
         }

         return var2.append(var1).toString();
      }
   }

   static final class ReducedPrinterParser extends DateTimeFormatterBuilder.NumberPrinterParser {
      static final LocalDate g = LocalDate.a(2000, 1, 1);
      private final int h;
      private final ChronoLocalDate i;

      ReducedPrinterParser(TemporalField var1, int var2, int var3, int var4, ChronoLocalDate var5) {
         super(var1, var2, var3, SignStyle.d);
         if(var2 >= 1 && var2 <= 10) {
            if(var3 >= 1 && var3 <= 10) {
               if(var3 < var2) {
                  throw new IllegalArgumentException("The maxWidth must be greater than the width");
               } else {
                  if(var5 == null) {
                     if(!var1.a().a((long)var4)) {
                        throw new IllegalArgumentException("The base value must be within the range of the field");
                     }

                     if((long)var4 + (long)a[var2] > 2147483647L) {
                        throw new DateTimeException("Unable to add printer-parser as the range exceeds the capacity of an int");
                     }
                  }

                  this.h = var4;
                  this.i = var5;
               }
            } else {
               throw new IllegalArgumentException("The maxWidth must be from 1 to 10 inclusive but was " + var3);
            }
         } else {
            throw new IllegalArgumentException("The width must be from 1 to 10 inclusive but was " + var2);
         }
      }

      private ReducedPrinterParser(TemporalField var1, int var2, int var3, int var4, ChronoLocalDate var5, int var6) {
         super(var1, var2, var3, SignStyle.d, var6, null);
         this.h = var4;
         this.i = var5;
      }

      int a(DateTimeParseContext var1, long var2, int var4, int var5) {
         int var6 = this.h;
         if(this.i != null) {
            var6 = var1.d().b((TemporalAccessor)this.i).c((TemporalField)this.b);
            var1.a(this, var2, var4, var5);
         }

         long var7 = var2;
         if(var5 - var4 == this.c) {
            var7 = var2;
            if(var2 >= 0L) {
               long var9 = (long)a[this.c];
               var7 = (long)var6;
               var7 = (long)var6 - var7 % var9;
               if(var6 > 0) {
                  var2 += var7;
               } else {
                  var2 = var7 - var2;
               }

               var7 = var2;
               if(var2 < (long)var6) {
                  var7 = var2 + var9;
               }
            }
         }

         return var1.a(this.b, var7, var4, var5);
      }

      long a(DateTimePrintContext var1, long var2) {
         long var5 = Math.abs(var2);
         int var4 = this.h;
         if(this.i != null) {
            var4 = Chronology.a(var1.a()).b((TemporalAccessor)this.i).c((TemporalField)this.b);
         }

         if(var2 >= (long)var4 && var2 < (long)(var4 + a[this.c])) {
            var2 = var5 % (long)a[this.c];
         } else {
            var2 = var5 % (long)a[this.d];
         }

         return var2;
      }

      DateTimeFormatterBuilder.NumberPrinterParser a() {
         DateTimeFormatterBuilder.ReducedPrinterParser var1;
         if(this.f == -1) {
            var1 = this;
         } else {
            var1 = new DateTimeFormatterBuilder.ReducedPrinterParser(this.b, this.c, this.d, this.h, this.i, -1);
         }

         return var1;
      }

      // $FF: synthetic method
      DateTimeFormatterBuilder.NumberPrinterParser a(int var1) {
         return this.b(var1);
      }

      boolean a(DateTimeParseContext var1) {
         boolean var2;
         if(!var1.f()) {
            var2 = false;
         } else {
            var2 = super.a(var1);
         }

         return var2;
      }

      DateTimeFormatterBuilder.ReducedPrinterParser b(int var1) {
         return new DateTimeFormatterBuilder.ReducedPrinterParser(this.b, this.c, this.d, this.h, this.i, this.f + var1);
      }

      public String toString() {
         StringBuilder var2 = (new StringBuilder()).append("ReducedValue(").append(this.b).append(",").append(this.c).append(",").append(this.d).append(",");
         Object var1;
         if(this.i != null) {
            var1 = this.i;
         } else {
            var1 = Integer.valueOf(this.h);
         }

         return var2.append(var1).append(")").toString();
      }
   }

   static enum SettingsParser implements DateTimeFormatterBuilder.DateTimePrinterParser {
      a,
      b,
      c,
      d;

      public int a(DateTimeParseContext var1, CharSequence var2, int var3) {
         switch(this.ordinal()) {
         case 0:
            var1.a(true);
            break;
         case 1:
            var1.a(false);
            break;
         case 2:
            var1.b(true);
            break;
         case 3:
            var1.b(false);
         }

         return var3;
      }

      public boolean a(DateTimePrintContext var1, StringBuilder var2) {
         return true;
      }

      public String toString() {
         String var1;
         switch(this.ordinal()) {
         case 0:
            var1 = "ParseCaseSensitive(true)";
            break;
         case 1:
            var1 = "ParseCaseSensitive(false)";
            break;
         case 2:
            var1 = "ParseStrict(true)";
            break;
         case 3:
            var1 = "ParseStrict(false)";
            break;
         default:
            throw new IllegalStateException("Unreachable");
         }

         return var1;
      }
   }

   static final class StringLiteralPrinterParser implements DateTimeFormatterBuilder.DateTimePrinterParser {
      private final String a;

      StringLiteralPrinterParser(String var1) {
         this.a = var1;
      }

      public int a(DateTimeParseContext var1, CharSequence var2, int var3) {
         if(var3 <= var2.length() && var3 >= 0) {
            if(!var1.a(var2, var3, this.a, 0, this.a.length())) {
               var3 = ~var3;
            } else {
               var3 += this.a.length();
            }

            return var3;
         } else {
            throw new IndexOutOfBoundsException();
         }
      }

      public boolean a(DateTimePrintContext var1, StringBuilder var2) {
         var2.append(this.a);
         return true;
      }

      public String toString() {
         String var1 = this.a.replace("'", "''");
         return "'" + var1 + "'";
      }
   }

   static final class TextPrinterParser implements DateTimeFormatterBuilder.DateTimePrinterParser {
      private final TemporalField a;
      private final TextStyle b;
      private final DateTimeTextProvider c;
      private volatile DateTimeFormatterBuilder.NumberPrinterParser d;

      TextPrinterParser(TemporalField var1, TextStyle var2, DateTimeTextProvider var3) {
         this.a = var1;
         this.b = var2;
         this.c = var3;
      }

      private DateTimeFormatterBuilder.NumberPrinterParser a() {
         if(this.d == null) {
            this.d = new DateTimeFormatterBuilder.NumberPrinterParser(this.a, 1, 19, SignStyle.a);
         }

         return this.d;
      }

      public int a(DateTimeParseContext var1, CharSequence var2, int var3) {
         int var4 = var2.length();
         if(var3 >= 0 && var3 <= var4) {
            TextStyle var5;
            if(var1.f()) {
               var5 = this.b;
            } else {
               var5 = null;
            }

            Iterator var6 = this.c.a(this.a, var5, var1.b());
            if(var6 != null) {
               while(true) {
                  if(!var6.hasNext()) {
                     if(var1.f()) {
                        var3 = ~var3;
                        return var3;
                     }
                     break;
                  }

                  Entry var8 = (Entry)var6.next();
                  String var7 = (String)var8.getKey();
                  if(var1.a(var7, 0, var2, var3, var7.length())) {
                     var3 = var1.a(this.a, ((Long)var8.getValue()).longValue(), var3, var3 + var7.length());
                     return var3;
                  }
               }
            }

            var3 = this.a().a(var1, var2, var3);
            return var3;
         } else {
            throw new IndexOutOfBoundsException();
         }
      }

      public boolean a(DateTimePrintContext var1, StringBuilder var2) {
         Long var4 = var1.a(this.a);
         boolean var3;
         if(var4 == null) {
            var3 = false;
         } else {
            String var5 = this.c.a(this.a, var4.longValue(), this.b, var1.b());
            if(var5 == null) {
               var3 = this.a().a(var1, var2);
            } else {
               var2.append(var5);
               var3 = true;
            }
         }

         return var3;
      }

      public String toString() {
         String var1;
         if(this.b == TextStyle.a) {
            var1 = "Text(" + this.a + ")";
         } else {
            var1 = "Text(" + this.a + "," + this.b + ")";
         }

         return var1;
      }
   }

   static final class WeekFieldsPrinterParser implements DateTimeFormatterBuilder.DateTimePrinterParser {
      private final char a;
      private final int b;

      public WeekFieldsPrinterParser(char var1, int var2) {
         this.a = var1;
         this.b = var2;
      }

      private DateTimeFormatterBuilder.DateTimePrinterParser a(WeekFields var1) {
         Object var4;
         switch(this.a) {
         case 'W':
            var4 = new DateTimeFormatterBuilder.NumberPrinterParser(var1.d(), 1, 2, SignStyle.d);
            break;
         case 'Y':
            if(this.b == 2) {
               var4 = new DateTimeFormatterBuilder.ReducedPrinterParser(var1.f(), 2, 2, 0, DateTimeFormatterBuilder.ReducedPrinterParser.g);
            } else {
               TemporalField var3 = var1.f();
               int var2 = this.b;
               SignStyle var5;
               if(this.b < 4) {
                  var5 = SignStyle.a;
               } else {
                  var5 = SignStyle.e;
               }

               var4 = new DateTimeFormatterBuilder.NumberPrinterParser(var3, var2, 19, var5, -1, null);
            }
            break;
         case 'c':
            var4 = new DateTimeFormatterBuilder.NumberPrinterParser(var1.c(), this.b, 2, SignStyle.d);
            break;
         case 'e':
            var4 = new DateTimeFormatterBuilder.NumberPrinterParser(var1.c(), this.b, 2, SignStyle.d);
            break;
         case 'w':
            var4 = new DateTimeFormatterBuilder.NumberPrinterParser(var1.e(), this.b, 2, SignStyle.d);
            break;
         default:
            var4 = null;
         }

         return (DateTimeFormatterBuilder.DateTimePrinterParser)var4;
      }

      public int a(DateTimeParseContext var1, CharSequence var2, int var3) {
         return this.a(WeekFields.a(var1.b())).a(var1, var2, var3);
      }

      public boolean a(DateTimePrintContext var1, StringBuilder var2) {
         return this.a(WeekFields.a(var1.b())).a(var1, var2);
      }

      public String toString() {
         StringBuilder var2 = new StringBuilder(30);
         var2.append("Localized(");
         if(this.a == 89) {
            if(this.b == 1) {
               var2.append("WeekBasedYear");
            } else if(this.b == 2) {
               var2.append("ReducedValue(WeekBasedYear,2,2,2000-01-01)");
            } else {
               StringBuilder var3 = var2.append("WeekBasedYear,").append(this.b).append(",").append(19).append(",");
               SignStyle var1;
               if(this.b < 4) {
                  var1 = SignStyle.a;
               } else {
                  var1 = SignStyle.e;
               }

               var3.append(var1);
            }
         } else {
            if(this.a != 99 && this.a != 101) {
               if(this.a == 119) {
                  var2.append("WeekOfWeekBasedYear");
               } else if(this.a == 87) {
                  var2.append("WeekOfMonth");
               }
            } else {
               var2.append("DayOfWeek");
            }

            var2.append(",");
            var2.append(this.b);
         }

         var2.append(")");
         return var2.toString();
      }
   }

   static final class ZoneIdPrinterParser implements DateTimeFormatterBuilder.DateTimePrinterParser {
      private static volatile Entry c;
      private final TemporalQuery a;
      private final String b;

      ZoneIdPrinterParser(TemporalQuery var1, String var2) {
         this.a = var1;
         this.b = var2;
      }

      private int a(DateTimeParseContext var1, CharSequence var2, int var3, int var4) {
         String var5 = var2.subSequence(var3, var4).toString().toUpperCase();
         DateTimeParseContext var6 = var1.a();
         if(var4 < var2.length() && var1.a(var2.charAt(var4), 'Z')) {
            var1.a(ZoneId.a(var5, ZoneOffset.d));
            var3 = var4;
         } else {
            var3 = DateTimeFormatterBuilder.OffsetIdPrinterParser.b.a(var6, var2, var4);
            if(var3 < 0) {
               var1.a(ZoneId.a(var5, ZoneOffset.d));
               var3 = var4;
            } else {
               var1.a(ZoneId.a(var5, ZoneOffset.a((int)var6.a((TemporalField)ChronoField.D).longValue())));
            }
         }

         return var3;
      }

      private ZoneId a(Set var1, String var2, boolean var3) {
         Object var4 = null;
         ZoneId var6;
         if(var2 == null) {
            var6 = (ZoneId)var4;
         } else if(var3) {
            if(var1.contains(var2)) {
               var6 = ZoneId.a(var2);
            } else {
               var6 = null;
            }
         } else {
            Iterator var5 = var1.iterator();

            while(true) {
               var6 = (ZoneId)var4;
               if(!var5.hasNext()) {
                  break;
               }

               String var7 = (String)var5.next();
               if(var7.equalsIgnoreCase(var2)) {
                  var6 = ZoneId.a(var7);
                  break;
               }
            }
         }

         return var6;
      }

      private static DateTimeFormatterBuilder.SubstringTree a(Set var0) {
         ArrayList var1 = new ArrayList(var0);
         Collections.sort(var1, DateTimeFormatterBuilder.a);
         DateTimeFormatterBuilder.SubstringTree var2 = new DateTimeFormatterBuilder.SubstringTree(((String)var1.get(0)).length(), null);
         Iterator var3 = var1.iterator();

         while(var3.hasNext()) {
            var2.a((String)var3.next());
         }

         return var2;
      }

      public int a(DateTimeParseContext param1, CharSequence param2, int param3) {
         // $FF: Couldn't be decompiled
      }

      public boolean a(DateTimePrintContext var1, StringBuilder var2) {
         ZoneId var4 = (ZoneId)var1.a(this.a);
         boolean var3;
         if(var4 == null) {
            var3 = false;
         } else {
            var2.append(var4.c());
            var3 = true;
         }

         return var3;
      }

      public String toString() {
         return this.b;
      }
   }

   private static final class SubstringTree {
      final int a;
      private final Map b;
      private final Map c;

      private SubstringTree(int var1) {
         this.b = new HashMap();
         this.c = new HashMap();
         this.a = var1;
      }

      // $FF: synthetic method
      SubstringTree(int var1, Object var2) {
         this(var1);
      }

      private DateTimeFormatterBuilder.SubstringTree a(CharSequence var1, boolean var2) {
         DateTimeFormatterBuilder.SubstringTree var3;
         if(var2) {
            var3 = (DateTimeFormatterBuilder.SubstringTree)this.b.get(var1);
         } else {
            var3 = (DateTimeFormatterBuilder.SubstringTree)this.c.get(var1.toString().toLowerCase(Locale.ENGLISH));
         }

         return var3;
      }

      // $FF: synthetic method
      static DateTimeFormatterBuilder.SubstringTree a(DateTimeFormatterBuilder.SubstringTree var0, CharSequence var1, boolean var2) {
         return var0.a(var1, var2);
      }

      private void a(String var1) {
         int var2 = var1.length();
         if(var2 == this.a) {
            this.b.put(var1, (Object)null);
            this.c.put(var1.toLowerCase(Locale.ENGLISH), (Object)null);
         } else if(var2 > this.a) {
            String var5 = var1.substring(0, this.a);
            DateTimeFormatterBuilder.SubstringTree var4 = (DateTimeFormatterBuilder.SubstringTree)this.b.get(var5);
            DateTimeFormatterBuilder.SubstringTree var3 = var4;
            if(var4 == null) {
               var3 = new DateTimeFormatterBuilder.SubstringTree(var2);
               this.b.put(var5, var3);
               this.c.put(var5.toLowerCase(Locale.ENGLISH), var3);
            }

            var3.a(var1);
         }

      }
   }

   static final class ZoneTextPrinterParser implements DateTimeFormatterBuilder.DateTimePrinterParser {
      private static final Comparator a = new Comparator() {
         public int a(String var1, String var2) {
            int var4 = var2.length() - var1.length();
            int var3 = var4;
            if(var4 == 0) {
               var3 = var1.compareTo(var2);
            }

            return var3;
         }

         // $FF: synthetic method
         public int compare(Object var1, Object var2) {
            return this.a((String)var1, (String)var2);
         }
      };
      private final TextStyle b;

      ZoneTextPrinterParser(TextStyle var1) {
         this.b = (TextStyle)Jdk8Methods.a(var1, (String)"textStyle");
      }

      public int a(DateTimeParseContext var1, CharSequence var2, int var3) {
         TreeMap var5 = new TreeMap(a);
         Iterator var8 = ZoneId.b().iterator();

         while(var8.hasNext()) {
            String var7 = (String)var8.next();
            var5.put(var7, var7);
            TimeZone var6 = TimeZone.getTimeZone(var7);
            byte var4;
            if(this.b.a() == TextStyle.a) {
               var4 = 1;
            } else {
               var4 = 0;
            }

            var5.put(var6.getDisplayName(false, var4, var1.b()), var7);
            var5.put(var6.getDisplayName(true, var4, var1.b()), var7);
         }

         Iterator var11 = var5.entrySet().iterator();

         while(true) {
            if(var11.hasNext()) {
               Entry var10 = (Entry)var11.next();
               String var9 = (String)var10.getKey();
               if(!var1.a(var2, var3, var9, 0, var9.length())) {
                  continue;
               }

               var1.a(ZoneId.a((String)var10.getValue()));
               var3 += var9.length();
               break;
            }

            var3 = ~var3;
            break;
         }

         return var3;
      }

      public boolean a(DateTimePrintContext var1, StringBuilder var2) {
         boolean var5 = true;
         byte var3 = 0;
         ZoneId var6 = (ZoneId)var1.a(TemporalQueries.a());
         boolean var4;
         if(var6 == null) {
            var4 = false;
         } else if(var6.e() instanceof ZoneOffset) {
            var2.append(var6.c());
            var4 = var5;
         } else {
            TemporalAccessor var7 = var1.a();
            if(var7.a((TemporalField)ChronoField.C)) {
               Instant var9 = Instant.a(var7.d(ChronoField.C));
               var4 = var6.d().c(var9);
            } else {
               var4 = false;
            }

            TimeZone var8 = TimeZone.getTimeZone(var6.c());
            if(this.b.a() == TextStyle.a) {
               var3 = 1;
            }

            var2.append(var8.getDisplayName(var4, var3, var1.b()));
            var4 = var5;
         }

         return var4;
      }

      public String toString() {
         return "ZoneText(" + this.b + ")";
      }
   }
}
