package org.threeten.bp.format;

import java.util.Locale;
import org.threeten.bp.DateTimeException;
import org.threeten.bp.Instant;
import org.threeten.bp.ZoneId;
import org.threeten.bp.ZoneOffset;
import org.threeten.bp.chrono.ChronoLocalDate;
import org.threeten.bp.chrono.Chronology;
import org.threeten.bp.chrono.IsoChronology;
import org.threeten.bp.jdk8.DefaultInterfaceTemporalAccessor;
import org.threeten.bp.jdk8.Jdk8Methods;
import org.threeten.bp.temporal.ChronoField;
import org.threeten.bp.temporal.TemporalAccessor;
import org.threeten.bp.temporal.TemporalField;
import org.threeten.bp.temporal.TemporalQueries;
import org.threeten.bp.temporal.TemporalQuery;
import org.threeten.bp.temporal.ValueRange;

final class DateTimePrintContext {
   private TemporalAccessor a;
   private Locale b;
   private DecimalStyle c;
   private int d;

   DateTimePrintContext(TemporalAccessor var1, DateTimeFormatter var2) {
      this.a = a(var1, var2);
      this.b = var2.a();
      this.c = var2.b();
   }

   private static TemporalAccessor a(final TemporalAccessor var0, DateTimeFormatter var1) {
      Object var7 = null;
      Chronology var4 = var1.c();
      ZoneId var5 = var1.d();
      final Object var13;
      if(var4 == null && var5 == null) {
         var13 = var0;
      } else {
         Chronology var8 = (Chronology)var0.a(TemporalQueries.b());
         final ZoneId var6 = (ZoneId)var0.a(TemporalQueries.a());
         Chronology var11 = var4;
         if(Jdk8Methods.a(var8, (Object)var4)) {
            var11 = null;
         }

         ZoneId var12 = var5;
         if(Jdk8Methods.a(var6, (Object)var5)) {
            var12 = null;
         }

         if(var11 == null) {
            var13 = var0;
            if(var12 == null) {
               return (TemporalAccessor)var13;
            }
         }

         if(var11 != null) {
            var13 = var11;
         } else {
            var13 = var8;
         }

         if(var12 != null) {
            var6 = var12;
         }

         if(var12 != null) {
            if(var0.a((TemporalField)ChronoField.C)) {
               if(var13 == null) {
                  var13 = IsoChronology.b;
               }

               var13 = ((Chronology)var13).a(Instant.a(var0), var12);
               return (TemporalAccessor)var13;
            }

            ZoneId var9 = var12.e();
            ZoneOffset var10 = (ZoneOffset)var0.a(TemporalQueries.e());
            if(var9 instanceof ZoneOffset && var10 != null && !var9.equals(var10)) {
               throw new DateTimeException("Invalid override zone for temporal: " + var12 + " " + var0);
            }
         }

         final ChronoLocalDate var14 = (ChronoLocalDate)var7;
         if(var11 != null) {
            if(var0.a((TemporalField)ChronoField.u)) {
               var14 = ((Chronology)var13).b(var0);
            } else {
               label88: {
                  if(var11 == IsoChronology.b) {
                     var14 = (ChronoLocalDate)var7;
                     if(var8 == null) {
                        break label88;
                     }
                  }

                  ChronoField[] var16 = ChronoField.values();
                  int var3 = var16.length;
                  int var2 = 0;

                  while(true) {
                     var14 = (ChronoLocalDate)var7;
                     if(var2 >= var3) {
                        break;
                     }

                     ChronoField var15 = var16[var2];
                     if(var15.b() && var0.a((TemporalField)var15)) {
                        throw new DateTimeException("Invalid override chronology for temporal: " + var11 + " " + var0);
                     }

                     ++var2;
                  }
               }
            }
         }

         var13 = new DefaultInterfaceTemporalAccessor() {
            public Object a(TemporalQuery var1) {
               Object var2;
               if(var1 == TemporalQueries.b()) {
                  var2 = var13;
               } else if(var1 == TemporalQueries.a()) {
                  var2 = var6;
               } else if(var1 == TemporalQueries.c()) {
                  var2 = var0.a(var1);
               } else {
                  var2 = var1.b(this);
               }

               return var2;
            }

            public boolean a(TemporalField var1) {
               boolean var2;
               if(var14 != null && var1.b()) {
                  var2 = var14.a(var1);
               } else {
                  var2 = var0.a(var1);
               }

               return var2;
            }

            public ValueRange b(TemporalField var1) {
               ValueRange var2;
               if(var14 != null && var1.b()) {
                  var2 = var14.b((TemporalField)var1);
               } else {
                  var2 = var0.b(var1);
               }

               return var2;
            }

            public long d(TemporalField var1) {
               long var2;
               if(var14 != null && var1.b()) {
                  var2 = var14.d(var1);
               } else {
                  var2 = var0.d(var1);
               }

               return var2;
            }
         };
      }

      return (TemporalAccessor)var13;
   }

   Long a(TemporalField var1) {
      long var2;
      Long var5;
      try {
         var2 = this.a.d(var1);
      } catch (DateTimeException var4) {
         if(this.d > 0) {
            var5 = null;
            return var5;
         }

         throw var4;
      }

      var5 = Long.valueOf(var2);
      return var5;
   }

   Object a(TemporalQuery var1) {
      Object var2 = this.a.a(var1);
      if(var2 == null && this.d == 0) {
         throw new DateTimeException("Unable to extract value: " + this.a.getClass());
      } else {
         return var2;
      }
   }

   TemporalAccessor a() {
      return this.a;
   }

   Locale b() {
      return this.b;
   }

   DecimalStyle c() {
      return this.c;
   }

   void d() {
      ++this.d;
   }

   void e() {
      --this.d;
   }

   public String toString() {
      return this.a.toString();
   }
}
