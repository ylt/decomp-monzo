package org.threeten.bp.format;

import org.threeten.bp.DateTimeException;

public class DateTimeParseException extends DateTimeException {
   private final String a;
   private final int b;

   public DateTimeParseException(String var1, CharSequence var2, int var3) {
      super(var1);
      this.a = var2.toString();
      this.b = var3;
   }

   public DateTimeParseException(String var1, CharSequence var2, int var3, Throwable var4) {
      super(var1, var4);
      this.a = var2.toString();
      this.b = var3;
   }
}
