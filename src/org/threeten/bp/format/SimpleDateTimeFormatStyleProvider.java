package org.threeten.bp.format;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import org.threeten.bp.chrono.Chronology;

final class SimpleDateTimeFormatStyleProvider extends DateTimeFormatStyleProvider {
   private static final ConcurrentMap a = new ConcurrentHashMap(16, 0.75F, 2);

   private int a(FormatStyle var1) {
      return var1.ordinal();
   }

   public DateTimeFormatter a(FormatStyle var1, FormatStyle var2, Chronology var3, Locale var4) {
      if(var1 == null && var2 == null) {
         throw new IllegalArgumentException("Date and Time style must not both be null");
      } else {
         String var9 = var3.a() + '|' + var4.toString() + '|' + var1 + var2;
         Object var5 = a.get(var9);
         DateTimeFormatter var6;
         if(var5 != null) {
            if(var5.equals("")) {
               throw new IllegalArgumentException("Unable to convert DateFormat to DateTimeFormatter");
            }

            var6 = (DateTimeFormatter)var5;
         } else {
            DateFormat var7;
            if(var1 != null) {
               if(var2 != null) {
                  var7 = DateFormat.getDateTimeInstance(this.a(var1), this.a(var2), var4);
               } else {
                  var7 = DateFormat.getDateInstance(this.a(var1), var4);
               }
            } else {
               var7 = DateFormat.getTimeInstance(this.a(var2), var4);
            }

            if(!(var7 instanceof SimpleDateFormat)) {
               a.putIfAbsent(var9, "");
               throw new IllegalArgumentException("Unable to convert DateFormat to DateTimeFormatter");
            }

            String var8 = ((SimpleDateFormat)var7).toPattern();
            var6 = (new DateTimeFormatterBuilder()).b(var8).a(var4);
            a.putIfAbsent(var9, var6);
         }

         return var6;
      }
   }
}
