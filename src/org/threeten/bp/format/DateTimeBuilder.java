package org.threeten.bp.format;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import org.threeten.bp.DateTimeException;
import org.threeten.bp.Instant;
import org.threeten.bp.LocalDate;
import org.threeten.bp.LocalTime;
import org.threeten.bp.Period;
import org.threeten.bp.ZoneId;
import org.threeten.bp.ZoneOffset;
import org.threeten.bp.chrono.ChronoLocalDate;
import org.threeten.bp.chrono.ChronoLocalDateTime;
import org.threeten.bp.chrono.ChronoZonedDateTime;
import org.threeten.bp.chrono.Chronology;
import org.threeten.bp.chrono.IsoChronology;
import org.threeten.bp.jdk8.DefaultInterfaceTemporalAccessor;
import org.threeten.bp.jdk8.Jdk8Methods;
import org.threeten.bp.temporal.ChronoField;
import org.threeten.bp.temporal.TemporalAccessor;
import org.threeten.bp.temporal.TemporalAmount;
import org.threeten.bp.temporal.TemporalField;
import org.threeten.bp.temporal.TemporalQueries;
import org.threeten.bp.temporal.TemporalQuery;

final class DateTimeBuilder extends DefaultInterfaceTemporalAccessor implements Cloneable, TemporalAccessor {
   final Map a = new HashMap();
   Chronology b;
   ZoneId c;
   ChronoLocalDate d;
   LocalTime e;
   boolean f;
   Period g;

   private void a() {
      if(this.a.containsKey(ChronoField.C)) {
         if(this.c != null) {
            this.a(this.c);
         } else {
            Long var1 = (Long)this.a.get(ChronoField.D);
            if(var1 != null) {
               this.a((ZoneId)ZoneOffset.a(var1.intValue()));
            }
         }
      }

   }

   private void a(LocalDate var1) {
      if(var1 != null) {
         this.a((ChronoLocalDate)var1);
         Iterator var4 = this.a.keySet().iterator();

         while(true) {
            TemporalField var6;
            do {
               do {
                  if(!var4.hasNext()) {
                     return;
                  }

                  var6 = (TemporalField)var4.next();
               } while(!(var6 instanceof ChronoField));
            } while(!var6.b());

            long var2;
            try {
               var2 = var1.d(var6);
            } catch (DateTimeException var7) {
               continue;
            }

            Long var5 = (Long)this.a.get(var6);
            if(var2 != var5.longValue()) {
               throw new DateTimeException("Conflict found: Field " + var6 + " " + var2 + " differs from " + var6 + " " + var5 + " derived from " + var1);
            }
         }
      }
   }

   private void a(ZoneId var1) {
      Instant var2 = Instant.a(((Long)this.a.remove(ChronoField.C)).longValue());
      ChronoZonedDateTime var3 = this.b.a(var2, var1);
      if(this.d == null) {
         this.a(var3.i());
      } else {
         this.a((TemporalField)ChronoField.C, (ChronoLocalDate)var3.i());
      }

      this.a(ChronoField.h, (long)var3.f().e());
   }

   private void a(TemporalAccessor var1) {
      Iterator var6 = this.a.entrySet().iterator();

      while(true) {
         long var4;
         TemporalField var7;
         do {
            if(!var6.hasNext()) {
               return;
            }

            Entry var8 = (Entry)var6.next();
            var7 = (TemporalField)var8.getKey();
            var4 = ((Long)var8.getValue()).longValue();
         } while(!var1.a(var7));

         long var2;
         try {
            var2 = var1.d(var7);
         } catch (RuntimeException var9) {
            continue;
         }

         if(var2 != var4) {
            throw new DateTimeException("Cross check failed: " + var7 + " " + var2 + " vs " + var7 + " " + var4);
         }

         var6.remove();
      }
   }

   private void a(TemporalField var1, LocalTime var2) {
      long var3 = var2.f();
      Long var5 = (Long)this.a.put(ChronoField.b, Long.valueOf(var3));
      if(var5 != null && var5.longValue() != var3) {
         throw new DateTimeException("Conflict found: " + LocalTime.b(var5.longValue()) + " differs from " + var2 + " while resolving  " + var1);
      }
   }

   private void a(TemporalField var1, ChronoLocalDate var2) {
      if(!this.b.equals(var2.o())) {
         throw new DateTimeException("ChronoLocalDate must use the effective parsed chronology: " + this.b);
      } else {
         long var3 = var2.n();
         Long var5 = (Long)this.a.put(ChronoField.u, Long.valueOf(var3));
         if(var5 != null && var5.longValue() != var3) {
            throw new DateTimeException("Conflict found: " + LocalDate.a(var5.longValue()) + " differs from " + LocalDate.a(var3) + " while resolving  " + var1);
         }
      }
   }

   private boolean a(ResolverStyle var1) {
      int var2 = 0;

      label53:
      while(var2 < 100) {
         Iterator var4 = this.a.entrySet().iterator();

         TemporalField var6;
         do {
            if(!var4.hasNext()) {
               break label53;
            }

            var6 = (TemporalField)((Entry)var4.next()).getKey();
            TemporalAccessor var5 = var6.a(this.a, this, var1);
            if(var5 != null) {
               Object var7 = var5;
               if(var5 instanceof ChronoZonedDateTime) {
                  ChronoZonedDateTime var8 = (ChronoZonedDateTime)var5;
                  if(this.c == null) {
                     this.c = var8.b();
                  } else if(!this.c.equals(var8.b())) {
                     throw new DateTimeException("ChronoZonedDateTime must use the effective parsed zone: " + this.c);
                  }

                  var7 = var8.h();
               }

               if(var7 instanceof ChronoLocalDate) {
                  this.a(var6, (ChronoLocalDate)var7);
                  ++var2;
               } else if(var7 instanceof LocalTime) {
                  this.a(var6, (LocalTime)var7);
                  ++var2;
               } else {
                  if(!(var7 instanceof ChronoLocalDateTime)) {
                     throw new DateTimeException("Unknown type: " + var7.getClass().getName());
                  }

                  ChronoLocalDateTime var9 = (ChronoLocalDateTime)var7;
                  this.a(var6, var9.l());
                  this.a(var6, var9.k());
                  ++var2;
               }
               continue label53;
            }
         } while(this.a.containsKey(var6));

         ++var2;
      }

      if(var2 == 100) {
         throw new DateTimeException("Badly written field");
      } else {
         boolean var3;
         if(var2 > 0) {
            var3 = true;
         } else {
            var3 = false;
         }

         return var3;
      }
   }

   private DateTimeBuilder b(TemporalField var1, long var2) {
      this.a.put(var1, Long.valueOf(var2));
      return this;
   }

   private void b() {
      if(this.a.size() > 0) {
         if(this.d != null && this.e != null) {
            this.a((TemporalAccessor)this.d.b(this.e));
         } else if(this.d != null) {
            this.a((TemporalAccessor)this.d);
         } else if(this.e != null) {
            this.a((TemporalAccessor)this.e);
         }
      }

   }

   private void b(ResolverStyle var1) {
      if(this.b instanceof IsoChronology) {
         this.a(IsoChronology.b.a(this.a, var1));
      } else if(this.a.containsKey(ChronoField.u)) {
         this.a(LocalDate.a(((Long)this.a.remove(ChronoField.u)).longValue()));
      }

   }

   private void c() {
      if(this.e == null && (this.a.containsKey(ChronoField.C) || this.a.containsKey(ChronoField.h) || this.a.containsKey(ChronoField.g))) {
         if(this.a.containsKey(ChronoField.a)) {
            long var1 = ((Long)this.a.get(ChronoField.a)).longValue();
            this.a.put(ChronoField.c, Long.valueOf(var1 / 1000L));
            this.a.put(ChronoField.e, Long.valueOf(var1 / 1000000L));
         } else {
            this.a.put(ChronoField.a, Long.valueOf(0L));
            this.a.put(ChronoField.c, Long.valueOf(0L));
            this.a.put(ChronoField.e, Long.valueOf(0L));
         }
      }

   }

   private void c(ResolverStyle var1) {
      long var2;
      long var4;
      ChronoField var6;
      if(this.a.containsKey(ChronoField.n)) {
         var4 = ((Long)this.a.remove(ChronoField.n)).longValue();
         if(var1 != ResolverStyle.c && (var1 != ResolverStyle.b || var4 != 0L)) {
            ChronoField.n.a(var4);
         }

         var6 = ChronoField.m;
         var2 = var4;
         if(var4 == 24L) {
            var2 = 0L;
         }

         this.a(var6, var2);
      }

      if(this.a.containsKey(ChronoField.l)) {
         var4 = ((Long)this.a.remove(ChronoField.l)).longValue();
         if(var1 != ResolverStyle.c && (var1 != ResolverStyle.b || var4 != 0L)) {
            ChronoField.l.a(var4);
         }

         var6 = ChronoField.k;
         var2 = var4;
         if(var4 == 12L) {
            var2 = 0L;
         }

         this.a(var6, var2);
      }

      if(var1 != ResolverStyle.c) {
         if(this.a.containsKey(ChronoField.o)) {
            ChronoField.o.a(((Long)this.a.get(ChronoField.o)).longValue());
         }

         if(this.a.containsKey(ChronoField.k)) {
            ChronoField.k.a(((Long)this.a.get(ChronoField.k)).longValue());
         }
      }

      if(this.a.containsKey(ChronoField.o) && this.a.containsKey(ChronoField.k)) {
         var4 = ((Long)this.a.remove(ChronoField.o)).longValue();
         var2 = ((Long)this.a.remove(ChronoField.k)).longValue();
         this.a(ChronoField.m, var2 + var4 * 12L);
      }

      if(this.a.containsKey(ChronoField.b)) {
         var2 = ((Long)this.a.remove(ChronoField.b)).longValue();
         if(var1 != ResolverStyle.c) {
            ChronoField.b.a(var2);
         }

         this.a(ChronoField.h, var2 / 1000000000L);
         this.a(ChronoField.a, var2 % 1000000000L);
      }

      if(this.a.containsKey(ChronoField.d)) {
         var2 = ((Long)this.a.remove(ChronoField.d)).longValue();
         if(var1 != ResolverStyle.c) {
            ChronoField.d.a(var2);
         }

         this.a(ChronoField.h, var2 / 1000000L);
         this.a(ChronoField.c, var2 % 1000000L);
      }

      if(this.a.containsKey(ChronoField.f)) {
         var2 = ((Long)this.a.remove(ChronoField.f)).longValue();
         if(var1 != ResolverStyle.c) {
            ChronoField.f.a(var2);
         }

         this.a(ChronoField.h, var2 / 1000L);
         this.a(ChronoField.e, var2 % 1000L);
      }

      if(this.a.containsKey(ChronoField.h)) {
         var2 = ((Long)this.a.remove(ChronoField.h)).longValue();
         if(var1 != ResolverStyle.c) {
            ChronoField.h.a(var2);
         }

         this.a(ChronoField.m, var2 / 3600L);
         this.a(ChronoField.i, var2 / 60L % 60L);
         this.a(ChronoField.g, var2 % 60L);
      }

      if(this.a.containsKey(ChronoField.j)) {
         var2 = ((Long)this.a.remove(ChronoField.j)).longValue();
         if(var1 != ResolverStyle.c) {
            ChronoField.j.a(var2);
         }

         this.a(ChronoField.m, var2 / 60L);
         this.a(ChronoField.i, var2 % 60L);
      }

      if(var1 != ResolverStyle.c) {
         if(this.a.containsKey(ChronoField.e)) {
            ChronoField.e.a(((Long)this.a.get(ChronoField.e)).longValue());
         }

         if(this.a.containsKey(ChronoField.c)) {
            ChronoField.c.a(((Long)this.a.get(ChronoField.c)).longValue());
         }
      }

      if(this.a.containsKey(ChronoField.e) && this.a.containsKey(ChronoField.c)) {
         var4 = ((Long)this.a.remove(ChronoField.e)).longValue();
         var2 = ((Long)this.a.get(ChronoField.c)).longValue();
         this.a(ChronoField.c, var2 % 1000L + var4 * 1000L);
      }

      if(this.a.containsKey(ChronoField.c) && this.a.containsKey(ChronoField.a)) {
         var2 = ((Long)this.a.get(ChronoField.a)).longValue();
         this.a(ChronoField.c, var2 / 1000L);
         this.a.remove(ChronoField.c);
      }

      if(this.a.containsKey(ChronoField.e) && this.a.containsKey(ChronoField.a)) {
         var2 = ((Long)this.a.get(ChronoField.a)).longValue();
         this.a(ChronoField.e, var2 / 1000000L);
         this.a.remove(ChronoField.e);
      }

      if(this.a.containsKey(ChronoField.c)) {
         var2 = ((Long)this.a.remove(ChronoField.c)).longValue();
         this.a(ChronoField.a, var2 * 1000L);
      } else if(this.a.containsKey(ChronoField.e)) {
         var2 = ((Long)this.a.remove(ChronoField.e)).longValue();
         this.a(ChronoField.a, var2 * 1000000L);
      }

   }

   private void d() {
      if(this.d != null && this.e != null) {
         long var1;
         if(this.c != null) {
            var1 = this.d.b(this.e).b(this.c).d((TemporalField)ChronoField.C);
            this.a.put(ChronoField.C, Long.valueOf(var1));
         } else {
            Long var3 = (Long)this.a.get(ChronoField.D);
            if(var3 != null) {
               ZoneOffset var4 = ZoneOffset.a(var3.intValue());
               var1 = this.d.b(this.e).b((ZoneId)var4).d((TemporalField)ChronoField.C);
               this.a.put(ChronoField.C, Long.valueOf(var1));
            }
         }
      }

   }

   private void d(ResolverStyle var1) {
      Long var9 = (Long)this.a.get(ChronoField.m);
      Long var10 = (Long)this.a.get(ChronoField.i);
      Long var11 = (Long)this.a.get(ChronoField.g);
      Long var8 = (Long)this.a.get(ChronoField.a);
      if(var9 != null && (var10 != null || var11 == null && var8 == null) && (var10 == null || var11 != null || var8 == null)) {
         int var2;
         if(var1 != ResolverStyle.c) {
            if(var9 != null) {
               Long var7 = var9;
               if(var1 == ResolverStyle.b) {
                  var7 = var9;
                  if(var9.longValue() == 24L) {
                     label89: {
                        if(var10 != null) {
                           var7 = var9;
                           if(var10.longValue() != 0L) {
                              break label89;
                           }
                        }

                        if(var11 != null) {
                           var7 = var9;
                           if(var11.longValue() != 0L) {
                              break label89;
                           }
                        }

                        if(var8 != null) {
                           var7 = var9;
                           if(var8.longValue() != 0L) {
                              break label89;
                           }
                        }

                        var7 = Long.valueOf(0L);
                        this.g = Period.a(1);
                     }
                  }
               }

               int var3 = ChronoField.m.b(var7.longValue());
               if(var10 != null) {
                  var2 = ChronoField.i.b(var10.longValue());
                  if(var11 != null) {
                     int var4 = ChronoField.g.b(var11.longValue());
                     if(var8 != null) {
                        this.a(LocalTime.a(var3, var2, var4, ChronoField.a.b(var8.longValue())));
                     } else {
                        this.a(LocalTime.a(var3, var2, var4));
                     }
                  } else if(var8 == null) {
                     this.a(LocalTime.a(var3, var2));
                  }
               } else if(var11 == null && var8 == null) {
                  this.a(LocalTime.a(var3, 0));
               }
            }
         } else if(var9 != null) {
            long var5 = var9.longValue();
            if(var10 != null) {
               if(var11 != null) {
                  Long var12 = var8;
                  if(var8 == null) {
                     var12 = Long.valueOf(0L);
                  }

                  var5 = Jdk8Methods.b(Jdk8Methods.b(Jdk8Methods.b(Jdk8Methods.d(var5, 3600000000000L), Jdk8Methods.d(var10.longValue(), 60000000000L)), Jdk8Methods.d(var11.longValue(), 1000000000L)), var12.longValue());
                  var2 = (int)Jdk8Methods.e(var5, 86400000000000L);
                  this.a(LocalTime.b(Jdk8Methods.f(var5, 86400000000000L)));
                  this.g = Period.a(var2);
               } else {
                  var5 = Jdk8Methods.b(Jdk8Methods.d(var5, 3600L), Jdk8Methods.d(var10.longValue(), 60L));
                  var2 = (int)Jdk8Methods.e(var5, 86400L);
                  this.a(LocalTime.a(Jdk8Methods.f(var5, 86400L)));
                  this.g = Period.a(var2);
               }
            } else {
               var2 = Jdk8Methods.a(Jdk8Methods.e(var5, 24L));
               this.a(LocalTime.a((int)((long)Jdk8Methods.b(var5, 24)), 0));
               this.g = Period.a(var2);
            }
         }

         this.a.remove(ChronoField.m);
         this.a.remove(ChronoField.i);
         this.a.remove(ChronoField.g);
         this.a.remove(ChronoField.a);
      }

   }

   private Long e(TemporalField var1) {
      return (Long)this.a.get(var1);
   }

   public Object a(TemporalQuery var1) {
      Object var2 = null;
      if(var1 == TemporalQueries.a()) {
         var2 = this.c;
      } else if(var1 == TemporalQueries.b()) {
         var2 = this.b;
      } else if(var1 == TemporalQueries.f()) {
         if(this.d != null) {
            var2 = LocalDate.a((TemporalAccessor)this.d);
         }
      } else if(var1 == TemporalQueries.g()) {
         var2 = this.e;
      } else if(var1 != TemporalQueries.d() && var1 != TemporalQueries.e()) {
         if(var1 != TemporalQueries.c()) {
            var2 = var1.b(this);
         }
      } else {
         var2 = var1.b(this);
      }

      return var2;
   }

   public DateTimeBuilder a(ResolverStyle var1, Set var2) {
      if(var2 != null) {
         this.a.keySet().retainAll(var2);
      }

      this.a();
      this.b(var1);
      this.c(var1);
      if(this.a(var1)) {
         this.a();
         this.b(var1);
         this.c(var1);
      }

      this.d(var1);
      this.b();
      if(this.g != null && !this.g.b() && this.d != null && this.e != null) {
         this.d = this.d.b((TemporalAmount)this.g);
         this.g = Period.a;
      }

      this.c();
      this.d();
      return this;
   }

   DateTimeBuilder a(TemporalField var1, long var2) {
      Jdk8Methods.a(var1, (String)"field");
      Long var4 = this.e(var1);
      if(var4 != null && var4.longValue() != var2) {
         throw new DateTimeException("Conflict found: " + var1 + " " + var4 + " differs from " + var1 + " " + var2 + ": " + this);
      } else {
         return this.b(var1, var2);
      }
   }

   void a(LocalTime var1) {
      this.e = var1;
   }

   void a(ChronoLocalDate var1) {
      this.d = var1;
   }

   public boolean a(TemporalField var1) {
      boolean var3 = false;
      boolean var2;
      if(var1 == null) {
         var2 = var3;
      } else {
         if(!this.a.containsKey(var1) && (this.d == null || !this.d.a(var1))) {
            var2 = var3;
            if(this.e == null) {
               return var2;
            }

            var2 = var3;
            if(!this.e.a(var1)) {
               return var2;
            }
         }

         var2 = true;
      }

      return var2;
   }

   public Object b(TemporalQuery var1) {
      return var1.b(this);
   }

   public long d(TemporalField var1) {
      Jdk8Methods.a(var1, (String)"field");
      Long var4 = this.e(var1);
      long var2;
      if(var4 == null) {
         if(this.d != null && this.d.a(var1)) {
            var2 = this.d.d(var1);
         } else {
            if(this.e == null || !this.e.a(var1)) {
               throw new DateTimeException("Field not found: " + var1);
            }

            var2 = this.e.d(var1);
         }
      } else {
         var2 = var4.longValue();
      }

      return var2;
   }

   public String toString() {
      StringBuilder var1 = new StringBuilder(128);
      var1.append("DateTimeBuilder[");
      if(this.a.size() > 0) {
         var1.append("fields=").append(this.a);
      }

      var1.append(", ").append(this.b);
      var1.append(", ").append(this.c);
      var1.append(", ").append(this.d);
      var1.append(", ").append(this.e);
      var1.append(']');
      return var1.toString();
   }
}
