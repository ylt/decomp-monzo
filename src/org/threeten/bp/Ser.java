package org.threeten.bp;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.Externalizable;
import java.io.IOException;
import java.io.InvalidClassException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.io.StreamCorruptedException;

final class Ser implements Externalizable {
   private byte a;
   private Object b;

   public Ser() {
   }

   Ser(byte var1, Object var2) {
      this.a = var1;
      this.b = var2;
   }

   private static Object a(byte var0, DataInput var1) throws IOException {
      Object var2;
      switch(var0) {
      case 1:
         var2 = Duration.a(var1);
         break;
      case 2:
         var2 = Instant.a(var1);
         break;
      case 3:
         var2 = LocalDate.a(var1);
         break;
      case 4:
         var2 = LocalDateTime.a(var1);
         break;
      case 5:
         var2 = LocalTime.a(var1);
         break;
      case 6:
         var2 = ZonedDateTime.a(var1);
         break;
      case 7:
         var2 = ZoneRegion.a(var1);
         break;
      case 8:
         var2 = ZoneOffset.a(var1);
         break;
      case 64:
         var2 = MonthDay.a(var1);
         break;
      case 66:
         var2 = OffsetTime.a(var1);
         break;
      case 67:
         var2 = Year.a(var1);
         break;
      case 68:
         var2 = YearMonth.a(var1);
         break;
      case 69:
         var2 = OffsetDateTime.a(var1);
         break;
      default:
         throw new StreamCorruptedException("Unknown serialized type");
      }

      return var2;
   }

   static Object a(DataInput var0) throws IOException {
      return a(var0.readByte(), var0);
   }

   static void a(byte var0, Object var1, DataOutput var2) throws IOException {
      var2.writeByte(var0);
      switch(var0) {
      case 1:
         ((Duration)var1).a(var2);
         break;
      case 2:
         ((Instant)var1).a(var2);
         break;
      case 3:
         ((LocalDate)var1).a(var2);
         break;
      case 4:
         ((LocalDateTime)var1).a(var2);
         break;
      case 5:
         ((LocalTime)var1).a(var2);
         break;
      case 6:
         ((ZonedDateTime)var1).a(var2);
         break;
      case 7:
         ((ZoneRegion)var1).b(var2);
         break;
      case 8:
         ((ZoneOffset)var1).b(var2);
         break;
      case 64:
         ((MonthDay)var1).a(var2);
         break;
      case 66:
         ((OffsetTime)var1).a(var2);
         break;
      case 67:
         ((Year)var1).a(var2);
         break;
      case 68:
         ((YearMonth)var1).a(var2);
         break;
      case 69:
         ((OffsetDateTime)var1).a(var2);
         break;
      default:
         throw new InvalidClassException("Unknown serialized type");
      }

   }

   private Object readResolve() {
      return this.b;
   }

   public void readExternal(ObjectInput var1) throws IOException {
      this.a = var1.readByte();
      this.b = a(this.a, var1);
   }

   public void writeExternal(ObjectOutput var1) throws IOException {
      a(this.a, this.b, var1);
   }
}
