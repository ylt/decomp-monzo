package org.threeten.bp;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.InvalidObjectException;
import java.io.ObjectStreamException;
import java.io.Serializable;
import org.threeten.bp.chrono.Chronology;
import org.threeten.bp.chrono.IsoChronology;
import org.threeten.bp.format.DateTimeFormatter;
import org.threeten.bp.format.DateTimeFormatterBuilder;
import org.threeten.bp.jdk8.DefaultInterfaceTemporalAccessor;
import org.threeten.bp.jdk8.Jdk8Methods;
import org.threeten.bp.temporal.ChronoField;
import org.threeten.bp.temporal.Temporal;
import org.threeten.bp.temporal.TemporalAccessor;
import org.threeten.bp.temporal.TemporalAdjuster;
import org.threeten.bp.temporal.TemporalField;
import org.threeten.bp.temporal.TemporalQueries;
import org.threeten.bp.temporal.TemporalQuery;
import org.threeten.bp.temporal.UnsupportedTemporalTypeException;
import org.threeten.bp.temporal.ValueRange;

public final class MonthDay extends DefaultInterfaceTemporalAccessor implements Serializable, Comparable, TemporalAccessor, TemporalAdjuster {
   public static final TemporalQuery a = new TemporalQuery() {
      public MonthDay a(TemporalAccessor var1) {
         return MonthDay.a(var1);
      }

      // $FF: synthetic method
      public Object b(TemporalAccessor var1) {
         return this.a(var1);
      }
   };
   private static final DateTimeFormatter b;
   private final int c;
   private final int d;

   static {
      b = (new DateTimeFormatterBuilder()).a("--").a(ChronoField.x, 2).a('-').a(ChronoField.s, 2).j();
   }

   private MonthDay(int var1, int var2) {
      this.c = var1;
      this.d = var2;
   }

   public static MonthDay a(int var0, int var1) {
      return a(Month.a(var0), var1);
   }

   static MonthDay a(DataInput var0) throws IOException {
      return a(var0.readByte(), var0.readByte());
   }

   public static MonthDay a(Month var0, int var1) {
      Jdk8Methods.a(var0, (String)"month");
      ChronoField.s.a((long)var1);
      if(var1 > var0.c()) {
         throw new DateTimeException("Illegal value for DayOfMonth field, value " + var1 + " is not valid for month " + var0.name());
      } else {
         return new MonthDay(var0.a(), var1);
      }
   }

   public static MonthDay a(TemporalAccessor param0) {
      // $FF: Couldn't be decompiled
   }

   private Object readResolve() throws ObjectStreamException {
      throw new InvalidObjectException("Deserialization via serialization delegate");
   }

   private Object writeReplace() {
      return new Ser(64, this);
   }

   public int a(MonthDay var1) {
      int var3 = this.c - var1.c;
      int var2 = var3;
      if(var3 == 0) {
         var2 = this.d - var1.d;
      }

      return var2;
   }

   public Object a(TemporalQuery var1) {
      Object var2;
      if(var1 == TemporalQueries.b()) {
         var2 = IsoChronology.b;
      } else {
         var2 = super.a(var1);
      }

      return var2;
   }

   public Month a() {
      return Month.a(this.c);
   }

   public Temporal a(Temporal var1) {
      if(!Chronology.a((TemporalAccessor)var1).equals(IsoChronology.b)) {
         throw new DateTimeException("Adjustment only supported on ISO date-time");
      } else {
         var1 = var1.b(ChronoField.x, (long)this.c);
         return var1.b(ChronoField.s, Math.min(var1.b(ChronoField.s).c(), (long)this.d));
      }
   }

   void a(DataOutput var1) throws IOException {
      var1.writeByte(this.c);
      var1.writeByte(this.d);
   }

   public boolean a(TemporalField var1) {
      boolean var2 = true;
      boolean var3 = false;
      if(var1 instanceof ChronoField) {
         if(var1 != ChronoField.x) {
            var2 = var3;
            if(var1 != ChronoField.s) {
               return var2;
            }
         }

         var2 = true;
      } else if(var1 == null || !var1.a(this)) {
         var2 = false;
      }

      return var2;
   }

   public ValueRange b(TemporalField var1) {
      ValueRange var2;
      if(var1 == ChronoField.x) {
         var2 = var1.a();
      } else if(var1 == ChronoField.s) {
         var2 = ValueRange.a(1L, (long)this.a().b(), (long)this.a().c());
      } else {
         var2 = super.b(var1);
      }

      return var2;
   }

   public int c(TemporalField var1) {
      return this.b(var1).b(this.d(var1), var1);
   }

   // $FF: synthetic method
   public int compareTo(Object var1) {
      return this.a((MonthDay)var1);
   }

   public long d(TemporalField var1) {
      long var2;
      if(var1 instanceof ChronoField) {
         switch(null.a[((ChronoField)var1).ordinal()]) {
         case 1:
            var2 = (long)this.d;
            break;
         case 2:
            var2 = (long)this.c;
            break;
         default:
            throw new UnsupportedTemporalTypeException("Unsupported field: " + var1);
         }
      } else {
         var2 = var1.c(this);
      }

      return var2;
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(this != var1) {
         if(var1 instanceof MonthDay) {
            MonthDay var3 = (MonthDay)var1;
            if(this.c != var3.c || this.d != var3.d) {
               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public int hashCode() {
      return (this.c << 6) + this.d;
   }

   public String toString() {
      StringBuilder var2 = (new StringBuilder(10)).append("--");
      String var1;
      if(this.c < 10) {
         var1 = "0";
      } else {
         var1 = "";
      }

      var2 = var2.append(var1).append(this.c);
      if(this.d < 10) {
         var1 = "-0";
      } else {
         var1 = "-";
      }

      return var2.append(var1).append(this.d).toString();
   }
}
