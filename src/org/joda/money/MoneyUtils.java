package org.joda.money;

public final class MoneyUtils {
   public static BigMoney add(BigMoney var0, BigMoney var1) {
      if(var0 != null) {
         if(var1 == null) {
            var1 = var0;
         } else {
            var1 = var0.plus((BigMoneyProvider)var1);
         }
      }

      return var1;
   }

   public static Money add(Money var0, Money var1) {
      if(var0 != null) {
         if(var1 == null) {
            var1 = var0;
         } else {
            var1 = var0.plus(var1);
         }
      }

      return var1;
   }

   static void checkNotNull(Object var0, String var1) {
      if(var0 == null) {
         throw new NullPointerException(var1);
      }
   }

   public static boolean isNegative(BigMoneyProvider var0) {
      boolean var1;
      if(var0 != null && var0.toBigMoney().isNegative()) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public static boolean isNegativeOrZero(BigMoneyProvider var0) {
      boolean var1;
      if(var0 != null && !var0.toBigMoney().isNegativeOrZero()) {
         var1 = false;
      } else {
         var1 = true;
      }

      return var1;
   }

   public static boolean isPositive(BigMoneyProvider var0) {
      boolean var1;
      if(var0 != null && var0.toBigMoney().isPositive()) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public static boolean isPositiveOrZero(BigMoneyProvider var0) {
      boolean var1;
      if(var0 != null && !var0.toBigMoney().isPositiveOrZero()) {
         var1 = false;
      } else {
         var1 = true;
      }

      return var1;
   }

   public static boolean isZero(BigMoneyProvider var0) {
      boolean var1;
      if(var0 != null && !var0.toBigMoney().isZero()) {
         var1 = false;
      } else {
         var1 = true;
      }

      return var1;
   }

   public static BigMoney max(BigMoney var0, BigMoney var1) {
      BigMoney var2;
      if(var0 == null) {
         var2 = var1;
      } else {
         var2 = var0;
         if(var1 != null) {
            var2 = var0;
            if(var0.compareTo((BigMoneyProvider)var1) <= 0) {
               var2 = var1;
            }
         }
      }

      return var2;
   }

   public static Money max(Money var0, Money var1) {
      Money var2;
      if(var0 == null) {
         var2 = var1;
      } else {
         var2 = var0;
         if(var1 != null) {
            var2 = var0;
            if(var0.compareTo((BigMoneyProvider)var1) <= 0) {
               var2 = var1;
            }
         }
      }

      return var2;
   }

   public static BigMoney min(BigMoney var0, BigMoney var1) {
      BigMoney var2;
      if(var0 == null) {
         var2 = var1;
      } else {
         var2 = var0;
         if(var1 != null) {
            var2 = var0;
            if(var0.compareTo((BigMoneyProvider)var1) >= 0) {
               var2 = var1;
            }
         }
      }

      return var2;
   }

   public static Money min(Money var0, Money var1) {
      Money var2;
      if(var0 == null) {
         var2 = var1;
      } else {
         var2 = var0;
         if(var1 != null) {
            var2 = var0;
            if(var0.compareTo((BigMoneyProvider)var1) >= 0) {
               var2 = var1;
            }
         }
      }

      return var2;
   }

   public static BigMoney subtract(BigMoney var0, BigMoney var1) {
      if(var1 != null) {
         if(var0 == null) {
            var0 = var1.negated();
         } else {
            var0 = var0.minus((BigMoneyProvider)var1);
         }
      }

      return var0;
   }

   public static Money subtract(Money var0, Money var1) {
      if(var1 != null) {
         if(var0 == null) {
            var0 = var1.negated();
         } else {
            var0 = var0.minus(var1);
         }
      }

      return var0;
   }
}
