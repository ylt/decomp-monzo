package org.joda.money;

import java.util.List;

public abstract class CurrencyUnitDataProvider {
   protected abstract void registerCurrencies() throws Exception;

   protected final void registerCurrency(String var1, int var2, int var3, List var4) {
      CurrencyUnit.registerCurrency(var1, var2, var3, var4, true);
   }
}
