package org.joda.money;

import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.Iterator;
import org.joda.convert.FromString;
import org.joda.convert.ToString;

public final class Money implements Serializable, Comparable, BigMoneyProvider {
   // $FF: synthetic field
   static final boolean $assertionsDisabled;
   private static final long serialVersionUID = 1L;
   private final BigMoney money;

   static {
      boolean var0;
      if(!Money.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      $assertionsDisabled = var0;
   }

   private Money() {
      this.money = null;
   }

   Money(BigMoney var1) {
      if(!$assertionsDisabled && var1 == null) {
         throw new AssertionError("Joda-Money bug: BigMoney must not be null");
      } else if(!$assertionsDisabled && !var1.isCurrencyScale()) {
         throw new AssertionError("Joda-Money bug: Only currency scale is valid for Money");
      } else {
         this.money = var1;
      }
   }

   public static Money nonNull(Money var0, CurrencyUnit var1) {
      Money var2;
      if(var0 == null) {
         var2 = zero(var1);
      } else {
         var2 = var0;
         if(!var0.getCurrencyUnit().equals(var1)) {
            MoneyUtils.checkNotNull(var1, "Currency must not be null");
            throw new CurrencyMismatchException(var0.getCurrencyUnit(), var1);
         }
      }

      return var2;
   }

   public static Money of(BigMoneyProvider var0) {
      return of(var0, RoundingMode.UNNECESSARY);
   }

   public static Money of(BigMoneyProvider var0, RoundingMode var1) {
      MoneyUtils.checkNotNull(var0, "BigMoneyProvider must not be null");
      MoneyUtils.checkNotNull(var1, "RoundingMode must not be null");
      return new Money(BigMoney.of(var0).withCurrencyScale(var1));
   }

   public static Money of(CurrencyUnit var0, double var1) {
      return of(var0, BigDecimal.valueOf(var1));
   }

   public static Money of(CurrencyUnit var0, double var1, RoundingMode var3) {
      return of(var0, BigDecimal.valueOf(var1), var3);
   }

   public static Money of(CurrencyUnit var0, BigDecimal var1) {
      MoneyUtils.checkNotNull(var0, "Currency must not be null");
      MoneyUtils.checkNotNull(var1, "Amount must not be null");
      if(var1.scale() > var0.getDecimalPlaces()) {
         throw new ArithmeticException("Scale of amount " + var1 + " is greater than the scale of the currency " + var0);
      } else {
         return of(var0, var1, RoundingMode.UNNECESSARY);
      }
   }

   public static Money of(CurrencyUnit var0, BigDecimal var1, RoundingMode var2) {
      MoneyUtils.checkNotNull(var0, "CurrencyUnit must not be null");
      MoneyUtils.checkNotNull(var1, "Amount must not be null");
      MoneyUtils.checkNotNull(var2, "RoundingMode must not be null");
      return new Money(BigMoney.of(var0, var1.setScale(var0.getDecimalPlaces(), var2)));
   }

   public static Money ofMajor(CurrencyUnit var0, long var1) {
      return of(var0, BigDecimal.valueOf(var1), RoundingMode.UNNECESSARY);
   }

   public static Money ofMinor(CurrencyUnit var0, long var1) {
      return new Money(BigMoney.ofMinor(var0, var1));
   }

   @FromString
   public static Money parse(String var0) {
      return of(BigMoney.parse(var0));
   }

   private void readObject(ObjectInputStream var1) throws InvalidObjectException {
      throw new InvalidObjectException("Serialization delegate required");
   }

   public static Money total(Iterable var0) {
      MoneyUtils.checkNotNull(var0, "Money iterator must not be null");
      Iterator var1 = var0.iterator();
      if(!var1.hasNext()) {
         throw new IllegalArgumentException("Money iterator must not be empty");
      } else {
         Money var2 = (Money)var1.next();
         MoneyUtils.checkNotNull(var2, "Money iterator must not contain null entries");

         while(var1.hasNext()) {
            var2 = var2.plus((Money)var1.next());
         }

         return var2;
      }
   }

   public static Money total(CurrencyUnit var0, Iterable var1) {
      return zero(var0).plus(var1);
   }

   public static Money total(CurrencyUnit var0, Money... var1) {
      return zero(var0).plus((Iterable)Arrays.asList(var1));
   }

   public static Money total(Money... var0) {
      MoneyUtils.checkNotNull(var0, "Money array must not be null");
      if(var0.length == 0) {
         throw new IllegalArgumentException("Money array must not be empty");
      } else {
         Money var2 = var0[0];
         MoneyUtils.checkNotNull(var2, "Money arary must not contain null entries");

         for(int var1 = 1; var1 < var0.length; ++var1) {
            var2 = var2.plus(var0[var1]);
         }

         return var2;
      }
   }

   private Money with(BigMoney var1) {
      Money var2;
      if(this.money.equals(var1)) {
         var2 = this;
      } else {
         var2 = new Money(var1);
      }

      return var2;
   }

   private Object writeReplace() {
      return new Ser(77, this);
   }

   public static Money zero(CurrencyUnit var0) {
      MoneyUtils.checkNotNull(var0, "Currency must not be null");
      return new Money(BigMoney.of(var0, BigDecimal.valueOf(0L, var0.getDecimalPlaces())));
   }

   public Money abs() {
      Money var1 = this;
      if(this.isNegative()) {
         var1 = this.negated();
      }

      return var1;
   }

   public int compareTo(BigMoneyProvider var1) {
      return this.money.compareTo(var1);
   }

   public Money convertedTo(CurrencyUnit var1, BigDecimal var2, RoundingMode var3) {
      return this.with(this.money.convertedTo(var1, var2).withCurrencyScale(var3));
   }

   public Money dividedBy(double var1, RoundingMode var3) {
      return this.with(this.money.dividedBy(var1, var3));
   }

   public Money dividedBy(long var1, RoundingMode var3) {
      return this.with(this.money.dividedBy(var1, var3));
   }

   public Money dividedBy(BigDecimal var1, RoundingMode var2) {
      return this.with(this.money.dividedBy(var1, var2));
   }

   public boolean equals(Object var1) {
      boolean var2;
      if(this == var1) {
         var2 = true;
      } else if(var1 instanceof Money) {
         Money var3 = (Money)var1;
         var2 = this.money.equals(var3.money);
      } else {
         var2 = false;
      }

      return var2;
   }

   public BigDecimal getAmount() {
      return this.money.getAmount();
   }

   public BigDecimal getAmountMajor() {
      return this.money.getAmountMajor();
   }

   public int getAmountMajorInt() {
      return this.money.getAmountMajorInt();
   }

   public long getAmountMajorLong() {
      return this.money.getAmountMajorLong();
   }

   public BigDecimal getAmountMinor() {
      return this.money.getAmountMinor();
   }

   public int getAmountMinorInt() {
      return this.money.getAmountMinorInt();
   }

   public long getAmountMinorLong() {
      return this.money.getAmountMinorLong();
   }

   public CurrencyUnit getCurrencyUnit() {
      return this.money.getCurrencyUnit();
   }

   public int getMinorPart() {
      return this.money.getMinorPart();
   }

   public int getScale() {
      return this.money.getScale();
   }

   public int hashCode() {
      return this.money.hashCode() + 3;
   }

   public boolean isEqual(BigMoneyProvider var1) {
      return this.money.isEqual(var1);
   }

   public boolean isGreaterThan(BigMoneyProvider var1) {
      return this.money.isGreaterThan(var1);
   }

   public boolean isLessThan(BigMoneyProvider var1) {
      return this.money.isLessThan(var1);
   }

   public boolean isNegative() {
      return this.money.isNegative();
   }

   public boolean isNegativeOrZero() {
      return this.money.isNegativeOrZero();
   }

   public boolean isPositive() {
      return this.money.isPositive();
   }

   public boolean isPositiveOrZero() {
      return this.money.isPositiveOrZero();
   }

   public boolean isSameCurrency(BigMoneyProvider var1) {
      return this.money.isSameCurrency(var1);
   }

   public boolean isZero() {
      return this.money.isZero();
   }

   public Money minus(double var1) {
      return this.minus(var1, RoundingMode.UNNECESSARY);
   }

   public Money minus(double var1, RoundingMode var3) {
      return this.with(this.money.minusRetainScale(var1, var3));
   }

   public Money minus(Iterable var1) {
      return this.with(this.money.minus(var1));
   }

   public Money minus(BigDecimal var1) {
      return this.minus(var1, RoundingMode.UNNECESSARY);
   }

   public Money minus(BigDecimal var1, RoundingMode var2) {
      return this.with(this.money.minusRetainScale(var1, var2));
   }

   public Money minus(Money var1) {
      return this.with(this.money.minus((BigMoneyProvider)var1));
   }

   public Money minusMajor(long var1) {
      return this.with(this.money.minusMajor(var1));
   }

   public Money minusMinor(long var1) {
      return this.with(this.money.minusMinor(var1));
   }

   public Money multipliedBy(double var1, RoundingMode var3) {
      return this.with(this.money.multiplyRetainScale(var1, var3));
   }

   public Money multipliedBy(long var1) {
      return this.with(this.money.multipliedBy(var1));
   }

   public Money multipliedBy(BigDecimal var1, RoundingMode var2) {
      return this.with(this.money.multiplyRetainScale(var1, var2));
   }

   public Money negated() {
      return this.with(this.money.negated());
   }

   public Money plus(double var1) {
      return this.plus(var1, RoundingMode.UNNECESSARY);
   }

   public Money plus(double var1, RoundingMode var3) {
      return this.with(this.money.plusRetainScale(var1, var3));
   }

   public Money plus(Iterable var1) {
      return this.with(this.money.plus(var1));
   }

   public Money plus(BigDecimal var1) {
      return this.plus(var1, RoundingMode.UNNECESSARY);
   }

   public Money plus(BigDecimal var1, RoundingMode var2) {
      return this.with(this.money.plusRetainScale(var1, var2));
   }

   public Money plus(Money var1) {
      return this.with(this.money.plus((BigMoneyProvider)var1));
   }

   public Money plusMajor(long var1) {
      return this.with(this.money.plusMajor(var1));
   }

   public Money plusMinor(long var1) {
      return this.with(this.money.plusMinor(var1));
   }

   public Money rounded(int var1, RoundingMode var2) {
      return this.with(this.money.rounded(var1, var2));
   }

   public BigMoney toBigMoney() {
      return this.money;
   }

   @ToString
   public String toString() {
      return this.money.toString();
   }

   public Money withAmount(double var1) {
      return this.withAmount(var1, RoundingMode.UNNECESSARY);
   }

   public Money withAmount(double var1, RoundingMode var3) {
      return this.with(this.money.withAmount(var1).withCurrencyScale(var3));
   }

   public Money withAmount(BigDecimal var1) {
      return this.withAmount(var1, RoundingMode.UNNECESSARY);
   }

   public Money withAmount(BigDecimal var1, RoundingMode var2) {
      return this.with(this.money.withAmount(var1).withCurrencyScale(var2));
   }

   public Money withCurrencyUnit(CurrencyUnit var1) {
      return this.withCurrencyUnit(var1, RoundingMode.UNNECESSARY);
   }

   public Money withCurrencyUnit(CurrencyUnit var1, RoundingMode var2) {
      return this.with(this.money.withCurrencyUnit(var1).withCurrencyScale(var2));
   }
}
