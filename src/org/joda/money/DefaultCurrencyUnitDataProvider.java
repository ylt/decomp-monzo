package org.joda.money;

import java.util.regex.Pattern;

class DefaultCurrencyUnitDataProvider extends CurrencyUnitDataProvider {
   private static final Pattern REGEX_LINE = Pattern.compile("([A-Z]{3}),(-1|[0-9]{1,3}),(-1|[0-9]),([A-Z]*)#?.*");

   private void loadCurrenciesFromFile(String param1, boolean param2) throws Exception {
      // $FF: Couldn't be decompiled
   }

   protected void registerCurrencies() throws Exception {
      this.loadCurrenciesFromFile("/org/joda/money/MoneyData.csv", true);
      this.loadCurrenciesFromFile("/org/joda/money/MoneyDataExtension.csv", false);
   }
}
