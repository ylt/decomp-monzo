package org.joda.money;

import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.Iterator;
import java.util.regex.Pattern;
import org.joda.convert.FromString;
import org.joda.convert.ToString;

public final class BigMoney implements Serializable, Comparable, BigMoneyProvider {
   // $FF: synthetic field
   static final boolean $assertionsDisabled;
   private static final Pattern PARSE_REGEX;
   private static final long serialVersionUID = 1L;
   private final BigDecimal amount;
   private final CurrencyUnit currency;

   static {
      boolean var0;
      if(!BigMoney.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      $assertionsDisabled = var0;
      PARSE_REGEX = Pattern.compile("[+-]?[0-9]*[.]?[0-9]*");
   }

   private BigMoney() {
      this.currency = null;
      this.amount = null;
   }

   BigMoney(CurrencyUnit var1, BigDecimal var2) {
      if(!$assertionsDisabled && var1 == null) {
         throw new AssertionError("Joda-Money bug: Currency must not be null");
      } else if(!$assertionsDisabled && var2 == null) {
         throw new AssertionError("Joda-Money bug: Amount must not be null");
      } else {
         this.currency = var1;
         BigDecimal var3 = var2;
         if(var2.scale() < 0) {
            var3 = var2.setScale(0);
         }

         this.amount = var3;
      }
   }

   private BigMoney checkCurrencyEqual(BigMoneyProvider var1) {
      BigMoney var2 = of(var1);
      if(!this.isSameCurrency(var2)) {
         throw new CurrencyMismatchException(this.getCurrencyUnit(), var2.getCurrencyUnit());
      } else {
         return var2;
      }
   }

   public static BigMoney nonNull(BigMoney var0, CurrencyUnit var1) {
      BigMoney var2;
      if(var0 == null) {
         var2 = zero(var1);
      } else {
         var2 = var0;
         if(!var0.getCurrencyUnit().equals(var1)) {
            MoneyUtils.checkNotNull(var1, "Currency must not be null");
            throw new CurrencyMismatchException(var0.getCurrencyUnit(), var1);
         }
      }

      return var2;
   }

   public static BigMoney of(BigMoneyProvider var0) {
      MoneyUtils.checkNotNull(var0, "BigMoneyProvider must not be null");
      BigMoney var1 = var0.toBigMoney();
      MoneyUtils.checkNotNull(var1, "BigMoneyProvider must not return null");
      return var1;
   }

   public static BigMoney of(CurrencyUnit var0, double var1) {
      MoneyUtils.checkNotNull(var0, "Currency must not be null");
      return of(var0, BigDecimal.valueOf(var1).stripTrailingZeros());
   }

   public static BigMoney of(CurrencyUnit var0, BigDecimal var1) {
      MoneyUtils.checkNotNull(var0, "Currency must not be null");
      MoneyUtils.checkNotNull(var1, "Amount must not be null");
      BigDecimal var2 = var1;
      if(var1.getClass() != BigDecimal.class) {
         BigInteger var3 = var1.unscaledValue();
         if(var3 == null) {
            throw new IllegalArgumentException("Illegal BigDecimal subclass");
         }

         if(var3.getClass() != BigInteger.class) {
            var3 = new BigInteger(var3.toString());
         }

         var2 = new BigDecimal(var3, var1.scale());
      }

      return new BigMoney(var0, var2);
   }

   public static BigMoney ofMajor(CurrencyUnit var0, long var1) {
      MoneyUtils.checkNotNull(var0, "CurrencyUnit must not be null");
      return of(var0, BigDecimal.valueOf(var1));
   }

   public static BigMoney ofMinor(CurrencyUnit var0, long var1) {
      MoneyUtils.checkNotNull(var0, "CurrencyUnit must not be null");
      return of(var0, BigDecimal.valueOf(var1, var0.getDecimalPlaces()));
   }

   public static BigMoney ofScale(CurrencyUnit var0, long var1, int var3) {
      MoneyUtils.checkNotNull(var0, "Currency must not be null");
      return of(var0, BigDecimal.valueOf(var1, var3));
   }

   public static BigMoney ofScale(CurrencyUnit var0, BigDecimal var1, int var2) {
      return ofScale(var0, var1, var2, RoundingMode.UNNECESSARY);
   }

   public static BigMoney ofScale(CurrencyUnit var0, BigDecimal var1, int var2, RoundingMode var3) {
      MoneyUtils.checkNotNull(var0, "CurrencyUnit must not be null");
      MoneyUtils.checkNotNull(var1, "Amount must not be null");
      MoneyUtils.checkNotNull(var3, "RoundingMode must not be null");
      return of(var0, var1.setScale(var2, var3));
   }

   @FromString
   public static BigMoney parse(String var0) {
      int var1 = 3;
      MoneyUtils.checkNotNull(var0, "Money must not be null");
      if(var0.length() < 4) {
         throw new IllegalArgumentException("Money '" + var0 + "' cannot be parsed");
      } else {
         String var3;
         for(var3 = var0.substring(0, 3); var1 < var0.length() && var0.charAt(var1) == 32; ++var1) {
            ;
         }

         String var2 = var0.substring(var1);
         if(!PARSE_REGEX.matcher(var2).matches()) {
            throw new IllegalArgumentException("Money amount '" + var0 + "' cannot be parsed");
         } else {
            return of(CurrencyUnit.of(var3), new BigDecimal(var2));
         }
      }
   }

   private void readObject(ObjectInputStream var1) throws InvalidObjectException {
      throw new InvalidObjectException("Serialization delegate required");
   }

   public static BigMoney total(Iterable var0) {
      MoneyUtils.checkNotNull(var0, "Money iterator must not be null");
      Iterator var1 = var0.iterator();
      if(!var1.hasNext()) {
         throw new IllegalArgumentException("Money iterator must not be empty");
      } else {
         BigMoney var2 = of((BigMoneyProvider)var1.next());
         MoneyUtils.checkNotNull(var2, "Money iterator must not contain null entries");

         while(var1.hasNext()) {
            var2 = var2.plus((BigMoneyProvider)var1.next());
         }

         return var2;
      }
   }

   public static BigMoney total(CurrencyUnit var0, Iterable var1) {
      return zero(var0).plus(var1);
   }

   public static BigMoney total(CurrencyUnit var0, BigMoneyProvider... var1) {
      return zero(var0).plus((Iterable)Arrays.asList(var1));
   }

   public static BigMoney total(BigMoneyProvider... var0) {
      MoneyUtils.checkNotNull(var0, "Money array must not be null");
      if(var0.length == 0) {
         throw new IllegalArgumentException("Money array must not be empty");
      } else {
         BigMoney var2 = of(var0[0]);
         MoneyUtils.checkNotNull(var2, "Money array must not contain null entries");

         for(int var1 = 1; var1 < var0.length; ++var1) {
            var2 = var2.plus((BigMoneyProvider)of(var0[var1]));
         }

         return var2;
      }
   }

   private BigMoney with(BigDecimal var1) {
      BigMoney var2;
      if(var1 == this.amount) {
         var2 = this;
      } else {
         var2 = new BigMoney(this.currency, var1);
      }

      return var2;
   }

   private Object writeReplace() {
      return new Ser(66, this);
   }

   public static BigMoney zero(CurrencyUnit var0) {
      return of(var0, BigDecimal.ZERO);
   }

   public static BigMoney zero(CurrencyUnit var0, int var1) {
      return of(var0, BigDecimal.valueOf(0L, var1));
   }

   public BigMoney abs() {
      BigMoney var1 = this;
      if(this.isNegative()) {
         var1 = this.negated();
      }

      return var1;
   }

   public int compareTo(BigMoneyProvider var1) {
      BigMoney var2 = of(var1);
      if(!this.currency.equals(var2.currency)) {
         throw new CurrencyMismatchException(this.getCurrencyUnit(), var2.getCurrencyUnit());
      } else {
         return this.amount.compareTo(var2.amount);
      }
   }

   public BigMoney convertRetainScale(CurrencyUnit var1, BigDecimal var2, RoundingMode var3) {
      return this.convertedTo(var1, var2).withScale(this.getScale(), var3);
   }

   public BigMoney convertedTo(CurrencyUnit var1, BigDecimal var2) {
      MoneyUtils.checkNotNull(var1, "CurrencyUnit must not be null");
      MoneyUtils.checkNotNull(var2, "Multiplier must not be null");
      BigMoney var3;
      if(this.currency == var1) {
         if(var2.compareTo(BigDecimal.ONE) != 0) {
            throw new IllegalArgumentException("Cannot convert to the same currency");
         }

         var3 = this;
      } else {
         if(var2.compareTo(BigDecimal.ZERO) < 0) {
            throw new IllegalArgumentException("Cannot convert using a negative conversion multiplier");
         }

         var3 = of(var1, this.amount.multiply(var2));
      }

      return var3;
   }

   public BigMoney dividedBy(double var1, RoundingMode var3) {
      MoneyUtils.checkNotNull(var3, "RoundingMode must not be null");
      BigMoney var4;
      if(var1 == 1.0D) {
         var4 = this;
      } else {
         BigDecimal var5 = this.amount.divide(BigDecimal.valueOf(var1), var3);
         var4 = of(this.currency, var5);
      }

      return var4;
   }

   public BigMoney dividedBy(long var1, RoundingMode var3) {
      BigMoney var4;
      if(var1 == 1L) {
         var4 = this;
      } else {
         BigDecimal var5 = this.amount.divide(BigDecimal.valueOf(var1), var3);
         var4 = of(this.currency, var5);
      }

      return var4;
   }

   public BigMoney dividedBy(BigDecimal var1, RoundingMode var2) {
      MoneyUtils.checkNotNull(var1, "Divisor must not be null");
      MoneyUtils.checkNotNull(var2, "RoundingMode must not be null");
      BigMoney var3;
      if(var1.compareTo(BigDecimal.ONE) == 0) {
         var3 = this;
      } else {
         var1 = this.amount.divide(var1, var2);
         var3 = of(this.currency, var1);
      }

      return var3;
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(this != var1) {
         if(var1 instanceof BigMoney) {
            BigMoney var3 = (BigMoney)var1;
            if(!this.currency.equals(var3.getCurrencyUnit()) || !this.amount.equals(var3.amount)) {
               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public BigDecimal getAmount() {
      return this.amount;
   }

   public BigDecimal getAmountMajor() {
      return this.amount.setScale(0, RoundingMode.DOWN);
   }

   public int getAmountMajorInt() {
      return this.getAmountMajor().intValueExact();
   }

   public long getAmountMajorLong() {
      return this.getAmountMajor().longValueExact();
   }

   public BigDecimal getAmountMinor() {
      int var1 = this.getCurrencyUnit().getDecimalPlaces();
      return this.amount.setScale(var1, RoundingMode.DOWN).movePointRight(var1);
   }

   public int getAmountMinorInt() {
      return this.getAmountMinor().intValueExact();
   }

   public long getAmountMinorLong() {
      return this.getAmountMinor().longValueExact();
   }

   public CurrencyUnit getCurrencyUnit() {
      return this.currency;
   }

   public int getMinorPart() {
      int var1 = this.getCurrencyUnit().getDecimalPlaces();
      return this.amount.setScale(var1, RoundingMode.DOWN).remainder(BigDecimal.ONE).movePointRight(var1).intValueExact();
   }

   public int getScale() {
      return this.amount.scale();
   }

   public int hashCode() {
      return this.currency.hashCode() ^ this.amount.hashCode();
   }

   public boolean isCurrencyScale() {
      boolean var1;
      if(this.amount.scale() == this.currency.getDecimalPlaces()) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public boolean isEqual(BigMoneyProvider var1) {
      boolean var2;
      if(this.compareTo(var1) == 0) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   public boolean isGreaterThan(BigMoneyProvider var1) {
      boolean var2;
      if(this.compareTo(var1) > 0) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   public boolean isLessThan(BigMoneyProvider var1) {
      boolean var2;
      if(this.compareTo(var1) < 0) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   public boolean isNegative() {
      boolean var1;
      if(this.amount.compareTo(BigDecimal.ZERO) < 0) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public boolean isNegativeOrZero() {
      boolean var1;
      if(this.amount.compareTo(BigDecimal.ZERO) <= 0) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public boolean isPositive() {
      boolean var1;
      if(this.amount.compareTo(BigDecimal.ZERO) > 0) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public boolean isPositiveOrZero() {
      boolean var1;
      if(this.amount.compareTo(BigDecimal.ZERO) >= 0) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public boolean isSameCurrency(BigMoneyProvider var1) {
      return this.currency.equals(of(var1).getCurrencyUnit());
   }

   public boolean isZero() {
      boolean var1;
      if(this.amount.compareTo(BigDecimal.ZERO) == 0) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public BigMoney minus(double var1) {
      BigMoney var3;
      if(var1 == 0.0D) {
         var3 = this;
      } else {
         BigDecimal var4 = this.amount.subtract(BigDecimal.valueOf(var1));
         var3 = of(this.currency, var4);
      }

      return var3;
   }

   public BigMoney minus(Iterable var1) {
      BigDecimal var2 = this.amount;
      Iterator var3 = var1.iterator();

      BigDecimal var4;
      for(var4 = var2; var3.hasNext(); var4 = var4.subtract(this.checkCurrencyEqual((BigMoneyProvider)var3.next()).amount)) {
         ;
      }

      return this.with(var4);
   }

   public BigMoney minus(BigDecimal var1) {
      MoneyUtils.checkNotNull(var1, "Amount must not be null");
      BigMoney var2;
      if(var1.compareTo(BigDecimal.ZERO) == 0) {
         var2 = this;
      } else {
         var1 = this.amount.subtract(var1);
         var2 = of(this.currency, var1);
      }

      return var2;
   }

   public BigMoney minus(BigMoneyProvider var1) {
      return this.minus(this.checkCurrencyEqual(var1).getAmount());
   }

   public BigMoney minusMajor(long var1) {
      BigMoney var3;
      if(var1 == 0L) {
         var3 = this;
      } else {
         BigDecimal var4 = this.amount.subtract(BigDecimal.valueOf(var1));
         var3 = of(this.currency, var4);
      }

      return var3;
   }

   public BigMoney minusMinor(long var1) {
      BigMoney var3;
      if(var1 == 0L) {
         var3 = this;
      } else {
         BigDecimal var4 = this.amount.subtract(BigDecimal.valueOf(var1, this.currency.getDecimalPlaces()));
         var3 = of(this.currency, var4);
      }

      return var3;
   }

   public BigMoney minusRetainScale(double var1, RoundingMode var3) {
      BigMoney var4;
      if(var1 == 0.0D) {
         var4 = this;
      } else {
         BigDecimal var5 = this.amount.subtract(BigDecimal.valueOf(var1)).setScale(this.getScale(), var3);
         var4 = of(this.currency, var5);
      }

      return var4;
   }

   public BigMoney minusRetainScale(BigDecimal var1, RoundingMode var2) {
      MoneyUtils.checkNotNull(var1, "Amount must not be null");
      BigMoney var3;
      if(var1.compareTo(BigDecimal.ZERO) == 0) {
         var3 = this;
      } else {
         var1 = this.amount.subtract(var1).setScale(this.getScale(), var2);
         var3 = of(this.currency, var1);
      }

      return var3;
   }

   public BigMoney minusRetainScale(BigMoneyProvider var1, RoundingMode var2) {
      return this.minusRetainScale(this.checkCurrencyEqual(var1).getAmount(), var2);
   }

   public BigMoney multipliedBy(double var1) {
      BigMoney var3;
      if(var1 == 1.0D) {
         var3 = this;
      } else {
         BigDecimal var4 = this.amount.multiply(BigDecimal.valueOf(var1));
         var3 = of(this.currency, var4);
      }

      return var3;
   }

   public BigMoney multipliedBy(long var1) {
      BigMoney var3;
      if(var1 == 1L) {
         var3 = this;
      } else {
         BigDecimal var4 = this.amount.multiply(BigDecimal.valueOf(var1));
         var3 = of(this.currency, var4);
      }

      return var3;
   }

   public BigMoney multipliedBy(BigDecimal var1) {
      MoneyUtils.checkNotNull(var1, "Multiplier must not be null");
      BigMoney var2;
      if(var1.compareTo(BigDecimal.ONE) == 0) {
         var2 = this;
      } else {
         var1 = this.amount.multiply(var1);
         var2 = of(this.currency, var1);
      }

      return var2;
   }

   public BigMoney multiplyRetainScale(double var1, RoundingMode var3) {
      return this.multiplyRetainScale(BigDecimal.valueOf(var1), var3);
   }

   public BigMoney multiplyRetainScale(BigDecimal var1, RoundingMode var2) {
      MoneyUtils.checkNotNull(var1, "Multiplier must not be null");
      MoneyUtils.checkNotNull(var2, "RoundingMode must not be null");
      BigMoney var3;
      if(var1.compareTo(BigDecimal.ONE) == 0) {
         var3 = this;
      } else {
         var1 = this.amount.multiply(var1).setScale(this.getScale(), var2);
         var3 = of(this.currency, var1);
      }

      return var3;
   }

   public BigMoney negated() {
      BigMoney var1;
      if(this.isZero()) {
         var1 = this;
      } else {
         var1 = of(this.currency, this.amount.negate());
      }

      return var1;
   }

   public BigMoney plus(double var1) {
      BigMoney var3;
      if(var1 == 0.0D) {
         var3 = this;
      } else {
         BigDecimal var4 = this.amount.add(BigDecimal.valueOf(var1));
         var3 = of(this.currency, var4);
      }

      return var3;
   }

   public BigMoney plus(Iterable var1) {
      BigDecimal var2 = this.amount;
      Iterator var3 = var1.iterator();

      BigDecimal var4;
      for(var4 = var2; var3.hasNext(); var4 = var4.add(this.checkCurrencyEqual((BigMoneyProvider)var3.next()).amount)) {
         ;
      }

      return this.with(var4);
   }

   public BigMoney plus(BigDecimal var1) {
      MoneyUtils.checkNotNull(var1, "Amount must not be null");
      BigMoney var2;
      if(var1.compareTo(BigDecimal.ZERO) == 0) {
         var2 = this;
      } else {
         var1 = this.amount.add(var1);
         var2 = of(this.currency, var1);
      }

      return var2;
   }

   public BigMoney plus(BigMoneyProvider var1) {
      return this.plus(this.checkCurrencyEqual(var1).getAmount());
   }

   public BigMoney plusMajor(long var1) {
      BigMoney var3;
      if(var1 == 0L) {
         var3 = this;
      } else {
         BigDecimal var4 = this.amount.add(BigDecimal.valueOf(var1));
         var3 = of(this.currency, var4);
      }

      return var3;
   }

   public BigMoney plusMinor(long var1) {
      BigMoney var3;
      if(var1 == 0L) {
         var3 = this;
      } else {
         BigDecimal var4 = this.amount.add(BigDecimal.valueOf(var1, this.currency.getDecimalPlaces()));
         var3 = of(this.currency, var4);
      }

      return var3;
   }

   public BigMoney plusRetainScale(double var1, RoundingMode var3) {
      BigMoney var4;
      if(var1 == 0.0D) {
         var4 = this;
      } else {
         BigDecimal var5 = this.amount.add(BigDecimal.valueOf(var1)).setScale(this.getScale(), var3);
         var4 = of(this.currency, var5);
      }

      return var4;
   }

   public BigMoney plusRetainScale(BigDecimal var1, RoundingMode var2) {
      MoneyUtils.checkNotNull(var1, "Amount must not be null");
      BigMoney var3;
      if(var1.compareTo(BigDecimal.ZERO) == 0) {
         var3 = this;
      } else {
         var1 = this.amount.add(var1).setScale(this.getScale(), var2);
         var3 = of(this.currency, var1);
      }

      return var3;
   }

   public BigMoney plusRetainScale(BigMoneyProvider var1, RoundingMode var2) {
      return this.plusRetainScale(this.checkCurrencyEqual(var1).getAmount(), var2);
   }

   public BigMoney rounded(int var1, RoundingMode var2) {
      MoneyUtils.checkNotNull(var2, "RoundingMode must not be null");
      BigMoney var4;
      if(var1 >= this.getScale()) {
         var4 = this;
      } else {
         int var3 = this.amount.scale();
         BigDecimal var5 = this.amount.setScale(var1, var2).setScale(var3);
         var4 = of(this.currency, var5);
      }

      return var4;
   }

   public BigMoney toBigMoney() {
      return this;
   }

   public Money toMoney() {
      return Money.of(this);
   }

   public Money toMoney(RoundingMode var1) {
      return Money.of((BigMoneyProvider)this, (RoundingMode)var1);
   }

   @ToString
   public String toString() {
      return this.currency.getCode() + ' ' + this.amount.toPlainString();
   }

   public BigMoney withAmount(double var1) {
      return this.withAmount(BigDecimal.valueOf(var1));
   }

   public BigMoney withAmount(BigDecimal var1) {
      MoneyUtils.checkNotNull(var1, "Amount must not be null");
      BigMoney var2;
      if(this.amount.equals(var1)) {
         var2 = this;
      } else {
         var2 = of(this.currency, var1);
      }

      return var2;
   }

   public BigMoney withCurrencyScale() {
      return this.withScale(this.currency.getDecimalPlaces(), RoundingMode.UNNECESSARY);
   }

   public BigMoney withCurrencyScale(RoundingMode var1) {
      return this.withScale(this.currency.getDecimalPlaces(), var1);
   }

   public BigMoney withCurrencyUnit(CurrencyUnit var1) {
      MoneyUtils.checkNotNull(var1, "CurrencyUnit must not be null");
      BigMoney var2;
      if(this.currency == var1) {
         var2 = this;
      } else {
         var2 = new BigMoney(var1, this.amount);
      }

      return var2;
   }

   public BigMoney withScale(int var1) {
      return this.withScale(var1, RoundingMode.UNNECESSARY);
   }

   public BigMoney withScale(int var1, RoundingMode var2) {
      MoneyUtils.checkNotNull(var2, "RoundingMode must not be null");
      BigMoney var3;
      if(var1 == this.amount.scale()) {
         var3 = this;
      } else {
         var3 = of(this.currency, this.amount.setScale(var1, var2));
      }

      return var3;
   }
}
