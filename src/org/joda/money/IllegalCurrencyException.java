package org.joda.money;

public class IllegalCurrencyException extends IllegalArgumentException {
   private static final long serialVersionUID = 1L;

   public IllegalCurrencyException(String var1) {
      super(var1);
   }
}
