package org.joda.money;

import java.io.Externalizable;
import java.io.IOException;
import java.io.InvalidClassException;
import java.io.InvalidObjectException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.io.StreamCorruptedException;
import java.math.BigDecimal;
import java.math.BigInteger;

final class Ser implements Externalizable {
   static final byte BIG_MONEY = 66;
   static final byte CURRENCY_UNIT = 67;
   static final byte MONEY = 77;
   private Object object;
   private byte type;

   public Ser() {
   }

   Ser(byte var1, Object var2) {
      this.type = var1;
      this.object = var2;
   }

   private BigMoney readBigMoney(ObjectInput var1) throws IOException {
      CurrencyUnit var2 = this.readCurrency(var1);
      byte[] var3 = new byte[var1.readInt()];
      var1.readFully(var3);
      return new BigMoney(var2, new BigDecimal(new BigInteger(var3), var1.readInt()));
   }

   private CurrencyUnit readCurrency(ObjectInput var1) throws IOException {
      String var2 = var1.readUTF();
      CurrencyUnit var3 = CurrencyUnit.of(var2);
      if(var3.getNumericCode() != var1.readShort()) {
         throw new InvalidObjectException("Deserialization found a mismatch in the numeric code for currency " + var2);
      } else if(var3.getDefaultFractionDigits() != var1.readShort()) {
         throw new InvalidObjectException("Deserialization found a mismatch in the decimal places for currency " + var2);
      } else {
         return var3;
      }
   }

   private Object readResolve() {
      return this.object;
   }

   private void writeBigMoney(ObjectOutput var1, BigMoney var2) throws IOException {
      this.writeCurrency(var1, var2.getCurrencyUnit());
      byte[] var3 = var2.getAmount().unscaledValue().toByteArray();
      var1.writeInt(var3.length);
      var1.write(var3);
      var1.writeInt(var2.getScale());
   }

   private void writeCurrency(ObjectOutput var1, CurrencyUnit var2) throws IOException {
      var1.writeUTF(var2.getCode());
      var1.writeShort(var2.getNumericCode());
      var1.writeShort(var2.getDefaultFractionDigits());
   }

   public void readExternal(ObjectInput var1) throws IOException, ClassNotFoundException {
      this.type = var1.readByte();
      switch(this.type) {
      case 66:
         this.object = this.readBigMoney(var1);
         break;
      case 67:
         this.object = this.readCurrency(var1);
         break;
      case 77:
         this.object = new Money(this.readBigMoney(var1));
         break;
      default:
         throw new StreamCorruptedException("Serialization input has invalid type");
      }

   }

   public void writeExternal(ObjectOutput var1) throws IOException {
      var1.writeByte(this.type);
      switch(this.type) {
      case 66:
         this.writeBigMoney(var1, (BigMoney)this.object);
         break;
      case 67:
         this.writeCurrency(var1, (CurrencyUnit)this.object);
         break;
      case 77:
         this.writeBigMoney(var1, ((Money)this.object).toBigMoney());
         break;
      default:
         throw new InvalidClassException("Joda-Money bug: Serialization broken");
      }

   }
}
