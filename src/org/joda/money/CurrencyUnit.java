package org.joda.money;

import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Currency;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.regex.Pattern;
import org.joda.convert.FromString;
import org.joda.convert.ToString;

public final class CurrencyUnit implements Serializable, Comparable {
   // $FF: synthetic field
   static final boolean $assertionsDisabled;
   public static final CurrencyUnit AUD;
   public static final CurrencyUnit CAD;
   public static final CurrencyUnit CHF;
   private static final Pattern CODE;
   public static final CurrencyUnit EUR;
   public static final CurrencyUnit GBP;
   public static final CurrencyUnit JPY;
   public static final CurrencyUnit USD;
   private static final ConcurrentMap currenciesByCode;
   private static final ConcurrentMap currenciesByCountry;
   private static final ConcurrentMap currenciesByNumericCode;
   private static final long serialVersionUID = 327835287287L;
   private final String code;
   private final short decimalPlaces;
   private final short numericCode;

   static {
      boolean var0;
      if(!CurrencyUnit.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      $assertionsDisabled = var0;
      CODE = Pattern.compile("[A-Z][A-Z][A-Z]");
      currenciesByCode = new ConcurrentHashMap();
      currenciesByNumericCode = new ConcurrentHashMap();
      currenciesByCountry = new ConcurrentHashMap();

      try {
         try {
            String var5 = System.getProperty("org.joda.money.CurrencyUnitDataProvider", "org.joda.money.DefaultCurrencyUnitDataProvider");
            ((CurrencyUnitDataProvider)CurrencyUnit.class.getClassLoader().loadClass(var5).asSubclass(CurrencyUnitDataProvider.class).newInstance()).registerCurrencies();
         } catch (SecurityException var2) {
            DefaultCurrencyUnitDataProvider var1 = new DefaultCurrencyUnitDataProvider();
            var1.registerCurrencies();
         }
      } catch (RuntimeException var3) {
         throw var3;
      } catch (Exception var4) {
         throw new RuntimeException(var4.toString(), var4);
      }

      USD = of("USD");
      EUR = of("EUR");
      JPY = of("JPY");
      GBP = of("GBP");
      CHF = of("CHF");
      AUD = of("AUD");
      CAD = of("CAD");
   }

   CurrencyUnit(String var1, short var2, short var3) {
      if(!$assertionsDisabled && var1 == null) {
         throw new AssertionError("Joda-Money bug: Currency code must not be null");
      } else {
         this.code = var1;
         this.numericCode = var2;
         this.decimalPlaces = var3;
      }
   }

   public static CurrencyUnit getInstance(String var0) {
      return of(var0);
   }

   public static CurrencyUnit getInstance(Locale var0) {
      return of(var0);
   }

   @FromString
   public static CurrencyUnit of(String var0) {
      MoneyUtils.checkNotNull(var0, "Currency code must not be null");
      CurrencyUnit var1 = (CurrencyUnit)currenciesByCode.get(var0);
      if(var1 == null) {
         throw new IllegalCurrencyException("Unknown currency '" + var0 + '\'');
      } else {
         return var1;
      }
   }

   public static CurrencyUnit of(Currency var0) {
      MoneyUtils.checkNotNull(var0, "Currency must not be null");
      return of(var0.getCurrencyCode());
   }

   public static CurrencyUnit of(Locale var0) {
      MoneyUtils.checkNotNull(var0, "Locale must not be null");
      CurrencyUnit var1 = (CurrencyUnit)currenciesByCountry.get(var0.getCountry());
      if(var1 == null) {
         throw new IllegalCurrencyException("Unknown currency for locale '" + var0 + '\'');
      } else {
         return var1;
      }
   }

   public static CurrencyUnit ofCountry(String var0) {
      MoneyUtils.checkNotNull(var0, "Country code must not be null");
      CurrencyUnit var1 = (CurrencyUnit)currenciesByCountry.get(var0);
      if(var1 == null) {
         throw new IllegalCurrencyException("Unknown currency for country '" + var0 + '\'');
      } else {
         return var1;
      }
   }

   public static CurrencyUnit ofNumericCode(int var0) {
      CurrencyUnit var1 = (CurrencyUnit)currenciesByNumericCode.get(Integer.valueOf(var0));
      if(var1 == null) {
         throw new IllegalCurrencyException("Unknown currency '" + var0 + '\'');
      } else {
         return var1;
      }
   }

   public static CurrencyUnit ofNumericCode(String var0) {
      MoneyUtils.checkNotNull(var0, "Currency code must not be null");
      CurrencyUnit var1;
      switch(var0.length()) {
      case 1:
         var1 = ofNumericCode(var0.charAt(0) - 48);
         break;
      case 2:
         var1 = ofNumericCode((var0.charAt(0) - 48) * 10 + var0.charAt(1) - 48);
         break;
      case 3:
         var1 = ofNumericCode((var0.charAt(0) - 48) * 100 + (var0.charAt(1) - 48) * 10 + var0.charAt(2) - 48);
         break;
      default:
         throw new IllegalCurrencyException("Unknown currency '" + var0 + '\'');
      }

      return var1;
   }

   private void readObject(ObjectInputStream var1) throws InvalidObjectException {
      throw new InvalidObjectException("Serialization delegate required");
   }

   public static CurrencyUnit registerCurrency(String var0, int var1, int var2, List var3) {
      synchronized(CurrencyUnit.class){}

      CurrencyUnit var6;
      try {
         var6 = registerCurrency(var0, var1, var2, var3, false);
      } finally {
         ;
      }

      return var6;
   }

   public static CurrencyUnit registerCurrency(String param0, int param1, int param2, List param3, boolean param4) {
      // $FF: Couldn't be decompiled
   }

   public static List registeredCurrencies() {
      ArrayList var0 = new ArrayList(currenciesByCode.values());
      Collections.sort(var0);
      return var0;
   }

   private Object writeReplace() {
      return new Ser(67, this);
   }

   public int compareTo(CurrencyUnit var1) {
      return this.code.compareTo(var1.code);
   }

   public boolean equals(Object var1) {
      boolean var2;
      if(var1 == this) {
         var2 = true;
      } else if(var1 instanceof CurrencyUnit) {
         var2 = this.code.equals(((CurrencyUnit)var1).code);
      } else {
         var2 = false;
      }

      return var2;
   }

   public String getCode() {
      return this.code;
   }

   public Set getCountryCodes() {
      HashSet var3 = new HashSet();
      Iterator var2 = currenciesByCountry.entrySet().iterator();

      while(var2.hasNext()) {
         Entry var1 = (Entry)var2.next();
         if(this.equals(var1.getValue())) {
            var3.add(var1.getKey());
         }
      }

      return var3;
   }

   public String getCurrencyCode() {
      return this.code;
   }

   public int getDecimalPlaces() {
      short var1;
      if(this.decimalPlaces < 0) {
         var1 = 0;
      } else {
         var1 = this.decimalPlaces;
      }

      return var1;
   }

   public int getDefaultFractionDigits() {
      return this.decimalPlaces;
   }

   public String getNumeric3Code() {
      String var1;
      if(this.numericCode < 0) {
         var1 = "";
      } else {
         String var2 = Integer.toString(this.numericCode);
         if(var2.length() == 1) {
            var1 = "00" + var2;
         } else {
            var1 = var2;
            if(var2.length() == 2) {
               var1 = "0" + var2;
            }
         }
      }

      return var1;
   }

   public int getNumericCode() {
      return this.numericCode;
   }

   public String getSymbol() {
      String var1;
      try {
         var1 = Currency.getInstance(this.code).getSymbol();
      } catch (IllegalArgumentException var2) {
         var1 = this.code;
      }

      return var1;
   }

   public String getSymbol(Locale var1) {
      MoneyUtils.checkNotNull(var1, "Locale must not be null");

      String var3;
      try {
         var3 = Currency.getInstance(this.code).getSymbol(var1);
      } catch (IllegalArgumentException var2) {
         var3 = this.code;
      }

      return var3;
   }

   public int hashCode() {
      return this.code.hashCode();
   }

   public boolean isPseudoCurrency() {
      boolean var1;
      if(this.decimalPlaces < 0) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public Currency toCurrency() {
      return Currency.getInstance(this.code);
   }

   @ToString
   public String toString() {
      return this.code;
   }
}
