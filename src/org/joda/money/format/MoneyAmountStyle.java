package org.joda.money.format;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public final class MoneyAmountStyle implements Serializable {
   public static final MoneyAmountStyle ASCII_DECIMAL_COMMA_GROUP3_DOT;
   public static final MoneyAmountStyle ASCII_DECIMAL_COMMA_GROUP3_SPACE;
   public static final MoneyAmountStyle ASCII_DECIMAL_COMMA_NO_GROUPING;
   public static final MoneyAmountStyle ASCII_DECIMAL_POINT_GROUP3_COMMA;
   public static final MoneyAmountStyle ASCII_DECIMAL_POINT_GROUP3_SPACE;
   public static final MoneyAmountStyle ASCII_DECIMAL_POINT_NO_GROUPING;
   private static final ConcurrentMap LOCALIZED_CACHE;
   public static final MoneyAmountStyle LOCALIZED_GROUPING;
   public static final MoneyAmountStyle LOCALIZED_NO_GROUPING;
   private static final long serialVersionUID = 1L;
   private final boolean absValue;
   private final int decimalPointCharacter;
   private final int extendedGroupingSize;
   private final boolean forceDecimalPoint;
   private final int groupingCharacter;
   private final int groupingSize;
   private final GroupingStyle groupingStyle;
   private final int negativeCharacter;
   private final int positiveCharacter;
   private final int zeroCharacter;

   static {
      ASCII_DECIMAL_POINT_GROUP3_COMMA = new MoneyAmountStyle(48, 43, 45, 46, GroupingStyle.FULL, 44, 3, 0, false, false);
      ASCII_DECIMAL_POINT_GROUP3_SPACE = new MoneyAmountStyle(48, 43, 45, 46, GroupingStyle.FULL, 32, 3, 0, false, false);
      ASCII_DECIMAL_POINT_NO_GROUPING = new MoneyAmountStyle(48, 43, 45, 46, GroupingStyle.NONE, 44, 3, 0, false, false);
      ASCII_DECIMAL_COMMA_GROUP3_DOT = new MoneyAmountStyle(48, 43, 45, 44, GroupingStyle.FULL, 46, 3, 0, false, false);
      ASCII_DECIMAL_COMMA_GROUP3_SPACE = new MoneyAmountStyle(48, 43, 45, 44, GroupingStyle.FULL, 32, 3, 0, false, false);
      ASCII_DECIMAL_COMMA_NO_GROUPING = new MoneyAmountStyle(48, 43, 45, 44, GroupingStyle.NONE, 46, 3, 0, false, false);
      LOCALIZED_GROUPING = new MoneyAmountStyle(-1, -1, -1, -1, GroupingStyle.FULL, -1, -1, -1, false, false);
      LOCALIZED_NO_GROUPING = new MoneyAmountStyle(-1, -1, -1, -1, GroupingStyle.NONE, -1, -1, -1, false, false);
      LOCALIZED_CACHE = new ConcurrentHashMap();
   }

   private MoneyAmountStyle(int var1, int var2, int var3, int var4, GroupingStyle var5, int var6, int var7, int var8, boolean var9, boolean var10) {
      this.zeroCharacter = var1;
      this.positiveCharacter = var2;
      this.negativeCharacter = var3;
      this.decimalPointCharacter = var4;
      this.groupingStyle = var5;
      this.groupingCharacter = var6;
      this.groupingSize = var7;
      this.extendedGroupingSize = var8;
      this.forceDecimalPoint = var9;
      this.absValue = var10;
   }

   private static MoneyAmountStyle getLocalizedStyle(Locale var0) {
      MoneyAmountStyle var3 = (MoneyAmountStyle)LOCALIZED_CACHE.get(var0);
      MoneyAmountStyle var2 = var3;
      if(var3 == null) {
         DecimalFormatSymbols var5;
         try {
            var5 = (DecimalFormatSymbols)DecimalFormatSymbols.class.getMethod("getInstance", new Class[]{Locale.class}).invoke((Object)null, new Object[]{var0});
         } catch (Exception var4) {
            var5 = new DecimalFormatSymbols(var0);
         }

         NumberFormat var6 = NumberFormat.getCurrencyInstance(var0);
         int var1;
         if(var6 instanceof DecimalFormat) {
            var1 = ((DecimalFormat)var6).getGroupingSize();
         } else {
            var1 = 3;
         }

         var2 = new MoneyAmountStyle(var5.getZeroDigit(), 43, var5.getMinusSign(), var5.getMonetaryDecimalSeparator(), GroupingStyle.FULL, var5.getGroupingSeparator(), var1, 0, false, false);
         LOCALIZED_CACHE.putIfAbsent(var0, var2);
      }

      return var2;
   }

   public static MoneyAmountStyle of(Locale var0) {
      return getLocalizedStyle(var0);
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(var1 != this) {
         if(!(var1 instanceof MoneyAmountStyle)) {
            var2 = false;
         } else {
            MoneyAmountStyle var3 = (MoneyAmountStyle)var1;
            if(this.zeroCharacter != var3.zeroCharacter || this.positiveCharacter != var3.positiveCharacter || this.negativeCharacter != var3.negativeCharacter || this.decimalPointCharacter != var3.decimalPointCharacter || this.groupingStyle != var3.groupingStyle || this.groupingCharacter != var3.groupingCharacter || this.groupingSize != var3.groupingSize || this.forceDecimalPoint != var3.forceDecimalPoint || this.absValue != var3.absValue) {
               var2 = false;
            }
         }
      }

      return var2;
   }

   public Character getDecimalPointCharacter() {
      Character var1;
      if(this.decimalPointCharacter < 0) {
         var1 = null;
      } else {
         var1 = Character.valueOf((char)this.decimalPointCharacter);
      }

      return var1;
   }

   public Integer getExtendedGroupingSize() {
      Integer var1;
      if(this.extendedGroupingSize < 0) {
         var1 = null;
      } else {
         var1 = Integer.valueOf(this.extendedGroupingSize);
      }

      return var1;
   }

   public Character getGroupingCharacter() {
      Character var1;
      if(this.groupingCharacter < 0) {
         var1 = null;
      } else {
         var1 = Character.valueOf((char)this.groupingCharacter);
      }

      return var1;
   }

   public Integer getGroupingSize() {
      Integer var1;
      if(this.groupingSize < 0) {
         var1 = null;
      } else {
         var1 = Integer.valueOf(this.groupingSize);
      }

      return var1;
   }

   public GroupingStyle getGroupingStyle() {
      return this.groupingStyle;
   }

   public Character getNegativeSignCharacter() {
      Character var1;
      if(this.negativeCharacter < 0) {
         var1 = null;
      } else {
         var1 = Character.valueOf((char)this.negativeCharacter);
      }

      return var1;
   }

   public Character getPositiveSignCharacter() {
      Character var1;
      if(this.positiveCharacter < 0) {
         var1 = null;
      } else {
         var1 = Character.valueOf((char)this.positiveCharacter);
      }

      return var1;
   }

   public Character getZeroCharacter() {
      Character var1;
      if(this.zeroCharacter < 0) {
         var1 = null;
      } else {
         var1 = Character.valueOf((char)this.zeroCharacter);
      }

      return var1;
   }

   public int hashCode() {
      int var6 = this.zeroCharacter;
      int var5 = this.positiveCharacter;
      int var9 = this.negativeCharacter;
      int var8 = this.decimalPointCharacter;
      int var3 = this.groupingStyle.hashCode();
      int var7 = this.groupingCharacter;
      int var4 = this.groupingSize;
      byte var1;
      if(this.forceDecimalPoint) {
         var1 = 2;
      } else {
         var1 = 4;
      }

      byte var2;
      if(this.absValue) {
         var2 = 3;
      } else {
         var2 = 9;
      }

      return var2 + var4 * 17 + 13 + var6 * 17 + var5 * 17 + var9 * 17 + var8 * 17 + var3 * 17 + var7 * 17 + var1;
   }

   public boolean isAbsValue() {
      return this.absValue;
   }

   public boolean isForcedDecimalPoint() {
      return this.forceDecimalPoint;
   }

   public MoneyAmountStyle localize(Locale var1) {
      MoneyFormatter.checkNotNull(var1, "Locale must not be null");
      MoneyAmountStyle var4 = null;
      MoneyAmountStyle var2;
      if(this.zeroCharacter < 0) {
         var4 = getLocalizedStyle(var1);
         var2 = this.withZeroCharacter(var4.getZeroCharacter());
      } else {
         var2 = this;
      }

      MoneyAmountStyle var3 = var2;
      if(this.positiveCharacter < 0) {
         var4 = getLocalizedStyle(var1);
         var3 = var2.withPositiveSignCharacter(var4.getPositiveSignCharacter());
      }

      var2 = var4;
      MoneyAmountStyle var5 = var3;
      if(this.negativeCharacter < 0) {
         var2 = getLocalizedStyle(var1);
         var5 = var3.withNegativeSignCharacter(var2.getNegativeSignCharacter());
      }

      var3 = var2;
      var4 = var5;
      if(this.decimalPointCharacter < 0) {
         var3 = var2;
         if(var2 == null) {
            var3 = getLocalizedStyle(var1);
         }

         var4 = var5.withDecimalPointCharacter(var3.getDecimalPointCharacter());
      }

      var2 = var3;
      var5 = var4;
      if(this.groupingCharacter < 0) {
         var2 = var3;
         if(var3 == null) {
            var2 = getLocalizedStyle(var1);
         }

         var5 = var4.withGroupingCharacter(var2.getGroupingCharacter());
      }

      var4 = var2;
      var3 = var5;
      if(this.groupingSize < 0) {
         var4 = var2;
         if(var2 == null) {
            var4 = getLocalizedStyle(var1);
         }

         var3 = var5.withGroupingSize(var4.getGroupingSize());
      }

      var2 = var3;
      if(this.extendedGroupingSize < 0) {
         var2 = var4;
         if(var4 == null) {
            var2 = getLocalizedStyle(var1);
         }

         var2 = var3.withExtendedGroupingSize(var2.getExtendedGroupingSize());
      }

      return var2;
   }

   public String toString() {
      return "MoneyAmountStyle['" + this.getZeroCharacter() + "','" + this.getPositiveSignCharacter() + "','" + this.getNegativeSignCharacter() + "','" + this.getDecimalPointCharacter() + "','" + this.getGroupingStyle() + "," + this.getGroupingCharacter() + "','" + this.getGroupingSize() + "'," + this.isForcedDecimalPoint() + "'," + this.isAbsValue() + "]";
   }

   public MoneyAmountStyle withAbsValue(boolean var1) {
      MoneyAmountStyle var2;
      if(this.absValue == var1) {
         var2 = this;
      } else {
         var2 = new MoneyAmountStyle(this.zeroCharacter, this.positiveCharacter, this.negativeCharacter, this.decimalPointCharacter, this.groupingStyle, this.groupingCharacter, this.groupingSize, this.extendedGroupingSize, this.forceDecimalPoint, var1);
      }

      return var2;
   }

   public MoneyAmountStyle withDecimalPointCharacter(Character var1) {
      int var2;
      if(var1 == null) {
         var2 = -1;
      } else {
         var2 = var1.charValue();
      }

      MoneyAmountStyle var3;
      if(var2 == this.decimalPointCharacter) {
         var3 = this;
      } else {
         var3 = new MoneyAmountStyle(this.zeroCharacter, this.positiveCharacter, this.negativeCharacter, var2, this.groupingStyle, this.groupingCharacter, this.groupingSize, this.extendedGroupingSize, this.forceDecimalPoint, this.absValue);
      }

      return var3;
   }

   public MoneyAmountStyle withExtendedGroupingSize(Integer var1) {
      int var2;
      if(var1 == null) {
         var2 = -1;
      } else {
         var2 = var1.intValue();
      }

      if(var1 != null && var2 < 0) {
         throw new IllegalArgumentException("Extended grouping size must not be negative");
      } else {
         MoneyAmountStyle var3;
         if(var2 == this.extendedGroupingSize) {
            var3 = this;
         } else {
            var3 = new MoneyAmountStyle(this.zeroCharacter, this.positiveCharacter, this.negativeCharacter, this.decimalPointCharacter, this.groupingStyle, this.groupingCharacter, this.groupingSize, var2, this.forceDecimalPoint, this.absValue);
         }

         return var3;
      }
   }

   public MoneyAmountStyle withForcedDecimalPoint(boolean var1) {
      MoneyAmountStyle var2;
      if(this.forceDecimalPoint == var1) {
         var2 = this;
      } else {
         var2 = new MoneyAmountStyle(this.zeroCharacter, this.positiveCharacter, this.negativeCharacter, this.decimalPointCharacter, this.groupingStyle, this.groupingCharacter, this.groupingSize, this.extendedGroupingSize, var1, this.absValue);
      }

      return var2;
   }

   public MoneyAmountStyle withGroupingCharacter(Character var1) {
      int var2;
      if(var1 == null) {
         var2 = -1;
      } else {
         var2 = var1.charValue();
      }

      MoneyAmountStyle var3;
      if(var2 == this.groupingCharacter) {
         var3 = this;
      } else {
         var3 = new MoneyAmountStyle(this.zeroCharacter, this.positiveCharacter, this.negativeCharacter, this.decimalPointCharacter, this.groupingStyle, var2, this.groupingSize, this.extendedGroupingSize, this.forceDecimalPoint, this.absValue);
      }

      return var3;
   }

   public MoneyAmountStyle withGroupingSize(Integer var1) {
      int var2;
      if(var1 == null) {
         var2 = -1;
      } else {
         var2 = var1.intValue();
      }

      if(var1 != null && var2 <= 0) {
         throw new IllegalArgumentException("Grouping size must be greater than zero");
      } else {
         MoneyAmountStyle var3;
         if(var2 == this.groupingSize) {
            var3 = this;
         } else {
            var3 = new MoneyAmountStyle(this.zeroCharacter, this.positiveCharacter, this.negativeCharacter, this.decimalPointCharacter, this.groupingStyle, this.groupingCharacter, var2, this.extendedGroupingSize, this.forceDecimalPoint, this.absValue);
         }

         return var3;
      }
   }

   public MoneyAmountStyle withGroupingStyle(GroupingStyle var1) {
      MoneyFormatter.checkNotNull(var1, "groupingStyle");
      MoneyAmountStyle var2;
      if(this.groupingStyle == var1) {
         var2 = this;
      } else {
         var2 = new MoneyAmountStyle(this.zeroCharacter, this.positiveCharacter, this.negativeCharacter, this.decimalPointCharacter, var1, this.groupingCharacter, this.groupingSize, this.extendedGroupingSize, this.forceDecimalPoint, this.absValue);
      }

      return var2;
   }

   public MoneyAmountStyle withNegativeSignCharacter(Character var1) {
      int var2;
      if(var1 == null) {
         var2 = -1;
      } else {
         var2 = var1.charValue();
      }

      MoneyAmountStyle var3;
      if(var2 == this.negativeCharacter) {
         var3 = this;
      } else {
         var3 = new MoneyAmountStyle(this.zeroCharacter, this.positiveCharacter, var2, this.decimalPointCharacter, this.groupingStyle, this.groupingCharacter, this.groupingSize, this.extendedGroupingSize, this.forceDecimalPoint, this.absValue);
      }

      return var3;
   }

   public MoneyAmountStyle withPositiveSignCharacter(Character var1) {
      int var2;
      if(var1 == null) {
         var2 = -1;
      } else {
         var2 = var1.charValue();
      }

      MoneyAmountStyle var3;
      if(var2 == this.positiveCharacter) {
         var3 = this;
      } else {
         var3 = new MoneyAmountStyle(this.zeroCharacter, var2, this.negativeCharacter, this.decimalPointCharacter, this.groupingStyle, this.groupingCharacter, this.groupingSize, this.extendedGroupingSize, this.forceDecimalPoint, this.absValue);
      }

      return var3;
   }

   public MoneyAmountStyle withZeroCharacter(Character var1) {
      int var2;
      if(var1 == null) {
         var2 = -1;
      } else {
         var2 = var1.charValue();
      }

      MoneyAmountStyle var3;
      if(var2 == this.zeroCharacter) {
         var3 = this;
      } else {
         var3 = new MoneyAmountStyle(var2, this.positiveCharacter, this.negativeCharacter, this.decimalPointCharacter, this.groupingStyle, this.groupingCharacter, this.groupingSize, this.extendedGroupingSize, this.forceDecimalPoint, this.absValue);
      }

      return var3;
   }
}
