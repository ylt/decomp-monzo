package org.joda.money.format;

import java.io.IOException;
import org.joda.money.BigMoney;

public interface MoneyPrinter {
   void print(MoneyPrintContext var1, Appendable var2, BigMoney var3) throws IOException;
}
