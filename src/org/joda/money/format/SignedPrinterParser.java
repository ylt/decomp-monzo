package org.joda.money.format;

import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import org.joda.money.BigMoney;

final class SignedPrinterParser implements Serializable, MoneyParser, MoneyPrinter {
   private static final long serialVersionUID = 1L;
   private final MoneyFormatter whenNegative;
   private final MoneyFormatter whenPositive;
   private final MoneyFormatter whenZero;

   SignedPrinterParser(MoneyFormatter var1, MoneyFormatter var2, MoneyFormatter var3) {
      this.whenPositive = var1;
      this.whenZero = var2;
      this.whenNegative = var3;
   }

   public void parse(MoneyParseContext var1) {
      MoneyParseContext var3 = var1.createChild();
      this.whenPositive.getPrinterParser().parse(var3);
      MoneyParseContext var4 = var1.createChild();
      this.whenZero.getPrinterParser().parse(var4);
      MoneyParseContext var5 = var1.createChild();
      this.whenNegative.getPrinterParser().parse(var5);
      if(var3.isError()) {
         var3 = null;
      }

      MoneyParseContext var2 = var3;
      if(!var4.isError()) {
         label43: {
            if(var3 != null) {
               var2 = var3;
               if(var4.getIndex() <= var3.getIndex()) {
                  break label43;
               }
            }

            var2 = var4;
         }
      }

      var3 = var2;
      if(!var5.isError()) {
         label37: {
            if(var2 != null) {
               var3 = var2;
               if(var5.getIndex() <= var2.getIndex()) {
                  break label37;
               }
            }

            var3 = var5;
         }
      }

      if(var3 == null) {
         var1.setError();
      } else {
         var1.mergeChild(var3);
         if(var3 == var4) {
            if(var1.getAmount() == null || var1.getAmount().compareTo(BigDecimal.ZERO) != 0) {
               var1.setAmount(BigDecimal.ZERO);
            }
         } else if(var3 == var5 && var1.getAmount().compareTo(BigDecimal.ZERO) > 0) {
            var1.setAmount(var1.getAmount().negate());
         }
      }

   }

   public void print(MoneyPrintContext var1, Appendable var2, BigMoney var3) throws IOException {
      MoneyFormatter var4;
      if(var3.isZero()) {
         var4 = this.whenZero;
      } else if(var3.isPositive()) {
         var4 = this.whenPositive;
      } else {
         var4 = this.whenNegative;
      }

      var4.getPrinterParser().print(var1, var2, var3);
   }

   public String toString() {
      return "PositiveZeroNegative(" + this.whenPositive + "," + this.whenZero + "," + this.whenNegative + ")";
   }
}
