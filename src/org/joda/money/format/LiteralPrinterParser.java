package org.joda.money.format;

import java.io.IOException;
import java.io.Serializable;
import org.joda.money.BigMoney;

final class LiteralPrinterParser implements Serializable, MoneyParser, MoneyPrinter {
   private static final long serialVersionUID = 1L;
   private final String literal;

   LiteralPrinterParser(String var1) {
      this.literal = var1;
   }

   public void parse(MoneyParseContext var1) {
      int var2 = var1.getIndex() + this.literal.length();
      if(var2 <= var1.getTextLength() && var1.getTextSubstring(var1.getIndex(), var2).equals(this.literal)) {
         var1.setIndex(var2);
      } else {
         var1.setError();
      }

   }

   public void print(MoneyPrintContext var1, Appendable var2, BigMoney var3) throws IOException {
      var2.append(this.literal);
   }

   public String toString() {
      return "'" + this.literal + "'";
   }
}
