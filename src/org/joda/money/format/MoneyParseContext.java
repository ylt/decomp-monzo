package org.joda.money.format;

import java.math.BigDecimal;
import java.text.ParsePosition;
import java.util.Locale;
import org.joda.money.BigMoney;
import org.joda.money.CurrencyUnit;

public final class MoneyParseContext {
   private BigDecimal amount;
   private CurrencyUnit currency;
   private Locale locale;
   private CharSequence text;
   private int textErrorIndex = -1;
   private int textIndex;

   MoneyParseContext(Locale var1, CharSequence var2, int var3) {
      this.locale = var1;
      this.text = var2;
      this.textIndex = var3;
   }

   MoneyParseContext(Locale var1, CharSequence var2, int var3, int var4, CurrencyUnit var5, BigDecimal var6) {
      this.locale = var1;
      this.text = var2;
      this.textIndex = var3;
      this.textErrorIndex = var4;
      this.currency = var5;
      this.amount = var6;
   }

   MoneyParseContext createChild() {
      return new MoneyParseContext(this.locale, this.text, this.textIndex, this.textErrorIndex, this.currency, this.amount);
   }

   public BigDecimal getAmount() {
      return this.amount;
   }

   public CurrencyUnit getCurrency() {
      return this.currency;
   }

   public int getErrorIndex() {
      return this.textErrorIndex;
   }

   public int getIndex() {
      return this.textIndex;
   }

   public Locale getLocale() {
      return this.locale;
   }

   public CharSequence getText() {
      return this.text;
   }

   public int getTextLength() {
      return this.text.length();
   }

   public String getTextSubstring(int var1, int var2) {
      return this.text.subSequence(var1, var2).toString();
   }

   public boolean isComplete() {
      boolean var1;
      if(this.currency != null && this.amount != null) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public boolean isError() {
      boolean var1;
      if(this.textErrorIndex >= 0) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public boolean isFullyParsed() {
      boolean var1;
      if(this.textIndex == this.getTextLength()) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   void mergeChild(MoneyParseContext var1) {
      this.setLocale(var1.getLocale());
      this.setText(var1.getText());
      this.setIndex(var1.getIndex());
      this.setErrorIndex(var1.getErrorIndex());
      this.setCurrency(var1.getCurrency());
      this.setAmount(var1.getAmount());
   }

   public void setAmount(BigDecimal var1) {
      this.amount = var1;
   }

   public void setCurrency(CurrencyUnit var1) {
      this.currency = var1;
   }

   public void setError() {
      this.textErrorIndex = this.textIndex;
   }

   public void setErrorIndex(int var1) {
      this.textErrorIndex = var1;
   }

   public void setIndex(int var1) {
      this.textIndex = var1;
   }

   public void setLocale(Locale var1) {
      MoneyFormatter.checkNotNull(var1, "Locale must not be null");
      this.locale = var1;
   }

   public void setText(CharSequence var1) {
      MoneyFormatter.checkNotNull(var1, "Text must not be null");
      this.text = var1;
   }

   public BigMoney toBigMoney() {
      if(this.currency == null) {
         throw new MoneyFormatException("Cannot convert to BigMoney as no currency found");
      } else if(this.amount == null) {
         throw new MoneyFormatException("Cannot convert to BigMoney as no amount found");
      } else {
         return BigMoney.of(this.currency, this.amount);
      }
   }

   public ParsePosition toParsePosition() {
      ParsePosition var1 = new ParsePosition(this.textIndex);
      var1.setErrorIndex(this.textErrorIndex);
      return var1;
   }
}
