package org.joda.money.format;

import java.io.IOException;
import java.io.Serializable;
import java.util.Arrays;
import org.joda.money.BigMoney;

final class MultiPrinterParser implements Serializable, MoneyParser, MoneyPrinter {
   private static final long serialVersionUID = 1L;
   private final MoneyParser[] parsers;
   private final MoneyPrinter[] printers;

   MultiPrinterParser(MoneyPrinter[] var1, MoneyParser[] var2) {
      this.printers = var1;
      this.parsers = var2;
   }

   void appendTo(MoneyFormatterBuilder var1) {
      for(int var2 = 0; var2 < this.printers.length; ++var2) {
         var1.append(this.printers[var2], this.parsers[var2]);
      }

   }

   boolean isParser() {
      boolean var1;
      if(!Arrays.asList(this.parsers).contains((Object)null)) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   boolean isPrinter() {
      boolean var1;
      if(!Arrays.asList(this.printers).contains((Object)null)) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public void parse(MoneyParseContext var1) {
      MoneyParser[] var4 = this.parsers;
      int var3 = var4.length;

      for(int var2 = 0; var2 < var3; ++var2) {
         var4[var2].parse(var1);
         if(var1.isError()) {
            break;
         }
      }

   }

   public void print(MoneyPrintContext var1, Appendable var2, BigMoney var3) throws IOException {
      MoneyPrinter[] var6 = this.printers;
      int var5 = var6.length;

      for(int var4 = 0; var4 < var5; ++var4) {
         var6[var4].print(var1, var2, var3);
      }

   }

   public String toString() {
      byte var2 = 0;
      StringBuilder var5 = new StringBuilder();
      int var1;
      int var3;
      if(this.isPrinter()) {
         MoneyPrinter[] var4 = this.printers;
         var3 = var4.length;

         for(var1 = 0; var1 < var3; ++var1) {
            var5.append(var4[var1].toString());
         }
      }

      StringBuilder var7 = new StringBuilder();
      if(this.isParser()) {
         MoneyParser[] var6 = this.parsers;
         var3 = var6.length;

         for(var1 = var2; var1 < var3; ++var1) {
            var7.append(var6[var1].toString());
         }
      }

      String var9 = var5.toString();
      String var10 = var7.toString();
      String var8;
      if(this.isPrinter() && !this.isParser()) {
         var8 = var9;
      } else if(this.isParser() && !this.isPrinter()) {
         var8 = var10;
      } else {
         var8 = var9;
         if(!var9.equals(var10)) {
            var8 = var9 + ":" + var10;
         }
      }

      return var8;
   }
}
