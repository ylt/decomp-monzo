package org.joda.money.format;

import java.io.IOException;
import java.io.Serializable;
import java.util.Locale;
import org.joda.money.BigMoney;
import org.joda.money.BigMoneyProvider;
import org.joda.money.Money;

public final class MoneyFormatter implements Serializable {
   // $FF: synthetic field
   static final boolean $assertionsDisabled;
   private static final long serialVersionUID = 2385346258L;
   private final Locale locale;
   private final MultiPrinterParser printerParser;

   static {
      boolean var0;
      if(!MoneyFormatter.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      $assertionsDisabled = var0;
   }

   private MoneyFormatter(Locale var1, MultiPrinterParser var2) {
      if(!$assertionsDisabled && var1 == null) {
         throw new AssertionError();
      } else if(!$assertionsDisabled && var2 == null) {
         throw new AssertionError();
      } else {
         this.locale = var1;
         this.printerParser = var2;
      }
   }

   MoneyFormatter(Locale var1, MoneyPrinter[] var2, MoneyParser[] var3) {
      if(!$assertionsDisabled && var1 == null) {
         throw new AssertionError();
      } else if(!$assertionsDisabled && var2 == null) {
         throw new AssertionError();
      } else if(!$assertionsDisabled && var3 == null) {
         throw new AssertionError();
      } else if(!$assertionsDisabled && var2.length != var3.length) {
         throw new AssertionError();
      } else {
         this.locale = var1;
         this.printerParser = new MultiPrinterParser(var2, var3);
      }
   }

   static void checkNotNull(Object var0, String var1) {
      if(var0 == null) {
         throw new NullPointerException(var1);
      }
   }

   public Locale getLocale() {
      return this.locale;
   }

   MultiPrinterParser getPrinterParser() {
      return this.printerParser;
   }

   public boolean isParser() {
      return this.printerParser.isParser();
   }

   public boolean isPrinter() {
      return this.printerParser.isPrinter();
   }

   public MoneyParseContext parse(CharSequence var1, int var2) {
      checkNotNull(var1, "Text must not be null");
      if(var2 >= 0 && var2 <= var1.length()) {
         if(!this.isParser()) {
            throw new UnsupportedOperationException("MoneyFomatter has not been configured to be able to parse");
         } else {
            MoneyParseContext var3 = new MoneyParseContext(this.locale, var1, var2);
            this.printerParser.parse(var3);
            return var3;
         }
      } else {
         throw new StringIndexOutOfBoundsException("Invalid start index: " + var2);
      }
   }

   public BigMoney parseBigMoney(CharSequence var1) {
      checkNotNull(var1, "Text must not be null");
      MoneyParseContext var2 = this.parse(var1, 0);
      if(!var2.isError() && var2.isFullyParsed() && var2.isComplete()) {
         return var2.toBigMoney();
      } else {
         String var3;
         if(var1.length() > 64) {
            var3 = var1.subSequence(0, 64).toString() + "...";
         } else {
            var3 = var1.toString();
         }

         if(var2.isError()) {
            throw new MoneyFormatException("Text could not be parsed at index " + var2.getErrorIndex() + ": " + var3);
         } else if(!var2.isFullyParsed()) {
            throw new MoneyFormatException("Unparsed text found at index " + var2.getIndex() + ": " + var3);
         } else {
            throw new MoneyFormatException("Parsing did not find both currency and amount: " + var3);
         }
      }
   }

   public Money parseMoney(CharSequence var1) {
      return this.parseBigMoney(var1).toMoney();
   }

   public String print(BigMoneyProvider var1) {
      StringBuilder var2 = new StringBuilder();
      this.print(var2, var1);
      return var2.toString();
   }

   public void print(Appendable var1, BigMoneyProvider var2) {
      try {
         this.printIO(var1, var2);
      } catch (IOException var3) {
         throw new MoneyFormatException(var3.getMessage(), var3);
      }
   }

   public void printIO(Appendable var1, BigMoneyProvider var2) throws IOException {
      checkNotNull(var2, "BigMoneyProvider must not be null");
      if(!this.isPrinter()) {
         throw new UnsupportedOperationException("MoneyFomatter has not been configured to be able to print");
      } else {
         BigMoney var4 = BigMoney.of(var2);
         MoneyPrintContext var3 = new MoneyPrintContext(this.locale);
         this.printerParser.print(var3, var1, var4);
      }
   }

   public String toString() {
      return this.printerParser.toString();
   }

   public MoneyFormatter withLocale(Locale var1) {
      checkNotNull(var1, "Locale must not be null");
      return new MoneyFormatter(var1, this.printerParser);
   }
}
