package org.joda.money.format;

import java.util.Locale;

public final class MoneyPrintContext {
   private Locale locale;

   MoneyPrintContext(Locale var1) {
      this.locale = var1;
   }

   public Locale getLocale() {
      return this.locale;
   }

   public void setLocale(Locale var1) {
      MoneyFormatter.checkNotNull(var1, "Locale must not be null");
      this.locale = var1;
   }
}
