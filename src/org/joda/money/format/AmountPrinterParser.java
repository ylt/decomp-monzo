package org.joda.money.format;

import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import org.joda.money.BigMoney;

final class AmountPrinterParser implements Serializable, MoneyParser, MoneyPrinter {
   private static final long serialVersionUID = 1L;
   private final MoneyAmountStyle style;

   AmountPrinterParser(MoneyAmountStyle var1) {
      this.style = var1;
   }

   private boolean isPostGroupingPoint(int var1, int var2, int var3, int var4) {
      boolean var5 = true;
      boolean var6;
      if(var1 + 1 >= var2) {
         var6 = true;
      } else {
         var6 = false;
      }

      if(var1 > var3) {
         if((var1 - var3) % var4 != var4 - 1 || var6) {
            var5 = false;
         }
      } else if(var1 % var3 != var3 - 1 || var6) {
         var5 = false;
      }

      return var5;
   }

   private boolean isPreGroupingPoint(int var1, int var2, int var3) {
      boolean var4 = true;
      if(var1 >= var2 + var3) {
         if((var1 - var2) % var3 != 0) {
            var4 = false;
         }
      } else if(var1 % var2 != 0) {
         var4 = false;
      }

      return var4;
   }

   public void parse(MoneyParseContext var1) {
      int var7 = var1.getTextLength();
      MoneyAmountStyle var9 = this.style.localize(var1.getLocale());
      char[] var8 = new char[var7 - var1.getIndex()];
      int var2 = var1.getIndex();
      byte var3;
      int var4;
      boolean var13;
      if(var2 < var7) {
         CharSequence var10 = var1.getText();
         var4 = var2 + 1;
         char var12 = var10.charAt(var2);
         if(var12 == var9.getNegativeSignCharacter().charValue()) {
            var8[0] = 45;
            var13 = false;
            var3 = 1;
         } else if(var12 == var9.getPositiveSignCharacter().charValue()) {
            var8[0] = 43;
            var13 = false;
            var3 = 1;
         } else if(var12 >= var9.getZeroCharacter().charValue() && var12 < var9.getZeroCharacter().charValue() + 10) {
            var8[0] = (char)(var12 + 48 - var9.getZeroCharacter().charValue());
            var13 = false;
            var3 = 1;
         } else {
            if(var12 != var9.getDecimalPointCharacter().charValue()) {
               var1.setError();
               return;
            }

            var8[0] = 46;
            var13 = true;
            var3 = 1;
         }
      } else {
         var4 = var2;
         var3 = 0;
         var13 = false;
      }

      boolean var6 = false;
      int var5 = var3;

      boolean var14;
      int var17;
      for(var14 = var6; var4 < var7; var4 = var17) {
         char var16 = var1.getText().charAt(var4);
         if(var16 >= var9.getZeroCharacter().charValue() && var16 < var9.getZeroCharacter().charValue() + 10) {
            var8[var5] = (char)(var16 + 48 - var9.getZeroCharacter().charValue());
            ++var5;
            var6 = false;
            var14 = var13;
            var13 = var6;
         } else if(var16 == var9.getDecimalPointCharacter().charValue() && !var13) {
            var8[var5] = 46;
            var14 = true;
            ++var5;
            var13 = false;
         } else {
            if(var16 != var9.getGroupingCharacter().charValue() || var14) {
               break;
            }

            var14 = var13;
            var13 = true;
         }

         var17 = var4 + 1;
         boolean var15 = var13;
         var13 = var14;
         var14 = var15;
      }

      if(var14) {
         var2 = var4 - 1;
      } else {
         var2 = var4;
      }

      try {
         BigDecimal var18 = new BigDecimal(var8, 0, var5);
         var1.setAmount(var18);
         var1.setIndex(var2);
      } catch (NumberFormatException var11) {
         var1.setError();
      }

   }

   public void print(MoneyPrintContext var1, Appendable var2, BigMoney var3) throws IOException {
      byte var9 = 0;
      MoneyAmountStyle var13 = this.style.localize(var1.getLocale());
      BigMoney var14 = var3;
      if(var3.isNegative()) {
         var3 = var3.negated();
         var14 = var3;
         if(!var13.isAbsValue()) {
            var2.append(var13.getNegativeSignCharacter().charValue());
            var14 = var3;
         }
      }

      String var15 = var14.getAmount().toPlainString();
      char var6 = var13.getZeroCharacter().charValue();
      int var5;
      if(var6 != 48) {
         StringBuilder var16 = new StringBuilder(var15);

         for(var5 = 0; var5 < var15.length(); ++var5) {
            char var7 = var15.charAt(var5);
            if(var7 >= 48 && var7 <= 57) {
               var16.setCharAt(var5, (char)(var7 + (var6 - 48)));
            }
         }

         var15 = var16.toString();
      }

      int var11 = var15.indexOf(46);
      int var12 = var11 + 1;
      if(var13.getGroupingStyle() == GroupingStyle.NONE) {
         if(var11 < 0) {
            var2.append(var15);
            if(var13.isForcedDecimalPoint()) {
               var2.append(var13.getDecimalPointCharacter().charValue());
            }
         } else {
            var2.append(var15.subSequence(0, var11)).append(var13.getDecimalPointCharacter().charValue()).append(var15.substring(var12));
         }
      } else {
         int var10 = var13.getGroupingSize().intValue();
         int var17 = var13.getExtendedGroupingSize().intValue();
         var5 = var17;
         if(var17 == 0) {
            var5 = var10;
         }

         char var4 = var13.getGroupingCharacter().charValue();
         if(var11 < 0) {
            var17 = var15.length();
         } else {
            var17 = var11;
         }

         int var18;
         if(var11 < 0) {
            var18 = 0;
         } else {
            var18 = var15.length() - var11 - 1;
         }

         var2.append(var15.charAt(0));

         for(int var8 = 1; var8 < var17; ++var8) {
            if(this.isPreGroupingPoint(var17 - var8, var10, var5)) {
               var2.append(var4);
            }

            var2.append(var15.charAt(var8));
         }

         if(var11 >= 0 || var13.isForcedDecimalPoint()) {
            var2.append(var13.getDecimalPointCharacter().charValue());
         }

         var17 = var9;
         if(var13.getGroupingStyle() == GroupingStyle.BEFORE_DECIMAL_POINT) {
            if(var11 >= 0) {
               var2.append(var15.substring(var12));
            }
         } else {
            for(; var17 < var18; ++var17) {
               var2.append(var15.charAt(var17 + var12));
               if(this.isPostGroupingPoint(var17, var18, var10, var5)) {
                  var2.append(var4);
               }
            }
         }
      }

   }

   public String toString() {
      return "${amount}";
   }
}
