package org.joda.money.format;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import org.joda.money.BigMoney;
import org.joda.money.CurrencyUnit;
import org.joda.money.IllegalCurrencyException;

public final class MoneyFormatterBuilder {
   private final List parsers = new ArrayList();
   private final List printers = new ArrayList();

   private MoneyFormatterBuilder appendInternal(MoneyPrinter var1, MoneyParser var2) {
      this.printers.add(var1);
      this.parsers.add(var2);
      return this;
   }

   public MoneyFormatterBuilder append(MoneyFormatter var1) {
      MoneyFormatter.checkNotNull(var1, "MoneyFormatter must not be null");
      var1.getPrinterParser().appendTo(this);
      return this;
   }

   public MoneyFormatterBuilder append(MoneyPrinter var1, MoneyParser var2) {
      return this.appendInternal(var1, var2);
   }

   public MoneyFormatterBuilder appendAmount() {
      AmountPrinterParser var1 = new AmountPrinterParser(MoneyAmountStyle.ASCII_DECIMAL_POINT_GROUP3_COMMA);
      return this.appendInternal(var1, var1);
   }

   public MoneyFormatterBuilder appendAmount(MoneyAmountStyle var1) {
      MoneyFormatter.checkNotNull(var1, "MoneyAmountStyle must not be null");
      AmountPrinterParser var2 = new AmountPrinterParser(var1);
      return this.appendInternal(var2, var2);
   }

   public MoneyFormatterBuilder appendAmountLocalized() {
      AmountPrinterParser var1 = new AmountPrinterParser(MoneyAmountStyle.LOCALIZED_GROUPING);
      return this.appendInternal(var1, var1);
   }

   public MoneyFormatterBuilder appendCurrencyCode() {
      return this.appendInternal(MoneyFormatterBuilder.Singletons.CODE, MoneyFormatterBuilder.Singletons.CODE);
   }

   public MoneyFormatterBuilder appendCurrencyNumeric3Code() {
      return this.appendInternal(MoneyFormatterBuilder.Singletons.NUMERIC_3_CODE, MoneyFormatterBuilder.Singletons.NUMERIC_3_CODE);
   }

   public MoneyFormatterBuilder appendCurrencyNumericCode() {
      return this.appendInternal(MoneyFormatterBuilder.Singletons.NUMERIC_CODE, MoneyFormatterBuilder.Singletons.NUMERIC_CODE);
   }

   public MoneyFormatterBuilder appendCurrencySymbolLocalized() {
      return this.appendInternal(MoneyFormatterBuilder.SingletonPrinters.LOCALIZED_SYMBOL, (MoneyParser)null);
   }

   public MoneyFormatterBuilder appendLiteral(CharSequence var1) {
      MoneyFormatterBuilder var2 = this;
      if(var1 != null) {
         if(var1.length() == 0) {
            var2 = this;
         } else {
            LiteralPrinterParser var3 = new LiteralPrinterParser(var1.toString());
            var2 = this.appendInternal(var3, var3);
         }
      }

      return var2;
   }

   public MoneyFormatterBuilder appendSigned(MoneyFormatter var1, MoneyFormatter var2) {
      return this.appendSigned(var1, var1, var2);
   }

   public MoneyFormatterBuilder appendSigned(MoneyFormatter var1, MoneyFormatter var2, MoneyFormatter var3) {
      MoneyFormatter.checkNotNull(var1, "MoneyFormatter whenPositive must not be null");
      MoneyFormatter.checkNotNull(var2, "MoneyFormatter whenZero must not be null");
      MoneyFormatter.checkNotNull(var3, "MoneyFormatter whenNegative must not be null");
      SignedPrinterParser var4 = new SignedPrinterParser(var1, var2, var3);
      return this.appendInternal(var4, var4);
   }

   public MoneyFormatter toFormatter() {
      return this.toFormatter(Locale.getDefault());
   }

   public MoneyFormatter toFormatter(Locale var1) {
      MoneyFormatter.checkNotNull(var1, "Locale must not be null");
      return new MoneyFormatter(var1, (MoneyPrinter[])((MoneyPrinter[])this.printers.toArray(new MoneyPrinter[this.printers.size()])), (MoneyParser[])((MoneyParser[])this.parsers.toArray(new MoneyParser[this.parsers.size()])));
   }

   private static enum SingletonPrinters implements MoneyPrinter {
      LOCALIZED_SYMBOL;

      public void print(MoneyPrintContext var1, Appendable var2, BigMoney var3) throws IOException {
         var2.append(var3.getCurrencyUnit().getSymbol(var1.getLocale()));
      }

      public String toString() {
         return "${symbolLocalized}";
      }
   }

   private static enum Singletons implements MoneyParser, MoneyPrinter {
      CODE("${code}") {
         public void parse(MoneyParseContext var1) {
            int var2 = var1.getIndex() + 3;
            if(var2 > var1.getTextLength()) {
               var1.setError();
            } else {
               String var3 = var1.getTextSubstring(var1.getIndex(), var2);

               try {
                  var1.setCurrency(CurrencyUnit.of(var3));
                  var1.setIndex(var2);
               } catch (IllegalCurrencyException var4) {
                  var1.setError();
               }
            }

         }

         public void print(MoneyPrintContext var1, Appendable var2, BigMoney var3) throws IOException {
            var2.append(var3.getCurrencyUnit().getCode());
         }
      },
      NUMERIC_3_CODE("${numeric3Code}") {
         public void parse(MoneyParseContext var1) {
            int var2 = var1.getIndex() + 3;
            if(var2 > var1.getTextLength()) {
               var1.setError();
            } else {
               String var3 = var1.getTextSubstring(var1.getIndex(), var2);

               try {
                  var1.setCurrency(CurrencyUnit.ofNumericCode(var3));
                  var1.setIndex(var2);
               } catch (IllegalCurrencyException var4) {
                  var1.setError();
               }
            }

         }

         public void print(MoneyPrintContext var1, Appendable var2, BigMoney var3) throws IOException {
            var2.append(var3.getCurrencyUnit().getNumeric3Code());
         }
      },
      NUMERIC_CODE("${numericCode}") {
         public void parse(MoneyParseContext var1) {
            int var2;
            for(var2 = 0; var2 < 3 && var1.getIndex() + var2 < var1.getTextLength(); ++var2) {
               char var3 = var1.getText().charAt(var1.getIndex() + var2);
               if(var3 < 48 || var3 > 57) {
                  break;
               }
            }

            var2 += var1.getIndex();
            String var4 = var1.getTextSubstring(var1.getIndex(), var2);

            try {
               var1.setCurrency(CurrencyUnit.ofNumericCode(var4));
               var1.setIndex(var2);
            } catch (IllegalCurrencyException var5) {
               var1.setError();
            }

         }

         public void print(MoneyPrintContext var1, Appendable var2, BigMoney var3) throws IOException {
            var2.append(Integer.toString(var3.getCurrencyUnit().getNumericCode()));
         }
      };

      private final String toString;

      private Singletons(String var3) {
         this.toString = var3;
      }

      // $FF: synthetic method
      Singletons(String var3, Object var4) {
         this(var3);
      }

      public String toString() {
         return this.toString;
      }
   }
}
