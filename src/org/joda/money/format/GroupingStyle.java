package org.joda.money.format;

public enum GroupingStyle {
   BEFORE_DECIMAL_POINT,
   FULL,
   NONE;
}
