package org.joda.money.format;

import java.io.IOException;

public class MoneyFormatException extends RuntimeException {
   private static final long serialVersionUID = 87533576L;

   public MoneyFormatException(String var1) {
      super(var1);
   }

   public MoneyFormatException(String var1, Throwable var2) {
      super(var1, var2);
   }

   public void rethrowIOException() throws IOException {
      if(this.getCause() instanceof IOException) {
         throw (IOException)this.getCause();
      }
   }
}
