package org.joda.money.format;

public interface MoneyParser {
   void parse(MoneyParseContext var1);
}
