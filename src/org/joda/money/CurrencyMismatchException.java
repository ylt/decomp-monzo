package org.joda.money;

public class CurrencyMismatchException extends IllegalArgumentException {
   private static final long serialVersionUID = 1L;
   private final CurrencyUnit firstCurrency;
   private final CurrencyUnit secondCurrency;

   public CurrencyMismatchException(CurrencyUnit var1, CurrencyUnit var2) {
      StringBuilder var4 = (new StringBuilder()).append("Currencies differ: ");
      String var3;
      if(var1 != null) {
         var3 = var1.getCode();
      } else {
         var3 = "null";
      }

      var4 = var4.append(var3).append('/');
      if(var2 != null) {
         var3 = var2.getCode();
      } else {
         var3 = "null";
      }

      super(var4.append(var3).toString());
      this.firstCurrency = var1;
      this.secondCurrency = var2;
   }

   public CurrencyUnit getFirstCurrency() {
      return this.firstCurrency;
   }

   public CurrencyUnit getSecondCurrency() {
      return this.secondCurrency;
   }
}
