package org.apache.a.a;

import java.io.Closeable;
import java.io.Flushable;
import java.io.IOException;
import java.util.Iterator;

public final class c implements Closeable, Flushable {
   private final Appendable a;
   private final b b;
   private boolean c = true;

   public c(Appendable var1, b var2) throws IOException {
      a.a(var1, "out");
      a.a(var2, "format");
      this.a = var1;
      this.b = var2;
      if(var2.e() != null) {
         String[] var5 = var2.e();
         int var4 = var5.length;

         for(int var3 = 0; var3 < var4; ++var3) {
            String var6 = var5[var3];
            if(var6 != null) {
               this.a(var6);
            }
         }
      }

      if(var2.d() != null && !var2.k()) {
         this.a((Object[])var2.d());
      }

   }

   public void a() throws IOException {
      this.b.a(this.a);
      this.c = true;
   }

   public void a(Iterable var1) throws IOException {
      Iterator var2 = var1.iterator();

      while(var2.hasNext()) {
         this.a(var2.next());
      }

      this.a();
   }

   public void a(Object var1) throws IOException {
      this.b.a(var1, this.a, this.c);
      this.c = false;
   }

   public void a(String var1) throws IOException {
      if(this.b.n()) {
         if(!this.c) {
            this.a();
         }

         this.a.append(this.b.a().charValue());
         this.a.append(' ');

         for(int var3 = 0; var3 < var1.length(); ++var3) {
            char var2 = var1.charAt(var3);
            int var4 = var3;
            switch(var2) {
            case '\u000b':
            case '\f':
            default:
               this.a.append(var2);
               break;
            case '\r':
               var4 = var3;
               if(var3 + 1 < var1.length()) {
                  var4 = var3;
                  if(var1.charAt(var3 + 1) == 10) {
                     var4 = var3 + 1;
                  }
               }
            case '\n':
               this.a();
               this.a.append(this.b.a().charValue());
               this.a.append(' ');
               var3 = var4;
            }
         }

         this.a();
      }

   }

   public void a(Object... var1) throws IOException {
      this.b.a(this.a, var1);
      this.c = true;
   }

   public void b(Iterable var1) throws IOException {
      Iterator var2 = var1.iterator();

      while(var2.hasNext()) {
         Object var3 = var2.next();
         if(var3 instanceof Object[]) {
            this.a((Object[])((Object[])var3));
         } else if(var3 instanceof Iterable) {
            this.a((Iterable)var3);
         } else {
            this.a(new Object[]{var3});
         }
      }

   }

   public void close() throws IOException {
      if(this.a instanceof Closeable) {
         ((Closeable)this.a).close();
      }

   }

   public void flush() throws IOException {
      if(this.a instanceof Flushable) {
         ((Flushable)this.a).flush();
      }

   }
}
