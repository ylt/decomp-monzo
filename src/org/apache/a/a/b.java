package org.apache.a.a;

import java.io.IOException;
import java.io.Serializable;
import java.util.Arrays;
import java.util.HashSet;

public final class b implements Serializable {
   public static final b a;
   public static final b b;
   public static final b c;
   public static final b d;
   public static final b e;
   public static final b f;
   public static final b g;
   public static final b h;
   public static final b i;
   private final boolean j;
   private final Character k;
   private final char l;
   private final Character m;
   private final String[] n;
   private final String[] o;
   private final boolean p;
   private final boolean q;
   private final boolean r;
   private final String s;
   private final Character t;
   private final e u;
   private final String v;
   private final boolean w;
   private final boolean x;
   private final boolean y;

   static {
      a = new b(',', d.a, (e)null, (Character)null, (Character)null, false, true, "\r\n", (String)null, (Object[])null, (String[])null, false, false, false, false, false);
      b = a.b(false).r();
      c = a.a('|').b('\\').b(d.a).c('\n');
      d = a.a(',').b(d.a).c('\n');
      e = a.a('\t').b('\\').b(false).b((Character)null).c('\n').a("\\N").a(e.b);
      f = a.a(',').a(d.a).b(false).b(d.a).c('\n').a("").a(e.b);
      g = a.a('\t').a(d.a).b(false).b(d.a).c('\n').a("\\N").a(e.b);
      h = a.b(false);
      i = a.a('\t').s();
   }

   private b(char var1, Character var2, e var3, Character var4, Character var5, boolean var6, boolean var7, String var8, String var9, Object[] var10, String[] var11, boolean var12, boolean var13, boolean var14, boolean var15, boolean var16) {
      this.l = var1;
      this.t = var2;
      this.u = var3;
      this.k = var4;
      this.m = var5;
      this.r = var6;
      this.j = var13;
      this.p = var7;
      this.v = var8;
      this.s = var9;
      this.o = this.a(var10);
      String[] var17;
      if(var11 == null) {
         var17 = null;
      } else {
         var17 = (String[])var11.clone();
      }

      this.n = var17;
      this.w = var12;
      this.q = var14;
      this.x = var16;
      this.y = var15;
      this.t();
   }

   private CharSequence a(CharSequence var1) {
      Object var5;
      if(var1 instanceof String) {
         var5 = ((String)var1).trim();
      } else {
         int var4 = var1.length();

         int var2;
         for(var2 = 0; var2 < var4 && var1.charAt(var2) <= 32; ++var2) {
            ;
         }

         int var3;
         for(var3 = var4; var2 < var3 && var1.charAt(var3 - 1) <= 32; --var3) {
            ;
         }

         if(var2 <= 0) {
            var5 = var1;
            if(var3 >= var4) {
               return (CharSequence)var5;
            }
         }

         var5 = var1.subSequence(var2, var3);
      }

      return (CharSequence)var5;
   }

   private void a(CharSequence var1, int var2, int var3, Appendable var4) throws IOException {
      char var10 = this.b();
      char var7 = this.c().charValue();
      int var8 = var2;

      int var9;
      for(var9 = var2; var9 < var2 + var3; ++var9) {
         char var6 = var1.charAt(var9);
         if(var6 == 13 || var6 == 10 || var6 == var10 || var6 == var7) {
            if(var9 > var8) {
               var4.append(var1, var8, var9);
            }

            char var5;
            if(var6 == 10) {
               var5 = 110;
            } else {
               var5 = var6;
               if(var6 == 13) {
                  var5 = 114;
               }
            }

            var4.append(var7);
            var4.append(var5);
            var8 = var9 + 1;
         }
      }

      if(var9 > var8) {
         var4.append(var1, var8, var9);
      }

   }

   private void a(Object var1, CharSequence var2, int var3, int var4, Appendable var5, boolean var6) throws IOException {
      if(!var6) {
         var5.append(this.b());
      }

      if(var1 == null) {
         var5.append(var2);
      } else if(this.q()) {
         this.b(var1, var2, var3, var4, var5, var6);
      } else if(this.o()) {
         this.a(var2, var3, var4, var5);
      } else {
         var5.append(var2, var3, var3 + var4);
      }

   }

   private String[] a(Object[] var1) {
      Object var3 = null;
      String[] var5;
      if(var1 == null) {
         var5 = (String[])var3;
      } else {
         String[] var4 = new String[var1.length];

         for(int var2 = 0; var2 < var1.length; ++var2) {
            var3 = var1[var2];
            String var6;
            if(var3 == null) {
               var6 = null;
            } else {
               var6 = var3.toString();
            }

            var4[var2] = var6;
         }

         var5 = var4;
      }

      return var5;
   }

   private void b(Object var1, CharSequence var2, int var3, int var4, Appendable var5, boolean var6) throws IOException {
      int var11 = var3 + var4;
      char var9 = this.b();
      char var7 = this.i().charValue();
      e var13 = this.j();
      e var12 = var13;
      if(var13 == null) {
         var12 = e.c;
      }

      int var8;
      boolean var16;
      switch(null.a[var12.ordinal()]) {
      case 1:
      case 2:
         var16 = true;
         var8 = var3;
         break;
      case 3:
         if(!(var1 instanceof Number)) {
            var16 = true;
         } else {
            var16 = false;
         }

         var8 = var3;
         break;
      case 4:
         this.a(var2, var3, var4, var5);
         return;
      case 5:
         boolean var15;
         if(var4 <= 0) {
            if(var6) {
               var15 = true;
               var8 = var3;
            } else {
               var8 = var3;
               var15 = false;
            }
         } else {
            char var14 = var2.charAt(var3);
            if(var6 && (var14 < 32 || var14 > 33 && var14 < 35 || var14 > 43 && var14 < 45 || var14 > 126)) {
               var15 = true;
               var8 = var3;
            } else if(var14 <= 35) {
               var15 = true;
               var8 = var3;
            } else {
               var8 = var3;

               while(true) {
                  if(var8 < var11) {
                     var14 = var2.charAt(var8);
                     if(var14 != 10 && var14 != 13 && var14 != var7 && var14 != var9) {
                        ++var8;
                        continue;
                     }

                     var16 = true;
                     break;
                  }

                  var16 = false;
                  break;
               }

               var15 = var16;
               if(!var16) {
                  int var10 = var11 - 1;
                  var15 = var16;
                  var8 = var10;
                  if(var2.charAt(var10) <= 32) {
                     var15 = true;
                     var8 = var10;
                  }
               }
            }
         }

         var16 = var15;
         if(!var15) {
            var5.append(var2, var3, var11);
            return;
         }
         break;
      default:
         throw new IllegalStateException("Unexpected Quote value: " + var12);
      }

      if(!var16) {
         var5.append(var2, var3, var11);
      } else {
         var5.append(var7);

         for(var4 = var3; var8 < var11; var4 = var3) {
            var3 = var4;
            if(var2.charAt(var8) == var7) {
               var5.append(var2, var4, var8 + 1);
               var3 = var8;
            }

            ++var8;
         }

         var5.append(var2, var4, var8);
         var5.append(var7);
      }

   }

   private static boolean c(Character var0) {
      boolean var1;
      if(var0 != null && d(var0.charValue())) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   private static boolean d(char var0) {
      boolean var1;
      if(var0 != 10 && var0 != 13) {
         var1 = false;
      } else {
         var1 = true;
      }

      return var1;
   }

   private void t() throws IllegalArgumentException {
      if(d(this.l)) {
         throw new IllegalArgumentException("The delimiter cannot be a line break");
      } else if(this.t != null && this.l == this.t.charValue()) {
         throw new IllegalArgumentException("The quoteChar character and the delimiter cannot be the same ('" + this.t + "')");
      } else if(this.m != null && this.l == this.m.charValue()) {
         throw new IllegalArgumentException("The escape character and the delimiter cannot be the same ('" + this.m + "')");
      } else if(this.k != null && this.l == this.k.charValue()) {
         throw new IllegalArgumentException("The comment start character and the delimiter cannot be the same ('" + this.k + "')");
      } else if(this.t != null && this.t.equals(this.k)) {
         throw new IllegalArgumentException("The comment start character and the quoteChar cannot be the same ('" + this.k + "')");
      } else if(this.m != null && this.m.equals(this.k)) {
         throw new IllegalArgumentException("The comment start and the escape character cannot be the same ('" + this.k + "')");
      } else if(this.m == null && this.u == e.e) {
         throw new IllegalArgumentException("No quotes mode set but no escape character is set");
      } else {
         if(this.n != null) {
            HashSet var3 = new HashSet();
            String[] var5 = this.n;
            int var2 = var5.length;

            for(int var1 = 0; var1 < var2; ++var1) {
               String var4 = var5[var1];
               if(!var3.add(var4)) {
                  throw new IllegalArgumentException("The header contains a duplicate entry: '" + var4 + "' in " + Arrays.toString(this.n));
               }
            }
         }

      }
   }

   public Character a() {
      return this.k;
   }

   public b a(char var1) {
      if(d(var1)) {
         throw new IllegalArgumentException("The delimiter cannot be a line break");
      } else {
         return new b(var1, this.t, this.u, this.k, this.m, this.r, this.p, this.v, this.s, this.o, this.n, this.w, this.j, this.q, this.y, this.x);
      }
   }

   public b a(Character var1) {
      if(c(var1)) {
         throw new IllegalArgumentException("The escape character cannot be a line break");
      } else {
         return new b(this.l, this.t, this.u, this.k, var1, this.r, this.p, this.v, this.s, this.o, this.n, this.w, this.j, this.q, this.y, this.x);
      }
   }

   public b a(String var1) {
      return new b(this.l, this.t, this.u, this.k, this.m, this.r, this.p, this.v, var1, this.o, this.n, this.w, this.j, this.q, this.y, this.x);
   }

   public b a(e var1) {
      return new b(this.l, this.t, var1, this.k, this.m, this.r, this.p, this.v, this.s, this.o, this.n, this.w, this.j, this.q, this.y, this.x);
   }

   public b a(boolean var1) {
      return new b(this.l, this.t, this.u, this.k, this.m, this.r, this.p, this.v, this.s, this.o, this.n, this.w, var1, this.q, this.y, this.x);
   }

   public b a(String... var1) {
      return new b(this.l, this.t, this.u, this.k, this.m, this.r, this.p, this.v, this.s, this.o, var1, this.w, this.j, this.q, this.y, this.x);
   }

   public void a(Appendable var1) throws IOException {
      if(this.l()) {
         var1.append(this.b());
      }

      if(this.v != null) {
         var1.append(this.v);
      }

   }

   public void a(Appendable var1, Object... var2) throws IOException {
      for(int var3 = 0; var3 < var2.length; ++var3) {
         Object var5 = var2[var3];
         boolean var4;
         if(var3 == 0) {
            var4 = true;
         } else {
            var4 = false;
         }

         this.a(var5, var1, var4);
      }

      this.a(var1);
   }

   public void a(Object var1, Appendable var2, boolean var3) throws IOException {
      Object var4;
      if(var1 == null) {
         if(this.s == null) {
            var4 = "";
         } else if(e.a == this.u) {
            var4 = this.t + this.s + this.t;
         } else {
            var4 = this.s;
         }
      } else if(var1 instanceof CharSequence) {
         var4 = (CharSequence)var1;
      } else {
         var4 = var1.toString();
      }

      if(this.m()) {
         var4 = this.a((CharSequence)var4);
      }

      this.a(var1, (CharSequence)var4, 0, ((CharSequence)var4).length(), var2, var3);
   }

   public char b() {
      return this.l;
   }

   public b b(char var1) {
      return this.a(Character.valueOf(var1));
   }

   public b b(Character var1) {
      if(c(var1)) {
         throw new IllegalArgumentException("The quoteChar cannot be a line break");
      } else {
         return new b(this.l, var1, this.u, this.k, this.m, this.r, this.p, this.v, this.s, this.o, this.n, this.w, this.j, this.q, this.y, this.x);
      }
   }

   public b b(String var1) {
      return new b(this.l, this.t, this.u, this.k, this.m, this.r, this.p, var1, this.s, this.o, this.n, this.w, this.j, this.q, this.y, this.x);
   }

   public b b(boolean var1) {
      return new b(this.l, this.t, this.u, this.k, this.m, this.r, var1, this.v, this.s, this.o, this.n, this.w, this.j, this.q, this.y, this.x);
   }

   public Character c() {
      return this.m;
   }

   public b c(char var1) {
      return this.b(String.valueOf(var1));
   }

   public b c(boolean var1) {
      return new b(this.l, this.t, this.u, this.k, this.m, var1, this.p, this.v, this.s, this.o, this.n, this.w, this.j, this.q, this.y, this.x);
   }

   public String[] d() {
      String[] var1;
      if(this.n != null) {
         var1 = (String[])this.n.clone();
      } else {
         var1 = null;
      }

      return var1;
   }

   public String[] e() {
      String[] var1;
      if(this.o != null) {
         var1 = (String[])this.o.clone();
      } else {
         var1 = null;
      }

      return var1;
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(this != var1) {
         if(var1 == null) {
            var2 = false;
         } else if(this.getClass() != var1.getClass()) {
            var2 = false;
         } else {
            b var3 = (b)var1;
            if(this.l != var3.l) {
               var2 = false;
            } else if(this.u != var3.u) {
               var2 = false;
            } else {
               if(this.t == null) {
                  if(var3.t != null) {
                     var2 = false;
                     return var2;
                  }
               } else if(!this.t.equals(var3.t)) {
                  var2 = false;
                  return var2;
               }

               if(this.k == null) {
                  if(var3.k != null) {
                     var2 = false;
                     return var2;
                  }
               } else if(!this.k.equals(var3.k)) {
                  var2 = false;
                  return var2;
               }

               if(this.m == null) {
                  if(var3.m != null) {
                     var2 = false;
                     return var2;
                  }
               } else if(!this.m.equals(var3.m)) {
                  var2 = false;
                  return var2;
               }

               if(this.s == null) {
                  if(var3.s != null) {
                     var2 = false;
                     return var2;
                  }
               } else if(!this.s.equals(var3.s)) {
                  var2 = false;
                  return var2;
               }

               if(!Arrays.equals(this.n, var3.n)) {
                  var2 = false;
               } else if(this.r != var3.r) {
                  var2 = false;
               } else if(this.p != var3.p) {
                  var2 = false;
               } else if(this.w != var3.w) {
                  var2 = false;
               } else if(this.v == null) {
                  if(var3.v != null) {
                     var2 = false;
                  }
               } else if(!this.v.equals(var3.v)) {
                  var2 = false;
               }
            }
         }
      }

      return var2;
   }

   public boolean f() {
      return this.p;
   }

   public boolean g() {
      return this.q;
   }

   public boolean h() {
      return this.r;
   }

   public int hashCode() {
      short var9 = 1231;
      int var10 = 0;
      char var11 = this.l;
      int var1;
      if(this.u == null) {
         var1 = 0;
      } else {
         var1 = this.u.hashCode();
      }

      int var2;
      if(this.t == null) {
         var2 = 0;
      } else {
         var2 = this.t.hashCode();
      }

      int var3;
      if(this.k == null) {
         var3 = 0;
      } else {
         var3 = this.k.hashCode();
      }

      int var4;
      if(this.m == null) {
         var4 = 0;
      } else {
         var4 = this.m.hashCode();
      }

      int var5;
      if(this.s == null) {
         var5 = 0;
      } else {
         var5 = this.s.hashCode();
      }

      short var6;
      if(this.r) {
         var6 = 1231;
      } else {
         var6 = 1237;
      }

      short var7;
      if(this.q) {
         var7 = 1231;
      } else {
         var7 = 1237;
      }

      short var8;
      if(this.p) {
         var8 = 1231;
      } else {
         var8 = 1237;
      }

      if(!this.w) {
         var9 = 1237;
      }

      if(this.v != null) {
         var10 = this.v.hashCode();
      }

      return (((var8 + (var7 + (var6 + (var5 + (var4 + (var3 + (var2 + (var1 + (var11 + 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31 + var9) * 31 + var10) * 31 + Arrays.hashCode(this.n);
   }

   public Character i() {
      return this.t;
   }

   public e j() {
      return this.u;
   }

   public boolean k() {
      return this.w;
   }

   public boolean l() {
      return this.x;
   }

   public boolean m() {
      return this.y;
   }

   public boolean n() {
      boolean var1;
      if(this.k != null) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public boolean o() {
      boolean var1;
      if(this.m != null) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public boolean p() {
      boolean var1;
      if(this.s != null) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public boolean q() {
      boolean var1;
      if(this.t != null) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public b r() {
      return this.a(true);
   }

   public b s() {
      return this.c(true);
   }

   public String toString() {
      StringBuilder var1 = new StringBuilder();
      var1.append("Delimiter=<").append(this.l).append('>');
      if(this.o()) {
         var1.append(' ');
         var1.append("Escape=<").append(this.m).append('>');
      }

      if(this.q()) {
         var1.append(' ');
         var1.append("QuoteChar=<").append(this.t).append('>');
      }

      if(this.n()) {
         var1.append(' ');
         var1.append("CommentStart=<").append(this.k).append('>');
      }

      if(this.p()) {
         var1.append(' ');
         var1.append("NullString=<").append(this.s).append('>');
      }

      if(this.v != null) {
         var1.append(' ');
         var1.append("RecordSeparator=<").append(this.v).append('>');
      }

      if(this.f()) {
         var1.append(" EmptyLines:ignored");
      }

      if(this.h()) {
         var1.append(" SurroundingSpaces:ignored");
      }

      if(this.g()) {
         var1.append(" IgnoreHeaderCase:ignored");
      }

      var1.append(" SkipHeaderRecord:").append(this.w);
      if(this.o != null) {
         var1.append(' ');
         var1.append("HeaderComments:").append(Arrays.toString(this.o));
      }

      if(this.n != null) {
         var1.append(' ');
         var1.append("Header:").append(Arrays.toString(this.n));
      }

      return var1.toString();
   }
}
