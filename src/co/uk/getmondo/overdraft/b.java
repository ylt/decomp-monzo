package co.uk.getmondo.overdraft;

import co.uk.getmondo.common.ui.f;
import co.uk.getmondo.d.w;
import io.reactivex.n;
import io.reactivex.u;
import io.reactivex.c.g;
import io.reactivex.c.h;
import io.reactivex.c.q;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0019B3\b\u0007\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b¢\u0006\u0002\u0010\fJ\u0010\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0011H\u0002J\u0010\u0010\u0017\u001a\u00020\u00152\u0006\u0010\u0018\u001a\u00020\u0002H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082D¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u000eX\u0082D¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082.¢\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082.¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u001a"},
   d2 = {"Lco/uk/getmondo/overdraft/ChangeOverdraftLimitPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/overdraft/ChangeOverdraftLimitPresenter$View;", "ioScheduler", "Lio/reactivex/Scheduler;", "uiScheduler", "accountManager", "Lco/uk/getmondo/common/accounts/AccountManager;", "overdraftManager", "Lco/uk/getmondo/overdraft/data/OverdraftManager;", "apiErrorHandler", "Lco/uk/getmondo/common/errors/ApiErrorHandler;", "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/common/accounts/AccountManager;Lco/uk/getmondo/overdraft/data/OverdraftManager;Lco/uk/getmondo/common/errors/ApiErrorHandler;)V", "limitIncrementAmount", "", "maxDaysInMonth", "selectedLimit", "Lco/uk/getmondo/model/Amount;", "status", "Lco/uk/getmondo/model/OverdraftStatus;", "handleNewLimit", "", "newLimit", "register", "view", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class b extends co.uk.getmondo.common.ui.b {
   private final long c;
   private final long d;
   private w e;
   private co.uk.getmondo.d.c f;
   private final u g;
   private final u h;
   private final co.uk.getmondo.common.accounts.b i;
   private final co.uk.getmondo.overdraft.a.c j;
   private final co.uk.getmondo.common.e.a k;

   public b(u var1, u var2, co.uk.getmondo.common.accounts.b var3, co.uk.getmondo.overdraft.a.c var4, co.uk.getmondo.common.e.a var5) {
      l.b(var1, "ioScheduler");
      l.b(var2, "uiScheduler");
      l.b(var3, "accountManager");
      l.b(var4, "overdraftManager");
      l.b(var5, "apiErrorHandler");
      super();
      this.g = var1;
      this.h = var2;
      this.i = var3;
      this.j = var4;
      this.k = var5;
      this.c = 10000L;
      this.d = 31L;
   }

   private final void a(co.uk.getmondo.d.c var1) {
      boolean var3 = true;
      this.f = var1;
      ((b.a)this.a).a(var1);
      b.a var6 = (b.a)this.a;
      boolean var2;
      if(var1.k() > 0L) {
         var2 = true;
      } else {
         var2 = false;
      }

      var6.b(var2);
      var6 = (b.a)this.a;
      long var4 = var1.k();
      w var7 = this.e;
      if(var7 == null) {
         l.b("status");
      }

      if(var4 < var7.h().k()) {
         var2 = var3;
      } else {
         var2 = false;
      }

      var6.c(var2);
      var6 = (b.a)this.a;
      var7 = this.e;
      if(var7 == null) {
         l.b("status");
      }

      var6.a(l.a(var1, var7.g()) ^ true);
   }

   // $FF: synthetic method
   public static final w b(b var0) {
      w var1 = var0.e;
      if(var1 == null) {
         l.b("status");
      }

      return var1;
   }

   // $FF: synthetic method
   public static final co.uk.getmondo.d.c d(b var0) {
      co.uk.getmondo.d.c var1 = var0.f;
      if(var1 == null) {
         l.b("selectedLimit");
      }

      return var1;
   }

   public void a(final b.a var1) {
      l.b(var1, "view");
      super.a((f)var1);
      io.reactivex.b.a var3 = this.b;
      io.reactivex.b.b var2 = this.i.e().flatMap((h)(new h() {
         public final n a(String var1) {
            l.b(var1, "accountId");
            return b.this.j.b(var1);
         }
      })).subscribe((g)(new g() {
         public final void a(w var1x) {
            b var2 = b.this;
            l.a(var1x, "status");
            var2.e = var1x;
            var1.a(var1x.f().k() + "p", var1x.e().toString(), var1x.f().c(b.this.d).toString());
            b.this.a(var1x.g());
         }
      }));
      l.a(var2, "accountManager.accountId….limit)\n                }");
      this.b = co.uk.getmondo.common.j.f.a(var3, var2);
      var3 = this.b;
      var2 = var1.a().filter((q)(new q() {
         public final boolean a(kotlin.n var1) {
            l.b(var1, "it");
            boolean var2;
            if(b.d(b.this).k() + b.this.c <= b.b(b.this).h().k()) {
               var2 = true;
            } else {
               var2 = false;
            }

            return var2;
         }
      })).subscribe((g)(new g() {
         public final void a(kotlin.n var1) {
            b.this.a(b.d(b.this).a(b.this.c));
         }
      }));
      l.a(var2, "view.plusButtonClicks\n  …+ limitIncrementAmount) }");
      this.b = co.uk.getmondo.common.j.f.a(var3, var2);
      io.reactivex.b.a var5 = this.b;
      io.reactivex.b.b var6 = var1.b().filter((q)(new q() {
         public final boolean a(kotlin.n var1) {
            l.b(var1, "it");
            boolean var2;
            if(b.d(b.this).k() - b.this.c >= 0L) {
               var2 = true;
            } else {
               var2 = false;
            }

            return var2;
         }
      })).subscribe((g)(new g() {
         public final void a(kotlin.n var1) {
            b.this.a(b.d(b.this).b(b.this.c));
         }
      }));
      l.a(var6, "view.minusButtonClicks\n …- limitIncrementAmount) }");
      this.b = co.uk.getmondo.common.j.f.a(var5, var6);
      var3 = this.b;
      var2 = var1.c().subscribe((g)(new g() {
         public final void a(kotlin.n var1x) {
            var1.b(b.d(b.this));
         }
      }));
      l.a(var2, "view.buttonConfirmationC…onPrompt(selectedLimit) }");
      this.b = co.uk.getmondo.common.j.f.a(var3, var2);
      var5 = this.b;
      io.reactivex.b.b var4 = var1.e().flatMapMaybe((h)(new h() {
         public final io.reactivex.h a(kotlin.n var1x) {
            l.b(var1x, "it");
            return co.uk.getmondo.common.j.f.a((io.reactivex.b)b.this.i.e().flatMapCompletable((h)(new h() {
               public final io.reactivex.b a(String var1x) {
                  l.b(var1x, "accountId");
                  return b.this.j.a(var1x, b.d(b.this));
               }
            })).b(b.this.g).a(b.this.h).c((g)(new g() {
               public final void a(io.reactivex.b.b var1x) {
                  var1.f();
               }
            })).b((g)(new g() {
               public final void a(Throwable var1x) {
                  var1.g();
               }
            })).a((g)(new g() {
               public final void a(Throwable var1x) {
                  co.uk.getmondo.common.e.a var2 = b.this.k;
                  l.a(var1x, "it");
                  var2.a(var1x, (co.uk.getmondo.common.e.a.a)var1);
               }
            })), (Object)kotlin.n.a);
         }
      })).subscribe((g)(new g() {
         public final void a(kotlin.n var1x) {
            var1.c(b.d(b.this));
         }
      }));
      l.a(var4, "view.dialogConfirmationC…onScreen(selectedLimit) }");
      this.b = co.uk.getmondo.common.j.f.a(var5, var4);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0006\bf\u0018\u00002\u00020\u00012\u00020\u0002J\b\u0010\u000e\u001a\u00020\u0005H&J\u0010\u0010\u000f\u001a\u00020\u00052\u0006\u0010\u0010\u001a\u00020\u0011H&J\u0010\u0010\u0012\u001a\u00020\u00052\u0006\u0010\u0013\u001a\u00020\u0014H&J\u0010\u0010\u0015\u001a\u00020\u00052\u0006\u0010\u0013\u001a\u00020\u0014H&J\u0010\u0010\u0016\u001a\u00020\u00052\u0006\u0010\u0013\u001a\u00020\u0014H&J \u0010\u0017\u001a\u00020\u00052\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u00192\u0006\u0010\u001b\u001a\u00020\u0019H&J\b\u0010\u001c\u001a\u00020\u0005H&J\u0010\u0010\u001d\u001a\u00020\u00052\u0006\u0010\u0010\u001a\u00020\u0011H&J\u0010\u0010\u001e\u001a\u00020\u00052\u0006\u0010\u0010\u001a\u00020\u0011H&R\u0018\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0006\u0010\u0007R\u0018\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\t\u0010\u0007R\u0018\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\u0007R\u0018\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\r\u0010\u0007¨\u0006\u001f"},
      d2 = {"Lco/uk/getmondo/overdraft/ChangeOverdraftLimitPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "buttonConfirmationClicks", "Lio/reactivex/Observable;", "", "getButtonConfirmationClicks", "()Lio/reactivex/Observable;", "dialogConfirmationClicks", "getDialogConfirmationClicks", "minusButtonClicks", "getMinusButtonClicks", "plusButtonClicks", "getPlusButtonClicks", "hideLoading", "openOverdraftConfirmationScreen", "newLimit", "Lco/uk/getmondo/model/Amount;", "setConfirmationButtonEnabled", "enabled", "", "setMinusButtonEnabled", "setPlusButtonEnabled", "showDescription", "dailyFee", "", "buffer", "maxCharge", "showLoading", "showNewLimit", "showOverdraftConfirmationPrompt", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, f {
      n a();

      void a(co.uk.getmondo.d.c var1);

      void a(String var1, String var2, String var3);

      void a(boolean var1);

      n b();

      void b(co.uk.getmondo.d.c var1);

      void b(boolean var1);

      n c();

      void c(co.uk.getmondo.d.c var1);

      void c(boolean var1);

      n e();

      void f();

      void g();
   }
}
