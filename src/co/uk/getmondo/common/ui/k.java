package co.uk.getmondo.common.ui;

import android.graphics.Paint;
import android.graphics.Typeface;
import android.text.TextPaint;
import android.text.style.MetricAffectingSpan;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0002J\u0010\u0010\t\u001a\u00020\u00062\u0006\u0010\n\u001a\u00020\u000bH\u0016J\u0010\u0010\f\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u000bH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\r"},
   d2 = {"Lco/uk/getmondo/common/ui/TypefaceSpan;", "Landroid/text/style/MetricAffectingSpan;", "typeface", "Landroid/graphics/Typeface;", "(Landroid/graphics/Typeface;)V", "apply", "", "paint", "Landroid/graphics/Paint;", "updateDrawState", "drawState", "Landroid/text/TextPaint;", "updateMeasureState", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class k extends MetricAffectingSpan {
   private final Typeface a;

   public k(Typeface var1) {
      kotlin.d.b.l.b(var1, "typeface");
      super();
      this.a = var1;
   }

   private final void a(Paint var1) {
      Typeface var3 = var1.getTypeface();
      int var2;
      if(var3 != null) {
         var2 = var3.getStyle();
      } else {
         var2 = 0;
      }

      var2 &= ~this.a.getStyle();
      if((var2 & 1) != 0) {
         var1.setFakeBoldText(true);
      }

      if((var2 & 2) != 0) {
         var1.setTextSkewX(-0.25F);
      }

      var1.setTypeface(this.a);
   }

   public void updateDrawState(TextPaint var1) {
      kotlin.d.b.l.b(var1, "drawState");
      this.a((Paint)var1);
   }

   public void updateMeasureState(TextPaint var1) {
      kotlin.d.b.l.b(var1, "paint");
      this.a((Paint)var1);
   }
}
