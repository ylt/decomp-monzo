package co.uk.getmondo.common.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.t;
import android.view.View;

public class h extends android.support.v7.widget.RecyclerView.g {
   private final int a;
   private final Drawable b;
   private final a.a.a.a.a.a c;
   private final int d;
   private final int e;

   public h(Context var1, a.a.a.a.a.a var2, int var3) {
      this.a = var3;
      this.b = android.support.v4.content.a.a(var1, 2130837967);
      this.d = var1.getResources().getDimensionPixelSize(2131427605);
      this.e = var1.getResources().getDimensionPixelSize(2131427603);
      this.c = var2;
   }

   public void getItemOffsets(Rect var1, View var2, RecyclerView var3, t var4) {
      int var5 = var3.f(var2);
      if(!var3.s() && var5 != -1) {
         if(var5 == 0) {
            var5 = this.d;
         } else if(this.c.a(var5) != this.c.a(var5 - 1)) {
            var5 = this.e;
         } else {
            var5 = 0;
         }

         var1.set(0, var5, 0, 0);
      }

   }

   public void onDrawOver(Canvas var1, RecyclerView var2, t var3) {
      if(!var2.s()) {
         int var8 = var2.getPaddingLeft();
         int var5 = this.a;
         int var6 = var2.getWidth();
         int var9 = var2.getPaddingRight();
         int var7 = var2.getChildCount();

         for(int var4 = 0; var4 < var7 - 1; ++var4) {
            View var13 = var2.getChildAt(var4);
            int var10 = var2.f(var13);
            if(var10 != -1 && this.c.a(var10) == this.c.a(var10 + 1)) {
               android.support.v7.widget.RecyclerView.i var12 = (android.support.v7.widget.RecyclerView.i)var13.getLayoutParams();
               var10 = var13.getBottom();
               int var11 = var12.bottomMargin + var10;
               var10 = this.b.getIntrinsicHeight();
               this.b.setBounds(var8 + var5, var11, var6 - var9, var10 + var11);
               this.b.draw(var1);
            }
         }
      }

   }
}
