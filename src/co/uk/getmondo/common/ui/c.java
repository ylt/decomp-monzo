package co.uk.getmondo.common.ui;

import android.graphics.Bitmap;
import android.widget.ImageView;

public class c extends com.bumptech.glide.g.b.b {
   public c(ImageView var1) {
      super(var1);
   }

   protected void a(Bitmap var1) {
      android.support.v4.a.a.f var2 = android.support.v4.a.a.h.a(((ImageView)this.c).getResources(), var1);
      var2.a(true);
      ((ImageView)this.c).setImageDrawable(var2);
   }
}
