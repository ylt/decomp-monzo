package co.uk.getmondo.common.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.aa;
import android.text.SpannableString;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.util.AttributeSet;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\n\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\t\u0018\u00002\u00020\u0001B%\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u0010\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001bH\u0002J\u0010\u0010\u001c\u001a\u00020\u00192\u0006\u0010\u001d\u001a\u00020\u001eH\u0002J0\u0010\u001c\u001a\u00020\u00192\u0006\u0010\u001f\u001a\u00020\n2\u0006\u0010 \u001a\u00020!2\u0006\u0010\"\u001a\u00020!2\u0006\u0010#\u001a\u00020\u00072\u0006\u0010$\u001a\u00020\u0007H\u0002J\u0010\u0010%\u001a\u00020\u00072\u0006\u0010&\u001a\u00020\u0007H\u0002J\u000e\u0010'\u001a\u00020\u00192\u0006\u0010\u001d\u001a\u00020\u001eJ\u0018\u0010(\u001a\u00020\u00192\b\b\u0001\u0010)\u001a\u00020\u00072\u0006\u0010\u001d\u001a\u00020\u001eR\u0010\u0010\t\u001a\u0004\u0018\u00010\nX\u0082\u000e¢\u0006\u0002\n\u0000R\u0012\u0010\u000b\u001a\u00020\u00078\u0002@\u0002X\u0083\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\f\u001a\u0004\u0018\u00010\nX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u000e¢\u0006\u0002\n\u0000R\u0012\u0010\u000f\u001a\u00020\u00078\u0002@\u0002X\u0083\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0010\u001a\u0004\u0018\u00010\nX\u0082\u000e¢\u0006\u0002\n\u0000R\u0012\u0010\u0011\u001a\u00020\u00078\u0002@\u0002X\u0083\u000e¢\u0006\u0002\n\u0000R\u0012\u0010\u0012\u001a\u00020\u00078\u0002@\u0002X\u0083\u000e¢\u0006\u0002\n\u0000R\u0012\u0010\u0013\u001a\u00020\u00078\u0002@\u0002X\u0083\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u000eX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u000eX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u000eX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u000eX\u0082\u000e¢\u0006\u0002\n\u0000¨\u0006*"},
   d2 = {"Lco/uk/getmondo/common/ui/AmountView;", "Landroid/support/v7/widget/AppCompatTextView;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "currencyFont", "", "currencyTextSize", "fractionalFont", "fractionalSemiTransparent", "", "fractionalTextSize", "integerFont", "integerTextSize", "negativeAmountColor", "positiveAmountColor", "showCurrency", "showFractionalPart", "showNegativeIfDebit", "showPlusIfCredit", "applyAttributes", "", "a", "Landroid/content/res/TypedArray;", "bind", "amount", "Lco/uk/getmondo/model/Amount;", "currencySymbol", "amountValue", "", "integerPartAbs", "fractionalPartAbs", "fractionalDigits", "convertToSemiTransparent", "originalColor", "setAmount", "setAmountWithStyle", "style", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class AmountView extends aa {
   private boolean a;
   private boolean b;
   private boolean c;
   private boolean d;
   private boolean e;
   private int f;
   private int g;
   private int h;
   private int i;
   private int j;
   private String k;
   private String l;
   private String m;

   public AmountView(Context var1) {
      this(var1, (AttributeSet)null, 0, 6, (kotlin.d.b.i)null);
   }

   public AmountView(Context var1, AttributeSet var2) {
      this(var1, var2, 0, 4, (kotlin.d.b.i)null);
   }

   public AmountView(Context var1, AttributeSet var2, int var3) {
      kotlin.d.b.l.b(var1, "context");
      super(var1, var2, var3);
      if(var2 != null) {
         TypedArray var6 = var1.getTheme().obtainStyledAttributes(var2, co.uk.getmondo.c.b.AmountView, var3, 2131493041);

         try {
            kotlin.d.b.l.a(var6, "attributes");
            this.a(var6);
         } finally {
            var6.recycle();
         }
      }

      if(this.isInEditMode()) {
         this.a("£", 12050L, 120L, 50, 2);
      }

   }

   // $FF: synthetic method
   public AmountView(Context var1, AttributeSet var2, int var3, int var4, kotlin.d.b.i var5) {
      if((var4 & 2) != 0) {
         var2 = (AttributeSet)null;
      }

      if((var4 & 4) != 0) {
         var3 = 0;
      }

      this(var1, var2, var3);
   }

   private final int a(int var1) {
      return Color.argb(102, var1 >> 16 & 255, var1 >> 8 & 255, var1 & 255);
   }

   private final void a(TypedArray var1) {
      this.a = var1.getBoolean(9, true);
      this.b = var1.getBoolean(7, false);
      this.c = var1.getBoolean(8, false);
      this.d = var1.getBoolean(6, true);
      this.f = var1.getColor(3, android.support.v4.content.a.c(this.getContext(), 2131689706));
      this.g = var1.getColor(4, this.f);
      this.e = var1.getBoolean(5, false);
      this.h = var1.getDimensionPixelSize(0, this.getResources().getDimensionPixelSize(2131427482));
      this.i = var1.getDimensionPixelSize(1, this.getResources().getDimensionPixelSize(2131427431));
      this.j = var1.getDimensionPixelSize(2, this.getResources().getDimensionPixelSize(2131427427));
      String var2;
      if(var1.hasValue(12)) {
         var2 = var1.getString(12);
      } else {
         var2 = "sans-serif-light";
      }

      this.k = var2;
      if(var1.hasValue(10)) {
         var2 = var1.getString(10);
      } else {
         var2 = "sans-serif-light";
      }

      this.l = var2;
      String var3;
      if(var1.hasValue(11)) {
         var3 = var1.getString(11);
      } else {
         var3 = "sans-serif";
      }

      this.m = var3;
   }

   private final void a(co.uk.getmondo.d.c var1) {
      this.a(var1.g(), var1.k(), var1.c(), var1.d(), var1.l().a());
   }

   private final void a(String var1, long var2, long var4, int var6, int var7) {
      this.setText((CharSequence)null);
      String var9;
      if(this.b && var2 > 0L) {
         var9 = "+";
      } else if(this.c && var2 < 0L) {
         var9 = "-";
      } else {
         var9 = "";
      }

      if(!this.d) {
         var1 = "";
      }

      String var12 = co.uk.getmondo.common.i.b.a.a(var4);
      String var10;
      if(!this.a) {
         var10 = "";
      } else {
         var10 = co.uk.getmondo.common.i.d.a(var6, var7);
      }

      SpannableString var13 = new SpannableString((CharSequence)(var9 + var1 + var12 + var10));
      if(var2 < 0L) {
         var6 = this.g;
      } else {
         var6 = this.f;
      }

      boolean var15;
      if(((CharSequence)var9).length() > 0) {
         var15 = true;
      } else {
         var15 = false;
      }

      int var8;
      Typeface var16;
      if(var15) {
         var8 = var9.length();
         if(this.d) {
            var7 = this.h;
         } else {
            var7 = this.i;
         }

         String var11;
         if(this.d) {
            var11 = this.k;
         } else {
            var11 = this.l;
         }

         var13.setSpan(new ForegroundColorSpan(var6), 0, var8, 33);
         var13.setSpan(new AbsoluteSizeSpan(var7), 0, var8, 33);
         var16 = Typeface.create(var11, 0);
         kotlin.d.b.l.a(var16, "Typeface.create(prefixFont, Typeface.NORMAL)");
         var13.setSpan(new k(var16), 0, var8, 33);
      }

      if(((CharSequence)var1).length() > 0) {
         var15 = true;
      } else {
         var15 = false;
      }

      if(var15) {
         var7 = var9.length();
         var8 = var1.length() + var7;
         var13.setSpan(new ForegroundColorSpan(var6), var7, var8, 33);
         var13.setSpan(new AbsoluteSizeSpan(this.h), var7, var8, 33);
         var16 = Typeface.create(this.k, 0);
         kotlin.d.b.l.a(var16, "Typeface.create(currencyFont, Typeface.NORMAL)");
         var13.setSpan(new k(var16), var7, var8, 33);
      }

      if(((CharSequence)var12).length() > 0) {
         var15 = true;
      } else {
         var15 = false;
      }

      if(var15) {
         var7 = var9.length() + var1.length();
         var8 = var12.length() + var7;
         var13.setSpan(new ForegroundColorSpan(var6), var7, var8, 33);
         var13.setSpan(new AbsoluteSizeSpan(this.i), var7, var8, 33);
         var16 = Typeface.create(this.l, 0);
         kotlin.d.b.l.a(var16, "Typeface.create(integerFont, Typeface.NORMAL)");
         var13.setSpan(new k(var16), var7, var8, 33);
      }

      if(((CharSequence)var10).length() > 0) {
         var15 = true;
      } else {
         var15 = false;
      }

      if(var15) {
         var7 = var6;
         if(this.e) {
            var7 = this.a(var6);
         }

         var8 = var9.length() + var1.length() + var12.length();
         var6 = var10.length() + var8;
         var13.setSpan(new ForegroundColorSpan(var7), var8, var6, 33);
         var13.setSpan(new AbsoluteSizeSpan(this.j), var8, var6, 33);
         Typeface var14 = Typeface.create(this.m, 0);
         kotlin.d.b.l.a(var14, "Typeface.create(fractionalFont, Typeface.NORMAL)");
         var13.setSpan(new k(var14), var8, var6, 33);
      }

      this.setText((CharSequence)var13);
   }

   public final void a(int var1, co.uk.getmondo.d.c var2) {
      kotlin.d.b.l.b(var2, "amount");
      TypedArray var3 = this.getContext().obtainStyledAttributes(var1, co.uk.getmondo.c.b.AmountView);

      try {
         kotlin.d.b.l.a(var3, "attributes");
         this.a(var3);
      } finally {
         var3.recycle();
      }

      this.a(var2);
   }

   public final void setAmount(co.uk.getmondo.d.c var1) {
      kotlin.d.b.l.b(var1, "amount");
      this.a(var1);
   }
}
