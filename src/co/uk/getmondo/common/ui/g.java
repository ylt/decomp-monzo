package co.uk.getmondo.common.ui;

import android.view.View;
import android.view.View.OnFocusChangeListener;

// $FF: synthetic class
final class g implements OnFocusChangeListener {
   private final PinEntryView a;

   private g(PinEntryView var1) {
      this.a = var1;
   }

   public static OnFocusChangeListener a(PinEntryView var0) {
      return new g(var0);
   }

   public void onFocusChange(View var1, boolean var2) {
      PinEntryView.a(this.a, var1, var2);
   }
}
