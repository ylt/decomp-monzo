package co.uk.getmondo.common.ui;

import com.google.android.exoplayer2.upstream.RawResourceDataSource;

// $FF: synthetic class
final class l implements com.google.android.exoplayer2.upstream.c.a {
   private final RawResourceDataSource a;

   private l(RawResourceDataSource var1) {
      this.a = var1;
   }

   public static com.google.android.exoplayer2.upstream.c.a a(RawResourceDataSource var0) {
      return new l(var0);
   }

   public com.google.android.exoplayer2.upstream.c a() {
      return VideoImageView.a(this.a);
   }
}
