package co.uk.getmondo.common.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import co.uk.getmondo.common.ae;
import java.util.HashMap;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0014\u0018\u00002\u00020\u0001B%\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bR(\u0010\u000b\u001a\u0004\u0018\u00010\n2\b\u0010\t\u001a\u0004\u0018\u00010\n@FX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\r\"\u0004\b\u000e\u0010\u000fR(\u0010\u0010\u001a\u0004\u0018\u00010\n2\b\u0010\t\u001a\u0004\u0018\u00010\n@FX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\r\"\u0004\b\u0012\u0010\u000fR&\u0010\u0013\u001a\u00020\u00072\u0006\u0010\t\u001a\u00020\u00078\u0006@FX\u0087\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0014\u0010\u0015\"\u0004\b\u0016\u0010\u0017R(\u0010\u0018\u001a\u0004\u0018\u00010\n2\b\u0010\t\u001a\u0004\u0018\u00010\n@FX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\r\"\u0004\b\u001a\u0010\u000fR&\u0010\u001b\u001a\u00020\u00072\u0006\u0010\t\u001a\u00020\u00078\u0006@FX\u0087\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u001c\u0010\u0015\"\u0004\b\u001d\u0010\u0017¨\u0006\u001e"},
   d2 = {"Lco/uk/getmondo/common/ui/ActionView;", "Landroid/support/constraint/ConstraintLayout;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyle", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "value", "", "actionSubtitle", "getActionSubtitle", "()Ljava/lang/String;", "setActionSubtitle", "(Ljava/lang/String;)V", "actionTitle", "getActionTitle", "setActionTitle", "actionTitleColor", "getActionTitleColor", "()I", "setActionTitleColor", "(I)V", "iconUrl", "getIconUrl", "setIconUrl", "placeholderIcon", "getPlaceholderIcon", "setPlaceholderIcon", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ActionView extends ConstraintLayout {
   private String c;
   private int d;
   private String e;
   private int f;
   private String g;
   private HashMap h;

   public ActionView(Context var1) {
      this(var1, (AttributeSet)null, 0, 6, (kotlin.d.b.i)null);
   }

   public ActionView(Context var1, AttributeSet var2) {
      this(var1, var2, 0, 4, (kotlin.d.b.i)null);
   }

   public ActionView(Context var1, AttributeSet var2, int var3) {
      kotlin.d.b.l.b(var1, "context");
      super(var1, var2, var3);
      LayoutInflater.from(var1).inflate(2131034419, (ViewGroup)this);
      ((ImageView)this.b(co.uk.getmondo.c.a.transactionActionIconImageView)).setClipToOutline(true);
      if(var2 != null) {
         TypedArray var4 = var1.obtainStyledAttributes(var2, co.uk.getmondo.c.b.ActionView, var3, 0);
         this.setActionTitle(var4.getString(0));
         this.setActionTitleColor(var4.getColor(1, 0));
         this.setActionSubtitle(var4.getString(2));
         this.setPlaceholderIcon(var4.getResourceId(3, 0));
         var4.recycle();
      }

   }

   // $FF: synthetic method
   public ActionView(Context var1, AttributeSet var2, int var3, int var4, kotlin.d.b.i var5) {
      if((var4 & 2) != 0) {
         var2 = (AttributeSet)null;
      }

      if((var4 & 4) != 0) {
         var3 = 0;
      }

      this(var1, var2, var3);
   }

   public View b(int var1) {
      if(this.h == null) {
         this.h = new HashMap();
      }

      View var3 = (View)this.h.get(Integer.valueOf(var1));
      View var2 = var3;
      if(var3 == null) {
         var2 = this.findViewById(var1);
         this.h.put(Integer.valueOf(var1), var2);
      }

      return var2;
   }

   public final String getActionSubtitle() {
      return this.e;
   }

   public final String getActionTitle() {
      return this.c;
   }

   public final int getActionTitleColor() {
      return this.d;
   }

   public final String getIconUrl() {
      return this.g;
   }

   public final int getPlaceholderIcon() {
      return this.f;
   }

   public final void setActionSubtitle(String var1) {
      this.e = var1;
      if(var1 != null) {
         ((TextView)this.b(co.uk.getmondo.c.a.transactionActionSubtitleTextView)).setText((CharSequence)var1);
         ae.a((View)((TextView)this.b(co.uk.getmondo.c.a.transactionActionSubtitleTextView)));
      } else {
         ae.b((TextView)this.b(co.uk.getmondo.c.a.transactionActionSubtitleTextView));
      }

   }

   public final void setActionTitle(String var1) {
      this.c = var1;
      ((TextView)this.b(co.uk.getmondo.c.a.transactionActionTitleTextView)).setText((CharSequence)var1);
   }

   public final void setActionTitleColor(int var1) {
      if(var1 == 0) {
         var1 = 2131689487;
      }

      this.d = var1;
      ((TextView)this.b(co.uk.getmondo.c.a.transactionActionTitleTextView)).setTextColor(android.support.v4.content.a.b.b(this.getResources(), this.d, this.getContext().getTheme()));
   }

   public final void setIconUrl(String var1) {
      this.g = var1;
      com.bumptech.glide.g.b(this.getContext()).a(this.g).a(this.f).a(0.1F).a((ImageView)this.b(co.uk.getmondo.c.a.transactionActionIconImageView));
   }

   public final void setPlaceholderIcon(int var1) {
      this.f = var1;
      ((ImageView)this.b(co.uk.getmondo.c.a.transactionActionIconImageView)).setImageResource(this.f);
   }
}
