package co.uk.getmondo.common.ui;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.animation.DecelerateInterpolator;
import android.widget.TextView;
import java.text.NumberFormat;
import java.util.Locale;

public class AnimatedNumberView extends TextView {
   private ObjectAnimator a;
   private int b;
   private int c;

   public AnimatedNumberView(Context var1) {
      this(var1, (AttributeSet)null, 0);
   }

   public AnimatedNumberView(Context var1, AttributeSet var2) {
      this(var1, var2, 0);
   }

   public AnimatedNumberView(Context var1, AttributeSet var2, int var3) {
      super(var1, var2, var3);
      if(var2 != null) {
         TypedArray var4 = this.getContext().obtainStyledAttributes(var2, co.uk.getmondo.c.b.AnimatedNumberView);
         this.c = var4.getInteger(0, 3000);
         var4.recycle();
      } else {
         this.c = 3000;
      }

   }

   private void setNumberAsString(int var1) {
      this.setText(String.valueOf(NumberFormat.getNumberInstance(Locale.getDefault()).format((long)var1)));
   }

   public void a(int var1) {
      if(this.a != null) {
         this.a.cancel();
      }

      this.a = ObjectAnimator.ofInt(this, "numberInterpolator", new int[]{Math.max(this.b - var1, 0), this.b});
      this.a.setInterpolator(new DecelerateInterpolator(3.0F));
      this.a.setDuration((long)this.c);
      this.a.start();
   }

   public void setNumber(int var1) {
      if(var1 == 0) {
         this.setText(2131362497);
      } else {
         this.setNumberAsString(var1);
      }

      if(this.c == 0) {
         this.setNumberAsString(var1);
      } else {
         if(this.a != null) {
            this.a.cancel();
         }

         this.a = ObjectAnimator.ofInt(this, "numberInterpolator", new int[]{this.b, var1});
         this.a.setInterpolator(new DecelerateInterpolator(3.0F));
         this.a.setDuration((long)this.c);
         this.a.start();
      }

   }

   public void setNumberInterpolator(int var1) {
      this.b = var1;
      this.setNumberAsString(var1);
   }
}
