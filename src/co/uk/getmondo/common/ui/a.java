package co.uk.getmondo.common.ui;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import co.uk.getmondo.common.k.n;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\u0018\u0000 \u000b2\u00020\u0001:\u0002\u000b\fB\u0013\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\u0002\u0010\u0005J\u000e\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\tJ\u000e\u0010\n\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\tR\u0014\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\r"},
   d2 = {"Lco/uk/getmondo/common/ui/AvatarGenerator;", "", "colors", "", "", "(Ljava/util/List;)V", "from", "Lco/uk/getmondo/common/ui/AvatarGenerator$Creator;", "text", "", "generateColor", "Companion", "Creator", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class a {
   public static final a.a a = new a.a((kotlin.d.b.i)null);
   private final List b;

   public a(List var1) {
      kotlin.d.b.l.b(var1, "colors");
      super();
      this.b = var1;
   }

   public static final a a(Context var0) {
      kotlin.d.b.l.b(var0, "context");
      return a.a(var0);
   }

   public final a.b a(String var1) {
      kotlin.d.b.l.b(var1, "text");
      if(kotlin.h.j.a((CharSequence)var1)) {
         throw (Throwable)(new IllegalArgumentException("'Input is blank"));
      } else {
         boolean var2;
         if(kotlin.h.j.f((CharSequence)var1) != 43 && !Character.isDigit(kotlin.h.j.f((CharSequence)var1))) {
            var2 = false;
         } else {
            var2 = true;
         }

         String var3;
         if(var2) {
            var3 = "#";
         } else {
            var3 = String.valueOf(kotlin.h.j.f((CharSequence)var1));
         }

         return new a.b(var3, this.b(var1));
      }
   }

   public final int b(String var1) {
      kotlin.d.b.l.b(var1, "text");
      CharSequence var4 = (CharSequence)var1;
      int var2 = 0;

      int var3;
      for(var3 = 0; var2 < var4.length(); ++var2) {
         var3 += var4.charAt(var2);
      }

      var2 = var3 % this.b.size();
      if(var2 < this.b.size()) {
         var2 = ((Number)this.b.get(var2)).intValue();
      } else {
         var2 = ((Number)this.b.get(0)).intValue();
      }

      return var2;
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007¨\u0006\u0007"},
      d2 = {"Lco/uk/getmondo/common/ui/AvatarGenerator$Companion;", "", "()V", "ofCategories", "Lco/uk/getmondo/common/ui/AvatarGenerator;", "context", "Landroid/content/Context;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var1) {
         this();
      }

      public final a a(Context var1) {
         kotlin.d.b.l.b(var1, "context");
         Object[] var4 = (Object[])(new co.uk.getmondo.d.h[]{co.uk.getmondo.d.h.GROCERIES, co.uk.getmondo.d.h.ENTERTAINMENT, co.uk.getmondo.d.h.EATING_OUT, co.uk.getmondo.d.h.SHOPPING, co.uk.getmondo.d.h.HOLIDAYS, co.uk.getmondo.d.h.EXPENSES, co.uk.getmondo.d.h.TRANSPORT, co.uk.getmondo.d.h.CASH});
         Collection var3 = (Collection)(new ArrayList(var4.length));

         for(int var2 = 0; var2 < var4.length; ++var2) {
            var3.add(Integer.valueOf(android.support.v4.content.a.c(var1, ((co.uk.getmondo.d.h)var4[var2]).b())));
         }

         return new a((List)var3);
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B\u0017\b\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J&\u0010\u000b\u001a\u00020\f2\b\b\u0002\u0010\r\u001a\u00020\u00052\b\b\u0002\u0010\u000e\u001a\u00020\u000f2\b\b\u0002\u0010\u0010\u001a\u00020\u0011H\u0007R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0012"},
      d2 = {"Lco/uk/getmondo/common/ui/AvatarGenerator$Creator;", "", "text", "", "color", "", "(Ljava/lang/String;I)V", "getColor", "()I", "getText", "()Ljava/lang/String;", "create", "Landroid/graphics/drawable/Drawable;", "fontSize", "typeface", "Landroid/graphics/Typeface;", "isSquare", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class b {
      private final String a;
      private final int b;

      public b(String var1, int var2) {
         kotlin.d.b.l.b(var1, "text");
         super();
         this.a = var1;
         this.b = var2;
      }

      // $FF: synthetic method
      public static Drawable a(a.b var0, int var1, Typeface var2, boolean var3, int var4, Object var5) {
         if((var4 & 1) != 0) {
            var1 = n.b(20);
         }

         if((var4 & 2) != 0) {
            var2 = Typeface.create("sans-serif-medium", 0);
            kotlin.d.b.l.a(var2, "Typeface.create(\"sans-se…medium\", Typeface.NORMAL)");
         }

         if((var4 & 4) != 0) {
            var3 = false;
         }

         return var0.a(var1, var2, var3);
      }

      public final Drawable a(int var1) {
         return a(this, var1, (Typeface)null, false, 6, (Object)null);
      }

      public final Drawable a(int var1, Typeface var2, boolean var3) {
         kotlin.d.b.l.b(var2, "typeface");
         int var4 = co.uk.getmondo.common.k.c.a(this.b, 0.2F);
         com.a.a.a.d var5 = com.a.a.a.a().b().b(var1).a(var2).a(this.b).a().c();
         com.a.a.a var6;
         Drawable var7;
         if(var3) {
            var6 = var5.a(this.a, var4);
            kotlin.d.b.l.a(var6, "builder\n                …ct(text, backgroundColor)");
            var7 = (Drawable)var6;
         } else {
            var6 = var5.b(this.a, var4);
            kotlin.d.b.l.a(var6, "builder\n                …nd(text, backgroundColor)");
            var7 = (Drawable)var6;
         }

         return var7;
      }
   }
}
