package co.uk.getmondo.common.ui;

import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\r\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003¢\u0006\u0002\u0010\u0005J\u0018\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\u00012\u0006\u0010\r\u001a\u00020\u000eH\u0002J\u0006\u0010\u000f\u001a\u00020\u0010J\u0018\u0010\u0011\u001a\u00020\u00002\u0006\u0010\u0012\u001a\u00020\u00132\b\b\u0002\u0010\r\u001a\u00020\u000eJ\u0018\u0010\u0014\u001a\u00020\u00002\u0006\u0010\u0015\u001a\u00020\u00132\b\b\u0002\u0010\r\u001a\u00020\u000eJ \u0010\u0016\u001a\u00020\u00002\u0006\u0010\u0017\u001a\u00020\u00032\u0006\u0010\u0018\u001a\u00020\u00132\b\b\u0002\u0010\r\u001a\u00020\u000eR\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0019"},
   d2 = {"Lco/uk/getmondo/common/ui/SpannableBuilder;", "", "text", "", "textToFormat", "(Ljava/lang/String;Ljava/lang/String;)V", "matcher", "Ljava/util/regex/Matcher;", "spannableString", "Landroid/text/SpannableString;", "apply", "", "span", "applyOnlyToSubstring", "", "build", "", "setColour", "colour", "", "setSize", "size", "setTypeface", "fontFamily", "style", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class j {
   private final Matcher a;
   private final SpannableString b;

   public j(String var1, String var2) {
      kotlin.d.b.l.b(var1, "text");
      kotlin.d.b.l.b(var2, "textToFormat");
      super();
      Matcher var3 = Pattern.compile(var2).matcher((CharSequence)var1);
      kotlin.d.b.l.a(var3, "Pattern.compile(textToFormat).matcher(text)");
      this.a = var3;
      this.b = new SpannableString((CharSequence)var1);
   }

   private final void a(Object var1, boolean var2) {
      if(!var2) {
         this.b.setSpan(var1, 0, this.b.length(), 33);
      } else {
         while(true) {
            if(!this.a.find()) {
               this.a.reset();
               break;
            }

            this.b.setSpan(var1, this.a.start(), this.a.end(), 33);
         }
      }

   }

   public final j a(int var1, boolean var2) {
      this.a(new ForegroundColorSpan(var1), var2);
      return this;
   }

   public final j a(String var1, int var2, boolean var3) {
      kotlin.d.b.l.b(var1, "fontFamily");
      Typeface var4 = Typeface.create(var1, var2);
      kotlin.d.b.l.a(var4, "Typeface.create(fontFamily, style)");
      this.a(new k(var4), var3);
      return this;
   }

   public final CharSequence a() {
      return (CharSequence)this.b;
   }
}
