package co.uk.getmondo.common.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.net.Uri;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.n;
import com.google.android.exoplayer2.t;
import com.google.android.exoplayer2.u;
import com.google.android.exoplayer2.source.q;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.RawResourceDataSource;
import com.google.android.exoplayer2.upstream.RawResourceDataSource.RawResourceDataSourceException;

public class VideoImageView extends FrameLayout {
   @BindView(2131821711)
   SimpleExoPlayerView exoPlayerView;
   @BindView(2131821712)
   ImageView placeholderImageView;

   public VideoImageView(Context var1) {
      this(var1, (AttributeSet)null, 0);
   }

   public VideoImageView(Context var1, AttributeSet var2) {
      this(var1, var2, 0);
   }

   public VideoImageView(Context var1, AttributeSet var2, int var3) {
      super(var1, var2, var3);
      LayoutInflater.from(this.getContext()).inflate(2131034426, this, true);
      ButterKnife.bind((View)this);
      TypedArray var7 = this.getContext().getTheme().obtainStyledAttributes(var2, co.uk.getmondo.c.b.VideoImageView, 0, 0);

      int var4;
      try {
         var3 = var7.getResourceId(0, 0);
         this.placeholderImageView.setImageResource(var3);
         var4 = var7.getResourceId(1, 0);
         this.placeholderImageView.setImageResource(var3);
         VideoImageView.a var8 = VideoImageView.a.a(var7.getInt(2, 0));
         this.exoPlayerView.setResizeMode(var8.f);
      } finally {
         var7.recycle();
      }

      this.a(var4);
   }

   // $FF: synthetic method
   static com.google.android.exoplayer2.upstream.c a(RawResourceDataSource var0) {
      return var0;
   }

   private void a(int var1) {
      try {
         Context var2 = this.getContext();
         com.google.android.exoplayer2.b.b var4 = new com.google.android.exoplayer2.b.b();
         com.google.android.exoplayer2.c var3 = new com.google.android.exoplayer2.c();
         t var8 = com.google.android.exoplayer2.f.a(var2, var4, var3);
         this.exoPlayerView.setPlayer(var8);
         RawResourceDataSource var5 = new RawResourceDataSource(this.getContext());
         com.google.android.exoplayer2.upstream.e var9 = new com.google.android.exoplayer2.upstream.e(RawResourceDataSource.a(var1));
         var5.a(var9);
         Uri var12 = var5.a();
         com.google.android.exoplayer2.upstream.c.a var14 = l.a(var5);
         com.google.android.exoplayer2.extractor.c var6 = new com.google.android.exoplayer2.extractor.c();
         com.google.android.exoplayer2.source.f var10 = new com.google.android.exoplayer2.source.f(var12, var14, var6, (Handler)null, (com.google.android.exoplayer2.source.f.a)null);
         com.google.android.exoplayer2.source.g var13 = new com.google.android.exoplayer2.source.g(var10);
         var8.a(var13);
         var8.a(true);
         com.google.android.exoplayer2.e.a var11 = new com.google.android.exoplayer2.e.a() {
            public void a() {
            }

            public void a(int var1) {
            }

            public void a(ExoPlaybackException var1) {
               VideoImageView.this.exoPlayerView.setVisibility(8);
               VideoImageView.this.placeholderImageView.setVisibility(0);
            }

            public void a(n var1) {
            }

            public void a(q var1, com.google.android.exoplayer2.b.f var2) {
            }

            public void a(u var1, Object var2) {
            }

            public void a(boolean var1) {
            }

            public void a(boolean var1, int var2) {
               if(var2 == 3) {
                  VideoImageView.this.exoPlayerView.setVisibility(0);
                  VideoImageView.this.placeholderImageView.setVisibility(8);
               }

            }
         };
         var8.a(var11);
      } catch (RawResourceDataSourceException var7) {
         d.a.a.a(var7, "Failed to play video", new Object[0]);
      }

   }

   public void a() {
      t var1 = this.exoPlayerView.getPlayer();
      if(var1 != null) {
         var1.d();
      }

   }

   private static enum a {
      a(0, 3),
      b(1, 0),
      c(2, 2),
      d(3, 1);

      private final int e;
      private final int f;

      private a(int var3, int var4) {
         this.e = var3;
         this.f = var4;
      }

      static VideoImageView.a a(int var0) {
         VideoImageView.a[] var4 = values();
         int var2 = var4.length;

         for(int var1 = 0; var1 < var2; ++var1) {
            VideoImageView.a var3 = var4[var1];
            if(var3.e == var0) {
               return var3;
            }
         }

         throw new IllegalArgumentException("Unexpected ResizeMode id: " + var0);
      }
   }
}
