package co.uk.getmondo.common.ui;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.TextView;

public final class i {
   public static Snackbar a(Context var0, View var1, String var2, int var3, boolean var4) {
      int var5;
      if(var4) {
         var5 = 2131689680;
      } else {
         var5 = 2131689663;
      }

      var5 = android.support.v4.content.a.c(var0, var5);
      int var6 = android.support.v4.content.a.c(var0, 2131689706);
      Snackbar var7 = Snackbar.a(var1, var2, var3);
      var7.b().setBackgroundColor(var5);
      var7.e(var6);
      TextView var8 = (TextView)var7.b().findViewById(2131821234);
      var8.setTextColor(var6);
      var8.setMaxLines(5);
      return var7;
   }
}
