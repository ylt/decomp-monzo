package co.uk.getmondo.common.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.graphics.drawable.shapes.Shape;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import co.uk.getmondo.common.ae;
import com.airbnb.lottie.LottieAnimationView;
import io.reactivex.o;
import io.reactivex.p;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.n;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0010\u000e\n\u0002\b\u0011\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u000f\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B%\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ,\u00109\u001a\u00020.2\u0006\u0010\u001f\u001a\u00020\u001b2\b\b\u0002\u0010:\u001a\u00020\u00122\b\b\u0002\u00106\u001a\u00020\u001b2\b\b\u0003\u0010\f\u001a\u00020\u0007J\u001a\u0010;\u001a\u00020.2\u0006\u0010<\u001a\u00020\u00122\n\b\u0002\u0010\u001f\u001a\u0004\u0018\u00010\u001bJ\f\u0010=\u001a\b\u0012\u0004\u0012\u00020.0>R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004¢\u0006\u0002\n\u0000R&\u0010\f\u001a\u00020\u00072\u0006\u0010\u000b\u001a\u00020\u00078\u0006@FX\u0087\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R\u0012\u0010\u0011\u001a\u00020\u00078\u0002@\u0002X\u0083\u000e¢\u0006\u0002\n\u0000R$\u0010\u0013\u001a\u00020\u00122\u0006\u0010\u000b\u001a\u00020\u0012@FX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0014\u0010\u0015\"\u0004\b\u0016\u0010\u0017R&\u0010\u0018\u001a\u00020\u00072\u0006\u0010\u000b\u001a\u00020\u00078\u0006@FX\u0087\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\u000e\"\u0004\b\u001a\u0010\u0010R\u001e\u0010\u001c\u001a\u00020\u001b2\u0006\u0010\u000b\u001a\u00020\u001b@BX\u0082\u000e¢\u0006\b\n\u0000\"\u0004\b\u001d\u0010\u001eR(\u0010\u001f\u001a\u0004\u0018\u00010\u001b2\b\u0010\u000b\u001a\u0004\u0018\u00010\u001b8F@FX\u0086\u000e¢\u0006\f\u001a\u0004\b \u0010!\"\u0004\b\"\u0010\u001eR$\u0010#\u001a\u00020\u00072\u0006\u0010\u000b\u001a\u00020\u0007@FX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b$\u0010\u000e\"\u0004\b%\u0010\u0010R&\u0010&\u001a\u00020\u00072\u0006\u0010\u000b\u001a\u00020\u00078\u0006@FX\u0087\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b'\u0010\u000e\"\u0004\b(\u0010\u0010R$\u0010)\u001a\u00020\u00122\u0006\u0010\u000b\u001a\u00020\u0012@FX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b*\u0010\u0015\"\u0004\b+\u0010\u0017R\"\u0010,\u001a\n\u0012\u0004\u0012\u00020.\u0018\u00010-X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b/\u00100\"\u0004\b1\u00102R$\u00103\u001a\u00020\u00122\u0006\u0010\u000b\u001a\u00020\u0012@FX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b4\u0010\u0015\"\u0004\b5\u0010\u0017R$\u00106\u001a\u00020\u001b2\u0006\u0010\u000b\u001a\u00020\u001b8F@FX\u0086\u000e¢\u0006\f\u001a\u0004\b7\u0010!\"\u0004\b8\u0010\u001e¨\u0006?"},
   d2 = {"Lco/uk/getmondo/common/ui/LoadingErrorView;", "Landroid/support/constraint/ConstraintLayout;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyle", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "circleBackground", "Landroid/graphics/drawable/ShapeDrawable;", "value", "iconRes", "getIconRes", "()I", "setIconRes", "(I)V", "iconTint", "", "loading", "getLoading", "()Z", "setLoading", "(Z)V", "loadingIconBackgroundColor", "getLoadingIconBackgroundColor", "setLoadingIconBackgroundColor", "", "lottieAnimationFileName", "setLottieAnimationFileName", "(Ljava/lang/String;)V", "message", "getMessage", "()Ljava/lang/String;", "setMessage", "messageTextColor", "getMessageTextColor", "setMessageTextColor", "overlayBackgroundColor", "getOverlayBackgroundColor", "setOverlayBackgroundColor", "overlayEnabled", "getOverlayEnabled", "setOverlayEnabled", "retryClickListener", "Lkotlin/Function0;", "", "getRetryClickListener", "()Lkotlin/jvm/functions/Function0;", "setRetryClickListener", "(Lkotlin/jvm/functions/Function0;)V", "retryEnabled", "getRetryEnabled", "setRetryEnabled", "retryLabel", "getRetryLabel", "setRetryLabel", "error", "shouldRetry", "load", "isLoading", "retryClicks", "Lio/reactivex/Observable;", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class LoadingErrorView extends ConstraintLayout {
   private final ShapeDrawable c;
   private boolean d;
   private int e;
   private boolean f;
   private int g;
   private int h;
   private boolean i;
   private int j;
   private kotlin.d.a.a k;
   private int l;
   private String m;
   private HashMap n;

   public LoadingErrorView(Context var1) {
      this(var1, (AttributeSet)null, 0, 6, (kotlin.d.b.i)null);
   }

   public LoadingErrorView(Context var1, AttributeSet var2) {
      this(var1, var2, 0, 4, (kotlin.d.b.i)null);
   }

   public LoadingErrorView(Context var1, AttributeSet var2, int var3) {
      kotlin.d.b.l.b(var1, "context");
      super(var1, var2, var3);
      this.c = new ShapeDrawable((Shape)(new OvalShape()));
      this.e = 2131689558;
      this.h = 2131689589;
      this.j = 2131689560;
      this.m = "lottie/loading_spinner_colored.json";
      View.inflate(var1, 2131034434, (ViewGroup)this);
      final FrameLayout var4 = (FrameLayout)this.b(co.uk.getmondo.c.a.progressLayout);
      var4.getViewTreeObserver().addOnGlobalLayoutListener((OnGlobalLayoutListener)(new OnGlobalLayoutListener() {
         public void onGlobalLayout() {
            var4.getViewTreeObserver().removeOnGlobalLayoutListener((OnGlobalLayoutListener)this);
            FrameLayout var1 = (FrameLayout)var4;
            ShapeDrawable var2 = LoadingErrorView.this.c;
            var2.setIntrinsicWidth(((FrameLayout)((View)var1).findViewById(co.uk.getmondo.c.a.progressLayout)).getMeasuredWidth());
            var2.setIntrinsicHeight(((FrameLayout)((View)var1).findViewById(co.uk.getmondo.c.a.progressLayout)).getMeasuredHeight());
         }
      }));
      if(var2 != null) {
         TypedArray var6 = var1.obtainStyledAttributes(var2, co.uk.getmondo.c.b.LoadingErrorView, var3, 0);
         this.setOverlayEnabled(var6.getBoolean(0, false));
         this.setOverlayBackgroundColor(var6.getResourceId(1, 2131689558));
         this.setLoadingIconBackgroundColor(var6.getResourceId(5, 0));
         this.setLoading(var6.getBoolean(2, false));
         this.l = var6.getResourceId(4, 0);
         this.setIconRes(var6.getResourceId(3, 0));
         this.setMessage(var6.getString(6));
         this.setRetryEnabled(var6.getBoolean(8, false));
         String var5 = var6.getString(9);
         if(var5 == null) {
            var5 = ae.a(this, 2131362405);
         }

         this.setRetryLabel(var5);
         var5 = var6.getString(10);
         if(var5 == null) {
            var5 = "lottie/loading_spinner_colored.json";
         }

         this.setLottieAnimationFileName(var5);
         this.setMessageTextColor(var6.getResourceId(7, 2131689560));
         var6.recycle();
      }

      ((Button)this.b(co.uk.getmondo.c.a.retryButton)).setOnClickListener((OnClickListener)(new OnClickListener() {
         public final void onClick(View var1) {
            kotlin.d.a.a var2 = LoadingErrorView.this.getRetryClickListener();
            if(var2 != null) {
               n var3 = (n)var2.v_();
            }

         }
      }));
   }

   // $FF: synthetic method
   public LoadingErrorView(Context var1, AttributeSet var2, int var3, int var4, kotlin.d.b.i var5) {
      if((var4 & 2) != 0) {
         var2 = (AttributeSet)null;
      }

      if((var4 & 4) != 0) {
         var3 = 2130772372;
      }

      this(var1, var2, var3);
   }

   // $FF: synthetic method
   public static void a(LoadingErrorView var0, String var1, boolean var2, String var3, int var4, int var5, Object var6) {
      if((var5 & 2) != 0) {
         var2 = var0.i;
      }

      if((var5 & 4) != 0) {
         var3 = var0.getRetryLabel();
      }

      if((var5 & 8) != 0) {
         var4 = var0.g;
      }

      var0.a(var1, var2, var3, var4);
   }

   private final void setLottieAnimationFileName(String var1) {
      this.m = var1;
      ((LottieAnimationView)this.b(co.uk.getmondo.c.a.loadingProgress)).setAnimation(this.m);
   }

   public final void a(String var1, boolean var2, String var3, int var4) {
      kotlin.d.b.l.b(var1, "message");
      kotlin.d.b.l.b(var3, "retryLabel");
      this.setMessage(var1);
      this.setRetryEnabled(var2);
      this.setRetryLabel(var3);
      this.setIconRes(var4);
   }

   public final void a(boolean var1, String var2) {
      this.setLoading(var1);
      this.setMessage(var2);
      ae.b((FrameLayout)this.b(co.uk.getmondo.c.a.iconLayout));
      ae.b((Button)this.b(co.uk.getmondo.c.a.retryButton));
   }

   public View b(int var1) {
      if(this.n == null) {
         this.n = new HashMap();
      }

      View var3 = (View)this.n.get(Integer.valueOf(var1));
      View var2 = var3;
      if(var3 == null) {
         var2 = this.findViewById(var1);
         this.n.put(Integer.valueOf(var1), var2);
      }

      return var2;
   }

   public final io.reactivex.n c() {
      io.reactivex.n var1 = io.reactivex.n.create((p)(new p() {
         public final void a(final o var1) {
            kotlin.d.b.l.b(var1, "emitter");
            LoadingErrorView.this.setRetryClickListener((kotlin.d.a.a)(new kotlin.d.a.a() {
               public final void b() {
                  var1.a(n.a);
               }

               // $FF: synthetic method
               public Object v_() {
                  this.b();
                  return n.a;
               }
            }));
            var1.a((io.reactivex.c.f)(new io.reactivex.c.f() {
               public final void a() {
                  LoadingErrorView.this.setRetryClickListener((kotlin.d.a.a)null);
               }
            }));
         }
      }));
      kotlin.d.b.l.a(var1, "Observable.create { emit…stener = null }\n        }");
      return var1;
   }

   public final int getIconRes() {
      return this.g;
   }

   public final boolean getLoading() {
      return this.f;
   }

   public final int getLoadingIconBackgroundColor() {
      return this.h;
   }

   public final String getMessage() {
      return ((TextView)this.b(co.uk.getmondo.c.a.messageText)).getText().toString();
   }

   public final int getMessageTextColor() {
      return this.j;
   }

   public final int getOverlayBackgroundColor() {
      return this.e;
   }

   public final boolean getOverlayEnabled() {
      return this.d;
   }

   public final kotlin.d.a.a getRetryClickListener() {
      return this.k;
   }

   public final boolean getRetryEnabled() {
      return this.i;
   }

   public final String getRetryLabel() {
      return ((Button)this.b(co.uk.getmondo.c.a.retryButton)).getText().toString();
   }

   public final void setIconRes(int var1) {
      this.g = var1;
      if(var1 != 0) {
         Drawable var2 = android.support.v4.content.a.a(this.getContext(), var1);
         if(this.l != 0) {
            android.support.v4.a.a.a.a(var2, android.support.v4.content.a.c(this.getContext(), this.l));
         }

         ((ImageView)this.b(co.uk.getmondo.c.a.iconImageView)).setImageDrawable(var2);
         ae.a((View)((FrameLayout)this.b(co.uk.getmondo.c.a.iconLayout)));
      } else {
         ae.b((FrameLayout)this.b(co.uk.getmondo.c.a.iconLayout));
      }

   }

   public final void setLoading(boolean var1) {
      this.f = var1;
      if(var1) {
         ((LottieAnimationView)this.b(co.uk.getmondo.c.a.loadingProgress)).c();
         ae.a((View)((FrameLayout)this.b(co.uk.getmondo.c.a.progressLayout)));
      } else {
         ae.b((FrameLayout)this.b(co.uk.getmondo.c.a.progressLayout));
         ((LottieAnimationView)this.b(co.uk.getmondo.c.a.loadingProgress)).d();
      }

   }

   public final void setLoadingIconBackgroundColor(int var1) {
      this.h = var1;
      switch(var1) {
      default:
         final ShapeDrawable var2 = this.c;
         var2.getPaint().setColor(android.support.v4.content.a.c(this.getContext(), var1));
         this.post((Runnable)(new Runnable() {
            public final void run() {
               ((ImageView)LoadingErrorView.this.b(co.uk.getmondo.c.a.progressBackgroundView)).setImageDrawable((Drawable)var2);
               ((ImageView)LoadingErrorView.this.b(co.uk.getmondo.c.a.iconBackgroundView)).setImageDrawable((Drawable)var2);
            }
         }));
      case 0:
      }
   }

   public final void setMessage(String var1) {
      ((TextView)this.b(co.uk.getmondo.c.a.messageText)).setText((CharSequence)var1);
      if(var1 == null) {
         ae.b((TextView)this.b(co.uk.getmondo.c.a.messageText));
      } else {
         ae.a((View)((TextView)this.b(co.uk.getmondo.c.a.messageText)));
      }

   }

   public final void setMessageTextColor(int var1) {
      this.j = var1;
      ((TextView)this.b(co.uk.getmondo.c.a.messageText)).setTextColor(android.support.v4.content.a.c(this.getContext(), var1));
   }

   public final void setOverlayBackgroundColor(int var1) {
      this.e = var1;
      this.b(co.uk.getmondo.c.a.loadingOverlay).setBackgroundResource(var1);
   }

   public final void setOverlayEnabled(boolean var1) {
      this.d = var1;
      if(var1) {
         ae.a(this.b(co.uk.getmondo.c.a.loadingOverlay));
         ((TextView)this.b(co.uk.getmondo.c.a.messageText)).setTextSize(2, 16.0F);
      } else {
         ae.b(this.b(co.uk.getmondo.c.a.loadingOverlay));
         ((TextView)this.b(co.uk.getmondo.c.a.messageText)).setTextSize(2, 14.0F);
      }

   }

   public final void setRetryClickListener(kotlin.d.a.a var1) {
      this.k = var1;
   }

   public final void setRetryEnabled(boolean var1) {
      this.i = var1;
      if(var1) {
         ae.a((View)((Button)this.b(co.uk.getmondo.c.a.retryButton)));
      } else {
         ae.b((Button)this.b(co.uk.getmondo.c.a.retryButton));
      }

   }

   public final void setRetryLabel(String var1) {
      kotlin.d.b.l.b(var1, "value");
      ((Button)this.b(co.uk.getmondo.c.a.retryButton)).setText((CharSequence)var1);
   }
}
