package co.uk.getmondo.common.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class InfoTextActivity extends co.uk.getmondo.common.activities.b {
   @BindView(2131820811)
   ViewGroup mainContentGroup;

   private void a() {
      for(int var1 = 0; var1 < this.mainContentGroup.getChildCount(); ++var1) {
         if(this.mainContentGroup.getChildAt(var1) instanceof TextView) {
            ((TextView)this.mainContentGroup.getChildAt(var1)).setMovementMethod(LinkMovementMethod.getInstance());
         }
      }

   }

   public static void a(Context var0, String var1, int var2) {
      Intent var3 = new Intent(var0, InfoTextActivity.class);
      var3.putExtra("EXTRA_TITLE", var1);
      var3.putExtra("EXTRA_LAYOUT_ID", var2);
      var0.startActivity(var3);
   }

   protected void onCreate(Bundle var1) {
      super.onCreate(var1);
      int var2 = this.getIntent().getIntExtra("EXTRA_LAYOUT_ID", 2131034217);
      String var3 = this.getIntent().getStringExtra("EXTRA_TITLE");
      this.setContentView(var2);
      ButterKnife.bind((Activity)this);
      this.setTitle(var3);
      this.getSupportActionBar().b(true);
      this.a();
   }
}
