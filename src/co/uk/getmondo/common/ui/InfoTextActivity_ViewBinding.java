package co.uk.getmondo.common.ui;

import android.view.View;
import android.view.ViewGroup;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class InfoTextActivity_ViewBinding implements Unbinder {
   private InfoTextActivity a;

   public InfoTextActivity_ViewBinding(InfoTextActivity var1, View var2) {
      this.a = var1;
      var1.mainContentGroup = (ViewGroup)Utils.findRequiredViewAsType(var2, 2131820811, "field 'mainContentGroup'", ViewGroup.class);
   }

   public void unbind() {
      InfoTextActivity var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.mainContentGroup = null;
      }
   }
}
