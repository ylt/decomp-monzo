package co.uk.getmondo.common.ui;

import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class AmountInputView_ViewBinding implements Unbinder {
   private AmountInputView a;

   public AmountInputView_ViewBinding(AmountInputView var1, View var2) {
      this.a = var1;
      var1.editText = (EditText)Utils.findRequiredViewAsType(var2, 2131821730, "field 'editText'", EditText.class);
      var1.textInputLayout = (TextInputLayout)Utils.findRequiredViewAsType(var2, 2131821729, "field 'textInputLayout'", TextInputLayout.class);
      var1.titleTextView = (TextView)Utils.findRequiredViewAsType(var2, 2131821728, "field 'titleTextView'", TextView.class);
   }

   public void unbind() {
      AmountInputView var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.editText = null;
         var1.textInputLayout = null;
         var1.titleTextView = null;
      }
   }
}
