package co.uk.getmondo.common.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.at;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.TypeCastException;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001:\u0001\u0012B%\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u0012\u0010\u000b\u001a\u00020\f2\b\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0014J\b\u0010\u000f\u001a\u00020\u0010H\u0014J\u0010\u0010\u0011\u001a\u00020\u00102\u0006\u0010\u0004\u001a\u00020\u0005H\u0016J\u0012\u0010\u0011\u001a\u00020\u00102\b\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0014R\u000e\u0010\t\u001a\u00020\u0007X\u0082D¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0007X\u0082D¢\u0006\u0002\n\u0000¨\u0006\u0013"},
   d2 = {"Lco/uk/getmondo/common/ui/CopyableLinearLayout;", "Landroid/support/v7/widget/LinearLayoutCompat;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyle", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "copyAsContent", "copyAsLabel", "checkLayoutParams", "", "p", "Landroid/view/ViewGroup$LayoutParams;", "generateDefaultLayoutParams", "Landroid/support/v7/widget/LinearLayoutCompat$LayoutParams;", "generateLayoutParams", "LayoutParams", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class CopyableLinearLayout extends at {
   private final int a;
   private final int b;

   public CopyableLinearLayout(Context var1) {
      this(var1, (AttributeSet)null, 0, 6, (kotlin.d.b.i)null);
   }

   public CopyableLinearLayout(Context var1, AttributeSet var2) {
      this(var1, var2, 0, 4, (kotlin.d.b.i)null);
   }

   public CopyableLinearLayout(final Context var1, AttributeSet var2, int var3) {
      kotlin.d.b.l.b(var1, "context");
      super(var1, var2, var3);
      this.a = 1;
      this.b = 2;
      TypedArray var5 = var1.obtainStyledAttributes(var2, co.uk.getmondo.c.b.CopyableLinearLayout);
      final kotlin.d.b.x.c var4 = new kotlin.d.b.x.c();
      var4.a = var5.getString(0);
      if((String)var4.a == null) {
         var4.a = " ";
      }

      var5.recycle();
      this.setOnLongClickListener((OnLongClickListener)(new OnLongClickListener() {
         public final boolean onLongClick(View var1x) {
            Iterable var3 = (Iterable)m.a(CopyableLinearLayout.this);
            Collection var7 = (Collection)(new ArrayList());
            Iterator var9 = var3.iterator();

            boolean var2;
            Object var4x;
            while(var9.hasNext()) {
               label44: {
                  var4x = var9.next();
                  View var5 = (View)var4x;
                  if(var5 instanceof TextView) {
                     LayoutParams var14 = ((TextView)var5).getLayoutParams();
                     if(var14 == null) {
                        throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.common.ui.CopyableLinearLayout.LayoutParams");
                     }

                     if(((CopyableLinearLayout.a)var14).a() == CopyableLinearLayout.this.a) {
                        var2 = true;
                        break label44;
                     }
                  }

                  var2 = false;
               }

               if(var2) {
                  var7.add(var4x);
               }
            }

            var3 = (Iterable)((List)var7);
            String var8 = (String)var4.a;
            kotlin.d.b.l.a(var8, "separator");
            var8 = kotlin.a.m.a(var3, (CharSequence)var8, (CharSequence)null, (CharSequence)null, 0, (CharSequence)null, (kotlin.d.a.b)null.a, 30, (Object)null);
            Iterable var11 = (Iterable)m.a(CopyableLinearLayout.this);
            Collection var10 = (Collection)(new ArrayList());
            Iterator var15 = var11.iterator();

            while(var15.hasNext()) {
               label34: {
                  var4x = var15.next();
                  View var6 = (View)var4x;
                  if(var6 instanceof TextView) {
                     LayoutParams var16 = ((TextView)var6).getLayoutParams();
                     if(var16 == null) {
                        throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.common.ui.CopyableLinearLayout.LayoutParams");
                     }

                     if(((CopyableLinearLayout.a)var16).a() == CopyableLinearLayout.this.b) {
                        var2 = true;
                        break label34;
                     }
                  }

                  var2 = false;
               }

               if(var2) {
                  var10.add(var4x);
               }
            }

            var3 = (Iterable)((List)var10);
            String var13 = (String)var4.a;
            kotlin.d.b.l.a(var13, "separator");
            String var12 = kotlin.a.m.a(var3, (CharSequence)var13, (CharSequence)null, (CharSequence)null, 0, (CharSequence)null, (kotlin.d.a.b)null.a, 30, (Object)null);
            co.uk.getmondo.common.e.a(var1, var8, var12);
            return true;
         }
      }));
   }

   // $FF: synthetic method
   public CopyableLinearLayout(Context var1, AttributeSet var2, int var3, int var4, kotlin.d.b.i var5) {
      if((var4 & 2) != 0) {
         var2 = (AttributeSet)null;
      }

      if((var4 & 4) != 0) {
         var3 = 0;
      }

      this(var1, var2, var3);
   }

   public android.support.v7.widget.at.a b(AttributeSet var1) {
      kotlin.d.b.l.b(var1, "attrs");
      Context var2 = this.getContext();
      kotlin.d.b.l.a(var2, "context");
      return (android.support.v7.widget.at.a)(new CopyableLinearLayout.a(var2, var1));
   }

   protected android.support.v7.widget.at.a b(LayoutParams var1) {
      return new android.support.v7.widget.at.a(var1);
   }

   protected boolean checkLayoutParams(LayoutParams var1) {
      return var1 instanceof CopyableLinearLayout.a;
   }

   // $FF: synthetic method
   public LayoutParams generateDefaultLayoutParams() {
      return (LayoutParams)this.j();
   }

   // $FF: synthetic method
   public LayoutParams generateLayoutParams(AttributeSet var1) {
      return (LayoutParams)this.b(var1);
   }

   // $FF: synthetic method
   public LayoutParams generateLayoutParams(LayoutParams var1) {
      return (LayoutParams)this.b(var1);
   }

   protected android.support.v7.widget.at.a j() {
      return new android.support.v7.widget.at.a(this.getContext(), (AttributeSet)null);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0017\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006B\u000f\b\u0016\u0012\u0006\u0010\u0007\u001a\u00020\b¢\u0006\u0002\u0010\tR\u001a\u0010\n\u001a\u00020\u000bX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\r\"\u0004\b\u000e\u0010\u000f¨\u0006\u0010"},
      d2 = {"Lco/uk/getmondo/common/ui/CopyableLinearLayout$LayoutParams;", "Landroid/support/v7/widget/LinearLayoutCompat$LayoutParams;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "p", "Landroid/view/ViewGroup$LayoutParams;", "(Landroid/view/ViewGroup$LayoutParams;)V", "copyAs", "", "getCopyAs", "()I", "setCopyAs", "(I)V", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a extends android.support.v7.widget.at.a {
      private int a;

      public a(Context var1, AttributeSet var2) {
         kotlin.d.b.l.b(var1, "context");
         kotlin.d.b.l.b(var2, "attrs");
         super(var1, var2);
         TypedArray var3 = var1.obtainStyledAttributes(var2, co.uk.getmondo.c.b.CopyableLinearLayout_layout);
         this.a = var3.getInteger(0, 0);
         var3.recycle();
      }

      public final int a() {
         return this.a;
      }
   }
}
