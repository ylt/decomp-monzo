package co.uk.getmondo.common.ui;

import android.view.View;
import android.widget.ImageView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;

public class VideoImageView_ViewBinding implements Unbinder {
   private VideoImageView a;

   public VideoImageView_ViewBinding(VideoImageView var1, View var2) {
      this.a = var1;
      var1.exoPlayerView = (SimpleExoPlayerView)Utils.findRequiredViewAsType(var2, 2131821711, "field 'exoPlayerView'", SimpleExoPlayerView.class);
      var1.placeholderImageView = (ImageView)Utils.findRequiredViewAsType(var2, 2131821712, "field 'placeholderImageView'", ImageView.class);
   }

   public void unbind() {
      VideoImageView var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.exoPlayerView = null;
         var1.placeholderImageView = null;
      }
   }
}
