package co.uk.getmondo.common.ui;

import android.graphics.Rect;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.t;
import android.view.View;
import kotlin.Metadata;
import kotlin.TypeCastException;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J(\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0011"},
   d2 = {"Lco/uk/getmondo/common/ui/GridSpacingItemDecoration;", "Landroid/support/v7/widget/RecyclerView$ItemDecoration;", "spacing", "", "includeEdge", "", "(IZ)V", "getItemOffsets", "", "outRect", "Landroid/graphics/Rect;", "view", "Landroid/view/View;", "parent", "Landroid/support/v7/widget/RecyclerView;", "state", "Landroid/support/v7/widget/RecyclerView$State;", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class d extends android.support.v7.widget.RecyclerView.g {
   private final int a;
   private final boolean b;

   public d(int var1, boolean var2) {
      this.a = var1;
      this.b = var2;
   }

   // $FF: synthetic method
   public d(int var1, boolean var2, int var3, kotlin.d.b.i var4) {
      if((var3 & 2) != 0) {
         var2 = false;
      }

      this(var1, var2);
   }

   public void getItemOffsets(Rect var1, View var2, RecyclerView var3, t var4) {
      kotlin.d.b.l.b(var1, "outRect");
      kotlin.d.b.l.b(var2, "view");
      kotlin.d.b.l.b(var3, "parent");
      kotlin.d.b.l.b(var4, "state");
      int var5 = var3.f(var2);
      android.support.v7.widget.RecyclerView.h var9 = var3.getLayoutManager();
      if(var9 == null) {
         throw new TypeCastException("null cannot be cast to non-null type android.support.v7.widget.GridLayoutManager");
      } else {
         GridLayoutManager var10 = (GridLayoutManager)var9;
         int var7 = var10.c();
         int var8 = var10.b().a(var5);
         if(var5 >= 0) {
            int var6 = var5 % var7;
            if(this.b) {
               var1.left = this.a - this.a * var6 / var7;
               var1.right = (var6 + 1) * this.a / var7;
               if(var5 < var7) {
                  var1.top = this.a;
               }

               var1.bottom = this.a;
            } else {
               var1.left = this.a * var6 / var7;
               if(var8 < var7) {
                  var1.right = this.a - (var6 + 1) * this.a / var7;
               }

               if(var5 >= var7) {
                  var1.top = this.a;
               }
            }
         } else {
            var1.left = 0;
            var1.right = 0;
            var1.top = 0;
            var1.bottom = 0;
         }

      }
   }
}
