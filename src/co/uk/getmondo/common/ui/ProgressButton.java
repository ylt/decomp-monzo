package co.uk.getmondo.common.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Drawable.Callback;
import android.util.AttributeSet;
import kotlin.Metadata;
import kotlin.TypeCastException;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\r\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\r\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B%\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u0010\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0017H\u0014J0\u0010\u0018\u001a\u00020\u00152\u0006\u0010\u0019\u001a\u00020\n2\u0006\u0010\u001a\u001a\u00020\u00072\u0006\u0010\u001b\u001a\u00020\u00072\u0006\u0010\u001c\u001a\u00020\u00072\u0006\u0010\u001d\u001a\u00020\u0007H\u0014J(\u0010\u001e\u001a\u00020\u00152\u0006\u0010\u001f\u001a\u00020\u00072\u0006\u0010 \u001a\u00020\u00072\u0006\u0010!\u001a\u00020\u00072\u0006\u0010\"\u001a\u00020\u0007H\u0014J\u0012\u0010#\u001a\u00020\n2\b\u0010$\u001a\u0004\u0018\u00010%H\u0014R$\u0010\u000b\u001a\u00020\n2\u0006\u0010\t\u001a\u00020\n@FX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\nX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u000e¢\u0006\u0002\n\u0000¨\u0006&"},
   d2 = {"Lco/uk/getmondo/common/ui/ProgressButton;", "Landroid/support/v7/widget/AppCompatButton;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyle", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "value", "", "isLoading", "()Z", "setLoading", "(Z)V", "progressDrawable", "Landroid/graphics/drawable/AnimatedVectorDrawable;", "progressDrawableBoundsChanged", "textCopy", "", "onDraw", "", "canvas", "Landroid/graphics/Canvas;", "onLayout", "changed", "left", "top", "right", "bottom", "onSizeChanged", "w", "h", "oldw", "oldh", "verifyDrawable", "who", "Landroid/graphics/drawable/Drawable;", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ProgressButton extends android.support.v7.widget.i {
   private final AnimatedVectorDrawable a;
   private CharSequence b;
   private boolean c;
   private boolean d;

   public ProgressButton(Context var1) {
      this(var1, (AttributeSet)null, 0, 6, (kotlin.d.b.i)null);
   }

   public ProgressButton(Context var1, AttributeSet var2) {
      this(var1, var2, 0, 4, (kotlin.d.b.i)null);
   }

   public ProgressButton(Context var1, AttributeSet var2, int var3) {
      kotlin.d.b.l.b(var1, "context");
      super(var1, var2, var3);
      Drawable var4 = var1.getDrawable(2130837592);
      if(var4 == null) {
         throw new TypeCastException("null cannot be cast to non-null type android.graphics.drawable.AnimatedVectorDrawable");
      } else {
         this.a = (AnimatedVectorDrawable)var4;
         this.b = (CharSequence)"";
         this.a.setCallback((Callback)this);
      }
   }

   // $FF: synthetic method
   public ProgressButton(Context var1, AttributeSet var2, int var3, int var4, kotlin.d.b.i var5) {
      if((var4 & 2) != 0) {
         var2 = (AttributeSet)null;
      }

      if((var4 & 4) != 0) {
         var3 = 2130772208;
      }

      this(var1, var2, var3);
   }

   protected void onDraw(Canvas var1) {
      kotlin.d.b.l.b(var1, "canvas");
      super.onDraw(var1);
      if(this.d) {
         if(this.c) {
            this.c = false;
            int var3 = (int)((double)Math.min(this.getWidth(), this.getHeight()) * 0.7D);
            int var2 = this.getWidth() / 2 - var3 / 2;
            int var4 = this.getHeight() / 2 - var3 / 2;
            this.a.setBounds(var2, var4, var2 + var3, var3 + var4);
         }

         this.a.setTint(this.getCurrentTextColor());
         this.a.draw(var1);
      }

   }

   protected void onLayout(boolean var1, int var2, int var3, int var4, int var5) {
      super.onLayout(var1, var2, var3, var4, var5);
      this.c |= var1;
   }

   protected void onSizeChanged(int var1, int var2, int var3, int var4) {
      super.onSizeChanged(var1, var2, var3, var4);
      this.c = true;
   }

   public final void setLoading(boolean var1) {
      if(this.d != var1) {
         this.d = var1;
         if(var1) {
            this.setClickable(false);
            CharSequence var2 = this.getText();
            kotlin.d.b.l.a(var2, "text");
            this.b = var2;
            this.setText((CharSequence)"");
            this.a.start();
         } else {
            this.setClickable(true);
            this.setText(this.b);
            this.b = (CharSequence)"";
            this.a.stop();
         }

         this.invalidate();
      }

   }

   protected boolean verifyDrawable(Drawable var1) {
      boolean var2;
      if(!super.verifyDrawable(var1) && var1 != this.a) {
         var2 = false;
      } else {
         var2 = true;
      }

      return var2;
   }
}
