package co.uk.getmondo.common.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class SecureButton extends android.support.v7.widget.i {
   public SecureButton(Context var1) {
      super(var1);
      this.setFilterTouchesWhenObscured(true);
   }

   public SecureButton(Context var1, AttributeSet var2) {
      super(var1, var2);
      this.setFilterTouchesWhenObscured(true);
   }

   public SecureButton(Context var1, AttributeSet var2, int var3) {
      super(var1, var2, var3);
      this.setFilterTouchesWhenObscured(true);
   }

   public boolean onFilterTouchEventForSecurity(MotionEvent var1) {
      boolean var2;
      if(!super.onFilterTouchEventForSecurity(var1)) {
         var2 = true;
      } else {
         var2 = false;
      }

      if(var2 && this.getContext() instanceof android.support.v4.app.j) {
         co.uk.getmondo.common.d.a.a(this.getContext().getString(2131362625), this.getContext().getString(2131362624), false).show(((android.support.v4.app.j)this.getContext()).getSupportFragmentManager(), "MESSAGE_FRAGMENT_TAG");
      }

      boolean var3;
      if(!var2) {
         var3 = true;
      } else {
         var3 = false;
      }

      return var3;
   }

   public void setFilterTouchesWhenObscured(boolean var1) {
      super.setFilterTouchesWhenObscured(true);
   }
}
