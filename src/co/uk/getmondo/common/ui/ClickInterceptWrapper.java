package co.uk.getmondo.common.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;

public class ClickInterceptWrapper extends FrameLayout {
   private OnClickListener a;
   private GestureDetector b = new GestureDetector(this.getContext(), new SimpleOnGestureListener() {
      public boolean onDoubleTap(MotionEvent var1) {
         return false;
      }

      public boolean onSingleTapConfirmed(MotionEvent var1) {
         if(ClickInterceptWrapper.this.a != null) {
            ClickInterceptWrapper.this.a.onClick(ClickInterceptWrapper.this);
         }

         return false;
      }
   });

   public ClickInterceptWrapper(Context var1) {
      super(var1);
   }

   public ClickInterceptWrapper(Context var1, AttributeSet var2) {
      super(var1, var2);
   }

   public ClickInterceptWrapper(Context var1, AttributeSet var2, int var3) {
      super(var1, var2, var3);
   }

   public boolean onInterceptTouchEvent(MotionEvent var1) {
      this.b.onTouchEvent(var1);
      return super.onInterceptTouchEvent(var1);
   }

   public void setOnClickListener(OnClickListener var1) {
      this.a = var1;
   }
}
