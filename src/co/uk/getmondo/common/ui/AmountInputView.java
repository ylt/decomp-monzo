package co.uk.getmondo.common.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.InputFilter;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.uk.getmondo.common.k.p;
import io.reactivex.n;
import java.util.Locale;
import java.util.regex.Pattern;

public class AmountInputView extends LinearLayout {
   private final co.uk.getmondo.common.i.b a = new co.uk.getmondo.common.i.b(false, false, true);
   private co.uk.getmondo.common.i.c b;
   private int c;
   private co.uk.getmondo.d.c d;
   @BindView(2131821730)
   EditText editText;
   @BindView(2131821729)
   TextInputLayout textInputLayout;
   @BindView(2131821728)
   TextView titleTextView;

   public AmountInputView(Context var1) {
      super(var1);
      this.b = co.uk.getmondo.common.i.c.a;
      this.c = 6;
      this.a((AttributeSet)null);
   }

   public AmountInputView(Context var1, AttributeSet var2) {
      super(var1, var2);
      this.b = co.uk.getmondo.common.i.c.a;
      this.c = 6;
      this.a(var2);
   }

   public AmountInputView(Context var1, AttributeSet var2, int var3) {
      super(var1, var2, var3);
      this.b = co.uk.getmondo.common.i.c.a;
      this.c = 6;
      this.a(var2);
   }

   private void a(AttributeSet var1) {
      boolean var2 = false;
      this.setOrientation(1);
      ButterKnife.bind(this, (View)LayoutInflater.from(this.getContext()).inflate(2131034429, this, true));
      if(var1 != null) {
         TypedArray var7 = this.getContext().getTheme().obtainStyledAttributes(var1, co.uk.getmondo.c.b.AmountInputView, 0, 0);

         try {
            String var3 = var7.getString(0);
            if(p.c(var3)) {
               co.uk.getmondo.common.i.c var4 = new co.uk.getmondo.common.i.c(var3);
               this.b = var4;
            }

            this.c = var7.getInteger(1, 6);
            var2 = var7.getBoolean(2, false);
         } finally {
            var7.recycle();
         }
      }

      this.textInputLayout.setErrorEnabled(var2);
      this.b();
      this.c();
      this.d();
   }

   private void b() {
      if(this.editText != null) {
         this.editText.setFilters(new InputFilter[]{new AmountInputView.a(this.c, this.b.a())});
      }

   }

   private void c() {
      if(this.titleTextView != null) {
         this.titleTextView.setText(this.getContext().getString(2131362010, new Object[]{this.b.a(Locale.ENGLISH)}));
      }

   }

   private void d() {
      if(this.editText != null && this.d != null) {
         this.editText.setText(this.a.a(this.d));
      }

   }

   public n a() {
      return com.b.a.d.e.c(this.editText);
   }

   public Editable getAmountText() {
      return this.editText.getText();
   }

   public void setCurrency(co.uk.getmondo.common.i.c var1) {
      if(!this.b.equals(var1)) {
         this.b = var1;
         this.b();
         this.c();
      }

   }

   public void setDefaultAmount(co.uk.getmondo.d.c var1) {
      this.d = var1;
      this.d();
      if(this.b != var1.l()) {
         this.b = var1.l();
         this.c();
      }

   }

   public void setError(CharSequence var1) {
      this.textInputLayout.setError(var1);
   }

   private static class a implements InputFilter {
      private Pattern a;
      private final int b;
      private final int c;

      a(int var1, int var2) {
         String var3;
         if(var2 > 0) {
            var3 = "[0-9.]*";
         } else {
            var3 = "[0-9]*";
         }

         this.a = Pattern.compile(var3);
         this.c = var1;
         this.b = var2;
      }

      public CharSequence filter(CharSequence var1, int var2, int var3, Spanned var4, int var5, int var6) {
         var1 = var1.subSequence(var2, var3);
         String var7;
         if(!this.a.matcher(var1).matches()) {
            var7 = "";
         } else {
            SpannableStringBuilder var8 = new SpannableStringBuilder(var4);
            var8.replace(var5, var6, var1);
            var7 = var8.toString();
            String[] var9 = var7.split("\\.");
            if(var9.length > 2) {
               var7 = "";
            } else {
               if(var9.length > 0) {
                  var7 = var9[0];
               }

               String var10;
               if(var9.length > 1) {
                  var10 = var9[1];
               } else {
                  var10 = "";
               }

               if(var7.length() <= this.c && var10.length() <= this.b) {
                  var7 = null;
               } else {
                  var7 = "";
               }
            }
         }

         return var7;
      }
   }
}
