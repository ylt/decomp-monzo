package co.uk.getmondo.common.ui;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.a.aa;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u001e\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\u001a\u001e\u0010\u0006\u001a\u00020\u0002*\u00020\u00032\b\b\u0001\u0010\u0007\u001a\u00020\b2\b\b\u0002\u0010\t\u001a\u00020\n\"\u001b\u0010\u0000\u001a\b\u0012\u0004\u0012\u00020\u00020\u0001*\u00020\u00038F¢\u0006\u0006\u001a\u0004\b\u0004\u0010\u0005¨\u0006\u000b"},
   d2 = {"children", "", "Landroid/view/View;", "Landroid/view/ViewGroup;", "getChildren", "(Landroid/view/ViewGroup;)Ljava/util/List;", "inflate", "layoutRes", "", "attachToRoot", "", "app_monzoPrepaidRelease"},
   k = 2,
   mv = {1, 1, 7}
)
public final class m {
   public static final View a(ViewGroup var0, int var1, boolean var2) {
      kotlin.d.b.l.b(var0, "$receiver");
      View var3 = LayoutInflater.from(var0.getContext()).inflate(var1, var0, var2);
      kotlin.d.b.l.a(var3, "LayoutInflater.from(cont…tRes, this, attachToRoot)");
      return var3;
   }

   public static final List a(ViewGroup var0) {
      kotlin.d.b.l.b(var0, "$receiver");
      Iterable var2 = (Iterable)kotlin.f.d.b(0, var0.getChildCount());
      Collection var1 = (Collection)(new ArrayList(kotlin.a.m.a(var2, 10)));
      Iterator var3 = var2.iterator();

      while(var3.hasNext()) {
         var1.add(var0.getChildAt(((aa)var3).b()));
      }

      return (List)var1;
   }
}
