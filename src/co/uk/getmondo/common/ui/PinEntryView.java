package co.uk.getmondo.common.ui;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.support.v7.widget.n;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.text.InputFilter.LengthFilter;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;

public class PinEntryView extends n {
   private final Paint a;
   private final Paint b;
   private final Paint c;
   private final Paint d;
   private final Paint e;
   private final int f;
   private final int g;
   private final int h;
   private final int i;
   private final int j;
   private PinEntryView.a k;
   private int l;
   private boolean m;
   private boolean n;

   public PinEntryView(Context var1) {
      this(var1, (AttributeSet)null, 2130772212);
   }

   public PinEntryView(Context var1, AttributeSet var2) {
      this(var1, var2, 2130772212);
   }

   public PinEntryView(Context var1, AttributeSet var2, int var3) {
      super(var1, var2, var3);
      this.a = new Paint(1);
      this.b = new Paint(1);
      this.c = new Paint(1);
      this.d = new Paint(1);
      this.e = new Paint(1);
      this.l = 0;
      this.n = false;
      DisplayMetrics var5 = this.getResources().getDisplayMetrics();
      int var4 = android.support.v4.content.a.c(var1, 2131689582);
      TypedArray var8 = var1.getTheme().obtainStyledAttributes(var2, co.uk.getmondo.c.b.PinEntryView, 0, 0);

      try {
         var3 = var8.getColor(1, var4);
         this.n = var8.getBoolean(0, false);
      } finally {
         var8.recycle();
      }

      this.a.setColor(var3);
      this.i = (int)TypedValue.applyDimension(2, 6.0F, var5);
      TypedValue var9 = new TypedValue();
      var1.getTheme().resolveAttribute(16843829, var9, true);
      if(var9.resourceId > 0) {
         var3 = android.support.v4.content.a.c(var1, var9.resourceId);
      } else {
         var3 = var9.data;
      }

      this.c.setColor(var4);
      this.c.setStrokeWidth(TypedValue.applyDimension(2, 2.0F, var5));
      this.d.setColor(var3);
      this.d.setStrokeWidth(this.c.getStrokeWidth());
      this.e.setColor(android.support.v4.content.a.c(var1, 2131689680));
      this.e.setStrokeWidth(this.c.getStrokeWidth());
      this.b.setColor(-1);
      this.b.setStyle(Style.FILL);
      this.b.setTextSize(48.0F);
      this.b.setTypeface(android.support.v4.content.a.b.a(var1, 2130968576));
      this.j = this.getResources().getDimensionPixelOffset(2131427603);
      this.h = this.getResources().getDimensionPixelSize(2131427605);
      this.f = this.getResources().getDimensionPixelSize(2131427368);
      this.g = this.getResources().getDimensionPixelSize(2131427367);
      this.setBackgroundColor(android.support.v4.content.a.c(this.getContext(), 17170445));
      this.setTextColor(android.support.v4.content.a.c(this.getContext(), 17170445));
      this.setCursorVisible(false);
      this.setFilters(new InputFilter[]{new LengthFilter(4)});
      this.setInputType(18);
      this.setImeOptions(268435456);
      this.setOnFocusChangeListener(g.a(this));
      this.addTextChangedListener(new TextWatcher() {
         public void afterTextChanged(Editable var1) {
            if(PinEntryView.this.isEnabled()) {
               PinEntryView.this.l = var1.length();
               if(PinEntryView.this.l == 4 && PinEntryView.this.k != null) {
                  PinEntryView.this.k.a(var1.toString());
               }

               PinEntryView.this.invalidate();
            }

         }

         public void beforeTextChanged(CharSequence var1, int var2, int var3, int var4) {
         }

         public void onTextChanged(CharSequence var1, int var2, int var3, int var4) {
         }
      });
   }

   // $FF: synthetic method
   static void a(PinEntryView var0, View var1, boolean var2) {
      var0.setSelection(var0.getText().length());
   }

   public void a() {
      this.l = 0;
      this.setText("");
   }

   public void a(boolean var1) {
      this.l = -1;
      this.m = true;
      this.invalidate();
      if(var1) {
         ObjectAnimator.ofFloat(this, "translationX", new float[]{0.0F, 25.0F, -25.0F, 25.0F, -25.0F, 15.0F, -15.0F, 6.0F, -6.0F, 0.0F}).setDuration(300L).start();
      }

   }

   public void b() {
      this.setEnabled(false);
   }

   public void c() {
      this.setEnabled(true);
   }

   public void d() {
      this.l = this.getText().length();
      this.m = false;
      this.invalidate();
   }

   protected void dispatchDraw(Canvas var1) {
      super.dispatchDraw(var1);

      int var3;
      int var4;
      for(var3 = 0; var3 < this.l; ++var3) {
         var4 = this.f * var3 + this.h * var3 + this.i / 2 + this.f / 2;
         int var5 = this.g / 2 + this.i / 2;
         var1.drawCircle((float)var4, (float)var5, (float)this.i, this.a);
         if(this.n) {
            String var6 = this.getText().charAt(var3) + "";
            float var2 = this.b.measureText(var6);
            var1.drawText(var6, (float)var4 - var2 / 2.0F, (float)(var5 - this.j), this.b);
         }
      }

      for(var3 = 0; var3 < 4; ++var3) {
         var4 = this.f * var3 + this.h * var3;
         Paint var7;
         if(var3 == this.l) {
            var7 = this.d;
         } else if(this.m) {
            var7 = this.e;
         } else {
            var7 = this.c;
         }

         var1.drawLine((float)var4, (float)this.g, (float)(var4 + this.f), (float)this.g, var7);
      }

   }

   protected void onMeasure(int var1, int var2) {
      this.setMeasuredDimension(this.f * 4 + this.h * 3 + this.getPaddingLeft() + this.getPaddingRight(), this.g + this.getPaddingTop() + this.getPaddingBottom());
   }

   public void setOnPinEnteredListener(PinEntryView.a var1) {
      this.k = var1;
   }

   public interface a {
      void a(String var1);
   }
}
