package co.uk.getmondo.common.c;

import com.squareup.moshi.g;
import com.squareup.moshi.x;
import kotlin.Metadata;
import kotlin.d.b.l;
import kotlin.h.j;
import org.threeten.bp.LocalDate;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.ZonedDateTime;
import org.threeten.bp.format.DateTimeFormatter;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007J\u0012\u0010\u0007\u001a\u0004\u0018\u00010\b2\u0006\u0010\u0005\u001a\u00020\u0006H\u0007J\u0010\u0010\t\u001a\u00020\u00062\u0006\u0010\n\u001a\u00020\bH\u0007J\u0010\u0010\u000b\u001a\u00020\u00062\u0006\u0010\f\u001a\u00020\u0004H\u0007J\u0012\u0010\r\u001a\u0004\u0018\u00010\u000e2\u0006\u0010\u0005\u001a\u00020\u0006H\u0007J\u0010\u0010\u000f\u001a\u00020\u00062\u0006\u0010\u0010\u001a\u00020\u000eH\u0007¨\u0006\u0011"},
   d2 = {"Lco/uk/getmondo/common/date/ThreeTenMoshiAdapter;", "", "()V", "localDateFromJson", "Lorg/threeten/bp/LocalDate;", "date", "", "localDateTimeFromJson", "Lorg/threeten/bp/LocalDateTime;", "localDateTimeToJason", "localDateTime", "localDateToJason", "localDate", "zonedDateTimeFromJson", "Lorg/threeten/bp/ZonedDateTime;", "zonedDateTimeToJason", "zonedDateTime", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class b {
   @g
   public final LocalDate localDateFromJson(String var1) {
      l.b(var1, "date");
      boolean var2;
      if(!j.a((CharSequence)var1)) {
         var2 = true;
      } else {
         var2 = false;
      }

      LocalDate var3;
      if(var2) {
         var3 = LocalDate.a((CharSequence)var1);
      } else {
         var3 = null;
      }

      return var3;
   }

   @g
   public final LocalDateTime localDateTimeFromJson(String var1) {
      l.b(var1, "date");
      boolean var2;
      if(!j.a((CharSequence)var1)) {
         var2 = true;
      } else {
         var2 = false;
      }

      LocalDateTime var3;
      if(var2) {
         var3 = ZonedDateTime.a((CharSequence)var1).d();
      } else {
         var3 = null;
      }

      return var3;
   }

   @x
   public final String localDateTimeToJason(LocalDateTime var1) {
      l.b(var1, "localDateTime");
      String var2 = var1.a(DateTimeFormatter.g);
      l.a(var2, "localDateTime.format(Dat…tter.ISO_LOCAL_DATE_TIME)");
      return var2;
   }

   @x
   public final String localDateToJason(LocalDate var1) {
      l.b(var1, "localDate");
      String var2 = var1.a(DateTimeFormatter.a);
      l.a(var2, "localDate.format(DateTimeFormatter.ISO_LOCAL_DATE)");
      return var2;
   }

   @g
   public final ZonedDateTime zonedDateTimeFromJson(String var1) {
      l.b(var1, "date");
      boolean var2;
      if(!j.a((CharSequence)var1)) {
         var2 = true;
      } else {
         var2 = false;
      }

      ZonedDateTime var3;
      if(var2) {
         var3 = ZonedDateTime.a((CharSequence)var1);
      } else {
         var3 = null;
      }

      return var3;
   }

   @x
   public final String zonedDateTimeToJason(ZonedDateTime var1) {
      l.b(var1, "zonedDateTime");
      String var2 = var1.a(DateTimeFormatter.i);
      l.a(var2, "zonedDateTime.format(Dat…tter.ISO_ZONED_DATE_TIME)");
      return var2;
   }
}
