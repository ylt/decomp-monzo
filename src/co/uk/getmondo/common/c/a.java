package co.uk.getmondo.common.c;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import org.threeten.bp.DateTimeUtils;
import org.threeten.bp.Instant;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.YearMonth;

public final class a {
   private static final Calendar a = Calendar.getInstance();
   private static final SimpleDateFormat b;
   private static final SimpleDateFormat c;
   private static final SimpleDateFormat d;
   private static final SimpleDateFormat e;

   static {
      b = new SimpleDateFormat("EEEE", Locale.UK);
      c = new SimpleDateFormat("MMMM", Locale.UK);
      d = new SimpleDateFormat("HH:mm", Locale.UK);
      e = new SimpleDateFormat("EEEE d MMMM yyyy HH:mm:ss", Locale.UK);
   }

   public static int a(long var0) {
      a.setTimeInMillis(var0);
      return a.get(6);
   }

   public static String a(int var0) {
      if(var0 >= 1 && var0 <= 31) {
         String var1;
         if(var0 >= 11 && var0 <= 13) {
            var1 = "th";
         } else {
            switch(var0 % 10) {
            case 1:
               var1 = "st";
               break;
            case 2:
               var1 = "nd";
               break;
            case 3:
               var1 = "rd";
               break;
            default:
               var1 = "th";
            }
         }

         return var1;
      } else {
         throw new IllegalArgumentException("Illegal day of month: " + var0);
      }
   }

   public static String a(long var0, boolean var2) {
      Date var4 = new Date(var0);
      a.setTimeInMillis(var0);
      int var3 = a.get(5);
      Calendar var5 = Calendar.getInstance();
      String var7;
      if(var5.get(1) == a.get(1) && var5.get(6) == a.get(6)) {
         var7 = "Today";
      } else if(!var2 || var5.get(1) >= a.get(1) && var5.get(6) >= a.get(6)) {
         String var8 = a(var3);
         var8 = var3 + var8;
         String var6 = b.format(var4);
         var7 = c.format(var4);
         var7 = String.format(Locale.UK, "%1$s %2$s %3$s", new Object[]{var6, var8, var7});
      } else {
         var7 = String.format(Locale.UK, "%1$s days from now", new Object[]{Long.valueOf(TimeUnit.DAYS.convert(a.getTimeInMillis() - var5.getTimeInMillis(), TimeUnit.MILLISECONDS))});
      }

      return var7;
   }

   public static String a(Date var0) {
      a.setTime(var0);
      String var1 = d.format(var0);
      return String.format(Locale.UK, "%1$s, %2$s", new Object[]{b(var0.getTime()), var1});
   }

   public static Date a() {
      a.setTimeInMillis(System.currentTimeMillis());
      a.add(6, -7);
      return a.getTime();
   }

   public static Date a(Date var0, Date var1) {
      if(var0.getTime() >= var1.getTime()) {
         var0 = var1;
      }

      return var0;
   }

   public static Date a(LocalDateTime var0) {
      Calendar var1 = Calendar.getInstance();
      var1.set(var0.b(), var0.c() - 1, var0.e(), var0.f(), var0.g(), var0.h());
      return var1.getTime();
   }

   public static LocalDateTime a(Date var0, TimeZone var1) {
      return LocalDateTime.a(Instant.b(var0.getTime()), DateTimeUtils.a(var1));
   }

   public static String b(long var0) {
      return a(var0, false);
   }

   public static Date b(Date var0) {
      a.setTime(var0);
      a.add(6, -1);
      return a.getTime();
   }

   public static YearMonth b(Date var0, TimeZone var1) {
      LocalDateTime var2 = a(var0, var1);
      return YearMonth.a(var2.b(), var2.d());
   }

   public static String c(Date var0) {
      SimpleDateFormat var1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.UK);
      var1.setTimeZone(TimeZone.getTimeZone("UTC"));
      return var1.format(var0);
   }

   public static String d(Date var0) {
      return e.format(var0);
   }
}
