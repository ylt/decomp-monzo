package co.uk.getmondo.common.d;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import co.uk.getmondo.MonzoApplication;

public class e extends DialogFragment {
   private boolean a;

   public static e a() {
      return a(false);
   }

   public static e a(boolean var0) {
      e var1 = new e();
      Bundle var2 = new Bundle();
      var2.putBoolean("ARGUMENT_FINISH_ACTIVITY", var0);
      var1.setArguments(var2);
      return var1;
   }

   // $FF: synthetic method
   static void a(Activity var0, DialogInterface var1, int var2) {
      MonzoApplication.a(var0).b().p().a();
   }

   public void onCreate(Bundle var1) {
      super.onCreate(var1);
      var1 = this.getArguments();
      boolean var2;
      if(var1 != null && var1.getBoolean("ARGUMENT_FINISH_ACTIVITY")) {
         var2 = true;
      } else {
         var2 = false;
      }

      this.a = var2;
   }

   public Dialog onCreateDialog(Bundle var1) {
      Activity var2 = this.getActivity();
      return (new Builder(var2)).setTitle(2131362567).setMessage(2131362562).setPositiveButton(this.getString(2131362085), f.a(var2)).setCancelable(false).setNegativeButton(this.getString(2131362499), (OnClickListener)null).create();
   }

   public void onDismiss(DialogInterface var1) {
      super.onDismiss(var1);
      if(this.a) {
         this.getActivity().finish();
      }

   }
}
