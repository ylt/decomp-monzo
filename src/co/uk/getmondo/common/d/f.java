package co.uk.getmondo.common.d;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

// $FF: synthetic class
final class f implements OnClickListener {
   private final Activity a;

   private f(Activity var1) {
      this.a = var1;
   }

   public static OnClickListener a(Activity var0) {
      return new f(var0);
   }

   public void onClick(DialogInterface var1, int var2) {
      e.a(this.a, var1, var2);
   }
}
