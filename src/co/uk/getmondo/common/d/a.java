package co.uk.getmondo.common.d;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.i;
import android.support.v4.app.j;

public class a extends i {
   public static a a(String var0) {
      return a("", var0);
   }

   public static a a(String var0, String var1) {
      return a(var0, var1, true);
   }

   public static a a(String var0, String var1, int var2, boolean var3) {
      a var5 = new a();
      Bundle var4 = new Bundle();
      var4.putString("ARGUMENT_TITLE", var0);
      var4.putString("ARGUMENT_MESSAGE", var1);
      var4.putInt("ARGUMENT_BUTTON_TEXT", var2);
      var4.putBoolean("ARGUMENT_FINISH_ACTIVITY", var3);
      var5.setArguments(var4);
      return var5;
   }

   public static a a(String var0, String var1, boolean var2) {
      return a(var0, var1, 17039370, var2);
   }

   // $FF: synthetic method
   static void a(a var0, Activity var1, DialogInterface var2, int var3) {
      if(var0.getArguments().getBoolean("ARGUMENT_FINISH_ACTIVITY")) {
         var1.finish();
      }

   }

   public Dialog onCreateDialog(Bundle var1) {
      j var2 = this.getActivity();
      return (new android.support.v7.app.d.a(var2, 2131493138)).a(this.getArguments().getString("ARGUMENT_TITLE")).b(this.getArguments().getString("ARGUMENT_MESSAGE")).a(this.getArguments().getInt("ARGUMENT_BUTTON_TEXT"), b.a(this, var2)).b();
   }
}
