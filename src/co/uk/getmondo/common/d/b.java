package co.uk.getmondo.common.d;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

// $FF: synthetic class
final class b implements OnClickListener {
   private final a a;
   private final Activity b;

   private b(a var1, Activity var2) {
      this.a = var1;
      this.b = var2;
   }

   public static OnClickListener a(a var0, Activity var1) {
      return new b(var0, var1);
   }

   public void onClick(DialogInterface var1, int var2) {
      a.a(this.a, this.b, var1, var2);
   }
}
