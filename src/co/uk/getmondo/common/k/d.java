package co.uk.getmondo.common.k;

import android.text.Editable;
import android.text.TextWatcher;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\f\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH\u0016J(\u0010\f\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u00042\u0006\u0010\u000f\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0004H\u0016J(\u0010\u0010\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u000f\u001a\u00020\u0004H\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082D¢\u0006\u0002\n\u0000¨\u0006\u0011"},
   d2 = {"Lco/uk/getmondo/common/utils/DateInputFormatter;", "Landroid/text/TextWatcher;", "()V", "after", "", "before", "divider", "", "afterTextChanged", "", "text", "Landroid/text/Editable;", "beforeTextChanged", "", "start", "count", "onTextChanged", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class d implements TextWatcher {
   private final char a = 47;
   private int b;
   private int c;

   public void afterTextChanged(Editable var1) {
      boolean var5 = true;
      kotlin.d.b.l.b(var1, "text");
      boolean var2;
      if(this.c < this.b) {
         var2 = true;
      } else {
         var2 = false;
      }

      if(!var2) {
         if(((CharSequence)var1).length() == 0) {
            var2 = true;
         } else {
            var2 = false;
         }

         if(!var2) {
            if(var1.charAt(kotlin.h.j.e((CharSequence)var1)) == this.a) {
               var2 = true;
            } else {
               var2 = false;
            }

            boolean var3;
            if(var1.length() != 2 && var1.length() != 5) {
               var3 = false;
            } else {
               var3 = true;
            }

            boolean var4 = var5;
            if(var1.length() != 3) {
               if(var1.length() == 6) {
                  var4 = var5;
               } else {
                  var4 = false;
               }
            }

            if(var2 && var1.length() == 2) {
               var1.insert(0, (CharSequence)"0");
            } else if(var2 && var1.charAt(2) == this.a && var1.length() == 5) {
               var1.insert(3, (CharSequence)"0");
            } else if(var3) {
               var1.append(this.a);
            } else if(var4 && !var2) {
               var1.insert(kotlin.h.j.e((CharSequence)var1), (CharSequence)("" + this.a));
            } else if(!var4 && var2) {
               var1.delete(kotlin.h.j.e((CharSequence)var1), kotlin.h.j.e((CharSequence)var1) + 1);
            }
         }
      }

   }

   public void beforeTextChanged(CharSequence var1, int var2, int var3, int var4) {
      kotlin.d.b.l.b(var1, "text");
      this.c = var4;
   }

   public void onTextChanged(CharSequence var1, int var2, int var3, int var4) {
      kotlin.d.b.l.b(var1, "text");
      this.b = var3;
   }
}
