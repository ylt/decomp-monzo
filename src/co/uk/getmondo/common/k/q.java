package co.uk.getmondo.common.k;

import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000*\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\u001a\u001c\u0010\u0005\u001a\u00020\u0006*\u00020\u00072\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0002H\u0002\u001a\u001a\u0010\u000b\u001a\u00020\u0006*\u00020\u00072\u0006\u0010\f\u001a\u00020\t2\u0006\u0010\r\u001a\u00020\u000e\"\u0014\u0010\u0000\u001a\b\u0012\u0004\u0012\u00020\u00020\u0001X\u0082\u0004¢\u0006\u0002\n\u0000\"\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00040\u0001X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u000f"},
   d2 = {"MATRIX", "Ljava/lang/ThreadLocal;", "Landroid/graphics/Matrix;", "RECT_F", "Landroid/graphics/RectF;", "offsetDescendantMatrix", "", "Landroid/view/ViewGroup;", "view", "Landroid/view/View;", "m", "offsetDescendantRect", "child", "rect", "Landroid/graphics/Rect;", "app_monzoPrepaidRelease"},
   k = 2,
   mv = {1, 1, 7}
)
public final class q {
   private static final ThreadLocal a = new ThreadLocal();
   private static final ThreadLocal b = new ThreadLocal();

   private static final void a(ViewGroup var0, View var1, Matrix var2) {
      ViewParent var3 = var1.getParent();
      if(var3 instanceof View && var3 != var0) {
         View var4 = (View)var3;
         a(var0, var4, var2);
         var2.preTranslate((float)(-var4.getScrollX()), (float)(-var4.getScrollY()));
      }

      var2.preTranslate((float)var1.getLeft(), (float)var1.getTop());
      if(!var1.getMatrix().isIdentity()) {
         var2.preConcat(var1.getMatrix());
      }

   }

   public static final void a(ViewGroup var0, View var1, Rect var2) {
      kotlin.d.b.l.b(var0, "$receiver");
      kotlin.d.b.l.b(var1, "child");
      kotlin.d.b.l.b(var2, "rect");
      var2.set(0, 0, var1.getWidth(), var1.getHeight());
      Matrix var3 = (Matrix)a.get();
      if(var3 == null) {
         var3 = new Matrix();
         a.set(var3);
      } else {
         var3.reset();
      }

      a(var0, var1, var3);
      RectF var5 = (RectF)b.get();
      RectF var4 = var5;
      if(var5 == null) {
         var4 = new RectF();
         b.set(var4);
      }

      var4.set(var2);
      var3.mapRect(var4);
      var2.set((int)(var4.left + 0.5F), (int)(var4.top + 0.5F), (int)(var4.right + 0.5F), (int)(var4.bottom + 0.5F));
   }
}
