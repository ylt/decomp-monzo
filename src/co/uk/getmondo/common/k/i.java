package co.uk.getmondo.common.k;

import android.text.Editable;
import android.text.TextWatcher;

public class i {
   private char a;
   private int b;

   private i(char var1) {
      this.a = var1;
      this.b = 2;
   }

   public static i a(char var0) {
      return new i(var0);
   }

   public TextWatcher a() {
      return new TextWatcher() {
         public void afterTextChanged(Editable var1) {
            for(int var2 = 0; var2 < var1.length(); ++var2) {
               if(var2 % (i.this.b + 1) == i.this.b && var1.charAt(var2) != i.this.a) {
                  var1.insert(var2, String.valueOf(i.this.a));
               }
            }

         }

         public void beforeTextChanged(CharSequence var1, int var2, int var3, int var4) {
         }

         public void onTextChanged(CharSequence var1, int var2, int var3, int var4) {
         }
      };
   }

   public i a(int var1) {
      this.b = var1;
      return this;
   }
}
