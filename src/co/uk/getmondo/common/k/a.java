package co.uk.getmondo.common.k;

import co.uk.getmondo.d.s;
import java.util.ArrayList;

public class a {
   public static String a(s var0) {
      return a(a(var0.h()), "\n") + a(var0.i(), " ") + p.h(var0.c());
   }

   private static String a(String var0, String var1) {
      if(p.d(var0)) {
         var0 = "";
      } else {
         var0 = var0 + var1;
      }

      return var0;
   }

   public static String a(String[] var0) {
      String var5;
      if(var0 == null) {
         var5 = "";
      } else {
         ArrayList var3 = new ArrayList(var0.length);
         int var2 = var0.length;

         for(int var1 = 0; var1 < var2; ++var1) {
            String var4 = var0[var1];
            if(p.c(var4)) {
               var3.add(var4);
            }
         }

         var5 = p.a(", ", var3);
      }

      return var5;
   }

   public static String[] a(String var0) {
      return var0.split(",");
   }

   public static String b(s var0) {
      return a(a(var0.h()), ", ") + a(var0.i(), ", ") + p.h(var0.c());
   }
}
