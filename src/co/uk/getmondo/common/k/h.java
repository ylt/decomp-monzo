package co.uk.getmondo.common.k;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.CancellationSignal;
import android.os.Environment;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

@Deprecated
public class h {
   public static long a(Uri var0, ContentResolver var1) throws IOException {
      Object var3 = null;
      Cursor var7 = var1.query(var0, (String[])null, (String)null, (String[])null, (String)null, (CancellationSignal)null);
      String var6 = (String)var3;
      if(var7 != null) {
         var6 = (String)var3;

         try {
            if(var7.moveToFirst()) {
               int var2 = var7.getColumnIndex("_size");
               if(var7.isNull(var2)) {
                  IOException var8 = new IOException("No file size defined");
                  throw var8;
               }

               var6 = var7.getString(var2);
            }
         } finally {
            if(var7 != null) {
               var7.close();
            }

         }
      }

      return Long.parseLong(var6);
   }

   public static File a(Context var0) throws IOException {
      String var1 = (new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH)).format(new Date());
      return File.createTempFile("JPEG_" + var1 + "_", ".jpg", var0.getExternalFilesDir(Environment.DIRECTORY_PICTURES));
   }
}
