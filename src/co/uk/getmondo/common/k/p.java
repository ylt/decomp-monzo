package co.uk.getmondo.common.k;

import android.text.Spannable;
import android.text.style.URLSpan;
import java.util.Iterator;
import java.util.regex.Pattern;

public final class p {
   private static final Pattern a = Pattern.compile("[^\\p{ASCII}]");

   public static Spannable a(Spannable var0) {
      URLSpan[] var5 = (URLSpan[])var0.getSpans(0, var0.length(), URLSpan.class);
      int var2 = var5.length;

      for(int var1 = 0; var1 < var2; ++var1) {
         URLSpan var6 = var5[var1];
         int var3 = var0.getSpanStart(var6);
         int var4 = var0.getSpanEnd(var6);
         var0.removeSpan(var6);
         var0.setSpan(new co.uk.getmondo.transaction.details.views.a(var6.getURL()), var3, var4, 0);
      }

      return var0;
   }

   public static String a(CharSequence var0, Iterable var1) {
      StringBuilder var2 = new StringBuilder();
      Iterator var3 = var1.iterator();
      if(var3.hasNext()) {
         var2.append(var3.next());

         while(var3.hasNext()) {
            var2.append(var0);
            var2.append(var3.next());
         }
      }

      return var2.toString();
   }

   public static String a(String var0, char var1) {
      StringBuilder var3 = new StringBuilder();
      char[] var4 = var0.toCharArray();

      for(int var2 = var4.length; var2 > 0; --var2) {
         if(var2 % 3 == 0 && var2 != var4.length) {
            var3.append(var1).append(var4[var4.length - var2]);
         } else {
            var3.append(var4[var4.length - var2]);
         }
      }

      return var3.toString();
   }

   public static boolean a(String var0) {
      boolean var1 = true;
      if(var0.split(" ").length <= 1) {
         var1 = false;
      }

      return var1;
   }

   public static String b(String var0) {
      String var1 = var0;
      if(var0.contains(" ")) {
         var1 = var0.split(" ")[0];
      }

      return var1;
   }

   public static boolean c(String var0) {
      boolean var1;
      if(var0 != null && !var0.isEmpty()) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public static boolean d(String var0) {
      boolean var1;
      if(var0 != null && !var0.isEmpty()) {
         var1 = false;
      } else {
         var1 = true;
      }

      return var1;
   }

   public static String e(String var0) {
      return a(var0, ' ');
   }

   public static String f(String var0) {
      return a(var0, '-');
   }

   public static String g(String var0) {
      String var1 = var0;
      if(var0 != null) {
         if(var0.isEmpty()) {
            var1 = var0;
         } else {
            var1 = a.matcher(var0).replaceAll("?");
         }
      }

      return var1;
   }

   public static String h(String var0) {
      String var1 = var0;
      if(var0 == null) {
         var1 = "";
      }

      return var1;
   }

   public static String i(String var0) {
      if(!var0.isEmpty()) {
         var0 = var0.substring(0, 1).toUpperCase() + var0.substring(1);
      }

      return var0;
   }
}
