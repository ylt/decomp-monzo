package co.uk.getmondo.common.k;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build.VERSION;
import co.uk.getmondo.common.ThirdPartyShareReceiver;

public class j {
   public static Intent a(Context var0, String var1, String var2, co.uk.getmondo.api.model.tracking.a var3) {
      Intent var6 = a(var2);
      Intent var5;
      if(VERSION.SDK_INT >= 22) {
         Intent var4 = new Intent(var0, ThirdPartyShareReceiver.class);
         var4.putExtra("KEY_ANALYTIC_NAME", var3.value);
         var5 = Intent.createChooser(var6, var1, PendingIntent.getBroadcast(var0, 0, var4, 134217728).getIntentSender());
      } else {
         var5 = Intent.createChooser(var6, var1);
      }

      return var5;
   }

   public static Intent a(String var0) {
      Intent var1 = new Intent("android.intent.action.SEND");
      var1.setType("text/plain");
      var1.putExtra("android.intent.extra.TEXT", var0);
      return var1;
   }
}
