package co.uk.getmondo.common.k;

import android.content.Context;
import android.net.Uri;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class l {
   private final Context a;

   l(Context var1) {
      this.a = var1;
   }

   public File a() {
      File var1 = null;

      File var2;
      try {
         var2 = h.a(this.a);
      } catch (IOException var3) {
         d.a.a.a(var3, "Failed to get photo file from file", new Object[0]);
         return var1;
      }

      var1 = var2;
      return var1;
   }

   public InputStream a(String var1) {
      Object var2 = null;

      InputStream var4;
      try {
         var4 = this.a.getContentResolver().openInputStream(Uri.parse(var1));
      } catch (FileNotFoundException var3) {
         d.a.a.a(var3, "Failed to get photo file from input stream", new Object[0]);
         var4 = (InputStream)var2;
      }

      return var4;
   }

   public long b(String var1) {
      long var2;
      try {
         var2 = h.a(Uri.parse(var1), this.a.getContentResolver());
      } catch (IOException var4) {
         d.a.a.a(var4, "Failed to get photo file size", new Object[0]);
         var2 = 0L;
      }

      return var2;
   }
}
