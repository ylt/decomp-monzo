package co.uk.getmondo.common.k;

import android.graphics.Color;

public class c {
   public static int a(int var0) {
      float var2 = 0.0F;
      float[] var3 = new float[3];
      Color.colorToHSV(var0, var3);
      float var1 = var3[2] - 0.25F;
      if(var1 < 0.0F) {
         var1 = var2;
      }

      var3[2] = var1;
      return Color.HSVToColor(var3);
   }

   public static int a(int var0, float var1) {
      return Color.argb(Math.round((float)Color.alpha(var0) * var1), Color.red(var0), Color.green(var0), Color.blue(var0));
   }
}
