package co.uk.getmondo.common.k;

import java.util.Iterator;
import kotlin.Metadata;
import kotlin.a.x;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\f\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\bH\u0007R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082T¢\u0006\u0002\n\u0000¨\u0006\n"},
   d2 = {"Lco/uk/getmondo/common/utils/SortCodeFormatter;", "", "()V", "divider", "", "maxCharacters", "", "addDashes", "", "sortCode", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class o {
   public static final o a;

   static {
      new o();
   }

   private o() {
      a = (o)this;
   }

   public static final String a(String var0) {
      kotlin.d.b.l.b(var0, "sortCode");
      boolean var1;
      if(((CharSequence)var0).length() == 0) {
         var1 = true;
      } else {
         var1 = false;
      }

      if(!var1) {
         StringBuilder var2 = new StringBuilder();
         char[] var4 = var0.toCharArray();
         kotlin.d.b.l.a(var4, "(this as java.lang.String).toCharArray()");
         Iterator var5 = kotlin.a.g.b(var4).iterator();

         while(var5.hasNext()) {
            x var3 = (x)var5.next();
            int var6 = var3.c();
            var2.append(((Character)var3.d()).charValue());
            if((var6 + 1) % 2 == 0 && var2.length() < 8) {
               var2.append('-');
            }
         }

         if(kotlin.h.j.g((CharSequence)var2) == 45) {
            var2.setLength(var2.length() - 1);
         }

         var0 = var2.toString();
         kotlin.d.b.l.a(var0, "sb.toString()");
      }

      return var0;
   }
}
