package co.uk.getmondo.common.h.a;

import android.content.res.Resources;

public final class m implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final b b;

   static {
      boolean var0;
      if(!m.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   public m(b var1) {
      if(!a && var1 == null) {
         throw new AssertionError();
      } else {
         this.b = var1;
      }
   }

   public static b.a.b a(b var0) {
      return new m(var0);
   }

   public Resources a() {
      return (Resources)b.a.d.a(this.b.c(), "Cannot return null from a non-@Nullable @Provides method");
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
