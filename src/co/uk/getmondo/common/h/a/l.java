package co.uk.getmondo.common.h.a;

import android.content.Context;

public final class l implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final b b;
   private final javax.a.a c;

   static {
      boolean var0;
      if(!l.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   public l(b var1, javax.a.a var2) {
      if(!a && var1 == null) {
         throw new AssertionError();
      } else {
         this.b = var1;
         if(!a && var2 == null) {
            throw new AssertionError();
         } else {
            this.c = var2;
         }
      }
   }

   public static b.a.b a(b var0, javax.a.a var1) {
      return new l(var0, var1);
   }

   public io.michaelrocks.libphonenumber.android.h a() {
      return (io.michaelrocks.libphonenumber.android.h)b.a.d.a(this.b.b((Context)this.c.b()), "Cannot return null from a non-@Nullable @Provides method");
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
