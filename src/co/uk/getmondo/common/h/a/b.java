package co.uk.getmondo.common.h.a;

import android.accounts.AccountManager;
import android.app.Application;
import android.content.Context;
import android.content.res.Resources;
import android.os.Build.VERSION;
import co.uk.getmondo.common.s;
import co.uk.getmondo.common.u;
import co.uk.getmondo.payments.send.data.p;
import io.intercom.android.sdk.push.IntercomPushClient;

public class b {
   protected final Application a;

   public b(Application var1) {
      this.a = var1;
   }

   Application a() {
      return this.a;
   }

   co.uk.getmondo.common.a.c a(Context var1, p var2, co.uk.getmondo.payments.send.data.h var3, co.uk.getmondo.common.accounts.d var4, co.uk.getmondo.common.accounts.k var5, co.uk.getmondo.common.a var6) {
      Object var7;
      if(VERSION.SDK_INT >= 25) {
         var7 = new co.uk.getmondo.common.a.a(var1, var2, var3, var4, var5, var6);
      } else {
         var7 = new co.uk.getmondo.common.a.d();
      }

      return (co.uk.getmondo.common.a.c)var7;
   }

   s a(Context var1) {
      return new u(var1);
   }

   Context b() {
      return this.a;
   }

   io.michaelrocks.libphonenumber.android.h b(Context var1) {
      return io.michaelrocks.libphonenumber.android.h.a(var1);
   }

   Resources c() {
      return this.a.getResources();
   }

   android.support.v4.b.b.a c(Context var1) {
      return android.support.v4.b.b.a.a(var1);
   }

   co.uk.getmondo.common.accounts.k d() {
      return new co.uk.getmondo.common.accounts.k(AccountManager.get(this.a));
   }

   io.reactivex.u e() {
      return io.reactivex.h.a.b();
   }

   io.reactivex.u f() {
      return io.reactivex.a.b.a.a();
   }

   io.reactivex.u g() {
      return io.reactivex.h.a.a();
   }

   IntercomPushClient h() {
      return new IntercomPushClient();
   }
}
