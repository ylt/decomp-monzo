package co.uk.getmondo.common.h.a;

import io.intercom.android.sdk.push.IntercomPushClient;

public final class i implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final b b;

   static {
      boolean var0;
      if(!i.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   public i(b var1) {
      if(!a && var1 == null) {
         throw new AssertionError();
      } else {
         this.b = var1;
      }
   }

   public static b.a.b a(b var0) {
      return new i(var0);
   }

   public IntercomPushClient a() {
      return (IntercomPushClient)b.a.d.a(this.b.h(), "Cannot return null from a non-@Nullable @Provides method");
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
