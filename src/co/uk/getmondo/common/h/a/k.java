package co.uk.getmondo.common.h.a;

import android.content.Context;
import co.uk.getmondo.common.s;

public final class k implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final b b;
   private final javax.a.a c;

   static {
      boolean var0;
      if(!k.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   public k(b var1, javax.a.a var2) {
      if(!a && var1 == null) {
         throw new AssertionError();
      } else {
         this.b = var1;
         if(!a && var2 == null) {
            throw new AssertionError();
         } else {
            this.c = var2;
         }
      }
   }

   public static b.a.b a(b var0, javax.a.a var1) {
      return new k(var0, var1);
   }

   public s a() {
      return (s)b.a.d.a(this.b.a((Context)this.c.b()), "Cannot return null from a non-@Nullable @Provides method");
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
