package co.uk.getmondo.common.h.b;

import co.uk.getmondo.bump_up.BumpUpActivity;
import co.uk.getmondo.bump_up.WebEventActivity;
import co.uk.getmondo.card.CardReplacementActivity;
import co.uk.getmondo.common.address.LegacyEnterAddressActivity;
import co.uk.getmondo.common.address.m;
import co.uk.getmondo.common.address.n;
import co.uk.getmondo.common.pin.g;
import co.uk.getmondo.create_account.VerifyIdentityActivity;
import co.uk.getmondo.create_account.phone_number.EnterCodeActivity;
import co.uk.getmondo.create_account.phone_number.EnterPhoneNumberActivity;
import co.uk.getmondo.create_account.topup.InitialTopupActivity;
import co.uk.getmondo.create_account.topup.TopupInfoActivity;
import co.uk.getmondo.create_account.wait.CardShippedActivity;
import co.uk.getmondo.feed.MonthlySpendingReportActivity;
import co.uk.getmondo.feed.search.FeedSearchActivity;
import co.uk.getmondo.feed.welcome.WelcomeToMonzoActivity;
import co.uk.getmondo.force_upgrade.ForceUpgradeActivity;
import co.uk.getmondo.help.HelpActivity;
import co.uk.getmondo.help.HelpSearchActivity;
import co.uk.getmondo.main.EddLimitsActivity;
import co.uk.getmondo.main.HomeActivity;
import co.uk.getmondo.migration.MigrationAnnouncementActivity;
import co.uk.getmondo.migration.MigrationTourActivity;
import co.uk.getmondo.monzo.me.onboarding.MonzoMeOnboardingActivity;
import co.uk.getmondo.monzo.me.request.RequestMoneyFragment;
import co.uk.getmondo.news.k;
import co.uk.getmondo.overdraft.ChangeOverdraftLimitActivity;
import co.uk.getmondo.payments.recurring_list.RecurringPaymentsActivity;
import co.uk.getmondo.payments.send.SendMoneyFragment;
import co.uk.getmondo.payments.send.bank.BankPaymentActivity;
import co.uk.getmondo.payments.send.bank.payee.UkBankPayeeDetailsFragment;
import co.uk.getmondo.payments.send.bank.payment.h;
import co.uk.getmondo.payments.send.bank.payment.i;
import co.uk.getmondo.payments.send.onboarding.PeerToPeerIntroActivity;
import co.uk.getmondo.payments.send.onboarding.PeerToPeerMoreInfoActivity;
import co.uk.getmondo.payments.send.payment_category.l;
import co.uk.getmondo.pin.ForgotPinActivity;
import co.uk.getmondo.pots.CreatePotActivity;
import co.uk.getmondo.pots.CustomPotNameActivity;
import co.uk.getmondo.profile.address.SelectAddressActivity;
import co.uk.getmondo.settings.LimitsActivity;
import co.uk.getmondo.settings.SettingsActivity;
import co.uk.getmondo.signup.EmailActivity;
import co.uk.getmondo.signup.card_activation.ActivateCardActivity;
import co.uk.getmondo.signup.card_activation.CardOnItsWayActivity;
import co.uk.getmondo.signup.card_ordering.OrderCardActivity;
import co.uk.getmondo.signup.documents.LegalDocumentsActivity;
import co.uk.getmondo.signup.identity_verification.CountrySelectionActivity;
import co.uk.getmondo.signup.identity_verification.IdentityApprovedActivity;
import co.uk.getmondo.signup.identity_verification.o;
import co.uk.getmondo.signup.identity_verification.p;
import co.uk.getmondo.signup.identity_verification.t;
import co.uk.getmondo.signup.identity_verification.chat_with_us.ChatWithUsActivity;
import co.uk.getmondo.signup.marketing_opt_in.MarketingOptInActivity;
import co.uk.getmondo.signup.pending.SignupPendingActivity;
import co.uk.getmondo.signup.phone_verification.PhoneVerificationActivity;
import co.uk.getmondo.signup.phone_verification.j;
import co.uk.getmondo.signup.profile.AddressConfirmationActivity;
import co.uk.getmondo.signup.profile.ProfileCreationActivity;
import co.uk.getmondo.signup.rejected.SignupRejectedActivity;
import co.uk.getmondo.signup.status.SignupStatusActivity;
import co.uk.getmondo.signup.tax_residency.TaxResidencyActivity;
import co.uk.getmondo.signup.tax_residency.b.q;
import co.uk.getmondo.signup_old.CreateProfileActivity;
import co.uk.getmondo.signup_old.SignUpActivity;
import co.uk.getmondo.splash.SplashActivity;
import co.uk.getmondo.terms_and_conditions.TermsAndConditionsActivity;
import co.uk.getmondo.top.NotInCountryActivity;
import co.uk.getmondo.top.TopActivity;
import co.uk.getmondo.topup.TopUpActivity;
import co.uk.getmondo.topup.bank.TopUpInstructionsActivity;
import co.uk.getmondo.topup.card.TopUpWithCardFragment;
import co.uk.getmondo.topup.card.TopUpWithNewCardActivity;
import co.uk.getmondo.topup.card.TopUpWithSavedCardActivity;
import co.uk.getmondo.topup.three_d_secure.ThreeDsWebActivity;
import co.uk.getmondo.transaction.attachment.AttachmentActivity;
import co.uk.getmondo.transaction.attachment.AttachmentDetailsActivity;
import co.uk.getmondo.waitlist.ShareActivity;
import co.uk.getmondo.waitlist.WaitlistActivity;
import co.uk.getmondo.welcome.WelcomeOnboardingActivity;

public interface b {
   co.uk.getmondo.card.activate.b a(co.uk.getmondo.card.activate.c var1);

   m a(n var1);

   co.uk.getmondo.common.pin.a a(g var1);

   co.uk.getmondo.golden_ticket.c a(co.uk.getmondo.golden_ticket.d var1);

   co.uk.getmondo.help.b.a a(co.uk.getmondo.help.b.b var1);

   co.uk.getmondo.help.b.e a(co.uk.getmondo.help.b.f var1);

   co.uk.getmondo.monzo.me.customise.d a(co.uk.getmondo.monzo.me.customise.e var1);

   co.uk.getmondo.monzo.me.deeplink.b a(co.uk.getmondo.monzo.me.deeplink.c var1);

   co.uk.getmondo.news.b a(k var1);

   co.uk.getmondo.payments.recurring_cancellation.e a(co.uk.getmondo.payments.recurring_cancellation.f var1);

   co.uk.getmondo.payments.send.authentication.e a(co.uk.getmondo.payments.send.authentication.f var1);

   h a(i var1);

   l a(co.uk.getmondo.payments.send.payment_category.m var1);

   co.uk.getmondo.payments.send.peer.e a(co.uk.getmondo.payments.send.peer.f var1);

   co.uk.getmondo.profile.address.b a(co.uk.getmondo.profile.address.c var1);

   o a(p var1);

   co.uk.getmondo.signup.identity_verification.sdd.b a(co.uk.getmondo.signup.identity_verification.sdd.c var1);

   co.uk.getmondo.signup.identity_verification.video.e a(co.uk.getmondo.signup.identity_verification.video.f var1);

   co.uk.getmondo.spending.merchant.a a(co.uk.getmondo.spending.merchant.f var1);

   co.uk.getmondo.spending.transactions.b a(co.uk.getmondo.spending.transactions.d var1);

   co.uk.getmondo.transaction.c a(co.uk.getmondo.transaction.d var1);

   void a(BumpUpActivity var1);

   void a(WebEventActivity var1);

   void a(co.uk.getmondo.c.a var1);

   void a(CardReplacementActivity var1);

   void a(co.uk.getmondo.card.a var1);

   void a(LegacyEnterAddressActivity var1);

   void a(co.uk.getmondo.common.d.c var1);

   void a(VerifyIdentityActivity var1);

   void a(EnterCodeActivity var1);

   void a(EnterPhoneNumberActivity var1);

   void a(InitialTopupActivity var1);

   void a(TopupInfoActivity var1);

   void a(CardShippedActivity var1);

   void a(MonthlySpendingReportActivity var1);

   void a(co.uk.getmondo.feed.e var1);

   void a(FeedSearchActivity var1);

   void a(WelcomeToMonzoActivity var1);

   void a(ForceUpgradeActivity var1);

   void a(HelpActivity var1);

   void a(HelpSearchActivity var1);

   void a(EddLimitsActivity var1);

   void a(HomeActivity var1);

   void a(MigrationAnnouncementActivity var1);

   void a(MigrationTourActivity var1);

   void a(MonzoMeOnboardingActivity var1);

   void a(RequestMoneyFragment var1);

   void a(ChangeOverdraftLimitActivity var1);

   void a(RecurringPaymentsActivity var1);

   void a(SendMoneyFragment var1);

   void a(BankPaymentActivity var1);

   void a(UkBankPayeeDetailsFragment var1);

   void a(PeerToPeerIntroActivity var1);

   void a(PeerToPeerMoreInfoActivity var1);

   void a(ForgotPinActivity var1);

   void a(CreatePotActivity var1);

   void a(CustomPotNameActivity var1);

   void a(SelectAddressActivity var1);

   void a(co.uk.getmondo.profile.address.n var1);

   void a(LimitsActivity var1);

   void a(SettingsActivity var1);

   void a(EmailActivity var1);

   void a(ActivateCardActivity var1);

   void a(CardOnItsWayActivity var1);

   void a(OrderCardActivity var1);

   void a(co.uk.getmondo.signup.card_ordering.a var1);

   void a(co.uk.getmondo.signup.card_ordering.e var1);

   void a(LegalDocumentsActivity var1);

   void a(co.uk.getmondo.signup.documents.b var1);

   void a(CountrySelectionActivity var1);

   void a(IdentityApprovedActivity var1);

   void a(ChatWithUsActivity var1);

   void a(t var1);

   void a(MarketingOptInActivity var1);

   void a(co.uk.getmondo.signup.marketing_opt_in.d var1);

   void a(SignupPendingActivity var1);

   void a(PhoneVerificationActivity var1);

   void a(co.uk.getmondo.signup.phone_verification.e var1);

   void a(j var1);

   void a(AddressConfirmationActivity var1);

   void a(ProfileCreationActivity var1);

   void a(co.uk.getmondo.signup.profile.e var1);

   void a(co.uk.getmondo.signup.profile.l var1);

   void a(SignupRejectedActivity var1);

   void a(SignupStatusActivity var1);

   void a(TaxResidencyActivity var1);

   void a(co.uk.getmondo.signup.tax_residency.b.b var1);

   void a(co.uk.getmondo.signup.tax_residency.b.g var1);

   void a(co.uk.getmondo.signup.tax_residency.b.l var1);

   void a(q var1);

   void a(CreateProfileActivity var1);

   void a(SignUpActivity var1);

   void a(co.uk.getmondo.spending.b.a var1);

   void a(co.uk.getmondo.spending.b var1);

   void a(SplashActivity var1);

   void a(TermsAndConditionsActivity var1);

   void a(NotInCountryActivity var1);

   void a(TopActivity var1);

   void a(TopUpActivity var1);

   void a(TopUpInstructionsActivity var1);

   void a(TopUpWithCardFragment var1);

   void a(TopUpWithNewCardActivity var1);

   void a(TopUpWithSavedCardActivity var1);

   void a(ThreeDsWebActivity var1);

   void a(AttachmentActivity var1);

   void a(AttachmentDetailsActivity var1);

   void a(ShareActivity var1);

   void a(WaitlistActivity var1);

   void a(WelcomeOnboardingActivity var1);
}
