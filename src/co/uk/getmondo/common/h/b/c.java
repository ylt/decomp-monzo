package co.uk.getmondo.common.h.b;

import android.app.Activity;
import android.content.Context;
import co.uk.getmondo.topup.three_d_secure.ThreeDsResolver;
import com.stripe.android.Stripe;

public class c {
   private Activity a;

   public c(Activity var1) {
      this.a = var1;
   }

   Context a() {
      return this.a;
   }

   ThreeDsResolver a(co.uk.getmondo.common.a var1) {
      return new co.uk.getmondo.topup.three_d_secure.b(this.a, var1);
   }

   Stripe a(Context var1) {
      return new Stripe(var1, "pk_live_LW3b2bi1iXHicd88NnwlCkaA");
   }
}
