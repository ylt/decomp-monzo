package co.uk.getmondo.common.h.b;

import android.content.Context;
import android.content.res.Resources;
import co.uk.getmondo.api.HelpApi;
import co.uk.getmondo.api.IdentityVerificationApi;
import co.uk.getmondo.api.MigrationApi;
import co.uk.getmondo.api.MonzoApi;
import co.uk.getmondo.api.OverdraftApi;
import co.uk.getmondo.api.PaymentLimitsApi;
import co.uk.getmondo.api.PaymentsApi;
import co.uk.getmondo.api.ServiceStatusApi;
import co.uk.getmondo.api.SignupApi;
import co.uk.getmondo.api.TaxResidencyApi;
import co.uk.getmondo.api.ae;
import co.uk.getmondo.bump_up.BumpUpActivity;
import co.uk.getmondo.bump_up.WebEventActivity;
import co.uk.getmondo.card.CardReplacementActivity;
import co.uk.getmondo.common.ab;
import co.uk.getmondo.common.address.LegacyEnterAddressActivity;
import co.uk.getmondo.common.pin.PinEntryActivity;
import co.uk.getmondo.create_account.VerifyIdentityActivity;
import co.uk.getmondo.create_account.phone_number.EnterCodeActivity;
import co.uk.getmondo.create_account.phone_number.EnterPhoneNumberActivity;
import co.uk.getmondo.create_account.phone_number.af;
import co.uk.getmondo.create_account.topup.InitialTopupActivity;
import co.uk.getmondo.create_account.topup.TopupInfoActivity;
import co.uk.getmondo.create_account.topup.y;
import co.uk.getmondo.create_account.wait.CardShippedActivity;
import co.uk.getmondo.feed.MonthlySpendingReportActivity;
import co.uk.getmondo.feed.a.w;
import co.uk.getmondo.feed.search.FeedSearchActivity;
import co.uk.getmondo.feed.welcome.WelcomeToMonzoActivity;
import co.uk.getmondo.force_upgrade.ForceUpgradeActivity;
import co.uk.getmondo.golden_ticket.GoldenTicketActivity;
import co.uk.getmondo.help.HelpActivity;
import co.uk.getmondo.help.HelpCategoryActivity;
import co.uk.getmondo.help.HelpSearchActivity;
import co.uk.getmondo.help.HelpTopicActivity;
import co.uk.getmondo.main.EddLimitsActivity;
import co.uk.getmondo.main.HomeActivity;
import co.uk.getmondo.migration.MigrationAnnouncementActivity;
import co.uk.getmondo.migration.MigrationTourActivity;
import co.uk.getmondo.monzo.me.customise.CustomiseMonzoMeLinkActivity;
import co.uk.getmondo.monzo.me.deeplink.MonzoMeActivity;
import co.uk.getmondo.monzo.me.onboarding.MonzoMeOnboardingActivity;
import co.uk.getmondo.monzo.me.request.RequestMoneyFragment;
import co.uk.getmondo.news.NewsActivity;
import co.uk.getmondo.overdraft.ChangeOverdraftLimitActivity;
import co.uk.getmondo.payments.recurring_cancellation.RecurringPaymentCancelActivity;
import co.uk.getmondo.payments.recurring_list.RecurringPaymentsActivity;
import co.uk.getmondo.payments.send.SendMoneyFragment;
import co.uk.getmondo.payments.send.authentication.PaymentAuthenticationActivity;
import co.uk.getmondo.payments.send.bank.BankPaymentActivity;
import co.uk.getmondo.payments.send.bank.payee.UkBankPayeeDetailsFragment;
import co.uk.getmondo.payments.send.bank.payment.BankPaymentDetailsActivity;
import co.uk.getmondo.payments.send.onboarding.PeerToPeerIntroActivity;
import co.uk.getmondo.payments.send.onboarding.PeerToPeerMoreInfoActivity;
import co.uk.getmondo.payments.send.payment_category.PaymentCategoryActivity;
import co.uk.getmondo.payments.send.peer.PeerPaymentActivity;
import co.uk.getmondo.pin.ForgotPinActivity;
import co.uk.getmondo.pots.CreatePotActivity;
import co.uk.getmondo.pots.CustomPotNameActivity;
import co.uk.getmondo.profile.address.EnterAddressActivity;
import co.uk.getmondo.profile.address.SelectAddressActivity;
import co.uk.getmondo.profile.data.MonzoProfileApi;
import co.uk.getmondo.settings.LimitsActivity;
import co.uk.getmondo.settings.SettingsActivity;
import co.uk.getmondo.signup.EmailActivity;
import co.uk.getmondo.signup.card_activation.ActivateCardActivity;
import co.uk.getmondo.signup.card_activation.CardOnItsWayActivity;
import co.uk.getmondo.signup.card_ordering.OrderCardActivity;
import co.uk.getmondo.signup.documents.LegalDocumentsActivity;
import co.uk.getmondo.signup.identity_verification.CountrySelectionActivity;
import co.uk.getmondo.signup.identity_verification.IdentityApprovedActivity;
import co.uk.getmondo.signup.identity_verification.IdentityVerificationActivity;
import co.uk.getmondo.signup.identity_verification.VerificationPendingActivity;
import co.uk.getmondo.signup.identity_verification.ad;
import co.uk.getmondo.signup.identity_verification.z;
import co.uk.getmondo.signup.identity_verification.chat_with_us.ChatWithUsActivity;
import co.uk.getmondo.signup.identity_verification.fallback.DocumentFallbackActivity;
import co.uk.getmondo.signup.identity_verification.fallback.VideoFallbackActivity;
import co.uk.getmondo.signup.identity_verification.id_picture.DocumentCameraActivity;
import co.uk.getmondo.signup.identity_verification.sdd.IdentityVerificationSddActivity;
import co.uk.getmondo.signup.identity_verification.video.VideoPlaybackActivity;
import co.uk.getmondo.signup.identity_verification.video.VideoRecordingActivity;
import co.uk.getmondo.signup.identity_verification.video.aa;
import co.uk.getmondo.signup.identity_verification.video.ac;
import co.uk.getmondo.signup.marketing_opt_in.MarketingOptInActivity;
import co.uk.getmondo.signup.pending.SignupPendingActivity;
import co.uk.getmondo.signup.phone_verification.PhoneVerificationActivity;
import co.uk.getmondo.signup.profile.AddressConfirmationActivity;
import co.uk.getmondo.signup.profile.ProfileCreationActivity;
import co.uk.getmondo.signup.rejected.SignupRejectedActivity;
import co.uk.getmondo.signup.status.SignupStatusActivity;
import co.uk.getmondo.signup.tax_residency.TaxResidencyActivity;
import co.uk.getmondo.signup_old.CreateProfileActivity;
import co.uk.getmondo.signup_old.SignUpActivity;
import co.uk.getmondo.spending.merchant.SpendingByMerchantFragment;
import co.uk.getmondo.spending.transactions.SpendingTransactionsFragment;
import co.uk.getmondo.splash.SplashActivity;
import co.uk.getmondo.terms_and_conditions.TermsAndConditionsActivity;
import co.uk.getmondo.top.NotInCountryActivity;
import co.uk.getmondo.top.TopActivity;
import co.uk.getmondo.topup.TopUpActivity;
import co.uk.getmondo.topup.an;
import co.uk.getmondo.topup.ap;
import co.uk.getmondo.topup.a.x;
import co.uk.getmondo.topup.bank.TopUpInstructionsActivity;
import co.uk.getmondo.topup.card.TopUpWithCardFragment;
import co.uk.getmondo.topup.card.TopUpWithNewCardActivity;
import co.uk.getmondo.topup.card.TopUpWithSavedCardActivity;
import co.uk.getmondo.topup.three_d_secure.ThreeDsWebActivity;
import co.uk.getmondo.transaction.attachment.AttachmentActivity;
import co.uk.getmondo.transaction.attachment.AttachmentDetailsActivity;
import co.uk.getmondo.transaction.details.TransactionDetailsActivity;
import co.uk.getmondo.waitlist.ShareActivity;
import co.uk.getmondo.waitlist.WaitlistActivity;
import co.uk.getmondo.welcome.WelcomeOnboardingActivity;
import io.intercom.android.sdk.push.IntercomPushClient;

public final class a implements b {
   // $FF: synthetic field
   static final boolean a;
   private javax.a.a A;
   private javax.a.a B;
   private javax.a.a C;
   private javax.a.a D;
   private javax.a.a E;
   private javax.a.a F;
   private javax.a.a G;
   private javax.a.a H;
   private javax.a.a I;
   private javax.a.a J;
   private javax.a.a K;
   private javax.a.a L;
   private javax.a.a M;
   private javax.a.a N;
   private b.a O;
   private javax.a.a P;
   private javax.a.a Q;
   private b.a R;
   private b.a S;
   private javax.a.a T;
   private b.a U;
   private javax.a.a V;
   private b.a W;
   private javax.a.a X;
   private javax.a.a Y;
   private javax.a.a Z;
   private javax.a.a aA;
   private b.a aB;
   private javax.a.a aC;
   private b.a aD;
   private javax.a.a aE;
   private b.a aF;
   private javax.a.a aG;
   private javax.a.a aH;
   private javax.a.a aI;
   private b.a aJ;
   private javax.a.a aK;
   private b.a aL;
   private javax.a.a aM;
   private b.a aN;
   private javax.a.a aO;
   private javax.a.a aP;
   private javax.a.a aQ;
   private javax.a.a aR;
   private javax.a.a aS;
   private b.a aT;
   private javax.a.a aU;
   private b.a aV;
   private javax.a.a aW;
   private b.a aX;
   private javax.a.a aY;
   private javax.a.a aZ;
   private javax.a.a aa;
   private javax.a.a ab;
   private b.a ac;
   private javax.a.a ad;
   private b.a ae;
   private b.a af;
   private javax.a.a ag;
   private javax.a.a ah;
   private b.a ai;
   private javax.a.a aj;
   private javax.a.a ak;
   private javax.a.a al;
   private b.a am;
   private javax.a.a an;
   private b.a ao;
   private javax.a.a ap;
   private javax.a.a aq;
   private b.a ar;
   private javax.a.a as;
   private javax.a.a at;
   private javax.a.a au;
   private b.a av;
   private b.a aw;
   private b.a ax;
   private javax.a.a ay;
   private b.a az;
   private javax.a.a b;
   private javax.a.a bA;
   private b.a bB;
   private javax.a.a bC;
   private javax.a.a bD;
   private javax.a.a bE;
   private javax.a.a bF;
   private b.a bG;
   private javax.a.a bH;
   private b.a bI;
   private javax.a.a bJ;
   private b.a bK;
   private b.a bL;
   private javax.a.a bM;
   private javax.a.a bN;
   private b.a bO;
   private b.a bP;
   private javax.a.a bQ;
   private b.a bR;
   private javax.a.a bS;
   private b.a bT;
   private b.a bU;
   private javax.a.a bV;
   private b.a bW;
   private javax.a.a bX;
   private b.a bY;
   private b.a bZ;
   private b.a ba;
   private javax.a.a bb;
   private b.a bc;
   private javax.a.a bd;
   private b.a be;
   private javax.a.a bf;
   private b.a bg;
   private javax.a.a bh;
   private javax.a.a bi;
   private javax.a.a bj;
   private javax.a.a bk;
   private javax.a.a bl;
   private javax.a.a bm;
   private javax.a.a bn;
   private b.a bo;
   private javax.a.a bp;
   private b.a bq;
   private javax.a.a br;
   private b.a bs;
   private javax.a.a bt;
   private javax.a.a bu;
   private javax.a.a bv;
   private b.a bw;
   private javax.a.a bx;
   private b.a by;
   private javax.a.a bz;
   private javax.a.a c;
   private javax.a.a cA;
   private b.a cB;
   private javax.a.a cC;
   private b.a cD;
   private javax.a.a cE;
   private javax.a.a cF;
   private b.a cG;
   private javax.a.a cH;
   private b.a cI;
   private javax.a.a cJ;
   private javax.a.a cK;
   private b.a cL;
   private javax.a.a cM;
   private b.a cN;
   private javax.a.a cO;
   private b.a cP;
   private javax.a.a cQ;
   private javax.a.a cR;
   private b.a cS;
   private javax.a.a cT;
   private javax.a.a cU;
   private b.a cV;
   private b.a cW;
   private javax.a.a cX;
   private javax.a.a cY;
   private b.a cZ;
   private javax.a.a ca;
   private javax.a.a cb;
   private javax.a.a cc;
   private b.a cd;
   private b.a ce;
   private javax.a.a cf;
   private javax.a.a cg;
   private javax.a.a ch;
   private javax.a.a ci;
   private javax.a.a cj;
   private b.a ck;
   private javax.a.a cl;
   private b.a cm;
   private b.a cn;
   private javax.a.a co;
   private javax.a.a cp;
   private javax.a.a cq;
   private b.a cr;
   private javax.a.a cs;
   private b.a ct;
   private javax.a.a cu;
   private b.a cv;
   private javax.a.a cw;
   private b.a cx;
   private javax.a.a cy;
   private b.a cz;
   private javax.a.a d;
   private b.a dA;
   private javax.a.a dB;
   private b.a dC;
   private javax.a.a dD;
   private b.a dE;
   private javax.a.a dF;
   private b.a dG;
   private javax.a.a dH;
   private javax.a.a dI;
   private javax.a.a da;
   private b.a db;
   private javax.a.a dc;
   private javax.a.a dd;
   private b.a de;
   private javax.a.a df;
   private b.a dg;
   private b.a dh;
   private b.a di;
   private b.a dj;
   private b.a dk;
   private javax.a.a dl;
   private b.a dm;
   private javax.a.a dn;
   private b.a do;
   private javax.a.a dp;
   private b.a dq;
   private javax.a.a dr;
   private javax.a.a ds;
   private b.a dt;
   private javax.a.a du;
   private b.a dv;
   private javax.a.a dw;
   private b.a dx;
   private javax.a.a dy;
   private b.a dz;
   private javax.a.a e;
   private javax.a.a f;
   private b.a g;
   private javax.a.a h;
   private javax.a.a i;
   private javax.a.a j;
   private javax.a.a k;
   private javax.a.a l;
   private javax.a.a m;
   private javax.a.a n;
   private javax.a.a o;
   private javax.a.a p;
   private javax.a.a q;
   private javax.a.a r;
   private javax.a.a s;
   private javax.a.a t;
   private javax.a.a u;
   private javax.a.a v;
   private javax.a.a w;
   private javax.a.a x;
   private javax.a.a y;
   private javax.a.a z;

   static {
      boolean var0;
      if(!a.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   private a(a.d var1) {
      if(!a && var1 == null) {
         throw new AssertionError();
      } else {
         this.a(var1);
         this.b(var1);
         this.c(var1);
      }
   }

   // $FF: synthetic method
   a(a.d var1, Object var2) {
      this(var1);
   }

   public static a.d a() {
      return new a.d(null);
   }

   private void a(final a.d var1) {
      this.b = d.a(var1.a);
      this.c = new b.a.b() {
         private final co.uk.getmondo.common.h.a.a c;

         {
            this.c = var1.b;
         }

         public Context a() {
            return (Context)b.a.d.a(this.c.a(), "Cannot return null from a non-@Nullable component method");
         }

         // $FF: synthetic method
         public Object b() {
            return this.a();
         }
      };
      this.d = b.a.a.a(e.a(var1.a, this.c));
      this.e = new b.a.b() {
         private final co.uk.getmondo.common.h.a.a c;

         {
            this.c = var1.b;
         }

         public co.uk.getmondo.common.a a() {
            return (co.uk.getmondo.common.a)b.a.d.a(this.c.h(), "Cannot return null from a non-@Nullable component method");
         }

         // $FF: synthetic method
         public Object b() {
            return this.a();
         }
      };
      this.f = co.uk.getmondo.common.pager.i.a(this.e);
      this.g = co.uk.getmondo.signup.identity_verification.u.a(this.f);
      this.h = new b.a.b() {
         private final co.uk.getmondo.common.h.a.a c;

         {
            this.c = var1.b;
         }

         public io.reactivex.u a() {
            return (io.reactivex.u)b.a.d.a(this.c.e(), "Cannot return null from a non-@Nullable component method");
         }

         // $FF: synthetic method
         public Object b() {
            return this.a();
         }
      };
      this.i = new b.a.b() {
         private final co.uk.getmondo.common.h.a.a c;

         {
            this.c = var1.b;
         }

         public io.reactivex.u a() {
            return (io.reactivex.u)b.a.d.a(this.c.f(), "Cannot return null from a non-@Nullable component method");
         }

         // $FF: synthetic method
         public Object b() {
            return this.a();
         }
      };
      this.j = new b.a.b() {
         private final co.uk.getmondo.common.h.a.a c;

         {
            this.c = var1.b;
         }

         public co.uk.getmondo.common.accounts.d a() {
            return (co.uk.getmondo.common.accounts.d)b.a.d.a(this.c.b(), "Cannot return null from a non-@Nullable component method");
         }

         // $FF: synthetic method
         public Object b() {
            return this.a();
         }
      };
      this.k = co.uk.getmondo.common.e.b.a(this.j, this.e);
      this.l = new b.a.b() {
         private final co.uk.getmondo.common.h.a.a c;

         {
            this.c = var1.b;
         }

         public co.uk.getmondo.api.b.a a() {
            return (co.uk.getmondo.api.b.a)b.a.d.a(this.c.l(), "Cannot return null from a non-@Nullable component method");
         }

         // $FF: synthetic method
         public Object b() {
            return this.a();
         }
      };
      this.m = new b.a.b() {
         private final co.uk.getmondo.common.h.a.a c;

         {
            this.c = var1.b;
         }

         public ServiceStatusApi a() {
            return (ServiceStatusApi)b.a.d.a(this.c.J(), "Cannot return null from a non-@Nullable component method");
         }

         // $FF: synthetic method
         public Object b() {
            return this.a();
         }
      };
      this.n = new b.a.b() {
         private final co.uk.getmondo.common.h.a.a c;

         {
            this.c = var1.b;
         }

         public co.uk.getmondo.payments.send.data.h a() {
            return (co.uk.getmondo.payments.send.data.h)b.a.d.a(this.c.z(), "Cannot return null from a non-@Nullable component method");
         }

         // $FF: synthetic method
         public Object b() {
            return this.a();
         }
      };
      this.o = new b.a.b() {
         private final co.uk.getmondo.common.h.a.a c;

         {
            this.c = var1.b;
         }

         public co.uk.getmondo.payments.send.data.p a() {
            return (co.uk.getmondo.payments.send.data.p)b.a.d.a(this.c.v(), "Cannot return null from a non-@Nullable component method");
         }

         // $FF: synthetic method
         public Object b() {
            return this.a();
         }
      };
      this.p = new b.a.b() {
         private final co.uk.getmondo.common.h.a.a c;

         {
            this.c = var1.b;
         }

         public PaymentsApi a() {
            return (PaymentsApi)b.a.d.a(this.c.F(), "Cannot return null from a non-@Nullable component method");
         }

         // $FF: synthetic method
         public Object b() {
            return this.a();
         }
      };
      this.q = co.uk.getmondo.payments.send.data.e.a(this.p);
      this.r = new b.a.b() {
         private final co.uk.getmondo.common.h.a.a c;

         {
            this.c = var1.b;
         }

         public MonzoApi a() {
            return (MonzoApi)b.a.d.a(this.c.D(), "Cannot return null from a non-@Nullable component method");
         }

         // $FF: synthetic method
         public Object b() {
            return this.a();
         }
      };
      this.s = new b.a.b() {
         private final co.uk.getmondo.common.h.a.a c;

         {
            this.c = var1.b;
         }

         public co.uk.getmondo.settings.k a() {
            return (co.uk.getmondo.settings.k)b.a.d.a(this.c.x(), "Cannot return null from a non-@Nullable component method");
         }

         // $FF: synthetic method
         public Object b() {
            return this.a();
         }
      };
      this.t = co.uk.getmondo.news.j.a(this.r, this.s);
      this.u = new b.a.b() {
         private final co.uk.getmondo.common.h.a.a c;

         {
            this.c = var1.b;
         }

         public co.uk.getmondo.feed.a.d a() {
            return (co.uk.getmondo.feed.a.d)b.a.d.a(this.c.n(), "Cannot return null from a non-@Nullable component method");
         }

         // $FF: synthetic method
         public Object b() {
            return this.a();
         }
      };
      this.v = new b.a.b() {
         private final co.uk.getmondo.common.h.a.a c;

         {
            this.c = var1.b;
         }

         public co.uk.getmondo.a.a a() {
            return (co.uk.getmondo.a.a)b.a.d.a(this.c.m(), "Cannot return null from a non-@Nullable component method");
         }

         // $FF: synthetic method
         public Object b() {
            return this.a();
         }
      };
      this.w = new b.a.b() {
         private final co.uk.getmondo.common.h.a.a c;

         {
            this.c = var1.b;
         }

         public co.uk.getmondo.common.accounts.b a() {
            return (co.uk.getmondo.common.accounts.b)b.a.d.a(this.c.c(), "Cannot return null from a non-@Nullable component method");
         }

         // $FF: synthetic method
         public Object b() {
            return this.a();
         }
      };
      this.x = new b.a.b() {
         private final co.uk.getmondo.common.h.a.a c;

         {
            this.c = var1.b;
         }

         public OverdraftApi a() {
            return (OverdraftApi)b.a.d.a(this.c.M(), "Cannot return null from a non-@Nullable component method");
         }

         // $FF: synthetic method
         public Object b() {
            return this.a();
         }
      };
      this.y = co.uk.getmondo.overdraft.a.e.a(this.x, co.uk.getmondo.overdraft.a.g.c());
      this.z = co.uk.getmondo.overdraft.a.b.a(this.c);
      this.A = co.uk.getmondo.background_sync.f.a(this.h, this.u, this.v, this.w, this.y, this.z);
      this.B = new b.a.b() {
         private final co.uk.getmondo.common.h.a.a c;

         {
            this.c = var1.b;
         }

         public IdentityVerificationApi a() {
            return (IdentityVerificationApi)b.a.d.a(this.c.E(), "Cannot return null from a non-@Nullable component method");
         }

         // $FF: synthetic method
         public Object b() {
            return this.a();
         }
      };
      this.C = new b.a.b() {
         private final co.uk.getmondo.common.h.a.a c;

         {
            this.c = var1.b;
         }

         public co.uk.getmondo.main.h a() {
            return (co.uk.getmondo.main.h)b.a.d.a(this.c.A(), "Cannot return null from a non-@Nullable component method");
         }

         // $FF: synthetic method
         public Object b() {
            return this.a();
         }
      };
      this.D = new b.a.b() {
         private final co.uk.getmondo.common.h.a.a c;

         {
            this.c = var1.b;
         }

         public co.uk.getmondo.payments.send.a.e a() {
            return (co.uk.getmondo.payments.send.a.e)b.a.d.a(this.c.t(), "Cannot return null from a non-@Nullable component method");
         }

         // $FF: synthetic method
         public Object b() {
            return this.a();
         }
      };
      this.E = b.a.a.a(co.uk.getmondo.feed.a.u.a(this.D, w.c()));
      this.F = co.uk.getmondo.card.d.a(this.w, this.r, co.uk.getmondo.card.t.c());
      this.G = new b.a.b() {
         private final co.uk.getmondo.common.h.a.a c;

         {
            this.c = var1.b;
         }

         public SignupApi a() {
            return (SignupApi)b.a.d.a(this.c.I(), "Cannot return null from a non-@Nullable component method");
         }

         // $FF: synthetic method
         public Object b() {
            return this.a();
         }
      };
      this.H = new b.a.b() {
         private final co.uk.getmondo.common.h.a.a c;

         {
            this.c = var1.b;
         }

         public MigrationApi a() {
            return (MigrationApi)b.a.d.a(this.c.L(), "Cannot return null from a non-@Nullable component method");
         }

         // $FF: synthetic method
         public Object b() {
            return this.a();
         }
      };
      this.I = co.uk.getmondo.signup.status.h.a(this.c);
      this.J = co.uk.getmondo.migration.e.a(this.c);
      this.K = co.uk.getmondo.signup.status.c.a(this.G, this.H, this.l, this.I, this.J);
      this.L = b.a.a.a(co.uk.getmondo.main.e.a(b.a.c.a(), this.h, this.i, this.j, this.k, this.l, this.e, this.m, this.n, this.o, this.q, this.t, this.A, ab.c(), this.B, this.C, this.E, co.uk.getmondo.common.p.c(), this.F, this.K));
      this.M = new b.a.b() {
         private final co.uk.getmondo.common.h.a.a c;

         {
            this.c = var1.b;
         }

         public co.uk.getmondo.common.q a() {
            return (co.uk.getmondo.common.q)b.a.d.a(this.c.p(), "Cannot return null from a non-@Nullable component method");
         }

         // $FF: synthetic method
         public Object b() {
            return this.a();
         }
      };
      this.N = new b.a.b() {
         private final co.uk.getmondo.common.h.a.a c;

         {
            this.c = var1.b;
         }

         public co.uk.getmondo.common.a.c a() {
            return (co.uk.getmondo.common.a.c)b.a.d.a(this.c.u(), "Cannot return null from a non-@Nullable component method");
         }

         // $FF: synthetic method
         public Object b() {
            return this.a();
         }
      };
      this.O = co.uk.getmondo.main.b.a(this.L, this.M, this.N);
      this.P = co.uk.getmondo.developer_options.b.a(this.c);
      this.Q = b.a.a.a(co.uk.getmondo.card.h.a(b.a.c.a(), this.h, this.i, this.w, this.k, this.F, this.v, this.e, this.P, this.y));
      this.R = co.uk.getmondo.card.b.a(this.Q);
      this.S = co.uk.getmondo.signup.identity_verification.d.a(this.e);
      this.T = b.a.a.a(co.uk.getmondo.welcome.i.a(b.a.c.a()));
      this.U = co.uk.getmondo.welcome.d.a(this.T, this.f);
      this.V = b.a.a.a(co.uk.getmondo.signup_old.ab.a(b.a.c.a(), this.h, this.i, this.k, this.l, this.e));
      this.W = co.uk.getmondo.signup_old.v.a(this.V);
      this.X = new b.a.b() {
         private final co.uk.getmondo.common.h.a.a c;

         {
            this.c = var1.b;
         }

         public co.uk.getmondo.common.s a() {
            return (co.uk.getmondo.common.s)b.a.d.a(this.c.d(), "Cannot return null from a non-@Nullable component method");
         }

         // $FF: synthetic method
         public Object b() {
            return this.a();
         }
      };
      this.Y = new b.a.b() {
         private final co.uk.getmondo.common.h.a.a c;

         {
            this.c = var1.b;
         }

         public co.uk.getmondo.common.m a() {
            return (co.uk.getmondo.common.m)b.a.d.a(this.c.r(), "Cannot return null from a non-@Nullable component method");
         }

         // $FF: synthetic method
         public Object b() {
            return this.a();
         }
      };
      this.Z = new b.a.b() {
         private final co.uk.getmondo.common.h.a.a c;

         {
            this.c = var1.b;
         }

         public IntercomPushClient a() {
            return (IntercomPushClient)b.a.d.a(this.c.q(), "Cannot return null from a non-@Nullable component method");
         }

         // $FF: synthetic method
         public Object b() {
            return this.a();
         }
      };
      this.aa = co.uk.getmondo.fcm.f.a(this.c, this.Z);
      this.ab = b.a.a.a(co.uk.getmondo.signup_old.q.a(b.a.c.a(), this.h, this.i, this.k, this.j, this.X, this.Y, this.e, this.aa, this.l));
      this.ac = co.uk.getmondo.signup_old.e.a(this.ab);
      this.ad = b.a.a.a(co.uk.getmondo.signup.h.a(b.a.c.a(), this.h, this.i, this.k, co.uk.getmondo.common.l.c(), this.l, this.e, this.aa));
      this.ae = co.uk.getmondo.signup.e.a(co.uk.getmondo.common.l.c(), this.X, this.ad);
      this.af = co.uk.getmondo.waitlist.a.a(this.e, this.j);
      this.ag = new b.a.b() {
         private final co.uk.getmondo.common.h.a.a c;

         {
            this.c = var1.b;
         }

         public co.uk.getmondo.waitlist.i a() {
            return (co.uk.getmondo.waitlist.i)b.a.d.a(this.c.k(), "Cannot return null from a non-@Nullable component method");
         }

         // $FF: synthetic method
         public Object b() {
            return this.a();
         }
      };
      this.ah = b.a.a.a(co.uk.getmondo.waitlist.h.a(b.a.c.a(), this.h, this.i, this.k, this.j, this.l, this.X, this.ag, this.e));
      this.ai = co.uk.getmondo.waitlist.b.a(this.ah);
      this.aj = new b.a.b() {
         private final co.uk.getmondo.common.h.a.a c;

         {
            this.c = var1.b;
         }

         public co.uk.getmondo.transaction.a.a a() {
            return (co.uk.getmondo.transaction.a.a)b.a.d.a(this.c.o(), "Cannot return null from a non-@Nullable component method");
         }

         // $FF: synthetic method
         public Object b() {
            return this.a();
         }
      };
      this.ak = co.uk.getmondo.common.k.m.a(this.c);
      this.al = b.a.a.a(co.uk.getmondo.transaction.attachment.t.a(b.a.c.a(), this.h, this.i, this.j, this.aj, this.ak, this.e, this.k));
      this.am = co.uk.getmondo.transaction.attachment.b.a(this.al);
      this.an = b.a.a.a(co.uk.getmondo.top.e.a(b.a.c.a(), this.h, this.i, this.j, this.k, this.X, this.l, this.e));
      this.ao = co.uk.getmondo.top.a.a(this.an);
      this.ap = new b.a.b() {
         private final co.uk.getmondo.common.h.a.a c;

         {
            this.c = var1.b;
         }

         public com.google.gson.f a() {
            return (com.google.gson.f)b.a.d.a(this.c.O(), "Cannot return null from a non-@Nullable component method");
         }

         // $FF: synthetic method
         public Object b() {
            return this.a();
         }
      };
      this.aq = b.a.a.a(co.uk.getmondo.bump_up.k.a(b.a.c.a(), this.j, this.ap));
      this.ar = co.uk.getmondo.bump_up.c.a(this.e, this.aq, co.uk.getmondo.common.l.c());
      this.as = co.uk.getmondo.topup.a.t.a(this.r, this.i, co.uk.getmondo.topup.a.b.c(), this.d, co.uk.getmondo.topup.a.u.c());
      this.at = b.a.a.a(f.a(var1.a, this.e));
      this.au = b.a.a.a(co.uk.getmondo.topup.card.l.a(b.a.c.a(), this.h, this.i, this.k, this.j, this.e, this.as, x.c(), this.at));
      this.av = co.uk.getmondo.topup.card.g.a(this.e, this.au, this.at);
      this.aw = co.uk.getmondo.topup.three_d_secure.a.a(this.e);
      this.ax = co.uk.getmondo.bump_up.a.a(this.j);
      this.ay = b.a.a.a(co.uk.getmondo.topup.card.f.a(b.a.c.a(), this.h, this.i, this.k, this.j, this.e, this.as, x.c()));
      this.az = co.uk.getmondo.topup.card.a.a(this.ay, this.e);
      this.aA = b.a.a.a(co.uk.getmondo.common.address.h.a(b.a.c.a(), this.e));
      this.aB = co.uk.getmondo.common.address.d.a(this.aA);
      this.aC = b.a.a.a(co.uk.getmondo.create_account.phone_number.r.a(b.a.c.a(), this.h, this.i, this.j, this.r, this.e, this.k));
      this.aD = co.uk.getmondo.create_account.phone_number.j.a(this.aC);
      this.aE = b.a.a.a(af.a(b.a.c.a(), this.h, this.i, this.k, this.j, this.r, this.e));
      this.aF = co.uk.getmondo.create_account.phone_number.w.a(this.aE);
      this.aG = co.uk.getmondo.create_account.topup.p.a(this.c);
      this.aH = co.uk.getmondo.create_account.topup.f.a(this.j, this.r);
      this.aI = b.a.a.a(co.uk.getmondo.create_account.topup.n.a(b.a.c.a(), this.h, this.i, this.j, this.k, this.M, this.aG, this.e, this.Y, this.aH));
      this.aJ = co.uk.getmondo.create_account.topup.b.a(this.aI);
      this.aK = b.a.a.a(y.a(b.a.c.a(), this.h, this.i, this.k, this.e, this.l, this.aH));
      this.aL = co.uk.getmondo.create_account.topup.q.a(this.aK);
      this.aM = b.a.a.a(co.uk.getmondo.create_account.wait.f.a(b.a.c.a(), this.h, this.i, this.j, this.k, this.r, this.e, this.M, this.ag));
      this.aN = co.uk.getmondo.create_account.wait.b.a(this.aM);
      this.aO = co.uk.getmondo.feed.a.c.a(this.c);
      this.aP = co.uk.getmondo.feed.adapter.b.a(b.a.c.a(), this.aO);
      this.aQ = new b.a.b() {
         private final co.uk.getmondo.common.h.a.a c;

         {
            this.c = var1.b;
         }

         public co.uk.getmondo.common.i a() {
            return (co.uk.getmondo.common.i)b.a.d.a(this.c.j(), "Cannot return null from a non-@Nullable component method");
         }

         // $FF: synthetic method
         public Object b() {
            return this.a();
         }
      };
      this.aR = b.a.a.a(co.uk.getmondo.feed.h.a(b.a.c.a(), this.h, this.i, this.k, this.aQ, this.u, this.A, this.F, this.v, this.w));
      this.aS = b.a.a.a(co.uk.getmondo.feed.d.a(this.b));
      this.aT = co.uk.getmondo.feed.f.a(this.aP, this.aR, this.aS);
      this.aU = b.a.a.a(co.uk.getmondo.force_upgrade.f.a(b.a.c.a(), this.e));
      this.aV = co.uk.getmondo.force_upgrade.b.a(this.aU);
      this.aW = b.a.a.a(co.uk.getmondo.top.k.a(b.a.c.a(), this.h, this.i, this.j, this.k, this.l, this.e, this.ag, this.aH));
   }

   private void b(final a.d var1) {
      this.aX = co.uk.getmondo.top.f.a(this.aW);
      this.aY = ap.a(this.c);
      this.aZ = co.uk.getmondo.topup.bank.c.a(b.a.c.a(), this.j, this.e, this.F, this.aY);
      this.ba = co.uk.getmondo.topup.bank.a.a(this.aZ);
      this.bb = b.a.a.a(an.a(b.a.c.a(), this.h, this.i, this.j, this.k, this.e, this.r, this.aY, this.v, this.as, x.c()));
      this.bc = co.uk.getmondo.topup.p.a(this.bb, this.M, this.N, co.uk.getmondo.topup.d.c(), this.e);
      this.bd = b.a.a.a(co.uk.getmondo.transaction.attachment.l.a(b.a.c.a(), this.h, this.i, this.k, this.aj));
      this.be = co.uk.getmondo.transaction.attachment.d.a(this.bd);
      this.bf = b.a.a.a(co.uk.getmondo.card.r.a(b.a.c.a(), this.h, this.i, this.j, this.k, this.F, this.e));
      this.bg = co.uk.getmondo.card.i.a(this.bf);
      this.bh = new b.a.b() {
         private final co.uk.getmondo.common.h.a.a c;

         {
            this.c = var1.b;
         }

         public io.reactivex.u a() {
            return (io.reactivex.u)b.a.d.a(this.c.g(), "Cannot return null from a non-@Nullable component method");
         }

         // $FF: synthetic method
         public Object b() {
            return this.a();
         }
      };
      this.bi = new b.a.b() {
         private final co.uk.getmondo.common.h.a.a c;

         {
            this.c = var1.b;
         }

         public co.uk.getmondo.transaction.a.j a() {
            return (co.uk.getmondo.transaction.a.j)b.a.d.a(this.c.C(), "Cannot return null from a non-@Nullable component method");
         }

         // $FF: synthetic method
         public Object b() {
            return this.a();
         }
      };
      this.bj = co.uk.getmondo.spending.a.t.a(this.bi);
      this.bk = b.a.a.a(co.uk.getmondo.payments.a.g.c());
      this.bl = b.a.a.a(co.uk.getmondo.payments.a.c.c());
      this.bm = b.a.a.a(co.uk.getmondo.payments.a.j.a(this.bk, this.bl, this.p, this.j));
      this.bn = b.a.a.a(co.uk.getmondo.spending.f.a(b.a.c.a(), this.i, this.h, this.bh, this.bj, co.uk.getmondo.spending.a.f.c(), this.e, this.bm, this.k, this.F, this.w));
      this.bo = co.uk.getmondo.spending.c.a(this.bn);
      this.bp = b.a.a.a(co.uk.getmondo.payments.send.onboarding.l.a(b.a.c.a(), this.h, this.i, this.k, this.n, this.e));
      this.bq = co.uk.getmondo.payments.send.onboarding.a.a(this.bp);
      this.br = b.a.a.a(co.uk.getmondo.payments.send.onboarding.u.a(b.a.c.a(), this.h, this.i, this.k, this.n, this.e));
      this.bs = co.uk.getmondo.payments.send.onboarding.m.a(this.br);
      this.bt = b.a.a.a(co.uk.getmondo.payments.send.data.d.a(this.p, this.j, this.bm, co.uk.getmondo.payments.send.data.g.c()));
      this.bu = b.a.a.a(co.uk.getmondo.payments.send.p.a(b.a.c.a(), this.h, this.i, this.k, this.j, co.uk.getmondo.common.k.g.c(), this.e, this.r, this.D, this.q, this.bt, this.w, co.uk.getmondo.common.p.c()));
      this.bv = b.a.a.a(co.uk.getmondo.payments.send.g.a(b.a.c.a(), this.b));
      this.bw = co.uk.getmondo.payments.send.n.a(this.bu, this.e, this.bv);
      this.bx = b.a.a.a(co.uk.getmondo.splash.c.a(b.a.c.a(), this.j, this.K));
      this.by = co.uk.getmondo.splash.a.a(this.bx, this.M, this.N);
      this.bz = new b.a.b() {
         private final co.uk.getmondo.common.h.a.a c;

         {
            this.c = var1.b;
         }

         public co.uk.getmondo.common.accounts.o a() {
            return (co.uk.getmondo.common.accounts.o)b.a.d.a(this.c.y(), "Cannot return null from a non-@Nullable component method");
         }

         // $FF: synthetic method
         public Object b() {
            return this.a();
         }
      };
      this.bA = b.a.a.a(co.uk.getmondo.settings.w.a(b.a.c.a(), this.h, this.i, this.j, this.e, this.n, this.r, this.k, this.o, this.bz, this.s, this.l, co.uk.getmondo.common.p.c()));
      this.bB = co.uk.getmondo.settings.t.a(this.bA);
      this.bC = new b.a.b() {
         private final co.uk.getmondo.common.h.a.a c;

         {
            this.c = var1.b;
         }

         public PaymentLimitsApi a() {
            return (PaymentLimitsApi)b.a.d.a(this.c.N(), "Cannot return null from a non-@Nullable component method");
         }

         // $FF: synthetic method
         public Object b() {
            return this.a();
         }
      };
      this.bD = new b.a.b() {
         private final co.uk.getmondo.common.h.a.a c;

         {
            this.c = var1.b;
         }

         public co.uk.getmondo.signup.identity_verification.a.h a() {
            return (co.uk.getmondo.signup.identity_verification.a.h)b.a.d.a(this.c.w(), "Cannot return null from a non-@Nullable component method");
         }

         // $FF: synthetic method
         public Object b() {
            return this.a();
         }
      };
      this.bE = b.a.a.a(co.uk.getmondo.signup.identity_verification.a.g.a(this.bD));
      this.bF = b.a.a.a(co.uk.getmondo.settings.j.a(b.a.c.a(), this.h, this.i, this.j, this.r, this.bC, this.e, this.k, this.bE));
      this.bG = co.uk.getmondo.settings.e.a(this.bF);
      this.bH = b.a.a.a(co.uk.getmondo.monzo.me.onboarding.i.a(b.a.c.a(), this.s));
      this.bI = co.uk.getmondo.monzo.me.onboarding.d.a(this.bH, this.f);
      this.bJ = b.a.a.a(co.uk.getmondo.monzo.me.request.h.a(b.a.c.a(), this.e, this.n, this.j, co.uk.getmondo.monzo.me.b.c(), this.s));
      this.bK = co.uk.getmondo.monzo.me.request.a.a(this.bJ);
      this.bL = co.uk.getmondo.feed.j.a(this.e);
      this.bM = b.a.a.a(co.uk.getmondo.pin.a.h.a(this.j, this.r));
      this.bN = b.a.a.a(co.uk.getmondo.pin.w.a(b.a.c.a(), this.h, this.i, this.bh, this.bM, this.k, co.uk.getmondo.signup_old.t.c(), this.e));
      this.bO = co.uk.getmondo.pin.d.a(this.bN, this.M);
      this.bP = co.uk.getmondo.main.a.a(this.e);
      this.bQ = b.a.a.a(co.uk.getmondo.feed.search.n.a(b.a.c.a(), this.h, this.i, this.k, this.u, this.e));
      this.bR = co.uk.getmondo.feed.search.f.a(this.aP, this.aS, this.bQ);
      this.bS = b.a.a.a(co.uk.getmondo.signup.identity_verification.chat_with_us.c.a(b.a.c.a(), this.M));
      this.bT = co.uk.getmondo.signup.identity_verification.chat_with_us.a.a(this.bS);
      this.bU = co.uk.getmondo.signup.identity_verification.c.a(this.e);
      this.bV = b.a.a.a(co.uk.getmondo.create_account.k.a(b.a.c.a(), this.h, this.i, this.j, this.k, this.l, this.e));
      this.bW = co.uk.getmondo.create_account.f.a(this.bV);
      this.bX = b.a.a.a(co.uk.getmondo.c.d.a(b.a.c.a(), this.i, this.h, this.e, this.w, this.v, this.k, this.F, co.uk.getmondo.common.p.c()));
      this.bY = co.uk.getmondo.c.b.a(this.bX);
      this.bZ = co.uk.getmondo.common.d.d.a(this.M);
      this.ca = new b.a.b() {
         private final co.uk.getmondo.common.h.a.a c;

         {
            this.c = var1.b;
         }

         public MonzoProfileApi a() {
            return (MonzoProfileApi)b.a.d.a(this.c.H(), "Cannot return null from a non-@Nullable component method");
         }

         // $FF: synthetic method
         public Object b() {
            return this.a();
         }
      };
      this.cb = co.uk.getmondo.profile.data.c.a(this.r, this.ca, this.j, co.uk.getmondo.topup.a.b.c());
      this.cc = b.a.a.a(co.uk.getmondo.profile.address.m.a(b.a.c.a(), this.cb, this.i, this.h, this.k, this.e));
      this.cd = co.uk.getmondo.profile.address.j.a(this.cc);
      this.ce = co.uk.getmondo.profile.address.o.a(this.M, this.e);
      this.cf = new b.a.b() {
         private final co.uk.getmondo.common.h.a.a c;

         {
            this.c = var1.b;
         }

         public HelpApi a() {
            return (HelpApi)b.a.d.a(this.c.K(), "Cannot return null from a non-@Nullable component method");
         }

         // $FF: synthetic method
         public Object b() {
            return this.a();
         }
      };
      this.cg = new b.a.b() {
         private final co.uk.getmondo.common.h.a.a c;

         {
            this.c = var1.b;
         }

         public Resources a() {
            return (Resources)b.a.d.a(this.c.B(), "Cannot return null from a non-@Nullable component method");
         }

         // $FF: synthetic method
         public Object b() {
            return this.a();
         }
      };
      this.ch = co.uk.getmondo.common.w.a(this.cg);
      this.ci = co.uk.getmondo.help.a.b.a(this.cf, this.ch);
      this.cj = b.a.a.a(co.uk.getmondo.help.h.a(b.a.c.a(), this.i, this.h, this.ci, this.k, this.e));
      this.ck = co.uk.getmondo.help.a.a(this.cj, this.M);
      this.cl = b.a.a.a(co.uk.getmondo.help.l.a(b.a.c.a(), this.i, this.h, this.ci, this.k));
      this.cm = co.uk.getmondo.help.i.a(this.cl, this.M);
      this.cn = co.uk.getmondo.signup.marketing_opt_in.a.a(this.M);
      this.co = new b.a.b() {
         private final co.uk.getmondo.common.h.a.a c;

         {
            this.c = var1.b;
         }

         public TaxResidencyApi a() {
            return (TaxResidencyApi)b.a.d.a(this.c.G(), "Cannot return null from a non-@Nullable component method");
         }

         // $FF: synthetic method
         public Object b() {
            return this.a();
         }
      };
      this.cp = co.uk.getmondo.signup.tax_residency.a.d.a(this.co);
      this.cq = b.a.a.a(co.uk.getmondo.signup.tax_residency.e.a(b.a.c.a(), this.i, this.h, this.cp, this.k));
      this.cr = co.uk.getmondo.signup.tax_residency.a.a(this.M, this.cq);
      this.cs = b.a.a.a(co.uk.getmondo.signup.tax_residency.b.f.a(b.a.c.a(), this.i, this.h, this.cp, this.k, this.e));
      this.ct = co.uk.getmondo.signup.tax_residency.b.c.a(this.cs);
      this.cu = b.a.a.a(co.uk.getmondo.signup.tax_residency.b.p.a(b.a.c.a(), this.i, this.h, this.cp, this.k, this.e));
      this.cv = co.uk.getmondo.signup.tax_residency.b.m.a(this.cu);
      this.cw = b.a.a.a(co.uk.getmondo.signup.card_activation.h.a(b.a.c.a(), this.e, this.s));
      this.cx = co.uk.getmondo.signup.card_activation.f.a(this.M, this.cw);
      this.cy = co.uk.getmondo.signup.status.f.a(b.a.c.a(), this.h, this.i, this.K, this.k, this.l);
      this.cz = co.uk.getmondo.signup.status.a.a(this.cy);
      this.cA = b.a.a.a(co.uk.getmondo.signup.tax_residency.b.k.a(b.a.c.a(), this.i, this.h, this.cp, this.k, this.e));
      this.cB = co.uk.getmondo.signup.tax_residency.b.h.a(this.e, this.cA);
      this.cC = b.a.a.a(co.uk.getmondo.signup.tax_residency.b.u.a(b.a.c.a(), this.i, this.h, this.cp, this.k, this.e));
      this.cD = co.uk.getmondo.signup.tax_residency.b.r.a(this.cC);
      this.cE = co.uk.getmondo.signup.phone_verification.d.a(this.G);
      this.cF = co.uk.getmondo.signup.phone_verification.i.a(b.a.c.a(), this.h, this.i, this.cE, this.k, this.e);
      this.cG = co.uk.getmondo.signup.phone_verification.f.a(this.cF);
      this.cH = co.uk.getmondo.signup.phone_verification.n.a(b.a.c.a(), this.h, this.i, this.cE, this.k, this.e);
      this.cI = co.uk.getmondo.signup.phone_verification.k.a(this.cH);
      this.cJ = co.uk.getmondo.signup.profile.r.a(this.G);
      this.cK = co.uk.getmondo.signup.profile.p.a(b.a.c.a(), this.h, this.i, this.cJ, this.k, this.e);
      this.cL = co.uk.getmondo.signup.profile.m.a(this.cK);
      this.cM = b.a.a.a(co.uk.getmondo.signup.profile.h.a(b.a.c.a(), this.i, this.h, this.k, this.cJ, this.e));
      this.cN = co.uk.getmondo.signup.profile.f.a(this.cM);
      this.cO = b.a.a.a(co.uk.getmondo.signup.profile.d.a(b.a.c.a(), this.i, this.h, this.k, this.cJ, this.e));
      this.cP = co.uk.getmondo.signup.profile.a.a(this.M, this.cO);
      this.cQ = co.uk.getmondo.signup.marketing_opt_in.c.a(this.G);
      this.cR = co.uk.getmondo.signup.marketing_opt_in.g.a(b.a.c.a(), this.h, this.i, this.e, this.cQ, this.k);
      this.cS = co.uk.getmondo.signup.marketing_opt_in.e.a(this.cR);
   }

   private void c(final a.d var1) {
      this.cT = co.uk.getmondo.signup.documents.e.a(this.G);
      this.cU = co.uk.getmondo.signup.documents.g.a(b.a.c.a(), this.h, this.i, this.e, this.cT, this.k);
      this.cV = co.uk.getmondo.signup.documents.c.a(this.cU);
      this.cW = co.uk.getmondo.signup.card_ordering.j.a(this.M);
      this.cX = co.uk.getmondo.signup.card_ordering.l.a(this.G);
      this.cY = b.a.a.a(co.uk.getmondo.signup.card_ordering.i.a(b.a.c.a(), this.i, this.h, this.cX, this.k, this.e));
      this.cZ = co.uk.getmondo.signup.card_ordering.f.a(this.cY);
      this.da = b.a.a.a(co.uk.getmondo.signup.card_ordering.d.a(b.a.c.a(), this.i, this.h, this.cX, this.k, this.e));
      this.db = co.uk.getmondo.signup.card_ordering.b.a(this.da);
      this.dc = co.uk.getmondo.signup.card_activation.e.a(this.G);
      this.dd = b.a.a.a(co.uk.getmondo.signup.card_activation.c.a(b.a.c.a(), this.h, this.i, this.dc, this.k, this.e));
      this.de = co.uk.getmondo.signup.card_activation.a.a(this.dd, this.M);
      this.df = b.a.a.a(co.uk.getmondo.signup.pending.c.a(b.a.c.a(), this.h, this.i, this.K, this.k, this.e));
      this.dg = co.uk.getmondo.signup.pending.a.a(this.M, this.df);
      this.dh = co.uk.getmondo.signup.rejected.a.a(this.e);
      this.di = co.uk.getmondo.signup.profile.i.a(this.M);
      this.dj = co.uk.getmondo.signup.phone_verification.a.a(this.M);
      this.dk = co.uk.getmondo.signup.documents.a.a(this.M);
      this.dl = co.uk.getmondo.terms_and_conditions.j.a(b.a.c.a());
      this.dm = co.uk.getmondo.terms_and_conditions.f.a(this.dl);
      this.dn = b.a.a.a(co.uk.getmondo.payments.send.bank.g.a(b.a.c.a(), this.h, this.v, this.j));
      this.do = co.uk.getmondo.payments.send.bank.a.a(this.dn);
      this.dp = b.a.a.a(co.uk.getmondo.payments.send.bank.payee.v.a(b.a.c.a(), this.h, this.i, this.bt, this.k));
      this.dq = co.uk.getmondo.payments.send.bank.payee.f.a(this.dp);
      this.dr = co.uk.getmondo.payments.recurring_list.g.a(b.a.c.a(), this.bm);
      this.ds = co.uk.getmondo.payments.recurring_list.b.a(b.a.c.a(), this.b);
      this.dt = co.uk.getmondo.payments.recurring_list.e.a(this.dr, this.ds);
      this.du = b.a.a.a(co.uk.getmondo.feed.welcome.c.a(b.a.c.a(), this.e, this.j));
      this.dv = co.uk.getmondo.feed.welcome.a.a(this.du);
      this.dw = co.uk.getmondo.migration.c.a(b.a.c.a(), this.e, this.J);
      this.dx = co.uk.getmondo.migration.a.a(this.dw);
      this.dy = b.a.a.a(co.uk.getmondo.spending.b.d.a(b.a.c.a(), this.i, this.h, this.bj, this.ch, co.uk.getmondo.spending.b.a.c.c(), this.e));
      this.dz = co.uk.getmondo.spending.b.b.a(this.dy);
      this.dA = co.uk.getmondo.migration.f.a(this.M, this.f);
      this.dB = b.a.a.a(co.uk.getmondo.overdraft.c.a(b.a.c.a(), this.i, this.h, this.w, this.y, this.k));
      this.dC = co.uk.getmondo.overdraft.a.a(this.dB);
      this.dD = b.a.a.a(co.uk.getmondo.pots.d.a(b.a.c.a(), this.ch));
      this.dE = co.uk.getmondo.pots.a.a(this.dD);
      this.dF = b.a.a.a(co.uk.getmondo.pots.g.a(b.a.c.a()));
      this.dG = co.uk.getmondo.pots.e.a(this.dF);
      this.dH = new b.a.b() {
         private final co.uk.getmondo.common.h.a.a c;

         {
            this.c = var1.b;
         }

         public co.uk.getmondo.payments.send.payment_category.b.a a() {
            return (co.uk.getmondo.payments.send.payment_category.b.a)b.a.d.a(this.c.s(), "Cannot return null from a non-@Nullable component method");
         }

         // $FF: synthetic method
         public Object b() {
            return this.a();
         }
      };
      this.dI = new b.a.b() {
         private final co.uk.getmondo.common.h.a.a c;

         {
            this.c = var1.b;
         }

         public ae a() {
            return (ae)b.a.d.a(this.c.P(), "Cannot return null from a non-@Nullable component method");
         }

         // $FF: synthetic method
         public Object b() {
            return this.a();
         }
      };
   }

   public co.uk.getmondo.card.activate.b a(co.uk.getmondo.card.activate.c var1) {
      return new a.a(var1, null);
   }

   public co.uk.getmondo.common.address.m a(co.uk.getmondo.common.address.n var1) {
      return new a.r(var1, null);
   }

   public co.uk.getmondo.common.pin.a a(co.uk.getmondo.common.pin.g var1) {
      return new a.p(var1, null);
   }

   public co.uk.getmondo.golden_ticket.c a(co.uk.getmondo.golden_ticket.d var1) {
      return new a.f(var1, null);
   }

   public co.uk.getmondo.help.b.a a(co.uk.getmondo.help.b.b var1) {
      return new a.g(var1, null);
   }

   public co.uk.getmondo.help.b.e a(co.uk.getmondo.help.b.f var1) {
      return new a.h(var1, null);
   }

   public co.uk.getmondo.monzo.me.customise.d a(co.uk.getmondo.monzo.me.customise.e var1) {
      return new a.e(var1, null);
   }

   public co.uk.getmondo.monzo.me.deeplink.b a(co.uk.getmondo.monzo.me.deeplink.c var1) {
      return new a.k(var1, null);
   }

   public co.uk.getmondo.news.b a(co.uk.getmondo.news.k var1) {
      return new a.l(var1, null);
   }

   public co.uk.getmondo.payments.recurring_cancellation.e a(co.uk.getmondo.payments.recurring_cancellation.f var1) {
      return new a.q(var1, null);
   }

   public co.uk.getmondo.payments.send.authentication.e a(co.uk.getmondo.payments.send.authentication.f var1) {
      return new a.m(var1, null);
   }

   public co.uk.getmondo.payments.send.bank.payment.h a(co.uk.getmondo.payments.send.bank.payment.i var1) {
      return new a.c(var1, null);
   }

   public co.uk.getmondo.payments.send.payment_category.l a(co.uk.getmondo.payments.send.payment_category.m var1) {
      return new a.n(var1, null);
   }

   public co.uk.getmondo.payments.send.peer.e a(co.uk.getmondo.payments.send.peer.f var1) {
      return new a.o(var1, null);
   }

   public co.uk.getmondo.profile.address.b a(co.uk.getmondo.profile.address.c var1) {
      return new a.b(var1, null);
   }

   public co.uk.getmondo.signup.identity_verification.o a(co.uk.getmondo.signup.identity_verification.p var1) {
      return new a.i(var1, null);
   }

   public co.uk.getmondo.signup.identity_verification.sdd.b a(co.uk.getmondo.signup.identity_verification.sdd.c var1) {
      return new a.j(var1, null);
   }

   public co.uk.getmondo.signup.identity_verification.video.e a(co.uk.getmondo.signup.identity_verification.video.f var1) {
      return new a.v(var1, null);
   }

   public co.uk.getmondo.spending.merchant.a a(co.uk.getmondo.spending.merchant.f var1) {
      return new a.s(var1, null);
   }

   public co.uk.getmondo.spending.transactions.b a(co.uk.getmondo.spending.transactions.d var1) {
      return new a.t(var1, null);
   }

   public co.uk.getmondo.transaction.c a(co.uk.getmondo.transaction.d var1) {
      return new a.u(var1, null);
   }

   public void a(BumpUpActivity var1) {
      this.ax.a(var1);
   }

   public void a(WebEventActivity var1) {
      this.ar.a(var1);
   }

   public void a(co.uk.getmondo.c.a var1) {
      this.bY.a(var1);
   }

   public void a(CardReplacementActivity var1) {
      this.bg.a(var1);
   }

   public void a(co.uk.getmondo.card.a var1) {
      this.R.a(var1);
   }

   public void a(LegacyEnterAddressActivity var1) {
      this.aB.a(var1);
   }

   public void a(co.uk.getmondo.common.d.c var1) {
      this.bZ.a(var1);
   }

   public void a(VerifyIdentityActivity var1) {
      this.bW.a(var1);
   }

   public void a(EnterCodeActivity var1) {
      this.aD.a(var1);
   }

   public void a(EnterPhoneNumberActivity var1) {
      this.aF.a(var1);
   }

   public void a(InitialTopupActivity var1) {
      this.aJ.a(var1);
   }

   public void a(TopupInfoActivity var1) {
      this.aL.a(var1);
   }

   public void a(CardShippedActivity var1) {
      this.aN.a(var1);
   }

   public void a(MonthlySpendingReportActivity var1) {
      this.bL.a(var1);
   }

   public void a(co.uk.getmondo.feed.e var1) {
      this.aT.a(var1);
   }

   public void a(FeedSearchActivity var1) {
      this.bR.a(var1);
   }

   public void a(WelcomeToMonzoActivity var1) {
      this.dv.a(var1);
   }

   public void a(ForceUpgradeActivity var1) {
      this.aV.a(var1);
   }

   public void a(HelpActivity var1) {
      this.ck.a(var1);
   }

   public void a(HelpSearchActivity var1) {
      this.cm.a(var1);
   }

   public void a(EddLimitsActivity var1) {
      this.bP.a(var1);
   }

   public void a(HomeActivity var1) {
      this.O.a(var1);
   }

   public void a(MigrationAnnouncementActivity var1) {
      this.dx.a(var1);
   }

   public void a(MigrationTourActivity var1) {
      this.dA.a(var1);
   }

   public void a(MonzoMeOnboardingActivity var1) {
      this.bI.a(var1);
   }

   public void a(RequestMoneyFragment var1) {
      this.bK.a(var1);
   }

   public void a(ChangeOverdraftLimitActivity var1) {
      this.dC.a(var1);
   }

   public void a(RecurringPaymentsActivity var1) {
      this.dt.a(var1);
   }

   public void a(SendMoneyFragment var1) {
      this.bw.a(var1);
   }

   public void a(BankPaymentActivity var1) {
      this.do.a(var1);
   }

   public void a(UkBankPayeeDetailsFragment var1) {
      this.dq.a(var1);
   }

   public void a(PeerToPeerIntroActivity var1) {
      this.bq.a(var1);
   }

   public void a(PeerToPeerMoreInfoActivity var1) {
      this.bs.a(var1);
   }

   public void a(ForgotPinActivity var1) {
      this.bO.a(var1);
   }

   public void a(CreatePotActivity var1) {
      this.dE.a(var1);
   }

   public void a(CustomPotNameActivity var1) {
      this.dG.a(var1);
   }

   public void a(SelectAddressActivity var1) {
      this.cd.a(var1);
   }

   public void a(co.uk.getmondo.profile.address.n var1) {
      this.ce.a(var1);
   }

   public void a(LimitsActivity var1) {
      this.bG.a(var1);
   }

   public void a(SettingsActivity var1) {
      this.bB.a(var1);
   }

   public void a(EmailActivity var1) {
      this.ae.a(var1);
   }

   public void a(ActivateCardActivity var1) {
      this.de.a(var1);
   }

   public void a(CardOnItsWayActivity var1) {
      this.cx.a(var1);
   }

   public void a(OrderCardActivity var1) {
      this.cW.a(var1);
   }

   public void a(co.uk.getmondo.signup.card_ordering.a var1) {
      this.db.a(var1);
   }

   public void a(co.uk.getmondo.signup.card_ordering.e var1) {
      this.cZ.a(var1);
   }

   public void a(LegalDocumentsActivity var1) {
      this.dk.a(var1);
   }

   public void a(co.uk.getmondo.signup.documents.b var1) {
      this.cV.a(var1);
   }

   public void a(CountrySelectionActivity var1) {
      this.bU.a(var1);
   }

   public void a(IdentityApprovedActivity var1) {
      this.S.a(var1);
   }

   public void a(ChatWithUsActivity var1) {
      this.bT.a(var1);
   }

   public void a(co.uk.getmondo.signup.identity_verification.t var1) {
      this.g.a(var1);
   }

   public void a(MarketingOptInActivity var1) {
      this.cn.a(var1);
   }

   public void a(co.uk.getmondo.signup.marketing_opt_in.d var1) {
      this.cS.a(var1);
   }

   public void a(SignupPendingActivity var1) {
      this.dg.a(var1);
   }

   public void a(PhoneVerificationActivity var1) {
      this.dj.a(var1);
   }

   public void a(co.uk.getmondo.signup.phone_verification.e var1) {
      this.cG.a(var1);
   }

   public void a(co.uk.getmondo.signup.phone_verification.j var1) {
      this.cI.a(var1);
   }

   public void a(AddressConfirmationActivity var1) {
      this.cP.a(var1);
   }

   public void a(ProfileCreationActivity var1) {
      this.di.a(var1);
   }

   public void a(co.uk.getmondo.signup.profile.e var1) {
      this.cN.a(var1);
   }

   public void a(co.uk.getmondo.signup.profile.l var1) {
      this.cL.a(var1);
   }

   public void a(SignupRejectedActivity var1) {
      this.dh.a(var1);
   }

   public void a(SignupStatusActivity var1) {
      this.cz.a(var1);
   }

   public void a(TaxResidencyActivity var1) {
      this.cr.a(var1);
   }

   public void a(co.uk.getmondo.signup.tax_residency.b.b var1) {
      this.ct.a(var1);
   }

   public void a(co.uk.getmondo.signup.tax_residency.b.g var1) {
      this.cB.a(var1);
   }

   public void a(co.uk.getmondo.signup.tax_residency.b.l var1) {
      this.cv.a(var1);
   }

   public void a(co.uk.getmondo.signup.tax_residency.b.q var1) {
      this.cD.a(var1);
   }

   public void a(CreateProfileActivity var1) {
      this.ac.a(var1);
   }

   public void a(SignUpActivity var1) {
      this.W.a(var1);
   }

   public void a(co.uk.getmondo.spending.b.a var1) {
      this.dz.a(var1);
   }

   public void a(co.uk.getmondo.spending.b var1) {
      this.bo.a(var1);
   }

   public void a(SplashActivity var1) {
      this.by.a(var1);
   }

   public void a(TermsAndConditionsActivity var1) {
      this.dm.a(var1);
   }

   public void a(NotInCountryActivity var1) {
      this.ao.a(var1);
   }

   public void a(TopActivity var1) {
      this.aX.a(var1);
   }

   public void a(TopUpActivity var1) {
      this.bc.a(var1);
   }

   public void a(TopUpInstructionsActivity var1) {
      this.ba.a(var1);
   }

   public void a(TopUpWithCardFragment var1) {
      this.az.a(var1);
   }

   public void a(TopUpWithNewCardActivity var1) {
      b.a.c.a().a(var1);
   }

   public void a(TopUpWithSavedCardActivity var1) {
      this.av.a(var1);
   }

   public void a(ThreeDsWebActivity var1) {
      this.aw.a(var1);
   }

   public void a(AttachmentActivity var1) {
      this.am.a(var1);
   }

   public void a(AttachmentDetailsActivity var1) {
      this.be.a(var1);
   }

   public void a(ShareActivity var1) {
      this.af.a(var1);
   }

   public void a(WaitlistActivity var1) {
      this.ai.a(var1);
   }

   public void a(WelcomeOnboardingActivity var1) {
      this.U.a(var1);
   }

   private final class a implements co.uk.getmondo.card.activate.b {
      private final co.uk.getmondo.card.activate.c b;
      private javax.a.a c;
      private javax.a.a d;
      private b.a e;

      private a(co.uk.getmondo.card.activate.c var2) {
         this.b = (co.uk.getmondo.card.activate.c)b.a.d.a(var2);
         this.a();
      }

      // $FF: synthetic method
      a(co.uk.getmondo.card.activate.c var2, Object var3) {
         this(var2);
      }

      private void a() {
         this.c = co.uk.getmondo.card.activate.d.a(this.b);
         this.d = b.a.a.a(co.uk.getmondo.card.activate.o.a(b.a.c.a(), a.this.h, a.this.i, a.this.j, a.this.k, a.this.F, a.this.e, a.this.ag, this.c));
         this.e = co.uk.getmondo.card.activate.a.a(this.d);
      }

      public void a(co.uk.getmondo.card.activate.ActivateCardActivity var1) {
         this.e.a(var1);
      }
   }

   private final class b implements co.uk.getmondo.profile.address.b {
      private final co.uk.getmondo.profile.address.c b;
      private javax.a.a c;
      private javax.a.a d;
      private b.a e;

      private b(co.uk.getmondo.profile.address.c var2) {
         this.b = (co.uk.getmondo.profile.address.c)b.a.d.a(var2);
         this.a();
      }

      // $FF: synthetic method
      b(co.uk.getmondo.profile.address.c var2, Object var3) {
         this(var2);
      }

      private void a() {
         this.c = b.a.a.a(co.uk.getmondo.profile.address.d.a(this.b));
         this.d = b.a.a.a(co.uk.getmondo.profile.address.i.a(b.a.c.a(), this.c, a.this.e));
         this.e = co.uk.getmondo.profile.address.f.a(this.d);
      }

      public void a(EnterAddressActivity var1) {
         this.e.a(var1);
      }
   }

   private final class c implements co.uk.getmondo.payments.send.bank.payment.h {
      private final co.uk.getmondo.payments.send.bank.payment.i b;
      private javax.a.a c;
      private javax.a.a d;
      private b.a e;

      private c(co.uk.getmondo.payments.send.bank.payment.i var2) {
         this.b = (co.uk.getmondo.payments.send.bank.payment.i)b.a.d.a(var2);
         this.a();
      }

      // $FF: synthetic method
      c(co.uk.getmondo.payments.send.bank.payment.i var2, Object var3) {
         this(var2);
      }

      private void a() {
         this.c = co.uk.getmondo.payments.send.bank.payment.j.a(this.b);
         this.d = b.a.a.a(co.uk.getmondo.payments.send.bank.payment.l.a(b.a.c.a(), a.this.h, a.this.i, a.this.v, a.this.j, a.this.k, this.c));
         this.e = co.uk.getmondo.payments.send.bank.payment.g.a(this.d);
      }

      public void a(BankPaymentDetailsActivity var1) {
         this.e.a(var1);
      }
   }

   public static final class d {
      private c a;
      private co.uk.getmondo.common.h.a.a b;

      private d() {
      }

      // $FF: synthetic method
      d(Object var1) {
         this();
      }

      public a.d a(co.uk.getmondo.common.h.a.a var1) {
         this.b = (co.uk.getmondo.common.h.a.a)b.a.d.a(var1);
         return this;
      }

      public a.d a(c var1) {
         this.a = (c)b.a.d.a(var1);
         return this;
      }

      public b a() {
         if(this.a == null) {
            throw new IllegalStateException(c.class.getCanonicalName() + " must be set");
         } else if(this.b == null) {
            throw new IllegalStateException(co.uk.getmondo.common.h.a.a.class.getCanonicalName() + " must be set");
         } else {
            return new a(this, null);
         }
      }
   }

   private final class e implements co.uk.getmondo.monzo.me.customise.d {
      private final co.uk.getmondo.monzo.me.customise.e b;
      private javax.a.a c;
      private javax.a.a d;
      private javax.a.a e;
      private javax.a.a f;
      private b.a g;

      private e(co.uk.getmondo.monzo.me.customise.e var2) {
         this.b = (co.uk.getmondo.monzo.me.customise.e)b.a.d.a(var2);
         this.a();
      }

      // $FF: synthetic method
      e(co.uk.getmondo.monzo.me.customise.e var2, Object var3) {
         this(var2);
      }

      private void a() {
         this.c = co.uk.getmondo.monzo.me.customise.f.a(this.b);
         this.d = co.uk.getmondo.monzo.me.customise.h.a(this.b);
         this.e = co.uk.getmondo.monzo.me.customise.g.a(this.b);
         this.f = b.a.a.a(co.uk.getmondo.monzo.me.customise.o.a(b.a.c.a(), a.this.e, a.this.n, this.c, this.d, co.uk.getmondo.monzo.me.b.c(), this.e, a.this.o));
         this.g = co.uk.getmondo.monzo.me.customise.c.a(this.f);
      }

      public void a(CustomiseMonzoMeLinkActivity var1) {
         this.g.a(var1);
      }
   }

   private final class f implements co.uk.getmondo.golden_ticket.c {
      private final co.uk.getmondo.golden_ticket.d b;
      private javax.a.a c;
      private javax.a.a d;
      private javax.a.a e;
      private b.a f;

      private f(co.uk.getmondo.golden_ticket.d var2) {
         this.b = (co.uk.getmondo.golden_ticket.d)b.a.d.a(var2);
         this.a();
      }

      // $FF: synthetic method
      f(co.uk.getmondo.golden_ticket.d var2, Object var3) {
         this(var2);
      }

      private void a() {
         this.c = co.uk.getmondo.golden_ticket.f.a(this.b);
         this.d = co.uk.getmondo.golden_ticket.e.a(this.b);
         this.e = b.a.a.a(co.uk.getmondo.golden_ticket.n.a(b.a.c.a(), a.this.i, a.this.h, this.c, this.d, a.this.r, a.this.k, a.this.e));
         this.f = co.uk.getmondo.golden_ticket.b.a(this.e);
      }

      public void a(GoldenTicketActivity var1) {
         this.f.a(var1);
      }
   }

   private final class g implements co.uk.getmondo.help.b.a {
      private final co.uk.getmondo.help.b.b b;
      private javax.a.a c;
      private javax.a.a d;
      private javax.a.a e;
      private b.a f;

      private g(co.uk.getmondo.help.b.b var2) {
         this.b = (co.uk.getmondo.help.b.b)b.a.d.a(var2);
         this.a();
      }

      // $FF: synthetic method
      g(co.uk.getmondo.help.b.b var2, Object var3) {
         this(var2);
      }

      private void a() {
         this.c = b.a.a.a(co.uk.getmondo.help.b.c.a(this.b));
         this.d = b.a.a.a(co.uk.getmondo.help.b.d.a(this.b, a.this.b));
         this.e = b.a.a.a(co.uk.getmondo.help.e.a(b.a.c.a(), a.this.i, a.this.h, a.this.ci, a.this.k, this.c, this.d, a.this.e));
         this.f = co.uk.getmondo.help.b.a(this.e, a.this.M);
      }

      public void a(HelpCategoryActivity var1) {
         this.f.a(var1);
      }
   }

   private final class h implements co.uk.getmondo.help.b.e {
      private final co.uk.getmondo.help.b.f b;
      private javax.a.a c;
      private javax.a.a d;
      private b.a e;

      private h(co.uk.getmondo.help.b.f var2) {
         this.b = (co.uk.getmondo.help.b.f)b.a.d.a(var2);
         this.a();
      }

      // $FF: synthetic method
      h(co.uk.getmondo.help.b.f var2, Object var3) {
         this(var2);
      }

      private void a() {
         this.c = b.a.a.a(co.uk.getmondo.help.b.g.a(this.b));
         this.d = b.a.a.a(co.uk.getmondo.help.q.a(b.a.c.a(), this.c, a.this.e));
         this.e = co.uk.getmondo.help.n.a(this.d, a.this.M);
      }

      public void a(HelpTopicActivity var1) {
         this.e.a(var1);
      }
   }

   private final class i implements co.uk.getmondo.signup.identity_verification.o {
      private final co.uk.getmondo.signup.identity_verification.p b;
      private javax.a.a c;
      private javax.a.a d;
      private javax.a.a e;
      private javax.a.a f;
      private javax.a.a g;
      private javax.a.a h;
      private b.a i;
      private javax.a.a j;
      private b.a k;
      private javax.a.a l;
      private b.a m;
      private javax.a.a n;

      private i(co.uk.getmondo.signup.identity_verification.p var2) {
         this.b = (co.uk.getmondo.signup.identity_verification.p)b.a.d.a(var2);
         this.a();
      }

      // $FF: synthetic method
      i(co.uk.getmondo.signup.identity_verification.p var2, Object var3) {
         this(var2);
      }

      private void a() {
         this.c = co.uk.getmondo.signup.identity_verification.s.a(this.b);
         this.d = b.a.a.a(co.uk.getmondo.signup.identity_verification.q.a(this.b, a.this.bD, a.this.B, a.this.dI, a.this.aQ, this.c));
         this.e = co.uk.getmondo.signup.identity_verification.video.c.a(a.this.e);
         this.f = co.uk.getmondo.signup.identity_verification.a.b.a(a.this.c);
         this.g = ac.a(a.this.c, a.this.j);
         this.h = b.a.a.a(aa.a(b.a.c.a(), a.this.h, a.this.bh, this.d, a.this.e, a.this.M, this.e, this.f, this.g));
         this.i = co.uk.getmondo.signup.identity_verification.video.m.a(this.h);
         this.j = b.a.a.a(ad.a(b.a.c.a(), a.this.i, a.this.h, this.d, a.this.k, a.this.j));
         this.k = z.a(this.j);
         this.l = b.a.a.a(co.uk.getmondo.signup.identity_verification.fallback.j.a(b.a.c.a(), this.d, this.g, a.this.e));
         this.m = co.uk.getmondo.signup.identity_verification.fallback.g.a(this.l, this.f);
         this.n = co.uk.getmondo.signup.identity_verification.r.a(this.b);
      }

      public co.uk.getmondo.signup.identity_verification.i a(co.uk.getmondo.signup.identity_verification.j var1) {
         return new a.b(var1, null);
      }

      public co.uk.getmondo.signup.identity_verification.id_picture.e a(co.uk.getmondo.signup.identity_verification.id_picture.f var1) {
         return new a.a(var1, null);
      }

      public void a(VerificationPendingActivity var1) {
         this.k.a(var1);
      }

      public void a(VideoFallbackActivity var1) {
         this.m.a(var1);
      }

      public void a(VideoRecordingActivity var1) {
         this.i.a(var1);
      }
   }

   private final class a implements co.uk.getmondo.signup.identity_verification.id_picture.e {
      private final co.uk.getmondo.signup.identity_verification.id_picture.f b;
      private javax.a.a c;
      private javax.a.a d;
      private javax.a.a e;
      private javax.a.a f;
      private javax.a.a g;
      private b.a h;
      private javax.a.a i;
      private javax.a.a j;
      private b.a k;

      private a(co.uk.getmondo.signup.identity_verification.id_picture.f var2) {
         this.b = (co.uk.getmondo.signup.identity_verification.id_picture.f)b.a.d.a(var2);
         this.a();
      }

      // $FF: synthetic method
      a(co.uk.getmondo.signup.identity_verification.id_picture.f var2, Object var3) {
         this(var2);
      }

      private void a() {
         this.c = co.uk.getmondo.signup.identity_verification.id_picture.i.a(this.b);
         this.d = co.uk.getmondo.signup.identity_verification.id_picture.t.a(a.super.a.c, this.c);
         this.e = co.uk.getmondo.signup.identity_verification.id_picture.g.a(this.b);
         this.f = co.uk.getmondo.signup.identity_verification.id_picture.h.a(this.b);
         this.g = b.a.a.a(co.uk.getmondo.signup.identity_verification.id_picture.r.a(b.a.c.a(), a.super.a.h, a.super.a.i, a.super.a.bh, a.super.d, this.d, a.super.a.e, co.uk.getmondo.signup.identity_verification.id_picture.v.c(), a.super.n, this.c, this.e, a.super.c, this.f));
         this.h = co.uk.getmondo.signup.identity_verification.id_picture.d.a(this.g, a.super.f);
         this.i = co.uk.getmondo.signup.identity_verification.fallback.f.a(a.super.a.c);
         this.j = b.a.a.a(co.uk.getmondo.signup.identity_verification.fallback.d.a(b.a.c.a(), a.super.a.bh, a.super.a.h, this.e, this.c, a.super.d, co.uk.getmondo.signup.identity_verification.id_picture.v.c(), this.i, a.super.a.e));
         this.k = co.uk.getmondo.signup.identity_verification.fallback.a.a(this.j, a.super.f);
      }

      public void a(DocumentFallbackActivity var1) {
         this.k.a(var1);
      }

      public void a(DocumentCameraActivity var1) {
         this.h.a(var1);
      }
   }

   private final class b implements co.uk.getmondo.signup.identity_verification.i {
      private final co.uk.getmondo.signup.identity_verification.j b;
      private javax.a.a c;
      private b.a d;
      private javax.a.a e;
      private javax.a.a f;
      private javax.a.a g;
      private javax.a.a h;
      private javax.a.a i;
      private b.a j;

      private b(co.uk.getmondo.signup.identity_verification.j var2) {
         this.b = (co.uk.getmondo.signup.identity_verification.j)b.a.d.a(var2);
         this.a();
      }

      // $FF: synthetic method
      b(co.uk.getmondo.signup.identity_verification.j var2, Object var3) {
         this(var2);
      }

      private void a() {
         this.c = b.a.a.a(co.uk.getmondo.signup.identity_verification.x.a(b.a.c.a(), a.super.a.i, a.super.a.h, a.super.a.k, a.super.d, a.super.a.cb));
         this.d = co.uk.getmondo.signup.identity_verification.n.a(a.super.a.M, this.c);
         this.e = b.a.a.a(co.uk.getmondo.signup.identity_verification.a.d.c());
         this.f = co.uk.getmondo.signup.identity_verification.l.a(this.b);
         this.g = co.uk.getmondo.signup.identity_verification.m.a(this.b);
         this.h = co.uk.getmondo.signup.identity_verification.k.a(this.b);
         this.i = b.a.a.a(co.uk.getmondo.signup.identity_verification.h.a(b.a.c.a(), a.super.a.i, a.super.a.h, a.super.a.k, a.super.d, this.e, a.super.a.e, a.super.n, this.f, this.g, this.h));
         this.j = co.uk.getmondo.signup.identity_verification.f.a(this.i);
      }

      public void a(IdentityVerificationActivity var1) {
         this.d.a(var1);
      }

      public void a(co.uk.getmondo.signup.identity_verification.e var1) {
         this.j.a(var1);
      }
   }

   private final class j implements co.uk.getmondo.signup.identity_verification.sdd.b {
      private final co.uk.getmondo.signup.identity_verification.sdd.c b;
      private javax.a.a c;
      private javax.a.a d;
      private javax.a.a e;
      private b.a f;

      private j(co.uk.getmondo.signup.identity_verification.sdd.c var2) {
         this.b = (co.uk.getmondo.signup.identity_verification.sdd.c)b.a.d.a(var2);
         this.a();
      }

      // $FF: synthetic method
      j(co.uk.getmondo.signup.identity_verification.sdd.c var2, Object var3) {
         this(var2);
      }

      private void a() {
         this.c = co.uk.getmondo.signup.identity_verification.sdd.e.a(this.b);
         this.d = co.uk.getmondo.signup.identity_verification.sdd.d.a(this.b);
         this.e = b.a.a.a(co.uk.getmondo.signup.identity_verification.sdd.i.a(b.a.c.a(), this.c, a.this.e, this.d));
         this.f = co.uk.getmondo.signup.identity_verification.sdd.a.a(this.e);
      }

      public void a(IdentityVerificationSddActivity var1) {
         this.f.a(var1);
      }
   }

   private final class k implements co.uk.getmondo.monzo.me.deeplink.b {
      private final co.uk.getmondo.monzo.me.deeplink.c b;
      private javax.a.a c;
      private javax.a.a d;
      private b.a e;

      private k(co.uk.getmondo.monzo.me.deeplink.c var2) {
         this.b = (co.uk.getmondo.monzo.me.deeplink.c)b.a.d.a(var2);
         this.a();
      }

      // $FF: synthetic method
      k(co.uk.getmondo.monzo.me.deeplink.c var2, Object var3) {
         this(var2);
      }

      private void a() {
         this.c = co.uk.getmondo.monzo.me.deeplink.e.a(this.b);
         this.d = b.a.a.a(co.uk.getmondo.monzo.me.deeplink.d.a(this.b, a.this.j, a.this.o));
         this.e = co.uk.getmondo.monzo.me.deeplink.a.a(this.d);
      }

      public void a(MonzoMeActivity var1) {
         this.e.a(var1);
      }
   }

   private final class l implements co.uk.getmondo.news.b {
      private final co.uk.getmondo.news.k b;
      private javax.a.a c;
      private javax.a.a d;
      private b.a e;

      private l(co.uk.getmondo.news.k var2) {
         this.b = (co.uk.getmondo.news.k)b.a.d.a(var2);
         this.a();
      }

      // $FF: synthetic method
      l(co.uk.getmondo.news.k var2, Object var3) {
         this(var2);
      }

      private void a() {
         this.c = co.uk.getmondo.news.l.a(this.b);
         this.d = b.a.a.a(co.uk.getmondo.news.n.a(b.a.c.a(), this.c, a.this.s, a.this.e));
         this.e = co.uk.getmondo.news.a.a(this.d);
      }

      public void a(NewsActivity var1) {
         this.e.a(var1);
      }
   }

   private final class m implements co.uk.getmondo.payments.send.authentication.e {
      private final co.uk.getmondo.payments.send.authentication.f b;
      private javax.a.a c;
      private javax.a.a d;
      private javax.a.a e;
      private b.a f;

      private m(co.uk.getmondo.payments.send.authentication.f var2) {
         this.b = (co.uk.getmondo.payments.send.authentication.f)b.a.d.a(var2);
         this.a();
      }

      // $FF: synthetic method
      m(co.uk.getmondo.payments.send.authentication.f var2, Object var3) {
         this(var2);
      }

      private void a() {
         this.c = co.uk.getmondo.payments.send.authentication.g.a(this.b);
         this.d = b.a.a.a(co.uk.getmondo.payments.send.authentication.o.a(b.a.c.a(), a.this.h, a.this.i, a.this.k, a.this.bt, a.this.e, this.c, a.this.w));
         this.e = b.a.a.a(co.uk.getmondo.payments.send.c.a(a.this.b));
         this.f = co.uk.getmondo.payments.send.authentication.d.a(this.d, this.e);
      }

      public void a(PaymentAuthenticationActivity var1) {
         this.f.a(var1);
      }
   }

   private final class n implements co.uk.getmondo.payments.send.payment_category.l {
      private final co.uk.getmondo.payments.send.payment_category.m b;
      private javax.a.a c;
      private javax.a.a d;
      private b.a e;

      private n(co.uk.getmondo.payments.send.payment_category.m var2) {
         this.b = (co.uk.getmondo.payments.send.payment_category.m)b.a.d.a(var2);
         this.a();
      }

      // $FF: synthetic method
      n(co.uk.getmondo.payments.send.payment_category.m var2, Object var3) {
         this(var2);
      }

      private void a() {
         this.c = b.a.a.a(co.uk.getmondo.payments.send.payment_category.n.a(this.b));
         this.d = b.a.a.a(co.uk.getmondo.payments.send.payment_category.t.a(b.a.c.a(), a.this.i, a.this.dH, a.this.e, this.c));
         this.e = co.uk.getmondo.payments.send.payment_category.k.a(this.d);
      }

      public void a(PaymentCategoryActivity var1) {
         this.e.a(var1);
      }
   }

   private final class o implements co.uk.getmondo.payments.send.peer.e {
      private final co.uk.getmondo.payments.send.peer.f b;
      private javax.a.a c;
      private javax.a.a d;
      private javax.a.a e;
      private javax.a.a f;
      private b.a g;

      private o(co.uk.getmondo.payments.send.peer.f var2) {
         this.b = (co.uk.getmondo.payments.send.peer.f)b.a.d.a(var2);
         this.a();
      }

      // $FF: synthetic method
      o(co.uk.getmondo.payments.send.peer.f var2, Object var3) {
         this(var2);
      }

      private void a() {
         this.c = co.uk.getmondo.payments.send.peer.i.a(this.b);
         this.d = co.uk.getmondo.payments.send.peer.g.a(this.b);
         this.e = co.uk.getmondo.payments.send.peer.h.a(this.b);
         this.f = b.a.a.a(co.uk.getmondo.payments.send.peer.r.a(b.a.c.a(), a.this.h, a.this.i, a.this.k, a.this.j, this.c, this.d, this.e, a.this.v));
         this.g = co.uk.getmondo.payments.send.peer.d.a(this.f, a.this.e);
      }

      public void a(PeerPaymentActivity var1) {
         this.g.a(var1);
      }
   }

   private final class p implements co.uk.getmondo.common.pin.a {
      private final co.uk.getmondo.common.pin.g b;
      private javax.a.a c;
      private javax.a.a d;
      private b.a e;

      private p(co.uk.getmondo.common.pin.g var2) {
         this.b = (co.uk.getmondo.common.pin.g)b.a.d.a(var2);
         this.a();
      }

      // $FF: synthetic method
      p(co.uk.getmondo.common.pin.g var2, Object var3) {
         this(var2);
      }

      private void a() {
         this.c = b.a.a.a(co.uk.getmondo.common.pin.h.a(this.b));
         this.d = b.a.a.a(co.uk.getmondo.common.pin.f.a(b.a.c.a(), a.this.i, a.this.h, a.this.k, this.c, a.this.cb, a.this.e));
         this.e = co.uk.getmondo.common.pin.b.a(this.d);
      }

      public void a(PinEntryActivity var1) {
         this.e.a(var1);
      }
   }

   private final class q implements co.uk.getmondo.payments.recurring_cancellation.e {
      private final co.uk.getmondo.payments.recurring_cancellation.f b;
      private javax.a.a c;
      private javax.a.a d;
      private b.a e;

      private q(co.uk.getmondo.payments.recurring_cancellation.f var2) {
         this.b = (co.uk.getmondo.payments.recurring_cancellation.f)b.a.d.a(var2);
         this.a();
      }

      // $FF: synthetic method
      q(co.uk.getmondo.payments.recurring_cancellation.f var2, Object var3) {
         this(var2);
      }

      private void a() {
         this.c = co.uk.getmondo.payments.recurring_cancellation.g.a(this.b);
         this.d = b.a.a.a(co.uk.getmondo.payments.recurring_cancellation.d.a(b.a.c.a(), a.this.i, a.this.h, a.this.bm, a.this.k, this.c));
         this.e = co.uk.getmondo.payments.recurring_cancellation.a.a(this.d);
      }

      public void a(RecurringPaymentCancelActivity var1) {
         this.e.a(var1);
      }
   }

   private final class r implements co.uk.getmondo.common.address.m {
      private final co.uk.getmondo.common.address.n b;
      private javax.a.a c;
      private javax.a.a d;
      private b.a e;

      private r(co.uk.getmondo.common.address.n var2) {
         this.b = (co.uk.getmondo.common.address.n)b.a.d.a(var2);
         this.a();
      }

      // $FF: synthetic method
      r(co.uk.getmondo.common.address.n var2, Object var3) {
         this(var2);
      }

      private void a() {
         this.c = co.uk.getmondo.common.address.o.a(this.b);
         this.d = b.a.a.a(co.uk.getmondo.common.address.w.a(b.a.c.a(), a.this.h, a.this.i, a.this.j, a.this.k, a.this.r, a.this.e, this.c));
         this.e = co.uk.getmondo.common.address.l.a(this.d);
      }

      public void a(co.uk.getmondo.common.address.SelectAddressActivity var1) {
         this.e.a(var1);
      }
   }

   private final class s implements co.uk.getmondo.spending.merchant.a {
      private final co.uk.getmondo.spending.merchant.f b;
      private javax.a.a c;
      private b.a d;

      private s(co.uk.getmondo.spending.merchant.f var2) {
         this.b = (co.uk.getmondo.spending.merchant.f)b.a.d.a(var2);
         this.a();
      }

      // $FF: synthetic method
      s(co.uk.getmondo.spending.merchant.f var2, Object var3) {
         this(var2);
      }

      private void a() {
         this.c = b.a.a.a(co.uk.getmondo.spending.merchant.g.a(this.b, a.this.h, a.this.bj, co.uk.getmondo.spending.a.f.c(), a.this.e));
         this.d = co.uk.getmondo.spending.merchant.e.a(this.c);
      }

      public void a(SpendingByMerchantFragment var1) {
         this.d.a(var1);
      }
   }

   private final class t implements co.uk.getmondo.spending.transactions.b {
      private final co.uk.getmondo.spending.transactions.d b;
      private javax.a.a c;
      private javax.a.a d;
      private b.a e;

      private t(co.uk.getmondo.spending.transactions.d var2) {
         this.b = (co.uk.getmondo.spending.transactions.d)b.a.d.a(var2);
         this.a();
      }

      // $FF: synthetic method
      t(co.uk.getmondo.spending.transactions.d var2, Object var3) {
         this(var2);
      }

      private void a() {
         this.c = co.uk.getmondo.spending.transactions.e.a(this.b, a.this.h, a.this.bj, a.this.e);
         this.d = co.uk.getmondo.spending.transactions.a.a(b.a.c.a(), a.this.aO);
         this.e = co.uk.getmondo.spending.transactions.c.a(this.c, this.d);
      }

      public void a(SpendingTransactionsFragment var1) {
         this.e.a(var1);
      }
   }

   private final class u implements co.uk.getmondo.transaction.c {
      private final co.uk.getmondo.transaction.d b;
      private javax.a.a c;
      private javax.a.a d;
      private javax.a.a e;
      private b.a f;

      private u(co.uk.getmondo.transaction.d var2) {
         this.b = (co.uk.getmondo.transaction.d)b.a.d.a(var2);
         this.a();
      }

      // $FF: synthetic method
      u(co.uk.getmondo.transaction.d var2, Object var3) {
         this(var2);
      }

      private void a() {
         this.c = co.uk.getmondo.transaction.e.a(this.b);
         this.d = co.uk.getmondo.transaction.b.a(a.this.n);
         this.e = b.a.a.a(co.uk.getmondo.transaction.details.d.a(b.a.c.a(), a.this.h, a.this.i, this.c, a.this.aj, a.this.A, a.this.k, this.d, a.this.o, a.this.e, a.this.w));
         this.f = co.uk.getmondo.transaction.details.a.a(this.e);
      }

      public void a(TransactionDetailsActivity var1) {
         this.f.a(var1);
      }
   }

   private final class v implements co.uk.getmondo.signup.identity_verification.video.e {
      private final co.uk.getmondo.signup.identity_verification.video.f b;
      private javax.a.a c;
      private javax.a.a d;
      private b.a e;

      private v(co.uk.getmondo.signup.identity_verification.video.f var2) {
         this.b = (co.uk.getmondo.signup.identity_verification.video.f)b.a.d.a(var2);
         this.a();
      }

      // $FF: synthetic method
      v(co.uk.getmondo.signup.identity_verification.video.f var2, Object var3) {
         this(var2);
      }

      private void a() {
         this.c = co.uk.getmondo.signup.identity_verification.video.g.a(this.b);
         this.d = b.a.a.a(co.uk.getmondo.signup.identity_verification.video.l.a(b.a.c.a(), a.this.e, this.c));
         this.e = co.uk.getmondo.signup.identity_verification.video.d.a(this.d);
      }

      public void a(VideoPlaybackActivity var1) {
         this.e.a(var1);
      }
   }
}
