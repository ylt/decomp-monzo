package co.uk.getmondo.common.h.b;

import co.uk.getmondo.topup.three_d_secure.ThreeDsResolver;

public final class f implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final c b;
   private final javax.a.a c;

   static {
      boolean var0;
      if(!f.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   public f(c var1, javax.a.a var2) {
      if(!a && var1 == null) {
         throw new AssertionError();
      } else {
         this.b = var1;
         if(!a && var2 == null) {
            throw new AssertionError();
         } else {
            this.c = var2;
         }
      }
   }

   public static b.a.b a(c var0, javax.a.a var1) {
      return new f(var0, var1);
   }

   public ThreeDsResolver a() {
      return (ThreeDsResolver)b.a.d.a(this.b.a((co.uk.getmondo.common.a)this.c.b()), "Cannot return null from a non-@Nullable @Provides method");
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
