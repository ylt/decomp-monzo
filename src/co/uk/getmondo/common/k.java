package co.uk.getmondo.common;

import android.content.Context;
import android.content.Intent;
import android.content.pm.LabeledIntent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.support.v4.content.FileProvider;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

@Deprecated
public class k {
   public static Intent a() {
      Intent var0 = new Intent("android.intent.action.OPEN_DOCUMENT");
      var0.addCategory("android.intent.category.OPENABLE");
      var0.setType("image/*");
      return var0;
   }

   private Intent a(Context var1, PackageManager var2, List var3) {
      Intent var4;
      if(var3.size() <= 0) {
         var4 = null;
      } else {
         Intent var5 = var2.getLaunchIntentForPackage(((ResolveInfo)var3.get(0)).activityInfo.packageName);
         if(var5 == null) {
            var4 = null;
         } else {
            var4 = Intent.createChooser(var5, var1.getString(2131362504));
         }
      }

      return var4;
   }

   public static Intent a(Context var0, File var1) {
      Intent var2 = new Intent("android.media.action.IMAGE_CAPTURE");
      Intent var3;
      if(var2.resolveActivity(var0.getPackageManager()) != null) {
         var2.putExtra("output", FileProvider.a(var0, var0.getString(2131362187), var1));
         var3 = var2;
      } else {
         var3 = null;
      }

      return var3;
   }

   private void a(PackageManager var1, List var2, List var3, int var4) {
      ResolveInfo var5 = (ResolveInfo)var2.get(var4);
      if(!"com.paypal.android.p2pmobile".equals(var5.activityInfo.packageName)) {
         Intent var6 = var1.getLaunchIntentForPackage(var5.activityInfo.packageName);
         if(var6 != null) {
            var3.add(new LabeledIntent(var6, var5.activityInfo.packageName, var5.loadLabel(var1), var5.icon));
         }
      }

   }

   private LabeledIntent[] a(PackageManager var1, List var2) {
      ArrayList var4 = new ArrayList();

      for(int var3 = 1; var3 < var2.size(); ++var3) {
         this.a(var1, var2, var4, var3);
      }

      return (LabeledIntent[])var4.toArray(new LabeledIntent[var4.size()]);
   }

   public void a(Context var1) {
      PackageManager var2 = var1.getPackageManager();
      List var4 = var2.queryIntentActivities(new Intent("android.intent.action.SENDTO", Uri.parse("mailto:")), 0);
      Intent var3 = this.a(var1, var2, var4);
      if(var3 == null) {
         throw new NoSuchElementException(var1.getString(2131362496));
      } else {
         var3.putExtra("android.intent.extra.INITIAL_INTENTS", this.a(var2, var4));
         var1.startActivity(var3);
      }
   }

   public void a(Context var1, String var2) {
      Intent var3 = new Intent("android.intent.action.VIEW");
      var3.setData(Uri.parse(var2));
      var1.startActivity(var3);
   }
}
