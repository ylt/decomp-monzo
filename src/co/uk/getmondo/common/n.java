package co.uk.getmondo.common;

import android.app.Application;

public final class n implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;

   static {
      boolean var0;
      if(!n.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   public n(javax.a.a var1) {
      if(!a && var1 == null) {
         throw new AssertionError();
      } else {
         this.b = var1;
      }
   }

   public static b.a.b a(javax.a.a var0) {
      return new n(var0);
   }

   public m a() {
      return new m((Application)this.b.b());
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
