package co.uk.getmondo.common.f;

import android.view.View;
import android.view.View.OnClickListener;

// $FF: synthetic class
final class e implements OnClickListener {
   private final c.a a;

   private e(c.a var1) {
      this.a = var1;
   }

   public static OnClickListener a(c.a var0) {
      return new e(var0);
   }

   public void onClick(View var1) {
      c.a(this.a, var1);
   }
}
