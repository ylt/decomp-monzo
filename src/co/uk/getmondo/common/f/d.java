package co.uk.getmondo.common.f;

import android.view.View;
import android.view.View.OnClickListener;

// $FF: synthetic class
final class d implements OnClickListener {
   private final c.a a;

   private d(c.a var1) {
      this.a = var1;
   }

   public static OnClickListener a(c.a var0) {
      return new d(var0);
   }

   public void onClick(View var1) {
      c.b(this.a, var1);
   }
}
