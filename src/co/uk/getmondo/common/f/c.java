package co.uk.getmondo.common.f;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.design.widget.Snackbar;
import android.view.ContextThemeWrapper;
import android.view.View;
import co.uk.getmondo.common.ui.i;

public class c {
   private final Context a;
   private final View b;
   private ProgressDialog c;
   private Snackbar d;

   public c(Context var1, View var2) {
      this.a = var1;
      this.b = var2;
      if(var2 == null) {
         throw new RuntimeException("The view passed into the UiMessageHelper cannot be null");
      }
   }

   // $FF: synthetic method
   static void a(c.a var0, View var1) {
      var0.a();
   }

   // $FF: synthetic method
   static void b(c.a var0, View var1) {
      var0.a();
   }

   public void a() {
      this.c(this.a.getString(2131362402));
   }

   public void a(int var1, String var2) {
      this.b(this.a.getString(var1, new Object[]{var2}), (String)null, (c.a)null);
   }

   public void a(String var1) {
      i.a(this.b.getContext(), this.b, var1, 0, false).c();
   }

   public void a(String var1, String var2, c.a var3) {
      Snackbar var4 = i.a(this.b.getContext(), this.b, var1, -2, false);
      var4.a(var2, d.a(var3));
      var4.c();
   }

   public Snackbar b(String var1, String var2, c.a var3) {
      if(this.d == null) {
         this.d = i.a(this.b.getContext(), this.b, var1, -2, false);
      } else {
         this.d.a(var1);
      }

      if(var2 != null && var3 != null) {
         this.d.a(var2, e.a(var3));
      }

      if(!this.d.e()) {
         this.d.c();
      }

      return this.d;
   }

   public void b() {
      if(this.c != null) {
         this.c.dismiss();
      }

   }

   public void b(String var1) {
      this.a(var1);
   }

   public void c(String var1) {
      try {
         if(this.c == null) {
            ContextThemeWrapper var3 = new ContextThemeWrapper(this.a, 2131493134);
            ProgressDialog var2 = new ProgressDialog(var3);
            this.c = var2;
         }

         this.c.setTitle("");
         this.c.setMessage(var1);
         this.c.setIndeterminate(true);
         this.c.show();
      } catch (Exception var4) {
         d.a.a.a(var4, "Problem showing progress dialog", new Object[0]);
      }

   }

   public interface a {
      void a();
   }
}
