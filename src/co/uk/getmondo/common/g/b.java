package co.uk.getmondo.common.g;

import com.google.gson.JsonParseException;
import com.google.gson.f;
import com.google.gson.l;
import com.google.gson.n;
import com.google.gson.o;
import com.google.gson.s;
import com.google.gson.t;
import com.google.gson.b.j;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

public final class b implements t {
   private final Class a;
   private final String b;
   private final Map c = new LinkedHashMap();
   private final Map d = new LinkedHashMap();
   private Class e;

   private b(Class var1, String var2) {
      if(var2 != null && var1 != null) {
         this.a = var1;
         this.b = var2;
      } else {
         throw new NullPointerException();
      }
   }

   public static b a(Class var0, String var1) {
      return new b(var0, var1);
   }

   public b a(Class var1) {
      if(var1 == null) {
         throw new NullPointerException();
      } else if(var1 == this.e) {
         throw new IllegalArgumentException("type must be unique");
      } else {
         this.e = var1;
         return this;
      }
   }

   public s a(final f var1, com.google.gson.c.a var2) {
      s var7;
      if(var2.a() != this.a) {
         var7 = null;
      } else {
         final LinkedHashMap var3 = new LinkedHashMap();
         final LinkedHashMap var8 = new LinkedHashMap();
         Iterator var5 = this.c.entrySet().iterator();

         while(var5.hasNext()) {
            Entry var4 = (Entry)var5.next();
            s var6 = var1.a(this, com.google.gson.c.a.b((Class)var4.getValue()));
            var3.put(var4.getKey(), var6);
            var8.put(var4.getValue(), var6);
         }

         var7 = (new s() {
            public Object a(JsonReader var1x) throws IOException {
               l var4 = j.a(var1x);
               l var2 = var4.k().a(b.this.b);
               if(var2 == null && b.this.e == null) {
                  throw new JsonParseException("cannot deserialize " + b.this.a + " because it does not define a field named " + b.this.b + " and no read fallback type is registered");
               } else {
                  Object var5;
                  if(var2 == null) {
                     var5 = var1.a(b.this, com.google.gson.c.a.b(b.this.e)).a(var4);
                  } else {
                     String var3x = var2.b();
                     s var6 = (s)var3.get(var3x);
                     if(var6 == null) {
                        throw new JsonParseException("cannot deserialize " + b.this.a + " subtype named " + var3x + "; did you forget to register a subtype?");
                     }

                     var5 = var6.a(var4);
                  }

                  return var5;
               }
            }

            public void a(JsonWriter var1x, Object var2) throws IOException {
               Class var4 = var2.getClass();
               String var3x = (String)b.this.d.get(var4);
               s var5 = (s)var8.get(var4);
               if(var5 == null) {
                  throw new JsonParseException("cannot serialize " + var4.getName() + "; did you forget to register a subtype?");
               } else {
                  n var9 = var5.a(var2).k();
                  if(var9.b(b.this.b)) {
                     throw new JsonParseException("cannot serialize " + var4.getName() + " because it already defines a field named " + b.this.b);
                  } else {
                     n var6 = new n();
                     var6.a(b.this.b, new o(var3x));
                     Iterator var7 = var9.o().iterator();

                     while(var7.hasNext()) {
                        Entry var8x = (Entry)var7.next();
                        var6.a((String)var8x.getKey(), (l)var8x.getValue());
                     }

                     j.a(var6, var1x);
                  }
               }
            }
         }).a();
      }

      return var7;
   }

   public b b(Class var1, String var2) {
      if(var1 != null && var2 != null) {
         if(!this.d.containsKey(var1) && !this.c.containsKey(var2)) {
            this.c.put(var2, var1);
            this.d.put(var1, var2);
            return this;
         } else {
            throw new IllegalArgumentException("types and labels must be unique");
         }
      } else {
         throw new NullPointerException();
      }
   }
}
