package co.uk.getmondo.common.g;

import com.google.gson.f;
import com.google.gson.l;
import com.google.gson.s;
import com.google.gson.t;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;

public class a implements t {
   public s a(f var1, com.google.gson.c.a var2) {
      return (new a.a(var1.a(this, var2), var1.a(l.class))).a();
   }

   public static class a extends s {
      private final s a;
      private final s b;

      public a(s var1, s var2) {
         this.a = var1;
         this.b = var2;
      }

      public Object a(JsonReader var1) throws IOException {
         l var2 = (l)this.b.a(var1);
         Object var3;
         if(var2.h() && var2.k().o().isEmpty()) {
            var3 = null;
         } else {
            var3 = this.a.a(var2);
         }

         return var3;
      }

      public void a(JsonWriter var1, Object var2) throws IOException {
         this.a.a(var1, var2);
      }
   }
}
