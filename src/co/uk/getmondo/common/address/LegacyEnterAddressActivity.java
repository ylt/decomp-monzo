package co.uk.getmondo.common.address;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.widget.Button;
import android.widget.EditText;
import butterknife.BindView;
import butterknife.ButterKnife;

@Deprecated
public class LegacyEnterAddressActivity extends co.uk.getmondo.common.activities.b implements e.a {
   e a;
   @BindView(2131820900)
   EditText cityView;
   @BindView(2131820899)
   TextInputLayout cityWrapper;
   @BindView(2131820901)
   EditText countryView;
   @BindView(2131820898)
   EditText postalCodeView;
   @BindView(2131820897)
   TextInputLayout postalCodeWrapper;
   @BindView(2131820896)
   EditText streetAddressView;
   @BindView(2131820895)
   TextInputLayout streetAddressWrapper;
   @BindView(2131820902)
   Button submitButton;

   public static Intent a(Context var0, String var1, co.uk.getmondo.common.activities.b.a var2) {
      Intent var3 = new Intent(var0, LegacyEnterAddressActivity.class);
      var3.putExtra("EXTRA_SUBMIT_TEXT", var1);
      var3.putExtra("EXTRA_THEME", var2);
      return var3;
   }

   public static Intent a(Context var0, String var1, co.uk.getmondo.common.activities.b.a var2, co.uk.getmondo.d.s var3) {
      Intent var4 = a(var0, var1, var2);
      if(var3 != null) {
         var4.putExtra("EXTRA_ADDRESS", var3);
      }

      return var4;
   }

   // $FF: synthetic method
   static co.uk.getmondo.common.address.a.a.a a(LegacyEnterAddressActivity var0, Object var1) throws Exception {
      return new co.uk.getmondo.common.address.a.a.a(var0.streetAddressView.getText().toString(), var0.postalCodeView.getText().toString(), var0.cityView.getText().toString());
   }

   public static co.uk.getmondo.d.s a(Intent var0) {
      return (co.uk.getmondo.d.s)var0.getParcelableExtra("EXTRA_ADDRESS");
   }

   public io.reactivex.n a() {
      return com.b.a.c.c.a(this.submitButton).map(c.a(this));
   }

   public void a(co.uk.getmondo.d.s var1) {
      Intent var2 = new Intent();
      var2.putExtra("EXTRA_ADDRESS", var1);
      this.setResult(-1, var2);
      this.finish();
   }

   public void a(String var1) {
      this.streetAddressView.setText(var1);
   }

   public void b() {
      this.streetAddressWrapper.setErrorEnabled(true);
      this.streetAddressWrapper.setError(this.getString(2131362578));
   }

   public void c() {
      this.postalCodeWrapper.setErrorEnabled(true);
      this.postalCodeWrapper.setError(this.getString(2131362577));
   }

   public void d() {
      this.cityWrapper.setErrorEnabled(true);
      this.cityWrapper.setError(this.getString(2131362575));
   }

   public void d(String var1) {
      this.postalCodeView.setText(var1);
   }

   public void e() {
      this.streetAddressWrapper.setErrorEnabled(false);
      this.postalCodeWrapper.setErrorEnabled(false);
      this.cityWrapper.setErrorEnabled(false);
   }

   public void e(String var1) {
      this.cityView.setText(var1);
   }

   public void f(String var1) {
      this.countryView.setText(var1);
   }

   protected void onCreate(Bundle var1) {
      co.uk.getmondo.common.activities.b.a var3 = (co.uk.getmondo.common.activities.b.a)this.getIntent().getSerializableExtra("EXTRA_THEME");
      if(var3 != null) {
         int var2;
         if(var3 == co.uk.getmondo.common.activities.b.a.b) {
            var2 = 2131493160;
         } else {
            var2 = 2131493157;
         }

         this.setTheme(var2);
      }

      super.onCreate(var1);
      this.setContentView(2131034166);
      ButterKnife.bind((Activity)this);
      this.l().a(this);
      String var4 = this.getIntent().getStringExtra("EXTRA_SUBMIT_TEXT");
      this.submitButton.setText(var4);
      this.getSupportActionBar().b(true);
      this.a.a((co.uk.getmondo.d.s)this.getIntent().getParcelableExtra("EXTRA_ADDRESS"));
      this.a.a((e.a)this);
   }

   protected void onDestroy() {
      this.a.b();
      super.onDestroy();
   }
}
