package co.uk.getmondo.common.address;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.uk.getmondo.create_account.phone_number.EnterPhoneNumberActivity;
import java.util.List;

@Deprecated
public class SelectAddressActivity extends co.uk.getmondo.common.activities.b implements p.a {
   p a;
   @BindView(2131821054)
   RecyclerView addressesRecyclerView;
   private AddressAdapter b;
   private co.uk.getmondo.common.activities.b.a c;
   @BindView(2131820870)
   EditText postalCode;
   @BindView(2131820869)
   TextInputLayout postalCodeWrapper;
   @BindView(2131821053)
   TextView selectAddressLabel;

   public static co.uk.getmondo.d.s a(Intent var0) {
      return (co.uk.getmondo.d.s)var0.getParcelableExtra("RESULT_EXTRA_ADDRESS");
   }

   public static void a(Context var0, co.uk.getmondo.common.activities.b.a var1, boolean var2) {
      var0.startActivity(b(var0, var1, var2));
   }

   // $FF: synthetic method
   static boolean a(SelectAddressActivity var0, TextView var1, int var2, KeyEvent var3) {
      boolean var4;
      if(var2 == 3) {
         var0.a((View)var1);
         var1.clearFocus();
         var0.a.a(var0.postalCode.getText().toString());
         var4 = true;
      } else {
         var4 = false;
      }

      return var4;
   }

   public static Intent b(Context var0, co.uk.getmondo.common.activities.b.a var1, boolean var2) {
      Intent var3 = new Intent(var0, SelectAddressActivity.class);
      var3.putExtra("EXTRA_THEME", var1);
      var3.putExtra("EXTRA_IS_SIGN_UP_FLOW", var2);
      return var3;
   }

   public void a() {
      this.selectAddressLabel.setVisibility(4);
   }

   public void a(co.uk.getmondo.d.s var1) {
      Intent var2 = new Intent();
      var2.putExtra("RESULT_EXTRA_ADDRESS", var1);
      this.setResult(-1, var2);
      this.finish();
   }

   public void a(String var1) {
      this.postalCode.setText(var1);
   }

   public void a(List var1) {
      this.selectAddressLabel.setVisibility(0);
      this.addressesRecyclerView.setVisibility(0);
      this.b.a(var1);
      this.addressesRecyclerView.getLayoutManager().e(0);
   }

   public void b() {
      this.selectAddressLabel.setVisibility(0);
   }

   public void c() {
      this.postalCodeWrapper.setErrorEnabled(true);
      this.postalCodeWrapper.setError(this.getString(2131362495));
      this.addressesRecyclerView.setVisibility(0);
      this.b.a(this.getString(2131362169));
   }

   public void d() {
      this.postalCodeWrapper.setErrorEnabled(false);
      this.addressesRecyclerView.setVisibility(0);
      this.b.a(this.getString(2131361998));
   }

   public void e() {
      EnterPhoneNumberActivity.a((Context)this);
   }

   public void f() {
      this.startActivityForResult(LegacyEnterAddressActivity.a(this, this.getString(2131362494), this.c), 1);
   }

   public void g() {
      this.b.a();
   }

   protected void onActivityResult(int var1, int var2, Intent var3) {
      super.onActivityResult(var1, var2, var3);
      if(var1 == 1 && var2 == -1) {
         co.uk.getmondo.d.s var4 = LegacyEnterAddressActivity.a(var3);
         this.a.a(var4);
      }

   }

   protected void onCreate(Bundle var1) {
      this.c = (co.uk.getmondo.common.activities.b.a)this.getIntent().getSerializableExtra("EXTRA_THEME");
      if(this.c != null) {
         int var2;
         if(this.c == co.uk.getmondo.common.activities.b.a.b) {
            var2 = 2131493160;
         } else {
            var2 = 2131493157;
         }

         this.setTheme(var2);
      }

      super.onCreate(var1);
      this.setContentView(2131034204);
      ButterKnife.bind((Activity)this);
      boolean var3 = this.getIntent().getBooleanExtra("EXTRA_IS_SIGN_UP_FLOW", false);
      this.l().a(new n(var3)).a(this);
      if(!var3) {
         this.getWindow().setSoftInputMode(4);
         this.postalCode.requestFocus();
      }

      this.postalCode.setOnEditorActionListener(i.a(this));
      p var5 = this.a;
      var5.getClass();
      AddressAdapter.a var4 = j.a(var5);
      var5 = this.a;
      var5.getClass();
      this.b = new AddressAdapter(var4, k.a(var5), this.getString(2131361998));
      this.addressesRecyclerView.setLayoutManager(new LinearLayoutManager(this));
      this.addressesRecyclerView.setAdapter(this.b);
      this.a.a((p.a)this);
      this.setTitle(this.getString(2131362775));
   }

   protected void onDestroy() {
      this.a.b();
      super.onDestroy();
   }
}
