package co.uk.getmondo.common.address;

import android.view.View;
import android.view.View.OnClickListener;

// $FF: synthetic class
final class b implements OnClickListener {
   private final AddressAdapter.EnterAddressViewHolder a;

   private b(AddressAdapter.EnterAddressViewHolder var1) {
      this.a = var1;
   }

   public static OnClickListener a(AddressAdapter.EnterAddressViewHolder var0) {
      return new b(var0);
   }

   public void onClick(View var1) {
      AddressAdapter.EnterAddressViewHolder.a(this.a, var1);
   }
}
