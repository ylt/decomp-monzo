package co.uk.getmondo.common.address;

import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class LegacyEnterAddressActivity_ViewBinding implements Unbinder {
   private LegacyEnterAddressActivity a;

   public LegacyEnterAddressActivity_ViewBinding(LegacyEnterAddressActivity var1, View var2) {
      this.a = var1;
      var1.postalCodeWrapper = (TextInputLayout)Utils.findRequiredViewAsType(var2, 2131820897, "field 'postalCodeWrapper'", TextInputLayout.class);
      var1.postalCodeView = (EditText)Utils.findRequiredViewAsType(var2, 2131820898, "field 'postalCodeView'", EditText.class);
      var1.streetAddressView = (EditText)Utils.findRequiredViewAsType(var2, 2131820896, "field 'streetAddressView'", EditText.class);
      var1.streetAddressWrapper = (TextInputLayout)Utils.findRequiredViewAsType(var2, 2131820895, "field 'streetAddressWrapper'", TextInputLayout.class);
      var1.cityWrapper = (TextInputLayout)Utils.findRequiredViewAsType(var2, 2131820899, "field 'cityWrapper'", TextInputLayout.class);
      var1.cityView = (EditText)Utils.findRequiredViewAsType(var2, 2131820900, "field 'cityView'", EditText.class);
      var1.countryView = (EditText)Utils.findRequiredViewAsType(var2, 2131820901, "field 'countryView'", EditText.class);
      var1.submitButton = (Button)Utils.findRequiredViewAsType(var2, 2131820902, "field 'submitButton'", Button.class);
   }

   public void unbind() {
      LegacyEnterAddressActivity var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.postalCodeWrapper = null;
         var1.postalCodeView = null;
         var1.streetAddressView = null;
         var1.streetAddressWrapper = null;
         var1.cityWrapper = null;
         var1.cityView = null;
         var1.countryView = null;
         var1.submitButton = null;
      }
   }
}
