package co.uk.getmondo.common.address;

import android.view.View;
import android.view.View.OnClickListener;

// $FF: synthetic class
final class a implements OnClickListener {
   private final AddressAdapter.AddressViewHolder a;
   private final co.uk.getmondo.d.s b;

   private a(AddressAdapter.AddressViewHolder var1, co.uk.getmondo.d.s var2) {
      this.a = var1;
      this.b = var2;
   }

   public static OnClickListener a(AddressAdapter.AddressViewHolder var0, co.uk.getmondo.d.s var1) {
      return new a(var0, var1);
   }

   public void onClick(View var1) {
      AddressAdapter.AddressViewHolder.a(this.a, this.b, var1);
   }
}
