package co.uk.getmondo.common.address;

import co.uk.getmondo.api.MonzoApi;
import co.uk.getmondo.api.model.ApiPostcodeResponse;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.d.ak;
import io.reactivex.z;
import java.util.List;

public class p extends co.uk.getmondo.common.ui.b {
   private final io.reactivex.u c;
   private final io.reactivex.u d;
   private final co.uk.getmondo.common.accounts.d e;
   private final co.uk.getmondo.common.e.a f;
   private final MonzoApi g;
   private final co.uk.getmondo.common.a h;
   private final co.uk.getmondo.d.a.i i = new co.uk.getmondo.d.a.i();
   private boolean j;

   p(io.reactivex.u var1, io.reactivex.u var2, co.uk.getmondo.common.accounts.d var3, co.uk.getmondo.common.e.a var4, MonzoApi var5, co.uk.getmondo.common.a var6, boolean var7) {
      this.c = var1;
      this.d = var2;
      this.e = var3;
      this.f = var4;
      this.g = var5;
      this.h = var6;
      this.j = var7;
   }

   // $FF: synthetic method
   static List a(ApiPostcodeResponse var0) throws Exception {
      return var0.a();
   }

   // $FF: synthetic method
   static void a(p var0, Throwable var1) throws Exception {
      var0.f.a(var1, (co.uk.getmondo.common.e.a.a)var0.a);
   }

   // $FF: synthetic method
   static void a(p var0, List var1) throws Exception {
      if(var1.isEmpty()) {
         ((p.a)var0.a).a();
         ((p.a)var0.a).c();
      } else {
         ((p.a)var0.a).b();
         ((p.a)var0.a).d();
         ((p.a)var0.a).a(var1);
      }

   }

   // $FF: synthetic method
   static void a(p var0, List var1, Throwable var2) throws Exception {
      ((p.a)var0.a).t();
   }

   // $FF: synthetic method
   static z b(p var0, List var1) throws Exception {
      io.reactivex.n var3 = io.reactivex.n.fromIterable(var1);
      co.uk.getmondo.d.a.i var2 = var0.i;
      var2.getClass();
      return var3.map(v.a(var2)).toList();
   }

   private void b(String var1) {
      ((p.a)this.a).s();
      ((p.a)this.a).g();
      this.a((io.reactivex.b.b)this.g.lookupPostcode(var1, co.uk.getmondo.d.i.i().e()).d(q.a()).a(r.a(this)).b(this.d).a(this.c).a(s.a(this)).a(t.a(this), u.a(this)));
   }

   void a() {
      ((p.a)this.a).f();
   }

   public void a(p.a var1) {
      super.a((co.uk.getmondo.common.ui.f)var1);
      co.uk.getmondo.common.a var3 = this.h;
      Impression var2;
      if(this.j) {
         var2 = Impression.q();
      } else {
         var2 = Impression.F();
      }

      var3.a(var2);
      if(this.j) {
         String var4 = this.e.b().d().h().c();
         var1.a(var4);
         this.b(var4);
      } else {
         var1.a();
      }

   }

   void a(co.uk.getmondo.d.s var1) {
      if(this.j) {
         ak var2 = this.e.b();
         this.e.a(var2.d().a(var1));
         ((p.a)this.a).e();
      } else {
         this.h.a(Impression.G());
         ((p.a)this.a).a(var1);
      }

   }

   void a(String var1) {
      var1 = var1.trim();
      if(!var1.isEmpty()) {
         this.b(var1);
      }

   }

   interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      void a();

      void a(co.uk.getmondo.d.s var1);

      void a(String var1);

      void a(List var1);

      void b();

      void c();

      void d();

      void e();

      void f();

      void g();

      void s();

      void t();
   }
}
