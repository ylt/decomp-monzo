package co.uk.getmondo.common.address;

import co.uk.getmondo.api.model.tracking.Impression;

class e extends co.uk.getmondo.common.ui.b {
   private final co.uk.getmondo.common.a c;
   private co.uk.getmondo.d.s d;

   e(co.uk.getmondo.common.a var1) {
      this.c = var1;
   }

   // $FF: synthetic method
   static void a(e.a var0, co.uk.getmondo.common.address.a.a.a var1) throws Exception {
      String var2 = var1.a().trim();
      String var3 = var1.b().trim();
      String var4 = var1.c().trim();
      if(co.uk.getmondo.common.k.p.d(var2)) {
         var0.b();
      } else if(co.uk.getmondo.common.k.p.d(var3)) {
         var0.c();
      } else if(co.uk.getmondo.common.k.p.d(var4)) {
         var0.d();
      } else {
         var0.a(new co.uk.getmondo.d.s(var3, co.uk.getmondo.common.k.a.a(var2), var4, co.uk.getmondo.d.i.i().e(), (String)null));
      }

   }

   // $FF: synthetic method
   static void b(e.a var0, co.uk.getmondo.common.address.a.a.a var1) throws Exception {
      var0.e();
   }

   public void a(e.a var1) {
      super.a((co.uk.getmondo.common.ui.f)var1);
      this.c.a(Impression.r());
      if(this.d != null) {
         if(this.d.f()) {
            var1.a(co.uk.getmondo.common.k.a.a(this.d.h()));
         }

         var1.d(this.d.c());
         if(this.d.g()) {
            var1.e(this.d.i());
         }
      }

      var1.f(co.uk.getmondo.d.i.i().b());
      this.a((io.reactivex.b.b)var1.a().doOnNext(f.a(var1)).subscribe(g.a(var1)));
   }

   public void a(co.uk.getmondo.d.s var1) {
      this.d = var1;
   }

   interface a extends co.uk.getmondo.common.ui.f {
      io.reactivex.n a();

      void a(co.uk.getmondo.d.s var1);

      void a(String var1);

      void b();

      void c();

      void d();

      void d(String var1);

      void e();

      void e(String var1);

      void f(String var1);
   }
}
