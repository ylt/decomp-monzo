package co.uk.getmondo.common.address;

import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class AddressAdapter$AddressViewHolder_ViewBinding implements Unbinder {
   private AddressAdapter.AddressViewHolder a;

   public AddressAdapter$AddressViewHolder_ViewBinding(AddressAdapter.AddressViewHolder var1, View var2) {
      this.a = var1;
      var1.addressLine1 = (TextView)Utils.findRequiredViewAsType(var2, 2131821583, "field 'addressLine1'", TextView.class);
      var1.addressLine2 = (TextView)Utils.findRequiredViewAsType(var2, 2131821584, "field 'addressLine2'", TextView.class);
   }

   public void unbind() {
      AddressAdapter.AddressViewHolder var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.addressLine1 = null;
         var1.addressLine2 = null;
      }
   }
}
