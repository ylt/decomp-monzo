package co.uk.getmondo.common.address.a.a;

import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\f\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003¢\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003HÆ\u0003J\t\u0010\f\u001a\u00020\u0003HÆ\u0003J\t\u0010\r\u001a\u00020\u0003HÆ\u0003J'\u0010\u000e\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\u000f\u001a\u00020\u00102\b\u0010\u0011\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0012\u001a\u00020\u0013HÖ\u0001J\t\u0010\u0014\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0005\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\b¨\u0006\u0015"},
   d2 = {"Lco/uk/getmondo/common/address/data/model/AddressForm;", "", "street", "", "postCode", "city", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getCity", "()Ljava/lang/String;", "getPostCode", "getStreet", "component1", "component2", "component3", "copy", "equals", "", "other", "hashCode", "", "toString", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class a {
   private final String a;
   private final String b;
   private final String c;

   public a(String var1, String var2, String var3) {
      l.b(var1, "street");
      l.b(var2, "postCode");
      l.b(var3, "city");
      super();
      this.a = var1;
      this.b = var2;
      this.c = var3;
   }

   public final String a() {
      return this.a;
   }

   public final String b() {
      return this.b;
   }

   public final String c() {
      return this.c;
   }

   public boolean equals(Object var1) {
      boolean var2;
      if(this != var1) {
         label30: {
            if(var1 instanceof a) {
               a var3 = (a)var1;
               if(l.a(this.a, var3.a) && l.a(this.b, var3.b) && l.a(this.c, var3.c)) {
                  break label30;
               }
            }

            var2 = false;
            return var2;
         }
      }

      var2 = true;
      return var2;
   }

   public int hashCode() {
      int var3 = 0;
      String var4 = this.a;
      int var1;
      if(var4 != null) {
         var1 = var4.hashCode();
      } else {
         var1 = 0;
      }

      var4 = this.b;
      int var2;
      if(var4 != null) {
         var2 = var4.hashCode();
      } else {
         var2 = 0;
      }

      var4 = this.c;
      if(var4 != null) {
         var3 = var4.hashCode();
      }

      return (var2 + var1 * 31) * 31 + var3;
   }

   public String toString() {
      return "AddressForm(street=" + this.a + ", postCode=" + this.b + ", city=" + this.c + ")";
   }
}
