package co.uk.getmondo.common.address;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import java.util.ArrayList;
import java.util.List;

@Deprecated
public class AddressAdapter extends android.support.v7.widget.RecyclerView.a {
   private final AddressAdapter.a a;
   private final AddressAdapter.b b;
   private final List c = new ArrayList();
   private String d;

   public AddressAdapter(AddressAdapter.a var1, AddressAdapter.b var2, String var3) {
      this.a = var1;
      this.b = var2;
      this.d = var3;
   }

   public void a() {
      int var1 = this.c.size();
      this.c.clear();
      this.notifyItemRangeRemoved(0, var1);
   }

   public void a(String var1) {
      this.d = var1;
      this.notifyItemChanged(this.getItemCount() - 1);
   }

   public void a(List var1) {
      int var2 = this.c.size();
      this.c.clear();
      this.notifyItemRangeRemoved(0, var2);
      this.c.addAll(var1);
      this.notifyItemRangeInserted(0, var1.size());
   }

   public int getItemCount() {
      return this.c.size() + 1;
   }

   public int getItemViewType(int var1) {
      if(var1 == this.c.size()) {
         var1 = 1;
      } else if(var1 < this.c.size()) {
         var1 = 0;
      } else {
         var1 = super.getItemViewType(var1);
      }

      return var1;
   }

   public void onBindViewHolder(android.support.v7.widget.RecyclerView.w var1, int var2) {
      if(var1 instanceof AddressAdapter.AddressViewHolder) {
         ((AddressAdapter.AddressViewHolder)var1).a((co.uk.getmondo.d.s)this.c.get(var2));
      } else {
         if(!(var1 instanceof AddressAdapter.EnterAddressViewHolder)) {
            throw new RuntimeException("Unknown view holder type");
         }

         ((AddressAdapter.EnterAddressViewHolder)var1).a(this.d);
      }

   }

   public android.support.v7.widget.RecyclerView.w onCreateViewHolder(ViewGroup var1, int var2) {
      Object var3;
      switch(var2) {
      case 0:
         var3 = new AddressAdapter.AddressViewHolder(LayoutInflater.from(var1.getContext()).inflate(2131034360, var1, false), this.a);
         break;
      case 1:
         var3 = new AddressAdapter.EnterAddressViewHolder(LayoutInflater.from(var1.getContext()).inflate(2131034361, var1, false), this.b);
         break;
      default:
         throw new RuntimeException("Unknown view type");
      }

      return (android.support.v7.widget.RecyclerView.w)var3;
   }

   class AddressViewHolder extends android.support.v7.widget.RecyclerView.w {
      @BindView(2131821583)
      TextView addressLine1;
      @BindView(2131821584)
      TextView addressLine2;
      private final AddressAdapter.a b;

      public AddressViewHolder(View var2, AddressAdapter.a var3) {
         super(var2);
         ButterKnife.bind(this, (View)var2);
         this.b = var3;
      }

      // $FF: synthetic method
      static void a(AddressAdapter.AddressViewHolder var0, co.uk.getmondo.d.s var1, View var2) {
         if(var0.b != null) {
            var0.b.a(var1);
         }

      }

      public void a(co.uk.getmondo.d.s var1) {
         String var2 = co.uk.getmondo.common.k.a.a(var1.h());
         this.addressLine1.setText(var2);
         this.addressLine2.setText(this.itemView.getResources().getString(2131361995, new Object[]{var1.c(), var1.i()}));
         this.itemView.setOnClickListener(a.a(this, var1));
      }
   }

   class EnterAddressViewHolder extends android.support.v7.widget.RecyclerView.w {
      @BindView(2131821585)
      TextView addressNotInListText;
      private final AddressAdapter.b b;

      public EnterAddressViewHolder(View var2, AddressAdapter.b var3) {
         super(var2);
         this.b = var3;
         ButterKnife.bind(this, (View)var2);
      }

      // $FF: synthetic method
      static void a(AddressAdapter.EnterAddressViewHolder var0, View var1) {
         if(var0.b != null) {
            var0.b.a();
         }

      }

      public void a(String var1) {
         this.addressNotInListText.setText(var1);
         this.itemView.setOnClickListener(b.a(this));
      }
   }

   public interface a {
      void a(co.uk.getmondo.d.s var1);
   }

   public interface b {
      void a();
   }
}
