package co.uk.getmondo.common.address;

import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class SelectAddressActivity_ViewBinding implements Unbinder {
   private SelectAddressActivity a;

   public SelectAddressActivity_ViewBinding(SelectAddressActivity var1, View var2) {
      this.a = var1;
      var1.postalCodeWrapper = (TextInputLayout)Utils.findRequiredViewAsType(var2, 2131820869, "field 'postalCodeWrapper'", TextInputLayout.class);
      var1.selectAddressLabel = (TextView)Utils.findRequiredViewAsType(var2, 2131821053, "field 'selectAddressLabel'", TextView.class);
      var1.postalCode = (EditText)Utils.findRequiredViewAsType(var2, 2131820870, "field 'postalCode'", EditText.class);
      var1.addressesRecyclerView = (RecyclerView)Utils.findRequiredViewAsType(var2, 2131821054, "field 'addressesRecyclerView'", RecyclerView.class);
   }

   public void unbind() {
      SelectAddressActivity var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.postalCodeWrapper = null;
         var1.selectAddressLabel = null;
         var1.postalCode = null;
         var1.addressesRecyclerView = null;
      }
   }
}
