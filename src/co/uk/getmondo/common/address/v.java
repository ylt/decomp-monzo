package co.uk.getmondo.common.address;

import co.uk.getmondo.api.model.LegacyApiAddress;

// $FF: synthetic class
final class v implements io.reactivex.c.h {
   private final co.uk.getmondo.d.a.i a;

   private v(co.uk.getmondo.d.a.i var1) {
      this.a = var1;
   }

   public static io.reactivex.c.h a(co.uk.getmondo.d.a.i var0) {
      return new v(var0);
   }

   public Object a(Object var1) {
      return this.a.a((LegacyApiAddress)var1);
   }
}
