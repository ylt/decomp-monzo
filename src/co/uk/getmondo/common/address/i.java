package co.uk.getmondo.common.address;

import android.view.KeyEvent;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

// $FF: synthetic class
final class i implements OnEditorActionListener {
   private final SelectAddressActivity a;

   private i(SelectAddressActivity var1) {
      this.a = var1;
   }

   public static OnEditorActionListener a(SelectAddressActivity var0) {
      return new i(var0);
   }

   public boolean onEditorAction(TextView var1, int var2, KeyEvent var3) {
      return SelectAddressActivity.a(this.a, var1, var2, var3);
   }
}
