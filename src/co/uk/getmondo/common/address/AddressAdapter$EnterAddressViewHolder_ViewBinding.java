package co.uk.getmondo.common.address;

import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class AddressAdapter$EnterAddressViewHolder_ViewBinding implements Unbinder {
   private AddressAdapter.EnterAddressViewHolder a;

   public AddressAdapter$EnterAddressViewHolder_ViewBinding(AddressAdapter.EnterAddressViewHolder var1, View var2) {
      this.a = var1;
      var1.addressNotInListText = (TextView)Utils.findRequiredViewAsType(var2, 2131821585, "field 'addressNotInListText'", TextView.class);
   }

   public void unbind() {
      AddressAdapter.EnterAddressViewHolder var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.addressNotInListText = null;
      }
   }
}
