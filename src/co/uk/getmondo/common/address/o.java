package co.uk.getmondo.common.address;

public final class o implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final n b;

   static {
      boolean var0;
      if(!o.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   public o(n var1) {
      if(!a && var1 == null) {
         throw new AssertionError();
      } else {
         this.b = var1;
      }
   }

   public static b.a.b a(n var0) {
      return new o(var0);
   }

   public Boolean a() {
      return (Boolean)b.a.d.a(Boolean.valueOf(this.b.a()), "Cannot return null from a non-@Nullable @Provides method");
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
