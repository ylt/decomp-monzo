package co.uk.getmondo.common.i;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.util.Locale;
import org.joda.money.CurrencyUnit;
import org.joda.money.IllegalCurrencyException;

public class c implements Parcelable {
   public static final Creator CREATOR = new Creator() {
      public c a(Parcel var1) {
         return new c(var1);
      }

      public c[] a(int var1) {
         return new c[var1];
      }

      // $FF: synthetic method
      public Object createFromParcel(Parcel var1) {
         return this.a(var1);
      }

      // $FF: synthetic method
      public Object[] newArray(int var1) {
         return this.a(var1);
      }
   };
   public static final c a = new c("GBP");
   private CurrencyUnit b;
   private String c;

   protected c(Parcel var1) {
      this.b = (CurrencyUnit)var1.readSerializable();
      this.c = var1.readString();
   }

   public c(String var1) {
      Object var2;
      try {
         this.b = CurrencyUnit.of(var1);
         return;
      } catch (IllegalCurrencyException var4) {
         var2 = var4;
      } catch (NullPointerException var5) {
         var2 = var5;
      }

      this.b = CurrencyUnit.GBP;
      String var3 = var1;
      if(var1 == null) {
         var3 = "";
      }

      this.c = var3;
      d.a.a.a((Throwable)(new IllegalArgumentException("Invalid currency code", (Throwable)var2)));
   }

   public int a() {
      return this.b.getDecimalPlaces();
   }

   public String a(Locale var1) {
      String var2;
      if(this.c != null) {
         var2 = this.c;
      } else {
         var2 = this.b.getSymbol(var1);
      }

      return var2;
   }

   public String b() {
      String var1;
      if(this.c != null) {
         var1 = this.c;
      } else {
         var1 = this.b.getCurrencyCode();
      }

      return var1;
   }

   public int describeContents() {
      return 0;
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(this != var1) {
         if(var1 != null && this.getClass() == var1.getClass()) {
            c var3;
            label35: {
               var3 = (c)var1;
               if(this.b != null) {
                  if(this.b.equals(var3.b)) {
                     break label35;
                  }
               } else if(var3.b == null) {
                  break label35;
               }

               var2 = false;
               return var2;
            }

            if(this.c != null) {
               var2 = this.c.equals(var3.c);
            } else if(var3.c != null) {
               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public int hashCode() {
      int var2 = 0;
      int var1;
      if(this.b != null) {
         var1 = this.b.hashCode();
      } else {
         var1 = 0;
      }

      if(this.c != null) {
         var2 = this.c.hashCode();
      }

      return var1 * 31 + var2;
   }

   public void writeToParcel(Parcel var1, int var2) {
      var1.writeSerializable(this.b);
      var1.writeString(this.c);
   }
}
