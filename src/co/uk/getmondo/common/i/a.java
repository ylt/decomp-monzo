package co.uk.getmondo.common.i;

public class a {
   private final c a;
   private long b;
   private int c;

   public a(c var1) {
      this.a = var1;
      this.b = 0L;
      this.c = 0;
   }

   public co.uk.getmondo.d.c a() {
      return new co.uk.getmondo.d.c(this.b, this.a);
   }

   public void a(co.uk.getmondo.d.c var1) {
      if(var1 != null) {
         if(!this.a.b().equals(var1.l().b())) {
            throw new IllegalArgumentException("This amount aggregator only supports amounts with currency " + this.a.b());
         }

         this.b += var1.k();
         ++this.c;
      }

   }

   public int b() {
      return this.c;
   }

   public void c() {
      this.b = 0L;
      this.c = 0;
   }
}
