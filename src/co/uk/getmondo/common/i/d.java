package co.uk.getmondo.common.i;

import java.util.Locale;

public class d {
   public static int a(long var0, int var2) {
      if(var2 <= 0) {
         var2 = 0;
      } else {
         var2 = (int)Math.abs(var0) % (int)Math.pow(10.0D, (double)var2);
      }

      return var2;
   }

   public static String a(int var0, int var1) {
      String var2;
      if(var1 <= 0) {
         var2 = "";
      } else {
         var2 = String.format(Locale.UK, ".%0" + var1 + "d", new Object[]{Integer.valueOf(var0)});
      }

      return var2;
   }

   public static long b(long var0, int var2) {
      if(var2 <= 0) {
         var0 = Math.abs(var0);
      } else {
         var0 = Math.abs(var0) / (long)((int)Math.pow(10.0D, (double)var2));
      }

      return var0;
   }

   public static double c(long var0, int var2) {
      double var3;
      if(var2 <= 0) {
         var3 = (double)var0;
      } else {
         var3 = (double)var0 / Math.pow(10.0D, (double)var2);
      }

      return var3;
   }

   public static long d(long var0, int var2) {
      long var4;
      if(var0 != 0L && var2 > 0) {
         var4 = Math.abs(var0);
         var2 = (int)Math.pow(10.0D, (double)var2);
         int var3 = (int)var4 % var2;
         long var6;
         if(var3 <= var2 / 2) {
            var6 = var4 - (long)var3;
         } else {
            var6 = var4 - (long)var3 + (long)var2;
         }

         var4 = var6;
         if(var0 < 0L) {
            var4 = var6 * -1L;
         }
      } else {
         var4 = var0;
      }

      return var4;
   }

   public static long e(long var0, int var2) {
      long var6;
      if(var0 != 0L && var2 > 0) {
         var6 = Math.abs(var0);
         var2 = (int)Math.pow(10.0D, (double)var2);
         int var3 = (int)var6 % var2;
         long var4 = var6;
         if(var3 > 0) {
            var4 = var6 - (long)var3 + (long)var2;
         }

         var6 = var4;
         if(var0 < 0L) {
            var6 = var4 * -1L;
         }
      } else {
         var6 = var0;
      }

      return var6;
   }
}
