package co.uk.getmondo.common.i;

import java.text.NumberFormat;
import java.util.Locale;
import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 \u000f2\u00020\u0001:\u0001\u000fB%\b\u0007\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0005\u001a\u00020\u0003¢\u0006\u0002\u0010\u0006J\u000e\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0005\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\bR\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\b¨\u0006\u0010"},
   d2 = {"Lco/uk/getmondo/common/money/AmountFormatter;", "", "alwaysPrintFractionalPart", "", "showNegative", "hideCurrency", "(ZZZ)V", "getAlwaysPrintFractionalPart", "()Z", "getHideCurrency", "getShowNegative", "format", "", "amount", "Lco/uk/getmondo/model/Amount;", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class b {
   public static final b.a a = new b.a((i)null);
   private final boolean b;
   private final boolean c;
   private final boolean d;

   public b() {
      this(false, false, false, 7, (i)null);
   }

   public b(boolean var1, boolean var2, boolean var3) {
      this.b = var1;
      this.c = var2;
      this.d = var3;
   }

   // $FF: synthetic method
   public b(boolean var1, boolean var2, boolean var3, int var4, i var5) {
      if((var4 & 1) != 0) {
         var1 = false;
      }

      if((var4 & 2) != 0) {
         var2 = false;
      }

      if((var4 & 4) != 0) {
         var3 = false;
      }

      this(var1, var2, var3);
   }

   public final String a(co.uk.getmondo.d.c var1) {
      l.b(var1, "amount");
      String var5;
      if(this.d) {
         var5 = "";
      } else {
         var5 = var1.g();
      }

      String var6;
      if(this.c && var1.a()) {
         var6 = "-";
      } else {
         var6 = "";
      }

      String var7 = a.a(var1.c());
      int var3 = var1.d();
      int var4 = var1.l().a();
      boolean var2;
      if(!this.b && var3 <= 0) {
         var2 = false;
      } else {
         var2 = true;
      }

      String var8;
      if(var2) {
         var8 = d.a(var3, var4);
      } else {
         var8 = "";
      }

      return var5 + var6 + var7 + var8;
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007¨\u0006\u0007"},
      d2 = {"Lco/uk/getmondo/common/money/AmountFormatter$Companion;", "", "()V", "formatIntegerPart", "", "absoluteIntegerPart", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(i var1) {
         this();
      }

      public final String a(long var1) {
         String var3 = NumberFormat.getNumberInstance(Locale.getDefault()).format(var1);
         l.a(var3, "NumberFormat.getNumberIn…rmat(absoluteIntegerPart)");
         return var3;
      }
   }
}
