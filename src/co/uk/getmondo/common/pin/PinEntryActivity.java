package co.uk.getmondo.common.pin;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import co.uk.getmondo.common.ae;
import co.uk.getmondo.common.activities.ConfirmationActivity;
import co.uk.getmondo.common.ui.PinEntryView;
import co.uk.getmondo.pin.ForgotPinActivity;
import io.reactivex.n;
import io.reactivex.o;
import io.reactivex.p;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.w;
import kotlin.d.b.y;
import kotlin.reflect.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\b\u0018\u0000 (2\u00020\u00012\u00020\u0002:\u0001(B\u0005¢\u0006\u0002\u0010\u0003J\b\u0010\u0011\u001a\u00020\u0012H\u0016J\b\u0010\u0013\u001a\u00020\u0012H\u0016J\"\u0010\u0014\u001a\u00020\u00122\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00162\b\u0010\u0018\u001a\u0004\u0018\u00010\u0019H\u0014J\u0012\u0010\u001a\u001a\u00020\u00122\b\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u0014J\u000e\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u00120\u001eH\u0016J\u000e\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020\u00120\u001eH\u0016J\u000e\u0010 \u001a\b\u0012\u0004\u0012\u00020!0\u001eH\u0016J\u0010\u0010\"\u001a\u00020\u00122\u0006\u0010\u0004\u001a\u00020\u0005H\u0016J\b\u0010#\u001a\u00020\u0012H\u0016J\b\u0010$\u001a\u00020\u0012H\u0016J\b\u0010%\u001a\u00020\u0012H\u0016J\b\u0010&\u001a\u00020\u0012H\u0016J\b\u0010'\u001a\u00020\u0012H\u0016R#\u0010\u0004\u001a\n \u0006*\u0004\u0018\u00010\u00050\u00058BX\u0082\u0084\u0002¢\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u0007\u0010\bR\u001e\u0010\u000b\u001a\u00020\f8\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010¨\u0006)"},
   d2 = {"Lco/uk/getmondo/common/pin/PinEntryActivity;", "Lco/uk/getmondo/common/activities/BaseActivity;", "Lco/uk/getmondo/common/pin/PinEntryPresenter$View;", "()V", "operation", "Lco/uk/getmondo/common/pin/data/model/PinOperation;", "kotlin.jvm.PlatformType", "getOperation", "()Lco/uk/getmondo/common/pin/data/model/PinOperation;", "operation$delegate", "Lkotlin/Lazy;", "presenter", "Lco/uk/getmondo/common/pin/PinEntryPresenter;", "getPresenter", "()Lco/uk/getmondo/common/pin/PinEntryPresenter;", "setPresenter", "(Lco/uk/getmondo/common/pin/PinEntryPresenter;)V", "hideLoading", "", "hidePinError", "onActivityResult", "requestCode", "", "resultCode", "data", "Landroid/content/Intent;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onForgotPin", "Lio/reactivex/Observable;", "onPinClicked", "onPinEntered", "", "openConfirmation", "showForgotPin", "showGenericError", "showLoading", "showPinBlocked", "showPinError", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class PinEntryActivity extends co.uk.getmondo.common.activities.b implements c.a {
   // $FF: synthetic field
   static final l[] a = new l[]{(l)y.a(new w(y.a(PinEntryActivity.class), "operation", "getOperation()Lco/uk/getmondo/common/pin/data/model/PinOperation;"))};
   public static final PinEntryActivity.a c = new PinEntryActivity.a((i)null);
   public c b;
   private final kotlin.c e = kotlin.d.a((kotlin.d.a.a)(new kotlin.d.a.a() {
      public final co.uk.getmondo.common.pin.a.a.b b() {
         return (co.uk.getmondo.common.pin.a.a.b)PinEntryActivity.this.getIntent().getParcelableExtra("operation");
      }

      // $FF: synthetic method
      public Object v_() {
         return this.b();
      }
   }));
   private HashMap f;

   private final co.uk.getmondo.common.pin.a.a.b k() {
      kotlin.c var2 = this.e;
      l var1 = a[0];
      return (co.uk.getmondo.common.pin.a.a.b)var2.a();
   }

   public View a(int var1) {
      if(this.f == null) {
         this.f = new HashMap();
      }

      View var3 = (View)this.f.get(Integer.valueOf(var1));
      View var2 = var3;
      if(var3 == null) {
         var2 = this.findViewById(var1);
         this.f.put(Integer.valueOf(var1), var2);
      }

      return var2;
   }

   public n a() {
      n var1 = com.b.a.c.c.a((PinEntryView)this.a(co.uk.getmondo.c.a.pinEntry)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var1, "RxView.clicks(this).map(VoidToUnit)");
      return var1;
   }

   public void a(co.uk.getmondo.common.pin.a.a.b var1) {
      kotlin.d.b.l.b(var1, "operation");
      if(var1 instanceof co.uk.getmondo.common.pin.a.a.b.a) {
         ConfirmationActivity.a((Context)this, this.getString(2131362007), this.getString(2131362006), var1.a());
      }

   }

   public n b() {
      n var1 = n.create((p)(new p() {
         public final void a(final o var1) {
            kotlin.d.b.l.b(var1, "emitter");
            ((PinEntryView)PinEntryActivity.this.a(co.uk.getmondo.c.a.pinEntry)).setOnPinEnteredListener((PinEntryView.a)(new PinEntryView.a() {
               public final void a(String var1x) {
                  var1.a(var1x);
               }
            }));
            var1.a((io.reactivex.c.f)(new io.reactivex.c.f() {
               public final void a() {
                  ((PinEntryView)PinEntryActivity.this.a(co.uk.getmondo.c.a.pinEntry)).setOnPinEnteredListener((PinEntryView.a)null);
               }
            }));
         }
      }));
      kotlin.d.b.l.a(var1, "Observable.create { emit…istener(null) }\n        }");
      return var1;
   }

   public n c() {
      n var1 = com.b.a.c.c.a((Button)this.a(co.uk.getmondo.c.a.pinActionView)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var1, "RxView.clicks(this).map(VoidToUnit)");
      return var1;
   }

   public void d() {
      this.b(2131362178);
      ((PinEntryView)this.a(co.uk.getmondo.c.a.pinEntry)).a();
      ((PinEntryView)this.a(co.uk.getmondo.c.a.pinEntry)).a(true);
   }

   public void e() {
      ((PinEntryView)this.a(co.uk.getmondo.c.a.pinEntry)).d();
   }

   public void f() {
      ((PinEntryView)this.a(co.uk.getmondo.c.a.pinEntry)).a(true);
      co.uk.getmondo.common.d.e.a(true).show(this.getFragmentManager(), co.uk.getmondo.common.d.e.class.getName());
   }

   public void g() {
      this.b(2131362198);
   }

   public void h() {
      this.startActivityForResult(new Intent((Context)this, ForgotPinActivity.class), 1);
   }

   public void i() {
      ae.c((PinEntryView)this.a(co.uk.getmondo.c.a.pinEntry));
      ae.a(this.a(co.uk.getmondo.c.a.progressOverlay));
      ae.a((View)((ProgressBar)this.a(co.uk.getmondo.c.a.progressBar)));
   }

   public void j() {
      ae.b((ProgressBar)this.a(co.uk.getmondo.c.a.progressBar));
      ae.b(this.a(co.uk.getmondo.c.a.progressOverlay));
   }

   protected void onActivityResult(int var1, int var2, Intent var3) {
      if(var1 == 1) {
         if(var3 != null && var3.hasExtra("pin_error_msg")) {
            this.b(var3.getStringExtra("pin_error_msg"));
         }
      } else {
         super.onActivityResult(var1, var2, var3);
      }

   }

   protected void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.setContentView(2131034198);
      co.uk.getmondo.common.h.b.b var3 = this.l();
      co.uk.getmondo.common.pin.a.a.b var2 = this.k();
      kotlin.d.b.l.a(var2, "operation");
      var3.a(new g(var2)).a(this);
      c var4 = this.b;
      if(var4 == null) {
         kotlin.d.b.l.b("presenter");
      }

      var4.a((c.a)this);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0016\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fR\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082T¢\u0006\u0002\n\u0000¨\u0006\r"},
      d2 = {"Lco/uk/getmondo/common/pin/PinEntryActivity$Companion;", "", "()V", "KEY_OPERATION", "", "REQUEST_CODE_PIN", "", "buildIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "operation", "Lco/uk/getmondo/common/pin/data/model/PinOperation;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(i var1) {
         this();
      }

      public final Intent a(Context var1, co.uk.getmondo.common.pin.a.a.b var2) {
         kotlin.d.b.l.b(var1, "context");
         kotlin.d.b.l.b(var2, "operation");
         Intent var3 = (new Intent(var1, PinEntryActivity.class)).putExtra("operation", (Parcelable)var2);
         kotlin.d.b.l.a(var3, "Intent(context, PinEntry…KEY_OPERATION, operation)");
         return var3;
      }
   }
}
