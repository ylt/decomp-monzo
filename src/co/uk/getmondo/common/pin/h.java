package co.uk.getmondo.common.pin;

public final class h implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final g b;

   static {
      boolean var0;
      if(!h.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   public h(g var1) {
      if(!a && var1 == null) {
         throw new AssertionError();
      } else {
         this.b = var1;
      }
   }

   public static b.a.b a(g var0) {
      return new h(var0);
   }

   public co.uk.getmondo.common.pin.a.a.b a() {
      return (co.uk.getmondo.common.pin.a.a.b)b.a.d.a(this.b.a(), "Cannot return null from a non-@Nullable @Provides method");
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
