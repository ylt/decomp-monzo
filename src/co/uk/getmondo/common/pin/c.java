package co.uk.getmondo.common.pin;

import co.uk.getmondo.api.ApiException;
import co.uk.getmondo.api.model.tracking.Impression;
import io.reactivex.n;
import io.reactivex.u;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\b\u0004\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0015B;\b\u0007\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\f\u001a\u00020\r¢\u0006\u0002\u0010\u000eJ\u0018\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0002H\u0002J\u0010\u0010\u0014\u001a\u00020\u00102\u0006\u0010\u0013\u001a\u00020\u0002H\u0016R\u000e\u0010\f\u001a\u00020\rX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0016"},
   d2 = {"Lco/uk/getmondo/common/pin/PinEntryPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/common/pin/PinEntryPresenter$View;", "ioScheduler", "Lio/reactivex/Scheduler;", "uiScheduler", "apiErrorHandler", "Lco/uk/getmondo/common/errors/ApiErrorHandler;", "operation", "Lco/uk/getmondo/common/pin/data/model/PinOperation;", "profileManager", "Lco/uk/getmondo/profile/data/ProfileManager;", "analytics", "Lco/uk/getmondo/common/AnalyticsService;", "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/common/pin/data/model/PinOperation;Lco/uk/getmondo/profile/data/ProfileManager;Lco/uk/getmondo/common/AnalyticsService;)V", "handleError", "", "throwable", "", "view", "register", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class c extends co.uk.getmondo.common.ui.b {
   private final u c;
   private final u d;
   private final co.uk.getmondo.common.e.a e;
   private final co.uk.getmondo.common.pin.a.a.b f;
   private final co.uk.getmondo.profile.data.a g;
   private final co.uk.getmondo.common.a h;

   public c(u var1, u var2, co.uk.getmondo.common.e.a var3, co.uk.getmondo.common.pin.a.a.b var4, co.uk.getmondo.profile.data.a var5, co.uk.getmondo.common.a var6) {
      l.b(var1, "ioScheduler");
      l.b(var2, "uiScheduler");
      l.b(var3, "apiErrorHandler");
      l.b(var4, "operation");
      l.b(var5, "profileManager");
      l.b(var6, "analytics");
      super();
      this.c = var1;
      this.d = var2;
      this.e = var3;
      this.f = var4;
      this.g = var5;
      this.h = var6;
   }

   private final void a(Throwable var1, c.a var2) {
      if(var1 instanceof ApiException) {
         co.uk.getmondo.common.e.f[] var4 = (co.uk.getmondo.common.e.f[])co.uk.getmondo.payments.a.h.values();
         co.uk.getmondo.api.model.b var3 = ((ApiException)var1).e();
         String var5;
         if(var3 != null) {
            var5 = var3.a();
         } else {
            var5 = null;
         }

         co.uk.getmondo.payments.a.h var6 = (co.uk.getmondo.payments.a.h)co.uk.getmondo.common.e.d.a(var4, var5);
         if(var6 != null) {
            switch(d.a[var6.ordinal()]) {
            case 1:
               var2.f();
               return;
            case 2:
               var2.d();
               return;
            default:
               return;
            }
         }
      }

      if(!this.e.a(var1, (co.uk.getmondo.common.e.a.a)var2)) {
         var2.g();
      }

   }

   public void a(final c.a var1) {
      l.b(var1, "view");
      super.a((co.uk.getmondo.common.ui.f)var1);
      this.h.a(Impression.Companion.b(this.f.b()));
      io.reactivex.b.a var4 = this.b;
      n var5 = var1.a();
      io.reactivex.c.g var6 = (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n var1x) {
            var1.e();
         }
      });
      kotlin.d.a.b var3 = (kotlin.d.a.b)null.a;
      Object var2 = var3;
      if(var3 != null) {
         var2 = new e(var3);
      }

      io.reactivex.b.b var9 = var5.subscribe(var6, (io.reactivex.c.g)var2);
      l.a(var9, "view.onPinClicked()\n    …ePinError() }, Timber::e)");
      this.b = co.uk.getmondo.common.j.f.a(var4, var9);
      var4 = this.b;
      var5 = var1.b().flatMapMaybe((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.h a(String var1x) {
            l.b(var1x, "it");
            if(c.this.f instanceof co.uk.getmondo.common.pin.a.a.b.a) {
               return co.uk.getmondo.common.j.f.a((io.reactivex.b)c.this.g.a(((co.uk.getmondo.common.pin.a.a.b.a)c.this.f).c(), co.uk.getmondo.common.pin.a.a.a.a.a(var1x)).b(c.this.c).a(c.this.d).c((io.reactivex.c.g)(new io.reactivex.c.g() {
                  public final void a(io.reactivex.b.b var1x) {
                     var1.i();
                  }
               })).b((io.reactivex.c.g)(new io.reactivex.c.g() {
                  public final void a(Throwable var1x) {
                     var1.j();
                  }
               })).b((io.reactivex.c.a)(new io.reactivex.c.a() {
                  public final void a() {
                     c.this.h.a(Impression.Companion.m(true));
                  }
               })).a((io.reactivex.c.g)(new io.reactivex.c.g() {
                  public final void a(Throwable var1x) {
                     c.this.h.a(Impression.Companion.m(false));
                     c var2 = c.this;
                     l.a(var1x, "it");
                     var2.a(var1x, var1);
                  }
               })), (Object)kotlin.n.a);
            } else {
               throw new NoWhenBranchMatchedException();
            }
         }
      }));
      var6 = (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n var1x) {
            var1.a(c.this.f);
         }
      });
      var3 = (kotlin.d.a.b)null.a;
      var2 = var3;
      if(var3 != null) {
         var2 = new e(var3);
      }

      var9 = var5.subscribe(var6, (io.reactivex.c.g)var2);
      l.a(var9, "view.onPinEntered()\n    …            }, Timber::e)");
      this.b = co.uk.getmondo.common.j.f.a(var4, var9);
      io.reactivex.b.a var10 = this.b;
      n var11 = var1.c();
      io.reactivex.c.g var13 = (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n var1x) {
            c.this.h.a(Impression.Companion.a(c.this.f.b()));
            var1.h();
         }
      });
      kotlin.d.a.b var12 = (kotlin.d.a.b)null.a;
      Object var7 = var12;
      if(var12 != null) {
         var7 = new e(var12);
      }

      io.reactivex.b.b var8 = var11.subscribe(var13, (io.reactivex.c.g)var7);
      l.a(var8, "view.onForgotPin()\n     …            }, Timber::e)");
      this.b = co.uk.getmondo.common.j.f.a(var10, var8);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\bf\u0018\u00002\u00020\u00012\u00020\u0002J\b\u0010\u0003\u001a\u00020\u0004H&J\b\u0010\u0005\u001a\u00020\u0004H&J\u000e\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00040\u0007H&J\u000e\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00040\u0007H&J\u000e\u0010\t\u001a\b\u0012\u0004\u0012\u00020\n0\u0007H&J\u0010\u0010\u000b\u001a\u00020\u00042\u0006\u0010\f\u001a\u00020\rH&J\b\u0010\u000e\u001a\u00020\u0004H&J\b\u0010\u000f\u001a\u00020\u0004H&J\b\u0010\u0010\u001a\u00020\u0004H&J\b\u0010\u0011\u001a\u00020\u0004H&J\b\u0010\u0012\u001a\u00020\u0004H&¨\u0006\u0013"},
      d2 = {"Lco/uk/getmondo/common/pin/PinEntryPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "hideLoading", "", "hidePinError", "onForgotPin", "Lio/reactivex/Observable;", "onPinClicked", "onPinEntered", "", "openConfirmation", "operation", "Lco/uk/getmondo/common/pin/data/model/PinOperation;", "showForgotPin", "showGenericError", "showLoading", "showPinBlocked", "showPinError", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      n a();

      void a(co.uk.getmondo.common.pin.a.a.b var1);

      n b();

      n c();

      void d();

      void e();

      void f();

      void g();

      void h();

      void i();

      void j();
   }
}
