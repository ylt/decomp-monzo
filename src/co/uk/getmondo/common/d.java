package co.uk.getmondo.common;

import co.uk.getmondo.api.AnalyticsApi;

public final class d implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;
   private final javax.a.a c;

   static {
      boolean var0;
      if(!d.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   public d(javax.a.a var1, javax.a.a var2) {
      if(!a && var1 == null) {
         throw new AssertionError();
      } else {
         this.b = var1;
         if(!a && var2 == null) {
            throw new AssertionError();
         } else {
            this.c = var2;
         }
      }
   }

   public static b.a.b a(javax.a.a var0, javax.a.a var1) {
      return new d(var0, var1);
   }

   public a a() {
      return new a((AnalyticsApi)this.b.b(), (io.reactivex.u)this.c.b());
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
