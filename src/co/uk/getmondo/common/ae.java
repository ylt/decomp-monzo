package co.uk.getmondo.common;

import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import kotlin.Metadata;
import kotlin.TypeCastException;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\u001a7\u0010\u0000\u001a\u00020\u0001\"\b\b\u0000\u0010\u0002*\u00020\u0003*\u0002H\u00022\u0019\b\u0004\u0010\u0004\u001a\u0013\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u00020\u00010\u0005¢\u0006\u0002\b\u0006H\u0086\b¢\u0006\u0002\u0010\u0007\u001a\u0012\u0010\b\u001a\u00020\t*\u00020\u00032\u0006\u0010\n\u001a\u00020\u000b\u001a\n\u0010\f\u001a\u00020\u0001*\u00020\u0003\u001a\n\u0010\r\u001a\u00020\u0001*\u00020\u0003\u001a\n\u0010\u000e\u001a\u00020\u0001*\u00020\u0003\u001a\u0014\u0010\u000f\u001a\u00020\u0001*\u00020\u00032\b\b\u0001\u0010\n\u001a\u00020\u000b\u001a\n\u0010\u0010\u001a\u00020\u0001*\u00020\u0011\u001a\n\u0010\u0012\u001a\u00020\u0001*\u00020\u0003¨\u0006\u0013"},
   d2 = {"afterLayout", "", "T", "Landroid/view/View;", "f", "Lkotlin/Function1;", "Lkotlin/ExtensionFunctionType;", "(Landroid/view/View;Lkotlin/jvm/functions/Function1;)V", "getString", "", "resId", "", "gone", "hideKeyboard", "invisible", "setHeight", "showKeyboard", "Landroid/widget/EditText;", "visible", "app_monzoPrepaidRelease"},
   k = 2,
   mv = {1, 1, 7}
)
public final class ae {
   public static final String a(View var0, int var1) {
      kotlin.d.b.l.b(var0, "$receiver");
      String var2 = var0.getResources().getString(var1);
      kotlin.d.b.l.a(var2, "resources.getString(resId)");
      return var2;
   }

   public static final void a(View var0) {
      kotlin.d.b.l.b(var0, "$receiver");
      var0.setVisibility(0);
   }

   public static final void a(EditText var0) {
      kotlin.d.b.l.b(var0, "$receiver");
      Object var1 = var0.getContext().getSystemService("input_method");
      if(var1 == null) {
         throw new TypeCastException("null cannot be cast to non-null type android.view.inputmethod.InputMethodManager");
      } else {
         ((InputMethodManager)var1).showSoftInput((View)var0, 1);
      }
   }

   public static final void b(View var0) {
      kotlin.d.b.l.b(var0, "$receiver");
      var0.setVisibility(8);
   }

   public static final void b(View var0, int var1) {
      kotlin.d.b.l.b(var0, "$receiver");
      var0.getLayoutParams().height = var0.getResources().getDimensionPixelSize(var1);
      var0.requestLayout();
   }

   public static final void c(View var0) {
      kotlin.d.b.l.b(var0, "$receiver");
      Object var1 = var0.getContext().getSystemService("input_method");
      if(var1 == null) {
         throw new TypeCastException("null cannot be cast to non-null type android.view.inputmethod.InputMethodManager");
      } else {
         ((InputMethodManager)var1).hideSoftInputFromWindow(var0.getWindowToken(), 0);
      }
   }
}
