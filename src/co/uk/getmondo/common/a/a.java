package co.uk.getmondo.common.a;

import android.accounts.Account;
import android.accounts.OnAccountsUpdateListener;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.ShortcutInfo;
import android.content.pm.ShortcutManager;
import android.content.pm.ShortcutInfo.Builder;
import android.graphics.drawable.Icon;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.common.accounts.k;
import co.uk.getmondo.d.ak;
import co.uk.getmondo.payments.send.data.h;
import co.uk.getmondo.payments.send.data.p;
import io.reactivex.n;
import io.reactivex.o;
import io.reactivex.r;
import io.reactivex.c.f;
import io.reactivex.c.g;
import java.util.Arrays;
import kotlin.Metadata;
import kotlin.a.m;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\\\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0007\u0018\u00002\u00020\u0001B5\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\f\u001a\u00020\r¢\u0006\u0002\u0010\u000eJ\u0018\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0002\u001a\u00020\u0003H\u0002J\b\u0010\u0016\u001a\u00020\u0017H\u0016J\u0012\u0010\u0018\u001a\u00020\u00172\b\u0010\u0019\u001a\u0004\u0018\u00010\u001aH\u0002J\u0010\u0010\u001b\u001a\u00020\u00172\u0006\u0010\u001c\u001a\u00020\u0015H\u0016J\u0018\u0010\u001b\u001a\u00020\u00172\u0006\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020\u001eH\u0016R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u0016\u0010\u000f\u001a\n \u0011*\u0004\u0018\u00010\u00100\u0010X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006 "},
   d2 = {"Lco/uk/getmondo/common/app_shortcuts/Api25AppShortcuts;", "Lco/uk/getmondo/common/app_shortcuts/AppShortcuts;", "context", "Landroid/content/Context;", "userSettingsStorage", "Lco/uk/getmondo/payments/send/data/UserSettingsStorage;", "userSettingsRepository", "Lco/uk/getmondo/payments/send/data/UserSettingsRepository;", "accountService", "Lco/uk/getmondo/common/accounts/AccountService;", "androidAccountManager", "Lco/uk/getmondo/common/accounts/AndroidAccountManager;", "analyticsService", "Lco/uk/getmondo/common/AnalyticsService;", "(Landroid/content/Context;Lco/uk/getmondo/payments/send/data/UserSettingsStorage;Lco/uk/getmondo/payments/send/data/UserSettingsRepository;Lco/uk/getmondo/common/accounts/AccountService;Lco/uk/getmondo/common/accounts/AndroidAccountManager;Lco/uk/getmondo/common/AnalyticsService;)V", "shortcutManager", "Landroid/content/pm/ShortcutManager;", "kotlin.jvm.PlatformType", "buildInfo", "Landroid/content/pm/ShortcutInfo;", "shortcut", "Lco/uk/getmondo/common/app_shortcuts/AppShortcut;", "initialise", "", "refresh", "user", "Lco/uk/getmondo/model/User;", "reportUsed", "appShortcut", "appShortcutId", "", "analyticEvent", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
@TargetApi(25)
public final class a implements c {
   private final ShortcutManager a;
   private final Context b;
   private final p c;
   private final h d;
   private final co.uk.getmondo.common.accounts.d e;
   private final k f;
   private final co.uk.getmondo.common.a g;

   public a(Context var1, p var2, h var3, co.uk.getmondo.common.accounts.d var4, k var5, co.uk.getmondo.common.a var6) {
      l.b(var1, "context");
      l.b(var2, "userSettingsStorage");
      l.b(var3, "userSettingsRepository");
      l.b(var4, "accountService");
      l.b(var5, "androidAccountManager");
      l.b(var6, "analyticsService");
      super();
      this.b = var1;
      this.c = var2;
      this.d = var3;
      this.e = var4;
      this.f = var5;
      this.g = var6;
      this.a = (ShortcutManager)this.b.getSystemService(ShortcutManager.class);
   }

   private final ShortcutInfo a(b var1, Context var2) {
      ShortcutInfo var3 = (new Builder(var2, var1.a())).setShortLabel((CharSequence)var2.getString(var1.b())).setIcon(Icon.createWithResource(var2, var1.c())).setIntent(var1.a(var2)).build();
      l.a(var3, "ShortcutInfo.Builder(con…\n                .build()");
      return var3;
   }

   private final void a(ak var1) {
      if(var1 != null && var1.a() == ak.a.HAS_ACCOUNT) {
         ShortcutInfo var3 = (ShortcutInfo)null;
         if(l.a(this.c.a(), co.uk.getmondo.payments.send.data.a.d.a)) {
            var3 = this.a(b.a, this.b);
         } else {
            int var2;
            if(l.a(this.c.a(), co.uk.getmondo.payments.send.data.a.d.b)) {
               var2 = 2131362014;
            } else {
               var2 = 2131362013;
            }

            this.a.disableShortcuts(m.a(b.a.a()), (CharSequence)this.b.getString(var2));
         }

         ShortcutInfo var4 = (ShortcutInfo)null;
         co.uk.getmondo.d.a var5 = var1.c();
         ShortcutInfo var6 = var4;
         if(var5 != null) {
            var6 = var4;
            if(!var5.e()) {
               var6 = this.a(b.b, this.b);
            }
         }

         this.a.setDynamicShortcuts(m.c(new ShortcutInfo[]{this.a(b.d, this.b), this.a(b.c, this.b), var6, var3}));
      } else {
         this.a.removeAllDynamicShortcuts();
         this.a.disableShortcuts(Arrays.asList(new String[]{b.d.a(), b.c.a(), b.b.a(), b.a.a()}), (CharSequence)this.b.getString(2131362015));
      }

   }

   public void a() {
      n.create((io.reactivex.p)(new io.reactivex.p() {
         public final void a(final o var1) {
            l.b(var1, "emitter");
            final OnAccountsUpdateListener var2 = (OnAccountsUpdateListener)(new OnAccountsUpdateListener() {
               public final void onAccountsUpdated(Account[] var1x) {
                  var1.a(co.uk.getmondo.common.b.a.a);
               }
            });
            a.this.f.a(var2);
            var1.a((f)(new f() {
               public final void a() {
                  a.this.f.b(var2);
               }
            }));
         }
      })).mergeWith((r)this.d.b()).subscribe((g)(new g() {
         public final void a(Object var1) {
            a.this.a(a.this.e.b());
         }
      }), (g)null.a);
   }

   public void a(b var1) {
      l.b(var1, "appShortcut");
      this.a(var1.a(), var1.d());
   }

   public void a(String var1, String var2) {
      l.b(var1, "appShortcutId");
      l.b(var2, "analyticEvent");
      this.a.reportShortcutUsed(var1);
      this.g.a(Impression.Companion.m(var2));
   }
}
