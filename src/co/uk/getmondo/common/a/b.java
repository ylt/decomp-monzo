package co.uk.getmondo.common.a;

import android.content.Context;
import android.content.Intent;
import co.uk.getmondo.main.HomeActivity;
import co.uk.getmondo.main.g;
import co.uk.getmondo.splash.SplashActivity;
import co.uk.getmondo.topup.TopUpActivity;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\n\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B+\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0001\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0003¢\u0006\u0002\u0010\bJ\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H&R\u0011\u0010\u0007\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0006\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\nR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\fj\u0002\b\u0013j\u0002\b\u0014j\u0002\b\u0015j\u0002\b\u0016¨\u0006\u0017"},
   d2 = {"Lco/uk/getmondo/common/app_shortcuts/AppShortcut;", "", "id", "", "shortLabel", "", "icon", "analyticEvent", "(Ljava/lang/String;ILjava/lang/String;IILjava/lang/String;)V", "getAnalyticEvent", "()Ljava/lang/String;", "getIcon", "()I", "getId", "getShortLabel", "getIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "SEND_MONEY", "TOP_UP", "SPENDING", "CHAT", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public enum b {
   a,
   b,
   c,
   d;

   private final String f;
   private final int g;
   private final int h;
   private final String i;

   static {
      b.b var0 = new b.b("SEND_MONEY", 0);
      a = var0;
      b.d var3 = new b.d("TOP_UP", 1);
      b = var3;
      b.c var1 = new b.c("SPENDING", 2);
      c = var1;
      b.a var2 = new b.a("CHAT", 3);
      d = var2;
   }

   protected b(String var3, int var4, int var5, String var6) {
      l.b(var3, "id");
      l.b(var6, "analyticEvent");
      super(var1, var2);
      this.f = var3;
      this.g = var4;
      this.h = var5;
      this.i = var6;
   }

   public abstract Intent a(Context var1);

   public final String a() {
      return this.f;
   }

   public final int b() {
      return this.g;
   }

   public final int c() {
      return this.h;
   }

   public final String d() {
      return this.i;
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bÆ\u0001\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016¨\u0006\u0007"},
      d2 = {"Lco/uk/getmondo/common/app_shortcuts/AppShortcut$CHAT;", "Lco/uk/getmondo/common/app_shortcuts/AppShortcut;", "(Ljava/lang/String;I)V", "getIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a extends b {
      a(String var1, int var2) {
         super("id_chat", 2131362091, 2130837587, "customer_support");
      }

      public Intent a(Context var1) {
         l.b(var1, "context");
         Intent var2 = SplashActivity.c(var1);
         l.a(var2, "SplashActivity.buildIntercomIntent(context)");
         return var2;
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bÆ\u0001\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016¨\u0006\u0007"},
      d2 = {"Lco/uk/getmondo/common/app_shortcuts/AppShortcut$SEND_MONEY;", "Lco/uk/getmondo/common/app_shortcuts/AppShortcut;", "(Ljava/lang/String;I)V", "getIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class b extends b {
      b(String var1, int var2) {
         Boolean var4 = co.uk.getmondo.a.c;
         l.a(var4, "BuildConfig.BANK");
         int var3;
         if(var4.booleanValue()) {
            var3 = 2131362029;
         } else {
            var3 = 2131362548;
         }

         super("id_send", var3, 2130837588, "send_money");
      }

      public Intent a(Context var1) {
         l.b(var1, "context");
         return HomeActivity.f.a(var1, g.d, b.a);
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bÆ\u0001\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016¨\u0006\u0007"},
      d2 = {"Lco/uk/getmondo/common/app_shortcuts/AppShortcut$SPENDING;", "Lco/uk/getmondo/common/app_shortcuts/AppShortcut;", "(Ljava/lang/String;I)V", "getIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class c extends b {
      c(String var1, int var2) {
         super("id_spending", 2131362490, 2130837589, "spending");
      }

      public Intent a(Context var1) {
         l.b(var1, "context");
         return HomeActivity.f.a(var1, g.b, b.c);
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bÆ\u0001\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016¨\u0006\u0007"},
      d2 = {"Lco/uk/getmondo/common/app_shortcuts/AppShortcut$TOP_UP;", "Lco/uk/getmondo/common/app_shortcuts/AppShortcut;", "(Ljava/lang/String;I)V", "getIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class d extends b {
      d(String var1, int var2) {
         super("id_top_up", 2131362844, 2130837590, "top_up");
      }

      public Intent a(Context var1) {
         l.b(var1, "context");
         Intent var2 = TopUpActivity.a(var1, true);
         l.a(var2, "TopUpActivity.buildIntent(context, true)");
         return var2;
      }
   }
}
