package co.uk.getmondo.common.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.inputmethod.InputMethodManager;
import co.uk.getmondo.MonzoApplication;
import co.uk.getmondo.force_upgrade.ForceUpgradeActivity;
import co.uk.getmondo.splash.SplashActivity;
import com.crashlytics.android.Crashlytics;

public abstract class b extends android.support.v7.app.e implements co.uk.getmondo.common.e.a.a {
   private co.uk.getmondo.common.h.b.b a;
   private Toolbar b;
   protected com.c.b.b d = com.c.b.b.c();

   // $FF: synthetic method
   static void a(String var0, co.uk.getmondo.common.f.c var1) {
      var1.c(var0);
   }

   // $FF: synthetic method
   static void b(String var0, co.uk.getmondo.common.f.c var1) {
      var1.b(var0);
   }

   protected void a(View var1) {
      if(var1 != null) {
         ((InputMethodManager)this.getSystemService("input_method")).hideSoftInputFromWindow(var1.getWindowToken(), 2);
      }

   }

   public void b(int var1) {
      this.b(this.getString(var1));
   }

   public void b(String var1) {
      this.a(this.n());
      this.d.a(c.a(var1));
   }

   public void c(int var1) {
      this.startActivity(SplashActivity.a(this, this.getString(var1)));
   }

   public void c(String var1) {
      this.d.a(d.a(var1));
   }

   protected co.uk.getmondo.common.h.b.b l() {
      return this.a;
   }

   public View m() {
      return this.n();
   }

   public View n() {
      View var1;
      if(this.getCurrentFocus() == null) {
         var1 = this.getWindow().getDecorView().findViewById(16908290);
      } else {
         var1 = this.getCurrentFocus();
      }

      return var1;
   }

   protected void o() {
      try {
         this.onBackPressed();
      } catch (IllegalStateException var2) {
         Crashlytics.logException(var2);
      }

   }

   protected void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.a = co.uk.getmondo.common.h.b.a.a().a(MonzoApplication.a(this).b()).a(new co.uk.getmondo.common.h.b.c(this)).a();
   }

   protected void onDestroy() {
      this.d = com.c.b.b.c();
      super.onDestroy();
   }

   public boolean onOptionsItemSelected(MenuItem var1) {
      boolean var2;
      switch(var1.getItemId()) {
      case 16908332:
         this.o();
         var2 = true;
         break;
      default:
         var2 = super.onOptionsItemSelected(var1);
      }

      return var2;
   }

   protected void p() {
      View var1 = this.n().findViewById(2131820798);
      if(var1 != null && var1 instanceof Toolbar) {
         this.b = (Toolbar)var1;
         this.setSupportActionBar(this.b);
         android.support.v7.app.a var2 = this.getSupportActionBar();
         if(var2 != null) {
            var2.b(true);
            var2.d(true);
         }
      }

   }

   public void q() {
      this.a(this.n());
      this.startActivity(ForceUpgradeActivity.a((Context)this));
   }

   protected Toolbar r() {
      return this.b;
   }

   public void s() {
      this.d.a(e.a());
   }

   public void setContentView(int var1) {
      super.setContentView(var1);
      this.p();
      this.d = com.c.b.b.b(new co.uk.getmondo.common.f.c(this, this.m()));
   }

   public void setContentView(View var1) {
      super.setContentView(var1);
      this.p();
      this.d = com.c.b.b.b(new co.uk.getmondo.common.f.c(this, this.m()));
   }

   public void setContentView(View var1, LayoutParams var2) {
      super.setContentView(var1, var2);
      this.p();
   }

   public void t() {
      this.d.a(f.a());
   }

   public void u() {
      SplashActivity.a(this);
   }

   public static enum a {
      a,
      b;
   }
}
