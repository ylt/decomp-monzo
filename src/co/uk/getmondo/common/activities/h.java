package co.uk.getmondo.common.activities;

import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup.LayoutParams;

public abstract class h extends b {
   private boolean a = true;

   private void b() {
      Toolbar var1 = this.r();
      if(var1 != null && this.a) {
         var1.setNavigationIcon(android.support.v4.content.a.a(var1.getContext(), 2130837836).mutate());
      }

   }

   protected void a() {
      this.setResult(0);
   }

   public boolean onOptionsItemSelected(MenuItem var1) {
      switch(var1.getItemId()) {
      case 16908332:
         if(this.a) {
            this.a();
         }
      default:
         return super.onOptionsItemSelected(var1);
      }
   }

   public void setContentView(int var1) {
      super.setContentView(var1);
      this.b();
   }

   public void setContentView(View var1) {
      super.setContentView(var1);
      this.b();
   }

   public void setContentView(View var1, LayoutParams var2) {
      super.setContentView(var1, var2);
      this.b();
   }
}
