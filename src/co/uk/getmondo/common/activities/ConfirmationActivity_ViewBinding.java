package co.uk.getmondo.common.activities;

import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class ConfirmationActivity_ViewBinding implements Unbinder {
   private ConfirmationActivity a;

   public ConfirmationActivity_ViewBinding(ConfirmationActivity var1, View var2) {
      this.a = var1;
      var1.titleView = (TextView)Utils.findRequiredViewAsType(var2, 2131820583, "field 'titleView'", TextView.class);
      var1.subTitleView = (TextView)Utils.findRequiredViewAsType(var2, 2131820857, "field 'subTitleView'", TextView.class);
   }

   public void unbind() {
      ConfirmationActivity var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.titleView = null;
         var1.subTitleView = null;
      }
   }
}
