package co.uk.getmondo.common.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ConfirmationActivity extends b {
   @BindView(2131820857)
   TextView subTitleView;
   @BindView(2131820583)
   TextView titleView;

   public static void a(Context var0, Intent var1) {
      a(var0, var0.getString(2131362792), var0.getString(2131362791), var1);
   }

   public static void a(Context var0, String var1, String var2) {
      var0.startActivity(b(var0, var1, var2));
   }

   public static void a(Context var0, String var1, String var2, Intent var3) {
      Intent var4 = b(var0, var1, var2);
      var4.putExtra("EXTRA_NEXT_ACTIVITY_INTENT", var3);
      var0.startActivity(var4);
   }

   // $FF: synthetic method
   static void a(ConfirmationActivity var0) {
      Intent var1 = (Intent)var0.getIntent().getParcelableExtra("EXTRA_NEXT_ACTIVITY_INTENT");
      if(var1 != null) {
         var0.startActivity(var1);
         var0.finishAffinity();
      } else {
         var0.setResult(-1);
         var0.finish();
      }

   }

   public static Intent b(Context var0, String var1, String var2) {
      Intent var3 = new Intent(var0, ConfirmationActivity.class);
      var3.putExtra("EXTRA_TITLE", var1);
      var3.putExtra("EXTRA_SUBTITLE", var2);
      return var3;
   }

   public void onBackPressed() {
   }

   protected void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.setContentView(2131034154);
      ButterKnife.bind((Activity)this);
      this.titleView.setText(this.getIntent().getStringExtra("EXTRA_TITLE"));
      this.subTitleView.setText(this.getIntent().getStringExtra("EXTRA_SUBTITLE"));
      (new Handler()).postDelayed(g.a(this), 2500L);
   }
}
