package co.uk.getmondo.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class u implements s {
   private final SharedPreferences a;

   public u(Context var1) {
      this.a = var1.getSharedPreferences("co.uk.getmondo", 0);
   }

   public String a() {
      return this.f("EMAIL_KEY");
   }

   public void a(String var1) {
      this.a(var1, "EMAIL_KEY");
   }

   protected void a(String var1, String var2) {
      Editor var3 = this.a.edit();
      if(var1 == null) {
         var3.remove(var2);
      } else {
         var3.putString(var2, var1);
      }

      var3.apply();
   }

   public void a(boolean var1) {
      this.a.edit().putBoolean("FIRST_TIME_WAITING_LIST_KEY", var1).apply();
   }

   public String b() {
      return this.f("OAUTH_STATE_KEY");
   }

   public void b(String var1) {
      this.a(var1, "OAUTH_STATE_KEY");
   }

   public void b(boolean var1) {
      this.a.edit().putBoolean("NOTIFY_WHEN_READY_KEY", var1).apply();
   }

   public String c() {
      return this.f("SESSION_KEY");
   }

   public void c(String var1) {
      this.a(var1, "SESSION_KEY");
   }

   public String d() {
      return this.f("FINGER_PRINT_KEY");
   }

   public void d(String var1) {
      this.a(var1, "FINGER_PRINT_KEY");
   }

   public void e(String var1) {
      this.a(var1, "READ_EVENTS_KEY");
   }

   public boolean e() {
      return this.a.getBoolean("FIRST_TIME_WAITING_LIST_KEY", true);
   }

   public String f() {
      String var2 = this.f("READ_EVENTS_KEY");
      String var1 = var2;
      if(var2 == null) {
         var1 = "";
      }

      return var1;
   }

   protected String f(String var1) {
      return this.a.getString(var1, (String)null);
   }

   public boolean g() {
      return this.a.getBoolean("NOTIFY_WHEN_READY_KEY", false);
   }
}
