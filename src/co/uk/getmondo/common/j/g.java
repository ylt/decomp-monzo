package co.uk.getmondo.common.j;

import io.reactivex.n;
import io.reactivex.o;
import io.reactivex.p;
import io.realm.al;
import io.realm.am;
import io.realm.av;
import io.realm.bb;
import io.realm.bg;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001:\u0001\u0015B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007J8\u0010\u0007\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\n0\t0\b\"\b\b\u0000\u0010\n*\u00020\u000b2\u0018\u0010\f\u001a\u0014\u0012\u0004\u0012\u00020\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\n0\u000f0\rH\u0007J\u001a\u0010\u0010\u001a\n\u0012\u0004\u0012\u00020\u0012\u0018\u00010\u00112\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014H\u0002¨\u0006\u0016"},
   d2 = {"Lco/uk/getmondo/common/rx/RxRealm;", "", "()V", "asCompletable", "Lio/reactivex/Completable;", "realmTransaction", "Lio/realm/Realm$Transaction;", "asObservable", "Lio/reactivex/Observable;", "Lco/uk/getmondo/common/rx/RxRealm$Result;", "T", "Lio/realm/RealmModel;", "query", "Lkotlin/Function1;", "Lio/realm/Realm;", "Lio/realm/RealmResults;", "calculateDiff", "", "Lco/uk/getmondo/common/data/QueryResults$Change;", "realmChangeSet", "Lio/realm/OrderedCollectionChangeSet;", "Result", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class g {
   public static final g a;

   static {
      new g();
   }

   private g() {
      a = (g)this;
   }

   public static final io.reactivex.b a(final av.a var0) {
      l.b(var0, "realmTransaction");
      io.reactivex.b var1 = io.reactivex.b.a((io.reactivex.c.a)(new io.reactivex.c.a() {
         public final void a() {
            // $FF: Couldn't be decompiled
         }
      }));
      l.a(var1, "Completable.fromAction {…)\n            }\n        }");
      return var1;
   }

   public static final n a(final kotlin.d.a.b var0) {
      l.b(var0, "query");
      n var1 = n.create((p)(new p() {
         public final void a(final o var1) {
            l.b(var1, "emitter");
            final av var2 = av.n();
            final am var3 = (am)(new am() {
               public final void a(bg var1x, al var2x) {
                  if(var1x.c() && var1x.a()) {
                     o var4 = var1;
                     av var3 = var2;
                     l.a(var3, "realm");
                     l.a(var1x, "realmResults");
                     var4.a(new g.a(var3, new co.uk.getmondo.common.b.b((List)var1x, g.a.a(var2x))));
                  }

               }
            });
            kotlin.d.a.b var4 = var0;
            l.a(var2, "realm");
            final bg var5 = (bg)var4.a(var2);
            var5.a(var3);
            var1.a((io.reactivex.c.f)(new io.reactivex.c.f() {
               public final void a() {
                  var5.b(var3);
                  var2.close();
               }
            }));
            if(var5.c() && var5.a()) {
               var1.a(new g.a(var2, new co.uk.getmondo.common.b.b((List)var5, (List)null, 2, (i)null)));
            }

         }
      }));
      l.a(var1, "Observable.create<Result…)\n            }\n        }");
      return var1;
   }

   private final List a(al var1) {
      byte var3 = 0;
      List var7;
      if(var1 == null) {
         var7 = null;
      } else {
         List var4 = (List)(new ArrayList());
         al.a[] var5 = var1.a();
         if(var5 != null) {
            List var9 = kotlin.a.g.f((Object[])var5);
            if(var9 != null) {
               Iterator var6 = ((Iterable)var9).iterator();

               while(var6.hasNext()) {
                  al.a var10 = (al.a)var6.next();
                  var4.add(new co.uk.getmondo.common.b.b.a(co.uk.getmondo.common.b.b.a.b, var10.a, var10.b));
               }
            }
         }

         var5 = var1.b();
         int var2;
         Object[] var11;
         if(var5 != null) {
            var11 = (Object[])var5;

            for(var2 = 0; var2 < var11.length; ++var2) {
               al.a var12 = (al.a)var11[var2];
               var4.add(new co.uk.getmondo.common.b.b.a(co.uk.getmondo.common.b.b.a.a, var12.a, var12.b));
            }
         }

         var5 = var1.c();
         var7 = var4;
         if(var5 != null) {
            var11 = (Object[])var5;
            var2 = var3;

            while(true) {
               var7 = var4;
               if(var2 >= var11.length) {
                  break;
               }

               al.a var8 = (al.a)var11[var2];
               var4.add(new co.uk.getmondo.common.b.b.a(co.uk.getmondo.common.b.b.a.c, var8.a, var8.b));
               ++var2;
            }
         }
      }

      return var7;
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\n\n\u0002\u0010 \n\u0002\b\u0003\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u0000*\n\b\u0000\u0010\u0001 \u0001*\u00020\u00022\u00020\u0003B\u001b\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00028\u00000\u0007¢\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0005HÂ\u0003J\u000f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00028\u00000\u0007HÆ\u0003J)\u0010\u0011\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\b\b\u0002\u0010\u0004\u001a\u00020\u00052\u000e\b\u0002\u0010\u0006\u001a\b\u0012\u0004\u0012\u00028\u00000\u0007HÆ\u0001J\r\u0010\u0012\u001a\u0004\u0018\u00018\u0000¢\u0006\u0002\u0010\u0013J\f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00028\u00000\u0015J\u0013\u0010\u0016\u001a\u00020\n2\b\u0010\u0017\u001a\u0004\u0018\u00010\u0003HÖ\u0003J\t\u0010\u0018\u001a\u00020\u0019HÖ\u0001J\t\u0010\u001a\u001a\u00020\u001bHÖ\u0001R\u0011\u0010\t\u001a\u00020\n8F¢\u0006\u0006\u001a\u0004\b\t\u0010\u000bR\u0011\u0010\f\u001a\u00020\n8F¢\u0006\u0006\u001a\u0004\b\f\u0010\u000bR\u0017\u0010\u0006\u001a\b\u0012\u0004\u0012\u00028\u00000\u0007¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u001c"},
      d2 = {"Lco/uk/getmondo/common/rx/RxRealm$Result;", "T", "Lio/realm/RealmModel;", "", "realm", "Lio/realm/Realm;", "queryResults", "Lco/uk/getmondo/common/data/QueryResults;", "(Lio/realm/Realm;Lco/uk/getmondo/common/data/QueryResults;)V", "isEmpty", "", "()Z", "isNotEmpty", "getQueryResults", "()Lco/uk/getmondo/common/data/QueryResults;", "component1", "component2", "copy", "copyFirstFromRealm", "()Lio/realm/RealmModel;", "copyFromRealm", "", "equals", "other", "hashCode", "", "toString", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private final av a;
      private final co.uk.getmondo.common.b.b b;

      public a(av var1, co.uk.getmondo.common.b.b var2) {
         l.b(var1, "realm");
         l.b(var2, "queryResults");
         super();
         this.a = var1;
         this.b = var2;
      }

      public final boolean a() {
         boolean var1;
         if(!((Collection)this.b.a()).isEmpty()) {
            var1 = true;
         } else {
            var1 = false;
         }

         return var1;
      }

      public final boolean b() {
         return this.b.a().isEmpty();
      }

      public final List c() {
         List var1 = this.a.a((Iterable)this.b.a());
         l.a(var1, "realm.copyFromRealm(queryResults.results)");
         return var1;
      }

      public final bb d() {
         bb var1;
         if(this.b.a().isEmpty()) {
            var1 = null;
         } else {
            var1 = this.a.e((bb)this.b.a().get(0));
         }

         return var1;
      }

      public final co.uk.getmondo.common.b.b e() {
         return this.b;
      }

      public boolean equals(Object var1) {
         boolean var2;
         if(this != var1) {
            label28: {
               if(var1 instanceof g.a) {
                  g.a var3 = (g.a)var1;
                  if(l.a(this.a, var3.a) && l.a(this.b, var3.b)) {
                     break label28;
                  }
               }

               var2 = false;
               return var2;
            }
         }

         var2 = true;
         return var2;
      }

      public int hashCode() {
         int var2 = 0;
         av var3 = this.a;
         int var1;
         if(var3 != null) {
            var1 = var3.hashCode();
         } else {
            var1 = 0;
         }

         co.uk.getmondo.common.b.b var4 = this.b;
         if(var4 != null) {
            var2 = var4.hashCode();
         }

         return var1 * 31 + var2;
      }

      public String toString() {
         return "Result(realm=" + this.a + ", queryResults=" + this.b + ")";
      }
   }
}
