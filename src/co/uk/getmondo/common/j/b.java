package co.uk.getmondo.common.j;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import io.reactivex.n;
import io.reactivex.o;

public class b {
   public static n a(Context var0, IntentFilter var1) {
      return n.create(c.a(var0, var1));
   }

   // $FF: synthetic method
   static void a(Context var0, BroadcastReceiver var1) throws Exception {
      var0.unregisterReceiver(var1);
   }

   // $FF: synthetic method
   static void a(Context var0, IntentFilter var1, final o var2) throws Exception {
      BroadcastReceiver var3 = new BroadcastReceiver() {
         public void onReceive(Context var1, Intent var2x) {
            var2.a(var2x);
         }
      };
      var0.registerReceiver(var3, var1);
      var2.a(d.a(var0, var3));
   }
}
