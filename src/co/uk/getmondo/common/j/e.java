package co.uk.getmondo.common.j;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import io.reactivex.h;
import io.reactivex.i;
import io.reactivex.k;
import kotlin.Metadata;
import kotlin.n;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002JC\u0010\u0003\u001a\b\u0012\u0004\u0012\u0002H\u00050\u0004\"\u0004\b\u0000\u0010\u00052-\u0010\u0006\u001a)\u0012\u001f\u0012\u001d\u0012\u0004\u0012\u0002H\u0005\u0012\u0004\u0012\u00020\b0\u0007¢\u0006\f\b\t\u0012\b\b\n\u0012\u0004\b\b(\u000b\u0012\u0004\u0012\u00020\f0\u0007H\u0007¨\u0006\r"},
   d2 = {"Lco/uk/getmondo/common/rx/RxDialog;", "", "()V", "create", "Lio/reactivex/Maybe;", "T", "createDialog", "Lkotlin/Function1;", "", "Lkotlin/ParameterName;", "name", "setResult", "Landroid/app/Dialog;", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class e {
   public static final e a;

   static {
      new e();
   }

   private e() {
      a = (e)this;
   }

   public static final h a(final kotlin.d.a.b var0) {
      l.b(var0, "createDialog");
      h var1 = h.a((k)(new k() {
         public final void a(final i var1) {
            l.b(var1, "emitter");
            final Dialog var2 = (Dialog)var0.a(new kotlin.d.a.b() {
               // $FF: synthetic method
               public Object a(Object var1x) {
                  this.b(var1x);
                  return n.a;
               }

               public final void b(Object var1x) {
                  var1.a(var1x);
               }
            });
            var2.setOnDismissListener((OnDismissListener)(new OnDismissListener() {
               public final void onDismiss(DialogInterface var1x) {
                  if(!var1.isDisposed()) {
                     var1.a();
                  }

               }
            }));
            var1.a((io.reactivex.c.f)(new io.reactivex.c.f() {
               public final void a() {
                  var2.setOnDismissListener((OnDismissListener)null);
                  var2.dismiss();
               }
            }));
            var2.show();
         }
      }));
      l.a(var1, "Maybe.create { emitter -…  dialog.show()\n        }");
      return var1;
   }
}
