package co.uk.getmondo.common.j;

import android.content.Context;
import android.content.IntentFilter;
import io.reactivex.o;
import io.reactivex.p;

// $FF: synthetic class
final class c implements p {
   private final Context a;
   private final IntentFilter b;

   private c(Context var1, IntentFilter var2) {
      this.a = var1;
      this.b = var2;
   }

   public static p a(Context var0, IntentFilter var1) {
      return new c(var0, var1);
   }

   public void a(o var1) {
      b.a(this.a, this.b, var1);
   }
}
