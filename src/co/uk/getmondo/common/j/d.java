package co.uk.getmondo.common.j;

import android.content.BroadcastReceiver;
import android.content.Context;

// $FF: synthetic class
final class d implements io.reactivex.c.f {
   private final Context a;
   private final BroadcastReceiver b;

   private d(Context var1, BroadcastReceiver var2) {
      this.a = var1;
      this.b = var2;
   }

   public static io.reactivex.c.f a(Context var0, BroadcastReceiver var1) {
      return new d(var0, var1);
   }

   public void a() {
      b.a(this.a, this.b);
   }
}
