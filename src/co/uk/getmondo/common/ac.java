package co.uk.getmondo.common;

import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\u001a(\u0010\u0000\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u00020\u0001*\u00020\u00032\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0004¨\u0006\u0006"},
   d2 = {"changes", "Lio/reactivex/Observable;", "Lkotlin/Pair;", "Landroid/content/SharedPreferences;", "", "filterKey", "app_monzoPrepaidRelease"},
   k = 2,
   mv = {1, 1, 7}
)
public final class ac {
   public static final io.reactivex.n a(final SharedPreferences var0, final String var1) {
      kotlin.d.b.l.b(var0, "$receiver");
      io.reactivex.n var2 = io.reactivex.n.create((io.reactivex.p)(new io.reactivex.p() {
         public final void a(final io.reactivex.o var1x) {
            kotlin.d.b.l.b(var1x, "emitter");
            final OnSharedPreferenceChangeListener var2 = (OnSharedPreferenceChangeListener)(new OnSharedPreferenceChangeListener() {
               public final void onSharedPreferenceChanged(SharedPreferences var1xx, String var2) {
                  if(var1 == null || kotlin.d.b.l.a(var1, var2)) {
                     var1x.a(new kotlin.h(var1xx, var2));
                  }

               }
            });
            var0.registerOnSharedPreferenceChangeListener(var2);
            var1x.a((io.reactivex.c.f)(new io.reactivex.c.f() {
               public final void a() {
                  var0.unregisterOnSharedPreferenceChangeListener(var2);
               }
            }));
         }
      }));
      kotlin.d.b.l.a(var2, "Observable.create { emit…istener(listener) }\n    }");
      return var2;
   }
}
