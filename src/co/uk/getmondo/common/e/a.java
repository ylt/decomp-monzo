package co.uk.getmondo.common.e;

import co.uk.getmondo.api.ApiException;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.d.ac;
import co.uk.getmondo.d.ak;
import java.io.IOException;
import javax.net.ssl.SSLPeerUnverifiedException;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.n;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0003\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\u0018\u0000 \u001c2\u00020\u0001:\u0002\u001b\u001cB\u0017\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J,\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\u0012\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\b0\u000eH\u0002J\u0016\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u000b\u001a\u00020\fJ*\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u000b\u001a\u00020\f2\u0012\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\b0\u000eJ\u0012\u0010\u0014\u001a\u00020\u00112\b\u0010\u0015\u001a\u0004\u0018\u00010\u0013H\u0002J \u0010\u0016\u001a\u00020\b2\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u000b\u001a\u00020\fH\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u001d"},
   d2 = {"Lco/uk/getmondo/common/errors/ApiErrorHandler;", "", "accountService", "Lco/uk/getmondo/common/accounts/AccountService;", "analyticsService", "Lco/uk/getmondo/common/AnalyticsService;", "(Lco/uk/getmondo/common/accounts/AccountService;Lco/uk/getmondo/common/AnalyticsService;)V", "handleApiException", "", "apiException", "Lco/uk/getmondo/api/ApiException;", "view", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "showError", "Lkotlin/Function1;", "", "handleError", "", "throwable", "", "isNetworkException", "t", "performLogOut", "errorCause", "Lco/uk/getmondo/api/authentication/AuthError;", "logoutReason", "", "ApiErrorView", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class a {
   public static final a.b a = new a.b((i)null);
   private final co.uk.getmondo.common.accounts.d b;
   private final co.uk.getmondo.common.a c;

   public a(co.uk.getmondo.common.accounts.d var1, co.uk.getmondo.common.a var2) {
      l.b(var1, "accountService");
      l.b(var2, "analyticsService");
      super();
      this.b = var1;
      this.c = var2;
   }

   private final void a(ApiException var1, a.a var2, kotlin.d.a.b var3) {
      d.a.a.a((Throwable)(new Exception("Handling api error", (Throwable)var1)), "Api Error with view %s", new Object[]{var2.getClass().getSimpleName()});
      co.uk.getmondo.common.a var5 = this.c;
      Impression.Companion var6 = Impression.Companion;
      int var4 = var1.b();
      String var7 = var1.c();
      l.a(var7, "apiException.path");
      var5.a(var6.b(var4, var7, var1.d(), var1.e()));
      var4 = var1.b();
      co.uk.getmondo.api.model.b var10 = var1.e();
      if(400 <= var4 && 499 >= var4 && var10 != null) {
         co.uk.getmondo.api.authentication.a var8 = (co.uk.getmondo.api.authentication.a)d.a((f[])co.uk.getmondo.api.authentication.a.values(), var10.a());
         if(var8 != null && var8.b()) {
            String var9 = var10.b();
            l.a(var9, "apiError.message");
            this.a(var8, var9, var2);
            return;
         }

         if(var10.c()) {
            var2.q();
            return;
         }
      }

      if(var4 >= 500) {
         var3.a(Integer.valueOf(2131362199));
      } else {
         var3.a(Integer.valueOf(2131362198));
      }

   }

   private final void a(co.uk.getmondo.api.authentication.a var1, String var2, a.a var3) {
      if(this.b.a()) {
         String var6;
         label21: {
            ak var4 = this.b.b();
            if(var4 != null) {
               ac var5 = var4.d();
               if(var5 != null) {
                  var6 = var5.b();
                  break label21;
               }
            }

            var6 = null;
         }

         if(var6 == null) {
            l.a();
         }

         this.c.a(Impression.Companion.a(var2, var6));
         this.b.e();
         if(l.a(var1, co.uk.getmondo.api.authentication.a.a)) {
            var3.c(2131362864);
         } else {
            var3.u();
         }
      }

   }

   public static final boolean a(Throwable var0) {
      return a.a(var0);
   }

   private final boolean b(Throwable var1) {
      boolean var2;
      if(var1 != null && var1 instanceof IOException) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   public final boolean a(Throwable var1, final a.a var2) {
      l.b(var1, "throwable");
      l.b(var2, "view");
      return this.a(var1, var2, (kotlin.d.a.b)(new kotlin.d.a.b() {
         // $FF: synthetic method
         public Object a(Object var1) {
            this.a(((Number)var1).intValue());
            return n.a;
         }

         public final void a(int var1) {
            var2.b(var1);
         }
      }));
   }

   public final boolean a(Throwable var1, a.a var2, kotlin.d.a.b var3) {
      l.b(var1, "throwable");
      l.b(var2, "view");
      l.b(var3, "showError");
      boolean var4;
      if(!a.a(var1) && !a.a(var1.getCause())) {
         if(var1 instanceof ApiException) {
            this.a((ApiException)var1, var2, var3);
            var4 = true;
         } else if(var1.getCause() != null && var1.getCause() instanceof ApiException) {
            var1 = var1.getCause();
            if(var1 == null) {
               throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.api.ApiException");
            }

            this.a((ApiException)var1, var2, var3);
            var4 = true;
         } else if(!this.b(var1) && !this.b(var1.getCause())) {
            d.a.a.a((Throwable)(new Exception("Failed to handle error", var1)), "Failed to handle error with view %s", new Object[]{var2.getClass().getSimpleName()});
            var4 = false;
         } else {
            var3.a(Integer.valueOf(2131362247));
            d.a.a.a((Throwable)(new Exception("Network error", var1)), "Network failure with view %s", new Object[]{var2.getClass().getSimpleName()});
            var4 = true;
         }
      } else {
         var3.a(Integer.valueOf(2131362179));
         d.a.a.a((Throwable)(new Exception("Wrong certificate", var1)), "Wrong certificate with view %s", new Object[]{var2.getClass().getSimpleName()});
         var4 = true;
      }

      return var4;
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0000\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&J\b\u0010\u0004\u001a\u00020\u0003H&J\u0012\u0010\u0005\u001a\u00020\u00032\b\b\u0001\u0010\u0006\u001a\u00020\u0007H&¨\u0006\b"},
      d2 = {"Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "Lco/uk/getmondo/common/errors/ErrorView;", "openForceUpgrade", "", "openSplash", "openSplashWithMessage", "messageRes", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends e {
      void c(int var1);

      void q();

      void u();
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0003\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0007¨\u0006\u0007"},
      d2 = {"Lco/uk/getmondo/common/errors/ApiErrorHandler$Companion;", "", "()V", "isCertificatePinningException", "", "t", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class b {
      private b() {
      }

      // $FF: synthetic method
      public b(i var1) {
         this();
      }

      public final boolean a(Throwable var1) {
         boolean var2;
         if(var1 != null && var1 instanceof SSLPeerUnverifiedException) {
            var2 = true;
         } else {
            var2 = false;
         }

         return var2;
      }
   }
}
