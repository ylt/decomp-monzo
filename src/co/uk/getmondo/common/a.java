package co.uk.getmondo.common;

import co.uk.getmondo.api.AnalyticsApi;
import co.uk.getmondo.api.model.tracking.Impression;

public class a {
   private final AnalyticsApi a;
   private final io.reactivex.u b;

   public a(AnalyticsApi var1, io.reactivex.u var2) {
      this.a = var1;
      this.b = var2;
   }

   // $FF: synthetic method
   static void b(Impression var0) throws Exception {
      d.a.a.b("Impression tracked: %s", new Object[]{var0.toString()});
   }

   public void a(Impression var1) {
      this.a.trackEvent(var1).b(this.b).a(b.a(var1), c.a());
   }
}
