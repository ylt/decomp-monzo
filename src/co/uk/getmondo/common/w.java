package co.uk.getmondo.common;

import android.content.res.Resources;

public final class w implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;

   static {
      boolean var0;
      if(!w.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   public w(javax.a.a var1) {
      if(!a && var1 == null) {
         throw new AssertionError();
      } else {
         this.b = var1;
      }
   }

   public static b.a.b a(javax.a.a var0) {
      return new w(var0);
   }

   public v a() {
      return new v((Resources)this.b.b());
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
