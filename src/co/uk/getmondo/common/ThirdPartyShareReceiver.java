package co.uk.getmondo.common;

import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import co.uk.getmondo.MonzoApplication;
import co.uk.getmondo.api.model.tracking.Impression;

public class ThirdPartyShareReceiver extends BroadcastReceiver {
   a a;

   @TargetApi(22)
   public void onReceive(Context var1, Intent var2) {
      MonzoApplication.a(var1).b().a(this);
      ComponentName var3 = (ComponentName)var2.getParcelableExtra("android.intent.extra.CHOSEN_COMPONENT");
      String var4 = var2.getStringExtra("KEY_ANALYTIC_NAME");
      String var5 = var3.getClassName();
      this.a.a(Impression.b(var4, var5));
   }
}
