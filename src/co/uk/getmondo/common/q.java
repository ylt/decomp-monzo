package co.uk.getmondo.common;

import android.app.Application;
import co.uk.getmondo.api.MonzoApi;
import io.intercom.android.sdk.Intercom;
import io.intercom.android.sdk.UserAttributes;
import io.intercom.android.sdk.identity.Registration;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0007\u0018\u00002\u00020\u0001B!\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0001\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u0006\u0010\t\u001a\u00020\nJ\u000e\u0010\u000b\u001a\u00020\n2\u0006\u0010\f\u001a\u00020\rJ\u0006\u0010\u000e\u001a\u00020\nJ\u000e\u0010\u000f\u001a\u00020\n2\u0006\u0010\u0010\u001a\u00020\rJ\u0018\u0010\u0011\u001a\u00020\n2\u0006\u0010\u0012\u001a\u00020\u00132\b\u0010\u0014\u001a\u0004\u0018\u00010\u0015R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0016"},
   d2 = {"Lco/uk/getmondo/common/IntercomService;", "", "application", "Landroid/app/Application;", "monzoApi", "Lco/uk/getmondo/api/MonzoApi;", "ioScheduler", "Lio/reactivex/Scheduler;", "(Landroid/app/Application;Lco/uk/getmondo/api/MonzoApi;Lio/reactivex/Scheduler;)V", "clearRegisteredUser", "", "composeMessage", "message", "", "openIntercom", "registerUser", "userId", "updateUser", "profile", "Lco/uk/getmondo/model/Profile;", "account", "Lco/uk/getmondo/model/Account;", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class q {
   private final MonzoApi a;
   private final io.reactivex.u b;

   public q(Application var1, MonzoApi var2, io.reactivex.u var3) {
      kotlin.d.b.l.b(var1, "application");
      kotlin.d.b.l.b(var2, "monzoApi");
      kotlin.d.b.l.b(var3, "ioScheduler");
      super();
      this.a = var2;
      this.b = var3;
      Intercom.initialize(var1, "android_sdk-33cefb6b165cfd2bc9843a6e33d8bb1201552534", "n3exvq9z");
   }

   public final void a() {
      Intercom.client().displayConversationsList();
   }

   public final void a(co.uk.getmondo.d.ac var1, co.uk.getmondo.d.a var2) {
      kotlin.d.b.l.b(var1, "profile");
      Boolean var3 = co.uk.getmondo.a.b;
      kotlin.d.b.l.a(var3, "BuildConfig.GHOST");
      if(!var3.booleanValue()) {
         UserAttributes.Builder var6 = (new UserAttributes.Builder()).withName(var1.c()).withEmail(var1.d());
         if(var2 != null) {
            var6.withCustomAttribute("account_id", var2.a());
         }

         String var4;
         String var5;
         String var7;
         label16: {
            Intercom.client().updateUser(var6.build());
            var7 = var1.c();
            var4 = var1.d();
            if(var2 != null) {
               var5 = var2.a();
               if(var5 != null) {
                  break label16;
               }
            }

            var5 = "";
         }

         d.a.a.b("Intercom user updated | name: %s email: %s accountID: %s", new Object[]{var7, var4, var5});
      }

   }

   public final void a(String var1) {
      kotlin.d.b.l.b(var1, "userId");
      Boolean var2 = co.uk.getmondo.a.b;
      kotlin.d.b.l.a(var2, "BuildConfig.GHOST");
      if(!var2.booleanValue()) {
         Registration var3 = Registration.create().withUserId(var1);
         Intercom.client().registerIdentifiedUser(var3);
         d.a.a.b("User registered with Intercom | userId: %s", new Object[]{var3.getUserId()});
         this.a.intercomUserHash("n3exvq9z").a(3L).d((io.reactivex.c.h)null.a).b(this.b).a((io.reactivex.c.g)null.a, (io.reactivex.c.g)null.a);
      }

   }

   public final void b() {
      Intercom.client().reset();
   }

   public final void b(String var1) {
      kotlin.d.b.l.b(var1, "message");
      Intercom.client().displayMessageComposer(var1);
   }
}
