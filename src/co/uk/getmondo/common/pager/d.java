package co.uk.getmondo.common.pager;

import co.uk.getmondo.api.model.tracking.Impression;

public class d extends a implements f.c {
   private final int a;
   private final int b;
   private final int c;
   private final Integer d;

   public d(int var1, int var2, int var3, Impression var4) {
      this(var1, var2, var3, (Integer)null, var4);
   }

   public d(int var1, int var2, int var3, Integer var4, Impression var5) {
      super(var5);
      this.a = var1;
      this.b = var2;
      this.c = var3;
      this.d = var4;
   }

   public int b() {
      return this.a;
   }

   public int c() {
      return this.b;
   }

   public int d() {
      return this.c;
   }

   public Integer e() {
      return this.d;
   }
}
