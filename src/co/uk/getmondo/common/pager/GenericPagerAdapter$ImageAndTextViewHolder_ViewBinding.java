package co.uk.getmondo.common.pager;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class GenericPagerAdapter$ImageAndTextViewHolder_ViewBinding implements Unbinder {
   private GenericPagerAdapter.ImageAndTextViewHolder a;

   public GenericPagerAdapter$ImageAndTextViewHolder_ViewBinding(GenericPagerAdapter.ImageAndTextViewHolder var1, View var2) {
      this.a = var1;
      var1.imageView = (ImageView)Utils.findRequiredViewAsType(var2, 2131820750, "field 'imageView'", ImageView.class);
      var1.titleTextView = (TextView)Utils.findRequiredViewAsType(var2, 2131821026, "field 'titleTextView'", TextView.class);
      var1.contentTextView = (TextView)Utils.findRequiredViewAsType(var2, 2131821119, "field 'contentTextView'", TextView.class);
      var1.footerTextView = (TextView)Utils.findRequiredViewAsType(var2, 2131821665, "field 'footerTextView'", TextView.class);
      var1.spacerView = Utils.findRequiredView(var2, 2131820755, "field 'spacerView'");
   }

   public void unbind() {
      GenericPagerAdapter.ImageAndTextViewHolder var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.imageView = null;
         var1.titleTextView = null;
         var1.contentTextView = null;
         var1.footerTextView = null;
         var1.spacerView = null;
      }
   }
}
