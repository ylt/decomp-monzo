package co.uk.getmondo.common.pager;

import android.view.View;
import co.uk.getmondo.api.model.tracking.Impression;

public interface f {
   Impression a();

   public interface a extends f {
      int b();
   }

   public interface b extends f {
      void b();

      View c();
   }

   public interface c extends f {
      int b();

      int c();

      int d();

      Integer e();
   }

   public interface d extends f {
      String b();

      int c();

      int d();
   }

   public interface e extends f {
      int b();

      int c();
   }
}
