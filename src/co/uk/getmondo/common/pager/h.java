package co.uk.getmondo.common.pager;

import android.support.v4.view.ViewPager;
import android.support.v4.view.p;
import android.support.v4.view.ViewPager.j;

public class h {
   private final co.uk.getmondo.common.a a;
   private ViewPager b;
   private final android.support.v4.view.ViewPager.f c = new j() {
      public void b(int var1) {
         h.this.a(var1);
      }
   };

   public h(co.uk.getmondo.common.a var1) {
      this.a = var1;
   }

   private void a(int var1) {
      if(var1 >= 0) {
         p var2 = this.b.getAdapter();
         if(var2 == null) {
            throw new RuntimeException("The ViewPager must have an adapter");
         }

         if(!(var2 instanceof GenericPagerAdapter)) {
            throw new RuntimeException("PageViewTracker requires an adapter of type " + GenericPagerAdapter.class.getSimpleName());
         }

         GenericPagerAdapter var3 = (GenericPagerAdapter)var2;
         if(var1 < var3.b()) {
            f var4 = (f)var3.a.get(var1);
            if(var4.a() != null) {
               this.a.a(var4.a());
            }
         }
      }

   }

   public void a() {
      if(this.b != null) {
         this.b.b(this.c);
         this.b = null;
      }

   }

   public void a(ViewPager var1) {
      this.a();
      this.b = var1;
      this.a(var1.getCurrentItem());
      var1.a(this.c);
   }
}
