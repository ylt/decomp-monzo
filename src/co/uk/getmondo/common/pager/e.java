package co.uk.getmondo.common.pager;

import co.uk.getmondo.api.model.tracking.Impression;

public class e extends a implements f.d {
   private final String a;
   private final int b;
   private final int c;

   public e(String var1, int var2, int var3, Impression var4) {
      super(var4);
      this.a = var1;
      this.b = var2;
      this.c = var3;
   }

   public String b() {
      return this.a;
   }

   public int c() {
      return this.b;
   }

   public int d() {
      return this.c;
   }
}
