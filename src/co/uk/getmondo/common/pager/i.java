package co.uk.getmondo.common.pager;

public final class i implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;

   static {
      boolean var0;
      if(!i.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   public i(javax.a.a var1) {
      if(!a && var1 == null) {
         throw new AssertionError();
      } else {
         this.b = var1;
      }
   }

   public static b.a.b a(javax.a.a var0) {
      return new i(var0);
   }

   public h a() {
      return new h((co.uk.getmondo.common.a)this.b.b());
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
