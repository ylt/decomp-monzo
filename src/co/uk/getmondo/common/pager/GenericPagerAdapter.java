package co.uk.getmondo.common.pager;

import android.support.v4.view.p;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.airbnb.lottie.LottieAnimationView;
import java.util.Arrays;
import java.util.List;

public class GenericPagerAdapter extends p {
   final List a;

   public GenericPagerAdapter(List var1) {
      this.a = var1;
   }

   public GenericPagerAdapter(f... var1) {
      this.a = Arrays.asList(var1);
   }

   public Object a(ViewGroup var1, int var2) {
      f var3 = (f)this.a.get(var2);
      LayoutInflater var4 = LayoutInflater.from(var1.getContext());
      View var5;
      if(var3 instanceof f.a) {
         var5 = var4.inflate(((f.a)var3).b(), var1, false);
      } else if(var3 instanceof f.c) {
         var5 = (new GenericPagerAdapter.ImageAndTextViewHolder(var4.inflate(2131034406, var1, false))).a((f.c)var3);
      } else if(var3 instanceof f.e) {
         var5 = (new GenericPagerAdapter.TitleAndImageViewHolder(var4.inflate(2131034410, var1, false))).a((f.e)var3);
      } else if(var3 instanceof f.b) {
         var5 = ((f.b)var3).c();
      } else {
         if(!(var3 instanceof f.d)) {
            throw new RuntimeException("Unsupported page type: " + var3);
         }

         var5 = (new GenericPagerAdapter.LottieAnimationAndTextViewHolder(var4.inflate(2131034407, var1, false))).a((f.d)var3);
      }

      var1.addView(var5);
      return var5;
   }

   public void a(ViewGroup var1, int var2, Object var3) {
      f var4 = (f)this.a.get(var2);
      if(var4 instanceof f.b) {
         ((f.b)var4).b();
      }

      var1.removeView((View)var3);
   }

   public boolean a(View var1, Object var2) {
      boolean var3;
      if(var1 == var2) {
         var3 = true;
      } else {
         var3 = false;
      }

      return var3;
   }

   public int b() {
      return this.a.size();
   }

   public void b(ViewGroup var1, int var2, Object var3) {
      super.b(var1, var2, var3);
      LottieAnimationView var4 = (LottieAnimationView)((View)var3).findViewById(2131821666);
      if(var4 != null && !var4.b()) {
         var4.c();
      }

   }

   static class ImageAndTextViewHolder extends g {
      @BindView(2131821119)
      TextView contentTextView;
      @BindView(2131821665)
      TextView footerTextView;
      @BindView(2131820750)
      ImageView imageView;
      @BindView(2131820755)
      View spacerView;
      @BindView(2131821026)
      TextView titleTextView;

      ImageAndTextViewHolder(View var1) {
         super(var1);
         ButterKnife.bind(this, (View)var1);
      }

      View a(f.c var1) {
         this.imageView.setImageResource(var1.b());
         this.titleTextView.setText(var1.c());
         this.contentTextView.setText(var1.d());
         if(var1.e() == null) {
            this.footerTextView.setVisibility(8);
            this.spacerView.setVisibility(8);
         } else {
            this.footerTextView.setText(var1.e().intValue());
            this.footerTextView.setVisibility(0);
            this.spacerView.setVisibility(0);
         }

         return this.a;
      }
   }

   static class LottieAnimationAndTextViewHolder extends g {
      @BindView(2131821119)
      TextView contentTextView;
      @BindView(2131821666)
      LottieAnimationView lottieAnimationView;
      @BindView(2131821026)
      TextView titleTextView;

      LottieAnimationAndTextViewHolder(View var1) {
         super(var1);
         ButterKnife.bind(this, (View)var1);
      }

      View a(f.d var1) {
         this.lottieAnimationView.setAnimation(var1.b());
         this.titleTextView.setText(var1.c());
         this.contentTextView.setText(var1.d());
         return this.a;
      }
   }

   static class TitleAndImageViewHolder extends g {
      @BindView(2131820750)
      ImageView imageView;
      @BindView(2131821026)
      TextView titleTextView;

      TitleAndImageViewHolder(View var1) {
         super(var1);
         ButterKnife.bind(this, (View)var1);
      }

      View a(f.e var1) {
         this.imageView.setImageResource(var1.c());
         this.titleTextView.setText(var1.b());
         return this.a;
      }
   }
}
