package co.uk.getmondo.common.pager;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class GenericPagerAdapter$TitleAndImageViewHolder_ViewBinding implements Unbinder {
   private GenericPagerAdapter.TitleAndImageViewHolder a;

   public GenericPagerAdapter$TitleAndImageViewHolder_ViewBinding(GenericPagerAdapter.TitleAndImageViewHolder var1, View var2) {
      this.a = var1;
      var1.titleTextView = (TextView)Utils.findRequiredViewAsType(var2, 2131821026, "field 'titleTextView'", TextView.class);
      var1.imageView = (ImageView)Utils.findRequiredViewAsType(var2, 2131820750, "field 'imageView'", ImageView.class);
   }

   public void unbind() {
      GenericPagerAdapter.TitleAndImageViewHolder var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.titleTextView = null;
         var1.imageView = null;
      }
   }
}
