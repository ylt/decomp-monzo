package co.uk.getmondo.common.pager;

import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.airbnb.lottie.LottieAnimationView;

public class GenericPagerAdapter$LottieAnimationAndTextViewHolder_ViewBinding implements Unbinder {
   private GenericPagerAdapter.LottieAnimationAndTextViewHolder a;

   public GenericPagerAdapter$LottieAnimationAndTextViewHolder_ViewBinding(GenericPagerAdapter.LottieAnimationAndTextViewHolder var1, View var2) {
      this.a = var1;
      var1.titleTextView = (TextView)Utils.findRequiredViewAsType(var2, 2131821026, "field 'titleTextView'", TextView.class);
      var1.contentTextView = (TextView)Utils.findRequiredViewAsType(var2, 2131821119, "field 'contentTextView'", TextView.class);
      var1.lottieAnimationView = (LottieAnimationView)Utils.findRequiredViewAsType(var2, 2131821666, "field 'lottieAnimationView'", LottieAnimationView.class);
   }

   public void unbind() {
      GenericPagerAdapter.LottieAnimationAndTextViewHolder var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.titleTextView = null;
         var1.contentTextView = null;
         var1.lottieAnimationView = null;
      }
   }
}
