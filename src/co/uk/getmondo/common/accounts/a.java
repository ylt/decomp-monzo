package co.uk.getmondo.common.accounts;

import android.accounts.AbstractAccountAuthenticator;
import android.accounts.Account;
import android.accounts.AccountAuthenticatorResponse;
import android.accounts.AccountManager;
import android.accounts.NetworkErrorException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import co.uk.getmondo.splash.SplashActivity;

class a extends AbstractAccountAuthenticator {
   private final Context a;

   a(Context var1) {
      super(var1);
      this.a = var1;
   }

   private Bundle a(AccountAuthenticatorResponse var1, String var2) {
      Intent var3 = new Intent(this.a, SplashActivity.class);
      var3.putExtra("accountType", var2);
      var3.putExtra("accountAuthenticatorResponse", var1);
      Bundle var4 = new Bundle();
      var4.putParcelable("intent", var3);
      return var4;
   }

   public Bundle addAccount(AccountAuthenticatorResponse var1, String var2, String var3, String[] var4, Bundle var5) throws NetworkErrorException {
      return this.a(var1, var2);
   }

   public Bundle confirmCredentials(AccountAuthenticatorResponse var1, Account var2, Bundle var3) throws NetworkErrorException {
      return null;
   }

   public Bundle editProperties(AccountAuthenticatorResponse var1, String var2) {
      return null;
   }

   public Bundle getAuthToken(AccountAuthenticatorResponse var1, Account var2, String var3, Bundle var4) throws NetworkErrorException {
      var3 = AccountManager.get(this.a).peekAuthToken(var2, var3);
      Bundle var5;
      if(!TextUtils.isEmpty(var3)) {
         var5 = new Bundle();
         var5.putString("authAccount", var2.name);
         var5.putString("accountType", var2.type);
         var5.putString("authtoken", var3);
      } else {
         var5 = this.a(var1, var2.type);
      }

      return var5;
   }

   public String getAuthTokenLabel(String var1) {
      return this.a.getString(2131362012);
   }

   public Bundle hasFeatures(AccountAuthenticatorResponse var1, Account var2, String[] var3) throws NetworkErrorException {
      return null;
   }

   public Bundle updateCredentials(AccountAuthenticatorResponse var1, Account var2, String var3, Bundle var4) throws NetworkErrorException {
      return null;
   }
}
