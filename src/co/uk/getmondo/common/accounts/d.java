package co.uk.getmondo.common.accounts;

import co.uk.getmondo.common.q;
import co.uk.getmondo.d.ac;
import co.uk.getmondo.d.ai;
import co.uk.getmondo.d.ak;
import co.uk.getmondo.d.an;
import io.reactivex.u;
import io.reactivex.v;
import io.realm.av;

public class d {
   private final k a;
   private final u b;
   private final q c;
   private final co.uk.getmondo.waitlist.i d;
   private final co.uk.getmondo.payments.send.payment_category.b.a e;
   private final co.uk.getmondo.settings.k f;
   private final co.uk.getmondo.signup.identity_verification.a.h g;
   private final co.uk.getmondo.signup.status.g h;
   private final co.uk.getmondo.migration.d i;

   public d(k var1, u var2, co.uk.getmondo.waitlist.i var3, q var4, co.uk.getmondo.payments.send.payment_category.b.a var5, co.uk.getmondo.settings.k var6, co.uk.getmondo.signup.identity_verification.a.h var7, co.uk.getmondo.signup.status.g var8, co.uk.getmondo.migration.d var9) {
      this.a = var1;
      this.b = var2;
      this.c = var4;
      this.d = var3;
      this.e = var5;
      this.f = var6;
      this.g = var7;
      this.h = var8;
      this.i = var9;
   }

   // $FF: synthetic method
   static void a(av var0) {
      var0.l();
   }

   // $FF: synthetic method
   static void f() throws Exception {
      // $FF: Couldn't be decompiled
   }

   private void g() {
      io.reactivex.b.a(h.b()).b(this.b).c();
   }

   public void a(l var1) {
      synchronized(this){}

      try {
         ak var2 = this.a.c();
         this.a.a(var2.a(var1));
      } finally {
         ;
      }

   }

   public void a(co.uk.getmondo.d.a var1) {
      synchronized(this){}

      try {
         ak var2 = this.a.c();
         this.a.a(var2.a(var1));
      } finally {
         ;
      }

   }

   public void a(ac var1) {
      synchronized(this){}

      try {
         ak var2 = this.a.c();
         this.a.a(var2.a(var1));
      } finally {
         ;
      }

   }

   public void a(ac var1, an var2) {
      synchronized(this){}

      try {
         ak var3 = this.a.c();
         this.a.a(var3.a(var1, var2));
      } finally {
         ;
      }

   }

   public void a(ai var1, String var2, String var3) {
      synchronized(this){}

      try {
         this.a.a(var1, var2);
         k var7 = this.a;
         ak var6 = new ak(var3);
         var7.a(var6);
      } finally {
         ;
      }

   }

   public void a(an var1) {
      synchronized(this){}

      try {
         ak var2 = this.a.c();
         this.a.a(var2.a(var1));
      } finally {
         ;
      }

   }

   public boolean a() {
      synchronized(this){}

      boolean var1;
      try {
         var1 = this.a.e();
      } finally {
         ;
      }

      return var1;
   }

   public ak b() {
      return this.a.c();
   }

   public v c() {
      k var1 = this.a;
      var1.getClass();
      return v.c(e.a(var1));
   }

   @Deprecated
   public v d() {
      return this.c().d(f.a()).d(g.a());
   }

   public void e() {
      synchronized(this){}

      try {
         this.a.d();
         this.d.c();
         this.f.e();
         this.g();
         this.c.b();
         this.e.b();
         this.g.i();
         this.h.c();
         this.i.c();
      } finally {
         ;
      }

   }
}
