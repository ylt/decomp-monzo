package co.uk.getmondo.common.accounts;

import co.uk.getmondo.d.ac;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\f\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B#\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007¢\u0006\u0002\u0010\bJ\u000b\u0010\u000f\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u0010\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\u000b\u0010\u0011\u001a\u0004\u0018\u00010\u0007HÆ\u0003J-\u0010\u0012\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007HÆ\u0001J\u0013\u0010\u0013\u001a\u00020\u00142\b\u0010\u0015\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0016\u001a\u00020\u0017HÖ\u0001J\t\u0010\u0018\u001a\u00020\u0007HÖ\u0001R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0007¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e¨\u0006\u0019"},
   d2 = {"Lco/uk/getmondo/common/accounts/ProfileAndAccountInfo;", "", "profile", "Lco/uk/getmondo/model/Profile;", "mainAccount", "Lco/uk/getmondo/model/Account;", "secondaryAccountId", "", "(Lco/uk/getmondo/model/Profile;Lco/uk/getmondo/model/Account;Ljava/lang/String;)V", "getMainAccount", "()Lco/uk/getmondo/model/Account;", "getProfile", "()Lco/uk/getmondo/model/Profile;", "getSecondaryAccountId", "()Ljava/lang/String;", "component1", "component2", "component3", "copy", "equals", "", "other", "hashCode", "", "toString", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class l {
   private final ac a;
   private final co.uk.getmondo.d.a b;
   private final String c;

   public l(ac var1, co.uk.getmondo.d.a var2, String var3) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
   }

   public final ac a() {
      return this.a;
   }

   public final co.uk.getmondo.d.a b() {
      return this.b;
   }

   public final String c() {
      return this.c;
   }

   public boolean equals(Object var1) {
      boolean var2;
      if(this != var1) {
         label30: {
            if(var1 instanceof l) {
               l var3 = (l)var1;
               if(kotlin.d.b.l.a(this.a, var3.a) && kotlin.d.b.l.a(this.b, var3.b) && kotlin.d.b.l.a(this.c, var3.c)) {
                  break label30;
               }
            }

            var2 = false;
            return var2;
         }
      }

      var2 = true;
      return var2;
   }

   public int hashCode() {
      int var3 = 0;
      ac var4 = this.a;
      int var1;
      if(var4 != null) {
         var1 = var4.hashCode();
      } else {
         var1 = 0;
      }

      co.uk.getmondo.d.a var5 = this.b;
      int var2;
      if(var5 != null) {
         var2 = var5.hashCode();
      } else {
         var2 = 0;
      }

      String var6 = this.c;
      if(var6 != null) {
         var3 = var6.hashCode();
      }

      return (var2 + var1 * 31) * 31 + var3;
   }

   public String toString() {
      return "ProfileAndAccountInfo(profile=" + this.a + ", mainAccount=" + this.b + ", secondaryAccountId=" + this.c + ")";
   }
}
