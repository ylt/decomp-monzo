package co.uk.getmondo.common.accounts;

import co.uk.getmondo.d.ac;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B\u0007\b\u0007¢\u0006\u0002\u0010\u0002J*\u0010\u0003\u001a\u00020\u00042\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u00062\f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\n0\t2\u0006\u0010\u000b\u001a\u00020\f¨\u0006\r"},
   d2 = {"Lco/uk/getmondo/common/accounts/ProfileAndAccountInfoZipper;", "", "()V", "zip", "Lco/uk/getmondo/common/accounts/ProfileAndAccountInfo;", "accounts", "", "Lco/uk/getmondo/model/Account;", "profileOptional", "Lcom/memoizrlabs/poweroptional/Optional;", "Lco/uk/getmondo/model/Profile;", "isPrepaidAccountMigrated", "", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class m {
   public final l a(List var1, com.c.b.b var2, boolean var3) {
      Object var6 = null;
      kotlin.d.b.l.b(var1, "accounts");
      kotlin.d.b.l.b(var2, "profileOptional");
      co.uk.getmondo.d.a var5 = (co.uk.getmondo.d.a)null;
      String var4 = (String)null;
      Boolean var7 = co.uk.getmondo.a.c;
      kotlin.d.b.l.a(var7, "BuildConfig.BANK");
      Object var8;
      co.uk.getmondo.d.a var9;
      Iterator var13;
      if(var7.booleanValue()) {
         var13 = ((Iterable)var1).iterator();

         do {
            if(!var13.hasNext()) {
               var8 = null;
               break;
            }

            var8 = var13.next();
         } while(!kotlin.d.b.l.a(((co.uk.getmondo.d.a)var8).d(), co.uk.getmondo.d.a.b.RETAIL));

         var9 = (co.uk.getmondo.d.a)var8;
      } else if(var1.size() == 1) {
         var9 = (co.uk.getmondo.d.a)var1.get(0);
      } else if(var1.size() > 1) {
         if(var3) {
            var13 = ((Iterable)var1).iterator();

            Object var11;
            do {
               if(!var13.hasNext()) {
                  var11 = null;
                  break;
               }

               var11 = var13.next();
            } while(!kotlin.d.b.l.a(((co.uk.getmondo.d.a)var11).d(), co.uk.getmondo.d.a.b.RETAIL));

            var5 = (co.uk.getmondo.d.a)var11;
            Iterator var14 = ((Iterable)var1).iterator();

            do {
               if(!var14.hasNext()) {
                  var8 = null;
                  break;
               }

               var8 = var14.next();
            } while(!kotlin.d.b.l.a(((co.uk.getmondo.d.a)var8).d(), co.uk.getmondo.d.a.b.PREPAID));

            var9 = (co.uk.getmondo.d.a)var8;
            String var12;
            if(var9 != null) {
               var12 = var9.a();
            } else {
               var12 = null;
            }

            var4 = var12;
            var9 = var5;
         } else {
            var13 = ((Iterable)var1).iterator();

            do {
               if(!var13.hasNext()) {
                  var8 = null;
                  break;
               }

               var8 = var13.next();
            } while(!kotlin.d.b.l.a(((co.uk.getmondo.d.a)var8).d(), co.uk.getmondo.d.a.b.PREPAID));

            var9 = (co.uk.getmondo.d.a)var8;
         }
      } else {
         var9 = var5;
      }

      ac var10;
      if(var2.d()) {
         var10 = (ac)var6;
      } else {
         var10 = (ac)var2.a();
      }

      return new l(var10, var9, var4);
   }
}
