package co.uk.getmondo.common.accounts;

public final class p implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;

   static {
      boolean var0;
      if(!p.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   public p(javax.a.a var1) {
      if(!a && var1 == null) {
         throw new AssertionError();
      } else {
         this.b = var1;
      }
   }

   public static b.a.b a(javax.a.a var0) {
      return new p(var0);
   }

   public o a() {
      return new o((k)this.b.b());
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
