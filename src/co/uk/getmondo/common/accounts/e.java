package co.uk.getmondo.common.accounts;

import java.util.concurrent.Callable;

// $FF: synthetic class
final class e implements Callable {
   private final k a;

   private e(k var1) {
      this.a = var1;
   }

   public static Callable a(k var0) {
      return new e(var0);
   }

   public Object call() {
      return this.a.c();
   }
}
