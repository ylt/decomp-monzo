package co.uk.getmondo.common.accounts;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AuthenticatorException;
import android.accounts.OnAccountsUpdateListener;
import android.accounts.OperationCanceledException;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Build.VERSION;
import co.uk.getmondo.d.ab;
import co.uk.getmondo.d.ad;
import co.uk.getmondo.d.ai;
import co.uk.getmondo.d.ak;
import java.io.IOException;

public class k {
   private final AccountManager a;
   private final com.google.gson.f b;
   private final Handler c = new Handler(Looper.getMainLooper());
   private ak d;

   public k(AccountManager var1) {
      this.a = var1;
      co.uk.getmondo.common.g.b var2 = co.uk.getmondo.common.g.b.a(co.uk.getmondo.d.a.class, "account").b(ab.class, "prepaid").b(ad.class, "retail");
      Class var3;
      if(co.uk.getmondo.a.c.booleanValue()) {
         var3 = ad.class;
      } else {
         var3 = ab.class;
      }

      co.uk.getmondo.common.g.b var4 = var2.a(var3);
      this.b = (new com.google.gson.g()).a(var4).a();
   }

   private Account f() {
      Account[] var1 = this.a.getAccountsByType("co.uk.getmondo");
      Account var2;
      if(var1.length <= 0) {
         var2 = null;
      } else {
         var2 = var1[0];
      }

      return var2;
   }

   public ai a() {
      Account var1 = this.f();
      ai var8;
      if(var1 != null) {
         Object var7;
         try {
            String var2 = this.a.blockingGetAuthToken(var1, "co.uk.getmondo_authTokenType", true);
            String var9 = this.a.getUserData(var1, "co.uk.getmondo_refreshData");
            ai.a var3 = (ai.a)this.b.a(var9, ai.a.class);
            var8 = new ai(var2, var3);
            return var8;
         } catch (OperationCanceledException var4) {
            var7 = var4;
         } catch (IOException var5) {
            var7 = var5;
         } catch (AuthenticatorException var6) {
            var7 = var6;
         }

         d.a.a.a((Throwable)(new RuntimeException("Failed to get auth token", (Throwable)var7)));
      }

      var8 = null;
      return var8;
   }

   public void a(OnAccountsUpdateListener var1) {
      this.a.addOnAccountsUpdatedListener(var1, this.c, true);
   }

   public void a(ai var1) {
      Account var2 = this.f();
      this.a.setAuthToken(var2, "co.uk.getmondo_authTokenType", var1.b());
      this.a.setUserData(var2, "co.uk.getmondo_refreshData", this.b.a(var1.c()));
   }

   public void a(ai var1, String var2) {
      Account var3 = new Account(var2, "co.uk.getmondo");
      this.a.addAccountExplicitly(var3, (String)null, (Bundle)null);
      this.a(var1);
   }

   public void a(ak param1) {
      // $FF: Couldn't be decompiled
   }

   public String b() {
      Account var1 = this.f();
      String var6;
      if(var1 != null) {
         Object var5;
         try {
            var6 = this.a.blockingGetAuthToken(var1, "co.uk.getmondo_authTokenType", true);
            return var6;
         } catch (OperationCanceledException var2) {
            var5 = var2;
         } catch (IOException var3) {
            var5 = var3;
         } catch (AuthenticatorException var4) {
            var5 = var4;
         }

         d.a.a.a((Throwable)(new RuntimeException("Failed to get auth token", (Throwable)var5)));
      }

      var6 = null;
      return var6;
   }

   public void b(OnAccountsUpdateListener var1) {
      this.a.removeOnAccountsUpdatedListener(var1);
   }

   public ak c() {
      // $FF: Couldn't be decompiled
   }

   public void d() {
      this.d = null;
      Account var1 = this.f();
      if(var1 != null) {
         if(VERSION.SDK_INT < 22) {
            this.a.removeAccount(var1, (AccountManagerCallback)null, (Handler)null);
         } else {
            this.a.removeAccountExplicitly(var1);
         }
      }

   }

   public boolean e() {
      boolean var1;
      if(this.f() != null) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }
}
