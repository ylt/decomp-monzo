package co.uk.getmondo.golden_ticket;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import co.uk.getmondo.common.ui.VideoImageView;

public class GoldenTicketActivity_ViewBinding implements Unbinder {
   private GoldenTicketActivity a;

   public GoldenTicketActivity_ViewBinding(GoldenTicketActivity var1, View var2) {
      this.a = var1;
      var1.progressBar = (ProgressBar)Utils.findRequiredViewAsType(var2, 2131820912, "field 'progressBar'", ProgressBar.class);
      var1.titleTextView = (TextView)Utils.findRequiredViewAsType(var2, 2131820914, "field 'titleTextView'", TextView.class);
      var1.bodyTextView = (TextView)Utils.findRequiredViewAsType(var2, 2131820917, "field 'bodyTextView'", TextView.class);
      var1.imageView = (ImageView)Utils.findRequiredViewAsType(var2, 2131820916, "field 'imageView'", ImageView.class);
      var1.videoImageView = (VideoImageView)Utils.findRequiredViewAsType(var2, 2131820915, "field 'videoImageView'", VideoImageView.class);
      var1.shareLinkButton = (Button)Utils.findRequiredViewAsType(var2, 2131820918, "field 'shareLinkButton'", Button.class);
   }

   public void unbind() {
      GoldenTicketActivity var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.progressBar = null;
         var1.titleTextView = null;
         var1.bodyTextView = null;
         var1.imageView = null;
         var1.videoImageView = null;
         var1.shareLinkButton = null;
      }
   }
}
