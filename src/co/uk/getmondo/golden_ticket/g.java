package co.uk.getmondo.golden_ticket;

import co.uk.getmondo.api.MonzoApi;
import co.uk.getmondo.api.model.ApiGoldenTicket;
import co.uk.getmondo.api.model.tracking.Impression;
import io.reactivex.u;
import io.reactivex.v;

public class g extends co.uk.getmondo.common.ui.b {
   private final u c;
   private final u d;
   private final String e;
   private final boolean f;
   private final MonzoApi g;
   private final co.uk.getmondo.common.e.a h;
   private final co.uk.getmondo.common.a i;
   private String j;

   g(u var1, u var2, String var3, boolean var4, MonzoApi var5, co.uk.getmondo.common.e.a var6, co.uk.getmondo.common.a var7) {
      this.c = var1;
      this.d = var2;
      this.e = var3;
      this.f = var4;
      this.g = var5;
      this.h = var6;
      this.i = var7;
   }

   // $FF: synthetic method
   static void a(g.a var0, io.reactivex.b.b var1) throws Exception {
      var0.a();
   }

   // $FF: synthetic method
   static void a(g var0, co.uk.getmondo.common.b.a var1) throws Exception {
      var0.i.a(Impression.j(var0.f));
   }

   // $FF: synthetic method
   static void a(g var0, g.a var1, ApiGoldenTicket var2) throws Exception {
      var0.j = var2.b();
      if(var2.a() == ApiGoldenTicket.GoldenTicketStatus.ACTIVE) {
         if(var0.f) {
            var1.c();
         } else {
            var1.d();
         }
      } else {
         var1.e();
      }

   }

   // $FF: synthetic method
   static void a(g var0, g.a var1, co.uk.getmondo.common.b.a var2) throws Exception {
      var1.a(var0.j);
   }

   // $FF: synthetic method
   static void a(g var0, g.a var1, Throwable var2) throws Exception {
      var0.h.a(var2, var1);
   }

   public void a(g.a var1) {
      super.a((co.uk.getmondo.common.ui.f)var1);
      this.i.a(Impression.i(this.f));
      v var2 = this.g.goldenTicketStatus(this.e).b(this.c).a(this.d).b(h.a(var1));
      var1.getClass();
      this.a((io.reactivex.b.b)var2.a(i.a(var1)).a(j.a(this, var1), k.a(this, var1)));
      this.a((io.reactivex.b.b)var1.f().doOnNext(l.a(this)).subscribe(m.a(this, var1)));
   }

   interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      void a();

      void a(String var1);

      void b();

      void c();

      void d();

      void e();

      io.reactivex.n f();
   }
}
