package co.uk.getmondo.golden_ticket;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.uk.getmondo.common.ui.VideoImageView;
import co.uk.getmondo.d.o;

public class GoldenTicketActivity extends co.uk.getmondo.common.activities.b implements g.a {
   g a;
   @BindView(2131820917)
   TextView bodyTextView;
   @BindView(2131820916)
   ImageView imageView;
   @BindView(2131820912)
   ProgressBar progressBar;
   @BindView(2131820918)
   Button shareLinkButton;
   @BindView(2131820914)
   TextView titleTextView;
   @BindView(2131820915)
   VideoImageView videoImageView;

   // $FF: synthetic method
   static co.uk.getmondo.common.b.a a(Object var0) throws Exception {
      return co.uk.getmondo.common.b.a.a;
   }

   public static void a(Context var0, o var1, boolean var2) {
      Intent var3 = new Intent(var0, GoldenTicketActivity.class);
      var3.putExtra("KEY_GOLDEN_TICKET_ID", var1.a());
      var3.putExtra("KEY_IS_FIRST_GOLDEN_TICKET", var2);
      var0.startActivity(var3);
   }

   public void a() {
      this.progressBar.setVisibility(0);
   }

   public void a(String var1) {
      this.startActivity(co.uk.getmondo.common.k.j.a(this, this.getString(2131362208), var1, co.uk.getmondo.api.model.tracking.a.GOLDEN_TICKET));
   }

   public void b() {
      this.progressBar.setVisibility(8);
   }

   public void c() {
      this.titleTextView.setText(2131362206);
      this.bodyTextView.setText(2131362205);
      this.shareLinkButton.setVisibility(0);
      this.videoImageView.setVisibility(0);
   }

   public void d() {
      this.titleTextView.setText(2131362210);
      this.bodyTextView.setText(2131362209);
      this.shareLinkButton.setVisibility(0);
      this.videoImageView.setVisibility(0);
   }

   public void e() {
      this.titleTextView.setText(2131362212);
      this.bodyTextView.setText(2131362211);
      this.shareLinkButton.setVisibility(8);
      this.imageView.setVisibility(0);
   }

   public io.reactivex.n f() {
      return com.b.a.c.c.a(this.shareLinkButton).map(a.a());
   }

   protected void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.setContentView(2131034172);
      ButterKnife.bind((Activity)this);
      Intent var2 = this.getIntent();
      this.l().a(new d(var2.getStringExtra("KEY_GOLDEN_TICKET_ID"), var2.getBooleanExtra("KEY_IS_FIRST_GOLDEN_TICKET", false))).a(this);
      this.a.a((g.a)this);
   }

   protected void onDestroy() {
      this.videoImageView.a();
      this.a.b();
      super.onDestroy();
   }
}
