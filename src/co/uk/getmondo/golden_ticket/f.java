package co.uk.getmondo.golden_ticket;

public final class f implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final d b;

   static {
      boolean var0;
      if(!f.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   public f(d var1) {
      if(!a && var1 == null) {
         throw new AssertionError();
      } else {
         this.b = var1;
      }
   }

   public static b.a.b a(d var0) {
      return new f(var0);
   }

   public String a() {
      return (String)b.a.d.a(this.b.a(), "Cannot return null from a non-@Nullable @Provides method");
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
