package co.uk.getmondo.golden_ticket;

// $FF: synthetic class
final class k implements io.reactivex.c.g {
   private final g a;
   private final g.a b;

   private k(g var1, g.a var2) {
      this.a = var1;
      this.b = var2;
   }

   public static io.reactivex.c.g a(g var0, g.a var1) {
      return new k(var0, var1);
   }

   public void a(Object var1) {
      g.a(this.a, this.b, (Throwable)var1);
   }
}
