package co.uk.getmondo.b;

import android.app.Activity;
import android.app.Application.ActivityLifecycleCallbacks;
import android.os.Bundle;
import co.uk.getmondo.api.model.tracking.Impression;

public class c implements ActivityLifecycleCallbacks {
   private final co.uk.getmondo.common.a a;
   private int b = 0;

   c(co.uk.getmondo.common.a var1) {
      this.a = var1;
   }

   private boolean a(Activity var1) {
      return var1.getClass().getName().startsWith("co.uk.getmondo");
   }

   public void onActivityCreated(Activity var1, Bundle var2) {
   }

   public void onActivityDestroyed(Activity var1) {
   }

   public void onActivityPaused(Activity var1) {
   }

   public void onActivityResumed(Activity var1) {
   }

   public void onActivitySaveInstanceState(Activity var1, Bundle var2) {
   }

   public void onActivityStarted(Activity var1) {
      ++this.b;
      if(this.b == 1 && this.a(var1)) {
         this.a.a(Impression.b());
      }

   }

   public void onActivityStopped(Activity var1) {
      if(this.b > 0) {
         --this.b;
      }

      if(this.b == 0 && this.a(var1)) {
         this.a.a(Impression.c());
      }

   }
}
