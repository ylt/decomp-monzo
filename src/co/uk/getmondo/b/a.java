package co.uk.getmondo.b;

import android.app.NotificationManager;
import android.content.Context;
import android.nfc.NfcAdapter;
import android.nfc.NfcManager;
import android.os.Build.VERSION;
import co.uk.getmondo.api.model.tracking.Impression;

public class a {
   private final android.support.v4.b.b.a a;
   private final co.uk.getmondo.common.a b;

   a(android.support.v4.b.b.a var1, co.uk.getmondo.common.a var2) {
      this.a = var1;
      this.b = var2;
   }

   private Impression.FingerprintSupportType a() {
      Impression.FingerprintSupportType var1;
      if(!this.a.b()) {
         var1 = Impression.FingerprintSupportType.NO_HARDWARE_DETECTED;
      } else if(!this.a.a()) {
         var1 = Impression.FingerprintSupportType.NO_ENROLLED_FINGERPRINTS;
      } else {
         var1 = Impression.FingerprintSupportType.READY_TO_USE;
      }

      return var1;
   }

   private int b(Context var1) {
      int var2;
      if(VERSION.SDK_INT >= 23) {
         var2 = ((NotificationManager)var1.getSystemService("notification")).getActiveNotifications().length;
      } else {
         var2 = -1;
      }

      return var2;
   }

   private boolean c(Context var1) {
      NfcAdapter var3 = ((NfcManager)var1.getSystemService("nfc")).getDefaultAdapter();
      boolean var2;
      if(var3 != null && var3.isEnabled()) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   public void a(Context var1) {
      try {
         Impression var3 = Impression.a(this.a(), this.b(var1), this.c(var1));
         this.b.a(var3);
      } catch (Throwable var2) {
         d.a.a.a(var2, "Failed to send analytic", new Object[0]);
      }

   }
}
