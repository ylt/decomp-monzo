package co.uk.getmondo.profile.data;

import co.uk.getmondo.api.MonzoApi;
import co.uk.getmondo.api.model.ApiPostcodeResponse;
import co.uk.getmondo.api.model.ApiProfile;
import co.uk.getmondo.api.model.LegacyApiAddress;
import co.uk.getmondo.common.accounts.d;
import co.uk.getmondo.d.ac;
import co.uk.getmondo.d.s;
import co.uk.getmondo.d.a.i;
import co.uk.getmondo.d.a.p;
import io.reactivex.n;
import io.reactivex.v;
import io.reactivex.c.g;
import io.reactivex.c.h;
import kotlin.Metadata;
import kotlin.d.b.l;
import kotlin.d.b.y;
import kotlin.reflect.e;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B'\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t¢\u0006\u0002\u0010\nJ\u001a\u0010\r\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00100\u000f0\u000e2\u0006\u0010\u0011\u001a\u00020\u0012J\u0006\u0010\u0013\u001a\u00020\u0014J\u0016\u0010\u0015\u001a\u00020\u00142\u0006\u0010\u0016\u001a\u00020\u00102\u0006\u0010\u0017\u001a\u00020\u0018R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0019"},
   d2 = {"Lco/uk/getmondo/profile/data/ProfileManager;", "", "monzoApi", "Lco/uk/getmondo/api/MonzoApi;", "profileApi", "Lco/uk/getmondo/profile/data/MonzoProfileApi;", "accountService", "Lco/uk/getmondo/common/accounts/AccountService;", "idempotencyGenerator", "Lco/uk/getmondo/topup/util/IdempotencyGenerator;", "(Lco/uk/getmondo/api/MonzoApi;Lco/uk/getmondo/profile/data/MonzoProfileApi;Lco/uk/getmondo/common/accounts/AccountService;Lco/uk/getmondo/topup/util/IdempotencyGenerator;)V", "addressMapper", "Lco/uk/getmondo/model/mapper/LegacyAddressMapper;", "lookupPostcode", "Lio/reactivex/Single;", "", "Lco/uk/getmondo/model/LegacyAddress;", "postcode", "", "syncProfile", "Lio/reactivex/Completable;", "updateAddress", "address", "challenge", "Lco/uk/getmondo/common/pin/data/model/Challenge;", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class a {
   private final i a;
   private final MonzoApi b;
   private final MonzoProfileApi c;
   private final d d;
   private final co.uk.getmondo.topup.a.a e;

   public a(MonzoApi var1, MonzoProfileApi var2, d var3, co.uk.getmondo.topup.a.a var4) {
      l.b(var1, "monzoApi");
      l.b(var2, "profileApi");
      l.b(var3, "accountService");
      l.b(var4, "idempotencyGenerator");
      super();
      this.b = var1;
      this.c = var2;
      this.d = var3;
      this.e = var4;
      this.a = new i();
   }

   public final io.reactivex.b a() {
      io.reactivex.b var1 = this.c.profile().d((h)(new b((kotlin.d.a.b)(new kotlin.d.a.b(new p()) {
         public final ac a(ApiProfile var1) {
            l.b(var1, "p1");
            return ((p)this.b).a(var1);
         }

         public final e a() {
            return y.a(p.class);
         }

         public final String b() {
            return "apply";
         }

         public final String c() {
            return "apply(Lco/uk/getmondo/api/model/ApiProfile;)Lco/uk/getmondo/model/Profile;";
         }
      })))).c((g)(new g() {
         public final void a(ac var1) {
            a.this.d.a(var1);
         }
      })).c();
      l.a(var1, "profileApi.profile()\n   …         .toCompletable()");
      return var1;
   }

   public final io.reactivex.b a(final s var1, final co.uk.getmondo.common.pin.a.a.a var2) {
      l.b(var1, "address");
      l.b(var2, "challenge");
      io.reactivex.b var3 = this.d.c().d((h)null.a).c((h)(new h() {
         public final io.reactivex.b a(String var1x) {
            l.b(var1x, "userId");
            MonzoProfileApi var3 = a.this.c;
            String[] var9 = var1.h();
            String var7 = var1.i();
            String var6 = var1.b();
            String var2x = var1.c();
            String var4 = var1.j();
            String var8 = var2.b();
            String var10 = var2.a().a();
            String var5 = a.this.e.a(var1);
            l.a(var5, "idempotencyGenerator.getKey(address)");
            return var3.updateAddress(var1x, var9, var7, var6, var2x, var4, var8, var10, var5);
         }
      })).b((io.reactivex.d)this.a());
      l.a(var3, "accountService.user()\n  …  .andThen(syncProfile())");
      return var3;
   }

   public final v a(String var1) {
      l.b(var1, "postcode");
      v var2 = this.b.lookupPostcode(var1, co.uk.getmondo.d.i.Companion.a().e()).a((h)(new h() {
         public final v a(ApiPostcodeResponse var1) {
            l.b(var1, "it");
            return n.fromIterable((Iterable)var1.a()).map((h)(new h() {
               public final s a(LegacyApiAddress var1) {
                  l.b(var1, "it");
                  return a.this.a.a(var1);
               }
            })).toList();
         }
      }));
      l.a(var2, "monzoApi.lookupPostcode(…oList()\n                }");
      return var2;
   }
}
