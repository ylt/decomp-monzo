package co.uk.getmondo.profile.address;

import co.uk.getmondo.api.model.Address;

public final class i implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final b.a b;
   private final javax.a.a c;
   private final javax.a.a d;

   static {
      boolean var0;
      if(!i.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   public i(b.a var1, javax.a.a var2, javax.a.a var3) {
      if(!a && var1 == null) {
         throw new AssertionError();
      } else {
         this.b = var1;
         if(!a && var2 == null) {
            throw new AssertionError();
         } else {
            this.c = var2;
            if(!a && var3 == null) {
               throw new AssertionError();
            } else {
               this.d = var3;
            }
         }
      }
   }

   public static b.a.b a(b.a var0, javax.a.a var1, javax.a.a var2) {
      return new i(var0, var1, var2);
   }

   public g a() {
      return (g)b.a.c.a(this.b, new g((Address)this.c.b(), (co.uk.getmondo.common.a)this.d.b()));
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
