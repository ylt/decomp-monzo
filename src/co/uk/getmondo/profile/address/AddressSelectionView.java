package co.uk.getmondo.profile.address;

import android.animation.TimeInterpolator;
import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;
import co.uk.getmondo.api.model.Address;
import co.uk.getmondo.common.ae;
import co.uk.getmondo.common.ui.ProgressButton;
import io.reactivex.p;
import io.reactivex.c.q;
import java.util.HashMap;
import java.util.List;
import kotlin.Metadata;
import kotlin.TypeCastException;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000d\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\t\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\r\n\u0002\b\b\n\u0002\u0010 \n\u0002\b\u0002\u0018\u00002\u00020\u00012\u00020\u0002B%\b\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\b\b\u0002\u0010\u0007\u001a\u00020\b¢\u0006\u0002\u0010\tJ\f\u0010#\u001a\b\u0012\u0004\u0012\u00020%0$J\f\u0010&\u001a\b\u0012\u0004\u0012\u00020%0$J\f\u0010'\u001a\b\u0012\u0004\u0012\u00020(0$J\b\u0010)\u001a\u00020%H\u0016J\b\u0010*\u001a\u00020%H\u0016J\b\u0010+\u001a\u00020%H\u0016J\b\u0010,\u001a\u00020%H\u0014J\b\u0010-\u001a\u00020%H\u0014J\u000e\u0010.\u001a\b\u0012\u0004\u0012\u00020/0$H\u0016J\u000e\u00100\u001a\b\u0012\u0004\u0012\u00020%0$H\u0016J\f\u00101\u001a\b\u0012\u0004\u0012\u00020\u00170$J\f\u00102\u001a\b\u0012\u0004\u0012\u00020%0$J\u0006\u00103\u001a\u00020%J\b\u00104\u001a\u00020%H\u0016J\b\u00105\u001a\u00020%H\u0016J\u0014\u00106\u001a\u00020%2\f\u00107\u001a\b\u0012\u0004\u0012\u00020(08J\u0006\u00109\u001a\u00020%R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R$\u0010\u000e\u001a\u00020\r2\u0006\u0010\f\u001a\u00020\r8V@VX\u0096\u000e¢\u0006\f\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011R$\u0010\u0012\u001a\u00020\r2\u0006\u0010\f\u001a\u00020\r8V@VX\u0096\u000e¢\u0006\f\u001a\u0004\b\u0012\u0010\u000f\"\u0004\b\u0013\u0010\u0011R$\u0010\u0014\u001a\u00020\r2\u0006\u0010\f\u001a\u00020\r8V@VX\u0096\u000e¢\u0006\f\u001a\u0004\b\u0014\u0010\u000f\"\u0004\b\u0015\u0010\u0011R\u0011\u0010\u0016\u001a\u00020\u00178F¢\u0006\u0006\u001a\u0004\b\u0018\u0010\u0019R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082\u0004¢\u0006\u0002\n\u0000R$\u0010\u001c\u001a\u00020\u00172\u0006\u0010\f\u001a\u00020\u00178F@FX\u0086\u000e¢\u0006\f\u001a\u0004\b\u001d\u0010\u0019\"\u0004\b\u001e\u0010\u001fR$\u0010 \u001a\u00020\u00172\u0006\u0010\f\u001a\u00020\u00178F@FX\u0086\u000e¢\u0006\f\u001a\u0004\b!\u0010\u0019\"\u0004\b\"\u0010\u001f¨\u0006:"},
   d2 = {"Lco/uk/getmondo/profile/address/AddressSelectionView;", "Landroid/widget/LinearLayout;", "Lco/uk/getmondo/profile/address/AddressSelectionPresenter$View;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyle", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "addressAdapter", "Lco/uk/getmondo/profile/address/AddressAdapter;", "value", "", "isAddressLoading", "()Z", "setAddressLoading", "(Z)V", "isPrimaryButtonEnabled", "setPrimaryButtonEnabled", "isPrimaryButtonVisible", "setPrimaryButtonVisible", "postcode", "", "getPostcode", "()Ljava/lang/String;", "presenter", "Lco/uk/getmondo/profile/address/AddressSelectionPresenter;", "primaryButtonText", "getPrimaryButtonText", "setPrimaryButtonText", "(Ljava/lang/String;)V", "title", "getTitle", "setTitle", "addressNotInListButtonClicks", "Lio/reactivex/Observable;", "", "addressResidenceButtonClicks", "addressSelected", "Lco/uk/getmondo/api/model/Address;", "clearPostcodeError", "hideAddressNotInListButton", "hideAddressPicker", "onAttachedToWindow", "onDetachedFromWindow", "onPostcodeChanged", "", "onViewedAddresses", "postcodeReady", "primaryButtonClicks", "showAddressNotFoundError", "showAddressNotInListButton", "showAddressPicker", "showAddresses", "addresses", "", "showInvalidPostcodeError", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class AddressSelectionView extends LinearLayout implements e.a {
   private final a a;
   private final e b;
   private HashMap c;

   public AddressSelectionView(Context var1) {
      this(var1, (AttributeSet)null, 0, 6, (kotlin.d.b.i)null);
   }

   public AddressSelectionView(Context var1, AttributeSet var2) {
      this(var1, var2, 0, 4, (kotlin.d.b.i)null);
   }

   public AddressSelectionView(Context var1, AttributeSet var2, int var3) {
      kotlin.d.b.l.b(var1, "context");
      super(var1, var2, var3);
      this.a = new a((List)null, (kotlin.d.a.b)null, 3, (kotlin.d.b.i)null);
      this.b = new e();
      this.setOrientation(1);
      LayoutInflater.from(var1).inflate(2131034428, (ViewGroup)this);
      LinearLayoutManager var4 = new LinearLayoutManager(var1);
      ((RecyclerView)this.a(co.uk.getmondo.c.a.addressRecyclerView)).setLayoutManager((android.support.v7.widget.RecyclerView.h)(new LinearLayoutManager(var1)));
      ((RecyclerView)this.a(co.uk.getmondo.c.a.addressRecyclerView)).setAdapter((android.support.v7.widget.RecyclerView.a)this.a);
      ((RecyclerView)this.a(co.uk.getmondo.c.a.addressRecyclerView)).a((android.support.v7.widget.RecyclerView.g)(new co.uk.getmondo.common.ui.e(var1, var4.h(), (Drawable)null, this.getResources().getDimensionPixelSize(2131427603), 0, 0, 0, 116, (kotlin.d.b.i)null)));
      if(var2 != null) {
         TypedArray var6 = var1.obtainStyledAttributes(var2, co.uk.getmondo.c.b.AddressSelectionView, 0, 0);
         String var5 = var6.getString(1);
         if(var5 == null) {
            var5 = "";
         }

         this.setPrimaryButtonText(var5);
         var5 = var6.getString(0);
         if(var5 == null) {
            var5 = "";
         }

         this.setTitle(var5);
         var6.recycle();
      }

   }

   // $FF: synthetic method
   public AddressSelectionView(Context var1, AttributeSet var2, int var3, int var4, kotlin.d.b.i var5) {
      if((var4 & 2) != 0) {
         var2 = (AttributeSet)null;
      }

      if((var4 & 4) != 0) {
         var3 = 0;
      }

      this(var1, var2, var3);
   }

   public View a(int var1) {
      if(this.c == null) {
         this.c = new HashMap();
      }

      View var3 = (View)this.c.get(Integer.valueOf(var1));
      View var2 = var3;
      if(var3 == null) {
         var2 = this.findViewById(var1);
         this.c.put(Integer.valueOf(var1), var2);
      }

      return var2;
   }

   public io.reactivex.n a() {
      com.b.a.a var1 = com.b.a.d.e.c((TextInputEditText)this.a(co.uk.getmondo.c.a.addressPostcodeEditText));
      kotlin.d.b.l.a(var1, "RxTextView.textChanges(this)");
      io.reactivex.n var2 = var1.b();
      kotlin.d.b.l.a(var2, "addressPostcodeEditText.…nges().skipInitialValue()");
      return var2;
   }

   public final void a(final List var1) {
      kotlin.d.b.l.b(var1, "addresses");
      ((TextInputEditText)this.a(co.uk.getmondo.c.a.addressPostcodeEditText)).clearFocus();
      ae.c((TextInputEditText)this.a(co.uk.getmondo.c.a.addressPostcodeEditText));
      this.postDelayed((Runnable)(new Runnable() {
         public final void run() {
            if(AddressSelectionView.this.isAttachedToWindow()) {
               AddressSelectionView.this.a.a(var1);
               ((RecyclerView)AddressSelectionView.this.a(co.uk.getmondo.c.a.addressRecyclerView)).a(0);
               AddressSelectionView.this.n();
            }

         }
      }), 300L);
   }

   public io.reactivex.n b() {
      io.reactivex.n var1 = io.reactivex.n.create((p)(new p() {
         public final void a(final io.reactivex.o var1) {
            kotlin.d.b.l.b(var1, "emitter");
            ((RecyclerView)AddressSelectionView.this.a(co.uk.getmondo.c.a.addressRecyclerView)).setOnScrollListener((android.support.v7.widget.RecyclerView.m)(new android.support.v7.widget.RecyclerView.m() {
               public void onScrollStateChanged(RecyclerView var1x, int var2) {
                  super.onScrollStateChanged(var1x, var2);
                  var1.a(kotlin.n.a);
               }
            }));
            var1.a((io.reactivex.c.f)(new io.reactivex.c.f() {
               public final void a() {
                  ((RecyclerView)AddressSelectionView.this.a(co.uk.getmondo.c.a.addressRecyclerView)).setOnScrollListener((android.support.v7.widget.RecyclerView.m)null);
               }
            }));
         }
      })).filter((q)(new q() {
         public final boolean a(kotlin.n var1) {
            kotlin.d.b.l.b(var1, "it");
            boolean var2;
            if(!((RecyclerView)AddressSelectionView.this.a(co.uk.getmondo.c.a.addressRecyclerView)).canScrollVertically(130)) {
               var2 = true;
            } else {
               var2 = false;
            }

            return var2;
         }
      })).map((io.reactivex.c.h)null.a);
      kotlin.d.b.l.a(var1, "Observable.create<Unit> …\n                .map { }");
      return var1;
   }

   public void c() {
      if(((Button)this.a(co.uk.getmondo.c.a.addressNotInListButton)).getVisibility() != 0) {
         final Context var1 = this.getContext();
         if(var1 instanceof Activity) {
            ((Button)this.a(co.uk.getmondo.c.a.addressNotInListButton)).post((Runnable)(new Runnable() {
               public final void run() {
                  ((Activity)var1).getWindow().setSoftInputMode(36);
               }
            }));
         }

         ae.a((View)((Button)this.a(co.uk.getmondo.c.a.addressNotInListButton)));
      }

   }

   public void d() {
      if(((Button)this.a(co.uk.getmondo.c.a.addressNotInListButton)).getVisibility() == 0) {
         ae.b((Button)this.a(co.uk.getmondo.c.a.addressNotInListButton));
         final Context var1 = this.getContext();
         if(var1 instanceof Activity) {
            ((Button)this.a(co.uk.getmondo.c.a.addressNotInListButton)).post((Runnable)(new Runnable() {
               public final void run() {
                  ((Activity)var1).getWindow().setSoftInputMode(20);
               }
            }));
         }
      }

   }

   public void e() {
      float var1 = (float)this.getResources().getDimensionPixelSize(2131427426);
      if(((ConstraintLayout)this.a(co.uk.getmondo.c.a.addressPickerLayout)).getTranslationY() != var1) {
         ScrollView var3 = (ScrollView)this.a(co.uk.getmondo.c.a.addressScrollView);
         LayoutParams var2 = new LayoutParams(-1, 0);
         var2.weight = 1.0F;
         var3.setLayoutParams((android.view.ViewGroup.LayoutParams)var2);
         ((ConstraintLayout)this.a(co.uk.getmondo.c.a.addressPickerLayout)).setLayoutParams((android.view.ViewGroup.LayoutParams)(new LayoutParams(-1, -2)));
         ConstraintLayout var4 = (ConstraintLayout)this.a(co.uk.getmondo.c.a.addressPickerLayout);
         ae.b((View)var4);
         var4.animate().translationY(var1).setInterpolator((TimeInterpolator)(new android.support.v4.view.b.b()));
      }

   }

   public void f() {
      ((TextInputLayout)this.a(co.uk.getmondo.c.a.addressPostcodeInputLayout)).setErrorEnabled(false);
      ((TextInputLayout)this.a(co.uk.getmondo.c.a.addressPostcodeInputLayout)).setError((CharSequence)null);
   }

   public final io.reactivex.n g() {
      io.reactivex.n var1 = com.b.a.c.c.a((TextInputEditText)this.a(co.uk.getmondo.c.a.addressResidenceEditText)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var1, "RxView.clicks(this).map(VoidToUnit)");
      return var1;
   }

   public final String getPostcode() {
      return ((TextInputEditText)this.a(co.uk.getmondo.c.a.addressPostcodeEditText)).getText().toString();
   }

   public final String getPrimaryButtonText() {
      return ((ProgressButton)this.a(co.uk.getmondo.c.a.addressPrimaryButton)).getText().toString();
   }

   public final String getTitle() {
      return ((TextView)this.a(co.uk.getmondo.c.a.addressBodyText)).getText().toString();
   }

   public final io.reactivex.n h() {
      io.reactivex.n var1 = com.b.a.c.c.a((ProgressButton)this.a(co.uk.getmondo.c.a.addressPrimaryButton)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var1, "RxView.clicks(this).map(VoidToUnit)");
      return var1;
   }

   public final io.reactivex.n i() {
      io.reactivex.n var1 = this.h().map((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final String a(kotlin.n var1) {
            kotlin.d.b.l.b(var1, "it");
            String var2 = AddressSelectionView.this.getPostcode();
            if(var2 == null) {
               throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
            } else {
               return kotlin.h.j.b((CharSequence)var2).toString();
            }
         }
      }));
      kotlin.d.b.l.a(var1, "primaryButtonClicks().map { postcode.trim() }");
      return var1;
   }

   public final io.reactivex.n j() {
      io.reactivex.n var1 = com.b.a.c.c.a((Button)this.a(co.uk.getmondo.c.a.addressNotInListButton)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var1, "RxView.clicks(this).map(VoidToUnit)");
      return var1;
   }

   public final io.reactivex.n k() {
      io.reactivex.n var1 = io.reactivex.n.create((p)(new p() {
         public final void a(final io.reactivex.o var1) {
            kotlin.d.b.l.b(var1, "emitter");
            AddressSelectionView.this.a.a((kotlin.d.a.b)(new kotlin.d.a.b() {
               public final void a(Address var1x) {
                  kotlin.d.b.l.b(var1x, "it");
                  var1.a(var1x);
               }
            }));
            var1.a((io.reactivex.c.f)(new io.reactivex.c.f() {
               public final void a() {
                  AddressSelectionView.this.a.a((kotlin.d.a.b)null);
               }
            }));
         }
      }));
      kotlin.d.b.l.a(var1, "Observable.create { emit…stener = null }\n        }");
      return var1;
   }

   public final void l() {
      ((TextInputLayout)this.a(co.uk.getmondo.c.a.addressPostcodeInputLayout)).setErrorEnabled(true);
      ((TextInputLayout)this.a(co.uk.getmondo.c.a.addressPostcodeInputLayout)).setError((CharSequence)this.getResources().getString(2131362495));
   }

   public final void m() {
      ((TextInputLayout)this.a(co.uk.getmondo.c.a.addressPostcodeInputLayout)).setErrorEnabled(true);
      ((TextInputLayout)this.a(co.uk.getmondo.c.a.addressPostcodeInputLayout)).setError((CharSequence)this.getResources().getString(2131362005));
   }

   public void n() {
      ScrollView var2 = (ScrollView)this.a(co.uk.getmondo.c.a.addressScrollView);
      LayoutParams var1 = new LayoutParams(-1, -2);
      var1.weight = 0.0F;
      var2.setLayoutParams((android.view.ViewGroup.LayoutParams)var1);
      ConstraintLayout var4 = (ConstraintLayout)this.a(co.uk.getmondo.c.a.addressPickerLayout);
      var1 = new LayoutParams(-1, 0);
      var1.weight = 1.0F;
      var1.topMargin = this.getResources().getDimensionPixelSize(2131427605);
      var4.setLayoutParams((android.view.ViewGroup.LayoutParams)var1);
      ConstraintLayout var3 = (ConstraintLayout)this.a(co.uk.getmondo.c.a.addressPickerLayout);
      ae.a((View)var3);
      var3.animate().translationY(0.0F).setInterpolator((TimeInterpolator)(new android.support.v4.view.b.b()));
   }

   protected void onAttachedToWindow() {
      super.onAttachedToWindow();
      this.b.a((e.a)this);
      this.post((Runnable)(new Runnable() {
         public final void run() {
            ae.a((EditText)((TextInputEditText)AddressSelectionView.this.a(co.uk.getmondo.c.a.addressPostcodeEditText)));
         }
      }));
   }

   protected void onDetachedFromWindow() {
      this.b.b();
      ae.c(this);
      super.onDetachedFromWindow();
   }

   public void setAddressLoading(boolean var1) {
      ((ProgressButton)this.a(co.uk.getmondo.c.a.addressPrimaryButton)).setLoading(var1);
   }

   public void setPrimaryButtonEnabled(boolean var1) {
      ((ProgressButton)this.a(co.uk.getmondo.c.a.addressPrimaryButton)).setEnabled(var1);
   }

   public final void setPrimaryButtonText(String var1) {
      kotlin.d.b.l.b(var1, "value");
      ((ProgressButton)this.a(co.uk.getmondo.c.a.addressPrimaryButton)).setText((CharSequence)var1);
   }

   public void setPrimaryButtonVisible(boolean var1) {
      ProgressButton var3 = (ProgressButton)this.a(co.uk.getmondo.c.a.addressPrimaryButton);
      byte var2;
      if(var1) {
         var2 = 0;
      } else {
         var2 = 8;
      }

      var3.setVisibility(var2);
   }

   public final void setTitle(String var1) {
      kotlin.d.b.l.b(var1, "value");
      ((TextView)this.a(co.uk.getmondo.c.a.addressBodyText)).setText((CharSequence)var1);
   }
}
