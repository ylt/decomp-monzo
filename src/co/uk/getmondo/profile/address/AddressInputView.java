package co.uk.getmondo.profile.address;

import android.content.Context;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import co.uk.getmondo.api.model.Address;
import co.uk.getmondo.api.model.ApiAddress;
import co.uk.getmondo.d.s;
import io.reactivex.r;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.TypeCastException;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\f\u0018\u00002\u00020\u0001B%\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bR\u0011\u0010\t\u001a\u00020\n8F¢\u0006\u0006\u001a\u0004\b\u000b\u0010\fR\u0017\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000f0\u000e8F¢\u0006\u0006\u001a\u0004\b\u0010\u0010\u0011R\u0011\u0010\u0012\u001a\u00020\u00138F¢\u0006\u0006\u001a\u0004\b\u0014\u0010\u0015R\u0011\u0010\u0016\u001a\u00020\u00178F¢\u0006\u0006\u001a\u0004\b\u0018\u0010\u0019R\u0011\u0010\u001a\u001a\u00020\u00138F¢\u0006\u0006\u001a\u0004\b\u001b\u0010\u0015R(\u0010\u001d\u001a\u0004\u0018\u00010\n2\b\u0010\u001c\u001a\u0004\u0018\u00010\n@FX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u001e\u0010\f\"\u0004\b\u001f\u0010 R\u0011\u0010!\u001a\u00020\u00138F¢\u0006\u0006\u001a\u0004\b\"\u0010\u0015¨\u0006#"},
   d2 = {"Lco/uk/getmondo/profile/address/AddressInputView;", "Landroid/widget/LinearLayout;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyle", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "address", "Lco/uk/getmondo/api/model/Address;", "getAddress", "()Lco/uk/getmondo/api/model/Address;", "addressInputValid", "Lio/reactivex/Observable;", "", "getAddressInputValid", "()Lio/reactivex/Observable;", "city", "", "getCity", "()Ljava/lang/String;", "legacyAddress", "Lco/uk/getmondo/model/LegacyAddress;", "getLegacyAddress", "()Lco/uk/getmondo/model/LegacyAddress;", "postcode", "getPostcode", "value", "preFilledAddress", "getPreFilledAddress", "setPreFilledAddress", "(Lco/uk/getmondo/api/model/Address;)V", "street", "getStreet", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class AddressInputView extends LinearLayout {
   private Address a;
   private HashMap b;

   public AddressInputView(Context var1) {
      this(var1, (AttributeSet)null, 0, 6, (kotlin.d.b.i)null);
   }

   public AddressInputView(Context var1, AttributeSet var2) {
      this(var1, var2, 0, 4, (kotlin.d.b.i)null);
   }

   public AddressInputView(Context var1, AttributeSet var2, int var3) {
      kotlin.d.b.l.b(var1, "context");
      super(var1, var2, var3);
      this.setOrientation(1);
      this.setFocusable(true);
      this.setFocusableInTouchMode(true);
      LayoutInflater.from(var1).inflate(2131034427, (ViewGroup)this);
   }

   // $FF: synthetic method
   public AddressInputView(Context var1, AttributeSet var2, int var3, int var4, kotlin.d.b.i var5) {
      if((var4 & 2) != 0) {
         var2 = (AttributeSet)null;
      }

      if((var4 & 4) != 0) {
         var3 = 0;
      }

      this(var1, var2, var3);
   }

   public View a(int var1) {
      if(this.b == null) {
         this.b = new HashMap();
      }

      View var3 = (View)this.b.get(Integer.valueOf(var1));
      View var2 = var3;
      if(var3 == null) {
         var2 = this.findViewById(var1);
         this.b.put(Integer.valueOf(var1), var2);
      }

      return var2;
   }

   public final Address getAddress() {
      Iterable var3 = (Iterable)kotlin.a.m.a((Collection)kotlin.h.j.b((CharSequence)this.getStreet(), new String[]{","}, false, 0, 6, (Object)null), this.getCity());
      Collection var2 = (Collection)(new ArrayList(kotlin.a.m.a(var3, 10)));
      Iterator var4 = var3.iterator();

      while(var4.hasNext()) {
         String var6 = (String)var4.next();
         if(var6 == null) {
            throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
         }

         var2.add(kotlin.h.j.b((CharSequence)var6).toString());
      }

      var3 = (Iterable)((List)var2);
      var2 = (Collection)(new ArrayList());
      var4 = var3.iterator();

      while(var4.hasNext()) {
         Object var7 = var4.next();
         boolean var1;
         if(!kotlin.h.j.a((CharSequence)((String)var7))) {
            var1 = true;
         } else {
            var1 = false;
         }

         if(var1) {
            var2.add(var7);
         }
      }

      List var8 = (List)var2;
      Address var5 = this.a;
      if(var5 == null || !(var5 instanceof ApiAddress) || !kotlin.d.b.l.a(var5.a(), var8) || !kotlin.d.b.l.a(((ApiAddress)var5).c(), this.getPostcode())) {
         var5 = (Address)(new ApiAddress((String)null, var8, "", this.getPostcode(), co.uk.getmondo.d.i.UNITED_KINGDOM.e()));
      }

      return var5;
   }

   public final io.reactivex.n getAddressInputValid() {
      com.b.a.a var1 = com.b.a.d.e.c((TextInputEditText)this.a(co.uk.getmondo.c.a.addressStreetEditText));
      kotlin.d.b.l.a(var1, "RxTextView.textChanges(this)");
      r var4 = (r)var1;
      com.b.a.a var2 = com.b.a.d.e.c((TextInputEditText)this.a(co.uk.getmondo.c.a.addressPostcodeEditText));
      kotlin.d.b.l.a(var2, "RxTextView.textChanges(this)");
      r var3 = (r)var2;
      var2 = com.b.a.d.e.c((TextInputEditText)this.a(co.uk.getmondo.c.a.addressCityEditText));
      kotlin.d.b.l.a(var2, "RxTextView.textChanges(this)");
      io.reactivex.n var5 = io.reactivex.n.combineLatest(var4, var3, (r)var2, (io.reactivex.c.i)null.a).distinctUntilChanged();
      kotlin.d.b.l.a(var5, "Observable.combineLatest…  .distinctUntilChanged()");
      return var5;
   }

   public final String getCity() {
      String var1 = ((TextInputEditText)this.a(co.uk.getmondo.c.a.addressCityEditText)).getText().toString();
      if(var1 == null) {
         throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
      } else {
         return kotlin.h.j.b((CharSequence)var1).toString();
      }
   }

   public final s getLegacyAddress() {
      return new s(this.getPostcode(), co.uk.getmondo.common.k.a.a(this.getStreet()), this.getCity(), co.uk.getmondo.d.i.UNITED_KINGDOM.e(), (String)null, 16, (kotlin.d.b.i)null);
   }

   public final String getPostcode() {
      String var1 = ((TextInputEditText)this.a(co.uk.getmondo.c.a.addressPostcodeEditText)).getText().toString();
      if(var1 == null) {
         throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
      } else {
         return kotlin.h.j.b((CharSequence)var1).toString();
      }
   }

   public final Address getPreFilledAddress() {
      return this.a;
   }

   public final String getStreet() {
      String var1 = ((TextInputEditText)this.a(co.uk.getmondo.c.a.addressStreetEditText)).getText().toString();
      if(var1 == null) {
         throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
      } else {
         return kotlin.h.j.b((CharSequence)var1).toString();
      }
   }

   public final void setPreFilledAddress(Address var1) {
      this.a = var1;
      ((TextInputLayout)this.a(co.uk.getmondo.c.a.addressStreetInputLayout)).setHintAnimationEnabled(false);
      ((TextInputLayout)this.a(co.uk.getmondo.c.a.addressCityInputLayout)).setHintAnimationEnabled(false);
      ((TextInputLayout)this.a(co.uk.getmondo.c.a.addressPostcodeInputLayout)).setHintAnimationEnabled(false);
      String var3;
      if(var1 instanceof ApiAddress) {
         var3 = (String)kotlin.a.m.g(var1.a());
      } else if(var1 instanceof s) {
         var3 = ((s)var1).i();
      } else {
         var3 = "";
      }

      List var4;
      if(var1 instanceof ApiAddress) {
         var4 = kotlin.a.m.d(var1.a(), 1);
      } else {
         label44: {
            if(var1 != null) {
               List var5 = var1.a();
               var4 = var5;
               if(var5 != null) {
                  break label44;
               }
            }

            var4 = kotlin.a.m.a();
         }
      }

      Iterable var7 = (Iterable)var4;
      Collection var8 = (Collection)(new ArrayList());
      Iterator var9 = var7.iterator();

      while(var9.hasNext()) {
         Object var6 = var9.next();
         boolean var2;
         if(!kotlin.h.j.a((CharSequence)((String)var6))) {
            var2 = true;
         } else {
            var2 = false;
         }

         if(var2) {
            var8.add(var6);
         }
      }

      String var10 = kotlin.a.m.a((Iterable)((List)var8), (CharSequence)", ", (CharSequence)null, (CharSequence)null, 0, (CharSequence)null, (kotlin.d.a.b)null, 62, (Object)null);
      ((TextInputEditText)this.a(co.uk.getmondo.c.a.addressStreetEditText)).setText((CharSequence)var10);
      ((TextInputEditText)this.a(co.uk.getmondo.c.a.addressCityEditText)).setText((CharSequence)var3);
      TextInputEditText var11 = (TextInputEditText)this.a(co.uk.getmondo.c.a.addressPostcodeEditText);
      if(var1 != null) {
         var3 = var1.c();
      } else {
         var3 = null;
      }

      var11.setText((CharSequence)var3);
      ((TextInputLayout)this.a(co.uk.getmondo.c.a.addressStreetInputLayout)).setHintAnimationEnabled(true);
      ((TextInputLayout)this.a(co.uk.getmondo.c.a.addressCityInputLayout)).setHintAnimationEnabled(true);
      ((TextInputLayout)this.a(co.uk.getmondo.c.a.addressPostcodeInputLayout)).setHintAnimationEnabled(true);
      if(var1 != null) {
         this.requestFocus();
      } else {
         ((TextInputEditText)this.a(co.uk.getmondo.c.a.addressStreetEditText)).requestFocus();
      }

   }
}
