package co.uk.getmondo.profile.address;

import co.uk.getmondo.api.model.Address;

public final class d implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final c b;

   static {
      boolean var0;
      if(!d.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   public d(c var1) {
      if(!a && var1 == null) {
         throw new AssertionError();
      } else {
         this.b = var1;
      }
   }

   public static b.a.b a(c var0) {
      return new d(var0);
   }

   public Address a() {
      return this.b.a();
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
