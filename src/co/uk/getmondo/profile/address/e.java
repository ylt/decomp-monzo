package co.uk.getmondo.profile.address;

import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0007B\u0005¢\u0006\u0002\u0010\u0003J\u0010\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0002H\u0016¨\u0006\b"},
   d2 = {"Lco/uk/getmondo/profile/address/AddressSelectionPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/profile/address/AddressSelectionPresenter$View;", "()V", "register", "", "view", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class e extends co.uk.getmondo.common.ui.b {
   public void a(final e.a var1) {
      kotlin.d.b.l.b(var1, "view");
      super.a((co.uk.getmondo.common.ui.f)var1);
      io.reactivex.b.a var3 = this.b;
      io.reactivex.b.b var2 = var1.a().map((io.reactivex.c.h)null.a).distinctUntilChanged().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Boolean var1x) {
            e.a var2 = var1;
            kotlin.d.b.l.a(var1x, "it");
            var2.setPrimaryButtonEnabled(var1x.booleanValue());
         }
      }));
      kotlin.d.b.l.a(var2, "view.onPostcodeChanged()…imaryButtonEnabled = it }");
      this.b = co.uk.getmondo.common.j.f.a(var3, var2);
      io.reactivex.b.a var5 = this.b;
      io.reactivex.b.b var6 = var1.a().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(CharSequence var1x) {
            var1.d();
            var1.e();
            var1.f();
            var1.setPrimaryButtonVisible(true);
         }
      }));
      kotlin.d.b.l.a(var6, "view.onPostcodeChanged()… = true\n                }");
      this.b = co.uk.getmondo.common.j.f.a(var5, var6);
      var5 = this.b;
      io.reactivex.b.b var4 = var1.b().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n var1x) {
            var1.c();
         }
      }));
      kotlin.d.b.l.a(var4, "view.onViewedAddresses()…ddressNotInListButton() }");
      this.b = co.uk.getmondo.common.j.f.a(var5, var4);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0010\r\n\u0002\b\u0004\bf\u0018\u00002\u00020\u0001J\b\u0010\u000b\u001a\u00020\fH&J\b\u0010\r\u001a\u00020\fH&J\b\u0010\u000e\u001a\u00020\fH&J\u000e\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00110\u0010H&J\u000e\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\f0\u0010H&J\b\u0010\u0013\u001a\u00020\fH&J\b\u0010\u0014\u001a\u00020\fH&R\u0018\u0010\u0002\u001a\u00020\u0003X¦\u000e¢\u0006\f\u001a\u0004\b\u0002\u0010\u0004\"\u0004\b\u0005\u0010\u0006R\u0018\u0010\u0007\u001a\u00020\u0003X¦\u000e¢\u0006\f\u001a\u0004\b\u0007\u0010\u0004\"\u0004\b\b\u0010\u0006R\u0018\u0010\t\u001a\u00020\u0003X¦\u000e¢\u0006\f\u001a\u0004\b\t\u0010\u0004\"\u0004\b\n\u0010\u0006¨\u0006\u0015"},
      d2 = {"Lco/uk/getmondo/profile/address/AddressSelectionPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "isAddressLoading", "", "()Z", "setAddressLoading", "(Z)V", "isPrimaryButtonEnabled", "setPrimaryButtonEnabled", "isPrimaryButtonVisible", "setPrimaryButtonVisible", "clearPostcodeError", "", "hideAddressNotInListButton", "hideAddressPicker", "onPostcodeChanged", "Lio/reactivex/Observable;", "", "onViewedAddresses", "showAddressNotInListButton", "showAddressPicker", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.ui.f {
      io.reactivex.n a();

      io.reactivex.n b();

      void c();

      void d();

      void e();

      void f();

      void setPrimaryButtonEnabled(boolean var1);

      void setPrimaryButtonVisible(boolean var1);
   }
}
