package co.uk.getmondo.profile.address;

import co.uk.getmondo.api.model.Address;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.d.s;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u000bB\u0019\b\u0007\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J\u0010\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0002H\u0016R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\f"},
   d2 = {"Lco/uk/getmondo/profile/address/EnterAddressPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/profile/address/EnterAddressPresenter$View;", "address", "Lco/uk/getmondo/api/model/Address;", "analytics", "Lco/uk/getmondo/common/AnalyticsService;", "(Lco/uk/getmondo/api/model/Address;Lco/uk/getmondo/common/AnalyticsService;)V", "register", "", "view", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class g extends co.uk.getmondo.common.ui.b {
   private final Address c;
   private final co.uk.getmondo.common.a d;

   public g(Address var1, co.uk.getmondo.common.a var2) {
      kotlin.d.b.l.b(var2, "analytics");
      super();
      this.c = var1;
      this.d = var2;
   }

   public void a(final g.a var1) {
      kotlin.d.b.l.b(var1, "view");
      super.a((co.uk.getmondo.common.ui.f)var1);
      this.d.a(Impression.Companion.aP());
      var1.a(this.c);
      io.reactivex.b.a var4 = this.b;
      io.reactivex.n var6 = var1.a();
      io.reactivex.c.g var5 = (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Boolean var1x) {
            g.a var2 = var1;
            kotlin.d.b.l.a(var1x, "it");
            var2.a(var1x.booleanValue());
         }
      });
      kotlin.d.a.b var3 = (kotlin.d.a.b)null.a;
      Object var2 = var3;
      if(var3 != null) {
         var2 = new h(var3);
      }

      io.reactivex.b.b var9 = var6.subscribe(var5, (io.reactivex.c.g)var2);
      kotlin.d.b.l.a(var9, "view.onAddressInputValid…Enabled(it) }, Timber::e)");
      this.b = co.uk.getmondo.common.j.f.a(var4, var9);
      io.reactivex.b.a var10 = this.b;
      io.reactivex.n var12 = var1.b();
      var5 = (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(s var1x) {
            g.a var2 = var1;
            kotlin.d.b.l.a(var1x, "it");
            var2.a(var1x);
         }
      });
      kotlin.d.a.b var11 = (kotlin.d.a.b)null.a;
      Object var7 = var11;
      if(var11 != null) {
         var7 = new h(var11);
      }

      io.reactivex.b.b var8 = var12.subscribe(var5, (io.reactivex.c.g)var7);
      kotlin.d.b.l.a(var8, "view.onConfirmAddress()\n…inEntry(it) }, Timber::e)");
      this.b = co.uk.getmondo.common.j.f.a(var10, var8);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bf\u0018\u00002\u00020\u0001J\u000e\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H&J\u000e\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00060\u0003H&J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u0006H&J\u0012\u0010\n\u001a\u00020\b2\b\u0010\t\u001a\u0004\u0018\u00010\u000bH&J\u0010\u0010\f\u001a\u00020\b2\u0006\u0010\r\u001a\u00020\u0004H&¨\u0006\u000e"},
      d2 = {"Lco/uk/getmondo/profile/address/EnterAddressPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "onAddressInputValid", "Lio/reactivex/Observable;", "", "onConfirmAddress", "Lco/uk/getmondo/model/LegacyAddress;", "openPinEntry", "", "address", "setAddress", "Lco/uk/getmondo/api/model/Address;", "setConfirmEnabled", "enabled", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.ui.f {
      io.reactivex.n a();

      void a(Address var1);

      void a(s var1);

      void a(boolean var1);

      io.reactivex.n b();
   }
}
