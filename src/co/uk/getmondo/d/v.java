package co.uk.getmondo.d;

import io.realm.bb;
import org.threeten.bp.YearMonth;

public class v implements io.realm.aj, bb {
   private String amountCurrency;
   private long amountValue;
   private String id;
   private String month;

   public v() {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

   }

   public v(String var1, long var2, String var4, String var5) {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

      this.a(var1);
      this.a(var2);
      this.b(var4);
      this.c(var5);
   }

   public c a() {
      return new c(this.d(), this.e());
   }

   public void a(long var1) {
      this.amountValue = var1;
   }

   public void a(String var1) {
      this.id = var1;
   }

   public YearMonth b() {
      return YearMonth.a((CharSequence)this.f());
   }

   public void b(String var1) {
      this.amountCurrency = var1;
   }

   public String c() {
      return this.id;
   }

   public void c(String var1) {
      this.month = var1;
   }

   public long d() {
      return this.amountValue;
   }

   public String e() {
      return this.amountCurrency;
   }

   public boolean equals(Object var1) {
      boolean var4 = true;
      boolean var3 = false;
      boolean var2;
      if(this == var1) {
         var2 = true;
      } else {
         var2 = var3;
         if(var1 != null) {
            var2 = var3;
            if(this.getClass() == var1.getClass()) {
               v var5 = (v)var1;
               var2 = var3;
               if(this.d() == var5.d()) {
                  if(this.c() != null) {
                     var2 = var3;
                     if(!this.c().equals(var5.c())) {
                        return var2;
                     }
                  } else if(var5.c() != null) {
                     var2 = var3;
                     return var2;
                  }

                  if(this.e() != null) {
                     var2 = var3;
                     if(!this.e().equals(var5.e())) {
                        return var2;
                     }
                  } else if(var5.e() != null) {
                     var2 = var3;
                     return var2;
                  }

                  if(this.f() != null) {
                     var2 = this.f().equals(var5.f());
                  } else {
                     var2 = var4;
                     if(var5.f() != null) {
                        var2 = false;
                     }
                  }
               }
            }
         }
      }

      return var2;
   }

   public String f() {
      return this.month;
   }

   public int hashCode() {
      int var3 = 0;
      int var1;
      if(this.c() != null) {
         var1 = this.c().hashCode();
      } else {
         var1 = 0;
      }

      int var4 = (int)(this.d() ^ this.d() >>> 32);
      int var2;
      if(this.e() != null) {
         var2 = this.e().hashCode();
      } else {
         var2 = 0;
      }

      if(this.f() != null) {
         var3 = this.f().hashCode();
      }

      return (var2 + (var1 * 31 + var4) * 31) * 31 + var3;
   }
}
