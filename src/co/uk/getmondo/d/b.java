package co.uk.getmondo.d;

import io.realm.az;
import io.realm.bb;
import java.util.Iterator;
import java.util.List;

public class b implements io.realm.b, bb {
   private String accountId;
   private long balanceAmountValue;
   private String balanceCurrency;
   private az localSpend;
   private long spentTodayAmountValue;
   private String spentTodayCurrency;

   public b() {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

   }

   public b(String var1, long var2, String var4, long var5, String var7, az var8) {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

      this.a(var1);
      this.a(var2);
      this.b(var4);
      this.b(var5);
      this.c(var7);
      this.a(var8);
   }

   public c a() {
      return new c(this.f(), this.g());
   }

   public void a(long var1) {
      this.balanceAmountValue = var1;
   }

   public void a(az var1) {
      this.localSpend = var1;
   }

   public void a(String var1) {
      this.accountId = var1;
   }

   public c b() {
      return new c(this.h(), this.i());
   }

   public void b(long var1) {
      this.spentTodayAmountValue = var1;
   }

   public void b(String var1) {
      this.balanceCurrency = var1;
   }

   public List c() {
      return this.j();
   }

   public void c(String var1) {
      this.spentTodayCurrency = var1;
   }

   public boolean d() {
      Iterator var2 = this.j().iterator();

      boolean var1;
      while(true) {
         if(var2.hasNext()) {
            if(((t)var2.next()).a().l().b().equals(this.g())) {
               continue;
            }

            var1 = true;
            break;
         }

         var1 = false;
         break;
      }

      return var1;
   }

   public String e() {
      return this.accountId;
   }

   public boolean equals(Object var1) {
      boolean var4 = true;
      boolean var3 = false;
      boolean var2;
      if(this == var1) {
         var2 = true;
      } else {
         var2 = var3;
         if(var1 != null) {
            var2 = var3;
            if(this.getClass() == var1.getClass()) {
               b var5 = (b)var1;
               var2 = var3;
               if(this.f() == var5.f()) {
                  var2 = var3;
                  if(this.h() == var5.h()) {
                     if(this.e() != null) {
                        var2 = var3;
                        if(!this.e().equals(var5.e())) {
                           return var2;
                        }
                     } else if(var5.e() != null) {
                        var2 = var3;
                        return var2;
                     }

                     if(this.g() != null) {
                        var2 = var3;
                        if(!this.g().equals(var5.g())) {
                           return var2;
                        }
                     } else if(var5.g() != null) {
                        var2 = var3;
                        return var2;
                     }

                     if(this.i() != null) {
                        var2 = var3;
                        if(!this.i().equals(var5.i())) {
                           return var2;
                        }
                     } else if(var5.i() != null) {
                        var2 = var3;
                        return var2;
                     }

                     if(this.j() != null) {
                        var2 = this.j().equals(var5.j());
                     } else {
                        var2 = var4;
                        if(var5.j() != null) {
                           var2 = false;
                        }
                     }
                  }
               }
            }
         }
      }

      return var2;
   }

   public long f() {
      return this.balanceAmountValue;
   }

   public String g() {
      return this.balanceCurrency;
   }

   public long h() {
      return this.spentTodayAmountValue;
   }

   public int hashCode() {
      int var4 = 0;
      int var1;
      if(this.e() != null) {
         var1 = this.e().hashCode();
      } else {
         var1 = 0;
      }

      int var5 = (int)(this.f() ^ this.f() >>> 32);
      int var2;
      if(this.g() != null) {
         var2 = this.g().hashCode();
      } else {
         var2 = 0;
      }

      int var6 = (int)(this.h() ^ this.h() >>> 32);
      int var3;
      if(this.i() != null) {
         var3 = this.i().hashCode();
      } else {
         var3 = 0;
      }

      if(this.j() != null) {
         var4 = this.j().hashCode();
      }

      return (var3 + ((var2 + (var1 * 31 + var5) * 31) * 31 + var6) * 31) * 31 + var4;
   }

   public String i() {
      return this.spentTodayCurrency;
   }

   public az j() {
      return this.localSpend;
   }
}
