package co.uk.getmondo.d;

import io.realm.bb;

public class u implements io.realm.ah, bb {
   public static final String TFL_MERCHANT_NAME = "Transport for London";
   private boolean atm;
   private String category;
   private String emoji;
   private String formattedAddress;
   private String groupId;
   private String id;
   private Double latitude;
   private String logoUrl;
   private Double longitude;
   private String name;
   private boolean online;

   public u() {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

   }

   public u(String var1, String var2, String var3, String var4, String var5, String var6, boolean var7, boolean var8, String var9, Double var10, Double var11) {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

      this.a(var1);
      this.b(var2);
      this.c(var3);
      this.d(var4);
      this.e(var5);
      this.f(var6);
      this.a(var7);
      this.b(var8);
      this.g(var9);
      this.a(var10);
      this.b(var11);
   }

   public void a(Double var1) {
      this.latitude = var1;
   }

   public void a(String var1) {
      this.id = var1;
   }

   public void a(boolean var1) {
      this.online = var1;
   }

   boolean a() {
      return "Transport for London".equalsIgnoreCase(this.o());
   }

   public h b() {
      return h.a(this.r());
   }

   public void b(Double var1) {
      this.longitude = var1;
   }

   public void b(String var1) {
      this.groupId = var1;
   }

   public void b(boolean var1) {
      this.atm = var1;
   }

   public com.c.b.b c() {
      return com.c.b.b.b(this.u());
   }

   public void c(String var1) {
      this.name = var1;
   }

   public Double d() {
      return this.v();
   }

   public void d(String var1) {
      this.logoUrl = var1;
   }

   public Double e() {
      return this.w();
   }

   public void e(String var1) {
      this.emoji = var1;
   }

   public boolean equals(Object var1) {
      boolean var3 = true;
      boolean var4 = false;
      boolean var2;
      if(this == var1) {
         var2 = true;
      } else {
         var2 = var4;
         if(var1 != null) {
            var2 = var4;
            if(this.getClass() == var1.getClass()) {
               u var5 = (u)var1;
               var2 = var4;
               if(this.s() == var5.s()) {
                  var2 = var4;
                  if(this.t() == var5.t()) {
                     if(this.m() != null) {
                        var2 = var4;
                        if(!this.m().equals(var5.m())) {
                           return var2;
                        }
                     } else if(var5.m() != null) {
                        var2 = var4;
                        return var2;
                     }

                     if(this.n() != null) {
                        var2 = var4;
                        if(!this.n().equals(var5.n())) {
                           return var2;
                        }
                     } else if(var5.n() != null) {
                        var2 = var4;
                        return var2;
                     }

                     if(this.o() != null) {
                        var2 = var4;
                        if(!this.o().equals(var5.o())) {
                           return var2;
                        }
                     } else if(var5.o() != null) {
                        var2 = var4;
                        return var2;
                     }

                     if(this.p() != null) {
                        var2 = var4;
                        if(!this.p().equals(var5.p())) {
                           return var2;
                        }
                     } else if(var5.p() != null) {
                        var2 = var4;
                        return var2;
                     }

                     if(this.q() != null) {
                        var2 = var4;
                        if(!this.q().equals(var5.q())) {
                           return var2;
                        }
                     } else if(var5.q() != null) {
                        var2 = var4;
                        return var2;
                     }

                     if(this.r() != null) {
                        var2 = var4;
                        if(!this.r().equals(var5.r())) {
                           return var2;
                        }
                     } else if(var5.r() != null) {
                        var2 = var4;
                        return var2;
                     }

                     if(this.u() != null) {
                        var2 = var4;
                        if(!this.u().equals(var5.u())) {
                           return var2;
                        }
                     } else if(var5.u() != null) {
                        var2 = var4;
                        return var2;
                     }

                     if(this.v() != null) {
                        var2 = var4;
                        if(!this.v().equals(var5.v())) {
                           return var2;
                        }
                     } else if(var5.v() != null) {
                        var2 = var4;
                        return var2;
                     }

                     if(this.w() != null) {
                        var2 = this.w().equals(var5.w());
                     } else {
                        var2 = var3;
                        if(var5.w() != null) {
                           var2 = false;
                        }
                     }
                  }
               }
            }
         }
      }

      return var2;
   }

   public String f() {
      String var1;
      if(co.uk.getmondo.common.k.p.d(this.q())) {
         var1 = this.o();
      } else {
         var1 = this.q() + " " + this.o();
      }

      return var1;
   }

   public void f(String var1) {
      this.category = var1;
   }

   public void g(String var1) {
      this.formattedAddress = var1;
   }

   public boolean g() {
      return this.t();
   }

   public String h() {
      return this.n();
   }

   public int hashCode() {
      byte var8 = 1;
      int var11 = 0;
      int var1;
      if(this.m() != null) {
         var1 = this.m().hashCode();
      } else {
         var1 = 0;
      }

      int var2;
      if(this.n() != null) {
         var2 = this.n().hashCode();
      } else {
         var2 = 0;
      }

      int var3;
      if(this.o() != null) {
         var3 = this.o().hashCode();
      } else {
         var3 = 0;
      }

      int var4;
      if(this.p() != null) {
         var4 = this.p().hashCode();
      } else {
         var4 = 0;
      }

      int var5;
      if(this.q() != null) {
         var5 = this.q().hashCode();
      } else {
         var5 = 0;
      }

      int var6;
      if(this.r() != null) {
         var6 = this.r().hashCode();
      } else {
         var6 = 0;
      }

      byte var7;
      if(this.s()) {
         var7 = 1;
      } else {
         var7 = 0;
      }

      if(!this.t()) {
         var8 = 0;
      }

      int var9;
      if(this.u() != null) {
         var9 = this.u().hashCode();
      } else {
         var9 = 0;
      }

      int var10;
      if(this.v() != null) {
         var10 = this.v().hashCode();
      } else {
         var10 = 0;
      }

      if(this.w() != null) {
         var11 = this.w().hashCode();
      }

      return (var10 + (var9 + ((var7 + (var6 + (var5 + (var4 + (var3 + (var2 + var1 * 31) * 31) * 31) * 31) * 31) * 31) * 31 + var8) * 31) * 31) * 31 + var11;
   }

   public String i() {
      return this.o();
   }

   public String j() {
      return this.p();
   }

   public boolean k() {
      return this.s();
   }

   public String l() {
      return this.q();
   }

   public String m() {
      return this.id;
   }

   public String n() {
      return this.groupId;
   }

   public String o() {
      return this.name;
   }

   public String p() {
      return this.logoUrl;
   }

   public String q() {
      return this.emoji;
   }

   public String r() {
      return this.category;
   }

   public boolean s() {
      return this.online;
   }

   public boolean t() {
      return this.atm;
   }

   public String u() {
      return this.formattedAddress;
   }

   public Double v() {
      return this.latitude;
   }

   public Double w() {
      return this.longitude;
   }
}
