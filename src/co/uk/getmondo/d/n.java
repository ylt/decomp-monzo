package co.uk.getmondo.d;

import io.realm.bb;
import java.util.Date;

public class n implements bb, io.realm.v {
   private String id;
   private Date lastFetched;

   public n() {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

      this.a("feed_metadata_id");
   }

   public n(Date var1) {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

      this.a("feed_metadata_id");
      this.a(var1);
   }

   public String a() {
      return this.id;
   }

   public void a(String var1) {
      this.id = var1;
   }

   public void a(Date var1) {
      this.lastFetched = var1;
   }

   public Date b() {
      return this.lastFetched;
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(this != var1) {
         if(var1 != null && this.getClass() == var1.getClass()) {
            n var3;
            label35: {
               var3 = (n)var1;
               if(this.a() != null) {
                  if(this.a().equals(var3.a())) {
                     break label35;
                  }
               } else if(var3.a() == null) {
                  break label35;
               }

               var2 = false;
               return var2;
            }

            if(this.b() != null) {
               var2 = this.b().equals(var3.b());
            } else if(var3.b() != null) {
               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public int hashCode() {
      int var2 = 0;
      int var1;
      if(this.a() != null) {
         var1 = this.a().hashCode();
      } else {
         var1 = 0;
      }

      if(this.b() != null) {
         var2 = this.b().hashCode();
      }

      return var1 * 31 + var2;
   }
}
