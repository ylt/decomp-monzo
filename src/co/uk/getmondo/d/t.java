package co.uk.getmondo.d;

import io.realm.bb;

public class t implements io.realm.af, bb {
   private long amountValue;
   private String currency;

   public t() {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

   }

   public t(String var1, long var2) {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

      this.a(var1);
      this.a(var2);
   }

   public c a() {
      return new c(this.c(), this.b());
   }

   public void a(long var1) {
      this.amountValue = var1;
   }

   public void a(String var1) {
      this.currency = var1;
   }

   public String b() {
      return this.currency;
   }

   public long c() {
      return this.amountValue;
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(this != var1) {
         if(var1 != null && this.getClass() == var1.getClass()) {
            t var3 = (t)var1;
            if(this.c() != var3.c()) {
               var2 = false;
            } else if(this.b() != null) {
               var2 = this.b().equals(var3.b());
            } else if(var3.b() != null) {
               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public int hashCode() {
      int var1;
      if(this.b() != null) {
         var1 = this.b().hashCode();
      } else {
         var1 = 0;
      }

      return var1 * 31 + (int)(this.c() ^ this.c() >>> 32);
   }
}
