package co.uk.getmondo.d;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import io.realm.at;
import io.realm.bb;

public class aa implements Parcelable, co.uk.getmondo.payments.send.data.a.e, at, bb {
   public static final Creator CREATOR = new Creator() {
      public aa a(Parcel var1) {
         return new aa(var1);
      }

      public aa[] a(int var1) {
         return new aa[var1];
      }

      // $FF: synthetic method
      public Object createFromParcel(Parcel var1) {
         return this.a(var1);
      }

      // $FF: synthetic method
      public Object[] newArray(int var1) {
         return this.a(var1);
      }
   };
   private String contactName;
   private String contactPhoto;
   private boolean isEnriched;
   private String peerName;
   private String phoneNumber;
   private String userId;
   private String username;

   public aa() {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

   }

   protected aa(Parcel var1) {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

      this.c(var1.readString());
      this.d(var1.readString());
      this.e(var1.readString());
      this.f(var1.readString());
      this.g(var1.readString());
      this.h(var1.readString());
      boolean var2;
      if(var1.readByte() != 0) {
         var2 = true;
      } else {
         var2 = false;
      }

      this.b(var2);
   }

   public aa(String var1, String var2, String var3, String var4, String var5, String var6, boolean var7) {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

      this.c(var1);
      this.d(var2);
      this.e(var3);
      this.f(var4);
      this.g(var5);
      this.h(var6);
      this.b(var7);
   }

   public aa a(co.uk.getmondo.payments.send.a.b var1) {
      String var4 = null;
      String var3;
      if(var1 != null) {
         var3 = var1.b();
      } else {
         var3 = null;
      }

      if(var1 != null) {
         var4 = var1.e();
      }

      d.a.a.a("Enriching peer %s(phone: %s) | Contact name: %s photo: %s", new Object[]{this.a(), this.d(), var3, var4});
      this.a(var3);
      this.b(var4);
      boolean var2;
      if(var1 != null) {
         var2 = true;
      } else {
         var2 = false;
      }

      this.a(var2);
      return this;
   }

   public String a() {
      return this.j();
   }

   public void a(String var1) {
      if((var1 != null || this.n() != null) && (this.n() == null || !this.n().equals(var1))) {
         this.g(var1);
      }

   }

   public void a(boolean var1) {
      this.b(var1);
   }

   public String b() {
      String var1;
      if(this.n() != null) {
         var1 = this.n();
      } else if(this.k() != null) {
         var1 = this.k();
      } else if(this.l() != null) {
         var1 = this.l();
      } else {
         var1 = "";
      }

      return var1;
   }

   public void b(String var1) {
      if((var1 != null || this.o() != null) && (this.o() == null || !this.o().equals(var1))) {
         this.h(var1);
      }

   }

   public void b(boolean var1) {
      this.isEnriched = var1;
   }

   public String c() {
      String var1 = this.b();
      if(co.uk.getmondo.common.k.p.d(var1)) {
         var1 = this.l();
      } else {
         var1 = var1.split(" ")[0];
      }

      return var1;
   }

   public void c(String var1) {
      this.userId = var1;
   }

   public String d() {
      return this.l();
   }

   public void d(String var1) {
      this.peerName = var1;
   }

   public int describeContents() {
      return 0;
   }

   public String e() {
      return this.m();
   }

   public void e(String var1) {
      this.phoneNumber = var1;
   }

   public boolean equals(Object var1) {
      boolean var3 = true;
      boolean var4 = false;
      boolean var2;
      if(this == var1) {
         var2 = true;
      } else {
         var2 = var4;
         if(var1 != null) {
            var2 = var4;
            if(this.getClass() == var1.getClass()) {
               aa var5 = (aa)var1;
               var2 = var4;
               if(this.p() == var5.p()) {
                  if(this.j() != null) {
                     var2 = var4;
                     if(!this.j().equals(var5.j())) {
                        return var2;
                     }
                  } else if(var5.j() != null) {
                     var2 = var4;
                     return var2;
                  }

                  if(this.k() != null) {
                     var2 = var4;
                     if(!this.k().equals(var5.k())) {
                        return var2;
                     }
                  } else if(var5.k() != null) {
                     var2 = var4;
                     return var2;
                  }

                  if(this.l() != null) {
                     var2 = var4;
                     if(!this.l().equals(var5.l())) {
                        return var2;
                     }
                  } else if(var5.l() != null) {
                     var2 = var4;
                     return var2;
                  }

                  if(this.m() != null) {
                     var2 = var4;
                     if(!this.m().equals(var5.m())) {
                        return var2;
                     }
                  } else if(var5.m() != null) {
                     var2 = var4;
                     return var2;
                  }

                  if(this.n() != null) {
                     var2 = var4;
                     if(!this.n().equals(var5.n())) {
                        return var2;
                     }
                  } else if(var5.n() != null) {
                     var2 = var4;
                     return var2;
                  }

                  if(this.o() != null) {
                     var2 = this.o().equals(var5.o());
                  } else {
                     var2 = var3;
                     if(var5.o() != null) {
                        var2 = false;
                     }
                  }
               }
            }
         }
      }

      return var2;
   }

   public void f(String var1) {
      this.username = var1;
   }

   public boolean f() {
      boolean var1;
      if(this.o() != null && !this.o().isEmpty()) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public String g() {
      return this.o();
   }

   public void g(String var1) {
      this.contactName = var1;
   }

   public void h(String var1) {
      this.contactPhoto = var1;
   }

   public boolean h() {
      boolean var1;
      if(this.n() != null) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public int hashCode() {
      byte var7 = 0;
      int var1;
      if(this.j() != null) {
         var1 = this.j().hashCode();
      } else {
         var1 = 0;
      }

      int var2;
      if(this.k() != null) {
         var2 = this.k().hashCode();
      } else {
         var2 = 0;
      }

      int var3;
      if(this.l() != null) {
         var3 = this.l().hashCode();
      } else {
         var3 = 0;
      }

      int var4;
      if(this.m() != null) {
         var4 = this.m().hashCode();
      } else {
         var4 = 0;
      }

      int var5;
      if(this.n() != null) {
         var5 = this.n().hashCode();
      } else {
         var5 = 0;
      }

      int var6;
      if(this.o() != null) {
         var6 = this.o().hashCode();
      } else {
         var6 = 0;
      }

      if(this.p()) {
         var7 = 1;
      }

      return (var6 + (var5 + (var4 + (var3 + (var2 + var1 * 31) * 31) * 31) * 31) * 31) * 31 + var7;
   }

   public boolean i() {
      return co.uk.getmondo.common.k.p.c(this.l());
   }

   public String j() {
      return this.userId;
   }

   public String k() {
      return this.peerName;
   }

   public String l() {
      return this.phoneNumber;
   }

   public String m() {
      return this.username;
   }

   public String n() {
      return this.contactName;
   }

   public String o() {
      return this.contactPhoto;
   }

   public boolean p() {
      return this.isEnriched;
   }

   public void writeToParcel(Parcel var1, int var2) {
      var1.writeString(this.j());
      var1.writeString(this.k());
      var1.writeString(this.l());
      var1.writeString(this.m());
      var1.writeString(this.n());
      var1.writeString(this.o());
      byte var3;
      if(this.p()) {
         var3 = 1;
      } else {
         var3 = 0;
      }

      var1.writeByte((byte)var3);
   }
}
