package co.uk.getmondo.d;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import io.realm.bb;

public class r implements Parcelable, io.realm.ad, bb {
   public static final Creator CREATOR = new Creator() {
      public r a(Parcel var1) {
         return new r(var1);
      }

      public r[] a(int var1) {
         return new r[var1];
      }

      // $FF: synthetic method
      public Object createFromParcel(Parcel var1) {
         return this.a(var1);
      }

      // $FF: synthetic method
      public Object[] newArray(int var1) {
         return this.a(var1);
      }
   };
   private String customerRejectedMessage;
   private String id;
   private String rejectedReason;

   public r() {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

   }

   protected r(Parcel var1) {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

      this.a(var1.readString());
      this.b(var1.readString());
      this.c(var1.readString());
   }

   public r(String var1, String var2, String var3) {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

      this.a(var1);
      this.b(var2);
      this.c(var3);
   }

   public co.uk.getmondo.signup.identity_verification.y a() {
      return co.uk.getmondo.signup.identity_verification.y.a(this.d());
   }

   public void a(String var1) {
      this.id = var1;
   }

   public String b() {
      return this.e();
   }

   public void b(String var1) {
      this.rejectedReason = var1;
   }

   public String c() {
      return this.id;
   }

   public void c(String var1) {
      this.customerRejectedMessage = var1;
   }

   public String d() {
      return this.rejectedReason;
   }

   public int describeContents() {
      return 0;
   }

   public String e() {
      return this.customerRejectedMessage;
   }

   public boolean equals(Object var1) {
      boolean var4 = true;
      boolean var3 = false;
      boolean var2;
      if(this == var1) {
         var2 = true;
      } else {
         var2 = var3;
         if(var1 != null) {
            var2 = var3;
            if(this.getClass() == var1.getClass()) {
               r var5 = (r)var1;
               if(this.c() != null) {
                  var2 = var3;
                  if(!this.c().equals(var5.c())) {
                     return var2;
                  }
               } else if(var5.c() != null) {
                  var2 = var3;
                  return var2;
               }

               if(this.d() != null) {
                  var2 = var3;
                  if(!this.d().equals(var5.d())) {
                     return var2;
                  }
               } else if(var5.d() != null) {
                  var2 = var3;
                  return var2;
               }

               if(this.e() != null) {
                  var2 = this.e().equals(var5.e());
               } else {
                  var2 = var4;
                  if(var5.e() != null) {
                     var2 = false;
                  }
               }
            }
         }
      }

      return var2;
   }

   public int hashCode() {
      int var3 = 0;
      int var1;
      if(this.c() != null) {
         var1 = this.c().hashCode();
      } else {
         var1 = 0;
      }

      int var2;
      if(this.d() != null) {
         var2 = this.d().hashCode();
      } else {
         var2 = 0;
      }

      if(this.e() != null) {
         var3 = this.e().hashCode();
      }

      return (var2 + var1 * 31) * 31 + var3;
   }

   public void writeToParcel(Parcel var1, int var2) {
      var1.writeString(this.c());
      var1.writeString(this.d());
      var1.writeString(this.e());
   }
}
