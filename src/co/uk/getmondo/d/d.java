package co.uk.getmondo.d;

import io.realm.bb;
import java.util.Date;

public class d implements bb, io.realm.d {
   private Date created;
   private String externalId;
   private String id;
   private String url;

   public d() {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

   }

   public d(String var1, String var2, Date var3, String var4) {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

      this.a(var1);
      this.b(var2);
      this.a(var3);
      this.c(var4);
   }

   public String a() {
      return this.c();
   }

   public void a(String var1) {
      this.id = var1;
   }

   public void a(Date var1) {
      this.created = var1;
   }

   public String b() {
      return this.f();
   }

   public void b(String var1) {
      this.externalId = var1;
   }

   public String c() {
      return this.id;
   }

   public void c(String var1) {
      this.url = var1;
   }

   public String d() {
      return this.externalId;
   }

   public Date e() {
      return this.created;
   }

   public boolean equals(Object var1) {
      boolean var3 = true;
      boolean var4 = false;
      boolean var2;
      if(this == var1) {
         var2 = true;
      } else {
         var2 = var4;
         if(var1 != null) {
            var2 = var4;
            if(this.getClass() == var1.getClass()) {
               d var5 = (d)var1;
               if(this.c() != null) {
                  var2 = var4;
                  if(!this.c().equals(var5.c())) {
                     return var2;
                  }
               } else if(var5.c() != null) {
                  var2 = var4;
                  return var2;
               }

               if(this.d() != null) {
                  var2 = var4;
                  if(!this.d().equals(var5.d())) {
                     return var2;
                  }
               } else if(var5.d() != null) {
                  var2 = var4;
                  return var2;
               }

               if(this.e() != null) {
                  var2 = var4;
                  if(!this.e().equals(var5.e())) {
                     return var2;
                  }
               } else if(var5.e() != null) {
                  var2 = var4;
                  return var2;
               }

               if(this.f() != null) {
                  var2 = this.f().equals(var5.f());
               } else {
                  var2 = var3;
                  if(var5.f() != null) {
                     var2 = false;
                  }
               }
            }
         }
      }

      return var2;
   }

   public String f() {
      return this.url;
   }

   public int hashCode() {
      int var4 = 0;
      int var1;
      if(this.c() != null) {
         var1 = this.c().hashCode();
      } else {
         var1 = 0;
      }

      int var2;
      if(this.d() != null) {
         var2 = this.d().hashCode();
      } else {
         var2 = 0;
      }

      int var3;
      if(this.e() != null) {
         var3 = this.e().hashCode();
      } else {
         var3 = 0;
      }

      if(this.f() != null) {
         var4 = this.f().hashCode();
      }

      return (var3 + (var2 + var1 * 31) * 31) * 31 + var4;
   }
}
