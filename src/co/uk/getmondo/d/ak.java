package co.uk.getmondo.d;

import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0086\b\u0018\u00002\u00020\u0001:\u0001.BA\b\u0007\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\t\u0012\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\u000bJ\u000b\u0010\u001c\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u001d\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\u000b\u0010\u001e\u001a\u0004\u0018\u00010\u0007HÆ\u0003J\u000b\u0010\u001f\u001a\u0004\u0018\u00010\tHÆ\u0003J\u000b\u0010 \u001a\u0004\u0018\u00010\u0003HÆ\u0003JE\u0010!\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00072\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\t2\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u0003HÆ\u0001J\u0013\u0010\"\u001a\u00020\u000f2\b\u0010#\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010$\u001a\u00020%HÖ\u0001J\t\u0010&\u001a\u00020\u0003HÖ\u0001J\u000e\u0010'\u001a\u00020\u00002\u0006\u0010\u0004\u001a\u00020\u0005J\u000e\u0010(\u001a\u00020\u00002\u0006\u0010\u0006\u001a\u00020\u0007J\u000e\u0010)\u001a\u00020\u00002\u0006\u0010*\u001a\u00020+J\u0018\u0010,\u001a\u00020\u00002\u0006\u0010\u0006\u001a\u00020\u00072\b\u0010\b\u001a\u0004\u0018\u00010\tJ\u0010\u0010-\u001a\u00020\u00002\b\u0010\b\u001a\u0004\u0018\u00010\tR\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0014\u0010\u000e\u001a\u00020\u000f8BX\u0082\u0004¢\u0006\u0006\u001a\u0004\b\u000e\u0010\u0010R\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u0013\u0010\n\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u0011\u0010\u0015\u001a\u00020\u00168F¢\u0006\u0006\u001a\u0004\b\u0017\u0010\u0018R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u0014R\u0013\u0010\b\u001a\u0004\u0018\u00010\t¢\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u001b¨\u0006/"},
   d2 = {"Lco/uk/getmondo/model/User;", "", "userId", "", "account", "Lco/uk/getmondo/model/Account;", "profile", "Lco/uk/getmondo/model/Profile;", "waitlistProfile", "Lco/uk/getmondo/model/WaitlistProfile;", "secondaryAccountId", "(Ljava/lang/String;Lco/uk/getmondo/model/Account;Lco/uk/getmondo/model/Profile;Lco/uk/getmondo/model/WaitlistProfile;Ljava/lang/String;)V", "getAccount", "()Lco/uk/getmondo/model/Account;", "isGoogleTestUser", "", "()Z", "getProfile", "()Lco/uk/getmondo/model/Profile;", "getSecondaryAccountId", "()Ljava/lang/String;", "state", "Lco/uk/getmondo/model/User$State;", "getState", "()Lco/uk/getmondo/model/User$State;", "getUserId", "getWaitlistProfile", "()Lco/uk/getmondo/model/WaitlistProfile;", "component1", "component2", "component3", "component4", "component5", "copy", "equals", "other", "hashCode", "", "toString", "withAccount", "withProfile", "withProfileAndAccountInfo", "info", "Lco/uk/getmondo/common/accounts/ProfileAndAccountInfo;", "withProfileAndWaitlistProfile", "withWaitlistProfile", "State", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ak {
   private final a account;
   private final ac profile;
   private final String secondaryAccountId;
   private final String userId;
   private final an waitlistProfile;

   public ak(String var1) {
      this(var1, (a)null, (ac)null, (an)null, (String)null, 30, (kotlin.d.b.i)null);
   }

   public ak(String var1, a var2) {
      this(var1, var2, (ac)null, (an)null, (String)null, 28, (kotlin.d.b.i)null);
   }

   public ak(String var1, a var2, ac var3) {
      this(var1, var2, var3, (an)null, (String)null, 24, (kotlin.d.b.i)null);
   }

   public ak(String var1, a var2, ac var3, an var4) {
      this(var1, var2, var3, var4, (String)null, 16, (kotlin.d.b.i)null);
   }

   public ak(String var1, a var2, ac var3, an var4, String var5) {
      this.userId = var1;
      this.account = var2;
      this.profile = var3;
      this.waitlistProfile = var4;
      this.secondaryAccountId = var5;
   }

   // $FF: synthetic method
   public ak(String var1, a var2, ac var3, an var4, String var5, int var6, kotlin.d.b.i var7) {
      if((var6 & 2) != 0) {
         var2 = (a)null;
      }

      if((var6 & 4) != 0) {
         var3 = (ac)null;
      }

      if((var6 & 8) != 0) {
         var4 = (an)null;
      }

      if((var6 & 16) != 0) {
         var5 = (String)null;
      }

      this(var1, var2, var3, var4, var5);
   }

   private final boolean g() {
      boolean var1;
      if(this.profile != null && kotlin.d.b.l.a(this.profile.d(), "apmonzotest@gmail.com")) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public final ak.a a() {
      Boolean var2 = co.uk.getmondo.a.c;
      kotlin.d.b.l.a(var2, "BuildConfig.BANK");
      ak.a var3;
      if(var2.booleanValue()) {
         if(this.profile == null) {
            var3 = ak.a.NO_PROFILE;
         } else if(this.account == null) {
            var3 = ak.a.ACCOUNT_CREATION;
         } else if(!this.account.c()) {
            var3 = ak.a.NO_CARD;
         } else {
            var3 = ak.a.HAS_ACCOUNT;
         }
      } else if(this.account instanceof ab && !((ab)this.account).h() && !this.g()) {
         var3 = ak.a.INITIAL_TOPUP_REQUIRED;
      } else {
         a var4 = this.account;
         if(var4 != null && !var4.c()) {
            var3 = ak.a.NO_CARD;
         } else if(this.account != null) {
            var3 = ak.a.HAS_ACCOUNT;
         } else {
            an var5 = this.waitlistProfile;
            boolean var1;
            if(var5 != null) {
               var1 = var5.a();
            } else {
               var1 = false;
            }

            if(var1) {
               var3 = ak.a.INELIGIBLE;
            } else {
               var5 = this.waitlistProfile;
               if(var5 != null) {
                  var1 = var5.f();
               } else {
                  var1 = false;
               }

               if(var1) {
                  var3 = ak.a.ACCOUNT_CREATION;
               } else if(this.profile != null) {
                  var3 = ak.a.ON_WAITLIST;
               } else {
                  var3 = ak.a.NO_PROFILE;
               }
            }
         }
      }

      return var3;
   }

   public final ak a(co.uk.getmondo.common.accounts.l var1) {
      kotlin.d.b.l.b(var1, "info");
      ak var4;
      if(var1.a() == null) {
         ac var2 = var1.a();
         var4 = a(this, (String)null, (a)var1.a(), var2, (an)null, var1.c(), 9, (Object)null);
      } else {
         String var5 = var1.a().b();
         ac var3 = var1.a();
         var4 = a(this, var5, var1.b(), var3, (an)null, var1.c(), 8, (Object)null);
      }

      return var4;
   }

   public final ak a(a var1) {
      kotlin.d.b.l.b(var1, "account");
      return a(this, (String)null, var1, (ac)null, (an)null, (String)null, 29, (Object)null);
   }

   public final ak a(ac var1) {
      kotlin.d.b.l.b(var1, "profile");
      return a(this, var1.b(), (a)null, var1, (an)null, (String)null, 26, (Object)null);
   }

   public final ak a(ac var1, an var2) {
      kotlin.d.b.l.b(var1, "profile");
      return a(this, var1.b(), (a)null, var1, var2, (String)null, 18, (Object)null);
   }

   public final ak a(an var1) {
      return a(this, (String)null, (a)null, (ac)null, var1, (String)null, 23, (Object)null);
   }

   public final ak a(String var1, a var2, ac var3, an var4, String var5) {
      return new ak(var1, var2, var3, var4, var5);
   }

   public final String b() {
      return this.userId;
   }

   public final a c() {
      return this.account;
   }

   public final ac d() {
      return this.profile;
   }

   public final an e() {
      return this.waitlistProfile;
   }

   public boolean equals(Object var1) {
      boolean var2;
      if(this != var1) {
         label34: {
            if(var1 instanceof ak) {
               ak var3 = (ak)var1;
               if(kotlin.d.b.l.a(this.userId, var3.userId) && kotlin.d.b.l.a(this.account, var3.account) && kotlin.d.b.l.a(this.profile, var3.profile) && kotlin.d.b.l.a(this.waitlistProfile, var3.waitlistProfile) && kotlin.d.b.l.a(this.secondaryAccountId, var3.secondaryAccountId)) {
                  break label34;
               }
            }

            var2 = false;
            return var2;
         }
      }

      var2 = true;
      return var2;
   }

   public final String f() {
      return this.secondaryAccountId;
   }

   public int hashCode() {
      int var5 = 0;
      String var6 = this.userId;
      int var1;
      if(var6 != null) {
         var1 = var6.hashCode();
      } else {
         var1 = 0;
      }

      a var7 = this.account;
      int var2;
      if(var7 != null) {
         var2 = var7.hashCode();
      } else {
         var2 = 0;
      }

      ac var8 = this.profile;
      int var3;
      if(var8 != null) {
         var3 = var8.hashCode();
      } else {
         var3 = 0;
      }

      an var9 = this.waitlistProfile;
      int var4;
      if(var9 != null) {
         var4 = var9.hashCode();
      } else {
         var4 = 0;
      }

      var6 = this.secondaryAccountId;
      if(var6 != null) {
         var5 = var6.hashCode();
      }

      return (var4 + (var3 + (var2 + var1 * 31) * 31) * 31) * 31 + var5;
   }

   public String toString() {
      return "User(userId=" + this.userId + ", account=" + this.account + ", profile=" + this.profile + ", waitlistProfile=" + this.waitlistProfile + ", secondaryAccountId=" + this.secondaryAccountId + ")";
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\t\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007j\u0002\b\bj\u0002\b\t¨\u0006\n"},
      d2 = {"Lco/uk/getmondo/model/User$State;", "", "(Ljava/lang/String;I)V", "NO_PROFILE", "ON_WAITLIST", "INELIGIBLE", "ACCOUNT_CREATION", "INITIAL_TOPUP_REQUIRED", "NO_CARD", "HAS_ACCOUNT", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static enum a {
      ACCOUNT_CREATION,
      HAS_ACCOUNT,
      INELIGIBLE,
      INITIAL_TOPUP_REQUIRED,
      NO_CARD,
      NO_PROFILE,
      ON_WAITLIST;

      static {
         ak.a var2 = new ak.a("NO_PROFILE", 0);
         NO_PROFILE = var2;
         ak.a var5 = new ak.a("ON_WAITLIST", 1);
         ON_WAITLIST = var5;
         ak.a var1 = new ak.a("INELIGIBLE", 2);
         INELIGIBLE = var1;
         ak.a var6 = new ak.a("ACCOUNT_CREATION", 3);
         ACCOUNT_CREATION = var6;
         ak.a var3 = new ak.a("INITIAL_TOPUP_REQUIRED", 4);
         INITIAL_TOPUP_REQUIRED = var3;
         ak.a var4 = new ak.a("NO_CARD", 5);
         NO_CARD = var4;
         ak.a var0 = new ak.a("HAS_ACCOUNT", 6);
         HAS_ACCOUNT = var0;
      }
   }
}
