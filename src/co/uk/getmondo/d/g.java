package co.uk.getmondo.d;

import io.realm.bb;

public class g implements bb, io.realm.k {
   private String accountId;
   private String cardStatus;
   private String expires;
   private String id;
   private String lastDigits;
   private String processorToken;
   private boolean replacementOrdered;

   public g() {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

   }

   public g(String var1, String var2, String var3, String var4, String var5, String var6, boolean var7) {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

      this.a(var1);
      this.b(var2);
      this.c(var3);
      this.d(var4);
      this.e(var5);
      this.f(var6);
      this.a(var7);
   }

   public String a() {
      return this.i();
   }

   public void a(String var1) {
      this.id = var1;
   }

   public void a(boolean var1) {
      this.replacementOrdered = var1;
   }

   public String b() {
      return this.j();
   }

   public void b(String var1) {
      this.accountId = var1;
   }

   public String c() {
      return this.r_();
   }

   public void c(String var1) {
      this.expires = var1;
   }

   public String d() {
      return this.l();
   }

   public void d(String var1) {
      this.lastDigits = var1;
   }

   public String e() {
      return this.m();
   }

   public void e(String var1) {
      this.processorToken = var1;
   }

   public boolean equals(Object var1) {
      boolean var4 = true;
      boolean var3 = false;
      boolean var2;
      if(this == var1) {
         var2 = true;
      } else {
         var2 = var3;
         if(var1 != null) {
            var2 = var3;
            if(this.getClass() == var1.getClass()) {
               g var5 = (g)var1;
               var2 = var3;
               if(this.o() == var5.o()) {
                  if(this.i() != null) {
                     var2 = var3;
                     if(!this.i().equals(var5.i())) {
                        return var2;
                     }
                  } else if(var5.i() != null) {
                     var2 = var3;
                     return var2;
                  }

                  if(this.j() != null) {
                     var2 = var3;
                     if(!this.j().equals(var5.j())) {
                        return var2;
                     }
                  } else if(var5.j() != null) {
                     var2 = var3;
                     return var2;
                  }

                  if(this.r_() != null) {
                     var2 = var3;
                     if(!this.r_().equals(var5.r_())) {
                        return var2;
                     }
                  } else if(var5.r_() != null) {
                     var2 = var3;
                     return var2;
                  }

                  if(this.l() != null) {
                     var2 = var3;
                     if(!this.l().equals(var5.l())) {
                        return var2;
                     }
                  } else if(var5.l() != null) {
                     var2 = var3;
                     return var2;
                  }

                  if(this.m() != null) {
                     var2 = var3;
                     if(!this.m().equals(var5.m())) {
                        return var2;
                     }
                  } else if(var5.m() != null) {
                     var2 = var3;
                     return var2;
                  }

                  if(this.n() != null) {
                     var2 = this.n().equals(var5.n());
                  } else {
                     var2 = var4;
                     if(var5.n() != null) {
                        var2 = false;
                     }
                  }
               }
            }
         }
      }

      return var2;
   }

   public void f(String var1) {
      this.cardStatus = var1;
   }

   public boolean f() {
      return this.o();
   }

   public co.uk.getmondo.api.model.a g() {
      return co.uk.getmondo.api.model.a.valueOf(this.n());
   }

   public boolean h() {
      boolean var1;
      if(this.g() == co.uk.getmondo.api.model.a.ACTIVE) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public int hashCode() {
      byte var7 = 0;
      int var1;
      if(this.i() != null) {
         var1 = this.i().hashCode();
      } else {
         var1 = 0;
      }

      int var2;
      if(this.j() != null) {
         var2 = this.j().hashCode();
      } else {
         var2 = 0;
      }

      int var3;
      if(this.r_() != null) {
         var3 = this.r_().hashCode();
      } else {
         var3 = 0;
      }

      int var4;
      if(this.l() != null) {
         var4 = this.l().hashCode();
      } else {
         var4 = 0;
      }

      int var5;
      if(this.m() != null) {
         var5 = this.m().hashCode();
      } else {
         var5 = 0;
      }

      int var6;
      if(this.n() != null) {
         var6 = this.n().hashCode();
      } else {
         var6 = 0;
      }

      if(this.o()) {
         var7 = 1;
      }

      return (var6 + (var5 + (var4 + (var3 + (var2 + var1 * 31) * 31) * 31) * 31) * 31) * 31 + var7;
   }

   public String i() {
      return this.id;
   }

   public String j() {
      return this.accountId;
   }

   public String l() {
      return this.lastDigits;
   }

   public String m() {
      return this.processorToken;
   }

   public String n() {
      return this.cardStatus;
   }

   public boolean o() {
      return this.replacementOrdered;
   }

   public String r_() {
      return this.expires;
   }
}
