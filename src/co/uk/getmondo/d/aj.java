package co.uk.getmondo.d;

import io.realm.az;
import io.realm.bb;
import io.realm.bn;
import java.util.Date;

public class aj implements bb, bn {
   private String amountCurrency;
   private String amountLocalCurrency;
   private long amountLocalValue;
   private long amountValue;
   private az attachments;
   private String bacsDirectDebitInstructionId;
   private co.uk.getmondo.payments.send.data.a.a bankDetails;
   private String category;
   private Date created;
   private String createdDateFormatted;
   private String declineReason;
   private String description;
   private boolean fromAtm;
   private boolean fromMonzoMe;
   private boolean hideAmount;
   private String id;
   private boolean includeInSpending;
   private u merchant;
   private String merchantDescription;
   private String notes;
   private aa peer;
   private boolean peerToPeer;
   private String scheme;
   private boolean settled;
   private boolean topUp;
   private Date updated;

   public aj() {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

   }

   public aj(String var1, long var2, String var4, long var5, String var7, Date var8, String var9, Date var10, String var11, String var12, String var13, String var14, boolean var15, boolean var16, u var17, String var18, boolean var19, boolean var20, boolean var21, boolean var22, az var23, aa var24, boolean var25, co.uk.getmondo.payments.send.data.a.a var26, String var27, String var28) {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

      this.c(var1);
      this.a(var2);
      this.d(var4);
      this.b(var5);
      this.e(var7);
      this.a(var8);
      this.f(var9);
      this.b(var10);
      this.g(var11);
      this.h(var12);
      this.i(var13);
      this.j(var14);
      this.a(var15);
      this.b(var16);
      this.a(var17);
      this.k(var18);
      this.c(var19);
      this.d(var20);
      this.e(var21);
      this.f(var22);
      this.a(var23);
      this.a(var24);
      this.g(var25);
      this.a(var26);
      this.l(var27);
      this.m(var28);
   }

   public az A() {
      return this.ab();
   }

   public aa B() {
      return this.ac();
   }

   public co.uk.getmondo.payments.send.data.a.a C() {
      return this.ad();
   }

   public af D() {
      return af.a(this.af());
   }

   public boolean E() {
      return this.ae();
   }

   public String F() {
      String var1;
      if(this.ab() != null && !this.ab().isEmpty()) {
         var1 = ((d)this.ab().b()).b();
      } else {
         var1 = null;
      }

      return var1;
   }

   public String G() {
      return this.ag();
   }

   public String H() {
      return this.id;
   }

   public long I() {
      return this.amountValue;
   }

   public String J() {
      return this.amountCurrency;
   }

   public long K() {
      return this.amountLocalValue;
   }

   public String L() {
      return this.amountLocalCurrency;
   }

   public Date M() {
      return this.created;
   }

   public String N() {
      return this.createdDateFormatted;
   }

   public Date O() {
      return this.updated;
   }

   public String P() {
      return this.merchantDescription;
   }

   public String Q() {
      return this.description;
   }

   public String R() {
      return this.declineReason;
   }

   public String S() {
      return this.notes;
   }

   public boolean T() {
      return this.hideAmount;
   }

   public boolean U() {
      return this.settled;
   }

   public u V() {
      return this.merchant;
   }

   public String W() {
      return this.category;
   }

   public boolean X() {
      return this.fromAtm;
   }

   public boolean Y() {
      return this.peerToPeer;
   }

   public boolean Z() {
      return this.topUp;
   }

   public void a(long var1) {
      this.amountValue = var1;
   }

   public void a(aa var1) {
      this.peer = var1;
   }

   public void a(u var1) {
      this.merchant = var1;
   }

   public void a(co.uk.getmondo.payments.send.data.a.a var1) {
      this.bankDetails = var1;
   }

   public void a(az var1) {
      this.attachments = var1;
   }

   public void a(String var1) {
      this.k(var1);
   }

   public void a(Date var1) {
      this.created = var1;
   }

   public void a(boolean var1) {
      this.hideAmount = var1;
   }

   public boolean a() {
      boolean var1;
      if(this.V() != null && this.V().a()) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public boolean aa() {
      return this.includeInSpending;
   }

   public az ab() {
      return this.attachments;
   }

   public aa ac() {
      return this.peer;
   }

   public co.uk.getmondo.payments.send.data.a.a ad() {
      return this.bankDetails;
   }

   public boolean ae() {
      return this.fromMonzoMe;
   }

   public String af() {
      return this.scheme;
   }

   public String ag() {
      return this.bacsDirectDebitInstructionId;
   }

   public void b(long var1) {
      this.amountLocalValue = var1;
   }

   public void b(String var1) {
      this.j(var1);
   }

   public void b(Date var1) {
      this.updated = var1;
   }

   public void b(boolean var1) {
      this.settled = var1;
   }

   public boolean b() {
      boolean var1;
      if(this.V() != null && this.c() == h.EATING_OUT && "USD".equals(this.L())) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public h c() {
      return h.a(this.W());
   }

   public void c(String var1) {
      this.id = var1;
   }

   public void c(boolean var1) {
      this.fromAtm = var1;
   }

   public com.c.b.b d() {
      return com.c.b.b.b(this.R());
   }

   public void d(String var1) {
      this.amountCurrency = var1;
   }

   public void d(boolean var1) {
      this.peerToPeer = var1;
   }

   public String e() {
      return this.S();
   }

   public void e(String var1) {
      this.amountLocalCurrency = var1;
   }

   public void e(boolean var1) {
      this.topUp = var1;
   }

   public boolean equals(Object var1) {
      boolean var4 = true;
      boolean var3 = false;
      boolean var2;
      if(this == var1) {
         var2 = true;
      } else {
         var2 = var3;
         if(var1 != null) {
            var2 = var3;
            if(this.getClass() == var1.getClass()) {
               aj var5 = (aj)var1;
               var2 = var3;
               if(this.I() == var5.I()) {
                  var2 = var3;
                  if(this.K() == var5.K()) {
                     var2 = var3;
                     if(this.T() == var5.T()) {
                        var2 = var3;
                        if(this.U() == var5.U()) {
                           var2 = var3;
                           if(this.X() == var5.X()) {
                              var2 = var3;
                              if(this.Y() == var5.Y()) {
                                 var2 = var3;
                                 if(this.Z() == var5.Z()) {
                                    var2 = var3;
                                    if(this.aa() == var5.aa()) {
                                       var2 = var3;
                                       if(this.ae() == var5.ae()) {
                                          if(this.H() != null) {
                                             var2 = var3;
                                             if(!this.H().equals(var5.H())) {
                                                return var2;
                                             }
                                          } else if(var5.H() != null) {
                                             var2 = var3;
                                             return var2;
                                          }

                                          if(this.J() != null) {
                                             var2 = var3;
                                             if(!this.J().equals(var5.J())) {
                                                return var2;
                                             }
                                          } else if(var5.J() != null) {
                                             var2 = var3;
                                             return var2;
                                          }

                                          if(this.L() != null) {
                                             var2 = var3;
                                             if(!this.L().equals(var5.L())) {
                                                return var2;
                                             }
                                          } else if(var5.L() != null) {
                                             var2 = var3;
                                             return var2;
                                          }

                                          if(this.M() != null) {
                                             var2 = var3;
                                             if(!this.M().equals(var5.M())) {
                                                return var2;
                                             }
                                          } else if(var5.M() != null) {
                                             var2 = var3;
                                             return var2;
                                          }

                                          if(this.N() != null) {
                                             var2 = var3;
                                             if(!this.N().equals(var5.N())) {
                                                return var2;
                                             }
                                          } else if(var5.N() != null) {
                                             var2 = var3;
                                             return var2;
                                          }

                                          if(this.O() != null) {
                                             var2 = var3;
                                             if(!this.O().equals(var5.O())) {
                                                return var2;
                                             }
                                          } else if(var5.O() != null) {
                                             var2 = var3;
                                             return var2;
                                          }

                                          if(this.P() != null) {
                                             var2 = var3;
                                             if(!this.P().equals(var5.P())) {
                                                return var2;
                                             }
                                          } else if(var5.P() != null) {
                                             var2 = var3;
                                             return var2;
                                          }

                                          if(this.Q() != null) {
                                             var2 = var3;
                                             if(!this.Q().equals(var5.Q())) {
                                                return var2;
                                             }
                                          } else if(var5.Q() != null) {
                                             var2 = var3;
                                             return var2;
                                          }

                                          if(this.R() != null) {
                                             var2 = var3;
                                             if(!this.R().equals(var5.R())) {
                                                return var2;
                                             }
                                          } else if(var5.R() != null) {
                                             var2 = var3;
                                             return var2;
                                          }

                                          if(this.S() != null) {
                                             var2 = var3;
                                             if(!this.S().equals(var5.S())) {
                                                return var2;
                                             }
                                          } else if(var5.S() != null) {
                                             var2 = var3;
                                             return var2;
                                          }

                                          if(this.V() != null) {
                                             var2 = var3;
                                             if(!this.V().equals(var5.V())) {
                                                return var2;
                                             }
                                          } else if(var5.V() != null) {
                                             var2 = var3;
                                             return var2;
                                          }

                                          if(this.W() != null) {
                                             var2 = var3;
                                             if(!this.W().equals(var5.W())) {
                                                return var2;
                                             }
                                          } else if(var5.W() != null) {
                                             var2 = var3;
                                             return var2;
                                          }

                                          if(this.ab() != null) {
                                             var2 = var3;
                                             if(!this.ab().equals(var5.ab())) {
                                                return var2;
                                             }
                                          } else if(var5.ab() != null) {
                                             var2 = var3;
                                             return var2;
                                          }

                                          if(this.ac() != null) {
                                             var2 = var3;
                                             if(!this.ac().equals(var5.ac())) {
                                                return var2;
                                             }
                                          } else if(var5.ac() != null) {
                                             var2 = var3;
                                             return var2;
                                          }

                                          if(this.ad() != null) {
                                             var2 = var3;
                                             if(!this.ad().equals(var5.ad())) {
                                                return var2;
                                             }
                                          } else if(var5.ad() != null) {
                                             var2 = var3;
                                             return var2;
                                          }

                                          if(this.af() != null) {
                                             var2 = var3;
                                             if(!this.af().equals(var5.af())) {
                                                return var2;
                                             }
                                          } else if(var5.af() != null) {
                                             var2 = var3;
                                             return var2;
                                          }

                                          if(this.ag() != null) {
                                             var2 = this.ag().equals(var5.ag());
                                          } else {
                                             var2 = var4;
                                             if(var5.ag() != null) {
                                                var2 = false;
                                             }
                                          }
                                       }
                                    }
                                 }
                              }
                           }
                        }
                     }
                  }
               }
            }
         }
      }

      return var2;
   }

   public u f() {
      return this.V();
   }

   public void f(String var1) {
      this.createdDateFormatted = var1;
   }

   public void f(boolean var1) {
      this.includeInSpending = var1;
   }

   public c g() {
      return new c(this.I(), this.J());
   }

   public void g(String var1) {
      this.merchantDescription = var1;
   }

   public void g(boolean var1) {
      this.fromMonzoMe = var1;
   }

   public c h() {
      return new c(this.K(), this.L());
   }

   public void h(String var1) {
      this.description = var1;
   }

   public int hashCode() {
      byte var22 = 1;
      int var24 = 0;
      int var1;
      if(this.H() != null) {
         var1 = this.H().hashCode();
      } else {
         var1 = 0;
      }

      int var25 = (int)(this.I() ^ this.I() >>> 32);
      int var2;
      if(this.J() != null) {
         var2 = this.J().hashCode();
      } else {
         var2 = 0;
      }

      int var26 = (int)(this.K() ^ this.K() >>> 32);
      int var3;
      if(this.L() != null) {
         var3 = this.L().hashCode();
      } else {
         var3 = 0;
      }

      int var4;
      if(this.M() != null) {
         var4 = this.M().hashCode();
      } else {
         var4 = 0;
      }

      int var5;
      if(this.N() != null) {
         var5 = this.N().hashCode();
      } else {
         var5 = 0;
      }

      int var6;
      if(this.O() != null) {
         var6 = this.O().hashCode();
      } else {
         var6 = 0;
      }

      int var7;
      if(this.P() != null) {
         var7 = this.P().hashCode();
      } else {
         var7 = 0;
      }

      int var8;
      if(this.Q() != null) {
         var8 = this.Q().hashCode();
      } else {
         var8 = 0;
      }

      int var9;
      if(this.R() != null) {
         var9 = this.R().hashCode();
      } else {
         var9 = 0;
      }

      int var10;
      if(this.S() != null) {
         var10 = this.S().hashCode();
      } else {
         var10 = 0;
      }

      byte var11;
      if(this.T()) {
         var11 = 1;
      } else {
         var11 = 0;
      }

      byte var12;
      if(this.U()) {
         var12 = 1;
      } else {
         var12 = 0;
      }

      int var13;
      if(this.V() != null) {
         var13 = this.V().hashCode();
      } else {
         var13 = 0;
      }

      int var14;
      if(this.W() != null) {
         var14 = this.W().hashCode();
      } else {
         var14 = 0;
      }

      byte var15;
      if(this.X()) {
         var15 = 1;
      } else {
         var15 = 0;
      }

      byte var16;
      if(this.Y()) {
         var16 = 1;
      } else {
         var16 = 0;
      }

      byte var17;
      if(this.Z()) {
         var17 = 1;
      } else {
         var17 = 0;
      }

      byte var18;
      if(this.aa()) {
         var18 = 1;
      } else {
         var18 = 0;
      }

      int var19;
      if(this.ab() != null) {
         var19 = this.ab().hashCode();
      } else {
         var19 = 0;
      }

      int var20;
      if(this.ac() != null) {
         var20 = this.ac().hashCode();
      } else {
         var20 = 0;
      }

      int var21;
      if(this.ad() != null) {
         var21 = this.ad().hashCode();
      } else {
         var21 = 0;
      }

      if(!this.ae()) {
         var22 = 0;
      }

      int var23;
      if(this.af() != null) {
         var23 = this.af().hashCode();
      } else {
         var23 = 0;
      }

      if(this.ag() != null) {
         var24 = this.ag().hashCode();
      }

      return (var23 + ((var21 + (var20 + (var19 + (var18 + (var17 + (var16 + (var15 + (var14 + (var13 + (var12 + (var11 + (var10 + (var9 + (var8 + (var7 + (var6 + (var5 + (var4 + (var3 + ((var2 + (var1 * 31 + var25) * 31) * 31 + var26) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31 + var22) * 31) * 31 + var24;
   }

   public void i(String var1) {
      this.declineReason = var1;
   }

   public boolean i() {
      boolean var1;
      if(!this.J().equals(this.L())) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public double j() {
      double var1 = 0.0D;
      double var3 = this.g().f();
      if(var3 != 0.0D) {
         var1 = this.h().f() / var3;
      }

      return var1;
   }

   public void j(String var1) {
      this.notes = var1;
   }

   public void k(String var1) {
      this.category = var1;
   }

   public boolean k() {
      boolean var1;
      if(!this.U() && this.d().d() && !this.T() && this.g().a()) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public String l() {
      return this.P().trim().replaceAll(" +", " ");
   }

   public void l(String var1) {
      this.scheme = var1;
   }

   public void m(String var1) {
      this.bacsDirectDebitInstructionId = var1;
   }

   public boolean m() {
      boolean var1;
      if(this.S() != null && !this.S().equals("")) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public boolean n() {
      boolean var1;
      if(this.f() != null && co.uk.getmondo.common.k.p.c(this.f().j())) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public String o() {
      return co.uk.getmondo.common.c.a.a(this.v());
   }

   public boolean p() {
      boolean var1;
      if(this.d().b() && co.uk.getmondo.common.k.p.c((String)this.d().a())) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public boolean q() {
      return this.Y();
   }

   public boolean r() {
      boolean var1;
      if(this.D() == af.FASTER_PAYMENT && this.ad() != null) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public boolean s() {
      boolean var1;
      if(this.D() == af.BACS) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public boolean t() {
      boolean var1;
      if(this.D() == af.OVERDRAFT) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public boolean u() {
      boolean var1;
      if(this.s() && this.G() != null) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public Date v() {
      return this.M();
   }

   public String w() {
      return this.H();
   }

   public String x() {
      return this.Q();
   }

   public boolean y() {
      return this.T();
   }

   public boolean z() {
      return this.Z();
   }
}
