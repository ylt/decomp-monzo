package co.uk.getmondo.d;

public enum j {
   CARD_BLOCKED,
   CARD_INACTIVE,
   CONTACTLESS_FAILURE,
   EXCEEDS_TRANSACTION_AMOUNT_LIMIT,
   EXCEEDS_TRANSACTION_AMOUNT_LIMIT_KYC_REQUIRED,
   EXCEEDS_WITHDRAWAL_AMOUNT_LIMIT,
   EXCEEDS_WITHDRAWAL_AMOUNT_LIMIT_KYC_REQUIRED,
   EXCEEDS_WITHDRAWAL_COUNT_LIMIT,
   EXCEEDS_WITHDRAWAL_COUNT_LIMIT_KYC_REQUIRED,
   INSUFFICIENT_FUNDS,
   INVALID_CVC,
   INVALID_EXPIRY_DATE,
   INVALID_MERCHANT,
   INVALID_PIN,
   KYC_REQUIRED,
   MAGNETIC_STRIP_ATM,
   NOT_DECLINED,
   OTHER,
   PIN_RETRY_COUNT_EXCEEDED;

   public static j a(String var0) {
      j var2;
      if(var0 == null) {
         var2 = NOT_DECLINED;
      } else {
         byte var1 = -1;
         switch(var0.hashCode()) {
         case -1777036633:
            if(var0.equals("EXCEEDS_WITHDRAWAL_COUNT_LIMIT_KYC_REQUIRED")) {
               var1 = 15;
            }
            break;
         case -1205294198:
            if(var0.equals("EXCEEDS_WITHDRAWAL_AMOUNT_LIMIT_KYC_REQUIRED")) {
               var1 = 14;
            }
            break;
         case -1022719036:
            if(var0.equals("PIN_RETRY_COUNT_EXCEEDED")) {
               var1 = 16;
            }
            break;
         case -760002350:
            if(var0.equals("INVALID_EXPIRY_DATE")) {
               var1 = 6;
            }
            break;
         case -627306704:
            if(var0.equals("INVALID_MERCHANT")) {
               var1 = 4;
            }
            break;
         case -510312674:
            if(var0.equals("EXCEEDS_WITHDRAWAL_AMOUNT_LIMIT")) {
               var1 = 11;
            }
            break;
         case -89191199:
            if(var0.equals("EXCEEDS_WITHDRAWAL_COUNT_LIMIT")) {
               var1 = 12;
            }
            break;
         case 75532016:
            if(var0.equals("OTHER")) {
               var1 = 0;
            }
            break;
         case 86317810:
            if(var0.equals("INSUFFICIENT_FUNDS")) {
               var1 = 1;
            }
            break;
         case 274890761:
            if(var0.equals("KYC_REQUIRED")) {
               var1 = 9;
            }
            break;
         case 284837365:
            if(var0.equals("EXCEEDS_TRANSACTION_AMOUNT_LIMIT")) {
               var1 = 10;
            }
            break;
         case 449948728:
            if(var0.equals("MAGNETIC_STRIP_ATM")) {
               var1 = 5;
            }
            break;
         case 693598109:
            if(var0.equals("CARD_BLOCKED")) {
               var1 = 2;
            }
            break;
         case 715947194:
            if(var0.equals("CARD_INACTIVE")) {
               var1 = 3;
            }
            break;
         case 1201074120:
            if(var0.equals("INVALID_CVC")) {
               var1 = 7;
            }
            break;
         case 1201086221:
            if(var0.equals("INVALID_PIN")) {
               var1 = 17;
            }
            break;
         case 1491188243:
            if(var0.equals("EXCEEDS_TRANSACTION_AMOUNT_LIMIT_KYC_REQUIRED")) {
               var1 = 13;
            }
            break;
         case 1811088580:
            if(var0.equals("CONTACTLESS_FAILURE")) {
               var1 = 8;
            }
         }

         switch(var1) {
         case 0:
            var2 = OTHER;
            break;
         case 1:
            var2 = INSUFFICIENT_FUNDS;
            break;
         case 2:
            var2 = CARD_BLOCKED;
            break;
         case 3:
            var2 = CARD_INACTIVE;
            break;
         case 4:
            var2 = INVALID_MERCHANT;
            break;
         case 5:
            var2 = MAGNETIC_STRIP_ATM;
            break;
         case 6:
            var2 = INVALID_EXPIRY_DATE;
            break;
         case 7:
            var2 = INVALID_CVC;
            break;
         case 8:
            var2 = CONTACTLESS_FAILURE;
            break;
         case 9:
            var2 = KYC_REQUIRED;
            break;
         case 10:
            var2 = EXCEEDS_TRANSACTION_AMOUNT_LIMIT;
            break;
         case 11:
            var2 = EXCEEDS_WITHDRAWAL_AMOUNT_LIMIT;
            break;
         case 12:
            var2 = EXCEEDS_WITHDRAWAL_COUNT_LIMIT;
            break;
         case 13:
            var2 = EXCEEDS_TRANSACTION_AMOUNT_LIMIT_KYC_REQUIRED;
            break;
         case 14:
            var2 = EXCEEDS_WITHDRAWAL_AMOUNT_LIMIT_KYC_REQUIRED;
            break;
         case 15:
            var2 = EXCEEDS_WITHDRAWAL_COUNT_LIMIT_KYC_REQUIRED;
            break;
         case 16:
            var2 = PIN_RETRY_COUNT_EXCEEDED;
            break;
         case 17:
            var2 = INVALID_PIN;
            break;
         default:
            var2 = OTHER;
         }
      }

      return var2;
   }
}
