package co.uk.getmondo.d;

import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\b\f\b\u0086\u0001\u0018\u0000 \u000e2\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u000eB\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\u0007j\u0002\b\bj\u0002\b\tj\u0002\b\nj\u0002\b\u000bj\u0002\b\fj\u0002\b\r¨\u0006\u000f"},
   d2 = {"Lco/uk/getmondo/model/Scheme;", "", "apiValue", "", "(Ljava/lang/String;ILjava/lang/String;)V", "getApiValue", "()Ljava/lang/String;", "GPS_MASTERCARD", "BACS", "FASTER_PAYMENT", "MASTERCARD", "OVERDRAFT", "CHAPS", "UNKNOWN", "Find", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public enum af {
   BACS,
   CHAPS,
   FASTER_PAYMENT;

   public static final af.a Find;
   GPS_MASTERCARD,
   MASTERCARD,
   OVERDRAFT,
   UNKNOWN;

   private final String apiValue;

   static {
      af var3 = new af("GPS_MASTERCARD", 0, "gps_mastercard");
      GPS_MASTERCARD = var3;
      af var6 = new af("BACS", 1, "bacs");
      BACS = var6;
      af var0 = new af("FASTER_PAYMENT", 2, "payport_faster_payments");
      FASTER_PAYMENT = var0;
      af var2 = new af("MASTERCARD", 3, "mastercard");
      MASTERCARD = var2;
      af var4 = new af("OVERDRAFT", 4, "overdraft");
      OVERDRAFT = var4;
      af var1 = new af("CHAPS", 5, "chaps");
      CHAPS = var1;
      af var5 = new af("UNKNOWN", 6, "");
      UNKNOWN = var5;
      Find = new af.a((kotlin.d.b.i)null);
   }

   protected af(String var3) {
      kotlin.d.b.l.b(var3, "apiValue");
      super(var1, var2);
      this.apiValue = var3;
   }

   public static final af a(String var0) {
      return Find.a(var0);
   }

   public final String a() {
      return this.apiValue;
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0007¨\u0006\u0007"},
      d2 = {"Lco/uk/getmondo/model/Scheme$Find;", "", "()V", "fromApiType", "Lco/uk/getmondo/model/Scheme;", "apiValue", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var1) {
         this();
      }

      public final af a(String var1) {
         Object[] var4 = (Object[])af.values();
         int var2 = 0;

         Object var5;
         while(true) {
            if(var2 >= var4.length) {
               var5 = null;
               break;
            }

            Object var3 = var4[var2];
            if(kotlin.d.b.l.a(((af)var3).a(), var1)) {
               var5 = var3;
               break;
            }

            ++var2;
         }

         af var6 = (af)var5;
         if(var6 == null) {
            var6 = af.UNKNOWN;
         }

         return var6;
      }
   }
}
