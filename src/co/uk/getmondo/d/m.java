package co.uk.getmondo.d;

import android.net.Uri;
import io.realm.bb;
import java.util.Date;

public class m implements bb, io.realm.t {
   private String accountId;
   private String appUri;
   private f basicItemInfo;
   private Date created;
   private o goldenTicket;
   private String id;
   private boolean isDismissable;
   private r kycRejected;
   private v monthlySpendingReport;
   private String sddRejectedReason;
   private aj transaction;
   private String type;

   public m() {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

   }

   public m(String var1, String var2, Date var3, String var4, String var5, o var6, v var7, aj var8, r var9, String var10, f var11, boolean var12) {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

      this.a(var1);
      this.b(var2);
      this.a(var3);
      this.c(var4);
      this.d(var5);
      this.a(var6);
      this.a(var7);
      this.a(var8);
      this.a(var9);
      this.e(var10);
      this.a(var11);
      this.a(var12);
   }

   public String a() {
      return this.l();
   }

   public void a(aj var1) {
      this.transaction = var1;
   }

   public void a(f var1) {
      this.basicItemInfo = var1;
   }

   public void a(o var1) {
      this.goldenTicket = var1;
   }

   public void a(r var1) {
      this.kycRejected = var1;
   }

   public void a(v var1) {
      this.monthlySpendingReport = var1;
   }

   public void a(String var1) {
      this.id = var1;
   }

   public void a(Date var1) {
      this.created = var1;
   }

   public void a(boolean var1) {
      this.isDismissable = var1;
   }

   public String b() {
      return this.m();
   }

   public void b(String var1) {
      this.accountId = var1;
   }

   public Date c() {
      return this.n();
   }

   public void c(String var1) {
      this.type = var1;
   }

   public Uri d() {
      Uri var1;
      if(this.p() == null) {
         var1 = null;
      } else {
         var1 = Uri.parse(this.p());
      }

      return var1;
   }

   public void d(String var1) {
      this.appUri = var1;
   }

   public com.c.b.b e() {
      return com.c.b.b.b(this.s());
   }

   public void e(String var1) {
      this.sddRejectedReason = var1;
   }

   public boolean equals(Object var1) {
      boolean var3 = true;
      boolean var4 = false;
      boolean var2;
      if(this == var1) {
         var2 = true;
      } else {
         var2 = var4;
         if(var1 != null) {
            var2 = var4;
            if(this.getClass() == var1.getClass()) {
               m var5 = (m)var1;
               var2 = var4;
               if(this.w() == var5.w()) {
                  if(this.l() != null) {
                     var2 = var4;
                     if(!this.l().equals(var5.l())) {
                        return var2;
                     }
                  } else if(var5.l() != null) {
                     var2 = var4;
                     return var2;
                  }

                  if(this.m() != null) {
                     var2 = var4;
                     if(!this.m().equals(var5.m())) {
                        return var2;
                     }
                  } else if(var5.m() != null) {
                     var2 = var4;
                     return var2;
                  }

                  if(this.n() != null) {
                     var2 = var4;
                     if(!this.n().equals(var5.n())) {
                        return var2;
                     }
                  } else if(var5.n() != null) {
                     var2 = var4;
                     return var2;
                  }

                  if(this.o() != null) {
                     var2 = var4;
                     if(!this.o().equals(var5.o())) {
                        return var2;
                     }
                  } else if(var5.o() != null) {
                     var2 = var4;
                     return var2;
                  }

                  if(this.p() != null) {
                     var2 = var4;
                     if(!this.p().equals(var5.p())) {
                        return var2;
                     }
                  } else if(var5.p() != null) {
                     var2 = var4;
                     return var2;
                  }

                  if(this.q() != null) {
                     var2 = var4;
                     if(!this.q().equals(var5.q())) {
                        return var2;
                     }
                  } else if(var5.q() != null) {
                     var2 = var4;
                     return var2;
                  }

                  if(this.r() != null) {
                     var2 = var4;
                     if(!this.r().equals(var5.r())) {
                        return var2;
                     }
                  } else if(var5.r() != null) {
                     var2 = var4;
                     return var2;
                  }

                  if(this.s() != null) {
                     var2 = var4;
                     if(!this.s().equals(var5.s())) {
                        return var2;
                     }
                  } else if(var5.s() != null) {
                     var2 = var4;
                     return var2;
                  }

                  if(this.t() != null) {
                     var2 = var4;
                     if(!this.t().equals(var5.t())) {
                        return var2;
                     }
                  } else if(var5.t() != null) {
                     var2 = var4;
                     return var2;
                  }

                  if(this.u() != null) {
                     var2 = var4;
                     if(!this.u().equals(var5.u())) {
                        return var2;
                     }
                  } else if(var5.u() != null) {
                     var2 = var4;
                     return var2;
                  }

                  if(this.v() != null) {
                     var2 = this.v().equals(var5.v());
                  } else {
                     var2 = var3;
                     if(var5.v() != null) {
                        var2 = false;
                     }
                  }
               }
            }
         }
      }

      return var2;
   }

   public o f() {
      return this.q();
   }

   public v g() {
      return this.r();
   }

   public r h() {
      return this.t();
   }

   public int hashCode() {
      byte var12 = 0;
      int var1;
      if(this.l() != null) {
         var1 = this.l().hashCode();
      } else {
         var1 = 0;
      }

      int var2;
      if(this.m() != null) {
         var2 = this.m().hashCode();
      } else {
         var2 = 0;
      }

      int var3;
      if(this.n() != null) {
         var3 = this.n().hashCode();
      } else {
         var3 = 0;
      }

      int var4;
      if(this.o() != null) {
         var4 = this.o().hashCode();
      } else {
         var4 = 0;
      }

      int var5;
      if(this.p() != null) {
         var5 = this.p().hashCode();
      } else {
         var5 = 0;
      }

      int var6;
      if(this.q() != null) {
         var6 = this.q().hashCode();
      } else {
         var6 = 0;
      }

      int var7;
      if(this.r() != null) {
         var7 = this.r().hashCode();
      } else {
         var7 = 0;
      }

      int var8;
      if(this.s() != null) {
         var8 = this.s().hashCode();
      } else {
         var8 = 0;
      }

      int var9;
      if(this.t() != null) {
         var9 = this.t().hashCode();
      } else {
         var9 = 0;
      }

      int var10;
      if(this.u() != null) {
         var10 = this.u().hashCode();
      } else {
         var10 = 0;
      }

      int var11;
      if(this.v() != null) {
         var11 = this.v().hashCode();
      } else {
         var11 = 0;
      }

      if(this.w()) {
         var12 = 1;
      }

      return (var11 + (var10 + (var9 + (var8 + (var7 + (var6 + (var5 + (var4 + (var3 + (var2 + var1 * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31 + var12;
   }

   public f i() {
      return this.v();
   }

   public co.uk.getmondo.feed.a.a.a j() {
      return co.uk.getmondo.feed.a.a.a.o.a(this.o());
   }

   public boolean k() {
      return this.w();
   }

   public String l() {
      return this.id;
   }

   public String m() {
      return this.accountId;
   }

   public Date n() {
      return this.created;
   }

   public String o() {
      return this.type;
   }

   public String p() {
      return this.appUri;
   }

   public o q() {
      return this.goldenTicket;
   }

   public v r() {
      return this.monthlySpendingReport;
   }

   public aj s() {
      return this.transaction;
   }

   public r t() {
      return this.kycRejected;
   }

   public String u() {
      return this.sddRejectedReason;
   }

   public f v() {
      return this.basicItemInfo;
   }

   public boolean w() {
      return this.isDismissable;
   }
}
