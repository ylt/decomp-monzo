package co.uk.getmondo.d;

import io.realm.bb;
import io.realm.bp;

public class am implements bb, bp {
   public static final String ID = "user_settings_storage_static_id";
   private String id;
   private p inboundP2P;
   private String monzoMeUsername;
   private boolean prepaidAccountMigrated;
   private String statusId;

   public am() {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

      this.a("user_settings_storage_static_id");
   }

   public am(String var1, String var2, p var3, boolean var4) {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

      this.a("user_settings_storage_static_id");
      this.b(var1);
      this.c(var2);
      this.a(var3);
      this.a(var4);
   }

   public co.uk.getmondo.payments.send.data.a.d a() {
      return co.uk.getmondo.payments.send.data.a.d.a(this.f());
   }

   public void a(p var1) {
      this.inboundP2P = var1;
   }

   public void a(String var1) {
      this.id = var1;
   }

   public void a(boolean var1) {
      this.prepaidAccountMigrated = var1;
   }

   public String b() {
      return this.g();
   }

   public void b(String var1) {
      this.statusId = var1;
   }

   public p c() {
      return this.h();
   }

   public void c(String var1) {
      this.monzoMeUsername = var1;
   }

   public boolean d() {
      return this.i();
   }

   public String e() {
      return this.id;
   }

   public boolean equals(Object var1) {
      boolean var4 = true;
      boolean var3 = false;
      boolean var2;
      if(this == var1) {
         var2 = true;
      } else {
         var2 = var3;
         if(var1 != null) {
            var2 = var3;
            if(this.getClass() == var1.getClass()) {
               am var5 = (am)var1;
               var2 = var3;
               if(this.i() == var5.i()) {
                  if(this.e() != null) {
                     var2 = var3;
                     if(!this.e().equals(var5.e())) {
                        return var2;
                     }
                  } else if(var5.e() != null) {
                     var2 = var3;
                     return var2;
                  }

                  if(this.f() != null) {
                     var2 = var3;
                     if(!this.f().equals(var5.f())) {
                        return var2;
                     }
                  } else if(var5.f() != null) {
                     var2 = var3;
                     return var2;
                  }

                  if(this.g() != null) {
                     var2 = var3;
                     if(!this.g().equals(var5.g())) {
                        return var2;
                     }
                  } else if(var5.g() != null) {
                     var2 = var3;
                     return var2;
                  }

                  if(this.h() != null) {
                     var2 = this.h().equals(var5.h());
                  } else {
                     var2 = var4;
                     if(var5.h() != null) {
                        var2 = false;
                     }
                  }
               }
            }
         }
      }

      return var2;
   }

   public String f() {
      return this.statusId;
   }

   public String g() {
      return this.monzoMeUsername;
   }

   public p h() {
      return this.inboundP2P;
   }

   public int hashCode() {
      byte var5 = 0;
      int var1;
      if(this.e() != null) {
         var1 = this.e().hashCode();
      } else {
         var1 = 0;
      }

      int var2;
      if(this.f() != null) {
         var2 = this.f().hashCode();
      } else {
         var2 = 0;
      }

      int var3;
      if(this.g() != null) {
         var3 = this.g().hashCode();
      } else {
         var3 = 0;
      }

      int var4;
      if(this.h() != null) {
         var4 = this.h().hashCode();
      } else {
         var4 = 0;
      }

      if(this.i()) {
         var5 = 1;
      }

      return (var4 + (var3 + (var2 + var1 * 31) * 31) * 31) * 31 + var5;
   }

   public boolean i() {
      return this.prepaidAccountMigrated;
   }
}
