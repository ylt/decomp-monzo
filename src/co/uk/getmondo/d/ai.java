package co.uk.getmondo.d;

import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\b\b\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\b\u0086\b\u0018\u00002\u00020\u0001:\u0001\u0018B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\u000f\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0010\u001a\u00020\u0005HÆ\u0003J\u001d\u0010\u0011\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u0012\u001a\u00020\u00132\b\u0010\u0014\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0015\u001a\u00020\u0016HÖ\u0001J\t\u0010\u0017\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\t\u001a\u00020\n8F¢\u0006\u0006\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e¨\u0006\u0019"},
   d2 = {"Lco/uk/getmondo/model/Token;", "", "accessToken", "", "refreshData", "Lco/uk/getmondo/model/Token$RefreshData;", "(Ljava/lang/String;Lco/uk/getmondo/model/Token$RefreshData;)V", "getAccessToken", "()Ljava/lang/String;", "ageInMilliseconds", "", "getAgeInMilliseconds", "()J", "getRefreshData", "()Lco/uk/getmondo/model/Token$RefreshData;", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "RefreshData", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ai {
   private final String accessToken;
   private final ai.a refreshData;

   public ai(String var1, ai.a var2) {
      kotlin.d.b.l.b(var1, "accessToken");
      kotlin.d.b.l.b(var2, "refreshData");
      super();
      this.accessToken = var1;
      this.refreshData = var2;
   }

   public final long a() {
      return System.currentTimeMillis() - this.refreshData.c();
   }

   public final String b() {
      return this.accessToken;
   }

   public final ai.a c() {
      return this.refreshData;
   }

   public boolean equals(Object var1) {
      boolean var2;
      if(this != var1) {
         label28: {
            if(var1 instanceof ai) {
               ai var3 = (ai)var1;
               if(kotlin.d.b.l.a(this.accessToken, var3.accessToken) && kotlin.d.b.l.a(this.refreshData, var3.refreshData)) {
                  break label28;
               }
            }

            var2 = false;
            return var2;
         }
      }

      var2 = true;
      return var2;
   }

   public int hashCode() {
      int var2 = 0;
      String var3 = this.accessToken;
      int var1;
      if(var3 != null) {
         var1 = var3.hashCode();
      } else {
         var1 = 0;
      }

      ai.a var4 = this.refreshData;
      if(var4 != null) {
         var2 = var4.hashCode();
      }

      return var1 * 31 + var2;
   }

   public String toString() {
      return "Token(accessToken=" + this.accessToken + ", refreshData=" + this.refreshData + ")";
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\b\r\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0010\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0011\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0012\u001a\u00020\u0007HÆ\u0003J1\u0010\u0013\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u0007HÆ\u0001J\u0013\u0010\u0014\u001a\u00020\u00152\b\u0010\u0016\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0017\u001a\u00020\u0018HÖ\u0001J\t\u0010\u0019\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0005\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\fR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\f¨\u0006\u001a"},
      d2 = {"Lco/uk/getmondo/model/Token$RefreshData;", "", "refreshToken", "", "expiresIn", "refreshAttempt", "createdAt", "", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V", "getCreatedAt", "()J", "getExpiresIn", "()Ljava/lang/String;", "getRefreshAttempt", "getRefreshToken", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "hashCode", "", "toString", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private final long createdAt;
      private final String expiresIn;
      private final String refreshAttempt;
      private final String refreshToken;

      public a(String var1, String var2, String var3, long var4) {
         kotlin.d.b.l.b(var1, "refreshToken");
         kotlin.d.b.l.b(var2, "expiresIn");
         kotlin.d.b.l.b(var3, "refreshAttempt");
         super();
         this.refreshToken = var1;
         this.expiresIn = var2;
         this.refreshAttempt = var3;
         this.createdAt = var4;
      }

      public final String a() {
         return this.refreshToken;
      }

      public final String b() {
         return this.refreshAttempt;
      }

      public final long c() {
         return this.createdAt;
      }

      public boolean equals(Object var1) {
         boolean var4 = false;
         boolean var3;
         if(this != var1) {
            var3 = var4;
            if(!(var1 instanceof ai.a)) {
               return var3;
            }

            ai.a var5 = (ai.a)var1;
            var3 = var4;
            if(!kotlin.d.b.l.a(this.refreshToken, var5.refreshToken)) {
               return var3;
            }

            var3 = var4;
            if(!kotlin.d.b.l.a(this.expiresIn, var5.expiresIn)) {
               return var3;
            }

            var3 = var4;
            if(!kotlin.d.b.l.a(this.refreshAttempt, var5.refreshAttempt)) {
               return var3;
            }

            boolean var2;
            if(this.createdAt == var5.createdAt) {
               var2 = true;
            } else {
               var2 = false;
            }

            var3 = var4;
            if(!var2) {
               return var3;
            }
         }

         var3 = true;
         return var3;
      }

      public int hashCode() {
         int var3 = 0;
         String var6 = this.refreshToken;
         int var1;
         if(var6 != null) {
            var1 = var6.hashCode();
         } else {
            var1 = 0;
         }

         var6 = this.expiresIn;
         int var2;
         if(var6 != null) {
            var2 = var6.hashCode();
         } else {
            var2 = 0;
         }

         var6 = this.refreshAttempt;
         if(var6 != null) {
            var3 = var6.hashCode();
         }

         long var4 = this.createdAt;
         return ((var2 + var1 * 31) * 31 + var3) * 31 + (int)(var4 ^ var4 >>> 32);
      }

      public String toString() {
         return "RefreshData(refreshToken=" + this.refreshToken + ", expiresIn=" + this.expiresIn + ", refreshAttempt=" + this.refreshAttempt + ", createdAt=" + this.createdAt + ")";
      }
   }
}
