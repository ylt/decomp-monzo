package co.uk.getmondo.d;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import kotlin.Metadata;
import kotlin.TypeCastException;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0014\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0004\b\u0086\b\u0018\u0000 '2\u00020\u0001:\u0001'B\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004B)\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0006\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u0006\u0012\u0006\u0010\t\u001a\u00020\u0006¢\u0006\u0002\u0010\nJ\t\u0010\u0015\u001a\u00020\u0006HÆ\u0003J\t\u0010\u0016\u001a\u00020\u0006HÆ\u0003J\u000b\u0010\u0017\u001a\u0004\u0018\u00010\u0006HÆ\u0003J\t\u0010\u0018\u001a\u00020\u0006HÆ\u0003J3\u0010\u0019\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u00062\b\b\u0002\u0010\u0007\u001a\u00020\u00062\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u00062\b\b\u0002\u0010\t\u001a\u00020\u0006HÆ\u0001J\b\u0010\u001a\u001a\u00020\u001bH\u0016J\b\u0010\u001c\u001a\u0004\u0018\u00010\u0006J\u0013\u0010\u001d\u001a\u00020\u001e2\b\u0010\u001f\u001a\u0004\u0018\u00010 HÖ\u0003J\t\u0010!\u001a\u00020\u001bHÖ\u0001J\t\u0010\"\u001a\u00020\u0006HÖ\u0001J\u0018\u0010#\u001a\u00020$2\u0006\u0010%\u001a\u00020\u00032\u0006\u0010&\u001a\u00020\u001bH\u0016R\u001a\u0010\t\u001a\u00020\u0006X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u001c\u0010\b\u001a\u0004\u0018\u00010\u0006X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\f\"\u0004\b\u0010\u0010\u000eR\u001a\u0010\u0007\u001a\u00020\u0006X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\f\"\u0004\b\u0012\u0010\u000eR\u001a\u0010\u0005\u001a\u00020\u0006X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\f\"\u0004\b\u0014\u0010\u000e¨\u0006("},
   d2 = {"Lco/uk/getmondo/model/SavedStripeCard;", "Landroid/os/Parcelable;", "source", "Landroid/os/Parcel;", "(Landroid/os/Parcel;)V", "stripeCardId", "", "lastFour", "endDate", "brand", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getBrand", "()Ljava/lang/String;", "setBrand", "(Ljava/lang/String;)V", "getEndDate", "setEndDate", "getLastFour", "setLastFour", "getStripeCardId", "setStripeCardId", "component1", "component2", "component3", "component4", "copy", "describeContents", "", "endDateFormatted", "equals", "", "other", "", "hashCode", "toString", "writeToParcel", "", "dest", "flags", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ae implements Parcelable {
   public static final Creator CREATOR = (Creator)(new Creator() {
      public ae a(Parcel var1) {
         kotlin.d.b.l.b(var1, "parcel");
         return new ae(var1);
      }

      public ae[] a(int var1) {
         return new ae[var1];
      }

      // $FF: synthetic method
      public Object createFromParcel(Parcel var1) {
         return this.a(var1);
      }

      // $FF: synthetic method
      public Object[] newArray(int var1) {
         return (Object[])this.a(var1);
      }
   });
   public static final ae.a Companion = new ae.a((kotlin.d.b.i)null);
   private String brand;
   private String endDate;
   private String lastFour;
   private String stripeCardId;

   public ae(Parcel var1) {
      kotlin.d.b.l.b(var1, "source");
      String var3 = var1.readString();
      kotlin.d.b.l.a(var3, "source.readString()");
      String var4 = var1.readString();
      kotlin.d.b.l.a(var4, "source.readString()");
      String var2 = var1.readString();
      String var5 = var1.readString();
      kotlin.d.b.l.a(var5, "source.readString()");
      this(var3, var4, var2, var5);
   }

   public ae(String var1, String var2, String var3, String var4) {
      kotlin.d.b.l.b(var1, "stripeCardId");
      kotlin.d.b.l.b(var2, "lastFour");
      kotlin.d.b.l.b(var4, "brand");
      super();
      this.stripeCardId = var1;
      this.lastFour = var2;
      this.endDate = var3;
      this.brand = var4;
   }

   // $FF: synthetic method
   public ae(String var1, String var2, String var3, String var4, int var5, kotlin.d.b.i var6) {
      if((var5 & 4) != 0) {
         var3 = (String)null;
      }

      this(var1, var2, var3, var4);
   }

   public final String a() {
      Object var2 = null;
      StringBuilder var3 = null;
      String var1 = var3;
      if(this.endDate != null) {
         String var4 = this.endDate;
         var1 = var3;
         if(var4 != null) {
            if(var4.length() != 4) {
               var1 = var3;
            } else {
               var3 = new StringBuilder();
               var1 = this.endDate;
               if(var1 != null) {
                  if(var1 == null) {
                     throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                  }

                  var1 = var1.substring(0, 2);
                  kotlin.d.b.l.a(var1, "(this as java.lang.Strin…ing(startIndex, endIndex)");
               } else {
                  var1 = null;
               }

               var3 = var3.append(var1).append("/");
               var4 = this.endDate;
               var1 = (String)var2;
               if(var4 != null) {
                  if(var4 == null) {
                     throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                  }

                  var1 = var4.substring(2, 4);
                  kotlin.d.b.l.a(var1, "(this as java.lang.Strin…ing(startIndex, endIndex)");
               }

               var1 = var3.append(var1).toString();
            }
         }
      }

      return var1;
   }

   public final String b() {
      return this.stripeCardId;
   }

   public final String c() {
      return this.lastFour;
   }

   public final String d() {
      return this.brand;
   }

   public int describeContents() {
      return 0;
   }

   public boolean equals(Object var1) {
      boolean var2;
      if(this != var1) {
         label32: {
            if(var1 instanceof ae) {
               ae var3 = (ae)var1;
               if(kotlin.d.b.l.a(this.stripeCardId, var3.stripeCardId) && kotlin.d.b.l.a(this.lastFour, var3.lastFour) && kotlin.d.b.l.a(this.endDate, var3.endDate) && kotlin.d.b.l.a(this.brand, var3.brand)) {
                  break label32;
               }
            }

            var2 = false;
            return var2;
         }
      }

      var2 = true;
      return var2;
   }

   public int hashCode() {
      int var4 = 0;
      String var5 = this.stripeCardId;
      int var1;
      if(var5 != null) {
         var1 = var5.hashCode();
      } else {
         var1 = 0;
      }

      var5 = this.lastFour;
      int var2;
      if(var5 != null) {
         var2 = var5.hashCode();
      } else {
         var2 = 0;
      }

      var5 = this.endDate;
      int var3;
      if(var5 != null) {
         var3 = var5.hashCode();
      } else {
         var3 = 0;
      }

      var5 = this.brand;
      if(var5 != null) {
         var4 = var5.hashCode();
      }

      return (var3 + (var2 + var1 * 31) * 31) * 31 + var4;
   }

   public String toString() {
      return "SavedStripeCard(stripeCardId=" + this.stripeCardId + ", lastFour=" + this.lastFour + ", endDate=" + this.endDate + ", brand=" + this.brand + ")";
   }

   public void writeToParcel(Parcel var1, int var2) {
      kotlin.d.b.l.b(var1, "dest");
      var1.writeString(this.stripeCardId);
      var1.writeString(this.lastFour);
      var1.writeString(this.endDate);
      var1.writeString(this.brand);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"},
      d2 = {"Lco/uk/getmondo/model/SavedStripeCard$Companion;", "", "()V", "CREATOR", "Landroid/os/Parcelable$Creator;", "Lco/uk/getmondo/model/SavedStripeCard;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var1) {
         this();
      }
   }
}
