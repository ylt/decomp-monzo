package co.uk.getmondo.d;

import io.realm.bb;

public class p implements io.realm.ab, bb {
   public static final String ID = "inbound_p2p_storage_static_id";
   private boolean enabled;
   private String id;
   private String ineligibilityReason;
   private int max;
   private int min;

   public p() {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

      this.a("inbound_p2p_storage_static_id");
   }

   public p(boolean var1, int var2, int var3, String var4) {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

      this.a("inbound_p2p_storage_static_id");
      this.a(var1);
      this.a(var2);
      this.b(var3);
      this.b(var4);
   }

   public int a() {
      return this.e();
   }

   public void a(int var1) {
      this.max = var1;
   }

   public void a(String var1) {
      this.id = var1;
   }

   public void a(boolean var1) {
      this.enabled = var1;
   }

   public int b() {
      return this.f();
   }

   public void b(int var1) {
      this.min = var1;
   }

   public void b(String var1) {
      this.ineligibilityReason = var1;
   }

   public String c() {
      return this.id;
   }

   public boolean d() {
      return this.enabled;
   }

   public int e() {
      return this.max;
   }

   public boolean equals(Object var1) {
      boolean var4 = true;
      boolean var3 = false;
      boolean var2;
      if(this == var1) {
         var2 = true;
      } else {
         var2 = var3;
         if(var1 != null) {
            var2 = var3;
            if(this.getClass() == var1.getClass()) {
               p var5 = (p)var1;
               var2 = var3;
               if(this.d() == var5.d()) {
                  var2 = var3;
                  if(this.e() == var5.e()) {
                     var2 = var3;
                     if(this.f() == var5.f()) {
                        if(this.g() != null) {
                           var2 = this.g().equals(var5.g());
                        } else {
                           var2 = var4;
                           if(var5.g() != null) {
                              var2 = false;
                           }
                        }
                     }
                  }
               }
            }
         }
      }

      return var2;
   }

   public int f() {
      return this.min;
   }

   public String g() {
      return this.ineligibilityReason;
   }

   public int hashCode() {
      int var2 = 0;
      byte var1;
      if(this.d()) {
         var1 = 1;
      } else {
         var1 = 0;
      }

      int var3 = this.e();
      int var4 = this.f();
      if(this.g() != null) {
         var2 = this.g().hashCode();
      }

      return ((var1 * 31 + var3) * 31 + var4) * 31 + var2;
   }
}
