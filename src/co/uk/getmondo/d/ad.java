package co.uk.getmondo.d;

import kotlin.Metadata;
import org.threeten.bp.LocalDateTime;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u001a\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\b\u0086\b\u0018\u00002\u00020\u0001B=\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u0003\u0012\u0006\u0010\f\u001a\u00020\u0003¢\u0006\u0002\u0010\rJ\t\u0010\u001b\u001a\u00020\u0003HÆ\u0003J\t\u0010\u001c\u001a\u00020\u0003HÆ\u0003J\t\u0010\u001d\u001a\u00020\u0006HÆ\u0003J\t\u0010\u001e\u001a\u00020\bHÆ\u0003J\t\u0010\u001f\u001a\u00020\nHÆ\u0003J\t\u0010 \u001a\u00020\u0003HÆ\u0003J\t\u0010!\u001a\u00020\u0003HÆ\u0003JO\u0010\"\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00062\b\b\u0002\u0010\u0007\u001a\u00020\b2\b\b\u0002\u0010\t\u001a\u00020\n2\b\b\u0002\u0010\u000b\u001a\u00020\u00032\b\b\u0002\u0010\f\u001a\u00020\u0003HÆ\u0001J\u0013\u0010#\u001a\u00020\b2\b\u0010$\u001a\u0004\u0018\u00010%HÖ\u0003J\t\u0010&\u001a\u00020'HÖ\u0001J\t\u0010(\u001a\u00020\u0003HÖ\u0001J\u0010\u0010)\u001a\u00020\u00002\u0006\u0010*\u001a\u00020\bH\u0016R\u0011\u0010\f\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0014\u0010\u0007\u001a\u00020\bX\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0014\u0010\u0005\u001a\u00020\u0006X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u0014\u0010\u0004\u001a\u00020\u0003X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u000fR\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u000fR\u0011\u0010\u000b\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u000fR\u0011\u0010\u0017\u001a\u00020\u00038F¢\u0006\u0006\u001a\u0004\b\u0018\u0010\u000fR\u0014\u0010\t\u001a\u00020\nX\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u001a¨\u0006+"},
   d2 = {"Lco/uk/getmondo/model/RetailAccount;", "Lco/uk/getmondo/model/Account;", "id", "", "description", "created", "Lorg/threeten/bp/LocalDateTime;", "cardActivated", "", "type", "Lco/uk/getmondo/model/Account$Type;", "sortCode", "accountNumber", "(Ljava/lang/String;Ljava/lang/String;Lorg/threeten/bp/LocalDateTime;ZLco/uk/getmondo/model/Account$Type;Ljava/lang/String;Ljava/lang/String;)V", "getAccountNumber", "()Ljava/lang/String;", "getCardActivated", "()Z", "getCreated", "()Lorg/threeten/bp/LocalDateTime;", "getDescription", "getId", "getSortCode", "sortCodeWithDashes", "getSortCodeWithDashes", "getType", "()Lco/uk/getmondo/model/Account$Type;", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "copy", "equals", "other", "", "hashCode", "", "toString", "withCardActivated", "isActivated", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ad implements a {
   private final String accountNumber;
   private final boolean cardActivated;
   private final LocalDateTime created;
   private final String description;
   private final String id;
   private final String sortCode;
   private final a.b type;

   public ad(String var1, String var2, LocalDateTime var3, boolean var4, a.b var5, String var6, String var7) {
      kotlin.d.b.l.b(var1, "id");
      kotlin.d.b.l.b(var2, "description");
      kotlin.d.b.l.b(var3, "created");
      kotlin.d.b.l.b(var5, "type");
      kotlin.d.b.l.b(var6, "sortCode");
      kotlin.d.b.l.b(var7, "accountNumber");
      super();
      this.id = var1;
      this.description = var2;
      this.created = var3;
      this.cardActivated = var4;
      this.type = var5;
      this.sortCode = var6;
      this.accountNumber = var7;
   }

   // $FF: synthetic method
   public static ad a(ad var0, String var1, String var2, LocalDateTime var3, boolean var4, a.b var5, String var6, String var7, int var8, Object var9) {
      if((var8 & 1) != 0) {
         var1 = var0.a();
      }

      if((var8 & 2) != 0) {
         var2 = var0.h();
      }

      if((var8 & 4) != 0) {
         var3 = var0.b();
      }

      if((var8 & 8) != 0) {
         var4 = var0.c();
      }

      if((var8 & 16) != 0) {
         var5 = var0.d();
      }

      if((var8 & 32) != 0) {
         var6 = var0.sortCode;
      }

      if((var8 & 64) != 0) {
         var7 = var0.accountNumber;
      }

      return var0.a(var1, var2, var3, var4, var5, var6, var7);
   }

   // $FF: synthetic method
   public a a(boolean var1) {
      return (a)this.b(var1);
   }

   public final ad a(String var1, String var2, LocalDateTime var3, boolean var4, a.b var5, String var6, String var7) {
      kotlin.d.b.l.b(var1, "id");
      kotlin.d.b.l.b(var2, "description");
      kotlin.d.b.l.b(var3, "created");
      kotlin.d.b.l.b(var5, "type");
      kotlin.d.b.l.b(var6, "sortCode");
      kotlin.d.b.l.b(var7, "accountNumber");
      return new ad(var1, var2, var3, var4, var5, var6, var7);
   }

   public String a() {
      return this.id;
   }

   public ad b(boolean var1) {
      return a(this, (String)null, (String)null, (LocalDateTime)null, var1, (a.b)null, (String)null, (String)null, 119, (Object)null);
   }

   public LocalDateTime b() {
      return this.created;
   }

   public boolean c() {
      return this.cardActivated;
   }

   public a.b d() {
      return this.type;
   }

   public boolean e() {
      return a.a.a(this);
   }

   public boolean equals(Object var1) {
      boolean var4 = false;
      boolean var3;
      if(this != var1) {
         var3 = var4;
         if(!(var1 instanceof ad)) {
            return var3;
         }

         ad var5 = (ad)var1;
         var3 = var4;
         if(!kotlin.d.b.l.a(this.a(), var5.a())) {
            return var3;
         }

         var3 = var4;
         if(!kotlin.d.b.l.a(this.h(), var5.h())) {
            return var3;
         }

         var3 = var4;
         if(!kotlin.d.b.l.a(this.b(), var5.b())) {
            return var3;
         }

         boolean var2;
         if(this.c() == var5.c()) {
            var2 = true;
         } else {
            var2 = false;
         }

         var3 = var4;
         if(!var2) {
            return var3;
         }

         var3 = var4;
         if(!kotlin.d.b.l.a(this.d(), var5.d())) {
            return var3;
         }

         var3 = var4;
         if(!kotlin.d.b.l.a(this.sortCode, var5.sortCode)) {
            return var3;
         }

         var3 = var4;
         if(!kotlin.d.b.l.a(this.accountNumber, var5.accountNumber)) {
            return var3;
         }
      }

      var3 = true;
      return var3;
   }

   public boolean f() {
      return a.a.b(this);
   }

   public final String g() {
      return co.uk.getmondo.common.k.o.a(this.sortCode);
   }

   public String h() {
      return this.description;
   }

   public int hashCode() {
      throw new RuntimeException("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.copyTypes(TypeTransformer.java:311)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.fixTypes(TypeTransformer.java:226)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:207)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:161)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:433)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:129)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:528)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:425)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:441)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:171)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:271)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:109)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:290)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:33)\n");
   }

   public final String i() {
      return this.sortCode;
   }

   public final String j() {
      return this.accountNumber;
   }

   public String toString() {
      return "RetailAccount(id=" + this.a() + ", description=" + this.h() + ", created=" + this.b() + ", cardActivated=" + this.c() + ", type=" + this.d() + ", sortCode=" + this.sortCode + ", accountNumber=" + this.accountNumber + ")";
   }
}
