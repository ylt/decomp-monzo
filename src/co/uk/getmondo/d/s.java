package co.uk.getmondo.d;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import co.uk.getmondo.api.model.Address;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.TypeCastException;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0011\n\u0002\b\u0005\n\u0002\u0010 \n\u0002\b\u0014\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0002\b\u0004\b\u0086\b\u0018\u0000 12\u00020\u0001:\u00011B\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004BA\u0012\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\u000e\u0010\u0007\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\b\u0012\b\u0010\t\u001a\u0004\u0018\u00010\u0006\u0012\b\u0010\n\u001a\u0004\u0018\u00010\u0006\u0012\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u0006¢\u0006\u0002\u0010\fJ\u000b\u0010\u001b\u001a\u0004\u0018\u00010\u0006HÆ\u0003J\u0016\u0010\u001c\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\bHÆ\u0003¢\u0006\u0002\u0010\u0019J\u000b\u0010\u001d\u001a\u0004\u0018\u00010\u0006HÆ\u0003J\u000b\u0010\u001e\u001a\u0004\u0018\u00010\u0006HÆ\u0003J\u000b\u0010\u001f\u001a\u0004\u0018\u00010\u0006HÆ\u0003JP\u0010 \u001a\u00020\u00002\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00062\u0010\b\u0002\u0010\u0007\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\b2\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u00062\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u00062\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u0006HÆ\u0001¢\u0006\u0002\u0010!J\b\u0010\"\u001a\u00020#H\u0016J\u0013\u0010$\u001a\u00020%2\b\u0010&\u001a\u0004\u0018\u00010'H\u0096\u0002J\b\u0010(\u001a\u00020\u0006H\u0016J\u0006\u0010)\u001a\u00020%J\u0006\u0010*\u001a\u00020%J\b\u0010+\u001a\u00020#H\u0016J\t\u0010,\u001a\u00020\u0006HÖ\u0001J\u0018\u0010-\u001a\u00020.2\u0006\u0010/\u001a\u00020\u00032\u0006\u00100\u001a\u00020#H\u0016R\u001a\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00060\u000e8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u000f\u0010\u0010R\u0016\u0010\u000b\u001a\u0004\u0018\u00010\u0006X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u0013\u0010\t\u001a\u0004\u0018\u00010\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0012R\u0013\u0010\n\u001a\u0004\u0018\u00010\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0012R\u0014\u0010\u0015\u001a\u00020\u00068VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0016\u0010\u0012R\u0016\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0012R\u001b\u0010\u0007\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\b¢\u0006\n\n\u0002\u0010\u001a\u001a\u0004\b\u0018\u0010\u0019¨\u00062"},
   d2 = {"Lco/uk/getmondo/model/LegacyAddress;", "Lco/uk/getmondo/api/model/Address;", "source", "Landroid/os/Parcel;", "(Landroid/os/Parcel;)V", "postalCode", "", "streetAddress", "", "city", "country", "administrativeArea", "(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "addressLines", "", "getAddressLines", "()Ljava/util/List;", "getAdministrativeArea", "()Ljava/lang/String;", "getCity", "getCountry", "countryCode", "getCountryCode", "getPostalCode", "getStreetAddress", "()[Ljava/lang/String;", "[Ljava/lang/String;", "component1", "component2", "component3", "component4", "component5", "copy", "(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lco/uk/getmondo/model/LegacyAddress;", "describeContents", "", "equals", "", "other", "", "formattedSingleLine", "hasCity", "hasStreetAddress", "hashCode", "toString", "writeToParcel", "", "dest", "flags", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class s implements Address {
   public static final Creator CREATOR = (Creator)(new Creator() {
      public s a(Parcel var1) {
         kotlin.d.b.l.b(var1, "source");
         return new s(var1);
      }

      public s[] a(int var1) {
         return new s[var1];
      }

      // $FF: synthetic method
      public Object createFromParcel(Parcel var1) {
         return this.a(var1);
      }

      // $FF: synthetic method
      public Object[] newArray(int var1) {
         return (Object[])this.a(var1);
      }
   });
   public static final s.a Companion = new s.a((kotlin.d.b.i)null);
   private final String administrativeArea;
   private final String city;
   private final String country;
   private final String postalCode;
   private final String[] streetAddress;

   public s(Parcel var1) {
      kotlin.d.b.l.b(var1, "source");
      this(var1.readString(), var1.createStringArray(), var1.readString(), var1.readString(), var1.readString());
   }

   public s(String var1, String[] var2, String var3, String var4, String var5) {
      this.postalCode = var1;
      this.streetAddress = var2;
      this.city = var3;
      this.country = var4;
      this.administrativeArea = var5;
   }

   // $FF: synthetic method
   public s(String var1, String[] var2, String var3, String var4, String var5, int var6, kotlin.d.b.i var7) {
      if((var6 & 1) != 0) {
         var1 = "";
      }

      if((var6 & 16) != 0) {
         var5 = (String)null;
      }

      this(var1, var2, var3, var4, var5);
   }

   public List a() {
      String[] var1 = this.streetAddress;
      List var2;
      if(var1 != null) {
         var2 = kotlin.a.g.h((Object[])var1);
         if(var2 != null) {
            return var2;
         }
      }

      var2 = kotlin.a.m.a();
      return var2;
   }

   public String b() {
      return this.administrativeArea;
   }

   public String c() {
      return this.postalCode;
   }

   public String d() {
      String var1 = this.country;
      if(var1 == null) {
         var1 = "";
      }

      return var1;
   }

   public int describeContents() {
      return 0;
   }

   public String e() {
      Iterable var3 = (Iterable)kotlin.a.m.b((Collection)this.a(), (Iterable)kotlin.a.m.c(new String[]{this.city, this.b(), this.d()}));
      Collection var2 = (Collection)(new ArrayList());
      Iterator var5 = var3.iterator();

      while(var5.hasNext()) {
         Object var4 = var5.next();
         boolean var1;
         if(!kotlin.h.j.a((CharSequence)((String)var4))) {
            var1 = true;
         } else {
            var1 = false;
         }

         if(var1) {
            var2.add(var4);
         }
      }

      return kotlin.a.m.a((Iterable)((List)var2), (CharSequence)", ", (CharSequence)null, (CharSequence)null, 0, (CharSequence)null, (kotlin.d.a.b)null, 62, (Object)null);
   }

   public boolean equals(Object var1) {
      boolean var2;
      if((s)this == var1) {
         var2 = true;
      } else {
         Class var3;
         if(var1 != null) {
            var3 = var1.getClass();
         } else {
            var3 = null;
         }

         if(kotlin.d.b.l.a(var3, this.getClass()) ^ true) {
            var2 = false;
         } else {
            if(var1 == null) {
               throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.model.LegacyAddress");
            }

            s var4 = (s)var1;
            if(kotlin.d.b.l.a(this.c(), ((s)var1).c()) ^ true) {
               var2 = false;
            } else if(!Arrays.equals((Object[])this.streetAddress, (Object[])((s)var1).streetAddress)) {
               var2 = false;
            } else if(kotlin.d.b.l.a(this.city, ((s)var1).city) ^ true) {
               var2 = false;
            } else if(kotlin.d.b.l.a(this.country, ((s)var1).country) ^ true) {
               var2 = false;
            } else if(kotlin.d.b.l.a(this.b(), ((s)var1).b()) ^ true) {
               var2 = false;
            } else {
               var2 = true;
            }
         }
      }

      return var2;
   }

   public final boolean f() {
      boolean var1;
      if(this.streetAddress != null) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public final boolean g() {
      CharSequence var3 = (CharSequence)this.city;
      boolean var1;
      if(var3 != null && var3.length() != 0) {
         var1 = false;
      } else {
         var1 = true;
      }

      boolean var2;
      if(!var1) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   public final String[] h() {
      return this.streetAddress;
   }

   public int hashCode() {
      int var5 = 0;
      String var6 = this.c();
      int var1;
      if(var6 != null) {
         var1 = var6.hashCode();
      } else {
         var1 = 0;
      }

      String[] var7 = this.streetAddress;
      int var2;
      if(var7 != null) {
         var2 = Arrays.hashCode((Object[])var7);
      } else {
         var2 = 0;
      }

      var6 = this.city;
      int var3;
      if(var6 != null) {
         var3 = var6.hashCode();
      } else {
         var3 = 0;
      }

      var6 = this.country;
      int var4;
      if(var6 != null) {
         var4 = var6.hashCode();
      } else {
         var4 = 0;
      }

      var6 = this.b();
      if(var6 != null) {
         var5 = var6.hashCode();
      }

      return (var4 + (var3 + (var2 + var1 * 31) * 31) * 31) * 31 + var5;
   }

   public final String i() {
      return this.city;
   }

   public final String j() {
      return this.country;
   }

   public String toString() {
      return "LegacyAddress(postalCode=" + this.c() + ", streetAddress=" + Arrays.toString(this.streetAddress) + ", city=" + this.city + ", country=" + this.country + ", administrativeArea=" + this.b() + ")";
   }

   public void writeToParcel(Parcel var1, int var2) {
      kotlin.d.b.l.b(var1, "dest");
      var1.writeString(this.c());
      var1.writeStringArray(this.streetAddress);
      var1.writeString(this.city);
      var1.writeString(this.country);
      var1.writeString(this.b());
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"},
      d2 = {"Lco/uk/getmondo/model/LegacyAddress$Companion;", "", "()V", "CREATOR", "Landroid/os/Parcelable$Creator;", "Lco/uk/getmondo/model/LegacyAddress;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var1) {
         this();
      }
   }
}
