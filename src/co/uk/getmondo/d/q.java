package co.uk.getmondo.d;

import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0007\b\u0086\u0001\u0018\u0000 \t2\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\tB\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\u0007j\u0002\b\b¨\u0006\n"},
   d2 = {"Lco/uk/getmondo/model/InboundPaymentIneligibilityReason;", "", "apiValue", "", "(Ljava/lang/String;ILjava/lang/String;)V", "getApiValue", "()Ljava/lang/String;", "NOT_VERIFIED", "OPTED_OUT", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public enum q {
   public static final q.a Companion;
   NOT_VERIFIED,
   OPTED_OUT;

   private final String apiValue;

   static {
      q var0 = new q("NOT_VERIFIED", 0, "not_verified");
      NOT_VERIFIED = var0;
      q var1 = new q("OPTED_OUT", 1, "p2p_opted_out");
      OPTED_OUT = var1;
      Companion = new q.a((kotlin.d.b.i)null);
   }

   protected q(String var3) {
      kotlin.d.b.l.b(var3, "apiValue");
      super(var1, var2);
      this.apiValue = var3;
   }

   public static final q a(String var0) {
      return Companion.a(var0);
   }

   public final String a() {
      return this.apiValue;
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0007¨\u0006\u0007"},
      d2 = {"Lco/uk/getmondo/model/InboundPaymentIneligibilityReason$Companion;", "", "()V", "from", "Lco/uk/getmondo/model/InboundPaymentIneligibilityReason;", "apiValue", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var1) {
         this();
      }

      public final q a(String var1) {
         Object[] var4 = (Object[])q.values();
         int var2 = 0;

         Object var5;
         while(true) {
            if(var2 >= var4.length) {
               var5 = null;
               break;
            }

            Object var3 = var4[var2];
            if(kotlin.d.b.l.a(((q)var3).a(), var1)) {
               var5 = var3;
               break;
            }

            ++var2;
         }

         return (q)var5;
      }
   }
}
