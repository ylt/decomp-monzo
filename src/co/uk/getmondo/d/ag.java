package co.uk.getmondo.d;

import io.realm.bb;
import io.realm.bk;

public class ag implements bb, bk {
   private String sddMigrationType;

   public ag() {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

   }

   public ag(String var1) {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

      this.a(var1);
   }

   public ah a() {
      return ah.a(this.b());
   }

   public void a(String var1) {
      this.sddMigrationType = var1;
   }

   public String b() {
      return this.sddMigrationType;
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(this != var1) {
         if(var1 != null && this.getClass() == var1.getClass()) {
            ag var3 = (ag)var1;
            if(this.b() != null) {
               var2 = this.b().equals(var3.b());
            } else if(var3.b() != null) {
               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public int hashCode() {
      int var1;
      if(this.b() != null) {
         var1 = this.b().hashCode();
      } else {
         var1 = 0;
      }

      return var1;
   }
}
