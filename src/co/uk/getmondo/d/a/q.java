package co.uk.getmondo.d.a;

import co.uk.getmondo.api.model.ApiAccount;
import co.uk.getmondo.d.ad;
import kotlin.Metadata;
import org.threeten.bp.LocalDateTime;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\u0006\u001a\u00020\u00022\u0006\u0010\u0007\u001a\u00020\bH\u0016¨\u0006\t"},
   d2 = {"Lco/uk/getmondo/model/mapper/RetailAccountMapper;", "Lco/uk/getmondo/model/mapper/AccountMapper;", "Lco/uk/getmondo/model/RetailAccount;", "cardActivated", "", "(Z)V", "apply", "apiValue", "Lco/uk/getmondo/api/model/ApiAccount;", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class q extends a {
   public q(boolean var1) {
      super(var1);
   }

   public ad a(ApiAccount var1) {
      kotlin.d.b.l.b(var1, "apiValue");
      String var5 = var1.a();
      String var3 = var1.c();
      LocalDateTime var7 = var1.b();
      boolean var2 = this.a();
      String var4 = var1.e();
      if(var4 == null) {
         kotlin.d.b.l.a();
      }

      String var6 = var1.d();
      if(var6 == null) {
         kotlin.d.b.l.a();
      }

      return new ad(var5, var3, var7, var2, co.uk.getmondo.d.a.b.Find.a(var1.f()), var6, var4);
   }
}
