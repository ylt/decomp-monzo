package co.uk.getmondo.d.a;

import co.uk.getmondo.api.model.ApiBalance;
import co.uk.getmondo.api.model.ApiLocalSpend;
import io.realm.az;
import io.realm.bb;
import java.util.Iterator;
import java.util.List;

public class d implements j {
   private final String accountId;

   public d(String var1) {
      this.accountId = var1;
   }

   private az a(List var1) {
      az var2 = new az();
      Iterator var4 = var1.iterator();

      while(var4.hasNext()) {
         ApiLocalSpend var3 = (ApiLocalSpend)var4.next();
         var2.a((bb)(new co.uk.getmondo.d.t(var3.b(), var3.a())));
      }

      return var2;
   }

   public co.uk.getmondo.d.b a(ApiBalance var1) {
      return new co.uk.getmondo.d.b(this.accountId, var1.a(), var1.c(), var1.b(), var1.c(), this.a(var1.d()));
   }
}
