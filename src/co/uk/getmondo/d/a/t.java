package co.uk.getmondo.d.a;

import co.uk.getmondo.api.model.feed.ApiAttachment;
import co.uk.getmondo.api.model.feed.ApiCounterparty;
import co.uk.getmondo.api.model.feed.ApiMetadata;
import co.uk.getmondo.api.model.feed.ApiTransaction;
import co.uk.getmondo.d.aa;
import co.uk.getmondo.d.aj;
import io.realm.az;
import io.realm.bb;
import java.util.Date;
import java.util.Iterator;

public class t {
   private final co.uk.getmondo.common.accounts.b accountManager;
   private final b attachmentMapper;
   private final co.uk.getmondo.feed.a declineReasonStringProvider;
   private final k merchantMapper;

   public t(co.uk.getmondo.feed.a var1, co.uk.getmondo.common.accounts.b var2) {
      this.declineReasonStringProvider = var1;
      this.accountManager = var2;
      this.merchantMapper = new k();
      this.attachmentMapper = new b();
   }

   private boolean a(String var1) {
      return co.uk.getmondo.common.k.p.c(var1);
   }

   private co.uk.getmondo.payments.send.data.a.a b(ApiTransaction var1) {
      ApiCounterparty var2 = var1.q();
      co.uk.getmondo.payments.send.data.a.a var3;
      if(this.accountManager.b() && var2 != null && var2.b() != null && var2.e() != null && var2.f() != null) {
         var3 = new co.uk.getmondo.payments.send.data.a.a(var2.b(), var2.e(), var2.f());
      } else {
         var3 = null;
      }

      return var3;
   }

   private aa c(ApiTransaction var1) {
      aa var2;
      if(var1.q() != null) {
         var2 = new aa(var1.q().d(), var1.q().b(), var1.q().c(), (String)null, (String)null, (String)null, false);
      } else {
         var2 = null;
      }

      return var2;
   }

   private az d(ApiTransaction var1) {
      az var2 = new az();
      if(var1.u() != null) {
         Iterator var4 = var1.u().iterator();

         while(var4.hasNext()) {
            ApiAttachment var3 = (ApiAttachment)var4.next();
            var2.a((bb)this.attachmentMapper.a(var3));
         }
      }

      return var2;
   }

   private co.uk.getmondo.d.u e(ApiTransaction var1) {
      co.uk.getmondo.d.u var2;
      if(var1.b()) {
         var2 = this.merchantMapper.a(var1.h());
      } else {
         var2 = null;
      }

      return var2;
   }

   private String f(ApiTransaction var1) {
      co.uk.getmondo.d.j var2 = co.uk.getmondo.d.j.a(var1.p());
      return this.declineReasonStringProvider.a(var2);
   }

   private String g(ApiTransaction var1) {
      String var2;
      if(var1.e() && var1.s().a() && !var1.s().b().isEmpty()) {
         var2 = var1.s().b();
      } else {
         var2 = null;
      }

      return var2;
   }

   private String h(ApiTransaction var1) {
      String var2;
      if(var1.q() != null) {
         if(var1.q().a()) {
            var2 = var1.q().b();
         } else {
            var2 = var1.q().c();
         }
      } else if(var1.b()) {
         var2 = var1.h().g();
      } else {
         var2 = var1.o();
      }

      return var2;
   }

   private boolean i(ApiTransaction var1) {
      boolean var3 = false;
      ApiMetadata var4 = null;
      if(var1.e()) {
         var4 = var1.s();
      }

      boolean var2;
      if(var4 != null) {
         if(var4.c() != null && var4.c().booleanValue()) {
            var2 = true;
         } else {
            var2 = false;
         }
      } else {
         var2 = false;
      }

      if(var2 || var1.f()) {
         var3 = true;
      }

      return var3;
   }

   private String j(ApiTransaction var1) {
      ApiMetadata var2 = var1.s();
      String var3;
      if(var2 != null) {
         var3 = var2.f();
      } else {
         var3 = null;
      }

      return var3;
   }

   public aj a(ApiTransaction var1) {
      String var17 = var1.g();
      long var9 = var1.i();
      String var22 = var1.m();
      long var11 = var1.j();
      String var16 = var1.n();
      Date var26 = var1.k();
      String var15 = co.uk.getmondo.common.c.a.d(var1.k());
      Date var18 = var1.l();
      String var23 = var1.o();
      String var19 = this.h(var1);
      String var21 = this.f(var1);
      String var25 = this.g(var1);
      boolean var8 = this.i(var1);
      boolean var5 = this.a(var1.r());
      co.uk.getmondo.d.u var24 = this.e(var1);
      String var14 = co.uk.getmondo.d.h.a(var1.t()).f();
      boolean var7 = var1.a();
      boolean var6 = var1.d();
      boolean var3 = var1.c();
      boolean var4 = var1.v();
      az var20 = this.d(var1);
      aa var13 = this.c(var1);
      boolean var2;
      if(var1.s() != null && co.uk.getmondo.common.k.p.c(var1.s().d())) {
         var2 = true;
      } else {
         var2 = false;
      }

      return new aj(var17, var9, var22, var11, var16, var26, var15, var18, var23, var19, var21, var25, var8, var5, var24, var14, var7, var6, var3, var4, var20, var13, var2, this.b(var1), var1.w(), this.j(var1));
   }
}
