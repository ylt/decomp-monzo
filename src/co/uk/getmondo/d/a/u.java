package co.uk.getmondo.d.a;

public final class u implements b.a.b {
   // $FF: synthetic field
   static final boolean $assertionsDisabled;
   private final javax.a.a accountManagerProvider;
   private final javax.a.a declineReasonStringProvider;

   static {
      boolean var0;
      if(!u.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      $assertionsDisabled = var0;
   }

   public u(javax.a.a var1, javax.a.a var2) {
      if(!$assertionsDisabled && var1 == null) {
         throw new AssertionError();
      } else {
         this.declineReasonStringProvider = var1;
         if(!$assertionsDisabled && var2 == null) {
            throw new AssertionError();
         } else {
            this.accountManagerProvider = var2;
         }
      }
   }

   public static b.a.b a(javax.a.a var0, javax.a.a var1) {
      return new u(var0, var1);
   }

   public t a() {
      return new t((co.uk.getmondo.feed.a)this.declineReasonStringProvider.b(), (co.uk.getmondo.common.accounts.b)this.accountManagerProvider.b());
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
