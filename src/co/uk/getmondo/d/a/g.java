package co.uk.getmondo.d.a;

public final class g implements b.a.b {
   // $FF: synthetic field
   static final boolean $assertionsDisabled;
   private final javax.a.a transactionMapperProvider;

   static {
      boolean var0;
      if(!g.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      $assertionsDisabled = var0;
   }

   public g(javax.a.a var1) {
      if(!$assertionsDisabled && var1 == null) {
         throw new AssertionError();
      } else {
         this.transactionMapperProvider = var1;
      }
   }

   public static b.a.b a(javax.a.a var0) {
      return new g(var0);
   }

   public f a() {
      return new f((t)this.transactionMapperProvider.b());
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
