package co.uk.getmondo.d.a;

import co.uk.getmondo.api.model.feed.ApiFeedItem;
import co.uk.getmondo.d.aj;
import org.threeten.bp.Duration;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.YearMonth;
import org.threeten.bp.temporal.TemporalAccessor;

public class f implements j {
   private final t transactionMapper;

   public f(t var1) {
      this.transactionMapper = var1;
   }

   private aj b(ApiFeedItem var1) {
      aj var2;
      if(var1.f() == null) {
         var2 = null;
      } else {
         var2 = this.transactionMapper.a(var1.f());
      }

      return var2;
   }

   private co.uk.getmondo.d.o c(ApiFeedItem var1) {
      co.uk.getmondo.d.o var2;
      if(var1.g() != null && !co.uk.getmondo.common.k.p.d(var1.g().a())) {
         var2 = new co.uk.getmondo.d.o(var1.g().a());
      } else {
         var2 = null;
      }

      return var2;
   }

   private co.uk.getmondo.d.v d(ApiFeedItem var1) {
      Object var3 = null;
      co.uk.getmondo.d.v var2 = (co.uk.getmondo.d.v)var3;
      if(var1.g() != null) {
         if(co.uk.getmondo.common.k.p.d(var1.g().f())) {
            var2 = (co.uk.getmondo.d.v)var3;
         } else {
            var2 = (co.uk.getmondo.d.v)var3;
            if(var1.g().f().equals("month")) {
               LocalDateTime var4 = var1.g().d();
               var4 = var4.c(Duration.a(var4, var1.g().e()).b() / 2L);
               var2 = new co.uk.getmondo.d.v(var1.c(), var1.g().c().longValue(), var1.g().b(), YearMonth.a((TemporalAccessor)var4).toString());
            }
         }
      }

      return var2;
   }

   private co.uk.getmondo.d.r e(ApiFeedItem var1) {
      co.uk.getmondo.d.r var2;
      if(var1.g() == null) {
         var2 = null;
      } else {
         var2 = new co.uk.getmondo.d.r(var1.c(), var1.g().g(), var1.g().h());
      }

      return var2;
   }

   private String f(ApiFeedItem var1) {
      String var2;
      if(var1.g() == null) {
         var2 = null;
      } else {
         var2 = var1.g().i();
      }

      return var2;
   }

   private co.uk.getmondo.d.f g(ApiFeedItem var1) {
      ApiFeedItem.Params var4 = var1.g();
      co.uk.getmondo.d.f var5;
      if(var4 == null) {
         var5 = null;
      } else {
         String var2;
         if(var4.j() != null) {
            var2 = var4.j();
         } else {
            var2 = "";
         }

         String var3;
         if(var4.k() != null) {
            var3 = var4.k();
         } else {
            var3 = "";
         }

         var5 = new co.uk.getmondo.d.f(var1.c(), var2, var3, var4.l());
      }

      return var5;
   }

   public co.uk.getmondo.d.m a(ApiFeedItem var1) {
      return new co.uk.getmondo.d.m(var1.a(), var1.b(), var1.e(), var1.d(), var1.h(), this.c(var1), this.d(var1), this.b(var1), this.e(var1), this.f(var1), this.g(var1), var1.i());
   }
}
