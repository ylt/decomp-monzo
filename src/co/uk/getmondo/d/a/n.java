package co.uk.getmondo.d.a;

import co.uk.getmondo.api.model.ApiPaymentLimit;
import kotlin.Metadata;

// $FF: synthetic class
@Metadata(
   bv = {1, 0, 2},
   k = 3,
   mv = {1, 1, 7}
)
public final class n {
   // $FF: synthetic field
   public static final int[] $EnumSwitchMapping$0 = new int[ApiPaymentLimit.LimitType.values().length];

   static {
      $EnumSwitchMapping$0[ApiPaymentLimit.LimitType.AMOUNT.ordinal()] = 1;
      $EnumSwitchMapping$0[ApiPaymentLimit.LimitType.COUNT.ordinal()] = 2;
      $EnumSwitchMapping$0[ApiPaymentLimit.LimitType.VIRTUAL.ordinal()] = 3;
   }
}
