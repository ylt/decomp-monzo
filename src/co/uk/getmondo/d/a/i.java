package co.uk.getmondo.d.a;

import co.uk.getmondo.api.model.LegacyApiAddress;
import java.util.Collection;
import java.util.List;
import kotlin.Metadata;
import kotlin.TypeCastException;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u0005¢\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0002H\u0016¨\u0006\u0007"},
   d2 = {"Lco/uk/getmondo/model/mapper/LegacyAddressMapper;", "Lco/uk/getmondo/model/mapper/Mapper;", "Lco/uk/getmondo/api/model/LegacyApiAddress;", "Lco/uk/getmondo/model/LegacyAddress;", "()V", "apply", "apiValue", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class i implements j {
   public co.uk.getmondo.d.s a(LegacyApiAddress var1) {
      kotlin.d.b.l.b(var1, "apiValue");
      String var3 = var1.c();
      List var2 = var1.a();
      String[] var7;
      if(var2 != null) {
         Collection var5 = (Collection)var2;
         if(var5 == null) {
            throw new TypeCastException("null cannot be cast to non-null type java.util.Collection<T>");
         }

         Object[] var6 = var5.toArray(new String[var5.size()]);
         if(var6 == null) {
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
         }

         var7 = (String[])var6;
      } else {
         var7 = null;
      }

      co.uk.getmondo.d.s var4 = new co.uk.getmondo.d.s(var3, var7, var1.b(), var1.d(), var1.e());
      return var4;
   }
}
