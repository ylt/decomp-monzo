package co.uk.getmondo.d.a;

import co.uk.getmondo.api.model.ApiWaitlistPosition;
import co.uk.getmondo.d.an;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class v implements j {
   private List a(List var1) {
      ArrayList var2 = new ArrayList(var1.size());
      Iterator var4 = var1.iterator();

      while(var4.hasNext()) {
         co.uk.getmondo.api.model.c var3 = (co.uk.getmondo.api.model.c)var4.next();
         var2.add(new co.uk.getmondo.d.k(var3.a(), var3.b(), var3.c(), var3.d()));
      }

      return var2;
   }

   public an a(ApiWaitlistPosition var1) {
      return new an(var1.a(), var1.b(), var1.c(), var1.d(), this.a(var1.e()), var1.f(), var1.g(), var1.h(), var1.i());
   }
}
