package co.uk.getmondo.d.a;

import co.uk.getmondo.api.model.ApiPaymentLimit;
import co.uk.getmondo.api.model.ApiPaymentLimits;
import co.uk.getmondo.api.model.ApiPaymentLimitsSection;
import co.uk.getmondo.d.x;
import co.uk.getmondo.d.y;
import co.uk.getmondo.d.z;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bÆ\u0002\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0002H\u0016J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0002¨\u0006\u000b"},
   d2 = {"Lco/uk/getmondo/model/mapper/PaymentLimitsMapper;", "Lco/uk/getmondo/model/mapper/Mapper;", "Lco/uk/getmondo/api/model/ApiPaymentLimits;", "Lco/uk/getmondo/model/PaymentLimits;", "()V", "apply", "apiValue", "mapLimit", "Lco/uk/getmondo/model/PaymentLimit;", "limit", "Lco/uk/getmondo/api/model/ApiPaymentLimit;", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class m implements j {
   public static final m INSTANCE;

   static {
      new m();
   }

   private m() {
      INSTANCE = (m)this;
   }

   private final x a(ApiPaymentLimit var1) {
      ApiPaymentLimit.LimitType var4 = var1.c();
      long var2;
      String var5;
      Long var6;
      x var8;
      String var11;
      switch(n.$EnumSwitchMapping$0[var4.ordinal()]) {
      case 1:
         var11 = var1.a();
         var5 = var1.b();
         var6 = var1.e();
         if(var6 == null) {
            kotlin.d.b.l.a();
         }

         var2 = var6.longValue();
         String var12 = var1.d();
         if(var12 == null) {
            kotlin.d.b.l.a();
         }

         co.uk.getmondo.d.c var13 = new co.uk.getmondo.d.c(var2, var12);
         Long var7 = var1.f();
         if(var7 == null) {
            kotlin.d.b.l.a();
         }

         var2 = var7.longValue();
         String var10 = var1.d();
         if(var10 == null) {
            kotlin.d.b.l.a();
         }

         var8 = (x)(new x.a(var11, var5, var13, new co.uk.getmondo.d.c(var2, var10)));
         break;
      case 2:
         var11 = var1.a();
         var5 = var1.b();
         var6 = var1.e();
         if(var6 == null) {
            kotlin.d.b.l.a();
         }

         var2 = var6.longValue();
         Long var9 = var1.f();
         if(var9 == null) {
            kotlin.d.b.l.a();
         }

         var8 = (x)(new x.b(var11, var5, var2, var9.longValue()));
         break;
      case 3:
         var8 = (x)(new x.c(var1.a(), var1.b()));
         break;
      default:
         throw new NoWhenBranchMatchedException();
      }

      return var8;
   }

   public y a(ApiPaymentLimits var1) {
      kotlin.d.b.l.b(var1, "apiValue");
      Iterable var2 = (Iterable)var1.a();
      Collection var8 = (Collection)(new ArrayList(kotlin.a.m.a(var2, 10)));
      Iterator var3 = var2.iterator();

      while(var3.hasNext()) {
         ApiPaymentLimitsSection var5 = (ApiPaymentLimitsSection)var3.next();
         String var9 = var5.a();
         String var4 = var5.b();
         Iterable var6 = (Iterable)var5.c();
         Collection var10 = (Collection)(new ArrayList(kotlin.a.m.a(var6, 10)));
         Iterator var11 = var6.iterator();

         while(var11.hasNext()) {
            ApiPaymentLimit var7 = (ApiPaymentLimit)var11.next();
            var10.add(INSTANCE.a(var7));
         }

         var8.add(new z(var9, var4, (List)var10));
      }

      return new y((List)var8);
   }
}
