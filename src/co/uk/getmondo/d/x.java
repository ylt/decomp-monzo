package co.uk.getmondo.d;

import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0003\u0004\u0005B\u0007\b\u0002¢\u0006\u0002\u0010\u0002\u0082\u0001\u0003\u0006\u0007\b¨\u0006\t"},
   d2 = {"Lco/uk/getmondo/model/PaymentLimit;", "", "()V", "AmountLimit", "CountLimit", "VirtualLimit", "Lco/uk/getmondo/model/PaymentLimit$AmountLimit;", "Lco/uk/getmondo/model/PaymentLimit$CountLimit;", "Lco/uk/getmondo/model/PaymentLimit$VirtualLimit;", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public abstract class x {
   private x() {
   }

   // $FF: synthetic method
   public x(kotlin.d.b.i var1) {
      this();
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0006¢\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0010\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0011\u001a\u00020\u0006HÆ\u0003J\t\u0010\u0012\u001a\u00020\u0006HÆ\u0003J1\u0010\u0013\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00062\b\b\u0002\u0010\u0007\u001a\u00020\u0006HÆ\u0001J\u0013\u0010\u0014\u001a\u00020\u00152\b\u0010\u0016\u001a\u0004\u0018\u00010\u0017HÖ\u0003J\t\u0010\u0018\u001a\u00020\u0019HÖ\u0001J\t\u0010\u001a\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\nR\u0011\u0010\u0007\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\f¨\u0006\u001b"},
      d2 = {"Lco/uk/getmondo/model/PaymentLimit$AmountLimit;", "Lco/uk/getmondo/model/PaymentLimit;", "id", "", "name", "limit", "Lco/uk/getmondo/model/Amount;", "used", "(Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/model/Amount;Lco/uk/getmondo/model/Amount;)V", "getId", "()Ljava/lang/String;", "getLimit", "()Lco/uk/getmondo/model/Amount;", "getName", "getUsed", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "", "hashCode", "", "toString", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a extends x {
      private final String id;
      private final c limit;
      private final String name;
      private final c used;

      public a(String var1, String var2, c var3, c var4) {
         kotlin.d.b.l.b(var1, "id");
         kotlin.d.b.l.b(var2, "name");
         kotlin.d.b.l.b(var3, "limit");
         kotlin.d.b.l.b(var4, "used");
         super((kotlin.d.b.i)null);
         this.id = var1;
         this.name = var2;
         this.limit = var3;
         this.used = var4;
      }

      public final String a() {
         return this.name;
      }

      public final c b() {
         return this.limit;
      }

      public final c c() {
         return this.used;
      }

      public boolean equals(Object var1) {
         boolean var2;
         if(this != var1) {
            label32: {
               if(var1 instanceof x.a) {
                  x.a var3 = (x.a)var1;
                  if(kotlin.d.b.l.a(this.id, var3.id) && kotlin.d.b.l.a(this.name, var3.name) && kotlin.d.b.l.a(this.limit, var3.limit) && kotlin.d.b.l.a(this.used, var3.used)) {
                     break label32;
                  }
               }

               var2 = false;
               return var2;
            }
         }

         var2 = true;
         return var2;
      }

      public int hashCode() {
         int var4 = 0;
         String var5 = this.id;
         int var1;
         if(var5 != null) {
            var1 = var5.hashCode();
         } else {
            var1 = 0;
         }

         var5 = this.name;
         int var2;
         if(var5 != null) {
            var2 = var5.hashCode();
         } else {
            var2 = 0;
         }

         c var6 = this.limit;
         int var3;
         if(var6 != null) {
            var3 = var6.hashCode();
         } else {
            var3 = 0;
         }

         var6 = this.used;
         if(var6 != null) {
            var4 = var6.hashCode();
         }

         return (var3 + (var2 + var1 * 31) * 31) * 31 + var4;
      }

      public String toString() {
         return "AmountLimit(id=" + this.id + ", name=" + this.name + ", limit=" + this.limit + ", used=" + this.used + ")";
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u000e\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0006¢\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0010\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0011\u001a\u00020\u0006HÆ\u0003J\t\u0010\u0012\u001a\u00020\u0006HÆ\u0003J1\u0010\u0013\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00062\b\b\u0002\u0010\u0007\u001a\u00020\u0006HÆ\u0001J\u0013\u0010\u0014\u001a\u00020\u00152\b\u0010\u0016\u001a\u0004\u0018\u00010\u0017HÖ\u0003J\t\u0010\u0018\u001a\u00020\u0019HÖ\u0001J\t\u0010\u001a\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\nR\u0011\u0010\u0007\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\f¨\u0006\u001b"},
      d2 = {"Lco/uk/getmondo/model/PaymentLimit$CountLimit;", "Lco/uk/getmondo/model/PaymentLimit;", "id", "", "name", "limit", "", "used", "(Ljava/lang/String;Ljava/lang/String;JJ)V", "getId", "()Ljava/lang/String;", "getLimit", "()J", "getName", "getUsed", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "", "hashCode", "", "toString", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class b extends x {
      private final String id;
      private final long limit;
      private final String name;
      private final long used;

      public b(String var1, String var2, long var3, long var5) {
         kotlin.d.b.l.b(var1, "id");
         kotlin.d.b.l.b(var2, "name");
         super((kotlin.d.b.i)null);
         this.id = var1;
         this.name = var2;
         this.limit = var3;
         this.used = var5;
      }

      public final String a() {
         return this.name;
      }

      public final long b() {
         return this.limit;
      }

      public final long c() {
         return this.used;
      }

      public boolean equals(Object var1) {
         boolean var4 = false;
         boolean var3;
         if(this != var1) {
            var3 = var4;
            if(!(var1 instanceof x.b)) {
               return var3;
            }

            x.b var5 = (x.b)var1;
            var3 = var4;
            if(!kotlin.d.b.l.a(this.id, var5.id)) {
               return var3;
            }

            var3 = var4;
            if(!kotlin.d.b.l.a(this.name, var5.name)) {
               return var3;
            }

            boolean var2;
            if(this.limit == var5.limit) {
               var2 = true;
            } else {
               var2 = false;
            }

            var3 = var4;
            if(!var2) {
               return var3;
            }

            if(this.used == var5.used) {
               var2 = true;
            } else {
               var2 = false;
            }

            var3 = var4;
            if(!var2) {
               return var3;
            }
         }

         var3 = true;
         return var3;
      }

      public int hashCode() {
         int var2 = 0;
         String var6 = this.id;
         int var1;
         if(var6 != null) {
            var1 = var6.hashCode();
         } else {
            var1 = 0;
         }

         var6 = this.name;
         if(var6 != null) {
            var2 = var6.hashCode();
         }

         long var4 = this.limit;
         int var3 = (int)(var4 ^ var4 >>> 32);
         var4 = this.used;
         return ((var1 * 31 + var2) * 31 + var3) * 31 + (int)(var4 ^ var4 >>> 32);
      }

      public String toString() {
         return "CountLimit(id=" + this.id + ", name=" + this.name + ", limit=" + this.limit + ", used=" + this.used + ")";
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003¢\u0006\u0002\u0010\u0005J\t\u0010\t\u001a\u00020\u0003HÆ\u0003J\t\u0010\n\u001a\u00020\u0003HÆ\u0003J\u001d\u0010\u000b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\f\u001a\u00020\r2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000fHÖ\u0003J\t\u0010\u0010\u001a\u00020\u0011HÖ\u0001J\t\u0010\u0012\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007R\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u0007¨\u0006\u0013"},
      d2 = {"Lco/uk/getmondo/model/PaymentLimit$VirtualLimit;", "Lco/uk/getmondo/model/PaymentLimit;", "id", "", "name", "(Ljava/lang/String;Ljava/lang/String;)V", "getId", "()Ljava/lang/String;", "getName", "component1", "component2", "copy", "equals", "", "other", "", "hashCode", "", "toString", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class c extends x {
      private final String id;
      private final String name;

      public c(String var1, String var2) {
         kotlin.d.b.l.b(var1, "id");
         kotlin.d.b.l.b(var2, "name");
         super((kotlin.d.b.i)null);
         this.id = var1;
         this.name = var2;
      }

      public final String a() {
         return this.name;
      }

      public boolean equals(Object var1) {
         boolean var2;
         if(this != var1) {
            label28: {
               if(var1 instanceof x.c) {
                  x.c var3 = (x.c)var1;
                  if(kotlin.d.b.l.a(this.id, var3.id) && kotlin.d.b.l.a(this.name, var3.name)) {
                     break label28;
                  }
               }

               var2 = false;
               return var2;
            }
         }

         var2 = true;
         return var2;
      }

      public int hashCode() {
         int var2 = 0;
         String var3 = this.id;
         int var1;
         if(var3 != null) {
            var1 = var3.hashCode();
         } else {
            var1 = 0;
         }

         var3 = this.name;
         if(var3 != null) {
            var2 = var3.hashCode();
         }

         return var1 * 31 + var2;
      }

      public String toString() {
         return "VirtualLimit(id=" + this.id + ", name=" + this.name + ")";
      }
   }
}
