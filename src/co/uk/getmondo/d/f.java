package co.uk.getmondo.d;

import io.realm.bc;
import kotlin.Metadata;
import kotlin.TypeCastException;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u000e\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\b\u0016\u0018\u00002\u00020\u0001B)\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\u0007B\u0005¢\u0006\u0002\u0010\bJ\u0013\u0010\u0011\u001a\u00020\u00122\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014H\u0096\u0002J\b\u0010\u0015\u001a\u00020\u0016H\u0016R\u0012\u0010\u0002\u001a\u00020\u00038\u0002@\u0002X\u0083\u000e¢\u0006\u0002\n\u0000R\u001c\u0010\u0006\u001a\u0004\u0018\u00010\u0003X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\t\u0010\n\"\u0004\b\u000b\u0010\fR\u001a\u0010\u0005\u001a\u00020\u0003X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\n\"\u0004\b\u000e\u0010\fR\u001a\u0010\u0004\u001a\u00020\u0003X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\n\"\u0004\b\u0010\u0010\f¨\u0006\u0017"},
   d2 = {"Lco/uk/getmondo/model/BasicItemInfo;", "Lio/realm/RealmObject;", "id", "", "title", "subtitle", "imageUrl", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "()V", "getImageUrl", "()Ljava/lang/String;", "setImageUrl", "(Ljava/lang/String;)V", "getSubtitle", "setSubtitle", "getTitle", "setTitle", "equals", "", "other", "", "hashCode", "", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public class f extends bc implements io.realm.i {
   private String id;
   private String imageUrl;
   private String subtitle;
   private String title;

   public f() {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

      this.a("");
      this.b("");
      this.c("");
   }

   public f(String var1, String var2, String var3, String var4) {
      kotlin.d.b.l.b(var1, "id");
      kotlin.d.b.l.b(var2, "title");
      kotlin.d.b.l.b(var3, "subtitle");
      this();
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

      this.a(var1);
      this.b(var2);
      this.c(var3);
      this.d(var4);
   }

   public final String a() {
      return this.e();
   }

   public void a(String var1) {
      this.id = var1;
   }

   public final String b() {
      return this.f();
   }

   public void b(String var1) {
      this.title = var1;
   }

   public final String c() {
      return this.g();
   }

   public void c(String var1) {
      this.subtitle = var1;
   }

   public String d() {
      return this.id;
   }

   public void d(String var1) {
      this.imageUrl = var1;
   }

   public String e() {
      return this.title;
   }

   public boolean equals(Object var1) {
      boolean var2;
      if((f)this == var1) {
         var2 = true;
      } else {
         Class var3;
         if(var1 != null) {
            var3 = var1.getClass();
         } else {
            var3 = null;
         }

         if(kotlin.d.b.l.a(var3, this.getClass()) ^ true) {
            var2 = false;
         } else {
            if(var1 == null) {
               throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.model.BasicItemInfo");
            }

            f var4 = (f)var1;
            if(kotlin.d.b.l.a(this.d(), ((f)var1).d()) ^ true) {
               var2 = false;
            } else if(kotlin.d.b.l.a(this.e(), ((f)var1).e()) ^ true) {
               var2 = false;
            } else if(kotlin.d.b.l.a(this.f(), ((f)var1).f()) ^ true) {
               var2 = false;
            } else if(kotlin.d.b.l.a(this.g(), ((f)var1).g()) ^ true) {
               var2 = false;
            } else {
               var2 = true;
            }
         }
      }

      return var2;
   }

   public String f() {
      return this.subtitle;
   }

   public String g() {
      return this.imageUrl;
   }

   public int hashCode() {
      int var2 = this.d().hashCode();
      int var3 = this.e().hashCode();
      int var4 = this.f().hashCode();
      String var5 = this.g();
      int var1;
      if(var5 != null) {
         var1 = var5.hashCode();
      } else {
         var1 = 0;
      }

      return var1 + ((var2 * 31 + var3) * 31 + var4) * 31;
   }
}
