package co.uk.getmondo.d;

import kotlin.Metadata;
import org.threeten.bp.LocalDateTime;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0015\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0006\b\u0086\b\u0018\u00002\u00020\u0001B7\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\b\u0010\t\u001a\u0004\u0018\u00010\n\u0012\u0006\u0010\u000b\u001a\u00020\b¢\u0006\u0002\u0010\fJ\t\u0010\u0017\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0018\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0019\u001a\u00020\u0006HÆ\u0003J\t\u0010\u001a\u001a\u00020\bHÆ\u0003J\u000b\u0010\u001b\u001a\u0004\u0018\u00010\nHÆ\u0003J\t\u0010\u001c\u001a\u00020\bHÆ\u0003JG\u0010\u001d\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00062\b\b\u0002\u0010\u0007\u001a\u00020\b2\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\n2\b\b\u0002\u0010\u000b\u001a\u00020\bHÆ\u0001J\u0013\u0010\u001e\u001a\u00020\b2\b\u0010\u001f\u001a\u0004\u0018\u00010 HÖ\u0003J\t\u0010!\u001a\u00020\"HÖ\u0001J\t\u0010#\u001a\u00020\u0003HÖ\u0001J\u0010\u0010$\u001a\u00020\u00012\u0006\u0010%\u001a\u00020\bH\u0016J\u000e\u0010&\u001a\u00020\u00012\u0006\u0010'\u001a\u00020\bR\u0014\u0010\u0007\u001a\u00020\bX\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0014\u0010\u0005\u001a\u00020\u0006X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0014\u0010\u0004\u001a\u00020\u0003X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0012R\u0011\u0010\u000b\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u000eR\u0016\u0010\t\u001a\u0004\u0018\u00010\nX\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016¨\u0006("},
   d2 = {"Lco/uk/getmondo/model/PrepaidAccount;", "Lco/uk/getmondo/model/Account;", "id", "", "description", "created", "Lorg/threeten/bp/LocalDateTime;", "cardActivated", "", "type", "Lco/uk/getmondo/model/Account$Type;", "initialTopupCompleted", "(Ljava/lang/String;Ljava/lang/String;Lorg/threeten/bp/LocalDateTime;ZLco/uk/getmondo/model/Account$Type;Z)V", "getCardActivated", "()Z", "getCreated", "()Lorg/threeten/bp/LocalDateTime;", "getDescription", "()Ljava/lang/String;", "getId", "getInitialTopupCompleted", "getType", "()Lco/uk/getmondo/model/Account$Type;", "component1", "component2", "component3", "component4", "component5", "component6", "copy", "equals", "other", "", "hashCode", "", "toString", "withCardActivated", "isActivated", "withInitialTopUpCompleted", "isCompleted", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ab implements a {
   private final boolean cardActivated;
   private final LocalDateTime created;
   private final String description;
   private final String id;
   private final boolean initialTopupCompleted;
   private final a.b type;

   public ab(String var1, String var2, LocalDateTime var3, boolean var4, a.b var5, boolean var6) {
      kotlin.d.b.l.b(var1, "id");
      kotlin.d.b.l.b(var2, "description");
      kotlin.d.b.l.b(var3, "created");
      super();
      this.id = var1;
      this.description = var2;
      this.created = var3;
      this.cardActivated = var4;
      this.type = var5;
      this.initialTopupCompleted = var6;
   }

   // $FF: synthetic method
   public static ab a(ab var0, String var1, String var2, LocalDateTime var3, boolean var4, a.b var5, boolean var6, int var7, Object var8) {
      if((var7 & 1) != 0) {
         var1 = var0.a();
      }

      if((var7 & 2) != 0) {
         var2 = var0.g();
      }

      if((var7 & 4) != 0) {
         var3 = var0.b();
      }

      if((var7 & 8) != 0) {
         var4 = var0.c();
      }

      if((var7 & 16) != 0) {
         var5 = var0.d();
      }

      if((var7 & 32) != 0) {
         var6 = var0.initialTopupCompleted;
      }

      return var0.a(var1, var2, var3, var4, var5, var6);
   }

   public a a(boolean var1) {
      return (a)a(this, (String)null, (String)null, (LocalDateTime)null, var1, (a.b)null, false, 55, (Object)null);
   }

   public final ab a(String var1, String var2, LocalDateTime var3, boolean var4, a.b var5, boolean var6) {
      kotlin.d.b.l.b(var1, "id");
      kotlin.d.b.l.b(var2, "description");
      kotlin.d.b.l.b(var3, "created");
      return new ab(var1, var2, var3, var4, var5, var6);
   }

   public String a() {
      return this.id;
   }

   public final a b(boolean var1) {
      return (a)a(this, (String)null, (String)null, (LocalDateTime)null, false, (a.b)null, var1, 31, (Object)null);
   }

   public LocalDateTime b() {
      return this.created;
   }

   public boolean c() {
      return this.cardActivated;
   }

   public a.b d() {
      return this.type;
   }

   public boolean e() {
      return a.a.a(this);
   }

   public boolean equals(Object var1) {
      boolean var4 = false;
      boolean var3;
      if(this != var1) {
         var3 = var4;
         if(!(var1 instanceof ab)) {
            return var3;
         }

         ab var5 = (ab)var1;
         var3 = var4;
         if(!kotlin.d.b.l.a(this.a(), var5.a())) {
            return var3;
         }

         var3 = var4;
         if(!kotlin.d.b.l.a(this.g(), var5.g())) {
            return var3;
         }

         var3 = var4;
         if(!kotlin.d.b.l.a(this.b(), var5.b())) {
            return var3;
         }

         boolean var2;
         if(this.c() == var5.c()) {
            var2 = true;
         } else {
            var2 = false;
         }

         var3 = var4;
         if(!var2) {
            return var3;
         }

         var3 = var4;
         if(!kotlin.d.b.l.a(this.d(), var5.d())) {
            return var3;
         }

         if(this.initialTopupCompleted == var5.initialTopupCompleted) {
            var2 = true;
         } else {
            var2 = false;
         }

         var3 = var4;
         if(!var2) {
            return var3;
         }
      }

      var3 = true;
      return var3;
   }

   public boolean f() {
      return a.a.b(this);
   }

   public String g() {
      return this.description;
   }

   public final boolean h() {
      return this.initialTopupCompleted;
   }

   public int hashCode() {
      throw new RuntimeException("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.copyTypes(TypeTransformer.java:311)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.fixTypes(TypeTransformer.java:226)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:207)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:161)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:433)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:129)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:528)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:425)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:441)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:171)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:271)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:109)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:290)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:33)\n");
   }

   public String toString() {
      return "PrepaidAccount(id=" + this.a() + ", description=" + this.g() + ", created=" + this.b() + ", cardActivated=" + this.c() + ", type=" + this.d() + ", initialTopupCompleted=" + this.initialTopupCompleted + ")";
   }
}
