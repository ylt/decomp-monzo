package co.uk.getmondo.d;

import co.uk.getmondo.api.model.VerificationType;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\bB\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B\u00ad\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0005\u0012\u0006\u0010\b\u001a\u00020\u0005\u0012\u0006\u0010\t\u001a\u00020\u0005\u0012\u0006\u0010\n\u001a\u00020\u0005\u0012\u0006\u0010\u000b\u001a\u00020\u0005\u0012\u0006\u0010\f\u001a\u00020\u0005\u0012\u0006\u0010\r\u001a\u00020\u0005\u0012\u0006\u0010\u000e\u001a\u00020\u0005\u0012\u0006\u0010\u000f\u001a\u00020\u0005\u0012\u0006\u0010\u0010\u001a\u00020\u0005\u0012\u0006\u0010\u0011\u001a\u00020\u0005\u0012\u0006\u0010\u0012\u001a\u00020\u0005\u0012\u0006\u0010\u0013\u001a\u00020\u0005\u0012\u0006\u0010\u0014\u001a\u00020\u0005\u0012\u0006\u0010\u0015\u001a\u00020\u0005\u0012\u0006\u0010\u0016\u001a\u00020\u0005\u0012\u0006\u0010\u0017\u001a\u00020\u0005\u0012\u0006\u0010\u0018\u001a\u00020\u0005¢\u0006\u0002\u0010\u0019J\t\u00101\u001a\u00020\u0003HÆ\u0003J\t\u00102\u001a\u00020\u0005HÆ\u0003J\t\u00103\u001a\u00020\u0005HÆ\u0003J\t\u00104\u001a\u00020\u0005HÆ\u0003J\t\u00105\u001a\u00020\u0005HÆ\u0003J\t\u00106\u001a\u00020\u0005HÆ\u0003J\t\u00107\u001a\u00020\u0005HÆ\u0003J\t\u00108\u001a\u00020\u0005HÆ\u0003J\t\u00109\u001a\u00020\u0005HÆ\u0003J\t\u0010:\u001a\u00020\u0005HÆ\u0003J\t\u0010;\u001a\u00020\u0005HÆ\u0003J\t\u0010<\u001a\u00020\u0005HÆ\u0003J\t\u0010=\u001a\u00020\u0005HÆ\u0003J\t\u0010>\u001a\u00020\u0005HÆ\u0003J\t\u0010?\u001a\u00020\u0005HÆ\u0003J\t\u0010@\u001a\u00020\u0005HÆ\u0003J\t\u0010A\u001a\u00020\u0005HÆ\u0003J\t\u0010B\u001a\u00020\u0005HÆ\u0003J\t\u0010C\u001a\u00020\u0005HÆ\u0003J\t\u0010D\u001a\u00020\u0005HÆ\u0003J\t\u0010E\u001a\u00020\u0005HÆ\u0003JÛ\u0001\u0010F\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00052\b\b\u0002\u0010\u0007\u001a\u00020\u00052\b\b\u0002\u0010\b\u001a\u00020\u00052\b\b\u0002\u0010\t\u001a\u00020\u00052\b\b\u0002\u0010\n\u001a\u00020\u00052\b\b\u0002\u0010\u000b\u001a\u00020\u00052\b\b\u0002\u0010\f\u001a\u00020\u00052\b\b\u0002\u0010\r\u001a\u00020\u00052\b\b\u0002\u0010\u000e\u001a\u00020\u00052\b\b\u0002\u0010\u000f\u001a\u00020\u00052\b\b\u0002\u0010\u0010\u001a\u00020\u00052\b\b\u0002\u0010\u0011\u001a\u00020\u00052\b\b\u0002\u0010\u0012\u001a\u00020\u00052\b\b\u0002\u0010\u0013\u001a\u00020\u00052\b\b\u0002\u0010\u0014\u001a\u00020\u00052\b\b\u0002\u0010\u0015\u001a\u00020\u00052\b\b\u0002\u0010\u0016\u001a\u00020\u00052\b\b\u0002\u0010\u0017\u001a\u00020\u00052\b\b\u0002\u0010\u0018\u001a\u00020\u0005HÆ\u0001J\u0013\u0010G\u001a\u00020H2\b\u0010I\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010J\u001a\u00020KHÖ\u0001J\t\u0010L\u001a\u00020MHÖ\u0001R\u0011\u0010\u0011\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u001bR\u0011\u0010\u0012\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u001bR\u0011\u0010\r\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u001bR\u0011\u0010\u000e\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u001bR\u0011\u0010\u000b\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010\u001bR\u0011\u0010\f\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b \u0010\u001bR\u0011\u0010\u0006\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b!\u0010\u001bR\u0011\u0010\u0007\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\"\u0010\u001bR\u0011\u0010\b\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b#\u0010\u001bR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b$\u0010\u001bR\u0011\u0010\t\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b%\u0010\u001bR\u0011\u0010\n\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b&\u0010\u001bR\u0011\u0010\u0017\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b'\u0010\u001bR\u0011\u0010\u0018\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b(\u0010\u001bR\u0011\u0010\u0016\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b)\u0010\u001bR\u0011\u0010\u0014\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b*\u0010\u001bR\u0011\u0010\u0015\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b+\u0010\u001bR\u0011\u0010\u0013\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b,\u0010\u001bR\u0011\u0010\u000f\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b-\u0010\u001bR\u0011\u0010\u0010\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b.\u0010\u001bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b/\u00100¨\u0006N"},
   d2 = {"Lco/uk/getmondo/model/BalanceLimit;", "", "verificationType", "Lco/uk/getmondo/api/model/VerificationType;", "maxSingleCardPayment", "Lco/uk/getmondo/model/Amount;", "maxBalance", "maxDailyTopUp", "maxDailyTopUpRemaining", "maxTopUpOver30Days", "maxTopUpOver30DaysRemaining", "maxAnnualTopUp", "maxAnnualTopUpRemaining", "dailyWithdrawalLimit", "dailyWithdrawalLimitRemaining", "rolling30DayLimit", "rolling30DayLimitRemaining", "annualWithdrawalLimit", "annualWithdrawalLimitRemaining", "paymentsMaxSinglePayment", "paymentsMaxReceivedOver30Days", "paymentsMaxReceivedOver30DaysRemaining", "monzoMeMaxSinglePayment", "monzoMeMaxReceivedOver30Days", "monzoMeMaxReceivedOver30DaysRemaining", "(Lco/uk/getmondo/api/model/VerificationType;Lco/uk/getmondo/model/Amount;Lco/uk/getmondo/model/Amount;Lco/uk/getmondo/model/Amount;Lco/uk/getmondo/model/Amount;Lco/uk/getmondo/model/Amount;Lco/uk/getmondo/model/Amount;Lco/uk/getmondo/model/Amount;Lco/uk/getmondo/model/Amount;Lco/uk/getmondo/model/Amount;Lco/uk/getmondo/model/Amount;Lco/uk/getmondo/model/Amount;Lco/uk/getmondo/model/Amount;Lco/uk/getmondo/model/Amount;Lco/uk/getmondo/model/Amount;Lco/uk/getmondo/model/Amount;Lco/uk/getmondo/model/Amount;Lco/uk/getmondo/model/Amount;Lco/uk/getmondo/model/Amount;Lco/uk/getmondo/model/Amount;Lco/uk/getmondo/model/Amount;)V", "getAnnualWithdrawalLimit", "()Lco/uk/getmondo/model/Amount;", "getAnnualWithdrawalLimitRemaining", "getDailyWithdrawalLimit", "getDailyWithdrawalLimitRemaining", "getMaxAnnualTopUp", "getMaxAnnualTopUpRemaining", "getMaxBalance", "getMaxDailyTopUp", "getMaxDailyTopUpRemaining", "getMaxSingleCardPayment", "getMaxTopUpOver30Days", "getMaxTopUpOver30DaysRemaining", "getMonzoMeMaxReceivedOver30Days", "getMonzoMeMaxReceivedOver30DaysRemaining", "getMonzoMeMaxSinglePayment", "getPaymentsMaxReceivedOver30Days", "getPaymentsMaxReceivedOver30DaysRemaining", "getPaymentsMaxSinglePayment", "getRolling30DayLimit", "getRolling30DayLimitRemaining", "getVerificationType", "()Lco/uk/getmondo/api/model/VerificationType;", "component1", "component10", "component11", "component12", "component13", "component14", "component15", "component16", "component17", "component18", "component19", "component2", "component20", "component21", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "", "other", "hashCode", "", "toString", "", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class e {
   private final c annualWithdrawalLimit;
   private final c annualWithdrawalLimitRemaining;
   private final c dailyWithdrawalLimit;
   private final c dailyWithdrawalLimitRemaining;
   private final c maxAnnualTopUp;
   private final c maxAnnualTopUpRemaining;
   private final c maxBalance;
   private final c maxDailyTopUp;
   private final c maxDailyTopUpRemaining;
   private final c maxSingleCardPayment;
   private final c maxTopUpOver30Days;
   private final c maxTopUpOver30DaysRemaining;
   private final c monzoMeMaxReceivedOver30Days;
   private final c monzoMeMaxReceivedOver30DaysRemaining;
   private final c monzoMeMaxSinglePayment;
   private final c paymentsMaxReceivedOver30Days;
   private final c paymentsMaxReceivedOver30DaysRemaining;
   private final c paymentsMaxSinglePayment;
   private final c rolling30DayLimit;
   private final c rolling30DayLimitRemaining;
   private final VerificationType verificationType;

   public e(VerificationType var1, c var2, c var3, c var4, c var5, c var6, c var7, c var8, c var9, c var10, c var11, c var12, c var13, c var14, c var15, c var16, c var17, c var18, c var19, c var20, c var21) {
      kotlin.d.b.l.b(var1, "verificationType");
      kotlin.d.b.l.b(var2, "maxSingleCardPayment");
      kotlin.d.b.l.b(var3, "maxBalance");
      kotlin.d.b.l.b(var4, "maxDailyTopUp");
      kotlin.d.b.l.b(var5, "maxDailyTopUpRemaining");
      kotlin.d.b.l.b(var6, "maxTopUpOver30Days");
      kotlin.d.b.l.b(var7, "maxTopUpOver30DaysRemaining");
      kotlin.d.b.l.b(var8, "maxAnnualTopUp");
      kotlin.d.b.l.b(var9, "maxAnnualTopUpRemaining");
      kotlin.d.b.l.b(var10, "dailyWithdrawalLimit");
      kotlin.d.b.l.b(var11, "dailyWithdrawalLimitRemaining");
      kotlin.d.b.l.b(var12, "rolling30DayLimit");
      kotlin.d.b.l.b(var13, "rolling30DayLimitRemaining");
      kotlin.d.b.l.b(var14, "annualWithdrawalLimit");
      kotlin.d.b.l.b(var15, "annualWithdrawalLimitRemaining");
      kotlin.d.b.l.b(var16, "paymentsMaxSinglePayment");
      kotlin.d.b.l.b(var17, "paymentsMaxReceivedOver30Days");
      kotlin.d.b.l.b(var18, "paymentsMaxReceivedOver30DaysRemaining");
      kotlin.d.b.l.b(var19, "monzoMeMaxSinglePayment");
      kotlin.d.b.l.b(var20, "monzoMeMaxReceivedOver30Days");
      kotlin.d.b.l.b(var21, "monzoMeMaxReceivedOver30DaysRemaining");
      super();
      this.verificationType = var1;
      this.maxSingleCardPayment = var2;
      this.maxBalance = var3;
      this.maxDailyTopUp = var4;
      this.maxDailyTopUpRemaining = var5;
      this.maxTopUpOver30Days = var6;
      this.maxTopUpOver30DaysRemaining = var7;
      this.maxAnnualTopUp = var8;
      this.maxAnnualTopUpRemaining = var9;
      this.dailyWithdrawalLimit = var10;
      this.dailyWithdrawalLimitRemaining = var11;
      this.rolling30DayLimit = var12;
      this.rolling30DayLimitRemaining = var13;
      this.annualWithdrawalLimit = var14;
      this.annualWithdrawalLimitRemaining = var15;
      this.paymentsMaxSinglePayment = var16;
      this.paymentsMaxReceivedOver30Days = var17;
      this.paymentsMaxReceivedOver30DaysRemaining = var18;
      this.monzoMeMaxSinglePayment = var19;
      this.monzoMeMaxReceivedOver30Days = var20;
      this.monzoMeMaxReceivedOver30DaysRemaining = var21;
   }

   public final VerificationType a() {
      return this.verificationType;
   }

   public final c b() {
      return this.maxSingleCardPayment;
   }

   public final c c() {
      return this.maxBalance;
   }

   public final c d() {
      return this.maxDailyTopUp;
   }

   public final c e() {
      return this.maxDailyTopUpRemaining;
   }

   public boolean equals(Object var1) {
      boolean var2;
      if(this != var1) {
         label66: {
            if(var1 instanceof e) {
               e var3 = (e)var1;
               if(kotlin.d.b.l.a(this.verificationType, var3.verificationType) && kotlin.d.b.l.a(this.maxSingleCardPayment, var3.maxSingleCardPayment) && kotlin.d.b.l.a(this.maxBalance, var3.maxBalance) && kotlin.d.b.l.a(this.maxDailyTopUp, var3.maxDailyTopUp) && kotlin.d.b.l.a(this.maxDailyTopUpRemaining, var3.maxDailyTopUpRemaining) && kotlin.d.b.l.a(this.maxTopUpOver30Days, var3.maxTopUpOver30Days) && kotlin.d.b.l.a(this.maxTopUpOver30DaysRemaining, var3.maxTopUpOver30DaysRemaining) && kotlin.d.b.l.a(this.maxAnnualTopUp, var3.maxAnnualTopUp) && kotlin.d.b.l.a(this.maxAnnualTopUpRemaining, var3.maxAnnualTopUpRemaining) && kotlin.d.b.l.a(this.dailyWithdrawalLimit, var3.dailyWithdrawalLimit) && kotlin.d.b.l.a(this.dailyWithdrawalLimitRemaining, var3.dailyWithdrawalLimitRemaining) && kotlin.d.b.l.a(this.rolling30DayLimit, var3.rolling30DayLimit) && kotlin.d.b.l.a(this.rolling30DayLimitRemaining, var3.rolling30DayLimitRemaining) && kotlin.d.b.l.a(this.annualWithdrawalLimit, var3.annualWithdrawalLimit) && kotlin.d.b.l.a(this.annualWithdrawalLimitRemaining, var3.annualWithdrawalLimitRemaining) && kotlin.d.b.l.a(this.paymentsMaxSinglePayment, var3.paymentsMaxSinglePayment) && kotlin.d.b.l.a(this.paymentsMaxReceivedOver30Days, var3.paymentsMaxReceivedOver30Days) && kotlin.d.b.l.a(this.paymentsMaxReceivedOver30DaysRemaining, var3.paymentsMaxReceivedOver30DaysRemaining) && kotlin.d.b.l.a(this.monzoMeMaxSinglePayment, var3.monzoMeMaxSinglePayment) && kotlin.d.b.l.a(this.monzoMeMaxReceivedOver30Days, var3.monzoMeMaxReceivedOver30Days) && kotlin.d.b.l.a(this.monzoMeMaxReceivedOver30DaysRemaining, var3.monzoMeMaxReceivedOver30DaysRemaining)) {
                  break label66;
               }
            }

            var2 = false;
            return var2;
         }
      }

      var2 = true;
      return var2;
   }

   public final c f() {
      return this.maxTopUpOver30Days;
   }

   public final c g() {
      return this.maxTopUpOver30DaysRemaining;
   }

   public final c h() {
      return this.maxAnnualTopUp;
   }

   public int hashCode() {
      int var21 = 0;
      VerificationType var22 = this.verificationType;
      int var1;
      if(var22 != null) {
         var1 = var22.hashCode();
      } else {
         var1 = 0;
      }

      c var23 = this.maxSingleCardPayment;
      int var2;
      if(var23 != null) {
         var2 = var23.hashCode();
      } else {
         var2 = 0;
      }

      var23 = this.maxBalance;
      int var3;
      if(var23 != null) {
         var3 = var23.hashCode();
      } else {
         var3 = 0;
      }

      var23 = this.maxDailyTopUp;
      int var4;
      if(var23 != null) {
         var4 = var23.hashCode();
      } else {
         var4 = 0;
      }

      var23 = this.maxDailyTopUpRemaining;
      int var5;
      if(var23 != null) {
         var5 = var23.hashCode();
      } else {
         var5 = 0;
      }

      var23 = this.maxTopUpOver30Days;
      int var6;
      if(var23 != null) {
         var6 = var23.hashCode();
      } else {
         var6 = 0;
      }

      var23 = this.maxTopUpOver30DaysRemaining;
      int var7;
      if(var23 != null) {
         var7 = var23.hashCode();
      } else {
         var7 = 0;
      }

      var23 = this.maxAnnualTopUp;
      int var8;
      if(var23 != null) {
         var8 = var23.hashCode();
      } else {
         var8 = 0;
      }

      var23 = this.maxAnnualTopUpRemaining;
      int var9;
      if(var23 != null) {
         var9 = var23.hashCode();
      } else {
         var9 = 0;
      }

      var23 = this.dailyWithdrawalLimit;
      int var10;
      if(var23 != null) {
         var10 = var23.hashCode();
      } else {
         var10 = 0;
      }

      var23 = this.dailyWithdrawalLimitRemaining;
      int var11;
      if(var23 != null) {
         var11 = var23.hashCode();
      } else {
         var11 = 0;
      }

      var23 = this.rolling30DayLimit;
      int var12;
      if(var23 != null) {
         var12 = var23.hashCode();
      } else {
         var12 = 0;
      }

      var23 = this.rolling30DayLimitRemaining;
      int var13;
      if(var23 != null) {
         var13 = var23.hashCode();
      } else {
         var13 = 0;
      }

      var23 = this.annualWithdrawalLimit;
      int var14;
      if(var23 != null) {
         var14 = var23.hashCode();
      } else {
         var14 = 0;
      }

      var23 = this.annualWithdrawalLimitRemaining;
      int var15;
      if(var23 != null) {
         var15 = var23.hashCode();
      } else {
         var15 = 0;
      }

      var23 = this.paymentsMaxSinglePayment;
      int var16;
      if(var23 != null) {
         var16 = var23.hashCode();
      } else {
         var16 = 0;
      }

      var23 = this.paymentsMaxReceivedOver30Days;
      int var17;
      if(var23 != null) {
         var17 = var23.hashCode();
      } else {
         var17 = 0;
      }

      var23 = this.paymentsMaxReceivedOver30DaysRemaining;
      int var18;
      if(var23 != null) {
         var18 = var23.hashCode();
      } else {
         var18 = 0;
      }

      var23 = this.monzoMeMaxSinglePayment;
      int var19;
      if(var23 != null) {
         var19 = var23.hashCode();
      } else {
         var19 = 0;
      }

      var23 = this.monzoMeMaxReceivedOver30Days;
      int var20;
      if(var23 != null) {
         var20 = var23.hashCode();
      } else {
         var20 = 0;
      }

      var23 = this.monzoMeMaxReceivedOver30DaysRemaining;
      if(var23 != null) {
         var21 = var23.hashCode();
      }

      return (var20 + (var19 + (var18 + (var17 + (var16 + (var15 + (var14 + (var13 + (var12 + (var11 + (var10 + (var9 + (var8 + (var7 + (var6 + (var5 + (var4 + (var3 + (var2 + var1 * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31 + var21;
   }

   public final c i() {
      return this.maxAnnualTopUpRemaining;
   }

   public final c j() {
      return this.dailyWithdrawalLimit;
   }

   public final c k() {
      return this.dailyWithdrawalLimitRemaining;
   }

   public final c l() {
      return this.rolling30DayLimit;
   }

   public final c m() {
      return this.rolling30DayLimitRemaining;
   }

   public final c n() {
      return this.annualWithdrawalLimit;
   }

   public final c o() {
      return this.annualWithdrawalLimitRemaining;
   }

   public final c p() {
      return this.paymentsMaxSinglePayment;
   }

   public final c q() {
      return this.paymentsMaxReceivedOver30Days;
   }

   public final c r() {
      return this.paymentsMaxReceivedOver30DaysRemaining;
   }

   public final c s() {
      return this.monzoMeMaxSinglePayment;
   }

   public final c t() {
      return this.monzoMeMaxReceivedOver30Days;
   }

   public String toString() {
      return "BalanceLimit(verificationType=" + this.verificationType + ", maxSingleCardPayment=" + this.maxSingleCardPayment + ", maxBalance=" + this.maxBalance + ", maxDailyTopUp=" + this.maxDailyTopUp + ", maxDailyTopUpRemaining=" + this.maxDailyTopUpRemaining + ", maxTopUpOver30Days=" + this.maxTopUpOver30Days + ", maxTopUpOver30DaysRemaining=" + this.maxTopUpOver30DaysRemaining + ", maxAnnualTopUp=" + this.maxAnnualTopUp + ", maxAnnualTopUpRemaining=" + this.maxAnnualTopUpRemaining + ", dailyWithdrawalLimit=" + this.dailyWithdrawalLimit + ", dailyWithdrawalLimitRemaining=" + this.dailyWithdrawalLimitRemaining + ", rolling30DayLimit=" + this.rolling30DayLimit + ", rolling30DayLimitRemaining=" + this.rolling30DayLimitRemaining + ", annualWithdrawalLimit=" + this.annualWithdrawalLimit + ", annualWithdrawalLimitRemaining=" + this.annualWithdrawalLimitRemaining + ", paymentsMaxSinglePayment=" + this.paymentsMaxSinglePayment + ", paymentsMaxReceivedOver30Days=" + this.paymentsMaxReceivedOver30Days + ", paymentsMaxReceivedOver30DaysRemaining=" + this.paymentsMaxReceivedOver30DaysRemaining + ", monzoMeMaxSinglePayment=" + this.monzoMeMaxSinglePayment + ", monzoMeMaxReceivedOver30Days=" + this.monzoMeMaxReceivedOver30Days + ", monzoMeMaxReceivedOver30DaysRemaining=" + this.monzoMeMaxReceivedOver30DaysRemaining + ")";
   }

   public final c u() {
      return this.monzoMeMaxReceivedOver30DaysRemaining;
   }
}
