package co.uk.getmondo.adjust;

import android.content.Context;
import android.content.SharedPreferences;

public class g {
   private SharedPreferences a;

   public g(Context var1) {
      this.a = var1.getSharedPreferences("adjust_storage", 0);
   }

   void a(boolean var1) {
      this.a.edit().putBoolean("KEY_TRACKED", var1).apply();
   }

   boolean a() {
      return this.a.getBoolean("KEY_TRACKED", false);
   }
}
