package co.uk.getmondo.adjust;

import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.os.Build.VERSION;
import co.uk.getmondo.api.AnalyticsApi;
import co.uk.getmondo.api.model.tracking.Impression;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import io.reactivex.u;
import io.reactivex.v;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import org.threeten.bp.LocalDateTime;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class a {
   private final Context a;
   private final u b;
   private final g c;
   private final AnalyticsApi d;
   private final AdjustApi e;

   a(Context var1, u var2, String var3, g var4, OkHttpClient var5, AnalyticsApi var6, HttpLoggingInterceptor var7) {
      this.a = var1;
      this.b = var2;
      this.c = var4;
      this.d = var6;
      OkHttpClient var8 = var5.newBuilder().addInterceptor(var7).build();
      this.e = (AdjustApi)(new Retrofit.Builder()).baseUrl(var3).addConverterFactory(GsonConverterFactory.create((new com.google.gson.g()).a())).client(var8).addCallAdapterFactory(RxJava2CallAdapterFactory.create()).build().create(AdjustApi.class);
   }

   // $FF: synthetic method
   static io.reactivex.d a(a var0, Map var1) throws Exception {
      Impression var2 = Impression.j((String)var1.get("gps_adid"));
      return var0.d.trackEvent(var2).b((io.reactivex.d)var0.e.trackSession(var1));
   }

   // $FF: synthetic method
   static Map a(a var0) throws Exception {
      Resources var2 = var0.a.getResources();
      android.support.v4.g.a var1 = new android.support.v4.g.a();
      var1.put("app_token", "fe40w1xej1ts");
      var1.put("environment", "production");
      var1.put("device_type", "phone");
      var1.put("device_manufacturer", Build.MANUFACTURER);
      var1.put("os_build", Build.ID);
      var1.put("api_level", String.valueOf(VERSION.SDK_INT));
      var1.put("os_name", "android");
      var1.put("display_width", String.valueOf(var2.getDisplayMetrics().widthPixels));
      var1.put("display_height", String.valueOf(var2.getDisplayMetrics().heightPixels));
      var1.put("package_name", var0.a.getPackageName());
      var1.put("app_version", var0.a.getPackageManager().getPackageInfo(var0.a.getPackageName(), 0).versionName);
      var1.put("country", Locale.getDefault().getCountry());
      var1.put("language", Locale.getDefault().getLanguage());
      var1.put("device_name", Build.DEVICE);
      var1.put("os_version", VERSION.RELEASE);
      String var4 = var0.d();
      if(var4 != null) {
         var1.put("fb_id", var4);
      }

      var1.put("needs_response_details", "0");
      var1.put("event_buffering_enabled", "0");
      var1.put("android_uuid", UUID.randomUUID().toString());
      var1.put("gps_adid", AdvertisingIdClient.getAdvertisingIdInfo(var0.a).getId());
      String var3 = LocalDateTime.a().toString();
      var1.put("created_at", var3);
      var1.put("sent_at", var3);
      return var1;
   }

   // $FF: synthetic method
   static void b() throws Exception {
      d.a.a.b("App install tracked with Adjust", new Object[0]);
   }

   // $FF: synthetic method
   static void b(a var0) throws Exception {
      var0.c.a(true);
   }

   private v c() {
      return v.c(f.a(this));
   }

   private String d() {
      // $FF: Couldn't be decompiled
   }

   public void a() {
      if(!co.uk.getmondo.a.b.booleanValue() && !this.c.a()) {
         this.c().c(b.a(this)).b(c.a(this)).b(this.b).a(d.b(), e.a());
      }

   }
}
