package co.uk.getmondo.adjust;

import java.util.concurrent.Callable;

// $FF: synthetic class
final class f implements Callable {
   private final a a;

   private f(a var1) {
      this.a = var1;
   }

   public static Callable a(a var0) {
      return new f(var0);
   }

   public Object call() {
      return a.a(this.a);
   }
}
