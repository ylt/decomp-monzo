package co.uk.getmondo.feed;

import android.content.Context;
import android.net.Uri;
import co.uk.getmondo.api.model.signup.SignupSource;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.common.k.p;
import co.uk.getmondo.d.aj;
import co.uk.getmondo.d.m;
import co.uk.getmondo.d.r;
import co.uk.getmondo.feed.welcome.WelcomeToMonzoActivity;
import co.uk.getmondo.golden_ticket.GoldenTicketActivity;
import co.uk.getmondo.main.EddLimitsActivity;
import co.uk.getmondo.signup.identity_verification.IdentityApprovedActivity;
import co.uk.getmondo.signup.identity_verification.IdentityVerificationActivity;
import co.uk.getmondo.signup.identity_verification.sdd.IdentityVerificationSddActivity;
import co.uk.getmondo.transaction.details.TransactionDetailsActivity;

public class c {
   private final Context a;

   c(Context var1) {
      this.a = var1;
   }

   public void a(m var1) {
      switch(null.a[var1.j().ordinal()]) {
      case 1:
         if(!((aj)var1.e().a()).t()) {
            this.a.startActivity(TransactionDetailsActivity.a(this.a, ((aj)var1.e().a()).w()));
         }
         break;
      case 2:
         this.a.startActivity(IdentityVerificationActivity.a(this.a, co.uk.getmondo.signup.identity_verification.a.j.a, Impression.KycFrom.FEED, SignupSource.LEGACY_PREPAID));
         break;
      case 3:
      case 4:
         IdentityApprovedActivity.a(this.a);
         break;
      case 5:
         GoldenTicketActivity.a(this.a, var1.f(), true);
         break;
      case 6:
         GoldenTicketActivity.a(this.a, var1.f(), false);
         break;
      case 7:
         this.a.startActivity(MonthlySpendingReportActivity.a(this.a, var1.g()));
         break;
      case 8:
         this.a.startActivity(EddLimitsActivity.a(this.a));
         break;
      case 9:
         r var3 = var1.h();
         String var2 = var3.b();
         String var5 = var2;
         if(p.d(var2)) {
            var5 = this.a.getString(var3.a().b());
         }

         this.a.startActivity(IdentityVerificationActivity.a(this.a, co.uk.getmondo.signup.identity_verification.a.j.a, Impression.KycFrom.FEED, SignupSource.LEGACY_PREPAID, co.uk.getmondo.signup.j.b, var5));
         break;
      case 10:
         this.a.startActivity(IdentityVerificationSddActivity.a(this.a, co.uk.getmondo.signup.identity_verification.sdd.j.a));
         break;
      case 11:
         this.a.startActivity(IdentityVerificationActivity.a(this.a, co.uk.getmondo.signup.identity_verification.a.j.b, Impression.KycFrom.FEED, SignupSource.SDD_MIGRATION));
         break;
      case 12:
         Uri var4 = var1.d();
         if(var4 != null && var4.getQueryParameter("url") != null) {
            this.a.startActivity(InternalWebActivity.a(this.a, var4.getQueryParameter("url"), var4.getQueryParameter("style")));
         }
         break;
      case 13:
         this.a.startActivity(WelcomeToMonzoActivity.a(this.a));
      }

   }
}
