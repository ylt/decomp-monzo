package co.uk.getmondo.feed;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import co.uk.getmondo.common.ui.AmountView;

public class MonthlySpendingReportActivity_ViewBinding implements Unbinder {
   private MonthlySpendingReportActivity a;
   private View b;

   public MonthlySpendingReportActivity_ViewBinding(final MonthlySpendingReportActivity var1, View var2) {
      this.a = var1;
      var1.amountView = (AmountView)Utils.findRequiredViewAsType(var2, 2131821100, "field 'amountView'", AmountView.class);
      var1.monthTextView = (TextView)Utils.findRequiredViewAsType(var2, 2131821101, "field 'monthTextView'", TextView.class);
      var2 = Utils.findRequiredView(var2, 2131821103, "field 'goToSpendingButton' and method 'onGoToSpendingButtonClicked'");
      var1.goToSpendingButton = (Button)Utils.castView(var2, 2131821103, "field 'goToSpendingButton'", Button.class);
      this.b = var2;
      var2.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View var1x) {
            var1.onGoToSpendingButtonClicked();
         }
      });
   }

   public void unbind() {
      MonthlySpendingReportActivity var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.amountView = null;
         var1.monthTextView = null;
         var1.goToSpendingButton = null;
         this.b.setOnClickListener((OnClickListener)null);
         this.b = null;
      }
   }
}
