package co.uk.getmondo.feed;

// $FF: synthetic class
final class i implements SpendingReportFeedbackDialogFragment.b {
   private final MonthlySpendingReportActivity a;

   private i(MonthlySpendingReportActivity var1) {
      this.a = var1;
   }

   public static SpendingReportFeedbackDialogFragment.b a(MonthlySpendingReportActivity var0) {
      return new i(var0);
   }

   public void a(SpendingReportFeedbackDialogFragment.a var1) {
      MonthlySpendingReportActivity.a(this.a, var1);
   }
}
