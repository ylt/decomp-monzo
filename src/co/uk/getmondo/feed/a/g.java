package co.uk.getmondo.feed.a;

import java.util.Date;
import java.util.List;

// $FF: synthetic class
final class g implements io.reactivex.c.h {
   private final String a;
   private final Date b;

   private g(String var1, Date var2) {
      this.a = var1;
      this.b = var2;
   }

   public static io.reactivex.c.h a(String var0, Date var1) {
      return new g(var0, var1);
   }

   public Object a(Object var1) {
      return d.a(this.a, this.b, (List)var1);
   }
}
