package co.uk.getmondo.feed.a;

import io.realm.av;
import io.realm.bb;
import io.realm.bf;
import io.realm.bg;
import io.realm.bl;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import kotlin.Metadata;
import kotlin.TypeCastException;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000d\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\b\u0005\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0001\u0018\u0000 %2\u00020\u0001:\u0001%B\u0007\b\u0007¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nJ\u0012\u0010\u000b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000e0\r0\fJ\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u000e0\fJ\u0010\u0010\u0010\u001a\u0004\u0018\u00010\u00112\u0006\u0010\u0012\u001a\u00020\nJ\u0018\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0018H\u0002J\u0014\u0010\u0013\u001a\u00020\b2\f\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u00180\u001aJ\u000e\u0010\u001b\u001a\u00020\b2\u0006\u0010\u001c\u001a\u00020\u000eJ<\u0010\u001d\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000e0\r0\f2\b\b\u0002\u0010\u001e\u001a\u00020\n2\u000e\b\u0002\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020!0 2\u000e\b\u0002\u0010\"\u001a\b\u0012\u0004\u0012\u00020#0 J \u0010$\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u001c\u001a\u00020\u000e2\u0006\u0010\u0012\u001a\u00020\nH\u0002R\u0013\u0010\u0003\u001a\u0004\u0018\u00010\u00048F¢\u0006\u0006\u001a\u0004\b\u0005\u0010\u0006¨\u0006&"},
   d2 = {"Lco/uk/getmondo/feed/data/FeedStorage;", "", "()V", "feedMetadata", "Lco/uk/getmondo/model/FeedMetadata;", "getFeedMetadata", "()Lco/uk/getmondo/model/FeedMetadata;", "deleteFeedItem", "Lio/reactivex/Completable;", "feedItemId", "", "feedItems", "Lio/reactivex/Observable;", "Lco/uk/getmondo/common/data/QueryResults;", "Lco/uk/getmondo/model/FeedItem;", "knowYourCustomerRequestFeedItem", "mostRecentItemDateWithAccountId", "Ljava/util/Date;", "accountId", "saveFeed", "", "realm", "Lio/realm/Realm;", "feed", "Lco/uk/getmondo/feed/data/model/GetFeedResult;", "feedResults", "", "saveFeedItem", "feedItem", "searchFeedItems", "searchTerm", "types", "", "Lco/uk/getmondo/feed/data/model/FeedItemType;", "categories", "Lco/uk/getmondo/model/Category;", "updateIfDifferent", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class r {
   public static final r.a a = new r.a((kotlin.d.b.i)null);

   private final void a(av var1, co.uk.getmondo.d.m var2, String var3) {
      co.uk.getmondo.d.m var4 = (co.uk.getmondo.d.m)var1.a(co.uk.getmondo.d.m.class).a("id", var2.a()).a("accountId", var3).h();
      if(var4 == null || kotlin.d.b.l.a(var2, (co.uk.getmondo.d.m)var1.e((bb)var4)) ^ true) {
         var1.d((bb)var2);
      }

   }

   private final void a(av var1, co.uk.getmondo.feed.a.a.b var2) {
      String[] var6;
      String var7;
      label36: {
         var6 = new String[var2.b().size()];
         var7 = var2.a();
         kotlin.f.a var8 = kotlin.f.d.a((kotlin.f.a)kotlin.a.m.a((Collection)var2.b()));
         int var3 = var8.a();
         int var4 = var8.b();
         int var5 = var8.c();
         if(var5 > 0) {
            if(var3 > var4) {
               break label36;
            }
         } else if(var3 < var4) {
            break label36;
         }

         while(true) {
            co.uk.getmondo.d.m var11 = (co.uk.getmondo.d.m)var2.b().get(var3);
            this.a(var1, var11, var7);
            var6[var3] = var11.a();
            if(var3 == var4) {
               break;
            }

            var3 += var5;
         }
      }

      if(var2.c() != null) {
         bf var9 = var1.a(co.uk.getmondo.d.m.class).a("accountId", var7).a("created", var2.c());
         boolean var10;
         if(((Object[])var6).length == 0) {
            var10 = true;
         } else {
            var10 = false;
         }

         if(!var10) {
            var10 = true;
         } else {
            var10 = false;
         }

         if(var10) {
            var9.d().a("id", var6);
         }

         var9.b("id", "overdraft_charges_item_id");
         var9.f().b();
      }

   }

   public final co.uk.getmondo.d.n a() {
      // $FF: Couldn't be decompiled
   }

   public final io.reactivex.b a(final co.uk.getmondo.d.m var1) {
      kotlin.d.b.l.b(var1, "feedItem");
      return co.uk.getmondo.common.j.g.a((av.a)(new av.a() {
         public final void a(av var1x) {
            r var4 = r.this;
            kotlin.d.b.l.a(var1x, "realm");
            co.uk.getmondo.d.m var3 = var1;
            String var2 = var1.b();
            kotlin.d.b.l.a(var2, "feedItem.accountId");
            var4.a(var1x, var3, var2);
         }
      }));
   }

   public final io.reactivex.b a(final List var1) {
      kotlin.d.b.l.b(var1, "feedResults");
      return co.uk.getmondo.common.j.g.a((av.a)(new av.a() {
         public final void a(av var1x) {
            Iterator var3 = var1.iterator();

            while(var3.hasNext()) {
               co.uk.getmondo.feed.a.a.b var2 = (co.uk.getmondo.feed.a.a.b)var3.next();
               r var4 = r.this;
               kotlin.d.b.l.a(var1x, "realm");
               var4.a(var1x, var2);
            }

            var1x.d((bb)(new co.uk.getmondo.d.n(new Date())));
         }
      }));
   }

   public final io.reactivex.n a(final String var1, final Set var2, final Set var3) {
      kotlin.d.b.l.b(var1, "searchTerm");
      kotlin.d.b.l.b(var2, "types");
      kotlin.d.b.l.b(var3, "categories");
      io.reactivex.n var4 = co.uk.getmondo.common.j.g.a((kotlin.d.a.b)(new kotlin.d.a.b() {
         public final bg a(av var1x) {
            kotlin.d.b.l.b(var1x, "realm");
            bf var6 = var1x.a(co.uk.getmondo.d.m.class);
            boolean var2x;
            if(!kotlin.h.j.a((CharSequence)var1)) {
               var2x = true;
            } else {
               var2x = false;
            }

            if(var2x) {
               var6.c("transaction.merchant.name", var1, io.realm.l.b).c().c("transaction.merchant.formattedAddress", var1, io.realm.l.b).c().c("transaction.merchant.emoji", var1, io.realm.l.b).c().c("transaction.peer.peerName", var1, io.realm.l.b).c().c("transaction.peer.contactName", var1, io.realm.l.b).c().c("transaction.peer.phoneNumber", var1, io.realm.l.b).c().c("transaction.declineReason", var1, io.realm.l.b).c().c("transaction.notes", var1, io.realm.l.b).c().c("transaction.description", var1, io.realm.l.b).c().c("transaction.createdDateFormatted", var1, io.realm.l.b).c().c("basicItemInfo.title", var1, io.realm.l.b).c().c("basicItemInfo.subtitle", var1, io.realm.l.b);
            }

            if(!((Collection)var2).isEmpty()) {
               var2x = true;
            } else {
               var2x = false;
            }

            bf var3x;
            Collection var4;
            Iterable var5;
            Object[] var7;
            Iterator var8;
            if(var2x) {
               var3x = var6.c();
               var5 = (Iterable)var2;
               var4 = (Collection)(new ArrayList(kotlin.a.m.a(var5, 10)));
               var8 = var5.iterator();

               while(var8.hasNext()) {
                  var4.add(((co.uk.getmondo.feed.a.a.a)var8.next()).a());
               }

               var4 = (Collection)((List)var4);
               var7 = var4.toArray(new String[var4.size()]);
               if(var7 == null) {
                  throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
               }

               var3x.a("type", (String[])var7);
            }

            if(!((Collection)var3).isEmpty()) {
               var2x = true;
            } else {
               var2x = false;
            }

            if(var2x) {
               var3x = var6.c();
               var5 = (Iterable)var3;
               var4 = (Collection)(new ArrayList(kotlin.a.m.a(var5, 10)));
               var8 = var5.iterator();

               while(var8.hasNext()) {
                  var4.add(((co.uk.getmondo.d.h)var8.next()).f());
               }

               var4 = (Collection)((List)var4);
               var7 = var4.toArray(new String[var4.size()]);
               if(var7 == null) {
                  throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
               }

               var3x.a("transaction.category", (String[])var7);
            }

            return var6.a("created", bl.b);
         }
      })).map((io.reactivex.c.h)null.a);
      kotlin.d.b.l.a(var4, "RxRealm.asObservable { r…}.map { it.queryResults }");
      return var4;
   }

   public final Date a(String param1) {
      // $FF: Couldn't be decompiled
   }

   public final io.reactivex.b b(final String var1) {
      kotlin.d.b.l.b(var1, "feedItemId");
      return co.uk.getmondo.common.j.g.a((av.a)(new av.a() {
         public final void a(av var1x) {
            var1x.a(co.uk.getmondo.d.m.class).a("id", var1).f().b();
         }
      }));
   }

   public final io.reactivex.n b() {
      io.reactivex.n var1 = co.uk.getmondo.common.j.g.a((kotlin.d.a.b)null.a).map((io.reactivex.c.h)null.a);
      kotlin.d.b.l.a(var1, "RxRealm.asObservable { r…}.map { it.queryResults }");
      return var1;
   }

   public final io.reactivex.n c() {
      io.reactivex.n var1 = co.uk.getmondo.common.j.g.a((kotlin.d.a.b)null.a).filter((io.reactivex.c.q)null.a).map((io.reactivex.c.h)null.a);
      kotlin.d.b.l.a(var1, "RxRealm.asObservable { r….copyFirstFromRealm()!! }");
      return var1;
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000¨\u0006\u0007"},
      d2 = {"Lco/uk/getmondo/feed/data/FeedStorage$Companion;", "", "()V", "FIELD_ACCOUNT_ID", "", "FIELD_CREATED", "FIELD_ID", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var1) {
         this();
      }
   }
}
