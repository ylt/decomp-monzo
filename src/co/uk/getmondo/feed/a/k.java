package co.uk.getmondo.feed.a;

import java.util.concurrent.Callable;

// $FF: synthetic class
final class k implements Callable {
   private final d a;
   private final String b;

   private k(d var1, String var2) {
      this.a = var1;
      this.b = var2;
   }

   public static Callable a(d var0, String var1) {
      return new k(var0, var1);
   }

   public Object call() {
      return d.b(this.a, this.b);
   }
}
