package co.uk.getmondo.feed.a;

public final class u implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;
   private final javax.a.a c;

   static {
      boolean var0;
      if(!u.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   public u(javax.a.a var1, javax.a.a var2) {
      if(!a && var1 == null) {
         throw new AssertionError();
      } else {
         this.b = var1;
         if(!a && var2 == null) {
            throw new AssertionError();
         } else {
            this.c = var2;
         }
      }
   }

   public static b.a.b a(javax.a.a var0, javax.a.a var1) {
      return new u(var0, var1);
   }

   public t a() {
      return new t((co.uk.getmondo.payments.send.a.e)this.b.b(), (v)this.c.b());
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
