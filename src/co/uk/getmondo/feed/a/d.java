package co.uk.getmondo.feed.a;

import co.uk.getmondo.api.MonzoApi;
import co.uk.getmondo.api.model.feed.ApiFeedResponse;
import co.uk.getmondo.d.aa;
import co.uk.getmondo.d.aj;
import io.reactivex.z;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class d {
   private final r a;
   private final MonzoApi b;
   private final co.uk.getmondo.d.a.f c;
   private final co.uk.getmondo.payments.send.payment_category.b d;
   private final a e;
   private final co.uk.getmondo.feed.search.a f;
   private final co.uk.getmondo.payments.send.a.e g;
   private final co.uk.getmondo.common.accounts.b h;
   private final io.reactivex.u i;

   public d(r var1, MonzoApi var2, co.uk.getmondo.d.a.f var3, co.uk.getmondo.payments.send.payment_category.b var4, a var5, co.uk.getmondo.feed.search.a var6, co.uk.getmondo.payments.send.a.e var7, co.uk.getmondo.common.accounts.b var8, io.reactivex.u var9) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
      this.d = var4;
      this.e = var5;
      this.f = var6;
      this.g = var7;
      this.h = var8;
      this.i = var9;
   }

   // $FF: synthetic method
   static co.uk.getmondo.feed.a.a.b a(String var0, Date var1, List var2) throws Exception {
      return new co.uk.getmondo.feed.a.a.b(var0, var2, var1);
   }

   // $FF: synthetic method
   static io.reactivex.r a(ApiFeedResponse var0) throws Exception {
      return io.reactivex.n.fromIterable(var0.a());
   }

   // $FF: synthetic method
   static io.reactivex.r a(d var0, String var1) throws Exception {
      String var3 = var1.trim();
      Set var4 = var0.e.a(var3);
      Set var2 = var0.f.a(var3);
      return var0.a.a(var3, var4, var2);
   }

   // $FF: synthetic method
   static List a(Map var0, List var1) throws Exception {
      Iterator var3 = var1.iterator();

      while(var3.hasNext()) {
         Iterator var2 = ((co.uk.getmondo.feed.a.a.b)var3.next()).b().iterator();

         while(var2.hasNext()) {
            ((co.uk.getmondo.d.m)var2.next()).e().a(h.a(var0));
         }
      }

      return var1;
   }

   // $FF: synthetic method
   static List a(Object[] var0) throws Exception {
      ArrayList var3 = new ArrayList(var0.length);
      int var2 = var0.length;

      for(int var1 = 0; var1 < var2; ++var1) {
         var3.add((co.uk.getmondo.feed.a.a.b)var0[var1]);
      }

      return var3;
   }

   // $FF: synthetic method
   static void a(Map var0, aj var1) {
      aa var2 = var1.B();
      if(var2 != null && var2.d() != null) {
         var2.a((co.uk.getmondo.payments.send.a.b)var0.get(var2.d()));
      }

   }

   // $FF: synthetic method
   static boolean a(d var0, co.uk.getmondo.d.m var1) {
      return var0.c(var1);
   }

   // $FF: synthetic method
   static z b(d var0, String var1) throws Exception {
      String var3 = null;
      Date var2 = var0.a.a(var1);
      if(var2 != null) {
         var2 = co.uk.getmondo.common.c.a.a(co.uk.getmondo.common.c.a.b(var2), co.uk.getmondo.common.c.a.a());
      } else {
         var2 = null;
      }

      if(var2 != null) {
         var3 = co.uk.getmondo.common.c.a.c(var2);
      }

      io.reactivex.n var4 = var0.b.feed(var1, var3).b(m.a());
      co.uk.getmondo.payments.send.payment_category.b var5 = var0.d;
      var5.getClass();
      var4 = var4.flatMapSingle(n.a(var5));
      co.uk.getmondo.d.a.f var6 = var0.c;
      var6.getClass();
      return var4.map(o.a(var6)).filter(p.a(var0)).filter(f.a(var0)).toList().d(g.a(var1, var2)).b(var0.i);
   }

   private boolean b(co.uk.getmondo.d.m var1) {
      boolean var2;
      if(!this.h.b() && var1.j() == co.uk.getmondo.feed.a.a.a.l) {
         var2 = false;
      } else {
         var2 = true;
      }

      return var2;
   }

   // $FF: synthetic method
   static boolean b(d var0, co.uk.getmondo.d.m var1) {
      return var0.b(var1);
   }

   private boolean c(co.uk.getmondo.d.m var1) {
      boolean var2;
      if(var1.j() == co.uk.getmondo.feed.a.a.a.n || !var1.e().b() && var1.j() == co.uk.getmondo.feed.a.a.a.a) {
         var2 = false;
      } else {
         var2 = true;
      }

      return var2;
   }

   private io.reactivex.v d(String var1) {
      return io.reactivex.v.a(k.a(this, var1));
   }

   public io.reactivex.b a(co.uk.getmondo.d.m var1) {
      return this.a.a(var1);
   }

   public io.reactivex.b a(List var1) {
      ArrayList var2 = new ArrayList();
      Iterator var3 = var1.iterator();

      while(var3.hasNext()) {
         var2.add(this.d((String)var3.next()));
      }

      io.reactivex.v var5 = io.reactivex.v.a(this.g.a().firstOrError(), io.reactivex.v.a((Iterable)var2, (io.reactivex.c.h)e.a()), i.a());
      r var4 = this.a;
      var4.getClass();
      return var5.c(j.a(var4));
   }

   public io.reactivex.n a() {
      return this.a.b();
   }

   public io.reactivex.n a(String var1) {
      return io.reactivex.n.defer(l.a(this, var1));
   }

   public io.reactivex.b b(String var1) {
      return this.a.b(var1).b((io.reactivex.d)this.b.deleteFeedItem(var1));
   }

   public io.reactivex.n b() {
      return this.a.c();
   }

   public co.uk.getmondo.d.n c() {
      return this.a.a();
   }

   public io.reactivex.b c(String var1) {
      return this.a.b(var1);
   }
}
