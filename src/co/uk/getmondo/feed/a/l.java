package co.uk.getmondo.feed.a;

import java.util.concurrent.Callable;

// $FF: synthetic class
final class l implements Callable {
   private final d a;
   private final String b;

   private l(d var1, String var2) {
      this.a = var1;
      this.b = var2;
   }

   public static Callable a(d var0, String var1) {
      return new l(var0, var1);
   }

   public Object call() {
      return d.a(this.a, this.b);
   }
}
