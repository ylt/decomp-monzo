package co.uk.getmondo.feed.a;

import co.uk.getmondo.d.aa;
import io.realm.av;
import java.util.Iterator;
import java.util.Map;
import kotlin.Metadata;
import kotlin.TypeCastException;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0007\b\u0007¢\u0006\u0002\u0010\u0002J\u001a\u0010\u0003\u001a\u00020\u00042\u0012\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\b0\u0006¨\u0006\t"},
   d2 = {"Lco/uk/getmondo/feed/data/PeerStorage;", "", "()V", "enrichPeers", "Lio/reactivex/Completable;", "contactsMap", "", "", "Lco/uk/getmondo/payments/send/contacts/Contact;", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class v {
   public final io.reactivex.b a(final Map var1) {
      kotlin.d.b.l.b(var1, "contactsMap");
      return co.uk.getmondo.common.j.g.a((av.a)(new av.a() {
         public final void a(av var1x) {
            Iterator var6 = ((Iterable)var1x.a(aa.class).f()).iterator();

            while(var6.hasNext()) {
               aa var3 = (aa)var6.next();
               CharSequence var4 = (CharSequence)var3.d();
               boolean var2;
               if(var4 != null && !kotlin.h.j.a(var4)) {
                  var2 = false;
               } else {
                  var2 = true;
               }

               if(!var2) {
                  Map var7 = var1;
                  String var5 = var3.d();
                  if(var7 == null) {
                     throw new TypeCastException("null cannot be cast to non-null type kotlin.collections.Map<K, V>");
                  }

                  var3.a((co.uk.getmondo.payments.send.a.b)var7.get(var5));
               } else {
                  d.a.a.a("Skipping enrichment of peer %s(phone: %s), phone number empty", new Object[]{var3.a(), var3.d()});
               }
            }

         }
      }));
   }
}
