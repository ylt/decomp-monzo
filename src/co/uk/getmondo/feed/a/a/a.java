package co.uk.getmondo.feed.a.a;

import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0013\b\u0086\u0001\u0018\u0000 \u00152\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u0015B\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\u0007j\u0002\b\bj\u0002\b\tj\u0002\b\nj\u0002\b\u000bj\u0002\b\fj\u0002\b\rj\u0002\b\u000ej\u0002\b\u000fj\u0002\b\u0010j\u0002\b\u0011j\u0002\b\u0012j\u0002\b\u0013j\u0002\b\u0014¨\u0006\u0016"},
   d2 = {"Lco/uk/getmondo/feed/data/model/FeedItemType;", "", "type", "", "(Ljava/lang/String;ILjava/lang/String;)V", "getType", "()Ljava/lang/String;", "TRANSACTION", "KYC_REQUEST", "KYC_PASSED", "KYC_REJECTED", "KYC_SDD_REQUEST", "KYC_SDD_APPROVED", "KYC_SDD_REJECTED", "GOLDEN_TICKET_AWARDED", "GOLDEN_TICKET_SUBSEQUENT", "MONTHLY_SPENDING_REPORT", "EDD_LIMITS", "WELCOME_FEED_ITEM", "BASIC_TITLE_AND_BODY", "UNKNOWN", "Find", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public enum a {
   a,
   b,
   c,
   d,
   e,
   f,
   g,
   h,
   i,
   j,
   k,
   l,
   m,
   n;

   public static final a.a o;
   private final String q;

   static {
      a var1 = new a("TRANSACTION", 0, "transaction");
      a = var1;
      a var6 = new a("KYC_REQUEST", 1, "kyc_request");
      b = var6;
      a var7 = new a("KYC_PASSED", 2, "kyc_passed");
      c = var7;
      a var5 = new a("KYC_REJECTED", 3, "kyc_rejected");
      d = var5;
      a var12 = new a("KYC_SDD_REQUEST", 4, "sdd_migration_request");
      e = var12;
      a var13 = new a("KYC_SDD_APPROVED", 5, "sdd_migration_approved");
      f = var13;
      a var4 = new a("KYC_SDD_REJECTED", 6, "sdd_migration_rejected");
      g = var4;
      a var2 = new a("GOLDEN_TICKET_AWARDED", 7, "golden_ticket_awarded");
      h = var2;
      a var9 = new a("GOLDEN_TICKET_SUBSEQUENT", 8, "golden_ticket_subsequent_award");
      i = var9;
      a var8 = new a("MONTHLY_SPENDING_REPORT", 9, "spending_report");
      j = var8;
      a var11 = new a("EDD_LIMITS", 10, "kyc_enhanced_limits");
      k = var11;
      a var10 = new a("WELCOME_FEED_ITEM", 11, "onboarding_welcome");
      l = var10;
      a var0 = new a("BASIC_TITLE_AND_BODY", 12, "basic_title_and_body");
      m = var0;
      a var3 = new a("UNKNOWN", 13, "unknown");
      n = var3;
      o = new a.a((i)null);
   }

   protected a(String var3) {
      l.b(var3, "type");
      super(var1, var2);
      this.q = var3;
   }

   public final String a() {
      return this.q;
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\u0007"},
      d2 = {"Lco/uk/getmondo/feed/data/model/FeedItemType$Find;", "", "()V", "fromApiType", "Lco/uk/getmondo/feed/data/model/FeedItemType;", "type", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(i var1) {
         this();
      }

      public final a a(String var1) {
         l.b(var1, "type");
         Object[] var4 = (Object[])a.values();
         int var2 = 0;

         Object var5;
         while(true) {
            if(var2 >= var4.length) {
               var5 = null;
               break;
            }

            Object var3 = var4[var2];
            if(l.a(((a)var3).a(), var1)) {
               var5 = var3;
               break;
            }

            ++var2;
         }

         a var6 = (a)var5;
         if(var6 == null) {
            var6 = a.n;
         }

         return var6;
      }
   }
}
