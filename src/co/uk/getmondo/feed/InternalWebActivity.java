package co.uk.getmondo.feed;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.MailTo;
import android.net.Uri;
import android.net.Uri.Builder;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\u0018\u0000 \u000f2\u00020\u0001:\u0002\u000f\u0010B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0002J\b\u0010\u0007\u001a\u00020\bH\u0016J\u0012\u0010\t\u001a\u00020\b2\b\u0010\n\u001a\u0004\u0018\u00010\u000bH\u0015J\u0018\u0010\f\u001a\u00020\u00062\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\r\u001a\u00020\u000eH\u0002¨\u0006\u0011"},
   d2 = {"Lco/uk/getmondo/feed/InternalWebActivity;", "Landroid/app/Activity;", "()V", "handleUri", "", "uri", "Landroid/net/Uri;", "onBackPressed", "", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "uriWithoutParam", "paramKey", "", "Companion", "CustomWebViewClient", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class InternalWebActivity extends Activity {
   public static final InternalWebActivity.a a = new InternalWebActivity.a((kotlin.d.b.i)null);
   private HashMap b;

   public static final Intent a(Context var0, String var1, String var2) {
      l.b(var0, "context");
      l.b(var1, "url");
      return a.a(var0, var1, var2);
   }

   private final Uri a(Uri var1, String var2) {
      Set var4 = var1.getQueryParameterNames();
      Builder var3 = var1.buildUpon().clearQuery();
      Iterable var5 = (Iterable)var4;
      Collection var9 = (Collection)(new ArrayList());
      Iterator var11 = var5.iterator();

      while(var11.hasNext()) {
         Object var6 = var11.next();
         if(l.a((String)var6, var2) ^ true) {
            var9.add(var6);
         }
      }

      Iterable var7 = (Iterable)((List)var9);
      Iterator var8 = var7.iterator();

      while(var8.hasNext()) {
         String var10 = (String)var8.next();
         var3.appendQueryParameter(var10, var1.getQueryParameter(var10));
      }

      var1 = var3.build();
      l.a(var1, "newUri.build()");
      return var1;
   }

   private final boolean a(Uri var1) {
      boolean var2 = true;
      if(MailTo.isMailTo(var1.toString())) {
         this.startActivity(new Intent("android.intent.action.SENDTO", var1));
      } else {
         if(var1.isHierarchical()) {
            String var3 = var1.getQueryParameter("monzo_action");
            if(var3 != null) {
               switch(var3.hashCode()) {
               case -1820761141:
                  if(var3.equals("external")) {
                     String var4 = this.a(var1, "monzo_action").toString();
                     l.a(var4, "uriWithoutParam(uri, MONZO_ACTION).toString()");
                     co.uk.getmondo.common.activities.a.a(this, var4, 2131689487, true);
                     return var2;
                  }
                  break;
               case 3015911:
                  if(var3.equals("back")) {
                     this.finish();
                     return var2;
                  }
               }
            }
         }

         var2 = false;
      }

      return var2;
   }

   public View a(int var1) {
      if(this.b == null) {
         this.b = new HashMap();
      }

      View var3 = (View)this.b.get(Integer.valueOf(var1));
      View var2 = var3;
      if(var3 == null) {
         var2 = this.findViewById(var1);
         this.b.put(Integer.valueOf(var1), var2);
      }

      return var2;
   }

   public void onBackPressed() {
      if(((WebView)this.a(co.uk.getmondo.c.a.webView)).canGoBack()) {
         ((WebView)this.a(co.uk.getmondo.c.a.webView)).goBack();
      } else {
         super.onBackPressed();
      }

   }

   @SuppressLint({"SetJavaScriptEnabled"})
   protected void onCreate(Bundle var1) {
      super.onCreate(var1);
      int var2;
      if(this.getIntent().getBooleanExtra("EXTRA_USE_DARK_THEME", false)) {
         var2 = 2131493156;
      } else {
         var2 = 2131493159;
      }

      this.setTheme(var2);
      this.setContentView(2131034181);
      ((WebView)this.a(co.uk.getmondo.c.a.webView)).getSettings().setJavaScriptEnabled(true);
      ((WebView)this.a(co.uk.getmondo.c.a.webView)).getSettings().setAllowFileAccess(false);
      ((WebView)this.a(co.uk.getmondo.c.a.webView)).setWebViewClient((WebViewClient)(new InternalWebActivity.b()));
      ((Button)this.a(co.uk.getmondo.c.a.retryButton)).setOnClickListener((OnClickListener)(new OnClickListener() {
         public final void onClick(View var1) {
            ((WebView)InternalWebActivity.this.a(co.uk.getmondo.c.a.webView)).reload();
         }
      }));
      String var4 = this.getIntent().getStringExtra("EXTRA_URL");
      Uri var3 = Uri.parse(var4);
      if(l.a(var3.getQueryParameter("monzo_action"), "external")) {
         l.a(var3, "uri");
         var4 = this.a(var3, "monzo_action").toString();
         l.a(var4, "uriWithoutParam(uri, MONZO_ACTION).toString()");
         co.uk.getmondo.common.activities.a.a(this, var4, 2131689487, true);
         this.finish();
      } else {
         ((WebView)this.a(co.uk.getmondo.c.a.webView)).loadUrl(var4);
      }

   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\"\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u00042\b\u0010\u000f\u001a\u0004\u0018\u00010\u0004H\u0007R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000¨\u0006\u0010"},
      d2 = {"Lco/uk/getmondo/feed/InternalWebActivity$Companion;", "", "()V", "DARK_THEME_VALUE", "", "EXTRA_URL", "EXTRA_USE_DARK_THEME", "MONZO_ACTION", "MONZO_ACTION_BACK", "MONZO_ACTION_EXTERNAL", "buildIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "url", "style", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var1) {
         this();
      }

      public final Intent a(Context var1, String var2, String var3) {
         l.b(var1, "context");
         l.b(var2, "url");
         Intent var4 = new Intent(var1, InternalWebActivity.class);
         var4.putExtra("EXTRA_URL", var2);
         var4.putExtra("EXTRA_USE_DARK_THEME", l.a(var3, "dark"));
         return var4;
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0004\b\u0086\u0004\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0018\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0016J\"\u0010\u000b\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\b\u0010\f\u001a\u0004\u0018\u00010\rH\u0016J(\u0010\u000e\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\n2\u0006\u0010\u0012\u001a\u00020\nH\u0016J\u0018\u0010\u0013\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u000e¢\u0006\u0002\n\u0000¨\u0006\u0014"},
      d2 = {"Lco/uk/getmondo/feed/InternalWebActivity$CustomWebViewClient;", "Landroid/webkit/WebViewClient;", "(Lco/uk/getmondo/feed/InternalWebActivity;)V", "errorReceived", "", "onPageFinished", "", "view", "Landroid/webkit/WebView;", "url", "", "onPageStarted", "favicon", "Landroid/graphics/Bitmap;", "onReceivedError", "errorCode", "", "description", "failingUrl", "shouldOverrideUrlLoading", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public final class b extends WebViewClient {
      private boolean b;

      public void onPageFinished(WebView var1, String var2) {
         byte var4 = 0;
         l.b(var1, "view");
         l.b(var2, "url");
         var1 = (WebView)InternalWebActivity.this.a(co.uk.getmondo.c.a.webView);
         byte var3;
         if(this.b) {
            var3 = 8;
         } else {
            var3 = 0;
         }

         var1.setVisibility(var3);
         LinearLayout var5 = (LinearLayout)InternalWebActivity.this.a(co.uk.getmondo.c.a.errorLayout);
         if(this.b) {
            var3 = var4;
         } else {
            var3 = 8;
         }

         var5.setVisibility(var3);
         ((ProgressBar)InternalWebActivity.this.a(co.uk.getmondo.c.a.progress)).setVisibility(8);
      }

      public void onPageStarted(WebView var1, String var2, Bitmap var3) {
         l.b(var1, "view");
         l.b(var2, "url");
         this.b = false;
         ((ProgressBar)InternalWebActivity.this.a(co.uk.getmondo.c.a.progress)).setVisibility(0);
         ((WebView)InternalWebActivity.this.a(co.uk.getmondo.c.a.webView)).setVisibility(8);
         ((LinearLayout)InternalWebActivity.this.a(co.uk.getmondo.c.a.errorLayout)).setVisibility(8);
      }

      public void onReceivedError(WebView var1, int var2, String var3, String var4) {
         l.b(var1, "view");
         l.b(var3, "description");
         l.b(var4, "failingUrl");
         this.b = true;
      }

      public boolean shouldOverrideUrlLoading(WebView var1, String var2) {
         l.b(var1, "view");
         l.b(var2, "url");
         InternalWebActivity var3 = InternalWebActivity.this;
         Uri var4 = Uri.parse(var2);
         l.a(var4, "Uri.parse(url)");
         return var3.a(var4);
      }
   }
}
