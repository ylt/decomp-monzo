package co.uk.getmondo.feed.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v7.widget.RecyclerView.w;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import co.uk.getmondo.common.ae;
import co.uk.getmondo.common.k.o;
import co.uk.getmondo.common.ui.AmountView;
import co.uk.getmondo.d.aj;
import co.uk.getmondo.d.m;
import co.uk.getmondo.d.u;
import com.bumptech.glide.g;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.d.b.i;
import kotlin.d.b.l;
import kotlin.h.j;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0000\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0006J\u000e\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000b\u001a\u00020\fJ\u001a\u0010\u000f\u001a\u00020\u000e2\b\u0010\u0010\u001a\u0004\u0018\u00010\u00112\u0006\u0010\u0012\u001a\u00020\u0011H\u0002R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0082.¢\u0006\u0002\n\u0000R\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0013"},
   d2 = {"Lco/uk/getmondo/feed/adapter/TransactionViewHolder;", "Landroid/support/v7/widget/RecyclerView$ViewHolder;", "view", "Landroid/view/View;", "listener", "Lco/uk/getmondo/feed/adapter/FeedAdapter$OnFeedItemActionListener;", "(Landroid/view/View;Lco/uk/getmondo/feed/adapter/FeedAdapter$OnFeedItemActionListener;)V", "amountFormatter", "Lco/uk/getmondo/common/money/AmountFormatter;", "avatarGenerator", "Lco/uk/getmondo/common/ui/AvatarGenerator;", "feedItem", "Lco/uk/getmondo/model/FeedItem;", "bind", "", "bindP2pFeedItem", "photoUrl", "", "name", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class f extends w {
   private final co.uk.getmondo.common.i.b a;
   private final co.uk.getmondo.common.ui.a b;
   private m c;
   private final a.a d;

   public f(View var1, a.a var2) {
      l.b(var1, "view");
      super(var1);
      this.d = var2;
      this.a = new co.uk.getmondo.common.i.b(true, false, false, 6, (i)null);
      co.uk.getmondo.common.ui.a.a var4 = co.uk.getmondo.common.ui.a.a;
      Context var3 = var1.getContext();
      l.a(var3, "view.context");
      this.b = var4.a(var3);
      var1.setOnClickListener((OnClickListener)(new OnClickListener() {
         public final void onClick(View var1) {
            if(f.this.d == null) {
               d.a.a.a("Need to set OnFeedItemActionListener", new Object[0]);
            }

            a.a var2 = f.this.d;
            if(var2 != null) {
               var2.a(f.b(f.this));
            }

         }
      }));
      ((ImageView)this.itemView.findViewById(co.uk.getmondo.c.a.overflowButton)).setOnClickListener((OnClickListener)(new OnClickListener() {
         public final void onClick(View var1) {
            Context var2 = var1.getContext();
            l.a(var2, "anchorView.context");
            l.a(var1, "anchorView");
            m var4 = f.b(f.this);
            a.a var3 = f.this.d;
            if(var3 == null) {
               l.a();
            }

            (new c(var2, var1, var4, var3)).show();
         }
      }));
   }

   private final void a(String var1, String var2) {
      CharSequence var4 = (CharSequence)var1;
      boolean var3;
      if(var4 != null && !j.a(var4)) {
         var3 = false;
      } else {
         var3 = true;
      }

      if(!var3) {
         g.b(((ImageView)this.itemView.findViewById(co.uk.getmondo.c.a.merchantIconView)).getContext()).a(Uri.parse(var1)).h().a(com.bumptech.glide.load.engine.b.b).a().a((com.bumptech.glide.g.b.j)((com.bumptech.glide.g.b.j)(new co.uk.getmondo.common.ui.c((ImageView)this.itemView.findViewById(co.uk.getmondo.c.a.merchantIconView)))));
      } else {
         ((ImageView)this.itemView.findViewById(co.uk.getmondo.c.a.merchantIconView)).setImageDrawable(co.uk.getmondo.common.ui.a.b.a(this.b.a(var2), 0, (Typeface)null, false, 7, (Object)null));
      }

      ((ImageView)this.itemView.findViewById(co.uk.getmondo.c.a.merchantIconView)).setBackgroundResource(0);
      ((ImageView)this.itemView.findViewById(co.uk.getmondo.c.a.merchantIconView)).setClipToOutline(false);
      ((TextView)this.itemView.findViewById(co.uk.getmondo.c.a.transactionDescriptionTextView)).setText((CharSequence)var2);
   }

   // $FF: synthetic method
   public static final m b(f var0) {
      m var1 = var0.c;
      if(var1 == null) {
         l.b("feedItem");
      }

      return var1;
   }

   public final void a(m var1) {
      l.b(var1, "feedItem");
      this.c = var1;
      if(var1.e().d()) {
         d.a.a.a((Throwable)(new IllegalArgumentException("Trying to bind a FeedItem in TransactionViewHolder, but there's no Transaction! Feed item type: " + var1.j() + " id: " + var1.a())));
      }

      aj var4 = (aj)var1.e().a();
      co.uk.getmondo.payments.send.data.a.a var3;
      String var7;
      if(var4.s()) {
         label104: {
            var3 = var4.C();
            if(var3 != null) {
               var7 = var3.a();
               if(var7 != null) {
                  break label104;
               }
            }

            var7 = var4.x();
         }

         co.uk.getmondo.common.ui.a var5 = this.b;
         l.a(var7, "title");
         Drawable var9 = co.uk.getmondo.common.ui.a.b.a(var5.a(var7), 0, (Typeface)null, true, 3, (Object)null);
         ((ImageView)this.itemView.findViewById(co.uk.getmondo.c.a.merchantIconView)).setImageDrawable(var9);
         ((TextView)this.itemView.findViewById(co.uk.getmondo.c.a.transactionDescriptionTextView)).setText((CharSequence)var7);
      } else if(var4.r()) {
         if(j.a((CharSequence)var4.B().b())) {
            var3 = var4.C();
            if(var3 != null) {
               var7 = var3.b();
            } else {
               var7 = null;
            }

            if(var7 == null) {
               l.a();
            }

            var7 = o.a(var7);
            StringBuilder var10 = (new StringBuilder()).append("").append(var7).append(" • ");
            var3 = var4.C();
            if(var3 != null) {
               var7 = var3.c();
            } else {
               var7 = null;
            }

            var7 = var10.append(var7).toString();
         } else {
            var7 = var4.B().b();
            l.a(var7, "transaction.peer.name");
         }

         ((ImageView)this.itemView.findViewById(co.uk.getmondo.c.a.merchantIconView)).setImageDrawable(co.uk.getmondo.common.ui.a.b.a(this.b.a(var7), 0, (Typeface)null, false, 7, (Object)null));
         ((TextView)this.itemView.findViewById(co.uk.getmondo.c.a.transactionDescriptionTextView)).setText((CharSequence)var7);
      } else if(var4.t()) {
         ((TextView)this.itemView.findViewById(co.uk.getmondo.c.a.transactionDescriptionTextView)).setText((CharSequence)var4.x());
         ((ImageView)this.itemView.findViewById(co.uk.getmondo.c.a.merchantIconView)).setImageResource(2130837982);
      } else if(var4.q()) {
         String var11 = var4.B().g();
         var7 = var4.B().b();
         l.a(var7, "transaction.peer.name");
         this.a(var11, var7);
      } else {
         if(var4.n()) {
            com.bumptech.glide.j var12 = g.b(((ImageView)this.itemView.findViewById(co.uk.getmondo.c.a.merchantIconView)).getContext());
            u var15 = var4.f();
            if(var15 == null) {
               l.a();
            }

            var12.a(var15.j()).a().a(var4.c().c()).c().a((ImageView)this.itemView.findViewById(co.uk.getmondo.c.a.merchantIconView));
         } else if(var4.z()) {
            ((ImageView)this.itemView.findViewById(co.uk.getmondo.c.a.merchantIconView)).setImageResource(2130837850);
         } else {
            ((ImageView)this.itemView.findViewById(co.uk.getmondo.c.a.merchantIconView)).setImageResource(var4.c().c());
         }

         ((TextView)this.itemView.findViewById(co.uk.getmondo.c.a.transactionDescriptionTextView)).setText((CharSequence)var4.x());
      }

      if(var4.r()) {
         ((ImageView)this.itemView.findViewById(co.uk.getmondo.c.a.merchantIconView)).setClipToOutline(false);
         ((ImageView)this.itemView.findViewById(co.uk.getmondo.c.a.merchantIconView)).setBackgroundResource(0);
      } else {
         ((ImageView)this.itemView.findViewById(co.uk.getmondo.c.a.merchantIconView)).setClipToOutline(true);
         ((ImageView)this.itemView.findViewById(co.uk.getmondo.c.a.merchantIconView)).setBackgroundResource(2130838008);
      }

      AmountView var13;
      co.uk.getmondo.d.c var16;
      if(var4.t()) {
         var13 = (AmountView)this.itemView.findViewById(co.uk.getmondo.c.a.amountView);
         var16 = var4.g();
         l.a(var16, "transaction.amount");
         var13.a(2131493049, var16);
      } else {
         var13 = (AmountView)this.itemView.findViewById(co.uk.getmondo.c.a.amountView);
         var16 = var4.g();
         l.a(var16, "transaction.amount");
         var13.a(2131493048, var16);
      }

      if(var4.d().b()) {
         ((TextView)this.itemView.findViewById(co.uk.getmondo.c.a.declinedView)).setText((CharSequence)var4.d().a());
         ae.a((View)((TextView)this.itemView.findViewById(co.uk.getmondo.c.a.declinedView)));
      } else {
         ae.b((TextView)this.itemView.findViewById(co.uk.getmondo.c.a.declinedView));
      }

      if(var4.m() && ((TextView)this.itemView.findViewById(co.uk.getmondo.c.a.declinedView)).getVisibility() != 0) {
         ((TextView)this.itemView.findViewById(co.uk.getmondo.c.a.notesView)).setText((CharSequence)var4.e());
         ae.a((View)((TextView)this.itemView.findViewById(co.uk.getmondo.c.a.notesView)));
      } else {
         ae.b((TextView)this.itemView.findViewById(co.uk.getmondo.c.a.notesView));
      }

      if(var4.y()) {
         ae.b((LinearLayout)this.itemView.findViewById(co.uk.getmondo.c.a.amountContainer));
      } else {
         ae.a((View)((LinearLayout)this.itemView.findViewById(co.uk.getmondo.c.a.amountContainer)));
      }

      if(var4.i()) {
         ae.a((View)((TextView)this.itemView.findViewById(co.uk.getmondo.c.a.localAmountView)));
         TextView var14 = (TextView)this.itemView.findViewById(co.uk.getmondo.c.a.localAmountView);
         co.uk.getmondo.common.i.b var17 = this.a;
         co.uk.getmondo.d.c var8 = var4.h();
         l.a(var8, "transaction.localAmount");
         var14.setText((CharSequence)var17.a(var8));
      } else {
         ae.b((TextView)this.itemView.findViewById(co.uk.getmondo.c.a.localAmountView));
      }

      LayoutParams var6;
      if(var1.k()) {
         ae.a((View)((ImageView)this.itemView.findViewById(co.uk.getmondo.c.a.overflowButton)));
         var6 = ((LinearLayout)this.itemView.findViewById(co.uk.getmondo.c.a.amountContainer)).getLayoutParams();
         if(var6 == null) {
            throw new TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
         }

         ((MarginLayoutParams)var6).setMarginEnd(0);
      } else {
         ae.b((ImageView)this.itemView.findViewById(co.uk.getmondo.c.a.overflowButton));
         int var2 = ((LinearLayout)this.itemView.findViewById(co.uk.getmondo.c.a.amountContainer)).getContext().getResources().getDimensionPixelSize(2131427603);
         var6 = ((LinearLayout)this.itemView.findViewById(co.uk.getmondo.c.a.amountContainer)).getLayoutParams();
         if(var6 == null) {
            throw new TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
         }

         ((MarginLayoutParams)var6).setMarginEnd(var2);
      }

   }
}
