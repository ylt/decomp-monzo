package co.uk.getmondo.feed.adapter;

import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class HeaderViewHolder_ViewBinding implements Unbinder {
   private HeaderViewHolder a;

   public HeaderViewHolder_ViewBinding(HeaderViewHolder var1, View var2) {
      this.a = var1;
      var1.dateView = (TextView)Utils.findRequiredViewAsType(var2, 2131821599, "field 'dateView'", TextView.class);
   }

   public void unbind() {
      HeaderViewHolder var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.dateView = null;
      }
   }
}
