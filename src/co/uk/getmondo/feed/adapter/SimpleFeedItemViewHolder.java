package co.uk.getmondo.feed.adapter;

import android.support.v7.widget.RecyclerView.w;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.uk.getmondo.d.m;
import com.bumptech.glide.g;

class SimpleFeedItemViewHolder extends w {
   private final co.uk.getmondo.feed.a.a a;
   private final a.a b;
   private m c;
   @BindView(2131821598)
   TextView descriptionTextView;
   @BindView(2131821595)
   ImageView iconImageView;
   @BindView(2131821596)
   View overflowButton;
   @BindView(2131821597)
   TextView titleTextView;

   SimpleFeedItemViewHolder(View var1, co.uk.getmondo.feed.a.a var2, a.a var3) {
      super(var1);
      this.a = var2;
      this.b = var3;
      ButterKnife.bind(this, (View)var1);
      var1.setOnClickListener(d.a(this, var3));
      this.overflowButton.setOnClickListener(e.a(this));
   }

   // $FF: synthetic method
   static void a(SimpleFeedItemViewHolder var0, View var1) {
      (new c(var1.getContext(), var1, var0.c, var0.b)).show();
   }

   // $FF: synthetic method
   static void a(SimpleFeedItemViewHolder var0, a.a var1, View var2) {
      var1.a(var0.c);
   }

   void a(m var1) {
      this.c = var1;
      co.uk.getmondo.d.f var3 = var1.i();
      int var2 = this.a.a(var1.j());
      if(var3 != null && var3.c() != null) {
         g.b(this.iconImageView.getContext()).a(var3.c()).a().a(var2).a(this.iconImageView);
      } else {
         this.iconImageView.setImageResource(var2);
      }

      this.iconImageView.setClipToOutline(true);
      this.titleTextView.setText(this.a.a(var1));
      this.descriptionTextView.setText(this.a.b(var1));
      View var5 = this.overflowButton;
      byte var4;
      if(var1.k()) {
         var4 = 0;
      } else {
         var4 = 8;
      }

      var5.setVisibility(var4);
   }
}
