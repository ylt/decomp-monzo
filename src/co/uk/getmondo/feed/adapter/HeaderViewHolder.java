package co.uk.getmondo.feed.adapter;

import android.support.v7.widget.RecyclerView.w;
import android.view.View;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;

class HeaderViewHolder extends w {
   @BindView(2131821599)
   TextView dateView;

   HeaderViewHolder(View var1) {
      super(var1);
      ButterKnife.bind(this, (View)var1);
   }
}
