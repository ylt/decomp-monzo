package co.uk.getmondo.feed.adapter;

import android.view.View;
import android.view.View.OnClickListener;

// $FF: synthetic class
final class d implements OnClickListener {
   private final SimpleFeedItemViewHolder a;
   private final a.a b;

   private d(SimpleFeedItemViewHolder var1, a.a var2) {
      this.a = var1;
      this.b = var2;
   }

   public static OnClickListener a(SimpleFeedItemViewHolder var0, a.a var1) {
      return new d(var0, var1);
   }

   public void onClick(View var1) {
      SimpleFeedItemViewHolder.a(this.a, this.b, var1);
   }
}
