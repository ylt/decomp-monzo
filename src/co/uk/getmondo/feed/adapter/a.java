package co.uk.getmondo.feed.adapter;

import android.support.v7.widget.RecyclerView.w;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import co.uk.getmondo.d.m;
import com.crashlytics.android.Crashlytics;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

public class a extends android.support.v7.widget.RecyclerView.a implements a.a.a.a.a.a {
   protected List a = Collections.emptyList();
   private a.a b;
   private final co.uk.getmondo.feed.a.a c;

   public a(co.uk.getmondo.feed.a.a var1) {
      this.c = var1;
   }

   private void a(String var1, Object... var2) {
      d.a.a.a(var1, var2);
      Crashlytics.log(String.format(Locale.ENGLISH, var1, var2));
   }

   public long a(int var1) {
      return (long)co.uk.getmondo.common.c.a.a(((m)this.a.get(var1)).c().getTime());
   }

   public w a(ViewGroup var1) {
      return new HeaderViewHolder(LayoutInflater.from(var1.getContext()).inflate(2131034371, var1, false));
   }

   public void a(w var1, int var2) {
      ((HeaderViewHolder)var1).dateView.setText(co.uk.getmondo.common.c.a.a(((m)this.a.get(var2)).c().getTime(), true));
      ((HeaderViewHolder)var1).dateView.setVisibility(0);
   }

   public void a(co.uk.getmondo.common.b.b var1) {
      this.a("Setting feed items from query results - size: %s changes: %s", new Object[]{Integer.valueOf(var1.a().size()), var1.b()});
      List var2;
      if(var1.a().isEmpty()) {
         var2 = Collections.emptyList();
      } else {
         var2 = var1.a();
      }

      this.a = var2;
      if(var1.b() == null) {
         this.a("notifyDataSetChanged called", new Object[0]);
         this.notifyDataSetChanged();
      } else {
         Iterator var4 = var1.b().iterator();

         while(var4.hasNext()) {
            co.uk.getmondo.common.b.b.a var3 = (co.uk.getmondo.common.b.b.a)var4.next();
            switch(null.a[var3.a().ordinal()]) {
            case 1:
               this.a("notifyItemRangeInserted called startIndex: %d length: %d", new Object[]{Integer.valueOf(var3.b()), Integer.valueOf(var3.c())});
               this.notifyItemRangeInserted(var3.b(), var3.c());
               break;
            case 2:
               this.a("notifyItemRangeRemoved called startIndex: %d length: %d", new Object[]{Integer.valueOf(var3.b()), Integer.valueOf(var3.c())});
               this.notifyItemRangeRemoved(var3.b(), var3.c());
               break;
            case 3:
               this.a("notifyItemRangeChanged called startIndex: %d length: %d", new Object[]{Integer.valueOf(var3.b()), Integer.valueOf(var3.c())});
               this.notifyItemRangeChanged(var3.b(), var3.c());
            }
         }
      }

   }

   public void a(a.a var1) {
      this.b = var1;
   }

   public int getItemCount() {
      return this.a.size();
   }

   public int getItemViewType(int var1) {
      byte var2;
      if(((m)this.a.get(var1)).j() == co.uk.getmondo.feed.a.a.a.a) {
         var2 = 0;
      } else {
         var2 = 1;
      }

      return var2;
   }

   public void onBindViewHolder(w var1, int var2) {
      m var3 = (m)this.a.get(var2);
      if(var1 instanceof f) {
         ((f)var1).a(var3);
      } else if(var1 instanceof SimpleFeedItemViewHolder) {
         ((SimpleFeedItemViewHolder)var1).a(var3);
      }

   }

   public w onCreateViewHolder(ViewGroup var1, int var2) {
      LayoutInflater var3 = LayoutInflater.from(var1.getContext());
      Object var4;
      switch(var2) {
      case 0:
         var4 = new f(var3.inflate(2131034380, var1, false), this.b);
         break;
      case 1:
         var4 = new SimpleFeedItemViewHolder(var3.inflate(2131034370, var1, false), this.c, this.b);
         break;
      default:
         throw new UnsupportedOperationException("Unknown feed item type: 16843169");
      }

      return (w)var4;
   }

   public interface a {
      void a(m var1);

      void b(m var1);
   }
}
