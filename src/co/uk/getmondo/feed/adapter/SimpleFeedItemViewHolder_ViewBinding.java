package co.uk.getmondo.feed.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class SimpleFeedItemViewHolder_ViewBinding implements Unbinder {
   private SimpleFeedItemViewHolder a;

   public SimpleFeedItemViewHolder_ViewBinding(SimpleFeedItemViewHolder var1, View var2) {
      this.a = var1;
      var1.iconImageView = (ImageView)Utils.findRequiredViewAsType(var2, 2131821595, "field 'iconImageView'", ImageView.class);
      var1.overflowButton = Utils.findRequiredView(var2, 2131821596, "field 'overflowButton'");
      var1.titleTextView = (TextView)Utils.findRequiredViewAsType(var2, 2131821597, "field 'titleTextView'", TextView.class);
      var1.descriptionTextView = (TextView)Utils.findRequiredViewAsType(var2, 2131821598, "field 'descriptionTextView'", TextView.class);
   }

   public void unbind() {
      SimpleFeedItemViewHolder var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.iconImageView = null;
         var1.overflowButton = null;
         var1.titleTextView = null;
         var1.descriptionTextView = null;
      }
   }
}
