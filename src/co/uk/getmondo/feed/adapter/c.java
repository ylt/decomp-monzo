package co.uk.getmondo.feed.adapter;

import android.content.Context;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import co.uk.getmondo.d.m;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t¢\u0006\u0002\u0010\n¨\u0006\u000b"},
   d2 = {"Lco/uk/getmondo/feed/adapter/FeedItemPopupMenu;", "Landroid/widget/PopupMenu;", "context", "Landroid/content/Context;", "anchor", "Landroid/view/View;", "feedItem", "Lco/uk/getmondo/model/FeedItem;", "listener", "Lco/uk/getmondo/feed/adapter/FeedAdapter$OnFeedItemActionListener;", "(Landroid/content/Context;Landroid/view/View;Lco/uk/getmondo/model/FeedItem;Lco/uk/getmondo/feed/adapter/FeedAdapter$OnFeedItemActionListener;)V", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class c extends PopupMenu {
   public c(Context var1, View var2, final m var3, final a.a var4) {
      l.b(var1, "context");
      l.b(var2, "anchor");
      l.b(var3, "feedItem");
      l.b(var4, "listener");
      super(var1, var2);
      this.getMenuInflater().inflate(2131951619, this.getMenu());
      this.setOnMenuItemClickListener((OnMenuItemClickListener)(new OnMenuItemClickListener() {
         public final boolean onMenuItemClick(MenuItem var1) {
            boolean var2;
            if(var1.getItemId() == 2131821777) {
               var4.b(var3);
               var2 = true;
            } else {
               var2 = false;
            }

            return var2;
         }
      }));
   }
}
