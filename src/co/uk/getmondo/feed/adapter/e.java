package co.uk.getmondo.feed.adapter;

import android.view.View;
import android.view.View.OnClickListener;

// $FF: synthetic class
final class e implements OnClickListener {
   private final SimpleFeedItemViewHolder a;

   private e(SimpleFeedItemViewHolder var1) {
      this.a = var1;
   }

   public static OnClickListener a(SimpleFeedItemViewHolder var0) {
      return new e(var0);
   }

   public void onClick(View var1) {
      SimpleFeedItemViewHolder.a(this.a, var1);
   }
}
