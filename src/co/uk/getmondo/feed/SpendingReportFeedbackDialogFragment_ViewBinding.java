package co.uk.getmondo.feed;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;

public class SpendingReportFeedbackDialogFragment_ViewBinding implements Unbinder {
   private SpendingReportFeedbackDialogFragment a;
   private View b;
   private View c;
   private View d;

   public SpendingReportFeedbackDialogFragment_ViewBinding(final SpendingReportFeedbackDialogFragment var1, View var2) {
      this.a = var1;
      View var3 = Utils.findRequiredView(var2, 2131821258, "field 'positiveFeedbackTextView' and method 'onCategoryClicked'");
      var1.positiveFeedbackTextView = (TextView)Utils.castView(var3, 2131821258, "field 'positiveFeedbackTextView'", TextView.class);
      this.b = var3;
      var3.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View var1x) {
            var1.onCategoryClicked((TextView)Utils.castParam(var1x, "doClick", 0, "onCategoryClicked", 0, TextView.class));
         }
      });
      var3 = Utils.findRequiredView(var2, 2131821259, "field 'neutralFeedbackTextView' and method 'onCategoryClicked'");
      var1.neutralFeedbackTextView = (TextView)Utils.castView(var3, 2131821259, "field 'neutralFeedbackTextView'", TextView.class);
      this.c = var3;
      var3.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View var1x) {
            var1.onCategoryClicked((TextView)Utils.castParam(var1x, "doClick", 0, "onCategoryClicked", 0, TextView.class));
         }
      });
      var2 = Utils.findRequiredView(var2, 2131821260, "field 'negativeFeedbackTextView' and method 'onCategoryClicked'");
      var1.negativeFeedbackTextView = (TextView)Utils.castView(var2, 2131821260, "field 'negativeFeedbackTextView'", TextView.class);
      this.d = var2;
      var2.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View var1x) {
            var1.onCategoryClicked((TextView)Utils.castParam(var1x, "doClick", 0, "onCategoryClicked", 0, TextView.class));
         }
      });
   }

   public void unbind() {
      SpendingReportFeedbackDialogFragment var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.positiveFeedbackTextView = null;
         var1.neutralFeedbackTextView = null;
         var1.negativeFeedbackTextView = null;
         this.b.setOnClickListener((OnClickListener)null);
         this.b = null;
         this.c.setOnClickListener((OnClickListener)null);
         this.c = null;
         this.d.setOnClickListener((OnClickListener)null);
         this.d = null;
      }
   }
}
