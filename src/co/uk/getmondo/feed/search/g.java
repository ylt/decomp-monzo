package co.uk.getmondo.feed.search;

import co.uk.getmondo.api.model.tracking.Impression;
import io.reactivex.u;

class g extends co.uk.getmondo.common.ui.b {
   private final u c;
   private final u d;
   private final co.uk.getmondo.common.e.a e;
   private final co.uk.getmondo.feed.a.d f;
   private final co.uk.getmondo.common.a g;

   g(u var1, u var2, co.uk.getmondo.common.e.a var3, co.uk.getmondo.feed.a.d var4, co.uk.getmondo.common.a var5) {
      this.c = var1;
      this.d = var2;
      this.e = var3;
      this.f = var4;
      this.g = var5;
   }

   // $FF: synthetic method
   static io.reactivex.d a(g var0, g.a var1, co.uk.getmondo.d.m var2) throws Exception {
      return var0.f.b(var2.a()).b(var0.d).a(m.a(var0, var1)).b();
   }

   // $FF: synthetic method
   static void a(g.a var0, co.uk.getmondo.common.b.b var1) throws Exception {
      if(var1.a().isEmpty()) {
         var0.c();
      } else {
         var0.d();
         var0.a(var1);
      }

   }

   // $FF: synthetic method
   static void a(g var0, g.a var1, Throwable var2) throws Exception {
      var0.e.a(var2, var1);
   }

   public void a(g.a var1) {
      super.a((co.uk.getmondo.common.ui.f)var1);
      this.g.a(Impression.W());
      io.reactivex.n var2 = var1.a().observeOn(this.c);
      co.uk.getmondo.feed.a.d var3 = this.f;
      var3.getClass();
      this.a((io.reactivex.b.b)var2.switchMap(h.a(var3)).subscribe(i.a(var1), j.a()));
      this.a((io.reactivex.b.b)var1.b().flatMapCompletable(k.a(this, var1)).a(co.uk.getmondo.common.j.a.a(), l.a()));
   }

   public void b() {
      this.g.a(Impression.X());
      super.b();
   }

   interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      io.reactivex.n a();

      void a(co.uk.getmondo.common.b.b var1);

      io.reactivex.n b();

      void c();

      void d();
   }
}
