package co.uk.getmondo.feed.search;

import android.content.Context;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0018\u00002\u00020\u0001B\u0011\b\u0007\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0014\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u00062\u0006\u0010\b\u001a\u00020\tR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\n"},
   d2 = {"Lco/uk/getmondo/feed/search/CategorySearcher;", "", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "searchByName", "", "Lco/uk/getmondo/model/Category;", "searchTerm", "", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class a {
   private final Context a;

   public a(Context var1) {
      kotlin.d.b.l.b(var1, "context");
      super();
      this.a = var1;
   }

   public final Set a(String var1) {
      kotlin.d.b.l.b(var1, "searchTerm");
      Object[] var6 = (Object[])co.uk.getmondo.d.h.values();
      Collection var3 = (Collection)(new ArrayList());

      for(int var2 = 0; var2 < var6.length; ++var2) {
         Object var4 = var6[var2];
         co.uk.getmondo.d.h var5 = (co.uk.getmondo.d.h)var4;
         if(kotlin.h.j.b((CharSequence)this.a.getString(var5.a()), (CharSequence)var1, true)) {
            var3.add(var4);
         }
      }

      return kotlin.a.m.m((Iterable)((List)var3));
   }
}
