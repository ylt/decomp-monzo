package co.uk.getmondo.feed.search;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class FeedSearchActivity_ViewBinding implements Unbinder {
   private FeedSearchActivity a;

   public FeedSearchActivity_ViewBinding(FeedSearchActivity var1, View var2) {
      this.a = var1;
      var1.searchView = (SearchView)Utils.findRequiredViewAsType(var2, 2131820907, "field 'searchView'", SearchView.class);
      var1.searchRecyclerView = (RecyclerView)Utils.findRequiredViewAsType(var2, 2131820908, "field 'searchRecyclerView'", RecyclerView.class);
      var1.noResults = Utils.findRequiredView(var2, 2131820909, "field 'noResults'");
   }

   public void unbind() {
      FeedSearchActivity var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.searchView = null;
         var1.searchRecyclerView = null;
         var1.noResults = null;
      }
   }
}
