package co.uk.getmondo.feed.search;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.View;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.r;
import java.util.concurrent.TimeUnit;

public class FeedSearchActivity extends co.uk.getmondo.common.activities.b implements co.uk.getmondo.feed.adapter.a.a, g.a {
   co.uk.getmondo.feed.adapter.a a;
   co.uk.getmondo.feed.c b;
   g c;
   private final com.b.b.c e = com.b.b.c.a();
   private final com.b.b.c f = com.b.b.c.a();
   @BindView(2131820909)
   View noResults;
   @BindView(2131820908)
   RecyclerView searchRecyclerView;
   @BindView(2131820907)
   SearchView searchView;

   public static Intent a(Context var0, String var1) {
      return (new Intent(var0, FeedSearchActivity.class)).putExtra("EXTRA_QUERY", var1).setFlags(67108864);
   }

   // $FF: synthetic method
   static r a(String var0) throws Exception {
      io.reactivex.n var1;
      if(TextUtils.isEmpty(var0)) {
         var1 = io.reactivex.n.just(var0);
      } else {
         var1 = io.reactivex.n.just(var0).delay(400L, TimeUnit.MILLISECONDS);
      }

      return var1;
   }

   // $FF: synthetic method
   static void a(FeedSearchActivity var0) {
      var0.searchRecyclerView.a(0);
   }

   // $FF: synthetic method
   static void b(FeedSearchActivity var0) {
      var0.searchView.clearFocus();
   }

   private void e() {
      this.searchView.setQueryHint(this.getString(2131362186));
      this.searchView.setOnQueryTextListener(new android.support.v7.widget.SearchView.c() {
         public boolean a(String var1) {
            return false;
         }

         public boolean b(String var1) {
            FeedSearchActivity.this.e.a((Object)var1);
            return true;
         }
      });
   }

   public io.reactivex.n a() {
      return this.e.debounce(d.a());
   }

   public void a(co.uk.getmondo.common.b.b var1) {
      this.a.a(var1);
      this.searchRecyclerView.postDelayed(e.a(this), 50L);
   }

   public void a(co.uk.getmondo.d.m var1) {
      this.b.a(var1);
   }

   public io.reactivex.n b() {
      return this.f;
   }

   public void b(co.uk.getmondo.d.m var1) {
      this.f.a((Object)var1);
   }

   public void c() {
      this.noResults.setVisibility(0);
      this.noResults.animate().alpha(1.0F).setDuration(120L);
      this.searchRecyclerView.animate().alpha(0.0F).setDuration(120L);
   }

   public void d() {
      this.noResults.animate().cancel();
      this.noResults.setAlpha(0.0F);
      this.noResults.setVisibility(8);
      this.searchRecyclerView.animate().cancel();
      this.searchRecyclerView.setAlpha(1.0F);
   }

   protected void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.setContentView(2131034169);
      ButterKnife.bind((Activity)this);
      this.l().a(this);
      this.e();
      this.a.a((co.uk.getmondo.feed.adapter.a.a)this);
      LinearLayoutManager var3 = new LinearLayoutManager(this, 1, false);
      this.searchRecyclerView.setLayoutManager(var3);
      this.searchRecyclerView.setAdapter(this.a);
      this.searchRecyclerView.setHasFixedSize(true);
      int var2 = this.getResources().getDimensionPixelSize(2131427609);
      this.searchRecyclerView.a(new co.uk.getmondo.common.ui.h(this, this.a, var2));
      this.searchRecyclerView.a(new a.a.a.a.a.b(this.a));
      this.c.a((g.a)this);
      this.onNewIntent(this.getIntent());
   }

   protected void onDestroy() {
      this.c.b();
      super.onDestroy();
   }

   protected void onNewIntent(Intent var1) {
      if(var1.hasExtra("EXTRA_QUERY")) {
         String var2 = var1.getStringExtra("EXTRA_QUERY");
         if(!TextUtils.isEmpty(var2)) {
            this.searchView.a(var2, false);
            this.searchView.post(c.a(this));
         }
      } else {
         this.e.a((Object)"");
      }

   }
}
