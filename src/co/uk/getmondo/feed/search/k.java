package co.uk.getmondo.feed.search;

// $FF: synthetic class
final class k implements io.reactivex.c.h {
   private final g a;
   private final g.a b;

   private k(g var1, g.a var2) {
      this.a = var1;
      this.b = var2;
   }

   public static io.reactivex.c.h a(g var0, g.a var1) {
      return new k(var0, var1);
   }

   public Object a(Object var1) {
      return g.a(this.a, this.b, (co.uk.getmondo.d.m)var1);
   }
}
