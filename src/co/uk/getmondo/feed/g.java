package co.uk.getmondo.feed;

import co.uk.getmondo.a.k;
import co.uk.getmondo.d.m;
import co.uk.getmondo.d.t;
import io.reactivex.u;
import io.reactivex.c.q;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.n;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u001cBS\b\u0007\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\f\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013¢\u0006\u0002\u0010\u0014J\u0010\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u0002H\u0016R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u001d"},
   d2 = {"Lco/uk/getmondo/feed/HomeFeedPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/feed/HomeFeedPresenter$View;", "uiScheduler", "Lio/reactivex/Scheduler;", "ioScheduler", "apiErrorHandler", "Lco/uk/getmondo/common/errors/ApiErrorHandler;", "deleteFeedItemStorage", "Lco/uk/getmondo/common/DeleteFeedItemStorage;", "feedManager", "Lco/uk/getmondo/feed/data/FeedManager;", "syncManager", "Lco/uk/getmondo/background_sync/SyncManager;", "cardManager", "Lco/uk/getmondo/card/CardManager;", "balanceRepository", "Lco/uk/getmondo/account_balance/BalanceRepository;", "accountManager", "Lco/uk/getmondo/common/accounts/AccountManager;", "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/common/DeleteFeedItemStorage;Lco/uk/getmondo/feed/data/FeedManager;Lco/uk/getmondo/background_sync/SyncManager;Lco/uk/getmondo/card/CardManager;Lco/uk/getmondo/account_balance/BalanceRepository;Lco/uk/getmondo/common/accounts/AccountManager;)V", "amountFormatter", "Lco/uk/getmondo/common/money/AmountFormatter;", "localSpendComparator", "Lco/uk/getmondo/account_balance/LocalSpendComparator;", "register", "", "view", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class g extends co.uk.getmondo.common.ui.b {
   private final co.uk.getmondo.common.i.b c;
   private final k d;
   private final u e;
   private final u f;
   private final co.uk.getmondo.common.e.a g;
   private final co.uk.getmondo.common.i h;
   private final co.uk.getmondo.feed.a.d i;
   private final co.uk.getmondo.background_sync.d j;
   private final co.uk.getmondo.card.c k;
   private final co.uk.getmondo.a.a l;
   private final co.uk.getmondo.common.accounts.b m;

   public g(u var1, u var2, co.uk.getmondo.common.e.a var3, co.uk.getmondo.common.i var4, co.uk.getmondo.feed.a.d var5, co.uk.getmondo.background_sync.d var6, co.uk.getmondo.card.c var7, co.uk.getmondo.a.a var8, co.uk.getmondo.common.accounts.b var9) {
      l.b(var1, "uiScheduler");
      l.b(var2, "ioScheduler");
      l.b(var3, "apiErrorHandler");
      l.b(var4, "deleteFeedItemStorage");
      l.b(var5, "feedManager");
      l.b(var6, "syncManager");
      l.b(var7, "cardManager");
      l.b(var8, "balanceRepository");
      l.b(var9, "accountManager");
      super();
      this.e = var1;
      this.f = var2;
      this.g = var3;
      this.h = var4;
      this.i = var5;
      this.j = var6;
      this.k = var7;
      this.l = var8;
      this.m = var9;
      this.c = new co.uk.getmondo.common.i.b(true, false, false, 6, (kotlin.d.b.i)null);
      this.d = new k();
   }

   public void a(final g.a var1) {
      l.b(var1, "view");
      super.a((co.uk.getmondo.common.ui.f)var1);
      io.reactivex.b.a var3 = this.b;
      io.reactivex.b.b var2 = this.i.a().observeOn(this.e).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(co.uk.getmondo.common.b.b var1x) {
            if(var1x.a().isEmpty() && g.this.i.c() == null) {
               var1.e();
            } else {
               var1.f();
            }

            g.a var2 = var1;
            l.a(var1x, "queryResults");
            var2.a(var1x);
         }
      }), (io.reactivex.c.g)null.a);
      l.a(var2, "feedManager.feedItems()\n…     }, { Timber.e(it) })");
      this.b = co.uk.getmondo.common.j.f.a(var3, var2);
      var3 = this.b;
      var2 = this.i.b().filter((q)(new q() {
         public final boolean a(m var1) {
            l.b(var1, "<anonymous parameter 0>");
            return g.this.h.b();
         }
      })).flatMapCompletable((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.b a(m var1) {
            l.b(var1, "kycFeedItem");
            return g.this.i.b(var1.a()).b((io.reactivex.c.a)(new io.reactivex.c.a() {
               public final void a() {
                  g.this.h.c();
               }
            })).b(g.this.f).a((io.reactivex.c.g)null.a).b();
         }
      })).a((io.reactivex.c.a)null.a, (io.reactivex.c.g)null.a);
      l.a(var2, "feedManager.knowYourCust…ibe({}, { Timber.e(it) })");
      this.b = co.uk.getmondo.common.j.f.a(var3, var2);
      var3 = this.b;
      var2 = var1.a().flatMapCompletable((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.b a(n var1x) {
            l.b(var1x, "<anonymous parameter 0>");
            return g.this.j.a().b(g.this.f).a(g.this.e).c((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(io.reactivex.b.b var1x) {
                  var1.a(true);
               }
            })).b((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(Throwable var1x) {
                  var1.a(false);
               }
            })).a((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(Throwable var1x) {
                  co.uk.getmondo.common.e.a var2 = g.this.g;
                  l.a(var1x, "throwable");
                  var2.a(var1x, (co.uk.getmondo.common.e.a.a)var1);
               }
            })).b();
         }
      })).a((io.reactivex.c.a)null.a, (io.reactivex.c.g)null.a);
      l.a(var2, "view.onRefreshAction()\n …ce\") }, { Timber.e(it) })");
      this.b = co.uk.getmondo.common.j.f.a(var3, var2);
      io.reactivex.b.a var5 = this.b;
      io.reactivex.b.b var6 = var1.b().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Object var1x) {
            var1.d();
         }
      }), (io.reactivex.c.g)null.a);
      l.a(var6, "view.onSearch()\n        …ch() }, { Timber.e(it) })");
      this.b = co.uk.getmondo.common.j.f.a(var5, var6);
      var3 = this.b;
      var2 = var1.c().flatMapCompletable((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.b a(m var1x) {
            l.b(var1x, "feedItem");
            return g.this.i.b(var1x.a()).b(g.this.f).a((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(Throwable var1x) {
                  co.uk.getmondo.common.e.a var2 = g.this.g;
                  l.a(var1x, "throwable");
                  var2.a(var1x, (co.uk.getmondo.common.e.a.a)var1);
               }
            })).b();
         }
      })).a((io.reactivex.c.a)null.a, (io.reactivex.c.g)null.a);
      l.a(var2, "view.onFeedItemDeleted()…ibe({}, { Timber.e(it) })");
      this.b = co.uk.getmondo.common.j.f.a(var3, var2);
      var3 = this.b;
      var2 = this.k.a().observeOn(this.e).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(co.uk.getmondo.d.g var1x) {
            if(var1x.h()) {
               var1.g();
            } else {
               var1.h();
            }

         }
      }), (io.reactivex.c.g)null.a);
      l.a(var2, "cardManager.card()\n     …led to get card state\") }");
      this.b = co.uk.getmondo.common.j.f.a(var3, var2);
      var3 = this.b;
      var2 = this.m.e().flatMap((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.n a(String var1) {
            l.b(var1, "it");
            return g.this.l.a(var1);
         }
      })).observeOn(this.e).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(co.uk.getmondo.d.b var1x) {
            g.a var2 = var1;
            co.uk.getmondo.d.c var3 = var1x.a();
            l.a(var3, "accountBalance.balance");
            var2.a(var3);
            var2 = var1;
            var3 = var1x.b();
            l.a(var3, "accountBalance.spentToday");
            var2.b(var3);
            if(var1x.d()) {
               ArrayList var5 = new ArrayList((Collection)var1x.c());
               kotlin.a.m.a((List)var5, (Comparator)g.this.d);
               Iterable var8 = (Iterable)var5;
               Collection var6 = (Collection)(new ArrayList(kotlin.a.m.a(var8, 10)));
               Iterator var10 = var8.iterator();

               while(var10.hasNext()) {
                  t var4 = (t)var10.next();
                  co.uk.getmondo.common.i.b var9 = g.this.c;
                  co.uk.getmondo.d.c var11 = var4.a();
                  l.a(var11, "it.spendToday");
                  var6.add(var9.a(var11));
               }

               List var7 = (List)var6;
               var1.a(var7);
            } else {
               var1.j();
            }

         }
      }), (io.reactivex.c.g)null.a);
      l.a(var2, "accountManager.accountId…     }, { Timber.e(it) })");
      this.b = co.uk.getmondo.common.j.f.a(var3, var2);
      var5 = this.b;
      io.reactivex.b.b var4 = var1.i().startWith((Object)n.a).flatMapCompletable((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.b a(n var1x) {
            l.b(var1x, "it");
            return g.this.m.e().flatMapCompletable((io.reactivex.c.h)(new io.reactivex.c.h() {
               public final io.reactivex.b a(String var1x) {
                  l.b(var1x, "it");
                  return g.this.l.b(var1x);
               }
            })).b(g.this.f).a(g.this.e).a((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(Throwable var1x) {
                  co.uk.getmondo.common.e.a var2 = g.this.g;
                  l.a(var1x, "error");
                  var2.a(var1x, (co.uk.getmondo.common.e.a.a)var1);
               }
            })).b();
         }
      })).a((io.reactivex.c.a)null.a, (io.reactivex.c.g)null.a);
      l.a(var4, "view.onDateChanged()\n   …ibe({}, { Timber.e(it) })");
      this.b = co.uk.getmondo.common.j.f.a(var5, var4);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\u0003\bf\u0018\u00002\u00020\u00012\u00020\u0002J\b\u0010\u0003\u001a\u00020\u0004H&J\b\u0010\u0005\u001a\u00020\u0004H&J\u000e\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00040\u0007H&J\u000e\u0010\b\u001a\b\u0012\u0004\u0012\u00020\t0\u0007H&J\u000e\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00040\u0007H&J\u000e\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\f0\u0007H&J\b\u0010\r\u001a\u00020\u0004H&J\u0010\u0010\u000e\u001a\u00020\u00042\u0006\u0010\u000f\u001a\u00020\u0010H&J\u0016\u0010\u0011\u001a\u00020\u00042\f\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\t0\u0013H&J\u0010\u0010\u0014\u001a\u00020\u00042\u0006\u0010\u0015\u001a\u00020\u0016H&J\u0010\u0010\u0017\u001a\u00020\u00042\u0006\u0010\u0018\u001a\u00020\u0010H&J\b\u0010\u0019\u001a\u00020\u0004H&J\u0016\u0010\u001a\u001a\u00020\u00042\f\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u001d0\u001cH&J\b\u0010\u001e\u001a\u00020\u0004H&J\b\u0010\u001f\u001a\u00020\u0004H&¨\u0006 "},
      d2 = {"Lco/uk/getmondo/feed/HomeFeedPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "hideForeignCurrencyAmounts", "", "hideProgressIndicator", "onDateChanged", "Lio/reactivex/Observable;", "onFeedItemDeleted", "Lco/uk/getmondo/model/FeedItem;", "onRefreshAction", "onSearch", "", "openSearch", "setBalance", "balance", "Lco/uk/getmondo/model/Amount;", "setFeedItems", "feedItems", "Lco/uk/getmondo/common/data/QueryResults;", "setRefreshing", "refreshing", "", "setSpentToday", "spentToday", "showCardFrozen", "showForeignCurrencyAmounts", "amounts", "", "", "showProgressIndicator", "showTitle", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      io.reactivex.n a();

      void a(co.uk.getmondo.common.b.b var1);

      void a(co.uk.getmondo.d.c var1);

      void a(List var1);

      void a(boolean var1);

      io.reactivex.n b();

      void b(co.uk.getmondo.d.c var1);

      io.reactivex.n c();

      void d();

      void e();

      void f();

      void g();

      void h();

      io.reactivex.n i();

      void j();
   }
}
