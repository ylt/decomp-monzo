package co.uk.getmondo.feed.welcome;

import co.uk.getmondo.common.accounts.d;

public final class c implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final b.a b;
   private final javax.a.a c;
   private final javax.a.a d;

   static {
      boolean var0;
      if(!c.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   public c(b.a var1, javax.a.a var2, javax.a.a var3) {
      if(!a && var1 == null) {
         throw new AssertionError();
      } else {
         this.b = var1;
         if(!a && var2 == null) {
            throw new AssertionError();
         } else {
            this.c = var2;
            if(!a && var3 == null) {
               throw new AssertionError();
            } else {
               this.d = var3;
            }
         }
      }
   }

   public static b.a.b a(b.a var0, javax.a.a var1, javax.a.a var2) {
      return new c(var0, var1, var2);
   }

   public b a() {
      return (b)b.a.c.a(this.b, new b((co.uk.getmondo.common.a)this.c.b(), (d)this.d.b()));
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
