package co.uk.getmondo.feed;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.common.ui.AmountView;
import co.uk.getmondo.d.v;
import co.uk.getmondo.main.HomeActivity;
import java.util.Locale;
import org.threeten.bp.YearMonth;
import org.threeten.bp.format.TextStyle;

public class MonthlySpendingReportActivity extends co.uk.getmondo.common.activities.b {
   co.uk.getmondo.common.a a;
   @BindView(2131821100)
   AmountView amountView;
   private YearMonth b;
   @BindView(2131821103)
   Button goToSpendingButton;
   @BindView(2131821101)
   TextView monthTextView;

   public static Intent a(Context var0, v var1) {
      Intent var2 = new Intent(var0, MonthlySpendingReportActivity.class);
      var2.putExtra("KEY_MONTHLY_SPENDING_AMOUNT", var1.a());
      var2.putExtra("KEY_MONTHLY_SPENDING_MONTH", var1.b());
      return var2;
   }

   // $FF: synthetic method
   static void a(MonthlySpendingReportActivity var0, SpendingReportFeedbackDialogFragment.a var1) {
      Impression var2 = Impression.a(var1, var0.b);
      var0.a.a(var2);
   }

   protected void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.setContentView(2131034213);
      ButterKnife.bind((Activity)this);
      this.l().a(this);
      co.uk.getmondo.d.c var2 = (co.uk.getmondo.d.c)this.getIntent().getParcelableExtra("KEY_MONTHLY_SPENDING_AMOUNT");
      this.b = (YearMonth)this.getIntent().getSerializableExtra("KEY_MONTHLY_SPENDING_MONTH");
      this.a.a(Impression.a(this.b));
      this.amountView.setAmount(var2);
      String var3 = this.b.c().a(TextStyle.a, Locale.ENGLISH);
      this.monthTextView.setText(this.getString(2131362451, new Object[]{var3}));
      this.goToSpendingButton.setText(this.getString(2131362452, new Object[]{var3}));
   }

   public boolean onCreateOptionsMenu(Menu var1) {
      this.getMenuInflater().inflate(2131951624, var1);
      return true;
   }

   @OnClick({2131821103})
   public void onGoToSpendingButtonClicked() {
      this.startActivity(HomeActivity.a((Context)this, (YearMonth)this.b));
   }

   public boolean onOptionsItemSelected(MenuItem var1) {
      boolean var2;
      if(var1.getItemId() == 2131821783) {
         SpendingReportFeedbackDialogFragment var3 = SpendingReportFeedbackDialogFragment.a();
         var3.a(i.a(this));
         var3.show(this.getFragmentManager(), "spending_report");
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }
}
