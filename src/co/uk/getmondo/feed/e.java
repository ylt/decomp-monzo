package co.uk.getmondo.feed;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import co.uk.getmondo.common.ae;
import co.uk.getmondo.common.ui.AmountView;
import co.uk.getmondo.d.m;
import co.uk.getmondo.feed.search.FeedSearchActivity;
import co.uk.getmondo.main.HomeActivity;
import io.reactivex.n;
import io.reactivex.o;
import io.reactivex.p;
import java.util.HashMap;
import java.util.List;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u009e\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010 \n\u0002\b\u0003\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003B\u0005¢\u0006\u0002\u0010\u0004J\b\u0010\u001f\u001a\u00020 H\u0016J\b\u0010!\u001a\u00020 H\u0016J\u0012\u0010\"\u001a\u00020 2\b\u0010#\u001a\u0004\u0018\u00010$H\u0016J\u001a\u0010%\u001a\u00020 2\b\u0010&\u001a\u0004\u0018\u00010'2\u0006\u0010(\u001a\u00020)H\u0016J$\u0010*\u001a\u00020+2\u0006\u0010(\u001a\u00020,2\b\u0010-\u001a\u0004\u0018\u00010.2\b\u0010#\u001a\u0004\u0018\u00010$H\u0016J\u000e\u0010/\u001a\b\u0012\u0004\u0012\u00020 00H\u0016J\b\u00101\u001a\u00020 H\u0016J\u0010\u00102\u001a\u00020 2\u0006\u00103\u001a\u00020\u000fH\u0016J\u0010\u00104\u001a\u00020 2\u0006\u00103\u001a\u00020\u000fH\u0016J\u000e\u00105\u001a\b\u0012\u0004\u0012\u00020\u000f00H\u0016J\u0010\u00106\u001a\u0002072\u0006\u00108\u001a\u000209H\u0016J\u000e\u0010:\u001a\b\u0012\u0004\u0012\u00020 00H\u0016J\u0012\u0010;\u001a\u00020 2\b\u0010<\u001a\u0004\u0018\u00010$H\u0016J\u000e\u0010=\u001a\b\u0012\u0004\u0012\u00020\u001e00H\u0016J\u001a\u0010>\u001a\u00020 2\u0006\u0010?\u001a\u00020+2\b\u0010#\u001a\u0004\u0018\u00010$H\u0016J\b\u0010@\u001a\u00020 H\u0016J\u0010\u0010A\u001a\u00020 2\u0006\u0010B\u001a\u00020CH\u0016J\u0016\u0010D\u001a\u00020 2\f\u0010E\u001a\b\u0012\u0004\u0012\u00020\u000f0FH\u0016J\u0010\u0010G\u001a\u00020 2\u0006\u0010H\u001a\u000207H\u0016J\u0010\u0010I\u001a\u00020 2\u0006\u0010J\u001a\u00020CH\u0016J\b\u0010K\u001a\u00020 H\u0016J\u0016\u0010L\u001a\u00020 2\f\u0010M\u001a\b\u0012\u0004\u0012\u00020\u00060NH\u0016J\b\u0010O\u001a\u00020 H\u0016J\b\u0010P\u001a\u00020 H\u0016R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082D¢\u0006\u0002\n\u0000R\u001e\u0010\u0007\u001a\u00020\b8\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\t\u0010\n\"\u0004\b\u000b\u0010\fR2\u0010\r\u001a&\u0012\f\u0012\n \u0010*\u0004\u0018\u00010\u000f0\u000f \u0010*\u0012\u0012\f\u0012\n \u0010*\u0004\u0018\u00010\u000f0\u000f\u0018\u00010\u000e0\u000eX\u0082\u0004¢\u0006\u0002\n\u0000R\u001e\u0010\u0011\u001a\u00020\u00128\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\u0016R\u001e\u0010\u0017\u001a\u00020\u00188\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\u001a\"\u0004\b\u001b\u0010\u001cR2\u0010\u001d\u001a&\u0012\f\u0012\n \u0010*\u0004\u0018\u00010\u001e0\u001e \u0010*\u0012\u0012\f\u0012\n \u0010*\u0004\u0018\u00010\u001e0\u001e\u0018\u00010\u000e0\u000eX\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006Q"},
   d2 = {"Lco/uk/getmondo/feed/HomeFeedFragment;", "Lco/uk/getmondo/common/fragments/BaseFragment;", "Lco/uk/getmondo/feed/HomeFeedPresenter$View;", "Lco/uk/getmondo/feed/adapter/FeedAdapter$OnFeedItemActionListener;", "()V", "FEED_STATE_KEY", "", "feedAdapter", "Lco/uk/getmondo/feed/adapter/FeedAdapter;", "getFeedAdapter", "()Lco/uk/getmondo/feed/adapter/FeedAdapter;", "setFeedAdapter", "(Lco/uk/getmondo/feed/adapter/FeedAdapter;)V", "feedItemDeletionRelay", "Lcom/jakewharton/rxrelay2/PublishRelay;", "Lco/uk/getmondo/model/FeedItem;", "kotlin.jvm.PlatformType", "feedItemNavigator", "Lco/uk/getmondo/feed/FeedItemNavigator;", "getFeedItemNavigator", "()Lco/uk/getmondo/feed/FeedItemNavigator;", "setFeedItemNavigator", "(Lco/uk/getmondo/feed/FeedItemNavigator;)V", "homeFeedPresenter", "Lco/uk/getmondo/feed/HomeFeedPresenter;", "getHomeFeedPresenter", "()Lco/uk/getmondo/feed/HomeFeedPresenter;", "setHomeFeedPresenter", "(Lco/uk/getmondo/feed/HomeFeedPresenter;)V", "searchRelay", "", "hideForeignCurrencyAmounts", "", "hideProgressIndicator", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onCreateOptionsMenu", "menu", "Landroid/view/Menu;", "inflater", "Landroid/view/MenuInflater;", "onCreateView", "Landroid/view/View;", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "onDateChanged", "Lio/reactivex/Observable;", "onDestroyView", "onFeedItemClicked", "feedItem", "onFeedItemDeleteRequested", "onFeedItemDeleted", "onOptionsItemSelected", "", "item", "Landroid/view/MenuItem;", "onRefreshAction", "onSaveInstanceState", "outState", "onSearch", "onViewCreated", "view", "openSearch", "setBalance", "balance", "Lco/uk/getmondo/model/Amount;", "setFeedItems", "feedItems", "Lco/uk/getmondo/common/data/QueryResults;", "setRefreshing", "refreshing", "setSpentToday", "spentToday", "showCardFrozen", "showForeignCurrencyAmounts", "amounts", "", "showProgressIndicator", "showTitle", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class e extends co.uk.getmondo.common.f.a implements co.uk.getmondo.feed.adapter.a.a, g.a {
   public co.uk.getmondo.feed.adapter.a a;
   public g c;
   public c d;
   private final String e = "FEED_STATE_KEY";
   private final com.b.b.c f = com.b.b.c.a();
   private final com.b.b.c g = com.b.b.c.a();
   private HashMap h;

   public View a(int var1) {
      if(this.h == null) {
         this.h = new HashMap();
      }

      View var3 = (View)this.h.get(Integer.valueOf(var1));
      View var2 = var3;
      if(var3 == null) {
         var2 = this.getView();
         if(var2 == null) {
            var2 = null;
         } else {
            var2 = var2.findViewById(var1);
            this.h.put(Integer.valueOf(var1), var2);
         }
      }

      return var2;
   }

   public n a() {
      n var1 = n.create((p)(new p() {
         public final void a(final o var1) {
            l.b(var1, "emitter");
            ((SwipeRefreshLayout)e.this.a(co.uk.getmondo.c.a.feedSwipeToRefreshLayout)).setOnRefreshListener((android.support.v4.widget.SwipeRefreshLayout.b)(new android.support.v4.widget.SwipeRefreshLayout.b() {
               public final void a() {
                  var1.a(kotlin.n.a);
               }
            }));
            var1.a((io.reactivex.c.f)(new io.reactivex.c.f() {
               public final void a() {
                  ((SwipeRefreshLayout)e.this.a(co.uk.getmondo.c.a.feedSwipeToRefreshLayout)).setOnRefreshListener((android.support.v4.widget.SwipeRefreshLayout.b)null);
               }
            }));
         }
      }));
      l.a(var1, "Observable.create { emit…istener(null) }\n        }");
      return var1;
   }

   public void a(co.uk.getmondo.common.b.b var1) {
      l.b(var1, "feedItems");
      co.uk.getmondo.feed.adapter.a var2 = this.a;
      if(var2 == null) {
         l.b("feedAdapter");
      }

      var2.a(var1);
   }

   public void a(co.uk.getmondo.d.c var1) {
      l.b(var1, "balance");
      ((AmountView)this.a(co.uk.getmondo.c.a.balanceAmount)).setAmount(var1);
   }

   public void a(m var1) {
      l.b(var1, "feedItem");
      c var2 = this.d;
      if(var2 == null) {
         l.b("feedItemNavigator");
      }

      var2.a(var1);
   }

   public void a(List var1) {
      l.b(var1, "amounts");
      ((TextView)this.a(co.uk.getmondo.c.a.spentTodayLabel)).setText((CharSequence)this.getString(2131362406, new Object[]{kotlin.a.m.a((Iterable)var1, (CharSequence)" + ", (CharSequence)null, (CharSequence)null, 0, (CharSequence)null, (kotlin.d.a.b)null, 62, (Object)null)}));
      ((LinearLayout)this.a(co.uk.getmondo.c.a.spendTodayBalanceView)).setVisibility(0);
   }

   public void a(final boolean var1) {
      ((RecyclerView)this.a(co.uk.getmondo.c.a.feedRecyclerView)).post((Runnable)(new Runnable() {
         public final void run() {
            if((SwipeRefreshLayout)e.this.a(co.uk.getmondo.c.a.feedSwipeToRefreshLayout) != null) {
               ((SwipeRefreshLayout)e.this.a(co.uk.getmondo.c.a.feedSwipeToRefreshLayout)).setRefreshing(var1);
            }

         }
      }));
   }

   public n b() {
      com.b.b.c var1 = this.f;
      l.a(var1, "searchRelay");
      return (n)var1;
   }

   public void b(co.uk.getmondo.d.c var1) {
      l.b(var1, "spentToday");
      ((AmountView)this.a(co.uk.getmondo.c.a.spentTodayAmount)).setAmount(var1);
   }

   public void b(m var1) {
      l.b(var1, "feedItem");
      this.g.a((Object)var1);
   }

   public n c() {
      com.b.b.c var1 = this.g;
      l.a(var1, "feedItemDeletionRelay");
      return (n)var1;
   }

   public void d() {
      this.startActivity(new Intent((Context)this.getActivity(), FeedSearchActivity.class));
   }

   public void e() {
      ae.a((View)((ProgressBar)this.a(co.uk.getmondo.c.a.progress)));
   }

   public void f() {
      ae.b((ProgressBar)this.a(co.uk.getmondo.c.a.progress));
   }

   public void g() {
      ((Toolbar)this.a(co.uk.getmondo.c.a.toolbar)).setTitle(2131362012);
      ((Toolbar)this.a(co.uk.getmondo.c.a.toolbar)).setTitleTextColor(android.support.v4.content.a.c(this.getContext(), 2131689706));
   }

   public void h() {
      ((Toolbar)this.a(co.uk.getmondo.c.a.toolbar)).setTitle(2131362481);
      ((Toolbar)this.a(co.uk.getmondo.c.a.toolbar)).setTitleTextColor(android.support.v4.content.a.c(this.getContext(), 2131689593));
   }

   public n i() {
      n var1 = co.uk.getmondo.common.j.b.a(this.getContext(), new IntentFilter("android.intent.action.DATE_CHANGED")).map((io.reactivex.c.h)null.a);
      l.a(var1, "RxBroadcastReceiver.crea…\n                .map { }");
      return var1;
   }

   public void j() {
      ((TextView)this.a(co.uk.getmondo.c.a.spentTodayLabel)).setText((CharSequence)this.getString(2131362741));
   }

   public void k() {
      if(this.h != null) {
         this.h.clear();
      }

   }

   public void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.B().a(this);
   }

   public void onCreateOptionsMenu(Menu var1, MenuInflater var2) {
      l.b(var2, "inflater");
      super.onCreateOptionsMenu(var1, var2);
      var2.inflate(2131951618, var1);
   }

   public View onCreateView(LayoutInflater var1, ViewGroup var2, Bundle var3) {
      l.b(var1, "inflater");
      View var4 = var1.inflate(2131034270, var2, false);
      l.a(var4, "inflater.inflate(R.layou…t_feed, container, false)");
      return var4;
   }

   public void onDestroyView() {
      ((RecyclerView)this.a(co.uk.getmondo.c.a.feedRecyclerView)).setAdapter((android.support.v7.widget.RecyclerView.a)null);
      g var1 = this.c;
      if(var1 == null) {
         l.b("homeFeedPresenter");
      }

      var1.b();
      ((SwipeRefreshLayout)this.a(co.uk.getmondo.c.a.feedSwipeToRefreshLayout)).setRefreshing(false);
      ((SwipeRefreshLayout)this.a(co.uk.getmondo.c.a.feedSwipeToRefreshLayout)).destroyDrawingCache();
      ((SwipeRefreshLayout)this.a(co.uk.getmondo.c.a.feedSwipeToRefreshLayout)).clearAnimation();
      super.onDestroyView();
      this.k();
   }

   public boolean onOptionsItemSelected(MenuItem var1) {
      l.b(var1, "item");
      boolean var2;
      if(var1.getItemId() == 2131821776) {
         this.f.a((Object)co.uk.getmondo.common.b.a.a);
         var2 = true;
      } else {
         var2 = super.onOptionsItemSelected(var1);
      }

      return var2;
   }

   public void onSaveInstanceState(Bundle var1) {
      super.onSaveInstanceState(var1);
      if(var1 == null) {
         l.a();
      }

      var1.putParcelable(this.e, ((RecyclerView)this.a(co.uk.getmondo.c.a.feedRecyclerView)).getLayoutManager().e());
   }

   public void onViewCreated(View var1, Bundle var2) {
      l.b(var1, "view");
      super.onViewCreated(var1, var2);
      this.setHasOptionsMenu(true);
      final LinearLayoutManager var4 = new LinearLayoutManager(var1.getContext(), 1, false);
      ((RecyclerView)this.a(co.uk.getmondo.c.a.feedRecyclerView)).setLayoutManager((android.support.v7.widget.RecyclerView.h)var4);
      co.uk.getmondo.feed.adapter.a var5 = this.a;
      if(var5 == null) {
         l.b("feedAdapter");
      }

      var5.registerAdapterDataObserver((android.support.v7.widget.RecyclerView.c)(new android.support.v7.widget.RecyclerView.c() {
         public void b(int var1, int var2) {
            super.b(var1, var2);
            var4.b(0, 0);
         }
      }));
      co.uk.getmondo.feed.adapter.a var13 = this.a;
      if(var13 == null) {
         l.b("feedAdapter");
      }

      var13.a((co.uk.getmondo.feed.adapter.a.a)this);
      RecyclerView var15 = (RecyclerView)this.a(co.uk.getmondo.c.a.feedRecyclerView);
      var13 = this.a;
      if(var13 == null) {
         l.b("feedAdapter");
      }

      var15.setAdapter((android.support.v7.widget.RecyclerView.a)var13);
      ((RecyclerView)this.a(co.uk.getmondo.c.a.feedRecyclerView)).setHasFixedSize(true);
      int var3 = this.getResources().getDimensionPixelSize(2131427609);
      RecyclerView var14 = (RecyclerView)this.a(co.uk.getmondo.c.a.feedRecyclerView);
      Context var6 = var1.getContext();
      var5 = this.a;
      if(var5 == null) {
         l.b("feedAdapter");
      }

      var14.a((android.support.v7.widget.RecyclerView.g)(new co.uk.getmondo.common.ui.h(var6, (a.a.a.a.a.a)var5, var3)));
      RecyclerView var7 = (RecyclerView)this.a(co.uk.getmondo.c.a.feedRecyclerView);
      var13 = this.a;
      if(var13 == null) {
         l.b("feedAdapter");
      }

      var7.a((android.support.v7.widget.RecyclerView.g)(new a.a.a.a.a.b((a.a.a.a.a.a)var13)));
      ((SwipeRefreshLayout)this.a(co.uk.getmondo.c.a.feedSwipeToRefreshLayout)).setColorSchemeResources(new int[]{2131689558});
      if(var2 != null) {
         Parcelable var8 = var2.getParcelable(this.e);
         ((RecyclerView)this.a(co.uk.getmondo.c.a.feedRecyclerView)).getLayoutManager().a(var8);
      }

      android.support.v4.app.j var9 = this.getActivity();
      if(var9 == null) {
         throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.main.HomeActivity");
      } else {
         HomeActivity var11 = (HomeActivity)var9;
         var11.setSupportActionBar((Toolbar)this.a(co.uk.getmondo.c.a.toolbar));
         Toolbar var10 = (Toolbar)this.a(co.uk.getmondo.c.a.toolbar);
         l.a(var10, "toolbar");
         var11.a(var10);
         g var12 = this.c;
         if(var12 == null) {
            l.b("homeFeedPresenter");
         }

         var12.a((g.a)this);
      }
   }
}
