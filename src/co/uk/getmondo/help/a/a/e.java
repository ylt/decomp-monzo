package co.uk.getmondo.help.a.a;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import co.uk.getmondo.api.model.help.Topic;
import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0004\b\u0086\b\u0018\u0000  2\u00020\u0001:\u0001 B\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004B\u001f\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\b\b\u0002\u0010\t\u001a\u00020\u0006¢\u0006\u0002\u0010\nJ\t\u0010\u0010\u001a\u00020\u0006HÆ\u0003J\t\u0010\u0011\u001a\u00020\bHÆ\u0003J\t\u0010\u0012\u001a\u00020\u0006HÆ\u0003J'\u0010\u0013\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u00062\b\b\u0002\u0010\u0007\u001a\u00020\b2\b\b\u0002\u0010\t\u001a\u00020\u0006HÆ\u0001J\b\u0010\u0014\u001a\u00020\u0015H\u0016J\u0013\u0010\u0016\u001a\u00020\u00172\b\u0010\u0018\u001a\u0004\u0018\u00010\u0019HÖ\u0003J\t\u0010\u001a\u001a\u00020\u0015HÖ\u0001J\t\u0010\u001b\u001a\u00020\u0006HÖ\u0001J\u0018\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u00032\u0006\u0010\u001f\u001a\u00020\u0015H\u0016R\u0011\u0010\t\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\fR\u0011\u0010\u0007\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000f¨\u0006!"},
   d2 = {"Lco/uk/getmondo/help/data/model/TopicViewModel;", "Landroid/os/Parcelable;", "source", "Landroid/os/Parcel;", "(Landroid/os/Parcel;)V", "title", "", "topic", "Lco/uk/getmondo/api/model/help/Topic;", "query", "(Ljava/lang/String;Lco/uk/getmondo/api/model/help/Topic;Ljava/lang/String;)V", "getQuery", "()Ljava/lang/String;", "getTitle", "getTopic", "()Lco/uk/getmondo/api/model/help/Topic;", "component1", "component2", "component3", "copy", "describeContents", "", "equals", "", "other", "", "hashCode", "toString", "writeToParcel", "", "dest", "flags", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class e implements Parcelable {
   public static final Creator CREATOR = (Creator)(new Creator() {
      public e a(Parcel var1) {
         l.b(var1, "source");
         return new e(var1);
      }

      public e[] a(int var1) {
         return new e[var1];
      }

      // $FF: synthetic method
      public Object createFromParcel(Parcel var1) {
         return this.a(var1);
      }

      // $FF: synthetic method
      public Object[] newArray(int var1) {
         return (Object[])this.a(var1);
      }
   });
   public static final e.a a = new e.a((i)null);
   private final String b;
   private final Topic c;
   private final String d;

   public e(Parcel var1) {
      l.b(var1, "source");
      String var2 = var1.readString();
      l.a(var2, "source.readString()");
      Parcelable var3 = var1.readParcelable(Topic.class.getClassLoader());
      l.a(var3, "source.readParcelable<To…::class.java.classLoader)");
      Topic var5 = (Topic)var3;
      String var4 = var1.readString();
      l.a(var4, "source.readString()");
      this(var2, var5, var4);
   }

   public e(String var1, Topic var2, String var3) {
      l.b(var1, "title");
      l.b(var2, "topic");
      l.b(var3, "query");
      super();
      this.b = var1;
      this.c = var2;
      this.d = var3;
   }

   // $FF: synthetic method
   public e(String var1, Topic var2, String var3, int var4, i var5) {
      if((var4 & 4) != 0) {
         var3 = "";
      }

      this(var1, var2, var3);
   }

   public final String a() {
      return this.b;
   }

   public final Topic b() {
      return this.c;
   }

   public final String c() {
      return this.d;
   }

   public int describeContents() {
      return 0;
   }

   public boolean equals(Object var1) {
      boolean var2;
      if(this != var1) {
         label30: {
            if(var1 instanceof e) {
               e var3 = (e)var1;
               if(l.a(this.b, var3.b) && l.a(this.c, var3.c) && l.a(this.d, var3.d)) {
                  break label30;
               }
            }

            var2 = false;
            return var2;
         }
      }

      var2 = true;
      return var2;
   }

   public int hashCode() {
      int var3 = 0;
      String var4 = this.b;
      int var1;
      if(var4 != null) {
         var1 = var4.hashCode();
      } else {
         var1 = 0;
      }

      Topic var5 = this.c;
      int var2;
      if(var5 != null) {
         var2 = var5.hashCode();
      } else {
         var2 = 0;
      }

      var4 = this.d;
      if(var4 != null) {
         var3 = var4.hashCode();
      }

      return (var2 + var1 * 31) * 31 + var3;
   }

   public String toString() {
      return "TopicViewModel(title=" + this.b + ", topic=" + this.c + ", query=" + this.d + ")";
   }

   public void writeToParcel(Parcel var1, int var2) {
      l.b(var1, "dest");
      var1.writeString(this.b);
      var1.writeParcelable((Parcelable)this.c, 0);
      var1.writeString(this.d);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"},
      d2 = {"Lco/uk/getmondo/help/data/model/TopicViewModel$Companion;", "", "()V", "CREATOR", "Landroid/os/Parcelable$Creator;", "Lco/uk/getmondo/help/data/model/TopicViewModel;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(i var1) {
         this();
      }
   }
}
