package co.uk.getmondo.help.a;

import co.uk.getmondo.api.HelpApi;
import co.uk.getmondo.api.model.help.Content;
import co.uk.getmondo.api.model.help.SearchQuery;
import co.uk.getmondo.api.model.help.Section;
import co.uk.getmondo.api.model.help.Topic;
import co.uk.getmondo.common.v;
import co.uk.getmondo.help.a.a.d;
import io.reactivex.c.g;
import io.reactivex.c.h;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.a.m;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0017\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u001a\u0010\r\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00100\u000f0\u000e2\u0006\u0010\u0011\u001a\u00020\bJ\u0012\u0010\u0012\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00130\u000f0\u000eJ\u001a\u0010\u0014\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00100\u000f0\u000e2\u0006\u0010\u0015\u001a\u00020\bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\u0007\u001a\u00020\bX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\t\u0010\n\"\u0004\b\u000b\u0010\f¨\u0006\u0016"},
   d2 = {"Lco/uk/getmondo/help/data/HelpManager;", "", "helpApi", "Lco/uk/getmondo/api/HelpApi;", "resourceProvider", "Lco/uk/getmondo/common/ResourceProvider;", "(Lco/uk/getmondo/api/HelpApi;Lco/uk/getmondo/common/ResourceProvider;)V", "trendingTitle", "", "getTrendingTitle", "()Ljava/lang/String;", "setTrendingTitle", "(Ljava/lang/String;)V", "loadCategoryItems", "Lio/reactivex/Single;", "", "Lco/uk/getmondo/help/data/model/SectionItem;", "id", "loadTrendingTopics", "Lco/uk/getmondo/api/model/help/Topic;", "search", "query", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class a {
   private String a;
   private final HelpApi b;
   private final v c;

   public a(HelpApi var1, v var2) {
      l.b(var1, "helpApi");
      l.b(var2, "resourceProvider");
      super();
      this.b = var1;
      this.c = var2;
      this.a = "";
   }

   public final String a() {
      return this.a;
   }

   public final void a(String var1) {
      l.b(var1, "<set-?>");
      this.a = var1;
   }

   public final io.reactivex.v b() {
      io.reactivex.v var1 = this.b.trending().c((g)(new g() {
         public final void a(Content var1) {
            a.this.a(((Section)m.e(var1.a())).a());
         }
      })).d((h)null.a);
      l.a(var1, "helpApi.trending()\n     ….first().topics.take(5) }");
      return var1;
   }

   public final io.reactivex.v b(String var1) {
      l.b(var1, "id");
      io.reactivex.v var2 = this.b.categories(var1).d((h)(new h() {
         public final List a(Content var1) {
            l.b(var1, "<name for destructuring parameter 0>");
            List var2 = var1.b();
            List var5 = (List)(new ArrayList());

            Collection var8;
            for(Iterator var6 = var2.iterator(); var6.hasNext(); var8 = (Collection)var5) {
               Section var4 = (Section)var6.next();
               String var3 = var4.c();
               List var9 = var4.d();
               var5.add(new d.b(var3));
               Iterator var10 = ((Iterable)var9).iterator();

               while(var10.hasNext()) {
                  Object var7 = var10.next();
                  ((Collection)var5).add(new d.c((Topic)var7));
               }
            }

            ((Collection)var5).add(new d.a(a.this.c.a(2131362239)));
            return var5;
         }
      }));
      l.a(var2, "helpApi.categories(id)\n …ryItems\n                }");
      return var2;
   }

   public final io.reactivex.v c(String var1) {
      l.b(var1, "query");
      io.reactivex.v var2 = this.b.search(var1).d((h)(new h() {
         public final List a(SearchQuery var1) {
            l.b(var1, "<name for destructuring parameter 0>");
            List var2 = var1.a();
            List var4 = (List)(new ArrayList());
            ((Collection)var4).add(new d.b(a.this.c.a(2131362237)));
            Iterator var5 = ((Iterable)((Section)m.e(var2)).b()).iterator();

            while(var5.hasNext()) {
               Object var3 = var5.next();
               ((Collection)var4).add(new d.c((Topic)var3));
            }

            Collection var6 = (Collection)var4;
            ((Collection)var4).add(new d.a(a.this.c.a(2131362233)));
            return var4;
         }
      }));
      l.a(var2, "helpApi.search(query)\n  …icItems\n                }");
      return var2;
   }
}
