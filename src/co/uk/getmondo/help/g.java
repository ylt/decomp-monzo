package co.uk.getmondo.help;

import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   k = 3,
   mv = {1, 1, 7}
)
final class g implements io.reactivex.c.g {
   // $FF: synthetic field
   private final kotlin.d.a.b a;

   g(kotlin.d.a.b var1) {
      this.a = var1;
   }

   // $FF: synthetic method
   public final void a(Object var1) {
      kotlin.d.b.l.a(this.a.a(var1), "invoke(...)");
   }
}
