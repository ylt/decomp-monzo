package co.uk.getmondo.help;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.Group;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import co.uk.getmondo.api.model.help.Topic;
import co.uk.getmondo.common.ae;
import co.uk.getmondo.help.ui.HelpCategoryView;
import com.google.android.flexbox.FlexboxLayout;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000v\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\b\u0002\u0018\u0000 :2\u00020\u00012\u00020\u0002:\u0001:B\u0005¢\u0006\u0002\u0010\u0003J\u0012\u0010&\u001a\u00020\u00112\b\u0010'\u001a\u0004\u0018\u00010(H\u0014J\u0010\u0010)\u001a\u00020\u00112\u0006\u0010*\u001a\u00020+H\u0016J\b\u0010,\u001a\u00020\u0011H\u0016J\u0010\u0010-\u001a\u00020\u00112\u0006\u0010.\u001a\u00020+H\u0016J\b\u0010/\u001a\u00020\u0011H\u0016J\u0018\u00100\u001a\u00020\u00112\u0006\u00101\u001a\u00020$2\u0006\u00102\u001a\u00020+H\u0016J\u0010\u00103\u001a\u00020\u00112\u0006\u00104\u001a\u000205H\u0016J\u0010\u00106\u001a\u00020\u00112\u0006\u00104\u001a\u000205H\u0016J\u0016\u00107\u001a\u00020\u00112\f\u00108\u001a\b\u0012\u0004\u0012\u00020$09H\u0016R2\u0010\u0004\u001a&\u0012\f\u0012\n \u0007*\u0004\u0018\u00010\u00060\u0006 \u0007*\u0012\u0012\f\u0012\n \u0007*\u0004\u0018\u00010\u00060\u0006\u0018\u00010\u00050\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\b\u001a\b\u0012\u0004\u0012\u00020\n0\t8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\fR\u001a\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000e0\t8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u000f\u0010\fR\u001a\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00110\t8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0012\u0010\fR\u001e\u0010\u0013\u001a\u00020\u00148\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\u0015\u0010\u0016\"\u0004\b\u0017\u0010\u0018R\u001e\u0010\u0019\u001a\u00020\u001a8\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\u001b\u0010\u001c\"\u0004\b\u001d\u0010\u001eR\u001a\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020\u00110\t8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b \u0010\fR\u000e\u0010!\u001a\u00020\"X\u0082\u0004¢\u0006\u0002\n\u0000R\u001a\u0010#\u001a\b\u0012\u0004\u0012\u00020$0\t8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b%\u0010\f¨\u0006;"},
   d2 = {"Lco/uk/getmondo/help/HelpActivity;", "Lco/uk/getmondo/common/activities/BaseActivity;", "Lco/uk/getmondo/help/HelpPresenter$View;", "()V", "categoryClickRelay", "Lcom/jakewharton/rxrelay2/PublishRelay;", "Lco/uk/getmondo/help/data/model/Category;", "kotlin.jvm.PlatformType", "categoryClicked", "Lio/reactivex/Observable;", "Lco/uk/getmondo/help/data/model/HelpCategory;", "getCategoryClicked", "()Lio/reactivex/Observable;", "communityForumClicked", "Lco/uk/getmondo/help/data/model/CommunityCategory;", "getCommunityForumClicked", "helpClicked", "", "getHelpClicked", "intercomService", "Lco/uk/getmondo/common/IntercomService;", "getIntercomService", "()Lco/uk/getmondo/common/IntercomService;", "setIntercomService", "(Lco/uk/getmondo/common/IntercomService;)V", "presenter", "Lco/uk/getmondo/help/HelpPresenter;", "getPresenter", "()Lco/uk/getmondo/help/HelpPresenter;", "setPresenter", "(Lco/uk/getmondo/help/HelpPresenter;)V", "searchClicked", "getSearchClicked", "trendingAdapter", "Lco/uk/getmondo/help/TrendingAdapter;", "trendingTopicClicked", "Lco/uk/getmondo/api/model/help/Topic;", "getTrendingTopicClicked", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "openCategory", "categoryId", "", "openChat", "openCommunity", "url", "openSearch", "openTopic", "topic", "title", "setTrendingError", "isEnabled", "", "setTrendingLoading", "showTrendingTopics", "topics", "", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class HelpActivity extends co.uk.getmondo.common.activities.b implements f.a {
   public static final HelpActivity.a c = new HelpActivity.a((kotlin.d.b.i)null);
   public f a;
   public co.uk.getmondo.common.q b;
   private final r e = new r((List)null, (kotlin.d.a.b)null, 3, (kotlin.d.b.i)null);
   private final com.b.b.c f = com.b.b.c.a();
   private HashMap g;

   public View a(int var1) {
      if(this.g == null) {
         this.g = new HashMap();
      }

      View var3 = (View)this.g.get(Integer.valueOf(var1));
      View var2 = var3;
      if(var3 == null) {
         var2 = this.findViewById(var1);
         this.g.put(Integer.valueOf(var1), var2);
      }

      return var2;
   }

   public io.reactivex.n a() {
      io.reactivex.n var1 = this.f.filter((io.reactivex.c.q)null.a).map((io.reactivex.c.h)null.a);
      kotlin.d.b.l.a(var1, "categoryClickRelay\n     …elpCategory.from(it.id) }");
      return var1;
   }

   public void a(Topic var1, String var2) {
      kotlin.d.b.l.b(var1, "topic");
      kotlin.d.b.l.b(var2, "title");
      this.startActivity(HelpTopicActivity.e.a((Context)this, new co.uk.getmondo.help.a.a.e(var2, var1, (String)null, 4, (kotlin.d.b.i)null)));
   }

   public void a(String var1) {
      kotlin.d.b.l.b(var1, "categoryId");
      this.startActivity(HelpCategoryActivity.e.a((Context)this, var1));
   }

   public void a(List var1) {
      kotlin.d.b.l.b(var1, "topics");
      this.e.a(var1);
   }

   public void a(boolean var1) {
      if(var1) {
         ae.a((View)((ProgressBar)this.a(co.uk.getmondo.c.a.trendingProgress)));
      } else {
         ae.b((ProgressBar)this.a(co.uk.getmondo.c.a.trendingProgress));
      }

   }

   public io.reactivex.n b() {
      io.reactivex.n var1 = io.reactivex.n.create((io.reactivex.p)(new io.reactivex.p() {
         public final void a(final io.reactivex.o var1) {
            kotlin.d.b.l.b(var1, "emitter");
            HelpActivity.this.e.a((kotlin.d.a.b)(new kotlin.d.a.b() {
               public final void a(Topic var1x) {
                  kotlin.d.b.l.b(var1x, "it");
                  var1.a(var1x);
               }
            }));
            var1.a((io.reactivex.c.f)(new io.reactivex.c.f() {
               public final void a() {
                  HelpActivity.this.e.a((kotlin.d.a.b)null);
               }
            }));
         }
      }));
      kotlin.d.b.l.a(var1, "Observable.create { emit…stener = null }\n        }");
      return var1;
   }

   public void b(boolean var1) {
      if(var1) {
         ae.a((View)((TextView)this.a(co.uk.getmondo.c.a.trendingErrorText)));
      } else {
         ae.b((TextView)this.a(co.uk.getmondo.c.a.trendingErrorText));
      }

   }

   public io.reactivex.n c() {
      io.reactivex.n var1 = this.f.filter((io.reactivex.c.q)null.a).map((io.reactivex.c.h)null.a);
      kotlin.d.b.l.a(var1, "categoryClickRelay\n     …ityCategory.from(it.id) }");
      return var1;
   }

   public io.reactivex.n d() {
      io.reactivex.n var1 = com.b.a.c.c.a((Button)this.a(co.uk.getmondo.c.a.helpButton)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var1, "RxView.clicks(this).map(VoidToUnit)");
      return var1;
   }

   public void d(String var1) {
      kotlin.d.b.l.b(var1, "url");
      co.uk.getmondo.common.activities.a.a(this, var1, 2131689487, true);
   }

   public io.reactivex.n e() {
      io.reactivex.n var1 = com.b.a.c.c.a((LinearLayout)this.a(co.uk.getmondo.c.a.helpSearchLayout)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var1, "RxView.clicks(this).map(VoidToUnit)");
      return var1;
   }

   public void f() {
      this.startActivity(HelpSearchActivity.c.a((Context)this));
   }

   public void g() {
      co.uk.getmondo.common.q var1 = this.b;
      if(var1 == null) {
         kotlin.d.b.l.b("intercomService");
      }

      var1.a();
   }

   protected void onCreate(Bundle var1) {
      byte var3 = 0;
      super.onCreate(var1);
      this.setContentView(2131034173);
      this.l().a(this);
      Iterable var4 = (Iterable)co.uk.getmondo.common.ui.m.a((FlexboxLayout)this.a(co.uk.getmondo.c.a.categoriesLayout));
      Collection var6 = (Collection)(new ArrayList());
      Iterator var5 = var4.iterator();

      Object var11;
      while(var5.hasNext()) {
         var11 = var5.next();
         if(var11 instanceof HelpCategoryView) {
            var6.add(var11);
         }
      }

      Iterable var7 = (Iterable)((List)var6);
      Iterator var8 = var7.iterator();

      int var2;
      HelpCategoryView var12;
      for(var2 = 0; var8.hasNext(); ++var2) {
         var12 = (HelpCategoryView)var8.next();
         var12.a((co.uk.getmondo.help.a.a.a)co.uk.getmondo.help.a.a.c.values()[var2]);
         var12.setClickListener((kotlin.d.a.b)(new kotlin.d.a.b() {
            public final void a(co.uk.getmondo.help.a.a.a var1) {
               HelpActivity.this.f.a((Object)var1);
            }
         }));
      }

      LinearLayoutManager var9 = new LinearLayoutManager((Context)this);
      ((RecyclerView)this.a(co.uk.getmondo.c.a.trendingRecyclerView)).setLayoutManager((android.support.v7.widget.RecyclerView.h)var9);
      ((RecyclerView)this.a(co.uk.getmondo.c.a.trendingRecyclerView)).setAdapter((android.support.v7.widget.RecyclerView.a)this.e);
      ((RecyclerView)this.a(co.uk.getmondo.c.a.trendingRecyclerView)).setHasFixedSize(true);
      int[] var13 = ((Group)this.a(co.uk.getmondo.c.a.communityCategoryGroup)).getReferencedIds();
      var6 = (Collection)(new ArrayList(var13.length));

      for(var2 = 0; var2 < var13.length; ++var2) {
         var6.add(this.findViewById(var13[var2]));
      }

      var4 = (Iterable)((List)var6);
      var6 = (Collection)(new ArrayList());
      var5 = var4.iterator();

      while(var5.hasNext()) {
         var11 = var5.next();
         if(var11 instanceof HelpCategoryView) {
            var6.add(var11);
         }
      }

      var7 = (Iterable)((List)var6);
      var8 = var7.iterator();

      for(var2 = var3; var8.hasNext(); ++var2) {
         var12 = (HelpCategoryView)var8.next();
         var12.a((co.uk.getmondo.help.a.a.a)co.uk.getmondo.help.a.a.b.values()[var2]);
         var12.setClickListener((kotlin.d.a.b)(new kotlin.d.a.b() {
            public final void a(co.uk.getmondo.help.a.a.a var1) {
               HelpActivity.this.f.a((Object)var1);
            }
         }));
      }

      f var10 = this.a;
      if(var10 == null) {
         kotlin.d.b.l.b("presenter");
      }

      var10.a((f.a)this);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007¨\u0006\u0007"},
      d2 = {"Lco/uk/getmondo/help/HelpActivity$Companion;", "", "()V", "buildIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var1) {
         this();
      }

      public final Intent a(Context var1) {
         kotlin.d.b.l.b(var1, "context");
         return new Intent(var1, HelpActivity.class);
      }
   }
}
