package co.uk.getmondo.help;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.Group;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import co.uk.getmondo.common.ae;
import co.uk.getmondo.common.ui.LoadingErrorView;
import java.util.HashMap;
import java.util.List;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 72\u00020\u00012\u00020\u0002:\u00017B\u0005¢\u0006\u0002\u0010\u0003J\b\u0010#\u001a\u00020$H\u0016J\b\u0010%\u001a\u00020$H\u0016J\u0012\u0010&\u001a\u00020$2\b\u0010'\u001a\u0004\u0018\u00010(H\u0014J\u0010\u0010)\u001a\u00020$2\u0006\u0010*\u001a\u00020\u0006H\u0016J\u0010\u0010+\u001a\u00020$2\u0006\u0010,\u001a\u00020!H\u0016J\u0010\u0010-\u001a\u00020$2\u0006\u0010.\u001a\u00020/H\u0016J\u0010\u00100\u001a\u00020$2\u0006\u0010*\u001a\u00020\u0006H\u0016J\b\u00101\u001a\u00020$H\u0016J\b\u00102\u001a\u00020$H\u0016J\u0016\u00103\u001a\u00020$2\f\u00104\u001a\b\u0012\u0004\u0012\u00020605H\u0016R\u001a\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0007\u0010\bR\u001e\u0010\t\u001a\u00020\n8\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u001e\u0010\u000f\u001a\u00020\u00108\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014R\u001a\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0016\u0010\bR\u0014\u0010\u0017\u001a\u00020\u00068BX\u0082\u0004¢\u0006\u0006\u001a\u0004\b\u0018\u0010\u0019R\u001a\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u001b\u0010\bR\u000e\u0010\u001c\u001a\u00020\u001dX\u0082\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u001f\u0010\bR\u001a\u0010 \u001a\b\u0012\u0004\u0012\u00020!0\u00058VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\"\u0010\b¨\u00068"},
   d2 = {"Lco/uk/getmondo/help/HelpSearchActivity;", "Lco/uk/getmondo/common/activities/BaseActivity;", "Lco/uk/getmondo/help/HelpSearchPresenter$View;", "()V", "helpClicked", "Lio/reactivex/Observable;", "", "getHelpClicked", "()Lio/reactivex/Observable;", "intercomService", "Lco/uk/getmondo/common/IntercomService;", "getIntercomService", "()Lco/uk/getmondo/common/IntercomService;", "setIntercomService", "(Lco/uk/getmondo/common/IntercomService;)V", "presenter", "Lco/uk/getmondo/help/HelpSearchPresenter;", "getPresenter", "()Lco/uk/getmondo/help/HelpSearchPresenter;", "setPresenter", "(Lco/uk/getmondo/help/HelpSearchPresenter;)V", "queryChanged", "getQueryChanged", "queryString", "getQueryString", "()Ljava/lang/String;", "retryClicked", "getRetryClicked", "sectionAdapter", "Lco/uk/getmondo/help/HelpSectionAdapter;", "suggestionClicked", "getSuggestionClicked", "topicClicked", "Lco/uk/getmondo/help/data/model/TopicViewModel;", "getTopicClicked", "hideSuggestions", "", "hideTopics", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "openChat", "query", "openTopic", "topicViewModel", "setLoading", "enabled", "", "setQuery", "showError", "showSuggestions", "showTopics", "topics", "", "Lco/uk/getmondo/help/data/model/SectionItem;", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class HelpSearchActivity extends co.uk.getmondo.common.activities.b implements j.a {
   public static final HelpSearchActivity.a c = new HelpSearchActivity.a((kotlin.d.b.i)null);
   public j a;
   public co.uk.getmondo.common.q b;
   private final m e = new m((List)null, (kotlin.d.a.b)null, (kotlin.d.a.a)null, 7, (kotlin.d.b.i)null);
   private HashMap f;

   private final String j() {
      return ((EditText)this.a(co.uk.getmondo.c.a.queryField)).getText().toString();
   }

   public View a(int var1) {
      if(this.f == null) {
         this.f = new HashMap();
      }

      View var3 = (View)this.f.get(Integer.valueOf(var1));
      View var2 = var3;
      if(var3 == null) {
         var2 = this.findViewById(var1);
         this.f.put(Integer.valueOf(var1), var2);
      }

      return var2;
   }

   public io.reactivex.n a() {
      io.reactivex.n var1 = com.b.a.c.c.a((TextView)this.a(co.uk.getmondo.c.a.helpSearchSuggestion1)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var1, "RxView.clicks(this).map(VoidToUnit)");
      io.reactivex.r var4 = (io.reactivex.r)var1.map((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final CharSequence a(kotlin.n var1) {
            kotlin.d.b.l.b(var1, "it");
            return ((TextView)HelpSearchActivity.this.a(co.uk.getmondo.c.a.helpSearchSuggestion1)).getText();
         }
      }));
      io.reactivex.n var2 = com.b.a.c.c.a((TextView)this.a(co.uk.getmondo.c.a.helpSearchSuggestion2)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var2, "RxView.clicks(this).map(VoidToUnit)");
      io.reactivex.r var5 = (io.reactivex.r)var2.map((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final CharSequence a(kotlin.n var1) {
            kotlin.d.b.l.b(var1, "it");
            return ((TextView)HelpSearchActivity.this.a(co.uk.getmondo.c.a.helpSearchSuggestion2)).getText();
         }
      }));
      io.reactivex.n var3 = com.b.a.c.c.a((TextView)this.a(co.uk.getmondo.c.a.helpSearchSuggestion3)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var3, "RxView.clicks(this).map(VoidToUnit)");
      var1 = io.reactivex.n.merge(var4, var5, (io.reactivex.r)var3.map((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final CharSequence a(kotlin.n var1) {
            kotlin.d.b.l.b(var1, "it");
            return ((TextView)HelpSearchActivity.this.a(co.uk.getmondo.c.a.helpSearchSuggestion3)).getText();
         }
      }))).map((io.reactivex.c.h)null.a);
      kotlin.d.b.l.a(var1, "Observable.merge(\n      …   .map { it.toString() }");
      return var1;
   }

   public void a(co.uk.getmondo.help.a.a.e var1) {
      kotlin.d.b.l.b(var1, "topicViewModel");
      this.startActivity(HelpTopicActivity.e.a((Context)this, var1));
   }

   public void a(String var1) {
      kotlin.d.b.l.b(var1, "query");
      ((EditText)this.a(co.uk.getmondo.c.a.queryField)).setText((CharSequence)"");
      ((EditText)this.a(co.uk.getmondo.c.a.queryField)).append((CharSequence)var1);
   }

   public void a(List var1) {
      kotlin.d.b.l.b(var1, "topics");
      this.e.a(var1);
      ((RecyclerView)this.a(co.uk.getmondo.c.a.helpSearchRecyclerView)).a(0);
      ae.a((View)((RecyclerView)this.a(co.uk.getmondo.c.a.helpSearchRecyclerView)));
   }

   public void a(boolean var1) {
      ((LoadingErrorView)this.a(co.uk.getmondo.c.a.loadingErrorView)).a(var1, this.getString(2131362236));
      if(var1) {
         ae.a((View)((LoadingErrorView)this.a(co.uk.getmondo.c.a.loadingErrorView)));
      } else {
         ae.b((LoadingErrorView)this.a(co.uk.getmondo.c.a.loadingErrorView));
      }

   }

   public io.reactivex.n b() {
      com.b.a.a var1 = com.b.a.d.e.c((EditText)this.a(co.uk.getmondo.c.a.queryField));
      kotlin.d.b.l.a(var1, "RxTextView.textChanges(this)");
      io.reactivex.n var2 = var1.b().debounce((io.reactivex.c.h)null.a).map((io.reactivex.c.h)null.a).observeOn(io.reactivex.a.b.a.a());
      kotlin.d.b.l.a(var2, "queryField.textChanges()…dSchedulers.mainThread())");
      return var2;
   }

   public io.reactivex.n c() {
      io.reactivex.n var1 = io.reactivex.n.create((io.reactivex.p)(new io.reactivex.p() {
         public final void a(final io.reactivex.o var1) {
            kotlin.d.b.l.b(var1, "emitter");
            HelpSearchActivity.this.e.a((kotlin.d.a.b)(new kotlin.d.a.b() {
               public final void a(co.uk.getmondo.help.a.a.d.c var1x) {
                  kotlin.d.b.l.b(var1x, "it");
                  io.reactivex.o var3 = var1;
                  String var2 = HelpSearchActivity.this.getString(2131362238);
                  kotlin.d.b.l.a(var2, "getString(R.string.help_search_title)");
                  var3.a(new co.uk.getmondo.help.a.a.e(var2, var1x.c(), HelpSearchActivity.this.j()));
               }
            }));
            var1.a((io.reactivex.c.f)(new io.reactivex.c.f() {
               public final void a() {
                  HelpSearchActivity.this.e.a((kotlin.d.a.b)null);
               }
            }));
         }
      }));
      kotlin.d.b.l.a(var1, "Observable.create { emit…stener = null }\n        }");
      return var1;
   }

   public io.reactivex.n d() {
      io.reactivex.n var1 = io.reactivex.n.create((io.reactivex.p)(new io.reactivex.p() {
         public final void a(final io.reactivex.o var1) {
            kotlin.d.b.l.b(var1, "emitter");
            HelpSearchActivity.this.e.a((kotlin.d.a.a)(new kotlin.d.a.a() {
               public final void b() {
                  var1.a(HelpSearchActivity.this.j());
               }

               // $FF: synthetic method
               public Object v_() {
                  this.b();
                  return kotlin.n.a;
               }
            }));
            var1.a((io.reactivex.c.f)(new io.reactivex.c.f() {
               public final void a() {
                  HelpSearchActivity.this.e.a((kotlin.d.a.a)null);
               }
            }));
         }
      }));
      kotlin.d.b.l.a(var1, "Observable.create { emit…stener = null }\n        }");
      return var1;
   }

   public void d(String var1) {
      kotlin.d.b.l.b(var1, "query");
      co.uk.getmondo.common.q var2 = this.b;
      if(var2 == null) {
         kotlin.d.b.l.b("intercomService");
      }

      var2.b(var1);
   }

   public io.reactivex.n e() {
      io.reactivex.n var1 = ((LoadingErrorView)this.a(co.uk.getmondo.c.a.loadingErrorView)).c().map((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final String a(kotlin.n var1) {
            kotlin.d.b.l.b(var1, "it");
            return HelpSearchActivity.this.j();
         }
      }));
      kotlin.d.b.l.a(var1, "loadingErrorView.retryClicks().map { queryString }");
      return var1;
   }

   public void f() {
      ae.a((View)((Group)this.a(co.uk.getmondo.c.a.helpSearchSuggestions)));
   }

   public void g() {
      ae.b((RecyclerView)this.a(co.uk.getmondo.c.a.helpSearchRecyclerView));
   }

   public void h() {
      ae.b((Group)this.a(co.uk.getmondo.c.a.helpSearchSuggestions));
   }

   public void i() {
      LoadingErrorView var2 = (LoadingErrorView)this.a(co.uk.getmondo.c.a.loadingErrorView);
      String var1 = this.getString(2131362234);
      kotlin.d.b.l.a(var1, "getString(R.string.help_search_error)");
      LoadingErrorView.a(var2, var1, false, (String)null, 2130837839, 6, (Object)null);
      ae.a((View)((LoadingErrorView)this.a(co.uk.getmondo.c.a.loadingErrorView)));
   }

   protected void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.setContentView(2131034175);
      this.l().a(this);
      String var2 = this.getString(2131362235);
      SpannableString var3 = new SpannableString((CharSequence)var2);
      var3.setSpan(new RelativeSizeSpan(0.9F), 0, var2.length(), 33);
      ((EditText)this.a(co.uk.getmondo.c.a.queryField)).setHint((CharSequence)var3);
      LinearLayoutManager var4 = new LinearLayoutManager((Context)this);
      ((RecyclerView)this.a(co.uk.getmondo.c.a.helpSearchRecyclerView)).setLayoutManager((android.support.v7.widget.RecyclerView.h)var4);
      ((RecyclerView)this.a(co.uk.getmondo.c.a.helpSearchRecyclerView)).setAdapter((android.support.v7.widget.RecyclerView.a)this.e);
      ((RecyclerView)this.a(co.uk.getmondo.c.a.helpSearchRecyclerView)).setHasFixedSize(true);
      j var5 = this.a;
      if(var5 == null) {
         kotlin.d.b.l.b("presenter");
      }

      var5.a((j.a)this);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\u0007"},
      d2 = {"Lco/uk/getmondo/help/HelpSearchActivity$Companion;", "", "()V", "buildIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var1) {
         this();
      }

      public final Intent a(Context var1) {
         kotlin.d.b.l.b(var1, "context");
         return new Intent(var1, HelpSearchActivity.class);
      }
   }
}
