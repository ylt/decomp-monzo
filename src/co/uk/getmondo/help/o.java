package co.uk.getmondo.help;

import co.uk.getmondo.api.model.help.Topic;
import co.uk.getmondo.api.model.tracking.Impression;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u000bB\u0017\b\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J\u0010\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0002H\u0016R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\f"},
   d2 = {"Lco/uk/getmondo/help/HelpTopicPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/help/HelpTopicPresenter$View;", "topicViewModel", "Lco/uk/getmondo/help/data/model/TopicViewModel;", "analyticsService", "Lco/uk/getmondo/common/AnalyticsService;", "(Lco/uk/getmondo/help/data/model/TopicViewModel;Lco/uk/getmondo/common/AnalyticsService;)V", "register", "", "view", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class o extends co.uk.getmondo.common.ui.b {
   private final co.uk.getmondo.help.a.a.e c;
   private final co.uk.getmondo.common.a d;

   public o(co.uk.getmondo.help.a.a.e var1, co.uk.getmondo.common.a var2) {
      kotlin.d.b.l.b(var1, "topicViewModel");
      kotlin.d.b.l.b(var2, "analyticsService");
      super();
      this.c = var1;
      this.d = var2;
   }

   public void a(final o.a var1) {
      kotlin.d.b.l.b(var1, "view");
      super.a((co.uk.getmondo.common.ui.f)var1);
      var1.a(this.c.a());
      var1.a(this.c.b());
      io.reactivex.b.a var4 = this.b;
      io.reactivex.n var5 = var1.a();
      io.reactivex.c.g var6 = (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n var1x) {
            o.this.d.a(Impression.Companion.a(Impression.HelpOutcome.THUMBS_UP, o.this.c.b().a()));
            var1.d();
         }
      });
      kotlin.d.a.b var3 = (kotlin.d.a.b)null.a;
      Object var2 = var3;
      if(var3 != null) {
         var2 = new p(var3);
      }

      io.reactivex.b.b var9 = var5.subscribe(var6, (io.reactivex.c.g)var2);
      kotlin.d.b.l.a(var9, "view.feedbackPositiveCli…            }, Timber::e)");
      this.b = co.uk.getmondo.common.j.f.a(var4, var9);
      io.reactivex.b.a var10 = this.b;
      io.reactivex.n var12 = var1.b();
      io.reactivex.c.g var13 = (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n var1x) {
            o.this.d.a(Impression.Companion.a(Impression.Companion, Impression.IntercomFrom.HELP, o.this.c.b().a(), (String)null, 4, (Object)null));
            o.this.d.a(Impression.Companion.a(Impression.HelpOutcome.THUMBS_DOWN, o.this.c.b().a()));
            var1.c();
         }
      });
      kotlin.d.a.b var11 = (kotlin.d.a.b)null.a;
      Object var7 = var11;
      if(var11 != null) {
         var7 = new p(var11);
      }

      io.reactivex.b.b var8 = var12.subscribe(var13, (io.reactivex.c.g)var7);
      kotlin.d.b.l.a(var8, "view.feedbackNegativeCli…            }, Timber::e)");
      this.b = co.uk.getmondo.common.j.f.a(var10, var8);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\b\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\b\u0010\t\u001a\u00020\u0004H&J\b\u0010\n\u001a\u00020\u0004H&J\u0010\u0010\u000b\u001a\u00020\u00042\u0006\u0010\f\u001a\u00020\rH&J\u0010\u0010\u000e\u001a\u00020\u00042\u0006\u0010\u000f\u001a\u00020\u0010H&R\u0018\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0005\u0010\u0006R\u0018\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003X¦\u0004¢\u0006\u0006\u001a\u0004\b\b\u0010\u0006¨\u0006\u0011"},
      d2 = {"Lco/uk/getmondo/help/HelpTopicPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "feedbackNegativeClicked", "Lio/reactivex/Observable;", "", "getFeedbackNegativeClicked", "()Lio/reactivex/Observable;", "feedbackPositiveClicked", "getFeedbackPositiveClicked", "openChat", "openHelpHome", "setTitle", "title", "", "showTopic", "topic", "Lco/uk/getmondo/api/model/help/Topic;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.ui.f {
      io.reactivex.n a();

      void a(Topic var1);

      void a(String var1);

      io.reactivex.n b();

      void c();

      void d();
   }
}
