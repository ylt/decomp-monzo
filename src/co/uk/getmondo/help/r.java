package co.uk.getmondo.help;

import android.support.v7.widget.RecyclerView.w;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.TextView;
import co.uk.getmondo.api.model.help.Topic;
import co.uk.getmondo.common.ae;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0010\b\n\u0000\n\u0002\u0010\t\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u0002\u0018\u00002\f\u0012\b\u0012\u00060\u0002R\u00020\u00000\u0001:\u0001\u001cB-\u0012\u000e\b\u0002\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004\u0012\u0016\b\u0002\u0010\u0006\u001a\u0010\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\b\u0018\u00010\u0007¢\u0006\u0002\u0010\tJ\b\u0010\u000e\u001a\u00020\u000fH\u0016J\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u000fH\u0016J\u0010\u0010\u0013\u001a\u00020\u000f2\u0006\u0010\u0012\u001a\u00020\u000fH\u0016J\u001c\u0010\u0014\u001a\u00020\b2\n\u0010\u0015\u001a\u00060\u0002R\u00020\u00002\u0006\u0010\u0012\u001a\u00020\u000fH\u0016J\u001c\u0010\u0016\u001a\u00060\u0002R\u00020\u00002\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u000fH\u0016J\u0014\u0010\u001a\u001a\u00020\b2\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u001bR(\u0010\u0006\u001a\u0010\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\b\u0018\u00010\u0007X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u000b\"\u0004\b\f\u0010\rR\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u001d"},
   d2 = {"Lco/uk/getmondo/help/TrendingAdapter;", "Landroid/support/v7/widget/RecyclerView$Adapter;", "Lco/uk/getmondo/help/TrendingAdapter$TrendingViewHolder;", "topics", "", "Lco/uk/getmondo/api/model/help/Topic;", "clickListener", "Lkotlin/Function1;", "", "(Ljava/util/List;Lkotlin/jvm/functions/Function1;)V", "getClickListener", "()Lkotlin/jvm/functions/Function1;", "setClickListener", "(Lkotlin/jvm/functions/Function1;)V", "getItemCount", "", "getItemId", "", "position", "getItemViewType", "onBindViewHolder", "holder", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "setTopics", "", "TrendingViewHolder", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class r extends android.support.v7.widget.RecyclerView.a {
   private final List a;
   private kotlin.d.a.b b;

   public r() {
      this((List)null, (kotlin.d.a.b)null, 3, (kotlin.d.b.i)null);
   }

   public r(List var1, kotlin.d.a.b var2) {
      kotlin.d.b.l.b(var1, "topics");
      super();
      this.a = var1;
      this.b = var2;
      this.setHasStableIds(true);
   }

   // $FF: synthetic method
   public r(List var1, kotlin.d.a.b var2, int var3, kotlin.d.b.i var4) {
      if((var3 & 1) != 0) {
         var1 = (List)(new ArrayList());
      }

      if((var3 & 2) != 0) {
         var2 = (kotlin.d.a.b)null;
      }

      this(var1, var2);
   }

   public r.a a(ViewGroup var1, int var2) {
      kotlin.d.b.l.b(var1, "parent");
      return new r.a(co.uk.getmondo.common.ui.m.a(var1, var2, false, 2, (Object)null));
   }

   public final kotlin.d.a.b a() {
      return this.b;
   }

   public void a(r.a var1, int var2) {
      kotlin.d.b.l.b(var1, "holder");
      var1.a((Topic)this.a.get(var2));
   }

   public final void a(List var1) {
      kotlin.d.b.l.b(var1, "topics");
      this.a.clear();
      this.a.addAll((Collection)var1);
      this.notifyDataSetChanged();
   }

   public final void a(kotlin.d.a.b var1) {
      this.b = var1;
   }

   public int getItemCount() {
      return this.a.size();
   }

   public long getItemId(int var1) {
      return (long)((Topic)this.a.get(var1)).a().hashCode();
   }

   public int getItemViewType(int var1) {
      return 2131034372;
   }

   // $FF: synthetic method
   public void onBindViewHolder(w var1, int var2) {
      this.a((r.a)var1, var2);
   }

   // $FF: synthetic method
   public w onCreateViewHolder(ViewGroup var1, int var2) {
      return (w)this.a(var1, var2);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u000e\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u000b"},
      d2 = {"Lco/uk/getmondo/help/TrendingAdapter$TrendingViewHolder;", "Landroid/support/v7/widget/RecyclerView$ViewHolder;", "view", "Landroid/view/View;", "(Lco/uk/getmondo/help/TrendingAdapter;Landroid/view/View;)V", "getView", "()Landroid/view/View;", "bind", "", "topic", "Lco/uk/getmondo/api/model/help/Topic;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public final class a extends w {
      private final View b;

      public a(View var2) {
         kotlin.d.b.l.b(var2, "view");
         super(var2);
         this.b = var2;
         this.b.setOnClickListener((OnClickListener)(new OnClickListener() {
            public final void onClick(View var1) {
               kotlin.d.a.b var2 = r.this.a();
               if(var2 != null) {
                  kotlin.n var3 = (kotlin.n)var2.a(r.this.a.get(a.this.getAdapterPosition()));
               }

            }
         }));
      }

      public final void a(Topic var1) {
         kotlin.d.b.l.b(var1, "topic");
         ((TextView)this.b.findViewById(co.uk.getmondo.c.a.trendingTopicTitle)).setText((CharSequence)var1.b());
         if(kotlin.d.b.l.a(var1, (Topic)kotlin.a.m.g(r.this.a))) {
            ae.b(this.b.findViewById(co.uk.getmondo.c.a.divider));
         }

      }
   }
}
