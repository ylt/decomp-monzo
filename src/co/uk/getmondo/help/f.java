package co.uk.getmondo.help;

import co.uk.getmondo.api.model.help.Topic;
import co.uk.getmondo.api.model.tracking.Impression;
import io.reactivex.u;
import io.reactivex.v;
import java.util.List;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0010B3\b\u0007\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b¢\u0006\u0002\u0010\fJ\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0002H\u0016R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0011"},
   d2 = {"Lco/uk/getmondo/help/HelpPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/help/HelpPresenter$View;", "ioScheduler", "Lio/reactivex/Scheduler;", "uiScheduler", "helpManager", "Lco/uk/getmondo/help/data/HelpManager;", "apiErrorHandler", "Lco/uk/getmondo/common/errors/ApiErrorHandler;", "analyticsService", "Lco/uk/getmondo/common/AnalyticsService;", "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/help/data/HelpManager;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/common/AnalyticsService;)V", "register", "", "view", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class f extends co.uk.getmondo.common.ui.b {
   private final u c;
   private final u d;
   private final co.uk.getmondo.help.a.a e;
   private final co.uk.getmondo.common.e.a f;
   private final co.uk.getmondo.common.a g;

   public f(u var1, u var2, co.uk.getmondo.help.a.a var3, co.uk.getmondo.common.e.a var4, co.uk.getmondo.common.a var5) {
      kotlin.d.b.l.b(var1, "ioScheduler");
      kotlin.d.b.l.b(var2, "uiScheduler");
      kotlin.d.b.l.b(var3, "helpManager");
      kotlin.d.b.l.b(var4, "apiErrorHandler");
      kotlin.d.b.l.b(var5, "analyticsService");
      super();
      this.c = var1;
      this.d = var2;
      this.e = var3;
      this.f = var4;
      this.g = var5;
   }

   public void a(final f.a var1) {
      kotlin.d.b.l.b(var1, "view");
      super.a((co.uk.getmondo.common.ui.f)var1);
      io.reactivex.b.a var4 = this.b;
      io.reactivex.n var5 = var1.e();
      io.reactivex.c.g var6 = (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n var1x) {
            var1.f();
         }
      });
      kotlin.d.a.b var3 = (kotlin.d.a.b)null.a;
      Object var2 = var3;
      if(var3 != null) {
         var2 = new g(var3);
      }

      io.reactivex.b.b var9 = var5.subscribe(var6, (io.reactivex.c.g)var2);
      kotlin.d.b.l.a(var9, "view.searchClicked\n     …penSearch() }, Timber::e)");
      this.b = co.uk.getmondo.common.j.f.a(var4, var9);
      var4 = this.b;
      v var11 = this.e.b().b(this.c).a(this.d).b((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(io.reactivex.b.b var1x) {
            var1.b(false);
            var1.a(true);
         }
      })).a((io.reactivex.c.b)(new io.reactivex.c.b() {
         public final void a(List var1x, Throwable var2) {
            var1.a(false);
         }
      })).d((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Throwable var1x) {
            var1.b(true);
            co.uk.getmondo.common.e.a var2 = f.this.f;
            kotlin.d.b.l.a(var1x, "it");
            var2.a(var1x, (co.uk.getmondo.common.e.a.a)var1);
         }
      }));
      var6 = (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(List var1x) {
            f.a var2 = var1;
            kotlin.d.b.l.a(var1x, "it");
            var2.a(var1x);
         }
      });
      var3 = (kotlin.d.a.b)null.a;
      var2 = var3;
      if(var3 != null) {
         var2 = new g(var3);
      }

      var9 = var11.a(var6, (io.reactivex.c.g)var2);
      kotlin.d.b.l.a(var9, "helpManager.loadTrending…gTopics(it) }, Timber::e)");
      this.b = co.uk.getmondo.common.j.f.a(var4, var9);
      var4 = this.b;
      var5 = var1.a();
      var6 = (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(co.uk.getmondo.help.a.a.c var1x) {
            var1.a(var1x.a());
         }
      });
      var3 = (kotlin.d.a.b)null.a;
      var2 = var3;
      if(var3 != null) {
         var2 = new g(var3);
      }

      var9 = var5.subscribe(var6, (io.reactivex.c.g)var2);
      kotlin.d.b.l.a(var9, "view.categoryClicked\n   …gory(it.id) }, Timber::e)");
      this.b = co.uk.getmondo.common.j.f.a(var4, var9);
      var4 = this.b;
      io.reactivex.n var14 = var1.b();
      io.reactivex.c.g var13 = (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Topic var1x) {
            f.a var2 = var1;
            kotlin.d.b.l.a(var1x, "it");
            var2.a(var1x, f.this.e.a());
         }
      });
      var3 = (kotlin.d.a.b)null.a;
      var2 = var3;
      if(var3 != null) {
         var2 = new g(var3);
      }

      var9 = var14.subscribe(var13, (io.reactivex.c.g)var2);
      kotlin.d.b.l.a(var9, "view.trendingTopicClicke…ndingTitle) }, Timber::e)");
      this.b = co.uk.getmondo.common.j.f.a(var4, var9);
      var4 = this.b;
      var5 = var1.c();
      var6 = (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(co.uk.getmondo.help.a.a.b var1x) {
            var1.d(var1x.e());
         }
      });
      var3 = (kotlin.d.a.b)null.a;
      var2 = var3;
      if(var3 != null) {
         var2 = new g(var3);
      }

      var9 = var5.subscribe(var6, (io.reactivex.c.g)var2);
      kotlin.d.b.l.a(var9, "view.communityForumClick…ity(it.url) }, Timber::e)");
      this.b = co.uk.getmondo.common.j.f.a(var4, var9);
      io.reactivex.b.a var10 = this.b;
      io.reactivex.n var12 = var1.d();
      var13 = (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n var1x) {
            f.this.g.a(Impression.Companion.a(Impression.Companion, Impression.IntercomFrom.HELP, (String)null, (String)null, 6, (Object)null));
            var1.g();
         }
      });
      kotlin.d.a.b var15 = (kotlin.d.a.b)null.a;
      Object var7 = var15;
      if(var15 != null) {
         var7 = new g(var15);
      }

      io.reactivex.b.b var8 = var12.subscribe(var13, (io.reactivex.c.g)var7);
      kotlin.d.b.l.a(var8, "view.helpClicked\n       …            }, Timber::e)");
      this.b = co.uk.getmondo.common.j.f.a(var10, var8);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010 \n\u0000\bf\u0018\u00002\u00020\u00012\u00020\u0002J\u0010\u0010\u0013\u001a\u00020\f2\u0006\u0010\u0014\u001a\u00020\u0015H&J\b\u0010\u0016\u001a\u00020\fH&J\u0010\u0010\u0017\u001a\u00020\f2\u0006\u0010\u0018\u001a\u00020\u0015H&J\b\u0010\u0019\u001a\u00020\fH&J\u0018\u0010\u001a\u001a\u00020\f2\u0006\u0010\u001b\u001a\u00020\u00112\u0006\u0010\u001c\u001a\u00020\u0015H&J\u0010\u0010\u001d\u001a\u00020\f2\u0006\u0010\u001e\u001a\u00020\u001fH&J\u0010\u0010 \u001a\u00020\f2\u0006\u0010\u001e\u001a\u00020\u001fH&J\u0016\u0010!\u001a\u00020\f2\f\u0010\"\u001a\b\u0012\u0004\u0012\u00020\u00110#H&R\u0018\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0006\u0010\u0007R\u0018\u0010\b\u001a\b\u0012\u0004\u0012\u00020\t0\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\n\u0010\u0007R\u0018\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\f0\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\r\u0010\u0007R\u0018\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\f0\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\u000f\u0010\u0007R\u0018\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00110\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0012\u0010\u0007¨\u0006$"},
      d2 = {"Lco/uk/getmondo/help/HelpPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "categoryClicked", "Lio/reactivex/Observable;", "Lco/uk/getmondo/help/data/model/HelpCategory;", "getCategoryClicked", "()Lio/reactivex/Observable;", "communityForumClicked", "Lco/uk/getmondo/help/data/model/CommunityCategory;", "getCommunityForumClicked", "helpClicked", "", "getHelpClicked", "searchClicked", "getSearchClicked", "trendingTopicClicked", "Lco/uk/getmondo/api/model/help/Topic;", "getTrendingTopicClicked", "openCategory", "categoryId", "", "openChat", "openCommunity", "url", "openSearch", "openTopic", "topic", "title", "setTrendingError", "isEnabled", "", "setTrendingLoading", "showTrendingTopics", "topics", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      io.reactivex.n a();

      void a(Topic var1, String var2);

      void a(String var1);

      void a(List var1);

      void a(boolean var1);

      io.reactivex.n b();

      void b(boolean var1);

      io.reactivex.n c();

      io.reactivex.n d();

      void d(String var1);

      io.reactivex.n e();

      void f();

      void g();
   }
}
