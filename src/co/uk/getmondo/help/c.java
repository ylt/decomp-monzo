package co.uk.getmondo.help;

import co.uk.getmondo.api.model.help.Topic;
import co.uk.getmondo.api.model.tracking.Impression;
import io.reactivex.u;
import java.util.List;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0013BG\b\u0007\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\b\b\u0001\u0010\n\u001a\u00020\u000b\u0012\b\b\u0001\u0010\f\u001a\u00020\u000b\u0012\u0006\u0010\r\u001a\u00020\u000e¢\u0006\u0002\u0010\u000fJ\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0002H\u0016R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0014"},
   d2 = {"Lco/uk/getmondo/help/HelpCategoryPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/help/HelpCategoryPresenter$View;", "ioScheduler", "Lio/reactivex/Scheduler;", "uiScheduler", "helpManager", "Lco/uk/getmondo/help/data/HelpManager;", "apiErrorHandler", "Lco/uk/getmondo/common/errors/ApiErrorHandler;", "categoryId", "", "categoryTitle", "analyticsService", "Lco/uk/getmondo/common/AnalyticsService;", "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/help/data/HelpManager;Lco/uk/getmondo/common/errors/ApiErrorHandler;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/common/AnalyticsService;)V", "register", "", "view", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class c extends co.uk.getmondo.common.ui.b {
   private final u c;
   private final u d;
   private final co.uk.getmondo.help.a.a e;
   private final co.uk.getmondo.common.e.a f;
   private final String g;
   private final String h;
   private final co.uk.getmondo.common.a i;

   public c(u var1, u var2, co.uk.getmondo.help.a.a var3, co.uk.getmondo.common.e.a var4, String var5, String var6, co.uk.getmondo.common.a var7) {
      kotlin.d.b.l.b(var1, "ioScheduler");
      kotlin.d.b.l.b(var2, "uiScheduler");
      kotlin.d.b.l.b(var3, "helpManager");
      kotlin.d.b.l.b(var4, "apiErrorHandler");
      kotlin.d.b.l.b(var5, "categoryId");
      kotlin.d.b.l.b(var6, "categoryTitle");
      kotlin.d.b.l.b(var7, "analyticsService");
      super();
      this.c = var1;
      this.d = var2;
      this.e = var3;
      this.f = var4;
      this.g = var5;
      this.h = var6;
      this.i = var7;
   }

   public void a(final c.a var1) {
      kotlin.d.b.l.b(var1, "view");
      super.a((co.uk.getmondo.common.ui.f)var1);
      this.i.a(Impression.Companion.o(this.g));
      co.uk.getmondo.help.a.a.c var2 = co.uk.getmondo.help.a.a.c.h.a(this.g);
      StringBuilder var3 = new StringBuilder();
      String var9;
      if(var2 != null) {
         var9 = co.uk.getmondo.common.k.e.b(var2.c());
      } else {
         var9 = null;
      }

      final String var4 = var3.append(var9).append(" ").append(this.h).toString();
      var1.a(var4);
      io.reactivex.b.a var5 = this.b;
      io.reactivex.n var7 = io.reactivex.n.just(kotlin.n.a).mergeWith((io.reactivex.r)var1.a()).flatMapMaybe((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.h a(kotlin.n var1x) {
            kotlin.d.b.l.b(var1x, "it");
            return co.uk.getmondo.common.j.f.a(c.this.e.b(c.this.g).b(c.this.c).a(c.this.d).b((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(io.reactivex.b.b var1x) {
                  var1.a(true);
               }
            })).a((io.reactivex.c.b)(new io.reactivex.c.b() {
               public final void a(List var1x, Throwable var2) {
                  var1.a(false);
               }
            })).d((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(Throwable var1x) {
                  co.uk.getmondo.common.e.a var2 = c.this.f;
                  kotlin.d.b.l.a(var1x, "it");
                  var2.a(var1x, (co.uk.getmondo.common.e.a.a)var1);
                  var1.d();
               }
            })));
         }
      }));
      io.reactivex.c.g var6 = (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(List var1x) {
            c.a var2 = var1;
            kotlin.d.b.l.a(var1x, "it");
            var2.a(var1x);
         }
      });
      kotlin.d.a.b var11 = (kotlin.d.a.b)null.a;
      Object var12 = var11;
      if(var11 != null) {
         var12 = new d(var11);
      }

      io.reactivex.b.b var14 = var7.subscribe(var6, (io.reactivex.c.g)var12);
      kotlin.d.b.l.a(var14, "Observable.just(Unit) //…Content(it) }, Timber::e)");
      this.b = co.uk.getmondo.common.j.f.a(var5, var14);
      var5 = this.b;
      io.reactivex.n var19 = var1.b();
      io.reactivex.c.g var15 = (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(co.uk.getmondo.help.a.a.d.c var1x) {
            var1.a(var1x.c(), var4);
         }
      });
      var11 = (kotlin.d.a.b)null.a;
      var12 = var11;
      if(var11 != null) {
         var12 = new d(var11);
      }

      var14 = var19.subscribe(var15, (io.reactivex.c.g)var12);
      kotlin.d.b.l.a(var14, "view.topicClicked\n      …pic, title) }, Timber::e)");
      this.b = co.uk.getmondo.common.j.f.a(var5, var14);
      io.reactivex.b.a var13 = this.b;
      io.reactivex.n var16 = var1.c();
      io.reactivex.c.g var17 = (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n var1x) {
            c.this.i.a(Impression.Companion.a(Impression.Companion, Impression.IntercomFrom.HELP, c.this.g, (String)null, 4, (Object)null));
            var1.e();
         }
      });
      kotlin.d.a.b var18 = (kotlin.d.a.b)null.a;
      Object var8 = var18;
      if(var18 != null) {
         var8 = new d(var18);
      }

      io.reactivex.b.b var10 = var16.subscribe(var17, (io.reactivex.c.g)var8);
      kotlin.d.b.l.a(var10, "view.helpClicked\n       …            }, Timber::e)");
      this.b = co.uk.getmondo.common.j.f.a(var13, var10);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\bf\u0018\u00002\u00020\u00012\u00020\u0002J\b\u0010\r\u001a\u00020\u0005H&J\u0018\u0010\u000e\u001a\u00020\u00052\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H&J\u0010\u0010\u0013\u001a\u00020\u00052\u0006\u0010\u0014\u001a\u00020\u0015H&J\u0010\u0010\u0016\u001a\u00020\u00052\u0006\u0010\u0011\u001a\u00020\u0012H&J\u0016\u0010\u0017\u001a\u00020\u00052\f\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u001a0\u0019H&J\b\u0010\u001b\u001a\u00020\u0005H&R\u0018\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0006\u0010\u0007R\u0018\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\t\u0010\u0007R\u0018\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u000b0\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\f\u0010\u0007¨\u0006\u001c"},
      d2 = {"Lco/uk/getmondo/help/HelpCategoryPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "helpClicked", "Lio/reactivex/Observable;", "", "getHelpClicked", "()Lio/reactivex/Observable;", "retryClicked", "getRetryClicked", "topicClicked", "Lco/uk/getmondo/help/data/model/SectionItem$TopicItem;", "getTopicClicked", "openChat", "openTopic", "topic", "Lco/uk/getmondo/api/model/help/Topic;", "title", "", "setLoading", "isEnabled", "", "setTitle", "showContent", "sectionItems", "", "Lco/uk/getmondo/help/data/model/SectionItem;", "showError", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      io.reactivex.n a();

      void a(Topic var1, String var2);

      void a(String var1);

      void a(List var1);

      void a(boolean var1);

      io.reactivex.n b();

      io.reactivex.n c();

      void d();

      void e();
   }
}
