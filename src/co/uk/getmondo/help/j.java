package co.uk.getmondo.help;

import io.reactivex.u;
import java.util.List;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u000eB+\b\u0007\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t¢\u0006\u0002\u0010\nJ\u0010\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u0002H\u0016R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u000f"},
   d2 = {"Lco/uk/getmondo/help/HelpSearchPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/help/HelpSearchPresenter$View;", "ioScheduler", "Lio/reactivex/Scheduler;", "uiScheduler", "helpManager", "Lco/uk/getmondo/help/data/HelpManager;", "apiErrorHandler", "Lco/uk/getmondo/common/errors/ApiErrorHandler;", "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/help/data/HelpManager;Lco/uk/getmondo/common/errors/ApiErrorHandler;)V", "register", "", "view", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class j extends co.uk.getmondo.common.ui.b {
   private final u c;
   private final u d;
   private final co.uk.getmondo.help.a.a e;
   private final co.uk.getmondo.common.e.a f;

   public j(u var1, u var2, co.uk.getmondo.help.a.a var3, co.uk.getmondo.common.e.a var4) {
      kotlin.d.b.l.b(var1, "ioScheduler");
      kotlin.d.b.l.b(var2, "uiScheduler");
      kotlin.d.b.l.b(var3, "helpManager");
      kotlin.d.b.l.b(var4, "apiErrorHandler");
      super();
      this.c = var1;
      this.d = var2;
      this.e = var3;
      this.f = var4;
   }

   public void a(final j.a var1) {
      kotlin.d.b.l.b(var1, "view");
      super.a((co.uk.getmondo.common.ui.f)var1);
      io.reactivex.b.a var4 = this.b;
      io.reactivex.n var5 = var1.a();
      io.reactivex.c.g var6 = (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(String var1x) {
            j.a var2 = var1;
            kotlin.d.b.l.a(var1x, "it");
            var2.a(var1x);
         }
      });
      kotlin.d.a.b var3 = (kotlin.d.a.b)null.a;
      Object var2 = var3;
      if(var3 != null) {
         var2 = new k(var3);
      }

      io.reactivex.b.b var9 = var5.subscribe(var6, (io.reactivex.c.g)var2);
      kotlin.d.b.l.a(var9, "view.suggestionClicked\n …etQuery(it) }, Timber::e)");
      this.b = co.uk.getmondo.common.j.f.a(var4, var9);
      var4 = this.b;
      var5 = var1.b().mergeWith((io.reactivex.r)var1.e()).doOnNext((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(String var1x) {
            var1.h();
            var1.g();
         }
      })).switchMap((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.n a(String var1x) {
            kotlin.d.b.l.b(var1x, "it");
            io.reactivex.n var2;
            if(kotlin.h.j.a((CharSequence)var1x)) {
               var2 = io.reactivex.n.just(kotlin.a.m.a());
            } else {
               var2 = j.this.e.c(kotlin.h.j.b((CharSequence)var1x).toString()).b(j.this.c).a(j.this.d).b((io.reactivex.c.g)(new io.reactivex.c.g() {
                  public final void a(io.reactivex.b.b var1x) {
                     var1.a(true);
                  }
               })).a((io.reactivex.c.b)(new io.reactivex.c.b() {
                  public final void a(List var1x, Throwable var2) {
                     var1.a(false);
                  }
               })).d((io.reactivex.c.g)(new io.reactivex.c.g() {
                  public final void a(Throwable var1x) {
                     co.uk.getmondo.common.e.a var2 = j.this.f;
                     kotlin.d.b.l.a(var1x, "it");
                     var2.a(var1x, (co.uk.getmondo.common.e.a.a)var1);
                     var1.i();
                  }
               })).f().onErrorResumeNext((io.reactivex.r)io.reactivex.n.empty());
            }

            return var2;
         }
      }));
      var6 = (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(List var1x) {
            if(var1x.isEmpty()) {
               var1.f();
            } else {
               j.a var2 = var1;
               kotlin.d.b.l.a(var1x, "it");
               var2.a(var1x);
            }

         }
      });
      var3 = (kotlin.d.a.b)null.a;
      var2 = var3;
      if(var3 != null) {
         var2 = new k(var3);
      }

      var9 = var5.subscribe(var6, (io.reactivex.c.g)var2);
      kotlin.d.b.l.a(var9, "view.queryChanged\n      …wTopics(it) }, Timber::e)");
      this.b = co.uk.getmondo.common.j.f.a(var4, var9);
      var4 = this.b;
      io.reactivex.n var14 = var1.c();
      io.reactivex.c.g var12 = (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(co.uk.getmondo.help.a.a.e var1x) {
            j.a var2 = var1;
            kotlin.d.b.l.a(var1x, "it");
            var2.a(var1x);
         }
      });
      var3 = (kotlin.d.a.b)null.a;
      var2 = var3;
      if(var3 != null) {
         var2 = new k(var3);
      }

      var9 = var14.subscribe(var12, (io.reactivex.c.g)var2);
      kotlin.d.b.l.a(var9, "view.topicClicked\n      …enTopic(it) }, Timber::e)");
      this.b = co.uk.getmondo.common.j.f.a(var4, var9);
      io.reactivex.b.a var10 = this.b;
      io.reactivex.n var11 = var1.d();
      var12 = (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(String var1x) {
            j.a var2 = var1;
            kotlin.d.b.l.a(var1x, "it");
            var2.d(var1x);
         }
      });
      kotlin.d.a.b var13 = (kotlin.d.a.b)null.a;
      Object var7 = var13;
      if(var13 != null) {
         var7 = new k(var13);
      }

      io.reactivex.b.b var8 = var11.subscribe(var12, (io.reactivex.c.g)var7);
      kotlin.d.b.l.a(var8, "view.helpClicked\n       …penChat(it) }, Timber::e)");
      this.b = co.uk.getmondo.common.j.f.a(var10, var8);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u00012\u00020\u0002J\b\u0010\u0011\u001a\u00020\u0012H&J\b\u0010\u0013\u001a\u00020\u0012H&J\u0010\u0010\u0014\u001a\u00020\u00122\u0006\u0010\u0015\u001a\u00020\u0005H&J\u0010\u0010\u0016\u001a\u00020\u00122\u0006\u0010\u0017\u001a\u00020\u000fH&J\u0010\u0010\u0018\u001a\u00020\u00122\u0006\u0010\u0019\u001a\u00020\u001aH&J\u0010\u0010\u001b\u001a\u00020\u00122\u0006\u0010\u0015\u001a\u00020\u0005H&J\b\u0010\u001c\u001a\u00020\u0012H&J\b\u0010\u001d\u001a\u00020\u0012H&J\u0016\u0010\u001e\u001a\u00020\u00122\f\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020!0 H&R\u0018\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0006\u0010\u0007R\u0018\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\t\u0010\u0007R\u0018\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\u0007R\u0018\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\r\u0010\u0007R\u0018\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u000f0\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0010\u0010\u0007¨\u0006\""},
      d2 = {"Lco/uk/getmondo/help/HelpSearchPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "helpClicked", "Lio/reactivex/Observable;", "", "getHelpClicked", "()Lio/reactivex/Observable;", "queryChanged", "getQueryChanged", "retryClicked", "getRetryClicked", "suggestionClicked", "getSuggestionClicked", "topicClicked", "Lco/uk/getmondo/help/data/model/TopicViewModel;", "getTopicClicked", "hideSuggestions", "", "hideTopics", "openChat", "query", "openTopic", "topicViewModel", "setLoading", "enabled", "", "setQuery", "showError", "showSuggestions", "showTopics", "topics", "", "Lco/uk/getmondo/help/data/model/SectionItem;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      io.reactivex.n a();

      void a(co.uk.getmondo.help.a.a.e var1);

      void a(String var1);

      void a(List var1);

      void a(boolean var1);

      io.reactivex.n b();

      io.reactivex.n c();

      io.reactivex.n d();

      void d(String var1);

      io.reactivex.n e();

      void f();

      void g();

      void h();

      void i();
   }
}
