package co.uk.getmondo.c;

import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.common.o;
import co.uk.getmondo.common.ui.f;
import io.reactivex.r;
import io.reactivex.u;
import io.reactivex.c.g;
import io.reactivex.c.h;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0016BK\b\u0007\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\f\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011¢\u0006\u0002\u0010\u0012J\u0010\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0002H\u0016R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0017"},
   d2 = {"Lco/uk/getmondo/contacts/ContactsPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/contacts/ContactsPresenter$View;", "ioScheduler", "Lio/reactivex/Scheduler;", "uiScheduler", "analyticsService", "Lco/uk/getmondo/common/AnalyticsService;", "accountManager", "Lco/uk/getmondo/common/accounts/AccountManager;", "balanceRepository", "Lco/uk/getmondo/account_balance/BalanceRepository;", "apiErrorHandler", "Lco/uk/getmondo/common/errors/ApiErrorHandler;", "cardManager", "Lco/uk/getmondo/card/CardManager;", "featureFlagsStorage", "Lco/uk/getmondo/common/FeatureFlagsStorage;", "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/common/AnalyticsService;Lco/uk/getmondo/common/accounts/AccountManager;Lco/uk/getmondo/account_balance/BalanceRepository;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/card/CardManager;Lco/uk/getmondo/common/FeatureFlagsStorage;)V", "register", "", "view", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class c extends co.uk.getmondo.common.ui.b {
   private final u c;
   private final u d;
   private final co.uk.getmondo.common.a e;
   private final co.uk.getmondo.common.accounts.b f;
   private final co.uk.getmondo.a.a g;
   private final co.uk.getmondo.common.e.a h;
   private final co.uk.getmondo.card.c i;
   private final o j;

   public c(u var1, u var2, co.uk.getmondo.common.a var3, co.uk.getmondo.common.accounts.b var4, co.uk.getmondo.a.a var5, co.uk.getmondo.common.e.a var6, co.uk.getmondo.card.c var7, o var8) {
      l.b(var1, "ioScheduler");
      l.b(var2, "uiScheduler");
      l.b(var3, "analyticsService");
      l.b(var4, "accountManager");
      l.b(var5, "balanceRepository");
      l.b(var6, "apiErrorHandler");
      l.b(var7, "cardManager");
      l.b(var8, "featureFlagsStorage");
      super();
      this.c = var1;
      this.d = var2;
      this.e = var3;
      this.f = var4;
      this.g = var5;
      this.h = var6;
      this.i = var7;
      this.j = var8;
   }

   public void a(final c.a var1) {
      l.b(var1, "view");
      super.a((f)var1);
      this.e.a(Impression.Companion.aC());
      io.reactivex.b.a var2;
      if(this.f.b()) {
         var1.b();
         var2 = this.b;
         io.reactivex.b.b var3 = this.j.a().map((h)null.a).distinctUntilChanged().subscribe((g)(new g() {
            public final void a(Boolean var1x) {
               c.a var2 = var1;
               l.a(var1x, "it");
               var2.a(var1x.booleanValue());
            }
         }));
         l.a(var3, "featureFlagsStorage.feat…tContactsFabVisible(it) }");
         this.b = co.uk.getmondo.common.j.f.a(var2, var3);
      } else {
         var1.a();
         var1.a(true);
      }

      io.reactivex.b.a var6 = this.b;
      io.reactivex.b.b var5 = this.g.b(this.f.c()).b(this.c).a(this.d).a((io.reactivex.c.a)null.a, (g)(new g() {
         public final void a(Throwable var1x) {
            co.uk.getmondo.common.e.a var2 = c.this.h;
            l.a(var1x, "error");
            var2.a(var1x, (co.uk.getmondo.common.e.a.a)var1);
         }
      }));
      l.a(var5, "balanceRepository.refres…ndleError(error, view) })");
      this.b = co.uk.getmondo.common.j.f.a(var6, var5);
      var2 = this.b;
      io.reactivex.b.b var4 = co.uk.getmondo.common.j.f.a(this.g.a(this.f.c()).observeOn(this.d).map((h)null.a), (r)this.i.a()).subscribe((g)(new g() {
         public final void a(kotlin.h var1x) {
            co.uk.getmondo.d.c var2 = (co.uk.getmondo.d.c)var1x.c();
            if(((co.uk.getmondo.d.g)var1x.d()).h()) {
               c.a var3 = var1;
               l.a(var2, "balance");
               var3.a(var2);
            } else {
               var1.c();
            }

         }
      }), (g)null.a);
      l.a(var4, "balanceRepository.balanc…     }, { Timber.e(it) })");
      this.b = co.uk.getmondo.common.j.f.a(var2, var4);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\bf\u0018\u00002\u00020\u00012\u00020\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H&J\u0010\u0010\u0007\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\tH&J\b\u0010\n\u001a\u00020\u0004H&J\b\u0010\u000b\u001a\u00020\u0004H&J\b\u0010\f\u001a\u00020\u0004H&¨\u0006\r"},
      d2 = {"Lco/uk/getmondo/contacts/ContactsPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "setContactsFabVisible", "", "visible", "", "showBalance", "balance", "Lco/uk/getmondo/model/Amount;", "showCardFrozen", "showPrepaidUi", "showRetailUi", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, f {
      void a();

      void a(co.uk.getmondo.d.c var1);

      void a(boolean var1);

      void b();

      void c();
   }
}
