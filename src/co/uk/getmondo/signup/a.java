package co.uk.getmondo.signup;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import co.uk.getmondo.common.q;
import java.io.Serializable;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.d.b.w;
import kotlin.d.b.y;
import kotlin.reflect.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\b&\u0018\u0000 \u00192\u00020\u00012\u00020\u0002:\u0001\u0019B\u0005¢\u0006\u0002\u0010\u0003J\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0016J\u0010\u0010\u0014\u001a\u00020\u00112\u0006\u0010\u0015\u001a\u00020\u0016H\u0016J\b\u0010\u0017\u001a\u00020\u0018H\u0016R\u001e\u0010\u0004\u001a\u00020\u00058\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\tR\u001b\u0010\n\u001a\u00020\u000b8DX\u0084\u0084\u0002¢\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\f\u0010\r¨\u0006\u001a"},
   d2 = {"Lco/uk/getmondo/signup/BaseSignupActivity;", "Lco/uk/getmondo/common/activities/BaseActivity;", "Lco/uk/getmondo/signup/SignupApiErrorHandler$View;", "()V", "intercomService", "Lco/uk/getmondo/common/IntercomService;", "getIntercomService", "()Lco/uk/getmondo/common/IntercomService;", "setIntercomService", "(Lco/uk/getmondo/common/IntercomService;)V", "signupEntryPoint", "Lco/uk/getmondo/signup/SignupEntryPoint;", "getSignupEntryPoint", "()Lco/uk/getmondo/signup/SignupEntryPoint;", "signupEntryPoint$delegate", "Lkotlin/Lazy;", "onCreateOptionsMenu", "", "menu", "Landroid/view/Menu;", "onOptionsItemSelected", "item", "Landroid/view/MenuItem;", "reloadSignupStatus", "", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public abstract class a extends co.uk.getmondo.common.activities.b implements i.a {
   // $FF: synthetic field
   static final l[] c = new l[]{(l)y.a(new w(y.a(a.class), "signupEntryPoint", "getSignupEntryPoint()Lco/uk/getmondo/signup/SignupEntryPoint;"))};
   public static final a.a f = new a.a((kotlin.d.b.i)null);
   private final kotlin.c a = kotlin.d.a((kotlin.d.a.a)(new kotlin.d.a.a() {
      public final j b() {
         Serializable var1 = a.this.getIntent().getSerializableExtra("KEY_SIGNUP_ENTRY_POINT");
         if(var1 == null) {
            throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.signup.SignupEntryPoint");
         } else {
            return (j)var1;
         }
      }

      // $FF: synthetic method
      public Object v_() {
         return this.b();
      }
   }));
   private HashMap b;
   public q e;

   public View a(int var1) {
      if(this.b == null) {
         this.b = new HashMap();
      }

      View var3 = (View)this.b.get(Integer.valueOf(var1));
      View var2 = var3;
      if(var3 == null) {
         var2 = this.findViewById(var1);
         this.b.put(Integer.valueOf(var1), var2);
      }

      return var2;
   }

   protected final j a() {
      kotlin.c var2 = this.a;
      l var1 = c[0];
      return (j)var2.a();
   }

   public void b() {
      this.setResult(-1);
      this.finish();
   }

   public boolean onCreateOptionsMenu(Menu var1) {
      kotlin.d.b.l.b(var1, "menu");
      this.getMenuInflater().inflate(2131951622, var1);
      MenuItem var2 = var1.findItem(2131821781);
      MenuItem var4 = var1.findItem(2131821780);
      j var3 = this.a();
      switch(b.a[var3.ordinal()]) {
      case 1:
         var2.setVisible(true);
         var4.setVisible(false);
         break;
      case 2:
         var2.setVisible(false);
         var4.setVisible(true);
      }

      return true;
   }

   public boolean onOptionsItemSelected(MenuItem var1) {
      boolean var2 = true;
      kotlin.d.b.l.b(var1, "item");
      if(var1.getItemId() == 2131821781) {
         q var3 = this.e;
         if(var3 == null) {
            kotlin.d.b.l.b("intercomService");
         }

         var3.a();
      } else if(var1.getItemId() == 2131821780) {
         (new co.uk.getmondo.migration.g()).show(this.getFragmentManager(), "TAG_SHOW_DIALOG");
      } else {
         var2 = super.onOptionsItemSelected(var1);
      }

      return var2;
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000¨\u0006\u0006"},
      d2 = {"Lco/uk/getmondo/signup/BaseSignupActivity$Companion;", "", "()V", "KEY_SIGNUP_ENTRY_POINT", "", "TAG_SHOW_DIALOG", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var1) {
         this();
      }
   }
}
