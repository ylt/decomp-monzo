package co.uk.getmondo.signup.identity_verification.video;

import android.content.Context;
import co.uk.getmondo.d.ak;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0019\b\u0007\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0006\u0010\u0007\u001a\u00020\bJ\u0006\u0010\t\u001a\u00020\bJ\u0006\u0010\n\u001a\u00020\bR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b"},
   d2 = {"Lco/uk/getmondo/signup/identity_verification/video/VideoStringProvider;", "", "context", "Landroid/content/Context;", "accountService", "Lco/uk/getmondo/common/accounts/AccountService;", "(Landroid/content/Context;Lco/uk/getmondo/common/accounts/AccountService;)V", "selfieConfirmation", "", "selfieInstructions", "selfieTextToRead", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ab {
   private final Context a;
   private final co.uk.getmondo.common.accounts.d b;

   public ab(Context var1, co.uk.getmondo.common.accounts.d var2) {
      kotlin.d.b.l.b(var1, "context");
      kotlin.d.b.l.b(var2, "accountService");
      super();
      this.a = var1;
      this.b = var2;
   }

   public final String a() {
      String var5;
      label18: {
         ak var2 = this.b.b();
         if(var2 != null) {
            co.uk.getmondo.d.ac var4 = var2.d();
            if(var4 != null) {
               var5 = var4.c();
               if(var5 != null) {
                  break label18;
               }
            }
         }

         var5 = "";
      }

      var5 = co.uk.getmondo.common.k.p.b(var5);
      Boolean var3 = co.uk.getmondo.a.c;
      kotlin.d.b.l.a(var3, "BuildConfig.BANK");
      int var1;
      if(var3.booleanValue()) {
         var1 = 2131362326;
      } else {
         var1 = 2131362327;
      }

      var5 = this.a.getString(var1, new Object[]{var5});
      kotlin.d.b.l.a(var5, "context.getString(textResource, name)");
      return var5;
   }

   public final String b() {
      String var1 = this.a.getString(2131362337);
      kotlin.d.b.l.a(var1, "context.getString(R.stri…ra_fallback_instructions)");
      return var1;
   }

   public final String c() {
      String var1 = this.a.getString(2131362336);
      kotlin.d.b.l.a(var1, "context.getString(R.stri…ra_fallback_confirmation)");
      return var1;
   }
}
