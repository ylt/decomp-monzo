package co.uk.getmondo.signup.identity_verification.video;

// $FF: synthetic class
final class k implements io.reactivex.c.g {
   private final h a;
   private final h.a b;

   private k(h var1, h.a var2) {
      this.a = var1;
      this.b = var2;
   }

   public static io.reactivex.c.g a(h var0, h.a var1) {
      return new k(var0, var1);
   }

   public void a(Object var1) {
      h.a(this.a, this.b, var1);
   }
}
