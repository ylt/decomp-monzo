package co.uk.getmondo.signup.identity_verification.video;

import co.uk.getmondo.api.model.tracking.Impression;

class h extends co.uk.getmondo.common.ui.b {
   private final co.uk.getmondo.common.a c;
   private final String d;

   h(co.uk.getmondo.common.a var1, String var2) {
      this.c = var1;
      this.d = var2;
   }

   // $FF: synthetic method
   static void a(h.a var0, Boolean var1) throws Exception {
      if(var1.booleanValue()) {
         var0.g();
      } else {
         var0.f();
      }

   }

   // $FF: synthetic method
   static void a(h.a var0, Object var1) throws Exception {
      var0.d();
   }

   // $FF: synthetic method
   static void a(h var0, h.a var1, Object var2) throws Exception {
      var0.c.a(Impression.D());
      var1.e();
   }

   public void a(h.a var1) {
      super.a((co.uk.getmondo.common.ui.f)var1);
      var1.a(this.d);
      this.a((io.reactivex.b.b)var1.a().subscribe(i.a(var1)));
      this.a((io.reactivex.b.b)var1.c().subscribe(j.a(var1)));
      this.a((io.reactivex.b.b)var1.b().subscribe(k.a(this, var1)));
   }

   interface a extends co.uk.getmondo.common.ui.f {
      io.reactivex.n a();

      void a(String var1);

      io.reactivex.n b();

      io.reactivex.n c();

      void d();

      void e();

      void f();

      void g();
   }
}
