package co.uk.getmondo.signup.identity_verification.video;

import android.content.Context;
import android.util.AttributeSet;

public class MuteButton extends android.support.v7.widget.o {
   private static final int[] a = new int[]{2130772584};
   private boolean b = true;

   public MuteButton(Context var1) {
      super(var1);
   }

   public MuteButton(Context var1, AttributeSet var2) {
      super(var1, var2);
   }

   public MuteButton(Context var1, AttributeSet var2, int var3) {
      super(var1, var2, var3);
   }

   public boolean a() {
      return this.b;
   }

   public void b() {
      boolean var1;
      if(!this.b) {
         var1 = true;
      } else {
         var1 = false;
      }

      this.b = var1;
   }

   public int[] onCreateDrawableState(int var1) {
      int[] var2 = super.onCreateDrawableState(var1 + 1);
      if(this.b) {
         mergeDrawableStates(var2, a);
      }

      return var2;
   }
}
