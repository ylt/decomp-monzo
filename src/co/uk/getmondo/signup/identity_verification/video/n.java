package co.uk.getmondo.signup.identity_verification.video;

import co.uk.getmondo.api.model.tracking.Impression;
import java.util.concurrent.TimeUnit;

public class n extends co.uk.getmondo.common.ui.b {
   private final io.reactivex.u c;
   private final io.reactivex.u d;
   private final co.uk.getmondo.signup.identity_verification.a.e e;
   private final co.uk.getmondo.common.a f;
   private final co.uk.getmondo.common.q g;
   private final a h;
   private final String i;
   private final ab j;

   n(io.reactivex.u var1, io.reactivex.u var2, co.uk.getmondo.signup.identity_verification.a.e var3, co.uk.getmondo.common.a var4, co.uk.getmondo.common.q var5, a var6, co.uk.getmondo.signup.identity_verification.a.a var7, ab var8) {
      this.c = var1;
      this.d = var2;
      this.e = var3;
      this.f = var4;
      this.g = var5;
      this.h = var6;
      this.i = var7.a().getAbsolutePath();
      this.j = var8;
   }

   // $FF: synthetic method
   static io.reactivex.z a(n var0, Object var1) throws Exception {
      return var0.h.a(var0.i).b(var0.d).f(r.a(var0));
   }

   // $FF: synthetic method
   static io.reactivex.z a(n var0, Throwable var1) throws Exception {
      boolean var2 = var1 instanceof OutOfMemoryError;
      var0.f.a(Impression.b(var1.getMessage(), var2));
      return io.reactivex.v.a((Object)var0.i);
   }

   // $FF: synthetic method
   static void a(n.a var0, io.reactivex.m var1) throws Exception {
      var0.C();
   }

   // $FF: synthetic method
   static void a(n.a var0, Object var1) throws Exception {
      var0.finish();
   }

   // $FF: synthetic method
   static void a(n.a var0, Throwable var1) throws Exception {
      d.a.a.a(var1);
      var0.G();
   }

   // $FF: synthetic method
   static void a(n var0, n.a var1, Boolean var2) throws Exception {
      if(var2.booleanValue()) {
         var0.e.a(var0.i, false);
         var1.finish();
      } else {
         var0.b(var1);
      }

   }

   // $FF: synthetic method
   static void a(n var0, n.a var1, Object var2) throws Exception {
      var1.e(var0.j.a());
      var1.w();
      var1.y();
      var1.k();
      var1.c();
      var0.f.a(Impression.C());
   }

   // $FF: synthetic method
   static void a(n var0, n.a var1, String var2) throws Exception {
      var1.d(var0.i);
      var1.v();
   }

   private void b(n.a var1) {
      var1.F();
      var1.z();
      var1.w();
   }

   // $FF: synthetic method
   static void b(n.a var0, Object var1) throws Exception {
      var0.d();
      var0.j();
      var0.b();
      var0.D();
      var0.B();
   }

   // $FF: synthetic method
   static void b(n.a var0, Throwable var1) throws Exception {
      d.a.a.a(var1);
      var0.G();
   }

   // $FF: synthetic method
   static void b(n var0, n.a var1, Object var2) throws Exception {
      var1.A();
      var1.j();
      var1.E();
      var1.x();
      var1.a(var0.i);
   }

   void a() {
      this.g.a();
   }

   public void a(n.a var1) {
      super.a((co.uk.getmondo.common.ui.f)var1);
      this.f.a(Impression.f(false));
      this.a((io.reactivex.b.b)var1.e().doOnNext(o.a(this, var1)).delay(1L, TimeUnit.SECONDS, this.d).observeOn(this.c).subscribe(s.a(this, var1), t.a(var1)));
      this.a((io.reactivex.b.b)io.reactivex.n.merge(var1.f(), var1.i()).observeOn(this.c).doOnNext(u.a(var1)).delay(2L, TimeUnit.SECONDS, this.d).flatMapSingle(v.a(this)).observeOn(this.c).doOnEach(w.a(var1)).subscribe(x.a(this, var1), y.a(var1)));
      this.a((io.reactivex.b.b)var1.g().subscribe(z.a(var1)));
      this.a((io.reactivex.b.b)var1.h().subscribe(p.a(this, var1), q.a()));
   }

   interface a extends co.uk.getmondo.common.ui.f {
      void A();

      void B();

      void C();

      void D();

      void E();

      void F();

      void G();

      void a(String var1);

      void b();

      void c();

      void d();

      void d(String var1);

      io.reactivex.n e();

      void e(String var1);

      io.reactivex.n f();

      void finish();

      io.reactivex.n g();

      io.reactivex.n h();

      io.reactivex.n i();

      void j();

      void k();

      void v();

      void w();

      void x();

      void y();

      void z();
   }
}
