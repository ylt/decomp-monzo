package co.uk.getmondo.signup.identity_verification.video;

public final class g implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final f b;

   static {
      boolean var0;
      if(!g.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   public g(f var1) {
      if(!a && var1 == null) {
         throw new AssertionError();
      } else {
         this.b = var1;
      }
   }

   public static b.a.b a(f var0) {
      return new g(var0);
   }

   public String a() {
      return (String)b.a.d.a(this.b.a(), "Cannot return null from a non-@Nullable @Provides method");
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
