package co.uk.getmondo.signup.identity_verification.video;

import java.util.concurrent.Callable;

// $FF: synthetic class
final class b implements Callable {
   private final a a;
   private final String b;

   private b(a var1, String var2) {
      this.a = var1;
      this.b = var2;
   }

   public static Callable a(a var0, String var1) {
      return new b(var0, var1);
   }

   public Object call() {
      return a.a(this.a, this.b);
   }
}
