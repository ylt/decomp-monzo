package co.uk.getmondo.signup.identity_verification;

import co.uk.getmondo.api.model.identity_verification.IdentityVerification;
import kotlin.Metadata;

// $FF: synthetic class
@Metadata(
   bv = {1, 0, 2},
   k = 3,
   mv = {1, 1, 7}
)
public final class w {
   // $FF: synthetic field
   public static final int[] a = new int[IdentityVerification.Status.values().length];

   static {
      a[IdentityVerification.Status.NOT_STARTED.ordinal()] = 1;
      a[IdentityVerification.Status.PENDING_SUBMISSION.ordinal()] = 2;
      a[IdentityVerification.Status.BLOCKED.ordinal()] = 3;
   }
}
