package co.uk.getmondo.signup.identity_verification;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import co.uk.getmondo.api.model.signup.SignupSource;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.create_account.topup.TopupInfoActivity;
import co.uk.getmondo.main.HomeActivity;
import co.uk.getmondo.signup.identity_verification.chat_with_us.ChatWithUsActivity;
import java.io.Serializable;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.TypeCastException;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0006\u0018\u0000 -2\u00020\u00012\u00020\u0002:\u0001-B\u0005¢\u0006\u0002\u0010\u0003J\b\u0010\u001c\u001a\u00020\u0011H\u0016J\u0012\u0010\u001d\u001a\u00020\u00112\b\u0010\u001e\u001a\u0004\u0018\u00010\u001fH\u0014J\b\u0010 \u001a\u00020\u0011H\u0014J\u000e\u0010!\u001a\b\u0012\u0004\u0012\u00020\u00110\"H\u0016J\b\u0010#\u001a\u00020\u0011H\u0014J\b\u0010$\u001a\u00020\u0011H\u0016J\b\u0010%\u001a\u00020\u0011H\u0016J\u0012\u0010&\u001a\u00020\u00112\b\u0010'\u001a\u0004\u0018\u00010(H\u0016J\b\u0010)\u001a\u00020\u0011H\u0016J\b\u0010*\u001a\u00020\u0011H\u0016J\b\u0010+\u001a\u00020\u0011H\u0016J\b\u0010,\u001a\u00020\u0011H\u0016R\u001b\u0010\u0004\u001a\u00020\u00058BX\u0082\u0084\u0002¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\u0006\u0010\u0007R\u001b\u0010\n\u001a\u00020\u000b8BX\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u000e\u0010\t\u001a\u0004\b\f\u0010\rR\u0014\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00110\u0010X\u0082\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0012\u001a\u00020\u00138BX\u0082\u0004¢\u0006\u0006\u001a\u0004\b\u0014\u0010\u0015R\u001e\u0010\u0016\u001a\u00020\u00178\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\u0018\u0010\u0019\"\u0004\b\u001a\u0010\u001b¨\u0006."},
   d2 = {"Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity;", "Lco/uk/getmondo/common/activities/BaseActivity;", "Lco/uk/getmondo/signup/identity_verification/VerificationPendingPresenter$View;", "()V", "from", "Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;", "getFrom", "()Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;", "from$delegate", "Lkotlin/Lazy;", "identityVerificationVersion", "Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationVersion;", "getIdentityVerificationVersion", "()Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationVersion;", "identityVerificationVersion$delegate", "resumeRelay", "Lcom/jakewharton/rxrelay2/PublishRelay;", "", "signUpSource", "Lco/uk/getmondo/api/model/signup/SignupSource;", "getSignUpSource", "()Lco/uk/getmondo/api/model/signup/SignupSource;", "verificationPendingPresenter", "Lco/uk/getmondo/signup/identity_verification/VerificationPendingPresenter;", "getVerificationPendingPresenter", "()Lco/uk/getmondo/signup/identity_verification/VerificationPendingPresenter;", "setVerificationPendingPresenter", "(Lco/uk/getmondo/signup/identity_verification/VerificationPendingPresenter;)V", "hideLoading", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onDestroy", "onRefresh", "Lio/reactivex/Observable;", "onResume", "openChat", "openHome", "openIdentityVerification", "rejectionNote", "", "openIdentityVerificationOnboarding", "openTopupInfo", "showLoading", "showVerifyingMessage", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class VerificationPendingActivity extends co.uk.getmondo.common.activities.b implements aa.a {
   // $FF: synthetic field
   static final kotlin.reflect.l[] a = new kotlin.reflect.l[]{(kotlin.reflect.l)kotlin.d.b.y.a(new kotlin.d.b.w(kotlin.d.b.y.a(VerificationPendingActivity.class), "identityVerificationVersion", "getIdentityVerificationVersion()Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationVersion;")), (kotlin.reflect.l)kotlin.d.b.y.a(new kotlin.d.b.w(kotlin.d.b.y.a(VerificationPendingActivity.class), "from", "getFrom()Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;"))};
   public static final VerificationPendingActivity.a c = new VerificationPendingActivity.a((kotlin.d.b.i)null);
   public aa b;
   private final com.b.b.c e;
   private final kotlin.c f;
   private final kotlin.c g;
   private HashMap h;

   public VerificationPendingActivity() {
      com.b.b.c var1 = com.b.b.c.a();
      kotlin.d.b.l.a(var1, "PublishRelay.create()");
      this.e = var1;
      this.f = kotlin.d.a((kotlin.d.a.a)(new kotlin.d.a.a() {
         public final co.uk.getmondo.signup.identity_verification.a.j b() {
            Serializable var1 = VerificationPendingActivity.this.getIntent().getSerializableExtra("KEY_VERSION");
            if(var1 == null) {
               throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.signup.identity_verification.data.IdentityVerificationVersion");
            } else {
               return (co.uk.getmondo.signup.identity_verification.a.j)var1;
            }
         }

         // $FF: synthetic method
         public Object v_() {
            return this.b();
         }
      }));
      this.g = kotlin.d.a((kotlin.d.a.a)(new kotlin.d.a.a() {
         public final Impression.KycFrom b() {
            Serializable var1 = VerificationPendingActivity.this.getIntent().getSerializableExtra("KEY_ENTRY_POINT");
            if(var1 == null) {
               throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.api.model.tracking.Impression.KycFrom");
            } else {
               return (Impression.KycFrom)var1;
            }
         }

         // $FF: synthetic method
         public Object v_() {
            return this.b();
         }
      }));
   }

   public static final Intent a(Context var0, co.uk.getmondo.signup.identity_verification.a.j var1, Impression.KycFrom var2) {
      kotlin.d.b.l.b(var0, "context");
      kotlin.d.b.l.b(var1, "identityVerificationVersion");
      kotlin.d.b.l.b(var2, "from");
      return c.a(var0, var1, var2);
   }

   private final co.uk.getmondo.signup.identity_verification.a.j i() {
      kotlin.c var1 = this.f;
      kotlin.reflect.l var2 = a[0];
      return (co.uk.getmondo.signup.identity_verification.a.j)var1.a();
   }

   private final Impression.KycFrom j() {
      kotlin.c var1 = this.g;
      kotlin.reflect.l var2 = a[1];
      return (Impression.KycFrom)var1.a();
   }

   private final SignupSource k() {
      SignupSource var1;
      if(this.j().a()) {
         var1 = SignupSource.SDD_MIGRATION;
      } else {
         var1 = SignupSource.LEGACY_PREPAID;
      }

      return var1;
   }

   public View a(int var1) {
      if(this.h == null) {
         this.h = new HashMap();
      }

      View var3 = (View)this.h.get(Integer.valueOf(var1));
      View var2 = var3;
      if(var3 == null) {
         var2 = this.findViewById(var1);
         this.h.put(Integer.valueOf(var1), var2);
      }

      return var2;
   }

   public io.reactivex.n a() {
      return (io.reactivex.n)this.e;
   }

   public void a(String var1) {
      this.startActivity(IdentityVerificationActivity.g.a((Context)this, this.i(), this.j(), this.k(), co.uk.getmondo.signup.j.b, var1));
      this.finish();
   }

   public void b() {
      ((ProgressBar)this.a(co.uk.getmondo.c.a.verificationPendingProgress)).setVisibility(0);
   }

   public void c() {
      ((ProgressBar)this.a(co.uk.getmondo.c.a.verificationPendingProgress)).setVisibility(8);
   }

   public void d() {
      ((ImageView)this.a(co.uk.getmondo.c.a.verificationPendingImage)).setVisibility(0);
      ((TextView)this.a(co.uk.getmondo.c.a.verificationPendingTitle)).setVisibility(0);
      ((TextView)this.a(co.uk.getmondo.c.a.verificationPendingSubtitle)).setVisibility(0);
   }

   public void e() {
      this.startActivity(new Intent((Context)this, TopupInfoActivity.class));
      this.finish();
   }

   public void f() {
      this.startActivity(IdentityVerificationActivity.a.a(IdentityVerificationActivity.g, (Context)this, this.i(), this.j(), this.k(), co.uk.getmondo.signup.j.b, (String)null, 32, (Object)null));
      this.finish();
   }

   public void g() {
      this.startActivity(ChatWithUsActivity.b.a((Context)this));
      this.finish();
   }

   public void h() {
      this.startActivity(new Intent((Context)this, HomeActivity.class));
      this.finish();
   }

   protected void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.setContentView(2131034226);
      this.l().a(new p(this.i(), this.k())).a(this);
      aa var2 = this.b;
      if(var2 == null) {
         kotlin.d.b.l.b("verificationPendingPresenter");
      }

      var2.a((aa.a)this);
   }

   protected void onDestroy() {
      aa var1 = this.b;
      if(var1 == null) {
         kotlin.d.b.l.b("verificationPendingPresenter");
      }

      var1.b();
      super.onDestroy();
   }

   protected void onResume() {
      super.onResume();
      this.e.a((Object)kotlin.n.a);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J \u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\rH\u0007R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000¨\u0006\u000e"},
      d2 = {"Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity$Companion;", "", "()V", "KEY_ENTRY_POINT", "", "KEY_VERSION", "buildIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "identityVerificationVersion", "Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationVersion;", "from", "Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var1) {
         this();
      }

      public final Intent a(Context var1, co.uk.getmondo.signup.identity_verification.a.j var2, Impression.KycFrom var3) {
         kotlin.d.b.l.b(var1, "context");
         kotlin.d.b.l.b(var2, "identityVerificationVersion");
         kotlin.d.b.l.b(var3, "from");
         Intent var4 = (new Intent(var1, VerificationPendingActivity.class)).putExtra("KEY_VERSION", (Serializable)var2).putExtra("KEY_ENTRY_POINT", (Serializable)var3);
         kotlin.d.b.l.a(var4, "Intent(context, Verifica…ra(KEY_ENTRY_POINT, from)");
         return var4;
      }
   }
}
