package co.uk.getmondo.signup.identity_verification;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.ProgressBar;
import co.uk.getmondo.api.model.signup.SignupSource;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.common.ae;
import co.uk.getmondo.common.ui.LoadingErrorView;
import co.uk.getmondo.signup.identity_verification.chat_with_us.ChatWithUsActivity;
import java.io.Serializable;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.TypeCastException;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000d\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0005\u0018\u0000 92\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u0004:\u00019B\u0005¢\u0006\u0002\u0010\u0005J\b\u0010*\u001a\u00020\u0013H\u0016J\b\u0010+\u001a\u00020\u0013H\u0016J\u0012\u0010,\u001a\u00020\u00132\b\u0010-\u001a\u0004\u0018\u00010.H\u0014J\b\u0010/\u001a\u00020\u0013H\u0014J\b\u00100\u001a\u00020\u0013H\u0016J\b\u0010\u0011\u001a\u00020\u0013H\u0016J\b\u00101\u001a\u00020\u0013H\u0016J\b\u00102\u001a\u00020\u0013H\u0016J\u001a\u00103\u001a\u00020\u00132\u0006\u00104\u001a\u0002052\b\u00106\u001a\u0004\u0018\u00010!H\u0016J\b\u00107\u001a\u00020\u0013H\u0016J\b\u00108\u001a\u00020\u0013H\u0016R\u001b\u0010\u0006\u001a\u00020\u00078BX\u0082\u0084\u0002¢\u0006\f\n\u0004\b\n\u0010\u000b\u001a\u0004\b\b\u0010\tR\u001b\u0010\f\u001a\u00020\r8BX\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0010\u0010\u000b\u001a\u0004\b\u000e\u0010\u000fR\u001a\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00130\u0012X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0015R\u001a\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00130\u00178VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0018\u0010\u0019R\u001e\u0010\u001a\u001a\u00020\u001b8\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\u001c\u0010\u001d\"\u0004\b\u001e\u0010\u001fR\u001d\u0010 \u001a\u0004\u0018\u00010!8BX\u0082\u0084\u0002¢\u0006\f\n\u0004\b$\u0010\u000b\u001a\u0004\b\"\u0010#R\u001b\u0010%\u001a\u00020&8BX\u0082\u0084\u0002¢\u0006\f\n\u0004\b)\u0010\u000b\u001a\u0004\b'\u0010(¨\u0006:"},
   d2 = {"Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity;", "Lco/uk/getmondo/signup/BaseSignupActivity;", "Lco/uk/getmondo/signup/identity_verification/IdentityVerificationPresenter$View;", "Lco/uk/getmondo/signup/identity_verification/IdentityDocumentsFragment$StepListener;", "Lco/uk/getmondo/signup/identity_verification/IdentityVerificationOnboardingFragment$StepListener;", "()V", "entryPoint", "Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;", "getEntryPoint", "()Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;", "entryPoint$delegate", "Lkotlin/Lazy;", "identityVerificationVersion", "Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationVersion;", "getIdentityVerificationVersion", "()Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationVersion;", "identityVerificationVersion$delegate", "onOnboardingComplete", "Lcom/jakewharton/rxrelay2/PublishRelay;", "", "getOnOnboardingComplete", "()Lcom/jakewharton/rxrelay2/PublishRelay;", "onRetryClicked", "Lio/reactivex/Observable;", "getOnRetryClicked", "()Lio/reactivex/Observable;", "presenter", "Lco/uk/getmondo/signup/identity_verification/IdentityVerificationPresenter;", "getPresenter", "()Lco/uk/getmondo/signup/identity_verification/IdentityVerificationPresenter;", "setPresenter", "(Lco/uk/getmondo/signup/identity_verification/IdentityVerificationPresenter;)V", "rejectedReason", "", "getRejectedReason", "()Ljava/lang/String;", "rejectedReason$delegate", "signupSource", "Lco/uk/getmondo/api/model/signup/SignupSource;", "getSignupSource", "()Lco/uk/getmondo/api/model/signup/SignupSource;", "signupSource$delegate", "hideLoading", "onBackPressed", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onDestroy", "onIdentityVerificationDocumentsSubmitted", "openBlocked", "openGenericError", "showIdentityDocuments", "allowSystemCamera", "", "rejectionNote", "showLoading", "showOnboarding", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class IdentityVerificationActivity extends co.uk.getmondo.signup.a implements e.b, t.b, v.a {
   // $FF: synthetic field
   static final kotlin.reflect.l[] a = new kotlin.reflect.l[]{(kotlin.reflect.l)kotlin.d.b.y.a(new kotlin.d.b.w(kotlin.d.b.y.a(IdentityVerificationActivity.class), "identityVerificationVersion", "getIdentityVerificationVersion()Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationVersion;")), (kotlin.reflect.l)kotlin.d.b.y.a(new kotlin.d.b.w(kotlin.d.b.y.a(IdentityVerificationActivity.class), "entryPoint", "getEntryPoint()Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;")), (kotlin.reflect.l)kotlin.d.b.y.a(new kotlin.d.b.w(kotlin.d.b.y.a(IdentityVerificationActivity.class), "rejectedReason", "getRejectedReason()Ljava/lang/String;")), (kotlin.reflect.l)kotlin.d.b.y.a(new kotlin.d.b.w(kotlin.d.b.y.a(IdentityVerificationActivity.class), "signupSource", "getSignupSource()Lco/uk/getmondo/api/model/signup/SignupSource;"))};
   public static final IdentityVerificationActivity.a g = new IdentityVerificationActivity.a((kotlin.d.b.i)null);
   public v b;
   private final kotlin.c h = kotlin.d.a((kotlin.d.a.a)(new kotlin.d.a.a() {
      public final co.uk.getmondo.signup.identity_verification.a.j b() {
         Serializable var1 = IdentityVerificationActivity.this.getIntent().getSerializableExtra("KEY_VERSION");
         if(var1 == null) {
            throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.signup.identity_verification.data.IdentityVerificationVersion");
         } else {
            return (co.uk.getmondo.signup.identity_verification.a.j)var1;
         }
      }

      // $FF: synthetic method
      public Object v_() {
         return this.b();
      }
   }));
   private final kotlin.c i = kotlin.d.a((kotlin.d.a.a)(new kotlin.d.a.a() {
      public final Impression.KycFrom b() {
         Serializable var1 = IdentityVerificationActivity.this.getIntent().getSerializableExtra("KEY_ENTRY_POINT");
         if(var1 == null) {
            throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.api.model.tracking.Impression.KycFrom");
         } else {
            return (Impression.KycFrom)var1;
         }
      }

      // $FF: synthetic method
      public Object v_() {
         return this.b();
      }
   }));
   private final kotlin.c j = kotlin.d.a((kotlin.d.a.a)(new kotlin.d.a.a() {
      public final String b() {
         return IdentityVerificationActivity.this.getIntent().getStringExtra("KEY_REJECTION_REASON");
      }

      // $FF: synthetic method
      public Object v_() {
         return this.b();
      }
   }));
   private final kotlin.c k = kotlin.d.a((kotlin.d.a.a)(new kotlin.d.a.a() {
      public final SignupSource b() {
         Serializable var1 = IdentityVerificationActivity.this.getIntent().getSerializableExtra("KEY_SIGNUP_SOURCE");
         if(var1 == null) {
            throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.api.model.signup.SignupSource");
         } else {
            return (SignupSource)var1;
         }
      }

      // $FF: synthetic method
      public Object v_() {
         return this.b();
      }
   }));
   private final com.b.b.c l;
   private HashMap m;

   public IdentityVerificationActivity() {
      com.b.b.c var1 = com.b.b.c.a();
      kotlin.d.b.l.a(var1, "PublishRelay.create()");
      this.l = var1;
   }

   public static final Intent a(Context var0, co.uk.getmondo.signup.identity_verification.a.j var1, Impression.KycFrom var2, SignupSource var3) {
      return IdentityVerificationActivity.a.a(g, var0, var1, var2, var3, (co.uk.getmondo.signup.j)null, (String)null, 48, (Object)null);
   }

   public static final Intent a(Context var0, co.uk.getmondo.signup.identity_verification.a.j var1, Impression.KycFrom var2, SignupSource var3, co.uk.getmondo.signup.j var4, String var5) {
      kotlin.d.b.l.b(var0, "context");
      kotlin.d.b.l.b(var1, "version");
      kotlin.d.b.l.b(var2, "from");
      kotlin.d.b.l.b(var3, "signupSource");
      kotlin.d.b.l.b(var4, "signupEntryPoint");
      return g.a(var0, var1, var2, var3, var4, var5);
   }

   private final co.uk.getmondo.signup.identity_verification.a.j v() {
      kotlin.c var2 = this.h;
      kotlin.reflect.l var1 = a[0];
      return (co.uk.getmondo.signup.identity_verification.a.j)var2.a();
   }

   private final Impression.KycFrom w() {
      kotlin.c var1 = this.i;
      kotlin.reflect.l var2 = a[1];
      return (Impression.KycFrom)var1.a();
   }

   private final String x() {
      kotlin.c var2 = this.j;
      kotlin.reflect.l var1 = a[2];
      return (String)var2.a();
   }

   private final SignupSource y() {
      kotlin.c var2 = this.k;
      kotlin.reflect.l var1 = a[3];
      return (SignupSource)var2.a();
   }

   public View a(int var1) {
      if(this.m == null) {
         this.m = new HashMap();
      }

      View var3 = (View)this.m.get(Integer.valueOf(var1));
      View var2 = var3;
      if(var3 == null) {
         var2 = this.findViewById(var1);
         this.m.put(Integer.valueOf(var1), var2);
      }

      return var2;
   }

   public void a(boolean var1, String var2) {
      Fragment var3 = this.getSupportFragmentManager().a(2131820968);
      if(!(var3 instanceof e)) {
         if(var2 == null) {
            var2 = this.x();
         }

         Fragment var4 = e.d.a(this.v(), this.w(), var2, var1, this.y());
         android.support.v4.app.t var5 = this.getSupportFragmentManager().a().b(2131820968, var4);
         if(var3 != null) {
            var5.a((String)null).a(4097);
         }

         var5.d();
      }

   }

   public io.reactivex.n c() {
      io.reactivex.n var1 = com.b.a.c.c.a((LoadingErrorView)this.a(co.uk.getmondo.c.a.idvErrorView)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var1, "RxView.clicks(this).map(VoidToUnit)");
      return var1;
   }

   public com.b.b.c d() {
      return this.l;
   }

   // $FF: synthetic method
   public io.reactivex.n e() {
      return (io.reactivex.n)this.d();
   }

   public void e_() {
      if(kotlin.d.b.l.a(this.y(), SignupSource.PERSONAL_ACCOUNT)) {
         this.setResult(-1);
         this.finish();
      } else {
         this.startActivity(VerificationPendingActivity.c.a((Context)this, this.v(), this.w()));
      }

   }

   public void f() {
      ae.a((View)((ProgressBar)this.a(co.uk.getmondo.c.a.idvStatusProgressBar)));
   }

   public void g() {
      ae.b((ProgressBar)this.a(co.uk.getmondo.c.a.idvStatusProgressBar));
   }

   public void h() {
      if(!(this.getSupportFragmentManager().a(2131820968) instanceof t)) {
         Fragment var1 = t.d.a(this.v(), this.w());
         this.getSupportFragmentManager().a().b(2131820968, var1).d();
      }

   }

   public void i() {
      this.startActivity(ChatWithUsActivity.b.a((Context)this));
      this.setResult(0);
      this.finish();
   }

   public void j() {
      this.startActivity(ChatWithUsActivity.b.a((Context)this));
      this.setResult(0);
      this.finish();
   }

   public void k() {
      this.d().a((Object)kotlin.n.a);
   }

   public void onBackPressed() {
      Fragment var1 = this.getSupportFragmentManager().a(2131820968);
      if(var1 instanceof t && ((t)var1).a()) {
         ((t)var1).b();
      } else {
         super.onBackPressed();
      }

   }

   protected void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.setContentView(2131034179);
      android.support.v7.app.a var2 = this.getSupportActionBar();
      if(var2 != null) {
         var2.b(false);
      }

      var2 = this.getSupportActionBar();
      if(var2 != null) {
         var2.d(false);
      }

      this.l().a(new p(this.v(), this.y())).a(new j(this.w(), this.x(), false)).a(this);
      v var3 = this.b;
      if(var3 == null) {
         kotlin.d.b.l.b("presenter");
      }

      var3.a((v.a)this);
   }

   protected void onDestroy() {
      v var1 = this.b;
      if(var1 == null) {
         kotlin.d.b.l.b("presenter");
      }

      var1.b();
      super.onDestroy();
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J>\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\b\b\u0002\u0010\u0013\u001a\u00020\u00142\n\b\u0002\u0010\u0015\u001a\u0004\u0018\u00010\u0004H\u0007R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000¨\u0006\u0016"},
      d2 = {"Lco/uk/getmondo/signup/identity_verification/IdentityVerificationActivity$Companion;", "", "()V", "KEY_ENTRY_POINT", "", "KEY_REJECTION_REASON", "KEY_SIGNUP_ENTRY_POINT", "KEY_SIGNUP_SOURCE", "KEY_VERSION", "buildIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "version", "Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationVersion;", "from", "Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;", "signupSource", "Lco/uk/getmondo/api/model/signup/SignupSource;", "signupEntryPoint", "Lco/uk/getmondo/signup/SignupEntryPoint;", "rejectedReason", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var1) {
         this();
      }

      public final Intent a(Context var1, co.uk.getmondo.signup.identity_verification.a.j var2, Impression.KycFrom var3, SignupSource var4, co.uk.getmondo.signup.j var5, String var6) {
         kotlin.d.b.l.b(var1, "context");
         kotlin.d.b.l.b(var2, "version");
         kotlin.d.b.l.b(var3, "from");
         kotlin.d.b.l.b(var4, "signupSource");
         kotlin.d.b.l.b(var5, "signupEntryPoint");
         Intent var7 = (new Intent(var1, IdentityVerificationActivity.class)).putExtra("KEY_VERSION", (Serializable)var2).putExtra("KEY_ENTRY_POINT", (Serializable)var3).putExtra("KEY_REJECTION_REASON", var6).putExtra("KEY_SIGNUP_SOURCE", (Serializable)var4).putExtra("KEY_SIGNUP_ENTRY_POINT", (Serializable)var5);
         kotlin.d.b.l.a(var7, "Intent(context, Identity…_POINT, signupEntryPoint)");
         return var7;
      }
   }
}
