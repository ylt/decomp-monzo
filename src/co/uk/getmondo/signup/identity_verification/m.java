package co.uk.getmondo.signup.identity_verification;

public final class m implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final j b;

   static {
      boolean var0;
      if(!m.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   public m(j var1) {
      if(!a && var1 == null) {
         throw new AssertionError();
      } else {
         this.b = var1;
      }
   }

   public static b.a.b a(j var0) {
      return new m(var0);
   }

   public String a() {
      return this.b.b();
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
