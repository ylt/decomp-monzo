package co.uk.getmondo.signup.identity_verification;

import co.uk.getmondo.signup.identity_verification.fallback.VideoFallbackActivity;
import co.uk.getmondo.signup.identity_verification.video.VideoRecordingActivity;

public interface o {
   i a(j var1);

   co.uk.getmondo.signup.identity_verification.id_picture.e a(co.uk.getmondo.signup.identity_verification.id_picture.f var1);

   void a(VerificationPendingActivity var1);

   void a(VideoFallbackActivity var1);

   void a(VideoRecordingActivity var1);
}
