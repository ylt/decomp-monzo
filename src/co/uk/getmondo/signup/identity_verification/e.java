package co.uk.getmondo.signup.identity_verification;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import co.uk.getmondo.api.model.identity_verification.IdentityDocumentType;
import co.uk.getmondo.api.model.signup.SignupSource;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.common.ui.ProgressButton;
import co.uk.getmondo.signup.identity_verification.fallback.DocumentFallbackActivity;
import co.uk.getmondo.signup.identity_verification.fallback.VideoFallbackActivity;
import co.uk.getmondo.signup.identity_verification.id_picture.DocumentCameraActivity;
import co.uk.getmondo.signup.identity_verification.video.VideoRecordingActivity;
import java.io.File;
import java.io.Serializable;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.TypeCastException;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000Ä\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0000\n\u0002\b\u0011\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\r\u0018\u0000 p2\u00020\u00012\u00020\u0002:\u0002pqB\u0005¢\u0006\u0002\u0010\u0003J\b\u0010/\u001a\u000200H\u0016J\b\u00101\u001a\u000200H\u0016J\b\u00102\u001a\u000200H\u0016J\u001a\u00103\u001a\u0002002\b\u00104\u001a\u0004\u0018\u00010$2\u0006\u00105\u001a\u000206H\u0002J\"\u00107\u001a\u0002002\u0006\u00108\u001a\u0002092\u0006\u0010:\u001a\u0002092\b\u0010;\u001a\u0004\u0018\u00010<H\u0016J\u0010\u0010=\u001a\u0002002\u0006\u0010>\u001a\u00020?H\u0016J\u0012\u0010@\u001a\u0002002\b\u0010A\u001a\u0004\u0018\u00010BH\u0016J$\u0010C\u001a\u00020D2\u0006\u0010E\u001a\u00020F2\b\u0010G\u001a\u0004\u0018\u00010H2\b\u0010A\u001a\u0004\u0018\u00010BH\u0016J\b\u0010I\u001a\u000200H\u0016J\u001a\u0010J\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u000e0\f0KH\u0016J\u000e\u0010L\u001a\b\u0012\u0004\u0012\u00020\u000e0KH\u0016J\b\u0010M\u001a\u000200H\u0016J\u000e\u0010N\u001a\b\u0012\u0004\u0012\u0002000KH\u0016J\u000e\u0010O\u001a\b\u0012\u0004\u0012\u00020P0KH\u0016J\u000e\u0010Q\u001a\b\u0012\u0004\u0012\u0002000KH\u0016J\u000e\u0010R\u001a\b\u0012\u0004\u0012\u0002000KH\u0016J\u001a\u0010S\u001a\u0002002\u0006\u0010T\u001a\u00020D2\b\u0010A\u001a\u0004\u0018\u00010BH\u0016J\u0018\u0010U\u001a\u0002002\u0006\u0010V\u001a\u00020\u000e2\u0006\u0010W\u001a\u00020\u0005H\u0016J \u0010X\u001a\u0002002\u0006\u0010Y\u001a\u00020\u00112\u0006\u0010Z\u001a\u00020\u000e2\u0006\u0010[\u001a\u00020\rH\u0016J \u0010\\\u001a\u0002002\u0006\u0010Y\u001a\u00020\u00112\u0006\u0010V\u001a\u00020\u000e2\u0006\u0010[\u001a\u00020\rH\u0016J\u0010\u0010]\u001a\u0002002\u0006\u0010Y\u001a\u00020\u0011H\u0016J\u0018\u0010^\u001a\u0002002\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010_\u001a\u00020\u0016H\u0016J\b\u0010`\u001a\u000200H\u0016J\u000e\u0010a\u001a\b\u0012\u0004\u0012\u00020\u00050bH\u0017J\u0010\u0010c\u001a\u0002002\u0006\u0010d\u001a\u00020eH\u0016J\b\u0010f\u001a\u000200H\u0016J\u0016\u0010g\u001a\b\u0012\u0004\u0012\u00020\u00050b2\u0006\u0010V\u001a\u00020\u000eH\u0016J\u000e\u0010h\u001a\b\u0012\u0004\u0012\u00020\u000e0bH\u0017J\b\u0010i\u001a\u000200H\u0016J\u000e\u0010j\u001a\b\u0012\u0004\u0012\u00020\u000e0bH\u0017J\u0010\u0010k\u001a\u0002002\u0006\u0010l\u001a\u00020$H\u0016J\u0010\u0010m\u001a\u0002002\u0006\u0010n\u001a\u00020$H\u0016J\b\u0010o\u001a\u000200H\u0016R\u001b\u0010\u0004\u001a\u00020\u00058BX\u0082\u0084\u0002¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\u0006\u0010\u0007Rb\u0010\n\u001aV\u0012$\u0012\"\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u000e \u000f*\u0010\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u000e\u0018\u00010\f0\f \u000f**\u0012$\u0012\"\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u000e \u000f*\u0010\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u000e\u0018\u00010\f0\f\u0018\u00010\u000b0\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u001b\u0010\u0010\u001a\u00020\u00118BX\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0014\u0010\t\u001a\u0004\b\u0012\u0010\u0013R\u001b\u0010\u0015\u001a\u00020\u00168BX\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0019\u0010\t\u001a\u0004\b\u0017\u0010\u0018R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082.¢\u0006\u0002\n\u0000R2\u0010\u001c\u001a&\u0012\f\u0012\n \u000f*\u0004\u0018\u00010\u000e0\u000e \u000f*\u0012\u0012\f\u0012\n \u000f*\u0004\u0018\u00010\u000e0\u000e\u0018\u00010\u000b0\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u001e\u0010\u001d\u001a\u00020\u001e8\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\u001f\u0010 \"\u0004\b!\u0010\"R\u001d\u0010#\u001a\u0004\u0018\u00010$8BX\u0082\u0084\u0002¢\u0006\f\n\u0004\b'\u0010\t\u001a\u0004\b%\u0010&R\u0010\u0010(\u001a\u0004\u0018\u00010)X\u0082\u000e¢\u0006\u0002\n\u0000R\u001b\u0010*\u001a\u00020+8BX\u0082\u0084\u0002¢\u0006\f\n\u0004\b.\u0010\t\u001a\u0004\b,\u0010-¨\u0006r"},
   d2 = {"Lco/uk/getmondo/signup/identity_verification/IdentityDocumentsFragment;", "Lco/uk/getmondo/common/fragments/BaseFragment;", "Lco/uk/getmondo/signup/identity_verification/IdentityDocumentsPresenter$View;", "()V", "allowSystemCamera", "", "getAllowSystemCamera", "()Z", "allowSystemCamera$delegate", "Lkotlin/Lazy;", "documentCountryRelay", "Lcom/jakewharton/rxrelay2/PublishRelay;", "Lkotlin/Pair;", "Lco/uk/getmondo/model/Country;", "Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;", "kotlin.jvm.PlatformType", "identityVerificationVersion", "Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationVersion;", "getIdentityVerificationVersion", "()Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationVersion;", "identityVerificationVersion$delegate", "kycFrom", "Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;", "getKycFrom", "()Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;", "kycFrom$delegate", "listener", "Lco/uk/getmondo/signup/identity_verification/IdentityDocumentsFragment$StepListener;", "photoIdRelay", "presenter", "Lco/uk/getmondo/signup/identity_verification/IdentityDocumentsPresenter;", "getPresenter", "()Lco/uk/getmondo/signup/identity_verification/IdentityDocumentsPresenter;", "setPresenter", "(Lco/uk/getmondo/signup/identity_verification/IdentityDocumentsPresenter;)V", "rejectedReason", "", "getRejectedReason", "()Ljava/lang/String;", "rejectedReason$delegate", "rejectedReasonSnackbar", "Landroid/support/design/widget/Snackbar;", "signupSource", "Lco/uk/getmondo/api/model/signup/SignupSource;", "getSignupSource", "()Lco/uk/getmondo/api/model/signup/SignupSource;", "signupSource$delegate", "disableSubmitVerificationButton", "", "enableSubmitVerificationButton", "hideLoading", "loadThumbnail", "path", "imageView", "Landroid/widget/ImageView;", "onActivityResult", "requestCode", "", "resultCode", "data", "Landroid/content/Intent;", "onAttach", "context", "Landroid/content/Context;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "onDestroyView", "onDocumentCountrySelected", "Lio/reactivex/Observable;", "onPhotoIdTypeChosen", "onStop", "onSubmitVerificationClicked", "onSubmitVerificationConfirmationClicked", "", "onTakePhotoClicked", "onTakeVideoClicked", "onViewCreated", "view", "openCountrySelection", "documentType", "useSystemCamera", "openDocumentCamera", "version", "idDocumentType", "country", "openDocumentCameraFallback", "openTakeVideo", "openVerificationPending", "from", "openVideoFallback", "showDoYouHavePassport", "Lio/reactivex/Maybe;", "showIdentityDocComplete", "identityDocument", "Lco/uk/getmondo/signup/identity_verification/data/model/IdentityDocument;", "showIdentityDocIncomplete", "showIsDocumentUk", "showLegacyIdentityDocumentSelection", "showLoading", "showOtherIdentityDocumentsSelection", "showRejectedReason", "message", "showVideoComplete", "videoPath", "showVideoIncomplete", "Companion", "StepListener", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class e extends co.uk.getmondo.common.f.a implements g.a {
   // $FF: synthetic field
   static final kotlin.reflect.l[] a = new kotlin.reflect.l[]{(kotlin.reflect.l)kotlin.d.b.y.a(new kotlin.d.b.w(kotlin.d.b.y.a(e.class), "identityVerificationVersion", "getIdentityVerificationVersion()Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationVersion;")), (kotlin.reflect.l)kotlin.d.b.y.a(new kotlin.d.b.w(kotlin.d.b.y.a(e.class), "kycFrom", "getKycFrom()Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;")), (kotlin.reflect.l)kotlin.d.b.y.a(new kotlin.d.b.w(kotlin.d.b.y.a(e.class), "rejectedReason", "getRejectedReason()Ljava/lang/String;")), (kotlin.reflect.l)kotlin.d.b.y.a(new kotlin.d.b.w(kotlin.d.b.y.a(e.class), "allowSystemCamera", "getAllowSystemCamera()Z")), (kotlin.reflect.l)kotlin.d.b.y.a(new kotlin.d.b.w(kotlin.d.b.y.a(e.class), "signupSource", "getSignupSource()Lco/uk/getmondo/api/model/signup/SignupSource;"))};
   public static final e.a d = new e.a((kotlin.d.b.i)null);
   public g c;
   private final com.b.b.c e = com.b.b.c.a();
   private final com.b.b.c f = com.b.b.c.a();
   private final kotlin.c g = kotlin.d.a((kotlin.d.a.a)(new kotlin.d.a.a() {
      public final co.uk.getmondo.signup.identity_verification.a.j b() {
         Serializable var1 = e.this.getArguments().getSerializable("KEY_VERSION");
         if(var1 == null) {
            throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.signup.identity_verification.data.IdentityVerificationVersion");
         } else {
            return (co.uk.getmondo.signup.identity_verification.a.j)var1;
         }
      }

      // $FF: synthetic method
      public Object v_() {
         return this.b();
      }
   }));
   private final kotlin.c h = kotlin.d.a((kotlin.d.a.a)(new kotlin.d.a.a() {
      public final Impression.KycFrom b() {
         Serializable var1 = e.this.getArguments().getSerializable("KEY_ENTRY_POINT");
         if(var1 == null) {
            throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.api.model.tracking.Impression.KycFrom");
         } else {
            return (Impression.KycFrom)var1;
         }
      }

      // $FF: synthetic method
      public Object v_() {
         return this.b();
      }
   }));
   private final kotlin.c i = kotlin.d.a((kotlin.d.a.a)(new kotlin.d.a.a() {
      public final String b() {
         return e.this.getArguments().getString("KEY_REJECTION_REASON");
      }

      // $FF: synthetic method
      public Object v_() {
         return this.b();
      }
   }));
   private final kotlin.c j = kotlin.d.a((kotlin.d.a.a)(new kotlin.d.a.a() {
      public final boolean b() {
         return e.this.getArguments().getBoolean("KEY_ALLOW_SYSTEM_CAMERA", false);
      }

      // $FF: synthetic method
      public Object v_() {
         return Boolean.valueOf(this.b());
      }
   }));
   private final kotlin.c k = kotlin.d.a((kotlin.d.a.a)(new kotlin.d.a.a() {
      public final SignupSource b() {
         Serializable var1 = e.this.getArguments().getSerializable("KEY_SIGNUP_SOURCE");
         if(var1 == null) {
            throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.api.model.signup.SignupSource");
         } else {
            return (SignupSource)var1;
         }
      }

      // $FF: synthetic method
      public Object v_() {
         return this.b();
      }
   }));
   private e.b l;
   private Snackbar m;
   private HashMap n;

   private final void a(String var1, ImageView var2) {
      File var3 = new File(var1);
      com.bumptech.glide.g.a((Fragment)this).a(var3).a((com.bumptech.glide.load.c)(new com.bumptech.glide.h.b(String.valueOf(var3.lastModified())))).a(0.2F).a(var2);
   }

   private final co.uk.getmondo.signup.identity_verification.a.j r() {
      kotlin.c var1 = this.g;
      kotlin.reflect.l var2 = a[0];
      return (co.uk.getmondo.signup.identity_verification.a.j)var1.a();
   }

   private final Impression.KycFrom s() {
      kotlin.c var1 = this.h;
      kotlin.reflect.l var2 = a[1];
      return (Impression.KycFrom)var1.a();
   }

   private final String t() {
      kotlin.c var2 = this.i;
      kotlin.reflect.l var1 = a[2];
      return (String)var2.a();
   }

   private final boolean v() {
      kotlin.c var1 = this.j;
      kotlin.reflect.l var2 = a[3];
      return ((Boolean)var1.a()).booleanValue();
   }

   private final SignupSource w() {
      kotlin.c var2 = this.k;
      kotlin.reflect.l var1 = a[4];
      return (SignupSource)var2.a();
   }

   public View a(int var1) {
      if(this.n == null) {
         this.n = new HashMap();
      }

      View var3 = (View)this.n.get(Integer.valueOf(var1));
      View var2 = var3;
      if(var3 == null) {
         var2 = this.getView();
         if(var2 == null) {
            var2 = null;
         } else {
            var2 = var2.findViewById(var1);
            this.n.put(Integer.valueOf(var1), var2);
         }
      }

      return var2;
   }

   public io.reactivex.h a(IdentityDocumentType var1) {
      kotlin.d.b.l.b(var1, "documentType");
      return co.uk.getmondo.common.j.e.a((kotlin.d.a.b)(new kotlin.d.a.b(this.getString(var1.c())) {
         // $FF: synthetic field
         final String b;

         {
            this.b = var2;
         }

         public final android.support.v7.app.d a(final kotlin.d.a.b var1) {
            kotlin.d.b.l.b(var1, "setResult");
            android.support.v7.app.d var2 = (new android.support.v7.app.d.a(e.this.getContext(), 2131493135)).a((CharSequence)e.this.getString(2131362161, new Object[]{co.uk.getmondo.common.k.p.i(this.b)})).b((CharSequence)e.this.getString(2131362160, new Object[]{this.b})).a(2131362159, (OnClickListener)(new OnClickListener() {
               public final void onClick(DialogInterface var1x, int var2) {
                  var1.a(Boolean.valueOf(true));
               }
            })).b(2131362158, (OnClickListener)(new OnClickListener() {
               public final void onClick(DialogInterface var1x, int var2) {
                  var1.a(Boolean.valueOf(false));
               }
            })).a(false).b();
            kotlin.d.b.l.a(var2, "AlertDialog.Builder(cont…                .create()");
            return var2;
         }
      }));
   }

   public io.reactivex.n a() {
      io.reactivex.n var1 = com.b.a.c.c.a((Button)this.a(co.uk.getmondo.c.a.takePhotoButton)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var1, "RxView.clicks(this).map(VoidToUnit)");
      return var1;
   }

   public void a(IdentityDocumentType var1, boolean var2) {
      kotlin.d.b.l.b(var1, "documentType");
      this.startActivityForResult(CountrySelectionActivity.a(this.getContext(), var1), 1001);
   }

   public void a(co.uk.getmondo.signup.identity_verification.a.a.b var1) {
      kotlin.d.b.l.b(var1, "identityDocument");
      ((ImageView)this.a(co.uk.getmondo.c.a.photoCompleteImageView)).setImageResource(2130837954);
      ((Button)this.a(co.uk.getmondo.c.a.takePhotoButton)).setText(2131362298);
      ((Button)this.a(co.uk.getmondo.c.a.takePhotoButton)).setActivated(true);
      ((ImageView)this.a(co.uk.getmondo.c.a.primaryDocumentThumbnail)).setVisibility(0);
      String var3 = var1.a();
      ImageView var2 = (ImageView)this.a(co.uk.getmondo.c.a.primaryDocumentThumbnail);
      kotlin.d.b.l.a(var2, "primaryDocumentThumbnail");
      this.a(var3, var2);
      if(var1.d() != null) {
         ((ImageView)this.a(co.uk.getmondo.c.a.secondaryDocumentThumbnail)).setVisibility(0);
         String var4 = var1.d();
         var2 = (ImageView)this.a(co.uk.getmondo.c.a.secondaryDocumentThumbnail);
         kotlin.d.b.l.a(var2, "secondaryDocumentThumbnail");
         this.a(var4, var2);
      } else {
         ((ImageView)this.a(co.uk.getmondo.c.a.secondaryDocumentThumbnail)).setVisibility(8);
      }

   }

   public void a(co.uk.getmondo.signup.identity_verification.a.j var1) {
      kotlin.d.b.l.b(var1, "version");
      VideoRecordingActivity.a var2 = VideoRecordingActivity.b;
      Context var3 = this.getContext();
      kotlin.d.b.l.a(var3, "context");
      this.startActivity(var2.a(var3, var1, this.w()));
   }

   public void a(co.uk.getmondo.signup.identity_verification.a.j var1, IdentityDocumentType var2, co.uk.getmondo.d.i var3) {
      kotlin.d.b.l.b(var1, "version");
      kotlin.d.b.l.b(var2, "idDocumentType");
      kotlin.d.b.l.b(var3, "country");
      this.startActivity(DocumentCameraActivity.a(this.getContext(), var1, var2, var3, this.w()));
   }

   public void a(co.uk.getmondo.signup.identity_verification.a.j var1, Impression.KycFrom var2) {
      kotlin.d.b.l.b(var1, "identityVerificationVersion");
      kotlin.d.b.l.b(var2, "from");
      e.b var3 = this.l;
      if(var3 == null) {
         kotlin.d.b.l.b("listener");
      }

      var3.e_();
   }

   public void a(String var1) {
      kotlin.d.b.l.b(var1, "message");
      Snackbar var2 = co.uk.getmondo.common.ui.i.a(this.getContext(), (RelativeLayout)this.a(co.uk.getmondo.c.a.rootView), var1, (int)TimeUnit.MINUTES.toMillis(1L), true).a(2131362202, (android.view.View.OnClickListener)(new android.view.View.OnClickListener() {
         public final void onClick(View var1) {
            Snackbar var2 = e.this.m;
            if(var2 != null) {
               var2.d();
            }

         }
      })).e(android.support.v4.content.a.c(this.getContext(), 2131689706));
      var2.c();
      this.m = var2;
   }

   public io.reactivex.n b() {
      io.reactivex.n var1 = com.b.a.c.c.a((Button)this.a(co.uk.getmondo.c.a.takeVideoButton)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var1, "RxView.clicks(this).map(VoidToUnit)");
      return var1;
   }

   public void b(co.uk.getmondo.signup.identity_verification.a.j var1, IdentityDocumentType var2, co.uk.getmondo.d.i var3) {
      kotlin.d.b.l.b(var1, "version");
      kotlin.d.b.l.b(var2, "documentType");
      kotlin.d.b.l.b(var3, "country");
      DocumentFallbackActivity.a var4 = DocumentFallbackActivity.c;
      Context var5 = this.getContext();
      kotlin.d.b.l.a(var5, "context");
      this.startActivity(var4.a(var5, var2, var3, this.w()));
   }

   public void b(String var1) {
      kotlin.d.b.l.b(var1, "videoPath");
      ((ImageView)this.a(co.uk.getmondo.c.a.videoCompleteImageView)).setImageResource(2130837954);
      ((Button)this.a(co.uk.getmondo.c.a.takeVideoButton)).setText(2131362298);
      ((Button)this.a(co.uk.getmondo.c.a.takeVideoButton)).setActivated(true);
      ((ImageView)this.a(co.uk.getmondo.c.a.videoThumbnail)).setVisibility(0);
      ImageView var2 = (ImageView)this.a(co.uk.getmondo.c.a.videoThumbnail);
      kotlin.d.b.l.a(var2, "videoThumbnail");
      this.a(var1, var2);
   }

   public io.reactivex.n c() {
      io.reactivex.n var1 = com.b.a.c.c.a((ProgressButton)this.a(co.uk.getmondo.c.a.submitVerificationButton)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var1, "RxView.clicks(this).map(VoidToUnit)");
      return var1;
   }

   public io.reactivex.n d() {
      io.reactivex.n var1 = io.reactivex.n.create((io.reactivex.p)(new io.reactivex.p() {
         public final void a(final io.reactivex.o var1) {
            kotlin.d.b.l.b(var1, "emitter");
            final a var2 = a.a.a();
            var2.a((kotlin.d.a.a)(new kotlin.d.a.a() {
               public final void b() {
                  var1.a(kotlin.n.a);
               }

               // $FF: synthetic method
               public Object v_() {
                  this.b();
                  return kotlin.n.a;
               }
            }));
            var2.b((kotlin.d.a.a)(new kotlin.d.a.a() {
               public final void b() {
                  var1.a();
               }

               // $FF: synthetic method
               public Object v_() {
                  this.b();
                  return kotlin.n.a;
               }
            }));
            var2.show(e.this.getChildFragmentManager(), "TAG_CONFIRM_VERIFY_IDENTITY");
            var1.a((io.reactivex.c.f)(new io.reactivex.c.f() {
               public final void a() {
                  var2.a((kotlin.d.a.a)null);
                  var2.b((kotlin.d.a.a)null);
                  var2.dismiss();
               }
            }));
         }
      }));
      kotlin.d.b.l.a(var1, "Observable.create { emit…)\n            }\n        }");
      return var1;
   }

   public io.reactivex.n e() {
      com.b.b.c var1 = this.f;
      kotlin.d.b.l.a(var1, "documentCountryRelay");
      return (io.reactivex.n)var1;
   }

   @SuppressLint({"InflateParams"})
   public io.reactivex.h f() {
      return co.uk.getmondo.common.j.e.a((kotlin.d.a.b)(new kotlin.d.a.b() {
         public final android.support.design.widget.c a(final kotlin.d.a.b var1) {
            kotlin.d.b.l.b(var1, "setResult");
            ContextThemeWrapper var2 = new ContextThemeWrapper(e.this.getContext(), 2131493134);
            View var4 = e.this.getLayoutInflater().cloneInContext((Context)var2).inflate(2131034236, (ViewGroup)null);
            android.support.design.widget.c var3 = new android.support.design.widget.c(e.this.getContext());
            var3.setContentView(var4);
            var4.findViewById(2131821200).setOnClickListener((android.view.View.OnClickListener)(new android.view.View.OnClickListener() {
               public final void onClick(View var1x) {
                  var1.a(IdentityDocumentType.DRIVING_LICENSE);
               }
            }));
            var4.findViewById(2131821201).setOnClickListener((android.view.View.OnClickListener)(new android.view.View.OnClickListener() {
               public final void onClick(View var1x) {
                  var1.a(IdentityDocumentType.PASSPORT);
               }
            }));
            return var3;
         }
      }));
   }

   @SuppressLint({"InflateParams"})
   public io.reactivex.h g() {
      return co.uk.getmondo.common.j.e.a((kotlin.d.a.b)(new kotlin.d.a.b() {
         public final android.support.design.widget.c a(final kotlin.d.a.b var1) {
            kotlin.d.b.l.b(var1, "setResult");
            ContextThemeWrapper var2 = new ContextThemeWrapper(e.this.getContext(), 2131493134);
            View var3 = e.this.getLayoutInflater().cloneInContext((Context)var2).inflate(2131034235, (ViewGroup)null);
            android.support.design.widget.c var4 = new android.support.design.widget.c(e.this.getContext());
            var4.setContentView(var3);
            var3.findViewById(2131821198).setOnClickListener((android.view.View.OnClickListener)(new android.view.View.OnClickListener() {
               public final void onClick(View var1x) {
                  var1.a(Boolean.valueOf(true));
               }
            }));
            var3.findViewById(2131821199).setOnClickListener((android.view.View.OnClickListener)(new android.view.View.OnClickListener() {
               public final void onClick(View var1x) {
                  var1.a(Boolean.valueOf(false));
               }
            }));
            return var4;
         }
      }));
   }

   @SuppressLint({"InflateParams"})
   public io.reactivex.h h() {
      return co.uk.getmondo.common.j.e.a((kotlin.d.a.b)(new kotlin.d.a.b() {
         public final android.support.design.widget.c a(final kotlin.d.a.b var1) {
            kotlin.d.b.l.b(var1, "setResult");
            ContextThemeWrapper var2 = new ContextThemeWrapper(e.this.getContext(), 2131493134);
            View var3 = e.this.getLayoutInflater().cloneInContext((Context)var2).inflate(2131034233, (ViewGroup)null);
            android.support.design.widget.c var4 = new android.support.design.widget.c(e.this.getContext());
            var4.setContentView(var3);
            var3.findViewById(2131821195).setOnClickListener((android.view.View.OnClickListener)(new android.view.View.OnClickListener() {
               public final void onClick(View var1x) {
                  var1.a(IdentityDocumentType.NATIONAL_ID);
               }
            }));
            var3.findViewById(2131821194).setOnClickListener((android.view.View.OnClickListener)(new android.view.View.OnClickListener() {
               public final void onClick(View var1x) {
                  var1.a(IdentityDocumentType.DRIVING_LICENSE);
               }
            }));
            return var4;
         }
      }));
   }

   public void i() {
      VideoFallbackActivity.a var1 = VideoFallbackActivity.e;
      Context var2 = this.getContext();
      kotlin.d.b.l.a(var2, "context");
      this.startActivity(var1.a(var2, this.w()));
   }

   public void j() {
      ((ProgressButton)this.a(co.uk.getmondo.c.a.submitVerificationButton)).setLoading(true);
   }

   public void k() {
      ((ProgressButton)this.a(co.uk.getmondo.c.a.submitVerificationButton)).setLoading(false);
   }

   public void l() {
      ((ProgressButton)this.a(co.uk.getmondo.c.a.submitVerificationButton)).setEnabled(true);
   }

   public void m() {
      ((ProgressButton)this.a(co.uk.getmondo.c.a.submitVerificationButton)).setEnabled(false);
   }

   public void n() {
      ((ImageView)this.a(co.uk.getmondo.c.a.photoCompleteImageView)).setImageResource(2130837834);
      ((Button)this.a(co.uk.getmondo.c.a.takePhotoButton)).setText(2131362319);
      ((Button)this.a(co.uk.getmondo.c.a.takePhotoButton)).setActivated(false);
      ((ImageView)this.a(co.uk.getmondo.c.a.primaryDocumentThumbnail)).setVisibility(8);
      ((ImageView)this.a(co.uk.getmondo.c.a.primaryDocumentThumbnail)).setImageBitmap((Bitmap)null);
      ((ImageView)this.a(co.uk.getmondo.c.a.secondaryDocumentThumbnail)).setVisibility(8);
      ((ImageView)this.a(co.uk.getmondo.c.a.secondaryDocumentThumbnail)).setImageBitmap((Bitmap)null);
   }

   public void o() {
      ((ImageView)this.a(co.uk.getmondo.c.a.videoCompleteImageView)).setImageResource(2130837835);
      ((Button)this.a(co.uk.getmondo.c.a.takeVideoButton)).setText(2131362332);
      ((Button)this.a(co.uk.getmondo.c.a.takeVideoButton)).setActivated(false);
      ((ImageView)this.a(co.uk.getmondo.c.a.videoThumbnail)).setVisibility(8);
      ((ImageView)this.a(co.uk.getmondo.c.a.videoThumbnail)).setImageBitmap((Bitmap)null);
   }

   public void onActivityResult(int var1, int var2, Intent var3) {
      if(var1 == 1001 && var2 == -1) {
         co.uk.getmondo.d.i var4 = CountrySelectionActivity.b(var3);
         IdentityDocumentType var5 = CountrySelectionActivity.a(var3);
         this.f.a((Object)(new kotlin.h(var4, var5)));
      } else {
         super.onActivityResult(var1, var2, var3);
      }

   }

   public void onAttach(Context var1) {
      kotlin.d.b.l.b(var1, "context");
      super.onAttach(var1);
      if(var1 instanceof e.b) {
         this.l = (e.b)var1;
      } else {
         throw (Throwable)(new IllegalStateException("Activity must implement StepListener"));
      }
   }

   public void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.B().a(new p(this.r(), this.w())).a(new j(this.s(), this.t(), this.v())).a(this);
   }

   public View onCreateView(LayoutInflater var1, ViewGroup var2, Bundle var3) {
      kotlin.d.b.l.b(var1, "inflater");
      View var4 = var1.inflate(2131034271, var2, false);
      kotlin.d.b.l.a(var4, "inflater.inflate(R.layou…uments, container, false)");
      return var4;
   }

   public void onDestroyView() {
      g var1 = this.c;
      if(var1 == null) {
         kotlin.d.b.l.b("presenter");
      }

      var1.b();
      super.onDestroyView();
      this.p();
   }

   public void onStop() {
      Snackbar var1 = this.m;
      if(var1 != null) {
         var1.d();
      }

      super.onStop();
   }

   public void onViewCreated(View var1, Bundle var2) {
      kotlin.d.b.l.b(var1, "view");
      super.onViewCreated(var1, var2);
      ((ImageView)this.a(co.uk.getmondo.c.a.primaryDocumentThumbnail)).setClipToOutline(true);
      ((ImageView)this.a(co.uk.getmondo.c.a.secondaryDocumentThumbnail)).setClipToOutline(true);
      ((ImageView)this.a(co.uk.getmondo.c.a.videoThumbnail)).setClipToOutline(true);
      g var3 = this.c;
      if(var3 == null) {
         kotlin.d.b.l.b("presenter");
      }

      var3.a((g.a)this);
   }

   public void p() {
      if(this.n != null) {
         this.n.clear();
      }

   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J0\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\b\u0010\u0012\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000¨\u0006\u0017"},
      d2 = {"Lco/uk/getmondo/signup/identity_verification/IdentityDocumentsFragment$Companion;", "", "()V", "KEY_ALLOW_SYSTEM_CAMERA", "", "KEY_ENTRY_POINT", "KEY_REJECTION_REASON", "KEY_REQUEST_CODE_COUNTRY", "", "KEY_SIGNUP_SOURCE", "KEY_VERSION", "TAG_CONFIRM_VERIFY_IDENTITY", "newInstance", "Landroid/support/v4/app/Fragment;", "version", "Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationVersion;", "from", "Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;", "rejectedReason", "allowSystemCamera", "", "signupSource", "Lco/uk/getmondo/api/model/signup/SignupSource;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var1) {
         this();
      }

      public final Fragment a(co.uk.getmondo.signup.identity_verification.a.j var1, Impression.KycFrom var2, String var3, boolean var4, SignupSource var5) {
         kotlin.d.b.l.b(var1, "version");
         kotlin.d.b.l.b(var2, "from");
         kotlin.d.b.l.b(var5, "signupSource");
         e var6 = new e();
         Bundle var7 = new Bundle();
         var7.putSerializable("KEY_VERSION", (Serializable)var1);
         var7.putSerializable("KEY_ENTRY_POINT", (Serializable)var2);
         var7.putString("KEY_REJECTION_REASON", var3);
         var7.putBoolean("KEY_ALLOW_SYSTEM_CAMERA", var4);
         var7.putSerializable("KEY_SIGNUP_SOURCE", (Serializable)var5);
         var6.setArguments(var7);
         return (Fragment)var6;
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&¨\u0006\u0004"},
      d2 = {"Lco/uk/getmondo/signup/identity_verification/IdentityDocumentsFragment$StepListener;", "", "onIdentityVerificationDocumentsSubmitted", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface b {
      void e_();
   }
}
