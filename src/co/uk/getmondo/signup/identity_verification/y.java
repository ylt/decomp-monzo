package co.uk.getmondo.signup.identity_verification;

import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0013\b\u0086\u0001\u0018\u0000 \u00172\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u0017B\u0019\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nj\u0002\b\u000bj\u0002\b\fj\u0002\b\rj\u0002\b\u000ej\u0002\b\u000fj\u0002\b\u0010j\u0002\b\u0011j\u0002\b\u0012j\u0002\b\u0013j\u0002\b\u0014j\u0002\b\u0015j\u0002\b\u0016¨\u0006\u0018"},
   d2 = {"Lco/uk/getmondo/signup/identity_verification/KycRejectedReason;", "", "apiValue", "", "rejectedReasonResId", "", "(Ljava/lang/String;ILjava/lang/String;I)V", "getApiValue", "()Ljava/lang/String;", "getRejectedReasonResId", "()I", "UNKNOWN", "DARK_PHOTO", "BLURRY_PHOTO", "PARTIAL_PHOTO", "REFLECTION_IN_PHOTO", "DARK_VIDEO", "NO_SOUND", "EXPIRED_DOCUMENT", "NOT_GOV_ID", "NO_DOB", "NO_EXPIRY_DATE", "OTHER", "Find", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public enum y {
   a,
   b,
   c,
   d,
   e,
   f,
   g,
   h,
   i,
   j,
   k,
   l;

   public static final y.a m;
   private final String o;
   private final int p;

   static {
      y var4 = new y("UNKNOWN", 0, "", 2131362366);
      a = var4;
      y var8 = new y("DARK_PHOTO", 1, "DARK_PHOTO", 2131362356);
      b = var8;
      y var10 = new y("BLURRY_PHOTO", 2, "BLURRY_PHOTO", 2131362355);
      c = var10;
      y var11 = new y("PARTIAL_PHOTO", 3, "PARTIAL_PHOTO", 2131362364);
      d = var11;
      y var3 = new y("REFLECTION_IN_PHOTO", 4, "REFLECTION_IN_PHOTO", 2131362365);
      e = var3;
      y var7 = new y("DARK_VIDEO", 5, "DARK_VIDEO", 2131362357);
      f = var7;
      y var5 = new y("NO_SOUND", 6, "NO_SOUND", 2131362362);
      g = var5;
      y var9 = new y("EXPIRED_DOCUMENT", 7, "EXPIRED_DOCUMENT", 2131362358);
      h = var9;
      y var6 = new y("NOT_GOV_ID", 8, "NOT_GOV_ID", 2131362361);
      i = var6;
      y var2 = new y("NO_DOB", 9, "NO_DOB", 2131362359);
      j = var2;
      y var0 = new y("NO_EXPIRY_DATE", 10, "NO_EXPIRY_DATE", 2131362360);
      k = var0;
      y var1 = new y("OTHER", 11, "OTHER", 2131362363);
      l = var1;
      m = new y.a((kotlin.d.b.i)null);
   }

   protected y(String var3, int var4) {
      kotlin.d.b.l.b(var3, "apiValue");
      super(var1, var2);
      this.o = var3;
      this.p = var4;
   }

   public static final y a(String var0) {
      kotlin.d.b.l.b(var0, "apiValue");
      return m.a(var0);
   }

   public final String a() {
      return this.o;
   }

   public final int b() {
      return this.p;
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007¨\u0006\u0007"},
      d2 = {"Lco/uk/getmondo/signup/identity_verification/KycRejectedReason$Find;", "", "()V", "fromApiType", "Lco/uk/getmondo/signup/identity_verification/KycRejectedReason;", "apiValue", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var1) {
         this();
      }

      public final y a(String var1) {
         kotlin.d.b.l.b(var1, "apiValue");
         Object[] var4 = (Object[])y.values();
         int var2 = 0;

         Object var5;
         while(true) {
            if(var2 >= var4.length) {
               var5 = null;
               break;
            }

            Object var3 = var4[var2];
            if(kotlin.d.b.l.a(((y)var3).a(), var1)) {
               var5 = var3;
               break;
            }

            ++var2;
         }

         y var6 = (y)var5;
         if(var6 == null) {
            var6 = y.a;
         }

         return var6;
      }
   }
}
