package co.uk.getmondo.signup.identity_verification;

import co.uk.getmondo.api.IdentityVerificationApi;
import co.uk.getmondo.api.ae;
import co.uk.getmondo.api.model.signup.SignupSource;

public final class q implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final p b;
   private final javax.a.a c;
   private final javax.a.a d;
   private final javax.a.a e;
   private final javax.a.a f;
   private final javax.a.a g;

   static {
      boolean var0;
      if(!q.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   public q(p var1, javax.a.a var2, javax.a.a var3, javax.a.a var4, javax.a.a var5, javax.a.a var6) {
      if(!a && var1 == null) {
         throw new AssertionError();
      } else {
         this.b = var1;
         if(!a && var2 == null) {
            throw new AssertionError();
         } else {
            this.c = var2;
            if(!a && var3 == null) {
               throw new AssertionError();
            } else {
               this.d = var3;
               if(!a && var4 == null) {
                  throw new AssertionError();
               } else {
                  this.e = var4;
                  if(!a && var5 == null) {
                     throw new AssertionError();
                  } else {
                     this.f = var5;
                     if(!a && var6 == null) {
                        throw new AssertionError();
                     } else {
                        this.g = var6;
                     }
                  }
               }
            }
         }
      }
   }

   public static b.a.b a(p var0, javax.a.a var1, javax.a.a var2, javax.a.a var3, javax.a.a var4, javax.a.a var5) {
      return new q(var0, var1, var2, var3, var4, var5);
   }

   public co.uk.getmondo.signup.identity_verification.a.e a() {
      return (co.uk.getmondo.signup.identity_verification.a.e)b.a.d.a(this.b.a((co.uk.getmondo.signup.identity_verification.a.h)this.c.b(), (IdentityVerificationApi)this.d.b(), (ae)this.e.b(), (co.uk.getmondo.common.i)this.f.b(), (SignupSource)this.g.b()), "Cannot return null from a non-@Nullable @Provides method");
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
