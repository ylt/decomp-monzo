package co.uk.getmondo.signup.identity_verification.a;

import co.uk.getmondo.api.IdentityVerificationApi;
import co.uk.getmondo.api.ae;
import co.uk.getmondo.api.model.identity_verification.ContentType;
import co.uk.getmondo.api.model.identity_verification.FileType;
import co.uk.getmondo.api.model.identity_verification.FileUpload;
import co.uk.getmondo.api.model.identity_verification.IdentityVerification;
import co.uk.getmondo.api.model.signup.SignupSource;
import io.reactivex.n;
import io.reactivex.v;
import io.reactivex.z;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.UUID;
import java.util.concurrent.Callable;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000l\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001B-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b¢\u0006\u0002\u0010\fJ4\u0010\r\u001a&\u0012\f\u0012\n \u0010*\u0004\u0018\u00010\u000f0\u000f \u0010*\u0012\u0012\f\u0012\n \u0010*\u0004\u0018\u00010\u000f0\u000f\u0018\u00010\u000e0\u000e2\u0006\u0010\u0011\u001a\u00020\u0012H\u0002J\u0014\u0010\u0013\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00160\u00150\u0014H\u0016J\u000e\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00180\u000eH\u0016J\u000e\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u001a0\u000eH\u0016J\b\u0010\u001b\u001a\u00020\u001cH\u0002J\u001e\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u00120\u000e2\u0006\u0010\u001e\u001a\u00020\u00122\u0006\u0010\u001f\u001a\u00020 H\u0002J\u0018\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020\u00162\u0006\u0010$\u001a\u00020\u001aH\u0016J\u0018\u0010%\u001a\u00020\"2\u0006\u0010&\u001a\u00020\u00122\u0006\u0010$\u001a\u00020\u001aH\u0016J\u0014\u0010'\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00120\u00150\u0014H\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006("},
   d2 = {"Lco/uk/getmondo/signup/identity_verification/data/NewIdentityVerificationManager;", "Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationManager;", "storage", "Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationStorage;", "api", "Lco/uk/getmondo/api/IdentityVerificationApi;", "fileUploader", "Lco/uk/getmondo/api/FileUploader;", "deleteFeedItemStorage", "Lco/uk/getmondo/common/DeleteFeedItemStorage;", "source", "Lco/uk/getmondo/api/model/signup/SignupSource;", "(Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationStorage;Lco/uk/getmondo/api/IdentityVerificationApi;Lco/uk/getmondo/api/FileUploader;Lco/uk/getmondo/common/DeleteFeedItemStorage;Lco/uk/getmondo/api/model/signup/SignupSource;)V", "fileWrapper", "Lio/reactivex/Single;", "Lco/uk/getmondo/signup/identity_verification/data/model/FileWrapper;", "kotlin.jvm.PlatformType", "path", "", "identityDocument", "Lio/reactivex/Observable;", "Lcom/memoizrlabs/poweroptional/Optional;", "Lco/uk/getmondo/signup/identity_verification/data/model/IdentityDocument;", "status", "Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;", "submitEvidence", "", "uploadDocuments", "Lio/reactivex/Completable;", "uploadFile", "filePath", "fileType", "Lco/uk/getmondo/api/model/identity_verification/FileType;", "useIdentityDocument", "", "evidence", "usedSystemCamera", "useVideoSelfie", "videoPath", "videoSelfiePath", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class l implements e {
   private final h a;
   private final IdentityVerificationApi b;
   private final ae c;
   private final co.uk.getmondo.common.i d;
   private final SignupSource e;

   public l(h var1, IdentityVerificationApi var2, ae var3, co.uk.getmondo.common.i var4, SignupSource var5) {
      kotlin.d.b.l.b(var1, "storage");
      kotlin.d.b.l.b(var2, "api");
      kotlin.d.b.l.b(var3, "fileUploader");
      kotlin.d.b.l.b(var4, "deleteFeedItemStorage");
      kotlin.d.b.l.b(var5, "source");
      super();
      this.a = var1;
      this.b = var2;
      this.c = var3;
      this.d = var4;
      this.e = var5;
   }

   private final v a(final String var1) {
      return v.c((Callable)(new Callable() {
         public final co.uk.getmondo.signup.identity_verification.a.a.a a() {
            return co.uk.getmondo.signup.identity_verification.a.a.a.a.a(new File(var1));
         }

         // $FF: synthetic method
         public Object call() {
            return this.a();
         }
      }));
   }

   private final v a(String var1, final FileType var2) {
      v var3 = this.a(var1).a((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final v a(co.uk.getmondo.signup.identity_verification.a.a.a var1) {
            kotlin.d.b.l.b(var1, "<name for destructuring parameter 0>");
            final FileInputStream var4 = var1.c();
            String var5 = var1.d();
            final long var2x = var1.e();
            final ContentType var6 = var1.f();
            return l.this.b.requestFileUpload(l.this.e, var2, var5, var6, var2x).a((io.reactivex.c.h)(new io.reactivex.c.h() {
               public final v a(FileUpload var1) {
                  kotlin.d.b.l.b(var1, "<name for destructuring parameter 0>");
                  String var2xx = var1.a();
                  String var3 = var1.b();
                  return l.this.c.a(var3, (InputStream)var4, var2x, var6.a()).a((Object)var2xx);
               }
            }));
         }
      }));
      kotlin.d.b.l.a(var3, "fileWrapper(filePath)\n  …      }\n                }");
      return var3;
   }

   private final io.reactivex.b e() {
      final co.uk.getmondo.signup.identity_verification.a.a.b var1 = this.a.a();
      String var2 = this.a.c();
      io.reactivex.b var3;
      if(var1 != null && var2 != null) {
         if(var1.d() == null) {
            var3 = this.a(var1.a(), FileType.IDENTITY_DOCUMENT).c((io.reactivex.c.h)(new io.reactivex.c.h() {
               public final io.reactivex.b a(String var1x) {
                  kotlin.d.b.l.b(var1x, "fileId");
                  return IdentityVerificationApi.DefaultImpls.registerIdentityDocument$default(l.this.b, l.this.e, var1.b(), var1.c().e(), l.this.a.b(), var1x, (String)null, 32, (Object)null);
               }
            }));
         } else {
            var3 = co.uk.getmondo.common.j.f.a(this.a(var1.a(), FileType.IDENTITY_DOCUMENT), (z)this.a(var1.d(), FileType.IDENTITY_DOCUMENT)).c((io.reactivex.c.h)(new io.reactivex.c.h() {
               public final io.reactivex.b a(kotlin.h var1x) {
                  kotlin.d.b.l.b(var1x, "<name for destructuring parameter 0>");
                  String var2 = (String)var1x.c();
                  String var3 = (String)var1x.d();
                  return l.this.b.registerIdentityDocument(l.this.e, var1.b(), var1.c().e(), l.this.a.b(), var2, var3);
               }
            }));
         }

         var3 = var3.d((io.reactivex.d)this.a(var2, FileType.SELFIE_VIDEO).c((io.reactivex.c.h)(new io.reactivex.c.h() {
            public final io.reactivex.b a(String var1) {
               kotlin.d.b.l.b(var1, "it");
               return l.this.b.registerSelfieVideo(l.this.e, var1, l.this.a.d());
            }
         }))).b((io.reactivex.d)this.b.submit(this.e));
         kotlin.d.b.l.a(var3, "idEvidenceUpload.mergeWi…dThen(api.submit(source))");
      } else {
         var3 = io.reactivex.b.a((Throwable)(new IllegalStateException("Cannot submit evidence because ID evidence or video " + "path is missing | idEvidence: " + var1 + " videoPath " + var2)));
         kotlin.d.b.l.a(var3, "Completable.error(Illega…e videoPath $videoPath\"))");
      }

      return var3;
   }

   public v a() {
      io.reactivex.b var2 = this.e();
      boolean var1;
      if(this.a.b() && this.a.d()) {
         var1 = true;
      } else {
         var1 = false;
      }

      v var3 = var2.a((Object)Boolean.valueOf(var1)).c((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Boolean var1) {
            h var2 = l.this.a;
            String var3 = UUID.randomUUID().toString();
            kotlin.d.b.l.a(var3, "UUID.randomUUID().toString()");
            var2.a(var3);
            l.this.a.h();
            l.this.d.a();
         }
      }));
      kotlin.d.b.l.a(var3, "uploadDocuments()\n      …tFlag()\n                }");
      return var3;
   }

   public void a(co.uk.getmondo.signup.identity_verification.a.a.b var1, boolean var2) {
      kotlin.d.b.l.b(var1, "evidence");
      this.a.a(var1, var2);
   }

   public void a(String var1, boolean var2) {
      kotlin.d.b.l.b(var1, "videoPath");
      this.a.a(var1, var2);
   }

   public n b() {
      return this.a.f();
   }

   public n c() {
      return this.a.g();
   }

   public v d() {
      v var1 = this.b.status(this.e).c((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(IdentityVerification var1) {
            h var2 = l.this.a;
            kotlin.d.b.l.a(var1, "it");
            var2.a(var1);
         }
      }));
      kotlin.d.b.l.a(var1, "api.status(source).doOnS…dentityVerification(it) }");
      return var1;
   }
}
