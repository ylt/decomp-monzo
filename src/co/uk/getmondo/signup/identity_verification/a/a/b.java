package co.uk.getmondo.signup.identity_verification.a.a;

import co.uk.getmondo.api.model.identity_verification.IdentityDocumentType;
import co.uk.getmondo.d.i;
import co.uk.getmondo.signup.identity_verification.a.c;
import co.uk.getmondo.signup.identity_verification.a.j;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B+\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\tJ\t\u0010\u0011\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0012\u001a\u00020\u0005HÆ\u0003J\t\u0010\u0013\u001a\u00020\u0007HÆ\u0003J\u000b\u0010\u0014\u001a\u0004\u0018\u00010\u0003HÆ\u0003J3\u0010\u0015\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00072\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u0003HÆ\u0001J\u0013\u0010\u0016\u001a\u00020\u00172\b\u0010\u0018\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0019\u001a\u00020\u001aHÖ\u0001J\u0016\u0010\u001b\u001a\u00020\u00172\u0006\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u001fJ\t\u0010 \u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0013\u0010\b\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\rR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010¨\u0006!"},
   d2 = {"Lco/uk/getmondo/signup/identity_verification/data/model/IdentityDocument;", "", "primaryPhotoPath", "", "type", "Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;", "country", "Lco/uk/getmondo/model/Country;", "secondaryPhotoPath", "(Ljava/lang/String;Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;Lco/uk/getmondo/model/Country;Ljava/lang/String;)V", "getCountry", "()Lco/uk/getmondo/model/Country;", "getPrimaryPhotoPath", "()Ljava/lang/String;", "getSecondaryPhotoPath", "getType", "()Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "hashCode", "", "isValid", "fileValidator", "Lco/uk/getmondo/signup/identity_verification/data/FileValidator;", "version", "Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationVersion;", "toString", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class b {
   private final String a;
   private final IdentityDocumentType b;
   private final i c;
   private final String d;

   public b(String var1, IdentityDocumentType var2, i var3, String var4) {
      l.b(var1, "primaryPhotoPath");
      l.b(var2, "type");
      l.b(var3, "country");
      super();
      this.a = var1;
      this.b = var2;
      this.c = var3;
      this.d = var4;
   }

   public final String a() {
      return this.a;
   }

   public final boolean a(c var1, j var2) {
      l.b(var1, "fileValidator");
      l.b(var2, "version");
      boolean var3;
      if(l.a(var2, j.a)) {
         var3 = var1.a(this.a);
      } else if(l.a(this.b, IdentityDocumentType.PASSPORT)) {
         var3 = var1.a(this.a);
      } else if(var1.a(this.a) && var1.a(this.d)) {
         var3 = true;
      } else {
         var3 = false;
      }

      return var3;
   }

   public final IdentityDocumentType b() {
      return this.b;
   }

   public final i c() {
      return this.c;
   }

   public final String d() {
      return this.d;
   }

   public boolean equals(Object var1) {
      boolean var2;
      if(this != var1) {
         label32: {
            if(var1 instanceof b) {
               b var3 = (b)var1;
               if(l.a(this.a, var3.a) && l.a(this.b, var3.b) && l.a(this.c, var3.c) && l.a(this.d, var3.d)) {
                  break label32;
               }
            }

            var2 = false;
            return var2;
         }
      }

      var2 = true;
      return var2;
   }

   public int hashCode() {
      int var4 = 0;
      String var5 = this.a;
      int var1;
      if(var5 != null) {
         var1 = var5.hashCode();
      } else {
         var1 = 0;
      }

      IdentityDocumentType var6 = this.b;
      int var2;
      if(var6 != null) {
         var2 = var6.hashCode();
      } else {
         var2 = 0;
      }

      i var7 = this.c;
      int var3;
      if(var7 != null) {
         var3 = var7.hashCode();
      } else {
         var3 = 0;
      }

      var5 = this.d;
      if(var5 != null) {
         var4 = var5.hashCode();
      }

      return (var3 + (var2 + var1 * 31) * 31) * 31 + var4;
   }

   public String toString() {
      return "IdentityDocument(primaryPhotoPath=" + this.a + ", type=" + this.b + ", country=" + this.c + ", secondaryPhotoPath=" + this.d + ")";
   }
}
