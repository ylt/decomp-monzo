package co.uk.getmondo.signup.identity_verification.fallback;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.content.FileProvider;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import co.uk.getmondo.api.model.identity_verification.IdentityDocumentType;
import co.uk.getmondo.api.model.signup.SignupSource;
import co.uk.getmondo.signup.identity_verification.p;
import io.reactivex.n;
import io.reactivex.r;
import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.concurrent.Callable;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u008c\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0015\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 >2\u00020\u00012\u00020\u0002:\u0001>B\u0005¢\u0006\u0002\u0010\u0003J\b\u0010\u0017\u001a\u00020\u0018H\u0002J\"\u0010\u0019\u001a\u00020\u000f2\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001b2\b\u0010\u001d\u001a\u0004\u0018\u00010\u001eH\u0014J\u0012\u0010\u001f\u001a\u00020\u000f2\b\u0010 \u001a\u0004\u0018\u00010!H\u0014J\b\u0010\"\u001a\u00020\u000fH\u0014J\u000e\u0010#\u001a\b\u0012\u0004\u0012\u00020\u00180$H\u0016J\u000e\u0010%\u001a\b\u0012\u0004\u0012\u00020\f0$H\u0016J+\u0010&\u001a\u00020\u000f2\u0006\u0010\u001a\u001a\u00020\u001b2\f\u0010'\u001a\b\u0012\u0004\u0012\u00020)0(2\u0006\u0010*\u001a\u00020+H\u0016¢\u0006\u0002\u0010,J\u000e\u0010-\u001a\b\u0012\u0004\u0012\u00020.0$H\u0016J\b\u0010/\u001a\u00020\u000fH\u0016J\b\u00100\u001a\u00020\u000fH\u0016J\b\u00101\u001a\u00020\u000fH\u0016J\u001e\u00102\u001a\b\u0012\u0004\u0012\u0002030$2\u0006\u00104\u001a\u0002052\u0006\u00106\u001a\u00020\u0018H\u0016J\u0018\u00107\u001a\u00020\u000f2\u0006\u00108\u001a\u00020)2\u0006\u00109\u001a\u000203H\u0016J\u0018\u0010:\u001a\u00020\u000f2\u0006\u0010;\u001a\u00020)2\u0006\u0010<\u001a\u00020=H\u0016R\u001e\u0010\u0004\u001a\u00020\u00058\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\tR2\u0010\n\u001a&\u0012\f\u0012\n \r*\u0004\u0018\u00010\f0\f \r*\u0012\u0012\f\u0012\n \r*\u0004\u0018\u00010\f0\f\u0018\u00010\u000b0\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R2\u0010\u000e\u001a&\u0012\f\u0012\n \r*\u0004\u0018\u00010\u000f0\u000f \r*\u0012\u0012\f\u0012\n \r*\u0004\u0018\u00010\u000f0\u000f\u0018\u00010\u000b0\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\fX\u0082.¢\u0006\u0002\n\u0000R\u001e\u0010\u0011\u001a\u00020\u00128\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\u0016¨\u0006?"},
   d2 = {"Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity;", "Lco/uk/getmondo/common/activities/BaseActivity;", "Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackPresenter$View;", "()V", "fileGenerator", "Lco/uk/getmondo/signup/identity_verification/data/FileGenerator;", "getFileGenerator", "()Lco/uk/getmondo/signup/identity_verification/data/FileGenerator;", "setFileGenerator", "(Lco/uk/getmondo/signup/identity_verification/data/FileGenerator;)V", "imageChangedRelay", "Lcom/jakewharton/rxrelay2/PublishRelay;", "Ljava/io/File;", "kotlin.jvm.PlatformType", "permissionGrantedRelay", "", "photoFile", "presenter", "Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackPresenter;", "getPresenter", "()Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackPresenter;", "setPresenter", "(Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackPresenter;)V", "hasCameraPermission", "", "onActivityResult", "requestCode", "", "resultCode", "data", "Landroid/content/Intent;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onDestroy", "onMainActionClicked", "Lio/reactivex/Observable;", "onPictureChanged", "onRequestPermissionsResult", "permissions", "", "", "grantResults", "", "(I[Ljava/lang/String;[I)V", "onRetakeClicked", "", "openCamera", "requestCameraPermission", "resetActionButtons", "saveInFile", "Landroid/net/Uri;", "bitmap", "Landroid/graphics/Bitmap;", "isPrimaryFile", "showConfirmation", "title", "uri", "showInstructions", "text", "preview", "Landroid/graphics/drawable/Drawable;", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class DocumentFallbackActivity extends co.uk.getmondo.common.activities.b implements b.a {
   public static final DocumentFallbackActivity.a c = new DocumentFallbackActivity.a((kotlin.d.b.i)null);
   public b a;
   public co.uk.getmondo.signup.identity_verification.a.a b;
   private final com.b.b.c e = com.b.b.c.a();
   private final com.b.b.c f = com.b.b.c.a();
   private File g;
   private HashMap h;

   private final boolean h() {
      boolean var1;
      if(android.support.v4.content.a.b((Context)this, "android.permission.CAMERA") == 0) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public View a(int var1) {
      if(this.h == null) {
         this.h = new HashMap();
      }

      View var3 = (View)this.h.get(Integer.valueOf(var1));
      View var2 = var3;
      if(var3 == null) {
         var2 = this.findViewById(var1);
         this.h.put(Integer.valueOf(var1), var2);
      }

      return var2;
   }

   public final co.uk.getmondo.signup.identity_verification.a.a a() {
      co.uk.getmondo.signup.identity_verification.a.a var1 = this.b;
      if(var1 == null) {
         l.b("fileGenerator");
      }

      return var1;
   }

   public n a(final Bitmap var1, final boolean var2) {
      l.b(var1, "bitmap");
      n var3 = n.fromCallable((Callable)(new Callable() {
         public final Uri a() {
            File var2x;
            if(var2) {
               var2x = DocumentFallbackActivity.this.a().b();
            } else {
               var2x = DocumentFallbackActivity.this.a().c();
            }

            Closeable var3 = (Closeable)(new FileOutputStream(var2x));

            label123: {
               Exception var17;
               try {
                  FileOutputStream var4 = (FileOutputStream)var3;
                  var1.compress(CompressFormat.JPEG, 90, (OutputStream)var4);
                  break label123;
               } catch (Exception var15) {
                  var17 = var15;
               } finally {
                  ;
               }

               boolean var1x = true;

               try {
                  try {
                     var3.close();
                  } catch (Exception var13) {
                     ;
                  }

                  throw (Throwable)var17;
               } finally {
                  if(!var1x) {
                     var3.close();
                  }

                  throw var17;
               }
            }

            var3.close();
            return Uri.fromFile(var2x);
         }

         // $FF: synthetic method
         public Object call() {
            return this.a();
         }
      }));
      l.a(var3, "Observable.fromCallable ….fromFile(file)\n        }");
      return var3;
   }

   public void a(String var1, Drawable var2) {
      l.b(var1, "text");
      l.b(var2, "preview");
      ((TextView)this.a(co.uk.getmondo.c.a.fallbackDocumentBodyTextView)).setText((CharSequence)var1);
      ((ImageView)this.a(co.uk.getmondo.c.a.fallbackDocumentPreviewImageView)).setImageDrawable(var2);
   }

   public void a(String var1, Uri var2) {
      l.b(var1, "title");
      l.b(var2, "uri");
      ((TextView)this.a(co.uk.getmondo.c.a.fallbackDocumentBodyTextView)).setText((CharSequence)var1);
      ((Button)this.a(co.uk.getmondo.c.a.mainActionButton)).setText((CharSequence)this.getString(2131362351));
      ((Button)this.a(co.uk.getmondo.c.a.retakePhotoButton)).setVisibility(0);
      File var3 = new File(var2.getPath());
      com.bumptech.glide.g.a((android.support.v4.app.j)this).a(var3).a((com.bumptech.glide.load.c)(new com.bumptech.glide.h.b(String.valueOf(var3.lastModified())))).a((ImageView)this.a(co.uk.getmondo.c.a.fallbackDocumentPreviewImageView));
   }

   public n b() {
      n var1 = n.merge((r)this.e, (r)com.b.a.c.c.a((Button)this.a(co.uk.getmondo.c.a.mainActionButton))).map((io.reactivex.c.h)(new io.reactivex.c.h() {
         // $FF: synthetic method
         public Object a(Object var1) {
            return Boolean.valueOf(this.b(var1));
         }

         public final boolean b(Object var1) {
            l.b(var1, "it");
            return DocumentFallbackActivity.this.h();
         }
      }));
      l.a(var1, "Observable.merge(permiss…{ hasCameraPermission() }");
      return var1;
   }

   public n c() {
      n var1 = com.b.a.c.c.a((Button)this.a(co.uk.getmondo.c.a.retakePhotoButton));
      l.a(var1, "RxView.clicks(retakePhotoButton)");
      return var1;
   }

   public n d() {
      com.b.b.c var1 = this.f;
      l.a(var1, "imageChangedRelay");
      return (n)var1;
   }

   public void e() {
      android.support.v4.app.a.a((Activity)this, (String[])((Object[])(new String[]{"android.permission.CAMERA"})), 1000);
   }

   public void f() {
      Intent var3 = new Intent("android.media.action.IMAGE_CAPTURE");
      if(var3.resolveActivity(this.getPackageManager()) != null) {
         Context var4 = (Context)this;
         String var1 = this.getString(2131362187);
         File var2 = this.g;
         if(var2 == null) {
            l.b("photoFile");
         }

         var3.putExtra("output", (Parcelable)FileProvider.a(var4, var1, var2));
         this.startActivityForResult(var3, 1001);
      } else {
         d.a.a.a((Throwable)(new RuntimeException("Failed to find default camera to open")));
      }

   }

   public void g() {
      ((Button)this.a(co.uk.getmondo.c.a.mainActionButton)).setText((CharSequence)this.getString(2131362347));
      ((Button)this.a(co.uk.getmondo.c.a.retakePhotoButton)).setVisibility(8);
   }

   protected void onActivityResult(int var1, int var2, Intent var3) {
      if(var1 == 1001 && var2 == -1) {
         com.b.b.c var4 = this.f;
         File var5 = this.g;
         if(var5 == null) {
            l.b("photoFile");
         }

         var4.a((Object)var5);
      } else {
         super.onActivityResult(var1, var2, var3);
      }

   }

   protected void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.setContentView(2131034162);
      co.uk.getmondo.d.i var4 = co.uk.getmondo.d.i.Companion.a(this.getIntent().getStringExtra("KEY_COUNTRY_CODE"));
      if(var4 == null) {
         var4 = co.uk.getmondo.d.i.UNITED_KINGDOM;
      }

      IdentityDocumentType var2 = IdentityDocumentType.PASSPORT;
      if(this.getIntent().hasExtra("KEY_ID_DOCUMENT_TYPE")) {
         Serializable var5 = this.getIntent().getSerializableExtra("KEY_ID_DOCUMENT_TYPE");
         if(var5 == null) {
            throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.api.model.identity_verification.IdentityDocumentType");
         }

         var2 = (IdentityDocumentType)var5;
      }

      Serializable var3 = this.getIntent().getSerializableExtra("KEY_SIGNUP_SOURCE");
      if(var3 == null) {
         throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.api.model.signup.SignupSource");
      } else {
         SignupSource var9 = (SignupSource)var3;
         this.l().a(new p(co.uk.getmondo.signup.identity_verification.a.j.b, var9)).a(new co.uk.getmondo.signup.identity_verification.id_picture.f(var2, var4, (String)null)).a(this);
         b var6 = this.a;
         if(var6 == null) {
            l.b("presenter");
         }

         var6.a((b.a)this);
         co.uk.getmondo.signup.identity_verification.a.a var7 = this.b;
         if(var7 == null) {
            l.b("fileGenerator");
         }

         File var8 = var7.e();
         l.a(var8, "fileGenerator.tempFallbackIdentityDocFile");
         this.g = var8;
      }
   }

   protected void onDestroy() {
      File var1 = this.g;
      if(var1 == null) {
         l.b("photoFile");
      }

      if(var1.exists()) {
         var1 = this.g;
         if(var1 == null) {
            l.b("photoFile");
         }

         var1.delete();
      }

      b var2 = this.a;
      if(var2 == null) {
         l.b("presenter");
      }

      var2.b();
      super.onDestroy();
   }

   public void onRequestPermissionsResult(int var1, String[] var2, int[] var3) {
      boolean var5 = true;
      l.b(var2, "permissions");
      l.b(var3, "grantResults");
      if(var1 == 1000) {
         boolean var4;
         if(var3.length == 0) {
            var4 = true;
         } else {
            var4 = false;
         }

         if(!var4) {
            var4 = var5;
         } else {
            var4 = false;
         }

         if(var4 && var3[0] == 0) {
            this.e.a((Object)kotlin.n.a);
            return;
         }
      }

      super.onRequestPermissionsResult(var1, var2, var3);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J(\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0007R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0007X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000¨\u0006\u0014"},
      d2 = {"Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackActivity$Companion;", "", "()V", "KEY_COUNTRY_CODE", "", "KEY_ID_DOCUMENT_TYPE", "KEY_REQUEST_CAMERA_PERMISSION", "", "KEY_REQUEST_TAKE_PHOTO", "KEY_SIGNUP_SOURCE", "buildIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "identityDocumentType", "Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;", "country", "Lco/uk/getmondo/model/Country;", "signupSource", "Lco/uk/getmondo/api/model/signup/SignupSource;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var1) {
         this();
      }

      public final Intent a(Context var1, IdentityDocumentType var2, co.uk.getmondo.d.i var3, SignupSource var4) {
         l.b(var1, "context");
         l.b(var2, "identityDocumentType");
         l.b(var3, "country");
         l.b(var4, "signupSource");
         Intent var5 = (new Intent(var1, DocumentFallbackActivity.class)).putExtra("KEY_ID_DOCUMENT_TYPE", (Serializable)var2).putExtra("KEY_COUNTRY_CODE", var3.e()).putExtra("KEY_SIGNUP_SOURCE", (Serializable)var4);
         l.a(var5, "Intent(context, Document…NUP_SOURCE, signupSource)");
         return var5;
      }
   }
}
