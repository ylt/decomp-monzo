package co.uk.getmondo.signup.identity_verification.fallback;

import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.signup.identity_verification.video.ab;
import io.reactivex.n;
import java.io.File;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u000fB\u001f\b\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b¢\u0006\u0002\u0010\tJ\u0010\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u0002H\u0016R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\n\u001a\u0004\u0018\u00010\u000bX\u0082\u000e¢\u0006\u0002\n\u0000¨\u0006\u0010"},
   d2 = {"Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackPresenter$View;", "verificationManager", "Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationManager;", "stringProvider", "Lco/uk/getmondo/signup/identity_verification/video/VideoStringProvider;", "analyticsService", "Lco/uk/getmondo/common/AnalyticsService;", "(Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationManager;Lco/uk/getmondo/signup/identity_verification/video/VideoStringProvider;Lco/uk/getmondo/common/AnalyticsService;)V", "videoPath", "", "register", "", "view", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class h extends co.uk.getmondo.common.ui.b {
   private String c;
   private final co.uk.getmondo.signup.identity_verification.a.e d;
   private final ab e;
   private final co.uk.getmondo.common.a f;

   public h(co.uk.getmondo.signup.identity_verification.a.e var1, ab var2, co.uk.getmondo.common.a var3) {
      l.b(var1, "verificationManager");
      l.b(var2, "stringProvider");
      l.b(var3, "analyticsService");
      super();
      this.d = var1;
      this.e = var2;
      this.f = var3;
   }

   public void a(final h.a var1) {
      l.b(var1, "view");
      super.a((co.uk.getmondo.common.ui.f)var1);
      this.f.a(Impression.Companion.f(true));
      var1.a(this.e.b(), this.e.a());
      io.reactivex.b.a var4 = this.b;
      n var5 = var1.b();
      io.reactivex.c.g var6 = (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Boolean var1x) {
            l.a(var1x, "hasCameraPermission");
            if(var1x.booleanValue()) {
               String var2 = h.this.c;
               if(var2 != null) {
                  h.this.d.a(var2, true);
                  var1.a(var2);
                  var1.finish();
               } else {
                  var1.g();
               }
            } else {
               var1.f();
            }

         }
      });
      kotlin.d.a.b var3 = (kotlin.d.a.b)null.a;
      Object var2 = var3;
      if(var3 != null) {
         var2 = new i(var3);
      }

      io.reactivex.b.b var9 = var5.subscribe(var6, (io.reactivex.c.g)var2);
      l.a(var9, "view.onMainActionClicked…            }, Timber::e)");
      this.b = co.uk.getmondo.common.j.f.a(var4, var9);
      io.reactivex.b.a var10 = this.b;
      io.reactivex.b.b var11 = var1.c().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(File var1x) {
            if(var1x.length() > (long)20971520) {
               var1.i();
            } else {
               h.this.c = var1x.getPath();
               h.a var2 = var1;
               l.a(var1x, "file");
               var2.a(var1x, h.this.e.c(), h.this.e.a());
            }

         }
      }));
      l.a(var11, "view.onVideoTaken()\n    …      }\n                }");
      this.b = co.uk.getmondo.common.j.f.a(var10, var11);
      io.reactivex.b.a var12 = this.b;
      var9 = var1.e().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Boolean var1x) {
            l.a(var1x, "isMuted");
            if(var1x.booleanValue()) {
               var1.k();
            } else {
               var1.j();
            }

         }
      }));
      l.a(var9, "view.onMuteClicked()\n   …      }\n                }");
      this.b = co.uk.getmondo.common.j.f.a(var12, var9);
      var12 = this.b;
      n var14 = var1.d();
      io.reactivex.c.g var15 = (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Object var1x) {
            h.this.c = (String)null;
            var1.h();
            var1.a(h.this.e.b(), h.this.e.a());
         }
      });
      kotlin.d.a.b var13 = (kotlin.d.a.b)null.a;
      Object var7 = var13;
      if(var13 != null) {
         var7 = new i(var13);
      }

      io.reactivex.b.b var8 = var14.subscribe(var15, (io.reactivex.c.g)var7);
      l.a(var8, "view.onRetakeClicked()\n …            }, Timber::e)");
      this.b = co.uk.getmondo.common.j.f.a(var12, var8);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000b\bf\u0018\u00002\u00020\u00012\u00020\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H&J\b\u0010\u0007\u001a\u00020\u0004H&J\b\u0010\b\u001a\u00020\u0004H&J\u000e\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\nH&J\u000e\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000b0\nH&J\u000e\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000e0\nH&J\u000e\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00100\nH&J\b\u0010\u0011\u001a\u00020\u0004H&J\b\u0010\u0012\u001a\u00020\u0004H&J \u0010\u0013\u001a\u00020\u00042\u0006\u0010\u0014\u001a\u00020\u00102\u0006\u0010\u0015\u001a\u00020\u00062\u0006\u0010\u0016\u001a\u00020\u0006H&J\u0018\u0010\u0017\u001a\u00020\u00042\u0006\u0010\u0015\u001a\u00020\u00062\u0006\u0010\u0016\u001a\u00020\u0006H&J\b\u0010\u0018\u001a\u00020\u0004H&J\b\u0010\u0019\u001a\u00020\u0004H&J\b\u0010\u001a\u001a\u00020\u0004H&¨\u0006\u001b"},
      d2 = {"Lco/uk/getmondo/signup/identity_verification/fallback/VideoFallbackPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "deleteOldVideos", "", "usedSelfiePath", "", "finish", "mute", "onMainActionClicked", "Lio/reactivex/Observable;", "", "onMuteClicked", "onRetakeClicked", "", "onVideoTaken", "Ljava/io/File;", "requestCameraPermission", "resetToExample", "showConfirmation", "videoFile", "sentence", "textToRead", "showInitialInstructions", "showVideoTooLongError", "takeVideo", "unmute", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      void a(File var1, String var2, String var3);

      void a(String var1);

      void a(String var1, String var2);

      n b();

      n c();

      n d();

      n e();

      void f();

      void finish();

      void g();

      void h();

      void i();

      void j();

      void k();
   }
}
