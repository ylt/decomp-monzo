package co.uk.getmondo.signup.identity_verification.fallback;

import android.content.Context;

public final class f implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;

   static {
      boolean var0;
      if(!f.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   public f(javax.a.a var1) {
      if(!a && var1 == null) {
         throw new AssertionError();
      } else {
         this.b = var1;
      }
   }

   public static b.a.b a(javax.a.a var0) {
      return new f(var0);
   }

   public e a() {
      return new e((Context)this.b.b());
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
