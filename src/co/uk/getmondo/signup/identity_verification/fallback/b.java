package co.uk.getmondo.signup.identity_verification.fallback;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import co.uk.getmondo.api.model.identity_verification.IdentityDocumentType;
import co.uk.getmondo.api.model.tracking.Impression;
import io.reactivex.n;
import io.reactivex.u;
import java.io.File;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u001dBK\b\u0007\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\f\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011¢\u0006\u0002\u0010\u0012J\b\u0010\u0018\u001a\u00020\u0017H\u0002J\u0010\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u0002H\u0016J\u0010\u0010\u001c\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u0002H\u0002R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0013\u001a\u0004\u0018\u00010\u0014X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0015\u001a\u0004\u0018\u00010\u0014X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u001e"},
   d2 = {"Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackPresenter$View;", "computationScheduler", "Lio/reactivex/Scheduler;", "uiScheduler", "country", "Lco/uk/getmondo/model/Country;", "documentType", "Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;", "verificationManager", "Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationManager;", "imageResizer", "Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentImageResizer;", "stringProvider", "Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackStringProvider;", "analyticsService", "Lco/uk/getmondo/common/AnalyticsService;", "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/model/Country;Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationManager;Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentImageResizer;Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackStringProvider;Lco/uk/getmondo/common/AnalyticsService;)V", "backPhotoPath", "", "frontPhotoPath", "shownBackInstructions", "", "isFront", "register", "", "view", "showInstructions", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class b extends co.uk.getmondo.common.ui.b {
   private String c;
   private String d;
   private boolean e;
   private final u f;
   private final u g;
   private final co.uk.getmondo.d.i h;
   private final IdentityDocumentType i;
   private final co.uk.getmondo.signup.identity_verification.a.e j;
   private final co.uk.getmondo.signup.identity_verification.id_picture.u k;
   private final e l;
   private final co.uk.getmondo.common.a m;

   public b(u var1, u var2, co.uk.getmondo.d.i var3, IdentityDocumentType var4, co.uk.getmondo.signup.identity_verification.a.e var5, co.uk.getmondo.signup.identity_verification.id_picture.u var6, e var7, co.uk.getmondo.common.a var8) {
      l.b(var1, "computationScheduler");
      l.b(var2, "uiScheduler");
      l.b(var3, "country");
      l.b(var4, "documentType");
      l.b(var5, "verificationManager");
      l.b(var6, "imageResizer");
      l.b(var7, "stringProvider");
      l.b(var8, "analyticsService");
      super();
      this.f = var1;
      this.g = var2;
      this.h = var3;
      this.i = var4;
      this.j = var5;
      this.k = var6;
      this.l = var7;
      this.m = var8;
   }

   private final boolean a() {
      boolean var1;
      if(this.i.b() && this.c == null) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   private final void b(b.a var1) {
      kotlin.h var2 = this.l.a(this.i, this.a());
      var1.a((String)var2.c(), (Drawable)var2.d());
   }

   public void a(final b.a var1) {
      l.b(var1, "view");
      super.a((co.uk.getmondo.common.ui.f)var1);
      this.m.a(Impression.Companion.a(this.i.a(), true));
      this.b(var1);
      io.reactivex.b.a var4 = this.b;
      n var6 = var1.b();
      io.reactivex.c.g var5 = (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Boolean var1x) {
            boolean var3 = false;
            l.a(var1x, "hasCameraPermission");
            if(var1x.booleanValue()) {
               boolean var2;
               if(b.this.c != null) {
                  var2 = true;
               } else {
                  var2 = false;
               }

               if(b.this.d != null) {
                  var3 = true;
               }

               boolean var4 = b.this.i.b();
               if(var2 && var3 || var2 && !var4) {
                  String var5 = b.this.c;
                  if(var5 == null) {
                     l.a();
                  }

                  co.uk.getmondo.signup.identity_verification.a.a.b var6 = new co.uk.getmondo.signup.identity_verification.a.a.b(var5, b.this.i, b.this.h, b.this.d);
                  b.this.j.a(var6, true);
                  var1.finish();
               } else if(var2 && var4 && !var3 && !b.this.e) {
                  b.this.b(var1);
                  var1.g();
                  b.this.e = true;
               } else {
                  var1.f();
               }
            } else {
               var1.e();
            }

         }
      });
      kotlin.d.a.b var3 = (kotlin.d.a.b)null.a;
      Object var2 = var3;
      if(var3 != null) {
         var2 = new c(var3);
      }

      io.reactivex.b.b var9 = var6.subscribe(var5, (io.reactivex.c.g)var2);
      l.a(var9, "view.onMainActionClicked…            }, Timber::e)");
      this.b = co.uk.getmondo.common.j.f.a(var4, var9);
      var4 = this.b;
      var6 = var1.d().concatMap((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final n a(File var1) {
            l.b(var1, "file");
            return b.this.k.a(var1).subscribeOn(b.this.f);
         }
      })).concatMap((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final n a(Bitmap var1x) {
            l.b(var1x, "bitmap");
            return var1.a(var1x, b.this.a()).subscribeOn(b.this.f);
         }
      })).observeOn(this.g);
      var5 = (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Uri var1x) {
            String var4;
            b.a var5;
            if(b.this.c == null) {
               b.this.c = var1x.getPath();
               if(!b.this.i.b()) {
                  b.a var2 = var1;
                  String var3 = b.this.l.a();
                  l.a(var1x, "uri");
                  var2.a(var3, var1x);
               } else {
                  var5 = var1;
                  var4 = b.this.l.b(b.this.i, true);
                  l.a(var1x, "uri");
                  var5.a(var4, var1x);
               }
            } else {
               b.this.d = var1x.getPath();
               var5 = var1;
               var4 = b.this.l.b(b.this.i, false);
               l.a(var1x, "uri");
               var5.a(var4, var1x);
            }

         }
      });
      var3 = (kotlin.d.a.b)null.a;
      var2 = var3;
      if(var3 != null) {
         var2 = new c(var3);
      }

      var9 = var6.subscribe(var5, (io.reactivex.c.g)var2);
      l.a(var9, "view.onPictureChanged()\n…            }, Timber::e)");
      this.b = co.uk.getmondo.common.j.f.a(var4, var9);
      io.reactivex.b.a var10 = this.b;
      n var11 = var1.c();
      var5 = (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Object var1x) {
            boolean var3 = true;
            boolean var2;
            if(b.this.c != null) {
               var2 = true;
            } else {
               var2 = false;
            }

            if(b.this.d == null) {
               var3 = false;
            }

            if(var2 && !var3) {
               b.this.c = (String)null;
            } else if(var2 && var3) {
               b.this.d = (String)null;
            }

            b.this.e = false;
            var1.g();
            b.this.b(var1);
         }
      });
      kotlin.d.a.b var12 = (kotlin.d.a.b)null.a;
      Object var7 = var12;
      if(var12 != null) {
         var7 = new c(var12);
      }

      io.reactivex.b.b var8 = var11.subscribe(var5, (io.reactivex.c.g)var7);
      l.a(var8, "view.onRetakeClicked()\n …            }, Timber::e)");
      this.b = co.uk.getmondo.common.j.f.a(var10, var8);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u00012\u00020\u0002J\b\u0010\u0003\u001a\u00020\u0004H&J\u000e\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006H&J\u000e\u0010\b\u001a\b\u0012\u0004\u0012\u00020\t0\u0006H&J\u000e\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u000b0\u0006H&J\b\u0010\f\u001a\u00020\u0004H&J\b\u0010\r\u001a\u00020\u0004H&J\b\u0010\u000e\u001a\u00020\u0004H&J\u001e\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00100\u00062\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0007H&J\u0018\u0010\u0014\u001a\u00020\u00042\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0010H&J\u0018\u0010\u0018\u001a\u00020\u00042\u0006\u0010\u0019\u001a\u00020\u00162\u0006\u0010\u001a\u001a\u00020\u001bH&¨\u0006\u001c"},
      d2 = {"Lco/uk/getmondo/signup/identity_verification/fallback/DocumentFallbackPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "finish", "", "onMainActionClicked", "Lio/reactivex/Observable;", "", "onPictureChanged", "Ljava/io/File;", "onRetakeClicked", "", "openCamera", "requestCameraPermission", "resetActionButtons", "saveInFile", "Landroid/net/Uri;", "bitmap", "Landroid/graphics/Bitmap;", "isPrimaryFile", "showConfirmation", "title", "", "uri", "showInstructions", "text", "preview", "Landroid/graphics/drawable/Drawable;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      n a(Bitmap var1, boolean var2);

      void a(String var1, Drawable var2);

      void a(String var1, Uri var2);

      n b();

      n c();

      n d();

      void e();

      void f();

      void finish();

      void g();
   }
}
