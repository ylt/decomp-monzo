package co.uk.getmondo.signup.identity_verification;

import co.uk.getmondo.api.model.signup.SignupSource;

public final class s implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final p b;

   static {
      boolean var0;
      if(!s.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   public s(p var1) {
      if(!a && var1 == null) {
         throw new AssertionError();
      } else {
         this.b = var1;
      }
   }

   public static b.a.b a(p var0) {
      return new s(var0);
   }

   public SignupSource a() {
      return (SignupSource)b.a.d.a(this.b.b(), "Cannot return null from a non-@Nullable @Provides method");
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
