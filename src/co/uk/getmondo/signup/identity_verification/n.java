package co.uk.getmondo.signup.identity_verification;

public final class n implements b.a {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;
   private final javax.a.a c;

   static {
      boolean var0;
      if(!n.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   public n(javax.a.a var1, javax.a.a var2) {
      if(!a && var1 == null) {
         throw new AssertionError();
      } else {
         this.b = var1;
         if(!a && var2 == null) {
            throw new AssertionError();
         } else {
            this.c = var2;
         }
      }
   }

   public static b.a a(javax.a.a var0, javax.a.a var1) {
      return new n(var0, var1);
   }

   public void a(IdentityVerificationActivity var1) {
      if(var1 == null) {
         throw new NullPointerException("Cannot inject members into a null reference");
      } else {
         var1.e = (co.uk.getmondo.common.q)this.b.b();
         var1.b = (v)this.c.b();
      }
   }
}
