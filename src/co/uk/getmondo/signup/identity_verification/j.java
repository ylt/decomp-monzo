package co.uk.getmondo.signup.identity_verification;

import co.uk.getmondo.api.model.tracking.Impression;

public class j {
   private final Impression.KycFrom a;
   private final String b;
   private final boolean c;

   j(Impression.KycFrom var1, String var2, boolean var3) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
   }

   Impression.KycFrom a() {
      return this.a;
   }

   String b() {
      return this.b;
   }

   boolean c() {
      return this.c;
   }
}
