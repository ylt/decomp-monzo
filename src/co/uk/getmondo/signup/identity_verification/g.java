package co.uk.getmondo.signup.identity_verification;

import co.uk.getmondo.api.model.identity_verification.IdentityDocumentType;
import co.uk.getmondo.api.model.tracking.Impression;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0003\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u001bB]\b\u0007\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\f\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\b\u0010\u0012\u001a\u0004\u0018\u00010\u0013\u0012\u0006\u0010\u0014\u001a\u00020\u0015¢\u0006\u0002\u0010\u0016J\u0010\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u0002H\u0016R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0015X\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0012\u001a\u0004\u0018\u00010\u0013X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u001c"},
   d2 = {"Lco/uk/getmondo/signup/identity_verification/IdentityDocumentsPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/signup/identity_verification/IdentityDocumentsPresenter$View;", "ioScheduler", "Lio/reactivex/Scheduler;", "uiScheduler", "apiErrorHandler", "Lco/uk/getmondo/common/errors/ApiErrorHandler;", "verificationManager", "Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationManager;", "fileValidator", "Lco/uk/getmondo/signup/identity_verification/data/FileValidator;", "analyticsService", "Lco/uk/getmondo/common/AnalyticsService;", "version", "Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationVersion;", "from", "Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;", "rejectionReason", "", "allowSystemCamera", "", "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationManager;Lco/uk/getmondo/signup/identity_verification/data/FileValidator;Lco/uk/getmondo/common/AnalyticsService;Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationVersion;Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;Ljava/lang/String;Z)V", "pageViewTracked", "register", "", "view", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class g extends co.uk.getmondo.common.ui.b {
   private boolean c;
   private final io.reactivex.u d;
   private final io.reactivex.u e;
   private final co.uk.getmondo.common.e.a f;
   private final co.uk.getmondo.signup.identity_verification.a.e g;
   private final co.uk.getmondo.signup.identity_verification.a.c h;
   private final co.uk.getmondo.common.a i;
   private final co.uk.getmondo.signup.identity_verification.a.j j;
   private final Impression.KycFrom k;
   private final String l;
   private boolean m;

   public g(io.reactivex.u var1, io.reactivex.u var2, co.uk.getmondo.common.e.a var3, co.uk.getmondo.signup.identity_verification.a.e var4, co.uk.getmondo.signup.identity_verification.a.c var5, co.uk.getmondo.common.a var6, co.uk.getmondo.signup.identity_verification.a.j var7, Impression.KycFrom var8, String var9, boolean var10) {
      kotlin.d.b.l.b(var1, "ioScheduler");
      kotlin.d.b.l.b(var2, "uiScheduler");
      kotlin.d.b.l.b(var3, "apiErrorHandler");
      kotlin.d.b.l.b(var4, "verificationManager");
      kotlin.d.b.l.b(var5, "fileValidator");
      kotlin.d.b.l.b(var6, "analyticsService");
      kotlin.d.b.l.b(var7, "version");
      kotlin.d.b.l.b(var8, "from");
      super();
      this.d = var1;
      this.e = var2;
      this.f = var3;
      this.g = var4;
      this.h = var5;
      this.i = var6;
      this.j = var7;
      this.k = var8;
      this.l = var9;
      this.m = var10;
   }

   public void a(final g.a var1) {
      final boolean var3 = false;
      kotlin.d.b.l.b(var1, "view");
      super.a((co.uk.getmondo.common.ui.f)var1);
      CharSequence var4 = (CharSequence)this.l;
      boolean var2;
      if(var4 != null && !kotlin.h.j.a(var4)) {
         var2 = false;
      } else {
         var2 = true;
      }

      if(!var2) {
         var3 = true;
      }

      if(var3) {
         String var7 = this.l;
         if(var7 == null) {
            kotlin.d.b.l.a();
         }

         var1.a(var7);
      }

      io.reactivex.b.a var5;
      io.reactivex.b.b var8;
      io.reactivex.b.a var9;
      io.reactivex.b.b var10;
      if(this.j == co.uk.getmondo.signup.identity_verification.a.j.b) {
         var5 = this.b;
         var8 = var1.a().flatMapMaybe((io.reactivex.c.h)(new io.reactivex.c.h() {
            public final io.reactivex.h a(kotlin.n var1x) {
               kotlin.d.b.l.b(var1x, "it");
               return var1.g();
            }
         })).flatMapMaybe((io.reactivex.c.h)(new io.reactivex.c.h() {
            public final io.reactivex.h a(Boolean var1x) {
               kotlin.d.b.l.b(var1x, "hasPassport");
               io.reactivex.h var2;
               if(var1x.booleanValue()) {
                  var2 = io.reactivex.h.a((Object)IdentityDocumentType.PASSPORT);
               } else {
                  var2 = var1.h();
               }

               return var2;
            }
         })).flatMapMaybe((io.reactivex.c.h)(new io.reactivex.c.h() {
            public final io.reactivex.h a(final IdentityDocumentType var1x) {
               kotlin.d.b.l.b(var1x, "documentType");
               io.reactivex.h var2;
               if(var1x == IdentityDocumentType.NATIONAL_ID) {
                  var2 = io.reactivex.h.a((Object)(new kotlin.h(var1x, Boolean.valueOf(false))));
               } else {
                  var2 = var1.a(var1x).c((io.reactivex.c.h)(new io.reactivex.c.h() {
                     public final kotlin.h a(Boolean var1xx) {
                        kotlin.d.b.l.b(var1xx, "isUk");
                        return new kotlin.h(var1x, var1xx);
                     }
                  }));
               }

               return var2;
            }
         })).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
            public final void a(kotlin.h var1x) {
               IdentityDocumentType var2 = (IdentityDocumentType)var1x.c();
               g.a var4;
               if(((Boolean)var1x.d()).booleanValue()) {
                  co.uk.getmondo.signup.identity_verification.a.j var3;
                  if(g.this.m) {
                     var4 = var1;
                     var3 = g.this.j;
                     kotlin.d.b.l.a(var2, "documentType");
                     var4.b(var3, var2, co.uk.getmondo.d.i.UNITED_KINGDOM);
                  } else {
                     var4 = var1;
                     var3 = g.this.j;
                     kotlin.d.b.l.a(var2, "documentType");
                     var4.a(var3, var2, co.uk.getmondo.d.i.UNITED_KINGDOM);
                  }
               } else {
                  var4 = var1;
                  kotlin.d.b.l.a(var2, "documentType");
                  var4.a(var2, g.this.m);
               }

            }
         }), (io.reactivex.c.g)null.a);
         kotlin.d.b.l.a(var8, "view.onTakePhotoClicked(…     }, { Timber.e(it) })");
         this.b = co.uk.getmondo.common.j.f.a(var5, var8);
         var9 = this.b;
         var10 = var1.e().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
            public final void a(kotlin.h var1x) {
               co.uk.getmondo.d.i var2 = (co.uk.getmondo.d.i)var1x.c();
               IdentityDocumentType var3 = (IdentityDocumentType)var1x.d();
               if(g.this.m) {
                  var1.b(g.this.j, var3, var2);
               } else {
                  var1.a(g.this.j, var3, var2);
               }

            }
         }));
         kotlin.d.b.l.a(var10, "view.onDocumentCountrySe…  }\n                    }");
         this.b = co.uk.getmondo.common.j.f.a(var9, var10);
      } else {
         var5 = this.b;
         var8 = var1.a().flatMapMaybe((io.reactivex.c.h)(new io.reactivex.c.h() {
            public final io.reactivex.h a(kotlin.n var1x) {
               kotlin.d.b.l.b(var1x, "it");
               return var1.f();
            }
         })).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
            public final void a(IdentityDocumentType var1x) {
               g.a var3 = var1;
               co.uk.getmondo.signup.identity_verification.a.j var2 = g.this.j;
               kotlin.d.b.l.a(var1x, "idDocumentType");
               var3.a(var2, var1x, co.uk.getmondo.d.i.UNITED_KINGDOM);
            }
         }), (io.reactivex.c.g)null.a);
         kotlin.d.b.l.a(var8, "view.onTakePhotoClicked(…     }, { Timber.e(it) })");
         this.b = co.uk.getmondo.common.j.f.a(var5, var8);
      }

      var9 = this.b;
      var10 = var1.b().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n var1x) {
            if(g.this.m) {
               var1.i();
            } else {
               var1.a(g.this.j);
            }

         }
      }), (io.reactivex.c.g)null.a);
      kotlin.d.b.l.a(var10, "view.onTakeVideoClicked(…en take video screen\") })");
      this.b = co.uk.getmondo.common.j.f.a(var9, var10);
      var9 = this.b;
      var10 = co.uk.getmondo.common.j.f.a(this.g.b(), this.g.c()).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.h var1x) {
            boolean var4 = false;
            com.c.b.b var5 = (com.c.b.b)var1x.c();
            com.c.b.b var7 = (com.c.b.b)var1x.d();
            boolean var2;
            if(var5.b() && ((co.uk.getmondo.signup.identity_verification.a.a.b)var5.a()).a(g.this.h, g.this.j)) {
               var2 = true;
            } else {
               var2 = false;
            }

            boolean var3x = var4;
            if(var7.b()) {
               var3x = var4;
               if(g.this.h.a((String)var7.a())) {
                  var3x = true;
               }
            }

            if(var2 && var3x) {
               var1.l();
            } else {
               var1.m();
            }

            if(var2) {
               g.a var6 = var1;
               Object var9 = var5.a();
               kotlin.d.b.l.a(var9, "identityDocument.get()");
               var6.a((co.uk.getmondo.signup.identity_verification.a.a.b)var9);
            } else {
               var1.n();
            }

            if(var3x) {
               g.a var10 = var1;
               Object var8 = var7.a();
               kotlin.d.b.l.a(var8, "videoPath.get()");
               var10.b((String)var8);
            } else {
               var1.o();
            }

            if(!g.this.c) {
               g.this.i.a(Impression.Companion.a(var2, var3x, var3));
            }

         }
      }), (io.reactivex.c.g)null.a);
      kotlin.d.b.l.a(var10, "combineLatest(verificati…ate of submit button\") })");
      this.b = co.uk.getmondo.common.j.f.a(var9, var10);
      var9 = this.b;
      io.reactivex.b.b var6 = var1.c().flatMap((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.n a(kotlin.n var1x) {
            kotlin.d.b.l.b(var1x, "it");
            return var1.d();
         }
      })).flatMapMaybe((io.reactivex.c.h)(new io.reactivex.c.h() {
         // $FF: synthetic method
         public Object a(Object var1x) {
            return this.b(var1x);
         }

         public final io.reactivex.h b(Object var1x) {
            kotlin.d.b.l.b(var1x, "it");
            return g.this.g.a().e().b(g.this.d).a(g.this.e).b((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(io.reactivex.b.b var1x) {
                  var1.j();
               }
            })).a((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(Throwable var1x) {
                  co.uk.getmondo.common.e.a var2 = g.this.f;
                  kotlin.d.b.l.a(var1x, "throwable");
                  if(!var2.a(var1x, (co.uk.getmondo.common.e.a.a)var1)) {
                     var1.b(2131362331);
                  }

               }
            })).f().a((io.reactivex.c.b)(new io.reactivex.c.b() {
               public final void a(Boolean var1x, Throwable var2) {
                  var1.k();
               }
            }));
         }
      })).doOnNext((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Boolean var1) {
            co.uk.getmondo.common.a var2 = g.this.i;
            Impression.Companion var4 = Impression.Companion;
            Impression.KycFrom var3 = g.this.k;
            if(var1 == null) {
               kotlin.d.b.l.a();
            }

            var2.a(var4.a(var3, var1.booleanValue()));
         }
      })).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Boolean var1x) {
            var1.a(g.this.j, g.this.k);
         }
      }), (io.reactivex.c.g)null.a);
      kotlin.d.b.l.a(var6, "view.onSubmitVerificatio…rification documents\") })");
      this.b = co.uk.getmondo.common.j.f.a(var9, var6);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0004\bf\u0018\u00002\u00020\u00012\u00020\u0002J\b\u0010\u0003\u001a\u00020\u0004H&J\b\u0010\u0005\u001a\u00020\u0004H&J\b\u0010\u0006\u001a\u00020\u0004H&J\u001a\u0010\u0007\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000b0\t0\bH&J\u000e\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000b0\bH&J\u000e\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00040\bH&J\u000e\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u000f0\bH&J\u000e\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00040\bH&J\u000e\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00040\bH&J\u0018\u0010\u0012\u001a\u00020\u00042\u0006\u0010\u0013\u001a\u00020\u000b2\u0006\u0010\u0014\u001a\u00020\u0015H&J \u0010\u0016\u001a\u00020\u00042\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u000b2\u0006\u0010\u001a\u001a\u00020\nH&J \u0010\u001b\u001a\u00020\u00042\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0013\u001a\u00020\u000b2\u0006\u0010\u001a\u001a\u00020\nH&J\u0010\u0010\u001c\u001a\u00020\u00042\u0006\u0010\u0017\u001a\u00020\u0018H&J\u0018\u0010\u001d\u001a\u00020\u00042\u0006\u0010\u001e\u001a\u00020\u00182\u0006\u0010\u001f\u001a\u00020 H&J\b\u0010!\u001a\u00020\u0004H&J\u000e\u0010\"\u001a\b\u0012\u0004\u0012\u00020\u00150#H&J\u0010\u0010$\u001a\u00020\u00042\u0006\u0010%\u001a\u00020&H&J\b\u0010'\u001a\u00020\u0004H&J\u0016\u0010(\u001a\b\u0012\u0004\u0012\u00020\u00150#2\u0006\u0010\u0013\u001a\u00020\u000bH&J\u000e\u0010)\u001a\b\u0012\u0004\u0012\u00020\u000b0#H&J\b\u0010*\u001a\u00020\u0004H&J\u000e\u0010+\u001a\b\u0012\u0004\u0012\u00020\u000b0#H&J\u0010\u0010,\u001a\u00020\u00042\u0006\u0010-\u001a\u00020.H&J\u0010\u0010/\u001a\u00020\u00042\u0006\u00100\u001a\u00020.H&J\b\u00101\u001a\u00020\u0004H&¨\u00062"},
      d2 = {"Lco/uk/getmondo/signup/identity_verification/IdentityDocumentsPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "disableSubmitVerificationButton", "", "enableSubmitVerificationButton", "hideLoading", "onDocumentCountrySelected", "Lio/reactivex/Observable;", "Lkotlin/Pair;", "Lco/uk/getmondo/model/Country;", "Lco/uk/getmondo/api/model/identity_verification/IdentityDocumentType;", "onPhotoIdTypeChosen", "onSubmitVerificationClicked", "onSubmitVerificationConfirmationClicked", "", "onTakePhotoClicked", "onTakeVideoClicked", "openCountrySelection", "documentType", "useSystemCamera", "", "openDocumentCamera", "version", "Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationVersion;", "idDocumentType", "country", "openDocumentCameraFallback", "openTakeVideo", "openVerificationPending", "identityVerificationVersion", "from", "Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;", "openVideoFallback", "showDoYouHavePassport", "Lio/reactivex/Maybe;", "showIdentityDocComplete", "identityDocument", "Lco/uk/getmondo/signup/identity_verification/data/model/IdentityDocument;", "showIdentityDocIncomplete", "showIsDocumentUk", "showLegacyIdentityDocumentSelection", "showLoading", "showOtherIdentityDocumentsSelection", "showRejectedReason", "message", "", "showVideoComplete", "videoPath", "showVideoIncomplete", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      io.reactivex.h a(IdentityDocumentType var1);

      io.reactivex.n a();

      void a(IdentityDocumentType var1, boolean var2);

      void a(co.uk.getmondo.signup.identity_verification.a.a.b var1);

      void a(co.uk.getmondo.signup.identity_verification.a.j var1);

      void a(co.uk.getmondo.signup.identity_verification.a.j var1, IdentityDocumentType var2, co.uk.getmondo.d.i var3);

      void a(co.uk.getmondo.signup.identity_verification.a.j var1, Impression.KycFrom var2);

      void a(String var1);

      io.reactivex.n b();

      void b(co.uk.getmondo.signup.identity_verification.a.j var1, IdentityDocumentType var2, co.uk.getmondo.d.i var3);

      void b(String var1);

      io.reactivex.n c();

      io.reactivex.n d();

      io.reactivex.n e();

      io.reactivex.h f();

      io.reactivex.h g();

      io.reactivex.h h();

      void i();

      void j();

      void k();

      void l();

      void m();

      void n();

      void o();
   }
}
