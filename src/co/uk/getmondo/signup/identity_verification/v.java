package co.uk.getmondo.signup.identity_verification;

import co.uk.getmondo.api.model.identity_verification.IdentityVerification;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0012B3\b\u0007\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b¢\u0006\u0002\u0010\fJ\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0002H\u0016R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0013"},
   d2 = {"Lco/uk/getmondo/signup/identity_verification/IdentityVerificationPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/signup/identity_verification/IdentityVerificationPresenter$View;", "ioScheduler", "Lio/reactivex/Scheduler;", "uiScheduler", "apiErrorHandler", "Lco/uk/getmondo/common/errors/ApiErrorHandler;", "verificationManager", "Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationManager;", "profileManager", "Lco/uk/getmondo/profile/data/ProfileManager;", "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationManager;Lco/uk/getmondo/profile/data/ProfileManager;)V", "allowSystemCamera", "", "register", "", "view", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class v extends co.uk.getmondo.common.ui.b {
   private boolean c;
   private final io.reactivex.u d;
   private final io.reactivex.u e;
   private final co.uk.getmondo.common.e.a f;
   private final co.uk.getmondo.signup.identity_verification.a.e g;
   private final co.uk.getmondo.profile.data.a h;

   public v(io.reactivex.u var1, io.reactivex.u var2, co.uk.getmondo.common.e.a var3, co.uk.getmondo.signup.identity_verification.a.e var4, co.uk.getmondo.profile.data.a var5) {
      kotlin.d.b.l.b(var1, "ioScheduler");
      kotlin.d.b.l.b(var2, "uiScheduler");
      kotlin.d.b.l.b(var3, "apiErrorHandler");
      kotlin.d.b.l.b(var4, "verificationManager");
      kotlin.d.b.l.b(var5, "profileManager");
      super();
      this.d = var1;
      this.e = var2;
      this.f = var3;
      this.g = var4;
      this.h = var5;
   }

   public void a(final v.a var1) {
      kotlin.d.b.l.b(var1, "view");
      super.a((co.uk.getmondo.common.ui.f)var1);
      io.reactivex.b.a var3 = this.b;
      io.reactivex.b.b var2 = var1.c().startWith((Object)kotlin.n.a).doOnSubscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(io.reactivex.b.b var1x) {
            var1.f();
         }
      })).flatMapMaybe((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.h a(kotlin.n var1x) {
            kotlin.d.b.l.b(var1x, "it");
            return co.uk.getmondo.common.j.f.a(v.this.g.d().a((io.reactivex.c.h)(new io.reactivex.c.h() {
               public final io.reactivex.v a(IdentityVerification var1x) {
                  kotlin.d.b.l.b(var1x, "it");
                  io.reactivex.v var2;
                  if(kotlin.d.b.l.a(var1x.b(), IdentityVerification.Status.NOT_STARTED)) {
                     var2 = v.this.h.a().a((Object)var1x);
                  } else {
                     var2 = io.reactivex.v.a((Object)var1x);
                  }

                  return var2;
               }
            })).b(v.this.d).a(v.this.e).d((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(Throwable var1x) {
                  var1.g();
                  co.uk.getmondo.signup.i var2 = co.uk.getmondo.signup.i.a;
                  kotlin.d.b.l.a(var1x, "it");
                  if(!var2.a(var1x, (co.uk.getmondo.signup.i.a)var1) && !v.this.f.a(var1x, (co.uk.getmondo.common.e.a.a)var1)) {
                     var1.b(2131362198);
                  }

               }
            })));
         }
      })).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(IdentityVerification var1x) {
            var1.g();
            v.this.c = var1x.c();
            IdentityVerification.Status var2 = var1x.b();
            if(var2 != null) {
               switch(w.a[var2.ordinal()]) {
               case 1:
                  var1.h();
                  return;
               case 2:
                  var1.a(var1x.c(), var1x.a());
                  return;
               case 3:
                  var1.i();
                  return;
               }
            }

            var1.j();
         }
      }));
      kotlin.d.b.l.a(var2, "view.onRetryClicked\n    …      }\n                }");
      this.b = co.uk.getmondo.common.j.f.a(var3, var2);
      io.reactivex.b.a var5 = this.b;
      io.reactivex.b.b var4 = var1.e().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n var1x) {
            var1.a(v.this.c, (String)null);
         }
      }));
      kotlin.d.b.l.a(var4, "view.onOnboardingComplet…llowSystemCamera, null) }");
      this.b = co.uk.getmondo.common.j.f.a(var5, var4);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\bf\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003J\b\u0010\u000b\u001a\u00020\u0006H&J\b\u0010\f\u001a\u00020\u0006H&J\b\u0010\r\u001a\u00020\u0006H&J\u001a\u0010\u000e\u001a\u00020\u00062\u0006\u0010\u000f\u001a\u00020\u00102\b\u0010\u0011\u001a\u0004\u0018\u00010\u0012H&J\b\u0010\u0013\u001a\u00020\u0006H&J\b\u0010\u0014\u001a\u00020\u0006H&R\u0018\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0007\u0010\bR\u0018\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X¦\u0004¢\u0006\u0006\u001a\u0004\b\n\u0010\b¨\u0006\u0015"},
      d2 = {"Lco/uk/getmondo/signup/identity_verification/IdentityVerificationPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "Lco/uk/getmondo/signup/SignupApiErrorHandler$View;", "onOnboardingComplete", "Lio/reactivex/Observable;", "", "getOnOnboardingComplete", "()Lio/reactivex/Observable;", "onRetryClicked", "getOnRetryClicked", "hideLoading", "openBlocked", "openGenericError", "showIdentityDocuments", "allowSystemCamera", "", "rejectionNote", "", "showLoading", "showOnboarding", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f, co.uk.getmondo.signup.i.a {
      void a(boolean var1, String var2);

      io.reactivex.n c();

      io.reactivex.n e();

      void f();

      void g();

      void h();

      void i();

      void j();
   }
}
