package co.uk.getmondo.signup.identity_verification.id_picture;

import android.content.Context;
import co.uk.getmondo.api.model.identity_verification.IdentityDocumentType;

public final class t implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;
   private final javax.a.a c;

   static {
      boolean var0;
      if(!t.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   public t(javax.a.a var1, javax.a.a var2) {
      if(!a && var1 == null) {
         throw new AssertionError();
      } else {
         this.b = var1;
         if(!a && var2 == null) {
            throw new AssertionError();
         } else {
            this.c = var2;
         }
      }
   }

   public static b.a.b a(javax.a.a var0, javax.a.a var1) {
      return new t(var0, var1);
   }

   public s a() {
      return new s((Context)this.b.b(), (IdentityDocumentType)this.c.b());
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
