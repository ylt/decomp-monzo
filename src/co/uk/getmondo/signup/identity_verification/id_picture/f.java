package co.uk.getmondo.signup.identity_verification.id_picture;

import co.uk.getmondo.api.model.identity_verification.IdentityDocumentType;

public class f {
   private final IdentityDocumentType a;
   private final co.uk.getmondo.d.i b;
   private final String c;

   public f(IdentityDocumentType var1, co.uk.getmondo.d.i var2, String var3) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
   }

   IdentityDocumentType a() {
      return this.a;
   }

   co.uk.getmondo.d.i b() {
      return this.b;
   }

   String c() {
      return this.c;
   }
}
