package co.uk.getmondo.signup.identity_verification.id_picture;

// $FF: synthetic class
final class q implements io.reactivex.c.g {
   private final j a;
   private final j.a b;
   private final boolean c;

   private q(j var1, j.a var2, boolean var3) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
   }

   public static io.reactivex.c.g a(j var0, j.a var1, boolean var2) {
      return new q(var0, var1, var2);
   }

   public void a(Object var1) {
      j.a(this.a, this.b, this.c, (Boolean)var1);
   }
}
