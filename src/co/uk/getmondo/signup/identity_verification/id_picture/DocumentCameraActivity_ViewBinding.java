package co.uk.getmondo.signup.identity_verification.id_picture;

import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.google.android.cameraview.CameraView;

public class DocumentCameraActivity_ViewBinding implements Unbinder {
   private DocumentCameraActivity a;

   public DocumentCameraActivity_ViewBinding(DocumentCameraActivity var1, View var2) {
      this.a = var1;
      var1.cameraView = (CameraView)Utils.findRequiredViewAsType(var2, 2131821105, "field 'cameraView'", CameraView.class);
      var1.previewImage = (ImageView)Utils.findRequiredViewAsType(var2, 2131821106, "field 'previewImage'", ImageView.class);
      var1.takePictureButton = (ImageButton)Utils.findRequiredViewAsType(var2, 2131821109, "field 'takePictureButton'", ImageButton.class);
      var1.instructionsView = (TextView)Utils.findRequiredViewAsType(var2, 2131821104, "field 'instructionsView'", TextView.class);
      var1.frameOverlayView = (FrameOverlayView)Utils.findRequiredViewAsType(var2, 2131821107, "field 'frameOverlayView'", FrameOverlayView.class);
      var1.usePictureButton = (Button)Utils.findRequiredViewAsType(var2, 2131821110, "field 'usePictureButton'", Button.class);
      var1.cameraProgress = (ProgressBar)Utils.findRequiredViewAsType(var2, 2131820977, "field 'cameraProgress'", ProgressBar.class);
      var1.thumbnailImageView = (ImageView)Utils.findRequiredViewAsType(var2, 2131821111, "field 'thumbnailImageView'", ImageView.class);
      var1.documentPositionTextView = (TextView)Utils.findRequiredViewAsType(var2, 2131821108, "field 'documentPositionTextView'", TextView.class);
   }

   public void unbind() {
      DocumentCameraActivity var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.cameraView = null;
         var1.previewImage = null;
         var1.takePictureButton = null;
         var1.instructionsView = null;
         var1.frameOverlayView = null;
         var1.usePictureButton = null;
         var1.cameraProgress = null;
         var1.thumbnailImageView = null;
         var1.documentPositionTextView = null;
      }
   }
}
