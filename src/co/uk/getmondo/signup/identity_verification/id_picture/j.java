package co.uk.getmondo.signup.identity_verification.id_picture;

import android.graphics.Bitmap;
import android.net.Uri;
import co.uk.getmondo.api.model.identity_verification.IdentityDocumentType;
import co.uk.getmondo.api.model.signup.SignupSource;
import co.uk.getmondo.api.model.tracking.Impression;

public class j extends co.uk.getmondo.common.ui.b {
   private Uri c;
   private final io.reactivex.u d;
   private final io.reactivex.u e;
   private final s f;
   private final io.reactivex.u g;
   private final co.uk.getmondo.signup.identity_verification.a.e h;
   private final co.uk.getmondo.common.a i;
   private final u j;
   private final co.uk.getmondo.signup.identity_verification.a.j k;
   private final IdentityDocumentType l;
   private final SignupSource m;
   private final String n;
   private final co.uk.getmondo.d.i o;

   j(io.reactivex.u var1, io.reactivex.u var2, io.reactivex.u var3, co.uk.getmondo.signup.identity_verification.a.e var4, s var5, co.uk.getmondo.common.a var6, u var7, co.uk.getmondo.signup.identity_verification.a.j var8, IdentityDocumentType var9, co.uk.getmondo.d.i var10, SignupSource var11, String var12) {
      this.d = var1;
      this.e = var2;
      this.g = var3;
      this.h = var4;
      this.f = var5;
      this.i = var6;
      this.j = var7;
      this.k = var8;
      this.l = var9;
      this.m = var11;
      this.n = var12;
      this.o = var10;
   }

   // $FF: synthetic method
   static io.reactivex.r a(j var0, j.a var1, boolean var2, Bitmap var3) throws Exception {
      return var1.a(var3, var2).subscribeOn(var0.e);
   }

   // $FF: synthetic method
   static io.reactivex.r a(j var0, byte[] var1) throws Exception {
      return var0.j.a(var1).subscribeOn(var0.g);
   }

   // $FF: synthetic method
   static void a(j.a var0, Object var1) throws Exception {
      var0.b(false);
      var0.c(false);
      var0.h();
      var0.j();
   }

   // $FF: synthetic method
   static void a(j var0, j.a var1, Bitmap var2) throws Exception {
      var1.i();
      var1.a(var2);
      var1.e();
      var1.a(var0.f.a());
   }

   // $FF: synthetic method
   static void a(j var0, j.a var1, Uri var2) throws Exception {
      var0.c = var2;
      var1.c(true);
   }

   // $FF: synthetic method
   static void a(j var0, j.a var1, Object var2) throws Exception {
      if(var0.k == co.uk.getmondo.signup.identity_verification.a.j.b && var0.l.b() && var0.n == null) {
         var1.a(var0.k, var0.l, var0.o, var0.m, var0.c.getPath());
      } else {
         String var4;
         if(var0.n == null) {
            var4 = var0.c.getPath();
         } else {
            var4 = var0.n;
         }

         String var3;
         if(var0.n != null) {
            var3 = var0.c.getPath();
         } else {
            var3 = null;
         }

         var0.h.a(new co.uk.getmondo.signup.identity_verification.a.a.b(var4, var0.l, var0.o, var3), false);
         var0.i.a(Impression.h(var0.l.a()));
      }

      var1.finish();
   }

   // $FF: synthetic method
   static void a(j var0, j.a var1, boolean var2, Boolean var3) throws Exception {
      if(var3.booleanValue()) {
         var1.g();
         var1.f();
         var1.b(true);
         var1.a(var0.f.a(var2));
         if(var0.n != null) {
            var1.d(var0.n);
         }
      } else {
         var1.k();
      }

   }

   public void a(j.a var1) {
      boolean var3 = true;
      super.a((co.uk.getmondo.common.ui.f)var1);
      this.i.a(Impression.a(this.l.a(), false));
      boolean var2;
      if(this.n == null) {
         var2 = true;
      } else {
         var2 = false;
      }

      var1.f();
      var1.a(this.f.a(var2));
      if(!var2) {
         var1.d(this.n);
      }

      if(this.k == co.uk.getmondo.signup.identity_verification.a.j.b && this.l.b()) {
         if(this.n != null) {
            var3 = false;
         }

         var1.a(var3);
      }

      this.a((io.reactivex.b.b)var1.b().subscribe(k.a(var1)));
      this.a((io.reactivex.b.b)var1.d().concatMap(l.a(this)).observeOn(this.d).doOnNext(m.a(this, var1)).concatMap(n.a(this, var1, var2)).observeOn(this.d).subscribe(o.a(this, var1)));
      this.a((io.reactivex.b.b)var1.c().subscribe(p.a(this, var1)));
      this.a((io.reactivex.b.b)var1.a().subscribe(q.a(this, var1, var2)));
   }

   interface a extends co.uk.getmondo.common.ui.f {
      io.reactivex.n a();

      io.reactivex.n a(Bitmap var1, boolean var2);

      void a(Bitmap var1);

      void a(co.uk.getmondo.signup.identity_verification.a.j var1, IdentityDocumentType var2, co.uk.getmondo.d.i var3, SignupSource var4, String var5);

      void a(String var1);

      void a(boolean var1);

      io.reactivex.n b();

      void b(boolean var1);

      io.reactivex.n c();

      void c(boolean var1);

      io.reactivex.n d();

      void d(String var1);

      void e();

      void f();

      void finish();

      void g();

      void h();

      void i();

      void j();

      void k();
   }
}
