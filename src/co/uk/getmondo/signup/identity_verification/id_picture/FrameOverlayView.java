package co.uk.getmondo.signup.identity_verification.id_picture;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Paint.Style;
import android.util.AttributeSet;
import android.view.View;
import co.uk.getmondo.api.model.identity_verification.IdentityDocumentType;

public class FrameOverlayView extends View {
   private final int a;
   private final int b;
   private final int c;
   private final int d;
   private final int e;
   private final int f;
   private final Paint g;
   private final Paint h;
   private final Paint i;
   private final Path j;
   private IdentityDocumentType k;
   private boolean l;

   public FrameOverlayView(Context var1) {
      this(var1, (AttributeSet)null, 0);
   }

   public FrameOverlayView(Context var1, AttributeSet var2) {
      this(var1, var2, 0);
   }

   public FrameOverlayView(Context var1, AttributeSet var2, int var3) {
      super(var1, var2, var3);
      this.a = co.uk.getmondo.common.k.n.b(16);
      this.b = co.uk.getmondo.common.k.n.b(62);
      this.c = co.uk.getmondo.common.k.n.b(2);
      this.d = co.uk.getmondo.common.k.n.b(75);
      this.e = co.uk.getmondo.common.k.n.b(16);
      this.f = co.uk.getmondo.common.k.n.b(3);
      this.j = new Path();
      this.k = IdentityDocumentType.PASSPORT;
      this.l = false;
      this.g = new Paint(1);
      this.g.setStyle(Style.STROKE);
      this.g.setColor(-1);
      this.g.setStrokeWidth((float)this.c);
      this.g.setPathEffect(new DashPathEffect(new float[]{20.0F, 10.0F}, 0.0F));
      this.i = new Paint();
      this.i.setAntiAlias(true);
      this.i.setStyle(Style.STROKE);
      this.h = new Paint();
      this.h.setColor(android.support.v4.content.a.c(var1, 2131689526));
      this.h.setStyle(Style.FILL);
   }

   private void a(Canvas var1) {
      if(this.l) {
         this.a(var1, this.b, this.a);
      }

      this.b(var1, this.b, this.a);
      var1.drawPath(this.j, this.g);
   }

   private void a(Canvas var1, int var2, int var3) {
      float var4 = (float)this.c / 2.0F;
      var1.drawRect(0.0F, 0.0F, (float)var2 + var4, (float)this.getHeight(), this.h);
      var1.drawRect((float)(this.getWidth() - var2) - var4, 0.0F, (float)this.getWidth(), (float)this.getHeight(), this.h);
      var1.drawRect((float)var2, 0.0F, (float)(this.getWidth() - var2), (float)var3, this.h);
      var1.drawRect((float)var2, (float)(this.getHeight() - var3), (float)(this.getWidth() - var2), (float)this.getHeight(), this.h);
   }

   private void b(Canvas var1) {
      if(this.l) {
         this.a(var1, this.e, this.d);
      }

      this.b(var1, this.e, this.d);
   }

   private void b(Canvas var1, int var2, int var3) {
      var1.drawRoundRect((float)var2, (float)var3, (float)(this.getWidth() - var2), (float)(this.getHeight() - var3), (float)this.f, (float)this.f, this.i);
   }

   protected void onDraw(Canvas var1) {
      super.onDraw(var1);
      if(this.k == IdentityDocumentType.PASSPORT) {
         this.a(var1);
      } else {
         this.b(var1);
      }

   }

   protected void onLayout(boolean var1, int var2, int var3, int var4, int var5) {
      super.onLayout(var1, var2, var3, var4, var5);
      if(this.k == IdentityDocumentType.PASSPORT) {
         this.j.reset();
         this.j.moveTo((float)this.b, (float)(this.getHeight() / 2));
         this.j.lineTo((float)(this.getWidth() - this.b), (float)(this.getHeight() / 2));
      }

   }

   public void setFrameIdType(IdentityDocumentType var1) {
      this.k = var1;
      this.i.setColor(-1);
      this.i.setStrokeWidth((float)this.c);
   }

   public void setOpaqueFrame(boolean var1) {
      if(this.l != var1) {
         this.l = var1;
         this.invalidate();
      }

   }
}
