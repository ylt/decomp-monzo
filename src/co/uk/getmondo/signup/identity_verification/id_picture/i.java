package co.uk.getmondo.signup.identity_verification.id_picture;

import co.uk.getmondo.api.model.identity_verification.IdentityDocumentType;

public final class i implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final f b;

   static {
      boolean var0;
      if(!i.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   public i(f var1) {
      if(!a && var1 == null) {
         throw new AssertionError();
      } else {
         this.b = var1;
      }
   }

   public static b.a.b a(f var0) {
      return new i(var0);
   }

   public IdentityDocumentType a() {
      return (IdentityDocumentType)b.a.d.a(this.b.a(), "Cannot return null from a non-@Nullable @Provides method");
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
