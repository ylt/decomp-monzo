package co.uk.getmondo.signup.identity_verification.id_picture;

import android.graphics.Bitmap;

// $FF: synthetic class
final class m implements io.reactivex.c.g {
   private final j a;
   private final j.a b;

   private m(j var1, j.a var2) {
      this.a = var1;
      this.b = var2;
   }

   public static io.reactivex.c.g a(j var0, j.a var1) {
      return new m(var0, var1);
   }

   public void a(Object var1) {
      j.a(this.a, this.b, (Bitmap)var1);
   }
}
