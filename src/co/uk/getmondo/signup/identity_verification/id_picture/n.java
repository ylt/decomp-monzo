package co.uk.getmondo.signup.identity_verification.id_picture;

import android.graphics.Bitmap;

// $FF: synthetic class
final class n implements io.reactivex.c.h {
   private final j a;
   private final j.a b;
   private final boolean c;

   private n(j var1, j.a var2, boolean var3) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
   }

   public static io.reactivex.c.h a(j var0, j.a var1, boolean var2) {
      return new n(var0, var1, var2);
   }

   public Object a(Object var1) {
      return j.a(this.a, this.b, this.c, (Bitmap)var1);
   }
}
