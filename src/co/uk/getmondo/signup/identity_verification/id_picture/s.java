package co.uk.getmondo.signup.identity_verification.id_picture;

import android.content.Context;
import co.uk.getmondo.api.model.identity_verification.IdentityDocumentType;

public class s {
   private Context a;
   private final IdentityDocumentType b;

   s(Context var1, IdentityDocumentType var2) {
      this.a = var1;
      this.b = var2;
   }

   private String b() {
      return this.a.getString(this.b.c());
   }

   String a() {
      return this.a.getString(2131362256, new Object[]{this.b()});
   }

   String a(boolean var1) {
      String var3;
      if(!this.b.b()) {
         var3 = this.a.getString(2131362254, new Object[]{this.b()});
      } else {
         Context var4 = this.a;
         int var2;
         if(var1) {
            var2 = 2131362262;
         } else {
            var2 = 2131362261;
         }

         var3 = var4.getString(var2).toLowerCase();
         var3 = this.a.getString(2131362255, new Object[]{var3, this.b()});
      }

      return var3;
   }
}
