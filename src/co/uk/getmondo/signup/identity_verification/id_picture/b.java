package co.uk.getmondo.signup.identity_verification.id_picture;

import android.graphics.Bitmap;
import java.util.concurrent.Callable;

// $FF: synthetic class
final class b implements Callable {
   private final DocumentCameraActivity a;
   private final boolean b;
   private final Bitmap c;

   private b(DocumentCameraActivity var1, boolean var2, Bitmap var3) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
   }

   public static Callable a(DocumentCameraActivity var0, boolean var1, Bitmap var2) {
      return new b(var0, var1, var2);
   }

   public Object call() {
      return DocumentCameraActivity.a(this.a, this.b, this.c);
   }
}
