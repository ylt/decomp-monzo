package co.uk.getmondo.signup.identity_verification.id_picture;

import co.uk.getmondo.api.model.identity_verification.IdentityDocumentType;
import co.uk.getmondo.api.model.signup.SignupSource;

public final class r implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final b.a b;
   private final javax.a.a c;
   private final javax.a.a d;
   private final javax.a.a e;
   private final javax.a.a f;
   private final javax.a.a g;
   private final javax.a.a h;
   private final javax.a.a i;
   private final javax.a.a j;
   private final javax.a.a k;
   private final javax.a.a l;
   private final javax.a.a m;
   private final javax.a.a n;

   static {
      boolean var0;
      if(!r.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   public r(b.a var1, javax.a.a var2, javax.a.a var3, javax.a.a var4, javax.a.a var5, javax.a.a var6, javax.a.a var7, javax.a.a var8, javax.a.a var9, javax.a.a var10, javax.a.a var11, javax.a.a var12, javax.a.a var13) {
      if(!a && var1 == null) {
         throw new AssertionError();
      } else {
         this.b = var1;
         if(!a && var2 == null) {
            throw new AssertionError();
         } else {
            this.c = var2;
            if(!a && var3 == null) {
               throw new AssertionError();
            } else {
               this.d = var3;
               if(!a && var4 == null) {
                  throw new AssertionError();
               } else {
                  this.e = var4;
                  if(!a && var5 == null) {
                     throw new AssertionError();
                  } else {
                     this.f = var5;
                     if(!a && var6 == null) {
                        throw new AssertionError();
                     } else {
                        this.g = var6;
                        if(!a && var7 == null) {
                           throw new AssertionError();
                        } else {
                           this.h = var7;
                           if(!a && var8 == null) {
                              throw new AssertionError();
                           } else {
                              this.i = var8;
                              if(!a && var9 == null) {
                                 throw new AssertionError();
                              } else {
                                 this.j = var9;
                                 if(!a && var10 == null) {
                                    throw new AssertionError();
                                 } else {
                                    this.k = var10;
                                    if(!a && var11 == null) {
                                       throw new AssertionError();
                                    } else {
                                       this.l = var11;
                                       if(!a && var12 == null) {
                                          throw new AssertionError();
                                       } else {
                                          this.m = var12;
                                          if(!a && var13 == null) {
                                             throw new AssertionError();
                                          } else {
                                             this.n = var13;
                                          }
                                       }
                                    }
                                 }
                              }
                           }
                        }
                     }
                  }
               }
            }
         }
      }
   }

   public static b.a.b a(b.a var0, javax.a.a var1, javax.a.a var2, javax.a.a var3, javax.a.a var4, javax.a.a var5, javax.a.a var6, javax.a.a var7, javax.a.a var8, javax.a.a var9, javax.a.a var10, javax.a.a var11, javax.a.a var12) {
      return new r(var0, var1, var2, var3, var4, var5, var6, var7, var8, var9, var10, var11, var12);
   }

   public j a() {
      return (j)b.a.c.a(this.b, new j((io.reactivex.u)this.c.b(), (io.reactivex.u)this.d.b(), (io.reactivex.u)this.e.b(), (co.uk.getmondo.signup.identity_verification.a.e)this.f.b(), (s)this.g.b(), (co.uk.getmondo.common.a)this.h.b(), (u)this.i.b(), (co.uk.getmondo.signup.identity_verification.a.j)this.j.b(), (IdentityDocumentType)this.k.b(), (co.uk.getmondo.d.i)this.l.b(), (SignupSource)this.m.b(), (String)this.n.b()));
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
