package co.uk.getmondo.signup.identity_verification.id_picture;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.uk.getmondo.api.model.identity_verification.IdentityDocumentType;
import co.uk.getmondo.api.model.signup.SignupSource;
import com.google.android.cameraview.CameraView;
import java.io.File;
import java.io.FileOutputStream;

public class DocumentCameraActivity extends co.uk.getmondo.common.activities.b implements j.a {
   j a;
   co.uk.getmondo.signup.identity_verification.a.a b;
   private com.b.b.c c = com.b.b.c.a();
   @BindView(2131820977)
   ProgressBar cameraProgress;
   @BindView(2131821105)
   CameraView cameraView;
   @BindView(2131821108)
   TextView documentPositionTextView;
   private boolean e = false;
   @BindView(2131821107)
   FrameOverlayView frameOverlayView;
   @BindView(2131821104)
   TextView instructionsView;
   @BindView(2131821106)
   ImageView previewImage;
   @BindView(2131821109)
   ImageButton takePictureButton;
   @BindView(2131821111)
   ImageView thumbnailImageView;
   @BindView(2131821110)
   Button usePictureButton;

   public static Intent a(Context var0, co.uk.getmondo.signup.identity_verification.a.j var1, IdentityDocumentType var2, co.uk.getmondo.d.i var3, SignupSource var4) {
      return (new Intent(var0, DocumentCameraActivity.class)).putExtra("KEY_VERSION", var1).putExtra("KEY_ID_DOCUMENT_TYPE", var2).putExtra("KEY_COUNTRY_CODE", var3.e()).putExtra("KEY_SIGNUP_SOURCE", var4);
   }

   public static Intent a(Context var0, co.uk.getmondo.signup.identity_verification.a.j var1, IdentityDocumentType var2, co.uk.getmondo.d.i var3, SignupSource var4, String var5) {
      return a(var0, var1, var2, var3, var4).putExtra("KEY_FRONT_PHOTO_PATH", var5);
   }

   // $FF: synthetic method
   static Uri a(DocumentCameraActivity var0, boolean var1, Bitmap var2) throws Exception {
      File var20;
      if(var1) {
         var20 = var0.b.b();
      } else {
         var20 = var0.b.c();
      }

      FileOutputStream var4 = new FileOutputStream(var20);
      Object var3 = null;

      label141: {
         Throwable var21;
         try {
            var2.compress(CompressFormat.JPEG, 90, var4);
            break label141;
         } catch (Throwable var18) {
            var21 = var18;
         } finally {
            ;
         }

         try {
            throw var21;
         } finally {
            if(var4 != null) {
               if(var21 != null) {
                  try {
                     var4.close();
                  } catch (Throwable var15) {
                     ;
                  }
               } else {
                  var4.close();
               }
            }

            throw var20;
         }
      }

      if(var4 != null) {
         if(false) {
            try {
               var4.close();
            } catch (Throwable var17) {
               ;
            }
         } else {
            var4.close();
         }
      }

      return Uri.fromFile(var20);
   }

   // $FF: synthetic method
   static void a(DocumentCameraActivity var0, com.google.android.cameraview.CameraView.a var1) throws Exception {
      var0.cameraView.b(var1);
   }

   // $FF: synthetic method
   static void a(final DocumentCameraActivity var0, final io.reactivex.o var1) throws Exception {
      com.google.android.cameraview.CameraView.a var2 = new com.google.android.cameraview.CameraView.a() {
         public void a(CameraView var1x, byte[] var2) {
            var1.a(var2);
         }
      };
      var0.cameraView.a(var2);
      var1.a(c.a(var0, var2));
   }

   private boolean v() {
      boolean var1;
      if(android.support.v4.content.a.b(this, "android.permission.CAMERA") == 0) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public io.reactivex.n a() {
      return this.c;
   }

   public io.reactivex.n a(Bitmap var1, boolean var2) {
      return io.reactivex.n.fromCallable(b.a(this, var2, var1));
   }

   public void a(Bitmap var1) {
      this.thumbnailImageView.setVisibility(8);
      this.previewImage.setImageBitmap(var1);
      this.previewImage.setVisibility(0);
      this.frameOverlayView.setOpaqueFrame(true);
   }

   public void a(co.uk.getmondo.signup.identity_verification.a.j var1, IdentityDocumentType var2, co.uk.getmondo.d.i var3, SignupSource var4, String var5) {
      this.startActivity(a(this, var1, var2, var3, var4, var5));
   }

   public void a(String var1) {
      this.instructionsView.setText(var1);
   }

   public void a(boolean var1) {
      TextView var3 = this.documentPositionTextView;
      int var2;
      if(var1) {
         var2 = 2131362262;
      } else {
         var2 = 2131362261;
      }

      var3.setText(var2);
      this.documentPositionTextView.setVisibility(0);
   }

   public io.reactivex.n b() {
      return com.b.a.c.c.a(this.takePictureButton);
   }

   public void b(boolean var1) {
      this.takePictureButton.setEnabled(var1);
   }

   public io.reactivex.n c() {
      return com.b.a.c.c.a(this.usePictureButton);
   }

   public void c(boolean var1) {
      this.usePictureButton.setEnabled(var1);
   }

   public io.reactivex.n d() {
      return io.reactivex.n.create(a.a(this));
   }

   public void d(String var1) {
      File var2 = new File(var1);
      com.bumptech.glide.g.a((android.support.v4.app.j)this).a(var2).a(new com.bumptech.glide.h.b(String.valueOf(var2.lastModified()))).a(0.2F).a(this.thumbnailImageView);
      this.thumbnailImageView.setVisibility(0);
   }

   public void e() {
      this.usePictureButton.setVisibility(0);
      this.takePictureButton.setVisibility(8);
   }

   public void f() {
      this.usePictureButton.setVisibility(8);
      this.takePictureButton.setVisibility(0);
   }

   public void g() {
      this.previewImage.setVisibility(8);
      this.frameOverlayView.setOpaqueFrame(false);
   }

   public void h() {
      this.cameraProgress.setVisibility(0);
   }

   public void i() {
      this.cameraProgress.setVisibility(8);
   }

   public void j() {
      this.cameraView.g();
   }

   public void k() {
      super.onBackPressed();
   }

   public void onBackPressed() {
      boolean var1;
      if(this.previewImage.getVisibility() == 0) {
         var1 = true;
      } else {
         var1 = false;
      }

      this.c.a((Object)Boolean.valueOf(var1));
   }

   protected void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.setContentView(2131034214);
      ButterKnife.bind((Activity)this);
      this.thumbnailImageView.setBackgroundResource(2130838017);
      this.thumbnailImageView.setClipToOutline(true);
      android.support.v7.app.a var6 = this.getSupportActionBar();
      if(var6 != null) {
         var6.b(true);
      }

      co.uk.getmondo.signup.identity_verification.a.j var3 = (co.uk.getmondo.signup.identity_verification.a.j)this.getIntent().getSerializableExtra("KEY_VERSION");
      if(var3 == null) {
         throw new RuntimeException("Identity verification flow version required");
      } else {
         co.uk.getmondo.d.i var7 = co.uk.getmondo.d.i.a(this.getIntent().getStringExtra("KEY_COUNTRY_CODE"));
         if(var7 == null) {
            var7 = co.uk.getmondo.d.i.UNITED_KINGDOM;
         }

         IdentityDocumentType var2 = IdentityDocumentType.PASSPORT;
         if(this.getIntent().hasExtra("KEY_ID_DOCUMENT_TYPE")) {
            var2 = (IdentityDocumentType)this.getIntent().getSerializableExtra("KEY_ID_DOCUMENT_TYPE");
         }

         SignupSource var4 = (SignupSource)this.getIntent().getSerializableExtra("KEY_SIGNUP_SOURCE");
         String var5 = this.getIntent().getStringExtra("KEY_FRONT_PHOTO_PATH");
         this.l().a(new co.uk.getmondo.signup.identity_verification.p(var3, var4)).a(new f(var2, var7, var5)).a(this);
         this.a.a((j.a)this);
         this.frameOverlayView.setFrameIdType(var2);
         if(!this.v()) {
            android.support.v4.app.a.a(this, new String[]{"android.permission.CAMERA"}, 1);
         }

      }
   }

   protected void onDestroy() {
      this.a.b();
      super.onDestroy();
   }

   public void onPause() {
      this.cameraView.c();
      super.onPause();
   }

   public void onRequestPermissionsResult(int var1, String[] var2, int[] var3) {
      if(var1 == 1) {
         if(var3.length != 1 || var3[0] != 0) {
            this.e = true;
         }
      } else {
         super.onRequestPermissionsResult(var1, var2, var3);
      }

   }

   public void onResume() {
      super.onResume();
      if(this.v()) {
         this.cameraView.a();
      }

   }

   protected void onResumeFragments() {
      super.onResumeFragments();
      if(this.e) {
         co.uk.getmondo.common.d.a.a(this.getString(2131362302)).show(this.getSupportFragmentManager(), "TAG_ERROR_DIALOG_FRAGMENT");
         this.e = false;
      }

   }
}
