package co.uk.getmondo.signup.identity_verification.id_picture;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.BitmapFactory.Options;
import android.util.SizeF;
import java.io.ByteArrayInputStream;
import java.io.Closeable;
import java.io.File;
import java.io.InputStream;
import java.util.concurrent.Callable;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u0012\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 \u001a2\u00020\u0001:\u0001\u001aB\u0007\b\u0007¢\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0002J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0002J\"\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\f2\u0006\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0004H\u0002J\u0010\u0010\u0011\u001a\u00020\n2\u0006\u0010\u0012\u001a\u00020\u0013H\u0002J\u0010\u0010\u0014\u001a\u00020\n2\u0006\u0010\u0015\u001a\u00020\u0016H\u0002J\u0014\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\f0\u00182\u0006\u0010\u0015\u001a\u00020\u0016J\u0014\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\f0\u00182\u0006\u0010\u0012\u001a\u00020\u0013¨\u0006\u001b"},
   d2 = {"Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentImageResizer;", "", "()V", "calculateRotationMatrix", "Landroid/graphics/Matrix;", "exifInterface", "Landroid/support/media/ExifInterface;", "calculateScalingOptions", "Landroid/graphics/BitmapFactory$Options;", "size", "Landroid/util/SizeF;", "cropAndRotate", "Landroid/graphics/Bitmap;", "bitmap", "shouldCrop", "", "rotationMatrix", "extractPictureSizeFromByteArray", "pictureData", "", "extractPictureSizeFromFile", "file", "Ljava/io/File;", "prepareFallbackImage", "Lio/reactivex/Observable;", "prepareImage", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class u {
   public static final u.a a = new u.a((kotlin.d.b.i)null);

   private final Bitmap a(Bitmap var1, boolean var2, Matrix var3) {
      int var6 = var1.getWidth();
      int var7 = var1.getHeight();
      Bitmap var8;
      if(var7 == var6) {
         var8 = var1;
         if(var3 == null) {
            return var8;
         }
      }

      int var5 = var6;
      int var4 = var7;
      if(var2) {
         var4 = Math.min(var6, var7);
         var5 = var4;
      }

      if(var3 == null) {
         var8 = Bitmap.createBitmap(var1, 0, 0, var5, var4);
         kotlin.d.b.l.a(var8, "Bitmap.createBitmap(bitmap, 0, 0, width, height)");
      } else {
         var8 = Bitmap.createBitmap(var1, 0, 0, var5, var4, var3, true);
         kotlin.d.b.l.a(var8, "Bitmap.createBitmap(bitm…ht, rotationMatrix, true)");
      }

      return var8;
   }

   private final Options a(SizeF var1) {
      double var2 = Math.sqrt(7000000.0D) * 2.0D * (double)var1.getWidth() / (double)(var1.getWidth() + var1.getHeight());
      Options var4 = new Options();
      if((double)var1.getWidth() > var2) {
         var4.inScaled = true;
         var4.inDensity = (int)var1.getWidth();
         if(Math.floor((double)var1.getWidth() / var2) >= (double)2) {
            var4.inSampleSize = 2;
            var4.inTargetDensity = (int)var2 * var4.inSampleSize;
         } else {
            var4.inTargetDensity = (int)var2;
         }
      }

      return var4;
   }

   private final Matrix a(android.support.d.a var1) {
      short var2;
      switch(var1.a("Orientation", 1)) {
      case 3:
         var2 = 180;
         break;
      case 4:
      case 5:
      case 7:
      default:
         var2 = 0;
         break;
      case 6:
         var2 = 90;
         break;
      case 8:
         var2 = 270;
      }

      Matrix var3;
      if(var2 > 0) {
         var3 = new Matrix();
         var3.postRotate((float)var2);
      } else {
         var3 = null;
      }

      return var3;
   }

   private final SizeF b(File var1) {
      Options var2 = new Options();
      var2.inJustDecodeBounds = true;
      BitmapFactory.decodeFile(var1.getPath(), var2);
      return new SizeF((float)var2.outWidth, (float)var2.outHeight);
   }

   private final SizeF b(byte[] var1) {
      Options var2 = new Options();
      var2.inJustDecodeBounds = true;
      BitmapFactory.decodeByteArray(var1, 0, var1.length, var2);
      return new SizeF((float)var2.outWidth, (float)var2.outHeight);
   }

   public final io.reactivex.n a(final File var1) {
      kotlin.d.b.l.b(var1, "file");
      io.reactivex.n var2 = io.reactivex.n.fromCallable((Callable)(new Callable() {
         public final Bitmap a() {
            SizeF var1x = u.this.b(var1);
            Bitmap var4 = BitmapFactory.decodeFile(var1.getPath(), u.this.a(var1x));
            Matrix var2 = u.this.a(new android.support.d.a(var1.getPath()));
            u var3 = u.this;
            kotlin.d.b.l.a(var4, "bitmap");
            return var3.a(var4, false, var2);
         }

         // $FF: synthetic method
         public Object call() {
            return this.a();
         }
      }));
      kotlin.d.b.l.a(var2, "Observable.fromCallable … false, matrix)\n        }");
      return var2;
   }

   public final io.reactivex.n a(final byte[] var1) {
      kotlin.d.b.l.b(var1, "pictureData");
      io.reactivex.n var2 = io.reactivex.n.fromCallable((Callable)(new Callable() {
         public final Bitmap a() {
            boolean var1x = false;
            Options var2 = u.this.a(u.this.b(var1));
            Bitmap var19 = BitmapFactory.decodeByteArray(var1, 0, var1.length, var2);
            Matrix var3 = (Matrix)null;
            Closeable var20 = (Closeable)(new ByteArrayInputStream(var1));
            boolean var14 = false;

            Matrix var23;
            label102: {
               Exception var4;
               try {
                  var14 = true;
                  ByteArrayInputStream var5 = (ByteArrayInputStream)var20;
                  android.support.d.a var22 = new android.support.d.a((InputStream)var5);
                  var23 = u.this.a(var22);
                  kotlin.n var24 = kotlin.n.a;
                  var14 = false;
                  break label102;
               } catch (Exception var17) {
                  var4 = var17;
                  var14 = false;
               } finally {
                  if(var14) {
                     if(!var1x) {
                        var20.close();
                     }

                  }
               }

               try {
                  try {
                     var20.close();
                  } catch (Exception var15) {
                     ;
                  }

                  throw (Throwable)var4;
               } finally {
                  ;
               }
            }

            var20.close();
            u var21 = u.this;
            kotlin.d.b.l.a(var19, "bitmap");
            return var21.a(var19, true, var23);
         }

         // $FF: synthetic method
         public Object call() {
            return this.a();
         }
      }));
      kotlin.d.b.l.a(var2, "Observable.fromCallable …rotationMatrix)\n        }");
      return var2;
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0006\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000¨\u0006\u0005"},
      d2 = {"Lco/uk/getmondo/signup/identity_verification/id_picture/DocumentImageResizer$Companion;", "", "()V", "MAX_IMAGE_PIXEL", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var1) {
         this();
      }
   }
}
