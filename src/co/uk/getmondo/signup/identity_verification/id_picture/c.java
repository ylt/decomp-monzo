package co.uk.getmondo.signup.identity_verification.id_picture;

// $FF: synthetic class
final class c implements io.reactivex.c.f {
   private final DocumentCameraActivity a;
   private final com.google.android.cameraview.CameraView.a b;

   private c(DocumentCameraActivity var1, com.google.android.cameraview.CameraView.a var2) {
      this.a = var1;
      this.b = var2;
   }

   public static io.reactivex.c.f a(DocumentCameraActivity var0, com.google.android.cameraview.CameraView.a var1) {
      return new c(var0, var1);
   }

   public void a() {
      DocumentCameraActivity.a(this.a, this.b);
   }
}
