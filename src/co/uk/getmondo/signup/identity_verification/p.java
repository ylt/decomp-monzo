package co.uk.getmondo.signup.identity_verification;

import co.uk.getmondo.api.IdentityVerificationApi;
import co.uk.getmondo.api.ae;
import co.uk.getmondo.api.model.signup.SignupSource;

public class p {
   private final co.uk.getmondo.signup.identity_verification.a.j a;
   private final SignupSource b;

   public p(co.uk.getmondo.signup.identity_verification.a.j var1, SignupSource var2) {
      this.a = var1;
      this.b = var2;
   }

   co.uk.getmondo.signup.identity_verification.a.e a(co.uk.getmondo.signup.identity_verification.a.h var1, IdentityVerificationApi var2, ae var3, co.uk.getmondo.common.i var4, SignupSource var5) {
      Object var6;
      if(this.a == co.uk.getmondo.signup.identity_verification.a.j.a) {
         var6 = new co.uk.getmondo.signup.identity_verification.a.k(var1, var2, var3, var4);
      } else {
         var6 = new co.uk.getmondo.signup.identity_verification.a.l(var1, var2, var3, var4, var5);
      }

      return (co.uk.getmondo.signup.identity_verification.a.e)var6;
   }

   co.uk.getmondo.signup.identity_verification.a.j a() {
      return this.a;
   }

   SignupSource b() {
      return this.b;
   }
}
