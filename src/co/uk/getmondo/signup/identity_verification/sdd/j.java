package co.uk.getmondo.signup.identity_verification.sdd;

import co.uk.getmondo.api.model.tracking.Impression;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000b\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u001f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000ej\u0002\b\u000fj\u0002\b\u0010j\u0002\b\u0011¨\u0006\u0012"},
   d2 = {"Lco/uk/getmondo/signup/identity_verification/sdd/SddUpgradeLevelType;", "", "impression", "Lco/uk/getmondo/api/model/tracking/Impression;", "from", "", "onboardingFrom", "Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;", "(Ljava/lang/String;ILco/uk/getmondo/api/model/tracking/Impression;Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;)V", "getFrom", "()Ljava/lang/String;", "getImpression", "()Lco/uk/getmondo/api/model/tracking/Impression;", "getOnboardingFrom", "()Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;", "ENCOURAGE", "WARNING", "BLOCKED", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public enum j {
   a,
   b,
   c;

   private final Impression e;
   private final String f;
   private final Impression.KycFrom g;

   static {
      j var2 = new j("ENCOURAGE", 0, Impression.Companion.az(), "kyc-june26", Impression.KycFrom.KYC_SDD_ENCOURAGE);
      a = var2;
      j var1 = new j("WARNING", 1, Impression.Companion.aA(), "kyc-june26-warning", Impression.KycFrom.KYC_SDD_WARNING);
      b = var1;
      j var0 = new j("BLOCKED", 2, Impression.Companion.aB(), "kyc-june26-blocked", Impression.KycFrom.KYC_SDD_BLOCKED);
      c = var0;
   }

   protected j(Impression var3, String var4, Impression.KycFrom var5) {
      l.b(var3, "impression");
      l.b(var4, "from");
      l.b(var5, "onboardingFrom");
      super(var1, var2);
      this.e = var3;
      this.f = var4;
      this.g = var5;
   }

   public final Impression a() {
      return this.e;
   }

   public final String b() {
      return this.f;
   }

   public final Impression.KycFrom c() {
      return this.g;
   }
}
