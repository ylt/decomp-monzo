package co.uk.getmondo.signup.identity_verification.sdd;

// $FF: synthetic class
final class g implements io.reactivex.c.g {
   private final f a;
   private final f.a b;

   private g(f var1, f.a var2) {
      this.a = var1;
      this.b = var2;
   }

   public static io.reactivex.c.g a(f var0, f.a var1) {
      return new g(var0, var1);
   }

   public void a(Object var1) {
      f.b(this.a, this.b, var1);
   }
}
