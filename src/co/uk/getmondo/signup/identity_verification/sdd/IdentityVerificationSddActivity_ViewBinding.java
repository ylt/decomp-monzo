package co.uk.getmondo.signup.identity_verification.sdd;

import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class IdentityVerificationSddActivity_ViewBinding implements Unbinder {
   private IdentityVerificationSddActivity a;

   public IdentityVerificationSddActivity_ViewBinding(IdentityVerificationSddActivity var1, View var2) {
      this.a = var1;
      var1.toolbar = (Toolbar)Utils.findRequiredViewAsType(var2, 2131820798, "field 'toolbar'", Toolbar.class);
      var1.sddTitleTextView = (TextView)Utils.findRequiredViewAsType(var2, 2131820970, "field 'sddTitleTextView'", TextView.class);
      var1.sddBodyTextView = (TextView)Utils.findRequiredViewAsType(var2, 2131820971, "field 'sddBodyTextView'", TextView.class);
      var1.sddVerifyButton = (Button)Utils.findRequiredViewAsType(var2, 2131820972, "field 'sddVerifyButton'", Button.class);
      var1.sddNotNowButton = (Button)Utils.findRequiredViewAsType(var2, 2131820973, "field 'sddNotNowButton'", Button.class);
   }

   public void unbind() {
      IdentityVerificationSddActivity var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.toolbar = null;
         var1.sddTitleTextView = null;
         var1.sddBodyTextView = null;
         var1.sddVerifyButton = null;
         var1.sddNotNowButton = null;
      }
   }
}
