package co.uk.getmondo.signup.identity_verification.sdd;

import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.common.k.p;
import io.reactivex.n;

class f extends co.uk.getmondo.common.ui.b {
   private final j c;
   private final co.uk.getmondo.common.a d;
   private final String e;

   f(j var1, co.uk.getmondo.common.a var2, String var3) {
      this.c = var1;
      this.d = var2;
      this.e = var3;
   }

   // $FF: synthetic method
   static void a(f var0, f.a var1, Object var2) throws Exception {
      var0.d.a(Impression.a(var0.c));
      var1.a("https://monzo.com/-webviews/identity-upgrade");
   }

   // $FF: synthetic method
   static void b(f var0, f.a var1, Object var2) throws Exception {
      if(p.d(var0.e)) {
         var1.a(var0.c.c());
      } else {
         var1.b(var0.c.c());
      }

   }

   public void a(f.a var1) {
      super.a((co.uk.getmondo.common.ui.f)var1);
      this.d.a(this.c.a());
      if(this.c == j.b) {
         var1.c();
      } else if(this.c == j.c) {
         var1.d();
      }

      this.a((io.reactivex.b.b)var1.a().subscribe(g.a(this, var1)));
      this.a((io.reactivex.b.b)var1.b().subscribe(h.a(this, var1)));
   }

   interface a extends co.uk.getmondo.common.ui.f {
      n a();

      void a(Impression.KycFrom var1);

      void a(String var1);

      n b();

      void b(Impression.KycFrom var1);

      void c();

      void d();
   }
}
