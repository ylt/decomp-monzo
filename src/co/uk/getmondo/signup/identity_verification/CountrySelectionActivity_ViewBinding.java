package co.uk.getmondo.signup.identity_verification;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class CountrySelectionActivity_ViewBinding implements Unbinder {
   private CountrySelectionActivity a;

   public CountrySelectionActivity_ViewBinding(CountrySelectionActivity var1, View var2) {
      this.a = var1;
      var1.recyclerView = (RecyclerView)Utils.findRequiredViewAsType(var2, 2131820862, "field 'recyclerView'", RecyclerView.class);
   }

   public void unbind() {
      CountrySelectionActivity var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.recyclerView = null;
      }
   }
}
