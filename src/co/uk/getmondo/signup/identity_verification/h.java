package co.uk.getmondo.signup.identity_verification;

import co.uk.getmondo.api.model.tracking.Impression;

public final class h implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final b.a b;
   private final javax.a.a c;
   private final javax.a.a d;
   private final javax.a.a e;
   private final javax.a.a f;
   private final javax.a.a g;
   private final javax.a.a h;
   private final javax.a.a i;
   private final javax.a.a j;
   private final javax.a.a k;
   private final javax.a.a l;

   static {
      boolean var0;
      if(!h.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   public h(b.a var1, javax.a.a var2, javax.a.a var3, javax.a.a var4, javax.a.a var5, javax.a.a var6, javax.a.a var7, javax.a.a var8, javax.a.a var9, javax.a.a var10, javax.a.a var11) {
      if(!a && var1 == null) {
         throw new AssertionError();
      } else {
         this.b = var1;
         if(!a && var2 == null) {
            throw new AssertionError();
         } else {
            this.c = var2;
            if(!a && var3 == null) {
               throw new AssertionError();
            } else {
               this.d = var3;
               if(!a && var4 == null) {
                  throw new AssertionError();
               } else {
                  this.e = var4;
                  if(!a && var5 == null) {
                     throw new AssertionError();
                  } else {
                     this.f = var5;
                     if(!a && var6 == null) {
                        throw new AssertionError();
                     } else {
                        this.g = var6;
                        if(!a && var7 == null) {
                           throw new AssertionError();
                        } else {
                           this.h = var7;
                           if(!a && var8 == null) {
                              throw new AssertionError();
                           } else {
                              this.i = var8;
                              if(!a && var9 == null) {
                                 throw new AssertionError();
                              } else {
                                 this.j = var9;
                                 if(!a && var10 == null) {
                                    throw new AssertionError();
                                 } else {
                                    this.k = var10;
                                    if(!a && var11 == null) {
                                       throw new AssertionError();
                                    } else {
                                       this.l = var11;
                                    }
                                 }
                              }
                           }
                        }
                     }
                  }
               }
            }
         }
      }
   }

   public static b.a.b a(b.a var0, javax.a.a var1, javax.a.a var2, javax.a.a var3, javax.a.a var4, javax.a.a var5, javax.a.a var6, javax.a.a var7, javax.a.a var8, javax.a.a var9, javax.a.a var10) {
      return new h(var0, var1, var2, var3, var4, var5, var6, var7, var8, var9, var10);
   }

   public g a() {
      return (g)b.a.c.a(this.b, new g((io.reactivex.u)this.c.b(), (io.reactivex.u)this.d.b(), (co.uk.getmondo.common.e.a)this.e.b(), (co.uk.getmondo.signup.identity_verification.a.e)this.f.b(), (co.uk.getmondo.signup.identity_verification.a.c)this.g.b(), (co.uk.getmondo.common.a)this.h.b(), (co.uk.getmondo.signup.identity_verification.a.j)this.i.b(), (Impression.KycFrom)this.j.b(), (String)this.k.b(), ((Boolean)this.l.b()).booleanValue()));
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
