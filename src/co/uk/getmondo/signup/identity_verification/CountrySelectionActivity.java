package co.uk.getmondo.signup.identity_verification;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.uk.getmondo.api.model.identity_verification.IdentityDocumentType;
import co.uk.getmondo.api.model.tracking.Impression;

public class CountrySelectionActivity extends co.uk.getmondo.common.activities.b {
   co.uk.getmondo.common.a a;
   @BindView(2131820862)
   RecyclerView recyclerView;

   public static Intent a(Context var0, IdentityDocumentType var1) {
      return (new Intent(var0, CountrySelectionActivity.class)).putExtra("KEY_DOCUMENT_TYPE", var1);
   }

   public static IdentityDocumentType a(Intent var0) {
      return (IdentityDocumentType)var0.getSerializableExtra("KEY_DOCUMENT_TYPE");
   }

   // $FF: synthetic method
   static kotlin.n a(CountrySelectionActivity var0, IdentityDocumentType var1, co.uk.getmondo.d.i var2) {
      var0.setResult(-1, (new Intent()).putExtra("KEY_COUNTRY", var2).putExtra("KEY_DOCUMENT_TYPE", var1));
      var0.finish();
      return kotlin.n.a;
   }

   public static co.uk.getmondo.d.i b(Intent var0) {
      return (co.uk.getmondo.d.i)var0.getParcelableExtra("KEY_COUNTRY");
   }

   public void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.setContentView(2131034156);
      ButterKnife.bind((Activity)this);
      IdentityDocumentType var2 = (IdentityDocumentType)this.getIntent().getSerializableExtra("KEY_DOCUMENT_TYPE");
      if(var2 == null) {
         throw new IllegalStateException("Identity document type is required");
      } else {
         this.l().a(this);
         this.setTitle(this.getString(2131362257, new Object[]{co.uk.getmondo.common.k.p.i(this.getString(var2.c()))}));
         co.uk.getmondo.signup_old.a var3 = new co.uk.getmondo.signup_old.a(co.uk.getmondo.d.i.j());
         this.recyclerView.setLayoutManager(new LinearLayoutManager(this));
         this.recyclerView.setAdapter(var3);
         this.recyclerView.setHasFixedSize(true);
         var3.a(b.a(this, var2));
         this.a.a(Impression.i(var2.a()));
      }
   }
}
