package co.uk.getmondo.signup.identity_verification;

import co.uk.getmondo.api.model.identity_verification.IdentityDocumentType;

// $FF: synthetic class
final class b implements kotlin.d.a.b {
   private final CountrySelectionActivity a;
   private final IdentityDocumentType b;

   private b(CountrySelectionActivity var1, IdentityDocumentType var2) {
      this.a = var1;
      this.b = var2;
   }

   public static kotlin.d.a.b a(CountrySelectionActivity var0, IdentityDocumentType var1) {
      return new b(var0, var1);
   }

   public Object a(Object var1) {
      return CountrySelectionActivity.a(this.a, this.b, (co.uk.getmondo.d.i)var1);
   }
}
