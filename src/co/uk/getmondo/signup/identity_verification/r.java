package co.uk.getmondo.signup.identity_verification;

public final class r implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final p b;

   static {
      boolean var0;
      if(!r.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   public r(p var1) {
      if(!a && var1 == null) {
         throw new AssertionError();
      } else {
         this.b = var1;
      }
   }

   public static b.a.b a(p var0) {
      return new r(var0);
   }

   public co.uk.getmondo.signup.identity_verification.a.j a() {
      return (co.uk.getmondo.signup.identity_verification.a.j)b.a.d.a(this.b.a(), "Cannot return null from a non-@Nullable @Provides method");
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
