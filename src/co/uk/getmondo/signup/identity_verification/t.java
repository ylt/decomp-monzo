package co.uk.getmondo.signup.identity_verification;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.common.pager.GenericPagerAdapter;
import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.TypeCastException;
import me.relex.circleindicator.CircleIndicator;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 -2\u00020\u0001:\u0002-.B\u0005¢\u0006\u0002\u0010\u0002J\u0013\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00180\u0017H\u0002¢\u0006\u0002\u0010\u0019J\u0006\u0010\u001a\u001a\u00020\u001bJ\u0006\u0010\u001c\u001a\u00020\u001dJ\u0010\u0010\u001e\u001a\u00020\u001d2\u0006\u0010\u001f\u001a\u00020 H\u0016J\u0012\u0010!\u001a\u00020\u001d2\b\u0010\"\u001a\u0004\u0018\u00010#H\u0016J&\u0010$\u001a\u0004\u0018\u00010%2\u0006\u0010&\u001a\u00020'2\b\u0010(\u001a\u0004\u0018\u00010)2\b\u0010\"\u001a\u0004\u0018\u00010#H\u0016J\b\u0010*\u001a\u00020\u001dH\u0016J\u001c\u0010+\u001a\u00020\u001d2\b\u0010,\u001a\u0004\u0018\u00010%2\b\u0010\"\u001a\u0004\u0018\u00010#H\u0016R\u001b\u0010\u0003\u001a\u00020\u00048BX\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0007\u0010\b\u001a\u0004\b\u0005\u0010\u0006R\u001b\u0010\t\u001a\u00020\n8BX\u0082\u0084\u0002¢\u0006\f\n\u0004\b\r\u0010\b\u001a\u0004\b\u000b\u0010\fR\u000e\u0010\u000e\u001a\u00020\u000fX\u0082.¢\u0006\u0002\n\u0000R\u001e\u0010\u0010\u001a\u00020\u00118\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u0013\"\u0004\b\u0014\u0010\u0015¨\u0006/"},
   d2 = {"Lco/uk/getmondo/signup/identity_verification/IdentityVerificationOnboardingFragment;", "Lco/uk/getmondo/common/fragments/BaseFragment;", "()V", "from", "Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;", "getFrom", "()Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;", "from$delegate", "Lkotlin/Lazy;", "identityVerificationVersion", "Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationVersion;", "getIdentityVerificationVersion", "()Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationVersion;", "identityVerificationVersion$delegate", "listener", "Lco/uk/getmondo/signup/identity_verification/IdentityVerificationOnboardingFragment$StepListener;", "pageViewTracker", "Lco/uk/getmondo/common/pager/PageViewTracker;", "getPageViewTracker", "()Lco/uk/getmondo/common/pager/PageViewTracker;", "setPageViewTracker", "(Lco/uk/getmondo/common/pager/PageViewTracker;)V", "buildOnboardingPages", "", "Lco/uk/getmondo/common/pager/Page;", "()[Lco/uk/getmondo/common/pager/Page;", "canGoBack", "", "goBack", "", "onAttach", "context", "Landroid/content/Context;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "onDestroy", "onViewCreated", "view", "Companion", "StepListener", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class t extends co.uk.getmondo.common.f.a {
   // $FF: synthetic field
   static final kotlin.reflect.l[] a = new kotlin.reflect.l[]{(kotlin.reflect.l)kotlin.d.b.y.a(new kotlin.d.b.w(kotlin.d.b.y.a(t.class), "identityVerificationVersion", "getIdentityVerificationVersion()Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationVersion;")), (kotlin.reflect.l)kotlin.d.b.y.a(new kotlin.d.b.w(kotlin.d.b.y.a(t.class), "from", "getFrom()Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;"))};
   public static final t.a d = new t.a((kotlin.d.b.i)null);
   public co.uk.getmondo.common.pager.h c;
   private final kotlin.c e = kotlin.d.a((kotlin.d.a.a)(new kotlin.d.a.a() {
      public final co.uk.getmondo.signup.identity_verification.a.j b() {
         Serializable var1 = t.this.getArguments().getSerializable("KEY_VERSION");
         if(var1 == null) {
            throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.signup.identity_verification.data.IdentityVerificationVersion");
         } else {
            return (co.uk.getmondo.signup.identity_verification.a.j)var1;
         }
      }

      // $FF: synthetic method
      public Object v_() {
         return this.b();
      }
   }));
   private final kotlin.c f = kotlin.d.a((kotlin.d.a.a)(new kotlin.d.a.a() {
      public final Impression.KycFrom b() {
         Serializable var1 = t.this.getArguments().getSerializable("KEY_ENTRY_POINT");
         if(var1 == null) {
            throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.api.model.tracking.Impression.KycFrom");
         } else {
            return (Impression.KycFrom)var1;
         }
      }

      // $FF: synthetic method
      public Object v_() {
         return this.b();
      }
   }));
   private t.b g;
   private HashMap h;

   // $FF: synthetic method
   public static final t.b a(t var0) {
      t.b var1 = var0.g;
      if(var1 == null) {
         kotlin.d.b.l.b("listener");
      }

      return var1;
   }

   private final co.uk.getmondo.signup.identity_verification.a.j d() {
      kotlin.c var2 = this.e;
      kotlin.reflect.l var1 = a[0];
      return (co.uk.getmondo.signup.identity_verification.a.j)var2.a();
   }

   private final Impression.KycFrom e() {
      kotlin.c var2 = this.f;
      kotlin.reflect.l var1 = a[1];
      return (Impression.KycFrom)var2.a();
   }

   private final co.uk.getmondo.common.pager.f[] f() {
      int var1;
      if(this.d() == co.uk.getmondo.signup.identity_verification.a.j.a) {
         var1 = 2131362311;
      } else {
         var1 = 2131362310;
      }

      Object[] var2 = (Object[])(new co.uk.getmondo.common.pager.f[]{(co.uk.getmondo.common.pager.f)(new co.uk.getmondo.common.pager.d(2130837955, 2131362314, var1, Impression.Companion.a(this.e()))), (co.uk.getmondo.common.pager.f)(new co.uk.getmondo.common.pager.d(2130837956, 2131362315, 2131362312, Impression.Companion.b(this.e()))), (co.uk.getmondo.common.pager.f)(new co.uk.getmondo.common.pager.d(2130837957, 2131362316, 2131362313, Impression.Companion.c(this.e())))});
      return (co.uk.getmondo.common.pager.f[])var2;
   }

   public View a(int var1) {
      if(this.h == null) {
         this.h = new HashMap();
      }

      View var3 = (View)this.h.get(Integer.valueOf(var1));
      View var2 = var3;
      if(var3 == null) {
         var2 = this.getView();
         if(var2 == null) {
            var2 = null;
         } else {
            var2 = var2.findViewById(var1);
            this.h.put(Integer.valueOf(var1), var2);
         }
      }

      return var2;
   }

   public final boolean a() {
      boolean var1;
      if(((ViewPager)this.a(co.uk.getmondo.c.a.idvOnboardingViewPager)).getCurrentItem() > 0) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public final void b() {
      ((ViewPager)this.a(co.uk.getmondo.c.a.idvOnboardingViewPager)).a(((ViewPager)this.a(co.uk.getmondo.c.a.idvOnboardingViewPager)).getCurrentItem() - 1, true);
   }

   public void c() {
      if(this.h != null) {
         this.h.clear();
      }

   }

   public void onAttach(Context var1) {
      kotlin.d.b.l.b(var1, "context");
      super.onAttach(var1);
      if(var1 instanceof t.b) {
         this.g = (t.b)var1;
      } else {
         throw (Throwable)(new IllegalStateException("Activity must implement StepListener"));
      }
   }

   public void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.B().a(this);
   }

   public View onCreateView(LayoutInflater var1, ViewGroup var2, Bundle var3) {
      kotlin.d.b.l.b(var1, "inflater");
      return var1.inflate(2131034272, var2, false);
   }

   public void onDestroy() {
      co.uk.getmondo.common.pager.h var1 = this.c;
      if(var1 == null) {
         kotlin.d.b.l.b("pageViewTracker");
      }

      var1.a();
      super.onDestroy();
   }

   // $FF: synthetic method
   public void onDestroyView() {
      super.onDestroyView();
      this.c();
   }

   public void onViewCreated(View var1, Bundle var2) {
      super.onViewCreated(var1, var2);
      ViewPager var3 = (ViewPager)this.a(co.uk.getmondo.c.a.idvOnboardingViewPager);
      co.uk.getmondo.common.pager.f[] var5 = this.f();
      var3.setAdapter((android.support.v4.view.p)(new GenericPagerAdapter((co.uk.getmondo.common.pager.f[])Arrays.copyOf(var5, var5.length))));
      co.uk.getmondo.common.pager.h var4 = this.c;
      if(var4 == null) {
         kotlin.d.b.l.b("pageViewTracker");
      }

      var4.a((ViewPager)this.a(co.uk.getmondo.c.a.idvOnboardingViewPager));
      ((CircleIndicator)this.a(co.uk.getmondo.c.a.idvOnboardingViewPagerIndicator)).setViewPager((ViewPager)this.a(co.uk.getmondo.c.a.idvOnboardingViewPager));
      ((Button)this.a(co.uk.getmondo.c.a.idvOnboardingNextButton)).setOnClickListener((OnClickListener)(new OnClickListener() {
         public final void onClick(View var1) {
            int var2 = ((ViewPager)t.this.a(co.uk.getmondo.c.a.idvOnboardingViewPager)).getCurrentItem() + 1;
            if(var2 < ((ViewPager)t.this.a(co.uk.getmondo.c.a.idvOnboardingViewPager)).getAdapter().b()) {
               ((ViewPager)t.this.a(co.uk.getmondo.c.a.idvOnboardingViewPager)).a(var2, true);
            } else {
               t.a(t.this).k();
            }

         }
      }));
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0016\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bR\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000¨\u0006\f"},
      d2 = {"Lco/uk/getmondo/signup/identity_verification/IdentityVerificationOnboardingFragment$Companion;", "", "()V", "KEY_ENTRY_POINT", "", "KEY_VERSION", "newInstance", "Landroid/support/v4/app/Fragment;", "version", "Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationVersion;", "from", "Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var1) {
         this();
      }

      public final Fragment a(co.uk.getmondo.signup.identity_verification.a.j var1, Impression.KycFrom var2) {
         kotlin.d.b.l.b(var1, "version");
         kotlin.d.b.l.b(var2, "from");
         t var4 = new t();
         Bundle var3 = new Bundle();
         var3.putSerializable("KEY_VERSION", (Serializable)var1);
         var3.putSerializable("KEY_ENTRY_POINT", (Serializable)var2);
         var4.setArguments(var3);
         return (Fragment)var4;
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&¨\u0006\u0004"},
      d2 = {"Lco/uk/getmondo/signup/identity_verification/IdentityVerificationOnboardingFragment$StepListener;", "", "onOnboardingComplete", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface b {
      void k();
   }
}
