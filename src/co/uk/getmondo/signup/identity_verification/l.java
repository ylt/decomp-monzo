package co.uk.getmondo.signup.identity_verification;

import co.uk.getmondo.api.model.tracking.Impression;

public final class l implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final j b;

   static {
      boolean var0;
      if(!l.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   public l(j var1) {
      if(!a && var1 == null) {
         throw new AssertionError();
      } else {
         this.b = var1;
      }
   }

   public static b.a.b a(j var0) {
      return new l(var0);
   }

   public Impression.KycFrom a() {
      return (Impression.KycFrom)b.a.d.a(this.b.a(), "Cannot return null from a non-@Nullable @Provides method");
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
