package co.uk.getmondo.signup.identity_verification;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.main.HomeActivity;

public class IdentityApprovedActivity extends co.uk.getmondo.common.activities.b {
   co.uk.getmondo.common.a a;

   public static void a(Context var0) {
      var0.startActivity(b(var0));
   }

   public static Intent b(Context var0) {
      return new Intent(var0, IdentityApprovedActivity.class);
   }

   protected void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.l().a(this);
      this.setContentView(2131034178);
      ButterKnife.bind((Activity)this);
      this.a.a(Impression.E());
   }

   @OnClick({2131820965})
   public void onReturnToHomeClicked() {
      this.finish();
      HomeActivity.a((Context)this);
   }
}
