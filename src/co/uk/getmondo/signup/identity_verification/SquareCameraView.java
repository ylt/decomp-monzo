package co.uk.getmondo.signup.identity_verification;

import android.content.Context;
import android.util.AttributeSet;
import com.google.android.cameraview.CameraView;

public class SquareCameraView extends CameraView {
   public SquareCameraView(Context var1) {
      super(var1);
   }

   public SquareCameraView(Context var1, AttributeSet var2) {
      super(var1, var2);
   }

   public SquareCameraView(Context var1, AttributeSet var2, int var3) {
      super(var1, var2, var3);
   }

   protected void onMeasure(int var1, int var2) {
      super.onMeasure(var1, var1);
   }
}
