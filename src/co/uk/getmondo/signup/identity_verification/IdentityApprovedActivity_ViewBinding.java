package co.uk.getmondo.signup.identity_verification;

import android.view.View;
import android.view.View.OnClickListener;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;

public class IdentityApprovedActivity_ViewBinding implements Unbinder {
   private IdentityApprovedActivity a;
   private View b;

   public IdentityApprovedActivity_ViewBinding(final IdentityApprovedActivity var1, View var2) {
      this.a = var1;
      var2 = Utils.findRequiredView(var2, 2131820965, "method 'onReturnToHomeClicked'");
      this.b = var2;
      var2.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View var1x) {
            var1.onReturnToHomeClicked();
         }
      });
   }

   public void unbind() {
      if(this.a == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         this.b.setOnClickListener((OnClickListener)null);
         this.b = null;
      }
   }
}
