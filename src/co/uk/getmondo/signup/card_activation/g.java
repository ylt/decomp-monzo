package co.uk.getmondo.signup.card_activation;

import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.settings.k;
import co.uk.getmondo.signup.j;
import kotlin.Metadata;
import kotlin.n;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u000bB\u0017\b\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J\u0010\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0002H\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\f"},
   d2 = {"Lco/uk/getmondo/signup/card_activation/CardOnItsWayPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/signup/card_activation/CardOnItsWayPresenter$View;", "analyticsService", "Lco/uk/getmondo/common/AnalyticsService;", "localUserSettingStorage", "Lco/uk/getmondo/settings/LocalUserSettingStorage;", "(Lco/uk/getmondo/common/AnalyticsService;Lco/uk/getmondo/settings/LocalUserSettingStorage;)V", "register", "", "view", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class g extends co.uk.getmondo.common.ui.b {
   private final co.uk.getmondo.common.a c;
   private final k d;

   public g(co.uk.getmondo.common.a var1, k var2) {
      l.b(var1, "analyticsService");
      l.b(var2, "localUserSettingStorage");
      super();
      this.c = var1;
      this.d = var2;
   }

   public void a(final g.a var1) {
      l.b(var1, "view");
      super.a((co.uk.getmondo.common.ui.f)var1);
      this.c.a(Impression.Companion.aR());
      final boolean var2 = this.d.d();
      io.reactivex.b.a var4 = this.b;
      io.reactivex.b.b var3 = var1.d().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(n var1x) {
            if(g.this.d.d()) {
               var1.f();
            } else {
               if(l.a(var1.c(), j.a)) {
                  var1.g();
               }

               g.this.d.c(true);
            }

         }
      }));
      l.a(var3, "view.onRefresh\n         …      }\n                }");
      this.b = co.uk.getmondo.common.j.f.a(var4, var3);
      io.reactivex.b.a var6 = this.b;
      io.reactivex.b.b var5 = var1.e().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(n var1x) {
            if(l.a(var1.c(), j.a) && !var2) {
               var1.i();
            } else {
               var1.h();
            }

         }
      }));
      l.a(var5, "view.onMyCardArrivedClic…      }\n                }");
      this.b = co.uk.getmondo.common.j.f.a(var6, var5);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\t\bf\u0018\u00002\u00020\u00012\u00020\u0002J\b\u0010\u000e\u001a\u00020\tH&J\b\u0010\u000f\u001a\u00020\tH&J\b\u0010\u0010\u001a\u00020\tH&J\b\u0010\u0011\u001a\u00020\tH&R\u0012\u0010\u0003\u001a\u00020\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0005\u0010\u0006R\u0018\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\bX¦\u0004¢\u0006\u0006\u001a\u0004\b\n\u0010\u000bR\u0018\u0010\f\u001a\b\u0012\u0004\u0012\u00020\t0\bX¦\u0004¢\u0006\u0006\u001a\u0004\b\r\u0010\u000b¨\u0006\u0012"},
      d2 = {"Lco/uk/getmondo/signup/card_activation/CardOnItsWayPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "entryPoint", "Lco/uk/getmondo/signup/SignupEntryPoint;", "getEntryPoint", "()Lco/uk/getmondo/signup/SignupEntryPoint;", "onMyCardArrivedClicked", "Lio/reactivex/Observable;", "", "getOnMyCardArrivedClicked", "()Lio/reactivex/Observable;", "onRefresh", "getOnRefresh", "openCardActivation", "openHome", "showBackToMonzoFeed", "showMyCardArrived", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      j c();

      io.reactivex.n d();

      io.reactivex.n e();

      void f();

      void g();

      void h();

      void i();
   }
}
