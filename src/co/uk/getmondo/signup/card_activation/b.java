package co.uk.getmondo.signup.card_activation;

import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.signup.i;
import io.reactivex.n;
import io.reactivex.u;
import kotlin.Metadata;
import kotlin.d.b.l;
import kotlin.h.j;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u0003\n\u0002\b\u0003\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0013B3\b\u0007\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b¢\u0006\u0002\u0010\fJ\u0018\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00022\u0006\u0010\u0010\u001a\u00020\u0011H\u0002J\u0010\u0010\u0012\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0002H\u0016R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0014"},
   d2 = {"Lco/uk/getmondo/signup/card_activation/ActivateCardPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/signup/card_activation/ActivateCardPresenter$View;", "uiScheduler", "Lio/reactivex/Scheduler;", "ioScheduler", "cardActivationManager", "Lco/uk/getmondo/signup/card_activation/CardActivationManager;", "apiErrorHandler", "Lco/uk/getmondo/common/errors/ApiErrorHandler;", "analyticsService", "Lco/uk/getmondo/common/AnalyticsService;", "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/signup/card_activation/CardActivationManager;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/common/AnalyticsService;)V", "handleError", "", "view", "it", "", "register", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class b extends co.uk.getmondo.common.ui.b {
   private final u c;
   private final u d;
   private final d e;
   private final co.uk.getmondo.common.e.a f;
   private final co.uk.getmondo.common.a g;

   public b(u var1, u var2, d var3, co.uk.getmondo.common.e.a var4, co.uk.getmondo.common.a var5) {
      l.b(var1, "uiScheduler");
      l.b(var2, "ioScheduler");
      l.b(var3, "cardActivationManager");
      l.b(var4, "apiErrorHandler");
      l.b(var5, "analyticsService");
      super();
      this.c = var1;
      this.d = var2;
      this.e = var3;
      this.f = var4;
      this.g = var5;
   }

   private final void a(b.a var1, Throwable var2) {
      var1.e();
      if(!i.a.a(var2, (i.a)var1) && !this.f.a(var2, (co.uk.getmondo.common.e.a.a)var1)) {
         var1.b(2131362198);
      }

   }

   public void a(final b.a var1) {
      l.b(var1, "view");
      super.a((co.uk.getmondo.common.ui.f)var1);
      this.g.a(Impression.Companion.J());
      io.reactivex.b.a var2 = this.b;
      io.reactivex.b.b var3 = var1.a().flatMapMaybe((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.h a(String var1x) {
            l.b(var1x, "cardPan");
            return b.this.e.a(j.a(var1x, " ", "", false, 4, (Object)null)).a((Object)co.uk.getmondo.common.b.a.a).e().b(b.this.d).a(b.this.c).b((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(io.reactivex.b.b var1x) {
                  var1.d();
               }
            })).a((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(Throwable var1x) {
                  b var3 = b.this;
                  b.a var2 = var1;
                  l.a(var1x, "it");
                  var3.a(var2, var1x);
               }
            })).a((io.reactivex.c.b)(new io.reactivex.c.b() {
               public final void a(co.uk.getmondo.common.b.a var1x, Throwable var2) {
                  var1.e();
               }
            })).f();
         }
      })).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(co.uk.getmondo.common.b.a var1x) {
            var1.c();
         }
      }));
      l.a(var3, "view.onCardPanEntered\n  … { view.cardActivated() }");
      this.b = co.uk.getmondo.common.j.f.a(var2, var3);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0003\bf\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003J\b\u0010\t\u001a\u00020\nH&J\b\u0010\u000b\u001a\u00020\nH&J\b\u0010\f\u001a\u00020\nH&R\u0018\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0007\u0010\b¨\u0006\r"},
      d2 = {"Lco/uk/getmondo/signup/card_activation/ActivateCardPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "Lco/uk/getmondo/signup/SignupApiErrorHandler$View;", "onCardPanEntered", "Lio/reactivex/Observable;", "", "getOnCardPanEntered", "()Lio/reactivex/Observable;", "cardActivated", "", "hideLoading", "showLoading", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f, i.a {
      n a();

      void c();

      void d();

      void e();
   }
}
