package co.uk.getmondo.signup.card_activation;

import co.uk.getmondo.common.q;

public final class f implements b.a {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;
   private final javax.a.a c;

   static {
      boolean var0;
      if(!f.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   public f(javax.a.a var1, javax.a.a var2) {
      if(!a && var1 == null) {
         throw new AssertionError();
      } else {
         this.b = var1;
         if(!a && var2 == null) {
            throw new AssertionError();
         } else {
            this.c = var2;
         }
      }
   }

   public static b.a a(javax.a.a var0, javax.a.a var1) {
      return new f(var0, var1);
   }

   public void a(CardOnItsWayActivity var1) {
      if(var1 == null) {
         throw new NullPointerException("Cannot inject members into a null reference");
      } else {
         var1.e = (q)this.b.b();
         var1.b = (g)this.c.b();
      }
   }
}
