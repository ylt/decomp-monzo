package co.uk.getmondo.signup.card_activation;

import co.uk.getmondo.settings.k;

public final class h implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final b.a b;
   private final javax.a.a c;
   private final javax.a.a d;

   static {
      boolean var0;
      if(!h.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   public h(b.a var1, javax.a.a var2, javax.a.a var3) {
      if(!a && var1 == null) {
         throw new AssertionError();
      } else {
         this.b = var1;
         if(!a && var2 == null) {
            throw new AssertionError();
         } else {
            this.c = var2;
            if(!a && var3 == null) {
               throw new AssertionError();
            } else {
               this.d = var3;
            }
         }
      }
   }

   public static b.a.b a(b.a var0, javax.a.a var1, javax.a.a var2) {
      return new h(var0, var1, var2);
   }

   public g a() {
      return (g)b.a.c.a(this.b, new g((co.uk.getmondo.common.a)this.c.b(), (k)this.d.b()));
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
