package co.uk.getmondo.signup.marketing_opt_in;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.j;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import co.uk.getmondo.common.ui.ProgressButton;
import co.uk.getmondo.signup.i;
import io.reactivex.n;
import io.reactivex.c.h;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0005\u0018\u00002\u00020\u00012\u00020\u0002:\u0001*B\u0005¢\u0006\u0002\u0010\u0003J\b\u0010\u0011\u001a\u00020\u0006H\u0016J\b\u0010\u0012\u001a\u00020\u0006H\u0016J\b\u0010\u0013\u001a\u00020\u0006H\u0016J\u0010\u0010\u0014\u001a\u00020\u00062\u0006\u0010\u0015\u001a\u00020\u0016H\u0016J\u0012\u0010\u0017\u001a\u00020\u00062\b\u0010\u0018\u001a\u0004\u0018\u00010\u0019H\u0016J$\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001d2\b\u0010\u001e\u001a\u0004\u0018\u00010\u001f2\b\u0010\u0018\u001a\u0004\u0018\u00010\u0019H\u0016J\b\u0010 \u001a\u00020\u0006H\u0016J\u001c\u0010!\u001a\u00020\u00062\b\u0010\"\u001a\u0004\u0018\u00010\u001b2\b\u0010\u0018\u001a\u0004\u0018\u00010\u0019H\u0016J\b\u0010#\u001a\u00020\u0006H\u0016J\u0010\u0010$\u001a\u00020\u00062\u0006\u0010%\u001a\u00020&H\u0016J\u0010\u0010'\u001a\u00020\u00062\u0006\u0010%\u001a\u00020&H\u0016J\b\u0010(\u001a\u00020\u0006H\u0016J\b\u0010)\u001a\u00020\u0006H\u0016R\u001a\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0007\u0010\bR\u001a\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\n\u0010\bR\u001e\u0010\u000b\u001a\u00020\f8\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010¨\u0006+"},
   d2 = {"Lco/uk/getmondo/signup/marketing_opt_in/NewsAndUpdatesFragment;", "Lco/uk/getmondo/common/fragments/BaseFragment;", "Lco/uk/getmondo/signup/marketing_opt_in/NewsAndUpdatesPresenter$View;", "()V", "onOptInClicked", "Lio/reactivex/Observable;", "", "getOnOptInClicked", "()Lio/reactivex/Observable;", "onOptOutClicked", "getOnOptOutClicked", "presenter", "Lco/uk/getmondo/signup/marketing_opt_in/NewsAndUpdatesPresenter;", "getPresenter", "()Lco/uk/getmondo/signup/marketing_opt_in/NewsAndUpdatesPresenter;", "setPresenter", "(Lco/uk/getmondo/signup/marketing_opt_in/NewsAndUpdatesPresenter;)V", "completeStage", "hideOptInLoading", "hideOptOutLoading", "onAttach", "context", "Landroid/content/Context;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "onDestroyView", "onViewCreated", "view", "reloadSignupStatus", "setOptInButtonEnabled", "enabled", "", "setOptOutButtonEnabled", "showOptInLoading", "showOptOutLoading", "StepListener", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class d extends co.uk.getmondo.common.f.a implements f.a {
   public f a;
   private HashMap c;

   public View a(int var1) {
      if(this.c == null) {
         this.c = new HashMap();
      }

      View var3 = (View)this.c.get(Integer.valueOf(var1));
      View var2 = var3;
      if(var3 == null) {
         var2 = this.getView();
         if(var2 == null) {
            var2 = null;
         } else {
            var2 = var2.findViewById(var1);
            this.c.put(Integer.valueOf(var1), var2);
         }
      }

      return var2;
   }

   public n a() {
      n var1 = com.b.a.c.c.a((ProgressButton)this.a(co.uk.getmondo.c.a.optInButton)).map((h)com.b.a.a.d.a);
      l.a(var1, "RxView.clicks(this).map(VoidToUnit)");
      return var1;
   }

   public void a(boolean var1) {
      ((ProgressButton)this.a(co.uk.getmondo.c.a.optInButton)).setClickable(var1);
   }

   public void b() {
      i.a var1 = (i.a)this.getActivity();
      if(var1 != null) {
         var1.b();
      }

   }

   public void b(boolean var1) {
      ((ProgressButton)this.a(co.uk.getmondo.c.a.optOutButton)).setClickable(var1);
   }

   public n c() {
      n var1 = com.b.a.c.c.a((ProgressButton)this.a(co.uk.getmondo.c.a.optOutButton)).map((h)com.b.a.a.d.a);
      l.a(var1, "RxView.clicks(this).map(VoidToUnit)");
      return var1;
   }

   public void d() {
      ((ProgressButton)this.a(co.uk.getmondo.c.a.optInButton)).setLoading(true);
   }

   public void e() {
      ((ProgressButton)this.a(co.uk.getmondo.c.a.optInButton)).setLoading(false);
   }

   public void f() {
      ((ProgressButton)this.a(co.uk.getmondo.c.a.optOutButton)).setLoading(true);
   }

   public void g() {
      ((ProgressButton)this.a(co.uk.getmondo.c.a.optOutButton)).setLoading(false);
   }

   public void h() {
      j var1 = this.getActivity();
      if(var1 == null) {
         throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.signup.marketing_opt_in.NewsAndUpdatesFragment.StepListener");
      } else {
         ((d.a)var1).c();
      }
   }

   public void i() {
      if(this.c != null) {
         this.c.clear();
      }

   }

   public void onAttach(Context var1) {
      l.b(var1, "context");
      super.onAttach(var1);
      if(!(var1 instanceof d.a)) {
         throw (Throwable)(new IllegalStateException(("Activity must implement " + d.a.class.getSimpleName()).toString()));
      } else if(!(var1 instanceof i.a)) {
         throw (Throwable)(new IllegalStateException(("Activity must implement " + i.a.class.getSimpleName()).toString()));
      }
   }

   public void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.B().a(this);
   }

   public View onCreateView(LayoutInflater var1, ViewGroup var2, Bundle var3) {
      l.b(var1, "inflater");
      View var4 = var1.inflate(2131034274, var2, false);
      l.a(var4, "inflater.inflate(R.layou…pdates, container, false)");
      return var4;
   }

   public void onDestroyView() {
      f var1 = this.a;
      if(var1 == null) {
         l.b("presenter");
      }

      var1.b();
      super.onDestroyView();
      this.i();
   }

   public void onViewCreated(View var1, Bundle var2) {
      super.onViewCreated(var1, var2);
      f var3 = this.a;
      if(var3 == null) {
         l.b("presenter");
      }

      var3.a((f.a)this);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&¨\u0006\u0004"},
      d2 = {"Lco/uk/getmondo/signup/marketing_opt_in/NewsAndUpdatesFragment$StepListener;", "", "onStepComplete", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a {
      void c();
   }
}
