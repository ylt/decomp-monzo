package co.uk.getmondo.signup.card_ordering;

import co.uk.getmondo.api.model.order_card.CardOrderName;
import kotlin.Metadata;

// $FF: synthetic class
@Metadata(
   bv = {1, 0, 2},
   k = 3,
   mv = {1, 1, 7}
)
public final class h {
   // $FF: synthetic field
   public static final int[] a = new int[CardOrderName.Type.values().length];

   static {
      a[CardOrderName.Type.LEGAL.ordinal()] = 1;
      a[CardOrderName.Type.PREFERRED.ordinal()] = 2;
   }
}
