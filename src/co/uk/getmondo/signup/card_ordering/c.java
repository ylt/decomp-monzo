package co.uk.getmondo.signup.card_ordering;

import co.uk.getmondo.api.model.order_card.CardOrderName;
import co.uk.getmondo.api.model.tracking.Impression;
import io.reactivex.n;
import io.reactivex.u;
import io.reactivex.c.q;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u0003\n\u0002\b\u0003\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0013B3\b\u0007\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b¢\u0006\u0002\u0010\fJ\u0018\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00022\u0006\u0010\u0010\u001a\u00020\u0011H\u0002J\u0010\u0010\u0012\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0002H\u0016R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0014"},
   d2 = {"Lco/uk/getmondo/signup/card_ordering/ChoosePinPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/signup/card_ordering/ChoosePinPresenter$View;", "ioScheduler", "Lio/reactivex/Scheduler;", "uiScheduler", "orderCardManager", "Lco/uk/getmondo/signup/card_ordering/OrderCardManager;", "apiErrorHandler", "Lco/uk/getmondo/common/errors/ApiErrorHandler;", "analyticsService", "Lco/uk/getmondo/common/AnalyticsService;", "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/signup/card_ordering/OrderCardManager;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/common/AnalyticsService;)V", "handleError", "", "view", "it", "", "register", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class c extends co.uk.getmondo.common.ui.b {
   private final u c;
   private final u d;
   private final k e;
   private final co.uk.getmondo.common.e.a f;
   private final co.uk.getmondo.common.a g;

   public c(u var1, u var2, k var3, co.uk.getmondo.common.e.a var4, co.uk.getmondo.common.a var5) {
      kotlin.d.b.l.b(var1, "ioScheduler");
      kotlin.d.b.l.b(var2, "uiScheduler");
      kotlin.d.b.l.b(var3, "orderCardManager");
      kotlin.d.b.l.b(var4, "apiErrorHandler");
      kotlin.d.b.l.b(var5, "analyticsService");
      super();
      this.c = var1;
      this.d = var2;
      this.e = var3;
      this.f = var4;
      this.g = var5;
   }

   private final void a(c.a var1, Throwable var2) {
      var1.h();
      var1.f();
      if(!co.uk.getmondo.signup.i.a.a(var2, (co.uk.getmondo.signup.i.a)var1) && !this.f.a(var2, (co.uk.getmondo.common.e.a.a)var1)) {
         var1.b(2131362198);
      }

   }

   public void a(final c.a var1) {
      kotlin.d.b.l.b(var1, "view");
      super.a((co.uk.getmondo.common.ui.f)var1);
      var1.e();
      if(var1.c() == null) {
         this.g.a(Impression.Companion.aS());
      } else {
         this.g.a(Impression.Companion.aT());
      }

      n var2 = var1.d().share();
      io.reactivex.b.a var3 = this.b;
      io.reactivex.b.b var4 = var2.filter((q)(new q() {
         public final boolean a(String var1x) {
            kotlin.d.b.l.b(var1x, "<anonymous parameter 0>");
            boolean var2;
            if(var1.c() == null) {
               var2 = true;
            } else {
               var2 = false;
            }

            return var2;
         }
      })).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(String var1x) {
            c.a var2 = var1;
            kotlin.d.b.l.a(var1x, "pin");
            var2.a(var1x);
         }
      }));
      kotlin.d.b.l.a(var4, "onPinEntered\n           …inEnteredFirstTime(pin) }");
      this.b = co.uk.getmondo.common.j.f.a(var3, var4);
      io.reactivex.b.a var7 = this.b;
      io.reactivex.b.b var6 = var2.filter((q)(new q() {
         public final boolean a(String var1x) {
            kotlin.d.b.l.b(var1x, "pin");
            boolean var2;
            if(var1.c() != null && kotlin.d.b.l.a(var1.c(), var1x) ^ true) {
               var2 = true;
            } else {
               var2 = false;
            }

            return var2;
         }
      })).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(String var1x) {
            c.this.g.a(Impression.Companion.aU());
            var1.i();
            var1.b(2131362516);
         }
      }));
      kotlin.d.b.l.a(var6, "onPinEntered\n           …ct_pin)\n                }");
      this.b = co.uk.getmondo.common.j.f.a(var7, var6);
      var3 = this.b;
      io.reactivex.b.b var5 = var2.filter((q)(new q() {
         public final boolean a(String var1x) {
            kotlin.d.b.l.b(var1x, "pin");
            return kotlin.d.b.l.a(var1.c(), var1x);
         }
      })).flatMapMaybe((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.h a(String var1x) {
            kotlin.d.b.l.b(var1x, "pin");
            return co.uk.getmondo.common.j.f.a((io.reactivex.b)c.this.e.a(var1.a(), var1x).b(c.this.c).a(c.this.d).c((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(io.reactivex.b.b var1x) {
                  var1.g();
               }
            })).a((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(Throwable var1x) {
                  c var2 = c.this;
                  c.a var3 = var1;
                  kotlin.d.b.l.a(var1x, "it");
                  var2.a(var3, var1x);
               }
            })), (Object)kotlin.n.a);
         }
      })).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n var1x) {
            var1.j();
         }
      }));
      kotlin.d.b.l.a(var5, "onPinEntered\n           …be { view.cardOrdered() }");
      this.b = co.uk.getmondo.common.j.f.a(var3, var5);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0002\b\b\bf\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003J\b\u0010\u0010\u001a\u00020\u0011H&J\b\u0010\u0012\u001a\u00020\u0011H&J\b\u0010\u0013\u001a\u00020\u0011H&J\b\u0010\u0014\u001a\u00020\u0011H&J\u0010\u0010\u0015\u001a\u00020\u00112\u0006\u0010\u0016\u001a\u00020\nH&J\b\u0010\u0017\u001a\u00020\u0011H&J\b\u0010\u0018\u001a\u00020\u0011H&R\u0012\u0010\u0004\u001a\u00020\u0005X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0006\u0010\u0007R\u0018\u0010\b\u001a\b\u0012\u0004\u0012\u00020\n0\tX¦\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\fR\u0014\u0010\r\u001a\u0004\u0018\u00010\nX¦\u0004¢\u0006\u0006\u001a\u0004\b\u000e\u0010\u000f¨\u0006\u0019"},
      d2 = {"Lco/uk/getmondo/signup/card_ordering/ChoosePinPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "Lco/uk/getmondo/signup/SignupApiErrorHandler$View;", "nameType", "Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;", "getNameType", "()Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;", "onPinEntered", "Lio/reactivex/Observable;", "", "getOnPinEntered", "()Lio/reactivex/Observable;", "previousPin", "getPreviousPin", "()Ljava/lang/String;", "cardOrdered", "", "clearPin", "hideKeyboard", "hideLoading", "onPinEnteredFirstTime", "pin", "showKeyboard", "showLoading", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f, co.uk.getmondo.signup.i.a {
      CardOrderName.Type a();

      void a(String var1);

      String c();

      n d();

      void e();

      void f();

      void g();

      void h();

      void i();

      void j();
   }
}
