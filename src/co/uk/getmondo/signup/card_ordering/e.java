package co.uk.getmondo.signup.card_ordering;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import co.uk.getmondo.api.model.order_card.CardOrderName;
import co.uk.getmondo.common.ae;
import co.uk.getmondo.common.ui.LoadingErrorView;
import io.reactivex.n;
import java.util.HashMap;
import java.util.Locale;
import kotlin.Metadata;
import kotlin.TypeCastException;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u000e\u0018\u0000 62\u00020\u00012\u00020\u0002:\u000267B\u0005¢\u0006\u0002\u0010\u0003J\b\u0010\u0015\u001a\u00020\u0006H\u0016J\b\u0010\u0016\u001a\u00020\u0006H\u0016J\u0010\u0010\u0017\u001a\u00020\u00062\u0006\u0010\u0018\u001a\u00020\u0019H\u0016J\u0012\u0010\u001a\u001a\u00020\u00062\b\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u0016J$\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020 2\b\u0010!\u001a\u0004\u0018\u00010\"2\b\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u0016J\b\u0010#\u001a\u00020\u0006H\u0016J\u001a\u0010$\u001a\u00020\u00062\u0006\u0010%\u001a\u00020\u001e2\b\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u0016J\u0018\u0010&\u001a\u00020\u00062\u0006\u0010'\u001a\u00020(2\u0006\u0010)\u001a\u00020*H\u0016J\b\u0010+\u001a\u00020\u0006H\u0016J\u0010\u0010,\u001a\u00020\u00062\u0006\u0010-\u001a\u00020*H\u0016J\u0010\u0010.\u001a\u00020\u00062\u0006\u0010-\u001a\u00020*H\u0016J\u0010\u0010/\u001a\u00020\u00062\u0006\u00100\u001a\u00020*H\u0016J\u0010\u00101\u001a\u00020\u00062\u0006\u00102\u001a\u00020*H\u0016J\u0010\u00103\u001a\u00020\u00062\u0006\u0010-\u001a\u00020*H\u0016J\b\u00104\u001a\u00020\u0006H\u0016J\u0010\u00105\u001a\u00020\u00062\u0006\u0010-\u001a\u00020*H\u0016R\u001a\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0007\u0010\bR\u001a\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\n\u0010\bR\u001a\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\f\u0010\bR\u001a\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u000e\u0010\bR\u001e\u0010\u000f\u001a\u00020\u00108\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014¨\u00068"},
   d2 = {"Lco/uk/getmondo/signup/card_ordering/ConfirmCardDetailsFragment;", "Lco/uk/getmondo/common/fragments/BaseFragment;", "Lco/uk/getmondo/signup/card_ordering/ConfirmCardDetailsPresenter$View;", "()V", "onLegalNameSelected", "Lio/reactivex/Observable;", "", "getOnLegalNameSelected", "()Lio/reactivex/Observable;", "onPreferredNameSelected", "getOnPreferredNameSelected", "onRetryClicked", "getOnRetryClicked", "onSendCardClicked", "getOnSendCardClicked", "presenter", "Lco/uk/getmondo/signup/card_ordering/ConfirmCardDetailsPresenter;", "getPresenter", "()Lco/uk/getmondo/signup/card_ordering/ConfirmCardDetailsPresenter;", "setPresenter", "(Lco/uk/getmondo/signup/card_ordering/ConfirmCardDetailsPresenter;)V", "enableChoosingAName", "hideLoading", "onAttach", "context", "Landroid/content/Context;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "onDestroyView", "onViewCreated", "view", "openChooseYourPin", "nameType", "Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;", "nameOnCard", "", "reloadSignupStatus", "setLegalNameSelected", "name", "setPreferredNameSelected", "showAddress", "address", "showError", "message", "showLegalName", "showLoading", "showPreferredName", "Companion", "StepListener", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class e extends co.uk.getmondo.common.f.a implements g.a {
   public static final e.a c = new e.a((kotlin.d.b.i)null);
   public g a;
   private HashMap d;

   public View a(int var1) {
      if(this.d == null) {
         this.d = new HashMap();
      }

      View var3 = (View)this.d.get(Integer.valueOf(var1));
      View var2 = var3;
      if(var3 == null) {
         var2 = this.getView();
         if(var2 == null) {
            var2 = null;
         } else {
            var2 = var2.findViewById(var1);
            this.d.put(Integer.valueOf(var1), var2);
         }
      }

      return var2;
   }

   public n a() {
      return ((LoadingErrorView)this.a(co.uk.getmondo.c.a.orderCardErrorView)).c();
   }

   public void a(CardOrderName.Type var1, String var2) {
      kotlin.d.b.l.b(var1, "nameType");
      kotlin.d.b.l.b(var2, "nameOnCard");
      android.support.v4.app.j var3 = this.getActivity();
      if(var3 == null) {
         throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.signup.card_ordering.ConfirmCardDetailsFragment.StepListener");
      } else {
         ((e.b)var3).a(var1, var2);
      }
   }

   public void a(String var1) {
      kotlin.d.b.l.b(var1, "name");
      ae.a((View)((TextView)this.a(co.uk.getmondo.c.a.orderCardLegalNameTitleTextView)));
      ae.a((View)((TextView)this.a(co.uk.getmondo.c.a.orderCardLegalNameTextView)));
      TextView var2 = (TextView)this.a(co.uk.getmondo.c.a.orderCardLegalNameTextView);
      Resources var3 = this.getResources();
      Locale var4 = Locale.ENGLISH;
      kotlin.d.b.l.a(var4, "Locale.ENGLISH");
      String var5 = var1.toUpperCase(var4);
      kotlin.d.b.l.a(var5, "(this as java.lang.String).toUpperCase(locale)");
      var2.setText((CharSequence)var3.getString(2131362512, new Object[]{var5}));
      ((TextView)this.a(co.uk.getmondo.c.a.orderCardNameOnCardTextView)).setText((CharSequence)var1);
   }

   public void b() {
      co.uk.getmondo.signup.i.a var1 = (co.uk.getmondo.signup.i.a)this.getActivity();
      if(var1 != null) {
         var1.b();
      }

   }

   public void b(String var1) {
      kotlin.d.b.l.b(var1, "name");
      ae.a((View)((TextView)this.a(co.uk.getmondo.c.a.orderCardPreferredNameTitleTextView)));
      ae.a((View)((TextView)this.a(co.uk.getmondo.c.a.orderCardPreferredNameTextView)));
      TextView var2 = (TextView)this.a(co.uk.getmondo.c.a.orderCardPreferredNameTextView);
      Resources var3 = this.getResources();
      Locale var4 = Locale.ENGLISH;
      kotlin.d.b.l.a(var4, "Locale.ENGLISH");
      String var5 = var1.toUpperCase(var4);
      kotlin.d.b.l.a(var5, "(this as java.lang.String).toUpperCase(locale)");
      var2.setText((CharSequence)var3.getString(2131362514, new Object[]{var5}));
      ((TextView)this.a(co.uk.getmondo.c.a.orderCardNameOnCardTextView)).setText((CharSequence)var1);
   }

   public n c() {
      n var1 = com.b.a.c.c.a((Button)this.a(co.uk.getmondo.c.a.orderCardNextButton)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var1, "RxView.clicks(this).map(VoidToUnit)");
      return var1;
   }

   public void c(String var1) {
      kotlin.d.b.l.b(var1, "address");
      ((TextView)this.a(co.uk.getmondo.c.a.orderCardAddressTextView)).setText((CharSequence)var1);
      ae.a((View)((LinearLayout)this.a(co.uk.getmondo.c.a.confirmCardDetailsContainer)));
   }

   public n d() {
      n var1 = com.b.a.c.c.a((RelativeLayout)this.a(co.uk.getmondo.c.a.orderCardLegalNameViewGroup)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var1, "RxView.clicks(this).map(VoidToUnit)");
      return var1;
   }

   public void d(String var1) {
      kotlin.d.b.l.b(var1, "message");
      ((LoadingErrorView)this.a(co.uk.getmondo.c.a.orderCardErrorView)).setMessage(var1);
      ae.a((View)((LoadingErrorView)this.a(co.uk.getmondo.c.a.orderCardErrorView)));
   }

   public n e() {
      n var1 = com.b.a.c.c.a((RelativeLayout)this.a(co.uk.getmondo.c.a.orderCardPreferredNameViewGroup)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var1, "RxView.clicks(this).map(VoidToUnit)");
      return var1;
   }

   public void e(String var1) {
      kotlin.d.b.l.b(var1, "name");
      ae.b((ImageView)this.a(co.uk.getmondo.c.a.orderCardPreferredNameImageView));
      ae.a((View)((ImageView)this.a(co.uk.getmondo.c.a.orderCardLegalNameImageView)));
      ((TextView)this.a(co.uk.getmondo.c.a.orderCardNameOnCardTextView)).setText((CharSequence)var1);
   }

   public void f() {
      ae.b((LoadingErrorView)this.a(co.uk.getmondo.c.a.orderCardErrorView));
      ae.a((View)((ProgressBar)this.a(co.uk.getmondo.c.a.orderCardProgressBar)));
   }

   public void f(String var1) {
      kotlin.d.b.l.b(var1, "name");
      ae.b((ImageView)this.a(co.uk.getmondo.c.a.orderCardLegalNameImageView));
      ae.a((View)((ImageView)this.a(co.uk.getmondo.c.a.orderCardPreferredNameImageView)));
      ((TextView)this.a(co.uk.getmondo.c.a.orderCardNameOnCardTextView)).setText((CharSequence)var1);
   }

   public void g() {
      ae.b((ProgressBar)this.a(co.uk.getmondo.c.a.orderCardProgressBar));
   }

   public void h() {
      ae.a(this.a(co.uk.getmondo.c.a.orderCardDividerView));
      ((TextView)this.a(co.uk.getmondo.c.a.orderCardTitleTextView)).setText((CharSequence)this.getResources().getString(2131362508));
   }

   public void i() {
      if(this.d != null) {
         this.d.clear();
      }

   }

   public void onAttach(Context var1) {
      kotlin.d.b.l.b(var1, "context");
      super.onAttach(var1);
      if(!(var1 instanceof e.b)) {
         throw (Throwable)(new IllegalStateException(("Activity must implement " + e.b.class.getSimpleName()).toString()));
      } else if(!(var1 instanceof co.uk.getmondo.signup.i.a)) {
         throw (Throwable)(new IllegalStateException(("Activity must implement " + co.uk.getmondo.signup.i.a.class.getSimpleName()).toString()));
      }
   }

   public void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.B().a(this);
   }

   public View onCreateView(LayoutInflater var1, ViewGroup var2, Bundle var3) {
      kotlin.d.b.l.b(var1, "inflater");
      View var4 = var1.inflate(2131034268, var2, false);
      kotlin.d.b.l.a(var4, "inflater.inflate(R.layou…etails, container, false)");
      return var4;
   }

   public void onDestroyView() {
      g var1 = this.a;
      if(var1 == null) {
         kotlin.d.b.l.b("presenter");
      }

      var1.b();
      super.onDestroyView();
      this.i();
   }

   public void onViewCreated(View var1, Bundle var2) {
      kotlin.d.b.l.b(var1, "view");
      super.onViewCreated(var1, var2);
      g var3 = this.a;
      if(var3 == null) {
         kotlin.d.b.l.b("presenter");
      }

      var3.a((g.a)this);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0006\u0010\u0003\u001a\u00020\u0004¨\u0006\u0005"},
      d2 = {"Lco/uk/getmondo/signup/card_ordering/ConfirmCardDetailsFragment$Companion;", "", "()V", "newInstance", "Landroid/support/v4/app/Fragment;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var1) {
         this();
      }

      public final Fragment a() {
         return (Fragment)(new e());
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\bf\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H&¨\u0006\b"},
      d2 = {"Lco/uk/getmondo/signup/card_ordering/ConfirmCardDetailsFragment$StepListener;", "", "onCardDetailsConfirmed", "", "nameType", "Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;", "nameOnCard", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface b {
      void a(CardOrderName.Type var1, String var2);
   }
}
