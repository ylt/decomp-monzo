package co.uk.getmondo.signup.card_ordering;

import co.uk.getmondo.api.model.ApiAddress;
import co.uk.getmondo.api.model.order_card.CardOrderName;
import co.uk.getmondo.api.model.order_card.CardOrderOptions;
import co.uk.getmondo.api.model.tracking.Impression;
import io.reactivex.u;
import io.reactivex.c.q;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.n;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0019B3\b\u0007\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b¢\u0006\u0002\u0010\fJ\u0010\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0002H\u0016J\u0010\u0010\u0017\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0002H\u0002J\u0010\u0010\u0018\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0002H\u0002R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u000f\u001a\u0004\u0018\u00010\u0010X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0011\u001a\u0004\u0018\u00010\u0010X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082.¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u001a"},
   d2 = {"Lco/uk/getmondo/signup/card_ordering/ConfirmCardDetailsPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/signup/card_ordering/ConfirmCardDetailsPresenter$View;", "ioScheduler", "Lio/reactivex/Scheduler;", "uiScheduler", "orderCardManager", "Lco/uk/getmondo/signup/card_ordering/OrderCardManager;", "apiErrorHandler", "Lco/uk/getmondo/common/errors/ApiErrorHandler;", "analyticsService", "Lco/uk/getmondo/common/AnalyticsService;", "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/signup/card_ordering/OrderCardManager;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/common/AnalyticsService;)V", "canChooseName", "", "legalName", "", "preferredName", "selectedNameType", "Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;", "register", "", "view", "setLegalNameSelected", "setPreferredNameSelected", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class g extends co.uk.getmondo.common.ui.b {
   private String c;
   private String d;
   private boolean e;
   private CardOrderName.Type f;
   private final u g;
   private final u h;
   private final k i;
   private final co.uk.getmondo.common.e.a j;
   private final co.uk.getmondo.common.a k;

   public g(u var1, u var2, k var3, co.uk.getmondo.common.e.a var4, co.uk.getmondo.common.a var5) {
      kotlin.d.b.l.b(var1, "ioScheduler");
      kotlin.d.b.l.b(var2, "uiScheduler");
      kotlin.d.b.l.b(var3, "orderCardManager");
      kotlin.d.b.l.b(var4, "apiErrorHandler");
      kotlin.d.b.l.b(var5, "analyticsService");
      super();
      this.g = var1;
      this.h = var2;
      this.i = var3;
      this.j = var4;
      this.k = var5;
   }

   private final void b(g.a var1) {
      this.f = CardOrderName.Type.PREFERRED;
      String var2 = this.d;
      if(var2 == null) {
         kotlin.d.b.l.a();
      }

      var1.f(var2);
   }

   private final void c(g.a var1) {
      this.f = CardOrderName.Type.LEGAL;
      String var2 = this.c;
      if(var2 == null) {
         kotlin.d.b.l.a();
      }

      var1.e(var2);
   }

   // $FF: synthetic method
   public static final CardOrderName.Type f(g var0) {
      CardOrderName.Type var1 = var0.f;
      if(var1 == null) {
         kotlin.d.b.l.b("selectedNameType");
      }

      return var1;
   }

   public void a(final g.a var1) {
      kotlin.d.b.l.b(var1, "view");
      super.a((co.uk.getmondo.common.ui.f)var1);
      this.k.a(Impression.Companion.aQ());
      io.reactivex.b.a var3 = this.b;
      io.reactivex.b.b var2 = var1.a().startWith((Object)n.a).flatMapMaybe((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.h a(n var1x) {
            kotlin.d.b.l.b(var1x, "it");
            return co.uk.getmondo.common.j.f.a(g.this.i.a().b(g.this.g).a(g.this.h).b((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(io.reactivex.b.b var1x) {
                  var1.f();
               }
            })).a((io.reactivex.c.b)(new io.reactivex.c.b() {
               public final void a(CardOrderOptions var1x, Throwable var2) {
                  var1.g();
               }
            })).d((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(Throwable var1x) {
                  co.uk.getmondo.signup.i var2 = co.uk.getmondo.signup.i.a;
                  kotlin.d.b.l.a(var1x, "it");
                  if(!var2.a(var1x, (co.uk.getmondo.signup.i.a)var1) && !g.this.j.a(var1x, (co.uk.getmondo.common.e.a.a)var1)) {
                     var1.b(2131362198);
                  }

               }
            })));
         }
      })).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(CardOrderOptions var1x) {
            List var2 = var1x.a();
            ApiAddress var3 = var1x.b();
            Iterator var7 = ((Iterable)var2).iterator();

            while(var7.hasNext()) {
               CardOrderName var5 = (CardOrderName)var7.next();
               CardOrderName.Type var4 = var5.a();
               String var6 = var5.b();
               switch(h.a[var4.ordinal()]) {
               case 1:
                  g.this.c = var6;
                  g.this.f = CardOrderName.Type.LEGAL;
                  var1.a(var6);
                  break;
               case 2:
                  g.this.d = var6;
                  g.this.f = CardOrderName.Type.PREFERRED;
                  var1.b(var6);
               }
            }

            if(var2.size() > 1) {
               var1.h();
               g.this.c(var1);
               g.this.e = true;
            }

            var1.c(var3.e());
         }
      }));
      kotlin.d.b.l.a(var2, "view.onRetryClicked\n    …Line())\n                }");
      this.b = co.uk.getmondo.common.j.f.a(var3, var2);
      io.reactivex.b.a var5 = this.b;
      io.reactivex.b.b var6 = var1.d().skipWhile((q)(new q() {
         public final boolean a(n var1) {
            kotlin.d.b.l.b(var1, "it");
            boolean var2;
            if(!g.this.e) {
               var2 = true;
            } else {
               var2 = false;
            }

            return var2;
         }
      })).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(n var1x) {
            g.this.c(var1);
         }
      }));
      kotlin.d.b.l.a(var6, "view.onLegalNameSelected…LegalNameSelected(view) }");
      this.b = co.uk.getmondo.common.j.f.a(var5, var6);
      var5 = this.b;
      var6 = var1.e().skipWhile((q)(new q() {
         public final boolean a(n var1) {
            kotlin.d.b.l.b(var1, "it");
            boolean var2;
            if(!g.this.e) {
               var2 = true;
            } else {
               var2 = false;
            }

            return var2;
         }
      })).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(n var1x) {
            g.this.b(var1);
         }
      }));
      kotlin.d.b.l.a(var6, "view.onPreferredNameSele…erredNameSelected(view) }");
      this.b = co.uk.getmondo.common.j.f.a(var5, var6);
      var5 = this.b;
      io.reactivex.b.b var4 = var1.c().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(n var1x) {
            String var2;
            String var3;
            if(kotlin.d.b.l.a(g.f(g.this), CardOrderName.Type.LEGAL)) {
               var2 = g.this.c;
               var3 = var2;
               if(var2 == null) {
                  kotlin.d.b.l.a();
                  var3 = var2;
               }
            } else {
               var2 = g.this.d;
               var3 = var2;
               if(var2 == null) {
                  kotlin.d.b.l.a();
                  var3 = var2;
               }
            }

            g.this.k.a(Impression.Companion.p(g.f(g.this).a()));
            var1.a(g.f(g.this), var3);
         }
      }));
      kotlin.d.b.l.a(var4, "view.onSendCardClicked\n …OnCard)\n                }");
      this.b = co.uk.getmondo.common.j.f.a(var5, var4);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\f\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\t\bf\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003J\b\u0010\u000f\u001a\u00020\u0006H&J\b\u0010\u0010\u001a\u00020\u0006H&J\u0018\u0010\u0011\u001a\u00020\u00062\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0015H&J\u0010\u0010\u0016\u001a\u00020\u00062\u0006\u0010\u0017\u001a\u00020\u0015H&J\u0010\u0010\u0018\u001a\u00020\u00062\u0006\u0010\u0017\u001a\u00020\u0015H&J\u0010\u0010\u0019\u001a\u00020\u00062\u0006\u0010\u001a\u001a\u00020\u0015H&J\u0010\u0010\u001b\u001a\u00020\u00062\u0006\u0010\u0017\u001a\u00020\u0015H&J\b\u0010\u001c\u001a\u00020\u0006H&J\u0010\u0010\u001d\u001a\u00020\u00062\u0006\u0010\u0017\u001a\u00020\u0015H&R\u0018\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0007\u0010\bR\u0018\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X¦\u0004¢\u0006\u0006\u001a\u0004\b\n\u0010\bR\u0018\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X¦\u0004¢\u0006\u0006\u001a\u0004\b\f\u0010\bR\u0018\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X¦\u0004¢\u0006\u0006\u001a\u0004\b\u000e\u0010\b¨\u0006\u001e"},
      d2 = {"Lco/uk/getmondo/signup/card_ordering/ConfirmCardDetailsPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "Lco/uk/getmondo/signup/SignupApiErrorHandler$View;", "onLegalNameSelected", "Lio/reactivex/Observable;", "", "getOnLegalNameSelected", "()Lio/reactivex/Observable;", "onPreferredNameSelected", "getOnPreferredNameSelected", "onRetryClicked", "getOnRetryClicked", "onSendCardClicked", "getOnSendCardClicked", "enableChoosingAName", "hideLoading", "openChooseYourPin", "nameType", "Lco/uk/getmondo/api/model/order_card/CardOrderName$Type;", "nameOnCard", "", "setLegalNameSelected", "name", "setPreferredNameSelected", "showAddress", "address", "showLegalName", "showLoading", "showPreferredName", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f, co.uk.getmondo.signup.i.a {
      io.reactivex.n a();

      void a(CardOrderName.Type var1, String var2);

      void a(String var1);

      void b(String var1);

      io.reactivex.n c();

      void c(String var1);

      io.reactivex.n d();

      io.reactivex.n e();

      void e(String var1);

      void f();

      void f(String var1);

      void g();

      void h();
   }
}
