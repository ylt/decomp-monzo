package co.uk.getmondo.signup.card_ordering;

import co.uk.getmondo.api.SignupApi;

public final class l implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;

   static {
      boolean var0;
      if(!l.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   public l(javax.a.a var1) {
      if(!a && var1 == null) {
         throw new AssertionError();
      } else {
         this.b = var1;
      }
   }

   public static b.a.b a(javax.a.a var0) {
      return new l(var0);
   }

   public k a() {
      return new k((SignupApi)this.b.b());
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
