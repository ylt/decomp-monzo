package co.uk.getmondo.signup.phone_verification;

import co.uk.getmondo.api.model.tracking.Impression;
import io.reactivex.r;
import io.reactivex.u;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u0003\n\u0002\b\u0004\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0014B3\b\u0007\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b¢\u0006\u0002\u0010\fJ\u0006\u0010\r\u001a\u00020\u000eJ\u0018\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0002H\u0002J\u0010\u0010\u0013\u001a\u00020\u000e2\u0006\u0010\u0012\u001a\u00020\u0002H\u0016R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0015"},
   d2 = {"Lco/uk/getmondo/signup/phone_verification/SmsVerificationPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/signup/phone_verification/SmsVerificationPresenter$View;", "uiScheduler", "Lio/reactivex/Scheduler;", "ioScheduler", "phoneVerificationManager", "Lco/uk/getmondo/signup/phone_verification/PhoneVerificationManager;", "apiErrorHandler", "Lco/uk/getmondo/common/errors/ApiErrorHandler;", "analyticsService", "Lco/uk/getmondo/common/AnalyticsService;", "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/signup/phone_verification/PhoneVerificationManager;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/common/AnalyticsService;)V", "getNewCode", "", "handleError", "throwable", "", "view", "register", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class l extends co.uk.getmondo.common.ui.b {
   private final u c;
   private final u d;
   private final c e;
   private final co.uk.getmondo.common.e.a f;
   private final co.uk.getmondo.common.a g;

   public l(u var1, u var2, c var3, co.uk.getmondo.common.e.a var4, co.uk.getmondo.common.a var5) {
      kotlin.d.b.l.b(var1, "uiScheduler");
      kotlin.d.b.l.b(var2, "ioScheduler");
      kotlin.d.b.l.b(var3, "phoneVerificationManager");
      kotlin.d.b.l.b(var4, "apiErrorHandler");
      kotlin.d.b.l.b(var5, "analyticsService");
      super();
      this.c = var1;
      this.d = var2;
      this.e = var3;
      this.f = var4;
      this.g = var5;
   }

   private final void a(Throwable var1, l.a var2) {
      boolean var3;
      label30: {
         var2.g();
         b var5 = (b)co.uk.getmondo.common.e.c.a(var1, (co.uk.getmondo.common.e.f[])b.values());
         if(var5 != null) {
            switch(m.a[var5.ordinal()]) {
            case 1:
               var2.j();
               var3 = true;
               break label30;
            case 2:
               var2.k();
               var3 = true;
               break label30;
            case 3:
               var2.m();
               var3 = true;
               break label30;
            case 4:
               var2.n();
               var2.o();
               var3 = true;
               break label30;
            }
         }

         var3 = false;
      }

      boolean var4 = var3;
      if(!var3) {
         var4 = co.uk.getmondo.signup.i.a.a(var1, (co.uk.getmondo.signup.i.a)var2);
      }

      var3 = var4;
      if(!var4) {
         var3 = this.f.a(var1, (co.uk.getmondo.common.e.a.a)var2);
      }

      if(!var3) {
         var2.b(2131362198);
      }

   }

   // $FF: synthetic method
   public static final l.a d(l var0) {
      return (l.a)var0.a;
   }

   public final void a() {
      ((l.a)this.a).f();
      io.reactivex.b.a var1 = this.b;
      io.reactivex.b.b var2 = this.e.a(((l.a)this.a).h()).b(this.d).a(this.c).a((io.reactivex.c.a)(new io.reactivex.c.a() {
         public final void a() {
            l.d(l.this).g();
         }
      }), (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Throwable var1) {
            l var3 = l.this;
            kotlin.d.b.l.a(var1, "it");
            l.a var2 = l.d(l.this);
            kotlin.d.b.l.a(var2, "view");
            var3.a(var1, var2);
         }
      }));
      kotlin.d.b.l.a(var2, "phoneVerificationManager…view) }\n                )");
      this.b = co.uk.getmondo.common.j.f.a(var1, var2);
   }

   public void a(final l.a var1) {
      kotlin.d.b.l.b(var1, "view");
      super.a((co.uk.getmondo.common.ui.f)var1);
      this.g.a(Impression.Companion.B());
      io.reactivex.n var2 = var1.e().share();
      io.reactivex.b.a var3 = this.b;
      io.reactivex.b.b var4 = var2.map((io.reactivex.c.h)null.a).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Boolean var1x) {
            l.a var2 = var1;
            kotlin.d.b.l.a(var1x, "it");
            var2.a(var1x.booleanValue());
         }
      }));
      kotlin.d.b.l.a(var4, "codeChanges\n            …ntinueButtonEnabled(it) }");
      this.b = co.uk.getmondo.common.j.f.a(var3, var4);
      var3 = this.b;
      var4 = var2.subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(String var1x) {
            var1.l();
         }
      }));
      kotlin.d.b.l.a(var4, "codeChanges\n            …ideIncorrectCodeError() }");
      this.b = co.uk.getmondo.common.j.f.a(var3, var4);
      io.reactivex.n var7 = var1.d();
      kotlin.d.b.l.a(var2, "codeChanges");
      var2 = co.uk.getmondo.common.j.f.a(var7, (r)var2).map((io.reactivex.c.h)null.a);
      var7 = var1.c().share();
      io.reactivex.b.a var8 = this.b;
      io.reactivex.b.b var5 = var7.observeOn(this.c).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(String var1x) {
            l.a var2 = var1;
            kotlin.d.b.l.a(var1x, "it");
            var2.a(var1x);
         }
      }), (io.reactivex.c.g)null.a);
      kotlin.d.b.l.a(var5, "smsCodeReceived\n        …(it) }, { Timber.e(it) })");
      this.b = co.uk.getmondo.common.j.f.a(var8, var5);
      var8 = this.b;
      io.reactivex.b.b var6 = io.reactivex.n.merge((r)var2, (r)var7).flatMapMaybe((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.h a(String var1x) {
            kotlin.d.b.l.b(var1x, "code");
            return co.uk.getmondo.common.j.f.a((io.reactivex.b)l.this.e.a(var1.h(), var1x).b(l.this.d).a(l.this.c).c((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(io.reactivex.b.b var1x) {
                  var1.f();
               }
            })).a((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(Throwable var1x) {
                  l var2 = l.this;
                  kotlin.d.b.l.a(var1x, "it");
                  var2.a(var1x, var1);
               }
            })), (Object)kotlin.n.a);
         }
      })).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n var1x) {
            var1.i();
         }
      }));
      kotlin.d.b.l.a(var6, "Observable.merge(manualS…onPhoneNumberVerified() }");
      this.b = co.uk.getmondo.common.j.f.a(var8, var6);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u000e\n\u0002\u0010\u000b\n\u0002\b\u0006\bf\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003J\u0010\u0010\u0011\u001a\u00020\n2\u0006\u0010\u0012\u001a\u00020\u0006H&J\b\u0010\u0013\u001a\u00020\nH&J\b\u0010\u0014\u001a\u00020\nH&J\b\u0010\u0015\u001a\u00020\nH&J\b\u0010\u0016\u001a\u00020\nH&J\u0010\u0010\u0017\u001a\u00020\n2\u0006\u0010\u0018\u001a\u00020\u0019H&J\b\u0010\u001a\u001a\u00020\nH&J\b\u0010\u001b\u001a\u00020\nH&J\b\u0010\u001c\u001a\u00020\nH&J\b\u0010\u001d\u001a\u00020\nH&J\b\u0010\u001e\u001a\u00020\nH&R\u0018\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0007\u0010\bR\u0018\u0010\t\u001a\b\u0012\u0004\u0012\u00020\n0\u0005X¦\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\bR\u0018\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X¦\u0004¢\u0006\u0006\u001a\u0004\b\r\u0010\bR\u0012\u0010\u000e\u001a\u00020\u0006X¦\u0004¢\u0006\u0006\u001a\u0004\b\u000f\u0010\u0010¨\u0006\u001f"},
      d2 = {"Lco/uk/getmondo/signup/phone_verification/SmsVerificationPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "Lco/uk/getmondo/signup/SignupApiErrorHandler$View;", "onCodeChanged", "Lio/reactivex/Observable;", "", "getOnCodeChanged", "()Lio/reactivex/Observable;", "onContinueClicked", "", "getOnContinueClicked", "onSmsCodeReceived", "getOnSmsCodeReceived", "phoneNumber", "getPhoneNumber", "()Ljava/lang/String;", "fillCode", "code", "hideIncorrectCodeError", "hideLoading", "navigateToSmsSendScreen", "onPhoneNumberVerified", "setContinueButtonEnabled", "enabled", "", "showExpiredCodeError", "showIncorrectCodeError", "showLoading", "showPhoneNumberAlreadyInUseError", "showTooManyAttemptsError", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f, co.uk.getmondo.signup.i.a {
      void a(String var1);

      void a(boolean var1);

      io.reactivex.n c();

      io.reactivex.n d();

      io.reactivex.n e();

      void f();

      void g();

      String h();

      void i();

      void j();

      void k();

      void l();

      void m();

      void n();

      void o();
   }
}
