package co.uk.getmondo.signup.phone_verification;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import java.io.Serializable;
import java.util.HashMap;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\u0018\u0000 \u000e2\u00020\u00012\u00020\u00022\u00020\u0003:\u0001\u000eB\u0005¢\u0006\u0002\u0010\u0004J\b\u0010\u0005\u001a\u00020\u0006H\u0016J\u0012\u0010\u0007\u001a\u00020\u00062\b\u0010\b\u001a\u0004\u0018\u00010\tH\u0014J\u0010\u0010\n\u001a\u00020\u00062\u0006\u0010\u000b\u001a\u00020\fH\u0016J\b\u0010\r\u001a\u00020\u0006H\u0016¨\u0006\u000f"},
   d2 = {"Lco/uk/getmondo/signup/phone_verification/PhoneVerificationActivity;", "Lco/uk/getmondo/signup/BaseSignupActivity;", "Lco/uk/getmondo/signup/phone_verification/SmsSendFragment$StepListener;", "Lco/uk/getmondo/signup/phone_verification/SmsVerificationFragment$StepListener;", "()V", "navigateToSmsSendScreen", "", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onPhoneNumberSent", "phoneNumber", "", "onPhoneNumberVerified", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class PhoneVerificationActivity extends co.uk.getmondo.signup.a implements e.b, j.d {
   public static final PhoneVerificationActivity.a a = new PhoneVerificationActivity.a((kotlin.d.b.i)null);
   private HashMap b;

   public View a(int var1) {
      if(this.b == null) {
         this.b = new HashMap();
      }

      View var3 = (View)this.b.get(Integer.valueOf(var1));
      View var2 = var3;
      if(var3 == null) {
         var2 = this.findViewById(var1);
         this.b.put(Integer.valueOf(var1), var2);
      }

      return var2;
   }

   public void a(String var1) {
      kotlin.d.b.l.b(var1, "phoneNumber");
      j var3 = new j();
      Bundle var2 = new Bundle();
      var2.putString("phoneNumber", var1);
      var3.setArguments(var2);
      this.getSupportFragmentManager().a().b(2131821030, (Fragment)var3).a((String)null).a(4097).c();
   }

   public void c() {
      this.getSupportFragmentManager().b();
   }

   public void d() {
      this.setResult(-1);
      this.finish();
   }

   protected void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.setContentView(2131034197);
      this.l().a(this);
      android.support.v7.app.a var2 = this.getSupportActionBar();
      if(var2 != null) {
         var2.b(false);
      }

      var2 = this.getSupportActionBar();
      if(var2 != null) {
         var2.d(false);
      }

      if(var1 == null) {
         this.getSupportFragmentManager().a().a(2131821030, (Fragment)(new e())).c();
      }

   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0016\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b¨\u0006\t"},
      d2 = {"Lco/uk/getmondo/signup/phone_verification/PhoneVerificationActivity$Companion;", "", "()V", "buildIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "signupEntryPoint", "Lco/uk/getmondo/signup/SignupEntryPoint;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var1) {
         this();
      }

      public final Intent a(Context var1, co.uk.getmondo.signup.j var2) {
         kotlin.d.b.l.b(var1, "context");
         kotlin.d.b.l.b(var2, "signupEntryPoint");
         Intent var3 = (new Intent(var1, PhoneVerificationActivity.class)).putExtra("KEY_SIGNUP_ENTRY_POINT", (Serializable)var2);
         kotlin.d.b.l.a(var3, "Intent(context, PhoneVer…_POINT, signupEntryPoint)");
         return var3;
      }
   }
}
