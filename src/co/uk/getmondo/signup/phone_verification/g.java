package co.uk.getmondo.signup.phone_verification;

import co.uk.getmondo.api.model.tracking.Impression;
import io.reactivex.r;
import io.reactivex.u;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\b\u0004\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0013B3\b\u0007\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b¢\u0006\u0002\u0010\fJ\u0018\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0002H\u0002J\u0010\u0010\u0012\u001a\u00020\u000e2\u0006\u0010\u0011\u001a\u00020\u0002H\u0016R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0014"},
   d2 = {"Lco/uk/getmondo/signup/phone_verification/SmsSendPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/signup/phone_verification/SmsSendPresenter$View;", "uiScheduler", "Lio/reactivex/Scheduler;", "ioScheduler", "phoneVerificationManager", "Lco/uk/getmondo/signup/phone_verification/PhoneVerificationManager;", "apiErrorHandler", "Lco/uk/getmondo/common/errors/ApiErrorHandler;", "analyticsService", "Lco/uk/getmondo/common/AnalyticsService;", "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/signup/phone_verification/PhoneVerificationManager;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/common/AnalyticsService;)V", "handleError", "", "throwable", "", "view", "register", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class g extends co.uk.getmondo.common.ui.b {
   private final u c;
   private final u d;
   private final c e;
   private final co.uk.getmondo.common.e.a f;
   private final co.uk.getmondo.common.a g;

   public g(u var1, u var2, c var3, co.uk.getmondo.common.e.a var4, co.uk.getmondo.common.a var5) {
      kotlin.d.b.l.b(var1, "uiScheduler");
      kotlin.d.b.l.b(var2, "ioScheduler");
      kotlin.d.b.l.b(var3, "phoneVerificationManager");
      kotlin.d.b.l.b(var4, "apiErrorHandler");
      kotlin.d.b.l.b(var5, "analyticsService");
      super();
      this.c = var1;
      this.d = var2;
      this.e = var3;
      this.f = var4;
      this.g = var5;
   }

   private final void a(Throwable var1, g.a var2) {
      boolean var4;
      label27: {
         var2.e();
         b var5 = (b)co.uk.getmondo.common.e.c.a(var1, (co.uk.getmondo.common.e.f[])b.values());
         if(var5 != null) {
            switch(h.a[var5.ordinal()]) {
            case 1:
               var2.f();
               var4 = true;
               break label27;
            }
         }

         var4 = false;
      }

      boolean var3 = var4;
      if(!var4) {
         var3 = co.uk.getmondo.signup.i.a.a(var1, (co.uk.getmondo.signup.i.a)var2);
      }

      var4 = var3;
      if(!var3) {
         var4 = this.f.a(var1, (co.uk.getmondo.common.e.a.a)var2);
      }

      if(!var4) {
         var2.b(2131362198);
      }

   }

   public void a(final g.a var1) {
      kotlin.d.b.l.b(var1, "view");
      super.a((co.uk.getmondo.common.ui.f)var1);
      io.reactivex.n var3 = var1.c().share();
      io.reactivex.n var4 = var1.a().share();
      this.g.a(Impression.Companion.A());
      io.reactivex.b.a var2 = this.b;
      io.reactivex.b.b var5 = var3.map((io.reactivex.c.h)null.a).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Boolean var1x) {
            g.a var2 = var1;
            kotlin.d.b.l.a(var1x, "it");
            var2.a(var1x.booleanValue());
         }
      }));
      kotlin.d.b.l.a(var5, "phoneNumberChanges\n     …ntinueButtonEnabled(it) }");
      this.b = co.uk.getmondo.common.j.f.a(var2, var5);
      io.reactivex.b.a var9 = this.b;
      io.reactivex.b.b var7 = var4.subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n var1x) {
            if(!var1.h()) {
               var1.j();
            }

         }
      }));
      kotlin.d.b.l.a(var7, "continueClicks\n         …      }\n                }");
      this.b = co.uk.getmondo.common.j.f.a(var9, var7);
      var9 = this.b;
      var7 = var3.subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(String var1x) {
            var1.g();
         }
      }));
      kotlin.d.b.l.a(var7, "phoneNumberChanges\n     …validPhoneNumberError() }");
      this.b = co.uk.getmondo.common.j.f.a(var9, var7);
      io.reactivex.n var8;
      if(!var1.h()) {
         var8 = var1.i();
      } else {
         var8 = io.reactivex.n.just(Boolean.valueOf(true));
      }

      kotlin.d.b.l.a(var4, "continueClicks");
      kotlin.d.b.l.a(var8, "onSmsPermissionGranted");
      var4 = co.uk.getmondo.common.j.f.a(var4, var8);
      var2 = this.b;
      kotlin.d.b.l.a(var3, "phoneNumberChanges");
      io.reactivex.b.b var6 = co.uk.getmondo.common.j.f.a(var4, (r)var3).flatMapMaybe((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.h a(kotlin.h var1x) {
            kotlin.d.b.l.b(var1x, "<name for destructuring parameter 0>");
            String var3 = (String)var1x.d();
            c var2 = g.this.e;
            kotlin.d.b.l.a(var3, "phoneNumber");
            return co.uk.getmondo.common.j.f.a((io.reactivex.b)var2.a(var3).b(g.this.d).a(g.this.c).c((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(io.reactivex.b.b var1x) {
                  var1.d();
               }
            })).a((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(Throwable var1x) {
                  g var2 = g.this;
                  kotlin.d.b.l.a(var1x, "error");
                  var2.a(var1x, var1);
               }
            })), (Object)var3);
         }
      })).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(String var1x) {
            g.a var2 = var1;
            kotlin.d.b.l.a(var1x, "phoneNumber");
            var2.a(var1x);
         }
      }));
      kotlin.d.b.l.a(var6, "continueClicksWithPermis…NumberSent(phoneNumber) }");
      this.b = co.uk.getmondo.common.j.f.a(var2, var6);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\f\bf\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003J\b\u0010\u000f\u001a\u00020\rH&J\b\u0010\u0010\u001a\u00020\u0006H&J\b\u0010\u0011\u001a\u00020\u0006H&J\u0010\u0010\u0012\u001a\u00020\u00062\u0006\u0010\u0013\u001a\u00020\nH&J\b\u0010\u0014\u001a\u00020\u0006H&J\u0010\u0010\u0015\u001a\u00020\u00062\u0006\u0010\u0016\u001a\u00020\rH&J\b\u0010\u0017\u001a\u00020\u0006H&J\b\u0010\u0018\u001a\u00020\u0006H&R\u0018\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0007\u0010\bR\u0018\u0010\t\u001a\b\u0012\u0004\u0012\u00020\n0\u0005X¦\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\bR\u0018\u0010\f\u001a\b\u0012\u0004\u0012\u00020\r0\u0005X¦\u0004¢\u0006\u0006\u001a\u0004\b\u000e\u0010\b¨\u0006\u0019"},
      d2 = {"Lco/uk/getmondo/signup/phone_verification/SmsSendPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "Lco/uk/getmondo/signup/SignupApiErrorHandler$View;", "onContinueClicked", "Lio/reactivex/Observable;", "", "getOnContinueClicked", "()Lio/reactivex/Observable;", "onPhoneNumberChanged", "", "getOnPhoneNumberChanged", "onSmsPermissionsGranted", "", "getOnSmsPermissionsGranted", "checkSmsPermissions", "hideInvalidPhoneNumberError", "hideLoading", "onPhoneNumberSent", "phoneNumber", "requestSmsPermissions", "setContinueButtonEnabled", "enabled", "showInvalidPhoneNumberError", "showLoading", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f, co.uk.getmondo.signup.i.a {
      io.reactivex.n a();

      void a(String var1);

      void a(boolean var1);

      io.reactivex.n c();

      void d();

      void e();

      void f();

      void g();

      boolean h();

      io.reactivex.n i();

      void j();
   }
}
