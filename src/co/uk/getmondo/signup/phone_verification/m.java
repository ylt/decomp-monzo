package co.uk.getmondo.signup.phone_verification;

import kotlin.Metadata;

// $FF: synthetic class
@Metadata(
   bv = {1, 0, 2},
   k = 3,
   mv = {1, 1, 7}
)
public final class m {
   // $FF: synthetic field
   public static final int[] a = new int[b.values().length];

   static {
      a[b.b.ordinal()] = 1;
      a[b.c.ordinal()] = 2;
      a[b.d.ordinal()] = 3;
      a[b.e.ordinal()] = 4;
   }
}
