package co.uk.getmondo.signup.profile;

import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0007\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u00012\u00020\u0002B\u000f\b\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005R\u0014\u0010\u0003\u001a\u00020\u0004X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007j\u0002\b\bj\u0002\b\tj\u0002\b\n¨\u0006\u000b"},
   d2 = {"Lco/uk/getmondo/signup/profile/ProfileCreationError;", "", "Lco/uk/getmondo/common/errors/MatchableError;", "prefix", "", "(Ljava/lang/String;ILjava/lang/String;)V", "getPrefix", "()Ljava/lang/String;", "LEGAL_NAME_UNSUPPORTED_CHAR", "PREFERRED_NAME_UNSUPPORTED_CHAR", "INVALID_DATE_OF_BIRTH", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public enum j implements co.uk.getmondo.common.e.f {
   a,
   b,
   c;

   private final String e;

   static {
      j var0 = new j("LEGAL_NAME_UNSUPPORTED_CHAR", 0, "bad_request.bad_param.legal_name.unsupported_characters");
      a = var0;
      j var1 = new j("PREFERRED_NAME_UNSUPPORTED_CHAR", 1, "bad_request.bad_param.preferred_name.unsupported_characters");
      b = var1;
      j var2 = new j("INVALID_DATE_OF_BIRTH", 2, "bad_request.bad_param.date_of_birth");
      c = var2;
   }

   protected j(String var3) {
      kotlin.d.b.l.b(var3, "prefix");
      super(var1, var2);
      this.e = var3;
   }

   public String a() {
      return this.e;
   }
}
