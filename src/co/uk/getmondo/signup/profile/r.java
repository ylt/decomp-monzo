package co.uk.getmondo.signup.profile;

import co.uk.getmondo.api.SignupApi;

public final class r implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;

   static {
      boolean var0;
      if(!r.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   public r(javax.a.a var1) {
      if(!a && var1 == null) {
         throw new AssertionError();
      } else {
         this.b = var1;
      }
   }

   public static b.a.b a(javax.a.a var0) {
      return new r(var0);
   }

   public q a() {
      return new q((SignupApi)this.b.b());
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
