package co.uk.getmondo.signup.profile;

import co.uk.getmondo.api.model.signup.SignUpProfile;
import co.uk.getmondo.api.model.tracking.Impression;
import io.reactivex.u;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.d.b.w;
import kotlin.d.b.y;
import org.threeten.bp.LocalDate;
import org.threeten.bp.format.DateTimeFormatter;
import org.threeten.bp.format.DateTimeParseException;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u001fB3\b\u0007\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b¢\u0006\u0002\u0010\fJ\u0018\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u0002H\u0002J\u0012\u0010\u001a\u001a\u0004\u0018\u00010\u001b2\u0006\u0010\u001c\u001a\u00020\u001dH\u0002J\u0010\u0010\u001e\u001a\u00020\u00162\u0006\u0010\u0019\u001a\u00020\u0002H\u0016R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u001b\u0010\r\u001a\u00020\u000e8BX\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u000f\u0010\u0010R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082D¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006 "},
   d2 = {"Lco/uk/getmondo/signup/profile/ProfileDetailsPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/signup/profile/ProfileDetailsPresenter$View;", "uiScheduler", "Lio/reactivex/Scheduler;", "ioScheduler", "signupProfileManager", "Lco/uk/getmondo/signup/profile/SignupProfileManager;", "apiErrorHandler", "Lco/uk/getmondo/common/errors/ApiErrorHandler;", "analyticsService", "Lco/uk/getmondo/common/AnalyticsService;", "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/signup/profile/SignupProfileManager;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/common/AnalyticsService;)V", "dateFormatter", "Lorg/threeten/bp/format/DateTimeFormatter;", "getDateFormatter", "()Lorg/threeten/bp/format/DateTimeFormatter;", "dateFormatter$delegate", "Lkotlin/Lazy;", "dateLength", "", "handleError", "", "throwable", "", "view", "parseDate", "Lorg/threeten/bp/LocalDate;", "date", "", "register", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class n extends co.uk.getmondo.common.ui.b {
   // $FF: synthetic field
   static final kotlin.reflect.l[] c = new kotlin.reflect.l[]{(kotlin.reflect.l)y.a(new w(y.a(n.class), "dateFormatter", "getDateFormatter()Lorg/threeten/bp/format/DateTimeFormatter;"))};
   private final kotlin.c d;
   private final int e;
   private final u f;
   private final u g;
   private final q h;
   private final co.uk.getmondo.common.e.a i;
   private final co.uk.getmondo.common.a j;

   public n(u var1, u var2, q var3, co.uk.getmondo.common.e.a var4, co.uk.getmondo.common.a var5) {
      kotlin.d.b.l.b(var1, "uiScheduler");
      kotlin.d.b.l.b(var2, "ioScheduler");
      kotlin.d.b.l.b(var3, "signupProfileManager");
      kotlin.d.b.l.b(var4, "apiErrorHandler");
      kotlin.d.b.l.b(var5, "analyticsService");
      super();
      this.f = var1;
      this.g = var2;
      this.h = var3;
      this.i = var4;
      this.j = var5;
      this.d = kotlin.d.a((kotlin.d.a.a)null.a);
      this.e = 10;
   }

   private final LocalDate a(String var1) {
      LocalDate var3;
      try {
         var3 = LocalDate.a((CharSequence)var1, this.a());
      } catch (DateTimeParseException var2) {
         var3 = null;
      }

      return var3;
   }

   private final DateTimeFormatter a() {
      kotlin.c var2 = this.d;
      kotlin.reflect.l var1 = c[0];
      return (DateTimeFormatter)var2.a();
   }

   private final void a(Throwable var1, n.a var2) {
      var2.b(false);
      j var3 = (j)co.uk.getmondo.common.e.c.a(var1, (co.uk.getmondo.common.e.f[])j.values());
      if(var3 != null) {
         switch(o.a[var3.ordinal()]) {
         case 1:
            var2.j();
            break;
         case 2:
            var2.k();
            break;
         case 3:
            var2.g();
         }
      } else if(!co.uk.getmondo.signup.i.a.a(var1, (co.uk.getmondo.signup.i.a)var2) && !this.i.a(var1, (co.uk.getmondo.common.e.a.a)var2)) {
         var2.b(2131362198);
      }

   }

   public void a(final n.a var1) {
      kotlin.d.b.l.b(var1, "view");
      super.a((co.uk.getmondo.common.ui.f)var1);
      this.j.a(Impression.Companion.f());
      io.reactivex.b.a var2 = this.b;
      io.reactivex.b.b var3 = var1.a().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n var1x) {
            var1.f();
         }
      }));
      kotlin.d.b.l.a(var3, "view.onAddPreferredNameC…howPreferredNameField() }");
      this.b = co.uk.getmondo.common.j.f.a(var2, var3);
      io.reactivex.b.a var7 = this.b;
      io.reactivex.b.b var6 = this.h.a().b(this.g).a(this.f).d((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final k a(SignUpProfile var1) {
            kotlin.d.b.l.b(var1, "it");
            String var3 = var1.a();
            String var2;
            if(var1.c()) {
               var2 = null;
            } else {
               var2 = var1.b();
            }

            LocalDate var4 = var1.d();
            String var5;
            if(var4 != null) {
               var5 = var4.a(n.this.a());
               if(var5 != null) {
                  return new k(var3, var2, var5);
               }
            }

            var5 = "";
            return new k(var3, var2, var5);
         }
      })).a((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(k var1x) {
            var1.a(var1x);
         }
      }), (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Throwable var1x) {
            co.uk.getmondo.common.e.a var2 = n.this.i;
            kotlin.d.b.l.a(var1x, "it");
            var2.a(var1x, (co.uk.getmondo.common.e.a.a)var1);
            n.a.a(var1, (k)null, 1, (Object)null);
         }
      }));
      kotlin.d.b.l.a(var6, "signupProfileManager.exi…      }\n                )");
      this.b = co.uk.getmondo.common.j.f.a(var7, var6);
      io.reactivex.n var4 = var1.d().map((io.reactivex.c.h)null.a);
      io.reactivex.n var8 = var1.c().map((io.reactivex.c.h)(new io.reactivex.c.h() {
         // $FF: synthetic method
         public Object a(Object var1) {
            return Boolean.valueOf(this.a((CharSequence)var1));
         }

         public final boolean a(CharSequence var1) {
            kotlin.d.b.l.b(var1, "dateText");
            boolean var2;
            if(var1.length() == n.this.e && co.uk.getmondo.common.c.c.a(n.this.a(var1.toString()), 18L)) {
               var2 = true;
            } else {
               var2 = false;
            }

            return var2;
         }
      }));
      var2 = this.b;
      kotlin.d.b.l.a(var4, "legalNameValid");
      kotlin.d.b.l.a(var8, "dateOfBirthValid");
      var3 = co.uk.getmondo.common.j.f.a(var4, var8).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.h var1x) {
            boolean var2;
            n.a var5;
            label12: {
               Boolean var3 = (Boolean)var1x.c();
               Boolean var4 = (Boolean)var1x.d();
               var5 = var1;
               kotlin.d.b.l.a(var3, "legalNameValid");
               if(var3.booleanValue()) {
                  kotlin.d.b.l.a(var4, "dateOfBirthValid");
                  if(var4.booleanValue()) {
                     var2 = true;
                     break label12;
                  }
               }

               var2 = false;
            }

            var5.a(var2);
         }
      }));
      kotlin.d.b.l.a(var3, "combineLatest(legalNameV…hValid)\n                }");
      this.b = co.uk.getmondo.common.j.f.a(var2, var3);
      var7 = this.b;
      var6 = var1.c().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(CharSequence var1x) {
            if(var1x.length() != n.this.e) {
               var1.h();
            } else {
               LocalDate var2 = n.this.a(var1x.toString());
               if(var2 == null) {
                  var1.g();
               } else if(!co.uk.getmondo.common.c.c.a(var2, 18L)) {
                  var1.i();
               }
            }

         }
      }));
      kotlin.d.b.l.a(var6, "view.onDateOfBirthTextCh…      }\n                }");
      this.b = co.uk.getmondo.common.j.f.a(var7, var6);
      var2 = this.b;
      io.reactivex.b.b var5 = var1.e().filter((io.reactivex.c.q)(new io.reactivex.c.q() {
         public final boolean a(k var1) {
            kotlin.d.b.l.b(var1, "it");
            boolean var2;
            if(!kotlin.h.j.a((CharSequence)var1.a())) {
               var2 = true;
            } else {
               var2 = false;
            }

            boolean var3;
            if(var2) {
               if(!kotlin.h.j.a((CharSequence)var1.c())) {
                  var2 = true;
               } else {
                  var2 = false;
               }

               if(var2 && co.uk.getmondo.common.c.c.a(n.this.a(var1.c()), 18L)) {
                  var3 = true;
                  return var3;
               }
            }

            var3 = false;
            return var3;
         }
      })).flatMapMaybe((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.h a(k var1x) {
            kotlin.d.b.l.b(var1x, "it");
            LocalDate var3 = n.this.a(var1x.c());
            if(var3 == null) {
               kotlin.d.b.l.a();
            }

            q var2 = n.this.h;
            String var4 = var1x.a();
            if(var4 == null) {
               throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
            } else {
               var4 = kotlin.h.j.b((CharSequence)var4).toString();
               String var5 = var1x.b();
               if(var5 != null) {
                  if(var5 == null) {
                     throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
                  }

                  var5 = kotlin.h.j.b((CharSequence)var5).toString();
               } else {
                  var5 = null;
               }

               return co.uk.getmondo.common.j.f.a((io.reactivex.b)var2.a(var4, var5, var3).b(n.this.g).a(n.this.f).c((io.reactivex.c.g)(new io.reactivex.c.g() {
                  public final void a(io.reactivex.b.b var1x) {
                     var1.b(true);
                  }
               })).a((io.reactivex.c.g)(new io.reactivex.c.g() {
                  public final void a(Throwable var1x) {
                     n var2 = n.this;
                     kotlin.d.b.l.a(var1x, "it");
                     var2.a(var1x, var1);
                  }
               })), (Object)kotlin.n.a);
            }
         }
      })).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n var1x) {
            var1.l();
         }
      }));
      kotlin.d.b.l.a(var5, "view.onNextClicked()\n   …itted()\n                }");
      this.b = co.uk.getmondo.common.j.f.a(var2, var5);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\n\bf\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003J\b\u0010\u0004\u001a\u00020\u0005H&J\u000e\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u0007H&J\u000e\u0010\b\u001a\b\u0012\u0004\u0012\u00020\t0\u0007H&J\u000e\u0010\n\u001a\b\u0012\u0004\u0012\u00020\t0\u0007H&J\u000e\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\f0\u0007H&J\b\u0010\r\u001a\u00020\u0005H&J\u0010\u0010\u000e\u001a\u00020\u00052\u0006\u0010\u000f\u001a\u00020\u0010H&J\u0010\u0010\u0011\u001a\u00020\u00052\u0006\u0010\u0012\u001a\u00020\u0010H&J\u0014\u0010\u0013\u001a\u00020\u00052\n\b\u0002\u0010\u0014\u001a\u0004\u0018\u00010\fH&J\b\u0010\u0015\u001a\u00020\u0005H&J\b\u0010\u0016\u001a\u00020\u0005H&J\b\u0010\u0017\u001a\u00020\u0005H&J\b\u0010\u0018\u001a\u00020\u0005H&J\b\u0010\u0019\u001a\u00020\u0005H&¨\u0006\u001a"},
      d2 = {"Lco/uk/getmondo/signup/profile/ProfileDetailsPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "Lco/uk/getmondo/signup/SignupApiErrorHandler$View;", "clearDateOfBirthError", "", "onAddPreferredNameClicked", "Lio/reactivex/Observable;", "onDateOfBirthTextChange", "", "onLegalNameTextChange", "onNextClicked", "Lco/uk/getmondo/signup/profile/ProfileDetailsFormData;", "profileDetailsSubmitted", "setNextButtonEnabled", "enabled", "", "setNextButtonLoading", "loading", "showForm", "profileData", "showInvalidDateOfBirthError", "showLegalNameUnsupportedCharError", "showPreferredNameField", "showPreferredNameUnsupportedCharError", "showUnderAgeError", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f, co.uk.getmondo.signup.i.a {
      io.reactivex.n a();

      void a(k var1);

      void a(boolean var1);

      void b(boolean var1);

      io.reactivex.n c();

      io.reactivex.n d();

      io.reactivex.n e();

      void f();

      void g();

      void h();

      void i();

      void j();

      void k();

      void l();
   }

   @Metadata(
      bv = {1, 0, 2},
      k = 3,
      mv = {1, 1, 7}
   )
   public static final class a {
      // $FF: synthetic method
      public static void a(n.a var0, k var1, int var2, Object var3) {
         if(var3 != null) {
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: showForm");
         } else {
            if((var2 & 1) != 0) {
               var1 = (k)null;
            }

            var0.a(var1);
         }
      }
   }
}
