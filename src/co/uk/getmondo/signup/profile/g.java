package co.uk.getmondo.signup.profile;

import co.uk.getmondo.api.model.Address;
import co.uk.getmondo.api.model.tracking.Impression;
import io.reactivex.u;
import java.util.List;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\b\u0004\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0013B3\b\u0007\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b¢\u0006\u0002\u0010\fJ\u0016\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0002J\u0010\u0010\u0012\u001a\u00020\u000e2\u0006\u0010\u0011\u001a\u00020\u0002H\u0016R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0014"},
   d2 = {"Lco/uk/getmondo/signup/profile/ProfileAddressPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/signup/profile/ProfileAddressPresenter$View;", "ioScheduler", "Lio/reactivex/Scheduler;", "uiScheduler", "apiErrorHandler", "Lco/uk/getmondo/common/errors/ApiErrorHandler;", "signupProfileManager", "Lco/uk/getmondo/signup/profile/SignupProfileManager;", "analyticsService", "Lco/uk/getmondo/common/AnalyticsService;", "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/signup/profile/SignupProfileManager;Lco/uk/getmondo/common/AnalyticsService;)V", "handleError", "", "throwable", "", "view", "register", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class g extends co.uk.getmondo.common.ui.b {
   private final u c;
   private final u d;
   private final co.uk.getmondo.common.e.a e;
   private final q f;
   private final co.uk.getmondo.common.a g;

   public g(u var1, u var2, co.uk.getmondo.common.e.a var3, q var4, co.uk.getmondo.common.a var5) {
      kotlin.d.b.l.b(var1, "ioScheduler");
      kotlin.d.b.l.b(var2, "uiScheduler");
      kotlin.d.b.l.b(var3, "apiErrorHandler");
      kotlin.d.b.l.b(var4, "signupProfileManager");
      kotlin.d.b.l.b(var5, "analyticsService");
      super();
      this.c = var1;
      this.d = var2;
      this.e = var3;
      this.f = var4;
      this.g = var5;
   }

   public void a(final g.a var1) {
      kotlin.d.b.l.b(var1, "view");
      super.a((co.uk.getmondo.common.ui.f)var1);
      this.g.a(Impression.Companion.y());
      io.reactivex.b.a var3 = this.b;
      io.reactivex.b.b var2 = var1.a().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n var1x) {
            var1.f();
         }
      }));
      kotlin.d.b.l.a(var2, "view.residencyClicked\n  …ibe { view.showUkOnly() }");
      this.b = co.uk.getmondo.common.j.f.a(var3, var2);
      var3 = this.b;
      var2 = var1.c().flatMapMaybe((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.h a(String var1x) {
            kotlin.d.b.l.b(var1x, "it");
            return co.uk.getmondo.common.j.f.a(q.a(g.this.f, (String)null, var1x, 1, (Object)null).b(g.this.c).a(g.this.d).b((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(io.reactivex.b.b var1x) {
                  var1.a(true);
               }
            })).a((io.reactivex.c.b)(new io.reactivex.c.b() {
               public final void a(List var1x, Throwable var2) {
                  var1.a(false);
               }
            })).d((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(Throwable var1x) {
                  g var2 = g.this;
                  kotlin.d.b.l.a(var1x, "it");
                  var2.a(var1x, var1);
               }
            })));
         }
      })).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(List var1x) {
            var1.g();
            if(var1x.isEmpty()) {
               var1.i();
               var1.h();
            } else {
               g.a var2 = var1;
               kotlin.d.b.l.a(var1x, "it");
               var2.a(var1x);
            }

         }
      }));
      kotlin.d.b.l.a(var2, "view.postcodeReady\n     …      }\n                }");
      this.b = co.uk.getmondo.common.j.f.a(var3, var2);
      var3 = this.b;
      var2 = var1.e().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Address var1x) {
            var1.a(var1x);
         }
      }));
      kotlin.d.b.l.a(var2, "view.addressSelected\n   …AddressConfirmation(it) }");
      this.b = co.uk.getmondo.common.j.f.a(var3, var2);
      io.reactivex.b.a var5 = this.b;
      io.reactivex.b.b var4 = var1.d().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n var1x) {
            g.a.a(var1, (Address)null, 1, (Object)null);
         }
      }));
      kotlin.d.b.l.a(var4, "view.addressNotInListCli…enAddressConfirmation() }");
      this.b = co.uk.getmondo.common.j.f.a(var5, var4);
   }

   public final void a(Throwable var1, g.a var2) {
      kotlin.d.b.l.b(var1, "throwable");
      kotlin.d.b.l.b(var2, "view");
      if(kotlin.h.j.a(co.uk.getmondo.common.e.c.a(var1), "bad_request.bad_param.postal_code", false, 2, (Object)null)) {
         var2.j();
      } else if(!co.uk.getmondo.signup.i.a.a(var1, (co.uk.getmondo.signup.i.a)var2) && !this.e.a(var1, (co.uk.getmondo.common.e.a.a)var2)) {
         var2.b(2131362198);
      }

   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010 \n\u0002\b\u0003\bf\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003J\b\u0010\u0011\u001a\u00020\u0006H&J\u0014\u0010\u0012\u001a\u00020\u00062\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\nH&J\u0010\u0010\u0014\u001a\u00020\u00062\u0006\u0010\u0015\u001a\u00020\u0016H&J\u0010\u0010\u0017\u001a\u00020\u00062\u0006\u0010\u0018\u001a\u00020\u0016H&J\b\u0010\u0019\u001a\u00020\u0006H&J\b\u0010\u001a\u001a\u00020\u0006H&J\u0016\u0010\u001b\u001a\u00020\u00062\f\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\n0\u001dH&J\b\u0010\u001e\u001a\u00020\u0006H&J\b\u0010\u001f\u001a\u00020\u0006H&R\u0018\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0007\u0010\bR\u0018\u0010\t\u001a\b\u0012\u0004\u0012\u00020\n0\u0005X¦\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\bR\u0018\u0010\f\u001a\b\u0012\u0004\u0012\u00020\r0\u0005X¦\u0004¢\u0006\u0006\u001a\u0004\b\u000e\u0010\bR\u0018\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0010\u0010\b¨\u0006 "},
      d2 = {"Lco/uk/getmondo/signup/profile/ProfileAddressPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/signup/SignupApiErrorHandler$View;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "addressNotInListClicked", "Lio/reactivex/Observable;", "", "getAddressNotInListClicked", "()Lio/reactivex/Observable;", "addressSelected", "Lco/uk/getmondo/api/model/Address;", "getAddressSelected", "postcodeReady", "", "getPostcodeReady", "residencyClicked", "getResidencyClicked", "hideContinue", "openAddressConfirmation", "address", "setAddressLoading", "loading", "", "setContinueEnabled", "enabled", "showAddressNotFound", "showAddressNotInListButton", "showAddresses", "addresses", "", "showInvalidPostcodeError", "showUkOnly", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f, co.uk.getmondo.signup.i.a {
      io.reactivex.n a();

      void a(Address var1);

      void a(List var1);

      void a(boolean var1);

      io.reactivex.n c();

      io.reactivex.n d();

      io.reactivex.n e();

      void f();

      void g();

      void h();

      void i();

      void j();
   }

   @Metadata(
      bv = {1, 0, 2},
      k = 3,
      mv = {1, 1, 7}
   )
   public static final class a {
      // $FF: synthetic method
      public static void a(g.a var0, Address var1, int var2, Object var3) {
         if(var3 != null) {
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: openAddressConfirmation");
         } else {
            if((var2 & 1) != 0) {
               var1 = (Address)null;
            }

            var0.a(var1);
         }
      }
   }
}
