package co.uk.getmondo.signup;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.uk.getmondo.common.k;
import co.uk.getmondo.common.s;
import co.uk.getmondo.common.k.p;
import co.uk.getmondo.main.HomeActivity;
import co.uk.getmondo.signup.status.SignupStatusActivity;
import io.reactivex.n;
import java.util.NoSuchElementException;

public class EmailActivity extends co.uk.getmondo.common.activities.b implements f.a {
   k a;
   s b;
   f c;
   @BindView(2131820654)
   TextView content;
   private final com.b.b.b e = com.b.b.b.a();

   // $FF: synthetic method
   static void a(int var0, String var1, co.uk.getmondo.common.f.c var2) {
      var2.a(var0, var1);
   }

   public static void a(Activity var0) {
      var0.startActivity(new Intent(var0, EmailActivity.class));
   }

   private void a(Intent var1) {
      if(var1.getData() != null) {
         String var2 = var1.getData().getQueryParameter("code");
         String var3 = var1.getData().getQueryParameter("state");
         if(p.c(var2) && p.c(var3)) {
            this.e.a((Object)android.support.v4.g.j.a(var2, var3));
         }
      }

   }

   private void a(String var1) {
      this.a((View)this.n());
      this.d.a(c.a(var1));
   }

   // $FF: synthetic method
   static void c(String var0, co.uk.getmondo.common.f.c var1) {
      var1.a(var0);
   }

   public void a() {
      this.startActivity(SignupStatusActivity.a(this, j.b));
      this.finishAffinity();
   }

   public void a(int var1, String var2) {
      this.a((View)this.n());
      this.d.a(d.a(var1, var2));
   }

   public n b() {
      return this.e;
   }

   public void c() {
      this.startActivity(HomeActivity.b((Context)this));
      this.finishAffinity();
   }

   protected void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.l().a(this);
      this.a(this.getIntent());
      this.setContentView(2131034164);
      ButterKnife.bind((Activity)this);
      this.setTitle(2131362708);
      this.content.setText(this.getString(2131362870, new Object[]{this.b.a()}));
      this.c.a((f.a)this);
   }

   protected void onDestroy() {
      this.c.b();
      super.onDestroy();
   }

   protected void onNewIntent(Intent var1) {
      super.onNewIntent(var1);
      this.setIntent(var1);
      this.a(this.getIntent());
   }

   @OnClick({2131820891})
   void onNoEmailClick() {
      this.c.a((Context)this);
   }

   @OnClick({2131820890})
   void openMail() {
      try {
         this.a.a(this);
         this.c.a();
      } catch (NoSuchElementException var2) {
         this.a(var2.getMessage());
      } catch (Exception var3) {
         this.a(this.getString(2131362177));
      }

   }
}
