package co.uk.getmondo.signup;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;

public class EmailActivity_ViewBinding implements Unbinder {
   private EmailActivity a;
   private View b;
   private View c;

   public EmailActivity_ViewBinding(final EmailActivity var1, View var2) {
      this.a = var1;
      var1.content = (TextView)Utils.findRequiredViewAsType(var2, 2131820654, "field 'content'", TextView.class);
      View var3 = Utils.findRequiredView(var2, 2131820890, "method 'openMail'");
      this.b = var3;
      var3.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View var1x) {
            var1.openMail();
         }
      });
      var2 = Utils.findRequiredView(var2, 2131820891, "method 'onNoEmailClick'");
      this.c = var2;
      var2.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View var1x) {
            var1.onNoEmailClick();
         }
      });
   }

   public void unbind() {
      EmailActivity var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.content = null;
         this.b.setOnClickListener((OnClickListener)null);
         this.b = null;
         this.c.setOnClickListener((OnClickListener)null);
         this.c = null;
      }
   }
}
