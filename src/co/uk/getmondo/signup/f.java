package co.uk.getmondo.signup;

import android.content.Context;
import co.uk.getmondo.api.authentication.OAuthException;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.common.k;
import co.uk.getmondo.d.ak;
import io.reactivex.n;
import io.reactivex.u;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0018BC\b\u0007\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\f\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f¢\u0006\u0002\u0010\u0010J\u000e\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014J\u0006\u0010\u0015\u001a\u00020\u0012J\u0010\u0010\u0016\u001a\u00020\u00122\u0006\u0010\u0017\u001a\u00020\u0002H\u0016R\u000e\u0010\f\u001a\u00020\rX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0019"},
   d2 = {"Lco/uk/getmondo/signup/EmailPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/signup/EmailPresenter$EmailView;", "uiScheduler", "Lio/reactivex/Scheduler;", "ioScheduler", "apiErrorHandler", "Lco/uk/getmondo/common/errors/ApiErrorHandler;", "intentBuilder", "Lco/uk/getmondo/common/ExternalIntentBuilder;", "userInteractor", "Lco/uk/getmondo/api/interactors/UserInteractor;", "analyticsService", "Lco/uk/getmondo/common/AnalyticsService;", "pushNotificationRegistration", "Lco/uk/getmondo/fcm/PushNotificationRegistration;", "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/common/ExternalIntentBuilder;Lco/uk/getmondo/api/interactors/UserInteractor;Lco/uk/getmondo/common/AnalyticsService;Lco/uk/getmondo/fcm/PushNotificationRegistration;)V", "onNoEmail", "", "context", "Landroid/content/Context;", "onOpenEmailApp", "register", "view", "EmailView", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class f extends co.uk.getmondo.common.ui.b {
   private final u c;
   private final u d;
   private final co.uk.getmondo.common.e.a e;
   private final k f;
   private final co.uk.getmondo.api.b.a g;
   private final co.uk.getmondo.common.a h;
   private final co.uk.getmondo.fcm.e i;

   public f(u var1, u var2, co.uk.getmondo.common.e.a var3, k var4, co.uk.getmondo.api.b.a var5, co.uk.getmondo.common.a var6, co.uk.getmondo.fcm.e var7) {
      l.b(var1, "uiScheduler");
      l.b(var2, "ioScheduler");
      l.b(var3, "apiErrorHandler");
      l.b(var4, "intentBuilder");
      l.b(var5, "userInteractor");
      l.b(var6, "analyticsService");
      l.b(var7, "pushNotificationRegistration");
      super();
      this.c = var1;
      this.d = var2;
      this.e = var3;
      this.f = var4;
      this.g = var5;
      this.h = var6;
      this.i = var7;
   }

   public final void a() {
      this.h.a(Impression.Companion.h());
   }

   public final void a(Context var1) {
      l.b(var1, "context");
      this.h.a(Impression.Companion.i());
      this.f.a(var1, "https://monzo.com/faq/no-email/");
   }

   public void a(final f.a var1) {
      l.b(var1, "view");
      super.a((co.uk.getmondo.common.ui.f)var1);
      this.h.a(Impression.Companion.g());
      io.reactivex.b.a var3 = this.b;
      n var4 = var1.b().flatMapMaybe((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.h a(android.support.v4.g.j var1x) {
            l.b(var1x, "codeAndState");
            return f.this.g.a((String)var1x.a, (String)var1x.b).e().a(f.this.c).b(f.this.d).a((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(Throwable var1x) {
                  if(var1x instanceof OAuthException) {
                     var1.a(2131362176, ((OAuthException)var1x).a());
                  } else if(l.a((co.uk.getmondo.api.authentication.a)co.uk.getmondo.common.e.c.a(var1x, (co.uk.getmondo.common.e.f[])co.uk.getmondo.api.authentication.a.values()), co.uk.getmondo.api.authentication.a.e)) {
                     var1.b(2131362175);
                  } else {
                     co.uk.getmondo.common.e.a var2 = f.this.e;
                     l.a(var1x, "error");
                     if(!var2.a(var1x, (co.uk.getmondo.common.e.a.a)var1)) {
                        var1.b(2131362408);
                     }
                  }

               }
            })).a((io.reactivex.l)io.reactivex.h.a());
         }
      })).observeOn(this.c);
      io.reactivex.c.g var5 = (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(ak var1x) {
            if(var1x.a() != ak.a.NO_PROFILE) {
               f.this.i.a();
            }

            if(!co.uk.getmondo.a.c.booleanValue()) {
               co.uk.getmondo.d.a var2 = var1x.c();
               if(var2 != null && var2.f()) {
                  var1.c();
                  return;
               }
            }

            var1.a();
         }
      });
      kotlin.d.a.b var2 = (kotlin.d.a.b)null.a;
      Object var6 = var2;
      if(var2 != null) {
         var6 = new g(var2);
      }

      io.reactivex.b.b var7 = var4.subscribe(var5, (io.reactivex.c.g)var6);
      l.a(var7, "view.onMagicLinkChanged(…            }, Timber::e)");
      io.reactivex.rxkotlin.a.a(var3, var7);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\bf\u0018\u00002\u00020\u00012\u00020\u0002J\u001a\u0010\u0003\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00060\u00050\u0004H&J\b\u0010\u0007\u001a\u00020\bH&J\b\u0010\t\u001a\u00020\bH&J\u001a\u0010\n\u001a\u00020\b2\b\b\u0001\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u0006H&¨\u0006\u000e"},
      d2 = {"Lco/uk/getmondo/signup/EmailPresenter$EmailView;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "onMagicLinkChanged", "Lio/reactivex/Observable;", "Landroid/support/v4/util/Pair;", "", "openHome", "", "openSignupStatus", "showStickyError", "stringRes", "", "formatArg", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      void a();

      void a(int var1, String var2);

      n b();

      void c();
   }
}
