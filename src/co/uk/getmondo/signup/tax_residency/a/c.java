package co.uk.getmondo.signup.tax_residency.a;

import co.uk.getmondo.api.TaxResidencyApi;
import co.uk.getmondo.api.model.tax_residency.Jurisdiction;
import co.uk.getmondo.api.model.tax_residency.TaxResidencyInfo;
import io.reactivex.v;
import io.reactivex.c.h;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.a.m;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000R\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\u0018\u0000 \u001f2\u00020\u0001:\u0001\u001fB\u000f\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\"\u0010\u0005\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\u00062\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\b0\nH\u0002J(\u0010\u000b\u001a\u0012\u0012\u0004\u0012\u00020\r0\fj\b\u0012\u0004\u0012\u00020\r`\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H\u0002J\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00100\u0006J$\u0010\u0014\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\r0\fj\b\u0012\u0004\u0012\u00020\r`\u000e0\u00062\u0006\u0010\u0011\u001a\u00020\u0012J\u0012\u0010\u0015\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\u0006J\u001e\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\b2\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u001aJ\u0006\u0010\u001c\u001a\u00020\u0017J\u001c\u0010\u001d\u001a\u00020\u00172\u0006\u0010\u0011\u001a\u00020\u00122\f\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\r0\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006 "},
   d2 = {"Lco/uk/getmondo/signup/tax_residency/data/TaxResidencyManager;", "", "api", "Lco/uk/getmondo/api/TaxResidencyApi;", "(Lco/uk/getmondo/api/TaxResidencyApi;)V", "checkIsLastJurisdiction", "Lio/reactivex/Single;", "Lcom/memoizrlabs/poweroptional/Optional;", "Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;", "jurisdictions", "", "generateTaxCountriesList", "Ljava/util/ArrayList;", "Lco/uk/getmondo/signup/tax_residency/data/TaxCountry;", "Lkotlin/collections/ArrayList;", "taxResidency", "Lco/uk/getmondo/api/model/tax_residency/TaxResidencyInfo;", "isUsTaxResident", "", "getStatus", "getTaxCountries", "nextJurisdictionOrSubmit", "reportTin", "Lio/reactivex/Completable;", "jurisdiction", "tinId", "", "tinValue", "submitUkTaxResidentOnly", "updateSelfCertification", "countries", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class c {
   public static final c.a a = new c.a((i)null);
   private final TaxResidencyApi b;

   public c(TaxResidencyApi var1) {
      l.b(var1, "api");
      super();
      this.b = var1;
   }

   private final v a(List var1) {
      v var2;
      if(var1.isEmpty()) {
         var2 = this.b.submit().a((Object)com.c.b.b.c());
         l.a(var2, "api.submit()\n           …Default(Optional.empty())");
      } else {
         var2 = v.a((Object)com.c.b.b.b(var1.get(0)));
         l.a(var2, "Single.just(Optional.optionOf(jurisdictions[0]))");
      }

      return var2;
   }

   private final ArrayList a(TaxResidencyInfo var1, boolean var2) {
      ArrayList var7 = new ArrayList();
      Iterator var6 = co.uk.getmondo.d.i.Companion.b().iterator();
      int var3 = 0;

      while(var6.hasNext()) {
         co.uk.getmondo.d.i var8 = (co.uk.getmondo.d.i)var6.next();
         String var9 = var8.f();
         String var10 = var8.g();
         String var12 = var8.h();
         boolean var5;
         if(!l.a(var12, co.uk.getmondo.d.i.UNITED_KINGDOM.e()) && (!l.a(var12, co.uk.getmondo.d.i.UNITED_STATES.e()) || !var2)) {
            var5 = false;
         } else {
            var5 = true;
         }

         List var11 = var1.d();
         boolean var4;
         if(var11 != null) {
            Iterable var14 = (Iterable)var11;
            if(var14 instanceof Collection && ((Collection)var14).isEmpty()) {
               var4 = false;
            } else {
               Iterator var15 = var14.iterator();

               while(true) {
                  if(!var15.hasNext()) {
                     var4 = false;
                     break;
                  }

                  if(l.a(((Jurisdiction)var15.next()).e(), var12)) {
                     var4 = true;
                     break;
                  }
               }
            }
         } else {
            var4 = false;
         }

         a var13 = new a(var9, var10, var12, var5, var4);
         if(var1.c().contains(var12)) {
            var7.add(var3, var13);
            ++var3;
         } else {
            var7.add(var13);
         }
      }

      return var7;
   }

   public final io.reactivex.b a(Jurisdiction var1, String var2, String var3) {
      l.b(var1, "jurisdiction");
      l.b(var2, "tinId");
      l.b(var3, "tinValue");
      io.reactivex.b var4;
      if(l.a(var3, "NO_TIN") && var1.h()) {
         var4 = this.b.noTin(var1.e());
      } else {
         var4 = this.b.updateTin(var1.e(), var2, var3);
      }

      return var4;
   }

   public final io.reactivex.b a(boolean var1, List var2) {
      l.b(var2, "countries");
      Iterable var3 = (Iterable)var2;
      Collection var4 = (Collection)(new ArrayList(m.a(var3, 10)));
      Iterator var6 = var3.iterator();

      while(var6.hasNext()) {
         var4.add(((a)var6.next()).c());
      }

      var4 = (Collection)((List)var4);
      Object[] var5 = var4.toArray(new String[var4.size()]);
      if(var5 == null) {
         throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
      } else {
         String[] var7 = (String[])var5;
         return this.b.updateSelfCertification(false, var1, var7);
      }
   }

   public final v a() {
      return this.b.status();
   }

   public final v a(final boolean var1) {
      v var2 = this.a().d((h)(new h() {
         public final ArrayList a(TaxResidencyInfo var1x) {
            l.b(var1x, "taxResidencyInfo");
            return c.this.a(var1x, var1);
         }
      }));
      l.a(var2, "getStatus()\n            …yInfo, isUsTaxResident) }");
      return var2;
   }

   public final v b() {
      v var1 = this.a().d((h)null.a).a((h)(new h() {
         public final v a(List var1) {
            l.b(var1, "it");
            return c.this.a(var1);
         }
      }));
      l.a(var1, "getStatus()\n            …kIsLastJurisdiction(it) }");
      return var1;
   }

   public final io.reactivex.b c() {
      io.reactivex.b var1 = this.b.updateSelfCertification(true, false, (String[])((Object[])(new String[]{co.uk.getmondo.d.i.UNITED_KINGDOM.e()}))).b((io.reactivex.d)this.b.submit());
      l.a(var1, "api.updateSelfCertificat…   .andThen(api.submit())");
      return var1;
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000¨\u0006\u0005"},
      d2 = {"Lco/uk/getmondo/signup/tax_residency/data/TaxResidencyManager$Companion;", "", "()V", "NO_TIN", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(i var1) {
         this();
      }
   }
}
