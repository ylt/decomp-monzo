package co.uk.getmondo.signup.tax_residency.b;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import co.uk.getmondo.common.ui.ProgressButton;
import java.util.HashMap;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\\\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0002\u0018\u00002\u00020\u00012\u00020\u0002:\u0001-B\u0005¢\u0006\u0002\u0010\u0003J\b\u0010\u0019\u001a\u00020\u000eH\u0016J\b\u0010\u001a\u001a\u00020\u000eH\u0016J\u0010\u0010\u001b\u001a\u00020\u000e2\u0006\u0010\u001c\u001a\u00020\u001dH\u0016J\u0012\u0010\u001e\u001a\u00020\u000e2\b\u0010\u001f\u001a\u0004\u0018\u00010 H\u0016J&\u0010!\u001a\u0004\u0018\u00010\"2\u0006\u0010#\u001a\u00020$2\b\u0010%\u001a\u0004\u0018\u00010&2\b\u0010\u001f\u001a\u0004\u0018\u00010 H\u0016J\b\u0010'\u001a\u00020\u000eH\u0016J\u001c\u0010(\u001a\u00020\u000e2\b\u0010)\u001a\u0004\u0018\u00010\"2\b\u0010\u001f\u001a\u0004\u0018\u00010 H\u0016J\u0010\u0010*\u001a\u00020\u000e2\u0006\u0010+\u001a\u00020,H\u0016R\u001e\u0010\u0004\u001a\u00020\u00058\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\tR\u000e\u0010\n\u001a\u00020\u000bX\u0082.¢\u0006\u0002\n\u0000R\u001a\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000e0\r8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u000f\u0010\u0010R\u001a\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u000e0\r8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0012\u0010\u0010R\u001e\u0010\u0013\u001a\u00020\u00148\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\u0015\u0010\u0016\"\u0004\b\u0017\u0010\u0018¨\u0006."},
   d2 = {"Lco/uk/getmondo/signup/tax_residency/step/TaxResidencySummaryFragment;", "Lco/uk/getmondo/common/fragments/BaseFragment;", "Lco/uk/getmondo/signup/tax_residency/step/TaxResidencySummaryPresenter$View;", "()V", "analyticsService", "Lco/uk/getmondo/common/AnalyticsService;", "getAnalyticsService", "()Lco/uk/getmondo/common/AnalyticsService;", "setAnalyticsService", "(Lco/uk/getmondo/common/AnalyticsService;)V", "listener", "Lco/uk/getmondo/signup/tax_residency/step/TaxResidencySummaryFragment$StepListener;", "onOtherCountriesClicked", "Lio/reactivex/Observable;", "", "getOnOtherCountriesClicked", "()Lio/reactivex/Observable;", "onUkOnlyClicked", "getOnUkOnlyClicked", "taxResidencySummaryPresenter", "Lco/uk/getmondo/signup/tax_residency/step/TaxResidencySummaryPresenter;", "getTaxResidencySummaryPresenter", "()Lco/uk/getmondo/signup/tax_residency/step/TaxResidencySummaryPresenter;", "setTaxResidencySummaryPresenter", "(Lco/uk/getmondo/signup/tax_residency/step/TaxResidencySummaryPresenter;)V", "completeStage", "goToNextStep", "onAttach", "context", "Landroid/content/Context;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "onDestroyView", "onViewCreated", "view", "setButtonLoading", "loading", "", "StepListener", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class g extends co.uk.getmondo.common.f.a implements i.a {
   public co.uk.getmondo.common.a a;
   public i c;
   private g.a d;
   private HashMap e;

   public View a(int var1) {
      if(this.e == null) {
         this.e = new HashMap();
      }

      View var3 = (View)this.e.get(Integer.valueOf(var1));
      View var2 = var3;
      if(var3 == null) {
         var2 = this.getView();
         if(var2 == null) {
            var2 = null;
         } else {
            var2 = var2.findViewById(var1);
            this.e.put(Integer.valueOf(var1), var2);
         }
      }

      return var2;
   }

   public io.reactivex.n a() {
      io.reactivex.n var1 = com.b.a.c.c.a((ProgressButton)this.a(co.uk.getmondo.c.a.taxResidencyJustUkButton)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var1, "RxView.clicks(this).map(VoidToUnit)");
      return var1;
   }

   public void a(boolean var1) {
      ((ProgressButton)this.a(co.uk.getmondo.c.a.taxResidencyJustUkButton)).setLoading(var1);
   }

   public io.reactivex.n b() {
      io.reactivex.n var1 = com.b.a.c.c.a((Button)this.a(co.uk.getmondo.c.a.taxResidencyElsewhereButton)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var1, "RxView.clicks(this).map(VoidToUnit)");
      return var1;
   }

   public void c() {
      g.a var1 = this.d;
      if(var1 == null) {
         kotlin.d.b.l.b("listener");
      }

      var1.i();
   }

   public void d() {
      g.a var1 = this.d;
      if(var1 == null) {
         kotlin.d.b.l.b("listener");
      }

      var1.j();
   }

   public void e() {
      if(this.e != null) {
         this.e.clear();
      }

   }

   public void onAttach(Context var1) {
      kotlin.d.b.l.b(var1, "context");
      super.onAttach(var1);
      if(var1 instanceof g.a) {
         this.d = (g.a)var1;
      } else {
         throw (Throwable)(new IllegalStateException("Activity must implement TaxResidencySummaryFragment.StepListener"));
      }
   }

   public void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.B().a(this);
   }

   public View onCreateView(LayoutInflater var1, ViewGroup var2, Bundle var3) {
      kotlin.d.b.l.b(var1, "inflater");
      return var1.inflate(2131034285, var2, false);
   }

   public void onDestroyView() {
      i var1 = this.c;
      if(var1 == null) {
         kotlin.d.b.l.b("taxResidencySummaryPresenter");
      }

      var1.b();
      super.onDestroyView();
      this.e();
   }

   public void onViewCreated(View var1, Bundle var2) {
      super.onViewCreated(var1, var2);
      i var3 = this.c;
      if(var3 == null) {
         kotlin.d.b.l.b("taxResidencySummaryPresenter");
      }

      var3.a((i.a)this);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&¨\u0006\u0004"},
      d2 = {"Lco/uk/getmondo/signup/tax_residency/step/TaxResidencySummaryFragment$StepListener;", "Lco/uk/getmondo/signup/tax_residency/TaxResidencyStageListener;", "onSummaryStepComplete", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.signup.tax_residency.f {
      void i();
   }
}
