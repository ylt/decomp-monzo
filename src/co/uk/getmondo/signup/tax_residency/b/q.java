package co.uk.getmondo.signup.tax_residency.b;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import co.uk.getmondo.common.ui.ProgressButton;
import java.util.ArrayList;
import java.util.HashMap;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000d\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u00002\u00020\u00012\u00020\u0002:\u0001*B\u0005¢\u0006\u0002\u0010\u0003J(\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\b2\u0016\u0010\u0014\u001a\u0012\u0012\u0004\u0012\u00020\u00160\u0015j\b\u0012\u0004\u0012\u00020\u0016`\u0017H\u0016J\u0010\u0010\u0018\u001a\u00020\u00122\u0006\u0010\u0019\u001a\u00020\u001aH\u0016J\u0012\u0010\u001b\u001a\u00020\u00122\b\u0010\u001c\u001a\u0004\u0018\u00010\u001dH\u0016J&\u0010\u001e\u001a\u0004\u0018\u00010\u001f2\u0006\u0010 \u001a\u00020!2\b\u0010\"\u001a\u0004\u0018\u00010#2\b\u0010\u001c\u001a\u0004\u0018\u00010\u001dH\u0016J\b\u0010$\u001a\u00020\u0012H\u0016J\u001c\u0010%\u001a\u00020\u00122\b\u0010&\u001a\u0004\u0018\u00010\u001f2\b\u0010\u001c\u001a\u0004\u0018\u00010\u001dH\u0016J\u0010\u0010'\u001a\u00020\u00122\u0006\u0010(\u001a\u00020\bH\u0016J\u0010\u0010)\u001a\u00020\u00122\u0006\u0010(\u001a\u00020\bH\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082.¢\u0006\u0002\n\u0000R\u001a\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u00078VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\t\u0010\nR\u001e\u0010\u000b\u001a\u00020\f8\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010¨\u0006+"},
   d2 = {"Lco/uk/getmondo/signup/tax_residency/step/TaxResidencyUsResidentFragment;", "Lco/uk/getmondo/common/fragments/BaseFragment;", "Lco/uk/getmondo/signup/tax_residency/step/TaxResidencyUsResidentPresenter$View;", "()V", "listener", "Lco/uk/getmondo/signup/tax_residency/step/TaxResidencyUsResidentFragment$StepListener;", "onUsResidentSelected", "Lio/reactivex/Observable;", "", "getOnUsResidentSelected", "()Lio/reactivex/Observable;", "taxResidencyUsResidentPresenter", "Lco/uk/getmondo/signup/tax_residency/step/TaxResidencyUsResidentPresenter;", "getTaxResidencyUsResidentPresenter", "()Lco/uk/getmondo/signup/tax_residency/step/TaxResidencyUsResidentPresenter;", "setTaxResidencyUsResidentPresenter", "(Lco/uk/getmondo/signup/tax_residency/step/TaxResidencyUsResidentPresenter;)V", "goToNextStep", "", "isUsTaxResident", "taxCountries", "Ljava/util/ArrayList;", "Lco/uk/getmondo/signup/tax_residency/data/TaxCountry;", "Lkotlin/collections/ArrayList;", "onAttach", "context", "Landroid/content/Context;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "onDestroyView", "onViewCreated", "view", "setNonUsButtonLoading", "loading", "setUsButtonLoading", "StepListener", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class q extends co.uk.getmondo.common.f.a implements s.a {
   public s a;
   private q.a c;
   private HashMap d;

   public View a(int var1) {
      if(this.d == null) {
         this.d = new HashMap();
      }

      View var3 = (View)this.d.get(Integer.valueOf(var1));
      View var2 = var3;
      if(var3 == null) {
         var2 = this.getView();
         if(var2 == null) {
            var2 = null;
         } else {
            var2 = var2.findViewById(var1);
            this.d.put(Integer.valueOf(var1), var2);
         }
      }

      return var2;
   }

   public io.reactivex.n a() {
      io.reactivex.n var1 = com.b.a.c.c.a((ProgressButton)this.a(co.uk.getmondo.c.a.taxResidencyUsCitizenButton)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var1, "RxView.clicks(this).map(VoidToUnit)");
      io.reactivex.r var2 = (io.reactivex.r)var1.map((io.reactivex.c.h)null.a);
      var1 = com.b.a.c.c.a((ProgressButton)this.a(co.uk.getmondo.c.a.taxResidencyNonUsCitizenButton)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var1, "RxView.clicks(this).map(VoidToUnit)");
      var1 = io.reactivex.n.merge(var2, (io.reactivex.r)var1.map((io.reactivex.c.h)null.a));
      kotlin.d.b.l.a(var1, "Observable.merge(\n      …          .map { false })");
      return var1;
   }

   public void a(boolean var1) {
      ((ProgressButton)this.a(co.uk.getmondo.c.a.taxResidencyUsCitizenButton)).setLoading(var1);
   }

   public void a(boolean var1, ArrayList var2) {
      kotlin.d.b.l.b(var2, "taxCountries");
      q.a var3 = this.c;
      if(var3 == null) {
         kotlin.d.b.l.b("listener");
      }

      var3.a(var1, var2);
   }

   public void b() {
      if(this.d != null) {
         this.d.clear();
      }

   }

   public void b(boolean var1) {
      ((ProgressButton)this.a(co.uk.getmondo.c.a.taxResidencyNonUsCitizenButton)).setLoading(var1);
   }

   public void onAttach(Context var1) {
      kotlin.d.b.l.b(var1, "context");
      super.onAttach(var1);
      if(var1 instanceof q.a) {
         this.c = (q.a)var1;
      } else {
         throw (Throwable)(new IllegalStateException("Activity must implement TaxResidencyUsResidentFragment.StepListener"));
      }
   }

   public void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.B().a(this);
   }

   public View onCreateView(LayoutInflater var1, ViewGroup var2, Bundle var3) {
      kotlin.d.b.l.b(var1, "inflater");
      return var1.inflate(2131034287, var2, false);
   }

   public void onDestroyView() {
      super.onDestroyView();
      s var1 = this.a;
      if(var1 == null) {
         kotlin.d.b.l.b("taxResidencyUsResidentPresenter");
      }

      var1.b();
      this.b();
   }

   public void onViewCreated(View var1, Bundle var2) {
      super.onViewCreated(var1, var2);
      s var3 = this.a;
      if(var3 == null) {
         kotlin.d.b.l.b("taxResidencyUsResidentPresenter");
      }

      var3.a((s.a)this);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J(\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0016\u0010\u0006\u001a\u0012\u0012\u0004\u0012\u00020\b0\u0007j\b\u0012\u0004\u0012\u00020\b`\tH&¨\u0006\n"},
      d2 = {"Lco/uk/getmondo/signup/tax_residency/step/TaxResidencyUsResidentFragment$StepListener;", "Lco/uk/getmondo/signup/tax_residency/TaxResidencyStageListener;", "onUsResidentStepCompleted", "", "isUsTaxResident", "", "taxCountries", "Ljava/util/ArrayList;", "Lco/uk/getmondo/signup/tax_residency/data/TaxCountry;", "Lkotlin/collections/ArrayList;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.signup.tax_residency.f {
      void a(boolean var1, ArrayList var2);
   }
}
