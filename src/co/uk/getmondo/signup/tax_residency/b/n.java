package co.uk.getmondo.signup.tax_residency.b;

import co.uk.getmondo.api.ApiException;
import co.uk.getmondo.api.model.tax_residency.Jurisdiction;
import co.uk.getmondo.api.model.tracking.Impression;
import io.reactivex.z;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0018B3\b\u0007\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b¢\u0006\u0002\u0010\fJ\u0018\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0002H\u0002J\u001a\u0010\u0014\u001a\u0004\u0018\u00010\u000e2\u0006\u0010\u0015\u001a\u00020\u000e2\u0006\u0010\u0011\u001a\u00020\u0012H\u0002J\u0010\u0010\u0016\u001a\u00020\u00102\u0006\u0010\u0013\u001a\u00020\u0002H\u0016J \u0010\u0017\u001a\u00020\u00102\u0006\u0010\u0015\u001a\u00020\u000e2\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0002H\u0002R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082.¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0019"},
   d2 = {"Lco/uk/getmondo/signup/tax_residency/step/TaxResidencyTinEntryPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/signup/tax_residency/step/TaxResidencyTinEntryPresenter$View;", "ioScheduler", "Lio/reactivex/Scheduler;", "uiScheduler", "taxResidencyManager", "Lco/uk/getmondo/signup/tax_residency/data/TaxResidencyManager;", "apiErrorHandler", "Lco/uk/getmondo/common/errors/ApiErrorHandler;", "analyticsService", "Lco/uk/getmondo/common/AnalyticsService;", "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/signup/tax_residency/data/TaxResidencyManager;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/common/AnalyticsService;)V", "currentTinType", "Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;", "displayJurisdiction", "", "jurisdiction", "Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;", "view", "getAltTinType", "tinType", "register", "updateTinTypes", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class n extends co.uk.getmondo.common.ui.b {
   private Jurisdiction.TinType c;
   private final io.reactivex.u d;
   private final io.reactivex.u e;
   private final co.uk.getmondo.signup.tax_residency.a.c f;
   private final co.uk.getmondo.common.e.a g;
   private final co.uk.getmondo.common.a h;

   public n(io.reactivex.u var1, io.reactivex.u var2, co.uk.getmondo.signup.tax_residency.a.c var3, co.uk.getmondo.common.e.a var4, co.uk.getmondo.common.a var5) {
      kotlin.d.b.l.b(var1, "ioScheduler");
      kotlin.d.b.l.b(var2, "uiScheduler");
      kotlin.d.b.l.b(var3, "taxResidencyManager");
      kotlin.d.b.l.b(var4, "apiErrorHandler");
      kotlin.d.b.l.b(var5, "analyticsService");
      super();
      this.d = var1;
      this.e = var2;
      this.f = var3;
      this.g = var4;
      this.h = var5;
   }

   private final Jurisdiction.TinType a(Jurisdiction.TinType var1, Jurisdiction var2) {
      if(kotlin.d.b.l.a(var1, var2.c())) {
         var1 = var2.d();
      } else {
         var1 = var2.c();
      }

      return var1;
   }

   // $FF: synthetic method
   public static final Jurisdiction.TinType a(n var0) {
      Jurisdiction.TinType var1 = var0.c;
      if(var1 == null) {
         kotlin.d.b.l.b("currentTinType");
      }

      return var1;
   }

   private final void a(Jurisdiction.TinType var1, Jurisdiction var2, n.a var3) {
      var3.a(var1);
      var1 = this.a(var1, var2);
      if(var1 != null) {
         var3.b(var1);
      } else {
         var3.f();
      }

   }

   private final void a(Jurisdiction var1, n.a var2) {
      this.c = var1.c();
      Jurisdiction.TinType var3 = this.c;
      if(var3 == null) {
         kotlin.d.b.l.b("currentTinType");
      }

      this.a(var3, var1, var2);
      if(var1.h()) {
         var2.a(var1.b());
      } else {
         var2.g();
      }

   }

   public void a(final n.a var1) {
      kotlin.d.b.l.b(var1, "view");
      super.a((co.uk.getmondo.common.ui.f)var1);
      this.h.a(Impression.Companion.aG());
      final Jurisdiction var4 = var1.a();
      this.a(var4, var1);
      io.reactivex.b.a var5 = this.b;
      io.reactivex.n var6 = var1.c();
      io.reactivex.c.g var7 = (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(CharSequence var1x) {
            boolean var3 = true;
            n.a var4 = var1;
            boolean var2;
            if(var1x != null && !kotlin.h.j.a(var1x)) {
               var2 = false;
            } else {
               var2 = true;
            }

            if(var2) {
               var3 = false;
            }

            var4.b(var3);
            var1.h();
         }
      });
      kotlin.d.a.b var3 = (kotlin.d.a.b)null.a;
      Object var2 = var3;
      if(var3 != null) {
         var2 = new o(var3);
      }

      io.reactivex.b.b var10 = var6.subscribe(var7, (io.reactivex.c.g)var2);
      kotlin.d.b.l.a(var10, "view.onTinChanges\n      …            }, Timber::e)");
      this.b = co.uk.getmondo.common.j.f.a(var5, var10);
      var5 = this.b;
      io.reactivex.n var16 = var1.b().doOnNext((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n var1) {
            n var2 = n.this;
            Jurisdiction.TinType var3 = n.this.a(n.a(n.this), var4);
            if(var3 == null) {
               kotlin.d.b.l.a();
            }

            var2.c = var3;
         }
      }));
      io.reactivex.c.g var15 = (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n var1x) {
            n.this.a(n.a(n.this), var4, var1);
         }
      });
      var3 = (kotlin.d.a.b)null.a;
      var2 = var3;
      if(var3 != null) {
         var2 = new o(var3);
      }

      var10 = var16.subscribe(var15, (io.reactivex.c.g)var2);
      kotlin.d.b.l.a(var10, "view.onAltTinClicked\n   …tion, view) }, Timber::e)");
      this.b = co.uk.getmondo.common.j.f.a(var5, var10);
      io.reactivex.b.a var11 = this.b;
      io.reactivex.n var12 = var1.d().mergeWith((io.reactivex.r)var1.e().map((io.reactivex.c.h)null.a)).filter((io.reactivex.c.q)null.a).map((io.reactivex.c.h)null.a).flatMapMaybe((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.h a(final String var1x) {
            kotlin.d.b.l.b(var1x, "tinValue");
            return co.uk.getmondo.common.j.f.a(n.this.f.a(var4, n.a(n.this).a(), var1x).a((z)n.this.f.b()).b(n.this.d).a(n.this.e).b((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(io.reactivex.b.b var1xx) {
                  if(kotlin.d.b.l.a(var1x, "NO_TIN") ^ true) {
                     var1.c(true);
                  } else {
                     var1.d(true);
                  }

               }
            })).d((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(Throwable var1x) {
                  var1.c(false);
                  var1.d(false);
                  if(var1x instanceof ApiException) {
                     co.uk.getmondo.common.e.f[] var3 = (co.uk.getmondo.common.e.f[])co.uk.getmondo.signup.tax_residency.a.b.values();
                     co.uk.getmondo.api.model.b var2 = ((ApiException)var1x).e();
                     String var4x;
                     if(var2 != null) {
                        var4x = var2.a();
                     } else {
                        var4x = null;
                     }

                     if(kotlin.d.b.l.a((co.uk.getmondo.signup.tax_residency.a.b)co.uk.getmondo.common.e.d.a(var3, var4x), co.uk.getmondo.signup.tax_residency.a.b.a)) {
                        var1.c(n.a(n.this));
                        return;
                     }
                  }

                  co.uk.getmondo.common.e.a var5 = n.this.g;
                  kotlin.d.b.l.a(var1x, "it");
                  var5.a(var1x, (co.uk.getmondo.common.e.a.a)var1);
               }
            })));
         }
      }));
      io.reactivex.c.g var14 = (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(com.c.b.b var1x) {
            if(var1x.d()) {
               var1.i();
            } else {
               n.a var2 = var1;
               Object var3 = var1x.a();
               kotlin.d.b.l.a(var3, "nextJurisdiction.get()");
               var2.a((Jurisdiction)var3);
            }

         }
      });
      kotlin.d.a.b var13 = (kotlin.d.a.b)null.a;
      Object var8 = var13;
      if(var13 != null) {
         var8 = new o(var13);
      }

      io.reactivex.b.b var9 = var12.subscribe(var14, (io.reactivex.c.g)var8);
      kotlin.d.b.l.a(var9, "view.onContinueClicked\n …            }, Timber::e)");
      this.b = co.uk.getmondo.common.j.f.a(var11, var9);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\r\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0006\bf\u0018\u00002\u00020\u00012\u00020\u0002J\b\u0010\u0014\u001a\u00020\tH&J\u0010\u0010\u0015\u001a\u00020\t2\u0006\u0010\u0016\u001a\u00020\u0004H&J\b\u0010\u0017\u001a\u00020\tH&J\b\u0010\u0018\u001a\u00020\tH&J\b\u0010\u0019\u001a\u00020\tH&J\u0010\u0010\u001a\u001a\u00020\t2\u0006\u0010\u001b\u001a\u00020\u001cH&J\u0010\u0010\u001d\u001a\u00020\t2\u0006\u0010\u001e\u001a\u00020\u001cH&J\u0010\u0010\u001f\u001a\u00020\t2\u0006\u0010\u001e\u001a\u00020\u001cH&J\u0010\u0010 \u001a\u00020\t2\u0006\u0010!\u001a\u00020\"H&J\u0010\u0010#\u001a\u00020\t2\u0006\u0010$\u001a\u00020\"H&J\u0010\u0010%\u001a\u00020\t2\u0006\u0010&\u001a\u00020\u001cH&J\u0010\u0010'\u001a\u00020\t2\u0006\u0010$\u001a\u00020\"H&R\u0012\u0010\u0003\u001a\u00020\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0005\u0010\u0006R\u0018\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\bX¦\u0004¢\u0006\u0006\u001a\u0004\b\n\u0010\u000bR\u0018\u0010\f\u001a\b\u0012\u0004\u0012\u00020\r0\bX¦\u0004¢\u0006\u0006\u001a\u0004\b\u000e\u0010\u000bR\u0018\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\t0\bX¦\u0004¢\u0006\u0006\u001a\u0004\b\u0010\u0010\u000bR\u0018\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00120\bX¦\u0004¢\u0006\u0006\u001a\u0004\b\u0013\u0010\u000b¨\u0006("},
      d2 = {"Lco/uk/getmondo/signup/tax_residency/step/TaxResidencyTinEntryPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "jurisdiction", "Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;", "getJurisdiction", "()Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;", "onAltTinClicked", "Lio/reactivex/Observable;", "", "getOnAltTinClicked", "()Lio/reactivex/Observable;", "onContinueClicked", "", "getOnContinueClicked", "onNoTinClicked", "getOnNoTinClicked", "onTinChanges", "", "getOnTinChanges", "completeStage", "goToNextStep", "nextJurisdiction", "hideAltTinType", "hideInvalidTinError", "hideNoTin", "setContinueButtonEnabled", "enabled", "", "setContinueButtonLoading", "loading", "setNoTinButtonLoading", "showAltTinType", "altTinType", "Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;", "showInvalidTinError", "tinType", "showNoTin", "isPlural", "showTinType", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      Jurisdiction a();

      void a(Jurisdiction.TinType var1);

      void a(Jurisdiction var1);

      void a(boolean var1);

      io.reactivex.n b();

      void b(Jurisdiction.TinType var1);

      void b(boolean var1);

      io.reactivex.n c();

      void c(Jurisdiction.TinType var1);

      void c(boolean var1);

      io.reactivex.n d();

      void d(boolean var1);

      io.reactivex.n e();

      void f();

      void g();

      void h();

      void i();
   }
}
