package co.uk.getmondo.signup.tax_residency.b;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.ak;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import co.uk.getmondo.api.model.tax_residency.Jurisdiction;
import co.uk.getmondo.common.ui.ProgressButton;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import kotlin.Metadata;
import kotlin.TypeCastException;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000r\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010 \n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\n\u0018\u0000 42\u00020\u00012\u00020\u0002:\u000245B\u0005¢\u0006\u0002\u0010\u0003J\b\u0010\u001d\u001a\u00020\rH\u0016J\u0010\u0010\u001e\u001a\u00020\r2\u0006\u0010\u001f\u001a\u00020 H\u0016J\u0010\u0010!\u001a\u00020\r2\u0006\u0010\"\u001a\u00020#H\u0016J\u0012\u0010$\u001a\u00020\r2\b\u0010%\u001a\u0004\u0018\u00010&H\u0016J&\u0010'\u001a\u0004\u0018\u00010(2\u0006\u0010)\u001a\u00020*2\b\u0010+\u001a\u0004\u0018\u00010,2\b\u0010%\u001a\u0004\u0018\u00010&H\u0016J\b\u0010-\u001a\u00020\rH\u0016J\u001c\u0010.\u001a\u00020\r2\b\u0010/\u001a\u0004\u0018\u00010(2\b\u0010%\u001a\u0004\u0018\u00010&H\u0016J\u0010\u00100\u001a\u00020\r2\u0006\u00101\u001a\u00020\u0011H\u0016J\u0010\u00102\u001a\u00020\r2\u0006\u00103\u001a\u00020\u0007H\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u00020\u00078VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0006\u0010\bR\u000e\u0010\t\u001a\u00020\nX\u0082.¢\u0006\u0002\n\u0000R\u001a\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\r0\f8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u000e\u0010\u000fR\u001a\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00110\f8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0012\u0010\u000fR\u001e\u0010\u0013\u001a\u00020\u00148\u0006@\u0006X\u0087.¢\u0006\u000e\n\u0000\u001a\u0004\b\u0015\u0010\u0016\"\u0004\b\u0017\u0010\u0018R\u001a\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u00110\u001a8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u001b\u0010\u001c¨\u00066"},
   d2 = {"Lco/uk/getmondo/signup/tax_residency/step/TaxResidencySelectionFragment;", "Lco/uk/getmondo/common/fragments/BaseFragment;", "Lco/uk/getmondo/signup/tax_residency/step/TaxResidencySelectionPresenter$View;", "()V", "adapter", "Lco/uk/getmondo/signup/tax_residency/step/TaxResidencySelectionAdapter;", "isUsTaxResident", "", "()Z", "listener", "Lco/uk/getmondo/signup/tax_residency/step/TaxResidencySelectionFragment$StepListener;", "onContinue", "Lio/reactivex/Observable;", "", "getOnContinue", "()Lio/reactivex/Observable;", "onCountryClicked", "Lco/uk/getmondo/signup/tax_residency/data/TaxCountry;", "getOnCountryClicked", "presenter", "Lco/uk/getmondo/signup/tax_residency/step/TaxResidencySelectionPresenter;", "getPresenter", "()Lco/uk/getmondo/signup/tax_residency/step/TaxResidencySelectionPresenter;", "setPresenter", "(Lco/uk/getmondo/signup/tax_residency/step/TaxResidencySelectionPresenter;)V", "taxCountries", "", "getTaxCountries", "()Ljava/util/List;", "completeStage", "goToNextStep", "jurisdiction", "Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;", "onAttach", "context", "Landroid/content/Context;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "onDestroyView", "onViewCreated", "view", "setSelection", "taxCountry", "showLoading", "loading", "Companion", "StepListener", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class b extends co.uk.getmondo.common.f.a implements d.a {
   public static final b.a c = new b.a((kotlin.d.b.i)null);
   public d a;
   private b.b d;
   private final a e = new a((List)null, (kotlin.d.a.b)null, 3, (kotlin.d.b.i)null);
   private HashMap f;

   public View a(int var1) {
      if(this.f == null) {
         this.f = new HashMap();
      }

      View var3 = (View)this.f.get(Integer.valueOf(var1));
      View var2 = var3;
      if(var3 == null) {
         var2 = this.getView();
         if(var2 == null) {
            var2 = null;
         } else {
            var2 = var2.findViewById(var1);
            this.f.put(Integer.valueOf(var1), var2);
         }
      }

      return var2;
   }

   public io.reactivex.n a() {
      io.reactivex.n var1 = io.reactivex.n.create((io.reactivex.p)(new io.reactivex.p() {
         public final void a(final io.reactivex.o var1) {
            kotlin.d.b.l.b(var1, "emitter");
            b.this.e.a((kotlin.d.a.b)(new kotlin.d.a.b() {
               public final void a(co.uk.getmondo.signup.tax_residency.a.a var1x) {
                  kotlin.d.b.l.b(var1x, "it");
                  var1.a(var1x);
               }
            }));
            var1.a((io.reactivex.c.f)(new io.reactivex.c.f() {
               public final void a() {
                  b.this.e.a((kotlin.d.a.b)null);
               }
            }));
         }
      }));
      kotlin.d.b.l.a(var1, "Observable.create { emit…stener = null }\n        }");
      return var1;
   }

   public void a(Jurisdiction var1) {
      kotlin.d.b.l.b(var1, "jurisdiction");
      b.b var2 = this.d;
      if(var2 == null) {
         kotlin.d.b.l.b("listener");
      }

      var2.a(var1);
   }

   public void a(co.uk.getmondo.signup.tax_residency.a.a var1) {
      kotlin.d.b.l.b(var1, "taxCountry");
      this.e.a(var1);
   }

   public void a(boolean var1) {
      ((ProgressButton)this.a(co.uk.getmondo.c.a.continueButton)).setLoading(var1);
   }

   public io.reactivex.n b() {
      io.reactivex.n var1 = com.b.a.c.c.a((ProgressButton)this.a(co.uk.getmondo.c.a.continueButton)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var1, "RxView.clicks(this).map(VoidToUnit)");
      return var1;
   }

   public List c() {
      return this.e.a();
   }

   public boolean d() {
      return this.getArguments().getBoolean("ARG_US_TAX_RESIDENT");
   }

   public void e() {
      b.b var1 = this.d;
      if(var1 == null) {
         kotlin.d.b.l.b("listener");
      }

      var1.j();
   }

   public void f() {
      if(this.f != null) {
         this.f.clear();
      }

   }

   public void onAttach(Context var1) {
      kotlin.d.b.l.b(var1, "context");
      super.onAttach(var1);
      if(var1 instanceof b.b) {
         this.d = (b.b)var1;
      } else {
         throw (Throwable)(new IllegalStateException("Activity must implement TaxResidencySelectionFragment.StepListener"));
      }
   }

   public void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.B().a(this);
   }

   public View onCreateView(LayoutInflater var1, ViewGroup var2, Bundle var3) {
      kotlin.d.b.l.b(var1, "inflater");
      return var1.inflate(2131034284, var2, false);
   }

   public void onDestroyView() {
      d var1 = this.a;
      if(var1 == null) {
         kotlin.d.b.l.b("presenter");
      }

      var1.b();
      super.onDestroyView();
      this.f();
   }

   public void onViewCreated(View var1, Bundle var2) {
      super.onViewCreated(var1, var2);
      LinearLayoutManager var3 = new LinearLayoutManager((Context)this.getActivity());
      ((RecyclerView)this.a(co.uk.getmondo.c.a.residencyCountriesRecyclerView)).setLayoutManager((android.support.v7.widget.RecyclerView.h)var3);
      ((RecyclerView)this.a(co.uk.getmondo.c.a.residencyCountriesRecyclerView)).setAdapter((android.support.v7.widget.RecyclerView.a)this.e);
      ((RecyclerView)this.a(co.uk.getmondo.c.a.residencyCountriesRecyclerView)).setHasFixedSize(true);
      ((RecyclerView)this.a(co.uk.getmondo.c.a.residencyCountriesRecyclerView)).a((android.support.v7.widget.RecyclerView.g)(new co.uk.getmondo.common.ui.e((Context)this.getActivity(), var3.h(), (Drawable)null, 0, 0, 0, 0, 124, (kotlin.d.b.i)null)));
      android.support.v7.widget.RecyclerView.e var4 = ((RecyclerView)this.a(co.uk.getmondo.c.a.residencyCountriesRecyclerView)).getItemAnimator();
      if(var4 == null) {
         throw new TypeCastException("null cannot be cast to non-null type android.support.v7.widget.DefaultItemAnimator");
      } else {
         ((ak)var4).a(false);
         a var6 = this.e;
         ArrayList var5 = this.getArguments().getParcelableArrayList("ARG_TAX_COUNTRIES");
         kotlin.d.b.l.a(var5, "arguments.getParcelableA…ayList(ARG_TAX_COUNTRIES)");
         var6.a((List)var5);
         d var7 = this.a;
         if(var7 == null) {
            kotlin.d.b.l.b("presenter");
         }

         var7.a((d.a)this);
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u001c\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\t2\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\f0\u000bR\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000¨\u0006\r"},
      d2 = {"Lco/uk/getmondo/signup/tax_residency/step/TaxResidencySelectionFragment$Companion;", "", "()V", "ARG_TAX_COUNTRIES", "", "ARG_US_TAX_RESIDENT", "newInstance", "Lco/uk/getmondo/signup/tax_residency/step/TaxResidencySelectionFragment;", "isUsTaxResident", "", "taxCountries", "Ljava/util/ArrayList;", "Lco/uk/getmondo/signup/tax_residency/data/TaxCountry;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var1) {
         this();
      }

      public final b a(boolean var1, ArrayList var2) {
         kotlin.d.b.l.b(var2, "taxCountries");
         b var3 = new b();
         Bundle var4 = new Bundle();
         var4.putBoolean("ARG_US_TAX_RESIDENT", var1);
         var4.putParcelableArrayList("ARG_TAX_COUNTRIES", var2);
         var3.setArguments(var4);
         return var3;
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&¨\u0006\u0006"},
      d2 = {"Lco/uk/getmondo/signup/tax_residency/step/TaxResidencySelectionFragment$StepListener;", "Lco/uk/getmondo/signup/tax_residency/TaxResidencyStageListener;", "onCountrySelectionComplete", "", "firstJurisdiction", "Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface b extends co.uk.getmondo.signup.tax_residency.f {
      void a(Jurisdiction var1);
   }
}
