package co.uk.getmondo.signup.tax_residency.b;

import co.uk.getmondo.api.model.tax_residency.Jurisdiction;
import co.uk.getmondo.api.model.tracking.Impression;
import io.reactivex.z;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0010B3\b\u0007\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b¢\u0006\u0002\u0010\fJ\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0002H\u0016R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0011"},
   d2 = {"Lco/uk/getmondo/signup/tax_residency/step/TaxResidencySelectionPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/signup/tax_residency/step/TaxResidencySelectionPresenter$View;", "ioScheduler", "Lio/reactivex/Scheduler;", "uiScheduler", "taxResidencyManager", "Lco/uk/getmondo/signup/tax_residency/data/TaxResidencyManager;", "apiErrorHandler", "Lco/uk/getmondo/common/errors/ApiErrorHandler;", "analyticsService", "Lco/uk/getmondo/common/AnalyticsService;", "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/signup/tax_residency/data/TaxResidencyManager;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/common/AnalyticsService;)V", "register", "", "view", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class d extends co.uk.getmondo.common.ui.b {
   private final io.reactivex.u c;
   private final io.reactivex.u d;
   private final co.uk.getmondo.signup.tax_residency.a.c e;
   private final co.uk.getmondo.common.e.a f;
   private final co.uk.getmondo.common.a g;

   public d(io.reactivex.u var1, io.reactivex.u var2, co.uk.getmondo.signup.tax_residency.a.c var3, co.uk.getmondo.common.e.a var4, co.uk.getmondo.common.a var5) {
      kotlin.d.b.l.b(var1, "ioScheduler");
      kotlin.d.b.l.b(var2, "uiScheduler");
      kotlin.d.b.l.b(var3, "taxResidencyManager");
      kotlin.d.b.l.b(var4, "apiErrorHandler");
      kotlin.d.b.l.b(var5, "analyticsService");
      super();
      this.c = var1;
      this.d = var2;
      this.e = var3;
      this.f = var4;
      this.g = var5;
   }

   public void a(final d.a var1) {
      kotlin.d.b.l.b(var1, "view");
      super.a((co.uk.getmondo.common.ui.f)var1);
      this.g.a(Impression.Companion.aF());
      io.reactivex.b.a var4 = this.b;
      io.reactivex.n var5 = var1.a().map((io.reactivex.c.h)null.a);
      io.reactivex.c.g var6 = (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(co.uk.getmondo.signup.tax_residency.a.a var1x) {
            d.a var2 = var1;
            kotlin.d.b.l.a(var1x, "it");
            var2.a(var1x);
         }
      });
      kotlin.d.a.b var3 = (kotlin.d.a.b)null.a;
      Object var2 = var3;
      if(var3 != null) {
         var2 = new e(var3);
      }

      io.reactivex.b.b var8 = var5.subscribe(var6, (io.reactivex.c.g)var2);
      kotlin.d.b.l.a(var8, "view.onCountryClicked\n  …lection(it) }, Timber::e)");
      this.b = co.uk.getmondo.common.j.f.a(var4, var8);
      io.reactivex.b.a var9 = this.b;
      io.reactivex.b.b var7 = var1.b().flatMapMaybe((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.h a(kotlin.n var1x) {
            kotlin.d.b.l.b(var1x, "it");
            co.uk.getmondo.signup.tax_residency.a.c var4 = d.this.e;
            boolean var3 = var1.d();
            Iterable var5 = (Iterable)var1.c();
            Collection var8 = (Collection)(new ArrayList());
            Iterator var9 = var5.iterator();

            while(var9.hasNext()) {
               Object var7 = var9.next();
               co.uk.getmondo.signup.tax_residency.a.a var6 = (co.uk.getmondo.signup.tax_residency.a.a)var7;
               boolean var2;
               if(!var6.e() && !var6.d()) {
                  var2 = false;
               } else {
                  var2 = true;
               }

               if(var2) {
                  var8.add(var7);
               }
            }

            return co.uk.getmondo.common.j.f.a(var4.a(var3, (List)var8).a((z)d.this.e.b()).b(d.this.c).a(d.this.d).b((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(io.reactivex.b.b var1x) {
                  var1.a(true);
               }
            })).d((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(Throwable var1x) {
                  var1.a(false);
                  co.uk.getmondo.common.e.a var2 = d.this.f;
                  kotlin.d.b.l.a(var1x, "it");
                  if(!var2.a(var1x, (co.uk.getmondo.common.e.a.a)var1)) {
                     var1.b(2131362198);
                  }

               }
            })));
         }
      })).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(com.c.b.b var1x) {
            if(var1x.d()) {
               var1.e();
            } else {
               d.a var2 = var1;
               Object var3 = var1x.a();
               kotlin.d.b.l.a(var3, "jurisdiction.get()");
               var2.a((Jurisdiction)var3);
            }

         }
      }));
      kotlin.d.b.l.a(var7, "view.onContinue\n        …     }\n                })");
      this.b = co.uk.getmondo.common.j.f.a(var9, var7);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\bf\u0018\u00002\u00020\u00012\u00020\u0002J\b\u0010\u0012\u001a\u00020\bH&J\u0010\u0010\u0013\u001a\u00020\b2\u0006\u0010\u0014\u001a\u00020\u0015H&J\u0010\u0010\u0016\u001a\u00020\b2\u0006\u0010\u0017\u001a\u00020\fH&J\u0010\u0010\u0018\u001a\u00020\b2\u0006\u0010\u0019\u001a\u00020\u0004H&R\u0012\u0010\u0003\u001a\u00020\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0003\u0010\u0005R\u0018\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007X¦\u0004¢\u0006\u0006\u001a\u0004\b\t\u0010\nR\u0018\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\f0\u0007X¦\u0004¢\u0006\u0006\u001a\u0004\b\r\u0010\nR\u0018\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\f0\u000fX¦\u0004¢\u0006\u0006\u001a\u0004\b\u0010\u0010\u0011¨\u0006\u001a"},
      d2 = {"Lco/uk/getmondo/signup/tax_residency/step/TaxResidencySelectionPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "isUsTaxResident", "", "()Z", "onContinue", "Lio/reactivex/Observable;", "", "getOnContinue", "()Lio/reactivex/Observable;", "onCountryClicked", "Lco/uk/getmondo/signup/tax_residency/data/TaxCountry;", "getOnCountryClicked", "taxCountries", "", "getTaxCountries", "()Ljava/util/List;", "completeStage", "goToNextStep", "jurisdiction", "Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;", "setSelection", "taxCountry", "showLoading", "loading", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      io.reactivex.n a();

      void a(Jurisdiction var1);

      void a(co.uk.getmondo.signup.tax_residency.a.a var1);

      void a(boolean var1);

      io.reactivex.n b();

      List c();

      boolean d();

      void e();
   }
}
