package co.uk.getmondo.signup.tax_residency;

import co.uk.getmondo.api.model.tax_residency.TaxResidencyInfo;
import co.uk.getmondo.signup.i;
import io.reactivex.u;
import io.reactivex.c.g;
import io.reactivex.c.h;
import kotlin.Metadata;
import kotlin.n;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\b\u0004\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0011B+\b\u0007\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t¢\u0006\u0002\u0010\nJ\u0018\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0002H\u0002J\u0010\u0010\u0010\u001a\u00020\f2\u0006\u0010\u000f\u001a\u00020\u0002H\u0016R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0012"},
   d2 = {"Lco/uk/getmondo/signup/tax_residency/TaxResidencyPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/signup/tax_residency/TaxResidencyPresenter$View;", "ioScheduler", "Lio/reactivex/Scheduler;", "uiScheduler", "taxResidencyManager", "Lco/uk/getmondo/signup/tax_residency/data/TaxResidencyManager;", "apiErrorHandler", "Lco/uk/getmondo/common/errors/ApiErrorHandler;", "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/signup/tax_residency/data/TaxResidencyManager;Lco/uk/getmondo/common/errors/ApiErrorHandler;)V", "handleStatusError", "", "it", "", "view", "register", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class b extends co.uk.getmondo.common.ui.b {
   private final u c;
   private final u d;
   private final co.uk.getmondo.signup.tax_residency.a.c e;
   private final co.uk.getmondo.common.e.a f;

   public b(u var1, u var2, co.uk.getmondo.signup.tax_residency.a.c var3, co.uk.getmondo.common.e.a var4) {
      l.b(var1, "ioScheduler");
      l.b(var2, "uiScheduler");
      l.b(var3, "taxResidencyManager");
      l.b(var4, "apiErrorHandler");
      super();
      this.c = var1;
      this.d = var2;
      this.e = var3;
      this.f = var4;
   }

   private final void a(Throwable var1, final b.a var2) {
      if(!i.a.a(var1, (i.a)var2) && !this.f.a(var1, (co.uk.getmondo.common.e.a.a)var2, (kotlin.d.a.b)(new kotlin.d.a.b() {
         // $FF: synthetic method
         public Object a(Object var1) {
            this.a(((Number)var1).intValue());
            return n.a;
         }

         public final void a(int var1) {
            var2.d(var1);
         }
      }))) {
         var2.b(2131362198);
      }

   }

   public void a(final b.a var1) {
      l.b(var1, "view");
      super.a((co.uk.getmondo.common.ui.f)var1);
      io.reactivex.b.a var3 = this.b;
      io.reactivex.n var4 = var1.c().startWith((Object)n.a).flatMapMaybe((h)(new h() {
         public final io.reactivex.h a(n var1x) {
            l.b(var1x, "it");
            return co.uk.getmondo.common.j.f.a(b.this.e.a().b(b.this.c).a(b.this.d).b((g)(new g() {
               public final void a(io.reactivex.b.b var1x) {
                  var1.d();
               }
            })).a((io.reactivex.c.b)(new io.reactivex.c.b() {
               public final void a(TaxResidencyInfo var1x, Throwable var2) {
                  var1.e();
               }
            })).d((g)(new g() {
               public final void a(Throwable var1x) {
                  b var2 = b.this;
                  l.a(var1x, "it");
                  var2.a(var1x, var1);
               }
            })));
         }
      }));
      g var5 = (g)(new g() {
         public final void a(TaxResidencyInfo var1x) {
            TaxResidencyInfo.Status var2 = var1x.a();
            switch(c.a[var2.ordinal()]) {
            case 1:
            case 2:
               if(var1x.b()) {
                  var1.f();
               } else {
                  var1.g();
               }
               break;
            case 3:
               var1.h();
            }

         }
      });
      kotlin.d.a.b var2 = (kotlin.d.a.b)null.a;
      Object var6 = var2;
      if(var2 != null) {
         var6 = new d(var2);
      }

      io.reactivex.b.b var7 = var4.subscribe(var5, (g)var6);
      l.a(var7, "view.onRetryClicked\n    …            }, Timber::e)");
      this.b = co.uk.getmondo.common.j.f.a(var3, var7);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0010\b\n\u0002\b\u0004\bf\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003J\b\u0010\t\u001a\u00020\u0006H&J\b\u0010\n\u001a\u00020\u0006H&J\u0012\u0010\u000b\u001a\u00020\u00062\b\b\u0001\u0010\f\u001a\u00020\rH&J\b\u0010\u000e\u001a\u00020\u0006H&J\b\u0010\u000f\u001a\u00020\u0006H&J\b\u0010\u0010\u001a\u00020\u0006H&R\u0018\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0007\u0010\b¨\u0006\u0011"},
      d2 = {"Lco/uk/getmondo/signup/tax_residency/TaxResidencyPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "Lco/uk/getmondo/signup/SignupApiErrorHandler$View;", "onRetryClicked", "Lio/reactivex/Observable;", "", "getOnRetryClicked", "()Lio/reactivex/Observable;", "finishStage", "hideLoading", "showFullScreenError", "errorMessage", "", "showLoading", "showSummaryScreen", "showUsTaxResidentScreen", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f, i.a {
      io.reactivex.n c();

      void d();

      void d(int var1);

      void e();

      void f();

      void g();

      void h();
   }
}
