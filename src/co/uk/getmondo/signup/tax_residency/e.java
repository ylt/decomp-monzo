package co.uk.getmondo.signup.tax_residency;

import io.reactivex.u;

public final class e implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final b.a b;
   private final javax.a.a c;
   private final javax.a.a d;
   private final javax.a.a e;
   private final javax.a.a f;

   static {
      boolean var0;
      if(!e.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   public e(b.a var1, javax.a.a var2, javax.a.a var3, javax.a.a var4, javax.a.a var5) {
      if(!a && var1 == null) {
         throw new AssertionError();
      } else {
         this.b = var1;
         if(!a && var2 == null) {
            throw new AssertionError();
         } else {
            this.c = var2;
            if(!a && var3 == null) {
               throw new AssertionError();
            } else {
               this.d = var3;
               if(!a && var4 == null) {
                  throw new AssertionError();
               } else {
                  this.e = var4;
                  if(!a && var5 == null) {
                     throw new AssertionError();
                  } else {
                     this.f = var5;
                  }
               }
            }
         }
      }
   }

   public static b.a.b a(b.a var0, javax.a.a var1, javax.a.a var2, javax.a.a var3, javax.a.a var4) {
      return new e(var0, var1, var2, var3, var4);
   }

   public b a() {
      return (b)b.a.c.a(this.b, new b((u)this.c.b(), (u)this.d.b(), (co.uk.getmondo.signup.tax_residency.a.c)this.e.b(), (co.uk.getmondo.common.e.a)this.f.b()));
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
