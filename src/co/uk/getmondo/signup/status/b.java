package co.uk.getmondo.signup.status;

import co.uk.getmondo.api.MigrationApi;
import co.uk.getmondo.api.SignupApi;
import co.uk.getmondo.api.model.signup.SignupInfo;
import io.reactivex.n;
import io.reactivex.v;
import io.reactivex.z;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001B/\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b¢\u0006\u0002\u0010\fJ\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00120\u0011J\u0016\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00150\u00142\u0006\u0010\u0016\u001a\u00020\u0015H\u0002J\f\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00150\u0014J\f\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00150\u0014J\u0016\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u00150\u00142\b\b\u0002\u0010\u001a\u001a\u00020\u000eR\u0011\u0010\r\u001a\u00020\u000e8F¢\u0006\u0006\u001a\u0004\b\r\u0010\u000fR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u001b"},
   d2 = {"Lco/uk/getmondo/signup/status/SignupStatusManager;", "", "signupApi", "Lco/uk/getmondo/api/SignupApi;", "migrationApi", "Lco/uk/getmondo/api/MigrationApi;", "userInteractor", "Lco/uk/getmondo/api/interactors/UserInteractor;", "statusStorage", "Lco/uk/getmondo/signup/status/SignupStatusStorage;", "migrationStorage", "Lco/uk/getmondo/migration/MigrationStorage;", "(Lco/uk/getmondo/api/SignupApi;Lco/uk/getmondo/api/MigrationApi;Lco/uk/getmondo/api/interactors/UserInteractor;Lco/uk/getmondo/signup/status/SignupStatusStorage;Lco/uk/getmondo/migration/MigrationStorage;)V", "isSignupCompleted", "", "()Z", "migrationInfo", "Lio/reactivex/Observable;", "Lco/uk/getmondo/api/model/sign_up/MigrationInfo;", "overrideStageIfInWaitingList", "Lio/reactivex/Single;", "Lco/uk/getmondo/api/model/signup/SignupInfo;", "signupInfo", "skipFingerprintEnrolment", "startSignUp", "status", "waitingListEnabled", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class b {
   private final SignupApi a;
   private final MigrationApi b;
   private final co.uk.getmondo.api.b.a c;
   private final g d;
   private final co.uk.getmondo.migration.d e;

   public b(SignupApi var1, MigrationApi var2, co.uk.getmondo.api.b.a var3, g var4, co.uk.getmondo.migration.d var5) {
      l.b(var1, "signupApi");
      l.b(var2, "migrationApi");
      l.b(var3, "userInteractor");
      l.b(var4, "statusStorage");
      l.b(var5, "migrationStorage");
      super();
      this.a = var1;
      this.b = var2;
      this.c = var3;
      this.d = var4;
      this.e = var5;
   }

   private final v a(final SignupInfo var1) {
      v var2;
      if(l.a(var1.a(), SignupInfo.Status.NOT_STARTED)) {
         var2 = this.c.b().d((io.reactivex.c.h)null.a).d((io.reactivex.c.h)(new io.reactivex.c.h() {
            public final SignupInfo a(Boolean var1x) {
               l.b(var1x, "finishedWaitingList");
               SignupInfo var2;
               if(var1x.booleanValue()) {
                  var2 = var1;
               } else {
                  var2 = SignupInfo.a(var1, (SignupInfo.Status)null, SignupInfo.Stage.WAIT_LIST, 1, (Object)null);
               }

               return var2;
            }
         })).f((io.reactivex.c.h)(new io.reactivex.c.h() {
            public final z a(Throwable var1x) {
               l.b(var1x, "it");
               Integer var2 = co.uk.getmondo.common.e.c.b(var1x);
               z var3;
               if(var2 != null && var2.intValue() == 404) {
                  var3 = (z)v.a((Object)SignupInfo.a(var1, (SignupInfo.Status)null, SignupInfo.Stage.WAIT_LIST_SIGNUP, 1, (Object)null));
               } else {
                  var3 = (z)v.a(var1x);
               }

               return var3;
            }
         }));
         l.a(var2, "userInteractor.fetchWait…  }\n                    }");
      } else {
         var2 = v.a((Object)var1);
         l.a(var2, "Single.just(signupInfo)");
      }

      return var2;
   }

   public final v a(final boolean var1) {
      v var2 = this.a.signupStatus().a((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final v a(SignupInfo var1x) {
            l.b(var1x, "it");
            v var2;
            if(var1) {
               var2 = b.this.a(var1x);
            } else {
               var2 = v.a((Object)var1x);
            }

            return var2;
         }
      })).a((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(SignupInfo var1) {
            b.this.d.a(l.a(var1.a(), SignupInfo.Status.COMPLETED));
         }
      }));
      l.a(var2, "signupApi.signupStatus()…upInfo.Status.COMPLETED }");
      return var2;
   }

   public final boolean a() {
      return this.d.a();
   }

   public final v b() {
      v var1 = this.a.startSignUp().a((z)a(this, false, 1, (Object)null));
      l.a(var1, "signupApi.startSignUp().andThen(status())");
      return var1;
   }

   public final v c() {
      v var1 = this.a.skipFingerprintEnrolment().a((z)a(this, false, 1, (Object)null));
      l.a(var1, "signupApi.skipFingerprin…lment().andThen(status())");
      return var1;
   }

   public final n d() {
      io.reactivex.rxkotlin.b var2 = io.reactivex.rxkotlin.b.a;
      n var1 = this.b.migrationInfo().f();
      l.a(var1, "migrationApi.migrationInfo().toObservable()");
      var1 = var2.a(var1, this.e.b(), this.d.b()).map((io.reactivex.c.h)null.a);
      l.a(var1, "Observables.combineLates…o\n            }\n        }");
      return var1;
   }
}
