package co.uk.getmondo.bump_up;

import co.uk.getmondo.d.ak;
import io.reactivex.l;
import io.reactivex.n;
import io.reactivex.v;
import io.reactivex.z;

class d extends co.uk.getmondo.common.ui.b {
   private final co.uk.getmondo.common.accounts.d c;
   private final com.google.gson.f d;

   d(co.uk.getmondo.common.accounts.d var1, com.google.gson.f var2) {
      this.c = var1;
      this.d = var2;
   }

   // $FF: synthetic method
   static b a(b var0, ak var1) throws Exception {
      if(var0.e() && var1.e() != null) {
         var0.a(var1.e().g());
      }

      return var0;
   }

   // $FF: synthetic method
   static b a(d var0, String var1) throws Exception {
      return (b)var0.d.a(var1, b.class);
   }

   private v a(String var1) {
      return v.c(i.a(this, var1));
   }

   // $FF: synthetic method
   static z a(d var0, b var1) throws Exception {
      return var0.c.c().d(j.a(var1));
   }

   // $FF: synthetic method
   static void a(d.a var0, b var1) throws Exception {
      if(var1.d()) {
         var0.a(var1);
      } else if(var1.c()) {
         var0.b(var1);
      }

   }

   // $FF: synthetic method
   static l b(d var0, String var1) throws Exception {
      return var0.a(var1).e().a((l)io.reactivex.h.a());
   }

   public void a(d.a var1) {
      super.a((co.uk.getmondo.common.ui.f)var1);
      this.a((io.reactivex.b.b)var1.b().flatMapMaybe(e.a(this)).flatMapSingle(f.a(this)).subscribe(g.a(var1), h.a()));
   }

   interface a extends co.uk.getmondo.common.ui.f {
      void a(b var1);

      n b();

      void b(b var1);
   }
}
