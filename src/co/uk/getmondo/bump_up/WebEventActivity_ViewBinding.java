package co.uk.getmondo.bump_up;

import android.view.View;
import android.webkit.WebView;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class WebEventActivity_ViewBinding implements Unbinder {
   private WebEventActivity a;

   public WebEventActivity_ViewBinding(WebEventActivity var1, View var2) {
      this.a = var1;
      var1.eventWebView = (WebView)Utils.findRequiredViewAsType(var2, 2131821179, "field 'eventWebView'", WebView.class);
   }

   public void unbind() {
      WebEventActivity var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.eventWebView = null;
      }
   }
}
