package co.uk.getmondo.bump_up;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import co.uk.getmondo.common.ui.AnimatedNumberView;

public class BumpUpActivity_ViewBinding implements Unbinder {
   private BumpUpActivity a;
   private View b;

   public BumpUpActivity_ViewBinding(final BumpUpActivity var1, View var2) {
      this.a = var1;
      var1.eventBumpText = (AnimatedNumberView)Utils.findRequiredViewAsType(var2, 2131820829, "field 'eventBumpText'", AnimatedNumberView.class);
      var1.eventMessageText = (TextView)Utils.findRequiredViewAsType(var2, 2131820828, "field 'eventMessageText'", TextView.class);
      var2 = Utils.findRequiredView(var2, 2131820830, "method 'onShareMoreInvites'");
      this.b = var2;
      var2.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View var1x) {
            var1.onShareMoreInvites();
         }
      });
   }

   public void unbind() {
      BumpUpActivity var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.eventBumpText = null;
         var1.eventMessageText = null;
         this.b.setOnClickListener((OnClickListener)null);
         this.b = null;
      }
   }
}
