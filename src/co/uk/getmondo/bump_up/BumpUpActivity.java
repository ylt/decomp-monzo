package co.uk.getmondo.bump_up;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.uk.getmondo.common.ui.AnimatedNumberView;

public class BumpUpActivity extends co.uk.getmondo.common.activities.h {
   co.uk.getmondo.common.accounts.d a;
   private co.uk.getmondo.d.k b;
   @BindView(2131820829)
   AnimatedNumberView eventBumpText;
   @BindView(2131820828)
   TextView eventMessageText;

   public static int a(Intent var0) {
      int var1 = 0;
      if(var0 != null) {
         int var2;
         try {
            var2 = var0.getIntExtra("BUMP_EXTRA", 0);
         } catch (Exception var3) {
            return var1;
         }

         var1 = var2;
      }

      return var1;
   }

   public static void a(Activity var0, co.uk.getmondo.d.k var1) {
      Intent var2 = new Intent(var0, BumpUpActivity.class);
      var2.putExtra(co.uk.getmondo.d.k.class.getSimpleName(), var1);
      var0.startActivityForResult(var2, 11323);
   }

   public static boolean a(int var0) {
      boolean var1;
      if(11323 == var0) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public void finish() {
      if(this.b != null) {
         Intent var1 = new Intent();
         var1.putExtra("BUMP_EXTRA", this.b.d());
         this.setResult(-1, var1);
      }

      super.finish();
   }

   protected void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.setContentView(2131034148);
      ButterKnife.bind((Activity)this);
      this.l().a(this);
      if(!this.getIntent().hasExtra(co.uk.getmondo.d.k.class.getSimpleName())) {
         this.finish();
      } else {
         this.b = (co.uk.getmondo.d.k)this.getIntent().getSerializableExtra(co.uk.getmondo.d.k.class.getSimpleName());
         this.eventBumpText.setNumber(this.b.d());
         this.eventMessageText.setText(this.b.c());
      }

   }

   @OnClick({2131820830})
   public void onShareMoreInvites() {
      String var1 = this.getString(2131362012);
      this.startActivity(Intent.createChooser(co.uk.getmondo.common.k.j.a(this.a.b().e().g()), var1));
   }
}
