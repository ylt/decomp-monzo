package co.uk.getmondo.pin;

import android.support.design.widget.TextInputLayout;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class DateOfBirthDialogFragment_ViewBinding implements Unbinder {
   private DateOfBirthDialogFragment a;
   private View b;

   public DateOfBirthDialogFragment_ViewBinding(final DateOfBirthDialogFragment var1, View var2) {
      this.a = var1;
      var1.dobWrapper = (TextInputLayout)Utils.findRequiredViewAsType(var2, 2131821163, "field 'dobWrapper'", TextInputLayout.class);
      var2 = Utils.findRequiredView(var2, 2131821254, "field 'dobEditText' and method 'onNext'");
      var1.dobEditText = (EditText)Utils.castView(var2, 2131821254, "field 'dobEditText'", EditText.class);
      this.b = var2;
      ((TextView)var2).setOnEditorActionListener(new OnEditorActionListener() {
         public boolean onEditorAction(TextView var1x, int var2, KeyEvent var3) {
            return var1.onNext(var2);
         }
      });
   }

   public void unbind() {
      DateOfBirthDialogFragment var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.dobWrapper = null;
         var1.dobEditText = null;
         ((TextView)this.b).setOnEditorActionListener((OnEditorActionListener)null);
         this.b = null;
      }
   }
}
