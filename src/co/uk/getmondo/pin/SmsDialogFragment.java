package co.uk.getmondo.pin;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.AlertDialog.Builder;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class SmsDialogFragment extends android.support.v4.app.i {
   public static final String a = SmsDialogFragment.class.getSimpleName();
   private String b;
   private Unbinder c;
   private SmsDialogFragment.a d;
   @BindView(2131821255)
   Button textMeButton;

   public static SmsDialogFragment a(String var0) {
      SmsDialogFragment var2 = new SmsDialogFragment();
      Bundle var1 = new Bundle();
      var1.putString("phone_number", var0);
      var2.setArguments(var1);
      var2.setCancelable(false);
      return var2;
   }

   public void a(SmsDialogFragment.a var1) {
      this.d = var1;
   }

   @OnClick({2131821256})
   public void onChangedNumber() {
      if(this.d != null) {
         this.d.E();
      }

   }

   public void onCreate(Bundle var1) {
      super.onCreate(var1);
      if(this.getArguments().containsKey("phone_number")) {
         this.b = this.getArguments().getString("phone_number", "");
      }

   }

   public Dialog onCreateDialog(Bundle var1) {
      View var3 = LayoutInflater.from(this.getActivity()).inflate(2131034262, (ViewGroup)null);
      this.c = ButterKnife.bind(this, (View)var3);
      this.textMeButton.setText(this.getString(2131362765, new Object[]{this.b}));
      AlertDialog var2 = (new Builder(this.getActivity(), 2131493138)).setTitle(2131362569).setView(var3).create();
      Window var4 = var2.getWindow();
      if(var4 != null) {
         var4.clearFlags(2);
      }

      return var2;
   }

   public void onDestroyView() {
      this.c.unbind();
      super.onDestroyView();
   }

   @OnClick({2131821257})
   public void onNotNow() {
      if(this.d != null) {
         this.d.a();
      }

   }

   @OnClick({2131821255})
   public void onTextMe() {
      if(this.d != null) {
         this.d.D();
      }

   }

   interface a {
      void D();

      void E();

      void a();
   }
}
