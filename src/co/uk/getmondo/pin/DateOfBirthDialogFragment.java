package co.uk.getmondo.pin;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnEditorAction;
import butterknife.Unbinder;

public class DateOfBirthDialogFragment extends android.support.v4.app.i {
   public static final String a = DateOfBirthDialogFragment.class.getSimpleName();
   private Unbinder b;
   private DateOfBirthDialogFragment.a c;
   @BindView(2131821254)
   EditText dobEditText;
   @BindView(2131821163)
   TextInputLayout dobWrapper;

   public static DateOfBirthDialogFragment a() {
      DateOfBirthDialogFragment var0 = new DateOfBirthDialogFragment();
      var0.setCancelable(false);
      return var0;
   }

   // $FF: synthetic method
   static void a(DateOfBirthDialogFragment var0, AlertDialog var1, DialogInterface var2) {
      var1.getButton(-1).setOnClickListener(c.a(var0));
   }

   // $FF: synthetic method
   static void a(DateOfBirthDialogFragment var0, DialogInterface var1, int var2) {
      if(var0.c != null) {
         var0.c.a();
      }

   }

   // $FF: synthetic method
   static void a(DateOfBirthDialogFragment var0, View var1) {
      if(var0.c != null) {
         var0.c.a(var0.dobEditText.getText().toString());
      }

   }

   public void a(DateOfBirthDialogFragment.a var1) {
      this.c = var1;
   }

   public void a(String var1) {
      this.dobWrapper.setErrorEnabled(true);
      this.dobWrapper.setError(var1);
   }

   public Dialog onCreateDialog(Bundle var1) {
      View var3 = LayoutInflater.from(this.getActivity()).inflate(2131034261, (ViewGroup)null);
      this.b = ButterKnife.bind(this, (View)var3);
      final co.uk.getmondo.signup_old.r var2 = new co.uk.getmondo.signup_old.r();
      this.dobEditText.addTextChangedListener(new TextWatcher() {
         public void afterTextChanged(Editable var1) {
         }

         public void beforeTextChanged(CharSequence var1, int var2x, int var3, int var4) {
         }

         public void onTextChanged(CharSequence var1, int var2x, int var3, int var4) {
            DateOfBirthDialogFragment.this.dobWrapper.setErrorEnabled(false);
            DateOfBirthDialogFragment.this.dobWrapper.setError((CharSequence)null);
            var2.a(var1, var3, var4, this, DateOfBirthDialogFragment.this.dobEditText);
         }
      });
      AlertDialog var4 = (new Builder(this.getActivity(), 2131493138)).setTitle(2131362563).setView(var3).setPositiveButton(this.getString(2131362493), (OnClickListener)null).setNeutralButton(this.getString(2131362499), a.a(this)).setCancelable(false).create();
      var4.setOnShowListener(b.a(this, var4));
      Window var5 = var4.getWindow();
      if(var5 != null) {
         var5.clearFlags(2);
         var5.setSoftInputMode(4);
      }

      return var4;
   }

   public void onDestroyView() {
      this.b.unbind();
      super.onDestroyView();
   }

   @OnEditorAction({2131821254})
   protected boolean onNext(int var1) {
      boolean var3 = false;
      boolean var2 = var3;
      if(var1 == 6) {
         if(this.c == null) {
            var2 = var3;
         } else {
            this.c.a(this.dobEditText.getText().toString());
            var2 = true;
         }
      }

      return var2;
   }

   interface a {
      void a();

      void a(String var1);
   }
}
