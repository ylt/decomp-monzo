package co.uk.getmondo.pin;

import android.view.View;
import android.view.View.OnClickListener;

// $FF: synthetic class
final class c implements OnClickListener {
   private final DateOfBirthDialogFragment a;

   private c(DateOfBirthDialogFragment var1) {
      this.a = var1;
   }

   public static OnClickListener a(DateOfBirthDialogFragment var0) {
      return new c(var0);
   }

   public void onClick(View var1) {
      DateOfBirthDialogFragment.a(this.a, var1);
   }
}
