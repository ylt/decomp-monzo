package co.uk.getmondo.pin;

import co.uk.getmondo.api.ApiException;
import co.uk.getmondo.api.model.tracking.Impression;
import java.util.concurrent.TimeUnit;
import org.threeten.bp.LocalDate;
import org.threeten.bp.format.DateTimeFormatter;

class e extends co.uk.getmondo.common.ui.b {
   private final DateTimeFormatter c = DateTimeFormatter.a("dd/MM/yyyy");
   private LocalDate d;
   private final io.reactivex.u e;
   private final io.reactivex.u f;
   private final io.reactivex.u g;
   private final co.uk.getmondo.pin.a.a h;
   private final co.uk.getmondo.common.e.a i;
   private final co.uk.getmondo.signup_old.s j;
   private final co.uk.getmondo.common.a k;

   e(io.reactivex.u var1, io.reactivex.u var2, io.reactivex.u var3, co.uk.getmondo.pin.a.a var4, co.uk.getmondo.common.e.a var5, co.uk.getmondo.signup_old.s var6, co.uk.getmondo.common.a var7) {
      this.e = var1;
      this.f = var2;
      this.g = var3;
      this.h = var4;
      this.i = var5;
      this.j = var6;
      this.k = var7;
   }

   // $FF: synthetic method
   static String a(String var0) throws Exception {
      return var0.replace(" ", "");
   }

   // $FF: synthetic method
   static void a(e.a var0, co.uk.getmondo.common.b.a var1) throws Exception {
      var0.B();
   }

   // $FF: synthetic method
   static void a(e.a var0, io.reactivex.b.b var1) throws Exception {
      var0.i();
   }

   // $FF: synthetic method
   static void a(e.a var0, Boolean var1, Throwable var2) throws Exception {
      var0.k();
   }

   // $FF: synthetic method
   static void a(e.a var0, Object var1) throws Exception {
      var0.B();
   }

   // $FF: synthetic method
   static void a(e var0, e.a var1, Boolean var2) throws Exception {
      if(var2.booleanValue()) {
         var0.k.a(Impression.M());
         var1.z();
      } else {
         var1.v();
      }

   }

   // $FF: synthetic method
   static void a(e var0, e.a var1, Object var2) throws Exception {
      var1.x();
      var0.k.a(Impression.N());
      var1.y();
   }

   // $FF: synthetic method
   static void a(e var0, e.a var1, String var2, String var3) throws Exception {
      if(var0.j.a(var3)) {
         var0.d = LocalDate.a((CharSequence)var3, (DateTimeFormatter)var0.c);
         var1.w();
         var0.k.a(Impression.P());
         var1.d(var2);
      } else {
         var1.g();
      }

   }

   // $FF: synthetic method
   static void a(e var0, e.a var1, Throwable var2) throws Exception {
      var1.k();
      if(var0.a(var2)) {
         var0.k.a(Impression.M());
         var1.z();
      } else {
         var0.i.a(var2, var1);
      }

   }

   private boolean a(Throwable var1) {
      boolean var2;
      if(this.b(var1) != null) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   private co.uk.getmondo.api.model.pin.a b(Throwable var1) {
      Object var2 = null;
      co.uk.getmondo.api.model.pin.a var4;
      if(!(var1 instanceof ApiException)) {
         var4 = (co.uk.getmondo.api.model.pin.a)var2;
      } else {
         co.uk.getmondo.api.model.b var3 = ((ApiException)var1).e();
         var4 = (co.uk.getmondo.api.model.pin.a)var2;
         if(var3 != null) {
            var4 = (co.uk.getmondo.api.model.pin.a)co.uk.getmondo.common.e.d.a(co.uk.getmondo.api.model.pin.a.values(), var3.a());
         }
      }

      return var4;
   }

   // $FF: synthetic method
   static io.reactivex.r b(e var0, e.a var1, Object var2) throws Exception {
      return var0.h.a(var0.d).b(var0.f).a(var0.e).c(m.a(var1)).a(n.a(var0, var1)).a((io.reactivex.r)io.reactivex.n.just(co.uk.getmondo.common.b.a.a));
   }

   // $FF: synthetic method
   static void b(e.a var0, co.uk.getmondo.common.b.a var1) throws Exception {
      var0.k();
   }

   // $FF: synthetic method
   static void b(e.a var0, io.reactivex.b.b var1) throws Exception {
      var0.h();
   }

   // $FF: synthetic method
   static void b(e var0, e.a var1, Throwable var2) throws Exception {
      var0.i.a(var2, var1);
   }

   // $FF: synthetic method
   static void c(e.a var0, co.uk.getmondo.common.b.a var1) throws Exception {
      var0.j();
   }

   // $FF: synthetic method
   static void c(e var0, e.a var1, Object var2) throws Exception {
      var0.k.a(Impression.O());
      var1.x();
   }

   // $FF: synthetic method
   static void d(e var0, e.a var1, Object var2) throws Exception {
      var0.k.a(Impression.a(Impression.IntercomFrom.PIN));
      var1.A();
      var1.B();
   }

   public void a(e.a var1) {
      super.a((co.uk.getmondo.common.ui.f)var1);
      this.a((io.reactivex.b.b)var1.b().subscribe(f.a(var1)));
      this.a((io.reactivex.b.b)var1.f().subscribe(o.a(this, var1)));
      String var2 = this.h.a();
      if(co.uk.getmondo.common.k.p.d(var2)) {
         this.k.a(Impression.M());
         var1.z();
      } else {
         this.a((io.reactivex.b.b)this.h.b().b(this.f).a(this.e).b(p.a(var1)).a(q.a(var1)).a(r.a(this, var1), s.a(this, var1)));
         this.a((io.reactivex.b.b)var1.c().map(t.a()).subscribe(u.a(this, var1, var2)));
         this.a((io.reactivex.b.b)var1.d().doOnNext(v.a(this, var1)).switchMap(g.a(this, var1)).doOnNext(h.a(var1)).delay(5L, TimeUnit.SECONDS, this.g).doOnNext(i.a(var1)).subscribe(j.a(var1), k.a()));
         this.a((io.reactivex.b.b)var1.e().subscribe(l.a(this, var1)));
      }

   }

   interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      void A();

      void B();

      io.reactivex.n b();

      io.reactivex.n c();

      io.reactivex.n d();

      void d(String var1);

      io.reactivex.n e();

      io.reactivex.n f();

      void g();

      void h();

      void i();

      void j();

      void k();

      void v();

      void w();

      void x();

      void y();

      void z();
   }
}
