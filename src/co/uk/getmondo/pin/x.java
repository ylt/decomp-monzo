package co.uk.getmondo.pin;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Window;

public class x extends android.support.v4.app.i {
   public static final String a = x.class.getSimpleName();
   private String b;
   private String c;
   private x.a d;

   public static x a(String var0, String var1) {
      x var3 = new x();
      Bundle var2 = new Bundle();
      var2.putString("title", var0);
      var2.putString("message", var1);
      var3.setArguments(var2);
      var3.setCancelable(false);
      return var3;
   }

   // $FF: synthetic method
   static void a(x var0, DialogInterface var1, int var2) {
      if(var0.d != null) {
         var0.d.C();
      }

   }

   // $FF: synthetic method
   static void b(x var0, DialogInterface var1, int var2) {
      if(var0.d != null) {
         var0.d.a();
      }

   }

   public void a(x.a var1) {
      this.d = var1;
   }

   public void onCreate(Bundle var1) {
      super.onCreate(var1);
      if(this.getArguments().containsKey("title")) {
         this.b = this.getArguments().getString("title", "");
      }

      if(this.getArguments().containsKey("message")) {
         this.c = this.getArguments().getString("message", "");
      }

   }

   public Dialog onCreateDialog(Bundle var1) {
      AlertDialog var3 = (new Builder(this.getActivity(), 2131493138)).setTitle(this.b).setMessage(this.c).setNeutralButton(2131362499, y.a(this)).setPositiveButton(2131362091, z.a(this)).create();
      Window var2 = var3.getWindow();
      if(var2 != null) {
         var2.clearFlags(2);
      }

      return var3;
   }

   interface a {
      void C();

      void a();
   }
}
