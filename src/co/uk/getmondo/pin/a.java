package co.uk.getmondo.pin;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

// $FF: synthetic class
final class a implements OnClickListener {
   private final DateOfBirthDialogFragment a;

   private a(DateOfBirthDialogFragment var1) {
      this.a = var1;
   }

   public static OnClickListener a(DateOfBirthDialogFragment var0) {
      return new a(var0);
   }

   public void onClick(DialogInterface var1, int var2) {
      DateOfBirthDialogFragment.a(this.a, var1, var2);
   }
}
