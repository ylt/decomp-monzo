package co.uk.getmondo.pin;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnShowListener;

// $FF: synthetic class
final class b implements OnShowListener {
   private final DateOfBirthDialogFragment a;
   private final AlertDialog b;

   private b(DateOfBirthDialogFragment var1, AlertDialog var2) {
      this.a = var1;
      this.b = var2;
   }

   public static OnShowListener a(DateOfBirthDialogFragment var0, AlertDialog var1) {
      return new b(var0, var1);
   }

   public void onShow(DialogInterface var1) {
      DateOfBirthDialogFragment.a(this.a, this.b, var1);
   }
}
