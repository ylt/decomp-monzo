package co.uk.getmondo.pin;

// $FF: synthetic class
final class u implements io.reactivex.c.g {
   private final e a;
   private final e.a b;
   private final String c;

   private u(e var1, e.a var2, String var3) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
   }

   public static io.reactivex.c.g a(e var0, e.a var1, String var2) {
      return new u(var0, var1, var2);
   }

   public void a(Object var1) {
      e.a(this.a, this.b, this.c, (String)var1);
   }
}
