package co.uk.getmondo.pin;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

// $FF: synthetic class
final class z implements OnClickListener {
   private final x a;

   private z(x var1) {
      this.a = var1;
   }

   public static OnClickListener a(x var0) {
      return new z(var0);
   }

   public void onClick(DialogInterface var1, int var2) {
      x.a(this.a, var1, var2);
   }
}
