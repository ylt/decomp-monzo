package co.uk.getmondo.pin;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;

public class SmsDialogFragment_ViewBinding implements Unbinder {
   private SmsDialogFragment a;
   private View b;
   private View c;
   private View d;

   public SmsDialogFragment_ViewBinding(final SmsDialogFragment var1, View var2) {
      this.a = var1;
      View var3 = Utils.findRequiredView(var2, 2131821255, "field 'textMeButton' and method 'onTextMe'");
      var1.textMeButton = (Button)Utils.castView(var3, 2131821255, "field 'textMeButton'", Button.class);
      this.b = var3;
      var3.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View var1x) {
            var1.onTextMe();
         }
      });
      var3 = Utils.findRequiredView(var2, 2131821256, "method 'onChangedNumber'");
      this.c = var3;
      var3.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View var1x) {
            var1.onChangedNumber();
         }
      });
      var2 = Utils.findRequiredView(var2, 2131821257, "method 'onNotNow'");
      this.d = var2;
      var2.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View var1x) {
            var1.onNotNow();
         }
      });
   }

   public void unbind() {
      SmsDialogFragment var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.textMeButton = null;
         this.b.setOnClickListener((OnClickListener)null);
         this.b = null;
         this.c.setOnClickListener((OnClickListener)null);
         this.c = null;
         this.d.setOnClickListener((OnClickListener)null);
         this.d = null;
      }
   }
}
