package co.uk.getmondo.pin.a;

import co.uk.getmondo.api.MonzoApi;
import co.uk.getmondo.d.ac;
import io.reactivex.v;
import org.threeten.bp.LocalDate;
import org.threeten.bp.format.DateTimeFormatter;

public class a {
   private final co.uk.getmondo.common.accounts.d a;
   private final MonzoApi b;

   a(co.uk.getmondo.common.accounts.d var1, MonzoApi var2) {
      this.a = var1;
      this.b = var2;
   }

   // $FF: synthetic method
   static io.reactivex.d a(a var0, String var1, String var2) throws Exception {
      return var0.b.pinViaSms(var2, var1);
   }

   // $FF: synthetic method
   static String a(ac var0) throws Exception {
      return var0.f();
   }

   // $FF: synthetic method
   static String a(Throwable var0) throws Exception {
      return "";
   }

   public io.reactivex.b a(LocalDate var1) {
      String var2 = var1.a(DateTimeFormatter.c);
      return this.a.d().c(e.a(this, var2));
   }

   public String a() {
      return (String)this.a.c().d(b.a()).d(c.a()).e(d.a()).b();
   }

   public v b() {
      v var1 = this.a.d();
      MonzoApi var2 = this.b;
      var2.getClass();
      return var1.a(f.a(var2)).d(g.a());
   }
}
