package co.uk.getmondo.pin;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

// $FF: synthetic class
final class y implements OnClickListener {
   private final x a;

   private y(x var1) {
      this.a = var1;
   }

   public static OnClickListener a(x var0) {
      return new y(var0);
   }

   public void onClick(DialogInterface var1, int var2) {
      x.b(this.a, var1, var2);
   }
}
