package co.uk.getmondo.pin;

// $FF: synthetic class
final class g implements io.reactivex.c.h {
   private final e a;
   private final e.a b;

   private g(e var1, e.a var2) {
      this.a = var1;
      this.b = var2;
   }

   public static io.reactivex.c.h a(e var0, e.a var1) {
      return new g(var0, var1);
   }

   public Object a(Object var1) {
      return e.b(this.a, this.b, var1);
   }
}
