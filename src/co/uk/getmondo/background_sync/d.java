package co.uk.getmondo.background_sync;

import co.uk.getmondo.d.w;
import io.reactivex.u;
import java.util.ArrayList;
import java.util.List;

public class d {
   private final co.uk.getmondo.feed.a.d a;
   private final co.uk.getmondo.a.a b;
   private final co.uk.getmondo.common.accounts.b c;
   private final co.uk.getmondo.overdraft.a.c d;
   private final co.uk.getmondo.overdraft.a.a e;
   private final u f;

   public d(u var1, co.uk.getmondo.feed.a.d var2, co.uk.getmondo.a.a var3, co.uk.getmondo.common.accounts.b var4, co.uk.getmondo.overdraft.a.c var5, co.uk.getmondo.overdraft.a.a var6) {
      this.a = var2;
      this.b = var3;
      this.c = var4;
      this.d = var5;
      this.e = var6;
      this.f = var1;
   }

   private io.reactivex.b a(String var1) {
      return this.d.b(var1).firstElement().b(e.a(this)).b(this.f);
   }

   // $FF: synthetic method
   static io.reactivex.d a(d var0, w var1) throws Exception {
      io.reactivex.b var2;
      if(var1.d().b()) {
         var2 = var0.a.c("overdraft_charges_item_id");
      } else {
         var2 = var0.a.a(var0.e.a(var1));
      }

      return var2;
   }

   public io.reactivex.b a() {
      ArrayList var1 = new ArrayList();
      String var3 = this.c.c();
      var1.add(var3);
      if(this.c.d() != null) {
         var1.add(this.c.d());
      }

      io.reactivex.b var2 = this.a.a((List)var1).d(this.b.b(var3));
      io.reactivex.b var4 = var2;
      if(this.c.b()) {
         var4 = var2.d(this.d.a(var3)).b((io.reactivex.d)this.a(var3));
      }

      return var4;
   }
}
