package co.uk.getmondo.background_sync;

import android.app.job.JobParameters;
import io.reactivex.c.g;

// $FF: synthetic class
final class b implements g {
   private final SyncFeedAndBalanceJobService a;
   private final JobParameters b;

   private b(SyncFeedAndBalanceJobService var1, JobParameters var2) {
      this.a = var1;
      this.b = var2;
   }

   public static g a(SyncFeedAndBalanceJobService var0, JobParameters var1) {
      return new b(var0, var1);
   }

   public void a(Object var1) {
      SyncFeedAndBalanceJobService.a(this.a, this.b, (Throwable)var1);
   }
}
