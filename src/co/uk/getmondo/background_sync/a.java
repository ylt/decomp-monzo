package co.uk.getmondo.background_sync;

import android.app.job.JobParameters;

// $FF: synthetic class
final class a implements io.reactivex.c.a {
   private final SyncFeedAndBalanceJobService a;
   private final JobParameters b;

   private a(SyncFeedAndBalanceJobService var1, JobParameters var2) {
      this.a = var1;
      this.b = var2;
   }

   public static io.reactivex.c.a a(SyncFeedAndBalanceJobService var0, JobParameters var1) {
      return new a(var0, var1);
   }

   public void a() {
      SyncFeedAndBalanceJobService.a(this.a, this.b);
   }
}
