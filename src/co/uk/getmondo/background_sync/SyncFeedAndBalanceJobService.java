package co.uk.getmondo.background_sync;

import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.app.job.JobInfo.Builder;
import android.content.ComponentName;
import android.content.Context;
import co.uk.getmondo.MonzoApplication;
import co.uk.getmondo.api.ApiException;
import io.reactivex.u;
import io.realm.internal.IOException;

public class SyncFeedAndBalanceJobService extends JobService {
   u a;
   d b;
   private final io.reactivex.b.a c = new io.reactivex.b.a();

   private void a(JobParameters var1, Throwable var2) {
      boolean var3;
      label22: {
         if(var2 instanceof ApiException) {
            if(((ApiException)var2).b() >= 500) {
               var3 = true;
               break label22;
            }
         } else if(!co.uk.getmondo.common.e.a.a(var2) && var2 instanceof IOException) {
            d.a.a.a((Throwable)(new RuntimeException("Rescheduling background refresh", var2)));
            var3 = true;
            break label22;
         }

         var3 = false;
      }

      if(var3) {
         d.a.a.a("Error with background refresh, rescheduling", new Object[0]);
      } else {
         d.a.a.a((Throwable)(new RuntimeException("Giving up trying to perform background refresh", var2)));
      }

      this.jobFinished(var1, var3);
   }

   public static void a(Context var0) {
      ComponentName var1 = new ComponentName(var0, SyncFeedAndBalanceJobService.class);
      d.a.a.a("Creating background refresher job", new Object[0]);
      ((JobScheduler)var0.getSystemService("jobscheduler")).schedule((new Builder(100, var1)).setRequiredNetworkType(1).build());
   }

   // $FF: synthetic method
   static void a(SyncFeedAndBalanceJobService var0, JobParameters var1) throws Exception {
      d.a.a.b("Feed and balance synced successfully", new Object[0]);
      var0.jobFinished(var1, false);
   }

   // $FF: synthetic method
   static void a(SyncFeedAndBalanceJobService var0, JobParameters var1, Throwable var2) throws Exception {
      var0.a(var1, var2);
   }

   public void onCreate() {
      super.onCreate();
      MonzoApplication.a(this).b().a(this);
   }

   public void onDestroy() {
      this.c.a();
      super.onDestroy();
   }

   public boolean onStartJob(JobParameters var1) {
      d.a.a.a("Background sync job triggered", new Object[0]);
      this.c.a(this.b.a().b(this.a).a(a.a(this, var1), b.a(this, var1)));
      return true;
   }

   public boolean onStopJob(JobParameters var1) {
      this.c.a();
      return true;
   }
}
