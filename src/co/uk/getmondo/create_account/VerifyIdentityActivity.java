package co.uk.getmondo.create_account;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.common.address.LegacyEnterAddressActivity;
import co.uk.getmondo.d.s;
import co.uk.getmondo.signup.identity_verification.VerificationPendingActivity;
import co.uk.getmondo.signup_old.r;

public class VerifyIdentityActivity extends co.uk.getmondo.common.activities.b implements g.a {
   g a;
   @BindView(2131821165)
   EditText addressInput;
   private final Handler b = new Handler();
   private final Runnable c = e.a(this);
   @BindView(2131821164)
   EditText dobInput;
   @BindView(2131821163)
   TextInputLayout dobWrapper;
   @BindView(2131820866)
   EditText nameInput;
   @BindView(2131820865)
   TextInputLayout nameWrapper;

   public static Intent a(Context var0) {
      return (new Intent(var0, VerifyIdentityActivity.class)).addFlags(268468224);
   }

   private void j() {
      final r var1 = new r();
      this.dobInput.addTextChangedListener(new TextWatcher() {
         public void afterTextChanged(Editable var1x) {
         }

         public void beforeTextChanged(CharSequence var1x, int var2, int var3, int var4) {
         }

         public void onTextChanged(CharSequence var1x, int var2, int var3, int var4) {
            var1.a(var1x, var3, var4, this, VerifyIdentityActivity.this.dobInput);
         }
      });
   }

   public void a() {
      this.dobWrapper.setErrorEnabled(true);
      this.dobWrapper.setError(this.getString(2131362903));
   }

   public void a(s var1) {
      this.startActivityForResult(LegacyEnterAddressActivity.a(this, this.getString(2131362494), co.uk.getmondo.common.activities.b.a.a, var1), 1);
   }

   public void a(String var1) {
      this.nameInput.setText(var1);
   }

   public void b() {
      this.dobWrapper.setErrorEnabled(true);
      this.dobWrapper.setError(this.getString(2131362148));
   }

   public void c() {
      co.uk.getmondo.common.d.a.a(this.getString(2131362868), this.getString(2131362162), false).show(this.getSupportFragmentManager(), "DUPLICATE_NUMBER");
   }

   public void d() {
      this.nameWrapper.setErrorEnabled(true);
      this.nameWrapper.setError(this.getString(2131362479));
   }

   public void d(String var1) {
      this.dobInput.setText(var1);
   }

   public void e() {
      this.dobWrapper.setErrorEnabled(false);
   }

   public void e(String var1) {
      this.addressInput.setText(var1);
   }

   public void f() {
      this.nameWrapper.setErrorEnabled(false);
   }

   public void g() {
      this.startActivity(VerificationPendingActivity.a(this, co.uk.getmondo.signup.identity_verification.a.j.b, Impression.KycFrom.SIGNUP));
      this.finishAffinity();
   }

   public void h() {
      this.b.removeCallbacks(this.c);
      this.t();
   }

   public void i() {
      this.b.postDelayed(this.c, 300L);
   }

   protected void onActivityResult(int var1, int var2, Intent var3) {
      super.onActivityResult(var1, var2, var3);
      if(var1 == 1 && var2 == -1) {
         s var4 = LegacyEnterAddressActivity.a(var3);
         this.a.a(var4);
      }

   }

   @OnFocusChange({2131821165})
   void onAddressFocusChange(boolean var1) {
      if(var1) {
         this.a.a();
         this.addressInput.clearFocus();
      }

   }

   @SuppressLint({"SetTextI18n"})
   protected void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.setContentView(2131034227);
      ButterKnife.bind((Activity)this);
      this.l().a(this);
      this.j();
      this.getSupportActionBar().b(false);
      this.setTitle(this.getString(2131362263));
      this.a.a((g.a)this);
   }

   @OnFocusChange({2131821164})
   void onDateFocusChange(boolean var1) {
      String var2 = this.dobInput.getText().toString();
      if(var1) {
         this.dobInput.setSelection(var2.length());
      }

   }

   @OnFocusChange({2131821164})
   void onDateOfBirthFocusChange(boolean var1) {
      if(var1) {
         this.a.d();
      }

   }

   protected void onDestroy() {
      this.a.b();
      super.onDestroy();
   }

   @OnClick({2131821166})
   void onDetailsCorrectClick() {
      this.a.a(this.nameInput.getText().toString(), this.dobInput.getText().toString());
   }

   @OnFocusChange({2131820866})
   void onNameFocusChange(boolean var1) {
      if(var1) {
         this.a.c();
      }

   }
}
