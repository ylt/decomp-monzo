package co.uk.getmondo.create_account.wait;

import co.uk.getmondo.api.MonzoApi;
import co.uk.getmondo.api.model.ApiCardDispatchStatus;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.common.q;
import co.uk.getmondo.d.ac;
import co.uk.getmondo.waitlist.i;
import io.reactivex.u;

public class c extends co.uk.getmondo.common.ui.b {
   private final q c;
   private final u d;
   private final u e;
   private final co.uk.getmondo.common.accounts.d f;
   private final co.uk.getmondo.common.e.a g;
   private final MonzoApi h;
   private final co.uk.getmondo.common.a i;
   private final i j;

   c(u var1, u var2, co.uk.getmondo.common.accounts.d var3, co.uk.getmondo.common.e.a var4, MonzoApi var5, co.uk.getmondo.common.a var6, q var7, i var8) {
      this.d = var1;
      this.e = var2;
      this.f = var3;
      this.g = var4;
      this.h = var5;
      this.i = var6;
      this.c = var7;
      this.j = var8;
   }

   // $FF: synthetic method
   static void a(c var0, c.a var1, ApiCardDispatchStatus var2) throws Exception {
      var1.d();
      ac var3 = var0.f.b().d();
      if(var2.a() != null) {
         var1.a(co.uk.getmondo.common.k.a.a(var3.h()), co.uk.getmondo.common.c.a.b(var2.a().getTime()));
      } else {
         var1.a(co.uk.getmondo.common.k.a.a(var3.h()));
      }

   }

   // $FF: synthetic method
   static void a(c var0, c.a var1, Throwable var2) throws Exception {
      var0.g.a(var2, var1);
   }

   void a() {
      ((c.a)this.a).c();
   }

   public void a(c.a var1) {
      super.a((co.uk.getmondo.common.ui.f)var1);
      this.i.a(Impression.A());
      var1.e();
      this.a((io.reactivex.b.b)this.h.getCardDispatchStatus(this.f.b().c().a()).b(this.e).a(this.d).a(d.a(this, var1), e.a(this, var1)));
      if(this.j.a()) {
         var1.a();
      } else {
         var1.b();
      }

   }

   void c() {
      this.i.a(Impression.a(Impression.IntercomFrom.GOOD_TO_GO));
      this.c.a();
   }

   interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      void a();

      void a(String var1);

      void a(String var1, String var2);

      void b();

      void c();

      void d();

      void e();
   }
}
