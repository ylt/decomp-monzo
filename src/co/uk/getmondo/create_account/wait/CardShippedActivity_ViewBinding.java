package co.uk.getmondo.create_account.wait;

import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;

public class CardShippedActivity_ViewBinding implements Unbinder {
   private CardShippedActivity a;
   private View b;
   private View c;

   public CardShippedActivity_ViewBinding(final CardShippedActivity var1, View var2) {
      this.a = var1;
      var1.descriptionView = (TextView)Utils.findRequiredViewAsType(var2, 2131820842, "field 'descriptionView'", TextView.class);
      var1.descriptionOnceArrivedView = (TextView)Utils.findRequiredViewAsType(var2, 2131820847, "field 'descriptionOnceArrivedView'", TextView.class);
      var1.animationView = (ImageView)Utils.findRequiredViewAsType(var2, 2131820843, "field 'animationView'", ImageView.class);
      var1.buttonsContainer = (ViewGroup)Utils.findRequiredViewAsType(var2, 2131820844, "field 'buttonsContainer'", ViewGroup.class);
      View var3 = Utils.findRequiredView(var2, 2131820845, "method 'onCardArrivedClick'");
      this.b = var3;
      var3.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View var1x) {
            var1.onCardArrivedClick();
         }
      });
      var2 = Utils.findRequiredView(var2, 2131820846, "method 'onCardDidntArriveClick'");
      this.c = var2;
      var2.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View var1x) {
            var1.onCardDidntArriveClick();
         }
      });
   }

   public void unbind() {
      CardShippedActivity var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.descriptionView = null;
         var1.descriptionOnceArrivedView = null;
         var1.animationView = null;
         var1.buttonsContainer = null;
         this.b.setOnClickListener((OnClickListener)null);
         this.b = null;
         this.c.setOnClickListener((OnClickListener)null);
         this.c = null;
      }
   }
}
