package co.uk.getmondo.create_account.wait;

import co.uk.getmondo.api.model.ApiCardDispatchStatus;
import io.reactivex.c.g;

// $FF: synthetic class
final class d implements g {
   private final c a;
   private final c.a b;

   private d(c var1, c.a var2) {
      this.a = var1;
      this.b = var2;
   }

   public static g a(c var0, c.a var1) {
      return new d(var0, var1);
   }

   public void a(Object var1) {
      c.a(this.a, this.b, (ApiCardDispatchStatus)var1);
   }
}
