package co.uk.getmondo.create_account;

import android.text.TextWatcher;
import android.widget.EditText;

public class a {
   private static String a(CharSequence var0) {
      String var7 = var0.toString();
      String var5;
      if(var7.length() > 19) {
         var5 = var7.substring(0, 19);
      } else {
         char[] var6 = var7.replace(" ", "").toCharArray();
         int var4 = var6.length;
         int var3 = 0;
         int var2 = 0;
         var7 = "";

         while(true) {
            var5 = var7;
            if(var2 >= var4) {
               break;
            }

            char var1 = var6[var2];
            var5 = var7 + var1;
            ++var3;
            var7 = var5;
            if(var3 > 0) {
               var7 = var5;
               if(var3 < 16) {
                  var7 = var5;
                  if(var3 % 4 == 0) {
                     var7 = var5 + " ";
                  }
               }
            }

            ++var2;
         }
      }

      return var5;
   }

   public static void a(CharSequence var0, int var1, EditText var2, TextWatcher var3) {
      if(var1 > 0) {
         String var4 = a(var0.toString());
         var2.removeTextChangedListener(var3);
         var2.setText(var4);
         var2.setSelection(var4.length());
         var2.addTextChangedListener(var3);
      }

   }
}
