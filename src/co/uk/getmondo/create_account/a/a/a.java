package co.uk.getmondo.create_account.a.a;

import co.uk.getmondo.create_account.b;
import co.uk.getmondo.d.s;
import java.util.Map;
import kotlin.Metadata;
import kotlin.h;
import kotlin.a.ab;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0010$\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0006\u0010\u0007\u001a\u00020\b¢\u0006\u0002\u0010\tJ\t\u0010\u0011\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0012\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0013\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0014\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0015\u001a\u00020\bHÆ\u0003J;\u0010\u0016\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u00032\b\b\u0002\u0010\u0007\u001a\u00020\bHÆ\u0001J\u0012\u0010\u0017\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00030\u0018J\u0013\u0010\u0019\u001a\u00020\u001a2\b\u0010\u001b\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u001c\u001a\u00020\u001dHÖ\u0001J\t\u0010\u001e\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0007\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\u0005\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\rR\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\rR\u0011\u0010\u0006\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\r¨\u0006\u001f"},
   d2 = {"Lco/uk/getmondo/create_account/data/model/SignUpRequest;", "", "email", "", "name", "dateOfBirth", "phoneNumber", "address", "Lco/uk/getmondo/model/LegacyAddress;", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/model/LegacyAddress;)V", "getAddress", "()Lco/uk/getmondo/model/LegacyAddress;", "getDateOfBirth", "()Ljava/lang/String;", "getEmail", "getName", "getPhoneNumber", "component1", "component2", "component3", "component4", "component5", "copy", "createMap", "", "equals", "", "other", "hashCode", "", "toString", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class a {
   private final String a;
   private final String b;
   private final String c;
   private final String d;
   private final s e;

   public a(String var1, String var2, String var3, String var4, s var5) {
      l.b(var1, "email");
      l.b(var2, "name");
      l.b(var3, "dateOfBirth");
      l.b(var4, "phoneNumber");
      l.b(var5, "address");
      super();
      this.a = var1;
      this.b = var2;
      this.c = var3;
      this.d = var4;
      this.e = var5;
   }

   public final Map a() {
      String var1 = this.e.b();
      if(var1 == null) {
         var1 = "";
      }

      String var2 = this.e.i();
      if(var2 == null) {
         var2 = "";
      }

      String var3 = this.e.c();
      if(var3 == null) {
         var3 = "";
      }

      String var4 = this.e.j();
      if(var4 == null) {
         var4 = "";
      }

      h var6 = kotlin.l.a("email", this.a);
      h var5 = kotlin.l.a("name", this.b);
      String var7 = b.c(this.c);
      if(var7 == null) {
         l.a();
      }

      return ab.a(new h[]{var6, var5, kotlin.l.a("date_of_birth", var7), kotlin.l.a("phone_number", this.d), kotlin.l.a("address[locality]", var2), kotlin.l.a("address[administrative_area]", var1), kotlin.l.a("address[postal_code]", var3), kotlin.l.a("address[country]", var4)});
   }

   public final s b() {
      return this.e;
   }

   public boolean equals(Object var1) {
      boolean var2;
      if(this != var1) {
         label34: {
            if(var1 instanceof a) {
               a var3 = (a)var1;
               if(l.a(this.a, var3.a) && l.a(this.b, var3.b) && l.a(this.c, var3.c) && l.a(this.d, var3.d) && l.a(this.e, var3.e)) {
                  break label34;
               }
            }

            var2 = false;
            return var2;
         }
      }

      var2 = true;
      return var2;
   }

   public int hashCode() {
      int var5 = 0;
      String var6 = this.a;
      int var1;
      if(var6 != null) {
         var1 = var6.hashCode();
      } else {
         var1 = 0;
      }

      var6 = this.b;
      int var2;
      if(var6 != null) {
         var2 = var6.hashCode();
      } else {
         var2 = 0;
      }

      var6 = this.c;
      int var3;
      if(var6 != null) {
         var3 = var6.hashCode();
      } else {
         var3 = 0;
      }

      var6 = this.d;
      int var4;
      if(var6 != null) {
         var4 = var6.hashCode();
      } else {
         var4 = 0;
      }

      s var7 = this.e;
      if(var7 != null) {
         var5 = var7.hashCode();
      }

      return (var4 + (var3 + (var2 + var1 * 31) * 31) * 31) * 31 + var5;
   }

   public String toString() {
      return "SignUpRequest(email=" + this.a + ", name=" + this.b + ", dateOfBirth=" + this.c + ", phoneNumber=" + this.d + ", address=" + this.e + ")";
   }
}
