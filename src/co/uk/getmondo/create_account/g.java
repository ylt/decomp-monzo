package co.uk.getmondo.create_account;

import co.uk.getmondo.api.ApiException;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.common.k.p;
import co.uk.getmondo.d.ac;
import co.uk.getmondo.d.ak;
import co.uk.getmondo.d.s;
import io.reactivex.u;
import java.text.ParseException;
import java.util.Date;

public class g extends co.uk.getmondo.common.ui.b {
   private final u c;
   private final u d;
   private final co.uk.getmondo.common.accounts.d e;
   private final co.uk.getmondo.common.e.a f;
   private final co.uk.getmondo.api.b.a g;
   private final co.uk.getmondo.common.a h;

   g(u var1, u var2, co.uk.getmondo.common.accounts.d var3, co.uk.getmondo.common.e.a var4, co.uk.getmondo.api.b.a var5, co.uk.getmondo.common.a var6) {
      this.c = var1;
      this.d = var2;
      this.e = var3;
      this.f = var4;
      this.g = var5;
      this.h = var6;
   }

   // $FF: synthetic method
   static void a(g var0) throws Exception {
      ((g.a)var0.a).g();
   }

   // $FF: synthetic method
   static void a(g var0, Throwable var1) throws Exception {
      if(var1 instanceof ApiException) {
         co.uk.getmondo.api.model.b var2 = ((ApiException)var1).e();
         d var3 = (d)co.uk.getmondo.common.e.d.a(d.values(), var2.a());
         if(d.a.equals(var3)) {
            ((g.a)var0.a).c();
            return;
         }
      }

      var0.f.a(var1, (co.uk.getmondo.common.e.a.a)var0.a);
   }

   // $FF: synthetic method
   static void b(g var0, Throwable var1) throws Exception {
      ((g.a)var0.a).h();
   }

   void a() {
      this.h.a(Impression.x());
      s var1 = this.e.b().d().h();
      ((g.a)this.a).a(var1);
   }

   public void a(g.a var1) {
      super.a((co.uk.getmondo.common.ui.f)var1);
      this.h.a(Impression.u());
      ak var2 = this.e.b();
      ((g.a)this.a).a(var2.d().c());
      ((g.a)this.a).d(b.b(var2.d().e()));
      ((g.a)this.a).e(co.uk.getmondo.common.k.a.a(var2.d().h()));
   }

   void a(s var1) {
      ak var2 = this.e.b();
      this.e.a(var2.d().a(var1));
      ((g.a)this.a).e(co.uk.getmondo.common.k.a.a(var1));
   }

   void a(String var1, String var2) {
      ((g.a)this.a).e();
      ((g.a)this.a).f();
      Date var3 = new Date();

      Date var4;
      try {
         var4 = b.a(var2);
      } catch (ParseException var7) {
         ((g.a)this.a).b();
         return;
      }

      if(b.b(var4, var3)) {
         ((g.a)this.a).b();
      } else if(!b.a(var4, var3)) {
         ((g.a)this.a).a();
      } else if(!p.a(var1)) {
         ((g.a)this.a).d();
      } else {
         ((g.a)this.a).i();
         ak var6 = this.e.b();
         String var8 = var6.d().f();
         String var5 = var6.d().d();
         s var9 = var6.d().h();
         ac var10 = var6.d().a(var1, b.c(var2));
         this.e.a(var10);
         this.a((io.reactivex.b.b)this.g.a(new co.uk.getmondo.create_account.a.a.a(var5, var1, var2, var8, var9)).b(this.d).a(this.c).b(h.a(this)).a(i.a(this), j.a(this)));
      }

   }

   void c() {
      this.h.a(Impression.v());
   }

   void d() {
      this.h.a(Impression.w());
   }

   interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      void a();

      void a(s var1);

      void a(String var1);

      void b();

      void c();

      void d();

      void d(String var1);

      void e();

      void e(String var1);

      void f();

      void g();

      void h();

      void i();
   }
}
