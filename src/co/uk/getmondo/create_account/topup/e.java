package co.uk.getmondo.create_account.topup;

import co.uk.getmondo.api.model.ApiInitialTopupStatus;

// $FF: synthetic class
final class e implements io.reactivex.c.h {
   private final c a;

   private e(c var1) {
      this.a = var1;
   }

   public static io.reactivex.c.h a(c var0) {
      return new e(var0);
   }

   public Object a(Object var1) {
      return c.a(this.a, (ApiInitialTopupStatus)var1);
   }
}
