package co.uk.getmondo.create_account.topup;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.uk.getmondo.create_account.wait.CardShippedActivity;
import co.uk.getmondo.main.HomeActivity;

public class TopupInfoActivity extends co.uk.getmondo.common.activities.b implements r.a {
   r a;
   @BindView(2131821026)
   TextView titleTextView;
   @BindView(2131821133)
   View topupButton;

   public io.reactivex.n a() {
      return com.b.a.c.c.a(this.topupButton);
   }

   public void a(co.uk.getmondo.d.c var1) {
      InitialTopupActivity.a(this, var1);
   }

   public void a(boolean var1) {
      this.topupButton.setEnabled(var1);
   }

   public void b() {
      CardShippedActivity.a(this, true);
   }

   public void b(co.uk.getmondo.d.c var1) {
      this.titleTextView.setText(this.getString(2131362779, new Object[]{var1.toString()}));
   }

   public void c() {
      HomeActivity.a((Context)this);
   }

   @SuppressLint({"SetTextI18n"})
   protected void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.setContentView(2131034221);
      ButterKnife.bind((Activity)this);
      this.l().a(this);
      this.a.a((r.a)this);
   }

   protected void onDestroy() {
      super.onDestroy();
      this.a.b();
   }
}
