package co.uk.getmondo.create_account.topup;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import co.uk.getmondo.create_account.wait.CardShippedActivity;
import co.uk.getmondo.topup.card.TopUpWithCardFragment;

public class InitialTopupActivity extends co.uk.getmondo.common.activities.b implements g.a, TopUpWithCardFragment.a {
   g a;

   public static void a(Context var0, co.uk.getmondo.d.c var1) {
      Intent var2 = new Intent(var0, InitialTopupActivity.class);
      var2.putExtra("EXTRA_TOPUP_AMOUNT", var1);
      var0.startActivity(var2);
   }

   // $FF: synthetic method
   static void a(String var0, String var1, co.uk.getmondo.common.f.c.a var2, co.uk.getmondo.common.f.c var3) {
      var3.a(var0, var1, var2);
   }

   public void a() {
      CardShippedActivity.a(this, false);
   }

   public void a(String var1, String var2, co.uk.getmondo.common.f.c.a var3) {
      this.d.a(a.a(var1, var2, var3));
   }

   public boolean a(Throwable var1) {
      return this.a.a(var1);
   }

   public void b() {
      this.a.a();
   }

   public void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.l().a(this);
      co.uk.getmondo.d.c var2 = (co.uk.getmondo.d.c)this.getIntent().getParcelableExtra("EXTRA_TOPUP_AMOUNT");
      this.setTitle(this.getString(2131362777, new Object[]{var2.toString()}));
      if(this.getSupportFragmentManager().a(16908290) == null) {
         this.getSupportFragmentManager().a().b(16908290, TopUpWithCardFragment.a(var2, true)).c();
      }

      this.a.a((g.a)this);
   }

   protected void onDestroy() {
      super.onDestroy();
      this.a.b();
   }
}
