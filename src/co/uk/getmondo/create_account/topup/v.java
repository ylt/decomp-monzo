package co.uk.getmondo.create_account.topup;

import co.uk.getmondo.d.ak;

// $FF: synthetic class
final class v implements io.reactivex.c.h {
   private final r a;
   private final r.a b;

   private v(r var1, r.a var2) {
      this.a = var1;
      this.b = var2;
   }

   public static io.reactivex.c.h a(r var0, r.a var1) {
      return new v(var0, var1);
   }

   public Object a(Object var1) {
      return r.a(this.a, this.b, (ak)var1);
   }
}
