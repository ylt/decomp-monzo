package co.uk.getmondo.create_account.topup;

import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.d.ak;
import io.reactivex.z;

public class r extends co.uk.getmondo.common.ui.b {
   private final io.reactivex.u c;
   private final io.reactivex.u d;
   private final co.uk.getmondo.common.e.a e;
   private final co.uk.getmondo.common.a f;
   private final co.uk.getmondo.api.b.a g;
   private final c h;
   private co.uk.getmondo.d.c i;

   r(io.reactivex.u var1, io.reactivex.u var2, co.uk.getmondo.common.e.a var3, co.uk.getmondo.common.a var4, co.uk.getmondo.api.b.a var5, c var6) {
      this.c = var1;
      this.d = var2;
      this.e = var3;
      this.f = var4;
      this.g = var5;
      this.h = var6;
   }

   // $FF: synthetic method
   static z a(r var0, r.a var1, ak var2) throws Exception {
      io.reactivex.v var3;
      switch(null.a[var2.a().ordinal()]) {
      case 1:
         var1.b();
         var3 = io.reactivex.v.o_();
         break;
      case 2:
         var1.c();
         var3 = io.reactivex.v.o_();
         break;
      default:
         var3 = var0.h.b().b(var0.d).a(var0.c);
      }

      return var3;
   }

   // $FF: synthetic method
   static void a(r.a var0, io.reactivex.b.b var1) throws Exception {
      var0.a(false);
   }

   // $FF: synthetic method
   static void a(r var0, r.a var1, co.uk.getmondo.d.c var2) throws Exception {
      var0.i = var2;
      var1.b(var0.i);
      var1.a(true);
   }

   // $FF: synthetic method
   static void a(r var0, r.a var1, Object var2) throws Exception {
      var1.a(var0.i);
   }

   // $FF: synthetic method
   static void a(r var0, r.a var1, Throwable var2) throws Exception {
      var0.e.a(var2, var1);
   }

   // $FF: synthetic method
   static boolean a(r var0, Object var1) throws Exception {
      boolean var2;
      if(var0.i == null) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   public void a(r.a var1) {
      super.a((co.uk.getmondo.common.ui.f)var1);
      this.f.a(Impression.y());
      this.a((io.reactivex.b.b)var1.a().skipWhile(s.a(this)).subscribe(t.a(this, var1)));
      this.a((io.reactivex.b.b)this.g.a().b(this.d).a(this.c).b(u.a(var1)).a(v.a(this, var1)).a(w.a(this, var1), x.a(this, var1)));
   }

   interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      io.reactivex.n a();

      void a(co.uk.getmondo.d.c var1);

      void a(boolean var1);

      void b();

      void b(co.uk.getmondo.d.c var1);

      void c();
   }
}
