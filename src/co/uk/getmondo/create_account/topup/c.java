package co.uk.getmondo.create_account.topup;

import co.uk.getmondo.api.MonzoApi;
import co.uk.getmondo.api.model.ApiInitialTopupStatus;

public class c {
   private static final co.uk.getmondo.d.c a;
   private final co.uk.getmondo.common.accounts.d b;
   private final MonzoApi c;

   static {
      a = new co.uk.getmondo.d.c(10000L, co.uk.getmondo.common.i.c.a);
   }

   public c(co.uk.getmondo.common.accounts.d var1, MonzoApi var2) {
      this.b = var1;
      this.c = var2;
   }

   private co.uk.getmondo.d.c a(ApiInitialTopupStatus var1) {
      return new co.uk.getmondo.d.c(var1.b(), var1.c());
   }

   // $FF: synthetic method
   static co.uk.getmondo.d.c a(c var0, ApiInitialTopupStatus var1) {
      return var0.a(var1);
   }

   public io.reactivex.v a() {
      io.reactivex.v var1;
      if(this.b.b().c() != null) {
         var1 = this.c.getInitialTopupStatus(this.b.b().c().a()).d(d.a());
      } else {
         var1 = io.reactivex.v.a((Object)Boolean.valueOf(false));
      }

      return var1;
   }

   public io.reactivex.v b() {
      co.uk.getmondo.d.a var1 = this.b.b().c();
      io.reactivex.v var2;
      if(var1 != null) {
         var2 = this.c.getInitialTopupStatus(var1.a()).d(e.a(this));
      } else {
         var2 = io.reactivex.v.a((Object)a);
      }

      return var2;
   }
}
