package co.uk.getmondo.create_account.topup;

import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class TopupInfoActivity_ViewBinding implements Unbinder {
   private TopupInfoActivity a;

   public TopupInfoActivity_ViewBinding(TopupInfoActivity var1, View var2) {
      this.a = var1;
      var1.titleTextView = (TextView)Utils.findRequiredViewAsType(var2, 2131821026, "field 'titleTextView'", TextView.class);
      var1.topupButton = Utils.findRequiredView(var2, 2131821133, "field 'topupButton'");
   }

   public void unbind() {
      TopupInfoActivity var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.titleTextView = null;
         var1.topupButton = null;
      }
   }
}
