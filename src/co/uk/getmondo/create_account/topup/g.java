package co.uk.getmondo.create_account.topup;

import co.uk.getmondo.api.ApiException;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.d.ab;
import java.util.concurrent.TimeUnit;

public class g extends co.uk.getmondo.common.ui.b {
   private final io.reactivex.u c;
   private final io.reactivex.u d;
   private final co.uk.getmondo.common.accounts.d e;
   private final co.uk.getmondo.common.e.a f;
   private final co.uk.getmondo.common.q g;
   private final o h;
   private final co.uk.getmondo.common.a i;
   private final co.uk.getmondo.common.m j;
   private final c k;

   g(io.reactivex.u var1, io.reactivex.u var2, co.uk.getmondo.common.accounts.d var3, co.uk.getmondo.common.e.a var4, co.uk.getmondo.common.q var5, o var6, co.uk.getmondo.common.a var7, co.uk.getmondo.common.m var8, c var9) {
      this.c = var1;
      this.d = var2;
      this.e = var3;
      this.f = var4;
      this.g = var5;
      this.h = var6;
      this.i = var7;
      this.j = var8;
      this.k = var9;
   }

   // $FF: synthetic method
   static void a(g var0) {
      var0.i.a(Impression.a(Impression.IntercomFrom.INITIAL_TOPUP));
      var0.g.a();
   }

   // $FF: synthetic method
   static void a(g var0, g.a var1, Long var2) throws Exception {
      var1.a(var0.h.c(), var0.h.b(), m.a(var0));
   }

   // $FF: synthetic method
   static void a(g var0, Throwable var1) throws Exception {
      var0.f.a(var1, (co.uk.getmondo.common.e.a.a)var0.a);
   }

   // $FF: synthetic method
   static void a(g var0, boolean var1) {
      var0.a(var1);
   }

   private void a(boolean var1) {
      if(var1) {
         this.a();
      } else {
         g.a var5 = (g.a)this.a;
         String var3 = this.h.a();
         String var2 = this.h.b();
         co.uk.getmondo.common.q var4 = this.g;
         var4.getClass();
         var5.a(var3, var2, l.a(var4));
      }

   }

   void a() {
      ab var1 = (ab)this.e.b().c();
      this.e.a(var1.b(true));
      ((g.a)this.a).a();
      this.j.b();
   }

   public void a(g.a var1) {
      super.a((co.uk.getmondo.common.ui.f)var1);
      this.i.a(Impression.z());
      this.a((io.reactivex.b.b)io.reactivex.n.timer(30L, TimeUnit.SECONDS).observeOn(this.c).subscribe(h.a(this, var1), i.a()));
   }

   boolean a(Throwable var1) {
      boolean var2;
      if(var1 instanceof ApiException) {
         co.uk.getmondo.api.model.b var3 = ((ApiException)var1).e();
         if(var3 != null) {
            co.uk.getmondo.topup.a.v var4 = (co.uk.getmondo.topup.a.v)co.uk.getmondo.common.e.d.a(co.uk.getmondo.topup.a.v.values(), var3.a());
            if(co.uk.getmondo.topup.a.v.g.equals(var4)) {
               this.a((io.reactivex.b.b)this.k.a().b(this.d).a(this.c).a(j.a(this), k.a(this)));
               var2 = true;
               return var2;
            }
         }
      }

      var2 = false;
      return var2;
   }

   interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      void a();

      void a(String var1, String var2, co.uk.getmondo.common.f.c.a var3);
   }
}
