package co.uk.getmondo.create_account.phone_number;

import co.uk.getmondo.api.MonzoApi;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.d.ak;

public class x extends co.uk.getmondo.common.ui.b {
   private final io.reactivex.u c;
   private final io.reactivex.u d;
   private final co.uk.getmondo.common.e.a e;
   private final co.uk.getmondo.common.accounts.d f;
   private final MonzoApi g;
   private final co.uk.getmondo.common.a h;

   x(io.reactivex.u var1, io.reactivex.u var2, co.uk.getmondo.common.e.a var3, co.uk.getmondo.common.accounts.d var4, MonzoApi var5, co.uk.getmondo.common.a var6) {
      this.c = var1;
      this.d = var2;
      this.e = var3;
      this.f = var4;
      this.g = var5;
      this.h = var6;
   }

   // $FF: synthetic method
   static io.reactivex.l a(x var0, x.a var1, String var2) throws Exception {
      return var0.g.sendVerificationCode(var2).a((Object)co.uk.getmondo.common.b.a.a).e().b(var0.d).a(var0.c).a(ad.a(var0, var1)).f().a(ae.a(var1));
   }

   // $FF: synthetic method
   static void a(x.a var0, co.uk.getmondo.common.b.a var1) throws Exception {
      var0.d();
   }

   // $FF: synthetic method
   static void a(x.a var0, co.uk.getmondo.common.b.a var1, Throwable var2) throws Exception {
      var0.e();
   }

   // $FF: synthetic method
   static void a(x.a var0, Boolean var1) throws Exception {
      if(!var1.booleanValue()) {
         var0.c();
      }

   }

   // $FF: synthetic method
   static void a(x var0, x.a var1, ak var2, String var3) throws Exception {
      var1.f();
      var0.f.a(var2.d().a(var3));
   }

   // $FF: synthetic method
   static void a(x var0, x.a var1, Throwable var2) throws Exception {
      var0.e.a(var2, var1);
   }

   public void a(x.a var1) {
      super.a((co.uk.getmondo.common.ui.f)var1);
      this.h.a(Impression.s());
      ak var3 = this.f.b();
      co.uk.getmondo.d.ac var2 = null;
      if(var3 != null) {
         var2 = var3.d();
      }

      if(var2 != null) {
         var1.a(var2.f());
      }

      this.a((io.reactivex.b.b)var1.a().doOnNext(y.a(this, var1, var3)).flatMapMaybe(z.a(this, var1)).subscribe(aa.a(var1), ab.a()));
      this.a((io.reactivex.b.b)var1.b().subscribe(ac.a(var1)));
   }

   interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      io.reactivex.n a();

      void a(String var1);

      io.reactivex.n b();

      void c();

      void d();

      void e();

      void f();
   }
}
