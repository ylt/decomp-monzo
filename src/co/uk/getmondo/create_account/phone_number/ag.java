package co.uk.getmondo.create_account.phone_number;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Build.VERSION;
import android.telephony.SmsMessage;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import kotlin.Metadata;
import kotlin.TypeCastException;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0011\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 \u00102\u00020\u0001:\u0001\u0010B\u0005¢\u0006\u0002\u0010\u0002J\u001c\u0010\n\u001a\u00020\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\r2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000fH\u0016R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u000e¢\u0006\u0002\n\u0000R\u0016\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\bX\u0082\u000e¢\u0006\u0004\n\u0002\u0010\t¨\u0006\u0011"},
   d2 = {"Lco/uk/getmondo/create_account/phone_number/SmsCodeReceiver;", "Landroid/content/BroadcastReceiver;", "()V", "listener", "Lco/uk/getmondo/create_account/phone_number/CodeListener;", "regex", "", "sender", "", "[Ljava/lang/String;", "onReceive", "", "context", "Landroid/content/Context;", "intent", "Landroid/content/Intent;", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ag extends BroadcastReceiver {
   public static final ag.a a = new ag.a((kotlin.d.b.i)null);
   private String b = "";
   private String[] c = new String[0];
   private a d;

   public static final void a(Context var0, ag var1) {
      kotlin.d.b.l.b(var0, "context");
      kotlin.d.b.l.b(var1, "smsCodeReceiver");
      a.a(var0, var1);
   }

   public static final void a(Context var0, ag var1, String var2, String[] var3, a var4) {
      kotlin.d.b.l.b(var0, "context");
      kotlin.d.b.l.b(var1, "smsCodeReceiver");
      kotlin.d.b.l.b(var2, "regex");
      kotlin.d.b.l.b(var3, "sender");
      kotlin.d.b.l.b(var4, "listener");
      a.a(var0, var1, var2, var3, var4);
   }

   public void onReceive(Context var1, Intent var2) {
      Pattern var5 = null;
      Bundle var10;
      if(var2 != null) {
         var10 = var2.getExtras();
      } else {
         var10 = null;
      }

      Object var11 = var5;
      if(var10 != null) {
         var11 = var10.get("pdus");
      }

      if(var11 == null) {
         throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<*>");
      } else {
         Object[] var14 = (Object[])var11;
         String var6 = var10.getString("format");
         var5 = Pattern.compile(this.b);

         for(int var3 = 0; var3 < var14.length; ++var3) {
            Object var12 = var14[var3];
            SmsMessage var13;
            if(VERSION.SDK_INT >= 23) {
               var13 = SmsMessage.createFromPdu((byte[])var12, var6);
            } else {
               var13 = SmsMessage.createFromPdu((byte[])var12);
            }

            String var9 = var13.getDisplayOriginatingAddress();
            Object[] var7 = (Object[])this.c;
            int var4 = 0;

            boolean var17;
            while(true) {
               if(var4 >= var7.length) {
                  var17 = false;
                  break;
               }

               CharSequence var8 = (CharSequence)((String)var7[var4]);
               kotlin.d.b.l.a(var9, "originatingAddress");
               if(kotlin.h.j.b(var8, (CharSequence)var9, true)) {
                  var17 = true;
                  break;
               }

               ++var4;
            }

            if(!var17) {
               break;
            }

            Matcher var15 = var5.matcher((CharSequence)var13.getDisplayMessageBody());
            if(var15.find()) {
               String var16 = var15.group();
               a var18 = this.d;
               if(var18 != null) {
                  var18.a(var16);
               }
            }
         }

      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J;\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00042\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00040\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0007¢\u0006\u0002\u0010\u0014J\u0018\u0010\u0015\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0007R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0086T¢\u0006\u0002\n\u0000¨\u0006\u0016"},
      d2 = {"Lco/uk/getmondo/create_account/phone_number/SmsCodeReceiver$Companion;", "", "()V", "ACTION", "", "KEY_FORMAT", "KEY_PDUS", "PRIORITY", "", "register", "", "context", "Landroid/content/Context;", "smsCodeReceiver", "Lco/uk/getmondo/create_account/phone_number/SmsCodeReceiver;", "regex", "sender", "", "listener", "Lco/uk/getmondo/create_account/phone_number/CodeListener;", "(Landroid/content/Context;Lco/uk/getmondo/create_account/phone_number/SmsCodeReceiver;Ljava/lang/String;[Ljava/lang/String;Lco/uk/getmondo/create_account/phone_number/CodeListener;)V", "unregister", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var1) {
         this();
      }

      public final void a(Context var1, ag var2) {
         kotlin.d.b.l.b(var1, "context");
         kotlin.d.b.l.b(var2, "smsCodeReceiver");
         var1.unregisterReceiver((BroadcastReceiver)var2);
      }

      public final void a(Context var1, ag var2, String var3, String[] var4, a var5) {
         kotlin.d.b.l.b(var1, "context");
         kotlin.d.b.l.b(var2, "smsCodeReceiver");
         kotlin.d.b.l.b(var3, "regex");
         kotlin.d.b.l.b(var4, "sender");
         kotlin.d.b.l.b(var5, "listener");
         var2.b = var3;
         var2.c = var4;
         var2.d = var5;
         IntentFilter var6 = new IntentFilter("android.provider.Telephony.SMS_RECEIVED");
         var6.setPriority(999);
         var1.registerReceiver((BroadcastReceiver)var2, var6);
      }
   }
}
