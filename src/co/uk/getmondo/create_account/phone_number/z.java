package co.uk.getmondo.create_account.phone_number;

// $FF: synthetic class
final class z implements io.reactivex.c.h {
   private final x a;
   private final x.a b;

   private z(x var1, x.a var2) {
      this.a = var1;
      this.b = var2;
   }

   public static io.reactivex.c.h a(x var0, x.a var1) {
      return new z(var0, var1);
   }

   public Object a(Object var1) {
      return x.a(this.a, this.b, (String)var1);
   }
}
