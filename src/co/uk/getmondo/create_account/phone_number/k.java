package co.uk.getmondo.create_account.phone_number;

import co.uk.getmondo.api.MonzoApi;
import co.uk.getmondo.api.model.ApiCodeCheckResult;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.d.ak;

public class k extends co.uk.getmondo.common.ui.b {
   private final io.reactivex.u c;
   private final io.reactivex.u d;
   private final co.uk.getmondo.common.accounts.d e;
   private final MonzoApi f;
   private final co.uk.getmondo.common.a g;
   private final co.uk.getmondo.common.e.a h;

   k(io.reactivex.u var1, io.reactivex.u var2, co.uk.getmondo.common.accounts.d var3, MonzoApi var4, co.uk.getmondo.common.a var5, co.uk.getmondo.common.e.a var6) {
      this.c = var1;
      this.d = var2;
      this.e = var3;
      this.f = var4;
      this.g = var5;
      this.h = var6;
   }

   // $FF: synthetic method
   static io.reactivex.l a(k var0, ak var1, k.a var2, String var3) throws Exception {
      return var0.f.checkVerificationCode(var1.d().f(), var3).e().b(var0.d).a(var0.c).b(o.a(var2)).a(p.a(var0, var2)).a((io.reactivex.l)io.reactivex.h.a()).a(q.a(var2));
   }

   // $FF: synthetic method
   static void a(k.a var0, ApiCodeCheckResult var1) throws Exception {
      if(!var1.a()) {
         var0.b(2131362180);
      } else {
         var0.a();
      }

   }

   // $FF: synthetic method
   static void a(k.a var0, ApiCodeCheckResult var1, Throwable var2) throws Exception {
      var0.t();
      var0.a(true);
   }

   // $FF: synthetic method
   static void a(k.a var0, io.reactivex.b.b var1) throws Exception {
      var0.s();
      var0.a(false);
   }

   // $FF: synthetic method
   static void a(k var0, k.a var1, Throwable var2) throws Exception {
      var0.h.a(var2, var1);
   }

   public void a(k.a var1) {
      super.a((co.uk.getmondo.common.ui.f)var1);
      this.g.a(Impression.t());
      ak var2 = this.e.b();
      this.a((io.reactivex.b.b)var1.b().flatMapMaybe(l.a(this, var2, var1)).observeOn(this.c).subscribe(m.a(var1), n.a()));
   }

   interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      void a();

      void a(boolean var1);

      io.reactivex.n b();

      void s();

      void t();
   }
}
