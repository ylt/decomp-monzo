package co.uk.getmondo.create_account.phone_number;

import co.uk.getmondo.d.ak;

// $FF: synthetic class
final class l implements io.reactivex.c.h {
   private final k a;
   private final ak b;
   private final k.a c;

   private l(k var1, ak var2, k.a var3) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
   }

   public static io.reactivex.c.h a(k var0, ak var1, k.a var2) {
      return new l(var0, var1, var2);
   }

   public Object a(Object var1) {
      return k.a(this.a, this.b, this.c, (String)var1);
   }
}
