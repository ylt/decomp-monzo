package co.uk.getmondo.create_account.phone_number;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class EnterPhoneNumberActivity extends co.uk.getmondo.common.activities.b implements x.a {
   x a;
   private io.reactivex.n b;
   private final com.b.b.c c = com.b.b.c.a();
   private final com.b.b.c e = com.b.b.c.a();
   @BindView(2131820905)
   EditText phoneNumberInput;
   @BindView(2131820906)
   Button textCodeButton;

   // $FF: synthetic method
   static Boolean a(EnterPhoneNumberActivity var0, Object var1) throws Exception {
      return Boolean.valueOf(co.uk.getmondo.common.k.k.a(var0));
   }

   public static void a(Context var0) {
      var0.startActivity(new Intent(var0, EnterPhoneNumberActivity.class));
   }

   // $FF: synthetic method
   static boolean a(EnterPhoneNumberActivity var0, TextView var1, int var2, KeyEvent var3) {
      boolean var4;
      if(var2 == 4) {
         var0.c.a((Object)co.uk.getmondo.common.b.a.a);
         var4 = true;
      } else {
         var4 = false;
      }

      return var4;
   }

   // $FF: synthetic method
   static String b(EnterPhoneNumberActivity var0, Object var1) throws Exception {
      return var0.phoneNumberInput.getText().toString();
   }

   // $FF: synthetic method
   static boolean c(EnterPhoneNumberActivity var0, Object var1) throws Exception {
      return co.uk.getmondo.common.k.k.a(var0);
   }

   public io.reactivex.n a() {
      return io.reactivex.n.merge(this.b, this.c).filter(t.a(this)).mergeWith(this.e).map(u.a(this));
   }

   public void a(String var1) {
      this.phoneNumberInput.setText(var1);
   }

   public io.reactivex.n b() {
      return io.reactivex.n.merge(this.b, this.c).map(v.a(this));
   }

   public void c() {
      android.support.v4.app.a.a(this, new String[]{"android.permission.RECEIVE_SMS", "android.permission.READ_SMS"}, 1001);
   }

   public void d() {
      EnterCodeActivity.a((Context)this);
   }

   public void e() {
      this.t();
   }

   public void f() {
      this.s();
   }

   @SuppressLint({"SetTextI18n"})
   protected void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.setContentView(2131034168);
      ButterKnife.bind((Activity)this);
      this.l().a(this);
      this.b = com.b.a.c.c.a(this.textCodeButton).share();
      this.phoneNumberInput.setOnEditorActionListener(s.a(this));
      this.a.a((x.a)this);
   }

   protected void onDestroy() {
      super.onDestroy();
      this.a.b();
   }

   public void onRequestPermissionsResult(int var1, String[] var2, int[] var3) {
      if(var1 == 1001) {
         this.e.a((Object)co.uk.getmondo.common.b.a.a);
      } else {
         super.onRequestPermissionsResult(var1, var2, var3);
      }

   }
}
