package co.uk.getmondo.create_account.phone_number;

import android.view.KeyEvent;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

// $FF: synthetic class
final class s implements OnEditorActionListener {
   private final EnterPhoneNumberActivity a;

   private s(EnterPhoneNumberActivity var1) {
      this.a = var1;
   }

   public static OnEditorActionListener a(EnterPhoneNumberActivity var0) {
      return new s(var0);
   }

   public boolean onEditorAction(TextView var1, int var2, KeyEvent var3) {
      return EnterPhoneNumberActivity.a(this.a, var1, var2, var3);
   }
}
