package co.uk.getmondo.create_account.phone_number;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class EnterCodeActivity_ViewBinding implements Unbinder {
   private EnterCodeActivity a;

   public EnterCodeActivity_ViewBinding(EnterCodeActivity var1, View var2) {
      this.a = var1;
      var1.verificationCodeInput = (EditText)Utils.findRequiredViewAsType(var2, 2131820903, "field 'verificationCodeInput'", EditText.class);
      var1.submitCodeButton = (Button)Utils.findRequiredViewAsType(var2, 2131820904, "field 'submitCodeButton'", Button.class);
   }

   public void unbind() {
      EnterCodeActivity var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.verificationCodeInput = null;
         var1.submitCodeButton = null;
      }
   }
}
