package co.uk.getmondo.create_account.phone_number;

import co.uk.getmondo.api.model.ApiCodeCheckResult;

// $FF: synthetic class
final class q implements io.reactivex.c.b {
   private final k.a a;

   private q(k.a var1) {
      this.a = var1;
   }

   public static io.reactivex.c.b a(k.a var0) {
      return new q(var0);
   }

   public void a(Object var1, Object var2) {
      k.a(this.a, (ApiCodeCheckResult)var1, (Throwable)var2);
   }
}
