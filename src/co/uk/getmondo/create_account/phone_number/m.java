package co.uk.getmondo.create_account.phone_number;

import co.uk.getmondo.api.model.ApiCodeCheckResult;

// $FF: synthetic class
final class m implements io.reactivex.c.g {
   private final k.a a;

   private m(k.a var1) {
      this.a = var1;
   }

   public static io.reactivex.c.g a(k.a var0) {
      return new m(var0);
   }

   public void a(Object var1) {
      k.a(this.a, (ApiCodeCheckResult)var1);
   }
}
