package co.uk.getmondo.create_account.phone_number;

import android.view.KeyEvent;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

// $FF: synthetic class
final class h implements OnEditorActionListener {
   private final io.reactivex.o a;

   private h(io.reactivex.o var1) {
      this.a = var1;
   }

   public static OnEditorActionListener a(io.reactivex.o var0) {
      return new h(var0);
   }

   public boolean onEditorAction(TextView var1, int var2, KeyEvent var3) {
      return EnterCodeActivity.a(this.a, var1, var2, var3);
   }
}
