package co.uk.getmondo.create_account.phone_number;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.uk.getmondo.create_account.VerifyIdentityActivity;

public class EnterCodeActivity extends co.uk.getmondo.common.activities.b implements k.a {
   k a;
   @BindView(2131820904)
   Button submitCodeButton;
   @BindView(2131820903)
   EditText verificationCodeInput;

   // $FF: synthetic method
   static String a(EnterCodeActivity var0, Object var1) throws Exception {
      return var0.verificationCodeInput.getText().toString().trim();
   }

   public static void a(Context var0) {
      var0.startActivity(new Intent(var0, EnterCodeActivity.class));
   }

   // $FF: synthetic method
   static void a(EnterCodeActivity var0) throws Exception {
      var0.verificationCodeInput.setOnEditorActionListener((OnEditorActionListener)null);
   }

   // $FF: synthetic method
   static void a(EnterCodeActivity var0, ag var1) throws Exception {
      ag.a((Context)var0, (ag)var1);
   }

   // $FF: synthetic method
   static void a(EnterCodeActivity var0, io.reactivex.o var1) throws Exception {
      ag var3 = new ag();
      var1.getClass();
      a var2 = f.a(var1);
      ag.a(var0, var3, "\\d{4,8}", new String[]{"Monzo"}, var2);
      var1.a(g.a(var0, var3));
   }

   // $FF: synthetic method
   static void a(EnterCodeActivity var0, String var1) throws Exception {
      var0.verificationCodeInput.setText(var1);
   }

   // $FF: synthetic method
   static boolean a(io.reactivex.o var0, TextView var1, int var2, KeyEvent var3) {
      boolean var4;
      if(var2 == 4) {
         var0.a(co.uk.getmondo.common.b.a.a);
         var4 = true;
      } else {
         var4 = false;
      }

      return var4;
   }

   // $FF: synthetic method
   static void b(EnterCodeActivity var0, io.reactivex.o var1) throws Exception {
      var0.verificationCodeInput.setOnEditorActionListener(h.a(var1));
      var1.a(i.a(var0));
   }

   private io.reactivex.n c() {
      return io.reactivex.n.create(d.a(this));
   }

   private io.reactivex.n d() {
      return io.reactivex.n.create(e.a(this));
   }

   public void a() {
      this.startActivity(VerifyIdentityActivity.a((Context)this));
      this.finish();
   }

   public void a(boolean var1) {
      this.submitCodeButton.setEnabled(var1);
   }

   public io.reactivex.n b() {
      io.reactivex.n var2 = com.b.a.c.c.a(this.submitCodeButton).mergeWith(this.c());
      io.reactivex.n var1;
      if(co.uk.getmondo.common.k.k.a(this)) {
         var1 = this.d().doOnNext(b.a(this));
      } else {
         var1 = io.reactivex.n.empty();
      }

      return var2.mergeWith(var1).map(c.a(this));
   }

   protected void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.setContentView(2131034167);
      ButterKnife.bind((Activity)this);
      this.l().a(this);
      this.a.a((k.a)this);
   }

   protected void onDestroy() {
      this.a.b();
      super.onDestroy();
   }
}
