package co.uk.getmondo.create_account.phone_number;

// $FF: synthetic class
final class t implements io.reactivex.c.q {
   private final EnterPhoneNumberActivity a;

   private t(EnterPhoneNumberActivity var1) {
      this.a = var1;
   }

   public static io.reactivex.c.q a(EnterPhoneNumberActivity var0) {
      return new t(var0);
   }

   public boolean a(Object var1) {
      return EnterPhoneNumberActivity.c(this.a, var1);
   }
}
