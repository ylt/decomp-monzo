package co.uk.getmondo.create_account.phone_number;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class EnterPhoneNumberActivity_ViewBinding implements Unbinder {
   private EnterPhoneNumberActivity a;

   public EnterPhoneNumberActivity_ViewBinding(EnterPhoneNumberActivity var1, View var2) {
      this.a = var1;
      var1.phoneNumberInput = (EditText)Utils.findRequiredViewAsType(var2, 2131820905, "field 'phoneNumberInput'", EditText.class);
      var1.textCodeButton = (Button)Utils.findRequiredViewAsType(var2, 2131820906, "field 'textCodeButton'", Button.class);
   }

   public void unbind() {
      EnterPhoneNumberActivity var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.phoneNumberInput = null;
         var1.textCodeButton = null;
      }
   }
}
