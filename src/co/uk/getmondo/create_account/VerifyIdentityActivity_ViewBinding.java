package co.uk.getmondo.create_account;

import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.EditText;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;

public class VerifyIdentityActivity_ViewBinding implements Unbinder {
   private VerifyIdentityActivity a;
   private View b;
   private View c;
   private View d;
   private View e;

   public VerifyIdentityActivity_ViewBinding(final VerifyIdentityActivity var1, View var2) {
      this.a = var1;
      View var3 = Utils.findRequiredView(var2, 2131820866, "field 'nameInput' and method 'onNameFocusChange'");
      var1.nameInput = (EditText)Utils.castView(var3, 2131820866, "field 'nameInput'", EditText.class);
      this.b = var3;
      var3.setOnFocusChangeListener(new OnFocusChangeListener() {
         public void onFocusChange(View var1x, boolean var2) {
            var1.onNameFocusChange(var2);
         }
      });
      var1.nameWrapper = (TextInputLayout)Utils.findRequiredViewAsType(var2, 2131820865, "field 'nameWrapper'", TextInputLayout.class);
      var3 = Utils.findRequiredView(var2, 2131821164, "field 'dobInput', method 'onDateFocusChange', and method 'onDateOfBirthFocusChange'");
      var1.dobInput = (EditText)Utils.castView(var3, 2131821164, "field 'dobInput'", EditText.class);
      this.c = var3;
      var3.setOnFocusChangeListener(new OnFocusChangeListener() {
         public void onFocusChange(View var1x, boolean var2) {
            var1.onDateFocusChange(var2);
            var1.onDateOfBirthFocusChange(var2);
         }
      });
      var1.dobWrapper = (TextInputLayout)Utils.findRequiredViewAsType(var2, 2131821163, "field 'dobWrapper'", TextInputLayout.class);
      var3 = Utils.findRequiredView(var2, 2131821165, "field 'addressInput' and method 'onAddressFocusChange'");
      var1.addressInput = (EditText)Utils.castView(var3, 2131821165, "field 'addressInput'", EditText.class);
      this.d = var3;
      var3.setOnFocusChangeListener(new OnFocusChangeListener() {
         public void onFocusChange(View var1x, boolean var2) {
            var1.onAddressFocusChange(var2);
         }
      });
      var2 = Utils.findRequiredView(var2, 2131821166, "method 'onDetailsCorrectClick'");
      this.e = var2;
      var2.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View var1x) {
            var1.onDetailsCorrectClick();
         }
      });
   }

   public void unbind() {
      VerifyIdentityActivity var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.nameInput = null;
         var1.nameWrapper = null;
         var1.dobInput = null;
         var1.dobWrapper = null;
         var1.addressInput = null;
         this.b.setOnFocusChangeListener((OnFocusChangeListener)null);
         this.b = null;
         this.c.setOnFocusChangeListener((OnFocusChangeListener)null);
         this.c = null;
         this.d.setOnFocusChangeListener((OnFocusChangeListener)null);
         this.d = null;
         this.e.setOnClickListener((OnClickListener)null);
         this.e = null;
      }
   }
}
