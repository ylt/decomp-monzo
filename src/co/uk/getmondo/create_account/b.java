package co.uk.getmondo.create_account;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class b {
   private static final SimpleDateFormat a;
   private static final SimpleDateFormat b;

   static {
      a = new SimpleDateFormat("dd / MM / yyyy", Locale.UK);
      b = new SimpleDateFormat("yyyy-MM-dd", Locale.UK);
   }

   public static Date a(String var0) throws ParseException {
      return a.parse(var0);
   }

   public static boolean a(Date var0, Date var1) {
      boolean var2 = true;
      Calendar var3 = Calendar.getInstance();
      var3.setTime(var1);
      var3.add(1, -18);
      if(var3.getTimeInMillis() < var0.getTime()) {
         var2 = false;
      }

      return var2;
   }

   public static String b(String var0) {
      try {
         var0 = a.format(b.parse(var0));
      } catch (ParseException var1) {
         var0 = null;
      }

      return var0;
   }

   public static boolean b(Date var0, Date var1) {
      boolean var2 = true;
      Calendar var3 = Calendar.getInstance();
      var3.setTime(var1);
      var3.add(1, -150);
      if(var3.getTimeInMillis() <= var0.getTime()) {
         var2 = false;
      }

      return var2;
   }

   public static String c(String var0) {
      try {
         var0 = b.format(a.parse(var0));
      } catch (ParseException var1) {
         var0 = null;
      }

      return var0;
   }
}
