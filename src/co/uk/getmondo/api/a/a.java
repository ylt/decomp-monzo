package co.uk.getmondo.api.a;

import co.uk.getmondo.common.k.p;
import co.uk.getmondo.create_account.b;
import java.util.HashMap;
import java.util.Map;

public class a {
   private final String a;
   private final String b;
   private final String c;
   private final String d;
   private final String e;

   public a(String var1, String var2, String var3, String var4, String var5) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
      this.d = var4;
      this.e = var5;
   }

   public Map a() {
      HashMap var1 = new HashMap();
      var1.put("email", this.a);
      var1.put("name", this.b);
      var1.put("country", this.c);
      var1.put("date_of_birth", b.c(this.d));
      if(p.c(this.e)) {
         var1.put("postal_code", this.e);
      }

      var1.put("current_account", Boolean.TRUE);
      return var1;
   }
}
