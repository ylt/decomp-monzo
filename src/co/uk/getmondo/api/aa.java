package co.uk.getmondo.api;

public final class aa implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final y b;

   static {
      boolean var0;
      if(!aa.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   public aa(y var1) {
      if(!a && var1 == null) {
         throw new AssertionError();
      } else {
         this.b = var1;
      }
   }

   public static b.a.b a(y var0) {
      return new aa(var0);
   }

   public String a() {
      return (String)b.a.d.a(this.b.a(), "Cannot return null from a non-@Nullable @Provides method");
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
