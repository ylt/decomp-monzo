package co.uk.getmondo.api.model;

public class b {
   private String code;
   private String message;

   public b(String var1, String var2) {
      this.code = var1;
      this.message = var2;
   }

   public String a() {
      return this.code;
   }

   public String b() {
      return this.message;
   }

   public boolean c() {
      return "bad_request.app_version_outdated".equals(this.code);
   }

   public String toString() {
      return "ApiError{code=" + this.code + ", message=" + this.message + "}";
   }
}
