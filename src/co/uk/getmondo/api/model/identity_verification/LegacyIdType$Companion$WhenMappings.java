package co.uk.getmondo.api.model.identity_verification;

import kotlin.Metadata;

// $FF: synthetic class
@Metadata(
   bv = {1, 0, 2},
   k = 3,
   mv = {1, 1, 7}
)
public final class LegacyIdType$Companion$WhenMappings {
   // $FF: synthetic field
   public static final int[] $EnumSwitchMapping$0 = new int[IdentityDocumentType.values().length];

   static {
      $EnumSwitchMapping$0[IdentityDocumentType.PASSPORT.ordinal()] = 1;
      $EnumSwitchMapping$0[IdentityDocumentType.DRIVING_LICENSE.ordinal()] = 2;
      $EnumSwitchMapping$0[IdentityDocumentType.NATIONAL_ID.ordinal()] = 3;
   }
}
