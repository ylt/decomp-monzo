package co.uk.getmondo.api.model.identity_verification;

import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u000e\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001:\u0001\u0019B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005¢\u0006\u0002\u0010\u0007J\t\u0010\r\u001a\u00020\u0003HÆ\u0003J\t\u0010\u000e\u001a\u00020\u0005HÆ\u0003J\t\u0010\u000f\u001a\u00020\u0005HÆ\u0003J'\u0010\u0010\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u0011\u001a\u00020\u00052\b\u0010\u0012\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0013\u001a\u00020\u0014HÖ\u0001J\u0006\u0010\u0015\u001a\u00020\u0016J\t\u0010\u0017\u001a\u00020\u0018HÖ\u0001R\u0011\u0010\u0006\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\f¨\u0006\u001a"},
   d2 = {"Lco/uk/getmondo/api/model/identity_verification/LegacyKyc;", "", "status", "Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;", "evidenceSubmitted", "", "evidenceReviewed", "(Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;ZZ)V", "getEvidenceReviewed", "()Z", "getEvidenceSubmitted", "getStatus", "()Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;", "component1", "component2", "component3", "copy", "equals", "other", "hashCode", "", "toIdentityVerification", "Lco/uk/getmondo/api/model/identity_verification/IdentityVerification;", "toString", "", "Status", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class LegacyKyc {
   private final boolean evidenceReviewed;
   private final boolean evidenceSubmitted;
   private final LegacyKyc.Status status;

   public LegacyKyc(LegacyKyc.Status var1, boolean var2, boolean var3) {
      l.b(var1, "status");
      super();
      this.status = var1;
      this.evidenceSubmitted = var2;
      this.evidenceReviewed = var3;
   }

   public final IdentityVerification a() {
      return new IdentityVerification((String)null, (String)null, this.status.a(), this.evidenceSubmitted, this.evidenceSubmitted, false, false, 99, (i)null);
   }

   public boolean equals(Object var1) {
      boolean var4 = false;
      boolean var3;
      if(this != var1) {
         var3 = var4;
         if(!(var1 instanceof LegacyKyc)) {
            return var3;
         }

         LegacyKyc var5 = (LegacyKyc)var1;
         var3 = var4;
         if(!l.a(this.status, var5.status)) {
            return var3;
         }

         boolean var2;
         if(this.evidenceSubmitted == var5.evidenceSubmitted) {
            var2 = true;
         } else {
            var2 = false;
         }

         var3 = var4;
         if(!var2) {
            return var3;
         }

         if(this.evidenceReviewed == var5.evidenceReviewed) {
            var2 = true;
         } else {
            var2 = false;
         }

         var3 = var4;
         if(!var2) {
            return var3;
         }
      }

      var3 = true;
      return var3;
   }

   public int hashCode() {
      throw new RuntimeException("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.copyTypes(TypeTransformer.java:311)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.fixTypes(TypeTransformer.java:226)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:207)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:161)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:433)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:129)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:528)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:425)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:441)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:171)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:271)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:109)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:290)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:33)\n");
   }

   public String toString() {
      return "LegacyKyc(status=" + this.status + ", evidenceSubmitted=" + this.evidenceSubmitted + ", evidenceReviewed=" + this.evidenceReviewed + ")";
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007j\u0002\b\bj\u0002\b\t¨\u0006\n"},
      d2 = {"Lco/uk/getmondo/api/model/identity_verification/LegacyKyc$Status;", "", "(Ljava/lang/String;I)V", "toIdentityVerificationStatus", "Lco/uk/getmondo/api/model/identity_verification/IdentityVerification$Status;", "KYC_UNKNOWN", "KYC_PASSED", "KYC_FAILED", "KYC_BLOCKED", "KYC_PASSED_ENHANCED", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static enum Status {
      KYC_BLOCKED,
      KYC_FAILED,
      KYC_PASSED,
      KYC_PASSED_ENHANCED,
      KYC_UNKNOWN;

      static {
         LegacyKyc.Status var3 = new LegacyKyc.Status("KYC_UNKNOWN", 0);
         KYC_UNKNOWN = var3;
         LegacyKyc.Status var2 = new LegacyKyc.Status("KYC_PASSED", 1);
         KYC_PASSED = var2;
         LegacyKyc.Status var0 = new LegacyKyc.Status("KYC_FAILED", 2);
         KYC_FAILED = var0;
         LegacyKyc.Status var4 = new LegacyKyc.Status("KYC_BLOCKED", 3);
         KYC_BLOCKED = var4;
         LegacyKyc.Status var1 = new LegacyKyc.Status("KYC_PASSED_ENHANCED", 4);
         KYC_PASSED_ENHANCED = var1;
      }

      public final IdentityVerification.Status a() {
         IdentityVerification.Status var1;
         switch(LegacyKyc$Status$WhenMappings.$EnumSwitchMapping$0[this.ordinal()]) {
         case 1:
         case 2:
            var1 = IdentityVerification.Status.APPROVED;
            break;
         case 3:
            var1 = IdentityVerification.Status.PENDING_SUBMISSION;
            break;
         case 4:
            var1 = IdentityVerification.Status.BLOCKED;
            break;
         default:
            var1 = null;
         }

         return var1;
      }
   }
}
