package co.uk.getmondo.api.model.identity_verification;

import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003¢\u0006\u0002\u0010\u0005J\t\u0010\t\u001a\u00020\u0003HÆ\u0003J\t\u0010\n\u001a\u00020\u0003HÆ\u0003J\u001d\u0010\u000b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\f\u001a\u00020\r2\b\u0010\u000e\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u000f\u001a\u00020\u0010HÖ\u0001J\t\u0010\u0011\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007R\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u0007¨\u0006\u0012"},
   d2 = {"Lco/uk/getmondo/api/model/identity_verification/ApiUploadContainer;", "", "fileUrl", "", "uploadUrl", "(Ljava/lang/String;Ljava/lang/String;)V", "getFileUrl", "()Ljava/lang/String;", "getUploadUrl", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ApiUploadContainer {
   private final String fileUrl;
   private final String uploadUrl;

   public ApiUploadContainer(String var1, String var2) {
      l.b(var1, "fileUrl");
      l.b(var2, "uploadUrl");
      super();
      this.fileUrl = var1;
      this.uploadUrl = var2;
   }

   public final String a() {
      return this.fileUrl;
   }

   public final String b() {
      return this.uploadUrl;
   }

   public boolean equals(Object var1) {
      boolean var2;
      if(this != var1) {
         label28: {
            if(var1 instanceof ApiUploadContainer) {
               ApiUploadContainer var3 = (ApiUploadContainer)var1;
               if(l.a(this.fileUrl, var3.fileUrl) && l.a(this.uploadUrl, var3.uploadUrl)) {
                  break label28;
               }
            }

            var2 = false;
            return var2;
         }
      }

      var2 = true;
      return var2;
   }

   public int hashCode() {
      int var2 = 0;
      String var3 = this.fileUrl;
      int var1;
      if(var3 != null) {
         var1 = var3.hashCode();
      } else {
         var1 = 0;
      }

      var3 = this.uploadUrl;
      if(var3 != null) {
         var2 = var3.hashCode();
      }

      return var1 * 31 + var2;
   }

   public String toString() {
      return "ApiUploadContainer(fileUrl=" + this.fileUrl + ", uploadUrl=" + this.uploadUrl + ")";
   }
}
