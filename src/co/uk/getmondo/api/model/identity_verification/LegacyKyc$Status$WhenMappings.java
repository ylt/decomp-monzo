package co.uk.getmondo.api.model.identity_verification;

import kotlin.Metadata;

// $FF: synthetic class
@Metadata(
   bv = {1, 0, 2},
   k = 3,
   mv = {1, 1, 7}
)
public final class LegacyKyc$Status$WhenMappings {
   // $FF: synthetic field
   public static final int[] $EnumSwitchMapping$0 = new int[LegacyKyc.Status.values().length];

   static {
      $EnumSwitchMapping$0[LegacyKyc.Status.KYC_PASSED.ordinal()] = 1;
      $EnumSwitchMapping$0[LegacyKyc.Status.KYC_PASSED_ENHANCED.ordinal()] = 2;
      $EnumSwitchMapping$0[LegacyKyc.Status.KYC_FAILED.ordinal()] = 3;
      $EnumSwitchMapping$0[LegacyKyc.Status.KYC_BLOCKED.ordinal()] = 4;
   }
}
