package co.uk.getmondo.api.model;

import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\b\u0086\b\u0018\u00002\u00020\u0001:\u0001\u001aB%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0006\u0010\u0007\u001a\u00020\u0003¢\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0010\u001a\u00020\u0005HÆ\u0003J\t\u0010\u0011\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0012\u001a\u00020\u0003HÆ\u0003J1\u0010\u0013\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00032\b\b\u0002\u0010\u0007\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\u0014\u001a\u00020\u00152\b\u0010\u0016\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0017\u001a\u00020\u0018HÖ\u0001J\t\u0010\u0019\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0007\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\nR\u0011\u0010\u0006\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\n¨\u0006\u001b"},
   d2 = {"Lco/uk/getmondo/api/model/ApiGoldenTicket;", "", "ticketId", "", "status", "Lco/uk/getmondo/api/model/ApiGoldenTicket$GoldenTicketStatus;", "uri", "shortUri", "(Ljava/lang/String;Lco/uk/getmondo/api/model/ApiGoldenTicket$GoldenTicketStatus;Ljava/lang/String;Ljava/lang/String;)V", "getShortUri", "()Ljava/lang/String;", "getStatus", "()Lco/uk/getmondo/api/model/ApiGoldenTicket$GoldenTicketStatus;", "getTicketId", "getUri", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "hashCode", "", "toString", "GoldenTicketStatus", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ApiGoldenTicket {
   private final String shortUri;
   private final ApiGoldenTicket.GoldenTicketStatus status;
   private final String ticketId;
   private final String uri;

   public ApiGoldenTicket(String var1, ApiGoldenTicket.GoldenTicketStatus var2, String var3, String var4) {
      l.b(var1, "ticketId");
      l.b(var2, "status");
      l.b(var3, "uri");
      l.b(var4, "shortUri");
      super();
      this.ticketId = var1;
      this.status = var2;
      this.uri = var3;
      this.shortUri = var4;
   }

   public final ApiGoldenTicket.GoldenTicketStatus a() {
      return this.status;
   }

   public final String b() {
      return this.shortUri;
   }

   public boolean equals(Object var1) {
      boolean var2;
      if(this != var1) {
         label32: {
            if(var1 instanceof ApiGoldenTicket) {
               ApiGoldenTicket var3 = (ApiGoldenTicket)var1;
               if(l.a(this.ticketId, var3.ticketId) && l.a(this.status, var3.status) && l.a(this.uri, var3.uri) && l.a(this.shortUri, var3.shortUri)) {
                  break label32;
               }
            }

            var2 = false;
            return var2;
         }
      }

      var2 = true;
      return var2;
   }

   public int hashCode() {
      int var4 = 0;
      String var5 = this.ticketId;
      int var1;
      if(var5 != null) {
         var1 = var5.hashCode();
      } else {
         var1 = 0;
      }

      ApiGoldenTicket.GoldenTicketStatus var6 = this.status;
      int var2;
      if(var6 != null) {
         var2 = var6.hashCode();
      } else {
         var2 = 0;
      }

      var5 = this.uri;
      int var3;
      if(var5 != null) {
         var3 = var5.hashCode();
      } else {
         var3 = 0;
      }

      var5 = this.shortUri;
      if(var5 != null) {
         var4 = var5.hashCode();
      }

      return (var3 + (var2 + var1 * 31) * 31) * 31 + var4;
   }

   public String toString() {
      return "ApiGoldenTicket(ticketId=" + this.ticketId + ", status=" + this.status + ", uri=" + this.uri + ", shortUri=" + this.shortUri + ")";
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0007\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\u0007j\u0002\b\bj\u0002\b\t¨\u0006\n"},
      d2 = {"Lco/uk/getmondo/api/model/ApiGoldenTicket$GoldenTicketStatus;", "", "apiValue", "", "(Ljava/lang/String;ILjava/lang/String;)V", "getApiValue", "()Ljava/lang/String;", "ACTIVE", "INACTIVE", "CLAIMED", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static enum GoldenTicketStatus {
      ACTIVE,
      CLAIMED,
      INACTIVE;

      private final String apiValue;

      static {
         ApiGoldenTicket.GoldenTicketStatus var2 = new ApiGoldenTicket.GoldenTicketStatus("ACTIVE", 0, "ACTIVE");
         ACTIVE = var2;
         ApiGoldenTicket.GoldenTicketStatus var1 = new ApiGoldenTicket.GoldenTicketStatus("INACTIVE", 1, "INACTIVE");
         INACTIVE = var1;
         ApiGoldenTicket.GoldenTicketStatus var0 = new ApiGoldenTicket.GoldenTicketStatus("CLAIMED", 2, "CLAIMED");
         CLAIMED = var0;
      }

      protected GoldenTicketStatus(String var3) {
         l.b(var3, "apiValue");
         super(var1, var2);
         this.apiValue = var3;
      }
   }
}
