package co.uk.getmondo.api.model;

import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.l;
import org.threeten.bp.LocalDateTime;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0015\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001BA\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\nJ\t\u0010\u0013\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0014\u001a\u00020\u0005HÆ\u0003J\t\u0010\u0015\u001a\u00020\u0003HÆ\u0003J\u000b\u0010\u0016\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u0017\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u0018\u001a\u0004\u0018\u00010\u0003HÆ\u0003JK\u0010\u0019\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00032\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u0003HÆ\u0001J\u0013\u0010\u001a\u001a\u00020\u001b2\b\u0010\u001c\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u001d\u001a\u00020\u001eHÖ\u0001J\t\u0010\u001f\u001a\u00020\u0003HÖ\u0001R\u0013\u0010\b\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0011\u0010\u0006\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\fR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\fR\u0013\u0010\u0007\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\fR\u0013\u0010\t\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\f¨\u0006 "},
   d2 = {"Lco/uk/getmondo/api/model/ApiAccount;", "", "id", "", "created", "Lorg/threeten/bp/LocalDateTime;", "description", "sortCode", "accountNumber", "type", "(Ljava/lang/String;Lorg/threeten/bp/LocalDateTime;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getAccountNumber", "()Ljava/lang/String;", "getCreated", "()Lorg/threeten/bp/LocalDateTime;", "getDescription", "getId", "getSortCode", "getType", "component1", "component2", "component3", "component4", "component5", "component6", "copy", "equals", "", "other", "hashCode", "", "toString", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ApiAccount {
   private final String accountNumber;
   private final LocalDateTime created;
   private final String description;
   private final String id;
   private final String sortCode;
   private final String type;

   public ApiAccount(String var1, LocalDateTime var2, String var3, String var4, String var5, String var6) {
      l.b(var1, "id");
      l.b(var2, "created");
      l.b(var3, "description");
      super();
      this.id = var1;
      this.created = var2;
      this.description = var3;
      this.sortCode = var4;
      this.accountNumber = var5;
      this.type = var6;
   }

   // $FF: synthetic method
   public ApiAccount(String var1, LocalDateTime var2, String var3, String var4, String var5, String var6, int var7, i var8) {
      if((var7 & 8) != 0) {
         var4 = (String)null;
      }

      if((var7 & 16) != 0) {
         var5 = (String)null;
      }

      if((var7 & 32) != 0) {
         var6 = (String)null;
      }

      this(var1, var2, var3, var4, var5, var6);
   }

   public final String a() {
      return this.id;
   }

   public final LocalDateTime b() {
      return this.created;
   }

   public final String c() {
      return this.description;
   }

   public final String d() {
      return this.sortCode;
   }

   public final String e() {
      return this.accountNumber;
   }

   public boolean equals(Object var1) {
      boolean var2;
      if(this != var1) {
         label36: {
            if(var1 instanceof ApiAccount) {
               ApiAccount var3 = (ApiAccount)var1;
               if(l.a(this.id, var3.id) && l.a(this.created, var3.created) && l.a(this.description, var3.description) && l.a(this.sortCode, var3.sortCode) && l.a(this.accountNumber, var3.accountNumber) && l.a(this.type, var3.type)) {
                  break label36;
               }
            }

            var2 = false;
            return var2;
         }
      }

      var2 = true;
      return var2;
   }

   public final String f() {
      return this.type;
   }

   public int hashCode() {
      int var6 = 0;
      String var7 = this.id;
      int var1;
      if(var7 != null) {
         var1 = var7.hashCode();
      } else {
         var1 = 0;
      }

      LocalDateTime var8 = this.created;
      int var2;
      if(var8 != null) {
         var2 = var8.hashCode();
      } else {
         var2 = 0;
      }

      var7 = this.description;
      int var3;
      if(var7 != null) {
         var3 = var7.hashCode();
      } else {
         var3 = 0;
      }

      var7 = this.sortCode;
      int var4;
      if(var7 != null) {
         var4 = var7.hashCode();
      } else {
         var4 = 0;
      }

      var7 = this.accountNumber;
      int var5;
      if(var7 != null) {
         var5 = var7.hashCode();
      } else {
         var5 = 0;
      }

      var7 = this.type;
      if(var7 != null) {
         var6 = var7.hashCode();
      }

      return (var5 + (var4 + (var3 + (var2 + var1 * 31) * 31) * 31) * 31) * 31 + var6;
   }

   public String toString() {
      return "ApiAccount(id=" + this.id + ", created=" + this.created + ", description=" + this.description + ", sortCode=" + this.sortCode + ", accountNumber=" + this.accountNumber + ", type=" + this.type + ")";
   }
}
