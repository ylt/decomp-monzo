package co.uk.getmondo.api.model;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.squareup.moshi.h;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010 \n\u0002\b\u0012\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0004\b\u0086\b\u0018\u0000 &2\u00020\u0001:\u0001&B\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004B?\u0012\n\b\u0003\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\u000e\b\u0001\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\b\u0012\b\b\u0001\u0010\t\u001a\u00020\u0006\u0012\b\b\u0001\u0010\n\u001a\u00020\u0006\u0012\b\b\u0001\u0010\u000b\u001a\u00020\u0006¢\u0006\u0002\u0010\fJ\u000b\u0010\u0014\u001a\u0004\u0018\u00010\u0006HÆ\u0003J\u000f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00060\bHÆ\u0003J\t\u0010\u0016\u001a\u00020\u0006HÆ\u0003J\t\u0010\u0017\u001a\u00020\u0006HÆ\u0003J\t\u0010\u0018\u001a\u00020\u0006HÆ\u0003JC\u0010\u0019\u001a\u00020\u00002\n\b\u0003\u0010\u0005\u001a\u0004\u0018\u00010\u00062\u000e\b\u0003\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\b2\b\b\u0003\u0010\t\u001a\u00020\u00062\b\b\u0003\u0010\n\u001a\u00020\u00062\b\b\u0003\u0010\u000b\u001a\u00020\u0006HÆ\u0001J\b\u0010\u001a\u001a\u00020\u001bH\u0016J\u0013\u0010\u001c\u001a\u00020\u001d2\b\u0010\u001e\u001a\u0004\u0018\u00010\u001fHÖ\u0003J\t\u0010 \u001a\u00020\u001bHÖ\u0001J\t\u0010!\u001a\u00020\u0006HÖ\u0001J\u0018\u0010\"\u001a\u00020#2\u0006\u0010$\u001a\u00020\u00032\u0006\u0010%\u001a\u00020\u001bH\u0016R\u0013\u0010\u0005\u001a\u0004\u0018\u00010\u0006¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u001a\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\bX\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0014\u0010\t\u001a\u00020\u0006X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u000eR\u0014\u0010\u000b\u001a\u00020\u0006X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u000eR\u0014\u0010\n\u001a\u00020\u0006X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u000e¨\u0006'"},
   d2 = {"Lco/uk/getmondo/api/model/ApiAddress;", "Lco/uk/getmondo/api/model/Address;", "source", "Landroid/os/Parcel;", "(Landroid/os/Parcel;)V", "addressId", "", "addressLines", "", "administrativeArea", "postalCode", "countryCode", "(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getAddressId", "()Ljava/lang/String;", "getAddressLines", "()Ljava/util/List;", "getAdministrativeArea", "getCountryCode", "getPostalCode", "component1", "component2", "component3", "component4", "component5", "copy", "describeContents", "", "equals", "", "other", "", "hashCode", "toString", "writeToParcel", "", "dest", "flags", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ApiAddress implements Address {
   public static final Creator CREATOR = (Creator)(new Creator() {
      public ApiAddress a(Parcel var1) {
         l.b(var1, "source");
         return new ApiAddress(var1);
      }

      public ApiAddress[] a(int var1) {
         return new ApiAddress[var1];
      }

      // $FF: synthetic method
      public Object createFromParcel(Parcel var1) {
         return this.a(var1);
      }

      // $FF: synthetic method
      public Object[] newArray(int var1) {
         return (Object[])this.a(var1);
      }
   });
   public static final ApiAddress.Companion Companion = new ApiAddress.Companion((i)null);
   private final String addressId;
   private final List addressLines;
   private final String administrativeArea;
   private final String countryCode;
   private final String postalCode;

   public ApiAddress(Parcel var1) {
      l.b(var1, "source");
      String var2 = var1.readString();
      ArrayList var3 = var1.createStringArrayList();
      l.a(var3, "source.createStringArrayList()");
      List var5 = (List)var3;
      String var4 = var1.readString();
      l.a(var4, "source.readString()");
      String var7 = var1.readString();
      l.a(var7, "source.readString()");
      String var6 = var1.readString();
      l.a(var6, "source.readString()");
      this(var2, var5, var4, var7, var6);
   }

   public ApiAddress(@h(a = "address_id") String var1, @h(a = "address_lines") List var2, @h(a = "administrative_area") String var3, @h(a = "postal_code") String var4, @h(a = "country_code") String var5) {
      l.b(var2, "addressLines");
      l.b(var3, "administrativeArea");
      l.b(var4, "postalCode");
      l.b(var5, "countryCode");
      super();
      this.addressId = var1;
      this.addressLines = var2;
      this.administrativeArea = var3;
      this.postalCode = var4;
      this.countryCode = var5;
   }

   // $FF: synthetic method
   public ApiAddress(String var1, List var2, String var3, String var4, String var5, int var6, i var7) {
      if((var6 & 1) != 0) {
         var1 = (String)null;
      }

      this(var1, var2, var3, var4, var5);
   }

   public List a() {
      return this.addressLines;
   }

   public String b() {
      return this.administrativeArea;
   }

   public String c() {
      return this.postalCode;
   }

   public String d() {
      return this.countryCode;
   }

   public int describeContents() {
      return 0;
   }

   public String e() {
      return Address.DefaultImpls.a(this);
   }

   public boolean equals(Object var1) {
      boolean var2;
      if(this != var1) {
         label34: {
            if(var1 instanceof ApiAddress) {
               ApiAddress var3 = (ApiAddress)var1;
               if(l.a(this.addressId, var3.addressId) && l.a(this.a(), var3.a()) && l.a(this.b(), var3.b()) && l.a(this.c(), var3.c()) && l.a(this.d(), var3.d())) {
                  break label34;
               }
            }

            var2 = false;
            return var2;
         }
      }

      var2 = true;
      return var2;
   }

   public final String f() {
      return this.addressId;
   }

   public int hashCode() {
      int var5 = 0;
      String var6 = this.addressId;
      int var1;
      if(var6 != null) {
         var1 = var6.hashCode();
      } else {
         var1 = 0;
      }

      List var7 = this.a();
      int var2;
      if(var7 != null) {
         var2 = var7.hashCode();
      } else {
         var2 = 0;
      }

      var6 = this.b();
      int var3;
      if(var6 != null) {
         var3 = var6.hashCode();
      } else {
         var3 = 0;
      }

      var6 = this.c();
      int var4;
      if(var6 != null) {
         var4 = var6.hashCode();
      } else {
         var4 = 0;
      }

      var6 = this.d();
      if(var6 != null) {
         var5 = var6.hashCode();
      }

      return (var4 + (var3 + (var2 + var1 * 31) * 31) * 31) * 31 + var5;
   }

   public String toString() {
      return "ApiAddress(addressId=" + this.addressId + ", addressLines=" + this.a() + ", administrativeArea=" + this.b() + ", postalCode=" + this.c() + ", countryCode=" + this.d() + ")";
   }

   public void writeToParcel(Parcel var1, int var2) {
      l.b(var1, "dest");
      var1.writeString(this.addressId);
      var1.writeStringList(this.a());
      var1.writeString(this.b());
      var1.writeString(this.c());
      var1.writeString(this.d());
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"},
      d2 = {"Lco/uk/getmondo/api/model/ApiAddress$Companion;", "", "()V", "CREATOR", "Landroid/os/Parcelable$Creator;", "Lco/uk/getmondo/api/model/ApiAddress;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class Companion {
      private Companion() {
      }

      // $FF: synthetic method
      public Companion(i var1) {
         this();
      }
   }
}
