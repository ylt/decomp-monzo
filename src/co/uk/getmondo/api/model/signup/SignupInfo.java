package co.uk.getmondo.api.model.signup;

import com.squareup.moshi.h;
import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\b\u0086\b\u0018\u00002\u00020\u0001:\u0002\u0015\u0016B\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003HÆ\u0003J\t\u0010\f\u001a\u00020\u0005HÆ\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0011\u001a\u00020\u0012HÖ\u0001J\t\u0010\u0013\u001a\u00020\u0014HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0017"},
   d2 = {"Lco/uk/getmondo/api/model/signup/SignupInfo;", "", "status", "Lco/uk/getmondo/api/model/signup/SignupInfo$Status;", "stage", "Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;", "(Lco/uk/getmondo/api/model/signup/SignupInfo$Status;Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;)V", "getStage", "()Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;", "getStatus", "()Lco/uk/getmondo/api/model/signup/SignupInfo$Status;", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "", "Stage", "Status", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class SignupInfo {
   private final SignupInfo.Stage stage;
   private final SignupInfo.Status status;

   public SignupInfo(SignupInfo.Status var1, SignupInfo.Stage var2) {
      l.b(var1, "status");
      l.b(var2, "stage");
      super();
      this.status = var1;
      this.stage = var2;
   }

   // $FF: synthetic method
   public SignupInfo(SignupInfo.Status var1, SignupInfo.Stage var2, int var3, i var4) {
      if((var3 & 2) != 0) {
         var2 = SignupInfo.Stage.NONE;
      }

      this(var1, var2);
   }

   public final SignupInfo.Status a() {
      return this.status;
   }

   public final SignupInfo a(SignupInfo.Status var1, SignupInfo.Stage var2) {
      l.b(var1, "status");
      l.b(var2, "stage");
      return new SignupInfo(var1, var2);
   }

   public final SignupInfo.Stage b() {
      return this.stage;
   }

   public boolean equals(Object var1) {
      boolean var2;
      if(this != var1) {
         label28: {
            if(var1 instanceof SignupInfo) {
               SignupInfo var3 = (SignupInfo)var1;
               if(l.a(this.status, var3.status) && l.a(this.stage, var3.stage)) {
                  break label28;
               }
            }

            var2 = false;
            return var2;
         }
      }

      var2 = true;
      return var2;
   }

   public int hashCode() {
      int var2 = 0;
      SignupInfo.Status var3 = this.status;
      int var1;
      if(var3 != null) {
         var1 = var3.hashCode();
      } else {
         var1 = 0;
      }

      SignupInfo.Stage var4 = this.stage;
      if(var4 != null) {
         var2 = var4.hashCode();
      }

      return var1 * 31 + var2;
   }

   public String toString() {
      return "SignupInfo(status=" + this.status + ", stage=" + this.stage + ")";
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0010\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007j\u0002\b\bj\u0002\b\tj\u0002\b\nj\u0002\b\u000bj\u0002\b\fj\u0002\b\rj\u0002\b\u000ej\u0002\b\u000fj\u0002\b\u0010¨\u0006\u0011"},
      d2 = {"Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;", "", "(Ljava/lang/String;I)V", "PROFILE_DETAILS", "PHONE_VERIFICATION", "MARKETING_OPT_IN", "LEGAL_DOCUMENTS", "IDENTITY_VERIFICATION", "TAX_RESIDENCY", "CARD_ORDER", "DEVICE_AUTHENTICATION_ENROLMENT", "CARD_ACTIVATION", "PENDING", "DONE", "NONE", "WAIT_LIST_SIGNUP", "WAIT_LIST", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static enum Stage {
      @h(
         a = "card-activation"
      )
      CARD_ACTIVATION,
      @h(
         a = "card-order"
      )
      CARD_ORDER,
      @h(
         a = "device-authentication-enrolment"
      )
      DEVICE_AUTHENTICATION_ENROLMENT,
      @h(
         a = "done"
      )
      DONE,
      @h(
         a = "identity-verification"
      )
      IDENTITY_VERIFICATION,
      @h(
         a = "legal-documents"
      )
      LEGAL_DOCUMENTS,
      @h(
         a = "marketing-opt-in"
      )
      MARKETING_OPT_IN,
      @h(
         a = ""
      )
      NONE,
      @h(
         a = "pending"
      )
      PENDING,
      @h(
         a = "phone-verification"
      )
      PHONE_VERIFICATION,
      @h(
         a = "profile-details"
      )
      PROFILE_DETAILS,
      @h(
         a = "tax-residency"
      )
      TAX_RESIDENCY,
      WAIT_LIST,
      WAIT_LIST_SIGNUP;

      static {
         SignupInfo.Stage var5 = new SignupInfo.Stage("PROFILE_DETAILS", 0);
         PROFILE_DETAILS = var5;
         SignupInfo.Stage var8 = new SignupInfo.Stage("PHONE_VERIFICATION", 1);
         PHONE_VERIFICATION = var8;
         SignupInfo.Stage var10 = new SignupInfo.Stage("MARKETING_OPT_IN", 2);
         MARKETING_OPT_IN = var10;
         SignupInfo.Stage var2 = new SignupInfo.Stage("LEGAL_DOCUMENTS", 3);
         LEGAL_DOCUMENTS = var2;
         SignupInfo.Stage var0 = new SignupInfo.Stage("IDENTITY_VERIFICATION", 4);
         IDENTITY_VERIFICATION = var0;
         SignupInfo.Stage var7 = new SignupInfo.Stage("TAX_RESIDENCY", 5);
         TAX_RESIDENCY = var7;
         SignupInfo.Stage var1 = new SignupInfo.Stage("CARD_ORDER", 6);
         CARD_ORDER = var1;
         SignupInfo.Stage var11 = new SignupInfo.Stage("DEVICE_AUTHENTICATION_ENROLMENT", 7);
         DEVICE_AUTHENTICATION_ENROLMENT = var11;
         SignupInfo.Stage var13 = new SignupInfo.Stage("CARD_ACTIVATION", 8);
         CARD_ACTIVATION = var13;
         SignupInfo.Stage var4 = new SignupInfo.Stage("PENDING", 9);
         PENDING = var4;
         SignupInfo.Stage var6 = new SignupInfo.Stage("DONE", 10);
         DONE = var6;
         SignupInfo.Stage var12 = new SignupInfo.Stage("NONE", 11);
         NONE = var12;
         SignupInfo.Stage var3 = new SignupInfo.Stage("WAIT_LIST_SIGNUP", 12);
         WAIT_LIST_SIGNUP = var3;
         SignupInfo.Stage var9 = new SignupInfo.Stage("WAIT_LIST", 13);
         WAIT_LIST = var9;
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0007\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007¨\u0006\b"},
      d2 = {"Lco/uk/getmondo/api/model/signup/SignupInfo$Status;", "", "(Ljava/lang/String;I)V", "NOT_STARTED", "IN_PROGRESS", "APPROVED", "COMPLETED", "REJECTED", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static enum Status {
      @h(
         a = "approved"
      )
      APPROVED,
      @h(
         a = "completed"
      )
      COMPLETED,
      @h(
         a = "in-progress"
      )
      IN_PROGRESS,
      @h(
         a = "not-started"
      )
      NOT_STARTED,
      @h(
         a = "rejected"
      )
      REJECTED;

      static {
         SignupInfo.Status var4 = new SignupInfo.Status("NOT_STARTED", 0);
         NOT_STARTED = var4;
         SignupInfo.Status var0 = new SignupInfo.Status("IN_PROGRESS", 1);
         IN_PROGRESS = var0;
         SignupInfo.Status var2 = new SignupInfo.Status("APPROVED", 2);
         APPROVED = var2;
         SignupInfo.Status var3 = new SignupInfo.Status("COMPLETED", 3);
         COMPLETED = var3;
         SignupInfo.Status var1 = new SignupInfo.Status("REJECTED", 4);
         REJECTED = var1;
      }
   }
}
