package co.uk.getmondo.api.model.signup;

import com.squareup.moshi.h;
import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.l;
import org.threeten.bp.LocalDate;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0012\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B9\u0012\b\b\u0003\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0003\u0010\u0004\u001a\u00020\u0003\u0012\b\b\u0003\u0010\u0005\u001a\u00020\u0006\u0012\n\b\u0003\u0010\u0007\u001a\u0004\u0018\u00010\b\u0012\b\b\u0003\u0010\t\u001a\u00020\u0003¢\u0006\u0002\u0010\nJ\t\u0010\u0012\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0013\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0014\u001a\u00020\u0006HÆ\u0003J\u000b\u0010\u0015\u001a\u0004\u0018\u00010\bHÆ\u0003J\t\u0010\u0016\u001a\u00020\u0003HÆ\u0003J=\u0010\u0017\u001a\u00020\u00002\b\b\u0003\u0010\u0002\u001a\u00020\u00032\b\b\u0003\u0010\u0004\u001a\u00020\u00032\b\b\u0003\u0010\u0005\u001a\u00020\u00062\n\b\u0003\u0010\u0007\u001a\u0004\u0018\u00010\b2\b\b\u0003\u0010\t\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\u0018\u001a\u00020\u00062\b\u0010\u0019\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u001a\u001a\u00020\u001bHÖ\u0001J\t\u0010\u001c\u001a\u00020\u0003HÖ\u0001R\u0013\u0010\u0007\u001a\u0004\u0018\u00010\b¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\rR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0011\u0010\t\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u000fR\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u000f¨\u0006\u001d"},
   d2 = {"Lco/uk/getmondo/api/model/signup/SignUpProfile;", "", "legalName", "", "preferredName", "isPreferredNameDerived", "", "dateOfBirth", "Lorg/threeten/bp/LocalDate;", "phoneNumber", "(Ljava/lang/String;Ljava/lang/String;ZLorg/threeten/bp/LocalDate;Ljava/lang/String;)V", "getDateOfBirth", "()Lorg/threeten/bp/LocalDate;", "()Z", "getLegalName", "()Ljava/lang/String;", "getPhoneNumber", "getPreferredName", "component1", "component2", "component3", "component4", "component5", "copy", "equals", "other", "hashCode", "", "toString", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class SignUpProfile {
   private final LocalDate dateOfBirth;
   private final boolean isPreferredNameDerived;
   private final String legalName;
   private final String phoneNumber;
   private final String preferredName;

   public SignUpProfile() {
      this((String)null, (String)null, false, (LocalDate)null, (String)null, 31, (i)null);
   }

   public SignUpProfile(@h(a = "legal_name") String var1, @h(a = "preferred_name") String var2, @h(a = "is_preferred_name_derived") boolean var3, @h(a = "date_of_birth") LocalDate var4, @h(a = "phone_number") String var5) {
      l.b(var1, "legalName");
      l.b(var2, "preferredName");
      l.b(var5, "phoneNumber");
      super();
      this.legalName = var1;
      this.preferredName = var2;
      this.isPreferredNameDerived = var3;
      this.dateOfBirth = var4;
      this.phoneNumber = var5;
   }

   // $FF: synthetic method
   public SignUpProfile(String var1, String var2, boolean var3, LocalDate var4, String var5, int var6, i var7) {
      if((var6 & 1) != 0) {
         var1 = "";
      }

      if((var6 & 2) != 0) {
         var2 = "";
      }

      if((var6 & 4) != 0) {
         var3 = true;
      }

      if((var6 & 8) != 0) {
         var4 = (LocalDate)null;
      }

      if((var6 & 16) != 0) {
         var5 = "";
      }

      this(var1, var2, var3, var4, var5);
   }

   public final String a() {
      return this.legalName;
   }

   public final String b() {
      return this.preferredName;
   }

   public final boolean c() {
      return this.isPreferredNameDerived;
   }

   public final LocalDate d() {
      return this.dateOfBirth;
   }

   public boolean equals(Object var1) {
      boolean var4 = false;
      boolean var3;
      if(this != var1) {
         var3 = var4;
         if(!(var1 instanceof SignUpProfile)) {
            return var3;
         }

         SignUpProfile var5 = (SignUpProfile)var1;
         var3 = var4;
         if(!l.a(this.legalName, var5.legalName)) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.preferredName, var5.preferredName)) {
            return var3;
         }

         boolean var2;
         if(this.isPreferredNameDerived == var5.isPreferredNameDerived) {
            var2 = true;
         } else {
            var2 = false;
         }

         var3 = var4;
         if(!var2) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.dateOfBirth, var5.dateOfBirth)) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.phoneNumber, var5.phoneNumber)) {
            return var3;
         }
      }

      var3 = true;
      return var3;
   }

   public int hashCode() {
      throw new RuntimeException("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.copyTypes(TypeTransformer.java:311)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.fixTypes(TypeTransformer.java:226)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:207)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:161)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:433)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:129)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:528)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:425)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:441)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:171)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:271)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:109)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:290)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:33)\n");
   }

   public String toString() {
      return "SignUpProfile(legalName=" + this.legalName + ", preferredName=" + this.preferredName + ", isPreferredNameDerived=" + this.isPreferredNameDerived + ", dateOfBirth=" + this.dateOfBirth + ", phoneNumber=" + this.phoneNumber + ")";
   }
}
