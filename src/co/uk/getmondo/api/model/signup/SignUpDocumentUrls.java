package co.uk.getmondo.api.model.signup;

import com.squareup.moshi.h;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0015\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001BA\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0004\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0006\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0007\u001a\u00020\u0003\u0012\b\b\u0001\u0010\b\u001a\u00020\u0003¢\u0006\u0002\u0010\tJ\t\u0010\u0011\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0012\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0013\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0014\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0015\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0016\u001a\u00020\u0003HÆ\u0003JE\u0010\u0017\u001a\u00020\u00002\b\b\u0003\u0010\u0002\u001a\u00020\u00032\b\b\u0003\u0010\u0004\u001a\u00020\u00032\b\b\u0003\u0010\u0005\u001a\u00020\u00032\b\b\u0003\u0010\u0006\u001a\u00020\u00032\b\b\u0003\u0010\u0007\u001a\u00020\u00032\b\b\u0003\u0010\b\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\u0018\u001a\u00020\u00192\b\u0010\u001a\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u001b\u001a\u00020\u001cHÖ\u0001J\t\u0010\u001d\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0007\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\b\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\u000bR\u0011\u0010\u0005\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000bR\u0011\u0010\u0006\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u000bR\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u000b¨\u0006\u001e"},
   d2 = {"Lco/uk/getmondo/api/model/signup/SignUpDocumentUrls;", "", "termsAndConditionsUrl", "", "termsAndConditionsVersion", "privacyPolicyUrl", "privacyPolicyVersion", "fscsInformationSheetUrl", "fscsInformationSheetVersion", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getFscsInformationSheetUrl", "()Ljava/lang/String;", "getFscsInformationSheetVersion", "getPrivacyPolicyUrl", "getPrivacyPolicyVersion", "getTermsAndConditionsUrl", "getTermsAndConditionsVersion", "component1", "component2", "component3", "component4", "component5", "component6", "copy", "equals", "", "other", "hashCode", "", "toString", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class SignUpDocumentUrls {
   private final String fscsInformationSheetUrl;
   private final String fscsInformationSheetVersion;
   private final String privacyPolicyUrl;
   private final String privacyPolicyVersion;
   private final String termsAndConditionsUrl;
   private final String termsAndConditionsVersion;

   public SignUpDocumentUrls(@h(a = "terms_and_conditions_url") String var1, @h(a = "terms_and_conditions_version") String var2, @h(a = "privacy_policy_url") String var3, @h(a = "privacy_policy_version") String var4, @h(a = "fscs_information_sheet_url") String var5, @h(a = "fscs_information_sheet_version") String var6) {
      l.b(var1, "termsAndConditionsUrl");
      l.b(var2, "termsAndConditionsVersion");
      l.b(var3, "privacyPolicyUrl");
      l.b(var4, "privacyPolicyVersion");
      l.b(var5, "fscsInformationSheetUrl");
      l.b(var6, "fscsInformationSheetVersion");
      super();
      this.termsAndConditionsUrl = var1;
      this.termsAndConditionsVersion = var2;
      this.privacyPolicyUrl = var3;
      this.privacyPolicyVersion = var4;
      this.fscsInformationSheetUrl = var5;
      this.fscsInformationSheetVersion = var6;
   }

   public final String a() {
      return this.termsAndConditionsUrl;
   }

   public final String b() {
      return this.termsAndConditionsVersion;
   }

   public final String c() {
      return this.privacyPolicyUrl;
   }

   public final String d() {
      return this.privacyPolicyVersion;
   }

   public final String e() {
      return this.fscsInformationSheetUrl;
   }

   public boolean equals(Object var1) {
      boolean var2;
      if(this != var1) {
         label36: {
            if(var1 instanceof SignUpDocumentUrls) {
               SignUpDocumentUrls var3 = (SignUpDocumentUrls)var1;
               if(l.a(this.termsAndConditionsUrl, var3.termsAndConditionsUrl) && l.a(this.termsAndConditionsVersion, var3.termsAndConditionsVersion) && l.a(this.privacyPolicyUrl, var3.privacyPolicyUrl) && l.a(this.privacyPolicyVersion, var3.privacyPolicyVersion) && l.a(this.fscsInformationSheetUrl, var3.fscsInformationSheetUrl) && l.a(this.fscsInformationSheetVersion, var3.fscsInformationSheetVersion)) {
                  break label36;
               }
            }

            var2 = false;
            return var2;
         }
      }

      var2 = true;
      return var2;
   }

   public final String f() {
      return this.fscsInformationSheetVersion;
   }

   public int hashCode() {
      int var6 = 0;
      String var7 = this.termsAndConditionsUrl;
      int var1;
      if(var7 != null) {
         var1 = var7.hashCode();
      } else {
         var1 = 0;
      }

      var7 = this.termsAndConditionsVersion;
      int var2;
      if(var7 != null) {
         var2 = var7.hashCode();
      } else {
         var2 = 0;
      }

      var7 = this.privacyPolicyUrl;
      int var3;
      if(var7 != null) {
         var3 = var7.hashCode();
      } else {
         var3 = 0;
      }

      var7 = this.privacyPolicyVersion;
      int var4;
      if(var7 != null) {
         var4 = var7.hashCode();
      } else {
         var4 = 0;
      }

      var7 = this.fscsInformationSheetUrl;
      int var5;
      if(var7 != null) {
         var5 = var7.hashCode();
      } else {
         var5 = 0;
      }

      var7 = this.fscsInformationSheetVersion;
      if(var7 != null) {
         var6 = var7.hashCode();
      }

      return (var5 + (var4 + (var3 + (var2 + var1 * 31) * 31) * 31) * 31) * 31 + var6;
   }

   public String toString() {
      return "SignUpDocumentUrls(termsAndConditionsUrl=" + this.termsAndConditionsUrl + ", termsAndConditionsVersion=" + this.termsAndConditionsVersion + ", privacyPolicyUrl=" + this.privacyPolicyUrl + ", privacyPolicyVersion=" + this.privacyPolicyVersion + ", fscsInformationSheetUrl=" + this.fscsInformationSheetUrl + ", fscsInformationSheetVersion=" + this.fscsInformationSheetVersion + ")";
   }
}
