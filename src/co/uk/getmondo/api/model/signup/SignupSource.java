package co.uk.getmondo.api.model.signup;

import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\b\t\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\b\u0010\u0007\u001a\u00020\u0003H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\bj\u0002\b\tj\u0002\b\nj\u0002\b\u000b¨\u0006\f"},
   d2 = {"Lco/uk/getmondo/api/model/signup/SignupSource;", "", "apiValue", "", "(Ljava/lang/String;ILjava/lang/String;)V", "getApiValue", "()Ljava/lang/String;", "toString", "LEGACY_PREPAID", "LEGACY_PERSONAL_ACCOUNT", "PERSONAL_ACCOUNT", "SDD_MIGRATION", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public enum SignupSource {
   LEGACY_PERSONAL_ACCOUNT,
   LEGACY_PREPAID,
   PERSONAL_ACCOUNT,
   SDD_MIGRATION;

   private final String apiValue;

   static {
      SignupSource var2 = new SignupSource("LEGACY_PREPAID", 0, "legacy_prepaid_signup");
      LEGACY_PREPAID = var2;
      SignupSource var3 = new SignupSource("LEGACY_PERSONAL_ACCOUNT", 1, "legacy_personal_account_signup");
      LEGACY_PERSONAL_ACCOUNT = var3;
      SignupSource var1 = new SignupSource("PERSONAL_ACCOUNT", 2, "personal_account_signup");
      PERSONAL_ACCOUNT = var1;
      SignupSource var0 = new SignupSource("SDD_MIGRATION", 3, "sdd_migration");
      SDD_MIGRATION = var0;
   }

   protected SignupSource(String var3) {
      l.b(var3, "apiValue");
      super(var1, var2);
      this.apiValue = var3;
   }

   public final String a() {
      return this.apiValue;
   }

   public String toString() {
      return this.apiValue;
   }
}
