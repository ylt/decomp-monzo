package co.uk.getmondo.api.model.help;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.a.m;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\r\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0004\b\u0086\b\u0018\u0000 $2\u00020\u0001:\u0001$B\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004B/\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0006\u0012\b\b\u0002\u0010\b\u001a\u00020\u0006\u0012\u000e\b\u0002\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\n¢\u0006\u0002\u0010\fJ\t\u0010\u0013\u001a\u00020\u0006HÆ\u0003J\t\u0010\u0014\u001a\u00020\u0006HÆ\u0003J\t\u0010\u0015\u001a\u00020\u0006HÆ\u0003J\u000f\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u000b0\nHÆ\u0003J7\u0010\u0017\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u00062\b\b\u0002\u0010\u0007\u001a\u00020\u00062\b\b\u0002\u0010\b\u001a\u00020\u00062\u000e\b\u0002\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\nHÆ\u0001J\b\u0010\u0018\u001a\u00020\u0019H\u0016J\u0013\u0010\u001a\u001a\u00020\u001b2\b\u0010\u001c\u001a\u0004\u0018\u00010\u001dHÖ\u0003J\t\u0010\u001e\u001a\u00020\u0019HÖ\u0001J\t\u0010\u001f\u001a\u00020\u0006HÖ\u0001J\u0018\u0010 \u001a\u00020!2\u0006\u0010\"\u001a\u00020\u00032\u0006\u0010#\u001a\u00020\u0019H\u0016R\u0017\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\n¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0011\u0010\b\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0010R\u0011\u0010\u0007\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0010¨\u0006%"},
   d2 = {"Lco/uk/getmondo/api/model/help/Topic;", "Landroid/os/Parcelable;", "source", "Landroid/os/Parcel;", "(Landroid/os/Parcel;)V", "id", "", "title", "content", "actions", "", "Lco/uk/getmondo/api/model/help/Action;", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V", "getActions", "()Ljava/util/List;", "getContent", "()Ljava/lang/String;", "getId", "getTitle", "component1", "component2", "component3", "component4", "copy", "describeContents", "", "equals", "", "other", "", "hashCode", "toString", "writeToParcel", "", "dest", "flags", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class Topic implements Parcelable {
   public static final Creator CREATOR = (Creator)(new Creator() {
      public Topic a(Parcel var1) {
         l.b(var1, "source");
         return new Topic(var1);
      }

      public Topic[] a(int var1) {
         return new Topic[var1];
      }

      // $FF: synthetic method
      public Object createFromParcel(Parcel var1) {
         return this.a(var1);
      }

      // $FF: synthetic method
      public Object[] newArray(int var1) {
         return (Object[])this.a(var1);
      }
   });
   public static final Topic.Companion Companion = new Topic.Companion((i)null);
   private final List actions;
   private final String content;
   private final String id;
   private final String title;

   public Topic(Parcel var1) {
      l.b(var1, "source");
      String var4 = var1.readString();
      l.a(var4, "source.readString()");
      String var3 = var1.readString();
      l.a(var3, "source.readString()");
      String var2 = var1.readString();
      l.a(var2, "source.readString()");
      ArrayList var5 = var1.createTypedArrayList(Action.CREATOR);
      l.a(var5, "source.createTypedArrayList(Action.CREATOR)");
      this(var4, var3, var2, (List)var5);
   }

   public Topic(String var1, String var2, String var3, List var4) {
      l.b(var1, "id");
      l.b(var2, "title");
      l.b(var3, "content");
      l.b(var4, "actions");
      super();
      this.id = var1;
      this.title = var2;
      this.content = var3;
      this.actions = var4;
   }

   // $FF: synthetic method
   public Topic(String var1, String var2, String var3, List var4, int var5, i var6) {
      if((var5 & 4) != 0) {
         var3 = "";
      }

      if((var5 & 8) != 0) {
         var4 = m.a();
      }

      this(var1, var2, var3, var4);
   }

   public final String a() {
      return this.id;
   }

   public final String b() {
      return this.title;
   }

   public final String c() {
      return this.content;
   }

   public int describeContents() {
      return 0;
   }

   public boolean equals(Object var1) {
      boolean var2;
      if(this != var1) {
         label32: {
            if(var1 instanceof Topic) {
               Topic var3 = (Topic)var1;
               if(l.a(this.id, var3.id) && l.a(this.title, var3.title) && l.a(this.content, var3.content) && l.a(this.actions, var3.actions)) {
                  break label32;
               }
            }

            var2 = false;
            return var2;
         }
      }

      var2 = true;
      return var2;
   }

   public int hashCode() {
      int var4 = 0;
      String var5 = this.id;
      int var1;
      if(var5 != null) {
         var1 = var5.hashCode();
      } else {
         var1 = 0;
      }

      var5 = this.title;
      int var2;
      if(var5 != null) {
         var2 = var5.hashCode();
      } else {
         var2 = 0;
      }

      var5 = this.content;
      int var3;
      if(var5 != null) {
         var3 = var5.hashCode();
      } else {
         var3 = 0;
      }

      List var6 = this.actions;
      if(var6 != null) {
         var4 = var6.hashCode();
      }

      return (var3 + (var2 + var1 * 31) * 31) * 31 + var4;
   }

   public String toString() {
      return "Topic(id=" + this.id + ", title=" + this.title + ", content=" + this.content + ", actions=" + this.actions + ")";
   }

   public void writeToParcel(Parcel var1, int var2) {
      l.b(var1, "dest");
      var1.writeString(this.id);
      var1.writeString(this.title);
      var1.writeString(this.content);
      var1.writeTypedList(this.actions);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"},
      d2 = {"Lco/uk/getmondo/api/model/help/Topic$Companion;", "", "()V", "CREATOR", "Landroid/os/Parcelable$Creator;", "Lco/uk/getmondo/api/model/help/Topic;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class Companion {
      private Companion() {
      }

      // $FF: synthetic method
      public Companion(i var1) {
         this();
      }
   }
}
