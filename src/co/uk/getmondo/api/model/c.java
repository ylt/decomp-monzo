package co.uk.getmondo.api.model;

import java.io.Serializable;

public class c implements Serializable {
   private int bump;
   private String id;
   private String text;
   private String type;

   public c(String var1, int var2) {
      this.text = var1;
      this.bump = var2;
   }

   public String a() {
      return this.id;
   }

   public String b() {
      return this.type;
   }

   public String c() {
      return this.text;
   }

   public int d() {
      return this.bump;
   }
}
