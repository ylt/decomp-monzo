package co.uk.getmondo.api.model.sign_up;

import co.uk.getmondo.api.model.signup.SignupInfo;
import com.squareup.moshi.h;
import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0015\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001BA\u0012\b\b\u0003\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0003\u0010\u0004\u001a\u00020\u0003\u0012\b\b\u0003\u0010\u0005\u001a\u00020\u0006\u0012\b\b\u0003\u0010\u0007\u001a\u00020\u0006\u0012\b\b\u0003\u0010\b\u001a\u00020\t\u0012\b\b\u0003\u0010\n\u001a\u00020\u000b¢\u0006\u0002\u0010\fJ\t\u0010\u0017\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0018\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0019\u001a\u00020\u0006HÆ\u0003J\t\u0010\u001a\u001a\u00020\u0006HÆ\u0003J\t\u0010\u001b\u001a\u00020\tHÆ\u0003J\t\u0010\u001c\u001a\u00020\u000bHÆ\u0003JE\u0010\u001d\u001a\u00020\u00002\b\b\u0003\u0010\u0002\u001a\u00020\u00032\b\b\u0003\u0010\u0004\u001a\u00020\u00032\b\b\u0003\u0010\u0005\u001a\u00020\u00062\b\b\u0003\u0010\u0007\u001a\u00020\u00062\b\b\u0003\u0010\b\u001a\u00020\t2\b\b\u0003\u0010\n\u001a\u00020\u000bHÆ\u0001J\u0013\u0010\u001e\u001a\u00020\u00032\b\u0010\u001f\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010 \u001a\u00020!HÖ\u0001J\t\u0010\"\u001a\u00020\u0006HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0011\u0010\u0007\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0010R\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u000eR\u0011\u0010\n\u001a\u00020\u000b¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u0011\u0010\b\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016¨\u0006#"},
   d2 = {"Lco/uk/getmondo/api/model/sign_up/MigrationInfo;", "", "bannerEnabled", "", "signupAllowed", "bannerTitle", "", "bannerSubtitle", "signupStatus", "Lco/uk/getmondo/api/model/signup/SignupInfo$Status;", "signupStage", "Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;", "(ZZLjava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/signup/SignupInfo$Status;Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;)V", "getBannerEnabled", "()Z", "getBannerSubtitle", "()Ljava/lang/String;", "getBannerTitle", "getSignupAllowed", "getSignupStage", "()Lco/uk/getmondo/api/model/signup/SignupInfo$Stage;", "getSignupStatus", "()Lco/uk/getmondo/api/model/signup/SignupInfo$Status;", "component1", "component2", "component3", "component4", "component5", "component6", "copy", "equals", "other", "hashCode", "", "toString", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class MigrationInfo {
   private final boolean bannerEnabled;
   private final String bannerSubtitle;
   private final String bannerTitle;
   private final boolean signupAllowed;
   private final SignupInfo.Stage signupStage;
   private final SignupInfo.Status signupStatus;

   public MigrationInfo() {
      this(false, false, (String)null, (String)null, (SignupInfo.Status)null, (SignupInfo.Stage)null, 63, (i)null);
   }

   public MigrationInfo(@h(a = "banner_enabled") boolean var1, @h(a = "signup_allowed") boolean var2, @h(a = "banner_title") String var3, @h(a = "banner_subtitle") String var4, @h(a = "signup_status") SignupInfo.Status var5, @h(a = "signup_stage") SignupInfo.Stage var6) {
      l.b(var3, "bannerTitle");
      l.b(var4, "bannerSubtitle");
      l.b(var5, "signupStatus");
      l.b(var6, "signupStage");
      super();
      this.bannerEnabled = var1;
      this.signupAllowed = var2;
      this.bannerTitle = var3;
      this.bannerSubtitle = var4;
      this.signupStatus = var5;
      this.signupStage = var6;
   }

   // $FF: synthetic method
   public MigrationInfo(boolean var1, boolean var2, String var3, String var4, SignupInfo.Status var5, SignupInfo.Stage var6, int var7, i var8) {
      boolean var9 = false;
      if((var7 & 1) != 0) {
         var1 = false;
      }

      if((var7 & 2) != 0) {
         var2 = var9;
      }

      if((var7 & 4) != 0) {
         var3 = "";
      }

      if((var7 & 8) != 0) {
         var4 = "";
      }

      if((var7 & 16) != 0) {
         var5 = SignupInfo.Status.NOT_STARTED;
      }

      if((var7 & 32) != 0) {
         var6 = SignupInfo.Stage.NONE;
      }

      this(var1, var2, var3, var4, var5, var6);
   }

   public final MigrationInfo a(@h(a = "banner_enabled") boolean var1, @h(a = "signup_allowed") boolean var2, @h(a = "banner_title") String var3, @h(a = "banner_subtitle") String var4, @h(a = "signup_status") SignupInfo.Status var5, @h(a = "signup_stage") SignupInfo.Stage var6) {
      l.b(var3, "bannerTitle");
      l.b(var4, "bannerSubtitle");
      l.b(var5, "signupStatus");
      l.b(var6, "signupStage");
      return new MigrationInfo(var1, var2, var3, var4, var5, var6);
   }

   public final boolean a() {
      return this.bannerEnabled;
   }

   public final boolean b() {
      return this.signupAllowed;
   }

   public final String c() {
      return this.bannerTitle;
   }

   public final String d() {
      return this.bannerSubtitle;
   }

   public final SignupInfo.Status e() {
      return this.signupStatus;
   }

   public boolean equals(Object var1) {
      boolean var4 = false;
      boolean var3;
      if(this != var1) {
         var3 = var4;
         if(!(var1 instanceof MigrationInfo)) {
            return var3;
         }

         MigrationInfo var5 = (MigrationInfo)var1;
         boolean var2;
         if(this.bannerEnabled == var5.bannerEnabled) {
            var2 = true;
         } else {
            var2 = false;
         }

         var3 = var4;
         if(!var2) {
            return var3;
         }

         if(this.signupAllowed == var5.signupAllowed) {
            var2 = true;
         } else {
            var2 = false;
         }

         var3 = var4;
         if(!var2) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.bannerTitle, var5.bannerTitle)) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.bannerSubtitle, var5.bannerSubtitle)) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.signupStatus, var5.signupStatus)) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.signupStage, var5.signupStage)) {
            return var3;
         }
      }

      var3 = true;
      return var3;
   }

   public final SignupInfo.Stage f() {
      return this.signupStage;
   }

   public int hashCode() {
      throw new RuntimeException("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.copyTypes(TypeTransformer.java:311)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.fixTypes(TypeTransformer.java:226)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:207)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:161)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:433)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:129)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:528)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:425)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:441)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:171)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:271)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:109)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:290)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:33)\n");
   }

   public String toString() {
      return "MigrationInfo(bannerEnabled=" + this.bannerEnabled + ", signupAllowed=" + this.signupAllowed + ", bannerTitle=" + this.bannerTitle + ", bannerSubtitle=" + this.bannerSubtitle + ", signupStatus=" + this.signupStatus + ", signupStage=" + this.signupStage + ")";
   }
}
