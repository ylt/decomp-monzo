package co.uk.getmondo.api.model.tracking;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\b\u0086\b\u0018\u0000 \u00162\u00020\u0001:\u0001\u0016B\u0019\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\r\u001a\u00020\u0003HÆ\u0003J\u000b\u0010\u000e\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\u001f\u0010\u000f\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005HÆ\u0001J\u0013\u0010\u0010\u001a\u00020\u00112\b\u0010\u0012\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0013\u001a\u00020\u0014HÖ\u0001J\t\u0010\u0015\u001a\u00020\u0003HÖ\u0001R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u000b\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\n¨\u0006\u0017"},
   d2 = {"Lco/uk/getmondo/api/model/tracking/ImpressionEvent;", "", "name", "", "data", "Lco/uk/getmondo/api/model/tracking/Data;", "(Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/Data;)V", "getData", "()Lco/uk/getmondo/api/model/tracking/Data;", "getName", "()Ljava/lang/String;", "timestamp", "getTimestamp", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ImpressionEvent {
   public static final ImpressionEvent.Companion Companion = new ImpressionEvent.Companion((i)null);
   private static final SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.getDefault());
   private final Data data;
   private final String name;
   private final String timestamp;

   public ImpressionEvent(String var1, Data var2) {
      l.b(var1, "name");
      super();
      this.name = var1;
      this.data = var2;
      var1 = Companion.a().format(new Date());
      l.a(var1, "ImpressionEvent.DATE_FORMATTER.format(Date())");
      this.timestamp = var1;
   }

   // $FF: synthetic method
   public ImpressionEvent(String var1, Data var2, int var3, i var4) {
      if((var3 & 2) != 0) {
         var2 = (Data)null;
      }

      this(var1, var2);
   }

   public boolean equals(Object var1) {
      boolean var2;
      if(this != var1) {
         label28: {
            if(var1 instanceof ImpressionEvent) {
               ImpressionEvent var3 = (ImpressionEvent)var1;
               if(l.a(this.name, var3.name) && l.a(this.data, var3.data)) {
                  break label28;
               }
            }

            var2 = false;
            return var2;
         }
      }

      var2 = true;
      return var2;
   }

   public int hashCode() {
      int var2 = 0;
      String var3 = this.name;
      int var1;
      if(var3 != null) {
         var1 = var3.hashCode();
      } else {
         var1 = 0;
      }

      Data var4 = this.data;
      if(var4 != null) {
         var2 = var4.hashCode();
      }

      return var1 * 31 + var2;
   }

   public String toString() {
      return "ImpressionEvent(name=" + this.name + ", data=" + this.data + ")";
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0007"},
      d2 = {"Lco/uk/getmondo/api/model/tracking/ImpressionEvent$Companion;", "", "()V", "DATE_FORMATTER", "Ljava/text/SimpleDateFormat;", "getDATE_FORMATTER", "()Ljava/text/SimpleDateFormat;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class Companion {
      private Companion() {
      }

      // $FF: synthetic method
      public Companion(i var1) {
         this();
      }

      private final SimpleDateFormat a() {
         return ImpressionEvent.DATE_FORMATTER;
      }
   }
}
