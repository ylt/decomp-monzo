package co.uk.getmondo.api.model.tracking;

import co.uk.getmondo.api.model.b;
import co.uk.getmondo.d.h;
import com.google.gson.a.c;
import java.io.Serializable;
import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.l;
import org.threeten.bp.YearMonth;
import org.threeten.bp.temporal.ChronoUnit;
import org.threeten.bp.temporal.Temporal;
import org.threeten.bp.temporal.TemporalUnit;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u001f\n\u0002\u0010\t\n\u0002\bU\n\u0002\u0010\u0000\n\u0002\b\u0004\b\u0086\b\u0018\u0000 \u0088\u00012\u00020\u0001:\u0002\u0088\u0001B½\u0005\u0012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u000f\u0012\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u000f\u0012\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u0012\u001a\u0004\u0018\u00010\u000f\u0012\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u0014\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u0015\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u0016\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0017\u001a\u0004\u0018\u00010\u000f\u0012\n\b\u0002\u0010\u0018\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0019\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u001a\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u001b\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u001c\u001a\u0004\u0018\u00010\u000f\u0012\n\b\u0002\u0010\u001d\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u001e\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u001f\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010 \u001a\u0004\u0018\u00010\u000f\u0012\n\b\u0002\u0010!\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\"\u001a\u0004\u0018\u00010\u000f\u0012\n\b\u0002\u0010#\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010$\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010%\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010&\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010'\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010(\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010)\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010*\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010+\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010,\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010-\u001a\u0004\u0018\u00010\u000f\u0012\n\b\u0002\u0010.\u001a\u0004\u0018\u00010/\u0012\n\b\u0002\u00100\u001a\u0004\u0018\u00010/\u0012\n\b\u0002\u00101\u001a\u0004\u0018\u00010\u000f\u0012\n\b\u0002\u00102\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u00103\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u00104\u001a\u0004\u0018\u00010\u000f\u0012\n\b\u0002\u00105\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u00106\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u00107\u001a\u0004\u0018\u00010\u000f\u0012\n\b\u0002\u00108\u001a\u0004\u0018\u00010\u000f\u0012\n\b\u0002\u00109\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010:\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010;\u001a\u0004\u0018\u00010\u000f\u0012\n\b\u0002\u0010<\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010=\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010>\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010?\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010@J\u0010\u0010D\u001a\u0004\u0018\u00010\u0003HÂ\u0003¢\u0006\u0002\u0010EJ\u000b\u0010F\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u0010\u0010G\u001a\u0004\u0018\u00010\u000fHÂ\u0003¢\u0006\u0002\u0010HJ\u0010\u0010I\u001a\u0004\u0018\u00010\u000fHÂ\u0003¢\u0006\u0002\u0010HJ\u000b\u0010J\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u0010\u0010K\u001a\u0004\u0018\u00010\u000fHÂ\u0003¢\u0006\u0002\u0010HJ\u000b\u0010L\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u000b\u0010M\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u000b\u0010N\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u0010\u0010O\u001a\u0004\u0018\u00010\u0003HÂ\u0003¢\u0006\u0002\u0010EJ\u0010\u0010P\u001a\u0004\u0018\u00010\u000fHÂ\u0003¢\u0006\u0002\u0010HJ\u000b\u0010Q\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u0010\u0010R\u001a\u0004\u0018\u00010\u0003HÂ\u0003¢\u0006\u0002\u0010EJ\u0010\u0010S\u001a\u0004\u0018\u00010\u0003HÂ\u0003¢\u0006\u0002\u0010EJ\u000b\u0010T\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u000b\u0010U\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u0010\u0010V\u001a\u0004\u0018\u00010\u000fHÂ\u0003¢\u0006\u0002\u0010HJ\u000b\u0010W\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u000b\u0010X\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u0010\u0010Y\u001a\u0004\u0018\u00010\u0003HÂ\u0003¢\u0006\u0002\u0010EJ\u0010\u0010Z\u001a\u0004\u0018\u00010\u000fHÂ\u0003¢\u0006\u0002\u0010HJ\u000b\u0010[\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u000b\u0010\\\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u0010\u0010]\u001a\u0004\u0018\u00010\u000fHÂ\u0003¢\u0006\u0002\u0010HJ\u000b\u0010^\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u000b\u0010_\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u000b\u0010`\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u000b\u0010a\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u0010\u0010b\u001a\u0004\u0018\u00010\u0003HÂ\u0003¢\u0006\u0002\u0010EJ\u000b\u0010c\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u000b\u0010d\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u000b\u0010e\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u000b\u0010f\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u000b\u0010g\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u000b\u0010h\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u0010\u0010i\u001a\u0004\u0018\u00010\u000fHÂ\u0003¢\u0006\u0002\u0010HJ\u0010\u0010j\u001a\u0004\u0018\u00010/HÂ\u0003¢\u0006\u0002\u0010kJ\u0010\u0010l\u001a\u0004\u0018\u00010/HÂ\u0003¢\u0006\u0002\u0010kJ\u0010\u0010m\u001a\u0004\u0018\u00010\u000fHÂ\u0003¢\u0006\u0002\u0010HJ\u000b\u0010n\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u000b\u0010o\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u0010\u0010p\u001a\u0004\u0018\u00010\u000fHÂ\u0003¢\u0006\u0002\u0010HJ\u000b\u0010q\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u000b\u0010r\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u000b\u0010s\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u0010\u0010t\u001a\u0004\u0018\u00010\u000fHÂ\u0003¢\u0006\u0002\u0010HJ\u0010\u0010u\u001a\u0004\u0018\u00010\u000fHÂ\u0003¢\u0006\u0002\u0010HJ\u000b\u0010v\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u000b\u0010w\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u0010\u0010x\u001a\u0004\u0018\u00010\u000fHÂ\u0003¢\u0006\u0002\u0010HJ\u000b\u0010y\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u000b\u0010z\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u000b\u0010{\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u000b\u0010|\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u000b\u0010}\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u0010\u0010~\u001a\u0004\u0018\u00010\u0003HÂ\u0003¢\u0006\u0002\u0010EJ\u0010\u0010\u007f\u001a\u0004\u0018\u00010\u0003HÂ\u0003¢\u0006\u0002\u0010EJ\f\u0010\u0080\u0001\u001a\u0004\u0018\u00010\u0005HÂ\u0003JÈ\u0005\u0010\u0081\u0001\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u000f2\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u000f2\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0012\u001a\u0004\u0018\u00010\u000f2\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0014\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0015\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0016\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0017\u001a\u0004\u0018\u00010\u000f2\n\b\u0002\u0010\u0018\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0019\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u001a\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u001b\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u001c\u001a\u0004\u0018\u00010\u000f2\n\b\u0002\u0010\u001d\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u001e\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u001f\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010 \u001a\u0004\u0018\u00010\u000f2\n\b\u0002\u0010!\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\"\u001a\u0004\u0018\u00010\u000f2\n\b\u0002\u0010#\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010$\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010%\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010&\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010'\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010(\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010)\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010*\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010+\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010,\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010-\u001a\u0004\u0018\u00010\u000f2\n\b\u0002\u0010.\u001a\u0004\u0018\u00010/2\n\b\u0002\u00100\u001a\u0004\u0018\u00010/2\n\b\u0002\u00101\u001a\u0004\u0018\u00010\u000f2\n\b\u0002\u00102\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u00103\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u00104\u001a\u0004\u0018\u00010\u000f2\n\b\u0002\u00105\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u00106\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u00107\u001a\u0004\u0018\u00010\u000f2\n\b\u0002\u00108\u001a\u0004\u0018\u00010\u000f2\n\b\u0002\u00109\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010:\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010;\u001a\u0004\u0018\u00010\u000f2\n\b\u0002\u0010<\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010=\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010>\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010?\u001a\u0004\u0018\u00010\u0005HÆ\u0001¢\u0006\u0003\u0010\u0082\u0001J\u0016\u0010\u0083\u0001\u001a\u00020\u000f2\n\u0010\u0084\u0001\u001a\u0005\u0018\u00010\u0085\u0001HÖ\u0003J\n\u0010\u0086\u0001\u001a\u00020\u0003HÖ\u0001J\n\u0010\u0087\u0001\u001a\u00020\u0005HÖ\u0001R\u0012\u0010\u001f\u001a\u0004\u0018\u00010\u0003X\u0082\u0004¢\u0006\u0004\n\u0002\u0010AR\u0012\u0010\u000b\u001a\u0004\u0018\u00010\u0003X\u0082\u0004¢\u0006\u0004\n\u0002\u0010AR\u0014\u00107\u001a\u0004\u0018\u00010\u000f8\u0002X\u0083\u0004¢\u0006\u0004\n\u0002\u0010BR\u0012\u0010(\u001a\u0004\u0018\u00010\u00058\u0002X\u0083\u0004¢\u0006\u0002\n\u0000R\u0012\u0010)\u001a\u0004\u0018\u00010\u00058\u0002X\u0083\u0004¢\u0006\u0002\n\u0000R\u0014\u0010'\u001a\u0004\u0018\u00010\u00038\u0002X\u0083\u0004¢\u0006\u0004\n\u0002\u0010AR\u0010\u0010\u001d\u001a\u0004\u0018\u00010\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u001c\u001a\u0004\u0018\u00010\u000fX\u0082\u0004¢\u0006\u0004\n\u0002\u0010BR\u0012\u0010\f\u001a\u0004\u0018\u00010\u00058\u0002X\u0083\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0015\u001a\u0004\u0018\u00010\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u0018\u001a\u0004\u0018\u00010\u0003X\u0082\u0004¢\u0006\u0004\n\u0002\u0010AR\u0012\u0010\u0019\u001a\u0004\u0018\u00010\u0003X\u0082\u0004¢\u0006\u0004\n\u0002\u0010AR\u0010\u0010\r\u001a\u0004\u0018\u00010\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u0010\u0010!\u001a\u0004\u0018\u00010\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u000e\u001a\u0004\u0018\u00010\u000f8\u0002X\u0083\u0004¢\u0006\u0004\n\u0002\u0010BR\u0012\u0010\u0011\u001a\u0004\u0018\u00010\u00058\u0002X\u0083\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\n\u001a\u0004\u0018\u00010\u0003X\u0082\u0004¢\u0006\u0004\n\u0002\u0010AR\u0012\u0010\"\u001a\u0004\u0018\u00010\u000fX\u0082\u0004¢\u0006\u0004\n\u0002\u0010BR\u0010\u0010,\u001a\u0004\u0018\u00010\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u0012\u00108\u001a\u0004\u0018\u00010\u000fX\u0082\u0004¢\u0006\u0004\n\u0002\u0010BR\u0010\u00105\u001a\u0004\u0018\u00010\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u0012\u0010?\u001a\u0004\u0018\u00010\u00058\u0002X\u0083\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u001e\u001a\u0004\u0018\u00010\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u0012\u0010.\u001a\u0004\u0018\u00010/X\u0082\u0004¢\u0006\u0004\n\u0002\u0010CR\u0012\u00104\u001a\u0004\u0018\u00010\u000fX\u0082\u0004¢\u0006\u0004\n\u0002\u0010BR\u0010\u0010\u001a\u001a\u0004\u0018\u00010\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u0012\u0010#\u001a\u0004\u0018\u00010\u00058\u0002X\u0083\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u0017\u001a\u0004\u0018\u00010\u000fX\u0082\u0004¢\u0006\u0004\n\u0002\u0010BR\u0014\u0010\u0012\u001a\u0004\u0018\u00010\u000f8\u0002X\u0083\u0004¢\u0006\u0004\n\u0002\u0010BR\u0012\u0010*\u001a\u0004\u0018\u00010\u00058\u0002X\u0083\u0004¢\u0006\u0002\n\u0000R\u0012\u00101\u001a\u0004\u0018\u00010\u000fX\u0082\u0004¢\u0006\u0004\n\u0002\u0010BR\u0010\u0010\u001b\u001a\u0004\u0018\u00010\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u0016\u001a\u0004\u0018\u00010\u0003X\u0082\u0004¢\u0006\u0004\n\u0002\u0010AR\u0012\u0010>\u001a\u0004\u0018\u00010\u00058\u0002X\u0083\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u0014\u001a\u0004\u0018\u00010\u00058\u0002X\u0083\u0004¢\u0006\u0002\n\u0000R\u0012\u0010 \u001a\u0004\u0018\u00010\u000fX\u0082\u0004¢\u0006\u0004\n\u0002\u0010BR\u0012\u0010:\u001a\u0004\u0018\u00010\u00058\u0002X\u0083\u0004¢\u0006\u0002\n\u0000R\u0012\u00109\u001a\u0004\u0018\u00010\u00058\u0002X\u0083\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u0013\u001a\u0004\u0018\u00010\u00058\u0002X\u0083\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0002X\u0083\u0004¢\u0006\u0002\n\u0000R\u0012\u0010;\u001a\u0004\u0018\u00010\u000fX\u0082\u0004¢\u0006\u0004\n\u0002\u0010BR\u0010\u0010=\u001a\u0004\u0018\u00010\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u0010\u0010&\u001a\u0004\u0018\u00010\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\b\u001a\u0004\u0018\u00010\u00058\u0002X\u0083\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u0007\u001a\u0004\u0018\u00010\u00058\u0002X\u0083\u0004¢\u0006\u0002\n\u0000R\u0010\u00106\u001a\u0004\u0018\u00010\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u0012\u0010-\u001a\u0004\u0018\u00010\u000fX\u0082\u0004¢\u0006\u0004\n\u0002\u0010BR\u0012\u00100\u001a\u0004\u0018\u00010/X\u0082\u0004¢\u0006\u0004\n\u0002\u0010CR\u0014\u0010\u0010\u001a\u0004\u0018\u00010\u000f8\u0002X\u0083\u0004¢\u0006\u0004\n\u0002\u0010BR\u0012\u00103\u001a\u0004\u0018\u00010\u00058\u0002X\u0083\u0004¢\u0006\u0002\n\u0000R\u0012\u0010<\u001a\u0004\u0018\u00010\u00058\u0002X\u0083\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0002\u001a\u0004\u0018\u00010\u00038\u0002X\u0083\u0004¢\u0006\u0004\n\u0002\u0010AR\u0012\u0010$\u001a\u0004\u0018\u00010\u00058\u0002X\u0083\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u00058\u0002X\u0083\u0004¢\u0006\u0002\n\u0000R\u0010\u0010%\u001a\u0004\u0018\u00010\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u0004\u0018\u00010\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u0012\u0010+\u001a\u0004\u0018\u00010\u00058\u0002X\u0083\u0004¢\u0006\u0002\n\u0000R\u0010\u00102\u001a\u0004\u0018\u00010\u0005X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0089\u0001"},
   d2 = {"Lco/uk/getmondo/api/model/tracking/Data;", "Ljava/io/Serializable;", "tourPage", "", "openedFrom", "", "transactionId", "referrer", "redirectId", "url", "eligible", "amount", "cameFrom", "description", "docDone", "", "selfieDone", "docType", "kycRejected", "oldCategory", "newCategory", "category", "month", "granted", "contacts", "contactsOnMonzo", "from", "method", "authenticate", "appShortcutType", "fingerprintAuth", "activeNotifications", "nfcEnabled", "diligence", "enabled", "gpsAdid", "traceId", "type", "path", "apiStatusCode", "apiErrorCode", "apiErrorMessage", "logoutReason", "userId", "errorType", "sampleError", "firstSampleDelta", "", "secondSampleDelta", "memoryError", "videoErrorReason", "splitWith", "firstTime", "feedback", "reportMonth", "androidPayEnabled", "fallback", "notificationType", "notificationSubType", "optedIn", "topicId", "outcome", "nameOnCard", "fileType", "(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "Ljava/lang/Integer;", "Ljava/lang/Boolean;", "Ljava/lang/Long;", "component1", "()Ljava/lang/Integer;", "component10", "component11", "()Ljava/lang/Boolean;", "component12", "component13", "component14", "component15", "component16", "component17", "component18", "component19", "component2", "component20", "component21", "component22", "component23", "component24", "component25", "component26", "component27", "component28", "component29", "component3", "component30", "component31", "component32", "component33", "component34", "component35", "component36", "component37", "component38", "component39", "component4", "component40", "component41", "component42", "()Ljava/lang/Long;", "component43", "component44", "component45", "component46", "component47", "component48", "component49", "component5", "component50", "component51", "component52", "component53", "component54", "component55", "component56", "component57", "component58", "component6", "component7", "component8", "component9", "copy", "(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Data;", "equals", "other", "", "hashCode", "toString", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class Data implements Serializable {
   public static final Data.Companion Companion = new Data.Companion((i)null);
   private final Integer activeNotifications;
   private final Integer amount;
   @c(
      a = "androidpay_enabled"
   )
   private final Boolean androidPayEnabled;
   @c(
      a = "api_error_code"
   )
   private final String apiErrorCode;
   @c(
      a = "api_error_message"
   )
   private final String apiErrorMessage;
   @c(
      a = "api_status_code"
   )
   private final Integer apiStatusCode;
   private final String appShortcutType;
   private final Boolean authenticate;
   @c(
      a = "came_from"
   )
   private final String cameFrom;
   private final String category;
   private final Integer contacts;
   private final Integer contactsOnMonzo;
   private final String description;
   private final String diligence;
   @c(
      a = "doc_done"
   )
   private final Boolean docDone;
   @c(
      a = "doc_type"
   )
   private final String docType;
   private final Integer eligible;
   private final Boolean enabled;
   private final String errorType;
   private final Boolean fallback;
   private final String feedback;
   @c(
      a = "file_type"
   )
   private final String fileType;
   private final String fingerprintAuth;
   private final Long firstSampleDelta;
   private final Boolean firstTime;
   private final String from;
   @c(
      a = "gps_adid"
   )
   private final String gpsAdid;
   private final Boolean granted;
   @c(
      a = "kyc_rejected"
   )
   private final Boolean kycRejected;
   @c(
      a = "logout_reason"
   )
   private final String logoutReason;
   private final Boolean memoryError;
   private final String method;
   private final Integer month;
   @c(
      a = "name_on_card"
   )
   private final String nameOnCard;
   @c(
      a = "new_category"
   )
   private final String newCategory;
   private final Boolean nfcEnabled;
   @c(
      a = "notification_sub_type"
   )
   private final String notificationSubType;
   @c(
      a = "notification_type"
   )
   private final String notificationType;
   @c(
      a = "old_category"
   )
   private final String oldCategory;
   @c(
      a = "opened_from"
   )
   private final String openedFrom;
   private final Boolean optedIn;
   private final String outcome;
   private final String path;
   @c(
      a = "play_store_redirect_event_id"
   )
   private final String redirectId;
   @c(
      a = "referrer"
   )
   private final String referrer;
   private final String reportMonth;
   private final Boolean sampleError;
   private final Long secondSampleDelta;
   @c(
      a = "selfie_done"
   )
   private final Boolean selfieDone;
   @c(
      a = "split_with"
   )
   private final String splitWith;
   @c(
      a = "topic_id"
   )
   private final String topicId;
   @c(
      a = "tour_page"
   )
   private final Integer tourPage;
   @c(
      a = "trace_id"
   )
   private final String traceId;
   @c(
      a = "transaction_id"
   )
   private final String transactionId;
   private final String type;
   private final String url;
   @c(
      a = "user_id"
   )
   private final String userId;
   private final String videoErrorReason;

   public Data() {
      this((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -1, 67108863, (i)null);
   }

   public Data(Integer var1, String var2, String var3, String var4, String var5, String var6, Integer var7, Integer var8, String var9, String var10, Boolean var11, Boolean var12, String var13, Boolean var14, String var15, String var16, String var17, Integer var18, Boolean var19, Integer var20, Integer var21, String var22, String var23, Boolean var24, String var25, String var26, Integer var27, Boolean var28, String var29, Boolean var30, String var31, String var32, String var33, String var34, Integer var35, String var36, String var37, String var38, String var39, String var40, Boolean var41, Long var42, Long var43, Boolean var44, String var45, String var46, Boolean var47, String var48, String var49, Boolean var50, Boolean var51, String var52, String var53, Boolean var54, String var55, String var56, String var57, String var58) {
      this.tourPage = var1;
      this.openedFrom = var2;
      this.transactionId = var3;
      this.referrer = var4;
      this.redirectId = var5;
      this.url = var6;
      this.eligible = var7;
      this.amount = var8;
      this.cameFrom = var9;
      this.description = var10;
      this.docDone = var11;
      this.selfieDone = var12;
      this.docType = var13;
      this.kycRejected = var14;
      this.oldCategory = var15;
      this.newCategory = var16;
      this.category = var17;
      this.month = var18;
      this.granted = var19;
      this.contacts = var20;
      this.contactsOnMonzo = var21;
      this.from = var22;
      this.method = var23;
      this.authenticate = var24;
      this.appShortcutType = var25;
      this.fingerprintAuth = var26;
      this.activeNotifications = var27;
      this.nfcEnabled = var28;
      this.diligence = var29;
      this.enabled = var30;
      this.gpsAdid = var31;
      this.traceId = var32;
      this.type = var33;
      this.path = var34;
      this.apiStatusCode = var35;
      this.apiErrorCode = var36;
      this.apiErrorMessage = var37;
      this.logoutReason = var38;
      this.userId = var39;
      this.errorType = var40;
      this.sampleError = var41;
      this.firstSampleDelta = var42;
      this.secondSampleDelta = var43;
      this.memoryError = var44;
      this.videoErrorReason = var45;
      this.splitWith = var46;
      this.firstTime = var47;
      this.feedback = var48;
      this.reportMonth = var49;
      this.androidPayEnabled = var50;
      this.fallback = var51;
      this.notificationType = var52;
      this.notificationSubType = var53;
      this.optedIn = var54;
      this.topicId = var55;
      this.outcome = var56;
      this.nameOnCard = var57;
      this.fileType = var58;
   }

   // $FF: synthetic method
   public Data(Integer var1, String var2, String var3, String var4, String var5, String var6, Integer var7, Integer var8, String var9, String var10, Boolean var11, Boolean var12, String var13, Boolean var14, String var15, String var16, String var17, Integer var18, Boolean var19, Integer var20, Integer var21, String var22, String var23, Boolean var24, String var25, String var26, Integer var27, Boolean var28, String var29, Boolean var30, String var31, String var32, String var33, String var34, Integer var35, String var36, String var37, String var38, String var39, String var40, Boolean var41, Long var42, Long var43, Boolean var44, String var45, String var46, Boolean var47, String var48, String var49, Boolean var50, Boolean var51, String var52, String var53, Boolean var54, String var55, String var56, String var57, String var58, int var59, int var60, i var61) {
      if((var59 & 1) != 0) {
         var1 = (Integer)null;
      }

      if((var59 & 2) != 0) {
         var2 = (String)null;
      }

      if((var59 & 4) != 0) {
         var3 = (String)null;
      }

      if((var59 & 8) != 0) {
         var4 = (String)null;
      }

      if((var59 & 16) != 0) {
         var5 = (String)null;
      }

      if((var59 & 32) != 0) {
         var6 = (String)null;
      }

      if((var59 & 64) != 0) {
         var7 = (Integer)null;
      }

      if((var59 & 128) != 0) {
         var8 = (Integer)null;
      }

      if((var59 & 256) != 0) {
         var9 = (String)null;
      }

      if((var59 & 512) != 0) {
         var10 = (String)null;
      }

      if((var59 & 1024) != 0) {
         var11 = (Boolean)null;
      }

      if((var59 & 2048) != 0) {
         var12 = (Boolean)null;
      }

      if((var59 & 4096) != 0) {
         var13 = (String)null;
      }

      if((var59 & 8192) != 0) {
         var14 = (Boolean)null;
      }

      if((var59 & 16384) != 0) {
         var15 = (String)null;
      }

      if(('耀' & var59) != 0) {
         var16 = (String)null;
      }

      if((65536 & var59) != 0) {
         var17 = (String)null;
      }

      if((131072 & var59) != 0) {
         var18 = (Integer)null;
      }

      if((262144 & var59) != 0) {
         var19 = (Boolean)null;
      }

      if((524288 & var59) != 0) {
         var20 = (Integer)null;
      }

      if((1048576 & var59) != 0) {
         var21 = (Integer)null;
      }

      if((2097152 & var59) != 0) {
         var22 = (String)null;
      }

      if((4194304 & var59) != 0) {
         var23 = (String)null;
      }

      if((8388608 & var59) != 0) {
         var24 = (Boolean)null;
      }

      if((16777216 & var59) != 0) {
         var25 = (String)null;
      }

      if((33554432 & var59) != 0) {
         var26 = (String)null;
      }

      if((67108864 & var59) != 0) {
         var27 = (Integer)null;
      }

      if((134217728 & var59) != 0) {
         var28 = (Boolean)null;
      }

      if((268435456 & var59) != 0) {
         var29 = (String)null;
      }

      if((536870912 & var59) != 0) {
         var30 = (Boolean)null;
      }

      if((1073741824 & var59) != 0) {
         var31 = (String)null;
      }

      if((Integer.MIN_VALUE & var59) != 0) {
         var32 = (String)null;
      }

      if((var60 & 1) != 0) {
         var33 = (String)null;
      }

      if((var60 & 2) != 0) {
         var34 = (String)null;
      }

      if((var60 & 4) != 0) {
         var35 = (Integer)null;
      }

      if((var60 & 8) != 0) {
         var36 = (String)null;
      }

      if((var60 & 16) != 0) {
         var37 = (String)null;
      }

      if((var60 & 32) != 0) {
         var38 = (String)null;
      }

      if((var60 & 64) != 0) {
         var39 = (String)null;
      }

      if((var60 & 128) != 0) {
         var40 = (String)null;
      }

      if((var60 & 256) != 0) {
         var41 = (Boolean)null;
      }

      if((var60 & 512) != 0) {
         var42 = (Long)null;
      }

      if((var60 & 1024) != 0) {
         var43 = (Long)null;
      }

      if((var60 & 2048) != 0) {
         var44 = (Boolean)null;
      }

      if((var60 & 4096) != 0) {
         var45 = (String)null;
      }

      if((var60 & 8192) != 0) {
         var46 = (String)null;
      }

      if((var60 & 16384) != 0) {
         var47 = (Boolean)null;
      }

      if(('耀' & var60) != 0) {
         var48 = (String)null;
      }

      if((65536 & var60) != 0) {
         var49 = (String)null;
      }

      if((131072 & var60) != 0) {
         var50 = (Boolean)null;
      }

      if((262144 & var60) != 0) {
         var51 = (Boolean)null;
      }

      if((524288 & var60) != 0) {
         var52 = (String)null;
      }

      if((1048576 & var60) != 0) {
         var53 = (String)null;
      }

      if((2097152 & var60) != 0) {
         var54 = (Boolean)null;
      }

      if((4194304 & var60) != 0) {
         var55 = (String)null;
      }

      if((8388608 & var60) != 0) {
         var56 = (String)null;
      }

      if((16777216 & var60) != 0) {
         var57 = (String)null;
      }

      if((33554432 & var60) != 0) {
         var58 = (String)null;
      }

      this(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10, var11, var12, var13, var14, var15, var16, var17, var18, var19, var20, var21, var22, var23, var24, var25, var26, var27, var28, var29, var30, var31, var32, var33, var34, var35, var36, var37, var38, var39, var40, var41, var42, var43, var44, var45, var46, var47, var48, var49, var50, var51, var52, var53, var54, var55, var56, var57, var58);
   }

   public boolean equals(Object var1) {
      boolean var2;
      if(this != var1) {
         label140: {
            if(var1 instanceof Data) {
               Data var3 = (Data)var1;
               if(l.a(this.tourPage, var3.tourPage) && l.a(this.openedFrom, var3.openedFrom) && l.a(this.transactionId, var3.transactionId) && l.a(this.referrer, var3.referrer) && l.a(this.redirectId, var3.redirectId) && l.a(this.url, var3.url) && l.a(this.eligible, var3.eligible) && l.a(this.amount, var3.amount) && l.a(this.cameFrom, var3.cameFrom) && l.a(this.description, var3.description) && l.a(this.docDone, var3.docDone) && l.a(this.selfieDone, var3.selfieDone) && l.a(this.docType, var3.docType) && l.a(this.kycRejected, var3.kycRejected) && l.a(this.oldCategory, var3.oldCategory) && l.a(this.newCategory, var3.newCategory) && l.a(this.category, var3.category) && l.a(this.month, var3.month) && l.a(this.granted, var3.granted) && l.a(this.contacts, var3.contacts) && l.a(this.contactsOnMonzo, var3.contactsOnMonzo) && l.a(this.from, var3.from) && l.a(this.method, var3.method) && l.a(this.authenticate, var3.authenticate) && l.a(this.appShortcutType, var3.appShortcutType) && l.a(this.fingerprintAuth, var3.fingerprintAuth) && l.a(this.activeNotifications, var3.activeNotifications) && l.a(this.nfcEnabled, var3.nfcEnabled) && l.a(this.diligence, var3.diligence) && l.a(this.enabled, var3.enabled) && l.a(this.gpsAdid, var3.gpsAdid) && l.a(this.traceId, var3.traceId) && l.a(this.type, var3.type) && l.a(this.path, var3.path) && l.a(this.apiStatusCode, var3.apiStatusCode) && l.a(this.apiErrorCode, var3.apiErrorCode) && l.a(this.apiErrorMessage, var3.apiErrorMessage) && l.a(this.logoutReason, var3.logoutReason) && l.a(this.userId, var3.userId) && l.a(this.errorType, var3.errorType) && l.a(this.sampleError, var3.sampleError) && l.a(this.firstSampleDelta, var3.firstSampleDelta) && l.a(this.secondSampleDelta, var3.secondSampleDelta) && l.a(this.memoryError, var3.memoryError) && l.a(this.videoErrorReason, var3.videoErrorReason) && l.a(this.splitWith, var3.splitWith) && l.a(this.firstTime, var3.firstTime) && l.a(this.feedback, var3.feedback) && l.a(this.reportMonth, var3.reportMonth) && l.a(this.androidPayEnabled, var3.androidPayEnabled) && l.a(this.fallback, var3.fallback) && l.a(this.notificationType, var3.notificationType) && l.a(this.notificationSubType, var3.notificationSubType) && l.a(this.optedIn, var3.optedIn) && l.a(this.topicId, var3.topicId) && l.a(this.outcome, var3.outcome) && l.a(this.nameOnCard, var3.nameOnCard) && l.a(this.fileType, var3.fileType)) {
                  break label140;
               }
            }

            var2 = false;
            return var2;
         }
      }

      var2 = true;
      return var2;
   }

   public int hashCode() {
      int var58 = 0;
      Integer var59 = this.tourPage;
      int var1;
      if(var59 != null) {
         var1 = var59.hashCode();
      } else {
         var1 = 0;
      }

      String var60 = this.openedFrom;
      int var2;
      if(var60 != null) {
         var2 = var60.hashCode();
      } else {
         var2 = 0;
      }

      var60 = this.transactionId;
      int var3;
      if(var60 != null) {
         var3 = var60.hashCode();
      } else {
         var3 = 0;
      }

      var60 = this.referrer;
      int var4;
      if(var60 != null) {
         var4 = var60.hashCode();
      } else {
         var4 = 0;
      }

      var60 = this.redirectId;
      int var5;
      if(var60 != null) {
         var5 = var60.hashCode();
      } else {
         var5 = 0;
      }

      var60 = this.url;
      int var6;
      if(var60 != null) {
         var6 = var60.hashCode();
      } else {
         var6 = 0;
      }

      var59 = this.eligible;
      int var7;
      if(var59 != null) {
         var7 = var59.hashCode();
      } else {
         var7 = 0;
      }

      var59 = this.amount;
      int var8;
      if(var59 != null) {
         var8 = var59.hashCode();
      } else {
         var8 = 0;
      }

      var60 = this.cameFrom;
      int var9;
      if(var60 != null) {
         var9 = var60.hashCode();
      } else {
         var9 = 0;
      }

      var60 = this.description;
      int var10;
      if(var60 != null) {
         var10 = var60.hashCode();
      } else {
         var10 = 0;
      }

      Boolean var61 = this.docDone;
      int var11;
      if(var61 != null) {
         var11 = var61.hashCode();
      } else {
         var11 = 0;
      }

      var61 = this.selfieDone;
      int var12;
      if(var61 != null) {
         var12 = var61.hashCode();
      } else {
         var12 = 0;
      }

      var60 = this.docType;
      int var13;
      if(var60 != null) {
         var13 = var60.hashCode();
      } else {
         var13 = 0;
      }

      var61 = this.kycRejected;
      int var14;
      if(var61 != null) {
         var14 = var61.hashCode();
      } else {
         var14 = 0;
      }

      var60 = this.oldCategory;
      int var15;
      if(var60 != null) {
         var15 = var60.hashCode();
      } else {
         var15 = 0;
      }

      var60 = this.newCategory;
      int var16;
      if(var60 != null) {
         var16 = var60.hashCode();
      } else {
         var16 = 0;
      }

      var60 = this.category;
      int var17;
      if(var60 != null) {
         var17 = var60.hashCode();
      } else {
         var17 = 0;
      }

      var59 = this.month;
      int var18;
      if(var59 != null) {
         var18 = var59.hashCode();
      } else {
         var18 = 0;
      }

      var61 = this.granted;
      int var19;
      if(var61 != null) {
         var19 = var61.hashCode();
      } else {
         var19 = 0;
      }

      var59 = this.contacts;
      int var20;
      if(var59 != null) {
         var20 = var59.hashCode();
      } else {
         var20 = 0;
      }

      var59 = this.contactsOnMonzo;
      int var21;
      if(var59 != null) {
         var21 = var59.hashCode();
      } else {
         var21 = 0;
      }

      var60 = this.from;
      int var22;
      if(var60 != null) {
         var22 = var60.hashCode();
      } else {
         var22 = 0;
      }

      var60 = this.method;
      int var23;
      if(var60 != null) {
         var23 = var60.hashCode();
      } else {
         var23 = 0;
      }

      var61 = this.authenticate;
      int var24;
      if(var61 != null) {
         var24 = var61.hashCode();
      } else {
         var24 = 0;
      }

      var60 = this.appShortcutType;
      int var25;
      if(var60 != null) {
         var25 = var60.hashCode();
      } else {
         var25 = 0;
      }

      var60 = this.fingerprintAuth;
      int var26;
      if(var60 != null) {
         var26 = var60.hashCode();
      } else {
         var26 = 0;
      }

      var59 = this.activeNotifications;
      int var27;
      if(var59 != null) {
         var27 = var59.hashCode();
      } else {
         var27 = 0;
      }

      var61 = this.nfcEnabled;
      int var28;
      if(var61 != null) {
         var28 = var61.hashCode();
      } else {
         var28 = 0;
      }

      var60 = this.diligence;
      int var29;
      if(var60 != null) {
         var29 = var60.hashCode();
      } else {
         var29 = 0;
      }

      var61 = this.enabled;
      int var30;
      if(var61 != null) {
         var30 = var61.hashCode();
      } else {
         var30 = 0;
      }

      var60 = this.gpsAdid;
      int var31;
      if(var60 != null) {
         var31 = var60.hashCode();
      } else {
         var31 = 0;
      }

      var60 = this.traceId;
      int var32;
      if(var60 != null) {
         var32 = var60.hashCode();
      } else {
         var32 = 0;
      }

      var60 = this.type;
      int var33;
      if(var60 != null) {
         var33 = var60.hashCode();
      } else {
         var33 = 0;
      }

      var60 = this.path;
      int var34;
      if(var60 != null) {
         var34 = var60.hashCode();
      } else {
         var34 = 0;
      }

      var59 = this.apiStatusCode;
      int var35;
      if(var59 != null) {
         var35 = var59.hashCode();
      } else {
         var35 = 0;
      }

      var60 = this.apiErrorCode;
      int var36;
      if(var60 != null) {
         var36 = var60.hashCode();
      } else {
         var36 = 0;
      }

      var60 = this.apiErrorMessage;
      int var37;
      if(var60 != null) {
         var37 = var60.hashCode();
      } else {
         var37 = 0;
      }

      var60 = this.logoutReason;
      int var38;
      if(var60 != null) {
         var38 = var60.hashCode();
      } else {
         var38 = 0;
      }

      var60 = this.userId;
      int var39;
      if(var60 != null) {
         var39 = var60.hashCode();
      } else {
         var39 = 0;
      }

      var60 = this.errorType;
      int var40;
      if(var60 != null) {
         var40 = var60.hashCode();
      } else {
         var40 = 0;
      }

      var61 = this.sampleError;
      int var41;
      if(var61 != null) {
         var41 = var61.hashCode();
      } else {
         var41 = 0;
      }

      Long var62 = this.firstSampleDelta;
      int var42;
      if(var62 != null) {
         var42 = var62.hashCode();
      } else {
         var42 = 0;
      }

      var62 = this.secondSampleDelta;
      int var43;
      if(var62 != null) {
         var43 = var62.hashCode();
      } else {
         var43 = 0;
      }

      var61 = this.memoryError;
      int var44;
      if(var61 != null) {
         var44 = var61.hashCode();
      } else {
         var44 = 0;
      }

      var60 = this.videoErrorReason;
      int var45;
      if(var60 != null) {
         var45 = var60.hashCode();
      } else {
         var45 = 0;
      }

      var60 = this.splitWith;
      int var46;
      if(var60 != null) {
         var46 = var60.hashCode();
      } else {
         var46 = 0;
      }

      var61 = this.firstTime;
      int var47;
      if(var61 != null) {
         var47 = var61.hashCode();
      } else {
         var47 = 0;
      }

      var60 = this.feedback;
      int var48;
      if(var60 != null) {
         var48 = var60.hashCode();
      } else {
         var48 = 0;
      }

      var60 = this.reportMonth;
      int var49;
      if(var60 != null) {
         var49 = var60.hashCode();
      } else {
         var49 = 0;
      }

      var61 = this.androidPayEnabled;
      int var50;
      if(var61 != null) {
         var50 = var61.hashCode();
      } else {
         var50 = 0;
      }

      var61 = this.fallback;
      int var51;
      if(var61 != null) {
         var51 = var61.hashCode();
      } else {
         var51 = 0;
      }

      var60 = this.notificationType;
      int var52;
      if(var60 != null) {
         var52 = var60.hashCode();
      } else {
         var52 = 0;
      }

      var60 = this.notificationSubType;
      int var53;
      if(var60 != null) {
         var53 = var60.hashCode();
      } else {
         var53 = 0;
      }

      var61 = this.optedIn;
      int var54;
      if(var61 != null) {
         var54 = var61.hashCode();
      } else {
         var54 = 0;
      }

      var60 = this.topicId;
      int var55;
      if(var60 != null) {
         var55 = var60.hashCode();
      } else {
         var55 = 0;
      }

      var60 = this.outcome;
      int var56;
      if(var60 != null) {
         var56 = var60.hashCode();
      } else {
         var56 = 0;
      }

      var60 = this.nameOnCard;
      int var57;
      if(var60 != null) {
         var57 = var60.hashCode();
      } else {
         var57 = 0;
      }

      var60 = this.fileType;
      if(var60 != null) {
         var58 = var60.hashCode();
      }

      return (var57 + (var56 + (var55 + (var54 + (var53 + (var52 + (var51 + (var50 + (var49 + (var48 + (var47 + (var46 + (var45 + (var44 + (var43 + (var42 + (var41 + (var40 + (var39 + (var38 + (var37 + (var36 + (var35 + (var34 + (var33 + (var32 + (var31 + (var30 + (var29 + (var28 + (var27 + (var26 + (var25 + (var24 + (var23 + (var22 + (var21 + (var20 + (var19 + (var18 + (var17 + (var16 + (var15 + (var14 + (var13 + (var12 + (var11 + (var10 + (var9 + (var8 + (var7 + (var6 + (var5 + (var4 + (var3 + (var2 + var1 * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31 + var58;
   }

   public String toString() {
      return "Data(tourPage=" + this.tourPage + ", openedFrom=" + this.openedFrom + ", transactionId=" + this.transactionId + ", referrer=" + this.referrer + ", redirectId=" + this.redirectId + ", url=" + this.url + ", eligible=" + this.eligible + ", amount=" + this.amount + ", cameFrom=" + this.cameFrom + ", description=" + this.description + ", docDone=" + this.docDone + ", selfieDone=" + this.selfieDone + ", docType=" + this.docType + ", kycRejected=" + this.kycRejected + ", oldCategory=" + this.oldCategory + ", newCategory=" + this.newCategory + ", category=" + this.category + ", month=" + this.month + ", granted=" + this.granted + ", contacts=" + this.contacts + ", contactsOnMonzo=" + this.contactsOnMonzo + ", from=" + this.from + ", method=" + this.method + ", authenticate=" + this.authenticate + ", appShortcutType=" + this.appShortcutType + ", fingerprintAuth=" + this.fingerprintAuth + ", activeNotifications=" + this.activeNotifications + ", nfcEnabled=" + this.nfcEnabled + ", diligence=" + this.diligence + ", enabled=" + this.enabled + ", gpsAdid=" + this.gpsAdid + ", traceId=" + this.traceId + ", type=" + this.type + ", path=" + this.path + ", apiStatusCode=" + this.apiStatusCode + ", apiErrorCode=" + this.apiErrorCode + ", apiErrorMessage=" + this.apiErrorMessage + ", logoutReason=" + this.logoutReason + ", userId=" + this.userId + ", errorType=" + this.errorType + ", sampleError=" + this.sampleError + ", firstSampleDelta=" + this.firstSampleDelta + ", secondSampleDelta=" + this.secondSampleDelta + ", memoryError=" + this.memoryError + ", videoErrorReason=" + this.videoErrorReason + ", splitWith=" + this.splitWith + ", firstTime=" + this.firstTime + ", feedback=" + this.feedback + ", reportMonth=" + this.reportMonth + ", androidPayEnabled=" + this.androidPayEnabled + ", fallback=" + this.fallback + ", notificationType=" + this.notificationType + ", notificationSubType=" + this.notificationSubType + ", optedIn=" + this.optedIn + ", topicId=" + this.topicId + ", outcome=" + this.outcome + ", nameOnCard=" + this.nameOnCard + ", fileType=" + this.fileType + ")";
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000N\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0014\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0010\t\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0005J*\u0010\u0006\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u00052\u0006\u0010\b\u001a\u00020\t2\b\u0010\n\u001a\u0004\u0018\u00010\t2\b\u0010\u0006\u001a\u0004\u0018\u00010\u000bJ\u000e\u0010\f\u001a\u00020\u00042\u0006\u0010\r\u001a\u00020\tJ\u000e\u0010\u000e\u001a\u00020\u00042\u0006\u0010\u000e\u001a\u00020\tJ\u000e\u0010\u000f\u001a\u00020\u00042\u0006\u0010\u000f\u001a\u00020\u0010J\u000e\u0010\u0011\u001a\u00020\u00042\u0006\u0010\u0011\u001a\u00020\tJ\u000e\u0010\u0012\u001a\u00020\u00042\u0006\u0010\u0012\u001a\u00020\tJ\u0015\u0010\u0013\u001a\u00020\u00042\b\u0010\u0013\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0014J\u0015\u0010\u0015\u001a\u00020\u00042\b\u0010\u0015\u001a\u0004\u0018\u00010\u0016¢\u0006\u0002\u0010\u0017J\u000e\u0010\u0018\u001a\u00020\u00042\u0006\u0010\u0019\u001a\u00020\u0016J\u000e\u0010\u001a\u001a\u00020\u00042\u0006\u0010\u001b\u001a\u00020\u001cJ\u000e\u0010\u001a\u001a\u00020\u00042\u0006\u0010\u001d\u001a\u00020\tJ\u000e\u0010\u001e\u001a\u00020\u00042\u0006\u0010\u001f\u001a\u00020\tJ\u001e\u0010 \u001a\u00020\u00042\u0006\u0010!\u001a\u00020\u00162\u0006\u0010\"\u001a\u00020\u00162\u0006\u0010#\u001a\u00020\u0016J\u0016\u0010$\u001a\u00020\u00042\u0006\u0010$\u001a\u00020\t2\u0006\u0010%\u001a\u00020\tJ\u000e\u0010&\u001a\u00020\u00042\u0006\u0010&\u001a\u00020\tJ\u0016\u0010'\u001a\u00020\u00042\u0006\u0010(\u001a\u00020\u00052\u0006\u0010)\u001a\u00020\u0005J\u000e\u0010*\u001a\u00020\u00042\u0006\u0010*\u001a\u00020\u0016J\u000e\u0010+\u001a\u00020\u00042\u0006\u0010,\u001a\u00020\u0016J\u000e\u0010-\u001a\u00020\u00042\u0006\u0010-\u001a\u00020\tJ\u000e\u0010.\u001a\u00020\u00042\u0006\u0010.\u001a\u00020\tJ\u0016\u0010/\u001a\u00020\u00042\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u00100\u001a\u000201J\u000e\u00102\u001a\u00020\u00042\u0006\u00103\u001a\u00020\tJ\u0015\u00104\u001a\u00020\u00042\b\u00104\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0014J\u000e\u00105\u001a\u00020\u00042\u0006\u00105\u001a\u00020\tJ\u000e\u00106\u001a\u00020\u00042\u0006\u00106\u001a\u00020\tJ\u0016\u00107\u001a\u00020\u00042\u0006\u00108\u001a\u00020\t2\u0006\u00109\u001a\u00020\tJ\u000e\u0010:\u001a\u00020\u00042\u0006\u0010:\u001a\u00020\tJ\u0016\u0010;\u001a\u00020\u00042\u0006\u0010<\u001a\u00020\t2\u0006\u0010=\u001a\u00020\u0016J'\u0010>\u001a\u00020\u00042\u0006\u0010?\u001a\u00020\u00162\b\u0010@\u001a\u0004\u0018\u00010A2\b\u0010B\u001a\u0004\u0018\u00010A¢\u0006\u0002\u0010C¨\u0006D"},
      d2 = {"Lco/uk/getmondo/api/model/tracking/Data$Companion;", "", "()V", "amount", "Lco/uk/getmondo/api/model/tracking/Data;", "", "apiError", "apiStatusCode", "path", "", "traceId", "Lco/uk/getmondo/api/model/ApiError;", "appShortcutType", "shortcutType", "cameFrom", "category", "Lco/uk/getmondo/model/Category;", "description", "diligence", "eligible", "(Ljava/lang/Integer;)Lco/uk/getmondo/api/model/tracking/Data;", "enabled", "", "(Ljava/lang/Boolean;)Lco/uk/getmondo/api/model/tracking/Data;", "firstTime", "isFirstGoldenTicket", "from", "entryPoint", "Lco/uk/getmondo/api/model/tracking/Impression$CustomiseMonzoMeLinkFrom;", "value", "googlePlayServicesAdvertisingId", "gpsAdid", "identityVerificationSatus", "docDone", "selfieDone", "rejected", "logoutReason", "userId", "openedFrom", "p2pEnabled", "numberOfContactsOnMonzo", "numberOfContactsNotOnMonzo", "permissionGranted", "pinAuthentication", "isSuccess", "redirectId", "referrer", "spendingData", "month", "Lorg/threeten/bp/YearMonth;", "splitWith", "number", "tourPage", "transactionId", "type", "updateTransactionCategory", "oldCategory", "newCategory", "url", "videoError", "videoErrorReason", "memoryError", "videoMp4Info", "sampleError", "firstSampleDelta", "", "secondSampleDelta", "(ZLjava/lang/Long;Ljava/lang/Long;)Lco/uk/getmondo/api/model/tracking/Data;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class Companion {
      private Companion() {
      }

      // $FF: synthetic method
      public Companion(i var1) {
         this();
      }

      public final Data a(int var1) {
         return new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, Integer.valueOf(var1), (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -129, 67108863, (i)null);
      }

      public final Data a(int var1, int var2) {
         return new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, Integer.valueOf(var2), Integer.valueOf(var1), (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -1572865, 67108863, (i)null);
      }

      public final Data a(int var1, String var2, String var3, b var4) {
         l.b(var2, "path");
         String var5;
         if(var4 != null) {
            var5 = var4.a();
         } else {
            var5 = null;
         }

         String var6;
         if(var4 != null) {
            var6 = var4.b();
         } else {
            var6 = null;
         }

         return new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, var3, "error", var2, Integer.valueOf(var1), var5, var6, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, Integer.MAX_VALUE, 67108832, (i)null);
      }

      public final Data a(Impression.CustomiseMonzoMeLinkFrom var1) {
         l.b(var1, "entryPoint");
         return new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, var1.a(), (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -2097153, 67108863, (i)null);
      }

      public final Data a(h var1) {
         l.b(var1, "category");
         return new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, var1.f(), (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -65537, 67108863, (i)null);
      }

      public final Data a(h var1, YearMonth var2) {
         l.b(var1, "category");
         l.b(var2, "month");
         int var3 = (int)var2.c(1).a((Temporal)YearMonth.a().c(2), (TemporalUnit)ChronoUnit.j);
         return new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, var1.f(), Integer.valueOf(0 - var3), (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -196609, 67108863, (i)null);
      }

      public final Data a(Boolean var1) {
         return new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, var1, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -536870913, 67108863, (i)null);
      }

      public final Data a(Integer var1) {
         return new Data(var1, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -2, 67108863, (i)null);
      }

      public final Data a(String var1) {
         l.b(var1, "url");
         return new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, var1, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -33, 67108863, (i)null);
      }

      public final Data a(String var1, String var2) {
         l.b(var1, "oldCategory");
         l.b(var2, "newCategory");
         return new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, var1, var2, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -49153, 67108863, (i)null);
      }

      public final Data a(String var1, boolean var2) {
         l.b(var1, "videoErrorReason");
         return new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, Boolean.valueOf(var2), var1, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -1, 67102719, (i)null);
      }

      public final Data a(boolean var1) {
         return new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, Boolean.valueOf(var1), (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -262145, 67108863, (i)null);
      }

      public final Data a(boolean var1, Long var2, Long var3) {
         return new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, Boolean.valueOf(var1), var2, var3, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -1, 67107071, (i)null);
      }

      public final Data a(boolean var1, boolean var2, boolean var3) {
         return new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, Boolean.valueOf(var1), Boolean.valueOf(var2), (String)null, Boolean.valueOf(var3), (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -11265, 67108863, (i)null);
      }

      public final Data b(Integer var1) {
         return new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, var1, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -65, 67108863, (i)null);
      }

      public final Data b(String var1) {
         l.b(var1, "openedFrom");
         return new Data((Integer)null, var1, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -3, 67108863, (i)null);
      }

      public final Data b(String var1, String var2) {
         l.b(var1, "logoutReason");
         l.b(var2, "userId");
         return new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, var1, var2, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -1, 67108767, (i)null);
      }

      public final Data b(boolean var1) {
         return new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, "PIN", Boolean.valueOf(var1), (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -12582913, 67108863, (i)null);
      }

      public final Data c(String var1) {
         l.b(var1, "transactionId");
         return new Data((Integer)null, (String)null, var1, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -5, 67108863, (i)null);
      }

      public final Data c(boolean var1) {
         return new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, Boolean.valueOf(var1), (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -1, 67092479, (i)null);
      }

      public final Data d(String var1) {
         l.b(var1, "referrer");
         return new Data((Integer)null, (String)null, (String)null, var1, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -9, 67108863, (i)null);
      }

      public final Data e(String var1) {
         l.b(var1, "redirectId");
         return new Data((Integer)null, (String)null, (String)null, (String)null, var1, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -17, 67108863, (i)null);
      }

      public final Data f(String var1) {
         l.b(var1, "cameFrom");
         return new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, var1, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -257, 67108863, (i)null);
      }

      public final Data g(String var1) {
         l.b(var1, "type");
         return new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, var1, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -1, 67108862, (i)null);
      }

      public final Data h(String var1) {
         l.b(var1, "description");
         return new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, var1, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -513, 67108863, (i)null);
      }

      public final Data i(String var1) {
         l.b(var1, "value");
         return new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, var1, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -2097153, 67108863, (i)null);
      }

      public final Data j(String var1) {
         l.b(var1, "gpsAdid");
         return new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, var1, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -1073741825, 67108863, (i)null);
      }

      public final Data k(String var1) {
         l.b(var1, "shortcutType");
         return new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, var1, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -16777217, 67108863, (i)null);
      }

      public final Data l(String var1) {
         l.b(var1, "diligence");
         return new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, var1, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -268435457, 67108863, (i)null);
      }

      public final Data m(String var1) {
         l.b(var1, "number");
         return new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, var1, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -1, 67100671, (i)null);
      }
   }
}
