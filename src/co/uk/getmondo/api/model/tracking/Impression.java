package co.uk.getmondo.api.model.tracking;

import co.uk.getmondo.api.model.VerificationType;
import co.uk.getmondo.api.model.b;
import co.uk.getmondo.d.h;
import co.uk.getmondo.feed.SpendingReportFeedbackDialogFragment;
import co.uk.getmondo.signup.identity_verification.sdd.j;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.l;
import org.threeten.bp.YearMonth;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u000e\b\u0086\b\u0018\u0000 \u00142\u00020\u0001:\r\u0014\u0015\u0016\u0017\u0018\u0019\u001a\u001b\u001c\u001d\u001e\u001f B\u0015\u0012\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\u0002\u0010\u0005J\u000e\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0004J\u000f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003HÆ\u0003J\u0019\u0010\f\u001a\u00020\u00002\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003HÆ\u0001J\u0013\u0010\r\u001a\u00020\u000e2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0010\u001a\u00020\u0011HÖ\u0001J\b\u0010\u0012\u001a\u00020\u0013H\u0016R\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007¨\u0006!"},
   d2 = {"Lco/uk/getmondo/api/model/tracking/Impression;", "", "events", "", "Lco/uk/getmondo/api/model/tracking/ImpressionEvent;", "(Ljava/util/List;)V", "getEvents", "()Ljava/util/List;", "add", "", "event", "component1", "copy", "equals", "", "other", "hashCode", "", "toString", "", "Companion", "CustomiseMonzoMeLinkFrom", "FingerprintSupportType", "HelpOutcome", "IntercomFrom", "InvestIntroFrom", "KycFrom", "OpenedCommunityFrom", "OpenedFaqsFrom", "PaymentFlowFrom", "PinFrom", "TopUpSuccessType", "TopUpTapFrom", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class Impression {
   public static final Impression.Companion Companion = new Impression.Companion((i)null);
   private static final String EVENT_WAILIST_SHOW = "wl.waitinglist.show";
   private final List events;

   public Impression() {
      this((List)null, 1, (i)null);
   }

   public Impression(List var1) {
      l.b(var1, "events");
      super();
      this.events = var1;
   }

   // $FF: synthetic method
   public Impression(List var1, int var2, i var3) {
      if((var2 & 1) != 0) {
         var1 = (List)(new ArrayList());
      }

      this(var1);
   }

   public static final Impression A() {
      return Companion.I();
   }

   public static final Impression B() {
      return Companion.J();
   }

   public static final Impression C() {
      return Companion.K();
   }

   public static final Impression D() {
      return Companion.L();
   }

   public static final Impression E() {
      return Companion.M();
   }

   public static final Impression F() {
      return Companion.O();
   }

   public static final Impression G() {
      return Companion.P();
   }

   public static final Impression H() {
      return Companion.Q();
   }

   public static final Impression I() {
      return Companion.R();
   }

   public static final Impression J() {
      return Companion.W();
   }

   public static final Impression K() {
      return Companion.X();
   }

   public static final Impression L() {
      return Companion.Y();
   }

   public static final Impression M() {
      return Companion.ac();
   }

   public static final Impression N() {
      return Companion.ad();
   }

   public static final Impression O() {
      return Companion.ae();
   }

   public static final Impression P() {
      return Companion.af();
   }

   public static final Impression Q() {
      return Companion.ag();
   }

   public static final Impression R() {
      return Companion.ar();
   }

   public static final Impression S() {
      return Companion.as();
   }

   public static final Impression T() {
      return Companion.at();
   }

   public static final Impression U() {
      return Companion.au();
   }

   public static final Impression V() {
      return Companion.av();
   }

   public static final Impression W() {
      return Companion.aw();
   }

   public static final Impression X() {
      return Companion.ax();
   }

   public static final Impression Y() {
      return Companion.ay();
   }

   public static final Impression a(int var0) {
      return Companion.a(var0);
   }

   public static final Impression a(int var0, String var1, String var2, b var3) {
      l.b(var1, "path");
      return Companion.a(var0, var1, var2, var3);
   }

   public static final Impression a(Impression.CustomiseMonzoMeLinkFrom var0) {
      l.b(var0, "entryPoint");
      return Companion.a(var0);
   }

   public static final Impression a(Impression.FingerprintSupportType var0, int var1, boolean var2) {
      l.b(var0, "fingerprintSupportType");
      return Companion.a(var0, var1, var2);
   }

   public static final Impression a(Impression.IntercomFrom var0) {
      return Impression.Companion.a(Companion, var0, (String)null, (String)null, 6, (Object)null);
   }

   public static final Impression a(Impression.PaymentFlowFrom var0) {
      l.b(var0, "entryPoint");
      return Companion.a(var0);
   }

   public static final Impression a(Impression.TopUpSuccessType var0) {
      l.b(var0, "type");
      return Companion.a(var0);
   }

   public static final Impression a(Impression.TopUpTapFrom var0) {
      l.b(var0, "cameFrom");
      return Companion.a(var0);
   }

   public static final Impression a(h var0) {
      l.b(var0, "category");
      return Companion.a(var0);
   }

   public static final Impression a(h var0, YearMonth var1) {
      l.b(var0, "category");
      l.b(var1, "yearMonth");
      return Companion.a(var0, var1);
   }

   public static final Impression a(SpendingReportFeedbackDialogFragment.a var0, YearMonth var1) {
      l.b(var0, "type");
      l.b(var1, "yearMonth");
      return Companion.a(var0, var1);
   }

   public static final Impression a(j var0) {
      l.b(var0, "sddUpgradeLevelType");
      return Companion.a(var0);
   }

   public static final Impression a(String var0) {
      l.b(var0, "url");
      return Companion.a(var0);
   }

   public static final Impression a(String var0, String var1) {
      l.b(var0, "redirectUrl");
      l.b(var1, "errorType");
      return Companion.b(var0, var1);
   }

   public static final Impression a(String var0, boolean var1) {
      l.b(var0, "identityDocumentType");
      return Companion.a(var0, var1);
   }

   public static final Impression a(YearMonth var0) {
      l.b(var0, "yearMonth");
      return Companion.a(var0);
   }

   public static final Impression a(boolean var0) {
      return Companion.a(var0);
   }

   public static final Impression a(boolean var0, Long var1, Long var2) {
      return Companion.a(var0, var1, var2);
   }

   public static final Impression a(boolean var0, String var1) {
      l.b(var1, "url");
      return Companion.a(var0, var1);
   }

   public static final Impression b() {
      return Companion.b();
   }

   public static final Impression b(int var0) {
      return Companion.b(var0);
   }

   public static final Impression b(int var0, String var1, String var2, b var3) {
      l.b(var1, "path");
      return Companion.c(var0, var1, var2, var3);
   }

   public static final Impression b(h var0, YearMonth var1) {
      l.b(var0, "category");
      l.b(var1, "yearMonth");
      return Companion.b(var0, var1);
   }

   public static final Impression b(String var0) {
      l.b(var0, "url");
      return Companion.b(var0);
   }

   public static final Impression b(String var0, String var1) {
      l.b(var0, "analyticName");
      l.b(var1, "appName");
      return Companion.c(var0, var1);
   }

   public static final Impression b(String var0, boolean var1) {
      l.b(var0, "reason");
      return Companion.b(var0, var1);
   }

   public static final Impression b(boolean var0) {
      return Companion.b(var0);
   }

   public static final Impression c() {
      return Companion.c();
   }

   public static final Impression c(int var0) {
      return Companion.d(var0);
   }

   public static final Impression c(h var0, YearMonth var1) {
      l.b(var0, "category");
      l.b(var1, "yearMonth");
      return Companion.c(var0, var1);
   }

   public static final Impression c(String var0) {
      l.b(var0, "url");
      return Companion.c(var0);
   }

   public static final Impression c(String var0, String var1) {
      l.b(var0, "type");
      return Companion.d(var0, var1);
   }

   public static final Impression c(boolean var0) {
      return Companion.c(var0);
   }

   public static final Impression d() {
      return Companion.d();
   }

   public static final Impression d(String var0) {
      l.b(var0, "url");
      return Companion.d(var0);
   }

   public static final Impression d(boolean var0) {
      return Companion.d(var0);
   }

   public static final Impression e() {
      return Companion.e();
   }

   public static final Impression e(String var0) {
      l.b(var0, "description");
      return Companion.e(var0);
   }

   public static final Impression e(boolean var0) {
      return Companion.e(var0);
   }

   public static final Impression f() {
      return Companion.f();
   }

   public static final Impression f(String var0) {
      l.b(var0, "referrer");
      return Companion.h(var0);
   }

   public static final Impression f(boolean var0) {
      return Companion.f(var0);
   }

   public static final Impression g() {
      return Companion.j();
   }

   public static final Impression g(String var0) {
      l.b(var0, "redirectId");
      return Companion.i(var0);
   }

   public static final Impression g(boolean var0) {
      return Companion.h(var0);
   }

   public static final Impression h() {
      return Companion.k();
   }

   public static final Impression h(String var0) {
      l.b(var0, "identityDocumentType");
      return Companion.j(var0);
   }

   public static final Impression h(boolean var0) {
      return Companion.i(var0);
   }

   public static final Impression i() {
      return Companion.l();
   }

   public static final Impression i(String var0) {
      l.b(var0, "identityDocumentType");
      return Companion.k(var0);
   }

   public static final Impression i(boolean var0) {
      return Companion.j(var0);
   }

   public static final Impression j() {
      return Companion.m();
   }

   public static final Impression j(String var0) {
      l.b(var0, "advertisingId");
      return Companion.l(var0);
   }

   public static final Impression j(boolean var0) {
      return Companion.k(var0);
   }

   public static final Impression k() {
      return Companion.o();
   }

   public static final Impression l() {
      return Companion.q();
   }

   public static final Impression m() {
      return Companion.r();
   }

   public static final Impression n() {
      return Companion.v();
   }

   public static final Impression o() {
      return Companion.w();
   }

   public static final Impression p() {
      return Companion.x();
   }

   public static final Impression q() {
      return Companion.y();
   }

   public static final Impression r() {
      return Companion.z();
   }

   public static final Impression s() {
      return Companion.A();
   }

   public static final Impression t() {
      return Companion.B();
   }

   public static final Impression u() {
      return Companion.C();
   }

   public static final Impression v() {
      return Companion.D();
   }

   public static final Impression w() {
      return Companion.E();
   }

   public static final Impression x() {
      return Companion.F();
   }

   public static final Impression y() {
      return Companion.G();
   }

   public static final Impression z() {
      return Companion.H();
   }

   public final void a(ImpressionEvent var1) {
      l.b(var1, "event");
      this.events.add(var1);
   }

   public boolean equals(Object var1) {
      boolean var2;
      if(this != var1) {
         label26: {
            if(var1 instanceof Impression) {
               Impression var3 = (Impression)var1;
               if(l.a(this.events, var3.events)) {
                  break label26;
               }
            }

            var2 = false;
            return var2;
         }
      }

      var2 = true;
      return var2;
   }

   public int hashCode() {
      List var2 = this.events;
      int var1;
      if(var2 != null) {
         var1 = var2.hashCode();
      } else {
         var1 = 0;
      }

      return var1;
   }

   public String toString() {
      StringBuilder var2 = new StringBuilder();
      Iterator var1 = this.events.iterator();

      while(var1.hasNext()) {
         var2.append(((ImpressionEvent)var1.next()).toString());
         var2.append(" ");
      }

      String var3 = var2.toString();
      l.a(var3, "stringBuilder.toString()");
      return var3;
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000Æ\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u000f\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0018\u0002\n\u0002\b\r\n\u0002\u0010\t\n\u0002\b\u0017\n\u0002\u0010\u0011\n\u0002\b\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0017\n\u0002\u0018\u0002\n\u0002\b\u001b\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u0018\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u001c\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\n\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\b\u0010\u0007\u001a\u00020\bH\u0007J\b\u0010\t\u001a\u00020\bH\u0007J\u0010\u0010\n\u001a\u00020\b2\u0006\u0010\u000b\u001a\u00020\u0004H\u0007J,\u0010\f\u001a\u00020\b2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00042\b\u0010\u0010\u001a\u0004\u0018\u00010\u00042\b\u0010\f\u001a\u0004\u0018\u00010\u0011H\u0007J\b\u0010\u0012\u001a\u00020\bH\u0007J\b\u0010\u0013\u001a\u00020\bH\u0007J \u0010\u0014\u001a\u00020\b2\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u000e2\u0006\u0010\u0018\u001a\u00020\u0019H\u0007J\u0010\u0010\u001a\u001a\u00020\b2\u0006\u0010\u001b\u001a\u00020\u0004H\u0007J\u000e\u0010\u001c\u001a\u00020\b2\u0006\u0010\u001d\u001a\u00020\u0019J\u0018\u0010\u001e\u001a\u00020\b2\u0006\u0010\u001f\u001a\u00020\u00042\u0006\u0010 \u001a\u00020\u0004H\u0007J\b\u0010!\u001a\u00020\bH\u0007J\u000e\u0010\"\u001a\u00020\b2\u0006\u0010#\u001a\u00020\u0004J\b\u0010$\u001a\u00020\bH\u0007J\b\u0010%\u001a\u00020\bH\u0007J\b\u0010&\u001a\u00020\bH\u0007J\u0018\u0010'\u001a\u00020\b2\u0006\u0010(\u001a\u00020)2\u0006\u0010*\u001a\u00020)H\u0007J\u0010\u0010+\u001a\u00020\b2\u0006\u0010,\u001a\u00020\u0019H\u0007J\b\u0010-\u001a\u00020\bH\u0007J\b\u0010.\u001a\u00020\bH\u0007J\b\u0010/\u001a\u00020\bH\u0007J\u0018\u00100\u001a\u00020\b2\b\b\u0002\u00101\u001a\u00020\u00042\u0006\u00102\u001a\u00020\u0004J\b\u00103\u001a\u00020\bH\u0007J\u0010\u00104\u001a\u00020\u00042\u0006\u00105\u001a\u00020\u0019H\u0007J\u0016\u00106\u001a\u00020\b2\u0006\u00107\u001a\u0002082\u0006\u00109\u001a\u00020\u0004J\u0010\u0010:\u001a\u00020\b2\u0006\u0010;\u001a\u00020\u0004H\u0007J\b\u0010<\u001a\u00020\bH\u0007J\b\u0010=\u001a\u00020\bH\u0007J\u0010\u0010>\u001a\u00020\b2\u0006\u0010?\u001a\u00020\u0004H\u0007J\u0018\u0010@\u001a\u00020\b2\u0006\u0010A\u001a\u00020\u00042\u0006\u0010B\u001a\u00020\u0019H\u0007J)\u0010C\u001a\u00020\b2\u0006\u0010D\u001a\u00020\u00192\b\u0010E\u001a\u0004\u0018\u00010F2\b\u0010G\u001a\u0004\u0018\u00010FH\u0007¢\u0006\u0002\u0010HJ\b\u0010I\u001a\u00020\bH\u0007J\b\u0010J\u001a\u00020\bH\u0007J,\u0010K\u001a\u00020\b2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00042\b\u0010\u0010\u001a\u0004\u0018\u00010\u00042\b\u0010\f\u001a\u0004\u0018\u00010\u0011H\u0007J\b\u0010L\u001a\u00020\bH\u0007J\b\u0010M\u001a\u00020\bH\u0007J\b\u0010N\u001a\u00020\bH\u0007J\u0010\u0010O\u001a\u00020\b2\u0006\u0010P\u001a\u00020\u000eH\u0007J\u0006\u0010Q\u001a\u00020\bJ\u0006\u0010R\u001a\u00020\bJ\u0006\u0010S\u001a\u00020\bJ\u0006\u0010T\u001a\u00020\bJ\u0006\u0010U\u001a\u00020\bJ\u0006\u0010V\u001a\u00020\bJ\u0006\u0010W\u001a\u00020\bJ\u0006\u0010X\u001a\u00020\bJ\u0006\u0010Y\u001a\u00020\bJ\u0006\u0010Z\u001a\u00020\bJ\u0006\u0010[\u001a\u00020\bJ!\u0010\\\u001a\u00020\b2\u0012\u0010]\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00040^\"\u00020\u0004H\u0007¢\u0006\u0002\u0010_J\u001a\u0010`\u001a\u00020\b2\u0006\u0010a\u001a\u00020\u00042\b\u0010b\u001a\u0004\u0018\u00010\u0004H\u0007J\u0010\u0010c\u001a\u00020\b2\u0006\u0010d\u001a\u00020\u0004H\u0007J\b\u0010e\u001a\u00020\bH\u0007J\u0010\u0010f\u001a\u00020\b2\u0006\u0010g\u001a\u00020hH\u0007J\u0010\u0010i\u001a\u00020\b2\u0006\u00101\u001a\u00020jH\u0007J(\u0010k\u001a\u00020\b2\u0006\u00101\u001a\u00020l2\n\b\u0002\u0010m\u001a\u0004\u0018\u00010\u00042\n\b\u0002\u00109\u001a\u0004\u0018\u00010\u0004H\u0007J\b\u0010n\u001a\u00020\bH\u0007J\u0010\u0010o\u001a\u00020\b2\u0006\u0010d\u001a\u00020\u0004H\u0007J\u000e\u0010p\u001a\u00020\b2\u0006\u0010q\u001a\u00020\u0019J\u0010\u0010r\u001a\u00020\b2\u0006\u0010m\u001a\u00020)H\u0007J\u0018\u0010s\u001a\u00020\b2\u0006\u0010t\u001a\u00020\u000e2\u0006\u0010u\u001a\u00020\u000eH\u0007J\u0010\u0010v\u001a\u00020\b2\u0006\u0010w\u001a\u00020\u0019H\u0007J\u0010\u0010x\u001a\u00020\b2\u0006\u0010y\u001a\u00020\u0004H\u0007J,\u0010z\u001a\u00020\b2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00042\b\u0010\u0010\u001a\u0004\u0018\u00010\u00042\b\u0010\f\u001a\u0004\u0018\u00010\u0011H\u0007J\b\u0010{\u001a\u00020\bH\u0007J\u0010\u0010|\u001a\u00020\b2\u0006\u0010P\u001a\u00020\u000eH\u0007J\b\u0010}\u001a\u00020\bH\u0007J\b\u0010~\u001a\u00020\bH\u0007J\b\u0010\u007f\u001a\u00020\bH\u0007J\t\u0010\u0080\u0001\u001a\u00020\bH\u0007J\t\u0010\u0081\u0001\u001a\u00020\bH\u0007J\u0013\u0010\u0082\u0001\u001a\u00020\b2\b\u0010\u0083\u0001\u001a\u00030\u0084\u0001H\u0007J\t\u0010\u0085\u0001\u001a\u00020\bH\u0007J\t\u0010\u0086\u0001\u001a\u00020\bH\u0007J\t\u0010\u0087\u0001\u001a\u00020\bH\u0007J\t\u0010\u0088\u0001\u001a\u00020\bH\u0007J\t\u0010\u0089\u0001\u001a\u00020\bH\u0007J\t\u0010\u008a\u0001\u001a\u00020\bH\u0007J\u0012\u0010\u008b\u0001\u001a\u00020\b2\u0007\u0010\u008c\u0001\u001a\u00020\u0019H\u0007J\t\u0010\u008d\u0001\u001a\u00020\bH\u0007J\t\u0010\u008e\u0001\u001a\u00020\bH\u0007J\u0007\u0010\u008f\u0001\u001a\u00020\bJ\u0007\u0010\u0090\u0001\u001a\u00020\bJ\u0007\u0010\u0091\u0001\u001a\u00020\bJ\t\u0010\u0092\u0001\u001a\u00020\bH\u0007J\t\u0010\u0093\u0001\u001a\u00020\bH\u0007J\u0007\u0010\u0094\u0001\u001a\u00020\bJ\u0007\u0010\u0095\u0001\u001a\u00020\bJ\u0007\u0010\u0096\u0001\u001a\u00020\bJ\u0007\u0010\u0097\u0001\u001a\u00020\bJ\t\u0010\u0098\u0001\u001a\u00020\bH\u0007J\t\u0010\u0099\u0001\u001a\u00020\bH\u0007J\t\u0010\u009a\u0001\u001a\u00020\bH\u0007J\t\u0010\u009b\u0001\u001a\u00020\bH\u0007J\t\u0010\u009c\u0001\u001a\u00020\bH\u0007J\u0007\u0010\u009d\u0001\u001a\u00020\bJ\u0013\u0010\u009e\u0001\u001a\u00020\b2\b\u0010\u009f\u0001\u001a\u00030 \u0001H\u0007J\t\u0010¡\u0001\u001a\u00020\bH\u0007J\t\u0010¢\u0001\u001a\u00020\bH\u0007J\u0007\u0010£\u0001\u001a\u00020\bJ\u0012\u0010¤\u0001\u001a\u00020\b2\u0007\u0010\u008c\u0001\u001a\u00020\u0019H\u0007J\u0013\u0010¥\u0001\u001a\u00020\b2\n\b\u0002\u0010m\u001a\u0004\u0018\u00010\u0004J\t\u0010¦\u0001\u001a\u00020\bH\u0007J$\u0010¦\u0001\u001a\u00020\b2\u0007\u0010§\u0001\u001a\u00020\u00192\u0007\u0010¨\u0001\u001a\u00020\u00192\u0007\u0010©\u0001\u001a\u00020\u0019H\u0007J\t\u0010ª\u0001\u001a\u00020\bH\u0007J\t\u0010«\u0001\u001a\u00020\bH\u0007J\u001a\u0010¬\u0001\u001a\u00020\b2\u0006\u0010?\u001a\u00020\u00042\u0007\u0010\u00ad\u0001\u001a\u00020\u0019H\u0007J\u0012\u0010®\u0001\u001a\u00020\b2\u0007\u00101\u001a\u00030¯\u0001H\u0007J\u0012\u0010°\u0001\u001a\u00020\b2\u0007\u00101\u001a\u00030¯\u0001H\u0007J\u0012\u0010±\u0001\u001a\u00020\b2\u0007\u00101\u001a\u00030¯\u0001H\u0007J\t\u0010²\u0001\u001a\u00020\bH\u0007J\t\u0010³\u0001\u001a\u00020\bH\u0007J\u001b\u0010´\u0001\u001a\u00020\b2\u0007\u00101\u001a\u00030¯\u00012\u0007\u0010\u00ad\u0001\u001a\u00020\u0019H\u0007J\t\u0010µ\u0001\u001a\u00020\bH\u0007J\u0012\u0010¶\u0001\u001a\u00020\b2\u0007\u0010\u00ad\u0001\u001a\u00020\u0019H\u0007J\u0011\u0010·\u0001\u001a\u00020\b2\u0006\u0010?\u001a\u00020\u0004H\u0007J\t\u0010¸\u0001\u001a\u00020\bH\u0007J\u0007\u0010¹\u0001\u001a\u00020\bJ\u0007\u0010º\u0001\u001a\u00020\bJ\t\u0010»\u0001\u001a\u00020\bH\u0007J\t\u0010¼\u0001\u001a\u00020\bH\u0007J\t\u0010½\u0001\u001a\u00020\bH\u0007J\u0013\u0010¾\u0001\u001a\u00020\b2\b\u0010\u009f\u0001\u001a\u00030¿\u0001H\u0007J\t\u0010À\u0001\u001a\u00020\bH\u0007J\t\u0010Á\u0001\u001a\u00020\bH\u0007J\t\u0010Â\u0001\u001a\u00020\bH\u0007J\u0012\u0010Ã\u0001\u001a\u00020\b2\u0007\u00101\u001a\u00030Ä\u0001H\u0007J\u0011\u0010Å\u0001\u001a\u00020\b2\b\u0010Æ\u0001\u001a\u00030Ä\u0001J\t\u0010Ç\u0001\u001a\u00020\bH\u0007J\t\u0010È\u0001\u001a\u00020\bH\u0007J\u0007\u0010É\u0001\u001a\u00020\bJ\t\u0010Ê\u0001\u001a\u00020\bH\u0007J\t\u0010Ë\u0001\u001a\u00020\bH\u0007J\u0013\u0010Ì\u0001\u001a\u00020\b2\b\u0010Í\u0001\u001a\u00030Î\u0001H\u0007J\t\u0010Ï\u0001\u001a\u00020\bH\u0007J\t\u0010Ð\u0001\u001a\u00020\bH\u0007J\t\u0010Ñ\u0001\u001a\u00020\bH\u0007J\t\u0010Ò\u0001\u001a\u00020\bH\u0007J\t\u0010Ó\u0001\u001a\u00020\bH\u0007J\t\u0010Ô\u0001\u001a\u00020\bH\u0007J\t\u0010Õ\u0001\u001a\u00020\bH\u0007J\u0007\u0010Ö\u0001\u001a\u00020\bJ\u0007\u0010×\u0001\u001a\u00020\bJ\t\u0010Ø\u0001\u001a\u00020\bH\u0007J\u001b\u0010Ù\u0001\u001a\u00020\b2\u0006\u0010m\u001a\u00020)2\b\u0010Ú\u0001\u001a\u00030Û\u0001H\u0007J\u001b\u0010Ü\u0001\u001a\u00020\b2\u0006\u0010m\u001a\u00020)2\b\u0010Ú\u0001\u001a\u00030Û\u0001H\u0007J\u001b\u0010Ý\u0001\u001a\u00020\b2\u0006\u0010m\u001a\u00020)2\b\u0010Ú\u0001\u001a\u00030Û\u0001H\u0007J\u0013\u0010Þ\u0001\u001a\u00020\b2\b\u0010Ú\u0001\u001a\u00030Û\u0001H\u0007J\u0007\u0010ß\u0001\u001a\u00020\bJ\u0007\u0010à\u0001\u001a\u00020\bJ\u0007\u0010á\u0001\u001a\u00020\bJ\u0007\u0010â\u0001\u001a\u00020\bJ\u0007\u0010ã\u0001\u001a\u00020\bJ\t\u0010ä\u0001\u001a\u00020\bH\u0007J\t\u0010å\u0001\u001a\u00020\bH\u0007J\t\u0010æ\u0001\u001a\u00020\bH\u0007J\t\u0010ç\u0001\u001a\u00020\bH\u0007J\t\u0010è\u0001\u001a\u00020\bH\u0007J\t\u0010é\u0001\u001a\u00020\bH\u0007J\u0012\u0010ê\u0001\u001a\u00020\b2\u0007\u0010ë\u0001\u001a\u00020\u0004H\u0007J\u0007\u0010ì\u0001\u001a\u00020\bJ\t\u0010í\u0001\u001a\u00020\bH\u0007J\t\u0010î\u0001\u001a\u00020\bH\u0007J\t\u0010ï\u0001\u001a\u00020\bH\u0007J\u0007\u0010ð\u0001\u001a\u00020\bJ\u0012\u0010ñ\u0001\u001a\u00020\b2\u0007\u0010ò\u0001\u001a\u00020\u0004H\u0007J\u001c\u0010ñ\u0001\u001a\u00020\b2\u0007\u0010ò\u0001\u001a\u00020\u00042\b\u0010ó\u0001\u001a\u00030ô\u0001H\u0007J\u001c\u0010õ\u0001\u001a\u00020\b2\u0007\u0010a\u001a\u00030ö\u00012\b\u0010Ú\u0001\u001a\u00030Û\u0001H\u0007J\t\u0010÷\u0001\u001a\u00020\bH\u0007J\u0012\u0010ø\u0001\u001a\u00020\b2\u0007\u0010ù\u0001\u001a\u00020\u000eH\u0007J\t\u0010ú\u0001\u001a\u00020\bH\u0007J\t\u0010û\u0001\u001a\u00020\bH\u0007J\u0011\u0010ü\u0001\u001a\u00020\b2\u0006\u00105\u001a\u00020\u0019H\u0007J\u0011\u0010ý\u0001\u001a\u00020\b2\u0006\u00105\u001a\u00020\u0019H\u0007J\u0011\u0010þ\u0001\u001a\u00020\b2\u0006\u00105\u001a\u00020\u0019H\u0007J\u001a\u0010ÿ\u0001\u001a\u00020\b2\u0006\u00105\u001a\u00020\u00192\u0007\u0010ë\u0001\u001a\u00020\u0004H\u0007J\u0011\u0010\u0080\u0002\u001a\u00020\b2\u0006\u00105\u001a\u00020\u0019H\u0007J\u001b\u0010\u0081\u0002\u001a\u00020\b2\u0007\u0010\u0082\u0002\u001a\u00020\u00042\u0007\u0010\u0083\u0002\u001a\u00020\u0004H\u0007J\u001a\u0010\u0084\u0002\u001a\u00020\b2\t\u0010\u0085\u0002\u001a\u0004\u0018\u00010\u0019H\u0007¢\u0006\u0003\u0010\u0086\u0002J\u001a\u0010\u0087\u0002\u001a\u00020\b2\t\u0010\u0085\u0002\u001a\u0004\u0018\u00010\u0019H\u0007¢\u0006\u0003\u0010\u0086\u0002J\u001a\u0010\u0088\u0002\u001a\u00020\b2\t\u0010\u0085\u0002\u001a\u0004\u0018\u00010\u0019H\u0007¢\u0006\u0003\u0010\u0086\u0002J\t\u0010\u0089\u0002\u001a\u00020\bH\u0007J\t\u0010\u008a\u0002\u001a\u00020\bH\u0007J\t\u0010\u008b\u0002\u001a\u00020\bH\u0007J\u0012\u0010\u008c\u0002\u001a\u00020\b2\u0007\u0010\u008d\u0002\u001a\u00020\u0004H\u0007J\t\u0010\u008e\u0002\u001a\u00020\bH\u0007J\t\u0010\u008f\u0002\u001a\u00020\bH\u0007J\u0012\u0010\u0090\u0002\u001a\u00020\b2\u0007\u0010\u0091\u0002\u001a\u00020\u0019H\u0007J\u0012\u0010\u0092\u0002\u001a\u00020\b2\u0007\u0010a\u001a\u00030\u0093\u0002H\u0007J\u0013\u0010\u0094\u0002\u001a\u00020\b2\b\u0010\u0095\u0002\u001a\u00030\u0096\u0002H\u0007J\u001b\u0010\u0097\u0002\u001a\u00020\b2\u0007\u0010\u0098\u0002\u001a\u00020\u00042\u0007\u0010\u0099\u0002\u001a\u00020\u0004H\u0007J\t\u0010\u009a\u0002\u001a\u00020\bH\u0007J\t\u0010\u009b\u0002\u001a\u00020\bH\u0007J\u0012\u0010\u009c\u0002\u001a\u00020\b2\u0007\u0010ë\u0001\u001a\u00020\u0004H\u0007J\u0012\u0010\u009d\u0002\u001a\u00020\b2\u0007\u0010ë\u0001\u001a\u00020\u0004H\u0007J\u0012\u0010\u009e\u0002\u001a\u00020\b2\u0007\u0010ë\u0001\u001a\u00020\u0004H\u0007J\u0011\u0010\u009f\u0002\u001a\u00020\b2\u0006\u0010P\u001a\u00020\u000eH\u0007R\u0014\u0010\u0003\u001a\u00020\u0004X\u0086D¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006 \u0002"},
      d2 = {"Lco/uk/getmondo/api/model/tracking/Impression$Companion;", "", "()V", "EVENT_WAILIST_SHOW", "", "getEVENT_WAILIST_SHOW", "()Ljava/lang/String;", "addAttachmentTapped", "Lco/uk/getmondo/api/model/tracking/Impression;", "addNoteTapped", "advertisingIdObserved", "advertisingId", "apiError", "apiStatusCode", "", "path", "traceId", "Lco/uk/getmondo/api/model/ApiError;", "appBackground", "appForeground", "appInitialised", "fingerprintSupportType", "Lco/uk/getmondo/api/model/tracking/Impression$FingerprintSupportType;", "activeNotifications", "nfcEnabled", "", "appShortcutUsed", "shortcutType", "authenticateAddressChange", "isSuccess", "autoLogout", "logoutReason", "userId", "bypassWaitlist", "cardDetailsConfirmationTap", "selectedNameType", "cardReplacementAddressAddTap", "cardReplacementOrderedTap", "cardReplacementTap", "changeTransactionCategory", "oldCategory", "Lco/uk/getmondo/model/Category;", "newCategory", "contactsPermissionGranted", "permissionGranted", "createProfile", "defrostCardTapped", "dismissSearch", "exportDataTap", "from", "fileType", "freezeCardTapped", "getThreeDsDescription", "initialTopUp", "helpOutcome", "outcome", "Lco/uk/getmondo/api/model/tracking/Impression$HelpOutcome;", "topicId", "installReferral", "referrer", "inviteContactFromAllContacts", "inviteContactFromContactOnMonzo", "kycDocumentOkTap", "identityDocumentType", "kycVideoError", "reason", "memoryError", "kycVideoProcessed", "sampleError", "firstSampleDelta", "", "secondSampleDelta", "(ZLjava/lang/Long;Ljava/lang/Long;)Lco/uk/getmondo/api/model/tracking/Impression;", "kycVideoSelfieOkTap", "kycVideoSelfieRecording", "logOutError", "loginConfirm", "loginMailTap", "loginNoEmailTap", "loginShow", "page", "migrationAnnouncementBannerShow", "migrationAnnouncementDetailsShow", "migrationAnnouncementNotifyTap", "migrationContinueBannerShow", "migrationContinueBannerTap", "migrationIntroPackageShow", "migrationIntroStepsShow", "migrationInvitationBannerShow", "migrationInvitationDetailsShow", "migrationInvitationLearnMoreTap", "migrationStartTap", "multipleImpressions", "names", "", "([Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;", "notificationReceived", "type", "subType", "openMapForTransaction", "transactionId", "openSplitCost", "openedCommunity", "openedFrom", "Lco/uk/getmondo/api/model/tracking/Impression$OpenedCommunityFrom;", "openedFaqs", "Lco/uk/getmondo/api/model/tracking/Impression$OpenedFaqsFrom;", "openedIntercom", "Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;", "category", "openedSettings", "openedTransaction", "optInToMarketingEmails", "optIn", "p2pCategorized", "p2pEnabled", "numberOfContactsOnMonzo", "numberOfContactsNotOnMonzo", "p2pPaymentRequest", "transferSuccessful", "playstoreRedirect", "redirectId", "reportedError", "requestContactsPermission", "requestMoneyTour", "selectPhotoTapped", "sendMoneyErrorNotEnabled", "settingsCloseAccount", "settingsEditProfile", "settingsFscsProtection", "settingsLimits", "verificationType", "Lco/uk/getmondo/api/model/VerificationType;", "settingsLogOut", "settingsOpenSource", "settingsPaymentLimits", "settingsPrivacyPolicy", "settingsShowAbout", "settingsTermsAndConditions", "shareGoldenTicket", "isFirstGoldenTicket", "shareScreen", "showAddContact", "showAddressChangeConfirm", "showAddressChangeList", "showAddressChangePostcode", "showAddressPicker", "showCardActivation", "showCardDetailsConfirmation", "showCardOrderPinEntry", "showCardOrderPinVerify", "showCardOrderPinVerifyFailed", "showCardReplacementActivation", "showCardReplacementAddressPicker", "showCardScreen", "showCardShipped", "showChecklist", "showContacts", "showCustomiseMonzoMeLink", "entryPoint", "Lco/uk/getmondo/api/model/tracking/Impression$CustomiseMonzoMeLinkFrom;", "showEddFeedItem", "showForceUpgrade", "showFscsProtection", "showGoldenTicket", "showHelpContent", "showIdentityVerification", "photoIdDone", "selfieDone", "kycRejected", "showInitialTopUp", "showInitialTopUpChoice", "showKYCDocumentScan", "isFallback", "showKYCOnboardingPage1", "Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;", "showKYCOnboardingPage2", "showKYCOnboardingPage3", "showKYCPending", "showKYCRequest", "showKYCSubmitted", "showKYCVerified", "showKYCVideoSelfie", "showKycCountrySelection", "showManualAddress", "showMarketingOptIn", "showMonzoDocs", "showNews", "showP2pOnboardingScreen1", "showP2pOnboardingScreen2", "showPaymentInfo", "Lco/uk/getmondo/api/model/tracking/Impression$PaymentFlowFrom;", "showPhoneEntry", "showPhoneVerify", "showPinContactUs", "showPinDobConfirm", "Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;", "showPinEntry", "pinFrom", "showPinNumberChanged", "showPinSmsConfirm", "showPrivacyPolicy", "showRequestMoney", "showRequestShare", "showSddUpgradeLearnMore", "sddUpgradeLevelType", "Lco/uk/getmondo/signup/identity_verification/sdd/SddUpgradeLevelType;", "showSddUpgradeLevel1", "showSddUpgradeLevel2", "showSddUpgradeLevel3", "showSearch", "showSettings", "showShareChooser", "showShareMonzoMeLink", "showSignupPending", "showSingupRejected", "showSpending", "showSpendingCategoryMerchants", "yearMonth", "Lorg/threeten/bp/YearMonth;", "showSpendingCategoryTransactions", "showSpendingMerchantTransactions", "showSpendingReport", "showTaxResidencyCountry", "showTaxResidencyNumber", "showTaxResidencySummary", "showTaxResidencyUs", "showTermsAndConditions", "showTflInfo", "showTopAccountScreen", "showTopScreen", "showTopScreenNonUk", "showTransactionCategory", "showUSDRestaurantInfo", "showWebView", "url", "showWelcomeToMonzo", "signUpAddressEditTap", "signUpDateOfBirthEditTap", "signUpNameEditTap", "signupCardOnItsWay", "singleImpression", "name", "data", "Lco/uk/getmondo/api/model/tracking/Data;", "spendingReportFeedback", "Lco/uk/getmondo/feed/SpendingReportFeedbackDialogFragment$FeedbackType;", "splitCostCustomiseTapped", "splitCostWithTapped", "numberPeople", "takePhotoTapped", "tapPinSmsSend", "threeDSecureComplete", "threeDSecureEnter", "threeDSecureFail", "threeDSecurePageView", "threeDSecureSuccess", "threeDSecureWebError", "redirectUrl", "errorType", "toggleMagStripe", "enabled", "(Ljava/lang/Boolean;)Lco/uk/getmondo/api/model/tracking/Impression;", "toggleNotifications", "togglePayments", "topUpBankTransferShareSheetShow", "topUpBankTransferShow", "topUpBankTransferTap", "topUpError", "description", "topUpNewCardShow", "topUpSavedCardShow", "topUpShow", "androidPayEnabled", "topUpSuccess", "Lco/uk/getmondo/api/model/tracking/Impression$TopUpSuccessType;", "topUpTap", "cameFrom", "Lco/uk/getmondo/api/model/tracking/Impression$TopUpTapFrom;", "trackShareSheetAppUsed", "analyticName", "appName", "waitList", "waitListFirstTime", "webViewClose", "webViewExternalLink", "webViewShare", "welcomeTour", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class Companion {
      private Companion() {
      }

      // $FF: synthetic method
      public Companion(i var1) {
         this();
      }

      // $FF: synthetic method
      public static Impression a(Impression.Companion var0, String var1, String var2, int var3, Object var4) {
         if((var3 & 1) != 0) {
            var1 = "your_spend";
         }

         return var0.e(var1, var2);
      }

      public final Impression A() {
         return ((Impression.Companion)this).n("signup.phone-entry.show");
      }

      public final Impression B() {
         return ((Impression.Companion)this).n("signup.phone-verify.show");
      }

      public final Impression C() {
         return ((Impression.Companion)this).n("signup.identity-verification.show");
      }

      public final Impression D() {
         return ((Impression.Companion)this).n("signup.name-edit.tap");
      }

      public final Impression E() {
         return ((Impression.Companion)this).n("signup.dob-edit.tap");
      }

      public final Impression F() {
         return ((Impression.Companion)this).n("signup.address-edit.tap");
      }

      public final Impression G() {
         return ((Impression.Companion)this).a("signup.initial-topup-choice.show", Data.Companion.a(100));
      }

      public final Impression H() {
         return ((Impression.Companion)this).a("signup.topup.show", Data.Companion.a(100));
      }

      public final Impression I() {
         return ((Impression.Companion)this).n("signup.card-on-the-way.show");
      }

      public final Impression J() {
         return ((Impression.Companion)this).n("signup.card-activation.show");
      }

      public final Impression K() {
         return ((Impression.Companion)this).n("kyc-selfie-rec.show");
      }

      public final Impression L() {
         return ((Impression.Companion)this).n("kyc-selfie-ok.tap");
      }

      public final Impression M() {
         return ((Impression.Companion)this).n("kyc-verified.show");
      }

      public final Impression N() {
         return ((Impression.Companion)this).n("replacement-card.tap");
      }

      public final Impression O() {
         return ((Impression.Companion)this).n("card-replacement.address-picker.show");
      }

      public final Impression P() {
         return ((Impression.Companion)this).n("card-replacement.address-add.tap");
      }

      public final Impression Q() {
         return ((Impression.Companion)this).n("replacement-order.tap");
      }

      public final Impression R() {
         return ((Impression.Companion)this).n("new-card-activation.show");
      }

      public final Impression S() {
         return ((Impression.Companion)this).n("your-spending.show");
      }

      public final Impression T() {
         return ((Impression.Companion)this).n("transaction.split-cost.tap");
      }

      public final Impression U() {
         return ((Impression.Companion)this).a("split-cost.tap", Data.Companion.m("custom"));
      }

      public final Impression V() {
         return ((Impression.Companion)this).n("settings.open");
      }

      public final Impression W() {
         return ((Impression.Companion)this).n("p2p.disabled.show");
      }

      public final Impression X() {
         return ((Impression.Companion)this).n("p2p.explained.show");
      }

      public final Impression Y() {
         return ((Impression.Companion)this).n("p2p.contacts-priming.request");
      }

      public final Impression Z() {
         return ((Impression.Companion)this).a("p2p.invite.send", Data.Companion.g("contacts_on_monzo"));
      }

      public final Impression a(int var1) {
         return ((Impression.Companion)this).a("login.show", Data.Companion.a(Integer.valueOf(var1)));
      }

      public final Impression a(int var1, int var2) {
         return ((Impression.Companion)this).a("p2p.enabled.show", Data.Companion.a(var1, var2));
      }

      public final Impression a(int var1, String var2, String var3, b var4) {
         l.b(var2, "path");
         return ((Impression.Companion)this).a("error", Data.Companion.a(var1, var2, var3, var4));
      }

      public final Impression a(VerificationType var1) {
         l.b(var1, "verificationType");
         return ((Impression.Companion)this).a("limits.show", Data.Companion.l(var1.a()));
      }

      public final Impression a(Impression.CustomiseMonzoMeLinkFrom var1) {
         l.b(var1, "entryPoint");
         return ((Impression.Companion)this).a("request-custom.show", Data.Companion.a(var1));
      }

      public final Impression a(Impression.FingerprintSupportType var1, int var2, boolean var3) {
         l.b(var1, "fingerprintSupportType");
         Data var4 = new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, var1.a(), Integer.valueOf(var2), Boolean.valueOf(var3), (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -234881025, 67108863, (i)null);
         return ((Impression.Companion)this).a("app.init", var4);
      }

      public final Impression a(Impression.HelpOutcome var1, String var2) {
         l.b(var1, "outcome");
         l.b(var2, "topicId");
         return ((Impression.Companion)this).a("help-v2.outcome", new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, var2, var1.a(), (String)null, (String)null, -1, 54525951, (i)null));
      }

      public final Impression a(Impression.IntercomFrom var1, String var2, String var3) {
         l.b(var1, "from");
         return ((Impression.Companion)this).a("intercom.open", new Data((Integer)null, var1.a(), (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, var2, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, var3, (String)null, (String)null, (String)null, -65539, 62914559, (i)null));
      }

      public final Impression a(Impression.KycFrom var1) {
         l.b(var1, "from");
         return ((Impression.Companion)this).a("kyc-tour-1.show", Data.Companion.i(var1.b()));
      }

      public final Impression a(Impression.KycFrom var1, boolean var2) {
         l.b(var1, "from");
         return ((Impression.Companion)this).a("kyc-done.show", new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, var1.b(), (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, Boolean.valueOf(var2), (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -2097153, 66846719, (i)null));
      }

      public final Impression a(Impression.OpenedCommunityFrom var1) {
         l.b(var1, "openedFrom");
         return ((Impression.Companion)this).a("community.open", Data.Companion.b(var1.a()));
      }

      public final Impression a(Impression.PaymentFlowFrom var1) {
         l.b(var1, "entryPoint");
         return ((Impression.Companion)this).a("p2p.form.show", Data.Companion.i(var1.a()));
      }

      public final Impression a(Impression.PinFrom var1) {
         l.b(var1, "from");
         return ((Impression.Companion)this).a("pin-dob-confirm.show", Data.Companion.i(var1.a()));
      }

      public final Impression a(Impression.TopUpSuccessType var1) {
         l.b(var1, "type");
         return ((Impression.Companion)this).a("topup_success", Data.Companion.g(var1.a()));
      }

      public final Impression a(Impression.TopUpTapFrom var1) {
         l.b(var1, "cameFrom");
         return ((Impression.Companion)this).a("topup.tap", Data.Companion.f(var1.a()));
      }

      public final Impression a(h var1) {
         l.b(var1, "category");
         return ((Impression.Companion)this).a("p2p.categorise", Data.Companion.a(var1));
      }

      public final Impression a(h var1, h var2) {
         l.b(var1, "oldCategory");
         l.b(var2, "newCategory");
         Impression.Companion var3 = (Impression.Companion)this;
         Data.Companion var4 = Data.Companion;
         String var5 = var1.f();
         l.a(var5, "oldCategory.apiValue");
         String var6 = var2.f();
         l.a(var6, "newCategory.apiValue");
         return var3.a("transaction.category.save", var4.a(var5, var6));
      }

      public final Impression a(h var1, YearMonth var2) {
         l.b(var1, "category");
         l.b(var2, "yearMonth");
         return ((Impression.Companion)this).a("spend-category.show", Data.Companion.a(var1, var2));
      }

      public final Impression a(SpendingReportFeedbackDialogFragment.a var1, YearMonth var2) {
         l.b(var1, "type");
         l.b(var2, "yearMonth");
         Data var3 = new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, var1.a(), var2.toString(), (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -1, 67010559, (i)null);
         return ((Impression.Companion)this).a("spending-report.feedback", var3);
      }

      public final Impression a(j var1) {
         l.b(var1, "sddUpgradeLevelType");
         return ((Impression.Companion)this).a("kyc-june26-explained", Data.Companion.i(var1.b()));
      }

      public final Impression a(Boolean var1) {
         return ((Impression.Companion)this).a("profile-magstripe-atm.toggle", Data.Companion.a(var1));
      }

      public final Impression a(String var1) {
         l.b(var1, "url");
         return ((Impression.Companion)this).a("wl.webview.show", Data.Companion.a(var1));
      }

      public final Impression a(String var1, Data var2) {
         l.b(var1, "name");
         l.b(var2, "data");
         Impression var3 = new Impression((List)null, 1, (i)null);
         var3.a(new ImpressionEvent(var1, var2));
         return var3;
      }

      public final Impression a(String var1, String var2) {
         l.b(var1, "logoutReason");
         l.b(var2, "userId");
         return ((Impression.Companion)this).a("logout.auto", Data.Companion.b(var1, var2));
      }

      public final Impression a(String var1, boolean var2) {
         l.b(var1, "identityDocumentType");
         return ((Impression.Companion)this).a("kyc-document-scan.show", new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, var1, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, Boolean.valueOf(var2), (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -4097, 66846719, (i)null));
      }

      public final Impression a(YearMonth var1) {
         l.b(var1, "yearMonth");
         return ((Impression.Companion)this).a("spending-report.show", new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, var1.toString(), (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -1, 67043327, (i)null));
      }

      public final Impression a(boolean var1) {
         return ((Impression.Companion)this).a("topup.show", new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, Boolean.valueOf(var1), (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -1, 66977791, (i)null));
      }

      public final Impression a(boolean var1, Long var2, Long var3) {
         return ((Impression.Companion)this).a("kyc-video-processed", Data.Companion.a(var1, var2, var3));
      }

      public final Impression a(boolean var1, String var2) {
         l.b(var2, "url");
         Data var3 = new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, var2, (Integer)null, (Integer)null, (String)null, ((Impression.Companion)this).g(var1), (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -545, 67108863, (i)null);
         return ((Impression.Companion)this).a("topup-3dsecure.pageview", var3);
      }

      public final Impression a(boolean var1, boolean var2, boolean var3) {
         return ((Impression.Companion)this).a("kyc-acc-verification.show", Data.Companion.a(var1, var2, var3));
      }

      public final Impression a(String... var1) {
         l.b(var1, "names");
         Impression var3 = new Impression((List)null, 1, (i)null);

         for(int var2 = 0; var2 < var1.length; ++var2) {
            var3.a(new ImpressionEvent(var1[var2], (Data)null, 2, (i)null));
         }

         return var3;
      }

      public final String a() {
         return Impression.EVENT_WAILIST_SHOW;
      }

      public final Impression aA() {
         return ((Impression.Companion)this).n("kyc-june26-warning.show");
      }

      public final Impression aB() {
         return ((Impression.Companion)this).n("kyc-june26-blocked.show");
      }

      public final Impression aC() {
         return ((Impression.Companion)this).n("contacts.show");
      }

      public final Impression aD() {
         return ((Impression.Companion)this).n("gatca-one-last-thing.show");
      }

      public final Impression aE() {
         return ((Impression.Companion)this).n("gatca-us.show");
      }

      public final Impression aF() {
         return ((Impression.Companion)this).n("gatca-country-picker.show");
      }

      public final Impression aG() {
         return ((Impression.Companion)this).n("gatca-country-form.show");
      }

      public final Impression aH() {
         return ((Impression.Companion)this).n("marketing-opt-in.show");
      }

      public final Impression aI() {
         return ((Impression.Companion)this).n("monzo-docs.show");
      }

      public final Impression aJ() {
         return ((Impression.Companion)this).n("signup-terms-conditions.tap");
      }

      public final Impression aK() {
         return ((Impression.Companion)this).n("signup-privacy-policy.tap");
      }

      public final Impression aL() {
         return ((Impression.Companion)this).n("signup-fscs.tap");
      }

      public final Impression aM() {
         return ((Impression.Companion)this).n("feeditem-current-account-welcome.show");
      }

      public final Impression aN() {
         return ((Impression.Companion)this).n("address-change-postcode.show");
      }

      public final Impression aO() {
         return ((Impression.Companion)this).n("address-change-addresslist.show");
      }

      public final Impression aP() {
         return ((Impression.Companion)this).n("address-change-confirm.show");
      }

      public final Impression aQ() {
         return ((Impression.Companion)this).n("signup-card-details-confirmation.show");
      }

      public final Impression aR() {
         return ((Impression.Companion)this).n("signup.card-on-the-way.show");
      }

      public final Impression aS() {
         return ((Impression.Companion)this).n("signup-card-order-pin-entry.show");
      }

      public final Impression aT() {
         return ((Impression.Companion)this).n("signup-card-order-pin-verify.show");
      }

      public final Impression aU() {
         return ((Impression.Companion)this).n("signup-card-order-pin-verify-incorrect.show");
      }

      public final Impression aV() {
         return ((Impression.Companion)this).n("signup-pending.show");
      }

      public final Impression aW() {
         return ((Impression.Companion)this).n("signup-rejected.show");
      }

      public final Impression aX() {
         return ((Impression.Companion)this).n("migration-announcement-banner.show");
      }

      public final Impression aY() {
         return ((Impression.Companion)this).n("migration-announcement-details.show");
      }

      public final Impression aZ() {
         return ((Impression.Companion)this).n("migration-announcement-notify.tap");
      }

      public final Impression aa() {
         return ((Impression.Companion)this).a("p2p.invite.send", Data.Companion.g("all_contacts"));
      }

      public final Impression ab() {
         return ((Impression.Companion)this).n("p2p.add-contact.show");
      }

      public final Impression ac() {
         return ((Impression.Companion)this).n("pin-contactus.show");
      }

      public final Impression ad() {
         return ((Impression.Companion)this).n("pin-number-changed.show");
      }

      public final Impression ae() {
         return ((Impression.Companion)this).n("pin-sms-send.tap");
      }

      public final Impression af() {
         return ((Impression.Companion)this).n("pin-sms-confirm.show");
      }

      public final Impression ag() {
         return ((Impression.Companion)this).n("force-upgrade.show");
      }

      public final Impression ah() {
         return ((Impression.Companion)this).n("profile-edit.tap");
      }

      public final Impression ai() {
         return ((Impression.Companion)this).n("profile-about.tap");
      }

      public final Impression aj() {
         return ((Impression.Companion)this).n("profile-terms-conditions.tap");
      }

      public final Impression ak() {
         return ((Impression.Companion)this).n("profile-privacy-policy.tap");
      }

      public final Impression al() {
         return ((Impression.Companion)this).n("profile-fscs.tap");
      }

      public final Impression am() {
         return ((Impression.Companion)this).n("profile-open-source.tap");
      }

      public final Impression an() {
         return ((Impression.Companion)this).n("logout.tap");
      }

      public final Impression ao() {
         return ((Impression.Companion)this).n("close-account.tap");
      }

      public final Impression ap() {
         return ((Impression.Companion)this).n("settings.show");
      }

      public final Impression aq() {
         return ((Impression.Companion)this).n("payment-limits.show");
      }

      public final Impression ar() {
         return ((Impression.Companion)this).n("p2p.sdd-error.show");
      }

      public final Impression as() {
         return ((Impression.Companion)this).n("request-custom-share-sheet.show");
      }

      public final Impression at() {
         return ((Impression.Companion)this).n("request.show");
      }

      public final Impression au() {
         return ((Impression.Companion)this).n("request.share-sheet.show");
      }

      public final Impression av() {
         return ((Impression.Companion)this).n("feeditem-edd.show");
      }

      public final Impression aw() {
         return ((Impression.Companion)this).n("search.show");
      }

      public final Impression ax() {
         return ((Impression.Companion)this).n("search.dismiss");
      }

      public final Impression ay() {
         return ((Impression.Companion)this).n("news.show");
      }

      public final Impression az() {
         return ((Impression.Companion)this).n("kyc-june26.show");
      }

      public final Impression b() {
         return ((Impression.Companion)this).n("app.foreground");
      }

      public final Impression b(int var1) {
         return ((Impression.Companion)this).a("welcome-tour.show", Data.Companion.a(Integer.valueOf(var1)));
      }

      public final Impression b(int var1, String var2, String var3, b var4) {
         l.b(var2, "path");
         return ((Impression.Companion)this).a("reported_error", Data.Companion.a(var1, var2, var3, var4));
      }

      public final Impression b(Impression.KycFrom var1) {
         l.b(var1, "from");
         return ((Impression.Companion)this).a("kyc-tour-2.show", Data.Companion.i(var1.b()));
      }

      public final Impression b(Impression.PinFrom var1) {
         l.b(var1, "pinFrom");
         return ((Impression.Companion)this).a("pin.show", new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, var1.a(), (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -2097153, 67108863, (i)null));
      }

      public final Impression b(h var1, YearMonth var2) {
         l.b(var1, "category");
         l.b(var2, "yearMonth");
         return ((Impression.Companion)this).a("spend-merchants.show", Data.Companion.a(var1, var2));
      }

      public final Impression b(Boolean var1) {
         return ((Impression.Companion)this).a("profile-payments.toggle", Data.Companion.a(var1));
      }

      public final Impression b(String var1) {
         l.b(var1, "url");
         return ((Impression.Companion)this).a("wl.webview-close.tap", Data.Companion.a(var1));
      }

      public final Impression b(String var1, String var2) {
         l.b(var1, "redirectUrl");
         l.b(var2, "errorType");
         return ((Impression.Companion)this).a("topup-3dsecure.pageview.error", new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, var1, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, var2, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -33, 67108735, (i)null));
      }

      public final Impression b(String var1, boolean var2) {
         l.b(var1, "reason");
         return ((Impression.Companion)this).a("kyc-video-error", Data.Companion.a(var1, var2));
      }

      public final Impression b(boolean var1) {
         return ((Impression.Companion)this).a("topup-3dsecure.enter", Data.Companion.h(((Impression.Companion)this).g(var1)));
      }

      public final Impression ba() {
         return ((Impression.Companion)this).n("migration-invitation-banner.show");
      }

      public final Impression bb() {
         return ((Impression.Companion)this).n("migration-invitation-details.show");
      }

      public final Impression bc() {
         return ((Impression.Companion)this).n("migration-invitation-learn-more.tap");
      }

      public final Impression bd() {
         return ((Impression.Companion)this).n("migration-start.tap");
      }

      public final Impression be() {
         return ((Impression.Companion)this).n("migration-intro-steps.show");
      }

      public final Impression bf() {
         return ((Impression.Companion)this).n("migration-intro-package.show");
      }

      public final Impression bg() {
         return ((Impression.Companion)this).n("migration-continue-banner.show");
      }

      public final Impression bh() {
         return ((Impression.Companion)this).n("migration-continue-banner.tap");
      }

      public final Impression c() {
         return ((Impression.Companion)this).n("app.background");
      }

      public final Impression c(int var1) {
         return ((Impression.Companion)this).a("split-cost.tap", Data.Companion.m(String.valueOf(var1)));
      }

      public final Impression c(int var1, String var2, String var3, b var4) {
         l.b(var2, "path");
         return ((Impression.Companion)this).a("logout_error", Data.Companion.a(var1, var2, var3, var4));
      }

      public final Impression c(Impression.KycFrom var1) {
         l.b(var1, "from");
         return ((Impression.Companion)this).a("kyc-tour-3.show", Data.Companion.i(var1.b()));
      }

      public final Impression c(h var1, YearMonth var2) {
         l.b(var1, "category");
         l.b(var2, "yearMonth");
         return ((Impression.Companion)this).a("spend-merchants-list.show", Data.Companion.a(var1, var2));
      }

      public final Impression c(Boolean var1) {
         return ((Impression.Companion)this).a("profile-notifications.toggle", Data.Companion.a(var1));
      }

      public final Impression c(String var1) {
         l.b(var1, "url");
         return ((Impression.Companion)this).a("wl.webview-share-sheet.tap", Data.Companion.a(var1));
      }

      public final Impression c(String var1, String var2) {
         l.b(var1, "analyticName");
         l.b(var2, "appName");
         return ((Impression.Companion)this).a(var1, new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, var2, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -1, 67108862, (i)null));
      }

      public final Impression c(boolean var1) {
         return ((Impression.Companion)this).a("topup-3dsecure.fail", Data.Companion.h(((Impression.Companion)this).g(var1)));
      }

      public final Impression d() {
         return ((Impression.Companion)this).a(new String[]{"wl.welcome.show", ((Impression.Companion)this).a()});
      }

      public final Impression d(int var1) {
         return ((Impression.Companion)this).a("request-tour.show", Data.Companion.a(Integer.valueOf(var1)));
      }

      public final Impression d(String var1) {
         l.b(var1, "url");
         return ((Impression.Companion)this).a("wl.webview-external-link.tap", Data.Companion.a(var1));
      }

      public final Impression d(String var1, String var2) {
         l.b(var1, "type");
         return ((Impression.Companion)this).a("app.notification-received", new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, var1, var2, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -1, 65535999, (i)null));
      }

      public final Impression d(boolean var1) {
         return ((Impression.Companion)this).a("topup-3dsecure.success", Data.Companion.h(((Impression.Companion)this).g(var1)));
      }

      public final Impression e() {
         return ((Impression.Companion)this).n(((Impression.Companion)this).a());
      }

      public final Impression e(String var1) {
         l.b(var1, "description");
         return ((Impression.Companion)this).a("topup_error", Data.Companion.h(var1));
      }

      public final Impression e(String var1, String var2) {
         l.b(var1, "from");
         l.b(var2, "fileType");
         return ((Impression.Companion)this).a("export-data.tap", new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, var1, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, var2, -2097153, 33554431, (i)null));
      }

      public final Impression e(boolean var1) {
         return ((Impression.Companion)this).a("topup-3dsecure.complete", Data.Companion.h(((Impression.Companion)this).g(var1)));
      }

      public final Impression f() {
         return ((Impression.Companion)this).n("wl.name-dob.show");
      }

      public final Impression f(String var1) {
         l.b(var1, "transactionId");
         return ((Impression.Companion)this).a("transaction.show", Data.Companion.c(var1));
      }

      public final Impression f(boolean var1) {
         return ((Impression.Companion)this).a("kyc-selfie.show", new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, Boolean.valueOf(var1), (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -1, 66846719, (i)null));
      }

      public final Impression g() {
         return ((Impression.Companion)this).n("login.confirm.show");
      }

      public final Impression g(String var1) {
         l.b(var1, "transactionId");
         return ((Impression.Companion)this).a("full-map.show", Data.Companion.c(var1));
      }

      public final String g(boolean var1) {
         String var2;
         if(var1) {
            var2 = "initial_top_up";
         } else {
            var2 = "top_up";
         }

         return var2;
      }

      public final Impression h() {
         return ((Impression.Companion)this).n("login.mail.tap");
      }

      public final Impression h(String var1) {
         l.b(var1, "referrer");
         return ((Impression.Companion)this).a("app.play-store-referrer", Data.Companion.d(var1));
      }

      public final Impression h(boolean var1) {
         return ((Impression.Companion)this).a("p2p.contacts-access.request", Data.Companion.a(var1));
      }

      public final Impression i() {
         return ((Impression.Companion)this).n("login.nomail.tap");
      }

      public final Impression i(String var1) {
         l.b(var1, "redirectId");
         return ((Impression.Companion)this).a("app.play-store-redirect-event-id-received", Data.Companion.e(var1));
      }

      public final Impression i(boolean var1) {
         return ((Impression.Companion)this).a("p2p.authenticate", Data.Companion.b(var1));
      }

      public final Impression j() {
         return ((Impression.Companion)this).n("wl.invite-friends.show");
      }

      public final Impression j(String var1) {
         l.b(var1, "identityDocumentType");
         return ((Impression.Companion)this).a("kyc-document-ok.tap", new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, var1, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -4097, 67108863, (i)null));
      }

      public final Impression j(boolean var1) {
         return ((Impression.Companion)this).a("golden-ticket.show", Data.Companion.c(var1));
      }

      public final Impression k() {
         return ((Impression.Companion)this).n("wl.share-sheet.show");
      }

      public final Impression k(String var1) {
         l.b(var1, "identityDocumentType");
         return ((Impression.Companion)this).a("kyc-document-country.show", new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, var1, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -4097, 67108863, (i)null));
      }

      public final Impression k(boolean var1) {
         return ((Impression.Companion)this).a("golden-ticket.share-sheet.show", Data.Companion.c(var1));
      }

      public final Impression l() {
         return ((Impression.Companion)this).a("wl.top.show", Data.Companion.b(Integer.valueOf(1)));
      }

      public final Impression l(String var1) {
         l.b(var1, "advertisingId");
         return ((Impression.Companion)this).a("app.advertising-id-observed", Data.Companion.j(var1));
      }

      public final Impression l(boolean var1) {
         return ((Impression.Companion)this).a("marketing-opt-in", new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, Boolean.valueOf(var1), (String)null, (String)null, (String)null, (String)null, -1, 65011711, (i)null));
      }

      public final Impression m() {
         return ((Impression.Companion)this).a("wl.top.show", Data.Companion.b(Integer.valueOf(0)));
      }

      public final Impression m(String var1) {
         l.b(var1, "shortcutType");
         return ((Impression.Companion)this).a("appshortcut.tap", Data.Companion.k(var1));
      }

      public final Impression m(boolean var1) {
         return ((Impression.Companion)this).a("address-change.authenticate", Data.Companion.b(var1));
      }

      public final Impression n() {
         return ((Impression.Companion)this).n("topup.bank-transfer.show");
      }

      public final Impression n(String var1) {
         l.b(var1, "name");
         Impression var2 = new Impression((List)null, 1, (i)null);
         var2.a(new ImpressionEvent(var1, (Data)null, 2, (i)null));
         return var2;
      }

      public final Impression o() {
         return ((Impression.Companion)this).n("topup.bank-transfer.tap");
      }

      public final Impression o(String var1) {
         return ((Impression.Companion)this).a("help-content.show", new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, var1, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, -65537, 67108863, (i)null));
      }

      public final Impression p() {
         return ((Impression.Companion)this).n("topup.share-sheet.show");
      }

      public final Impression p(String var1) {
         l.b(var1, "selectedNameType");
         return ((Impression.Companion)this).a("signup-card-details-confirmation.tap", new Data((Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (Integer)null, (Boolean)null, (Integer)null, (Integer)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Integer)null, (Boolean)null, (String)null, (Boolean)null, (String)null, (String)null, (String)null, (String)null, (Integer)null, (String)null, (String)null, (String)null, (String)null, (String)null, (Boolean)null, (Long)null, (Long)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (Boolean)null, (String)null, (String)null, (Boolean)null, (String)null, (String)null, var1, (String)null, -1, 50331647, (i)null));
      }

      public final Impression q() {
         return ((Impression.Companion)this).n("topup-confirm.show");
      }

      public final Impression r() {
         return ((Impression.Companion)this).n("topup-new-card.show");
      }

      public final Impression s() {
         return ((Impression.Companion)this).n("card.show");
      }

      public final Impression t() {
         return ((Impression.Companion)this).n("freeze-card.tap");
      }

      public final Impression u() {
         return ((Impression.Companion)this).n("defrost-card.tap");
      }

      public final Impression v() {
         return ((Impression.Companion)this).n("add-note.tap");
      }

      public final Impression w() {
         return ((Impression.Companion)this).n("take-photo.tap");
      }

      public final Impression x() {
         return ((Impression.Companion)this).n("select-photo.tap");
      }

      public final Impression y() {
         return ((Impression.Companion)this).n("signup.address-picker.show");
      }

      public final Impression z() {
         return ((Impression.Companion)this).n("signup.address-manual.show");
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\b\b\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bj\u0002\b\tj\u0002\b\n¨\u0006\u000b"},
      d2 = {"Lco/uk/getmondo/api/model/tracking/Impression$CustomiseMonzoMeLinkFrom;", "", "value", "", "(Ljava/lang/String;ILjava/lang/String;)V", "getValue", "()Ljava/lang/String;", "setValue", "(Ljava/lang/String;)V", "REQUEST_MONEY", "SPLIT_COST", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static enum CustomiseMonzoMeLinkFrom {
      REQUEST_MONEY,
      SPLIT_COST;

      private String value;

      static {
         Impression.CustomiseMonzoMeLinkFrom var0 = new Impression.CustomiseMonzoMeLinkFrom("REQUEST_MONEY", 0, "request_money");
         REQUEST_MONEY = var0;
         Impression.CustomiseMonzoMeLinkFrom var1 = new Impression.CustomiseMonzoMeLinkFrom("SPLIT_COST", 1, "split_cost");
         SPLIT_COST = var1;
      }

      protected CustomiseMonzoMeLinkFrom(String var3) {
         l.b(var3, "value");
         super(var1, var2);
         this.value = var3;
      }

      public final String a() {
         return this.value;
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\b\t\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u001a\u0010\u0002\u001a\u00020\u0003X\u0080\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bj\u0002\b\tj\u0002\b\nj\u0002\b\u000b¨\u0006\f"},
      d2 = {"Lco/uk/getmondo/api/model/tracking/Impression$FingerprintSupportType;", "", "value", "", "(Ljava/lang/String;ILjava/lang/String;)V", "getValue$app_monzoPrepaidRelease", "()Ljava/lang/String;", "setValue$app_monzoPrepaidRelease", "(Ljava/lang/String;)V", "READY_TO_USE", "NO_ENROLLED_FINGERPRINTS", "NO_HARDWARE_DETECTED", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static enum FingerprintSupportType {
      NO_ENROLLED_FINGERPRINTS,
      NO_HARDWARE_DETECTED,
      READY_TO_USE;

      private String value;

      static {
         Impression.FingerprintSupportType var1 = new Impression.FingerprintSupportType("READY_TO_USE", 0, "ready_to_use");
         READY_TO_USE = var1;
         Impression.FingerprintSupportType var2 = new Impression.FingerprintSupportType("NO_ENROLLED_FINGERPRINTS", 1, "no_enrolled_fingerprints");
         NO_ENROLLED_FINGERPRINTS = var2;
         Impression.FingerprintSupportType var0 = new Impression.FingerprintSupportType("NO_HARDWARE_DETECTED", 2, "no_hardware");
         NO_HARDWARE_DETECTED = var0;
      }

      protected FingerprintSupportType(String var3) {
         l.b(var3, "value");
         super(var1, var2);
         this.value = var3;
      }

      public final String a() {
         return this.value;
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0014\u0010\u0002\u001a\u00020\u0003X\u0080\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\u0007j\u0002\b\b¨\u0006\t"},
      d2 = {"Lco/uk/getmondo/api/model/tracking/Impression$HelpOutcome;", "", "value", "", "(Ljava/lang/String;ILjava/lang/String;)V", "getValue$app_monzoPrepaidRelease", "()Ljava/lang/String;", "THUMBS_UP", "THUMBS_DOWN", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static enum HelpOutcome {
      THUMBS_DOWN,
      THUMBS_UP;

      private final String value;

      static {
         Impression.HelpOutcome var0 = new Impression.HelpOutcome("THUMBS_UP", 0, "thumbs-up.press");
         THUMBS_UP = var0;
         Impression.HelpOutcome var1 = new Impression.HelpOutcome("THUMBS_DOWN", 1, "thumbs-down.press");
         THUMBS_DOWN = var1;
      }

      protected HelpOutcome(String var3) {
         l.b(var3, "value");
         super(var1, var2);
         this.value = var3;
      }

      public final String a() {
         return this.value;
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\b\f\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u001a\u0010\u0002\u001a\u00020\u0003X\u0080\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bj\u0002\b\tj\u0002\b\nj\u0002\b\u000bj\u0002\b\fj\u0002\b\rj\u0002\b\u000e¨\u0006\u000f"},
      d2 = {"Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;", "", "value", "", "(Ljava/lang/String;ILjava/lang/String;)V", "getValue$app_monzoPrepaidRelease", "()Ljava/lang/String;", "setValue$app_monzoPrepaidRelease", "(Ljava/lang/String;)V", "MAIN_NAV", "INITIAL_TOPUP", "GOOD_TO_GO", "PIN", "ADDRESS_CHANGE", "HELP", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static enum IntercomFrom {
      ADDRESS_CHANGE,
      GOOD_TO_GO,
      HELP,
      INITIAL_TOPUP,
      MAIN_NAV,
      PIN;

      private String value;

      static {
         Impression.IntercomFrom var3 = new Impression.IntercomFrom("MAIN_NAV", 0, "main_nav");
         MAIN_NAV = var3;
         Impression.IntercomFrom var5 = new Impression.IntercomFrom("INITIAL_TOPUP", 1, "initial_topup");
         INITIAL_TOPUP = var5;
         Impression.IntercomFrom var2 = new Impression.IntercomFrom("GOOD_TO_GO", 2, "good_to_go");
         GOOD_TO_GO = var2;
         Impression.IntercomFrom var1 = new Impression.IntercomFrom("PIN", 3, "pin");
         PIN = var1;
         Impression.IntercomFrom var4 = new Impression.IntercomFrom("ADDRESS_CHANGE", 4, "address-change");
         ADDRESS_CHANGE = var4;
         Impression.IntercomFrom var0 = new Impression.IntercomFrom("HELP", 5, "help");
         HELP = var0;
      }

      protected IntercomFrom(String var3) {
         l.b(var3, "value");
         super(var1, var2);
         this.value = var3;
      }

      public final String a() {
         return this.value;
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u000b\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0011\u0010\u0005\u001a\u00020\u00068F¢\u0006\u0006\u001a\u0004\b\u0005\u0010\u0007R\u0014\u0010\u0002\u001a\u00020\u0003X\u0080\u0004¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tj\u0002\b\nj\u0002\b\u000bj\u0002\b\fj\u0002\b\rj\u0002\b\u000ej\u0002\b\u000fj\u0002\b\u0010¨\u0006\u0011"},
      d2 = {"Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;", "", "value", "", "(Ljava/lang/String;ILjava/lang/String;)V", "isSdd", "", "()Z", "getValue$app_monzoPrepaidRelease", "()Ljava/lang/String;", "FEED", "LIMITS", "REQUEST", "SIGNUP", "KYC_SDD_ENCOURAGE", "KYC_SDD_WARNING", "KYC_SDD_BLOCKED", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static enum KycFrom {
      FEED,
      KYC_SDD_BLOCKED,
      KYC_SDD_ENCOURAGE,
      KYC_SDD_WARNING,
      LIMITS,
      REQUEST,
      SIGNUP;

      private final String value;

      static {
         Impression.KycFrom var1 = new Impression.KycFrom("FEED", 0, "feed");
         FEED = var1;
         Impression.KycFrom var0 = new Impression.KycFrom("LIMITS", 1, "limits");
         LIMITS = var0;
         Impression.KycFrom var3 = new Impression.KycFrom("REQUEST", 2, "request");
         REQUEST = var3;
         Impression.KycFrom var5 = new Impression.KycFrom("SIGNUP", 3, "signup");
         SIGNUP = var5;
         Impression.KycFrom var2 = new Impression.KycFrom("KYC_SDD_ENCOURAGE", 4, "kyc_june26");
         KYC_SDD_ENCOURAGE = var2;
         Impression.KycFrom var4 = new Impression.KycFrom("KYC_SDD_WARNING", 5, "kyc_june26_warning");
         KYC_SDD_WARNING = var4;
         Impression.KycFrom var6 = new Impression.KycFrom("KYC_SDD_BLOCKED", 6, "kyc_june26_blocked");
         KYC_SDD_BLOCKED = var6;
      }

      protected KycFrom(String var3) {
         l.b(var3, "value");
         super(var1, var2);
         this.value = var3;
      }

      public final boolean a() {
         boolean var1;
         if(l.a((Impression.KycFrom)this, KYC_SDD_ENCOURAGE) && l.a((Impression.KycFrom)this, KYC_SDD_WARNING) && l.a((Impression.KycFrom)this, KYC_SDD_BLOCKED)) {
            var1 = true;
         } else {
            var1 = false;
         }

         return var1;
      }

      public final String b() {
         return this.value;
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\u0007¨\u0006\b"},
      d2 = {"Lco/uk/getmondo/api/model/tracking/Impression$OpenedCommunityFrom;", "", "value", "", "(Ljava/lang/String;ILjava/lang/String;)V", "getValue", "()Ljava/lang/String;", "FROM_MAIN_NAV", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static enum OpenedCommunityFrom {
      FROM_MAIN_NAV;

      private final String value;

      static {
         Impression.OpenedCommunityFrom var0 = new Impression.OpenedCommunityFrom("FROM_MAIN_NAV", 0, "main_nav");
         FROM_MAIN_NAV = var0;
      }

      protected OpenedCommunityFrom(String var3) {
         l.b(var3, "value");
         super(var1, var2);
         this.value = var3;
      }

      public final String a() {
         return this.value;
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\b\t\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u001a\u0010\u0002\u001a\u00020\u0003X\u0080\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bj\u0002\b\tj\u0002\b\nj\u0002\b\u000b¨\u0006\f"},
      d2 = {"Lco/uk/getmondo/api/model/tracking/Impression$PaymentFlowFrom;", "", "value", "", "(Ljava/lang/String;ILjava/lang/String;)V", "getValue$app_monzoPrepaidRelease", "()Ljava/lang/String;", "setValue$app_monzoPrepaidRelease", "(Ljava/lang/String;)V", "FROM_CONTACT_DISCOVERY", "FROM_TRANSACTION_DETAIL", "FROM_MONZO_ME", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static enum PaymentFlowFrom {
      FROM_CONTACT_DISCOVERY,
      FROM_MONZO_ME,
      FROM_TRANSACTION_DETAIL;

      private String value;

      static {
         Impression.PaymentFlowFrom var0 = new Impression.PaymentFlowFrom("FROM_CONTACT_DISCOVERY", 0, "contacts");
         FROM_CONTACT_DISCOVERY = var0;
         Impression.PaymentFlowFrom var2 = new Impression.PaymentFlowFrom("FROM_TRANSACTION_DETAIL", 1, "transaction");
         FROM_TRANSACTION_DETAIL = var2;
         Impression.PaymentFlowFrom var1 = new Impression.PaymentFlowFrom("FROM_MONZO_ME", 2, "monzome");
         FROM_MONZO_ME = var1;
      }

      protected PaymentFlowFrom(String var3) {
         l.b(var3, "value");
         super(var1, var2);
         this.value = var3;
      }

      public final String a() {
         return this.value;
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\b\b\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u001a\u0010\u0002\u001a\u00020\u0003X\u0080\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bj\u0002\b\tj\u0002\b\n¨\u0006\u000b"},
      d2 = {"Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;", "", "value", "", "(Ljava/lang/String;ILjava/lang/String;)V", "getValue$app_monzoPrepaidRelease", "()Ljava/lang/String;", "setValue$app_monzoPrepaidRelease", "(Ljava/lang/String;)V", "CARD", "ADDRESS_CHANGE", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static enum PinFrom {
      ADDRESS_CHANGE,
      CARD;

      private String value;

      static {
         Impression.PinFrom var0 = new Impression.PinFrom("CARD", 0, "card");
         CARD = var0;
         Impression.PinFrom var1 = new Impression.PinFrom("ADDRESS_CHANGE", 1, "address-change");
         ADDRESS_CHANGE = var1;
      }

      protected PinFrom(String var3) {
         l.b(var3, "value");
         super(var1, var2);
         this.value = var3;
      }

      public final String a() {
         return this.value;
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\b\t\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u001a\u0010\u0002\u001a\u00020\u0003X\u0080\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bj\u0002\b\tj\u0002\b\nj\u0002\b\u000b¨\u0006\f"},
      d2 = {"Lco/uk/getmondo/api/model/tracking/Impression$TopUpSuccessType;", "", "value", "", "(Ljava/lang/String;ILjava/lang/String;)V", "getValue$app_monzoPrepaidRelease", "()Ljava/lang/String;", "setValue$app_monzoPrepaidRelease", "(Ljava/lang/String;)V", "NEW_CARD", "SAVED_CARD", "ANDROID_PAY", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static enum TopUpSuccessType {
      ANDROID_PAY,
      NEW_CARD,
      SAVED_CARD;

      private String value;

      static {
         Impression.TopUpSuccessType var2 = new Impression.TopUpSuccessType("NEW_CARD", 0, "new_card");
         NEW_CARD = var2;
         Impression.TopUpSuccessType var1 = new Impression.TopUpSuccessType("SAVED_CARD", 1, "existing_card");
         SAVED_CARD = var1;
         Impression.TopUpSuccessType var0 = new Impression.TopUpSuccessType("ANDROID_PAY", 2, "android_pay");
         ANDROID_PAY = var0;
      }

      protected TopUpSuccessType(String var3) {
         l.b(var3, "value");
         super(var1, var2);
         this.value = var3;
      }

      public final String a() {
         return this.value;
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\b\t\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u001a\u0010\u0002\u001a\u00020\u0003X\u0080\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bj\u0002\b\tj\u0002\b\nj\u0002\b\u000b¨\u0006\f"},
      d2 = {"Lco/uk/getmondo/api/model/tracking/Impression$TopUpTapFrom;", "", "value", "", "(Ljava/lang/String;ILjava/lang/String;)V", "getValue$app_monzoPrepaidRelease", "()Ljava/lang/String;", "setValue$app_monzoPrepaidRelease", "(Ljava/lang/String;)V", "FROM_NEW_CARD", "FROM_SAVED_CARD", "ANDROID_PAY", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static enum TopUpTapFrom {
      ANDROID_PAY,
      FROM_NEW_CARD,
      FROM_SAVED_CARD;

      private String value;

      static {
         Impression.TopUpTapFrom var1 = new Impression.TopUpTapFrom("FROM_NEW_CARD", 0, "new_card");
         FROM_NEW_CARD = var1;
         Impression.TopUpTapFrom var2 = new Impression.TopUpTapFrom("FROM_SAVED_CARD", 1, "topup_confirm");
         FROM_SAVED_CARD = var2;
         Impression.TopUpTapFrom var0 = new Impression.TopUpTapFrom("ANDROID_PAY", 2, "android_pay");
         ANDROID_PAY = var0;
      }

      protected TopUpTapFrom(String var3) {
         l.b(var3, "value");
         super(var1, var2);
         this.value = var3;
      }

      public final String a() {
         return this.value;
      }
   }
}
