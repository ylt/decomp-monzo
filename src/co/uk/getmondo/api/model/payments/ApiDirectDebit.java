package co.uk.getmondo.api.model.payments;

import kotlin.Metadata;
import kotlin.d.b.l;
import org.threeten.bp.LocalDateTime;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u001f\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B_\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0006\u0010\u0007\u001a\u00020\u0003\u0012\u0006\u0010\b\u001a\u00020\u0003\u0012\u0006\u0010\t\u001a\u00020\u0003\u0012\u0006\u0010\n\u001a\u00020\u0003\u0012\u0006\u0010\u000b\u001a\u00020\f\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\b\u0010\u000f\u001a\u0004\u0018\u00010\u000e¢\u0006\u0002\u0010\u0010J\t\u0010\u001f\u001a\u00020\u0003HÆ\u0003J\t\u0010 \u001a\u00020\u000eHÆ\u0003J\u000b\u0010!\u001a\u0004\u0018\u00010\u000eHÆ\u0003J\t\u0010\"\u001a\u00020\u0003HÆ\u0003J\t\u0010#\u001a\u00020\u0003HÆ\u0003J\t\u0010$\u001a\u00020\u0003HÆ\u0003J\t\u0010%\u001a\u00020\u0003HÆ\u0003J\t\u0010&\u001a\u00020\u0003HÆ\u0003J\t\u0010'\u001a\u00020\u0003HÆ\u0003J\t\u0010(\u001a\u00020\u0003HÆ\u0003J\t\u0010)\u001a\u00020\fHÆ\u0003Jy\u0010*\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u00032\b\b\u0002\u0010\u0007\u001a\u00020\u00032\b\b\u0002\u0010\b\u001a\u00020\u00032\b\b\u0002\u0010\t\u001a\u00020\u00032\b\b\u0002\u0010\n\u001a\u00020\u00032\b\b\u0002\u0010\u000b\u001a\u00020\f2\b\b\u0002\u0010\r\u001a\u00020\u000e2\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÆ\u0001J\u0013\u0010+\u001a\u00020\f2\b\u0010,\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010-\u001a\u00020.HÖ\u0001J\t\u0010/\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u0011\u0010\u000b\u001a\u00020\f¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u0011\u0010\r\u001a\u00020\u000e¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0012R\u0013\u0010\u000f\u001a\u0004\u0018\u00010\u000e¢\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0016R\u0011\u0010\u0006\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u0012R\u0011\u0010\u0007\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u0012R\u0011\u0010\u0005\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u0012R\u0011\u0010\n\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u0012R\u0011\u0010\t\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u0012R\u0011\u0010\b\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u0012¨\u00060"},
   d2 = {"Lco/uk/getmondo/api/model/payments/ApiDirectDebit;", "", "id", "", "accountId", "payerSortCode", "payerAccountNumber", "payerName", "serviceUserNumber", "serviceUserName", "reference", "active", "", "created", "Lorg/threeten/bp/LocalDateTime;", "lastCollected", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/LocalDateTime;)V", "getAccountId", "()Ljava/lang/String;", "getActive", "()Z", "getCreated", "()Lorg/threeten/bp/LocalDateTime;", "getId", "getLastCollected", "getPayerAccountNumber", "getPayerName", "getPayerSortCode", "getReference", "getServiceUserName", "getServiceUserNumber", "component1", "component10", "component11", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "other", "hashCode", "", "toString", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ApiDirectDebit {
   private final String accountId;
   private final boolean active;
   private final LocalDateTime created;
   private final String id;
   private final LocalDateTime lastCollected;
   private final String payerAccountNumber;
   private final String payerName;
   private final String payerSortCode;
   private final String reference;
   private final String serviceUserName;
   private final String serviceUserNumber;

   public ApiDirectDebit(String var1, String var2, String var3, String var4, String var5, String var6, String var7, String var8, boolean var9, LocalDateTime var10, LocalDateTime var11) {
      l.b(var1, "id");
      l.b(var2, "accountId");
      l.b(var3, "payerSortCode");
      l.b(var4, "payerAccountNumber");
      l.b(var5, "payerName");
      l.b(var6, "serviceUserNumber");
      l.b(var7, "serviceUserName");
      l.b(var8, "reference");
      l.b(var10, "created");
      super();
      this.id = var1;
      this.accountId = var2;
      this.payerSortCode = var3;
      this.payerAccountNumber = var4;
      this.payerName = var5;
      this.serviceUserNumber = var6;
      this.serviceUserName = var7;
      this.reference = var8;
      this.active = var9;
      this.created = var10;
      this.lastCollected = var11;
   }

   public final String a() {
      return this.id;
   }

   public final String b() {
      return this.accountId;
   }

   public final String c() {
      return this.payerSortCode;
   }

   public final String d() {
      return this.payerAccountNumber;
   }

   public final String e() {
      return this.payerName;
   }

   public boolean equals(Object var1) {
      boolean var4 = false;
      boolean var3;
      if(this != var1) {
         var3 = var4;
         if(!(var1 instanceof ApiDirectDebit)) {
            return var3;
         }

         ApiDirectDebit var5 = (ApiDirectDebit)var1;
         var3 = var4;
         if(!l.a(this.id, var5.id)) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.accountId, var5.accountId)) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.payerSortCode, var5.payerSortCode)) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.payerAccountNumber, var5.payerAccountNumber)) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.payerName, var5.payerName)) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.serviceUserNumber, var5.serviceUserNumber)) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.serviceUserName, var5.serviceUserName)) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.reference, var5.reference)) {
            return var3;
         }

         boolean var2;
         if(this.active == var5.active) {
            var2 = true;
         } else {
            var2 = false;
         }

         var3 = var4;
         if(!var2) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.created, var5.created)) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.lastCollected, var5.lastCollected)) {
            return var3;
         }
      }

      var3 = true;
      return var3;
   }

   public final String f() {
      return this.serviceUserNumber;
   }

   public final String g() {
      return this.serviceUserName;
   }

   public final String h() {
      return this.reference;
   }

   public int hashCode() {
      throw new RuntimeException("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.copyTypes(TypeTransformer.java:311)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.fixTypes(TypeTransformer.java:226)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:207)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:161)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:433)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:129)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:528)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:425)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:441)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:171)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:271)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:109)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:290)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:33)\n");
   }

   public final boolean i() {
      return this.active;
   }

   public final LocalDateTime j() {
      return this.created;
   }

   public final LocalDateTime k() {
      return this.lastCollected;
   }

   public String toString() {
      return "ApiDirectDebit(id=" + this.id + ", accountId=" + this.accountId + ", payerSortCode=" + this.payerSortCode + ", payerAccountNumber=" + this.payerAccountNumber + ", payerName=" + this.payerName + ", serviceUserNumber=" + this.serviceUserNumber + ", serviceUserName=" + this.serviceUserName + ", reference=" + this.reference + ", active=" + this.active + ", created=" + this.created + ", lastCollected=" + this.lastCollected + ")";
   }
}
