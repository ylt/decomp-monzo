package co.uk.getmondo.api.model.payments;

import kotlin.Metadata;
import kotlin.d.b.l;
import org.threeten.bp.LocalDate;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u001e\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\b\u0086\b\u0018\u00002\u00020\u0001:\u00010BW\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0003\u0012\u0006\u0010\b\u001a\u00020\u0003\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\f\u0012\u0006\u0010\r\u001a\u00020\u0003\u0012\u0006\u0010\u000e\u001a\u00020\f\u0012\b\u0010\u000f\u001a\u0004\u0018\u00010\f¢\u0006\u0002\u0010\u0010J\t\u0010\u001f\u001a\u00020\u0003HÆ\u0003J\u000b\u0010 \u001a\u0004\u0018\u00010\fHÆ\u0003J\t\u0010!\u001a\u00020\u0003HÆ\u0003J\t\u0010\"\u001a\u00020\u0006HÆ\u0003J\t\u0010#\u001a\u00020\u0003HÆ\u0003J\t\u0010$\u001a\u00020\u0003HÆ\u0003J\t\u0010%\u001a\u00020\nHÆ\u0003J\t\u0010&\u001a\u00020\fHÆ\u0003J\t\u0010'\u001a\u00020\u0003HÆ\u0003J\t\u0010(\u001a\u00020\fHÆ\u0003Jo\u0010)\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00062\b\b\u0002\u0010\u0007\u001a\u00020\u00032\b\b\u0002\u0010\b\u001a\u00020\u00032\b\b\u0002\u0010\t\u001a\u00020\n2\b\b\u0002\u0010\u000b\u001a\u00020\f2\b\b\u0002\u0010\r\u001a\u00020\u00032\b\b\u0002\u0010\u000e\u001a\u00020\f2\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\fHÆ\u0001J\u0013\u0010*\u001a\u00020+2\b\u0010,\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010-\u001a\u00020.HÖ\u0001J\t\u0010/\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u0011\u0010\u0007\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0012R\u0013\u0010\u000f\u001a\u0004\u0018\u00010\f¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0012R\u0011\u0010\r\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u0012R\u0011\u0010\u000e\u001a\u00020\f¢\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u0017R\u0011\u0010\b\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u0012R\u0011\u0010\t\u001a\u00020\n¢\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u001dR\u0011\u0010\u000b\u001a\u00020\f¢\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u0017¨\u00061"},
   d2 = {"Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;", "", "id", "", "accountId", "amount", "", "currency", "scheme", "schemeData", "Lco/uk/getmondo/api/model/payments/ApiPaymentSeries$FpsSchemeData;", "startDate", "Lorg/threeten/bp/LocalDate;", "intervalType", "nextIterationDate", "endDate", "(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/payments/ApiPaymentSeries$FpsSchemeData;Lorg/threeten/bp/LocalDate;Ljava/lang/String;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;)V", "getAccountId", "()Ljava/lang/String;", "getAmount", "()J", "getCurrency", "getEndDate", "()Lorg/threeten/bp/LocalDate;", "getId", "getIntervalType", "getNextIterationDate", "getScheme", "getSchemeData", "()Lco/uk/getmondo/api/model/payments/ApiPaymentSeries$FpsSchemeData;", "getStartDate", "component1", "component10", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "", "other", "hashCode", "", "toString", "FpsSchemeData", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ApiPaymentSeries {
   private final String accountId;
   private final long amount;
   private final String currency;
   private final LocalDate endDate;
   private final String id;
   private final String intervalType;
   private final LocalDate nextIterationDate;
   private final String scheme;
   private final ApiPaymentSeries.FpsSchemeData schemeData;
   private final LocalDate startDate;

   public ApiPaymentSeries(String var1, String var2, long var3, String var5, String var6, ApiPaymentSeries.FpsSchemeData var7, LocalDate var8, String var9, LocalDate var10, LocalDate var11) {
      l.b(var1, "id");
      l.b(var2, "accountId");
      l.b(var5, "currency");
      l.b(var6, "scheme");
      l.b(var7, "schemeData");
      l.b(var8, "startDate");
      l.b(var9, "intervalType");
      l.b(var10, "nextIterationDate");
      super();
      this.id = var1;
      this.accountId = var2;
      this.amount = var3;
      this.currency = var5;
      this.scheme = var6;
      this.schemeData = var7;
      this.startDate = var8;
      this.intervalType = var9;
      this.nextIterationDate = var10;
      this.endDate = var11;
   }

   public final String a() {
      return this.id;
   }

   public final String b() {
      return this.accountId;
   }

   public final long c() {
      return this.amount;
   }

   public final String d() {
      return this.currency;
   }

   public final String e() {
      return this.scheme;
   }

   public boolean equals(Object var1) {
      boolean var4 = false;
      boolean var3;
      if(this != var1) {
         var3 = var4;
         if(!(var1 instanceof ApiPaymentSeries)) {
            return var3;
         }

         ApiPaymentSeries var5 = (ApiPaymentSeries)var1;
         var3 = var4;
         if(!l.a(this.id, var5.id)) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.accountId, var5.accountId)) {
            return var3;
         }

         boolean var2;
         if(this.amount == var5.amount) {
            var2 = true;
         } else {
            var2 = false;
         }

         var3 = var4;
         if(!var2) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.currency, var5.currency)) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.scheme, var5.scheme)) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.schemeData, var5.schemeData)) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.startDate, var5.startDate)) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.intervalType, var5.intervalType)) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.nextIterationDate, var5.nextIterationDate)) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.endDate, var5.endDate)) {
            return var3;
         }
      }

      var3 = true;
      return var3;
   }

   public final ApiPaymentSeries.FpsSchemeData f() {
      return this.schemeData;
   }

   public final LocalDate g() {
      return this.startDate;
   }

   public final String h() {
      return this.intervalType;
   }

   public int hashCode() {
      int var9 = 0;
      String var13 = this.id;
      int var1;
      if(var13 != null) {
         var1 = var13.hashCode();
      } else {
         var1 = 0;
      }

      var13 = this.accountId;
      int var2;
      if(var13 != null) {
         var2 = var13.hashCode();
      } else {
         var2 = 0;
      }

      long var11 = this.amount;
      int var10 = (int)(var11 ^ var11 >>> 32);
      var13 = this.currency;
      int var3;
      if(var13 != null) {
         var3 = var13.hashCode();
      } else {
         var3 = 0;
      }

      var13 = this.scheme;
      int var4;
      if(var13 != null) {
         var4 = var13.hashCode();
      } else {
         var4 = 0;
      }

      ApiPaymentSeries.FpsSchemeData var14 = this.schemeData;
      int var5;
      if(var14 != null) {
         var5 = var14.hashCode();
      } else {
         var5 = 0;
      }

      LocalDate var15 = this.startDate;
      int var6;
      if(var15 != null) {
         var6 = var15.hashCode();
      } else {
         var6 = 0;
      }

      var13 = this.intervalType;
      int var7;
      if(var13 != null) {
         var7 = var13.hashCode();
      } else {
         var7 = 0;
      }

      var15 = this.nextIterationDate;
      int var8;
      if(var15 != null) {
         var8 = var15.hashCode();
      } else {
         var8 = 0;
      }

      var15 = this.endDate;
      if(var15 != null) {
         var9 = var15.hashCode();
      }

      return (var8 + (var7 + (var6 + (var5 + (var4 + (var3 + ((var2 + var1 * 31) * 31 + var10) * 31) * 31) * 31) * 31) * 31) * 31) * 31 + var9;
   }

   public final LocalDate i() {
      return this.nextIterationDate;
   }

   public final LocalDate j() {
      return this.endDate;
   }

   public String toString() {
      return "ApiPaymentSeries(id=" + this.id + ", accountId=" + this.accountId + ", amount=" + this.amount + ", currency=" + this.currency + ", scheme=" + this.scheme + ", schemeData=" + this.schemeData + ", startDate=" + this.startDate + ", intervalType=" + this.intervalType + ", nextIterationDate=" + this.nextIterationDate + ", endDate=" + this.endDate + ")";
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u000f\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0003¢\u0006\u0002\u0010\u0007J\t\u0010\r\u001a\u00020\u0003HÆ\u0003J\t\u0010\u000e\u001a\u00020\u0003HÆ\u0003J\t\u0010\u000f\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0010\u001a\u00020\u0003HÆ\u0003J1\u0010\u0011\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\u0012\u001a\u00020\u00132\b\u0010\u0014\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0015\u001a\u00020\u0016HÖ\u0001J\t\u0010\u0017\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0006\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\tR\u0011\u0010\u0005\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\t¨\u0006\u0018"},
      d2 = {"Lco/uk/getmondo/api/model/payments/ApiPaymentSeries$FpsSchemeData;", "", "reference", "", "beneficiaryAccountNumber", "beneficiarySortCode", "beneficiaryCustomerName", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getBeneficiaryAccountNumber", "()Ljava/lang/String;", "getBeneficiaryCustomerName", "getBeneficiarySortCode", "getReference", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "hashCode", "", "toString", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class FpsSchemeData {
      private final String beneficiaryAccountNumber;
      private final String beneficiaryCustomerName;
      private final String beneficiarySortCode;
      private final String reference;

      public FpsSchemeData(String var1, String var2, String var3, String var4) {
         l.b(var1, "reference");
         l.b(var2, "beneficiaryAccountNumber");
         l.b(var3, "beneficiarySortCode");
         l.b(var4, "beneficiaryCustomerName");
         super();
         this.reference = var1;
         this.beneficiaryAccountNumber = var2;
         this.beneficiarySortCode = var3;
         this.beneficiaryCustomerName = var4;
      }

      public final String a() {
         return this.reference;
      }

      public final String b() {
         return this.beneficiaryAccountNumber;
      }

      public final String c() {
         return this.beneficiarySortCode;
      }

      public final String d() {
         return this.beneficiaryCustomerName;
      }

      public boolean equals(Object var1) {
         boolean var2;
         if(this != var1) {
            label32: {
               if(var1 instanceof ApiPaymentSeries.FpsSchemeData) {
                  ApiPaymentSeries.FpsSchemeData var3 = (ApiPaymentSeries.FpsSchemeData)var1;
                  if(l.a(this.reference, var3.reference) && l.a(this.beneficiaryAccountNumber, var3.beneficiaryAccountNumber) && l.a(this.beneficiarySortCode, var3.beneficiarySortCode) && l.a(this.beneficiaryCustomerName, var3.beneficiaryCustomerName)) {
                     break label32;
                  }
               }

               var2 = false;
               return var2;
            }
         }

         var2 = true;
         return var2;
      }

      public int hashCode() {
         int var4 = 0;
         String var5 = this.reference;
         int var1;
         if(var5 != null) {
            var1 = var5.hashCode();
         } else {
            var1 = 0;
         }

         var5 = this.beneficiaryAccountNumber;
         int var2;
         if(var5 != null) {
            var2 = var5.hashCode();
         } else {
            var2 = 0;
         }

         var5 = this.beneficiarySortCode;
         int var3;
         if(var5 != null) {
            var3 = var5.hashCode();
         } else {
            var3 = 0;
         }

         var5 = this.beneficiaryCustomerName;
         if(var5 != null) {
            var4 = var5.hashCode();
         }

         return (var3 + (var2 + var1 * 31) * 31) * 31 + var4;
      }

      public String toString() {
         return "FpsSchemeData(reference=" + this.reference + ", beneficiaryAccountNumber=" + this.beneficiaryAccountNumber + ", beneficiarySortCode=" + this.beneficiarySortCode + ", beneficiaryCustomerName=" + this.beneficiaryCustomerName + ")";
      }
   }
}
