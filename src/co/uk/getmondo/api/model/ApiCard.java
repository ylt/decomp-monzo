package co.uk.getmondo.api.model;

import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\t\n\u0002\b\u0015\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B?\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0006\u0010\u0007\u001a\u00020\u0003\u0012\u0006\u0010\b\u001a\u00020\u0003\u0012\b\u0010\t\u001a\u0004\u0018\u00010\n¢\u0006\u0002\u0010\u000bJ\t\u0010\u0016\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0017\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0018\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0019\u001a\u00020\u0003HÆ\u0003J\t\u0010\u001a\u001a\u00020\u0003HÆ\u0003J\t\u0010\u001b\u001a\u00020\u0003HÆ\u0003J\u0010\u0010\u001c\u001a\u0004\u0018\u00010\nHÆ\u0003¢\u0006\u0002\u0010\u0013JV\u0010\u001d\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u00032\b\b\u0002\u0010\u0007\u001a\u00020\u00032\b\b\u0002\u0010\b\u001a\u00020\u00032\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\nHÆ\u0001¢\u0006\u0002\u0010\u001eJ\u0013\u0010\u001f\u001a\u00020 2\b\u0010!\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\"\u001a\u00020#HÖ\u0001J\t\u0010$\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0011\u0010\u0006\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\rR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\rR\u0011\u0010\u0007\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\rR\u0011\u0010\u0005\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\rR\u0015\u0010\t\u001a\u0004\u0018\u00010\n¢\u0006\n\n\u0002\u0010\u0014\u001a\u0004\b\u0012\u0010\u0013R\u0011\u0010\b\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\r¨\u0006%"},
   d2 = {"Lco/uk/getmondo/api/model/ApiCard;", "", "id", "", "accountId", "processorToken", "expires", "lastDigits", "status", "replacementOrdered", "", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V", "getAccountId", "()Ljava/lang/String;", "getExpires", "getId", "getLastDigits", "getProcessorToken", "getReplacementOrdered", "()Ljava/lang/Long;", "Ljava/lang/Long;", "getStatus", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "copy", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)Lco/uk/getmondo/api/model/ApiCard;", "equals", "", "other", "hashCode", "", "toString", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ApiCard {
   private final String accountId;
   private final String expires;
   private final String id;
   private final String lastDigits;
   private final String processorToken;
   private final Long replacementOrdered;
   private final String status;

   public ApiCard(String var1, String var2, String var3, String var4, String var5, String var6, Long var7) {
      l.b(var1, "id");
      l.b(var2, "accountId");
      l.b(var3, "processorToken");
      l.b(var4, "expires");
      l.b(var5, "lastDigits");
      l.b(var6, "status");
      super();
      this.id = var1;
      this.accountId = var2;
      this.processorToken = var3;
      this.expires = var4;
      this.lastDigits = var5;
      this.status = var6;
      this.replacementOrdered = var7;
   }

   public final String a() {
      return this.id;
   }

   public final String b() {
      return this.accountId;
   }

   public final String c() {
      return this.processorToken;
   }

   public final String d() {
      return this.expires;
   }

   public final String e() {
      return this.lastDigits;
   }

   public boolean equals(Object var1) {
      boolean var2;
      if(this != var1) {
         label38: {
            if(var1 instanceof ApiCard) {
               ApiCard var3 = (ApiCard)var1;
               if(l.a(this.id, var3.id) && l.a(this.accountId, var3.accountId) && l.a(this.processorToken, var3.processorToken) && l.a(this.expires, var3.expires) && l.a(this.lastDigits, var3.lastDigits) && l.a(this.status, var3.status) && l.a(this.replacementOrdered, var3.replacementOrdered)) {
                  break label38;
               }
            }

            var2 = false;
            return var2;
         }
      }

      var2 = true;
      return var2;
   }

   public final String f() {
      return this.status;
   }

   public final Long g() {
      return this.replacementOrdered;
   }

   public int hashCode() {
      int var7 = 0;
      String var8 = this.id;
      int var1;
      if(var8 != null) {
         var1 = var8.hashCode();
      } else {
         var1 = 0;
      }

      var8 = this.accountId;
      int var2;
      if(var8 != null) {
         var2 = var8.hashCode();
      } else {
         var2 = 0;
      }

      var8 = this.processorToken;
      int var3;
      if(var8 != null) {
         var3 = var8.hashCode();
      } else {
         var3 = 0;
      }

      var8 = this.expires;
      int var4;
      if(var8 != null) {
         var4 = var8.hashCode();
      } else {
         var4 = 0;
      }

      var8 = this.lastDigits;
      int var5;
      if(var8 != null) {
         var5 = var8.hashCode();
      } else {
         var5 = 0;
      }

      var8 = this.status;
      int var6;
      if(var8 != null) {
         var6 = var8.hashCode();
      } else {
         var6 = 0;
      }

      Long var9 = this.replacementOrdered;
      if(var9 != null) {
         var7 = var9.hashCode();
      }

      return (var6 + (var5 + (var4 + (var3 + (var2 + var1 * 31) * 31) * 31) * 31) * 31) * 31 + var7;
   }

   public String toString() {
      return "ApiCard(id=" + this.id + ", accountId=" + this.accountId + ", processorToken=" + this.processorToken + ", expires=" + this.expires + ", lastDigits=" + this.lastDigits + ", status=" + this.status + ", replacementOrdered=" + this.replacementOrdered + ")";
   }
}
