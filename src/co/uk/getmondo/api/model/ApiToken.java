package co.uk.getmondo.api.model;

import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0015\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B5\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0006\u0010\u0007\u001a\u00020\u0003\u0012\u0006\u0010\b\u001a\u00020\u0003¢\u0006\u0002\u0010\tJ\t\u0010\u0011\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0012\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0013\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0014\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0015\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0016\u001a\u00020\u0003HÆ\u0003JE\u0010\u0017\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u00032\b\b\u0002\u0010\u0007\u001a\u00020\u00032\b\b\u0002\u0010\b\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\u0018\u001a\u00020\u00192\b\u0010\u001a\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u001b\u001a\u00020\u001cHÖ\u0001J\t\u0010\u001d\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\u000bR\u0011\u0010\u0005\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000bR\u0011\u0010\u0006\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000bR\u0011\u0010\u0007\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u000bR\u0011\u0010\b\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u000b¨\u0006\u001e"},
   d2 = {"Lco/uk/getmondo/api/model/ApiToken;", "", "accessToken", "", "clientId", "expiresIn", "refreshToken", "tokenType", "userId", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getAccessToken", "()Ljava/lang/String;", "getClientId", "getExpiresIn", "getRefreshToken", "getTokenType", "getUserId", "component1", "component2", "component3", "component4", "component5", "component6", "copy", "equals", "", "other", "hashCode", "", "toString", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ApiToken {
   private final String accessToken;
   private final String clientId;
   private final String expiresIn;
   private final String refreshToken;
   private final String tokenType;
   private final String userId;

   public ApiToken(String var1, String var2, String var3, String var4, String var5, String var6) {
      l.b(var1, "accessToken");
      l.b(var2, "clientId");
      l.b(var3, "expiresIn");
      l.b(var4, "refreshToken");
      l.b(var5, "tokenType");
      l.b(var6, "userId");
      super();
      this.accessToken = var1;
      this.clientId = var2;
      this.expiresIn = var3;
      this.refreshToken = var4;
      this.tokenType = var5;
      this.userId = var6;
   }

   public final String a() {
      return this.accessToken;
   }

   public final String b() {
      return this.expiresIn;
   }

   public final String c() {
      return this.refreshToken;
   }

   public final String d() {
      return this.userId;
   }

   public boolean equals(Object var1) {
      boolean var2;
      if(this != var1) {
         label36: {
            if(var1 instanceof ApiToken) {
               ApiToken var3 = (ApiToken)var1;
               if(l.a(this.accessToken, var3.accessToken) && l.a(this.clientId, var3.clientId) && l.a(this.expiresIn, var3.expiresIn) && l.a(this.refreshToken, var3.refreshToken) && l.a(this.tokenType, var3.tokenType) && l.a(this.userId, var3.userId)) {
                  break label36;
               }
            }

            var2 = false;
            return var2;
         }
      }

      var2 = true;
      return var2;
   }

   public int hashCode() {
      int var6 = 0;
      String var7 = this.accessToken;
      int var1;
      if(var7 != null) {
         var1 = var7.hashCode();
      } else {
         var1 = 0;
      }

      var7 = this.clientId;
      int var2;
      if(var7 != null) {
         var2 = var7.hashCode();
      } else {
         var2 = 0;
      }

      var7 = this.expiresIn;
      int var3;
      if(var7 != null) {
         var3 = var7.hashCode();
      } else {
         var3 = 0;
      }

      var7 = this.refreshToken;
      int var4;
      if(var7 != null) {
         var4 = var7.hashCode();
      } else {
         var4 = 0;
      }

      var7 = this.tokenType;
      int var5;
      if(var7 != null) {
         var5 = var7.hashCode();
      } else {
         var5 = 0;
      }

      var7 = this.userId;
      if(var7 != null) {
         var6 = var7.hashCode();
      }

      return (var5 + (var4 + (var3 + (var2 + var1 * 31) * 31) * 31) * 31) * 31 + var6;
   }

   public String toString() {
      return "ApiToken(accessToken=" + this.accessToken + ", clientId=" + this.clientId + ", expiresIn=" + this.expiresIn + ", refreshToken=" + this.refreshToken + ", tokenType=" + this.tokenType + ", userId=" + this.userId + ")";
   }
}
