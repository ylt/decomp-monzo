package co.uk.getmondo.api.model;

import co.uk.getmondo.payments.send.data.a.d;
import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0017\n\u0002\u0010\b\n\u0002\b\u0006\b\u0086\b\u0018\u00002\u00020\u0001:\u0004'()*B=\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u000b\u0012\b\b\u0002\u0010\f\u001a\u00020\r¢\u0006\u0002\u0010\u000eJ\t\u0010\u001b\u001a\u00020\u0003HÆ\u0003J\u000b\u0010\u001c\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\t\u0010\u001d\u001a\u00020\u0007HÆ\u0003J\t\u0010\u001e\u001a\u00020\tHÆ\u0003J\u000b\u0010\u001f\u001a\u0004\u0018\u00010\u000bHÆ\u0003J\t\u0010 \u001a\u00020\rHÆ\u0003JI\u0010!\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00072\b\b\u0002\u0010\b\u001a\u00020\t2\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u000b2\b\b\u0002\u0010\f\u001a\u00020\rHÆ\u0001J\u0013\u0010\"\u001a\u00020\r2\b\u0010#\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010$\u001a\u00020%HÖ\u0001J\t\u0010&\u001a\u00020\u000bHÖ\u0001R\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0011\u0010\b\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u0011\u0010\f\u001a\u00020\r¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016R\u0013\u0010\n\u001a\u0004\u0018\u00010\u000b¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0018R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u001a¨\u0006+"},
   d2 = {"Lco/uk/getmondo/api/model/ApiConfig;", "", "p2p", "Lco/uk/getmondo/api/model/ApiConfig$P2p;", "username", "Lco/uk/getmondo/api/model/ApiConfig$MonzoMeUsername;", "featureFlags", "Lco/uk/getmondo/api/model/ApiConfig$FeatureFlags;", "inboundP2p", "Lco/uk/getmondo/api/model/ApiConfig$InboundP2p;", "sddSplashscreen", "", "prepaidAccountMigrated", "", "(Lco/uk/getmondo/api/model/ApiConfig$P2p;Lco/uk/getmondo/api/model/ApiConfig$MonzoMeUsername;Lco/uk/getmondo/api/model/ApiConfig$FeatureFlags;Lco/uk/getmondo/api/model/ApiConfig$InboundP2p;Ljava/lang/String;Z)V", "getFeatureFlags", "()Lco/uk/getmondo/api/model/ApiConfig$FeatureFlags;", "getInboundP2p", "()Lco/uk/getmondo/api/model/ApiConfig$InboundP2p;", "getP2p", "()Lco/uk/getmondo/api/model/ApiConfig$P2p;", "getPrepaidAccountMigrated", "()Z", "getSddSplashscreen", "()Ljava/lang/String;", "getUsername", "()Lco/uk/getmondo/api/model/ApiConfig$MonzoMeUsername;", "component1", "component2", "component3", "component4", "component5", "component6", "copy", "equals", "other", "hashCode", "", "toString", "FeatureFlags", "InboundP2p", "MonzoMeUsername", "P2p", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ApiConfig {
   private final ApiConfig.FeatureFlags featureFlags;
   private final ApiConfig.InboundP2p inboundP2p;
   private final ApiConfig.P2p p2p;
   private final boolean prepaidAccountMigrated;
   private final String sddSplashscreen;
   private final ApiConfig.MonzoMeUsername username;

   public ApiConfig(ApiConfig.P2p var1, ApiConfig.MonzoMeUsername var2, ApiConfig.FeatureFlags var3, ApiConfig.InboundP2p var4, String var5, boolean var6) {
      l.b(var1, "p2p");
      l.b(var3, "featureFlags");
      l.b(var4, "inboundP2p");
      super();
      this.p2p = var1;
      this.username = var2;
      this.featureFlags = var3;
      this.inboundP2p = var4;
      this.sddSplashscreen = var5;
      this.prepaidAccountMigrated = var6;
   }

   // $FF: synthetic method
   public ApiConfig(ApiConfig.P2p var1, ApiConfig.MonzoMeUsername var2, ApiConfig.FeatureFlags var3, ApiConfig.InboundP2p var4, String var5, boolean var6, int var7, i var8) {
      if((var7 & 16) != 0) {
         var5 = (String)null;
      }

      if((var7 & 32) != 0) {
         var6 = false;
      }

      this(var1, var2, var3, var4, var5, var6);
   }

   public final ApiConfig.P2p a() {
      return this.p2p;
   }

   public final ApiConfig.MonzoMeUsername b() {
      return this.username;
   }

   public final ApiConfig.FeatureFlags c() {
      return this.featureFlags;
   }

   public final ApiConfig.InboundP2p d() {
      return this.inboundP2p;
   }

   public final String e() {
      return this.sddSplashscreen;
   }

   public boolean equals(Object var1) {
      boolean var4 = false;
      boolean var3;
      if(this != var1) {
         var3 = var4;
         if(!(var1 instanceof ApiConfig)) {
            return var3;
         }

         ApiConfig var5 = (ApiConfig)var1;
         var3 = var4;
         if(!l.a(this.p2p, var5.p2p)) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.username, var5.username)) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.featureFlags, var5.featureFlags)) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.inboundP2p, var5.inboundP2p)) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.sddSplashscreen, var5.sddSplashscreen)) {
            return var3;
         }

         boolean var2;
         if(this.prepaidAccountMigrated == var5.prepaidAccountMigrated) {
            var2 = true;
         } else {
            var2 = false;
         }

         var3 = var4;
         if(!var2) {
            return var3;
         }
      }

      var3 = true;
      return var3;
   }

   public final boolean f() {
      return this.prepaidAccountMigrated;
   }

   public int hashCode() {
      throw new RuntimeException("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.copyTypes(TypeTransformer.java:311)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.fixTypes(TypeTransformer.java:226)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:207)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:161)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:433)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:129)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:528)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:425)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:441)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:171)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:271)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:109)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:290)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:33)\n");
   }

   public String toString() {
      return "ApiConfig(p2p=" + this.p2p + ", username=" + this.username + ", featureFlags=" + this.featureFlags + ", inboundP2p=" + this.inboundP2p + ", sddSplashscreen=" + this.sddSplashscreen + ", prepaidAccountMigrated=" + this.prepaidAccountMigrated + ")";
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\n\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B\u0019\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0003¢\u0006\u0002\u0010\u0005J\t\u0010\b\u001a\u00020\u0003HÆ\u0003J\t\u0010\t\u001a\u00020\u0003HÆ\u0003J\u001d\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\u000b\u001a\u00020\u00032\b\u0010\f\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\r\u001a\u00020\u000eHÖ\u0001J\t\u0010\u000f\u001a\u00020\u0010HÖ\u0001R\u0016\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0002\u0010\u0006R\u0016\u0010\u0004\u001a\u00020\u00038\u0006X\u0087\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\u0006¨\u0006\u0011"},
      d2 = {"Lco/uk/getmondo/api/model/ApiConfig$FeatureFlags;", "", "isCurrentAccountP2pEnabled", "", "potsEnabled", "(ZZ)V", "()Z", "getPotsEnabled", "component1", "component2", "copy", "equals", "other", "hashCode", "", "toString", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class FeatureFlags {
      @com.google.gson.a.c(
         a = "current_account_p2p"
      )
      private final boolean isCurrentAccountP2pEnabled;
      @com.google.gson.a.c(
         a = "pots_enabled"
      )
      private final boolean potsEnabled;

      public FeatureFlags() {
         this(false, false, 3, (i)null);
      }

      public FeatureFlags(boolean var1, boolean var2) {
         this.isCurrentAccountP2pEnabled = var1;
         this.potsEnabled = var2;
      }

      // $FF: synthetic method
      public FeatureFlags(boolean var1, boolean var2, int var3, i var4) {
         if((var3 & 1) != 0) {
            var1 = false;
         }

         if((var3 & 2) != 0) {
            var2 = false;
         }

         this(var1, var2);
      }

      public final boolean a() {
         return this.isCurrentAccountP2pEnabled;
      }

      public final boolean b() {
         return this.potsEnabled;
      }

      public boolean equals(Object var1) {
         boolean var4 = false;
         boolean var3;
         if(this != var1) {
            var3 = var4;
            if(!(var1 instanceof ApiConfig.FeatureFlags)) {
               return var3;
            }

            ApiConfig.FeatureFlags var5 = (ApiConfig.FeatureFlags)var1;
            boolean var2;
            if(this.isCurrentAccountP2pEnabled == var5.isCurrentAccountP2pEnabled) {
               var2 = true;
            } else {
               var2 = false;
            }

            var3 = var4;
            if(!var2) {
               return var3;
            }

            if(this.potsEnabled == var5.potsEnabled) {
               var2 = true;
            } else {
               var2 = false;
            }

            var3 = var4;
            if(!var2) {
               return var3;
            }
         }

         var3 = true;
         return var3;
      }

      public int hashCode() {
         throw new RuntimeException("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.copyTypes(TypeTransformer.java:311)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.fixTypes(TypeTransformer.java:226)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:207)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:161)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:433)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:129)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:528)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:425)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:441)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:171)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:271)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:109)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:290)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:33)\n");
      }

      public String toString() {
         return "FeatureFlags(isCurrentAccountP2pEnabled=" + this.isCurrentAccountP2pEnabled + ", potsEnabled=" + this.potsEnabled + ")";
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0012\b\u0086\b\u0018\u00002\u00020\u0001B'\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\b\u0010\u0007\u001a\u0004\u0018\u00010\b¢\u0006\u0002\u0010\tJ\t\u0010\u0011\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0012\u001a\u00020\u0005HÆ\u0003J\t\u0010\u0013\u001a\u00020\u0005HÆ\u0003J\u000b\u0010\u0014\u001a\u0004\u0018\u00010\bHÆ\u0003J3\u0010\u0015\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00052\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\bHÆ\u0001J\u0013\u0010\u0016\u001a\u00020\u00032\b\u0010\u0017\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0018\u001a\u00020\u0005HÖ\u0001J\t\u0010\u0019\u001a\u00020\bHÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0011\u0010\u0006\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\rR\u0013\u0010\u0007\u001a\u0004\u0018\u00010\b¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010¨\u0006\u001a"},
      d2 = {"Lco/uk/getmondo/api/model/ApiConfig$InboundP2p;", "", "enabled", "", "max", "", "min", "reason", "", "(ZIILjava/lang/String;)V", "getEnabled", "()Z", "getMax", "()I", "getMin", "getReason", "()Ljava/lang/String;", "component1", "component2", "component3", "component4", "copy", "equals", "other", "hashCode", "toString", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class InboundP2p {
      private final boolean enabled;
      private final int max;
      private final int min;
      private final String reason;

      public InboundP2p(boolean var1, int var2, int var3, String var4) {
         this.enabled = var1;
         this.max = var2;
         this.min = var3;
         this.reason = var4;
      }

      public final boolean a() {
         return this.enabled;
      }

      public final int b() {
         return this.max;
      }

      public final int c() {
         return this.min;
      }

      public final String d() {
         return this.reason;
      }

      public boolean equals(Object var1) {
         boolean var4 = false;
         boolean var3;
         if(this != var1) {
            var3 = var4;
            if(!(var1 instanceof ApiConfig.InboundP2p)) {
               return var3;
            }

            ApiConfig.InboundP2p var5 = (ApiConfig.InboundP2p)var1;
            boolean var2;
            if(this.enabled == var5.enabled) {
               var2 = true;
            } else {
               var2 = false;
            }

            var3 = var4;
            if(!var2) {
               return var3;
            }

            if(this.max == var5.max) {
               var2 = true;
            } else {
               var2 = false;
            }

            var3 = var4;
            if(!var2) {
               return var3;
            }

            if(this.min == var5.min) {
               var2 = true;
            } else {
               var2 = false;
            }

            var3 = var4;
            if(!var2) {
               return var3;
            }

            var3 = var4;
            if(!l.a(this.reason, var5.reason)) {
               return var3;
            }
         }

         var3 = true;
         return var3;
      }

      public int hashCode() {
         throw new RuntimeException("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.copyTypes(TypeTransformer.java:311)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.fixTypes(TypeTransformer.java:226)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:207)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:161)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:433)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:129)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:528)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:425)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:441)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:171)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:271)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:109)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:290)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:33)\n");
      }

      public String toString() {
         return "InboundP2p(enabled=" + this.enabled + ", max=" + this.max + ", min=" + this.min + ", reason=" + this.reason + ")";
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003HÆ\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\f\u001a\u00020\rHÖ\u0001J\t\u0010\u000e\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u000f"},
      d2 = {"Lco/uk/getmondo/api/model/ApiConfig$MonzoMeUsername;", "", "primary", "", "(Ljava/lang/String;)V", "getPrimary", "()Ljava/lang/String;", "component1", "copy", "equals", "", "other", "hashCode", "", "toString", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class MonzoMeUsername {
      private final String primary;

      public MonzoMeUsername(String var1) {
         l.b(var1, "primary");
         super();
         this.primary = var1;
      }

      public final String a() {
         return this.primary;
      }

      public boolean equals(Object var1) {
         boolean var2;
         if(this != var1) {
            label26: {
               if(var1 instanceof ApiConfig.MonzoMeUsername) {
                  ApiConfig.MonzoMeUsername var3 = (ApiConfig.MonzoMeUsername)var1;
                  if(l.a(this.primary, var3.primary)) {
                     break label26;
                  }
               }

               var2 = false;
               return var2;
            }
         }

         var2 = true;
         return var2;
      }

      public int hashCode() {
         String var2 = this.primary;
         int var1;
         if(var2 != null) {
            var1 = var2.hashCode();
         } else {
            var1 = 0;
         }

         return var1;
      }

      public String toString() {
         return "MonzoMeUsername(primary=" + this.primary + ")";
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003HÆ\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\f\u001a\u00020\rHÖ\u0001J\t\u0010\u000e\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u000f"},
      d2 = {"Lco/uk/getmondo/api/model/ApiConfig$P2p;", "", "status", "", "(Ljava/lang/String;)V", "getStatus", "()Ljava/lang/String;", "component1", "copy", "equals", "", "other", "hashCode", "", "toString", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class P2p {
      private final String status;

      public P2p() {
         this((String)null, 1, (i)null);
      }

      public P2p(String var1) {
         l.b(var1, "status");
         super();
         this.status = var1;
      }

      // $FF: synthetic method
      public P2p(String var1, int var2, i var3) {
         if((var2 & 1) != 0) {
            var1 = d.b.d;
            l.a(var1, "P2pStatus.DISABLED.id");
         }

         this(var1);
      }

      public final String a() {
         return this.status;
      }

      public boolean equals(Object var1) {
         boolean var2;
         if(this != var1) {
            label26: {
               if(var1 instanceof ApiConfig.P2p) {
                  ApiConfig.P2p var3 = (ApiConfig.P2p)var1;
                  if(l.a(this.status, var3.status)) {
                     break label26;
                  }
               }

               var2 = false;
               return var2;
            }
         }

         var2 = true;
         return var2;
      }

      public int hashCode() {
         String var2 = this.status;
         int var1;
         if(var2 != null) {
            var1 = var2.hashCode();
         } else {
            var1 = 0;
         }

         return var1;
      }

      public String toString() {
         return "P2p(status=" + this.status + ")";
      }
   }
}
