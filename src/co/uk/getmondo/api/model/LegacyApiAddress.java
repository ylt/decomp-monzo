package co.uk.getmondo.api.model;

import com.squareup.moshi.h;
import java.util.List;
import kotlin.Metadata;
import kotlin.a.m;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\u0013\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B;\u0012\u0010\b\u0003\u0010\u0002\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0006\u001a\u00020\u0004\u0012\u0006\u0010\u0007\u001a\u00020\u0004\u0012\b\b\u0001\u0010\b\u001a\u00020\u0004¢\u0006\u0002\u0010\tJ\u0011\u0010\u0011\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0003HÆ\u0003J\t\u0010\u0012\u001a\u00020\u0004HÆ\u0003J\t\u0010\u0013\u001a\u00020\u0004HÆ\u0003J\t\u0010\u0014\u001a\u00020\u0004HÆ\u0003J\t\u0010\u0015\u001a\u00020\u0004HÆ\u0003JC\u0010\u0016\u001a\u00020\u00002\u0010\b\u0003\u0010\u0002\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00042\b\b\u0003\u0010\u0006\u001a\u00020\u00042\b\b\u0002\u0010\u0007\u001a\u00020\u00042\b\b\u0003\u0010\b\u001a\u00020\u0004HÆ\u0001J\u0013\u0010\u0017\u001a\u00020\u00182\b\u0010\u0019\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u001a\u001a\u00020\u001bHÖ\u0001J\t\u0010\u001c\u001a\u00020\u0004HÖ\u0001R\u0011\u0010\b\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\u0007\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\u000bR\u0011\u0010\u0005\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000bR\u0011\u0010\u0006\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000bR\u0019\u0010\u0002\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010¨\u0006\u001d"},
   d2 = {"Lco/uk/getmondo/api/model/LegacyApiAddress;", "", "streetAddress", "", "", "locality", "postalCode", "country", "administrativeArea", "(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getAdministrativeArea", "()Ljava/lang/String;", "getCountry", "getLocality", "getPostalCode", "getStreetAddress", "()Ljava/util/List;", "component1", "component2", "component3", "component4", "component5", "copy", "equals", "", "other", "hashCode", "", "toString", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class LegacyApiAddress {
   private final String administrativeArea;
   private final String country;
   private final String locality;
   private final String postalCode;
   private final List streetAddress;

   public LegacyApiAddress(@h(a = "street_address") List var1, String var2, @h(a = "postal_code") String var3, String var4, @h(a = "administrative_area") String var5) {
      l.b(var2, "locality");
      l.b(var3, "postalCode");
      l.b(var4, "country");
      l.b(var5, "administrativeArea");
      super();
      this.streetAddress = var1;
      this.locality = var2;
      this.postalCode = var3;
      this.country = var4;
      this.administrativeArea = var5;
   }

   // $FF: synthetic method
   public LegacyApiAddress(List var1, String var2, String var3, String var4, String var5, int var6, i var7) {
      if((var6 & 1) != 0) {
         var1 = m.a();
      }

      this(var1, var2, var3, var4, var5);
   }

   public final List a() {
      return this.streetAddress;
   }

   public final String b() {
      return this.locality;
   }

   public final String c() {
      return this.postalCode;
   }

   public final String d() {
      return this.country;
   }

   public final String e() {
      return this.administrativeArea;
   }

   public boolean equals(Object var1) {
      boolean var2;
      if(this != var1) {
         label34: {
            if(var1 instanceof LegacyApiAddress) {
               LegacyApiAddress var3 = (LegacyApiAddress)var1;
               if(l.a(this.streetAddress, var3.streetAddress) && l.a(this.locality, var3.locality) && l.a(this.postalCode, var3.postalCode) && l.a(this.country, var3.country) && l.a(this.administrativeArea, var3.administrativeArea)) {
                  break label34;
               }
            }

            var2 = false;
            return var2;
         }
      }

      var2 = true;
      return var2;
   }

   public int hashCode() {
      int var5 = 0;
      List var6 = this.streetAddress;
      int var1;
      if(var6 != null) {
         var1 = var6.hashCode();
      } else {
         var1 = 0;
      }

      String var7 = this.locality;
      int var2;
      if(var7 != null) {
         var2 = var7.hashCode();
      } else {
         var2 = 0;
      }

      var7 = this.postalCode;
      int var3;
      if(var7 != null) {
         var3 = var7.hashCode();
      } else {
         var3 = 0;
      }

      var7 = this.country;
      int var4;
      if(var7 != null) {
         var4 = var7.hashCode();
      } else {
         var4 = 0;
      }

      var7 = this.administrativeArea;
      if(var7 != null) {
         var5 = var7.hashCode();
      }

      return (var4 + (var3 + (var2 + var1 * 31) * 31) * 31) * 31 + var5;
   }

   public String toString() {
      return "LegacyApiAddress(streetAddress=" + this.streetAddress + ", locality=" + this.locality + ", postalCode=" + this.postalCode + ", country=" + this.country + ", administrativeArea=" + this.administrativeArea + ")";
   }
}
