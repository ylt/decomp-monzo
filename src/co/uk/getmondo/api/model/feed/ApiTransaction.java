package co.uk.getmondo.api.model.feed;

import java.util.Date;
import java.util.List;
import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b8\n\u0002\u0010\b\n\u0002\b\u0003\b\u0086\b\u0018\u00002\u00020\u0001B©\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\u0007\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\n\u0012\u0006\u0010\f\u001a\u00020\u0003\u0012\u0006\u0010\r\u001a\u00020\u0003\u0012\u0006\u0010\u000e\u001a\u00020\u0003\u0012\b\u0010\u000f\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0010\u001a\u0004\u0018\u00010\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u0012\u0006\u0010\u0014\u001a\u00020\u0003\u0012\b\u0010\u0015\u001a\u0004\u0018\u00010\u0016\u0012\u0006\u0010\u0017\u001a\u00020\u0003\u0012\u000e\u0010\u0018\u001a\n\u0012\u0004\u0012\u00020\u001a\u0018\u00010\u0019\u0012\u0006\u0010\u001b\u001a\u00020\u0013\u0012\n\b\u0002\u0010\u001c\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\u001dJ\t\u00109\u001a\u00020\u0003HÆ\u0003J\u000b\u0010:\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010;\u001a\u0004\u0018\u00010\u0011HÆ\u0003J\t\u0010<\u001a\u00020\u0013HÆ\u0003J\t\u0010=\u001a\u00020\u0003HÆ\u0003J\u000b\u0010>\u001a\u0004\u0018\u00010\u0016HÆ\u0003J\t\u0010?\u001a\u00020\u0003HÆ\u0003J\u0011\u0010@\u001a\n\u0012\u0004\u0012\u00020\u001a\u0018\u00010\u0019HÆ\u0003J\t\u0010A\u001a\u00020\u0013HÆ\u0003J\u000b\u0010B\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010C\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\t\u0010D\u001a\u00020\u0007HÆ\u0003J\t\u0010E\u001a\u00020\u0007HÆ\u0003J\t\u0010F\u001a\u00020\nHÆ\u0003J\t\u0010G\u001a\u00020\nHÆ\u0003J\t\u0010H\u001a\u00020\u0003HÆ\u0003J\t\u0010I\u001a\u00020\u0003HÆ\u0003J\t\u0010J\u001a\u00020\u0003HÆ\u0003JÏ\u0001\u0010K\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00072\b\b\u0002\u0010\b\u001a\u00020\u00072\b\b\u0002\u0010\t\u001a\u00020\n2\b\b\u0002\u0010\u000b\u001a\u00020\n2\b\b\u0002\u0010\f\u001a\u00020\u00032\b\b\u0002\u0010\r\u001a\u00020\u00032\b\b\u0002\u0010\u000e\u001a\u00020\u00032\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u00112\b\b\u0002\u0010\u0012\u001a\u00020\u00132\b\b\u0002\u0010\u0014\u001a\u00020\u00032\n\b\u0002\u0010\u0015\u001a\u0004\u0018\u00010\u00162\b\b\u0002\u0010\u0017\u001a\u00020\u00032\u0010\b\u0002\u0010\u0018\u001a\n\u0012\u0004\u0012\u00020\u001a\u0018\u00010\u00192\b\b\u0002\u0010\u001b\u001a\u00020\u00132\n\b\u0002\u0010\u001c\u001a\u0004\u0018\u00010\u0003HÆ\u0001J\u0013\u0010L\u001a\u00020\u00132\b\u0010M\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\u0006\u0010N\u001a\u00020\u0013J\u0006\u0010O\u001a\u00020\u0013J\u0006\u0010P\u001a\u00020\u0013J\u0006\u0010Q\u001a\u00020\u0013J\t\u0010R\u001a\u00020SHÖ\u0001J\t\u0010T\u001a\u00020\u0003HÖ\u0001J\u000e\u0010U\u001a\u00020\u00002\u0006\u0010\u0017\u001a\u00020\u0003R\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u001fR\u0019\u0010\u0018\u001a\n\u0012\u0004\u0012\u00020\u001a\u0018\u00010\u0019¢\u0006\b\n\u0000\u001a\u0004\b \u0010!R\u0011\u0010\u0017\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\"\u0010#R\u0013\u0010\u0010\u001a\u0004\u0018\u00010\u0011¢\u0006\b\n\u0000\u001a\u0004\b$\u0010%R\u0011\u0010\t\u001a\u00020\n¢\u0006\b\n\u0000\u001a\u0004\b&\u0010'R\u0011\u0010\f\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b(\u0010#R\u0013\u0010\u000f\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b)\u0010#R\u0011\u0010\u000e\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b*\u0010#R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b+\u0010#R\u0011\u0010\u001b\u001a\u00020\u0013¢\u0006\b\n\u0000\u001a\u0004\b,\u0010-R\u0011\u0010\u0012\u001a\u00020\u0013¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010-R\u0011\u0010.\u001a\u00020\u00138F¢\u0006\u0006\u001a\u0004\b.\u0010-R\u0011\u0010/\u001a\u00020\u00138F¢\u0006\u0006\u001a\u0004\b/\u0010-R\u0011\u0010\b\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b0\u0010\u001fR\u0011\u0010\r\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b1\u0010#R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b2\u00103R\u0013\u0010\u0015\u001a\u0004\u0018\u00010\u0016¢\u0006\b\n\u0000\u001a\u0004\b4\u00105R\u0013\u0010\u001c\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b6\u0010#R\u0011\u0010\u0014\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b7\u0010#R\u0011\u0010\u000b\u001a\u00020\n¢\u0006\b\n\u0000\u001a\u0004\b8\u0010'¨\u0006V"},
   d2 = {"Lco/uk/getmondo/api/model/feed/ApiTransaction;", "", "id", "", "merchant", "Lco/uk/getmondo/api/model/feed/ApiMerchant;", "amount", "", "localAmount", "created", "Ljava/util/Date;", "updated", "currency", "localCurrency", "description", "declineReason", "counterparty", "Lco/uk/getmondo/api/model/feed/ApiCounterparty;", "isLoad", "", "settled", "metadata", "Lco/uk/getmondo/api/model/feed/ApiMetadata;", "category", "attachments", "", "Lco/uk/getmondo/api/model/feed/ApiAttachment;", "includeInSpending", "scheme", "(Ljava/lang/String;Lco/uk/getmondo/api/model/feed/ApiMerchant;JJLjava/util/Date;Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/feed/ApiCounterparty;ZLjava/lang/String;Lco/uk/getmondo/api/model/feed/ApiMetadata;Ljava/lang/String;Ljava/util/List;ZLjava/lang/String;)V", "getAmount", "()J", "getAttachments", "()Ljava/util/List;", "getCategory", "()Ljava/lang/String;", "getCounterparty", "()Lco/uk/getmondo/api/model/feed/ApiCounterparty;", "getCreated", "()Ljava/util/Date;", "getCurrency", "getDeclineReason", "getDescription", "getId", "getIncludeInSpending", "()Z", "isPeerToPeer", "isTopup", "getLocalAmount", "getLocalCurrency", "getMerchant", "()Lco/uk/getmondo/api/model/feed/ApiMerchant;", "getMetadata", "()Lco/uk/getmondo/api/model/feed/ApiMetadata;", "getScheme", "getSettled", "getUpdated", "component1", "component10", "component11", "component12", "component13", "component14", "component15", "component16", "component17", "component18", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "other", "fromAtm", "hasDeclineReason", "hasMerchant", "hasMetadata", "hashCode", "", "toString", "withCategory", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ApiTransaction {
   private final long amount;
   private final List attachments;
   private final String category;
   private final ApiCounterparty counterparty;
   private final Date created;
   private final String currency;
   private final String declineReason;
   private final String description;
   private final String id;
   private final boolean includeInSpending;
   private final boolean isLoad;
   private final long localAmount;
   private final String localCurrency;
   private final ApiMerchant merchant;
   private final ApiMetadata metadata;
   private final String scheme;
   private final String settled;
   private final Date updated;

   public ApiTransaction(String var1, ApiMerchant var2, long var3, long var5, Date var7, Date var8, String var9, String var10, String var11, String var12, ApiCounterparty var13, boolean var14, String var15, ApiMetadata var16, String var17, List var18, boolean var19, String var20) {
      l.b(var1, "id");
      l.b(var7, "created");
      l.b(var8, "updated");
      l.b(var9, "currency");
      l.b(var10, "localCurrency");
      l.b(var11, "description");
      l.b(var15, "settled");
      l.b(var17, "category");
      super();
      this.id = var1;
      this.merchant = var2;
      this.amount = var3;
      this.localAmount = var5;
      this.created = var7;
      this.updated = var8;
      this.currency = var9;
      this.localCurrency = var10;
      this.description = var11;
      this.declineReason = var12;
      this.counterparty = var13;
      this.isLoad = var14;
      this.settled = var15;
      this.metadata = var16;
      this.category = var17;
      this.attachments = var18;
      this.includeInSpending = var19;
      this.scheme = var20;
   }

   // $FF: synthetic method
   public ApiTransaction(String var1, ApiMerchant var2, long var3, long var5, Date var7, Date var8, String var9, String var10, String var11, String var12, ApiCounterparty var13, boolean var14, String var15, ApiMetadata var16, String var17, List var18, boolean var19, String var20, int var21, i var22) {
      if((131072 & var21) != 0) {
         var20 = (String)null;
      }

      this(var1, var2, var3, var5, var7, var8, var9, var10, var11, var12, var13, var14, var15, var16, var17, var18, var19, var20);
   }

   public final ApiTransaction a(String var1) {
      l.b(var1, "category");
      return a(this, (String)null, (ApiMerchant)null, 0L, 0L, (Date)null, (Date)null, (String)null, (String)null, (String)null, (String)null, (ApiCounterparty)null, false, (String)null, (ApiMetadata)null, var1, (List)null, false, (String)null, 245759, (Object)null);
   }

   public final ApiTransaction a(String var1, ApiMerchant var2, long var3, long var5, Date var7, Date var8, String var9, String var10, String var11, String var12, ApiCounterparty var13, boolean var14, String var15, ApiMetadata var16, String var17, List var18, boolean var19, String var20) {
      l.b(var1, "id");
      l.b(var7, "created");
      l.b(var8, "updated");
      l.b(var9, "currency");
      l.b(var10, "localCurrency");
      l.b(var11, "description");
      l.b(var15, "settled");
      l.b(var17, "category");
      return new ApiTransaction(var1, var2, var3, var5, var7, var8, var9, var10, var11, var12, var13, var14, var15, var16, var17, var18, var19, var20);
   }

   public final boolean a() {
      ApiMerchant var2 = this.merchant;
      boolean var1;
      if(var2 != null) {
         var1 = var2.f();
      } else {
         var1 = false;
      }

      return var1;
   }

   public final boolean b() {
      boolean var1;
      if(this.merchant != null) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public final boolean c() {
      boolean var1;
      if(!this.d() && this.isLoad) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public final boolean d() {
      Object var3 = null;
      ApiMetadata var2 = this.metadata;
      String var5;
      if(var2 != null) {
         var5 = var2.e();
      } else {
         var5 = null;
      }

      boolean var1;
      label23: {
         if(var5 == null) {
            ApiMetadata var4 = this.metadata;
            var5 = (String)var3;
            if(var4 != null) {
               var5 = var4.d();
            }

            if(var5 == null) {
               break label23;
            }
         }

         if(this.counterparty != null) {
            var1 = true;
            return var1;
         }
      }

      var1 = false;
      return var1;
   }

   public final boolean e() {
      boolean var1;
      if(this.metadata != null) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public boolean equals(Object var1) {
      boolean var4 = false;
      boolean var3;
      if(this != var1) {
         var3 = var4;
         if(!(var1 instanceof ApiTransaction)) {
            return var3;
         }

         ApiTransaction var5 = (ApiTransaction)var1;
         var3 = var4;
         if(!l.a(this.id, var5.id)) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.merchant, var5.merchant)) {
            return var3;
         }

         boolean var2;
         if(this.amount == var5.amount) {
            var2 = true;
         } else {
            var2 = false;
         }

         var3 = var4;
         if(!var2) {
            return var3;
         }

         if(this.localAmount == var5.localAmount) {
            var2 = true;
         } else {
            var2 = false;
         }

         var3 = var4;
         if(!var2) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.created, var5.created)) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.updated, var5.updated)) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.currency, var5.currency)) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.localCurrency, var5.localCurrency)) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.description, var5.description)) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.declineReason, var5.declineReason)) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.counterparty, var5.counterparty)) {
            return var3;
         }

         if(this.isLoad == var5.isLoad) {
            var2 = true;
         } else {
            var2 = false;
         }

         var3 = var4;
         if(!var2) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.settled, var5.settled)) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.metadata, var5.metadata)) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.category, var5.category)) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.attachments, var5.attachments)) {
            return var3;
         }

         if(this.includeInSpending == var5.includeInSpending) {
            var2 = true;
         } else {
            var2 = false;
         }

         var3 = var4;
         if(!var2) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.scheme, var5.scheme)) {
            return var3;
         }
      }

      var3 = true;
      return var3;
   }

   public final boolean f() {
      boolean var1;
      if(this.declineReason != null) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public final String g() {
      return this.id;
   }

   public final ApiMerchant h() {
      return this.merchant;
   }

   public int hashCode() {
      throw new RuntimeException("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.copyTypes(TypeTransformer.java:311)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.fixTypes(TypeTransformer.java:226)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:207)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:161)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:433)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:129)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:528)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:425)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:441)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:171)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:271)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:109)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:290)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:33)\n");
   }

   public final long i() {
      return this.amount;
   }

   public final long j() {
      return this.localAmount;
   }

   public final Date k() {
      return this.created;
   }

   public final Date l() {
      return this.updated;
   }

   public final String m() {
      return this.currency;
   }

   public final String n() {
      return this.localCurrency;
   }

   public final String o() {
      return this.description;
   }

   public final String p() {
      return this.declineReason;
   }

   public final ApiCounterparty q() {
      return this.counterparty;
   }

   public final String r() {
      return this.settled;
   }

   public final ApiMetadata s() {
      return this.metadata;
   }

   public final String t() {
      return this.category;
   }

   public String toString() {
      return "ApiTransaction(id=" + this.id + ", merchant=" + this.merchant + ", amount=" + this.amount + ", localAmount=" + this.localAmount + ", created=" + this.created + ", updated=" + this.updated + ", currency=" + this.currency + ", localCurrency=" + this.localCurrency + ", description=" + this.description + ", declineReason=" + this.declineReason + ", counterparty=" + this.counterparty + ", isLoad=" + this.isLoad + ", settled=" + this.settled + ", metadata=" + this.metadata + ", category=" + this.category + ", attachments=" + this.attachments + ", includeInSpending=" + this.includeInSpending + ", scheme=" + this.scheme + ")";
   }

   public final List u() {
      return this.attachments;
   }

   public final boolean v() {
      return this.includeInSpending;
   }

   public final String w() {
      return this.scheme;
   }
}
