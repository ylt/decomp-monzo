package co.uk.getmondo.api.model.feed;

import java.util.Date;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0006\u0010\u0007\u001a\u00020\u0003¢\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0010\u001a\u00020\u0005HÆ\u0003J\t\u0010\u0011\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0012\u001a\u00020\u0003HÆ\u0003J1\u0010\u0013\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00032\b\b\u0002\u0010\u0007\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\u0014\u001a\u00020\u00152\b\u0010\u0016\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0017\u001a\u00020\u0018HÖ\u0001J\t\u0010\u0019\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0006\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0007\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\fR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\f¨\u0006\u001a"},
   d2 = {"Lco/uk/getmondo/api/model/feed/ApiAttachment;", "", "id", "", "created", "Ljava/util/Date;", "externalId", "fileUrl", "(Ljava/lang/String;Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;)V", "getCreated", "()Ljava/util/Date;", "getExternalId", "()Ljava/lang/String;", "getFileUrl", "getId", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "hashCode", "", "toString", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ApiAttachment {
   private final Date created;
   private final String externalId;
   private final String fileUrl;
   private final String id;

   public ApiAttachment(String var1, Date var2, String var3, String var4) {
      l.b(var1, "id");
      l.b(var2, "created");
      l.b(var3, "externalId");
      l.b(var4, "fileUrl");
      super();
      this.id = var1;
      this.created = var2;
      this.externalId = var3;
      this.fileUrl = var4;
   }

   public final String a() {
      return this.id;
   }

   public final Date b() {
      return this.created;
   }

   public final String c() {
      return this.externalId;
   }

   public final String d() {
      return this.fileUrl;
   }

   public boolean equals(Object var1) {
      boolean var2;
      if(this != var1) {
         label32: {
            if(var1 instanceof ApiAttachment) {
               ApiAttachment var3 = (ApiAttachment)var1;
               if(l.a(this.id, var3.id) && l.a(this.created, var3.created) && l.a(this.externalId, var3.externalId) && l.a(this.fileUrl, var3.fileUrl)) {
                  break label32;
               }
            }

            var2 = false;
            return var2;
         }
      }

      var2 = true;
      return var2;
   }

   public int hashCode() {
      int var4 = 0;
      String var5 = this.id;
      int var1;
      if(var5 != null) {
         var1 = var5.hashCode();
      } else {
         var1 = 0;
      }

      Date var6 = this.created;
      int var2;
      if(var6 != null) {
         var2 = var6.hashCode();
      } else {
         var2 = 0;
      }

      var5 = this.externalId;
      int var3;
      if(var5 != null) {
         var3 = var5.hashCode();
      } else {
         var3 = 0;
      }

      var5 = this.fileUrl;
      if(var5 != null) {
         var4 = var5.hashCode();
      }

      return (var3 + (var2 + var1 * 31) * 31) * 31 + var4;
   }

   public String toString() {
      return "ApiAttachment(id=" + this.id + ", created=" + this.created + ", externalId=" + this.externalId + ", fileUrl=" + this.fileUrl + ")";
   }
}
