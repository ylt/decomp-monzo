package co.uk.getmondo.api.model.feed;

import java.util.Date;
import kotlin.Metadata;
import kotlin.d.b.l;
import org.threeten.bp.LocalDateTime;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u001b\n\u0002\u0010\b\n\u0002\b\u0003\b\u0086\b\u0018\u00002\u00020\u0001:\u0001-BS\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\b\u0010\t\u001a\u0004\u0018\u00010\n\u0012\b\u0010\u000b\u001a\u0004\u0018\u00010\f\u0012\b\u0010\r\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u000e\u001a\u00020\u000f¢\u0006\u0002\u0010\u0010J\t\u0010\u001e\u001a\u00020\u0003HÆ\u0003J\t\u0010\u001f\u001a\u00020\u0003HÆ\u0003J\t\u0010 \u001a\u00020\u0003HÆ\u0003J\t\u0010!\u001a\u00020\u0003HÆ\u0003J\t\u0010\"\u001a\u00020\bHÆ\u0003J\u000b\u0010#\u001a\u0004\u0018\u00010\nHÆ\u0003J\u000b\u0010$\u001a\u0004\u0018\u00010\fHÆ\u0003J\u000b\u0010%\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\t\u0010&\u001a\u00020\u000fHÆ\u0003Ji\u0010'\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u00032\b\b\u0002\u0010\u0007\u001a\u00020\b2\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\n2\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\f2\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u00032\b\b\u0002\u0010\u000e\u001a\u00020\u000fHÆ\u0001J\u0013\u0010(\u001a\u00020\u000f2\b\u0010)\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010*\u001a\u00020+HÖ\u0001J\t\u0010,\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u0013\u0010\r\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0012R\u0011\u0010\u0007\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0015R\u0011\u0010\u0005\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0012R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0012R\u0011\u0010\u000e\u001a\u00020\u000f¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u0018R\u0013\u0010\u000b\u001a\u0004\u0018\u00010\f¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u001aR\u0013\u0010\t\u001a\u0004\u0018\u00010\n¢\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u001cR\u0011\u0010\u0006\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u0012¨\u0006."},
   d2 = {"Lco/uk/getmondo/api/model/feed/ApiFeedItem;", "", "id", "", "accountId", "externalId", "type", "created", "Ljava/util/Date;", "transaction", "Lco/uk/getmondo/api/model/feed/ApiTransaction;", "params", "Lco/uk/getmondo/api/model/feed/ApiFeedItem$Params;", "appUri", "isDismissable", "", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Lco/uk/getmondo/api/model/feed/ApiTransaction;Lco/uk/getmondo/api/model/feed/ApiFeedItem$Params;Ljava/lang/String;Z)V", "getAccountId", "()Ljava/lang/String;", "getAppUri", "getCreated", "()Ljava/util/Date;", "getExternalId", "getId", "()Z", "getParams", "()Lco/uk/getmondo/api/model/feed/ApiFeedItem$Params;", "getTransaction", "()Lco/uk/getmondo/api/model/feed/ApiTransaction;", "getType", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "other", "hashCode", "", "toString", "Params", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ApiFeedItem {
   private final String accountId;
   private final String appUri;
   private final Date created;
   private final String externalId;
   private final String id;
   private final boolean isDismissable;
   private final ApiFeedItem.Params params;
   private final ApiTransaction transaction;
   private final String type;

   public ApiFeedItem(String var1, String var2, String var3, String var4, Date var5, ApiTransaction var6, ApiFeedItem.Params var7, String var8, boolean var9) {
      l.b(var1, "id");
      l.b(var2, "accountId");
      l.b(var3, "externalId");
      l.b(var4, "type");
      l.b(var5, "created");
      super();
      this.id = var1;
      this.accountId = var2;
      this.externalId = var3;
      this.type = var4;
      this.created = var5;
      this.transaction = var6;
      this.params = var7;
      this.appUri = var8;
      this.isDismissable = var9;
   }

   public final String a() {
      return this.id;
   }

   public final String b() {
      return this.accountId;
   }

   public final String c() {
      return this.externalId;
   }

   public final String d() {
      return this.type;
   }

   public final Date e() {
      return this.created;
   }

   public boolean equals(Object var1) {
      boolean var4 = false;
      boolean var3;
      if(this != var1) {
         var3 = var4;
         if(!(var1 instanceof ApiFeedItem)) {
            return var3;
         }

         ApiFeedItem var5 = (ApiFeedItem)var1;
         var3 = var4;
         if(!l.a(this.id, var5.id)) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.accountId, var5.accountId)) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.externalId, var5.externalId)) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.type, var5.type)) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.created, var5.created)) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.transaction, var5.transaction)) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.params, var5.params)) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.appUri, var5.appUri)) {
            return var3;
         }

         boolean var2;
         if(this.isDismissable == var5.isDismissable) {
            var2 = true;
         } else {
            var2 = false;
         }

         var3 = var4;
         if(!var2) {
            return var3;
         }
      }

      var3 = true;
      return var3;
   }

   public final ApiTransaction f() {
      return this.transaction;
   }

   public final ApiFeedItem.Params g() {
      return this.params;
   }

   public final String h() {
      return this.appUri;
   }

   public int hashCode() {
      throw new RuntimeException("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.copyTypes(TypeTransformer.java:311)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.fixTypes(TypeTransformer.java:226)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:207)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:161)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:433)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:129)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:528)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:425)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:441)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:171)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:271)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:109)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:290)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:33)\n");
   }

   public final boolean i() {
      return this.isDismissable;
   }

   public String toString() {
      return "ApiFeedItem(id=" + this.id + ", accountId=" + this.accountId + ", externalId=" + this.externalId + ", type=" + this.type + ", created=" + this.created + ", transaction=" + this.transaction + ", params=" + this.params + ", appUri=" + this.appUri + ", isDismissable=" + this.isDismissable + ")";
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b(\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B}\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\b\u0010\u0007\u001a\u0004\u0018\u00010\b\u0012\b\u0010\t\u001a\u0004\u0018\u00010\b\u0012\b\u0010\n\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u000b\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\f\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\r\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u000e\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u000f\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0010\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\u0011J\u000b\u0010\"\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010#\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010$\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010%\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010&\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u0010\u0010'\u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0002\u0010\u0013J\u000b\u0010(\u001a\u0004\u0018\u00010\bHÆ\u0003J\u000b\u0010)\u001a\u0004\u0018\u00010\bHÆ\u0003J\u000b\u0010*\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010+\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010,\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010-\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u009e\u0001\u0010.\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00062\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\b2\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\b2\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u0003HÆ\u0001¢\u0006\u0002\u0010/J\u0013\u00100\u001a\u0002012\b\u00102\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u00103\u001a\u000204HÖ\u0001J\t\u00105\u001a\u00020\u0003HÖ\u0001R\u0015\u0010\u0005\u001a\u0004\u0018\u00010\u0006¢\u0006\n\n\u0002\u0010\u0014\u001a\u0004\b\u0012\u0010\u0013R\u0013\u0010\u000f\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0016R\u0013\u0010\u0010\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0016R\u0013\u0010\t\u001a\u0004\u0018\u00010\b¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u001aR\u0013\u0010\u0007\u001a\u0004\u0018\u00010\b¢\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u001aR\u0013\u0010\n\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u0016R\u0013\u0010\f\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u0016R\u0013\u0010\u000b\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u0016R\u0013\u0010\r\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010\u0016R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b \u0010\u0016R\u0013\u0010\u000e\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b!\u0010\u0016¨\u00066"},
      d2 = {"Lco/uk/getmondo/api/model/feed/ApiFeedItem$Params;", "", "ticketId", "", "currency", "amount", "", "intervalStart", "Lorg/threeten/bp/LocalDateTime;", "intervalEnd", "intervalType", "kycRejectedReason", "kycRejectedCustomerMessage", "sddMigrationRejectionNote", "title", "body", "imageUrl", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Lorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/LocalDateTime;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getAmount", "()Ljava/lang/Long;", "Ljava/lang/Long;", "getBody", "()Ljava/lang/String;", "getCurrency", "getImageUrl", "getIntervalEnd", "()Lorg/threeten/bp/LocalDateTime;", "getIntervalStart", "getIntervalType", "getKycRejectedCustomerMessage", "getKycRejectedReason", "getSddMigrationRejectionNote", "getTicketId", "getTitle", "component1", "component10", "component11", "component12", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Lorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/LocalDateTime;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lco/uk/getmondo/api/model/feed/ApiFeedItem$Params;", "equals", "", "other", "hashCode", "", "toString", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class Params {
      private final Long amount;
      private final String body;
      private final String currency;
      private final String imageUrl;
      private final LocalDateTime intervalEnd;
      private final LocalDateTime intervalStart;
      private final String intervalType;
      private final String kycRejectedCustomerMessage;
      private final String kycRejectedReason;
      private final String sddMigrationRejectionNote;
      private final String ticketId;
      private final String title;

      public Params(String var1, String var2, Long var3, LocalDateTime var4, LocalDateTime var5, String var6, String var7, String var8, String var9, String var10, String var11, String var12) {
         this.ticketId = var1;
         this.currency = var2;
         this.amount = var3;
         this.intervalStart = var4;
         this.intervalEnd = var5;
         this.intervalType = var6;
         this.kycRejectedReason = var7;
         this.kycRejectedCustomerMessage = var8;
         this.sddMigrationRejectionNote = var9;
         this.title = var10;
         this.body = var11;
         this.imageUrl = var12;
      }

      public final String a() {
         return this.ticketId;
      }

      public final String b() {
         return this.currency;
      }

      public final Long c() {
         return this.amount;
      }

      public final LocalDateTime d() {
         return this.intervalStart;
      }

      public final LocalDateTime e() {
         return this.intervalEnd;
      }

      public boolean equals(Object var1) {
         boolean var2;
         label57: {
            if(this != var1) {
               if(!(var1 instanceof ApiFeedItem.Params)) {
                  break label57;
               }

               ApiFeedItem.Params var3 = (ApiFeedItem.Params)var1;
               if(!l.a(this.ticketId, var3.ticketId) || !l.a(this.currency, var3.currency) || !l.a(this.amount, var3.amount) || !l.a(this.intervalStart, var3.intervalStart) || !l.a(this.intervalEnd, var3.intervalEnd) || !l.a(this.intervalType, var3.intervalType) || !l.a(this.kycRejectedReason, var3.kycRejectedReason) || !l.a(this.kycRejectedCustomerMessage, var3.kycRejectedCustomerMessage) || !l.a(this.sddMigrationRejectionNote, var3.sddMigrationRejectionNote) || !l.a(this.title, var3.title) || !l.a(this.body, var3.body) || !l.a(this.imageUrl, var3.imageUrl)) {
                  break label57;
               }
            }

            var2 = true;
            return var2;
         }

         var2 = false;
         return var2;
      }

      public final String f() {
         return this.intervalType;
      }

      public final String g() {
         return this.kycRejectedReason;
      }

      public final String h() {
         return this.kycRejectedCustomerMessage;
      }

      public int hashCode() {
         int var12 = 0;
         String var13 = this.ticketId;
         int var1;
         if(var13 != null) {
            var1 = var13.hashCode();
         } else {
            var1 = 0;
         }

         var13 = this.currency;
         int var2;
         if(var13 != null) {
            var2 = var13.hashCode();
         } else {
            var2 = 0;
         }

         Long var14 = this.amount;
         int var3;
         if(var14 != null) {
            var3 = var14.hashCode();
         } else {
            var3 = 0;
         }

         LocalDateTime var15 = this.intervalStart;
         int var4;
         if(var15 != null) {
            var4 = var15.hashCode();
         } else {
            var4 = 0;
         }

         var15 = this.intervalEnd;
         int var5;
         if(var15 != null) {
            var5 = var15.hashCode();
         } else {
            var5 = 0;
         }

         var13 = this.intervalType;
         int var6;
         if(var13 != null) {
            var6 = var13.hashCode();
         } else {
            var6 = 0;
         }

         var13 = this.kycRejectedReason;
         int var7;
         if(var13 != null) {
            var7 = var13.hashCode();
         } else {
            var7 = 0;
         }

         var13 = this.kycRejectedCustomerMessage;
         int var8;
         if(var13 != null) {
            var8 = var13.hashCode();
         } else {
            var8 = 0;
         }

         var13 = this.sddMigrationRejectionNote;
         int var9;
         if(var13 != null) {
            var9 = var13.hashCode();
         } else {
            var9 = 0;
         }

         var13 = this.title;
         int var10;
         if(var13 != null) {
            var10 = var13.hashCode();
         } else {
            var10 = 0;
         }

         var13 = this.body;
         int var11;
         if(var13 != null) {
            var11 = var13.hashCode();
         } else {
            var11 = 0;
         }

         var13 = this.imageUrl;
         if(var13 != null) {
            var12 = var13.hashCode();
         }

         return (var11 + (var10 + (var9 + (var8 + (var7 + (var6 + (var5 + (var4 + (var3 + (var2 + var1 * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31 + var12;
      }

      public final String i() {
         return this.sddMigrationRejectionNote;
      }

      public final String j() {
         return this.title;
      }

      public final String k() {
         return this.body;
      }

      public final String l() {
         return this.imageUrl;
      }

      public String toString() {
         return "Params(ticketId=" + this.ticketId + ", currency=" + this.currency + ", amount=" + this.amount + ", intervalStart=" + this.intervalStart + ", intervalEnd=" + this.intervalEnd + ", intervalType=" + this.intervalType + ", kycRejectedReason=" + this.kycRejectedReason + ", kycRejectedCustomerMessage=" + this.kycRejectedCustomerMessage + ", sddMigrationRejectionNote=" + this.sddMigrationRejectionNote + ", title=" + this.title + ", body=" + this.body + ", imageUrl=" + this.imageUrl + ")";
      }
   }
}
