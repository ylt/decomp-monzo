package co.uk.getmondo.api.model.feed;

import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0017\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001BA\u0012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\tJ\u000b\u0010\u0012\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u0010\u0010\u0013\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0002\u0010\rJ\u000b\u0010\u0014\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u0015\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u0016\u001a\u0004\u0018\u00010\u0003HÆ\u0003JJ\u0010\u0017\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u0003HÆ\u0001¢\u0006\u0002\u0010\u0018J\u0013\u0010\u0019\u001a\u00020\u00052\b\u0010\u001a\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\u0006\u0010\u001b\u001a\u00020\u0005J\t\u0010\u001c\u001a\u00020\u001dHÖ\u0001J\t\u0010\u001e\u001a\u00020\u0003HÖ\u0001R\u0013\u0010\b\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0015\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\n\n\u0002\u0010\u000e\u001a\u0004\b\f\u0010\rR\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u000bR\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u000bR\u0013\u0010\u0007\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u000b¨\u0006\u001f"},
   d2 = {"Lco/uk/getmondo/api/model/feed/ApiMetadata;", "", "notes", "", "hideAmount", "", "inboundP2pId", "p2pTransferId", "bacsDirectDebitInstructionId", "(Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getBacsDirectDebitInstructionId", "()Ljava/lang/String;", "getHideAmount", "()Ljava/lang/Boolean;", "Ljava/lang/Boolean;", "getInboundP2pId", "getNotes", "getP2pTransferId", "component1", "component2", "component3", "component4", "component5", "copy", "(Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lco/uk/getmondo/api/model/feed/ApiMetadata;", "equals", "other", "hasNotes", "hashCode", "", "toString", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ApiMetadata {
   private final String bacsDirectDebitInstructionId;
   private final Boolean hideAmount;
   private final String inboundP2pId;
   private final String notes;
   private final String p2pTransferId;

   public ApiMetadata() {
      this((String)null, (Boolean)null, (String)null, (String)null, (String)null, 31, (i)null);
   }

   public ApiMetadata(String var1, Boolean var2, String var3, String var4, String var5) {
      this.notes = var1;
      this.hideAmount = var2;
      this.inboundP2pId = var3;
      this.p2pTransferId = var4;
      this.bacsDirectDebitInstructionId = var5;
   }

   // $FF: synthetic method
   public ApiMetadata(String var1, Boolean var2, String var3, String var4, String var5, int var6, i var7) {
      if((var6 & 1) != 0) {
         var1 = (String)null;
      }

      if((var6 & 2) != 0) {
         var2 = (Boolean)null;
      }

      if((var6 & 4) != 0) {
         var3 = (String)null;
      }

      if((var6 & 8) != 0) {
         var4 = (String)null;
      }

      if((var6 & 16) != 0) {
         var5 = (String)null;
      }

      this(var1, var2, var3, var4, var5);
   }

   public final boolean a() {
      boolean var1;
      if(this.notes != null) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public final String b() {
      return this.notes;
   }

   public final Boolean c() {
      return this.hideAmount;
   }

   public final String d() {
      return this.inboundP2pId;
   }

   public final String e() {
      return this.p2pTransferId;
   }

   public boolean equals(Object var1) {
      boolean var2;
      if(this != var1) {
         label34: {
            if(var1 instanceof ApiMetadata) {
               ApiMetadata var3 = (ApiMetadata)var1;
               if(l.a(this.notes, var3.notes) && l.a(this.hideAmount, var3.hideAmount) && l.a(this.inboundP2pId, var3.inboundP2pId) && l.a(this.p2pTransferId, var3.p2pTransferId) && l.a(this.bacsDirectDebitInstructionId, var3.bacsDirectDebitInstructionId)) {
                  break label34;
               }
            }

            var2 = false;
            return var2;
         }
      }

      var2 = true;
      return var2;
   }

   public final String f() {
      return this.bacsDirectDebitInstructionId;
   }

   public int hashCode() {
      int var5 = 0;
      String var6 = this.notes;
      int var1;
      if(var6 != null) {
         var1 = var6.hashCode();
      } else {
         var1 = 0;
      }

      Boolean var7 = this.hideAmount;
      int var2;
      if(var7 != null) {
         var2 = var7.hashCode();
      } else {
         var2 = 0;
      }

      var6 = this.inboundP2pId;
      int var3;
      if(var6 != null) {
         var3 = var6.hashCode();
      } else {
         var3 = 0;
      }

      var6 = this.p2pTransferId;
      int var4;
      if(var6 != null) {
         var4 = var6.hashCode();
      } else {
         var4 = 0;
      }

      var6 = this.bacsDirectDebitInstructionId;
      if(var6 != null) {
         var5 = var6.hashCode();
      }

      return (var4 + (var3 + (var2 + var1 * 31) * 31) * 31) * 31 + var5;
   }

   public String toString() {
      return "ApiMetadata(notes=" + this.notes + ", hideAmount=" + this.hideAmount + ", inboundP2pId=" + this.inboundP2pId + ", p2pTransferId=" + this.p2pTransferId + ", bacsDirectDebitInstructionId=" + this.bacsDirectDebitInstructionId + ")";
   }
}
