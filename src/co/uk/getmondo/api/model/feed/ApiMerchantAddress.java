package co.uk.getmondo.api.model.feed;

import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0006\n\u0002\b\u000e\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0006¢\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0010\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0011\u001a\u00020\u0006HÆ\u0003J\t\u0010\u0012\u001a\u00020\u0006HÆ\u0003J1\u0010\u0013\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00062\b\b\u0002\u0010\u0007\u001a\u00020\u0006HÆ\u0001J\u0013\u0010\u0014\u001a\u00020\u00152\b\u0010\u0016\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0017\u001a\u00020\u0018HÖ\u0001J\t\u0010\u0019\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0007\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\fR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\n¨\u0006\u001a"},
   d2 = {"Lco/uk/getmondo/api/model/feed/ApiMerchantAddress;", "", "shortFormatted", "", "formatted", "latitude", "", "longitude", "(Ljava/lang/String;Ljava/lang/String;DD)V", "getFormatted", "()Ljava/lang/String;", "getLatitude", "()D", "getLongitude", "getShortFormatted", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "hashCode", "", "toString", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ApiMerchantAddress {
   private final String formatted;
   private final double latitude;
   private final double longitude;
   private final String shortFormatted;

   public ApiMerchantAddress(String var1, String var2, double var3, double var5) {
      l.b(var1, "shortFormatted");
      l.b(var2, "formatted");
      super();
      this.shortFormatted = var1;
      this.formatted = var2;
      this.latitude = var3;
      this.longitude = var5;
   }

   public final String a() {
      return this.shortFormatted;
   }

   public final double b() {
      return this.latitude;
   }

   public final double c() {
      return this.longitude;
   }

   public boolean equals(Object var1) {
      boolean var2;
      if(this != var1) {
         label32: {
            if(var1 instanceof ApiMerchantAddress) {
               ApiMerchantAddress var3 = (ApiMerchantAddress)var1;
               if(l.a(this.shortFormatted, var3.shortFormatted) && l.a(this.formatted, var3.formatted) && Double.compare(this.latitude, var3.latitude) == 0 && Double.compare(this.longitude, var3.longitude) == 0) {
                  break label32;
               }
            }

            var2 = false;
            return var2;
         }
      }

      var2 = true;
      return var2;
   }

   public int hashCode() {
      int var2 = 0;
      String var6 = this.shortFormatted;
      int var1;
      if(var6 != null) {
         var1 = var6.hashCode();
      } else {
         var1 = 0;
      }

      var6 = this.formatted;
      if(var6 != null) {
         var2 = var6.hashCode();
      }

      long var4 = Double.doubleToLongBits(this.latitude);
      int var3 = (int)(var4 ^ var4 >>> 32);
      var4 = Double.doubleToLongBits(this.longitude);
      return ((var1 * 31 + var2) * 31 + var3) * 31 + (int)(var4 ^ var4 >>> 32);
   }

   public String toString() {
      return "ApiMerchantAddress(shortFormatted=" + this.shortFormatted + ", formatted=" + this.formatted + ", latitude=" + this.latitude + ", longitude=" + this.longitude + ")";
   }
}
