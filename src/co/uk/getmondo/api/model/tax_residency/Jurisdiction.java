package co.uk.getmondo.api.model.tax_residency;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.squareup.moshi.h;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.a.m;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0019\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0005\b\u0086\b\u0018\u0000 12\u00020\u0001:\u000212B\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004B9\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\u0006\u0010\t\u001a\u00020\b\u0012\b\b\u0001\u0010\n\u001a\u00020\b\u0012\u000e\b\u0001\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\r0\f¢\u0006\u0002\u0010\u000eJ\t\u0010 \u001a\u00020\u0006HÆ\u0003J\t\u0010!\u001a\u00020\bHÆ\u0003J\t\u0010\"\u001a\u00020\bHÆ\u0003J\t\u0010#\u001a\u00020\bHÆ\u0003J\u000f\u0010$\u001a\b\u0012\u0004\u0012\u00020\r0\fHÆ\u0003JA\u0010%\u001a\u00020\u00002\b\b\u0003\u0010\u0005\u001a\u00020\u00062\b\b\u0002\u0010\u0007\u001a\u00020\b2\b\b\u0002\u0010\t\u001a\u00020\b2\b\b\u0003\u0010\n\u001a\u00020\b2\u000e\b\u0003\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\r0\fHÆ\u0001J\b\u0010&\u001a\u00020'H\u0016J\u0013\u0010(\u001a\u00020\b2\b\u0010)\u001a\u0004\u0018\u00010*HÖ\u0003J\t\u0010+\u001a\u00020'HÖ\u0001J\t\u0010,\u001a\u00020\u0006HÖ\u0001J\u0018\u0010-\u001a\u00020.2\u0006\u0010/\u001a\u00020\u00032\u0006\u00100\u001a\u00020'H\u0016R\u0011\u0010\n\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0013\u0010\u0011\u001a\u0004\u0018\u00010\u00068F¢\u0006\u0006\u001a\u0004\b\u0012\u0010\u0013R\u0011\u0010\t\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0010R\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0013R\u0011\u0010\u0016\u001a\u00020\b8F¢\u0006\u0006\u001a\u0004\b\u0017\u0010\u0010R\u0011\u0010\u0018\u001a\u00020\r8F¢\u0006\u0006\u001a\u0004\b\u0019\u0010\u001aR\u0011\u0010\u0007\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u0010R\u0013\u0010\u001c\u001a\u0004\u0018\u00010\r8F¢\u0006\u0006\u001a\u0004\b\u001d\u0010\u001aR\u0017\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\r0\f¢\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u001f¨\u00063"},
   d2 = {"Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;", "Landroid/os/Parcelable;", "source", "Landroid/os/Parcel;", "(Landroid/os/Parcel;)V", "countryCode", "", "reportable", "", "completed", "allowNoTin", "tinTypes", "", "Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;", "(Ljava/lang/String;ZZZLjava/util/List;)V", "getAllowNoTin", "()Z", "alpha2CountryCode", "getAlpha2CountryCode", "()Ljava/lang/String;", "getCompleted", "getCountryCode", "hasMultipleTinTypes", "getHasMultipleTinTypes", "primaryTinType", "getPrimaryTinType", "()Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;", "getReportable", "secondaryTinType", "getSecondaryTinType", "getTinTypes", "()Ljava/util/List;", "component1", "component2", "component3", "component4", "component5", "copy", "describeContents", "", "equals", "other", "", "hashCode", "toString", "writeToParcel", "", "dest", "flags", "Companion", "TinType", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class Jurisdiction implements Parcelable {
   public static final Creator CREATOR = (Creator)(new Creator() {
      public Jurisdiction a(Parcel var1) {
         l.b(var1, "source");
         return new Jurisdiction(var1);
      }

      public Jurisdiction[] a(int var1) {
         return new Jurisdiction[var1];
      }

      // $FF: synthetic method
      public Object createFromParcel(Parcel var1) {
         return this.a(var1);
      }

      // $FF: synthetic method
      public Object[] newArray(int var1) {
         return (Object[])this.a(var1);
      }
   });
   public static final Jurisdiction.Companion Companion = new Jurisdiction.Companion((i)null);
   private final boolean allowNoTin;
   private final boolean completed;
   private final String countryCode;
   private final boolean reportable;
   private final List tinTypes;

   public Jurisdiction(Parcel var1) {
      boolean var4 = false;
      l.b(var1, "source");
      String var5 = var1.readString();
      l.a(var5, "source.readString()");
      boolean var2;
      if(1 == var1.readInt()) {
         var2 = true;
      } else {
         var2 = false;
      }

      boolean var3;
      if(1 == var1.readInt()) {
         var3 = true;
      } else {
         var3 = false;
      }

      if(1 == var1.readInt()) {
         var4 = true;
      }

      ArrayList var6 = var1.createTypedArrayList(Jurisdiction.TinType.CREATOR);
      l.a(var6, "source.createTypedArrayList(TinType.CREATOR)");
      this(var5, var2, var3, var4, (List)var6);
   }

   public Jurisdiction(@h(a = "country_code") String var1, boolean var2, boolean var3, @h(a = "allow_no_tin") boolean var4, @h(a = "tin_types") List var5) {
      l.b(var1, "countryCode");
      l.b(var5, "tinTypes");
      super();
      this.countryCode = var1;
      this.reportable = var2;
      this.completed = var3;
      this.allowNoTin = var4;
      this.tinTypes = var5;
   }

   public final String a() {
      co.uk.getmondo.d.i var1 = co.uk.getmondo.d.i.Companion.a(this.countryCode);
      String var2;
      if(var1 != null) {
         var2 = var1.d();
      } else {
         var2 = null;
      }

      return var2;
   }

   public final boolean b() {
      boolean var1;
      if(this.tinTypes.size() >= 2) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public final Jurisdiction.TinType c() {
      return (Jurisdiction.TinType)this.tinTypes.get(0);
   }

   public final Jurisdiction.TinType d() {
      return (Jurisdiction.TinType)m.c(this.tinTypes, 1);
   }

   public int describeContents() {
      return 0;
   }

   public final String e() {
      return this.countryCode;
   }

   public boolean equals(Object var1) {
      boolean var4 = false;
      boolean var3;
      if(this != var1) {
         var3 = var4;
         if(!(var1 instanceof Jurisdiction)) {
            return var3;
         }

         Jurisdiction var5 = (Jurisdiction)var1;
         var3 = var4;
         if(!l.a(this.countryCode, var5.countryCode)) {
            return var3;
         }

         boolean var2;
         if(this.reportable == var5.reportable) {
            var2 = true;
         } else {
            var2 = false;
         }

         var3 = var4;
         if(!var2) {
            return var3;
         }

         if(this.completed == var5.completed) {
            var2 = true;
         } else {
            var2 = false;
         }

         var3 = var4;
         if(!var2) {
            return var3;
         }

         if(this.allowNoTin == var5.allowNoTin) {
            var2 = true;
         } else {
            var2 = false;
         }

         var3 = var4;
         if(!var2) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.tinTypes, var5.tinTypes)) {
            return var3;
         }
      }

      var3 = true;
      return var3;
   }

   public final boolean f() {
      return this.reportable;
   }

   public final boolean g() {
      return this.completed;
   }

   public final boolean h() {
      return this.allowNoTin;
   }

   public int hashCode() {
      throw new RuntimeException("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.copyTypes(TypeTransformer.java:311)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.fixTypes(TypeTransformer.java:226)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:207)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:161)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:433)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:129)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:528)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:425)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:441)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:171)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:271)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:109)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:290)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:33)\n");
   }

   public String toString() {
      return "Jurisdiction(countryCode=" + this.countryCode + ", reportable=" + this.reportable + ", completed=" + this.completed + ", allowNoTin=" + this.allowNoTin + ", tinTypes=" + this.tinTypes + ")";
   }

   public void writeToParcel(Parcel var1, int var2) {
      byte var3 = 1;
      l.b(var1, "dest");
      var1.writeString(this.countryCode);
      byte var4;
      if(this.reportable) {
         var4 = 1;
      } else {
         var4 = 0;
      }

      var1.writeInt(var4);
      if(this.completed) {
         var4 = 1;
      } else {
         var4 = 0;
      }

      var1.writeInt(var4);
      if(this.allowNoTin) {
         var4 = var3;
      } else {
         var4 = 0;
      }

      var1.writeInt(var4);
      var1.writeTypedList(this.tinTypes);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"},
      d2 = {"Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$Companion;", "", "()V", "CREATOR", "Landroid/os/Parcelable$Creator;", "Lco/uk/getmondo/api/model/tax_residency/Jurisdiction;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class Companion {
      private Companion() {
      }

      // $FF: synthetic method
      public Companion(i var1) {
         this();
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\f\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0004\b\u0086\b\u0018\u0000 \u001e2\u00020\u0001:\u0001\u001eB\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004B\u001f\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\b\b\u0001\u0010\u0007\u001a\u00020\u0006\u0012\u0006\u0010\b\u001a\u00020\u0006¢\u0006\u0002\u0010\tJ\t\u0010\u000e\u001a\u00020\u0006HÆ\u0003J\t\u0010\u000f\u001a\u00020\u0006HÆ\u0003J\t\u0010\u0010\u001a\u00020\u0006HÆ\u0003J'\u0010\u0011\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u00062\b\b\u0003\u0010\u0007\u001a\u00020\u00062\b\b\u0002\u0010\b\u001a\u00020\u0006HÆ\u0001J\b\u0010\u0012\u001a\u00020\u0013H\u0016J\u0013\u0010\u0014\u001a\u00020\u00152\b\u0010\u0016\u001a\u0004\u0018\u00010\u0017HÖ\u0003J\t\u0010\u0018\u001a\u00020\u0013HÖ\u0001J\t\u0010\u0019\u001a\u00020\u0006HÖ\u0001J\u0018\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u00032\u0006\u0010\u001d\u001a\u00020\u0013H\u0016R\u0011\u0010\b\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\u0007\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\u000bR\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000b¨\u0006\u001f"},
      d2 = {"Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;", "Landroid/os/Parcelable;", "source", "Landroid/os/Parcel;", "(Landroid/os/Parcel;)V", "id", "", "friendlyName", "example", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getExample", "()Ljava/lang/String;", "getFriendlyName", "getId", "component1", "component2", "component3", "copy", "describeContents", "", "equals", "", "other", "", "hashCode", "toString", "writeToParcel", "", "dest", "flags", "Companion", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class TinType implements Parcelable {
      public static final Creator CREATOR = (Creator)(new Creator() {
         public Jurisdiction.TinType a(Parcel var1) {
            l.b(var1, "source");
            return new Jurisdiction.TinType(var1);
         }

         public Jurisdiction.TinType[] a(int var1) {
            return new Jurisdiction.TinType[var1];
         }

         // $FF: synthetic method
         public Object createFromParcel(Parcel var1) {
            return this.a(var1);
         }

         // $FF: synthetic method
         public Object[] newArray(int var1) {
            return (Object[])this.a(var1);
         }
      });
      public static final Jurisdiction.Companion Companion = new Jurisdiction.Companion((i)null);
      private final String example;
      private final String friendlyName;
      private final String id;

      public TinType(Parcel var1) {
         l.b(var1, "source");
         String var2 = var1.readString();
         l.a(var2, "source.readString()");
         String var3 = var1.readString();
         l.a(var3, "source.readString()");
         String var4 = var1.readString();
         l.a(var4, "source.readString()");
         this(var2, var3, var4);
      }

      public TinType(String var1, @h(a = "friendly_name") String var2, String var3) {
         l.b(var1, "id");
         l.b(var2, "friendlyName");
         l.b(var3, "example");
         super();
         this.id = var1;
         this.friendlyName = var2;
         this.example = var3;
      }

      public final String a() {
         return this.id;
      }

      public final String b() {
         return this.friendlyName;
      }

      public final String c() {
         return this.example;
      }

      public int describeContents() {
         return 0;
      }

      public boolean equals(Object var1) {
         boolean var2;
         if(this != var1) {
            label30: {
               if(var1 instanceof Jurisdiction.TinType) {
                  Jurisdiction.TinType var3 = (Jurisdiction.TinType)var1;
                  if(l.a(this.id, var3.id) && l.a(this.friendlyName, var3.friendlyName) && l.a(this.example, var3.example)) {
                     break label30;
                  }
               }

               var2 = false;
               return var2;
            }
         }

         var2 = true;
         return var2;
      }

      public int hashCode() {
         int var3 = 0;
         String var4 = this.id;
         int var1;
         if(var4 != null) {
            var1 = var4.hashCode();
         } else {
            var1 = 0;
         }

         var4 = this.friendlyName;
         int var2;
         if(var4 != null) {
            var2 = var4.hashCode();
         } else {
            var2 = 0;
         }

         var4 = this.example;
         if(var4 != null) {
            var3 = var4.hashCode();
         }

         return (var2 + var1 * 31) * 31 + var3;
      }

      public String toString() {
         return "TinType(id=" + this.id + ", friendlyName=" + this.friendlyName + ", example=" + this.example + ")";
      }

      public void writeToParcel(Parcel var1, int var2) {
         l.b(var1, "dest");
         var1.writeString(this.id);
         var1.writeString(this.friendlyName);
         var1.writeString(this.example);
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"},
      d2 = {"Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType$Companion;", "", "()V", "CREATOR", "Landroid/os/Parcelable$Creator;", "Lco/uk/getmondo/api/model/tax_residency/Jurisdiction$TinType;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class Companion {
      private Companion() {
      }

      // $FF: synthetic method
      public Companion(i var1) {
         this();
      }
   }
}
