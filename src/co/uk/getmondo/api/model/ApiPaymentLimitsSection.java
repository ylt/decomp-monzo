package co.uk.getmondo.api.model;

import java.util.List;
import kotlin.Metadata;
import kotlin.a.m;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u000e\b\u0002\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006¢\u0006\u0002\u0010\bJ\t\u0010\u000e\u001a\u00020\u0003HÆ\u0003J\t\u0010\u000f\u001a\u00020\u0003HÆ\u0003J\u000f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006HÆ\u0003J-\u0010\u0011\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\u000e\b\u0002\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006HÆ\u0001J\u0013\u0010\u0012\u001a\u00020\u00132\b\u0010\u0014\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0015\u001a\u00020\u0016HÖ\u0001J\t\u0010\u0017\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0017\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\n¨\u0006\u0018"},
   d2 = {"Lco/uk/getmondo/api/model/ApiPaymentLimitsSection;", "", "id", "", "name", "limits", "", "Lco/uk/getmondo/api/model/ApiPaymentLimit;", "(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V", "getId", "()Ljava/lang/String;", "getLimits", "()Ljava/util/List;", "getName", "component1", "component2", "component3", "copy", "equals", "", "other", "hashCode", "", "toString", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ApiPaymentLimitsSection {
   private final String id;
   private final List limits;
   private final String name;

   public ApiPaymentLimitsSection(String var1, String var2, List var3) {
      l.b(var1, "id");
      l.b(var2, "name");
      l.b(var3, "limits");
      super();
      this.id = var1;
      this.name = var2;
      this.limits = var3;
   }

   // $FF: synthetic method
   public ApiPaymentLimitsSection(String var1, String var2, List var3, int var4, i var5) {
      if((var4 & 4) != 0) {
         var3 = m.a();
      }

      this(var1, var2, var3);
   }

   public final String a() {
      return this.id;
   }

   public final String b() {
      return this.name;
   }

   public final List c() {
      return this.limits;
   }

   public boolean equals(Object var1) {
      boolean var2;
      if(this != var1) {
         label30: {
            if(var1 instanceof ApiPaymentLimitsSection) {
               ApiPaymentLimitsSection var3 = (ApiPaymentLimitsSection)var1;
               if(l.a(this.id, var3.id) && l.a(this.name, var3.name) && l.a(this.limits, var3.limits)) {
                  break label30;
               }
            }

            var2 = false;
            return var2;
         }
      }

      var2 = true;
      return var2;
   }

   public int hashCode() {
      int var3 = 0;
      String var4 = this.id;
      int var1;
      if(var4 != null) {
         var1 = var4.hashCode();
      } else {
         var1 = 0;
      }

      var4 = this.name;
      int var2;
      if(var4 != null) {
         var2 = var4.hashCode();
      } else {
         var2 = 0;
      }

      List var5 = this.limits;
      if(var5 != null) {
         var3 = var5.hashCode();
      }

      return (var2 + var1 * 31) * 31 + var3;
   }

   public String toString() {
      return "ApiPaymentLimitsSection(id=" + this.id + ", name=" + this.name + ", limits=" + this.limits + ")";
   }
}
