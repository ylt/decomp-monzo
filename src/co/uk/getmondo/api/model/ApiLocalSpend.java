package co.uk.getmondo.api.model;

import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003HÆ\u0003J\t\u0010\f\u001a\u00020\u0005HÆ\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0011\u001a\u00020\u0012HÖ\u0001J\t\u0010\u0013\u001a\u00020\u0005HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0014"},
   d2 = {"Lco/uk/getmondo/api/model/ApiLocalSpend;", "", "spendToday", "", "currency", "", "(JLjava/lang/String;)V", "getCurrency", "()Ljava/lang/String;", "getSpendToday", "()J", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ApiLocalSpend {
   private final String currency;
   private final long spendToday;

   public ApiLocalSpend(long var1, String var3) {
      l.b(var3, "currency");
      super();
      this.spendToday = var1;
      this.currency = var3;
   }

   public final long a() {
      return this.spendToday;
   }

   public final String b() {
      return this.currency;
   }

   public boolean equals(Object var1) {
      boolean var4 = false;
      boolean var3;
      if(this != var1) {
         var3 = var4;
         if(!(var1 instanceof ApiLocalSpend)) {
            return var3;
         }

         ApiLocalSpend var5 = (ApiLocalSpend)var1;
         boolean var2;
         if(this.spendToday == var5.spendToday) {
            var2 = true;
         } else {
            var2 = false;
         }

         var3 = var4;
         if(!var2) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.currency, var5.currency)) {
            return var3;
         }
      }

      var3 = true;
      return var3;
   }

   public int hashCode() {
      long var3 = this.spendToday;
      int var2 = (int)(var3 ^ var3 >>> 32);
      String var5 = this.currency;
      int var1;
      if(var5 != null) {
         var1 = var5.hashCode();
      } else {
         var1 = 0;
      }

      return var1 + var2 * 31;
   }

   public String toString() {
      return "ApiLocalSpend(spendToday=" + this.spendToday + ", currency=" + this.currency + ")";
   }
}
