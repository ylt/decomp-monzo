package co.uk.getmondo.api.model;

import java.util.Date;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003HÆ\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\f\u001a\u00020\rHÖ\u0001J\t\u0010\u000e\u001a\u00020\u000fHÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0010"},
   d2 = {"Lco/uk/getmondo/api/model/ApiCardReplacementOrder;", "", "estimatedDeliveryDate", "Ljava/util/Date;", "(Ljava/util/Date;)V", "getEstimatedDeliveryDate", "()Ljava/util/Date;", "component1", "copy", "equals", "", "other", "hashCode", "", "toString", "", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ApiCardReplacementOrder {
   private final Date estimatedDeliveryDate;

   public ApiCardReplacementOrder(Date var1) {
      l.b(var1, "estimatedDeliveryDate");
      super();
      this.estimatedDeliveryDate = var1;
   }

   public final Date a() {
      return this.estimatedDeliveryDate;
   }

   public boolean equals(Object var1) {
      boolean var2;
      if(this != var1) {
         label26: {
            if(var1 instanceof ApiCardReplacementOrder) {
               ApiCardReplacementOrder var3 = (ApiCardReplacementOrder)var1;
               if(l.a(this.estimatedDeliveryDate, var3.estimatedDeliveryDate)) {
                  break label26;
               }
            }

            var2 = false;
            return var2;
         }
      }

      var2 = true;
      return var2;
   }

   public int hashCode() {
      Date var2 = this.estimatedDeliveryDate;
      int var1;
      if(var2 != null) {
         var1 = var2.hashCode();
      } else {
         var1 = 0;
      }

      return var1;
   }

   public String toString() {
      return "ApiCardReplacementOrder(estimatedDeliveryDate=" + this.estimatedDeliveryDate + ")";
   }
}
