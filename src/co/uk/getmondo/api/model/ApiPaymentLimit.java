package co.uk.getmondo.api.model;

import com.squareup.moshi.h;
import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u001a\n\u0002\u0010\b\n\u0002\b\u0003\b\u0086\b\u0018\u00002\u00020\u0001:\u0001)BM\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\t\u0012\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\t\u0012\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\f¢\u0006\u0002\u0010\rJ\t\u0010\u001b\u001a\u00020\u0003HÆ\u0003J\t\u0010\u001c\u001a\u00020\u0003HÆ\u0003J\t\u0010\u001d\u001a\u00020\u0006HÆ\u0003J\u000b\u0010\u001e\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u0010\u0010\u001f\u001a\u0004\u0018\u00010\tHÆ\u0003¢\u0006\u0002\u0010\u0012J\u0010\u0010 \u001a\u0004\u0018\u00010\tHÆ\u0003¢\u0006\u0002\u0010\u0012J\u0010\u0010!\u001a\u0004\u0018\u00010\fHÆ\u0003¢\u0006\u0002\u0010\u0018J\\\u0010\"\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00062\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\t2\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\t2\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\fHÆ\u0001¢\u0006\u0002\u0010#J\u0013\u0010$\u001a\u00020\f2\b\u0010%\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010&\u001a\u00020'HÖ\u0001J\t\u0010(\u001a\u00020\u0003HÖ\u0001R\u0013\u0010\u0007\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u000fR\u0015\u0010\b\u001a\u0004\u0018\u00010\t¢\u0006\n\n\u0002\u0010\u0013\u001a\u0004\b\u0011\u0010\u0012R\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u000fR\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016R\u0015\u0010\u000b\u001a\u0004\u0018\u00010\f¢\u0006\n\n\u0002\u0010\u0019\u001a\u0004\b\u0017\u0010\u0018R\u0015\u0010\n\u001a\u0004\u0018\u00010\t¢\u0006\n\n\u0002\u0010\u0013\u001a\u0004\b\u001a\u0010\u0012¨\u0006*"},
   d2 = {"Lco/uk/getmondo/api/model/ApiPaymentLimit;", "", "id", "", "name", "type", "Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;", "currency", "limit", "", "utilization", "unlimited", "", "(Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;)V", "getCurrency", "()Ljava/lang/String;", "getId", "getLimit", "()Ljava/lang/Long;", "Ljava/lang/Long;", "getName", "getType", "()Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;", "getUnlimited", "()Ljava/lang/Boolean;", "Ljava/lang/Boolean;", "getUtilization", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "copy", "(Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;)Lco/uk/getmondo/api/model/ApiPaymentLimit;", "equals", "other", "hashCode", "", "toString", "LimitType", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ApiPaymentLimit {
   private final String currency;
   private final String id;
   private final Long limit;
   private final String name;
   private final ApiPaymentLimit.LimitType type;
   private final Boolean unlimited;
   private final Long utilization;

   public ApiPaymentLimit(String var1, String var2, ApiPaymentLimit.LimitType var3, String var4, Long var5, Long var6, Boolean var7) {
      l.b(var1, "id");
      l.b(var2, "name");
      l.b(var3, "type");
      super();
      this.id = var1;
      this.name = var2;
      this.type = var3;
      this.currency = var4;
      this.limit = var5;
      this.utilization = var6;
      this.unlimited = var7;
   }

   // $FF: synthetic method
   public ApiPaymentLimit(String var1, String var2, ApiPaymentLimit.LimitType var3, String var4, Long var5, Long var6, Boolean var7, int var8, i var9) {
      if((var8 & 8) != 0) {
         var4 = (String)null;
      }

      if((var8 & 16) != 0) {
         var5 = (Long)null;
      }

      if((var8 & 32) != 0) {
         var6 = (Long)null;
      }

      if((var8 & 64) != 0) {
         var7 = Boolean.valueOf(false);
      }

      this(var1, var2, var3, var4, var5, var6, var7);
   }

   public final String a() {
      return this.id;
   }

   public final String b() {
      return this.name;
   }

   public final ApiPaymentLimit.LimitType c() {
      return this.type;
   }

   public final String d() {
      return this.currency;
   }

   public final Long e() {
      return this.limit;
   }

   public boolean equals(Object var1) {
      boolean var2;
      if(this != var1) {
         label38: {
            if(var1 instanceof ApiPaymentLimit) {
               ApiPaymentLimit var3 = (ApiPaymentLimit)var1;
               if(l.a(this.id, var3.id) && l.a(this.name, var3.name) && l.a(this.type, var3.type) && l.a(this.currency, var3.currency) && l.a(this.limit, var3.limit) && l.a(this.utilization, var3.utilization) && l.a(this.unlimited, var3.unlimited)) {
                  break label38;
               }
            }

            var2 = false;
            return var2;
         }
      }

      var2 = true;
      return var2;
   }

   public final Long f() {
      return this.utilization;
   }

   public int hashCode() {
      int var7 = 0;
      String var8 = this.id;
      int var1;
      if(var8 != null) {
         var1 = var8.hashCode();
      } else {
         var1 = 0;
      }

      var8 = this.name;
      int var2;
      if(var8 != null) {
         var2 = var8.hashCode();
      } else {
         var2 = 0;
      }

      ApiPaymentLimit.LimitType var9 = this.type;
      int var3;
      if(var9 != null) {
         var3 = var9.hashCode();
      } else {
         var3 = 0;
      }

      var8 = this.currency;
      int var4;
      if(var8 != null) {
         var4 = var8.hashCode();
      } else {
         var4 = 0;
      }

      Long var10 = this.limit;
      int var5;
      if(var10 != null) {
         var5 = var10.hashCode();
      } else {
         var5 = 0;
      }

      var10 = this.utilization;
      int var6;
      if(var10 != null) {
         var6 = var10.hashCode();
      } else {
         var6 = 0;
      }

      Boolean var11 = this.unlimited;
      if(var11 != null) {
         var7 = var11.hashCode();
      }

      return (var6 + (var5 + (var4 + (var3 + (var2 + var1 * 31) * 31) * 31) * 31) * 31) * 31 + var7;
   }

   public String toString() {
      return "ApiPaymentLimit(id=" + this.id + ", name=" + this.name + ", type=" + this.type + ", currency=" + this.currency + ", limit=" + this.limit + ", utilization=" + this.utilization + ", unlimited=" + this.unlimited + ")";
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0005\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005¨\u0006\u0006"},
      d2 = {"Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;", "", "(Ljava/lang/String;I)V", "AMOUNT", "COUNT", "VIRTUAL", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static enum LimitType {
      @h(
         a = "amount"
      )
      AMOUNT,
      @h(
         a = "count"
      )
      COUNT,
      @h(
         a = "virtual"
      )
      VIRTUAL;

      static {
         ApiPaymentLimit.LimitType var1 = new ApiPaymentLimit.LimitType("AMOUNT", 0);
         AMOUNT = var1;
         ApiPaymentLimit.LimitType var2 = new ApiPaymentLimit.LimitType("COUNT", 1);
         COUNT = var2;
         ApiPaymentLimit.LimitType var0 = new ApiPaymentLimit.LimitType("VIRTUAL", 2);
         VIRTUAL = var0;
      }
   }
}
