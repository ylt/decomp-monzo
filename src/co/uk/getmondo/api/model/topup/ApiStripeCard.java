package co.uk.getmondo.api.model.topup;

import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0012\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0006\u0010\u0007\u001a\u00020\u0003¢\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0010\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0011\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0012\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0013\u001a\u00020\u0003HÆ\u0003J;\u0010\u0014\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u00032\b\b\u0002\u0010\u0007\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\u0015\u001a\u00020\u00162\b\u0010\u0017\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0018\u001a\u00020\u0019HÖ\u0001J\t\u0010\u001a\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0006\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0005\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\nR\u0011\u0010\u0007\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\nR\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\nR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\n¨\u0006\u001b"},
   d2 = {"Lco/uk/getmondo/api/model/topup/ApiStripeCard;", "", "stripeCardId", "", "lastFour", "endDate", "brand", "funding", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getBrand", "()Ljava/lang/String;", "getEndDate", "getFunding", "getLastFour", "getStripeCardId", "component1", "component2", "component3", "component4", "component5", "copy", "equals", "", "other", "hashCode", "", "toString", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ApiStripeCard {
   private final String brand;
   private final String endDate;
   private final String funding;
   private final String lastFour;
   private final String stripeCardId;

   public ApiStripeCard(String var1, String var2, String var3, String var4, String var5) {
      l.b(var1, "stripeCardId");
      l.b(var2, "lastFour");
      l.b(var3, "endDate");
      l.b(var4, "brand");
      l.b(var5, "funding");
      super();
      this.stripeCardId = var1;
      this.lastFour = var2;
      this.endDate = var3;
      this.brand = var4;
      this.funding = var5;
   }

   public final String a() {
      return this.stripeCardId;
   }

   public final String b() {
      return this.lastFour;
   }

   public final String c() {
      return this.endDate;
   }

   public final String d() {
      return this.brand;
   }

   public boolean equals(Object var1) {
      boolean var2;
      if(this != var1) {
         label34: {
            if(var1 instanceof ApiStripeCard) {
               ApiStripeCard var3 = (ApiStripeCard)var1;
               if(l.a(this.stripeCardId, var3.stripeCardId) && l.a(this.lastFour, var3.lastFour) && l.a(this.endDate, var3.endDate) && l.a(this.brand, var3.brand) && l.a(this.funding, var3.funding)) {
                  break label34;
               }
            }

            var2 = false;
            return var2;
         }
      }

      var2 = true;
      return var2;
   }

   public int hashCode() {
      int var5 = 0;
      String var6 = this.stripeCardId;
      int var1;
      if(var6 != null) {
         var1 = var6.hashCode();
      } else {
         var1 = 0;
      }

      var6 = this.lastFour;
      int var2;
      if(var6 != null) {
         var2 = var6.hashCode();
      } else {
         var2 = 0;
      }

      var6 = this.endDate;
      int var3;
      if(var6 != null) {
         var3 = var6.hashCode();
      } else {
         var3 = 0;
      }

      var6 = this.brand;
      int var4;
      if(var6 != null) {
         var4 = var6.hashCode();
      } else {
         var4 = 0;
      }

      var6 = this.funding;
      if(var6 != null) {
         var5 = var6.hashCode();
      }

      return (var4 + (var3 + (var2 + var1 * 31) * 31) * 31) * 31 + var5;
   }

   public String toString() {
      return "ApiStripeCard(stripeCardId=" + this.stripeCardId + ", lastFour=" + this.lastFour + ", endDate=" + this.endDate + ", brand=" + this.brand + ", funding=" + this.funding + ")";
   }
}
