package co.uk.getmondo.api.model.overdraft;

import com.squareup.moshi.h;
import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\b!\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001Be\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\b\b\u0002\u0010\u0007\u001a\u00020\u0003\u0012\b\b\u0003\u0010\b\u001a\u00020\t\u0012\b\b\u0003\u0010\n\u001a\u00020\t\u0012\b\b\u0003\u0010\u000b\u001a\u00020\t\u0012\b\b\u0003\u0010\f\u001a\u00020\t\u0012\b\b\u0003\u0010\r\u001a\u00020\t\u0012\b\b\u0003\u0010\u000e\u001a\u00020\u0003¢\u0006\u0002\u0010\u000fJ\t\u0010\u001d\u001a\u00020\u0003HÆ\u0003J\t\u0010\u001e\u001a\u00020\u0003HÆ\u0003J\t\u0010\u001f\u001a\u00020\u0005HÆ\u0003J\t\u0010 \u001a\u00020\u0005HÆ\u0003J\t\u0010!\u001a\u00020\u0003HÆ\u0003J\t\u0010\"\u001a\u00020\tHÆ\u0003J\t\u0010#\u001a\u00020\tHÆ\u0003J\t\u0010$\u001a\u00020\tHÆ\u0003J\t\u0010%\u001a\u00020\tHÆ\u0003J\t\u0010&\u001a\u00020\tHÆ\u0003Jm\u0010'\u001a\u00020\u00002\b\b\u0003\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00052\b\b\u0002\u0010\u0007\u001a\u00020\u00032\b\b\u0003\u0010\b\u001a\u00020\t2\b\b\u0003\u0010\n\u001a\u00020\t2\b\b\u0003\u0010\u000b\u001a\u00020\t2\b\b\u0003\u0010\f\u001a\u00020\t2\b\b\u0003\u0010\r\u001a\u00020\t2\b\b\u0003\u0010\u000e\u001a\u00020\u0003HÆ\u0001J\u0013\u0010(\u001a\u00020\u00052\b\u0010)\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010*\u001a\u00020+HÖ\u0001J\t\u0010,\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0011\u0010\b\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0015R\u0011\u0010\n\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0013R\u0011\u0010\u000e\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0011R\u0011\u0010\u0007\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0011R\u0011\u0010\u000b\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u0013R\u0011\u0010\u0006\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u0015R\u0011\u0010\f\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u0013R\u0011\u0010\r\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u0013¨\u0006-"},
   d2 = {"Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;", "", "accountId", "", "active", "", "eligible", "currency", "accruedFeesAmount", "", "bufferAmount", "dailyFeeAmount", "limitAmount", "preapprovedLimitAmount", "chargeDate", "(Ljava/lang/String;ZZLjava/lang/String;JJJJJLjava/lang/String;)V", "getAccountId", "()Ljava/lang/String;", "getAccruedFeesAmount", "()J", "getActive", "()Z", "getBufferAmount", "getChargeDate", "getCurrency", "getDailyFeeAmount", "getEligible", "getLimitAmount", "getPreapprovedLimitAmount", "component1", "component10", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "other", "hashCode", "", "toString", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ApiOverdraftStatus {
   private final String accountId;
   private final long accruedFeesAmount;
   private final boolean active;
   private final long bufferAmount;
   private final String chargeDate;
   private final String currency;
   private final long dailyFeeAmount;
   private final boolean eligible;
   private final long limitAmount;
   private final long preapprovedLimitAmount;

   public ApiOverdraftStatus(@h(a = "account_id") String var1, boolean var2, boolean var3, String var4, @h(a = "accrued_fees_amount") long var5, @h(a = "buffer_amount") long var7, @h(a = "daily_fee_amount") long var9, @h(a = "limit_amount") long var11, @h(a = "preapproved_limit_amount") long var13, @h(a = "charge_date") String var15) {
      l.b(var1, "accountId");
      l.b(var4, "currency");
      l.b(var15, "chargeDate");
      super();
      this.accountId = var1;
      this.active = var2;
      this.eligible = var3;
      this.currency = var4;
      this.accruedFeesAmount = var5;
      this.bufferAmount = var7;
      this.dailyFeeAmount = var9;
      this.limitAmount = var11;
      this.preapprovedLimitAmount = var13;
      this.chargeDate = var15;
   }

   // $FF: synthetic method
   public ApiOverdraftStatus(String var1, boolean var2, boolean var3, String var4, long var5, long var7, long var9, long var11, long var13, String var15, int var16, i var17) {
      if((var16 & 8) != 0) {
         var4 = "";
      }

      if((var16 & 16) != 0) {
         var5 = 0L;
      }

      if((var16 & 32) != 0) {
         var7 = 0L;
      }

      if((var16 & 64) != 0) {
         var9 = 0L;
      }

      if((var16 & 128) != 0) {
         var11 = 0L;
      }

      if((var16 & 256) != 0) {
         var13 = 0L;
      }

      if((var16 & 512) != 0) {
         var15 = "";
      }

      this(var1, var2, var3, var4, var5, var7, var9, var11, var13, var15);
   }

   public final String a() {
      return this.accountId;
   }

   public final boolean b() {
      return this.active;
   }

   public final boolean c() {
      return this.eligible;
   }

   public final String d() {
      return this.currency;
   }

   public final long e() {
      return this.accruedFeesAmount;
   }

   public boolean equals(Object var1) {
      boolean var4 = false;
      boolean var3;
      if(this != var1) {
         var3 = var4;
         if(!(var1 instanceof ApiOverdraftStatus)) {
            return var3;
         }

         ApiOverdraftStatus var5 = (ApiOverdraftStatus)var1;
         var3 = var4;
         if(!l.a(this.accountId, var5.accountId)) {
            return var3;
         }

         boolean var2;
         if(this.active == var5.active) {
            var2 = true;
         } else {
            var2 = false;
         }

         var3 = var4;
         if(!var2) {
            return var3;
         }

         if(this.eligible == var5.eligible) {
            var2 = true;
         } else {
            var2 = false;
         }

         var3 = var4;
         if(!var2) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.currency, var5.currency)) {
            return var3;
         }

         if(this.accruedFeesAmount == var5.accruedFeesAmount) {
            var2 = true;
         } else {
            var2 = false;
         }

         var3 = var4;
         if(!var2) {
            return var3;
         }

         if(this.bufferAmount == var5.bufferAmount) {
            var2 = true;
         } else {
            var2 = false;
         }

         var3 = var4;
         if(!var2) {
            return var3;
         }

         if(this.dailyFeeAmount == var5.dailyFeeAmount) {
            var2 = true;
         } else {
            var2 = false;
         }

         var3 = var4;
         if(!var2) {
            return var3;
         }

         if(this.limitAmount == var5.limitAmount) {
            var2 = true;
         } else {
            var2 = false;
         }

         var3 = var4;
         if(!var2) {
            return var3;
         }

         if(this.preapprovedLimitAmount == var5.preapprovedLimitAmount) {
            var2 = true;
         } else {
            var2 = false;
         }

         var3 = var4;
         if(!var2) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.chargeDate, var5.chargeDate)) {
            return var3;
         }
      }

      var3 = true;
      return var3;
   }

   public final long f() {
      return this.bufferAmount;
   }

   public final long g() {
      return this.dailyFeeAmount;
   }

   public final long h() {
      return this.limitAmount;
   }

   public int hashCode() {
      throw new RuntimeException("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.copyTypes(TypeTransformer.java:311)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.fixTypes(TypeTransformer.java:226)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:207)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:161)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:433)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:129)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:528)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:425)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:441)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:171)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:271)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:109)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:290)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:33)\n");
   }

   public final long i() {
      return this.preapprovedLimitAmount;
   }

   public final String j() {
      return this.chargeDate;
   }

   public String toString() {
      return "ApiOverdraftStatus(accountId=" + this.accountId + ", active=" + this.active + ", eligible=" + this.eligible + ", currency=" + this.currency + ", accruedFeesAmount=" + this.accruedFeesAmount + ", bufferAmount=" + this.bufferAmount + ", dailyFeeAmount=" + this.dailyFeeAmount + ", limitAmount=" + this.limitAmount + ", preapprovedLimitAmount=" + this.preapprovedLimitAmount + ", chargeDate=" + this.chargeDate + ")";
   }
}
