package co.uk.getmondo.api.model;

import com.squareup.moshi.h;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u001d\b\u0086\b\u0018\u00002\u00020\u0001B[\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\n\b\u0001\u0010\u0005\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\b\b\u0001\u0010\t\u001a\u00020\n\u0012\b\b\u0001\u0010\u000b\u001a\u00020\u0003\u0012\b\b\u0001\u0010\f\u001a\u00020\u0003\u0012\b\b\u0001\u0010\r\u001a\u00020\u000e¢\u0006\u0002\u0010\u000fJ\t\u0010\u001d\u001a\u00020\u0003HÆ\u0003J\t\u0010\u001e\u001a\u00020\u0003HÆ\u0003J\u000b\u0010\u001f\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\t\u0010 \u001a\u00020\u0003HÆ\u0003J\t\u0010!\u001a\u00020\bHÆ\u0003J\t\u0010\"\u001a\u00020\nHÆ\u0003J\t\u0010#\u001a\u00020\u0003HÆ\u0003J\t\u0010$\u001a\u00020\u0003HÆ\u0003J\t\u0010%\u001a\u00020\u000eHÆ\u0003Je\u0010&\u001a\u00020\u00002\b\b\u0003\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\n\b\u0003\u0010\u0005\u001a\u0004\u0018\u00010\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u00032\b\b\u0002\u0010\u0007\u001a\u00020\b2\b\b\u0003\u0010\t\u001a\u00020\n2\b\b\u0003\u0010\u000b\u001a\u00020\u00032\b\b\u0003\u0010\f\u001a\u00020\u00032\b\b\u0003\u0010\r\u001a\u00020\u000eHÆ\u0001J\u0013\u0010'\u001a\u00020\n2\b\u0010(\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010)\u001a\u00020\u000eHÖ\u0001J\t\u0010*\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0007\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0011\u0010\t\u001a\u00020\n¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u0011\u0010\u000b\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0015R\u0011\u0010\u0006\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0015R\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0015R\u0011\u0010\f\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0015R\u0013\u0010\u0005\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u0015R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u0015R\u0011\u0010\r\u001a\u00020\u000e¢\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u001c¨\u0006+"},
   d2 = {"Lco/uk/getmondo/api/model/ApiProfile;", "", "userId", "", "name", "preferredName", "email", "address", "Lco/uk/getmondo/api/model/LegacyApiAddress;", "addressUpdatable", "", "dateOfBirth", "phoneNumber", "userNumber", "", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/LegacyApiAddress;ZLjava/lang/String;Ljava/lang/String;I)V", "getAddress", "()Lco/uk/getmondo/api/model/LegacyApiAddress;", "getAddressUpdatable", "()Z", "getDateOfBirth", "()Ljava/lang/String;", "getEmail", "getName", "getPhoneNumber", "getPreferredName", "getUserId", "getUserNumber", "()I", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "other", "hashCode", "toString", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ApiProfile {
   private final LegacyApiAddress address;
   private final boolean addressUpdatable;
   private final String dateOfBirth;
   private final String email;
   private final String name;
   private final String phoneNumber;
   private final String preferredName;
   private final String userId;
   private final int userNumber;

   public ApiProfile(@h(a = "user_id") String var1, String var2, @h(a = "preferred_name") String var3, String var4, LegacyApiAddress var5, @h(a = "address_updatable") boolean var6, @h(a = "date_of_birth") String var7, @h(a = "phone_number") String var8, @h(a = "user_number") int var9) {
      l.b(var1, "userId");
      l.b(var2, "name");
      l.b(var4, "email");
      l.b(var5, "address");
      l.b(var7, "dateOfBirth");
      l.b(var8, "phoneNumber");
      super();
      this.userId = var1;
      this.name = var2;
      this.preferredName = var3;
      this.email = var4;
      this.address = var5;
      this.addressUpdatable = var6;
      this.dateOfBirth = var7;
      this.phoneNumber = var8;
      this.userNumber = var9;
   }

   public final String a() {
      return this.userId;
   }

   public final String b() {
      return this.name;
   }

   public final String c() {
      return this.preferredName;
   }

   public final String d() {
      return this.email;
   }

   public final LegacyApiAddress e() {
      return this.address;
   }

   public boolean equals(Object var1) {
      boolean var4 = false;
      boolean var3;
      if(this != var1) {
         var3 = var4;
         if(!(var1 instanceof ApiProfile)) {
            return var3;
         }

         ApiProfile var5 = (ApiProfile)var1;
         var3 = var4;
         if(!l.a(this.userId, var5.userId)) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.name, var5.name)) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.preferredName, var5.preferredName)) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.email, var5.email)) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.address, var5.address)) {
            return var3;
         }

         boolean var2;
         if(this.addressUpdatable == var5.addressUpdatable) {
            var2 = true;
         } else {
            var2 = false;
         }

         var3 = var4;
         if(!var2) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.dateOfBirth, var5.dateOfBirth)) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.phoneNumber, var5.phoneNumber)) {
            return var3;
         }

         if(this.userNumber == var5.userNumber) {
            var2 = true;
         } else {
            var2 = false;
         }

         var3 = var4;
         if(!var2) {
            return var3;
         }
      }

      var3 = true;
      return var3;
   }

   public final boolean f() {
      return this.addressUpdatable;
   }

   public final String g() {
      return this.dateOfBirth;
   }

   public final String h() {
      return this.phoneNumber;
   }

   public int hashCode() {
      throw new RuntimeException("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.copyTypes(TypeTransformer.java:311)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.fixTypes(TypeTransformer.java:226)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:207)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:161)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:433)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:129)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:528)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:425)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:441)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:171)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:271)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:109)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:290)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:33)\n");
   }

   public final int i() {
      return this.userNumber;
   }

   public String toString() {
      return "ApiProfile(userId=" + this.userId + ", name=" + this.name + ", preferredName=" + this.preferredName + ", email=" + this.email + ", address=" + this.address + ", addressUpdatable=" + this.addressUpdatable + ", dateOfBirth=" + this.dateOfBirth + ", phoneNumber=" + this.phoneNumber + ", userNumber=" + this.userNumber + ")";
   }
}
