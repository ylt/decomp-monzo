package co.uk.getmondo.api;

import okhttp3.logging.HttpLoggingInterceptor;

public final class k implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final c b;

   static {
      boolean var0;
      if(!k.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   public k(c var1) {
      if(!a && var1 == null) {
         throw new AssertionError();
      } else {
         this.b = var1;
      }
   }

   public static b.a.b a(c var0) {
      return new k(var0);
   }

   public HttpLoggingInterceptor a() {
      return (HttpLoggingInterceptor)b.a.d.a(this.b.d(), "Cannot return null from a non-@Nullable @Provides method");
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
