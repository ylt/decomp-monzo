package co.uk.getmondo.api;

import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class ae {
   private final OkHttpClient a;

   public ae(OkHttpClient var1) {
      this.a = var1.newBuilder().writeTimeout(1L, TimeUnit.MINUTES).readTimeout(1L, TimeUnit.MINUTES).build();
   }

   // $FF: synthetic method
   static void a(ae var0, InputStream var1, long var2, String var4, String var5) throws Exception {
      ah var7 = new ah(var1, var2, var4);
      Request var8 = (new Request.Builder()).url(var5).put(var7).build();
      Response var6 = var0.a.newCall(var8).execute();
      if(!var6.isSuccessful()) {
         throw new IOException("Error uploading file: " + var6);
      }
   }

   public io.reactivex.b a(String var1, InputStream var2, long var3, String var5) {
      return io.reactivex.b.a(af.a(this, var2, var3, var5, var1));
   }
}
