package co.uk.getmondo.api;

import co.uk.getmondo.api.model.signup.SignupSource;
import java.io.IOException;
import kotlin.Metadata;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u000f\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0012\u0010\u0005\u001a\u0004\u0018\u00010\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0017R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\t"},
   d2 = {"Lco/uk/getmondo/api/SignUpSourceInterceptor;", "Lokhttp3/Interceptor;", "signupSource", "Lco/uk/getmondo/api/model/signup/SignupSource;", "(Lco/uk/getmondo/api/model/signup/SignupSource;)V", "intercept", "Lokhttp3/Response;", "chain", "Lokhttp3/Interceptor$Chain;", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ai implements Interceptor {
   private final SignupSource a;

   public ai(SignupSource var1) {
      kotlin.d.b.l.b(var1, "signupSource");
      super();
      this.a = var1;
   }

   public Response intercept(Interceptor.Chain var1) throws IOException {
      kotlin.d.b.l.b(var1, "chain");
      Request var3 = var1.request();
      HttpUrl var2 = var3.url().newBuilder().addQueryParameter("source", this.a.a()).build();
      return var1.proceed(var3.newBuilder().url(var2).build());
   }
}
