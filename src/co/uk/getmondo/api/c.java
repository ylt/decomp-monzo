package co.uk.getmondo.api;

import android.content.Context;
import android.net.Uri;
import co.uk.getmondo.api.authentication.MonzoOAuthApi;
import co.uk.getmondo.api.model.signup.SignupSource;
import co.uk.getmondo.profile.data.MonzoProfileApi;
import java.lang.reflect.Type;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import okhttp3.Authenticator;
import okhttp3.CertificatePinner;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import org.threeten.bp.LocalDate;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.ZonedDateTime;
import retrofit2.CallAdapter;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.moshi.MoshiConverterFactory;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000Ð\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0007\u0018\u00002\u00020\u0001:\u0003[\\]B\u0005¢\u0006\u0002\u0010\u0002J \u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0002JA\u0010\u000b\u001a\u00020\b2\b\b\u0001\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\b2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\b\b\u0001\u0010\u0013\u001a\u00020\u00062\u0006\u0010\u0014\u001a\u00020\u0015H\u0001¢\u0006\u0002\b\u0016J\r\u0010\u0017\u001a\u00020\bH\u0001¢\u0006\u0002\b\u0018J/\u0010\u0019\u001a\u00020\b2\b\b\u0001\u0010\u001a\u001a\u00020\b2\u0006\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020 H\u0001¢\u0006\u0002\b!J/\u0010\"\u001a\u00020\b2\b\b\u0001\u0010\u001a\u001a\u00020\b2\u0006\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020 H\u0001¢\u0006\u0002\b#J)\u0010$\u001a\u00020\u00042\b\b\u0001\u0010%\u001a\u00020\b2\b\b\u0001\u0010\u0013\u001a\u00020\u00062\u0006\u0010&\u001a\u00020'H\u0001¢\u0006\u0002\b(J\r\u0010)\u001a\u00020'H\u0001¢\u0006\u0002\b*J)\u0010+\u001a\u00020,2\b\b\u0001\u0010-\u001a\u00020\b2\u0006\u0010.\u001a\u00020/2\b\b\u0001\u0010\u0013\u001a\u00020\u0006H\u0001¢\u0006\u0002\b0J\r\u00101\u001a\u00020\u0010H\u0001¢\u0006\u0002\b2J\u0015\u00103\u001a\u0002042\u0006\u00105\u001a\u00020\u0004H\u0001¢\u0006\u0002\b6J)\u00107\u001a\u0002082\b\b\u0001\u0010-\u001a\u00020\b2\u0006\u0010.\u001a\u00020/2\b\b\u0001\u0010\u0013\u001a\u00020\u0006H\u0001¢\u0006\u0002\b9JA\u0010:\u001a\u00020;2\b\b\u0001\u0010\u001a\u001a\u00020\b2\u0006\u0010&\u001a\u00020'2\u0006\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020 2\b\b\u0001\u0010\u0013\u001a\u00020\u0006H\u0001¢\u0006\u0002\b<J\u0015\u0010=\u001a\u00020>2\u0006\u00105\u001a\u00020\u0004H\u0001¢\u0006\u0002\b?J\u0015\u0010@\u001a\u00020A2\u0006\u00105\u001a\u00020\u0004H\u0001¢\u0006\u0002\bBJ\r\u0010C\u001a\u00020/H\u0001¢\u0006\u0002\bDJ1\u0010E\u001a\u00020F2\b\b\u0001\u0010\u001a\u001a\u00020\b2\u0006\u0010&\u001a\u00020'2\u0006\u0010\u001d\u001a\u00020\u001e2\b\b\u0001\u0010\u0013\u001a\u00020\u0006H\u0001¢\u0006\u0002\bGJ)\u0010H\u001a\u00020I2\b\b\u0001\u0010-\u001a\u00020\b2\u0006\u0010.\u001a\u00020/2\b\b\u0001\u0010\u0013\u001a\u00020\u0006H\u0001¢\u0006\u0002\bJJ)\u0010K\u001a\u00020L2\b\b\u0001\u0010-\u001a\u00020\b2\u0006\u0010.\u001a\u00020/2\b\b\u0001\u0010\u0013\u001a\u00020\u0006H\u0001¢\u0006\u0002\bMJ)\u0010N\u001a\u00020O2\b\b\u0001\u0010-\u001a\u00020\b2\u0006\u0010.\u001a\u00020/2\b\b\u0001\u0010\u0013\u001a\u00020\u0006H\u0001¢\u0006\u0002\bPJ/\u0010Q\u001a\u00020R2\u0006\u0010\u000e\u001a\u00020\b2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010&\u001a\u00020'2\b\b\u0001\u0010S\u001a\u00020\u0006H\u0001¢\u0006\u0002\bTJ)\u0010U\u001a\u00020V2\b\b\u0001\u0010-\u001a\u00020\b2\u0006\u0010.\u001a\u00020/2\b\b\u0001\u0010\u0013\u001a\u00020\u0006H\u0001¢\u0006\u0002\bWJ)\u0010X\u001a\u00020Y2\b\b\u0001\u0010-\u001a\u00020\b2\u0006\u0010.\u001a\u00020/2\b\b\u0001\u0010\u0013\u001a\u00020\u0006H\u0001¢\u0006\u0002\bZ¨\u0006^"},
   d2 = {"Lco/uk/getmondo/api/ApiModule;", "", "()V", "buildRetrofit", "Lretrofit2/Retrofit;", "baseUrl", "", "client", "Lokhttp3/OkHttpClient;", "converterFactory", "Lretrofit2/Converter$Factory;", "provideBaseMonzoOkHttpClient", "context", "Landroid/content/Context;", "baseOkHttpClient", "loggingInterceptor", "Lokhttp3/logging/HttpLoggingInterceptor;", "additionalHeadersInterceptor", "Lco/uk/getmondo/api/AdditionalHeadersInterceptor;", "apiUrl", "preferences", "Lco/uk/getmondo/developer_options/DeveloperOptionsPreferences;", "provideBaseMonzoOkHttpClient$app_monzoPrepaidRelease", "provideBaseOkHttpClient", "provideBaseOkHttpClient$app_monzoPrepaidRelease", "provideConfiguredForSignUpOkHttpClient", "baseMonzoHttpClient", "bearerAuthInterceptor", "Lco/uk/getmondo/api/authentication/BearerAuthInterceptor;", "errorHandlerInterceptor", "Lco/uk/getmondo/api/ErrorHandlerInterceptor;", "monzoAuthenticator", "Lco/uk/getmondo/api/authentication/MonzoAuthenticator;", "provideConfiguredForSignUpOkHttpClient$app_monzoPrepaidRelease", "provideConfiguredOkHttpClient", "provideConfiguredOkHttpClient$app_monzoPrepaidRelease", "provideDefaultMonzoRetrofit", "configuredMonzoHttpClient", "gson", "Lcom/google/gson/Gson;", "provideDefaultMonzoRetrofit$app_monzoPrepaidRelease", "provideGson", "provideGson$app_monzoPrepaidRelease", "provideHelpApi", "Lco/uk/getmondo/api/HelpApi;", "okHttpClient", "moshi", "Lcom/squareup/moshi/Moshi;", "provideHelpApi$app_monzoPrepaidRelease", "provideHttpLoggingInterceptor", "provideHttpLoggingInterceptor$app_monzoPrepaidRelease", "provideIdentityVerificationApi", "Lco/uk/getmondo/api/IdentityVerificationApi;", "retrofit", "provideIdentityVerificationApi$app_monzoPrepaidRelease", "provideMigrationApi", "Lco/uk/getmondo/api/MigrationApi;", "provideMigrationApi$app_monzoPrepaidRelease", "provideMonzoAnalyticsApi", "Lco/uk/getmondo/api/AnalyticsApi;", "provideMonzoAnalyticsApi$app_monzoPrepaidRelease", "provideMonzoApi", "Lco/uk/getmondo/api/MonzoApi;", "provideMonzoApi$app_monzoPrepaidRelease", "provideMonzoPaymentsApi", "Lco/uk/getmondo/api/PaymentsApi;", "provideMonzoPaymentsApi$app_monzoPrepaidRelease", "provideMoshi", "provideMoshi$app_monzoPrepaidRelease", "provideOAuthMonzoApi", "Lco/uk/getmondo/api/authentication/MonzoOAuthApi;", "provideOAuthMonzoApi$app_monzoPrepaidRelease", "provideOverdraftApi", "Lco/uk/getmondo/api/OverdraftApi;", "provideOverdraftApi$app_monzoPrepaidRelease", "providePaymentLimitsApi", "Lco/uk/getmondo/api/PaymentLimitsApi;", "providePaymentLimitsApi$app_monzoPrepaidRelease", "provideProfileApi", "Lco/uk/getmondo/profile/data/MonzoProfileApi;", "provideProfileApi$app_monzoPrepaidRelease", "provideServiceStatusApi", "Lco/uk/getmondo/api/ServiceStatusApi;", "statusApiUrl", "provideServiceStatusApi$app_monzoPrepaidRelease", "provideSignUpApi", "Lco/uk/getmondo/api/SignupApi;", "provideSignUpApi$app_monzoPrepaidRelease", "provideTaxResidencyApi", "Lco/uk/getmondo/api/TaxResidencyApi;", "provideTaxResidencyApi$app_monzoPrepaidRelease", "ConfiguredForSignUpMonzoHttpClient", "ConfiguredMonzoHttpClient", "MonzoHttpClient", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class c {
   private final Retrofit a(String var1, OkHttpClient var2, Converter.Factory var3) {
      Retrofit var4 = (new Retrofit.Builder()).baseUrl(var1).client(var2).addConverterFactory(var3).addCallAdapterFactory((CallAdapter.Factory)RxJava2CallAdapterFactory.create()).build();
      kotlin.d.b.l.a(var4, "Retrofit.Builder()\n     …\n                .build()");
      return var4;
   }

   public final AnalyticsApi a(OkHttpClient var1, com.google.gson.f var2, co.uk.getmondo.api.authentication.b var3, ac var4, co.uk.getmondo.api.authentication.k var5, String var6) {
      kotlin.d.b.l.b(var1, "baseMonzoHttpClient");
      kotlin.d.b.l.b(var2, "gson");
      kotlin.d.b.l.b(var3, "bearerAuthInterceptor");
      kotlin.d.b.l.b(var4, "errorHandlerInterceptor");
      kotlin.d.b.l.b(var5, "monzoAuthenticator");
      kotlin.d.b.l.b(var6, "apiUrl");
      var4.a(false);
      var1 = var1.newBuilder().authenticator((Authenticator)var5).addInterceptor((Interceptor)var3).addInterceptor((Interceptor)var4).build();
      kotlin.d.b.l.a(var1, "okHttpClient");
      GsonConverterFactory var8 = GsonConverterFactory.create(var2);
      kotlin.d.b.l.a(var8, "GsonConverterFactory.create(gson)");
      Object var7 = this.a(var6, var1, (Converter.Factory)var8).create(AnalyticsApi.class);
      kotlin.d.b.l.a(var7, "buildRetrofit(apiUrl, ok…AnalyticsApi::class.java)");
      return (AnalyticsApi)var7;
   }

   public final MonzoApi a(Retrofit var1) {
      kotlin.d.b.l.b(var1, "retrofit");
      Object var2 = var1.create(MonzoApi.class);
      kotlin.d.b.l.a(var2, "retrofit.create(MonzoApi::class.java)");
      return (MonzoApi)var2;
   }

   public final ServiceStatusApi a(OkHttpClient var1, HttpLoggingInterceptor var2, com.google.gson.f var3, String var4) {
      kotlin.d.b.l.b(var1, "baseOkHttpClient");
      kotlin.d.b.l.b(var2, "loggingInterceptor");
      kotlin.d.b.l.b(var3, "gson");
      kotlin.d.b.l.b(var4, "statusApiUrl");
      var1 = var1.newBuilder().addInterceptor((Interceptor)var2).build();
      Object var5 = (new Retrofit.Builder()).baseUrl(var4).client(var1).addConverterFactory((Converter.Factory)GsonConverterFactory.create(var3)).addCallAdapterFactory((CallAdapter.Factory)RxJava2CallAdapterFactory.create()).build().create(ServiceStatusApi.class);
      kotlin.d.b.l.a(var5, "Retrofit.Builder()\n     …iceStatusApi::class.java)");
      return (ServiceStatusApi)var5;
   }

   public final TaxResidencyApi a(OkHttpClient var1, com.squareup.moshi.v var2, String var3) {
      kotlin.d.b.l.b(var1, "okHttpClient");
      kotlin.d.b.l.b(var2, "moshi");
      kotlin.d.b.l.b(var3, "apiUrl");
      MoshiConverterFactory var5 = MoshiConverterFactory.create(var2);
      kotlin.d.b.l.a(var5, "MoshiConverterFactory.create(moshi)");
      Object var4 = this.a(var3, var1, (Converter.Factory)var5).create(TaxResidencyApi.class);
      kotlin.d.b.l.a(var4, "buildRetrofit(apiUrl, ok…ResidencyApi::class.java)");
      return (TaxResidencyApi)var4;
   }

   public final MonzoOAuthApi a(OkHttpClient var1, com.google.gson.f var2, ac var3, String var4) {
      kotlin.d.b.l.b(var1, "baseMonzoHttpClient");
      kotlin.d.b.l.b(var2, "gson");
      kotlin.d.b.l.b(var3, "errorHandlerInterceptor");
      kotlin.d.b.l.b(var4, "apiUrl");
      var3.a(false);
      var1 = var1.newBuilder().addInterceptor((Interceptor)var3).build();
      kotlin.d.b.l.a(var1, "okHttpClient");
      GsonConverterFactory var6 = GsonConverterFactory.create(var2);
      kotlin.d.b.l.a(var6, "GsonConverterFactory.create(gson)");
      Object var5 = this.a(var4, var1, (Converter.Factory)var6).create(MonzoOAuthApi.class);
      kotlin.d.b.l.a(var5, "buildRetrofit(apiUrl, ok…onzoOAuthApi::class.java)");
      return (MonzoOAuthApi)var5;
   }

   public final com.squareup.moshi.v a() {
      com.squareup.moshi.v var1 = (new com.squareup.moshi.v.a()).a((com.squareup.moshi.i.a)(new com.squareup.moshi.r())).a((Object)(new co.uk.getmondo.signup_old.ac())).a((Object)(new co.uk.getmondo.common.c.b())).a();
      kotlin.d.b.l.a(var1, "Moshi.Builder()\n        …\n                .build()");
      return var1;
   }

   public final OkHttpClient a(Context var1, OkHttpClient var2, HttpLoggingInterceptor var3, a var4, String var5, co.uk.getmondo.developer_options.a var6) {
      kotlin.d.b.l.b(var1, "context");
      kotlin.d.b.l.b(var2, "baseOkHttpClient");
      kotlin.d.b.l.b(var3, "loggingInterceptor");
      kotlin.d.b.l.b(var4, "additionalHeadersInterceptor");
      kotlin.d.b.l.b(var5, "apiUrl");
      kotlin.d.b.l.b(var6, "preferences");
      var5 = Uri.parse(var5).getHost();
      CertificatePinner.Builder var8 = new CertificatePinner.Builder();
      String[] var11 = co.uk.getmondo.a.a;

      for(int var7 = 0; var7 < var11.length; ++var7) {
         var8.add(var5, new String[]{var11[var7]});
      }

      OkHttpClient.Builder var10 = var2.newBuilder().addInterceptor((Interceptor)var3).addInterceptor((Interceptor)var4);
      var10.certificatePinner(var8.build());
      OkHttpClient var9 = var10.build();
      kotlin.d.b.l.a(var9, "builder.build()");
      return var9;
   }

   public final OkHttpClient a(OkHttpClient var1, co.uk.getmondo.api.authentication.b var2, ac var3, co.uk.getmondo.api.authentication.k var4) {
      kotlin.d.b.l.b(var1, "baseMonzoHttpClient");
      kotlin.d.b.l.b(var2, "bearerAuthInterceptor");
      kotlin.d.b.l.b(var3, "errorHandlerInterceptor");
      kotlin.d.b.l.b(var4, "monzoAuthenticator");
      var3.a(true);
      var1 = var1.newBuilder().authenticator((Authenticator)var4).addInterceptor((Interceptor)var3).addInterceptor((Interceptor)var2).build();
      kotlin.d.b.l.a(var1, "baseMonzoHttpClient.newB…\n                .build()");
      return var1;
   }

   public final Retrofit a(OkHttpClient var1, String var2, com.google.gson.f var3) {
      kotlin.d.b.l.b(var1, "configuredMonzoHttpClient");
      kotlin.d.b.l.b(var2, "apiUrl");
      kotlin.d.b.l.b(var3, "gson");
      var1 = var1.newBuilder().readTimeout(120L, TimeUnit.SECONDS).build();
      kotlin.d.b.l.a(var1, "okHttpClient");
      GsonConverterFactory var4 = GsonConverterFactory.create(var3);
      kotlin.d.b.l.a(var4, "GsonConverterFactory.create(gson)");
      return this.a(var2, var1, (Converter.Factory)var4);
   }

   public final IdentityVerificationApi b(Retrofit var1) {
      kotlin.d.b.l.b(var1, "retrofit");
      Object var2 = var1.create(IdentityVerificationApi.class);
      kotlin.d.b.l.a(var2, "retrofit.create(Identity…ificationApi::class.java)");
      return (IdentityVerificationApi)var2;
   }

   public final MonzoProfileApi b(OkHttpClient var1, com.squareup.moshi.v var2, String var3) {
      kotlin.d.b.l.b(var1, "okHttpClient");
      kotlin.d.b.l.b(var2, "moshi");
      kotlin.d.b.l.b(var3, "apiUrl");
      MoshiConverterFactory var5 = MoshiConverterFactory.create(var2);
      kotlin.d.b.l.a(var5, "MoshiConverterFactory.create(moshi)");
      Object var4 = this.a(var3, var1, (Converter.Factory)var5).create(MonzoProfileApi.class);
      kotlin.d.b.l.a(var4, "buildRetrofit(apiUrl, ok…zoProfileApi::class.java)");
      return (MonzoProfileApi)var4;
   }

   public final com.google.gson.f b() {
      com.google.gson.f var1 = (new com.google.gson.g()).a(com.google.gson.d.d).a((com.google.gson.t)(new co.uk.getmondo.common.g.a())).a((Type)LocalDateTime.class, null.a).a((Type)LocalDate.class, null.a).a((Type)ZonedDateTime.class, null.a).a("yyyy-MM-dd'T'HH:mm:ss.SSSZ").a();
      kotlin.d.b.l.a(var1, "GsonBuilder()\n          …                .create()");
      return var1;
   }

   public final OkHttpClient b(OkHttpClient var1, co.uk.getmondo.api.authentication.b var2, ac var3, co.uk.getmondo.api.authentication.k var4) {
      kotlin.d.b.l.b(var1, "baseMonzoHttpClient");
      kotlin.d.b.l.b(var2, "bearerAuthInterceptor");
      kotlin.d.b.l.b(var3, "errorHandlerInterceptor");
      kotlin.d.b.l.b(var4, "monzoAuthenticator");
      var3.a(true);
      OkHttpClient.Builder var5 = var1.newBuilder().authenticator((Authenticator)var4).addInterceptor((Interceptor)var3).addInterceptor((Interceptor)var2);
      var5.interceptors().add(0, new ai(SignupSource.PERSONAL_ACCOUNT));
      var1 = var5.build();
      kotlin.d.b.l.a(var1, "builder.build()");
      return var1;
   }

   public final HelpApi c(OkHttpClient var1, com.squareup.moshi.v var2, String var3) {
      kotlin.d.b.l.b(var1, "okHttpClient");
      kotlin.d.b.l.b(var2, "moshi");
      kotlin.d.b.l.b(var3, "apiUrl");
      MoshiConverterFactory var5 = MoshiConverterFactory.create(var2);
      kotlin.d.b.l.a(var5, "MoshiConverterFactory.create(moshi)");
      Object var4 = this.a(var3, var1, (Converter.Factory)var5).create(HelpApi.class);
      kotlin.d.b.l.a(var4, "buildRetrofit(apiUrl, ok…eate(HelpApi::class.java)");
      return (HelpApi)var4;
   }

   public final PaymentsApi c(Retrofit var1) {
      kotlin.d.b.l.b(var1, "retrofit");
      Object var2 = var1.create(PaymentsApi.class);
      kotlin.d.b.l.a(var2, "retrofit.create(PaymentsApi::class.java)");
      return (PaymentsApi)var2;
   }

   public final OkHttpClient c() {
      OkHttpClient var1 = (new OkHttpClient.Builder()).build();
      kotlin.d.b.l.a(var1, "OkHttpClient.Builder().build()");
      return var1;
   }

   public final SignupApi d(OkHttpClient var1, com.squareup.moshi.v var2, String var3) {
      kotlin.d.b.l.b(var1, "okHttpClient");
      kotlin.d.b.l.b(var2, "moshi");
      kotlin.d.b.l.b(var3, "apiUrl");
      MoshiConverterFactory var5 = MoshiConverterFactory.create(var2);
      kotlin.d.b.l.a(var5, "MoshiConverterFactory.create(moshi)");
      Object var4 = this.a(var3, var1, (Converter.Factory)var5).create(SignupApi.class);
      kotlin.d.b.l.a(var4, "buildRetrofit(apiUrl, ok…te(SignupApi::class.java)");
      return (SignupApi)var4;
   }

   public final HttpLoggingInterceptor d() {
      HttpLoggingInterceptor var1 = new HttpLoggingInterceptor();
      var1.setLevel(HttpLoggingInterceptor.Level.NONE);
      return var1;
   }

   public final MigrationApi e(OkHttpClient var1, com.squareup.moshi.v var2, String var3) {
      kotlin.d.b.l.b(var1, "okHttpClient");
      kotlin.d.b.l.b(var2, "moshi");
      kotlin.d.b.l.b(var3, "apiUrl");
      MoshiConverterFactory var5 = MoshiConverterFactory.create(var2);
      kotlin.d.b.l.a(var5, "MoshiConverterFactory.create(moshi)");
      Object var4 = this.a(var3, var1, (Converter.Factory)var5).create(MigrationApi.class);
      kotlin.d.b.l.a(var4, "buildRetrofit(apiUrl, ok…MigrationApi::class.java)");
      return (MigrationApi)var4;
   }

   public final OverdraftApi f(OkHttpClient var1, com.squareup.moshi.v var2, String var3) {
      kotlin.d.b.l.b(var1, "okHttpClient");
      kotlin.d.b.l.b(var2, "moshi");
      kotlin.d.b.l.b(var3, "apiUrl");
      MoshiConverterFactory var5 = MoshiConverterFactory.create(var2);
      kotlin.d.b.l.a(var5, "MoshiConverterFactory.create(moshi)");
      Object var4 = this.a(var3, var1, (Converter.Factory)var5).create(OverdraftApi.class);
      kotlin.d.b.l.a(var4, "buildRetrofit(apiUrl, ok…OverdraftApi::class.java)");
      return (OverdraftApi)var4;
   }

   public final PaymentLimitsApi g(OkHttpClient var1, com.squareup.moshi.v var2, String var3) {
      kotlin.d.b.l.b(var1, "okHttpClient");
      kotlin.d.b.l.b(var2, "moshi");
      kotlin.d.b.l.b(var3, "apiUrl");
      MoshiConverterFactory var5 = MoshiConverterFactory.create(var2);
      kotlin.d.b.l.a(var5, "MoshiConverterFactory.create(moshi)");
      Object var4 = this.a(var3, var1, (Converter.Factory)var5).create(PaymentLimitsApi.class);
      kotlin.d.b.l.a(var4, "buildRetrofit(apiUrl, ok…entLimitsApi::class.java)");
      return (PaymentLimitsApi)var4;
   }
}
