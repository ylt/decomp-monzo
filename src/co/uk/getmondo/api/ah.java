package co.uk.getmondo.api;

import java.io.IOException;
import java.io.InputStream;
import okhttp3.MediaType;
import okhttp3.RequestBody;

class ah extends RequestBody {
   private final InputStream a;
   private final long b;
   private final String c;

   ah(InputStream var1, long var2, String var4) {
      this.a = var1;
      this.b = var2;
      this.c = var4;
   }

   public long contentLength() {
      return this.b;
   }

   public MediaType contentType() {
      return MediaType.parse(this.c);
   }

   public void writeTo(c.d param1) throws IOException {
      // $FF: Couldn't be decompiled
   }
}
