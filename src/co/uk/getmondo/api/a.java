package co.uk.getmondo.api;

import android.content.Context;
import android.provider.Settings.Secure;
import java.io.IOException;
import java.util.UUID;
import okhttp3.Headers;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class a implements Interceptor {
   private final Context a;
   private final co.uk.getmondo.common.s b;

   public a(Context var1, co.uk.getmondo.common.s var2) {
      this.a = var1;
      this.b = var2;
   }

   private String a() {
      String var1 = Secure.getString(this.a.getContentResolver(), "android_id");
      return UUID.nameUUIDFromBytes(var1.getBytes()) + ";1";
   }

   private String b() {
      String var2 = this.b.c();
      String var1 = var2;
      if(var2 == null) {
         var1 = UUID.randomUUID().toString();
         this.b.c(var1);
      }

      return var1;
   }

   private String c() {
      String var2 = this.b.d();
      String var1 = var2;
      if(var2 == null) {
         var1 = UUID.randomUUID().toString();
         this.b.d(var1);
      }

      return var1;
   }

   public Response intercept(Interceptor.Chain var1) throws IOException {
      Request var3 = var1.request();
      Headers.Builder var2 = var3.headers().newBuilder().add("Mondo-Build", String.valueOf(1140150)).add("Mondo-Version", "1.14.1").add("Prod", "1").add("Mondo-Idempotence", this.a()).add("User-Agent", co.uk.getmondo.common.k.p.g(System.getProperty("http.agent"))).add("Mondo-Session", this.b()).add("Mondo-Fingerprint", this.c()).add("__auth_v2", "true");
      return var1.proceed(var3.newBuilder().headers(var2.build()).build());
   }
}
