package co.uk.getmondo.api;

import java.io.InputStream;

// $FF: synthetic class
final class af implements io.reactivex.c.a {
   private final ae a;
   private final InputStream b;
   private final long c;
   private final String d;
   private final String e;

   private af(ae var1, InputStream var2, long var3, String var5, String var6) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
      this.d = var5;
      this.e = var6;
   }

   public static io.reactivex.c.a a(ae var0, InputStream var1, long var2, String var4, String var5) {
      return new af(var0, var1, var2, var4, var5);
   }

   public void a() {
      ae.a(this.a, this.b, this.c, this.d, this.e);
   }
}
