package co.uk.getmondo.api;

import java.io.IOException;
import java.util.Locale;

public class ApiException extends IOException {
   private final int a;
   private final String b;
   private final String c;
   private final co.uk.getmondo.api.model.b d;

   public ApiException(int var1, String var2, String var3, co.uk.getmondo.api.model.b var4) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
      this.d = var4;
   }

   public boolean a() {
      boolean var1;
      if(this.a == 404) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public int b() {
      return this.a;
   }

   public String c() {
      return this.b;
   }

   public String d() {
      return this.c;
   }

   public co.uk.getmondo.api.model.b e() {
      return this.d;
   }

   public String getMessage() {
      String var1;
      if(this.d != null) {
         var1 = this.d.toString();
      } else {
         var1 = "";
      }

      return String.format(Locale.ENGLISH, "Request to %s failed with %d due to: %s", new Object[]{this.b, Integer.valueOf(this.a), var1});
   }
}
