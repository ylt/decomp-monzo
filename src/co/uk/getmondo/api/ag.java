package co.uk.getmondo.api;

import okhttp3.OkHttpClient;

public final class ag implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;

   static {
      boolean var0;
      if(!ag.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   public ag(javax.a.a var1) {
      if(!a && var1 == null) {
         throw new AssertionError();
      } else {
         this.b = var1;
      }
   }

   public static b.a.b a(javax.a.a var0) {
      return new ag(var0);
   }

   public ae a() {
      return new ae((OkHttpClient)this.b.b());
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
