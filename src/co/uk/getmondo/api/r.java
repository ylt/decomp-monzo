package co.uk.getmondo.api;

import co.uk.getmondo.api.authentication.MonzoOAuthApi;
import okhttp3.OkHttpClient;

public final class r implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final c b;
   private final javax.a.a c;
   private final javax.a.a d;
   private final javax.a.a e;
   private final javax.a.a f;

   static {
      boolean var0;
      if(!r.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   public r(c var1, javax.a.a var2, javax.a.a var3, javax.a.a var4, javax.a.a var5) {
      if(!a && var1 == null) {
         throw new AssertionError();
      } else {
         this.b = var1;
         if(!a && var2 == null) {
            throw new AssertionError();
         } else {
            this.c = var2;
            if(!a && var3 == null) {
               throw new AssertionError();
            } else {
               this.d = var3;
               if(!a && var4 == null) {
                  throw new AssertionError();
               } else {
                  this.e = var4;
                  if(!a && var5 == null) {
                     throw new AssertionError();
                  } else {
                     this.f = var5;
                  }
               }
            }
         }
      }
   }

   public static b.a.b a(c var0, javax.a.a var1, javax.a.a var2, javax.a.a var3, javax.a.a var4) {
      return new r(var0, var1, var2, var3, var4);
   }

   public MonzoOAuthApi a() {
      return (MonzoOAuthApi)b.a.d.a(this.b.a((OkHttpClient)this.c.b(), (com.google.gson.f)this.d.b(), (ac)this.e.b(), (String)this.f.b()), "Cannot return null from a non-@Nullable @Provides method");
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
