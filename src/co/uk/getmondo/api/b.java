package co.uk.getmondo.api;

import android.content.Context;

public final class b implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;
   private final javax.a.a c;

   static {
      boolean var0;
      if(!b.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   public b(javax.a.a var1, javax.a.a var2) {
      if(!a && var1 == null) {
         throw new AssertionError();
      } else {
         this.b = var1;
         if(!a && var2 == null) {
            throw new AssertionError();
         } else {
            this.c = var2;
         }
      }
   }

   public static b.a.b a(javax.a.a var0, javax.a.a var1) {
      return new b(var0, var1);
   }

   public a a() {
      return new a((Context)this.b.b(), (co.uk.getmondo.common.s)this.c.b());
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
