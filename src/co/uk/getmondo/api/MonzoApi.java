package co.uk.getmondo.api;

import co.uk.getmondo.api.model.signup.SignupSource;
import java.util.List;
import java.util.Map;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MonzoApi {
   @GET("account-settings")
   io.reactivex.v accountSettings(@Query("account_id") String var1);

   @GET("accounts")
   io.reactivex.v accounts();

   @FormUrlEncoded
   @POST("card-activation/activate")
   io.reactivex.v activateCardBank(@Field("account_id") String var1, @Field("card_pan") String var2);

   @FormUrlEncoded
   @POST("card-activation/activate")
   io.reactivex.v activateCardPrepaid(@Field("account_id") String var1, @Field("card_token") String var2);

   @FormUrlEncoded
   @POST("card-replacement/activate")
   io.reactivex.v activateReplacementCard(@Field("account_id") String var1, @Field("old_card_id") String var2, @Field("new_card_token") String var3);

   @FormUrlEncoded
   @POST("stripe/cards")
   io.reactivex.v addStripeCard(@Field("account_id") String var1, @Field("stripe_token") String var2, @Field("iin") String var3, @Field("save") boolean var4, @Field("idempotency_key") String var5);

   @FormUrlEncoded
   @POST("signup/apply")
   io.reactivex.b applyForAccount(@Query("source") SignupSource var1, @Field("v2_enabled") boolean var2, @FieldMap Map var3, @Field("address[street_address][]") String... var4);

   @GET("balance")
   io.reactivex.v balance(@Query("account_id") String var1);

   @GET("balance/limits")
   io.reactivex.v balanceLimits(@Query("account_id") String var1);

   @GET("card/list")
   io.reactivex.v cards(@Query("account_id") String var1);

   @FormUrlEncoded
   @POST("phone/check-code")
   io.reactivex.v checkVerificationCode(@Field("phone_number") String var1, @Field("code") String var2);

   @GET("config")
   io.reactivex.v config();

   @FormUrlEncoded
   @POST("signup/create")
   io.reactivex.b createAccount(@Query("source") SignupSource var1, @FieldMap Map var2, @Field("address[street_address][]") String... var3);

   @FormUrlEncoded
   @POST("attachment/upload")
   io.reactivex.v createAttachmentUploadUrl(@Field("account_id") String var1, @Field("file_name") String var2, @Field("file_type") String var3);

   @DELETE("feed/{id}")
   io.reactivex.b deleteFeedItem(@Path("id") String var1);

   @FormUrlEncoded
   @POST("attachment/deregister")
   io.reactivex.b deregisterAttachment(@Field("id") String var1);

   @GET("feed")
   io.reactivex.v feed(@Query("account_id") String var1, @Query("start_time") String var2);

   @GET("/card-dispatch/status")
   io.reactivex.v getCardDispatchStatus(@Query("account_id") String var1);

   @GET("topup/status")
   io.reactivex.v getInitialTopupStatus(@Query("account_id") String var1);

   @GET("golden-ticket/{id}")
   io.reactivex.v goldenTicketStatus(@Path("id") String var1);

   @GET("intercom/tokens/{intercom_app_id}")
   io.reactivex.v intercomUserHash(@Path("intercom_app_id") String var1);

   @GET("geocode/postal-code-lookup")
   io.reactivex.v lookupPostcode(@Query("postal_code") String var1, @Query("country") String var2);

   @GET("news")
   io.reactivex.v news();

   @FormUrlEncoded
   @PUT("user-settings")
   io.reactivex.v optInToPeerToPeer(@Field("p2p_enabled") boolean var1);

   @FormUrlEncoded
   @POST("card-replacement/order")
   io.reactivex.b orderReplacementCardBank(@Field("card_id") String var1, @Field("address[street_address][]") String[] var2, @Field("address[locality]") String var3, @Field("address[administrative_area]") String var4, @Field("address[postal_code]") String var5, @Field("address[country]") String var6);

   @FormUrlEncoded
   @POST("card-replacement/order")
   io.reactivex.v orderReplacementCardPrepaid(@Field("card_id") String var1, @Field("address[street_address][]") String[] var2, @Field("address[locality]") String var3, @Field("address[administrative_area]") String var4, @Field("address[postal_code]") String var5, @Field("address[country]") String var6);

   @FormUrlEncoded
   @POST("pin/sms")
   io.reactivex.b pinViaSms(@Field("account_id") String var1, @Field("date_of_birth") String var2);

   @GET("contact-discovery/query")
   io.reactivex.v queryContacts(@Query("hash[][phone]") List var1);

   @FormUrlEncoded
   @POST("attachment/register")
   io.reactivex.v registerAttachment(@Field("account_id") String var1, @Field("external_id") String var2, @Field("file_url") String var3);

   @FormUrlEncoded
   @POST("fcm/register")
   io.reactivex.b registerForFcm(@Field("device_token") String var1, @Field("app_id") String var2);

   @FormUrlEncoded
   @POST("phone/send-code")
   io.reactivex.b sendVerificationCode(@Field("phone_number") String var1);

   @FormUrlEncoded
   @POST("signup")
   io.reactivex.v signup(@Query("source") SignupSource var1, @Field("v2_enabled") boolean var2);

   @GET("pin/sms_blocked")
   io.reactivex.v smsPinBlocked(@Query("account_id") String var1);

   @GET("stripe/cards")
   io.reactivex.v stripeCards(@Query("account_id") String var1);

   @FormUrlEncoded
   @POST("stripe/three_d_secure")
   io.reactivex.v threeDSecure(@Field("account_id") String var1, @Field("currency") String var2, @Field("amount") long var3, @Field("is_initial") boolean var5, @Field("stripe_card_id") String var6, @Field("return_url") String var7, @Field("idempotency_key") String var8);

   @FormUrlEncoded
   @PUT("card/toggle")
   io.reactivex.b toggleCard(@Field("card_id") String var1, @Field("status") co.uk.getmondo.api.model.a var2);

   @GET("topup/limits")
   io.reactivex.v topUpLimits(@Query("account_id") String var1);

   @FormUrlEncoded
   @POST("stripe/top_up")
   io.reactivex.b topUpThreeDSecure(@Field("account_id") String var1, @Field("stripe_source") String var2, @Field("currency") String var3, @Field("amount") long var4, @Field("is_initial") boolean var6, @Field("three_d_secure_decision") String var7, @Field("idempotency_key") String var8);

   @FormUrlEncoded
   @POST("transactions/update-metadata")
   io.reactivex.v updateNotes(@Field("transaction_id") String var1, @Field("metadata[notes]") String var2);

   @FormUrlEncoded
   @PUT("account-settings")
   io.reactivex.v updateSettings(@Field("account_id") String var1, @Field("magstripe_atm_enabled") boolean var2);

   @FormUrlEncoded
   @PATCH("transactions/{transaction_id}")
   io.reactivex.b updateTransactionCategory(@Path("transaction_id") String var1, @Field("category") String var2);

   @GET("waitlist")
   io.reactivex.v waitlist();

   @FormUrlEncoded
   @POST("waitlist/signup")
   io.reactivex.v waitlistSignUp(@FieldMap Map var1);
}
