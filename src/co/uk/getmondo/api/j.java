package co.uk.getmondo.api;

import okhttp3.OkHttpClient;

public final class j implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final c b;
   private final javax.a.a c;
   private final javax.a.a d;
   private final javax.a.a e;

   static {
      boolean var0;
      if(!j.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   public j(c var1, javax.a.a var2, javax.a.a var3, javax.a.a var4) {
      if(!a && var1 == null) {
         throw new AssertionError();
      } else {
         this.b = var1;
         if(!a && var2 == null) {
            throw new AssertionError();
         } else {
            this.c = var2;
            if(!a && var3 == null) {
               throw new AssertionError();
            } else {
               this.d = var3;
               if(!a && var4 == null) {
                  throw new AssertionError();
               } else {
                  this.e = var4;
               }
            }
         }
      }
   }

   public static b.a.b a(c var0, javax.a.a var1, javax.a.a var2, javax.a.a var3) {
      return new j(var0, var1, var2, var3);
   }

   public HelpApi a() {
      return (HelpApi)b.a.d.a(this.b.c((OkHttpClient)this.c.b(), (com.squareup.moshi.v)this.d.b(), (String)this.e.b()), "Cannot return null from a non-@Nullable @Provides method");
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
