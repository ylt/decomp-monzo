package co.uk.getmondo.api.b;

import co.uk.getmondo.api.model.ApiWaitlistPosition;

// $FF: synthetic class
final class b implements io.reactivex.c.h {
   private final co.uk.getmondo.d.a.v a;

   private b(co.uk.getmondo.d.a.v var1) {
      this.a = var1;
   }

   public static io.reactivex.c.h a(co.uk.getmondo.d.a.v var0) {
      return new b(var0);
   }

   public Object a(Object var1) {
      return this.a.a((ApiWaitlistPosition)var1);
   }
}
