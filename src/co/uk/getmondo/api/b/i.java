package co.uk.getmondo.api.b;

import java.util.concurrent.Callable;

// $FF: synthetic class
final class i implements Callable {
   private final a a;

   private i(a var1) {
      this.a = var1;
   }

   public static Callable a(a var0) {
      return new i(var0);
   }

   public Object call() {
      return a.a(this.a);
   }
}
