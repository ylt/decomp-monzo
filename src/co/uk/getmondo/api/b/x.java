package co.uk.getmondo.api.b;

import java.util.concurrent.Callable;

// $FF: synthetic class
final class x implements Callable {
   private final a a;
   private final String b;
   private final String c;

   private x(a var1, String var2, String var3) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
   }

   public static Callable a(a var0, String var1, String var2) {
      return new x(var0, var1, var2);
   }

   public Object call() {
      return a.a(this.a, this.b, this.c);
   }
}
