package co.uk.getmondo.api.b;

import java.util.List;

// $FF: synthetic class
final class v implements io.reactivex.c.i {
   private final co.uk.getmondo.common.accounts.m a;

   private v(co.uk.getmondo.common.accounts.m var1) {
      this.a = var1;
   }

   public static io.reactivex.c.i a(co.uk.getmondo.common.accounts.m var0) {
      return new v(var0);
   }

   public Object a(Object var1, Object var2, Object var3) {
      return this.a.a((List)var1, (com.c.b.b)var2, ((Boolean)var3).booleanValue());
   }
}
