package co.uk.getmondo.api.b;

import co.uk.getmondo.d.ai;
import java.util.concurrent.Callable;

// $FF: synthetic class
final class z implements Callable {
   private final a a;
   private final ai b;
   private final String c;
   private final String d;

   private z(a var1, ai var2, String var3, String var4) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
      this.d = var4;
   }

   public static Callable a(a var0, ai var1, String var2, String var3) {
      return new z(var0, var1, var2, var3);
   }

   public Object call() {
      return a.a(this.a, this.b, this.c, this.d);
   }
}
