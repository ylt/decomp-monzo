package co.uk.getmondo.api.b;

import co.uk.getmondo.api.ApiException;
import co.uk.getmondo.api.MonzoApi;
import co.uk.getmondo.api.authentication.MonzoOAuthApi;
import co.uk.getmondo.api.authentication.OAuthException;
import co.uk.getmondo.api.model.ApiAccount;
import co.uk.getmondo.api.model.ApiToken;
import co.uk.getmondo.api.model.signup.SignupSource;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.d.ai;
import co.uk.getmondo.d.ak;
import co.uk.getmondo.d.an;
import co.uk.getmondo.profile.data.MonzoProfileApi;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.UUID;

public class a {
   private final co.uk.getmondo.common.s a;
   private final co.uk.getmondo.common.accounts.d b;
   private final MonzoApi c;
   private final MonzoOAuthApi d;
   private final MonzoProfileApi e;
   private final co.uk.getmondo.card.c f;
   private final co.uk.getmondo.common.k.f g;
   private final co.uk.getmondo.common.a h;
   private final co.uk.getmondo.common.q i;
   private final co.uk.getmondo.common.g j;
   private final co.uk.getmondo.common.accounts.m k;
   private co.uk.getmondo.payments.send.data.h l;

   public a(co.uk.getmondo.common.s var1, co.uk.getmondo.common.accounts.d var2, MonzoApi var3, MonzoOAuthApi var4, MonzoProfileApi var5, co.uk.getmondo.card.c var6, co.uk.getmondo.common.k.f var7, co.uk.getmondo.common.a var8, co.uk.getmondo.common.q var9, co.uk.getmondo.common.g var10, co.uk.getmondo.common.accounts.m var11, co.uk.getmondo.payments.send.data.h var12) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
      this.d = var4;
      this.e = var5;
      this.f = var6;
      this.g = var7;
      this.h = var8;
      this.i = var9;
      this.j = var10;
      this.k = var11;
      this.l = var12;
   }

   // $FF: synthetic method
   static android.support.v4.g.j a(ApiToken var0) throws Exception {
      return android.support.v4.g.j.a((new co.uk.getmondo.d.a.s()).a(var0), var0.d());
   }

   // $FF: synthetic method
   static ai a(a var0, ai var1, String var2, String var3) throws Exception {
      var0.b.e();
      var0.b.a(var1, var2, var3);
      return var1;
   }

   // $FF: synthetic method
   static ak a(a var0, co.uk.getmondo.common.accounts.l var1) throws Exception {
      return var0.b.b();
   }

   // $FF: synthetic method
   static io.reactivex.v a(a var0, ApiAccount var1) {
      return var0.a(var1);
   }

   private io.reactivex.v a(ApiAccount var1) {
      io.reactivex.v var2;
      if(co.uk.getmondo.d.a.b.a(var1.f()) == co.uk.getmondo.d.a.b.RETAIL) {
         var2 = this.f.a(var1.a()).d(j.a());
      } else {
         var2 = io.reactivex.v.a(this.c(var1.a()), this.f.a(var1.a()), k.a());
      }

      return var2.d(l.a(var1)).a(co.uk.getmondo.d.a.class);
   }

   // $FF: synthetic method
   static io.reactivex.z a(a var0) throws Exception {
      return io.reactivex.v.a((Object)var0.l.d());
   }

   // $FF: synthetic method
   static io.reactivex.z a(a var0, ai var1) throws Exception {
      io.reactivex.v var2 = var0.c();
      io.reactivex.v var5 = var0.e();
      io.reactivex.v var3 = var0.d();
      co.uk.getmondo.common.accounts.m var4 = var0.k;
      var4.getClass();
      return io.reactivex.v.a(var2, var5, var3, v.a(var4));
   }

   // $FF: synthetic method
   static io.reactivex.z a(a var0, String var1, android.support.v4.g.j var2) throws Exception {
      return var0.a((ai)var2.a, var1, (String)var2.b);
   }

   // $FF: synthetic method
   static io.reactivex.z a(a var0, String var1, String var2) throws Exception {
      String var3 = var0.a.a();
      io.reactivex.v var4;
      if(!var1.equals(var0.b(var3, var0.a.b()))) {
         var4 = io.reactivex.v.a((Throwable)(new OAuthException(var3)));
      } else {
         var4 = var0.d.requestUserToken(MonzoOAuthApi.a, "authorization_code", var2, "https://monzo.com/-magic-auth").d(w.a()).a(y.a(var0, var3));
      }

      return var4;
   }

   // $FF: synthetic method
   static io.reactivex.z a(a var0, List var1) throws Exception {
      return io.reactivex.n.fromIterable(var1).flatMapSingle(u.a(var0)).toList();
   }

   // $FF: synthetic method
   static io.reactivex.z a(Throwable var0) throws Exception {
      io.reactivex.v var1;
      if(var0 instanceof ApiException && ((ApiException)var0).a()) {
         var1 = io.reactivex.v.a((Object)com.c.b.b.c());
      } else {
         var1 = io.reactivex.v.a(var0);
      }

      return var1;
   }

   // $FF: synthetic method
   static Object a(ApiAccount var0, co.uk.getmondo.d.a.a var1) throws Exception {
      return var1.a(var0);
   }

   // $FF: synthetic method
   static void a(a var0, ak var1) throws Exception {
      var0.i.a(var1.d(), var1.c());
   }

   // $FF: synthetic method
   static void a(a var0, co.uk.getmondo.signup_old.a.a.a var1, String var2, an var3) throws Exception {
      co.uk.getmondo.d.ac var4 = new co.uk.getmondo.d.ac(var3.c(), var1.a(), (String)null, var2, var1.b(), (String)null, (Integer)null, var1.c(), false);
      var0.b.a(var4, var3);
   }

   // $FF: synthetic method
   static void a(a var0, Throwable var1) throws Exception {
      if(var1 instanceof ApiException) {
         ApiException var4 = (ApiException)var1;
         int var2 = var4.b();
         String var3 = var4.c();
         String var5 = var4.d();
         co.uk.getmondo.api.model.b var6 = var4.e();
         var0.h.a(Impression.b(var2, var3, var5, var6));
      }

   }

   // $FF: synthetic method
   static ak b(a var0, co.uk.getmondo.common.accounts.l var1) throws Exception {
      return var0.b.b();
   }

   private io.reactivex.b b(co.uk.getmondo.signup_old.a.a.a var1) {
      String var2 = this.a.a();
      io.reactivex.v var3 = this.c.waitlistSignUp((new co.uk.getmondo.api.a.a(var2, var1.a(), var1.c().j(), var1.b(), var1.c().c())).a());
      co.uk.getmondo.d.a.v var4 = new co.uk.getmondo.d.a.v();
      var4.getClass();
      return var3.d(b.a(var4)).c(m.a(this, var1, var2)).c();
   }

   private String b(String var1, String var2) {
      try {
         co.uk.getmondo.common.k.f var4 = this.g;
         StringBuilder var3 = new StringBuilder();
         var1 = var4.a(var3.append(var2).append(var1).toString());
      } catch (NoSuchAlgorithmException var5) {
         var1 = var2;
      }

      return var1;
   }

   // $FF: synthetic method
   static void b(a var0, ak var1) throws Exception {
      var0.i.a(var1.b());
      var0.j.a(var1.b());
   }

   // $FF: synthetic method
   static void b(a var0, Throwable var1) throws Exception {
      var0.b.e();
   }

   private io.reactivex.v c() {
      return this.c.accounts().d(g.a()).a(h.a(this));
   }

   private io.reactivex.v c(String var1) {
      return this.c.getInitialTopupStatus(var1).d(n.a());
   }

   private io.reactivex.v d() {
      return this.l.a().a((io.reactivex.z)io.reactivex.v.a(i.a(this)));
   }

   private io.reactivex.v e() {
      io.reactivex.v var1 = this.e.profile();
      co.uk.getmondo.d.a.p var2 = new co.uk.getmondo.d.a.p();
      var2.getClass();
      return var1.d(o.a(var2)).d(p.a()).f(q.a());
   }

   public io.reactivex.b a(co.uk.getmondo.create_account.a.a.a var1) {
      return this.c.createAccount(SignupSource.LEGACY_PREPAID, var1.a(), var1.b().h()).a((io.reactivex.z)this.a()).c();
   }

   public io.reactivex.b a(co.uk.getmondo.signup_old.a.a.a var1) {
      return this.b(var1);
   }

   public io.reactivex.b a(String var1) {
      String var2 = UUID.randomUUID().toString();
      this.a.b(var2);
      this.a.a(var1);
      return this.d.authorize(MonzoOAuthApi.a, var1, "oauthclient_000097JsUCy1aF4Hud2iJN", "code", "https://monzo.com/-magic-auth", this.b(var1, var2));
   }

   public io.reactivex.v a() {
      io.reactivex.v var4 = this.c();
      io.reactivex.v var1 = this.e();
      io.reactivex.v var3 = this.d();
      co.uk.getmondo.common.accounts.m var2 = this.k;
      var2.getClass();
      var1 = io.reactivex.v.a(var4, var1, var3, c.a(var2));
      co.uk.getmondo.common.accounts.d var5 = this.b;
      var5.getClass();
      return var1.c(d.a(var5)).d(e.a(this)).c(f.a(this));
   }

   public io.reactivex.v a(ai var1, String var2, String var3) {
      io.reactivex.v var4 = io.reactivex.v.c(z.a(this, var1, var2, var3)).a(aa.a(this));
      co.uk.getmondo.common.accounts.d var5 = this.b;
      var5.getClass();
      return var4.c(ab.a(var5)).d(ac.a(this)).d(ad.a(this)).c(ae.a(this));
   }

   public io.reactivex.v a(String var1, String var2) {
      return io.reactivex.v.a(x.a(this, var2, var1));
   }

   public io.reactivex.b b(String var1) {
      return this.d.logOut("Bearer " + var1).a(t.a(this));
   }

   public io.reactivex.v b() {
      io.reactivex.v var1 = this.c.waitlist();
      co.uk.getmondo.d.a.v var2 = new co.uk.getmondo.d.a.v();
      var2.getClass();
      io.reactivex.v var4 = var1.d(r.a(var2));
      co.uk.getmondo.common.accounts.d var3 = this.b;
      var3.getClass();
      return var4.c(s.a(var3));
   }
}
