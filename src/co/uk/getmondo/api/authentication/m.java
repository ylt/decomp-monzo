package co.uk.getmondo.api.authentication;

import co.uk.getmondo.api.ApiException;
import co.uk.getmondo.d.ai;
import co.uk.getmondo.d.a.s;
import io.reactivex.v;
import java.util.UUID;

public class m {
   private final MonzoOAuthApi a;
   private final co.uk.getmondo.common.accounts.o b;

   m(MonzoOAuthApi var1, co.uk.getmondo.common.accounts.o var2) {
      this.a = var1;
      this.b = var2;
   }

   public static String a() {
      return UUID.randomUUID().toString();
   }

   // $FF: synthetic method
   static boolean a(Integer var0, Throwable var1) throws Exception {
      boolean var4 = true;
      boolean var3;
      if(var0.intValue() >= 3) {
         var3 = false;
      } else {
         var3 = var4;
         if(var1 instanceof ApiException) {
            int var2 = ((ApiException)var1).b();
            if(var2 != 401) {
               var3 = var4;
               if(var2 != 403) {
                  return var3;
               }
            }

            var3 = false;
         }
      }

      return var3;
   }

   v a(ai var1) {
      v var2 = this.a.refreshToken(MonzoOAuthApi.a, "refresh_token", var1.c().a(), var1.c().b()).a(n.a());
      s var3 = new s();
      var3.getClass();
      var2 = var2.d(o.a(var3));
      co.uk.getmondo.common.accounts.o var4 = this.b;
      var4.getClass();
      return var2.c(p.a(var4));
   }
}
