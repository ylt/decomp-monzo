package co.uk.getmondo.api.authentication;

import android.content.Context;
import android.content.SharedPreferences;
import co.uk.getmondo.api.model.ApiToken;

public class i {
   private final com.google.gson.f a;
   private SharedPreferences b;

   i(Context var1, com.google.gson.f var2) {
      this.a = var2;
      this.b = var1.getSharedPreferences("client-storage", 0);
   }

   ApiToken a() {
      ApiToken var1 = null;
      String var2 = this.b.getString("KEY_TOKEN", (String)null);
      if(var2 != null) {
         var1 = (ApiToken)this.a.a(var2, ApiToken.class);
      }

      return var1;
   }

   void a(ApiToken var1) {
      this.b.edit().putString("KEY_TOKEN", this.a.a(var1)).apply();
   }
}
