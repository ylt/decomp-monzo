package co.uk.getmondo.api.authentication;

import co.uk.getmondo.api.model.ApiToken;
import co.uk.getmondo.d.ai;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import okhttp3.Authenticator;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.Route;

public class k implements Authenticator {
   static final long a;
   private final co.uk.getmondo.common.accounts.o b;
   private final d c;
   private final com.google.gson.f d;
   private final m e;

   static {
      a = TimeUnit.SECONDS.toMillis(30L);
   }

   k(co.uk.getmondo.common.accounts.o var1, d var2, com.google.gson.f var3, m var4) {
      this.b = var1;
      this.c = var2;
      this.d = var3;
      this.e = var4;
   }

   private String a(Request var1) {
      String var2 = var1.header("Authorization");
      if(var2 != null && var2.contains("Bearer ")) {
         var2 = var2.replace("Bearer ", "").trim();
      } else {
         var2 = null;
      }

      return var2;
   }

   private Request a(Response var1, String var2) {
      ApiToken var4 = (ApiToken)this.c.a().b();
      String var3 = var4.a();
      if(var4.a().equals(var2)) {
         var2 = ((ApiToken)this.c.b().b()).a();
      } else {
         var2 = var3;
      }

      d.a.a.a("Proceeding with new client token %s", new Object[]{var2});
      return this.b(var1, var2);
   }

   private Request a(Response var1, String var2, ai var3) {
      Object var4 = null;
      Request var5;
      if(var3 == null) {
         d.a.a.d("Attempted to refresh user token, but there isn't an existing user token saved", new Object[0]);
         var5 = (Request)var4;
      } else {
         if(!var3.b().equals(var2)) {
            var2 = var3.b();
         } else {
            if(Long.valueOf(var3.a()).longValue() < a) {
               d.a.a.d("Illegal refresh. User token has attempted to be refreshed within less than %d seconds of its creation", new Object[]{Long.valueOf(TimeUnit.MILLISECONDS.toSeconds(a))});
               var5 = (Request)var4;
               return var5;
            }

            var2 = ((ai)this.e.a(var3).b()).b();
         }

         d.a.a.a("Proceeding with new user token %s", new Object[]{var2});
         var5 = this.b(var1, var2);
      }

      return var5;
   }

   private Request b(Response var1, String var2) {
      return var1.request().newBuilder().header("Authorization", "Bearer " + var2).build();
   }

   public Request authenticate(Route param1, Response param2) throws IOException {
      // $FF: Couldn't be decompiled
   }
}
