package co.uk.getmondo.api.authentication;

import java.util.concurrent.Callable;

// $FF: synthetic class
final class e implements Callable {
   private final d a;

   private e(d var1) {
      this.a = var1;
   }

   public static Callable a(d var0) {
      return new e(var0);
   }

   public Object call() {
      return d.a(this.a);
   }
}
