package co.uk.getmondo.api.authentication;

import co.uk.getmondo.api.model.ApiToken;
import io.reactivex.v;
import io.reactivex.z;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class d {
   private final i a;
   private final MonzoOAuthApi b;
   private final Set c = Collections.synchronizedSet(new HashSet());

   public d(i var1, MonzoOAuthApi var2) {
      this.a = var1;
      this.b = var2;
   }

   // $FF: synthetic method
   static z a(d var0) throws Exception {
      ApiToken var1 = var0.a.a();
      v var2;
      if(var1 == null) {
         var2 = var0.b();
      } else {
         var2 = v.a((Object)var1);
      }

      return var2;
   }

   // $FF: synthetic method
   static void a(d var0, ApiToken var1) throws Exception {
      var0.a.a(var1);
      d.a.a.b("New client token created %s", new Object[]{var1.a()});
   }

   // $FF: synthetic method
   static void b(d var0, ApiToken var1) throws Exception {
      var0.c.add(var1.a());
   }

   v a() {
      return v.a(e.a(this)).c(f.a(this));
   }

   boolean a(String var1) {
      return this.c.contains(var1);
   }

   v b() {
      return this.b.requestClientToken(MonzoOAuthApi.a, "client_credentials").c(g.a(this));
   }
}
