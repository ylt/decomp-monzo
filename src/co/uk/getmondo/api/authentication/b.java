package co.uk.getmondo.api.authentication;

import co.uk.getmondo.api.model.ApiToken;
import java.io.IOException;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class b implements Interceptor {
   private final d a;
   private final co.uk.getmondo.common.accounts.o b;

   b(d var1, co.uk.getmondo.common.accounts.o var2) {
      this.a = var1;
      this.b = var2;
   }

   private String a() {
      String var1 = this.b.a();
      if(var1 == null) {
         var1 = ((ApiToken)this.a.a().b()).a();
      }

      return var1;
   }

   public Response intercept(Interceptor.Chain var1) throws IOException {
      Request var3 = var1.request();
      String var2 = this.a();
      Response var4;
      if(var2 == null) {
         var4 = var1.proceed(var3);
      } else {
         var4 = var1.proceed(var3.newBuilder().header("Authorization", "Bearer " + var2).build());
      }

      return var4;
   }
}
