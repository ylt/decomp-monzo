package co.uk.getmondo.api.authentication;

public final class q implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;
   private final javax.a.a c;

   static {
      boolean var0;
      if(!q.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   public q(javax.a.a var1, javax.a.a var2) {
      if(!a && var1 == null) {
         throw new AssertionError();
      } else {
         this.b = var1;
         if(!a && var2 == null) {
            throw new AssertionError();
         } else {
            this.c = var2;
         }
      }
   }

   public static b.a.b a(javax.a.a var0, javax.a.a var1) {
      return new q(var0, var1);
   }

   public m a() {
      return new m((MonzoOAuthApi)this.b.b(), (co.uk.getmondo.common.accounts.o)this.c.b());
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
