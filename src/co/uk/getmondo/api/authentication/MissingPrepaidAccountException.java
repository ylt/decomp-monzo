package co.uk.getmondo.api.authentication;

import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00060\u0001j\u0002`\u0002B\u0005¢\u0006\u0002\u0010\u0003¨\u0006\u0004"},
   d2 = {"Lco/uk/getmondo/api/authentication/MissingPrepaidAccountException;", "Ljava/lang/RuntimeException;", "Lkotlin/RuntimeException;", "()V", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class MissingPrepaidAccountException extends RuntimeException {
}
