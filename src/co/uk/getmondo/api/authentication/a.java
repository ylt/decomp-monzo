package co.uk.getmondo.api.authentication;

public enum a implements co.uk.getmondo.common.e.f {
   a("unauthorized.bad_access_token.evicted"),
   b("unauthorized.bad_access_token.expired"),
   c("unauthorized.bad_access_token"),
   d("unauthorized.bad_refresh_token"),
   e("unauthorized.bad_authorization_code"),
   f("unauthorized"),
   g("forbidden");

   private final String h;

   private a(String var3) {
      this.h = var3;
   }

   public String a() {
      return this.h;
   }

   public boolean b() {
      boolean var1;
      if(this != a && this != d) {
         var1 = false;
      } else {
         var1 = true;
      }

      return var1;
   }
}
