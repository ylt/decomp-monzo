package co.uk.getmondo.api.authentication;

public final class l implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;
   private final javax.a.a c;
   private final javax.a.a d;
   private final javax.a.a e;

   static {
      boolean var0;
      if(!l.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   public l(javax.a.a var1, javax.a.a var2, javax.a.a var3, javax.a.a var4) {
      if(!a && var1 == null) {
         throw new AssertionError();
      } else {
         this.b = var1;
         if(!a && var2 == null) {
            throw new AssertionError();
         } else {
            this.c = var2;
            if(!a && var3 == null) {
               throw new AssertionError();
            } else {
               this.d = var3;
               if(!a && var4 == null) {
                  throw new AssertionError();
               } else {
                  this.e = var4;
               }
            }
         }
      }
   }

   public static b.a.b a(javax.a.a var0, javax.a.a var1, javax.a.a var2, javax.a.a var3) {
      return new l(var0, var1, var2, var3);
   }

   public k a() {
      return new k((co.uk.getmondo.common.accounts.o)this.b.b(), (d)this.c.b(), (com.google.gson.f)this.d.b(), (m)this.e.b());
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
