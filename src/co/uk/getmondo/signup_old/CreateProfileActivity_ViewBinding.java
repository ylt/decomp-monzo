package co.uk.getmondo.signup_old;

import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class CreateProfileActivity_ViewBinding implements Unbinder {
   private CreateProfileActivity a;
   private View b;
   private View c;
   private TextWatcher d;
   private View e;
   private TextWatcher f;

   public CreateProfileActivity_ViewBinding(final CreateProfileActivity var1, View var2) {
      this.a = var1;
      var1.nameWrapper = (TextInputLayout)Utils.findRequiredViewAsType(var2, 2131820865, "field 'nameWrapper'", TextInputLayout.class);
      var1.dateWrapper = (TextInputLayout)Utils.findRequiredViewAsType(var2, 2131820867, "field 'dateWrapper'", TextInputLayout.class);
      var1.postcodeWrapper = (TextInputLayout)Utils.findRequiredViewAsType(var2, 2131820869, "field 'postcodeWrapper'", TextInputLayout.class);
      var1.countryWrapper = (TextInputLayout)Utils.findRequiredViewAsType(var2, 2131820871, "field 'countryWrapper'", TextInputLayout.class);
      var1.countryEditText = (EditText)Utils.findRequiredViewAsType(var2, 2131820872, "field 'countryEditText'", EditText.class);
      var1.createAccountButton = (Button)Utils.findRequiredViewAsType(var2, 2131820873, "field 'createAccountButton'", Button.class);
      View var3 = Utils.findRequiredView(var2, 2131820868, "method 'onDateAction' and method 'onDateFocusChange'");
      this.b = var3;
      ((TextView)var3).setOnEditorActionListener(new OnEditorActionListener() {
         public boolean onEditorAction(TextView var1x, int var2, KeyEvent var3) {
            return var1.onDateAction();
         }
      });
      var3.setOnFocusChangeListener(new OnFocusChangeListener() {
         public void onFocusChange(View var1x, boolean var2) {
            var1.onDateFocusChange(var2);
         }
      });
      var3 = Utils.findRequiredView(var2, 2131820866, "method 'onNameChanged'");
      this.c = var3;
      this.d = new TextWatcher() {
         public void afterTextChanged(Editable var1x) {
         }

         public void beforeTextChanged(CharSequence var1x, int var2, int var3, int var4) {
         }

         public void onTextChanged(CharSequence var1x, int var2, int var3, int var4) {
            var1.onNameChanged();
         }
      };
      ((TextView)var3).addTextChangedListener(this.d);
      var2 = Utils.findRequiredView(var2, 2131820870, "method 'onPostcodeChanged'");
      this.e = var2;
      this.f = new TextWatcher() {
         public void afterTextChanged(Editable var1x) {
         }

         public void beforeTextChanged(CharSequence var1x, int var2, int var3, int var4) {
         }

         public void onTextChanged(CharSequence var1x, int var2, int var3, int var4) {
            var1.onPostcodeChanged();
         }
      };
      ((TextView)var2).addTextChangedListener(this.f);
   }

   public void unbind() {
      CreateProfileActivity var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.nameWrapper = null;
         var1.dateWrapper = null;
         var1.postcodeWrapper = null;
         var1.countryWrapper = null;
         var1.countryEditText = null;
         var1.createAccountButton = null;
         ((TextView)this.b).setOnEditorActionListener((OnEditorActionListener)null);
         this.b.setOnFocusChangeListener((OnFocusChangeListener)null);
         this.b = null;
         ((TextView)this.c).removeTextChangedListener(this.d);
         this.d = null;
         this.c = null;
         ((TextView)this.e).removeTextChangedListener(this.f);
         this.f = null;
         this.e = null;
      }
   }
}
