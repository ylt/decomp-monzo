package co.uk.getmondo.signup_old.a.a;

import co.uk.getmondo.d.s;
import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u000f\n\u0002\u0010\b\n\u0002\b\u0004\b\u0086\b\u0018\u00002\u00020\u0001B)\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\b\b\u0002\u0010\u0007\u001a\u00020\b¢\u0006\u0002\u0010\tJ\t\u0010\u0010\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0011\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0012\u001a\u00020\u0006HÆ\u0003J\t\u0010\u0013\u001a\u00020\bHÆ\u0003J1\u0010\u0014\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00062\b\b\u0002\u0010\u0007\u001a\u00020\bHÆ\u0001J\u0013\u0010\u0015\u001a\u00020\b2\b\u0010\u0016\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0017\u001a\u00020\u0018HÖ\u0001J\t\u0010\u0019\u001a\u00020\u0003HÖ\u0001J\u000e\u0010\u001a\u001a\u00020\u00002\u0006\u0010\u0005\u001a\u00020\u0006J\u000e\u0010\u001b\u001a\u00020\u00002\u0006\u0010\u0007\u001a\u00020\bR\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0011\u0010\u0007\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\u000eR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\r¨\u0006\u001c"},
   d2 = {"Lco/uk/getmondo/signup_old/data/model/ProfileData;", "", "name", "", "dateOfBirth", "address", "Lco/uk/getmondo/model/LegacyAddress;", "isValid", "", "(Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/model/LegacyAddress;Z)V", "getAddress", "()Lco/uk/getmondo/model/LegacyAddress;", "getDateOfBirth", "()Ljava/lang/String;", "()Z", "getName", "component1", "component2", "component3", "component4", "copy", "equals", "other", "hashCode", "", "toString", "withAddress", "withValidity", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class a {
   private final String a;
   private final String b;
   private final s c;
   private final boolean d;

   public a(String var1, String var2, s var3) {
      this(var1, var2, var3, false, 8, (i)null);
   }

   public a(String var1, String var2, s var3, boolean var4) {
      l.b(var1, "name");
      l.b(var2, "dateOfBirth");
      l.b(var3, "address");
      super();
      this.a = var1;
      this.b = var2;
      this.c = var3;
      this.d = var4;
   }

   // $FF: synthetic method
   public a(String var1, String var2, s var3, boolean var4, int var5, i var6) {
      if((var5 & 8) != 0) {
         var4 = false;
      }

      this(var1, var2, var3, var4);
   }

   public final a a(s var1) {
      l.b(var1, "address");
      return a(this, (String)null, (String)null, var1, false, 11, (Object)null);
   }

   public final a a(String var1, String var2, s var3, boolean var4) {
      l.b(var1, "name");
      l.b(var2, "dateOfBirth");
      l.b(var3, "address");
      return new a(var1, var2, var3, var4);
   }

   public final a a(boolean var1) {
      return a(this, (String)null, (String)null, (s)null, var1, 7, (Object)null);
   }

   public final String a() {
      return this.a;
   }

   public final String b() {
      return this.b;
   }

   public final s c() {
      return this.c;
   }

   public final boolean d() {
      return this.d;
   }

   public boolean equals(Object var1) {
      boolean var4 = false;
      boolean var3;
      if(this != var1) {
         var3 = var4;
         if(!(var1 instanceof a)) {
            return var3;
         }

         a var5 = (a)var1;
         var3 = var4;
         if(!l.a(this.a, var5.a)) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.b, var5.b)) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.c, var5.c)) {
            return var3;
         }

         boolean var2;
         if(this.d == var5.d) {
            var2 = true;
         } else {
            var2 = false;
         }

         var3 = var4;
         if(!var2) {
            return var3;
         }
      }

      var3 = true;
      return var3;
   }

   public int hashCode() {
      throw new RuntimeException("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.copyTypes(TypeTransformer.java:311)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.fixTypes(TypeTransformer.java:226)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:207)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:161)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:433)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:129)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:528)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:425)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:441)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:171)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:271)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:109)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:290)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:33)\n");
   }

   public String toString() {
      return "ProfileData(name=" + this.a + ", dateOfBirth=" + this.b + ", address=" + this.c + ", isValid=" + this.d + ")";
   }
}
