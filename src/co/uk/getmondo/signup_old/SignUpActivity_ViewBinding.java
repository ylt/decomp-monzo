package co.uk.getmondo.signup_old;

import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class SignUpActivity_ViewBinding implements Unbinder {
   private SignUpActivity a;
   private View b;
   private TextWatcher c;

   public SignUpActivity_ViewBinding(final SignUpActivity var1, View var2) {
      this.a = var1;
      var1.emailWrapper = (TextInputLayout)Utils.findRequiredViewAsType(var2, 2131821084, "field 'emailWrapper'", TextInputLayout.class);
      View var3 = Utils.findRequiredView(var2, 2131821085, "field 'emailEditText' and method 'onTextChanged'");
      var1.emailEditText = (EditText)Utils.castView(var3, 2131821085, "field 'emailEditText'", EditText.class);
      this.b = var3;
      this.c = new TextWatcher() {
         public void afterTextChanged(Editable var1x) {
         }

         public void beforeTextChanged(CharSequence var1x, int var2, int var3, int var4) {
         }

         public void onTextChanged(CharSequence var1x, int var2, int var3, int var4) {
            var1.onTextChanged(var1x);
         }
      };
      ((TextView)var3).addTextChangedListener(this.c);
      var1.continueButton = (Button)Utils.findRequiredViewAsType(var2, 2131821086, "field 'continueButton'", Button.class);
   }

   public void unbind() {
      SignUpActivity var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.emailWrapper = null;
         var1.emailEditText = null;
         var1.continueButton = null;
         ((TextView)this.b).removeTextChangedListener(this.c);
         this.c = null;
         this.b = null;
      }
   }
}
