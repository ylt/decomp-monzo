package co.uk.getmondo.signup_old;

import co.uk.getmondo.api.model.tracking.Impression;
import java.util.regex.Pattern;

public class w extends co.uk.getmondo.common.ui.b {
   private static final Pattern c = Pattern.compile("[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}\\@[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}(\\.[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25})+");
   private final io.reactivex.u d;
   private final io.reactivex.u e;
   private final co.uk.getmondo.common.e.a f;
   private final co.uk.getmondo.api.b.a g;
   private final co.uk.getmondo.common.a h;

   w(io.reactivex.u var1, io.reactivex.u var2, co.uk.getmondo.common.e.a var3, co.uk.getmondo.api.b.a var4, co.uk.getmondo.common.a var5) {
      this.d = var1;
      this.e = var2;
      this.f = var3;
      this.g = var4;
      this.h = var5;
   }

   // $FF: synthetic method
   static io.reactivex.l a(w var0, w.a var1, Object var2) throws Exception {
      io.reactivex.h var3;
      if(!var0.a(var1.c())) {
         var1.f();
         var3 = io.reactivex.h.a();
      } else {
         var1.e();
         var3 = var0.g.a(var1.c()).a((Object)co.uk.getmondo.common.b.a.a).e().b(var0.e).a(var0.d).a(aa.a(var0, var1)).a((io.reactivex.l)io.reactivex.h.a());
      }

      return var3;
   }

   // $FF: synthetic method
   static void a(w.a var0, co.uk.getmondo.common.b.a var1) throws Exception {
      var0.d();
   }

   // $FF: synthetic method
   static void a(w var0, w.a var1, Throwable var2) throws Exception {
      if(!var0.f.a(var2, var1)) {
         var1.b(2131362709);
      }

   }

   private boolean a(String var1) {
      boolean var2;
      if(var1 != null && var1.length() != 0) {
         var2 = c.matcher(var1).matches();
      } else {
         var2 = false;
      }

      return var2;
   }

   private int b(w.a var1) {
      int var2;
      try {
         var2 = ((SignUpActivity)var1).getIntent().getIntExtra("page_extra", 1);
      } catch (Exception var3) {
         var2 = 0;
      }

      return var2;
   }

   public void a(w.a var1) {
      super.a((co.uk.getmondo.common.ui.f)var1);
      this.h.a(Impression.a(this.b(var1)));
      this.a((io.reactivex.b.b)io.reactivex.n.merge(var1.b(), var1.a()).flatMapMaybe(x.a(this, var1)).observeOn(this.d).subscribe(y.a(var1), z.a()));
   }

   interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      io.reactivex.n a();

      io.reactivex.n b();

      String c();

      void d();

      void e();

      void f();
   }
}
