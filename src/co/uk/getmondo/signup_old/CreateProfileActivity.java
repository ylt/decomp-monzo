package co.uk.getmondo.signup_old;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.EditText;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnEditorAction;
import butterknife.OnFocusChange;
import butterknife.OnTextChanged;

public class CreateProfileActivity extends co.uk.getmondo.common.activities.b implements f.a {
   f a;
   private final Handler b = new Handler();
   private final Runnable c = c.a(this);
   @BindView(2131820872)
   EditText countryEditText;
   @BindView(2131820871)
   TextInputLayout countryWrapper;
   @BindView(2131820873)
   Button createAccountButton;
   @BindView(2131820867)
   TextInputLayout dateWrapper;
   @BindView(2131820865)
   TextInputLayout nameWrapper;
   @BindView(2131820869)
   TextInputLayout postcodeWrapper;

   // $FF: synthetic method
   static co.uk.getmondo.signup_old.a.a.a a(CreateProfileActivity var0, Object var1) throws Exception {
      return new co.uk.getmondo.signup_old.a.a.a(var0.a(var0.nameWrapper), var0.a(var0.dateWrapper), new co.uk.getmondo.d.s(var0.a(var0.postcodeWrapper), (String[])null, (String)null, (String)null, (String)null));
   }

   private String a(TextInputLayout var1) {
      String var2;
      if(var1.getEditText() != null) {
         var2 = var1.getEditText().getText().toString();
      } else {
         var2 = null;
      }

      return var2;
   }

   @Deprecated
   public static void a(Context var0) {
      Intent var1 = b(var0);
      var1.addFlags(268533760);
      var0.startActivity(var1);
   }

   private void a(TextInputLayout var1, String var2) {
      if(var1.getEditText() != null) {
         var1.getEditText().setText(var2);
      }

   }

   public static Intent b(Context var0) {
      return new Intent(var0, CreateProfileActivity.class);
   }

   private void k() {
      final r var1 = new r();
      EditText var2 = this.dateWrapper.getEditText();
      if(var2 != null) {
         var2.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable var1x) {
            }

            public void beforeTextChanged(CharSequence var1x, int var2, int var3, int var4) {
            }

            public void onTextChanged(CharSequence var1x, int var2, int var3, int var4) {
               CreateProfileActivity.this.dateWrapper.setError((CharSequence)null);
               var1.a(var1x, var3, var4, this, CreateProfileActivity.this.dateWrapper.getEditText());
            }
         });
      }

   }

   public io.reactivex.n a() {
      return com.b.a.c.c.a(this.createAccountButton).map(d.a(this));
   }

   public void a(String var1) {
      this.a(this.nameWrapper, var1);
   }

   public io.reactivex.n b() {
      return com.b.a.c.c.a(this.countryEditText);
   }

   public void c() {
      this.setResult(-1);
      this.finish();
   }

   public void d() {
      this.nameWrapper.setError(this.getString(2131362173));
   }

   public void d(String var1) {
      this.a(this.dateWrapper, var1);
   }

   public void e() {
      this.dateWrapper.setError(this.getString(2131362171));
   }

   public void e(String var1) {
      this.a(this.postcodeWrapper, var1);
   }

   public void f() {
      this.postcodeWrapper.setError(this.getString(2131362174));
   }

   public void f(String var1) {
      this.countryEditText.setText(var1);
      this.countryEditText.setSelection(var1.length() - 1);
   }

   public void g() {
      this.dateWrapper.setError(this.getString(2131362903));
   }

   public void h() {
      this.b.postDelayed(this.c, 300L);
   }

   public void i() {
      this.b.removeCallbacks(this.c);
      this.t();
   }

   public void j() {
      co.uk.getmondo.common.d.a.a(this.getString(2131362856), this.getString(2131362857), 2131362217, false).show(this.getSupportFragmentManager(), "TAG_UNAVAILABLE_OUTSIDE_UK");
   }

   protected void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.setContentView(2131034158);
      this.l().a(this);
      ButterKnife.bind((Activity)this);
      this.a.a((f.a)this);
      this.k();
      if(co.uk.getmondo.a.c.booleanValue()) {
         this.countryEditText.setAlpha(0.5F);
      }

   }

   @OnEditorAction({2131820868})
   boolean onDateAction() {
      this.postcodeWrapper.requestFocus();
      return true;
   }

   @OnFocusChange({2131820868})
   void onDateFocusChange(boolean var1) {
      String var2 = this.a(this.dateWrapper);
      if(var1 && this.dateWrapper.getEditText() != null && var2 != null) {
         this.dateWrapper.getEditText().setSelection(var2.length());
      }

   }

   protected void onDestroy() {
      this.a.b();
      super.onDestroy();
   }

   @OnTextChanged({2131820866})
   void onNameChanged() {
      this.nameWrapper.setError((CharSequence)null);
   }

   @OnTextChanged({2131820870})
   void onPostcodeChanged() {
      this.postcodeWrapper.setError((CharSequence)null);
   }
}
