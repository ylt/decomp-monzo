package co.uk.getmondo.signup_old;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class s {
   private Pattern a = Pattern.compile("(0?[1-9]|[12][0-9]|3[01])/(0?[1-9]|1[012])/((19|20)\\d\\d)");

   public boolean a(String var1) {
      boolean var6 = true;
      boolean var4 = true;
      boolean var5 = false;
      Matcher var9 = this.a.matcher(var1);
      boolean var3 = var5;
      if(var9.matches()) {
         var9.reset();
         var3 = var5;
         if(var9.find()) {
            String var8 = var9.group(1);
            String var7 = var9.group(2);
            int var2 = Integer.parseInt(var9.group(3));
            if("31".equals(var8)) {
               var3 = var5;
               if("4".equals(var7)) {
                  return var3;
               }

               var3 = var5;
               if("6".equals(var7)) {
                  return var3;
               }

               var3 = var5;
               if("9".equals(var7)) {
                  return var3;
               }

               var3 = var5;
               if("11".equals(var7)) {
                  return var3;
               }

               var3 = var5;
               if("04".equals(var7)) {
                  return var3;
               }

               var3 = var5;
               if("06".equals(var7)) {
                  return var3;
               }

               if("09".equals(var7)) {
                  var3 = var5;
                  return var3;
               }
            }

            if(!"2".equals(var7) && !"02".equals(var7)) {
               var3 = true;
            } else if(var2 % 4 == 0) {
               if(!"30".equals(var8) && !"31".equals(var8)) {
                  var3 = var4;
               } else {
                  var3 = false;
               }
            } else if(!"29".equals(var8) && !"30".equals(var8) && !"31".equals(var8)) {
               var3 = var6;
            } else {
               var3 = false;
            }
         }
      }

      return var3;
   }
}
