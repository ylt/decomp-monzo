package co.uk.getmondo.signup_old;

import android.text.TextUtils;
import android.text.TextWatcher;
import android.widget.EditText;

public class r {
   private String a() {
      return " / ";
   }

   private void a(CharSequence var1, TextWatcher var2, EditText var3) {
      Object var5;
      if(this.a(var1)) {
         var5 = var1.subSequence(0, var1.length() - 1);
      } else {
         char var4 = this.b(var1);
         var5 = var1.subSequence(0, var1.length() - 1) + this.a() + var4;
      }

      this.c((CharSequence)var5, var2, var3);
   }

   private boolean a(CharSequence var1) {
      char var2 = this.b(var1);
      boolean var3;
      if(var2 != 47 && var2 != 46 && var2 != 95 && var2 != 45) {
         var3 = false;
      } else {
         var3 = true;
      }

      return var3;
   }

   private char b(CharSequence var1) {
      return var1.charAt(var1.length() - 1);
   }

   private void b(CharSequence var1, TextWatcher var2, EditText var3) {
      Object var4;
      if(this.a(var1)) {
         var4 = var1.subSequence(0, var1.length() - 1);
      } else {
         var4 = var1.subSequence(0, var1.length()) + this.a();
      }

      this.c((CharSequence)var4, var2, var3);
   }

   private void c(CharSequence var1, TextWatcher var2, EditText var3) {
      var3.removeTextChangedListener(var2);
      var3.setText(var1);
      var3.addTextChangedListener(var2);
      var3.setSelection(var1.length());
   }

   private boolean c(CharSequence var1) {
      boolean var2;
      if(var1.length() == 7) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   private boolean d(CharSequence var1) {
      boolean var2;
      if(var1.length() == 2) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   @Deprecated
   public void a(CharSequence var1, int var2, int var3, TextWatcher var4, EditText var5) {
      if(var1 != null && var1.length() != 0) {
         if(var2 <= var3 && var1.length() <= 14) {
            if(this.d(var1) || this.c(var1)) {
               this.c(var1 + this.a(), var4, var5);
            }

            if(var1.length() != 3 && var1.length() != 8) {
               if(var1.length() != 2 && var1.length() != 7) {
                  if(TextUtils.isDigitsOnly(this.b(var1) + "")) {
                     this.c(var1, var4, var5);
                  } else {
                     this.c(var1.subSequence(0, var1.length() - 1), var4, var5);
                  }
               } else {
                  this.b(var1, var4, var5);
               }
            } else {
               this.a(var1, var4, var5);
            }
         } else if(var1.length() == 9) {
            this.c(var1.subSequence(0, 7), var4, var5);
         } else if(var1.length() == 4) {
            this.c(var1.subSequence(0, 2), var4, var5);
         } else if(var1.length() > 14) {
            this.c(var1.subSequence(0, 14), var4, var5);
         }
      }

   }
}
