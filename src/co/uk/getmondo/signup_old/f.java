package co.uk.getmondo.signup_old;

import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.d.ak;
import java.text.ParseException;
import java.util.Date;

public class f extends co.uk.getmondo.common.ui.b {
   private final co.uk.getmondo.common.s c;
   private final io.reactivex.u d;
   private final io.reactivex.u e;
   private final co.uk.getmondo.common.e.a f;
   private final co.uk.getmondo.common.accounts.d g;
   private final co.uk.getmondo.common.m h;
   private final co.uk.getmondo.common.a i;
   private final co.uk.getmondo.fcm.e j;
   private final co.uk.getmondo.api.b.a k;
   private co.uk.getmondo.d.i l;

   f(io.reactivex.u var1, io.reactivex.u var2, co.uk.getmondo.common.e.a var3, co.uk.getmondo.common.accounts.d var4, co.uk.getmondo.common.s var5, co.uk.getmondo.common.m var6, co.uk.getmondo.common.a var7, co.uk.getmondo.fcm.e var8, co.uk.getmondo.api.b.a var9) {
      this.d = var1;
      this.e = var2;
      this.f = var3;
      this.g = var4;
      this.c = var5;
      this.h = var6;
      this.i = var7;
      this.j = var8;
      this.k = var9;
   }

   // $FF: synthetic method
   static co.uk.getmondo.signup_old.a.a.a a(f var0, co.uk.getmondo.signup_old.a.a.a var1) throws Exception {
      return var1.a(var0.a(var1));
   }

   // $FF: synthetic method
   static io.reactivex.l a(f var0, f.a var1, co.uk.getmondo.signup_old.a.a.a var2) throws Exception {
      return var0.k.a(var2).a((Object)co.uk.getmondo.common.b.a.a).e().b(var0.e).a(var0.d).b(o.a(var1)).a(p.a(var0, var1)).a(h.a(var1)).a((io.reactivex.l)io.reactivex.h.a());
   }

   // $FF: synthetic method
   static void a(f.a var0, co.uk.getmondo.common.b.a var1, Throwable var2) throws Exception {
      var0.i();
   }

   // $FF: synthetic method
   static void a(f.a var0, io.reactivex.b.b var1) throws Exception {
      var0.h();
   }

   // $FF: synthetic method
   static void a(f.a var0, Object var1) throws Exception {
      var0.j();
   }

   // $FF: synthetic method
   static void a(f var0, f.a var1, co.uk.getmondo.common.b.a var2) throws Exception {
      var0.h.a();
      var0.j.a();
      var0.c.a(true);
      var1.c();
   }

   // $FF: synthetic method
   static void a(f var0, f.a var1, Throwable var2) throws Exception {
      d.a.a.a(var2);
      if(!var0.f.a(var2, var1)) {
         var1.b(2131362868);
      }

   }

   private boolean a(co.uk.getmondo.signup_old.a.a.a var1) {
      boolean var2;
      if(!this.c(var1.a())) {
         ((f.a)this.a).d();
         var2 = false;
      } else if(!this.b(var1.b())) {
         ((f.a)this.a).e();
         var2 = false;
      } else {
         Date var4 = new Date();

         Date var3;
         try {
            var3 = co.uk.getmondo.create_account.b.a(var1.b());
         } catch (ParseException var5) {
            ((f.a)this.a).e();
            var2 = false;
            return var2;
         }

         if(!co.uk.getmondo.create_account.b.a(var3, var4)) {
            ((f.a)this.a).g();
            var2 = false;
         } else if(this.l.a() && !this.a(var1.c().c())) {
            ((f.a)this.a).f();
            var2 = false;
         } else {
            var2 = true;
         }
      }

      return var2;
   }

   private boolean a(String var1) {
      return co.uk.getmondo.common.k.p.c(var1);
   }

   // $FF: synthetic method
   static co.uk.getmondo.signup_old.a.a.a b(f var0, co.uk.getmondo.signup_old.a.a.a var1) throws Exception {
      String var2;
      if(var0.l.a()) {
         var2 = var1.c().c();
      } else {
         var2 = null;
      }

      return var1.a(new co.uk.getmondo.d.s(var2, (String[])null, (String)null, var0.l.e(), (String)null));
   }

   private boolean b(String var1) {
      boolean var2;
      if(var1 == null) {
         var2 = false;
      } else {
         var1 = var1.replace(" ", "");
         var2 = (new s()).a(var1);
      }

      return var2;
   }

   private boolean c(String var1) {
      return co.uk.getmondo.common.k.p.c(var1);
   }

   public void a(f.a var1) {
      super.a((co.uk.getmondo.common.ui.f)var1);
      this.i.a(Impression.f());
      this.l = co.uk.getmondo.d.i.i();
      var1.f(this.l.b());
      ak var3 = this.g.b();
      co.uk.getmondo.d.ac var2 = null;
      if(var3 != null) {
         var2 = var3.d();
      }

      if(var2 != null) {
         var1.a(var2.c());
         var1.d(co.uk.getmondo.create_account.b.b(var2.e()));
         var1.e(var2.h().c());
      }

      this.a((io.reactivex.b.b)var1.a().map(g.a(this)).map(i.a(this)).filter(j.a()).flatMapMaybe(k.a(this, var1)).subscribe(l.a(this, var1), m.a()));
      this.a((io.reactivex.b.b)var1.b().subscribe(n.a(var1)));
   }

   interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      io.reactivex.n a();

      void a(String var1);

      io.reactivex.n b();

      void c();

      void d();

      void d(String var1);

      void e();

      void e(String var1);

      void f();

      void f(String var1);

      void g();

      void h();

      void i();

      void j();
   }
}
