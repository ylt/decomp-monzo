package co.uk.getmondo.signup_old;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class CountryDialogFragment_ViewBinding implements Unbinder {
   private CountryDialogFragment a;

   public CountryDialogFragment_ViewBinding(CountryDialogFragment var1, View var2) {
      this.a = var1;
      var1.recyclerView = (RecyclerView)Utils.findRequiredViewAsType(var2, 2131820862, "field 'recyclerView'", RecyclerView.class);
   }

   public void unbind() {
      CountryDialogFragment var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.recyclerView = null;
      }
   }
}
