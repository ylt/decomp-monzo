package co.uk.getmondo.force_upgrade;

import android.view.View;
import android.widget.Button;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class ForceUpgradeActivity_ViewBinding implements Unbinder {
   private ForceUpgradeActivity a;

   public ForceUpgradeActivity_ViewBinding(ForceUpgradeActivity var1, View var2) {
      this.a = var1;
      var1.upgradeButton = (Button)Utils.findRequiredViewAsType(var2, 2131820910, "field 'upgradeButton'", Button.class);
   }

   public void unbind() {
      ForceUpgradeActivity var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.upgradeButton = null;
      }
   }
}
