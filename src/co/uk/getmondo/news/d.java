package co.uk.getmondo.news;

import co.uk.getmondo.api.MonzoApi;
import co.uk.getmondo.api.model.ApiNewsItem;
import io.reactivex.z;
import java.util.List;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.chrono.ChronoLocalDateTime;

public class d {
   private final MonzoApi a;
   private final co.uk.getmondo.settings.k b;

   public d(MonzoApi var1, co.uk.getmondo.settings.k var2) {
      this.a = var1;
      this.b = var2;
   }

   // $FF: synthetic method
   static c a(List var0) throws Exception {
      ApiNewsItem var1 = (ApiNewsItem)var0.get(0);
      return new c(var1.a(), var1.b(), var1.c());
   }

   // $FF: synthetic method
   static z a(d var0, List var1) throws Exception {
      return io.reactivex.n.fromIterable(var1).filter(i.a(var0)).toList();
   }

   private boolean a(ApiNewsItem var1) {
      return LocalDateTime.a().c((ChronoLocalDateTime)var1.d());
   }

   // $FF: synthetic method
   static boolean a(d var0, ApiNewsItem var1) throws Exception {
      boolean var2;
      if(var0.a(var1) && !var0.b(var1)) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   private boolean b(ApiNewsItem var1) {
      return this.b.a(var1.a());
   }

   // $FF: synthetic method
   static boolean b(List var0) throws Exception {
      boolean var1;
      if(!var0.isEmpty()) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public io.reactivex.h a() {
      return this.a.news().d(e.a()).a(f.a(this)).a(g.a()).c(h.a());
   }
}
