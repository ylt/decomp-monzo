package co.uk.getmondo.news;

import co.uk.getmondo.api.model.ApiNewsItem;
import io.reactivex.c.q;

// $FF: synthetic class
final class i implements q {
   private final d a;

   private i(d var1) {
      this.a = var1;
   }

   public static q a(d var0) {
      return new i(var0);
   }

   public boolean a(Object var1) {
      return d.a(this.a, (ApiNewsItem)var1);
   }
}
