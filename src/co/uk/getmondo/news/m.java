package co.uk.getmondo.news;

import co.uk.getmondo.api.model.tracking.Impression;

class m extends co.uk.getmondo.common.ui.b {
   private final c c;
   private final co.uk.getmondo.settings.k d;
   private final co.uk.getmondo.common.a e;

   m(c var1, co.uk.getmondo.settings.k var2, co.uk.getmondo.common.a var3) {
      this.c = var1;
      this.d = var2;
      this.e = var3;
   }

   public void a(m.a var1) {
      super.a((co.uk.getmondo.common.ui.f)var1);
      var1.a(this.c);
      this.d.b(this.c.a());
      this.e.a(Impression.Y());
   }

   interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      void a(c var1);
   }
}
