package co.uk.getmondo.news;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\f\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0004\b\u0086\b\u0018\u0000 \u001e2\u00020\u0001:\u0001\u001eB\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004B\u001d\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0006\u0012\u0006\u0010\b\u001a\u00020\u0006¢\u0006\u0002\u0010\tJ\t\u0010\u000e\u001a\u00020\u0006HÆ\u0003J\t\u0010\u000f\u001a\u00020\u0006HÆ\u0003J\t\u0010\u0010\u001a\u00020\u0006HÆ\u0003J'\u0010\u0011\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u00062\b\b\u0002\u0010\u0007\u001a\u00020\u00062\b\b\u0002\u0010\b\u001a\u00020\u0006HÆ\u0001J\b\u0010\u0012\u001a\u00020\u0013H\u0016J\u0013\u0010\u0014\u001a\u00020\u00152\b\u0010\u0016\u001a\u0004\u0018\u00010\u0017HÖ\u0003J\t\u0010\u0018\u001a\u00020\u0013HÖ\u0001J\t\u0010\u0019\u001a\u00020\u0006HÖ\u0001J\u001a\u0010\u001a\u001a\u00020\u001b2\b\u0010\u001c\u001a\u0004\u0018\u00010\u00032\u0006\u0010\u001d\u001a\u00020\u0013H\u0016R\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\b\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\u000bR\u0011\u0010\u0007\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000b¨\u0006\u001f"},
   d2 = {"Lco/uk/getmondo/news/NewsItem;", "Landroid/os/Parcelable;", "source", "Landroid/os/Parcel;", "(Landroid/os/Parcel;)V", "id", "", "uri", "title", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getId", "()Ljava/lang/String;", "getTitle", "getUri", "component1", "component2", "component3", "copy", "describeContents", "", "equals", "", "other", "", "hashCode", "toString", "writeToParcel", "", "dest", "flags", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class c implements Parcelable {
   public static final Creator CREATOR = (Creator)(new Creator() {
      public c a(Parcel var1) {
         kotlin.d.b.l.b(var1, "source");
         return new c(var1);
      }

      public c[] a(int var1) {
         return new c[var1];
      }

      // $FF: synthetic method
      public Object createFromParcel(Parcel var1) {
         return this.a(var1);
      }

      // $FF: synthetic method
      public Object[] newArray(int var1) {
         return (Object[])this.a(var1);
      }
   });
   public static final c.a a = new c.a((kotlin.d.b.i)null);
   private final String b;
   private final String c;
   private final String d;

   public c(Parcel var1) {
      kotlin.d.b.l.b(var1, "source");
      String var3 = var1.readString();
      kotlin.d.b.l.a(var3, "source.readString()");
      String var2 = var1.readString();
      kotlin.d.b.l.a(var2, "source.readString()");
      String var4 = var1.readString();
      kotlin.d.b.l.a(var4, "source.readString()");
      this(var3, var2, var4);
   }

   public c(String var1, String var2, String var3) {
      kotlin.d.b.l.b(var1, "id");
      kotlin.d.b.l.b(var2, "uri");
      kotlin.d.b.l.b(var3, "title");
      super();
      this.b = var1;
      this.c = var2;
      this.d = var3;
   }

   public final String a() {
      return this.b;
   }

   public final String b() {
      return this.c;
   }

   public final String c() {
      return this.d;
   }

   public int describeContents() {
      return 0;
   }

   public boolean equals(Object var1) {
      boolean var2;
      if(this != var1) {
         label30: {
            if(var1 instanceof c) {
               c var3 = (c)var1;
               if(kotlin.d.b.l.a(this.b, var3.b) && kotlin.d.b.l.a(this.c, var3.c) && kotlin.d.b.l.a(this.d, var3.d)) {
                  break label30;
               }
            }

            var2 = false;
            return var2;
         }
      }

      var2 = true;
      return var2;
   }

   public int hashCode() {
      int var3 = 0;
      String var4 = this.b;
      int var1;
      if(var4 != null) {
         var1 = var4.hashCode();
      } else {
         var1 = 0;
      }

      var4 = this.c;
      int var2;
      if(var4 != null) {
         var2 = var4.hashCode();
      } else {
         var2 = 0;
      }

      var4 = this.d;
      if(var4 != null) {
         var3 = var4.hashCode();
      }

      return (var2 + var1 * 31) * 31 + var3;
   }

   public String toString() {
      return "NewsItem(id=" + this.b + ", uri=" + this.c + ", title=" + this.d + ")";
   }

   public void writeToParcel(Parcel var1, int var2) {
      if(var1 != null) {
         var1.writeString(this.b);
      }

      if(var1 != null) {
         var1.writeString(this.c);
      }

      if(var1 != null) {
         var1.writeString(this.d);
      }

   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"},
      d2 = {"Lco/uk/getmondo/news/NewsItem$Companion;", "", "()V", "CREATOR", "Landroid/os/Parcelable$Creator;", "Lco/uk/getmondo/news/NewsItem;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var1) {
         this();
      }
   }
}
