package co.uk.getmondo.news;

import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebView;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class NewsActivity_ViewBinding implements Unbinder {
   private NewsActivity a;

   public NewsActivity_ViewBinding(NewsActivity var1, View var2) {
      this.a = var1;
      var1.toolbar = (Toolbar)Utils.findRequiredViewAsType(var2, 2131820798, "field 'toolbar'", Toolbar.class);
      var1.newsWebView = (WebView)Utils.findRequiredViewAsType(var2, 2131821004, "field 'newsWebView'", WebView.class);
   }

   public void unbind() {
      NewsActivity var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.toolbar = null;
         var1.newsWebView = null;
      }
   }
}
