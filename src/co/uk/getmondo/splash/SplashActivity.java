package co.uk.getmondo.splash;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Toast;
import co.uk.getmondo.common.q;
import co.uk.getmondo.main.HomeActivity;
import co.uk.getmondo.signup.j;
import co.uk.getmondo.signup.status.SignupStatusActivity;
import co.uk.getmondo.welcome.WelcomeOnboardingActivity;

public class SplashActivity extends co.uk.getmondo.common.activities.b implements b.a {
   b a;
   q b;
   co.uk.getmondo.common.a.c c;

   public static Intent a(Context var0, String var1) {
      Intent var2 = new Intent(var0, SplashActivity.class);
      var2.addFlags(268533760);
      if(var1 != null) {
         var2.putExtra("KEY_NOTICE_MESSAGE", var1);
      }

      return var2;
   }

   public static void a(Context var0) {
      var0.startActivity(a(var0, (String)null));
   }

   public static Intent b(Context var0) {
      return a(var0, (String)null);
   }

   public static Intent c(Context var0) {
      return (new Intent("android.intent.action.MAIN", Uri.EMPTY, var0, SplashActivity.class)).putExtra("KEY_OPEN_INTERCOM", true).setFlags('耀');
   }

   public void a() {
      WelcomeOnboardingActivity.a(this);
      this.overridePendingTransition(0, 0);
      this.finish();
   }

   public void b() {
      this.startActivity(HomeActivity.b((Context)this));
      this.finish();
   }

   public void c() {
      this.startActivity(SignupStatusActivity.a(this, j.b));
      this.finish();
   }

   protected void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.l().a(this);
      this.a.a((b.a)this);
      if(this.getIntent().getBooleanExtra("consumed_by_intercom", false)) {
         this.b.a();
      }

      if(this.getIntent().hasExtra("KEY_OPEN_INTERCOM")) {
         this.b.a();
         this.c.a(co.uk.getmondo.common.a.b.d);
      }

      String var2 = this.getIntent().getStringExtra("KEY_NOTICE_MESSAGE");
      if(var2 != null) {
         Toast.makeText(this, var2, 1).show();
      }

   }

   protected void onDestroy() {
      this.a.b();
      super.onDestroy();
   }
}
