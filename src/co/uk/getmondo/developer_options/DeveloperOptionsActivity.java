package co.uk.getmondo.developer_options;

import android.app.Fragment;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.Preference.OnPreferenceClickListener;
import android.widget.Toast;
import co.uk.getmondo.common.k.p;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.d.b.i;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 \u00072\u00020\u0001:\u0002\u0007\bB\u0005¢\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0014¨\u0006\t"},
   d2 = {"Lco/uk/getmondo/developer_options/DeveloperOptionsActivity;", "Lco/uk/getmondo/common/activities/BaseActivity;", "()V", "onCreate", "", "savedInstanceState", "Landroid/os/Bundle;", "Companion", "DeveloperOptionsPreferenceFragment", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class DeveloperOptionsActivity extends co.uk.getmondo.common.activities.b {
   public static final DeveloperOptionsActivity.a a = new DeveloperOptionsActivity.a((i)null);

   protected void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.getFragmentManager().beginTransaction().replace(16908290, (Fragment)(new DeveloperOptionsActivity.b())).commit();
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007¨\u0006\u0007"},
      d2 = {"Lco/uk/getmondo/developer_options/DeveloperOptionsActivity$Companion;", "", "()V", "buildIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(i var1) {
         this();
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0002J\u0012\u0010\u0005\u001a\u00020\u00042\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007H\u0016¨\u0006\b"},
      d2 = {"Lco/uk/getmondo/developer_options/DeveloperOptionsActivity$DeveloperOptionsPreferenceFragment;", "Landroid/preference/PreferenceFragment;", "()V", "initDeviceInfo", "", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class b extends PreferenceFragment {
      private HashMap a;

      private final void b() {
         Preference var2 = this.findPreference((CharSequence)this.getString(2131362136));
         CharSequence var1;
         switch(this.getResources().getDisplayMetrics().densityDpi) {
         case 120:
            var1 = (CharSequence)"ldpi";
            break;
         case 160:
            var1 = (CharSequence)"mdpi";
            break;
         case 213:
            var1 = (CharSequence)"tvdpi";
            break;
         case 240:
            var1 = (CharSequence)"hdpi";
            break;
         case 320:
            var1 = (CharSequence)"xhdpi";
            break;
         case 420:
         case 480:
            var1 = (CharSequence)"xxhdpi";
            break;
         case 640:
            var1 = (CharSequence)"xxxhdpi";
            break;
         default:
            var1 = (CharSequence)String.valueOf(this.getResources().getDisplayMetrics().densityDpi);
         }

         var2.setTitle(var1);
         final Preference var3 = this.findPreference((CharSequence)this.getString(2131362137));
         var3.setSummary((CharSequence)p.g(System.getProperty("http.agent")));
         var3.setOnPreferenceClickListener((OnPreferenceClickListener)(new OnPreferenceClickListener() {
            public final boolean onPreferenceClick(Preference var1) {
               Object var2 = b.this.getActivity().getSystemService("clipboard");
               if(var2 == null) {
                  throw new TypeCastException("null cannot be cast to non-null type android.content.ClipboardManager");
               } else {
                  ((ClipboardManager)var2).setPrimaryClip(ClipData.newPlainText((CharSequence)null, var3.getSummary()));
                  Toast.makeText((Context)b.this.getActivity(), 2131362764, 0).show();
                  return false;
               }
            }
         }));
      }

      public void a() {
         if(this.a != null) {
            this.a.clear();
         }

      }

      public void onCreate(Bundle var1) {
         super.onCreate(var1);
         this.getPreferenceManager().setSharedPreferencesName("developer_options");
         this.addPreferencesFromResource(2131230721);
         this.findPreference((CharSequence)this.getString(2131362135)).setEnabled(false);
         this.findPreference((CharSequence)this.getString(2131362134)).setEnabled(false);
         this.b();
      }

      // $FF: synthetic method
      public void onDestroyView() {
         super.onDestroyView();
         this.a();
      }
   }
}
