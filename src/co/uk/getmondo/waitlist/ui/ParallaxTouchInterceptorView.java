package co.uk.getmondo.waitlist.ui;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.FrameLayout;

public class ParallaxTouchInterceptorView extends FrameLayout {
   private int a;
   private ObjectAnimator b;
   private ParallaxTouchInterceptorView.a c;
   private float d;
   private ParallaxSwipeToRefresh e;
   private int f;
   private int g;
   private int h;
   private int i;

   public ParallaxTouchInterceptorView(Context var1) {
      super(var1);
      this.a(var1, (AttributeSet)null);
   }

   public ParallaxTouchInterceptorView(Context var1, AttributeSet var2) {
      super(var1, var2);
      this.a(var1, var2);
   }

   public ParallaxTouchInterceptorView(Context var1, AttributeSet var2, int var3) {
      super(var1, var2, var3);
      this.a(var1, var2);
   }

   private int a(float var1) {
      float var2;
      int var3;
      if(var1 > 0.0F) {
         var2 = var1;
         if((double)var1 > (double)this.i * 1.5D) {
            var2 = (float)this.i * 1.5F;
         }

         var3 = (int)((1.5D - Math.pow((double)(var2 / ((float)this.i * 1.5F)), 0.125D)) * (double)var2);
      } else {
         var2 = var1;
         if(var1 < (float)this.h * 1.5F) {
            var2 = (float)this.h * 1.5F;
         }

         var3 = (int)((1.5D - Math.pow((double)(var2 / ((float)this.h * 1.5F)), 0.012500000186264515D)) * (double)var2);
      }

      return var3;
   }

   private void a(int var1, int var2, int var3) {
      if(this.b != null) {
         this.b.cancel();
      }

      this.b = ObjectAnimator.ofInt(this, "animatedPosition", new int[]{var1, var2});
      this.b.setInterpolator(new AccelerateDecelerateInterpolator());
      this.b.setDuration((long)var3);
      this.b.start();
   }

   private void a(Context var1, AttributeSet var2) {
      if(var2 != null) {
         TypedArray var5 = var1.getTheme().obtainStyledAttributes(var2, co.uk.getmondo.c.b.ParallaxTouchInterceptorView, 0, 0);

         try {
            this.f = var5.getDimensionPixelSize(2, 0);
            this.g = var5.getDimensionPixelSize(3, 0);
            this.h = var5.getDimensionPixelSize(0, 0);
            this.i = var5.getDimensionPixelSize(1, 0);
         } finally {
            var5.recycle();
         }
      }

   }

   public void a() {
      this.a(this.a, this.f, 250);
   }

   public void a(ParallaxSwipeToRefresh var1) {
      this.e = var1;
   }

   public void b() {
      this.a(this.g, this.f, 3500);
   }

   public boolean onTouchEvent(MotionEvent var1) {
      this.e.onTouchEvent(var1);
      switch(var1.getAction()) {
      case 0:
         this.d = var1.getY();
         break;
      case 1:
         this.d = -1.0F;
         this.a();
         break;
      case 2:
         if(this.d != -1.0F) {
            this.setPosition(-(var1.getY() - this.d));
         }
         break;
      case 3:
         this.d = -1.0F;
         this.a();
      }

      return true;
   }

   public void setAnimatedPosition(int var1) {
      this.a = var1;
      this.c.a((float)var1);
   }

   public void setInitialPosition(boolean var1) {
      if(var1) {
         this.setAnimatedPosition(this.g);
      } else {
         this.setAnimatedPosition(this.f);
      }

   }

   public void setOnYAxisScrollChangeListener(ParallaxTouchInterceptorView.a var1) {
      this.c = var1;
   }

   public void setPosition(float var1) {
      float var2 = (float)this.a(var1);
      var1 = var2;
      if(var2 < (float)this.h) {
         var1 = (float)this.h;
      }

      var2 = var1;
      if(var1 > (float)this.i) {
         var2 = (float)this.i;
      }

      int var3 = this.f;
      this.a = (int)var2 + var3;
      this.c.a((float)this.a);
   }

   public interface a {
      void a(float var1);
   }
}
