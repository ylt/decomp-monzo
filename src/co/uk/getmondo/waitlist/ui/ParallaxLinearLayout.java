package co.uk.getmondo.waitlist.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.uk.getmondo.MonzoApplication;
import co.uk.getmondo.common.ui.AnimatedNumberView;
import co.uk.getmondo.d.an;

public class ParallaxLinearLayout extends LinearLayout {
   private int a;
   @BindView(2131821580)
   AnimatedNumberView ahead;
   private int b;
   @BindView(2131821581)
   AnimatedNumberView behind;
   @BindView(2131821579)
   ImageView imageView;

   public ParallaxLinearLayout(Context var1) {
      this(var1, (AttributeSet)null);
   }

   public ParallaxLinearLayout(Context var1, AttributeSet var2) {
      this(var1, var2, 0);
   }

   public ParallaxLinearLayout(Context var1, AttributeSet var2, int var3) {
      this(var1, var2, var3, 0);
   }

   public ParallaxLinearLayout(Context var1, AttributeSet var2, int var3, int var4) {
      super(var1, var2, var3, var4);
      this.a = -1;
      MonzoApplication.a(var1).b().a(this);
      this.setOrientation(1);
      if(var2 != null) {
         TypedArray var7 = var1.getTheme().obtainStyledAttributes(var2, co.uk.getmondo.c.b.ParallaxLinearLayout, 0, 0);

         try {
            this.b = var7.getDimensionPixelSize(0, 0);
         } finally {
            var7.recycle();
         }
      }

   }

   public void a(int var1) {
      this.scrollTo(0, Float.valueOf((float)var1).intValue());
   }

   public void a(an var1) {
      if(var1 != null) {
         this.ahead.setNumber(var1.d());
         this.behind.setNumber(var1.e());
         int var2 = var1.b();
         if(this.a != var2) {
            this.a = var2;
            String var3 = "waiting_list_" + var2;
            this.imageView.setImageResource(this.getResources().getIdentifier(var3, "drawable", this.getContext().getPackageName()));
         }
      }

   }

   public void b(int var1) {
      this.ahead.a(var1);
      this.behind.a(var1);
   }

   public boolean canScrollVertically(int var1) {
      boolean var2;
      if(this.getScrollY() >= this.b) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   protected void onFinishInflate() {
      super.onFinishInflate();
      ButterKnife.bind((View)this);
   }
}
