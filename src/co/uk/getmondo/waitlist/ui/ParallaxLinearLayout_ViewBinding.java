package co.uk.getmondo.waitlist.ui;

import android.view.View;
import android.widget.ImageView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import co.uk.getmondo.common.ui.AnimatedNumberView;

public class ParallaxLinearLayout_ViewBinding implements Unbinder {
   private ParallaxLinearLayout a;

   public ParallaxLinearLayout_ViewBinding(ParallaxLinearLayout var1, View var2) {
      this.a = var1;
      var1.imageView = (ImageView)Utils.findRequiredViewAsType(var2, 2131821579, "field 'imageView'", ImageView.class);
      var1.ahead = (AnimatedNumberView)Utils.findRequiredViewAsType(var2, 2131821580, "field 'ahead'", AnimatedNumberView.class);
      var1.behind = (AnimatedNumberView)Utils.findRequiredViewAsType(var2, 2131821581, "field 'behind'", AnimatedNumberView.class);
   }

   public void unbind() {
      ParallaxLinearLayout var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.imageView = null;
         var1.ahead = null;
         var1.behind = null;
      }
   }
}
