package co.uk.getmondo.waitlist.ui;

// $FF: synthetic class
final class a implements ParallaxTouchInterceptorView.a {
   private final ParallaxAnimationView a;

   private a(ParallaxAnimationView var1) {
      this.a = var1;
   }

   public static ParallaxTouchInterceptorView.a a(ParallaxAnimationView var0) {
      return new a(var0);
   }

   public void a(float var1) {
      ParallaxAnimationView.a(this.a, var1);
   }
}
