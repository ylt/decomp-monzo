package co.uk.getmondo.waitlist.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.ImageView;

public class ParallaxImageView extends ImageView {
   private int a;
   private float b;

   public ParallaxImageView(Context var1) {
      this(var1, (AttributeSet)null);
   }

   public ParallaxImageView(Context var1, AttributeSet var2) {
      this(var1, var2, 0);
   }

   public ParallaxImageView(Context var1, AttributeSet var2, int var3) {
      this(var1, var2, var3, 0);
   }

   public ParallaxImageView(Context var1, AttributeSet var2, int var3, int var4) {
      super(var1, var2, var3, var4);
      if(var2 != null) {
         TypedArray var7 = var1.getTheme().obtainStyledAttributes(var2, co.uk.getmondo.c.b.ParallaxImageView, 0, 0);

         try {
            this.b = var7.getFloat(0, 0.0F);
            this.a = var7.getDimensionPixelSize(1, 0);
         } finally {
            var7.recycle();
         }
      }

   }

   public void a(float var1) {
      this.scrollTo(0, (int)((float)(-this.a) + this.b * var1));
   }

   protected void onFinishInflate() {
      super.onFinishInflate();
   }
}
