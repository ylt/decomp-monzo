package co.uk.getmondo.waitlist.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import co.uk.getmondo.d.an;

public class ParallaxAnimationView extends FrameLayout {
   private ParallaxImageView a;
   private ParallaxImageView b;
   private ParallaxImageView c;
   private ParallaxImageView d;
   private ParallaxImageView e;
   private ParallaxImageView f;
   private ParallaxLinearLayout g;
   private ParallaxImageView h;
   private ParallaxSwipeToRefresh i;
   private ParallaxTouchInterceptorView j;
   private com.c.b.b k;
   private boolean l;

   public ParallaxAnimationView(Context var1) {
      this(var1, (AttributeSet)null, 0);
   }

   public ParallaxAnimationView(Context var1, AttributeSet var2) {
      this(var1, var2, 0);
   }

   public ParallaxAnimationView(Context var1, AttributeSet var2, int var3) {
      super(var1, var2, var3);
      this.k = com.c.b.b.c();
      LayoutInflater.from(this.getContext()).inflate(2131034359, this, true);
   }

   private void a(float var1) {
      this.h.a(var1);
      this.a.a(var1);
      this.b.a(var1);
      this.c.a(var1);
      this.d.a(var1);
      this.e.a(var1);
      this.f.a(var1);
      this.g.a((int)var1);
   }

   // $FF: synthetic method
   static void a(ParallaxAnimationView var0) {
      if(var0.l) {
         var0.l = false;
         var0.j.b();
      }

   }

   // $FF: synthetic method
   static void a(ParallaxAnimationView var0, float var1) {
      var0.a(var1);
   }

   private void b() {
      this.k.a(d.a());
   }

   // $FF: synthetic method
   static void b(ParallaxAnimationView var0) {
      var0.b();
   }

   public void a() {
      this.j.setInitialPosition(false);
   }

   public void a(int var1) {
      this.g.b(var1);
   }

   public void a(an var1) {
      this.g.a(var1);
   }

   public void a(an var1, boolean var2) {
      this.l = var2;
      this.g.a(var1);
      this.j.setInitialPosition(var2);
   }

   protected void onFinishInflate() {
      super.onFinishInflate();
      this.a = (ParallaxImageView)this.findViewById(2131821571);
      this.b = (ParallaxImageView)this.findViewById(2131821572);
      this.c = (ParallaxImageView)this.findViewById(2131821573);
      this.d = (ParallaxImageView)this.findViewById(2131821574);
      this.e = (ParallaxImageView)this.findViewById(2131821575);
      this.f = (ParallaxImageView)this.findViewById(2131821576);
      this.g = (ParallaxLinearLayout)this.findViewById(2131821578);
      this.h = (ParallaxImageView)this.findViewById(2131821570);
      this.i = (ParallaxSwipeToRefresh)this.findViewById(2131821577);
      this.j = (ParallaxTouchInterceptorView)this.findViewById(2131821582);
      this.i.setColorSchemeResources(new int[]{2131689477});
      this.j.setOnYAxisScrollChangeListener(a.a(this));
      this.j.a(this.i);
      this.i.setOnRefreshListener(b.a(this));
      this.postDelayed(c.a(this), 150L);
   }

   public void setParallaxAnimationListener(ParallaxAnimationView.a var1) {
      this.i.setColorSchemeResources(new int[]{2131689477});
      this.k = com.c.b.b.b(var1);
   }

   public void setRefreshing(boolean var1) {
      this.i.setRefreshing(var1);
   }

   public interface a {
      void a();
   }
}
