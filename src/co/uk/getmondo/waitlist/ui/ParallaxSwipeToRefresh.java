package co.uk.getmondo.waitlist.ui;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class ParallaxSwipeToRefresh extends SwipeRefreshLayout {
   private boolean m;

   public ParallaxSwipeToRefresh(Context var1) {
      this(var1, (AttributeSet)null);
   }

   public ParallaxSwipeToRefresh(Context var1, AttributeSet var2) {
      super(var1, var2);
   }

   public boolean onTouchEvent(MotionEvent var1) {
      if(!this.m) {
         this.m = this.onInterceptTouchEvent(var1);
      }

      boolean var2;
      if(this.m) {
         var2 = super.onTouchEvent(var1);
      } else {
         var2 = false;
      }

      return var2;
   }

   public void setRefreshing(boolean var1) {
      super.setRefreshing(var1);
      this.m = false;
   }
}
