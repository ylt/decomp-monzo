package co.uk.getmondo.waitlist;

import co.uk.getmondo.d.ak;

// $FF: synthetic class
final class f implements io.reactivex.c.g {
   private final c a;
   private final boolean b;

   private f(c var1, boolean var2) {
      this.a = var1;
      this.b = var2;
   }

   public static io.reactivex.c.g a(c var0, boolean var1) {
      return new f(var0, var1);
   }

   public void a(Object var1) {
      c.a(this.a, this.b, (ak)var1);
   }
}
