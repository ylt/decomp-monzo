package co.uk.getmondo.waitlist;

import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.common.s;
import co.uk.getmondo.d.ak;
import co.uk.getmondo.d.an;
import co.uk.getmondo.d.k;
import io.reactivex.n;
import io.reactivex.u;

public class c extends co.uk.getmondo.common.ui.b {
   private final u c;
   private final u d;
   private final co.uk.getmondo.common.e.a e;
   private final co.uk.getmondo.common.accounts.d f;
   private final co.uk.getmondo.api.b.a g;
   private final co.uk.getmondo.common.a h;
   private final s i;
   private final i j;
   private boolean k = false;

   c(u var1, u var2, co.uk.getmondo.common.e.a var3, co.uk.getmondo.common.accounts.d var4, co.uk.getmondo.api.b.a var5, s var6, i var7, co.uk.getmondo.common.a var8) {
      this.c = var1;
      this.d = var2;
      this.e = var3;
      this.f = var4;
      this.g = var5;
      this.i = var6;
      this.j = var7;
      this.h = var8;
   }

   // $FF: synthetic method
   static ak a(c var0, an var1) throws Exception {
      return var0.f.b();
   }

   private void a(ak var1) {
      if(var1 == null) {
         ((c.a)this.a).b(2131361985);
      } else if(var1.e() != null) {
         if(var1.e().f()) {
            ((c.a)this.a).f();
         } else if(var1.e().a()) {
            ((c.a)this.a).c();
         } else {
            if(var1.e().i()) {
               ((c.a)this.a).a(true);
               ((c.a)this.a).a(2131362119);
            } else if(var1.e().d() == 0) {
               ((c.a)this.a).a(false);
               ((c.a)this.a).a(2131362901);
            } else {
               ((c.a)this.a).a(true);
               ((c.a)this.a).a(2131362900);
            }

            if(this.k && this.a(var1.e())) {
               ((c.a)this.a).b();
            } else if(!this.k) {
               ((c.a)this.a).a(var1.e(), this.i.e());
               this.k = true;
            } else {
               ((c.a)this.a).a(var1.e());
            }
         }
      }

   }

   // $FF: synthetic method
   static void a(c.a var0, Object var1) throws Exception {
      var0.e();
   }

   // $FF: synthetic method
   static void a(c var0, Throwable var1) throws Exception {
      var0.e.a(var1, (co.uk.getmondo.common.e.a.a)var0.a);
   }

   // $FF: synthetic method
   static void a(c var0, boolean var1, ak var2) throws Exception {
      if(var1) {
         ((c.a)var0.a).t();
      }

      var0.a(var2);
   }

   private void a(boolean var1) {
      if(var1) {
         ((c.a)this.a).s();
      }

      this.a((io.reactivex.b.b)this.g.b().d(e.a(this)).b(this.d).a(this.c).a(f.a(this, var1), g.a(this)));
   }

   private boolean a(an var1) {
      boolean var2 = false;
      if(var1 != null) {
         String var3 = this.i.f();
         k var4 = var1.a(var3);
         if(var4 != null) {
            var3 = var3 + "|" + var4.b();
            this.i.e(var3);
            ((c.a)this.a).a(var4);
            var2 = true;
         }
      }

      return var2;
   }

   void a() {
      this.a(true);
   }

   public void a(c.a var1) {
      super.a((co.uk.getmondo.common.ui.f)var1);
      this.a((io.reactivex.b.b)var1.d().subscribe(d.a(var1)));
      if(this.i.e()) {
         this.h.a(Impression.d());
         this.i.a(false);
      } else {
         this.h.a(Impression.e());
      }

      this.a(this.f.b());
   }

   void c() {
      this.a(false);
   }

   void d() {
      this.a(this.f.b().e());
   }

   interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      void a(int var1);

      void a(an var1);

      void a(an var1, boolean var2);

      void a(k var1);

      void a(boolean var1);

      void b();

      void c();

      n d();

      void e();

      void f();

      void s();

      void t();
   }
}
