package co.uk.getmondo.waitlist;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View.OnTouchListener;
import android.widget.RelativeLayout;

public class TouchInterceptingRelativeLayout extends RelativeLayout {
   private OnTouchListener a;

   public TouchInterceptingRelativeLayout(Context var1) {
      super(var1);
   }

   public TouchInterceptingRelativeLayout(Context var1, AttributeSet var2) {
      super(var1, var2);
   }

   public TouchInterceptingRelativeLayout(Context var1, AttributeSet var2, int var3) {
      super(var1, var2, var3);
   }

   public boolean onInterceptTouchEvent(MotionEvent var1) {
      if(this.a != null) {
         this.a.onTouch(this, var1);
      }

      return false;
   }

   public void setOnTouchListener(OnTouchListener var1) {
      this.a = var1;
      super.setOnTouchListener(var1);
   }
}
