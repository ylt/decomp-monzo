package co.uk.getmondo.waitlist;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;

public class ShareActivity_ViewBinding implements Unbinder {
   private ShareActivity a;
   private View b;

   public ShareActivity_ViewBinding(final ShareActivity var1, View var2) {
      this.a = var1;
      var1.link = (TextView)Utils.findRequiredViewAsType(var2, 2131821080, "field 'link'", TextView.class);
      var1.description = (TextView)Utils.findRequiredViewAsType(var2, 2131821081, "field 'description'", TextView.class);
      var2 = Utils.findRequiredView(var2, 2131821082, "method 'share'");
      this.b = var2;
      var2.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View var1x) {
            var1.share();
         }
      });
   }

   public void unbind() {
      ShareActivity var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.link = null;
         var1.description = null;
         this.b.setOnClickListener((OnClickListener)null);
         this.b = null;
      }
   }
}
