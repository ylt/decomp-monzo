package co.uk.getmondo.waitlist.referral;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import co.uk.getmondo.MonzoApplication;
import co.uk.getmondo.api.model.tracking.Impression;
import java.io.UnsupportedEncodingException;

public class InstallReferrerReceiver extends BroadcastReceiver {
   co.uk.getmondo.common.a a;

   public void onReceive(Context var1, Intent var2) {
      MonzoApplication.a(var1).b().a(this);
      if(var2.hasExtra("referrer")) {
         String var6 = var2.getStringExtra("referrer");

         try {
            a var5 = a.a(var6);
            if(var5.b() && var5.c()) {
               this.a.a(Impression.g(var5.d()));
            } else {
               this.a.a(Impression.f(var6));
            }
         } catch (IllegalArgumentException var3) {
            ;
         } catch (UnsupportedEncodingException var4) {
            ;
         }
      }

   }
}
