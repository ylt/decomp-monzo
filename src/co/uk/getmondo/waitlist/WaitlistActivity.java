package co.uk.getmondo.waitlist;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.uk.getmondo.bump_up.BumpUpActivity;
import co.uk.getmondo.bump_up.WebEventActivity;
import co.uk.getmondo.d.an;
import co.uk.getmondo.d.k;
import co.uk.getmondo.top.NotInCountryActivity;
import co.uk.getmondo.waitlist.ui.ParallaxAnimationView;
import io.reactivex.n;

public class WaitlistActivity extends co.uk.getmondo.common.activities.b implements android.support.v4.widget.SwipeRefreshLayout.b, c.a, ParallaxAnimationView.a {
   c a;
   @BindView(2131821178)
   View bumpButton;
   @BindView(2131821177)
   TextView inviteFriendsTextView;
   @BindView(2131821175)
   ParallaxAnimationView parallaxAnimationView;
   @BindView(2131821174)
   TouchInterceptingRelativeLayout threeFingerView;

   public static Intent a(Context var0) {
      return new Intent(var0, WaitlistActivity.class);
   }

   public void a() {
      this.a.a();
   }

   public void a(int var1) {
      this.inviteFriendsTextView.setText(var1);
   }

   public void a(an var1) {
      this.parallaxAnimationView.a(var1);
   }

   public void a(an var1, boolean var2) {
      this.parallaxAnimationView.a(var1, var2);
   }

   public void a(k var1) {
      if(var1.a()) {
         WebEventActivity.a(this, var1);
      } else {
         BumpUpActivity.a(this, var1);
      }

   }

   public void a(boolean var1) {
      View var3 = this.bumpButton;
      byte var2;
      if(var1) {
         var2 = 0;
      } else {
         var2 = 8;
      }

      var3.setVisibility(var2);
   }

   public void b() {
      this.parallaxAnimationView.a();
   }

   public void c() {
      NotInCountryActivity.b(this);
   }

   public n d() {
      return com.b.a.c.c.a(this.bumpButton);
   }

   public void e() {
      ShareActivity.a((Activity)this);
   }

   public void f() {
      this.setResult(-1);
      this.finish();
   }

   protected void onActivityResult(int var1, int var2, Intent var3) {
      super.onActivityResult(var1, var2, var3);
      if(BumpUpActivity.a(var1)) {
         this.a.d();
         this.parallaxAnimationView.a(BumpUpActivity.a(var3));
      }

   }

   protected void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.setContentView(2131034229);
      ButterKnife.bind((Activity)this);
      this.l().a(this);
      this.a.a((c.a)this);
      this.parallaxAnimationView.setParallaxAnimationListener(this);
   }

   protected void onDestroy() {
      this.a.b();
      super.onDestroy();
   }

   protected void onResume() {
      super.onResume();
      this.a.c();
   }

   public void s() {
      this.parallaxAnimationView.setRefreshing(true);
   }

   public void t() {
      this.parallaxAnimationView.setRefreshing(false);
   }
}
