package co.uk.getmondo.waitlist;

import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import co.uk.getmondo.waitlist.ui.ParallaxAnimationView;

public class WaitlistActivity_ViewBinding implements Unbinder {
   private WaitlistActivity a;

   public WaitlistActivity_ViewBinding(WaitlistActivity var1, View var2) {
      this.a = var1;
      var1.parallaxAnimationView = (ParallaxAnimationView)Utils.findRequiredViewAsType(var2, 2131821175, "field 'parallaxAnimationView'", ParallaxAnimationView.class);
      var1.threeFingerView = (TouchInterceptingRelativeLayout)Utils.findRequiredViewAsType(var2, 2131821174, "field 'threeFingerView'", TouchInterceptingRelativeLayout.class);
      var1.inviteFriendsTextView = (TextView)Utils.findRequiredViewAsType(var2, 2131821177, "field 'inviteFriendsTextView'", TextView.class);
      var1.bumpButton = Utils.findRequiredView(var2, 2131821178, "field 'bumpButton'");
   }

   public void unbind() {
      WaitlistActivity var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.parallaxAnimationView = null;
         var1.threeFingerView = null;
         var1.inviteFriendsTextView = null;
         var1.bumpButton = null;
      }
   }
}
