package co.uk.getmondo.waitlist;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.d.ak;

public class ShareActivity extends co.uk.getmondo.common.activities.h {
   co.uk.getmondo.common.a a;
   co.uk.getmondo.common.accounts.d b;
   @BindView(2131821081)
   TextView description;
   @BindView(2131821080)
   TextView link;

   public static void a(Activity var0) {
      var0.startActivity(new Intent(var0, ShareActivity.class));
   }

   private void a(ak var1) {
      this.link.setText(var1.e().g());
      int var2 = var1.e().h();
      this.description.setText(this.getString(2131362182, new Object[]{Integer.valueOf(var2), this.getResources().getQuantityString(2131886081, var2)}));
   }

   protected void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.setContentView(2131034206);
      ButterKnife.bind((Activity)this);
      this.l().a(this);
      this.a.a(Impression.g());
      this.a(this.b.b());
   }

   @OnClick({2131821082})
   void share() {
      this.a.a(Impression.h());
      this.startActivity(co.uk.getmondo.common.k.j.a(this, this.getString(2131362700), this.link.getText().toString(), co.uk.getmondo.api.model.tracking.a.WAITING_LIST_SHARE));
   }
}
