package co.uk.getmondo.settings;

import android.animation.LayoutTransition;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class SwitchView extends LinearLayout {
   @BindView(2131821761)
   TextView descriptionTextView;
   @BindView(2131821759)
   ProgressBar progressBar;
   @BindView(2131821760)
   Switch settingSwitch;
   @BindView(2131821758)
   TextView titleTextView;

   public SwitchView(Context var1) {
      this(var1, (AttributeSet)null, 0);
   }

   public SwitchView(Context var1, AttributeSet var2) {
      this(var1, var2, 0);
   }

   public SwitchView(Context var1, AttributeSet var2, int var3) {
      super(var1, var2, var3);
      LayoutInflater.from(this.getContext()).inflate(2131034437, this);
      this.setOrientation(0);
      this.setLayoutTransition(new LayoutTransition());
      ButterKnife.bind((View)this);
      TypedArray var6 = var1.getTheme().obtainStyledAttributes(var2, co.uk.getmondo.c.b.SwitchView, 0, 0);

      try {
         this.titleTextView.setText(var6.getString(0));
      } finally {
         var6.recycle();
      }

      this.settingSwitch.setClickable(false);
   }

   public void a() {
      this.settingSwitch.setVisibility(8);
      this.progressBar.setVisibility(0);
   }

   public void b() {
      this.settingSwitch.setVisibility(0);
      this.progressBar.setVisibility(8);
   }

   public void c() {
      this.settingSwitch.setChecked(true);
   }

   public void d() {
      this.settingSwitch.setChecked(false);
   }

   public boolean e() {
      return this.settingSwitch.isChecked();
   }

   public void setDescription(String var1) {
      this.descriptionTextView.setVisibility(0);
      this.descriptionTextView.setText(var1);
   }
}
