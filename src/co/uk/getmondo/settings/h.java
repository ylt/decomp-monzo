package co.uk.getmondo.settings;

import co.uk.getmondo.api.MonzoApi;
import co.uk.getmondo.api.PaymentLimitsApi;
import co.uk.getmondo.api.model.VerificationType;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.d.ak;
import co.uk.getmondo.d.y;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0016BK\b\u0007\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\f\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011¢\u0006\u0002\u0010\u0012J\u0010\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0002H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0017"},
   d2 = {"Lco/uk/getmondo/settings/LimitsPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/settings/LimitsPresenter$View;", "uiScheduler", "Lio/reactivex/Scheduler;", "ioScheduler", "accountService", "Lco/uk/getmondo/common/accounts/AccountService;", "monzoApi", "Lco/uk/getmondo/api/MonzoApi;", "paymentLimitsApi", "Lco/uk/getmondo/api/PaymentLimitsApi;", "analyticsService", "Lco/uk/getmondo/common/AnalyticsService;", "apiErrorHandler", "Lco/uk/getmondo/common/errors/ApiErrorHandler;", "identityVerificationStatus", "Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationStatus;", "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/common/accounts/AccountService;Lco/uk/getmondo/api/MonzoApi;Lco/uk/getmondo/api/PaymentLimitsApi;Lco/uk/getmondo/common/AnalyticsService;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/signup/identity_verification/data/IdentityVerificationStatus;)V", "register", "", "view", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class h extends co.uk.getmondo.common.ui.b {
   private final io.reactivex.u c;
   private final io.reactivex.u d;
   private final co.uk.getmondo.common.accounts.d e;
   private final MonzoApi f;
   private final PaymentLimitsApi g;
   private final co.uk.getmondo.common.a h;
   private final co.uk.getmondo.common.e.a i;
   private final co.uk.getmondo.signup.identity_verification.a.f j;

   public h(io.reactivex.u var1, io.reactivex.u var2, co.uk.getmondo.common.accounts.d var3, MonzoApi var4, PaymentLimitsApi var5, co.uk.getmondo.common.a var6, co.uk.getmondo.common.e.a var7, co.uk.getmondo.signup.identity_verification.a.f var8) {
      kotlin.d.b.l.b(var1, "uiScheduler");
      kotlin.d.b.l.b(var2, "ioScheduler");
      kotlin.d.b.l.b(var3, "accountService");
      kotlin.d.b.l.b(var4, "monzoApi");
      kotlin.d.b.l.b(var5, "paymentLimitsApi");
      kotlin.d.b.l.b(var6, "analyticsService");
      kotlin.d.b.l.b(var7, "apiErrorHandler");
      kotlin.d.b.l.b(var8, "identityVerificationStatus");
      super();
      this.c = var1;
      this.d = var2;
      this.e = var3;
      this.f = var4;
      this.g = var5;
      this.h = var6;
      this.i = var7;
      this.j = var8;
   }

   public void a(final h.a var1) {
      kotlin.d.b.l.b(var1, "view");
      super.a((co.uk.getmondo.common.ui.f)var1);
      ak var2 = this.e.b();
      if(var2 == null) {
         kotlin.d.b.l.a();
      }

      final co.uk.getmondo.d.a var8 = var2.c();
      if(var8 == null) {
         kotlin.d.b.l.a();
      }

      kotlin.d.a.b var3;
      io.reactivex.b.a var4;
      io.reactivex.n var5;
      io.reactivex.c.g var6;
      Object var9;
      io.reactivex.b.b var10;
      if(kotlin.d.b.l.a(var8.d(), co.uk.getmondo.d.a.b.PREPAID)) {
         var4 = this.b;
         var5 = var1.b().startWith((Object)kotlin.n.a).flatMapMaybe((io.reactivex.c.h)(new io.reactivex.c.h() {
            public final io.reactivex.h a(kotlin.n var1x) {
               kotlin.d.b.l.b(var1x, "it");
               return co.uk.getmondo.common.j.f.a(h.this.f.balanceLimits(var8.a()).d((io.reactivex.c.h)null.a).b(h.this.d).a(h.this.c).b((io.reactivex.c.g)(new io.reactivex.c.g() {
                  public final void a(io.reactivex.b.b var1x) {
                     var1.c();
                  }
               })).a((io.reactivex.c.b)(new io.reactivex.c.b() {
                  public final void a(co.uk.getmondo.d.e var1x, Throwable var2) {
                     var1.d();
                  }
               })).d((io.reactivex.c.g)(new io.reactivex.c.g() {
                  public final void a(Throwable var1x) {
                     co.uk.getmondo.common.e.a var2 = h.this.i;
                     kotlin.d.b.l.a(var1x, "it");
                     var2.a(var1x, (co.uk.getmondo.common.e.a.a)var1);
                     var1.e();
                  }
               })));
            }
         }));
         var6 = (io.reactivex.c.g)(new io.reactivex.c.g() {
            public final void a(co.uk.getmondo.d.e var1x) {
               Impression var3 = Impression.Companion.a(var1x.a());
               h.this.h.a(var3);
               boolean var2;
               if(!kotlin.d.b.l.a(var1x.a(), VerificationType.STANDARD) && !kotlin.d.b.l.a(var1x.a(), VerificationType.BLOCKED)) {
                  var2 = false;
               } else {
                  var2 = true;
               }

               if(var2 && !h.this.j.a()) {
                  var1.f();
               }

               h.a var4 = var1;
               kotlin.d.b.l.a(var1x, "balanceLimits");
               var4.a(var1x);
            }
         });
         var3 = (kotlin.d.a.b)null.a;
         var9 = var3;
         if(var3 != null) {
            var9 = new i(var3);
         }

         var10 = var5.subscribe(var6, (io.reactivex.c.g)var9);
         kotlin.d.b.l.a(var10, "view.onRetryClicked\n    …            }, Timber::e)");
         this.b = co.uk.getmondo.common.j.f.a(var4, var10);
      } else {
         this.h.a(Impression.Companion.aq());
         var4 = this.b;
         var5 = var1.b().startWith((Object)kotlin.n.a).flatMapMaybe((io.reactivex.c.h)(new io.reactivex.c.h() {
            public final io.reactivex.h a(kotlin.n var1x) {
               kotlin.d.b.l.b(var1x, "it");
               return co.uk.getmondo.common.j.f.a(h.this.g.paymentLimits(var8.a()).d((io.reactivex.c.h)null.a).b(h.this.d).a(h.this.c).b((io.reactivex.c.g)(new io.reactivex.c.g() {
                  public final void a(io.reactivex.b.b var1x) {
                     var1.c();
                  }
               })).a((io.reactivex.c.b)(new io.reactivex.c.b() {
                  public final void a(y var1x, Throwable var2) {
                     var1.d();
                  }
               })).d((io.reactivex.c.g)(new io.reactivex.c.g() {
                  public final void a(Throwable var1x) {
                     co.uk.getmondo.common.e.a var2 = h.this.i;
                     kotlin.d.b.l.a(var1x, "it");
                     var2.a(var1x, (co.uk.getmondo.common.e.a.a)var1);
                     var1.e();
                  }
               })));
            }
         }));
         var6 = (io.reactivex.c.g)(new io.reactivex.c.g() {
            public final void a(y var1x) {
               h.a var2 = var1;
               kotlin.d.b.l.a(var1x, "paymentLimits");
               var2.a(var1x);
            }
         });
         var3 = (kotlin.d.a.b)null.a;
         var9 = var3;
         if(var3 != null) {
            var9 = new i(var3);
         }

         var10 = var5.subscribe(var6, (io.reactivex.c.g)var9);
         kotlin.d.b.l.a(var10, "view.onRetryClicked\n    …            }, Timber::e)");
         this.b = co.uk.getmondo.common.j.f.a(var4, var10);
      }

      io.reactivex.b.a var11 = this.b;
      io.reactivex.b.b var7 = var1.a().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n var1x) {
            var1.g();
         }
      }));
      kotlin.d.b.l.a(var7, "view.onRequestHigherLimi…iew.openKycOnboarding() }");
      this.b = co.uk.getmondo.common.j.f.a(var11, var7);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\b`\u0018\u00002\u00020\u00012\u00020\u0002J\b\u0010\n\u001a\u00020\u0005H&J\b\u0010\u000b\u001a\u00020\u0005H&J\u0010\u0010\f\u001a\u00020\u00052\u0006\u0010\r\u001a\u00020\u000eH&J\u0010\u0010\u000f\u001a\u00020\u00052\u0006\u0010\u0010\u001a\u00020\u0011H&J\b\u0010\u0012\u001a\u00020\u0005H&J\b\u0010\u0013\u001a\u00020\u0005H&J\b\u0010\u0014\u001a\u00020\u0005H&R\u0018\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0006\u0010\u0007R\u0018\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\t\u0010\u0007¨\u0006\u0015"},
      d2 = {"Lco/uk/getmondo/settings/LimitsPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "onRequestHigherLimitsClicked", "Lio/reactivex/Observable;", "", "getOnRequestHigherLimitsClicked", "()Lio/reactivex/Observable;", "onRetryClicked", "getOnRetryClicked", "hideLoading", "openKycOnboarding", "setBalanceLimitPrepaid", "balanceLimit", "Lco/uk/getmondo/model/BalanceLimit;", "setPaymentLimits", "paymentLimits", "Lco/uk/getmondo/model/PaymentLimits;", "showError", "showLoading", "showRequestHigherLimitsInfo", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      io.reactivex.n a();

      void a(co.uk.getmondo.d.e var1);

      void a(y var1);

      io.reactivex.n b();

      void c();

      void d();

      void e();

      void f();

      void g();
   }
}
