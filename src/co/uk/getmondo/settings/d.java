package co.uk.getmondo.settings;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

// $FF: synthetic class
final class d implements OnClickListener {
   private final Activity a;

   private d(Activity var1) {
      this.a = var1;
   }

   public static OnClickListener a(Activity var0) {
      return new d(var0);
   }

   public void onClick(DialogInterface var1, int var2) {
      c.a(this.a, var1, var2);
   }
}
