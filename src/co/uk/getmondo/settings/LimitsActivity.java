package co.uk.getmondo.settings;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.ButterKnife;
import co.uk.getmondo.api.model.signup.SignupSource;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.common.ae;
import co.uk.getmondo.common.ui.LoadingErrorView;
import co.uk.getmondo.d.y;
import co.uk.getmondo.signup.identity_verification.IdentityVerificationActivity;
import java.util.HashMap;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u0000 %2\u00020\u00012\u00020\u0002:\u0001%B\u0005¢\u0006\u0002\u0010\u0003J\b\u0010\u0013\u001a\u00020\bH\u0016J\u0012\u0010\u0014\u001a\u00020\b2\b\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u0016J\b\u0010\u0017\u001a\u00020\bH\u0014J\b\u0010\u0018\u001a\u00020\bH\u0016J\u0010\u0010\u0019\u001a\u00020\b2\u0006\u0010\u001a\u001a\u00020\u001bH\u0016J\u0010\u0010\u001c\u001a\u00020\b2\u0006\u0010\u001d\u001a\u00020\u001eH\u0016J\b\u0010\u001f\u001a\u00020\bH\u0016J\u0010\u0010 \u001a\u00020\b2\u0006\u0010!\u001a\u00020\"H\u0002J\b\u0010#\u001a\u00020\bH\u0016J\b\u0010$\u001a\u00020\bH\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082.¢\u0006\u0002\n\u0000R\u001a\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u00078VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\t\u0010\nR\u001a\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\b0\u00078VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\f\u0010\nR\u001e\u0010\r\u001a\u00020\u000e8\u0000@\u0000X\u0081.¢\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\u0010\"\u0004\b\u0011\u0010\u0012¨\u0006&"},
   d2 = {"Lco/uk/getmondo/settings/LimitsActivity;", "Lco/uk/getmondo/common/activities/BaseActivity;", "Lco/uk/getmondo/settings/LimitsPresenter$View;", "()V", "adapter", "Lco/uk/getmondo/settings/LimitsAdapter;", "onRequestHigherLimitsClicked", "Lio/reactivex/Observable;", "", "getOnRequestHigherLimitsClicked", "()Lio/reactivex/Observable;", "onRetryClicked", "getOnRetryClicked", "presenter", "Lco/uk/getmondo/settings/LimitsPresenter;", "getPresenter$app_monzoPrepaidRelease", "()Lco/uk/getmondo/settings/LimitsPresenter;", "setPresenter$app_monzoPrepaidRelease", "(Lco/uk/getmondo/settings/LimitsPresenter;)V", "hideLoading", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onDestroy", "openKycOnboarding", "setBalanceLimitPrepaid", "balanceLimit", "Lco/uk/getmondo/model/BalanceLimit;", "setPaymentLimits", "paymentLimits", "Lco/uk/getmondo/model/PaymentLimits;", "showError", "showLimitDetails", "limitDetails", "Lco/uk/getmondo/settings/LimitsAdapter$LimitDetails;", "showLoading", "showRequestHigherLimitsInfo", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class LimitsActivity extends co.uk.getmondo.common.activities.b implements h.a {
   public static final LimitsActivity.a b = new LimitsActivity.a((kotlin.d.b.i)null);
   public h a;
   private f c;
   private HashMap e;

   public static final void a(Context var0) {
      kotlin.d.b.l.b(var0, "context");
      b.a(var0);
   }

   private final void a(f.b var1) {
      g.a.a(var1).show(this.getFragmentManager(), "TAG_LIMITS_DETAILS");
   }

   public View a(int var1) {
      if(this.e == null) {
         this.e = new HashMap();
      }

      View var3 = (View)this.e.get(Integer.valueOf(var1));
      View var2 = var3;
      if(var3 == null) {
         var2 = this.findViewById(var1);
         this.e.put(Integer.valueOf(var1), var2);
      }

      return var2;
   }

   public io.reactivex.n a() {
      io.reactivex.n var1 = com.b.a.c.c.a((Button)this.a(co.uk.getmondo.c.a.limitsRequestHigherButton)).map((io.reactivex.c.h)com.b.a.a.d.a);
      kotlin.d.b.l.a(var1, "RxView.clicks(this).map(VoidToUnit)");
      return var1;
   }

   public void a(co.uk.getmondo.d.e var1) {
      kotlin.d.b.l.b(var1, "balanceLimit");
      ae.a((View)((TextView)this.a(co.uk.getmondo.c.a.limitsSubtitleTextView)));
      ((TextView)this.a(co.uk.getmondo.c.a.limitsSubtitleTextView)).setText(var1.a().b());
      f var3 = this.c;
      if(var3 == null) {
         kotlin.d.b.l.b("adapter");
      }

      Resources var2 = this.getResources();
      kotlin.d.b.l.a(var2, "resources");
      var3.a(var1, var2);
   }

   public void a(y var1) {
      kotlin.d.b.l.b(var1, "paymentLimits");
      f var3 = this.c;
      if(var3 == null) {
         kotlin.d.b.l.b("adapter");
      }

      Resources var2 = this.getResources();
      kotlin.d.b.l.a(var2, "resources");
      var3.a(var1, var2);
   }

   public io.reactivex.n b() {
      return ((LoadingErrorView)this.a(co.uk.getmondo.c.a.limitsErrorView)).c();
   }

   public void c() {
      ae.a((View)((ProgressBar)this.a(co.uk.getmondo.c.a.limitsProgressBar)));
      ae.b((LoadingErrorView)this.a(co.uk.getmondo.c.a.limitsErrorView));
   }

   public void d() {
      ae.b((ProgressBar)this.a(co.uk.getmondo.c.a.limitsProgressBar));
   }

   public void e() {
      ae.a((View)((LoadingErrorView)this.a(co.uk.getmondo.c.a.limitsErrorView)));
   }

   public void f() {
      ae.a((View)((LinearLayout)this.a(co.uk.getmondo.c.a.limitsRequestHigherViewGroup)));
   }

   public void g() {
      this.startActivity(IdentityVerificationActivity.a.a(IdentityVerificationActivity.g, (Context)this, co.uk.getmondo.signup.identity_verification.a.j.a, Impression.KycFrom.LIMITS, SignupSource.LEGACY_PREPAID, (co.uk.getmondo.signup.j)null, (String)null, 48, (Object)null));
   }

   public void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.setContentView(2131034184);
      ButterKnife.bind((Activity)this);
      this.l().a(this);
      h var4 = this.a;
      if(var4 == null) {
         kotlin.d.b.l.b("presenter");
      }

      var4.a((h.a)this);
      this.c = new f((kotlin.d.a.b)(new kotlin.d.a.b() {
         public final void a(f.b var1) {
            kotlin.d.b.l.b(var1, "it");
            LimitsActivity.this.a(var1);
         }
      }));
      RecyclerView var5 = (RecyclerView)this.a(co.uk.getmondo.c.a.limitsRecyclerView);
      Context var3 = (Context)this;
      f var2 = this.c;
      if(var2 == null) {
         kotlin.d.b.l.b("adapter");
      }

      var5.a((android.support.v7.widget.RecyclerView.g)(new co.uk.getmondo.common.ui.h(var3, (a.a.a.a.a.a)var2, 0)));
      ((RecyclerView)this.a(co.uk.getmondo.c.a.limitsRecyclerView)).setHasFixedSize(true);
      ((RecyclerView)this.a(co.uk.getmondo.c.a.limitsRecyclerView)).setLayoutManager((android.support.v7.widget.RecyclerView.h)(new LinearLayoutManager((Context)this)));
      RecyclerView var7 = (RecyclerView)this.a(co.uk.getmondo.c.a.limitsRecyclerView);
      f var6 = this.c;
      if(var6 == null) {
         kotlin.d.b.l.b("adapter");
      }

      var7.setAdapter((android.support.v7.widget.RecyclerView.a)var6);
      var7 = (RecyclerView)this.a(co.uk.getmondo.c.a.limitsRecyclerView);
      var6 = this.c;
      if(var6 == null) {
         kotlin.d.b.l.b("adapter");
      }

      var7.a((android.support.v7.widget.RecyclerView.g)(new a.a.a.a.a.b((a.a.a.a.a.a)var6)));
   }

   protected void onDestroy() {
      h var1 = this.a;
      if(var1 == null) {
         kotlin.d.b.l.b("presenter");
      }

      var1.b();
      super.onDestroy();
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0007R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000¨\u0006\t"},
      d2 = {"Lco/uk/getmondo/settings/LimitsActivity$Companion;", "", "()V", "TAG_LIMITS_DETAILS", "", "start", "", "context", "Landroid/content/Context;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var1) {
         this();
      }

      public final void a(Context var1) {
         kotlin.d.b.l.b(var1, "context");
         var1.startActivity(new Intent(var1, LimitsActivity.class));
      }
   }
}
