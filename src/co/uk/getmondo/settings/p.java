package co.uk.getmondo.settings;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;

public class p extends DialogFragment {
   private p.a a;

   public static p a() {
      return new p();
   }

   // $FF: synthetic method
   static void a(p var0, DialogInterface var1, int var2) {
      var0.a.a();
   }

   public void a(p.a var1) {
      this.a = var1;
   }

   public Dialog onCreateDialog(Bundle var1) {
      return (new Builder(this.getActivity())).setTitle(2131362676).setMessage(2131362675).setPositiveButton(2131362142, q.a(this)).setNegativeButton(2131362140, (OnClickListener)null).create();
   }

   interface a {
      void a();
   }
}
