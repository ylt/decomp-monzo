package co.uk.getmondo.settings;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

// $FF: synthetic class
final class b implements OnClickListener {
   private final Activity a;

   private b(Activity var1) {
      this.a = var1;
   }

   public static OnClickListener a(Activity var0) {
      return new b(var0);
   }

   public void onClick(DialogInterface var1, int var2) {
      a.a(this.a, var1, var2);
   }
}
