package co.uk.getmondo.settings;

import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class OpenSourceLicensesActivity_ViewBinding implements Unbinder {
   private OpenSourceLicensesActivity a;

   public OpenSourceLicensesActivity_ViewBinding(OpenSourceLicensesActivity var1, View var2) {
      this.a = var1;
      var1.recyclerView = (RecyclerView)Utils.findRequiredViewAsType(var2, 2131821008, "field 'recyclerView'", RecyclerView.class);
      var1.appBarLayout = (AppBarLayout)Utils.findRequiredViewAsType(var2, 2131821005, "field 'appBarLayout'", AppBarLayout.class);
      var1.collapsingToolbarLayout = (CollapsingToolbarLayout)Utils.findRequiredViewAsType(var2, 2131821006, "field 'collapsingToolbarLayout'", CollapsingToolbarLayout.class);
      var1.descriptionTextView = (TextView)Utils.findRequiredViewAsType(var2, 2131821007, "field 'descriptionTextView'", TextView.class);
   }

   public void unbind() {
      OpenSourceLicensesActivity var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.recyclerView = null;
         var1.appBarLayout = null;
         var1.collapsingToolbarLayout = null;
         var1.descriptionTextView = null;
      }
   }
}
