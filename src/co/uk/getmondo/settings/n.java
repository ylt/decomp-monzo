package co.uk.getmondo.settings;

import android.content.SharedPreferences.OnSharedPreferenceChangeListener;

// $FF: synthetic class
final class n implements io.reactivex.c.f {
   private final k a;
   private final OnSharedPreferenceChangeListener b;

   private n(k var1, OnSharedPreferenceChangeListener var2) {
      this.a = var1;
      this.b = var2;
   }

   public static io.reactivex.c.f a(k var0, OnSharedPreferenceChangeListener var1) {
      return new n(var0, var1);
   }

   public void a() {
      k.a(this.a, this.b);
   }
}
