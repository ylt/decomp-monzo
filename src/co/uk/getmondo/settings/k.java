package co.uk.getmondo.settings;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import java.util.Set;

public class k {
   private SharedPreferences a;

   public k(Context var1) {
      this.a = var1.getSharedPreferences("local_user_settings", 0);
   }

   // $FF: synthetic method
   static void a(k var0, OnSharedPreferenceChangeListener var1) throws Exception {
      var0.a.unregisterOnSharedPreferenceChangeListener(var1);
   }

   // $FF: synthetic method
   static void a(k var0, io.reactivex.o var1) throws Exception {
      OnSharedPreferenceChangeListener var2 = m.a(var1);
      var0.a.registerOnSharedPreferenceChangeListener(var2);
      var1.a(n.a(var0, var2));
   }

   // $FF: synthetic method
   static void a(io.reactivex.o var0, SharedPreferences var1, String var2) {
      if(var2.equals("KEY_NOTIFICATIONS_ENABLED")) {
         var0.a(Boolean.valueOf(var1.getBoolean(var2, true)));
      }

   }

   void a(boolean var1) {
      this.a.edit().putBoolean("KEY_NOTIFICATIONS_ENABLED", var1).apply();
   }

   public boolean a() {
      return this.a.getBoolean("KEY_NOTIFICATIONS_ENABLED", true);
   }

   public boolean a(String var1) {
      Set var3 = this.a.getStringSet("KEY_SEEN_NEWS_ITEM", (Set)null);
      boolean var2;
      if(var3 != null && var3.contains(var1)) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   io.reactivex.n b() {
      return io.reactivex.n.create(l.a(this));
   }

   public void b(String var1) {
      android.support.v4.g.b var2 = new android.support.v4.g.b(this.a.getStringSet("KEY_SEEN_NEWS_ITEM", new android.support.v4.g.b()));
      var2.add(var1);
      this.a.edit().putStringSet("KEY_SEEN_NEWS_ITEM", var2).apply();
   }

   public void b(boolean var1) {
      this.a.edit().putBoolean("MONZOME_ONBOARDING_KEY", var1).apply();
   }

   public void c(boolean var1) {
      this.a.edit().putBoolean("KEY_CARD_ON_ITS_WAY", var1).apply();
   }

   public boolean c() {
      return this.a.getBoolean("MONZOME_ONBOARDING_KEY", false);
   }

   public boolean d() {
      return this.a.getBoolean("KEY_CARD_ON_ITS_WAY", false);
   }

   public void e() {
      this.a.edit().clear().apply();
   }
}
