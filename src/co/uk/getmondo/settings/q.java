package co.uk.getmondo.settings;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

// $FF: synthetic class
final class q implements OnClickListener {
   private final p a;

   private q(p var1) {
      this.a = var1;
   }

   public static OnClickListener a(p var0) {
      return new q(var0);
   }

   public void onClick(DialogInterface var1, int var2) {
      p.a(this.a, var1, var2);
   }
}
