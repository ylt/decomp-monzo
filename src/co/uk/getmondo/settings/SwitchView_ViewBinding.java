package co.uk.getmondo.settings;

import android.view.View;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class SwitchView_ViewBinding implements Unbinder {
   private SwitchView a;

   public SwitchView_ViewBinding(SwitchView var1, View var2) {
      this.a = var1;
      var1.titleTextView = (TextView)Utils.findRequiredViewAsType(var2, 2131821758, "field 'titleTextView'", TextView.class);
      var1.descriptionTextView = (TextView)Utils.findRequiredViewAsType(var2, 2131821761, "field 'descriptionTextView'", TextView.class);
      var1.settingSwitch = (Switch)Utils.findRequiredViewAsType(var2, 2131821760, "field 'settingSwitch'", Switch.class);
      var1.progressBar = (ProgressBar)Utils.findRequiredViewAsType(var2, 2131821759, "field 'progressBar'", ProgressBar.class);
   }

   public void unbind() {
      SwitchView var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.titleTextView = null;
         var1.descriptionTextView = null;
         var1.settingSwitch = null;
         var1.progressBar = null;
      }
   }
}
