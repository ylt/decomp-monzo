package co.uk.getmondo.settings;

import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class OpenSourceLicensesActivity$OpenSourceLibraryHolder_ViewBinding implements Unbinder {
   private OpenSourceLicensesActivity.OpenSourceLibraryHolder a;

   public OpenSourceLicensesActivity$OpenSourceLibraryHolder_ViewBinding(OpenSourceLicensesActivity.OpenSourceLibraryHolder var1, View var2) {
      this.a = var1;
      var1.libraryNameTextView = (TextView)Utils.findRequiredViewAsType(var2, 2131821604, "field 'libraryNameTextView'", TextView.class);
      var1.libraryLicenseTextView = (TextView)Utils.findRequiredViewAsType(var2, 2131821605, "field 'libraryLicenseTextView'", TextView.class);
   }

   public void unbind() {
      OpenSourceLicensesActivity.OpenSourceLibraryHolder var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.libraryNameTextView = null;
         var1.libraryLicenseTextView = null;
      }
   }
}
