package co.uk.getmondo.settings;

import android.support.design.widget.AppBarLayout;

// $FF: synthetic class
final class r implements android.support.design.widget.AppBarLayout.b {
   private final OpenSourceLicensesActivity a;

   private r(OpenSourceLicensesActivity var1) {
      this.a = var1;
   }

   public static android.support.design.widget.AppBarLayout.b a(OpenSourceLicensesActivity var0) {
      return new r(var0);
   }

   public void onOffsetChanged(AppBarLayout var1, int var2) {
      OpenSourceLicensesActivity.a(this.a, var1, var2);
   }
}
