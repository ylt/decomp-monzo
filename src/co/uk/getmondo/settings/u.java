package co.uk.getmondo.settings;

import co.uk.getmondo.api.MonzoApi;
import co.uk.getmondo.api.model.ApiAccountSettings;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.d.ac;
import co.uk.getmondo.d.ad;
import co.uk.getmondo.d.ak;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import org.threeten.bp.LocalDateTime;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000l\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001$Bk\b\u0007\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\f\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u0012\u0006\u0010\u0014\u001a\u00020\u0015\u0012\u0006\u0010\u0016\u001a\u00020\u0017\u0012\u0006\u0010\u0018\u001a\u00020\u0019¢\u0006\u0002\u0010\u001aJ\u0010\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020\u0002H\u0016J\u0018\u0010!\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020\u00022\u0006\u0010\"\u001a\u00020#H\u0002R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u001cX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u001cX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006%"},
   d2 = {"Lco/uk/getmondo/settings/SettingsPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/settings/SettingsPresenter$View;", "uiScheduler", "Lio/reactivex/Scheduler;", "ioScheduler", "accountService", "Lco/uk/getmondo/common/accounts/AccountService;", "analyticsService", "Lco/uk/getmondo/common/AnalyticsService;", "userSettingsRepository", "Lco/uk/getmondo/payments/send/data/UserSettingsRepository;", "monzoApi", "Lco/uk/getmondo/api/MonzoApi;", "apiErrorHandler", "Lco/uk/getmondo/common/errors/ApiErrorHandler;", "userSettingsStorage", "Lco/uk/getmondo/payments/send/data/UserSettingsStorage;", "userAccessTokenManager", "Lco/uk/getmondo/common/accounts/UserAccessTokenManager;", "localUserSettingStorage", "Lco/uk/getmondo/settings/LocalUserSettingStorage;", "userInteractor", "Lco/uk/getmondo/api/interactors/UserInteractor;", "featureFlagsStorage", "Lco/uk/getmondo/common/FeatureFlagsStorage;", "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/common/accounts/AccountService;Lco/uk/getmondo/common/AnalyticsService;Lco/uk/getmondo/payments/send/data/UserSettingsRepository;Lco/uk/getmondo/api/MonzoApi;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/payments/send/data/UserSettingsStorage;Lco/uk/getmondo/common/accounts/UserAccessTokenManager;Lco/uk/getmondo/settings/LocalUserSettingStorage;Lco/uk/getmondo/api/interactors/UserInteractor;Lco/uk/getmondo/common/FeatureFlagsStorage;)V", "magStripeInitialised", "", "paymentsInitialised", "register", "", "view", "updateMagStripeDescription", "accountSettings", "Lco/uk/getmondo/api/model/ApiAccountSettings;", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class u extends co.uk.getmondo.common.ui.b {
   private boolean c;
   private boolean d;
   private final io.reactivex.u e;
   private final io.reactivex.u f;
   private final co.uk.getmondo.common.accounts.d g;
   private final co.uk.getmondo.common.a h;
   private final co.uk.getmondo.payments.send.data.h i;
   private final MonzoApi j;
   private final co.uk.getmondo.common.e.a k;
   private final co.uk.getmondo.payments.send.data.p l;
   private final co.uk.getmondo.common.accounts.o m;
   private final k n;
   private final co.uk.getmondo.api.b.a o;
   private final co.uk.getmondo.common.o p;

   public u(io.reactivex.u var1, io.reactivex.u var2, co.uk.getmondo.common.accounts.d var3, co.uk.getmondo.common.a var4, co.uk.getmondo.payments.send.data.h var5, MonzoApi var6, co.uk.getmondo.common.e.a var7, co.uk.getmondo.payments.send.data.p var8, co.uk.getmondo.common.accounts.o var9, k var10, co.uk.getmondo.api.b.a var11, co.uk.getmondo.common.o var12) {
      kotlin.d.b.l.b(var1, "uiScheduler");
      kotlin.d.b.l.b(var2, "ioScheduler");
      kotlin.d.b.l.b(var3, "accountService");
      kotlin.d.b.l.b(var4, "analyticsService");
      kotlin.d.b.l.b(var5, "userSettingsRepository");
      kotlin.d.b.l.b(var6, "monzoApi");
      kotlin.d.b.l.b(var7, "apiErrorHandler");
      kotlin.d.b.l.b(var8, "userSettingsStorage");
      kotlin.d.b.l.b(var9, "userAccessTokenManager");
      kotlin.d.b.l.b(var10, "localUserSettingStorage");
      kotlin.d.b.l.b(var11, "userInteractor");
      kotlin.d.b.l.b(var12, "featureFlagsStorage");
      super();
      this.e = var1;
      this.f = var2;
      this.g = var3;
      this.h = var4;
      this.i = var5;
      this.j = var6;
      this.k = var7;
      this.l = var8;
      this.m = var9;
      this.n = var10;
      this.o = var11;
      this.p = var12;
   }

   private final void a(u.a var1, ApiAccountSettings var2) {
      if(var2.a()) {
         int var3 = (int)TimeUnit.SECONDS.toHours(var2.b());
         var1.a(var3, (int)TimeUnit.SECONDS.toMinutes(var2.b() - TimeUnit.HOURS.toSeconds((long)var3)));
      } else {
         var1.L();
      }

   }

   public void a(final u.a var1) {
      kotlin.d.b.l.b(var1, "view");
      super.a((co.uk.getmondo.common.ui.f)var1);
      final ak var2 = this.g.b();
      if(var2 == null) {
         kotlin.d.b.l.a();
      }

      final co.uk.getmondo.d.a var5 = var2.c();
      ak var3 = this.g.b();
      if(var3 == null) {
         kotlin.d.b.l.a();
      }

      final ac var4 = var3.d();
      if(var4 == null) {
         kotlin.d.b.l.a();
      }

      if(var5 == null) {
         kotlin.d.b.l.a();
      }

      var1.a(var4, var5.b());
      this.h.a(Impression.Companion.ap());
      io.reactivex.b.a var11 = this.b;
      io.reactivex.b.b var6 = var1.b().doOnNext((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n var1) {
            u.this.h.a(Impression.Companion.ah());
         }
      })).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n var1x) {
            var1.A();
         }
      }), (io.reactivex.c.g)null.a);
      kotlin.d.b.l.a(var6, "view.onNameClicked()\n   …on() }, { Timber.e(it) })");
      this.b = co.uk.getmondo.common.j.f.a(var11, var6);
      io.reactivex.b.a var17 = this.b;
      io.reactivex.n var7 = var1.c().doOnNext((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n var1) {
            u.this.h.a(Impression.Companion.ah());
         }
      }));
      io.reactivex.c.g var8 = (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n var1x) {
            ac var2x = var2.d();
            if(var2x != null && var2x.i()) {
               var1.C();
            } else {
               var1.B();
            }

         }
      });
      kotlin.d.a.b var13 = (kotlin.d.a.b)null.a;
      Object var10 = var13;
      if(var13 != null) {
         var10 = new v(var13);
      }

      io.reactivex.b.b var12 = var7.subscribe(var8, (io.reactivex.c.g)var10);
      kotlin.d.b.l.a(var12, "view.onAddressClicked()\n…            }, Timber::e)");
      this.b = co.uk.getmondo.common.j.f.a(var17, var12);
      io.reactivex.b.a var14 = this.b;
      io.reactivex.b.b var15 = this.n.b().startWith((Object)Boolean.valueOf(this.n.a())).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Boolean var1x) {
            kotlin.d.b.l.a(var1x, "notificationsEnabled");
            if(var1x.booleanValue()) {
               var1.V();
            } else {
               var1.W();
            }

         }
      }));
      kotlin.d.b.l.a(var15, "localUserSettingStorage.…      }\n                }");
      this.b = co.uk.getmondo.common.j.f.a(var14, var15);
      var11 = this.b;
      var12 = var1.w().doOnNext((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Boolean var1) {
            u.this.h.a(Impression.Companion.c(var1));
         }
      })).observeOn(this.e).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Boolean var1) {
            k var2 = u.this.n;
            kotlin.d.b.l.a(var1, "it");
            var2.a(var1.booleanValue());
         }
      }), (io.reactivex.c.g)null.a);
      kotlin.d.b.l.a(var12, "view.onNotificationsStat…(it) }, { Timber.e(it) })");
      this.b = co.uk.getmondo.common.j.f.a(var11, var12);
      var11 = this.b;
      var12 = var1.d().doOnNext((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n var1) {
            u.this.h.a(Impression.Companion.ai());
         }
      })).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n var1x) {
            var1.G();
         }
      }), (io.reactivex.c.g)null.a);
      kotlin.d.b.l.a(var12, "view.onAboutMonzoClicked…zo() }, { Timber.e(it) })");
      this.b = co.uk.getmondo.common.j.f.a(var11, var12);
      var14 = this.b;
      var15 = var1.e().doOnNext((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n var1) {
            u.this.h.a(Impression.Companion.aj());
         }
      })).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n var1x) {
            var1.H();
         }
      }), (io.reactivex.c.g)null.a);
      kotlin.d.b.l.a(var15, "view.onTermsAndCondition…ns() }, { Timber.e(it) })");
      this.b = co.uk.getmondo.common.j.f.a(var14, var15);
      var14 = this.b;
      var15 = var1.f().doOnNext((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n var1) {
            u.this.h.a(Impression.Companion.ak());
         }
      })).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n var1x) {
            var1.I();
         }
      }), (io.reactivex.c.g)null.a);
      kotlin.d.b.l.a(var15, "view.onPrivacyPolicyClic…cy() }, { Timber.e(it) })");
      this.b = co.uk.getmondo.common.j.f.a(var14, var15);
      var14 = this.b;
      var15 = var1.g().doOnNext((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n var1) {
            u.this.h.a(Impression.Companion.al());
         }
      })).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n var1x) {
            var1.J();
         }
      }), (io.reactivex.c.g)null.a);
      kotlin.d.b.l.a(var15, "view.onFscsProtectionCli…on() }, { Timber.e(it) })");
      this.b = co.uk.getmondo.common.j.f.a(var14, var15);
      var1.L();
      var14 = this.b;
      var15 = this.j.accountSettings(var5.a()).b(this.f).a(this.e).a((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(ApiAccountSettings var1x) {
            u var3 = u.this;
            u.a var2 = var1;
            kotlin.d.b.l.a(var1x, "accountSettings");
            var3.a(var2, var1x);
            u.this.c = true;
         }
      }), (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Throwable var1x) {
            u.this.c = true;
            co.uk.getmondo.common.e.a var2 = u.this.k;
            kotlin.d.b.l.a(var1x, "error");
            var2.a(var1x, (co.uk.getmondo.common.e.a.a)var1);
         }
      }));
      kotlin.d.b.l.a(var15, "monzoApi.accountSettings… view)\n                })");
      this.b = co.uk.getmondo.common.j.f.a(var14, var15);
      var11 = this.b;
      var12 = var1.x().filter((io.reactivex.c.q)(new io.reactivex.c.q() {
         public final boolean a(Boolean var1) {
            kotlin.d.b.l.b(var1, "it");
            return u.this.c;
         }
      })).doOnNext((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Boolean var1) {
            u.this.h.a(Impression.Companion.a(var1));
         }
      })).doOnNext((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Boolean var1x) {
            var1.M();
         }
      })).flatMapMaybe((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.h a(Boolean var1x) {
            kotlin.d.b.l.b(var1x, "state");
            return co.uk.getmondo.common.j.f.a(u.this.j.updateSettings(var5.a(), var1x.booleanValue()).b(u.this.f).a(u.this.e).d((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(Throwable var1x) {
                  var1.N();
                  co.uk.getmondo.common.e.a var2 = u.this.k;
                  kotlin.d.b.l.a(var1x, "error");
                  var2.a(var1x, (co.uk.getmondo.common.e.a.a)var1);
               }
            })));
         }
      })).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(ApiAccountSettings var1x) {
            var1.N();
            u var2 = u.this;
            u.a var3 = var1;
            kotlin.d.b.l.a(var1x, "accountSettings");
            var2.a(var3, var1x);
         }
      }), (io.reactivex.c.g)null.a);
      kotlin.d.b.l.a(var12, "view.onMagStripeStateTog…     }, { Timber.e(it) })");
      this.b = co.uk.getmondo.common.j.f.a(var11, var12);
      if(kotlin.d.b.l.a(var5.d(), co.uk.getmondo.d.a.b.PREPAID)) {
         var1.X();
      } else {
         var1.Y();
         final String var18 = ((ad)var5).j();
         final String var19 = ((ad)var5).g();
         var1.a(var18, var19);
         io.reactivex.b.a var16 = this.b;
         var6 = this.p.a().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
            public final void a(co.uk.getmondo.d.l var1x) {
               if(var1x.a()) {
                  var1.O();
               } else {
                  var1.P();
               }

            }
         }));
         kotlin.d.b.l.a(var6, "featureFlagsStorage.feat…  }\n                    }");
         this.b = co.uk.getmondo.common.j.f.a(var16, var6);
         var16 = this.b;
         var12 = var1.z().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
            public final void a(kotlin.n var1x) {
               var1.a(var4.c(), var18, var19);
            }
         }));
         kotlin.d.b.l.a(var12, "view.onShareAccountDetai…de)\n                    }");
         this.b = co.uk.getmondo.common.j.f.a(var16, var12);
      }

      var11 = this.b;
      var12 = var1.h().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n var1x) {
            var1.F();
         }
      }), (io.reactivex.c.g)null.a);
      kotlin.d.b.l.a(var12, "view.onLimitsClicked().s…ts() }, { Timber.e(it) })");
      this.b = co.uk.getmondo.common.j.f.a(var11, var12);
      var14 = this.b;
      var15 = this.i.b().map((io.reactivex.c.h)null.a).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(co.uk.getmondo.payments.send.data.a.d var1x) {
            if(kotlin.d.b.l.a(var1x, co.uk.getmondo.payments.send.data.a.d.a)) {
               var1.R();
            } else {
               var1.S();
            }

            u.this.d = true;
         }
      }));
      kotlin.d.b.l.a(var15, "userSettingsRepository.o… = true\n                }");
      this.b = co.uk.getmondo.common.j.f.a(var14, var15);
      var14 = this.b;
      var15 = var1.y().filter((io.reactivex.c.q)(new io.reactivex.c.q() {
         public final boolean a(Boolean var1) {
            kotlin.d.b.l.b(var1, "it");
            return u.this.d;
         }
      })).doOnNext((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Boolean var1) {
            u.this.h.a(Impression.Companion.b(var1));
         }
      })).doOnNext((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Boolean var1x) {
            kotlin.d.b.l.a(var1x, "state");
            if(var1x.booleanValue() && kotlin.d.b.l.a(u.this.l.a(), co.uk.getmondo.payments.send.data.a.d.c)) {
               var1.Q();
            }

         }
      })).filter((io.reactivex.c.q)(new io.reactivex.c.q() {
         public final boolean a(Boolean var1) {
            kotlin.d.b.l.b(var1, "it");
            return kotlin.d.b.l.a(u.this.l.a(), co.uk.getmondo.payments.send.data.a.d.c) ^ true;
         }
      })).doOnNext((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Boolean var1x) {
            var1.T();
         }
      })).switchMapSingle((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.v a(Boolean var1x) {
            kotlin.d.b.l.b(var1x, "state");
            return u.this.i.a(var1x.booleanValue()).d((io.reactivex.c.h)null.a).b(u.this.f).a(u.this.e).d((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(Throwable var1x) {
                  co.uk.getmondo.common.e.a var2 = u.this.k;
                  kotlin.d.b.l.a(var1x, "error");
                  var2.a(var1x, (co.uk.getmondo.common.e.a.a)var1);
               }
            })).a(io.reactivex.v.a((Object)kotlin.n.a));
         }
      })).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n var1x) {
            var1.U();
         }
      }), (io.reactivex.c.g)null.a);
      kotlin.d.b.l.a(var15, "view.onPaymentsStateTogg…ng() }, { Timber.e(it) })");
      this.b = co.uk.getmondo.common.j.f.a(var14, var15);
      var14 = this.b;
      var15 = var1.i().doOnNext((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n var1) {
            u.this.h.a(Impression.Companion.am());
         }
      })).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n var1x) {
            var1.D();
         }
      }), (io.reactivex.c.g)null.a);
      kotlin.d.b.l.a(var15, "view.onOpenSourceLicense…es() }, { Timber.e(it) })");
      this.b = co.uk.getmondo.common.j.f.a(var14, var15);
      var11 = this.b;
      var12 = var1.j().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n var1x) {
            var1.K();
         }
      }), (io.reactivex.c.g)null.a);
      kotlin.d.b.l.a(var12, "view.onLogOutClicked()\n …on() }, { Timber.e(it) })");
      this.b = co.uk.getmondo.common.j.f.a(var11, var12);
      var14 = this.b;
      var15 = var1.k().doOnNext((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n var1) {
            u.this.h.a(Impression.Companion.an());
         }
      })).flatMapMaybe((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.h a(kotlin.n var1) {
            kotlin.d.b.l.b(var1, "it");
            return io.reactivex.h.a((Callable)(new Callable() {
               public final String a() {
                  return u.this.m.a();
               }

               // $FF: synthetic method
               public Object call() {
                  return this.a();
               }
            })).b(u.this.f);
         }
      })).doOnNext((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(String var1) {
            if(!co.uk.getmondo.a.b.booleanValue()) {
               u.this.o.b(var1).b(u.this.f).a((io.reactivex.c.a)null.a, (io.reactivex.c.g)null.a);
            }

         }
      })).doOnNext((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(String var1) {
            u.this.g.e();
         }
      })).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(String var1x) {
            var1.u();
         }
      }), (io.reactivex.c.g)null.a);
      kotlin.d.b.l.a(var15, "view.onConfirmLogOutClic…sh() }, { Timber.e(it) })");
      this.b = co.uk.getmondo.common.j.f.a(var14, var15);
      var14 = this.b;
      io.reactivex.b.b var9 = var1.v().doOnNext((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n var1) {
            u.this.h.a(Impression.Companion.ao());
         }
      })).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n var1x) {
            var1.E();
         }
      }), (io.reactivex.c.g)null.a);
      kotlin.d.b.l.a(var9, "view.onCloseAccountClick…nt() }, { Timber.e(it) })");
      this.b = co.uk.getmondo.common.j.f.a(var14, var9);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0002\b\u000f\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\b\n\u0002\b\u000e\bf\u0018\u00002\u00020\u00012\u00020\u0002J\b\u0010\u0003\u001a\u00020\u0004H&J\b\u0010\u0005\u001a\u00020\u0004H&J\b\u0010\u0006\u001a\u00020\u0004H&J\u000e\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00040\bH&J\u000e\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00040\bH&J\u000e\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00040\bH&J\u000e\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00040\bH&J\u000e\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00040\bH&J\u000e\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00040\bH&J\u000e\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00040\bH&J\u000e\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00100\bH&J\u000e\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00040\bH&J\u000e\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00100\bH&J\u000e\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00040\bH&J\u000e\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00100\bH&J\u000e\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00040\bH&J\u000e\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00040\bH&J\u000e\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00040\bH&J\b\u0010\u0018\u001a\u00020\u0004H&J\b\u0010\u0019\u001a\u00020\u0004H&J\b\u0010\u001a\u001a\u00020\u0004H&J\b\u0010\u001b\u001a\u00020\u0004H&J\b\u0010\u001c\u001a\u00020\u0004H&J\b\u0010\u001d\u001a\u00020\u0004H&J\u0018\u0010\u001e\u001a\u00020\u00042\u0006\u0010\u001f\u001a\u00020 2\u0006\u0010!\u001a\u00020 H&J\u001a\u0010\"\u001a\u00020\u00042\u0006\u0010#\u001a\u00020$2\b\u0010%\u001a\u0004\u0018\u00010&H&J \u0010'\u001a\u00020\u00042\u0006\u0010(\u001a\u00020 2\u0006\u0010\u001f\u001a\u00020 2\u0006\u0010!\u001a\u00020 H&J\b\u0010)\u001a\u00020\u0004H&J\b\u0010*\u001a\u00020\u0004H&J\b\u0010+\u001a\u00020\u0004H&J\b\u0010,\u001a\u00020\u0004H&J\u0018\u0010-\u001a\u00020\u00042\u0006\u0010.\u001a\u00020/2\u0006\u00100\u001a\u00020/H&J\b\u00101\u001a\u00020\u0004H&J\b\u00102\u001a\u00020\u0004H&J\b\u00103\u001a\u00020\u0004H&J\b\u00104\u001a\u00020\u0004H&J\b\u00105\u001a\u00020\u0004H&J\b\u00106\u001a\u00020\u0004H&J\b\u00107\u001a\u00020\u0004H&J\b\u00108\u001a\u00020\u0004H&J\b\u00109\u001a\u00020\u0004H&J\b\u0010:\u001a\u00020\u0004H&J\b\u0010;\u001a\u00020\u0004H&J\b\u0010<\u001a\u00020\u0004H&¨\u0006="},
      d2 = {"Lco/uk/getmondo/settings/SettingsPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "hideMagStripeLoading", "", "hidePaymentsLoading", "hidePaymentsUi", "onAboutMonzoClicked", "Lio/reactivex/Observable;", "onAddressClicked", "onCloseAccountClicked", "onConfirmLogOutClicked", "onFscsProtectionClicked", "onLimitsClicked", "onLogOutClicked", "onMagStripeStateToggled", "", "onNameClicked", "onNotificationsStateToggled", "onOpenSourceLicensesClicked", "onPaymentsStateToggled", "onPrivacyPolicyClicked", "onShareAccountDetailsClicked", "onTermsAndConditionsClicked", "openAboutMonzo", "openFscsProtection", "openLimits", "openPrivacyPolicy", "openTermsAndConditions", "openUpdateAddress", "setAccountInformation", "accountNumber", "", "sortCode", "setProfileInformation", "profile", "Lco/uk/getmondo/model/Profile;", "created", "Lorg/threeten/bp/LocalDateTime;", "shareAccountDetails", "profileName", "showChangeInformation", "showCloseAccount", "showLogoutConfirmation", "showMagStripeDisabled", "showMagStripeEnabled", "hours", "", "minutes", "showMagStripeLoading", "showNotificationsChecked", "showNotificationsUnchecked", "showOpenSourceLicenses", "showPaymentsBlocked", "showPaymentsDisabled", "showPaymentsEnabled", "showPaymentsLoading", "showPaymentsUi", "showPrepaidUi", "showRetailUi", "showUpdateAddressSupport", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      void A();

      void B();

      void C();

      void D();

      void E();

      void F();

      void G();

      void H();

      void I();

      void J();

      void K();

      void L();

      void M();

      void N();

      void O();

      void P();

      void Q();

      void R();

      void S();

      void T();

      void U();

      void V();

      void W();

      void X();

      void Y();

      void a(int var1, int var2);

      void a(ac var1, LocalDateTime var2);

      void a(String var1, String var2);

      void a(String var1, String var2, String var3);

      io.reactivex.n b();

      io.reactivex.n c();

      io.reactivex.n d();

      io.reactivex.n e();

      io.reactivex.n f();

      io.reactivex.n g();

      io.reactivex.n h();

      io.reactivex.n i();

      io.reactivex.n j();

      io.reactivex.n k();

      io.reactivex.n v();

      io.reactivex.n w();

      io.reactivex.n x();

      io.reactivex.n y();

      io.reactivex.n z();
   }
}
