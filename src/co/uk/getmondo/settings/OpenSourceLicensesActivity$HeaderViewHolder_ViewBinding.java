package co.uk.getmondo.settings;

import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class OpenSourceLicensesActivity$HeaderViewHolder_ViewBinding implements Unbinder {
   private OpenSourceLicensesActivity.HeaderViewHolder a;

   public OpenSourceLicensesActivity$HeaderViewHolder_ViewBinding(OpenSourceLicensesActivity.HeaderViewHolder var1, View var2) {
      this.a = var1;
      var1.titleTextView = (TextView)Utils.findRequiredViewAsType(var2, 2131821599, "field 'titleTextView'", TextView.class);
   }

   public void unbind() {
      OpenSourceLicensesActivity.HeaderViewHolder var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.titleTextView = null;
      }
   }
}
