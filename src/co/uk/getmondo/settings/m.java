package co.uk.getmondo.settings;

import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;

// $FF: synthetic class
final class m implements OnSharedPreferenceChangeListener {
   private final io.reactivex.o a;

   private m(io.reactivex.o var1) {
      this.a = var1;
   }

   public static OnSharedPreferenceChangeListener a(io.reactivex.o var0) {
      return new m(var0);
   }

   public void onSharedPreferenceChanged(SharedPreferences var1, String var2) {
      k.a(this.a, var1, var2);
   }
}
