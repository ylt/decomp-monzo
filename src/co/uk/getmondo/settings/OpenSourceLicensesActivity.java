package co.uk.getmondo.settings;

import android.animation.ArgbEvaluator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class OpenSourceLicensesActivity extends co.uk.getmondo.common.activities.b {
   private static final AtomicInteger a = new AtomicInteger(0);
   @BindView(2131821005)
   AppBarLayout appBarLayout;
   @BindView(2131821006)
   CollapsingToolbarLayout collapsingToolbarLayout;
   @BindView(2131821007)
   TextView descriptionTextView;
   @BindView(2131821008)
   RecyclerView recyclerView;

   public static void a(Context var0) {
      var0.startActivity(new Intent(var0, OpenSourceLicensesActivity.class));
   }

   // $FF: synthetic method
   static void a(OpenSourceLicensesActivity var0, AppBarLayout var1, int var2) {
      float var3 = Math.abs((float)var2 / (float)var1.getTotalScrollRange());
      var0.descriptionTextView.setAlpha(1.0F - 1.5F * var3);
      var2 = android.support.v4.content.a.c(var0, 17170445);
      int var4 = android.support.v4.content.a.c(var0, 2131689706);
      Object var5 = (new ArgbEvaluator()).evaluate(var3, Integer.valueOf(var2), Integer.valueOf(var4));
      var0.collapsingToolbarLayout.setCollapsedTitleTextColor(((Integer)var5).intValue());
      var0.collapsingToolbarLayout.setExpandedTitleColor(((Integer)var5).intValue());
   }

   public void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.setContentView(2131034190);
      ButterKnife.bind((Activity)this);
      this.descriptionTextView.setText(this.getString(2131362684, new Object[]{co.uk.getmondo.common.k.e.b()}));
      OpenSourceLicensesActivity.d var2 = new OpenSourceLicensesActivity.d();
      this.recyclerView.setHasFixedSize(true);
      this.recyclerView.setLayoutManager(new LinearLayoutManager(this));
      this.recyclerView.setAdapter(var2);
      this.recyclerView.a(new a.a.a.a.a.b(var2));
      this.appBarLayout.a(r.a(this));
   }

   static class HeaderViewHolder extends android.support.v7.widget.RecyclerView.w {
      @BindView(2131821599)
      TextView titleTextView;

      HeaderViewHolder(View var1) {
         super(var1);
         ButterKnife.bind(this, (View)var1);
      }

      public void a(OpenSourceLicensesActivity.c var1) {
         this.titleTextView.setText(var1.c.z);
         this.titleTextView.setVisibility(0);
      }
   }

   static class OpenSourceLibraryHolder extends android.support.v7.widget.RecyclerView.w {
      @BindView(2131821605)
      TextView libraryLicenseTextView;
      @BindView(2131821604)
      TextView libraryNameTextView;

      private OpenSourceLibraryHolder(View var1) {
         super(var1);
         ButterKnife.bind(this, (View)var1);
      }

      // $FF: synthetic method
      OpenSourceLibraryHolder(View var1, Object var2) {
         this(var1);
      }

      public void a(OpenSourceLicensesActivity.c var1) {
         this.libraryNameTextView.setText(var1.a);
         this.libraryLicenseTextView.setText(var1.b.e);
      }
   }

   private static enum a {
      a("Amulya Khare"),
      b("Android"),
      c("Artem Zinnatullin"),
      d("Chris Banes"),
      e("Eduardo Barrenechea"),
      f("Esko Luontola and contributors"),
      g("Facebook"),
      h("Google"),
      i("Jake Wharton"),
      j("Airbnb"),
      k("memoizr"),
      l("Michael Rozumyanskiy"),
      m("Monzo"),
      n("ReactiveX"),
      o("relex"),
      p("Square"),
      q("Stripe"),
      r("sannies"),
      s("JUnit"),
      t("AssertJ contributors"),
      u("Mockito contributors"),
      v("LinkedIn"),
      w("Realm"),
      x("JodaOrg");

      private final int y;
      private final String z;

      private a(String var3) {
         this.y = OpenSourceLicensesActivity.a.getAndIncrement();
         this.z = var3;
      }
   }

   private static enum b {
      a("Apache 2.0"),
      b("MIT"),
      c("Facebook Platform License"),
      d("Eclipse Public License 1.0");

      private final String e;

      private b(String var3) {
         this.e = var3;
      }
   }

   private static class c {
      private final String a;
      private final OpenSourceLicensesActivity.b b;
      private final OpenSourceLicensesActivity.a c;

      c(String var1, OpenSourceLicensesActivity.b var2, OpenSourceLicensesActivity.a var3) {
         this.a = var1;
         this.b = var2;
         this.c = var3;
      }
   }

   private static class d extends android.support.v7.widget.RecyclerView.a implements a.a.a.a.a.a {
      private final List a;

      d() {
         this.a = Arrays.asList(new OpenSourceLicensesActivity.c[]{new OpenSourceLicensesActivity.c("AppCompat", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.b), new OpenSourceLicensesActivity.c("AOSP", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.b), new OpenSourceLicensesActivity.c("CardView", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.b), new OpenSourceLicensesActivity.c("Constraint Layout", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.b), new OpenSourceLicensesActivity.c("Custom Tabs", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.b), new OpenSourceLicensesActivity.c("Design", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.b), new OpenSourceLicensesActivity.c("ExifInterface", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.b), new OpenSourceLicensesActivity.c("MultiDex", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.b), new OpenSourceLicensesActivity.c("RecyclerView", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.b), new OpenSourceLicensesActivity.c("ButterKnife", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.i), new OpenSourceLicensesActivity.c("Glide", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.h), new OpenSourceLicensesActivity.c("PhotoView", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.d), new OpenSourceLicensesActivity.c("CircleIndicator", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.o), new OpenSourceLicensesActivity.c("TextDrawable", OpenSourceLicensesActivity.b.b, OpenSourceLicensesActivity.a.a), new OpenSourceLicensesActivity.c("power-optional", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.k), new OpenSourceLicensesActivity.c("header-decor", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.e), new OpenSourceLicensesActivity.c("Gson", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.h), new OpenSourceLicensesActivity.c("Retrofit 2", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.p), new OpenSourceLicensesActivity.c("Retrofit 2 Converter (Gson)", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.p), new OpenSourceLicensesActivity.c("Retrofit 2 Adapter RxJava", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.p), new OpenSourceLicensesActivity.c("OkHttp", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.p), new OpenSourceLicensesActivity.c("OkHttp Logging Interceptor", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.p), new OpenSourceLicensesActivity.c("OkHttp MockWebServer", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.p), new OpenSourceLicensesActivity.c("Okio", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.p), new OpenSourceLicensesActivity.c("Leak Canary", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.p), new OpenSourceLicensesActivity.c("Facebook SDK", OpenSourceLicensesActivity.b.c, OpenSourceLicensesActivity.a.g), new OpenSourceLicensesActivity.c("Stripe", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.q), new OpenSourceLicensesActivity.c("libphonenumber", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.l), new OpenSourceLicensesActivity.c("Retrolambda", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.f), new OpenSourceLicensesActivity.c("RxJava", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.n), new OpenSourceLicensesActivity.c("RxAndroid", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.n), new OpenSourceLicensesActivity.c("RxJava Proguard Rules", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.c), new OpenSourceLicensesActivity.c("Timber", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.i), new OpenSourceLicensesActivity.c("RxBinding", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.i), new OpenSourceLicensesActivity.c("ThreeTenAbp", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.i), new OpenSourceLicensesActivity.c("Dagger 2", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.h), new OpenSourceLicensesActivity.c("cameraview", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.m), new OpenSourceLicensesActivity.c("mp4parser", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.r), new OpenSourceLicensesActivity.c("JUnit", OpenSourceLicensesActivity.b.d, OpenSourceLicensesActivity.a.s), new OpenSourceLicensesActivity.c("AssertJ", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.t), new OpenSourceLicensesActivity.c("Espresso", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.h), new OpenSourceLicensesActivity.c("Dexmaker", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.v), new OpenSourceLicensesActivity.c("Realm Java", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.w), new OpenSourceLicensesActivity.c("Joda Money", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.x), new OpenSourceLicensesActivity.c("Lottie", OpenSourceLicensesActivity.b.a, OpenSourceLicensesActivity.a.j)});
         Collections.sort(this.a, s.a());
      }

      // $FF: synthetic method
      static int a(OpenSourceLicensesActivity.c var0, OpenSourceLicensesActivity.c var1) {
         int var2;
         if(var0.c.y == var1.c.y) {
            var2 = var0.a.compareTo(var1.a);
         } else {
            var2 = var0.c.z.compareTo(var1.c.z);
         }

         return var2;
      }

      public long a(int var1) {
         return (long)((OpenSourceLicensesActivity.c)this.a.get(var1)).c.y;
      }

      // $FF: synthetic method
      public android.support.v7.widget.RecyclerView.w a(ViewGroup var1) {
         return this.b(var1);
      }

      public OpenSourceLicensesActivity.OpenSourceLibraryHolder a(ViewGroup var1, int var2) {
         return new OpenSourceLicensesActivity.OpenSourceLibraryHolder(LayoutInflater.from(var1.getContext()).inflate(2131034374, var1, false));
      }

      public void a(OpenSourceLicensesActivity.HeaderViewHolder var1, int var2) {
         var1.a((OpenSourceLicensesActivity.c)this.a.get(var2));
      }

      public void a(OpenSourceLicensesActivity.OpenSourceLibraryHolder var1, int var2) {
         var1.a((OpenSourceLicensesActivity.c)this.a.get(var2));
      }

      public OpenSourceLicensesActivity.HeaderViewHolder b(ViewGroup var1) {
         return new OpenSourceLicensesActivity.HeaderViewHolder(LayoutInflater.from(var1.getContext()).inflate(2131034371, var1, false));
      }

      public int getItemCount() {
         return this.a.size();
      }

      // $FF: synthetic method
      public void onBindViewHolder(android.support.v7.widget.RecyclerView.w var1, int var2) {
         this.a((OpenSourceLicensesActivity.OpenSourceLibraryHolder)var1, var2);
      }

      // $FF: synthetic method
      public android.support.v7.widget.RecyclerView.w onCreateViewHolder(ViewGroup var1, int var2) {
         return this.a(var1, var2);
      }
   }
}
