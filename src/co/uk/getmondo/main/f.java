package co.uk.getmondo.main;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import co.uk.getmondo.d.aa;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\b\u0086\b\u0018\u0000 $2\u00020\u0001:\u0001$B\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004B!\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\b\u0010\u0007\u001a\u0004\u0018\u00010\b\u0012\b\u0010\t\u001a\u0004\u0018\u00010\u0006¢\u0006\u0002\u0010\nJ\t\u0010\u0010\u001a\u00020\u0006HÆ\u0003J\u000b\u0010\u0011\u001a\u0004\u0018\u00010\bHÆ\u0003J\u000b\u0010\u0012\u001a\u0004\u0018\u00010\u0006HÆ\u0003J+\u0010\u0013\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u00062\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\b2\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u0006HÆ\u0001J\b\u0010\u0014\u001a\u00020\u0015H\u0016J\u0013\u0010\u0016\u001a\u00020\u00172\b\u0010\u0018\u001a\u0004\u0018\u00010\u0019HÖ\u0003J\t\u0010\u001a\u001a\u00020\u0015HÖ\u0001J\u000e\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001eJ\t\u0010\u001f\u001a\u00020\u0006HÖ\u0001J\u0018\u0010 \u001a\u00020!2\u0006\u0010\"\u001a\u00020\u00032\u0006\u0010#\u001a\u00020\u0015H\u0016R\u0013\u0010\u0007\u001a\u0004\u0018\u00010\b¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0013\u0010\t\u001a\u0004\u0018\u00010\u0006¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u000e¨\u0006%"},
   d2 = {"Lco/uk/getmondo/main/MonzoMeData;", "Landroid/os/Parcelable;", "parcel", "Landroid/os/Parcel;", "(Landroid/os/Parcel;)V", "username", "", "amount", "Lco/uk/getmondo/model/Amount;", "notes", "(Ljava/lang/String;Lco/uk/getmondo/model/Amount;Ljava/lang/String;)V", "getAmount", "()Lco/uk/getmondo/model/Amount;", "getNotes", "()Ljava/lang/String;", "getUsername", "component1", "component2", "component3", "copy", "describeContents", "", "equals", "", "other", "", "hashCode", "toPeerPayment", "Lco/uk/getmondo/payments/send/data/model/PeerPayment;", "peer", "Lco/uk/getmondo/model/Peer;", "toString", "writeToParcel", "", "dest", "flags", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class f implements Parcelable {
   public static final Creator CREATOR = (Creator)(new Creator() {
      public f a(Parcel var1) {
         l.b(var1, "source");
         return new f(var1);
      }

      public f[] a(int var1) {
         return new f[var1];
      }

      // $FF: synthetic method
      public Object createFromParcel(Parcel var1) {
         return this.a(var1);
      }

      // $FF: synthetic method
      public Object[] newArray(int var1) {
         return (Object[])this.a(var1);
      }
   });
   public static final f.a a = new f.a((kotlin.d.b.i)null);
   private final String b;
   private final co.uk.getmondo.d.c c;
   private final String d;

   public f(Parcel var1) {
      l.b(var1, "parcel");
      String var2 = var1.readString();
      l.a(var2, "parcel.readString()");
      this(var2, (co.uk.getmondo.d.c)var1.readParcelable(co.uk.getmondo.d.c.class.getClassLoader()), var1.readString());
   }

   public f(String var1, co.uk.getmondo.d.c var2, String var3) {
      l.b(var1, "username");
      super();
      this.b = var1;
      this.c = var2;
      this.d = var3;
   }

   public final co.uk.getmondo.payments.send.data.a.g a(aa var1) {
      l.b(var1, "peer");
      co.uk.getmondo.d.c var2 = this.c;
      if(var2 == null) {
         co.uk.getmondo.common.i.c var4 = co.uk.getmondo.common.i.c.a;
         l.a(var4, "Currency.GBP");
         var2 = new co.uk.getmondo.d.c(0L, var4);
      }

      String var3 = this.d;
      if(var3 == null) {
         var3 = "";
      }

      return new co.uk.getmondo.payments.send.data.a.g(var2, var3, var1);
   }

   public final String a() {
      return this.b;
   }

   public int describeContents() {
      return 0;
   }

   public boolean equals(Object var1) {
      boolean var2;
      if(this != var1) {
         label30: {
            if(var1 instanceof f) {
               f var3 = (f)var1;
               if(l.a(this.b, var3.b) && l.a(this.c, var3.c) && l.a(this.d, var3.d)) {
                  break label30;
               }
            }

            var2 = false;
            return var2;
         }
      }

      var2 = true;
      return var2;
   }

   public int hashCode() {
      int var3 = 0;
      String var4 = this.b;
      int var1;
      if(var4 != null) {
         var1 = var4.hashCode();
      } else {
         var1 = 0;
      }

      co.uk.getmondo.d.c var5 = this.c;
      int var2;
      if(var5 != null) {
         var2 = var5.hashCode();
      } else {
         var2 = 0;
      }

      var4 = this.d;
      if(var4 != null) {
         var3 = var4.hashCode();
      }

      return (var2 + var1 * 31) * 31 + var3;
   }

   public String toString() {
      return "MonzoMeData(username=" + this.b + ", amount=" + this.c + ", notes=" + this.d + ")";
   }

   public void writeToParcel(Parcel var1, int var2) {
      l.b(var1, "dest");
      var1.writeString(this.b);
      var1.writeParcelable((Parcelable)this.c, var2);
      var1.writeString(this.d);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"},
      d2 = {"Lco/uk/getmondo/main/MonzoMeData$Companion;", "", "()V", "CREATOR", "Landroid/os/Parcelable$Creator;", "Lco/uk/getmondo/main/MonzoMeData;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var1) {
         this();
      }
   }
}
