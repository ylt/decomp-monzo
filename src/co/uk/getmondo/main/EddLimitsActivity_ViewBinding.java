package co.uk.getmondo.main;

import android.view.View;
import android.view.View.OnClickListener;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;

public class EddLimitsActivity_ViewBinding implements Unbinder {
   private EddLimitsActivity a;
   private View b;
   private View c;

   public EddLimitsActivity_ViewBinding(final EddLimitsActivity var1, View var2) {
      this.a = var1;
      View var3 = Utils.findRequiredView(var2, 2131820888, "method 'onViewLimitsClicked'");
      this.b = var3;
      var3.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View var1x) {
            var1.onViewLimitsClicked();
         }
      });
      var2 = Utils.findRequiredView(var2, 2131820889, "method 'onNotNowClicked'");
      this.c = var2;
      var2.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View var1x) {
            var1.onNotNowClicked();
         }
      });
   }

   public void unbind() {
      if(this.a == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         this.b.setOnClickListener((OnClickListener)null);
         this.b = null;
         this.c.setOnClickListener((OnClickListener)null);
         this.c = null;
      }
   }
}
