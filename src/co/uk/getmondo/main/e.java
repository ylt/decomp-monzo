package co.uk.getmondo.main;

import co.uk.getmondo.api.IdentityVerificationApi;
import co.uk.getmondo.api.ServiceStatusApi;
import co.uk.getmondo.common.o;
import co.uk.getmondo.common.x;
import co.uk.getmondo.feed.a.t;
import co.uk.getmondo.payments.send.data.PeerToPeerRepository;
import co.uk.getmondo.payments.send.data.p;
import io.reactivex.u;

public final class e implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final b.a b;
   private final javax.a.a c;
   private final javax.a.a d;
   private final javax.a.a e;
   private final javax.a.a f;
   private final javax.a.a g;
   private final javax.a.a h;
   private final javax.a.a i;
   private final javax.a.a j;
   private final javax.a.a k;
   private final javax.a.a l;
   private final javax.a.a m;
   private final javax.a.a n;
   private final javax.a.a o;
   private final javax.a.a p;
   private final javax.a.a q;
   private final javax.a.a r;
   private final javax.a.a s;
   private final javax.a.a t;
   private final javax.a.a u;

   static {
      boolean var0;
      if(!e.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   public e(b.a var1, javax.a.a var2, javax.a.a var3, javax.a.a var4, javax.a.a var5, javax.a.a var6, javax.a.a var7, javax.a.a var8, javax.a.a var9, javax.a.a var10, javax.a.a var11, javax.a.a var12, javax.a.a var13, javax.a.a var14, javax.a.a var15, javax.a.a var16, javax.a.a var17, javax.a.a var18, javax.a.a var19, javax.a.a var20) {
      if(!a && var1 == null) {
         throw new AssertionError();
      } else {
         this.b = var1;
         if(!a && var2 == null) {
            throw new AssertionError();
         } else {
            this.c = var2;
            if(!a && var3 == null) {
               throw new AssertionError();
            } else {
               this.d = var3;
               if(!a && var4 == null) {
                  throw new AssertionError();
               } else {
                  this.e = var4;
                  if(!a && var5 == null) {
                     throw new AssertionError();
                  } else {
                     this.f = var5;
                     if(!a && var6 == null) {
                        throw new AssertionError();
                     } else {
                        this.g = var6;
                        if(!a && var7 == null) {
                           throw new AssertionError();
                        } else {
                           this.h = var7;
                           if(!a && var8 == null) {
                              throw new AssertionError();
                           } else {
                              this.i = var8;
                              if(!a && var9 == null) {
                                 throw new AssertionError();
                              } else {
                                 this.j = var9;
                                 if(!a && var10 == null) {
                                    throw new AssertionError();
                                 } else {
                                    this.k = var10;
                                    if(!a && var11 == null) {
                                       throw new AssertionError();
                                    } else {
                                       this.l = var11;
                                       if(!a && var12 == null) {
                                          throw new AssertionError();
                                       } else {
                                          this.m = var12;
                                          if(!a && var13 == null) {
                                             throw new AssertionError();
                                          } else {
                                             this.n = var13;
                                             if(!a && var14 == null) {
                                                throw new AssertionError();
                                             } else {
                                                this.o = var14;
                                                if(!a && var15 == null) {
                                                   throw new AssertionError();
                                                } else {
                                                   this.p = var15;
                                                   if(!a && var16 == null) {
                                                      throw new AssertionError();
                                                   } else {
                                                      this.q = var16;
                                                      if(!a && var17 == null) {
                                                         throw new AssertionError();
                                                      } else {
                                                         this.r = var17;
                                                         if(!a && var18 == null) {
                                                            throw new AssertionError();
                                                         } else {
                                                            this.s = var18;
                                                            if(!a && var19 == null) {
                                                               throw new AssertionError();
                                                            } else {
                                                               this.t = var19;
                                                               if(!a && var20 == null) {
                                                                  throw new AssertionError();
                                                               } else {
                                                                  this.u = var20;
                                                               }
                                                            }
                                                         }
                                                      }
                                                   }
                                                }
                                             }
                                          }
                                       }
                                    }
                                 }
                              }
                           }
                        }
                     }
                  }
               }
            }
         }
      }
   }

   public static b.a.b a(b.a var0, javax.a.a var1, javax.a.a var2, javax.a.a var3, javax.a.a var4, javax.a.a var5, javax.a.a var6, javax.a.a var7, javax.a.a var8, javax.a.a var9, javax.a.a var10, javax.a.a var11, javax.a.a var12, javax.a.a var13, javax.a.a var14, javax.a.a var15, javax.a.a var16, javax.a.a var17, javax.a.a var18, javax.a.a var19) {
      return new e(var0, var1, var2, var3, var4, var5, var6, var7, var8, var9, var10, var11, var12, var13, var14, var15, var16, var17, var18, var19);
   }

   public c a() {
      return (c)b.a.c.a(this.b, new c((u)this.c.b(), (u)this.d.b(), (co.uk.getmondo.common.accounts.d)this.e.b(), (co.uk.getmondo.common.e.a)this.f.b(), (co.uk.getmondo.api.b.a)this.g.b(), (co.uk.getmondo.common.a)this.h.b(), (ServiceStatusApi)this.i.b(), (co.uk.getmondo.payments.send.data.h)this.j.b(), (p)this.k.b(), (PeerToPeerRepository)this.l.b(), (co.uk.getmondo.news.d)this.m.b(), (co.uk.getmondo.background_sync.d)this.n.b(), (x)this.o.b(), (IdentityVerificationApi)this.p.b(), (h)this.q.b(), (t)this.r.b(), (o)this.s.b(), (co.uk.getmondo.card.c)this.t.b(), (co.uk.getmondo.signup.status.b)this.u.b()));
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
