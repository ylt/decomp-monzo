package co.uk.getmondo.main;

import android.support.v4.app.Fragment;

public enum g {
   a(1, 2131821772, co.uk.getmondo.feed.e.class),
   b(2, 2131821773, co.uk.getmondo.spending.b.class),
   c(3, 2131821774, co.uk.getmondo.card.a.class),
   d(4, 2131821775, co.uk.getmondo.c.a.class);

   final int e;
   int f;
   Class g;

   private g(int var3, int var4, Class var5) {
      this.e = var3;
      this.f = var4;
      this.g = var5;
   }

   static g a(int var0) {
      g[] var4 = values();
      int var2 = var4.length;
      int var1 = 0;

      g var3;
      while(true) {
         if(var1 >= var2) {
            var3 = null;
            break;
         }

         var3 = var4[var1];
         if(var3.e == var0) {
            break;
         }

         ++var1;
      }

      return var3;
   }

   public static g b(int var0) {
      g[] var4 = values();
      int var2 = var4.length;
      int var1 = 0;

      g var3;
      while(true) {
         if(var1 >= var2) {
            var3 = null;
            break;
         }

         var3 = var4[var1];
         if(var3.f == var0) {
            break;
         }

         ++var1;
      }

      return var3;
   }

   Fragment a() {
      Object var1;
      try {
         Fragment var4 = (Fragment)this.g.newInstance();
         return var4;
      } catch (InstantiationException var2) {
         var1 = var2;
      } catch (IllegalAccessException var3) {
         var1 = var3;
      }

      throw new RuntimeException((Throwable)var1);
   }
}
