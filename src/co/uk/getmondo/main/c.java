package co.uk.getmondo.main;

import co.uk.getmondo.api.IdentityVerificationApi;
import co.uk.getmondo.api.ServiceStatusApi;
import co.uk.getmondo.api.model.identity_verification.IdentityVerification;
import co.uk.getmondo.api.model.service_status.ServiceStatusIncident;
import co.uk.getmondo.api.model.sign_up.MigrationInfo;
import co.uk.getmondo.api.model.signup.SignupInfo;
import co.uk.getmondo.api.model.signup.SignupSource;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.common.o;
import co.uk.getmondo.common.x;
import co.uk.getmondo.d.aa;
import co.uk.getmondo.d.ac;
import co.uk.getmondo.d.ad;
import co.uk.getmondo.d.ag;
import co.uk.getmondo.d.ah;
import co.uk.getmondo.d.ak;
import co.uk.getmondo.feed.a.t;
import co.uk.getmondo.payments.send.data.PeerToPeerRepository;
import co.uk.getmondo.payments.send.data.p;
import io.reactivex.r;
import io.reactivex.u;
import io.reactivex.v;
import io.reactivex.c.q;
import java.util.Collection;
import java.util.List;
import kotlin.Metadata;
import kotlin.n;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u008c\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001/B£\u0001\b\u0007\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\f\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u0012\u0006\u0010\u0014\u001a\u00020\u0015\u0012\u0006\u0010\u0016\u001a\u00020\u0017\u0012\u0006\u0010\u0018\u001a\u00020\u0019\u0012\u0006\u0010\u001a\u001a\u00020\u001b\u0012\u0006\u0010\u001c\u001a\u00020\u001d\u0012\u0006\u0010\u001e\u001a\u00020\u001f\u0012\u0006\u0010 \u001a\u00020!\u0012\u0006\u0010\"\u001a\u00020#\u0012\u0006\u0010$\u001a\u00020%\u0012\u0006\u0010&\u001a\u00020'¢\u0006\u0002\u0010(J\u0010\u0010+\u001a\u00020,2\u0006\u0010)\u001a\u00020*H\u0002J\u0010\u0010-\u001a\u00020,2\u0006\u0010.\u001a\u00020\u0002H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010$\u001a\u00020%X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\"\u001a\u00020#X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u001dX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u0010\u0010)\u001a\u0004\u0018\u00010*X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010 \u001a\u00020!X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u001e\u001a\u00020\u001fX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010&\u001a\u00020'X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004¢\u0006\u0002\n\u0000¨\u00060"},
   d2 = {"Lco/uk/getmondo/main/HomePresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/main/HomePresenter$View;", "uiScheduler", "Lio/reactivex/Scheduler;", "ioScheduler", "accountService", "Lco/uk/getmondo/common/accounts/AccountService;", "apiErrorHandler", "Lco/uk/getmondo/common/errors/ApiErrorHandler;", "userInteractor", "Lco/uk/getmondo/api/interactors/UserInteractor;", "analyticsService", "Lco/uk/getmondo/common/AnalyticsService;", "serviceStatusApi", "Lco/uk/getmondo/api/ServiceStatusApi;", "userSettingsRepository", "Lco/uk/getmondo/payments/send/data/UserSettingsRepository;", "userSettingsStorage", "Lco/uk/getmondo/payments/send/data/UserSettingsStorage;", "peerToPeerRepository", "Lco/uk/getmondo/payments/send/data/PeerToPeerRepository;", "newsManager", "Lco/uk/getmondo/news/NewsManager;", "syncManager", "Lco/uk/getmondo/background_sync/SyncManager;", "sddMigrationStorage", "Lco/uk/getmondo/common/SddMigrationStorage;", "identityVerificationApi", "Lco/uk/getmondo/api/IdentityVerificationApi;", "sddDismissibleState", "Lco/uk/getmondo/main/SddDismissibleState;", "peerManager", "Lco/uk/getmondo/feed/data/PeerManager;", "featureFlagsStorage", "Lco/uk/getmondo/common/FeatureFlagsStorage;", "cardManager", "Lco/uk/getmondo/card/CardManager;", "signupStatusManager", "Lco/uk/getmondo/signup/status/SignupStatusManager;", "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/common/accounts/AccountService;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/api/interactors/UserInteractor;Lco/uk/getmondo/common/AnalyticsService;Lco/uk/getmondo/api/ServiceStatusApi;Lco/uk/getmondo/payments/send/data/UserSettingsRepository;Lco/uk/getmondo/payments/send/data/UserSettingsStorage;Lco/uk/getmondo/payments/send/data/PeerToPeerRepository;Lco/uk/getmondo/news/NewsManager;Lco/uk/getmondo/background_sync/SyncManager;Lco/uk/getmondo/common/SddMigrationStorage;Lco/uk/getmondo/api/IdentityVerificationApi;Lco/uk/getmondo/main/SddDismissibleState;Lco/uk/getmondo/feed/data/PeerManager;Lco/uk/getmondo/common/FeatureFlagsStorage;Lco/uk/getmondo/card/CardManager;Lco/uk/getmondo/signup/status/SignupStatusManager;)V", "migrationInfo", "Lco/uk/getmondo/api/model/sign_up/MigrationInfo;", "displayMigrationBanner", "", "register", "view", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class c extends co.uk.getmondo.common.ui.b {
   private MigrationInfo c;
   private final u d;
   private final u e;
   private final co.uk.getmondo.common.accounts.d f;
   private final co.uk.getmondo.common.e.a g;
   private final co.uk.getmondo.api.b.a h;
   private final co.uk.getmondo.common.a i;
   private final ServiceStatusApi j;
   private final co.uk.getmondo.payments.send.data.h k;
   private final p l;
   private final PeerToPeerRepository m;
   private final co.uk.getmondo.news.d n;
   private final co.uk.getmondo.background_sync.d o;
   private final x p;
   private final IdentityVerificationApi q;
   private final h r;
   private final t s;
   private final o t;
   private final co.uk.getmondo.card.c u;
   private final co.uk.getmondo.signup.status.b v;

   public c(u var1, u var2, co.uk.getmondo.common.accounts.d var3, co.uk.getmondo.common.e.a var4, co.uk.getmondo.api.b.a var5, co.uk.getmondo.common.a var6, ServiceStatusApi var7, co.uk.getmondo.payments.send.data.h var8, p var9, PeerToPeerRepository var10, co.uk.getmondo.news.d var11, co.uk.getmondo.background_sync.d var12, x var13, IdentityVerificationApi var14, h var15, t var16, o var17, co.uk.getmondo.card.c var18, co.uk.getmondo.signup.status.b var19) {
      l.b(var1, "uiScheduler");
      l.b(var2, "ioScheduler");
      l.b(var3, "accountService");
      l.b(var4, "apiErrorHandler");
      l.b(var5, "userInteractor");
      l.b(var6, "analyticsService");
      l.b(var7, "serviceStatusApi");
      l.b(var8, "userSettingsRepository");
      l.b(var9, "userSettingsStorage");
      l.b(var10, "peerToPeerRepository");
      l.b(var11, "newsManager");
      l.b(var12, "syncManager");
      l.b(var13, "sddMigrationStorage");
      l.b(var14, "identityVerificationApi");
      l.b(var15, "sddDismissibleState");
      l.b(var16, "peerManager");
      l.b(var17, "featureFlagsStorage");
      l.b(var18, "cardManager");
      l.b(var19, "signupStatusManager");
      super();
      this.d = var1;
      this.e = var2;
      this.f = var3;
      this.g = var4;
      this.h = var5;
      this.i = var6;
      this.j = var7;
      this.k = var8;
      this.l = var9;
      this.m = var10;
      this.n = var11;
      this.o = var12;
      this.p = var13;
      this.q = var14;
      this.r = var15;
      this.s = var16;
      this.t = var17;
      this.u = var18;
      this.v = var19;
   }

   private final void a(MigrationInfo var1) {
      SignupInfo.Status var3 = var1.e();
      SignupInfo.Stage var2 = var1.f();
      if(!var1.b()) {
         ((c.a)this.a).a(var1.c(), var1.d(), false);
         this.i.a(Impression.Companion.aX());
      } else if(l.a(var3, SignupInfo.Status.NOT_STARTED)) {
         ((c.a)this.a).a(var1.c(), var1.d(), true);
         this.i.a(Impression.Companion.ba());
      } else if(l.a(var2, SignupInfo.Stage.CARD_ACTIVATION)) {
         ((c.a)this.a).M();
      } else {
         ((c.a)this.a).L();
         this.i.a(Impression.Companion.bg());
      }

   }

   public void a(final c.a var1) {
      l.b(var1, "view");
      super.a((co.uk.getmondo.common.ui.f)var1);
      ak var4 = this.f.b();
      if(var4 == null) {
         l.a();
      }

      final ac var3 = var4.d();
      co.uk.getmondo.d.a var5 = var4.c();
      Boolean var9 = co.uk.getmondo.a.b;
      l.a(var9, "BuildConfig.GHOST");
      if(var9.booleanValue()) {
         if(var3 == null) {
            l.a();
         }

         var1.a(var3.d());
      }

      final boolean var2;
      if(var5 != null && var5.e()) {
         var2 = true;
      } else {
         var2 = false;
      }

      if(var2) {
         ad var10 = (ad)var5;
         if(var10 == null) {
            l.a();
         }

         var1.b(var10.g(), var10.j());
      }

      if(var3 == null) {
         l.a();
      }

      var1.a(var3.a(), var3.d());
      io.reactivex.b.a var12 = this.b;
      io.reactivex.b.b var11 = this.h.a().b(this.e).a(this.d).a((io.reactivex.c.g)null.a, (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Throwable var1x) {
            co.uk.getmondo.common.e.a var2 = c.this.g;
            l.a(var1x, "error");
            var2.a(var1x, (co.uk.getmondo.common.e.a.a)var1);
         }
      }));
      l.a(var11, "userInteractor.refreshAc…ndleError(error, view) })");
      io.reactivex.rxkotlin.a.a(var12, var11);
      var12 = this.b;
      var11 = this.u.c().b(this.e).a(this.d).a((io.reactivex.c.a)null.a, (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Throwable var1x) {
            co.uk.getmondo.common.e.a var2 = c.this.g;
            l.a(var1x, "error");
            var2.a(var1x, (co.uk.getmondo.common.e.a.a)var1);
         }
      }));
      l.a(var11, "cardManager.syncCard()\n …view) }\n                )");
      io.reactivex.rxkotlin.a.a(var12, var11);
      var12 = this.b;
      var11 = this.n.a().b(this.e).a(this.d).a((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(co.uk.getmondo.news.c var1x) {
            c.a var2 = var1;
            l.a(var1x, "it");
            var2.a(var1x);
         }
      }), (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Throwable var1x) {
            co.uk.getmondo.common.e.a var2 = c.this.g;
            l.a(var1x, "error");
            var2.a(var1x, (co.uk.getmondo.common.e.a.a)var1);
         }
      }));
      l.a(var11, "newsManager.unreadNewsIt…ndleError(error, view) })");
      io.reactivex.rxkotlin.a.a(var12, var11);
      var12 = this.b;
      var11 = var1.h().flatMapMaybe((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.h a(n var1) {
            l.b(var1, "it");
            return c.this.j.getUnresolvedIncidents().b((io.reactivex.c.h)null.a).filter((q)null.a).toList().b(c.this.e).e().f();
         }
      })).observeOn(this.d).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(List var1x) {
            boolean var2;
            if(!((Collection)var1x).isEmpty()) {
               var2 = true;
            } else {
               var2 = false;
            }

            if(var2) {
               c.a var3 = var1;
               Object var4 = var1x.get(0);
               l.a(var4, "incidents[0]");
               var3.a((ServiceStatusIncident)var4);
            } else {
               var1.E();
            }

         }
      }));
      l.a(var11, "view.onRefreshIncidents(…      }\n                }");
      io.reactivex.rxkotlin.a.a(var12, var11);
      var12 = this.b;
      var11 = var1.b().flatMapCompletable((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.b a(n var1x) {
            l.b(var1x, "it");
            return c.this.k.a().b(c.this.e).a(c.this.d).a((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(Throwable var1x) {
                  co.uk.getmondo.common.e.a var2 = c.this.g;
                  l.a(var1x, "error");
                  var2.a(var1x, (co.uk.getmondo.common.e.a.a)var1);
               }
            })).b();
         }
      })).a((io.reactivex.c.a)null.a, (io.reactivex.c.g)null.a);
      l.a(var11, "view.onRefreshUserSettin…ibe({}, { Timber.e(it) })");
      io.reactivex.rxkotlin.a.a(var12, var11);
      io.reactivex.b.a var14 = this.b;
      io.reactivex.b.b var15 = this.p.a().distinctUntilChanged().filter((q)null.a).switchMapSingle((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final v a(final ag var1x) {
            l.b(var1x, "sddMigration");
            return c.this.q.status(SignupSource.SDD_MIGRATION).a(3L).d((io.reactivex.c.h)(new io.reactivex.c.h() {
               public final kotlin.h a(IdentityVerification var1xx) {
                  l.b(var1xx, "identityVerification");
                  return new kotlin.h(var1x, var1xx);
               }
            })).b(c.this.e).a(c.this.d).d((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(Throwable var1x) {
                  co.uk.getmondo.common.e.a var2 = c.this.g;
                  l.a(var1x, "error");
                  var2.a(var1x, (co.uk.getmondo.common.e.a.a)var1);
               }
            }));
         }
      })).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.h var1x) {
            ag var3 = (ag)var1x.c();
            IdentityVerification var4 = (IdentityVerification)var1x.d();
            String var5 = var4.d();
            IdentityVerification.Status var7 = var4.e();
            boolean var2;
            if(var7 != IdentityVerification.Status.PENDING_SUBMISSION && var7 != IdentityVerification.Status.NOT_STARTED) {
               var2 = false;
            } else {
               var2 = true;
            }

            ah var6 = var3.a();
            if(var6 != null) {
               switch(d.a[var6.ordinal()]) {
               case 1:
                  if(!c.this.r.a() && var2) {
                     var1.d(var5);
                     c.this.r.a(true);
                  }
                  break;
               case 2:
                  if(var2) {
                     var1.e(var5);
                  } else if(var7 == IdentityVerification.Status.PENDING_REVIEW || var7 == IdentityVerification.Status.BLOCKED) {
                     var1.H();
                  }
               }
            }

         }
      }), (io.reactivex.c.g)null.a);
      l.a(var15, "sddMigrationStorage.sddM…     }, { Timber.e(it) })");
      io.reactivex.rxkotlin.a.a(var14, var15);
      var14 = this.b;
      io.reactivex.b.b var7 = var1.D().observeOn(this.d).doOnNext((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(f var1x) {
            var1.F();
         }
      })).switchMap((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.n a(final f var1x) {
            l.b(var1x, "monzoMeData");
            return c.this.m.a(var1x.a()).d((io.reactivex.c.h)(new io.reactivex.c.h() {
               public final kotlin.h a(aa var1xx) {
                  l.b(var1xx, "peer");
                  return new kotlin.h(var1xx, var1x);
               }
            })).b(c.this.e).a(c.this.d).d((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(Throwable var1x) {
                  var1.G();
                  d.a.a.a(var1x, "Failed opening a monzo.me link", new Object[0]);
                  if(var1x instanceof PeerToPeerRepository.a) {
                     var1.z();
                  } else if(var1x instanceof PeerToPeerRepository.UserHasP2pDisabledException) {
                     var1.A();
                  } else {
                     var1.B();
                  }

               }
            })).f().onErrorResumeNext((r)io.reactivex.n.empty());
         }
      })).observeOn(this.d).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.h var1x) {
            var1.G();
            aa var2 = (aa)var1x.a();
            f var4 = (f)var1x.b();
            if(l.a(var3.b(), var2.a())) {
               var1.y();
            } else {
               c.a var3x;
               if(l.a(c.this.l.a(), co.uk.getmondo.payments.send.data.a.d.b)) {
                  var3x = var1;
                  l.a(var2, "peer");
                  var3x.b(var4.a(var2));
               } else if(l.a(c.this.l.a(), co.uk.getmondo.payments.send.data.a.d.a)) {
                  var3x = var1;
                  l.a(var2, "peer");
                  var3x.a(var4.a(var2));
               }
            }

         }
      }), (io.reactivex.c.g)null.a);
      l.a(var7, "view.onPendingMonzoMeDat…     }, { Timber.e(it) })");
      io.reactivex.rxkotlin.a.a(var14, var7);
      var14 = this.b;
      var7 = var1.j().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(n var1x) {
            c.this.i.a(Impression.Companion.V());
            var1.x();
         }
      }));
      l.a(var7, "view.onSettingsClicked()…tings()\n                }");
      io.reactivex.rxkotlin.a.a(var14, var7);
      io.reactivex.b.a var8 = this.b;
      var11 = var1.k().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(n var1x) {
            c.this.i.a(Impression.Companion.a(Impression.OpenedCommunityFrom.FROM_MAIN_NAV));
            var1.e();
         }
      }));
      l.a(var11, "view.onCommunityClicked(…unity()\n                }");
      io.reactivex.rxkotlin.a.a(var8, var11);
      var8 = this.b;
      var11 = var1.w().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(n var1x) {
            c.this.i.a(Impression.Companion.a(Impression.Companion, Impression.IntercomFrom.MAIN_NAV, (String)null, (String)null, 6, (Object)null));
            var1.g();
         }
      }));
      l.a(var11, "view.onChatClicked()\n   …nChat()\n                }");
      io.reactivex.rxkotlin.a.a(var8, var11);
      var8 = this.b;
      var11 = var1.v().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(n var1x) {
            if(var2) {
               var1.f();
            } else {
               var1.c();
            }

         }
      }));
      l.a(var11, "view.onHelpClicked()\n   …      }\n                }");
      io.reactivex.rxkotlin.a.a(var8, var11);
      var8 = this.b;
      var11 = var1.a().doOnNext((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(n var1) {
            c.this.f.e();
         }
      })).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(n var1x) {
            var1.u();
         }
      }));
      l.a(var11, "view.onGhostLogOutClicke…ibe { view.openSplash() }");
      io.reactivex.rxkotlin.a.a(var8, var11);
      var8 = this.b;
      var11 = this.o.a().b(this.e).a((io.reactivex.c.a)null.a, (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Throwable var1x) {
            co.uk.getmondo.common.e.a var2 = c.this.g;
            l.a(var1x, "throwable");
            var2.a(var1x, (co.uk.getmondo.common.e.a.a)var1);
         }
      }));
      l.a(var11, "syncManager.syncFeedAndB…Error(throwable, view) })");
      io.reactivex.rxkotlin.a.a(var8, var11);
      var14 = this.b;
      var7 = this.s.a().subscribe((io.reactivex.c.g)null.a, (io.reactivex.c.g)null.a);
      l.a(var7, "peerManager.keepPeersEnr…iching peers\", error)) })");
      io.reactivex.rxkotlin.a.a(var14, var7);
      final io.reactivex.n var13 = var1.i().replay().a();
      var12 = this.b;
      var11 = var13.subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(g var1x) {
            if(l.a(var1x, g.d)) {
               boolean var2x;
               if(var2 && !c.this.t.b().a()) {
                  var2x = false;
               } else {
                  var2x = true;
               }

               if(var2x) {
                  co.uk.getmondo.payments.send.data.a.d var4 = c.this.l.a();
                  if(var4 != null) {
                     switch(d.b[var4.ordinal()]) {
                     case 1:
                        var1.a(g.d);
                        break;
                     case 2:
                        var1.d();
                        break;
                     case 3:
                        var1.C();
                     }
                  }
               } else {
                  var1.a(var1x);
               }
            } else {
               c.a var3 = var1;
               l.a(var1x, "screen");
               var3.a(var1x);
            }

         }
      }));
      l.a(var11, "onScreenChanged\n        …      }\n                }");
      io.reactivex.rxkotlin.a.a(var12, var11);
      if(!co.uk.getmondo.a.c.booleanValue() && !this.v.a()) {
         var14 = this.b;
         var7 = var1.J().switchMap((io.reactivex.c.h)(new io.reactivex.c.h() {
            public final io.reactivex.n a(n var1) {
               l.b(var1, "it");
               io.reactivex.rxkotlin.b var2 = io.reactivex.rxkotlin.b.a;
               io.reactivex.n var3 = c.this.v.d().subscribeOn(c.this.e);
               l.a(var3, "signupStatusManager.migr….subscribeOn(ioScheduler)");
               io.reactivex.n var4 = var13;
               l.a(var4, "onScreenChanged");
               return var2.a(var3, var4);
            }
         })).observeOn(this.d).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
            public final void a(kotlin.h var1x) {
               MigrationInfo var2 = (MigrationInfo)var1x.c();
               g var3 = (g)var1x.d();
               c.this.c = var2;
               if(var2.a() && l.a(var3, g.a)) {
                  c var4 = c.this;
                  l.a(var2, "migrationInfo");
                  var4.a(var2);
               } else {
                  var1.N();
               }

            }
         }), (io.reactivex.c.g)(new io.reactivex.c.g() {
            public final void a(Throwable var1x) {
               co.uk.getmondo.common.e.a var2 = c.this.g;
               l.a(var1x, "it");
               var2.a(var1x, (co.uk.getmondo.common.e.a.a)var1);
            }
         }));
         l.a(var7, "view.onRefreshMigrationB….handleError(it, view) })");
         io.reactivex.rxkotlin.a.a(var14, var7);
         var8 = this.b;
         io.reactivex.b.b var6 = var1.K().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
            public final void a(n var1x) {
               MigrationInfo var2 = c.this.c;
               if(var2 != null) {
                  if(var2.b() && l.a(var2.e(), SignupInfo.Status.NOT_STARTED) ^ true) {
                     var1.Q();
                     c.this.i.a(Impression.Companion.bh());
                  } else if(var2.b()) {
                     var1.P();
                  } else {
                     var1.O();
                  }
               }

            }
         }));
         l.a(var6, "view.onMigrationBannerCl…  }\n                    }");
         io.reactivex.rxkotlin.a.a(var8, var6);
      }

   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0013\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\bf\u0018\u00002\u00020\u00012\u00020\u0002J\b\u0010\u0003\u001a\u00020\u0004H&J\b\u0010\u0005\u001a\u00020\u0004H&J\b\u0010\u0006\u001a\u00020\u0004H&J\u000e\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00040\bH&J\u000e\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00040\bH&J\u000e\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00040\bH&J\u000e\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00040\bH&J\u000e\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00040\bH&J\u000e\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000e0\bH&J\u000e\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00040\bH&J\u000e\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00040\bH&J\u000e\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00040\bH&J\u000e\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00130\bH&J\u000e\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00040\bH&J\b\u0010\u0015\u001a\u00020\u0004H&J\b\u0010\u0016\u001a\u00020\u0004H&J\b\u0010\u0017\u001a\u00020\u0004H&J\b\u0010\u0018\u001a\u00020\u0004H&J\b\u0010\u0019\u001a\u00020\u0004H&J\b\u0010\u001a\u001a\u00020\u0004H&J\b\u0010\u001b\u001a\u00020\u0004H&J\u0010\u0010\u001c\u001a\u00020\u00042\u0006\u0010\u001d\u001a\u00020\u001eH&J\u0010\u0010\u001f\u001a\u00020\u00042\u0006\u0010\u001d\u001a\u00020\u001eH&J\u0012\u0010 \u001a\u00020\u00042\b\u0010!\u001a\u0004\u0018\u00010\"H&J\u0012\u0010#\u001a\u00020\u00042\b\u0010!\u001a\u0004\u0018\u00010\"H&J\b\u0010$\u001a\u00020\u0004H&J\b\u0010%\u001a\u00020\u0004H&J\b\u0010&\u001a\u00020\u0004H&J\u001a\u0010'\u001a\u00020\u00042\b\u0010(\u001a\u0004\u0018\u00010\"2\u0006\u0010)\u001a\u00020\"H&J\b\u0010*\u001a\u00020\u0004H&J\b\u0010+\u001a\u00020\u0004H&J\b\u0010,\u001a\u00020\u0004H&J\b\u0010-\u001a\u00020\u0004H&J\b\u0010.\u001a\u00020\u0004H&J\u0010\u0010/\u001a\u00020\u00042\u0006\u00100\u001a\u00020\"H&J\b\u00101\u001a\u00020\u0004H&J \u00102\u001a\u00020\u00042\u0006\u00103\u001a\u00020\"2\u0006\u00104\u001a\u00020\"2\u0006\u00105\u001a\u000206H&J\b\u00107\u001a\u00020\u0004H&J\u0010\u00108\u001a\u00020\u00042\u0006\u00109\u001a\u00020:H&J\u0010\u0010;\u001a\u00020\u00042\u0006\u0010<\u001a\u00020=H&J\b\u0010>\u001a\u00020\u0004H&J\u0018\u0010?\u001a\u00020\u00042\u0006\u0010@\u001a\u00020\"2\u0006\u0010A\u001a\u00020\"H&J\u0010\u0010B\u001a\u00020\u00042\u0006\u0010C\u001a\u00020\u0013H&¨\u0006D"},
      d2 = {"Lco/uk/getmondo/main/HomePresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "hideLoading", "", "hideMigrationBanner", "hideOutageWarning", "onChatClicked", "Lio/reactivex/Observable;", "onCommunityClicked", "onGhostLogOutClicked", "onHelpClicked", "onMigrationBannerClicked", "onPendingMonzoMeDataChanged", "Lco/uk/getmondo/main/MonzoMeData;", "onRefreshIncidents", "onRefreshMigrationBanner", "onRefreshUserSettings", "onScreenChanged", "Lco/uk/getmondo/main/Screen;", "onSettingsClicked", "openChat", "openCommunity", "openCurrentAccountComing", "openCurrentAccountHere", "openFaqs", "openHelp", "openP2pOnboardingFromContacts", "openP2pOnboardingFromMonzoMeDeepLink", "pendingPayment", "Lco/uk/getmondo/payments/send/data/model/PeerPayment;", "openPaymentInfo", "openSddMigrationDismissible", "rejectionNote", "", "openSddMigrationPersistent", "openSettings", "openSignup", "openVerificationPending", "setProfileInformation", "nameToDisplay", "emailAddress", "showActivateCardBanner", "showCannotPayYourself", "showContactHasP2pDisabled", "showContactNotOnMonzo", "showContinueSignupBanner", "showGhostBanner", "email", "showLoading", "showMigrationBanner", "title", "subtitle", "signupAllowed", "", "showMonzoMeGenericError", "showNews", "newsItem", "Lco/uk/getmondo/news/NewsItem;", "showOutageWarning", "incident", "Lco/uk/getmondo/api/model/service_status/ServiceStatusIncident;", "showP2pBlocked", "showRetailUi", "sortCode", "accountNumber", "showScreen", "screen", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      void A();

      void B();

      void C();

      io.reactivex.n D();

      void E();

      void F();

      void G();

      void H();

      io.reactivex.n J();

      io.reactivex.n K();

      void L();

      void M();

      void N();

      void O();

      void P();

      void Q();

      io.reactivex.n a();

      void a(ServiceStatusIncident var1);

      void a(g var1);

      void a(co.uk.getmondo.news.c var1);

      void a(co.uk.getmondo.payments.send.data.a.g var1);

      void a(String var1);

      void a(String var1, String var2);

      void a(String var1, String var2, boolean var3);

      io.reactivex.n b();

      void b(co.uk.getmondo.payments.send.data.a.g var1);

      void b(String var1, String var2);

      void c();

      void d();

      void d(String var1);

      void e();

      void e(String var1);

      void f();

      void g();

      io.reactivex.n h();

      io.reactivex.n i();

      io.reactivex.n j();

      io.reactivex.n k();

      io.reactivex.n v();

      io.reactivex.n w();

      void x();

      void y();

      void z();
   }
}
