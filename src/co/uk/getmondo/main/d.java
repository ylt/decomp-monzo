package co.uk.getmondo.main;

import co.uk.getmondo.d.ah;
import kotlin.Metadata;

// $FF: synthetic class
@Metadata(
   bv = {1, 0, 2},
   k = 3,
   mv = {1, 1, 7}
)
public final class d {
   // $FF: synthetic field
   public static final int[] a = new int[ah.values().length];
   // $FF: synthetic field
   public static final int[] b;

   static {
      a[ah.DISMISSIBLE.ordinal()] = 1;
      a[ah.PERSISTENT.ordinal()] = 2;
      b = new int[co.uk.getmondo.payments.send.data.a.d.values().length];
      b[co.uk.getmondo.payments.send.data.a.d.a.ordinal()] = 1;
      b[co.uk.getmondo.payments.send.data.a.d.b.ordinal()] = 2;
      b[co.uk.getmondo.payments.send.data.a.d.c.ordinal()] = 3;
   }
}
