package co.uk.getmondo.terms_and_conditions;

import android.webkit.ValueCallback;
import io.reactivex.o;

// $FF: synthetic class
final class e implements ValueCallback {
   private final TermsAndConditionsActivity a;
   private final o b;

   private e(TermsAndConditionsActivity var1, o var2) {
      this.a = var1;
      this.b = var2;
   }

   public static ValueCallback a(TermsAndConditionsActivity var0, o var1) {
      return new e(var0, var1);
   }

   public void onReceiveValue(Object var1) {
      TermsAndConditionsActivity.a(this.a, this.b, (String)var1);
   }
}
