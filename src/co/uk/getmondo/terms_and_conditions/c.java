package co.uk.getmondo.terms_and_conditions;

import android.webkit.ValueCallback;

// $FF: synthetic class
final class c implements ValueCallback {
   private final TermsAndConditionsActivity a;

   private c(TermsAndConditionsActivity var1) {
      this.a = var1;
   }

   public static ValueCallback a(TermsAndConditionsActivity var0) {
      return new c(var0);
   }

   public void onReceiveValue(Object var1) {
      TermsAndConditionsActivity.b(this.a, (String)var1);
   }
}
