package co.uk.getmondo.terms_and_conditions;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.n;
import io.reactivex.o;
import io.reactivex.r;

public class TermsAndConditionsActivity extends co.uk.getmondo.common.activities.b implements g.a {
   g a;
   @BindView(2131821116)
   Button nextPageButton;
   @BindView(2131821115)
   WebView termsWebView;

   public static Intent a(Context var0) {
      return new Intent(var0, TermsAndConditionsActivity.class);
   }

   public static Intent a(Context var0, String var1) {
      Intent var2 = new Intent(var0, TermsAndConditionsActivity.class);
      var2.putExtra("EXTRA_OVERRIDE_URL", var1);
      return var2;
   }

   // $FF: synthetic method
   static r a(TermsAndConditionsActivity var0, Object var1) throws Exception {
      return n.create(d.a(var0));
   }

   // $FF: synthetic method
   static void a(TermsAndConditionsActivity var0, o var1) throws Exception {
      var0.termsWebView.evaluateJavascript("window.isLastArticle()", e.a(var0, var1));
   }

   // $FF: synthetic method
   static void a(TermsAndConditionsActivity var0, o var1, String var2) {
      var1.a(Boolean.valueOf(var0.a(var2)));
   }

   // $FF: synthetic method
   static void a(TermsAndConditionsActivity var0, String var1) {
      var0.termsWebView.evaluateJavascript("window.isLastArticle()", c.a(var0));
   }

   private boolean a(String var1) {
      return var1.equals("true");
   }

   // $FF: synthetic method
   static void b(TermsAndConditionsActivity var0, String var1) {
      if(var0.a(var1)) {
         var0.nextPageButton.setText(2131362761);
      }

   }

   public n a() {
      return com.b.a.c.c.a(this.nextPageButton).flatMap(a.a(this));
   }

   public void b() {
      this.termsWebView.evaluateJavascript("window.nextArticle()", b.a(this));
   }

   @SuppressLint({"SetJavaScriptEnabled"})
   protected void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.setContentView(2131034216);
      ButterKnife.bind((Activity)this);
      this.l().a(this);
      this.getSupportActionBar().b(false);
      this.termsWebView.getSettings().setJavaScriptEnabled(true);
      this.termsWebView.setWebViewClient(new WebViewClient() {
         public boolean shouldOverrideUrlLoading(WebView var1, String var2) {
            Uri var3 = Uri.parse(var2);
            (new android.support.b.a.a()).a(android.support.v4.content.a.c(TermsAndConditionsActivity.this, 2131689487)).a().a(TermsAndConditionsActivity.this, var3);
            return true;
         }
      });
      String var2 = "https://monzo.com/-webviews/terms-and-conditions/";
      if(this.getIntent().hasExtra("EXTRA_OVERRIDE_URL")) {
         var2 = this.getIntent().getStringExtra("EXTRA_OVERRIDE_URL");
      }

      this.termsWebView.loadUrl(var2);
      this.a.a((g.a)this);
   }

   protected void onDestroy() {
      this.a.b();
      super.onDestroy();
   }
}
