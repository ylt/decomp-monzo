package co.uk.getmondo.terms_and_conditions;

public final class j implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final b.a b;

   static {
      boolean var0;
      if(!j.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   public j(b.a var1) {
      if(!a && var1 == null) {
         throw new AssertionError();
      } else {
         this.b = var1;
      }
   }

   public static b.a.b a(b.a var0) {
      return new j(var0);
   }

   public g a() {
      return (g)b.a.c.a(this.b, new g());
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
