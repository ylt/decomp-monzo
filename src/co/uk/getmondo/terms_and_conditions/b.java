package co.uk.getmondo.terms_and_conditions;

import android.webkit.ValueCallback;

// $FF: synthetic class
final class b implements ValueCallback {
   private final TermsAndConditionsActivity a;

   private b(TermsAndConditionsActivity var1) {
      this.a = var1;
   }

   public static ValueCallback a(TermsAndConditionsActivity var0) {
      return new b(var0);
   }

   public void onReceiveValue(Object var1) {
      TermsAndConditionsActivity.a(this.a, (String)var1);
   }
}
