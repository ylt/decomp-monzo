package co.uk.getmondo.terms_and_conditions;

import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class TermsAndConditionsActivity_ViewBinding implements Unbinder {
   private TermsAndConditionsActivity a;

   public TermsAndConditionsActivity_ViewBinding(TermsAndConditionsActivity var1, View var2) {
      this.a = var1;
      var1.termsWebView = (WebView)Utils.findRequiredViewAsType(var2, 2131821115, "field 'termsWebView'", WebView.class);
      var1.nextPageButton = (Button)Utils.findRequiredViewAsType(var2, 2131821116, "field 'nextPageButton'", Button.class);
   }

   public void unbind() {
      TermsAndConditionsActivity var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.termsWebView = null;
         var1.nextPageButton = null;
      }
   }
}
