package co.uk.getmondo.pots;

public final class g implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final b.a b;

   static {
      boolean var0;
      if(!g.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   public g(b.a var1) {
      if(!a && var1 == null) {
         throw new AssertionError();
      } else {
         this.b = var1;
      }
   }

   public static b.a.b a(b.a var0) {
      return new g(var0);
   }

   public f a() {
      return (f)b.a.c.a(this.b, new f());
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
