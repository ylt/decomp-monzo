package co.uk.getmondo;

import android.app.Activity;
import android.app.Application;
import android.app.Service;
import android.content.Context;
import co.uk.getmondo.common.f;
import co.uk.getmondo.common.g;
import co.uk.getmondo.common.m;
import co.uk.getmondo.common.q;
import co.uk.getmondo.common.accounts.d;
import co.uk.getmondo.common.h.a.o;
import co.uk.getmondo.common.k.p;
import co.uk.getmondo.d.ak;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.h;
import io.realm.av;
import io.realm.ay;

public class MonzoApplication extends android.support.e.b {
   d a;
   co.uk.getmondo.b.a b;
   co.uk.getmondo.common.a.c c;
   q d;
   m e;
   g f;
   co.uk.getmondo.adjust.a g;
   private final co.uk.getmondo.common.h.a.a h = this.a();

   public static MonzoApplication a(Context var0) {
      Application var1;
      if(var0 instanceof Activity) {
         var1 = ((Activity)var0).getApplication();
      } else if(var0 instanceof Service) {
         var1 = ((Service)var0).getApplication();
      } else {
         var1 = (Application)var0.getApplicationContext();
      }

      return (MonzoApplication)var1;
   }

   protected co.uk.getmondo.common.h.a.a a() {
      return o.Q().a(new co.uk.getmondo.common.h.a.b(this)).a();
   }

   public co.uk.getmondo.common.h.a.a b() {
      return this.h;
   }

   public void onCreate() {
      super.onCreate();
      this.b().a(this);
      io.fabric.sdk.android.c.a((Context)this, (h[])(new h[]{new Crashlytics()}));
      com.squareup.a.a.a(this);
      com.b.c.a.a((Application)this);
      av.a((Context)this);
      d.a.a.a((d.a.a.b)(new f()));
      ak var1 = this.a.b();
      if(var1 != null) {
         String var2;
         if(var1.d() != null) {
            var2 = var1.d().b();
         } else {
            var2 = var1.b();
         }

         if(p.c(var2)) {
            this.f.a(var2);
            this.d.a(var2);
         }
      }

      this.b.a(this);
      this.registerActivityLifecycleCallbacks(this.b().i());
      this.g.a();
      av.b((new ay.a()).a().b());
      this.c.a();
      android.support.text.emoji.a.a(new android.support.text.emoji.a.a(this.getApplicationContext()));
   }

   public void onTerminate() {
      this.unregisterActivityLifecycleCallbacks(this.b().i());
      super.onTerminate();
   }
}
