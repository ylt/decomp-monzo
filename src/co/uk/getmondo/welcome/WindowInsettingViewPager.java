package co.uk.getmondo.welcome;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.View;
import android.view.WindowInsets;
import android.view.View.OnApplyWindowInsetsListener;
import android.view.ViewGroup.LayoutParams;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\u0018\u00002\u00020\u0001:\u0001\u0017B\u001b\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0006J\u0012\u0010\f\u001a\u00020\r2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000fH\u0014J\b\u0010\u0010\u001a\u00020\u000fH\u0014J\u0012\u0010\u0011\u001a\u00020\u000f2\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005H\u0016J\u0012\u0010\u0011\u001a\u00020\u000f2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000fH\u0014J\u0018\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0015H\u0014R\"\u0010\t\u001a\u0004\u0018\u00010\b2\b\u0010\u0007\u001a\u0004\u0018\u00010\b@BX\u0082\u000e¢\u0006\b\n\u0000\"\u0004\b\n\u0010\u000b¨\u0006\u0018"},
   d2 = {"Lco/uk/getmondo/welcome/WindowInsettingViewPager;", "Landroid/support/v4/view/ViewPager;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "value", "Landroid/view/WindowInsets;", "lastInsets", "setLastInsets", "(Landroid/view/WindowInsets;)V", "checkLayoutParams", "", "p", "Landroid/view/ViewGroup$LayoutParams;", "generateDefaultLayoutParams", "generateLayoutParams", "onMeasure", "", "widthMeasureSpec", "", "heightMeasureSpec", "LayoutParams", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class WindowInsettingViewPager extends ViewPager {
   private WindowInsets d;

   public WindowInsettingViewPager(Context var1) {
      this(var1, (AttributeSet)null, 2, (kotlin.d.b.i)null);
   }

   public WindowInsettingViewPager(Context var1, AttributeSet var2) {
      l.b(var1, "context");
      super(var1, var2);
      this.setOnApplyWindowInsetsListener((OnApplyWindowInsetsListener)(new OnApplyWindowInsetsListener() {
         public final WindowInsets onApplyWindowInsets(View var1, WindowInsets var2) {
            WindowInsettingViewPager.this.setLastInsets(var2);
            return var2.consumeSystemWindowInsets();
         }
      }));
   }

   // $FF: synthetic method
   public WindowInsettingViewPager(Context var1, AttributeSet var2, int var3, kotlin.d.b.i var4) {
      if((var3 & 2) != 0) {
         var2 = (AttributeSet)null;
      }

      this(var1, var2);
   }

   private final void setLastInsets(WindowInsets var1) {
      this.d = var1;
      this.requestLayout();
   }

   protected boolean checkLayoutParams(LayoutParams var1) {
      boolean var2;
      if(var1 instanceof WindowInsettingViewPager.a && super.checkLayoutParams(var1)) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   protected LayoutParams generateDefaultLayoutParams() {
      return (LayoutParams)(new WindowInsettingViewPager.a());
   }

   public LayoutParams generateLayoutParams(AttributeSet var1) {
      return (LayoutParams)(new android.support.v4.view.ViewPager.c(this.getContext(), var1));
   }

   protected LayoutParams generateLayoutParams(LayoutParams var1) {
      return this.generateDefaultLayoutParams();
   }

   protected void onMeasure(int var1, int var2) {
      int var4 = this.getChildCount();

      for(int var3 = 0; var3 < var4; ++var3) {
         View var13 = this.getChildAt(var3);
         if(var13.getVisibility() != 8) {
            WindowInsets var14 = this.d;
            if(var14 != null && this.getFitsSystemWindows()) {
               if(var13.getFitsSystemWindows()) {
                  var13.dispatchApplyWindowInsets(this.d);
               } else {
                  LayoutParams var15 = var13.getLayoutParams();
                  if(var15 == null) {
                     throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.welcome.WindowInsettingViewPager.LayoutParams");
                  }

                  WindowInsettingViewPager.a var16 = (WindowInsettingViewPager.a)var15;
                  int var9 = var13.getPaddingLeft();
                  int var12 = var16.a();
                  int var5 = var13.getPaddingTop();
                  int var6 = var16.b();
                  int var7 = var13.getPaddingRight();
                  int var8 = var16.d();
                  int var11 = var13.getPaddingBottom();
                  int var10 = var16.c();
                  var16.a(var14.getSystemWindowInsetLeft());
                  var16.b(var14.getSystemWindowInsetTop());
                  var16.d(var14.getSystemWindowInsetRight());
                  var16.c(var14.getSystemWindowInsetBottom());
                  var13.setPadding(var16.a() + (var9 - var12), var16.b() + (var5 - var6), var16.d() + (var7 - var8), var16.c() + (var11 - var10));
               }
            }
         }
      }

      super.onMeasure(var1, var2);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u000e\u0018\u00002\u00020\u0001B\u0007\b\u0016¢\u0006\u0002\u0010\u0002B\u0017\b\u0016\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007R\u001a\u0010\b\u001a\u00020\tX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u000b\"\u0004\b\f\u0010\rR\u001a\u0010\u000e\u001a\u00020\tX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\u000b\"\u0004\b\u0010\u0010\rR\u001a\u0010\u0011\u001a\u00020\tX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u000b\"\u0004\b\u0013\u0010\rR\u001a\u0010\u0014\u001a\u00020\tX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0015\u0010\u000b\"\u0004\b\u0016\u0010\r¨\u0006\u0017"},
      d2 = {"Lco/uk/getmondo/welcome/WindowInsettingViewPager$LayoutParams;", "Landroid/support/v4/view/ViewPager$LayoutParams;", "()V", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "offsetBottom", "", "getOffsetBottom", "()I", "setOffsetBottom", "(I)V", "offsetLeft", "getOffsetLeft", "setOffsetLeft", "offsetRight", "getOffsetRight", "setOffsetRight", "offsetTop", "getOffsetTop", "setOffsetTop", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a extends android.support.v4.view.ViewPager.c {
      private int g;
      private int h;
      private int i;
      private int j;

      public final int a() {
         return this.g;
      }

      public final void a(int var1) {
         this.g = var1;
      }

      public final int b() {
         return this.h;
      }

      public final void b(int var1) {
         this.h = var1;
      }

      public final int c() {
         return this.i;
      }

      public final void c(int var1) {
         this.i = var1;
      }

      public final int d() {
         return this.j;
      }

      public final void d(int var1) {
         this.j = var1;
      }
   }
}
