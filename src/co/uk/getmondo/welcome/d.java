package co.uk.getmondo.welcome;

public final class d implements b.a {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;
   private final javax.a.a c;

   static {
      boolean var0;
      if(!d.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   public d(javax.a.a var1, javax.a.a var2) {
      if(!a && var1 == null) {
         throw new AssertionError();
      } else {
         this.b = var1;
         if(!a && var2 == null) {
            throw new AssertionError();
         } else {
            this.c = var2;
         }
      }
   }

   public static b.a a(javax.a.a var0, javax.a.a var1) {
      return new d(var0, var1);
   }

   public void a(WelcomeOnboardingActivity var1) {
      if(var1 == null) {
         throw new NullPointerException("Cannot inject members into a null reference");
      } else {
         var1.a = (e)this.b.b();
         var1.b = (co.uk.getmondo.common.pager.h)this.c.b();
      }
   }
}
