package co.uk.getmondo.welcome;

public final class i implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final b.a b;

   static {
      boolean var0;
      if(!i.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   public i(b.a var1) {
      if(!a && var1 == null) {
         throw new AssertionError();
      } else {
         this.b = var1;
      }
   }

   public static b.a.b a(b.a var0) {
      return new i(var0);
   }

   public e a() {
      return (e)b.a.c.a(this.b, new e());
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
