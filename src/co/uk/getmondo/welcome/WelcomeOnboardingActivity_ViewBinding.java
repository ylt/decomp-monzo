package co.uk.getmondo.welcome;

import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class WelcomeOnboardingActivity_ViewBinding implements Unbinder {
   private WelcomeOnboardingActivity a;

   public WelcomeOnboardingActivity_ViewBinding(WelcomeOnboardingActivity var1, View var2) {
      this.a = var1;
      var1.viewPager = (ViewPager)Utils.findRequiredViewAsType(var2, 2131820808, "field 'viewPager'", ViewPager.class);
      var1.mainActionButton = (Button)Utils.findRequiredViewAsType(var2, 2131820809, "field 'mainActionButton'", Button.class);
      var1.logInButton = (Button)Utils.findRequiredViewAsType(var2, 2131820810, "field 'logInButton'", Button.class);
   }

   public void unbind() {
      WelcomeOnboardingActivity var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.viewPager = null;
         var1.mainActionButton = null;
         var1.logInButton = null;
      }
   }
}
