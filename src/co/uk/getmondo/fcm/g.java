package co.uk.getmondo.fcm;

import co.uk.getmondo.settings.k;
import io.intercom.android.sdk.push.IntercomPushClient;

public final class g implements b.a {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;
   private final javax.a.a c;
   private final javax.a.a d;
   private final javax.a.a e;
   private final javax.a.a f;

   static {
      boolean var0;
      if(!g.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   public g(javax.a.a var1, javax.a.a var2, javax.a.a var3, javax.a.a var4, javax.a.a var5) {
      if(!a && var1 == null) {
         throw new AssertionError();
      } else {
         this.b = var1;
         if(!a && var2 == null) {
            throw new AssertionError();
         } else {
            this.c = var2;
            if(!a && var3 == null) {
               throw new AssertionError();
            } else {
               this.d = var3;
               if(!a && var4 == null) {
                  throw new AssertionError();
               } else {
                  this.e = var4;
                  if(!a && var5 == null) {
                     throw new AssertionError();
                  } else {
                     this.f = var5;
                  }
               }
            }
         }
      }
   }

   public static b.a a(javax.a.a var0, javax.a.a var1, javax.a.a var2, javax.a.a var3, javax.a.a var4) {
      return new g(var0, var1, var2, var3, var4);
   }

   public void a(PushNotificationService var1) {
      if(var1 == null) {
         throw new NullPointerException("Cannot inject members into a null reference");
      } else {
         var1.a = (com.google.gson.f)this.b.b();
         var1.b = (co.uk.getmondo.common.accounts.d)this.c.b();
         var1.c = (IntercomPushClient)this.d.b();
         var1.d = (k)this.e.b();
         var1.e = (co.uk.getmondo.common.a)this.f.b();
      }
   }
}
