package co.uk.getmondo.fcm;

import android.app.job.JobParameters;

// $FF: synthetic class
final class a implements io.reactivex.c.a {
   private final FcmRegistrationJobService a;
   private final JobParameters b;

   private a(FcmRegistrationJobService var1, JobParameters var2) {
      this.a = var1;
      this.b = var2;
   }

   public static io.reactivex.c.a a(FcmRegistrationJobService var0, JobParameters var1) {
      return new a(var0, var1);
   }

   public void a() {
      FcmRegistrationJobService.a(this.a, this.b);
   }
}
