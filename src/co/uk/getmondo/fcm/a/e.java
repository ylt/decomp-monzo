package co.uk.getmondo.fcm.a;

import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.net.Uri;
import co.uk.getmondo.api.model.NotificationMessage;
import co.uk.getmondo.d.ak;
import co.uk.getmondo.transaction.details.TransactionDetailsActivity;

public class e extends d {
   private static final Uri d = Uri.parse("android.resource://co.uk.getmondo/2131296264");
   private static final Uri e = Uri.parse("android.resource://co.uk.getmondo/2131296263");
   private final String f;
   private final String g;

   public e(NotificationMessage var1, String var2, String var3) {
      super(var2.hashCode(), var1);
      this.f = var2;
      this.g = var3;
   }

   public PendingIntent a(Context var1, ak.a var2) {
      PendingIntent var3;
      if(var2 != ak.a.HAS_ACCOUNT) {
         var3 = super.a(var1, var2);
      } else {
         var3 = TaskStackBuilder.create(var1).addNextIntentWithParentStack(TransactionDetailsActivity.a(var1, this.f)).getPendingIntent(this.b(), 134217728);
      }

      return var3;
   }

   public Uri a() {
      Uri var1;
      if(!this.g.equals("transaction_topup") && !this.g.equals("transaction_credit")) {
         if(!this.g.equals("transaction_decline")) {
            var1 = d;
         } else {
            var1 = a;
         }
      } else {
         var1 = e;
      }

      return var1;
   }
}
