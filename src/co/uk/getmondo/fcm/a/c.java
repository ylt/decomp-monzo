package co.uk.getmondo.fcm.a;

import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import co.uk.getmondo.api.model.NotificationMessage;
import co.uk.getmondo.api.model.signup.SignupSource;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.d.ak;
import co.uk.getmondo.signup.identity_verification.IdentityApprovedActivity;
import co.uk.getmondo.signup.identity_verification.IdentityVerificationActivity;
import co.uk.getmondo.signup.identity_verification.a.j;

public class c extends d {
   private final String d;

   public c(String var1, NotificationMessage var2, String var3) {
      super(var1.hashCode(), var2);
      this.d = var3;
   }

   public PendingIntent a(Context var1, ak.a var2) {
      PendingIntent var3;
      if(var2 != ak.a.HAS_ACCOUNT) {
         var3 = super.a(var1, var2);
      } else {
         Intent var4;
         if(this.d.equals("kyc_request")) {
            var4 = IdentityVerificationActivity.a(var1, j.a, Impression.KycFrom.FEED, SignupSource.LEGACY_PREPAID);
         } else {
            if(!this.d.equals("kyc_passed")) {
               throw new IllegalArgumentException("Unexpected KYC subtype found: " + this.d);
            }

            var4 = IdentityApprovedActivity.b(var1);
         }

         var3 = TaskStackBuilder.create(var1).addNextIntentWithParentStack(var4).getPendingIntent(this.b(), 134217728);
      }

      return var3;
   }
}
