package co.uk.getmondo.fcm.a;

import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import co.uk.getmondo.api.model.NotificationMessage;
import co.uk.getmondo.d.ak;

public class b extends d {
   private final String d;

   public b(int var1, NotificationMessage var2, String var3) {
      super(var1, var2);
      this.d = var3;
   }

   public PendingIntent a(Context var1, ak.a var2) {
      Intent var3 = new Intent();
      var3.setComponent(new ComponentName("co.uk.getmondo.ghost", "co.uk.getmondo.ghost.GhostLoginActivity"));
      var3.putExtra("KEY_EXTRA_ACCESS_TOKEN", this.d);
      PendingIntent var4;
      if(var3.resolveActivity(var1.getPackageManager()) == null) {
         var4 = super.a(var1, var2);
      } else {
         var4 = PendingIntent.getActivity(var1, this.b(), var3, 134217728);
      }

      return var4;
   }
}
