package co.uk.getmondo.fcm.a;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import co.uk.getmondo.api.model.NotificationMessage;
import co.uk.getmondo.d.ak;
import co.uk.getmondo.main.HomeActivity;
import co.uk.getmondo.splash.SplashActivity;
import java.io.Serializable;

public abstract class d implements Serializable {
   static final Uri a = RingtoneManager.getDefaultUri(2);
   protected final int b;
   protected final NotificationMessage c;

   d(int var1, NotificationMessage var2) {
      this.b = var1;
      this.c = var2;
   }

   public PendingIntent a(Context var1, ak.a var2) {
      Intent var3;
      if(var2 == ak.a.HAS_ACCOUNT) {
         var3 = HomeActivity.c(var1);
      } else {
         var3 = SplashActivity.b(var1);
      }

      return PendingIntent.getActivity(var1, this.b(), var3, 134217728);
   }

   public Uri a() {
      return a;
   }

   public int b() {
      return this.b;
   }

   public NotificationMessage c() {
      return this.c;
   }
}
