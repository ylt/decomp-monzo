package co.uk.getmondo.fcm;

import android.app.job.JobParameters;
import android.app.job.JobService;
import co.uk.getmondo.MonzoApplication;
import co.uk.getmondo.api.ApiException;
import co.uk.getmondo.api.MonzoApi;
import io.reactivex.u;
import java.io.IOException;

public class FcmRegistrationJobService extends JobService {
   u a;
   MonzoApi b;
   private io.reactivex.b.b c;

   // $FF: synthetic method
   static void a(FcmRegistrationJobService var0, JobParameters var1) throws Exception {
      d.a.a.a("Successfully registered for FCM messaging", new Object[0]);
      var0.jobFinished(var1, false);
   }

   // $FF: synthetic method
   static void a(FcmRegistrationJobService var0, JobParameters var1, Throwable var2) throws Exception {
      boolean var3;
      label21: {
         d.a.a.a((Throwable)(new RuntimeException("Failure in FCM registration", var2)));
         if(var2 instanceof IOException) {
            if(!(var2 instanceof ApiException)) {
               var3 = true;
               break label21;
            }

            if(((ApiException)var2).b() >= 500) {
               var3 = true;
               break label21;
            }
         }

         var3 = false;
      }

      if(var3) {
         d.a.a.a("Error with FCM registration, rescheduling", new Object[0]);
      } else {
         d.a.a.a((Throwable)(new RuntimeException("Giving up trying to register for FCM", var2)));
      }

      var0.jobFinished(var1, var3);
   }

   public void onCreate() {
      super.onCreate();
      MonzoApplication.a(this).b().a(this);
   }

   public void onDestroy() {
      if(this.c != null && !this.c.isDisposed()) {
         this.c.dispose();
      }

      super.onDestroy();
   }

   public boolean onStartJob(JobParameters var1) {
      String var2 = var1.getExtras().getString("KEY_TOKEN");
      this.c = this.b.registerForFcm(var2, "co.uk.getmondo").b(this.a).a(a.a(this, var1), b.a(this, var1));
      return true;
   }

   public boolean onStopJob(JobParameters var1) {
      if(this.c != null && !this.c.isDisposed()) {
         this.c.dispose();
      }

      return true;
   }
}
