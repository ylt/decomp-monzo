package co.uk.getmondo.fcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.os.Build.VERSION;
import co.uk.getmondo.MonzoApplication;
import co.uk.getmondo.api.model.NotificationMessage;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.background_sync.SyncFeedAndBalanceJobService;
import co.uk.getmondo.common.k.p;
import co.uk.getmondo.d.ak;
import co.uk.getmondo.settings.k;
import co.uk.getmondo.splash.SplashActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.gson.l;
import com.google.gson.n;
import io.intercom.android.sdk.push.IntercomPushClient;
import java.util.Map;

public class PushNotificationService extends FirebaseMessagingService {
   com.google.gson.f a;
   co.uk.getmondo.common.accounts.d b;
   IntercomPushClient c;
   k d;
   co.uk.getmondo.common.a e;
   private NotificationManager f;
   private int g;

   private co.uk.getmondo.fcm.a.d a(n var1, String var2, NotificationMessage var3) {
      byte var4 = -1;
      switch(var2.hashCode()) {
      case -1170057799:
         if(var2.equals("ghost_login")) {
            var4 = 2;
         }
         break;
      case 106677:
         if(var2.equals("kyc")) {
            var4 = 1;
         }
         break;
      case 2141246174:
         if(var2.equals("transaction")) {
            var4 = 0;
         }
      }

      Object var5;
      switch(var4) {
      case 0:
         var5 = new co.uk.getmondo.fcm.a.e(var3, var1.c("transaction_id").b(), var1.c("sub_type").b());
         break;
      case 1:
         var5 = new co.uk.getmondo.fcm.a.c(var1.c("feed_item_id").b(), var3, var1.c("sub_type").b());
         break;
      case 2:
         var5 = new co.uk.getmondo.fcm.a.b(2, var3, var1.c("access_token").b());
         break;
      default:
         int var6 = this.g;
         this.g = var6 + 1;
         var5 = new co.uk.getmondo.fcm.a.a(var6, var3);
      }

      return (co.uk.getmondo.fcm.a.d)var5;
   }

   private void a(co.uk.getmondo.fcm.a.d var1, ak.a var2) {
      if(this.d.a()) {
         NotificationMessage var4 = var1.c();
         android.support.v4.app.ab.c var7 = (new android.support.v4.app.ab.c(this)).a(2130837871).e(android.support.v4.content.a.c(this, 2131689526)).a(var1.a(this, var2)).a(true).a(var1.a());
         if(p.d(var4.a()) && p.d(var4.b())) {
            if(VERSION.SDK_INT < 24) {
               var7.a(this.getString(2131362012));
            }

            var7.b(var4.c()).a((new android.support.v4.app.ab.b()).a(var4.c()));
         } else if(p.d(var4.c())) {
            var7.a(var4.a()).b(var4.b()).a((new android.support.v4.app.ab.b()).a(var4.b()));
         } else {
            var7.a(var4.a()).b(var4.b()).a((new android.support.v4.app.ab.e()).a(var4.a()).b(var4.b()).b(var4.c()));
         }

         if(VERSION.SDK_INT >= 24) {
            var7.b("summary_group");
            this.f.notify(var1.b(), var7.a());
            int var3 = this.f.getActiveNotifications().length;
            if(var3 > 1) {
               PendingIntent var5 = PendingIntent.getActivity(this, 1, SplashActivity.b(this), 134217728);
               android.support.v4.app.ab.c var6 = (new android.support.v4.app.ab.c(this)).a(2130837871).b("summary_group").b(var3).a(true).a(var5).e(android.support.v4.content.a.c(this, 2131689526)).c(true);
               this.f.notify(1, var6.a());
            }
         } else {
            this.f.notify(var1.b(), var7.a());
         }
      }

   }

   private NotificationMessage b() {
      return new NotificationMessage("", "", this.getString(2131362500));
   }

   private NotificationMessage b(String param1) {
      // $FF: Couldn't be decompiled
   }

   private NotificationMessage c(String var1) {
      NotificationMessage var3;
      if(p.c(var1) && var1.contains("\n")) {
         String[] var2 = var1.split("\\r?\\n");
         if(var2.length > 1) {
            var3 = new NotificationMessage(d(var2[0]), d(var2[1]), "");
            return var3;
         }
      }

      var3 = new NotificationMessage("", "", var1);
      return var3;
   }

   private static String d(String var0) {
      String var1 = var0;
      if(var0.endsWith(".")) {
         var1 = var0.substring(0, var0.length() - 1);
      }

      return var1;
   }

   public void a(com.google.firebase.messaging.a var1) {
      d.a.a.b("Push notification received", new Object[0]);
      Map var3 = var1.a();
      if(this.c.isIntercomPush(var3)) {
         this.c.handlePush(this.getApplication(), var3);
      } else if(var3.containsKey("payload")) {
         n var4 = ((l)this.a.a((String)var3.get("payload"), l.class)).k();
         String var2 = var4.c("type").b();
         String var5;
         if(var4.c("sub_type") == null) {
            var5 = null;
         } else {
            var5 = var4.c("sub_type").b();
         }

         this.e.a(Impression.c(var2, var5));
         co.uk.getmondo.fcm.a.d var6 = this.a(var4, var2, this.b((String)var3.get("alert")));
         ak var7 = this.b.b();
         ak.a var8;
         if(var7 != null) {
            var8 = var7.a();
         } else {
            var8 = ak.a.NO_PROFILE;
         }

         if(var8 == ak.a.HAS_ACCOUNT) {
            SyncFeedAndBalanceJobService.a(this);
         }

         this.a(var6, var8);
      }

   }

   public void onCreate() {
      super.onCreate();
      MonzoApplication.a(this).b().a(this);
      this.f = (NotificationManager)this.getSystemService("notification");
   }
}
