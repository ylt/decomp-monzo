package co.uk.getmondo.fcm;

import android.app.job.JobScheduler;
import android.app.job.JobInfo.Builder;
import android.content.ComponentName;
import android.content.Context;
import android.os.PersistableBundle;
import co.uk.getmondo.MonzoApplication;
import co.uk.getmondo.common.k.p;
import com.google.firebase.iid.FirebaseInstanceId;
import io.intercom.android.sdk.push.IntercomPushClient;

public class e {
   private final Context a;
   private final IntercomPushClient b;

   e(Context var1, IntercomPushClient var2) {
      this.a = var1;
      this.b = var2;
   }

   public void a() {
      if(!co.uk.getmondo.a.b.booleanValue()) {
         String var3 = FirebaseInstanceId.a().c();
         if(p.c(var3)) {
            this.b.sendTokenToIntercom(MonzoApplication.a(this.a), var3);
            ComponentName var1 = new ComponentName(this.a, FcmRegistrationJobService.class);
            PersistableBundle var2 = new PersistableBundle();
            var2.putString("KEY_TOKEN", var3);
            ((JobScheduler)this.a.getSystemService("jobscheduler")).schedule((new Builder(0, var1)).setPersisted(true).setRequiredNetworkType(1).setExtras(var2).build());
         }
      }

   }
}
