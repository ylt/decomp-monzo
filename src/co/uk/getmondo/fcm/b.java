package co.uk.getmondo.fcm;

import android.app.job.JobParameters;

// $FF: synthetic class
final class b implements io.reactivex.c.g {
   private final FcmRegistrationJobService a;
   private final JobParameters b;

   private b(FcmRegistrationJobService var1, JobParameters var2) {
      this.a = var1;
      this.b = var2;
   }

   public static io.reactivex.c.g a(FcmRegistrationJobService var0, JobParameters var1) {
      return new b(var0, var1);
   }

   public void a(Object var1) {
      FcmRegistrationJobService.a(this.a, this.b, (Throwable)var1);
   }
}
