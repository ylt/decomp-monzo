package co.uk.getmondo.transaction.a;

import co.uk.getmondo.api.MonzoApi;
import co.uk.getmondo.api.ae;
import co.uk.getmondo.api.model.ApiAttachmentResponse;
import co.uk.getmondo.api.model.ApiTransactionResponse;
import co.uk.getmondo.api.model.identity_verification.ApiUploadContainer;
import co.uk.getmondo.d.aj;
import io.realm.av;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

public class a {
   private final MonzoApi a;
   private final j b;
   private final ae c;
   private final co.uk.getmondo.d.a.b d = new co.uk.getmondo.d.a.b();

   public a(MonzoApi var1, j var2, ae var3) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
   }

   // $FF: synthetic method
   static co.uk.getmondo.d.d a(a var0, ApiAttachmentResponse var1) throws Exception {
      return var0.d.a(var1.a());
   }

   // $FF: synthetic method
   static io.reactivex.d a(a var0, String var1, co.uk.getmondo.d.d var2) throws Exception {
      return var0.b.a(var2, var1);
   }

   // $FF: synthetic method
   static io.reactivex.d a(a var0, String var1, String var2, ApiTransactionResponse var3) throws Exception {
      return var0.b.a(var1, var2);
   }

   private io.reactivex.v a() {
      return io.reactivex.v.a(this.b.a(), this.b.b(), new a.c());
   }

   private io.reactivex.v a(co.uk.getmondo.d.aa var1) {
      return this.b.d(var1.a()).d((io.reactivex.c.h)(new a.b()));
   }

   private io.reactivex.v a(co.uk.getmondo.payments.send.data.a.a var1) {
      return this.b.b(var1.b(), var1.c()).d((io.reactivex.c.h)(new a.b()));
   }

   // $FF: synthetic method
   static io.reactivex.z a(a var0, InputStream var1, long var2, ApiUploadContainer var4) throws Exception {
      return var0.c.a(var4.b(), var1, var2, "image/jpeg").a((Object)var4);
   }

   // $FF: synthetic method
   static io.reactivex.z a(a var0, String var1, String var2, ApiUploadContainer var3) throws Exception {
      return var0.a.registerAttachment(var1, var2, var3.a());
   }

   // $FF: synthetic method
   static void a(String var0, co.uk.getmondo.d.h var1, av var2) {
      ((aj)var2.a(aj.class).a("id", var0).h()).a(var1.f());
   }

   // $FF: synthetic method
   static void b(String var0, co.uk.getmondo.d.h var1) throws Exception {
      av var3 = av.n();
      Object var2 = null;

      label126: {
         Throwable var19;
         try {
            var3.a(h.a(var0, var1));
            break label126;
         } catch (Throwable var17) {
            var19 = var17;
         } finally {
            ;
         }

         try {
            throw var19;
         } catch (Throwable var15) {
            if(var3 != null) {
               if(var19 != null) {
                  try {
                     var3.close();
                  } catch (Throwable var14) {
                     ;
                  }
               } else {
                  var3.close();
               }
            }

            throw var15;
         }
      }

      if(var3 != null) {
         if(false) {
            try {
               var3.close();
            } catch (Throwable var16) {
               ;
            }
         } else {
            var3.close();
         }
      }

   }

   private io.reactivex.v d(String var1) {
      return io.reactivex.v.a(this.b.e(var1), this.b.f(var1), new a.a());
   }

   private io.reactivex.v e(String var1) {
      return io.reactivex.v.a(this.b.g(var1), this.b.h(var1), new a.c());
   }

   public aj a(String var1) {
      return this.b.a(var1);
   }

   public io.reactivex.b a(String var1, co.uk.getmondo.d.h var2) {
      return this.a.updateTransactionCategory(var1, var2.f()).b((io.reactivex.d)io.reactivex.b.a(b.a(var1, var2)));
   }

   public io.reactivex.b a(String var1, InputStream var2, long var3, String var5) {
      return this.a.createAttachmentUploadUrl(var1, UUID.randomUUID().toString(), "image/jpeg").a(c.a(this, var2, var3)).a(d.a(this, var1, var5)).d(e.a(this)).c(f.a(this, var5));
   }

   public io.reactivex.b a(String var1, String var2) {
      return this.a.updateNotes(var2, var1).c(g.a(this, var2, var1));
   }

   public io.reactivex.v a(aj var1) {
      io.reactivex.v var2;
      if(var1.z()) {
         var2 = this.a().d((io.reactivex.c.h)(new a.d()));
      } else if(var1.r()) {
         var2 = this.a(var1.C());
      } else if(var1.u()) {
         var2 = this.d(var1.G());
      } else if(var1.q()) {
         var2 = this.a(var1.B());
      } else if(var1.f() != null) {
         var2 = this.e(var1.f().h()).d((io.reactivex.c.h)(new a.d()));
      } else {
         var2 = io.reactivex.v.a((Object)co.uk.getmondo.transaction.details.c.d.a);
      }

      return var2;
   }

   public io.reactivex.n b(String var1) {
      return this.b.b(var1);
   }

   public io.reactivex.b c(String var1) {
      return this.a.deregisterAttachment(var1).b((io.reactivex.d)this.b.c(var1));
   }

   private static class a implements io.reactivex.c.c {
      private a() {
      }

      // $FF: synthetic method
      a(Object var1) {
         this();
      }

      public co.uk.getmondo.transaction.details.c.a a(Long var1, Long var2) throws Exception {
         co.uk.getmondo.transaction.details.c.a var5;
         if(var1.longValue() == 0L) {
            var5 = new co.uk.getmondo.transaction.details.c.a(0L, new co.uk.getmondo.d.c(0L, co.uk.getmondo.common.i.c.a), new co.uk.getmondo.d.c(0L, co.uk.getmondo.common.i.c.a));
         } else {
            long var3 = var2.longValue() / var1.longValue();
            var5 = new co.uk.getmondo.transaction.details.c.a(var1.longValue(), new co.uk.getmondo.d.c(var3, co.uk.getmondo.common.i.c.a), new co.uk.getmondo.d.c(var2.longValue(), co.uk.getmondo.common.i.c.a));
         }

         return var5;
      }
   }

   private static class b implements io.reactivex.c.h {
      private b() {
      }

      // $FF: synthetic method
      b(Object var1) {
         this();
      }

      public co.uk.getmondo.transaction.details.c.b a(List var1) throws Exception {
         co.uk.getmondo.common.i.a var3 = new co.uk.getmondo.common.i.a(co.uk.getmondo.common.i.c.a);
         co.uk.getmondo.common.i.a var2 = new co.uk.getmondo.common.i.a(co.uk.getmondo.common.i.c.a);
         Iterator var5 = var1.iterator();

         while(var5.hasNext()) {
            aj var4 = (aj)var5.next();
            if(var4.g().a()) {
               var3.a(var4.g());
            } else {
               var2.a(var4.g());
            }
         }

         return new co.uk.getmondo.transaction.details.c.b((long)var3.b(), var3.a(), (long)var2.b(), var2.a());
      }
   }

   private static class c implements io.reactivex.c.c {
      private c() {
      }

      // $FF: synthetic method
      c(Object var1) {
         this();
      }

      public co.uk.getmondo.transaction.details.c.e a(Long var1, Long var2) throws Exception {
         co.uk.getmondo.transaction.details.c.e var5;
         if(var1.longValue() == 0L) {
            var5 = new co.uk.getmondo.transaction.details.c.e(new co.uk.getmondo.d.c(0L, co.uk.getmondo.common.i.c.a), new co.uk.getmondo.d.c(0L, co.uk.getmondo.common.i.c.a), 0L);
         } else {
            long var3 = var2.longValue() / var1.longValue();
            var5 = new co.uk.getmondo.transaction.details.c.e(new co.uk.getmondo.d.c(var2.longValue(), co.uk.getmondo.common.i.c.a), new co.uk.getmondo.d.c(var3, co.uk.getmondo.common.i.c.a), var1.longValue());
         }

         return var5;
      }
   }

   private static class d implements io.reactivex.c.h {
      private d() {
      }

      // $FF: synthetic method
      d(Object var1) {
         this();
      }

      public co.uk.getmondo.transaction.details.c.f a(co.uk.getmondo.transaction.details.c.e var1) throws Exception {
         return new co.uk.getmondo.transaction.details.c.a(var1.c(), var1.b(), var1.a());
      }
   }
}
