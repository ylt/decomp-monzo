package co.uk.getmondo.transaction.a;

import co.uk.getmondo.d.aj;
import io.realm.av;
import io.realm.bb;
import io.realm.bc;
import io.realm.bg;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import org.threeten.bp.DateTimeUtils;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.ZoneId;

public class j {
   // $FF: synthetic method
   static bg a(String var0, av var1) {
      return var1.a(aj.class).a("peer.userId", var0).g();
   }

   // $FF: synthetic method
   static bg a(String var0, String var1, av var2) {
      return var2.a(aj.class).b("bankDetails").a("bankDetails.sortCode", var0).a("bankDetails.accountNumber", var1).g();
   }

   // $FF: synthetic method
   static List a(Date param0, Date param1) throws Exception {
      // $FF: Couldn't be decompiled
   }

   // $FF: synthetic method
   static void a(co.uk.getmondo.d.d var0, String var1, av var2) {
      var0 = (co.uk.getmondo.d.d)var2.b((bb)var0);
      ((aj)var2.a(aj.class).a("id", var1).h()).A().a((bb)var0);
   }

   // $FF: synthetic method
   static void b(String var0, av var1) {
      co.uk.getmondo.d.d var2 = (co.uk.getmondo.d.d)var1.a(co.uk.getmondo.d.d.class).a("id", var0).h();
      if(bc.b(var2)) {
         bc.a(var2);
      }

   }

   // $FF: synthetic method
   static void b(String var0, String var1, av var2) {
      ((aj)var2.a(aj.class).a("id", var0).h()).b(var1);
   }

   // $FF: synthetic method
   static bg c(String var0, av var1) {
      return var1.a(aj.class).a("id", var0).g();
   }

   // $FF: synthetic method
   static aj d() throws Exception {
      // $FF: Couldn't be decompiled
   }

   // $FF: synthetic method
   static Long e() throws Exception {
      // $FF: Couldn't be decompiled
   }

   // $FF: synthetic method
   static Long f() throws Exception {
      // $FF: Couldn't be decompiled
   }

   // $FF: synthetic method
   static Long i(String param0) throws Exception {
      // $FF: Couldn't be decompiled
   }

   // $FF: synthetic method
   static Long j(String param0) throws Exception {
      // $FF: Couldn't be decompiled
   }

   // $FF: synthetic method
   static Long k(String param0) throws Exception {
      // $FF: Couldn't be decompiled
   }

   // $FF: synthetic method
   static Long l(String param0) throws Exception {
      // $FF: Couldn't be decompiled
   }

   aj a(String param1) {
      // $FF: Couldn't be decompiled
   }

   io.reactivex.b a(co.uk.getmondo.d.d var1, String var2) {
      return co.uk.getmondo.common.j.g.a(w.a(var1, var2));
   }

   io.reactivex.b a(String var1, String var2) {
      return co.uk.getmondo.common.j.g.a(y.a(var1, var2));
   }

   io.reactivex.v a() {
      return io.reactivex.v.c(o.a());
   }

   public io.reactivex.v a(LocalDateTime var1, LocalDateTime var2) {
      return io.reactivex.v.c(t.a(DateTimeUtils.a(var1.a(ZoneId.a()).j()), DateTimeUtils.a(var2.a(ZoneId.a()).j())));
   }

   public io.reactivex.n b(String var1) {
      return co.uk.getmondo.common.j.g.a(k.a(var1)).filter(u.a()).map(v.a());
   }

   io.reactivex.v b() {
      return io.reactivex.v.c(p.a());
   }

   io.reactivex.v b(String var1, String var2) {
      return co.uk.getmondo.common.j.g.a(z.a(var1, var2)).map(aa.a()).first(Collections.emptyList());
   }

   io.reactivex.b c(String var1) {
      return co.uk.getmondo.common.j.g.a(x.a(var1));
   }

   public io.reactivex.v c() {
      return io.reactivex.v.c(s.a());
   }

   io.reactivex.v d(String var1) {
      return co.uk.getmondo.common.j.g.a(ab.a(var1)).map(l.a()).first(Collections.emptyList());
   }

   io.reactivex.v e(String var1) {
      return io.reactivex.v.c(m.a(var1));
   }

   io.reactivex.v f(String var1) {
      return io.reactivex.v.c(n.a(var1));
   }

   io.reactivex.v g(String var1) {
      return io.reactivex.v.c(q.a(var1));
   }

   io.reactivex.v h(String var1) {
      return io.reactivex.v.c(r.a(var1));
   }
}
