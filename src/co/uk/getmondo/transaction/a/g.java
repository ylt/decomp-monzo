package co.uk.getmondo.transaction.a;

import co.uk.getmondo.api.model.ApiTransactionResponse;

// $FF: synthetic class
final class g implements io.reactivex.c.h {
   private final a a;
   private final String b;
   private final String c;

   private g(a var1, String var2, String var3) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
   }

   public static io.reactivex.c.h a(a var0, String var1, String var2) {
      return new g(var0, var1, var2);
   }

   public Object a(Object var1) {
      return a.a(this.a, this.b, this.c, (ApiTransactionResponse)var1);
   }
}
