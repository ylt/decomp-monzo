package co.uk.getmondo.transaction.a;

import co.uk.getmondo.api.MonzoApi;
import co.uk.getmondo.api.ae;

public final class i implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;
   private final javax.a.a c;
   private final javax.a.a d;

   static {
      boolean var0;
      if(!i.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   public i(javax.a.a var1, javax.a.a var2, javax.a.a var3) {
      if(!a && var1 == null) {
         throw new AssertionError();
      } else {
         this.b = var1;
         if(!a && var2 == null) {
            throw new AssertionError();
         } else {
            this.c = var2;
            if(!a && var3 == null) {
               throw new AssertionError();
            } else {
               this.d = var3;
            }
         }
      }
   }

   public static b.a.b a(javax.a.a var0, javax.a.a var1, javax.a.a var2) {
      return new i(var0, var1, var2);
   }

   public a a() {
      return new a((MonzoApi)this.b.b(), (j)this.c.b(), (ae)this.d.b());
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
