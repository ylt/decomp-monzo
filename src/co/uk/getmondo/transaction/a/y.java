package co.uk.getmondo.transaction.a;

import io.realm.av;

// $FF: synthetic class
final class y implements av.a {
   private final String a;
   private final String b;

   private y(String var1, String var2) {
      this.a = var1;
      this.b = var2;
   }

   public static av.a a(String var0, String var1) {
      return new y(var0, var1);
   }

   public void a(av var1) {
      j.b(this.a, this.b, var1);
   }
}
