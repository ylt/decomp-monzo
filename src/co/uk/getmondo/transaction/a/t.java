package co.uk.getmondo.transaction.a;

import java.util.Date;
import java.util.concurrent.Callable;

// $FF: synthetic class
final class t implements Callable {
   private final Date a;
   private final Date b;

   private t(Date var1, Date var2) {
      this.a = var1;
      this.b = var2;
   }

   public static Callable a(Date var0, Date var1) {
      return new t(var0, var1);
   }

   public Object call() {
      return j.a(this.a, this.b);
   }
}
