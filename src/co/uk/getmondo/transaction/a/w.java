package co.uk.getmondo.transaction.a;

import io.realm.av;

// $FF: synthetic class
final class w implements av.a {
   private final co.uk.getmondo.d.d a;
   private final String b;

   private w(co.uk.getmondo.d.d var1, String var2) {
      this.a = var1;
      this.b = var2;
   }

   public static av.a a(co.uk.getmondo.d.d var0, String var1) {
      return new w(var0, var1);
   }

   public void a(av var1) {
      j.a(this.a, this.b, var1);
   }
}
