package co.uk.getmondo.transaction.a;

import io.realm.av;

// $FF: synthetic class
final class z implements kotlin.d.a.b {
   private final String a;
   private final String b;

   private z(String var1, String var2) {
      this.a = var1;
      this.b = var2;
   }

   public static kotlin.d.a.b a(String var0, String var1) {
      return new z(var0, var1);
   }

   public Object a(Object var1) {
      return j.a(this.a, this.b, (av)var1);
   }
}
