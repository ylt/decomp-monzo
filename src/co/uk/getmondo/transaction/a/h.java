package co.uk.getmondo.transaction.a;

import io.realm.av;

// $FF: synthetic class
final class h implements av.a {
   private final String a;
   private final co.uk.getmondo.d.h b;

   private h(String var1, co.uk.getmondo.d.h var2) {
      this.a = var1;
      this.b = var2;
   }

   public static av.a a(String var0, co.uk.getmondo.d.h var1) {
      return new h(var0, var1);
   }

   public void a(av var1) {
      a.a(this.a, this.b, var1);
   }
}
