package co.uk.getmondo.transaction.splitting;

import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;

public class SplitBottomSheetFragment_ViewBinding implements Unbinder {
   private SplitBottomSheetFragment a;
   private View b;

   public SplitBottomSheetFragment_ViewBinding(final SplitBottomSheetFragment var1, View var2) {
      this.a = var1;
      var1.shareTextViews = (ViewGroup)Utils.findRequiredViewAsType(var2, 2131821202, "field 'shareTextViews'", ViewGroup.class);
      var2 = Utils.findRequiredView(var2, 2131821207, "method 'onCustomiseAmount'");
      this.b = var2;
      var2.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View var1x) {
            var1.onCustomiseAmount();
         }
      });
   }

   public void unbind() {
      SplitBottomSheetFragment var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.shareTextViews = null;
         this.b.setOnClickListener((OnClickListener)null);
         this.b = null;
      }
   }
}
