package co.uk.getmondo.transaction.splitting;

import android.app.Dialog;
import android.view.View;
import android.view.View.OnClickListener;

// $FF: synthetic class
final class a implements OnClickListener {
   private final SplitBottomSheetFragment a;
   private final Dialog b;
   private final int c;

   private a(SplitBottomSheetFragment var1, Dialog var2, int var3) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
   }

   public static OnClickListener a(SplitBottomSheetFragment var0, Dialog var1, int var2) {
      return new a(var0, var1, var2);
   }

   public void onClick(View var1) {
      SplitBottomSheetFragment.a(this.a, this.b, this.c, var1);
   }
}
