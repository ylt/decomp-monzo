package co.uk.getmondo.transaction.splitting;

import android.app.Dialog;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.design.widget.d;
import android.support.v7.app.e;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class SplitBottomSheetFragment extends d {
   private static final String a = SplitBottomSheetFragment.class.getSimpleName();
   private Unbinder b;
   private co.uk.getmondo.transaction.splitting.a.a c;
   private SplitBottomSheetFragment.a d;
   @BindView(2131821202)
   ViewGroup shareTextViews;

   public static SplitBottomSheetFragment a(co.uk.getmondo.transaction.splitting.a.a var0) {
      Bundle var1 = new Bundle();
      var1.putParcelable("view_model", var0);
      SplitBottomSheetFragment var2 = new SplitBottomSheetFragment();
      var2.setArguments(var1);
      return var2;
   }

   public static void a(e var0, co.uk.getmondo.transaction.splitting.a.a var1, SplitBottomSheetFragment.a var2) {
      SplitBottomSheetFragment var3 = a(var1);
      var3.a(var2);
      var3.show(var0.getSupportFragmentManager(), a);
   }

   // $FF: synthetic method
   static void a(SplitBottomSheetFragment var0, Dialog var1, int var2, View var3) {
      if(var0.d != null) {
         var0.d.a(var1, var2);
      }

   }

   public void a(SplitBottomSheetFragment.a var1) {
      this.d = var1;
   }

   public void onCreate(Bundle var1) {
      super.onCreate(var1);
      var1 = this.getArguments();
      if(var1 != null && var1.containsKey("view_model")) {
         this.c = (co.uk.getmondo.transaction.splitting.a.a)var1.getParcelable("view_model");
      }

   }

   @OnClick({2131821207})
   public void onCustomiseAmount() {
      if(this.d != null) {
         this.d.a(this.getDialog());
      }

   }

   public void onDestroyView() {
      super.onDestroyView();
      this.b.unbind();
   }

   public void setupDialog(Dialog var1, int var2) {
      super.setupDialog(var1, var2);
      View var5 = View.inflate(this.getContext(), 2131034237, (ViewGroup)null);
      var1.setContentView(var5);
      this.b = ButterKnife.bind(this, (View)var5);
      if(this.c != null) {
         Resources var8 = this.getResources();
         co.uk.getmondo.transaction.splitting.a.a.c[] var7 = this.c.a();

         for(var2 = 0; var2 < this.shareTextViews.getChildCount() && var2 < var7.length; ++var2) {
            TextView var6 = (TextView)this.shareTextViews.getChildAt(var2);
            co.uk.getmondo.transaction.splitting.a.a.c var9 = var7[var2];
            String var10;
            if(var9.c() == -1) {
               var10 = this.getString(2131362707, new Object[]{var9.d()});
            } else if(var9.c() == 1) {
               var10 = this.getString(2131362703, new Object[]{var9.d()});
            } else {
               var10 = this.getString(2131362699, new Object[]{var9.b()});
            }

            int var3 = var9.a();
            var6.setText(var8.getQuantityString(2131886088, var3, new Object[]{Integer.valueOf(var3)}) + " " + var10);
            boolean var4;
            if(var9.c() == 0) {
               var4 = true;
            } else {
               var4 = false;
            }

            var6.setEnabled(var4);
            var6.setOnClickListener(a.a(this, var1, var3));
         }
      }

   }

   public interface a {
      void a(Dialog var1);

      void a(Dialog var1, int var2);
   }
}
