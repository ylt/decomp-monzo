package co.uk.getmondo.transaction.splitting.a;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.util.Arrays;
import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\b\u0086\b\u0018\u0000 \u001c2\u00020\u0001:\u0002\u001c\u001dB\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004B\u0013\u0012\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006¢\u0006\u0002\u0010\bJ\u0014\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006HÆ\u0003¢\u0006\u0002\u0010\nJ\u001e\u0010\r\u001a\u00020\u00002\u000e\b\u0002\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006HÆ\u0001¢\u0006\u0002\u0010\u000eJ\b\u0010\u000f\u001a\u00020\u0010H\u0016J\u0013\u0010\u0011\u001a\u00020\u00122\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014HÖ\u0003J\t\u0010\u0015\u001a\u00020\u0010HÖ\u0001J\t\u0010\u0016\u001a\u00020\u0017HÖ\u0001J\u0018\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u00032\u0006\u0010\u001b\u001a\u00020\u0010H\u0016R\u0019\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006¢\u0006\n\n\u0002\u0010\u000b\u001a\u0004\b\t\u0010\n¨\u0006\u001e"},
   d2 = {"Lco/uk/getmondo/transaction/splitting/model/Split;", "Landroid/os/Parcelable;", "source", "Landroid/os/Parcel;", "(Landroid/os/Parcel;)V", "shares", "", "Lco/uk/getmondo/transaction/splitting/model/Split$Share;", "([Lco/uk/getmondo/transaction/splitting/model/Split$Share;)V", "getShares", "()[Lco/uk/getmondo/transaction/splitting/model/Split$Share;", "[Lco/uk/getmondo/transaction/splitting/model/Split$Share;", "component1", "copy", "([Lco/uk/getmondo/transaction/splitting/model/Split$Share;)Lco/uk/getmondo/transaction/splitting/model/Split;", "describeContents", "", "equals", "", "other", "", "hashCode", "toString", "", "writeToParcel", "", "dest", "flags", "Companion", "Share", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class a implements Parcelable {
   public static final Creator CREATOR = (Creator)(new Creator() {
      public a a(Parcel var1) {
         l.b(var1, "source");
         return new a(var1);
      }

      public a[] a(int var1) {
         return new a[var1];
      }

      // $FF: synthetic method
      public Object createFromParcel(Parcel var1) {
         return this.a(var1);
      }

      // $FF: synthetic method
      public Object[] newArray(int var1) {
         return (Object[])this.a(var1);
      }
   });
   public static final a.a a = new a.a((i)null);
   private final a.c[] b;

   public a(Parcel var1) {
      l.b(var1, "source");
      Object[] var2 = var1.createTypedArray(a.c.CREATOR);
      l.a(var2, "source.createTypedArray(Share.CREATOR)");
      this((a.c[])var2);
   }

   public a(a.c[] var1) {
      l.b(var1, "shares");
      super();
      this.b = var1;
   }

   public final a.c[] a() {
      return this.b;
   }

   public int describeContents() {
      return 0;
   }

   public boolean equals(Object var1) {
      boolean var2;
      if(this != var1) {
         label26: {
            if(var1 instanceof a) {
               a var3 = (a)var1;
               if(l.a(this.b, var3.b)) {
                  break label26;
               }
            }

            var2 = false;
            return var2;
         }
      }

      var2 = true;
      return var2;
   }

   public int hashCode() {
      a.c[] var2 = this.b;
      int var1;
      if(var2 != null) {
         var1 = Arrays.hashCode(var2);
      } else {
         var1 = 0;
      }

      return var1;
   }

   public String toString() {
      return "Split(shares=" + Arrays.toString(this.b) + ")";
   }

   public void writeToParcel(Parcel var1, int var2) {
      l.b(var1, "dest");
      var1.writeTypedArray((Parcelable[])this.b, var2);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"},
      d2 = {"Lco/uk/getmondo/transaction/splitting/model/Split$Companion;", "", "()V", "CREATOR", "Landroid/os/Parcelable$Creator;", "Lco/uk/getmondo/transaction/splitting/model/Split;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(i var1) {
         this();
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0010\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0004\b\u0086\b\u0018\u0000 \"2\u00020\u0001:\u0001\"B\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004B%\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\u0006\u0010\t\u001a\u00020\u0006\u0012\u0006\u0010\n\u001a\u00020\b¢\u0006\u0002\u0010\u000bJ\t\u0010\u0012\u001a\u00020\u0006HÆ\u0003J\t\u0010\u0013\u001a\u00020\bHÆ\u0003J\t\u0010\u0014\u001a\u00020\u0006HÆ\u0003J\t\u0010\u0015\u001a\u00020\bHÆ\u0003J1\u0010\u0016\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u00062\b\b\u0002\u0010\u0007\u001a\u00020\b2\b\b\u0002\u0010\t\u001a\u00020\u00062\b\b\u0002\u0010\n\u001a\u00020\bHÆ\u0001J\b\u0010\u0017\u001a\u00020\u0006H\u0016J\u0013\u0010\u0018\u001a\u00020\u00192\b\u0010\u001a\u001a\u0004\u0018\u00010\u001bHÖ\u0003J\t\u0010\u001c\u001a\u00020\u0006HÖ\u0001J\t\u0010\u001d\u001a\u00020\bHÖ\u0001J\u0018\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020\u00032\u0006\u0010!\u001a\u00020\u0006H\u0016R\u0011\u0010\u0007\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0011\u0010\t\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0011\u0010\n\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\rR\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u000f¨\u0006#"},
      d2 = {"Lco/uk/getmondo/transaction/splitting/model/Split$Share;", "Landroid/os/Parcelable;", "source", "Landroid/os/Parcel;", "(Landroid/os/Parcel;)V", "number", "", "each", "", "limit", "limitValue", "(ILjava/lang/String;ILjava/lang/String;)V", "getEach", "()Ljava/lang/String;", "getLimit", "()I", "getLimitValue", "getNumber", "component1", "component2", "component3", "component4", "copy", "describeContents", "equals", "", "other", "", "hashCode", "toString", "writeToParcel", "", "dest", "flags", "Companion", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class c implements Parcelable {
      public static final Creator CREATOR = (Creator)(new Creator() {
         public a.c a(Parcel var1) {
            l.b(var1, "source");
            return new a.c(var1);
         }

         public a.c[] a(int var1) {
            return new a.c[var1];
         }

         // $FF: synthetic method
         public Object createFromParcel(Parcel var1) {
            return this.a(var1);
         }

         // $FF: synthetic method
         public Object[] newArray(int var1) {
            return (Object[])this.a(var1);
         }
      });
      public static final int a = 0;
      public static final int b = -1;
      public static final int c = 1;
      public static final a.a d = new a.a((i)null);
      private final int e;
      private final String f;
      private final int g;
      private final String h;

      public c(int var1, String var2, int var3, String var4) {
         l.b(var2, "each");
         l.b(var4, "limitValue");
         super();
         this.e = var1;
         this.f = var2;
         this.g = var3;
         this.h = var4;
      }

      public c(Parcel var1) {
         l.b(var1, "source");
         int var2 = var1.readInt();
         String var4 = var1.readString();
         l.a(var4, "source.readString()");
         int var3 = var1.readInt();
         String var5 = var1.readString();
         l.a(var5, "source.readString()");
         this(var2, var4, var3, var5);
      }

      public final int a() {
         return this.e;
      }

      public final String b() {
         return this.f;
      }

      public final int c() {
         return this.g;
      }

      public final String d() {
         return this.h;
      }

      public int describeContents() {
         return 0;
      }

      public boolean equals(Object var1) {
         boolean var4 = false;
         boolean var3;
         if(this != var1) {
            var3 = var4;
            if(!(var1 instanceof a.c)) {
               return var3;
            }

            a.c var5 = (a.c)var1;
            boolean var2;
            if(this.e == var5.e) {
               var2 = true;
            } else {
               var2 = false;
            }

            var3 = var4;
            if(!var2) {
               return var3;
            }

            var3 = var4;
            if(!l.a(this.f, var5.f)) {
               return var3;
            }

            if(this.g == var5.g) {
               var2 = true;
            } else {
               var2 = false;
            }

            var3 = var4;
            if(!var2) {
               return var3;
            }

            var3 = var4;
            if(!l.a(this.h, var5.h)) {
               return var3;
            }
         }

         var3 = true;
         return var3;
      }

      public int hashCode() {
         int var2 = 0;
         int var3 = this.e;
         String var5 = this.f;
         int var1;
         if(var5 != null) {
            var1 = var5.hashCode();
         } else {
            var1 = 0;
         }

         int var4 = this.g;
         var5 = this.h;
         if(var5 != null) {
            var2 = var5.hashCode();
         }

         return ((var1 + var3 * 31) * 31 + var4) * 31 + var2;
      }

      public String toString() {
         return "Share(number=" + this.e + ", each=" + this.f + ", limit=" + this.g + ", limitValue=" + this.h + ")";
      }

      public void writeToParcel(Parcel var1, int var2) {
         l.b(var1, "dest");
         var1.writeInt(this.e);
         var1.writeString(this.f);
         var1.writeInt(this.g);
         var1.writeString(this.h);
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u00020\u00078\u0006X\u0087D¢\u0006\u0002\n\u0000R\u0010\u0010\b\u001a\u00020\u00078\u0006X\u0087D¢\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u00020\u00078\u0006X\u0087D¢\u0006\u0002\n\u0000¨\u0006\n"},
      d2 = {"Lco/uk/getmondo/transaction/splitting/model/Split$Share$Companion;", "", "()V", "CREATOR", "Landroid/os/Parcelable$Creator;", "Lco/uk/getmondo/transaction/splitting/model/Split$Share;", "OVER_LIMIT", "", "UNDER_LIMIT", "WITHIN_LIMIT", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(i var1) {
         this();
      }
   }
}
