package co.uk.getmondo.transaction.attachment;

import android.view.View;
import android.view.View.OnClickListener;

// $FF: synthetic class
final class w implements OnClickListener {
   private final u a;
   private final String b;
   private final int c;

   private w(u var1, String var2, int var3) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
   }

   public static OnClickListener a(u var0, String var1, int var2) {
      return new w(var0, var1, var2);
   }

   public void onClick(View var1) {
      u.a(this.a, this.b, this.c, var1);
   }
}
