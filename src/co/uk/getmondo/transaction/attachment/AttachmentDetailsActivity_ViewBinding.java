package co.uk.getmondo.transaction.attachment;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class AttachmentDetailsActivity_ViewBinding implements Unbinder {
   private AttachmentDetailsActivity a;

   public AttachmentDetailsActivity_ViewBinding(AttachmentDetailsActivity var1, View var2) {
      this.a = var1;
      var1.zoomableImageView = (ImageView)Utils.findRequiredViewAsType(var2, 2131820813, "field 'zoomableImageView'", ImageView.class);
      var1.deleteButton = (Button)Utils.findRequiredViewAsType(var2, 2131820815, "field 'deleteButton'", Button.class);
      var1.progress = (ProgressBar)Utils.findRequiredViewAsType(var2, 2131820814, "field 'progress'", ProgressBar.class);
   }

   public void unbind() {
      AttachmentDetailsActivity var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.zoomableImageView = null;
         var1.deleteButton = null;
         var1.progress = null;
      }
   }
}
