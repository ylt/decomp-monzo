package co.uk.getmondo.transaction.attachment;

class e extends co.uk.getmondo.common.ui.b {
   private final io.reactivex.u c;
   private final io.reactivex.u d;
   private final co.uk.getmondo.common.e.a e;
   private final co.uk.getmondo.transaction.a.a f;

   e(io.reactivex.u var1, io.reactivex.u var2, co.uk.getmondo.common.e.a var3, co.uk.getmondo.transaction.a.a var4) {
      this.c = var1;
      this.d = var2;
      this.e = var3;
      this.f = var4;
   }

   // $FF: synthetic method
   static io.reactivex.d a(e var0, e.a var1, String var2) throws Exception {
      io.reactivex.b var3 = var0.f.c(var2).b(var0.d).a(var0.c).c(h.a(var1)).b(i.a(var1));
      var1.getClass();
      return var3.b(j.a(var1)).a(k.a(var0, var1)).b();
   }

   // $FF: synthetic method
   static void a(e.a var0, io.reactivex.b.b var1) throws Exception {
      var0.b();
   }

   // $FF: synthetic method
   static void a(e.a var0, Throwable var1) throws Exception {
      var0.c();
   }

   // $FF: synthetic method
   static void a(e var0, e.a var1, Throwable var2) throws Exception {
      var0.e.a(var2, var1);
   }

   public void a(e.a var1) {
      super.a((co.uk.getmondo.common.ui.f)var1);
      this.a((io.reactivex.b.b)var1.a().flatMapCompletable(f.a(this, var1)).a(co.uk.getmondo.common.j.a.a(), g.a()));
   }

   interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      io.reactivex.n a();

      void b();

      void c();

      void finish();
   }
}
