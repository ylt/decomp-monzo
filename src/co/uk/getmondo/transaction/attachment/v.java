package co.uk.getmondo.transaction.attachment;

import android.view.View;
import android.view.View.OnClickListener;

// $FF: synthetic class
final class v implements OnClickListener {
   private final u a;

   private v(u var1) {
      this.a = var1;
   }

   public static OnClickListener a(u var0) {
      return new v(var0);
   }

   public void onClick(View var1) {
      u.a(this.a, var1);
   }
}
