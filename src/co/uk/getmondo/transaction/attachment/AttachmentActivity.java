package co.uk.getmondo.transaction.attachment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import java.io.File;
import java.util.List;
import me.relex.circleindicator.CircleIndicator;

public class AttachmentActivity extends co.uk.getmondo.common.activities.b implements OnClickListener, m.a {
   m a;
   private u b;
   private final Handler c = new Handler();
   private final Runnable e = a.a(this);
   @BindView(2131820803)
   ViewPager imagePager;
   @BindView(2131820802)
   EditText notesInput;
   @BindView(2131820798)
   Toolbar toolbar;
   @BindView(2131820804)
   CircleIndicator viewpagerIndicator;

   public static void a(Context var0, String var1) {
      Intent var2 = new Intent(var0, AttachmentActivity.class);
      var2.putExtra("EXTRA_TRANSACTION_ID", var1);
      var0.startActivity(var2);
   }

   private void b(File var1) {
      Intent var2 = co.uk.getmondo.common.k.a((Context)this, (File)var1);
      if(var2 != null) {
         this.startActivityForResult(var2, 1);
      }

   }

   private boolean f() {
      boolean var1;
      if(android.support.v4.content.a.b(this, "android.permission.CAMERA") == 0) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   private void g() {
      this.startActivityForResult(co.uk.getmondo.common.k.a(), 2);
   }

   public void a() {
      this.g();
   }

   public void a(File var1) {
      if(this.f()) {
         this.b(var1);
      } else {
         android.support.v4.app.a.a(this, new String[]{"android.permission.CAMERA"}, 3);
      }

   }

   public void a(String var1) {
      this.toolbar.setTitle(var1);
   }

   public void a(List var1) {
      this.b.a(var1);
   }

   public void b() {
      this.c(this.getString(2131362020));
   }

   public void c() {
      this.c.postDelayed(this.e, 300L);
   }

   public void d() {
      this.c.removeCallbacks(this.e);
      this.t();
   }

   public void d(String var1) {
      this.notesInput.setText(var1);
   }

   public void e() {
      this.finish();
   }

   protected void onActivityResult(int var1, int var2, Intent var3) {
      if(var1 == 1 && var2 == -1) {
         this.a.c();
      }

      if(var1 == 2 && var2 == -1 && var3 != null && var3.getData() != null) {
         String var4 = var3.getData().toString();
         this.a.b(var4);
      }

   }

   public void onBackPressed() {
      this.a.c(this.notesInput.getText().toString());
   }

   public void onClick(View var1) {
      this.a.a();
   }

   public void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.setContentView(2131034141);
      ButterKnife.bind((Activity)this);
      this.l().a(this);
      this.setSupportActionBar(this.toolbar);
      this.b = new u();
      this.b.a((OnClickListener)this);
      this.imagePager.setAdapter(this.b);
      this.viewpagerIndicator.setViewPager(this.imagePager);
      this.a.a((co.uk.getmondo.common.ui.f)this);
      String var2 = this.getIntent().getStringExtra("EXTRA_TRANSACTION_ID");
      this.a.a(var2);
   }

   protected void onDestroy() {
      this.a.b();
      super.onDestroy();
   }

   @OnFocusChange({2131820802})
   void onNotesFocusChanged(boolean var1) {
      if(var1) {
         this.a.e();
      }

   }

   public boolean onOptionsItemSelected(MenuItem var1) {
      boolean var2;
      switch(var1.getItemId()) {
      case 16908332:
         this.a.c(this.notesInput.getText().toString());
         var2 = true;
         break;
      default:
         var2 = super.onOptionsItemSelected(var1);
      }

      return var2;
   }

   public void onRequestPermissionsResult(int var1, String[] var2, int[] var3) {
      if(var1 == 3) {
         if(var3.length == 1 && var3[0] == 0) {
            this.a.f();
         } else {
            this.a.g();
         }
      } else {
         super.onRequestPermissionsResult(var1, var2, var3);
      }

   }

   @OnClick({2131820805})
   void onSelectFromDeviceClick() {
      this.a.d();
   }
}
