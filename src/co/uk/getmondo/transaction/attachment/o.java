package co.uk.getmondo.transaction.attachment;

import java.io.InputStream;

// $FF: synthetic class
final class o implements io.reactivex.c.h {
   private final m a;
   private final InputStream b;
   private final long c;

   private o(m var1, InputStream var2, long var3) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
   }

   public static io.reactivex.c.h a(m var0, InputStream var1, long var2) {
      return new o(var0, var1, var2);
   }

   public Object a(Object var1) {
      return m.a(this.a, this.b, this.c, (String)var1);
   }
}
