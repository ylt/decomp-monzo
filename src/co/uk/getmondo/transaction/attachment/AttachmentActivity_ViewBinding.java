package co.uk.getmondo.transaction.attachment;

import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.EditText;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import me.relex.circleindicator.CircleIndicator;

public class AttachmentActivity_ViewBinding implements Unbinder {
   private AttachmentActivity a;
   private View b;
   private View c;

   public AttachmentActivity_ViewBinding(final AttachmentActivity var1, View var2) {
      this.a = var1;
      View var3 = Utils.findRequiredView(var2, 2131820802, "field 'notesInput' and method 'onNotesFocusChanged'");
      var1.notesInput = (EditText)Utils.castView(var3, 2131820802, "field 'notesInput'", EditText.class);
      this.b = var3;
      var3.setOnFocusChangeListener(new OnFocusChangeListener() {
         public void onFocusChange(View var1x, boolean var2) {
            var1.onNotesFocusChanged(var2);
         }
      });
      var1.imagePager = (ViewPager)Utils.findRequiredViewAsType(var2, 2131820803, "field 'imagePager'", ViewPager.class);
      var1.viewpagerIndicator = (CircleIndicator)Utils.findRequiredViewAsType(var2, 2131820804, "field 'viewpagerIndicator'", CircleIndicator.class);
      var1.toolbar = (Toolbar)Utils.findRequiredViewAsType(var2, 2131820798, "field 'toolbar'", Toolbar.class);
      var2 = Utils.findRequiredView(var2, 2131820805, "method 'onSelectFromDeviceClick'");
      this.c = var2;
      var2.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View var1x) {
            var1.onSelectFromDeviceClick();
         }
      });
   }

   public void unbind() {
      AttachmentActivity var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.notesInput = null;
         var1.imagePager = null;
         var1.viewpagerIndicator = null;
         var1.toolbar = null;
         this.b.setOnFocusChangeListener((OnFocusChangeListener)null);
         this.b = null;
         this.c.setOnClickListener((OnClickListener)null);
         this.c = null;
      }
   }
}
