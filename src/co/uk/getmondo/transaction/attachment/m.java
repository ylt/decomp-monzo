package co.uk.getmondo.transaction.attachment;

import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.d.aj;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;

class m extends co.uk.getmondo.common.ui.b {
   private final io.reactivex.u c;
   private final io.reactivex.u d;
   private final co.uk.getmondo.common.accounts.d e;
   private final co.uk.getmondo.transaction.a.a f;
   private final co.uk.getmondo.common.k.l g;
   private final co.uk.getmondo.common.a h;
   private final co.uk.getmondo.common.e.a i;
   private String j;
   private String k;
   private File l;

   m(io.reactivex.u var1, io.reactivex.u var2, co.uk.getmondo.common.accounts.d var3, co.uk.getmondo.transaction.a.a var4, co.uk.getmondo.common.k.l var5, co.uk.getmondo.common.a var6, co.uk.getmondo.common.e.a var7) {
      this.c = var1;
      this.d = var2;
      this.e = var3;
      this.f = var4;
      this.g = var5;
      this.h = var6;
      this.i = var7;
   }

   // $FF: synthetic method
   static io.reactivex.d a(m var0, InputStream var1, long var2, String var4) throws Exception {
      return var0.f.a(var4, var1, var2, var0.j);
   }

   private void a(aj var1) {
      String var2;
      if(var1.e() == null) {
         var2 = "";
      } else {
         var2 = var1.e();
      }

      this.k = var2;
      if(var1.f() != null) {
         ((m.a)this.a).a(var1.f().i());
      }

      if(var1.e() != null) {
         ((m.a)this.a).d(var1.e());
      }

      ((m.a)this.a).a((List)var1.A());
   }

   // $FF: synthetic method
   static void a(m var0) throws Exception {
      ((m.a)var0.a).d();
      ((m.a)var0.a).e();
   }

   // $FF: synthetic method
   static void a(m var0, aj var1) {
      var0.a(var1);
   }

   // $FF: synthetic method
   static void a(m var0, Throwable var1) throws Exception {
      ((m.a)var0.a).d();
      var0.i.a(var1, (co.uk.getmondo.common.e.a.a)var0.a);
   }

   private void a(InputStream var1, long var2) {
      ((m.a)this.a).b();
      this.a((io.reactivex.b.b)this.e.d().c(o.a(this, var1, var2)).b(this.d).a(this.c).a(p.a(this), q.a(this)));
   }

   // $FF: synthetic method
   static void b(m var0) throws Exception {
      ((m.a)var0.a).d();
   }

   // $FF: synthetic method
   static void b(m var0, Throwable var1) throws Exception {
      ((m.a)var0.a).d();
      if(!var0.i.a(var1, (co.uk.getmondo.common.e.a.a)var0.a)) {
         ((m.a)var0.a).b(2131362277);
      }

   }

   void a() {
      this.h.a(Impression.o());
      this.l = this.g.a();
      if(this.l != null) {
         ((m.a)this.a).a(this.l);
      }

   }

   public void a(String var1) {
      this.j = var1;
      this.a((io.reactivex.b.b)this.f.b(var1).observeOn(this.c).subscribe(n.a(this)));
   }

   void b(String var1) {
      this.a(this.g.a(var1), this.g.b(var1));
   }

   void c() {
      if(this.l != null) {
         try {
            FileInputStream var1 = new FileInputStream(this.l);
            this.a(var1, this.l.length());
         } catch (FileNotFoundException var2) {
            d.a.a.a((Throwable)var2);
         }
      }

   }

   void c(String var1) {
      if(var1.equals(this.k)) {
         ((m.a)this.a).e();
      } else {
         ((m.a)this.a).c();
         this.a((io.reactivex.b.b)this.f.a(var1, this.j).b(this.d).a(this.c).a(r.a(this), s.a(this)));
      }

   }

   void d() {
      this.h.a(Impression.p());
      ((m.a)this.a).a();
   }

   void e() {
      this.h.a(Impression.n());
   }

   void f() {
      if(this.l != null) {
         ((m.a)this.a).a(this.l);
      }

   }

   void g() {
      ((m.a)this.a).b(2131362019);
   }

   interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      void a();

      void a(File var1);

      void a(String var1);

      void a(List var1);

      void b();

      void c();

      void d();

      void d(String var1);

      void e();
   }
}
