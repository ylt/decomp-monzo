package co.uk.getmondo.transaction.attachment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ImageView.ScaleType;
import butterknife.BindView;
import butterknife.ButterKnife;

public class AttachmentDetailsActivity extends co.uk.getmondo.common.activities.b implements e.a {
   e a;
   private String b;
   private e.a.a.a.d c;
   @BindView(2131820815)
   Button deleteButton;
   @BindView(2131820814)
   ProgressBar progress;
   @BindView(2131820813)
   ImageView zoomableImageView;

   // $FF: synthetic method
   static String a(AttachmentDetailsActivity var0, Object var1) throws Exception {
      return var0.b;
   }

   public static void a(Context var0, String var1, String var2) {
      Intent var3 = new Intent(var0, AttachmentDetailsActivity.class);
      var3.putExtra("EXTRA_IMAGE_URL", var1);
      var3.putExtra("EXTRA_ATTACHMENT_ID", var2);
      var0.startActivity(var3);
   }

   public io.reactivex.n a() {
      return com.b.a.c.c.a(this.deleteButton).map(c.a(this));
   }

   public void b() {
      this.progress.setVisibility(0);
   }

   public void c() {
      this.progress.setVisibility(8);
   }

   public void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.setContentView(2131034145);
      ButterKnife.bind((Activity)this);
      this.l().a(this);
      this.b = this.getIntent().getStringExtra("EXTRA_ATTACHMENT_ID");
      String var2 = this.getIntent().getStringExtra("EXTRA_IMAGE_URL");
      this.c = new e.a.a.a.d(this.zoomableImageView);
      com.bumptech.glide.g.a((android.support.v4.app.j)this).a(var2).a(new com.bumptech.glide.g.b.d(this.zoomableImageView) {
         public void a(com.bumptech.glide.load.resource.a.b var1, com.bumptech.glide.g.a.c var2) {
            super.a(var1, var2);
            AttachmentDetailsActivity.this.c.j();
         }
      });
      this.c.a(ScaleType.FIT_CENTER);
      this.a.a((e.a)this);
   }

   protected void onDestroy() {
      this.a.b();
      super.onDestroy();
   }
}
