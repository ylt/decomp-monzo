package co.uk.getmondo.transaction.attachment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import java.util.ArrayList;
import java.util.List;

class u extends android.support.v4.view.p {
   private final List a = new ArrayList();
   private OnClickListener b;

   // $FF: synthetic method
   static void a(u var0, View var1) {
      var0.b.onClick(var1);
   }

   // $FF: synthetic method
   static void a(u var0, String var1, int var2, View var3) {
      AttachmentDetailsActivity.a(var3.getContext(), var1, ((co.uk.getmondo.d.d)var0.a.get(var2)).a());
   }

   public int a(Object var1) {
      return -2;
   }

   public Object a(ViewGroup var1, int var2) {
      ViewGroup var3 = (ViewGroup)LayoutInflater.from(var1.getContext()).inflate(2131034232, var1, false);
      var1.addView(var3);
      ImageView var7 = (ImageView)var3.findViewById(2131821192);
      ImageView var6 = (ImageView)var3.findViewById(2131821191);
      Button var5 = (Button)var3.findViewById(2131821190);
      final ProgressBar var4 = (ProgressBar)var3.findViewById(2131821193);
      if(var2 == this.a.size()) {
         var7.setVisibility(8);
         var5.setVisibility(0);
         var6.setVisibility(0);
         var5.setOnClickListener(v.a(this));
         var3.setOnClickListener((OnClickListener)null);
         var4.setVisibility(8);
         var7.setBackgroundResource(0);
         var7.setClipToOutline(false);
      } else if(var2 < this.a.size()) {
         var7.setVisibility(0);
         var5.setVisibility(8);
         var6.setVisibility(8);
         var4.setVisibility(0);
         String var8 = ((co.uk.getmondo.d.d)this.a.get(var2)).b();
         var7.setBackgroundResource(2130838008);
         var7.setClipToOutline(true);
         com.bumptech.glide.g.b(var3.getContext()).a(var8).a(new com.bumptech.glide.g.d() {
            public boolean a(com.bumptech.glide.load.resource.a.b var1, String var2, com.bumptech.glide.g.b.j var3, boolean var4x, boolean var5) {
               var4.setVisibility(8);
               return false;
            }

            public boolean a(Exception var1, String var2, com.bumptech.glide.g.b.j var3, boolean var4x) {
               var4.setVisibility(8);
               return false;
            }
         }).a(var7);
         var3.setOnClickListener(w.a(this, var8, var2));
      }

      return var3;
   }

   void a(OnClickListener var1) {
      this.b = var1;
   }

   public void a(ViewGroup var1, int var2, Object var3) {
      var1.removeView((View)var3);
   }

   void a(List var1) {
      this.a.clear();
      this.a.addAll(var1);
      this.c();
   }

   public boolean a(View var1, Object var2) {
      boolean var3;
      if(var1 == var2) {
         var3 = true;
      } else {
         var3 = false;
      }

      return var3;
   }

   public int b() {
      return this.a.size() + 1;
   }

   public CharSequence c(int var1) {
      return "";
   }
}
