package co.uk.getmondo.transaction.details;

import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.common.ui.f;
import co.uk.getmondo.d.aj;
import co.uk.getmondo.payments.send.data.p;
import io.reactivex.n;
import io.reactivex.r;
import io.reactivex.u;
import io.reactivex.v;
import io.reactivex.c.g;
import io.reactivex.c.h;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u001aB[\b\u0007\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\f\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u0012\u0006\u0010\u0014\u001a\u00020\u0015¢\u0006\u0002\u0010\u0016J\u0010\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u0002H\u0016R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u001b"},
   d2 = {"Lco/uk/getmondo/transaction/details/TransactionDetailsPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/transaction/details/TransactionDetailsPresenter$View;", "uiScheduler", "Lio/reactivex/Scheduler;", "ioScheduler", "transactionId", "", "transactionManager", "Lco/uk/getmondo/transaction/data/TransactionManager;", "syncManager", "Lco/uk/getmondo/background_sync/SyncManager;", "apiErrorHandler", "Lco/uk/getmondo/common/errors/ApiErrorHandler;", "costSplitter", "Lco/uk/getmondo/transaction/CostSplitter;", "userSettingsStorage", "Lco/uk/getmondo/payments/send/data/UserSettingsStorage;", "analyticsService", "Lco/uk/getmondo/common/AnalyticsService;", "accountManager", "Lco/uk/getmondo/common/accounts/AccountManager;", "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Ljava/lang/String;Lco/uk/getmondo/transaction/data/TransactionManager;Lco/uk/getmondo/background_sync/SyncManager;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/transaction/CostSplitter;Lco/uk/getmondo/payments/send/data/UserSettingsStorage;Lco/uk/getmondo/common/AnalyticsService;Lco/uk/getmondo/common/accounts/AccountManager;)V", "register", "", "view", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class b extends co.uk.getmondo.common.ui.b {
   private final u c;
   private final u d;
   private final String e;
   private final co.uk.getmondo.transaction.a.a f;
   private final co.uk.getmondo.background_sync.d g;
   private final co.uk.getmondo.common.e.a h;
   private final co.uk.getmondo.transaction.a i;
   private final p j;
   private final co.uk.getmondo.common.a k;
   private final co.uk.getmondo.common.accounts.b l;

   public b(u var1, u var2, String var3, co.uk.getmondo.transaction.a.a var4, co.uk.getmondo.background_sync.d var5, co.uk.getmondo.common.e.a var6, co.uk.getmondo.transaction.a var7, p var8, co.uk.getmondo.common.a var9, co.uk.getmondo.common.accounts.b var10) {
      l.b(var1, "uiScheduler");
      l.b(var2, "ioScheduler");
      l.b(var3, "transactionId");
      l.b(var4, "transactionManager");
      l.b(var5, "syncManager");
      l.b(var6, "apiErrorHandler");
      l.b(var7, "costSplitter");
      l.b(var8, "userSettingsStorage");
      l.b(var9, "analyticsService");
      l.b(var10, "accountManager");
      super();
      this.c = var1;
      this.d = var2;
      this.e = var3;
      this.f = var4;
      this.g = var5;
      this.h = var6;
      this.i = var7;
      this.j = var8;
      this.k = var9;
      this.l = var10;
   }

   public void a(final b.a var1) {
      l.b(var1, "view");
      super.a((f)var1);
      if(this.f.a(this.e) == null) {
         d.a.a.a((Throwable)(new RuntimeException("Transaction missing, syncing")));
         this.a((io.reactivex.b.b)this.g.a().b(this.d).a((io.reactivex.c.a)null.a, (g)(new g() {
            public final void a(Throwable var1x) {
               co.uk.getmondo.common.e.a var2 = b.this.h;
               l.a(var1x, "throwable");
               var2.a(var1x, (co.uk.getmondo.common.e.a.a)var1);
            }
         })));
      }

      this.k.a(Impression.Companion.f(this.e));
      n var4 = this.f.b(this.e);
      io.reactivex.b.a var5 = this.b;
      n var7 = var4.switchMapSingle((h)(new h() {
         public final v a(final aj var1) {
            l.b(var1, "transaction");
            return b.this.f.a(var1).d((h)(new h() {
               public final kotlin.h a(co.uk.getmondo.transaction.details.c.f var1x) {
                  l.b(var1x, "history");
                  return new kotlin.h(var1, var1x);
               }
            }));
         }
      })).observeOn(this.c);
      g var6 = (g)(new g() {
         public final void a(kotlin.h var1x) {
            aj var2 = (aj)var1x.c();
            co.uk.getmondo.transaction.details.c.f var5 = (co.uk.getmondo.transaction.details.c.f)var1x.d();
            b.a var4 = var1;
            co.uk.getmondo.d.h var3 = var2.c();
            l.a(var3, "transaction.category");
            var4.a(var3);
            b.a var6;
            if(var2.r()) {
               var6 = var1;
               l.a(var2, "transaction");
               if(var5 == null) {
                  throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.transaction.details.model.CashFlowHistory");
               }

               var6.a((co.uk.getmondo.transaction.details.b.l)(new co.uk.getmondo.transaction.details.d.c.a(var2, (co.uk.getmondo.transaction.details.c.b)var5)));
            } else if(var2.s()) {
               var6 = var1;
               l.a(var2, "transaction");
               l.a(var5, "transactionHistory");
               var6.a((co.uk.getmondo.transaction.details.b.l)(new co.uk.getmondo.transaction.details.d.b.b(var2, var5)));
            } else if(var2.q()) {
               var6 = var1;
               l.a(var2, "transaction");
               if(var5 == null) {
                  throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.transaction.details.model.CashFlowHistory");
               }

               var6.a((co.uk.getmondo.transaction.details.b.l)(new co.uk.getmondo.transaction.details.d.f.c(var2, (co.uk.getmondo.transaction.details.c.b)var5)));
            } else if(var2.z()) {
               var6 = var1;
               l.a(var2, "transaction");
               if(var5 == null) {
                  throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.transaction.details.model.AverageSpendingHistory");
               }

               var6.a((co.uk.getmondo.transaction.details.b.l)(new co.uk.getmondo.transaction.details.d.h.c(var2, (co.uk.getmondo.transaction.details.c.a)var5, b.this.l)));
            } else if(var2.a()) {
               var6 = var1;
               l.a(var2, "transaction");
               if(var5 == null) {
                  throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.transaction.details.model.AverageSpendingHistory");
               }

               var6.a((co.uk.getmondo.transaction.details.b.l)(new co.uk.getmondo.transaction.details.d.g.b(var2, (co.uk.getmondo.transaction.details.c.a)var5)));
            } else {
               co.uk.getmondo.d.u var7 = var2.f();
               if(var7 != null && var7.g()) {
                  var6 = var1;
                  l.a(var2, "transaction");
                  if(var5 == null) {
                     throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.transaction.details.model.AverageSpendingHistory");
                  }

                  var6.a((co.uk.getmondo.transaction.details.b.l)(new co.uk.getmondo.transaction.details.d.a.a(var2, (co.uk.getmondo.transaction.details.c.a)var5)));
               } else {
                  var6 = var1;
                  l.a(var2, "transaction");
                  l.a(var5, "transactionHistory");
                  var6.a((co.uk.getmondo.transaction.details.b.l)(new co.uk.getmondo.transaction.details.d.d.d(var2, var5, b.this.i, b.this.j, b.this.k, b.this.l)));
               }
            }

         }
      });
      kotlin.d.a.b var3 = (kotlin.d.a.b)null.a;
      Object var2 = var3;
      if(var3 != null) {
         var2 = new c(var3);
      }

      io.reactivex.b.b var10 = var7.subscribe(var6, (g)var2);
      l.a(var10, "transactionObservable\n  …            }, Timber::e)");
      this.b = co.uk.getmondo.common.j.f.a(var5, var10);
      var5 = this.b;
      n var11 = var1.a().switchMap((h)(new h() {
         public final n a(kotlin.n var1x) {
            l.b(var1x, "it");
            return var1.a(b.this.e);
         }
      }));
      l.a(var4, "transactionObservable");
      io.reactivex.b var13 = co.uk.getmondo.common.j.f.a(var11, (r)var4).doOnNext((g)(new g() {
         public final void a(kotlin.h var1) {
            co.uk.getmondo.d.h var2 = (co.uk.getmondo.d.h)var1.c();
            aj var4 = (aj)var1.d();
            co.uk.getmondo.common.a var5 = b.this.k;
            Impression.Companion var3 = Impression.Companion;
            co.uk.getmondo.d.h var6 = var4.c();
            l.a(var6, "transaction.category");
            l.a(var2, "category");
            var5.a(var3.a(var6, var2));
         }
      })).flatMapCompletable((h)(new h() {
         public final io.reactivex.b a(kotlin.h var1x) {
            l.b(var1x, "<name for destructuring parameter 0>");
            co.uk.getmondo.d.h var2 = (co.uk.getmondo.d.h)var1x.c();
            return b.this.f.a(b.this.e, var2).b(b.this.d).a(b.this.c).a((g)(new g() {
               public final void a(Throwable var1x) {
                  co.uk.getmondo.common.e.a var2 = b.this.h;
                  l.a(var1x, "error");
                  var2.a(var1x, (co.uk.getmondo.common.e.a.a)var1);
               }
            })).b();
         }
      }));
      io.reactivex.c.a var16 = (io.reactivex.c.a)null.a;
      var3 = (kotlin.d.a.b)null.a;
      var2 = var3;
      if(var3 != null) {
         var2 = new c(var3);
      }

      var10 = var13.a(var16, (g)var2);
      l.a(var10, "view.onChangeCategoryCli….subscribe({}, Timber::e)");
      this.b = co.uk.getmondo.common.j.f.a(var5, var10);
      io.reactivex.b.a var12 = this.b;
      var4 = var1.c();
      g var15 = (g)(new g() {
         public final void a(kotlin.n var1) {
            b.this.k.a(Impression.Companion.g(b.this.e));
         }
      });
      kotlin.d.a.b var14 = (kotlin.d.a.b)null.a;
      Object var8 = var14;
      if(var14 != null) {
         var8 = new c(var14);
      }

      io.reactivex.b.b var9 = var4.subscribe(var15, (g)var8);
      l.a(var9, "view.onMapClicked()\n    …            }, Timber::e)");
      this.b = co.uk.getmondo.common.j.f.a(var12, var9);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u00012\u00020\u0002J\u0016\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0006\u001a\u00020\u0007H&J\u000e\u0010\b\u001a\b\u0012\u0004\u0012\u00020\t0\u0004H&J\u000e\u0010\n\u001a\b\u0012\u0004\u0012\u00020\t0\u0004H&J\u0010\u0010\u000b\u001a\u00020\t2\u0006\u0010\f\u001a\u00020\u0005H&J\u0010\u0010\r\u001a\u00020\t2\u0006\u0010\u000e\u001a\u00020\u000fH&¨\u0006\u0010"},
      d2 = {"Lco/uk/getmondo/transaction/details/TransactionDetailsPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "onCategoryChanged", "Lio/reactivex/Observable;", "Lco/uk/getmondo/model/Category;", "transactionId", "", "onChangeCategoryClicked", "", "onMapClicked", "setCategory", "category", "setTransaction", "transactionViewModel", "Lco/uk/getmondo/transaction/details/base/TransactionViewModel;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, f {
      n a();

      n a(String var1);

      void a(co.uk.getmondo.d.h var1);

      void a(co.uk.getmondo.transaction.details.b.l var1);

      n c();
   }
}
