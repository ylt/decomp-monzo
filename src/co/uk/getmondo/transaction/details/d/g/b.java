package co.uk.getmondo.transaction.details.d.g;

import android.content.Context;
import android.content.res.Resources;
import co.uk.getmondo.common.ui.InfoTextActivity;
import co.uk.getmondo.d.aj;
import co.uk.getmondo.transaction.details.a.c;
import co.uk.getmondo.transaction.details.b.d;
import co.uk.getmondo.transaction.details.b.e;
import co.uk.getmondo.transaction.details.b.f;
import co.uk.getmondo.transaction.details.b.h;
import co.uk.getmondo.transaction.details.b.l;
import co.uk.getmondo.transaction.details.b.n;
import java.util.List;
import kotlin.Metadata;
import kotlin.a.m;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\u0014\u001a\u00020\u0003HÂ\u0003J\t\u0010\u0015\u001a\u00020\u0005HÂ\u0003J\u001d\u0010\u0016\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u0017\u001a\u00020\u00182\b\u0010\u0019\u001a\u0004\u0018\u00010\u001aHÖ\u0003J\t\u0010\u001b\u001a\u00020\u001cHÖ\u0001J\t\u0010\u001d\u001a\u00020\u001eHÖ\u0001R\u001a\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\n\u0010\u000bR\u0014\u0010\f\u001a\u00020\r8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u000e\u0010\u000fR\u0014\u0010\u0010\u001a\u00020\u00118VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0012\u0010\u0013R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u001f"},
   d2 = {"Lco/uk/getmondo/transaction/details/types/tfl/TflTransaction;", "Lco/uk/getmondo/transaction/details/base/TransactionViewModel;", "transaction", "Lco/uk/getmondo/model/Transaction;", "transactionHistory", "Lco/uk/getmondo/transaction/details/model/AverageSpendingHistory;", "(Lco/uk/getmondo/model/Transaction;Lco/uk/getmondo/transaction/details/model/AverageSpendingHistory;)V", "actions", "", "Lco/uk/getmondo/transaction/details/base/Action;", "getActions", "()Ljava/util/List;", "content", "Lco/uk/getmondo/transaction/details/base/Content;", "getContent", "()Lco/uk/getmondo/transaction/details/base/Content;", "header", "Lco/uk/getmondo/transaction/details/base/BaseHeader;", "getHeader", "()Lco/uk/getmondo/transaction/details/base/BaseHeader;", "component1", "component2", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class b implements l {
   private final aj a;
   private final co.uk.getmondo.transaction.details.c.a b;

   public b(aj var1, co.uk.getmondo.transaction.details.c.a var2) {
      kotlin.d.b.l.b(var1, "transaction");
      kotlin.d.b.l.b(var2, "transactionHistory");
      super();
      this.a = var1;
      this.b = var2;
   }

   public d a() {
      return (d)(new a(this.a));
   }

   public List b() {
      return m.b(new co.uk.getmondo.transaction.details.b.a[]{(co.uk.getmondo.transaction.details.b.a)(new co.uk.getmondo.transaction.details.a.b(this.a)), (co.uk.getmondo.transaction.details.b.a)(new c(this.a))});
   }

   public e c() {
      return (e)(new e() {
         public h a() {
            return (h)(new h() {
               public String a(Resources var1) {
                  kotlin.d.b.l.b(var1, "resources");
                  String var2 = var1.getString(2131362250);
                  kotlin.d.b.l.a(var2, "resources.getString(R.st…how_do_tfl_payments_work)");
                  return var2;
               }

               public void a(Context var1) {
                  kotlin.d.b.l.b(var1, "context");
                  InfoTextActivity.a(var1, var1.getString(2131362773), 2131034217);
               }
            });
         }

         public n b() {
            return (n)(new co.uk.getmondo.transaction.details.d.e.a(b.this.a, b.this.b));
         }
      });
   }

   public f d() {
      return l.a.a(this);
   }

   public boolean equals(Object var1) {
      boolean var2;
      if(this != var1) {
         label28: {
            if(var1 instanceof b) {
               b var3 = (b)var1;
               if(kotlin.d.b.l.a(this.a, var3.a) && kotlin.d.b.l.a(this.b, var3.b)) {
                  break label28;
               }
            }

            var2 = false;
            return var2;
         }
      }

      var2 = true;
      return var2;
   }

   public int hashCode() {
      int var2 = 0;
      aj var3 = this.a;
      int var1;
      if(var3 != null) {
         var1 = var3.hashCode();
      } else {
         var1 = 0;
      }

      co.uk.getmondo.transaction.details.c.a var4 = this.b;
      if(var4 != null) {
         var2 = var4.hashCode();
      }

      return var1 * 31 + var2;
   }

   public String toString() {
      return "TflTransaction(transaction=" + this.a + ", transactionHistory=" + this.b + ")";
   }
}
