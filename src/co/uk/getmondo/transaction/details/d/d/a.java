package co.uk.getmondo.transaction.details.d.d;

import android.content.Context;
import co.uk.getmondo.d.u;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0016\u0010\u0005\u001a\u0004\u0018\u00010\u00068VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0007\u0010\bR\u0014\u0010\t\u001a\u00020\n8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\t\u0010\u000bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u0016\u0010\f\u001a\u0004\u0018\u00010\u00068VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\r\u0010\bR\u0016\u0010\u000e\u001a\u0004\u0018\u00010\u000f8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0010\u0010\u0011¨\u0006\u0012"},
   d2 = {"Lco/uk/getmondo/transaction/details/types/general/EnrichedAvatar;", "Lco/uk/getmondo/transaction/details/base/Avatar;", "merchant", "Lco/uk/getmondo/model/Merchant;", "(Lco/uk/getmondo/model/Merchant;)V", "imageUrl", "", "getImageUrl", "()Ljava/lang/String;", "isSquare", "", "()Z", "name", "getName", "resourceId", "", "getResourceId", "()Ljava/lang/Integer;", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class a implements co.uk.getmondo.transaction.details.b.c {
   private final u a;

   public a(u var1) {
      l.b(var1, "merchant");
      super();
      this.a = var1;
   }

   public Integer a(Context var1) {
      l.b(var1, "context");
      return co.uk.getmondo.transaction.details.b.c.a.a(this, var1);
   }

   public String a() {
      CharSequence var2 = (CharSequence)this.a.j();
      boolean var1;
      if(var2 != null && var2.length() != 0) {
         var1 = false;
      } else {
         var1 = true;
      }

      String var3;
      if(var1) {
         var3 = null;
      } else {
         var3 = this.a.j();
      }

      return var3;
   }

   public Integer b() {
      Integer var1;
      if(this.a.g()) {
         var1 = Integer.valueOf(2130837789);
      } else {
         var1 = null;
      }

      return var1;
   }

   public String c() {
      String var1;
      if(this.a.g()) {
         var1 = null;
      } else {
         var1 = this.a.i();
      }

      return var1;
   }

   public boolean d() {
      return true;
   }
}
