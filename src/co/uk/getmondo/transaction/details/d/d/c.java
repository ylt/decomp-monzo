package co.uk.getmondo.transaction.details.d.d;

import android.content.Context;
import android.content.res.Resources;
import co.uk.getmondo.d.aj;
import co.uk.getmondo.d.u;
import co.uk.getmondo.transaction.details.b.j;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0012\u0010\r\u001a\u0004\u0018\u00010\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\u0012\u0010\u0011\u001a\u0004\u0018\u00010\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\u0010\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0015H\u0016J\u0010\u0010\u0016\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016R\u0014\u0010\u0005\u001a\u00020\u00068VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0007\u0010\bR\u0016\u0010\t\u001a\u0004\u0018\u00010\n8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\fR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0017"},
   d2 = {"Lco/uk/getmondo/transaction/details/types/general/GeneralHeader;", "Lco/uk/getmondo/transaction/details/base/BaseHeader;", "transaction", "Lco/uk/getmondo/model/Transaction;", "(Lco/uk/getmondo/model/Transaction;)V", "avatar", "Lco/uk/getmondo/transaction/details/base/Avatar;", "getAvatar", "()Lco/uk/getmondo/transaction/details/base/Avatar;", "mapCoordinates", "Lco/uk/getmondo/transaction/details/model/MapCoordinates;", "getMapCoordinates", "()Lco/uk/getmondo/transaction/details/model/MapCoordinates;", "extraInformation", "", "resources", "Landroid/content/res/Resources;", "subtitle", "subtitleTextAppearance", "Lco/uk/getmondo/transaction/details/base/AddressTextAppearance;", "context", "Landroid/content/Context;", "title", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class c extends co.uk.getmondo.transaction.details.b.d {
   private final aj a;

   public c(aj var1) {
      l.b(var1, "transaction");
      super(var1);
      this.a = var1;
   }

   public String a(Resources var1) {
      Object var2 = null;
      l.b(var1, "resources");
      u var3 = this.a.f();
      String var4 = (String)var2;
      if(var3 != null) {
         if(var3.k()) {
            var4 = (String)var2;
         } else {
            com.c.b.b var5 = var3.c();
            var4 = (String)var2;
            if(var5 != null) {
               var4 = (String)var5.a((Object)null);
            }
         }
      }

      return var4;
   }

   // $FF: synthetic method
   public j b(Context var1) {
      return (j)this.c(var1);
   }

   public String b(Resources var1) {
      l.b(var1, "resources");
      String var6;
      if(this.a.i()) {
         String var4 = this.a.g().g();
         String var5 = this.a.h().g();
         double var2 = this.a.j();
         if(this.a.k()) {
            var6 = var1.getString(2131362554, new Object[]{var4, var5, Double.valueOf(var2)});
         } else {
            var6 = var1.getString(2131362189, new Object[]{var4, var5, Double.valueOf(var2)});
         }
      } else {
         u var7 = this.a.f();
         if(var7 != null && var7.k()) {
            var6 = var1.getString(2131362826);
         } else {
            var6 = null;
         }
      }

      return var6;
   }

   public co.uk.getmondo.transaction.details.b.b c(Context var1) {
      l.b(var1, "context");
      return new co.uk.getmondo.transaction.details.b.b(var1);
   }

   public String c(Resources var1) {
      l.b(var1, "resources");
      u var2 = this.a.f();
      String var3;
      if(var2 != null) {
         if(var2.g()) {
            var3 = var1.getString(2131362843);
            l.a(var3, "resources.getString(R.string.tx_title_atm)");
         } else {
            var3 = var2.i();
            l.a(var3, "merchant.name");
         }
      } else {
         var3 = this.a.x();
         l.a(var3, "transaction.description");
      }

      return var3;
   }

   public co.uk.getmondo.transaction.details.c.c h() {
      Object var2 = null;
      u var5 = this.a.f();
      co.uk.getmondo.transaction.details.c.c var1 = (co.uk.getmondo.transaction.details.c.c)var2;
      if(var5 != null) {
         if(var5.k()) {
            var1 = (co.uk.getmondo.transaction.details.c.c)var2;
         } else {
            Double var4 = var5.d();
            Double var3 = var5.e();
            var1 = (co.uk.getmondo.transaction.details.c.c)var2;
            if(var4 != null) {
               var1 = (co.uk.getmondo.transaction.details.c.c)var2;
               if(var3 != null) {
                  String var6 = var5.i();
                  l.a(var6, "merchant.name");
                  var1 = new co.uk.getmondo.transaction.details.c.c(var6, var4.doubleValue(), var3.doubleValue());
               }
            }
         }
      }

      return var1;
   }

   public co.uk.getmondo.transaction.details.b.c j() {
      u var1 = this.a.f();
      co.uk.getmondo.transaction.details.b.c var2;
      if(var1 == null) {
         var2 = (co.uk.getmondo.transaction.details.b.c)(new e(this.a));
      } else {
         var2 = (co.uk.getmondo.transaction.details.b.c)(new a(var1));
      }

      return var2;
   }
}
