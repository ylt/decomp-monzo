package co.uk.getmondo.transaction.details.d.f;

import android.content.Context;
import android.content.res.Resources;
import co.uk.getmondo.common.k.o;
import co.uk.getmondo.d.aj;
import co.uk.getmondo.transaction.details.b.d;
import kotlin.Metadata;
import kotlin.d.b.l;
import kotlin.h.j;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\b\u0010\r\u001a\u00020\u000eH\u0002J\u0012\u0010\u000f\u001a\u0004\u0018\u00010\u000e2\u0006\u0010\u0010\u001a\u00020\u0011H\u0016J\u0010\u0010\u0012\u001a\u00020\u000e2\u0006\u0010\u0010\u001a\u00020\u0011H\u0016R\u0014\u0010\u0005\u001a\u00020\u00068VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0007\u0010\bR\u0014\u0010\t\u001a\u00020\n8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\fR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0013"},
   d2 = {"Lco/uk/getmondo/transaction/details/types/p2p/P2pHeader;", "Lco/uk/getmondo/transaction/details/base/BaseHeader;", "transaction", "Lco/uk/getmondo/model/Transaction;", "(Lco/uk/getmondo/model/Transaction;)V", "avatar", "Lco/uk/getmondo/transaction/details/base/Avatar;", "getAvatar", "()Lco/uk/getmondo/transaction/details/base/Avatar;", "showSubtitleBeforeTitle", "", "getShowSubtitleBeforeTitle", "()Z", "getTitle", "", "subtitle", "resources", "Landroid/content/res/Resources;", "title", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class a extends d {
   private final aj a;

   public a(aj var1) {
      l.b(var1, "transaction");
      super(var1);
      this.a = var1;
   }

   private final String k() {
      String var1;
      if(this.a.r() && j.a((CharSequence)this.a.B().b())) {
         co.uk.getmondo.payments.send.data.a.a var3 = this.a.C();
         if(var3 != null) {
            var1 = var3.b();
         } else {
            var1 = null;
         }

         if(var1 == null) {
            l.a();
         }

         var1 = o.a(var1);
         StringBuilder var2 = (new StringBuilder()).append("").append(var1).append(" • ");
         var3 = this.a.C();
         if(var3 != null) {
            var1 = var3.c();
         } else {
            var1 = null;
         }

         var1 = var2.append(var1).toString();
      } else {
         var1 = this.a.B().b();
         l.a(var1, "transaction.peer.name");
      }

      return var1;
   }

   public String a(Resources var1) {
      l.b(var1, "resources");
      int var2;
      if(this.a.g().a()) {
         var2 = 2131362828;
      } else {
         var2 = 2131362827;
      }

      return var1.getString(var2);
   }

   public String c(Resources var1) {
      l.b(var1, "resources");
      return this.k();
   }

   public boolean g() {
      return this.a.g().a();
   }

   public co.uk.getmondo.transaction.details.b.c j() {
      return (co.uk.getmondo.transaction.details.b.c)(new co.uk.getmondo.transaction.details.b.c() {
         public Integer a(Context var1) {
            l.b(var1, "context");
            return co.uk.getmondo.transaction.details.b.c.a.a(this, var1);
         }

         public String a() {
            return a.this.a.B().g();
         }

         public Integer b() {
            return co.uk.getmondo.transaction.details.b.c.a.b(this);
         }

         public String c() {
            return a.this.k();
         }

         public boolean d() {
            return false;
         }
      });
   }
}
