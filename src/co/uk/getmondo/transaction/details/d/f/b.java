package co.uk.getmondo.transaction.details.d.f;

import android.content.res.Resources;
import co.uk.getmondo.d.aj;
import co.uk.getmondo.transaction.details.b.n;
import co.uk.getmondo.transaction.details.b.o;
import java.util.List;
import kotlin.Metadata;
import kotlin.a.m;
import kotlin.d.b.l;
import kotlin.h.j;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0012\u0010\f\u001a\u0004\u0018\u00010\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0016J\u0010\u0010\u0010\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0016R\u001a\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\n\u0010\u000bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0011"},
   d2 = {"Lco/uk/getmondo/transaction/details/types/p2p/P2pHistory;", "Lco/uk/getmondo/transaction/details/base/YourHistory;", "transaction", "Lco/uk/getmondo/model/Transaction;", "transactionHistory", "Lco/uk/getmondo/transaction/details/model/CashFlowHistory;", "(Lco/uk/getmondo/model/Transaction;Lco/uk/getmondo/transaction/details/model/CashFlowHistory;)V", "items", "", "Lco/uk/getmondo/transaction/details/base/YourHistoryItem;", "getItems", "()Ljava/util/List;", "searchQuery", "", "resources", "Landroid/content/res/Resources;", "title", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class b implements n {
   private final aj a;
   private final co.uk.getmondo.transaction.details.c.b b;

   public b(aj var1, co.uk.getmondo.transaction.details.c.b var2) {
      l.b(var1, "transaction");
      l.b(var2, "transactionHistory");
      super();
      this.a = var1;
      this.b = var2;
   }

   public String a(Resources var1) {
      l.b(var1, "resources");
      CharSequence var3 = (CharSequence)this.a.B().b();
      boolean var2;
      if(var3 != null && !j.a(var3)) {
         var2 = false;
      } else {
         var2 = true;
      }

      String var4;
      if(var2) {
         var4 = var1.getString(2131362643);
         l.a(var4, "resources.getString(R.string.send_money_history)");
      } else {
         var4 = var1.getString(2131362644, new Object[]{this.a.B().b()});
         l.a(var4, "resources.getString(R.st…t, transaction.peer.name)");
      }

      return var4;
   }

   public List a() {
      o var1 = new o() {
         public String a(Resources var1) {
            l.b(var1, "resources");
            String var2 = var1.getString(2131362840);
            l.a(var2, "resources.getString(R.st…ng.tx_p2p_no_of_payments)");
            return var2;
         }

         public String b(Resources var1) {
            l.b(var1, "resources");
            return null;
         }

         public String c(Resources var1) {
            l.b(var1, "resources");
            return String.valueOf(b.this.b.c() + b.this.b.a());
         }
      };
      o var2 = new o() {
         public String a(Resources var1) {
            l.b(var1, "resources");
            String var2 = var1.getString(2131362842);
            l.a(var2, "resources.getString(R.string.tx_p2p_total_sent)");
            return var2;
         }

         public String b(Resources var1) {
            l.b(var1, "resources");
            int var2 = (int)b.this.b.a();
            return var1.getQuantityString(2131886085, var2, new Object[]{Integer.valueOf(var2)});
         }

         public String c(Resources var1) {
            l.b(var1, "resources");
            String var2 = var1.getString(2131362195, new Object[]{Double.valueOf(b.this.b.b().e())});
            l.a(var2, "resources.getString(R.st…o_part_amount_gbp, value)");
            return var2;
         }
      };
      o var3 = new o() {
         public String a(Resources var1) {
            l.b(var1, "resources");
            String var2 = var1.getString(2131362841);
            l.a(var2, "resources.getString(R.st…ng.tx_p2p_total_received)");
            return var2;
         }

         public String b(Resources var1) {
            l.b(var1, "resources");
            int var2 = (int)b.this.b.c();
            return var1.getQuantityString(2131886085, var2, new Object[]{Integer.valueOf(var2)});
         }

         public String c(Resources var1) {
            l.b(var1, "resources");
            String var2 = var1.getString(2131362195, new Object[]{Double.valueOf(b.this.b.d().e())});
            l.a(var2, "resources.getString(R.st…o_part_amount_gbp, value)");
            return var2;
         }
      };
      return m.b(new o[]{(o)var1, (o)var2, (o)var3});
   }

   public String b(Resources var1) {
      l.b(var1, "resources");
      return this.a.B().b();
   }
}
