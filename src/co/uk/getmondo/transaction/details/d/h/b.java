package co.uk.getmondo.transaction.details.d.h;

import android.content.res.Resources;
import co.uk.getmondo.transaction.details.b.n;
import co.uk.getmondo.transaction.details.b.o;
import java.util.List;
import kotlin.Metadata;
import kotlin.a.m;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0012\u0010\n\u001a\u0004\u0018\u00010\u000b2\u0006\u0010\f\u001a\u00020\rH\u0016J\u0010\u0010\u000e\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\rH\u0016R\u001a\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u00068VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\b\u0010\tR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u000f"},
   d2 = {"Lco/uk/getmondo/transaction/details/types/topup/TopUpHistory;", "Lco/uk/getmondo/transaction/details/base/YourHistory;", "transactionHistory", "Lco/uk/getmondo/transaction/details/model/AverageSpendingHistory;", "(Lco/uk/getmondo/transaction/details/model/AverageSpendingHistory;)V", "items", "", "Lco/uk/getmondo/transaction/details/base/YourHistoryItem;", "getItems", "()Ljava/util/List;", "searchQuery", "", "resources", "Landroid/content/res/Resources;", "title", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class b implements n {
   private final co.uk.getmondo.transaction.details.c.a a;

   public b(co.uk.getmondo.transaction.details.c.a var1) {
      l.b(var1, "transactionHistory");
      super();
      this.a = var1;
   }

   public String a(Resources var1) {
      l.b(var1, "resources");
      String var2 = var1.getString(2131362906);
      l.a(var2, "resources.getString(R.string.your_topup_history)");
      return var2;
   }

   public List a() {
      List var1;
      if(this.a.a() <= 1L) {
         var1 = m.a();
      } else {
         o var3 = new o() {
            public String a(Resources var1) {
               l.b(var1, "resources");
               String var2 = var1.getString(2131362846);
               l.a(var2, "resources.getString(R.st…ng.tx_topup_no_of_visits)");
               return var2;
            }

            // $FF: synthetic method
            public String b(Resources var1) {
               return (String)this.d(var1);
            }

            public String c(Resources var1) {
               l.b(var1, "resources");
               return String.valueOf(b.this.a.a());
            }

            public Void d(Resources var1) {
               l.b(var1, "resources");
               return null;
            }
         };
         o var2 = new o() {
            public String a(Resources var1) {
               l.b(var1, "resources");
               String var2 = var1.getString(2131362845);
               l.a(var2, "resources.getString(R.st…g.tx_topup_average_spend)");
               return var2;
            }

            public String b(Resources var1) {
               l.b(var1, "resources");
               int var2 = (int)b.this.a.a();
               return var1.getQuantityString(2131886085, var2, new Object[]{Integer.valueOf(var2)});
            }

            public String c(Resources var1) {
               l.b(var1, "resources");
               String var2 = var1.getString(2131362195, new Object[]{Double.valueOf(b.this.a.b().e())});
               l.a(var2, "resources.getString(R.st…o_part_amount_gbp, value)");
               return var2;
            }
         };
         o var4 = new o() {
            public String a(Resources var1) {
               l.b(var1, "resources");
               String var2 = var1.getString(2131362847);
               l.a(var2, "resources.getString(R.string.tx_topup_total_spent)");
               return var2;
            }

            public String b(Resources var1) {
               l.b(var1, "resources");
               int var2 = (int)b.this.a.a();
               return var1.getQuantityString(2131886085, var2, new Object[]{Integer.valueOf(var2)});
            }

            public String c(Resources var1) {
               l.b(var1, "resources");
               String var2 = var1.getString(2131362195, new Object[]{Double.valueOf(b.this.a.c().e())});
               l.a(var2, "resources.getString(R.st…o_part_amount_gbp, value)");
               return var2;
            }
         };
         var1 = m.b(new o[]{(o)var3, (o)var2, (o)var4});
      }

      return var1;
   }

   public String b(Resources var1) {
      l.b(var1, "resources");
      return var1.getString(2131362844);
   }
}
