package co.uk.getmondo.transaction.details.c;

import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005¢\u0006\u0002\u0010\u0007J\t\u0010\r\u001a\u00020\u0003HÆ\u0003J\t\u0010\u000e\u001a\u00020\u0005HÆ\u0003J\t\u0010\u000f\u001a\u00020\u0005HÆ\u0003J'\u0010\u0010\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u0011\u001a\u00020\u00122\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014HÖ\u0003J\t\u0010\u0015\u001a\u00020\u0016HÖ\u0001J\t\u0010\u0017\u001a\u00020\u0018HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\u0006\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\t¨\u0006\u0019"},
   d2 = {"Lco/uk/getmondo/transaction/details/model/AverageSpendingHistory;", "Lco/uk/getmondo/transaction/details/model/TransactionHistory;", "total", "", "averageAmount", "Lco/uk/getmondo/model/Amount;", "totalAmount", "(JLco/uk/getmondo/model/Amount;Lco/uk/getmondo/model/Amount;)V", "getAverageAmount", "()Lco/uk/getmondo/model/Amount;", "getTotal", "()J", "getTotalAmount", "component1", "component2", "component3", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class a extends f {
   private final long a;
   private final co.uk.getmondo.d.c b;
   private final co.uk.getmondo.d.c c;

   public a(long var1, co.uk.getmondo.d.c var3, co.uk.getmondo.d.c var4) {
      l.b(var3, "averageAmount");
      l.b(var4, "totalAmount");
      super((i)null);
      this.a = var1;
      this.b = var3;
      this.c = var4;
   }

   public final long a() {
      return this.a;
   }

   public final co.uk.getmondo.d.c b() {
      return this.b;
   }

   public final co.uk.getmondo.d.c c() {
      return this.c;
   }

   public boolean equals(Object var1) {
      boolean var4 = false;
      boolean var3;
      if(this != var1) {
         var3 = var4;
         if(!(var1 instanceof a)) {
            return var3;
         }

         a var5 = (a)var1;
         boolean var2;
         if(this.a == var5.a) {
            var2 = true;
         } else {
            var2 = false;
         }

         var3 = var4;
         if(!var2) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.b, var5.b)) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.c, var5.c)) {
            return var3;
         }
      }

      var3 = true;
      return var3;
   }

   public int hashCode() {
      int var2 = 0;
      long var4 = this.a;
      int var3 = (int)(var4 ^ var4 >>> 32);
      co.uk.getmondo.d.c var6 = this.b;
      int var1;
      if(var6 != null) {
         var1 = var6.hashCode();
      } else {
         var1 = 0;
      }

      var6 = this.c;
      if(var6 != null) {
         var2 = var6.hashCode();
      }

      return (var1 + var3 * 31) * 31 + var2;
   }

   public String toString() {
      return "AverageSpendingHistory(total=" + this.a + ", averageAmount=" + this.b + ", totalAmount=" + this.c + ")";
   }
}
