package co.uk.getmondo.transaction.details.c;

import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u000b\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J\t\u0010\r\u001a\u00020\u0003HÆ\u0003J\t\u0010\u000e\u001a\u00020\u0003HÆ\u0003J\t\u0010\u000f\u001a\u00020\u0006HÆ\u0003J'\u0010\u0010\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u0006HÆ\u0001J\u0013\u0010\u0011\u001a\u00020\u00122\b\u0010\u0013\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0014\u001a\u00020\u0015HÖ\u0001J\t\u0010\u0016\u001a\u00020\u0017HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\t¨\u0006\u0018"},
   d2 = {"Lco/uk/getmondo/transaction/details/model/SpendingBehaviour;", "", "totalSpent", "Lco/uk/getmondo/model/Amount;", "averageSpend", "noOfTransactions", "", "(Lco/uk/getmondo/model/Amount;Lco/uk/getmondo/model/Amount;J)V", "getAverageSpend", "()Lco/uk/getmondo/model/Amount;", "getNoOfTransactions", "()J", "getTotalSpent", "component1", "component2", "component3", "copy", "equals", "", "other", "hashCode", "", "toString", "", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class e {
   private final co.uk.getmondo.d.c a;
   private final co.uk.getmondo.d.c b;
   private final long c;

   public e(co.uk.getmondo.d.c var1, co.uk.getmondo.d.c var2, long var3) {
      l.b(var1, "totalSpent");
      l.b(var2, "averageSpend");
      super();
      this.a = var1;
      this.b = var2;
      this.c = var3;
   }

   public final co.uk.getmondo.d.c a() {
      return this.a;
   }

   public final co.uk.getmondo.d.c b() {
      return this.b;
   }

   public final long c() {
      return this.c;
   }

   public boolean equals(Object var1) {
      boolean var4 = false;
      boolean var3;
      if(this != var1) {
         var3 = var4;
         if(!(var1 instanceof e)) {
            return var3;
         }

         e var5 = (e)var1;
         var3 = var4;
         if(!l.a(this.a, var5.a)) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.b, var5.b)) {
            return var3;
         }

         boolean var2;
         if(this.c == var5.c) {
            var2 = true;
         } else {
            var2 = false;
         }

         var3 = var4;
         if(!var2) {
            return var3;
         }
      }

      var3 = true;
      return var3;
   }

   public int hashCode() {
      int var2 = 0;
      co.uk.getmondo.d.c var5 = this.a;
      int var1;
      if(var5 != null) {
         var1 = var5.hashCode();
      } else {
         var1 = 0;
      }

      var5 = this.b;
      if(var5 != null) {
         var2 = var5.hashCode();
      }

      long var3 = this.c;
      return (var1 * 31 + var2) * 31 + (int)(var3 ^ var3 >>> 32);
   }

   public String toString() {
      return "SpendingBehaviour(totalSpent=" + this.a + ", averageSpend=" + this.b + ", noOfTransactions=" + this.c + ")";
   }
}
