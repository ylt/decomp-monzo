package co.uk.getmondo.transaction.details.views;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Rect;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.CoordinatorLayout.d;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewTreeObserver.OnPreDrawListener;
import co.uk.getmondo.c;
import co.uk.getmondo.common.k.n;
import co.uk.getmondo.common.k.q;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000b\u0018\u0000 \u001d2\u00020\u0001:\u0002\u001d\u001eB!\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u0018\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\nH\u0002J \u0010\u0016\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0017\u001a\u00020\u00072\u0006\u0010\u0015\u001a\u00020\nH\u0003J\u0010\u0010\u0018\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0002J\b\u0010\u0019\u001a\u00020\u0012H\u0016J\b\u0010\u001a\u001a\u00020\u0012H\u0016J\b\u0010\u001b\u001a\u00020\u0012H\u0002J\b\u0010\u001c\u001a\u00020\u0012H\u0002R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\nX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\nX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\nX\u0082\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u000e\u001a\u00060\u000fR\u00020\u0000X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\nX\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u001f"},
   d2 = {"Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;", "Landroid/support/design/widget/CoordinatorLayout;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "appBarLayoutRect", "Landroid/graphics/Rect;", "childRect", "desiredChildRect", "headerRect", "onPreDrawListener", "Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout$OnPreDrawListener;", "toolbarRect", "getChildRect", "", "child", "Landroid/view/View;", "out", "getDesiredChildRect", "gravity", "offsetChildIfNeeded", "onAttachedToWindow", "onDetachedFromWindow", "updateAvatar", "updateFab", "Companion", "OnPreDrawListener", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class TransactionCoordinatorLayout extends CoordinatorLayout {
   public static final TransactionCoordinatorLayout.a f = new TransactionCoordinatorLayout.a((i)null);
   private static final float m = -((float)n.b(72));
   private final TransactionCoordinatorLayout.b g;
   private final Rect h;
   private final Rect i;
   private final Rect j;
   private final Rect k;
   private final Rect l;
   private HashMap n;

   public TransactionCoordinatorLayout(Context var1, AttributeSet var2) {
      this(var1, var2, 0, 4, (i)null);
   }

   public TransactionCoordinatorLayout(Context var1, AttributeSet var2, int var3) {
      l.b(var1, "context");
      l.b(var2, "attrs");
      super(var1, var2, var3);
      this.g = new TransactionCoordinatorLayout.b();
      this.h = new Rect();
      this.i = new Rect();
      this.j = new Rect();
      this.k = new Rect();
      this.l = new Rect();
   }

   // $FF: synthetic method
   public TransactionCoordinatorLayout(Context var1, AttributeSet var2, int var3, int var4, i var5) {
      if((var4 & 4) != 0) {
         var3 = 0;
      }

      this(var1, var2, var3);
   }

   @SuppressLint({"RtlHardcoded"})
   private final void a(View var1, int var2, Rect var3) {
      LayoutParams var7 = var1.getLayoutParams();
      if(var7 == null) {
         throw new TypeCastException("null cannot be cast to non-null type android.support.design.widget.CoordinatorLayout.LayoutParams");
      } else {
         d var8 = (d)var7;
         int var4 = android.support.v4.view.d.a(var2, this.getLayoutDirection());
         int var5 = var1.getMeasuredWidth();
         int var6 = var1.getMeasuredHeight();
         switch(var4 & 7) {
         case 3:
            var2 = this.h.left;
            break;
         case 4:
         default:
            throw (Throwable)(new AssertionError());
         case 5:
            var2 = this.h.right - var5;
         }

         switch(var4 & 112) {
         case 48:
            var4 = this.h.top - var6 / 2;
            break;
         case 80:
            var4 = this.h.bottom - var6 + var6 / 2;
            break;
         default:
            throw (Throwable)(new AssertionError());
         }

         var4 = Math.max(var4, this.i.bottom - var6 / 2);
         var2 = Math.max(this.getPaddingLeft() + var8.leftMargin, Math.min(var2, this.getWidth() - this.getPaddingRight() - var5 - var8.rightMargin));
         var4 = Math.max(this.getPaddingTop() + var8.topMargin, Math.min(var4, this.getHeight() - this.getPaddingBottom() - var6 - var8.bottomMargin));
         var3.set(var2, var4, var2 + var5, var4 + var6);
      }
   }

   private final void d(View var1, Rect var2) {
      if(!var1.isLayoutRequested() && var1.getVisibility() != 8) {
         var2.set(var1.getLeft(), var1.getTop(), var1.getRight(), var1.getBottom());
      } else {
         var2.set(0, 0, 0, 0);
      }

   }

   private final void e(View var1) {
      int var3 = this.l.left - this.k.left;
      int var2 = this.l.top - this.k.top;
      if(var3 != 0) {
         var1.offsetLeftAndRight(var3);
      }

      if(var2 != 0) {
         var1.offsetTopAndBottom(var2);
      }

   }

   private final void f() {
      VisibilityAwareFloatingActionButton var1 = (VisibilityAwareFloatingActionButton)this.b(c.a.transactionActionFab);
      l.a(var1, "transactionActionFab");
      this.d((View)var1, this.k);
      var1 = (VisibilityAwareFloatingActionButton)this.b(c.a.transactionActionFab);
      l.a(var1, "transactionActionFab");
      this.a((View)var1, 8388693, this.l);
      var1 = (VisibilityAwareFloatingActionButton)this.b(c.a.transactionActionFab);
      l.a(var1, "transactionActionFab");
      this.e((View)var1);
      if(this.l.top <= this.i.bottom) {
         ((VisibilityAwareFloatingActionButton)this.b(c.a.transactionActionFab)).b();
      } else {
         ((VisibilityAwareFloatingActionButton)this.b(c.a.transactionActionFab)).a();
      }

   }

   private final void g() {
      AvatarView var3 = (AvatarView)this.b(c.a.transactionAvatarView);
      l.a(var3, "transactionAvatarView");
      this.d((View)var3, this.k);
      var3 = (AvatarView)this.b(c.a.transactionAvatarView);
      l.a(var3, "transactionAvatarView");
      this.a((View)var3, 8388659, this.l);
      var3 = (AvatarView)this.b(c.a.transactionAvatarView);
      l.a(var3, "transactionAvatarView");
      this.e((View)var3);
      float var1 = Math.min(-((float)((TransactionAppBarLayout)this.b(c.a.transactionAppBarLayout)).getTotalScrollRange()) - f.a(), 0.0F);
      float var2 = -((float)((TransactionAppBarLayout)this.b(c.a.transactionAppBarLayout)).getTotalScrollRange());
      var1 = Math.min(1.0F, ((float)this.j.top - var2) / (var1 - var2));
      ((AvatarView)this.b(c.a.transactionAvatarView)).setAlpha(var1);
   }

   public View b(int var1) {
      if(this.n == null) {
         this.n = new HashMap();
      }

      View var3 = (View)this.n.get(Integer.valueOf(var1));
      View var2 = var3;
      if(var3 == null) {
         var2 = this.findViewById(var1);
         this.n.put(Integer.valueOf(var1), var2);
      }

      return var2;
   }

   public void onAttachedToWindow() {
      super.onAttachedToWindow();
      this.getViewTreeObserver().addOnPreDrawListener((OnPreDrawListener)this.g);
   }

   public void onDetachedFromWindow() {
      super.onDetachedFromWindow();
      this.getViewTreeObserver().removeOnPreDrawListener((OnPreDrawListener)this.g);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0007\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0007"},
      d2 = {"Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout$Companion;", "", "()V", "MIN_FADE_OFFSET", "", "getMIN_FADE_OFFSET", "()F", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(i var1) {
         this();
      }

      private final float a() {
         return TransactionCoordinatorLayout.m;
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\b\u0082\u0004\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0016¨\u0006\u0005"},
      d2 = {"Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout$OnPreDrawListener;", "Landroid/view/ViewTreeObserver$OnPreDrawListener;", "(Lco/uk/getmondo/transaction/details/views/TransactionCoordinatorLayout;)V", "onPreDraw", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   private final class b implements OnPreDrawListener {
      public boolean onPreDraw() {
         TransactionCoordinatorLayout var1 = TransactionCoordinatorLayout.this;
         HeaderView var2 = (HeaderView)TransactionCoordinatorLayout.this.b(c.a.transactionHeaderView);
         l.a(var2, "transactionHeaderView");
         q.a(var1, (View)var2, (Rect)TransactionCoordinatorLayout.this.h);
         var1 = TransactionCoordinatorLayout.this;
         Toolbar var3 = (Toolbar)TransactionCoordinatorLayout.this.b(c.a.toolbar);
         l.a(var3, "toolbar");
         q.a(var1, (View)var3, (Rect)TransactionCoordinatorLayout.this.i);
         var1 = TransactionCoordinatorLayout.this;
         TransactionAppBarLayout var4 = (TransactionAppBarLayout)TransactionCoordinatorLayout.this.b(c.a.transactionAppBarLayout);
         l.a(var4, "transactionAppBarLayout");
         q.a(var1, (View)var4, (Rect)TransactionCoordinatorLayout.this.j);
         TransactionCoordinatorLayout.this.f();
         TransactionCoordinatorLayout.this.g();
         return true;
      }
   }
}
