package co.uk.getmondo.transaction.details.views;

import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import co.uk.getmondo.c;
import co.uk.getmondo.common.ae;
import co.uk.getmondo.feed.search.FeedSearchActivity;
import co.uk.getmondo.transaction.details.b.e;
import co.uk.getmondo.transaction.details.b.h;
import co.uk.getmondo.transaction.details.b.n;
import co.uk.getmondo.transaction.details.b.o;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import kotlin.Metadata;
import kotlin.a.m;
import kotlin.a.x;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B%\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u000e\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fJ\u0010\u0010\r\u001a\u00020\n2\u0006\u0010\u000e\u001a\u00020\u000fH\u0002¨\u0006\u0010"},
   d2 = {"Lco/uk/getmondo/transaction/details/views/ContentView;", "Landroid/widget/LinearLayout;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "bind", "", "content", "Lco/uk/getmondo/transaction/details/base/Content;", "bindYourHistory", "yourHistory", "Lco/uk/getmondo/transaction/details/base/YourHistory;", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class ContentView extends LinearLayout {
   private HashMap a;

   public ContentView(Context var1) {
      this(var1, (AttributeSet)null, 0, 6, (i)null);
   }

   public ContentView(Context var1, AttributeSet var2) {
      this(var1, var2, 0, 4, (i)null);
   }

   public ContentView(Context var1, AttributeSet var2, int var3) {
      l.b(var1, "context");
      super(var1, var2, var3);
      this.setOrientation(1);
      LayoutInflater.from(var1).inflate(2131034422, (ViewGroup)this);
   }

   // $FF: synthetic method
   public ContentView(Context var1, AttributeSet var2, int var3, int var4, i var5) {
      if((var4 & 2) != 0) {
         var2 = (AttributeSet)null;
      }

      if((var4 & 4) != 0) {
         var3 = 0;
      }

      this(var1, var2, var3);
   }

   private final void a(n var1) {
      ae.a((View)((TextView)this.a(c.a.transactionHistoryHeaderTextView)));
      TextView var4 = (TextView)this.a(c.a.transactionHistoryHeaderTextView);
      Resources var3 = this.getResources();
      l.a(var3, "resources");
      var4.setText((CharSequence)var1.a(var3));
      ((LinearLayout)this.a(c.a.transactionHistoryViewGroup)).removeAllViews();
      LayoutInflater var9 = LayoutInflater.from(this.getContext());
      Iterator var10 = m.n((Iterable)var1.a()).iterator();

      while(var10.hasNext()) {
         x var5 = (x)var10.next();
         int var2 = var5.c();
         o var6 = (o)var5.d();
         View var11 = var9.inflate(2131034425, (LinearLayout)this.a(c.a.transactionHistoryViewGroup), false);
         TextView var7 = (TextView)var11.findViewById(2131821708);
         Resources var8 = this.getResources();
         l.a(var8, "resources");
         var7.setText((CharSequence)var6.a(var8));
         Resources var14 = this.getResources();
         l.a(var14, "resources");
         String var15 = var6.b(var14);
         if(var15 == null) {
            ae.b(var11.findViewById(2131821709));
         } else {
            ((TextView)var11.findViewById(2131821709)).setText((CharSequence)var15);
         }

         TextView var16 = (TextView)var11.findViewById(2131821710);
         var14 = this.getResources();
         l.a(var14, "resources");
         var16.setText((CharSequence)var6.c(var14));
         ((LinearLayout)this.a(c.a.transactionHistoryViewGroup)).addView(var11);
         Resources var12 = this.getResources();
         l.a(var12, "resources");
         final String var13 = var1.b(var12);
         if(var13 != null && var2 == 0) {
            var11.setOnClickListener((OnClickListener)(new OnClickListener() {
               public final void onClick(View var1) {
                  ContentView.this.getContext().startActivity(FeedSearchActivity.a(ContentView.this.getContext(), var13));
               }
            }));
         }
      }

   }

   public View a(int var1) {
      if(this.a == null) {
         this.a = new HashMap();
      }

      View var3 = (View)this.a.get(Integer.valueOf(var1));
      View var2 = var3;
      if(var3 == null) {
         var2 = this.findViewById(var1);
         this.a.put(Integer.valueOf(var1), var2);
      }

      return var2;
   }

   public final void a(e var1) {
      l.b(var1, "content");
      final h var4 = var1.a();
      if(var4 != null) {
         ae.a((View)((Button)this.a(c.a.transactionFurtherInformationButton)));
         Button var3 = (Button)this.a(c.a.transactionFurtherInformationButton);
         Resources var5 = this.getResources();
         l.a(var5, "resources");
         var3.setText((CharSequence)var4.a(var5));
         ((Button)this.a(c.a.transactionFurtherInformationButton)).setOnClickListener((OnClickListener)(new OnClickListener() {
            public final void onClick(View var1) {
               h var3 = var4;
               Context var2 = ContentView.this.getContext();
               l.a(var2, "context");
               var3.a(var2);
            }
         }));
      }

      n var6 = var1.b();
      if(var6 != null) {
         boolean var2;
         if(!((Collection)var6.a()).isEmpty()) {
            var2 = true;
         } else {
            var2 = false;
         }

         if(var2) {
            this.a(var6);
         }
      }

   }
}
