package co.uk.getmondo.transaction.details.views;

import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import co.uk.getmondo.c;
import co.uk.getmondo.common.ae;
import co.uk.getmondo.transaction.details.b.f;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B%\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u000e\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f¨\u0006\r"},
   d2 = {"Lco/uk/getmondo/transaction/details/views/FooterView;", "Landroid/widget/LinearLayout;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "bind", "", "footer", "Lco/uk/getmondo/transaction/details/base/Footer;", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class FooterView extends LinearLayout {
   private HashMap a;

   public FooterView(Context var1) {
      this(var1, (AttributeSet)null, 0, 6, (i)null);
   }

   public FooterView(Context var1, AttributeSet var2) {
      this(var1, var2, 0, 4, (i)null);
   }

   public FooterView(Context var1, AttributeSet var2, int var3) {
      l.b(var1, "context");
      super(var1, var2, var3);
      this.setOrientation(1);
      LayoutInflater.from(var1).inflate(2131034423, (ViewGroup)this);
   }

   // $FF: synthetic method
   public FooterView(Context var1, AttributeSet var2, int var3, int var4, i var5) {
      if((var4 & 2) != 0) {
         var2 = (AttributeSet)null;
      }

      if((var4 & 4) != 0) {
         var3 = 0;
      }

      this(var1, var2, var3);
   }

   public View a(int var1) {
      if(this.a == null) {
         this.a = new HashMap();
      }

      View var3 = (View)this.a.get(Integer.valueOf(var1));
      View var2 = var3;
      if(var3 == null) {
         var2 = this.findViewById(var1);
         this.a.put(Integer.valueOf(var1), var2);
      }

      return var2;
   }

   public final void a(f var1) {
      l.b(var1, "footer");
      Resources var2 = this.getResources();
      l.a(var2, "resources");
      String var3 = var1.a(var2);
      if(var3 == null) {
         ae.b((TextView)this.a(c.a.transactionFooterTextView));
      } else {
         ((TextView)this.a(c.a.transactionFooterTextView)).setText((CharSequence)var3);
      }

   }
}
