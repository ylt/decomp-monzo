package co.uk.getmondo.transaction.details.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.bumptech.glide.g;
import com.bumptech.glide.g.b.b;
import com.bumptech.glide.g.b.j;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.c;
import kotlin.d;
import kotlin.n;
import kotlin.d.a.m;
import kotlin.d.b.i;
import kotlin.d.b.w;
import kotlin.d.b.y;
import kotlin.reflect.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B%\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u000e\u0010 \u001a\u00020\u00172\u0006\u0010!\u001a\u00020\"R\u001b\u0010\t\u001a\u00020\n8BX\u0082\u0084\u0002¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000b\u0010\fRL\u0010\u000f\u001a4\u0012\u0013\u0012\u00110\u0011¢\u0006\f\b\u0012\u0012\b\b\u0013\u0012\u0004\b\b(\u0014\u0012\u0013\u0012\u00110\u0015¢\u0006\f\b\u0012\u0012\b\b\u0013\u0012\u0004\b\b(\u0016\u0012\u0004\u0012\u00020\u0017\u0018\u00010\u0010X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0018\u0010\u0019\"\u0004\b\u001a\u0010\u001bR\u001b\u0010\u001c\u001a\u00020\u00078BX\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u001f\u0010\u000e\u001a\u0004\b\u001d\u0010\u001e¨\u0006#"},
   d2 = {"Lco/uk/getmondo/transaction/details/views/AvatarView;", "Landroid/widget/FrameLayout;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "avatarGenerator", "Lco/uk/getmondo/common/ui/AvatarGenerator;", "getAvatarGenerator", "()Lco/uk/getmondo/common/ui/AvatarGenerator;", "avatarGenerator$delegate", "Lkotlin/Lazy;", "backgroundListener", "Lkotlin/Function2;", "Landroid/graphics/drawable/Drawable;", "Lkotlin/ParameterName;", "name", "background", "", "showOverlay", "", "getBackgroundListener", "()Lkotlin/jvm/functions/Function2;", "setBackgroundListener", "(Lkotlin/jvm/functions/Function2;)V", "p2pPlaceholderSize", "getP2pPlaceholderSize", "()I", "p2pPlaceholderSize$delegate", "bind", "header", "Lco/uk/getmondo/transaction/details/base/BaseHeader;", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class AvatarView extends FrameLayout {
   // $FF: synthetic field
   static final l[] a = new l[]{(l)y.a(new w(y.a(AvatarView.class), "avatarGenerator", "getAvatarGenerator()Lco/uk/getmondo/common/ui/AvatarGenerator;")), (l)y.a(new w(y.a(AvatarView.class), "p2pPlaceholderSize", "getP2pPlaceholderSize()I"))};
   private final c b;
   private final c c;
   private m d;
   private HashMap e;

   public AvatarView(Context var1) {
      this(var1, (AttributeSet)null, 0, 6, (i)null);
   }

   public AvatarView(Context var1, AttributeSet var2) {
      this(var1, var2, 0, 4, (i)null);
   }

   public AvatarView(final Context var1, AttributeSet var2, int var3) {
      kotlin.d.b.l.b(var1, "context");
      super(var1, var2, var3);
      this.b = d.a((kotlin.d.a.a)(new kotlin.d.a.a() {
         public final co.uk.getmondo.common.ui.a b() {
            return co.uk.getmondo.common.ui.a.a.a(var1);
         }

         // $FF: synthetic method
         public Object v_() {
            return this.b();
         }
      }));
      this.c = d.a((kotlin.d.a.a)(new kotlin.d.a.a() {
         public final int b() {
            return (int)TypedValue.applyDimension(2, 26.0F, AvatarView.this.getResources().getDisplayMetrics());
         }

         // $FF: synthetic method
         public Object v_() {
            return Integer.valueOf(this.b());
         }
      }));
      LayoutInflater.from(var1).inflate(2131034421, (ViewGroup)this);
   }

   // $FF: synthetic method
   public AvatarView(Context var1, AttributeSet var2, int var3, int var4, i var5) {
      if((var4 & 2) != 0) {
         var2 = (AttributeSet)null;
      }

      if((var4 & 4) != 0) {
         var3 = 0;
      }

      this(var1, var2, var3);
   }

   private final co.uk.getmondo.common.ui.a getAvatarGenerator() {
      c var1 = this.b;
      l var2 = a[0];
      return (co.uk.getmondo.common.ui.a)var1.a();
   }

   private final int getP2pPlaceholderSize() {
      c var1 = this.c;
      l var2 = a[1];
      return ((Number)var1.a()).intValue();
   }

   public View a(int var1) {
      if(this.e == null) {
         this.e = new HashMap();
      }

      View var3 = (View)this.e.get(Integer.valueOf(var1));
      View var2 = var3;
      if(var3 == null) {
         var2 = this.findViewById(var1);
         this.e.put(Integer.valueOf(var1), var2);
      }

      return var2;
   }

   public final void a(co.uk.getmondo.transaction.details.b.d var1) {
      kotlin.d.b.l.b(var1, "header");
      co.uk.getmondo.transaction.details.b.c var5 = var1.j();
      if(!var5.d()) {
         this.a(co.uk.getmondo.c.a.transactionLogoOutlineView).setBackgroundResource(2130837688);
      } else {
         ((ImageView)this.a(co.uk.getmondo.c.a.transactionAvatarImageView)).setClipToOutline(true);
      }

      Context var3 = this.getContext();
      kotlin.d.b.l.a(var3, "context");
      final Integer var10 = var5.a(var3);
      if(var5.a() == null) {
         String var4 = var5.c();
         if(var4 != null) {
            int var2 = this.getAvatarGenerator().b(var4);
            Drawable var6 = co.uk.getmondo.common.ui.a.b.a(this.getAvatarGenerator().a(var4), this.getP2pPlaceholderSize(), (Typeface)null, var5.d(), 2, (Object)null);
            ((ImageView)this.a(co.uk.getmondo.c.a.transactionAvatarImageView)).setImageDrawable(var6);
            m var7 = this.d;
            if(var7 != null) {
               n var8 = (n)var7.a(new ColorDrawable(co.uk.getmondo.common.k.c.a(var2, 0.5F)), Boolean.valueOf(false));
            }
         } else {
            if(var10 != null) {
               m var13 = this.d;
               if(var13 != null) {
                  n var11 = (n)var13.a(new ColorDrawable(var10.intValue()), Boolean.valueOf(false));
               }
            }

            ImageView var12 = (ImageView)this.a(co.uk.getmondo.c.a.transactionAvatarImageView);
            Integer var9 = var5.b();
            if(var9 == null) {
               kotlin.d.b.l.a();
            }

            var12.setImageResource(var9.intValue());
         }
      } else {
         com.bumptech.glide.a var14 = g.b(this.getContext()).a(Uri.parse(var5.a())).h().a();
         if(var5.d()) {
            var14.a((j)((j)(new b((ImageView)this.a(co.uk.getmondo.c.a.transactionAvatarImageView)) {
               public void a(Bitmap var1, com.bumptech.glide.g.a.c var2) {
                  kotlin.d.b.l.b(var1, "bitmap");
                  super.a(var1, var2);
                  android.support.v7.d.b.a(var1).a((android.support.v7.d.b.c)(new android.support.v7.d.b.c() {
                     public final void a(android.support.v7.d.b var1) {
                        int var2 = var1.a(android.support.v4.content.a.c(AvatarView.this.getContext(), 2131689582));
                        m var3;
                        n var4;
                        if(var10 != null) {
                           var3 = AvatarView.this.getBackgroundListener();
                           if(var3 != null) {
                              var4 = (n)var3.a(new ColorDrawable(var10.intValue()), Boolean.valueOf(false));
                           }
                        } else {
                           var3 = AvatarView.this.getBackgroundListener();
                           if(var3 != null) {
                              var4 = (n)var3.a(new ColorDrawable(co.uk.getmondo.common.k.c.a(var2, 0.5F)), Boolean.valueOf(false));
                           }
                        }

                     }
                  }));
               }
            })));
         } else {
            var14.a(com.bumptech.glide.load.engine.b.b).a((j)((j)(new co.uk.getmondo.common.ui.c((ImageView)this.a(co.uk.getmondo.c.a.transactionAvatarImageView)) {
               public void a(Bitmap var1, com.bumptech.glide.g.a.c var2) {
                  kotlin.d.b.l.b(var1, "bitmap");
                  super.a(var1, var2);
                  m var4 = AvatarView.this.getBackgroundListener();
                  if(var4 != null) {
                     n var3 = (n)var4.a(new BitmapDrawable(AvatarView.this.getResources(), var1), Boolean.valueOf(true));
                  }

               }
            })));
         }
      }

   }

   public final m getBackgroundListener() {
      return this.d;
   }

   public final void setBackgroundListener(m var1) {
      this.d = var1;
   }
}
