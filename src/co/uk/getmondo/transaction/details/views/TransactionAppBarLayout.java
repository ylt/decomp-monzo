package co.uk.getmondo.transaction.details.views;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import co.uk.getmondo.c;
import co.uk.getmondo.common.ae;
import co.uk.getmondo.common.k.n;
import co.uk.getmondo.common.k.q;
import co.uk.getmondo.common.ui.ClickInterceptWrapper;
import co.uk.getmondo.transaction.details.b.d;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.d.a.b;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\r\n\u0002\u0010\u0006\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\u0018\u0000 +2\u00020\u0001:\u0001+B\u001b\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0006J\u000e\u0010\u0012\u001a\u00020\t2\u0006\u0010\u0013\u001a\u00020\u0014J\u0006\u0010\u0015\u001a\u00020\tJ\u0010\u0010\u0016\u001a\u00020\t2\b\u0010\u0017\u001a\u0004\u0018\u00010\u0018J\u0006\u0010\u0019\u001a\u00020\tJ\u0006\u0010\u001a\u001a\u00020\tJ\u0018\u0010\u001b\u001a\u00020\t2\u0006\u0010\u001c\u001a\u00020\u00112\u0006\u0010\u001d\u001a\u00020\u0011H\u0014J\u0006\u0010\u001e\u001a\u00020\tJ\u0006\u0010\u001f\u001a\u00020\tJ\u0006\u0010 \u001a\u00020\tJ\u0006\u0010!\u001a\u00020\tJ\u000e\u0010\"\u001a\u00020\t2\u0006\u0010#\u001a\u00020\u0018J\"\u0010$\u001a\u00020\t2\u0006\u0010%\u001a\u00020&2\u0006\u0010'\u001a\u00020&2\b\u0010(\u001a\u0004\u0018\u00010)H\u0002J\u0006\u0010*\u001a\u00020\tR(\u0010\u0007\u001a\u0010\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\t\u0018\u00010\bX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u000b\"\u0004\b\f\u0010\rR\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006,"},
   d2 = {"Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout;", "Landroid/support/design/widget/AppBarLayout;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "mapListener", "Lkotlin/Function1;", "", "getMapListener", "()Lkotlin/jvm/functions/Function1;", "setMapListener", "(Lkotlin/jvm/functions/Function1;)V", "rect", "Landroid/graphics/Rect;", "scrimVisibleHeightTriggerOffset", "", "bind", "header", "Lco/uk/getmondo/transaction/details/base/BaseHeader;", "hideTitle", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onDestroy", "onLowMemory", "onMeasure", "widthMeasureSpec", "heightMeasureSpec", "onPause", "onResume", "onStart", "onStop", "saveInstanceState", "outState", "showCoordinates", "latitude", "", "longitude", "label", "", "showTitle", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class TransactionAppBarLayout extends AppBarLayout {
   public static final TransactionAppBarLayout.a a = new TransactionAppBarLayout.a((i)null);
   private final Rect b;
   private final int c;
   private b d;
   private HashMap e;

   public TransactionAppBarLayout(Context var1) {
      this(var1, (AttributeSet)null, 2, (i)null);
   }

   public TransactionAppBarLayout(Context var1, AttributeSet var2) {
      l.b(var1, "context");
      super(var1, var2);
      this.b = new Rect();
      this.c = n.b(1);
      LayoutInflater.from(var1).inflate(2131034420, (ViewGroup)this);
   }

   // $FF: synthetic method
   public TransactionAppBarLayout(Context var1, AttributeSet var2, int var3, i var4) {
      if((var3 & 2) != 0) {
         var2 = (AttributeSet)null;
      }

      this(var1, var2);
   }

   private final void a(final double var1, final double var3, final String var5) {
      ((MapView)this.b(c.a.map)).getMapAsync((OnMapReadyCallback)(new OnMapReadyCallback() {
         public final void onMapReady(GoogleMap var1x) {
            LatLng var2 = new LatLng(var1, var3);
            var1x.getUiSettings().setMapToolbarEnabled(false);
            var1x.setPadding(0, 0, 0, 0);
            var1x.moveCamera(CameraUpdateFactory.newLatLng(var2));
            var1x.addMarker((new MarkerOptions()).position(var2).title(var5));
            var1x.setPadding(TransactionAppBarLayout.this.getResources().getDimensionPixelSize(2131427611), 0, 0, 0);
         }
      }));
      ae.b((CollapsingToolbarLayout)this.b(c.a.collapsingToolbar), 2131427610);
      ((MapView)this.b(c.a.map)).setVisibility(0);
      ((ImageView)this.b(c.a.backgroundImage)).setVisibility(8);
   }

   public final void a(Bundle var1) {
      if(var1 != null) {
         var1 = var1.getBundle("KEY_MAP_VIEW_STATE");
      } else {
         var1 = null;
      }

      ((MapView)this.b(c.a.map)).onCreate(var1);
   }

   public final void a(d var1) {
      Object var8 = null;
      l.b(var1, "header");
      TextView var7 = (TextView)this.b(c.a.titleView);
      Resources var6 = this.getResources();
      l.a(var6, "resources");
      var7.setText((CharSequence)var1.c(var6));
      co.uk.getmondo.transaction.details.c.c var10 = var1.h();
      Double var11;
      if(var10 != null) {
         var11 = Double.valueOf(var10.b());
      } else {
         var11 = null;
      }

      co.uk.getmondo.transaction.details.c.c var12 = var1.h();
      Double var13;
      if(var12 != null) {
         var13 = Double.valueOf(var12.c());
      } else {
         var13 = null;
      }

      if(var11 != null && var13 != null) {
         double var4 = var11.doubleValue();
         double var2 = var13.doubleValue();
         var10 = var1.h();
         String var9 = (String)var8;
         if(var10 != null) {
            var9 = var10.a();
         }

         this.a(var4, var2, var9);
      }

      ((ClickInterceptWrapper)this.b(c.a.mapTouchWrapper)).setOnClickListener((OnClickListener)(new OnClickListener() {
         public final void onClick(View var1) {
            b var2 = TransactionAppBarLayout.this.getMapListener();
            if(var2 != null) {
               kotlin.n var3 = (kotlin.n)var2.a(kotlin.n.a);
            }

         }
      }));
   }

   public View b(int var1) {
      if(this.e == null) {
         this.e = new HashMap();
      }

      View var3 = (View)this.e.get(Integer.valueOf(var1));
      View var2 = var3;
      if(var3 == null) {
         var2 = this.findViewById(var1);
         this.e.put(Integer.valueOf(var1), var2);
      }

      return var2;
   }

   public final void b(Bundle var1) {
      l.b(var1, "outState");
      Bundle var2 = new Bundle(var1);
      ((MapView)this.b(c.a.map)).onSaveInstanceState(var2);
      var1.putBundle("KEY_MAP_VIEW_STATE", var2);
   }

   public final void e() {
      if(((TextView)this.b(c.a.titleView)).getVisibility() == 4) {
         ((TextView)this.b(c.a.titleView)).setVisibility(0);
         ((TextView)this.b(c.a.titleView)).setAlpha(0.0F);
         ((TextView)this.b(c.a.titleView)).setTranslationY((float)((TextView)this.b(c.a.titleView)).getHeight() / (float)2);
         ((TextView)this.b(c.a.titleView)).animate().alpha(1.0F).translationY(0.0F).setDuration(200L);
      }

   }

   public final void f() {
      if(((TextView)this.b(c.a.titleView)).getVisibility() == 0) {
         ((TextView)this.b(c.a.titleView)).setVisibility(4);
      }

   }

   public final void g() {
      ((MapView)this.b(c.a.map)).onStart();
   }

   public final b getMapListener() {
      return this.d;
   }

   public final void h() {
      ((MapView)this.b(c.a.map)).onResume();
   }

   public final void i() {
      ((MapView)this.b(c.a.map)).onPause();
   }

   public final void j() {
      ((MapView)this.b(c.a.map)).onStop();
   }

   public final void k() {
      ((MapView)this.b(c.a.map)).onDestroy();
   }

   public final void l() {
      ((MapView)this.b(c.a.map)).onLowMemory();
   }

   protected void onMeasure(int var1, int var2) {
      super.onMeasure(var1, var2);
      ViewParent var3 = this.getParent();
      if(var3 == null) {
         throw new TypeCastException("null cannot be cast to non-null type android.view.ViewGroup");
      } else {
         ViewGroup var4 = (ViewGroup)var3;
         Toolbar var5 = (Toolbar)this.b(c.a.toolbar);
         l.a(var5, "toolbar");
         q.a(var4, (View)var5, this.b);
         ((CollapsingToolbarLayout)this.b(c.a.collapsingToolbar)).setScrimVisibleHeightTrigger(this.b.bottom + this.c);
      }
   }

   public final void setMapListener(b var1) {
      this.d = var1;
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000¨\u0006\u0005"},
      d2 = {"Lco/uk/getmondo/transaction/details/views/TransactionAppBarLayout$Companion;", "", "()V", "KEY_MAP_VIEW_STATE", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(i var1) {
         this();
      }
   }
}
