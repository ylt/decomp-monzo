package co.uk.getmondo.transaction.details.views;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.support.constraint.ConstraintLayout;
import android.text.Spannable;
import android.text.util.Linkify;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.TextView;
import co.uk.getmondo.common.ae;
import co.uk.getmondo.common.k.p;
import co.uk.getmondo.common.ui.AmountView;
import co.uk.getmondo.common.ui.k;
import co.uk.getmondo.d.c;
import co.uk.getmondo.transaction.details.b.d;
import co.uk.getmondo.transaction.details.b.j;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.d.a.b;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B%\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\b\u0010\u0012\u001a\u00020\u0013H\u0002J\u0010\u0010\u0014\u001a\u00020\u00132\u0006\u0010\t\u001a\u00020\nH\u0002J\u000e\u0010\u0015\u001a\u00020\u00132\u0006\u0010\t\u001a\u00020\nJ\u001a\u0010\u0016\u001a\u00020\u00132\b\u0010\u0017\u001a\u0004\u0018\u00010\u00182\u0006\u0010\u0019\u001a\u00020\u001aH\u0002J\u0006\u0010\u001b\u001a\u00020\u0007J \u0010\u001c\u001a\u00020\u0013*\u00020\u001d2\b\u0010\u001e\u001a\u0004\u0018\u00010\u001f2\b\b\u0001\u0010 \u001a\u00020\u0007H\u0002J\u001e\u0010!\u001a\u00020\u0013*\u00020\"2\b\u0010#\u001a\u0004\u0018\u00010\u00182\u0006\u0010$\u001a\u00020\u001aH\u0002J\u0016\u0010%\u001a\u00020\u0013*\u00020\"2\b\u0010#\u001a\u0004\u0018\u00010\u0018H\u0002R\u0010\u0010\t\u001a\u0004\u0018\u00010\nX\u0082\u000e¢\u0006\u0002\n\u0000R(\u0010\f\u001a\u00020\u0007*\u00020\r2\u0006\u0010\u000b\u001a\u00020\u00078B@BX\u0082\u000e¢\u0006\f\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011¨\u0006&"},
   d2 = {"Lco/uk/getmondo/transaction/details/views/HeaderView;", "Landroid/support/constraint/ConstraintLayout;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "header", "Lco/uk/getmondo/transaction/details/base/BaseHeader;", "value", "topMargin", "Landroid/view/View;", "getTopMargin", "(Landroid/view/View;)I", "setTopMargin", "(Landroid/view/View;I)V", "adjustAmountInfoMargin", "", "adjustSubtitleMargin", "bind", "setSubtitleText", "subtitleText", "", "subtitleTextAppearance", "Lco/uk/getmondo/transaction/details/base/TextAppearance;", "titleScrollDistance", "setAmountAndStyleOrHide", "Lco/uk/getmondo/common/ui/AmountView;", "amount", "Lco/uk/getmondo/model/Amount;", "style", "setTextAndAppearanceOrHide", "Landroid/widget/TextView;", "text", "textAppearance", "setTextOrHide", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class HeaderView extends ConstraintLayout {
   private d c;
   private HashMap d;

   public HeaderView(Context var1) {
      this(var1, (AttributeSet)null, 0, 6, (i)null);
   }

   public HeaderView(Context var1, AttributeSet var2) {
      this(var1, var2, 0, 4, (i)null);
   }

   public HeaderView(Context var1, AttributeSet var2, int var3) {
      l.b(var1, "context");
      super(var1, var2, var3);
      LayoutInflater.from(var1).inflate(2131034424, (ViewGroup)this);
   }

   // $FF: synthetic method
   public HeaderView(Context var1, AttributeSet var2, int var3, int var4, i var5) {
      if((var4 & 2) != 0) {
         var2 = (AttributeSet)null;
      }

      if((var4 & 4) != 0) {
         var3 = 0;
      }

      this(var1, var2, var3);
   }

   private final void a(View var1, int var2) {
      LayoutParams var3 = var1.getLayoutParams();
      if(var3 == null) {
         throw new TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
      } else {
         MarginLayoutParams var4 = (MarginLayoutParams)var3;
         var4.topMargin = var2;
         var1.setLayoutParams((LayoutParams)var4);
      }
   }

   private final void a(TextView var1, String var2) {
      if(var2 == null) {
         ae.b((View)var1);
      } else {
         var1.setText((CharSequence)var2);
      }

   }

   private final void a(TextView var1, String var2, j var3) {
      this.a(var1, var2);
      var1.setTextColor(var3.b());
      var1.setTextSize(2, var3.a());
      var1.setTypeface(var1.getTypeface(), var3.c());
   }

   private final void a(AmountView var1, c var2, int var3) {
      if(var2 == null) {
         ae.b((View)var1);
      } else {
         var1.a(var3, var2);
      }

   }

   private final void a(String var1, j var2) {
      this.a((TextView)this.b(co.uk.getmondo.c.a.transactionSubtitleTextView), var1, var2);
      if(Linkify.addLinks((TextView)this.b(co.uk.getmondo.c.a.transactionSubtitleTextView), 1)) {
         CharSequence var4 = ((TextView)this.b(co.uk.getmondo.c.a.transactionSubtitleTextView)).getText();
         if(var4 == null) {
            throw new TypeCastException("null cannot be cast to non-null type android.text.Spannable");
         }

         Spannable var6 = p.a((Spannable)var4);
         if(var1 == null) {
            l.a();
         }

         int var3 = var1.length();
         Typeface var5 = Typeface.create(var2.d(), var2.c());
         l.a(var5, "typeface");
         var6.setSpan(new k(var5), 0, var3, 33);
         ((TextView)this.b(co.uk.getmondo.c.a.transactionSubtitleTextView)).setText((CharSequence)var6);
      }

   }

   private final void b(d var1) {
      if(var1 instanceof co.uk.getmondo.transaction.details.d.f.a) {
         this.a((TextView)this.b(co.uk.getmondo.c.a.transactionSubtitleTextView), 0);
      }

   }

   private final void d() {
      if(((AmountView)this.b(co.uk.getmondo.c.a.transactionAmountView)).getVisibility() == 8) {
         this.a((TextView)this.b(co.uk.getmondo.c.a.transactionLocalAmountTextView), this.getResources().getDimensionPixelSize(2131427603));
      }

   }

   public final void a(d var1) {
      l.b(var1, "header");
      this.c = var1;
      Resources var2 = this.getResources();
      l.a(var2, "resources");
      String var8 = var1.c(var2);
      Context var3 = this.getContext();
      l.a(var3, "context");
      j var10 = var1.a(var3);
      Resources var4 = this.getResources();
      l.a(var4, "resources");
      String var6 = var1.a(var4);
      Context var12 = this.getContext();
      l.a(var12, "context");
      j var13 = var1.b(var12);
      String var5;
      j var7;
      if(var1.g()) {
         var7 = var13;
         var5 = var8;
         var13 = var10;
      } else {
         var5 = var6;
         var7 = var10;
         var6 = var8;
      }

      this.a((TextView)this.b(co.uk.getmondo.c.a.transactionTitleTextView), var6, var7);
      this.a(var5, var13);
      ((TextView)this.b(co.uk.getmondo.c.a.transactionDateTextView)).setText((CharSequence)var1.a());
      this.a((AmountView)this.b(co.uk.getmondo.c.a.transactionAmountView), var1.b(), var1.f());
      this.a((TextView)this.b(co.uk.getmondo.c.a.transactionLocalAmountTextView), var1.e());
      if(var1.c()) {
         ae.a((View)((TextView)this.b(co.uk.getmondo.c.a.transactionDeclinedTextView)));
      } else {
         ae.b((TextView)this.b(co.uk.getmondo.c.a.transactionDeclinedTextView));
      }

      TextView var9 = (TextView)this.b(co.uk.getmondo.c.a.transactionExtraInformationTextView);
      Resources var11 = this.getResources();
      l.a(var11, "resources");
      this.a(var9, var1.b(var11));
      this.a((TextView)this.b(co.uk.getmondo.c.a.transactionDeclinedExtraInformationTextView), var1.d());
      this.b(var1);
      this.d();
   }

   public View b(int var1) {
      if(this.d == null) {
         this.d = new HashMap();
      }

      View var3 = (View)this.d.get(Integer.valueOf(var1));
      View var2 = var3;
      if(var3 == null) {
         var2 = this.findViewById(var1);
         this.d.put(Integer.valueOf(var1), var2);
      }

      return var2;
   }

   public final int c() {
      d var3 = this.c;
      int var1;
      if(var3 != null) {
         boolean var2 = var3.g();
         b var4 = (b)null.a;
         if(var2) {
            var1 = ((Number)var4.a((TextView)this.b(co.uk.getmondo.c.a.transactionSubtitleTextView))).intValue();
         } else {
            var1 = ((Number)var4.a((TextView)this.b(co.uk.getmondo.c.a.transactionTitleTextView))).intValue();
         }
      } else {
         var1 = 0;
      }

      return var1;
   }
}
