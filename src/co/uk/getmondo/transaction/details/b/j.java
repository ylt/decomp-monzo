package co.uk.getmondo.transaction.details.b;

import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0007\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001B+\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\b¢\u0006\u0002\u0010\tR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0013\u0010\u0007\u001a\u0004\u0018\u00010\b¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0011\u0010\u0006\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u000b\u0082\u0001\u0004\u0011\u0012\u0013\u0014¨\u0006\u0015"},
   d2 = {"Lco/uk/getmondo/transaction/details/base/TextAppearance;", "", "size", "", "colour", "", "style", "fontFamily", "", "(FIILjava/lang/String;)V", "getColour", "()I", "getFontFamily", "()Ljava/lang/String;", "getSize", "()F", "getStyle", "Lco/uk/getmondo/transaction/details/base/TitleTextAppearance;", "Lco/uk/getmondo/transaction/details/base/AddressTextAppearance;", "Lco/uk/getmondo/transaction/details/base/SubTitleTextAppearance;", "Lco/uk/getmondo/transaction/details/base/UrlTextAppearance;", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public abstract class j {
   private final float a;
   private final int b;
   private final int c;
   private final String d;

   private j(float var1, int var2, int var3, String var4) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
      this.d = var4;
   }

   // $FF: synthetic method
   j(float var1, int var2, int var3, String var4, int var5, kotlin.d.b.i var6) {
      if((var5 & 8) != 0) {
         var4 = (String)null;
      }

      this(var1, var2, var3, var4);
   }

   // $FF: synthetic method
   public j(float var1, int var2, int var3, String var4, kotlin.d.b.i var5) {
      this(var1, var2, var3, var4);
   }

   public final float a() {
      return this.a;
   }

   public final int b() {
      return this.b;
   }

   public final int c() {
      return this.c;
   }

   public final String d() {
      return this.d;
   }
}
