package co.uk.getmondo.transaction.details.a;

import android.content.Context;
import android.content.res.Resources;
import co.uk.getmondo.common.k.o;
import co.uk.getmondo.common.k.p;
import co.uk.getmondo.d.aj;
import co.uk.getmondo.payments.send.bank.payment.BankPaymentDetailsActivity;
import kotlin.Metadata;
import kotlin.d.b.l;
import kotlin.h.j;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0016J\b\u0010\u000b\u001a\u00020\fH\u0016J\u0012\u0010\r\u001a\u0004\u0018\u00010\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\u0010\u0010\u0011\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0012"},
   d2 = {"Lco/uk/getmondo/transaction/details/actions/SendMoneyFasterPaymentAction;", "Lco/uk/getmondo/transaction/details/base/Action;", "transaction", "Lco/uk/getmondo/model/Transaction;", "(Lco/uk/getmondo/model/Transaction;)V", "bankDetails", "Lco/uk/getmondo/payments/send/data/model/BankDetails;", "action", "", "activity", "Landroid/support/v7/app/AppCompatActivity;", "placeholderIcon", "", "subtitle", "", "resources", "Landroid/content/res/Resources;", "title", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class e implements co.uk.getmondo.transaction.details.b.a {
   private final co.uk.getmondo.payments.send.data.a.a a;

   public e(aj var1) {
      l.b(var1, "transaction");
      super();
      co.uk.getmondo.payments.send.data.a.a var2 = var1.C();
      if(var2 == null) {
         l.a();
      }

      this.a = var2;
   }

   public int a() {
      return 2130837764;
   }

   public String a(Resources var1) {
      l.b(var1, "resources");
      CharSequence var3 = (CharSequence)this.a.a();
      boolean var2;
      if(var3 != null && !j.a(var3)) {
         var2 = false;
      } else {
         var2 = true;
      }

      String var4;
      if(var2) {
         var4 = var1.getString(2131362829);
         l.a(var4, "resources.getString(R.st…g.transaction_send_money)");
      } else {
         var4 = var1.getString(2131362830, new Object[]{p.b(this.a.a())});
         l.a(var4, "resources.getString(R.st…stWord(bankDetails.name))");
      }

      return var4;
   }

   public void a(android.support.v7.app.e var1) {
      l.b(var1, "activity");
      co.uk.getmondo.payments.send.data.a.b var2 = new co.uk.getmondo.payments.send.data.a.b(this.a.a(), this.a.b(), this.a.c());
      var1.startActivity(BankPaymentDetailsActivity.a((Context)var1, var2));
   }

   public String b() {
      return co.uk.getmondo.transaction.details.b.a.a.a(this);
   }

   public String b(Resources var1) {
      l.b(var1, "resources");
      return o.a(this.a.b()) + "・" + this.a.c();
   }
}
