package co.uk.getmondo.transaction.change_category;

import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;

public class ChangeCategoryDialogFragment_ViewBinding implements Unbinder {
   private ChangeCategoryDialogFragment a;
   private View b;
   private View c;
   private View d;
   private View e;
   private View f;
   private View g;
   private View h;
   private View i;
   private View j;
   private View k;

   public ChangeCategoryDialogFragment_ViewBinding(final ChangeCategoryDialogFragment var1, View var2) {
      this.a = var1;
      View var3 = Utils.findRequiredView(var2, 2131821245, "method 'onCategoryClicked'");
      this.b = var3;
      var3.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View var1x) {
            var1.onCategoryClicked((ViewGroup)Utils.castParam(var1x, "doClick", 0, "onCategoryClicked", 0, ViewGroup.class));
         }
      });
      var3 = Utils.findRequiredView(var2, 2131821249, "method 'onCategoryClicked'");
      this.c = var3;
      var3.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View var1x) {
            var1.onCategoryClicked((ViewGroup)Utils.castParam(var1x, "doClick", 0, "onCategoryClicked", 0, ViewGroup.class));
         }
      });
      var3 = Utils.findRequiredView(var2, 2131821246, "method 'onCategoryClicked'");
      this.d = var3;
      var3.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View var1x) {
            var1.onCategoryClicked((ViewGroup)Utils.castParam(var1x, "doClick", 0, "onCategoryClicked", 0, ViewGroup.class));
         }
      });
      var3 = Utils.findRequiredView(var2, 2131821250, "method 'onCategoryClicked'");
      this.e = var3;
      var3.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View var1x) {
            var1.onCategoryClicked((ViewGroup)Utils.castParam(var1x, "doClick", 0, "onCategoryClicked", 0, ViewGroup.class));
         }
      });
      var3 = Utils.findRequiredView(var2, 2131821247, "method 'onCategoryClicked'");
      this.f = var3;
      var3.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View var1x) {
            var1.onCategoryClicked((ViewGroup)Utils.castParam(var1x, "doClick", 0, "onCategoryClicked", 0, ViewGroup.class));
         }
      });
      var3 = Utils.findRequiredView(var2, 2131821251, "method 'onCategoryClicked'");
      this.g = var3;
      var3.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View var1x) {
            var1.onCategoryClicked((ViewGroup)Utils.castParam(var1x, "doClick", 0, "onCategoryClicked", 0, ViewGroup.class));
         }
      });
      var3 = Utils.findRequiredView(var2, 2131821244, "method 'onCategoryClicked'");
      this.h = var3;
      var3.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View var1x) {
            var1.onCategoryClicked((ViewGroup)Utils.castParam(var1x, "doClick", 0, "onCategoryClicked", 0, ViewGroup.class));
         }
      });
      var3 = Utils.findRequiredView(var2, 2131821252, "method 'onCategoryClicked'");
      this.i = var3;
      var3.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View var1x) {
            var1.onCategoryClicked((ViewGroup)Utils.castParam(var1x, "doClick", 0, "onCategoryClicked", 0, ViewGroup.class));
         }
      });
      var3 = Utils.findRequiredView(var2, 2131821248, "method 'onCategoryClicked'");
      this.j = var3;
      var3.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View var1x) {
            var1.onCategoryClicked((ViewGroup)Utils.castParam(var1x, "doClick", 0, "onCategoryClicked", 0, ViewGroup.class));
         }
      });
      var2 = Utils.findRequiredView(var2, 2131821253, "method 'onCategoryClicked'");
      this.k = var2;
      var2.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View var1x) {
            var1.onCategoryClicked((ViewGroup)Utils.castParam(var1x, "doClick", 0, "onCategoryClicked", 0, ViewGroup.class));
         }
      });
   }

   public void unbind() {
      if(this.a == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         this.b.setOnClickListener((OnClickListener)null);
         this.b = null;
         this.c.setOnClickListener((OnClickListener)null);
         this.c = null;
         this.d.setOnClickListener((OnClickListener)null);
         this.d = null;
         this.e.setOnClickListener((OnClickListener)null);
         this.e = null;
         this.f.setOnClickListener((OnClickListener)null);
         this.f = null;
         this.g.setOnClickListener((OnClickListener)null);
         this.g = null;
         this.h.setOnClickListener((OnClickListener)null);
         this.h = null;
         this.i.setOnClickListener((OnClickListener)null);
         this.i = null;
         this.j.setOnClickListener((OnClickListener)null);
         this.j = null;
         this.k.setOnClickListener((OnClickListener)null);
         this.k = null;
      }
   }
}
