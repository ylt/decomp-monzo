package co.uk.getmondo.transaction.change_category;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import co.uk.getmondo.d.h;

public class ChangeCategoryDialogFragment extends DialogFragment {
   private Unbinder a;
   private ChangeCategoryDialogFragment.a b;
   private ChangeCategoryDialogFragment.b c;

   public static ChangeCategoryDialogFragment a(String var0) {
      ChangeCategoryDialogFragment var2 = new ChangeCategoryDialogFragment();
      Bundle var1 = new Bundle();
      var1.putString("KEY_TRANSACTION_ID", var0);
      var2.setArguments(var1);
      return var2;
   }

   public void a(ChangeCategoryDialogFragment.a var1) {
      this.b = var1;
   }

   public void a(ChangeCategoryDialogFragment.b var1) {
      this.c = var1;
   }

   @OnClick({2131821245, 2131821249, 2131821246, 2131821250, 2131821247, 2131821251, 2131821244, 2131821252, 2131821248, 2131821253})
   void onCategoryClicked(ViewGroup var1) {
      switch(var1.getId()) {
      case 2131821244:
         this.b.a(h.SHOPPING);
         break;
      case 2131821245:
         this.b.a(h.TRANSPORT);
         break;
      case 2131821246:
         this.b.a(h.EATING_OUT);
         break;
      case 2131821247:
         this.b.a(h.BILLS);
         break;
      case 2131821248:
         this.b.a(h.GENERAL);
         break;
      case 2131821249:
         this.b.a(h.GROCERIES);
         break;
      case 2131821250:
         this.b.a(h.CASH);
         break;
      case 2131821251:
         this.b.a(h.ENTERTAINMENT);
         break;
      case 2131821252:
         this.b.a(h.HOLIDAYS);
         break;
      case 2131821253:
         this.b.a(h.EXPENSES);
      }

      this.getDialog().dismiss();
   }

   public Dialog onCreateDialog(Bundle var1) {
      View var2 = LayoutInflater.from(this.getActivity()).inflate(2131034259, (ViewGroup)null, false);
      this.a = ButterKnife.bind(this, (View)var2);
      return (new Builder(this.getActivity())).setView(var2).create();
   }

   public void onDestroyView() {
      this.a.unbind();
      super.onDestroyView();
   }

   public void onDismiss(DialogInterface var1) {
      if(this.c != null) {
         this.c.a();
      }

      super.onDismiss(var1);
   }

   public interface a {
      void a(h var1);
   }

   public interface b {
      void a();
   }
}
