package co.uk.getmondo.monzo.me.request;

// $FF: synthetic class
final class c implements io.reactivex.c.g {
   private final b a;
   private final b.a b;
   private final String c;

   private c(b var1, b.a var2, String var3) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
   }

   public static io.reactivex.c.g a(b var0, b.a var1, String var2) {
      return new c(var0, var1, var2);
   }

   public void a(Object var1) {
      b.a(this.a, this.b, this.c, var1);
   }
}
