package co.uk.getmondo.monzo.me.request;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class RequestMoneyFragment_ViewBinding implements Unbinder {
   private RequestMoneyFragment a;

   public RequestMoneyFragment_ViewBinding(RequestMoneyFragment var1, View var2) {
      this.a = var1;
      var1.iconImageView = (ImageView)Utils.findRequiredViewAsType(var2, 2131821356, "field 'iconImageView'", ImageView.class);
      var1.nameTextView = (TextView)Utils.findRequiredViewAsType(var2, 2131821357, "field 'nameTextView'", TextView.class);
      var1.linkTextView = (TextView)Utils.findRequiredViewAsType(var2, 2131821358, "field 'linkTextView'", TextView.class);
      var1.shareButton = (Button)Utils.findRequiredViewAsType(var2, 2131821359, "field 'shareButton'", Button.class);
      var1.customiseAmountButton = (Button)Utils.findRequiredViewAsType(var2, 2131821360, "field 'customiseAmountButton'", Button.class);
   }

   public void unbind() {
      RequestMoneyFragment var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.iconImageView = null;
         var1.nameTextView = null;
         var1.linkTextView = null;
         var1.shareButton = null;
         var1.customiseAmountButton = null;
      }
   }
}
