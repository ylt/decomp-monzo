package co.uk.getmondo.monzo.me.request;

import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.settings.k;
import io.reactivex.n;

class b extends co.uk.getmondo.common.ui.b {
   private final co.uk.getmondo.common.a c;
   private final co.uk.getmondo.payments.send.data.h d;
   private final co.uk.getmondo.common.accounts.d e;
   private final co.uk.getmondo.monzo.me.a f;
   private final k g;

   b(co.uk.getmondo.common.a var1, co.uk.getmondo.payments.send.data.h var2, co.uk.getmondo.common.accounts.d var3, co.uk.getmondo.monzo.me.a var4, k var5) {
      this.c = var1;
      this.d = var2;
      this.e = var3;
      this.f = var4;
      this.g = var5;
   }

   // $FF: synthetic method
   static void a(b.a var0, Boolean var1) throws Exception {
      var0.g();
   }

   // $FF: synthetic method
   static void a(b.a var0, Object var1) throws Exception {
      var0.g();
   }

   // $FF: synthetic method
   static void a(b var0, b.a var1, String var2, Object var3) throws Exception {
      var0.c.a(Impression.U());
      var1.a(var2);
   }

   // $FF: synthetic method
   static boolean a(b var0, Boolean var1) throws Exception {
      boolean var2;
      if(var1.booleanValue() && !var0.g.c()) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   // $FF: synthetic method
   static void b(b.a var0, Object var1) throws Exception {
      var0.f();
   }

   public void a(b.a var1) {
      super.a((co.uk.getmondo.common.ui.f)var1);
      this.c.a(Impression.T());
      String var3 = this.d.c();
      String var2 = String.format("%s/%s", new Object[]{"monzo.me", var3});
      var3 = this.f.a(var3, (String)null, (String)null);
      var1.a(this.e.b().d().a(), var2);
      this.a((io.reactivex.b.b)var1.c().subscribe(c.a(this, var1, var3)));
      this.a((io.reactivex.b.b)var1.d().subscribe(d.a(var1)));
      this.a((io.reactivex.b.b)var1.b().subscribe(e.a(var1)));
      this.a((io.reactivex.b.b)var1.e().filter(f.a(this)).subscribe(g.a(var1)));
   }

   interface a extends co.uk.getmondo.common.ui.f {
      void a(String var1);

      void a(String var1, String var2);

      n b();

      n c();

      n d();

      n e();

      void f();

      void g();
   }
}
