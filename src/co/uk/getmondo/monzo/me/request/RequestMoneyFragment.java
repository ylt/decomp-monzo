package co.uk.getmondo.monzo.me.request;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.common.k.j;
import co.uk.getmondo.monzo.me.customise.CustomiseMonzoMeLinkActivity;
import co.uk.getmondo.monzo.me.onboarding.MonzoMeOnboardingActivity;
import io.reactivex.n;

public class RequestMoneyFragment extends co.uk.getmondo.common.f.a implements b.a {
   b a;
   private final io.reactivex.i.a c = io.reactivex.i.a.a();
   @BindView(2131821360)
   Button customiseAmountButton;
   private final io.reactivex.i.a d = io.reactivex.i.a.a();
   private co.uk.getmondo.common.ui.a e;
   private Unbinder f;
   @BindView(2131821356)
   ImageView iconImageView;
   @BindView(2131821358)
   TextView linkTextView;
   @BindView(2131821357)
   TextView nameTextView;
   @BindView(2131821359)
   Button shareButton;

   public static Fragment a() {
      return new RequestMoneyFragment();
   }

   public void a(String var1) {
      String var2 = this.getString(2131362690);
      this.startActivity(j.a(this.getActivity(), var2, var1, co.uk.getmondo.api.model.tracking.a.REQUEST_MONEY));
   }

   public void a(String var1, String var2) {
      this.nameTextView.setText(var1);
      this.linkTextView.setText(var2);
      int var3 = (int)TypedValue.applyDimension(2, 28.0F, this.getResources().getDisplayMetrics());
      this.iconImageView.setImageDrawable(this.e.a(var1).a(var3));
   }

   public n b() {
      return this.d;
   }

   public n c() {
      return com.b.a.c.c.a(this.shareButton);
   }

   public n d() {
      return com.b.a.c.c.a(this.customiseAmountButton);
   }

   public n e() {
      return this.c;
   }

   public void f() {
      CustomiseMonzoMeLinkActivity.a(this.getActivity(), (co.uk.getmondo.d.c)null, (String)null, Impression.CustomiseMonzoMeLinkFrom.REQUEST_MONEY);
   }

   public void g() {
      this.startActivity(MonzoMeOnboardingActivity.a((Context)this.getActivity()));
   }

   public void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.setHasOptionsMenu(true);
      this.e = co.uk.getmondo.common.ui.a.a((Context)this.getActivity());
      this.B().a(this);
   }

   public void onCreateOptionsMenu(Menu var1, MenuInflater var2) {
      var2.inflate(2131951621, var1);
      super.onCreateOptionsMenu(var1, var2);
   }

   public View onCreateView(LayoutInflater var1, ViewGroup var2, Bundle var3) {
      return var1.inflate(2131034277, var2, false);
   }

   public void onDestroyView() {
      this.a.b();
      this.f.unbind();
      super.onDestroyView();
   }

   public boolean onOptionsItemSelected(MenuItem var1) {
      boolean var2;
      if(var1.getItemId() == 2131821779) {
         this.d.onNext(co.uk.getmondo.common.b.a.a);
         var2 = true;
      } else {
         var2 = super.onOptionsItemSelected(var1);
      }

      return var2;
   }

   public void onViewCreated(View var1, Bundle var2) {
      super.onViewCreated(var1, var2);
      this.f = ButterKnife.bind(this, (View)var1);
      this.a.a((b.a)this);
   }

   public void setUserVisibleHint(boolean var1) {
      super.setUserVisibleHint(var1);
      this.c.onNext(Boolean.valueOf(var1));
   }
}
