package co.uk.getmondo.monzo.me.customise;

public final class h implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final e b;

   static {
      boolean var0;
      if(!h.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   public h(e var1) {
      if(!a && var1 == null) {
         throw new AssertionError();
      } else {
         this.b = var1;
      }
   }

   public static b.a.b a(e var0) {
      return new h(var0);
   }

   public String a() {
      return this.b.b();
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
