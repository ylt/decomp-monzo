package co.uk.getmondo.monzo.me.customise;

// $FF: synthetic class
final class m implements io.reactivex.c.g {
   private final i a;
   private final i.a b;

   private m(i var1, i.a var2) {
      this.a = var1;
      this.b = var2;
   }

   public static io.reactivex.c.g a(i var0, i.a var1) {
      return new m(var0, var1);
   }

   public void a(Object var1) {
      i.a(this.a, this.b, (android.support.v4.g.j)var1);
   }
}
