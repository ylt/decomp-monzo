package co.uk.getmondo.monzo.me.customise;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.common.ui.AmountInputView;

public class CustomiseMonzoMeLinkActivity extends co.uk.getmondo.common.activities.b implements i.a {
   i a;
   @BindView(2131820878)
   AmountInputView amountInputView;
   @BindView(2131820879)
   EditText notesEditText;
   @BindView(2131820880)
   Button shareButton;

   public static void a(Context var0, co.uk.getmondo.d.c var1, String var2, Impression.CustomiseMonzoMeLinkFrom var3) {
      var0.startActivity(b(var0, var1, var2, var3));
   }

   public static Intent b(Context var0, co.uk.getmondo.d.c var1, String var2, Impression.CustomiseMonzoMeLinkFrom var3) {
      Intent var4 = new Intent(var0, CustomiseMonzoMeLinkActivity.class);
      var4.putExtra("key_notes", var2);
      var4.putExtra("key_amount", var1);
      var4.putExtra("key_entry_point", var3);
      return var4;
   }

   public io.reactivex.n a() {
      return this.amountInputView.a().map(a.a());
   }

   public void a(int var1, int var2) {
      this.amountInputView.setError(this.getString(2131362470, new Object[]{Integer.valueOf(var1), Integer.valueOf(var2)}));
   }

   public void a(co.uk.getmondo.d.c var1, String var2) {
      if(var1 != null) {
         this.amountInputView.setDefaultAmount(var1);
      }

      this.notesEditText.setText(var2);
   }

   public void a(String var1) {
      this.startActivity(co.uk.getmondo.common.k.j.a(this, this.getString(2131362690), var1, co.uk.getmondo.api.model.tracking.a.REQUEST_MONEY_CUSTOMISE));
   }

   public void a(boolean var1) {
      this.shareButton.setEnabled(var1);
   }

   public io.reactivex.n b() {
      return com.b.a.d.e.c(this.notesEditText).map(b.a());
   }

   public io.reactivex.n c() {
      return com.b.a.c.c.a(this.shareButton);
   }

   public void d() {
      this.amountInputView.setError(this.getString(2131362471));
   }

   public void e() {
      this.amountInputView.setError("");
   }

   protected void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.setContentView(2131034160);
      ButterKnife.bind((Activity)this);
      co.uk.getmondo.d.c var2 = (co.uk.getmondo.d.c)this.getIntent().getParcelableExtra("key_amount");
      String var3 = this.getIntent().getStringExtra("key_notes");
      Impression.CustomiseMonzoMeLinkFrom var4 = (Impression.CustomiseMonzoMeLinkFrom)this.getIntent().getSerializableExtra("key_entry_point");
      this.l().a(new e(var2, var3, var4)).a(this);
      this.a.a((i.a)this);
   }

   protected void onDestroy() {
      this.a.b();
      super.onDestroy();
   }
}
