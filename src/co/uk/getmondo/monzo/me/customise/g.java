package co.uk.getmondo.monzo.me.customise;

import co.uk.getmondo.api.model.tracking.Impression;

public final class g implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final e b;

   static {
      boolean var0;
      if(!g.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   public g(e var1) {
      if(!a && var1 == null) {
         throw new AssertionError();
      } else {
         this.b = var1;
      }
   }

   public static b.a.b a(e var0) {
      return new g(var0);
   }

   public Impression.CustomiseMonzoMeLinkFrom a() {
      return (Impression.CustomiseMonzoMeLinkFrom)b.a.d.a(this.b.c(), "Cannot return null from a non-@Nullable @Provides method");
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
