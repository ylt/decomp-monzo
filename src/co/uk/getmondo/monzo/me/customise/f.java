package co.uk.getmondo.monzo.me.customise;

public final class f implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final e b;

   static {
      boolean var0;
      if(!f.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   public f(e var1) {
      if(!a && var1 == null) {
         throw new AssertionError();
      } else {
         this.b = var1;
      }
   }

   public static b.a.b a(e var0) {
      return new f(var0);
   }

   public co.uk.getmondo.d.c a() {
      return this.b.a();
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
