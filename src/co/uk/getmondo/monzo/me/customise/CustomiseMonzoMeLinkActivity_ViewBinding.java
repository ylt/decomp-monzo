package co.uk.getmondo.monzo.me.customise;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import co.uk.getmondo.common.ui.AmountInputView;

public class CustomiseMonzoMeLinkActivity_ViewBinding implements Unbinder {
   private CustomiseMonzoMeLinkActivity a;

   public CustomiseMonzoMeLinkActivity_ViewBinding(CustomiseMonzoMeLinkActivity var1, View var2) {
      this.a = var1;
      var1.amountInputView = (AmountInputView)Utils.findRequiredViewAsType(var2, 2131820878, "field 'amountInputView'", AmountInputView.class);
      var1.notesEditText = (EditText)Utils.findRequiredViewAsType(var2, 2131820879, "field 'notesEditText'", EditText.class);
      var1.shareButton = (Button)Utils.findRequiredViewAsType(var2, 2131820880, "field 'shareButton'", Button.class);
   }

   public void unbind() {
      CustomiseMonzoMeLinkActivity var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.amountInputView = null;
         var1.notesEditText = null;
         var1.shareButton = null;
      }
   }
}
