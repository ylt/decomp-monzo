package co.uk.getmondo.monzo.me.customise;

import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.payments.send.data.p;

class i extends co.uk.getmondo.common.ui.b {
   private final co.uk.getmondo.common.a c;
   private final co.uk.getmondo.payments.send.data.h d;
   private final co.uk.getmondo.d.c e;
   private final String f;
   private final co.uk.getmondo.monzo.me.a g;
   private final Impression.CustomiseMonzoMeLinkFrom h;
   private final p i;

   i(co.uk.getmondo.common.a var1, co.uk.getmondo.payments.send.data.h var2, co.uk.getmondo.d.c var3, String var4, co.uk.getmondo.monzo.me.a var5, Impression.CustomiseMonzoMeLinkFrom var6, p var7) {
      this.c = var1;
      this.d = var2;
      this.e = var3;
      this.f = var4;
      this.g = var5;
      this.h = var6;
      this.i = var7;
   }

   // $FF: synthetic method
   static android.support.v4.g.j a(Object var0, String var1, String var2) throws Exception {
      return android.support.v4.g.j.a(var1, var2);
   }

   // $FF: synthetic method
   static void a(i var0, i.a var1, android.support.v4.g.j var2) throws Exception {
      var0.c.a(Impression.S());
      String var3 = var0.d.c();
      var1.a(var0.g.a(var3, (String)var2.a, (String)var2.b));
   }

   // $FF: synthetic method
   static void b(i var0, i.a var1, android.support.v4.g.j var2) throws Exception {
      var1.a(false);
      String var9 = (String)var2.a;
      String var11 = (String)var2.b;
      if(co.uk.getmondo.common.k.p.d(var9)) {
         var1.e();
         if(co.uk.getmondo.common.k.p.c(var11)) {
            var1.a(true);
         }
      } else {
         co.uk.getmondo.d.p var10 = var0.i.b();
         co.uk.getmondo.d.c var12 = co.uk.getmondo.d.c.a(var9, co.uk.getmondo.common.i.c.a);
         if(var12 != null) {
            long var7 = (long)var10.b();
            long var5 = (long)var10.a();
            double var3 = var12.f();
            if(var5 <= 0L) {
               var1.d();
            } else if(var3 >= (double)var7 && var3 <= (double)var5) {
               var1.e();
               var1.a(true);
            } else {
               var1.a(var10.b(), var10.a());
            }
         }
      }

   }

   public void a(i.a var1) {
      super.a((co.uk.getmondo.common.ui.f)var1);
      this.c.a(Impression.a(this.h));
      var1.a(this.e, this.f);
      this.a((io.reactivex.b.b)io.reactivex.n.combineLatest(var1.a(), var1.b(), j.a()).subscribe(k.a(this, var1)));
      this.a((io.reactivex.b.b)var1.c().withLatestFrom(var1.a(), var1.b(), l.a()).subscribe(m.a(this, var1), n.a()));
   }

   interface a extends co.uk.getmondo.common.ui.f {
      io.reactivex.n a();

      void a(int var1, int var2);

      void a(co.uk.getmondo.d.c var1, String var2);

      void a(String var1);

      void a(boolean var1);

      io.reactivex.n b();

      io.reactivex.n c();

      void d();

      void e();
   }
}
