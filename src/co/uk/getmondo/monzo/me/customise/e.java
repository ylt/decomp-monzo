package co.uk.getmondo.monzo.me.customise;

import co.uk.getmondo.api.model.tracking.Impression;

public class e {
   private co.uk.getmondo.d.c a;
   private final String b;
   private final Impression.CustomiseMonzoMeLinkFrom c;

   e(co.uk.getmondo.d.c var1, String var2, Impression.CustomiseMonzoMeLinkFrom var3) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
   }

   co.uk.getmondo.d.c a() {
      return this.a;
   }

   String b() {
      return this.b;
   }

   Impression.CustomiseMonzoMeLinkFrom c() {
      return this.c;
   }
}
