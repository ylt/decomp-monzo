package co.uk.getmondo.monzo.me.deeplink;

import android.net.Uri;
import co.uk.getmondo.d.ak;
import co.uk.getmondo.payments.send.data.p;
import java.math.BigDecimal;
import java.util.List;

class f extends co.uk.getmondo.common.ui.b {
   private final co.uk.getmondo.common.accounts.d c;
   private final Uri d;
   private final p e;

   f(co.uk.getmondo.common.accounts.d var1, Uri var2, p var3) {
      this.c = var1;
      this.d = var2;
      this.e = var3;
   }

   private co.uk.getmondo.d.c a(String var1) {
      co.uk.getmondo.d.c var4;
      try {
         BigDecimal var2 = new BigDecimal(var1);
         var2 = var2.scaleByPowerOfTen(co.uk.getmondo.common.i.c.a.a());
         var4 = new co.uk.getmondo.d.c(var2.longValue(), co.uk.getmondo.common.i.c.a);
      } catch (Exception var3) {
         var4 = null;
      }

      return var4;
   }

   public void a(f.a var1) {
      super.a((co.uk.getmondo.common.ui.f)var1);
      ak var2 = this.c.b();
      if(this.d != null && var2 != null && var2.a() == ak.a.HAS_ACCOUNT) {
         if(this.e.a() == co.uk.getmondo.payments.send.data.a.d.c) {
            var1.a();
         } else {
            List var5 = this.d.getPathSegments();
            String var3 = (String)var5.get(0);
            String var6;
            if(var5.size() > 1) {
               var6 = (String)var5.get(1);
            } else {
               var6 = "";
            }

            String var4 = this.d.getQueryParameter("d");
            var1.a(var3, this.a(var6), var4);
         }
      } else {
         var1.a(this.d);
      }

   }

   interface a extends co.uk.getmondo.common.ui.f {
      void a();

      void a(Uri var1);

      void a(String var1, co.uk.getmondo.d.c var2, String var3);
   }
}
