package co.uk.getmondo.monzo.me.deeplink;

import android.net.Uri;
import co.uk.getmondo.payments.send.data.p;

public class c {
   private final Uri a;

   c(Uri var1) {
      this.a = var1;
   }

   Uri a() {
      return this.a;
   }

   f a(co.uk.getmondo.common.accounts.d var1, p var2) {
      return new f(var1, this.a, var2);
   }
}
