package co.uk.getmondo.monzo.me.deeplink;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import co.uk.getmondo.main.HomeActivity;

public class MonzoMeActivity extends co.uk.getmondo.common.activities.b implements f.a {
   f a;

   public void a() {
      co.uk.getmondo.common.d.e.a(true).show(this.getFragmentManager(), "tag_error_dialog");
   }

   public void a(Uri var1) {
      Intent var4 = new Intent("android.intent.action.VIEW", var1);
      var4.addFlags(268435456);
      var4.setPackage("com.android.chrome");

      try {
         this.startActivity(var4);
      } catch (ActivityNotFoundException var3) {
         var4.setPackage((String)null);
         this.startActivity(var4);
      }

   }

   public void a(String var1, co.uk.getmondo.d.c var2, String var3) {
      HomeActivity.a(this, var1, var2, var3);
   }

   protected void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.l().a(new c(this.getIntent().getData())).a(this);
      this.a.a((f.a)this);
   }

   protected void onDestroy() {
      this.a.b();
      super.onDestroy();
   }
}
