package co.uk.getmondo.monzo.me.onboarding;

import io.reactivex.c.q;

// $FF: synthetic class
final class b implements q {
   private final MonzoMeOnboardingActivity a;

   private b(MonzoMeOnboardingActivity var1) {
      this.a = var1;
   }

   public static q a(MonzoMeOnboardingActivity var0) {
      return new b(var0);
   }

   public boolean a(Object var1) {
      return MonzoMeOnboardingActivity.a(this.a, var1);
   }
}
