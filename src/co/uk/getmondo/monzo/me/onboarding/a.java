package co.uk.getmondo.monzo.me.onboarding;

import io.reactivex.c.q;

// $FF: synthetic class
final class a implements q {
   private final MonzoMeOnboardingActivity a;

   private a(MonzoMeOnboardingActivity var1) {
      this.a = var1;
   }

   public static q a(MonzoMeOnboardingActivity var0) {
      return new a(var0);
   }

   public boolean a(Object var1) {
      return MonzoMeOnboardingActivity.b(this.a, var1);
   }
}
