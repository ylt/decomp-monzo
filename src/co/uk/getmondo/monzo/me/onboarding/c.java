package co.uk.getmondo.monzo.me.onboarding;

// $FF: synthetic class
final class c implements io.reactivex.c.h {
   private final MonzoMeOnboardingActivity a;

   private c(MonzoMeOnboardingActivity var1) {
      this.a = var1;
   }

   public static io.reactivex.c.h a(MonzoMeOnboardingActivity var0) {
      return new c(var0);
   }

   public Object a(Object var1) {
      return MonzoMeOnboardingActivity.a(this.a, (Integer)var1);
   }
}
