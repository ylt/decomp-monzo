package co.uk.getmondo.monzo.me.onboarding;

import co.uk.getmondo.settings.k;
import io.reactivex.n;

class e extends co.uk.getmondo.common.ui.b {
   private final k c;

   e(k var1) {
      this.c = var1;
   }

   // $FF: synthetic method
   static void a(e.a var0, Object var1) throws Exception {
      var0.a();
   }

   // $FF: synthetic method
   static void b(e.a var0, Object var1) throws Exception {
      var0.b();
   }

   public void a(e.a var1) {
      super.a((co.uk.getmondo.common.ui.f)var1);
      this.c.b(true);
      this.a((io.reactivex.b.b)var1.d().subscribe(f.a(var1)));
      this.a((io.reactivex.b.b)var1.c().subscribe(g.a(var1)));
      n var2 = var1.e();
      var1.getClass();
      this.a((io.reactivex.b.b)var2.subscribe(h.a(var1)));
   }

   interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      void a();

      void a(e.a var1);

      void b();

      n c();

      n d();

      n e();
   }

   public static enum a {
      a,
      b;
   }
}
