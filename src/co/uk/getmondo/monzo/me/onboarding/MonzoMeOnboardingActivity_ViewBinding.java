package co.uk.getmondo.monzo.me.onboarding;

import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import me.relex.circleindicator.CircleIndicator;

public class MonzoMeOnboardingActivity_ViewBinding implements Unbinder {
   private MonzoMeOnboardingActivity a;

   public MonzoMeOnboardingActivity_ViewBinding(MonzoMeOnboardingActivity var1, View var2) {
      this.a = var1;
      var1.viewPager = (ViewPager)Utils.findRequiredViewAsType(var2, 2131821000, "field 'viewPager'", ViewPager.class);
      var1.viewPagerIndicator = (CircleIndicator)Utils.findRequiredViewAsType(var2, 2131821001, "field 'viewPagerIndicator'", CircleIndicator.class);
      var1.actionButton = (Button)Utils.findRequiredViewAsType(var2, 2131821002, "field 'actionButton'", Button.class);
      var1.progress = (ProgressBar)Utils.findRequiredViewAsType(var2, 2131821003, "field 'progress'", ProgressBar.class);
      var1.toolbar = (Toolbar)Utils.findRequiredViewAsType(var2, 2131820798, "field 'toolbar'", Toolbar.class);
   }

   public void unbind() {
      MonzoMeOnboardingActivity var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.viewPager = null;
         var1.viewPagerIndicator = null;
         var1.actionButton = null;
         var1.progress = null;
         var1.toolbar = null;
      }
   }
}
