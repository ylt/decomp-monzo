package co.uk.getmondo.monzo.me.onboarding;

import co.uk.getmondo.settings.k;

public final class i implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final b.a b;
   private final javax.a.a c;

   static {
      boolean var0;
      if(!i.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   public i(b.a var1, javax.a.a var2) {
      if(!a && var1 == null) {
         throw new AssertionError();
      } else {
         this.b = var1;
         if(!a && var2 == null) {
            throw new AssertionError();
         } else {
            this.c = var2;
         }
      }
   }

   public static b.a.b a(b.a var0, javax.a.a var1) {
      return new i(var0, var1);
   }

   public e a() {
      return (e)b.a.c.a(this.b, new e((k)this.c.b()));
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
