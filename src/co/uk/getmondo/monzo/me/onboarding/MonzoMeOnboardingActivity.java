package co.uk.getmondo.monzo.me.onboarding;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.ProgressBar;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.common.pager.GenericPagerAdapter;
import io.reactivex.n;
import me.relex.circleindicator.CircleIndicator;

public class MonzoMeOnboardingActivity extends co.uk.getmondo.common.activities.b implements e.a {
   private static final co.uk.getmondo.common.pager.f[] c = new co.uk.getmondo.common.pager.f[]{new co.uk.getmondo.common.pager.d(2130837868, 2131362473, 2131362472, Impression.c(1)), new co.uk.getmondo.common.pager.d(2130837851, 2131362475, 2131362474, Impression.c(2)), new co.uk.getmondo.common.pager.d(2130837852, 2131362477, 2131362476, Impression.c(3))};
   e a;
   @BindView(2131821002)
   Button actionButton;
   co.uk.getmondo.common.pager.h b;
   private n e;
   @BindView(2131821003)
   ProgressBar progress;
   @BindView(2131820798)
   Toolbar toolbar;
   @BindView(2131821000)
   ViewPager viewPager;
   @BindView(2131821001)
   CircleIndicator viewPagerIndicator;

   public static Intent a(Context var0) {
      return new Intent(var0, MonzoMeOnboardingActivity.class);
   }

   // $FF: synthetic method
   static e.a a(MonzoMeOnboardingActivity var0, Integer var1) throws Exception {
      e.a var2;
      if(var0.f()) {
         var2 = e.a.b;
      } else {
         var2 = e.a.a;
      }

      return var2;
   }

   // $FF: synthetic method
   static boolean a(MonzoMeOnboardingActivity var0, Object var1) throws Exception {
      return var0.f();
   }

   // $FF: synthetic method
   static boolean b(MonzoMeOnboardingActivity var0, Object var1) throws Exception {
      boolean var2;
      if(!var0.f()) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   private boolean f() {
      boolean var1;
      if(this.viewPager.getCurrentItem() == this.viewPager.getAdapter().b() - 1) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public void a() {
      int var1 = this.viewPager.getCurrentItem() + 1;
      if(var1 < this.viewPager.getAdapter().b()) {
         this.viewPager.a(var1, true);
      }

   }

   public void a(e.a var1) {
      int var2 = 0;
      switch(null.a[var1.ordinal()]) {
      case 1:
         var2 = 2131362493;
         break;
      case 2:
         var2 = 2131362374;
      }

      this.actionButton.setText(var2);
   }

   public void b() {
      this.setResult(-1);
      this.finish();
   }

   public n c() {
      return this.e.filter(a.a(this));
   }

   public n d() {
      return this.e.filter(b.a(this));
   }

   public n e() {
      return com.b.a.b.b.a.a.a(this.viewPager).map(c.a(this));
   }

   protected void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.l().a(this);
      this.setContentView(2131034188);
      ButterKnife.bind((Activity)this);
      this.e = com.b.a.c.c.a(this.actionButton).share();
      this.toolbar.setNavigationIcon(2130837837);
      this.setTitle("");
      this.viewPager.setAdapter(new GenericPagerAdapter(c));
      this.viewPagerIndicator.setViewPager(this.viewPager);
      this.b.a(this.viewPager);
      this.a.a((e.a)this);
   }

   protected void onDestroy() {
      this.a.b();
      this.b.a();
      super.onDestroy();
   }
}
