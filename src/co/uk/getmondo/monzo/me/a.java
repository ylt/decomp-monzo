package co.uk.getmondo.monzo.me;

import android.net.Uri.Builder;
import co.uk.getmondo.common.k.p;

public class a {
   public String a(String var1, String var2, String var3) {
      Builder var4 = new Builder();
      var4.scheme("https").authority("monzo.me").appendPath(var1);
      if(p.c(var2)) {
         var4.appendPath(var2);
      }

      if(p.c(var3)) {
         var4.appendQueryParameter("d", var3);
      }

      return var4.build().toString();
   }
}
