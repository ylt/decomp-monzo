package co.uk.getmondo.card;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class CardReplacementActivity_ViewBinding implements Unbinder {
   private CardReplacementActivity a;

   public CardReplacementActivity_ViewBinding(CardReplacementActivity var1, View var2) {
      this.a = var1;
      var1.sendReplacementCardButton = (Button)Utils.findRequiredViewAsType(var2, 2131820835, "field 'sendReplacementCardButton'", Button.class);
      var1.editAddressButton = (Button)Utils.findRequiredViewAsType(var2, 2131820841, "field 'editAddressButton'", Button.class);
      var1.addressTextView = (TextView)Utils.findRequiredViewAsType(var2, 2131820840, "field 'addressTextView'", TextView.class);
   }

   public void unbind() {
      CardReplacementActivity var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.sendReplacementCardButton = null;
         var1.editAddressButton = null;
         var1.addressTextView = null;
      }
   }
}
