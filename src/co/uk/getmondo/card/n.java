package co.uk.getmondo.card;

// $FF: synthetic class
final class n implements io.reactivex.c.h {
   private final j a;
   private final j.a b;

   private n(j var1, j.a var2) {
      this.a = var1;
      this.b = var2;
   }

   public static io.reactivex.c.h a(j var0, j.a var1) {
      return new n(var0, var1);
   }

   public Object a(Object var1) {
      return j.a(this.a, this.b, var1);
   }
}
