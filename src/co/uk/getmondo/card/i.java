package co.uk.getmondo.card;

public final class i implements b.a {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;

   static {
      boolean var0;
      if(!i.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   public i(javax.a.a var1) {
      if(!a && var1 == null) {
         throw new AssertionError();
      } else {
         this.b = var1;
      }
   }

   public static b.a a(javax.a.a var0) {
      return new i(var0);
   }

   public void a(CardReplacementActivity var1) {
      if(var1 == null) {
         throw new NullPointerException("Cannot inject members into a null reference");
      } else {
         var1.a = (j)this.b.b();
      }
   }
}
