package co.uk.getmondo.card;

import kotlin.Metadata;

// $FF: synthetic class
@Metadata(
   bv = {1, 0, 2},
   k = 3,
   mv = {1, 1, 7}
)
public final class f {
   // $FF: synthetic field
   public static final int[] a = new int[co.uk.getmondo.api.model.a.values().length];

   static {
      a[co.uk.getmondo.api.model.a.ACTIVE.ordinal()] = 1;
      a[co.uk.getmondo.api.model.a.INACTIVE.ordinal()] = 2;
      a[co.uk.getmondo.api.model.a.BLOCKED.ordinal()] = 3;
   }
}
