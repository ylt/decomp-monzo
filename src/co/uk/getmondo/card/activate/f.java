package co.uk.getmondo.card.activate;

// $FF: synthetic class
final class f implements io.reactivex.c.h {
   private final e a;
   private final e.a b;

   private f(e var1, e.a var2) {
      this.a = var1;
      this.b = var2;
   }

   public static io.reactivex.c.h a(e var0, e.a var1) {
      return new f(var0, var1);
   }

   public Object a(Object var1) {
      return e.a(this.a, this.b, (String)var1);
   }
}
