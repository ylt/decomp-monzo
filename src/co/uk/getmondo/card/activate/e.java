package co.uk.getmondo.card.activate;

import co.uk.getmondo.api.ApiException;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.d.ak;
import io.reactivex.u;

public class e extends co.uk.getmondo.common.ui.b {
   private final u c;
   private final u d;
   private final co.uk.getmondo.common.accounts.d e;
   private final co.uk.getmondo.common.e.a f;
   private final co.uk.getmondo.card.c g;
   private final co.uk.getmondo.common.a h;
   private final co.uk.getmondo.waitlist.i i;
   private final boolean j;

   e(u var1, u var2, co.uk.getmondo.common.accounts.d var3, co.uk.getmondo.common.e.a var4, co.uk.getmondo.card.c var5, co.uk.getmondo.common.a var6, co.uk.getmondo.waitlist.i var7, boolean var8) {
      this.c = var1;
      this.d = var2;
      this.e = var3;
      this.f = var4;
      this.g = var5;
      this.h = var6;
      this.i = var7;
      this.j = var8;
   }

   // $FF: synthetic method
   static io.reactivex.l a(e var0, e.a var1, String var2) throws Exception {
      return var0.g.b(var2.replace(" ", "")).b(var0.d).a(var0.c).b(l.a(var1)).d(m.a(var0, var1)).a(n.a(var1)).e().f();
   }

   // $FF: synthetic method
   static void a(e.a var0, co.uk.getmondo.d.g var1) throws Exception {
      var0.f();
   }

   // $FF: synthetic method
   static void a(e.a var0, co.uk.getmondo.d.g var1, Throwable var2) throws Exception {
      var0.k();
      var0.h();
   }

   // $FF: synthetic method
   static void a(e.a var0, io.reactivex.b.b var1) throws Exception {
      var0.j();
   }

   // $FF: synthetic method
   static void a(e var0, e.a var1, Throwable var2) throws Exception {
      var0.f.a(var2, var1);
   }

   // $FF: synthetic method
   static void a(e var0, ak var1, co.uk.getmondo.d.g var2) throws Exception {
      ((e.a)var0.a).k();
      co.uk.getmondo.d.a var3 = var1.c().a(true);
      var0.e.a(var3);
      if(var0.j) {
         ((e.a)var0.a).g();
      } else {
         ((e.a)var0.a).f();
      }

   }

   // $FF: synthetic method
   static void a(e var0, co.uk.getmondo.d.g var1) throws Exception {
      var0.i.c();
   }

   // $FF: synthetic method
   static void a(e var0, Throwable var1) throws Exception {
      ((e.a)var0.a).k();
      ((e.a)var0.a).h();
      if(var1 instanceof ApiException) {
         co.uk.getmondo.api.model.b var2 = ((ApiException)var1).e();
         if(var2 != null) {
            p var3 = (p)co.uk.getmondo.common.e.d.a(p.values(), var2.a());
            if(p.a.equals(var3)) {
               ((e.a)var0.a).i();
               return;
            }

            if(var3 != null) {
               ((e.a)var0.a).b(var3.b());
               return;
            }
         }
      }

      var0.f.a(var1, (co.uk.getmondo.common.e.a.a)var0.a);
   }

   public void a(e.a var1) {
      super.a((co.uk.getmondo.common.ui.f)var1);
      co.uk.getmondo.common.a var3 = this.h;
      Impression var2;
      if(this.j) {
         var2 = Impression.I();
      } else {
         var2 = Impression.B();
      }

      var3.a(var2);
      ak var4 = this.e.b();
      if(var4.a() == ak.a.NO_CARD && co.uk.getmondo.common.k.p.c(this.i.b())) {
         var1.a(this.i.b());
         this.a(this.i.b());
      }

      if(var4.c().e()) {
         var1.b();
      } else {
         var1.c();
      }

      this.a((io.reactivex.b.b)var1.d().flatMapMaybe(f.a(this, var1)).subscribe(g.a(var1), h.a()));
   }

   void a(String var1) {
      ak var2 = this.e.b();
      if(!this.j && var2.a() == ak.a.ON_WAITLIST && this.i.a()) {
         this.i.a(var1);
         ((e.a)this.a).e();
      } else {
         ((e.a)this.a).j();
         this.a((io.reactivex.b.b)this.g.a(var1, this.j).c(i.a(this)).b(this.d).a(this.c).a(j.a(this, var2), k.a(this)));
      }

   }

   interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      void a(String var1);

      void b();

      void c();

      io.reactivex.n d();

      void e();

      void f();

      void g();

      void h();

      void i();

      void j();

      void k();
   }
}
