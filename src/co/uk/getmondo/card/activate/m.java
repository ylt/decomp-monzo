package co.uk.getmondo.card.activate;

// $FF: synthetic class
final class m implements io.reactivex.c.g {
   private final e a;
   private final e.a b;

   private m(e var1, e.a var2) {
      this.a = var1;
      this.b = var2;
   }

   public static io.reactivex.c.g a(e var0, e.a var1) {
      return new m(var0, var1);
   }

   public void a(Object var1) {
      e.a(this.a, this.b, (Throwable)var1);
   }
}
