package co.uk.getmondo.card.activate;

import co.uk.getmondo.d.ak;

// $FF: synthetic class
final class j implements io.reactivex.c.g {
   private final e a;
   private final ak b;

   private j(e var1, ak var2) {
      this.a = var1;
      this.b = var2;
   }

   public static io.reactivex.c.g a(e var0, ak var1) {
      return new j(var0, var1);
   }

   public void a(Object var1) {
      e.a(this.a, this.b, (co.uk.getmondo.d.g)var1);
   }
}
