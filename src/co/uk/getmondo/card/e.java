package co.uk.getmondo.card;

import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.d.w;
import io.reactivex.u;
import java.util.List;
import kotlin.Metadata;
import kotlin.TypeCastException;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0004\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001#BS\b\u0007\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\f\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013¢\u0006\u0002\u0010\u0014J\u0010\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0016H\u0002J\u0018\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u00022\u0006\u0010\u001b\u001a\u00020\u001cH\u0002J\u0010\u0010\u001d\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u0002H\u0016J\u0018\u0010\u001e\u001a\u00020\u00192\u0006\u0010\u001f\u001a\u00020 2\u0006\u0010\u001a\u001a\u00020\u0002H\u0002J\u0018\u0010!\u001a\u00020\u00192\u0006\u0010\u001f\u001a\u00020 2\u0006\u0010\u001a\u001a\u00020\u0002H\u0002J\u0018\u0010\"\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u00022\u0006\u0010\u001b\u001a\u00020\u001cH\u0002R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006$"},
   d2 = {"Lco/uk/getmondo/card/CardPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/card/CardPresenter$View;", "uiScheduler", "Lio/reactivex/Scheduler;", "ioScheduler", "accountManager", "Lco/uk/getmondo/common/accounts/AccountManager;", "apiErrorHandler", "Lco/uk/getmondo/common/errors/ApiErrorHandler;", "cardManager", "Lco/uk/getmondo/card/CardManager;", "balanceRepository", "Lco/uk/getmondo/account_balance/BalanceRepository;", "analyticsService", "Lco/uk/getmondo/common/AnalyticsService;", "developerOptionsPreferences", "Lco/uk/getmondo/developer_options/DeveloperOptionsPreferences;", "overdraftManager", "Lco/uk/getmondo/overdraft/data/OverdraftManager;", "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/common/accounts/AccountManager;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/card/CardManager;Lco/uk/getmondo/account_balance/BalanceRepository;Lco/uk/getmondo/common/AnalyticsService;Lco/uk/getmondo/developer_options/DeveloperOptionsPreferences;Lco/uk/getmondo/overdraft/data/OverdraftManager;)V", "formatExpirationDate", "", "expirationDate", "onCard", "", "view", "card", "Lco/uk/getmondo/model/Card;", "register", "setActivateReplacementButtonVisible", "visible", "", "setCardReplacementButtonVisible", "updateUiForCard", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class e extends co.uk.getmondo.common.ui.b {
   private final u c;
   private final u d;
   private final co.uk.getmondo.common.accounts.b e;
   private final co.uk.getmondo.common.e.a f;
   private final c g;
   private final co.uk.getmondo.a.a h;
   private final co.uk.getmondo.common.a i;
   private final co.uk.getmondo.developer_options.a j;
   private final co.uk.getmondo.overdraft.a.c k;

   public e(u var1, u var2, co.uk.getmondo.common.accounts.b var3, co.uk.getmondo.common.e.a var4, c var5, co.uk.getmondo.a.a var6, co.uk.getmondo.common.a var7, co.uk.getmondo.developer_options.a var8, co.uk.getmondo.overdraft.a.c var9) {
      kotlin.d.b.l.b(var1, "uiScheduler");
      kotlin.d.b.l.b(var2, "ioScheduler");
      kotlin.d.b.l.b(var3, "accountManager");
      kotlin.d.b.l.b(var4, "apiErrorHandler");
      kotlin.d.b.l.b(var5, "cardManager");
      kotlin.d.b.l.b(var6, "balanceRepository");
      kotlin.d.b.l.b(var7, "analyticsService");
      kotlin.d.b.l.b(var8, "developerOptionsPreferences");
      kotlin.d.b.l.b(var9, "overdraftManager");
      super();
      this.c = var1;
      this.d = var2;
      this.e = var3;
      this.f = var4;
      this.g = var5;
      this.h = var6;
      this.i = var7;
      this.j = var8;
      this.k = var9;
   }

   private final String a(String var1) {
      List var3 = kotlin.h.j.b((CharSequence)var1, new String[]{"/"}, false, 0, 6, (Object)null);
      String var2 = var1;
      if(var3.size() == 2) {
         var2 = var1;
         if(((String)var3.get(1)).length() == 4) {
            StringBuilder var4 = (new StringBuilder()).append((String)var3.get(0)).append("/");
            var2 = (String)var3.get(1);
            if(var2 == null) {
               throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
            }

            var2 = var2.substring(2, 4);
            kotlin.d.b.l.a(var2, "(this as java.lang.Strin…ing(startIndex, endIndex)");
            var2 = var4.append(var2).toString();
         }
      }

      return var2;
   }

   private final void a(e.a var1, co.uk.getmondo.d.g var2) {
      String var3;
      if(!this.e.b()) {
         var3 = co.uk.getmondo.common.k.p.f(var2.e());
         kotlin.d.b.l.a(var3, "token");
         var1.a(var3);
      }

      var3 = var2.c();
      kotlin.d.b.l.a(var3, "card.expires");
      var1.b(this.a(var3));
      var3 = var2.d();
      kotlin.d.b.l.a(var3, "card.lastDigits");
      var1.c(var3);
      this.b(var1, var2);
   }

   private final void a(boolean var1, e.a var2) {
      if(var1) {
         var2.a_();
      } else {
         var2.r();
      }

   }

   private final void b(e.a var1, co.uk.getmondo.d.g var2) {
      boolean var5 = true;
      boolean var3;
      if(!var2.f()) {
         var3 = true;
      } else {
         var3 = false;
      }

      co.uk.getmondo.api.model.a var6 = var2.g();
      if(var6 != null) {
         switch(f.a[var6.ordinal()]) {
         case 1:
            var1.i();
            var1.j();
            var1.v();
            var3 = false;
            break;
         case 2:
            var1.w();
            var1.h();
            var1.j();
            break;
         case 3:
            var1.w();
            var1.h();
            var1.k();
         }
      }

      boolean var4;
      if(this.e.b() && !this.j.a()) {
         var4 = false;
      } else {
         var4 = true;
      }

      this.a(var2.f(), var1);
      if(!var3 || !var4) {
         var5 = false;
      }

      this.b(var5, var1);
   }

   private final void b(boolean var1, e.a var2) {
      if(var1) {
         var2.l();
      } else {
         var2.m();
      }

   }

   public void a(final e.a var1) {
      kotlin.d.b.l.b(var1, "view");
      super.a((co.uk.getmondo.common.ui.f)var1);
      this.i.a(Impression.Companion.s());
      final boolean var2 = this.e.b();
      String var4 = this.e.c();
      if(var4 == null) {
         kotlin.d.b.l.a();
      }

      io.reactivex.b.a var3;
      io.reactivex.b.b var9;
      io.reactivex.b.b var11;
      io.reactivex.b.a var12;
      if(var2) {
         var3 = this.b;
         var11 = this.k.b(var4).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
            public final void a(w var1x) {
               if(var1x.b()) {
                  var1.a(true, var1x.g());
               } else if(var1x.c()) {
                  var1.a(false, var1x.g());
               } else {
                  var1.x();
               }

            }
         }));
         kotlin.d.b.l.a(var11, "overdraftManager.status(…  }\n                    }");
         this.b = co.uk.getmondo.common.j.f.a(var3, var11);
         var12 = this.b;
         var9 = var1.f().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
            public final void a(kotlin.n var1x) {
               var1.y();
            }
         }));
         kotlin.d.b.l.a(var9, "view.applyForOverdraftCl…iew.openOverdraftTour() }");
         this.b = co.uk.getmondo.common.j.f.a(var12, var9);
         var12 = this.b;
         var9 = var1.g().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
            public final void a(kotlin.n var1x) {
               var1.z();
            }
         }));
         kotlin.d.b.l.a(var9, "view.manageOverdraftClic…enOverdraftManagement() }");
         this.b = co.uk.getmondo.common.j.f.a(var12, var9);
      }

      io.reactivex.b.a var5 = this.b;
      io.reactivex.n var7 = this.h.a(this.e.c()).observeOn(this.c);
      io.reactivex.c.g var6 = (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(co.uk.getmondo.d.b var1x) {
            e.a var2x = var1;
            co.uk.getmondo.d.c var3 = var1x.a();
            kotlin.d.b.l.a(var3, "accountBalance.balance");
            var2x.a(var3, var2);
         }
      });
      kotlin.d.a.b var13 = (kotlin.d.a.b)null.a;
      Object var10 = var13;
      if(var13 != null) {
         var10 = new g(var13);
      }

      var9 = var7.subscribe(var6, (io.reactivex.c.g)var10);
      kotlin.d.b.l.a(var9, "balanceRepository.balanc…ailAccount) }, Timber::e)");
      this.b = co.uk.getmondo.common.j.f.a(var5, var9);
      var12 = this.b;
      var9 = this.g.c().b(this.d).a(this.c).a((io.reactivex.c.a)null.a, (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Throwable var1x) {
            co.uk.getmondo.common.e.a var2 = e.this.f;
            kotlin.d.b.l.a(var1x, "error");
            var2.a(var1x, (co.uk.getmondo.common.e.a.a)var1);
         }
      }));
      kotlin.d.b.l.a(var9, "cardManager.syncCard()\n …view) }\n                )");
      this.b = co.uk.getmondo.common.j.f.a(var12, var9);
      var3 = this.b;
      var11 = this.h.b(this.e.c()).b(this.d).a(this.c).a((io.reactivex.c.a)null.a, (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Throwable var1x) {
            co.uk.getmondo.common.e.a var2 = e.this.f;
            kotlin.d.b.l.a(var1x, "error");
            var2.a(var1x, (co.uk.getmondo.common.e.a.a)var1);
         }
      }));
      kotlin.d.b.l.a(var11, "balanceRepository.refres…ndleError(error, view) })");
      this.b = co.uk.getmondo.common.j.f.a(var3, var11);
      var5 = this.b;
      var7 = this.g.a().observeOn(this.c);
      var6 = (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(co.uk.getmondo.d.g var1x) {
            e var3 = e.this;
            e.a var2 = var1;
            kotlin.d.b.l.a(var1x, "card");
            var3.a(var2, var1x);
         }
      });
      var13 = (kotlin.d.a.b)null.a;
      var10 = var13;
      if(var13 != null) {
         var10 = new g(var13);
      }

      var9 = var7.subscribe(var6, (io.reactivex.c.g)var10);
      kotlin.d.b.l.a(var9, "cardManager.card()\n     …view, card) }, Timber::e)");
      this.b = co.uk.getmondo.common.j.f.a(var5, var9);
      var3 = this.b;
      var11 = var1.e().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n var1x) {
            var1.b(e.this.e.b());
         }
      }));
      kotlin.d.b.l.a(var11, "view.topUpClicks\n       …anager.isRetailAccount) }");
      this.b = co.uk.getmondo.common.j.f.a(var3, var11);
      var3 = this.b;
      var11 = var1.a().flatMapCompletable((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.b a(kotlin.n var1x) {
            kotlin.d.b.l.b(var1x, "it");
            co.uk.getmondo.d.g var3 = e.this.g.b();
            if(var3 == null) {
               kotlin.d.b.l.a();
            }

            co.uk.getmondo.api.model.a var4;
            if(kotlin.d.b.l.a(var3.g(), co.uk.getmondo.api.model.a.ACTIVE)) {
               var4 = co.uk.getmondo.api.model.a.INACTIVE;
            } else {
               var4 = co.uk.getmondo.api.model.a.ACTIVE;
            }

            Impression var2;
            if(kotlin.d.b.l.a(var3.g(), co.uk.getmondo.api.model.a.ACTIVE)) {
               var2 = Impression.Companion.t();
            } else {
               var2 = Impression.Companion.u();
            }

            e.this.i.a(var2);
            c var5 = e.this.g;
            String var6 = var3.a();
            kotlin.d.b.l.a(var6, "card.id");
            return var5.a(var6, var4).a(e.this.c).b(e.this.d).c((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(io.reactivex.b.b var1x) {
                  var1.o();
                  var1.a(false);
               }
            })).b((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(Throwable var1x) {
                  var1.p();
                  var1.a(true);
               }
            })).a((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(Throwable var1x) {
                  co.uk.getmondo.common.e.a var2 = e.this.f;
                  kotlin.d.b.l.a(var1x, "error");
                  if(!var2.a(var1x, (co.uk.getmondo.common.e.a.a)var1)) {
                     var1.b(2131362064);
                  }

               }
            })).b();
         }
      })).c();
      kotlin.d.b.l.a(var11, "view.toggleCardClicks\n  …             .subscribe()");
      this.b = co.uk.getmondo.common.j.f.a(var3, var11);
      var12 = this.b;
      var9 = var1.b().doOnNext((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n var1) {
            e.this.i.a(Impression.Companion.N());
         }
      })).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n var1x) {
            var1.n();
         }
      }));
      kotlin.d.b.l.a(var9, "view.cardReplacementClic…w.openCardReplacement() }");
      this.b = co.uk.getmondo.common.j.f.a(var12, var9);
      var3 = this.b;
      var11 = var1.c().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n var1x) {
            var1.s();
         }
      }));
      kotlin.d.b.l.a(var11, "view.activateReplacement…view.openActivateCard() }");
      this.b = co.uk.getmondo.common.j.f.a(var3, var11);
      var3 = this.b;
      io.reactivex.b.b var8 = var1.d().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.n var1x) {
            e.this.i.a(Impression.Companion.a(Impression.PinFrom.CARD));
            if(var2) {
               var1.b_();
            } else {
               var1.t();
            }

         }
      }));
      kotlin.d.b.l.a(var8, "view.forgotPinClicks\n   …      }\n                }");
      this.b = co.uk.getmondo.common.j.f.a(var3, var8);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u001a\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\r\bf\u0018\u00002\u00020\u00012\u00020\u0002J\b\u0010\u0014\u001a\u00020\u0005H&J\b\u0010\u0015\u001a\u00020\u0005H&J\b\u0010\u0016\u001a\u00020\u0005H&J\b\u0010\u0017\u001a\u00020\u0005H&J\b\u0010\u0018\u001a\u00020\u0005H&J\b\u0010\u0019\u001a\u00020\u0005H&J\b\u0010\u001a\u001a\u00020\u0005H&J\b\u0010\u001b\u001a\u00020\u0005H&J\b\u0010\u001c\u001a\u00020\u0005H&J\b\u0010\u001d\u001a\u00020\u0005H&J\u0010\u0010\u001e\u001a\u00020\u00052\u0006\u0010\u001f\u001a\u00020 H&J\b\u0010!\u001a\u00020\u0005H&J\b\u0010\"\u001a\u00020\u0005H&J\u0010\u0010#\u001a\u00020\u00052\u0006\u0010$\u001a\u00020%H&J\u0010\u0010&\u001a\u00020\u00052\u0006\u0010'\u001a\u00020%H&J\u0010\u0010(\u001a\u00020\u00052\u0006\u0010)\u001a\u00020 H&J\b\u0010*\u001a\u00020\u0005H&J\u0018\u0010+\u001a\u00020\u00052\u0006\u0010,\u001a\u00020-2\u0006\u0010.\u001a\u00020 H&J\b\u0010/\u001a\u00020\u0005H&J\b\u00100\u001a\u00020\u0005H&J\b\u00101\u001a\u00020\u0005H&J\b\u00102\u001a\u00020\u0005H&J\u0018\u00103\u001a\u00020\u00052\u0006\u00104\u001a\u00020 2\u0006\u00105\u001a\u00020-H&J\u0010\u00106\u001a\u00020\u00052\u0006\u00107\u001a\u00020%H&J\b\u00108\u001a\u00020\u0005H&J\b\u00109\u001a\u00020\u0005H&R\u0018\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0006\u0010\u0007R\u0018\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\t\u0010\u0007R\u0018\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\u0007R\u0018\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\r\u0010\u0007R\u0018\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\u000f\u0010\u0007R\u0018\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0011\u0010\u0007R\u0018\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0013\u0010\u0007¨\u0006:"},
      d2 = {"Lco/uk/getmondo/card/CardPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "activateReplacementCardClicks", "Lio/reactivex/Observable;", "", "getActivateReplacementCardClicks", "()Lio/reactivex/Observable;", "applyForOverdraftClicks", "getApplyForOverdraftClicks", "cardReplacementClicks", "getCardReplacementClicks", "forgotPinClicks", "getForgotPinClicks", "manageOverdraftClicks", "getManageOverdraftClicks", "toggleCardClicks", "getToggleCardClicks", "topUpClicks", "getTopUpClicks", "hideActivateReplacementCardButton", "hideCardLoading", "hideCardReplacementButton", "hideOverdraftAction", "hideToggleButton", "openActivateCard", "openCardReplacement", "openForgotPin", "openOverdraftManagement", "openOverdraftTour", "openTopUp", "isRetail", "", "setCardActive", "setCardInactive", "setExpirationDate", "expires", "", "setLastFourDigits", "lastDigits", "setToggleButtonEnabled", "enabled", "showActivateReplacementCardButton", "showBalance", "balance", "Lco/uk/getmondo/model/Amount;", "showOnToolbar", "showCardFrozen", "showCardLoading", "showCardReplacementButton", "showForgotPinDialog", "showOverdraftAction", "hasOverdraft", "limit", "showProcessorToken", "processorToken", "showTitle", "showToggleButton", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      io.reactivex.n a();

      void a(co.uk.getmondo.d.c var1, boolean var2);

      void a(String var1);

      void a(boolean var1);

      void a(boolean var1, co.uk.getmondo.d.c var2);

      void a_();

      io.reactivex.n b();

      void b(String var1);

      void b(boolean var1);

      void b_();

      io.reactivex.n c();

      void c(String var1);

      io.reactivex.n d();

      io.reactivex.n e();

      io.reactivex.n f();

      io.reactivex.n g();

      void h();

      void i();

      void j();

      void k();

      void l();

      void m();

      void n();

      void o();

      void p();

      void r();

      void s();

      void t();

      void v();

      void w();

      void x();

      void y();

      void z();
   }
}
