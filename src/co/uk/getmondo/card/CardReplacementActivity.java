package co.uk.getmondo.card;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.uk.getmondo.common.activities.ConfirmationActivity;
import co.uk.getmondo.common.address.SelectAddressActivity;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class CardReplacementActivity extends co.uk.getmondo.common.activities.b implements j.a {
   private static final SimpleDateFormat b;
   j a;
   @BindView(2131820840)
   TextView addressTextView;
   private com.b.b.c c = com.b.b.c.a();
   @BindView(2131820841)
   Button editAddressButton;
   @BindView(2131820835)
   Button sendReplacementCardButton;

   static {
      b = new SimpleDateFormat("EEE d", Locale.UK);
   }

   public static Intent a(Context var0) {
      return new Intent(var0, CardReplacementActivity.class);
   }

   private static String b(Date var0) {
      Calendar var2 = Calendar.getInstance();
      var2.setTime(var0);
      int var1 = var2.get(5);
      return b.format(var0) + co.uk.getmondo.common.c.a.a(var1);
   }

   public io.reactivex.n a() {
      return com.b.a.c.c.a(this.sendReplacementCardButton);
   }

   public void a(String var1) {
      this.addressTextView.setText(var1);
   }

   public void a(Date var1) {
      String var2;
      if(var1 == null) {
         var2 = "";
      } else {
         var2 = this.getString(2131362057, new Object[]{b(var1)});
      }

      ConfirmationActivity.a(this, this.getString(2131362058), var2);
      this.finish();
   }

   public void a(boolean var1) {
      this.sendReplacementCardButton.setEnabled(var1);
   }

   public io.reactivex.n b() {
      return com.b.a.c.c.a(this.editAddressButton);
   }

   public io.reactivex.n c() {
      return this.c;
   }

   public void d() {
      this.startActivityForResult(SelectAddressActivity.b(this, co.uk.getmondo.common.activities.b.a.b, false), 2);
   }

   public void e() {
      this.s();
   }

   public void f() {
      this.t();
   }

   protected void onActivityResult(int var1, int var2, Intent var3) {
      switch(var1) {
      case 1:
         this.finish();
         break;
      case 2:
         if(var2 == -1) {
            co.uk.getmondo.d.s var4 = SelectAddressActivity.a(var3);
            this.c.a((Object)var4);
         }
         break;
      default:
         super.onActivityResult(var1, var2, var3);
      }

   }

   protected void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.setContentView(2131034150);
      ButterKnife.bind((Activity)this);
      this.l().a(this);
      this.a.a((j.a)this);
   }

   protected void onDestroy() {
      this.a.b();
      super.onDestroy();
   }
}
