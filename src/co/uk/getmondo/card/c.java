package co.uk.getmondo.card;

import co.uk.getmondo.api.MonzoApi;
import io.reactivex.v;
import java.util.Date;
import java.util.concurrent.Callable;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u001f\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u0014\u0010\r\u001a\b\u0012\u0004\u0012\u00020\n0\u000e2\u0006\u0010\u000f\u001a\u00020\u0010J\u001c\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\n0\u000e2\u0006\u0010\u0012\u001a\u00020\u00102\u0006\u0010\u0013\u001a\u00020\u0014J\f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\n0\u0016J\u0016\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\n0\u00182\u0006\u0010\u0019\u001a\u00020\u0010H\u0002J\u0014\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u00140\u000e2\u0006\u0010\u0019\u001a\u00020\u0010J\u001e\u0010\u001b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u001d0\u001c0\u000e2\n\b\u0002\u0010\u001e\u001a\u0004\u0018\u00010\u001fJ\u001a\u0010 \u001a\u00020!2\u0006\u0010\u0019\u001a\u00020\u00102\b\u0010\u001e\u001a\u0004\u0018\u00010\u001fH\u0002J \u0010\"\u001a\b\u0012\u0004\u0012\u00020\u001d0\u000e2\u0006\u0010\u0019\u001a\u00020\u00102\b\u0010\u001e\u001a\u0004\u0018\u00010\u001fH\u0002J\u0006\u0010#\u001a\u00020!J\u0018\u0010$\u001a\n %*\u0004\u0018\u00010!0!2\u0006\u0010\u0019\u001a\u00020\u0010H\u0002J\u0016\u0010&\u001a\u00020!2\u0006\u0010'\u001a\u00020\u00102\u0006\u0010(\u001a\u00020)R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u0013\u0010\t\u001a\u0004\u0018\u00010\n8F¢\u0006\u0006\u001a\u0004\b\u000b\u0010\fR\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006*"},
   d2 = {"Lco/uk/getmondo/card/CardManager;", "", "accountManager", "Lco/uk/getmondo/common/accounts/AccountManager;", "monzoApi", "Lco/uk/getmondo/api/MonzoApi;", "cardStorage", "Lco/uk/getmondo/card/CardStorage;", "(Lco/uk/getmondo/common/accounts/AccountManager;Lco/uk/getmondo/api/MonzoApi;Lco/uk/getmondo/card/CardStorage;)V", "cachedCard", "Lco/uk/getmondo/model/Card;", "getCachedCard", "()Lco/uk/getmondo/model/Card;", "activateDebitCard", "Lio/reactivex/Single;", "cardPan", "", "activatePrepaidCard", "cardToken", "isReplacementCard", "", "card", "Lio/reactivex/Observable;", "getCardFromApi", "Lio/reactivex/Maybe;", "accountId", "hasCard", "replaceCard", "Lcom/memoizrlabs/poweroptional/Optional;", "Ljava/util/Date;", "address", "Lco/uk/getmondo/model/LegacyAddress;", "replaceCardBank", "Lio/reactivex/Completable;", "replaceCardPrepaid", "syncCard", "syncCardForAccount", "kotlin.jvm.PlatformType", "toggleCard", "cardId", "apiCardStatus", "Lco/uk/getmondo/api/model/ApiCardStatus;", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class c {
   private final co.uk.getmondo.common.accounts.b a;
   private final MonzoApi b;
   private final s c;

   public c(co.uk.getmondo.common.accounts.b var1, MonzoApi var2, s var3) {
      kotlin.d.b.l.b(var1, "accountManager");
      kotlin.d.b.l.b(var2, "monzoApi");
      kotlin.d.b.l.b(var3, "cardStorage");
      super();
      this.a = var1;
      this.b = var2;
      this.c = var3;
   }

   private final io.reactivex.b a(final String var1, final co.uk.getmondo.d.s var2) {
      io.reactivex.b var3 = v.c((Callable)(new Callable() {
         public final co.uk.getmondo.d.g a() {
            return c.this.c.a(var1);
         }

         // $FF: synthetic method
         public Object call() {
            return this.a();
         }
      })).c((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.d a(co.uk.getmondo.d.g var1) {
            kotlin.d.b.l.b(var1, "card");
            io.reactivex.d var2x;
            if(var2 == null) {
               var2x = (io.reactivex.d)c.this.b.orderReplacementCardBank(var1.a(), (String[])null, (String)null, (String)null, (String)null, (String)null);
            } else {
               var2x = (io.reactivex.d)c.this.b.orderReplacementCardBank(var1.a(), var2.h(), var2.i(), var2.b(), var2.c(), var2.j());
            }

            return var2x;
         }
      })).b((io.reactivex.d)this.c(var1));
      kotlin.d.b.l.a(var3, "Single.fromCallable({ ca…ardForAccount(accountId))");
      return var3;
   }

   private final v b(final String var1, final co.uk.getmondo.d.s var2) {
      v var3 = v.c((Callable)(new Callable() {
         public final co.uk.getmondo.d.g a() {
            return c.this.c.a(var1);
         }

         // $FF: synthetic method
         public Object call() {
            return this.a();
         }
      })).a((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final v a(co.uk.getmondo.d.g var1) {
            kotlin.d.b.l.b(var1, "card");
            v var2x;
            if(var2 == null) {
               var2x = c.this.b.orderReplacementCardPrepaid(var1.a(), (String[])null, (String)null, (String)null, (String)null, (String)null);
            } else {
               var2x = c.this.b.orderReplacementCardPrepaid(var1.a(), var2.h(), var2.i(), var2.b(), var2.c(), var2.j());
            }

            return var2x;
         }
      })).d((io.reactivex.c.h)null.a).a((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final v a(Date var1x) {
            kotlin.d.b.l.b(var1x, "it");
            return c.this.c(var1).a((Object)var1x);
         }
      }));
      kotlin.d.b.l.a(var3, "Single.fromCallable { ca…Id).toSingleDefault(it) }");
      return var3;
   }

   private final io.reactivex.b c(String var1) {
      return this.d(var1).c();
   }

   private final io.reactivex.h d(String var1) {
      io.reactivex.h var2 = this.b.cards(var1).d((io.reactivex.c.h)null.a).a((io.reactivex.c.q)null.a).c((io.reactivex.c.h)null.a).c((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(co.uk.getmondo.d.g var1) {
            s var2 = c.this.c;
            kotlin.d.b.l.a(var1, "it");
            var2.a(var1);
         }
      }));
      kotlin.d.b.l.a(var2, "monzoApi.cards(accountId…ardStorage.saveCard(it) }");
      return var2;
   }

   public final io.reactivex.b a(final String var1, final co.uk.getmondo.api.model.a var2) {
      kotlin.d.b.l.b(var1, "cardId");
      kotlin.d.b.l.b(var2, "apiCardStatus");
      io.reactivex.b var3 = this.a.e().firstOrError().c((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.b a(String var1x) {
            kotlin.d.b.l.b(var1x, "accountId");
            return c.this.b.toggleCard(var1, var2).b((io.reactivex.d)c.this.c(var1x));
         }
      }));
      kotlin.d.b.l.a(var3, "accountManager.accountId…untId))\n                }");
      return var3;
   }

   public final io.reactivex.n a() {
      io.reactivex.n var1 = this.a.e().flatMap((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.n a(String var1) {
            kotlin.d.b.l.b(var1, "it");
            return c.this.c.b(var1);
         }
      }));
      kotlin.d.b.l.a(var1, "accountManager.accountId… { cardStorage.card(it) }");
      return var1;
   }

   public final v a(final co.uk.getmondo.d.s var1) {
      v var2 = v.a((Callable)(new Callable() {
         public final v a() {
            co.uk.getmondo.d.a var1x = c.this.a.a();
            if(var1x == null) {
               kotlin.d.b.l.a();
            }

            v var2;
            if(var1x.e()) {
               var2 = c.this.a(var1x.a(), var1).a((Object)com.c.b.b.c());
            } else {
               var2 = c.this.b(var1x.a(), var1).d((io.reactivex.c.h)null.a);
            }

            return var2;
         }

         // $FF: synthetic method
         public Object call() {
            return this.a();
         }
      }));
      kotlin.d.b.l.a(var2, "Single.defer {\n         …}\n            }\n        }");
      return var2;
   }

   public final v a(final String var1) {
      kotlin.d.b.l.b(var1, "accountId");
      v var2 = v.a((Callable)(new Callable() {
         public final v a() {
            v var1x;
            if(c.this.c.a(var1) == null) {
               var1x = c.this.d(var1).d().d((io.reactivex.c.h)null.a);
            } else {
               var1x = v.a((Object)Boolean.valueOf(true));
            }

            return var1x;
         }

         // $FF: synthetic method
         public Object call() {
            return this.a();
         }
      }));
      kotlin.d.b.l.a(var2, "Single.defer {\n         …)\n            }\n        }");
      return var2;
   }

   public final v a(final String var1, final boolean var2) {
      kotlin.d.b.l.b(var1, "cardToken");
      v var3 = v.a((Callable)(new Callable() {
         public final v a() {
            String var2x = c.this.a.c();
            if(var2x == null) {
               kotlin.d.b.l.a();
            }

            v var3;
            if(var2) {
               co.uk.getmondo.d.g var1x = c.this.c.a(var2x);
               if(var1x == null) {
                  throw (Throwable)(new RuntimeException("Trying to activate a replacement card but there isn't a previous card saved"));
               }

               var3 = c.this.b.activateReplacementCard(var2x, var1x.a(), var1).d((io.reactivex.c.h)null.a);
            } else {
               var3 = c.this.b.activateCardPrepaid(var2x, var1).d((io.reactivex.c.h)null.a);
            }

            return var3;
         }

         // $FF: synthetic method
         public Object call() {
            return this.a();
         }
      })).d((io.reactivex.c.h)null.a).c((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(co.uk.getmondo.d.g var1) {
            s var2 = c.this.c;
            kotlin.d.b.l.a(var1, "card");
            var2.a(var1);
         }
      }));
      kotlin.d.b.l.a(var3, "activationObservable\n   …dStorage.saveCard(card) }");
      return var3;
   }

   public final co.uk.getmondo.d.g b() {
      String var1 = this.a.c();
      co.uk.getmondo.d.g var2;
      if(var1 != null) {
         var2 = this.c.a(var1);
      } else {
         var2 = null;
      }

      return var2;
   }

   public final v b(final String var1) {
      kotlin.d.b.l.b(var1, "cardPan");
      v var2 = this.a.e().firstOrError().a((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final v a(String var1x) {
            kotlin.d.b.l.b(var1x, "accountId");
            return c.this.b.activateCardBank(var1x, var1).d((io.reactivex.c.h)null.a).c((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(co.uk.getmondo.d.g var1x) {
                  s var2 = c.this.c;
                  kotlin.d.b.l.a(var1x, "card");
                  var2.a(var1x);
               }
            }));
         }
      }));
      kotlin.d.b.l.a(var2, "accountManager.accountId…card) }\n                }");
      return var2;
   }

   public final io.reactivex.b c() {
      io.reactivex.b var1 = this.a.e().firstOrError().c((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.b a(String var1) {
            kotlin.d.b.l.b(var1, "it");
            return c.this.c(var1);
         }
      }));
      kotlin.d.b.l.a(var1, "accountManager.accountId… syncCardForAccount(it) }");
      return var1;
   }
}
