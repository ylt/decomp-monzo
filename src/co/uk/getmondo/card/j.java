package co.uk.getmondo.card;

import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.d.ac;
import io.reactivex.u;
import java.util.Date;

class j extends co.uk.getmondo.common.ui.b {
   private final u c;
   private final u d;
   private final co.uk.getmondo.common.accounts.d e;
   private final co.uk.getmondo.common.e.a f;
   private final c g;
   private final co.uk.getmondo.common.a h;
   private co.uk.getmondo.d.s i;

   j(u var1, u var2, co.uk.getmondo.common.accounts.d var3, co.uk.getmondo.common.e.a var4, c var5, co.uk.getmondo.common.a var6) {
      this.c = var1;
      this.d = var2;
      this.e = var3;
      this.f = var4;
      this.g = var5;
      this.h = var6;
   }

   // $FF: synthetic method
   static io.reactivex.l a(j var0, j.a var1, Object var2) throws Exception {
      return var0.g.a(var0.i).b(var0.d).a(var0.c).d(q.a(var0, var1)).e().f();
   }

   private void a() {
      if(this.i != null) {
         ac var1 = this.e.b().d().a(this.i);
         this.e.a(var1);
      }

   }

   // $FF: synthetic method
   static void a(j.a var0, Object var1) throws Exception {
      var0.d();
   }

   // $FF: synthetic method
   static void a(j var0, j.a var1, co.uk.getmondo.d.s var2) throws Exception {
      var0.i = var2;
      var1.a(co.uk.getmondo.common.k.a.b(var2));
   }

   // $FF: synthetic method
   static void a(j var0, j.a var1, com.c.b.b var2) throws Exception {
      var0.a();
      var1.f();
      var1.a(true);
      Date var3;
      if(var2.b()) {
         var3 = (Date)var2.a();
      } else {
         var3 = null;
      }

      var1.a(var3);
   }

   // $FF: synthetic method
   static void a(j var0, j.a var1, Throwable var2) throws Exception {
      var1.f();
      var1.a(true);
      if(!var0.f.a(var2, var1)) {
         var1.b(2131362061);
      }

   }

   // $FF: synthetic method
   static void b(j var0, j.a var1, Object var2) throws Exception {
      var0.h.a(Impression.H());
      var1.e();
      var1.a(false);
   }

   public void a(j.a var1) {
      super.a((co.uk.getmondo.common.ui.f)var1);
      var1.a(co.uk.getmondo.common.k.a.b(this.e.b().d().h()));
      this.a((io.reactivex.b.b)var1.b().subscribe(k.a(var1)));
      this.a((io.reactivex.b.b)var1.c().subscribe(l.a(this, var1)));
      this.a((io.reactivex.b.b)var1.a().doOnNext(m.a(this, var1)).flatMapMaybe(n.a(this, var1)).subscribe(o.a(this, var1), p.a()));
   }

   interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      io.reactivex.n a();

      void a(String var1);

      void a(Date var1);

      void a(boolean var1);

      io.reactivex.n b();

      io.reactivex.n c();

      void d();

      void e();

      void f();
   }
}
