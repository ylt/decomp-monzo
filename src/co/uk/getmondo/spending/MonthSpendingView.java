package co.uk.getmondo.spending;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.uk.getmondo.common.k.p;
import co.uk.getmondo.common.ui.AmountView;
import co.uk.getmondo.d.aa;
import co.uk.getmondo.d.h;
import co.uk.getmondo.spending.a.g;
import com.bumptech.glide.g.b.j;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import org.threeten.bp.YearMonth;
import org.threeten.bp.format.TextStyle;

public class MonthSpendingView extends FrameLayout {
   private final co.uk.getmondo.common.ui.a a;
   private YearMonth b;
   private MonthSpendingView.a c;
   @BindView(2131821754)
   LinearLayout cardsContainer;
   private int d;
   @BindView(2131821753)
   AmountView monthAmountView;
   @BindView(2131821616)
   TextView monthNameTextView;

   public MonthSpendingView(Context var1) {
      this(var1, (AttributeSet)null, 0);
   }

   public MonthSpendingView(Context var1, AttributeSet var2) {
      this(var1, var2, 0);
   }

   public MonthSpendingView(Context var1, AttributeSet var2, int var3) {
      super(var1, var2, var3);
      this.a = co.uk.getmondo.common.ui.a.a(var1);
      LayoutInflater.from(var1).inflate(2131034435, this);
      ButterKnife.bind((View)this);
      if(this.isInEditMode()) {
         ArrayList var7 = new ArrayList();
         g var6 = new g(new co.uk.getmondo.d.c(2000L, "GBP"), 3, new ArrayList());
         h[] var5 = h.values();
         int var4 = var5.length;

         for(var3 = 0; var3 < var4; ++var3) {
            var7.add(new co.uk.getmondo.spending.a.h.a(var6, var5[var3]));
         }

         this.setSpendingBreakdownData(var7);
      }

      this.d = (int)TypedValue.applyDimension(2, 20.0F, this.getResources().getDisplayMetrics());
   }

   private void setSpendingBreakdownData(List var1) {
      int var2;
      for(var2 = 0; var2 < var1.size(); ++var2) {
         co.uk.getmondo.spending.a.h var4 = (co.uk.getmondo.spending.a.h)var1.get(var2);
         View var3 = this.cardsContainer.getChildAt(var2);
         MonthSpendingView.ViewHolder var6;
         if(var3 != null) {
            var3.setVisibility(0);
            var6 = (MonthSpendingView.ViewHolder)var3.getTag();
         } else {
            View var5 = LayoutInflater.from(this.getContext()).inflate(2131034378, this.cardsContainer, false);
            var6 = new MonthSpendingView.ViewHolder(var5);
            var5.setTag(var6);
            this.cardsContainer.addView(var5);
         }

         var6.a(var4);
      }

      if(this.cardsContainer.getChildCount() > var1.size()) {
         for(var2 = var1.size(); var2 < this.cardsContainer.getChildCount(); ++var2) {
            this.cardsContainer.getChildAt(var2).setVisibility(8);
         }
      }

   }

   public void a(YearMonth var1, g var2) {
      this.b = var1;
      this.monthAmountView.setAmount(var2.a());
      this.monthNameTextView.setText(var1.c().a(TextStyle.a, Locale.ENGLISH));
      this.setSpendingBreakdownData(var2.c());
   }

   public void setItemClickListener(MonthSpendingView.a var1) {
      this.c = var1;
   }

   class ViewHolder {
      @BindView(2131821019)
      AmountView amountView;
      private co.uk.getmondo.spending.a.h b;
      @BindView(2131821617)
      CardView cardView;
      @BindView(2131821618)
      ImageView logoImageView;
      @BindView(2131821619)
      TextView subtitleTextView;
      @BindView(2131821026)
      TextView titleTextView;

      ViewHolder(View var2) {
         ButterKnife.bind(this, (View)var2);
      }

      private void a(co.uk.getmondo.spending.a.h.a var1) {
         this.titleTextView.setText(var1.b().a());
         this.logoImageView.setImageResource(var1.b().d());
         this.logoImageView.setClipToOutline(false);
         this.logoImageView.setBackgroundResource(0);
      }

      private void a(co.uk.getmondo.spending.a.h.b var1) {
         this.titleTextView.setText(var1.b().i());
         this.logoImageView.setClipToOutline(true);
         this.logoImageView.setBackgroundResource(2130838008);
         if(!p.d(var1.b().j())) {
            com.bumptech.glide.g.b(this.logoImageView.getContext()).a(var1.b().j()).a().a(this.logoImageView);
         } else {
            this.logoImageView.setImageResource(var1.b().b().c());
         }

      }

      private void a(co.uk.getmondo.spending.a.h.c var1) {
         aa var2 = var1.b();
         this.titleTextView.setText(var2.b());
         if(var2.f()) {
            com.bumptech.glide.g.b(this.logoImageView.getContext()).a(var2.g()).h().a().a((j)(new co.uk.getmondo.common.ui.c(this.logoImageView)));
         } else {
            this.logoImageView.setImageDrawable(MonthSpendingView.this.a.a(var2.b()).a(MonthSpendingView.this.d));
         }

      }

      void a(co.uk.getmondo.spending.a.h var1) {
         this.b = var1;
         co.uk.getmondo.d.c var3 = var1.a().a();
         AmountView var4 = this.amountView;
         if(var3.e() >= 1.0D) {
            var3 = var3.h();
         } else {
            var3 = var3.i();
         }

         var4.setAmount(var3);
         int var2 = var1.a().b();
         this.subtitleTextView.setText(MonthSpendingView.this.getResources().getQuantityString(2131886084, var2, new Object[]{Integer.valueOf(var2)}));
         if(var1 instanceof co.uk.getmondo.spending.a.h.a) {
            this.a((co.uk.getmondo.spending.a.h.a)var1);
         } else if(var1 instanceof co.uk.getmondo.spending.a.h.b) {
            this.a((co.uk.getmondo.spending.a.h.b)var1);
         } else if(var1 instanceof co.uk.getmondo.spending.a.h.c) {
            this.a((co.uk.getmondo.spending.a.h.c)var1);
         }

      }

      @OnClick({2131821617})
      void onCardClicked() {
         if(MonthSpendingView.this.c != null) {
            MonthSpendingView.this.c.a(MonthSpendingView.this.b, this.b);
         }

      }
   }

   public interface a {
      void a(YearMonth var1, co.uk.getmondo.spending.a.h var2);
   }
}
