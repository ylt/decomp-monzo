package co.uk.getmondo.spending;

import android.support.v7.widget.CardView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import co.uk.getmondo.common.ui.AmountView;

public class MonthSpendingView$ViewHolder_ViewBinding implements Unbinder {
   private MonthSpendingView.ViewHolder a;
   private View b;

   public MonthSpendingView$ViewHolder_ViewBinding(final MonthSpendingView.ViewHolder var1, View var2) {
      this.a = var1;
      View var3 = Utils.findRequiredView(var2, 2131821617, "field 'cardView' and method 'onCardClicked'");
      var1.cardView = (CardView)Utils.castView(var3, 2131821617, "field 'cardView'", CardView.class);
      this.b = var3;
      var3.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View var1x) {
            var1.onCardClicked();
         }
      });
      var1.logoImageView = (ImageView)Utils.findRequiredViewAsType(var2, 2131821618, "field 'logoImageView'", ImageView.class);
      var1.titleTextView = (TextView)Utils.findRequiredViewAsType(var2, 2131821026, "field 'titleTextView'", TextView.class);
      var1.subtitleTextView = (TextView)Utils.findRequiredViewAsType(var2, 2131821619, "field 'subtitleTextView'", TextView.class);
      var1.amountView = (AmountView)Utils.findRequiredViewAsType(var2, 2131821019, "field 'amountView'", AmountView.class);
   }

   public void unbind() {
      MonthSpendingView.ViewHolder var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.cardView = null;
         var1.logoImageView = null;
         var1.titleTextView = null;
         var1.subtitleTextView = null;
         var1.amountView = null;
         this.b.setOnClickListener((OnClickListener)null);
         this.b = null;
      }
   }
}
