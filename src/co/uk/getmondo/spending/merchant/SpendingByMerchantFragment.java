package co.uk.getmondo.spending.merchant;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import co.uk.getmondo.spending.MonthSpendingView;
import co.uk.getmondo.spending.transactions.SpendingTransactionsActivity;
import org.threeten.bp.YearMonth;

public class SpendingByMerchantFragment extends co.uk.getmondo.common.f.a implements h.a {
   h a;
   private final com.b.b.c c = com.b.b.c.a();
   private Unbinder d;
   @BindView(2131821376)
   MonthSpendingView monthSpendingView;

   public static SpendingByMerchantFragment a(YearMonth var0, co.uk.getmondo.d.h var1) {
      Bundle var2 = new Bundle();
      var2.putSerializable("KEY_YEAR_MONTH", var0);
      var2.putSerializable("KEY_CATEGORY", var1);
      SpendingByMerchantFragment var3 = new SpendingByMerchantFragment();
      var3.setArguments(var2);
      return var3;
   }

   // $FF: synthetic method
   static void a(SpendingByMerchantFragment var0) throws Exception {
      var0.monthSpendingView.setItemClickListener((MonthSpendingView.a)null);
   }

   // $FF: synthetic method
   static void a(SpendingByMerchantFragment var0, io.reactivex.o var1) throws Exception {
      var0.monthSpendingView.setItemClickListener(c.a(var1));
      var1.a(d.a(var0));
   }

   // $FF: synthetic method
   static void a(io.reactivex.o var0, YearMonth var1, co.uk.getmondo.spending.a.h var2) {
      var0.a(var2);
   }

   public io.reactivex.n a() {
      return io.reactivex.n.create(b.a(this));
   }

   public void a(YearMonth var1, co.uk.getmondo.d.h var2, String var3, String var4) {
      SpendingTransactionsActivity.a(this.getActivity(), var1, var2, var3, var4);
   }

   public void a(YearMonth var1, co.uk.getmondo.spending.a.g var2) {
      this.monthSpendingView.a(var1, var2);
   }

   public io.reactivex.n b() {
      return this.c;
   }

   public void b(YearMonth var1, co.uk.getmondo.d.h var2, String var3, String var4) {
      SpendingTransactionsActivity.c(this.getActivity(), var1, var2, var3, var4);
   }

   public void onCreate(Bundle var1) {
      super.onCreate(var1);
      YearMonth var2 = (YearMonth)this.getArguments().getSerializable("KEY_YEAR_MONTH");
      co.uk.getmondo.d.h var3 = (co.uk.getmondo.d.h)this.getArguments().getSerializable("KEY_CATEGORY");
      this.B().a(new f(var2, var3)).a(this);
   }

   public View onCreateView(LayoutInflater var1, ViewGroup var2, Bundle var3) {
      return var1.inflate(2131034282, var2, false);
   }

   public void onDestroyView() {
      this.a.b();
      this.d.unbind();
      super.onDestroyView();
   }

   public void onViewCreated(View var1, Bundle var2) {
      super.onViewCreated(var1, var2);
      this.d = ButterKnife.bind(this, (View)var1);
      this.a.a((h.a)this);
   }

   public void setUserVisibleHint(boolean var1) {
      super.setUserVisibleHint(var1);
      this.c.a((Object)Boolean.valueOf(var1));
   }
}
