package co.uk.getmondo.spending.merchant;

import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import co.uk.getmondo.spending.MonthSpendingView;

public class SpendingByMerchantFragment_ViewBinding implements Unbinder {
   private SpendingByMerchantFragment a;

   public SpendingByMerchantFragment_ViewBinding(SpendingByMerchantFragment var1, View var2) {
      this.a = var1;
      var1.monthSpendingView = (MonthSpendingView)Utils.findRequiredViewAsType(var2, 2131821376, "field 'monthSpendingView'", MonthSpendingView.class);
   }

   public void unbind() {
      SpendingByMerchantFragment var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.monthSpendingView = null;
      }
   }
}
