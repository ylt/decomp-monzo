package co.uk.getmondo.spending.merchant;

import io.reactivex.u;

public final class g implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final f b;
   private final javax.a.a c;
   private final javax.a.a d;
   private final javax.a.a e;
   private final javax.a.a f;

   static {
      boolean var0;
      if(!g.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   public g(f var1, javax.a.a var2, javax.a.a var3, javax.a.a var4, javax.a.a var5) {
      if(!a && var1 == null) {
         throw new AssertionError();
      } else {
         this.b = var1;
         if(!a && var2 == null) {
            throw new AssertionError();
         } else {
            this.c = var2;
            if(!a && var3 == null) {
               throw new AssertionError();
            } else {
               this.d = var3;
               if(!a && var4 == null) {
                  throw new AssertionError();
               } else {
                  this.e = var4;
                  if(!a && var5 == null) {
                     throw new AssertionError();
                  } else {
                     this.f = var5;
                  }
               }
            }
         }
      }
   }

   public static b.a.b a(f var0, javax.a.a var1, javax.a.a var2, javax.a.a var3, javax.a.a var4) {
      return new g(var0, var1, var2, var3, var4);
   }

   public h a() {
      return (h)b.a.d.a(this.b.a((u)this.c.b(), (co.uk.getmondo.spending.a.i)this.d.b(), (co.uk.getmondo.spending.a.b)this.e.b(), (co.uk.getmondo.common.a)this.f.b()), "Cannot return null from a non-@Nullable @Provides method");
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
