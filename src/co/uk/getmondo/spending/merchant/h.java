package co.uk.getmondo.spending.merchant;

import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.d.aa;
import io.reactivex.r;
import io.reactivex.u;
import java.util.List;
import org.threeten.bp.YearMonth;

public class h extends co.uk.getmondo.common.ui.b {
   private final u c;
   private final co.uk.getmondo.spending.a.i d;
   private final co.uk.getmondo.spending.a.b e;
   private final YearMonth f;
   private final co.uk.getmondo.d.h g;
   private final co.uk.getmondo.common.a h;

   h(u var1, co.uk.getmondo.spending.a.i var2, co.uk.getmondo.spending.a.b var3, YearMonth var4, co.uk.getmondo.d.h var5, co.uk.getmondo.common.a var6) {
      this.c = var1;
      this.d = var2;
      this.e = var3;
      this.f = var4;
      this.g = var5;
      this.h = var6;
   }

   // $FF: synthetic method
   static r a(h var0, List var1) throws Exception {
      return var0.e.a(var1, co.uk.getmondo.spending.a.a.a).defaultIfEmpty(new android.support.v4.g.j(var0.f, new co.uk.getmondo.spending.a.g(new co.uk.getmondo.d.c(0L, co.uk.getmondo.common.i.c.a), 0)));
   }

   // $FF: synthetic method
   static void a(h.a var0, android.support.v4.g.j var1) throws Exception {
      var0.a((YearMonth)var1.a, (co.uk.getmondo.spending.a.g)var1.b);
   }

   // $FF: synthetic method
   static void a(h var0, h.a var1, co.uk.getmondo.spending.a.h var2) throws Exception {
      if(var2 instanceof co.uk.getmondo.spending.a.h.b) {
         co.uk.getmondo.d.u var3 = ((co.uk.getmondo.spending.a.h.b)var2).b();
         var1.a(var0.f, var0.g, var3.i(), var3.h());
      } else if(var2 instanceof co.uk.getmondo.spending.a.h.c) {
         aa var4 = ((co.uk.getmondo.spending.a.h.c)var2).b();
         var1.b(var0.f, var0.g, var4.b(), var4.a());
      }

   }

   // $FF: synthetic method
   static void a(h var0, Boolean var1) throws Exception {
      Impression var2 = Impression.b(var0.g, var0.f);
      var0.h.a(var2);
   }

   // $FF: synthetic method
   static boolean a(Boolean var0) throws Exception {
      return var0.booleanValue();
   }

   public void a(h.a var1) {
      super.a((co.uk.getmondo.common.ui.f)var1);
      this.a((io.reactivex.b.b)var1.b().filter(i.a()).subscribe(j.a(this), k.a()));
      this.a((io.reactivex.b.b)this.d.a(this.f, this.g).flatMap(l.a(this)).observeOn(this.c).subscribe(m.a(var1), n.a()));
      this.a((io.reactivex.b.b)var1.a().subscribe(o.a(this, var1)));
   }

   interface a extends co.uk.getmondo.common.ui.f {
      io.reactivex.n a();

      void a(YearMonth var1, co.uk.getmondo.d.h var2, String var3, String var4);

      void a(YearMonth var1, co.uk.getmondo.spending.a.g var2);

      io.reactivex.n b();

      void b(YearMonth var1, co.uk.getmondo.d.h var2, String var3, String var4);
   }
}
