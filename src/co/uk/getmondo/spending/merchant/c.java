package co.uk.getmondo.spending.merchant;

import co.uk.getmondo.spending.MonthSpendingView;
import org.threeten.bp.YearMonth;

// $FF: synthetic class
final class c implements MonthSpendingView.a {
   private final io.reactivex.o a;

   private c(io.reactivex.o var1) {
      this.a = var1;
   }

   public static MonthSpendingView.a a(io.reactivex.o var0) {
      return new c(var0);
   }

   public void a(YearMonth var1, co.uk.getmondo.spending.a.h var2) {
      SpendingByMerchantFragment.a(this.a, var1, var2);
   }
}
