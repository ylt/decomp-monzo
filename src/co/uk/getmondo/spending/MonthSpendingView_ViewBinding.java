package co.uk.getmondo.spending;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import co.uk.getmondo.common.ui.AmountView;

public class MonthSpendingView_ViewBinding implements Unbinder {
   private MonthSpendingView a;

   public MonthSpendingView_ViewBinding(MonthSpendingView var1, View var2) {
      this.a = var1;
      var1.monthAmountView = (AmountView)Utils.findRequiredViewAsType(var2, 2131821753, "field 'monthAmountView'", AmountView.class);
      var1.monthNameTextView = (TextView)Utils.findRequiredViewAsType(var2, 2131821616, "field 'monthNameTextView'", TextView.class);
      var1.cardsContainer = (LinearLayout)Utils.findRequiredViewAsType(var2, 2131821754, "field 'cardsContainer'", LinearLayout.class);
   }

   public void unbind() {
      MonthSpendingView var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.monthAmountView = null;
         var1.monthNameTextView = null;
         var1.cardsContainer = null;
      }
   }
}
