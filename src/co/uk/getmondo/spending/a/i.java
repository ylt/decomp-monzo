package co.uk.getmondo.spending.a;

import android.util.Pair;
import co.uk.getmondo.d.aj;
import io.reactivex.v;
import io.realm.av;
import io.realm.bf;
import io.realm.bg;
import io.realm.bl;
import java.util.Date;
import java.util.TimeZone;
import org.threeten.bp.LocalDate;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.YearMonth;

public class i {
   private co.uk.getmondo.transaction.a.j a;

   public i(co.uk.getmondo.transaction.a.j var1) {
      this.a = var1;
   }

   // $FF: synthetic method
   static Pair a(YearMonth var0, YearMonth var1) throws Exception {
      LocalDateTime var2 = var0.c(1).m();
      LocalDateTime var3 = var1.f().b(23, 59).a(59).b(999999999);
      return new Pair(co.uk.getmondo.common.c.a.a(var2), co.uk.getmondo.common.c.a.a(var3));
   }

   // $FF: synthetic method
   static io.reactivex.r a(co.uk.getmondo.d.h var0, Pair var1) throws Exception {
      return co.uk.getmondo.common.j.g.a(s.a(var1, var0));
   }

   // $FF: synthetic method
   static io.reactivex.r a(co.uk.getmondo.d.h var0, String var1, String var2, Pair var3) throws Exception {
      return co.uk.getmondo.common.j.g.a(k.a(var3, var0, var1, var2));
   }

   // $FF: synthetic method
   static bg a(Pair var0, co.uk.getmondo.d.h var1, av var2) {
      return var2.a(aj.class).a("created", (Date)var0.first).b("created", (Date)var0.second).a("category", var1.f()).a("includeInSpending", Boolean.valueOf(true)).g();
   }

   // $FF: synthetic method
   static bg a(Pair var0, co.uk.getmondo.d.h var1, String var2, String var3, av var4) {
      bf var6 = var4.a(co.uk.getmondo.d.m.class).b("transaction").a("transaction.created", (Date)var0.first).b("transaction.created", (Date)var0.second).a("transaction.includeInSpending", Boolean.valueOf(true)).a("transaction.category", var1.f());
      bf var5 = var6;
      if(var2 != null) {
         var5 = var6.a("transaction.merchant.groupId", var2);
      }

      var6 = var5;
      if(var3 != null) {
         var6 = var5.a("transaction.peer.userId", var3);
      }

      return var6.a("created", bl.b);
   }

   // $FF: synthetic method
   static bg a(av var0) {
      return var0.a(aj.class).a("includeInSpending", Boolean.valueOf(true)).g();
   }

   // $FF: synthetic method
   static LocalDate a(aj var0) throws Exception {
      return co.uk.getmondo.common.c.a.a(var0.v(), TimeZone.getTimeZone("UTC")).j();
   }

   private static v b(YearMonth var0, YearMonth var1) {
      return v.c(j.a(var0, var1));
   }

   public io.reactivex.n a() {
      return co.uk.getmondo.common.j.g.a(n.a()).map(o.a());
   }

   public io.reactivex.n a(YearMonth var1, co.uk.getmondo.d.h var2) {
      return b(var1, var1).b(p.a(var2)).map(q.a());
   }

   public io.reactivex.n a(YearMonth var1, co.uk.getmondo.d.h var2, String var3, String var4) {
      return b(var1, var1).b(l.a(var2, var3, var4)).map(m.a());
   }

   public v a(LocalDate var1, LocalDate var2) {
      return this.a.a(var1.m(), var2.m());
   }

   public v b() {
      return this.a.c().d(r.a());
   }
}
