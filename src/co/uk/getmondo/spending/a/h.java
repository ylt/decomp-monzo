package co.uk.getmondo.spending.a;

import co.uk.getmondo.d.aa;
import co.uk.getmondo.d.u;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0007\b\tB\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0012\u0010\u0003\u001a\u00020\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0005\u0010\u0006\u0082\u0001\u0003\n\u000b\f¨\u0006\r"},
   d2 = {"Lco/uk/getmondo/spending/data/SpendingGroup;", "", "()V", "spendingData", "Lco/uk/getmondo/spending/data/SpendingData;", "getSpendingData", "()Lco/uk/getmondo/spending/data/SpendingData;", "OfCategory", "OfMerchant", "OfPeer", "Lco/uk/getmondo/spending/data/SpendingGroup$OfMerchant;", "Lco/uk/getmondo/spending/data/SpendingGroup$OfPeer;", "Lco/uk/getmondo/spending/data/SpendingGroup$OfCategory;", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public abstract class h {
   private h() {
   }

   // $FF: synthetic method
   public h(kotlin.d.b.i var1) {
      this();
   }

   public abstract g a();

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003HÆ\u0003J\t\u0010\f\u001a\u00020\u0005HÆ\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0011HÖ\u0003J\t\u0010\u0012\u001a\u00020\u0013HÖ\u0001J\t\u0010\u0014\u001a\u00020\u0015HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0016"},
      d2 = {"Lco/uk/getmondo/spending/data/SpendingGroup$OfCategory;", "Lco/uk/getmondo/spending/data/SpendingGroup;", "spendingData", "Lco/uk/getmondo/spending/data/SpendingData;", "category", "Lco/uk/getmondo/model/Category;", "(Lco/uk/getmondo/spending/data/SpendingData;Lco/uk/getmondo/model/Category;)V", "getCategory", "()Lco/uk/getmondo/model/Category;", "getSpendingData", "()Lco/uk/getmondo/spending/data/SpendingData;", "component1", "component2", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a extends h {
      private final g a;
      private final co.uk.getmondo.d.h b;

      public a(g var1, co.uk.getmondo.d.h var2) {
         kotlin.d.b.l.b(var1, "spendingData");
         kotlin.d.b.l.b(var2, "category");
         super((kotlin.d.b.i)null);
         this.a = var1;
         this.b = var2;
      }

      public g a() {
         return this.a;
      }

      public final co.uk.getmondo.d.h b() {
         return this.b;
      }

      public boolean equals(Object var1) {
         boolean var2;
         if(this != var1) {
            label28: {
               if(var1 instanceof h.a) {
                  h.a var3 = (h.a)var1;
                  if(kotlin.d.b.l.a(this.a(), var3.a()) && kotlin.d.b.l.a(this.b, var3.b)) {
                     break label28;
                  }
               }

               var2 = false;
               return var2;
            }
         }

         var2 = true;
         return var2;
      }

      public int hashCode() {
         int var2 = 0;
         g var3 = this.a();
         int var1;
         if(var3 != null) {
            var1 = var3.hashCode();
         } else {
            var1 = 0;
         }

         co.uk.getmondo.d.h var4 = this.b;
         if(var4 != null) {
            var2 = var4.hashCode();
         }

         return var1 * 31 + var2;
      }

      public String toString() {
         return "OfCategory(spendingData=" + this.a() + ", category=" + this.b + ")";
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003HÆ\u0003J\t\u0010\f\u001a\u00020\u0005HÆ\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0011HÖ\u0003J\t\u0010\u0012\u001a\u00020\u0013HÖ\u0001J\t\u0010\u0014\u001a\u00020\u0015HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0016"},
      d2 = {"Lco/uk/getmondo/spending/data/SpendingGroup$OfMerchant;", "Lco/uk/getmondo/spending/data/SpendingGroup;", "spendingData", "Lco/uk/getmondo/spending/data/SpendingData;", "merchant", "Lco/uk/getmondo/model/Merchant;", "(Lco/uk/getmondo/spending/data/SpendingData;Lco/uk/getmondo/model/Merchant;)V", "getMerchant", "()Lco/uk/getmondo/model/Merchant;", "getSpendingData", "()Lco/uk/getmondo/spending/data/SpendingData;", "component1", "component2", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class b extends h {
      private final g a;
      private final u b;

      public b(g var1, u var2) {
         kotlin.d.b.l.b(var1, "spendingData");
         kotlin.d.b.l.b(var2, "merchant");
         super((kotlin.d.b.i)null);
         this.a = var1;
         this.b = var2;
      }

      public g a() {
         return this.a;
      }

      public final u b() {
         return this.b;
      }

      public boolean equals(Object var1) {
         boolean var2;
         if(this != var1) {
            label28: {
               if(var1 instanceof h.b) {
                  h.b var3 = (h.b)var1;
                  if(kotlin.d.b.l.a(this.a(), var3.a()) && kotlin.d.b.l.a(this.b, var3.b)) {
                     break label28;
                  }
               }

               var2 = false;
               return var2;
            }
         }

         var2 = true;
         return var2;
      }

      public int hashCode() {
         int var2 = 0;
         g var3 = this.a();
         int var1;
         if(var3 != null) {
            var1 = var3.hashCode();
         } else {
            var1 = 0;
         }

         u var4 = this.b;
         if(var4 != null) {
            var2 = var4.hashCode();
         }

         return var1 * 31 + var2;
      }

      public String toString() {
         return "OfMerchant(spendingData=" + this.a() + ", merchant=" + this.b + ")";
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003HÆ\u0003J\t\u0010\f\u001a\u00020\u0005HÆ\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0011HÖ\u0003J\t\u0010\u0012\u001a\u00020\u0013HÖ\u0001J\t\u0010\u0014\u001a\u00020\u0015HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0016"},
      d2 = {"Lco/uk/getmondo/spending/data/SpendingGroup$OfPeer;", "Lco/uk/getmondo/spending/data/SpendingGroup;", "spendingData", "Lco/uk/getmondo/spending/data/SpendingData;", "peer", "Lco/uk/getmondo/model/Peer;", "(Lco/uk/getmondo/spending/data/SpendingData;Lco/uk/getmondo/model/Peer;)V", "getPeer", "()Lco/uk/getmondo/model/Peer;", "getSpendingData", "()Lco/uk/getmondo/spending/data/SpendingData;", "component1", "component2", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class c extends h {
      private final g a;
      private final aa b;

      public c(g var1, aa var2) {
         kotlin.d.b.l.b(var1, "spendingData");
         kotlin.d.b.l.b(var2, "peer");
         super((kotlin.d.b.i)null);
         this.a = var1;
         this.b = var2;
      }

      public g a() {
         return this.a;
      }

      public final aa b() {
         return this.b;
      }

      public boolean equals(Object var1) {
         boolean var2;
         if(this != var1) {
            label28: {
               if(var1 instanceof h.c) {
                  h.c var3 = (h.c)var1;
                  if(kotlin.d.b.l.a(this.a(), var3.a()) && kotlin.d.b.l.a(this.b, var3.b)) {
                     break label28;
                  }
               }

               var2 = false;
               return var2;
            }
         }

         var2 = true;
         return var2;
      }

      public int hashCode() {
         int var2 = 0;
         g var3 = this.a();
         int var1;
         if(var3 != null) {
            var1 = var3.hashCode();
         } else {
            var1 = 0;
         }

         aa var4 = this.b;
         if(var4 != null) {
            var2 = var4.hashCode();
         }

         return var1 * 31 + var2;
      }

      public String toString() {
         return "OfPeer(spendingData=" + this.a() + ", peer=" + this.b + ")";
      }
   }
}
