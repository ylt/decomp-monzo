package co.uk.getmondo.spending.a;

import android.util.Pair;
import io.realm.av;

// $FF: synthetic class
final class s implements kotlin.d.a.b {
   private final Pair a;
   private final co.uk.getmondo.d.h b;

   private s(Pair var1, co.uk.getmondo.d.h var2) {
      this.a = var1;
      this.b = var2;
   }

   public static kotlin.d.a.b a(Pair var0, co.uk.getmondo.d.h var1) {
      return new s(var0, var1);
   }

   public Object a(Object var1) {
      return i.a(this.a, this.b, (av)var1);
   }
}
