package co.uk.getmondo.spending.a;

import co.uk.getmondo.d.aj;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import org.threeten.bp.YearMonth;

public class b {
   private static final co.uk.getmondo.common.i.c a;
   private static final TimeZone b;

   static {
      a = co.uk.getmondo.common.i.c.a;
      b = TimeZone.getTimeZone("UTC");
   }

   // $FF: synthetic method
   static int a(aj var0, aj var1) {
      return var0.v().compareTo(var1.v());
   }

   // $FF: synthetic method
   static int a(h var0, h var1) {
      long var2 = var0.a().a().k();
      return Long.compare(var1.a().a().k(), var2) * -1;
   }

   private static g a(Map var0, Map var1, a var2, co.uk.getmondo.common.i.a var3) {
      ArrayList var7 = new ArrayList(var0.size());
      Iterator var6 = var0.keySet().iterator();

      while(var6.hasNext()) {
         Object var4 = var6.next();
         co.uk.getmondo.common.i.a var5 = (co.uk.getmondo.common.i.a)var0.get(var4);
         var7.add(var2.a(new g(var5.a(), var5.b()), (aj)var1.get(var4)));
      }

      Collections.sort(var7, c.a());
      return new g(var3.a(), var3.b(), var7);
   }

   // $FF: synthetic method
   static void a(List var0, a var1, io.reactivex.o var2) throws Exception {
      if(co.uk.getmondo.common.k.b.a(var0)) {
         var2.a();
      } else {
         Collections.sort(var0, Collections.reverseOrder(e.a()));
         co.uk.getmondo.common.i.a var9 = new co.uk.getmondo.common.i.a(a);
         HashMap var8 = new HashMap();
         HashMap var10 = new HashMap();
         YearMonth var5 = co.uk.getmondo.common.c.a.b(((aj)var0.get(0)).v(), b);

         YearMonth var13;
         for(int var3 = 0; var3 < var0.size(); var5 = var13) {
            aj var12 = (aj)var0.get(var3);
            Object var11 = var1.a(var12);
            if(var11 != null) {
               co.uk.getmondo.common.i.a var7 = (co.uk.getmondo.common.i.a)var8.get(var11);
               co.uk.getmondo.common.i.a var6 = var7;
               if(var7 == null) {
                  var6 = new co.uk.getmondo.common.i.a(a);
                  var8.put(var11, var6);
               }

               var9.a(var12.g());
               var6.a(var12.g());
               if(!var10.containsKey(var11)) {
                  var10.put(var11, var12);
               }
            }

            boolean var4;
            if(var3 == var0.size() - 1) {
               var4 = true;
            } else {
               var4 = false;
            }

            if(var4) {
               var13 = null;
            } else {
               var13 = co.uk.getmondo.common.c.a.b(((aj)var0.get(var3 + 1)).v(), b);
            }

            label59: {
               if(var13 == null || var5.b(var13)) {
                  var2.a(new android.support.v4.g.j(var5, a(var8, var10, var1, var9)));
                  if(!var4) {
                     var8.clear();
                     var10.clear();
                     var9.c();
                     break label59;
                  }
               }

               var13 = var5;
            }

            ++var3;
         }

         var2.a();
      }

   }

   public io.reactivex.n a(List var1, a var2) {
      return io.reactivex.n.create(d.a(var1, var2));
   }
}
