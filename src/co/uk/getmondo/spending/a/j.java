package co.uk.getmondo.spending.a;

import java.util.concurrent.Callable;
import org.threeten.bp.YearMonth;

// $FF: synthetic class
final class j implements Callable {
   private final YearMonth a;
   private final YearMonth b;

   private j(YearMonth var1, YearMonth var2) {
      this.a = var1;
      this.b = var2;
   }

   public static Callable a(YearMonth var0, YearMonth var1) {
      return new j(var0, var1);
   }

   public Object call() {
      return i.a(this.a, this.b);
   }
}
