package co.uk.getmondo.spending.a;

import android.util.Pair;

// $FF: synthetic class
final class l implements io.reactivex.c.h {
   private final co.uk.getmondo.d.h a;
   private final String b;
   private final String c;

   private l(co.uk.getmondo.d.h var1, String var2, String var3) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
   }

   public static io.reactivex.c.h a(co.uk.getmondo.d.h var0, String var1, String var2) {
      return new l(var0, var1, var2);
   }

   public Object a(Object var1) {
      return i.a(this.a, this.b, this.c, (Pair)var1);
   }
}
