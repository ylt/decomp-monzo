package co.uk.getmondo.spending.a;

import android.util.Pair;
import io.realm.av;

// $FF: synthetic class
final class k implements kotlin.d.a.b {
   private final Pair a;
   private final co.uk.getmondo.d.h b;
   private final String c;
   private final String d;

   private k(Pair var1, co.uk.getmondo.d.h var2, String var3, String var4) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
      this.d = var4;
   }

   public static kotlin.d.a.b a(Pair var0, co.uk.getmondo.d.h var1, String var2, String var3) {
      return new k(var0, var1, var2, var3);
   }

   public Object a(Object var1) {
      return i.a(this.a, this.b, this.c, this.d, (av)var1);
   }
}
