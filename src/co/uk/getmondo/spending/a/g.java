package co.uk.getmondo.spending.a;

import java.util.List;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B'\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u000e\b\u0002\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007¢\u0006\u0002\u0010\tJ\t\u0010\u0010\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0011\u001a\u00020\u0005HÆ\u0003J\u000f\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\b0\u0007HÆ\u0003J-\u0010\u0013\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\u000e\b\u0002\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007HÆ\u0001J\u0013\u0010\u0014\u001a\u00020\u00152\b\u0010\u0016\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0017\u001a\u00020\u0005HÖ\u0001J\t\u0010\u0018\u001a\u00020\u0019HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0017\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000f¨\u0006\u001a"},
   d2 = {"Lco/uk/getmondo/spending/data/SpendingData;", "", "totalSpent", "Lco/uk/getmondo/model/Amount;", "numberOfTransactions", "", "spendingBreakdown", "", "Lco/uk/getmondo/spending/data/SpendingGroup;", "(Lco/uk/getmondo/model/Amount;ILjava/util/List;)V", "getNumberOfTransactions", "()I", "getSpendingBreakdown", "()Ljava/util/List;", "getTotalSpent", "()Lco/uk/getmondo/model/Amount;", "component1", "component2", "component3", "copy", "equals", "", "other", "hashCode", "toString", "", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class g {
   private final co.uk.getmondo.d.c a;
   private final int b;
   private final List c;

   public g(co.uk.getmondo.d.c var1, int var2) {
      this(var1, var2, (List)null, 4, (kotlin.d.b.i)null);
   }

   public g(co.uk.getmondo.d.c var1, int var2, List var3) {
      kotlin.d.b.l.b(var1, "totalSpent");
      kotlin.d.b.l.b(var3, "spendingBreakdown");
      super();
      this.a = var1;
      this.b = var2;
      this.c = var3;
   }

   // $FF: synthetic method
   public g(co.uk.getmondo.d.c var1, int var2, List var3, int var4, kotlin.d.b.i var5) {
      if((var4 & 4) != 0) {
         var3 = kotlin.a.m.a();
      }

      this(var1, var2, var3);
   }

   public final co.uk.getmondo.d.c a() {
      return this.a;
   }

   public final int b() {
      return this.b;
   }

   public final List c() {
      return this.c;
   }

   public boolean equals(Object var1) {
      boolean var4 = false;
      boolean var3;
      if(this != var1) {
         var3 = var4;
         if(!(var1 instanceof g)) {
            return var3;
         }

         g var5 = (g)var1;
         var3 = var4;
         if(!kotlin.d.b.l.a(this.a, var5.a)) {
            return var3;
         }

         boolean var2;
         if(this.b == var5.b) {
            var2 = true;
         } else {
            var2 = false;
         }

         var3 = var4;
         if(!var2) {
            return var3;
         }

         var3 = var4;
         if(!kotlin.d.b.l.a(this.c, var5.c)) {
            return var3;
         }
      }

      var3 = true;
      return var3;
   }

   public int hashCode() {
      int var2 = 0;
      co.uk.getmondo.d.c var4 = this.a;
      int var1;
      if(var4 != null) {
         var1 = var4.hashCode();
      } else {
         var1 = 0;
      }

      int var3 = this.b;
      List var5 = this.c;
      if(var5 != null) {
         var2 = var5.hashCode();
      }

      return (var1 * 31 + var3) * 31 + var2;
   }

   public String toString() {
      return "SpendingData(totalSpent=" + this.a + ", numberOfTransactions=" + this.b + ", spendingBreakdown=" + this.c + ")";
   }
}
