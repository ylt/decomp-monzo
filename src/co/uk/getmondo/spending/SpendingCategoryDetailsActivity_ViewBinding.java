package co.uk.getmondo.spending;

import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class SpendingCategoryDetailsActivity_ViewBinding implements Unbinder {
   private SpendingCategoryDetailsActivity a;

   public SpendingCategoryDetailsActivity_ViewBinding(SpendingCategoryDetailsActivity var1, View var2) {
      this.a = var1;
      var1.appBarLayout = (AppBarLayout)Utils.findRequiredViewAsType(var2, 2131821096, "field 'appBarLayout'", AppBarLayout.class);
      var1.toolbar = (Toolbar)Utils.findRequiredViewAsType(var2, 2131820798, "field 'toolbar'", Toolbar.class);
      var1.tabLayout = (TabLayout)Utils.findRequiredViewAsType(var2, 2131821097, "field 'tabLayout'", TabLayout.class);
      var1.viewPager = (ViewPager)Utils.findRequiredViewAsType(var2, 2131821095, "field 'viewPager'", ViewPager.class);
   }

   public void unbind() {
      SpendingCategoryDetailsActivity var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.appBarLayout = null;
         var1.toolbar = null;
         var1.tabLayout = null;
         var1.viewPager = null;
      }
   }
}
