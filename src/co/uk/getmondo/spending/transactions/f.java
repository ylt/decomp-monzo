package co.uk.getmondo.spending.transactions;

import android.support.v4.g.j;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.d.aj;
import co.uk.getmondo.d.m;
import io.reactivex.u;
import java.util.Iterator;
import java.util.List;
import org.threeten.bp.YearMonth;

public class f extends co.uk.getmondo.common.ui.b {
   private final u c;
   private final co.uk.getmondo.spending.a.i d;
   private final co.uk.getmondo.common.a e;
   private final YearMonth f;
   private final co.uk.getmondo.d.h g;
   private final String h;
   private final String i;

   f(u var1, co.uk.getmondo.spending.a.i var2, co.uk.getmondo.common.a var3, YearMonth var4, co.uk.getmondo.d.h var5, String var6, String var7) {
      this.c = var1;
      this.d = var2;
      this.e = var3;
      this.f = var4;
      this.g = var5;
      this.h = var6;
      this.i = var7;
   }

   // $FF: synthetic method
   static j a(List var0) throws Exception {
      co.uk.getmondo.common.i.a var2 = new co.uk.getmondo.common.i.a(co.uk.getmondo.common.i.c.a);
      Iterator var1 = var0.iterator();

      while(var1.hasNext()) {
         var2.a(((aj)((m)var1.next()).e().a()).g());
      }

      return j.a(var0, var2.a());
   }

   // $FF: synthetic method
   static void a(f var0, f.a var1, j var2) throws Exception {
      var1.a((List)var2.a, (co.uk.getmondo.d.c)var2.b, var0.f);
   }

   public void a(f.a var1) {
      super.a((co.uk.getmondo.common.ui.f)var1);
      co.uk.getmondo.common.a var3 = this.e;
      Impression var2;
      if(this.h == null && this.i == null) {
         var2 = Impression.a(this.g, this.f);
      } else {
         var2 = Impression.c(this.g, this.f);
      }

      var3.a(var2);
      this.a((io.reactivex.b.b)this.d.a(this.f, this.g, this.h, this.i).observeOn(this.c).map(g.a()).subscribe(h.a(this, var1), i.a()));
   }

   interface a extends co.uk.getmondo.common.ui.f {
      void a(List var1, co.uk.getmondo.d.c var2, YearMonth var3);
   }
}
