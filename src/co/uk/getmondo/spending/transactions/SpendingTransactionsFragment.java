package co.uk.getmondo.spending.transactions;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import co.uk.getmondo.d.aj;
import co.uk.getmondo.d.m;
import co.uk.getmondo.transaction.details.TransactionDetailsActivity;
import java.util.List;
import org.threeten.bp.YearMonth;

public class SpendingTransactionsFragment extends co.uk.getmondo.common.f.a implements co.uk.getmondo.feed.adapter.a.a, f.a {
   f a;
   SpendingFeedAdapter c;
   private Unbinder d;
   @BindView(2131821377)
   RecyclerView recyclerView;

   public static SpendingTransactionsFragment a(YearMonth var0, co.uk.getmondo.d.h var1) {
      Bundle var2 = new Bundle();
      var2.putSerializable("year_month", var0);
      var2.putSerializable("category", var1);
      SpendingTransactionsFragment var3 = new SpendingTransactionsFragment();
      var3.setArguments(var2);
      return var3;
   }

   public static SpendingTransactionsFragment a(YearMonth var0, co.uk.getmondo.d.h var1, String var2) {
      Bundle var3 = new Bundle();
      var3.putSerializable("year_month", var0);
      var3.putString("merchant_group_id", var2);
      var3.putSerializable("category", var1);
      SpendingTransactionsFragment var4 = new SpendingTransactionsFragment();
      var4.setArguments(var3);
      return var4;
   }

   public static SpendingTransactionsFragment b(YearMonth var0, co.uk.getmondo.d.h var1, String var2) {
      Bundle var3 = new Bundle();
      var3.putSerializable("year_month", var0);
      var3.putString("peer_id", var2);
      var3.putSerializable("category", var1);
      SpendingTransactionsFragment var4 = new SpendingTransactionsFragment();
      var4.setArguments(var3);
      return var4;
   }

   public void a(m var1) {
      if(var1.j() == co.uk.getmondo.feed.a.a.a.a) {
         this.getActivity().startActivity(TransactionDetailsActivity.a(this.getActivity(), ((aj)var1.e().a()).w()));
      }

   }

   public void a(List var1, co.uk.getmondo.d.c var2, YearMonth var3) {
      this.c.a(var1, var2, var3);
   }

   public void b(m var1) {
   }

   public void onCreate(Bundle var1) {
      super.onCreate(var1);
      YearMonth var3 = (YearMonth)this.getArguments().get("year_month");
      co.uk.getmondo.d.h var5 = (co.uk.getmondo.d.h)this.getArguments().get("category");
      String var4 = this.getArguments().getString("merchant_group_id");
      String var2 = this.getArguments().getString("peer_id");
      this.B().a(new d(var3, var5, var4, var2)).a(this);
   }

   public View onCreateView(LayoutInflater var1, ViewGroup var2, Bundle var3) {
      return var1.inflate(2131034283, var2, false);
   }

   public void onDestroyView() {
      this.a.b();
      this.d.unbind();
      super.onDestroyView();
   }

   public void onViewCreated(View var1, Bundle var2) {
      super.onViewCreated(var1, var2);
      this.d = ButterKnife.bind(this, (View)var1);
      LinearLayoutManager var4 = new LinearLayoutManager(this.getActivity());
      this.c.a(this);
      this.recyclerView.setLayoutManager(var4);
      this.recyclerView.setHasFixedSize(true);
      this.recyclerView.setAdapter(this.c);
      int var3 = this.getResources().getDimensionPixelSize(2131427609);
      this.recyclerView.a(new co.uk.getmondo.common.ui.h(this.getActivity(), this.c, var3));
      this.recyclerView.a(new a.a.a.a.a.b(this.c));
      this.a.a((f.a)this);
   }
}
