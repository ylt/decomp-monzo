package co.uk.getmondo.spending.transactions;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import butterknife.BindView;
import butterknife.ButterKnife;
import org.threeten.bp.YearMonth;

public class SpendingTransactionsActivity extends co.uk.getmondo.common.activities.b {
   @BindView(2131820798)
   Toolbar toolbar;

   public static void a(Context var0, YearMonth var1, co.uk.getmondo.d.h var2, String var3, String var4) {
      var0.startActivity(b(var0, var1, var2, var3, var4));
   }

   public static Intent b(Context var0, YearMonth var1, co.uk.getmondo.d.h var2, String var3, String var4) {
      Intent var5 = new Intent(var0, SpendingTransactionsActivity.class);
      var5.putExtra("EXTRA_YEAR_MONTH", var1);
      var5.putExtra("EXTRA_CATEGORY", var2);
      var5.putExtra("EXTRA_TITLE", var3);
      var5.putExtra("EXTRA_MERCHANT_GROUP_ID", var4);
      return var5;
   }

   public static void c(Context var0, YearMonth var1, co.uk.getmondo.d.h var2, String var3, String var4) {
      Intent var5 = new Intent(var0, SpendingTransactionsActivity.class);
      var5.putExtra("EXTRA_YEAR_MONTH", var1);
      var5.putExtra("EXTRA_CATEGORY", var2);
      var5.putExtra("EXTRA_TITLE", var3);
      var5.putExtra("EXTRA_PEER_ID", var4);
      var0.startActivity(var5);
   }

   public void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.setContentView(2131034212);
      ButterKnife.bind((Activity)this);
      this.setSupportActionBar(this.toolbar);
      co.uk.getmondo.d.h var3 = (co.uk.getmondo.d.h)this.getIntent().getSerializableExtra("EXTRA_CATEGORY");
      String var4 = this.getIntent().getStringExtra("EXTRA_TITLE");
      this.toolbar.setTitle(var4);
      int var2 = android.support.v4.content.a.c(this, var3.b());
      this.toolbar.setBackgroundColor(var2);
      this.getWindow().setStatusBarColor(co.uk.getmondo.common.k.c.a(var2));
      if(var1 == null) {
         String var5 = this.getIntent().getStringExtra("EXTRA_MERCHANT_GROUP_ID");
         YearMonth var6 = (YearMonth)this.getIntent().getSerializableExtra("EXTRA_YEAR_MONTH");
         var4 = this.getIntent().getStringExtra("EXTRA_PEER_ID");
         SpendingTransactionsFragment var7;
         if(var5 != null) {
            var7 = SpendingTransactionsFragment.a(var6, var3, var5);
         } else {
            var7 = SpendingTransactionsFragment.b(var6, var3, var4);
         }

         this.getSupportFragmentManager().a().a(2131821098, var7).c();
      }

   }
}
