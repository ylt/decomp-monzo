package co.uk.getmondo.spending.transactions;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class SpendingTransactionsFragment_ViewBinding implements Unbinder {
   private SpendingTransactionsFragment a;

   public SpendingTransactionsFragment_ViewBinding(SpendingTransactionsFragment var1, View var2) {
      this.a = var1;
      var1.recyclerView = (RecyclerView)Utils.findRequiredViewAsType(var2, 2131821377, "field 'recyclerView'", RecyclerView.class);
   }

   public void unbind() {
      SpendingTransactionsFragment var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.recyclerView = null;
      }
   }
}
