package co.uk.getmondo.spending.transactions;

import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import co.uk.getmondo.common.ui.AmountView;

public class SpendingFeedAdapter$SpendingHeaderViewHolder_ViewBinding implements Unbinder {
   private SpendingFeedAdapter.SpendingHeaderViewHolder a;

   public SpendingFeedAdapter$SpendingHeaderViewHolder_ViewBinding(SpendingFeedAdapter.SpendingHeaderViewHolder var1, View var2) {
      this.a = var1;
      var1.amountView = (AmountView)Utils.findRequiredViewAsType(var2, 2131821615, "field 'amountView'", AmountView.class);
      var1.monthNameTextView = (TextView)Utils.findRequiredViewAsType(var2, 2131821616, "field 'monthNameTextView'", TextView.class);
   }

   public void unbind() {
      SpendingFeedAdapter.SpendingHeaderViewHolder var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.amountView = null;
         var1.monthNameTextView = null;
      }
   }
}
