package co.uk.getmondo.spending.transactions;

import android.support.v7.widget.Toolbar;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class SpendingTransactionsActivity_ViewBinding implements Unbinder {
   private SpendingTransactionsActivity a;

   public SpendingTransactionsActivity_ViewBinding(SpendingTransactionsActivity var1, View var2) {
      this.a = var1;
      var1.toolbar = (Toolbar)Utils.findRequiredViewAsType(var2, 2131820798, "field 'toolbar'", Toolbar.class);
   }

   public void unbind() {
      SpendingTransactionsActivity var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.toolbar = null;
      }
   }
}
