package co.uk.getmondo.spending.transactions;

import android.support.v4.g.j;

// $FF: synthetic class
final class h implements io.reactivex.c.g {
   private final f a;
   private final f.a b;

   private h(f var1, f.a var2) {
      this.a = var1;
      this.b = var2;
   }

   public static io.reactivex.c.g a(f var0, f.a var1) {
      return new h(var0, var1);
   }

   public void a(Object var1) {
      f.a(this.a, this.b, (j)var1);
   }
}
