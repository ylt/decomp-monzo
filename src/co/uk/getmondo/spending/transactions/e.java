package co.uk.getmondo.spending.transactions;

import io.reactivex.u;

public final class e implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final d b;
   private final javax.a.a c;
   private final javax.a.a d;
   private final javax.a.a e;

   static {
      boolean var0;
      if(!e.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   public e(d var1, javax.a.a var2, javax.a.a var3, javax.a.a var4) {
      if(!a && var1 == null) {
         throw new AssertionError();
      } else {
         this.b = var1;
         if(!a && var2 == null) {
            throw new AssertionError();
         } else {
            this.c = var2;
            if(!a && var3 == null) {
               throw new AssertionError();
            } else {
               this.d = var3;
               if(!a && var4 == null) {
                  throw new AssertionError();
               } else {
                  this.e = var4;
               }
            }
         }
      }
   }

   public static b.a.b a(d var0, javax.a.a var1, javax.a.a var2, javax.a.a var3) {
      return new e(var0, var1, var2, var3);
   }

   public f a() {
      return (f)b.a.d.a(this.b.a((u)this.c.b(), (co.uk.getmondo.spending.a.i)this.d.b(), (co.uk.getmondo.common.a)this.e.b()), "Cannot return null from a non-@Nullable @Provides method");
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
