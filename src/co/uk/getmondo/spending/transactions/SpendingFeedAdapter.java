package co.uk.getmondo.spending.transactions;

import android.support.v4.g.j;
import android.support.v7.widget.RecyclerView.w;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.uk.getmondo.common.ui.AmountView;
import java.util.List;
import java.util.Locale;
import org.threeten.bp.YearMonth;
import org.threeten.bp.format.TextStyle;

class SpendingFeedAdapter extends co.uk.getmondo.feed.adapter.a {
   private static final int b = co.uk.getmondo.feed.a.a.a.values().length;
   private j c;

   SpendingFeedAdapter(co.uk.getmondo.feed.a.a var1) {
      super(var1);
   }

   private int b(int var1) {
      if(this.c != null) {
         --var1;
      }

      return var1;
   }

   public long a(int var1) {
      long var2;
      if(this.c != null && var1 == 0) {
         var2 = -1L;
      } else {
         var2 = super.a(this.b(var1));
      }

      return var2;
   }

   public void a(w var1, int var2) {
      super.a(var1, this.b(var2));
   }

   void a(List var1, co.uk.getmondo.d.c var2, YearMonth var3) {
      this.c = j.a(var3, var2);
      this.a = var1;
      this.notifyDataSetChanged();
   }

   public int getItemCount() {
      int var2 = super.getItemCount();
      byte var1;
      if(this.c != null) {
         var1 = 1;
      } else {
         var1 = 0;
      }

      return var1 + var2;
   }

   public long getItemId(int var1) {
      return super.getItemId(this.b(var1));
   }

   public int getItemViewType(int var1) {
      if(this.c != null && var1 == 0) {
         var1 = b;
      } else {
         var1 = super.getItemViewType(this.b(var1));
      }

      return var1;
   }

   public void onBindViewHolder(w var1, int var2) {
      if(var1 instanceof SpendingFeedAdapter.SpendingHeaderViewHolder) {
         ((SpendingFeedAdapter.SpendingHeaderViewHolder)var1).a((YearMonth)this.c.a, (co.uk.getmondo.d.c)this.c.b);
      } else {
         super.onBindViewHolder(var1, this.b(var2));
      }

   }

   public w onCreateViewHolder(ViewGroup var1, int var2) {
      Object var3;
      if(var2 == b) {
         var3 = new SpendingFeedAdapter.SpendingHeaderViewHolder(LayoutInflater.from(var1.getContext()).inflate(2131034377, var1, false));
      } else {
         var3 = super.onCreateViewHolder(var1, var2);
      }

      return (w)var3;
   }

   static class SpendingHeaderViewHolder extends w {
      @BindView(2131821615)
      AmountView amountView;
      @BindView(2131821616)
      TextView monthNameTextView;

      SpendingHeaderViewHolder(View var1) {
         super(var1);
         ButterKnife.bind(this, (View)var1);
      }

      void a(YearMonth var1, co.uk.getmondo.d.c var2) {
         this.amountView.setAmount(var2);
         this.monthNameTextView.setText(var1.c().a(TextStyle.a, Locale.ENGLISH));
      }
   }
}
