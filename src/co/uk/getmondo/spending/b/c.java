package co.uk.getmondo.spending.b;

import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.common.v;
import co.uk.getmondo.common.e.e;
import co.uk.getmondo.common.ui.f;
import co.uk.getmondo.spending.a.i;
import io.reactivex.n;
import io.reactivex.r;
import io.reactivex.u;
import io.reactivex.z;
import io.reactivex.c.g;
import io.reactivex.c.h;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.a.m;
import kotlin.d.b.ab;
import kotlin.d.b.l;
import org.threeten.bp.LocalDate;
import org.threeten.bp.YearMonth;
import org.threeten.bp.format.DateTimeFormatter;
import org.threeten.bp.format.FormatStyle;
import org.threeten.bp.format.TextStyle;
import org.threeten.bp.temporal.TemporalAccessor;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0016B;\b\u0007\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\f\u001a\u00020\r¢\u0006\u0002\u0010\u000eJ\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H\u0002J\u0010\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0002H\u0016R\u000e\u0010\f\u001a\u00020\rX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0017"},
   d2 = {"Lco/uk/getmondo/spending/export/ExportPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/spending/export/ExportPresenter$View;", "ioScheduler", "Lio/reactivex/Scheduler;", "uiScheduler", "spendingRepository", "Lco/uk/getmondo/spending/data/SpendingRepository;", "resourceProvider", "Lco/uk/getmondo/common/ResourceProvider;", "transactionExporter", "Lco/uk/getmondo/spending/export/data/TransactionExporter;", "analyticsService", "Lco/uk/getmondo/common/AnalyticsService;", "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/spending/data/SpendingRepository;Lco/uk/getmondo/common/ResourceProvider;Lco/uk/getmondo/spending/export/data/TransactionExporter;Lco/uk/getmondo/common/AnalyticsService;)V", "getPeriodDisplayName", "", "yearMonth", "Lorg/threeten/bp/YearMonth;", "register", "", "view", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class c extends co.uk.getmondo.common.ui.b {
   private final u c;
   private final u d;
   private final i e;
   private final v f;
   private final co.uk.getmondo.spending.b.a.a g;
   private final co.uk.getmondo.common.a h;

   public c(u var1, u var2, i var3, v var4, co.uk.getmondo.spending.b.a.a var5, co.uk.getmondo.common.a var6) {
      l.b(var1, "ioScheduler");
      l.b(var2, "uiScheduler");
      l.b(var3, "spendingRepository");
      l.b(var4, "resourceProvider");
      l.b(var5, "transactionExporter");
      l.b(var6, "analyticsService");
      super();
      this.c = var1;
      this.d = var2;
      this.e = var3;
      this.f = var4;
      this.g = var5;
      this.h = var6;
   }

   private final String a(YearMonth var1) {
      return "" + var1.c().a(TextStyle.a, Locale.getDefault()) + ' ' + var1.b();
   }

   public void a(final c.a var1) {
      l.b(var1, "view");
      super.a((f)var1);
      io.reactivex.v var3 = var1.c().d((h)(new h() {
         public final List a(List var1) {
            l.b(var1, "it");
            Iterable var2 = (Iterable)var1;
            Collection var6 = (Collection)(new ArrayList(m.a(var2, 10)));
            Iterator var7 = var2.iterator();

            while(var7.hasNext()) {
               YearMonth var3 = (YearMonth)var7.next();
               c var5 = c.this;
               YearMonth var4 = YearMonth.a(var3.b(), var3.c());
               l.a(var4, "YearMonth.of(it.year, it.month)");
               String var10 = var5.a(var4);
               LocalDate var11 = var3.c(1);
               l.a(var11, "it.atDay(1)");
               LocalDate var8 = var3.f();
               l.a(var8, "it.atEndOfMonth()");
               co.uk.getmondo.spending.b.a.a.c var9 = new co.uk.getmondo.spending.b.a.a.c(var11, var8, var10, co.uk.getmondo.spending.b.a.a.c.a.a);
               var6.add(var9);
            }

            return (List)var6;
         }
      }));
      io.reactivex.v var4 = this.e.b().d((h)(new h() {
         public final co.uk.getmondo.spending.b.a.a.c a(LocalDate var1) {
            l.b(var1, "it");
            String var4 = DateTimeFormatter.a(FormatStyle.d).a((TemporalAccessor)var1);
            String var3 = c.this.f.a(2131362736);
            LocalDate var2 = LocalDate.a();
            l.a(var2, "LocalDate.now()");
            ab var5 = ab.a;
            Object[] var6 = new Object[]{var4};
            var3 = String.format(var3, Arrays.copyOf(var6, var6.length));
            l.a(var3, "java.lang.String.format(format, *args)");
            return new co.uk.getmondo.spending.b.a.a.c(var1, var2, var3, co.uk.getmondo.spending.b.a.a.c.a.b);
         }
      }));
      io.reactivex.b.a var2 = this.b;
      l.a(var4, "allExportPeriod");
      var3 = var3.a((z)var4, (io.reactivex.c.c)(new io.reactivex.c.c() {
         public final Object a(Object var1, Object var2) {
            co.uk.getmondo.spending.b.a.a.c var3 = (co.uk.getmondo.spending.b.a.a.c)var2;
            return m.a((Collection)((List)var1), var3);
         }
      }));
      l.a(var3, "zipWith(other, BiFunctio…-> zipper.invoke(t, u) })");
      io.reactivex.b.b var6 = var3.e((g)(new g() {
         public final void a(List var1x) {
            c.a var2 = var1;
            l.a(var1x, "exportPeriods");
            var2.a(var1x);
         }
      }));
      l.a(var6, "monthExportPeriods\n     …Periods(exportPeriods) })");
      this.b = co.uk.getmondo.common.j.f.a(var2, var6);
      var2 = this.b;
      n var7 = var1.d().doOnNext((g)(new g() {
         public final void a(co.uk.getmondo.spending.b.a.a.c var1x) {
            var1.b(kotlin.a.g.m((Object[])co.uk.getmondo.spending.b.a.a.b.a.values()));
         }
      })).zipWith((r)var1.e(), (io.reactivex.c.c)(new io.reactivex.c.c() {
         public final Object a(Object var1, Object var2) {
            co.uk.getmondo.spending.b.a.a.b var3 = (co.uk.getmondo.spending.b.a.a.b)var2;
            return kotlin.l.a((co.uk.getmondo.spending.b.a.a.c)var1, var3);
         }
      }));
      l.a(var7, "zipWith(other, BiFunctio…-> zipper.invoke(t, u) })");
      io.reactivex.b.b var5 = var7.flatMapSingle((h)(new h() {
         public final io.reactivex.v a(kotlin.h var1) {
            l.b(var1, "<name for destructuring parameter 0>");
            final co.uk.getmondo.spending.b.a.a.c var2 = (co.uk.getmondo.spending.b.a.a.c)var1.c();
            final co.uk.getmondo.spending.b.a.a.b var3 = (co.uk.getmondo.spending.b.a.a.b)var1.d();
            return c.this.e.a(var2.a(), var2.b()).d((h)(new h() {
               public final co.uk.getmondo.spending.b.a.a.a a(List var1) {
                  l.b(var1, "it");
                  co.uk.getmondo.spending.b.a.a.b var2x = var3;
                  l.a(var2x, "exportOutput");
                  return new co.uk.getmondo.spending.b.a.a.a(var2x, var2.a(c.this.f), var1);
               }
            })).a((h)(new h() {
               public final io.reactivex.v a(co.uk.getmondo.spending.b.a.a.a var1) {
                  l.b(var1, "it");
                  return c.this.g.a(var1);
               }
            })).b(c.this.c).a(c.this.d).c((g)(new g() {
               public final void a(String var1) {
                  co.uk.getmondo.common.a var2 = c.this.h;
                  Impression.Companion var4 = Impression.Companion;
                  String var3x = var3.a().name();
                  if(var3x == null) {
                     throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                  } else {
                     var3x = var3x.toLowerCase();
                     l.a(var3x, "(this as java.lang.String).toLowerCase()");
                     var2.a(Impression.Companion.a(var4, (String)null, var3x, 1, (Object)null));
                  }
               }
            }));
         }
      })).subscribe((g)(new g() {
         public final void a(String var1x) {
            c.a var2 = var1;
            l.a(var1x, "it");
            var2.a(var1x);
         }
      }), (g)(new g() {
         public final void a(Throwable var1x) {
            d.a.a.a(var1x);
            var1.b(2131362732);
         }
      }));
      l.a(var5, "view.exportPeriodClicks\n…                       })");
      this.b = co.uk.getmondo.common.j.f.a(var2, var5);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0000\bf\u0018\u00002\u00020\u00012\u00020\u0002J\u0016\u0010\u0011\u001a\u00020\u00122\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00140\rH&J\u0016\u0010\u0015\u001a\u00020\u00122\f\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\t0\rH&J\u0010\u0010\u0017\u001a\u00020\u00122\u0006\u0010\u0018\u001a\u00020\u0019H&R\u0018\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0006\u0010\u0007R\u0018\u0010\b\u001a\b\u0012\u0004\u0012\u00020\t0\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\n\u0010\u0007R\u001e\u0010\u000b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000e0\r0\fX¦\u0004¢\u0006\u0006\u001a\u0004\b\u000f\u0010\u0010¨\u0006\u001a"},
      d2 = {"Lco/uk/getmondo/spending/export/ExportPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ErrorView;", "exportOutputClicks", "Lio/reactivex/Observable;", "Lco/uk/getmondo/spending/export/data/model/ExportOutput;", "getExportOutputClicks", "()Lio/reactivex/Observable;", "exportPeriodClicks", "Lco/uk/getmondo/spending/export/data/model/ExportPeriod;", "getExportPeriodClicks", "months", "Lio/reactivex/Single;", "", "Lorg/threeten/bp/YearMonth;", "getMonths", "()Lio/reactivex/Single;", "showExportFormats", "", "formats", "Lco/uk/getmondo/spending/export/data/model/ExportOutput$Format;", "showExportPeriods", "periods", "showExportShare", "filePathname", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends e, f {
      void a(String var1);

      void a(List var1);

      void b(List var1);

      io.reactivex.v c();

      n d();

      n e();
   }
}
