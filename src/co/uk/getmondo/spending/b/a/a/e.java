package co.uk.getmondo.spending.b.a.a;

import co.uk.getmondo.d.aj;
import co.uk.getmondo.d.u;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import kotlin.Metadata;
import kotlin.a.m;
import kotlin.d.b.i;
import kotlin.d.b.l;
import kotlin.h.j;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u001a\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0011\n\u0002\b\u0004\b\u0086\b\u0018\u0000 &2\u00020\u0001:\u0001&Be\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0006\u0010\u0007\u001a\u00020\u0003\u0012\u0006\u0010\b\u001a\u00020\u0003\u0012\u0006\u0010\t\u001a\u00020\u0003\u0012\u0006\u0010\n\u001a\u00020\u0003\u0012\u0006\u0010\u000b\u001a\u00020\u0003\u0012\u0006\u0010\f\u001a\u00020\u0003\u0012\u0006\u0010\r\u001a\u00020\u0003\u0012\u0006\u0010\u000e\u001a\u00020\u0003¢\u0006\u0002\u0010\u000fJ\t\u0010\u0010\u001a\u00020\u0003HÂ\u0003J\t\u0010\u0011\u001a\u00020\u0003HÂ\u0003J\t\u0010\u0012\u001a\u00020\u0003HÂ\u0003J\t\u0010\u0013\u001a\u00020\u0003HÂ\u0003J\t\u0010\u0014\u001a\u00020\u0003HÂ\u0003J\t\u0010\u0015\u001a\u00020\u0003HÂ\u0003J\t\u0010\u0016\u001a\u00020\u0003HÂ\u0003J\t\u0010\u0017\u001a\u00020\u0003HÂ\u0003J\t\u0010\u0018\u001a\u00020\u0003HÂ\u0003J\t\u0010\u0019\u001a\u00020\u0003HÂ\u0003J\t\u0010\u001a\u001a\u00020\u0003HÂ\u0003J\t\u0010\u001b\u001a\u00020\u0003HÂ\u0003J\u0081\u0001\u0010\u001c\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u00032\b\b\u0002\u0010\u0007\u001a\u00020\u00032\b\b\u0002\u0010\b\u001a\u00020\u00032\b\b\u0002\u0010\t\u001a\u00020\u00032\b\b\u0002\u0010\n\u001a\u00020\u00032\b\b\u0002\u0010\u000b\u001a\u00020\u00032\b\b\u0002\u0010\f\u001a\u00020\u00032\b\b\u0002\u0010\r\u001a\u00020\u00032\b\b\u0002\u0010\u000e\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\u001d\u001a\u00020\u001e2\b\u0010\u001f\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010 \u001a\u00020!HÖ\u0001J\u0011\u0010\"\u001a\b\u0012\u0004\u0012\u00020\u00030#¢\u0006\u0002\u0010$J\t\u0010%\u001a\u00020\u0003HÖ\u0001R\u000e\u0010\f\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006'"},
   d2 = {"Lco/uk/getmondo/spending/export/data/model/TransactionRecordItem;", "", "id", "", "created", "amount", "currency", "localAmount", "localCurrency", "category", "emoji", "description", "address", "notes", "receipt", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "component1", "component10", "component11", "component12", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "", "other", "hashCode", "", "toComponentRecord", "", "()[Ljava/lang/String;", "toString", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class e {
   public static final e.a a = new e.a((i)null);
   private static final SimpleDateFormat n = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS Z", Locale.getDefault());
   private static final co.uk.getmondo.common.i.b o = new co.uk.getmondo.common.i.b(false, true, true, 1, (i)null);
   private static final String[] p;
   private final String b;
   private final String c;
   private final String d;
   private final String e;
   private final String f;
   private final String g;
   private final String h;
   private final String i;
   private final String j;
   private final String k;
   private final String l;
   private final String m;

   static {
      Object[] var0 = (Object[])(new String[]{"id", "created", "amount", "currency", "local_amount", "local_currency", "category", "emoji", "description", "address", "notes", "receipt"});
      p = (String[])var0;
   }

   public e(String var1, String var2, String var3, String var4, String var5, String var6, String var7, String var8, String var9, String var10, String var11, String var12) {
      l.b(var1, "id");
      l.b(var2, "created");
      l.b(var3, "amount");
      l.b(var4, "currency");
      l.b(var5, "localAmount");
      l.b(var6, "localCurrency");
      l.b(var7, "category");
      l.b(var8, "emoji");
      l.b(var9, "description");
      l.b(var10, "address");
      l.b(var11, "notes");
      l.b(var12, "receipt");
      super();
      this.b = var1;
      this.c = var2;
      this.d = var3;
      this.e = var4;
      this.f = var5;
      this.g = var6;
      this.h = var7;
      this.i = var8;
      this.j = var9;
      this.k = var10;
      this.l = var11;
      this.m = var12;
   }

   public final String[] a() {
      Object[] var1 = (Object[])(new String[]{this.b, this.c, this.d, this.e, this.f, this.g, this.h, this.i, this.j, this.k, this.l, this.m});
      return (String[])var1;
   }

   public boolean equals(Object var1) {
      boolean var2;
      label57: {
         if(this != var1) {
            if(!(var1 instanceof e)) {
               break label57;
            }

            e var3 = (e)var1;
            if(!l.a(this.b, var3.b) || !l.a(this.c, var3.c) || !l.a(this.d, var3.d) || !l.a(this.e, var3.e) || !l.a(this.f, var3.f) || !l.a(this.g, var3.g) || !l.a(this.h, var3.h) || !l.a(this.i, var3.i) || !l.a(this.j, var3.j) || !l.a(this.k, var3.k) || !l.a(this.l, var3.l) || !l.a(this.m, var3.m)) {
               break label57;
            }
         }

         var2 = true;
         return var2;
      }

      var2 = false;
      return var2;
   }

   public int hashCode() {
      int var12 = 0;
      String var13 = this.b;
      int var1;
      if(var13 != null) {
         var1 = var13.hashCode();
      } else {
         var1 = 0;
      }

      var13 = this.c;
      int var2;
      if(var13 != null) {
         var2 = var13.hashCode();
      } else {
         var2 = 0;
      }

      var13 = this.d;
      int var3;
      if(var13 != null) {
         var3 = var13.hashCode();
      } else {
         var3 = 0;
      }

      var13 = this.e;
      int var4;
      if(var13 != null) {
         var4 = var13.hashCode();
      } else {
         var4 = 0;
      }

      var13 = this.f;
      int var5;
      if(var13 != null) {
         var5 = var13.hashCode();
      } else {
         var5 = 0;
      }

      var13 = this.g;
      int var6;
      if(var13 != null) {
         var6 = var13.hashCode();
      } else {
         var6 = 0;
      }

      var13 = this.h;
      int var7;
      if(var13 != null) {
         var7 = var13.hashCode();
      } else {
         var7 = 0;
      }

      var13 = this.i;
      int var8;
      if(var13 != null) {
         var8 = var13.hashCode();
      } else {
         var8 = 0;
      }

      var13 = this.j;
      int var9;
      if(var13 != null) {
         var9 = var13.hashCode();
      } else {
         var9 = 0;
      }

      var13 = this.k;
      int var10;
      if(var13 != null) {
         var10 = var13.hashCode();
      } else {
         var10 = 0;
      }

      var13 = this.l;
      int var11;
      if(var13 != null) {
         var11 = var13.hashCode();
      } else {
         var11 = 0;
      }

      var13 = this.m;
      if(var13 != null) {
         var12 = var13.hashCode();
      }

      return (var11 + (var10 + (var9 + (var8 + (var7 + (var6 + (var5 + (var4 + (var3 + (var2 + var1 * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31) * 31 + var12;
   }

   public String toString() {
      return "TransactionRecordItem(id=" + this.b + ", created=" + this.c + ", amount=" + this.d + ", currency=" + this.e + ", localAmount=" + this.f + ", localCurrency=" + this.g + ", category=" + this.h + ", emoji=" + this.i + ", description=" + this.j + ", address=" + this.k + ", notes=" + this.l + ", receipt=" + this.m + ")";
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0011\u001a\u00020\u00052\u0006\u0010\u0012\u001a\u00020\u0013J\u000e\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0017J\u000e\u0010\u0018\u001a\u00020\u00052\u0006\u0010\u0016\u001a\u00020\u0017R\u0019\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004¢\u0006\n\n\u0002\u0010\b\u001a\u0004\b\u0006\u0010\u0007R\u0014\u0010\t\u001a\u00020\nX\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0014\u0010\r\u001a\u00020\u000eX\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010¨\u0006\u0019"},
      d2 = {"Lco/uk/getmondo/spending/export/data/model/TransactionRecordItem$Companion;", "", "()V", "HEADERS", "", "", "getHEADERS", "()[Ljava/lang/String;", "[Ljava/lang/String;", "amountFormatter", "Lco/uk/getmondo/common/money/AmountFormatter;", "getAmountFormatter", "()Lco/uk/getmondo/common/money/AmountFormatter;", "dateFormatter", "Ljava/text/SimpleDateFormat;", "getDateFormatter", "()Ljava/text/SimpleDateFormat;", "formatAddress", "merchant", "Lco/uk/getmondo/model/Merchant;", "from", "Lco/uk/getmondo/spending/export/data/model/TransactionRecordItem;", "transaction", "Lco/uk/getmondo/model/Transaction;", "getDescription", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(i var1) {
         this();
      }

      private final SimpleDateFormat b() {
         return e.n;
      }

      private final co.uk.getmondo.common.i.b c() {
         return e.o;
      }

      public final e a(aj var1) {
         l.b(var1, "transaction");
         String var7 = var1.w();
         l.a(var7, "transaction.id");
         String var8 = ((e.a)this).b().format(var1.v());
         l.a(var8, "dateFormatter.format(transaction.created)");
         co.uk.getmondo.common.i.b var3 = ((e.a)this).c();
         co.uk.getmondo.d.c var2 = var1.g();
         l.a(var2, "transaction.amount");
         String var9 = var3.a(var2);
         String var17 = var1.g().l().b();
         if(var17 == null) {
            var17 = "";
         }

         var3 = ((e.a)this).c();
         co.uk.getmondo.d.c var4 = var1.h();
         l.a(var4, "transaction.localAmount");
         String var10 = var3.a(var4);
         String var19 = var1.h().l().b();
         if(var19 == null) {
            var19 = "";
         }

         String var11;
         String var21;
         label42: {
            var11 = var1.c().f();
            l.a(var11, "transaction.category.apiValue");
            u var20 = var1.f();
            if(var20 != null) {
               var21 = var20.l();
               if(var21 != null) {
                  break label42;
               }
            }

            var21 = "";
         }

         String var12;
         String var22;
         label37: {
            var12 = ((e.a)this).b(var1);
            u var6 = var1.f();
            if(var6 != null) {
               e.a var5 = e.a;
               l.a(var6, "it");
               var22 = var5.a(var6);
               if(var22 != null) {
                  break label37;
               }
            }

            var22 = "";
         }

         String var23 = var1.e();
         if(var23 == null) {
            var23 = "";
         }

         Iterable var13 = (Iterable)var1.A();
         Collection var14 = (Collection)(new ArrayList(m.a(var13, 10)));
         Iterator var24 = var13.iterator();

         while(var24.hasNext()) {
            var14.add(((co.uk.getmondo.d.d)var24.next()).b());
         }

         List var15 = (List)var14;
         String var16;
         if(var15.isEmpty()) {
            var16 = "";
         } else {
            var16 = var15.toString();
         }

         e var18 = new e(var7, var8, var9, var17, var10, var19, var11, var21, var12, var22, var23, var16);
         return var18;
      }

      public final String a(u var1) {
         l.b(var1, "merchant");
         String var2;
         if(var1.k()) {
            var2 = "Online transaction";
         } else {
            Object var3 = var1.c().a((Object)"");
            l.a(var3, "merchant.formattedAddress.orElse(\"\")");
            var2 = (String)var3;
         }

         return var2;
      }

      public final String[] a() {
         return e.p;
      }

      public final String b(aj var1) {
         l.b(var1, "transaction");
         String var5;
         if(var1.f() != null) {
            u var4 = var1.f();
            if(var4 == null) {
               l.a();
            }

            var5 = var4.i();
            l.a(var5, "transaction.merchant!!.name");
         } else {
            if(var1.B() != null) {
               String var3 = var1.B().b();
               if(var1.g().a()) {
                  var5 = "Payment to " + var3;
               } else {
                  var5 = "Payment from " + var3;
               }
            } else {
               boolean var2;
               if(!j.a((CharSequence)var1.x())) {
                  var2 = true;
               } else {
                  var2 = false;
               }

               if(var2) {
                  var5 = var1.x();
               } else {
                  var5 = "";
               }
            }

            l.a(var5, "if (transaction.peer != …         \"\"\n            }");
         }

         return var5;
      }
   }
}
