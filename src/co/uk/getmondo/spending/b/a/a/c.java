package co.uk.getmondo.spending.b.a.a;

import co.uk.getmondo.common.v;
import java.util.Locale;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.d.b.l;
import org.threeten.bp.LocalDate;
import org.threeten.bp.format.TextStyle;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\b\u0086\b\u0018\u00002\u00020\u0001:\u0001\u001fB%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b¢\u0006\u0002\u0010\tJ\t\u0010\u0011\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0012\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0013\u001a\u00020\u0006HÆ\u0003J\t\u0010\u0014\u001a\u00020\bHÆ\u0003J1\u0010\u0015\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00062\b\b\u0002\u0010\u0007\u001a\u00020\bHÆ\u0001J\u0013\u0010\u0016\u001a\u00020\u00172\b\u0010\u0018\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\u000e\u0010\u0019\u001a\u00020\u00062\u0006\u0010\u001a\u001a\u00020\u001bJ\t\u0010\u001c\u001a\u00020\u001dHÖ\u0001J\t\u0010\u001e\u001a\u00020\u0006HÖ\u0001R\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0011\u0010\u0007\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\r¨\u0006 "},
   d2 = {"Lco/uk/getmondo/spending/export/data/model/ExportPeriod;", "", "startDate", "Lorg/threeten/bp/LocalDate;", "endDate", "displayText", "", "length", "Lco/uk/getmondo/spending/export/data/model/ExportPeriod$Length;", "(Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;Ljava/lang/String;Lco/uk/getmondo/spending/export/data/model/ExportPeriod$Length;)V", "getDisplayText", "()Ljava/lang/String;", "getEndDate", "()Lorg/threeten/bp/LocalDate;", "getLength", "()Lco/uk/getmondo/spending/export/data/model/ExportPeriod$Length;", "getStartDate", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "getDescription", "resourceProvider", "Lco/uk/getmondo/common/ResourceProvider;", "hashCode", "", "toString", "Length", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class c {
   private final LocalDate a;
   private final LocalDate b;
   private final String c;
   private final c.a d;

   public c(LocalDate var1, LocalDate var2, String var3, c.a var4) {
      l.b(var1, "startDate");
      l.b(var2, "endDate");
      l.b(var3, "displayText");
      l.b(var4, "length");
      super();
      this.a = var1;
      this.b = var2;
      this.c = var3;
      this.d = var4;
   }

   public final String a(v var1) {
      l.b(var1, "resourceProvider");
      c.a var2 = this.d;
      String var3;
      switch(d.a[var2.ordinal()]) {
      case 1:
         var3 = "" + this.a.f().a(TextStyle.a, Locale.getDefault()) + ' ' + this.a.d();
         break;
      case 2:
         var3 = var1.a(2131362731);
         break;
      default:
         throw new NoWhenBranchMatchedException();
      }

      return var3;
   }

   public final LocalDate a() {
      return this.a;
   }

   public final LocalDate b() {
      return this.b;
   }

   public final String c() {
      return this.c;
   }

   public boolean equals(Object var1) {
      boolean var2;
      if(this != var1) {
         label32: {
            if(var1 instanceof c) {
               c var3 = (c)var1;
               if(l.a(this.a, var3.a) && l.a(this.b, var3.b) && l.a(this.c, var3.c) && l.a(this.d, var3.d)) {
                  break label32;
               }
            }

            var2 = false;
            return var2;
         }
      }

      var2 = true;
      return var2;
   }

   public int hashCode() {
      int var4 = 0;
      LocalDate var5 = this.a;
      int var1;
      if(var5 != null) {
         var1 = var5.hashCode();
      } else {
         var1 = 0;
      }

      var5 = this.b;
      int var2;
      if(var5 != null) {
         var2 = var5.hashCode();
      } else {
         var2 = 0;
      }

      String var6 = this.c;
      int var3;
      if(var6 != null) {
         var3 = var6.hashCode();
      } else {
         var3 = 0;
      }

      c.a var7 = this.d;
      if(var7 != null) {
         var4 = var7.hashCode();
      }

      return (var3 + (var2 + var1 * 31) * 31) * 31 + var4;
   }

   public String toString() {
      return "ExportPeriod(startDate=" + this.a + ", endDate=" + this.b + ", displayText=" + this.c + ", length=" + this.d + ")";
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0004\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004¨\u0006\u0005"},
      d2 = {"Lco/uk/getmondo/spending/export/data/model/ExportPeriod$Length;", "", "(Ljava/lang/String;I)V", "MONTH", "ALL_TIME", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static enum a {
      a,
      b;

      static {
         c.a var1 = new c.a("MONTH", 0);
         a = var1;
         c.a var0 = new c.a("ALL_TIME", 1);
         b = var0;
      }
   }
}
