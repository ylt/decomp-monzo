package co.uk.getmondo.spending.b;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.FileProvider;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;
import co.uk.getmondo.MonzoApplication;
import io.reactivex.v;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.n;
import kotlin.a.m;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000|\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u00012\u00020\u0002B\u001d\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u000e\b\u0002\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006¢\u0006\u0002\u0010\bJ2\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020$2\f\u0010%\u001a\b\u0012\u0004\u0012\u00020$0\u00062\u0012\u0010&\u001a\u000e\u0012\u0004\u0012\u00020(\u0012\u0004\u0012\u00020\"0'H\u0002J\b\u0010)\u001a\u00020\"H\u0016J\u0012\u0010*\u001a\u00020\"2\b\u0010+\u001a\u0004\u0018\u00010,H\u0014J\b\u0010-\u001a\u00020\"H\u0016J\u0010\u0010.\u001a\u00020\"2\u0006\u0010/\u001a\u00020(H\u0016J\u0012\u0010.\u001a\u00020\"2\b\u00100\u001a\u0004\u0018\u00010$H\u0016J\u0016\u00101\u001a\u00020\"2\f\u00102\u001a\b\u0012\u0004\u0012\u0002030\u0006H\u0016J\u0016\u00104\u001a\u00020\"2\f\u00105\u001a\b\u0012\u0004\u0012\u00020\u00120\u0006H\u0016J\u0010\u00106\u001a\u00020\"2\u0006\u00107\u001a\u00020$H\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\n8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\f\u0010\rR2\u0010\u000e\u001a&\u0012\f\u0012\n \u0010*\u0004\u0018\u00010\u000b0\u000b \u0010*\u0012\u0012\f\u0012\n \u0010*\u0004\u0018\u00010\u000b0\u000b\u0018\u00010\u000f0\u000fX\u0082\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00120\n8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0013\u0010\rR2\u0010\u0014\u001a&\u0012\f\u0012\n \u0010*\u0004\u0018\u00010\u00120\u0012 \u0010*\u0012\u0012\f\u0012\n \u0010*\u0004\u0018\u00010\u00120\u0012\u0018\u00010\u000f0\u000fX\u0082\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u0004¢\u0006\u0002\n\u0000R \u0010\u0015\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00070\u00060\u00168VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0017\u0010\u0018R\u001e\u0010\u0019\u001a\u00020\u001a8\u0000@\u0000X\u0081.¢\u0006\u000e\n\u0000\u001a\u0004\b\u001b\u0010\u001c\"\u0004\b\u001d\u0010\u001eR\u000e\u0010\u001f\u001a\u00020 X\u0082\u0004¢\u0006\u0002\n\u0000¨\u00068"},
   d2 = {"Lco/uk/getmondo/spending/export/ExportBottomSheet;", "Landroid/support/design/widget/BottomSheetDialog;", "Lco/uk/getmondo/spending/export/ExportPresenter$View;", "activity", "Landroid/app/Activity;", "initialMonths", "", "Lorg/threeten/bp/YearMonth;", "(Landroid/app/Activity;Ljava/util/List;)V", "exportOutputClicks", "Lio/reactivex/Observable;", "Lco/uk/getmondo/spending/export/data/model/ExportOutput;", "getExportOutputClicks", "()Lio/reactivex/Observable;", "exportOutputRelay", "Lcom/jakewharton/rxrelay2/PublishRelay;", "kotlin.jvm.PlatformType", "exportPeriodClicks", "Lco/uk/getmondo/spending/export/data/model/ExportPeriod;", "getExportPeriodClicks", "exportPeriodRelay", "months", "Lio/reactivex/Single;", "getMonths", "()Lio/reactivex/Single;", "presenter", "Lco/uk/getmondo/spending/export/ExportPresenter;", "getPresenter$app_monzoPrepaidRelease", "()Lco/uk/getmondo/spending/export/ExportPresenter;", "setPresenter$app_monzoPrepaidRelease", "(Lco/uk/getmondo/spending/export/ExportPresenter;)V", "sheetView", "Landroid/view/ViewGroup;", "bind", "", "title", "", "items", "clickListener", "Lkotlin/Function1;", "", "onAttachedToWindow", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onDetachedFromWindow", "showError", "stringRes", "message", "showExportFormats", "formats", "Lco/uk/getmondo/spending/export/data/model/ExportOutput$Format;", "showExportPeriods", "periods", "showExportShare", "filePathname", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class a extends android.support.design.widget.c implements c.a {
   public c b;
   private final com.b.b.c c;
   private final com.b.b.c d;
   private final ViewGroup e;
   private final Activity f;
   private final List g;

   public a(Activity var1, List var2) {
      l.b(var1, "activity");
      l.b(var2, "initialMonths");
      super((Context)var1);
      this.f = var1;
      this.g = var2;
      this.c = com.b.b.c.a();
      this.d = com.b.b.c.a();
      View var3 = View.inflate(this.getContext(), 2131034234, (ViewGroup)null);
      if(var3 == null) {
         throw new TypeCastException("null cannot be cast to non-null type android.widget.LinearLayout");
      } else {
         this.e = (ViewGroup)((LinearLayout)var3);
         this.setContentView((View)this.e);
      }
   }

   private final void a(String var1, List var2, final kotlin.d.a.b var3) {
      ((TextView)((View)this.e).findViewById(co.uk.getmondo.c.a.exportSheetTitleText)).setText((CharSequence)var1);
      this.e.removeViews(1, this.e.getChildCount() - 1);
      Iterator var6 = ((Iterable)var2).iterator();

      for(final int var4 = 0; var6.hasNext(); ++var4) {
         var1 = (String)var6.next();
         View var5 = View.inflate(this.e.getContext(), 2131034357, (ViewGroup)null);
         if(var5 == null) {
            throw new TypeCastException("null cannot be cast to non-null type android.widget.TextView");
         }

         TextView var7 = (TextView)var5;
         var7.setText((CharSequence)var1);
         var7.setOnClickListener((OnClickListener)(new OnClickListener() {
            public final void onClick(View var1) {
               var3.a(Integer.valueOf(var4));
               a.this.hide();
            }
         }));
         this.e.addView((View)var7, var4 + 1);
      }

   }

   public void a(String var1) {
      l.b(var1, "filePathname");
      File var3 = new File(var1);
      Uri var4 = FileProvider.a(this.getContext(), this.getContext().getString(2131362187), var3);
      Intent var2 = android.support.v4.app.ao.a.a(this.f).a(var4).a();
      var2.setData(var4);
      var2.addFlags(1);
      this.getContext().startActivity(var2);
      this.dismiss();
   }

   public void a(final List var1) {
      l.b(var1, "periods");
      String var2 = this.getContext().getString(2131362737);
      l.a(var2, "context.getString(R.stri…ding_export_period_title)");
      Iterable var4 = (Iterable)var1;
      Collection var3 = (Collection)(new ArrayList(m.a(var4, 10)));
      Iterator var5 = var4.iterator();

      while(var5.hasNext()) {
         var3.add(((co.uk.getmondo.spending.b.a.a.c)var5.next()).c());
      }

      this.a(var2, (List)var3, (kotlin.d.a.b)(new kotlin.d.a.b() {
         // $FF: synthetic method
         public Object a(Object var1x) {
            this.a(((Number)var1x).intValue());
            return n.a;
         }

         public final void a(int var1x) {
            a.this.c.a(var1.get(var1x));
         }
      }));
   }

   public void b(int var1) {
      this.b(this.getContext().getString(var1));
   }

   public void b(String var1) {
      Activity var3 = this.f;
      Activity var2 = var3;
      if(!(var3 instanceof co.uk.getmondo.common.activities.b)) {
         var2 = null;
      }

      co.uk.getmondo.common.activities.b var4 = (co.uk.getmondo.common.activities.b)var2;
      if(var4 != null) {
         var4.b(var1);
      }

   }

   public void b(final List var1) {
      l.b(var1, "formats");
      String var3 = this.getContext().getString(2131362735);
      l.a(var3, "context.getString(R.stri…ding_export_format_title)");
      Iterable var4 = (Iterable)var1;
      Collection var2 = (Collection)(new ArrayList(m.a(var4, 10)));
      Iterator var5 = var4.iterator();

      while(var5.hasNext()) {
         co.uk.getmondo.spending.b.a.a.b.a var6 = (co.uk.getmondo.spending.b.a.a.b.a)var5.next();
         var2.add(this.getContext().getString(var6.a()));
      }

      this.a(var3, (List)var2, (kotlin.d.a.b)(new kotlin.d.a.b() {
         // $FF: synthetic method
         public Object a(Object var1x) {
            this.a(((Number)var1x).intValue());
            return n.a;
         }

         public final void a(int var1x) {
            com.b.b.c var3 = a.this.d;
            co.uk.getmondo.spending.b.a.a.b.a var2 = (co.uk.getmondo.spending.b.a.a.b.a)var1.get(var1x);
            String var4 = a.this.getContext().getCacheDir().getAbsolutePath();
            l.a(var4, "context.cacheDir.absolutePath");
            var3.a((Object)(new co.uk.getmondo.spending.b.a.a.b(var2, var4)));
         }
      }));
      (new Handler()).post((Runnable)(new Runnable() {
         public final void run() {
            a.this.show();
         }
      }));
   }

   public v c() {
      v var1 = v.a((Object)this.g);
      l.a(var1, "Single.just(initialMonths)");
      return var1;
   }

   public io.reactivex.n d() {
      com.b.b.c var1 = this.c;
      l.a(var1, "exportPeriodRelay");
      return (io.reactivex.n)var1;
   }

   public io.reactivex.n e() {
      com.b.b.c var1 = this.d;
      l.a(var1, "exportOutputRelay");
      return (io.reactivex.n)var1;
   }

   public void onAttachedToWindow() {
      super.onAttachedToWindow();
      c var1 = this.b;
      if(var1 == null) {
         l.b("presenter");
      }

      var1.a((c.a)this);
   }

   protected void onCreate(Bundle var1) {
      super.onCreate(var1);
      co.uk.getmondo.common.h.b.a.a().a(MonzoApplication.a(this.getContext()).b()).a(new co.uk.getmondo.common.h.b.c(this.f)).a().a(this);
   }

   public void onDetachedFromWindow() {
      c var1 = this.b;
      if(var1 == null) {
         l.b("presenter");
      }

      var1.b();
      super.onDetachedFromWindow();
   }
}
