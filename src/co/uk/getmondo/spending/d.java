package co.uk.getmondo.spending;

import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.spending.a.i;
import io.reactivex.n;
import io.reactivex.u;
import io.reactivex.v;
import io.reactivex.c.g;
import io.reactivex.c.h;
import java.util.Collection;
import java.util.List;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0019B]\b\u0007\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0006\u001a\u00020\u0004\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\f\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u0006\u0010\u0013\u001a\u00020\u0014¢\u0006\u0002\u0010\u0015J\u0010\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0002H\u0016R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u001a"},
   d2 = {"Lco/uk/getmondo/spending/SpendingPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/spending/SpendingPresenter$View;", "ioScheduler", "Lio/reactivex/Scheduler;", "uiScheduler", "computation", "spendingRepository", "Lco/uk/getmondo/spending/data/SpendingRepository;", "spendingCalculator", "Lco/uk/getmondo/spending/data/SpendingCalculator;", "analyticsService", "Lco/uk/getmondo/common/AnalyticsService;", "recurringPaymentsManager", "Lco/uk/getmondo/payments/data/RecurringPaymentsManager;", "apiErrorHandler", "Lco/uk/getmondo/common/errors/ApiErrorHandler;", "cardManager", "Lco/uk/getmondo/card/CardManager;", "accountManager", "Lco/uk/getmondo/common/accounts/AccountManager;", "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/spending/data/SpendingRepository;Lco/uk/getmondo/spending/data/SpendingCalculator;Lco/uk/getmondo/common/AnalyticsService;Lco/uk/getmondo/payments/data/RecurringPaymentsManager;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/card/CardManager;Lco/uk/getmondo/common/accounts/AccountManager;)V", "register", "", "view", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class d extends co.uk.getmondo.common.ui.b {
   private final u c;
   private final u d;
   private final u e;
   private final i f;
   private final co.uk.getmondo.spending.a.b g;
   private final co.uk.getmondo.common.a h;
   private final co.uk.getmondo.payments.a.i i;
   private final co.uk.getmondo.common.e.a j;
   private final co.uk.getmondo.card.c k;
   private final co.uk.getmondo.common.accounts.b l;

   public d(u var1, u var2, u var3, i var4, co.uk.getmondo.spending.a.b var5, co.uk.getmondo.common.a var6, co.uk.getmondo.payments.a.i var7, co.uk.getmondo.common.e.a var8, co.uk.getmondo.card.c var9, co.uk.getmondo.common.accounts.b var10) {
      l.b(var1, "ioScheduler");
      l.b(var2, "uiScheduler");
      l.b(var3, "computation");
      l.b(var4, "spendingRepository");
      l.b(var5, "spendingCalculator");
      l.b(var6, "analyticsService");
      l.b(var7, "recurringPaymentsManager");
      l.b(var8, "apiErrorHandler");
      l.b(var9, "cardManager");
      l.b(var10, "accountManager");
      super();
      this.c = var1;
      this.d = var2;
      this.e = var3;
      this.f = var4;
      this.g = var5;
      this.h = var6;
      this.i = var7;
      this.j = var8;
      this.k = var9;
      this.l = var10;
   }

   public void a(final d.a var1) {
      l.b(var1, "view");
      super.a((co.uk.getmondo.common.ui.f)var1);
      this.h.a(Impression.Companion.S());
      io.reactivex.b.a var4 = this.b;
      n var5 = this.f.a().flatMapSingle((h)(new h() {
         public final v a(List var1) {
            l.b(var1, "transactions");
            return d.this.g.a(var1, co.uk.getmondo.spending.a.a.b).toList().b(d.this.e);
         }
      })).observeOn(this.d);
      g var6 = (g)(new g() {
         public final void a(List var1x) {
            boolean var2;
            if(!((Collection)var1x).isEmpty()) {
               var2 = true;
            } else {
               var2 = false;
            }

            if(!var2) {
               var1.c();
            } else {
               d.a var3 = var1;
               l.a(var1x, "spendingData");
               var3.a(var1x);
            }

            var1.a(var2);
         }
      });
      kotlin.d.a.b var3 = (kotlin.d.a.b)null.a;
      Object var2 = var3;
      if(var3 != null) {
         var2 = new e(var3);
      }

      io.reactivex.b.b var8 = var5.subscribe(var6, (g)var2);
      l.a(var8, "spendingRepository.allTr…            }, Timber::e)");
      this.b = co.uk.getmondo.common.j.f.a(var4, var8);
      io.reactivex.b.a var9 = this.b;
      var8 = this.k.a().observeOn(this.d).subscribe((g)(new g() {
         public final void a(co.uk.getmondo.d.g var1x) {
            if(var1x.h()) {
               var1.f();
            } else {
               var1.g();
            }

         }
      }), (g)null.a);
      l.a(var8, "cardManager.card()\n     …ed to get card state\") })");
      this.b = co.uk.getmondo.common.j.f.a(var9, var8);
      var9 = this.b;
      var8 = var1.a().subscribe((g)(new g() {
         public final void a(List var1x) {
            d.a var2 = var1;
            l.a(var1x, "it");
            var2.b(var1x);
         }
      }));
      l.a(var8, "view.exportClicks\n      …iew.showExportData(it) })");
      this.b = co.uk.getmondo.common.j.f.a(var9, var8);
      if(this.l.b()) {
         var9 = this.b;
         var8 = this.i.c().b(this.c).a(this.d).a((io.reactivex.c.a)null.a, (g)(new g() {
            public final void a(Throwable var1x) {
               co.uk.getmondo.common.e.a var2 = d.this.j;
               l.a(var1x, "error");
               if(!var2.a(var1x, (co.uk.getmondo.common.e.a.a)var1)) {
                  var1.b(2131362612);
               }

            }
         }));
         l.a(var8, "recurringPaymentsManager…  }\n                    )");
         this.b = co.uk.getmondo.common.j.f.a(var9, var8);
         io.reactivex.b.a var10 = this.b;
         io.reactivex.b.b var11 = this.i.g().observeOn(this.d).subscribe((g)(new g() {
            public final void a(kotlin.h var1x) {
               int var3 = ((Number)var1x.c()).intValue();
               int var2 = ((Number)var1x.d()).intValue();
               if(var3 + var2 > 0) {
                  var1.a(var3, var2);
               } else {
                  var1.e();
               }

            }
         }));
         l.a(var11, "recurringPaymentsManager…  }\n                    }");
         this.b = co.uk.getmondo.common.j.f.a(var10, var11);
         var10 = this.b;
         io.reactivex.b.b var7 = var1.b().subscribe((g)(new g() {
            public final void a(Object var1x) {
               var1.d();
            }
         }));
         l.a(var7, "view.onRecurringPayments…openRecurringPayments() }");
         this.b = co.uk.getmondo.common.j.f.a(var10, var7);
      }

   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\b\n\u0002\b\u0003\bf\u0018\u00002\u00020\u00012\u00020\u0002J\b\u0010\t\u001a\u00020\nH&J\u000e\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\f0\u0004H&J\b\u0010\r\u001a\u00020\nH&J\u0010\u0010\u000e\u001a\u00020\n2\u0006\u0010\u000f\u001a\u00020\u0010H&J\"\u0010\u0011\u001a\u00020\n2\u0018\u0010\u0012\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00140\u00130\u0005H&J\b\u0010\u0015\u001a\u00020\nH&J\b\u0010\u0016\u001a\u00020\nH&J\u0016\u0010\u0017\u001a\u00020\n2\f\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005H&J\u0018\u0010\u0019\u001a\u00020\n2\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001bH&J\b\u0010\u001d\u001a\u00020\nH&R\u001e\u0010\u0003\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0007\u0010\b¨\u0006\u001e"},
      d2 = {"Lco/uk/getmondo/spending/SpendingPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "exportClicks", "Lio/reactivex/Observable;", "", "Lorg/threeten/bp/YearMonth;", "getExportClicks", "()Lio/reactivex/Observable;", "hideRecurringPaymentsBanner", "", "onRecurringPaymentsClicked", "", "openRecurringPayments", "setExportEnabled", "isEnabled", "", "setSpendingData", "spendingData", "Landroid/support/v4/util/Pair;", "Lco/uk/getmondo/spending/data/SpendingData;", "showCardFrozen", "showEmptyNotice", "showExportData", "months", "showRecurringPaymentsBanner", "directDebitCount", "", "paymentSeriesCount", "showTitle", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      n a();

      void a(int var1, int var2);

      void a(List var1);

      void a(boolean var1);

      n b();

      void b(List var1);

      void c();

      void d();

      void e();

      void f();

      void g();
   }
}
