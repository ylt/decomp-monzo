package co.uk.getmondo.spending;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.g.j;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import co.uk.getmondo.main.HomeActivity;
import co.uk.getmondo.payments.recurring_list.RecurringPaymentsActivity;
import io.reactivex.n;
import io.reactivex.c.h;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.d.a.m;
import kotlin.d.b.i;
import kotlin.d.b.w;
import kotlin.d.b.y;
import kotlin.reflect.l;
import org.threeten.bp.YearMonth;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u0090\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0012\u0018\u0000 F2\u00020\u00012\u00020\u0002:\u0001FB\u0005¢\u0006\u0002\u0010\u0003J\"\u0010\u001f\u001a\u00020 2\u0018\u0010!\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020#0\"0\bH\u0002J\b\u0010$\u001a\u00020%H\u0016J\u0012\u0010&\u001a\u00020%2\b\u0010'\u001a\u0004\u0018\u00010(H\u0016J\u0018\u0010)\u001a\u00020%2\u0006\u0010*\u001a\u00020+2\u0006\u0010,\u001a\u00020-H\u0016J&\u0010.\u001a\u0004\u0018\u00010/2\u0006\u0010,\u001a\u0002002\b\u00101\u001a\u0004\u0018\u0001022\b\u0010'\u001a\u0004\u0018\u00010(H\u0016J\b\u00103\u001a\u00020%H\u0016J\u0010\u00104\u001a\u0002052\u0006\u00106\u001a\u00020\u0005H\u0016J\u000e\u00107\u001a\b\u0012\u0004\u0012\u00020\u000e0\u0007H\u0016J\u001a\u00108\u001a\u00020%2\u0006\u00109\u001a\u00020/2\b\u0010'\u001a\u0004\u0018\u00010(H\u0016J\b\u0010:\u001a\u00020%H\u0016J\u0010\u0010;\u001a\u00020%2\u0006\u0010<\u001a\u000205H\u0016J\"\u0010=\u001a\u00020%2\u0018\u0010!\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020#0\"0\bH\u0016J\b\u0010>\u001a\u00020%H\u0016J\b\u0010?\u001a\u00020%H\u0016J\u0016\u0010@\u001a\u00020%2\f\u0010A\u001a\b\u0012\u0004\u0012\u00020\t0\bH\u0016J\u0018\u0010B\u001a\u00020%2\u0006\u0010C\u001a\u00020 2\u0006\u0010D\u001a\u00020 H\u0016J\b\u0010E\u001a\u00020%H\u0016R\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0082\u000e¢\u0006\u0002\n\u0000R \u0010\u0006\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\t0\b0\u00078VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\n\u0010\u000bR2\u0010\f\u001a&\u0012\f\u0012\n \u000f*\u0004\u0018\u00010\u000e0\u000e \u000f*\u0012\u0012\f\u0012\n \u000f*\u0004\u0018\u00010\u000e0\u000e\u0018\u00010\r0\rX\u0082\u0004¢\u0006\u0002\n\u0000R\u001b\u0010\u0010\u001a\u00020\u00118BX\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0014\u0010\u0015\u001a\u0004\b\u0012\u0010\u0013R\u001e\u0010\u0016\u001a\u00020\u00178\u0000@\u0000X\u0081.¢\u0006\u000e\n\u0000\u001a\u0004\b\u0018\u0010\u0019\"\u0004\b\u001a\u0010\u001bR\u000e\u0010\u001c\u001a\u00020\u001dX\u0082\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u001e\u001a\u0004\u0018\u00010\tX\u0082\u000e¢\u0006\u0002\n\u0000¨\u0006G"},
   d2 = {"Lco/uk/getmondo/spending/SpendingFragment;", "Lco/uk/getmondo/common/fragments/BaseFragment;", "Lco/uk/getmondo/spending/SpendingPresenter$View;", "()V", "exportAction", "Landroid/view/MenuItem;", "exportClicks", "Lio/reactivex/Observable;", "", "Lorg/threeten/bp/YearMonth;", "getExportClicks", "()Lio/reactivex/Observable;", "exportRelay", "Lcom/jakewharton/rxrelay2/PublishRelay;", "", "kotlin.jvm.PlatformType", "linearLayoutManager", "Landroid/support/v7/widget/LinearLayoutManager;", "getLinearLayoutManager", "()Landroid/support/v7/widget/LinearLayoutManager;", "linearLayoutManager$delegate", "Lkotlin/Lazy;", "presenter", "Lco/uk/getmondo/spending/SpendingPresenter;", "getPresenter$app_monzoPrepaidRelease", "()Lco/uk/getmondo/spending/SpendingPresenter;", "setPresenter$app_monzoPrepaidRelease", "(Lco/uk/getmondo/spending/SpendingPresenter;)V", "spendingAdapter", "Lco/uk/getmondo/spending/SpendingAdapter;", "yearMonth", "findPositionByMonth", "", "spendingData", "Landroid/support/v4/util/Pair;", "Lco/uk/getmondo/spending/data/SpendingData;", "hideRecurringPaymentsBanner", "", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onCreateOptionsMenu", "menu", "Landroid/view/Menu;", "inflater", "Landroid/view/MenuInflater;", "onCreateView", "Landroid/view/View;", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "onDestroyView", "onOptionsItemSelected", "", "item", "onRecurringPaymentsClicked", "onViewCreated", "view", "openRecurringPayments", "setExportEnabled", "isEnabled", "setSpendingData", "showCardFrozen", "showEmptyNotice", "showExportData", "months", "showRecurringPaymentsBanner", "directDebitCount", "paymentSeriesCount", "showTitle", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class b extends co.uk.getmondo.common.f.a implements d.a {
   // $FF: synthetic field
   static final l[] a = new l[]{(l)y.a(new w(y.a(b.class), "linearLayoutManager", "getLinearLayoutManager()Landroid/support/v7/widget/LinearLayoutManager;"))};
   public static final b.a d = new b.a((i)null);
   public d c;
   private final com.b.b.c e = com.b.b.c.a();
   private final a f = new a((List)null, (m)null, 3, (i)null);
   private final kotlin.c g = kotlin.d.a((kotlin.d.a.a)(new kotlin.d.a.a() {
      public final LinearLayoutManager b() {
         return new LinearLayoutManager((Context)b.this.getActivity());
      }

      // $FF: synthetic method
      public Object v_() {
         return this.b();
      }
   }));
   private YearMonth h;
   private MenuItem i;
   private HashMap j;

   private final int c(List var1) {
      Iterator var4 = ((Iterable)kotlin.a.m.a((Collection)var1)).iterator();

      Object var5;
      while(true) {
         if(var4.hasNext()) {
            Object var3 = var4.next();
            if(!kotlin.d.b.l.a((YearMonth)((j)var1.get(((Number)var3).intValue())).a, this.h)) {
               continue;
            }

            var5 = var3;
            break;
         }

         var5 = null;
         break;
      }

      Integer var6 = (Integer)var5;
      int var2;
      if(var6 != null) {
         var2 = var6.intValue();
      } else {
         var2 = 0;
      }

      return var2;
   }

   private final LinearLayoutManager i() {
      kotlin.c var1 = this.g;
      l var2 = a[0];
      return (LinearLayoutManager)var1.a();
   }

   public View a(int var1) {
      if(this.j == null) {
         this.j = new HashMap();
      }

      View var3 = (View)this.j.get(Integer.valueOf(var1));
      View var2 = var3;
      if(var3 == null) {
         var2 = this.getView();
         if(var2 == null) {
            var2 = null;
         } else {
            var2 = var2.findViewById(var1);
            this.j.put(Integer.valueOf(var1), var2);
         }
      }

      return var2;
   }

   public n a() {
      n var1 = this.e.map((h)(new h() {
         // $FF: synthetic method
         public Object a(Object var1) {
            return this.b(var1);
         }

         public final List b(Object var1) {
            kotlin.d.b.l.b(var1, "it");
            int var2 = b.this.i().n();
            int var3 = b.this.i().p();
            Iterable var4 = (Iterable)b.this.f.a().subList(var2, var3 + 1);
            Collection var5 = (Collection)(new ArrayList(kotlin.a.m.a(var4, 10)));
            Iterator var6 = var4.iterator();

            while(var6.hasNext()) {
               var5.add((YearMonth)((j)var6.next()).a);
            }

            return (List)var5;
         }
      }));
      kotlin.d.b.l.a(var1, "exportRelay.map {\n      …ap { it.first }\n        }");
      return var1;
   }

   public void a(int var1, int var2) {
      StringBuilder var3 = new StringBuilder();
      if(var1 > 0) {
         var3.append(this.getResources().getQuantityString(2131886082, var1, new Object[]{Integer.valueOf(var1)}));
      }

      if(var1 > 0 && var2 > 0) {
         var3.append(", ");
      }

      if(var2 > 0) {
         var3.append(this.getResources().getQuantityString(2131886083, var2, new Object[]{Integer.valueOf(var2)}));
      }

      ((TextView)this.a(co.uk.getmondo.c.a.recurringBannerSubtitle)).setText((CharSequence)var3.toString());
      ((ConstraintLayout)this.a(co.uk.getmondo.c.a.recurringPaymentsBanner)).setVisibility(0);
      this.a(co.uk.getmondo.c.a.topShadow).setVisibility(0);
      android.support.v4.app.j var4 = this.getActivity();
      if(var4 == null) {
         throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.main.HomeActivity");
      } else {
         ((HomeActivity)var4).a(false);
      }
   }

   public void a(List var1) {
      kotlin.d.b.l.b(var1, "spendingData");
      this.f.a(var1);
      if(this.h != null) {
         ((RecyclerView)this.a(co.uk.getmondo.c.a.spendingRecyclerView)).a(this.c(var1));
         this.h = (YearMonth)null;
      }

      ((RecyclerView)this.a(co.uk.getmondo.c.a.spendingRecyclerView)).setVisibility(0);
      ((LinearLayout)this.a(co.uk.getmondo.c.a.spendingEmptyView)).setVisibility(8);
   }

   public void a(boolean var1) {
      MenuItem var2 = this.i;
      if(var2 != null) {
         var2.setVisible(var1);
      }

   }

   public n b() {
      n var1 = com.b.a.c.c.a((ConstraintLayout)this.a(co.uk.getmondo.c.a.recurringPaymentsBanner));
      kotlin.d.b.l.a(var1, "RxView.clicks(recurringPaymentsBanner)");
      return var1;
   }

   public void b(List var1) {
      kotlin.d.b.l.b(var1, "months");
      android.support.v4.app.j var2 = this.getActivity();
      kotlin.d.b.l.a(var2, "activity");
      (new co.uk.getmondo.spending.b.a((Activity)var2, var1)).show();
   }

   public void c() {
      ((LinearLayout)this.a(co.uk.getmondo.c.a.spendingEmptyView)).setVisibility(0);
      ((RecyclerView)this.a(co.uk.getmondo.c.a.spendingRecyclerView)).setVisibility(8);
   }

   public void d() {
      this.startActivity(new Intent((Context)this.getActivity(), RecurringPaymentsActivity.class));
   }

   public void e() {
      ((ConstraintLayout)this.a(co.uk.getmondo.c.a.recurringPaymentsBanner)).setVisibility(8);
      this.a(co.uk.getmondo.c.a.topShadow).setVisibility(8);
      android.support.v4.app.j var1 = this.getActivity();
      if(var1 == null) {
         throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.main.HomeActivity");
      } else {
         ((HomeActivity)var1).a(true);
      }
   }

   public void f() {
      ((Toolbar)this.a(co.uk.getmondo.c.a.toolbar)).setTitle(2131362490);
      ((Toolbar)this.a(co.uk.getmondo.c.a.toolbar)).setTitleTextColor(android.support.v4.content.a.c(this.getContext(), 17170443));
   }

   public void g() {
      ((Toolbar)this.a(co.uk.getmondo.c.a.toolbar)).setTitle(2131362481);
      ((Toolbar)this.a(co.uk.getmondo.c.a.toolbar)).setTitleTextColor(android.support.v4.content.a.c(this.getContext(), 2131689593));
   }

   public void h() {
      if(this.j != null) {
         this.j.clear();
      }

   }

   public void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.B().a(this);
      if(this.getArguments() != null && this.getArguments().containsKey("KEY_YEAR_MONTH")) {
         Serializable var2 = this.getArguments().getSerializable("KEY_YEAR_MONTH");
         if(var2 == null) {
            throw new TypeCastException("null cannot be cast to non-null type org.threeten.bp.YearMonth");
         }

         this.h = (YearMonth)var2;
      }

   }

   public void onCreateOptionsMenu(Menu var1, MenuInflater var2) {
      kotlin.d.b.l.b(var1, "menu");
      kotlin.d.b.l.b(var2, "inflater");
      super.onCreateOptionsMenu(var1, var2);
      var2.inflate(2131951623, var1);
      this.i = var1.findItem(2131821782);
   }

   public View onCreateView(LayoutInflater var1, ViewGroup var2, Bundle var3) {
      kotlin.d.b.l.b(var1, "inflater");
      return var1.inflate(2131034281, var2, false);
   }

   public void onDestroyView() {
      d var1 = this.c;
      if(var1 == null) {
         kotlin.d.b.l.b("presenter");
      }

      var1.b();
      super.onDestroyView();
      this.h();
   }

   public boolean onOptionsItemSelected(MenuItem var1) {
      kotlin.d.b.l.b(var1, "item");
      boolean var2;
      if(var1.getItemId() == 2131821782) {
         this.e.a((Object)kotlin.n.a);
         var2 = true;
      } else {
         var2 = super.onOptionsItemSelected(var1);
      }

      return var2;
   }

   public void onViewCreated(View var1, Bundle var2) {
      kotlin.d.b.l.b(var1, "view");
      super.onViewCreated(var1, var2);
      this.setHasOptionsMenu(true);
      this.f.a((m)(new m() {
         public final void a(YearMonth var1, co.uk.getmondo.spending.a.h var2) {
            kotlin.d.b.l.b(var1, "yearMonth");
            kotlin.d.b.l.b(var2, "spendingGroup");
            if(var2 instanceof co.uk.getmondo.spending.a.h.a) {
               co.uk.getmondo.d.h var3 = ((co.uk.getmondo.spending.a.h.a)var2).b();
               SpendingCategoryDetailsActivity.a((Context)b.this.getActivity(), var1, var3);
            }

         }
      }));
      ((RecyclerView)this.a(co.uk.getmondo.c.a.spendingRecyclerView)).setAdapter((android.support.v7.widget.RecyclerView.a)this.f);
      ((RecyclerView)this.a(co.uk.getmondo.c.a.spendingRecyclerView)).setLayoutManager((android.support.v7.widget.RecyclerView.h)this.i());
      d var3 = this.c;
      if(var3 == null) {
         kotlin.d.b.l.b("presenter");
      }

      var3.a((d.a)this);
      android.support.v4.app.j var4 = this.getActivity();
      if(var4 == null) {
         throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.main.HomeActivity");
      } else {
         HomeActivity var5 = (HomeActivity)var4;
         var5.setSupportActionBar((Toolbar)this.a(co.uk.getmondo.c.a.toolbar));
         Toolbar var6 = (Toolbar)this.a(co.uk.getmondo.c.a.toolbar);
         kotlin.d.b.l.a(var6, "toolbar");
         var5.a(var6);
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bR\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000¨\u0006\t"},
      d2 = {"Lco/uk/getmondo/spending/SpendingFragment$Companion;", "", "()V", "KEY_YEAR_MONTH", "", "newInstance", "Landroid/support/v4/app/Fragment;", "yearMonth", "Lorg/threeten/bp/YearMonth;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(i var1) {
         this();
      }
   }
}
