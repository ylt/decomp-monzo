package co.uk.getmondo.top;

import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.common.k.p;
import co.uk.getmondo.d.ak;
import io.reactivex.u;
import io.reactivex.v;

public class g extends co.uk.getmondo.common.ui.b {
   private final u c;
   private final u d;
   private final co.uk.getmondo.common.accounts.d e;
   private final co.uk.getmondo.common.e.a f;
   private final co.uk.getmondo.api.b.a g;
   private final co.uk.getmondo.common.a h;
   private final co.uk.getmondo.waitlist.i i;
   private final co.uk.getmondo.create_account.topup.c j;

   g(u var1, u var2, co.uk.getmondo.common.accounts.d var3, co.uk.getmondo.common.e.a var4, co.uk.getmondo.api.b.a var5, co.uk.getmondo.common.a var6, co.uk.getmondo.waitlist.i var7, co.uk.getmondo.create_account.topup.c var8) {
      this.c = var1;
      this.d = var2;
      this.e = var3;
      this.f = var4;
      this.g = var5;
      this.h = var6;
      this.i = var7;
      this.j = var8;
   }

   // $FF: synthetic method
   static void a(g var0, android.support.v4.g.j var1) throws Exception {
      ((g.a)var0.a).g();
      switch(null.a[((ak)var1.a).a().ordinal()]) {
      case 1:
         ((g.a)var0.a).b();
         break;
      case 2:
         ((g.a)var0.a).a((co.uk.getmondo.d.c)var1.b);
         break;
      case 3:
         ((g.a)var0.a).c();
         break;
      case 4:
         ((g.a)var0.a).d();
         break;
      case 5:
         ((g.a)var0.a).e();
      }

   }

   // $FF: synthetic method
   static void a(g var0, Throwable var1) throws Exception {
      var0.f.a(var1, (co.uk.getmondo.common.e.a.a)var0.a);
   }

   private void c() {
      ((g.a)this.a).f();
      this.a((io.reactivex.b.b)v.a(this.g.a(), this.j.b(), h.a()).b(this.d).a(this.c).a(i.a(this), j.a(this)));
   }

   void a() {
      ((g.a)this.a).a();
   }

   public void a(g.a var1) {
      super.a((co.uk.getmondo.common.ui.f)var1);
      this.h.a(Impression.i());
      var1.a(p.b(this.e.b().d().c()));
      this.c();
   }

   interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      void a();

      void a(co.uk.getmondo.d.c var1);

      void a(String var1);

      void b();

      void c();

      void d();

      void e();

      void f();

      void g();
   }
}
