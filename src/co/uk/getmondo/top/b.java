package co.uk.getmondo.top;

import android.app.Activity;
import android.content.Intent;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.common.s;
import co.uk.getmondo.d.ak;
import io.reactivex.u;

public class b extends co.uk.getmondo.common.ui.b {
   private final u c;
   private final u d;
   private final co.uk.getmondo.common.accounts.d e;
   private final co.uk.getmondo.common.e.a f;
   private final s g;
   private final co.uk.getmondo.api.b.a h;
   private final co.uk.getmondo.common.a i;

   b(u var1, u var2, co.uk.getmondo.common.accounts.d var3, co.uk.getmondo.common.e.a var4, s var5, co.uk.getmondo.api.b.a var6, co.uk.getmondo.common.a var7) {
      this.c = var1;
      this.d = var2;
      this.e = var3;
      this.f = var4;
      this.g = var5;
      this.h = var6;
      this.i = var7;
   }

   private static String a(ak var0) {
      String var1;
      if(var0 != null && var0.d() != null && var0.d().h() != null && var0.d().h().j() != null) {
         co.uk.getmondo.d.i var2 = co.uk.getmondo.d.i.a(var0.d().h().j());
         if(var2 == null) {
            var1 = "";
         } else {
            var1 = var2.c();
         }
      } else {
         var1 = "";
      }

      return var1;
   }

   // $FF: synthetic method
   static void a(b.a var0, ak var1) throws Exception {
      var0.a(a(var1));
      if(var1.c() != null) {
         var0.a();
      }

   }

   // $FF: synthetic method
   static void a(b var0, b.a var1, Throwable var2) throws Exception {
      var0.f.a(var2, var1);
   }

   public void a(Activity var1) {
      String var2 = var1.getString(2131362012);
      Intent.createChooser(co.uk.getmondo.common.k.j.a(this.e.b().e().g()), var2);
   }

   public void a(b.a var1) {
      super.a((co.uk.getmondo.common.ui.f)var1);
      this.i.a(Impression.j());
      var1.a(a(this.e.b()));
      this.a((io.reactivex.b.b)this.h.a().b(this.c).a(this.d).a(c.a(var1), d.a(this, var1)));
   }

   boolean a() {
      return this.g.g();
   }

   void c() {
      this.g.b(true);
   }

   interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      void a();

      void a(String var1);
   }
}
