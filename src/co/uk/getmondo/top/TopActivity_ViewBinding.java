package co.uk.getmondo.top;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;

public class TopActivity_ViewBinding implements Unbinder {
   private TopActivity a;
   private View b;

   public TopActivity_ViewBinding(final TopActivity var1, View var2) {
      this.a = var1;
      var1.titleTextView = (TextView)Utils.findRequiredViewAsType(var2, 2131821026, "field 'titleTextView'", TextView.class);
      var2 = Utils.findRequiredView(var2, 2131821118, "field 'getCardButton' and method 'onGetCardClick'");
      var1.getCardButton = (Button)Utils.castView(var2, 2131821118, "field 'getCardButton'", Button.class);
      this.b = var2;
      var2.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View var1x) {
            var1.onGetCardClick();
         }
      });
   }

   public void unbind() {
      TopActivity var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.titleTextView = null;
         var1.getCardButton = null;
         this.b.setOnClickListener((OnClickListener)null);
         this.b = null;
      }
   }
}
