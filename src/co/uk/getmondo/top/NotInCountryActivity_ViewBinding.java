package co.uk.getmondo.top;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;

public class NotInCountryActivity_ViewBinding implements Unbinder {
   private NotInCountryActivity a;
   private View b;
   private View c;

   public NotInCountryActivity_ViewBinding(final NotInCountryActivity var1, View var2) {
      this.a = var1;
      var1.image = (ImageView)Utils.findRequiredViewAsType(var2, 2131820750, "field 'image'", ImageView.class);
      var1.notified = (TextView)Utils.findRequiredViewAsType(var2, 2131820861, "field 'notified'", TextView.class);
      View var3 = Utils.findRequiredView(var2, 2131820860, "field 'notifyMe' and method 'onNotify'");
      var1.notifyMe = (Button)Utils.castView(var3, 2131820860, "field 'notifyMe'", Button.class);
      this.b = var3;
      var3.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View var1x) {
            var1.onNotify();
         }
      });
      var1.desc = (TextView)Utils.findRequiredViewAsType(var2, 2131820842, "field 'desc'", TextView.class);
      var2 = Utils.findRequiredView(var2, 2131820830, "method 'onShare'");
      this.c = var2;
      var2.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View var1x) {
            var1.onShare();
         }
      });
   }

   public void unbind() {
      NotInCountryActivity var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.image = null;
         var1.notified = null;
         var1.notifyMe = null;
         var1.desc = null;
         this.b.setOnClickListener((OnClickListener)null);
         this.b = null;
         this.c.setOnClickListener((OnClickListener)null);
         this.c = null;
      }
   }
}
