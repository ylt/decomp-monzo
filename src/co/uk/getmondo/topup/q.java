package co.uk.getmondo.topup;

import co.uk.getmondo.api.MonzoApi;
import co.uk.getmondo.api.model.topup.ApiTopUpLimits;
import co.uk.getmondo.api.model.tracking.Impression;
import java.util.List;
import org.json.JSONException;

public class q extends co.uk.getmondo.common.ui.b {
   private final co.uk.getmondo.a.a c;
   private final io.reactivex.u d;
   private final io.reactivex.u e;
   private final co.uk.getmondo.common.accounts.d f;
   private final co.uk.getmondo.common.e.a g;
   private final MonzoApi h;
   private final ao i;
   private a j;
   private co.uk.getmondo.d.ae k;
   private co.uk.getmondo.common.a l;
   private final co.uk.getmondo.topup.a.c m;
   private final co.uk.getmondo.topup.a.w n;

   q(io.reactivex.u var1, io.reactivex.u var2, co.uk.getmondo.common.accounts.d var3, co.uk.getmondo.common.e.a var4, co.uk.getmondo.common.a var5, MonzoApi var6, ao var7, co.uk.getmondo.a.a var8, co.uk.getmondo.topup.a.c var9, co.uk.getmondo.topup.a.w var10) {
      this.d = var1;
      this.e = var2;
      this.f = var3;
      this.g = var4;
      this.h = var6;
      this.i = var7;
      this.c = var8;
      this.l = var5;
      this.m = var9;
      this.n = var10;
   }

   // $FF: synthetic method
   static co.uk.getmondo.d.c a(Integer var0) throws Exception {
      return new co.uk.getmondo.d.c((long)(var0.intValue() * 100), "GBP");
   }

   // $FF: synthetic method
   static io.reactivex.l a(q var0, String var1, q.a var2, String var3) throws Exception {
      co.uk.getmondo.d.c var4 = new co.uk.getmondo.d.c(var0.j.g(), "GBP");
      return var0.m.a(var1, var3, var4).b(var0.e).a(var0.d).a(ab.a(var0, var2)).a((Object)co.uk.getmondo.common.b.a.a).e().a((io.reactivex.l)io.reactivex.h.a());
   }

   // $FF: synthetic method
   static io.reactivex.r a(q var0, String var1, a var2) throws Exception {
      return io.reactivex.n.combineLatest(var2.f().map(ad.a()), var0.c.a(var1), ae.a());
   }

   // $FF: synthetic method
   static io.reactivex.z a(co.uk.getmondo.d.a.r var0, List var1) throws Exception {
      io.reactivex.n var2 = io.reactivex.n.fromIterable(var1);
      var0.getClass();
      return var2.map(af.a(var0)).toList();
   }

   private void a(q.a var1, co.uk.getmondo.d.b var2) {
      if(var2 != null) {
         co.uk.getmondo.d.c var3 = var2.a().a((long)(this.j.e() * 100));
         var1.a(this.i.a(var3));
      }

   }

   // $FF: synthetic method
   static void a(q.a var0, Boolean var1) throws Exception {
      var0.g();
      if(!var1.booleanValue()) {
         var0.e();
      }

   }

   // $FF: synthetic method
   static void a(q var0, q.a var1, android.support.v4.g.j var2) throws Exception {
      var1.a((co.uk.getmondo.d.c)var2.a);
      var0.b(var1);
      var0.a(var1, (co.uk.getmondo.d.b)var2.b);
   }

   // $FF: synthetic method
   static void a(q var0, q.a var1, co.uk.getmondo.common.b.a var2) throws Exception {
      var0.l.a(Impression.a(Impression.TopUpSuccessType.ANDROID_PAY));
      var1.d();
   }

   // $FF: synthetic method
   static void a(q var0, q.a var1, Object var2) throws Exception {
      var1.f();
      var0.l.a(Impression.a(Impression.TopUpTapFrom.ANDROID_PAY));
   }

   // $FF: synthetic method
   static void a(q var0, q.a var1, Throwable var2) throws Exception {
      var1.g();
      if(var2 instanceof JSONException) {
         var1.e();
      } else if(!var0.n.a(var1, var2)) {
         var0.g.a(var2, var1);
      }

   }

   // $FF: synthetic method
   static boolean a(q.a var0, android.support.v4.g.j var1) throws Exception {
      ApiTopUpLimits var3 = (ApiTopUpLimits)var1.a;
      boolean var2;
      if(var3.b() >= 10.0F && var3.b() >= var3.a()) {
         var2 = true;
      } else {
         var0.b();
         var0.g();
         var2 = false;
      }

      return var2;
   }

   // $FF: synthetic method
   static a b(q var0, q.a var1, android.support.v4.g.j var2) throws Exception {
      ApiTopUpLimits var3 = (ApiTopUpLimits)var2.a;
      List var4 = (List)var2.b;
      co.uk.getmondo.d.ae var5;
      if(var4.size() > 0) {
         var5 = (co.uk.getmondo.d.ae)var4.get(0);
      } else {
         var5 = null;
      }

      var0.k = var5;
      var0.j = new a(var3.a(), var3.b(), 100);
      var1.a();
      var1.c(true);
      var1.g();
      return var0.j;
   }

   private void b(q.a var1) {
      boolean var3 = true;
      boolean var2;
      if(!this.j.d()) {
         var2 = true;
      } else {
         var2 = false;
      }

      var1.a(var2);
      if(!this.j.c()) {
         var2 = var3;
      } else {
         var2 = false;
      }

      var1.b(var2);
   }

   // $FF: synthetic method
   static void b(q var0, q.a var1, Object var2) throws Exception {
      co.uk.getmondo.d.c var3 = new co.uk.getmondo.d.c(var0.j.g(), "GBP");
      if(var0.k != null) {
         var1.a(var3, var0.k);
      } else {
         var1.b(var3);
      }

   }

   // $FF: synthetic method
   static void b(q var0, q.a var1, Throwable var2) throws Exception {
      var0.g.a(var2, var1);
   }

   // $FF: synthetic method
   static void c(q var0, q.a var1, Object var2) throws Exception {
      var0.l.a(Impression.k());
      var1.c();
   }

   // $FF: synthetic method
   static void c(q var0, q.a var1, Throwable var2) throws Exception {
      var1.g();
      if(!var0.g.a(var2, var1)) {
         var1.h();
      }

   }

   // $FF: synthetic method
   static void d() throws Exception {
      d.a.a.b("Balance refreshed", new Object[0]);
   }

   public void a(q.a var1) {
      super.a((co.uk.getmondo.common.ui.f)var1);
      var1.a(false);
      var1.b(false);
      String var2 = this.f.b().c().a();
      co.uk.getmondo.d.a.r var3 = new co.uk.getmondo.d.a.r();
      io.reactivex.v var4 = this.h.stripeCards(var2).d(r.a()).a(ac.a(var3));
      var1.f();
      this.a((io.reactivex.b.b)io.reactivex.v.a(this.h.topUpLimits(var2), var4, ag.a()).b(this.e).a(this.d).a(ah.a(var1)).c(ai.a(this, var1)).a(aj.a(this, var2)).observeOn(this.d).subscribe(ak.a(this, var1), al.a(this, var1)));
      this.a((io.reactivex.b.b)this.c.b(var2).b(this.e).a(this.d).a(am.b(), s.a(this, var1)));
      this.a((io.reactivex.b.b)var1.j().subscribe(t.a(this, var1)));
      this.a((io.reactivex.b.b)var1.i().subscribe(u.a(this, var1)));
      this.a((io.reactivex.b.b)var1.v().subscribe(v.a(this, var1)));
      this.a((io.reactivex.b.b)var1.k().flatMapMaybe(w.a(this, var2, var1)).subscribe(x.a(this, var1), y.a()));
      this.a((io.reactivex.b.b)var1.w().subscribe(z.a(var1), aa.a()));
   }

   boolean a() {
      if(this.j == null) {
         throw new IllegalStateException("Tried to increase load without loading limits");
      } else {
         return this.j.a();
      }
   }

   boolean c() {
      if(this.j == null) {
         throw new IllegalStateException("Tried to decrease load without loading limits");
      } else {
         return this.j.b();
      }
   }

   interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      void a();

      void a(co.uk.getmondo.d.c var1);

      void a(co.uk.getmondo.d.c var1, co.uk.getmondo.d.ae var2);

      void a(String var1);

      void a(boolean var1);

      void b();

      void b(co.uk.getmondo.d.c var1);

      void b(boolean var1);

      void c();

      void c(boolean var1);

      void d();

      void e();

      void f();

      void g();

      void h();

      io.reactivex.n i();

      io.reactivex.n j();

      io.reactivex.n k();

      io.reactivex.n v();

      io.reactivex.n w();
   }
}
