package co.uk.getmondo.topup;

// $FF: synthetic class
final class aj implements io.reactivex.c.h {
   private final q a;
   private final String b;

   private aj(q var1, String var2) {
      this.a = var1;
      this.b = var2;
   }

   public static io.reactivex.c.h a(q var0, String var1) {
      return new aj(var0, var1);
   }

   public Object a(Object var1) {
      return q.a(this.a, this.b, (a)var1);
   }
}
