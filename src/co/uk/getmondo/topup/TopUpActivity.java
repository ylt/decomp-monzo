package co.uk.getmondo.topup;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.as;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.common.activities.ConfirmationActivity;
import co.uk.getmondo.common.ui.AmountView;
import co.uk.getmondo.main.HomeActivity;
import co.uk.getmondo.topup.bank.TopUpInstructionsActivity;
import co.uk.getmondo.topup.card.TopUpWithNewCardActivity;
import co.uk.getmondo.topup.card.TopUpWithSavedCardActivity;
import com.google.android.gms.wallet.fragment.SupportWalletFragment;

public class TopUpActivity extends co.uk.getmondo.common.activities.b implements q.a {
   q a;
   co.uk.getmondo.common.q b;
   co.uk.getmondo.common.a.c c;
   @BindView(2131821124)
   ImageButton decreaseButton;
   b e;
   @BindView(2131821127)
   TextView expectedBalanceView;
   co.uk.getmondo.common.a f;
   private e g;
   @BindView(2131821126)
   ImageButton increaseButton;
   @BindView(2131821131)
   View overlayView;
   @BindView(2131821132)
   ProgressBar progressBar;
   @BindView(2131821125)
   AmountView toLoadAmountView;
   @BindView(2131821120)
   View topUpUnavailableView;
   @BindView(2131821122)
   View topUpView;
   @BindView(2131821128)
   Button topUpWithBankButton;
   @BindView(2131821129)
   Button topUpWithCardButton;

   public static Intent a(Context var0, boolean var1) {
      return (new Intent("android.intent.action.MAIN", Uri.EMPTY, var0, TopUpActivity.class)).putExtra("KEY_FROM_SHORTCUT", var1);
   }

   public static void a(Context var0) {
      var0.startActivity(b(var0));
   }

   // $FF: synthetic method
   static void a(TopUpActivity var0) throws Exception {
      var0.e.a((b.c)null);
   }

   // $FF: synthetic method
   static void a(TopUpActivity var0, io.reactivex.o var1) throws Exception {
      b var2 = var0.e;
      var1.getClass();
      var2.a(j.a(var1));
      var1.a(k.a(var0));
   }

   // $FF: synthetic method
   static void a(io.reactivex.o var0) {
      var0.a(co.uk.getmondo.common.b.a.a);
   }

   public static Intent b(Context var0) {
      return new Intent(var0, TopUpActivity.class);
   }

   // $FF: synthetic method
   static void b(TopUpActivity var0) throws Exception {
      var0.e.a((b.b)null);
   }

   // $FF: synthetic method
   static void b(TopUpActivity var0, io.reactivex.o var1) throws Exception {
      var0.e.a(l.a(var1));
      var1.a(m.a(var0));
   }

   // $FF: synthetic method
   static void c(TopUpActivity var0) throws Exception {
      var0.e.a((b.d)null);
   }

   // $FF: synthetic method
   static void c(TopUpActivity var0, io.reactivex.o var1) throws Exception {
      b var2 = var0.e;
      var1.getClass();
      var2.a(n.a(var1));
      var1.a(o.a(var0));
   }

   public void a() {
      this.topUpView.setVisibility(0);
      this.topUpUnavailableView.setVisibility(8);
   }

   public void a(co.uk.getmondo.d.c var1) {
      this.toLoadAmountView.setAmount(var1);
      this.e.a(var1);
   }

   public void a(co.uk.getmondo.d.c var1, co.uk.getmondo.d.ae var2) {
      TopUpWithSavedCardActivity.a(this, var1, var2);
   }

   public void a(String var1) {
      this.expectedBalanceView.setText(var1);
   }

   public void a(boolean var1) {
      this.decreaseButton.setEnabled(var1);
   }

   public void b() {
      this.topUpUnavailableView.setVisibility(0);
      this.topUpView.setVisibility(8);
   }

   public void b(co.uk.getmondo.d.c var1) {
      TopUpWithNewCardActivity.a(this, var1);
   }

   public void b(boolean var1) {
      this.increaseButton.setEnabled(var1);
   }

   public void c() {
      this.startActivity(TopUpInstructionsActivity.a((Context)this));
   }

   public void c(boolean var1) {
      this.topUpWithCardButton.setEnabled(var1);
   }

   public void d() {
      ConfirmationActivity.a(this, HomeActivity.b((Context)this));
   }

   public void e() {
      this.b(2131362011);
   }

   public void f() {
      this.overlayView.setVisibility(0);
      this.progressBar.setVisibility(0);
   }

   public void g() {
      this.overlayView.setVisibility(8);
      this.progressBar.setVisibility(8);
   }

   public void h() {
      co.uk.getmondo.common.d.a.a(this.getString(2131362819), this.getString(2131362818), true).show(this.getSupportFragmentManager(), "TAG_ERROR");
   }

   public io.reactivex.n i() {
      return com.b.a.c.c.a(this.topUpWithCardButton);
   }

   public io.reactivex.n j() {
      return com.b.a.c.c.a(this.topUpWithBankButton);
   }

   public io.reactivex.n k() {
      return io.reactivex.n.create(g.a(this));
   }

   protected void o() {
      Intent var1 = android.support.v4.app.y.a(this);
      if(!android.support.v4.app.y.a(this, var1) && !this.isTaskRoot()) {
         this.finish();
      } else {
         as.a(this).b(var1).a();
      }

   }

   protected void onActivityResult(int var1, int var2, Intent var3) {
      if(!this.e.a(var1, var2, var3)) {
         super.onActivityResult(var1, var2, var3);
      }

   }

   @OnClick({2131821121})
   void onContactSupportClicked() {
      this.b.a();
   }

   protected void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.setContentView(2131034220);
      ButterKnife.bind((Activity)this);
      this.l().a(this);
      this.a.a((q.a)this);
      this.g = new e();
      this.g.a(this.increaseButton, this.decreaseButton);
      this.g.a(new e.a() {
         public boolean a() {
            return TopUpActivity.this.a.a();
         }

         public boolean b() {
            return TopUpActivity.this.a.c();
         }
      });
      if(this.getIntent().hasExtra("KEY_FROM_SHORTCUT")) {
         this.c.a(co.uk.getmondo.common.a.b.b);
      }

      this.e.a(this, new b.a() {
         public void a() {
            TopUpActivity.this.f.a(Impression.a(false));
         }

         public void a(SupportWalletFragment var1) {
            TopUpActivity.this.f.a(Impression.a(true));
            TopUpActivity.this.getSupportFragmentManager().a().b(2131821130, var1).c();
         }
      });
      this.toLoadAmountView.setAmount(new co.uk.getmondo.d.c(1000L, co.uk.getmondo.common.i.c.a));
   }

   @OnClick({2131821124})
   void onDecreaseClicked() {
      this.a.c();
   }

   protected void onDestroy() {
      super.onDestroy();
      this.g.a();
      this.a.b();
   }

   @OnClick({2131821126})
   void onIncreaseClicked() {
      this.a.a();
   }

   public io.reactivex.n v() {
      return io.reactivex.n.create(h.a(this));
   }

   public io.reactivex.n w() {
      return io.reactivex.n.create(i.a(this));
   }
}
