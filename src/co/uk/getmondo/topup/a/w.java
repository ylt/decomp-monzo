package co.uk.getmondo.topup.a;

import co.uk.getmondo.api.ApiException;
import co.uk.getmondo.topup.three_d_secure.ThreeDsResolver;
import com.stripe.android.exception.CardException;

public class w {
   public boolean a(co.uk.getmondo.common.e.e var1, Throwable var2) {
      boolean var3;
      if(var2 instanceof CardException) {
         var1.b(2131362797);
         var3 = true;
      } else {
         if(var2 instanceof ApiException) {
            co.uk.getmondo.api.model.b var4 = ((ApiException)var2).e();
            if(var4 != null) {
               v var5 = (v)co.uk.getmondo.common.e.d.a(v.values(), var4.a());
               if(var5 != null) {
                  var1.b(var5.b());
                  var3 = true;
                  return var3;
               }
            }
         }

         if(var2 instanceof ThreeDsResolver.ThreeDsUnsuccessfulException) {
            switch(null.a[((ThreeDsResolver.ThreeDsUnsuccessfulException)var2).a.ordinal()]) {
            case 1:
            case 2:
               var1.b(2131362796);
            default:
               var3 = true;
            }
         } else {
            var3 = false;
         }
      }

      return var3;
   }
}
