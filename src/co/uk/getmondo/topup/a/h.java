package co.uk.getmondo.topup.a;

import co.uk.getmondo.api.model.topup.ThreeDSecureRequest;

// $FF: synthetic class
final class h implements io.reactivex.c.g {
   private final c a;
   private final ThreeDSecureRequest b;

   private h(c var1, ThreeDSecureRequest var2) {
      this.a = var1;
      this.b = var2;
   }

   public static io.reactivex.c.g a(c var0, ThreeDSecureRequest var1) {
      return new h(var0, var1);
   }

   public void a(Object var1) {
      c.a(this.a, this.b, (Throwable)var1);
   }
}
