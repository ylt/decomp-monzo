package co.uk.getmondo.topup.a;

import co.uk.getmondo.api.model.topup.TopUpRequest;

// $FF: synthetic class
final class f implements io.reactivex.c.g {
   private final c a;
   private final TopUpRequest b;

   private f(c var1, TopUpRequest var2) {
      this.a = var1;
      this.b = var2;
   }

   public static io.reactivex.c.g a(c var0, TopUpRequest var1) {
      return new f(var0, var1);
   }

   public void a(Object var1) {
      c.a(this.a, this.b, (Throwable)var1);
   }
}
