package co.uk.getmondo.topup.a;

import java.util.concurrent.Callable;

// $FF: synthetic class
final class d implements Callable {
   private final c a;
   private final String b;
   private final String c;

   private d(c var1, String var2, String var3) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
   }

   public static Callable a(c var0, String var1, String var2) {
      return new d(var0, var1, var2);
   }

   public Object call() {
      return c.a(this.a, this.b, this.c);
   }
}
