package co.uk.getmondo.topup.a;

import co.uk.getmondo.api.model.topup.ApiStripeCard;
import co.uk.getmondo.topup.three_d_secure.ThreeDsResolver;

// $FF: synthetic class
final class o implements io.reactivex.c.h {
   private final c a;
   private final String b;
   private final co.uk.getmondo.d.c c;
   private final boolean d;
   private final ThreeDsResolver e;

   private o(c var1, String var2, co.uk.getmondo.d.c var3, boolean var4, ThreeDsResolver var5) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
      this.d = var4;
      this.e = var5;
   }

   public static io.reactivex.c.h a(c var0, String var1, co.uk.getmondo.d.c var2, boolean var3, ThreeDsResolver var4) {
      return new o(var0, var1, var2, var3, var4);
   }

   public Object a(Object var1) {
      return c.a(this.a, this.b, this.c, this.d, this.e, (ApiStripeCard)var1);
   }
}
