package co.uk.getmondo.topup.a;

import co.uk.getmondo.api.model.topup.ApiThreeDsResponse;
import co.uk.getmondo.api.model.topup.ThreeDSecureRequest;

// $FF: synthetic class
final class g implements io.reactivex.c.h {
   private final ThreeDSecureRequest a;

   private g(ThreeDSecureRequest var1) {
      this.a = var1;
   }

   public static io.reactivex.c.h a(ThreeDSecureRequest var0) {
      return new g(var0);
   }

   public Object a(Object var1) {
      return c.a(this.a, (ApiThreeDsResponse)var1);
   }
}
