package co.uk.getmondo.topup.a;

import com.stripe.android.model.Card;
import java.util.concurrent.Callable;

// $FF: synthetic class
final class m implements Callable {
   private final c a;
   private final Card b;

   private m(c var1, Card var2) {
      this.a = var1;
      this.b = var2;
   }

   public static Callable a(c var0, Card var1) {
      return new m(var0, var1);
   }

   public Object call() {
      return c.a(this.a, this.b);
   }
}
