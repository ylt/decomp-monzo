package co.uk.getmondo.topup.a;

import co.uk.getmondo.api.model.topup.ApiStripeCard;

// $FF: synthetic class
final class k implements io.reactivex.c.h {
   private final c a;
   private final String b;
   private final co.uk.getmondo.d.c c;

   private k(c var1, String var2, co.uk.getmondo.d.c var3) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
   }

   public static io.reactivex.c.h a(c var0, String var1, co.uk.getmondo.d.c var2) {
      return new k(var0, var1, var2);
   }

   public Object a(Object var1) {
      return c.a(this.a, this.b, this.c, (ApiStripeCard)var1);
   }
}
