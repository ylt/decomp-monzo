package co.uk.getmondo.topup.a;

import co.uk.getmondo.api.model.topup.ApiThreeDsResponse;

// $FF: synthetic class
final class r implements io.reactivex.c.h {
   private final c a;
   private final String b;
   private final String c;
   private final co.uk.getmondo.d.c d;
   private final boolean e;

   private r(c var1, String var2, String var3, co.uk.getmondo.d.c var4, boolean var5) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
      this.d = var4;
      this.e = var5;
   }

   public static io.reactivex.c.h a(c var0, String var1, String var2, co.uk.getmondo.d.c var3, boolean var4) {
      return new r(var0, var1, var2, var3, var4);
   }

   public Object a(Object var1) {
      return c.a(this.a, this.b, this.c, this.d, this.e, (ApiThreeDsResponse)var1);
   }
}
