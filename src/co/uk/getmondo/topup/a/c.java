package co.uk.getmondo.topup.a;

import co.uk.getmondo.api.ApiException;
import co.uk.getmondo.api.MonzoApi;
import co.uk.getmondo.api.model.topup.AddStripeCardRequest;
import co.uk.getmondo.api.model.topup.ApiStripeCard;
import co.uk.getmondo.api.model.topup.ApiThreeDsResponse;
import co.uk.getmondo.api.model.topup.ThreeDSecureRequest;
import co.uk.getmondo.api.model.topup.TopUpRequest;
import co.uk.getmondo.topup.three_d_secure.ThreeDsResolver;
import com.stripe.android.Stripe;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.android.net.TokenParser;
import io.reactivex.z;
import org.json.JSONException;

public class c {
   private final MonzoApi a;
   private final io.reactivex.u b;
   private final a c;
   private final Stripe d;
   private final c.a e;

   public c(MonzoApi var1, io.reactivex.u var2, a var3, Stripe var4, c.a var5) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
      this.d = var4;
      this.e = var5;
   }

   // $FF: synthetic method
   static android.support.v4.g.j a(ThreeDSecureRequest var0, ApiThreeDsResponse var1) throws Exception {
      return new android.support.v4.g.j(var1, var0);
   }

   // $FF: synthetic method
   static AddStripeCardRequest a(c var0, String var1, String var2) throws Exception {
      return new AddStripeCardRequest(var2, var0.e.a(var1).getId(), "", false);
   }

   // $FF: synthetic method
   static Token a(c var0, Card var1) throws Exception {
      return var0.d.createTokenSynchronous(var1);
   }

   // $FF: synthetic method
   static io.reactivex.d a(c var0, String var1, co.uk.getmondo.d.c var2, AddStripeCardRequest var3) throws Exception {
      return var0.a.addStripeCard(var3.a(), var3.b(), var3.c(), var3.d(), var0.c.a(var3)).d(j.a(var0, var3)).c(k.a(var0, var1, var2));
   }

   // $FF: synthetic method
   static io.reactivex.d a(c var0, String var1, co.uk.getmondo.d.c var2, ApiStripeCard var3) throws Exception {
      return var0.a((String)var1, (String)var3.a(), (co.uk.getmondo.d.c)var2, false, (ThreeDsResolver)null);
   }

   // $FF: synthetic method
   static io.reactivex.d a(c var0, String var1, co.uk.getmondo.d.c var2, boolean var3, ThreeDsResolver var4, ApiStripeCard var5) throws Exception {
      return var0.a(var1, var5.a(), var2, var3, var4);
   }

   // $FF: synthetic method
   static io.reactivex.d a(c var0, String var1, String var2, co.uk.getmondo.d.c var3, boolean var4, ApiThreeDsResponse var5) throws Exception {
      if(var5.b()) {
         var2 = var5.d();
      }

      TopUpRequest var6 = new TopUpRequest(var1, var2, var3.l().b(), var3.k(), var4, var5.c());
      return var0.a.topUpThreeDSecure(var6.a(), var6.b(), var6.c(), var6.d(), var6.e(), var6.f(), var0.c.a(var6)).a(f.a(var0, var6)).b(var0.b);
   }

   private io.reactivex.v a(ApiThreeDsResponse var1, ThreeDSecureRequest var2, ThreeDsResolver var3, boolean var4) {
      io.reactivex.v var5;
      if(var1.a()) {
         var5 = var3.a(var1.e(), "https://threeds.monzo.com", var4).c(s.a(this, var2)).a(e.a(var1));
      } else {
         var5 = io.reactivex.v.a((Object)var1);
      }

      return var5;
   }

   // $FF: synthetic method
   static z a(ApiThreeDsResponse var0, ThreeDsResolver.a var1) throws Exception {
      io.reactivex.v var2;
      if(var1 != ThreeDsResolver.a.a) {
         var2 = io.reactivex.v.a((Throwable)(new ThreeDsResolver.ThreeDsUnsuccessfulException(var1)));
      } else {
         var2 = io.reactivex.v.a((Object)var0);
      }

      return var2;
   }

   // $FF: synthetic method
   static z a(c var0, ThreeDsResolver var1, boolean var2, android.support.v4.g.j var3) throws Exception {
      return var0.a((ApiThreeDsResponse)var3.a, (ThreeDSecureRequest)var3.b, var1, var2);
   }

   // $FF: synthetic method
   static z a(c var0, Card var1, String var2, boolean var3, Token var4) throws Exception {
      String var5 = var1.getNumber().substring(0, 6);
      AddStripeCardRequest var6 = new AddStripeCardRequest(var2, var4.getId(), var5, var3);
      return var0.a.addStripeCard(var6.a(), var6.b(), var6.c(), var6.d(), var0.c.a(var6)).d(i.a(var0, var6)).b(var0.b);
   }

   // $FF: synthetic method
   static z a(c var0, String var1, co.uk.getmondo.d.c var2, boolean var3, String var4) throws Exception {
      ThreeDSecureRequest var5 = new ThreeDSecureRequest(var1, var2.l().b(), var2.k(), var3, var4, "https://threeds.monzo.com");
      return var0.a.threeDSecure(var5.a(), var5.b(), var5.c(), var5.d(), var5.e(), var5.f(), var0.c.a(var5)).d(g.a(var5)).d(h.a(var0, var5));
   }

   // $FF: synthetic method
   static void a(c var0, AddStripeCardRequest var1, Throwable var2) throws Exception {
      var0.a((Throwable)var2, (Object)var1);
   }

   // $FF: synthetic method
   static void a(c var0, ThreeDSecureRequest var1, ThreeDsResolver.a var2) throws Exception {
      if(var2 == ThreeDsResolver.a.b) {
         var0.c.b(var1);
      }

   }

   // $FF: synthetic method
   static void a(c var0, ThreeDSecureRequest var1, Throwable var2) throws Exception {
      var0.a((Throwable)var2, (Object)var1);
   }

   // $FF: synthetic method
   static void a(c var0, TopUpRequest var1, Throwable var2) throws Exception {
      var0.a((Throwable)var2, (Object)var1);
   }

   private boolean a(co.uk.getmondo.api.model.b var1) {
      String var3;
      if(var1 != null) {
         var3 = var1.a();
      } else {
         var3 = "";
      }

      boolean var2;
      if(var3 != null && var3.startsWith("bad_request.card_error")) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   private boolean a(Throwable var1, Object var2) {
      boolean var3;
      if(var1 instanceof ApiException && this.a(((ApiException)var1).e())) {
         this.c.b(var2);
         var3 = true;
      } else {
         var3 = false;
      }

      return var3;
   }

   // $FF: synthetic method
   static void b(c var0, AddStripeCardRequest var1, Throwable var2) throws Exception {
      var0.a((Throwable)var2, (Object)var1);
   }

   public io.reactivex.b a(String var1, Card var2, boolean var3, co.uk.getmondo.d.c var4, boolean var5, ThreeDsResolver var6) {
      return io.reactivex.v.c(m.a(this, var2)).a(n.a(this, var2, var1, var3)).c(o.a(this, var1, var4, var5, var6));
   }

   public io.reactivex.b a(String var1, String var2, co.uk.getmondo.d.c var3) {
      return io.reactivex.v.c(d.a(this, var2, var1)).c(l.a(this, var1, var3));
   }

   public io.reactivex.b a(String var1, String var2, co.uk.getmondo.d.c var3, boolean var4, ThreeDsResolver var5) {
      return io.reactivex.v.a(p.a(this, var1, var3, var4, var2)).a(q.a(this, var5, var4)).c(r.a(this, var1, var2, var3, var4));
   }

   public static class a {
      public Token a(String var1) throws JSONException {
         return TokenParser.parseToken(var1);
      }
   }
}
