package co.uk.getmondo.topup.a;

import co.uk.getmondo.api.model.topup.AddStripeCardRequest;

// $FF: synthetic class
final class i implements io.reactivex.c.g {
   private final c a;
   private final AddStripeCardRequest b;

   private i(c var1, AddStripeCardRequest var2) {
      this.a = var1;
      this.b = var2;
   }

   public static io.reactivex.c.g a(c var0, AddStripeCardRequest var1) {
      return new i(var0, var1);
   }

   public void a(Object var1) {
      c.a(this.a, this.b, (Throwable)var1);
   }
}
