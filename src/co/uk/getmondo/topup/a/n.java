package co.uk.getmondo.topup.a;

import com.stripe.android.model.Card;
import com.stripe.android.model.Token;

// $FF: synthetic class
final class n implements io.reactivex.c.h {
   private final c a;
   private final Card b;
   private final String c;
   private final boolean d;

   private n(c var1, Card var2, String var3, boolean var4) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
      this.d = var4;
   }

   public static io.reactivex.c.h a(c var0, Card var1, String var2, boolean var3) {
      return new n(var0, var1, var2, var3);
   }

   public Object a(Object var1) {
      return c.a(this.a, this.b, this.c, this.d, (Token)var1);
   }
}
