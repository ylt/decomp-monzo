package co.uk.getmondo.topup.a;

import java.util.concurrent.Callable;

// $FF: synthetic class
final class p implements Callable {
   private final c a;
   private final String b;
   private final co.uk.getmondo.d.c c;
   private final boolean d;
   private final String e;

   private p(c var1, String var2, co.uk.getmondo.d.c var3, boolean var4, String var5) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
      this.d = var4;
      this.e = var5;
   }

   public static Callable a(c var0, String var1, co.uk.getmondo.d.c var2, boolean var3, String var4) {
      return new p(var0, var1, var2, var3, var4);
   }

   public Object call() {
      return c.a(this.a, this.b, this.c, this.d, this.e);
   }
}
