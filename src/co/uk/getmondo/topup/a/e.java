package co.uk.getmondo.topup.a;

import co.uk.getmondo.api.model.topup.ApiThreeDsResponse;
import co.uk.getmondo.topup.three_d_secure.ThreeDsResolver;

// $FF: synthetic class
final class e implements io.reactivex.c.h {
   private final ApiThreeDsResponse a;

   private e(ApiThreeDsResponse var1) {
      this.a = var1;
   }

   public static io.reactivex.c.h a(ApiThreeDsResponse var0) {
      return new e(var0);
   }

   public Object a(Object var1) {
      return c.a(this.a, (ThreeDsResolver.a)var1);
   }
}
