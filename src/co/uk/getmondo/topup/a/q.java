package co.uk.getmondo.topup.a;

import co.uk.getmondo.topup.three_d_secure.ThreeDsResolver;

// $FF: synthetic class
final class q implements io.reactivex.c.h {
   private final c a;
   private final ThreeDsResolver b;
   private final boolean c;

   private q(c var1, ThreeDsResolver var2, boolean var3) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
   }

   public static io.reactivex.c.h a(c var0, ThreeDsResolver var1, boolean var2) {
      return new q(var0, var1, var2);
   }

   public Object a(Object var1) {
      return c.a(this.a, this.b, this.c, (android.support.v4.g.j)var1);
   }
}
