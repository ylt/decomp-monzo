package co.uk.getmondo.topup;

// $FF: synthetic class
final class w implements io.reactivex.c.h {
   private final q a;
   private final String b;
   private final q.a c;

   private w(q var1, String var2, q.a var3) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
   }

   public static io.reactivex.c.h a(q var0, String var1, q.a var2) {
      return new w(var0, var1, var2);
   }

   public Object a(Object var1) {
      return q.a(this.a, this.b, this.c, (String)var1);
   }
}
