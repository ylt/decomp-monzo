package co.uk.getmondo.topup;

import android.content.Intent;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.BooleanResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.wallet.Cart;
import com.google.android.gms.wallet.FullWallet;
import com.google.android.gms.wallet.FullWalletRequest;
import com.google.android.gms.wallet.IsReadyToPayRequest;
import com.google.android.gms.wallet.LineItem;
import com.google.android.gms.wallet.MaskedWallet;
import com.google.android.gms.wallet.MaskedWalletRequest;
import com.google.android.gms.wallet.PaymentMethodTokenizationParameters;
import com.google.android.gms.wallet.Wallet;
import com.google.android.gms.wallet.Wallet.WalletOptions;
import com.google.android.gms.wallet.Wallet.WalletOptions.Builder;
import com.google.android.gms.wallet.fragment.SupportWalletFragment;
import com.google.android.gms.wallet.fragment.WalletFragmentInitParams;
import com.google.android.gms.wallet.fragment.WalletFragmentOptions;
import com.google.android.gms.wallet.fragment.WalletFragmentStyle;

class b implements OnConnectionFailedListener {
   private final co.uk.getmondo.common.i.b a = new co.uk.getmondo.common.i.b(true, false, true);
   private co.uk.getmondo.d.c b;
   private GoogleApiClient c;
   private b.d d;
   private b.b e;
   private b.c f;

   b() {
      this.b = new co.uk.getmondo.d.c(1000L, co.uk.getmondo.common.i.c.a);
   }

   private int a(Intent var1) {
      int var2 = -1;
      if(var1 != null) {
         var2 = var1.getIntExtra("com.google.android.gms.wallet.EXTRA_ERROR_CODE", -1);
      }

      return var2;
   }

   private FullWalletRequest a(MaskedWallet var1) {
      String var2 = this.a.a(this.b);
      LineItem var3 = LineItem.newBuilder().setCurrencyCode(this.b.l().b()).setQuantity("1").setTotalPrice(var2).build();
      Cart var4 = Cart.newBuilder().setCurrencyCode(this.b.l().b()).setTotalPrice(var2).addLineItem(var3).build();
      return FullWalletRequest.newBuilder().setCart(var4).setGoogleTransactionId(var1.getGoogleTransactionId()).build();
   }

   private SupportWalletFragment a() {
      WalletFragmentStyle var1 = (new WalletFragmentStyle()).setBuyButtonText(6).setBuyButtonAppearance(4).setBuyButtonWidth(-1);
      SupportWalletFragment var2 = SupportWalletFragment.newInstance(WalletFragmentOptions.newBuilder().setEnvironment(1).setFragmentStyle(var1).setTheme(1).setMode(1).build());
      var2.initialize(this.b());
      return var2;
   }

   // $FF: synthetic method
   static void a(b var0, b.a var1, BooleanResult var2) {
      if(var2.getStatus().isSuccess()) {
         if(var2.getValue()) {
            var1.a(var0.a());
         } else {
            var1.a();
         }
      } else {
         String var4 = var2.getStatus().getStatusMessage();
         int var3 = var2.getStatus().getStatusCode();
         d.a.a.a((Throwable)(new RuntimeException("AndroidPayDelegate, failed to determine if user ready to pay, message: " + var4 + " errorCode: " + var3)));
      }

   }

   private WalletFragmentInitParams b() {
      PaymentMethodTokenizationParameters var1 = PaymentMethodTokenizationParameters.newBuilder().setPaymentMethodTokenizationType(1).addParameter("gateway", "stripe").addParameter("stripe:publishableKey", "pk_live_LW3b2bi1iXHicd88NnwlCkaA").addParameter("stripe:version", "4.1.3").build();
      MaskedWalletRequest var2 = MaskedWalletRequest.newBuilder().setMerchantName("Monzo").setPhoneNumberRequired(false).setShippingAddressRequired(false).setCurrencyCode(this.b.l().b()).setEstimatedTotalPrice(this.a.a(this.b)).setPaymentMethodTokenizationParameters(var1).build();
      return WalletFragmentInitParams.newBuilder().setMaskedWalletRequest(var2).setMaskedWalletRequestCode(1001).build();
   }

   void a(android.support.v4.app.j var1, b.a var2) {
      WalletOptions var3 = (new Builder()).setEnvironment(1).setTheme(0).build();
      this.c = (new com.google.android.gms.common.api.GoogleApiClient.Builder(var1)).addApi(Wallet.API, var3).enableAutoManage(var1, 1000, this).build();
      IsReadyToPayRequest var4 = IsReadyToPayRequest.newBuilder().addAllowedCardNetwork(4).addAllowedCardNetwork(5).build();
      Wallet.Payments.isReadyToPay(this.c, var4).setResultCallback(c.a(this, var2));
   }

   void a(co.uk.getmondo.d.c var1) {
      this.b = var1;
   }

   void a(b.b var1) {
      this.e = var1;
   }

   void a(b.c var1) {
      this.f = var1;
   }

   void a(b.d var1) {
      this.d = var1;
   }

   boolean a(int var1, int var2, Intent var3) {
      boolean var4 = false;
      switch(var1) {
      case 1:
         this.f.a(false);
         d.a.a.a((Throwable)(new RuntimeException("AndroidPayDelegate, wallet constants error:" + this.a(var3))));
         var4 = true;
         break;
      case 1001:
         switch(var2) {
         case -1:
            this.e.a();
            FullWalletRequest var6 = this.a((MaskedWallet)var3.getParcelableExtra("com.google.android.gms.wallet.EXTRA_MASKED_WALLET"));
            Wallet.Payments.loadFullWallet(this.c, var6, 1002);
            break;
         case 0:
            this.f.a(true);
            break;
         default:
            this.f.a(false);
            d.a.a.a((Throwable)(new RuntimeException("AndroidPayDelegate, masked wallet request failed, error code:" + this.a(var3))));
         }

         var4 = true;
         break;
      case 1002:
         switch(var2) {
         case -1:
            FullWallet var5 = (FullWallet)var3.getParcelableExtra("com.google.android.gms.wallet.EXTRA_FULL_WALLET");
            this.d.a(var5.getPaymentMethodToken().getToken());
            break;
         case 0:
            this.f.a(true);
            break;
         default:
            this.f.a(false);
            d.a.a.a((Throwable)(new RuntimeException("AndroidPayDelegate, full wallet request failed, error code:" + this.a(var3))));
         }

         var4 = true;
      }

      return var4;
   }

   public void onConnectionFailed(ConnectionResult var1) {
      d.a.a.a((Throwable)(new RuntimeException("AndroidPayDelegate, failed to connect. Message: " + var1.getErrorMessage() + ", errorCode: " + var1.getErrorCode())));
   }

   interface a {
      void a();

      void a(SupportWalletFragment var1);
   }

   @FunctionalInterface
   interface b {
      void a();
   }

   @FunctionalInterface
   interface c {
      void a(boolean var1);
   }

   @FunctionalInterface
   interface d {
      void a(String var1);
   }
}
