package co.uk.getmondo.topup;

import android.content.Context;

public class ao {
   private final Context a;

   public ao(Context var1) {
      this.a = var1;
   }

   public String a() {
      return this.a.getString(2131362706);
   }

   public String a(co.uk.getmondo.d.c var1) {
      return this.a.getString(2131362804, new Object[]{var1.toString()});
   }

   public String a(String var1) {
      return this.a.getString(2131362705, new Object[]{var1});
   }

   public String a(String var1, String var2, String var3) {
      return this.a.getString(2131362704, new Object[]{var1, var2, var3});
   }
}
