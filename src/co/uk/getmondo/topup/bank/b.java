package co.uk.getmondo.topup.bank;

import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.common.accounts.d;
import co.uk.getmondo.common.k.p;
import co.uk.getmondo.common.ui.f;
import co.uk.getmondo.d.ac;
import co.uk.getmondo.d.ad;
import co.uk.getmondo.d.ak;
import co.uk.getmondo.topup.ao;
import io.reactivex.c.g;
import kotlin.Metadata;
import kotlin.n;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0011B'\b\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\u0006\u0010\t\u001a\u00020\n¢\u0006\u0002\u0010\u000bJ\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0002H\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\f\u001a\u0004\u0018\u00010\rX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0012"},
   d2 = {"Lco/uk/getmondo/topup/bank/TopUpInstructionsPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/topup/bank/TopUpInstructionsPresenter$TopUpInstructionsView;", "accountService", "Lco/uk/getmondo/common/accounts/AccountService;", "analyticsService", "Lco/uk/getmondo/common/AnalyticsService;", "cardManager", "Lco/uk/getmondo/card/CardManager;", "topUpStringProvider", "Lco/uk/getmondo/topup/TopUpStringProvider;", "(Lco/uk/getmondo/common/accounts/AccountService;Lco/uk/getmondo/common/AnalyticsService;Lco/uk/getmondo/card/CardManager;Lco/uk/getmondo/topup/TopUpStringProvider;)V", "lastToken", "", "register", "", "topUpInstructionsView", "TopUpInstructionsView", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class b extends co.uk.getmondo.common.ui.b {
   private String c;
   private final d d;
   private final co.uk.getmondo.common.a e;
   private final co.uk.getmondo.card.c f;
   private final ao g;

   public b(d var1, co.uk.getmondo.common.a var2, co.uk.getmondo.card.c var3, ao var4) {
      l.b(var1, "accountService");
      l.b(var2, "analyticsService");
      l.b(var3, "cardManager");
      l.b(var4, "topUpStringProvider");
      super();
      this.d = var1;
      this.e = var2;
      this.f = var3;
      this.g = var4;
   }

   // $FF: synthetic method
   public static final b.a a(b var0) {
      return (b.a)var0.a;
   }

   public void a(b.a var1) {
      l.b(var1, "topUpInstructionsView");
      super.a((f)var1);
      ak var2 = this.d.b();
      if(var2 == null) {
         l.a();
      }

      final co.uk.getmondo.d.a var5 = var2.c();
      if(var5 == null) {
         l.a();
      }

      final ac var7 = var2.d();
      if(var7 == null) {
         l.a();
      }

      io.reactivex.b.a var3;
      if(var5 instanceof ad) {
         this.e.a(Impression.Companion.n());
         ((b.a)this.a).a(var7.c(), ((ad)var5).j(), ((ad)var5).i());
      } else {
         var3 = this.b;
         io.reactivex.b.b var4 = this.f.a().subscribe((g)(new g() {
            public final void a(co.uk.getmondo.d.g var1) {
               String var3 = p.e(var1.e());
               b.a var2 = b.a(b.this);
               l.a(var3, "formattedToken");
               var2.a(var3);
               b.this.c = var3;
            }
         }), (g)null.a);
         l.a(var4, "cardManager.card()\n     …     }, { Timber.e(it) })");
         this.b = co.uk.getmondo.common.j.f.a(var3, var4);
      }

      var3 = this.b;
      io.reactivex.b.b var6 = ((b.a)this.a).a().subscribe((g)(new g() {
         public final void a(n var1) {
            b.this.e.a(Impression.Companion.p());
            String var3;
            if(var5 instanceof ad) {
               b.a var2 = b.a(b.this);
               String var4 = b.this.g.a();
               l.a(var4, "topUpStringProvider.instructionsTitle");
               var3 = b.this.g.a(var7.c(), ((ad)var5).j(), ((ad)var5).i());
               l.a(var3, "topUpStringProvider.getI…Number, account.sortCode)");
               var2.a(var4, var3);
            } else {
               b.a var5x = b.a(b.this);
               String var6 = b.this.g.a();
               l.a(var6, "topUpStringProvider.instructionsTitle");
               var3 = b.this.g.a(b.this.c);
               l.a(var3, "topUpStringProvider.getI…               lastToken)");
               var5x.a(var6, var3);
            }

         }
      }));
      l.a(var6, "view.shareClicks\n       …      }\n                }");
      this.b = co.uk.getmondo.common.j.f.a(var3, var6);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\b\bf\u0018\u00002\u00020\u00012\u00020\u0002J\u0018\u0010\b\u001a\u00020\u00052\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\nH&J\u0010\u0010\f\u001a\u00020\u00052\u0006\u0010\r\u001a\u00020\nH&J \u0010\u000e\u001a\u00020\u00052\u0006\u0010\u000f\u001a\u00020\n2\u0006\u0010\u0010\u001a\u00020\n2\u0006\u0010\u0011\u001a\u00020\nH&R\u0018\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0006\u0010\u0007¨\u0006\u0012"},
      d2 = {"Lco/uk/getmondo/topup/bank/TopUpInstructionsPresenter$TopUpInstructionsView;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "shareClicks", "Lio/reactivex/Observable;", "", "getShareClicks", "()Lio/reactivex/Observable;", "share", "subject", "", "text", "showMonzoDetails", "cardNumber", "showRetailCardDetails", "beneficiary", "accountNumber", "sortCode", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, f {
      io.reactivex.n a();

      void a(String var1);

      void a(String var1, String var2);

      void a(String var1, String var2, String var3);
   }
}
