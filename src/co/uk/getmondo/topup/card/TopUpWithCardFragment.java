package co.uk.getmondo.topup.card;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import butterknife.Unbinder;
import co.uk.getmondo.topup.three_d_secure.ThreeDsResolver;

public class TopUpWithCardFragment extends co.uk.getmondo.common.f.a implements b.a {
   b a;
   @BindView(2131821397)
   EditText billingPostcode;
   @BindView(2131821396)
   TextInputLayout billingPostcodeWrapper;
   co.uk.getmondo.common.a c;
   @BindView(2131821389)
   EditText cardHolderName;
   @BindView(2131821388)
   TextInputLayout cardHolderNameWrapper;
   @BindView(2131821391)
   EditText cardNumber;
   @BindView(2131821390)
   TextInputLayout cardNumberWrapper;
   @BindView(2131821395)
   EditText cvv;
   @BindView(2131821394)
   TextInputLayout cvvWrapper;
   private co.uk.getmondo.d.c d;
   @BindView(2131820842)
   TextView descriptionView;
   private ProgressDialog e;
   @BindView(2131821393)
   EditText expiryDate;
   @BindView(2131821392)
   TextInputLayout expiryDateWrapper;
   private Boolean f;
   private ThreeDsResolver g;
   private Unbinder h;
   private TopUpWithCardFragment.a i;
   @BindView(2131821147)
   Button topUpButton;

   public static Fragment a(co.uk.getmondo.d.c var0, boolean var1) {
      TopUpWithCardFragment var3 = new TopUpWithCardFragment();
      Bundle var2 = new Bundle();
      var2.putParcelable("ARG_AMOUNT", var0);
      var2.putBoolean("ARG_INITIAL_TOPUP", var1);
      var3.setArguments(var2);
      return var3;
   }

   private void h() {
      final co.uk.getmondo.create_account.c var1 = new co.uk.getmondo.create_account.c();
      this.expiryDate.addTextChangedListener(new TextWatcher() {
         public void afterTextChanged(Editable var1x) {
            if(TopUpWithCardFragment.this.expiryDate.getText().length() == 7) {
               TopUpWithCardFragment.this.cvv.requestFocus();
            }

         }

         public void beforeTextChanged(CharSequence var1x, int var2, int var3, int var4) {
         }

         public void onTextChanged(CharSequence var1x, int var2, int var3, int var4) {
            TopUpWithCardFragment.this.expiryDateWrapper.setError((CharSequence)null);
            var1.a(var1x, var3, var4, this, TopUpWithCardFragment.this.expiryDateWrapper.getEditText());
         }
      });
   }

   private void i() {
      this.cardNumber.addTextChangedListener(new TextWatcher() {
         public void afterTextChanged(Editable var1) {
            if(TopUpWithCardFragment.this.cardNumber.getText().length() == 19) {
               TopUpWithCardFragment.this.expiryDate.requestFocus();
            }

         }

         public void beforeTextChanged(CharSequence var1, int var2, int var3, int var4) {
         }

         public void onTextChanged(CharSequence var1, int var2, int var3, int var4) {
            TopUpWithCardFragment.this.cardNumberWrapper.setError((CharSequence)null);
            co.uk.getmondo.create_account.a.a(var1, var4, TopUpWithCardFragment.this.cardNumber, this);
         }
      });
   }

   private void j() {
      this.cvv.addTextChangedListener(new TextWatcher() {
         public void afterTextChanged(Editable var1) {
            if(TopUpWithCardFragment.this.cvv.getText().length() == 3) {
               TopUpWithCardFragment.this.billingPostcode.requestFocus();
            }

         }

         public void beforeTextChanged(CharSequence var1, int var2, int var3, int var4) {
         }

         public void onTextChanged(CharSequence var1, int var2, int var3, int var4) {
            TopUpWithCardFragment.this.cvvWrapper.setError((CharSequence)null);
         }
      });
   }

   public void a() {
      this.cardNumberWrapper.setError(this.getContext().getString(2131362805));
   }

   public void a(String var1) {
      this.cardHolderName.setText(var1);
      this.cardNumber.requestFocus();
   }

   public void a(boolean var1) {
      this.topUpButton.setEnabled(var1);
   }

   public boolean a(Throwable var1) {
      return this.i.a(var1);
   }

   public void b() {
      this.expiryDateWrapper.setError(this.getContext().getString(2131362807));
   }

   public void c() {
      this.cvvWrapper.setError(this.getContext().getString(2131362806));
   }

   public void d() {
      this.billingPostcodeWrapper.setError(this.getContext().getString(2131362808));
   }

   public void e() {
      this.i.b();
   }

   public void f() {
      if(this.e == null) {
         this.e = new ProgressDialog(this.getContext(), 2131493134);
         this.e.setCancelable(false);
         this.e.setMessage(this.getContext().getString(2131362809));
      }

      if(!this.e.isShowing()) {
         this.e.show();
      }

   }

   public void g() {
      if(this.e != null && this.e.isShowing()) {
         this.e.dismiss();
      }

   }

   public void onActivityResult(int var1, int var2, Intent var3) {
      if(!this.g.a(var1, var2, var3)) {
         super.onActivityResult(var1, var2, var3);
      }

   }

   public void onAttach(Context var1) {
      super.onAttach(var1);
      this.i = (TopUpWithCardFragment.a)var1;
   }

   public void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.B().a(this);
      this.g = new co.uk.getmondo.topup.three_d_secure.b(this, this.c);
   }

   public View onCreateView(LayoutInflater var1, ViewGroup var2, Bundle var3) {
      return var1.inflate(2131034288, var2, false);
   }

   public void onDestroyView() {
      this.a.b();
      this.h.unbind();
      super.onDestroyView();
   }

   @OnTextChanged({2131821397})
   public void onPostcodeTextChanged(CharSequence var1) {
      this.billingPostcodeWrapper.setError((CharSequence)null);
   }

   @OnClick({2131821147})
   public void onTopUpClicked() {
      this.cardNumberWrapper.setError((CharSequence)null);
      this.expiryDateWrapper.setError((CharSequence)null);
      this.cvvWrapper.setError((CharSequence)null);
      this.cardHolderNameWrapper.setError((CharSequence)null);
      this.billingPostcodeWrapper.setError((CharSequence)null);
      this.a.a(this.cardHolderName.getText().toString(), this.cardNumber.getText().toString(), this.expiryDate.getText().toString(), this.cvv.getText().toString(), this.billingPostcode.getText().toString(), this.d, this.f.booleanValue(), true, this.g);
   }

   public void onViewCreated(View var1, Bundle var2) {
      super.onViewCreated(var1, var2);
      this.h = ButterKnife.bind(this, (View)var1);
      this.a.a((b.a)this);
      this.d = (co.uk.getmondo.d.c)this.getArguments().getParcelable("ARG_AMOUNT");
      this.f = Boolean.valueOf(this.getArguments().getBoolean("ARG_INITIAL_TOPUP"));
      String var3 = this.d.toString();
      this.descriptionView.setText(this.getContext().getString(2131362821, new Object[]{var3}));
      this.topUpButton.setText(this.getContext().getString(2131362790, new Object[]{var3}));
      this.h();
      this.i();
      this.j();
   }

   public interface a {
      boolean a(Throwable var1);

      void b();
   }
}
