package co.uk.getmondo.topup.card;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;

public class TopUpWithSavedCardActivity_ViewBinding implements Unbinder {
   private TopUpWithSavedCardActivity a;
   private View b;
   private View c;

   public TopUpWithSavedCardActivity_ViewBinding(final TopUpWithSavedCardActivity var1, View var2) {
      this.a = var1;
      var1.savedCardView = (SavedCardView)Utils.findRequiredViewAsType(var2, 2131821148, "field 'savedCardView'", SavedCardView.class);
      var1.descriptionView = (TextView)Utils.findRequiredViewAsType(var2, 2131820842, "field 'descriptionView'", TextView.class);
      View var3 = Utils.findRequiredView(var2, 2131821147, "field 'topUpButton' and method 'topUpClicked'");
      var1.topUpButton = (Button)Utils.castView(var3, 2131821147, "field 'topUpButton'", Button.class);
      this.b = var3;
      var3.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View var1x) {
            var1.topUpClicked();
         }
      });
      var2 = Utils.findRequiredView(var2, 2131821149, "method 'onAddNewCardClicked'");
      this.c = var2;
      var2.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View var1x) {
            var1.onAddNewCardClicked();
         }
      });
   }

   public void unbind() {
      TopUpWithSavedCardActivity var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.savedCardView = null;
         var1.descriptionView = null;
         var1.topUpButton = null;
         this.b.setOnClickListener((OnClickListener)null);
         this.b = null;
         this.c.setOnClickListener((OnClickListener)null);
         this.c = null;
      }
   }
}
