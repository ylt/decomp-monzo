package co.uk.getmondo.topup.card;

import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;

public class TopUpWithCardFragment_ViewBinding implements Unbinder {
   private TopUpWithCardFragment a;
   private View b;
   private TextWatcher c;
   private View d;

   public TopUpWithCardFragment_ViewBinding(final TopUpWithCardFragment var1, View var2) {
      this.a = var1;
      var1.descriptionView = (TextView)Utils.findRequiredViewAsType(var2, 2131820842, "field 'descriptionView'", TextView.class);
      var1.cardHolderNameWrapper = (TextInputLayout)Utils.findRequiredViewAsType(var2, 2131821388, "field 'cardHolderNameWrapper'", TextInputLayout.class);
      var1.cardHolderName = (EditText)Utils.findRequiredViewAsType(var2, 2131821389, "field 'cardHolderName'", EditText.class);
      var1.cardNumberWrapper = (TextInputLayout)Utils.findRequiredViewAsType(var2, 2131821390, "field 'cardNumberWrapper'", TextInputLayout.class);
      var1.cardNumber = (EditText)Utils.findRequiredViewAsType(var2, 2131821391, "field 'cardNumber'", EditText.class);
      var1.expiryDateWrapper = (TextInputLayout)Utils.findRequiredViewAsType(var2, 2131821392, "field 'expiryDateWrapper'", TextInputLayout.class);
      var1.expiryDate = (EditText)Utils.findRequiredViewAsType(var2, 2131821393, "field 'expiryDate'", EditText.class);
      var1.cvvWrapper = (TextInputLayout)Utils.findRequiredViewAsType(var2, 2131821394, "field 'cvvWrapper'", TextInputLayout.class);
      var1.cvv = (EditText)Utils.findRequiredViewAsType(var2, 2131821395, "field 'cvv'", EditText.class);
      var1.billingPostcodeWrapper = (TextInputLayout)Utils.findRequiredViewAsType(var2, 2131821396, "field 'billingPostcodeWrapper'", TextInputLayout.class);
      View var3 = Utils.findRequiredView(var2, 2131821397, "field 'billingPostcode' and method 'onPostcodeTextChanged'");
      var1.billingPostcode = (EditText)Utils.castView(var3, 2131821397, "field 'billingPostcode'", EditText.class);
      this.b = var3;
      this.c = new TextWatcher() {
         public void afterTextChanged(Editable var1x) {
         }

         public void beforeTextChanged(CharSequence var1x, int var2, int var3, int var4) {
         }

         public void onTextChanged(CharSequence var1x, int var2, int var3, int var4) {
            var1.onPostcodeTextChanged(var1x);
         }
      };
      ((TextView)var3).addTextChangedListener(this.c);
      var2 = Utils.findRequiredView(var2, 2131821147, "field 'topUpButton' and method 'onTopUpClicked'");
      var1.topUpButton = (Button)Utils.castView(var2, 2131821147, "field 'topUpButton'", Button.class);
      this.d = var2;
      var2.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View var1x) {
            var1.onTopUpClicked();
         }
      });
   }

   public void unbind() {
      TopUpWithCardFragment var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.descriptionView = null;
         var1.cardHolderNameWrapper = null;
         var1.cardHolderName = null;
         var1.cardNumberWrapper = null;
         var1.cardNumber = null;
         var1.expiryDateWrapper = null;
         var1.expiryDate = null;
         var1.cvvWrapper = null;
         var1.cvv = null;
         var1.billingPostcodeWrapper = null;
         var1.billingPostcode = null;
         var1.topUpButton = null;
         ((TextView)this.b).removeTextChangedListener(this.c);
         this.c = null;
         this.b = null;
         this.d.setOnClickListener((OnClickListener)null);
         this.d = null;
      }
   }
}
