package co.uk.getmondo.topup.card;

import co.uk.getmondo.topup.three_d_secure.ThreeDsResolver;
import com.stripe.android.model.Card;

// $FF: synthetic class
final class c implements io.reactivex.c.h {
   private final b a;
   private final Card b;
   private final boolean c;
   private final co.uk.getmondo.d.c d;
   private final boolean e;
   private final ThreeDsResolver f;

   private c(b var1, Card var2, boolean var3, co.uk.getmondo.d.c var4, boolean var5, ThreeDsResolver var6) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
      this.d = var4;
      this.e = var5;
      this.f = var6;
   }

   public static io.reactivex.c.h a(b var0, Card var1, boolean var2, co.uk.getmondo.d.c var3, boolean var4, ThreeDsResolver var5) {
      return new c(var0, var1, var2, var3, var4, var5);
   }

   public Object a(Object var1) {
      return b.a(this.a, this.b, this.c, this.d, this.e, this.f, (String)var1);
   }
}
