package co.uk.getmondo.topup.card;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class SavedCardView_ViewBinding implements Unbinder {
   private SavedCardView a;

   public SavedCardView_ViewBinding(SavedCardView var1, View var2) {
      this.a = var1;
      var1.cardImageView = (ImageView)Utils.findRequiredViewAsType(var2, 2131821755, "field 'cardImageView'", ImageView.class);
      var1.cardHolderNameView = (TextView)Utils.findRequiredViewAsType(var2, 2131821389, "field 'cardHolderNameView'", TextView.class);
      var1.cardLastFourView = (TextView)Utils.findRequiredViewAsType(var2, 2131821756, "field 'cardLastFourView'", TextView.class);
      var1.cardExpiryView = (TextView)Utils.findRequiredViewAsType(var2, 2131821757, "field 'cardExpiryView'", TextView.class);
   }

   public void unbind() {
      SavedCardView var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.cardImageView = null;
         var1.cardHolderNameView = null;
         var1.cardLastFourView = null;
         var1.cardExpiryView = null;
      }
   }
}
