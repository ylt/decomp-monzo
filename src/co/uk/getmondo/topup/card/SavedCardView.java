package co.uk.getmondo.topup.card;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.uk.getmondo.d.ae;

public class SavedCardView extends FrameLayout {
   @BindView(2131821757)
   TextView cardExpiryView;
   @BindView(2131821389)
   TextView cardHolderNameView;
   @BindView(2131821755)
   ImageView cardImageView;
   @BindView(2131821756)
   TextView cardLastFourView;

   public SavedCardView(Context var1) {
      this(var1, (AttributeSet)null);
   }

   public SavedCardView(Context var1, AttributeSet var2) {
      this(var1, var2, 0);
   }

   public SavedCardView(Context var1, AttributeSet var2, int var3) {
      this(var1, var2, var3, 0);
   }

   public SavedCardView(Context var1, AttributeSet var2, int var3, int var4) {
      super(var1, var2, var3, var4);
      ButterKnife.bind(LayoutInflater.from(this.getContext()).inflate(2131034436, this, true), (View)this);
   }

   private int a(String var1) {
      int var2;
      if("visa".equalsIgnoreCase(var1)) {
         var2 = 2130837684;
      } else if("mastercard".equalsIgnoreCase(var1)) {
         var2 = 2130837634;
      } else {
         var2 = 2130837632;
      }

      return var2;
   }

   public void a(ae var1) {
      this.cardImageView.setImageResource(this.a(var1.d()));
      this.cardLastFourView.setText(var1.c());
      this.cardExpiryView.setText(var1.a());
   }
}
