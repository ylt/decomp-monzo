package co.uk.getmondo.topup.card;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import co.uk.getmondo.common.activities.ConfirmationActivity;
import co.uk.getmondo.main.HomeActivity;

public class TopUpWithNewCardActivity extends co.uk.getmondo.common.activities.b implements TopUpWithCardFragment.a {
   public static void a(Context var0, co.uk.getmondo.d.c var1) {
      Intent var2 = new Intent(var0, TopUpWithNewCardActivity.class);
      var2.putExtra("EXTRA_TOPUP_AMOUNT", var1);
      var0.startActivity(var2);
   }

   public boolean a(Throwable var1) {
      return false;
   }

   public void b() {
      ConfirmationActivity.a(this, HomeActivity.b((Context)this));
   }

   public void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.l().a(this);
      co.uk.getmondo.d.c var2 = (co.uk.getmondo.d.c)this.getIntent().getParcelableExtra("EXTRA_TOPUP_AMOUNT");
      if(var2 == null) {
         throw new RuntimeException("Top up amount required");
      } else {
         this.setTitle(this.getString(2131362820, new Object[]{var2.toString()}));
         if(this.getSupportFragmentManager().a(16908290) == null) {
            this.getSupportFragmentManager().a().b(16908290, TopUpWithCardFragment.a(var2, false)).c();
         }

      }
   }
}
