package co.uk.getmondo.topup.card;

import co.uk.getmondo.topup.three_d_secure.ThreeDsResolver;

public final class g implements b.a {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;
   private final javax.a.a c;
   private final javax.a.a d;

   static {
      boolean var0;
      if(!g.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   public g(javax.a.a var1, javax.a.a var2, javax.a.a var3) {
      if(!a && var1 == null) {
         throw new AssertionError();
      } else {
         this.b = var1;
         if(!a && var2 == null) {
            throw new AssertionError();
         } else {
            this.c = var2;
            if(!a && var3 == null) {
               throw new AssertionError();
            } else {
               this.d = var3;
            }
         }
      }
   }

   public static b.a a(javax.a.a var0, javax.a.a var1, javax.a.a var2) {
      return new g(var0, var1, var2);
   }

   public void a(TopUpWithSavedCardActivity var1) {
      if(var1 == null) {
         throw new NullPointerException("Cannot inject members into a null reference");
      } else {
         var1.a = (co.uk.getmondo.common.a)this.b.b();
         var1.b = (h)this.c.b();
         var1.c = (ThreeDsResolver)this.d.b();
      }
   }
}
