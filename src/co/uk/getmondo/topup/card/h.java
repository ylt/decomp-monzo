package co.uk.getmondo.topup.card;

import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.topup.a.w;
import co.uk.getmondo.topup.three_d_secure.ThreeDsResolver;
import io.reactivex.u;

class h extends co.uk.getmondo.common.ui.b {
   private final u c;
   private final u d;
   private final co.uk.getmondo.common.e.a e;
   private final co.uk.getmondo.common.accounts.d f;
   private final co.uk.getmondo.topup.a.c g;
   private final w h;
   private final co.uk.getmondo.common.a i;
   private final ThreeDsResolver j;

   h(u var1, u var2, co.uk.getmondo.common.e.a var3, co.uk.getmondo.common.accounts.d var4, co.uk.getmondo.common.a var5, co.uk.getmondo.topup.a.c var6, w var7, ThreeDsResolver var8) {
      this.c = var1;
      this.d = var2;
      this.e = var3;
      this.f = var4;
      this.g = var6;
      this.h = var7;
      this.i = var5;
      this.j = var8;
   }

   // $FF: synthetic method
   static io.reactivex.d a(h var0, String var1, co.uk.getmondo.d.c var2, String var3) throws Exception {
      return var0.g.a(var3, var1, var2, false, var0.j);
   }

   // $FF: synthetic method
   static void a(h var0) throws Exception {
      var0.i.a(Impression.a(Impression.TopUpSuccessType.SAVED_CARD));
      ((h.a)var0.a).b();
      ((h.a)var0.a).a(true);
      ((h.a)var0.a).c();
   }

   // $FF: synthetic method
   static void a(h var0, Throwable var1) throws Exception {
      var0.i.a(Impression.e(var1.getMessage()));
      ((h.a)var0.a).b();
      ((h.a)var0.a).a(true);
      if(!var0.h.a((co.uk.getmondo.common.e.e)var0.a, var1) && !var0.e.a(var1, (co.uk.getmondo.common.e.a.a)var0.a)) {
         ((h.a)var0.a).b(2131362802);
      }

   }

   void a(co.uk.getmondo.d.c var1, String var2) {
      this.i.a(Impression.a(Impression.TopUpTapFrom.FROM_SAVED_CARD));
      ((h.a)this.a).a();
      ((h.a)this.a).a(false);
      this.a((io.reactivex.b.b)this.f.d().c(i.a(this, var2, var1)).b(this.d).a(this.c).a(j.a(this), k.a(this)));
   }

   public void a(h.a var1) {
      super.a((co.uk.getmondo.common.ui.f)var1);
      this.i.a(Impression.l());
   }

   interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      void a();

      void a(boolean var1);

      void b();

      void c();
   }
}
