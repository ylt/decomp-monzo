package co.uk.getmondo.topup.card;

// $FF: synthetic class
final class i implements io.reactivex.c.h {
   private final h a;
   private final String b;
   private final co.uk.getmondo.d.c c;

   private i(h var1, String var2, co.uk.getmondo.d.c var3) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
   }

   public static io.reactivex.c.h a(h var0, String var1, co.uk.getmondo.d.c var2) {
      return new i(var0, var1, var2);
   }

   public Object a(Object var1) {
      return h.a(this.a, this.b, this.c, (String)var1);
   }
}
