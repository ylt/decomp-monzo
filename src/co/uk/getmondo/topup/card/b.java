package co.uk.getmondo.topup.card;

import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.common.k.p;
import co.uk.getmondo.d.ac;
import co.uk.getmondo.topup.a.w;
import co.uk.getmondo.topup.three_d_secure.ThreeDsResolver;
import com.stripe.android.model.Card;
import io.reactivex.u;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class b extends co.uk.getmondo.common.ui.b {
   private static final SimpleDateFormat c;
   private final u d;
   private final u e;
   private final co.uk.getmondo.common.e.a f;
   private final co.uk.getmondo.common.accounts.d g;
   private final co.uk.getmondo.topup.a.c h;
   private final w i;
   private final co.uk.getmondo.common.a j;

   static {
      c = new SimpleDateFormat("MM/yy", Locale.UK);
   }

   b(u var1, u var2, co.uk.getmondo.common.e.a var3, co.uk.getmondo.common.accounts.d var4, co.uk.getmondo.common.a var5, co.uk.getmondo.topup.a.c var6, w var7) {
      this.d = var1;
      this.e = var2;
      this.f = var3;
      this.g = var4;
      this.h = var6;
      this.i = var7;
      this.j = var5;
   }

   // $FF: synthetic method
   static io.reactivex.d a(b var0, Card var1, boolean var2, co.uk.getmondo.d.c var3, boolean var4, ThreeDsResolver var5, String var6) throws Exception {
      return var0.h.a(var6, var1, var2, var3, var4, var5);
   }

   private Calendar a(String var1) {
      Calendar var4;
      try {
         Date var2 = c.parse(var1);
         var4 = Calendar.getInstance();
         var4.setTime(var2);
      } catch (ParseException var3) {
         var4 = null;
      }

      return var4;
   }

   private void a() {
      this.j.a(Impression.a(Impression.TopUpSuccessType.NEW_CARD));
      ((b.a)this.a).g();
      ((b.a)this.a).a(true);
      ((b.a)this.a).e();
   }

   // $FF: synthetic method
   static void a(b var0) {
      var0.a();
   }

   // $FF: synthetic method
   static void a(b var0, Throwable var1) {
      var0.a(var1);
   }

   private void a(Card var1, b.a var2) {
      if(!var1.validateNumber()) {
         var2.a();
      }

      if(!var1.validateCVC()) {
         var2.c();
      }

      if(!var1.validateExpiryDate()) {
         var2.b();
      }

   }

   private void a(Throwable var1) {
      d.a.a.a(var1, "Error topping up", new Object[0]);
      this.j.a(Impression.e(var1.getMessage()));
      ((b.a)this.a).g();
      ((b.a)this.a).a(true);
      if(!((b.a)this.a).a(var1) && !this.i.a((co.uk.getmondo.common.e.e)this.a, var1) && !this.f.a(var1, (co.uk.getmondo.common.e.a.a)this.a)) {
         ((b.a)this.a).b(2131362802);
      }

   }

   private String b(String var1) {
      return var1.trim().replaceAll("\\s+", "");
   }

   public void a(b.a var1) {
      super.a((co.uk.getmondo.common.ui.f)var1);
      this.j.a(Impression.m());
      ac var2 = this.g.b().d();
      if(var2 != null && !p.d(var2.c())) {
         var1.a(var2.c());
      }

   }

   void a(String var1, String var2, String var3, String var4, String var5, co.uk.getmondo.d.c var6, boolean var7, boolean var8, ThreeDsResolver var9) {
      this.j.a(Impression.a(Impression.TopUpTapFrom.FROM_NEW_CARD));
      var5 = var5.trim();
      Calendar var14 = this.a(this.b(var3));
      int var10;
      if(var14 == null) {
         var10 = 0;
      } else {
         var10 = var14.get(2) + 1;
      }

      int var11;
      if(var14 == null) {
         var11 = 0;
      } else {
         var11 = var14.get(1);
      }

      Card var13 = (new Card.Builder(this.b(var2), Integer.valueOf(var10), Integer.valueOf(var11), var4)).addressZip(var5).name(var1).build();
      boolean var12 = var13.validateCard();
      boolean var15;
      if(!var5.isEmpty()) {
         var15 = true;
      } else {
         var15 = false;
      }

      boolean var16;
      if(var12 && var15) {
         var16 = true;
      } else {
         var16 = false;
      }

      if(!var12) {
         this.a(var13, (b.a)this.a);
      }

      if(!var15) {
         ((b.a)this.a).d();
      }

      if(var16) {
         ((b.a)this.a).f();
         ((b.a)this.a).a(false);
         this.a((io.reactivex.b.b)this.g.d().c(c.a(this, var13, var8, var6, var7, var9)).b(this.e).a(this.d).a(d.a(this), e.a(this)));
      }

   }

   interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      void a();

      void a(String var1);

      void a(boolean var1);

      boolean a(Throwable var1);

      void b();

      void c();

      void d();

      void e();

      void f();

      void g();
   }
}
