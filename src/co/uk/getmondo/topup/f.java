package co.uk.getmondo.topup;

import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

// $FF: synthetic class
final class f implements OnTouchListener {
   private final e a;
   private final Runnable b;

   private f(e var1, Runnable var2) {
      this.a = var1;
      this.b = var2;
   }

   public static OnTouchListener a(e var0, Runnable var1) {
      return new f(var0, var1);
   }

   public boolean onTouch(View var1, MotionEvent var2) {
      return e.a(this.a, this.b, var1, var2);
   }
}
