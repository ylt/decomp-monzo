package co.uk.getmondo.topup;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import co.uk.getmondo.common.ui.AmountView;

public class TopUpActivity_ViewBinding implements Unbinder {
   private TopUpActivity a;
   private View b;
   private View c;
   private View d;

   public TopUpActivity_ViewBinding(final TopUpActivity var1, View var2) {
      this.a = var1;
      var1.toLoadAmountView = (AmountView)Utils.findRequiredViewAsType(var2, 2131821125, "field 'toLoadAmountView'", AmountView.class);
      View var3 = Utils.findRequiredView(var2, 2131821124, "field 'decreaseButton' and method 'onDecreaseClicked'");
      var1.decreaseButton = (ImageButton)Utils.castView(var3, 2131821124, "field 'decreaseButton'", ImageButton.class);
      this.b = var3;
      var3.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View var1x) {
            var1.onDecreaseClicked();
         }
      });
      var3 = Utils.findRequiredView(var2, 2131821126, "field 'increaseButton' and method 'onIncreaseClicked'");
      var1.increaseButton = (ImageButton)Utils.castView(var3, 2131821126, "field 'increaseButton'", ImageButton.class);
      this.c = var3;
      var3.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View var1x) {
            var1.onIncreaseClicked();
         }
      });
      var1.expectedBalanceView = (TextView)Utils.findRequiredViewAsType(var2, 2131821127, "field 'expectedBalanceView'", TextView.class);
      var1.topUpView = Utils.findRequiredView(var2, 2131821122, "field 'topUpView'");
      var1.topUpUnavailableView = Utils.findRequiredView(var2, 2131821120, "field 'topUpUnavailableView'");
      var1.topUpWithCardButton = (Button)Utils.findRequiredViewAsType(var2, 2131821129, "field 'topUpWithCardButton'", Button.class);
      var1.topUpWithBankButton = (Button)Utils.findRequiredViewAsType(var2, 2131821128, "field 'topUpWithBankButton'", Button.class);
      var1.overlayView = Utils.findRequiredView(var2, 2131821131, "field 'overlayView'");
      var1.progressBar = (ProgressBar)Utils.findRequiredViewAsType(var2, 2131821132, "field 'progressBar'", ProgressBar.class);
      var2 = Utils.findRequiredView(var2, 2131821121, "method 'onContactSupportClicked'");
      this.d = var2;
      var2.setOnClickListener(new DebouncingOnClickListener() {
         public void doClick(View var1x) {
            var1.onContactSupportClicked();
         }
      });
   }

   public void unbind() {
      TopUpActivity var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.toLoadAmountView = null;
         var1.decreaseButton = null;
         var1.increaseButton = null;
         var1.expectedBalanceView = null;
         var1.topUpView = null;
         var1.topUpUnavailableView = null;
         var1.topUpWithCardButton = null;
         var1.topUpWithBankButton = null;
         var1.overlayView = null;
         var1.progressBar = null;
         this.b.setOnClickListener((OnClickListener)null);
         this.b = null;
         this.c.setOnClickListener((OnClickListener)null);
         this.c = null;
         this.d.setOnClickListener((OnClickListener)null);
         this.d = null;
      }
   }
}
