package co.uk.getmondo.topup;

class a {
   private final int a;
   private final int b;
   private final int c;
   private int d;
   private com.b.b.b e;

   a(float var1, float var2, int var3) {
      this.a = (int)Math.ceil((double)var1);
      this.b = (int)Math.floor((double)var2);
      this.c = var3;
      this.d = this.a;
      this.e = com.b.b.b.b((Object)Integer.valueOf(this.d));
   }

   private void a(int var1) {
      this.d = var1;
      this.e.a((Object)Integer.valueOf(var1));
   }

   private int h() {
      byte var1;
      if(this.d < this.c) {
         var1 = 10;
      } else {
         var1 = 50;
      }

      return var1;
   }

   private int i() {
      byte var1;
      if(this.d <= this.c) {
         var1 = 10;
      } else {
         var1 = 50;
      }

      return var1;
   }

   boolean a() {
      int var1 = this.d + this.h();
      if(var1 <= this.b) {
         this.a(var1);
      }

      return this.c();
   }

   boolean b() {
      int var1 = this.d - this.i();
      if(var1 >= this.a) {
         this.a(var1);
      }

      return this.d();
   }

   boolean c() {
      boolean var1;
      if(this.d < this.b && this.d + this.h() <= this.b) {
         var1 = false;
      } else {
         var1 = true;
      }

      return var1;
   }

   boolean d() {
      boolean var1;
      if(this.d > this.a && this.d - this.i() >= this.a) {
         var1 = false;
      } else {
         var1 = true;
      }

      return var1;
   }

   int e() {
      return this.d;
   }

   io.reactivex.n f() {
      return this.e;
   }

   long g() {
      return (long)(this.d * 100);
   }
}
