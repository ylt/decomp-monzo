package co.uk.getmondo.topup.three_d_secure;

import io.reactivex.c.g;

// $FF: synthetic class
final class d implements g {
   private final b a;
   private final boolean b;

   private d(b var1, boolean var2) {
      this.a = var1;
      this.b = var2;
   }

   public static g a(b var0, boolean var1) {
      return new d(var0, var1);
   }

   public void a(Object var1) {
      b.a(this.a, this.b, (ThreeDsResolver.a)var1);
   }
}
