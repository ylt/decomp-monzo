package co.uk.getmondo.topup.three_d_secure;

import android.content.Intent;
import io.reactivex.v;

public interface ThreeDsResolver {
   v a(String var1, String var2, boolean var3);

   boolean a(int var1, int var2, Intent var3);

   public static class ThreeDsUnsuccessfulException extends RuntimeException {
      public final ThreeDsResolver.a a;

      public ThreeDsUnsuccessfulException(ThreeDsResolver.a var1) {
         super("Problem resolving 3DS redirection. Result=" + var1);
         this.a = var1;
      }
   }

   public static enum a {
      a,
      b,
      c,
      d;
   }
}
