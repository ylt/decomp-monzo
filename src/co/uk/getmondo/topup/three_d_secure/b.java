package co.uk.getmondo.topup.three_d_secure;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.Fragment;
import co.uk.getmondo.api.model.tracking.Impression;
import io.reactivex.v;
import io.reactivex.w;

public class b implements ThreeDsResolver {
   private Activity a;
   private Fragment b;
   private co.uk.getmondo.common.a c;
   private w d;

   public b(Activity var1, co.uk.getmondo.common.a var2) {
      this.a = var1;
      this.c = var2;
   }

   public b(Fragment var1, co.uk.getmondo.common.a var2) {
      this.b = var1;
      this.c = var2;
   }

   private void a(ThreeDsResolver.a var1, boolean var2) {
      if(var1 != null) {
         switch(null.a[var1.ordinal()]) {
         case 1:
            this.c.a(Impression.d(var2));
            break;
         case 2:
            this.c.a(Impression.c(var2));
         }
      }

   }

   // $FF: synthetic method
   static void a(b var0) throws Exception {
      var0.d = null;
   }

   // $FF: synthetic method
   static void a(b var0, boolean var1, ThreeDsResolver.a var2) throws Exception {
      var0.a(var2, var1);
      var0.c.a(Impression.e(var1));
   }

   // $FF: synthetic method
   static void a(b var0, boolean var1, String var2, String var3, w var4) throws Exception {
      var0.c.a(Impression.b(var1));
      Intent var5;
      if(var0.b != null) {
         var5 = ThreeDsWebActivity.a(var0.b.getContext(), var2, var3, var1);
         var0.b.startActivityForResult(var5, 501);
      } else {
         if(var0.a == null) {
            throw new IllegalArgumentException("ThreeDsWebResolver requires a Fragment or Activity to launch ThreeDsWebActivity");
         }

         var5 = ThreeDsWebActivity.a(var0.a, var2, var3, var1);
         var0.a.startActivityForResult(var5, 501);
      }

      var0.d = var4;
      var4.a(e.a(var0));
   }

   public v a(String var1, String var2, boolean var3) {
      return v.a(c.a(this, var3, var1, var2)).c(d.a(this, var3)).b(io.reactivex.a.b.a.a());
   }

   public boolean a(int param1, int param2, Intent param3) {
      // $FF: Couldn't be decompiled
   }
}
