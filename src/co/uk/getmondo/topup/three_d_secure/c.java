package co.uk.getmondo.topup.three_d_secure;

import io.reactivex.w;
import io.reactivex.y;

// $FF: synthetic class
final class c implements y {
   private final b a;
   private final boolean b;
   private final String c;
   private final String d;

   private c(b var1, boolean var2, String var3, String var4) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
      this.d = var4;
   }

   public static y a(b var0, boolean var1, String var2, String var3) {
      return new c(var0, var1, var2, var3);
   }

   public void a(w var1) {
      b.a(this.a, this.b, this.c, this.d, var1);
   }
}
