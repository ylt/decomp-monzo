package co.uk.getmondo.topup.three_d_secure;

import android.view.View;
import android.webkit.WebView;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class ThreeDsWebActivity_ViewBinding implements Unbinder {
   private ThreeDsWebActivity a;

   public ThreeDsWebActivity_ViewBinding(ThreeDsWebActivity var1, View var2) {
      this.a = var1;
      var1.webView = (WebView)Utils.findRequiredViewAsType(var2, 2131821117, "field 'webView'", WebView.class);
   }

   public void unbind() {
      ThreeDsWebActivity var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.webView = null;
      }
   }
}
