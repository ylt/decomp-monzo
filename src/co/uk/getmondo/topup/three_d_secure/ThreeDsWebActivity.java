package co.uk.getmondo.topup.three_d_secure;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.os.Build.VERSION;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.uk.getmondo.api.model.tracking.Impression;

public class ThreeDsWebActivity extends co.uk.getmondo.common.activities.b {
   co.uk.getmondo.common.a a;
   private Uri b;
   private String c;
   @BindView(2131821117)
   WebView webView;

   public static Intent a(Context var0, String var1, String var2, boolean var3) {
      Intent var4 = new Intent(var0, ThreeDsWebActivity.class);
      var4.putExtra("EXTRA_REDIRECT_URL", var1);
      var4.putExtra("EXTRA_RETURN_URL", var2);
      var4.putExtra("EXTRA_INITIAL_TOP_UP", var3);
      return var4;
   }

   public static ThreeDsResolver.a a(Intent var0) {
      ThreeDsResolver.a var2;
      if(!var0.hasExtra("status")) {
         var2 = ThreeDsResolver.a.d;
      } else {
         String var3 = var0.getStringExtra("status");
         byte var1 = -1;
         switch(var3.hashCode()) {
         case -1281977283:
            if(var3.equals("failed")) {
               var1 = 1;
            }
            break;
         case 945734241:
            if(var3.equals("succeeded")) {
               var1 = 0;
            }
         }

         switch(var1) {
         case 0:
            var2 = ThreeDsResolver.a.a;
            break;
         case 1:
            var2 = ThreeDsResolver.a.b;
            break;
         default:
            d.a.a.d("Unknown 3DS status found, status=%s", new Object[]{var3});
            var2 = ThreeDsResolver.a.d;
         }
      }

      return var2;
   }

   private void a(Uri var1) {
      String var2 = var1.getQueryParameter("status");
      Intent var3 = new Intent();
      var3.putExtra("status", var2);
      this.setResult(-1, var3);
      this.finish();
   }

   protected void o() {
      this.finish();
   }

   @SuppressLint({"SetJavaScriptEnabled"})
   protected void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.c = this.getIntent().getStringExtra("EXTRA_REDIRECT_URL");
      boolean var2 = this.getIntent().getBooleanExtra("EXTRA_INITIAL_TOP_UP", false);
      this.b = Uri.parse(this.getIntent().getStringExtra("EXTRA_RETURN_URL"));
      if(this.c != null && this.b != null) {
         this.setContentView(2131034218);
         ButterKnife.bind((Activity)this);
         this.l().a(this);
         android.support.v7.app.a var3 = this.getSupportActionBar();
         if(var3 != null) {
            var3.b(true);
            var3.a(2130837836);
         }

         d.a.a.c("Redirecting to url %s", new Object[]{this.c});
         this.webView.getSettings().setJavaScriptEnabled(true);
         this.webView.setWebViewClient(new ThreeDsWebActivity.a());
         this.a.a(Impression.a(var2, this.c));
         this.webView.postUrl(this.c, (byte[])null);
      } else {
         throw new RuntimeException("Redirect url and return url are required");
      }
   }

   private class a extends WebViewClient {
      private a() {
      }

      // $FF: synthetic method
      a(Object var2) {
         this();
      }

      public void onReceivedError(WebView var1, int var2, String var3, String var4) {
         super.onReceivedError(var1, var2, var3, var4);
         String var5 = "generic_error_dep, code: " + var2 + ", description: " + var3 + ", failingUrl: " + var4;
         ThreeDsWebActivity.this.a.a(Impression.a(ThreeDsWebActivity.this.c, var5));
      }

      public void onReceivedError(WebView var1, WebResourceRequest var2, WebResourceError var3) {
         super.onReceivedError(var1, var2, var3);
         String var4 = "generic_error";
         if(VERSION.SDK_INT >= 23) {
            var4 = "generic_error" + ", code: " + var3.getErrorCode() + ", description: " + var3.getDescription();
         }

         ThreeDsWebActivity.this.a.a(Impression.a(ThreeDsWebActivity.this.c, var4));
      }

      public void onReceivedHttpError(WebView var1, WebResourceRequest var2, WebResourceResponse var3) {
         super.onReceivedHttpError(var1, var2, var3);
         String var4 = "http_error, code: " + var3.getStatusCode() + " " + var3.getReasonPhrase();
         ThreeDsWebActivity.this.a.a(Impression.a(ThreeDsWebActivity.this.c, var4));
      }

      public void onReceivedSslError(WebView var1, SslErrorHandler var2, SslError var3) {
         super.onReceivedSslError(var1, var2, var3);
         String var4 = "ssl_error: " + var3.toString();
         ThreeDsWebActivity.this.a.a(Impression.a(ThreeDsWebActivity.this.c, var4));
      }

      public boolean shouldOverrideUrlLoading(WebView var1, String var2) {
         boolean var3 = true;
         Uri var4 = Uri.parse(var2);
         if(var4.getHost().equals(ThreeDsWebActivity.this.b.getHost())) {
            d.a.a.c("3DS return url intercepted: %s", new Object[]{var2});
            ThreeDsWebActivity.this.a(var4);
         } else {
            var3 = false;
         }

         return var3;
      }
   }
}
