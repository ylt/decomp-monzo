package co.uk.getmondo.topup;

import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import java.util.HashMap;
import java.util.Map;

public class e {
   private final Handler a = new Handler();
   private e.a b;
   private View c;
   private View d;
   private Map e = new HashMap();
   private final Runnable f = new Runnable() {
      public void run() {
         e.this.e.put(this, Boolean.valueOf(true));
         if(e.this.b != null && !e.this.b.a()) {
            e.this.a.postDelayed(this, 200L);
         }

      }
   };
   private final Runnable g = new Runnable() {
      public void run() {
         e.this.e.put(this, Boolean.valueOf(true));
         if(e.this.b != null && !e.this.b.b()) {
            e.this.a.postDelayed(this, 200L);
         }

      }
   };

   private OnTouchListener a(Runnable var1) {
      return f.a(this, var1);
   }

   // $FF: synthetic method
   static boolean a(e var0, Runnable var1, View var2, MotionEvent var3) {
      boolean var6 = false;
      boolean var5 = false;
      boolean var4 = var5;
      switch(var3.getAction()) {
      case 0:
         var2.setPressed(true);
         var0.e.put(var1, Boolean.valueOf(false));
         var0.a.postDelayed(var1, 600L);
         var4 = true;
         break;
      case 1:
         var2.setPressed(false);
         var0.a.removeCallbacks(var1);
         var4 = var6;
         if(var0.e.containsKey(var1)) {
            var4 = ((Boolean)var0.e.get(var1)).booleanValue();
         }

         if(!var4) {
            var2.performClick();
         }

         var4 = true;
      case 2:
         break;
      case 3:
         var2.setPressed(false);
         var0.a.removeCallbacks(var1);
         var4 = true;
         break;
      default:
         var4 = var5;
      }

      return var4;
   }

   public void a() {
      if(this.c != null) {
         this.c.setOnTouchListener((OnTouchListener)null);
      }

      if(this.d != null) {
         this.d.setOnTouchListener((OnTouchListener)null);
      }

      this.a.removeCallbacks(this.g);
      this.a.removeCallbacks(this.f);
   }

   public void a(View var1, View var2) {
      this.c = var1;
      this.d = var2;
      this.c.setOnTouchListener(this.a(this.f));
      this.d.setOnTouchListener(this.a(this.g));
   }

   public void a(e.a var1) {
      this.b = var1;
   }

   public interface a {
      boolean a();

      boolean b();
   }
}
