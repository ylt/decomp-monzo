package co.uk.getmondo.topup;

public final class p implements b.a {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;
   private final javax.a.a c;
   private final javax.a.a d;
   private final javax.a.a e;
   private final javax.a.a f;

   static {
      boolean var0;
      if(!p.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   public p(javax.a.a var1, javax.a.a var2, javax.a.a var3, javax.a.a var4, javax.a.a var5) {
      if(!a && var1 == null) {
         throw new AssertionError();
      } else {
         this.b = var1;
         if(!a && var2 == null) {
            throw new AssertionError();
         } else {
            this.c = var2;
            if(!a && var3 == null) {
               throw new AssertionError();
            } else {
               this.d = var3;
               if(!a && var4 == null) {
                  throw new AssertionError();
               } else {
                  this.e = var4;
                  if(!a && var5 == null) {
                     throw new AssertionError();
                  } else {
                     this.f = var5;
                  }
               }
            }
         }
      }
   }

   public static b.a a(javax.a.a var0, javax.a.a var1, javax.a.a var2, javax.a.a var3, javax.a.a var4) {
      return new p(var0, var1, var2, var3, var4);
   }

   public void a(TopUpActivity var1) {
      if(var1 == null) {
         throw new NullPointerException("Cannot inject members into a null reference");
      } else {
         var1.a = (q)this.b.b();
         var1.b = (co.uk.getmondo.common.q)this.c.b();
         var1.c = (co.uk.getmondo.common.a.c)this.d.b();
         var1.e = (b)this.e.b();
         var1.f = (co.uk.getmondo.common.a)this.f.b();
      }
   }
}
