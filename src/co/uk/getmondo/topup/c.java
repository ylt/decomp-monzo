package co.uk.getmondo.topup;

import com.google.android.gms.common.api.BooleanResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultCallback;

// $FF: synthetic class
final class c implements ResultCallback {
   private final b a;
   private final b.a b;

   private c(b var1, b.a var2) {
      this.a = var1;
      this.b = var2;
   }

   public static ResultCallback a(b var0, b.a var1) {
      return new c(var0, var1);
   }

   public void onResult(Result var1) {
      b.a(this.a, this.b, (BooleanResult)var1);
   }
}
