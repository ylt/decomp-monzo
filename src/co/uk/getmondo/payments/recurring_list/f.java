package co.uk.getmondo.payments.recurring_list;

import co.uk.getmondo.payments.a.i;
import io.reactivex.n;
import java.util.List;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\tB\u000f\b\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u0002H\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\n"},
   d2 = {"Lco/uk/getmondo/payments/recurring_list/RecurringPaymentsPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/payments/recurring_list/RecurringPaymentsPresenter$View;", "manager", "Lco/uk/getmondo/payments/data/RecurringPaymentsManager;", "(Lco/uk/getmondo/payments/data/RecurringPaymentsManager;)V", "register", "", "view", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class f extends co.uk.getmondo.common.ui.b {
   private final i c;

   public f(i var1) {
      l.b(var1, "manager");
      super();
      this.c = var1;
   }

   public void a(final f.a var1) {
      l.b(var1, "view");
      super.a((co.uk.getmondo.common.ui.f)var1);
      io.reactivex.b.a var2 = this.b;
      io.reactivex.b.b var3 = this.c.f().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(List var1x) {
            f.a var2 = var1;
            l.a(var1x, "it");
            var2.a(var1x);
         }
      }));
      l.a(var3, "manager.allRecurringPaym…owRecurringPayments(it) }");
      this.b = co.uk.getmondo.common.j.f.a(var2, var3);
      var2 = this.b;
      io.reactivex.b.b var4 = var1.a().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(co.uk.getmondo.payments.a.a.f var1x) {
            f.a var2 = var1;
            l.a(var1x, "it");
            var2.a(var1x);
         }
      }));
      l.a(var4, "view.onRecurringPaymentC…rringPaymentDetails(it) }");
      this.b = co.uk.getmondo.common.j.f.a(var2, var4);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0000\bf\u0018\u00002\u00020\u00012\u00020\u0002J\u000e\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004H&J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u0005H&J\u0016\u0010\t\u001a\u00020\u00072\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00050\u000bH&¨\u0006\f"},
      d2 = {"Lco/uk/getmondo/payments/recurring_list/RecurringPaymentsPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "onRecurringPaymentClicked", "Lio/reactivex/Observable;", "Lco/uk/getmondo/payments/data/model/RecurringPayment;", "showRecurringPaymentDetails", "", "recurringPayment", "showRecurringPayments", "recurringPayments", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      n a();

      void a(co.uk.getmondo.payments.a.a.f var1);

      void a(List var1);
   }
}
