package co.uk.getmondo.payments.recurring_list;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.i;
import android.support.v4.app.j;
import co.uk.getmondo.payments.recurring_cancellation.RecurringPaymentCancelActivity;
import java.util.HashMap;
import java.util.Locale;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.d.b.w;
import kotlin.d.b.y;
import kotlin.reflect.l;
import org.threeten.bp.LocalDate;
import org.threeten.bp.format.TextStyle;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 \r2\u00020\u0001:\u0001\rB\u0005¢\u0006\u0002\u0010\u0002J\u0012\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fH\u0016R\u001b\u0010\u0003\u001a\u00020\u00048BX\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0007\u0010\b\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u000e"},
   d2 = {"Lco/uk/getmondo/payments/recurring_list/RecurringPaymentDetailsDialog;", "Landroid/support/v4/app/DialogFragment;", "()V", "paymentScheduleFormatter", "Lco/uk/getmondo/payments/send/PaymentScheduleFormatter;", "getPaymentScheduleFormatter", "()Lco/uk/getmondo/payments/send/PaymentScheduleFormatter;", "paymentScheduleFormatter$delegate", "Lkotlin/Lazy;", "onCreateDialog", "Landroid/app/Dialog;", "savedInstanceState", "Landroid/os/Bundle;", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class c extends i {
   // $FF: synthetic field
   static final l[] a = new l[]{(l)y.a(new w(y.a(c.class), "paymentScheduleFormatter", "getPaymentScheduleFormatter()Lco/uk/getmondo/payments/send/PaymentScheduleFormatter;"))};
   public static final c.a b = new c.a((kotlin.d.b.i)null);
   private final kotlin.c c = kotlin.d.a((kotlin.d.a.a)(new kotlin.d.a.a() {
      public final co.uk.getmondo.payments.send.a b() {
         j var1 = c.this.getActivity();
         kotlin.d.b.l.a(var1, "activity");
         return new co.uk.getmondo.payments.send.a((Context)var1);
      }

      // $FF: synthetic method
      public Object v_() {
         return this.b();
      }
   }));
   private HashMap d;

   private final co.uk.getmondo.payments.send.a b() {
      kotlin.c var2 = this.c;
      l var1 = a[0];
      return (co.uk.getmondo.payments.send.a)var2.a();
   }

   public void a() {
      if(this.d != null) {
         this.d.clear();
      }

   }

   public Dialog onCreateDialog(Bundle var1) {
      final co.uk.getmondo.payments.a.a.f var4 = (co.uk.getmondo.payments.a.a.f)this.getArguments().getParcelable("KEY_RECURRING_PAYMENT");
      String var8;
      if(var4 instanceof co.uk.getmondo.payments.a.a.e) {
         co.uk.getmondo.payments.a.a.d var5 = ((co.uk.getmondo.payments.a.a.e)var4).e();
         co.uk.getmondo.payments.a.a.d.c var7 = var5.c();
         switch(d.a[var7.ordinal()]) {
         case 1:
            var8 = this.getString(2131362547);
            break;
         default:
            var8 = this.getString(2131362546, new Object[]{this.getString(var5.c().b())});
         }

         co.uk.getmondo.payments.send.a var6 = this.b();
         LocalDate var3 = var5.e();
         if(var3 == null) {
            var3 = var5.b();
         }

         String var10 = co.uk.getmondo.payments.send.a.a(var6, var3, true, (TextStyle)null, true, 4, (Object)null);
         String var13 = ((co.uk.getmondo.payments.a.a.e)var4).b().a();
         Locale var11 = Locale.ENGLISH;
         kotlin.d.b.l.a(var11, "Locale.ENGLISH");
         if(var13 == null) {
            throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
         }

         String var12 = var13.toUpperCase(var11);
         kotlin.d.b.l.a(var12, "(this as java.lang.String).toUpperCase(locale)");
         var8 = this.getString(2131362545, new Object[]{var12, var8, ((co.uk.getmondo.payments.a.a.e)var4).c().toString(), var10, var4.d(), ((co.uk.getmondo.payments.a.a.e)var4).b().c(), ((co.uk.getmondo.payments.a.a.e)var4).b().b()});
      } else {
         var8 = this.getString(2131362146, new Object[]{var4.d()});
      }

      int var2;
      if(var4 instanceof co.uk.getmondo.payments.a.a.e) {
         var2 = 2131362543;
      } else {
         var2 = 2131362145;
      }

      android.support.v7.app.d var9 = (new android.support.v7.app.d.a((Context)this.getActivity(), 2131493137)).a((CharSequence)var4.d()).b((CharSequence)var8).a(var2, (OnClickListener)(new OnClickListener() {
         public final void onClick(DialogInterface var1, int var2) {
            c var6 = c.this;
            RecurringPaymentCancelActivity.a var3 = RecurringPaymentCancelActivity.b;
            j var4x = c.this.getActivity();
            kotlin.d.b.l.a(var4x, "activity");
            Context var7 = (Context)var4x;
            co.uk.getmondo.payments.a.a.f var5 = var4;
            kotlin.d.b.l.a(var5, "recurringPayment");
            var6.startActivity(var3.a(var7, var5));
         }
      })).b(2131362608, (OnClickListener)null).b();
      kotlin.d.b.l.a(var9, "AlertDialog.Builder(acti…                .create()");
      return (Dialog)var9;
   }

   // $FF: synthetic method
   public void onDestroyView() {
      super.onDestroyView();
      this.a();
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bR\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000¨\u0006\t"},
      d2 = {"Lco/uk/getmondo/payments/recurring_list/RecurringPaymentDetailsDialog$Companion;", "", "()V", "KEY_RECURRING_PAYMENT", "", "newInstance", "Lco/uk/getmondo/payments/recurring_list/RecurringPaymentDetailsDialog;", "recurringPayment", "Lco/uk/getmondo/payments/data/model/RecurringPayment;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var1) {
         this();
      }

      public final c a(co.uk.getmondo.payments.a.a.f var1) {
         kotlin.d.b.l.b(var1, "recurringPayment");
         c var2 = new c();
         Bundle var3 = new Bundle();
         var3.putParcelable("KEY_RECURRING_PAYMENT", (Parcelable)var1);
         var2.setArguments(var3);
         return var2;
      }
   }
}
