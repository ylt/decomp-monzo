package co.uk.getmondo.payments.send.a;

import java.util.concurrent.Callable;

// $FF: synthetic class
final class i implements Callable {
   private final e a;

   private i(e var1) {
      this.a = var1;
   }

   public static Callable a(e var0) {
      return new i(var0);
   }

   public Object call() {
      return e.a(this.a);
   }
}
