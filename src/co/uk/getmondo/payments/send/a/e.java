package co.uk.getmondo.payments.send.a;

import android.content.ContentResolver;
import android.content.Context;
import android.database.ContentObserver;
import android.os.Handler;
import android.provider.ContactsContract.Contacts;
import io.michaelrocks.libphonenumber.android.NumberParseException;
import io.reactivex.n;
import io.reactivex.o;
import io.reactivex.r;
import io.reactivex.u;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class e {
   private final Context a;
   private final u b;
   private final io.michaelrocks.libphonenumber.android.h c;
   private final ContentResolver d;
   private final io.reactivex.i.c e;
   private final n f;

   public e(Context var1, u var2, u var3, io.michaelrocks.libphonenumber.android.h var4) {
      this.d = var1.getContentResolver();
      this.a = var1;
      this.b = var2;
      this.c = var4;
      this.e = io.reactivex.i.a.a();
      this.f = n.merge(this.e, this.d()).switchMap(f.a(this, var3)).replay(1).b();
   }

   // $FF: synthetic method
   static r a(e var0, u var1, Object var2) throws Exception {
      return n.fromCallable(i.a(var0)).subscribeOn(var1);
   }

   private String a(String var1) {
      try {
         io.michaelrocks.libphonenumber.android.j.a var3 = this.c.a((CharSequence)var1, (String)"GB");
         var1 = this.c.a(var3, io.michaelrocks.libphonenumber.android.h.a.a);
      } catch (NumberParseException var2) {
         var1 = "";
      }

      return var1;
   }

   // $FF: synthetic method
   static Map a(e var0) {
      return var0.c();
   }

   // $FF: synthetic method
   static void a(e var0, ContentObserver var1) throws Exception {
      var0.d.unregisterContentObserver(var1);
   }

   // $FF: synthetic method
   static void a(final e var0, final o var1) throws Exception {
      ContentObserver var2 = new ContentObserver(new Handler()) {
         public void onChange(boolean var1x) {
            var1.a(co.uk.getmondo.common.b.a.a);
         }
      };
      var0.d.registerContentObserver(Contacts.CONTENT_URI, false, var2);
      var1.a(h.a(var0, var2));
      var1.a(co.uk.getmondo.common.b.a.a);
   }

   private Map c() {
      boolean var1;
      if(android.support.v4.content.a.b(this.a, "android.permission.READ_CONTACTS") == 0) {
         var1 = true;
      } else {
         var1 = false;
      }

      d.a.a.a("Loading contacts from device", new Object[0]);
      Map var2;
      if(var1) {
         var2 = this.e();
      } else {
         d.a.a.a("Missing contacts permission. Returning an empty map instead", new Object[0]);
         var2 = Collections.emptyMap();
      }

      return var2;
   }

   private n d() {
      return n.create(g.a(this)).subscribeOn(this.b).throttleFirst(5L, TimeUnit.SECONDS, this.b);
   }

   private Map e() {
      // $FF: Couldn't be decompiled
   }

   public n a() {
      return this.f;
   }

   public void b() {
      this.e.onNext(co.uk.getmondo.common.b.a.a);
   }
}
