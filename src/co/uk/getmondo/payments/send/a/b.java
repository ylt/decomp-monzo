package co.uk.getmondo.payments.send.a;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import co.uk.getmondo.common.k.p;
import kotlin.Metadata;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u000f\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0004\b\u0086\b\u0018\u0000 %2\u00020\u00012\u00020\u0002:\u0001%B\u000f\b\u0016\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005B+\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\b\u0010\b\u001a\u0004\u0018\u00010\t\u0012\b\u0010\n\u001a\u0004\u0018\u00010\t\u0012\b\u0010\u000b\u001a\u0004\u0018\u00010\t¢\u0006\u0002\u0010\fJ\t\u0010\u0013\u001a\u00020\u0007HÆ\u0003J\u000b\u0010\u0014\u001a\u0004\u0018\u00010\tHÆ\u0003J\u000b\u0010\u0015\u001a\u0004\u0018\u00010\tHÆ\u0003J\u000b\u0010\u0016\u001a\u0004\u0018\u00010\tHÆ\u0003J7\u0010\u0017\u001a\u00020\u00002\b\b\u0002\u0010\u0006\u001a\u00020\u00072\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\t2\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\t2\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\tHÆ\u0001J\b\u0010\u0018\u001a\u00020\u0019H\u0016J\u0013\u0010\u001a\u001a\u00020\u001b2\b\u0010\u001c\u001a\u0004\u0018\u00010\u001dHÖ\u0003J\b\u0010\u001e\u001a\u00020\u001bH\u0016J\t\u0010\u001f\u001a\u00020\u0019HÖ\u0001J\t\u0010 \u001a\u00020\tHÖ\u0001J\u0018\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020\u00042\u0006\u0010$\u001a\u00020\u0019H\u0016R\u0014\u0010\u0006\u001a\u00020\u0007X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0016\u0010\b\u001a\u0004\u0018\u00010\tX\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0016\u0010\n\u001a\u0004\u0018\u00010\tX\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0010R\u0016\u0010\u000b\u001a\u0004\u0018\u00010\tX\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0010¨\u0006&"},
   d2 = {"Lco/uk/getmondo/payments/send/contacts/Contact;", "Lco/uk/getmondo/payments/send/contacts/BaseContact;", "Landroid/os/Parcelable;", "source", "Landroid/os/Parcel;", "(Landroid/os/Parcel;)V", "id", "", "name", "", "phoneNumber", "photo", "(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getId", "()J", "getName", "()Ljava/lang/String;", "getPhoneNumber", "getPhoto", "component1", "component2", "component3", "component4", "copy", "describeContents", "", "equals", "", "other", "", "hasPhoto", "hashCode", "toString", "writeToParcel", "", "dest", "flags", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class b extends a implements Parcelable {
   public static final Creator CREATOR = (Creator)(new Creator() {
      public b a(Parcel var1) {
         l.b(var1, "parcel");
         return new b(var1);
      }

      public b[] a(int var1) {
         return new b[var1];
      }

      // $FF: synthetic method
      public Object createFromParcel(Parcel var1) {
         return this.a(var1);
      }

      // $FF: synthetic method
      public Object[] newArray(int var1) {
         return (Object[])this.a(var1);
      }
   });
   public static final b.a a = new b.a((kotlin.d.b.i)null);
   private final long b;
   private final String c;
   private final String d;
   private final String e;

   public b(long var1, String var3, String var4, String var5) {
      this.b = var1;
      this.c = var3;
      this.d = var4;
      this.e = var5;
   }

   public b(Parcel var1) {
      l.b(var1, "source");
      this(var1.readLong(), var1.readString(), var1.readString(), var1.readString());
   }

   public long a() {
      return this.b;
   }

   public String b() {
      return this.c;
   }

   public String c() {
      return this.d;
   }

   public boolean d() {
      return p.c(this.e());
   }

   public int describeContents() {
      return 0;
   }

   public String e() {
      return this.e;
   }

   public boolean equals(Object var1) {
      boolean var4 = false;
      boolean var3;
      if(this != var1) {
         var3 = var4;
         if(!(var1 instanceof b)) {
            return var3;
         }

         b var5 = (b)var1;
         boolean var2;
         if(this.a() == var5.a()) {
            var2 = true;
         } else {
            var2 = false;
         }

         var3 = var4;
         if(!var2) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.b(), var5.b())) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.c(), var5.c())) {
            return var3;
         }

         var3 = var4;
         if(!l.a(this.e(), var5.e())) {
            return var3;
         }
      }

      var3 = true;
      return var3;
   }

   public int hashCode() {
      int var3 = 0;
      long var5 = this.a();
      int var4 = (int)(var5 ^ var5 >>> 32);
      String var7 = this.b();
      int var1;
      if(var7 != null) {
         var1 = var7.hashCode();
      } else {
         var1 = 0;
      }

      var7 = this.c();
      int var2;
      if(var7 != null) {
         var2 = var7.hashCode();
      } else {
         var2 = 0;
      }

      var7 = this.e();
      if(var7 != null) {
         var3 = var7.hashCode();
      }

      return (var2 + (var1 + var4 * 31) * 31) * 31 + var3;
   }

   public String toString() {
      return "Contact(id=" + this.a() + ", name=" + this.b() + ", phoneNumber=" + this.c() + ", photo=" + this.e() + ")";
   }

   public void writeToParcel(Parcel var1, int var2) {
      l.b(var1, "dest");
      var1.writeLong(this.a());
      var1.writeString(this.b());
      var1.writeString(this.c());
      var1.writeString(this.e());
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"},
      d2 = {"Lco/uk/getmondo/payments/send/contacts/Contact$Companion;", "", "()V", "CREATOR", "Landroid/os/Parcelable$Creator;", "Lco/uk/getmondo/payments/send/contacts/Contact;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var1) {
         this();
      }
   }
}
