package co.uk.getmondo.payments.send.a;

import android.database.ContentObserver;

// $FF: synthetic class
final class h implements io.reactivex.c.f {
   private final e a;
   private final ContentObserver b;

   private h(e var1, ContentObserver var2) {
      this.a = var1;
      this.b = var2;
   }

   public static io.reactivex.c.f a(e var0, ContentObserver var1) {
      return new h(var0, var1);
   }

   public void a() {
      e.a(this.a, this.b);
   }
}
