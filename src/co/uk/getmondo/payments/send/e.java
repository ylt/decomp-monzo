package co.uk.getmondo.payments.send;

import android.view.View;
import android.view.View.OnClickListener;

// $FF: synthetic class
final class e implements OnClickListener {
   private final SendMoneyAdapter.ContactViewHolder a;
   private final SendMoneyAdapter.c b;

   private e(SendMoneyAdapter.ContactViewHolder var1, SendMoneyAdapter.c var2) {
      this.a = var1;
      this.b = var2;
   }

   public static OnClickListener a(SendMoneyAdapter.ContactViewHolder var0, SendMoneyAdapter.c var1) {
      return new e(var0, var1);
   }

   public void onClick(View var1) {
      SendMoneyAdapter.ContactViewHolder.a(this.a, this.b, var1);
   }
}
