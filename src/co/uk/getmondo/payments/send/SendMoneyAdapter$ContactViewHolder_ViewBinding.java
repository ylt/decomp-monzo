package co.uk.getmondo.payments.send;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class SendMoneyAdapter$ContactViewHolder_ViewBinding implements Unbinder {
   private SendMoneyAdapter.ContactViewHolder a;

   public SendMoneyAdapter$ContactViewHolder_ViewBinding(SendMoneyAdapter.ContactViewHolder var1, View var2) {
      this.a = var1;
      var1.contactImageView = (ImageView)Utils.findRequiredViewAsType(var2, 2131821011, "field 'contactImageView'", ImageView.class);
      var1.nameTextView = (TextView)Utils.findRequiredViewAsType(var2, 2131821012, "field 'nameTextView'", TextView.class);
      var1.numberTextView = (TextView)Utils.findRequiredViewAsType(var2, 2131821592, "field 'numberTextView'", TextView.class);
      var1.actionTextView = (TextView)Utils.findRequiredViewAsType(var2, 2131821593, "field 'actionTextView'", TextView.class);
   }

   public void unbind() {
      SendMoneyAdapter.ContactViewHolder var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.contactImageView = null;
         var1.nameTextView = null;
         var1.numberTextView = null;
         var1.actionTextView = null;
      }
   }
}
