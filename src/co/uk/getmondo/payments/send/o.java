package co.uk.getmondo.payments.send;

import co.uk.getmondo.api.MonzoApi;
import co.uk.getmondo.api.model.ApiContactDiscovery;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.d.aa;
import co.uk.getmondo.d.ac;
import co.uk.getmondo.d.ak;
import co.uk.getmondo.payments.send.data.PeerToPeerRepository;
import io.reactivex.r;
import io.reactivex.u;
import io.reactivex.v;
import io.reactivex.c.q;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.TypeCastException;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u0084\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0002\b\u0003\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001.Bk\b\u0007\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\f\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u0012\u0006\u0010\u0014\u001a\u00020\u0015\u0012\u0006\u0010\u0016\u001a\u00020\u0017\u0012\u0006\u0010\u0018\u001a\u00020\u0019¢\u0006\u0002\u0010\u001aJB\u0010\u001d\u001a\u001a\u0012\n\u0012\b\u0012\u0004\u0012\u00020 0\u001f\u0012\n\u0012\b\u0012\u0004\u0012\u00020!0\u001f0\u001e2\u0018\u0010\"\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020 \u0012\u0004\u0012\u00020$0\u001e0#2\u0006\u0010\u0010\u001a\u00020%H\u0002J\u0010\u0010&\u001a\u00020$2\u0006\u0010'\u001a\u00020$H\u0002J\u0012\u0010(\u001a\u00020$2\b\u0010)\u001a\u0004\u0018\u00010$H\u0002J\u0010\u0010*\u001a\u00020$2\u0006\u0010'\u001a\u00020$H\u0002J\u0010\u0010+\u001a\u00020,2\u0006\u0010-\u001a\u00020\u0002H\u0016R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u001cX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006/"},
   d2 = {"Lco/uk/getmondo/payments/send/SendMoneyPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/payments/send/SendMoneyPresenter$View;", "uiScheduler", "Lio/reactivex/Scheduler;", "ioScheduler", "apiErrorHandler", "Lco/uk/getmondo/common/errors/ApiErrorHandler;", "accountService", "Lco/uk/getmondo/common/accounts/AccountService;", "hashSHA256", "Lco/uk/getmondo/common/utils/HashSHA256;", "analyticsService", "Lco/uk/getmondo/common/AnalyticsService;", "monzoApi", "Lco/uk/getmondo/api/MonzoApi;", "contacts", "Lco/uk/getmondo/payments/send/contacts/RxContacts;", "peerToPeerRepository", "Lco/uk/getmondo/payments/send/data/PeerToPeerRepository;", "paymentsManager", "Lco/uk/getmondo/payments/send/data/PaymentsManager;", "accountManager", "Lco/uk/getmondo/common/accounts/AccountManager;", "featureFlagsStorage", "Lco/uk/getmondo/common/FeatureFlagsStorage;", "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/common/accounts/AccountService;Lco/uk/getmondo/common/utils/HashSHA256;Lco/uk/getmondo/common/AnalyticsService;Lco/uk/getmondo/api/MonzoApi;Lco/uk/getmondo/payments/send/contacts/RxContacts;Lco/uk/getmondo/payments/send/data/PeerToPeerRepository;Lco/uk/getmondo/payments/send/data/PaymentsManager;Lco/uk/getmondo/common/accounts/AccountManager;Lco/uk/getmondo/common/FeatureFlagsStorage;)V", "contactNameComparator", "Lco/uk/getmondo/payments/send/contacts/ContactNameComparator;", "findContactsOnMonzo", "Lkotlin/Pair;", "", "Lco/uk/getmondo/payments/send/contacts/Contact;", "Lco/uk/getmondo/payments/send/contacts/ContactWithMultipleNumbers;", "pairs", "", "", "Lco/uk/getmondo/api/model/ApiContactDiscovery;", "firstPartOfHashedNumber", "number", "hash", "toHash", "nextDigitsOfHashedNumber", "register", "", "view", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class o extends co.uk.getmondo.common.ui.b {
   private final co.uk.getmondo.payments.send.a.c c;
   private final u d;
   private final u e;
   private final co.uk.getmondo.common.e.a f;
   private final co.uk.getmondo.common.accounts.d g;
   private final co.uk.getmondo.common.k.f h;
   private final co.uk.getmondo.common.a i;
   private final MonzoApi j;
   private final co.uk.getmondo.payments.send.a.e k;
   private final PeerToPeerRepository l;
   private final co.uk.getmondo.payments.send.data.a m;
   private final co.uk.getmondo.common.accounts.b n;
   private final co.uk.getmondo.common.o o;

   public o(u var1, u var2, co.uk.getmondo.common.e.a var3, co.uk.getmondo.common.accounts.d var4, co.uk.getmondo.common.k.f var5, co.uk.getmondo.common.a var6, MonzoApi var7, co.uk.getmondo.payments.send.a.e var8, PeerToPeerRepository var9, co.uk.getmondo.payments.send.data.a var10, co.uk.getmondo.common.accounts.b var11, co.uk.getmondo.common.o var12) {
      kotlin.d.b.l.b(var1, "uiScheduler");
      kotlin.d.b.l.b(var2, "ioScheduler");
      kotlin.d.b.l.b(var3, "apiErrorHandler");
      kotlin.d.b.l.b(var4, "accountService");
      kotlin.d.b.l.b(var5, "hashSHA256");
      kotlin.d.b.l.b(var6, "analyticsService");
      kotlin.d.b.l.b(var7, "monzoApi");
      kotlin.d.b.l.b(var8, "contacts");
      kotlin.d.b.l.b(var9, "peerToPeerRepository");
      kotlin.d.b.l.b(var10, "paymentsManager");
      kotlin.d.b.l.b(var11, "accountManager");
      kotlin.d.b.l.b(var12, "featureFlagsStorage");
      super();
      this.d = var1;
      this.e = var2;
      this.f = var3;
      this.g = var4;
      this.h = var5;
      this.i = var6;
      this.j = var7;
      this.k = var8;
      this.l = var9;
      this.m = var10;
      this.n = var11;
      this.o = var12;
      this.c = new co.uk.getmondo.payments.send.a.c();
   }

   private final String a(String var1) {
      if(var1 == null) {
         throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
      } else {
         var1 = var1.substring(5, 12);
         kotlin.d.b.l.a(var1, "(this as java.lang.Strin…ing(startIndex, endIndex)");
         return var1;
      }
   }

   private final kotlin.h a(List var1, ApiContactDiscovery var2) {
      ArrayList var6 = new ArrayList();
      android.support.v4.g.a var7 = new android.support.v4.g.a(var1.size());
      Iterator var11 = var1.iterator();

      while(var11.hasNext()) {
         boolean var3;
         kotlin.h var8;
         String var9;
         label47: {
            var8 = (kotlin.h)var11.next();
            var9 = this.b((String)var8.b());
            if(var2.a() != null && var2.a().containsKey(var9)) {
               Object var15 = var2.a().get(var9);
               if(var15 == null) {
                  kotlin.d.b.l.a();
               }

               Iterator var16 = ((List)var15).iterator();

               while(var16.hasNext()) {
                  String var10 = (String)var16.next();
                  if(kotlin.d.b.l.a(this.a((String)var8.b()), var10)) {
                     var3 = true;
                     break label47;
                  }
               }
            }

            var3 = false;
         }

         co.uk.getmondo.payments.send.a.b var13 = (co.uk.getmondo.payments.send.a.b)var8.a();
         long var4 = var13.a();
         if(var3) {
            var6.add(var13);
         } else if(var7.containsKey(Long.valueOf(var4))) {
            co.uk.getmondo.payments.send.a.d var17 = (co.uk.getmondo.payments.send.a.d)var7.get(Long.valueOf(var4));
            if(var17 != null) {
               List var19 = var17.f();
               if(var19 != null) {
                  String var14 = var13.c();
                  if(var14 == null) {
                     kotlin.d.b.l.a();
                  }

                  var19.add(var14);
               }
            }
         } else {
            ArrayList var18 = new ArrayList();
            var9 = var13.c();
            if(var9 == null) {
               kotlin.d.b.l.a();
            }

            var18.add(var9);
            var7.put(Long.valueOf(var4), new co.uk.getmondo.payments.send.a.d(var4, var13.b(), var13.e(), (List)var18));
         }
      }

      Collections.sort((List)var6, (Comparator)this.c);
      ArrayList var12 = new ArrayList(var7.values());
      Collections.sort((List)var12, (Comparator)this.c);
      return new kotlin.h(var6, var12);
   }

   private final String b(String var1) {
      if(var1 == null) {
         throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
      } else {
         var1 = var1.substring(0, 5);
         kotlin.d.b.l.a(var1, "(this as java.lang.Strin…ing(startIndex, endIndex)");
         return var1;
      }
   }

   private final String c(String var1) {
      try {
         var1 = this.h.a(var1);
         kotlin.d.b.l.a(var1, "hashSHA256.hash(toHash)");
      } catch (NoSuchAlgorithmException var2) {
         var1 = "";
      }

      return var1;
   }

   public void a(final o.a var1) {
      kotlin.d.b.l.b(var1, "view");
      super.a((co.uk.getmondo.common.ui.f)var1);
      var1.b().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Object var1) {
            o.this.k.b();
         }
      }));
      io.reactivex.n var2;
      if(var1.i()) {
         var2 = io.reactivex.n.just(kotlin.n.a);
      } else {
         var2 = var1.b();
      }

      io.reactivex.b.a var3 = this.b;
      io.reactivex.b.b var5 = io.reactivex.n.merge((r)var2, (r)var1.g()).switchMap((io.reactivex.c.h)(new io.reactivex.c.h() {
         // $FF: synthetic method
         public Object a(Object var1) {
            return this.b(var1);
         }

         public final io.reactivex.n b(Object var1) {
            kotlin.d.b.l.b(var1, "it");
            return o.this.k.a();
         }
      })).observeOn(this.d).doOnNext((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Map var1x) {
            var1.l();
         }
      })).observeOn(this.e).doOnNext((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Map var1) {
            ak var2 = o.this.g.b();
            if(var2 == null) {
               kotlin.d.b.l.a();
            }

            ac var3 = var2.d();
            if(var3 == null) {
               kotlin.d.b.l.a();
            }

            var1.remove(var3.f());
         }
      })).flatMapSingle((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final v a(Map var1) {
            kotlin.d.b.l.b(var1, "contactsMap");
            return io.reactivex.n.fromIterable((Iterable)var1.values()).map((io.reactivex.c.h)(new io.reactivex.c.h() {
               public final kotlin.h a(co.uk.getmondo.payments.send.a.b var1) {
                  kotlin.d.b.l.b(var1, "contact");
                  return new kotlin.h(var1, o.this.c(o.this.c(var1.c())));
               }
            })).toList();
         }
      })).flatMapSingle((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final v a(final List var1) {
            kotlin.d.b.l.b(var1, "pairs");
            return io.reactivex.n.fromIterable((Iterable)var1).map((io.reactivex.c.h)(new io.reactivex.c.h() {
               public final String a(kotlin.h var1) {
                  kotlin.d.b.l.b(var1, "contactAndHashedNumber");
                  return o.this.b((String)var1.b());
               }
            })).distinct().toList().a((io.reactivex.c.h)(new io.reactivex.c.h() {
               public final v a(List var1) {
                  kotlin.d.b.l.b(var1, "it");
                  return o.this.j.queryContacts(var1);
               }
            })).d((io.reactivex.c.h)(new io.reactivex.c.h() {
               public final kotlin.h a(ApiContactDiscovery var1x) {
                  kotlin.d.b.l.b(var1x, "contacts");
                  o var3 = o.this;
                  List var2 = var1;
                  kotlin.d.b.l.a(var2, "pairs");
                  return var3.a(var2, var1x);
               }
            }));
         }
      })).observeOn(this.d).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.h var1x) {
            Collection var2 = (Collection)var1x.c();
            Collection var3 = (Collection)var1x.d();
            var1.m();
            var1.a(var2, var3);
            o.this.i.a(Impression.Companion.a(var2.size(), var3.size()));
         }
      }), (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Throwable var1x) {
            var1.m();
            co.uk.getmondo.common.e.a var2 = o.this.f;
            kotlin.d.b.l.a(var1x, "error");
            var2.a(var1x, (co.uk.getmondo.common.e.a.a)var1);
         }
      }));
      kotlin.d.b.l.a(var5, "Observable.merge(permiss… view)\n                })");
      this.b = co.uk.getmondo.common.j.f.a(var3, var5);
      io.reactivex.b.a var6;
      if(this.n.b()) {
         var1.h();
         var6 = this.b;
         io.reactivex.b.b var7 = this.m.a().filter((q)(new q() {
            public final boolean a(List var1) {
               kotlin.d.b.l.b(var1, "it");
               return o.this.n.b();
            }
         })).observeOn(this.e).observeOn(this.d).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
            public final void a(List var1x) {
               o.a var2 = var1;
               kotlin.d.b.l.a(var1x, "it");
               var2.a((Collection)var1x);
            }
         }), (io.reactivex.c.g)(new io.reactivex.c.g() {
            public final void a(Throwable var1x) {
               co.uk.getmondo.common.e.a var2 = o.this.f;
               kotlin.d.b.l.a(var1x, "error");
               var2.a(var1x, (co.uk.getmondo.common.e.a.a)var1);
            }
         }));
         kotlin.d.b.l.a(var7, "paymentsManager.recentPa…ndleError(error, view) })");
         this.b = co.uk.getmondo.common.j.f.a(var6, var7);
         var6 = this.b;
         var7 = var1.f().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
            public final void a(co.uk.getmondo.payments.send.data.a.e var1x) {
               if(var1x instanceof co.uk.getmondo.payments.send.data.a.b) {
                  var1.a((co.uk.getmondo.payments.send.data.a.b)var1x);
               } else {
                  if(!(var1x instanceof aa)) {
                     throw (Throwable)(new RuntimeException("Unexpected payee type"));
                  }

                  var1.a((aa)var1x);
               }

            }
         }));
         kotlin.d.b.l.a(var7, "view.onRecentPayeeClicke…  }\n                    }");
         this.b = co.uk.getmondo.common.j.f.a(var6, var7);
         var3 = this.b;
         var5 = this.o.a().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
            public final void a(co.uk.getmondo.d.l var1x) {
               if(var1x.a()) {
                  if(!var1.i()) {
                     var1.j();
                  }

                  var1.k();
               }

            }
         }));
         kotlin.d.b.l.a(var5, "featureFlagsStorage.feat…  }\n                    }");
         this.b = co.uk.getmondo.common.j.f.a(var3, var5);
      } else {
         if(!var1.i()) {
            var1.j();
         }

         var1.k();
      }

      var3 = this.b;
      var5 = var1.c().doOnNext((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(co.uk.getmondo.payments.send.a.b var1x) {
            var1.n();
         }
      })).switchMap((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.n a(co.uk.getmondo.payments.send.a.b var1x) {
            kotlin.d.b.l.b(var1x, "contact");
            return o.this.l.a(var1x).f().subscribeOn(o.this.e).observeOn(o.this.d).doOnNext((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(aa var1x) {
                  var1.o();
               }
            })).doOnError((io.reactivex.c.g)(new io.reactivex.c.g() {
               public final void a(Throwable var1x) {
                  var1.o();
                  if(var1x instanceof PeerToPeerRepository.a) {
                     var1.b(2131362637);
                     var1.a(((PeerToPeerRepository.a)var1x).a());
                     o.this.i.a(Impression.Companion.Z());
                  } else if(var1x instanceof PeerToPeerRepository.UserHasP2pDisabledException) {
                     var1.b(2131362636);
                  } else {
                     co.uk.getmondo.common.e.a var2 = o.this.f;
                     kotlin.d.b.l.a(var1x, "error");
                     if(!var2.a(var1x, (co.uk.getmondo.common.e.a.a)var1)) {
                        var1.b(2131362642);
                     }
                  }

               }
            })).onErrorResumeNext((r)io.reactivex.n.empty());
         }
      })).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(aa var1x) {
            o.a var2 = var1;
            kotlin.d.b.l.a(var1x, "it");
            var2.a(var1x);
         }
      }));
      kotlin.d.b.l.a(var5, "view.onContactWithMonzoC… view.openSendMoney(it) }");
      this.b = co.uk.getmondo.common.j.f.a(var3, var5);
      var3 = this.b;
      var5 = var1.d().doOnNext((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(String var1) {
            o.this.i.a(Impression.Companion.aa());
         }
      })).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(String var1x) {
            var1.a(var1x);
         }
      }));
      kotlin.d.b.l.a(var5, "view.onContactNotOnMonzo… view.inviteContact(it) }");
      this.b = co.uk.getmondo.common.j.f.a(var3, var5);
      var6 = this.b;
      io.reactivex.b.b var4 = var1.e().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Object var1x) {
            var1.p();
            o.this.i.a(Impression.Companion.ab());
         }
      }));
      kotlin.d.b.l.a(var4, "view.onAddContactClicked…tact())\n                }");
      this.b = co.uk.getmondo.common.j.f.a(var6, var4);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u001e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\bf\u0018\u00002\u00020\u00012\u00020\u0002J\b\u0010\u0003\u001a\u00020\u0004H&J\u000e\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006H&J\b\u0010\b\u001a\u00020\tH&J\b\u0010\n\u001a\u00020\tH&J\b\u0010\u000b\u001a\u00020\tH&J\b\u0010\f\u001a\u00020\tH&J\u0012\u0010\r\u001a\u00020\t2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000fH&J\u000e\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006H&J\u000e\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u000f0\u0006H&J\u000e\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00130\u0006H&J\u000e\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00150\u0006H&J\u000e\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006H&J\b\u0010\u0017\u001a\u00020\tH&J\u0010\u0010\u0018\u001a\u00020\t2\u0006\u0010\u0019\u001a\u00020\u001aH&J\u0010\u0010\u001b\u001a\u00020\t2\u0006\u0010\u001c\u001a\u00020\u001dH&J\b\u0010\u001e\u001a\u00020\tH&J\b\u0010\u001f\u001a\u00020\tH&J$\u0010 \u001a\u00020\t2\f\u0010!\u001a\b\u0012\u0004\u0012\u00020\u00130\"2\f\u0010#\u001a\b\u0012\u0004\u0012\u00020$0\"H&J\b\u0010%\u001a\u00020\tH&J\u0016\u0010&\u001a\u00020\t2\f\u0010'\u001a\b\u0012\u0004\u0012\u00020\u00150\"H&¨\u0006("},
      d2 = {"Lco/uk/getmondo/payments/send/SendMoneyPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "checkContactsPermission", "", "contactsPermissionAcceptClicks", "Lio/reactivex/Observable;", "", "enableBankPayments", "", "enableContactsRefresh", "hideContactLoading", "hideListLoading", "inviteContact", "name", "", "onAddContactClicked", "onContactNotOnMonzoClicked", "onContactWithMonzoClicked", "Lco/uk/getmondo/payments/send/contacts/Contact;", "onRecentPayeeClicked", "Lco/uk/getmondo/payments/send/data/model/Payee;", "onRefreshAction", "openAddContact", "openBankTransfer", "bankPayee", "Lco/uk/getmondo/payments/send/data/model/BankPayee;", "openSendMoney", "peer", "Lco/uk/getmondo/model/Peer;", "requestContactsPermission", "showContactLoading", "showContacts", "contactsOnMonzo", "", "allContacts", "Lco/uk/getmondo/payments/send/contacts/ContactWithMultipleNumbers;", "showListLoading", "showRecentPayees", "recentPayees", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      void a(aa var1);

      void a(co.uk.getmondo.payments.send.data.a.b var1);

      void a(String var1);

      void a(Collection var1);

      void a(Collection var1, Collection var2);

      io.reactivex.n b();

      io.reactivex.n c();

      io.reactivex.n d();

      io.reactivex.n e();

      io.reactivex.n f();

      io.reactivex.n g();

      void h();

      boolean i();

      void j();

      void k();

      void l();

      void m();

      void n();

      void o();

      void p();
   }
}
