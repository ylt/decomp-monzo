package co.uk.getmondo.payments.send;

import android.content.Context;
import java.util.Locale;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.TypeCastException;
import org.threeten.bp.LocalDate;
import org.threeten.bp.Year;
import org.threeten.bp.format.TextStyle;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0007\u0018\u00002\u00020\u0001B\u0011\b\u0007\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\bJ,\u0010\t\u001a\u00020\u00062\u0006\u0010\n\u001a\u00020\u000b2\b\b\u0002\u0010\f\u001a\u00020\r2\b\b\u0002\u0010\u000e\u001a\u00020\u000f2\b\b\u0002\u0010\u0010\u001a\u00020\rJ\u000e\u0010\u0011\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0012"},
   d2 = {"Lco/uk/getmondo/payments/send/PaymentScheduleFormatter;", "", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "format", "", "schedule", "Lco/uk/getmondo/payments/data/model/PaymentSchedule;", "formatDate", "localDate", "Lorg/threeten/bp/LocalDate;", "monthBeforeDay", "", "monthTextStyle", "Lorg/threeten/bp/format/TextStyle;", "showYearIfDifferent", "formatShort", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class a {
   private final Context a;

   public a(Context var1) {
      kotlin.d.b.l.b(var1, "context");
      super();
      this.a = var1;
   }

   public final String a(co.uk.getmondo.payments.a.a.d var1) {
      String var4;
      if(var1 != null && !var1.a()) {
         String var2 = a(this, var1.b(), false, (TextStyle)null, true, 6, (Object)null);
         String var3 = this.a.getString(var1.c().b());
         if(var1.c() == co.uk.getmondo.payments.a.a.d.c.a) {
            var4 = var2;
         } else if(var1.d() == null) {
            var4 = this.a.getString(2131362539, new Object[]{var3, var2});
            kotlin.d.b.l.a(var4, "context.getString(R.stri…Name, startDateFormatted)");
         } else {
            var4 = this.a.getString(2131362537, new Object[]{var3, var2, a(this, var1.d(), false, (TextStyle)null, false, 14, (Object)null)});
            kotlin.d.b.l.a(var4, "context.getString(R.stri…atDate(schedule.endDate))");
         }
      } else {
         var4 = "";
      }

      return var4;
   }

   public final String a(LocalDate var1, boolean var2, TextStyle var3, boolean var4) {
      kotlin.d.b.l.b(var1, "localDate");
      kotlin.d.b.l.b(var3, "monthTextStyle");
      String var6 = var1.f().a(var3, Locale.ENGLISH);
      if(var2) {
         var6 = "" + var6 + ' ' + co.uk.getmondo.common.c.c.a(var1);
      } else {
         var6 = "" + co.uk.getmondo.common.c.c.a(var1) + ' ' + var6;
      }

      String var5 = var6;
      if(var4) {
         var5 = var6;
         if(var1.d() > Year.a().b()) {
            var5 = "" + var6 + ' ' + var1.d();
         }
      }

      return var5;
   }

   public final String b(co.uk.getmondo.payments.a.a.d var1) {
      kotlin.d.b.l.b(var1, "schedule");
      co.uk.getmondo.payments.a.a.d.c var2 = var1.c();
      String var3;
      switch(b.a[var2.ordinal()]) {
      case 1:
         var3 = this.a(var1.b(), true, TextStyle.a, true);
         var3 = this.a.getString(2131362540, new Object[]{var3});
         kotlin.d.b.l.a(var3, "context.getString(R.stri…edule_pattern_once, date)");
         break;
      case 2:
         var3 = this.a.getString(var1.c().c());
         if(var3 == null) {
            throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
         }

         var3 = var3.toLowerCase();
         kotlin.d.b.l.a(var3, "(this as java.lang.String).toLowerCase()");
         break;
      case 3:
         var3 = var1.b().i().a(TextStyle.a, Locale.ENGLISH);
         var3 = this.a.getString(2131362541, new Object[]{var3});
         kotlin.d.b.l.a(var3, "context.getString(R.stri…attern_weekly, dayOfWeek)");
         break;
      case 4:
         var3 = this.a.getString(2131362538, new Object[]{co.uk.getmondo.common.c.c.a(var1.b())});
         kotlin.d.b.l.a(var3, "context.getString(R.stri…ate.dayOfMonthWithSuffix)");
         break;
      case 5:
         var3 = a(this, var1.b(), true, TextStyle.a, false, 8, (Object)null);
         var3 = this.a.getString(2131362542, new Object[]{var3});
         kotlin.d.b.l.a(var3, "context.getString(R.stri…ule_pattern_yearly, date)");
         break;
      default:
         throw new NoWhenBranchMatchedException();
      }

      return var3;
   }
}
