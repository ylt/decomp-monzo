package co.uk.getmondo.payments.send.authentication;

// $FF: synthetic class
final class k implements io.reactivex.c.h {
   private final h a;
   private final String b;
   private final h.a c;

   private k(h var1, String var2, h.a var3) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
   }

   public static io.reactivex.c.h a(h var0, String var1, h.a var2) {
      return new k(var0, var1, var2);
   }

   public Object a(Object var1) {
      return h.a(this.a, this.b, this.c, (String)var1);
   }
}
