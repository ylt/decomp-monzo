package co.uk.getmondo.payments.send.authentication;

import co.uk.getmondo.api.ApiException;
import co.uk.getmondo.api.model.tracking.Impression;
import io.reactivex.u;
import io.reactivex.z;
import java.util.UUID;

class h extends co.uk.getmondo.common.ui.b {
   private final u c;
   private final u d;
   private final co.uk.getmondo.common.e.a e;
   private final co.uk.getmondo.payments.send.data.a f;
   private final co.uk.getmondo.common.a g;
   private final co.uk.getmondo.payments.send.data.a.f h;
   private final co.uk.getmondo.common.accounts.b i;

   h(u var1, u var2, co.uk.getmondo.common.e.a var3, co.uk.getmondo.payments.send.data.a var4, co.uk.getmondo.common.a var5, co.uk.getmondo.payments.send.data.a.f var6, co.uk.getmondo.common.accounts.b var7) {
      this.c = var1;
      this.d = var2;
      this.e = var3;
      this.f = var4;
      this.g = var5;
      this.h = var6;
      this.i = var7;
   }

   // $FF: synthetic method
   static z a(h var0, String var1, h.a var2, String var3) throws Exception {
      return var0.f.a(var0.i.c(), var0.h, var3, var1).b(var0.d).a(var0.c).a(n.a(var0, var2)).a((Object)Boolean.valueOf(true)).b((Object)Boolean.valueOf(false));
   }

   // $FF: synthetic method
   static void a(h.a var0, String var1) throws Exception {
      var0.f();
      var0.c();
   }

   private void a(h.a var1, Throwable var2) {
      if(var2 instanceof ApiException) {
         co.uk.getmondo.api.model.b var3 = ((ApiException)var2).e();
         if(var3 != null) {
            p var4 = (p)co.uk.getmondo.common.e.d.a(p.values(), var3.a());
            if(var4 != null) {
               var1.a(var4, this.h);
               if(var4 == p.d) {
                  this.g.a(Impression.R());
               }

               return;
            }
         }
      }

      if(!this.e.a(var2, var1)) {
         var1.b();
      }

   }

   // $FF: synthetic method
   static void a(h var0, h.a var1, Boolean var2) throws Exception {
      if(var0.h instanceof co.uk.getmondo.payments.send.data.a.g) {
         var0.g.a(Impression.h(var2.booleanValue()));
      }

      if(var2.booleanValue()) {
         var1.a(var0.h);
      } else {
         var1.e();
         var1.g();
         var1.d();
      }

   }

   // $FF: synthetic method
   static void a(h var0, h.a var1, Throwable var2) throws Exception {
      var0.a(var1, var2);
   }

   // $FF: synthetic method
   static boolean a(String var0) throws Exception {
      boolean var1;
      if(var0.length() == 4) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public void a(h.a var1) {
      super.a((co.uk.getmondo.common.ui.f)var1);
      if(this.h instanceof co.uk.getmondo.payments.send.data.a.g) {
         var1.a((co.uk.getmondo.payments.send.data.a.g)this.h);
      } else if(this.h instanceof co.uk.getmondo.payments.send.data.a.c) {
         var1.a((co.uk.getmondo.payments.send.data.a.c)this.h);
      }

      String var2 = UUID.randomUUID().toString();
      this.a((io.reactivex.b.b)var1.a().filter(i.a()).doOnNext(j.a(var1)).flatMapSingle(k.a(this, var2, var1)).subscribe(l.a(this, var1), m.a()));
   }

   interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      io.reactivex.n a();

      void a(p var1, co.uk.getmondo.payments.send.data.a.f var2);

      void a(co.uk.getmondo.payments.send.data.a.c var1);

      void a(co.uk.getmondo.payments.send.data.a.f var1);

      void a(co.uk.getmondo.payments.send.data.a.g var1);

      void b();

      void c();

      void d();

      void e();

      void f();

      void g();
   }
}
