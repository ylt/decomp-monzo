package co.uk.getmondo.payments.send.authentication;

import co.uk.getmondo.common.ui.PinEntryView;

// $FF: synthetic class
final class b implements PinEntryView.a {
   private final io.reactivex.o a;

   private b(io.reactivex.o var1) {
      this.a = var1;
   }

   public static PinEntryView.a a(io.reactivex.o var0) {
      return new b(var0);
   }

   public void a(String var1) {
      this.a.a(var1);
   }
}
