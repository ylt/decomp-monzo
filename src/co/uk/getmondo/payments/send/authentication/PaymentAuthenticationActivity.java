package co.uk.getmondo.payments.send.authentication;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.uk.getmondo.common.ui.AmountView;
import co.uk.getmondo.common.ui.PinEntryView;
import co.uk.getmondo.d.aa;
import co.uk.getmondo.payments.send.payment_category.PaymentCategoryActivity;

public class PaymentAuthenticationActivity extends co.uk.getmondo.common.activities.b implements h.a {
   h a;
   @BindView(2131821014)
   TextView accountNumberTextView;
   @BindView(2131821013)
   AmountView amountView;
   co.uk.getmondo.payments.send.a b;
   private co.uk.getmondo.common.ui.a c;
   @BindView(2131821011)
   ImageView contactImageView;
   @BindView(2131821012)
   TextView contactNameTextView;
   @BindView(2131821682)
   PinEntryView pinEntryView;
   @BindView(2131821018)
   ProgressBar progressBar;
   @BindView(2131821017)
   View progressBarOverlay;
   @BindView(2131821015)
   TextView scheduleDescriptionTextView;

   public static Intent a(Context var0, co.uk.getmondo.payments.send.data.a.f var1) {
      return (new Intent(var0, PaymentAuthenticationActivity.class)).putExtra("KEY_PAYMENT", var1);
   }

   // $FF: synthetic method
   static void a(PaymentAuthenticationActivity var0) throws Exception {
      var0.pinEntryView.setOnPinEnteredListener((PinEntryView.a)null);
   }

   // $FF: synthetic method
   static void a(PaymentAuthenticationActivity var0, io.reactivex.o var1) throws Exception {
      PinEntryView var2 = var0.pinEntryView;
      var1.getClass();
      var2.setOnPinEnteredListener(b.a(var1));
      var1.a(c.a(var0));
   }

   public io.reactivex.n a() {
      return io.reactivex.n.create(a.a(this));
   }

   public void a(p var1, co.uk.getmondo.payments.send.data.a.f var2) {
      boolean var3 = true;
      if(var1 == p.a) {
         co.uk.getmondo.common.d.e.a(true).show(this.getFragmentManager(), "tag_error_dialog");
      } else {
         String var4 = var1.a(this.getResources(), var2);
         if(var1 == p.b) {
            var3 = false;
         }

         co.uk.getmondo.common.d.a.a(this.getString(2131362641), var4, var3).show(this.getSupportFragmentManager(), "tag_error_dialog");
      }

   }

   public void a(co.uk.getmondo.payments.send.data.a.c var1) {
      byte var2 = 8;
      co.uk.getmondo.payments.send.data.a.b var4 = var1.c();
      this.contactImageView.setVisibility(8);
      this.contactNameTextView.setText(var4.b());
      this.amountView.setAmount(var1.a());
      String var3 = var4.a();
      String var8 = var4.e();
      String var5 = var1.b();
      this.accountNumberTextView.setText(TextUtils.join("  •  ", new String[]{var3, var8, var5}));
      this.accountNumberTextView.setVisibility(0);
      String var6 = this.b.a(var1.e());
      this.scheduleDescriptionTextView.setText(var6);
      TextView var7 = this.scheduleDescriptionTextView;
      if(!var6.isEmpty()) {
         var2 = 0;
      }

      var7.setVisibility(var2);
   }

   public void a(co.uk.getmondo.payments.send.data.a.f var1) {
      this.finish();
      this.startActivity(PaymentCategoryActivity.a((Context)this, (co.uk.getmondo.payments.send.data.a.f)var1));
   }

   public void a(co.uk.getmondo.payments.send.data.a.g var1) {
      aa var3 = var1.c();
      if(var3.f()) {
         com.bumptech.glide.g.b(this.contactImageView.getContext()).a(Uri.parse(var3.g())).h().a().a((com.bumptech.glide.g.b.j)(new co.uk.getmondo.common.ui.c(this.contactImageView)));
      } else {
         int var2 = (int)TypedValue.applyDimension(2, 26.0F, this.getResources().getDisplayMetrics());
         this.contactImageView.setImageDrawable(this.c.a(var3.b()).a(var2));
      }

      this.contactImageView.setVisibility(0);
      this.contactNameTextView.setText(var3.b());
      this.amountView.setAmount(var1.a());
   }

   public void b() {
      co.uk.getmondo.common.d.a.a(this.getString(2131362641), this.getString(2131362642), false).show(this.getSupportFragmentManager(), "tag_error_dialog");
   }

   public void c() {
      this.progressBar.setVisibility(0);
      this.progressBarOverlay.setVisibility(0);
   }

   public void d() {
      this.progressBar.setVisibility(8);
      this.progressBarOverlay.setVisibility(8);
   }

   public void e() {
      this.pinEntryView.a();
   }

   public void f() {
      this.pinEntryView.b();
   }

   public void g() {
      this.pinEntryView.c();
   }

   protected void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.setContentView(2131034192);
      ButterKnife.bind((Activity)this);
      co.uk.getmondo.payments.send.data.a.f var2 = (co.uk.getmondo.payments.send.data.a.f)this.getIntent().getParcelableExtra("KEY_PAYMENT");
      if(var2 == null) {
         throw new RuntimeException("Activity requires a payment");
      } else {
         co.uk.getmondo.common.ui.a var3 = this.c;
         this.c = co.uk.getmondo.common.ui.a.a((Context)this);
         this.l().a(new f(var2)).a(this);
         this.a.a((h.a)this);
      }
   }

   protected void onDestroy() {
      this.a.b();
      super.onDestroy();
   }
}
