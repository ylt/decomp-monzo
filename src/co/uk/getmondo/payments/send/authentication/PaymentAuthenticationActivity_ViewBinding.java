package co.uk.getmondo.payments.send.authentication;

import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import co.uk.getmondo.common.ui.AmountView;
import co.uk.getmondo.common.ui.PinEntryView;

public class PaymentAuthenticationActivity_ViewBinding implements Unbinder {
   private PaymentAuthenticationActivity a;

   public PaymentAuthenticationActivity_ViewBinding(PaymentAuthenticationActivity var1, View var2) {
      this.a = var1;
      var1.contactImageView = (ImageView)Utils.findRequiredViewAsType(var2, 2131821011, "field 'contactImageView'", ImageView.class);
      var1.contactNameTextView = (TextView)Utils.findRequiredViewAsType(var2, 2131821012, "field 'contactNameTextView'", TextView.class);
      var1.amountView = (AmountView)Utils.findRequiredViewAsType(var2, 2131821013, "field 'amountView'", AmountView.class);
      var1.progressBar = (ProgressBar)Utils.findRequiredViewAsType(var2, 2131821018, "field 'progressBar'", ProgressBar.class);
      var1.progressBarOverlay = Utils.findRequiredView(var2, 2131821017, "field 'progressBarOverlay'");
      var1.pinEntryView = (PinEntryView)Utils.findRequiredViewAsType(var2, 2131821682, "field 'pinEntryView'", PinEntryView.class);
      var1.accountNumberTextView = (TextView)Utils.findRequiredViewAsType(var2, 2131821014, "field 'accountNumberTextView'", TextView.class);
      var1.scheduleDescriptionTextView = (TextView)Utils.findRequiredViewAsType(var2, 2131821015, "field 'scheduleDescriptionTextView'", TextView.class);
   }

   public void unbind() {
      PaymentAuthenticationActivity var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.contactImageView = null;
         var1.contactNameTextView = null;
         var1.amountView = null;
         var1.progressBar = null;
         var1.progressBarOverlay = null;
         var1.pinEntryView = null;
         var1.accountNumberTextView = null;
         var1.scheduleDescriptionTextView = null;
      }
   }
}
