package co.uk.getmondo.payments.send.authentication;

import io.reactivex.u;

public final class o implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final b.a b;
   private final javax.a.a c;
   private final javax.a.a d;
   private final javax.a.a e;
   private final javax.a.a f;
   private final javax.a.a g;
   private final javax.a.a h;
   private final javax.a.a i;

   static {
      boolean var0;
      if(!o.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   public o(b.a var1, javax.a.a var2, javax.a.a var3, javax.a.a var4, javax.a.a var5, javax.a.a var6, javax.a.a var7, javax.a.a var8) {
      if(!a && var1 == null) {
         throw new AssertionError();
      } else {
         this.b = var1;
         if(!a && var2 == null) {
            throw new AssertionError();
         } else {
            this.c = var2;
            if(!a && var3 == null) {
               throw new AssertionError();
            } else {
               this.d = var3;
               if(!a && var4 == null) {
                  throw new AssertionError();
               } else {
                  this.e = var4;
                  if(!a && var5 == null) {
                     throw new AssertionError();
                  } else {
                     this.f = var5;
                     if(!a && var6 == null) {
                        throw new AssertionError();
                     } else {
                        this.g = var6;
                        if(!a && var7 == null) {
                           throw new AssertionError();
                        } else {
                           this.h = var7;
                           if(!a && var8 == null) {
                              throw new AssertionError();
                           } else {
                              this.i = var8;
                           }
                        }
                     }
                  }
               }
            }
         }
      }
   }

   public static b.a.b a(b.a var0, javax.a.a var1, javax.a.a var2, javax.a.a var3, javax.a.a var4, javax.a.a var5, javax.a.a var6, javax.a.a var7) {
      return new o(var0, var1, var2, var3, var4, var5, var6, var7);
   }

   public h a() {
      return (h)b.a.c.a(this.b, new h((u)this.c.b(), (u)this.d.b(), (co.uk.getmondo.common.e.a)this.e.b(), (co.uk.getmondo.payments.send.data.a)this.f.b(), (co.uk.getmondo.common.a)this.g.b(), (co.uk.getmondo.payments.send.data.a.f)this.h.b(), (co.uk.getmondo.common.accounts.b)this.i.b()));
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
