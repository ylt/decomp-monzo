package co.uk.getmondo.payments.send;

import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import android.support.v7.widget.RecyclerView.w;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.uk.getmondo.d.aa;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

class SendMoneyAdapter extends android.support.v7.widget.RecyclerView.a implements a.a.a.a.a.a {
   private final List a = new ArrayList();
   private final Collection b = new ArrayList();
   private final List c = new ArrayList();
   private boolean d = false;
   private final List e = new ArrayList();
   private SendMoneyAdapter.c f;
   private SendMoneyAdapter.a g;
   private SendMoneyAdapter.f h;
   private boolean i = false;
   private co.uk.getmondo.common.ui.a j;

   SendMoneyAdapter(Context var1) {
      this.j = co.uk.getmondo.common.ui.a.a(var1);
   }

   private void a() {
      ArrayList var1 = new ArrayList();
      if(this.i) {
         var1.add("BANK_PAYMENTS_ITEM");
         var1.addAll(this.c);
      }

      var1.addAll(this.a);
      var1.addAll(this.b);
      if(this.d) {
         var1.add("FOOTER_ITEM");
      }

      android.support.v7.f.b.b var2 = android.support.v7.f.b.a(new SendMoneyAdapter.d(this.e, var1));
      this.e.clear();
      this.e.addAll(var1);
      var2.a(this);
   }

   public long a(int var1) {
      Object var4 = this.e.get(var1);
      long var2;
      if(!var4.equals("BANK_PAYMENTS_ITEM") && !var4.equals("FOOTER_ITEM")) {
         if(var4 instanceof co.uk.getmondo.payments.send.a.b) {
            var2 = 10L;
         } else if(var4 instanceof co.uk.getmondo.payments.send.data.a.e) {
            var2 = 11L;
         } else {
            var2 = 12L;
         }
      } else {
         var2 = -1L;
      }

      return var2;
   }

   // $FF: synthetic method
   public w a(ViewGroup var1) {
      return this.b(var1);
   }

   public void a(SendMoneyAdapter.StickyHeaderViewHolder var1, int var2) {
      Resources var3 = var1.headerTextView.getResources();
      String var4;
      switch((int)this.a(var2)) {
      case 10:
         var4 = var3.getString(2131362638);
         break;
      case 11:
         var4 = var3.getString(2131362652);
         break;
      default:
         var4 = var3.getString(2131362633);
      }

      var1.a(var4);
   }

   void a(SendMoneyAdapter.a var1) {
      this.g = var1;
   }

   void a(SendMoneyAdapter.c var1) {
      this.f = var1;
   }

   void a(SendMoneyAdapter.f var1) {
      this.h = var1;
   }

   void a(Collection var1) {
      this.c.clear();
      this.c.addAll(var1);
      this.a();
   }

   void a(Collection var1, Collection var2) {
      this.a.clear();
      this.b.clear();
      this.a.addAll(var1);
      this.b.addAll(var2);
      this.d = true;
      this.a();
   }

   void a(boolean var1) {
      this.i = var1;
      this.a();
   }

   public SendMoneyAdapter.StickyHeaderViewHolder b(ViewGroup var1) {
      return new SendMoneyAdapter.StickyHeaderViewHolder(LayoutInflater.from(var1.getContext()).inflate(2131034371, var1, false));
   }

   public int getItemCount() {
      return this.e.size();
   }

   public int getItemViewType(int var1) {
      Object var2 = this.e.get(var1);
      byte var3;
      if(var2.equals("BANK_PAYMENTS_ITEM")) {
         var3 = 2;
      } else if(var2.equals("FOOTER_ITEM")) {
         var3 = 1;
      } else if(var2 instanceof co.uk.getmondo.payments.send.data.a.e) {
         var3 = 3;
      } else {
         var3 = 0;
      }

      return var3;
   }

   public void onBindViewHolder(w var1, int var2) {
      if(var1 instanceof SendMoneyAdapter.ContactViewHolder) {
         ((SendMoneyAdapter.ContactViewHolder)var1).a((co.uk.getmondo.payments.send.a.a)this.e.get(var2));
      }

      if(var1 instanceof SendMoneyAdapter.RecentPayeeViewHolder) {
         ((SendMoneyAdapter.RecentPayeeViewHolder)var1).a((co.uk.getmondo.payments.send.data.a.e)this.e.get(var2));
      }

   }

   public w onCreateViewHolder(ViewGroup var1, int var2) {
      LayoutInflater var3 = LayoutInflater.from(var1.getContext());
      Object var4;
      switch(var2) {
      case 0:
         var4 = new SendMoneyAdapter.ContactViewHolder(var3.inflate(2131034367, var1, false), this.f);
         break;
      case 1:
         var4 = new SendMoneyAdapter.e(var3.inflate(2131034368, var1, false));
         break;
      case 2:
         var4 = new SendMoneyAdapter.b(var3.inflate(2131034363, var1, false), this.g);
         break;
      case 3:
         var4 = new SendMoneyAdapter.RecentPayeeViewHolder(var3.inflate(2131034375, var1, false), this.h);
         break;
      default:
         throw new RuntimeException("Unexpected viewType: " + var2);
      }

      return (w)var4;
   }

   class ContactViewHolder extends w {
      @BindView(2131821593)
      TextView actionTextView;
      private int b;
      @BindView(2131821011)
      ImageView contactImageView;
      @BindView(2131821012)
      TextView nameTextView;
      @BindView(2131821592)
      TextView numberTextView;

      ContactViewHolder(View var2, SendMoneyAdapter.c var3) {
         super(var2);
         ButterKnife.bind(this, (View)var2);
         this.b = (int)TypedValue.applyDimension(2, 20.0F, this.nameTextView.getResources().getDisplayMetrics());
         this.itemView.setOnClickListener(e.a(this, var3));
      }

      // $FF: synthetic method
      static void a(SendMoneyAdapter.ContactViewHolder var0, SendMoneyAdapter.c var1, View var2) {
         Object var3 = SendMoneyAdapter.this.e.get(var0.getAdapterPosition());
         if(var1 != null && var3 != null && var3 instanceof co.uk.getmondo.payments.send.a.a) {
            var1.a((co.uk.getmondo.payments.send.a.a)var3, var3 instanceof co.uk.getmondo.payments.send.a.b);
         }

      }

      void a(co.uk.getmondo.payments.send.a.a var1) {
         Resources var2 = this.nameTextView.getResources();
         if(var1 instanceof co.uk.getmondo.payments.send.a.b) {
            this.actionTextView.setText(2131362632);
            this.actionTextView.setTextColor(android.support.v4.content.a.c(this.nameTextView.getContext(), 2131689477));
            this.numberTextView.setVisibility(8);
         } else {
            this.actionTextView.setText(2131362631);
            this.actionTextView.setTextColor(android.support.v4.content.a.c(this.nameTextView.getContext(), 2131689649));
            this.numberTextView.setVisibility(0);
            if(((co.uk.getmondo.payments.send.a.d)var1).f().size() > 1) {
               this.numberTextView.setText(var2.getString(2131362650, new Object[]{var1.c(), Integer.valueOf(((co.uk.getmondo.payments.send.a.d)var1).f().size() - 1)}));
            } else {
               this.numberTextView.setText(var1.c());
            }
         }

         this.nameTextView.setText(var1.b());
         if(var1.d()) {
            com.bumptech.glide.g.b(this.contactImageView.getContext()).a(Uri.parse(var1.e())).h().a(com.bumptech.glide.load.engine.b.b).a().a((com.bumptech.glide.g.b.j)(new co.uk.getmondo.common.ui.c(this.contactImageView)));
         } else if(var1.b() != null) {
            this.contactImageView.setImageDrawable(SendMoneyAdapter.this.j.a(var1.b()).a(this.b));
         }

      }
   }

   class RecentPayeeViewHolder extends w {
      private int b;
      @BindView(2131821608)
      TextView payeeIdentifierView;
      @BindView(2131821606)
      ImageView payeeImageView;
      @BindView(2131821607)
      TextView payeeNameView;

      RecentPayeeViewHolder(View var2, SendMoneyAdapter.f var3) {
         super(var2);
         ButterKnife.bind(this, (View)var2);
         this.b = (int)TypedValue.applyDimension(2, 20.0F, var2.getResources().getDisplayMetrics());
         var2.setOnClickListener(f.a(this, var3));
      }

      // $FF: synthetic method
      static void a(SendMoneyAdapter.RecentPayeeViewHolder var0, SendMoneyAdapter.f var1, View var2) {
         int var3 = var0.getAdapterPosition();
         if(var3 != -1) {
            var1.a((co.uk.getmondo.payments.send.data.a.e)SendMoneyAdapter.this.e.get(var3));
         }

      }

      void a(co.uk.getmondo.payments.send.data.a.e var1) {
         String var3 = var1.b();
         String var2;
         String var5;
         if(var1 instanceof co.uk.getmondo.payments.send.data.a.b) {
            co.uk.getmondo.payments.send.data.a.b var4 = (co.uk.getmondo.payments.send.data.a.b)var1;
            var5 = String.format("%s  •  %s", new Object[]{var4.a(), var4.e()});
            var2 = null;
         } else {
            if(!(var1 instanceof aa)) {
               throw new RuntimeException("Unexpected payee type");
            }

            aa var6 = (aa)var1;
            var5 = var6.d();
            var2 = var6.g();
         }

         if(var5 == null) {
            throw new RuntimeException("Payee is missing information");
         } else {
            if(TextUtils.isEmpty(var3)) {
               this.payeeIdentifierView.setVisibility(8);
            } else {
               this.payeeIdentifierView.setText(var5);
               var5 = var3;
            }

            if(var2 != null && !var2.isEmpty()) {
               com.bumptech.glide.g.b(this.payeeImageView.getContext()).a(Uri.parse(var2)).h().a(com.bumptech.glide.load.engine.b.b).a().a((com.bumptech.glide.g.b.j)(new co.uk.getmondo.common.ui.c(this.payeeImageView)));
            } else {
               this.payeeImageView.setImageDrawable(SendMoneyAdapter.this.j.a(var5).a(this.b));
            }

            this.payeeNameView.setText(var5);
         }
      }
   }

   static class StickyHeaderViewHolder extends w {
      @BindView(2131821599)
      TextView headerTextView;

      StickyHeaderViewHolder(View var1) {
         super(var1);
         ButterKnife.bind(this, (View)var1);
      }

      void a(String var1) {
         this.headerTextView.setText(var1);
      }
   }

   @FunctionalInterface
   interface a {
      void a();
   }

   private class b extends w {
      b(View var2, SendMoneyAdapter.a var3) {
         super(var2);
         var2.setOnClickListener(d.a(var3));
      }

      // $FF: synthetic method
      static void a(SendMoneyAdapter.a var0, View var1) {
         var0.a();
      }
   }

   @FunctionalInterface
   interface c {
      void a(co.uk.getmondo.payments.send.a.a var1, boolean var2);
   }

   private static class d extends android.support.v7.f.b.a {
      private final List a;
      private final List b;

      private d(List var1, List var2) {
         this.a = var1;
         this.b = var2;
      }

      // $FF: synthetic method
      d(List var1, List var2, Object var3) {
         this(var1, var2);
      }

      public int a() {
         return this.a.size();
      }

      public boolean a(int var1, int var2) {
         Object var5 = this.a.get(var1);
         Object var4 = this.b.get(var2);
         boolean var3;
         if(var5 instanceof co.uk.getmondo.payments.send.a.a && var4 instanceof co.uk.getmondo.payments.send.a.a) {
            if(((co.uk.getmondo.payments.send.a.a)var5).a() == ((co.uk.getmondo.payments.send.a.a)var4).a()) {
               var3 = true;
            } else {
               var3 = false;
            }
         } else {
            var3 = var5.equals(var4);
         }

         return var3;
      }

      public int b() {
         return this.b.size();
      }

      public boolean b(int var1, int var2) {
         return this.a.get(var1).equals(this.b.get(var2));
      }
   }

   private class e extends w {
      e(View var2) {
         super(var2);
      }
   }

   @FunctionalInterface
   interface f {
      void a(co.uk.getmondo.payments.send.data.a.e var1);
   }
}
