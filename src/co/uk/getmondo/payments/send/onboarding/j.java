package co.uk.getmondo.payments.send.onboarding;

// $FF: synthetic class
final class j implements io.reactivex.c.b {
   private final b.a a;

   private j(b.a var1) {
      this.a = var1;
   }

   public static io.reactivex.c.b a(b.a var0) {
      return new j(var0);
   }

   public void a(Object var1, Object var2) {
      b.a(this.a, (Boolean)var1, (Throwable)var2);
   }
}
