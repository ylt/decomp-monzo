package co.uk.getmondo.payments.send.onboarding;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import butterknife.BindView;
import butterknife.ButterKnife;

public class PeerToPeerIntroActivity extends co.uk.getmondo.common.activities.b implements b.a {
   b a;
   private final com.b.b.c b = com.b.b.c.a();
   @BindView(2131821027)
   Button enablePeerToPeerButton;
   @BindView(2131821028)
   Button moreInfoButton;
   @BindView(2131820798)
   Toolbar toolbar;

   public static Intent a(Context var0, co.uk.getmondo.common.t var1) {
      return (new Intent(var0, PeerToPeerIntroActivity.class)).putExtra("KEY_NAV_FLOW", var1);
   }

   public static Intent a(Context var0, co.uk.getmondo.common.t var1, co.uk.getmondo.payments.send.data.a.g var2) {
      return a(var0, var1).putExtra("KEY_PENDING_PAYMENT", var2);
   }

   public static co.uk.getmondo.common.t a(Intent var0) {
      return (co.uk.getmondo.common.t)var0.getSerializableExtra("KEY_NAV_FLOW");
   }

   public static co.uk.getmondo.payments.send.data.a.g b(Intent var0) {
      return (co.uk.getmondo.payments.send.data.a.g)var0.getParcelableExtra("KEY_PENDING_PAYMENT");
   }

   public io.reactivex.n a() {
      return com.b.a.c.c.a(this.enablePeerToPeerButton);
   }

   public void a(boolean var1) {
      Intent var3 = new Intent();
      var3.putExtras(this.getIntent());
      byte var2;
      if(var1) {
         var2 = -1;
      } else {
         var2 = 0;
      }

      this.setResult(var2, var3);
      this.finish();
   }

   public io.reactivex.n b() {
      return com.b.a.c.c.a(this.moreInfoButton);
   }

   public void b(boolean var1) {
      this.enablePeerToPeerButton.setEnabled(var1);
   }

   public io.reactivex.n c() {
      return this.b;
   }

   public void d() {
      PeerToPeerMoreInfoActivity.a(this, 1);
   }

   protected void onActivityResult(int var1, int var2, Intent var3) {
      if(var1 == 1 && var2 == -1) {
         this.b.a((Object)Boolean.valueOf(PeerToPeerMoreInfoActivity.a(var3)));
      } else {
         super.onActivityResult(var1, var2, var3);
      }

   }

   protected void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.setContentView(2131034195);
      this.l().a(this);
      ButterKnife.bind((Activity)this);
      this.toolbar.setNavigationIcon(2130837837);
      this.a.a((b.a)this);
   }

   protected void onDestroy() {
      this.a.b();
      super.onDestroy();
   }
}
