package co.uk.getmondo.payments.send.onboarding;

// $FF: synthetic class
final class t implements io.reactivex.c.g {
   private final PeerToPeerMoreInfoPresenter a;
   private final PeerToPeerMoreInfoPresenter.a b;

   private t(PeerToPeerMoreInfoPresenter var1, PeerToPeerMoreInfoPresenter.a var2) {
      this.a = var1;
      this.b = var2;
   }

   public static io.reactivex.c.g a(PeerToPeerMoreInfoPresenter var0, PeerToPeerMoreInfoPresenter.a var1) {
      return new t(var0, var1);
   }

   public void a(Object var1) {
      PeerToPeerMoreInfoPresenter.a(this.a, this.b, (Throwable)var1);
   }
}
