package co.uk.getmondo.payments.send.onboarding;

// $FF: synthetic class
final class p implements io.reactivex.c.h {
   private final PeerToPeerMoreInfoPresenter a;
   private final PeerToPeerMoreInfoPresenter.a b;

   private p(PeerToPeerMoreInfoPresenter var1, PeerToPeerMoreInfoPresenter.a var2) {
      this.a = var1;
      this.b = var2;
   }

   public static io.reactivex.c.h a(PeerToPeerMoreInfoPresenter var0, PeerToPeerMoreInfoPresenter.a var1) {
      return new p(var0, var1);
   }

   public Object a(Object var1) {
      return PeerToPeerMoreInfoPresenter.a(this.a, this.b, (PeerToPeerMoreInfoPresenter.MessageFromWeb)var1);
   }
}
