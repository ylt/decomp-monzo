package co.uk.getmondo.payments.send.onboarding;

import android.support.annotation.Keep;
import co.uk.getmondo.api.model.tracking.Impression;
import io.reactivex.z;

class PeerToPeerMoreInfoPresenter extends co.uk.getmondo.common.ui.b {
   private final io.reactivex.u c;
   private final io.reactivex.u d;
   private final co.uk.getmondo.common.e.a e;
   private final co.uk.getmondo.payments.send.data.h f;
   private final co.uk.getmondo.common.a g;

   PeerToPeerMoreInfoPresenter(io.reactivex.u var1, io.reactivex.u var2, co.uk.getmondo.common.e.a var3, co.uk.getmondo.payments.send.data.h var4, co.uk.getmondo.common.a var5) {
      this.c = var1;
      this.d = var2;
      this.e = var3;
      this.f = var4;
      this.g = var5;
   }

   // $FF: synthetic method
   static PeerToPeerMoreInfoPresenter.MessageFromWeb a(String var0) throws Exception {
      return (PeerToPeerMoreInfoPresenter.MessageFromWeb)(new com.google.gson.f()).a(var0, PeerToPeerMoreInfoPresenter.MessageFromWeb.class);
   }

   // $FF: synthetic method
   static io.reactivex.r a(PeerToPeerMoreInfoPresenter.a var0, PeerToPeerMoreInfoPresenter.MessageFromWeb var1) throws Exception {
      io.reactivex.n var2;
      if(var1.proceed()) {
         var2 = io.reactivex.n.just(var1);
      } else if(var1.dismiss()) {
         var0.b();
         var2 = io.reactivex.n.empty();
      } else {
         var2 = io.reactivex.n.empty();
      }

      return var2;
   }

   // $FF: synthetic method
   static z a(PeerToPeerMoreInfoPresenter var0, PeerToPeerMoreInfoPresenter.a var1, PeerToPeerMoreInfoPresenter.MessageFromWeb var2) throws Exception {
      return var0.f.a(true).b(var0.d).a(var0.c).d(t.a(var0, var1)).b((Object)Boolean.valueOf(false));
   }

   // $FF: synthetic method
   static void a(PeerToPeerMoreInfoPresenter.a var0, Boolean var1) throws Exception {
      var0.c();
   }

   // $FF: synthetic method
   static void a(PeerToPeerMoreInfoPresenter var0, PeerToPeerMoreInfoPresenter.a var1, Throwable var2) throws Exception {
      var0.e.a(var2, var1);
   }

   // $FF: synthetic method
   static boolean a(Boolean var0) throws Exception {
      return var0.booleanValue();
   }

   public void a(PeerToPeerMoreInfoPresenter.a var1) {
      super.a((co.uk.getmondo.common.ui.f)var1);
      this.g.a(Impression.K());
      this.a((io.reactivex.b.b)var1.a().map(n.a()).switchMap(o.a(var1)).switchMapSingle(p.a(this, var1)).filter(q.a()).subscribe(r.a(var1), s.a()));
   }

   @Keep
   private static class MessageFromWeb {
      private final String action;

      public MessageFromWeb(String var1) {
         this.action = var1;
      }

      boolean dismiss() {
         return this.action.equals("dismiss");
      }

      boolean proceed() {
         return this.action.equals("request_permission");
      }
   }

   interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      io.reactivex.n a();

      void b();

      void c();
   }
}
