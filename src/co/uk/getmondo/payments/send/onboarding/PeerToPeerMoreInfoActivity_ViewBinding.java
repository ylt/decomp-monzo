package co.uk.getmondo.payments.send.onboarding;

import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebView;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class PeerToPeerMoreInfoActivity_ViewBinding implements Unbinder {
   private PeerToPeerMoreInfoActivity a;

   public PeerToPeerMoreInfoActivity_ViewBinding(PeerToPeerMoreInfoActivity var1, View var2) {
      this.a = var1;
      var1.webView = (WebView)Utils.findRequiredViewAsType(var2, 2131821029, "field 'webView'", WebView.class);
      var1.toolbar = (Toolbar)Utils.findRequiredViewAsType(var2, 2131820798, "field 'toolbar'", Toolbar.class);
   }

   public void unbind() {
      PeerToPeerMoreInfoActivity var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.webView = null;
         var1.toolbar = null;
      }
   }
}
