package co.uk.getmondo.payments.send.onboarding;

import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class PeerToPeerIntroActivity_ViewBinding implements Unbinder {
   private PeerToPeerIntroActivity a;

   public PeerToPeerIntroActivity_ViewBinding(PeerToPeerIntroActivity var1, View var2) {
      this.a = var1;
      var1.enablePeerToPeerButton = (Button)Utils.findRequiredViewAsType(var2, 2131821027, "field 'enablePeerToPeerButton'", Button.class);
      var1.moreInfoButton = (Button)Utils.findRequiredViewAsType(var2, 2131821028, "field 'moreInfoButton'", Button.class);
      var1.toolbar = (Toolbar)Utils.findRequiredViewAsType(var2, 2131820798, "field 'toolbar'", Toolbar.class);
   }

   public void unbind() {
      PeerToPeerIntroActivity var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.enablePeerToPeerButton = null;
         var1.moreInfoButton = null;
         var1.toolbar = null;
      }
   }
}
