package co.uk.getmondo.payments.send.onboarding;

// $FF: synthetic class
final class k implements io.reactivex.c.g {
   private final b a;
   private final b.a b;

   private k(b var1, b.a var2) {
      this.a = var1;
      this.b = var2;
   }

   public static io.reactivex.c.g a(b var0, b.a var1) {
      return new k(var0, var1);
   }

   public void a(Object var1) {
      b.a(this.a, this.b, (Throwable)var1);
   }
}
