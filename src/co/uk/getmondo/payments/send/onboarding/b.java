package co.uk.getmondo.payments.send.onboarding;

import co.uk.getmondo.api.model.tracking.Impression;
import io.reactivex.z;

class b extends co.uk.getmondo.common.ui.b {
   private final io.reactivex.u c;
   private final io.reactivex.u d;
   private final co.uk.getmondo.common.e.a e;
   private final co.uk.getmondo.payments.send.data.h f;
   private final co.uk.getmondo.common.a g;

   b(io.reactivex.u var1, io.reactivex.u var2, co.uk.getmondo.common.e.a var3, co.uk.getmondo.payments.send.data.h var4, co.uk.getmondo.common.a var5) {
      this.c = var1;
      this.d = var2;
      this.e = var3;
      this.f = var4;
      this.g = var5;
   }

   // $FF: synthetic method
   static z a(b var0, b.a var1, Object var2) throws Exception {
      return var0.f.a(true).b(var0.d).a(var0.c).b(i.a(var1)).a(j.a(var1)).d(k.a(var0, var1)).b((Object)Boolean.valueOf(false));
   }

   // $FF: synthetic method
   static void a(b.a var0, io.reactivex.b.b var1) throws Exception {
      var0.b(false);
   }

   // $FF: synthetic method
   static void a(b.a var0, Boolean var1, Throwable var2) throws Exception {
      var0.b(true);
   }

   // $FF: synthetic method
   static void a(b.a var0, Object var1) throws Exception {
      var0.d();
   }

   // $FF: synthetic method
   static void a(b var0, b.a var1, Throwable var2) throws Exception {
      var0.e.a(var2, var1);
   }

   // $FF: synthetic method
   static boolean a(Boolean var0) throws Exception {
      return var0.booleanValue();
   }

   public void a(b.a var1) {
      super.a((co.uk.getmondo.common.ui.f)var1);
      this.g.a(Impression.J());
      io.reactivex.n var2 = var1.a().switchMapSingle(c.a(this, var1)).filter(d.a());
      var1.getClass();
      this.a((io.reactivex.b.b)var2.subscribe(e.a(var1), f.a()));
      this.a((io.reactivex.b.b)var1.b().subscribe(g.a(var1)));
      var2 = var1.c();
      var1.getClass();
      this.a((io.reactivex.b.b)var2.subscribe(h.a(var1)));
   }

   interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      io.reactivex.n a();

      void a(boolean var1);

      io.reactivex.n b();

      void b(boolean var1);

      io.reactivex.n c();

      void d();
   }
}
