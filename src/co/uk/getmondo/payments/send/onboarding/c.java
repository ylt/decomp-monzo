package co.uk.getmondo.payments.send.onboarding;

// $FF: synthetic class
final class c implements io.reactivex.c.h {
   private final b a;
   private final b.a b;

   private c(b var1, b.a var2) {
      this.a = var1;
      this.b = var2;
   }

   public static io.reactivex.c.h a(b var0, b.a var1) {
      return new c(var0, var1);
   }

   public Object a(Object var1) {
      return b.a(this.a, this.b, var1);
   }
}
