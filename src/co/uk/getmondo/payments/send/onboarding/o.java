package co.uk.getmondo.payments.send.onboarding;

// $FF: synthetic class
final class o implements io.reactivex.c.h {
   private final PeerToPeerMoreInfoPresenter.a a;

   private o(PeerToPeerMoreInfoPresenter.a var1) {
      this.a = var1;
   }

   public static io.reactivex.c.h a(PeerToPeerMoreInfoPresenter.a var0) {
      return new o(var0);
   }

   public Object a(Object var1) {
      return PeerToPeerMoreInfoPresenter.a(this.a, (PeerToPeerMoreInfoPresenter.MessageFromWeb)var1);
   }
}
