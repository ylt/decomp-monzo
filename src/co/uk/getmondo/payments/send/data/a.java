package co.uk.getmondo.payments.send.data;

import co.uk.getmondo.api.PaymentsApi;

public class a {
   private final PaymentsApi a;
   private final co.uk.getmondo.common.accounts.d b;
   private final co.uk.getmondo.payments.a.i c;
   private final f d;

   public a(PaymentsApi var1, co.uk.getmondo.common.accounts.d var2, co.uk.getmondo.payments.a.i var3, f var4) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
      this.d = var4;
   }

   private io.reactivex.b a(co.uk.getmondo.payments.send.data.a.c var1, String var2, String var3) {
      return this.b.d().c(c.a(this, var1, var3, var2));
   }

   private io.reactivex.b a(String var1, co.uk.getmondo.payments.send.data.a.g var2, String var3, String var4) {
      co.uk.getmondo.d.c var8 = var2.a();
      String var7 = var2.c().d();
      String var6 = "phone";
      String var5 = var7;
      if(co.uk.getmondo.common.k.p.d(var7)) {
         var5 = var2.c().e();
         var6 = "username";
      }

      io.reactivex.b var9;
      if(co.uk.getmondo.common.k.p.d(var5)) {
         var9 = io.reactivex.b.a((Throwable)(new IllegalArgumentException("Peer must have a phone number or username")));
      } else {
         var9 = this.a.transferMoneyToPeer(var1, var5, var6, -var8.k(), var8.l().b(), "pin", var3, var4, var2.b());
      }

      return var9;
   }

   // $FF: synthetic method
   static io.reactivex.d a(a var0, co.uk.getmondo.payments.send.data.a.c var1, String var2, String var3, String var4) throws Exception {
      co.uk.getmondo.d.c var5 = var1.a();
      co.uk.getmondo.payments.send.data.a.b var6 = var1.c();
      co.uk.getmondo.payments.a.a.d var7 = var1.e();
      io.reactivex.b var8;
      if(var7 != null && !var7.a()) {
         var8 = var0.c.a(var1, var3, var2);
      } else {
         var8 = var0.a.transferMoneyToBank(var4, var5.k(), var5.l().b(), var6.e(), var6.d(), var6.b(), var2, "pin", var3, var1.b());
      }

      return var8;
   }

   // $FF: synthetic method
   static io.reactivex.d a(a var0, co.uk.getmondo.payments.send.data.a.f var1, String var2, String var3, String var4) throws Exception {
      io.reactivex.b var5;
      if(var1 instanceof co.uk.getmondo.payments.send.data.a.g) {
         var5 = var0.a(var2, (co.uk.getmondo.payments.send.data.a.g)var1, var3, var4);
      } else {
         if(!(var1 instanceof co.uk.getmondo.payments.send.data.a.c)) {
            throw new RuntimeException("Unsupported payment type");
         }

         var5 = var0.a((co.uk.getmondo.payments.send.data.a.c)var1, var3, var4);
      }

      return var5;
   }

   public io.reactivex.b a(co.uk.getmondo.payments.send.data.a.b var1) {
      return this.a.validatePayee(var1.e(), var1.d());
   }

   public io.reactivex.b a(String var1, co.uk.getmondo.payments.send.data.a.f var2, String var3, String var4) {
      return io.reactivex.b.a(b.a(this, var2, var1, var3, var4));
   }

   public io.reactivex.n a() {
      return this.d.a();
   }
}
