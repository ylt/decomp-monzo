package co.uk.getmondo.payments.send.data;

import java.util.concurrent.Callable;

// $FF: synthetic class
final class b implements Callable {
   private final a a;
   private final co.uk.getmondo.payments.send.data.a.f b;
   private final String c;
   private final String d;
   private final String e;

   private b(a var1, co.uk.getmondo.payments.send.data.a.f var2, String var3, String var4, String var5) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
      this.d = var4;
      this.e = var5;
   }

   public static Callable a(a var0, co.uk.getmondo.payments.send.data.a.f var1, String var2, String var3, String var4) {
      return new b(var0, var1, var2, var3, var4);
   }

   public Object call() {
      return a.a(this.a, this.b, this.c, this.d, this.e);
   }
}
