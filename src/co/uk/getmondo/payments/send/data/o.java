package co.uk.getmondo.payments.send.data;

import co.uk.getmondo.api.MonzoApi;
import co.uk.getmondo.common.x;

public final class o implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;
   private final javax.a.a c;
   private final javax.a.a d;
   private final javax.a.a e;

   static {
      boolean var0;
      if(!o.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   public o(javax.a.a var1, javax.a.a var2, javax.a.a var3, javax.a.a var4) {
      if(!a && var1 == null) {
         throw new AssertionError();
      } else {
         this.b = var1;
         if(!a && var2 == null) {
            throw new AssertionError();
         } else {
            this.c = var2;
            if(!a && var3 == null) {
               throw new AssertionError();
            } else {
               this.d = var3;
               if(!a && var4 == null) {
                  throw new AssertionError();
               } else {
                  this.e = var4;
               }
            }
         }
      }
   }

   public static b.a.b a(javax.a.a var0, javax.a.a var1, javax.a.a var2, javax.a.a var3) {
      return new o(var0, var1, var2, var3);
   }

   public h a() {
      return new h((MonzoApi)this.b.b(), (p)this.c.b(), (co.uk.getmondo.common.o)this.d.b(), (x)this.e.b());
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
