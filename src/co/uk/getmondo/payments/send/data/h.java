package co.uk.getmondo.payments.send.data;

import co.uk.getmondo.api.MonzoApi;
import co.uk.getmondo.api.model.ApiConfig;
import co.uk.getmondo.api.model.ApiUserSettings;
import co.uk.getmondo.common.x;
import co.uk.getmondo.d.am;
import io.reactivex.v;
import io.realm.av;
import io.realm.bg;

public class h {
   private final MonzoApi a;
   private final p b;
   private final co.uk.getmondo.common.o c;
   private final x d;

   public h(MonzoApi var1, p var2, co.uk.getmondo.common.o var3, x var4) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
      this.d = var4;
   }

   // $FF: synthetic method
   static io.reactivex.d a(h var0, ApiUserSettings var1) throws Exception {
      return var0.a();
   }

   // $FF: synthetic method
   static bg a(av var0) {
      return var0.a(am.class).g();
   }

   // $FF: synthetic method
   static Boolean a(h var0) throws Exception {
      boolean var1;
      if(var0.b.a() == co.uk.getmondo.payments.send.data.a.d.a) {
         var1 = true;
      } else {
         var1 = false;
      }

      return Boolean.valueOf(var1);
   }

   // $FF: synthetic method
   static void a(h var0, ApiConfig var1) throws Exception {
      co.uk.getmondo.payments.send.data.a.d var3 = co.uk.getmondo.payments.send.data.a.d.a(var1.a().a());
      String var2;
      if(var1.b() == null) {
         var2 = "";
      } else {
         var2 = var1.b().a();
      }

      co.uk.getmondo.d.p var4 = (new co.uk.getmondo.d.a.h()).a(var1.d());
      var0.d.a(var1.e());
      var0.b.a(new am(var3.a(), var2, var4, var1.f()));
      var0.c.a(new co.uk.getmondo.d.l(var1.c().a(), var1.c().b()));
   }

   public io.reactivex.b a() {
      return this.a.config().c(k.a(this)).c();
   }

   public v a(boolean var1) {
      return this.a.optInToPeerToPeer(var1).c(i.a(this)).b(j.a(this));
   }

   public io.reactivex.n b() {
      return co.uk.getmondo.common.j.g.a(l.a()).filter(m.a()).map(n.a());
   }

   public String c() {
      av var2 = av.n();
      am var1 = (am)var2.a(am.class).h();
      String var3;
      if(var1 != null) {
         var3 = var1.b();
      } else {
         var3 = "";
      }

      var2.close();
      return var3;
   }

   public Boolean d() {
      return Boolean.valueOf(this.b.c());
   }

   public co.uk.getmondo.d.p e() {
      return this.b.b();
   }
}
