package co.uk.getmondo.payments.send.data;

import co.uk.getmondo.api.model.ApiUserSettings;

// $FF: synthetic class
final class i implements io.reactivex.c.h {
   private final h a;

   private i(h var1) {
      this.a = var1;
   }

   public static io.reactivex.c.h a(h var0) {
      return new i(var0);
   }

   public Object a(Object var1) {
      return h.a(this.a, (ApiUserSettings)var1);
   }
}
