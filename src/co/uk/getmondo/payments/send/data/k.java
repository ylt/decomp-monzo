package co.uk.getmondo.payments.send.data;

import co.uk.getmondo.api.model.ApiConfig;

// $FF: synthetic class
final class k implements io.reactivex.c.g {
   private final h a;

   private k(h var1) {
      this.a = var1;
   }

   public static io.reactivex.c.g a(h var0) {
      return new k(var0);
   }

   public void a(Object var1) {
      h.a(this.a, (ApiConfig)var1);
   }
}
