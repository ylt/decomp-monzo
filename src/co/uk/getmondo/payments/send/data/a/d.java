package co.uk.getmondo.payments.send.data.a;

public enum d {
   a("enabled"),
   b("disabled"),
   c("blocked");

   public final String d;

   private d(String var3) {
      this.d = var3;
   }

   public static d a(String var0) {
      d[] var4 = values();
      int var2 = var4.length;

      for(int var1 = 0; var1 < var2; ++var1) {
         d var3 = var4[var1];
         if(var3.d.equals(var0)) {
            return var3;
         }
      }

      throw new IllegalArgumentException("P2pStatus id " + var0 + " not found");
   }

   public String a() {
      return this.d;
   }
}
