package co.uk.getmondo.payments.send.data.a;

import android.os.Parcelable;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\bf\u0018\u00002\u00020\u0001R\u0012\u0010\u0002\u001a\u00020\u0003X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0004\u0010\u0005R\u0012\u0010\u0006\u001a\u00020\u0007X¦\u0004¢\u0006\u0006\u001a\u0004\b\b\u0010\t¨\u0006\n"},
   d2 = {"Lco/uk/getmondo/payments/send/data/model/Payment;", "Landroid/os/Parcelable;", "amount", "Lco/uk/getmondo/model/Amount;", "getAmount", "()Lco/uk/getmondo/model/Amount;", "payee", "Lco/uk/getmondo/payments/send/data/model/Payee;", "getPayee", "()Lco/uk/getmondo/payments/send/data/model/Payee;", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public interface f extends Parcelable {
   co.uk.getmondo.d.c a();

   e d();
}
