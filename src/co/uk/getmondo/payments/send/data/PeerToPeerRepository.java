package co.uk.getmondo.payments.send.data;

import co.uk.getmondo.api.PaymentsApi;
import co.uk.getmondo.d.aa;
import co.uk.getmondo.d.al;
import io.reactivex.v;
import kotlin.Metadata;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001:\u0002\u0011\u0012B\u000f\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0014\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b2\u0006\u0010\n\u001a\u00020\u000bJ\u0014\u0010\f\u001a\b\u0012\u0004\u0012\u00020\t0\b2\u0006\u0010\r\u001a\u00020\u0006J.\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\t0\b2\b\b\u0002\u0010\r\u001a\u00020\u00062\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u000b2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u0002R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082D¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0013"},
   d2 = {"Lco/uk/getmondo/payments/send/data/PeerToPeerRepository;", "", "paymentsApi", "Lco/uk/getmondo/api/PaymentsApi;", "(Lco/uk/getmondo/api/PaymentsApi;)V", "USER_KEY", "", "userByContact", "Lio/reactivex/Single;", "Lco/uk/getmondo/model/Peer;", "contact", "Lco/uk/getmondo/payments/send/contacts/Contact;", "userByUsername", "username", "verifyContactIsOnMonzo", "userResponse", "Lco/uk/getmondo/model/UserResponse;", "UserHasP2pDisabledException", "UserNotOnMonzoException", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class PeerToPeerRepository {
   private final String a;
   private final PaymentsApi b;

   public PeerToPeerRepository(PaymentsApi var1) {
      kotlin.d.b.l.b(var1, "paymentsApi");
      super();
      this.b = var1;
      this.a = "0";
   }

   private final v a(String var1, co.uk.getmondo.payments.send.a.b var2, al var3) {
      String var5;
      String var6;
      label48: {
         var6 = null;
         if(var2 != null) {
            var5 = var2.b();
            if(var5 != null) {
               break label48;
            }
         }

         var5 = var1;
      }

      v var9;
      if(var3 != null && var3.a() != null && var3.a().containsKey(this.a)) {
         al.a var10 = (al.a)var3.a().get(this.a);
         if(var10 != null && !var10.a()) {
            String var7 = var10.b();
            String var8 = var10.c();
            String var11;
            if(var2 != null) {
               var11 = var2.c();
            } else {
               var11 = null;
            }

            if(var2 != null) {
               var5 = var2.b();
            } else {
               var5 = null;
            }

            if(var2 != null) {
               var6 = var2.e();
            }

            boolean var4;
            if(var2 != null) {
               var4 = true;
            } else {
               var4 = false;
            }

            var9 = v.a((Object)(new aa(var7, var8, var11, var1, var5, var6, var4)));
            kotlin.d.b.l.a(var9, "Single.just(Peer(user.us…        contact != null))");
         } else {
            var9 = v.a((Throwable)(new PeerToPeerRepository.UserHasP2pDisabledException(var5)));
            kotlin.d.b.l.a(var9, "Single.error(UserHasP2pDisabledException(name))");
         }
      } else {
         var9 = v.a((Throwable)(new PeerToPeerRepository.a(var5)));
         kotlin.d.b.l.a(var9, "Single.error(UserNotOnMonzoException(name))");
      }

      return var9;
   }

   public final v a(final co.uk.getmondo.payments.send.a.b var1) {
      kotlin.d.b.l.b(var1, "contact");
      PaymentsApi var3 = this.b;
      String var2 = var1.c();
      if(var2 == null) {
         kotlin.d.b.l.a();
      }

      v var4 = var3.userByPhoneNumber(var2).a((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final v a(al var1x) {
            kotlin.d.b.l.b(var1x, "userResponse");
            return PeerToPeerRepository.a(PeerToPeerRepository.this, (String)null, var1, var1x, 1, (Object)null);
         }
      }));
      kotlin.d.b.l.a(var4, "paymentsApi.userByPhoneN…esponse = userResponse) }");
      return var4;
   }

   public final v a(final String var1) {
      kotlin.d.b.l.b(var1, "username");
      v var2 = this.b.userByUsername(var1).a((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final v a(al var1x) {
            kotlin.d.b.l.b(var1x, "userResponse");
            return PeerToPeerRepository.a(PeerToPeerRepository.this, var1, (co.uk.getmondo.payments.send.a.b)null, var1x, 2, (Object)null);
         }
      }));
      kotlin.d.b.l.a(var2, "paymentsApi.userByUserna…esponse = userResponse) }");
      return var2;
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00060\u0001j\u0002`\u0002B\u000f\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004¢\u0006\u0002\u0010\u0005J\u000b\u0010\b\u001a\u0004\u0018\u00010\u0004HÆ\u0003J\u0015\u0010\t\u001a\u00020\u00002\n\b\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u0004HÆ\u0001J\u0013\u0010\n\u001a\u00020\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\rHÖ\u0003J\t\u0010\u000e\u001a\u00020\u000fHÖ\u0001J\t\u0010\u0010\u001a\u00020\u0004HÖ\u0001R\u0013\u0010\u0003\u001a\u0004\u0018\u00010\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007¨\u0006\u0011"},
      d2 = {"Lco/uk/getmondo/payments/send/data/PeerToPeerRepository$UserHasP2pDisabledException;", "Ljava/lang/Exception;", "Lkotlin/Exception;", "user", "", "(Ljava/lang/String;)V", "getUser", "()Ljava/lang/String;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class UserHasP2pDisabledException extends Exception {
      private final String a;

      public UserHasP2pDisabledException(String var1) {
         this.a = var1;
      }

      public boolean equals(Object var1) {
         boolean var2;
         if(this != var1) {
            label26: {
               if(var1 instanceof PeerToPeerRepository.UserHasP2pDisabledException) {
                  PeerToPeerRepository.UserHasP2pDisabledException var3 = (PeerToPeerRepository.UserHasP2pDisabledException)var1;
                  if(kotlin.d.b.l.a(this.a, var3.a)) {
                     break label26;
                  }
               }

               var2 = false;
               return var2;
            }
         }

         var2 = true;
         return var2;
      }

      public int hashCode() {
         String var2 = this.a;
         int var1;
         if(var2 != null) {
            var1 = var2.hashCode();
         } else {
            var1 = 0;
         }

         return var1;
      }

      public String toString() {
         return "UserHasP2pDisabledException(user=" + this.a + ")";
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0003\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\u0004J\u000b\u0010\u0007\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u0015\u0010\b\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003HÆ\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fHÖ\u0003J\t\u0010\r\u001a\u00020\u000eHÖ\u0001J\t\u0010\u000f\u001a\u00020\u0003HÖ\u0001R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0010"},
      d2 = {"Lco/uk/getmondo/payments/send/data/PeerToPeerRepository$UserNotOnMonzoException;", "", "user", "", "(Ljava/lang/String;)V", "getUser", "()Ljava/lang/String;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a extends Throwable {
      private final String a;

      public a(String var1) {
         this.a = var1;
      }

      public final String a() {
         return this.a;
      }

      public boolean equals(Object var1) {
         boolean var2;
         if(this != var1) {
            label26: {
               if(var1 instanceof PeerToPeerRepository.a) {
                  PeerToPeerRepository.a var3 = (PeerToPeerRepository.a)var1;
                  if(kotlin.d.b.l.a(this.a, var3.a)) {
                     break label26;
                  }
               }

               var2 = false;
               return var2;
            }
         }

         var2 = true;
         return var2;
      }

      public int hashCode() {
         String var2 = this.a;
         int var1;
         if(var2 != null) {
            var1 = var2.hashCode();
         } else {
            var1 = 0;
         }

         return var1;
      }

      public String toString() {
         return "UserNotOnMonzoException(user=" + this.a + ")";
      }
   }
}
