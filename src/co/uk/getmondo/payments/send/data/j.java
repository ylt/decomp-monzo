package co.uk.getmondo.payments.send.data;

import java.util.concurrent.Callable;

// $FF: synthetic class
final class j implements Callable {
   private final h a;

   private j(h var1) {
      this.a = var1;
   }

   public static Callable a(h var0) {
      return new j(var0);
   }

   public Object call() {
      return h.a(this.a);
   }
}
