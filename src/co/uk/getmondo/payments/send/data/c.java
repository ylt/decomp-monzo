package co.uk.getmondo.payments.send.data;

// $FF: synthetic class
final class c implements io.reactivex.c.h {
   private final a a;
   private final co.uk.getmondo.payments.send.data.a.c b;
   private final String c;
   private final String d;

   private c(a var1, co.uk.getmondo.payments.send.data.a.c var2, String var3, String var4) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
      this.d = var4;
   }

   public static io.reactivex.c.h a(a var0, co.uk.getmondo.payments.send.data.a.c var1, String var2, String var3) {
      return new c(var0, var1, var2, var3);
   }

   public Object a(Object var1) {
      return a.a(this.a, this.b, this.c, this.d, (String)var1);
   }
}
