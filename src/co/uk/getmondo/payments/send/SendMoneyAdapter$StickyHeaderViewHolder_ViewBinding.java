package co.uk.getmondo.payments.send;

import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class SendMoneyAdapter$StickyHeaderViewHolder_ViewBinding implements Unbinder {
   private SendMoneyAdapter.StickyHeaderViewHolder a;

   public SendMoneyAdapter$StickyHeaderViewHolder_ViewBinding(SendMoneyAdapter.StickyHeaderViewHolder var1, View var2) {
      this.a = var1;
      var1.headerTextView = (TextView)Utils.findRequiredViewAsType(var2, 2131821599, "field 'headerTextView'", TextView.class);
   }

   public void unbind() {
      SendMoneyAdapter.StickyHeaderViewHolder var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.headerTextView = null;
      }
   }
}
