package co.uk.getmondo.payments.send;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class SendMoneyAdapter$RecentPayeeViewHolder_ViewBinding implements Unbinder {
   private SendMoneyAdapter.RecentPayeeViewHolder a;

   public SendMoneyAdapter$RecentPayeeViewHolder_ViewBinding(SendMoneyAdapter.RecentPayeeViewHolder var1, View var2) {
      this.a = var1;
      var1.payeeImageView = (ImageView)Utils.findRequiredViewAsType(var2, 2131821606, "field 'payeeImageView'", ImageView.class);
      var1.payeeNameView = (TextView)Utils.findRequiredViewAsType(var2, 2131821607, "field 'payeeNameView'", TextView.class);
      var1.payeeIdentifierView = (TextView)Utils.findRequiredViewAsType(var2, 2131821608, "field 'payeeIdentifierView'", TextView.class);
   }

   public void unbind() {
      SendMoneyAdapter.RecentPayeeViewHolder var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.payeeImageView = null;
         var1.payeeNameView = null;
         var1.payeeIdentifierView = null;
      }
   }
}
