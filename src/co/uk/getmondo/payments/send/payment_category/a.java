package co.uk.getmondo.payments.send.payment_category;

import android.view.View;
import android.view.View.OnClickListener;

// $FF: synthetic class
final class a implements OnClickListener {
   private final CategoryView a;
   private final co.uk.getmondo.d.h b;

   private a(CategoryView var1, co.uk.getmondo.d.h var2) {
      this.a = var1;
      this.b = var2;
   }

   public static OnClickListener a(CategoryView var0, co.uk.getmondo.d.h var1) {
      return new a(var0, var1);
   }

   public void onClick(View var1) {
      CategoryView.a(this.a, this.b, var1);
   }
}
