package co.uk.getmondo.payments.send.payment_category;

import co.uk.getmondo.api.model.feed.ApiFeedItem;
import java.util.concurrent.Callable;

// $FF: synthetic class
final class c implements Callable {
   private final b a;
   private final ApiFeedItem b;

   private c(b var1, ApiFeedItem var2) {
      this.a = var1;
      this.b = var2;
   }

   public static Callable a(b var0, ApiFeedItem var1) {
      return new c(var0, var1);
   }

   public Object call() {
      return b.a(this.a, this.b);
   }
}
