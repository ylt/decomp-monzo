package co.uk.getmondo.payments.send.payment_category;

import android.content.Context;
import android.content.SharedPreferences;
import co.uk.getmondo.api.MonzoApi;
import co.uk.getmondo.api.model.feed.ApiFeedItem;
import co.uk.getmondo.api.model.feed.ApiTransaction;
import io.reactivex.v;
import io.reactivex.z;

public class b {
   private final MonzoApi a;
   private final b.a b;

   public b(MonzoApi var1, b.a var2) {
      this.a = var1;
      this.b = var2;
   }

   private ApiFeedItem a(ApiFeedItem var1, String var2) {
      ApiTransaction var3 = var1.f().a(var2);
      return new ApiFeedItem(var1.a(), var1.b(), var1.c(), var1.d(), var1.e(), var3, (ApiFeedItem.Params)null, var1.h(), var1.i());
   }

   // $FF: synthetic method
   static z a(b var0, ApiFeedItem var1) throws Exception {
      ApiTransaction var3 = var1.f();
      v var4;
      if(var3 != null && var3.d() && var3.i() < 0L && a(var3) && var0.b.a() != null) {
         String var2 = var0.b.a();
         var4 = var0.a.updateTransactionCategory(var3.g(), var2).a((Object)var0.a(var1, var2)).c(d.a(var0)).d(e.a()).b((Object)var1);
      } else {
         var4 = v.a((Object)var1);
      }

      return var4;
   }

   private static boolean a(ApiTransaction var0) {
      boolean var1;
      if(var0.t() == null || !var0.t().isEmpty() && !"mondo".equalsIgnoreCase(var0.t()) && !"monzo".equalsIgnoreCase(var0.t())) {
         var1 = false;
      } else {
         var1 = true;
      }

      return var1;
   }

   // $FF: synthetic method
   static void b(b var0, ApiFeedItem var1) throws Exception {
      var0.b.b();
   }

   public v a(ApiFeedItem var1) {
      return v.a(c.a(this, var1));
   }

   public static class a {
      private final SharedPreferences a;

      public a(Context var1) {
         this.a = var1.getSharedPreferences("p2p_category_hack", 0);
      }

      String a() {
         return this.a.getString("KEY_CATEGORY", (String)null);
      }

      void a(String var1) {
         this.a.edit().putString("KEY_CATEGORY", var1).apply();
      }

      public void b() {
         this.a.edit().remove("KEY_CATEGORY").apply();
      }
   }
}
