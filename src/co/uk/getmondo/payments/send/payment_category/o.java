package co.uk.getmondo.payments.send.payment_category;

import co.uk.getmondo.api.model.tracking.Impression;
import io.reactivex.u;

class o extends co.uk.getmondo.common.ui.b {
   private final u c;
   private final co.uk.getmondo.common.a d;
   private final b.a e;
   private final co.uk.getmondo.payments.send.data.a.f f;

   o(u var1, b.a var2, co.uk.getmondo.common.a var3, co.uk.getmondo.payments.send.data.a.f var4) {
      this.c = var1;
      this.d = var3;
      this.e = var2;
      this.f = var4;
   }

   // $FF: synthetic method
   static co.uk.getmondo.d.h a(o var0, co.uk.getmondo.d.h var1) throws Exception {
      var0.e.a(var1.f());
      return var1;
   }

   private io.reactivex.n a(co.uk.getmondo.d.h var1) {
      return io.reactivex.n.fromCallable(s.a(this, var1)).subscribeOn(this.c);
   }

   // $FF: synthetic method
   static void a(o var0, o.a var1, co.uk.getmondo.d.h var2) throws Exception {
      var0.d.a(Impression.a(var2));
      var1.b();
   }

   // $FF: synthetic method
   static io.reactivex.n b(o var0, co.uk.getmondo.d.h var1) {
      return var0.a(var1);
   }

   public void a(o.a var1) {
      super.a((co.uk.getmondo.common.ui.f)var1);
      var1.a(this.f.a());
      var1.a(this.f.d().b());
      this.a((io.reactivex.b.b)var1.a().switchMap(p.a(this)).subscribe(q.a(this, var1), r.a()));
   }

   interface a extends co.uk.getmondo.common.ui.f {
      io.reactivex.n a();

      void a(co.uk.getmondo.d.c var1);

      void a(String var1);

      void b();
   }
}
