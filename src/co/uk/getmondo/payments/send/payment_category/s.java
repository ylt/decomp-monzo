package co.uk.getmondo.payments.send.payment_category;

import java.util.concurrent.Callable;

// $FF: synthetic class
final class s implements Callable {
   private final o a;
   private final co.uk.getmondo.d.h b;

   private s(o var1, co.uk.getmondo.d.h var2) {
      this.a = var1;
      this.b = var2;
   }

   public static Callable a(o var0, co.uk.getmondo.d.h var1) {
      return new s(var0, var1);
   }

   public Object call() {
      return o.a(this.a, this.b);
   }
}
