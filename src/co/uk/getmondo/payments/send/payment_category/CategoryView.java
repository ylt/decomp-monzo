package co.uk.getmondo.payments.send.payment_category;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.GridLayout.LayoutParams;

public class CategoryView extends GridLayout {
   private CategoryView.a a;

   public CategoryView(Context var1) {
      super(var1);
      this.a();
   }

   public CategoryView(Context var1, AttributeSet var2) {
      super(var1, var2);
      this.a();
   }

   public CategoryView(Context var1, AttributeSet var2, int var3) {
      super(var1, var2, var3);
      this.a();
   }

   private View a(co.uk.getmondo.d.h var1) {
      View var4 = LayoutInflater.from(this.getContext()).inflate(2131034356, this, false);
      TextView var3 = (TextView)var4.findViewById(2131821568);
      ImageView var2 = (ImageView)var4.findViewById(2131821567);
      var3.setText(var1.a());
      var3.setTextColor(android.support.v4.content.a.c(var4.getContext(), var1.b()));
      var2.setImageResource(var1.e());
      var4.setOnClickListener(a.a(this, var1));
      return var4;
   }

   private void a() {
      this.setColumnCount(this.getResources().getInteger(2131558402));
      co.uk.getmondo.d.h[] var3 = co.uk.getmondo.d.h.values();
      int var2 = var3.length;

      for(int var1 = 0; var1 < var2; ++var1) {
         View var4 = this.a(var3[var1]);
         LayoutParams var5 = new LayoutParams();
         var5.setGravity(17);
         var5.columnSpec = GridLayout.spec(Integer.MIN_VALUE, GridLayout.FILL, 1.0F);
         var4.setLayoutParams(var5);
         var4.setMinimumWidth(co.uk.getmondo.common.k.n.b(80));
         this.addView(var4);
      }

   }

   // $FF: synthetic method
   static void a(CategoryView var0, co.uk.getmondo.d.h var1, View var2) {
      if(var0.a != null) {
         var0.a.a(var1);
      }

   }

   public void setCategoryClickListener(CategoryView.a var1) {
      this.a = var1;
   }

   interface a {
      void a(co.uk.getmondo.d.h var1);
   }
}
