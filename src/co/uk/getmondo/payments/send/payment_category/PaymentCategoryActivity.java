package co.uk.getmondo.payments.send.payment_category;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.uk.getmondo.common.ui.AmountView;
import co.uk.getmondo.main.HomeActivity;

public class PaymentCategoryActivity extends co.uk.getmondo.common.activities.b implements o.a {
   o a;
   @BindView(2131821019)
   AmountView amountView;
   @BindView(2131821022)
   CategoryView categoryView;
   @BindView(2131821020)
   TextView confirmationTextView;

   public static Intent a(Context var0, co.uk.getmondo.payments.send.data.a.f var1) {
      return (new Intent(var0, PaymentCategoryActivity.class)).putExtra("KEY_PAYMENT", var1);
   }

   // $FF: synthetic method
   static void a(PaymentCategoryActivity var0) throws Exception {
      var0.categoryView.setCategoryClickListener((CategoryView.a)null);
   }

   // $FF: synthetic method
   static void a(PaymentCategoryActivity var0, io.reactivex.o var1) throws Exception {
      CategoryView var2 = var0.categoryView;
      var1.getClass();
      var2.setCategoryClickListener(i.a(var1));
      var1.a(j.a(var0));
   }

   public io.reactivex.n a() {
      return io.reactivex.n.create(h.a(this));
   }

   public void a(co.uk.getmondo.d.c var1) {
      this.amountView.setAmount(var1);
   }

   public void a(String var1) {
      this.confirmationTextView.setText(this.getString(2131362635, new Object[]{var1}));
   }

   public void b() {
      HomeActivity.a((Context)this);
   }

   public void onBackPressed() {
      super.onBackPressed();
      this.b();
   }

   protected void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.setContentView(2131034193);
      ButterKnife.bind((Activity)this);
      co.uk.getmondo.payments.send.data.a.f var2 = (co.uk.getmondo.payments.send.data.a.f)this.getIntent().getParcelableExtra("KEY_PAYMENT");
      this.l().a(new m(var2)).a(this);
      this.a.a((o.a)this);
   }

   protected void onDestroy() {
      this.a.b();
      super.onDestroy();
   }
}
