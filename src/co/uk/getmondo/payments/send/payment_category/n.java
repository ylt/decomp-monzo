package co.uk.getmondo.payments.send.payment_category;

public final class n implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final m b;

   static {
      boolean var0;
      if(!n.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   public n(m var1) {
      if(!a && var1 == null) {
         throw new AssertionError();
      } else {
         this.b = var1;
      }
   }

   public static b.a.b a(m var0) {
      return new n(var0);
   }

   public co.uk.getmondo.payments.send.data.a.f a() {
      return (co.uk.getmondo.payments.send.data.a.f)b.a.d.a(this.b.a(), "Cannot return null from a non-@Nullable @Provides method");
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
