package co.uk.getmondo.payments.send.payment_category;

import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import co.uk.getmondo.common.ui.AmountView;

public class PaymentCategoryActivity_ViewBinding implements Unbinder {
   private PaymentCategoryActivity a;

   public PaymentCategoryActivity_ViewBinding(PaymentCategoryActivity var1, View var2) {
      this.a = var1;
      var1.amountView = (AmountView)Utils.findRequiredViewAsType(var2, 2131821019, "field 'amountView'", AmountView.class);
      var1.categoryView = (CategoryView)Utils.findRequiredViewAsType(var2, 2131821022, "field 'categoryView'", CategoryView.class);
      var1.confirmationTextView = (TextView)Utils.findRequiredViewAsType(var2, 2131821020, "field 'confirmationTextView'", TextView.class);
   }

   public void unbind() {
      PaymentCategoryActivity var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.amountView = null;
         var1.categoryView = null;
         var1.confirmationTextView = null;
      }
   }
}
