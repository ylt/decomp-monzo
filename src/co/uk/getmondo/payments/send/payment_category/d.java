package co.uk.getmondo.payments.send.payment_category;

import co.uk.getmondo.api.model.feed.ApiFeedItem;

// $FF: synthetic class
final class d implements io.reactivex.c.g {
   private final b a;

   private d(b var1) {
      this.a = var1;
   }

   public static io.reactivex.c.g a(b var0) {
      return new d(var0);
   }

   public void a(Object var1) {
      b.b(this.a, (ApiFeedItem)var1);
   }
}
