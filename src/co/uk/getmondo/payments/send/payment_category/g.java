package co.uk.getmondo.payments.send.payment_category;

import android.content.Context;

public final class g implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final javax.a.a b;

   static {
      boolean var0;
      if(!g.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   public g(javax.a.a var1) {
      if(!a && var1 == null) {
         throw new AssertionError();
      } else {
         this.b = var1;
      }
   }

   public static b.a.b a(javax.a.a var0) {
      return new g(var0);
   }

   public b.a a() {
      return new b.a((Context)this.b.b());
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
