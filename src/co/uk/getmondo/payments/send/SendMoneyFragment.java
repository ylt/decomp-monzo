package co.uk.getmondo.payments.send;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.d.aa;
import co.uk.getmondo.payments.send.bank.BankPaymentActivity;
import co.uk.getmondo.payments.send.bank.payment.BankPaymentDetailsActivity;
import co.uk.getmondo.payments.send.peer.PeerPaymentActivity;
import java.util.Collection;

public class SendMoneyFragment extends co.uk.getmondo.common.f.a implements o.a {
   o a;
   co.uk.getmondo.common.a c;
   @BindView(2131821361)
   ProgressBar contactsProgressBar;
   @BindView(2131821363)
   RecyclerView contactsRecyclerView;
   SendMoneyAdapter d;
   private final com.b.b.c e = com.b.b.c.a();
   private final com.b.b.c f = com.b.b.c.a();
   private final com.b.b.c g = com.b.b.c.a();
   private final com.b.b.c h = com.b.b.c.a();
   private FloatingActionButton i;
   private Unbinder j;
   private boolean k = false;
   @BindView(2131821362)
   SwipeRefreshLayout swipeRefreshLayout;

   public static Fragment a() {
      return new SendMoneyFragment();
   }

   // $FF: synthetic method
   static void a(SendMoneyFragment var0) throws Exception {
      var0.swipeRefreshLayout.setOnRefreshListener((android.support.v4.widget.SwipeRefreshLayout.b)null);
   }

   // $FF: synthetic method
   static void a(SendMoneyFragment var0, co.uk.getmondo.payments.send.a.a var1, boolean var2) {
      if(var2) {
         var0.f.a((Object)((co.uk.getmondo.payments.send.a.b)var1));
      } else {
         var0.g.a((Object)var1.b());
      }

   }

   // $FF: synthetic method
   static void a(SendMoneyFragment var0, io.reactivex.o var1) throws Exception {
      var0.swipeRefreshLayout.setOnRefreshListener(l.a(var1));
      var1.a(m.a(var0));
   }

   // $FF: synthetic method
   static void a(io.reactivex.o var0) {
      var0.a(co.uk.getmondo.common.b.a.a);
   }

   // $FF: synthetic method
   static void b(SendMoneyFragment var0) {
      var0.startActivity(BankPaymentActivity.a((Context)var0.getActivity()));
   }

   public void a(aa var1) {
      this.startActivity(PeerPaymentActivity.a((Context)this.getActivity(), (aa)var1, (Impression.PaymentFlowFrom)Impression.PaymentFlowFrom.FROM_CONTACT_DISCOVERY));
   }

   public void a(co.uk.getmondo.payments.send.data.a.b var1) {
      this.startActivity(BankPaymentDetailsActivity.a((Context)this.getActivity(), (co.uk.getmondo.payments.send.data.a.b)var1));
   }

   public void a(String var1) {
      var1 = this.getString(2131362646, new Object[]{var1});
      String var2 = this.getString(2131362645);
      this.startActivity(co.uk.getmondo.common.k.j.a(this.getActivity(), var1, var2, co.uk.getmondo.api.model.tracking.a.INVITE_CONTACT_DISCOVERY));
   }

   public void a(Collection var1) {
      this.d.a(var1);
   }

   public void a(Collection var1, Collection var2) {
      this.d.a(var1, var2);
   }

   public io.reactivex.n b() {
      return this.e;
   }

   public io.reactivex.n c() {
      return this.f;
   }

   public io.reactivex.n d() {
      return this.g;
   }

   public io.reactivex.n e() {
      return com.b.a.c.c.a(this.i);
   }

   public io.reactivex.n f() {
      return this.h;
   }

   public io.reactivex.n g() {
      return io.reactivex.n.create(k.a(this));
   }

   public void h() {
      this.d.a(true);
   }

   public boolean i() {
      boolean var1;
      if(android.support.v4.content.a.b(this.getActivity(), "android.permission.READ_CONTACTS") == 0) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public void j() {
      this.c.a(Impression.L());
      this.requestPermissions(new String[]{"android.permission.READ_CONTACTS"}, 0);
   }

   public void k() {
      this.swipeRefreshLayout.setEnabled(true);
   }

   public void l() {
      if(this.d.getItemCount() <= 1) {
         this.contactsProgressBar.setVisibility(0);
      } else {
         this.swipeRefreshLayout.setRefreshing(true);
      }

   }

   public void m() {
      this.contactsProgressBar.setVisibility(8);
      this.swipeRefreshLayout.setRefreshing(false);
   }

   public void n() {
      this.contactsProgressBar.setVisibility(0);
   }

   public void o() {
      this.contactsProgressBar.setVisibility(8);
   }

   public void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.B().a(this);
   }

   public View onCreateView(LayoutInflater var1, ViewGroup var2, Bundle var3) {
      return var1.inflate(2131034278, var2, false);
   }

   public void onDestroyView() {
      this.a.b();
      this.j.unbind();
      super.onDestroyView();
   }

   public void onRequestPermissionsResult(int var1, String[] var2, int[] var3) {
      boolean var5 = true;
      if(var1 == 0) {
         boolean var4;
         if(var3.length > 0 && var3[0] == 0) {
            this.e.a((Object)co.uk.getmondo.common.b.a.a);
            var4 = true;
         } else {
            var4 = false;
         }

         if(var4) {
            var5 = false;
         }

         this.k = var5;
         this.c.a(Impression.g(var4));
      } else {
         super.onRequestPermissionsResult(var1, var2, var3);
      }

   }

   public void onResume() {
      super.onResume();
      if(this.k) {
         co.uk.getmondo.common.d.a.a("", this.getString(2131362640), false).show(this.getChildFragmentManager(), "error_dialog");
         this.k = false;
      }

   }

   public void onViewCreated(View var1, Bundle var2) {
      super.onCreate(var2);
      this.j = ButterKnife.bind(this, (View)var1);
      this.i = (FloatingActionButton)this.getActivity().findViewById(2131821306);
      this.swipeRefreshLayout.setColorSchemeResources(new int[]{2131689558});
      this.swipeRefreshLayout.setEnabled(false);
      this.d.a(h.a(this));
      this.d.a(false);
      SendMoneyAdapter var4 = this.d;
      com.b.b.c var5 = this.h;
      var5.getClass();
      var4.a(i.a(var5));
      this.d.a(j.a(this));
      this.contactsRecyclerView.setHasFixedSize(true);
      this.contactsRecyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity()));
      this.contactsRecyclerView.setAdapter(this.d);
      int var3 = this.getResources().getDimensionPixelSize(2131427609);
      this.contactsRecyclerView.a(new co.uk.getmondo.common.ui.h(this.getActivity(), this.d, var3));
      this.contactsRecyclerView.a(new a.a.a.a.a.b(this.d));
      this.a.a((o.a)this);
   }

   public void p() {
      Intent var1 = new Intent("android.intent.action.INSERT");
      var1.setType("vnd.android.cursor.dir/raw_contact");
      var1.putExtra("finishActivityOnSaveCompleted", true);
      this.startActivity(var1);
   }
}
