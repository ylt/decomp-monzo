package co.uk.getmondo.payments.send;

// $FF: synthetic class
final class h implements SendMoneyAdapter.c {
   private final SendMoneyFragment a;

   private h(SendMoneyFragment var1) {
      this.a = var1;
   }

   public static SendMoneyAdapter.c a(SendMoneyFragment var0) {
      return new h(var0);
   }

   public void a(co.uk.getmondo.payments.send.a.a var1, boolean var2) {
      SendMoneyFragment.a(this.a, var1, var2);
   }
}
