package co.uk.getmondo.payments.send;

import android.view.View;
import android.view.View.OnClickListener;

// $FF: synthetic class
final class d implements OnClickListener {
   private final SendMoneyAdapter.a a;

   private d(SendMoneyAdapter.a var1) {
      this.a = var1;
   }

   public static OnClickListener a(SendMoneyAdapter.a var0) {
      return new d(var0);
   }

   public void onClick(View var1) {
      SendMoneyAdapter.b.a(this.a, var1);
   }
}
