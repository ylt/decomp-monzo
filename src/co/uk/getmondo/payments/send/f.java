package co.uk.getmondo.payments.send;

import android.view.View;
import android.view.View.OnClickListener;

// $FF: synthetic class
final class f implements OnClickListener {
   private final SendMoneyAdapter.RecentPayeeViewHolder a;
   private final SendMoneyAdapter.f b;

   private f(SendMoneyAdapter.RecentPayeeViewHolder var1, SendMoneyAdapter.f var2) {
      this.a = var1;
      this.b = var2;
   }

   public static OnClickListener a(SendMoneyAdapter.RecentPayeeViewHolder var0, SendMoneyAdapter.f var1) {
      return new f(var0, var1);
   }

   public void onClick(View var1) {
      SendMoneyAdapter.RecentPayeeViewHolder.a(this.a, this.b, var1);
   }
}
