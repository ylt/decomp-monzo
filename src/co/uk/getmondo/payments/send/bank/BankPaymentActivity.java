package co.uk.getmondo.payments.send.bank;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

public class BankPaymentActivity extends co.uk.getmondo.common.activities.b implements b.a {
   b a;

   public static Intent a(Context var0) {
      return new Intent(var0, BankPaymentActivity.class);
   }

   public void a(co.uk.getmondo.d.c var1) {
      android.support.v7.app.a var2 = this.getSupportActionBar();
      if(var2 != null) {
         var2.b(String.format(this.getString(2131362021), new Object[]{var1.toString()}));
      }

   }

   protected void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.setContentView(2131034146);
      this.l().a(this);
      this.a.a((b.a)this);
   }

   protected void onDestroy() {
      this.a.b();
      super.onDestroy();
   }
}
