package co.uk.getmondo.payments.send.bank.payment;

import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class IntervalRadioView_ViewBinding implements Unbinder {
   private IntervalRadioView a;

   public IntervalRadioView_ViewBinding(IntervalRadioView var1, View var2) {
      this.a = var1;
      var1.radioButton = (RadioButton)Utils.findRequiredViewAsType(var2, 2131821743, "field 'radioButton'", RadioButton.class);
      var1.descriptionTextView = (TextView)Utils.findRequiredViewAsType(var2, 2131821744, "field 'descriptionTextView'", TextView.class);
   }

   public void unbind() {
      IntervalRadioView var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.radioButton = null;
         var1.descriptionTextView = null;
      }
   }
}
