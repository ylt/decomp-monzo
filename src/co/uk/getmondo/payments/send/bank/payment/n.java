package co.uk.getmondo.payments.send.bank.payment;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

// $FF: synthetic class
final class n implements OnClickListener {
   private final m a;

   private n(m var1) {
      this.a = var1;
   }

   public static OnClickListener a(m var0) {
      return new n(var0);
   }

   public void onClick(DialogInterface var1, int var2) {
      m.a(this.a, var1, var2);
   }
}
