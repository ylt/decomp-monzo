package co.uk.getmondo.payments.send.bank.payment;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnShowListener;

// $FF: synthetic class
final class o implements OnShowListener {
   private final DatePickerDialog a;

   private o(DatePickerDialog var1) {
      this.a = var1;
   }

   public static OnShowListener a(DatePickerDialog var0) {
      return new o(var0);
   }

   public void onShow(DialogInterface var1) {
      m.a(this.a, var1);
   }
}
