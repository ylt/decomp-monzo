package co.uk.getmondo.payments.send.bank.payment;

import io.reactivex.u;
import kotlin.Metadata;
import kotlin.TypeCastException;
import org.threeten.bp.LocalDate;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u001fB;\b\u0007\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\f\u001a\u00020\r¢\u0006\u0002\u0010\u000eJ\u0012\u0010\u0011\u001a\u00020\u00122\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014H\u0002J\u0010\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0002H\u0016J\u0018\u0010\u0018\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00022\u0006\u0010\u0019\u001a\u00020\u001aH\u0002J \u0010\u001b\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00022\u0006\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u001dH\u0002R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006 "},
   d2 = {"Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsPresenter;", "Lco/uk/getmondo/common/ui/BasePresenter;", "Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsPresenter$View;", "uiScheduler", "Lio/reactivex/Scheduler;", "ioScheduler", "balanceRepository", "Lco/uk/getmondo/account_balance/BalanceRepository;", "accountService", "Lco/uk/getmondo/common/accounts/AccountService;", "apiErrorHandler", "Lco/uk/getmondo/common/errors/ApiErrorHandler;", "bankPayee", "Lco/uk/getmondo/payments/send/data/model/BankPayee;", "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/account_balance/BalanceRepository;Lco/uk/getmondo/common/accounts/AccountService;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/payments/send/data/model/BankPayee;)V", "referenceRegex", "Lkotlin/text/Regex;", "isValidBankPaymentAmount", "", "amount", "Lco/uk/getmondo/model/Amount;", "register", "", "view", "validateFinal", "formData", "Lco/uk/getmondo/payments/send/bank/payment/PaymentDetailsFormData;", "validateWhileTyping", "amountString", "", "reference", "View", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class k extends co.uk.getmondo.common.ui.b {
   private final kotlin.h.g c;
   private final u d;
   private final u e;
   private final co.uk.getmondo.a.a f;
   private final co.uk.getmondo.common.accounts.d g;
   private final co.uk.getmondo.common.e.a h;
   private final co.uk.getmondo.payments.send.data.a.b i;

   public k(u var1, u var2, co.uk.getmondo.a.a var3, co.uk.getmondo.common.accounts.d var4, co.uk.getmondo.common.e.a var5, co.uk.getmondo.payments.send.data.a.b var6) {
      kotlin.d.b.l.b(var1, "uiScheduler");
      kotlin.d.b.l.b(var2, "ioScheduler");
      kotlin.d.b.l.b(var3, "balanceRepository");
      kotlin.d.b.l.b(var4, "accountService");
      kotlin.d.b.l.b(var5, "apiErrorHandler");
      kotlin.d.b.l.b(var6, "bankPayee");
      super();
      this.d = var1;
      this.e = var2;
      this.f = var3;
      this.g = var4;
      this.h = var5;
      this.i = var6;
      this.c = new kotlin.h.g("^[a-zA-Z0-9 /?:().,+#=!\"%&*<>;{}@_\\-]+$");
   }

   private final void a(k.a var1, s var2) {
      co.uk.getmondo.d.c.a var5 = co.uk.getmondo.d.c.Companion;
      String var3 = var2.a();
      co.uk.getmondo.common.i.c var4 = co.uk.getmondo.common.i.c.a;
      kotlin.d.b.l.a(var4, "Currency.GBP");
      co.uk.getmondo.d.c var8 = var5.a(var3, var4);
      if(!this.a(var8)) {
         d.a.a.a((Throwable)(new RuntimeException("Balance string is not a valid number")));
      } else {
         co.uk.getmondo.payments.send.data.a.c var6;
         if(var2.c()) {
            LocalDate var7;
            if(var2.e() != co.uk.getmondo.payments.a.a.d.c.a) {
               var7 = var2.f();
            } else {
               var7 = null;
            }

            if(var8 == null) {
               kotlin.d.b.l.a();
            }

            var6 = new co.uk.getmondo.payments.send.data.a.c(var8, var2.b(), this.i, new co.uk.getmondo.payments.a.a.d(var2.d(), var2.e(), var7, (LocalDate)null, 8, (kotlin.d.b.i)null));
         } else {
            if(var8 == null) {
               kotlin.d.b.l.a();
            }

            var6 = new co.uk.getmondo.payments.send.data.a.c(var8, var2.b(), this.i, (co.uk.getmondo.payments.a.a.d)null, 8, (kotlin.d.b.i)null);
         }

         var1.a(var6);
      }

   }

   private final void a(k.a var1, String var2, String var3) {
      co.uk.getmondo.d.c.a var7 = co.uk.getmondo.d.c.Companion;
      co.uk.getmondo.common.i.c var6 = co.uk.getmondo.common.i.c.a;
      kotlin.d.b.l.a(var6, "Currency.GBP");
      co.uk.getmondo.d.c var9 = var7.a(var2, var6);
      kotlin.h.g var8 = this.c;
      if(var3 == null) {
         throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
      } else {
         boolean var5;
         label29: {
            var5 = var8.a((CharSequence)kotlin.h.j.b((CharSequence)var3).toString());
            if(!var5) {
               boolean var4;
               if(((CharSequence)var3).length() == 0) {
                  var4 = true;
               } else {
                  var4 = false;
               }

               if(!var4) {
                  var1.k();
                  break label29;
               }
            }

            var1.v();
         }

         if(this.a(var9) && var5) {
            var5 = true;
         } else {
            var5 = false;
         }

         var1.b(var5);
      }
   }

   private final boolean a(co.uk.getmondo.d.c var1) {
      boolean var2;
      if(var1 != null && var1.k() > 0L) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   public void a(final k.a var1) {
      kotlin.d.b.l.b(var1, "view");
      super.a((co.uk.getmondo.common.ui.f)var1);
      var1.a(this.i.b());
      io.reactivex.b.a var3 = this.b;
      io.reactivex.b.b var2 = this.g.d().c((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.b a(String var1) {
            kotlin.d.b.l.b(var1, "it");
            return k.this.f.b(var1);
         }
      })).b(this.e).a(this.d).a((io.reactivex.c.a)null.a, (io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Throwable var1x) {
            co.uk.getmondo.common.e.a var2 = k.this.h;
            kotlin.d.b.l.a(var1x, "error");
            var2.a(var1x, (co.uk.getmondo.common.e.a.a)var1);
         }
      }));
      kotlin.d.b.l.a(var2, "accountService.accountId…ndleError(error, view) })");
      this.b = co.uk.getmondo.common.j.f.a(var3, var2);
      io.reactivex.n var6 = this.g.d().b((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.n a(String var1) {
            kotlin.d.b.l.b(var1, "it");
            return k.this.f.a(var1);
         }
      })).map((io.reactivex.c.h)null.a).distinctUntilChanged();
      io.reactivex.b.a var5 = this.b;
      io.reactivex.b.b var7 = var6.observeOn(this.d).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(co.uk.getmondo.d.c var1x) {
            k.a var2 = var1;
            kotlin.d.b.l.a(var1x, "it");
            var2.a(var1x);
         }
      }), (io.reactivex.c.g)null.a);
      kotlin.d.b.l.a(var7, "balance.observeOn(uiSche…(it) }, { Timber.e(it) })");
      this.b = co.uk.getmondo.common.j.f.a(var5, var7);
      var3 = this.b;
      var2 = co.uk.getmondo.common.j.f.a(var1.b(), var1.a()).observeOn(this.d).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(kotlin.h var1x) {
            String var2 = (String)var1x.c();
            String var3 = (String)var1x.d();
            k.this.a(var1, var2, var3);
         }
      }));
      kotlin.d.b.l.a(var2, "combineLatest(view.onAmo…ng(view, first, second) }");
      this.b = co.uk.getmondo.common.j.f.a(var3, var2);
      var5 = this.b;
      var7 = var1.c().observeOn(this.d).subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(s var1x) {
            k var2 = k.this;
            k.a var3 = var1;
            kotlin.d.b.l.a(var1x, "formData");
            var2.a(var3, var1x);
         }
      }));
      kotlin.d.b.l.a(var7, "view.onNextStepClicked()…teFinal(view, formData) }");
      this.b = co.uk.getmondo.common.j.f.a(var5, var7);
      var3 = this.b;
      var2 = var1.d().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(Boolean var1x) {
            k.a var2 = var1;
            kotlin.d.b.l.a(var1x, "it");
            var2.c(var1x.booleanValue());
         }
      }));
      kotlin.d.b.l.a(var2, "view.onScheduleToggled()…eduleOptionsVisible(it) }");
      this.b = co.uk.getmondo.common.j.f.a(var3, var2);
      var5 = this.b;
      var7 = var1.e().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(LocalDate var1x) {
            k.a var2 = var1;
            kotlin.d.b.l.a(var1x, "it");
            var2.b(var1x);
         }
      }));
      kotlin.d.b.l.a(var7, "view.onStartDateClicked(…openStartDatePicker(it) }");
      this.b = co.uk.getmondo.common.j.f.a(var5, var7);
      var3 = this.b;
      var2 = var1.f().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(LocalDate var1x) {
            k.a var2 = var1;
            kotlin.d.b.l.a(var1x, "it");
            var2.a(var1x);
         }
      }));
      kotlin.d.b.l.a(var2, "view.onStartDateSelected…owSelectedStartDate(it) }");
      this.b = co.uk.getmondo.common.j.f.a(var3, var2);
      var3 = this.b;
      var2 = var1.g().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(co.uk.getmondo.payments.a.a.d.c var1x) {
            k.a var2 = var1;
            kotlin.d.b.l.a(var1x, "it");
            var2.b(var1x);
         }
      }));
      kotlin.d.b.l.a(var2, "view.onIntervalClicked()….openIntervalPicker(it) }");
      this.b = co.uk.getmondo.common.j.f.a(var3, var2);
      var3 = this.b;
      var2 = var1.h().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(co.uk.getmondo.payments.a.a.d.c var1x) {
            k.a var3 = var1;
            boolean var2;
            if(var1x != co.uk.getmondo.payments.a.a.d.c.a) {
               var2 = true;
            } else {
               var2 = false;
            }

            var3.a(var2);
            var3 = var1;
            kotlin.d.b.l.a(var1x, "interval");
            var3.c(var1x);
         }
      }));
      kotlin.d.b.l.a(var2, "view.onIntervalSelected(…terval)\n                }");
      this.b = co.uk.getmondo.common.j.f.a(var3, var2);
      var5 = this.b;
      var7 = var1.i().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(com.c.b.b var1x) {
            k.a var2 = var1;
            kotlin.d.b.l.a(var1x, "it");
            var2.a(var1x);
         }
      }));
      kotlin.d.b.l.a(var7, "view.onEndDateClicked()\n…w.openEndDatePicker(it) }");
      this.b = co.uk.getmondo.common.j.f.a(var5, var7);
      var5 = this.b;
      io.reactivex.b.b var4 = var1.j().subscribe((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(com.c.b.b var1x) {
            k.a var2 = var1;
            kotlin.d.b.l.a(var1x, "it");
            var2.b(var1x);
         }
      }));
      kotlin.d.b.l.a(var4, "view.onEndDateSelected()…showSelectedEndDate(it) }");
      this.b = co.uk.getmondo.common.j.f.a(var5, var4);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\n\b`\u0018\u00002\u00020\u00012\u00020\u0002J\b\u0010\u0003\u001a\u00020\u0004H&J\u000e\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006H&J\u0014\u0010\b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\u0006H&J\u0014\u0010\u000b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\u0006H&J\u000e\u0010\f\u001a\b\u0012\u0004\u0012\u00020\r0\u0006H&J\u000e\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\r0\u0006H&J\u000e\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00100\u0006H&J\u000e\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006H&J\u000e\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00130\u0006H&J\u000e\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\n0\u0006H&J\u000e\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\n0\u0006H&J\u0016\u0010\u0016\u001a\u00020\u00042\f\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\n0\tH&J\u0010\u0010\u0018\u001a\u00020\u00042\u0006\u0010\u0019\u001a\u00020\rH&J\u0010\u0010\u001a\u001a\u00020\u00042\u0006\u0010\u001b\u001a\u00020\u001cH&J\u0010\u0010\u001d\u001a\u00020\u00042\u0006\u0010\u001e\u001a\u00020\nH&J\u0010\u0010\u001f\u001a\u00020\u00042\u0006\u0010 \u001a\u00020\u0013H&J\u0010\u0010!\u001a\u00020\u00042\u0006\u0010\"\u001a\u00020\u0013H&J\u0010\u0010#\u001a\u00020\u00042\u0006\u0010 \u001a\u00020\u0013H&J\u0010\u0010$\u001a\u00020\u00042\u0006\u0010%\u001a\u00020&H&J\b\u0010'\u001a\u00020\u0004H&J\u0010\u0010(\u001a\u00020\u00042\u0006\u0010)\u001a\u00020\u0007H&J\u0016\u0010*\u001a\u00020\u00042\f\u0010+\u001a\b\u0012\u0004\u0012\u00020\n0\tH&J\u0010\u0010,\u001a\u00020\u00042\u0006\u0010-\u001a\u00020\rH&J\u0010\u0010.\u001a\u00020\u00042\u0006\u0010/\u001a\u00020\nH&¨\u00060"},
      d2 = {"Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsPresenter$View;", "Lco/uk/getmondo/common/ui/MvpView;", "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;", "clearInvalidReferenceError", "", "onAmountChanged", "Lio/reactivex/Observable;", "", "onEndDateClicked", "Lcom/memoizrlabs/poweroptional/Optional;", "Lorg/threeten/bp/LocalDate;", "onEndDateSelected", "onIntervalClicked", "Lco/uk/getmondo/payments/data/model/PaymentSchedule$Interval;", "onIntervalSelected", "onNextStepClicked", "Lco/uk/getmondo/payments/send/bank/payment/PaymentDetailsFormData;", "onReferenceChanged", "onScheduleToggled", "", "onStartDateClicked", "onStartDateSelected", "openEndDatePicker", "optionalDefaultDate", "openIntervalPicker", "defaultInterval", "openPaymentAuthentication", "bankPayment", "Lco/uk/getmondo/payments/send/data/model/BankPayment;", "openStartDatePicker", "defaultDate", "setEndDateVisible", "visible", "setNextStepButtonEnabled", "enabled", "setScheduleOptionsVisible", "showBalance", "balance", "Lco/uk/getmondo/model/Amount;", "showInvalidReference", "showRecipientName", "name", "showSelectedEndDate", "optionalEndDate", "showSelectedInterval", "interval", "showSelectedStartDate", "startDate", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      io.reactivex.n a();

      void a(co.uk.getmondo.d.c var1);

      void a(co.uk.getmondo.payments.send.data.a.c var1);

      void a(com.c.b.b var1);

      void a(String var1);

      void a(LocalDate var1);

      void a(boolean var1);

      io.reactivex.n b();

      void b(co.uk.getmondo.payments.a.a.d.c var1);

      void b(com.c.b.b var1);

      void b(LocalDate var1);

      void b(boolean var1);

      io.reactivex.n c();

      void c(co.uk.getmondo.payments.a.a.d.c var1);

      void c(boolean var1);

      io.reactivex.n d();

      io.reactivex.n e();

      io.reactivex.n f();

      io.reactivex.n g();

      io.reactivex.n h();

      io.reactivex.n i();

      io.reactivex.n j();

      void k();

      void v();
   }
}
