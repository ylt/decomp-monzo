package co.uk.getmondo.payments.send.bank.payment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.DatePicker;
import org.threeten.bp.Instant;
import org.threeten.bp.LocalDate;
import org.threeten.bp.LocalTime;
import org.threeten.bp.ZoneId;
import org.threeten.bp.ZonedDateTime;

public class m extends android.support.v4.app.i implements OnDateSetListener {
   private m.a a;
   private int b;

   public static m a(int var0, LocalDate var1, LocalDate var2, boolean var3) {
      Bundle var4 = new Bundle();
      var4.putInt("KEY_REQUEST_CODE", var0);
      var4.putSerializable("KEY_DEFAULT_DATE", var1);
      var4.putSerializable("KEY_MIN_DATE", var2);
      var4.putBoolean("KEY_CAN_REMOVE_DATE", var3);
      m var5 = new m();
      var5.setArguments(var4);
      return var5;
   }

   // $FF: synthetic method
   static void a(DatePickerDialog var0, DialogInterface var1) {
      var0.getButton(-3).setTextColor(-65536);
   }

   // $FF: synthetic method
   static void a(m var0, DialogInterface var1, int var2) {
      var0.a.a(var0.b);
   }

   public void onAttach(Context var1) {
      super.onAttach(var1);
      if(var1 instanceof m.a) {
         this.a = (m.a)var1;
      } else {
         throw new ClassCastException(var1.toString() + " must implement DateListener");
      }
   }

   public Dialog onCreateDialog(Bundle var1) {
      this.b = this.getArguments().getInt("KEY_REQUEST_CODE");
      LocalDate var3 = (LocalDate)this.getArguments().getSerializable("KEY_MIN_DATE");
      boolean var2 = this.getArguments().getBoolean("KEY_CAN_REMOVE_DATE", false);
      LocalDate var4 = (LocalDate)this.getArguments().get("KEY_DEFAULT_DATE");
      DatePickerDialog var5 = new DatePickerDialog(this.getActivity(), this, var4.d(), var4.e() - 1, var4.g());
      if(var2) {
         var5.setButton(-3, this.getString(2131362100), n.a(this));
         var5.setOnShowListener(o.a(var5));
      }

      Instant var6 = ZonedDateTime.a(var3, LocalTime.a(0, 0), ZoneId.a()).j();
      var5.getDatePicker().setMinDate(var6.c());
      return var5;
   }

   public void onDateSet(DatePicker var1, int var2, int var3, int var4) {
      this.a.a(LocalDate.a(var2, var3 + 1, var4), this.b);
   }

   interface a {
      void a(int var1);

      void a(LocalDate var1, int var2);
   }
}
