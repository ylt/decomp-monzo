package co.uk.getmondo.payments.send.bank.payment;

public final class j implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final i b;

   static {
      boolean var0;
      if(!j.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   public j(i var1) {
      if(!a && var1 == null) {
         throw new AssertionError();
      } else {
         this.b = var1;
      }
   }

   public static b.a.b a(i var0) {
      return new j(var0);
   }

   public co.uk.getmondo.payments.send.data.a.b a() {
      return (co.uk.getmondo.payments.send.data.a.b)b.a.d.a(this.b.a(), "Cannot return null from a non-@Nullable @Provides method");
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
