package co.uk.getmondo.payments.send.bank.payment;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;

// $FF: synthetic class
final class q implements OnClickListener {
   private final p a;
   private final LinearLayout b;

   private q(p var1, LinearLayout var2) {
      this.a = var1;
      this.b = var2;
   }

   public static OnClickListener a(p var0, LinearLayout var1) {
      return new q(var0, var1);
   }

   public void onClick(View var1) {
      p.a(this.a, this.b, var1);
   }
}
