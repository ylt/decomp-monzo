package co.uk.getmondo.payments.send.bank.payment;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

// $FF: synthetic class
final class r implements OnClickListener {
   private final p a;

   private r(p var1) {
      this.a = var1;
   }

   public static OnClickListener a(p var0) {
      return new r(var0);
   }

   public void onClick(DialogInterface var1, int var2) {
      p.a(this.a, var1, var2);
   }
}
