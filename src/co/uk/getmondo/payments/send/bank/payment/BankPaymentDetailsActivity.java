package co.uk.getmondo.payments.send.bank.payment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.text.SpannableString;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.uk.getmondo.common.ui.AmountInputView;
import co.uk.getmondo.payments.send.authentication.PaymentAuthenticationActivity;
import java.util.Locale;
import org.threeten.bp.LocalDate;
import org.threeten.bp.chrono.ChronoLocalDate;
import org.threeten.bp.format.DateTimeFormatter;

public class BankPaymentDetailsActivity extends co.uk.getmondo.common.activities.b implements k.a, m.a, p.a {
   k a;
   @BindView(2131820817)
   AmountInputView amountInputView;
   private final com.b.b.b b = com.b.b.b.b((Object)LocalDate.a());
   private final com.b.b.b c = com.b.b.b.b((Object)com.c.b.b.c());
   private final com.b.b.b e;
   private final DateTimeFormatter f;
   @BindView(2131820827)
   Button nextStepButton;
   @BindView(2131820819)
   EditText referenceEditText;
   @BindView(2131820818)
   TextInputLayout referenceInputLayout;
   @BindView(2131820825)
   ViewGroup schedulePaymentEndRow;
   @BindView(2131820826)
   TextView schedulePaymentEndTextView;
   @BindView(2131820823)
   ViewGroup schedulePaymentIntervalRow;
   @BindView(2131820824)
   TextView schedulePaymentIntervalTextView;
   @BindView(2131820821)
   ViewGroup schedulePaymentStartRow;
   @BindView(2131820822)
   TextView schedulePaymentStartTextView;
   @BindView(2131820820)
   Switch schedulePaymentSwitch;

   public BankPaymentDetailsActivity() {
      this.e = com.b.b.b.b((Object)co.uk.getmondo.payments.a.a.d.c.a);
      this.f = DateTimeFormatter.a("EEE, LLL d", Locale.ENGLISH);
   }

   public static Intent a(Context var0, co.uk.getmondo.payments.send.data.a.b var1) {
      return (new Intent(var0, BankPaymentDetailsActivity.class)).putExtra("KEY_RECIPIENT_BANK_ACCOUNT", var1);
   }

   // $FF: synthetic method
   static com.c.b.b a(BankPaymentDetailsActivity var0, Object var1) throws Exception {
      return (com.c.b.b)var0.c.b();
   }

   // $FF: synthetic method
   static co.uk.getmondo.payments.a.a.d.c b(BankPaymentDetailsActivity var0, Object var1) throws Exception {
      return (co.uk.getmondo.payments.a.a.d.c)var0.e.b();
   }

   // $FF: synthetic method
   static LocalDate c(BankPaymentDetailsActivity var0, Object var1) throws Exception {
      return (LocalDate)var0.b.b();
   }

   // $FF: synthetic method
   static s d(BankPaymentDetailsActivity var0, Object var1) throws Exception {
      return new s(var0.amountInputView.getAmountText().toString(), var0.referenceEditText.getText().toString(), var0.schedulePaymentSwitch.isChecked(), (LocalDate)var0.b.b(), (co.uk.getmondo.payments.a.a.d.c)var0.e.b(), (LocalDate)((com.c.b.b)var0.c.b()).a((Object)null));
   }

   private void w() {
      String var5 = this.getString(2131362621);
      int var1 = var5.indexOf(10);
      int var3 = var5.length();
      int var4 = android.support.v4.content.a.c(this, 2131689526);
      int var2 = android.support.v4.content.a.c(this, 2131689527);
      SpannableString var6 = new SpannableString(var5);
      var6.setSpan(new ForegroundColorSpan(var4), 0, var1, 17);
      var6.setSpan(new AbsoluteSizeSpan(co.uk.getmondo.common.k.n.b(12)), var1, var3, 34);
      var6.setSpan(new ForegroundColorSpan(var2), var1, var3, 34);
      this.schedulePaymentSwitch.setText(var6);
   }

   public io.reactivex.n a() {
      return com.b.a.d.e.c(this.referenceEditText).map(a.a());
   }

   public void a(int var1) {
      if(var1 == 101) {
         this.c.a((Object)com.c.b.b.c());
      }

   }

   public void a(co.uk.getmondo.d.c var1) {
      android.support.v7.app.a var2 = this.getSupportActionBar();
      if(var2 != null) {
         var2.b(String.format(this.getString(2131362021), new Object[]{var1.toString()}));
      }

   }

   public void a(co.uk.getmondo.payments.a.a.d.c var1) {
      this.e.a((Object)var1);
   }

   public void a(co.uk.getmondo.payments.send.data.a.c var1) {
      this.startActivity(PaymentAuthenticationActivity.a((Context)this, (co.uk.getmondo.payments.send.data.a.f)var1));
   }

   public void a(com.c.b.b var1) {
      LocalDate var2 = ((LocalDate)this.b.b()).e(1L);
      m.a(101, (LocalDate)var1.a((Object)var2), var2, var1.b()).show(this.getSupportFragmentManager(), "DATE_PICKER_FRAGMENT_TAG");
   }

   public void a(String var1) {
      this.setTitle(var1);
   }

   public void a(LocalDate var1) {
      String var2;
      if(var1.d((ChronoLocalDate)LocalDate.a())) {
         var2 = this.getString(2131362623);
      } else {
         var2 = var1.a(this.f);
      }

      this.schedulePaymentStartTextView.setText(var2);
   }

   public void a(LocalDate var1, int var2) {
      switch(var2) {
      case 100:
         this.b.a((Object)var1);
         break;
      case 101:
         this.c.a((Object)com.c.b.b.b(var1));
      }

   }

   public void a(boolean var1) {
      ViewGroup var3 = this.schedulePaymentEndRow;
      byte var2;
      if(var1) {
         var2 = 0;
      } else {
         var2 = 8;
      }

      var3.setVisibility(var2);
   }

   public io.reactivex.n b() {
      return this.amountInputView.a().map(b.a());
   }

   public void b(co.uk.getmondo.payments.a.a.d.c var1) {
      p.a(var1).show(this.getSupportFragmentManager(), "INTERVAL_PICKER_FRAGMENT_TAG");
   }

   public void b(com.c.b.b var1) {
      if(var1.b()) {
         this.schedulePaymentEndTextView.setText(((LocalDate)var1.a()).a(this.f));
      } else {
         this.schedulePaymentEndTextView.setText(2131362619);
      }

   }

   public void b(LocalDate var1) {
      m.a(100, var1, LocalDate.a(), false).show(this.getSupportFragmentManager(), "DATE_PICKER_FRAGMENT_TAG");
   }

   public void b(boolean var1) {
      this.nextStepButton.setEnabled(var1);
   }

   public io.reactivex.n c() {
      return com.b.a.c.c.a(this.nextStepButton).map(c.a(this));
   }

   public void c(co.uk.getmondo.payments.a.a.d.c var1) {
      this.schedulePaymentIntervalTextView.setText(var1.c());
   }

   public void c(boolean var1) {
      byte var2;
      if(var1) {
         var2 = 0;
      } else {
         var2 = 8;
      }

      this.schedulePaymentIntervalRow.setVisibility(var2);
      this.schedulePaymentStartRow.setVisibility(var2);
      if(var1 && this.e.b() != co.uk.getmondo.payments.a.a.d.c.a) {
         this.schedulePaymentEndRow.setVisibility(0);
      } else if(!var1) {
         this.schedulePaymentEndRow.setVisibility(8);
      }

      if(var1) {
         this.a((View)this.schedulePaymentSwitch);
      }

   }

   public io.reactivex.n d() {
      return com.b.a.d.d.a(this.schedulePaymentSwitch);
   }

   public io.reactivex.n e() {
      return com.b.a.c.c.a(this.schedulePaymentStartRow).map(d.a(this));
   }

   public io.reactivex.n f() {
      return this.b;
   }

   public io.reactivex.n g() {
      return com.b.a.c.c.a(this.schedulePaymentIntervalRow).map(e.a(this));
   }

   public io.reactivex.n h() {
      return this.e;
   }

   public io.reactivex.n i() {
      return com.b.a.c.c.a(this.schedulePaymentEndRow).map(f.a(this));
   }

   public io.reactivex.n j() {
      return this.c;
   }

   public void k() {
      this.referenceInputLayout.setError(this.getString(2131362027));
   }

   protected void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.setContentView(2131034147);
      ButterKnife.bind((Activity)this);
      co.uk.getmondo.payments.send.data.a.b var2 = (co.uk.getmondo.payments.send.data.a.b)this.getIntent().getParcelableExtra("KEY_RECIPIENT_BANK_ACCOUNT");
      if(var2 == null) {
         throw new RuntimeException("This activity requires a Payee");
      } else {
         this.l().a(new i(var2)).a(this);
         this.a.a((k.a)this);
         this.w();
      }
   }

   protected void onDestroy() {
      this.a.b();
      super.onDestroy();
   }

   public void v() {
      this.referenceInputLayout.setError("");
   }
}
