package co.uk.getmondo.payments.send.bank.payment;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Checkable;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class p extends android.support.v4.app.i {
   private p.a a;
   private co.uk.getmondo.payments.a.a.d.c b;

   public p() {
      this.b = co.uk.getmondo.payments.a.a.d.c.a;
   }

   public static p a(co.uk.getmondo.payments.a.a.d.c var0) {
      Bundle var1 = new Bundle();
      var1.putSerializable("KEY_DEFAULT_INTERVAL", var0);
      p var2 = new p();
      var2.setArguments(var1);
      return var2;
   }

   // $FF: synthetic method
   static void a(p var0, DialogInterface var1, int var2) {
      var0.a.a(var0.b);
   }

   // $FF: synthetic method
   static void a(p var0, LinearLayout var1, View var2) {
      var0.b = (co.uk.getmondo.payments.a.a.d.c)var2.getTag();

      for(int var3 = 0; var3 < var1.getChildCount(); ++var3) {
         Checkable var4 = (Checkable)var1.getChildAt(var3);
         var4.setChecked(var4.equals(var2));
      }

   }

   public void onAttach(Context var1) {
      super.onAttach(var1);
      if(var1 instanceof p.a) {
         this.a = (p.a)var1;
      } else {
         throw new ClassCastException(var1.toString() + " must implement IntervalSelectedListener");
      }
   }

   public void onCreate(Bundle var1) {
      super.onCreate(var1);
      if(this.getArguments() != null && this.getArguments().containsKey("KEY_DEFAULT_INTERVAL")) {
         this.b = (co.uk.getmondo.payments.a.a.d.c)this.getArguments().getSerializable("KEY_DEFAULT_INTERVAL");
      }

   }

   public Dialog onCreateDialog(Bundle var1) {
      int var4 = this.getResources().getDimensionPixelSize(2131427603);
      int var5 = this.getResources().getDimensionPixelSize(2131427605);
      LinearLayout var10 = new LinearLayout(this.getActivity());
      var10.setOrientation(1);
      var10.setPadding(var4, var4, var4, var5);
      OnClickListener var8 = q.a(this, var10);
      co.uk.getmondo.payments.a.a.d.c[] var7 = co.uk.getmondo.payments.a.a.d.c.values();
      int var3 = var7.length;

      for(int var2 = 0; var2 < var3; ++var2) {
         co.uk.getmondo.payments.a.a.d.c var6 = var7[var2];
         IntervalRadioView var9 = new IntervalRadioView(this.getActivity());
         var9.setPaddingRelative(var4, var5, var4, var5);
         var9.setInterval(var6);
         var9.setTag(var6);
         var9.setOnClickListener(var8);
         var10.addView(var9);
         if(var6 == this.b) {
            var9.setChecked(true);
         }
      }

      ScrollView var11 = new ScrollView(this.getActivity());
      var11.addView(var10);
      return (new android.support.v7.app.d.a(this.getActivity())).a(2131362292).b(var11).a(2131362291, r.a(this)).b();
   }

   @FunctionalInterface
   interface a {
      void a(co.uk.getmondo.payments.a.a.d.c var1);
   }
}
