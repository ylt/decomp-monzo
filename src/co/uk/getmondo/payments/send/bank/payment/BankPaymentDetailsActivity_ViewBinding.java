package co.uk.getmondo.payments.send.bank.payment;

import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import co.uk.getmondo.common.ui.AmountInputView;

public class BankPaymentDetailsActivity_ViewBinding implements Unbinder {
   private BankPaymentDetailsActivity a;

   public BankPaymentDetailsActivity_ViewBinding(BankPaymentDetailsActivity var1, View var2) {
      this.a = var1;
      var1.amountInputView = (AmountInputView)Utils.findRequiredViewAsType(var2, 2131820817, "field 'amountInputView'", AmountInputView.class);
      var1.referenceInputLayout = (TextInputLayout)Utils.findRequiredViewAsType(var2, 2131820818, "field 'referenceInputLayout'", TextInputLayout.class);
      var1.referenceEditText = (EditText)Utils.findRequiredViewAsType(var2, 2131820819, "field 'referenceEditText'", EditText.class);
      var1.nextStepButton = (Button)Utils.findRequiredViewAsType(var2, 2131820827, "field 'nextStepButton'", Button.class);
      var1.schedulePaymentSwitch = (Switch)Utils.findRequiredViewAsType(var2, 2131820820, "field 'schedulePaymentSwitch'", Switch.class);
      var1.schedulePaymentIntervalRow = (ViewGroup)Utils.findRequiredViewAsType(var2, 2131820823, "field 'schedulePaymentIntervalRow'", ViewGroup.class);
      var1.schedulePaymentStartRow = (ViewGroup)Utils.findRequiredViewAsType(var2, 2131820821, "field 'schedulePaymentStartRow'", ViewGroup.class);
      var1.schedulePaymentStartTextView = (TextView)Utils.findRequiredViewAsType(var2, 2131820822, "field 'schedulePaymentStartTextView'", TextView.class);
      var1.schedulePaymentIntervalTextView = (TextView)Utils.findRequiredViewAsType(var2, 2131820824, "field 'schedulePaymentIntervalTextView'", TextView.class);
      var1.schedulePaymentEndRow = (ViewGroup)Utils.findRequiredViewAsType(var2, 2131820825, "field 'schedulePaymentEndRow'", ViewGroup.class);
      var1.schedulePaymentEndTextView = (TextView)Utils.findRequiredViewAsType(var2, 2131820826, "field 'schedulePaymentEndTextView'", TextView.class);
   }

   public void unbind() {
      BankPaymentDetailsActivity var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.amountInputView = null;
         var1.referenceInputLayout = null;
         var1.referenceEditText = null;
         var1.nextStepButton = null;
         var1.schedulePaymentSwitch = null;
         var1.schedulePaymentIntervalRow = null;
         var1.schedulePaymentStartRow = null;
         var1.schedulePaymentStartTextView = null;
         var1.schedulePaymentIntervalTextView = null;
         var1.schedulePaymentEndRow = null;
         var1.schedulePaymentEndTextView = null;
      }
   }
}
