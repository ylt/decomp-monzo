package co.uk.getmondo.payments.send.bank.payment;

import kotlin.Metadata;
import org.threeten.bp.LocalDate;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0015\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B=\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\b\b\u0002\u0010\u0007\u001a\u00020\b\u0012\b\b\u0002\u0010\t\u001a\u00020\n\u0012\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\b¢\u0006\u0002\u0010\fJ\t\u0010\u0016\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0017\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0018\u001a\u00020\u0006HÆ\u0003J\t\u0010\u0019\u001a\u00020\bHÆ\u0003J\t\u0010\u001a\u001a\u00020\nHÆ\u0003J\u000b\u0010\u001b\u001a\u0004\u0018\u00010\bHÆ\u0003JG\u0010\u001c\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00062\b\b\u0002\u0010\u0007\u001a\u00020\b2\b\b\u0002\u0010\t\u001a\u00020\n2\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\bHÆ\u0001J\u0013\u0010\u001d\u001a\u00020\u00062\b\u0010\u001e\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u001f\u001a\u00020 HÖ\u0001J\t\u0010!\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0013\u0010\u000b\u001a\u0004\u0018\u00010\b¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0011\u0010\t\u001a\u00020\n¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0013R\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u000eR\u0011\u0010\u0007\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0010¨\u0006\""},
   d2 = {"Lco/uk/getmondo/payments/send/bank/payment/PaymentDetailsFormData;", "", "amountText", "", "reference", "isScheduleChecked", "", "startDate", "Lorg/threeten/bp/LocalDate;", "interval", "Lco/uk/getmondo/payments/data/model/PaymentSchedule$Interval;", "endDate", "(Ljava/lang/String;Ljava/lang/String;ZLorg/threeten/bp/LocalDate;Lco/uk/getmondo/payments/data/model/PaymentSchedule$Interval;Lorg/threeten/bp/LocalDate;)V", "getAmountText", "()Ljava/lang/String;", "getEndDate", "()Lorg/threeten/bp/LocalDate;", "getInterval", "()Lco/uk/getmondo/payments/data/model/PaymentSchedule$Interval;", "()Z", "getReference", "getStartDate", "component1", "component2", "component3", "component4", "component5", "component6", "copy", "equals", "other", "hashCode", "", "toString", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class s {
   private final String a;
   private final String b;
   private final boolean c;
   private final LocalDate d;
   private final co.uk.getmondo.payments.a.a.d.c e;
   private final LocalDate f;

   public s(String var1, String var2, boolean var3, LocalDate var4, co.uk.getmondo.payments.a.a.d.c var5, LocalDate var6) {
      kotlin.d.b.l.b(var1, "amountText");
      kotlin.d.b.l.b(var2, "reference");
      kotlin.d.b.l.b(var4, "startDate");
      kotlin.d.b.l.b(var5, "interval");
      super();
      this.a = var1;
      this.b = var2;
      this.c = var3;
      this.d = var4;
      this.e = var5;
      this.f = var6;
   }

   public final String a() {
      return this.a;
   }

   public final String b() {
      return this.b;
   }

   public final boolean c() {
      return this.c;
   }

   public final LocalDate d() {
      return this.d;
   }

   public final co.uk.getmondo.payments.a.a.d.c e() {
      return this.e;
   }

   public boolean equals(Object var1) {
      boolean var4 = false;
      boolean var3;
      if(this != var1) {
         var3 = var4;
         if(!(var1 instanceof s)) {
            return var3;
         }

         s var5 = (s)var1;
         var3 = var4;
         if(!kotlin.d.b.l.a(this.a, var5.a)) {
            return var3;
         }

         var3 = var4;
         if(!kotlin.d.b.l.a(this.b, var5.b)) {
            return var3;
         }

         boolean var2;
         if(this.c == var5.c) {
            var2 = true;
         } else {
            var2 = false;
         }

         var3 = var4;
         if(!var2) {
            return var3;
         }

         var3 = var4;
         if(!kotlin.d.b.l.a(this.d, var5.d)) {
            return var3;
         }

         var3 = var4;
         if(!kotlin.d.b.l.a(this.e, var5.e)) {
            return var3;
         }

         var3 = var4;
         if(!kotlin.d.b.l.a(this.f, var5.f)) {
            return var3;
         }
      }

      var3 = true;
      return var3;
   }

   public final LocalDate f() {
      return this.f;
   }

   public int hashCode() {
      throw new RuntimeException("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.copyTypes(TypeTransformer.java:311)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.fixTypes(TypeTransformer.java:226)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:207)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:161)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:433)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:129)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:528)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:425)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:441)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:171)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:271)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:109)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:290)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:33)\n");
   }

   public String toString() {
      return "PaymentDetailsFormData(amountText=" + this.a + ", reference=" + this.b + ", isScheduleChecked=" + this.c + ", startDate=" + this.d + ", interval=" + this.e + ", endDate=" + this.f + ")";
   }
}
