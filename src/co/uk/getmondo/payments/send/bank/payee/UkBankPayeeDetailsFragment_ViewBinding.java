package co.uk.getmondo.payments.send.bank.payee;

import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class UkBankPayeeDetailsFragment_ViewBinding implements Unbinder {
   private UkBankPayeeDetailsFragment a;

   public UkBankPayeeDetailsFragment_ViewBinding(UkBankPayeeDetailsFragment var1, View var2) {
      this.a = var1;
      var1.recipientInputLayout = (TextInputLayout)Utils.findRequiredViewAsType(var2, 2131821398, "field 'recipientInputLayout'", TextInputLayout.class);
      var1.recipientEditText = (EditText)Utils.findRequiredViewAsType(var2, 2131821399, "field 'recipientEditText'", EditText.class);
      var1.sortCodeInputLayout = (TextInputLayout)Utils.findRequiredViewAsType(var2, 2131821400, "field 'sortCodeInputLayout'", TextInputLayout.class);
      var1.sortCodeEditText = (EditText)Utils.findRequiredViewAsType(var2, 2131821401, "field 'sortCodeEditText'", EditText.class);
      var1.accountNumberInputLayout = (TextInputLayout)Utils.findRequiredViewAsType(var2, 2131821402, "field 'accountNumberInputLayout'", TextInputLayout.class);
      var1.accountNumberEditText = (EditText)Utils.findRequiredViewAsType(var2, 2131821403, "field 'accountNumberEditText'", EditText.class);
      var1.nextButton = (Button)Utils.findRequiredViewAsType(var2, 2131821404, "field 'nextButton'", Button.class);
      var1.progress = (ProgressBar)Utils.findRequiredViewAsType(var2, 2131821405, "field 'progress'", ProgressBar.class);
   }

   public void unbind() {
      UkBankPayeeDetailsFragment var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.recipientInputLayout = null;
         var1.recipientEditText = null;
         var1.sortCodeInputLayout = null;
         var1.sortCodeEditText = null;
         var1.accountNumberInputLayout = null;
         var1.accountNumberEditText = null;
         var1.nextButton = null;
         var1.progress = null;
      }
   }
}
