package co.uk.getmondo.payments.send.bank.payee;

// $FF: synthetic class
final class e implements io.reactivex.c.h {
   private final UkBankPayeeDetailsFragment a;

   private e(UkBankPayeeDetailsFragment var1) {
      this.a = var1;
   }

   public static io.reactivex.c.h a(UkBankPayeeDetailsFragment var0) {
      return new e(var0);
   }

   public Object a(Object var1) {
      return UkBankPayeeDetailsFragment.a(this.a, var1);
   }
}
