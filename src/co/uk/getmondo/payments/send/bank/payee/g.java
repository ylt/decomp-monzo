package co.uk.getmondo.payments.send.bank.payee;

import co.uk.getmondo.api.ApiException;
import io.reactivex.z;

public class g extends co.uk.getmondo.common.ui.b {
   private final co.uk.getmondo.payments.send.data.a c;
   private final co.uk.getmondo.common.e.a d;
   private final io.reactivex.u e;
   private final io.reactivex.u f;

   g(io.reactivex.u var1, io.reactivex.u var2, co.uk.getmondo.payments.send.data.a var3, co.uk.getmondo.common.e.a var4) {
      this.c = var3;
      this.d = var4;
      this.e = var2;
      this.f = var1;
   }

   // $FF: synthetic method
   static z a(g var0, g.a var1, co.uk.getmondo.payments.send.data.a.b var2) throws Exception {
      return var0.c.a(var2).b(var0.e).a(var0.f).c(k.a(var1)).b(l.a(var1)).a(m.a(var0, var1)).a((Object)var2).a(io.reactivex.v.o_());
   }

   // $FF: synthetic method
   static void a(g.a var0, android.support.v4.g.j var1) throws Exception {
      boolean var2;
      if(!((Boolean)var1.a).booleanValue()) {
         var2 = true;
      } else {
         var2 = false;
      }

      if(var2 && !co.uk.getmondo.payments.send.data.a.b.a.c((String)var1.b)) {
         var0.b();
      } else {
         var0.n();
      }

   }

   // $FF: synthetic method
   static void a(g.a var0, co.uk.getmondo.payments.send.data.a.b var1) throws Exception {
      var0.a(var1.c());
   }

   // $FF: synthetic method
   static void a(g.a var0, io.reactivex.b.b var1) throws Exception {
      var0.o();
   }

   // $FF: synthetic method
   static void a(g.a var0, Throwable var1) throws Exception {
      var0.p();
   }

   // $FF: synthetic method
   static void a(g var0, g.a var1, Throwable var2) throws Exception {
      var0.b(var1, var2);
   }

   // $FF: synthetic method
   static void b(g.a var0, android.support.v4.g.j var1) throws Exception {
      boolean var2;
      if(!((Boolean)var1.a).booleanValue()) {
         var2 = true;
      } else {
         var2 = false;
      }

      if(var2 && !co.uk.getmondo.payments.send.data.a.b.a.b((String)var1.b)) {
         var0.a();
      } else {
         var0.m();
      }

   }

   private void b(g.a var1, Throwable var2) {
      var1.n();
      var1.m();
      if(var2 instanceof ApiException) {
         co.uk.getmondo.api.model.b var3 = ((ApiException)var2).e();
         if(var3 != null) {
            co.uk.getmondo.payments.a.a.c var4 = (co.uk.getmondo.payments.a.a.c)co.uk.getmondo.common.e.d.a(co.uk.getmondo.payments.a.a.c.values(), var3.a());
            if(var4 != null) {
               var4.a(var1);
               return;
            }
         }
      }

      if(!this.d.a(var2, var1)) {
         var1.b(2131362198);
      }

   }

   // $FF: synthetic method
   static void c(g.a var0, android.support.v4.g.j var1) throws Exception {
      boolean var2;
      if(!((Boolean)var1.a).booleanValue()) {
         var2 = true;
      } else {
         var2 = false;
      }

      if(var2 && !co.uk.getmondo.payments.send.data.a.b.a.a((String)var1.b)) {
         var0.k();
      } else {
         var0.l();
      }

   }

   public void a(g.a var1) {
      super.a((co.uk.getmondo.common.ui.f)var1);
      this.a((io.reactivex.b.b)io.reactivex.n.combineLatest(var1.d(), var1.e(), var1.f(), h.a()).subscribe(n.a(var1)));
      this.a((io.reactivex.b.b)var1.g().withLatestFrom((io.reactivex.r)var1.d(), (io.reactivex.c.c)o.a()).subscribe(p.a(var1)));
      this.a((io.reactivex.b.b)var1.h().withLatestFrom((io.reactivex.r)var1.e(), (io.reactivex.c.c)q.a()).subscribe(r.a(var1)));
      this.a((io.reactivex.b.b)var1.i().withLatestFrom((io.reactivex.r)var1.f(), (io.reactivex.c.c)s.a()).subscribe(t.a(var1)));
      io.reactivex.n var2 = var1.j().filter(u.a()).flatMapSingle(i.a(this, var1));
      var1.getClass();
      this.a((io.reactivex.b.b)var2.subscribe(j.a(var1)));
   }

   interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f, a {
      void a(co.uk.getmondo.payments.send.data.a.b var1);

      void a(boolean var1);

      io.reactivex.n d();

      io.reactivex.n e();

      io.reactivex.n f();

      io.reactivex.n g();

      io.reactivex.n h();

      io.reactivex.n i();

      io.reactivex.n j();

      void k();

      void l();

      void m();

      void n();

      void o();

      void p();
   }
}
