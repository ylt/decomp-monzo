package co.uk.getmondo.payments.send.peer;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Button;
import android.widget.EditText;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.uk.getmondo.api.model.tracking.Impression;
import co.uk.getmondo.common.ui.AmountInputView;
import co.uk.getmondo.d.aa;
import co.uk.getmondo.payments.send.authentication.PaymentAuthenticationActivity;

public class PeerPaymentActivity extends co.uk.getmondo.common.activities.b implements j.a {
   j a;
   @BindView(2131821023)
   AmountInputView amountInputView;
   co.uk.getmondo.common.a b;
   private final Handler c = new Handler();
   @BindView(2131821024)
   EditText notesEditText;
   @BindView(2131821025)
   Button sendMoneyButton;

   public static Intent a(Context var0, aa var1, Impression.PaymentFlowFrom var2) {
      return (new Intent(var0, PeerPaymentActivity.class)).putExtra("key_peer", var1).putExtra("key_entry_point", var2);
   }

   public static Intent a(Context var0, co.uk.getmondo.payments.send.data.a.g var1, Impression.PaymentFlowFrom var2) {
      return (new Intent(var0, PeerPaymentActivity.class)).putExtra("key_peer", var1.c()).putExtra("key_notes", var1.b()).putExtra("key_amount", var1.a()).putExtra("key_entry_point", var2);
   }

   // $FF: synthetic method
   static android.support.v4.g.j a(PeerPaymentActivity var0, Object var1) throws Exception {
      return android.support.v4.g.j.a(var0.amountInputView.getAmountText().toString(), var0.notesEditText.getText().toString());
   }

   // $FF: synthetic method
   static void a(PeerPaymentActivity var0, android.support.v7.app.a var1, co.uk.getmondo.d.c var2) {
      var1.b(String.format(var0.getString(2131362021), new Object[]{var2.toString()}));
   }

   public io.reactivex.n a() {
      return this.amountInputView.a().map(b.a());
   }

   public void a(co.uk.getmondo.d.c var1) {
      android.support.v7.app.a var2 = this.getSupportActionBar();
      if(var2 != null) {
         this.c.post(a.a(this, var2, var1));
      }

   }

   public void a(co.uk.getmondo.d.c var1, String var2) {
      if(var1 != null && var1.k() != 0L) {
         this.amountInputView.setDefaultAmount(var1);
      }

      this.notesEditText.setText(var2);
   }

   public void a(co.uk.getmondo.payments.send.data.a.g var1) {
      this.startActivity(PaymentAuthenticationActivity.a((Context)this, (co.uk.getmondo.payments.send.data.a.f)var1));
   }

   public void a(String var1) {
      this.setTitle(var1);
   }

   public void a(boolean var1) {
      this.sendMoneyButton.setEnabled(var1);
   }

   public io.reactivex.n b() {
      return com.b.a.c.c.a(this.sendMoneyButton).map(c.a(this));
   }

   public void b(co.uk.getmondo.d.c var1) {
      co.uk.getmondo.common.d.a.a(this.getString(2131362535), this.getString(2131362536, new Object[]{var1.toString()}), false).show(this.getSupportFragmentManager(), "ERROR_FRAGMENT_TAG");
   }

   protected void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.setContentView(2131034194);
      ButterKnife.bind((Activity)this);
      aa var3 = (aa)this.getIntent().getParcelableExtra("key_peer");
      co.uk.getmondo.d.c var2 = (co.uk.getmondo.d.c)this.getIntent().getParcelableExtra("key_amount");
      String var4 = this.getIntent().getStringExtra("key_notes");
      this.l().a(new f(var3, var2, var4)).a(this);
      this.a.a((j.a)this);
      Impression.PaymentFlowFrom var5 = (Impression.PaymentFlowFrom)this.getIntent().getSerializableExtra("key_entry_point");
      this.b.a(Impression.a(var5));
   }

   protected void onDestroy() {
      this.a.b();
      super.onDestroy();
   }
}
