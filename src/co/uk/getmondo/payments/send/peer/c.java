package co.uk.getmondo.payments.send.peer;

// $FF: synthetic class
final class c implements io.reactivex.c.h {
   private final PeerPaymentActivity a;

   private c(PeerPaymentActivity var1) {
      this.a = var1;
   }

   public static io.reactivex.c.h a(PeerPaymentActivity var0) {
      return new c(var0);
   }

   public Object a(Object var1) {
      return PeerPaymentActivity.a(this.a, var1);
   }
}
