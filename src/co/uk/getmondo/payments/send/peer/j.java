package co.uk.getmondo.payments.send.peer;

import co.uk.getmondo.d.aa;
import io.reactivex.u;

class j extends co.uk.getmondo.common.ui.b {
   private final u c;
   private final u d;
   private final co.uk.getmondo.common.e.a e;
   private final co.uk.getmondo.common.accounts.d f;
   private final aa g;
   private final co.uk.getmondo.d.c h;
   private final String i;
   private final co.uk.getmondo.a.a j;
   private co.uk.getmondo.d.c k;

   j(u var1, u var2, co.uk.getmondo.common.e.a var3, co.uk.getmondo.common.accounts.d var4, aa var5, co.uk.getmondo.d.c var6, String var7, co.uk.getmondo.a.a var8) {
      this.c = var1;
      this.d = var2;
      this.e = var3;
      this.f = var4;
      this.g = var5;
      this.h = var6;
      this.i = var7;
      this.j = var8;
   }

   // $FF: synthetic method
   static void a() throws Exception {
      d.a.a.b("Balance synced", new Object[0]);
   }

   // $FF: synthetic method
   static void a(j var0, j.a var1, android.support.v4.g.j var2) throws Exception {
      co.uk.getmondo.d.c var3 = co.uk.getmondo.d.c.a((String)var2.a, co.uk.getmondo.common.i.c.a);
      if(!var0.a(var3)) {
         d.a.a.a((Throwable)(new RuntimeException("Balance string is not a valid number")));
      } else if(var0.k == null || !var0.k.a() && var3.k() <= var0.k.k()) {
         var1.a(new co.uk.getmondo.payments.send.data.a.g(var3, (String)var2.b, var0.g));
      } else {
         var1.b(var0.k);
      }

   }

   // $FF: synthetic method
   static void a(j var0, j.a var1, co.uk.getmondo.d.c var2) throws Exception {
      var0.k = var2;
      var1.a(var2);
   }

   // $FF: synthetic method
   static void a(j var0, j.a var1, String var2) throws Exception {
      var1.a(var0.a(co.uk.getmondo.d.c.a(var2, co.uk.getmondo.common.i.c.a)));
   }

   // $FF: synthetic method
   static void a(j var0, j.a var1, Throwable var2) throws Exception {
      var0.e.a(var2, var1);
   }

   private boolean a(co.uk.getmondo.d.c var1) {
      boolean var2;
      if(var1 != null && var1.f() > 0.0D) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   public void a(j.a var1) {
      super.a((co.uk.getmondo.common.ui.f)var1);
      var1.a(this.g.b());
      var1.a(this.h, this.i);
      String var2 = this.f.b().c().a();
      this.a((io.reactivex.b.b)this.j.a(var2).observeOn(this.c).map(k.a()).subscribe(l.a(this, var1), m.a()));
      this.a((io.reactivex.b.b)this.j.b(var2).b(this.d).a(this.c).a(n.b(), o.a(this, var1)));
      this.a((io.reactivex.b.b)var1.a().subscribe(p.a(this, var1)));
      this.a((io.reactivex.b.b)var1.b().subscribe(q.a(this, var1)));
   }

   interface a extends co.uk.getmondo.common.e.a.a, co.uk.getmondo.common.ui.f {
      io.reactivex.n a();

      void a(co.uk.getmondo.d.c var1);

      void a(co.uk.getmondo.d.c var1, String var2);

      void a(co.uk.getmondo.payments.send.data.a.g var1);

      void a(String var1);

      void a(boolean var1);

      io.reactivex.n b();

      void b(co.uk.getmondo.d.c var1);
   }
}
