package co.uk.getmondo.payments.send.peer;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import co.uk.getmondo.common.ui.AmountInputView;

public class PeerPaymentActivity_ViewBinding implements Unbinder {
   private PeerPaymentActivity a;

   public PeerPaymentActivity_ViewBinding(PeerPaymentActivity var1, View var2) {
      this.a = var1;
      var1.amountInputView = (AmountInputView)Utils.findRequiredViewAsType(var2, 2131821023, "field 'amountInputView'", AmountInputView.class);
      var1.notesEditText = (EditText)Utils.findRequiredViewAsType(var2, 2131821024, "field 'notesEditText'", EditText.class);
      var1.sendMoneyButton = (Button)Utils.findRequiredViewAsType(var2, 2131821025, "field 'sendMoneyButton'", Button.class);
   }

   public void unbind() {
      PeerPaymentActivity var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.amountInputView = null;
         var1.notesEditText = null;
         var1.sendMoneyButton = null;
      }
   }
}
