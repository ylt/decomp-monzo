package co.uk.getmondo.payments.send.peer;

// $FF: synthetic class
final class a implements Runnable {
   private final PeerPaymentActivity a;
   private final android.support.v7.app.a b;
   private final co.uk.getmondo.d.c c;

   private a(PeerPaymentActivity var1, android.support.v7.app.a var2, co.uk.getmondo.d.c var3) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
   }

   public static Runnable a(PeerPaymentActivity var0, android.support.v7.app.a var1, co.uk.getmondo.d.c var2) {
      return new a(var0, var1, var2);
   }

   public void run() {
      PeerPaymentActivity.a(this.a, this.b, this.c);
   }
}
