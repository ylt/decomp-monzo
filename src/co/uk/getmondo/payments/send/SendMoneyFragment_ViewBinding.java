package co.uk.getmondo.payments.send;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import butterknife.Unbinder;
import butterknife.internal.Utils;

public class SendMoneyFragment_ViewBinding implements Unbinder {
   private SendMoneyFragment a;

   public SendMoneyFragment_ViewBinding(SendMoneyFragment var1, View var2) {
      this.a = var1;
      var1.contactsRecyclerView = (RecyclerView)Utils.findRequiredViewAsType(var2, 2131821363, "field 'contactsRecyclerView'", RecyclerView.class);
      var1.contactsProgressBar = (ProgressBar)Utils.findRequiredViewAsType(var2, 2131821361, "field 'contactsProgressBar'", ProgressBar.class);
      var1.swipeRefreshLayout = (SwipeRefreshLayout)Utils.findRequiredViewAsType(var2, 2131821362, "field 'swipeRefreshLayout'", SwipeRefreshLayout.class);
   }

   public void unbind() {
      SendMoneyFragment var1 = this.a;
      if(var1 == null) {
         throw new IllegalStateException("Bindings already cleared.");
      } else {
         this.a = null;
         var1.contactsRecyclerView = null;
         var1.contactsProgressBar = null;
         var1.swipeRefreshLayout = null;
      }
   }
}
