package co.uk.getmondo.payments.send;

import android.content.Context;

public final class g implements b.a.b {
   // $FF: synthetic field
   static final boolean a;
   private final b.a b;
   private final javax.a.a c;

   static {
      boolean var0;
      if(!g.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   public g(b.a var1, javax.a.a var2) {
      if(!a && var1 == null) {
         throw new AssertionError();
      } else {
         this.b = var1;
         if(!a && var2 == null) {
            throw new AssertionError();
         } else {
            this.c = var2;
         }
      }
   }

   public static b.a.b a(b.a var0, javax.a.a var1) {
      return new g(var0, var1);
   }

   public SendMoneyAdapter a() {
      return (SendMoneyAdapter)b.a.c.a(this.b, new SendMoneyAdapter((Context)this.c.b()));
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
