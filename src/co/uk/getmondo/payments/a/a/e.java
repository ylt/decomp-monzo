package co.uk.getmondo.payments.a.a;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import io.realm.ar;
import io.realm.bc;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.d.b.i;
import kotlin.d.b.l;
import org.threeten.bp.LocalDate;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\b\u001d\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\b\u0016\u0018\u0000 ;2\u00020\u00012\u00020\u0002:\u0001;B7\b\u0016\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0004\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\f¢\u0006\u0002\u0010\rB\u000f\b\u0016\u0012\u0006\u0010\u000e\u001a\u00020\u000f¢\u0006\u0002\u0010\u0010B\u0005¢\u0006\u0002\u0010\u0011J\b\u00100\u001a\u000201H\u0016J\u0013\u00102\u001a\u0002032\b\u00104\u001a\u0004\u0018\u000105H\u0096\u0002J\b\u00106\u001a\u000201H\u0016J\u0018\u00107\u001a\u0002082\u0006\u00109\u001a\u00020\u000f2\u0006\u0010:\u001a\u000201H\u0016R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0004X\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0015\u001a\u0004\u0018\u00010\u0004X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0004X\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0017\u001a\u0004\u0018\u00010\u0004X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0004X\u0082\u000e¢\u0006\u0002\n\u0000R\u001a\u0010\u0005\u001a\u00020\u0004X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\u001a\"\u0004\b\u001b\u0010\u001cR$\u0010\t\u001a\u00020\n2\u0006\u0010\t\u001a\u00020\n8F@FX\u0086\u000e¢\u0006\f\u001a\u0004\b\u001d\u0010\u001e\"\u0004\b\u001f\u0010 R\u0011\u0010!\u001a\u00020\u00048F¢\u0006\u0006\u001a\u0004\b\"\u0010\u001aR\u001e\u0010\u0003\u001a\u00020\u00048\u0006@\u0006X\u0087\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b#\u0010\u001a\"\u0004\b$\u0010\u001cR$\u0010\u000b\u001a\u00020\f2\u0006\u0010%\u001a\u00020\f8F@FX\u0086\u000e¢\u0006\f\u001a\u0004\b&\u0010'\"\u0004\b(\u0010)R\u001a\u0010\u0007\u001a\u00020\bX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b*\u0010+\"\u0004\b,\u0010-R\u001a\u0010\u0006\u001a\u00020\u0004X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b.\u0010\u001a\"\u0004\b/\u0010\u001c¨\u0006<"},
   d2 = {"Lco/uk/getmondo/payments/data/model/PaymentSeries;", "Lio/realm/RealmObject;", "Lco/uk/getmondo/payments/data/model/RecurringPayment;", "id", "", "accountId", "scheme", "schemaData", "Lco/uk/getmondo/payments/data/model/FpsSchemaData;", "amount", "Lco/uk/getmondo/model/Amount;", "paymentSchedule", "Lco/uk/getmondo/payments/data/model/PaymentSchedule;", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/payments/data/model/FpsSchemaData;Lco/uk/getmondo/model/Amount;Lco/uk/getmondo/payments/data/model/PaymentSchedule;)V", "source", "Landroid/os/Parcel;", "(Landroid/os/Parcel;)V", "()V", "_amount", "", "_currency", "_endDate", "_intervalType", "_nextIterationDate", "_startDate", "getAccountId", "()Ljava/lang/String;", "setAccountId", "(Ljava/lang/String;)V", "getAmount", "()Lco/uk/getmondo/model/Amount;", "setAmount", "(Lco/uk/getmondo/model/Amount;)V", "displayName", "getDisplayName", "getId", "setId", "value", "getPaymentSchedule", "()Lco/uk/getmondo/payments/data/model/PaymentSchedule;", "setPaymentSchedule", "(Lco/uk/getmondo/payments/data/model/PaymentSchedule;)V", "getSchemaData", "()Lco/uk/getmondo/payments/data/model/FpsSchemaData;", "setSchemaData", "(Lco/uk/getmondo/payments/data/model/FpsSchemaData;)V", "getScheme", "setScheme", "describeContents", "", "equals", "", "other", "", "hashCode", "writeToParcel", "", "dest", "flags", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public class e extends bc implements f, ar {
   public static final Creator CREATOR = (Creator)(new Creator() {
      public e a(Parcel var1) {
         l.b(var1, "source");
         return new e(var1);
      }

      public e[] a(int var1) {
         return new e[var1];
      }

      // $FF: synthetic method
      public Object createFromParcel(Parcel var1) {
         return this.a(var1);
      }

      // $FF: synthetic method
      public Object[] newArray(int var1) {
         return (Object[])this.a(var1);
      }
   });
   public static final e.a a = new e.a((i)null);
   private String b;
   private String c;
   private String d;
   private b e;
   private long f;
   private String g;
   private String h;
   private String i;
   private String j;
   private String k;

   public e() {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

      this.a("");
      this.b("");
      this.c("");
      this.a(new b((String)null, (String)null, (String)null, (String)null, (String)null, 31, (i)null));
      this.d("");
      this.e("");
      this.h("");
   }

   public e(Parcel var1) {
      l.b(var1, "source");
      String var3 = var1.readString();
      l.a(var3, "source.readString()");
      String var2 = var1.readString();
      l.a(var2, "source.readString()");
      String var4 = var1.readString();
      l.a(var4, "source.readString()");
      Parcelable var5 = var1.readParcelable(b.class.getClassLoader());
      l.a(var5, "source.readParcelable(Fp…::class.java.classLoader)");
      b var7 = (b)var5;
      Parcelable var6 = var1.readParcelable(co.uk.getmondo.d.c.class.getClassLoader());
      l.a(var6, "source.readParcelable(Am…::class.java.classLoader)");
      co.uk.getmondo.d.c var8 = (co.uk.getmondo.d.c)var6;
      Parcelable var9 = var1.readParcelable(d.class.getClassLoader());
      l.a(var9, "source.readParcelable(Pa…::class.java.classLoader)");
      this(var3, var2, var4, var7, var8, (d)var9);
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

   }

   public e(String var1, String var2, String var3, b var4, co.uk.getmondo.d.c var5, d var6) {
      l.b(var1, "id");
      l.b(var2, "accountId");
      l.b(var3, "scheme");
      l.b(var4, "schemaData");
      l.b(var5, "amount");
      l.b(var6, "paymentSchedule");
      this();
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

      this.a(var1);
      this.b(var2);
      this.c(var3);
      this.a(var4);
      this.a(var5);
      this.a(var6);
   }

   public final String a() {
      return this.f();
   }

   public void a(long var1) {
      this.f = var1;
   }

   public final void a(co.uk.getmondo.d.c var1) {
      l.b(var1, "amount");
      this.a(var1.k());
      String var2 = var1.l().b();
      l.a(var2, "amount.currency.currencyCode");
      this.d(var2);
   }

   public void a(b var1) {
      this.e = var1;
   }

   public final void a(d var1) {
      Object var3 = null;
      l.b(var1, "value");
      String var2 = var1.b().toString();
      l.a(var2, "value.startDate.toString()");
      this.e(var2);
      LocalDate var5 = var1.d();
      if(var5 != null) {
         var2 = var5.toString();
      } else {
         var2 = null;
      }

      this.g(var2);
      this.h(var1.c().a());
      var5 = var1.e();
      String var4 = (String)var3;
      if(var5 != null) {
         var4 = var5.toString();
      }

      this.f(var4);
   }

   public void a(String var1) {
      this.b = var1;
   }

   public final b b() {
      return this.i();
   }

   public void b(String var1) {
      this.c = var1;
   }

   public final co.uk.getmondo.d.c c() {
      return new co.uk.getmondo.d.c(this.j(), this.k());
   }

   public void c(String var1) {
      this.d = var1;
   }

   public final String d() {
      return this.i().d();
   }

   public void d(String var1) {
      this.g = var1;
   }

   public int describeContents() {
      return 0;
   }

   public final d e() {
      LocalDate var2 = null;
      LocalDate var3 = LocalDate.a((CharSequence)this.l());
      l.a(var3, "LocalDate.parse(_startDate)");
      d.c var4 = d.c.f.a(this.o());
      LocalDate var1;
      if(this.n() != null) {
         var1 = LocalDate.a((CharSequence)this.n());
      } else {
         var1 = null;
      }

      if(this.m() != null) {
         var2 = LocalDate.a((CharSequence)this.m());
      }

      return new d(var3, var4, var1, var2);
   }

   public void e(String var1) {
      this.h = var1;
   }

   public boolean equals(Object var1) {
      boolean var2;
      if((e)this == var1) {
         var2 = true;
      } else {
         Class var3;
         if(var1 != null) {
            var3 = var1.getClass();
         } else {
            var3 = null;
         }

         if(l.a(var3, this.getClass()) ^ true) {
            var2 = false;
         } else {
            if(var1 == null) {
               throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.payments.data.model.PaymentSeries");
            }

            e var4 = (e)var1;
            if(l.a(this.f(), ((e)var1).f()) ^ true) {
               var2 = false;
            } else if(l.a(this.g(), ((e)var1).g()) ^ true) {
               var2 = false;
            } else if(l.a(this.h(), ((e)var1).h()) ^ true) {
               var2 = false;
            } else if(l.a(this.i(), ((e)var1).i()) ^ true) {
               var2 = false;
            } else if(this.j() != ((e)var1).j()) {
               var2 = false;
            } else if(l.a(this.k(), ((e)var1).k()) ^ true) {
               var2 = false;
            } else if(l.a(this.l(), ((e)var1).l()) ^ true) {
               var2 = false;
            } else if(l.a(this.m(), ((e)var1).m()) ^ true) {
               var2 = false;
            } else if(l.a(this.n(), ((e)var1).n()) ^ true) {
               var2 = false;
            } else if(l.a(this.o(), ((e)var1).o()) ^ true) {
               var2 = false;
            } else {
               var2 = true;
            }
         }
      }

      return var2;
   }

   public String f() {
      return this.b;
   }

   public void f(String var1) {
      this.i = var1;
   }

   public String g() {
      return this.c;
   }

   public void g(String var1) {
      this.j = var1;
   }

   public String h() {
      return this.d;
   }

   public void h(String var1) {
      this.k = var1;
   }

   public int hashCode() {
      int var2 = 0;
      int var7 = this.f().hashCode();
      int var6 = this.g().hashCode();
      int var4 = this.h().hashCode();
      int var8 = this.i().hashCode();
      int var5 = Long.valueOf(this.j()).hashCode();
      int var3 = this.k().hashCode();
      int var9 = this.l().hashCode();
      String var10 = this.m();
      int var1;
      if(var10 != null) {
         var1 = var10.hashCode();
      } else {
         var1 = 0;
      }

      var10 = this.n();
      if(var10 != null) {
         var2 = var10.hashCode();
      }

      return ((var1 + ((((((var7 * 31 + var6) * 31 + var4) * 31 + var8) * 31 + var5) * 31 + var3) * 31 + var9) * 31) * 31 + var2) * 31 + this.o().hashCode();
   }

   public b i() {
      return this.e;
   }

   public long j() {
      return this.f;
   }

   public String k() {
      return this.g;
   }

   public String l() {
      return this.h;
   }

   public String m() {
      return this.i;
   }

   public String n() {
      return this.j;
   }

   public String o() {
      return this.k;
   }

   public void writeToParcel(Parcel var1, int var2) {
      l.b(var1, "dest");
      var1.writeString(this.f());
      var1.writeString(this.g());
      var1.writeString(this.h());
      var1.writeParcelable((Parcelable)this.i(), var2);
      var1.writeParcelable((Parcelable)this.c(), var2);
      var1.writeParcelable((Parcelable)this.e(), var2);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"},
      d2 = {"Lco/uk/getmondo/payments/data/model/PaymentSeries$Companion;", "", "()V", "CREATOR", "Landroid/os/Parcelable$Creator;", "Lco/uk/getmondo/payments/data/model/PaymentSeries;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(i var1) {
         this();
      }
   }
}
