package co.uk.getmondo.payments.a.a;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.io.Serializable;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.d.b.i;
import kotlin.d.b.l;
import org.threeten.bp.LocalDate;
import org.threeten.bp.chrono.ChronoLocalDate;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u000b\n\u0002\b\t\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\b\u0086\b\u0018\u0000 &2\u00020\u0001:\u0002&'B\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004B/\b\u0007\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u0006\u0012\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u0006¢\u0006\u0002\u0010\u000bJ\t\u0010\u0015\u001a\u00020\u0006HÆ\u0003J\t\u0010\u0016\u001a\u00020\bHÆ\u0003J\u000b\u0010\u0017\u001a\u0004\u0018\u00010\u0006HÆ\u0003J\u000b\u0010\u0018\u001a\u0004\u0018\u00010\u0006HÆ\u0003J5\u0010\u0019\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u00062\b\b\u0002\u0010\u0007\u001a\u00020\b2\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u00062\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u0006HÆ\u0001J\b\u0010\u001a\u001a\u00020\u001bH\u0016J\u0013\u0010\u001c\u001a\u00020\u00112\b\u0010\u001d\u001a\u0004\u0018\u00010\u001eHÖ\u0003J\t\u0010\u001f\u001a\u00020\u001bHÖ\u0001J\t\u0010 \u001a\u00020!HÖ\u0001J\u0018\u0010\"\u001a\u00020#2\u0006\u0010$\u001a\u00020\u00032\u0006\u0010%\u001a\u00020\u001bH\u0016R\u0013\u0010\t\u001a\u0004\u0018\u00010\u0006¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0011\u0010\u0007\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0011\u0010\u0010\u001a\u00020\u00118F¢\u0006\u0006\u001a\u0004\b\u0010\u0010\u0012R\u0013\u0010\n\u001a\u0004\u0018\u00010\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\rR\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\r¨\u0006("},
   d2 = {"Lco/uk/getmondo/payments/data/model/PaymentSchedule;", "Landroid/os/Parcelable;", "source", "Landroid/os/Parcel;", "(Landroid/os/Parcel;)V", "startDate", "Lorg/threeten/bp/LocalDate;", "interval", "Lco/uk/getmondo/payments/data/model/PaymentSchedule$Interval;", "endDate", "nextIterationDate", "(Lorg/threeten/bp/LocalDate;Lco/uk/getmondo/payments/data/model/PaymentSchedule$Interval;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;)V", "getEndDate", "()Lorg/threeten/bp/LocalDate;", "getInterval", "()Lco/uk/getmondo/payments/data/model/PaymentSchedule$Interval;", "isTodayAndOnce", "", "()Z", "getNextIterationDate", "getStartDate", "component1", "component2", "component3", "component4", "copy", "describeContents", "", "equals", "other", "", "hashCode", "toString", "", "writeToParcel", "", "dest", "flags", "Companion", "Interval", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class d implements Parcelable {
   public static final Creator CREATOR = (Creator)(new Creator() {
      public d a(Parcel var1) {
         l.b(var1, "source");
         return new d(var1);
      }

      public d[] a(int var1) {
         return new d[var1];
      }

      // $FF: synthetic method
      public Object createFromParcel(Parcel var1) {
         return this.a(var1);
      }

      // $FF: synthetic method
      public Object[] newArray(int var1) {
         return (Object[])this.a(var1);
      }
   });
   public static final d.a a = new d.a((i)null);
   private final LocalDate b;
   private final d.c c;
   private final LocalDate d;
   private final LocalDate e;

   public d(Parcel var1) {
      l.b(var1, "source");
      Serializable var2 = var1.readSerializable();
      if(var2 == null) {
         throw new TypeCastException("null cannot be cast to non-null type org.threeten.bp.LocalDate");
      } else {
         this((LocalDate)var2, d.c.values()[var1.readInt()], (LocalDate)var1.readSerializable(), (LocalDate)var1.readSerializable());
      }
   }

   public d(LocalDate var1, d.c var2, LocalDate var3, LocalDate var4) {
      l.b(var1, "startDate");
      l.b(var2, "interval");
      super();
      this.b = var1;
      this.c = var2;
      this.d = var3;
      this.e = var4;
   }

   // $FF: synthetic method
   public d(LocalDate var1, d.c var2, LocalDate var3, LocalDate var4, int var5, i var6) {
      if((var5 & 4) != 0) {
         var3 = (LocalDate)null;
      }

      if((var5 & 8) != 0) {
         var4 = (LocalDate)null;
      }

      this(var1, var2, var3, var4);
   }

   public final boolean a() {
      boolean var1;
      if(this.b.d((ChronoLocalDate)LocalDate.a()) && l.a(this.c, d.c.a)) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public final LocalDate b() {
      return this.b;
   }

   public final d.c c() {
      return this.c;
   }

   public final LocalDate d() {
      return this.d;
   }

   public int describeContents() {
      return 0;
   }

   public final LocalDate e() {
      return this.e;
   }

   public boolean equals(Object var1) {
      boolean var2;
      if(this != var1) {
         label32: {
            if(var1 instanceof d) {
               d var3 = (d)var1;
               if(l.a(this.b, var3.b) && l.a(this.c, var3.c) && l.a(this.d, var3.d) && l.a(this.e, var3.e)) {
                  break label32;
               }
            }

            var2 = false;
            return var2;
         }
      }

      var2 = true;
      return var2;
   }

   public int hashCode() {
      int var4 = 0;
      LocalDate var5 = this.b;
      int var1;
      if(var5 != null) {
         var1 = var5.hashCode();
      } else {
         var1 = 0;
      }

      d.c var6 = this.c;
      int var2;
      if(var6 != null) {
         var2 = var6.hashCode();
      } else {
         var2 = 0;
      }

      var5 = this.d;
      int var3;
      if(var5 != null) {
         var3 = var5.hashCode();
      } else {
         var3 = 0;
      }

      var5 = this.e;
      if(var5 != null) {
         var4 = var5.hashCode();
      }

      return (var3 + (var2 + var1 * 31) * 31) * 31 + var4;
   }

   public String toString() {
      return "PaymentSchedule(startDate=" + this.b + ", interval=" + this.c + ", endDate=" + this.d + ", nextIterationDate=" + this.e + ")";
   }

   public void writeToParcel(Parcel var1, int var2) {
      l.b(var1, "dest");
      var1.writeSerializable((Serializable)this.b);
      var1.writeInt(this.c.ordinal());
      var1.writeSerializable((Serializable)this.d);
      var1.writeSerializable((Serializable)this.e);
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"},
      d2 = {"Lco/uk/getmondo/payments/data/model/PaymentSchedule$Companion;", "", "()V", "CREATOR", "Landroid/os/Parcelable$Creator;", "Lco/uk/getmondo/payments/data/model/PaymentSchedule;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(i var1) {
         this();
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0012\b\u0086\u0001\u0018\u0000 \u00162\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u0016B/\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0001\u0010\u0006\u001a\u00020\u0005\u0012\n\b\u0003\u0010\u0007\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0015\u0010\u0007\u001a\u0004\u0018\u00010\u0005¢\u0006\n\n\u0002\u0010\r\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0011\u0010\u0006\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u000fj\u0002\b\u0011j\u0002\b\u0012j\u0002\b\u0013j\u0002\b\u0014j\u0002\b\u0015¨\u0006\u0017"},
      d2 = {"Lco/uk/getmondo/payments/data/model/PaymentSchedule$Interval;", "", "apiValue", "", "nameRes", "", "repeatLabelRes", "descriptionRes", "(Ljava/lang/String;ILjava/lang/String;IILjava/lang/Integer;)V", "getApiValue", "()Ljava/lang/String;", "getDescriptionRes", "()Ljava/lang/Integer;", "Ljava/lang/Integer;", "getNameRes", "()I", "getRepeatLabelRes", "ONCE", "DAILY", "WEEKLY", "MONTHLY", "YEARLY", "Companion", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static enum c {
      a,
      b,
      c,
      d,
      e;

      public static final d.a f;
      private final String h;
      private final int i;
      private final int j;
      private final Integer k;

      static {
         d.c var3 = new d.c("ONCE", 0, "ONCE", 2131362288, 2131362295, (Integer)null, 8, (i)null);
         a = var3;
         d.c var0 = new d.c("DAILY", 1, "DAILY", 2131362286, 2131362293, (Integer)null, 8, (i)null);
         b = var0;
         d.c var1 = new d.c("WEEKLY", 2, "WEEKLY", 2131362289, 2131362296, (Integer)null, 8, (i)null);
         c = var1;
         d.c var4 = new d.c("MONTHLY", 3, "MONTHLY", 2131362287, 2131362294, Integer.valueOf(2131362285));
         d = var4;
         d.c var2 = new d.c("YEARLY", 4, "YEARLY", 2131362290, 2131362297, (Integer)null, 8, (i)null);
         e = var2;
         f = new d.a((i)null);
      }

      protected c(String var3, int var4, int var5, Integer var6) {
         l.b(var3, "apiValue");
         super(var1, var2);
         this.h = var3;
         this.i = var4;
         this.j = var5;
         this.k = var6;
      }

      // $FF: synthetic method
      c(String var3, int var4, int var5, Integer var6, int var7, i var8) {
         if((var7 & 8) != 0) {
            var6 = (Integer)null;
         }

         this(var3, var4, var5, var6);
      }

      public final String a() {
         return this.h;
      }

      public final int b() {
         return this.i;
      }

      public final int c() {
         return this.j;
      }

      public final Integer d() {
         return this.k;
      }
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\u0007"},
      d2 = {"Lco/uk/getmondo/payments/data/model/PaymentSchedule$Interval$Companion;", "", "()V", "from", "Lco/uk/getmondo/payments/data/model/PaymentSchedule$Interval;", "apiValue", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(i var1) {
         this();
      }

      public final d.c a(String var1) {
         l.b(var1, "apiValue");
         Object[] var4 = (Object[])d.c.values();
         int var2 = 0;

         Object var3;
         while(true) {
            if(var2 >= var4.length) {
               var3 = null;
               break;
            }

            var3 = var4[var2];
            if(l.a(((d.c)var3).a(), var1)) {
               break;
            }

            ++var2;
         }

         d.c var5 = (d.c)var3;
         if(var5 != null) {
            return var5;
         } else {
            throw (Throwable)(new IllegalArgumentException("Unsupported interval type: " + var1));
         }
      }
   }
}
