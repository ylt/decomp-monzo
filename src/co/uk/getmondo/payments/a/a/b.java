package co.uk.getmondo.payments.a.a;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import io.realm.bc;
import io.realm.x;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.d.b.i;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0012\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\b\u0016\u0018\u0000 $2\u00020\u00012\u00020\u0002:\u0001$B\u000f\b\u0016\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005B7\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007\u0012\b\b\u0002\u0010\b\u001a\u00020\u0007\u0012\b\b\u0002\u0010\t\u001a\u00020\u0007\u0012\b\b\u0002\u0010\n\u001a\u00020\u0007\u0012\b\b\u0002\u0010\u000b\u001a\u00020\u0007¢\u0006\u0002\u0010\fJ\b\u0010\u0019\u001a\u00020\u001aH\u0016J\u0013\u0010\u001b\u001a\u00020\u001c2\b\u0010\u001d\u001a\u0004\u0018\u00010\u001eH\u0096\u0002J\b\u0010\u001f\u001a\u00020\u001aH\u0016J\u0018\u0010 \u001a\u00020!2\u0006\u0010\"\u001a\u00020\u00042\u0006\u0010#\u001a\u00020\u001aH\u0016R\u001a\u0010\t\u001a\u00020\u0007X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R\u001a\u0010\u000b\u001a\u00020\u0007X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u000e\"\u0004\b\u0012\u0010\u0010R\u001e\u0010\u0006\u001a\u00020\u00078\u0006@\u0006X\u0087\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u000e\"\u0004\b\u0014\u0010\u0010R\u001a\u0010\b\u001a\u00020\u0007X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0015\u0010\u000e\"\u0004\b\u0016\u0010\u0010R\u001a\u0010\n\u001a\u00020\u0007X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0017\u0010\u000e\"\u0004\b\u0018\u0010\u0010¨\u0006%"},
   d2 = {"Lco/uk/getmondo/payments/data/model/FpsSchemaData;", "Lio/realm/RealmObject;", "Landroid/os/Parcelable;", "source", "Landroid/os/Parcel;", "(Landroid/os/Parcel;)V", "id", "", "reference", "accountNumber", "sortCode", "customerName", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getAccountNumber", "()Ljava/lang/String;", "setAccountNumber", "(Ljava/lang/String;)V", "getCustomerName", "setCustomerName", "getId", "setId", "getReference", "setReference", "getSortCode", "setSortCode", "describeContents", "", "equals", "", "other", "", "hashCode", "writeToParcel", "", "dest", "flags", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public class b extends bc implements Parcelable, x {
   public static final Creator CREATOR = (Creator)(new Creator() {
      public b a(Parcel var1) {
         l.b(var1, "source");
         return new b(var1);
      }

      public b[] a(int var1) {
         return new b[var1];
      }

      // $FF: synthetic method
      public Object createFromParcel(Parcel var1) {
         return this.a(var1);
      }

      // $FF: synthetic method
      public Object[] newArray(int var1) {
         return (Object[])this.a(var1);
      }
   });
   public static final b.a a = new b.a((i)null);
   private String b;
   private String c;
   private String d;
   private String e;
   private String f;

   public b() {
      this((String)null, (String)null, (String)null, (String)null, (String)null, 31, (i)null);
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

   }

   public b(Parcel var1) {
      l.b(var1, "source");
      String var4 = var1.readString();
      l.a(var4, "source.readString()");
      String var3 = var1.readString();
      l.a(var3, "source.readString()");
      String var2 = var1.readString();
      l.a(var2, "source.readString()");
      String var5 = var1.readString();
      l.a(var5, "source.readString()");
      String var6 = var1.readString();
      l.a(var6, "source.readString()");
      this(var4, var3, var2, var5, var6);
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

   }

   public b(String var1, String var2, String var3, String var4, String var5) {
      l.b(var1, "id");
      l.b(var2, "reference");
      l.b(var3, "accountNumber");
      l.b(var4, "sortCode");
      l.b(var5, "customerName");
      super();
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

      this.b(var1);
      this.c(var2);
      this.d(var3);
      this.e(var4);
      this.f(var5);
   }

   // $FF: synthetic method
   public b(String var1, String var2, String var3, String var4, String var5, int var6, i var7) {
      if((var6 & 1) != 0) {
         var1 = "";
      }

      if((var6 & 2) != 0) {
         var2 = "";
      }

      if((var6 & 4) != 0) {
         var3 = "";
      }

      if((var6 & 8) != 0) {
         var4 = "";
      }

      if((var6 & 16) != 0) {
         var5 = "";
      }

      this(var1, var2, var3, var4, var5);
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

   }

   public final String a() {
      return this.f();
   }

   public final void a(String var1) {
      l.b(var1, "<set-?>");
      this.b(var1);
   }

   public final String b() {
      return this.g();
   }

   public void b(String var1) {
      this.b = var1;
   }

   public final String c() {
      return this.h();
   }

   public void c(String var1) {
      this.c = var1;
   }

   public final String d() {
      return this.i();
   }

   public void d(String var1) {
      this.d = var1;
   }

   public int describeContents() {
      return 0;
   }

   public String e() {
      return this.b;
   }

   public void e(String var1) {
      this.e = var1;
   }

   public boolean equals(Object var1) {
      boolean var2;
      if((b)this == var1) {
         var2 = true;
      } else {
         Class var3;
         if(var1 != null) {
            var3 = var1.getClass();
         } else {
            var3 = null;
         }

         if(l.a(var3, this.getClass()) ^ true) {
            var2 = false;
         } else {
            if(var1 == null) {
               throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.payments.data.model.FpsSchemaData");
            }

            b var4 = (b)var1;
            if(l.a(this.e(), ((b)var1).e()) ^ true) {
               var2 = false;
            } else if(l.a(this.f(), ((b)var1).f()) ^ true) {
               var2 = false;
            } else if(l.a(this.g(), ((b)var1).g()) ^ true) {
               var2 = false;
            } else if(l.a(this.h(), ((b)var1).h()) ^ true) {
               var2 = false;
            } else if(l.a(this.i(), ((b)var1).i()) ^ true) {
               var2 = false;
            } else {
               var2 = true;
            }
         }
      }

      return var2;
   }

   public String f() {
      return this.c;
   }

   public void f(String var1) {
      this.f = var1;
   }

   public String g() {
      return this.d;
   }

   public String h() {
      return this.e;
   }

   public int hashCode() {
      return (((this.e().hashCode() * 31 + this.f().hashCode()) * 31 + this.g().hashCode()) * 31 + this.h().hashCode()) * 31 + this.i().hashCode();
   }

   public String i() {
      return this.f;
   }

   public void writeToParcel(Parcel var1, int var2) {
      l.b(var1, "dest");
      var1.writeString(this.e());
      var1.writeString(this.f());
      var1.writeString(this.g());
      var1.writeString(this.h());
      var1.writeString(this.i());
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"},
      d2 = {"Lco/uk/getmondo/payments/data/model/FpsSchemaData$Companion;", "", "()V", "CREATOR", "Landroid/os/Parcelable$Creator;", "Lco/uk/getmondo/payments/data/model/FpsSchemaData;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(i var1) {
         this();
      }
   }
}
