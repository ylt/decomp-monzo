package co.uk.getmondo.payments.a.a;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import io.realm.bc;
import io.realm.n;
import java.io.Serializable;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.d.b.i;
import kotlin.d.b.l;
import org.threeten.bp.LocalDateTime;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b$\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\b\u0016\u0018\u0000 A2\u00020\u00012\u00020\u0002:\u0001ABa\b\u0016\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0004\u0012\u0006\u0010\u0007\u001a\u00020\u0004\u0012\u0006\u0010\b\u001a\u00020\u0004\u0012\u0006\u0010\t\u001a\u00020\u0004\u0012\u0006\u0010\n\u001a\u00020\u0004\u0012\u0006\u0010\u000b\u001a\u00020\u0004\u0012\u0006\u0010\f\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\b\u0010\u0010\u001a\u0004\u0018\u00010\u000f¢\u0006\u0002\u0010\u0011B\u000f\b\u0016\u0012\u0006\u0010\u0012\u001a\u00020\u0013¢\u0006\u0002\u0010\u0014B\u0005¢\u0006\u0002\u0010\u0015J\b\u00107\u001a\u000208H\u0016J\u0013\u00109\u001a\u00020\r2\b\u0010:\u001a\u0004\u0018\u00010;H\u0096\u0002J\b\u0010<\u001a\u000208H\u0016J\u0018\u0010=\u001a\u00020>2\u0006\u0010?\u001a\u00020\u00132\u0006\u0010@\u001a\u000208H\u0016R\u000e\u0010\u0016\u001a\u00020\u0004X\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0017\u001a\u0004\u0018\u00010\u0004X\u0082\u000e¢\u0006\u0002\n\u0000R\u001a\u0010\u0005\u001a\u00020\u0004X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0018\u0010\u0019\"\u0004\b\u001a\u0010\u001bR\u001a\u0010\f\u001a\u00020\rX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u001c\u0010\u001d\"\u0004\b\u001e\u0010\u001fR$\u0010\u000e\u001a\u00020\u000f2\u0006\u0010 \u001a\u00020\u000f8F@FX\u0086\u000e¢\u0006\f\u001a\u0004\b!\u0010\"\"\u0004\b#\u0010$R\u0014\u0010%\u001a\u00020\u00048VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b&\u0010\u0019R\u001e\u0010\u0003\u001a\u00020\u00048\u0006@\u0006X\u0087\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b'\u0010\u0019\"\u0004\b(\u0010\u001bR(\u0010\u0010\u001a\u0004\u0018\u00010\u000f2\b\u0010 \u001a\u0004\u0018\u00010\u000f8F@FX\u0086\u000e¢\u0006\f\u001a\u0004\b)\u0010\"\"\u0004\b*\u0010$R\u001a\u0010\u0007\u001a\u00020\u0004X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b+\u0010\u0019\"\u0004\b,\u0010\u001bR\u001a\u0010\b\u001a\u00020\u0004X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b-\u0010\u0019\"\u0004\b.\u0010\u001bR\u001a\u0010\u0006\u001a\u00020\u0004X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b/\u0010\u0019\"\u0004\b0\u0010\u001bR\u001a\u0010\u000b\u001a\u00020\u0004X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b1\u0010\u0019\"\u0004\b2\u0010\u001bR\u001a\u0010\n\u001a\u00020\u0004X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b3\u0010\u0019\"\u0004\b4\u0010\u001bR\u001a\u0010\t\u001a\u00020\u0004X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b5\u0010\u0019\"\u0004\b6\u0010\u001b¨\u0006B"},
   d2 = {"Lco/uk/getmondo/payments/data/model/DirectDebit;", "Lio/realm/RealmObject;", "Lco/uk/getmondo/payments/data/model/RecurringPayment;", "id", "", "accountId", "payerSortCode", "payerAccountNumber", "payerName", "serviceUserNumber", "serviceUserName", "reference", "active", "", "created", "Lorg/threeten/bp/LocalDateTime;", "lastCollected", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/LocalDateTime;)V", "source", "Landroid/os/Parcel;", "(Landroid/os/Parcel;)V", "()V", "_created", "_lastCollected", "getAccountId", "()Ljava/lang/String;", "setAccountId", "(Ljava/lang/String;)V", "getActive", "()Z", "setActive", "(Z)V", "value", "getCreated", "()Lorg/threeten/bp/LocalDateTime;", "setCreated", "(Lorg/threeten/bp/LocalDateTime;)V", "displayName", "getDisplayName", "getId", "setId", "getLastCollected", "setLastCollected", "getPayerAccountNumber", "setPayerAccountNumber", "getPayerName", "setPayerName", "getPayerSortCode", "setPayerSortCode", "getReference", "setReference", "getServiceUserName", "setServiceUserName", "getServiceUserNumber", "setServiceUserNumber", "describeContents", "", "equals", "other", "", "hashCode", "writeToParcel", "", "dest", "flags", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public class a extends bc implements f, n {
   public static final Creator CREATOR = (Creator)(new Creator() {
      public a a(Parcel var1) {
         l.b(var1, "source");
         return new a(var1);
      }

      public a[] a(int var1) {
         return new a[var1];
      }

      // $FF: synthetic method
      public Object createFromParcel(Parcel var1) {
         return this.a(var1);
      }

      // $FF: synthetic method
      public Object[] newArray(int var1) {
         return (Object[])this.a(var1);
      }
   });
   public static final a.a a = new a.a((i)null);
   private String b;
   private String c;
   private String d;
   private String e;
   private String f;
   private String g;
   private String h;
   private String i;
   private boolean j;
   private String k;
   private String l;

   public a() {
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

      this.a("");
      this.b("");
      this.c("");
      this.d("");
      this.e("");
      this.f("");
      this.g("");
      this.h("");
      this.i("");
   }

   public a(Parcel var1) {
      boolean var2 = true;
      l.b(var1, "source");
      String var5 = var1.readString();
      l.a(var5, "source.readString()");
      String var10 = var1.readString();
      l.a(var10, "source.readString()");
      String var9 = var1.readString();
      l.a(var9, "source.readString()");
      String var3 = var1.readString();
      l.a(var3, "source.readString()");
      String var6 = var1.readString();
      l.a(var6, "source.readString()");
      String var8 = var1.readString();
      l.a(var8, "source.readString()");
      String var4 = var1.readString();
      l.a(var4, "source.readString()");
      String var11 = var1.readString();
      l.a(var11, "source.readString()");
      if(var1.readInt() != 1) {
         var2 = false;
      }

      Serializable var7 = var1.readSerializable();
      if(var7 == null) {
         throw new TypeCastException("null cannot be cast to non-null type org.threeten.bp.LocalDateTime");
      } else {
         this(var5, var10, var9, var3, var6, var8, var4, var11, var2, (LocalDateTime)var7, (LocalDateTime)var1.readSerializable());
         if(this instanceof io.realm.internal.l) {
            ((io.realm.internal.l)this).u_();
         }

      }
   }

   public a(String var1, String var2, String var3, String var4, String var5, String var6, String var7, String var8, boolean var9, LocalDateTime var10, LocalDateTime var11) {
      l.b(var1, "id");
      l.b(var2, "accountId");
      l.b(var3, "payerSortCode");
      l.b(var4, "payerAccountNumber");
      l.b(var5, "payerName");
      l.b(var6, "serviceUserNumber");
      l.b(var7, "serviceUserName");
      l.b(var8, "reference");
      l.b(var10, "created");
      this();
      if(this instanceof io.realm.internal.l) {
         ((io.realm.internal.l)this).u_();
      }

      this.a(var1);
      this.b(var2);
      this.c(var3);
      this.d(var4);
      this.e(var5);
      this.f(var6);
      this.g(var7);
      this.h(var8);
      this.a(var9);
      this.a(var10);
      this.b(var11);
   }

   public final String a() {
      return this.e();
   }

   public void a(String var1) {
      this.b = var1;
   }

   public final void a(LocalDateTime var1) {
      l.b(var1, "value");
      String var2 = var1.toString();
      l.a(var2, "value.toString()");
      this.i(var2);
   }

   public void a(boolean var1) {
      this.j = var1;
   }

   public final LocalDateTime b() {
      LocalDateTime var1 = LocalDateTime.a((CharSequence)this.n());
      l.a(var1, "LocalDateTime.parse(_created)");
      return var1;
   }

   public void b(String var1) {
      this.c = var1;
   }

   public final void b(LocalDateTime var1) {
      String var2;
      if(var1 != null) {
         var2 = var1.toString();
      } else {
         var2 = null;
      }

      this.j(var2);
   }

   public final LocalDateTime c() {
      LocalDateTime var1;
      if(this.o() != null) {
         var1 = LocalDateTime.a((CharSequence)this.o());
      } else {
         var1 = null;
      }

      return var1;
   }

   public void c(String var1) {
      this.d = var1;
   }

   public String d() {
      return this.t_();
   }

   public void d(String var1) {
      this.e = var1;
   }

   public int describeContents() {
      return 0;
   }

   public String e() {
      return this.b;
   }

   public void e(String var1) {
      this.f = var1;
   }

   public boolean equals(Object var1) {
      boolean var2;
      if((a)this == var1) {
         var2 = true;
      } else {
         Class var3;
         if(var1 != null) {
            var3 = var1.getClass();
         } else {
            var3 = null;
         }

         if(l.a(var3, this.getClass()) ^ true) {
            var2 = false;
         } else {
            if(var1 == null) {
               throw new TypeCastException("null cannot be cast to non-null type co.uk.getmondo.payments.data.model.DirectDebit");
            }

            a var4 = (a)var1;
            if(l.a(this.e(), ((a)var1).e()) ^ true) {
               var2 = false;
            } else if(l.a(this.f(), ((a)var1).f()) ^ true) {
               var2 = false;
            } else if(l.a(this.g(), ((a)var1).g()) ^ true) {
               var2 = false;
            } else if(l.a(this.h(), ((a)var1).h()) ^ true) {
               var2 = false;
            } else if(l.a(this.i(), ((a)var1).i()) ^ true) {
               var2 = false;
            } else if(l.a(this.j(), ((a)var1).j()) ^ true) {
               var2 = false;
            } else if(l.a(this.t_(), ((a)var1).t_()) ^ true) {
               var2 = false;
            } else if(l.a(this.l(), ((a)var1).l()) ^ true) {
               var2 = false;
            } else if(this.m() != ((a)var1).m()) {
               var2 = false;
            } else if(l.a(this.n(), ((a)var1).n()) ^ true) {
               var2 = false;
            } else if(l.a(this.o(), ((a)var1).o()) ^ true) {
               var2 = false;
            } else {
               var2 = true;
            }
         }
      }

      return var2;
   }

   public String f() {
      return this.c;
   }

   public void f(String var1) {
      this.g = var1;
   }

   public String g() {
      return this.d;
   }

   public void g(String var1) {
      this.h = var1;
   }

   public String h() {
      return this.e;
   }

   public void h(String var1) {
      this.i = var1;
   }

   public int hashCode() {
      int var9 = this.e().hashCode();
      int var5 = this.f().hashCode();
      int var8 = this.g().hashCode();
      int var10 = this.h().hashCode();
      int var3 = this.i().hashCode();
      int var11 = this.j().hashCode();
      int var7 = this.t_().hashCode();
      int var4 = this.l().hashCode();
      int var6 = Boolean.valueOf(this.m()).hashCode();
      int var2 = this.n().hashCode();
      String var12 = this.o();
      int var1;
      if(var12 != null) {
         var1 = var12.hashCode();
      } else {
         var1 = 0;
      }

      return var1 + (((((((((var9 * 31 + var5) * 31 + var8) * 31 + var10) * 31 + var3) * 31 + var11) * 31 + var7) * 31 + var4) * 31 + var6) * 31 + var2) * 31;
   }

   public String i() {
      return this.f;
   }

   public void i(String var1) {
      this.k = var1;
   }

   public String j() {
      return this.g;
   }

   public void j(String var1) {
      this.l = var1;
   }

   public String l() {
      return this.i;
   }

   public boolean m() {
      return this.j;
   }

   public String n() {
      return this.k;
   }

   public String o() {
      return this.l;
   }

   public String t_() {
      return this.h;
   }

   public void writeToParcel(Parcel var1, int var2) {
      l.b(var1, "dest");
      var1.writeString(this.e());
      var1.writeString(this.f());
      var1.writeString(this.g());
      var1.writeString(this.h());
      var1.writeString(this.i());
      var1.writeString(this.j());
      var1.writeString(this.t_());
      var1.writeString(this.l());
      byte var3;
      if(this.m()) {
         var3 = 1;
      } else {
         var3 = 0;
      }

      var1.writeInt(var3);
      var1.writeSerializable((Serializable)this.b());
      var1.writeSerializable((Serializable)this.c());
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"},
      d2 = {"Lco/uk/getmondo/payments/data/model/DirectDebit$Companion;", "", "()V", "CREATOR", "Landroid/os/Parcelable$Creator;", "Lco/uk/getmondo/payments/data/model/DirectDebit;", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(i var1) {
         this();
      }
   }
}
