package co.uk.getmondo.payments.a;

import co.uk.getmondo.api.model.payments.ApiPaymentSeries;
import kotlin.Metadata;
import kotlin.d.b.l;
import org.threeten.bp.LocalDate;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\bÆ\u0002\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0002H\u0016¨\u0006\u0007"},
   d2 = {"Lco/uk/getmondo/payments/data/PaymentSeriesMapper;", "Lco/uk/getmondo/model/mapper/Mapper;", "Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;", "Lco/uk/getmondo/payments/data/model/PaymentSeries;", "()V", "apply", "apiValue", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class e implements co.uk.getmondo.d.a.j {
   public static final e a;

   static {
      new e();
   }

   private e() {
      a = (e)this;
   }

   public co.uk.getmondo.payments.a.a.e a(ApiPaymentSeries var1) {
      l.b(var1, "apiValue");
      co.uk.getmondo.payments.a.a.b var2 = d.a.a(var1.f());
      var2.a(var1.a());
      LocalDate var5 = var1.g();
      co.uk.getmondo.payments.a.a.d.c var3 = co.uk.getmondo.payments.a.a.d.c.f.a(var1.h());
      LocalDate var4 = var1.i();
      co.uk.getmondo.payments.a.a.d var6 = new co.uk.getmondo.payments.a.a.d(var5, var3, var1.j(), var4);
      return new co.uk.getmondo.payments.a.a.e(var1.a(), var1.b(), var1.e(), var2, new co.uk.getmondo.d.c(var1.c(), var1.d()), var6);
   }
}
