package co.uk.getmondo.payments.a;

import co.uk.getmondo.api.PaymentsApi;
import io.reactivex.n;
import io.reactivex.r;
import io.reactivex.v;
import java.util.List;
import kotlin.Metadata;
import kotlin.a.m;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u0000`\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0007\u0018\u0000 '2\u00020\u0001:\u0001'B'\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t¢\u0006\u0002\u0010\nJ\u0012\u0010\u000b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000e0\r0\fJ\u0012\u0010\u000f\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00100\r0\fJ\u0012\u0010\u0011\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00120\r0\fJ&\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00162\u0006\u0010\u0018\u001a\u00020\u00162\u0006\u0010\u0019\u001a\u00020\u0016J&\u0010\u001a\u001a\u00020\u00142\u0006\u0010\u001b\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00162\u0006\u0010\u0018\u001a\u00020\u00162\u0006\u0010\u0019\u001a\u00020\u0016J\u001e\u0010\u001c\u001a\u00020\u00142\u0006\u0010\u001d\u001a\u00020\u00122\u0006\u0010\u0018\u001a\u00020\u00162\u0006\u0010\u0019\u001a\u00020\u0016J\u0018\u0010\u001e\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020 \u0012\u0004\u0012\u00020 0\u001f0\fJ\u001e\u0010!\u001a\u00020\u00142\u0006\u0010\"\u001a\u00020#2\u0006\u0010\u0018\u001a\u00020\u00162\u0006\u0010\u0019\u001a\u00020\u0016J\u0006\u0010$\u001a\u00020\u0014J\u0006\u0010%\u001a\u00020\u0014J\u0006\u0010&\u001a\u00020\u0014R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006("},
   d2 = {"Lco/uk/getmondo/payments/data/RecurringPaymentsManager;", "", "paymentSeriesStorage", "Lco/uk/getmondo/payments/data/PaymentSeriesStorage;", "directDebitsStorage", "Lco/uk/getmondo/payments/data/DirectDebitsStorage;", "paymentsApi", "Lco/uk/getmondo/api/PaymentsApi;", "accountService", "Lco/uk/getmondo/common/accounts/AccountService;", "(Lco/uk/getmondo/payments/data/PaymentSeriesStorage;Lco/uk/getmondo/payments/data/DirectDebitsStorage;Lco/uk/getmondo/api/PaymentsApi;Lco/uk/getmondo/common/accounts/AccountService;)V", "allDirectDebits", "Lio/reactivex/Observable;", "", "Lco/uk/getmondo/payments/data/model/DirectDebit;", "allPaymentSeries", "Lco/uk/getmondo/payments/data/model/PaymentSeries;", "allRecurringPayments", "Lco/uk/getmondo/payments/data/model/RecurringPayment;", "cancelDirectDebit", "Lio/reactivex/Completable;", "directDebitId", "", "accountId", "pin", "idempotencyKey", "cancelPaymentSeries", "paymentSeriesId", "cancelRecurringPayment", "recurringPayment", "counts", "Lkotlin/Pair;", "", "scheduleBankPayment", "bankPayment", "Lco/uk/getmondo/payments/send/data/model/BankPayment;", "syncAll", "syncDirectDebits", "syncPaymentSeries", "Companion", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class i {
   public static final i.a a = new i.a((kotlin.d.b.i)null);
   private final f b;
   private final b c;
   private final PaymentsApi d;
   private final co.uk.getmondo.common.accounts.d e;

   public i(f var1, b var2, PaymentsApi var3, co.uk.getmondo.common.accounts.d var4) {
      l.b(var1, "paymentSeriesStorage");
      l.b(var2, "directDebitsStorage");
      l.b(var3, "paymentsApi");
      l.b(var4, "accountService");
      super();
      this.b = var1;
      this.c = var2;
      this.d = var3;
      this.e = var4;
   }

   public final io.reactivex.b a() {
      io.reactivex.b var1 = this.e.d().a((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final v a(String var1) {
            l.b(var1, "accountId");
            return i.this.d.scheduledPaymentsSeries(var1);
         }
      })).d((io.reactivex.c.h)null.a).c((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.b a(List var1) {
            l.b(var1, "it");
            return i.this.b.a(var1);
         }
      }));
      l.a(var1, "accountService.accountId…riesStorage.saveAll(it) }");
      return var1;
   }

   public final io.reactivex.b a(final co.uk.getmondo.payments.a.a.f var1, final String var2, final String var3) {
      l.b(var1, "recurringPayment");
      l.b(var2, "pin");
      l.b(var3, "idempotencyKey");
      io.reactivex.b var4 = this.e.d().c((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.d a(String var1x) {
            l.b(var1x, "accountId");
            co.uk.getmondo.payments.a.a.f var2x = var1;
            io.reactivex.d var3x;
            if(var2x instanceof co.uk.getmondo.payments.a.a.a) {
               var3x = (io.reactivex.d)i.this.b(((co.uk.getmondo.payments.a.a.a)var1).a(), var1x, var2, var3);
            } else if(var2x instanceof co.uk.getmondo.payments.a.a.e) {
               var3x = (io.reactivex.d)i.this.a(((co.uk.getmondo.payments.a.a.e)var1).a(), var1x, var2, var3);
            } else {
               var3x = (io.reactivex.d)io.reactivex.b.a((Throwable)(new IllegalArgumentException("Unsupported payment type " + var1.getClass())));
            }

            return var3x;
         }
      }));
      l.a(var4, "accountService.accountId…      }\n                }");
      return var4;
   }

   public final io.reactivex.b a(final co.uk.getmondo.payments.send.data.a.c var1, final String var2, final String var3) {
      l.b(var1, "bankPayment");
      l.b(var2, "pin");
      l.b(var3, "idempotencyKey");
      io.reactivex.b var4 = this.e.d().a((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final v a(String var1x) {
            l.b(var1x, "accountId");
            if(var1.e() == null) {
               v.a((Throwable)(new IllegalStateException("Payment must contain scheduling data " + var1)));
            }

            PaymentsApi var9 = i.this.d;
            long var2x = var1.a().k();
            String var7 = var1.a().l().b();
            l.a(var7, "bankPayment.amount.currency.currencyCode");
            String var6 = var1.c().e();
            String var12 = var1.c().d();
            String var4 = var1.c().b();
            String var5 = var3;
            String var11 = var2;
            String var8 = var1.b();
            co.uk.getmondo.payments.a.a.d var10 = var1.e();
            if(var10 == null) {
               l.a();
            }

            return var9.scheduleBankPayment(var1x, var2x, var7, var6, var12, var4, var5, "pin", var11, var8, var10.b(), var1.e().c().a(), var1.e().d());
         }
      })).d((io.reactivex.c.h)null.a).c((io.reactivex.c.g)(new io.reactivex.c.g() {
         public final void a(co.uk.getmondo.payments.a.a.e var1) {
            f var2 = i.this.b;
            l.a(var1, "it");
            var2.a(var1);
         }
      })).c();
      l.a(var4, "accountService.accountId…         .toCompletable()");
      return var4;
   }

   public final io.reactivex.b a(String var1, String var2, String var3, String var4) {
      l.b(var1, "paymentSeriesId");
      l.b(var2, "accountId");
      l.b(var3, "pin");
      l.b(var4, "idempotencyKey");
      io.reactivex.b var5 = this.d.cancelScheduledPaymentSeries(var1, var2, "pin", var3, var4).b((io.reactivex.d)this.b.a(var1));
      l.a(var5, "paymentsApi\n            ….delete(paymentSeriesId))");
      return var5;
   }

   public final io.reactivex.b b() {
      io.reactivex.b var1 = this.e.d().a((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final v a(String var1) {
            l.b(var1, "accountId");
            return i.this.d.bacsDirectDebits(var1);
         }
      })).d((io.reactivex.c.h)null.a).c((io.reactivex.c.h)(new io.reactivex.c.h() {
         public final io.reactivex.b a(List var1) {
            l.b(var1, "it");
            return i.this.c.a(var1);
         }
      }));
      l.a(var1, "accountService.accountId…bitsStorage.saveAll(it) }");
      return var1;
   }

   public final io.reactivex.b b(String var1, String var2, String var3, String var4) {
      l.b(var1, "directDebitId");
      l.b(var2, "accountId");
      l.b(var3, "pin");
      l.b(var4, "idempotencyKey");
      io.reactivex.b var5 = this.d.cancelBacsDirectDebit(var1, var2, "pin", var3, var4).b((io.reactivex.d)this.c.a(var1));
      l.a(var5, "paymentsApi.cancelBacsDi…ge.delete(directDebitId))");
      return var5;
   }

   public final io.reactivex.b c() {
      io.reactivex.b var1 = io.reactivex.b.a((Iterable)m.b(new io.reactivex.b[]{this.a(), this.b()}));
      l.a(var1, "Completable.mergeDelayEr…s(), syncDirectDebits()))");
      return var1;
   }

   public final n d() {
      n var1 = this.b.a().map((io.reactivex.c.h)null.a);
      l.a(var1, "paymentSeriesStorage.all…      .map { it.results }");
      return var1;
   }

   public final n e() {
      n var1 = this.c.a().map((io.reactivex.c.h)null.a);
      l.a(var1, "directDebitsStorage.allD…      .map { it.results }");
      return var1;
   }

   public final n f() {
      n var1 = n.combineLatest((r)this.d(), (r)this.e(), (io.reactivex.c.c)null.a);
      l.a(var1, "Observable.combineLatest…hat makes sense\n        )");
      return var1;
   }

   public final n g() {
      return co.uk.getmondo.common.j.f.a(this.c.b(), this.b.b());
   }

   @Metadata(
      bv = {1, 0, 2},
      d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000¨\u0006\u0005"},
      d2 = {"Lco/uk/getmondo/payments/data/RecurringPaymentsManager$Companion;", "", "()V", "CHALLENGE_TYPE_PIN", "", "app_monzoPrepaidRelease"},
      k = 1,
      mv = {1, 1, 7}
   )
   public static final class a {
      private a() {
      }

      // $FF: synthetic method
      public a(kotlin.d.b.i var1) {
         this();
      }
   }
}
