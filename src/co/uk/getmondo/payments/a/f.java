package co.uk.getmondo.payments.a;

import io.reactivex.n;
import io.realm.av;
import io.realm.bb;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.a.m;
import kotlin.d.b.l;

@Metadata(
   bv = {1, 0, 2},
   d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010 \n\u0000\b\u0007\u0018\u00002\u00020\u0001B\u0007\b\u0007¢\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\u0004J\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\b0\u0004J\u000e\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fJ\u000e\u0010\r\u001a\u00020\n2\u0006\u0010\u000e\u001a\u00020\u0006J\u0014\u0010\u000f\u001a\u00020\n2\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00060\u0010¨\u0006\u0011"},
   d2 = {"Lco/uk/getmondo/payments/data/PaymentSeriesStorage;", "", "()V", "allPaymentSeries", "Lio/reactivex/Observable;", "Lco/uk/getmondo/common/data/QueryResults;", "Lco/uk/getmondo/payments/data/model/PaymentSeries;", "count", "", "delete", "Lio/reactivex/Completable;", "paymentSeriesId", "", "save", "paymentSeries", "saveAll", "", "app_monzoPrepaidRelease"},
   k = 1,
   mv = {1, 1, 7}
)
public final class f {
   public final io.reactivex.b a(final co.uk.getmondo.payments.a.a.e var1) {
      l.b(var1, "paymentSeries");
      return co.uk.getmondo.common.j.g.a((av.a)(new av.a() {
         public final void a(av var1x) {
            var1x.d((bb)var1);
         }
      }));
   }

   public final io.reactivex.b a(final String var1) {
      l.b(var1, "paymentSeriesId");
      return co.uk.getmondo.common.j.g.a((av.a)(new av.a() {
         public final void a(av var1x) {
            var1x.a(co.uk.getmondo.payments.a.a.e.class).a("id", var1).f().b();
         }
      }));
   }

   public final io.reactivex.b a(final List var1) {
      l.b(var1, "allPaymentSeries");
      return co.uk.getmondo.common.j.g.a((av.a)(new av.a() {
         public final void a(av var1x) {
            if(var1.isEmpty()) {
               var1x.a(co.uk.getmondo.payments.a.a.e.class).f().b();
            } else {
               var1x.a((Collection)var1);
               Iterable var3 = (Iterable)var1;
               Collection var2 = (Collection)(new ArrayList(m.a(var3, 10)));
               Iterator var6 = var3.iterator();

               while(var6.hasNext()) {
                  var2.add(((co.uk.getmondo.payments.a.a.e)var6.next()).a());
               }

               var2 = (Collection)((List)var2);
               Object[] var4 = var2.toArray(new String[var2.size()]);
               if(var4 == null) {
                  throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
               }

               String[] var5 = (String[])var4;
               var1x.a(co.uk.getmondo.payments.a.a.e.class).d().a("id", var5).f().b();
            }

         }
      }));
   }

   public final n a() {
      n var1 = co.uk.getmondo.common.j.g.a((kotlin.d.a.b)null.a).map((io.reactivex.c.h)null.a);
      l.a(var1, "RxRealm.asObservable { r… .map { it.queryResults }");
      return var1;
   }

   public final n b() {
      n var1 = this.a().map((io.reactivex.c.h)null.a).distinctUntilChanged();
      l.a(var1, "allPaymentSeries()\n     …  .distinctUntilChanged()");
      return var1;
   }
}
