package okhttp3;

import c.f;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.Nullable;
import javax.net.ssl.SSLPeerUnverifiedException;
import okhttp3.internal.Util;
import okhttp3.internal.tls.CertificateChainCleaner;

public final class CertificatePinner {
   public static final CertificatePinner DEFAULT = (new CertificatePinner.Builder()).build();
   @Nullable
   private final CertificateChainCleaner certificateChainCleaner;
   private final Set pins;

   CertificatePinner(Set var1, @Nullable CertificateChainCleaner var2) {
      this.pins = var1;
      this.certificateChainCleaner = var2;
   }

   public static String pin(Certificate var0) {
      if(!(var0 instanceof X509Certificate)) {
         throw new IllegalArgumentException("Certificate pinning requires X509 certificates");
      } else {
         return "sha256/" + sha256((X509Certificate)var0).b();
      }
   }

   static f sha1(X509Certificate var0) {
      return f.a(var0.getPublicKey().getEncoded()).d();
   }

   static f sha256(X509Certificate var0) {
      return f.a(var0.getPublicKey().getEncoded()).e();
   }

   public void check(String var1, List var2) throws SSLPeerUnverifiedException {
      List var10 = this.findMatchingPins(var1);
      if(!var10.isEmpty()) {
         List var9 = var2;
         if(this.certificateChainCleaner != null) {
            var9 = this.certificateChainCleaner.clean(var2, var1);
         }

         int var5 = var9.size();

         int var3;
         int var4;
         for(var3 = 0; var3 < var5; ++var3) {
            X509Certificate var11 = (X509Certificate)var9.get(var3);
            int var6 = var10.size();
            var4 = 0;
            f var7 = null;

            for(f var14 = null; var4 < var6; ++var4) {
               CertificatePinner.Pin var12 = (CertificatePinner.Pin)var10.get(var4);
               f var8;
               if(var12.hashAlgorithm.equals("sha256/")) {
                  var8 = var7;
                  if(var7 == null) {
                     var8 = sha256(var11);
                  }

                  if(var12.hash.equals(var8)) {
                     return;
                  }

                  var7 = var8;
               } else {
                  if(!var12.hashAlgorithm.equals("sha1/")) {
                     throw new AssertionError();
                  }

                  var8 = var14;
                  if(var14 == null) {
                     var8 = sha1(var11);
                  }

                  var14 = var8;
                  if(var12.hash.equals(var8)) {
                     return;
                  }
               }
            }
         }

         StringBuilder var15 = (new StringBuilder()).append("Certificate pinning failure!").append("\n  Peer certificate chain:");
         var4 = var9.size();

         for(var3 = 0; var3 < var4; ++var3) {
            X509Certificate var16 = (X509Certificate)var9.get(var3);
            var15.append("\n    ").append(pin(var16)).append(": ").append(var16.getSubjectDN().getName());
         }

         var15.append("\n  Pinned certificates for ").append(var1).append(":");
         var4 = var10.size();

         for(var3 = 0; var3 < var4; ++var3) {
            CertificatePinner.Pin var13 = (CertificatePinner.Pin)var10.get(var3);
            var15.append("\n    ").append(var13);
         }

         throw new SSLPeerUnverifiedException(var15.toString());
      }
   }

   public void check(String var1, Certificate... var2) throws SSLPeerUnverifiedException {
      this.check(var1, Arrays.asList(var2));
   }

   public boolean equals(@Nullable Object var1) {
      boolean var2 = true;
      if(var1 != this) {
         if(var1 instanceof CertificatePinner && Util.equal(this.certificateChainCleaner, ((CertificatePinner)var1).certificateChainCleaner) && this.pins.equals(((CertificatePinner)var1).pins)) {
            var2 = true;
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   List findMatchingPins(String var1) {
      Object var3 = Collections.emptyList();
      Iterator var4 = this.pins.iterator();

      while(var4.hasNext()) {
         CertificatePinner.Pin var5 = (CertificatePinner.Pin)var4.next();
         if(var5.matches(var1)) {
            Object var2 = var3;
            if(((List)var3).isEmpty()) {
               var2 = new ArrayList();
            }

            ((List)var2).add(var5);
            var3 = var2;
         }
      }

      return (List)var3;
   }

   public int hashCode() {
      int var1;
      if(this.certificateChainCleaner != null) {
         var1 = this.certificateChainCleaner.hashCode();
      } else {
         var1 = 0;
      }

      return var1 * 31 + this.pins.hashCode();
   }

   CertificatePinner withCertificateChainCleaner(CertificateChainCleaner var1) {
      CertificatePinner var2;
      if(Util.equal(this.certificateChainCleaner, var1)) {
         var2 = this;
      } else {
         var2 = new CertificatePinner(this.pins, var1);
      }

      return var2;
   }

   public static final class Builder {
      private final List pins = new ArrayList();

      public CertificatePinner.Builder add(String var1, String... var2) {
         if(var1 == null) {
            throw new NullPointerException("pattern == null");
         } else {
            int var4 = var2.length;

            for(int var3 = 0; var3 < var4; ++var3) {
               String var5 = var2[var3];
               this.pins.add(new CertificatePinner.Pin(var1, var5));
            }

            return this;
         }
      }

      public CertificatePinner build() {
         return new CertificatePinner(new LinkedHashSet(this.pins), (CertificateChainCleaner)null);
      }
   }

   static final class Pin {
      private static final String WILDCARD = "*.";
      final String canonicalHostname;
      final f hash;
      final String hashAlgorithm;
      final String pattern;

      Pin(String var1, String var2) {
         this.pattern = var1;
         if(var1.startsWith("*.")) {
            var1 = HttpUrl.parse("http://" + var1.substring("*.".length())).host();
         } else {
            var1 = HttpUrl.parse("http://" + var1).host();
         }

         this.canonicalHostname = var1;
         if(var2.startsWith("sha1/")) {
            this.hashAlgorithm = "sha1/";
            this.hash = f.b(var2.substring("sha1/".length()));
         } else {
            if(!var2.startsWith("sha256/")) {
               throw new IllegalArgumentException("pins must start with 'sha256/' or 'sha1/': " + var2);
            }

            this.hashAlgorithm = "sha256/";
            this.hash = f.b(var2.substring("sha256/".length()));
         }

         if(this.hash == null) {
            throw new IllegalArgumentException("pins must be base64: " + var2);
         }
      }

      public boolean equals(Object var1) {
         boolean var2;
         if(var1 instanceof CertificatePinner.Pin && this.pattern.equals(((CertificatePinner.Pin)var1).pattern) && this.hashAlgorithm.equals(((CertificatePinner.Pin)var1).hashAlgorithm) && this.hash.equals(((CertificatePinner.Pin)var1).hash)) {
            var2 = true;
         } else {
            var2 = false;
         }

         return var2;
      }

      public int hashCode() {
         return ((this.pattern.hashCode() + 527) * 31 + this.hashAlgorithm.hashCode()) * 31 + this.hash.hashCode();
      }

      boolean matches(String var1) {
         boolean var2;
         if(this.pattern.startsWith("*.")) {
            var2 = var1.regionMatches(false, var1.indexOf(46) + 1, this.canonicalHostname, 0, this.canonicalHostname.length());
         } else {
            var2 = var1.equals(this.canonicalHostname);
         }

         return var2;
      }

      public String toString() {
         return this.hashAlgorithm + this.hash.b();
      }
   }
}
