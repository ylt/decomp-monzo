package okhttp3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.Nullable;
import okhttp3.internal.Util;
import okhttp3.internal.http.HttpDate;
import okhttp3.internal.publicsuffix.PublicSuffixDatabase;

public final class Cookie {
   private static final Pattern DAY_OF_MONTH_PATTERN = Pattern.compile("(\\d{1,2})[^\\d]*");
   private static final Pattern MONTH_PATTERN = Pattern.compile("(?i)(jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec).*");
   private static final Pattern TIME_PATTERN = Pattern.compile("(\\d{1,2}):(\\d{1,2}):(\\d{1,2})[^\\d]*");
   private static final Pattern YEAR_PATTERN = Pattern.compile("(\\d{2,4})[^\\d]*");
   private final String domain;
   private final long expiresAt;
   private final boolean hostOnly;
   private final boolean httpOnly;
   private final String name;
   private final String path;
   private final boolean persistent;
   private final boolean secure;
   private final String value;

   private Cookie(String var1, String var2, long var3, String var5, String var6, boolean var7, boolean var8, boolean var9, boolean var10) {
      this.name = var1;
      this.value = var2;
      this.expiresAt = var3;
      this.domain = var5;
      this.path = var6;
      this.secure = var7;
      this.httpOnly = var8;
      this.hostOnly = var9;
      this.persistent = var10;
   }

   Cookie(Cookie.Builder var1) {
      if(var1.name == null) {
         throw new NullPointerException("builder.name == null");
      } else if(var1.value == null) {
         throw new NullPointerException("builder.value == null");
      } else if(var1.domain == null) {
         throw new NullPointerException("builder.domain == null");
      } else {
         this.name = var1.name;
         this.value = var1.value;
         this.expiresAt = var1.expiresAt;
         this.domain = var1.domain;
         this.path = var1.path;
         this.secure = var1.secure;
         this.httpOnly = var1.httpOnly;
         this.persistent = var1.persistent;
         this.hostOnly = var1.hostOnly;
      }
   }

   private static int dateCharacterOffset(String var0, int var1, int var2, boolean var3) {
      while(true) {
         int var4 = var2;
         if(var1 < var2) {
            char var6 = var0.charAt(var1);
            boolean var7;
            if((var6 >= 32 || var6 == 9) && var6 < 127 && (var6 < 48 || var6 > 57) && (var6 < 97 || var6 > 122) && (var6 < 65 || var6 > 90) && var6 != 58) {
               var7 = false;
            } else {
               var7 = true;
            }

            boolean var5;
            if(!var3) {
               var5 = true;
            } else {
               var5 = false;
            }

            if(var7 != var5) {
               ++var1;
               continue;
            }

            var4 = var1;
         }

         return var4;
      }
   }

   private static boolean domainMatch(String var0, String var1) {
      boolean var2 = true;
      if(!var0.equals(var1) && (!var0.endsWith(var1) || var0.charAt(var0.length() - var1.length() - 1) != 46 || Util.verifyAsIpAddress(var0))) {
         var2 = false;
      }

      return var2;
   }

   @Nullable
   static Cookie parse(long var0, HttpUrl var2, String var3) {
      int var5 = var3.length();
      int var4 = Util.delimiterOffset(var3, 0, var5, ';');
      int var6 = Util.delimiterOffset(var3, 0, var4, '=');
      Cookie var27;
      if(var6 == var4) {
         var27 = null;
      } else {
         String var21 = Util.trimSubstring(var3, 0, var6);
         if(!var21.isEmpty() && Util.indexOfControlOrNonAscii(var21) == -1) {
            String var22 = Util.trimSubstring(var3, var6 + 1, var4);
            if(Util.indexOfControlOrNonAscii(var22) != -1) {
               var27 = null;
            } else {
               long var8 = 253402300799999L;
               long var10 = -1L;
               String var18 = null;
               String var19 = null;
               boolean var17 = false;
               boolean var16 = false;
               boolean var15 = true;
               boolean var14 = false;
               ++var4;

               int var7;
               String var20;
               for(; var4 < var5; var4 = var7 + 1) {
                  var7 = Util.delimiterOffset(var3, var4, var5, ';');
                  var6 = Util.delimiterOffset(var3, var4, var7, '=');
                  String var23 = Util.trimSubstring(var3, var4, var6);
                  if(var6 < var7) {
                     var20 = Util.trimSubstring(var3, var6 + 1, var7);
                  } else {
                     var20 = "";
                  }

                  long var12;
                  if(var23.equalsIgnoreCase("expires")) {
                     try {
                        var12 = parseExpires(var20, 0, var20.length());
                     } catch (IllegalArgumentException var24) {
                        continue;
                     }

                     var14 = true;
                     var8 = var12;
                  } else if(var23.equalsIgnoreCase("max-age")) {
                     try {
                        var12 = parseMaxAge(var20);
                     } catch (NumberFormatException var25) {
                        continue;
                     }

                     var10 = var12;
                     var14 = true;
                  } else if(var23.equalsIgnoreCase("domain")) {
                     try {
                        var20 = parseDomain(var20);
                     } catch (IllegalArgumentException var26) {
                        continue;
                     }

                     var18 = var20;
                     var15 = false;
                  } else if(var23.equalsIgnoreCase("path")) {
                     var19 = var20;
                  } else if(var23.equalsIgnoreCase("secure")) {
                     var17 = true;
                  } else if(var23.equalsIgnoreCase("httponly")) {
                     var16 = true;
                  }
               }

               if(var10 == Long.MIN_VALUE) {
                  var0 = Long.MIN_VALUE;
               } else if(var10 != -1L) {
                  label126: {
                     if(var10 <= 9223372036854775L) {
                        var8 = var10 * 1000L;
                     } else {
                        var8 = Long.MAX_VALUE;
                     }

                     var8 += var0;
                     if(var8 >= var0) {
                        var0 = var8;
                        if(var8 <= 253402300799999L) {
                           break label126;
                        }
                     }

                     var0 = 253402300799999L;
                  }
               } else {
                  var0 = var8;
               }

               var20 = var2.host();
               if(var18 == null) {
                  var3 = var20;
               } else {
                  var3 = var18;
                  if(!domainMatch(var20, var18)) {
                     var27 = null;
                     return var27;
                  }
               }

               if(var20.length() != var3.length() && PublicSuffixDatabase.get().getEffectiveTldPlusOne(var3) == null) {
                  var27 = null;
               } else {
                  String var28;
                  if(var19 != null && var19.startsWith("/")) {
                     var28 = var19;
                  } else {
                     var28 = var2.encodedPath();
                     var4 = var28.lastIndexOf(47);
                     if(var4 != 0) {
                        var28 = var28.substring(0, var4);
                     } else {
                        var28 = "/";
                     }
                  }

                  var27 = new Cookie(var21, var22, var0, var3, var28, var17, var16, var15, var14);
               }
            }
         } else {
            var27 = null;
         }
      }

      return var27;
   }

   @Nullable
   public static Cookie parse(HttpUrl var0, String var1) {
      return parse(System.currentTimeMillis(), var0, var1);
   }

   public static List parseAll(HttpUrl var0, Headers var1) {
      List var4 = var1.values("Set-Cookie");
      ArrayList var7 = null;
      int var3 = var4.size();

      for(int var2 = 0; var2 < var3; ++var2) {
         Cookie var5 = parse(var0, (String)var4.get(var2));
         if(var5 != null) {
            if(var7 == null) {
               var7 = new ArrayList();
            }

            var7.add(var5);
         }
      }

      List var6;
      if(var7 != null) {
         var6 = Collections.unmodifiableList(var7);
      } else {
         var6 = Collections.emptyList();
      }

      return var6;
   }

   private static String parseDomain(String var0) {
      if(var0.endsWith(".")) {
         throw new IllegalArgumentException();
      } else {
         String var1 = var0;
         if(var0.startsWith(".")) {
            var1 = var0.substring(1);
         }

         var0 = Util.domainToAscii(var1);
         if(var0 == null) {
            throw new IllegalArgumentException();
         } else {
            return var0;
         }
      }
   }

   private static long parseExpires(String var0, int var1, int var2) {
      int var8 = dateCharacterOffset(var0, var1, var2, false);
      int var3 = -1;
      int var5 = -1;
      int var7 = -1;
      int var4 = -1;
      int var6 = -1;
      var1 = -1;

      int var14;
      for(Matcher var16 = TIME_PATTERN.matcher(var0); var8 < var2; var8 = var14) {
         var14 = dateCharacterOffset(var0, var8 + 1, var2, true);
         var16.region(var8, var14);
         int var9;
         int var10;
         int var11;
         int var12;
         int var13;
         if(var3 == -1 && var16.usePattern(TIME_PATTERN).matches()) {
            var9 = Integer.parseInt(var16.group(1));
            var10 = Integer.parseInt(var16.group(2));
            var12 = Integer.parseInt(var16.group(3));
            var13 = var4;
            var11 = var6;
            var8 = var1;
         } else if(var4 == -1 && var16.usePattern(DAY_OF_MONTH_PATTERN).matches()) {
            var13 = Integer.parseInt(var16.group(1));
            var8 = var1;
            var11 = var6;
            var12 = var7;
            var10 = var5;
            var9 = var3;
         } else if(var6 == -1 && var16.usePattern(MONTH_PATTERN).matches()) {
            String var15 = var16.group(1).toLowerCase(Locale.US);
            var11 = MONTH_PATTERN.pattern().indexOf(var15) / 4;
            var8 = var1;
            var13 = var4;
            var12 = var7;
            var10 = var5;
            var9 = var3;
         } else {
            var8 = var1;
            var11 = var6;
            var13 = var4;
            var12 = var7;
            var10 = var5;
            var9 = var3;
            if(var1 == -1) {
               var8 = var1;
               var11 = var6;
               var13 = var4;
               var12 = var7;
               var10 = var5;
               var9 = var3;
               if(var16.usePattern(YEAR_PATTERN).matches()) {
                  var8 = Integer.parseInt(var16.group(1));
                  var11 = var6;
                  var13 = var4;
                  var12 = var7;
                  var10 = var5;
                  var9 = var3;
               }
            }
         }

         var14 = dateCharacterOffset(var0, var14 + 1, var2, false);
         var1 = var8;
         var6 = var11;
         var4 = var13;
         var7 = var12;
         var5 = var10;
         var3 = var9;
      }

      var2 = var1;
      if(var1 >= 70) {
         var2 = var1;
         if(var1 <= 99) {
            var2 = var1 + 1900;
         }
      }

      var1 = var2;
      if(var2 >= 0) {
         var1 = var2;
         if(var2 <= 69) {
            var1 = var2 + 2000;
         }
      }

      if(var1 < 1601) {
         throw new IllegalArgumentException();
      } else if(var6 == -1) {
         throw new IllegalArgumentException();
      } else if(var4 >= 1 && var4 <= 31) {
         if(var3 >= 0 && var3 <= 23) {
            if(var5 >= 0 && var5 <= 59) {
               if(var7 >= 0 && var7 <= 59) {
                  GregorianCalendar var17 = new GregorianCalendar(Util.UTC);
                  var17.setLenient(false);
                  var17.set(1, var1);
                  var17.set(2, var6 - 1);
                  var17.set(5, var4);
                  var17.set(11, var3);
                  var17.set(12, var5);
                  var17.set(13, var7);
                  var17.set(14, 0);
                  return var17.getTimeInMillis();
               } else {
                  throw new IllegalArgumentException();
               }
            } else {
               throw new IllegalArgumentException();
            }
         } else {
            throw new IllegalArgumentException();
         }
      } else {
         throw new IllegalArgumentException();
      }
   }

   private static long parseMaxAge(String var0) {
      long var3 = Long.MIN_VALUE;

      long var1;
      try {
         var1 = Long.parseLong(var0);
      } catch (NumberFormatException var6) {
         if(var0.matches("-?\\d+")) {
            var1 = var3;
            if(!var0.startsWith("-")) {
               var1 = Long.MAX_VALUE;
            }

            return var1;
         }

         throw var6;
      }

      if(var1 <= 0L) {
         var1 = var3;
      }

      return var1;
   }

   private static boolean pathMatch(HttpUrl var0, String var1) {
      boolean var3 = true;
      String var4 = var0.encodedPath();
      boolean var2;
      if(var4.equals(var1)) {
         var2 = var3;
      } else {
         if(var4.startsWith(var1)) {
            var2 = var3;
            if(var1.endsWith("/")) {
               return var2;
            }

            var2 = var3;
            if(var4.charAt(var1.length()) == 47) {
               return var2;
            }
         }

         var2 = false;
      }

      return var2;
   }

   public String domain() {
      return this.domain;
   }

   public boolean equals(@Nullable Object var1) {
      boolean var3 = false;
      boolean var2;
      if(!(var1 instanceof Cookie)) {
         var2 = var3;
      } else {
         Cookie var4 = (Cookie)var1;
         var2 = var3;
         if(var4.name.equals(this.name)) {
            var2 = var3;
            if(var4.value.equals(this.value)) {
               var2 = var3;
               if(var4.domain.equals(this.domain)) {
                  var2 = var3;
                  if(var4.path.equals(this.path)) {
                     var2 = var3;
                     if(var4.expiresAt == this.expiresAt) {
                        var2 = var3;
                        if(var4.secure == this.secure) {
                           var2 = var3;
                           if(var4.httpOnly == this.httpOnly) {
                              var2 = var3;
                              if(var4.persistent == this.persistent) {
                                 var2 = var3;
                                 if(var4.hostOnly == this.hostOnly) {
                                    var2 = true;
                                 }
                              }
                           }
                        }
                     }
                  }
               }
            }
         }
      }

      return var2;
   }

   public long expiresAt() {
      return this.expiresAt;
   }

   public int hashCode() {
      byte var4 = 0;
      int var9 = this.name.hashCode();
      int var5 = this.value.hashCode();
      int var8 = this.domain.hashCode();
      int var7 = this.path.hashCode();
      int var6 = (int)(this.expiresAt ^ this.expiresAt >>> 32);
      byte var1;
      if(this.secure) {
         var1 = 0;
      } else {
         var1 = 1;
      }

      byte var2;
      if(this.httpOnly) {
         var2 = 0;
      } else {
         var2 = 1;
      }

      byte var3;
      if(this.persistent) {
         var3 = 0;
      } else {
         var3 = 1;
      }

      if(!this.hostOnly) {
         var4 = 1;
      }

      return (var3 + (var2 + (var1 + (((((var9 + 527) * 31 + var5) * 31 + var8) * 31 + var7) * 31 + var6) * 31) * 31) * 31) * 31 + var4;
   }

   public boolean hostOnly() {
      return this.hostOnly;
   }

   public boolean httpOnly() {
      return this.httpOnly;
   }

   public boolean matches(HttpUrl var1) {
      boolean var3 = false;
      boolean var2;
      if(this.hostOnly) {
         var2 = var1.host().equals(this.domain);
      } else {
         var2 = domainMatch(var1.host(), this.domain);
      }

      if(!var2) {
         var2 = var3;
      } else {
         var2 = var3;
         if(pathMatch(var1, this.path)) {
            if(this.secure) {
               var2 = var3;
               if(!var1.isHttps()) {
                  return var2;
               }
            }

            var2 = true;
         }
      }

      return var2;
   }

   public String name() {
      return this.name;
   }

   public String path() {
      return this.path;
   }

   public boolean persistent() {
      return this.persistent;
   }

   public boolean secure() {
      return this.secure;
   }

   public String toString() {
      return this.toString(false);
   }

   String toString(boolean var1) {
      StringBuilder var2 = new StringBuilder();
      var2.append(this.name);
      var2.append('=');
      var2.append(this.value);
      if(this.persistent) {
         if(this.expiresAt == Long.MIN_VALUE) {
            var2.append("; max-age=0");
         } else {
            var2.append("; expires=").append(HttpDate.format(new Date(this.expiresAt)));
         }
      }

      if(!this.hostOnly) {
         var2.append("; domain=");
         if(var1) {
            var2.append(".");
         }

         var2.append(this.domain);
      }

      var2.append("; path=").append(this.path);
      if(this.secure) {
         var2.append("; secure");
      }

      if(this.httpOnly) {
         var2.append("; httponly");
      }

      return var2.toString();
   }

   public String value() {
      return this.value;
   }

   public static final class Builder {
      String domain;
      long expiresAt = 253402300799999L;
      boolean hostOnly;
      boolean httpOnly;
      String name;
      String path = "/";
      boolean persistent;
      boolean secure;
      String value;

      private Cookie.Builder domain(String var1, boolean var2) {
         if(var1 == null) {
            throw new NullPointerException("domain == null");
         } else {
            String var3 = Util.domainToAscii(var1);
            if(var3 == null) {
               throw new IllegalArgumentException("unexpected domain: " + var1);
            } else {
               this.domain = var3;
               this.hostOnly = var2;
               return this;
            }
         }
      }

      public Cookie build() {
         return new Cookie(this);
      }

      public Cookie.Builder domain(String var1) {
         return this.domain(var1, false);
      }

      public Cookie.Builder expiresAt(long var1) {
         long var3 = 253402300799999L;
         if(var1 <= 0L) {
            var1 = Long.MIN_VALUE;
         }

         if(var1 > 253402300799999L) {
            var1 = var3;
         }

         this.expiresAt = var1;
         this.persistent = true;
         return this;
      }

      public Cookie.Builder hostOnlyDomain(String var1) {
         return this.domain(var1, true);
      }

      public Cookie.Builder httpOnly() {
         this.httpOnly = true;
         return this;
      }

      public Cookie.Builder name(String var1) {
         if(var1 == null) {
            throw new NullPointerException("name == null");
         } else if(!var1.trim().equals(var1)) {
            throw new IllegalArgumentException("name is not trimmed");
         } else {
            this.name = var1;
            return this;
         }
      }

      public Cookie.Builder path(String var1) {
         if(!var1.startsWith("/")) {
            throw new IllegalArgumentException("path must start with '/'");
         } else {
            this.path = var1;
            return this;
         }
      }

      public Cookie.Builder secure() {
         this.secure = true;
         return this;
      }

      public Cookie.Builder value(String var1) {
         if(var1 == null) {
            throw new NullPointerException("value == null");
         } else if(!var1.trim().equals(var1)) {
            throw new IllegalArgumentException("value is not trimmed");
         } else {
            this.value = var1;
            return this;
         }
      }
   }
}
