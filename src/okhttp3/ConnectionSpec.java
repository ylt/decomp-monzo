package okhttp3;

import java.util.Arrays;
import java.util.List;
import javax.annotation.Nullable;
import javax.net.ssl.SSLSocket;
import okhttp3.internal.Util;

public final class ConnectionSpec {
   private static final CipherSuite[] APPROVED_CIPHER_SUITES;
   public static final ConnectionSpec CLEARTEXT;
   public static final ConnectionSpec COMPATIBLE_TLS;
   public static final ConnectionSpec MODERN_TLS;
   @Nullable
   final String[] cipherSuites;
   final boolean supportsTlsExtensions;
   final boolean tls;
   @Nullable
   final String[] tlsVersions;

   static {
      APPROVED_CIPHER_SUITES = new CipherSuite[]{CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256, CipherSuite.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256, CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384, CipherSuite.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384, CipherSuite.TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305_SHA256, CipherSuite.TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305_SHA256, CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA, CipherSuite.TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA, CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA, CipherSuite.TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA, CipherSuite.TLS_RSA_WITH_AES_128_GCM_SHA256, CipherSuite.TLS_RSA_WITH_AES_256_GCM_SHA384, CipherSuite.TLS_RSA_WITH_AES_128_CBC_SHA, CipherSuite.TLS_RSA_WITH_AES_256_CBC_SHA, CipherSuite.TLS_RSA_WITH_3DES_EDE_CBC_SHA};
      MODERN_TLS = (new ConnectionSpec.Builder(true)).cipherSuites(APPROVED_CIPHER_SUITES).tlsVersions(new TlsVersion[]{TlsVersion.TLS_1_3, TlsVersion.TLS_1_2, TlsVersion.TLS_1_1, TlsVersion.TLS_1_0}).supportsTlsExtensions(true).build();
      COMPATIBLE_TLS = (new ConnectionSpec.Builder(MODERN_TLS)).tlsVersions(new TlsVersion[]{TlsVersion.TLS_1_0}).supportsTlsExtensions(true).build();
      CLEARTEXT = (new ConnectionSpec.Builder(false)).build();
   }

   ConnectionSpec(ConnectionSpec.Builder var1) {
      this.tls = var1.tls;
      this.cipherSuites = var1.cipherSuites;
      this.tlsVersions = var1.tlsVersions;
      this.supportsTlsExtensions = var1.supportsTlsExtensions;
   }

   private ConnectionSpec supportedSpec(SSLSocket var1, boolean var2) {
      String[] var4;
      if(this.cipherSuites != null) {
         var4 = Util.intersect(CipherSuite.ORDER_BY_NAME, var1.getEnabledCipherSuites(), this.cipherSuites);
      } else {
         var4 = var1.getEnabledCipherSuites();
      }

      String[] var5;
      if(this.tlsVersions != null) {
         var5 = Util.intersect(Util.NATURAL_ORDER, var1.getEnabledProtocols(), this.tlsVersions);
      } else {
         var5 = var1.getEnabledProtocols();
      }

      String[] var6 = var1.getSupportedCipherSuites();
      int var3 = Util.indexOf(CipherSuite.ORDER_BY_NAME, var6, "TLS_FALLBACK_SCSV");
      String[] var7 = var4;
      if(var2) {
         var7 = var4;
         if(var3 != -1) {
            var7 = Util.concat(var4, var6[var3]);
         }
      }

      return (new ConnectionSpec.Builder(this)).cipherSuites(var7).tlsVersions(var5).build();
   }

   void apply(SSLSocket var1, boolean var2) {
      ConnectionSpec var3 = this.supportedSpec(var1, var2);
      if(var3.tlsVersions != null) {
         var1.setEnabledProtocols(var3.tlsVersions);
      }

      if(var3.cipherSuites != null) {
         var1.setEnabledCipherSuites(var3.cipherSuites);
      }

   }

   @Nullable
   public List cipherSuites() {
      List var1;
      if(this.cipherSuites != null) {
         var1 = CipherSuite.forJavaNames(this.cipherSuites);
      } else {
         var1 = null;
      }

      return var1;
   }

   public boolean equals(@Nullable Object var1) {
      boolean var3 = false;
      boolean var2;
      if(!(var1 instanceof ConnectionSpec)) {
         var2 = var3;
      } else if(var1 == this) {
         var2 = true;
      } else {
         ConnectionSpec var4 = (ConnectionSpec)var1;
         var2 = var3;
         if(this.tls == var4.tls) {
            if(this.tls) {
               var2 = var3;
               if(!Arrays.equals(this.cipherSuites, var4.cipherSuites)) {
                  return var2;
               }

               var2 = var3;
               if(!Arrays.equals(this.tlsVersions, var4.tlsVersions)) {
                  return var2;
               }

               var2 = var3;
               if(this.supportsTlsExtensions != var4.supportsTlsExtensions) {
                  return var2;
               }
            }

            var2 = true;
         }
      }

      return var2;
   }

   public int hashCode() {
      int var1 = 17;
      if(this.tls) {
         int var2 = Arrays.hashCode(this.cipherSuites);
         int var3 = Arrays.hashCode(this.tlsVersions);
         byte var4;
         if(this.supportsTlsExtensions) {
            var4 = 0;
         } else {
            var4 = 1;
         }

         var1 = var4 + ((var2 + 527) * 31 + var3) * 31;
      }

      return var1;
   }

   public boolean isCompatible(SSLSocket var1) {
      boolean var3 = false;
      boolean var2;
      if(!this.tls) {
         var2 = var3;
      } else {
         if(this.tlsVersions != null) {
            var2 = var3;
            if(!Util.nonEmptyIntersection(Util.NATURAL_ORDER, this.tlsVersions, var1.getEnabledProtocols())) {
               return var2;
            }
         }

         if(this.cipherSuites != null) {
            var2 = var3;
            if(!Util.nonEmptyIntersection(CipherSuite.ORDER_BY_NAME, this.cipherSuites, var1.getEnabledCipherSuites())) {
               return var2;
            }
         }

         var2 = true;
      }

      return var2;
   }

   public boolean isTls() {
      return this.tls;
   }

   public boolean supportsTlsExtensions() {
      return this.supportsTlsExtensions;
   }

   @Nullable
   public List tlsVersions() {
      List var1;
      if(this.tlsVersions != null) {
         var1 = TlsVersion.forJavaNames(this.tlsVersions);
      } else {
         var1 = null;
      }

      return var1;
   }

   public String toString() {
      String var1;
      if(!this.tls) {
         var1 = "ConnectionSpec()";
      } else {
         if(this.cipherSuites != null) {
            var1 = this.cipherSuites().toString();
         } else {
            var1 = "[all enabled]";
         }

         String var2;
         if(this.tlsVersions != null) {
            var2 = this.tlsVersions().toString();
         } else {
            var2 = "[all enabled]";
         }

         var1 = "ConnectionSpec(cipherSuites=" + var1 + ", tlsVersions=" + var2 + ", supportsTlsExtensions=" + this.supportsTlsExtensions + ")";
      }

      return var1;
   }

   public static final class Builder {
      @Nullable
      String[] cipherSuites;
      boolean supportsTlsExtensions;
      boolean tls;
      @Nullable
      String[] tlsVersions;

      public Builder(ConnectionSpec var1) {
         this.tls = var1.tls;
         this.cipherSuites = var1.cipherSuites;
         this.tlsVersions = var1.tlsVersions;
         this.supportsTlsExtensions = var1.supportsTlsExtensions;
      }

      Builder(boolean var1) {
         this.tls = var1;
      }

      public ConnectionSpec.Builder allEnabledCipherSuites() {
         if(!this.tls) {
            throw new IllegalStateException("no cipher suites for cleartext connections");
         } else {
            this.cipherSuites = null;
            return this;
         }
      }

      public ConnectionSpec.Builder allEnabledTlsVersions() {
         if(!this.tls) {
            throw new IllegalStateException("no TLS versions for cleartext connections");
         } else {
            this.tlsVersions = null;
            return this;
         }
      }

      public ConnectionSpec build() {
         return new ConnectionSpec(this);
      }

      public ConnectionSpec.Builder cipherSuites(String... var1) {
         if(!this.tls) {
            throw new IllegalStateException("no cipher suites for cleartext connections");
         } else if(var1.length == 0) {
            throw new IllegalArgumentException("At least one cipher suite is required");
         } else {
            this.cipherSuites = (String[])var1.clone();
            return this;
         }
      }

      public ConnectionSpec.Builder cipherSuites(CipherSuite... var1) {
         if(!this.tls) {
            throw new IllegalStateException("no cipher suites for cleartext connections");
         } else {
            String[] var3 = new String[var1.length];

            for(int var2 = 0; var2 < var1.length; ++var2) {
               var3[var2] = var1[var2].javaName;
            }

            return this.cipherSuites(var3);
         }
      }

      public ConnectionSpec.Builder supportsTlsExtensions(boolean var1) {
         if(!this.tls) {
            throw new IllegalStateException("no TLS extensions for cleartext connections");
         } else {
            this.supportsTlsExtensions = var1;
            return this;
         }
      }

      public ConnectionSpec.Builder tlsVersions(String... var1) {
         if(!this.tls) {
            throw new IllegalStateException("no TLS versions for cleartext connections");
         } else if(var1.length == 0) {
            throw new IllegalArgumentException("At least one TLS version is required");
         } else {
            this.tlsVersions = (String[])var1.clone();
            return this;
         }
      }

      public ConnectionSpec.Builder tlsVersions(TlsVersion... var1) {
         if(!this.tls) {
            throw new IllegalStateException("no TLS versions for cleartext connections");
         } else {
            String[] var3 = new String[var1.length];

            for(int var2 = 0; var2 < var1.length; ++var2) {
               var3[var2] = var1[var2].javaName;
            }

            return this.tlsVersions(var3);
         }
      }
   }
}
