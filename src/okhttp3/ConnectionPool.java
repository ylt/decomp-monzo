package okhttp3;

import java.lang.ref.Reference;
import java.net.Socket;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import javax.annotation.Nullable;
import okhttp3.internal.Util;
import okhttp3.internal.connection.RealConnection;
import okhttp3.internal.connection.RouteDatabase;
import okhttp3.internal.connection.StreamAllocation;
import okhttp3.internal.platform.Platform;

public final class ConnectionPool {
   // $FF: synthetic field
   static final boolean $assertionsDisabled;
   private static final Executor executor;
   private final Runnable cleanupRunnable;
   boolean cleanupRunning;
   private final Deque connections;
   private final long keepAliveDurationNs;
   private final int maxIdleConnections;
   final RouteDatabase routeDatabase;

   static {
      boolean var0;
      if(!ConnectionPool.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      $assertionsDisabled = var0;
      executor = new ThreadPoolExecutor(0, Integer.MAX_VALUE, 60L, TimeUnit.SECONDS, new SynchronousQueue(), Util.threadFactory("OkHttp ConnectionPool", true));
   }

   public ConnectionPool() {
      this(5, 5L, TimeUnit.MINUTES);
   }

   public ConnectionPool(int var1, long var2, TimeUnit var4) {
      this.cleanupRunnable = new Runnable() {
         public void run() {
            // $FF: Couldn't be decompiled
         }
      };
      this.connections = new ArrayDeque();
      this.routeDatabase = new RouteDatabase();
      this.maxIdleConnections = var1;
      this.keepAliveDurationNs = var4.toNanos(var2);
      if(var2 <= 0L) {
         throw new IllegalArgumentException("keepAliveDuration <= 0: " + var2);
      }
   }

   private int pruneAndGetAllocationCount(RealConnection var1, long var2) {
      byte var5 = 0;
      List var6 = var1.allocations;
      int var4 = 0;

      while(true) {
         if(var4 < var6.size()) {
            Reference var7 = (Reference)var6.get(var4);
            if(var7.get() != null) {
               ++var4;
               continue;
            }

            StreamAllocation.StreamAllocationReference var9 = (StreamAllocation.StreamAllocationReference)var7;
            String var8 = "A connection to " + var1.route().address().url() + " was leaked. Did you forget to close a response body?";
            Platform.get().logCloseableLeak(var8, var9.callStackTrace);
            var6.remove(var4);
            var1.noNewStreams = true;
            if(!var6.isEmpty()) {
               continue;
            }

            var1.idleAtNanos = var2 - this.keepAliveDurationNs;
            var4 = var5;
            break;
         }

         var4 = var6.size();
         break;
      }

      return var4;
   }

   long cleanup(long param1) {
      // $FF: Couldn't be decompiled
   }

   boolean connectionBecameIdle(RealConnection var1) {
      if(!$assertionsDisabled && !Thread.holdsLock(this)) {
         throw new AssertionError();
      } else {
         boolean var2;
         if(!var1.noNewStreams && this.maxIdleConnections != 0) {
            this.notifyAll();
            var2 = false;
         } else {
            this.connections.remove(var1);
            var2 = true;
         }

         return var2;
      }
   }

   public int connectionCount() {
      synchronized(this){}

      int var1;
      try {
         var1 = this.connections.size();
      } finally {
         ;
      }

      return var1;
   }

   @Nullable
   Socket deduplicate(Address var1, StreamAllocation var2) {
      if(!$assertionsDisabled && !Thread.holdsLock(this)) {
         throw new AssertionError();
      } else {
         Iterator var4 = this.connections.iterator();

         Socket var5;
         while(true) {
            if(var4.hasNext()) {
               RealConnection var3 = (RealConnection)var4.next();
               if(!var3.isEligible(var1, (Route)null) || !var3.isMultiplexed() || var3 == var2.connection()) {
                  continue;
               }

               var5 = var2.releaseAndAcquire(var3);
               break;
            }

            var5 = null;
            break;
         }

         return var5;
      }
   }

   public void evictAll() {
      // $FF: Couldn't be decompiled
   }

   @Nullable
   RealConnection get(Address var1, StreamAllocation var2, Route var3) {
      if(!$assertionsDisabled && !Thread.holdsLock(this)) {
         throw new AssertionError();
      } else {
         Iterator var5 = this.connections.iterator();

         RealConnection var6;
         while(true) {
            if(var5.hasNext()) {
               RealConnection var4 = (RealConnection)var5.next();
               if(!var4.isEligible(var1, var3)) {
                  continue;
               }

               var2.acquire(var4);
               var6 = var4;
               break;
            }

            var6 = null;
            break;
         }

         return var6;
      }
   }

   public int idleConnectionCount() {
      // $FF: Couldn't be decompiled
   }

   void put(RealConnection var1) {
      if(!$assertionsDisabled && !Thread.holdsLock(this)) {
         throw new AssertionError();
      } else {
         if(!this.cleanupRunning) {
            this.cleanupRunning = true;
            executor.execute(this.cleanupRunnable);
         }

         this.connections.add(var1);
      }
   }
}
