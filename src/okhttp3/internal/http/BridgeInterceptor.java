package okhttp3.internal.http;

import c.j;
import c.l;
import c.t;
import java.io.IOException;
import java.util.List;
import okhttp3.Cookie;
import okhttp3.CookieJar;
import okhttp3.Headers;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.internal.Util;
import okhttp3.internal.Version;

public final class BridgeInterceptor implements Interceptor {
   private final CookieJar cookieJar;

   public BridgeInterceptor(CookieJar var1) {
      this.cookieJar = var1;
   }

   private String cookieHeader(List var1) {
      StringBuilder var5 = new StringBuilder();
      int var3 = var1.size();

      for(int var2 = 0; var2 < var3; ++var2) {
         if(var2 > 0) {
            var5.append("; ");
         }

         Cookie var4 = (Cookie)var1.get(var2);
         var5.append(var4.name()).append('=').append(var4.value());
      }

      return var5.toString();
   }

   public Response intercept(Interceptor.Chain var1) throws IOException {
      boolean var3 = false;
      Request var6 = var1.request();
      Request.Builder var7 = var6.newBuilder();
      RequestBody var9 = var6.body();
      if(var9 != null) {
         MediaType var8 = var9.contentType();
         if(var8 != null) {
            var7.header("Content-Type", var8.toString());
         }

         long var4 = var9.contentLength();
         if(var4 != -1L) {
            var7.header("Content-Length", Long.toString(var4));
            var7.removeHeader("Transfer-Encoding");
         } else {
            var7.header("Transfer-Encoding", "chunked");
            var7.removeHeader("Content-Length");
         }
      }

      if(var6.header("Host") == null) {
         var7.header("Host", Util.hostHeader(var6.url(), false));
      }

      if(var6.header("Connection") == null) {
         var7.header("Connection", "Keep-Alive");
      }

      boolean var2 = var3;
      if(var6.header("Accept-Encoding") == null) {
         var2 = var3;
         if(var6.header("Range") == null) {
            var2 = true;
            var7.header("Accept-Encoding", "gzip");
         }
      }

      List var14 = this.cookieJar.loadForRequest(var6.url());
      if(!var14.isEmpty()) {
         var7.header("Cookie", this.cookieHeader(var14));
      }

      if(var6.header("User-Agent") == null) {
         var7.header("User-Agent", Version.userAgent());
      }

      Response var12 = var1.proceed(var7.build());
      HttpHeaders.receiveHeaders(this.cookieJar, var6.url(), var12.headers());
      Response.Builder var11 = var12.newBuilder().request(var6);
      if(var2 && "gzip".equalsIgnoreCase(var12.header("Content-Encoding")) && HttpHeaders.hasBody(var12)) {
         j var10 = new j(var12.body().source());
         Headers var13 = var12.headers().newBuilder().removeAll("Content-Encoding").removeAll("Content-Length").build();
         var11.headers(var13);
         var11.body(new RealResponseBody(var13, l.a((t)var10)));
      }

      return var11.build();
   }
}
