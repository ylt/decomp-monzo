package okhttp3.internal.http;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.ProtocolException;
import java.net.Proxy;
import java.net.SocketTimeoutException;
import java.net.Proxy.Type;
import java.security.cert.CertificateException;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSocketFactory;
import okhttp3.Address;
import okhttp3.CertificatePinner;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.Route;
import okhttp3.internal.connection.RealConnection;
import okhttp3.internal.connection.StreamAllocation;

public final class RetryAndFollowUpInterceptor implements Interceptor {
   private static final int MAX_FOLLOW_UPS = 20;
   private Object callStackTrace;
   private volatile boolean canceled;
   private final OkHttpClient client;
   private final boolean forWebSocket;
   private StreamAllocation streamAllocation;

   public RetryAndFollowUpInterceptor(OkHttpClient var1, boolean var2) {
      this.client = var1;
      this.forWebSocket = var2;
   }

   private Address createAddress(HttpUrl var1) {
      CertificatePinner var3 = null;
      SSLSocketFactory var2;
      HostnameVerifier var4;
      if(var1.isHttps()) {
         var2 = this.client.sslSocketFactory();
         var4 = this.client.hostnameVerifier();
         var3 = this.client.certificatePinner();
      } else {
         var4 = null;
         var2 = null;
      }

      return new Address(var1.host(), var1.port(), this.client.dns(), this.client.socketFactory(), var2, var4, var3, this.client.proxyAuthenticator(), this.client.proxy(), this.client.protocols(), this.client.connectionSpecs(), this.client.proxySelector());
   }

   private Request followUpRequest(Response var1) throws IOException {
      Proxy var5 = null;
      Request.Builder var6 = null;
      if(var1 == null) {
         throw new IllegalStateException();
      } else {
         RealConnection var4 = this.streamAllocation.connection();
         Route var9;
         if(var4 != null) {
            var9 = var4.route();
         } else {
            var9 = null;
         }

         int var2 = var1.code();
         String var7 = var1.request().method();
         Request var10;
         switch(var2) {
         case 307:
         case 308:
            if(!var7.equals("GET")) {
               var10 = var6;
               if(!var7.equals("HEAD")) {
                  break;
               }
            }
         case 300:
         case 301:
         case 302:
         case 303:
            var10 = var6;
            if(this.client.followRedirects()) {
               String var8 = var1.header("Location");
               var10 = var6;
               if(var8 != null) {
                  HttpUrl var11 = var1.request().url().resolve(var8);
                  var10 = var6;
                  if(var11 != null) {
                     if(!var11.scheme().equals(var1.request().url().scheme())) {
                        var10 = var6;
                        if(!this.client.followSslRedirects()) {
                           return var10;
                        }
                     }

                     var6 = var1.request().newBuilder();
                     if(HttpMethod.permitsRequestBody(var7)) {
                        boolean var3 = HttpMethod.redirectsWithBody(var7);
                        if(HttpMethod.redirectsToGet(var7)) {
                           var6.method("GET", (RequestBody)null);
                        } else {
                           RequestBody var12 = var5;
                           if(var3) {
                              var12 = var1.request().body();
                           }

                           var6.method(var7, var12);
                        }

                        if(!var3) {
                           var6.removeHeader("Transfer-Encoding");
                           var6.removeHeader("Content-Length");
                           var6.removeHeader("Content-Type");
                        }
                     }

                     if(!this.sameConnection(var1, var11)) {
                        var6.removeHeader("Authorization");
                     }

                     var10 = var6.url(var11).build();
                  }
               }
            }
            break;
         case 401:
            var10 = this.client.authenticator().authenticate(var9, var1);
            break;
         case 407:
            if(var9 != null) {
               var5 = var9.proxy();
            } else {
               var5 = this.client.proxy();
            }

            if(var5.type() != Type.HTTP) {
               throw new ProtocolException("Received HTTP_PROXY_AUTH (407) code while not using proxy");
            }

            var10 = this.client.proxyAuthenticator().authenticate(var9, var1);
            break;
         case 408:
            var10 = var6;
            if(!(var1.request().body() instanceof UnrepeatableRequestBody)) {
               var10 = var1.request();
            }
            break;
         default:
            var10 = var6;
         }

         return var10;
      }
   }

   private boolean isRecoverable(IOException var1, boolean var2) {
      boolean var4 = true;
      boolean var3 = false;
      if(var1 instanceof ProtocolException) {
         var2 = var3;
      } else if(var1 instanceof InterruptedIOException) {
         if(var1 instanceof SocketTimeoutException && !var2) {
            var2 = var4;
         } else {
            var2 = false;
         }
      } else {
         if(var1 instanceof SSLHandshakeException) {
            var2 = var3;
            if(var1.getCause() instanceof CertificateException) {
               return var2;
            }
         }

         var2 = var3;
         if(!(var1 instanceof SSLPeerUnverifiedException)) {
            var2 = true;
         }
      }

      return var2;
   }

   private boolean recover(IOException var1, boolean var2, Request var3) {
      boolean var5 = false;
      this.streamAllocation.streamFailed(var1);
      boolean var4;
      if(!this.client.retryOnConnectionFailure()) {
         var4 = var5;
      } else {
         if(var2) {
            var4 = var5;
            if(var3.body() instanceof UnrepeatableRequestBody) {
               return var4;
            }
         }

         var4 = var5;
         if(this.isRecoverable(var1, var2)) {
            var4 = var5;
            if(this.streamAllocation.hasMoreRoutes()) {
               var4 = true;
            }
         }
      }

      return var4;
   }

   private boolean sameConnection(Response var1, HttpUrl var2) {
      HttpUrl var4 = var1.request().url();
      boolean var3;
      if(var4.host().equals(var2.host()) && var4.port() == var2.port() && var4.scheme().equals(var2.scheme())) {
         var3 = true;
      } else {
         var3 = false;
      }

      return var3;
   }

   public void cancel() {
      this.canceled = true;
      StreamAllocation var1 = this.streamAllocation;
      if(var1 != null) {
         var1.cancel();
      }

   }

   public Response intercept(Interceptor.Chain param1) throws IOException {
      // $FF: Couldn't be decompiled
   }

   public boolean isCanceled() {
      return this.canceled;
   }

   public void setCallStackTrace(Object var1) {
      this.callStackTrace = var1;
   }

   public StreamAllocation streamAllocation() {
      return this.streamAllocation;
   }
}
