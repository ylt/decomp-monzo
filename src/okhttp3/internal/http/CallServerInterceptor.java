package okhttp3.internal.http;

import c.d;
import c.l;
import java.io.IOException;
import java.net.ProtocolException;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.internal.Util;
import okhttp3.internal.connection.RealConnection;
import okhttp3.internal.connection.StreamAllocation;

public final class CallServerInterceptor implements Interceptor {
   private final boolean forWebSocket;

   public CallServerInterceptor(boolean var1) {
      this.forWebSocket = var1;
   }

   public Response intercept(Interceptor.Chain var1) throws IOException {
      RealInterceptorChain var9 = (RealInterceptorChain)var1;
      HttpCodec var7 = var9.httpStream();
      StreamAllocation var6 = var9.streamAllocation();
      RealConnection var5 = (RealConnection)var9.connection();
      Request var8 = var9.request();
      long var3 = System.currentTimeMillis();
      var7.writeRequestHeaders(var8);
      Response.Builder var10 = null;
      if(HttpMethod.permitsRequestBody(var8.method()) && var8.body() != null) {
         if("100-continue".equalsIgnoreCase(var8.header("Expect"))) {
            var7.flushRequest();
            var10 = var7.readResponseHeaders(true);
         }

         if(var10 == null) {
            d var12 = l.a(var7.createRequestBody(var8, var8.body().contentLength()));
            var8.body().writeTo(var12);
            var12.close();
         } else if(!var5.isMultiplexed()) {
            var6.noNewStreams();
         }
      } else {
         var10 = null;
      }

      var7.finishRequest();
      Response.Builder var13 = var10;
      if(var10 == null) {
         var13 = var7.readResponseHeaders(false);
      }

      Response var11 = var13.request(var8).handshake(var6.connection().handshake()).sentRequestAtMillis(var3).receivedResponseAtMillis(System.currentTimeMillis()).build();
      int var2 = var11.code();
      if(this.forWebSocket && var2 == 101) {
         var11 = var11.newBuilder().body(Util.EMPTY_RESPONSE).build();
      } else {
         var11 = var11.newBuilder().body(var7.openResponseBody(var11)).build();
      }

      if("close".equalsIgnoreCase(var11.request().header("Connection")) || "close".equalsIgnoreCase(var11.header("Connection"))) {
         var6.noNewStreams();
      }

      if((var2 == 204 || var2 == 205) && var11.body().contentLength() > 0L) {
         throw new ProtocolException("HTTP " + var2 + " had non-zero Content-Length: " + var11.body().contentLength());
      } else {
         return var11;
      }
   }
}
