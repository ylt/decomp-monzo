package okhttp3.internal.ws;

import c.d;
import c.e;
import c.f;
import java.io.Closeable;
import java.io.IOException;
import java.net.ProtocolException;
import java.util.ArrayDeque;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Protocol;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;
import okhttp3.internal.Internal;
import okhttp3.internal.Util;
import okhttp3.internal.connection.StreamAllocation;

public final class RealWebSocket implements WebSocket, WebSocketReader.FrameCallback {
   // $FF: synthetic field
   static final boolean $assertionsDisabled;
   private static final long CANCEL_AFTER_CLOSE_MILLIS = 60000L;
   private static final long MAX_QUEUE_SIZE = 16777216L;
   private static final List ONLY_HTTP1;
   private Call call;
   private ScheduledFuture cancelFuture;
   private boolean enqueuedClose;
   private ScheduledExecutorService executor;
   private boolean failed;
   private final String key;
   final WebSocketListener listener;
   private final ArrayDeque messageAndCloseQueue = new ArrayDeque();
   private final Request originalRequest;
   int pingCount;
   int pongCount;
   private final ArrayDeque pongQueue = new ArrayDeque();
   private long queueSize;
   private final Random random;
   private WebSocketReader reader;
   private int receivedCloseCode = -1;
   private String receivedCloseReason;
   private RealWebSocket.Streams streams;
   private WebSocketWriter writer;
   private final Runnable writerRunnable;

   static {
      boolean var0;
      if(!RealWebSocket.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      $assertionsDisabled = var0;
      ONLY_HTTP1 = Collections.singletonList(Protocol.HTTP_1_1);
   }

   public RealWebSocket(Request var1, WebSocketListener var2, Random var3) {
      if(!"GET".equals(var1.method())) {
         throw new IllegalArgumentException("Request must be GET: " + var1.method());
      } else {
         this.originalRequest = var1;
         this.listener = var2;
         this.random = var3;
         byte[] var4 = new byte[16];
         var3.nextBytes(var4);
         this.key = f.a(var4).b();
         this.writerRunnable = new Runnable() {
            public void run() {
               boolean var1;
               do {
                  try {
                     var1 = RealWebSocket.this.writeOneFrame();
                  } catch (IOException var3) {
                     RealWebSocket.this.failWebSocket(var3, (Response)null);
                     break;
                  }
               } while(var1);

            }
         };
      }
   }

   private void runWriter() {
      if(!$assertionsDisabled && !Thread.holdsLock(this)) {
         throw new AssertionError();
      } else {
         if(this.executor != null) {
            this.executor.execute(this.writerRunnable);
         }

      }
   }

   private boolean send(f param1, int param2) {
      // $FF: Couldn't be decompiled
   }

   void awaitTermination(int var1, TimeUnit var2) throws InterruptedException {
      this.executor.awaitTermination((long)var1, var2);
   }

   public void cancel() {
      this.call.cancel();
   }

   void checkResponse(Response var1) throws ProtocolException {
      if(var1.code() != 101) {
         throw new ProtocolException("Expected HTTP 101 response but was '" + var1.code() + " " + var1.message() + "'");
      } else {
         String var2 = var1.header("Connection");
         if(!"Upgrade".equalsIgnoreCase(var2)) {
            throw new ProtocolException("Expected 'Connection' header value 'Upgrade' but was '" + var2 + "'");
         } else {
            var2 = var1.header("Upgrade");
            if(!"websocket".equalsIgnoreCase(var2)) {
               throw new ProtocolException("Expected 'Upgrade' header value 'websocket' but was '" + var2 + "'");
            } else {
               var2 = var1.header("Sec-WebSocket-Accept");
               String var3 = f.a(this.key + "258EAFA5-E914-47DA-95CA-C5AB0DC85B11").d().b();
               if(!var3.equals(var2)) {
                  throw new ProtocolException("Expected 'Sec-WebSocket-Accept' header value '" + var3 + "' but was '" + var2 + "'");
               }
            }
         }
      }
   }

   public boolean close(int var1, String var2) {
      return this.close(var1, var2, 60000L);
   }

   boolean close(int param1, String param2, long param3) {
      // $FF: Couldn't be decompiled
   }

   public void connect(OkHttpClient var1) {
      var1 = var1.newBuilder().protocols(ONLY_HTTP1).build();
      final int var2 = var1.pingIntervalMillis();
      final Request var3 = this.originalRequest.newBuilder().header("Upgrade", "websocket").header("Connection", "Upgrade").header("Sec-WebSocket-Key", this.key).header("Sec-WebSocket-Version", "13").build();
      this.call = Internal.instance.newWebSocketCall(var1, var3);
      this.call.enqueue(new Callback() {
         public void onFailure(Call var1, IOException var2x) {
            RealWebSocket.this.failWebSocket(var2x, (Response)null);
         }

         public void onResponse(Call var1, Response var2x) {
            try {
               RealWebSocket.this.checkResponse(var2x);
            } catch (ProtocolException var5) {
               RealWebSocket.this.failWebSocket(var5, var2x);
               Util.closeQuietly((Closeable)var2x);
               return;
            }

            StreamAllocation var3x = Internal.instance.streamAllocation(var1);
            var3x.noNewStreams();
            RealWebSocket.Streams var6 = var3x.connection().newWebSocketStreams(var3x);

            try {
               RealWebSocket.this.listener.onOpen(RealWebSocket.this, var2x);
               StringBuilder var7 = new StringBuilder();
               String var8 = var7.append("OkHttp WebSocket ").append(var3.url().redact()).toString();
               RealWebSocket.this.initReaderAndWriter(var8, (long)var2, var6);
               var3x.connection().socket().setSoTimeout(0);
               RealWebSocket.this.loopReader();
            } catch (Exception var4) {
               RealWebSocket.this.failWebSocket(var4, (Response)null);
            }

         }
      });
   }

   public void failWebSocket(Exception param1, Response param2) {
      // $FF: Couldn't be decompiled
   }

   public void initReaderAndWriter(String param1, long param2, RealWebSocket.Streams param4) throws IOException {
      // $FF: Couldn't be decompiled
   }

   public void loopReader() throws IOException {
      while(this.receivedCloseCode == -1) {
         this.reader.processNextFrame();
      }

   }

   public void onReadClose(int param1, String param2) {
      // $FF: Couldn't be decompiled
   }

   public void onReadMessage(f var1) throws IOException {
      this.listener.onMessage(this, (f)var1);
   }

   public void onReadMessage(String var1) throws IOException {
      this.listener.onMessage(this, (String)var1);
   }

   public void onReadPing(f param1) {
      // $FF: Couldn't be decompiled
   }

   public void onReadPong(f var1) {
      synchronized(this){}

      try {
         ++this.pongCount;
      } finally {
         ;
      }

   }

   int pingCount() {
      synchronized(this){}

      int var1;
      try {
         var1 = this.pingCount;
      } finally {
         ;
      }

      return var1;
   }

   boolean pong(f param1) {
      // $FF: Couldn't be decompiled
   }

   int pongCount() {
      synchronized(this){}

      int var1;
      try {
         var1 = this.pongCount;
      } finally {
         ;
      }

      return var1;
   }

   boolean processNextFrame() throws IOException {
      boolean var2 = false;

      int var1;
      try {
         this.reader.processNextFrame();
         var1 = this.receivedCloseCode;
      } catch (Exception var4) {
         this.failWebSocket(var4, (Response)null);
         return var2;
      }

      if(var1 == -1) {
         var2 = true;
      }

      return var2;
   }

   public long queueSize() {
      synchronized(this){}

      long var1;
      try {
         var1 = this.queueSize;
      } finally {
         ;
      }

      return var1;
   }

   public Request request() {
      return this.originalRequest;
   }

   public boolean send(f var1) {
      if(var1 == null) {
         throw new NullPointerException("bytes == null");
      } else {
         return this.send(var1, 2);
      }
   }

   public boolean send(String var1) {
      if(var1 == null) {
         throw new NullPointerException("text == null");
      } else {
         return this.send(f.a(var1), 1);
      }
   }

   void tearDown() throws InterruptedException {
      if(this.cancelFuture != null) {
         this.cancelFuture.cancel(false);
      }

      this.executor.shutdown();
      this.executor.awaitTermination(10L, TimeUnit.SECONDS);
   }

   boolean writeOneFrame() throws IOException {
      // $FF: Couldn't be decompiled
   }

   void writePingFrame() {
      // $FF: Couldn't be decompiled
   }

   final class CancelRunnable implements Runnable {
      public void run() {
         RealWebSocket.this.cancel();
      }
   }

   static final class Close {
      final long cancelAfterCloseMillis;
      final int code;
      final f reason;

      Close(int var1, f var2, long var3) {
         this.code = var1;
         this.reason = var2;
         this.cancelAfterCloseMillis = var3;
      }
   }

   static final class Message {
      final f data;
      final int formatOpcode;

      Message(int var1, f var2) {
         this.formatOpcode = var1;
         this.data = var2;
      }
   }

   private final class PingRunnable implements Runnable {
      public void run() {
         RealWebSocket.this.writePingFrame();
      }
   }

   public abstract static class Streams implements Closeable {
      public final boolean client;
      public final d sink;
      public final e source;

      public Streams(boolean var1, e var2, d var3) {
         this.client = var1;
         this.source = var2;
         this.sink = var3;
      }
   }
}
