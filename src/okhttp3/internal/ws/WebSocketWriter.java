package okhttp3.internal.ws;

import c.c;
import c.d;
import c.f;
import c.s;
import c.u;
import java.io.IOException;
import java.util.Random;

final class WebSocketWriter {
   // $FF: synthetic field
   static final boolean $assertionsDisabled;
   boolean activeWriter;
   final c buffer;
   final WebSocketWriter.FrameSink frameSink;
   final boolean isClient;
   final byte[] maskBuffer;
   final byte[] maskKey;
   final Random random;
   final d sink;
   boolean writerClosed;

   static {
      boolean var0;
      if(!WebSocketWriter.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      $assertionsDisabled = var0;
   }

   WebSocketWriter(boolean var1, d var2, Random var3) {
      Object var4 = null;
      super();
      this.buffer = new c();
      this.frameSink = new WebSocketWriter.FrameSink();
      if(var2 == null) {
         throw new NullPointerException("sink == null");
      } else if(var3 == null) {
         throw new NullPointerException("random == null");
      } else {
         this.isClient = var1;
         this.sink = var2;
         this.random = var3;
         byte[] var5;
         if(var1) {
            var5 = new byte[4];
         } else {
            var5 = null;
         }

         this.maskKey = var5;
         var5 = (byte[])var4;
         if(var1) {
            var5 = new byte[8192];
         }

         this.maskBuffer = var5;
      }
   }

   private void writeControlFrameSynchronized(int var1, f var2) throws IOException {
      if(!$assertionsDisabled && !Thread.holdsLock(this)) {
         throw new AssertionError();
      } else if(this.writerClosed) {
         throw new IOException("closed");
      } else {
         int var3 = var2.h();
         if((long)var3 > 125L) {
            throw new IllegalArgumentException("Payload size must be less than or equal to 125");
         } else {
            this.sink.i(var1 | 128);
            if(this.isClient) {
               this.sink.i(var3 | 128);
               this.random.nextBytes(this.maskKey);
               this.sink.c(this.maskKey);
               byte[] var4 = var2.i();
               WebSocketProtocol.toggleMask(var4, (long)var4.length, this.maskKey, 0L);
               this.sink.c(var4);
            } else {
               this.sink.i(var3);
               this.sink.c(var2);
            }

            this.sink.flush();
         }
      }
   }

   s newMessageSink(int var1, long var2) {
      if(this.activeWriter) {
         throw new IllegalStateException("Another message writer is active. Did you call close()?");
      } else {
         this.activeWriter = true;
         this.frameSink.formatOpcode = var1;
         this.frameSink.contentLength = var2;
         this.frameSink.isFirstFrame = true;
         this.frameSink.closed = false;
         return this.frameSink;
      }
   }

   void writeClose(int param1, f param2) throws IOException {
      // $FF: Couldn't be decompiled
   }

   void writeMessageFrameSynchronized(int var1, long var2, boolean var4, boolean var5) throws IOException {
      if(!$assertionsDisabled && !Thread.holdsLock(this)) {
         throw new AssertionError();
      } else if(this.writerClosed) {
         throw new IOException("closed");
      } else {
         if(!var4) {
            var1 = 0;
         }

         int var6 = var1;
         if(var5) {
            var6 = var1 | 128;
         }

         this.sink.i(var6);
         short var9;
         if(this.isClient) {
            var9 = 128;
         } else {
            var9 = 0;
         }

         if(var2 <= 125L) {
            var6 = (int)var2;
            this.sink.i(var9 | var6);
         } else if(var2 <= 65535L) {
            this.sink.i(var9 | 126);
            this.sink.h((int)var2);
         } else {
            this.sink.i(var9 | 127);
            this.sink.o(var2);
         }

         if(this.isClient) {
            this.random.nextBytes(this.maskKey);
            this.sink.c(this.maskKey);

            for(long var7 = 0L; var7 < var2; var7 += (long)var1) {
               var1 = (int)Math.min(var2, (long)this.maskBuffer.length);
               var1 = this.buffer.a((byte[])this.maskBuffer, 0, var1);
               if(var1 == -1) {
                  throw new AssertionError();
               }

               WebSocketProtocol.toggleMask(this.maskBuffer, (long)var1, this.maskKey, var7);
               this.sink.c(this.maskBuffer, 0, var1);
            }
         } else {
            this.sink.write(this.buffer, var2);
         }

         this.sink.e();
      }
   }

   void writePing(f param1) throws IOException {
      // $FF: Couldn't be decompiled
   }

   void writePong(f param1) throws IOException {
      // $FF: Couldn't be decompiled
   }

   final class FrameSink implements s {
      boolean closed;
      long contentLength;
      int formatOpcode;
      boolean isFirstFrame;

      public void close() throws IOException {
         // $FF: Couldn't be decompiled
      }

      public void flush() throws IOException {
         // $FF: Couldn't be decompiled
      }

      public u timeout() {
         return WebSocketWriter.this.sink.timeout();
      }

      public void write(c param1, long param2) throws IOException {
         // $FF: Couldn't be decompiled
      }
   }
}
