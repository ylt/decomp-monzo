package okhttp3.internal.ws;

import c.c;
import c.e;
import c.f;
import java.io.EOFException;
import java.io.IOException;
import java.net.ProtocolException;
import java.util.concurrent.TimeUnit;

final class WebSocketReader {
   boolean closed;
   long frameBytesRead;
   final WebSocketReader.FrameCallback frameCallback;
   long frameLength;
   final boolean isClient;
   boolean isControlFrame;
   boolean isFinalFrame;
   boolean isMasked;
   final byte[] maskBuffer = new byte[8192];
   final byte[] maskKey = new byte[4];
   int opcode;
   final e source;

   WebSocketReader(boolean var1, e var2, WebSocketReader.FrameCallback var3) {
      if(var2 == null) {
         throw new NullPointerException("source == null");
      } else if(var3 == null) {
         throw new NullPointerException("frameCallback == null");
      } else {
         this.isClient = var1;
         this.source = var2;
         this.frameCallback = var3;
      }
   }

   private void readControlFrame() throws IOException {
      c var5 = new c();
      if(this.frameBytesRead < this.frameLength) {
         if(this.isClient) {
            this.source.a(var5, this.frameLength);
         } else {
            while(this.frameBytesRead < this.frameLength) {
               int var1 = (int)Math.min(this.frameLength - this.frameBytesRead, (long)this.maskBuffer.length);
               var1 = this.source.a(this.maskBuffer, 0, var1);
               if(var1 == -1) {
                  throw new EOFException();
               }

               WebSocketProtocol.toggleMask(this.maskBuffer, (long)var1, this.maskKey, this.frameBytesRead);
               var5.b((byte[])this.maskBuffer, 0, var1);
               this.frameBytesRead += (long)var1;
            }
         }
      }

      switch(this.opcode) {
      case 8:
         short var6 = 1005;
         String var4 = "";
         long var2 = var5.a();
         if(var2 == 1L) {
            throw new ProtocolException("Malformed close payload length of 1.");
         }

         if(var2 != 0L) {
            var6 = var5.j();
            var4 = var5.r();
            String var7 = WebSocketProtocol.closeCodeExceptionMessage(var6);
            if(var7 != null) {
               throw new ProtocolException(var7);
            }
         }

         this.frameCallback.onReadClose(var6, var4);
         this.closed = true;
         break;
      case 9:
         this.frameCallback.onReadPing(var5.q());
         break;
      case 10:
         this.frameCallback.onReadPong(var5.q());
         break;
      default:
         throw new ProtocolException("Unknown control opcode: " + Integer.toHexString(this.opcode));
      }

   }

   private void readHeader() throws IOException {
      boolean var5 = true;
      if(this.closed) {
         throw new IOException("closed");
      } else {
         long var6 = this.source.timeout().timeoutNanos();
         this.source.timeout().clearTimeout();
         boolean var10 = false;

         byte var1;
         try {
            var10 = true;
            var1 = this.source.i();
            var10 = false;
         } finally {
            if(var10) {
               this.source.timeout().timeout(var6, TimeUnit.NANOSECONDS);
            }
         }

         int var3 = var1 & 255;
         this.source.timeout().timeout(var6, TimeUnit.NANOSECONDS);
         this.opcode = var3 & 15;
         boolean var4;
         if((var3 & 128) != 0) {
            var4 = true;
         } else {
            var4 = false;
         }

         this.isFinalFrame = var4;
         if((var3 & 8) != 0) {
            var4 = true;
         } else {
            var4 = false;
         }

         this.isControlFrame = var4;
         if(this.isControlFrame && !this.isFinalFrame) {
            throw new ProtocolException("Control frames must be final.");
         } else {
            boolean var12;
            if((var3 & 64) != 0) {
               var12 = true;
            } else {
               var12 = false;
            }

            boolean var2;
            if((var3 & 32) != 0) {
               var2 = true;
            } else {
               var2 = false;
            }

            boolean var14;
            if((var3 & 16) != 0) {
               var14 = true;
            } else {
               var14 = false;
            }

            if(!var12 && !var2 && !var14) {
               int var13 = this.source.i() & 255;
               if((var13 & 128) != 0) {
                  var4 = var5;
               } else {
                  var4 = false;
               }

               this.isMasked = var4;
               if(this.isMasked == this.isClient) {
                  String var8;
                  if(this.isClient) {
                     var8 = "Server-sent frames must not be masked.";
                  } else {
                     var8 = "Client-sent frames must be masked.";
                  }

                  throw new ProtocolException(var8);
               } else {
                  this.frameLength = (long)(var13 & 127);
                  if(this.frameLength == 126L) {
                     this.frameLength = (long)this.source.j() & 65535L;
                  } else if(this.frameLength == 127L) {
                     this.frameLength = this.source.l();
                     if(this.frameLength < 0L) {
                        throw new ProtocolException("Frame length 0x" + Long.toHexString(this.frameLength) + " > 0x7FFFFFFFFFFFFFFF");
                     }
                  }

                  this.frameBytesRead = 0L;
                  if(this.isControlFrame && this.frameLength > 125L) {
                     throw new ProtocolException("Control frame must be less than 125B.");
                  } else {
                     if(this.isMasked) {
                        this.source.a(this.maskKey);
                     }

                  }
               }
            } else {
               throw new ProtocolException("Reserved flags are unsupported.");
            }
         }
      }
   }

   private void readMessage(c var1) throws IOException {
      long var2;
      for(; !this.closed; this.frameBytesRead += var2) {
         if(this.frameBytesRead == this.frameLength) {
            if(this.isFinalFrame) {
               return;
            }

            this.readUntilNonControlFrame();
            if(this.opcode != 0) {
               throw new ProtocolException("Expected continuation opcode. Got: " + Integer.toHexString(this.opcode));
            }

            if(this.isFinalFrame && this.frameLength == 0L) {
               return;
            }
         }

         var2 = this.frameLength - this.frameBytesRead;
         if(this.isMasked) {
            var2 = Math.min(var2, (long)this.maskBuffer.length);
            var2 = (long)this.source.a(this.maskBuffer, 0, (int)var2);
            if(var2 == -1L) {
               throw new EOFException();
            }

            WebSocketProtocol.toggleMask(this.maskBuffer, var2, this.maskKey, this.frameBytesRead);
            var1.b((byte[])this.maskBuffer, 0, (int)var2);
         } else {
            long var4 = this.source.read(var1, var2);
            var2 = var4;
            if(var4 == -1L) {
               throw new EOFException();
            }
         }
      }

      throw new IOException("closed");
   }

   private void readMessageFrame() throws IOException {
      int var1 = this.opcode;
      if(var1 != 1 && var1 != 2) {
         throw new ProtocolException("Unknown opcode: " + Integer.toHexString(var1));
      } else {
         c var2 = new c();
         this.readMessage(var2);
         if(var1 == 1) {
            this.frameCallback.onReadMessage(var2.r());
         } else {
            this.frameCallback.onReadMessage(var2.q());
         }

      }
   }

   void processNextFrame() throws IOException {
      this.readHeader();
      if(this.isControlFrame) {
         this.readControlFrame();
      } else {
         this.readMessageFrame();
      }

   }

   void readUntilNonControlFrame() throws IOException {
      while(true) {
         if(!this.closed) {
            this.readHeader();
            if(this.isControlFrame) {
               this.readControlFrame();
               continue;
            }
         }

         return;
      }
   }

   public interface FrameCallback {
      void onReadClose(int var1, String var2);

      void onReadMessage(f var1) throws IOException;

      void onReadMessage(String var1) throws IOException;

      void onReadPing(f var1);

      void onReadPong(f var1);
   }
}
