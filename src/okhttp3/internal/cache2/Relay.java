package okhttp3.internal.cache2;

import c.c;
import c.f;
import c.t;
import c.u;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

final class Relay {
   private static final long FILE_HEADER_SIZE = 32L;
   static final f PREFIX_CLEAN = f.a("OkHttp cache v1\n");
   static final f PREFIX_DIRTY = f.a("OkHttp DIRTY :(\n");
   private static final int SOURCE_FILE = 2;
   private static final int SOURCE_UPSTREAM = 1;
   final c buffer = new c();
   final long bufferMaxSize;
   boolean complete;
   RandomAccessFile file;
   private final f metadata;
   int sourceCount;
   t upstream;
   final c upstreamBuffer = new c();
   long upstreamPos;
   Thread upstreamReader;

   private Relay(RandomAccessFile var1, t var2, long var3, f var5, long var6) {
      this.file = var1;
      this.upstream = var2;
      boolean var8;
      if(var2 == null) {
         var8 = true;
      } else {
         var8 = false;
      }

      this.complete = var8;
      this.upstreamPos = var3;
      this.metadata = var5;
      this.bufferMaxSize = var6;
   }

   public static Relay edit(File var0, t var1, f var2, long var3) throws IOException {
      RandomAccessFile var5 = new RandomAccessFile(var0, "rw");
      Relay var6 = new Relay(var5, var1, 0L, var2, var3);
      var5.setLength(0L);
      var6.writeHeader(PREFIX_DIRTY, -1L, -1L);
      return var6;
   }

   public static Relay read(File var0) throws IOException {
      RandomAccessFile var7 = new RandomAccessFile(var0, "rw");
      FileOperator var5 = new FileOperator(var7.getChannel());
      c var6 = new c();
      var5.read(0L, var6, 32L);
      if(!var6.d((long)PREFIX_CLEAN.h()).equals(PREFIX_CLEAN)) {
         throw new IOException("unreadable cache file");
      } else {
         long var3 = var6.l();
         long var1 = var6.l();
         var6 = new c();
         var5.read(32L + var3, var6, var1);
         return new Relay(var7, (t)null, var3, var6.q(), 0L);
      }
   }

   private void writeHeader(f var1, long var2, long var4) throws IOException {
      c var6 = new c();
      var6.a(var1);
      var6.j(var2);
      var6.j(var4);
      if(var6.a() != 32L) {
         throw new IllegalArgumentException();
      } else {
         (new FileOperator(this.file.getChannel())).write(0L, var6, 32L);
      }
   }

   private void writeMetadata(long var1) throws IOException {
      c var3 = new c();
      var3.a(this.metadata);
      (new FileOperator(this.file.getChannel())).write(32L + var1, var3, (long)this.metadata.h());
   }

   void commit(long param1) throws IOException {
      // $FF: Couldn't be decompiled
   }

   boolean isClosed() {
      boolean var1;
      if(this.file == null) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public f metadata() {
      return this.metadata;
   }

   public t newSource() {
      // $FF: Couldn't be decompiled
   }

   class RelaySource implements t {
      private FileOperator fileOperator;
      private long sourcePos;
      private final u timeout = new u();

      RelaySource() {
         this.fileOperator = new FileOperator(Relay.this.file.getChannel());
      }

      public void close() throws IOException {
         // $FF: Couldn't be decompiled
      }

      public long read(c param1, long param2) throws IOException {
         // $FF: Couldn't be decompiled
      }

      public u timeout() {
         return this.timeout;
      }
   }
}
