package okhttp3.internal.publicsuffix;

import java.net.IDN;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;
import okhttp3.internal.Util;

public final class PublicSuffixDatabase {
   private static final String[] EMPTY_RULE = new String[0];
   private static final byte EXCEPTION_MARKER = 33;
   private static final String[] PREVAILING_RULE = new String[]{"*"};
   public static final String PUBLIC_SUFFIX_RESOURCE = "publicsuffixes.gz";
   private static final byte[] WILDCARD_LABEL = new byte[]{42};
   private static final PublicSuffixDatabase instance = new PublicSuffixDatabase();
   private final AtomicBoolean listRead = new AtomicBoolean(false);
   private byte[] publicSuffixExceptionListBytes;
   private byte[] publicSuffixListBytes;
   private final CountDownLatch readCompleteLatch = new CountDownLatch(1);

   private static String binarySearchBytes(byte[] var0, byte[][] var1, int var2) {
      int var3 = 0;
      int var4 = var0.length;

      String var15;
      while(true) {
         if(var3 >= var4) {
            var15 = null;
            break;
         }

         int var5;
         for(var5 = (var3 + var4) / 2; var5 > -1 && var0[var5] != 10; --var5) {
            ;
         }

         int var12 = var5 + 1;

         int var9;
         for(var9 = 1; var0[var12 + var9] != 10; ++var9) {
            ;
         }

         int var13 = var12 + var9 - var12;
         int var6 = 0;
         var5 = 0;
         boolean var7 = false;
         int var10 = var2;

         int var14;
         while(true) {
            int var8;
            if(var7) {
               var8 = 46;
               var7 = false;
            } else {
               var8 = var1[var10][var6] & 255;
            }

            var14 = var8 - (var0[var12 + var5] & 255);
            if(var14 != 0) {
               break;
            }

            ++var5;
            var8 = var6 + 1;
            if(var5 == var13) {
               var6 = var8;
               break;
            }

            var6 = var8;
            int var11 = var10;
            if(var1[var10].length == var8) {
               if(var10 == var1.length - 1) {
                  var6 = var8;
                  break;
               }

               var11 = var10 + 1;
               var6 = -1;
               var7 = true;
            }

            var10 = var11;
         }

         if(var14 < 0) {
            var5 = var12 - 1;
            var4 = var3;
            var3 = var5;
         } else if(var14 > 0) {
            var5 = var9 + var12 + 1;
            var3 = var4;
            var4 = var5;
         } else {
            int var16 = var13 - var5;
            var5 = var1[var10].length - var6;

            for(var6 = var10 + 1; var6 < var1.length; ++var6) {
               var5 += var1[var6].length;
            }

            if(var5 < var16) {
               var5 = var12 - 1;
               var4 = var3;
               var3 = var5;
            } else {
               if(var5 <= var16) {
                  var15 = new String(var0, var12, var13, Util.UTF_8);
                  break;
               }

               var5 = var9 + var12 + 1;
               var3 = var4;
               var4 = var5;
            }
         }

         var5 = var4;
         var4 = var3;
         var3 = var5;
      }

      return var15;
   }

   private String[] findMatchingRule(String[] param1) {
      // $FF: Couldn't be decompiled
   }

   public static PublicSuffixDatabase get() {
      return instance;
   }

   private void readTheList() {
      // $FF: Couldn't be decompiled
   }

   public String getEffectiveTldPlusOne(String var1) {
      if(var1 == null) {
         throw new NullPointerException("domain == null");
      } else {
         String[] var4 = IDN.toUnicode(var1).split("\\.");
         String[] var3 = this.findMatchingRule(var4);
         if(var4.length == var3.length && var3[0].charAt(0) != 33) {
            var1 = null;
         } else {
            int var2;
            if(var3[0].charAt(0) == 33) {
               var2 = var4.length - var3.length;
            } else {
               var2 = var4.length - (var3.length + 1);
            }

            StringBuilder var6 = new StringBuilder();

            for(String[] var5 = var1.split("\\."); var2 < var5.length; ++var2) {
               var6.append(var5[var2]).append('.');
            }

            var6.deleteCharAt(var6.length() - 1);
            var1 = var6.toString();
         }

         return var1;
      }
   }

   void setListBytes(byte[] var1, byte[] var2) {
      this.publicSuffixListBytes = var1;
      this.publicSuffixExceptionListBytes = var2;
      this.listRead.set(true);
      this.readCompleteLatch.countDown();
   }
}
