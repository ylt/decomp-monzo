package okhttp3.internal.http2;

import c.f;
import okhttp3.internal.Util;

public final class Header {
   public static final f PSEUDO_PREFIX = f.a(":");
   public static final f RESPONSE_STATUS = f.a(":status");
   public static final f TARGET_AUTHORITY = f.a(":authority");
   public static final f TARGET_METHOD = f.a(":method");
   public static final f TARGET_PATH = f.a(":path");
   public static final f TARGET_SCHEME = f.a(":scheme");
   final int hpackSize;
   public final f name;
   public final f value;

   public Header(f var1, f var2) {
      this.name = var1;
      this.value = var2;
      this.hpackSize = var1.h() + 32 + var2.h();
   }

   public Header(f var1, String var2) {
      this(var1, f.a(var2));
   }

   public Header(String var1, String var2) {
      this(f.a(var1), f.a(var2));
   }

   public boolean equals(Object var1) {
      boolean var3 = false;
      boolean var2 = var3;
      if(var1 instanceof Header) {
         Header var4 = (Header)var1;
         var2 = var3;
         if(this.name.equals(var4.name)) {
            var2 = var3;
            if(this.value.equals(var4.value)) {
               var2 = true;
            }
         }
      }

      return var2;
   }

   public int hashCode() {
      return (this.name.hashCode() + 527) * 31 + this.value.hashCode();
   }

   public String toString() {
      return Util.format("%s: %s", new Object[]{this.name.a(), this.value.a()});
   }
}
