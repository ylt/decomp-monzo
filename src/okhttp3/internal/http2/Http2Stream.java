package okhttp3.internal.http2;

import c.a;
import c.c;
import c.e;
import c.s;
import c.t;
import c.u;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.SocketTimeoutException;
import java.util.List;

public final class Http2Stream {
   // $FF: synthetic field
   static final boolean $assertionsDisabled;
   long bytesLeftInWriteWindow;
   final Http2Connection connection;
   ErrorCode errorCode = null;
   private boolean hasResponseHeaders;
   final int id;
   final Http2Stream.StreamTimeout readTimeout = new Http2Stream.StreamTimeout();
   private final List requestHeaders;
   private List responseHeaders;
   final Http2Stream.FramingSink sink;
   private final Http2Stream.FramingSource source;
   long unacknowledgedBytesRead = 0L;
   final Http2Stream.StreamTimeout writeTimeout = new Http2Stream.StreamTimeout();

   static {
      boolean var0;
      if(!Http2Stream.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      $assertionsDisabled = var0;
   }

   Http2Stream(int var1, Http2Connection var2, boolean var3, boolean var4, List var5) {
      if(var2 == null) {
         throw new NullPointerException("connection == null");
      } else if(var5 == null) {
         throw new NullPointerException("requestHeaders == null");
      } else {
         this.id = var1;
         this.connection = var2;
         this.bytesLeftInWriteWindow = (long)var2.peerSettings.getInitialWindowSize();
         this.source = new Http2Stream.FramingSource((long)var2.okHttpSettings.getInitialWindowSize());
         this.sink = new Http2Stream.FramingSink();
         this.source.finished = var4;
         this.sink.finished = var3;
         this.requestHeaders = var5;
      }
   }

   private boolean closeInternal(ErrorCode param1) {
      // $FF: Couldn't be decompiled
   }

   void addBytesToWriteWindow(long var1) {
      this.bytesLeftInWriteWindow += var1;
      if(var1 > 0L) {
         this.notifyAll();
      }

   }

   void cancelStreamIfNecessary() throws IOException {
      // $FF: Couldn't be decompiled
   }

   void checkOutNotClosed() throws IOException {
      if(this.sink.closed) {
         throw new IOException("stream closed");
      } else if(this.sink.finished) {
         throw new IOException("stream finished");
      } else if(this.errorCode != null) {
         throw new StreamResetException(this.errorCode);
      }
   }

   public void close(ErrorCode var1) throws IOException {
      if(this.closeInternal(var1)) {
         this.connection.writeSynReset(this.id, var1);
      }

   }

   public void closeLater(ErrorCode var1) {
      if(this.closeInternal(var1)) {
         this.connection.writeSynResetLater(this.id, var1);
      }

   }

   public Http2Connection getConnection() {
      return this.connection;
   }

   public ErrorCode getErrorCode() {
      synchronized(this){}

      ErrorCode var1;
      try {
         var1 = this.errorCode;
      } finally {
         ;
      }

      return var1;
   }

   public int getId() {
      return this.id;
   }

   public List getRequestHeaders() {
      return this.requestHeaders;
   }

   public s getSink() {
      // $FF: Couldn't be decompiled
   }

   public t getSource() {
      return this.source;
   }

   public boolean isLocallyInitiated() {
      boolean var2 = true;
      boolean var1;
      if((this.id & 1) == 1) {
         var1 = true;
      } else {
         var1 = false;
      }

      if(this.connection.client == var1) {
         var1 = var2;
      } else {
         var1 = false;
      }

      return var1;
   }

   public boolean isOpen() {
      // $FF: Couldn't be decompiled
   }

   public u readTimeout() {
      return this.readTimeout;
   }

   void receiveData(e var1, int var2) throws IOException {
      if(!$assertionsDisabled && Thread.holdsLock(this)) {
         throw new AssertionError();
      } else {
         this.source.receive(var1, (long)var2);
      }
   }

   void receiveFin() {
      // $FF: Couldn't be decompiled
   }

   void receiveHeaders(List param1) {
      // $FF: Couldn't be decompiled
   }

   void receiveRstStream(ErrorCode var1) {
      synchronized(this){}

      try {
         if(this.errorCode == null) {
            this.errorCode = var1;
            this.notifyAll();
         }
      } finally {
         ;
      }

   }

   public void sendResponseHeaders(List param1, boolean param2) throws IOException {
      // $FF: Couldn't be decompiled
   }

   public List takeResponseHeaders() throws IOException {
      // $FF: Couldn't be decompiled
   }

   void waitForIo() throws InterruptedIOException {
      try {
         this.wait();
      } catch (InterruptedException var2) {
         throw new InterruptedIOException();
      }
   }

   public u writeTimeout() {
      return this.writeTimeout;
   }

   final class FramingSink implements s {
      // $FF: synthetic field
      static final boolean $assertionsDisabled;
      private static final long EMIT_BUFFER_SIZE = 16384L;
      boolean closed;
      boolean finished;
      private final c sendBuffer = new c();

      static {
         boolean var0;
         if(!Http2Stream.class.desiredAssertionStatus()) {
            var0 = true;
         } else {
            var0 = false;
         }

         $assertionsDisabled = var0;
      }

      private void emitFrame(boolean param1) throws IOException {
         // $FF: Couldn't be decompiled
      }

      public void close() throws IOException {
         // $FF: Couldn't be decompiled
      }

      public void flush() throws IOException {
         // $FF: Couldn't be decompiled
      }

      public u timeout() {
         return Http2Stream.this.writeTimeout;
      }

      public void write(c var1, long var2) throws IOException {
         if(!$assertionsDisabled && Thread.holdsLock(Http2Stream.this)) {
            throw new AssertionError();
         } else {
            this.sendBuffer.write(var1, var2);

            while(this.sendBuffer.a() >= 16384L) {
               this.emitFrame(false);
            }

         }
      }
   }

   private final class FramingSource implements t {
      // $FF: synthetic field
      static final boolean $assertionsDisabled;
      boolean closed;
      boolean finished;
      private final long maxByteCount;
      private final c readBuffer = new c();
      private final c receiveBuffer = new c();

      static {
         boolean var0;
         if(!Http2Stream.class.desiredAssertionStatus()) {
            var0 = true;
         } else {
            var0 = false;
         }

         $assertionsDisabled = var0;
      }

      FramingSource(long var2) {
         this.maxByteCount = var2;
      }

      private void checkNotClosed() throws IOException {
         if(this.closed) {
            throw new IOException("stream closed");
         } else if(Http2Stream.this.errorCode != null) {
            throw new StreamResetException(Http2Stream.this.errorCode);
         }
      }

      private void waitUntilReadable() throws IOException {
         Http2Stream.this.readTimeout.enter();

         while(true) {
            boolean var3 = false;

            try {
               var3 = true;
               if(this.readBuffer.a() == 0L) {
                  if(!this.finished) {
                     if(!this.closed) {
                        if(Http2Stream.this.errorCode == null) {
                           Http2Stream.this.waitForIo();
                           var3 = false;
                           continue;
                        }

                        var3 = false;
                        break;
                     }

                     var3 = false;
                     break;
                  }

                  var3 = false;
                  break;
               }

               var3 = false;
               break;
            } finally {
               if(var3) {
                  Http2Stream.this.readTimeout.exitAndThrowIfTimedOut();
               }
            }
         }

         Http2Stream.this.readTimeout.exitAndThrowIfTimedOut();
      }

      public void close() throws IOException {
         // $FF: Couldn't be decompiled
      }

      public long read(c param1, long param2) throws IOException {
         // $FF: Couldn't be decompiled
      }

      void receive(e param1, long param2) throws IOException {
         // $FF: Couldn't be decompiled
      }

      public u timeout() {
         return Http2Stream.this.readTimeout;
      }
   }

   class StreamTimeout extends a {
      public void exitAndThrowIfTimedOut() throws IOException {
         if(this.exit()) {
            throw this.newTimeoutException((IOException)null);
         }
      }

      protected IOException newTimeoutException(IOException var1) {
         SocketTimeoutException var2 = new SocketTimeoutException("timeout");
         if(var1 != null) {
            var2.initCause(var1);
         }

         return var2;
      }

      protected void timedOut() {
         Http2Stream.this.closeLater(ErrorCode.CANCEL);
      }
   }
}
