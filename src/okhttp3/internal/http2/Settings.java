package okhttp3.internal.http2;

import java.util.Arrays;

public final class Settings {
   static final int COUNT = 10;
   static final int DEFAULT_INITIAL_WINDOW_SIZE = 65535;
   static final int ENABLE_PUSH = 2;
   static final int HEADER_TABLE_SIZE = 1;
   static final int INITIAL_WINDOW_SIZE = 7;
   static final int MAX_CONCURRENT_STREAMS = 4;
   static final int MAX_FRAME_SIZE = 5;
   static final int MAX_HEADER_LIST_SIZE = 6;
   private int set;
   private final int[] values = new int[10];

   void clear() {
      this.set = 0;
      Arrays.fill(this.values, 0);
   }

   int get(int var1) {
      return this.values[var1];
   }

   boolean getEnablePush(boolean var1) {
      boolean var3 = true;
      int var2;
      if((4 & this.set) != 0) {
         var2 = this.values[2];
      } else if(var1) {
         var2 = 1;
      } else {
         var2 = 0;
      }

      if(var2 == 1) {
         var1 = var3;
      } else {
         var1 = false;
      }

      return var1;
   }

   int getHeaderTableSize() {
      int var1;
      if((2 & this.set) != 0) {
         var1 = this.values[1];
      } else {
         var1 = -1;
      }

      return var1;
   }

   int getInitialWindowSize() {
      int var1;
      if((128 & this.set) != 0) {
         var1 = this.values[7];
      } else {
         var1 = '\uffff';
      }

      return var1;
   }

   int getMaxConcurrentStreams(int var1) {
      if((16 & this.set) != 0) {
         var1 = this.values[4];
      }

      return var1;
   }

   int getMaxFrameSize(int var1) {
      if((32 & this.set) != 0) {
         var1 = this.values[5];
      }

      return var1;
   }

   int getMaxHeaderListSize(int var1) {
      if((64 & this.set) != 0) {
         var1 = this.values[6];
      }

      return var1;
   }

   boolean isSet(int var1) {
      boolean var2 = true;
      if((1 << var1 & this.set) == 0) {
         var2 = false;
      }

      return var2;
   }

   void merge(Settings var1) {
      for(int var2 = 0; var2 < 10; ++var2) {
         if(var1.isSet(var2)) {
            this.set(var2, var1.get(var2));
         }
      }

   }

   Settings set(int var1, int var2) {
      if(var1 < this.values.length) {
         this.set |= 1 << var1;
         this.values[var1] = var2;
      }

      return this;
   }

   int size() {
      return Integer.bitCount(this.set);
   }
}
