package okhttp3.internal.http2;

import c.f;
import c.h;
import c.l;
import c.s;
import c.t;
import java.io.IOException;
import java.net.ProtocolException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import okhttp3.Headers;
import okhttp3.OkHttpClient;
import okhttp3.Protocol;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.internal.Internal;
import okhttp3.internal.Util;
import okhttp3.internal.connection.StreamAllocation;
import okhttp3.internal.http.HttpCodec;
import okhttp3.internal.http.RealResponseBody;
import okhttp3.internal.http.RequestLine;
import okhttp3.internal.http.StatusLine;

public final class Http2Codec implements HttpCodec {
   private static final f CONNECTION = f.a("connection");
   private static final f ENCODING = f.a("encoding");
   private static final f HOST = f.a("host");
   private static final List HTTP_2_SKIPPED_REQUEST_HEADERS;
   private static final List HTTP_2_SKIPPED_RESPONSE_HEADERS;
   private static final f KEEP_ALIVE = f.a("keep-alive");
   private static final f PROXY_CONNECTION = f.a("proxy-connection");
   private static final f TE = f.a("te");
   private static final f TRANSFER_ENCODING = f.a("transfer-encoding");
   private static final f UPGRADE = f.a("upgrade");
   private final OkHttpClient client;
   private final Http2Connection connection;
   private Http2Stream stream;
   final StreamAllocation streamAllocation;

   static {
      HTTP_2_SKIPPED_REQUEST_HEADERS = Util.immutableList((Object[])(new f[]{CONNECTION, HOST, KEEP_ALIVE, PROXY_CONNECTION, TE, TRANSFER_ENCODING, ENCODING, UPGRADE, Header.TARGET_METHOD, Header.TARGET_PATH, Header.TARGET_SCHEME, Header.TARGET_AUTHORITY}));
      HTTP_2_SKIPPED_RESPONSE_HEADERS = Util.immutableList((Object[])(new f[]{CONNECTION, HOST, KEEP_ALIVE, PROXY_CONNECTION, TE, TRANSFER_ENCODING, ENCODING, UPGRADE}));
   }

   public Http2Codec(OkHttpClient var1, StreamAllocation var2, Http2Connection var3) {
      this.client = var1;
      this.streamAllocation = var2;
      this.connection = var3;
   }

   public static List http2HeadersList(Request var0) {
      Headers var3 = var0.headers();
      ArrayList var4 = new ArrayList(var3.size() + 4);
      var4.add(new Header(Header.TARGET_METHOD, var0.method()));
      var4.add(new Header(Header.TARGET_PATH, RequestLine.requestPath(var0.url())));
      String var5 = var0.header("Host");
      if(var5 != null) {
         var4.add(new Header(Header.TARGET_AUTHORITY, var5));
      }

      var4.add(new Header(Header.TARGET_SCHEME, var0.url().scheme()));
      int var1 = 0;

      for(int var2 = var3.size(); var1 < var2; ++var1) {
         f var6 = f.a(var3.name(var1).toLowerCase(Locale.US));
         if(!HTTP_2_SKIPPED_REQUEST_HEADERS.contains(var6)) {
            var4.add(new Header(var6, var3.value(var1)));
         }
      }

      return var4;
   }

   public static Response.Builder readHttp2HeadersList(List var0) throws IOException {
      Headers.Builder var3 = new Headers.Builder();
      int var2 = var0.size();
      int var1 = 0;

      StatusLine var4;
      for(var4 = null; var1 < var2; ++var1) {
         Header var6 = (Header)var0.get(var1);
         if(var6 == null) {
            if(var4 != null && var4.code == 100) {
               var3 = new Headers.Builder();
               var4 = null;
            }
         } else {
            f var5 = var6.name;
            String var7 = var6.value.a();
            if(var5.equals(Header.RESPONSE_STATUS)) {
               var4 = StatusLine.parse("HTTP/1.1 " + var7);
            } else if(!HTTP_2_SKIPPED_RESPONSE_HEADERS.contains(var5)) {
               Internal.instance.addLenient(var3, var5.a(), var7);
            }
         }
      }

      if(var4 == null) {
         throw new ProtocolException("Expected ':status' header not present");
      } else {
         return (new Response.Builder()).protocol(Protocol.HTTP_2).code(var4.code).message(var4.message).headers(var3.build());
      }
   }

   public void cancel() {
      if(this.stream != null) {
         this.stream.closeLater(ErrorCode.CANCEL);
      }

   }

   public s createRequestBody(Request var1, long var2) {
      return this.stream.getSink();
   }

   public void finishRequest() throws IOException {
      this.stream.getSink().close();
   }

   public void flushRequest() throws IOException {
      this.connection.flush();
   }

   public ResponseBody openResponseBody(Response var1) throws IOException {
      Http2Codec.StreamFinishingSource var2 = new Http2Codec.StreamFinishingSource(this.stream.getSource());
      return new RealResponseBody(var1.headers(), l.a((t)var2));
   }

   public Response.Builder readResponseHeaders(boolean var1) throws IOException {
      Response.Builder var3 = readHttp2HeadersList(this.stream.takeResponseHeaders());
      Response.Builder var2 = var3;
      if(var1) {
         var2 = var3;
         if(Internal.instance.code(var3) == 100) {
            var2 = null;
         }
      }

      return var2;
   }

   public void writeRequestHeaders(Request var1) throws IOException {
      if(this.stream == null) {
         boolean var2;
         if(var1.body() != null) {
            var2 = true;
         } else {
            var2 = false;
         }

         List var3 = http2HeadersList(var1);
         this.stream = this.connection.newStream(var3, var2);
         this.stream.readTimeout().timeout((long)this.client.readTimeoutMillis(), TimeUnit.MILLISECONDS);
         this.stream.writeTimeout().timeout((long)this.client.writeTimeoutMillis(), TimeUnit.MILLISECONDS);
      }

   }

   class StreamFinishingSource extends h {
      StreamFinishingSource(t var2) {
         super(var2);
      }

      public void close() throws IOException {
         Http2Codec.this.streamAllocation.streamFinished(false, Http2Codec.this);
         super.close();
      }
   }
}
