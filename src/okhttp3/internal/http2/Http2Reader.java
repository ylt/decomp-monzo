package okhttp3.internal.http2;

import c.c;
import c.e;
import c.f;
import c.t;
import c.u;
import java.io.Closeable;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import okhttp3.internal.Util;

final class Http2Reader implements Closeable {
   static final Logger logger = Logger.getLogger(Http2.class.getName());
   private final boolean client;
   private final Http2Reader.ContinuationSource continuation;
   final Hpack.Reader hpackReader;
   private final e source;

   Http2Reader(e var1, boolean var2) {
      this.source = var1;
      this.client = var2;
      this.continuation = new Http2Reader.ContinuationSource(this.source);
      this.hpackReader = new Hpack.Reader(4096, this.continuation);
   }

   static int lengthWithoutPadding(int var0, byte var1, short var2) throws IOException {
      int var3 = var0;
      if((var1 & 8) != 0) {
         var3 = var0 - 1;
      }

      if(var2 > var3) {
         throw Http2.ioException("PROTOCOL_ERROR padding %s > remaining length %s", new Object[]{Short.valueOf(var2), Integer.valueOf(var3)});
      } else {
         return (short)(var3 - var2);
      }
   }

   private void readData(Http2Reader.Handler var1, int var2, byte var3, int var4) throws IOException {
      boolean var6 = true;
      short var5 = 0;
      if(var4 == 0) {
         throw Http2.ioException("PROTOCOL_ERROR: TYPE_DATA streamId == 0", new Object[0]);
      } else {
         boolean var7;
         if((var3 & 1) != 0) {
            var7 = true;
         } else {
            var7 = false;
         }

         if((var3 & 32) == 0) {
            var6 = false;
         }

         if(var6) {
            throw Http2.ioException("PROTOCOL_ERROR: FLAG_COMPRESSED without SETTINGS_COMPRESS_DATA", new Object[0]);
         } else {
            if((var3 & 8) != 0) {
               var5 = (short)(this.source.i() & 255);
            }

            var2 = lengthWithoutPadding(var2, var3, var5);
            var1.data(var7, var4, this.source, var2);
            this.source.i((long)var5);
         }
      }
   }

   private void readGoAway(Http2Reader.Handler var1, int var2, byte var3, int var4) throws IOException {
      if(var2 < 8) {
         throw Http2.ioException("TYPE_GOAWAY length < 8: %s", new Object[]{Integer.valueOf(var2)});
      } else if(var4 != 0) {
         throw Http2.ioException("TYPE_GOAWAY streamId != 0", new Object[0]);
      } else {
         var4 = this.source.k();
         int var7 = this.source.k();
         var2 -= 8;
         ErrorCode var6 = ErrorCode.fromHttp2(var7);
         if(var6 == null) {
            throw Http2.ioException("TYPE_GOAWAY unexpected error code: %d", new Object[]{Integer.valueOf(var7)});
         } else {
            f var5 = f.b;
            if(var2 > 0) {
               var5 = this.source.d((long)var2);
            }

            var1.goAway(var4, var6, var5);
         }
      }
   }

   private List readHeaderBlock(int var1, short var2, byte var3, int var4) throws IOException {
      Http2Reader.ContinuationSource var5 = this.continuation;
      this.continuation.left = var1;
      var5.length = var1;
      this.continuation.padding = var2;
      this.continuation.flags = var3;
      this.continuation.streamId = var4;
      this.hpackReader.readHeaders();
      return this.hpackReader.getAndResetHeaderList();
   }

   private void readHeaders(Http2Reader.Handler var1, int var2, byte var3, int var4) throws IOException {
      short var5 = 0;
      if(var4 == 0) {
         throw Http2.ioException("PROTOCOL_ERROR: TYPE_HEADERS streamId == 0", new Object[0]);
      } else {
         boolean var7;
         if((var3 & 1) != 0) {
            var7 = true;
         } else {
            var7 = false;
         }

         if((var3 & 8) != 0) {
            var5 = (short)(this.source.i() & 255);
         }

         int var6 = var2;
         if((var3 & 32) != 0) {
            this.readPriority(var1, var4);
            var6 = var2 - 5;
         }

         var1.headers(var7, var4, -1, this.readHeaderBlock(lengthWithoutPadding(var6, var3, var5), var5, var3, var4));
      }
   }

   static int readMedium(e var0) throws IOException {
      return (var0.i() & 255) << 16 | (var0.i() & 255) << 8 | var0.i() & 255;
   }

   private void readPing(Http2Reader.Handler var1, int var2, byte var3, int var4) throws IOException {
      boolean var5 = true;
      if(var2 != 8) {
         throw Http2.ioException("TYPE_PING length != 8: %s", new Object[]{Integer.valueOf(var2)});
      } else if(var4 != 0) {
         throw Http2.ioException("TYPE_PING streamId != 0", new Object[0]);
      } else {
         var2 = this.source.k();
         var4 = this.source.k();
         if((var3 & 1) == 0) {
            var5 = false;
         }

         var1.ping(var5, var2, var4);
      }
   }

   private void readPriority(Http2Reader.Handler var1, int var2) throws IOException {
      int var3 = this.source.k();
      boolean var4;
      if((Integer.MIN_VALUE & var3) != 0) {
         var4 = true;
      } else {
         var4 = false;
      }

      var1.priority(var2, var3 & Integer.MAX_VALUE, (this.source.i() & 255) + 1, var4);
   }

   private void readPriority(Http2Reader.Handler var1, int var2, byte var3, int var4) throws IOException {
      if(var2 != 5) {
         throw Http2.ioException("TYPE_PRIORITY length: %d != 5", new Object[]{Integer.valueOf(var2)});
      } else if(var4 == 0) {
         throw Http2.ioException("TYPE_PRIORITY streamId == 0", new Object[0]);
      } else {
         this.readPriority(var1, var4);
      }
   }

   private void readPushPromise(Http2Reader.Handler var1, int var2, byte var3, int var4) throws IOException {
      short var5 = 0;
      if(var4 == 0) {
         throw Http2.ioException("PROTOCOL_ERROR: TYPE_PUSH_PROMISE streamId == 0", new Object[0]);
      } else {
         if((var3 & 8) != 0) {
            var5 = (short)(this.source.i() & 255);
         }

         var1.pushPromise(var4, this.source.k() & Integer.MAX_VALUE, this.readHeaderBlock(lengthWithoutPadding(var2 - 4, var3, var5), var5, var3, var4));
      }
   }

   private void readRstStream(Http2Reader.Handler var1, int var2, byte var3, int var4) throws IOException {
      if(var2 != 4) {
         throw Http2.ioException("TYPE_RST_STREAM length: %d != 4", new Object[]{Integer.valueOf(var2)});
      } else if(var4 == 0) {
         throw Http2.ioException("TYPE_RST_STREAM streamId == 0", new Object[0]);
      } else {
         var2 = this.source.k();
         ErrorCode var5 = ErrorCode.fromHttp2(var2);
         if(var5 == null) {
            throw Http2.ioException("TYPE_RST_STREAM unexpected error code: %d", new Object[]{Integer.valueOf(var2)});
         } else {
            var1.rstStream(var4, var5);
         }
      }
   }

   private void readSettings(Http2Reader.Handler var1, int var2, byte var3, int var4) throws IOException {
      if(var4 != 0) {
         throw Http2.ioException("TYPE_SETTINGS streamId != 0", new Object[0]);
      } else if((var3 & 1) != 0) {
         if(var2 != 0) {
            throw Http2.ioException("FRAME_SIZE_ERROR ack frame should be empty!", new Object[0]);
         } else {
            var1.ackSettings();
         }
      } else if(var2 % 6 != 0) {
         throw Http2.ioException("TYPE_SETTINGS length %% 6 != 0: %s", new Object[]{Integer.valueOf(var2)});
      } else {
         Settings var7 = new Settings();

         for(var4 = 0; var4 < var2; var4 += 6) {
            short var5 = this.source.j();
            int var6 = this.source.k();
            short var8 = var5;
            switch(var5) {
            case 1:
            case 6:
               break;
            case 2:
               var8 = var5;
               if(var6 != 0) {
                  var8 = var5;
                  if(var6 != 1) {
                     throw Http2.ioException("PROTOCOL_ERROR SETTINGS_ENABLE_PUSH != 0 or 1", new Object[0]);
                  }
               }
               break;
            case 3:
               var8 = 4;
               break;
            case 4:
               var8 = 7;
               if(var6 < 0) {
                  throw Http2.ioException("PROTOCOL_ERROR SETTINGS_INITIAL_WINDOW_SIZE > 2^31 - 1", new Object[0]);
               }
               break;
            case 5:
               if(var6 < 16384) {
                  throw Http2.ioException("PROTOCOL_ERROR SETTINGS_MAX_FRAME_SIZE: %s", new Object[]{Integer.valueOf(var6)});
               }

               var8 = var5;
               if(var6 > 16777215) {
                  throw Http2.ioException("PROTOCOL_ERROR SETTINGS_MAX_FRAME_SIZE: %s", new Object[]{Integer.valueOf(var6)});
               }
               break;
            default:
               var8 = var5;
            }

            var7.set(var8, var6);
         }

         var1.settings(false, var7);
      }
   }

   private void readWindowUpdate(Http2Reader.Handler var1, int var2, byte var3, int var4) throws IOException {
      if(var2 != 4) {
         throw Http2.ioException("TYPE_WINDOW_UPDATE length !=4: %s", new Object[]{Integer.valueOf(var2)});
      } else {
         long var5 = (long)this.source.k() & 2147483647L;
         if(var5 == 0L) {
            throw Http2.ioException("windowSizeIncrement was 0", new Object[]{Long.valueOf(var5)});
         } else {
            var1.windowUpdate(var4, var5);
         }
      }
   }

   public void close() throws IOException {
      this.source.close();
   }

   public boolean nextFrame(boolean var1, Http2Reader.Handler var2) throws IOException {
      boolean var7 = true;

      try {
         this.source.a(9L);
      } catch (IOException var8) {
         var1 = false;
         return var1;
      }

      int var6 = readMedium(this.source);
      if(var6 < 0 || var6 > 16384) {
         throw Http2.ioException("FRAME_SIZE_ERROR: %s", new Object[]{Integer.valueOf(var6)});
      } else {
         byte var3 = (byte)(this.source.i() & 255);
         if(var1 && var3 != 4) {
            throw Http2.ioException("Expected a SETTINGS frame but was %s", new Object[]{Byte.valueOf(var3)});
         } else {
            byte var4 = (byte)(this.source.i() & 255);
            int var5 = this.source.k() & Integer.MAX_VALUE;
            if(logger.isLoggable(Level.FINE)) {
               logger.fine(Http2.frameLog(true, var5, var6, var3, var4));
            }

            switch(var3) {
            case 0:
               this.readData(var2, var6, var4, var5);
               var1 = var7;
               break;
            case 1:
               this.readHeaders(var2, var6, var4, var5);
               var1 = var7;
               break;
            case 2:
               this.readPriority(var2, var6, var4, var5);
               var1 = var7;
               break;
            case 3:
               this.readRstStream(var2, var6, var4, var5);
               var1 = var7;
               break;
            case 4:
               this.readSettings(var2, var6, var4, var5);
               var1 = var7;
               break;
            case 5:
               this.readPushPromise(var2, var6, var4, var5);
               var1 = var7;
               break;
            case 6:
               this.readPing(var2, var6, var4, var5);
               var1 = var7;
               break;
            case 7:
               this.readGoAway(var2, var6, var4, var5);
               var1 = var7;
               break;
            case 8:
               this.readWindowUpdate(var2, var6, var4, var5);
               var1 = var7;
               break;
            default:
               this.source.i((long)var6);
               var1 = var7;
            }

            return var1;
         }
      }
   }

   public void readConnectionPreface(Http2Reader.Handler var1) throws IOException {
      if(this.client) {
         if(!this.nextFrame(true, var1)) {
            throw Http2.ioException("Required SETTINGS preface not received", new Object[0]);
         }
      } else {
         f var2 = this.source.d((long)Http2.CONNECTION_PREFACE.h());
         if(logger.isLoggable(Level.FINE)) {
            logger.fine(Util.format("<< CONNECTION %s", new Object[]{var2.f()}));
         }

         if(!Http2.CONNECTION_PREFACE.equals(var2)) {
            throw Http2.ioException("Expected a connection header but was %s", new Object[]{var2.a()});
         }
      }

   }

   static final class ContinuationSource implements t {
      byte flags;
      int left;
      int length;
      short padding;
      private final e source;
      int streamId;

      ContinuationSource(e var1) {
         this.source = var1;
      }

      private void readContinuationHeader() throws IOException {
         int var2 = this.streamId;
         int var3 = Http2Reader.readMedium(this.source);
         this.left = var3;
         this.length = var3;
         byte var1 = (byte)(this.source.i() & 255);
         this.flags = (byte)(this.source.i() & 255);
         if(Http2Reader.logger.isLoggable(Level.FINE)) {
            Http2Reader.logger.fine(Http2.frameLog(true, this.streamId, this.length, var1, this.flags));
         }

         this.streamId = this.source.k() & Integer.MAX_VALUE;
         if(var1 != 9) {
            throw Http2.ioException("%s != TYPE_CONTINUATION", new Object[]{Byte.valueOf(var1)});
         } else if(this.streamId != var2) {
            throw Http2.ioException("TYPE_CONTINUATION streamId changed", new Object[0]);
         }
      }

      public void close() throws IOException {
      }

      public long read(c var1, long var2) throws IOException {
         long var4 = -1L;

         while(true) {
            if(this.left != 0) {
               long var6 = this.source.read(var1, Math.min(var2, (long)this.left));
               var2 = var4;
               if(var6 != -1L) {
                  this.left = (int)((long)this.left - var6);
                  var2 = var6;
               }
               break;
            }

            this.source.i((long)this.padding);
            this.padding = 0;
            if((this.flags & 4) != 0) {
               var2 = var4;
               break;
            }

            this.readContinuationHeader();
         }

         return var2;
      }

      public u timeout() {
         return this.source.timeout();
      }
   }

   interface Handler {
      void ackSettings();

      void alternateService(int var1, String var2, f var3, String var4, int var5, long var6);

      void data(boolean var1, int var2, e var3, int var4) throws IOException;

      void goAway(int var1, ErrorCode var2, f var3);

      void headers(boolean var1, int var2, int var3, List var4);

      void ping(boolean var1, int var2, int var3);

      void priority(int var1, int var2, int var3, boolean var4);

      void pushPromise(int var1, int var2, List var3) throws IOException;

      void rstStream(int var1, ErrorCode var2);

      void settings(boolean var1, Settings var2);

      void windowUpdate(int var1, long var2);
   }
}
