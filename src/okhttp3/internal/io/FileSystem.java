package okhttp3.internal.io;

import c.l;
import c.s;
import c.t;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public interface FileSystem {
   FileSystem SYSTEM = new FileSystem() {
      public s appendingSink(File var1) throws FileNotFoundException {
         s var2;
         s var4;
         try {
            var2 = l.c(var1);
         } catch (FileNotFoundException var3) {
            var1.getParentFile().mkdirs();
            var4 = l.c(var1);
            return var4;
         }

         var4 = var2;
         return var4;
      }

      public void delete(File var1) throws IOException {
         if(!var1.delete() && var1.exists()) {
            throw new IOException("failed to delete " + var1);
         }
      }

      public void deleteContents(File var1) throws IOException {
         File[] var4 = var1.listFiles();
         if(var4 == null) {
            throw new IOException("not a readable directory: " + var1);
         } else {
            int var3 = var4.length;

            for(int var2 = 0; var2 < var3; ++var2) {
               var1 = var4[var2];
               if(var1.isDirectory()) {
                  this.deleteContents(var1);
               }

               if(!var1.delete()) {
                  throw new IOException("failed to delete " + var1);
               }
            }

         }
      }

      public boolean exists(File var1) {
         return var1.exists();
      }

      public void rename(File var1, File var2) throws IOException {
         this.delete(var2);
         if(!var1.renameTo(var2)) {
            throw new IOException("failed to rename " + var1 + " to " + var2);
         }
      }

      public s sink(File var1) throws FileNotFoundException {
         s var2;
         s var4;
         try {
            var2 = l.b(var1);
         } catch (FileNotFoundException var3) {
            var1.getParentFile().mkdirs();
            var4 = l.b(var1);
            return var4;
         }

         var4 = var2;
         return var4;
      }

      public long size(File var1) {
         return var1.length();
      }

      public t source(File var1) throws FileNotFoundException {
         return l.a(var1);
      }
   };

   s appendingSink(File var1) throws FileNotFoundException;

   void delete(File var1) throws IOException;

   void deleteContents(File var1) throws IOException;

   boolean exists(File var1);

   void rename(File var1, File var2) throws IOException;

   s sink(File var1) throws FileNotFoundException;

   long size(File var1);

   t source(File var1) throws FileNotFoundException;
}
