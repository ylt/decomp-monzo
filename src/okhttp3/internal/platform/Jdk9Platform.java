package okhttp3.internal.platform;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import javax.net.ssl.SSLParameters;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.X509TrustManager;

final class Jdk9Platform extends Platform {
   final Method getProtocolMethod;
   final Method setProtocolMethod;

   Jdk9Platform(Method var1, Method var2) {
      this.setProtocolMethod = var1;
      this.getProtocolMethod = var2;
   }

   public static Jdk9Platform buildIfSupported() {
      Jdk9Platform var0;
      try {
         Method var1 = SSLParameters.class.getMethod("setApplicationProtocols", new Class[]{String[].class});
         Method var2 = SSLSocket.class.getMethod("getApplicationProtocol", new Class[0]);
         var0 = new Jdk9Platform(var1, var2);
      } catch (NoSuchMethodException var3) {
         var0 = null;
      }

      return var0;
   }

   public void configureTlsExtensions(SSLSocket var1, String var2, List var3) {
      try {
         SSLParameters var6 = var1.getSSLParameters();
         var3 = alpnProtocolNames(var3);
         this.setProtocolMethod.invoke(var6, new Object[]{var3.toArray(new String[var3.size()])});
         var1.setSSLParameters(var6);
         return;
      } catch (IllegalAccessException var4) {
         ;
      } catch (InvocationTargetException var5) {
         ;
      }

      throw new AssertionError();
   }

   public String getSelectedProtocol(SSLSocket param1) {
      // $FF: Couldn't be decompiled
   }

   public X509TrustManager trustManager(SSLSocketFactory var1) {
      throw new UnsupportedOperationException("clientBuilder.sslSocketFactory(SSLSocketFactory) not supported on JDK 9+");
   }
}
