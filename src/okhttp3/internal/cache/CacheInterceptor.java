package okhttp3.internal.cache;

import c.c;
import c.d;
import c.e;
import c.l;
import c.s;
import c.t;
import c.u;
import java.io.Closeable;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import okhttp3.Headers;
import okhttp3.Interceptor;
import okhttp3.Protocol;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.internal.Internal;
import okhttp3.internal.Util;
import okhttp3.internal.http.HttpHeaders;
import okhttp3.internal.http.HttpMethod;
import okhttp3.internal.http.RealResponseBody;

public final class CacheInterceptor implements Interceptor {
   final InternalCache cache;

   public CacheInterceptor(InternalCache var1) {
      this.cache = var1;
   }

   private Response cacheWritingResponse(final CacheRequest var1, Response var2) throws IOException {
      Response var3;
      if(var1 == null) {
         var3 = var2;
      } else {
         s var4 = var1.body();
         var3 = var2;
         if(var4 != null) {
            t var5 = new t(var2.body().source(), l.a(var4)) {
               boolean cacheRequestClosed;
               // $FF: synthetic field
               final d val$cacheBody;
               // $FF: synthetic field
               final e val$source;

               {
                  this.val$source = var2;
                  this.val$cacheBody = var4;
               }

               public void close() throws IOException {
                  if(!this.cacheRequestClosed && !Util.discard(this, 100, TimeUnit.MILLISECONDS)) {
                     this.cacheRequestClosed = true;
                     var1.abort();
                  }

                  this.val$source.close();
               }

               public long read(c var1x, long var2) throws IOException {
                  try {
                     var2 = this.val$source.read(var1x, var2);
                  } catch (IOException var4) {
                     if(!this.cacheRequestClosed) {
                        this.cacheRequestClosed = true;
                        var1.abort();
                     }

                     throw var4;
                  }

                  if(var2 == -1L) {
                     if(!this.cacheRequestClosed) {
                        this.cacheRequestClosed = true;
                        this.val$cacheBody.close();
                     }

                     var2 = -1L;
                  } else {
                     var1x.a(this.val$cacheBody.b(), var1x.a() - var2, var2);
                     this.val$cacheBody.y();
                  }

                  return var2;
               }

               public u timeout() {
                  return this.val$source.timeout();
               }
            };
            var3 = var2.newBuilder().body(new RealResponseBody(var2.headers(), l.a(var5))).build();
         }
      }

      return var3;
   }

   private static Headers combine(Headers var0, Headers var1) {
      byte var3 = 0;
      Headers.Builder var5 = new Headers.Builder();
      int var4 = var0.size();

      int var2;
      for(var2 = 0; var2 < var4; ++var2) {
         String var7 = var0.name(var2);
         String var6 = var0.value(var2);
         if((!"Warning".equalsIgnoreCase(var7) || !var6.startsWith("1")) && (!isEndToEnd(var7) || var1.get(var7) == null)) {
            Internal.instance.addLenient(var5, var7, var6);
         }
      }

      var4 = var1.size();

      for(var2 = var3; var2 < var4; ++var2) {
         String var8 = var1.name(var2);
         if(!"Content-Length".equalsIgnoreCase(var8) && isEndToEnd(var8)) {
            Internal.instance.addLenient(var5, var8, var1.value(var2));
         }
      }

      return var5.build();
   }

   static boolean isEndToEnd(String var0) {
      boolean var1;
      if(!"Connection".equalsIgnoreCase(var0) && !"Keep-Alive".equalsIgnoreCase(var0) && !"Proxy-Authenticate".equalsIgnoreCase(var0) && !"Proxy-Authorization".equalsIgnoreCase(var0) && !"TE".equalsIgnoreCase(var0) && !"Trailers".equalsIgnoreCase(var0) && !"Transfer-Encoding".equalsIgnoreCase(var0) && !"Upgrade".equalsIgnoreCase(var0)) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   private static Response stripBody(Response var0) {
      Response var1 = var0;
      if(var0 != null) {
         var1 = var0;
         if(var0.body() != null) {
            var1 = var0.newBuilder().body((ResponseBody)null).build();
         }
      }

      return var1;
   }

   public Response intercept(Interceptor.Chain var1) throws IOException {
      Response var2;
      if(this.cache != null) {
         var2 = this.cache.get(var1.request());
      } else {
         var2 = null;
      }

      CacheStrategy var5 = (new CacheStrategy.Factory(System.currentTimeMillis(), var1.request(), var2)).get();
      Request var3 = var5.networkRequest;
      Response var4 = var5.cacheResponse;
      if(this.cache != null) {
         this.cache.trackResponse(var5);
      }

      if(var2 != null && var4 == null) {
         Util.closeQuietly((Closeable)var2.body());
      }

      Response var11;
      if(var3 == null && var4 == null) {
         var11 = (new Response.Builder()).request(var1.request()).protocol(Protocol.HTTP_1_1).code(504).message("Unsatisfiable Request (only-if-cached)").body(Util.EMPTY_RESPONSE).sentRequestAtMillis(-1L).receivedResponseAtMillis(System.currentTimeMillis()).build();
      } else if(var3 == null) {
         var11 = var4.newBuilder().cacheResponse(stripBody(var4)).build();
      } else {
         boolean var8 = false;

         Response var12;
         try {
            var8 = true;
            var12 = var1.proceed(var3);
            var8 = false;
         } finally {
            if(var8) {
               if(true && var2 != null) {
                  Util.closeQuietly((Closeable)var2.body());
               }

            }
         }

         if(var12 == null && var2 != null) {
            Util.closeQuietly((Closeable)var2.body());
         }

         if(var4 != null) {
            if(var12.code() == 304) {
               var11 = var4.newBuilder().headers(combine(var4.headers(), var12.headers())).sentRequestAtMillis(var12.sentRequestAtMillis()).receivedResponseAtMillis(var12.receivedResponseAtMillis()).cacheResponse(stripBody(var4)).networkResponse(stripBody(var12)).build();
               var12.body().close();
               this.cache.trackConditionalCacheHit();
               this.cache.update(var4, var11);
               return var11;
            }

            Util.closeQuietly((Closeable)var4.body());
         }

         var2 = var12.newBuilder().cacheResponse(stripBody(var4)).networkResponse(stripBody(var12)).build();
         var11 = var2;
         if(this.cache != null) {
            if(HttpHeaders.hasBody(var2) && CacheStrategy.isCacheable(var2, var3)) {
               var11 = this.cacheWritingResponse(this.cache.put(var2), var2);
            } else {
               var11 = var2;
               if(HttpMethod.invalidatesCache(var3.method())) {
                  try {
                     this.cache.remove(var3);
                  } catch (IOException var10) {
                     var11 = var2;
                     return var11;
                  }

                  var11 = var2;
               }
            }
         }
      }

      return var11;
   }
}
