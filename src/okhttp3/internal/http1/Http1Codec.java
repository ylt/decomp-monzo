package okhttp3.internal.http1;

import c.c;
import c.d;
import c.e;
import c.i;
import c.l;
import c.s;
import c.t;
import c.u;
import java.io.IOException;
import java.net.ProtocolException;
import java.util.concurrent.TimeUnit;
import okhttp3.Headers;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.internal.Internal;
import okhttp3.internal.Util;
import okhttp3.internal.connection.RealConnection;
import okhttp3.internal.connection.StreamAllocation;
import okhttp3.internal.http.HttpCodec;
import okhttp3.internal.http.HttpHeaders;
import okhttp3.internal.http.RealResponseBody;
import okhttp3.internal.http.RequestLine;

public final class Http1Codec implements HttpCodec {
   private static final int STATE_CLOSED = 6;
   private static final int STATE_IDLE = 0;
   private static final int STATE_OPEN_REQUEST_BODY = 1;
   private static final int STATE_OPEN_RESPONSE_BODY = 4;
   private static final int STATE_READING_RESPONSE_BODY = 5;
   private static final int STATE_READ_RESPONSE_HEADERS = 3;
   private static final int STATE_WRITING_REQUEST_BODY = 2;
   final OkHttpClient client;
   final d sink;
   final e source;
   int state = 0;
   final StreamAllocation streamAllocation;

   public Http1Codec(OkHttpClient var1, StreamAllocation var2, e var3, d var4) {
      this.client = var1;
      this.streamAllocation = var2;
      this.source = var3;
      this.sink = var4;
   }

   private t getTransferStream(Response var1) throws IOException {
      t var4;
      if(!HttpHeaders.hasBody(var1)) {
         var4 = this.newFixedLengthSource(0L);
      } else if("chunked".equalsIgnoreCase(var1.header("Transfer-Encoding"))) {
         var4 = this.newChunkedSource(var1.request().url());
      } else {
         long var2 = HttpHeaders.contentLength(var1);
         if(var2 != -1L) {
            var4 = this.newFixedLengthSource(var2);
         } else {
            var4 = this.newUnknownLengthSource();
         }
      }

      return var4;
   }

   public void cancel() {
      RealConnection var1 = this.streamAllocation.connection();
      if(var1 != null) {
         var1.cancel();
      }

   }

   public s createRequestBody(Request var1, long var2) {
      s var4;
      if("chunked".equalsIgnoreCase(var1.header("Transfer-Encoding"))) {
         var4 = this.newChunkedSink();
      } else {
         if(var2 == -1L) {
            throw new IllegalStateException("Cannot stream a request body without chunked encoding or a known content length!");
         }

         var4 = this.newFixedLengthSink(var2);
      }

      return var4;
   }

   void detachTimeout(i var1) {
      u var2 = var1.a();
      var1.a(u.NONE);
      var2.clearDeadline();
      var2.clearTimeout();
   }

   public void finishRequest() throws IOException {
      this.sink.flush();
   }

   public void flushRequest() throws IOException {
      this.sink.flush();
   }

   public boolean isClosed() {
      boolean var1;
      if(this.state == 6) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public s newChunkedSink() {
      if(this.state != 1) {
         throw new IllegalStateException("state: " + this.state);
      } else {
         this.state = 2;
         return new Http1Codec.ChunkedSink();
      }
   }

   public t newChunkedSource(HttpUrl var1) throws IOException {
      if(this.state != 4) {
         throw new IllegalStateException("state: " + this.state);
      } else {
         this.state = 5;
         return new Http1Codec.ChunkedSource(var1);
      }
   }

   public s newFixedLengthSink(long var1) {
      if(this.state != 1) {
         throw new IllegalStateException("state: " + this.state);
      } else {
         this.state = 2;
         return new Http1Codec.FixedLengthSink(var1);
      }
   }

   public t newFixedLengthSource(long var1) throws IOException {
      if(this.state != 4) {
         throw new IllegalStateException("state: " + this.state);
      } else {
         this.state = 5;
         return new Http1Codec.FixedLengthSource(var1);
      }
   }

   public t newUnknownLengthSource() throws IOException {
      if(this.state != 4) {
         throw new IllegalStateException("state: " + this.state);
      } else if(this.streamAllocation == null) {
         throw new IllegalStateException("streamAllocation == null");
      } else {
         this.state = 5;
         this.streamAllocation.noNewStreams();
         return new Http1Codec.UnknownLengthSource();
      }
   }

   public ResponseBody openResponseBody(Response var1) throws IOException {
      t var2 = this.getTransferStream(var1);
      return new RealResponseBody(var1.headers(), l.a(var2));
   }

   public Headers readHeaders() throws IOException {
      Headers.Builder var1 = new Headers.Builder();

      while(true) {
         String var2 = this.source.s();
         if(var2.length() == 0) {
            return var1.build();
         }

         Internal.instance.addLenient(var1, var2);
      }
   }

   public Response.Builder readResponseHeaders(boolean param1) throws IOException {
      // $FF: Couldn't be decompiled
   }

   public void writeRequest(Headers var1, String var2) throws IOException {
      if(this.state != 0) {
         throw new IllegalStateException("state: " + this.state);
      } else {
         this.sink.b(var2).b("\r\n");
         int var3 = 0;

         for(int var4 = var1.size(); var3 < var4; ++var3) {
            this.sink.b(var1.name(var3)).b(": ").b(var1.value(var3)).b("\r\n");
         }

         this.sink.b("\r\n");
         this.state = 1;
      }
   }

   public void writeRequestHeaders(Request var1) throws IOException {
      String var2 = RequestLine.get(var1, this.streamAllocation.connection().route().proxy().type());
      this.writeRequest(var1.headers(), var2);
   }

   private abstract class AbstractSource implements t {
      protected boolean closed;
      protected final i timeout;

      private AbstractSource() {
         this.timeout = new i(Http1Codec.this.source.timeout());
      }

      // $FF: synthetic method
      AbstractSource(Object var2) {
         this();
      }

      protected final void endOfInput(boolean var1) throws IOException {
         if(Http1Codec.this.state != 6) {
            if(Http1Codec.this.state != 5) {
               throw new IllegalStateException("state: " + Http1Codec.this.state);
            }

            Http1Codec.this.detachTimeout(this.timeout);
            Http1Codec.this.state = 6;
            if(Http1Codec.this.streamAllocation != null) {
               StreamAllocation var2 = Http1Codec.this.streamAllocation;
               if(!var1) {
                  var1 = true;
               } else {
                  var1 = false;
               }

               var2.streamFinished(var1, Http1Codec.this);
            }
         }

      }

      public u timeout() {
         return this.timeout;
      }
   }

   private final class ChunkedSink implements s {
      private boolean closed;
      private final i timeout;

      ChunkedSink() {
         this.timeout = new i(Http1Codec.this.sink.timeout());
      }

      public void close() throws IOException {
         // $FF: Couldn't be decompiled
      }

      public void flush() throws IOException {
         // $FF: Couldn't be decompiled
      }

      public u timeout() {
         return this.timeout;
      }

      public void write(c var1, long var2) throws IOException {
         if(this.closed) {
            throw new IllegalStateException("closed");
         } else {
            if(var2 != 0L) {
               Http1Codec.this.sink.m(var2);
               Http1Codec.this.sink.b("\r\n");
               Http1Codec.this.sink.write(var1, var2);
               Http1Codec.this.sink.b("\r\n");
            }

         }
      }
   }

   private class ChunkedSource extends Http1Codec.AbstractSource {
      private static final long NO_CHUNK_YET = -1L;
      private long bytesRemainingInChunk = -1L;
      private boolean hasMoreChunks = true;
      private final HttpUrl url;

      ChunkedSource(HttpUrl var2) {
         super(null);
         this.url = var2;
      }

      private void readChunkSize() throws IOException {
         if(this.bytesRemainingInChunk != -1L) {
            Http1Codec.this.source.s();
         }

         try {
            this.bytesRemainingInChunk = Http1Codec.this.source.p();
            String var3 = Http1Codec.this.source.s().trim();
            if(this.bytesRemainingInChunk < 0L || !var3.isEmpty() && !var3.startsWith(";")) {
               StringBuilder var1 = new StringBuilder();
               ProtocolException var2 = new ProtocolException(var1.append("expected chunk size and optional extensions but was \"").append(this.bytesRemainingInChunk).append(var3).append("\"").toString());
               throw var2;
            }
         } catch (NumberFormatException var4) {
            throw new ProtocolException(var4.getMessage());
         }

         if(this.bytesRemainingInChunk == 0L) {
            this.hasMoreChunks = false;
            HttpHeaders.receiveHeaders(Http1Codec.this.client.cookieJar(), this.url, Http1Codec.this.readHeaders());
            this.endOfInput(true);
         }

      }

      public void close() throws IOException {
         if(!this.closed) {
            if(this.hasMoreChunks && !Util.discard(this, 100, TimeUnit.MILLISECONDS)) {
               this.endOfInput(false);
            }

            this.closed = true;
         }

      }

      public long read(c var1, long var2) throws IOException {
         long var4 = -1L;
         if(var2 < 0L) {
            throw new IllegalArgumentException("byteCount < 0: " + var2);
         } else if(this.closed) {
            throw new IllegalStateException("closed");
         } else {
            if(this.hasMoreChunks) {
               if(this.bytesRemainingInChunk == 0L || this.bytesRemainingInChunk == -1L) {
                  this.readChunkSize();
                  if(!this.hasMoreChunks) {
                     return var4;
                  }
               }

               var4 = Http1Codec.this.source.read(var1, Math.min(var2, this.bytesRemainingInChunk));
               if(var4 == -1L) {
                  this.endOfInput(false);
                  throw new ProtocolException("unexpected end of stream");
               }

               this.bytesRemainingInChunk -= var4;
            }

            return var4;
         }
      }
   }

   private final class FixedLengthSink implements s {
      private long bytesRemaining;
      private boolean closed;
      private final i timeout;

      FixedLengthSink(long var2) {
         this.timeout = new i(Http1Codec.this.sink.timeout());
         this.bytesRemaining = var2;
      }

      public void close() throws IOException {
         if(!this.closed) {
            this.closed = true;
            if(this.bytesRemaining > 0L) {
               throw new ProtocolException("unexpected end of stream");
            }

            Http1Codec.this.detachTimeout(this.timeout);
            Http1Codec.this.state = 3;
         }

      }

      public void flush() throws IOException {
         if(!this.closed) {
            Http1Codec.this.sink.flush();
         }

      }

      public u timeout() {
         return this.timeout;
      }

      public void write(c var1, long var2) throws IOException {
         if(this.closed) {
            throw new IllegalStateException("closed");
         } else {
            Util.checkOffsetAndCount(var1.a(), 0L, var2);
            if(var2 > this.bytesRemaining) {
               throw new ProtocolException("expected " + this.bytesRemaining + " bytes but received " + var2);
            } else {
               Http1Codec.this.sink.write(var1, var2);
               this.bytesRemaining -= var2;
            }
         }
      }
   }

   private class FixedLengthSource extends Http1Codec.AbstractSource {
      private long bytesRemaining;

      FixedLengthSource(long var2) throws IOException {
         super(null);
         this.bytesRemaining = var2;
         if(this.bytesRemaining == 0L) {
            this.endOfInput(true);
         }

      }

      public void close() throws IOException {
         if(!this.closed) {
            if(this.bytesRemaining != 0L && !Util.discard(this, 100, TimeUnit.MILLISECONDS)) {
               this.endOfInput(false);
            }

            this.closed = true;
         }

      }

      public long read(c var1, long var2) throws IOException {
         long var4 = -1L;
         if(var2 < 0L) {
            throw new IllegalArgumentException("byteCount < 0: " + var2);
         } else if(this.closed) {
            throw new IllegalStateException("closed");
         } else {
            if(this.bytesRemaining == 0L) {
               var2 = var4;
            } else {
               var2 = Http1Codec.this.source.read(var1, Math.min(this.bytesRemaining, var2));
               if(var2 == -1L) {
                  this.endOfInput(false);
                  throw new ProtocolException("unexpected end of stream");
               }

               this.bytesRemaining -= var2;
               if(this.bytesRemaining == 0L) {
                  this.endOfInput(true);
               }
            }

            return var2;
         }
      }
   }

   private class UnknownLengthSource extends Http1Codec.AbstractSource {
      private boolean inputExhausted;

      UnknownLengthSource() {
         super(null);
      }

      public void close() throws IOException {
         if(!this.closed) {
            if(!this.inputExhausted) {
               this.endOfInput(false);
            }

            this.closed = true;
         }

      }

      public long read(c var1, long var2) throws IOException {
         long var4 = -1L;
         if(var2 < 0L) {
            throw new IllegalArgumentException("byteCount < 0: " + var2);
         } else if(this.closed) {
            throw new IllegalStateException("closed");
         } else {
            if(this.inputExhausted) {
               var2 = var4;
            } else {
               var2 = Http1Codec.this.source.read(var1, var2);
               if(var2 == -1L) {
                  this.inputExhausted = true;
                  this.endOfInput(true);
                  var2 = var4;
               }
            }

            return var2;
         }
      }
   }
}
