package okhttp3.internal;

import c.c;
import c.e;
import c.f;
import c.t;
import java.io.Closeable;
import java.io.IOException;
import java.net.IDN;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;

public final class Util {
   public static final byte[] EMPTY_BYTE_ARRAY = new byte[0];
   public static final RequestBody EMPTY_REQUEST;
   public static final ResponseBody EMPTY_RESPONSE;
   public static final String[] EMPTY_STRING_ARRAY = new String[0];
   public static final Comparator NATURAL_ORDER;
   public static final TimeZone UTC;
   private static final Charset UTF_16_BE;
   private static final f UTF_16_BE_BOM;
   private static final Charset UTF_16_LE;
   private static final f UTF_16_LE_BOM;
   private static final Charset UTF_32_BE;
   private static final f UTF_32_BE_BOM;
   private static final Charset UTF_32_LE;
   private static final f UTF_32_LE_BOM;
   public static final Charset UTF_8;
   private static final f UTF_8_BOM;
   private static final Pattern VERIFY_AS_IP_ADDRESS;

   static {
      EMPTY_RESPONSE = ResponseBody.create((MediaType)null, (byte[])EMPTY_BYTE_ARRAY);
      EMPTY_REQUEST = RequestBody.create((MediaType)null, (byte[])EMPTY_BYTE_ARRAY);
      UTF_8_BOM = f.c("efbbbf");
      UTF_16_BE_BOM = f.c("feff");
      UTF_16_LE_BOM = f.c("fffe");
      UTF_32_BE_BOM = f.c("0000ffff");
      UTF_32_LE_BOM = f.c("ffff0000");
      UTF_8 = Charset.forName("UTF-8");
      UTF_16_BE = Charset.forName("UTF-16BE");
      UTF_16_LE = Charset.forName("UTF-16LE");
      UTF_32_BE = Charset.forName("UTF-32BE");
      UTF_32_LE = Charset.forName("UTF-32LE");
      UTC = TimeZone.getTimeZone("GMT");
      NATURAL_ORDER = new Comparator() {
         public int compare(String var1, String var2) {
            return var1.compareTo(var2);
         }
      };
      VERIFY_AS_IP_ADDRESS = Pattern.compile("([0-9a-fA-F]*:[0-9a-fA-F:.]*)|([\\d.]+)");
   }

   public static Charset bomAwareCharset(e var0, Charset var1) throws IOException {
      if(var0.a(0L, UTF_8_BOM)) {
         var0.i((long)UTF_8_BOM.h());
         var1 = UTF_8;
      } else if(var0.a(0L, UTF_16_BE_BOM)) {
         var0.i((long)UTF_16_BE_BOM.h());
         var1 = UTF_16_BE;
      } else if(var0.a(0L, UTF_16_LE_BOM)) {
         var0.i((long)UTF_16_LE_BOM.h());
         var1 = UTF_16_LE;
      } else if(var0.a(0L, UTF_32_BE_BOM)) {
         var0.i((long)UTF_32_BE_BOM.h());
         var1 = UTF_32_BE;
      } else if(var0.a(0L, UTF_32_LE_BOM)) {
         var0.i((long)UTF_32_LE_BOM.h());
         var1 = UTF_32_LE;
      }

      return var1;
   }

   public static void checkOffsetAndCount(long var0, long var2, long var4) {
      if((var2 | var4) < 0L || var2 > var0 || var0 - var2 < var4) {
         throw new ArrayIndexOutOfBoundsException();
      }
   }

   public static void closeQuietly(Closeable var0) {
      if(var0 != null) {
         try {
            var0.close();
         } catch (RuntimeException var1) {
            throw var1;
         } catch (Exception var2) {
            ;
         }
      }

   }

   public static void closeQuietly(ServerSocket var0) {
      if(var0 != null) {
         try {
            var0.close();
         } catch (RuntimeException var1) {
            throw var1;
         } catch (Exception var2) {
            ;
         }
      }

   }

   public static void closeQuietly(Socket var0) {
      if(var0 != null) {
         try {
            var0.close();
         } catch (AssertionError var1) {
            if(!isAndroidGetsocknameError(var1)) {
               throw var1;
            }
         } catch (RuntimeException var2) {
            throw var2;
         } catch (Exception var3) {
            ;
         }
      }

   }

   public static String[] concat(String[] var0, String var1) {
      String[] var2 = new String[var0.length + 1];
      System.arraycopy(var0, 0, var2, 0, var0.length);
      var2[var2.length - 1] = var1;
      return var2;
   }

   private static boolean containsInvalidHostnameAsciiCodes(String var0) {
      boolean var4 = false;
      int var1 = 0;

      boolean var3;
      while(true) {
         var3 = var4;
         if(var1 >= var0.length()) {
            break;
         }

         char var2 = var0.charAt(var1);
         if(var2 <= 31 || var2 >= 127) {
            var3 = true;
            break;
         }

         if(" #%/:?@[\\]".indexOf(var2) != -1) {
            var3 = true;
            break;
         }

         ++var1;
      }

      return var3;
   }

   public static int delimiterOffset(String var0, int var1, int var2, char var3) {
      while(true) {
         int var4 = var2;
         if(var1 < var2) {
            if(var0.charAt(var1) != var3) {
               ++var1;
               continue;
            }

            var4 = var1;
         }

         return var4;
      }
   }

   public static int delimiterOffset(String var0, int var1, int var2, String var3) {
      while(true) {
         int var4 = var2;
         if(var1 < var2) {
            if(var3.indexOf(var0.charAt(var1)) == -1) {
               ++var1;
               continue;
            }

            var4 = var1;
         }

         return var4;
      }
   }

   public static boolean discard(t var0, int var1, TimeUnit var2) {
      boolean var3;
      try {
         var3 = skipAll(var0, var1, var2);
      } catch (IOException var4) {
         var3 = false;
      }

      return var3;
   }

   public static String domainToAscii(String var0) {
      Object var2 = null;

      label33: {
         boolean var1;
         String var3;
         try {
            var3 = IDN.toASCII(var0).toLowerCase(Locale.US);
            if(var3.isEmpty()) {
               break label33;
            }

            var1 = containsInvalidHostnameAsciiCodes(var3);
         } catch (IllegalArgumentException var4) {
            var0 = (String)var2;
            return var0;
         }

         var0 = (String)var2;
         if(!var1) {
            var0 = var3;
         }

         return var0;
      }

      var0 = (String)var2;
      return var0;
   }

   public static boolean equal(Object var0, Object var1) {
      boolean var2;
      if(var0 != var1 && (var0 == null || !var0.equals(var1))) {
         var2 = false;
      } else {
         var2 = true;
      }

      return var2;
   }

   public static String format(String var0, Object... var1) {
      return String.format(Locale.US, var0, var1);
   }

   public static String hostHeader(HttpUrl var0, boolean var1) {
      String var2;
      if(var0.host().contains(":")) {
         var2 = "[" + var0.host() + "]";
      } else {
         var2 = var0.host();
      }

      String var3;
      if(!var1) {
         var3 = var2;
         if(var0.port() == HttpUrl.defaultPort(var0.scheme())) {
            return var3;
         }
      }

      var3 = var2 + ":" + var0.port();
      return var3;
   }

   public static List immutableList(List var0) {
      return Collections.unmodifiableList(new ArrayList(var0));
   }

   public static List immutableList(Object... var0) {
      return Collections.unmodifiableList(Arrays.asList((Object[])var0.clone()));
   }

   public static int indexOf(Comparator var0, String[] var1, String var2) {
      int var3 = 0;
      int var4 = var1.length;

      while(true) {
         if(var3 >= var4) {
            var3 = -1;
            break;
         }

         if(var0.compare(var1[var3], var2) == 0) {
            break;
         }

         ++var3;
      }

      return var3;
   }

   public static int indexOfControlOrNonAscii(String var0) {
      int var1 = 0;
      int var3 = var0.length();

      int var2;
      while(true) {
         if(var1 >= var3) {
            var2 = -1;
            break;
         }

         char var4 = var0.charAt(var1);
         var2 = var1;
         if(var4 <= 31) {
            break;
         }

         if(var4 >= 127) {
            var2 = var1;
            break;
         }

         ++var1;
      }

      return var2;
   }

   public static String[] intersect(Comparator var0, String[] var1, String[] var2) {
      ArrayList var7 = new ArrayList();
      int var5 = var1.length;

      for(int var3 = 0; var3 < var5; ++var3) {
         String var8 = var1[var3];
         int var6 = var2.length;

         for(int var4 = 0; var4 < var6; ++var4) {
            if(var0.compare(var8, var2[var4]) == 0) {
               var7.add(var8);
               break;
            }
         }
      }

      return (String[])var7.toArray(new String[var7.size()]);
   }

   public static boolean isAndroidGetsocknameError(AssertionError var0) {
      boolean var1;
      if(var0.getCause() != null && var0.getMessage() != null && var0.getMessage().contains("getsockname failed")) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public static boolean nonEmptyIntersection(Comparator var0, String[] var1, String[] var2) {
      boolean var8 = false;
      boolean var7 = var8;
      if(var1 != null) {
         var7 = var8;
         if(var2 != null) {
            var7 = var8;
            if(var1.length != 0) {
               if(var2.length == 0) {
                  var7 = var8;
               } else {
                  int var5 = var1.length;
                  int var3 = 0;

                  while(true) {
                     var7 = var8;
                     if(var3 >= var5) {
                        break;
                     }

                     String var9 = var1[var3];
                     int var6 = var2.length;

                     for(int var4 = 0; var4 < var6; ++var4) {
                        if(var0.compare(var9, var2[var4]) == 0) {
                           var7 = true;
                           return var7;
                        }
                     }

                     ++var3;
                  }
               }
            }
         }
      }

      return var7;
   }

   public static boolean skipAll(t param0, int param1, TimeUnit param2) throws IOException {
      // $FF: Couldn't be decompiled
   }

   public static int skipLeadingAsciiWhitespace(String var0, int var1, int var2) {
      while(true) {
         int var3 = var2;
         if(var1 < var2) {
            switch(var0.charAt(var1)) {
            case '\t':
            case '\n':
            case '\f':
            case '\r':
            case ' ':
               ++var1;
               continue;
            default:
               var3 = var1;
            }
         }

         return var3;
      }
   }

   public static int skipTrailingAsciiWhitespace(String var0, int var1, int var2) {
      --var2;

      while(true) {
         int var3 = var1;
         if(var2 < var1) {
            return var3;
         }

         switch(var0.charAt(var2)) {
         case '\t':
         case '\n':
         case '\f':
         case '\r':
         case ' ':
            --var2;
            break;
         default:
            var3 = var2 + 1;
            return var3;
         }
      }
   }

   public static ThreadFactory threadFactory(final String var0, final boolean var1) {
      return new ThreadFactory() {
         public Thread newThread(Runnable var1x) {
            Thread var2 = new Thread(var1x, var0);
            var2.setDaemon(var1);
            return var2;
         }
      };
   }

   public static String toHumanReadableAscii(String var0) {
      int var4 = var0.length();
      int var1 = 0;

      String var5;
      while(true) {
         var5 = var0;
         if(var1 >= var4) {
            break;
         }

         int var2 = var0.codePointAt(var1);
         if(var2 <= 31 || var2 >= 127) {
            c var6 = new c();
            var6.a((String)var0, 0, var1);

            while(var1 < var4) {
               int var3 = var0.codePointAt(var1);
               if(var3 > 31 && var3 < 127) {
                  var2 = var3;
               } else {
                  var2 = 63;
               }

               var6.a(var2);
               var1 += Character.charCount(var3);
            }

            var5 = var6.r();
            break;
         }

         var1 += Character.charCount(var2);
      }

      return var5;
   }

   public static String trimSubstring(String var0, int var1, int var2) {
      var1 = skipLeadingAsciiWhitespace(var0, var1, var2);
      return var0.substring(var1, skipTrailingAsciiWhitespace(var0, var1, var2));
   }

   public static boolean verifyAsIpAddress(String var0) {
      return VERIFY_AS_IP_ADDRESS.matcher(var0).matches();
   }
}
