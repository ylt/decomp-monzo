package okhttp3.internal.connection;

import c.d;
import c.e;
import c.l;
import c.t;
import java.io.IOException;
import java.net.ConnectException;
import java.net.ProtocolException;
import java.net.Proxy;
import java.net.Socket;
import java.net.SocketException;
import java.net.Proxy.Type;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.annotation.Nullable;
import javax.net.ssl.SSLPeerUnverifiedException;
import okhttp3.Address;
import okhttp3.Connection;
import okhttp3.ConnectionPool;
import okhttp3.Handshake;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Protocol;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.Route;
import okhttp3.internal.Internal;
import okhttp3.internal.Util;
import okhttp3.internal.Version;
import okhttp3.internal.http.HttpCodec;
import okhttp3.internal.http.HttpHeaders;
import okhttp3.internal.http1.Http1Codec;
import okhttp3.internal.http2.ErrorCode;
import okhttp3.internal.http2.Http2Codec;
import okhttp3.internal.http2.Http2Connection;
import okhttp3.internal.http2.Http2Stream;
import okhttp3.internal.platform.Platform;
import okhttp3.internal.tls.OkHostnameVerifier;
import okhttp3.internal.ws.RealWebSocket;

public final class RealConnection extends Http2Connection.Listener implements Connection {
   private static final String NPE_THROW_WITH_NULL = "throw with null exception";
   public int allocationLimit = 1;
   public final List allocations = new ArrayList();
   private final ConnectionPool connectionPool;
   private Handshake handshake;
   private Http2Connection http2Connection;
   public long idleAtNanos = Long.MAX_VALUE;
   public boolean noNewStreams;
   private Protocol protocol;
   private Socket rawSocket;
   private final Route route;
   private d sink;
   private Socket socket;
   private e source;
   public int successCount;

   public RealConnection(ConnectionPool var1, Route var2) {
      this.connectionPool = var1;
      this.route = var2;
   }

   private void connectSocket(int var1, int var2) throws IOException {
      Proxy var4 = this.route.proxy();
      Address var3 = this.route.address();
      Socket var7;
      if(var4.type() != Type.DIRECT && var4.type() != Type.HTTP) {
         var7 = new Socket(var4);
      } else {
         var7 = var3.socketFactory().createSocket();
      }

      this.rawSocket = var7;
      this.rawSocket.setSoTimeout(var2);

      try {
         Platform.get().connectSocket(this.rawSocket, this.route.socketAddress(), var1);
      } catch (ConnectException var5) {
         ConnectException var8 = new ConnectException("Failed to connect to " + this.route.socketAddress());
         var8.initCause(var5);
         throw var8;
      }

      try {
         this.source = l.a(l.b(this.rawSocket));
         this.sink = l.a(l.a(this.rawSocket));
      } catch (NullPointerException var6) {
         if("throw with null exception".equals(var6.getMessage())) {
            throw new IOException(var6);
         }
      }

   }

   private void connectTls(ConnectionSpecSelector param1) throws IOException {
      // $FF: Couldn't be decompiled
   }

   private void connectTunnel(int var1, int var2, int var3) throws IOException {
      Request var5 = this.createTunnelRequest();
      HttpUrl var6 = var5.url();
      int var4 = 0;

      while(true) {
         ++var4;
         if(var4 > 21) {
            throw new ProtocolException("Too many tunnel connections attempted: " + 21);
         }

         this.connectSocket(var1, var2);
         var5 = this.createTunnel(var2, var3, var5, var6);
         if(var5 == null) {
            return;
         }

         Util.closeQuietly(this.rawSocket);
         this.rawSocket = null;
         this.sink = null;
         this.source = null;
      }
   }

   private Request createTunnel(int var1, int var2, Request var3, HttpUrl var4) throws IOException {
      String var9 = "CONNECT " + Util.hostHeader(var4, true) + " HTTP/1.1";

      while(true) {
         Http1Codec var12 = new Http1Codec((OkHttpClient)null, (StreamAllocation)null, this.source, this.sink);
         this.source.timeout().timeout((long)var1, TimeUnit.MILLISECONDS);
         this.sink.timeout().timeout((long)var2, TimeUnit.MILLISECONDS);
         var12.writeRequest(var3.headers(), var9);
         var12.finishRequest();
         Response var10 = var12.readResponseHeaders(false).request(var3).build();
         long var7 = HttpHeaders.contentLength(var10);
         long var5 = var7;
         if(var7 == -1L) {
            var5 = 0L;
         }

         t var11 = var12.newFixedLengthSource(var5);
         Util.skipAll(var11, Integer.MAX_VALUE, TimeUnit.MILLISECONDS);
         var11.close();
         Request var13;
         switch(var10.code()) {
         case 200:
            if(!this.source.b().f() || !this.sink.b().f()) {
               throw new IOException("TLS tunnel buffered too many bytes!");
            }

            var13 = null;
            break;
         case 407:
            var13 = this.route.address().proxyAuthenticator().authenticate(this.route, var10);
            if(var13 == null) {
               throw new IOException("Failed to authenticate with proxy");
            }

            var3 = var13;
            if(!"close".equalsIgnoreCase(var10.header("Connection"))) {
               continue;
            }
            break;
         default:
            throw new IOException("Unexpected response code for CONNECT: " + var10.code());
         }

         return var13;
      }
   }

   private Request createTunnelRequest() {
      return (new Request.Builder()).url(this.route.address().url()).header("Host", Util.hostHeader(this.route.address().url(), true)).header("Proxy-Connection", "Keep-Alive").header("User-Agent", Version.userAgent()).build();
   }

   private void establishProtocol(ConnectionSpecSelector var1) throws IOException {
      if(this.route.address().sslSocketFactory() == null) {
         this.protocol = Protocol.HTTP_1_1;
         this.socket = this.rawSocket;
      } else {
         this.connectTls(var1);
         if(this.protocol == Protocol.HTTP_2) {
            this.socket.setSoTimeout(0);
            this.http2Connection = (new Http2Connection.Builder(true)).socket(this.socket, this.route.address().url().host(), this.source, this.sink).listener(this).build();
            this.http2Connection.start();
         }
      }

   }

   public static RealConnection testConnection(ConnectionPool var0, Route var1, Socket var2, long var3) {
      RealConnection var5 = new RealConnection(var0, var1);
      var5.socket = var2;
      var5.idleAtNanos = var3;
      return var5;
   }

   public void cancel() {
      Util.closeQuietly(this.rawSocket);
   }

   public void connect(int param1, int param2, int param3, boolean param4) {
      // $FF: Couldn't be decompiled
   }

   public Handshake handshake() {
      return this.handshake;
   }

   public boolean isEligible(Address var1, @Nullable Route var2) {
      boolean var4 = false;
      boolean var3 = var4;
      if(this.allocations.size() < this.allocationLimit) {
         if(this.noNewStreams) {
            var3 = var4;
         } else {
            var3 = var4;
            if(Internal.instance.equalsNonHost(this.route.address(), var1)) {
               if(var1.url().host().equals(this.route().address().url().host())) {
                  var3 = true;
               } else {
                  var3 = var4;
                  if(this.http2Connection != null) {
                     var3 = var4;
                     if(var2 != null) {
                        var3 = var4;
                        if(var2.proxy().type() == Type.DIRECT) {
                           var3 = var4;
                           if(this.route.proxy().type() == Type.DIRECT) {
                              var3 = var4;
                              if(this.route.socketAddress().equals(var2.socketAddress())) {
                                 var3 = var4;
                                 if(var2.address().hostnameVerifier() == OkHostnameVerifier.INSTANCE) {
                                    var3 = var4;
                                    if(this.supportsUrl(var1.url())) {
                                       try {
                                          var1.certificatePinner().check(var1.url().host(), this.handshake().peerCertificates());
                                       } catch (SSLPeerUnverifiedException var5) {
                                          var3 = var4;
                                          return var3;
                                       }

                                       var3 = true;
                                    }
                                 }
                              }
                           }
                        }
                     }
                  }
               }
            }
         }
      }

      return var3;
   }

   public boolean isHealthy(boolean param1) {
      // $FF: Couldn't be decompiled
   }

   public boolean isMultiplexed() {
      boolean var1;
      if(this.http2Connection != null) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public HttpCodec newCodec(OkHttpClient var1, StreamAllocation var2) throws SocketException {
      Object var3;
      if(this.http2Connection != null) {
         var3 = new Http2Codec(var1, var2, this.http2Connection);
      } else {
         this.socket.setSoTimeout(var1.readTimeoutMillis());
         this.source.timeout().timeout((long)var1.readTimeoutMillis(), TimeUnit.MILLISECONDS);
         this.sink.timeout().timeout((long)var1.writeTimeoutMillis(), TimeUnit.MILLISECONDS);
         var3 = new Http1Codec(var1, var2, this.source, this.sink);
      }

      return (HttpCodec)var3;
   }

   public RealWebSocket.Streams newWebSocketStreams(final StreamAllocation var1) {
      return new RealWebSocket.Streams(true, this.source, this.sink) {
         public void close() throws IOException {
            var1.streamFinished(true, var1.codec());
         }
      };
   }

   public void onSettings(Http2Connection param1) {
      // $FF: Couldn't be decompiled
   }

   public void onStream(Http2Stream var1) throws IOException {
      var1.close(ErrorCode.REFUSED_STREAM);
   }

   public Protocol protocol() {
      return this.protocol;
   }

   public Route route() {
      return this.route;
   }

   public Socket socket() {
      return this.socket;
   }

   public boolean supportsUrl(HttpUrl var1) {
      boolean var2 = false;
      if(var1.port() == this.route.address().url().port()) {
         if(!var1.host().equals(this.route.address().url().host())) {
            if(this.handshake != null && OkHostnameVerifier.INSTANCE.verify(var1.host(), (X509Certificate)this.handshake.peerCertificates().get(0))) {
               var2 = true;
            } else {
               var2 = false;
            }
         } else {
            var2 = true;
         }
      }

      return var2;
   }

   public String toString() {
      StringBuilder var2 = (new StringBuilder()).append("Connection{").append(this.route.address().url().host()).append(":").append(this.route.address().url().port()).append(", proxy=").append(this.route.proxy()).append(" hostAddress=").append(this.route.socketAddress()).append(" cipherSuite=");
      Object var1;
      if(this.handshake != null) {
         var1 = this.handshake.cipherSuite();
      } else {
         var1 = "none";
      }

      return var2.append(var1).append(" protocol=").append(this.protocol).append('}').toString();
   }
}
