package okhttp3.internal.connection;

import java.io.IOException;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.internal.http.RealInterceptorChain;

public final class ConnectInterceptor implements Interceptor {
   public final OkHttpClient client;

   public ConnectInterceptor(OkHttpClient var1) {
      this.client = var1;
   }

   public Response intercept(Interceptor.Chain var1) throws IOException {
      RealInterceptorChain var4 = (RealInterceptorChain)var1;
      Request var5 = var4.request();
      StreamAllocation var3 = var4.streamAllocation();
      boolean var2;
      if(!var5.method().equals("GET")) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var4.proceed(var5, var3, var3.newStream(this.client, var2), var3.connection());
   }
}
