package okhttp3;

import c.d;
import c.e;
import c.f;
import c.g;
import c.h;
import c.l;
import c.s;
import c.t;
import java.io.Closeable;
import java.io.File;
import java.io.Flushable;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import javax.annotation.Nullable;
import okhttp3.internal.Util;
import okhttp3.internal.cache.CacheRequest;
import okhttp3.internal.cache.CacheStrategy;
import okhttp3.internal.cache.DiskLruCache;
import okhttp3.internal.cache.InternalCache;
import okhttp3.internal.http.HttpHeaders;
import okhttp3.internal.http.HttpMethod;
import okhttp3.internal.http.StatusLine;
import okhttp3.internal.io.FileSystem;
import okhttp3.internal.platform.Platform;

public final class Cache implements Closeable, Flushable {
   private static final int ENTRY_BODY = 1;
   private static final int ENTRY_COUNT = 2;
   private static final int ENTRY_METADATA = 0;
   private static final int VERSION = 201105;
   final DiskLruCache cache;
   private int hitCount;
   final InternalCache internalCache;
   private int networkCount;
   private int requestCount;
   int writeAbortCount;
   int writeSuccessCount;

   public Cache(File var1, long var2) {
      this(var1, var2, FileSystem.SYSTEM);
   }

   Cache(File var1, long var2, FileSystem var4) {
      this.internalCache = new InternalCache() {
         public Response get(Request var1) throws IOException {
            return Cache.this.get(var1);
         }

         public CacheRequest put(Response var1) throws IOException {
            return Cache.this.put(var1);
         }

         public void remove(Request var1) throws IOException {
            Cache.this.remove(var1);
         }

         public void trackConditionalCacheHit() {
            Cache.this.trackConditionalCacheHit();
         }

         public void trackResponse(CacheStrategy var1) {
            Cache.this.trackResponse(var1);
         }

         public void update(Response var1, Response var2) {
            Cache.this.update(var1, var2);
         }
      };
      this.cache = DiskLruCache.create(var4, var1, 201105, 2, var2);
   }

   private void abortQuietly(@Nullable DiskLruCache.Editor var1) {
      if(var1 != null) {
         try {
            var1.abort();
         } catch (IOException var2) {
            ;
         }
      }

   }

   public static String key(HttpUrl var0) {
      return f.a(var0.toString()).c().f();
   }

   static int readInt(e param0) throws IOException {
      // $FF: Couldn't be decompiled
   }

   public void close() throws IOException {
      this.cache.close();
   }

   public void delete() throws IOException {
      this.cache.delete();
   }

   public File directory() {
      return this.cache.getDirectory();
   }

   public void evictAll() throws IOException {
      this.cache.evictAll();
   }

   public void flush() throws IOException {
      this.cache.flush();
   }

   @Nullable
   Response get(Request var1) {
      Object var2 = null;
      String var3 = key(var1.url());

      Response var7;
      DiskLruCache.Snapshot var8;
      try {
         var8 = this.cache.get(var3);
      } catch (IOException var6) {
         var7 = (Response)var2;
         return var7;
      }

      if(var8 == null) {
         var7 = (Response)var2;
      } else {
         Cache.Entry var4;
         try {
            var4 = new Cache.Entry(var8.getSource(0));
         } catch (IOException var5) {
            Util.closeQuietly((Closeable)var8);
            var7 = (Response)var2;
            return var7;
         }

         Response var9 = var4.response(var8);
         if(!var4.matches(var1, var9)) {
            Util.closeQuietly((Closeable)var9.body());
            var7 = (Response)var2;
         } else {
            var7 = var9;
         }
      }

      return var7;
   }

   public int hitCount() {
      synchronized(this){}

      int var1;
      try {
         var1 = this.hitCount;
      } finally {
         ;
      }

      return var1;
   }

   public void initialize() throws IOException {
      this.cache.initialize();
   }

   public boolean isClosed() {
      return this.cache.isClosed();
   }

   public long maxSize() {
      return this.cache.getMaxSize();
   }

   public int networkCount() {
      synchronized(this){}

      int var1;
      try {
         var1 = this.networkCount;
      } finally {
         ;
      }

      return var1;
   }

   @Nullable
   CacheRequest put(Response var1) {
      Object var3 = null;
      String var4 = var1.request().method();
      Cache.CacheRequestImpl var2;
      if(HttpMethod.invalidatesCache(var1.request().method())) {
         try {
            this.remove(var1.request());
         } catch (IOException var7) {
            var2 = (Cache.CacheRequestImpl)var3;
            return var2;
         }

         var2 = (Cache.CacheRequestImpl)var3;
      } else {
         var2 = (Cache.CacheRequestImpl)var3;
         if(var4.equals("GET")) {
            var2 = (Cache.CacheRequestImpl)var3;
            if(!HttpHeaders.hasVaryAll(var1)) {
               Cache.Entry var9 = new Cache.Entry(var1);

               DiskLruCache.Editor var8;
               label41: {
                  try {
                     var8 = this.cache.edit(key(var1.request().url()));
                  } catch (IOException var6) {
                     var8 = null;
                     break label41;
                  }

                  var2 = (Cache.CacheRequestImpl)var3;
                  if(var8 == null) {
                     return var2;
                  }

                  try {
                     var9.writeTo(var8);
                     var2 = new Cache.CacheRequestImpl(var8);
                     return var2;
                  } catch (IOException var5) {
                     ;
                  }
               }

               this.abortQuietly(var8);
               var2 = (Cache.CacheRequestImpl)var3;
            }
         }
      }

      return var2;
   }

   void remove(Request var1) throws IOException {
      this.cache.remove(key(var1.url()));
   }

   public int requestCount() {
      synchronized(this){}

      int var1;
      try {
         var1 = this.requestCount;
      } finally {
         ;
      }

      return var1;
   }

   public long size() throws IOException {
      return this.cache.size();
   }

   void trackConditionalCacheHit() {
      synchronized(this){}

      try {
         ++this.hitCount;
      } finally {
         ;
      }

   }

   void trackResponse(CacheStrategy var1) {
      synchronized(this){}

      try {
         ++this.requestCount;
         if(var1.networkRequest != null) {
            ++this.networkCount;
         } else if(var1.cacheResponse != null) {
            ++this.hitCount;
         }
      } finally {
         ;
      }

   }

   void update(Response param1, Response param2) {
      // $FF: Couldn't be decompiled
   }

   public Iterator urls() throws IOException {
      return new Iterator() {
         boolean canRemove;
         final Iterator delegate;
         @Nullable
         String nextUrl;

         {
            this.delegate = Cache.this.cache.snapshots();
         }

         public boolean hasNext() {
            boolean var1;
            if(this.nextUrl != null) {
               var1 = true;
            } else {
               this.canRemove = false;

               while(true) {
                  if(this.delegate.hasNext()) {
                     DiskLruCache.Snapshot var2 = (DiskLruCache.Snapshot)this.delegate.next();

                     try {
                        this.nextUrl = l.a(var2.getSource(0)).s();
                     } catch (IOException var6) {
                        continue;
                     } finally {
                        var2.close();
                     }

                     var1 = true;
                     break;
                  }

                  var1 = false;
                  break;
               }
            }

            return var1;
         }

         public String next() {
            if(!this.hasNext()) {
               throw new NoSuchElementException();
            } else {
               String var1 = this.nextUrl;
               this.nextUrl = null;
               this.canRemove = true;
               return var1;
            }
         }

         public void remove() {
            if(!this.canRemove) {
               throw new IllegalStateException("remove() before next()");
            } else {
               this.delegate.remove();
            }
         }
      };
   }

   public int writeAbortCount() {
      synchronized(this){}

      int var1;
      try {
         var1 = this.writeAbortCount;
      } finally {
         ;
      }

      return var1;
   }

   public int writeSuccessCount() {
      synchronized(this){}

      int var1;
      try {
         var1 = this.writeSuccessCount;
      } finally {
         ;
      }

      return var1;
   }

   private final class CacheRequestImpl implements CacheRequest {
      private s body;
      private s cacheOut;
      boolean done;
      private final DiskLruCache.Editor editor;

      CacheRequestImpl(final DiskLruCache.Editor var2) {
         this.editor = var2;
         this.cacheOut = var2.newSink(1);
         this.body = new g(this.cacheOut) {
            public void close() throws IOException {
               // $FF: Couldn't be decompiled
            }
         };
      }

      public void abort() {
         // $FF: Couldn't be decompiled
      }

      public s body() {
         return this.body;
      }
   }

   private static class CacheResponseBody extends ResponseBody {
      private final e bodySource;
      @Nullable
      private final String contentLength;
      @Nullable
      private final String contentType;
      final DiskLruCache.Snapshot snapshot;

      CacheResponseBody(final DiskLruCache.Snapshot var1, String var2, String var3) {
         this.snapshot = var1;
         this.contentType = var2;
         this.contentLength = var3;
         this.bodySource = l.a((t)(new h(var1.getSource(1)) {
            public void close() throws IOException {
               var1.close();
               super.close();
            }
         }));
      }

      public long contentLength() {
         long var3 = -1L;
         long var1 = var3;

         try {
            if(this.contentLength != null) {
               var1 = Long.parseLong(this.contentLength);
            }
         } catch (NumberFormatException var6) {
            var1 = var3;
         }

         return var1;
      }

      public MediaType contentType() {
         MediaType var1;
         if(this.contentType != null) {
            var1 = MediaType.parse(this.contentType);
         } else {
            var1 = null;
         }

         return var1;
      }

      public e source() {
         return this.bodySource;
      }
   }

   private static final class Entry {
      private static final String RECEIVED_MILLIS = Platform.get().getPrefix() + "-Received-Millis";
      private static final String SENT_MILLIS = Platform.get().getPrefix() + "-Sent-Millis";
      private final int code;
      @Nullable
      private final Handshake handshake;
      private final String message;
      private final Protocol protocol;
      private final long receivedResponseMillis;
      private final String requestMethod;
      private final Headers responseHeaders;
      private final long sentRequestMillis;
      private final String url;
      private final Headers varyHeaders;

      Entry(t param1) throws IOException {
         // $FF: Couldn't be decompiled
      }

      Entry(Response var1) {
         this.url = var1.request().url().toString();
         this.varyHeaders = HttpHeaders.varyHeaders(var1);
         this.requestMethod = var1.request().method();
         this.protocol = var1.protocol();
         this.code = var1.code();
         this.message = var1.message();
         this.responseHeaders = var1.headers();
         this.handshake = var1.handshake();
         this.sentRequestMillis = var1.sentRequestAtMillis();
         this.receivedResponseMillis = var1.receivedResponseAtMillis();
      }

      private boolean isHttps() {
         return this.url.startsWith("https://");
      }

      private List readCertificateList(e param1) throws IOException {
         // $FF: Couldn't be decompiled
      }

      private void writeCertList(d param1, List param2) throws IOException {
         // $FF: Couldn't be decompiled
      }

      public boolean matches(Request var1, Response var2) {
         boolean var3;
         if(this.url.equals(var1.url().toString()) && this.requestMethod.equals(var1.method()) && HttpHeaders.varyMatches(var2, this.varyHeaders, var1)) {
            var3 = true;
         } else {
            var3 = false;
         }

         return var3;
      }

      public Response response(DiskLruCache.Snapshot var1) {
         String var2 = this.responseHeaders.get("Content-Type");
         String var3 = this.responseHeaders.get("Content-Length");
         Request var4 = (new Request.Builder()).url(this.url).method(this.requestMethod, (RequestBody)null).headers(this.varyHeaders).build();
         return (new Response.Builder()).request(var4).protocol(this.protocol).code(this.code).message(this.message).headers(this.responseHeaders).body(new Cache.CacheResponseBody(var1, var2, var3)).handshake(this.handshake).sentRequestAtMillis(this.sentRequestMillis).receivedResponseAtMillis(this.receivedResponseMillis).build();
      }

      public void writeTo(DiskLruCache.Editor var1) throws IOException {
         byte var3 = 0;
         d var5 = l.a(var1.newSink(0));
         var5.b(this.url).i(10);
         var5.b(this.requestMethod).i(10);
         var5.n((long)this.varyHeaders.size()).i(10);
         int var4 = this.varyHeaders.size();

         int var2;
         for(var2 = 0; var2 < var4; ++var2) {
            var5.b(this.varyHeaders.name(var2)).b(": ").b(this.varyHeaders.value(var2)).i(10);
         }

         var5.b((new StatusLine(this.protocol, this.code, this.message)).toString()).i(10);
         var5.n((long)(this.responseHeaders.size() + 2)).i(10);
         var4 = this.responseHeaders.size();

         for(var2 = var3; var2 < var4; ++var2) {
            var5.b(this.responseHeaders.name(var2)).b(": ").b(this.responseHeaders.value(var2)).i(10);
         }

         var5.b(SENT_MILLIS).b(": ").n(this.sentRequestMillis).i(10);
         var5.b(RECEIVED_MILLIS).b(": ").n(this.receivedResponseMillis).i(10);
         if(this.isHttps()) {
            var5.i(10);
            var5.b(this.handshake.cipherSuite().javaName()).i(10);
            this.writeCertList(var5, this.handshake.peerCertificates());
            this.writeCertList(var5, this.handshake.localCertificates());
            var5.b(this.handshake.tlsVersion().javaName()).i(10);
         }

         var5.close();
      }
   }
}
