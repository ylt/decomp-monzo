package okhttp3.logging;

import c.c;
import c.e;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.concurrent.TimeUnit;
import okhttp3.Connection;
import okhttp3.Headers;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Protocol;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.internal.http.HttpHeaders;
import okhttp3.internal.platform.Platform;

public final class HttpLoggingInterceptor implements Interceptor {
   private static final Charset UTF8 = Charset.forName("UTF-8");
   private volatile HttpLoggingInterceptor.Level level;
   private final HttpLoggingInterceptor.Logger logger;

   public HttpLoggingInterceptor() {
      this(HttpLoggingInterceptor.Logger.DEFAULT);
   }

   public HttpLoggingInterceptor(HttpLoggingInterceptor.Logger var1) {
      this.level = HttpLoggingInterceptor.Level.NONE;
      this.logger = var1;
   }

   private boolean bodyEncoded(Headers var1) {
      String var3 = var1.get("Content-Encoding");
      boolean var2;
      if(var3 != null && !var3.equalsIgnoreCase("identity")) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   static boolean isPlaintext(c param0) {
      // $FF: Couldn't be decompiled
   }

   public HttpLoggingInterceptor.Level getLevel() {
      return this.level;
   }

   public Response intercept(Interceptor.Chain var1) throws IOException {
      HttpLoggingInterceptor.Level var11 = this.level;
      Request var13 = var1.request();
      Response var17;
      if(var11 == HttpLoggingInterceptor.Level.NONE) {
         var17 = var1.proceed(var13);
      } else {
         boolean var2;
         if(var11 == HttpLoggingInterceptor.Level.BODY) {
            var2 = true;
         } else {
            var2 = false;
         }

         boolean var3;
         if(!var2 && var11 != HttpLoggingInterceptor.Level.HEADERS) {
            var3 = false;
         } else {
            var3 = true;
         }

         RequestBody var14 = var13.body();
         boolean var4;
         if(var14 != null) {
            var4 = true;
         } else {
            var4 = false;
         }

         Connection var20 = var1.connection();
         Protocol var21;
         if(var20 != null) {
            var21 = var20.protocol();
         } else {
            var21 = Protocol.HTTP_1_1;
         }

         String var12 = "--> " + var13.method() + ' ' + var13.url() + ' ' + var21;
         String var22 = var12;
         if(!var3) {
            var22 = var12;
            if(var4) {
               var22 = var12 + " (" + var14.contentLength() + "-byte body)";
            }
         }

         this.logger.log(var22);
         Headers var28;
         Charset var30;
         if(var3) {
            if(var4) {
               if(var14.contentType() != null) {
                  this.logger.log("Content-Type: " + var14.contentType());
               }

               if(var14.contentLength() != -1L) {
                  this.logger.log("Content-Length: " + var14.contentLength());
               }
            }

            var28 = var13.headers();
            int var5 = 0;

            for(int var6 = var28.size(); var5 < var6; ++var5) {
               var12 = var28.name(var5);
               if(!"Content-Type".equalsIgnoreCase(var12) && !"Content-Length".equalsIgnoreCase(var12)) {
                  this.logger.log(var12 + ": " + var28.value(var5));
               }
            }

            if(var2 && var4) {
               if(this.bodyEncoded(var13.headers())) {
                  this.logger.log("--> END " + var13.method() + " (encoded body omitted)");
               } else {
                  c var23 = new c();
                  var14.writeTo(var23);
                  var30 = UTF8;
                  MediaType var15 = var14.contentType();
                  if(var15 != null) {
                     var30 = var15.charset(UTF8);
                  }

                  this.logger.log("");
                  if(isPlaintext(var23)) {
                     this.logger.log(var23.a(var30));
                     this.logger.log("--> END " + var13.method() + " (" + var14.contentLength() + "-byte body)");
                  } else {
                     this.logger.log("--> END " + var13.method() + " (binary " + var14.contentLength() + "-byte body omitted)");
                  }
               }
            } else {
               this.logger.log("--> END " + var13.method());
            }
         }

         long var7 = System.nanoTime();

         try {
            var17 = var1.proceed(var13);
         } catch (Exception var16) {
            this.logger.log("<-- HTTP FAILED: " + var16);
            throw var16;
         }

         var7 = TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - var7);
         ResponseBody var24 = var17.body();
         long var9 = var24.contentLength();
         if(var9 != -1L) {
            var22 = var9 + "-byte";
         } else {
            var22 = "unknown-length";
         }

         HttpLoggingInterceptor.Logger var29 = this.logger;
         StringBuilder var25 = (new StringBuilder()).append("<-- ").append(var17.code()).append(' ').append(var17.message()).append(' ').append(var17.request().url()).append(" (").append(var7).append("ms");
         if(!var3) {
            var22 = ", " + var22 + " body";
         } else {
            var22 = "";
         }

         var29.log(var25.append(var22).append(')').toString());
         if(var3) {
            var28 = var17.headers();
            int var18 = 0;

            for(int var19 = var28.size(); var18 < var19; ++var18) {
               this.logger.log(var28.name(var18) + ": " + var28.value(var18));
            }

            if(var2 && HttpHeaders.hasBody(var17)) {
               if(this.bodyEncoded(var17.headers())) {
                  this.logger.log("<-- END HTTP (encoded body omitted)");
               } else {
                  e var31 = var24.source();
                  var31.b(Long.MAX_VALUE);
                  c var27 = var31.b();
                  var30 = UTF8;
                  MediaType var26 = var24.contentType();
                  if(var26 != null) {
                     var30 = var26.charset(UTF8);
                  }

                  if(!isPlaintext(var27)) {
                     this.logger.log("");
                     this.logger.log("<-- END HTTP (binary " + var27.a() + "-byte body omitted)");
                  } else {
                     if(var9 != 0L) {
                        this.logger.log("");
                        this.logger.log(var27.w().a(var30));
                     }

                     this.logger.log("<-- END HTTP (" + var27.a() + "-byte body)");
                  }
               }
            } else {
               this.logger.log("<-- END HTTP");
            }
         }
      }

      return var17;
   }

   public HttpLoggingInterceptor setLevel(HttpLoggingInterceptor.Level var1) {
      if(var1 == null) {
         throw new NullPointerException("level == null. Use Level.NONE instead.");
      } else {
         this.level = var1;
         return this;
      }
   }

   public static enum Level {
      BASIC,
      BODY,
      HEADERS,
      NONE;
   }

   public interface Logger {
      HttpLoggingInterceptor.Logger DEFAULT = new HttpLoggingInterceptor.Logger() {
         public void log(String var1) {
            Platform.get().log(4, var1, (Throwable)null);
         }
      };

      void log(String var1);
   }
}
