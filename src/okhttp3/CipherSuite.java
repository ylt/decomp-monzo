package okhttp3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public final class CipherSuite {
   private static final Map INSTANCES;
   static final Comparator ORDER_BY_NAME = new Comparator() {
      public int compare(String var1, String var2) {
         byte var4 = -1;
         int var3 = 4;
         int var5 = Math.min(var1.length(), var2.length());

         byte var8;
         while(true) {
            if(var3 >= var5) {
               int var9 = var1.length();
               var5 = var2.length();
               if(var9 != var5) {
                  var8 = var4;
                  if(var9 >= var5) {
                     var8 = 1;
                  }
               } else {
                  var8 = 0;
               }
               break;
            }

            char var6 = var1.charAt(var3);
            char var7 = var2.charAt(var3);
            if(var6 != var7) {
               if(var6 < var7) {
                  var8 = var4;
               } else {
                  var8 = 1;
               }
               break;
            }

            ++var3;
         }

         return var8;
      }
   };
   public static final CipherSuite TLS_DHE_DSS_EXPORT_WITH_DES40_CBC_SHA;
   public static final CipherSuite TLS_DHE_DSS_WITH_3DES_EDE_CBC_SHA;
   public static final CipherSuite TLS_DHE_DSS_WITH_AES_128_CBC_SHA;
   public static final CipherSuite TLS_DHE_DSS_WITH_AES_128_CBC_SHA256;
   public static final CipherSuite TLS_DHE_DSS_WITH_AES_128_GCM_SHA256;
   public static final CipherSuite TLS_DHE_DSS_WITH_AES_256_CBC_SHA;
   public static final CipherSuite TLS_DHE_DSS_WITH_AES_256_CBC_SHA256;
   public static final CipherSuite TLS_DHE_DSS_WITH_AES_256_GCM_SHA384;
   public static final CipherSuite TLS_DHE_DSS_WITH_CAMELLIA_128_CBC_SHA;
   public static final CipherSuite TLS_DHE_DSS_WITH_CAMELLIA_256_CBC_SHA;
   public static final CipherSuite TLS_DHE_DSS_WITH_DES_CBC_SHA;
   public static final CipherSuite TLS_DHE_RSA_EXPORT_WITH_DES40_CBC_SHA;
   public static final CipherSuite TLS_DHE_RSA_WITH_3DES_EDE_CBC_SHA;
   public static final CipherSuite TLS_DHE_RSA_WITH_AES_128_CBC_SHA;
   public static final CipherSuite TLS_DHE_RSA_WITH_AES_128_CBC_SHA256;
   public static final CipherSuite TLS_DHE_RSA_WITH_AES_128_GCM_SHA256;
   public static final CipherSuite TLS_DHE_RSA_WITH_AES_256_CBC_SHA;
   public static final CipherSuite TLS_DHE_RSA_WITH_AES_256_CBC_SHA256;
   public static final CipherSuite TLS_DHE_RSA_WITH_AES_256_GCM_SHA384;
   public static final CipherSuite TLS_DHE_RSA_WITH_CAMELLIA_128_CBC_SHA;
   public static final CipherSuite TLS_DHE_RSA_WITH_CAMELLIA_256_CBC_SHA;
   public static final CipherSuite TLS_DHE_RSA_WITH_DES_CBC_SHA;
   public static final CipherSuite TLS_DH_anon_EXPORT_WITH_DES40_CBC_SHA;
   public static final CipherSuite TLS_DH_anon_EXPORT_WITH_RC4_40_MD5;
   public static final CipherSuite TLS_DH_anon_WITH_3DES_EDE_CBC_SHA;
   public static final CipherSuite TLS_DH_anon_WITH_AES_128_CBC_SHA;
   public static final CipherSuite TLS_DH_anon_WITH_AES_128_CBC_SHA256;
   public static final CipherSuite TLS_DH_anon_WITH_AES_128_GCM_SHA256;
   public static final CipherSuite TLS_DH_anon_WITH_AES_256_CBC_SHA;
   public static final CipherSuite TLS_DH_anon_WITH_AES_256_CBC_SHA256;
   public static final CipherSuite TLS_DH_anon_WITH_AES_256_GCM_SHA384;
   public static final CipherSuite TLS_DH_anon_WITH_DES_CBC_SHA;
   public static final CipherSuite TLS_DH_anon_WITH_RC4_128_MD5;
   public static final CipherSuite TLS_ECDHE_ECDSA_WITH_3DES_EDE_CBC_SHA;
   public static final CipherSuite TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA;
   public static final CipherSuite TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256;
   public static final CipherSuite TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256;
   public static final CipherSuite TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA;
   public static final CipherSuite TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA384;
   public static final CipherSuite TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384;
   public static final CipherSuite TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305_SHA256;
   public static final CipherSuite TLS_ECDHE_ECDSA_WITH_NULL_SHA;
   public static final CipherSuite TLS_ECDHE_ECDSA_WITH_RC4_128_SHA;
   public static final CipherSuite TLS_ECDHE_PSK_WITH_AES_128_CBC_SHA;
   public static final CipherSuite TLS_ECDHE_PSK_WITH_AES_256_CBC_SHA;
   public static final CipherSuite TLS_ECDHE_RSA_WITH_3DES_EDE_CBC_SHA;
   public static final CipherSuite TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA;
   public static final CipherSuite TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256;
   public static final CipherSuite TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256;
   public static final CipherSuite TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA;
   public static final CipherSuite TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA384;
   public static final CipherSuite TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384;
   public static final CipherSuite TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305_SHA256;
   public static final CipherSuite TLS_ECDHE_RSA_WITH_NULL_SHA;
   public static final CipherSuite TLS_ECDHE_RSA_WITH_RC4_128_SHA;
   public static final CipherSuite TLS_ECDH_ECDSA_WITH_3DES_EDE_CBC_SHA;
   public static final CipherSuite TLS_ECDH_ECDSA_WITH_AES_128_CBC_SHA;
   public static final CipherSuite TLS_ECDH_ECDSA_WITH_AES_128_CBC_SHA256;
   public static final CipherSuite TLS_ECDH_ECDSA_WITH_AES_128_GCM_SHA256;
   public static final CipherSuite TLS_ECDH_ECDSA_WITH_AES_256_CBC_SHA;
   public static final CipherSuite TLS_ECDH_ECDSA_WITH_AES_256_CBC_SHA384;
   public static final CipherSuite TLS_ECDH_ECDSA_WITH_AES_256_GCM_SHA384;
   public static final CipherSuite TLS_ECDH_ECDSA_WITH_NULL_SHA;
   public static final CipherSuite TLS_ECDH_ECDSA_WITH_RC4_128_SHA;
   public static final CipherSuite TLS_ECDH_RSA_WITH_3DES_EDE_CBC_SHA;
   public static final CipherSuite TLS_ECDH_RSA_WITH_AES_128_CBC_SHA;
   public static final CipherSuite TLS_ECDH_RSA_WITH_AES_128_CBC_SHA256;
   public static final CipherSuite TLS_ECDH_RSA_WITH_AES_128_GCM_SHA256;
   public static final CipherSuite TLS_ECDH_RSA_WITH_AES_256_CBC_SHA;
   public static final CipherSuite TLS_ECDH_RSA_WITH_AES_256_CBC_SHA384;
   public static final CipherSuite TLS_ECDH_RSA_WITH_AES_256_GCM_SHA384;
   public static final CipherSuite TLS_ECDH_RSA_WITH_NULL_SHA;
   public static final CipherSuite TLS_ECDH_RSA_WITH_RC4_128_SHA;
   public static final CipherSuite TLS_ECDH_anon_WITH_3DES_EDE_CBC_SHA;
   public static final CipherSuite TLS_ECDH_anon_WITH_AES_128_CBC_SHA;
   public static final CipherSuite TLS_ECDH_anon_WITH_AES_256_CBC_SHA;
   public static final CipherSuite TLS_ECDH_anon_WITH_NULL_SHA;
   public static final CipherSuite TLS_ECDH_anon_WITH_RC4_128_SHA;
   public static final CipherSuite TLS_EMPTY_RENEGOTIATION_INFO_SCSV;
   public static final CipherSuite TLS_FALLBACK_SCSV;
   public static final CipherSuite TLS_KRB5_EXPORT_WITH_DES_CBC_40_MD5;
   public static final CipherSuite TLS_KRB5_EXPORT_WITH_DES_CBC_40_SHA;
   public static final CipherSuite TLS_KRB5_EXPORT_WITH_RC4_40_MD5;
   public static final CipherSuite TLS_KRB5_EXPORT_WITH_RC4_40_SHA;
   public static final CipherSuite TLS_KRB5_WITH_3DES_EDE_CBC_MD5;
   public static final CipherSuite TLS_KRB5_WITH_3DES_EDE_CBC_SHA;
   public static final CipherSuite TLS_KRB5_WITH_DES_CBC_MD5;
   public static final CipherSuite TLS_KRB5_WITH_DES_CBC_SHA;
   public static final CipherSuite TLS_KRB5_WITH_RC4_128_MD5;
   public static final CipherSuite TLS_KRB5_WITH_RC4_128_SHA;
   public static final CipherSuite TLS_PSK_WITH_3DES_EDE_CBC_SHA;
   public static final CipherSuite TLS_PSK_WITH_AES_128_CBC_SHA;
   public static final CipherSuite TLS_PSK_WITH_AES_256_CBC_SHA;
   public static final CipherSuite TLS_PSK_WITH_RC4_128_SHA;
   public static final CipherSuite TLS_RSA_EXPORT_WITH_DES40_CBC_SHA;
   public static final CipherSuite TLS_RSA_EXPORT_WITH_RC4_40_MD5;
   public static final CipherSuite TLS_RSA_WITH_3DES_EDE_CBC_SHA;
   public static final CipherSuite TLS_RSA_WITH_AES_128_CBC_SHA;
   public static final CipherSuite TLS_RSA_WITH_AES_128_CBC_SHA256;
   public static final CipherSuite TLS_RSA_WITH_AES_128_GCM_SHA256;
   public static final CipherSuite TLS_RSA_WITH_AES_256_CBC_SHA;
   public static final CipherSuite TLS_RSA_WITH_AES_256_CBC_SHA256;
   public static final CipherSuite TLS_RSA_WITH_AES_256_GCM_SHA384;
   public static final CipherSuite TLS_RSA_WITH_CAMELLIA_128_CBC_SHA;
   public static final CipherSuite TLS_RSA_WITH_CAMELLIA_256_CBC_SHA;
   public static final CipherSuite TLS_RSA_WITH_DES_CBC_SHA;
   public static final CipherSuite TLS_RSA_WITH_NULL_MD5;
   public static final CipherSuite TLS_RSA_WITH_NULL_SHA;
   public static final CipherSuite TLS_RSA_WITH_NULL_SHA256;
   public static final CipherSuite TLS_RSA_WITH_RC4_128_MD5;
   public static final CipherSuite TLS_RSA_WITH_RC4_128_SHA;
   public static final CipherSuite TLS_RSA_WITH_SEED_CBC_SHA;
   final String javaName;

   static {
      INSTANCES = new TreeMap(ORDER_BY_NAME);
      TLS_RSA_WITH_NULL_MD5 = of("SSL_RSA_WITH_NULL_MD5", 1);
      TLS_RSA_WITH_NULL_SHA = of("SSL_RSA_WITH_NULL_SHA", 2);
      TLS_RSA_EXPORT_WITH_RC4_40_MD5 = of("SSL_RSA_EXPORT_WITH_RC4_40_MD5", 3);
      TLS_RSA_WITH_RC4_128_MD5 = of("SSL_RSA_WITH_RC4_128_MD5", 4);
      TLS_RSA_WITH_RC4_128_SHA = of("SSL_RSA_WITH_RC4_128_SHA", 5);
      TLS_RSA_EXPORT_WITH_DES40_CBC_SHA = of("SSL_RSA_EXPORT_WITH_DES40_CBC_SHA", 8);
      TLS_RSA_WITH_DES_CBC_SHA = of("SSL_RSA_WITH_DES_CBC_SHA", 9);
      TLS_RSA_WITH_3DES_EDE_CBC_SHA = of("SSL_RSA_WITH_3DES_EDE_CBC_SHA", 10);
      TLS_DHE_DSS_EXPORT_WITH_DES40_CBC_SHA = of("SSL_DHE_DSS_EXPORT_WITH_DES40_CBC_SHA", 17);
      TLS_DHE_DSS_WITH_DES_CBC_SHA = of("SSL_DHE_DSS_WITH_DES_CBC_SHA", 18);
      TLS_DHE_DSS_WITH_3DES_EDE_CBC_SHA = of("SSL_DHE_DSS_WITH_3DES_EDE_CBC_SHA", 19);
      TLS_DHE_RSA_EXPORT_WITH_DES40_CBC_SHA = of("SSL_DHE_RSA_EXPORT_WITH_DES40_CBC_SHA", 20);
      TLS_DHE_RSA_WITH_DES_CBC_SHA = of("SSL_DHE_RSA_WITH_DES_CBC_SHA", 21);
      TLS_DHE_RSA_WITH_3DES_EDE_CBC_SHA = of("SSL_DHE_RSA_WITH_3DES_EDE_CBC_SHA", 22);
      TLS_DH_anon_EXPORT_WITH_RC4_40_MD5 = of("SSL_DH_anon_EXPORT_WITH_RC4_40_MD5", 23);
      TLS_DH_anon_WITH_RC4_128_MD5 = of("SSL_DH_anon_WITH_RC4_128_MD5", 24);
      TLS_DH_anon_EXPORT_WITH_DES40_CBC_SHA = of("SSL_DH_anon_EXPORT_WITH_DES40_CBC_SHA", 25);
      TLS_DH_anon_WITH_DES_CBC_SHA = of("SSL_DH_anon_WITH_DES_CBC_SHA", 26);
      TLS_DH_anon_WITH_3DES_EDE_CBC_SHA = of("SSL_DH_anon_WITH_3DES_EDE_CBC_SHA", 27);
      TLS_KRB5_WITH_DES_CBC_SHA = of("TLS_KRB5_WITH_DES_CBC_SHA", 30);
      TLS_KRB5_WITH_3DES_EDE_CBC_SHA = of("TLS_KRB5_WITH_3DES_EDE_CBC_SHA", 31);
      TLS_KRB5_WITH_RC4_128_SHA = of("TLS_KRB5_WITH_RC4_128_SHA", 32);
      TLS_KRB5_WITH_DES_CBC_MD5 = of("TLS_KRB5_WITH_DES_CBC_MD5", 34);
      TLS_KRB5_WITH_3DES_EDE_CBC_MD5 = of("TLS_KRB5_WITH_3DES_EDE_CBC_MD5", 35);
      TLS_KRB5_WITH_RC4_128_MD5 = of("TLS_KRB5_WITH_RC4_128_MD5", 36);
      TLS_KRB5_EXPORT_WITH_DES_CBC_40_SHA = of("TLS_KRB5_EXPORT_WITH_DES_CBC_40_SHA", 38);
      TLS_KRB5_EXPORT_WITH_RC4_40_SHA = of("TLS_KRB5_EXPORT_WITH_RC4_40_SHA", 40);
      TLS_KRB5_EXPORT_WITH_DES_CBC_40_MD5 = of("TLS_KRB5_EXPORT_WITH_DES_CBC_40_MD5", 41);
      TLS_KRB5_EXPORT_WITH_RC4_40_MD5 = of("TLS_KRB5_EXPORT_WITH_RC4_40_MD5", 43);
      TLS_RSA_WITH_AES_128_CBC_SHA = of("TLS_RSA_WITH_AES_128_CBC_SHA", 47);
      TLS_DHE_DSS_WITH_AES_128_CBC_SHA = of("TLS_DHE_DSS_WITH_AES_128_CBC_SHA", 50);
      TLS_DHE_RSA_WITH_AES_128_CBC_SHA = of("TLS_DHE_RSA_WITH_AES_128_CBC_SHA", 51);
      TLS_DH_anon_WITH_AES_128_CBC_SHA = of("TLS_DH_anon_WITH_AES_128_CBC_SHA", 52);
      TLS_RSA_WITH_AES_256_CBC_SHA = of("TLS_RSA_WITH_AES_256_CBC_SHA", 53);
      TLS_DHE_DSS_WITH_AES_256_CBC_SHA = of("TLS_DHE_DSS_WITH_AES_256_CBC_SHA", 56);
      TLS_DHE_RSA_WITH_AES_256_CBC_SHA = of("TLS_DHE_RSA_WITH_AES_256_CBC_SHA", 57);
      TLS_DH_anon_WITH_AES_256_CBC_SHA = of("TLS_DH_anon_WITH_AES_256_CBC_SHA", 58);
      TLS_RSA_WITH_NULL_SHA256 = of("TLS_RSA_WITH_NULL_SHA256", 59);
      TLS_RSA_WITH_AES_128_CBC_SHA256 = of("TLS_RSA_WITH_AES_128_CBC_SHA256", 60);
      TLS_RSA_WITH_AES_256_CBC_SHA256 = of("TLS_RSA_WITH_AES_256_CBC_SHA256", 61);
      TLS_DHE_DSS_WITH_AES_128_CBC_SHA256 = of("TLS_DHE_DSS_WITH_AES_128_CBC_SHA256", 64);
      TLS_RSA_WITH_CAMELLIA_128_CBC_SHA = of("TLS_RSA_WITH_CAMELLIA_128_CBC_SHA", 65);
      TLS_DHE_DSS_WITH_CAMELLIA_128_CBC_SHA = of("TLS_DHE_DSS_WITH_CAMELLIA_128_CBC_SHA", 68);
      TLS_DHE_RSA_WITH_CAMELLIA_128_CBC_SHA = of("TLS_DHE_RSA_WITH_CAMELLIA_128_CBC_SHA", 69);
      TLS_DHE_RSA_WITH_AES_128_CBC_SHA256 = of("TLS_DHE_RSA_WITH_AES_128_CBC_SHA256", 103);
      TLS_DHE_DSS_WITH_AES_256_CBC_SHA256 = of("TLS_DHE_DSS_WITH_AES_256_CBC_SHA256", 106);
      TLS_DHE_RSA_WITH_AES_256_CBC_SHA256 = of("TLS_DHE_RSA_WITH_AES_256_CBC_SHA256", 107);
      TLS_DH_anon_WITH_AES_128_CBC_SHA256 = of("TLS_DH_anon_WITH_AES_128_CBC_SHA256", 108);
      TLS_DH_anon_WITH_AES_256_CBC_SHA256 = of("TLS_DH_anon_WITH_AES_256_CBC_SHA256", 109);
      TLS_RSA_WITH_CAMELLIA_256_CBC_SHA = of("TLS_RSA_WITH_CAMELLIA_256_CBC_SHA", 132);
      TLS_DHE_DSS_WITH_CAMELLIA_256_CBC_SHA = of("TLS_DHE_DSS_WITH_CAMELLIA_256_CBC_SHA", 135);
      TLS_DHE_RSA_WITH_CAMELLIA_256_CBC_SHA = of("TLS_DHE_RSA_WITH_CAMELLIA_256_CBC_SHA", 136);
      TLS_PSK_WITH_RC4_128_SHA = of("TLS_PSK_WITH_RC4_128_SHA", 138);
      TLS_PSK_WITH_3DES_EDE_CBC_SHA = of("TLS_PSK_WITH_3DES_EDE_CBC_SHA", 139);
      TLS_PSK_WITH_AES_128_CBC_SHA = of("TLS_PSK_WITH_AES_128_CBC_SHA", 140);
      TLS_PSK_WITH_AES_256_CBC_SHA = of("TLS_PSK_WITH_AES_256_CBC_SHA", 141);
      TLS_RSA_WITH_SEED_CBC_SHA = of("TLS_RSA_WITH_SEED_CBC_SHA", 150);
      TLS_RSA_WITH_AES_128_GCM_SHA256 = of("TLS_RSA_WITH_AES_128_GCM_SHA256", 156);
      TLS_RSA_WITH_AES_256_GCM_SHA384 = of("TLS_RSA_WITH_AES_256_GCM_SHA384", 157);
      TLS_DHE_RSA_WITH_AES_128_GCM_SHA256 = of("TLS_DHE_RSA_WITH_AES_128_GCM_SHA256", 158);
      TLS_DHE_RSA_WITH_AES_256_GCM_SHA384 = of("TLS_DHE_RSA_WITH_AES_256_GCM_SHA384", 159);
      TLS_DHE_DSS_WITH_AES_128_GCM_SHA256 = of("TLS_DHE_DSS_WITH_AES_128_GCM_SHA256", 162);
      TLS_DHE_DSS_WITH_AES_256_GCM_SHA384 = of("TLS_DHE_DSS_WITH_AES_256_GCM_SHA384", 163);
      TLS_DH_anon_WITH_AES_128_GCM_SHA256 = of("TLS_DH_anon_WITH_AES_128_GCM_SHA256", 166);
      TLS_DH_anon_WITH_AES_256_GCM_SHA384 = of("TLS_DH_anon_WITH_AES_256_GCM_SHA384", 167);
      TLS_EMPTY_RENEGOTIATION_INFO_SCSV = of("TLS_EMPTY_RENEGOTIATION_INFO_SCSV", 255);
      TLS_FALLBACK_SCSV = of("TLS_FALLBACK_SCSV", 22016);
      TLS_ECDH_ECDSA_WITH_NULL_SHA = of("TLS_ECDH_ECDSA_WITH_NULL_SHA", '쀁');
      TLS_ECDH_ECDSA_WITH_RC4_128_SHA = of("TLS_ECDH_ECDSA_WITH_RC4_128_SHA", '쀂');
      TLS_ECDH_ECDSA_WITH_3DES_EDE_CBC_SHA = of("TLS_ECDH_ECDSA_WITH_3DES_EDE_CBC_SHA", '쀃');
      TLS_ECDH_ECDSA_WITH_AES_128_CBC_SHA = of("TLS_ECDH_ECDSA_WITH_AES_128_CBC_SHA", '쀄');
      TLS_ECDH_ECDSA_WITH_AES_256_CBC_SHA = of("TLS_ECDH_ECDSA_WITH_AES_256_CBC_SHA", '쀅');
      TLS_ECDHE_ECDSA_WITH_NULL_SHA = of("TLS_ECDHE_ECDSA_WITH_NULL_SHA", '쀆');
      TLS_ECDHE_ECDSA_WITH_RC4_128_SHA = of("TLS_ECDHE_ECDSA_WITH_RC4_128_SHA", '쀇');
      TLS_ECDHE_ECDSA_WITH_3DES_EDE_CBC_SHA = of("TLS_ECDHE_ECDSA_WITH_3DES_EDE_CBC_SHA", '쀈');
      TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA = of("TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA", '쀉');
      TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA = of("TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA", '쀊');
      TLS_ECDH_RSA_WITH_NULL_SHA = of("TLS_ECDH_RSA_WITH_NULL_SHA", '쀋');
      TLS_ECDH_RSA_WITH_RC4_128_SHA = of("TLS_ECDH_RSA_WITH_RC4_128_SHA", '쀌');
      TLS_ECDH_RSA_WITH_3DES_EDE_CBC_SHA = of("TLS_ECDH_RSA_WITH_3DES_EDE_CBC_SHA", '쀍');
      TLS_ECDH_RSA_WITH_AES_128_CBC_SHA = of("TLS_ECDH_RSA_WITH_AES_128_CBC_SHA", '쀎');
      TLS_ECDH_RSA_WITH_AES_256_CBC_SHA = of("TLS_ECDH_RSA_WITH_AES_256_CBC_SHA", '쀏');
      TLS_ECDHE_RSA_WITH_NULL_SHA = of("TLS_ECDHE_RSA_WITH_NULL_SHA", '쀐');
      TLS_ECDHE_RSA_WITH_RC4_128_SHA = of("TLS_ECDHE_RSA_WITH_RC4_128_SHA", '쀑');
      TLS_ECDHE_RSA_WITH_3DES_EDE_CBC_SHA = of("TLS_ECDHE_RSA_WITH_3DES_EDE_CBC_SHA", '쀒');
      TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA = of("TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA", '쀓');
      TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA = of("TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA", '쀔');
      TLS_ECDH_anon_WITH_NULL_SHA = of("TLS_ECDH_anon_WITH_NULL_SHA", '쀕');
      TLS_ECDH_anon_WITH_RC4_128_SHA = of("TLS_ECDH_anon_WITH_RC4_128_SHA", '쀖');
      TLS_ECDH_anon_WITH_3DES_EDE_CBC_SHA = of("TLS_ECDH_anon_WITH_3DES_EDE_CBC_SHA", '쀗');
      TLS_ECDH_anon_WITH_AES_128_CBC_SHA = of("TLS_ECDH_anon_WITH_AES_128_CBC_SHA", '쀘');
      TLS_ECDH_anon_WITH_AES_256_CBC_SHA = of("TLS_ECDH_anon_WITH_AES_256_CBC_SHA", '쀙');
      TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256 = of("TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256", '쀣');
      TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA384 = of("TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA384", '쀤');
      TLS_ECDH_ECDSA_WITH_AES_128_CBC_SHA256 = of("TLS_ECDH_ECDSA_WITH_AES_128_CBC_SHA256", '쀥');
      TLS_ECDH_ECDSA_WITH_AES_256_CBC_SHA384 = of("TLS_ECDH_ECDSA_WITH_AES_256_CBC_SHA384", '쀦');
      TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256 = of("TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256", '쀧');
      TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA384 = of("TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA384", '쀨');
      TLS_ECDH_RSA_WITH_AES_128_CBC_SHA256 = of("TLS_ECDH_RSA_WITH_AES_128_CBC_SHA256", '쀩');
      TLS_ECDH_RSA_WITH_AES_256_CBC_SHA384 = of("TLS_ECDH_RSA_WITH_AES_256_CBC_SHA384", '쀪');
      TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256 = of("TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256", '쀫');
      TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384 = of("TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384", '쀬');
      TLS_ECDH_ECDSA_WITH_AES_128_GCM_SHA256 = of("TLS_ECDH_ECDSA_WITH_AES_128_GCM_SHA256", '쀭');
      TLS_ECDH_ECDSA_WITH_AES_256_GCM_SHA384 = of("TLS_ECDH_ECDSA_WITH_AES_256_GCM_SHA384", '쀮');
      TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256 = of("TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256", '쀯');
      TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384 = of("TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384", '쀰');
      TLS_ECDH_RSA_WITH_AES_128_GCM_SHA256 = of("TLS_ECDH_RSA_WITH_AES_128_GCM_SHA256", '쀱');
      TLS_ECDH_RSA_WITH_AES_256_GCM_SHA384 = of("TLS_ECDH_RSA_WITH_AES_256_GCM_SHA384", '쀲');
      TLS_ECDHE_PSK_WITH_AES_128_CBC_SHA = of("TLS_ECDHE_PSK_WITH_AES_128_CBC_SHA", '쀵');
      TLS_ECDHE_PSK_WITH_AES_256_CBC_SHA = of("TLS_ECDHE_PSK_WITH_AES_256_CBC_SHA", '쀶');
      TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305_SHA256 = of("TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305_SHA256", '첨');
      TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305_SHA256 = of("TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305_SHA256", '첩');
   }

   private CipherSuite(String var1) {
      if(var1 == null) {
         throw new NullPointerException();
      } else {
         this.javaName = var1;
      }
   }

   public static CipherSuite forJavaName(String param0) {
      // $FF: Couldn't be decompiled
   }

   static List forJavaNames(String... var0) {
      ArrayList var3 = new ArrayList(var0.length);
      int var2 = var0.length;

      for(int var1 = 0; var1 < var2; ++var1) {
         var3.add(forJavaName(var0[var1]));
      }

      return Collections.unmodifiableList(var3);
   }

   private static CipherSuite of(String var0, int var1) {
      return forJavaName(var0);
   }

   public String javaName() {
      return this.javaName;
   }

   public String toString() {
      return this.javaName;
   }
}
