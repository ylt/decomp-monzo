package okhttp3;

import javax.annotation.Nullable;

public final class Challenge {
   private final String realm;
   private final String scheme;

   public Challenge(String var1, String var2) {
      if(var1 == null) {
         throw new NullPointerException("scheme == null");
      } else if(var2 == null) {
         throw new NullPointerException("realm == null");
      } else {
         this.scheme = var1;
         this.realm = var2;
      }
   }

   public boolean equals(@Nullable Object var1) {
      boolean var2;
      if(var1 instanceof Challenge && ((Challenge)var1).scheme.equals(this.scheme) && ((Challenge)var1).realm.equals(this.realm)) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   public int hashCode() {
      return (this.realm.hashCode() + 899) * 31 + this.scheme.hashCode();
   }

   public String realm() {
      return this.realm;
   }

   public String scheme() {
      return this.scheme;
   }

   public String toString() {
      return this.scheme + " realm=\"" + this.realm + "\"";
   }
}
