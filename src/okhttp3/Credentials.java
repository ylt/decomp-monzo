package okhttp3;

import c.f;
import java.nio.charset.Charset;

public final class Credentials {
   public static String basic(String var0, String var1) {
      return basic(var0, var1, Charset.forName("ISO-8859-1"));
   }

   public static String basic(String var0, String var1, Charset var2) {
      var0 = f.a((var0 + ":" + var1).getBytes(var2)).b();
      return "Basic " + var0;
   }
}
