package retrofit2.adapter.rxjava;

import java.lang.reflect.Type;
import javax.annotation.Nullable;
import retrofit2.Call;
import retrofit2.CallAdapter;
import rx.d;
import rx.g;

final class RxJavaCallAdapter implements CallAdapter {
   private final boolean isAsync;
   private final boolean isBody;
   private final boolean isCompletable;
   private final boolean isResult;
   private final boolean isSingle;
   private final Type responseType;
   @Nullable
   private final g scheduler;

   RxJavaCallAdapter(Type var1, @Nullable g var2, boolean var3, boolean var4, boolean var5, boolean var6, boolean var7) {
      this.responseType = var1;
      this.scheduler = var2;
      this.isAsync = var3;
      this.isResult = var4;
      this.isBody = var5;
      this.isSingle = var6;
      this.isCompletable = var7;
   }

   public Object adapt(Call var1) {
      Object var3;
      if(this.isAsync) {
         var3 = new CallEnqueueOnSubscribe(var1);
      } else {
         var3 = new CallExecuteOnSubscribe(var1);
      }

      Object var2;
      if(this.isResult) {
         var2 = new ResultOnSubscribe((d.a)var3);
      } else {
         var2 = var3;
         if(this.isBody) {
            var2 = new BodyOnSubscribe((d.a)var3);
         }
      }

      d var5 = d.a((d.a)var2);
      d var4 = var5;
      if(this.scheduler != null) {
         var4 = var5.a(this.scheduler);
      }

      if(this.isSingle) {
         var2 = var4.a();
      } else {
         var2 = var4;
         if(this.isCompletable) {
            var2 = var4.b();
         }
      }

      return var2;
   }

   public Type responseType() {
      return this.responseType;
   }
}
