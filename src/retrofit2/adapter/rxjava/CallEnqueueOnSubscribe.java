package retrofit2.adapter.rxjava;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.d;
import rx.j;
import rx.exceptions.a;

final class CallEnqueueOnSubscribe implements d.a {
   private final Call originalCall;

   CallEnqueueOnSubscribe(Call var1) {
      this.originalCall = var1;
   }

   public void call(j var1) {
      Call var3 = this.originalCall.clone();
      final CallArbiter var2 = new CallArbiter(var3, var1);
      var1.add(var2);
      var1.setProducer(var2);
      var3.enqueue(new Callback() {
         public void onFailure(Call var1, Throwable var2x) {
            a.a(var2x);
            var2.emitError(var2x);
         }

         public void onResponse(Call var1, Response var2x) {
            var2.emitResponse(var2x);
         }
      });
   }
}
