package retrofit2.adapter.rxjava;

import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import javax.annotation.Nullable;
import retrofit2.CallAdapter;
import retrofit2.Response;
import retrofit2.Retrofit;
import rx.a;
import rx.d;
import rx.g;
import rx.h;

public final class RxJavaCallAdapterFactory extends CallAdapter.Factory {
   private final boolean isAsync;
   @Nullable
   private final g scheduler;

   private RxJavaCallAdapterFactory(@Nullable g var1, boolean var2) {
      this.scheduler = var1;
      this.isAsync = var2;
   }

   public static RxJavaCallAdapterFactory create() {
      return new RxJavaCallAdapterFactory((g)null, false);
   }

   public static RxJavaCallAdapterFactory createAsync() {
      return new RxJavaCallAdapterFactory((g)null, true);
   }

   public static RxJavaCallAdapterFactory createWithScheduler(g var0) {
      if(var0 == null) {
         throw new NullPointerException("scheduler == null");
      } else {
         return new RxJavaCallAdapterFactory(var0, false);
      }
   }

   public CallAdapter get(Type var1, Annotation[] var2, Retrofit var3) {
      Class var9 = getRawType(var1);
      boolean var5;
      if(var9 == h.class) {
         var5 = true;
      } else {
         var5 = false;
      }

      boolean var4;
      if(var9 == a.class) {
         var4 = true;
      } else {
         var4 = false;
      }

      RxJavaCallAdapter var8;
      if(var9 != d.class && !var5 && !var4) {
         var8 = null;
      } else if(var4) {
         var8 = new RxJavaCallAdapter(Void.class, this.scheduler, this.isAsync, false, true, false, true);
      } else {
         boolean var6 = false;
         boolean var7 = false;
         if(!(var1 instanceof ParameterizedType)) {
            String var10;
            if(var5) {
               var10 = "Single";
            } else {
               var10 = "Observable";
            }

            throw new IllegalStateException(var10 + " return type must be parameterized as " + var10 + "<Foo> or " + var10 + "<? extends Foo>");
         }

         var1 = getParameterUpperBound(0, (ParameterizedType)var1);
         var9 = getRawType(var1);
         if(var9 == Response.class) {
            if(!(var1 instanceof ParameterizedType)) {
               throw new IllegalStateException("Response must be parameterized as Response<Foo> or Response<? extends Foo>");
            }

            var1 = getParameterUpperBound(0, (ParameterizedType)var1);
         } else if(var9 == Result.class) {
            if(!(var1 instanceof ParameterizedType)) {
               throw new IllegalStateException("Result must be parameterized as Result<Foo> or Result<? extends Foo>");
            }

            var1 = getParameterUpperBound(0, (ParameterizedType)var1);
            var6 = true;
         } else {
            var7 = true;
         }

         var8 = new RxJavaCallAdapter(var1, this.scheduler, this.isAsync, var6, var7, var5, false);
      }

      return var8;
   }
}
