package retrofit2.adapter.rxjava;

import retrofit2.Response;
import rx.d;
import rx.j;
import rx.c.f;
import rx.exceptions.CompositeException;
import rx.exceptions.OnCompletedFailedException;
import rx.exceptions.OnErrorFailedException;
import rx.exceptions.OnErrorNotImplementedException;
import rx.exceptions.a;

final class BodyOnSubscribe implements d.a {
   private final d.a upstream;

   BodyOnSubscribe(d.a var1) {
      this.upstream = var1;
   }

   public void call(j var1) {
      this.upstream.call(new BodyOnSubscribe.BodySubscriber(var1));
   }

   private static class BodySubscriber extends j {
      private final j subscriber;
      private boolean subscriberTerminated;

      BodySubscriber(j var1) {
         super(var1);
         this.subscriber = var1;
      }

      public void onCompleted() {
         if(!this.subscriberTerminated) {
            this.subscriber.onCompleted();
         }

      }

      public void onError(Throwable var1) {
         if(!this.subscriberTerminated) {
            this.subscriber.onError(var1);
         } else {
            AssertionError var2 = new AssertionError("This should never happen! Report as a Retrofit bug with the full stacktrace.");
            var2.initCause(var1);
            f.a().b().a((Throwable)var2);
         }

      }

      public void onNext(Response var1) {
         if(var1.isSuccessful()) {
            this.subscriber.onNext(var1.body());
         } else {
            this.subscriberTerminated = true;
            HttpException var7 = new HttpException(var1);

            Object var9;
            try {
               this.subscriber.onError(var7);
               return;
            } catch (OnCompletedFailedException var3) {
               var9 = var3;
            } catch (OnErrorFailedException var4) {
               var9 = var4;
            } catch (OnErrorNotImplementedException var5) {
               var9 = var5;
            } catch (Throwable var6) {
               a.a(var6);
               CompositeException var8 = new CompositeException(new Throwable[]{var7, var6});
               f.a().b().a((Throwable)var8);
               return;
            }

            f.a().b().a((Throwable)var9);
         }

      }
   }
}
