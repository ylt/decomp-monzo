package retrofit2.adapter.rxjava;

import retrofit2.Call;
import retrofit2.Response;
import rx.d;
import rx.j;
import rx.exceptions.a;

final class CallExecuteOnSubscribe implements d.a {
   private final Call originalCall;

   CallExecuteOnSubscribe(Call var1) {
      this.originalCall = var1;
   }

   public void call(j var1) {
      Call var3 = this.originalCall.clone();
      CallArbiter var2 = new CallArbiter(var3, var1);
      var1.add(var2);
      var1.setProducer(var2);

      Response var5;
      try {
         var5 = var3.execute();
      } catch (Throwable var4) {
         a.a(var4);
         var2.emitError(var4);
         return;
      }

      var2.emitResponse(var5);
   }
}
