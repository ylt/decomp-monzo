package retrofit2.adapter.rxjava;

import retrofit2.Response;
import rx.d;
import rx.j;
import rx.c.f;
import rx.exceptions.CompositeException;
import rx.exceptions.OnCompletedFailedException;
import rx.exceptions.OnErrorFailedException;
import rx.exceptions.OnErrorNotImplementedException;
import rx.exceptions.a;

final class ResultOnSubscribe implements d.a {
   private final d.a upstream;

   ResultOnSubscribe(d.a var1) {
      this.upstream = var1;
   }

   public void call(j var1) {
      this.upstream.call(new ResultOnSubscribe.ResultSubscriber(var1));
   }

   private static class ResultSubscriber extends j {
      private final j subscriber;

      ResultSubscriber(j var1) {
         super(var1);
         this.subscriber = var1;
      }

      public void onCompleted() {
         this.subscriber.onCompleted();
      }

      public void onError(Throwable var1) {
         try {
            this.subscriber.onNext(Result.error(var1));
         } catch (Throwable var7) {
            var1 = var7;

            Object var9;
            try {
               this.subscriber.onError(var1);
               return;
            } catch (OnCompletedFailedException var3) {
               var9 = var3;
            } catch (OnErrorFailedException var4) {
               var9 = var4;
            } catch (OnErrorNotImplementedException var5) {
               var9 = var5;
            } catch (Throwable var6) {
               a.a(var6);
               CompositeException var8 = new CompositeException(new Throwable[]{var7, var6});
               f.a().b().a((Throwable)var8);
               return;
            }

            f.a().b().a((Throwable)var9);
            return;
         }

         this.subscriber.onCompleted();
      }

      public void onNext(Response var1) {
         this.subscriber.onNext(Result.response(var1));
      }
   }
}
