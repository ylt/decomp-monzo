package retrofit2.adapter.rxjava;

import java.util.concurrent.atomic.AtomicInteger;
import retrofit2.Call;
import retrofit2.Response;
import rx.f;
import rx.j;
import rx.k;
import rx.exceptions.CompositeException;
import rx.exceptions.OnCompletedFailedException;
import rx.exceptions.OnErrorFailedException;
import rx.exceptions.OnErrorNotImplementedException;
import rx.exceptions.a;

final class CallArbiter extends AtomicInteger implements f, k {
   private static final int STATE_HAS_RESPONSE = 2;
   private static final int STATE_REQUESTED = 1;
   private static final int STATE_TERMINATED = 3;
   private static final int STATE_WAITING = 0;
   private final Call call;
   private volatile Response response;
   private final j subscriber;

   CallArbiter(Call var1, j var2) {
      super(0);
      this.call = var1;
      this.subscriber = var2;
   }

   private void deliverResponse(Response var1) {
      Object var17;
      label77: {
         try {
            if(!this.isUnsubscribed()) {
               this.subscriber.onNext(var1);
            }
            break label77;
         } catch (OnCompletedFailedException var11) {
            var17 = var11;
         } catch (OnErrorFailedException var12) {
            var17 = var12;
         } catch (OnErrorNotImplementedException var13) {
            var17 = var13;
         } catch (Throwable var14) {
            Throwable var15 = var14;
            a.a(var14);

            try {
               this.subscriber.onError(var15);
               return;
            } catch (OnCompletedFailedException var7) {
               var17 = var7;
            } catch (OnErrorFailedException var8) {
               var17 = var8;
            } catch (OnErrorNotImplementedException var9) {
               var17 = var9;
            } catch (Throwable var10) {
               a.a(var10);
               CompositeException var16 = new CompositeException(new Throwable[]{var14, var10});
               rx.c.f.a().b().a((Throwable)var16);
               return;
            }

            rx.c.f.a().b().a((Throwable)var17);
            return;
         }

         rx.c.f.a().b().a((Throwable)var17);
         return;
      }

      try {
         if(!this.isUnsubscribed()) {
            this.subscriber.onCompleted();
         }

         return;
      } catch (OnCompletedFailedException var3) {
         var17 = var3;
      } catch (OnErrorFailedException var4) {
         var17 = var4;
      } catch (OnErrorNotImplementedException var5) {
         var17 = var5;
      } catch (Throwable var6) {
         a.a(var6);
         rx.c.f.a().b().a(var6);
         return;
      }

      rx.c.f.a().b().a((Throwable)var17);
   }

   void emitError(Throwable var1) {
      this.set(3);
      if(!this.isUnsubscribed()) {
         Object var8;
         try {
            this.subscriber.onError(var1);
            return;
         } catch (OnCompletedFailedException var3) {
            var8 = var3;
         } catch (OnErrorFailedException var4) {
            var8 = var4;
         } catch (OnErrorNotImplementedException var5) {
            var8 = var5;
         } catch (Throwable var6) {
            a.a(var6);
            CompositeException var7 = new CompositeException(new Throwable[]{var1, var6});
            rx.c.f.a().b().a((Throwable)var7);
            return;
         }

         rx.c.f.a().b().a((Throwable)var8);
      }

   }

   void emitResponse(Response var1) {
      while(true) {
         int var2 = this.get();
         switch(var2) {
         case 0:
            this.response = var1;
            if(!this.compareAndSet(0, 2)) {
               continue;
            }
            break;
         case 1:
            if(!this.compareAndSet(1, 3)) {
               continue;
            }

            this.deliverResponse(var1);
            break;
         case 2:
         case 3:
            throw new AssertionError();
         default:
            throw new IllegalStateException("Unknown state: " + var2);
         }

         return;
      }
   }

   public boolean isUnsubscribed() {
      return this.call.isCanceled();
   }

   public void request(long var1) {
      if(var1 != 0L) {
         while(true) {
            int var3 = this.get();
            switch(var3) {
            case 0:
               if(this.compareAndSet(0, 1)) {
                  return;
               }
               break;
            case 1:
            case 3:
               return;
            case 2:
               if(this.compareAndSet(2, 3)) {
                  this.deliverResponse(this.response);
                  return;
               }
               break;
            default:
               throw new IllegalStateException("Unknown state: " + var3);
            }
         }
      }
   }

   public void unsubscribe() {
      this.call.cancel();
   }
}
