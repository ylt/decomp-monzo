package retrofit2.adapter.rxjava2;

import io.reactivex.b;
import io.reactivex.f;
import io.reactivex.h;
import io.reactivex.n;
import io.reactivex.u;
import io.reactivex.v;
import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import javax.annotation.Nullable;
import retrofit2.CallAdapter;
import retrofit2.Response;
import retrofit2.Retrofit;

public final class RxJava2CallAdapterFactory extends CallAdapter.Factory {
   private final boolean isAsync;
   @Nullable
   private final u scheduler;

   private RxJava2CallAdapterFactory(@Nullable u var1, boolean var2) {
      this.scheduler = var1;
      this.isAsync = var2;
   }

   public static RxJava2CallAdapterFactory create() {
      return new RxJava2CallAdapterFactory((u)null, false);
   }

   public static RxJava2CallAdapterFactory createAsync() {
      return new RxJava2CallAdapterFactory((u)null, true);
   }

   public static RxJava2CallAdapterFactory createWithScheduler(u var0) {
      if(var0 == null) {
         throw new NullPointerException("scheduler == null");
      } else {
         return new RxJava2CallAdapterFactory(var0, false);
      }
   }

   public CallAdapter get(Type var1, Annotation[] var2, Retrofit var3) {
      Class var10 = getRawType(var1);
      RxJava2CallAdapter var9;
      if(var10 == b.class) {
         var9 = new RxJava2CallAdapter(Void.class, this.scheduler, this.isAsync, false, true, false, false, false, true);
      } else {
         boolean var4;
         if(var10 == f.class) {
            var4 = true;
         } else {
            var4 = false;
         }

         boolean var5;
         if(var10 == v.class) {
            var5 = true;
         } else {
            var5 = false;
         }

         boolean var6;
         if(var10 == h.class) {
            var6 = true;
         } else {
            var6 = false;
         }

         if(var10 != n.class && !var4 && !var5 && !var6) {
            var9 = null;
         } else {
            boolean var7 = false;
            boolean var8 = false;
            if(!(var1 instanceof ParameterizedType)) {
               String var11;
               if(var4) {
                  var11 = "Flowable";
               } else if(var5) {
                  var11 = "Single";
               } else if(var6) {
                  var11 = "Maybe";
               } else {
                  var11 = "Observable";
               }

               throw new IllegalStateException(var11 + " return type must be parameterized as " + var11 + "<Foo> or " + var11 + "<? extends Foo>");
            }

            var1 = getParameterUpperBound(0, (ParameterizedType)var1);
            var10 = getRawType(var1);
            if(var10 == Response.class) {
               if(!(var1 instanceof ParameterizedType)) {
                  throw new IllegalStateException("Response must be parameterized as Response<Foo> or Response<? extends Foo>");
               }

               var1 = getParameterUpperBound(0, (ParameterizedType)var1);
            } else if(var10 == Result.class) {
               if(!(var1 instanceof ParameterizedType)) {
                  throw new IllegalStateException("Result must be parameterized as Result<Foo> or Result<? extends Foo>");
               }

               var1 = getParameterUpperBound(0, (ParameterizedType)var1);
               var7 = true;
            } else {
               var8 = true;
            }

            var9 = new RxJava2CallAdapter(var1, this.scheduler, this.isAsync, var7, var8, var4, var5, var6, false);
         }
      }

      return var9;
   }
}
