package retrofit2.adapter.rxjava2;

import io.reactivex.n;
import io.reactivex.t;
import io.reactivex.b.b;
import io.reactivex.exceptions.CompositeException;
import io.reactivex.g.a;
import retrofit2.Response;

final class BodyObservable extends n {
   private final n upstream;

   BodyObservable(n var1) {
      this.upstream = var1;
   }

   protected void subscribeActual(t var1) {
      this.upstream.subscribe((t)(new BodyObservable.BodyObserver(var1)));
   }

   private static class BodyObserver implements t {
      private final t observer;
      private boolean terminated;

      BodyObserver(t var1) {
         this.observer = var1;
      }

      public void onComplete() {
         if(!this.terminated) {
            this.observer.onComplete();
         }

      }

      public void onError(Throwable var1) {
         if(!this.terminated) {
            this.observer.onError(var1);
         } else {
            AssertionError var2 = new AssertionError("This should never happen! Report as a bug with the full stacktrace.");
            var2.initCause(var1);
            a.a((Throwable)var2);
         }

      }

      public void onNext(Response var1) {
         if(var1.isSuccessful()) {
            this.observer.onNext(var1.body());
         } else {
            this.terminated = true;
            HttpException var2 = new HttpException(var1);

            try {
               this.observer.onError(var2);
            } catch (Throwable var3) {
               io.reactivex.exceptions.a.b(var3);
               a.a((Throwable)(new CompositeException(new Throwable[]{var2, var3})));
            }
         }

      }

      public void onSubscribe(b var1) {
         this.observer.onSubscribe(var1);
      }
   }
}
