package retrofit2.adapter.rxjava2;

import io.reactivex.a;
import io.reactivex.n;
import io.reactivex.u;
import java.lang.reflect.Type;
import javax.annotation.Nullable;
import retrofit2.Call;
import retrofit2.CallAdapter;

final class RxJava2CallAdapter implements CallAdapter {
   private final boolean isAsync;
   private final boolean isBody;
   private final boolean isCompletable;
   private final boolean isFlowable;
   private final boolean isMaybe;
   private final boolean isResult;
   private final boolean isSingle;
   private final Type responseType;
   @Nullable
   private final u scheduler;

   RxJava2CallAdapter(Type var1, @Nullable u var2, boolean var3, boolean var4, boolean var5, boolean var6, boolean var7, boolean var8, boolean var9) {
      this.responseType = var1;
      this.scheduler = var2;
      this.isAsync = var3;
      this.isResult = var4;
      this.isBody = var5;
      this.isFlowable = var6;
      this.isSingle = var7;
      this.isMaybe = var8;
      this.isCompletable = var9;
   }

   public Object adapt(Call var1) {
      Object var2;
      if(this.isAsync) {
         var2 = new CallEnqueueObservable(var1);
      } else {
         var2 = new CallExecuteObservable(var1);
      }

      Object var3;
      if(this.isResult) {
         var3 = new ResultObservable((n)var2);
      } else {
         var3 = var2;
         if(this.isBody) {
            var3 = new BodyObservable((n)var2);
         }
      }

      var2 = var3;
      if(this.scheduler != null) {
         var2 = ((n)var3).subscribeOn(this.scheduler);
      }

      if(this.isFlowable) {
         var3 = ((n)var2).toFlowable(a.e);
      } else if(this.isSingle) {
         var3 = ((n)var2).singleOrError();
      } else if(this.isMaybe) {
         var3 = ((n)var2).singleElement();
      } else {
         var3 = var2;
         if(this.isCompletable) {
            var3 = ((n)var2).ignoreElements();
         }
      }

      return var3;
   }

   public Type responseType() {
      return this.responseType;
   }
}
