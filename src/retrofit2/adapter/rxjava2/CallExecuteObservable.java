package retrofit2.adapter.rxjava2;

import io.reactivex.n;
import io.reactivex.t;
import io.reactivex.b.b;
import io.reactivex.exceptions.CompositeException;
import io.reactivex.exceptions.a;
import retrofit2.Call;
import retrofit2.Response;

final class CallExecuteObservable extends n {
   private final Call originalCall;

   CallExecuteObservable(Call var1) {
      this.originalCall = var1;
   }

   protected void subscribeActual(t var1) {
      Call var5 = this.originalCall.clone();
      var1.onSubscribe(new CallExecuteObservable.CallDisposable(var5));

      boolean var2;
      Throwable var4;
      label43: {
         boolean var3;
         try {
            Response var9 = var5.execute();
            if(!var5.isCanceled()) {
               var1.onNext(var9);
            }

            var3 = var5.isCanceled();
         } catch (Throwable var8) {
            var4 = var8;
            var2 = false;
            break label43;
         }

         if(var3) {
            return;
         }

         try {
            var1.onComplete();
            return;
         } catch (Throwable var7) {
            var4 = var7;
            var2 = true;
         }
      }

      a.b(var4);
      if(var2) {
         io.reactivex.g.a.a(var4);
      } else if(!var5.isCanceled()) {
         try {
            var1.onError(var4);
         } catch (Throwable var6) {
            a.b(var6);
            io.reactivex.g.a.a((Throwable)(new CompositeException(new Throwable[]{var4, var6})));
         }
      }

   }

   private static final class CallDisposable implements b {
      private final Call call;

      CallDisposable(Call var1) {
         this.call = var1;
      }

      public void dispose() {
         this.call.cancel();
      }

      public boolean isDisposed() {
         return this.call.isCanceled();
      }
   }
}
