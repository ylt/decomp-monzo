package retrofit2.adapter.rxjava2;

import javax.annotation.Nullable;
import retrofit2.Response;

public final class Result {
   @Nullable
   private final Throwable error;
   @Nullable
   private final Response response;

   private Result(@Nullable Response var1, @Nullable Throwable var2) {
      this.response = var1;
      this.error = var2;
   }

   public static Result error(Throwable var0) {
      if(var0 == null) {
         throw new NullPointerException("error == null");
      } else {
         return new Result((Response)null, var0);
      }
   }

   public static Result response(Response var0) {
      if(var0 == null) {
         throw new NullPointerException("response == null");
      } else {
         return new Result(var0, (Throwable)null);
      }
   }

   @Nullable
   public Throwable error() {
      return this.error;
   }

   public boolean isError() {
      boolean var1;
      if(this.error != null) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   @Nullable
   public Response response() {
      return this.response;
   }
}
