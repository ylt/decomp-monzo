package retrofit2.adapter.rxjava2;

import io.reactivex.n;
import io.reactivex.t;
import io.reactivex.b.b;
import io.reactivex.exceptions.CompositeException;
import io.reactivex.exceptions.a;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

final class CallEnqueueObservable extends n {
   private final Call originalCall;

   CallEnqueueObservable(Call var1) {
      this.originalCall = var1;
   }

   protected void subscribeActual(t var1) {
      Call var2 = this.originalCall.clone();
      CallEnqueueObservable.CallCallback var3 = new CallEnqueueObservable.CallCallback(var2, var1);
      var1.onSubscribe(var3);
      var2.enqueue(var3);
   }

   private static final class CallCallback implements b, Callback {
      private final Call call;
      private final t observer;
      boolean terminated = false;

      CallCallback(Call var1, t var2) {
         this.call = var1;
         this.observer = var2;
      }

      public void dispose() {
         this.call.cancel();
      }

      public boolean isDisposed() {
         return this.call.isCanceled();
      }

      public void onFailure(Call var1, Throwable var2) {
         if(!var1.isCanceled()) {
            try {
               this.observer.onError(var2);
            } catch (Throwable var3) {
               a.b(var3);
               io.reactivex.g.a.a((Throwable)(new CompositeException(new Throwable[]{var2, var3})));
            }
         }

      }

      public void onResponse(Call var1, Response var2) {
         if(!var1.isCanceled()) {
            try {
               this.observer.onNext(var2);
               if(!var1.isCanceled()) {
                  this.terminated = true;
                  this.observer.onComplete();
               }
            } catch (Throwable var4) {
               Throwable var5 = var4;
               if(this.terminated) {
                  io.reactivex.g.a.a(var4);
               } else if(!var1.isCanceled()) {
                  try {
                     this.observer.onError(var5);
                  } catch (Throwable var3) {
                     a.b(var3);
                     io.reactivex.g.a.a((Throwable)(new CompositeException(new Throwable[]{var4, var3})));
                  }
               }
            }
         }

      }
   }
}
