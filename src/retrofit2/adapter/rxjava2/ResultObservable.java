package retrofit2.adapter.rxjava2;

import io.reactivex.n;
import io.reactivex.t;
import io.reactivex.b.b;
import io.reactivex.exceptions.CompositeException;
import io.reactivex.exceptions.a;
import retrofit2.Response;

final class ResultObservable extends n {
   private final n upstream;

   ResultObservable(n var1) {
      this.upstream = var1;
   }

   protected void subscribeActual(t var1) {
      this.upstream.subscribe((t)(new ResultObservable.ResultObserver(var1)));
   }

   private static class ResultObserver implements t {
      private final t observer;

      ResultObserver(t var1) {
         this.observer = var1;
      }

      public void onComplete() {
         this.observer.onComplete();
      }

      public void onError(Throwable var1) {
         try {
            this.observer.onNext(Result.error(var1));
         } catch (Throwable var4) {
            var1 = var4;

            try {
               this.observer.onError(var1);
            } catch (Throwable var3) {
               a.b(var3);
               io.reactivex.g.a.a((Throwable)(new CompositeException(new Throwable[]{var4, var3})));
            }

            return;
         }

         this.observer.onComplete();
      }

      public void onNext(Response var1) {
         this.observer.onNext(Result.response(var1));
      }

      public void onSubscribe(b var1) {
         this.observer.onSubscribe(var1);
      }
   }
}
