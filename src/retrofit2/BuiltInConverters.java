package retrofit2;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.http.Streaming;

final class BuiltInConverters extends Converter.Factory {
   public Converter requestBodyConverter(Type var1, Annotation[] var2, Annotation[] var3, Retrofit var4) {
      BuiltInConverters.RequestBodyConverter var5;
      if(RequestBody.class.isAssignableFrom(Utils.getRawType(var1))) {
         var5 = BuiltInConverters.RequestBodyConverter.INSTANCE;
      } else {
         var5 = null;
      }

      return var5;
   }

   public Converter responseBodyConverter(Type var1, Annotation[] var2, Retrofit var3) {
      Object var4;
      if(var1 == ResponseBody.class) {
         if(Utils.isAnnotationPresent(var2, Streaming.class)) {
            var4 = BuiltInConverters.StreamingResponseBodyConverter.INSTANCE;
         } else {
            var4 = BuiltInConverters.BufferingResponseBodyConverter.INSTANCE;
         }
      } else if(var1 == Void.class) {
         var4 = BuiltInConverters.VoidResponseBodyConverter.INSTANCE;
      } else {
         var4 = null;
      }

      return (Converter)var4;
   }

   static final class BufferingResponseBodyConverter implements Converter {
      static final BuiltInConverters.BufferingResponseBodyConverter INSTANCE = new BuiltInConverters.BufferingResponseBodyConverter();

      public ResponseBody convert(ResponseBody var1) throws IOException {
         ResponseBody var2;
         try {
            var2 = Utils.buffer(var1);
         } finally {
            var1.close();
         }

         return var2;
      }
   }

   static final class RequestBodyConverter implements Converter {
      static final BuiltInConverters.RequestBodyConverter INSTANCE = new BuiltInConverters.RequestBodyConverter();

      public RequestBody convert(RequestBody var1) throws IOException {
         return var1;
      }
   }

   static final class StreamingResponseBodyConverter implements Converter {
      static final BuiltInConverters.StreamingResponseBodyConverter INSTANCE = new BuiltInConverters.StreamingResponseBodyConverter();

      public ResponseBody convert(ResponseBody var1) throws IOException {
         return var1;
      }
   }

   static final class ToStringConverter implements Converter {
      static final BuiltInConverters.ToStringConverter INSTANCE = new BuiltInConverters.ToStringConverter();

      public String convert(Object var1) {
         return var1.toString();
      }
   }

   static final class VoidResponseBodyConverter implements Converter {
      static final BuiltInConverters.VoidResponseBodyConverter INSTANCE = new BuiltInConverters.VoidResponseBodyConverter();

      public Void convert(ResponseBody var1) throws IOException {
         var1.close();
         return null;
      }
   }
}
