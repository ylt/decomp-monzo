package retrofit2;

import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import javax.annotation.Nullable;

public interface CallAdapter {
   Object adapt(Call var1);

   Type responseType();

   public abstract static class Factory {
      protected static Type getParameterUpperBound(int var0, ParameterizedType var1) {
         return Utils.getParameterUpperBound(var0, var1);
      }

      protected static Class getRawType(Type var0) {
         return Utils.getRawType(var0);
      }

      @Nullable
      public abstract CallAdapter get(Type var1, Annotation[] var2, Retrofit var3);
   }
}
