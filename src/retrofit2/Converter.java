package retrofit2;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import javax.annotation.Nullable;

public interface Converter {
   Object convert(Object var1) throws IOException;

   public abstract static class Factory {
      protected static Type getParameterUpperBound(int var0, ParameterizedType var1) {
         return Utils.getParameterUpperBound(var0, var1);
      }

      protected static Class getRawType(Type var0) {
         return Utils.getRawType(var0);
      }

      @Nullable
      public Converter requestBodyConverter(Type var1, Annotation[] var2, Annotation[] var3, Retrofit var4) {
         return null;
      }

      @Nullable
      public Converter responseBodyConverter(Type var1, Annotation[] var2, Retrofit var3) {
         return null;
      }

      @Nullable
      public Converter stringConverter(Type var1, Annotation[] var2, Retrofit var3) {
         return null;
      }
   }
}
