package retrofit2;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.concurrent.Executor;
import okhttp3.Request;

final class ExecutorCallAdapterFactory extends CallAdapter.Factory {
   final Executor callbackExecutor;

   ExecutorCallAdapterFactory(Executor var1) {
      this.callbackExecutor = var1;
   }

   public CallAdapter get(Type var1, Annotation[] var2, Retrofit var3) {
      CallAdapter var4;
      if(getRawType(var1) != Call.class) {
         var4 = null;
      } else {
         var4 = new CallAdapter(Utils.getCallResponseType(var1)) {
            // $FF: synthetic field
            final Type val$responseType;

            {
               this.val$responseType = var2;
            }

            public Call adapt(Call var1) {
               return new ExecutorCallAdapterFactory.ExecutorCallbackCall(ExecutorCallAdapterFactory.this.callbackExecutor, var1);
            }

            public Type responseType() {
               return this.val$responseType;
            }
         };
      }

      return var4;
   }

   static final class ExecutorCallbackCall implements Call {
      final Executor callbackExecutor;
      final Call delegate;

      ExecutorCallbackCall(Executor var1, Call var2) {
         this.callbackExecutor = var1;
         this.delegate = var2;
      }

      public void cancel() {
         this.delegate.cancel();
      }

      public Call clone() {
         return new ExecutorCallAdapterFactory.ExecutorCallbackCall(this.callbackExecutor, this.delegate.clone());
      }

      public void enqueue(final Callback var1) {
         Utils.checkNotNull(var1, "callback == null");
         this.delegate.enqueue(new Callback() {
            public void onFailure(Call var1x, final Throwable var2) {
               ExecutorCallbackCall.this.callbackExecutor.execute(new Runnable() {
                  public void run() {
                     var1.onFailure(ExecutorCallbackCall.this, var2);
                  }
               });
            }

            public void onResponse(Call var1x, final Response var2) {
               ExecutorCallbackCall.this.callbackExecutor.execute(new Runnable() {
                  public void run() {
                     if(ExecutorCallbackCall.this.delegate.isCanceled()) {
                        var1.onFailure(ExecutorCallbackCall.this, new IOException("Canceled"));
                     } else {
                        var1.onResponse(ExecutorCallbackCall.this, var2);
                     }

                  }
               });
            }
         });
      }

      public Response execute() throws IOException {
         return this.delegate.execute();
      }

      public boolean isCanceled() {
         return this.delegate.isCanceled();
      }

      public boolean isExecuted() {
         return this.delegate.isExecuted();
      }

      public Request request() {
         return this.delegate.request();
      }
   }
}
