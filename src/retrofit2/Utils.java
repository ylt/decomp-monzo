package retrofit2;

import c.c;
import c.s;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Array;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.GenericDeclaration;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.util.Arrays;
import java.util.NoSuchElementException;
import javax.annotation.Nullable;
import okhttp3.ResponseBody;

final class Utils {
   static final Type[] EMPTY_TYPE_ARRAY = new Type[0];

   static ResponseBody buffer(ResponseBody var0) throws IOException {
      c var1 = new c();
      var0.source().a((s)var1);
      return ResponseBody.create(var0.contentType(), var0.contentLength(), var1);
   }

   static Object checkNotNull(@Nullable Object var0, String var1) {
      if(var0 == null) {
         throw new NullPointerException(var1);
      } else {
         return var0;
      }
   }

   static void checkNotPrimitive(Type var0) {
      if(var0 instanceof Class && ((Class)var0).isPrimitive()) {
         throw new IllegalArgumentException();
      }
   }

   private static Class declaringClassOf(TypeVariable var0) {
      GenericDeclaration var1 = var0.getGenericDeclaration();
      Class var2;
      if(var1 instanceof Class) {
         var2 = (Class)var1;
      } else {
         var2 = null;
      }

      return var2;
   }

   private static boolean equal(Object var0, Object var1) {
      boolean var2;
      if(var0 != var1 && (var0 == null || !var0.equals(var1))) {
         var2 = false;
      } else {
         var2 = true;
      }

      return var2;
   }

   static boolean equals(Type var0, Type var1) {
      boolean var5 = true;
      boolean var6 = true;
      boolean var3 = true;
      boolean var4 = false;
      boolean var2;
      if(var0 == var1) {
         var2 = true;
      } else if(var0 instanceof Class) {
         var2 = var0.equals(var1);
      } else if(var0 instanceof ParameterizedType) {
         var2 = var4;
         if(var1 instanceof ParameterizedType) {
            ParameterizedType var7 = (ParameterizedType)var0;
            ParameterizedType var10 = (ParameterizedType)var1;
            if(equal(var7.getOwnerType(), var10.getOwnerType()) && var7.getRawType().equals(var10.getRawType()) && Arrays.equals(var7.getActualTypeArguments(), var10.getActualTypeArguments())) {
               var2 = var3;
            } else {
               var2 = false;
            }
         }
      } else if(var0 instanceof GenericArrayType) {
         var2 = var4;
         if(var1 instanceof GenericArrayType) {
            GenericArrayType var8 = (GenericArrayType)var0;
            GenericArrayType var12 = (GenericArrayType)var1;
            var2 = equals(var8.getGenericComponentType(), var12.getGenericComponentType());
         }
      } else if(var0 instanceof WildcardType) {
         var2 = var4;
         if(var1 instanceof WildcardType) {
            WildcardType var9 = (WildcardType)var0;
            WildcardType var13 = (WildcardType)var1;
            if(Arrays.equals(var9.getUpperBounds(), var13.getUpperBounds()) && Arrays.equals(var9.getLowerBounds(), var13.getLowerBounds())) {
               var2 = var5;
            } else {
               var2 = false;
            }
         }
      } else {
         var2 = var4;
         if(var0 instanceof TypeVariable) {
            var2 = var4;
            if(var1 instanceof TypeVariable) {
               TypeVariable var11 = (TypeVariable)var0;
               TypeVariable var14 = (TypeVariable)var1;
               if(var11.getGenericDeclaration() == var14.getGenericDeclaration() && var11.getName().equals(var14.getName())) {
                  var2 = var6;
               } else {
                  var2 = false;
               }
            }
         }
      }

      return var2;
   }

   static Type getCallResponseType(Type var0) {
      if(!(var0 instanceof ParameterizedType)) {
         throw new IllegalArgumentException("Call return type must be parameterized as Call<Foo> or Call<? extends Foo>");
      } else {
         return getParameterUpperBound(0, (ParameterizedType)var0);
      }
   }

   static Type getGenericSupertype(Type var0, Class var1, Class var2) {
      if(var2 != var1) {
         if(var2.isInterface()) {
            Class[] var5 = var1.getInterfaces();
            int var3 = 0;

            for(int var4 = var5.length; var3 < var4; ++var3) {
               if(var5[var3] == var2) {
                  var0 = var1.getGenericInterfaces()[var3];
                  return (Type)var0;
               }

               if(var2.isAssignableFrom(var5[var3])) {
                  var0 = getGenericSupertype(var1.getGenericInterfaces()[var3], var5[var3], var2);
                  return (Type)var0;
               }
            }
         }

         if(!var1.isInterface()) {
            while(var1 != Object.class) {
               Class var6 = var1.getSuperclass();
               if(var6 == var2) {
                  var0 = var1.getGenericSuperclass();
                  return (Type)var0;
               }

               if(var2.isAssignableFrom(var6)) {
                  var0 = getGenericSupertype(var1.getGenericSuperclass(), var6, var2);
                  return (Type)var0;
               }

               var1 = var6;
            }
         }

         var0 = var2;
      }

      return (Type)var0;
   }

   static Type getParameterUpperBound(int var0, ParameterizedType var1) {
      Type[] var2 = var1.getActualTypeArguments();
      if(var0 >= 0 && var0 < var2.length) {
         Type var4 = var2[var0];
         Type var3 = var4;
         if(var4 instanceof WildcardType) {
            var3 = ((WildcardType)var4).getUpperBounds()[0];
         }

         return var3;
      } else {
         throw new IllegalArgumentException("Index " + var0 + " not in range [0," + var2.length + ") for " + var1);
      }
   }

   static Class getRawType(Type var0) {
      checkNotNull(var0, "type == null");
      Class var1;
      if(var0 instanceof Class) {
         var1 = (Class)var0;
      } else if(var0 instanceof ParameterizedType) {
         var0 = ((ParameterizedType)var0).getRawType();
         if(!(var0 instanceof Class)) {
            throw new IllegalArgumentException();
         }

         var1 = (Class)var0;
      } else if(var0 instanceof GenericArrayType) {
         var1 = Array.newInstance(getRawType(((GenericArrayType)var0).getGenericComponentType()), 0).getClass();
      } else if(var0 instanceof TypeVariable) {
         var1 = Object.class;
      } else {
         if(!(var0 instanceof WildcardType)) {
            throw new IllegalArgumentException("Expected a Class, ParameterizedType, or GenericArrayType, but <" + var0 + "> is of type " + var0.getClass().getName());
         }

         var1 = getRawType(((WildcardType)var0).getUpperBounds()[0]);
      }

      return var1;
   }

   static Type getSupertype(Type var0, Class var1, Class var2) {
      if(!var2.isAssignableFrom(var1)) {
         throw new IllegalArgumentException();
      } else {
         return resolve(var0, var1, getGenericSupertype(var0, var1, var2));
      }
   }

   static boolean hasUnresolvableType(Type var0) {
      boolean var4 = false;
      boolean var3;
      if(var0 instanceof Class) {
         var3 = var4;
      } else if(var0 instanceof ParameterizedType) {
         Type[] var6 = ((ParameterizedType)var0).getActualTypeArguments();
         int var2 = var6.length;
         int var1 = 0;

         while(true) {
            var3 = var4;
            if(var1 >= var2) {
               break;
            }

            if(hasUnresolvableType(var6[var1])) {
               var3 = true;
               break;
            }

            ++var1;
         }
      } else if(var0 instanceof GenericArrayType) {
         var3 = hasUnresolvableType(((GenericArrayType)var0).getGenericComponentType());
      } else if(var0 instanceof TypeVariable) {
         var3 = true;
      } else {
         if(!(var0 instanceof WildcardType)) {
            String var5;
            if(var0 == null) {
               var5 = "null";
            } else {
               var5 = var0.getClass().getName();
            }

            throw new IllegalArgumentException("Expected a Class, ParameterizedType, or GenericArrayType, but <" + var0 + "> is of type " + var5);
         }

         var3 = true;
      }

      return var3;
   }

   static int hashCodeOrZero(Object var0) {
      int var1;
      if(var0 != null) {
         var1 = var0.hashCode();
      } else {
         var1 = 0;
      }

      return var1;
   }

   private static int indexOf(Object[] var0, Object var1) {
      for(int var2 = 0; var2 < var0.length; ++var2) {
         if(var1.equals(var0[var2])) {
            return var2;
         }
      }

      throw new NoSuchElementException();
   }

   static boolean isAnnotationPresent(Annotation[] var0, Class var1) {
      boolean var5 = false;
      int var3 = var0.length;
      int var2 = 0;

      boolean var4;
      while(true) {
         var4 = var5;
         if(var2 >= var3) {
            break;
         }

         if(var1.isInstance(var0[var2])) {
            var4 = true;
            break;
         }

         ++var2;
      }

      return var4;
   }

   static Type resolve(Type var0, Class var1, Type var2) {
      Object var7 = var2;

      Object var12;
      while(true) {
         if(!(var7 instanceof TypeVariable)) {
            if(var7 instanceof Class && ((Class)var7).isArray()) {
               var12 = (Class)var7;
               Class var19 = ((Class)var12).getComponentType();
               var0 = resolve(var0, var1, var19);
               if(var19 != var0) {
                  var12 = new Utils.GenericArrayTypeImpl(var0);
               }
               break;
            }

            if(var7 instanceof GenericArrayType) {
               var12 = (GenericArrayType)var7;
               Type var15 = ((GenericArrayType)var12).getGenericComponentType();
               var0 = resolve(var0, var1, var15);
               if(var15 != var0) {
                  var12 = new Utils.GenericArrayTypeImpl(var0);
               }
            } else if(var7 instanceof ParameterizedType) {
               ParameterizedType var8 = (ParameterizedType)var7;
               var2 = var8.getOwnerType();
               Type var9 = resolve(var0, var1, var2);
               boolean var3;
               if(var9 != var2) {
                  var3 = true;
               } else {
                  var3 = false;
               }

               Type[] var17 = var8.getActualTypeArguments();
               int var6 = var17.length;

               boolean var4;
               for(int var5 = 0; var5 < var6; var3 = var4) {
                  Type var10 = resolve(var0, var1, var17[var5]);
                  Type[] var13 = var17;
                  var4 = var3;
                  if(var10 != var17[var5]) {
                     var13 = var17;
                     var4 = var3;
                     if(!var3) {
                        var13 = (Type[])var17.clone();
                        var4 = true;
                     }

                     var13[var5] = var10;
                  }

                  ++var5;
                  var17 = var13;
               }

               var12 = var8;
               if(var3) {
                  var12 = new Utils.ParameterizedTypeImpl(var9, var8.getRawType(), var17);
               }
            } else {
               var12 = var7;
               if(var7 instanceof WildcardType) {
                  WildcardType var18 = (WildcardType)var7;
                  Type[] var16 = var18.getLowerBounds();
                  Type[] var20 = var18.getUpperBounds();
                  if(var16.length == 1) {
                     var0 = resolve(var0, var1, var16[0]);
                     var12 = var18;
                     if(var0 != var16[0]) {
                        var12 = new Utils.WildcardTypeImpl(new Type[]{Object.class}, new Type[]{var0});
                     }
                  } else {
                     var12 = var18;
                     if(var20.length == 1) {
                        var0 = resolve(var0, var1, var20[0]);
                        var12 = var18;
                        if(var0 != var20[0]) {
                           Type[] var11 = EMPTY_TYPE_ARRAY;
                           var12 = new Utils.WildcardTypeImpl(new Type[]{var0}, var11);
                        }
                     }
                  }
               }
            }
            break;
         }

         TypeVariable var14 = (TypeVariable)var7;
         var12 = resolveTypeVariable(var0, var1, var14);
         if(var12 == var14) {
            break;
         }

         var7 = var12;
      }

      return (Type)var12;
   }

   private static Type resolveTypeVariable(Type var0, Class var1, TypeVariable var2) {
      Class var4 = declaringClassOf(var2);
      Object var5;
      if(var4 == null) {
         var5 = var2;
      } else {
         Type var6 = getGenericSupertype(var0, var1, var4);
         var5 = var2;
         if(var6 instanceof ParameterizedType) {
            int var3 = indexOf(var4.getTypeParameters(), var2);
            var5 = ((ParameterizedType)var6).getActualTypeArguments()[var3];
         }
      }

      return (Type)var5;
   }

   static String typeToString(Type var0) {
      String var1;
      if(var0 instanceof Class) {
         var1 = ((Class)var0).getName();
      } else {
         var1 = var0.toString();
      }

      return var1;
   }

   static void validateServiceInterface(Class var0) {
      if(!var0.isInterface()) {
         throw new IllegalArgumentException("API declarations must be interfaces.");
      } else if(var0.getInterfaces().length > 0) {
         throw new IllegalArgumentException("API interfaces must not extend other interfaces.");
      }
   }

   private static final class GenericArrayTypeImpl implements GenericArrayType {
      private final Type componentType;

      GenericArrayTypeImpl(Type var1) {
         this.componentType = var1;
      }

      public boolean equals(Object var1) {
         boolean var2;
         if(var1 instanceof GenericArrayType && Utils.equals(this, (GenericArrayType)var1)) {
            var2 = true;
         } else {
            var2 = false;
         }

         return var2;
      }

      public Type getGenericComponentType() {
         return this.componentType;
      }

      public int hashCode() {
         return this.componentType.hashCode();
      }

      public String toString() {
         return Utils.typeToString(this.componentType) + "[]";
      }
   }

   private static final class ParameterizedTypeImpl implements ParameterizedType {
      private final Type ownerType;
      private final Type rawType;
      private final Type[] typeArguments;

      ParameterizedTypeImpl(Type var1, Type var2, Type... var3) {
         boolean var5 = true;
         byte var6 = 0;
         super();
         if(var2 instanceof Class) {
            boolean var4;
            if(var1 == null) {
               var4 = true;
            } else {
               var4 = false;
            }

            if(((Class)var2).getEnclosingClass() != null) {
               var5 = false;
            }

            if(var4 != var5) {
               throw new IllegalArgumentException();
            }
         }

         int var9 = var3.length;

         for(int var8 = var6; var8 < var9; ++var8) {
            Type var7 = var3[var8];
            Utils.checkNotNull(var7, "typeArgument == null");
            Utils.checkNotPrimitive(var7);
         }

         this.ownerType = var1;
         this.rawType = var2;
         this.typeArguments = (Type[])var3.clone();
      }

      public boolean equals(Object var1) {
         boolean var2;
         if(var1 instanceof ParameterizedType && Utils.equals(this, (ParameterizedType)var1)) {
            var2 = true;
         } else {
            var2 = false;
         }

         return var2;
      }

      public Type[] getActualTypeArguments() {
         return (Type[])this.typeArguments.clone();
      }

      public Type getOwnerType() {
         return this.ownerType;
      }

      public Type getRawType() {
         return this.rawType;
      }

      public int hashCode() {
         return Arrays.hashCode(this.typeArguments) ^ this.rawType.hashCode() ^ Utils.hashCodeOrZero(this.ownerType);
      }

      public String toString() {
         String var2;
         if(this.typeArguments.length == 0) {
            var2 = Utils.typeToString(this.rawType);
         } else {
            StringBuilder var3 = new StringBuilder((this.typeArguments.length + 1) * 30);
            var3.append(Utils.typeToString(this.rawType));
            var3.append("<").append(Utils.typeToString(this.typeArguments[0]));

            for(int var1 = 1; var1 < this.typeArguments.length; ++var1) {
               var3.append(", ").append(Utils.typeToString(this.typeArguments[var1]));
            }

            var2 = var3.append(">").toString();
         }

         return var2;
      }
   }

   private static final class WildcardTypeImpl implements WildcardType {
      private final Type lowerBound;
      private final Type upperBound;

      WildcardTypeImpl(Type[] var1, Type[] var2) {
         if(var2.length > 1) {
            throw new IllegalArgumentException();
         } else if(var1.length != 1) {
            throw new IllegalArgumentException();
         } else {
            if(var2.length == 1) {
               if(var2[0] == null) {
                  throw new NullPointerException();
               }

               Utils.checkNotPrimitive(var2[0]);
               if(var1[0] != Object.class) {
                  throw new IllegalArgumentException();
               }

               this.lowerBound = var2[0];
               this.upperBound = Object.class;
            } else {
               if(var1[0] == null) {
                  throw new NullPointerException();
               }

               Utils.checkNotPrimitive(var1[0]);
               this.lowerBound = null;
               this.upperBound = var1[0];
            }

         }
      }

      public boolean equals(Object var1) {
         boolean var2;
         if(var1 instanceof WildcardType && Utils.equals(this, (WildcardType)var1)) {
            var2 = true;
         } else {
            var2 = false;
         }

         return var2;
      }

      public Type[] getLowerBounds() {
         Type[] var1;
         if(this.lowerBound != null) {
            var1 = new Type[]{this.lowerBound};
         } else {
            var1 = Utils.EMPTY_TYPE_ARRAY;
         }

         return var1;
      }

      public Type[] getUpperBounds() {
         return new Type[]{this.upperBound};
      }

      public int hashCode() {
         int var1;
         if(this.lowerBound != null) {
            var1 = this.lowerBound.hashCode() + 31;
         } else {
            var1 = 1;
         }

         return var1 ^ this.upperBound.hashCode() + 31;
      }

      public String toString() {
         String var1;
         if(this.lowerBound != null) {
            var1 = "? super " + Utils.typeToString(this.lowerBound);
         } else if(this.upperBound == Object.class) {
            var1 = "?";
         } else {
            var1 = "? extends " + Utils.typeToString(this.upperBound);
         }

         return var1;
      }
   }
}
