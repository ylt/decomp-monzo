package retrofit2;

import java.io.IOException;
import okhttp3.Request;

public interface Call extends Cloneable {
   void cancel();

   Call clone();

   void enqueue(Callback var1);

   Response execute() throws IOException;

   boolean isCanceled();

   boolean isExecuted();

   Request request();
}
