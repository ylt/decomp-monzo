package retrofit2;

import c.c;
import c.d;
import java.io.IOException;
import javax.annotation.Nullable;
import okhttp3.FormBody;
import okhttp3.Headers;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.Request;
import okhttp3.RequestBody;

final class RequestBuilder {
   private static final char[] HEX_DIGITS = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
   private static final String PATH_SEGMENT_ALWAYS_ENCODE_SET = " \"<>^`{}|\\?#";
   private final HttpUrl baseUrl;
   @Nullable
   private RequestBody body;
   @Nullable
   private MediaType contentType;
   @Nullable
   private FormBody.Builder formBuilder;
   private final boolean hasBody;
   private final String method;
   @Nullable
   private MultipartBody.Builder multipartBuilder;
   @Nullable
   private String relativeUrl;
   private final Request.Builder requestBuilder;
   @Nullable
   private HttpUrl.Builder urlBuilder;

   RequestBuilder(String var1, HttpUrl var2, @Nullable String var3, @Nullable Headers var4, @Nullable MediaType var5, boolean var6, boolean var7, boolean var8) {
      this.method = var1;
      this.baseUrl = var2;
      this.relativeUrl = var3;
      this.requestBuilder = new Request.Builder();
      this.contentType = var5;
      this.hasBody = var6;
      if(var4 != null) {
         this.requestBuilder.headers(var4);
      }

      if(var7) {
         this.formBuilder = new FormBody.Builder();
      } else if(var8) {
         this.multipartBuilder = new MultipartBody.Builder();
         this.multipartBuilder.setType(MultipartBody.FORM);
      }

   }

   private static String canonicalizeForPath(String var0, boolean var1) {
      int var3 = var0.length();
      int var2 = 0;

      String var5;
      while(true) {
         var5 = var0;
         if(var2 >= var3) {
            break;
         }

         int var4 = var0.codePointAt(var2);
         if(var4 < 32 || var4 >= 127 || " \"<>^`{}|\\?#".indexOf(var4) != -1 || !var1 && (var4 == 47 || var4 == 37)) {
            c var6 = new c();
            var6.a((String)var0, 0, var2);
            canonicalizeForPath(var6, var0, var2, var3, var1);
            var5 = var6.r();
            break;
         }

         var2 += Character.charCount(var4);
      }

      return var5;
   }

   private static void canonicalizeForPath(c var0, String var1, int var2, int var3, boolean var4) {
      c var9;
      for(c var7 = null; var2 < var3; var7 = var9) {
         int var6;
         label67: {
            var6 = var1.codePointAt(var2);
            if(var4) {
               var9 = var7;
               if(var6 == 9) {
                  break label67;
               }

               var9 = var7;
               if(var6 == 10) {
                  break label67;
               }

               var9 = var7;
               if(var6 == 12) {
                  break label67;
               }

               if(var6 == 13) {
                  var9 = var7;
                  break label67;
               }
            }

            if(var6 >= 32 && var6 < 127 && " \"<>^`{}|\\?#".indexOf(var6) == -1 && (var4 || var6 != 47 && var6 != 37)) {
               var0.a(var6);
               var9 = var7;
            } else {
               c var8 = var7;
               if(var7 == null) {
                  var8 = new c();
               }

               var8.a(var6);

               while(true) {
                  var9 = var8;
                  if(var8.f()) {
                     break;
                  }

                  int var5 = var8.i() & 255;
                  var0.b(37);
                  var0.b(HEX_DIGITS[var5 >> 4 & 15]);
                  var0.b(HEX_DIGITS[var5 & 15]);
               }
            }
         }

         var2 += Character.charCount(var6);
      }

   }

   void addFormField(String var1, String var2, boolean var3) {
      if(var3) {
         this.formBuilder.addEncoded(var1, var2);
      } else {
         this.formBuilder.add(var1, var2);
      }

   }

   void addHeader(String var1, String var2) {
      if("Content-Type".equalsIgnoreCase(var1)) {
         MediaType var3 = MediaType.parse(var2);
         if(var3 == null) {
            throw new IllegalArgumentException("Malformed content type: " + var2);
         }

         this.contentType = var3;
      } else {
         this.requestBuilder.addHeader(var1, var2);
      }

   }

   void addPart(Headers var1, RequestBody var2) {
      this.multipartBuilder.addPart(var1, var2);
   }

   void addPart(MultipartBody.Part var1) {
      this.multipartBuilder.addPart(var1);
   }

   void addPathParam(String var1, String var2, boolean var3) {
      if(this.relativeUrl == null) {
         throw new AssertionError();
      } else {
         this.relativeUrl = this.relativeUrl.replace("{" + var1 + "}", canonicalizeForPath(var2, var3));
      }
   }

   void addQueryParam(String var1, @Nullable String var2, boolean var3) {
      if(this.relativeUrl != null) {
         this.urlBuilder = this.baseUrl.newBuilder(this.relativeUrl);
         if(this.urlBuilder == null) {
            throw new IllegalArgumentException("Malformed URL. Base: " + this.baseUrl + ", Relative: " + this.relativeUrl);
         }

         this.relativeUrl = null;
      }

      if(var3) {
         this.urlBuilder.addEncodedQueryParameter(var1, var2);
      } else {
         this.urlBuilder.addQueryParameter(var1, var2);
      }

   }

   Request build() {
      HttpUrl.Builder var1 = this.urlBuilder;
      HttpUrl var2;
      if(var1 != null) {
         var2 = var1.build();
      } else {
         HttpUrl var5 = this.baseUrl.resolve(this.relativeUrl);
         var2 = var5;
         if(var5 == null) {
            throw new IllegalArgumentException("Malformed URL. Base: " + this.baseUrl + ", Relative: " + this.relativeUrl);
         }
      }

      RequestBody var3 = this.body;
      Object var6 = var3;
      if(var3 == null) {
         if(this.formBuilder != null) {
            var6 = this.formBuilder.build();
         } else if(this.multipartBuilder != null) {
            var6 = this.multipartBuilder.build();
         } else {
            var6 = var3;
            if(this.hasBody) {
               var6 = RequestBody.create((MediaType)null, (byte[])(new byte[0]));
            }
         }
      }

      MediaType var4 = this.contentType;
      Object var7 = var6;
      if(var4 != null) {
         if(var6 != null) {
            var7 = new RequestBuilder.ContentTypeOverridingRequestBody((RequestBody)var6, var4);
         } else {
            this.requestBuilder.addHeader("Content-Type", var4.toString());
            var7 = var6;
         }
      }

      return this.requestBuilder.url(var2).method(this.method, (RequestBody)var7).build();
   }

   void setBody(RequestBody var1) {
      this.body = var1;
   }

   void setRelativeUrl(Object var1) {
      this.relativeUrl = var1.toString();
   }

   private static class ContentTypeOverridingRequestBody extends RequestBody {
      private final MediaType contentType;
      private final RequestBody delegate;

      ContentTypeOverridingRequestBody(RequestBody var1, MediaType var2) {
         this.delegate = var1;
         this.contentType = var2;
      }

      public long contentLength() throws IOException {
         return this.delegate.contentLength();
      }

      public MediaType contentType() {
         return this.contentType;
      }

      public void writeTo(d var1) throws IOException {
         this.delegate.writeTo(var1);
      }
   }
}
