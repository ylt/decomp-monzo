package retrofit2.converter.moshi;

import com.squareup.moshi.i;
import com.squareup.moshi.j;
import com.squareup.moshi.v;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;
import retrofit2.Converter;
import retrofit2.Retrofit;

public final class MoshiConverterFactory extends Converter.Factory {
   private final boolean failOnUnknown;
   private final boolean lenient;
   private final v moshi;
   private final boolean serializeNulls;

   private MoshiConverterFactory(v var1, boolean var2, boolean var3, boolean var4) {
      this.moshi = var1;
      this.lenient = var2;
      this.failOnUnknown = var3;
      this.serializeNulls = var4;
   }

   public static MoshiConverterFactory create() {
      return create((new v.a()).a());
   }

   public static MoshiConverterFactory create(v var0) {
      if(var0 == null) {
         throw new NullPointerException("moshi == null");
      } else {
         return new MoshiConverterFactory(var0, false, false, false);
      }
   }

   private static Set jsonAnnotations(Annotation[] var0) {
      int var2 = var0.length;
      LinkedHashSet var3 = null;

      LinkedHashSet var4;
      for(int var1 = 0; var1 < var2; var3 = var4) {
         Annotation var5 = var0[var1];
         var4 = var3;
         if(var5.annotationType().isAnnotationPresent(j.class)) {
            var4 = var3;
            if(var3 == null) {
               var4 = new LinkedHashSet();
            }

            var4.add(var5);
         }

         ++var1;
      }

      Set var6;
      if(var3 != null) {
         var6 = Collections.unmodifiableSet(var3);
      } else {
         var6 = Collections.emptySet();
      }

      return var6;
   }

   public MoshiConverterFactory asLenient() {
      return new MoshiConverterFactory(this.moshi, true, this.failOnUnknown, this.serializeNulls);
   }

   public MoshiConverterFactory failOnUnknown() {
      return new MoshiConverterFactory(this.moshi, this.lenient, true, this.serializeNulls);
   }

   public Converter requestBodyConverter(Type var1, Annotation[] var2, Annotation[] var3, Retrofit var4) {
      i var5 = this.moshi.a(var1, jsonAnnotations(var2));
      i var6 = var5;
      if(this.lenient) {
         var6 = var5.e();
      }

      var5 = var6;
      if(this.failOnUnknown) {
         var5 = var6.f();
      }

      var6 = var5;
      if(this.serializeNulls) {
         var6 = var5.c();
      }

      return new MoshiRequestBodyConverter(var6);
   }

   public Converter responseBodyConverter(Type var1, Annotation[] var2, Retrofit var3) {
      i var4 = this.moshi.a(var1, jsonAnnotations(var2));
      i var5 = var4;
      if(this.lenient) {
         var5 = var4.e();
      }

      var4 = var5;
      if(this.failOnUnknown) {
         var4 = var5.f();
      }

      var5 = var4;
      if(this.serializeNulls) {
         var5 = var4.c();
      }

      return new MoshiResponseBodyConverter(var5);
   }

   public MoshiConverterFactory withNullSerialization() {
      return new MoshiConverterFactory(this.moshi, this.lenient, this.failOnUnknown, true);
   }
}
