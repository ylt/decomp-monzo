package retrofit2.converter.moshi;

import c.c;
import c.d;
import com.squareup.moshi.i;
import com.squareup.moshi.p;
import java.io.IOException;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Converter;

final class MoshiRequestBodyConverter implements Converter {
   private static final MediaType MEDIA_TYPE = MediaType.parse("application/json; charset=UTF-8");
   private final i adapter;

   MoshiRequestBodyConverter(i var1) {
      this.adapter = var1;
   }

   public RequestBody convert(Object var1) throws IOException {
      c var3 = new c();
      p var2 = p.a((d)var3);
      this.adapter.a(var2, var1);
      return RequestBody.create(MEDIA_TYPE, var3.q());
   }
}
