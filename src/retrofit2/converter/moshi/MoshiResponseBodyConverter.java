package retrofit2.converter.moshi;

import c.e;
import c.f;
import com.squareup.moshi.i;
import java.io.IOException;
import okhttp3.ResponseBody;
import retrofit2.Converter;

final class MoshiResponseBodyConverter implements Converter {
   private static final f UTF8_BOM = f.c("EFBBBF");
   private final i adapter;

   MoshiResponseBodyConverter(i var1) {
      this.adapter = var1;
   }

   public Object convert(ResponseBody var1) throws IOException {
      e var2 = var1.source();

      Object var5;
      try {
         if(var2.a(0L, UTF8_BOM)) {
            var2.i((long)UTF8_BOM.h());
         }

         var5 = this.adapter.a(var2);
      } finally {
         var1.close();
      }

      return var5;
   }
}
