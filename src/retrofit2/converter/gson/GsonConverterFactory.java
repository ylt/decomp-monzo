package retrofit2.converter.gson;

import com.google.gson.f;
import com.google.gson.s;
import com.google.gson.c.a;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import retrofit2.Converter;
import retrofit2.Retrofit;

public final class GsonConverterFactory extends Converter.Factory {
   private final f gson;

   private GsonConverterFactory(f var1) {
      this.gson = var1;
   }

   public static GsonConverterFactory create() {
      return create(new f());
   }

   public static GsonConverterFactory create(f var0) {
      if(var0 == null) {
         throw new NullPointerException("gson == null");
      } else {
         return new GsonConverterFactory(var0);
      }
   }

   public Converter requestBodyConverter(Type var1, Annotation[] var2, Annotation[] var3, Retrofit var4) {
      s var5 = this.gson.a(a.a(var1));
      return new GsonRequestBodyConverter(this.gson, var5);
   }

   public Converter responseBodyConverter(Type var1, Annotation[] var2, Retrofit var3) {
      s var4 = this.gson.a(a.a(var1));
      return new GsonResponseBodyConverter(this.gson, var4);
   }
}
