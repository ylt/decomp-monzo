package retrofit2.converter.gson;

import c.c;
import com.google.gson.f;
import com.google.gson.s;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Converter;

final class GsonRequestBodyConverter implements Converter {
   private static final MediaType MEDIA_TYPE = MediaType.parse("application/json; charset=UTF-8");
   private static final Charset UTF_8 = Charset.forName("UTF-8");
   private final s adapter;
   private final f gson;

   GsonRequestBodyConverter(f var1, s var2) {
      this.gson = var1;
      this.adapter = var2;
   }

   public RequestBody convert(Object var1) throws IOException {
      c var2 = new c();
      OutputStreamWriter var3 = new OutputStreamWriter(var2.c(), UTF_8);
      JsonWriter var4 = this.gson.a(var3);
      this.adapter.a(var4, var1);
      var4.close();
      return RequestBody.create(MEDIA_TYPE, var2.q());
   }
}
