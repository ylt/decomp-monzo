package retrofit2.converter.gson;

import com.google.gson.f;
import com.google.gson.s;
import com.google.gson.stream.JsonReader;
import java.io.IOException;
import okhttp3.ResponseBody;
import retrofit2.Converter;

final class GsonResponseBodyConverter implements Converter {
   private final s adapter;
   private final f gson;

   GsonResponseBodyConverter(f var1, s var2) {
      this.gson = var1;
      this.adapter = var2;
   }

   public Object convert(ResponseBody var1) throws IOException {
      JsonReader var2 = this.gson.a(var1.charStream());

      Object var5;
      try {
         var5 = this.adapter.a(var2);
      } finally {
         var1.close();
      }

      return var5;
   }
}
