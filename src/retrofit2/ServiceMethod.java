package retrofit2;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.net.URI;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.Nullable;
import okhttp3.Headers;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.Request;
import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HEAD;
import retrofit2.http.HTTP;
import retrofit2.http.Header;
import retrofit2.http.HeaderMap;
import retrofit2.http.Multipart;
import retrofit2.http.OPTIONS;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import retrofit2.http.QueryName;
import retrofit2.http.Url;

final class ServiceMethod {
   static final String PARAM = "[a-zA-Z][a-zA-Z0-9_-]*";
   static final Pattern PARAM_NAME_REGEX = Pattern.compile("[a-zA-Z][a-zA-Z0-9_-]*");
   static final Pattern PARAM_URL_REGEX = Pattern.compile("\\{([a-zA-Z][a-zA-Z0-9_-]*)\\}");
   private final HttpUrl baseUrl;
   final CallAdapter callAdapter;
   final okhttp3.Call.Factory callFactory;
   private final MediaType contentType;
   private final boolean hasBody;
   private final Headers headers;
   private final String httpMethod;
   private final boolean isFormEncoded;
   private final boolean isMultipart;
   private final ParameterHandler[] parameterHandlers;
   private final String relativeUrl;
   private final Converter responseConverter;

   ServiceMethod(ServiceMethod.Builder var1) {
      this.callFactory = var1.retrofit.callFactory();
      this.callAdapter = var1.callAdapter;
      this.baseUrl = var1.retrofit.baseUrl();
      this.responseConverter = var1.responseConverter;
      this.httpMethod = var1.httpMethod;
      this.relativeUrl = var1.relativeUrl;
      this.headers = var1.headers;
      this.contentType = var1.contentType;
      this.hasBody = var1.hasBody;
      this.isFormEncoded = var1.isFormEncoded;
      this.isMultipart = var1.isMultipart;
      this.parameterHandlers = var1.parameterHandlers;
   }

   static Class boxIfPrimitive(Class var0) {
      Class var1;
      if(Boolean.TYPE == var0) {
         var1 = Boolean.class;
      } else if(Byte.TYPE == var0) {
         var1 = Byte.class;
      } else if(Character.TYPE == var0) {
         var1 = Character.class;
      } else if(Double.TYPE == var0) {
         var1 = Double.class;
      } else if(Float.TYPE == var0) {
         var1 = Float.class;
      } else if(Integer.TYPE == var0) {
         var1 = Integer.class;
      } else if(Long.TYPE == var0) {
         var1 = Long.class;
      } else {
         var1 = var0;
         if(Short.TYPE == var0) {
            var1 = Short.class;
         }
      }

      return var1;
   }

   static Set parsePathParameters(String var0) {
      Matcher var1 = PARAM_URL_REGEX.matcher(var0);
      LinkedHashSet var2 = new LinkedHashSet();

      while(var1.find()) {
         var2.add(var1.group(1));
      }

      return var2;
   }

   Request toRequest(@Nullable Object... var1) throws IOException {
      int var3 = 0;
      RequestBuilder var5 = new RequestBuilder(this.httpMethod, this.baseUrl, this.relativeUrl, this.headers, this.contentType, this.hasBody, this.isFormEncoded, this.isMultipart);
      ParameterHandler[] var4 = this.parameterHandlers;
      int var2;
      if(var1 != null) {
         var2 = var1.length;
      } else {
         var2 = 0;
      }

      if(var2 != var4.length) {
         throw new IllegalArgumentException("Argument count (" + var2 + ") doesn't match expected count (" + var4.length + ")");
      } else {
         while(var3 < var2) {
            var4[var3].apply(var5, var1[var3]);
            ++var3;
         }

         return var5.build();
      }
   }

   Object toResponse(ResponseBody var1) throws IOException {
      return this.responseConverter.convert(var1);
   }

   static final class Builder {
      CallAdapter callAdapter;
      MediaType contentType;
      boolean gotBody;
      boolean gotField;
      boolean gotPart;
      boolean gotPath;
      boolean gotQuery;
      boolean gotUrl;
      boolean hasBody;
      Headers headers;
      String httpMethod;
      boolean isFormEncoded;
      boolean isMultipart;
      final Method method;
      final Annotation[] methodAnnotations;
      final Annotation[][] parameterAnnotationsArray;
      ParameterHandler[] parameterHandlers;
      final Type[] parameterTypes;
      String relativeUrl;
      Set relativeUrlParamNames;
      Converter responseConverter;
      Type responseType;
      final Retrofit retrofit;

      Builder(Retrofit var1, Method var2) {
         this.retrofit = var1;
         this.method = var2;
         this.methodAnnotations = var2.getAnnotations();
         this.parameterTypes = var2.getGenericParameterTypes();
         this.parameterAnnotationsArray = var2.getParameterAnnotations();
      }

      private CallAdapter createCallAdapter() {
         Type var1 = this.method.getGenericReturnType();
         if(Utils.hasUnresolvableType(var1)) {
            throw this.methodError("Method return type must not include a type variable or wildcard: %s", new Object[]{var1});
         } else if(var1 == Void.TYPE) {
            throw this.methodError("Service methods cannot return void.", new Object[0]);
         } else {
            Annotation[] var2 = this.method.getAnnotations();

            try {
               CallAdapter var4 = this.retrofit.callAdapter(var1, var2);
               return var4;
            } catch (RuntimeException var3) {
               throw this.methodError(var3, "Unable to create call adapter for %s", new Object[]{var1});
            }
         }
      }

      private Converter createResponseConverter() {
         Annotation[] var1 = this.method.getAnnotations();

         try {
            Converter var3 = this.retrofit.responseBodyConverter(this.responseType, var1);
            return var3;
         } catch (RuntimeException var2) {
            throw this.methodError(var2, "Unable to create converter for %s", new Object[]{this.responseType});
         }
      }

      private RuntimeException methodError(String var1, Object... var2) {
         return this.methodError((Throwable)null, var1, var2);
      }

      private RuntimeException methodError(Throwable var1, String var2, Object... var3) {
         var2 = String.format(var2, var3);
         return new IllegalArgumentException(var2 + "\n    for method " + this.method.getDeclaringClass().getSimpleName() + "." + this.method.getName(), var1);
      }

      private RuntimeException parameterError(int var1, String var2, Object... var3) {
         return this.methodError(var2 + " (parameter #" + (var1 + 1) + ")", var3);
      }

      private RuntimeException parameterError(Throwable var1, int var2, String var3, Object... var4) {
         return this.methodError(var1, var3 + " (parameter #" + (var2 + 1) + ")", var4);
      }

      private Headers parseHeaders(String[] var1) {
         Headers.Builder var5 = new Headers.Builder();
         int var3 = var1.length;

         for(int var2 = 0; var2 < var3; ++var2) {
            String var7 = var1[var2];
            int var4 = var7.indexOf(58);
            if(var4 == -1 || var4 == 0 || var4 == var7.length() - 1) {
               throw this.methodError("@Headers value must be in the form \"Name: Value\". Found: \"%s\"", new Object[]{var7});
            }

            String var6 = var7.substring(0, var4);
            var7 = var7.substring(var4 + 1).trim();
            if("Content-Type".equalsIgnoreCase(var6)) {
               MediaType var8 = MediaType.parse(var7);
               if(var8 == null) {
                  throw this.methodError("Malformed content type: %s", new Object[]{var7});
               }

               this.contentType = var8;
            } else {
               var5.add(var6, var7);
            }
         }

         return var5.build();
      }

      private void parseHttpMethodAndPath(String var1, String var2, boolean var3) {
         if(this.httpMethod != null) {
            throw this.methodError("Only one HTTP method is allowed. Found: %s and %s.", new Object[]{this.httpMethod, var1});
         } else {
            this.httpMethod = var1;
            this.hasBody = var3;
            if(!var2.isEmpty()) {
               int var4 = var2.indexOf(63);
               if(var4 != -1 && var4 < var2.length() - 1) {
                  var1 = var2.substring(var4 + 1);
                  if(ServiceMethod.PARAM_URL_REGEX.matcher(var1).find()) {
                     throw this.methodError("URL query string \"%s\" must not have replace block. For dynamic query parameters use @Query.", new Object[]{var1});
                  }
               }

               this.relativeUrl = var2;
               this.relativeUrlParamNames = ServiceMethod.parsePathParameters(var2);
            }

         }
      }

      private void parseMethodAnnotation(Annotation var1) {
         if(var1 instanceof DELETE) {
            this.parseHttpMethodAndPath("DELETE", ((DELETE)var1).value(), false);
         } else if(var1 instanceof GET) {
            this.parseHttpMethodAndPath("GET", ((GET)var1).value(), false);
         } else if(var1 instanceof HEAD) {
            this.parseHttpMethodAndPath("HEAD", ((HEAD)var1).value(), false);
            if(!Void.class.equals(this.responseType)) {
               throw this.methodError("HEAD method must use Void as response type.", new Object[0]);
            }
         } else if(var1 instanceof PATCH) {
            this.parseHttpMethodAndPath("PATCH", ((PATCH)var1).value(), true);
         } else if(var1 instanceof POST) {
            this.parseHttpMethodAndPath("POST", ((POST)var1).value(), true);
         } else if(var1 instanceof PUT) {
            this.parseHttpMethodAndPath("PUT", ((PUT)var1).value(), true);
         } else if(var1 instanceof OPTIONS) {
            this.parseHttpMethodAndPath("OPTIONS", ((OPTIONS)var1).value(), false);
         } else if(var1 instanceof HTTP) {
            HTTP var2 = (HTTP)var1;
            this.parseHttpMethodAndPath(var2.method(), var2.path(), var2.hasBody());
         } else if(var1 instanceof retrofit2.http.Headers) {
            String[] var3 = ((retrofit2.http.Headers)var1).value();
            if(var3.length == 0) {
               throw this.methodError("@Headers annotation is empty.", new Object[0]);
            }

            this.headers = this.parseHeaders(var3);
         } else if(var1 instanceof Multipart) {
            if(this.isFormEncoded) {
               throw this.methodError("Only one encoding annotation is allowed.", new Object[0]);
            }

            this.isMultipart = true;
         } else if(var1 instanceof FormUrlEncoded) {
            if(this.isMultipart) {
               throw this.methodError("Only one encoding annotation is allowed.", new Object[0]);
            }

            this.isFormEncoded = true;
         }

      }

      private ParameterHandler parseParameter(int var1, Type var2, Annotation[] var3) {
         ParameterHandler var6 = null;
         int var5 = var3.length;

         for(int var4 = 0; var4 < var5; ++var4) {
            ParameterHandler var7 = this.parseParameterAnnotation(var1, var2, var3, var3[var4]);
            if(var7 != null) {
               if(var6 != null) {
                  throw this.parameterError(var1, "Multiple Retrofit annotations found, only one allowed.", new Object[0]);
               }

               var6 = var7;
            }
         }

         if(var6 == null) {
            throw this.parameterError(var1, "No Retrofit annotation found.", new Object[0]);
         } else {
            return var6;
         }
      }

      private ParameterHandler parseParameterAnnotation(int var1, Type var2, Annotation[] var3, Annotation var4) {
         Object var9;
         if(!(var4 instanceof Url)) {
            String var14;
            if(var4 instanceof Path) {
               if(this.gotQuery) {
                  throw this.parameterError(var1, "A @Path parameter must not come after a @Query.", new Object[0]);
               }

               if(this.gotUrl) {
                  throw this.parameterError(var1, "@Path parameters may not be used with @Url.", new Object[0]);
               }

               if(this.relativeUrl == null) {
                  throw this.parameterError(var1, "@Path can only be used with relative url on @%s", new Object[]{this.httpMethod});
               }

               this.gotPath = true;
               Path var23 = (Path)var4;
               var14 = var23.value();
               this.validatePathName(var1, var14);
               var9 = new ParameterHandler.Path(var14, this.retrofit.stringConverter(var2, var3), var23.encoded());
            } else {
               boolean var5;
               Class var6;
               Class var17;
               if(var4 instanceof Query) {
                  Query var22 = (Query)var4;
                  var14 = var22.value();
                  var5 = var22.encoded();
                  var6 = Utils.getRawType(var2);
                  this.gotQuery = true;
                  if(Iterable.class.isAssignableFrom(var6)) {
                     if(!(var2 instanceof ParameterizedType)) {
                        throw this.parameterError(var1, var6.getSimpleName() + " must include generic type (e.g., " + var6.getSimpleName() + "<String>)", new Object[0]);
                     }

                     var2 = Utils.getParameterUpperBound(0, (ParameterizedType)var2);
                     var9 = (new ParameterHandler.Query(var14, this.retrofit.stringConverter(var2, var3), var5)).iterable();
                  } else if(var6.isArray()) {
                     var17 = ServiceMethod.boxIfPrimitive(var6.getComponentType());
                     var9 = (new ParameterHandler.Query(var14, this.retrofit.stringConverter(var17, var3), var5)).array();
                  } else {
                     var9 = new ParameterHandler.Query(var14, this.retrofit.stringConverter(var2, var3), var5);
                  }
               } else {
                  Class var15;
                  if(var4 instanceof QueryName) {
                     var5 = ((QueryName)var4).encoded();
                     var15 = Utils.getRawType(var2);
                     this.gotQuery = true;
                     if(Iterable.class.isAssignableFrom(var15)) {
                        if(!(var2 instanceof ParameterizedType)) {
                           throw this.parameterError(var1, var15.getSimpleName() + " must include generic type (e.g., " + var15.getSimpleName() + "<String>)", new Object[0]);
                        }

                        var2 = Utils.getParameterUpperBound(0, (ParameterizedType)var2);
                        var9 = (new ParameterHandler.QueryName(this.retrofit.stringConverter(var2, var3), var5)).iterable();
                     } else if(var15.isArray()) {
                        var17 = ServiceMethod.boxIfPrimitive(var15.getComponentType());
                        var9 = (new ParameterHandler.QueryName(this.retrofit.stringConverter(var17, var3), var5)).array();
                     } else {
                        var9 = new ParameterHandler.QueryName(this.retrofit.stringConverter(var2, var3), var5);
                     }
                  } else if(var4 instanceof QueryMap) {
                     var6 = Utils.getRawType(var2);
                     if(!Map.class.isAssignableFrom(var6)) {
                        throw this.parameterError(var1, "@QueryMap parameter type must be Map.", new Object[0]);
                     }

                     var2 = Utils.getSupertype(var2, var6, Map.class);
                     if(!(var2 instanceof ParameterizedType)) {
                        throw this.parameterError(var1, "Map must include generic types (e.g., Map<String, String>)", new Object[0]);
                     }

                     ParameterizedType var21 = (ParameterizedType)var2;
                     var2 = Utils.getParameterUpperBound(0, var21);
                     if(String.class != var2) {
                        throw this.parameterError(var1, "@QueryMap keys must be of type String: " + var2, new Object[0]);
                     }

                     var2 = Utils.getParameterUpperBound(1, var21);
                     var9 = new ParameterHandler.QueryMap(this.retrofit.stringConverter(var2, var3), ((QueryMap)var4).encoded());
                  } else if(var4 instanceof Header) {
                     var14 = ((Header)var4).value();
                     var6 = Utils.getRawType(var2);
                     if(Iterable.class.isAssignableFrom(var6)) {
                        if(!(var2 instanceof ParameterizedType)) {
                           throw this.parameterError(var1, var6.getSimpleName() + " must include generic type (e.g., " + var6.getSimpleName() + "<String>)", new Object[0]);
                        }

                        var2 = Utils.getParameterUpperBound(0, (ParameterizedType)var2);
                        var9 = (new ParameterHandler.Header(var14, this.retrofit.stringConverter(var2, var3))).iterable();
                     } else if(var6.isArray()) {
                        var17 = ServiceMethod.boxIfPrimitive(var6.getComponentType());
                        var9 = (new ParameterHandler.Header(var14, this.retrofit.stringConverter(var17, var3))).array();
                     } else {
                        var9 = new ParameterHandler.Header(var14, this.retrofit.stringConverter(var2, var3));
                     }
                  } else if(var4 instanceof HeaderMap) {
                     var15 = Utils.getRawType(var2);
                     if(!Map.class.isAssignableFrom(var15)) {
                        throw this.parameterError(var1, "@HeaderMap parameter type must be Map.", new Object[0]);
                     }

                     var2 = Utils.getSupertype(var2, var15, Map.class);
                     if(!(var2 instanceof ParameterizedType)) {
                        throw this.parameterError(var1, "Map must include generic types (e.g., Map<String, String>)", new Object[0]);
                     }

                     ParameterizedType var16 = (ParameterizedType)var2;
                     var2 = Utils.getParameterUpperBound(0, var16);
                     if(String.class != var2) {
                        throw this.parameterError(var1, "@HeaderMap keys must be of type String: " + var2, new Object[0]);
                     }

                     var2 = Utils.getParameterUpperBound(1, var16);
                     var9 = new ParameterHandler.HeaderMap(this.retrofit.stringConverter(var2, var3));
                  } else if(var4 instanceof Field) {
                     if(!this.isFormEncoded) {
                        throw this.parameterError(var1, "@Field parameters can only be used with form encoding.", new Object[0]);
                     }

                     Field var19 = (Field)var4;
                     var14 = var19.value();
                     var5 = var19.encoded();
                     this.gotField = true;
                     var6 = Utils.getRawType(var2);
                     if(Iterable.class.isAssignableFrom(var6)) {
                        if(!(var2 instanceof ParameterizedType)) {
                           throw this.parameterError(var1, var6.getSimpleName() + " must include generic type (e.g., " + var6.getSimpleName() + "<String>)", new Object[0]);
                        }

                        var2 = Utils.getParameterUpperBound(0, (ParameterizedType)var2);
                        var9 = (new ParameterHandler.Field(var14, this.retrofit.stringConverter(var2, var3), var5)).iterable();
                     } else if(var6.isArray()) {
                        var17 = ServiceMethod.boxIfPrimitive(var6.getComponentType());
                        var9 = (new ParameterHandler.Field(var14, this.retrofit.stringConverter(var17, var3), var5)).array();
                     } else {
                        var9 = new ParameterHandler.Field(var14, this.retrofit.stringConverter(var2, var3), var5);
                     }
                  } else {
                     ParameterizedType var11;
                     Type var18;
                     if(var4 instanceof FieldMap) {
                        if(!this.isFormEncoded) {
                           throw this.parameterError(var1, "@FieldMap parameters can only be used with form encoding.", new Object[0]);
                        }

                        var6 = Utils.getRawType(var2);
                        if(!Map.class.isAssignableFrom(var6)) {
                           throw this.parameterError(var1, "@FieldMap parameter type must be Map.", new Object[0]);
                        }

                        var2 = Utils.getSupertype(var2, var6, Map.class);
                        if(!(var2 instanceof ParameterizedType)) {
                           throw this.parameterError(var1, "Map must include generic types (e.g., Map<String, String>)", new Object[0]);
                        }

                        var11 = (ParameterizedType)var2;
                        var18 = Utils.getParameterUpperBound(0, var11);
                        if(String.class != var18) {
                           throw this.parameterError(var1, "@FieldMap keys must be of type String: " + var18, new Object[0]);
                        }

                        var2 = Utils.getParameterUpperBound(1, var11);
                        Converter var20 = this.retrofit.stringConverter(var2, var3);
                        this.gotField = true;
                        var9 = new ParameterHandler.FieldMap(var20, ((FieldMap)var4).encoded());
                     } else if(var4 instanceof Part) {
                        if(!this.isMultipart) {
                           throw this.parameterError(var1, "@Part parameters can only be used with multipart encoding.", new Object[0]);
                        }

                        Part var12 = (Part)var4;
                        this.gotPart = true;
                        String var7 = var12.value();
                        var6 = Utils.getRawType(var2);
                        if(var7.isEmpty()) {
                           if(Iterable.class.isAssignableFrom(var6)) {
                              if(!(var2 instanceof ParameterizedType)) {
                                 throw this.parameterError(var1, var6.getSimpleName() + " must include generic type (e.g., " + var6.getSimpleName() + "<String>)", new Object[0]);
                              }

                              if(!MultipartBody.Part.class.isAssignableFrom(Utils.getRawType(Utils.getParameterUpperBound(0, (ParameterizedType)var2)))) {
                                 throw this.parameterError(var1, "@Part annotation must supply a name or use MultipartBody.Part parameter type.", new Object[0]);
                              }

                              var9 = ParameterHandler.RawPart.INSTANCE.iterable();
                           } else if(var6.isArray()) {
                              if(!MultipartBody.Part.class.isAssignableFrom(var6.getComponentType())) {
                                 throw this.parameterError(var1, "@Part annotation must supply a name or use MultipartBody.Part parameter type.", new Object[0]);
                              }

                              var9 = ParameterHandler.RawPart.INSTANCE.array();
                           } else {
                              if(!MultipartBody.Part.class.isAssignableFrom(var6)) {
                                 throw this.parameterError(var1, "@Part annotation must supply a name or use MultipartBody.Part parameter type.", new Object[0]);
                              }

                              var9 = ParameterHandler.RawPart.INSTANCE;
                           }
                        } else {
                           Headers var13 = Headers.of(new String[]{"Content-Disposition", "form-data; name=\"" + var7 + "\"", "Content-Transfer-Encoding", var12.encoding()});
                           if(Iterable.class.isAssignableFrom(var6)) {
                              if(!(var2 instanceof ParameterizedType)) {
                                 throw this.parameterError(var1, var6.getSimpleName() + " must include generic type (e.g., " + var6.getSimpleName() + "<String>)", new Object[0]);
                              }

                              var2 = Utils.getParameterUpperBound(0, (ParameterizedType)var2);
                              if(MultipartBody.Part.class.isAssignableFrom(Utils.getRawType(var2))) {
                                 throw this.parameterError(var1, "@Part parameters using the MultipartBody.Part must not include a part name in the annotation.", new Object[0]);
                              }

                              var9 = (new ParameterHandler.Part(var13, this.retrofit.requestBodyConverter(var2, var3, this.methodAnnotations))).iterable();
                           } else if(var6.isArray()) {
                              var17 = ServiceMethod.boxIfPrimitive(var6.getComponentType());
                              if(MultipartBody.Part.class.isAssignableFrom(var17)) {
                                 throw this.parameterError(var1, "@Part parameters using the MultipartBody.Part must not include a part name in the annotation.", new Object[0]);
                              }

                              var9 = (new ParameterHandler.Part(var13, this.retrofit.requestBodyConverter(var17, var3, this.methodAnnotations))).array();
                           } else {
                              if(MultipartBody.Part.class.isAssignableFrom(var6)) {
                                 throw this.parameterError(var1, "@Part parameters using the MultipartBody.Part must not include a part name in the annotation.", new Object[0]);
                              }

                              var9 = new ParameterHandler.Part(var13, this.retrofit.requestBodyConverter(var2, var3, this.methodAnnotations));
                           }
                        }
                     } else if(var4 instanceof PartMap) {
                        if(!this.isMultipart) {
                           throw this.parameterError(var1, "@PartMap parameters can only be used with multipart encoding.", new Object[0]);
                        }

                        this.gotPart = true;
                        var6 = Utils.getRawType(var2);
                        if(!Map.class.isAssignableFrom(var6)) {
                           throw this.parameterError(var1, "@PartMap parameter type must be Map.", new Object[0]);
                        }

                        var2 = Utils.getSupertype(var2, var6, Map.class);
                        if(!(var2 instanceof ParameterizedType)) {
                           throw this.parameterError(var1, "Map must include generic types (e.g., Map<String, String>)", new Object[0]);
                        }

                        var11 = (ParameterizedType)var2;
                        var18 = Utils.getParameterUpperBound(0, var11);
                        if(String.class != var18) {
                           throw this.parameterError(var1, "@PartMap keys must be of type String: " + var18, new Object[0]);
                        }

                        var2 = Utils.getParameterUpperBound(1, var11);
                        if(MultipartBody.Part.class.isAssignableFrom(Utils.getRawType(var2))) {
                           throw this.parameterError(var1, "@PartMap values cannot be MultipartBody.Part. Use @Part List<Part> or a different value type instead.", new Object[0]);
                        }

                        var9 = new ParameterHandler.PartMap(this.retrofit.requestBodyConverter(var2, var3, this.methodAnnotations), ((PartMap)var4).encoding());
                     } else if(var4 instanceof Body) {
                        if(this.isFormEncoded || this.isMultipart) {
                           throw this.parameterError(var1, "@Body parameters cannot be used with form or multi-part encoding.", new Object[0]);
                        }

                        if(this.gotBody) {
                           throw this.parameterError(var1, "Multiple @Body method annotations found.", new Object[0]);
                        }

                        Converter var10;
                        try {
                           var10 = this.retrofit.requestBodyConverter(var2, var3, this.methodAnnotations);
                        } catch (RuntimeException var8) {
                           throw this.parameterError(var8, var1, "Unable to create @Body converter for %s", new Object[]{var2});
                        }

                        this.gotBody = true;
                        var9 = new ParameterHandler.Body(var10);
                     } else {
                        var9 = null;
                     }
                  }
               }
            }
         } else {
            if(this.gotUrl) {
               throw this.parameterError(var1, "Multiple @Url method annotations found.", new Object[0]);
            }

            if(this.gotPath) {
               throw this.parameterError(var1, "@Path parameters may not be used with @Url.", new Object[0]);
            }

            if(this.gotQuery) {
               throw this.parameterError(var1, "A @Url parameter must not come after a @Query", new Object[0]);
            }

            if(this.relativeUrl != null) {
               throw this.parameterError(var1, "@Url cannot be used with @%s URL", new Object[]{this.httpMethod});
            }

            this.gotUrl = true;
            if(var2 != HttpUrl.class && var2 != String.class && var2 != URI.class && (!(var2 instanceof Class) || !"android.net.Uri".equals(((Class)var2).getName()))) {
               throw this.parameterError(var1, "@Url must be okhttp3.HttpUrl, String, java.net.URI, or android.net.Uri type.", new Object[0]);
            }

            var9 = new ParameterHandler.RelativeUrl();
         }

         return (ParameterHandler)var9;
      }

      private void validatePathName(int var1, String var2) {
         if(!ServiceMethod.PARAM_NAME_REGEX.matcher(var2).matches()) {
            throw this.parameterError(var1, "@Path parameter name must match %s. Found: %s", new Object[]{ServiceMethod.PARAM_URL_REGEX.pattern(), var2});
         } else if(!this.relativeUrlParamNames.contains(var2)) {
            throw this.parameterError(var1, "URL \"%s\" does not contain \"{%s}\".", new Object[]{this.relativeUrl, var2});
         }
      }

      public ServiceMethod build() {
         this.callAdapter = this.createCallAdapter();
         this.responseType = this.callAdapter.responseType();
         if(this.responseType != Response.class && this.responseType != okhttp3.Response.class) {
            this.responseConverter = this.createResponseConverter();
            Annotation[] var3 = this.methodAnnotations;
            int var2 = var3.length;

            int var1;
            for(var1 = 0; var1 < var2; ++var1) {
               this.parseMethodAnnotation(var3[var1]);
            }

            if(this.httpMethod == null) {
               throw this.methodError("HTTP method annotation is required (e.g., @GET, @POST, etc.).", new Object[0]);
            } else {
               if(!this.hasBody) {
                  if(this.isMultipart) {
                     throw this.methodError("Multipart can only be specified on HTTP methods with request body (e.g., @POST).", new Object[0]);
                  }

                  if(this.isFormEncoded) {
                     throw this.methodError("FormUrlEncoded can only be specified on HTTP methods with request body (e.g., @POST).", new Object[0]);
                  }
               }

               var2 = this.parameterAnnotationsArray.length;
               this.parameterHandlers = new ParameterHandler[var2];

               for(var1 = 0; var1 < var2; ++var1) {
                  Type var5 = this.parameterTypes[var1];
                  if(Utils.hasUnresolvableType(var5)) {
                     throw this.parameterError(var1, "Parameter type must not include a type variable or wildcard: %s", new Object[]{var5});
                  }

                  Annotation[] var4 = this.parameterAnnotationsArray[var1];
                  if(var4 == null) {
                     throw this.parameterError(var1, "No Retrofit annotation found.", new Object[0]);
                  }

                  this.parameterHandlers[var1] = this.parseParameter(var1, var5, var4);
               }

               if(this.relativeUrl == null && !this.gotUrl) {
                  throw this.methodError("Missing either @%s URL or @Url parameter.", new Object[]{this.httpMethod});
               } else if(!this.isFormEncoded && !this.isMultipart && !this.hasBody && this.gotBody) {
                  throw this.methodError("Non-body HTTP method cannot contain @Body.", new Object[0]);
               } else if(this.isFormEncoded && !this.gotField) {
                  throw this.methodError("Form-encoded method must contain at least one @Field.", new Object[0]);
               } else if(this.isMultipart && !this.gotPart) {
                  throw this.methodError("Multipart method must contain at least one @Part.", new Object[0]);
               } else {
                  return new ServiceMethod(this);
               }
            }
         } else {
            throw this.methodError("'" + Utils.getRawType(this.responseType).getName() + "' is not a valid response body type. Did you mean ResponseBody?", new Object[0]);
         }
      }
   }
}
