package rx;

import rx.exceptions.OnErrorFailedException;

public class d {
   final d.a a;

   protected d(d.a var1) {
      this.a = var1;
   }

   @Deprecated
   public static d a(d.a var0) {
      return new d(rx.c.c.a(var0));
   }

   public static d b(d.a var0) {
      return new d(rx.c.c.a(var0));
   }

   public final d a(g var1) {
      boolean var2;
      if(!(this.a instanceof rx.b.a.c)) {
         var2 = true;
      } else {
         var2 = false;
      }

      return this.a(var1, var2);
   }

   public final d a(g var1, boolean var2) {
      d var3;
      if(this instanceof rx.b.e.d) {
         var3 = ((rx.b.e.d)this).b(var1);
      } else {
         var3 = b(new rx.b.a.e(this, var1, var2));
      }

      return var3;
   }

   public h a() {
      return new h(rx.b.a.d.a(this));
   }

   public final k a(j var1) {
      k var6;
      k var7;
      try {
         var1.onStart();
         rx.c.c.a(this, this.a).call(var1);
         var7 = rx.c.c.a((k)var1);
      } catch (Throwable var4) {
         Throwable var2 = var4;
         rx.exceptions.a.a(var4);

         try {
            var1.onError(rx.c.c.c(var2));
         } catch (Throwable var3) {
            rx.exceptions.a.a(var3);
            OnErrorFailedException var5 = new OnErrorFailedException("Error occurred attempting to subscribe [" + var4.getMessage() + "] and then again while trying to pass to onError.", var3);
            rx.c.c.c(var5);
            throw var5;
         }

         var6 = rx.d.c.a();
         return var6;
      }

      var6 = var7;
      return var6;
   }

   public a b() {
      return a.a(this);
   }

   public interface a extends rx.a.b {
   }

   public interface b extends rx.a.c {
   }
}
