package rx.c;

import rx.k;

public final class c {
   static volatile rx.a.b a;
   static volatile rx.a.c b;
   static volatile rx.a.c c;
   static volatile rx.a.c d;
   static volatile rx.a.d e;
   static volatile rx.a.d f;
   static volatile rx.a.d g;
   static volatile rx.a.c h;
   static volatile rx.a.c i;
   static volatile rx.a.c j;
   static volatile rx.a.c k;
   static volatile rx.a.c l;
   static volatile rx.a.c m;
   static volatile rx.a.c n;
   static volatile rx.a.c o;
   static volatile rx.a.c p;

   static {
      a();
   }

   public static rx.a.a a(rx.a.a var0) {
      rx.a.c var1 = d;
      if(var1 != null) {
         var0 = (rx.a.a)var1.a(var0);
      }

      return var0;
   }

   public static rx.a.a a(rx.a.a var0) {
      rx.a.c var1 = h;
      if(var1 != null) {
         var0 = (rx.a.a)var1.a(var0);
      }

      return var0;
   }

   public static rx.d.a a(rx.d.a var0) {
      rx.a.c var1 = b;
      if(var1 != null) {
         var0 = (rx.d.a)var1.a(var0);
      }

      return var0;
   }

   public static rx.d.a a(rx.d var0, rx.d.a var1) {
      rx.a.d var2 = e;
      rx.d.a var3;
      if(var2 != null) {
         var3 = (rx.d.a)var2.a(var0, var1);
      } else {
         var3 = var1;
      }

      return var3;
   }

   public static rx.d.b a(rx.d.b var0) {
      rx.a.c var1 = o;
      if(var1 != null) {
         var0 = (rx.d.b)var1.a(var0);
      }

      return var0;
   }

   public static rx.h.a a(rx.h.a var0) {
      rx.a.c var1 = c;
      if(var1 != null) {
         var0 = (rx.h.a)var1.a(var0);
      }

      return var0;
   }

   public static k a(k var0) {
      rx.a.c var1 = i;
      if(var1 != null) {
         var0 = (k)var1.a(var0);
      }

      return var0;
   }

   static void a() {
      a = new rx.a.b() {
         public void a(Throwable var1) {
            f.a().b().a(var1);
         }

         // $FF: synthetic method
         public void call(Object var1) {
            this.a((Throwable)var1);
         }
      };
      e = new rx.a.d() {
         public rx.d.a a(rx.d var1, rx.d.a var2) {
            return f.a().c().a(var1, var2);
         }
      };
      i = new rx.a.c() {
         public k a(k var1) {
            return f.a().c().a(var1);
         }
      };
      f = new rx.a.d() {
         public rx.h.a a(rx.h var1, rx.h.a var2) {
            h var3 = f.a().d();
            if(var3 != i.a()) {
               var2 = new rx.b.a.f(var3.a(var1, new rx.b.a.h((rx.h.a)var2)));
            }

            return (rx.h.a)var2;
         }
      };
      j = new rx.a.c() {
         public k a(k var1) {
            return f.a().d().a(var1);
         }
      };
      g = new rx.a.d() {
         public rx.a.a a(rx.a var1, rx.a.a var2) {
            return f.a().e().a(var1, var2);
         }
      };
      h = new rx.a.c() {
         public rx.a.a a(rx.a.a var1) {
            return f.a().f().a(var1);
         }
      };
      k = new rx.a.c() {
         public Throwable a(Throwable var1) {
            return f.a().c().a(var1);
         }
      };
      n = new rx.a.c() {
         public rx.d.b a(rx.d.b var1) {
            return f.a().c().a(var1);
         }
      };
      l = new rx.a.c() {
         public Throwable a(Throwable var1) {
            return f.a().d().a(var1);
         }
      };
      o = new rx.a.c() {
         public rx.d.b a(rx.d.b var1) {
            return f.a().d().a(var1);
         }
      };
      m = new rx.a.c() {
         public Throwable a(Throwable var1) {
            return f.a().e().a(var1);
         }
      };
      p = new rx.a.c() {
         public rx.a.b a(rx.a.b var1) {
            return f.a().e().a(var1);
         }
      };
      b();
   }

   public static void a(Throwable var0) {
      rx.a.b var1 = a;
      if(var1 != null) {
         try {
            var1.call(var0);
            return;
         } catch (Throwable var2) {
            System.err.println("The onError handler threw an Exception. It shouldn't. => " + var2.getMessage());
            var2.printStackTrace();
            b(var2);
         }
      }

      b(var0);
   }

   static void b() {
      b = new rx.a.c() {
         public rx.d.a a(rx.d.a var1) {
            return f.a().c().a(var1);
         }
      };
      c = new rx.a.c() {
         public rx.h.a a(rx.h.a var1) {
            return f.a().d().a(var1);
         }
      };
      d = new rx.a.c() {
         public rx.a.a a(rx.a.a var1) {
            return f.a().e().a(var1);
         }
      };
   }

   static void b(Throwable var0) {
      Thread var1 = Thread.currentThread();
      var1.getUncaughtExceptionHandler().uncaughtException(var1, var0);
   }

   public static Throwable c(Throwable var0) {
      rx.a.c var1 = k;
      if(var1 != null) {
         var0 = (Throwable)var1.a(var0);
      }

      return var0;
   }
}
