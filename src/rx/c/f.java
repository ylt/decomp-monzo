package rx.c;

import java.util.Iterator;
import java.util.Properties;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicReference;

public class f {
   static final b a = new b() {
   };
   private static final f b = new f();
   private final AtomicReference c = new AtomicReference();
   private final AtomicReference d = new AtomicReference();
   private final AtomicReference e = new AtomicReference();
   private final AtomicReference f = new AtomicReference();
   private final AtomicReference g = new AtomicReference();

   static Object a(Class var0, Properties var1) {
      Properties var3 = (Properties)var1.clone();
      String var2 = var0.getSimpleName();
      String var12 = var3.getProperty("rxjava.plugin." + var2 + ".implementation");
      if(var12 == null) {
         Iterator var5 = var3.entrySet().iterator();

         while(var5.hasNext()) {
            Entry var4 = (Entry)var5.next();
            String var6 = var4.getKey().toString();
            if(var6.startsWith("rxjava.plugin.") && var6.endsWith(".class") && var2.equals(var4.getValue().toString())) {
               var12 = var6.substring(0, var6.length() - ".class".length()).substring("rxjava.plugin.".length());
               String var13 = "rxjava.plugin." + var12 + ".impl";
               var12 = var3.getProperty(var13);
               if(var12 == null) {
                  throw new IllegalStateException("Implementing class declaration for " + var2 + " missing: " + var13);
               }
               break;
            }
         }
      }

      Object var11;
      if(var12 != null) {
         try {
            var11 = Class.forName(var12).asSubclass(var0).newInstance();
         } catch (ClassCastException var7) {
            throw new IllegalStateException(var2 + " implementation is not an instance of " + var2 + ": " + var12, var7);
         } catch (ClassNotFoundException var8) {
            throw new IllegalStateException(var2 + " implementation class not found: " + var12, var8);
         } catch (InstantiationException var9) {
            throw new IllegalStateException(var2 + " implementation not able to be instantiated: " + var12, var9);
         } catch (IllegalAccessException var10) {
            throw new IllegalStateException(var2 + " implementation not able to be accessed: " + var12, var10);
         }
      } else {
         var11 = null;
      }

      return var11;
   }

   @Deprecated
   public static f a() {
      return b;
   }

   public b b() {
      if(this.c.get() == null) {
         Object var1 = a(b.class, System.getProperties());
         if(var1 == null) {
            this.c.compareAndSet((Object)null, a);
         } else {
            this.c.compareAndSet((Object)null, (b)var1);
         }
      }

      return (b)this.c.get();
   }

   public d c() {
      if(this.d.get() == null) {
         Object var1 = a(d.class, System.getProperties());
         if(var1 == null) {
            this.d.compareAndSet((Object)null, e.a());
         } else {
            this.d.compareAndSet((Object)null, (d)var1);
         }
      }

      return (d)this.d.get();
   }

   public h d() {
      if(this.e.get() == null) {
         Object var1 = a(h.class, System.getProperties());
         if(var1 == null) {
            this.e.compareAndSet((Object)null, i.a());
         } else {
            this.e.compareAndSet((Object)null, (h)var1);
         }
      }

      return (h)this.e.get();
   }

   public a e() {
      if(this.f.get() == null) {
         Object var1 = a(a.class, System.getProperties());
         if(var1 == null) {
            this.f.compareAndSet((Object)null, new a() {
            });
         } else {
            this.f.compareAndSet((Object)null, (a)var1);
         }
      }

      return (a)this.f.get();
   }

   public g f() {
      if(this.g.get() == null) {
         Object var1 = a(g.class, System.getProperties());
         if(var1 == null) {
            this.g.compareAndSet((Object)null, g.a());
         } else {
            this.g.compareAndSet((Object)null, (g)var1);
         }
      }

      return (g)this.g.get();
   }
}
