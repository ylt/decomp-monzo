package rx.b.c;

import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import rx.k;
import rx.b.e.e;
import rx.exceptions.OnErrorNotImplementedException;

public final class c extends AtomicReference implements Runnable, k {
   final e a;
   final rx.a.a b;

   public c(rx.a.a var1) {
      this.b = var1;
      this.a = new e();
   }

   public c(rx.a.a var1, e var2) {
      this.b = var1;
      this.a = new e(new c.b(this, var2));
   }

   void a(Throwable var1) {
      rx.c.c.a(var1);
      Thread var2 = Thread.currentThread();
      var2.getUncaughtExceptionHandler().uncaughtException(var2, var1);
   }

   public void a(Future var1) {
      this.a.a((k)(new c.a(var1)));
   }

   public boolean isUnsubscribed() {
      return this.a.isUnsubscribed();
   }

   public void run() {
      try {
         this.lazySet(Thread.currentThread());
         this.b.a();
      } catch (OnErrorNotImplementedException var6) {
         IllegalStateException var2 = new IllegalStateException("Exception thrown on Scheduler.Worker thread. Add `onError` handling.", var6);
         this.a((Throwable)var2);
      } catch (Throwable var7) {
         IllegalStateException var1 = new IllegalStateException("Fatal Exception thrown on Scheduler.Worker thread.", var7);
         this.a((Throwable)var1);
      } finally {
         this.unsubscribe();
      }

   }

   public void unsubscribe() {
      if(!this.a.isUnsubscribed()) {
         this.a.unsubscribe();
      }

   }

   final class a implements k {
      private final Future b;

      a(Future var2) {
         this.b = var2;
      }

      public boolean isUnsubscribed() {
         return this.b.isCancelled();
      }

      public void unsubscribe() {
         if(c.this.get() != Thread.currentThread()) {
            this.b.cancel(true);
         } else {
            this.b.cancel(false);
         }

      }
   }

   static final class b extends AtomicBoolean implements k {
      final c a;
      final e b;

      public b(c var1, e var2) {
         this.a = var1;
         this.b = var2;
      }

      public boolean isUnsubscribed() {
         return this.a.isUnsubscribed();
      }

      public void unsubscribe() {
         if(this.compareAndSet(false, true)) {
            this.b.b(this.a);
         }

      }
   }
}
