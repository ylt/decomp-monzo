package rx.b.c;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import rx.g;
import rx.k;
import rx.b.e.e;

public final class a extends g {
   static final int a;
   static final a.c b;
   static final a.b c;
   final AtomicReference d;

   static {
      int var0;
      label11: {
         int var1 = Integer.getInteger("rx.scheduler.max-computation-threads", 0).intValue();
         int var2 = Runtime.getRuntime().availableProcessors();
         if(var1 > 0) {
            var0 = var1;
            if(var1 <= var2) {
               break label11;
            }
         }

         var0 = var2;
      }

      a = var0;
      b = new a.c(rx.b.e.c.a);
      b.unsubscribe();
      c = new a.b((ThreadFactory)null, 0);
   }

   public g.a a() {
      return new a.a(((a.b)this.d.get()).a());
   }

   public k a(rx.a.a var1) {
      return ((a.b)this.d.get()).a().b(var1, -1L, TimeUnit.NANOSECONDS);
   }

   static final class a extends g.a {
      private final e a = new e();
      private final rx.d.a b = new rx.d.a();
      private final e c;
      private final a.c d;

      a(a.c var1) {
         this.c = new e(new k[]{this.a, this.b});
         this.d = var1;
      }

      public k a(final rx.a.a var1) {
         Object var2;
         if(this.isUnsubscribed()) {
            var2 = rx.d.c.a();
         } else {
            var2 = this.d.a(new rx.a.a() {
               public void a() {
                  if(!a.this.isUnsubscribed()) {
                     var1.a();
                  }

               }
            }, 0L, (TimeUnit)null, this.a);
         }

         return (k)var2;
      }

      public boolean isUnsubscribed() {
         return this.c.isUnsubscribed();
      }

      public void unsubscribe() {
         this.c.unsubscribe();
      }
   }

   static final class b {
      final int a;
      final a.c[] b;
      long c;

      b(ThreadFactory var1, int var2) {
         this.a = var2;
         this.b = new a.c[var2];

         for(int var3 = 0; var3 < var2; ++var3) {
            this.b[var3] = new a.c(var1);
         }

      }

      public a.c a() {
         int var1 = this.a;
         a.c var4;
         if(var1 == 0) {
            var4 = a.b;
         } else {
            a.c[] var5 = this.b;
            long var2 = this.c;
            this.c = 1L + var2;
            var4 = var5[(int)(var2 % (long)var1)];
         }

         return var4;
      }
   }

   static final class c extends b {
      c(ThreadFactory var1) {
         super(var1);
      }
   }
}
