package rx.b.c;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import rx.g;
import rx.k;
import rx.b.e.e;

public class b extends g.a implements k {
   public static final int b = Integer.getInteger("rx.scheduler.jdk6.purge-frequency-millis", 1000).intValue();
   private static final boolean d;
   private static final ConcurrentHashMap e = new ConcurrentHashMap();
   private static final AtomicReference f = new AtomicReference();
   private static volatile Object g;
   private static final Object h = new Object();
   volatile boolean a;
   private final ScheduledExecutorService c;

   static {
      boolean var1 = Boolean.getBoolean("rx.scheduler.jdk6.purge-force");
      int var0 = rx.b.e.a.b();
      if(var1 || var0 != 0 && var0 < 21) {
         var1 = false;
      } else {
         var1 = true;
      }

      d = var1;
   }

   public b(ThreadFactory var1) {
      ScheduledExecutorService var2 = Executors.newScheduledThreadPool(1, var1);
      if(!b(var2) && var2 instanceof ScheduledThreadPoolExecutor) {
         a((ScheduledThreadPoolExecutor)var2);
      }

      this.c = var2;
   }

   static void a() {
      // $FF: Couldn't be decompiled
   }

   public static void a(ScheduledExecutorService var0) {
      e.remove(var0);
   }

   public static void a(ScheduledThreadPoolExecutor var0) {
      while(true) {
         if((ScheduledExecutorService)f.get() == null) {
            ScheduledExecutorService var1 = Executors.newScheduledThreadPool(1, new rx.b.e.c("RxSchedulerPurge-"));
            if(!f.compareAndSet((Object)null, var1)) {
               var1.shutdownNow();
               continue;
            }

            var1.scheduleAtFixedRate(new Runnable() {
               public void run() {
                  b.a();
               }
            }, (long)b, (long)b, TimeUnit.MILLISECONDS);
         }

         e.putIfAbsent(var0, var0);
         return;
      }
   }

   public static boolean b(ScheduledExecutorService var0) {
      boolean var1;
      if(d) {
         Method var7;
         if(var0 instanceof ScheduledThreadPoolExecutor) {
            Object var2 = g;
            if(var2 == h) {
               var1 = false;
               return var1;
            }

            if(var2 == null) {
               var7 = c(var0);
               Object var3;
               if(var7 != null) {
                  var3 = var7;
               } else {
                  var3 = h;
               }

               g = var3;
            } else {
               var7 = (Method)var2;
            }
         } else {
            var7 = c(var0);
         }

         if(var7 != null) {
            label49: {
               try {
                  var7.invoke(var0, new Object[]{Boolean.valueOf(true)});
               } catch (InvocationTargetException var4) {
                  rx.c.c.a((Throwable)var4);
                  break label49;
               } catch (IllegalAccessException var5) {
                  rx.c.c.a((Throwable)var5);
                  break label49;
               } catch (IllegalArgumentException var6) {
                  rx.c.c.a((Throwable)var6);
                  break label49;
               }

               var1 = true;
               return var1;
            }
         }
      }

      var1 = false;
      return var1;
   }

   static Method c(ScheduledExecutorService var0) {
      Method[] var4 = var0.getClass().getMethods();
      int var2 = var4.length;
      int var1 = 0;

      Method var5;
      while(true) {
         if(var1 >= var2) {
            var5 = null;
            break;
         }

         var5 = var4[var1];
         if(var5.getName().equals("setRemoveOnCancelPolicy")) {
            Class[] var3 = var5.getParameterTypes();
            if(var3.length == 1 && var3[0] == Boolean.TYPE) {
               break;
            }
         }

         ++var1;
      }

      return var5;
   }

   public c a(rx.a.a var1, long var2, TimeUnit var4, e var5) {
      c var6 = new c(rx.c.c.a(var1), var5);
      var5.a((k)var6);
      Object var7;
      if(var2 <= 0L) {
         var7 = this.c.submit(var6);
      } else {
         var7 = this.c.schedule(var6, var2, var4);
      }

      var6.a((Future)var7);
      return var6;
   }

   public k a(rx.a.a var1) {
      return this.a(var1, 0L, (TimeUnit)null);
   }

   public k a(rx.a.a var1, long var2, TimeUnit var4) {
      Object var5;
      if(this.a) {
         var5 = rx.d.c.a();
      } else {
         var5 = this.b(var1, var2, var4);
      }

      return (k)var5;
   }

   public c b(rx.a.a var1, long var2, TimeUnit var4) {
      c var5 = new c(rx.c.c.a(var1));
      Object var6;
      if(var2 <= 0L) {
         var6 = this.c.submit(var5);
      } else {
         var6 = this.c.schedule(var5, var2, var4);
      }

      var5.a((Future)var6);
      return var5;
   }

   public boolean isUnsubscribed() {
      return this.a;
   }

   public void unsubscribe() {
      this.a = true;
      this.c.shutdownNow();
      a(this.c);
   }
}
