package rx.b.b;

import java.util.concurrent.atomic.AtomicBoolean;
import rx.f;
import rx.j;

public final class a extends AtomicBoolean implements f {
   final j a;
   final Object b;

   public a(j var1, Object var2) {
      this.a = var1;
      this.b = var2;
   }

   public void request(long var1) {
      if(var1 < 0L) {
         throw new IllegalArgumentException("n >= 0 required");
      } else {
         if(var1 != 0L && this.compareAndSet(false, true)) {
            j var4 = this.a;
            if(!var4.isUnsubscribed()) {
               Object var3 = this.b;

               try {
                  var4.onNext(var3);
               } catch (Throwable var6) {
                  rx.exceptions.a.a(var6, var4, var3);
                  return;
               }

               if(!var4.isUnsubscribed()) {
                  var4.onCompleted();
               }
            }
         }

      }
   }
}
