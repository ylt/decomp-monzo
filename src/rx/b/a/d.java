package rx.b.a;

import java.util.NoSuchElementException;
import rx.i;
import rx.j;
import rx.k;

public class d implements rx.h.a {
   private final rx.d a;

   public d(rx.d var1) {
      this.a = var1;
   }

   public static d a(rx.d var0) {
      return new d(var0);
   }

   public void a(final i var1) {
      j var2 = new j() {
         private boolean c;
         private boolean d;
         private Object e;

         public void onCompleted() {
            if(!this.c) {
               if(this.d) {
                  var1.a(this.e);
               } else {
                  var1.a((Throwable)(new NoSuchElementException("Observable emitted no items")));
               }
            }

         }

         public void onError(Throwable var1x) {
            var1.a(var1x);
            this.unsubscribe();
         }

         public void onNext(Object var1x) {
            if(this.d) {
               this.c = true;
               var1.a((Throwable)(new IllegalArgumentException("Observable emitted too many elements")));
               this.unsubscribe();
            } else {
               this.d = true;
               this.e = var1x;
            }

         }

         public void onStart() {
            this.request(2L);
         }
      };
      var1.a((k)var2);
      this.a.a(var2);
   }

   // $FF: synthetic method
   public void call(Object var1) {
      this.a((i)var1);
   }
}
