package rx.b.a;

import java.io.Serializable;

public final class b {
   private static final Object a = new Serializable() {
      public String toString() {
         return "Notification=>Completed";
      }
   };
   private static final Object b = new Serializable() {
      public String toString() {
         return "Notification=>NULL";
      }
   };

   public static Object a(Object var0) {
      Object var1 = var0;
      if(var0 == null) {
         var1 = b;
      }

      return var1;
   }

   public static Object b(Object var0) {
      Object var1 = var0;
      if(var0 == b) {
         var1 = null;
      }

      return var1;
   }
}
