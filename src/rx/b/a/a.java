package rx.b.a;

import java.util.concurrent.atomic.AtomicLong;

public final class a {
   public static long a(long var0, long var2) {
      var2 += var0;
      var0 = var2;
      if(var2 < 0L) {
         var0 = Long.MAX_VALUE;
      }

      return var0;
   }

   public static long a(AtomicLong var0, long var1) {
      long var3;
      do {
         var3 = var0.get();
      } while(!var0.compareAndSet(var3, a(var3, var1)));

      return var3;
   }

   public static boolean a(long var0) {
      if(var0 < 0L) {
         throw new IllegalArgumentException("n >= 0 required but it was " + var0);
      } else {
         boolean var2;
         if(var0 != 0L) {
            var2 = true;
         } else {
            var2 = false;
         }

         return var2;
      }
   }

   public static long b(AtomicLong var0, long var1) {
      long var3 = Long.MAX_VALUE;

      while(true) {
         long var7 = var0.get();
         if(var7 == Long.MAX_VALUE) {
            var1 = var3;
            break;
         }

         long var5 = var7 - var1;
         if(var5 < 0L) {
            throw new IllegalStateException("More produced than requested: " + var5);
         }

         if(var0.compareAndSet(var7, var5)) {
            var1 = var5;
            break;
         }
      }

      return var1;
   }
}
