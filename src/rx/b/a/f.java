package rx.b.a;

import java.util.NoSuchElementException;
import rx.i;
import rx.j;
import rx.k;

public final class f implements rx.h.a {
   final rx.d.a a;

   public f(rx.d.a var1) {
      this.a = var1;
   }

   public void a(i var1) {
      f.a var2 = new f.a(var1);
      var1.a((k)var2);
      this.a.call(var2);
   }

   // $FF: synthetic method
   public void call(Object var1) {
      this.a((i)var1);
   }

   static final class a extends j {
      final i a;
      Object b;
      int c;

      a(i var1) {
         this.a = var1;
      }

      public void onCompleted() {
         int var1 = this.c;
         if(var1 == 0) {
            this.a.a((Throwable)(new NoSuchElementException()));
         } else if(var1 == 1) {
            this.c = 2;
            Object var2 = this.b;
            this.b = null;
            this.a.a(var2);
         }

      }

      public void onError(Throwable var1) {
         if(this.c == 2) {
            rx.c.c.a(var1);
         } else {
            this.b = null;
            this.a.a(var1);
         }

      }

      public void onNext(Object var1) {
         int var2 = this.c;
         if(var2 == 0) {
            this.c = 1;
            this.b = var1;
         } else if(var2 == 1) {
            this.c = 2;
            this.a.a((Throwable)(new IndexOutOfBoundsException("The upstream produced more than one value")));
         }

      }
   }
}
