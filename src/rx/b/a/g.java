package rx.b.a;

import rx.i;
import rx.j;
import rx.k;

public final class g implements rx.h.a {
   final rx.h.a a;
   final rx.d.b b;

   public static i a(j var0) {
      g.a var1 = new g.a(var0);
      var0.add(var1);
      return var1;
   }

   public void a(i var1) {
      f.a var2 = new f.a(var1);
      var1.a((k)var2);

      try {
         j var3 = (j)rx.c.c.a(this.b).a(var2);
         i var5 = a(var3);
         var3.onStart();
         this.a.call(var5);
      } catch (Throwable var4) {
         rx.exceptions.a.a(var4, var1);
      }

   }

   // $FF: synthetic method
   public void call(Object var1) {
      this.a((i)var1);
   }

   static final class a extends i {
      final j a;

      a(j var1) {
         this.a = var1;
      }

      public void a(Object var1) {
         this.a.setProducer(new rx.b.b.a(this.a, var1));
      }

      public void a(Throwable var1) {
         this.a.onError(var1);
      }
   }
}
