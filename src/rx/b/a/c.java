package rx.b.a;

import java.util.Queue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import rx.j;
import rx.k;
import rx.exceptions.MissingBackpressureException;

public final class c implements rx.d.a {
   final rx.a.b a;
   final rx.c.a b;

   public void a(j var1) {
      Object var2;
      switch(null.a[this.b.ordinal()]) {
      case 1:
         var2 = new c.g(var1);
         break;
      case 2:
         var2 = new c.d(var1);
         break;
      case 3:
         var2 = new c.c(var1);
         break;
      case 4:
         var2 = new c.e(var1);
         break;
      default:
         var2 = new c.b(var1, rx.b.e.b.a);
      }

      var1.add((k)var2);
      var1.setProducer((rx.f)var2);
      this.a.call(var2);
   }

   // $FF: synthetic method
   public void call(Object var1) {
      this.a((j)var1);
   }

   abstract static class a extends AtomicLong implements rx.c, rx.f, k {
      final j a;
      final rx.d.b b;

      public a(j var1) {
         this.a = var1;
         this.b = new rx.d.b();
      }

      void a() {
      }

      void b() {
      }

      public final boolean isUnsubscribed() {
         return this.b.isUnsubscribed();
      }

      public void onCompleted() {
         if(!this.a.isUnsubscribed()) {
            try {
               this.a.onCompleted();
            } finally {
               this.b.unsubscribe();
            }
         }

      }

      public void onError(Throwable var1) {
         if(!this.a.isUnsubscribed()) {
            try {
               this.a.onError(var1);
            } finally {
               this.b.unsubscribe();
            }
         }

      }

      public final void request(long var1) {
         if(a.a(var1)) {
            a.a(this, var1);
            this.b();
         }

      }

      public final void unsubscribe() {
         this.b.unsubscribe();
         this.a();
      }
   }

   static final class b extends c.a {
      final Queue c;
      Throwable d;
      volatile boolean e;
      final AtomicInteger f;

      public b(j var1, int var2) {
         super(var1);
         Object var3;
         if(rx.b.e.b.h.a()) {
            var3 = new rx.b.e.b.b(var2);
         } else {
            var3 = new rx.b.e.a.a(var2);
         }

         this.c = (Queue)var3;
         this.f = new AtomicInteger();
      }

      void a() {
         if(this.f.getAndIncrement() == 0) {
            this.c.clear();
         }

      }

      void b() {
         this.c();
      }

      void c() {
         if(this.f.getAndIncrement() == 0) {
            j var9 = this.a;
            Queue var11 = this.c;
            int var1 = 1;

            int var12;
            do {
               long var5 = this.get();

               long var3;
               boolean var7;
               Throwable var13;
               for(var3 = 0L; var3 != var5; ++var3) {
                  if(var9.isUnsubscribed()) {
                     var11.clear();
                     return;
                  }

                  var7 = this.e;
                  Object var10 = var11.poll();
                  boolean var2;
                  if(var10 == null) {
                     var2 = true;
                  } else {
                     var2 = false;
                  }

                  if(var7 && var2) {
                     var13 = this.d;
                     if(var13 != null) {
                        super.onError(var13);
                     } else {
                        super.onCompleted();
                     }

                     return;
                  }

                  if(var2) {
                     break;
                  }

                  var9.onNext(b.b(var10));
               }

               if(var3 == var5) {
                  if(var9.isUnsubscribed()) {
                     var11.clear();
                     break;
                  }

                  var7 = this.e;
                  boolean var8 = var11.isEmpty();
                  if(var7 && var8) {
                     var13 = this.d;
                     if(var13 != null) {
                        super.onError(var13);
                     } else {
                        super.onCompleted();
                     }
                     break;
                  }
               }

               if(var3 != 0L) {
                  a.b(this, var3);
               }

               var12 = this.f.addAndGet(-var1);
               var1 = var12;
            } while(var12 != 0);
         }

      }

      public void onCompleted() {
         this.e = true;
         this.c();
      }

      public void onError(Throwable var1) {
         this.d = var1;
         this.e = true;
         this.c();
      }

      public void onNext(Object var1) {
         this.c.offer(b.a(var1));
         this.c();
      }
   }

   static final class c extends c.f {
      public c(j var1) {
         super(var1);
      }

      void c() {
      }
   }

   static final class d extends c.f {
      private boolean c;

      public d(j var1) {
         super(var1);
      }

      void c() {
         this.onError(new MissingBackpressureException("create: could not emit value due to lack of requests"));
      }

      public void onCompleted() {
         if(!this.c) {
            this.c = true;
            super.onCompleted();
         }

      }

      public void onError(Throwable var1) {
         if(this.c) {
            rx.c.c.a(var1);
         } else {
            this.c = true;
            super.onError(var1);
         }

      }

      public void onNext(Object var1) {
         if(!this.c) {
            super.onNext(var1);
         }

      }
   }

   static final class e extends c.a {
      final AtomicReference c = new AtomicReference();
      Throwable d;
      volatile boolean e;
      final AtomicInteger f = new AtomicInteger();

      public e(j var1) {
         super(var1);
      }

      void a() {
         if(this.f.getAndIncrement() == 0) {
            this.c.lazySet((Object)null);
         }

      }

      void b() {
         this.c();
      }

      void c() {
         if(this.f.getAndIncrement() == 0) {
            j var10 = this.a;
            AtomicReference var8 = this.c;
            int var1 = 1;

            int var11;
            do {
               long var5 = this.get();

               boolean var2;
               long var3;
               boolean var7;
               Throwable var12;
               for(var3 = 0L; var3 != var5; ++var3) {
                  if(var10.isUnsubscribed()) {
                     var8.lazySet((Object)null);
                     return;
                  }

                  var7 = this.e;
                  Object var9 = var8.getAndSet((Object)null);
                  if(var9 == null) {
                     var2 = true;
                  } else {
                     var2 = false;
                  }

                  if(var7 && var2) {
                     var12 = this.d;
                     if(var12 != null) {
                        super.onError(var12);
                     } else {
                        super.onCompleted();
                     }

                     return;
                  }

                  if(var2) {
                     break;
                  }

                  var10.onNext(b.b(var9));
               }

               if(var3 == var5) {
                  if(var10.isUnsubscribed()) {
                     var8.lazySet((Object)null);
                     break;
                  }

                  var7 = this.e;
                  if(var8.get() == null) {
                     var2 = true;
                  } else {
                     var2 = false;
                  }

                  if(var7 && var2) {
                     var12 = this.d;
                     if(var12 != null) {
                        super.onError(var12);
                     } else {
                        super.onCompleted();
                     }
                     break;
                  }
               }

               if(var3 != 0L) {
                  a.b(this, var3);
               }

               var11 = this.f.addAndGet(-var1);
               var1 = var11;
            } while(var11 != 0);
         }

      }

      public void onCompleted() {
         this.e = true;
         this.c();
      }

      public void onError(Throwable var1) {
         this.d = var1;
         this.e = true;
         this.c();
      }

      public void onNext(Object var1) {
         this.c.set(b.a(var1));
         this.c();
      }
   }

   abstract static class f extends c.a {
      public f(j var1) {
         super(var1);
      }

      abstract void c();

      public void onNext(Object var1) {
         if(!this.a.isUnsubscribed()) {
            if(this.get() != 0L) {
               this.a.onNext(var1);
               a.b(this, 1L);
            } else {
               this.c();
            }
         }

      }
   }

   static final class g extends c.a {
      public g(j var1) {
         super(var1);
      }

      public void onNext(Object var1) {
         if(!this.a.isUnsubscribed()) {
            this.a.onNext(var1);

            long var2;
            do {
               var2 = this.get();
            } while(var2 != 0L && !this.compareAndSet(var2, var2 - 1L));
         }

      }
   }
}
