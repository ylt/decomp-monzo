package rx.b.a;

import rx.j;

public final class e implements rx.d.a {
   final rx.g a;
   final rx.d b;
   final boolean c;

   public e(rx.d var1, rx.g var2, boolean var3) {
      this.a = var2;
      this.b = var1;
      this.c = var3;
   }

   public void a(j var1) {
      rx.g.a var2 = this.a.a();
      e.a var3 = new e.a(var1, this.c, var2, this.b);
      var1.add(var3);
      var1.add(var2);
      var2.a(var3);
   }

   // $FF: synthetic method
   public void call(Object var1) {
      this.a((j)var1);
   }

   static final class a extends j implements rx.a.a {
      final j a;
      final boolean b;
      final rx.g.a c;
      rx.d d;
      Thread e;

      a(j var1, boolean var2, rx.g.a var3, rx.d var4) {
         this.a = var1;
         this.b = var2;
         this.c = var3;
         this.d = var4;
      }

      public void a() {
         rx.d var1 = this.d;
         this.d = null;
         this.e = Thread.currentThread();
         var1.a((j)this);
      }

      public void onCompleted() {
         try {
            this.a.onCompleted();
         } finally {
            this.c.unsubscribe();
         }

      }

      public void onError(Throwable var1) {
         try {
            this.a.onError(var1);
         } finally {
            this.c.unsubscribe();
         }

      }

      public void onNext(Object var1) {
         this.a.onNext(var1);
      }

      public void setProducer(final rx.f var1) {
         this.a.setProducer(new rx.f() {
            public void request(final long var1x) {
               if(a.this.e != Thread.currentThread() && a.this.b) {
                  a.this.c.a(new rx.a.a() {
                     public void a() {
                        var1.request(var1x);
                     }
                  });
               } else {
                  var1.request(var1x);
               }

            }
         });
      }
   }
}
