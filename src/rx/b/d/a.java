package rx.b.d;

import java.util.concurrent.atomic.AtomicReference;
import rx.k;

public final class a extends AtomicReference implements k {
   public boolean isUnsubscribed() {
      boolean var1;
      if(this.get() == b.a) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public void unsubscribe() {
      if((k)this.get() != b.a) {
         k var1 = (k)this.getAndSet(b.a);
         if(var1 != null && var1 != b.a) {
            var1.unsubscribe();
         }
      }

   }
}
