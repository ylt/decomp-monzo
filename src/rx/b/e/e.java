package rx.b.e;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import rx.k;

public final class e implements k {
   private List a;
   private volatile boolean b;

   public e() {
   }

   public e(k var1) {
      this.a = new LinkedList();
      this.a.add(var1);
   }

   public e(k... var1) {
      this.a = new LinkedList(Arrays.asList(var1));
   }

   private static void a(Collection var0) {
      if(var0 != null) {
         k var1 = null;
         Iterator var2 = var0.iterator();
         ArrayList var4 = var1;

         while(var2.hasNext()) {
            var1 = (k)var2.next();

            try {
               var1.unsubscribe();
            } catch (Throwable var3) {
               if(var4 == null) {
                  var4 = new ArrayList();
               }

               var4.add(var3);
            }
         }

         rx.exceptions.a.a((List)var4);
      }

   }

   public void a(k param1) {
      // $FF: Couldn't be decompiled
   }

   public void b(k param1) {
      // $FF: Couldn't be decompiled
   }

   public boolean isUnsubscribed() {
      return this.b;
   }

   public void unsubscribe() {
      // $FF: Couldn't be decompiled
   }
}
