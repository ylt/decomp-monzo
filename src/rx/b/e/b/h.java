package rx.b.e.b;

import java.lang.reflect.Field;
import sun.misc.Unsafe;

public final class h {
   public static final Unsafe a;
   private static final boolean b;

   static {
      boolean var0 = true;
      if(System.getProperty("rx.unsafe-disable") == null) {
         var0 = false;
      }

      b = var0;

      Unsafe var1;
      try {
         Field var3 = Unsafe.class.getDeclaredField("theUnsafe");
         var3.setAccessible(true);
         var1 = (Unsafe)var3.get((Object)null);
      } catch (Throwable var2) {
         var1 = null;
      }

      a = var1;
   }

   public static boolean a() {
      boolean var0;
      if(a != null && !b) {
         var0 = true;
      } else {
         var0 = false;
      }

      return var0;
   }
}
