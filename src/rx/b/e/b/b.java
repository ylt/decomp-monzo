package rx.b.e.b;

import java.lang.reflect.Field;
import java.util.Iterator;

public class b extends d {
   static final int a = Integer.getInteger("jctools.spsc.max.lookahead.step", 4096).intValue();
   private static final long j;
   private static final long k;
   private static final long l;
   private static final int m;
   private static final Object n = new Object();

   static {
      int var0 = h.a.arrayIndexScale(Object[].class);
      if(4 == var0) {
         m = 2;
      } else {
         if(8 != var0) {
            throw new IllegalStateException("Unknown pointer size");
         }

         m = 3;
      }

      l = (long)h.a.arrayBaseOffset(Object[].class);

      Field var1;
      try {
         var1 = g.class.getDeclaredField("i");
         j = h.a.objectFieldOffset(var1);
      } catch (NoSuchFieldException var4) {
         InternalError var2 = new InternalError();
         var2.initCause(var4);
         throw var2;
      }

      try {
         var1 = d.class.getDeclaredField("d");
         k = h.a.objectFieldOffset(var1);
      } catch (NoSuchFieldException var3) {
         InternalError var5 = new InternalError();
         var5.initCause(var3);
         throw var5;
      }
   }

   public b(int var1) {
      var1 = a.a(var1);
      long var2 = (long)(var1 - 1);
      Object[] var4 = (Object[])(new Object[var1 + 1]);
      this.h = var4;
      this.g = var2;
      this.a(var1);
      this.c = var4;
      this.b = var2;
      this.f = var2 - 1L;
      this.a(0L);
   }

   private long a() {
      return h.a.getLongVolatile(this, j);
   }

   private static long a(long var0, long var2) {
      return c(var0 & var2);
   }

   private static Object a(Object[] var0, long var1) {
      return h.a.getObjectVolatile(var0, var1);
   }

   private Object a(Object[] var1, long var2, long var4) {
      Object var7 = null;
      this.c = var1;
      var4 = a(var2, var4);
      Object var6 = a(var1, var4);
      Object var8;
      if(var6 == null) {
         var8 = var7;
      } else {
         a(var1, var4, (Object)null);
         this.b(1L + var2);
         var8 = var6;
      }

      return var8;
   }

   private void a(int var1) {
      this.e = Math.min(var1 / 4, a);
   }

   private void a(long var1) {
      h.a.putOrderedLong(this, j, var1);
   }

   private void a(Object[] var1, long var2, long var4, Object var6, long var7) {
      Object[] var9 = (Object[])(new Object[var1.length]);
      this.h = var9;
      this.f = var2 + var7 - 1L;
      a(var9, var4, var6);
      this.a(var1, var9);
      a(var1, var4, n);
      this.a(var2 + 1L);
   }

   private static void a(Object[] var0, long var1, Object var3) {
      h.a.putOrderedObject(var0, var1, var3);
   }

   private void a(Object[] var1, Object[] var2) {
      a(var1, c((long)(var1.length - 1)), var2);
   }

   private boolean a(Object[] var1, Object var2, long var3, long var5) {
      a(var1, var5, var2);
      this.a(1L + var3);
      return true;
   }

   private Object[] a(Object[] var1) {
      return (Object[])((Object[])a(var1, c((long)(var1.length - 1))));
   }

   private long b() {
      return h.a.getLongVolatile(this, k);
   }

   private Object b(Object[] var1, long var2, long var4) {
      this.c = var1;
      return a(var1, a(var2, var4));
   }

   private void b(long var1) {
      h.a.putOrderedLong(this, k, var1);
   }

   private static long c(long var0) {
      return l + (var0 << m);
   }

   public final Iterator iterator() {
      throw new UnsupportedOperationException();
   }

   public final boolean offer(Object var1) {
      if(var1 == null) {
         throw new NullPointerException("Null is not a valid element");
      } else {
         Object[] var10 = this.h;
         long var4 = this.i;
         long var6 = this.g;
         long var8 = a(var4, var6);
         boolean var3;
         if(var4 < this.f) {
            var3 = this.a(var10, var1, var4, var8);
         } else {
            int var2 = this.e;
            if(a(var10, a((long)var2 + var4, var6)) == null) {
               this.f = (long)var2 + var4 - 1L;
               var3 = this.a(var10, var1, var4, var8);
            } else if(a(var10, a(1L + var4, var6)) != null) {
               var3 = this.a(var10, var1, var4, var8);
            } else {
               this.a(var10, var4, var8, var1, var6);
               var3 = true;
            }
         }

         return var3;
      }
   }

   public final Object peek() {
      Object[] var7 = this.c;
      long var1 = this.d;
      long var3 = this.b;
      Object var6 = a(var7, a(var1, var3));
      Object var5 = var6;
      if(var6 == n) {
         var5 = this.b(this.a(var7), var1, var3);
      }

      return var5;
   }

   public final Object poll() {
      Object[] var9 = this.c;
      long var4 = this.d;
      long var2 = this.b;
      long var6 = a(var4, var2);
      Object var8 = a(var9, var6);
      boolean var1;
      if(var8 == n) {
         var1 = true;
      } else {
         var1 = false;
      }

      if(var8 != null && !var1) {
         a(var9, var6, (Object)null);
         this.b(var4 + 1L);
      } else if(var1) {
         var8 = this.a(this.a(var9), var4, var2);
      } else {
         var8 = null;
      }

      return var8;
   }

   public final int size() {
      long var1 = this.b();

      while(true) {
         long var5 = this.a();
         long var3 = this.b();
         if(var1 == var3) {
            return (int)(var5 - var3);
         }

         var1 = var3;
      }
   }
}
