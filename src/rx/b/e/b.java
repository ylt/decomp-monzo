package rx.b.e;

import java.util.Queue;
import rx.k;

public class b implements k {
   public static final int a;
   private Queue b;

   static {
      short var0 = 128;
      if(a.a()) {
         var0 = 16;
      }

      String var2 = System.getProperty("rx.ring-buffer.size");
      int var1 = var0;
      if(var2 != null) {
         try {
            var1 = Integer.parseInt(var2);
         } catch (NumberFormatException var4) {
            System.err.println("Failed to set 'rx.buffer.size' with value " + var2 + " => " + var4.getMessage());
            var1 = var0;
         }
      }

      a = var1;
   }

   public void a() {
      synchronized(this){}
   }

   public boolean isUnsubscribed() {
      boolean var1;
      if(this.b == null) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public void unsubscribe() {
      this.a();
   }
}
