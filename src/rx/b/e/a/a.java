package rx.b.e.a;

import java.util.Collection;
import java.util.Iterator;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReferenceArray;

public final class a implements Queue {
   static final int a = Integer.getInteger("jctools.spsc.max.lookahead.step", 4096).intValue();
   private static final Object j = new Object();
   final AtomicLong b;
   int c;
   long d;
   int e;
   AtomicReferenceArray f;
   int g;
   AtomicReferenceArray h;
   final AtomicLong i;

   public a(int var1) {
      int var2 = rx.b.e.b.a.a(Math.max(8, var1));
      var1 = var2 - 1;
      this.b = new AtomicLong();
      this.i = new AtomicLong();
      AtomicReferenceArray var3 = new AtomicReferenceArray(var2 + 1);
      this.f = var3;
      this.e = var1;
      this.a(var2);
      this.h = var3;
      this.g = var1;
      this.d = (long)(var1 - 1);
      this.a(0L);
   }

   private static int a(long var0, int var2) {
      return b((int)var0 & var2);
   }

   private long a() {
      return this.b.get();
   }

   private static Object a(AtomicReferenceArray var0, int var1) {
      return var0.get(var1);
   }

   private Object a(AtomicReferenceArray var1, long var2, int var4) {
      Object var6 = null;
      this.h = var1;
      var4 = a(var2, var4);
      Object var5 = a(var1, var4);
      Object var7;
      if(var5 == null) {
         var7 = var6;
      } else {
         this.b(1L + var2);
         a(var1, var4, (Object)null);
         var7 = var5;
      }

      return var7;
   }

   private AtomicReferenceArray a(AtomicReferenceArray var1) {
      return (AtomicReferenceArray)a(var1, b(var1.length() - 1));
   }

   private void a(int var1) {
      this.c = Math.min(var1 / 4, a);
   }

   private void a(long var1) {
      this.b.lazySet(var1);
   }

   private static void a(AtomicReferenceArray var0, int var1, Object var2) {
      var0.lazySet(var1, var2);
   }

   private void a(AtomicReferenceArray var1, long var2, int var4, Object var5, long var6) {
      AtomicReferenceArray var8 = new AtomicReferenceArray(var1.length());
      this.f = var8;
      this.d = var2 + var6 - 1L;
      this.a(var2 + 1L);
      a(var8, var4, var5);
      this.a(var1, var8);
      a(var1, var4, j);
   }

   private void a(AtomicReferenceArray var1, AtomicReferenceArray var2) {
      a(var1, b(var1.length() - 1), var2);
   }

   private boolean a(AtomicReferenceArray var1, Object var2, long var3, int var5) {
      this.a(1L + var3);
      a(var1, var5, var2);
      return true;
   }

   private static int b(int var0) {
      return var0;
   }

   private long b() {
      return this.i.get();
   }

   private Object b(AtomicReferenceArray var1, long var2, int var4) {
      this.h = var1;
      return a(var1, a(var2, var4));
   }

   private void b(long var1) {
      this.i.lazySet(var1);
   }

   private long c() {
      return this.b.get();
   }

   private long d() {
      return this.i.get();
   }

   public boolean add(Object var1) {
      throw new UnsupportedOperationException();
   }

   public boolean addAll(Collection var1) {
      throw new UnsupportedOperationException();
   }

   public void clear() {
      while(this.poll() != null || !this.isEmpty()) {
         ;
      }

   }

   public boolean contains(Object var1) {
      throw new UnsupportedOperationException();
   }

   public boolean containsAll(Collection var1) {
      throw new UnsupportedOperationException();
   }

   public Object element() {
      throw new UnsupportedOperationException();
   }

   public boolean isEmpty() {
      boolean var1;
      if(this.a() == this.b()) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public Iterator iterator() {
      throw new UnsupportedOperationException();
   }

   public boolean offer(Object var1) {
      if(var1 == null) {
         throw new NullPointerException();
      } else {
         AtomicReferenceArray var8 = this.f;
         long var6 = this.c();
         int var2 = this.e;
         int var3 = a(var6, var2);
         boolean var5;
         if(var6 < this.d) {
            var5 = this.a(var8, var1, var6, var3);
         } else {
            int var4 = this.c;
            if(a(var8, a((long)var4 + var6, var2)) == null) {
               this.d = (long)var4 + var6 - 1L;
               var5 = this.a(var8, var1, var6, var3);
            } else if(a(var8, a(1L + var6, var2)) != null) {
               var5 = this.a(var8, var1, var6, var3);
            } else {
               this.a(var8, var6, var3, var1, (long)var2);
               var5 = true;
            }
         }

         return var5;
      }
   }

   public Object peek() {
      AtomicReferenceArray var6 = this.h;
      long var2 = this.d();
      int var1 = this.g;
      Object var5 = a(var6, a(var2, var1));
      Object var4 = var5;
      if(var5 == j) {
         var4 = this.b(this.a(var6), var2, var1);
      }

      return var4;
   }

   public Object poll() {
      AtomicReferenceArray var7 = this.h;
      long var4 = this.d();
      int var2 = this.g;
      int var3 = a(var4, var2);
      Object var6 = a(var7, var3);
      boolean var1;
      if(var6 == j) {
         var1 = true;
      } else {
         var1 = false;
      }

      if(var6 != null && !var1) {
         this.b(var4 + 1L);
         a(var7, var3, (Object)null);
      } else if(var1) {
         var6 = this.a(this.a(var7), var4, var2);
      } else {
         var6 = null;
      }

      return var6;
   }

   public Object remove() {
      throw new UnsupportedOperationException();
   }

   public boolean remove(Object var1) {
      throw new UnsupportedOperationException();
   }

   public boolean removeAll(Collection var1) {
      throw new UnsupportedOperationException();
   }

   public boolean retainAll(Collection var1) {
      throw new UnsupportedOperationException();
   }

   public int size() {
      long var1 = this.b();

      while(true) {
         long var5 = this.a();
         long var3 = this.b();
         if(var1 == var3) {
            return (int)(var5 - var3);
         }

         var1 = var3;
      }
   }

   public Object[] toArray() {
      throw new UnsupportedOperationException();
   }

   public Object[] toArray(Object[] var1) {
      throw new UnsupportedOperationException();
   }
}
