package rx.b.e;

import java.util.concurrent.atomic.AtomicBoolean;
import rx.f;
import rx.g;
import rx.j;
import rx.k;

public final class d extends rx.d {
   static final boolean c = Boolean.valueOf(System.getProperty("rx.just.strong-mode", "false")).booleanValue();
   final Object b;

   public rx.d b(final g var1) {
      rx.a.c var2;
      if(var1 instanceof rx.b.c.a) {
         var2 = new rx.a.c((rx.b.c.a)var1) {
            // $FF: synthetic field
            final rx.b.c.a a;

            {
               this.a = var2;
            }

            public k a(rx.a.a var1) {
               return this.a.a(var1);
            }
         };
      } else {
         var2 = new rx.a.c() {
            public k a(final rx.a.a var1x) {
               final g.a var2 = var1.a();
               var2.a(new rx.a.a() {
                  public void a() {
                     try {
                        var1x.a();
                     } finally {
                        var2.unsubscribe();
                     }

                  }
               });
               return var2;
            }
         };
      }

      return b(new d.a(this.b, var2));
   }

   static final class a implements rx.d.a {
      final Object a;
      final rx.a.c b;

      a(Object var1, rx.a.c var2) {
         this.a = var1;
         this.b = var2;
      }

      public void a(j var1) {
         var1.setProducer(new d.b(var1, this.a, this.b));
      }

      // $FF: synthetic method
      public void call(Object var1) {
         this.a((j)var1);
      }
   }

   static final class b extends AtomicBoolean implements rx.a.a, f {
      final j a;
      final Object b;
      final rx.a.c c;

      public b(j var1, Object var2, rx.a.c var3) {
         this.a = var1;
         this.b = var2;
         this.c = var3;
      }

      public void a() {
         j var2 = this.a;
         if(!var2.isUnsubscribed()) {
            Object var3 = this.b;

            try {
               var2.onNext(var3);
            } catch (Throwable var4) {
               rx.exceptions.a.a(var4, var2, var3);
               return;
            }

            if(!var2.isUnsubscribed()) {
               var2.onCompleted();
            }
         }

      }

      public void request(long var1) {
         if(var1 < 0L) {
            throw new IllegalArgumentException("n >= 0 required but it was " + var1);
         } else {
            if(var1 != 0L && this.compareAndSet(false, true)) {
               this.a.add((k)this.c.a(this));
            }

         }
      }

      public String toString() {
         return "ScalarAsyncProducer[" + this.b + ", " + this.get() + "]";
      }
   }
}
