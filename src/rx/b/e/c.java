package rx.b.e;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicLong;

public final class c extends AtomicLong implements ThreadFactory {
   public static final ThreadFactory a = new ThreadFactory() {
      public Thread newThread(Runnable var1) {
         throw new AssertionError("No threads allowed.");
      }
   };
   final String b;

   public c(String var1) {
      this.b = var1;
   }

   public Thread newThread(Runnable var1) {
      Thread var2 = new Thread(var1, this.b + this.incrementAndGet());
      var2.setDaemon(true);
      return var2;
   }
}
