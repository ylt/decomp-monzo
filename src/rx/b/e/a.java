package rx.b.e;

public final class a {
   private static final int a = c();
   private static final boolean b;

   static {
      boolean var0;
      if(a != 0) {
         var0 = true;
      } else {
         var0 = false;
      }

      b = var0;
   }

   public static boolean a() {
      return b;
   }

   public static int b() {
      return a;
   }

   private static int c() {
      int var0;
      try {
         var0 = ((Integer)Class.forName("android.os.Build$VERSION").getField("SDK_INT").get((Object)null)).intValue();
      } catch (Exception var2) {
         var0 = 0;
      }

      return var0;
   }
}
