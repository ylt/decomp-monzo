package rx;

public abstract class j implements e, k {
   private static final long NOT_SET = Long.MIN_VALUE;
   private f producer;
   private long requested;
   private final j subscriber;
   private final rx.b.e.e subscriptions;

   protected j() {
      this((j)null, false);
   }

   protected j(j var1) {
      this(var1, true);
   }

   protected j(j var1, boolean var2) {
      this.requested = Long.MIN_VALUE;
      this.subscriber = var1;
      rx.b.e.e var3;
      if(var2 && var1 != null) {
         var3 = var1.subscriptions;
      } else {
         var3 = new rx.b.e.e();
      }

      this.subscriptions = var3;
   }

   private void addToRequested(long var1) {
      if(this.requested == Long.MIN_VALUE) {
         this.requested = var1;
      } else {
         var1 += this.requested;
         if(var1 < 0L) {
            this.requested = Long.MAX_VALUE;
         } else {
            this.requested = var1;
         }
      }

   }

   public final void add(k var1) {
      this.subscriptions.a(var1);
   }

   public final boolean isUnsubscribed() {
      return this.subscriptions.isUnsubscribed();
   }

   public void onStart() {
   }

   protected final void request(long param1) {
      // $FF: Couldn't be decompiled
   }

   public void setProducer(f param1) {
      // $FF: Couldn't be decompiled
   }

   public final void unsubscribe() {
      this.subscriptions.unsubscribe();
   }
}
