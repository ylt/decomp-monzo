package rx;

public abstract class i implements k {
   private final rx.b.e.e a = new rx.b.e.e();

   public abstract void a(Object var1);

   public abstract void a(Throwable var1);

   public final void a(k var1) {
      this.a.a(var1);
   }

   public final boolean isUnsubscribed() {
      return this.a.isUnsubscribed();
   }

   public final void unsubscribe() {
      this.a.unsubscribe();
   }
}
