package rx.exceptions;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;

public final class CompositeException extends RuntimeException {
   private final List a;
   private final String b;
   private Throwable c;

   @Deprecated
   public CompositeException(String var1, Collection var2) {
      LinkedHashSet var5 = new LinkedHashSet();
      ArrayList var3 = new ArrayList();
      if(var2 != null) {
         Iterator var6 = var2.iterator();

         while(var6.hasNext()) {
            Throwable var4 = (Throwable)var6.next();
            if(var4 instanceof CompositeException) {
               var5.addAll(((CompositeException)var4).a());
            } else if(var4 != null) {
               var5.add(var4);
            } else {
               var5.add(new NullPointerException());
            }
         }
      } else {
         var5.add(new NullPointerException());
      }

      var3.addAll(var5);
      this.a = Collections.unmodifiableList(var3);
      this.b = this.a.size() + " exceptions occurred. ";
   }

   public CompositeException(Collection var1) {
      this((String)null, var1);
   }

   public CompositeException(Throwable... var1) {
      LinkedHashSet var6 = new LinkedHashSet();
      ArrayList var5 = new ArrayList();
      if(var1 != null) {
         int var3 = var1.length;

         for(int var2 = 0; var2 < var3; ++var2) {
            Throwable var4 = var1[var2];
            if(var4 instanceof CompositeException) {
               var6.addAll(((CompositeException)var4).a());
            } else if(var4 != null) {
               var6.add(var4);
            } else {
               var6.add(new NullPointerException());
            }
         }
      } else {
         var6.add(new NullPointerException());
      }

      var5.addAll(var6);
      this.a = Collections.unmodifiableList(var5);
      this.b = this.a.size() + " exceptions occurred. ";
   }

   private List a(Throwable var1) {
      ArrayList var4 = new ArrayList();
      Throwable var3 = var1.getCause();
      if(var3 != null) {
         Throwable var2 = var3;
         if(var3 != var1) {
            while(true) {
               var4.add(var2);
               var1 = var2.getCause();
               if(var1 == null || var1 == var2) {
                  break;
               }

               var2 = var2.getCause();
            }
         }
      }

      return var4;
   }

   private void a(StringBuilder var1, Throwable var2, String var3) {
      var1.append(var3).append(var2).append('\n');
      StackTraceElement[] var7 = var2.getStackTrace();
      int var5 = var7.length;

      for(int var4 = 0; var4 < var5; ++var4) {
         StackTraceElement var6 = var7[var4];
         var1.append("\t\tat ").append(var6).append('\n');
      }

      if(var2.getCause() != null) {
         var1.append("\tCaused by: ");
         this.a(var1, var2.getCause(), "");
      }

   }

   private void a(CompositeException.b param1) {
      // $FF: Couldn't be decompiled
   }

   private Throwable b(Throwable var1) {
      Throwable var3 = var1.getCause();
      if(var3 != null) {
         Throwable var2 = var3;
         if(var3 != var1) {
            while(true) {
               var3 = var2.getCause();
               var1 = var2;
               if(var3 == null) {
                  break;
               }

               if(var3 == var2) {
                  var1 = var2;
                  break;
               }

               var2 = var2.getCause();
            }
         }
      }

      return var1;
   }

   public List a() {
      return this.a;
   }

   public Throwable getCause() {
      // $FF: Couldn't be decompiled
   }

   public String getMessage() {
      return this.b;
   }

   public void printStackTrace() {
      this.printStackTrace(System.err);
   }

   public void printStackTrace(PrintStream var1) {
      this.a((CompositeException.b)(new CompositeException.c(var1)));
   }

   public void printStackTrace(PrintWriter var1) {
      this.a((CompositeException.b)(new CompositeException.d(var1)));
   }

   static final class a extends RuntimeException {
      public String getMessage() {
         return "Chain of Causes for CompositeException In Order Received =>";
      }
   }

   abstract static class b {
      abstract Object a();

      abstract void a(Object var1);
   }

   static final class c extends CompositeException.b {
      private final PrintStream a;

      c(PrintStream var1) {
         this.a = var1;
      }

      Object a() {
         return this.a;
      }

      void a(Object var1) {
         this.a.println(var1);
      }
   }

   static final class d extends CompositeException.b {
      private final PrintWriter a;

      d(PrintWriter var1) {
         this.a = var1;
      }

      Object a() {
         return this.a;
      }

      void a(Object var1) {
         this.a.println(var1);
      }
   }
}
