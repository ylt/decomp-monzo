package rx.exceptions;

public class MissingBackpressureException extends Exception {
   public MissingBackpressureException() {
   }

   public MissingBackpressureException(String var1) {
      super(var1);
   }
}
