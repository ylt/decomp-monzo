package rx.exceptions;

public class OnErrorFailedException extends RuntimeException {
   public OnErrorFailedException(String var1, Throwable var2) {
      if(var2 == null) {
         var2 = new NullPointerException();
      }

      super(var1, (Throwable)var2);
   }
}
