package rx.exceptions;

public final class AssemblyStackTraceException extends RuntimeException {
   public Throwable fillInStackTrace() {
      synchronized(this){}
      return this;
   }
}
