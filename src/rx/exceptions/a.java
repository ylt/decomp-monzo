package rx.exceptions;

import java.util.HashSet;
import java.util.List;
import rx.e;
import rx.i;

public final class a {
   public static void a(Throwable var0) {
      if(var0 instanceof OnErrorNotImplementedException) {
         throw (OnErrorNotImplementedException)var0;
      } else if(var0 instanceof OnErrorFailedException) {
         throw (OnErrorFailedException)var0;
      } else if(var0 instanceof OnCompletedFailedException) {
         throw (OnCompletedFailedException)var0;
      } else if(var0 instanceof VirtualMachineError) {
         throw (VirtualMachineError)var0;
      } else if(var0 instanceof ThreadDeath) {
         throw (ThreadDeath)var0;
      } else if(var0 instanceof LinkageError) {
         throw (LinkageError)var0;
      }
   }

   public static void a(Throwable var0, Throwable var1) {
      HashSet var4 = new HashSet();
      int var2 = 0;

      while(true) {
         Throwable var3 = var0;
         if(var0.getCause() != null) {
            if(var2 >= 25) {
               break;
            }

            var0 = var0.getCause();
            if(!var4.contains(var0.getCause())) {
               var4.add(var0.getCause());
               ++var2;
               continue;
            }

            var3 = var0;
         }

         try {
            var3.initCause(var1);
         } catch (Throwable var5) {
            ;
         }
         break;
      }

   }

   public static void a(Throwable var0, e var1, Object var2) {
      a(var0);
      var1.onError(OnErrorThrowable.a(var0, var2));
   }

   public static void a(Throwable var0, i var1) {
      a(var0);
      var1.a(var0);
   }

   public static void a(List var0) {
      if(var0 != null && !var0.isEmpty()) {
         if(var0.size() == 1) {
            Throwable var1 = (Throwable)var0.get(0);
            if(var1 instanceof RuntimeException) {
               throw (RuntimeException)var1;
            } else if(var1 instanceof Error) {
               throw (Error)var1;
            } else {
               throw new RuntimeException(var1);
            }
         } else {
            throw new CompositeException(var0);
         }
      }
   }

   public static Throwable b(Throwable var0) {
      int var1 = 0;

      Object var2;
      while(true) {
         var2 = var0;
         if(var0.getCause() == null) {
            break;
         }

         if(var1 >= 25) {
            var2 = new RuntimeException("Stack too deep to get final cause");
            break;
         }

         var0 = var0.getCause();
         ++var1;
      }

      return (Throwable)var2;
   }
}
