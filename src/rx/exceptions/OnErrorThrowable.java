package rx.exceptions;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import rx.c.f;

public final class OnErrorThrowable extends RuntimeException {
   public static Throwable a(Throwable var0, Object var1) {
      Object var2 = var0;
      if(var0 == null) {
         var2 = new NullPointerException();
      }

      var0 = a.b((Throwable)var2);
      if(!(var0 instanceof OnErrorThrowable.OnNextValue) || ((OnErrorThrowable.OnNextValue)var0).a() != var1) {
         a.a((Throwable)var2, (Throwable)(new OnErrorThrowable.OnNextValue(var1)));
      }

      return (Throwable)var2;
   }

   public static class OnNextValue extends RuntimeException {
      private final Object a;

      public OnNextValue(Object var1) {
         super("OnError while emitting onNext value: " + a(var1));
         if(!(var1 instanceof Serializable)) {
            try {
               var1 = String.valueOf(var1);
            } catch (Throwable var2) {
               var1 = var2.getMessage();
            }
         }

         this.a = var1;
      }

      static String a(Object var0) {
         String var2;
         if(var0 == null) {
            var2 = "null";
         } else if(OnErrorThrowable.a.a.contains(var0.getClass())) {
            var2 = var0.toString();
         } else if(var0 instanceof String) {
            var2 = (String)var0;
         } else if(var0 instanceof Enum) {
            var2 = ((Enum)var0).name();
         } else {
            String var1 = f.a().b().a(var0);
            if(var1 != null) {
               var2 = var1;
            } else {
               var2 = var0.getClass().getName() + ".class";
            }
         }

         return var2;
      }

      public Object a() {
         return this.a;
      }
   }

   static final class a {
      static final Set a = a();

      private static Set a() {
         HashSet var0 = new HashSet();
         var0.add(Boolean.class);
         var0.add(Character.class);
         var0.add(Byte.class);
         var0.add(Short.class);
         var0.add(Integer.class);
         var0.add(Long.class);
         var0.add(Float.class);
         var0.add(Double.class);
         return var0;
      }
   }
}
