package rx;

public class a {
   static final a a = new a(new a.a() {
      public void a(b var1) {
         var1.a(rx.d.c.a());
         var1.a();
      }

      // $FF: synthetic method
      public void call(Object var1) {
         this.a((b)var1);
      }
   }, false);
   static final a b = new a(new a.a() {
      public void a(b var1) {
         var1.a(rx.d.c.a());
      }

      // $FF: synthetic method
      public void call(Object var1) {
         this.a((b)var1);
      }
   }, false);
   private final a.a c;

   protected a(a.a var1) {
      this.c = rx.c.c.a(var1);
   }

   protected a(a.a var1, boolean var2) {
      a.a var3 = var1;
      if(var2) {
         var3 = rx.c.c.a(var1);
      }

      this.c = var3;
   }

   static NullPointerException a(Throwable var0) {
      NullPointerException var1 = new NullPointerException("Actually not, but can't pass out an exception otherwise...");
      var1.initCause(var0);
      return var1;
   }

   static Object a(Object var0) {
      if(var0 == null) {
         throw new NullPointerException();
      } else {
         return var0;
      }
   }

   public static a a(a.a var0) {
      a((Object)var0);

      try {
         a var3 = new a(var0);
         return var3;
      } catch (NullPointerException var1) {
         throw var1;
      } catch (Throwable var2) {
         rx.c.c.a(var2);
         throw a(var2);
      }
   }

   public static a a(final d var0) {
      a((Object)var0);
      return a(new a.a() {
         public void a(final b var1) {
            j var2 = new j() {
               public void onCompleted() {
                  var1.a();
               }

               public void onError(Throwable var1x) {
                  var1.a(var1x);
               }

               public void onNext(Object var1x) {
               }
            };
            var1.a((k)var2);
            var0.a(var2);
         }

         // $FF: synthetic method
         public void call(Object var1) {
            this.a((b)var1);
         }
      });
   }

   public interface a extends rx.a.b {
   }

   public interface b extends rx.a.c {
   }
}
