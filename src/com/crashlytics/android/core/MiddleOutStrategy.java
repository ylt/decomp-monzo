package com.crashlytics.android.core;

class MiddleOutStrategy implements StackTraceTrimmingStrategy {
   private final int trimmedSize;

   public MiddleOutStrategy(int var1) {
      this.trimmedSize = var1;
   }

   public StackTraceElement[] getTrimmedStackTrace(StackTraceElement[] var1) {
      if(var1.length > this.trimmedSize) {
         int var3 = this.trimmedSize / 2;
         int var2 = this.trimmedSize - var3;
         StackTraceElement[] var4 = new StackTraceElement[this.trimmedSize];
         System.arraycopy(var1, 0, var4, 0, var2);
         System.arraycopy(var1, var1.length - var3, var4, var2, var3);
         var1 = var4;
      }

      return var1;
   }
}
