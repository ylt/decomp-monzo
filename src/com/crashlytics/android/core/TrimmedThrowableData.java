package com.crashlytics.android.core;

class TrimmedThrowableData {
   public final TrimmedThrowableData cause;
   public final String className;
   public final String localizedMessage;
   public final StackTraceElement[] stacktrace;

   public TrimmedThrowableData(Throwable var1, StackTraceTrimmingStrategy var2) {
      this.localizedMessage = var1.getLocalizedMessage();
      this.className = var1.getClass().getName();
      this.stacktrace = var2.getTrimmedStackTrace(var1.getStackTrace());
      var1 = var1.getCause();
      TrimmedThrowableData var3;
      if(var1 != null) {
         var3 = new TrimmedThrowableData(var1, var2);
      } else {
         var3 = null;
      }

      this.cause = var3;
   }
}
