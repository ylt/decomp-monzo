package com.crashlytics.android.core;

class MiddleOutFallbackStrategy implements StackTraceTrimmingStrategy {
   private final int maximumStackSize;
   private final MiddleOutStrategy middleOutStrategy;
   private final StackTraceTrimmingStrategy[] trimmingStrategies;

   public MiddleOutFallbackStrategy(int var1, StackTraceTrimmingStrategy... var2) {
      this.maximumStackSize = var1;
      this.trimmingStrategies = var2;
      this.middleOutStrategy = new MiddleOutStrategy(var1);
   }

   public StackTraceElement[] getTrimmedStackTrace(StackTraceElement[] var1) {
      if(var1.length > this.maximumStackSize) {
         StackTraceTrimmingStrategy[] var5 = this.trimmingStrategies;
         int var3 = var5.length;
         int var2 = 0;

         StackTraceElement[] var4;
         for(var4 = var1; var2 < var3; ++var2) {
            StackTraceTrimmingStrategy var6 = var5[var2];
            if(var4.length <= this.maximumStackSize) {
               break;
            }

            var4 = var6.getTrimmedStackTrace(var1);
         }

         var1 = var4;
         if(var4.length > this.maximumStackSize) {
            var1 = this.middleOutStrategy.getTrimmedStackTrace(var4);
         }
      }

      return var1;
   }
}
