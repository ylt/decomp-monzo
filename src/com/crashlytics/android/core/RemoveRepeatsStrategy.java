package com.crashlytics.android.core;

import java.util.HashMap;

class RemoveRepeatsStrategy implements StackTraceTrimmingStrategy {
   private final int maxRepetitions;

   public RemoveRepeatsStrategy() {
      this(1);
   }

   public RemoveRepeatsStrategy(int var1) {
      this.maxRepetitions = var1;
   }

   private static boolean isRepeatingSequence(StackTraceElement[] var0, int var1, int var2) {
      boolean var6 = false;
      int var4 = var2 - var1;
      boolean var5;
      if(var2 + var4 > var0.length) {
         var5 = var6;
      } else {
         int var3 = 0;

         while(true) {
            if(var3 >= var4) {
               var5 = true;
               break;
            }

            var5 = var6;
            if(!var0[var1 + var3].equals(var0[var2 + var3])) {
               break;
            }

            ++var3;
         }
      }

      return var5;
   }

   private static StackTraceElement[] trimRepeats(StackTraceElement[] var0, int var1) {
      HashMap var9 = new HashMap();
      StackTraceElement[] var8 = new StackTraceElement[var0.length];
      int var2 = 0;
      int var4 = 1;

      int var3;
      int var5;
      for(var5 = 0; var2 < var0.length; var5 = var3) {
         StackTraceElement var11 = var0[var2];
         Integer var10 = (Integer)var9.get(var11);
         if(var10 != null && isRepeatingSequence(var0, var10.intValue(), var2)) {
            int var7 = var2 - var10.intValue();
            int var6 = var4;
            var3 = var5;
            if(var4 < var1) {
               System.arraycopy(var0, var2, var8, var5, var7);
               var3 = var5 + var7;
               var6 = var4 + 1;
            }

            var5 = var7 - 1 + var2;
            var4 = var6;
         } else {
            var8[var5] = var0[var2];
            var3 = var5 + 1;
            var5 = var2;
            var4 = 1;
         }

         var9.put(var11, Integer.valueOf(var2));
         var2 = var5 + 1;
      }

      var0 = new StackTraceElement[var5];
      System.arraycopy(var8, 0, var0, 0, var0.length);
      return var0;
   }

   public StackTraceElement[] getTrimmedStackTrace(StackTraceElement[] var1) {
      StackTraceElement[] var3 = trimRepeats(var1, this.maxRepetitions);
      StackTraceElement[] var2 = var1;
      if(var3.length < var1.length) {
         var2 = var3;
      }

      return var2;
   }
}
