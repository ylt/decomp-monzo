package com.crashlytics.android.core;

import io.fabric.sdk.android.services.network.f;
import java.io.InputStream;

class CrashlyticsPinningInfoProvider implements f {
   private final PinningInfoProvider pinningInfo;

   public CrashlyticsPinningInfoProvider(PinningInfoProvider var1) {
      this.pinningInfo = var1;
   }

   public String getKeyStorePassword() {
      return this.pinningInfo.getKeyStorePassword();
   }

   public InputStream getKeyStoreStream() {
      return this.pinningInfo.getKeyStoreStream();
   }

   public long getPinCreationTimeInMillis() {
      return -1L;
   }

   public String[] getPins() {
      return this.pinningInfo.getPins();
   }
}
