package com.crashlytics.android.core;

import io.fabric.sdk.android.c;
import io.fabric.sdk.android.services.b.h;
import java.io.File;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

class ReportUploader {
   static final Map HEADER_INVALID_CLS_FILE = Collections.singletonMap("X-CRASHLYTICS-INVALID-SESSION", "1");
   private static final short[] RETRY_INTERVALS = new short[]{10, 20, 30, 60, 120, 300};
   private final String apiKey;
   private final CreateReportSpiCall createReportCall;
   private final Object fileAccessLock = new Object();
   private final ReportUploader.HandlingExceptionCheck handlingExceptionCheck;
   private final ReportUploader.ReportFilesProvider reportFilesProvider;
   private Thread uploadThread;

   public ReportUploader(String var1, CreateReportSpiCall var2, ReportUploader.ReportFilesProvider var3, ReportUploader.HandlingExceptionCheck var4) {
      if(var2 == null) {
         throw new IllegalArgumentException("createReportCall must not be null.");
      } else {
         this.createReportCall = var2;
         this.apiKey = var1;
         this.reportFilesProvider = var3;
         this.handlingExceptionCheck = var4;
      }
   }

   List findReports() {
      // $FF: Couldn't be decompiled
   }

   boolean forceUpload(Report param1) {
      // $FF: Couldn't be decompiled
   }

   boolean isUploading() {
      boolean var1;
      if(this.uploadThread != null) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public void uploadReports(float var1, ReportUploader.SendCheck var2) {
      synchronized(this){}

      try {
         if(this.uploadThread != null) {
            c.h().a("CrashlyticsCore", "Report upload has already been started.");
         } else {
            ReportUploader.Worker var3 = new ReportUploader.Worker(var1, var2);
            Thread var6 = new Thread(var3, "Crashlytics Report Uploader");
            this.uploadThread = var6;
            this.uploadThread.start();
         }
      } finally {
         ;
      }

   }

   static final class AlwaysSendCheck implements ReportUploader.SendCheck {
      public boolean canSendReports() {
         return true;
      }
   }

   interface HandlingExceptionCheck {
      boolean isHandlingException();
   }

   interface ReportFilesProvider {
      File[] getCompleteSessionFiles();

      File[] getInvalidSessionFiles();
   }

   interface SendCheck {
      boolean canSendReports();
   }

   private class Worker extends h {
      private final float delay;
      private final ReportUploader.SendCheck sendCheck;

      Worker(float var2, ReportUploader.SendCheck var3) {
         this.delay = var2;
         this.sendCheck = var3;
      }

      private void attemptUploadWithRetry() {
         c.h().a("CrashlyticsCore", "Starting report processing in " + this.delay + " second(s)...");
         if(this.delay > 0.0F) {
            try {
               Thread.sleep((long)(this.delay * 1000.0F));
            } catch (InterruptedException var7) {
               Thread.currentThread().interrupt();
               return;
            }
         }

         List var4 = ReportUploader.this.findReports();
         if(!ReportUploader.this.handlingExceptionCheck.isHandlingException()) {
            if(!var4.isEmpty() && !this.sendCheck.canSendReports()) {
               c.h().a("CrashlyticsCore", "User declined to send. Removing " + var4.size() + " Report(s).");
               Iterator var9 = var4.iterator();

               while(var9.hasNext()) {
                  ((Report)var9.next()).remove();
               }
            } else {
               int var1 = 0;

               while(!var4.isEmpty() && !ReportUploader.this.handlingExceptionCheck.isHandlingException()) {
                  c.h().a("CrashlyticsCore", "Attempting to send " + var4.size() + " report(s)");
                  Iterator var5 = var4.iterator();

                  while(var5.hasNext()) {
                     Report var8 = (Report)var5.next();
                     ReportUploader.this.forceUpload(var8);
                  }

                  var4 = ReportUploader.this.findReports();
                  if(!var4.isEmpty()) {
                     long var2 = (long)ReportUploader.RETRY_INTERVALS[Math.min(var1, ReportUploader.RETRY_INTERVALS.length - 1)];
                     c.h().a("CrashlyticsCore", "Report submisson: scheduling delayed retry in " + var2 + " seconds");

                     try {
                        Thread.sleep(var2 * 1000L);
                     } catch (InterruptedException var6) {
                        Thread.currentThread().interrupt();
                        break;
                     }

                     ++var1;
                  }
               }
            }
         }

      }

      public void onRun() {
         try {
            this.attemptUploadWithRetry();
         } catch (Exception var2) {
            c.h().e("CrashlyticsCore", "An unexpected error occurred while attempting to upload crash reports.", var2);
         }

         ReportUploader.this.uploadThread = null;
      }
   }
}
