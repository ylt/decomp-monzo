package com.crashlytics.android.core;

import android.content.Context;
import android.util.Log;
import com.crashlytics.android.core.internal.CrashEventDataProvider;
import com.crashlytics.android.core.internal.models.SessionEventData;
import io.fabric.sdk.android.c;
import io.fabric.sdk.android.h;
import io.fabric.sdk.android.k;
import io.fabric.sdk.android.services.b.i;
import io.fabric.sdk.android.services.b.n;
import io.fabric.sdk.android.services.concurrency.d;
import io.fabric.sdk.android.services.concurrency.e;
import io.fabric.sdk.android.services.concurrency.g;
import io.fabric.sdk.android.services.concurrency.l;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.net.URL;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import javax.net.ssl.HttpsURLConnection;

@d(
   a = {CrashEventDataProvider.class}
)
public class CrashlyticsCore extends h {
   static final float CLS_DEFAULT_PROCESS_DELAY = 1.0F;
   static final String CRASHLYTICS_REQUIRE_BUILD_ID = "com.crashlytics.RequireBuildId";
   static final boolean CRASHLYTICS_REQUIRE_BUILD_ID_DEFAULT = true;
   static final String CRASH_MARKER_FILE_NAME = "crash_marker";
   static final int DEFAULT_MAIN_HANDLER_TIMEOUT_SEC = 4;
   private static final String INITIALIZATION_MARKER_FILE_NAME = "initialization_marker";
   static final int MAX_ATTRIBUTES = 64;
   static final int MAX_ATTRIBUTE_SIZE = 1024;
   private static final String MISSING_BUILD_ID_MSG = "This app relies on Crashlytics. Please sign up for access at https://fabric.io/sign_up,\ninstall an Android build tool and ask a team member to invite you to this app's organization.";
   private static final String PREFERENCE_STORE_NAME = "com.crashlytics.android.core.CrashlyticsCore";
   public static final String TAG = "CrashlyticsCore";
   private final ConcurrentHashMap attributes;
   private CrashlyticsBackgroundWorker backgroundWorker;
   private CrashlyticsController controller;
   private CrashlyticsFileMarker crashMarker;
   private float delay;
   private boolean disabled;
   private CrashEventDataProvider externalCrashEventDataProvider;
   private io.fabric.sdk.android.services.network.d httpRequestFactory;
   private CrashlyticsFileMarker initializationMarker;
   private CrashlyticsListener listener;
   private final PinningInfoProvider pinningInfo;
   private final long startTime;
   private String userEmail;
   private String userId;
   private String userName;

   public CrashlyticsCore() {
      this(1.0F, (CrashlyticsListener)null, (PinningInfoProvider)null, false);
   }

   CrashlyticsCore(float var1, CrashlyticsListener var2, PinningInfoProvider var3, boolean var4) {
      this(var1, var2, var3, var4, n.a("Crashlytics Exception Handler"));
   }

   CrashlyticsCore(float var1, CrashlyticsListener var2, PinningInfoProvider var3, boolean var4, ExecutorService var5) {
      this.userId = null;
      this.userEmail = null;
      this.userName = null;
      this.delay = var1;
      if(var2 == null) {
         var2 = new CrashlyticsCore.NoOpListener(null);
      }

      this.listener = (CrashlyticsListener)var2;
      this.pinningInfo = var3;
      this.disabled = var4;
      this.backgroundWorker = new CrashlyticsBackgroundWorker(var5);
      this.attributes = new ConcurrentHashMap();
      this.startTime = System.currentTimeMillis();
   }

   private void checkForPreviousCrash() {
      Boolean var1 = (Boolean)this.backgroundWorker.submitAndWait(new CrashlyticsCore.CrashMarkerCheck(this.crashMarker));
      if(Boolean.TRUE.equals(var1)) {
         try {
            this.listener.crashlyticsDidDetectCrashDuringPreviousExecution();
         } catch (Exception var2) {
            c.h().e("CrashlyticsCore", "Exception thrown by CrashlyticsListener while notifying of previous crash.", var2);
         }
      }

   }

   private void doLog(int var1, String var2, String var3) {
      if(!this.disabled && ensureFabricWithCalled("prior to logging messages.")) {
         long var6 = System.currentTimeMillis();
         long var4 = this.startTime;
         this.controller.writeToLog(var6 - var4, formatLogMessage(var1, var2, var3));
      }

   }

   private static boolean ensureFabricWithCalled(String var0) {
      CrashlyticsCore var2 = getInstance();
      boolean var1;
      if(var2 != null && var2.controller != null) {
         var1 = true;
      } else {
         c.h().e("CrashlyticsCore", "Crashlytics must be initialized by calling Fabric.with(Context) " + var0, (Throwable)null);
         var1 = false;
      }

      return var1;
   }

   private void finishInitSynchronously() {
      g var2 = new g() {
         public Void call() throws Exception {
            return CrashlyticsCore.this.doInBackground();
         }

         public e getPriority() {
            return e.d;
         }
      };
      Iterator var1 = this.getDependencies().iterator();

      while(var1.hasNext()) {
         var2.addDependency((l)var1.next());
      }

      Future var6 = this.getFabric().f().submit(var2);
      c.h().a("CrashlyticsCore", "Crashlytics detected incomplete initialization on previous app launch. Will initialize synchronously.");

      try {
         var6.get(4L, TimeUnit.SECONDS);
      } catch (InterruptedException var3) {
         c.h().e("CrashlyticsCore", "Crashlytics was interrupted during initialization.", var3);
      } catch (ExecutionException var4) {
         c.h().e("CrashlyticsCore", "Problem encountered during Crashlytics initialization.", var4);
      } catch (TimeoutException var5) {
         c.h().e("CrashlyticsCore", "Crashlytics timed out during initialization.", var5);
      }

   }

   private static String formatLogMessage(int var0, String var1, String var2) {
      return i.b(var0) + "/" + var1 + " " + var2;
   }

   public static CrashlyticsCore getInstance() {
      return (CrashlyticsCore)c.a(CrashlyticsCore.class);
   }

   static boolean isBuildIdValid(String var0, boolean var1) {
      boolean var2 = true;
      if(!var1) {
         c.h().a("CrashlyticsCore", "Configured not to require a build ID.");
         var1 = var2;
      } else {
         var1 = var2;
         if(i.c(var0)) {
            Log.e("CrashlyticsCore", ".");
            Log.e("CrashlyticsCore", ".     |  | ");
            Log.e("CrashlyticsCore", ".     |  |");
            Log.e("CrashlyticsCore", ".     |  |");
            Log.e("CrashlyticsCore", ".   \\ |  | /");
            Log.e("CrashlyticsCore", ".    \\    /");
            Log.e("CrashlyticsCore", ".     \\  /");
            Log.e("CrashlyticsCore", ".      \\/");
            Log.e("CrashlyticsCore", ".");
            Log.e("CrashlyticsCore", "This app relies on Crashlytics. Please sign up for access at https://fabric.io/sign_up,\ninstall an Android build tool and ask a team member to invite you to this app's organization.");
            Log.e("CrashlyticsCore", ".");
            Log.e("CrashlyticsCore", ".      /\\");
            Log.e("CrashlyticsCore", ".     /  \\");
            Log.e("CrashlyticsCore", ".    /    \\");
            Log.e("CrashlyticsCore", ".   / |  | \\");
            Log.e("CrashlyticsCore", ".     |  |");
            Log.e("CrashlyticsCore", ".     |  |");
            Log.e("CrashlyticsCore", ".     |  |");
            Log.e("CrashlyticsCore", ".");
            var1 = false;
         }
      }

      return var1;
   }

   private static String sanitizeAttribute(String var0) {
      String var1 = var0;
      if(var0 != null) {
         var0 = var0.trim();
         var1 = var0;
         if(var0.length() > 1024) {
            var1 = var0.substring(0, 1024);
         }
      }

      return var1;
   }

   public void crash() {
      (new CrashTest()).indexOutOfBounds();
   }

   void createCrashMarker() {
      this.crashMarker.create();
   }

   boolean didPreviousInitializationFail() {
      return ((Boolean)this.backgroundWorker.submitAndWait(new Callable() {
         public Boolean call() throws Exception {
            return Boolean.valueOf(CrashlyticsCore.this.initializationMarker.isPresent());
         }
      })).booleanValue();
   }

   protected Void doInBackground() {
      // $FF: Couldn't be decompiled
   }

   Map getAttributes() {
      return Collections.unmodifiableMap(this.attributes);
   }

   CrashlyticsController getController() {
      return this.controller;
   }

   SessionEventData getExternalCrashEventData() {
      SessionEventData var1 = null;
      if(this.externalCrashEventDataProvider != null) {
         var1 = this.externalCrashEventDataProvider.getCrashEventData();
      }

      return var1;
   }

   public String getIdentifier() {
      return "com.crashlytics.sdk.android.crashlytics-core";
   }

   public PinningInfoProvider getPinningInfoProvider() {
      PinningInfoProvider var1;
      if(!this.disabled) {
         var1 = this.pinningInfo;
      } else {
         var1 = null;
      }

      return var1;
   }

   String getUserEmail() {
      String var1;
      if(this.getIdManager().a()) {
         var1 = this.userEmail;
      } else {
         var1 = null;
      }

      return var1;
   }

   String getUserIdentifier() {
      String var1;
      if(this.getIdManager().a()) {
         var1 = this.userId;
      } else {
         var1 = null;
      }

      return var1;
   }

   String getUserName() {
      String var1;
      if(this.getIdManager().a()) {
         var1 = this.userName;
      } else {
         var1 = null;
      }

      return var1;
   }

   public String getVersion() {
      return "2.3.17.dev";
   }

   boolean internalVerifyPinning(URL var1) {
      boolean var2;
      if(this.getPinningInfoProvider() != null) {
         HttpRequest var3 = this.httpRequestFactory.a(io.fabric.sdk.android.services.network.c.a, var1.toString());
         ((HttpsURLConnection)var3.a()).setInstanceFollowRedirects(false);
         var3.b();
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   public void log(int var1, String var2, String var3) {
      this.doLog(var1, var2, var3);
      c.h().a(var1, "" + var2, "" + var3, true);
   }

   public void log(String var1) {
      this.doLog(3, "CrashlyticsCore", var1);
   }

   public void logException(Throwable var1) {
      if(!this.disabled && ensureFabricWithCalled("prior to logging exceptions.")) {
         if(var1 == null) {
            c.h().a(5, "CrashlyticsCore", "Crashlytics is ignoring a request to log a null exception.");
         } else {
            this.controller.writeNonFatalException(Thread.currentThread(), var1);
         }
      }

   }

   void markInitializationComplete() {
      this.backgroundWorker.submit(new Callable() {
         public Boolean call() throws Exception {
            boolean var1;
            Boolean var2;
            try {
               var1 = CrashlyticsCore.this.initializationMarker.remove();
               k var5 = c.h();
               StringBuilder var3 = new StringBuilder();
               var5.a("CrashlyticsCore", var3.append("Initialization marker file removed: ").append(var1).toString());
            } catch (Exception var4) {
               c.h().e("CrashlyticsCore", "Problem encountered deleting Crashlytics initialization marker.", var4);
               var2 = Boolean.valueOf(false);
               return var2;
            }

            var2 = Boolean.valueOf(var1);
            return var2;
         }
      });
   }

   void markInitializationStarted() {
      this.backgroundWorker.submitAndWait(new Callable() {
         public Void call() throws Exception {
            CrashlyticsCore.this.initializationMarker.create();
            c.h().a("CrashlyticsCore", "Initialization marker file created.");
            return null;
         }
      });
   }

   protected boolean onPreExecute() {
      return this.onPreExecute(super.getContext());
   }

   boolean onPreExecute(Context param1) {
      // $FF: Couldn't be decompiled
   }

   public void setBool(String var1, boolean var2) {
      this.setString(var1, Boolean.toString(var2));
   }

   public void setDouble(String var1, double var2) {
      this.setString(var1, Double.toString(var2));
   }

   void setExternalCrashEventDataProvider(CrashEventDataProvider var1) {
      this.externalCrashEventDataProvider = var1;
   }

   public void setFloat(String var1, float var2) {
      this.setString(var1, Float.toString(var2));
   }

   public void setInt(String var1, int var2) {
      this.setString(var1, Integer.toString(var2));
   }

   @Deprecated
   public void setListener(CrashlyticsListener param1) {
      // $FF: Couldn't be decompiled
   }

   public void setLong(String var1, long var2) {
      this.setString(var1, Long.toString(var2));
   }

   public void setString(String var1, String var2) {
      if(!this.disabled && ensureFabricWithCalled("prior to setting keys.")) {
         if(var1 == null) {
            Context var4 = this.getContext();
            if(var4 != null && i.i(var4)) {
               throw new IllegalArgumentException("Custom attribute key must not be null.");
            }

            c.h().e("CrashlyticsCore", "Attempting to set custom attribute with null key, ignoring.", (Throwable)null);
         } else {
            String var3 = sanitizeAttribute(var1);
            if(this.attributes.size() >= 64 && !this.attributes.containsKey(var3)) {
               c.h().a("CrashlyticsCore", "Exceeded maximum number of custom attributes (64)");
            } else {
               if(var2 == null) {
                  var1 = "";
               } else {
                  var1 = sanitizeAttribute(var2);
               }

               this.attributes.put(var3, var1);
               this.controller.cacheKeyData(this.attributes);
            }
         }
      }

   }

   public void setUserEmail(String var1) {
      if(!this.disabled && ensureFabricWithCalled("prior to setting user data.")) {
         this.userEmail = sanitizeAttribute(var1);
         this.controller.cacheUserData(this.userId, this.userName, this.userEmail);
      }

   }

   public void setUserIdentifier(String var1) {
      if(!this.disabled && ensureFabricWithCalled("prior to setting user data.")) {
         this.userId = sanitizeAttribute(var1);
         this.controller.cacheUserData(this.userId, this.userName, this.userEmail);
      }

   }

   public void setUserName(String var1) {
      if(!this.disabled && ensureFabricWithCalled("prior to setting user data.")) {
         this.userName = sanitizeAttribute(var1);
         this.controller.cacheUserData(this.userId, this.userName, this.userEmail);
      }

   }

   public boolean verifyPinning(URL var1) {
      boolean var2;
      try {
         var2 = this.internalVerifyPinning(var1);
      } catch (Exception var3) {
         c.h().e("CrashlyticsCore", "Could not verify SSL pinning", var3);
         var2 = false;
      }

      return var2;
   }

   public static class Builder {
      private float delay = -1.0F;
      private boolean disabled = false;
      private CrashlyticsListener listener;
      private PinningInfoProvider pinningInfoProvider;

      public CrashlyticsCore build() {
         if(this.delay < 0.0F) {
            this.delay = 1.0F;
         }

         return new CrashlyticsCore(this.delay, this.listener, this.pinningInfoProvider, this.disabled);
      }

      public CrashlyticsCore.Builder delay(float var1) {
         if(var1 <= 0.0F) {
            throw new IllegalArgumentException("delay must be greater than 0");
         } else if(this.delay > 0.0F) {
            throw new IllegalStateException("delay already set.");
         } else {
            this.delay = var1;
            return this;
         }
      }

      public CrashlyticsCore.Builder disabled(boolean var1) {
         this.disabled = var1;
         return this;
      }

      public CrashlyticsCore.Builder listener(CrashlyticsListener var1) {
         if(var1 == null) {
            throw new IllegalArgumentException("listener must not be null.");
         } else if(this.listener != null) {
            throw new IllegalStateException("listener already set.");
         } else {
            this.listener = var1;
            return this;
         }
      }

      @Deprecated
      public CrashlyticsCore.Builder pinningInfo(PinningInfoProvider var1) {
         if(var1 == null) {
            throw new IllegalArgumentException("pinningInfoProvider must not be null.");
         } else if(this.pinningInfoProvider != null) {
            throw new IllegalStateException("pinningInfoProvider already set.");
         } else {
            this.pinningInfoProvider = var1;
            return this;
         }
      }
   }

   private static final class CrashMarkerCheck implements Callable {
      private final CrashlyticsFileMarker crashMarker;

      public CrashMarkerCheck(CrashlyticsFileMarker var1) {
         this.crashMarker = var1;
      }

      public Boolean call() throws Exception {
         Boolean var1;
         if(!this.crashMarker.isPresent()) {
            var1 = Boolean.FALSE;
         } else {
            c.h().a("CrashlyticsCore", "Found previous crash marker.");
            this.crashMarker.remove();
            var1 = Boolean.TRUE;
         }

         return var1;
      }
   }

   private static final class NoOpListener implements CrashlyticsListener {
      private NoOpListener() {
      }

      // $FF: synthetic method
      NoOpListener(Object var1) {
         this();
      }

      public void crashlyticsDidDetectCrashDuringPreviousExecution() {
      }
   }
}
