package com.crashlytics.android.core;

import java.io.File;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

class MetaDataStore {
   private static final String KEYDATA_SUFFIX = "keys";
   private static final String KEY_USER_EMAIL = "userEmail";
   private static final String KEY_USER_ID = "userId";
   private static final String KEY_USER_NAME = "userName";
   private static final String METADATA_EXT = ".meta";
   private static final String USERDATA_SUFFIX = "user";
   private static final Charset UTF_8 = Charset.forName("UTF-8");
   private final File filesDir;

   public MetaDataStore(File var1) {
      this.filesDir = var1;
   }

   private File getKeysFileForSession(String var1) {
      return new File(this.filesDir, var1 + "keys" + ".meta");
   }

   private File getUserDataFileForSession(String var1) {
      return new File(this.filesDir, var1 + "user" + ".meta");
   }

   private static Map jsonToKeysData(String var0) throws JSONException {
      JSONObject var2 = new JSONObject(var0);
      HashMap var1 = new HashMap();
      Iterator var3 = var2.keys();

      while(var3.hasNext()) {
         var0 = (String)var3.next();
         var1.put(var0, valueOrNull(var2, var0));
      }

      return var1;
   }

   private static UserMetaData jsonToUserData(String var0) throws JSONException {
      JSONObject var1 = new JSONObject(var0);
      return new UserMetaData(valueOrNull(var1, "userId"), valueOrNull(var1, "userName"), valueOrNull(var1, "userEmail"));
   }

   private static String keysDataToJson(Map var0) throws JSONException {
      return (new JSONObject(var0)).toString();
   }

   private static String userDataToJson(final UserMetaData var0) throws JSONException {
      return (new JSONObject() {
         {
            this.put("userId", var0.id);
            this.put("userName", var0.name);
            this.put("userEmail", var0.email);
         }
      }).toString();
   }

   private static String valueOrNull(JSONObject var0, String var1) {
      String var2 = null;
      if(!var0.isNull(var1)) {
         var2 = var0.optString(var1, (String)null);
      }

      return var2;
   }

   public Map readKeyData(String param1) {
      // $FF: Couldn't be decompiled
   }

   public UserMetaData readUserData(String param1) {
      // $FF: Couldn't be decompiled
   }

   public void writeKeyData(String param1, Map param2) {
      // $FF: Couldn't be decompiled
   }

   public void writeUserData(String param1, UserMetaData param2) {
      // $FF: Couldn't be decompiled
   }
}
