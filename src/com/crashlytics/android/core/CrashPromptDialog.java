package com.crashlytics.android.core;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.widget.ScrollView;
import android.widget.TextView;
import io.fabric.sdk.android.services.e.o;
import java.util.concurrent.CountDownLatch;

class CrashPromptDialog {
   private final Builder dialog;
   private final CrashPromptDialog.OptInLatch latch;

   private CrashPromptDialog(Builder var1, CrashPromptDialog.OptInLatch var2) {
      this.latch = var2;
      this.dialog = var1;
   }

   public static CrashPromptDialog create(Activity var0, o var1, final CrashPromptDialog.AlwaysSendCallback var2) {
      final CrashPromptDialog.OptInLatch var3 = new CrashPromptDialog.OptInLatch(null);
      DialogStringResolver var5 = new DialogStringResolver(var0, var1);
      Builder var4 = new Builder(var0);
      ScrollView var7 = createDialogView(var0, var5.getMessage());
      OnClickListener var6 = new OnClickListener() {
         public void onClick(DialogInterface var1, int var2) {
            var3.setOptIn(true);
            var1.dismiss();
         }
      };
      var4.setView(var7).setTitle(var5.getTitle()).setCancelable(false).setNeutralButton(var5.getSendButtonTitle(), var6);
      OnClickListener var8;
      if(var1.d) {
         var8 = new OnClickListener() {
            public void onClick(DialogInterface var1, int var2) {
               var3.setOptIn(false);
               var1.dismiss();
            }
         };
         var4.setNegativeButton(var5.getCancelButtonTitle(), var8);
      }

      if(var1.f) {
         var8 = new OnClickListener() {
            public void onClick(DialogInterface var1, int var2x) {
               var2.sendUserReportsWithoutPrompting(true);
               var3.setOptIn(true);
               var1.dismiss();
            }
         };
         var4.setPositiveButton(var5.getAlwaysSendButtonTitle(), var8);
      }

      return new CrashPromptDialog(var4, var3);
   }

   private static ScrollView createDialogView(Activity var0, String var1) {
      float var2 = var0.getResources().getDisplayMetrics().density;
      int var3 = dipsToPixels(var2, 5);
      TextView var4 = new TextView(var0);
      var4.setAutoLinkMask(15);
      var4.setText(var1);
      var4.setTextAppearance(var0, 16973892);
      var4.setPadding(var3, var3, var3, var3);
      var4.setFocusable(false);
      ScrollView var5 = new ScrollView(var0);
      var5.setPadding(dipsToPixels(var2, 14), dipsToPixels(var2, 2), dipsToPixels(var2, 10), dipsToPixels(var2, 12));
      var5.addView(var4);
      return var5;
   }

   private static int dipsToPixels(float var0, int var1) {
      return (int)((float)var1 * var0);
   }

   public void await() {
      this.latch.await();
   }

   public boolean getOptIn() {
      return this.latch.getOptIn();
   }

   public void show() {
      this.dialog.show();
   }

   interface AlwaysSendCallback {
      void sendUserReportsWithoutPrompting(boolean var1);
   }

   private static class OptInLatch {
      private final CountDownLatch latch;
      private boolean send;

      private OptInLatch() {
         this.send = false;
         this.latch = new CountDownLatch(1);
      }

      // $FF: synthetic method
      OptInLatch(Object var1) {
         this();
      }

      void await() {
         try {
            this.latch.await();
         } catch (InterruptedException var2) {
            ;
         }

      }

      boolean getOptIn() {
         return this.send;
      }

      void setOptIn(boolean var1) {
         this.send = var1;
         this.latch.countDown();
      }
   }
}
