package com.crashlytics.android.core;

public class UserMetaData {
   public static final UserMetaData EMPTY = new UserMetaData();
   public final String email;
   public final String id;
   public final String name;

   public UserMetaData() {
      this((String)null, (String)null, (String)null);
   }

   public UserMetaData(String var1, String var2, String var3) {
      this.id = var1;
      this.name = var2;
      this.email = var3;
   }

   public boolean isEmpty() {
      boolean var1;
      if(this.id == null && this.name == null && this.email == null) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }
}
