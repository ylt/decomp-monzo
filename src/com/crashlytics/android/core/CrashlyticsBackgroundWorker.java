package com.crashlytics.android.core;

import android.os.Looper;
import io.fabric.sdk.android.c;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;

class CrashlyticsBackgroundWorker {
   private final ExecutorService executorService;

   public CrashlyticsBackgroundWorker(ExecutorService var1) {
      this.executorService = var1;
   }

   Future submit(final Runnable var1) {
      Future var5;
      try {
         ExecutorService var2 = this.executorService;
         Runnable var3 = new Runnable() {
            public void run() {
               try {
                  var1.run();
               } catch (Exception var2) {
                  c.h().e("CrashlyticsCore", "Failed to execute task.", var2);
               }

            }
         };
         var5 = var2.submit(var3);
      } catch (RejectedExecutionException var4) {
         c.h().a("CrashlyticsCore", "Executor is shut down because we're handling a fatal crash.");
         var5 = null;
      }

      return var5;
   }

   Future submit(final Callable var1) {
      Future var5;
      try {
         ExecutorService var2 = this.executorService;
         Callable var3 = new Callable() {
            public Object call() throws Exception {
               Object var1x;
               try {
                  var1x = var1.call();
               } catch (Exception var2) {
                  c.h().e("CrashlyticsCore", "Failed to execute task.", var2);
                  var1x = null;
               }

               return var1x;
            }
         };
         var5 = var2.submit(var3);
      } catch (RejectedExecutionException var4) {
         c.h().a("CrashlyticsCore", "Executor is shut down because we're handling a fatal crash.");
         var5 = null;
      }

      return var5;
   }

   Object submitAndWait(Callable var1) {
      Object var2 = null;

      Object var5;
      try {
         if(Looper.getMainLooper() == Looper.myLooper()) {
            var5 = this.executorService.submit(var1).get(4L, TimeUnit.SECONDS);
         } else {
            var5 = this.executorService.submit(var1).get();
         }
      } catch (RejectedExecutionException var3) {
         c.h().a("CrashlyticsCore", "Executor is shut down because we're handling a fatal crash.");
         var5 = var2;
      } catch (Exception var4) {
         c.h().e("CrashlyticsCore", "Failed to execute task.", var4);
         var5 = var2;
      }

      return var5;
   }
}
