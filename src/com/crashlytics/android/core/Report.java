package com.crashlytics.android.core;

import java.io.File;
import java.util.Map;

interface Report {
   Map getCustomHeaders();

   File getFile();

   String getFileName();

   File[] getFiles();

   String getIdentifier();

   void remove();
}
