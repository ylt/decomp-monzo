package com.crashlytics.android.core;

import android.app.ActivityManager.RunningAppProcessInfo;
import android.os.Build.VERSION;
import io.fabric.sdk.android.c;
import io.fabric.sdk.android.services.b.o;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

class SessionProtobufHelper {
   private static final String SIGNAL_DEFAULT = "0";
   private static final ByteString SIGNAL_DEFAULT_BYTE_STRING = ByteString.copyFromUtf8("0");
   private static final ByteString UNITY_PLATFORM_BYTE_STRING = ByteString.copyFromUtf8("Unity");

   private static int getBinaryImageSize(ByteString var0, ByteString var1) {
      int var3 = 0 + CodedOutputStream.computeUInt64Size(1, 0L) + CodedOutputStream.computeUInt64Size(2, 0L) + CodedOutputStream.computeBytesSize(3, var0);
      int var2 = var3;
      if(var1 != null) {
         var2 = var3 + CodedOutputStream.computeBytesSize(4, var1);
      }

      return var2;
   }

   private static int getDeviceIdentifierSize(o.a var0, String var1) {
      return CodedOutputStream.computeEnumSize(1, var0.h) + CodedOutputStream.computeBytesSize(2, ByteString.copyFromUtf8(var1));
   }

   private static int getEventAppCustomAttributeSize(String var0, String var1) {
      int var2 = CodedOutputStream.computeBytesSize(1, ByteString.copyFromUtf8(var0));
      var0 = var1;
      if(var1 == null) {
         var0 = "";
      }

      return var2 + CodedOutputStream.computeBytesSize(2, ByteString.copyFromUtf8(var0));
   }

   private static int getEventAppExecutionExceptionSize(TrimmedThrowableData var0, int var1, int var2) {
      byte var5 = 0;
      int var4 = CodedOutputStream.computeBytesSize(1, ByteString.copyFromUtf8(var0.className)) + 0;
      String var10 = var0.localizedMessage;
      int var3 = var4;
      if(var10 != null) {
         var3 = var4 + CodedOutputStream.computeBytesSize(3, ByteString.copyFromUtf8(var10));
      }

      StackTraceElement[] var11 = var0.stacktrace;
      int var6 = var11.length;

      int var7;
      int var8;
      int var9;
      for(var4 = 0; var4 < var6; var3 += var9 + var7 + var8) {
         var9 = getFrameSize(var11[var4], true);
         var7 = CodedOutputStream.computeTagSize(4);
         var8 = CodedOutputStream.computeRawVarint32Size(var9);
         ++var4;
      }

      TrimmedThrowableData var12 = var0.cause;
      var4 = var3;
      if(var12 != null) {
         var4 = var5;
         var0 = var12;
         if(var1 < var2) {
            var1 = getEventAppExecutionExceptionSize(var12, var1 + 1, var2);
            var4 = var3 + var1 + CodedOutputStream.computeTagSize(6) + CodedOutputStream.computeRawVarint32Size(var1);
         } else {
            while(var0 != null) {
               var0 = var0.cause;
               ++var4;
            }

            var4 = var3 + CodedOutputStream.computeUInt32Size(7, var4);
         }
      }

      return var4;
   }

   private static int getEventAppExecutionSignalSize() {
      return 0 + CodedOutputStream.computeBytesSize(1, SIGNAL_DEFAULT_BYTE_STRING) + CodedOutputStream.computeBytesSize(2, SIGNAL_DEFAULT_BYTE_STRING) + CodedOutputStream.computeUInt64Size(3, 0L);
   }

   private static int getEventAppExecutionSize(TrimmedThrowableData var0, Thread var1, StackTraceElement[] var2, Thread[] var3, List var4, int var5, ByteString var6, ByteString var7) {
      int var8 = getThreadSize(var1, var2, 4, true);
      int var11 = CodedOutputStream.computeTagSize(1);
      int var9 = CodedOutputStream.computeRawVarint32Size(var8);
      int var10 = var3.length;
      var8 = var8 + var11 + var9 + 0;

      for(var9 = 0; var9 < var10; ++var9) {
         var11 = getThreadSize(var3[var9], (StackTraceElement[])var4.get(var9), 0, false);
         var8 += var11 + CodedOutputStream.computeTagSize(1) + CodedOutputStream.computeRawVarint32Size(var11);
      }

      var10 = getEventAppExecutionExceptionSize(var0, 1, var5);
      int var13 = CodedOutputStream.computeTagSize(2);
      var11 = CodedOutputStream.computeRawVarint32Size(var10);
      int var12 = getEventAppExecutionSignalSize();
      var5 = CodedOutputStream.computeTagSize(3);
      int var14 = CodedOutputStream.computeRawVarint32Size(var12);
      var9 = getBinaryImageSize(var6, var7);
      return var10 + var13 + var11 + var8 + var12 + var5 + var14 + var9 + CodedOutputStream.computeTagSize(3) + CodedOutputStream.computeRawVarint32Size(var9);
   }

   private static int getEventAppSize(TrimmedThrowableData var0, Thread var1, StackTraceElement[] var2, Thread[] var3, List var4, int var5, ByteString var6, ByteString var7, Map var8, RunningAppProcessInfo var9, int var10) {
      var5 = getEventAppExecutionSize(var0, var1, var2, var3, var4, var5, var6, var7);
      var5 = 0 + var5 + CodedOutputStream.computeTagSize(1) + CodedOutputStream.computeRawVarint32Size(var5);
      int var11;
      if(var8 != null) {
         for(Iterator var14 = var8.entrySet().iterator(); var14.hasNext(); var5 += var11 + CodedOutputStream.computeTagSize(2) + CodedOutputStream.computeRawVarint32Size(var11)) {
            Entry var13 = (Entry)var14.next();
            var11 = getEventAppCustomAttributeSize((String)var13.getKey(), (String)var13.getValue());
         }
      }

      if(var9 != null) {
         boolean var12;
         if(var9.importance != 100) {
            var12 = true;
         } else {
            var12 = false;
         }

         var5 += CodedOutputStream.computeBoolSize(3, var12);
      }

      return var5 + CodedOutputStream.computeUInt32Size(4, var10);
   }

   private static int getEventDeviceSize(Float var0, int var1, boolean var2, int var3, long var4, long var6) {
      int var8 = 0;
      if(var0 != null) {
         var8 = 0 + CodedOutputStream.computeFloatSize(1, var0.floatValue());
      }

      return var8 + CodedOutputStream.computeSInt32Size(2, var1) + CodedOutputStream.computeBoolSize(3, var2) + CodedOutputStream.computeUInt32Size(4, var3) + CodedOutputStream.computeUInt64Size(5, var4) + CodedOutputStream.computeUInt64Size(6, var6);
   }

   private static int getEventLogSize(ByteString var0) {
      return CodedOutputStream.computeBytesSize(1, var0);
   }

   private static int getFrameSize(StackTraceElement var0, boolean var1) {
      int var2;
      if(var0.isNativeMethod()) {
         var2 = CodedOutputStream.computeUInt64Size(1, (long)Math.max(var0.getLineNumber(), 0)) + 0;
      } else {
         var2 = CodedOutputStream.computeUInt64Size(1, 0L) + 0;
      }

      int var3 = var2 + CodedOutputStream.computeBytesSize(2, ByteString.copyFromUtf8(var0.getClassName() + "." + var0.getMethodName()));
      var2 = var3;
      if(var0.getFileName() != null) {
         var2 = var3 + CodedOutputStream.computeBytesSize(3, ByteString.copyFromUtf8(var0.getFileName()));
      }

      if(!var0.isNativeMethod() && var0.getLineNumber() > 0) {
         var2 += CodedOutputStream.computeUInt64Size(4, (long)var0.getLineNumber());
      }

      byte var4;
      if(var1) {
         var4 = 2;
      } else {
         var4 = 0;
      }

      return CodedOutputStream.computeUInt32Size(5, var4) + var2;
   }

   private static int getSessionAppOrgSize(ByteString var0) {
      return 0 + CodedOutputStream.computeBytesSize(1, var0);
   }

   private static int getSessionAppSize(ByteString var0, ByteString var1, ByteString var2, ByteString var3, ByteString var4, int var5, ByteString var6) {
      int var10 = CodedOutputStream.computeBytesSize(1, var0);
      int var8 = CodedOutputStream.computeBytesSize(2, var2);
      int var7 = CodedOutputStream.computeBytesSize(3, var3);
      int var9 = getSessionAppOrgSize(var1);
      var8 = 0 + var10 + var8 + var7 + var9 + CodedOutputStream.computeTagSize(5) + CodedOutputStream.computeRawVarint32Size(var9) + CodedOutputStream.computeBytesSize(6, var4);
      var7 = var8;
      if(var6 != null) {
         var7 = var8 + CodedOutputStream.computeBytesSize(8, UNITY_PLATFORM_BYTE_STRING) + CodedOutputStream.computeBytesSize(9, var6);
      }

      return var7 + CodedOutputStream.computeEnumSize(10, var5);
   }

   private static int getSessionDeviceSize(int var0, ByteString var1, ByteString var2, int var3, long var4, long var6, boolean var8, Map var9, int var10, ByteString var11, ByteString var12) {
      int var13 = CodedOutputStream.computeBytesSize(1, var1);
      int var14 = CodedOutputStream.computeEnumSize(3, var0);
      if(var2 == null) {
         var0 = 0;
      } else {
         var0 = CodedOutputStream.computeBytesSize(4, var2);
      }

      var3 = var0 + var14 + 0 + var13 + CodedOutputStream.computeUInt32Size(5, var3) + CodedOutputStream.computeUInt64Size(6, var4) + CodedOutputStream.computeUInt64Size(7, var6) + CodedOutputStream.computeBoolSize(10, var8);
      if(var9 != null) {
         Iterator var15 = var9.entrySet().iterator();
         var0 = var3;

         while(true) {
            var3 = var0;
            if(!var15.hasNext()) {
               break;
            }

            Entry var16 = (Entry)var15.next();
            var3 = getDeviceIdentifierSize((o.a)var16.getKey(), (String)var16.getValue());
            var0 += var3 + CodedOutputStream.computeTagSize(11) + CodedOutputStream.computeRawVarint32Size(var3);
         }
      }

      var13 = CodedOutputStream.computeUInt32Size(12, var10);
      if(var11 == null) {
         var0 = 0;
      } else {
         var0 = CodedOutputStream.computeBytesSize(13, var11);
      }

      if(var12 == null) {
         var10 = 0;
      } else {
         var10 = CodedOutputStream.computeBytesSize(14, var12);
      }

      return var10 + var3 + var13 + var0;
   }

   private static int getSessionEventSize(long var0, String var2, TrimmedThrowableData var3, Thread var4, StackTraceElement[] var5, Thread[] var6, List var7, int var8, Map var9, RunningAppProcessInfo var10, int var11, ByteString var12, ByteString var13, Float var14, int var15, boolean var16, long var17, long var19, ByteString var21) {
      int var22 = CodedOutputStream.computeUInt64Size(1, var0);
      int var23 = CodedOutputStream.computeBytesSize(2, ByteString.copyFromUtf8(var2));
      int var24 = getEventAppSize(var3, var4, var5, var6, var7, var8, var12, var13, var9, var10, var11);
      int var25 = CodedOutputStream.computeTagSize(3);
      var8 = CodedOutputStream.computeRawVarint32Size(var24);
      var11 = getEventDeviceSize(var14, var15, var16, var11, var17, var19);
      var11 = var11 + CodedOutputStream.computeTagSize(5) + CodedOutputStream.computeRawVarint32Size(var11) + 0 + var22 + var23 + var24 + var25 + var8;
      var8 = var11;
      if(var21 != null) {
         var8 = getEventLogSize(var21);
         var8 = var11 + var8 + CodedOutputStream.computeTagSize(6) + CodedOutputStream.computeRawVarint32Size(var8);
      }

      return var8;
   }

   private static int getSessionOSSize(ByteString var0, ByteString var1, boolean var2) {
      return 0 + CodedOutputStream.computeEnumSize(1, 3) + CodedOutputStream.computeBytesSize(2, var0) + CodedOutputStream.computeBytesSize(3, var1) + CodedOutputStream.computeBoolSize(4, var2);
   }

   private static int getThreadSize(Thread var0, StackTraceElement[] var1, int var2, boolean var3) {
      int var4 = CodedOutputStream.computeBytesSize(1, ByteString.copyFromUtf8(var0.getName()));
      var4 += CodedOutputStream.computeUInt32Size(2, var2);
      int var5 = var1.length;

      for(var2 = 0; var2 < var5; ++var2) {
         int var6 = getFrameSize(var1[var2], var3);
         var4 += var6 + CodedOutputStream.computeTagSize(3) + CodedOutputStream.computeRawVarint32Size(var6);
      }

      return var4;
   }

   private static ByteString stringToByteString(String var0) {
      ByteString var1;
      if(var0 == null) {
         var1 = null;
      } else {
         var1 = ByteString.copyFromUtf8(var0);
      }

      return var1;
   }

   public static void writeBeginSession(CodedOutputStream var0, String var1, String var2, long var3) throws Exception {
      var0.writeBytes(1, ByteString.copyFromUtf8(var2));
      var0.writeBytes(2, ByteString.copyFromUtf8(var1));
      var0.writeUInt64(3, var3);
   }

   private static void writeFrame(CodedOutputStream var0, int var1, StackTraceElement var2, boolean var3) throws Exception {
      byte var4 = 4;
      var0.writeTag(var1, 2);
      var0.writeRawVarint32(getFrameSize(var2, var3));
      if(var2.isNativeMethod()) {
         var0.writeUInt64(1, (long)Math.max(var2.getLineNumber(), 0));
      } else {
         var0.writeUInt64(1, 0L);
      }

      var0.writeBytes(2, ByteString.copyFromUtf8(var2.getClassName() + "." + var2.getMethodName()));
      if(var2.getFileName() != null) {
         var0.writeBytes(3, ByteString.copyFromUtf8(var2.getFileName()));
      }

      if(!var2.isNativeMethod() && var2.getLineNumber() > 0) {
         var0.writeUInt64(4, (long)var2.getLineNumber());
      }

      byte var5;
      if(var3) {
         var5 = var4;
      } else {
         var5 = 0;
      }

      var0.writeUInt32(5, var5);
   }

   public static void writeSessionApp(CodedOutputStream var0, String var1, String var2, String var3, String var4, String var5, int var6, String var7) throws Exception {
      ByteString var8 = ByteString.copyFromUtf8(var1);
      ByteString var10 = ByteString.copyFromUtf8(var2);
      ByteString var11 = ByteString.copyFromUtf8(var3);
      ByteString var12 = ByteString.copyFromUtf8(var4);
      ByteString var13 = ByteString.copyFromUtf8(var5);
      ByteString var9;
      if(var7 != null) {
         var9 = ByteString.copyFromUtf8(var7);
      } else {
         var9 = null;
      }

      var0.writeTag(7, 2);
      var0.writeRawVarint32(getSessionAppSize(var8, var10, var11, var12, var13, var6, var9));
      var0.writeBytes(1, var8);
      var0.writeBytes(2, var11);
      var0.writeBytes(3, var12);
      var0.writeTag(5, 2);
      var0.writeRawVarint32(getSessionAppOrgSize(var10));
      var0.writeBytes(1, var10);
      var0.writeBytes(6, var13);
      if(var9 != null) {
         var0.writeBytes(8, UNITY_PLATFORM_BYTE_STRING);
         var0.writeBytes(9, var9);
      }

      var0.writeEnum(10, var6);
   }

   public static void writeSessionDevice(CodedOutputStream var0, String var1, int var2, String var3, int var4, long var5, long var7, boolean var9, Map var10, int var11, String var12, String var13) throws Exception {
      ByteString var15 = ByteString.copyFromUtf8(var1);
      ByteString var14 = stringToByteString(var3);
      ByteString var17 = stringToByteString(var13);
      ByteString var19 = stringToByteString(var12);
      var0.writeTag(9, 2);
      var0.writeRawVarint32(getSessionDeviceSize(var2, var15, var14, var4, var5, var7, var9, var10, var11, var19, var17));
      var0.writeBytes(1, var15);
      var0.writeEnum(3, var2);
      var0.writeBytes(4, var14);
      var0.writeUInt32(5, var4);
      var0.writeUInt64(6, var5);
      var0.writeUInt64(7, var7);
      var0.writeBool(10, var9);
      Iterator var18 = var10.entrySet().iterator();

      while(var18.hasNext()) {
         Entry var16 = (Entry)var18.next();
         var0.writeTag(11, 2);
         var0.writeRawVarint32(getDeviceIdentifierSize((o.a)var16.getKey(), (String)var16.getValue()));
         var0.writeEnum(1, ((o.a)var16.getKey()).h);
         var0.writeBytes(2, ByteString.copyFromUtf8((String)var16.getValue()));
      }

      var0.writeUInt32(12, var11);
      if(var19 != null) {
         var0.writeBytes(13, var19);
      }

      if(var17 != null) {
         var0.writeBytes(14, var17);
      }

   }

   public static void writeSessionEvent(CodedOutputStream var0, long var1, String var3, TrimmedThrowableData var4, Thread var5, StackTraceElement[] var6, Thread[] var7, List var8, Map var9, LogFileManager var10, RunningAppProcessInfo var11, int var12, String var13, String var14, Float var15, int var16, boolean var17, long var18, long var20) throws Exception {
      ByteString var22 = ByteString.copyFromUtf8(var13);
      ByteString var23;
      if(var14 == null) {
         var23 = null;
      } else {
         var23 = ByteString.copyFromUtf8(var14.replace("-", ""));
      }

      ByteString var24 = var10.getByteStringForLog();
      if(var24 == null) {
         c.h().a("CrashlyticsCore", "No log data to include with this event.");
      }

      var10.clearLog();
      var0.writeTag(10, 2);
      var0.writeRawVarint32(getSessionEventSize(var1, var3, var4, var5, var6, var7, var8, 8, var9, var11, var12, var22, var23, var15, var16, var17, var18, var20, var24));
      var0.writeUInt64(1, var1);
      var0.writeBytes(2, ByteString.copyFromUtf8(var3));
      writeSessionEventApp(var0, var4, var5, var6, var7, var8, 8, var22, var23, var9, var11, var12);
      writeSessionEventDevice(var0, var15, var16, var17, var12, var18, var20);
      writeSessionEventLog(var0, var24);
   }

   private static void writeSessionEventApp(CodedOutputStream var0, TrimmedThrowableData var1, Thread var2, StackTraceElement[] var3, Thread[] var4, List var5, int var6, ByteString var7, ByteString var8, Map var9, RunningAppProcessInfo var10, int var11) throws Exception {
      var0.writeTag(3, 2);
      var0.writeRawVarint32(getEventAppSize(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10, var11));
      writeSessionEventAppExecution(var0, var1, var2, var3, var4, var5, var6, var7, var8);
      if(var9 != null && !var9.isEmpty()) {
         writeSessionEventAppCustomAttributes(var0, var9);
      }

      if(var10 != null) {
         boolean var12;
         if(var10.importance != 100) {
            var12 = true;
         } else {
            var12 = false;
         }

         var0.writeBool(3, var12);
      }

      var0.writeUInt32(4, var11);
   }

   private static void writeSessionEventAppCustomAttributes(CodedOutputStream var0, Map var1) throws Exception {
      String var5;
      for(Iterator var3 = var1.entrySet().iterator(); var3.hasNext(); var0.writeBytes(2, ByteString.copyFromUtf8(var5))) {
         Entry var4 = (Entry)var3.next();
         var0.writeTag(2, 2);
         var0.writeRawVarint32(getEventAppCustomAttributeSize((String)var4.getKey(), (String)var4.getValue()));
         var0.writeBytes(1, ByteString.copyFromUtf8((String)var4.getKey()));
         String var2 = (String)var4.getValue();
         var5 = var2;
         if(var2 == null) {
            var5 = "";
         }
      }

   }

   private static void writeSessionEventAppExecution(CodedOutputStream var0, TrimmedThrowableData var1, Thread var2, StackTraceElement[] var3, Thread[] var4, List var5, int var6, ByteString var7, ByteString var8) throws Exception {
      var0.writeTag(1, 2);
      var0.writeRawVarint32(getEventAppExecutionSize(var1, var2, var3, var4, var5, var6, var7, var8));
      writeThread(var0, var2, var3, 4, true);
      int var10 = var4.length;

      for(int var9 = 0; var9 < var10; ++var9) {
         writeThread(var0, var4[var9], (StackTraceElement[])var5.get(var9), 0, false);
      }

      writeSessionEventAppExecutionException(var0, var1, 1, var6, 2);
      var0.writeTag(3, 2);
      var0.writeRawVarint32(getEventAppExecutionSignalSize());
      var0.writeBytes(1, SIGNAL_DEFAULT_BYTE_STRING);
      var0.writeBytes(2, SIGNAL_DEFAULT_BYTE_STRING);
      var0.writeUInt64(3, 0L);
      var0.writeTag(4, 2);
      var0.writeRawVarint32(getBinaryImageSize(var7, var8));
      var0.writeUInt64(1, 0L);
      var0.writeUInt64(2, 0L);
      var0.writeBytes(3, var7);
      if(var8 != null) {
         var0.writeBytes(4, var8);
      }

   }

   private static void writeSessionEventAppExecutionException(CodedOutputStream var0, TrimmedThrowableData var1, int var2, int var3, int var4) throws Exception {
      byte var5 = 0;
      var0.writeTag(var4, 2);
      var0.writeRawVarint32(getEventAppExecutionExceptionSize(var1, 1, var3));
      var0.writeBytes(1, ByteString.copyFromUtf8(var1.className));
      String var7 = var1.localizedMessage;
      if(var7 != null) {
         var0.writeBytes(3, ByteString.copyFromUtf8(var7));
      }

      StackTraceElement[] var8 = var1.stacktrace;
      int var6 = var8.length;

      for(var4 = 0; var4 < var6; ++var4) {
         writeFrame(var0, 4, var8[var4], true);
      }

      TrimmedThrowableData var9 = var1.cause;
      if(var9 != null) {
         var4 = var5;
         var1 = var9;
         if(var2 < var3) {
            writeSessionEventAppExecutionException(var0, var9, var2 + 1, var3, 6);
         } else {
            while(var1 != null) {
               var1 = var1.cause;
               ++var4;
            }

            var0.writeUInt32(7, var4);
         }
      }

   }

   private static void writeSessionEventDevice(CodedOutputStream var0, Float var1, int var2, boolean var3, int var4, long var5, long var7) throws Exception {
      var0.writeTag(5, 2);
      var0.writeRawVarint32(getEventDeviceSize(var1, var2, var3, var4, var5, var7));
      if(var1 != null) {
         var0.writeFloat(1, var1.floatValue());
      }

      var0.writeSInt32(2, var2);
      var0.writeBool(3, var3);
      var0.writeUInt32(4, var4);
      var0.writeUInt64(5, var5);
      var0.writeUInt64(6, var7);
   }

   private static void writeSessionEventLog(CodedOutputStream var0, ByteString var1) throws Exception {
      if(var1 != null) {
         var0.writeTag(6, 2);
         var0.writeRawVarint32(getEventLogSize(var1));
         var0.writeBytes(1, var1);
      }

   }

   public static void writeSessionOS(CodedOutputStream var0, boolean var1) throws Exception {
      ByteString var2 = ByteString.copyFromUtf8(VERSION.RELEASE);
      ByteString var3 = ByteString.copyFromUtf8(VERSION.CODENAME);
      var0.writeTag(8, 2);
      var0.writeRawVarint32(getSessionOSSize(var2, var3, var1));
      var0.writeEnum(1, 3);
      var0.writeBytes(2, var2);
      var0.writeBytes(3, var3);
      var0.writeBool(4, var1);
   }

   public static void writeSessionUser(CodedOutputStream var0, String var1, String var2, String var3) throws Exception {
      String var6 = var1;
      if(var1 == null) {
         var6 = "";
      }

      ByteString var8 = ByteString.copyFromUtf8(var6);
      ByteString var7 = stringToByteString(var2);
      ByteString var9 = stringToByteString(var3);
      int var5 = 0 + CodedOutputStream.computeBytesSize(1, var8);
      int var4 = var5;
      if(var2 != null) {
         var4 = var5 + CodedOutputStream.computeBytesSize(2, var7);
      }

      var5 = var4;
      if(var3 != null) {
         var5 = var4 + CodedOutputStream.computeBytesSize(3, var9);
      }

      var0.writeTag(6, 2);
      var0.writeRawVarint32(var5);
      var0.writeBytes(1, var8);
      if(var2 != null) {
         var0.writeBytes(2, var7);
      }

      if(var3 != null) {
         var0.writeBytes(3, var9);
      }

   }

   private static void writeThread(CodedOutputStream var0, Thread var1, StackTraceElement[] var2, int var3, boolean var4) throws Exception {
      var0.writeTag(1, 2);
      var0.writeRawVarint32(getThreadSize(var1, var2, var3, var4));
      var0.writeBytes(1, ByteString.copyFromUtf8(var1.getName()));
      var0.writeUInt32(2, var3);
      int var5 = var2.length;

      for(var3 = 0; var3 < var5; ++var3) {
         writeFrame(var0, 3, var2[var3], var4);
      }

   }
}
