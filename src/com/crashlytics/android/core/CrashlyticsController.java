package com.crashlytics.android.core;

import android.app.Activity;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.Context;
import android.os.Environment;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.core.internal.models.SessionEventData;
import io.fabric.sdk.android.c;
import io.fabric.sdk.android.h;
import io.fabric.sdk.android.k;
import io.fabric.sdk.android.services.b.i;
import io.fabric.sdk.android.services.b.j;
import io.fabric.sdk.android.services.b.o;
import io.fabric.sdk.android.services.d.a;
import io.fabric.sdk.android.services.e.p;
import io.fabric.sdk.android.services.e.q;
import io.fabric.sdk.android.services.e.t;
import io.fabric.sdk.android.services.network.d;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.lang.Thread.UncaughtExceptionHandler;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.Map.Entry;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class CrashlyticsController {
   private static final int ANALYZER_VERSION = 1;
   static final FilenameFilter ANY_SESSION_FILENAME_FILTER = new FilenameFilter() {
      public boolean accept(File var1, String var2) {
         return CrashlyticsController.SESSION_FILE_PATTERN.matcher(var2).matches();
      }
   };
   private static final String COLLECT_CUSTOM_KEYS = "com.crashlytics.CollectCustomKeys";
   private static final String CRASHLYTICS_API_ENDPOINT = "com.crashlytics.ApiEndpoint";
   private static final String EVENT_TYPE_CRASH = "crash";
   private static final String EVENT_TYPE_LOGGED = "error";
   static final String FATAL_SESSION_DIR = "fatal-sessions";
   private static final String GENERATOR_FORMAT = "Crashlytics Android SDK/%s";
   private static final String[] INITIAL_SESSION_PART_TAGS = new String[]{"SessionUser", "SessionApp", "SessionOS", "SessionDevice"};
   static final String INVALID_CLS_CACHE_DIR = "invalidClsFiles";
   static final Comparator LARGEST_FILE_NAME_FIRST = new Comparator() {
      public int compare(File var1, File var2) {
         return var2.getName().compareTo(var1.getName());
      }
   };
   static final int MAX_INVALID_SESSIONS = 4;
   private static final int MAX_LOCAL_LOGGED_EXCEPTIONS = 64;
   static final int MAX_OPEN_SESSIONS = 8;
   static final int MAX_STACK_SIZE = 1024;
   static final String NONFATAL_SESSION_DIR = "nonfatal-sessions";
   static final int NUM_STACK_REPETITIONS_ALLOWED = 10;
   private static final Map SEND_AT_CRASHTIME_HEADER = Collections.singletonMap("X-CRASHLYTICS-SEND-FLAGS", "1");
   static final String SESSION_APP_TAG = "SessionApp";
   static final String SESSION_BEGIN_TAG = "BeginSession";
   static final String SESSION_DEVICE_TAG = "SessionDevice";
   static final String SESSION_EVENT_MISSING_BINARY_IMGS_TAG = "SessionMissingBinaryImages";
   static final String SESSION_FATAL_TAG = "SessionCrash";
   static final FilenameFilter SESSION_FILE_FILTER = new FilenameFilter() {
      public boolean accept(File var1, String var2) {
         boolean var3;
         if(var2.length() == ".cls".length() + 35 && var2.endsWith(".cls")) {
            var3 = true;
         } else {
            var3 = false;
         }

         return var3;
      }
   };
   private static final Pattern SESSION_FILE_PATTERN = Pattern.compile("([\\d|A-Z|a-z]{12}\\-[\\d|A-Z|a-z]{4}\\-[\\d|A-Z|a-z]{4}\\-[\\d|A-Z|a-z]{12}).+");
   private static final int SESSION_ID_LENGTH = 35;
   static final String SESSION_NON_FATAL_TAG = "SessionEvent";
   static final String SESSION_OS_TAG = "SessionOS";
   static final String SESSION_USER_TAG = "SessionUser";
   private static final boolean SHOULD_PROMPT_BEFORE_SENDING_REPORTS_DEFAULT = false;
   static final Comparator SMALLEST_FILE_NAME_FIRST = new Comparator() {
      public int compare(File var1, File var2) {
         return var1.getName().compareTo(var2.getName());
      }
   };
   private final AppData appData;
   private final CrashlyticsBackgroundWorker backgroundWorker;
   private CrashlyticsUncaughtExceptionHandler crashHandler;
   private final CrashlyticsCore crashlyticsCore;
   private final DevicePowerStateListener devicePowerStateListener;
   private final AtomicInteger eventCounter = new AtomicInteger(0);
   private final a fileStore;
   private final ReportUploader.HandlingExceptionCheck handlingExceptionCheck;
   private final d httpRequestFactory;
   private final o idManager;
   private final CrashlyticsController.LogFileDirectoryProvider logFileDirectoryProvider;
   private final LogFileManager logFileManager;
   private final PreferenceManager preferenceManager;
   private final ReportUploader.ReportFilesProvider reportFilesProvider;
   private final StackTraceTrimmingStrategy stackTraceTrimmingStrategy;
   private final String unityVersion;

   CrashlyticsController(CrashlyticsCore var1, CrashlyticsBackgroundWorker var2, d var3, o var4, PreferenceManager var5, a var6, AppData var7, UnityVersionProvider var8) {
      this.crashlyticsCore = var1;
      this.backgroundWorker = var2;
      this.httpRequestFactory = var3;
      this.idManager = var4;
      this.preferenceManager = var5;
      this.fileStore = var6;
      this.appData = var7;
      this.unityVersion = var8.getUnityVersion();
      Context var9 = var1.getContext();
      this.logFileDirectoryProvider = new CrashlyticsController.LogFileDirectoryProvider(var6);
      this.logFileManager = new LogFileManager(var9, this.logFileDirectoryProvider);
      this.reportFilesProvider = new CrashlyticsController.ReportUploaderFilesProvider(null);
      this.handlingExceptionCheck = new CrashlyticsController.ReportUploaderHandlingExceptionCheck(null);
      this.devicePowerStateListener = new DevicePowerStateListener(var9);
      this.stackTraceTrimmingStrategy = new MiddleOutFallbackStrategy(1024, new StackTraceTrimmingStrategy[]{new RemoveRepeatsStrategy(10)});
   }

   private void closeOpenSessions(File[] var1, int var2, int var3) {
      c.h().a("CrashlyticsCore", "Closing open sessions.");

      while(var2 < var1.length) {
         File var4 = var1[var2];
         String var5 = getSessionIdFromSessionFile(var4);
         c.h().a("CrashlyticsCore", "Closing session: " + var5);
         this.writeSessionPartsToSessionFile(var4, var5, var3);
         ++var2;
      }

   }

   private void closeWithoutRenamingOrLog(ClsFileOutputStream var1) {
      if(var1 != null) {
         try {
            var1.closeInProgressStream();
         } catch (IOException var2) {
            c.h().e("CrashlyticsCore", "Error closing session file stream in the presence of an exception", var2);
         }
      }

   }

   private static void copyToCodedOutputStream(InputStream var0, CodedOutputStream var1, int var2) throws IOException {
      byte[] var4 = new byte[var2];

      int var3;
      for(var2 = 0; var2 < var4.length; var2 += var3) {
         var3 = var0.read(var4, var2, var4.length - var2);
         if(var3 < 0) {
            break;
         }
      }

      var1.writeRawBytes(var4);
   }

   private void deleteSessionPartFilesFor(String var1) {
      File[] var4 = this.listSessionPartFilesFor(var1);
      int var3 = var4.length;

      for(int var2 = 0; var2 < var3; ++var2) {
         var4[var2].delete();
      }

   }

   private void doCloseSessions(p var1, boolean var2) throws Exception {
      byte var3;
      if(var2) {
         var3 = 1;
      } else {
         var3 = 0;
      }

      this.trimOpenSessions(var3 + 8);
      File[] var4 = this.listSortedSessionBeginFiles();
      if(var4.length <= var3) {
         c.h().a("CrashlyticsCore", "No open sessions to be closed.");
      } else {
         this.writeSessionUser(getSessionIdFromSessionFile(var4[var3]));
         if(var1 == null) {
            c.h().a("CrashlyticsCore", "Unable to close session. Settings are not loaded.");
         } else {
            this.closeOpenSessions(var4, var3, var1.c);
         }
      }

   }

   private void doOpenSession() throws Exception {
      Date var1 = new Date();
      String var2 = (new CLSUUID(this.idManager)).toString();
      c.h().a("CrashlyticsCore", "Opening a new session with ID " + var2);
      this.writeBeginSession(var2, var1);
      this.writeSessionApp(var2);
      this.writeSessionOS(var2);
      this.writeSessionDevice(var2);
      this.logFileManager.setCurrentSession(var2);
   }

   private void doWriteExternalCrashEvent(SessionEventData param1) throws IOException {
      // $FF: Couldn't be decompiled
   }

   private void doWriteNonFatal(Date param1, Thread param2, Throwable param3) {
      // $FF: Couldn't be decompiled
   }

   private File[] ensureFileArrayNotNull(File[] var1) {
      File[] var2 = var1;
      if(var1 == null) {
         var2 = new File[0];
      }

      return var2;
   }

   private CreateReportSpiCall getCreateReportSpiCall(String var1) {
      String var2 = i.b(this.crashlyticsCore.getContext(), "com.crashlytics.ApiEndpoint");
      return new DefaultCreateReportSpiCall(this.crashlyticsCore, var2, var1, this.httpRequestFactory);
   }

   private String getCurrentSessionId() {
      File[] var1 = this.listSortedSessionBeginFiles();
      String var2;
      if(var1.length > 0) {
         var2 = getSessionIdFromSessionFile(var1[0]);
      } else {
         var2 = null;
      }

      return var2;
   }

   private String getPreviousSessionId() {
      File[] var1 = this.listSortedSessionBeginFiles();
      String var2;
      if(var1.length > 1) {
         var2 = getSessionIdFromSessionFile(var1[1]);
      } else {
         var2 = null;
      }

      return var2;
   }

   static String getSessionIdFromSessionFile(File var0) {
      return var0.getName().substring(0, 35);
   }

   private File[] getTrimmedNonFatalFiles(String var1, File[] var2, int var3) {
      File[] var4 = var2;
      if(var2.length > var3) {
         c.h().a("CrashlyticsCore", String.format(Locale.US, "Trimming down to %d logged exceptions.", new Object[]{Integer.valueOf(var3)}));
         this.trimSessionEventFiles(var1, var3);
         var4 = this.listFilesMatching(new CrashlyticsController.FileNameContainsFilter(var1 + "SessionEvent"));
      }

      return var4;
   }

   private UserMetaData getUserMetaData(String var1) {
      UserMetaData var2;
      if(this.isHandlingException()) {
         var2 = new UserMetaData(this.crashlyticsCore.getUserIdentifier(), this.crashlyticsCore.getUserName(), this.crashlyticsCore.getUserEmail());
      } else {
         var2 = (new MetaDataStore(this.getFilesDir())).readUserData(var1);
      }

      return var2;
   }

   private File[] listFiles(File var1) {
      return this.ensureFileArrayNotNull(var1.listFiles());
   }

   private File[] listFilesMatching(File var1, FilenameFilter var2) {
      return this.ensureFileArrayNotNull(var1.listFiles(var2));
   }

   private File[] listFilesMatching(FilenameFilter var1) {
      return this.listFilesMatching(this.getFilesDir(), var1);
   }

   private File[] listSessionPartFilesFor(String var1) {
      return this.listFilesMatching(new CrashlyticsController.SessionPartFileFilter(var1));
   }

   private File[] listSortedSessionBeginFiles() {
      File[] var1 = this.listSessionBeginFiles();
      Arrays.sort(var1, LARGEST_FILE_NAME_FIRST);
      return var1;
   }

   private static void recordFatalExceptionAnswersEvent(String var0, String var1) {
      Answers var2 = (Answers)c.a(Answers.class);
      if(var2 == null) {
         c.h().a("CrashlyticsCore", "Answers is not available");
      } else {
         var2.onException(new j.a(var0, var1));
      }

   }

   private static void recordLoggedExceptionAnswersEvent(String var0, String var1) {
      Answers var2 = (Answers)c.a(Answers.class);
      if(var2 == null) {
         c.h().a("CrashlyticsCore", "Answers is not available");
      } else {
         var2.onException(new j.b(var0, var1));
      }

   }

   private void retainSessions(File[] var1, Set var2) {
      int var4 = var1.length;

      for(int var3 = 0; var3 < var4; ++var3) {
         File var7 = var1[var3];
         String var6 = var7.getName();
         Matcher var5 = SESSION_FILE_PATTERN.matcher(var6);
         if(!var5.matches()) {
            c.h().a("CrashlyticsCore", "Deleting unknown file: " + var6);
            var7.delete();
            break;
         }

         if(!var2.contains(var5.group(1))) {
            c.h().a("CrashlyticsCore", "Trimming session file: " + var6);
            var7.delete();
         }
      }

   }

   private void sendSessionReports(t var1) {
      if(var1 == null) {
         c.h().d("CrashlyticsCore", "Cannot send reports. Settings are unavailable.");
      } else {
         Context var4 = this.crashlyticsCore.getContext();
         CreateReportSpiCall var7 = this.getCreateReportSpiCall(var1.a.d);
         ReportUploader var6 = new ReportUploader(this.appData.apiKey, var7, this.reportFilesProvider, this.handlingExceptionCheck);
         File[] var5 = this.listCompleteSessionFiles();
         int var3 = var5.length;

         for(int var2 = 0; var2 < var3; ++var2) {
            SessionReport var8 = new SessionReport(var5[var2], SEND_AT_CRASHTIME_HEADER);
            this.backgroundWorker.submit((Runnable)(new CrashlyticsController.SendReportRunnable(var4, var8, var6)));
         }
      }

   }

   private boolean shouldPromptUserBeforeSendingCrashReports(t var1) {
      boolean var3 = false;
      boolean var2;
      if(var1 == null) {
         var2 = var3;
      } else {
         var2 = var3;
         if(var1.d.a) {
            var2 = var3;
            if(!this.preferenceManager.shouldAlwaysSendReports()) {
               var2 = true;
            }
         }
      }

      return var2;
   }

   private void synthesizeSessionFile(File param1, String param2, File[] param3, File param4) {
      // $FF: Couldn't be decompiled
   }

   private void trimInvalidSessionFiles() {
      File var2 = this.getInvalidFilesDir();
      if(var2.exists()) {
         File[] var3 = this.listFilesMatching(var2, new CrashlyticsController.InvalidPartFileFilter());
         Arrays.sort(var3, Collections.reverseOrder());
         HashSet var4 = new HashSet();

         for(int var1 = 0; var1 < var3.length && var4.size() < 4; ++var1) {
            var4.add(getSessionIdFromSessionFile(var3[var1]));
         }

         this.retainSessions(this.listFiles(var2), var4);
      }

   }

   private void trimOpenSessions(int var1) {
      HashSet var4 = new HashSet();
      File[] var3 = this.listSortedSessionBeginFiles();
      int var2 = Math.min(var1, var3.length);

      for(var1 = 0; var1 < var2; ++var1) {
         var4.add(getSessionIdFromSessionFile(var3[var1]));
      }

      this.logFileManager.discardOldLogFiles(var4);
      this.retainSessions(this.listFilesMatching(new CrashlyticsController.AnySessionPartFileFilter(null)), var4);
   }

   private void trimSessionEventFiles(String var1, int var2) {
      Utils.capFileCount(this.getFilesDir(), new CrashlyticsController.FileNameContainsFilter(var1 + "SessionEvent"), var2, SMALLEST_FILE_NAME_FIRST);
   }

   private void writeBeginSession(String param1, Date param2) throws Exception {
      // $FF: Couldn't be decompiled
   }

   private void writeFatal(Date param1, Thread param2, Throwable param3) {
      // $FF: Couldn't be decompiled
   }

   private void writeInitialPartsTo(CodedOutputStream var1, String var2) throws IOException {
      String[] var5 = INITIAL_SESSION_PART_TAGS;
      int var4 = var5.length;

      for(int var3 = 0; var3 < var4; ++var3) {
         String var6 = var5[var3];
         File[] var7 = this.listFilesMatching(new CrashlyticsController.FileNameContainsFilter(var2 + var6));
         if(var7.length == 0) {
            c.h().e("CrashlyticsCore", "Can't find " + var6 + " data for session ID " + var2, (Throwable)null);
         } else {
            c.h().a("CrashlyticsCore", "Collecting " + var6 + " data for session ID " + var2);
            writeToCosFromFile(var1, var7[0]);
         }
      }

   }

   private static void writeNonFatalEventsTo(CodedOutputStream var0, File[] var1, String var2) {
      Arrays.sort(var1, i.a);
      int var4 = var1.length;

      for(int var3 = 0; var3 < var4; ++var3) {
         File var5 = var1[var3];

         try {
            c.h().a("CrashlyticsCore", String.format(Locale.US, "Found Non Fatal for session ID %s in %s ", new Object[]{var2, var5.getName()}));
            writeToCosFromFile(var0, var5);
         } catch (Exception var6) {
            c.h().e("CrashlyticsCore", "Error writting non-fatal to session.", var6);
         }
      }

   }

   private void writeSessionApp(String param1) throws Exception {
      // $FF: Couldn't be decompiled
   }

   private void writeSessionDevice(String param1) throws Exception {
      // $FF: Couldn't be decompiled
   }

   private void writeSessionEvent(CodedOutputStream var1, Date var2, Thread var3, Throwable var4, String var5, boolean var6) throws Exception {
      TrimmedThrowableData var19 = new TrimmedThrowableData(var4, this.stackTraceTrimmingStrategy);
      Context var26 = this.crashlyticsCore.getContext();
      long var10 = var2.getTime() / 1000L;
      Float var23 = i.c(var26);
      int var9 = i.a(var26, this.devicePowerStateListener.isPowerConnected());
      boolean var18 = i.d(var26);
      int var8 = var26.getResources().getConfiguration().orientation;
      long var12 = i.b();
      long var16 = i.b(var26);
      long var14 = i.b(Environment.getDataDirectory().getPath());
      RunningAppProcessInfo var24 = i.a(var26.getPackageName(), var26);
      LinkedList var25 = new LinkedList();
      StackTraceElement[] var22 = var19.stacktrace;
      String var21 = this.appData.buildId;
      String var20 = this.idManager.c();
      Thread[] var31;
      if(var6) {
         Map var30 = Thread.getAllStackTraces();
         Thread[] var28 = new Thread[var30.size()];
         Iterator var27 = var30.entrySet().iterator();
         int var7 = 0;

         while(true) {
            var31 = var28;
            if(!var27.hasNext()) {
               break;
            }

            Entry var32 = (Entry)var27.next();
            var28[var7] = (Thread)var32.getKey();
            var25.add(this.stackTraceTrimmingStrategy.getTrimmedStackTrace((StackTraceElement[])var32.getValue()));
            ++var7;
         }
      } else {
         var31 = new Thread[0];
      }

      Object var29;
      if(!i.a(var26, "com.crashlytics.CollectCustomKeys", true)) {
         var29 = new TreeMap();
      } else {
         var29 = this.crashlyticsCore.getAttributes();
         if(var29 != null && ((Map)var29).size() > 1) {
            var29 = new TreeMap((Map)var29);
         }
      }

      SessionProtobufHelper.writeSessionEvent(var1, var10, var5, var19, var3, var22, var31, var25, (Map)var29, this.logFileManager, var24, var8, var20, var21, var23, var9, var18, var12 - var16, var14);
   }

   private void writeSessionOS(String param1) throws Exception {
      // $FF: Couldn't be decompiled
   }

   private void writeSessionPartsToSessionFile(File var1, String var2, int var3) {
      c.h().a("CrashlyticsCore", "Collecting session parts for ID " + var2);
      File[] var6 = this.listFilesMatching(new CrashlyticsController.FileNameContainsFilter(var2 + "SessionCrash"));
      boolean var4;
      if(var6 != null && var6.length > 0) {
         var4 = true;
      } else {
         var4 = false;
      }

      c.h().a("CrashlyticsCore", String.format(Locale.US, "Session %s has fatal exception: %s", new Object[]{var2, Boolean.valueOf(var4)}));
      File[] var7 = this.listFilesMatching(new CrashlyticsController.FileNameContainsFilter(var2 + "SessionEvent"));
      boolean var5;
      if(var7 != null && var7.length > 0) {
         var5 = true;
      } else {
         var5 = false;
      }

      c.h().a("CrashlyticsCore", String.format(Locale.US, "Session %s has non-fatal exceptions: %s", new Object[]{var2, Boolean.valueOf(var5)}));
      if(!var4 && !var5) {
         c.h().a("CrashlyticsCore", "No events present for session ID " + var2);
      } else {
         var7 = this.getTrimmedNonFatalFiles(var2, var7, var3);
         File var8;
         if(var4) {
            var8 = var6[0];
         } else {
            var8 = null;
         }

         this.synthesizeSessionFile(var1, var2, var7, var8);
      }

      c.h().a("CrashlyticsCore", "Removing session part files for ID " + var2);
      this.deleteSessionPartFilesFor(var2);
   }

   private void writeSessionUser(String param1) throws Exception {
      // $FF: Couldn't be decompiled
   }

   private static void writeToCosFromFile(CodedOutputStream var0, File var1) throws IOException {
      if(!var1.exists()) {
         c.h().e("CrashlyticsCore", "Tried to include a file that doesn't exist: " + var1.getName(), (Throwable)null);
      } else {
         boolean var7 = false;

         FileInputStream var2;
         try {
            var7 = true;
            var2 = new FileInputStream(var1);
            var7 = false;
         } finally {
            if(var7) {
               var1 = null;
               i.a((Closeable)var1, (String)"Failed to close file input stream.");
            }
         }

         try {
            copyToCodedOutputStream(var2, var0, (int)var1.length());
         } finally {
            ;
         }

         i.a((Closeable)var2, (String)"Failed to close file input stream.");
      }

   }

   void cacheKeyData(final Map var1) {
      this.backgroundWorker.submit(new Callable() {
         public Void call() throws Exception {
            String var1x = CrashlyticsController.this.getCurrentSessionId();
            (new MetaDataStore(CrashlyticsController.this.getFilesDir())).writeKeyData(var1x, var1);
            return null;
         }
      });
   }

   void cacheUserData(final String var1, final String var2, final String var3) {
      this.backgroundWorker.submit(new Callable() {
         public Void call() throws Exception {
            String var1x = CrashlyticsController.this.getCurrentSessionId();
            (new MetaDataStore(CrashlyticsController.this.getFilesDir())).writeUserData(var1x, new UserMetaData(var1, var2, var3));
            return null;
         }
      });
   }

   void cleanInvalidTempFiles() {
      this.backgroundWorker.submit(new Runnable() {
         public void run() {
            CrashlyticsController.this.doCleanInvalidTempFiles(CrashlyticsController.this.listFilesMatching(new CrashlyticsController.InvalidPartFileFilter()));
         }
      });
   }

   void doCleanInvalidTempFiles(File[] var1) {
      byte var3 = 0;
      final HashSet var5 = new HashSet();
      int var4 = var1.length;

      int var2;
      for(var2 = 0; var2 < var4; ++var2) {
         File var6 = var1[var2];
         c.h().a("CrashlyticsCore", "Found invalid session part file: " + var6);
         var5.add(getSessionIdFromSessionFile(var6));
      }

      if(!var5.isEmpty()) {
         File var7 = this.getInvalidFilesDir();
         if(!var7.exists()) {
            var7.mkdir();
         }

         File[] var9 = this.listFilesMatching(new FilenameFilter() {
            public boolean accept(File var1, String var2) {
               boolean var3 = false;
               if(var2.length() >= 35) {
                  var3 = var5.contains(var2.substring(0, 35));
               }

               return var3;
            }
         });
         var4 = var9.length;

         for(var2 = var3; var2 < var4; ++var2) {
            File var8 = var9[var2];
            c.h().a("CrashlyticsCore", "Moving session file: " + var8);
            if(!var8.renameTo(new File(var7, var8.getName()))) {
               c.h().a("CrashlyticsCore", "Could not move session file. Deleting " + var8);
               var8.delete();
            }
         }

         this.trimInvalidSessionFiles();
      }

   }

   void doCloseSessions(p var1) throws Exception {
      this.doCloseSessions(var1, false);
   }

   void enableExceptionHandling(UncaughtExceptionHandler var1) {
      this.openSession();
      this.crashHandler = new CrashlyticsUncaughtExceptionHandler(new CrashlyticsUncaughtExceptionHandler.CrashListener() {
         public void onUncaughtException(Thread var1, Throwable var2) {
            CrashlyticsController.this.handleUncaughtException(var1, var2);
         }
      }, var1);
      Thread.setDefaultUncaughtExceptionHandler(this.crashHandler);
   }

   boolean finalizeSessions(final p var1) {
      return ((Boolean)this.backgroundWorker.submitAndWait(new Callable() {
         public Boolean call() throws Exception {
            Boolean var1x;
            if(CrashlyticsController.this.isHandlingException()) {
               c.h().a("CrashlyticsCore", "Skipping session finalization because a crash has already occurred.");
               var1x = Boolean.FALSE;
            } else {
               c.h().a("CrashlyticsCore", "Finalizing previously open sessions.");
               CrashlyticsController.this.doCloseSessions(var1, true);
               c.h().a("CrashlyticsCore", "Closed all previously open sessions");
               var1x = Boolean.TRUE;
            }

            return var1x;
         }
      })).booleanValue();
   }

   File getFatalSessionFilesDir() {
      return new File(this.getFilesDir(), "fatal-sessions");
   }

   File getFilesDir() {
      return this.fileStore.a();
   }

   File getInvalidFilesDir() {
      return new File(this.getFilesDir(), "invalidClsFiles");
   }

   File getNonFatalSessionFilesDir() {
      return new File(this.getFilesDir(), "nonfatal-sessions");
   }

   void handleUncaughtException(final Thread var1, final Throwable var2) {
      synchronized(this){}

      try {
         k var4 = c.h();
         StringBuilder var3 = new StringBuilder();
         var4.a("CrashlyticsCore", var3.append("Crashlytics is handling uncaught exception \"").append(var2).append("\" from thread ").append(var1.getName()).toString());
         this.devicePowerStateListener.dispose();
         final Date var8 = new Date();
         CrashlyticsBackgroundWorker var5 = this.backgroundWorker;
         Callable var9 = new Callable() {
            public Void call() throws Exception {
               CrashlyticsController.this.crashlyticsCore.createCrashMarker();
               CrashlyticsController.this.writeFatal(var8, var1, var2);
               t var2x = q.a().b();
               p var1x;
               if(var2x != null) {
                  var1x = var2x.b;
               } else {
                  var1x = null;
               }

               CrashlyticsController.this.doCloseSessions(var1x);
               CrashlyticsController.this.doOpenSession();
               if(var1x != null) {
                  CrashlyticsController.this.trimSessionFiles(var1x.g);
               }

               if(!CrashlyticsController.this.shouldPromptUserBeforeSendingCrashReports(var2x)) {
                  CrashlyticsController.this.sendSessionReports(var2x);
               }

               return null;
            }
         };
         var5.submitAndWait(var9);
      } finally {
         ;
      }

   }

   boolean hasOpenSession() {
      boolean var1;
      if(this.listSessionBeginFiles().length > 0) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   boolean isHandlingException() {
      boolean var1;
      if(this.crashHandler != null && this.crashHandler.isHandlingException()) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   File[] listCompleteSessionFiles() {
      LinkedList var1 = new LinkedList();
      Collections.addAll(var1, this.listFilesMatching(this.getFatalSessionFilesDir(), SESSION_FILE_FILTER));
      Collections.addAll(var1, this.listFilesMatching(this.getNonFatalSessionFilesDir(), SESSION_FILE_FILTER));
      Collections.addAll(var1, this.listFilesMatching(this.getFilesDir(), SESSION_FILE_FILTER));
      return (File[])var1.toArray(new File[var1.size()]);
   }

   File[] listSessionBeginFiles() {
      return this.listFilesMatching(new CrashlyticsController.FileNameContainsFilter("BeginSession"));
   }

   void openSession() {
      this.backgroundWorker.submit(new Callable() {
         public Void call() throws Exception {
            CrashlyticsController.this.doOpenSession();
            return null;
         }
      });
   }

   void submitAllReports(float var1, t var2) {
      if(var2 == null) {
         c.h().d("CrashlyticsCore", "Could not send reports. Settings are not available.");
      } else {
         CreateReportSpiCall var3 = this.getCreateReportSpiCall(var2.a.d);
         Object var4;
         if(this.shouldPromptUserBeforeSendingCrashReports(var2)) {
            var4 = new CrashlyticsController.PrivacyDialogCheck(this.crashlyticsCore, this.preferenceManager, var2.c);
         } else {
            var4 = new ReportUploader.AlwaysSendCheck();
         }

         (new ReportUploader(this.appData.apiKey, var3, this.reportFilesProvider, this.handlingExceptionCheck)).uploadReports(var1, (ReportUploader.SendCheck)var4);
      }

   }

   void trimSessionFiles(int var1) {
      int var2 = var1 - Utils.capFileCount(this.getFatalSessionFilesDir(), var1, SMALLEST_FILE_NAME_FIRST);
      var1 = Utils.capFileCount(this.getNonFatalSessionFilesDir(), var2, SMALLEST_FILE_NAME_FIRST);
      Utils.capFileCount(this.getFilesDir(), SESSION_FILE_FILTER, var2 - var1, SMALLEST_FILE_NAME_FIRST);
   }

   void writeExternalCrashEvent(final SessionEventData var1) {
      this.backgroundWorker.submit(new Callable() {
         public Void call() throws Exception {
            if(!CrashlyticsController.this.isHandlingException()) {
               CrashlyticsController.this.doWriteExternalCrashEvent(var1);
            }

            return null;
         }
      });
   }

   void writeNonFatalException(final Thread var1, final Throwable var2) {
      final Date var3 = new Date();
      this.backgroundWorker.submit(new Runnable() {
         public void run() {
            if(!CrashlyticsController.this.isHandlingException()) {
               CrashlyticsController.this.doWriteNonFatal(var3, var1, var2);
            }

         }
      });
   }

   void writeToLog(final long var1, final String var3) {
      this.backgroundWorker.submit(new Callable() {
         public Void call() throws Exception {
            if(!CrashlyticsController.this.isHandlingException()) {
               CrashlyticsController.this.logFileManager.writeToLog(var1, var3);
            }

            return null;
         }
      });
   }

   private static class AnySessionPartFileFilter implements FilenameFilter {
      private AnySessionPartFileFilter() {
      }

      // $FF: synthetic method
      AnySessionPartFileFilter(Object var1) {
         this();
      }

      public boolean accept(File var1, String var2) {
         boolean var3;
         if(!CrashlyticsController.SESSION_FILE_FILTER.accept(var1, var2) && CrashlyticsController.SESSION_FILE_PATTERN.matcher(var2).matches()) {
            var3 = true;
         } else {
            var3 = false;
         }

         return var3;
      }
   }

   static class FileNameContainsFilter implements FilenameFilter {
      private final String string;

      public FileNameContainsFilter(String var1) {
         this.string = var1;
      }

      public boolean accept(File var1, String var2) {
         boolean var3;
         if(var2.contains(this.string) && !var2.endsWith(".cls_temp")) {
            var3 = true;
         } else {
            var3 = false;
         }

         return var3;
      }
   }

   static class InvalidPartFileFilter implements FilenameFilter {
      public boolean accept(File var1, String var2) {
         boolean var3;
         if(!ClsFileOutputStream.TEMP_FILENAME_FILTER.accept(var1, var2) && !var2.contains("SessionMissingBinaryImages")) {
            var3 = false;
         } else {
            var3 = true;
         }

         return var3;
      }
   }

   private static final class LogFileDirectoryProvider implements LogFileManager.DirectoryProvider {
      private static final String LOG_FILES_DIR = "log-files";
      private final a rootFileStore;

      public LogFileDirectoryProvider(a var1) {
         this.rootFileStore = var1;
      }

      public File getLogFileDir() {
         File var1 = new File(this.rootFileStore.a(), "log-files");
         if(!var1.exists()) {
            var1.mkdirs();
         }

         return var1;
      }
   }

   private static final class PrivacyDialogCheck implements ReportUploader.SendCheck {
      private final h kit;
      private final PreferenceManager preferenceManager;
      private final io.fabric.sdk.android.services.e.o promptData;

      public PrivacyDialogCheck(h var1, PreferenceManager var2, io.fabric.sdk.android.services.e.o var3) {
         this.kit = var1;
         this.preferenceManager = var2;
         this.promptData = var3;
      }

      public boolean canSendReports() {
         Activity var2 = this.kit.getFabric().b();
         boolean var1;
         if(var2 != null && !var2.isFinishing()) {
            CrashPromptDialog.AlwaysSendCallback var3 = new CrashPromptDialog.AlwaysSendCallback() {
               public void sendUserReportsWithoutPrompting(boolean var1) {
                  PrivacyDialogCheck.this.preferenceManager.setShouldAlwaysSendReports(var1);
               }
            };
            final CrashPromptDialog var4 = CrashPromptDialog.create(var2, this.promptData, var3);
            var2.runOnUiThread(new Runnable() {
               public void run() {
                  var4.show();
               }
            });
            c.h().a("CrashlyticsCore", "Waiting for user opt-in.");
            var4.await();
            var1 = var4.getOptIn();
         } else {
            var1 = true;
         }

         return var1;
      }
   }

   private final class ReportUploaderFilesProvider implements ReportUploader.ReportFilesProvider {
      private ReportUploaderFilesProvider() {
      }

      // $FF: synthetic method
      ReportUploaderFilesProvider(Object var2) {
         this();
      }

      public File[] getCompleteSessionFiles() {
         return CrashlyticsController.this.listCompleteSessionFiles();
      }

      public File[] getInvalidSessionFiles() {
         return CrashlyticsController.this.getInvalidFilesDir().listFiles();
      }
   }

   private final class ReportUploaderHandlingExceptionCheck implements ReportUploader.HandlingExceptionCheck {
      private ReportUploaderHandlingExceptionCheck() {
      }

      // $FF: synthetic method
      ReportUploaderHandlingExceptionCheck(Object var2) {
         this();
      }

      public boolean isHandlingException() {
         return CrashlyticsController.this.isHandlingException();
      }
   }

   private static final class SendReportRunnable implements Runnable {
      private final Context context;
      private final Report report;
      private final ReportUploader reportUploader;

      public SendReportRunnable(Context var1, Report var2, ReportUploader var3) {
         this.context = var1;
         this.report = var2;
         this.reportUploader = var3;
      }

      public void run() {
         if(i.n(this.context)) {
            c.h().a("CrashlyticsCore", "Attempting to send crash report at time of crash...");
            this.reportUploader.forceUpload(this.report);
         }

      }
   }

   static class SessionPartFileFilter implements FilenameFilter {
      private final String sessionId;

      public SessionPartFileFilter(String var1) {
         this.sessionId = var1;
      }

      public boolean accept(File var1, String var2) {
         boolean var4 = false;
         boolean var3;
         if(var2.equals(this.sessionId + ".cls")) {
            var3 = var4;
         } else {
            var3 = var4;
            if(var2.contains(this.sessionId)) {
               var3 = var4;
               if(!var2.endsWith(".cls_temp")) {
                  var3 = true;
               }
            }
         }

         return var3;
      }
   }
}
