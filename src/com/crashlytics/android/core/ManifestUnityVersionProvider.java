package com.crashlytics.android.core;

import android.content.Context;

class ManifestUnityVersionProvider implements UnityVersionProvider {
   static final String FABRIC_UNITY_CRASHLYTICS_VERSION_KEY = "io.fabric.unity.crashlytics.version";
   private final Context context;
   private final String packageName;

   public ManifestUnityVersionProvider(Context var1, String var2) {
      this.context = var1;
      this.packageName = var2;
   }

   public String getUnityVersion() {
      // $FF: Couldn't be decompiled
   }
}
