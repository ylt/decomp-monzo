package com.crashlytics.android.core;

import io.fabric.sdk.android.h;
import io.fabric.sdk.android.services.b.a;
import io.fabric.sdk.android.services.b.r;
import io.fabric.sdk.android.services.network.HttpRequest;
import io.fabric.sdk.android.services.network.c;
import io.fabric.sdk.android.services.network.d;
import java.io.File;
import java.util.Iterator;
import java.util.Map.Entry;

class DefaultCreateReportSpiCall extends a implements CreateReportSpiCall {
   static final String FILE_CONTENT_TYPE = "application/octet-stream";
   static final String FILE_PARAM = "report[file]";
   static final String IDENTIFIER_PARAM = "report[identifier]";
   static final String MULTI_FILE_PARAM = "report[file";

   public DefaultCreateReportSpiCall(h var1, String var2, String var3, d var4) {
      super(var1, var2, var3, var4, c.b);
   }

   DefaultCreateReportSpiCall(h var1, String var2, String var3, d var4, c var5) {
      super(var1, var2, var3, var4, var5);
   }

   private HttpRequest applyHeadersTo(HttpRequest var1, CreateReportRequest var2) {
      var1 = var1.a("X-CRASHLYTICS-API-KEY", var2.apiKey).a("X-CRASHLYTICS-API-CLIENT-TYPE", "android").a("X-CRASHLYTICS-API-CLIENT-VERSION", this.kit.getVersion());

      for(Iterator var3 = var2.report.getCustomHeaders().entrySet().iterator(); var3.hasNext(); var1 = var1.a((Entry)var3.next())) {
         ;
      }

      return var1;
   }

   private HttpRequest applyMultipartDataTo(HttpRequest var1, Report var2) {
      int var4 = 0;
      var1.e("report[identifier]", var2.getIdentifier());
      HttpRequest var6;
      if(var2.getFiles().length == 1) {
         io.fabric.sdk.android.c.h().a("CrashlyticsCore", "Adding single file " + var2.getFileName() + " to report " + var2.getIdentifier());
         var6 = var1.a("report[file]", var2.getFileName(), "application/octet-stream", var2.getFile());
      } else {
         File[] var7 = var2.getFiles();
         int var5 = var7.length;
         int var3 = 0;

         while(true) {
            var6 = var1;
            if(var4 >= var5) {
               break;
            }

            File var8 = var7[var4];
            io.fabric.sdk.android.c.h().a("CrashlyticsCore", "Adding file " + var8.getName() + " to report " + var2.getIdentifier());
            var1.a("report[file" + var3 + "]", var8.getName(), "application/octet-stream", var8);
            ++var3;
            ++var4;
         }
      }

      return var6;
   }

   public boolean invoke(CreateReportRequest var1) {
      HttpRequest var4 = this.applyMultipartDataTo(this.applyHeadersTo(this.getHttpRequest(), var1), var1.report);
      io.fabric.sdk.android.c.h().a("CrashlyticsCore", "Sending report to: " + this.getUrl());
      int var2 = var4.b();
      io.fabric.sdk.android.c.h().a("CrashlyticsCore", "Create report request ID: " + var4.b("X-REQUEST-ID"));
      io.fabric.sdk.android.c.h().a("CrashlyticsCore", "Result was: " + var2);
      boolean var3;
      if(r.a(var2) == 0) {
         var3 = true;
      } else {
         var3 = false;
      }

      return var3;
   }
}
