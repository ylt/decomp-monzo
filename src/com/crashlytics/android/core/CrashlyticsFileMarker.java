package com.crashlytics.android.core;

import io.fabric.sdk.android.c;
import io.fabric.sdk.android.services.d.a;
import java.io.File;
import java.io.IOException;

class CrashlyticsFileMarker {
   private final a fileStore;
   private final String markerName;

   public CrashlyticsFileMarker(String var1, a var2) {
      this.markerName = var1;
      this.fileStore = var2;
   }

   private File getMarkerFile() {
      return new File(this.fileStore.a(), this.markerName);
   }

   public boolean create() {
      boolean var2 = false;

      boolean var1;
      try {
         var1 = this.getMarkerFile().createNewFile();
      } catch (IOException var4) {
         c.h().e("CrashlyticsCore", "Error creating marker: " + this.markerName, var4);
         var1 = var2;
      }

      return var1;
   }

   public boolean isPresent() {
      return this.getMarkerFile().exists();
   }

   public boolean remove() {
      return this.getMarkerFile().delete();
   }
}
