package com.crashlytics.android.core;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import io.fabric.sdk.android.services.b.o;

class AppData {
   public final String apiKey;
   public final String buildId;
   public final String installerPackageName;
   public final String packageName;
   public final String versionCode;
   public final String versionName;

   AppData(String var1, String var2, String var3, String var4, String var5, String var6) {
      this.apiKey = var1;
      this.buildId = var2;
      this.installerPackageName = var3;
      this.packageName = var4;
      this.versionCode = var5;
      this.versionName = var6;
   }

   public static AppData create(Context var0, o var1, String var2, String var3) throws NameNotFoundException {
      String var4 = var0.getPackageName();
      String var8 = var1.j();
      PackageInfo var6 = var0.getPackageManager().getPackageInfo(var4, 0);
      String var5 = Integer.toString(var6.versionCode);
      String var7;
      if(var6.versionName == null) {
         var7 = "0.0";
      } else {
         var7 = var6.versionName;
      }

      return new AppData(var2, var3, var8, var4, var5, var7);
   }
}
