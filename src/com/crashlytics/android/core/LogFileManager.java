package com.crashlytics.android.core;

import android.content.Context;
import io.fabric.sdk.android.c;
import io.fabric.sdk.android.services.b.i;
import java.io.File;
import java.util.Set;

class LogFileManager {
   private static final String COLLECT_CUSTOM_LOGS = "com.crashlytics.CollectCustomLogs";
   private static final String LOGFILE_EXT = ".temp";
   private static final String LOGFILE_PREFIX = "crashlytics-userlog-";
   static final int MAX_LOG_SIZE = 65536;
   private static final LogFileManager.NoopLogStore NOOP_LOG_STORE = new LogFileManager.NoopLogStore();
   private final Context context;
   private FileLogStore currentLog;
   private final LogFileManager.DirectoryProvider directoryProvider;

   LogFileManager(Context var1, LogFileManager.DirectoryProvider var2) {
      this(var1, var2, (String)null);
   }

   LogFileManager(Context var1, LogFileManager.DirectoryProvider var2, String var3) {
      this.context = var1;
      this.directoryProvider = var2;
      this.currentLog = NOOP_LOG_STORE;
      this.setCurrentSession(var3);
   }

   private String getSessionIdForFile(File var1) {
      String var3 = var1.getName();
      int var2 = var3.lastIndexOf(".temp");
      if(var2 != -1) {
         var3 = var3.substring("crashlytics-userlog-".length(), var2);
      }

      return var3;
   }

   private File getWorkingFileForSession(String var1) {
      var1 = "crashlytics-userlog-" + var1 + ".temp";
      return new File(this.directoryProvider.getLogFileDir(), var1);
   }

   void clearLog() {
      this.currentLog.deleteLogFile();
   }

   void discardOldLogFiles(Set var1) {
      File[] var5 = this.directoryProvider.getLogFileDir().listFiles();
      if(var5 != null) {
         int var3 = var5.length;

         for(int var2 = 0; var2 < var3; ++var2) {
            File var4 = var5[var2];
            if(!var1.contains(this.getSessionIdForFile(var4))) {
               var4.delete();
            }
         }
      }

   }

   ByteString getByteStringForLog() {
      return this.currentLog.getLogAsByteString();
   }

   final void setCurrentSession(String var1) {
      this.currentLog.closeLogFile();
      this.currentLog = NOOP_LOG_STORE;
      if(var1 != null) {
         if(!i.a(this.context, "com.crashlytics.CollectCustomLogs", true)) {
            c.h().a("CrashlyticsCore", "Preferences requested no custom logs. Aborting log file creation.");
         } else {
            this.setLogFile(this.getWorkingFileForSession(var1), 65536);
         }
      }

   }

   void setLogFile(File var1, int var2) {
      this.currentLog = new QueueFileLogStore(var1, var2);
   }

   void writeToLog(long var1, String var3) {
      this.currentLog.writeToLog(var1, var3);
   }

   public interface DirectoryProvider {
      File getLogFileDir();
   }

   private static final class NoopLogStore implements FileLogStore {
      private NoopLogStore() {
      }

      // $FF: synthetic method
      NoopLogStore(Object var1) {
         this();
      }

      public void closeLogFile() {
      }

      public void deleteLogFile() {
      }

      public ByteString getLogAsByteString() {
         return null;
      }

      public void writeToLog(long var1, String var3) {
      }
   }
}
