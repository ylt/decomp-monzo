package com.crashlytics.android.core;

import io.fabric.sdk.android.c;
import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

class InvalidSessionReport implements Report {
   private final Map customHeaders;
   private final File[] files;
   private final String identifier;

   public InvalidSessionReport(String var1, File[] var2) {
      this.files = var2;
      this.customHeaders = new HashMap(ReportUploader.HEADER_INVALID_CLS_FILE);
      this.identifier = var1;
   }

   public Map getCustomHeaders() {
      return Collections.unmodifiableMap(this.customHeaders);
   }

   public File getFile() {
      return this.files[0];
   }

   public String getFileName() {
      return this.files[0].getName();
   }

   public File[] getFiles() {
      return this.files;
   }

   public String getIdentifier() {
      return this.identifier;
   }

   public void remove() {
      File[] var3 = this.files;
      int var2 = var3.length;

      for(int var1 = 0; var1 < var2; ++var1) {
         File var4 = var3[var1];
         c.h().a("CrashlyticsCore", "Removing invalid report file at " + var4.getPath());
         var4.delete();
      }

   }
}
