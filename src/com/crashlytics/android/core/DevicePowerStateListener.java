package com.crashlytics.android.core;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import java.util.concurrent.atomic.AtomicBoolean;

class DevicePowerStateListener {
   private static final IntentFilter FILTER_BATTERY_CHANGED = new IntentFilter("android.intent.action.BATTERY_CHANGED");
   private static final IntentFilter FILTER_POWER_CONNECTED = new IntentFilter("android.intent.action.ACTION_POWER_CONNECTED");
   private static final IntentFilter FILTER_POWER_DISCONNECTED = new IntentFilter("android.intent.action.ACTION_POWER_DISCONNECTED");
   private final Context context;
   private boolean isPowerConnected;
   private final BroadcastReceiver powerConnectedReceiver;
   private final BroadcastReceiver powerDisconnectedReceiver;
   private final AtomicBoolean receiversRegistered;

   public DevicePowerStateListener(Context var1) {
      int var2 = -1;
      super();
      this.context = var1;
      Intent var4 = var1.registerReceiver((BroadcastReceiver)null, FILTER_BATTERY_CHANGED);
      if(var4 != null) {
         var2 = var4.getIntExtra("status", -1);
      }

      boolean var3;
      if(var2 != 2 && var2 != 5) {
         var3 = false;
      } else {
         var3 = true;
      }

      this.isPowerConnected = var3;
      this.powerConnectedReceiver = new BroadcastReceiver() {
         public void onReceive(Context var1, Intent var2) {
            DevicePowerStateListener.this.isPowerConnected = true;
         }
      };
      this.powerDisconnectedReceiver = new BroadcastReceiver() {
         public void onReceive(Context var1, Intent var2) {
            DevicePowerStateListener.this.isPowerConnected = false;
         }
      };
      var1.registerReceiver(this.powerConnectedReceiver, FILTER_POWER_CONNECTED);
      var1.registerReceiver(this.powerDisconnectedReceiver, FILTER_POWER_DISCONNECTED);
      this.receiversRegistered = new AtomicBoolean(true);
   }

   public void dispose() {
      if(this.receiversRegistered.getAndSet(false)) {
         this.context.unregisterReceiver(this.powerConnectedReceiver);
         this.context.unregisterReceiver(this.powerDisconnectedReceiver);
      }

   }

   public boolean isPowerConnected() {
      return this.isPowerConnected;
   }
}
