package com.crashlytics.android.core;

import android.annotation.SuppressLint;
import io.fabric.sdk.android.services.d.c;
import io.fabric.sdk.android.services.d.d;

@SuppressLint({"CommitPrefEdits"})
class PreferenceManager {
   static final String PREF_ALWAYS_SEND_REPORTS_KEY = "always_send_reports_opt_in";
   private static final String PREF_MIGRATION_COMPLETE = "preferences_migration_complete";
   private static final boolean SHOULD_ALWAYS_SEND_REPORTS_DEFAULT = false;
   private final c preferenceStore;

   public PreferenceManager(c var1) {
      this.preferenceStore = var1;
   }

   public static PreferenceManager create(c var0, CrashlyticsCore var1) {
      if(!var0.a().getBoolean("preferences_migration_complete", false)) {
         d var4 = new d(var1);
         boolean var2;
         if(!var0.a().contains("always_send_reports_opt_in") && var4.a().contains("always_send_reports_opt_in")) {
            var2 = true;
         } else {
            var2 = false;
         }

         if(var2) {
            boolean var3 = var4.a().getBoolean("always_send_reports_opt_in", false);
            var0.a(var0.b().putBoolean("always_send_reports_opt_in", var3));
         }

         var0.a(var0.b().putBoolean("preferences_migration_complete", true));
      }

      return new PreferenceManager(var0);
   }

   void setShouldAlwaysSendReports(boolean var1) {
      this.preferenceStore.a(this.preferenceStore.b().putBoolean("always_send_reports_opt_in", var1));
   }

   boolean shouldAlwaysSendReports() {
      return this.preferenceStore.a().getBoolean("always_send_reports_opt_in", false);
   }
}
