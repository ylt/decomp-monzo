package com.crashlytics.android.core;

import android.os.AsyncTask;
import io.fabric.sdk.android.c;

public class CrashTest {
   private void privateMethodThatThrowsException(String var1) {
      throw new RuntimeException(var1);
   }

   public void crashAsyncTask(final long var1) {
      (new AsyncTask() {
         protected Void doInBackground(Void... var1x) {
            try {
               Thread.sleep(var1);
            } catch (InterruptedException var2) {
               ;
            }

            CrashTest.this.throwRuntimeException("Background thread crash");
            return null;
         }
      }).execute(new Void[]{(Void)null});
   }

   public void indexOutOfBounds() {
      int var1 = (new int[2])[10];
      c.h().a("CrashlyticsCore", "Out of bounds value: " + var1);
   }

   public int stackOverflow() {
      return this.stackOverflow() + (int)Math.random();
   }

   public void throwFiveChainedExceptions() {
      try {
         this.privateMethodThatThrowsException("1");
      } catch (Exception var6) {
         Exception var2 = var6;

         RuntimeException var7;
         try {
            var7 = new RuntimeException("2", var2);
            throw var7;
         } catch (Exception var5) {
            Exception var1 = var5;

            try {
               RuntimeException var8 = new RuntimeException("3", var1);
               throw var8;
            } catch (Exception var4) {
               var2 = var4;

               try {
                  var7 = new RuntimeException("4", var2);
                  throw var7;
               } catch (Exception var3) {
                  throw new RuntimeException("5", var3);
               }
            }
         }
      }
   }

   public void throwRuntimeException(String var1) {
      throw new RuntimeException(var1);
   }
}
