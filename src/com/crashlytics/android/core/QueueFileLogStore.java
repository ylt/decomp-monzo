package com.crashlytics.android.core;

import io.fabric.sdk.android.c;
import io.fabric.sdk.android.services.b.i;
import io.fabric.sdk.android.services.b.q;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

class QueueFileLogStore implements FileLogStore {
   private q logFile;
   private final int maxLogSize;
   private final File workingFile;

   public QueueFileLogStore(File var1, int var2) {
      this.workingFile = var1;
      this.maxLogSize = var2;
   }

   private void doWriteToLog(long param1, String param3) {
      // $FF: Couldn't be decompiled
   }

   private void openLogFile() {
      if(this.logFile == null) {
         try {
            q var1 = new q(this.workingFile);
            this.logFile = var1;
         } catch (IOException var2) {
            c.h().e("CrashlyticsCore", "Could not open log file: " + this.workingFile, var2);
         }
      }

   }

   public void closeLogFile() {
      i.a((Closeable)this.logFile, (String)"There was a problem closing the Crashlytics log file.");
      this.logFile = null;
   }

   public void deleteLogFile() {
      this.closeLogFile();
      this.workingFile.delete();
   }

   public ByteString getLogAsByteString() {
      ByteString var1 = null;
      if(this.workingFile.exists()) {
         this.openLogFile();
         if(this.logFile != null) {
            final int[] var6 = new int[]{0};
            final byte[] var2 = new byte[this.logFile.a()];

            try {
               q var4 = this.logFile;
               q.c var3 = new q.c() {
                  public void read(InputStream var1, int var2x) throws IOException {
                     boolean var5 = false;

                     int[] var3;
                     try {
                        var5 = true;
                        var1.read(var2, var6[0], var2x);
                        var3 = var6;
                        var5 = false;
                     } finally {
                        if(var5) {
                           var1.close();
                        }
                     }

                     var3[0] += var2x;
                     var1.close();
                  }
               };
               var4.a(var3);
            } catch (IOException var5) {
               c.h().e("CrashlyticsCore", "A problem occurred while reading the Crashlytics log file.", var5);
            }

            var1 = ByteString.copyFrom(var2, 0, var6[0]);
         }
      }

      return var1;
   }

   public void writeToLog(long var1, String var3) {
      this.openLogFile();
      this.doWriteToLog(var1, var3);
   }
}
