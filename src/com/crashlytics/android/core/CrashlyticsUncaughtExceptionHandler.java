package com.crashlytics.android.core;

import io.fabric.sdk.android.c;
import java.lang.Thread.UncaughtExceptionHandler;
import java.util.concurrent.atomic.AtomicBoolean;

class CrashlyticsUncaughtExceptionHandler implements UncaughtExceptionHandler {
   private final CrashlyticsUncaughtExceptionHandler.CrashListener crashListener;
   private final UncaughtExceptionHandler defaultHandler;
   private final AtomicBoolean isHandlingException;

   public CrashlyticsUncaughtExceptionHandler(CrashlyticsUncaughtExceptionHandler.CrashListener var1, UncaughtExceptionHandler var2) {
      this.crashListener = var1;
      this.defaultHandler = var2;
      this.isHandlingException = new AtomicBoolean(false);
   }

   boolean isHandlingException() {
      return this.isHandlingException.get();
   }

   public void uncaughtException(Thread var1, Throwable var2) {
      this.isHandlingException.set(true);

      try {
         this.crashListener.onUncaughtException(var1, var2);
      } catch (Exception var6) {
         c.h().e("CrashlyticsCore", "An error occurred in the uncaught exception handler", var6);
      } finally {
         c.h().a("CrashlyticsCore", "Crashlytics completed exception processing. Invoking default exception handler.");
         this.defaultHandler.uncaughtException(var1, var2);
         this.isHandlingException.set(false);
      }

   }

   interface CrashListener {
      void onUncaughtException(Thread var1, Throwable var2);
   }
}
