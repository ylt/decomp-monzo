package com.crashlytics.android.answers;

public class RatingEvent extends PredefinedEvent {
   static final String CONTENT_ID_ATTRIBUTE = "contentId";
   static final String CONTENT_NAME_ATTRIBUTE = "contentName";
   static final String CONTENT_TYPE_ATTRIBUTE = "contentType";
   static final String RATING_ATTRIBUTE = "rating";
   static final String TYPE = "rating";

   String getPredefinedType() {
      return "rating";
   }

   public RatingEvent putContentId(String var1) {
      this.predefinedAttributes.put("contentId", var1);
      return this;
   }

   public RatingEvent putContentName(String var1) {
      this.predefinedAttributes.put("contentName", var1);
      return this;
   }

   public RatingEvent putContentType(String var1) {
      this.predefinedAttributes.put("contentType", var1);
      return this;
   }

   public RatingEvent putRating(int var1) {
      this.predefinedAttributes.put("rating", (Number)Integer.valueOf(var1));
      return this;
   }
}
