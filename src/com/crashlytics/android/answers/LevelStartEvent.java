package com.crashlytics.android.answers;

public class LevelStartEvent extends PredefinedEvent {
   static final String LEVEL_NAME_ATTRIBUTE = "levelName";
   static final String TYPE = "levelStart";

   String getPredefinedType() {
      return "levelStart";
   }

   public LevelStartEvent putLevelName(String var1) {
      this.predefinedAttributes.put("levelName", var1);
      return this;
   }
}
