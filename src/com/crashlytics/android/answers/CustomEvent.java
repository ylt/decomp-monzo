package com.crashlytics.android.answers;

public class CustomEvent extends AnswersEvent {
   private final String eventName;

   public CustomEvent(String var1) {
      if(var1 == null) {
         throw new NullPointerException("eventName must not be null");
      } else {
         this.eventName = this.validator.limitStringLength(var1);
      }
   }

   String getCustomType() {
      return this.eventName;
   }

   public String toString() {
      return "{eventName:\"" + this.eventName + '"' + ", customAttributes:" + this.customAttributes + "}";
   }
}
