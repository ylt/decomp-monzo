package com.crashlytics.android.answers;

import java.math.BigDecimal;
import java.util.Currency;

public class PurchaseEvent extends PredefinedEvent {
   static final String CURRENCY_ATTRIBUTE = "currency";
   static final String ITEM_ID_ATTRIBUTE = "itemId";
   static final String ITEM_NAME_ATTRIBUTE = "itemName";
   static final String ITEM_PRICE_ATTRIBUTE = "itemPrice";
   static final String ITEM_TYPE_ATTRIBUTE = "itemType";
   static final BigDecimal MICRO_CONSTANT = BigDecimal.valueOf(1000000L);
   static final String SUCCESS_ATTRIBUTE = "success";
   static final String TYPE = "purchase";

   String getPredefinedType() {
      return "purchase";
   }

   long priceToMicros(BigDecimal var1) {
      return MICRO_CONSTANT.multiply(var1).longValue();
   }

   public PurchaseEvent putCurrency(Currency var1) {
      if(!this.validator.isNull(var1, "currency")) {
         this.predefinedAttributes.put("currency", var1.getCurrencyCode());
      }

      return this;
   }

   public PurchaseEvent putItemId(String var1) {
      this.predefinedAttributes.put("itemId", var1);
      return this;
   }

   public PurchaseEvent putItemName(String var1) {
      this.predefinedAttributes.put("itemName", var1);
      return this;
   }

   public PurchaseEvent putItemPrice(BigDecimal var1) {
      if(!this.validator.isNull(var1, "itemPrice")) {
         this.predefinedAttributes.put("itemPrice", (Number)Long.valueOf(this.priceToMicros(var1)));
      }

      return this;
   }

   public PurchaseEvent putItemType(String var1) {
      this.predefinedAttributes.put("itemType", var1);
      return this;
   }

   public PurchaseEvent putSuccess(boolean var1) {
      this.predefinedAttributes.put("success", Boolean.toString(var1));
      return this;
   }
}
