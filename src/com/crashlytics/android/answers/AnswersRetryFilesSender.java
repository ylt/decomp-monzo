package com.crashlytics.android.answers;

import io.fabric.sdk.android.services.c.f;
import io.fabric.sdk.android.services.concurrency.a.b;
import io.fabric.sdk.android.services.concurrency.a.c;
import io.fabric.sdk.android.services.concurrency.a.e;
import java.util.List;

class AnswersRetryFilesSender implements f {
   private static final int BACKOFF_MS = 1000;
   private static final int BACKOFF_POWER = 8;
   private static final double JITTER_PERCENT = 0.1D;
   private static final int MAX_RETRIES = 5;
   private final SessionAnalyticsFilesSender filesSender;
   private final RetryManager retryManager;

   AnswersRetryFilesSender(SessionAnalyticsFilesSender var1, RetryManager var2) {
      this.filesSender = var1;
      this.retryManager = var2;
   }

   public static AnswersRetryFilesSender build(SessionAnalyticsFilesSender var0) {
      return new AnswersRetryFilesSender(var0, new RetryManager(new e(new RandomBackoff(new c(1000L, 8), 0.1D), new b(5))));
   }

   public boolean send(List var1) {
      boolean var3 = false;
      long var4 = System.nanoTime();
      boolean var2 = var3;
      if(this.retryManager.canRetry(var4)) {
         if(this.filesSender.send(var1)) {
            this.retryManager.reset();
            var2 = true;
         } else {
            this.retryManager.recordRetry(var4);
            var2 = var3;
         }
      }

      return var2;
   }
}
