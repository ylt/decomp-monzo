package com.crashlytics.android.answers;

public class ShareEvent extends PredefinedEvent {
   static final String CONTENT_ID_ATTRIBUTE = "contentId";
   static final String CONTENT_NAME_ATTRIBUTE = "contentName";
   static final String CONTENT_TYPE_ATTRIBUTE = "contentType";
   static final String METHOD_ATTRIBUTE = "method";
   static final String TYPE = "share";

   String getPredefinedType() {
      return "share";
   }

   public ShareEvent putContentId(String var1) {
      this.predefinedAttributes.put("contentId", var1);
      return this;
   }

   public ShareEvent putContentName(String var1) {
      this.predefinedAttributes.put("contentName", var1);
      return this;
   }

   public ShareEvent putContentType(String var1) {
      this.predefinedAttributes.put("contentType", var1);
      return this;
   }

   public ShareEvent putMethod(String var1) {
      this.predefinedAttributes.put("method", var1);
      return this;
   }
}
