package com.crashlytics.android.answers;

import io.fabric.sdk.android.services.c.e;
import io.fabric.sdk.android.services.e.b;

interface SessionAnalyticsManagerStrategy extends e {
   void deleteAllEvents();

   void processEvent(SessionEvent.Builder var1);

   void sendEvents();

   void setAnalyticsSettingsData(b var1, String var2);
}
