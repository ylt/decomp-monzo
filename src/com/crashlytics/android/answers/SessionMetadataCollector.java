package com.crashlytics.android.answers;

import android.content.Context;
import io.fabric.sdk.android.services.b.i;
import io.fabric.sdk.android.services.b.o;
import java.util.Map;
import java.util.UUID;

class SessionMetadataCollector {
   private final Context context;
   private final o idManager;
   private final String versionCode;
   private final String versionName;

   public SessionMetadataCollector(Context var1, o var2, String var3, String var4) {
      this.context = var1;
      this.idManager = var2;
      this.versionCode = var3;
      this.versionName = var4;
   }

   public SessionEventMetadata getMetadata() {
      Map var6 = this.idManager.i();
      String var5 = this.idManager.c();
      String var3 = this.idManager.b();
      String var2 = (String)var6.get(o.a.d);
      String var1 = (String)var6.get(o.a.g);
      Boolean var4 = this.idManager.l();
      String var9 = (String)var6.get(o.a.c);
      String var8 = i.m(this.context);
      String var7 = this.idManager.d();
      String var10 = this.idManager.g();
      return new SessionEventMetadata(var5, UUID.randomUUID().toString(), var3, var2, var1, var4, var9, var8, var7, var10, this.versionCode, this.versionName);
   }
}
