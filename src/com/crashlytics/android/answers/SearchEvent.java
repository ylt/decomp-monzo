package com.crashlytics.android.answers;

public class SearchEvent extends PredefinedEvent {
   static final String QUERY_ATTRIBUTE = "query";
   static final String TYPE = "search";

   String getPredefinedType() {
      return "search";
   }

   public SearchEvent putQuery(String var1) {
      this.predefinedAttributes.put("query", var1);
      return this;
   }
}
