package com.crashlytics.android.answers;

import android.app.Activity;
import android.os.Bundle;
import io.fabric.sdk.android.a;

class AnswersLifecycleCallbacks extends a.b {
   private final SessionAnalyticsManager analyticsManager;
   private final BackgroundManager backgroundManager;

   public AnswersLifecycleCallbacks(SessionAnalyticsManager var1, BackgroundManager var2) {
      this.analyticsManager = var1;
      this.backgroundManager = var2;
   }

   public void onActivityCreated(Activity var1, Bundle var2) {
   }

   public void onActivityDestroyed(Activity var1) {
   }

   public void onActivityPaused(Activity var1) {
      this.analyticsManager.onLifecycle(var1, SessionEvent.Type.PAUSE);
      this.backgroundManager.onActivityPaused();
   }

   public void onActivityResumed(Activity var1) {
      this.analyticsManager.onLifecycle(var1, SessionEvent.Type.RESUME);
      this.backgroundManager.onActivityResumed();
   }

   public void onActivitySaveInstanceState(Activity var1, Bundle var2) {
   }

   public void onActivityStarted(Activity var1) {
      this.analyticsManager.onLifecycle(var1, SessionEvent.Type.START);
   }

   public void onActivityStopped(Activity var1) {
      this.analyticsManager.onLifecycle(var1, SessionEvent.Type.STOP);
   }
}
