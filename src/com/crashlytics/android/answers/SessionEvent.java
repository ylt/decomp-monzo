package com.crashlytics.android.answers;

import android.app.Activity;
import java.util.Collections;
import java.util.Map;

final class SessionEvent {
   static final String ACTIVITY_KEY = "activity";
   static final String EXCEPTION_NAME_KEY = "exceptionName";
   static final String INSTALLED_AT_KEY = "installedAt";
   static final String SESSION_ID_KEY = "sessionId";
   public final Map customAttributes;
   public final String customType;
   public final Map details;
   public final Map predefinedAttributes;
   public final String predefinedType;
   public final SessionEventMetadata sessionEventMetadata;
   private String stringRepresentation;
   public final long timestamp;
   public final SessionEvent.Type type;

   private SessionEvent(SessionEventMetadata var1, long var2, SessionEvent.Type var4, Map var5, String var6, Map var7, String var8, Map var9) {
      this.sessionEventMetadata = var1;
      this.timestamp = var2;
      this.type = var4;
      this.details = var5;
      this.customType = var6;
      this.customAttributes = var7;
      this.predefinedType = var8;
      this.predefinedAttributes = var9;
   }

   // $FF: synthetic method
   SessionEvent(SessionEventMetadata var1, long var2, SessionEvent.Type var4, Map var5, String var6, Map var7, String var8, Map var9, Object var10) {
      this(var1, var2, var4, var5, var6, var7, var8, var9);
   }

   public static SessionEvent.Builder crashEventBuilder(String var0) {
      Map var1 = Collections.singletonMap("sessionId", var0);
      return (new SessionEvent.Builder(SessionEvent.Type.CRASH)).details(var1);
   }

   public static SessionEvent.Builder crashEventBuilder(String var0, String var1) {
      return crashEventBuilder(var0).customAttributes(Collections.singletonMap("exceptionName", var1));
   }

   public static SessionEvent.Builder customEventBuilder(CustomEvent var0) {
      return (new SessionEvent.Builder(SessionEvent.Type.CUSTOM)).customType(var0.getCustomType()).customAttributes(var0.getCustomAttributes());
   }

   public static SessionEvent.Builder installEventBuilder(long var0) {
      return (new SessionEvent.Builder(SessionEvent.Type.INSTALL)).details(Collections.singletonMap("installedAt", String.valueOf(var0)));
   }

   public static SessionEvent.Builder lifecycleEventBuilder(SessionEvent.Type var0, Activity var1) {
      Map var2 = Collections.singletonMap("activity", var1.getClass().getName());
      return (new SessionEvent.Builder(var0)).details(var2);
   }

   public static SessionEvent.Builder predefinedEventBuilder(PredefinedEvent var0) {
      return (new SessionEvent.Builder(SessionEvent.Type.PREDEFINED)).predefinedType(var0.getPredefinedType()).predefinedAttributes(var0.getPredefinedAttributes()).customAttributes(var0.getCustomAttributes());
   }

   public String toString() {
      if(this.stringRepresentation == null) {
         this.stringRepresentation = "[" + this.getClass().getSimpleName() + ": " + "timestamp=" + this.timestamp + ", type=" + this.type + ", details=" + this.details + ", customType=" + this.customType + ", customAttributes=" + this.customAttributes + ", predefinedType=" + this.predefinedType + ", predefinedAttributes=" + this.predefinedAttributes + ", metadata=[" + this.sessionEventMetadata + "]]";
      }

      return this.stringRepresentation;
   }

   static class Builder {
      Map customAttributes;
      String customType;
      Map details;
      Map predefinedAttributes;
      String predefinedType;
      final long timestamp;
      final SessionEvent.Type type;

      public Builder(SessionEvent.Type var1) {
         this.type = var1;
         this.timestamp = System.currentTimeMillis();
         this.details = null;
         this.customType = null;
         this.customAttributes = null;
         this.predefinedType = null;
         this.predefinedAttributes = null;
      }

      public SessionEvent build(SessionEventMetadata var1) {
         return new SessionEvent(var1, this.timestamp, this.type, this.details, this.customType, this.customAttributes, this.predefinedType, this.predefinedAttributes);
      }

      public SessionEvent.Builder customAttributes(Map var1) {
         this.customAttributes = var1;
         return this;
      }

      public SessionEvent.Builder customType(String var1) {
         this.customType = var1;
         return this;
      }

      public SessionEvent.Builder details(Map var1) {
         this.details = var1;
         return this;
      }

      public SessionEvent.Builder predefinedAttributes(Map var1) {
         this.predefinedAttributes = var1;
         return this;
      }

      public SessionEvent.Builder predefinedType(String var1) {
         this.predefinedType = var1;
         return this;
      }
   }

   static enum Type {
      CRASH,
      CUSTOM,
      INSTALL,
      PAUSE,
      PREDEFINED,
      RESUME,
      START,
      STOP;
   }
}
