package com.crashlytics.android.answers;

import android.annotation.SuppressLint;
import android.content.Context;
import io.fabric.sdk.android.services.d.c;
import io.fabric.sdk.android.services.d.d;

class AnswersPreferenceManager {
   static final String PREFKEY_ANALYTICS_LAUNCHED = "analytics_launched";
   static final String PREF_STORE_NAME = "settings";
   private final c prefStore;

   AnswersPreferenceManager(c var1) {
      this.prefStore = var1;
   }

   public static AnswersPreferenceManager build(Context var0) {
      return new AnswersPreferenceManager(new d(var0, "settings"));
   }

   @SuppressLint({"CommitPrefEdits"})
   public boolean hasAnalyticsLaunched() {
      return this.prefStore.a().getBoolean("analytics_launched", false);
   }

   @SuppressLint({"CommitPrefEdits"})
   public void setAnalyticsLaunched() {
      this.prefStore.a(this.prefStore.b().putBoolean("analytics_launched", true));
   }
}
