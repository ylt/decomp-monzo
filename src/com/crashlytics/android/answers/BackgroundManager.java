package com.crashlytics.android.answers;

import io.fabric.sdk.android.c;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

class BackgroundManager {
   private static final int BACKGROUND_DELAY = 5000;
   final AtomicReference backgroundFutureRef = new AtomicReference();
   private final ScheduledExecutorService executorService;
   private volatile boolean flushOnBackground = true;
   boolean inBackground = true;
   private final List listeners = new ArrayList();

   public BackgroundManager(ScheduledExecutorService var1) {
      this.executorService = var1;
   }

   private void notifyBackground() {
      Iterator var1 = this.listeners.iterator();

      while(var1.hasNext()) {
         ((BackgroundManager.Listener)var1.next()).onBackground();
      }

   }

   public void onActivityPaused() {
      if(this.flushOnBackground && !this.inBackground) {
         this.inBackground = true;

         try {
            AtomicReference var3 = this.backgroundFutureRef;
            ScheduledExecutorService var2 = this.executorService;
            Runnable var1 = new Runnable() {
               public void run() {
                  BackgroundManager.this.backgroundFutureRef.set((Object)null);
                  BackgroundManager.this.notifyBackground();
               }
            };
            var3.compareAndSet((Object)null, var2.schedule(var1, 5000L, TimeUnit.MILLISECONDS));
         } catch (RejectedExecutionException var4) {
            c.h().a("Answers", "Failed to schedule background detector", var4);
         }
      }

   }

   public void onActivityResumed() {
      this.inBackground = false;
      ScheduledFuture var1 = (ScheduledFuture)this.backgroundFutureRef.getAndSet((Object)null);
      if(var1 != null) {
         var1.cancel(false);
      }

   }

   public void registerListener(BackgroundManager.Listener var1) {
      this.listeners.add(var1);
   }

   public void setFlushOnBackground(boolean var1) {
      this.flushOnBackground = var1;
   }

   public interface Listener {
      void onBackground();
   }
}
