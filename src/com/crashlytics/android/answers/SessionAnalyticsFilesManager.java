package com.crashlytics.android.answers;

import android.content.Context;
import io.fabric.sdk.android.services.b.k;
import io.fabric.sdk.android.services.c.b;
import io.fabric.sdk.android.services.c.c;
import java.io.IOException;
import java.util.UUID;

class SessionAnalyticsFilesManager extends b {
   private static final String SESSION_ANALYTICS_TO_SEND_FILE_EXTENSION = ".tap";
   private static final String SESSION_ANALYTICS_TO_SEND_FILE_PREFIX = "sa";
   private io.fabric.sdk.android.services.e.b analyticsSettingsData;

   SessionAnalyticsFilesManager(Context var1, SessionEventTransform var2, k var3, c var4) throws IOException {
      super(var1, var2, var3, var4, 100);
   }

   protected String generateUniqueRollOverFileName() {
      UUID var1 = UUID.randomUUID();
      return "sa" + "_" + var1.toString() + "_" + this.currentTimeProvider.a() + ".tap";
   }

   protected int getMaxByteSizePerFile() {
      int var1;
      if(this.analyticsSettingsData == null) {
         var1 = super.getMaxByteSizePerFile();
      } else {
         var1 = this.analyticsSettingsData.c;
      }

      return var1;
   }

   protected int getMaxFilesToKeep() {
      int var1;
      if(this.analyticsSettingsData == null) {
         var1 = super.getMaxFilesToKeep();
      } else {
         var1 = this.analyticsSettingsData.e;
      }

      return var1;
   }

   void setAnalyticsSettingsData(io.fabric.sdk.android.services.e.b var1) {
      this.analyticsSettingsData = var1;
   }
}
