package com.crashlytics.android.answers;

public class LoginEvent extends PredefinedEvent {
   static final String METHOD_ATTRIBUTE = "method";
   static final String SUCCESS_ATTRIBUTE = "success";
   static final String TYPE = "login";

   String getPredefinedType() {
      return "login";
   }

   public LoginEvent putMethod(String var1) {
      this.predefinedAttributes.put("method", var1);
      return this;
   }

   public LoginEvent putSuccess(boolean var1) {
      this.predefinedAttributes.put("success", Boolean.toString(var1));
      return this;
   }
}
