package com.crashlytics.android.answers;

import android.content.Context;
import io.fabric.sdk.android.c;
import io.fabric.sdk.android.h;
import io.fabric.sdk.android.services.c.d;
import io.fabric.sdk.android.services.e.b;
import java.util.concurrent.ScheduledExecutorService;

class AnswersEventsHandler implements d {
   private final Context context;
   final ScheduledExecutorService executor;
   private final AnswersFilesManagerProvider filesManagerProvider;
   private final h kit;
   private final SessionMetadataCollector metadataCollector;
   private final io.fabric.sdk.android.services.network.d requestFactory;
   SessionAnalyticsManagerStrategy strategy = new DisabledSessionAnalyticsManagerStrategy();

   public AnswersEventsHandler(h var1, Context var2, AnswersFilesManagerProvider var3, SessionMetadataCollector var4, io.fabric.sdk.android.services.network.d var5, ScheduledExecutorService var6) {
      this.kit = var1;
      this.context = var2;
      this.filesManagerProvider = var3;
      this.metadataCollector = var4;
      this.requestFactory = var5;
      this.executor = var6;
   }

   private void executeAsync(Runnable var1) {
      try {
         this.executor.submit(var1);
      } catch (Exception var2) {
         c.h().e("Answers", "Failed to submit events task", var2);
      }

   }

   private void executeSync(Runnable var1) {
      try {
         this.executor.submit(var1).get();
      } catch (Exception var2) {
         c.h().e("Answers", "Failed to run events task", var2);
      }

   }

   public void disable() {
      this.executeAsync(new Runnable() {
         public void run() {
            try {
               SessionAnalyticsManagerStrategy var3 = AnswersEventsHandler.this.strategy;
               AnswersEventsHandler var1 = AnswersEventsHandler.this;
               DisabledSessionAnalyticsManagerStrategy var2 = new DisabledSessionAnalyticsManagerStrategy();
               var1.strategy = var2;
               var3.deleteAllEvents();
            } catch (Exception var4) {
               c.h().e("Answers", "Failed to disable events", var4);
            }

         }
      });
   }

   public void enable() {
      this.executeAsync(new Runnable() {
         public void run() {
            try {
               SessionEventMetadata var3 = AnswersEventsHandler.this.metadataCollector.getMetadata();
               SessionAnalyticsFilesManager var2 = AnswersEventsHandler.this.filesManagerProvider.getAnalyticsFilesManager();
               var2.registerRollOverListener(AnswersEventsHandler.this);
               AnswersEventsHandler var4 = AnswersEventsHandler.this;
               EnabledSessionAnalyticsManagerStrategy var1 = new EnabledSessionAnalyticsManagerStrategy(AnswersEventsHandler.this.kit, AnswersEventsHandler.this.context, AnswersEventsHandler.this.executor, var2, AnswersEventsHandler.this.requestFactory, var3);
               var4.strategy = var1;
            } catch (Exception var5) {
               c.h().e("Answers", "Failed to enable events", var5);
            }

         }
      });
   }

   public void flushEvents() {
      this.executeAsync(new Runnable() {
         public void run() {
            try {
               AnswersEventsHandler.this.strategy.rollFileOver();
            } catch (Exception var2) {
               c.h().e("Answers", "Failed to flush events", var2);
            }

         }
      });
   }

   public void onRollOver(String var1) {
      this.executeAsync(new Runnable() {
         public void run() {
            try {
               AnswersEventsHandler.this.strategy.sendEvents();
            } catch (Exception var2) {
               c.h().e("Answers", "Failed to send events files", var2);
            }

         }
      });
   }

   void processEvent(final SessionEvent.Builder var1, boolean var2, final boolean var3) {
      Runnable var4 = new Runnable() {
         public void run() {
            try {
               AnswersEventsHandler.this.strategy.processEvent(var1);
               if(var3) {
                  AnswersEventsHandler.this.strategy.rollFileOver();
               }
            } catch (Exception var2) {
               c.h().e("Answers", "Failed to process event", var2);
            }

         }
      };
      if(var2) {
         this.executeSync(var4);
      } else {
         this.executeAsync(var4);
      }

   }

   public void processEventAsync(SessionEvent.Builder var1) {
      this.processEvent(var1, false, false);
   }

   public void processEventAsyncAndFlush(SessionEvent.Builder var1) {
      this.processEvent(var1, false, true);
   }

   public void processEventSync(SessionEvent.Builder var1) {
      this.processEvent(var1, true, false);
   }

   public void setAnalyticsSettingsData(final b var1, final String var2) {
      this.executeAsync(new Runnable() {
         public void run() {
            try {
               AnswersEventsHandler.this.strategy.setAnalyticsSettingsData(var1, var2);
            } catch (Exception var2x) {
               c.h().e("Answers", "Failed to set analytics settings data", var2x);
            }

         }
      });
   }
}
