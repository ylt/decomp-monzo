package com.crashlytics.android.answers;

import io.fabric.sdk.android.services.e.b;
import java.io.IOException;

class DisabledSessionAnalyticsManagerStrategy implements SessionAnalyticsManagerStrategy {
   public void cancelTimeBasedFileRollOver() {
   }

   public void deleteAllEvents() {
   }

   public void processEvent(SessionEvent.Builder var1) {
   }

   public boolean rollFileOver() throws IOException {
      return false;
   }

   public void scheduleTimeBasedRollOverIfNeeded() {
   }

   public void sendEvents() {
   }

   public void setAnalyticsSettingsData(b var1, String var2) {
   }
}
