package com.crashlytics.android.answers;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.json.JSONObject;

class AnswersAttributes {
   final Map attributes = new ConcurrentHashMap();
   final AnswersEventValidator validator;

   public AnswersAttributes(AnswersEventValidator var1) {
      this.validator = var1;
   }

   void put(String var1, Number var2) {
      if(!this.validator.isNull(var1, "key") && !this.validator.isNull(var2, "value")) {
         this.putAttribute(this.validator.limitStringLength(var1), var2);
      }

   }

   void put(String var1, String var2) {
      if(!this.validator.isNull(var1, "key") && !this.validator.isNull(var2, "value")) {
         this.putAttribute(this.validator.limitStringLength(var1), this.validator.limitStringLength(var2));
      }

   }

   void putAttribute(String var1, Object var2) {
      if(!this.validator.isFullMap(this.attributes, var1)) {
         this.attributes.put(var1, var2);
      }

   }

   public String toString() {
      return (new JSONObject(this.attributes)).toString();
   }
}
