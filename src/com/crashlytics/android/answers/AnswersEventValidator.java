package com.crashlytics.android.answers;

import io.fabric.sdk.android.c;
import java.util.Locale;
import java.util.Map;

class AnswersEventValidator {
   boolean failFast;
   final int maxNumAttributes;
   final int maxStringLength;

   public AnswersEventValidator(int var1, int var2, boolean var3) {
      this.maxNumAttributes = var1;
      this.maxStringLength = var2;
      this.failFast = var3;
   }

   private void logOrThrowException(RuntimeException var1) {
      if(this.failFast) {
         throw var1;
      } else {
         c.h().e("Answers", "Invalid user input detected", var1);
      }
   }

   public boolean isFullMap(Map var1, String var2) {
      boolean var3 = true;
      if(var1.size() >= this.maxNumAttributes && !var1.containsKey(var2)) {
         this.logOrThrowException(new IllegalArgumentException(String.format(Locale.US, "Limit of %d attributes reached, skipping attribute", new Object[]{Integer.valueOf(this.maxNumAttributes)})));
      } else {
         var3 = false;
      }

      return var3;
   }

   public boolean isNull(Object var1, String var2) {
      boolean var3;
      if(var1 == null) {
         this.logOrThrowException(new NullPointerException(var2 + " must not be null"));
         var3 = true;
      } else {
         var3 = false;
      }

      return var3;
   }

   public String limitStringLength(String var1) {
      String var2 = var1;
      if(var1.length() > this.maxStringLength) {
         this.logOrThrowException(new IllegalArgumentException(String.format(Locale.US, "String is too long, truncating to %d characters", new Object[]{Integer.valueOf(this.maxStringLength)})));
         var2 = var1.substring(0, this.maxStringLength);
      }

      return var2;
   }
}
