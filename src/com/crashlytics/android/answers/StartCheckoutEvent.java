package com.crashlytics.android.answers;

import java.math.BigDecimal;
import java.util.Currency;

public class StartCheckoutEvent extends PredefinedEvent {
   static final String CURRENCY_ATTRIBUTE = "currency";
   static final String ITEM_COUNT_ATTRIBUTE = "itemCount";
   static final BigDecimal MICRO_CONSTANT = BigDecimal.valueOf(1000000L);
   static final String TOTAL_PRICE_ATTRIBUTE = "totalPrice";
   static final String TYPE = "startCheckout";

   String getPredefinedType() {
      return "startCheckout";
   }

   long priceToMicros(BigDecimal var1) {
      return MICRO_CONSTANT.multiply(var1).longValue();
   }

   public StartCheckoutEvent putCurrency(Currency var1) {
      if(!this.validator.isNull(var1, "currency")) {
         this.predefinedAttributes.put("currency", var1.getCurrencyCode());
      }

      return this;
   }

   public StartCheckoutEvent putItemCount(int var1) {
      this.predefinedAttributes.put("itemCount", (Number)Integer.valueOf(var1));
      return this;
   }

   public StartCheckoutEvent putTotalPrice(BigDecimal var1) {
      if(!this.validator.isNull(var1, "totalPrice")) {
         this.predefinedAttributes.put("totalPrice", (Number)Long.valueOf(this.priceToMicros(var1)));
      }

      return this;
   }
}
