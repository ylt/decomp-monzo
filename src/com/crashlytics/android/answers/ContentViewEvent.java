package com.crashlytics.android.answers;

public class ContentViewEvent extends PredefinedEvent {
   static final String CONTENT_ID_ATTRIBUTE = "contentId";
   static final String CONTENT_NAME_ATTRIBUTE = "contentName";
   static final String CONTENT_TYPE_ATTRIBUTE = "contentType";
   static final String TYPE = "contentView";

   String getPredefinedType() {
      return "contentView";
   }

   public ContentViewEvent putContentId(String var1) {
      this.predefinedAttributes.put("contentId", var1);
      return this;
   }

   public ContentViewEvent putContentName(String var1) {
      this.predefinedAttributes.put("contentName", var1);
      return this;
   }

   public ContentViewEvent putContentType(String var1) {
      this.predefinedAttributes.put("contentType", var1);
      return this;
   }
}
