package com.crashlytics.android.answers;

import android.content.Context;
import io.fabric.sdk.android.c;
import io.fabric.sdk.android.h;
import io.fabric.sdk.android.k;
import io.fabric.sdk.android.services.b.g;
import io.fabric.sdk.android.services.b.i;
import io.fabric.sdk.android.services.c.f;
import io.fabric.sdk.android.services.e.b;
import io.fabric.sdk.android.services.network.d;
import java.io.IOException;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

class EnabledSessionAnalyticsManagerStrategy implements SessionAnalyticsManagerStrategy {
   static final int UNDEFINED_ROLLOVER_INTERVAL_SECONDS = -1;
   g apiKey = new g();
   private final Context context;
   boolean customEventsEnabled = true;
   EventFilter eventFilter = new KeepAllEventFilter();
   private final ScheduledExecutorService executorService;
   private final SessionAnalyticsFilesManager filesManager;
   f filesSender;
   private final d httpRequestFactory;
   private final h kit;
   final SessionEventMetadata metadata;
   boolean predefinedEventsEnabled = true;
   private final AtomicReference rolloverFutureRef = new AtomicReference();
   volatile int rolloverIntervalSeconds = -1;

   public EnabledSessionAnalyticsManagerStrategy(h var1, Context var2, ScheduledExecutorService var3, SessionAnalyticsFilesManager var4, d var5, SessionEventMetadata var6) {
      this.kit = var1;
      this.context = var2;
      this.executorService = var3;
      this.filesManager = var4;
      this.httpRequestFactory = var5;
      this.metadata = var6;
   }

   public void cancelTimeBasedFileRollOver() {
      if(this.rolloverFutureRef.get() != null) {
         i.a(this.context, "Cancelling time-based rollover because no events are currently being generated.");
         ((ScheduledFuture)this.rolloverFutureRef.get()).cancel(false);
         this.rolloverFutureRef.set((Object)null);
      }

   }

   public void deleteAllEvents() {
      this.filesManager.deleteAllEventsFiles();
   }

   public void processEvent(SessionEvent.Builder var1) {
      SessionEvent var2 = var1.build(this.metadata);
      if(!this.customEventsEnabled && SessionEvent.Type.CUSTOM.equals(var2.type)) {
         c.h().a("Answers", "Custom events tracking disabled - skipping event: " + var2);
      } else if(!this.predefinedEventsEnabled && SessionEvent.Type.PREDEFINED.equals(var2.type)) {
         c.h().a("Answers", "Predefined events tracking disabled - skipping event: " + var2);
      } else if(this.eventFilter.skipEvent(var2)) {
         c.h().a("Answers", "Skipping filtered event: " + var2);
      } else {
         try {
            this.filesManager.writeEvent(var2);
         } catch (IOException var3) {
            c.h().e("Answers", "Failed to write event: " + var2, var3);
         }

         this.scheduleTimeBasedRollOverIfNeeded();
      }

   }

   public boolean rollFileOver() {
      boolean var1;
      try {
         var1 = this.filesManager.rollFileOver();
      } catch (IOException var3) {
         i.a((Context)this.context, (String)"Failed to roll file over.", (Throwable)var3);
         var1 = false;
      }

      return var1;
   }

   void scheduleTimeBasedFileRollOver(long var1, long var3) {
      boolean var5;
      if(this.rolloverFutureRef.get() == null) {
         var5 = true;
      } else {
         var5 = false;
      }

      if(var5) {
         io.fabric.sdk.android.services.c.i var6 = new io.fabric.sdk.android.services.c.i(this.context, this);
         i.a(this.context, "Scheduling time based file roll over every " + var3 + " seconds");

         try {
            this.rolloverFutureRef.set(this.executorService.scheduleAtFixedRate(var6, var1, var3, TimeUnit.SECONDS));
         } catch (RejectedExecutionException var7) {
            i.a((Context)this.context, (String)"Failed to schedule time based file roll over", (Throwable)var7);
         }
      }

   }

   public void scheduleTimeBasedRollOverIfNeeded() {
      boolean var1;
      if(this.rolloverIntervalSeconds != -1) {
         var1 = true;
      } else {
         var1 = false;
      }

      if(var1) {
         this.scheduleTimeBasedFileRollOver((long)this.rolloverIntervalSeconds, (long)this.rolloverIntervalSeconds);
      }

   }

   public void sendEvents() {
      // $FF: Couldn't be decompiled
   }

   public void setAnalyticsSettingsData(b var1, String var2) {
      this.filesSender = AnswersRetryFilesSender.build(new SessionAnalyticsFilesSender(this.kit, var2, var1.a, this.httpRequestFactory, this.apiKey.a(this.context)));
      this.filesManager.setAnalyticsSettingsData(var1);
      this.customEventsEnabled = var1.f;
      k var4 = c.h();
      StringBuilder var3 = (new StringBuilder()).append("Custom event tracking ");
      if(this.customEventsEnabled) {
         var2 = "enabled";
      } else {
         var2 = "disabled";
      }

      var4.a("Answers", var3.append(var2).toString());
      this.predefinedEventsEnabled = var1.g;
      k var5 = c.h();
      StringBuilder var6 = (new StringBuilder()).append("Predefined event tracking ");
      if(this.predefinedEventsEnabled) {
         var2 = "enabled";
      } else {
         var2 = "disabled";
      }

      var5.a("Answers", var6.append(var2).toString());
      if(var1.i > 1) {
         c.h().a("Answers", "Event sampling enabled");
         this.eventFilter = new SamplingEventFilter(var1.i);
      }

      this.rolloverIntervalSeconds = var1.b;
      this.scheduleTimeBasedFileRollOver(0L, (long)this.rolloverIntervalSeconds);
   }
}
