package com.crashlytics.android.answers;

public class InviteEvent extends PredefinedEvent {
   static final String METHOD_ATTRIBUTE = "method";
   static final String TYPE = "invite";

   String getPredefinedType() {
      return "invite";
   }

   public InviteEvent putMethod(String var1) {
      this.predefinedAttributes.put("method", var1);
      return this;
   }
}
