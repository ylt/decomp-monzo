package com.crashlytics.android.answers;

public class SignUpEvent extends PredefinedEvent {
   static final String METHOD_ATTRIBUTE = "method";
   static final String SUCCESS_ATTRIBUTE = "success";
   static final String TYPE = "signUp";

   String getPredefinedType() {
      return "signUp";
   }

   public SignUpEvent putMethod(String var1) {
      this.predefinedAttributes.put("method", var1);
      return this;
   }

   public SignUpEvent putSuccess(boolean var1) {
      this.predefinedAttributes.put("success", Boolean.toString(var1));
      return this;
   }
}
