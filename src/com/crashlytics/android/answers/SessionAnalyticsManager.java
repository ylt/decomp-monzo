package com.crashlytics.android.answers;

import android.app.Activity;
import android.content.Context;
import android.os.Looper;
import io.fabric.sdk.android.a;
import io.fabric.sdk.android.c;
import io.fabric.sdk.android.h;
import io.fabric.sdk.android.services.b.n;
import io.fabric.sdk.android.services.b.o;
import io.fabric.sdk.android.services.d.b;
import java.util.concurrent.ScheduledExecutorService;

class SessionAnalyticsManager implements BackgroundManager.Listener {
   static final String EXECUTOR_SERVICE = "Answers Events Handler";
   static final String ON_CRASH_ERROR_MSG = "onCrash called from main thread!!!";
   final BackgroundManager backgroundManager;
   final AnswersEventsHandler eventsHandler;
   private final long installedAt;
   final a lifecycleManager;
   final AnswersPreferenceManager preferenceManager;

   SessionAnalyticsManager(AnswersEventsHandler var1, a var2, BackgroundManager var3, AnswersPreferenceManager var4, long var5) {
      this.eventsHandler = var1;
      this.lifecycleManager = var2;
      this.backgroundManager = var3;
      this.preferenceManager = var4;
      this.installedAt = var5;
   }

   public static SessionAnalyticsManager build(h var0, Context var1, o var2, String var3, String var4, long var5) {
      SessionMetadataCollector var8 = new SessionMetadataCollector(var1, var2, var3, var4);
      AnswersFilesManagerProvider var7 = new AnswersFilesManagerProvider(var1, new b(var0));
      io.fabric.sdk.android.services.network.b var12 = new io.fabric.sdk.android.services.network.b(c.h());
      a var11 = new a(var1);
      ScheduledExecutorService var9 = n.b("Answers Events Handler");
      BackgroundManager var10 = new BackgroundManager(var9);
      return new SessionAnalyticsManager(new AnswersEventsHandler(var0, var1, var7, var8, var12, var9), var11, var10, AnswersPreferenceManager.build(var1), var5);
   }

   public void disable() {
      this.lifecycleManager.a();
      this.eventsHandler.disable();
   }

   public void enable() {
      this.eventsHandler.enable();
      this.lifecycleManager.a(new AnswersLifecycleCallbacks(this, this.backgroundManager));
      this.backgroundManager.registerListener(this);
      if(this.isFirstLaunch()) {
         this.onInstall(this.installedAt);
         this.preferenceManager.setAnalyticsLaunched();
      }

   }

   boolean isFirstLaunch() {
      boolean var1;
      if(!this.preferenceManager.hasAnalyticsLaunched()) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public void onBackground() {
      c.h().a("Answers", "Flush events when app is backgrounded");
      this.eventsHandler.flushEvents();
   }

   public void onCrash(String var1, String var2) {
      if(Looper.myLooper() == Looper.getMainLooper()) {
         throw new IllegalStateException("onCrash called from main thread!!!");
      } else {
         c.h().a("Answers", "Logged crash");
         this.eventsHandler.processEventSync(SessionEvent.crashEventBuilder(var1, var2));
      }
   }

   public void onCustom(CustomEvent var1) {
      c.h().a("Answers", "Logged custom event: " + var1);
      this.eventsHandler.processEventAsync(SessionEvent.customEventBuilder(var1));
   }

   public void onError(String var1) {
   }

   public void onInstall(long var1) {
      c.h().a("Answers", "Logged install");
      this.eventsHandler.processEventAsyncAndFlush(SessionEvent.installEventBuilder(var1));
   }

   public void onLifecycle(Activity var1, SessionEvent.Type var2) {
      c.h().a("Answers", "Logged lifecycle event: " + var2.name());
      this.eventsHandler.processEventAsync(SessionEvent.lifecycleEventBuilder(var2, var1));
   }

   public void onPredefined(PredefinedEvent var1) {
      c.h().a("Answers", "Logged predefined event: " + var1);
      this.eventsHandler.processEventAsync(SessionEvent.predefinedEventBuilder(var1));
   }

   public void setAnalyticsSettingsData(io.fabric.sdk.android.services.e.b var1, String var2) {
      this.backgroundManager.setFlushOnBackground(var1.h);
      this.eventsHandler.setAnalyticsSettingsData(var1, var2);
   }
}
