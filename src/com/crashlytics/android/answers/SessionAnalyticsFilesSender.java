package com.crashlytics.android.answers;

import io.fabric.sdk.android.h;
import io.fabric.sdk.android.services.b.a;
import io.fabric.sdk.android.services.b.r;
import io.fabric.sdk.android.services.c.f;
import io.fabric.sdk.android.services.network.HttpRequest;
import io.fabric.sdk.android.services.network.c;
import io.fabric.sdk.android.services.network.d;
import java.io.File;
import java.util.Iterator;
import java.util.List;

class SessionAnalyticsFilesSender extends a implements f {
   static final String FILE_CONTENT_TYPE = "application/vnd.crashlytics.android.events";
   static final String FILE_PARAM_NAME = "session_analytics_file_";
   private final String apiKey;

   public SessionAnalyticsFilesSender(h var1, String var2, String var3, d var4, String var5) {
      super(var1, var2, var3, var4, c.b);
      this.apiKey = var5;
   }

   public boolean send(List var1) {
      boolean var3 = false;
      HttpRequest var4 = this.getHttpRequest().a("X-CRASHLYTICS-API-CLIENT-TYPE", "android").a("X-CRASHLYTICS-API-CLIENT-VERSION", this.kit.getVersion()).a("X-CRASHLYTICS-API-KEY", this.apiKey);
      Iterator var5 = var1.iterator();

      int var2;
      for(var2 = 0; var5.hasNext(); ++var2) {
         File var6 = (File)var5.next();
         var4.a("session_analytics_file_" + var2, var6.getName(), "application/vnd.crashlytics.android.events", var6);
      }

      io.fabric.sdk.android.c.h().a("Answers", "Sending " + var1.size() + " analytics files to " + this.getUrl());
      var2 = var4.b();
      io.fabric.sdk.android.c.h().a("Answers", "Response code for analytics file send is " + var2);
      if(r.a(var2) == 0) {
         var3 = true;
      }

      return var3;
   }
}
