package com.crashlytics.android.answers;

public class LevelEndEvent extends PredefinedEvent {
   static final String LEVEL_NAME_ATTRIBUTE = "levelName";
   static final String SCORE_ATTRIBUTE = "score";
   static final String SUCCESS_ATTRIBUTE = "success";
   static final String TYPE = "levelEnd";

   String getPredefinedType() {
      return "levelEnd";
   }

   public LevelEndEvent putLevelName(String var1) {
      this.predefinedAttributes.put("levelName", var1);
      return this;
   }

   public LevelEndEvent putScore(Number var1) {
      this.predefinedAttributes.put("score", var1);
      return this;
   }

   public LevelEndEvent putSuccess(boolean var1) {
      AnswersAttributes var3 = this.predefinedAttributes;
      String var2;
      if(var1) {
         var2 = "true";
      } else {
         var2 = "false";
      }

      var3.put("success", var2);
      return this;
   }
}
