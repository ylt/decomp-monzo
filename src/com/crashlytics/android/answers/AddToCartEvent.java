package com.crashlytics.android.answers;

import java.math.BigDecimal;
import java.util.Currency;

public class AddToCartEvent extends PredefinedEvent {
   static final String CURRENCY_ATTRIBUTE = "currency";
   static final String ITEM_ID_ATTRIBUTE = "itemId";
   static final String ITEM_NAME_ATTRIBUTE = "itemName";
   static final String ITEM_PRICE_ATTRIBUTE = "itemPrice";
   static final String ITEM_TYPE_ATTRIBUTE = "itemType";
   static final BigDecimal MICRO_CONSTANT = BigDecimal.valueOf(1000000L);
   static final String TYPE = "addToCart";

   String getPredefinedType() {
      return "addToCart";
   }

   long priceToMicros(BigDecimal var1) {
      return MICRO_CONSTANT.multiply(var1).longValue();
   }

   public AddToCartEvent putCurrency(Currency var1) {
      if(!this.validator.isNull(var1, "currency")) {
         this.predefinedAttributes.put("currency", var1.getCurrencyCode());
      }

      return this;
   }

   public AddToCartEvent putItemId(String var1) {
      this.predefinedAttributes.put("itemId", var1);
      return this;
   }

   public AddToCartEvent putItemName(String var1) {
      this.predefinedAttributes.put("itemName", var1);
      return this;
   }

   public AddToCartEvent putItemPrice(BigDecimal var1) {
      if(!this.validator.isNull(var1, "itemPrice")) {
         this.predefinedAttributes.put("itemPrice", (Number)Long.valueOf(this.priceToMicros(var1)));
      }

      return this;
   }

   public AddToCartEvent putItemType(String var1) {
      this.predefinedAttributes.put("itemType", var1);
      return this;
   }
}
