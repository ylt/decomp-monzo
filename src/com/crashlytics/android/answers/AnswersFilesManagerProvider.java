package com.crashlytics.android.answers;

import android.content.Context;
import android.os.Looper;
import io.fabric.sdk.android.services.b.s;
import io.fabric.sdk.android.services.c.g;
import io.fabric.sdk.android.services.d.a;
import java.io.File;
import java.io.IOException;

class AnswersFilesManagerProvider {
   static final String SESSION_ANALYTICS_FILE_NAME = "session_analytics.tap";
   static final String SESSION_ANALYTICS_TO_SEND_DIR = "session_analytics_to_send";
   final Context context;
   final a fileStore;

   public AnswersFilesManagerProvider(Context var1, a var2) {
      this.context = var1;
      this.fileStore = var2;
   }

   public SessionAnalyticsFilesManager getAnalyticsFilesManager() throws IOException {
      if(Looper.myLooper() == Looper.getMainLooper()) {
         throw new IllegalStateException("AnswersFilesManagerProvider cannot be called on the main thread");
      } else {
         SessionEventTransform var2 = new SessionEventTransform();
         s var1 = new s();
         File var3 = this.fileStore.a();
         g var4 = new g(this.context, var3, "session_analytics.tap", "session_analytics_to_send");
         return new SessionAnalyticsFilesManager(this.context, var2, var1, var4);
      }
   }
}
