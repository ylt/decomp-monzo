package com.crashlytics.android.answers;

import io.fabric.sdk.android.c;
import java.util.Map;

public abstract class AnswersEvent {
   public static final int MAX_NUM_ATTRIBUTES = 20;
   public static final int MAX_STRING_LENGTH = 100;
   final AnswersAttributes customAttributes;
   final AnswersEventValidator validator = new AnswersEventValidator(20, 100, c.i());

   public AnswersEvent() {
      this.customAttributes = new AnswersAttributes(this.validator);
   }

   Map getCustomAttributes() {
      return this.customAttributes.attributes;
   }

   public AnswersEvent putCustomAttribute(String var1, Number var2) {
      this.customAttributes.put(var1, var2);
      return this;
   }

   public AnswersEvent putCustomAttribute(String var1, String var2) {
      this.customAttributes.put(var1, var2);
      return this;
   }
}
