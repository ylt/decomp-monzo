package com.crashlytics.android.answers;

import java.util.Map;

public abstract class PredefinedEvent extends AnswersEvent {
   final AnswersAttributes predefinedAttributes;

   public PredefinedEvent() {
      this.predefinedAttributes = new AnswersAttributes(this.validator);
   }

   Map getPredefinedAttributes() {
      return this.predefinedAttributes.attributes;
   }

   abstract String getPredefinedType();

   public String toString() {
      return "{type:\"" + this.getPredefinedType() + '"' + ", predefinedAttributes:" + this.predefinedAttributes + ", customAttributes:" + this.customAttributes + "}";
   }
}
