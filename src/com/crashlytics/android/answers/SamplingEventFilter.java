package com.crashlytics.android.answers;

import java.util.HashSet;
import java.util.Set;

class SamplingEventFilter implements EventFilter {
   static final Set EVENTS_TYPE_TO_SAMPLE = new HashSet() {
      {
         this.add(SessionEvent.Type.START);
         this.add(SessionEvent.Type.RESUME);
         this.add(SessionEvent.Type.PAUSE);
         this.add(SessionEvent.Type.STOP);
      }
   };
   final int samplingRate;

   public SamplingEventFilter(int var1) {
      this.samplingRate = var1;
   }

   public boolean skipEvent(SessionEvent var1) {
      boolean var4 = true;
      boolean var2;
      if(EVENTS_TYPE_TO_SAMPLE.contains(var1.type) && var1.sessionEventMetadata.betaDeviceToken == null) {
         var2 = true;
      } else {
         var2 = false;
      }

      boolean var3;
      if(Math.abs(var1.sessionEventMetadata.installationId.hashCode() % this.samplingRate) != 0) {
         var3 = true;
      } else {
         var3 = false;
      }

      if(!var2 || !var3) {
         var4 = false;
      }

      return var4;
   }
}
