package com.crashlytics.android.answers;

import android.annotation.SuppressLint;
import io.fabric.sdk.android.c;
import io.fabric.sdk.android.h;
import io.fabric.sdk.android.services.b.i;
import io.fabric.sdk.android.services.b.j;

public class Answers extends h {
   static final String CRASHLYTICS_API_ENDPOINT = "com.crashlytics.ApiEndpoint";
   public static final String TAG = "Answers";
   SessionAnalyticsManager analyticsManager;

   public static Answers getInstance() {
      return (Answers)c.a(Answers.class);
   }

   protected Boolean doInBackground() {
      // $FF: Couldn't be decompiled
   }

   public String getIdentifier() {
      return "com.crashlytics.sdk.android:answers";
   }

   String getOverridenSpiEndpoint() {
      return i.b(this.getContext(), "com.crashlytics.ApiEndpoint");
   }

   public String getVersion() {
      return "1.3.13.dev";
   }

   public void logAddToCart(AddToCartEvent var1) {
      if(var1 == null) {
         throw new NullPointerException("event must not be null");
      } else {
         if(this.analyticsManager != null) {
            this.analyticsManager.onPredefined(var1);
         }

      }
   }

   public void logContentView(ContentViewEvent var1) {
      if(var1 == null) {
         throw new NullPointerException("event must not be null");
      } else {
         if(this.analyticsManager != null) {
            this.analyticsManager.onPredefined(var1);
         }

      }
   }

   public void logCustom(CustomEvent var1) {
      if(var1 == null) {
         throw new NullPointerException("event must not be null");
      } else {
         if(this.analyticsManager != null) {
            this.analyticsManager.onCustom(var1);
         }

      }
   }

   public void logInvite(InviteEvent var1) {
      if(var1 == null) {
         throw new NullPointerException("event must not be null");
      } else {
         if(this.analyticsManager != null) {
            this.analyticsManager.onPredefined(var1);
         }

      }
   }

   public void logLevelEnd(LevelEndEvent var1) {
      if(var1 == null) {
         throw new NullPointerException("event must not be null");
      } else {
         if(this.analyticsManager != null) {
            this.analyticsManager.onPredefined(var1);
         }

      }
   }

   public void logLevelStart(LevelStartEvent var1) {
      if(var1 == null) {
         throw new NullPointerException("event must not be null");
      } else {
         if(this.analyticsManager != null) {
            this.analyticsManager.onPredefined(var1);
         }

      }
   }

   public void logLogin(LoginEvent var1) {
      if(var1 == null) {
         throw new NullPointerException("event must not be null");
      } else {
         if(this.analyticsManager != null) {
            this.analyticsManager.onPredefined(var1);
         }

      }
   }

   public void logPurchase(PurchaseEvent var1) {
      if(var1 == null) {
         throw new NullPointerException("event must not be null");
      } else {
         if(this.analyticsManager != null) {
            this.analyticsManager.onPredefined(var1);
         }

      }
   }

   public void logRating(RatingEvent var1) {
      if(var1 == null) {
         throw new NullPointerException("event must not be null");
      } else {
         if(this.analyticsManager != null) {
            this.analyticsManager.onPredefined(var1);
         }

      }
   }

   public void logSearch(SearchEvent var1) {
      if(var1 == null) {
         throw new NullPointerException("event must not be null");
      } else {
         if(this.analyticsManager != null) {
            this.analyticsManager.onPredefined(var1);
         }

      }
   }

   public void logShare(ShareEvent var1) {
      if(var1 == null) {
         throw new NullPointerException("event must not be null");
      } else {
         if(this.analyticsManager != null) {
            this.analyticsManager.onPredefined(var1);
         }

      }
   }

   public void logSignUp(SignUpEvent var1) {
      if(var1 == null) {
         throw new NullPointerException("event must not be null");
      } else {
         if(this.analyticsManager != null) {
            this.analyticsManager.onPredefined(var1);
         }

      }
   }

   public void logStartCheckout(StartCheckoutEvent var1) {
      if(var1 == null) {
         throw new NullPointerException("event must not be null");
      } else {
         if(this.analyticsManager != null) {
            this.analyticsManager.onPredefined(var1);
         }

      }
   }

   public void onException(j.a var1) {
      if(this.analyticsManager != null) {
         this.analyticsManager.onCrash(var1.a(), var1.b());
      }

   }

   public void onException(j.b var1) {
      if(this.analyticsManager != null) {
         this.analyticsManager.onError(var1.a());
      }

   }

   @SuppressLint({"NewApi"})
   protected boolean onPreExecute() {
      // $FF: Couldn't be decompiled
   }
}
