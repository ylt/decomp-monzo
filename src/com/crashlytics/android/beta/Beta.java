package com.crashlytics.android.beta;

import android.annotation.TargetApi;
import android.app.Application;
import android.content.Context;
import android.os.Build.VERSION;
import android.text.TextUtils;
import io.fabric.sdk.android.c;
import io.fabric.sdk.android.h;
import io.fabric.sdk.android.k;
import io.fabric.sdk.android.services.a.b;
import io.fabric.sdk.android.services.b.i;
import io.fabric.sdk.android.services.b.m;
import io.fabric.sdk.android.services.b.o;
import io.fabric.sdk.android.services.b.s;
import io.fabric.sdk.android.services.d.d;
import io.fabric.sdk.android.services.e.f;
import io.fabric.sdk.android.services.e.q;
import io.fabric.sdk.android.services.e.t;
import java.util.HashMap;
import java.util.Map;

public class Beta extends h implements m {
   private static final String CRASHLYTICS_API_ENDPOINT = "com.crashlytics.ApiEndpoint";
   private static final String CRASHLYTICS_BUILD_PROPERTIES = "crashlytics-build.properties";
   static final String NO_DEVICE_TOKEN = "";
   public static final String TAG = "Beta";
   private final b deviceTokenCache = new b();
   private final DeviceTokenLoader deviceTokenLoader = new DeviceTokenLoader();
   private UpdatesController updatesController;

   private String getBetaDeviceToken(Context var1, String var2) {
      boolean var3;
      String var6;
      label19: {
         try {
            var6 = (String)this.deviceTokenCache.a(var1, this.deviceTokenLoader);
            var3 = "".equals(var6);
         } catch (Exception var5) {
            c.h().e("Beta", "Failed to load the Beta device token", var5);
            var6 = null;
            break label19;
         }

         if(var3) {
            var6 = null;
         }
      }

      k var7 = c.h();
      StringBuilder var4 = (new StringBuilder()).append("Beta device token present: ");
      if(!TextUtils.isEmpty(var6)) {
         var3 = true;
      } else {
         var3 = false;
      }

      var7.a("Beta", var4.append(var3).toString());
      return var6;
   }

   private f getBetaSettingsData() {
      t var1 = q.a().b();
      f var2;
      if(var1 != null) {
         var2 = var1.f;
      } else {
         var2 = null;
      }

      return var2;
   }

   public static Beta getInstance() {
      return (Beta)c.a(Beta.class);
   }

   private BuildProperties loadBuildProperties(Context param1) {
      // $FF: Couldn't be decompiled
   }

   boolean canCheckForUpdates(f var1, BuildProperties var2) {
      boolean var3;
      if(var1 != null && !TextUtils.isEmpty(var1.a) && var2 != null) {
         var3 = true;
      } else {
         var3 = false;
      }

      return var3;
   }

   @TargetApi(14)
   UpdatesController createUpdatesController(int var1, Application var2) {
      Object var3;
      if(var1 >= 14) {
         var3 = new ActivityLifecycleCheckForUpdatesController(this.getFabric().e(), this.getFabric().f());
      } else {
         var3 = new ImmediateCheckForUpdatesController();
      }

      return (UpdatesController)var3;
   }

   protected Boolean doInBackground() {
      c.h().a("Beta", "Beta kit initializing...");
      Context var3 = this.getContext();
      o var4 = this.getIdManager();
      Boolean var1;
      if(TextUtils.isEmpty(this.getBetaDeviceToken(var3, var4.j()))) {
         c.h().a("Beta", "A Beta device token was not found for this app");
         var1 = Boolean.valueOf(false);
      } else {
         c.h().a("Beta", "Beta device token is present, checking for app updates.");
         f var5 = this.getBetaSettingsData();
         BuildProperties var2 = this.loadBuildProperties(var3);
         if(this.canCheckForUpdates(var5, var2)) {
            this.updatesController.initialize(var3, this, var4, var5, var2, new d(this), new s(), new io.fabric.sdk.android.services.network.b(c.h()));
         }

         var1 = Boolean.valueOf(true);
      }

      return var1;
   }

   public Map getDeviceIdentifiers() {
      String var1 = this.getIdManager().j();
      String var2 = this.getBetaDeviceToken(this.getContext(), var1);
      HashMap var3 = new HashMap();
      if(!TextUtils.isEmpty(var2)) {
         var3.put(o.a.c, var2);
      }

      return var3;
   }

   public String getIdentifier() {
      return "com.crashlytics.sdk.android:beta";
   }

   String getOverridenSpiEndpoint() {
      return i.b(this.getContext(), "com.crashlytics.ApiEndpoint");
   }

   public String getVersion() {
      return "1.2.5.dev";
   }

   @TargetApi(14)
   protected boolean onPreExecute() {
      Application var1 = (Application)this.getContext().getApplicationContext();
      this.updatesController = this.createUpdatesController(VERSION.SDK_INT, var1);
      return true;
   }
}
