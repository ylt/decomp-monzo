package com.crashlytics.android.beta;

import io.fabric.sdk.android.h;
import io.fabric.sdk.android.services.b.a;
import io.fabric.sdk.android.services.network.HttpRequest;
import io.fabric.sdk.android.services.network.c;
import io.fabric.sdk.android.services.network.d;
import java.util.HashMap;
import java.util.Map;

class CheckForUpdatesRequest extends a {
   static final String BETA_SOURCE = "3";
   static final String BUILD_VERSION = "build_version";
   static final String DISPLAY_VERSION = "display_version";
   static final String HEADER_BETA_TOKEN = "X-CRASHLYTICS-BETA-TOKEN";
   static final String INSTANCE = "instance";
   static final String SDK_ANDROID_DIR_TOKEN_TYPE = "3";
   static final String SOURCE = "source";
   private final CheckForUpdatesResponseTransform responseTransform;

   public CheckForUpdatesRequest(h var1, String var2, String var3, d var4, CheckForUpdatesResponseTransform var5) {
      super(var1, var2, var3, var4, c.a);
      this.responseTransform = var5;
   }

   private HttpRequest applyHeadersTo(HttpRequest var1, String var2, String var3) {
      return var1.a("Accept", "application/json").a("User-Agent", "Crashlytics Android SDK/" + this.kit.getVersion()).a("X-CRASHLYTICS-DEVELOPER-TOKEN", "470fa2b4ae81cd56ecbcda9735803434cec591fa").a("X-CRASHLYTICS-API-CLIENT-TYPE", "android").a("X-CRASHLYTICS-API-CLIENT-VERSION", this.kit.getVersion()).a("X-CRASHLYTICS-API-KEY", var2).a("X-CRASHLYTICS-BETA-TOKEN", createBetaTokenHeaderValueFor(var3));
   }

   static String createBetaTokenHeaderValueFor(String var0) {
      return "3:" + var0;
   }

   private Map getQueryParamsFor(BuildProperties var1) {
      HashMap var2 = new HashMap();
      var2.put("build_version", var1.versionCode);
      var2.put("display_version", var1.versionName);
      var2.put("instance", var1.buildId);
      var2.put("source", "3");
      return var2;
   }

   public CheckForUpdatesResponse invoke(String param1, String param2, BuildProperties param3) {
      // $FF: Couldn't be decompiled
   }
}
