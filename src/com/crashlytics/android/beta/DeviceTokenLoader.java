package com.crashlytics.android.beta;

import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import io.fabric.sdk.android.services.a.d;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class DeviceTokenLoader implements d {
   private static final String BETA_APP_PACKAGE_NAME = "io.crash.air";
   private static final String DIRFACTOR_DEVICE_TOKEN_PREFIX = "assets/com.crashlytics.android.beta/dirfactor-device-token=";

   String determineDeviceToken(ZipInputStream var1) throws IOException {
      ZipEntry var2 = var1.getNextEntry();
      String var3;
      if(var2 != null) {
         var3 = var2.getName();
         if(var3.startsWith("assets/com.crashlytics.android.beta/dirfactor-device-token=")) {
            var3 = var3.substring("assets/com.crashlytics.android.beta/dirfactor-device-token=".length(), var3.length() - 1);
            return var3;
         }
      }

      var3 = "";
      return var3;
   }

   ZipInputStream getZipInputStreamOfApkFrom(Context var1, String var2) throws NameNotFoundException, FileNotFoundException {
      return new ZipInputStream(new FileInputStream(var1.getPackageManager().getApplicationInfo(var2, 0).sourceDir));
   }

   public String load(Context param1) throws Exception {
      // $FF: Couldn't be decompiled
   }
}
