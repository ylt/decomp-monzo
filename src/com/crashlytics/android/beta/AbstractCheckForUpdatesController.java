package com.crashlytics.android.beta;

import android.annotation.SuppressLint;
import android.content.Context;
import io.fabric.sdk.android.services.b.g;
import io.fabric.sdk.android.services.b.k;
import io.fabric.sdk.android.services.b.o;
import io.fabric.sdk.android.services.d.c;
import io.fabric.sdk.android.services.e.f;
import io.fabric.sdk.android.services.network.d;
import java.util.concurrent.atomic.AtomicBoolean;

abstract class AbstractCheckForUpdatesController implements UpdatesController {
   static final long LAST_UPDATE_CHECK_DEFAULT = 0L;
   static final String LAST_UPDATE_CHECK_KEY = "last_update_check";
   private static final long MILLIS_PER_SECOND = 1000L;
   private Beta beta;
   private f betaSettings;
   private BuildProperties buildProps;
   private Context context;
   private k currentTimeProvider;
   private final AtomicBoolean externallyReady;
   private d httpRequestFactory;
   private o idManager;
   private final AtomicBoolean initialized;
   private long lastCheckTimeMillis;
   private c preferenceStore;

   public AbstractCheckForUpdatesController() {
      this(false);
   }

   public AbstractCheckForUpdatesController(boolean var1) {
      this.initialized = new AtomicBoolean();
      this.lastCheckTimeMillis = 0L;
      this.externallyReady = new AtomicBoolean(var1);
   }

   private void performUpdateCheck() {
      io.fabric.sdk.android.c.h().a("Beta", "Performing update check");
      String var2 = (new g()).a(this.context);
      String var1 = (String)this.idManager.i().get(o.a.c);
      (new CheckForUpdatesRequest(this.beta, this.beta.getOverridenSpiEndpoint(), this.betaSettings.a, this.httpRequestFactory, new CheckForUpdatesResponseTransform())).invoke(var2, var1, this.buildProps);
   }

   @SuppressLint({"CommitPrefEdits"})
   protected void checkForUpdates() {
      // $FF: Couldn't be decompiled
   }

   long getLastCheckTimeMillis() {
      return this.lastCheckTimeMillis;
   }

   public void initialize(Context var1, Beta var2, o var3, f var4, BuildProperties var5, c var6, k var7, d var8) {
      this.context = var1;
      this.beta = var2;
      this.idManager = var3;
      this.betaSettings = var4;
      this.buildProps = var5;
      this.preferenceStore = var6;
      this.currentTimeProvider = var7;
      this.httpRequestFactory = var8;
      if(this.signalInitialized()) {
         this.checkForUpdates();
      }

   }

   void setLastCheckTimeMillis(long var1) {
      this.lastCheckTimeMillis = var1;
   }

   protected boolean signalExternallyReady() {
      this.externallyReady.set(true);
      return this.initialized.get();
   }

   boolean signalInitialized() {
      this.initialized.set(true);
      return this.externallyReady.get();
   }
}
