package com.crashlytics.android.beta;

import android.annotation.TargetApi;
import android.app.Activity;
import io.fabric.sdk.android.a;
import java.util.concurrent.ExecutorService;

@TargetApi(14)
class ActivityLifecycleCheckForUpdatesController extends AbstractCheckForUpdatesController {
   private final a.b callbacks = new a.b() {
      public void onActivityStarted(Activity var1) {
         if(ActivityLifecycleCheckForUpdatesController.this.signalExternallyReady()) {
            ActivityLifecycleCheckForUpdatesController.this.executorService.submit(new Runnable() {
               public void run() {
                  ActivityLifecycleCheckForUpdatesController.this.checkForUpdates();
               }
            });
         }

      }
   };
   private final ExecutorService executorService;

   public ActivityLifecycleCheckForUpdatesController(a var1, ExecutorService var2) {
      this.executorService = var2;
      var1.a(this.callbacks);
   }

   public boolean isActivityLifecycleTriggered() {
      return true;
   }
}
