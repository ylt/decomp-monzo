package com.crashlytics.android.beta;

import android.content.Context;
import io.fabric.sdk.android.services.b.k;
import io.fabric.sdk.android.services.b.o;
import io.fabric.sdk.android.services.d.c;
import io.fabric.sdk.android.services.e.f;
import io.fabric.sdk.android.services.network.d;

interface UpdatesController {
   void initialize(Context var1, Beta var2, o var3, f var4, BuildProperties var5, c var6, k var7, d var8);

   boolean isActivityLifecycleTriggered();
}
