package com.coremedia.iso;

import java.io.UnsupportedEncodingException;

public final class j {
   public static String a(byte[] var0) {
      String var2;
      if(var0 != null) {
         try {
            var2 = new String(var0, "UTF-8");
         } catch (UnsupportedEncodingException var1) {
            throw new Error(var1);
         }
      } else {
         var2 = null;
      }

      return var2;
   }

   public static byte[] a(String var0) {
      byte[] var2;
      if(var0 != null) {
         try {
            var2 = var0.getBytes("UTF-8");
         } catch (UnsupportedEncodingException var1) {
            throw new Error(var1);
         }
      } else {
         var2 = null;
      }

      return var2;
   }

   public static int b(String var0) {
      int var1;
      if(var0 != null) {
         try {
            var1 = var0.getBytes("UTF-8").length;
         } catch (UnsupportedEncodingException var2) {
            throw new RuntimeException();
         }
      } else {
         var1 = 0;
      }

      return var1;
   }
}
