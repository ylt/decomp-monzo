package com.coremedia.iso.boxes;

import com.coremedia.iso.e;
import com.coremedia.iso.j;
import com.googlecode.mp4parser.AbstractFullBox;
import java.nio.ByteBuffer;

public class DataEntryUrnBox extends AbstractFullBox {
   public static final String TYPE = "urn ";
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_0;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_1;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_2;
   private String location;
   private String name;

   static {
      ajc$preClinit();
   }

   public DataEntryUrnBox() {
      super("urn ");
   }

   private static void ajc$preClinit() {
      org.mp4parser.aspectj.a.b.b var0 = new org.mp4parser.aspectj.a.b.b("DataEntryUrnBox.java", DataEntryUrnBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getName", "com.coremedia.iso.boxes.DataEntryUrnBox", "", "", "", "java.lang.String"), 40);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "getLocation", "com.coremedia.iso.boxes.DataEntryUrnBox", "", "", "", "java.lang.String"), 44);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "toString", "com.coremedia.iso.boxes.DataEntryUrnBox", "", "", "", "java.lang.String"), 67);
   }

   public void _parseDetails(ByteBuffer var1) {
      this.name = e.e(var1);
      this.location = e.e(var1);
   }

   protected void getContent(ByteBuffer var1) {
      var1.put(j.a(this.name));
      var1.put(0);
      var1.put(j.a(this.location));
      var1.put(0);
   }

   protected long getContentSize() {
      return (long)(j.b(this.name) + 1 + j.b(this.location) + 1);
   }

   public String getLocation() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_1, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.location;
   }

   public String getName() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.name;
   }

   public String toString() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_2, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return "DataEntryUrlBox[name=" + this.getName() + ";location=" + this.getLocation() + "]";
   }
}
