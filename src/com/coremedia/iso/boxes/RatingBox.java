package com.coremedia.iso.boxes;

import com.coremedia.iso.d;
import com.coremedia.iso.e;
import com.coremedia.iso.g;
import com.coremedia.iso.j;
import com.googlecode.mp4parser.AbstractFullBox;
import java.nio.ByteBuffer;

public class RatingBox extends AbstractFullBox {
   public static final String TYPE = "rtng";
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_0;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_1;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_2;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_3;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_4;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_5;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_6;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_7;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_8;
   private String language;
   private String ratingCriteria;
   private String ratingEntity;
   private String ratingInfo;

   static {
      ajc$preClinit();
   }

   public RatingBox() {
      super("rtng");
   }

   private static void ajc$preClinit() {
      org.mp4parser.aspectj.a.b.b var0 = new org.mp4parser.aspectj.a.b.b("RatingBox.java", RatingBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "setRatingEntity", "com.coremedia.iso.boxes.RatingBox", "java.lang.String", "ratingEntity", "", "void"), 46);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "setRatingCriteria", "com.coremedia.iso.boxes.RatingBox", "java.lang.String", "ratingCriteria", "", "void"), 50);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "setLanguage", "com.coremedia.iso.boxes.RatingBox", "java.lang.String", "language", "", "void"), 54);
      ajc$tjp_3 = var0.a("method-execution", var0.a("1", "setRatingInfo", "com.coremedia.iso.boxes.RatingBox", "java.lang.String", "ratingInfo", "", "void"), 58);
      ajc$tjp_4 = var0.a("method-execution", var0.a("1", "getLanguage", "com.coremedia.iso.boxes.RatingBox", "", "", "", "java.lang.String"), 62);
      ajc$tjp_5 = var0.a("method-execution", var0.a("1", "getRatingEntity", "com.coremedia.iso.boxes.RatingBox", "", "", "", "java.lang.String"), 73);
      ajc$tjp_6 = var0.a("method-execution", var0.a("1", "getRatingCriteria", "com.coremedia.iso.boxes.RatingBox", "", "", "", "java.lang.String"), 83);
      ajc$tjp_7 = var0.a("method-execution", var0.a("1", "getRatingInfo", "com.coremedia.iso.boxes.RatingBox", "", "", "", "java.lang.String"), 87);
      ajc$tjp_8 = var0.a("method-execution", var0.a("1", "toString", "com.coremedia.iso.boxes.RatingBox", "", "", "", "java.lang.String"), 115);
   }

   public void _parseDetails(ByteBuffer var1) {
      this.parseVersionAndFlags(var1);
      this.ratingEntity = e.k(var1);
      this.ratingCriteria = e.k(var1);
      this.language = e.j(var1);
      this.ratingInfo = e.e(var1);
   }

   protected void getContent(ByteBuffer var1) {
      this.writeVersionAndFlags(var1);
      var1.put(d.a(this.ratingEntity));
      var1.put(d.a(this.ratingCriteria));
      g.a(var1, this.language);
      var1.put(j.a(this.ratingInfo));
      var1.put(0);
   }

   protected long getContentSize() {
      return (long)(j.b(this.ratingInfo) + 15);
   }

   public String getLanguage() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_4, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.language;
   }

   public String getRatingCriteria() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_6, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.ratingCriteria;
   }

   public String getRatingEntity() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_5, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.ratingEntity;
   }

   public String getRatingInfo() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_7, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.ratingInfo;
   }

   public void setLanguage(String var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_2, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.language = var1;
   }

   public void setRatingCriteria(String var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_1, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.ratingCriteria = var1;
   }

   public void setRatingEntity(String var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_0, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.ratingEntity = var1;
   }

   public void setRatingInfo(String var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_3, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.ratingInfo = var1;
   }

   public String toString() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_8, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      StringBuilder var2 = new StringBuilder();
      var2.append("RatingBox[language=").append(this.getLanguage());
      var2.append("ratingEntity=").append(this.getRatingEntity());
      var2.append(";ratingCriteria=").append(this.getRatingCriteria());
      var2.append(";language=").append(this.getLanguage());
      var2.append(";ratingInfo=").append(this.getRatingInfo());
      var2.append("]");
      return var2.toString();
   }
}
