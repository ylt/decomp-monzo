package com.coremedia.iso.boxes.mdat;

import com.googlecode.mp4parser.b;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;

public final class MediaDataBox implements com.coremedia.iso.boxes.a {
   public static final String TYPE = "mdat";
   private b dataSource;
   private long offset;
   com.coremedia.iso.boxes.b parent;
   private long size;

   private static void transfer(b var0, long var1, long var3, WritableByteChannel var5) throws IOException {
      for(long var6 = 0L; var6 < var3; var6 += var0.a(var1 + var6, Math.min(67076096L, var3 - var6), var5)) {
         ;
      }

   }

   public void getBox(WritableByteChannel var1) throws IOException {
      transfer(this.dataSource, this.offset, this.size, var1);
   }

   public long getOffset() {
      return this.offset;
   }

   public com.coremedia.iso.boxes.b getParent() {
      return this.parent;
   }

   public long getSize() {
      return this.size;
   }

   public String getType() {
      return "mdat";
   }

   public void parse(b var1, ByteBuffer var2, long var3, com.coremedia.iso.b var5) throws IOException {
      this.offset = var1.b() - (long)var2.remaining();
      this.dataSource = var1;
      this.size = (long)var2.remaining() + var3;
      var1.a(var1.b() + var3);
   }

   public void setParent(com.coremedia.iso.boxes.b var1) {
      this.parent = var1;
   }

   public String toString() {
      return "MediaDataBox{size=" + this.size + '}';
   }
}
