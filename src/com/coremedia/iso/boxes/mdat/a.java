package com.coremedia.iso.boxes.mdat;

import com.coremedia.iso.d;
import com.coremedia.iso.boxes.TrackBox;
import com.coremedia.iso.boxes.b;
import com.coremedia.iso.boxes.fragment.MovieExtendsBox;
import com.googlecode.mp4parser.authoring.e;
import java.util.AbstractList;
import java.util.List;

public class a extends AbstractList {
   List a;

   public a(TrackBox var1, d... var2) {
      b var3 = ((com.coremedia.iso.boxes.a)var1.getParent()).getParent();
      if(var1.getParent().getBoxes(MovieExtendsBox.class).isEmpty()) {
         if(var2.length > 0) {
            throw new RuntimeException("The TrackBox comes from a standard MP4 file. Only use the additionalFragments param if you are dealing with ( fragmented MP4 files AND additional fragments in standalone files )");
         }

         this.a = new com.googlecode.mp4parser.authoring.a.a(var1.getTrackHeaderBox().getTrackId(), var3);
      } else {
         this.a = new com.googlecode.mp4parser.authoring.a.b(var1.getTrackHeaderBox().getTrackId(), var3, var2);
      }

   }

   public e a(int var1) {
      return (e)this.a.get(var1);
   }

   // $FF: synthetic method
   public Object get(int var1) {
      return this.a(var1);
   }

   public int size() {
      return this.a.size();
   }
}
