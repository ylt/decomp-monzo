package com.coremedia.iso.boxes;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;

public interface a {
   void getBox(WritableByteChannel var1) throws IOException;

   b getParent();

   long getSize();

   String getType();

   void parse(com.googlecode.mp4parser.b var1, ByteBuffer var2, long var3, com.coremedia.iso.b var5) throws IOException;

   void setParent(b var1);
}
