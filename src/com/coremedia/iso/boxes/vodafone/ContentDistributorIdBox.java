package com.coremedia.iso.boxes.vodafone;

import com.coremedia.iso.e;
import com.coremedia.iso.g;
import com.coremedia.iso.j;
import com.googlecode.mp4parser.AbstractFullBox;
import java.nio.ByteBuffer;
import org.mp4parser.aspectj.a.b.b;
import org.mp4parser.aspectj.lang.a;

public class ContentDistributorIdBox extends AbstractFullBox {
   public static final String TYPE = "cdis";
   private static final a.a ajc$tjp_0;
   private static final a.a ajc$tjp_1;
   private static final a.a ajc$tjp_2;
   private String contentDistributorId;
   private String language;

   static {
      ajc$preClinit();
   }

   public ContentDistributorIdBox() {
      super("cdis");
   }

   private static void ajc$preClinit() {
      b var0 = new b("ContentDistributorIdBox.java", ContentDistributorIdBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getLanguage", "com.coremedia.iso.boxes.vodafone.ContentDistributorIdBox", "", "", "", "java.lang.String"), 40);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "getContentDistributorId", "com.coremedia.iso.boxes.vodafone.ContentDistributorIdBox", "", "", "", "java.lang.String"), 44);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "toString", "com.coremedia.iso.boxes.vodafone.ContentDistributorIdBox", "", "", "", "java.lang.String"), 68);
   }

   public void _parseDetails(ByteBuffer var1) {
      this.parseVersionAndFlags(var1);
      this.language = e.j(var1);
      this.contentDistributorId = e.e(var1);
   }

   protected void getContent(ByteBuffer var1) {
      this.writeVersionAndFlags(var1);
      g.a(var1, this.language);
      var1.put(j.a(this.contentDistributorId));
      var1.put(0);
   }

   public String getContentDistributorId() {
      a var1 = b.a(ajc$tjp_1, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.contentDistributorId;
   }

   protected long getContentSize() {
      return (long)(j.b(this.contentDistributorId) + 2 + 5);
   }

   public String getLanguage() {
      a var1 = b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.language;
   }

   public String toString() {
      a var1 = b.a(ajc$tjp_2, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return "ContentDistributorIdBox[language=" + this.getLanguage() + ";contentDistributorId=" + this.getContentDistributorId() + "]";
   }
}
