package com.coremedia.iso.boxes.vodafone;

import com.coremedia.iso.e;
import com.coremedia.iso.g;
import com.coremedia.iso.j;
import com.googlecode.mp4parser.AbstractFullBox;
import java.nio.ByteBuffer;
import org.mp4parser.aspectj.a.b.b;
import org.mp4parser.aspectj.lang.a;

public class AlbumArtistBox extends AbstractFullBox {
   public static final String TYPE = "albr";
   private static final a.a ajc$tjp_0;
   private static final a.a ajc$tjp_1;
   private static final a.a ajc$tjp_2;
   private static final a.a ajc$tjp_3;
   private static final a.a ajc$tjp_4;
   private String albumArtist;
   private String language;

   static {
      ajc$preClinit();
   }

   public AlbumArtistBox() {
      super("albr");
   }

   private static void ajc$preClinit() {
      b var0 = new b("AlbumArtistBox.java", AlbumArtistBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getLanguage", "com.coremedia.iso.boxes.vodafone.AlbumArtistBox", "", "", "", "java.lang.String"), 42);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "getAlbumArtist", "com.coremedia.iso.boxes.vodafone.AlbumArtistBox", "", "", "", "java.lang.String"), 46);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "setLanguage", "com.coremedia.iso.boxes.vodafone.AlbumArtistBox", "java.lang.String", "language", "", "void"), 50);
      ajc$tjp_3 = var0.a("method-execution", var0.a("1", "setAlbumArtist", "com.coremedia.iso.boxes.vodafone.AlbumArtistBox", "java.lang.String", "albumArtist", "", "void"), 54);
      ajc$tjp_4 = var0.a("method-execution", var0.a("1", "toString", "com.coremedia.iso.boxes.vodafone.AlbumArtistBox", "", "", "", "java.lang.String"), 76);
   }

   public void _parseDetails(ByteBuffer var1) {
      this.parseVersionAndFlags(var1);
      this.language = e.j(var1);
      this.albumArtist = e.e(var1);
   }

   public String getAlbumArtist() {
      a var1 = b.a(ajc$tjp_1, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.albumArtist;
   }

   protected void getContent(ByteBuffer var1) {
      this.writeVersionAndFlags(var1);
      g.a(var1, this.language);
      var1.put(j.a(this.albumArtist));
      var1.put(0);
   }

   protected long getContentSize() {
      return (long)(j.b(this.albumArtist) + 6 + 1);
   }

   public String getLanguage() {
      a var1 = b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.language;
   }

   public void setAlbumArtist(String var1) {
      a var2 = b.a(ajc$tjp_3, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.albumArtist = var1;
   }

   public void setLanguage(String var1) {
      a var2 = b.a(ajc$tjp_2, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.language = var1;
   }

   public String toString() {
      a var1 = b.a(ajc$tjp_4, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return "AlbumArtistBox[language=" + this.getLanguage() + ";albumArtist=" + this.getAlbumArtist() + "]";
   }
}
