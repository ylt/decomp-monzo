package com.coremedia.iso.boxes.vodafone;

import com.coremedia.iso.e;
import com.coremedia.iso.j;
import com.googlecode.mp4parser.AbstractFullBox;
import java.nio.ByteBuffer;
import org.mp4parser.aspectj.a.b.b;
import org.mp4parser.aspectj.lang.a;

public class LyricsUriBox extends AbstractFullBox {
   public static final String TYPE = "lrcu";
   private static final a.a ajc$tjp_0;
   private static final a.a ajc$tjp_1;
   private static final a.a ajc$tjp_2;
   private String lyricsUri;

   static {
      ajc$preClinit();
   }

   public LyricsUriBox() {
      super("lrcu");
   }

   private static void ajc$preClinit() {
      b var0 = new b("LyricsUriBox.java", LyricsUriBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getLyricsUri", "com.coremedia.iso.boxes.vodafone.LyricsUriBox", "", "", "", "java.lang.String"), 39);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "setLyricsUri", "com.coremedia.iso.boxes.vodafone.LyricsUriBox", "java.lang.String", "lyricsUri", "", "void"), 43);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "toString", "com.coremedia.iso.boxes.vodafone.LyricsUriBox", "", "", "", "java.lang.String"), 64);
   }

   public void _parseDetails(ByteBuffer var1) {
      this.parseVersionAndFlags(var1);
      this.lyricsUri = e.e(var1);
   }

   protected void getContent(ByteBuffer var1) {
      this.writeVersionAndFlags(var1);
      var1.put(j.a(this.lyricsUri));
      var1.put(0);
   }

   protected long getContentSize() {
      return (long)(j.b(this.lyricsUri) + 5);
   }

   public String getLyricsUri() {
      a var1 = b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.lyricsUri;
   }

   public void setLyricsUri(String var1) {
      a var2 = b.a(ajc$tjp_1, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.lyricsUri = var1;
   }

   public String toString() {
      a var1 = b.a(ajc$tjp_2, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return "LyricsUriBox[lyricsUri=" + this.getLyricsUri() + "]";
   }
}
