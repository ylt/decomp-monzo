package com.coremedia.iso.boxes;

import com.coremedia.iso.e;
import com.coremedia.iso.g;
import com.googlecode.mp4parser.AbstractFullBox;
import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class ProgressiveDownloadInformationBox extends AbstractFullBox {
   public static final String TYPE = "pdin";
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_0;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_1;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_2;
   List entries = Collections.emptyList();

   static {
      ajc$preClinit();
   }

   public ProgressiveDownloadInformationBox() {
      super("pdin");
   }

   private static void ajc$preClinit() {
      org.mp4parser.aspectj.a.b.b var0 = new org.mp4parser.aspectj.a.b.b("ProgressiveDownloadInformationBox.java", ProgressiveDownloadInformationBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getEntries", "com.coremedia.iso.boxes.ProgressiveDownloadInformationBox", "", "", "", "java.util.List"), 38);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "setEntries", "com.coremedia.iso.boxes.ProgressiveDownloadInformationBox", "java.util.List", "entries", "", "void"), 42);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "toString", "com.coremedia.iso.boxes.ProgressiveDownloadInformationBox", "", "", "", "java.lang.String"), 112);
   }

   public void _parseDetails(ByteBuffer var1) {
      this.parseVersionAndFlags(var1);
      this.entries = new LinkedList();

      while(var1.remaining() >= 8) {
         ProgressiveDownloadInformationBox.a var2 = new ProgressiveDownloadInformationBox.a(e.a(var1), e.a(var1));
         this.entries.add(var2);
      }

   }

   protected void getContent(ByteBuffer var1) {
      this.writeVersionAndFlags(var1);
      Iterator var3 = this.entries.iterator();

      while(var3.hasNext()) {
         ProgressiveDownloadInformationBox.a var2 = (ProgressiveDownloadInformationBox.a)var3.next();
         g.b(var1, var2.a());
         g.b(var1, var2.b());
      }

   }

   protected long getContentSize() {
      return (long)(this.entries.size() * 8 + 4);
   }

   public List getEntries() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.entries;
   }

   public void setEntries(List var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_1, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.entries = var1;
   }

   public String toString() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_2, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return "ProgressiveDownloadInfoBox{entries=" + this.entries + '}';
   }

   public static class a {
      long a;
      long b;

      public a(long var1, long var3) {
         this.a = var1;
         this.b = var3;
      }

      public long a() {
         return this.a;
      }

      public long b() {
         return this.b;
      }

      public boolean equals(Object var1) {
         boolean var2 = true;
         if(this != var1) {
            if(var1 != null && this.getClass() == var1.getClass()) {
               ProgressiveDownloadInformationBox.a var3 = (ProgressiveDownloadInformationBox.a)var1;
               if(this.b != var3.b) {
                  var2 = false;
               } else if(this.a != var3.a) {
                  var2 = false;
               }
            } else {
               var2 = false;
            }
         }

         return var2;
      }

      public int hashCode() {
         return (int)(this.a ^ this.a >>> 32) * 31 + (int)(this.b ^ this.b >>> 32);
      }

      public String toString() {
         return "Entry{rate=" + this.a + ", initialDelay=" + this.b + '}';
      }
   }
}
