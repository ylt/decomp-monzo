package com.coremedia.iso.boxes;

import com.coremedia.iso.d;
import com.coremedia.iso.e;
import com.coremedia.iso.g;
import com.coremedia.iso.j;
import com.googlecode.mp4parser.AbstractFullBox;
import java.nio.ByteBuffer;

public class SchemeTypeBox extends AbstractFullBox {
   // $FF: synthetic field
   static final boolean $assertionsDisabled;
   public static final String TYPE = "schm";
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_0;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_1;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_2;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_3;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_4;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_5;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_6;
   String schemeType = "    ";
   String schemeUri = null;
   long schemeVersion;

   static {
      ajc$preClinit();
      boolean var0;
      if(!SchemeTypeBox.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      $assertionsDisabled = var0;
   }

   public SchemeTypeBox() {
      super("schm");
   }

   private static void ajc$preClinit() {
      org.mp4parser.aspectj.a.b.b var0 = new org.mp4parser.aspectj.a.b.b("SchemeTypeBox.java", SchemeTypeBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getSchemeType", "com.coremedia.iso.boxes.SchemeTypeBox", "", "", "", "java.lang.String"), 44);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "getSchemeVersion", "com.coremedia.iso.boxes.SchemeTypeBox", "", "", "", "long"), 48);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "getSchemeUri", "com.coremedia.iso.boxes.SchemeTypeBox", "", "", "", "java.lang.String"), 52);
      ajc$tjp_3 = var0.a("method-execution", var0.a("1", "setSchemeType", "com.coremedia.iso.boxes.SchemeTypeBox", "java.lang.String", "schemeType", "", "void"), 56);
      ajc$tjp_4 = var0.a("method-execution", var0.a("1", "setSchemeVersion", "com.coremedia.iso.boxes.SchemeTypeBox", "int", "schemeVersion", "", "void"), 61);
      ajc$tjp_5 = var0.a("method-execution", var0.a("1", "setSchemeUri", "com.coremedia.iso.boxes.SchemeTypeBox", "java.lang.String", "schemeUri", "", "void"), 65);
      ajc$tjp_6 = var0.a("method-execution", var0.a("1", "toString", "com.coremedia.iso.boxes.SchemeTypeBox", "", "", "", "java.lang.String"), 93);
   }

   public void _parseDetails(ByteBuffer var1) {
      this.parseVersionAndFlags(var1);
      this.schemeType = e.k(var1);
      this.schemeVersion = e.a(var1);
      if((this.getFlags() & 1) == 1) {
         this.schemeUri = e.e(var1);
      }

   }

   protected void getContent(ByteBuffer var1) {
      this.writeVersionAndFlags(var1);
      var1.put(d.a(this.schemeType));
      g.b(var1, this.schemeVersion);
      if((this.getFlags() & 1) == 1) {
         var1.put(j.a(this.schemeUri));
      }

   }

   protected long getContentSize() {
      int var1;
      if((this.getFlags() & 1) == 1) {
         var1 = j.b(this.schemeUri) + 1;
      } else {
         var1 = 0;
      }

      return (long)(var1 + 12);
   }

   public String getSchemeType() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.schemeType;
   }

   public String getSchemeUri() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_2, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.schemeUri;
   }

   public long getSchemeVersion() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_1, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.schemeVersion;
   }

   public void setSchemeType(String var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_3, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      if($assertionsDisabled || var1 != null && var1.length() == 4) {
         this.schemeType = var1;
      } else {
         throw new AssertionError("SchemeType may not be null or not 4 bytes long");
      }
   }

   public void setSchemeUri(String var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_5, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.schemeUri = var1;
   }

   public void setSchemeVersion(int var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_4, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.schemeVersion = (long)var1;
   }

   public String toString() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_6, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      StringBuilder var2 = new StringBuilder();
      var2.append("Schema Type Box[");
      var2.append("schemeUri=").append(this.schemeUri).append("; ");
      var2.append("schemeType=").append(this.schemeType).append("; ");
      var2.append("schemeVersion=").append(this.schemeVersion).append("; ");
      var2.append("]");
      return var2.toString();
   }
}
