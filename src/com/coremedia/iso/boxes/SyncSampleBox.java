package com.coremedia.iso.boxes;

import com.coremedia.iso.e;
import com.coremedia.iso.g;
import com.googlecode.mp4parser.AbstractFullBox;
import java.nio.ByteBuffer;

public class SyncSampleBox extends AbstractFullBox {
   public static final String TYPE = "stss";
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_0;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_1;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_2;
   private long[] sampleNumber;

   static {
      ajc$preClinit();
   }

   public SyncSampleBox() {
      super("stss");
   }

   private static void ajc$preClinit() {
      org.mp4parser.aspectj.a.b.b var0 = new org.mp4parser.aspectj.a.b.b("SyncSampleBox.java", SyncSampleBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getSampleNumber", "com.coremedia.iso.boxes.SyncSampleBox", "", "", "", "[J"), 46);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "toString", "com.coremedia.iso.boxes.SyncSampleBox", "", "", "", "java.lang.String"), 77);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "setSampleNumber", "com.coremedia.iso.boxes.SyncSampleBox", "[J", "sampleNumber", "", "void"), 81);
   }

   public void _parseDetails(ByteBuffer var1) {
      this.parseVersionAndFlags(var1);
      int var3 = com.googlecode.mp4parser.c.b.a(e.a(var1));
      this.sampleNumber = new long[var3];

      for(int var2 = 0; var2 < var3; ++var2) {
         this.sampleNumber[var2] = e.a(var1);
      }

   }

   protected void getContent(ByteBuffer var1) {
      this.writeVersionAndFlags(var1);
      g.b(var1, (long)this.sampleNumber.length);
      long[] var4 = this.sampleNumber;
      int var3 = var4.length;

      for(int var2 = 0; var2 < var3; ++var2) {
         g.b(var1, var4[var2]);
      }

   }

   protected long getContentSize() {
      return (long)(this.sampleNumber.length * 4 + 8);
   }

   public long[] getSampleNumber() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.sampleNumber;
   }

   public void setSampleNumber(long[] var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_2, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.sampleNumber = var1;
   }

   public String toString() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_1, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return "SyncSampleBox[entryCount=" + this.sampleNumber.length + "]";
   }
}
