package com.coremedia.iso.boxes;

import com.googlecode.mp4parser.AbstractContainerBox;
import java.util.Iterator;

public class MediaInformationBox extends AbstractContainerBox {
   public static final String TYPE = "minf";

   public MediaInformationBox() {
      super("minf");
   }

   public AbstractMediaHeaderBox getMediaHeaderBox() {
      Iterator var1 = this.getBoxes().iterator();

      AbstractMediaHeaderBox var3;
      while(true) {
         if(!var1.hasNext()) {
            var3 = null;
            break;
         }

         a var2 = (a)var1.next();
         if(var2 instanceof AbstractMediaHeaderBox) {
            var3 = (AbstractMediaHeaderBox)var2;
            break;
         }
      }

      return var3;
   }

   public SampleTableBox getSampleTableBox() {
      Iterator var1 = this.getBoxes().iterator();

      SampleTableBox var3;
      while(true) {
         if(!var1.hasNext()) {
            var3 = null;
            break;
         }

         a var2 = (a)var1.next();
         if(var2 instanceof SampleTableBox) {
            var3 = (SampleTableBox)var2;
            break;
         }
      }

      return var3;
   }
}
