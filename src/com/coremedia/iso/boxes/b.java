package com.coremedia.iso.boxes;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;
import java.util.List;

public interface b {
   List getBoxes();

   List getBoxes(Class var1);

   List getBoxes(Class var1, boolean var2);

   ByteBuffer getByteBuffer(long var1, long var3) throws IOException;

   void writeContainer(WritableByteChannel var1) throws IOException;
}
