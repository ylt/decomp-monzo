package com.coremedia.iso.boxes;

import com.coremedia.iso.e;
import com.coremedia.iso.g;
import com.googlecode.mp4parser.AbstractFullBox;
import com.googlecode.mp4parser.c.c;
import com.googlecode.mp4parser.c.f;
import com.googlecode.mp4parser.c.h;
import java.nio.ByteBuffer;
import java.util.Date;

public class TrackHeaderBox extends AbstractFullBox {
   private static f LOG;
   public static final String TYPE = "tkhd";
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_0;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_1;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_10;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_11;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_12;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_13;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_14;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_15;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_16;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_17;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_18;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_19;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_2;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_20;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_21;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_22;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_23;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_24;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_25;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_26;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_27;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_28;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_29;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_3;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_4;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_5;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_6;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_7;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_8;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_9;
   private int alternateGroup;
   private Date creationTime = new Date(0L);
   private long duration;
   private double height;
   private int layer;
   private h matrix;
   private Date modificationTime = new Date(0L);
   private long trackId;
   private float volume;
   private double width;

   static {
      ajc$preClinit();
      LOG = f.a(TrackHeaderBox.class);
   }

   public TrackHeaderBox() {
      super("tkhd");
      this.matrix = h.j;
   }

   private static void ajc$preClinit() {
      org.mp4parser.aspectj.a.b.b var0 = new org.mp4parser.aspectj.a.b.b("TrackHeaderBox.java", TrackHeaderBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getCreationTime", "com.coremedia.iso.boxes.TrackHeaderBox", "", "", "", "java.util.Date"), 62);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "getModificationTime", "com.coremedia.iso.boxes.TrackHeaderBox", "", "", "", "java.util.Date"), 66);
      ajc$tjp_10 = var0.a("method-execution", var0.a("1", "getContent", "com.coremedia.iso.boxes.TrackHeaderBox", "java.nio.ByteBuffer", "byteBuffer", "", "void"), 145);
      ajc$tjp_11 = var0.a("method-execution", var0.a("1", "toString", "com.coremedia.iso.boxes.TrackHeaderBox", "", "", "", "java.lang.String"), 173);
      ajc$tjp_12 = var0.a("method-execution", var0.a("1", "setCreationTime", "com.coremedia.iso.boxes.TrackHeaderBox", "java.util.Date", "creationTime", "", "void"), 199);
      ajc$tjp_13 = var0.a("method-execution", var0.a("1", "setModificationTime", "com.coremedia.iso.boxes.TrackHeaderBox", "java.util.Date", "modificationTime", "", "void"), 206);
      ajc$tjp_14 = var0.a("method-execution", var0.a("1", "setTrackId", "com.coremedia.iso.boxes.TrackHeaderBox", "long", "trackId", "", "void"), 214);
      ajc$tjp_15 = var0.a("method-execution", var0.a("1", "setDuration", "com.coremedia.iso.boxes.TrackHeaderBox", "long", "duration", "", "void"), 218);
      ajc$tjp_16 = var0.a("method-execution", var0.a("1", "setLayer", "com.coremedia.iso.boxes.TrackHeaderBox", "int", "layer", "", "void"), 225);
      ajc$tjp_17 = var0.a("method-execution", var0.a("1", "setAlternateGroup", "com.coremedia.iso.boxes.TrackHeaderBox", "int", "alternateGroup", "", "void"), 229);
      ajc$tjp_18 = var0.a("method-execution", var0.a("1", "setVolume", "com.coremedia.iso.boxes.TrackHeaderBox", "float", "volume", "", "void"), 233);
      ajc$tjp_19 = var0.a("method-execution", var0.a("1", "setMatrix", "com.coremedia.iso.boxes.TrackHeaderBox", "com.googlecode.mp4parser.util.Matrix", "matrix", "", "void"), 237);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "getTrackId", "com.coremedia.iso.boxes.TrackHeaderBox", "", "", "", "long"), 70);
      ajc$tjp_20 = var0.a("method-execution", var0.a("1", "setWidth", "com.coremedia.iso.boxes.TrackHeaderBox", "double", "width", "", "void"), 241);
      ajc$tjp_21 = var0.a("method-execution", var0.a("1", "setHeight", "com.coremedia.iso.boxes.TrackHeaderBox", "double", "height", "", "void"), 245);
      ajc$tjp_22 = var0.a("method-execution", var0.a("1", "isEnabled", "com.coremedia.iso.boxes.TrackHeaderBox", "", "", "", "boolean"), 250);
      ajc$tjp_23 = var0.a("method-execution", var0.a("1", "isInMovie", "com.coremedia.iso.boxes.TrackHeaderBox", "", "", "", "boolean"), 254);
      ajc$tjp_24 = var0.a("method-execution", var0.a("1", "isInPreview", "com.coremedia.iso.boxes.TrackHeaderBox", "", "", "", "boolean"), 258);
      ajc$tjp_25 = var0.a("method-execution", var0.a("1", "isInPoster", "com.coremedia.iso.boxes.TrackHeaderBox", "", "", "", "boolean"), 262);
      ajc$tjp_26 = var0.a("method-execution", var0.a("1", "setEnabled", "com.coremedia.iso.boxes.TrackHeaderBox", "boolean", "enabled", "", "void"), 266);
      ajc$tjp_27 = var0.a("method-execution", var0.a("1", "setInMovie", "com.coremedia.iso.boxes.TrackHeaderBox", "boolean", "inMovie", "", "void"), 274);
      ajc$tjp_28 = var0.a("method-execution", var0.a("1", "setInPreview", "com.coremedia.iso.boxes.TrackHeaderBox", "boolean", "inPreview", "", "void"), 282);
      ajc$tjp_29 = var0.a("method-execution", var0.a("1", "setInPoster", "com.coremedia.iso.boxes.TrackHeaderBox", "boolean", "inPoster", "", "void"), 290);
      ajc$tjp_3 = var0.a("method-execution", var0.a("1", "getDuration", "com.coremedia.iso.boxes.TrackHeaderBox", "", "", "", "long"), 74);
      ajc$tjp_4 = var0.a("method-execution", var0.a("1", "getLayer", "com.coremedia.iso.boxes.TrackHeaderBox", "", "", "", "int"), 78);
      ajc$tjp_5 = var0.a("method-execution", var0.a("1", "getAlternateGroup", "com.coremedia.iso.boxes.TrackHeaderBox", "", "", "", "int"), 82);
      ajc$tjp_6 = var0.a("method-execution", var0.a("1", "getVolume", "com.coremedia.iso.boxes.TrackHeaderBox", "", "", "", "float"), 86);
      ajc$tjp_7 = var0.a("method-execution", var0.a("1", "getMatrix", "com.coremedia.iso.boxes.TrackHeaderBox", "", "", "", "com.googlecode.mp4parser.util.Matrix"), 90);
      ajc$tjp_8 = var0.a("method-execution", var0.a("1", "getWidth", "com.coremedia.iso.boxes.TrackHeaderBox", "", "", "", "double"), 94);
      ajc$tjp_9 = var0.a("method-execution", var0.a("1", "getHeight", "com.coremedia.iso.boxes.TrackHeaderBox", "", "", "", "double"), 98);
   }

   public void _parseDetails(ByteBuffer var1) {
      this.parseVersionAndFlags(var1);
      if(this.getVersion() == 1) {
         this.creationTime = c.a(e.f(var1));
         this.modificationTime = c.a(e.f(var1));
         this.trackId = e.a(var1);
         e.a(var1);
         this.duration = var1.getLong();
      } else {
         this.creationTime = c.a(e.a(var1));
         this.modificationTime = c.a(e.a(var1));
         this.trackId = e.a(var1);
         e.a(var1);
         this.duration = (long)var1.getInt();
      }

      if(this.duration < -1L) {
         LOG.b("tkhd duration is not in expected range");
      }

      e.a(var1);
      e.a(var1);
      this.layer = e.c(var1);
      this.alternateGroup = e.c(var1);
      this.volume = e.i(var1);
      e.c(var1);
      this.matrix = h.a(var1);
      this.width = e.g(var1);
      this.height = e.g(var1);
   }

   public int getAlternateGroup() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_5, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.alternateGroup;
   }

   public void getContent(ByteBuffer var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_10, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.writeVersionAndFlags(var1);
      if(this.getVersion() == 1) {
         g.a(var1, c.a(this.creationTime));
         g.a(var1, c.a(this.modificationTime));
         g.b(var1, this.trackId);
         g.b(var1, 0L);
         var1.putLong(this.duration);
      } else {
         g.b(var1, c.a(this.creationTime));
         g.b(var1, c.a(this.modificationTime));
         g.b(var1, this.trackId);
         g.b(var1, 0L);
         var1.putInt((int)this.duration);
      }

      g.b(var1, 0L);
      g.b(var1, 0L);
      g.b(var1, this.layer);
      g.b(var1, this.alternateGroup);
      g.c(var1, (double)this.volume);
      g.b(var1, 0);
      this.matrix.b(var1);
      g.a(var1, this.width);
      g.a(var1, this.height);
   }

   protected long getContentSize() {
      long var1;
      if(this.getVersion() == 1) {
         var1 = 4L + 32L;
      } else {
         var1 = 4L + 20L;
      }

      return var1 + 60L;
   }

   public Date getCreationTime() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.creationTime;
   }

   public long getDuration() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_3, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.duration;
   }

   public double getHeight() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_9, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.height;
   }

   public int getLayer() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_4, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.layer;
   }

   public h getMatrix() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_7, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.matrix;
   }

   public Date getModificationTime() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_1, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.modificationTime;
   }

   public long getTrackId() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_2, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.trackId;
   }

   public float getVolume() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_6, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.volume;
   }

   public double getWidth() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_8, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.width;
   }

   public boolean isEnabled() {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_22, this, this);
      com.googlecode.mp4parser.e.a().a(var2);
      boolean var1;
      if((this.getFlags() & 1) > 0) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public boolean isInMovie() {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_23, this, this);
      com.googlecode.mp4parser.e.a().a(var2);
      boolean var1;
      if((this.getFlags() & 2) > 0) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public boolean isInPoster() {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_25, this, this);
      com.googlecode.mp4parser.e.a().a(var2);
      boolean var1;
      if((this.getFlags() & 8) > 0) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public boolean isInPreview() {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_24, this, this);
      com.googlecode.mp4parser.e.a().a(var2);
      boolean var1;
      if((this.getFlags() & 4) > 0) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public void setAlternateGroup(int var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_17, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.alternateGroup = var1;
   }

   public void setCreationTime(Date var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_12, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.creationTime = var1;
      if(c.a(var1) >= 4294967296L) {
         this.setVersion(1);
      }

   }

   public void setDuration(long var1) {
      org.mp4parser.aspectj.lang.a var3 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_15, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var3);
      this.duration = var1;
      if(var1 >= 4294967296L) {
         this.setFlags(1);
      }

   }

   public void setEnabled(boolean var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_26, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      if(var1) {
         this.setFlags(this.getFlags() | 1);
      } else {
         this.setFlags(this.getFlags() & -2);
      }

   }

   public void setHeight(double var1) {
      org.mp4parser.aspectj.lang.a var3 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_21, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var3);
      this.height = var1;
   }

   public void setInMovie(boolean var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_27, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      if(var1) {
         this.setFlags(this.getFlags() | 2);
      } else {
         this.setFlags(this.getFlags() & -3);
      }

   }

   public void setInPoster(boolean var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_29, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      if(var1) {
         this.setFlags(this.getFlags() | 8);
      } else {
         this.setFlags(this.getFlags() & -9);
      }

   }

   public void setInPreview(boolean var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_28, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      if(var1) {
         this.setFlags(this.getFlags() | 4);
      } else {
         this.setFlags(this.getFlags() & -5);
      }

   }

   public void setLayer(int var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_16, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.layer = var1;
   }

   public void setMatrix(h var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_19, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.matrix = var1;
   }

   public void setModificationTime(Date var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_13, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.modificationTime = var1;
      if(c.a(var1) >= 4294967296L) {
         this.setVersion(1);
      }

   }

   public void setTrackId(long var1) {
      org.mp4parser.aspectj.lang.a var3 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_14, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var3);
      this.trackId = var1;
   }

   public void setVolume(float var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_18, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.volume = var1;
   }

   public void setWidth(double var1) {
      org.mp4parser.aspectj.lang.a var3 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_20, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var3);
      this.width = var1;
   }

   public String toString() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_11, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      StringBuilder var2 = new StringBuilder();
      var2.append("TrackHeaderBox[");
      var2.append("creationTime=").append(this.getCreationTime());
      var2.append(";");
      var2.append("modificationTime=").append(this.getModificationTime());
      var2.append(";");
      var2.append("trackId=").append(this.getTrackId());
      var2.append(";");
      var2.append("duration=").append(this.getDuration());
      var2.append(";");
      var2.append("layer=").append(this.getLayer());
      var2.append(";");
      var2.append("alternateGroup=").append(this.getAlternateGroup());
      var2.append(";");
      var2.append("volume=").append(this.getVolume());
      var2.append(";");
      var2.append("matrix=").append(this.matrix);
      var2.append(";");
      var2.append("width=").append(this.getWidth());
      var2.append(";");
      var2.append("height=").append(this.getHeight());
      var2.append("]");
      return var2.toString();
   }
}
