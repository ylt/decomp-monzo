package com.coremedia.iso.boxes;

import com.coremedia.iso.e;
import com.coremedia.iso.g;
import com.googlecode.mp4parser.AbstractFullBox;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;

public class TimeToSampleBox extends AbstractFullBox {
   // $FF: synthetic field
   static final boolean $assertionsDisabled;
   public static final String TYPE = "stts";
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_0;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_1;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_2;
   static Map cache;
   List entries = Collections.emptyList();

   static {
      ajc$preClinit();
      boolean var0;
      if(!TimeToSampleBox.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      $assertionsDisabled = var0;
      cache = new WeakHashMap();
   }

   public TimeToSampleBox() {
      super("stts");
   }

   private static void ajc$preClinit() {
      org.mp4parser.aspectj.a.b.b var0 = new org.mp4parser.aspectj.a.b.b("TimeToSampleBox.java", TimeToSampleBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getEntries", "com.coremedia.iso.boxes.TimeToSampleBox", "", "", "", "java.util.List"), 79);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "setEntries", "com.coremedia.iso.boxes.TimeToSampleBox", "java.util.List", "entries", "", "void"), 83);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "toString", "com.coremedia.iso.boxes.TimeToSampleBox", "", "", "", "java.lang.String"), 87);
   }

   public static long[] blowupTimeToSamples(List param0) {
      // $FF: Couldn't be decompiled
   }

   public void _parseDetails(ByteBuffer var1) {
      this.parseVersionAndFlags(var1);
      int var3 = com.googlecode.mp4parser.c.b.a(e.a(var1));
      this.entries = new ArrayList(var3);

      for(int var2 = 0; var2 < var3; ++var2) {
         this.entries.add(new TimeToSampleBox.a(e.a(var1), e.a(var1)));
      }

   }

   protected void getContent(ByteBuffer var1) {
      this.writeVersionAndFlags(var1);
      g.b(var1, (long)this.entries.size());
      Iterator var3 = this.entries.iterator();

      while(var3.hasNext()) {
         TimeToSampleBox.a var2 = (TimeToSampleBox.a)var3.next();
         g.b(var1, var2.a());
         g.b(var1, var2.b());
      }

   }

   protected long getContentSize() {
      return (long)(this.entries.size() * 8 + 8);
   }

   public List getEntries() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.entries;
   }

   public void setEntries(List var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_1, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.entries = var1;
   }

   public String toString() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_2, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return "TimeToSampleBox[entryCount=" + this.entries.size() + "]";
   }

   public static class a {
      long a;
      long b;

      public a(long var1, long var3) {
         this.a = var1;
         this.b = var3;
      }

      public long a() {
         return this.a;
      }

      public void a(long var1) {
         this.a = var1;
      }

      public long b() {
         return this.b;
      }

      public void b(long var1) {
         this.b = var1;
      }

      public String toString() {
         return "Entry{count=" + this.a + ", delta=" + this.b + '}';
      }
   }
}
