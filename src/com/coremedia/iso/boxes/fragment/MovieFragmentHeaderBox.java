package com.coremedia.iso.boxes.fragment;

import com.coremedia.iso.e;
import com.coremedia.iso.g;
import com.googlecode.mp4parser.AbstractFullBox;
import java.nio.ByteBuffer;
import org.mp4parser.aspectj.a.b.b;

public class MovieFragmentHeaderBox extends AbstractFullBox {
   public static final String TYPE = "mfhd";
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_0;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_1;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_2;
   private long sequenceNumber;

   static {
      ajc$preClinit();
   }

   public MovieFragmentHeaderBox() {
      super("mfhd");
   }

   private static void ajc$preClinit() {
      b var0 = new b("MovieFragmentHeaderBox.java", MovieFragmentHeaderBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getSequenceNumber", "com.coremedia.iso.boxes.fragment.MovieFragmentHeaderBox", "", "", "", "long"), 59);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "setSequenceNumber", "com.coremedia.iso.boxes.fragment.MovieFragmentHeaderBox", "long", "sequenceNumber", "", "void"), 63);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "toString", "com.coremedia.iso.boxes.fragment.MovieFragmentHeaderBox", "", "", "", "java.lang.String"), 68);
   }

   public void _parseDetails(ByteBuffer var1) {
      this.parseVersionAndFlags(var1);
      this.sequenceNumber = e.a(var1);
   }

   protected void getContent(ByteBuffer var1) {
      this.writeVersionAndFlags(var1);
      g.b(var1, this.sequenceNumber);
   }

   protected long getContentSize() {
      return 8L;
   }

   public long getSequenceNumber() {
      org.mp4parser.aspectj.lang.a var1 = b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.sequenceNumber;
   }

   public void setSequenceNumber(long var1) {
      org.mp4parser.aspectj.lang.a var3 = b.a(ajc$tjp_1, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var3);
      this.sequenceNumber = var1;
   }

   public String toString() {
      org.mp4parser.aspectj.lang.a var1 = b.a(ajc$tjp_2, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return "MovieFragmentHeaderBox{sequenceNumber=" + this.sequenceNumber + '}';
   }
}
