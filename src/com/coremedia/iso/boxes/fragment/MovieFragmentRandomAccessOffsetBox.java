package com.coremedia.iso.boxes.fragment;

import com.coremedia.iso.e;
import com.coremedia.iso.g;
import com.googlecode.mp4parser.AbstractFullBox;
import java.nio.ByteBuffer;
import org.mp4parser.aspectj.a.b.b;

public class MovieFragmentRandomAccessOffsetBox extends AbstractFullBox {
   public static final String TYPE = "mfro";
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_0;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_1;
   private long mfraSize;

   static {
      ajc$preClinit();
   }

   public MovieFragmentRandomAccessOffsetBox() {
      super("mfro");
   }

   private static void ajc$preClinit() {
      b var0 = new b("MovieFragmentRandomAccessOffsetBox.java", MovieFragmentRandomAccessOffsetBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getMfraSize", "com.coremedia.iso.boxes.fragment.MovieFragmentRandomAccessOffsetBox", "", "", "", "long"), 56);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "setMfraSize", "com.coremedia.iso.boxes.fragment.MovieFragmentRandomAccessOffsetBox", "long", "mfraSize", "", "void"), 60);
   }

   public void _parseDetails(ByteBuffer var1) {
      this.parseVersionAndFlags(var1);
      this.mfraSize = e.a(var1);
   }

   protected void getContent(ByteBuffer var1) {
      this.writeVersionAndFlags(var1);
      g.b(var1, this.mfraSize);
   }

   protected long getContentSize() {
      return 8L;
   }

   public long getMfraSize() {
      org.mp4parser.aspectj.lang.a var1 = b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.mfraSize;
   }

   public void setMfraSize(long var1) {
      org.mp4parser.aspectj.lang.a var3 = b.a(ajc$tjp_1, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var3);
      this.mfraSize = var1;
   }
}
