package com.coremedia.iso.boxes.fragment;

import com.coremedia.iso.e;
import com.coremedia.iso.g;
import com.googlecode.mp4parser.AbstractFullBox;
import java.nio.ByteBuffer;
import org.mp4parser.aspectj.a.b.b;

public class TrackFragmentHeaderBox extends AbstractFullBox {
   public static final String TYPE = "tfhd";
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_0;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_1;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_10;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_11;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_12;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_13;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_14;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_15;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_16;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_17;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_18;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_19;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_2;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_20;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_21;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_3;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_4;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_5;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_6;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_7;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_8;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_9;
   private long baseDataOffset = -1L;
   private boolean defaultBaseIsMoof;
   private long defaultSampleDuration = -1L;
   private a defaultSampleFlags;
   private long defaultSampleSize = -1L;
   private boolean durationIsEmpty;
   private long sampleDescriptionIndex;
   private long trackId;

   static {
      ajc$preClinit();
   }

   public TrackFragmentHeaderBox() {
      super("tfhd");
   }

   private static void ajc$preClinit() {
      b var0 = new b("TrackFragmentHeaderBox.java", TrackFragmentHeaderBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "hasBaseDataOffset", "com.coremedia.iso.boxes.fragment.TrackFragmentHeaderBox", "", "", "", "boolean"), 126);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "hasSampleDescriptionIndex", "com.coremedia.iso.boxes.fragment.TrackFragmentHeaderBox", "", "", "", "boolean"), 130);
      ajc$tjp_10 = var0.a("method-execution", var0.a("1", "setSampleDescriptionIndex", "com.coremedia.iso.boxes.fragment.TrackFragmentHeaderBox", "long", "sampleDescriptionIndex", "", "void"), 171);
      ajc$tjp_11 = var0.a("method-execution", var0.a("1", "getDefaultSampleDuration", "com.coremedia.iso.boxes.fragment.TrackFragmentHeaderBox", "", "", "", "long"), 180);
      ajc$tjp_12 = var0.a("method-execution", var0.a("1", "setDefaultSampleDuration", "com.coremedia.iso.boxes.fragment.TrackFragmentHeaderBox", "long", "defaultSampleDuration", "", "void"), 184);
      ajc$tjp_13 = var0.a("method-execution", var0.a("1", "getDefaultSampleSize", "com.coremedia.iso.boxes.fragment.TrackFragmentHeaderBox", "", "", "", "long"), 191);
      ajc$tjp_14 = var0.a("method-execution", var0.a("1", "setDefaultSampleSize", "com.coremedia.iso.boxes.fragment.TrackFragmentHeaderBox", "long", "defaultSampleSize", "", "void"), 195);
      ajc$tjp_15 = var0.a("method-execution", var0.a("1", "getDefaultSampleFlags", "com.coremedia.iso.boxes.fragment.TrackFragmentHeaderBox", "", "", "", "com.coremedia.iso.boxes.fragment.SampleFlags"), 204);
      ajc$tjp_16 = var0.a("method-execution", var0.a("1", "setDefaultSampleFlags", "com.coremedia.iso.boxes.fragment.TrackFragmentHeaderBox", "com.coremedia.iso.boxes.fragment.SampleFlags", "defaultSampleFlags", "", "void"), 208);
      ajc$tjp_17 = var0.a("method-execution", var0.a("1", "isDurationIsEmpty", "com.coremedia.iso.boxes.fragment.TrackFragmentHeaderBox", "", "", "", "boolean"), 217);
      ajc$tjp_18 = var0.a("method-execution", var0.a("1", "setDurationIsEmpty", "com.coremedia.iso.boxes.fragment.TrackFragmentHeaderBox", "boolean", "durationIsEmpty", "", "void"), 221);
      ajc$tjp_19 = var0.a("method-execution", var0.a("1", "isDefaultBaseIsMoof", "com.coremedia.iso.boxes.fragment.TrackFragmentHeaderBox", "", "", "", "boolean"), 230);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "hasDefaultSampleDuration", "com.coremedia.iso.boxes.fragment.TrackFragmentHeaderBox", "", "", "", "boolean"), 134);
      ajc$tjp_20 = var0.a("method-execution", var0.a("1", "setDefaultBaseIsMoof", "com.coremedia.iso.boxes.fragment.TrackFragmentHeaderBox", "boolean", "defaultBaseIsMoof", "", "void"), 234);
      ajc$tjp_21 = var0.a("method-execution", var0.a("1", "toString", "com.coremedia.iso.boxes.fragment.TrackFragmentHeaderBox", "", "", "", "java.lang.String"), 244);
      ajc$tjp_3 = var0.a("method-execution", var0.a("1", "hasDefaultSampleSize", "com.coremedia.iso.boxes.fragment.TrackFragmentHeaderBox", "", "", "", "boolean"), 138);
      ajc$tjp_4 = var0.a("method-execution", var0.a("1", "hasDefaultSampleFlags", "com.coremedia.iso.boxes.fragment.TrackFragmentHeaderBox", "", "", "", "boolean"), 142);
      ajc$tjp_5 = var0.a("method-execution", var0.a("1", "getTrackId", "com.coremedia.iso.boxes.fragment.TrackFragmentHeaderBox", "", "", "", "long"), 146);
      ajc$tjp_6 = var0.a("method-execution", var0.a("1", "setTrackId", "com.coremedia.iso.boxes.fragment.TrackFragmentHeaderBox", "long", "trackId", "", "void"), 150);
      ajc$tjp_7 = var0.a("method-execution", var0.a("1", "getBaseDataOffset", "com.coremedia.iso.boxes.fragment.TrackFragmentHeaderBox", "", "", "", "long"), 154);
      ajc$tjp_8 = var0.a("method-execution", var0.a("1", "setBaseDataOffset", "com.coremedia.iso.boxes.fragment.TrackFragmentHeaderBox", "long", "baseDataOffset", "", "void"), 158);
      ajc$tjp_9 = var0.a("method-execution", var0.a("1", "getSampleDescriptionIndex", "com.coremedia.iso.boxes.fragment.TrackFragmentHeaderBox", "", "", "", "long"), 167);
   }

   public void _parseDetails(ByteBuffer var1) {
      this.parseVersionAndFlags(var1);
      this.trackId = e.a(var1);
      if((this.getFlags() & 1) == 1) {
         this.baseDataOffset = e.f(var1);
      }

      if((this.getFlags() & 2) == 2) {
         this.sampleDescriptionIndex = e.a(var1);
      }

      if((this.getFlags() & 8) == 8) {
         this.defaultSampleDuration = e.a(var1);
      }

      if((this.getFlags() & 16) == 16) {
         this.defaultSampleSize = e.a(var1);
      }

      if((this.getFlags() & 32) == 32) {
         this.defaultSampleFlags = new a(var1);
      }

      if((this.getFlags() & 65536) == 65536) {
         this.durationIsEmpty = true;
      }

      if((this.getFlags() & 131072) == 131072) {
         this.defaultBaseIsMoof = true;
      }

   }

   public long getBaseDataOffset() {
      org.mp4parser.aspectj.lang.a var1 = b.a(ajc$tjp_7, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.baseDataOffset;
   }

   protected void getContent(ByteBuffer var1) {
      this.writeVersionAndFlags(var1);
      g.b(var1, this.trackId);
      if((this.getFlags() & 1) == 1) {
         g.a(var1, this.getBaseDataOffset());
      }

      if((this.getFlags() & 2) == 2) {
         g.b(var1, this.getSampleDescriptionIndex());
      }

      if((this.getFlags() & 8) == 8) {
         g.b(var1, this.getDefaultSampleDuration());
      }

      if((this.getFlags() & 16) == 16) {
         g.b(var1, this.getDefaultSampleSize());
      }

      if((this.getFlags() & 32) == 32) {
         this.defaultSampleFlags.a(var1);
      }

   }

   protected long getContentSize() {
      long var4 = 8L;
      int var1 = this.getFlags();
      if((var1 & 1) == 1) {
         var4 = 8L + 8L;
      }

      long var2 = var4;
      if((var1 & 2) == 2) {
         var2 = var4 + 4L;
      }

      var4 = var2;
      if((var1 & 8) == 8) {
         var4 = var2 + 4L;
      }

      var2 = var4;
      if((var1 & 16) == 16) {
         var2 = var4 + 4L;
      }

      var4 = var2;
      if((var1 & 32) == 32) {
         var4 = var2 + 4L;
      }

      return var4;
   }

   public long getDefaultSampleDuration() {
      org.mp4parser.aspectj.lang.a var1 = b.a(ajc$tjp_11, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.defaultSampleDuration;
   }

   public a getDefaultSampleFlags() {
      org.mp4parser.aspectj.lang.a var1 = b.a(ajc$tjp_15, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.defaultSampleFlags;
   }

   public long getDefaultSampleSize() {
      org.mp4parser.aspectj.lang.a var1 = b.a(ajc$tjp_13, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.defaultSampleSize;
   }

   public long getSampleDescriptionIndex() {
      org.mp4parser.aspectj.lang.a var1 = b.a(ajc$tjp_9, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.sampleDescriptionIndex;
   }

   public long getTrackId() {
      org.mp4parser.aspectj.lang.a var1 = b.a(ajc$tjp_5, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.trackId;
   }

   public boolean hasBaseDataOffset() {
      org.mp4parser.aspectj.lang.a var2 = b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var2);
      boolean var1;
      if((this.getFlags() & 1) != 0) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public boolean hasDefaultSampleDuration() {
      org.mp4parser.aspectj.lang.a var2 = b.a(ajc$tjp_2, this, this);
      com.googlecode.mp4parser.e.a().a(var2);
      boolean var1;
      if((this.getFlags() & 8) != 0) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public boolean hasDefaultSampleFlags() {
      org.mp4parser.aspectj.lang.a var2 = b.a(ajc$tjp_4, this, this);
      com.googlecode.mp4parser.e.a().a(var2);
      boolean var1;
      if((this.getFlags() & 32) != 0) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public boolean hasDefaultSampleSize() {
      org.mp4parser.aspectj.lang.a var2 = b.a(ajc$tjp_3, this, this);
      com.googlecode.mp4parser.e.a().a(var2);
      boolean var1;
      if((this.getFlags() & 16) != 0) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public boolean hasSampleDescriptionIndex() {
      org.mp4parser.aspectj.lang.a var2 = b.a(ajc$tjp_1, this, this);
      com.googlecode.mp4parser.e.a().a(var2);
      boolean var1;
      if((this.getFlags() & 2) != 0) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public boolean isDefaultBaseIsMoof() {
      org.mp4parser.aspectj.lang.a var1 = b.a(ajc$tjp_19, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.defaultBaseIsMoof;
   }

   public boolean isDurationIsEmpty() {
      org.mp4parser.aspectj.lang.a var1 = b.a(ajc$tjp_17, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.durationIsEmpty;
   }

   public void setBaseDataOffset(long var1) {
      org.mp4parser.aspectj.lang.a var3 = b.a(ajc$tjp_8, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var3);
      if(var1 == -1L) {
         this.setFlags(this.getFlags() & 2147483646);
      } else {
         this.setFlags(this.getFlags() | 1);
      }

      this.baseDataOffset = var1;
   }

   public void setDefaultBaseIsMoof(boolean var1) {
      org.mp4parser.aspectj.lang.a var2 = b.a(ajc$tjp_20, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      if(var1) {
         this.setFlags(this.getFlags() | 131072);
      } else {
         this.setFlags(this.getFlags() & 16646143);
      }

      this.defaultBaseIsMoof = var1;
   }

   public void setDefaultSampleDuration(long var1) {
      org.mp4parser.aspectj.lang.a var3 = b.a(ajc$tjp_12, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var3);
      this.setFlags(this.getFlags() | 8);
      this.defaultSampleDuration = var1;
   }

   public void setDefaultSampleFlags(a var1) {
      org.mp4parser.aspectj.lang.a var2 = b.a(ajc$tjp_16, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      if(var1 != null) {
         this.setFlags(this.getFlags() | 32);
      } else {
         this.setFlags(this.getFlags() & 16777183);
      }

      this.defaultSampleFlags = var1;
   }

   public void setDefaultSampleSize(long var1) {
      org.mp4parser.aspectj.lang.a var3 = b.a(ajc$tjp_14, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var3);
      if(var1 != -1L) {
         this.setFlags(this.getFlags() | 16);
      } else {
         this.setFlags(this.getFlags() & 16777199);
      }

      this.defaultSampleSize = var1;
   }

   public void setDurationIsEmpty(boolean var1) {
      org.mp4parser.aspectj.lang.a var2 = b.a(ajc$tjp_18, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      if(this.defaultBaseIsMoof) {
         this.setFlags(this.getFlags() | 65536);
      } else {
         this.setFlags(this.getFlags() & 16711679);
      }

      this.durationIsEmpty = var1;
   }

   public void setSampleDescriptionIndex(long var1) {
      org.mp4parser.aspectj.lang.a var3 = b.a(ajc$tjp_10, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var3);
      if(var1 == -1L) {
         this.setFlags(this.getFlags() & 2147483645);
      } else {
         this.setFlags(this.getFlags() | 2);
      }

      this.sampleDescriptionIndex = var1;
   }

   public void setTrackId(long var1) {
      org.mp4parser.aspectj.lang.a var3 = b.a(ajc$tjp_6, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var3);
      this.trackId = var1;
   }

   public String toString() {
      org.mp4parser.aspectj.lang.a var1 = b.a(ajc$tjp_21, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      StringBuilder var2 = new StringBuilder();
      var2.append("TrackFragmentHeaderBox");
      var2.append("{trackId=").append(this.trackId);
      var2.append(", baseDataOffset=").append(this.baseDataOffset);
      var2.append(", sampleDescriptionIndex=").append(this.sampleDescriptionIndex);
      var2.append(", defaultSampleDuration=").append(this.defaultSampleDuration);
      var2.append(", defaultSampleSize=").append(this.defaultSampleSize);
      var2.append(", defaultSampleFlags=").append(this.defaultSampleFlags);
      var2.append(", durationIsEmpty=").append(this.durationIsEmpty);
      var2.append(", defaultBaseIsMoof=").append(this.defaultBaseIsMoof);
      var2.append('}');
      return var2.toString();
   }
}
