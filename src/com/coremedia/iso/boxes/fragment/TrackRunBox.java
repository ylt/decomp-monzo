package com.coremedia.iso.boxes.fragment;

import com.coremedia.iso.e;
import com.coremedia.iso.g;
import com.googlecode.mp4parser.AbstractFullBox;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.mp4parser.aspectj.a.b.b;

public class TrackRunBox extends AbstractFullBox {
   public static final String TYPE = "trun";
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_0;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_1;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_10;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_11;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_12;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_13;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_14;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_15;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_16;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_17;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_18;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_19;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_2;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_3;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_4;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_5;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_6;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_7;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_8;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_9;
   private int dataOffset;
   private List entries = new ArrayList();
   private a firstSampleFlags;

   static {
      ajc$preClinit();
   }

   public TrackRunBox() {
      super("trun");
   }

   private static void ajc$preClinit() {
      b var0 = new b("TrackRunBox.java", TrackRunBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getEntries", "com.coremedia.iso.boxes.fragment.TrackRunBox", "", "", "", "java.util.List"), 57);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "setDataOffset", "com.coremedia.iso.boxes.fragment.TrackRunBox", "int", "dataOffset", "", "void"), 120);
      ajc$tjp_10 = var0.a("method-execution", var0.a("1", "setDataOffsetPresent", "com.coremedia.iso.boxes.fragment.TrackRunBox", "boolean", "v", "", "void"), 267);
      ajc$tjp_11 = var0.a("method-execution", var0.a("1", "setSampleSizePresent", "com.coremedia.iso.boxes.fragment.TrackRunBox", "boolean", "v", "", "void"), 275);
      ajc$tjp_12 = var0.a("method-execution", var0.a("1", "setSampleDurationPresent", "com.coremedia.iso.boxes.fragment.TrackRunBox", "boolean", "v", "", "void"), 283);
      ajc$tjp_13 = var0.a("method-execution", var0.a("1", "setSampleFlagsPresent", "com.coremedia.iso.boxes.fragment.TrackRunBox", "boolean", "v", "", "void"), 292);
      ajc$tjp_14 = var0.a("method-execution", var0.a("1", "setSampleCompositionTimeOffsetPresent", "com.coremedia.iso.boxes.fragment.TrackRunBox", "boolean", "v", "", "void"), 300);
      ajc$tjp_15 = var0.a("method-execution", var0.a("1", "getDataOffset", "com.coremedia.iso.boxes.fragment.TrackRunBox", "", "", "", "int"), 309);
      ajc$tjp_16 = var0.a("method-execution", var0.a("1", "getFirstSampleFlags", "com.coremedia.iso.boxes.fragment.TrackRunBox", "", "", "", "com.coremedia.iso.boxes.fragment.SampleFlags"), 313);
      ajc$tjp_17 = var0.a("method-execution", var0.a("1", "setFirstSampleFlags", "com.coremedia.iso.boxes.fragment.TrackRunBox", "com.coremedia.iso.boxes.fragment.SampleFlags", "firstSampleFlags", "", "void"), 317);
      ajc$tjp_18 = var0.a("method-execution", var0.a("1", "toString", "com.coremedia.iso.boxes.fragment.TrackRunBox", "", "", "", "java.lang.String"), 327);
      ajc$tjp_19 = var0.a("method-execution", var0.a("1", "setEntries", "com.coremedia.iso.boxes.fragment.TrackRunBox", "java.util.List", "entries", "", "void"), 342);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "getSampleCompositionTimeOffsets", "com.coremedia.iso.boxes.fragment.TrackRunBox", "", "", "", "[J"), 129);
      ajc$tjp_3 = var0.a("method-execution", var0.a("1", "getSampleCount", "com.coremedia.iso.boxes.fragment.TrackRunBox", "", "", "", "long"), 238);
      ajc$tjp_4 = var0.a("method-execution", var0.a("1", "isDataOffsetPresent", "com.coremedia.iso.boxes.fragment.TrackRunBox", "", "", "", "boolean"), 242);
      ajc$tjp_5 = var0.a("method-execution", var0.a("1", "isFirstSampleFlagsPresent", "com.coremedia.iso.boxes.fragment.TrackRunBox", "", "", "", "boolean"), 246);
      ajc$tjp_6 = var0.a("method-execution", var0.a("1", "isSampleSizePresent", "com.coremedia.iso.boxes.fragment.TrackRunBox", "", "", "", "boolean"), 251);
      ajc$tjp_7 = var0.a("method-execution", var0.a("1", "isSampleDurationPresent", "com.coremedia.iso.boxes.fragment.TrackRunBox", "", "", "", "boolean"), 255);
      ajc$tjp_8 = var0.a("method-execution", var0.a("1", "isSampleFlagsPresent", "com.coremedia.iso.boxes.fragment.TrackRunBox", "", "", "", "boolean"), 259);
      ajc$tjp_9 = var0.a("method-execution", var0.a("1", "isSampleCompositionTimeOffsetPresent", "com.coremedia.iso.boxes.fragment.TrackRunBox", "", "", "", "boolean"), 263);
   }

   public void _parseDetails(ByteBuffer var1) {
      this.parseVersionAndFlags(var1);
      long var3 = e.a(var1);
      if((this.getFlags() & 1) == 1) {
         this.dataOffset = com.googlecode.mp4parser.c.b.a(e.a(var1));
      } else {
         this.dataOffset = -1;
      }

      if((this.getFlags() & 4) == 4) {
         this.firstSampleFlags = new a(var1);
      }

      for(int var2 = 0; (long)var2 < var3; ++var2) {
         TrackRunBox.a var5 = new TrackRunBox.a();
         if((this.getFlags() & 256) == 256) {
            var5.a = e.a(var1);
         }

         if((this.getFlags() & 512) == 512) {
            var5.b = e.a(var1);
         }

         if((this.getFlags() & 1024) == 1024) {
            var5.c = new a(var1);
         }

         if((this.getFlags() & 2048) == 2048) {
            var5.d = (long)var1.getInt();
         }

         this.entries.add(var5);
      }

   }

   protected void getContent(ByteBuffer var1) {
      this.writeVersionAndFlags(var1);
      g.b(var1, (long)this.entries.size());
      int var2 = this.getFlags();
      if((var2 & 1) == 1) {
         g.b(var1, (long)this.dataOffset);
      }

      if((var2 & 4) == 4) {
         this.firstSampleFlags.a(var1);
      }

      Iterator var3 = this.entries.iterator();

      while(var3.hasNext()) {
         TrackRunBox.a var4 = (TrackRunBox.a)var3.next();
         if((var2 & 256) == 256) {
            g.b(var1, var4.a);
         }

         if((var2 & 512) == 512) {
            g.b(var1, var4.b);
         }

         if((var2 & 1024) == 1024) {
            var4.c.a(var1);
         }

         if((var2 & 2048) == 2048) {
            if(this.getVersion() == 0) {
               g.b(var1, var4.d);
            } else {
               var1.putInt((int)var4.d);
            }
         }
      }

   }

   protected long getContentSize() {
      long var2 = 8L;
      int var1 = this.getFlags();
      if((var1 & 1) == 1) {
         var2 = 8L + 4L;
      }

      long var6;
      if((var1 & 4) == 4) {
         var6 = var2 + 4L;
      } else {
         var6 = var2;
      }

      long var4 = 0L;
      if((var1 & 256) == 256) {
         var4 = 0L + 4L;
      }

      var2 = var4;
      if((var1 & 512) == 512) {
         var2 = var4 + 4L;
      }

      var4 = var2;
      if((var1 & 1024) == 1024) {
         var4 = var2 + 4L;
      }

      var2 = var4;
      if((var1 & 2048) == 2048) {
         var2 = var4 + 4L;
      }

      return var2 * (long)this.entries.size() + var6;
   }

   public int getDataOffset() {
      org.mp4parser.aspectj.lang.a var1 = b.a(ajc$tjp_15, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.dataOffset;
   }

   public List getEntries() {
      org.mp4parser.aspectj.lang.a var1 = b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.entries;
   }

   public a getFirstSampleFlags() {
      org.mp4parser.aspectj.lang.a var1 = b.a(ajc$tjp_16, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.firstSampleFlags;
   }

   public long[] getSampleCompositionTimeOffsets() {
      org.mp4parser.aspectj.lang.a var2 = b.a(ajc$tjp_2, this, this);
      com.googlecode.mp4parser.e.a().a(var2);
      long[] var3;
      if(this.isSampleCompositionTimeOffsetPresent()) {
         var3 = new long[this.entries.size()];

         for(int var1 = 0; var1 < var3.length; ++var1) {
            var3[var1] = ((TrackRunBox.a)this.entries.get(var1)).d();
         }
      } else {
         var3 = null;
      }

      return var3;
   }

   public long getSampleCount() {
      org.mp4parser.aspectj.lang.a var1 = b.a(ajc$tjp_3, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return (long)this.entries.size();
   }

   public boolean isDataOffsetPresent() {
      boolean var1 = true;
      org.mp4parser.aspectj.lang.a var2 = b.a(ajc$tjp_4, this, this);
      com.googlecode.mp4parser.e.a().a(var2);
      if((this.getFlags() & 1) != 1) {
         var1 = false;
      }

      return var1;
   }

   public boolean isFirstSampleFlagsPresent() {
      org.mp4parser.aspectj.lang.a var2 = b.a(ajc$tjp_5, this, this);
      com.googlecode.mp4parser.e.a().a(var2);
      boolean var1;
      if((this.getFlags() & 4) == 4) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public boolean isSampleCompositionTimeOffsetPresent() {
      org.mp4parser.aspectj.lang.a var2 = b.a(ajc$tjp_9, this, this);
      com.googlecode.mp4parser.e.a().a(var2);
      boolean var1;
      if((this.getFlags() & 2048) == 2048) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public boolean isSampleDurationPresent() {
      org.mp4parser.aspectj.lang.a var2 = b.a(ajc$tjp_7, this, this);
      com.googlecode.mp4parser.e.a().a(var2);
      boolean var1;
      if((this.getFlags() & 256) == 256) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public boolean isSampleFlagsPresent() {
      org.mp4parser.aspectj.lang.a var2 = b.a(ajc$tjp_8, this, this);
      com.googlecode.mp4parser.e.a().a(var2);
      boolean var1;
      if((this.getFlags() & 1024) == 1024) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public boolean isSampleSizePresent() {
      org.mp4parser.aspectj.lang.a var2 = b.a(ajc$tjp_6, this, this);
      com.googlecode.mp4parser.e.a().a(var2);
      boolean var1;
      if((this.getFlags() & 512) == 512) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public void setDataOffset(int var1) {
      org.mp4parser.aspectj.lang.a var2 = b.a(ajc$tjp_1, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      if(var1 == -1) {
         this.setFlags(this.getFlags() & 16777214);
      } else {
         this.setFlags(this.getFlags() | 1);
      }

      this.dataOffset = var1;
   }

   public void setDataOffsetPresent(boolean var1) {
      org.mp4parser.aspectj.lang.a var2 = b.a(ajc$tjp_10, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      if(var1) {
         this.setFlags(this.getFlags() | 1);
      } else {
         this.setFlags(this.getFlags() & 16777214);
      }

   }

   public void setEntries(List var1) {
      org.mp4parser.aspectj.lang.a var2 = b.a(ajc$tjp_19, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.entries = var1;
   }

   public void setFirstSampleFlags(a var1) {
      org.mp4parser.aspectj.lang.a var2 = b.a(ajc$tjp_17, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      if(var1 == null) {
         this.setFlags(this.getFlags() & 16777211);
      } else {
         this.setFlags(this.getFlags() | 4);
      }

      this.firstSampleFlags = var1;
   }

   public void setSampleCompositionTimeOffsetPresent(boolean var1) {
      org.mp4parser.aspectj.lang.a var2 = b.a(ajc$tjp_14, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      if(var1) {
         this.setFlags(this.getFlags() | 2048);
      } else {
         this.setFlags(this.getFlags() & 16775167);
      }

   }

   public void setSampleDurationPresent(boolean var1) {
      org.mp4parser.aspectj.lang.a var2 = b.a(ajc$tjp_12, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      if(var1) {
         this.setFlags(this.getFlags() | 256);
      } else {
         this.setFlags(this.getFlags() & 16776959);
      }

   }

   public void setSampleFlagsPresent(boolean var1) {
      org.mp4parser.aspectj.lang.a var2 = b.a(ajc$tjp_13, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      if(var1) {
         this.setFlags(this.getFlags() | 1024);
      } else {
         this.setFlags(this.getFlags() & 16776191);
      }

   }

   public void setSampleSizePresent(boolean var1) {
      org.mp4parser.aspectj.lang.a var2 = b.a(ajc$tjp_11, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      if(var1) {
         this.setFlags(this.getFlags() | 512);
      } else {
         this.setFlags(this.getFlags() & 16776703);
      }

   }

   public String toString() {
      org.mp4parser.aspectj.lang.a var1 = b.a(ajc$tjp_18, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      StringBuilder var2 = new StringBuilder();
      var2.append("TrackRunBox");
      var2.append("{sampleCount=").append(this.entries.size());
      var2.append(", dataOffset=").append(this.dataOffset);
      var2.append(", dataOffsetPresent=").append(this.isDataOffsetPresent());
      var2.append(", sampleSizePresent=").append(this.isSampleSizePresent());
      var2.append(", sampleDurationPresent=").append(this.isSampleDurationPresent());
      var2.append(", sampleFlagsPresentPresent=").append(this.isSampleFlagsPresent());
      var2.append(", sampleCompositionTimeOffsetPresent=").append(this.isSampleCompositionTimeOffsetPresent());
      var2.append(", firstSampleFlags=").append(this.firstSampleFlags);
      var2.append('}');
      return var2.toString();
   }

   public static class a {
      private long a;
      private long b;
      private a c;
      private long d;

      public long a() {
         return this.a;
      }

      public long b() {
         return this.b;
      }

      public a c() {
         return this.c;
      }

      public long d() {
         return this.d;
      }

      public String toString() {
         return "Entry{duration=" + this.a + ", size=" + this.b + ", dlags=" + this.c + ", compTimeOffset=" + this.d + '}';
      }
   }
}
