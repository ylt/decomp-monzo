package com.coremedia.iso.boxes.fragment;

import com.coremedia.iso.e;
import com.coremedia.iso.g;
import com.googlecode.mp4parser.AbstractFullBox;
import java.nio.ByteBuffer;
import org.mp4parser.aspectj.a.b.b;

public class TrackFragmentBaseMediaDecodeTimeBox extends AbstractFullBox {
   public static final String TYPE = "tfdt";
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_0;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_1;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_2;
   private long baseMediaDecodeTime;

   static {
      ajc$preClinit();
   }

   public TrackFragmentBaseMediaDecodeTimeBox() {
      super("tfdt");
   }

   private static void ajc$preClinit() {
      b var0 = new b("TrackFragmentBaseMediaDecodeTimeBox.java", TrackFragmentBaseMediaDecodeTimeBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getBaseMediaDecodeTime", "com.coremedia.iso.boxes.fragment.TrackFragmentBaseMediaDecodeTimeBox", "", "", "", "long"), 65);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "setBaseMediaDecodeTime", "com.coremedia.iso.boxes.fragment.TrackFragmentBaseMediaDecodeTimeBox", "long", "baseMediaDecodeTime", "", "void"), 69);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "toString", "com.coremedia.iso.boxes.fragment.TrackFragmentBaseMediaDecodeTimeBox", "", "", "", "java.lang.String"), 74);
   }

   public void _parseDetails(ByteBuffer var1) {
      this.parseVersionAndFlags(var1);
      if(this.getVersion() == 1) {
         this.baseMediaDecodeTime = e.f(var1);
      } else {
         this.baseMediaDecodeTime = e.a(var1);
      }

   }

   public long getBaseMediaDecodeTime() {
      org.mp4parser.aspectj.lang.a var1 = b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.baseMediaDecodeTime;
   }

   protected void getContent(ByteBuffer var1) {
      this.writeVersionAndFlags(var1);
      if(this.getVersion() == 1) {
         g.a(var1, this.baseMediaDecodeTime);
      } else {
         g.b(var1, this.baseMediaDecodeTime);
      }

   }

   protected long getContentSize() {
      byte var1;
      if(this.getVersion() == 0) {
         var1 = 8;
      } else {
         var1 = 12;
      }

      return (long)var1;
   }

   public void setBaseMediaDecodeTime(long var1) {
      org.mp4parser.aspectj.lang.a var3 = b.a(ajc$tjp_1, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var3);
      this.baseMediaDecodeTime = var1;
   }

   public String toString() {
      org.mp4parser.aspectj.lang.a var1 = b.a(ajc$tjp_2, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return "TrackFragmentBaseMediaDecodeTimeBox{baseMediaDecodeTime=" + this.baseMediaDecodeTime + '}';
   }
}
