package com.coremedia.iso.boxes.fragment;

import com.coremedia.iso.e;
import com.coremedia.iso.g;
import com.googlecode.mp4parser.AbstractFullBox;
import java.nio.ByteBuffer;
import org.mp4parser.aspectj.a.b.b;

public class MovieExtendsHeaderBox extends AbstractFullBox {
   public static final String TYPE = "mehd";
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_0;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_1;
   private long fragmentDuration;

   static {
      ajc$preClinit();
   }

   public MovieExtendsHeaderBox() {
      super("mehd");
   }

   private static void ajc$preClinit() {
      b var0 = new b("MovieExtendsHeaderBox.java", MovieExtendsHeaderBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getFragmentDuration", "com.coremedia.iso.boxes.fragment.MovieExtendsHeaderBox", "", "", "", "long"), 65);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "setFragmentDuration", "com.coremedia.iso.boxes.fragment.MovieExtendsHeaderBox", "long", "fragmentDuration", "", "void"), 69);
   }

   public void _parseDetails(ByteBuffer var1) {
      this.parseVersionAndFlags(var1);
      long var2;
      if(this.getVersion() == 1) {
         var2 = e.f(var1);
      } else {
         var2 = e.a(var1);
      }

      this.fragmentDuration = var2;
   }

   protected void getContent(ByteBuffer var1) {
      this.writeVersionAndFlags(var1);
      if(this.getVersion() == 1) {
         g.a(var1, this.fragmentDuration);
      } else {
         g.b(var1, this.fragmentDuration);
      }

   }

   protected long getContentSize() {
      byte var1;
      if(this.getVersion() == 1) {
         var1 = 12;
      } else {
         var1 = 8;
      }

      return (long)var1;
   }

   public long getFragmentDuration() {
      org.mp4parser.aspectj.lang.a var1 = b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.fragmentDuration;
   }

   public void setFragmentDuration(long var1) {
      org.mp4parser.aspectj.lang.a var3 = b.a(ajc$tjp_1, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var3);
      this.fragmentDuration = var1;
   }
}
