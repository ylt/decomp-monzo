package com.coremedia.iso.boxes.fragment;

import com.googlecode.mp4parser.AbstractContainerBox;
import java.util.Iterator;

public class TrackFragmentBox extends AbstractContainerBox {
   public static final String TYPE = "traf";

   public TrackFragmentBox() {
      super("traf");
   }

   public TrackFragmentHeaderBox getTrackFragmentHeaderBox() {
      Iterator var1 = this.getBoxes().iterator();

      TrackFragmentHeaderBox var3;
      while(true) {
         if(!var1.hasNext()) {
            var3 = null;
            break;
         }

         com.coremedia.iso.boxes.a var2 = (com.coremedia.iso.boxes.a)var1.next();
         if(var2 instanceof TrackFragmentHeaderBox) {
            var3 = (TrackFragmentHeaderBox)var2;
            break;
         }
      }

      return var3;
   }
}
