package com.coremedia.iso.boxes.fragment;

import com.coremedia.iso.e;
import com.coremedia.iso.g;
import java.nio.ByteBuffer;

public class a {
   private byte a;
   private byte b;
   private byte c;
   private byte d;
   private byte e;
   private byte f;
   private boolean g;
   private int h;

   public a() {
   }

   public a(ByteBuffer var1) {
      long var3 = e.a(var1);
      this.a = (byte)((int)((-268435456L & var3) >> 28));
      this.b = (byte)((int)((201326592L & var3) >> 26));
      this.c = (byte)((int)((50331648L & var3) >> 24));
      this.d = (byte)((int)((12582912L & var3) >> 22));
      this.e = (byte)((int)((3145728L & var3) >> 20));
      this.f = (byte)((int)((917504L & var3) >> 17));
      boolean var2;
      if((65536L & var3) >> 16 > 0L) {
         var2 = true;
      } else {
         var2 = false;
      }

      this.g = var2;
      this.h = (int)(65535L & var3);
   }

   public void a(ByteBuffer var1) {
      long var3 = (long)(this.a << 28);
      long var5 = (long)(this.b << 26);
      long var9 = (long)(this.c << 24);
      long var13 = (long)(this.d << 22);
      long var11 = (long)(this.e << 20);
      long var7 = (long)(this.f << 17);
      byte var2;
      if(this.g) {
         var2 = 1;
      } else {
         var2 = 0;
      }

      g.b(var1, (long)(var2 << 16) | var7 | 0L | var3 | var5 | var9 | var13 | var11 | (long)this.h);
   }

   public boolean a() {
      return this.g;
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(this != var1) {
         if(var1 != null && this.getClass() == var1.getClass()) {
            a var3 = (a)var1;
            if(this.b != var3.b) {
               var2 = false;
            } else if(this.a != var3.a) {
               var2 = false;
            } else if(this.h != var3.h) {
               var2 = false;
            } else if(this.c != var3.c) {
               var2 = false;
            } else if(this.e != var3.e) {
               var2 = false;
            } else if(this.d != var3.d) {
               var2 = false;
            } else if(this.g != var3.g) {
               var2 = false;
            } else if(this.f != var3.f) {
               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public int hashCode() {
      byte var6 = this.a;
      byte var3 = this.b;
      byte var4 = this.c;
      byte var7 = this.d;
      byte var5 = this.e;
      byte var2 = this.f;
      byte var1;
      if(this.g) {
         var1 = 1;
      } else {
         var1 = 0;
      }

      return (var1 + (((((var6 * 31 + var3) * 31 + var4) * 31 + var7) * 31 + var5) * 31 + var2) * 31) * 31 + this.h;
   }

   public String toString() {
      return "SampleFlags{reserved=" + this.a + ", isLeading=" + this.b + ", depOn=" + this.c + ", isDepOn=" + this.d + ", hasRedundancy=" + this.e + ", padValue=" + this.f + ", isDiffSample=" + this.g + ", degradPrio=" + this.h + '}';
   }
}
