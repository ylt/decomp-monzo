package com.coremedia.iso.boxes.fragment;

import com.coremedia.iso.e;
import com.coremedia.iso.g;
import com.googlecode.mp4parser.AbstractFullBox;
import java.nio.ByteBuffer;
import org.mp4parser.aspectj.a.b.b;

public class TrackExtendsBox extends AbstractFullBox {
   public static final String TYPE = "trex";
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_0;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_1;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_10;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_2;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_3;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_4;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_5;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_6;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_7;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_8;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_9;
   private long defaultSampleDescriptionIndex;
   private long defaultSampleDuration;
   private a defaultSampleFlags;
   private long defaultSampleSize;
   private long trackId;

   static {
      ajc$preClinit();
   }

   public TrackExtendsBox() {
      super("trex");
   }

   private static void ajc$preClinit() {
      b var0 = new b("TrackExtendsBox.java", TrackExtendsBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getTrackId", "com.coremedia.iso.boxes.fragment.TrackExtendsBox", "", "", "", "long"), 72);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "getDefaultSampleDescriptionIndex", "com.coremedia.iso.boxes.fragment.TrackExtendsBox", "", "", "", "long"), 76);
      ajc$tjp_10 = var0.a("method-execution", var0.a("1", "setDefaultSampleFlags", "com.coremedia.iso.boxes.fragment.TrackExtendsBox", "com.coremedia.iso.boxes.fragment.SampleFlags", "defaultSampleFlags", "", "void"), 112);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "getDefaultSampleDuration", "com.coremedia.iso.boxes.fragment.TrackExtendsBox", "", "", "", "long"), 80);
      ajc$tjp_3 = var0.a("method-execution", var0.a("1", "getDefaultSampleSize", "com.coremedia.iso.boxes.fragment.TrackExtendsBox", "", "", "", "long"), 84);
      ajc$tjp_4 = var0.a("method-execution", var0.a("1", "getDefaultSampleFlags", "com.coremedia.iso.boxes.fragment.TrackExtendsBox", "", "", "", "com.coremedia.iso.boxes.fragment.SampleFlags"), 88);
      ajc$tjp_5 = var0.a("method-execution", var0.a("1", "getDefaultSampleFlagsStr", "com.coremedia.iso.boxes.fragment.TrackExtendsBox", "", "", "", "java.lang.String"), 92);
      ajc$tjp_6 = var0.a("method-execution", var0.a("1", "setTrackId", "com.coremedia.iso.boxes.fragment.TrackExtendsBox", "long", "trackId", "", "void"), 96);
      ajc$tjp_7 = var0.a("method-execution", var0.a("1", "setDefaultSampleDescriptionIndex", "com.coremedia.iso.boxes.fragment.TrackExtendsBox", "long", "defaultSampleDescriptionIndex", "", "void"), 100);
      ajc$tjp_8 = var0.a("method-execution", var0.a("1", "setDefaultSampleDuration", "com.coremedia.iso.boxes.fragment.TrackExtendsBox", "long", "defaultSampleDuration", "", "void"), 104);
      ajc$tjp_9 = var0.a("method-execution", var0.a("1", "setDefaultSampleSize", "com.coremedia.iso.boxes.fragment.TrackExtendsBox", "long", "defaultSampleSize", "", "void"), 108);
   }

   public void _parseDetails(ByteBuffer var1) {
      this.parseVersionAndFlags(var1);
      this.trackId = e.a(var1);
      this.defaultSampleDescriptionIndex = e.a(var1);
      this.defaultSampleDuration = e.a(var1);
      this.defaultSampleSize = e.a(var1);
      this.defaultSampleFlags = new a(var1);
   }

   protected void getContent(ByteBuffer var1) {
      this.writeVersionAndFlags(var1);
      g.b(var1, this.trackId);
      g.b(var1, this.defaultSampleDescriptionIndex);
      g.b(var1, this.defaultSampleDuration);
      g.b(var1, this.defaultSampleSize);
      this.defaultSampleFlags.a(var1);
   }

   protected long getContentSize() {
      return 24L;
   }

   public long getDefaultSampleDescriptionIndex() {
      org.mp4parser.aspectj.lang.a var1 = b.a(ajc$tjp_1, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.defaultSampleDescriptionIndex;
   }

   public long getDefaultSampleDuration() {
      org.mp4parser.aspectj.lang.a var1 = b.a(ajc$tjp_2, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.defaultSampleDuration;
   }

   public a getDefaultSampleFlags() {
      org.mp4parser.aspectj.lang.a var1 = b.a(ajc$tjp_4, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.defaultSampleFlags;
   }

   public String getDefaultSampleFlagsStr() {
      org.mp4parser.aspectj.lang.a var1 = b.a(ajc$tjp_5, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.defaultSampleFlags.toString();
   }

   public long getDefaultSampleSize() {
      org.mp4parser.aspectj.lang.a var1 = b.a(ajc$tjp_3, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.defaultSampleSize;
   }

   public long getTrackId() {
      org.mp4parser.aspectj.lang.a var1 = b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.trackId;
   }

   public void setDefaultSampleDescriptionIndex(long var1) {
      org.mp4parser.aspectj.lang.a var3 = b.a(ajc$tjp_7, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var3);
      this.defaultSampleDescriptionIndex = var1;
   }

   public void setDefaultSampleDuration(long var1) {
      org.mp4parser.aspectj.lang.a var3 = b.a(ajc$tjp_8, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var3);
      this.defaultSampleDuration = var1;
   }

   public void setDefaultSampleFlags(a var1) {
      org.mp4parser.aspectj.lang.a var2 = b.a(ajc$tjp_10, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.defaultSampleFlags = var1;
   }

   public void setDefaultSampleSize(long var1) {
      org.mp4parser.aspectj.lang.a var3 = b.a(ajc$tjp_9, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var3);
      this.defaultSampleSize = var1;
   }

   public void setTrackId(long var1) {
      org.mp4parser.aspectj.lang.a var3 = b.a(ajc$tjp_6, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var3);
      this.trackId = var1;
   }
}
