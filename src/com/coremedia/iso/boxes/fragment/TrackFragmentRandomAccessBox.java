package com.coremedia.iso.boxes.fragment;

import com.coremedia.iso.e;
import com.coremedia.iso.f;
import com.coremedia.iso.g;
import com.coremedia.iso.h;
import com.googlecode.mp4parser.AbstractFullBox;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.mp4parser.aspectj.a.b.b;

public class TrackFragmentRandomAccessBox extends AbstractFullBox {
   public static final String TYPE = "tfra";
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_0;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_1;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_10;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_11;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_12;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_2;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_3;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_4;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_5;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_6;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_7;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_8;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_9;
   private List entries = Collections.emptyList();
   private int lengthSizeOfSampleNum = 2;
   private int lengthSizeOfTrafNum = 2;
   private int lengthSizeOfTrunNum = 2;
   private int reserved;
   private long trackId;

   static {
      ajc$preClinit();
   }

   public TrackFragmentRandomAccessBox() {
      super("tfra");
   }

   private static void ajc$preClinit() {
      b var0 = new b("TrackFragmentRandomAccessBox.java", TrackFragmentRandomAccessBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "setTrackId", "com.coremedia.iso.boxes.fragment.TrackFragmentRandomAccessBox", "long", "trackId", "", "void"), 145);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "setLengthSizeOfTrafNum", "com.coremedia.iso.boxes.fragment.TrackFragmentRandomAccessBox", "int", "lengthSizeOfTrafNum", "", "void"), 149);
      ajc$tjp_10 = var0.a("method-execution", var0.a("1", "getEntries", "com.coremedia.iso.boxes.fragment.TrackFragmentRandomAccessBox", "", "", "", "java.util.List"), 185);
      ajc$tjp_11 = var0.a("method-execution", var0.a("1", "setEntries", "com.coremedia.iso.boxes.fragment.TrackFragmentRandomAccessBox", "java.util.List", "entries", "", "void"), 189);
      ajc$tjp_12 = var0.a("method-execution", var0.a("1", "toString", "com.coremedia.iso.boxes.fragment.TrackFragmentRandomAccessBox", "", "", "", "java.lang.String"), 290);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "setLengthSizeOfTrunNum", "com.coremedia.iso.boxes.fragment.TrackFragmentRandomAccessBox", "int", "lengthSizeOfTrunNum", "", "void"), 153);
      ajc$tjp_3 = var0.a("method-execution", var0.a("1", "setLengthSizeOfSampleNum", "com.coremedia.iso.boxes.fragment.TrackFragmentRandomAccessBox", "int", "lengthSizeOfSampleNum", "", "void"), 157);
      ajc$tjp_4 = var0.a("method-execution", var0.a("1", "getTrackId", "com.coremedia.iso.boxes.fragment.TrackFragmentRandomAccessBox", "", "", "", "long"), 161);
      ajc$tjp_5 = var0.a("method-execution", var0.a("1", "getReserved", "com.coremedia.iso.boxes.fragment.TrackFragmentRandomAccessBox", "", "", "", "int"), 165);
      ajc$tjp_6 = var0.a("method-execution", var0.a("1", "getLengthSizeOfTrafNum", "com.coremedia.iso.boxes.fragment.TrackFragmentRandomAccessBox", "", "", "", "int"), 169);
      ajc$tjp_7 = var0.a("method-execution", var0.a("1", "getLengthSizeOfTrunNum", "com.coremedia.iso.boxes.fragment.TrackFragmentRandomAccessBox", "", "", "", "int"), 173);
      ajc$tjp_8 = var0.a("method-execution", var0.a("1", "getLengthSizeOfSampleNum", "com.coremedia.iso.boxes.fragment.TrackFragmentRandomAccessBox", "", "", "", "int"), 177);
      ajc$tjp_9 = var0.a("method-execution", var0.a("1", "getNumberOfEntries", "com.coremedia.iso.boxes.fragment.TrackFragmentRandomAccessBox", "", "", "", "long"), 181);
   }

   public void _parseDetails(ByteBuffer var1) {
      this.parseVersionAndFlags(var1);
      this.trackId = e.a(var1);
      long var3 = e.a(var1);
      this.reserved = (int)(var3 >> 6);
      this.lengthSizeOfTrafNum = ((int)(63L & var3) >> 4) + 1;
      this.lengthSizeOfTrunNum = ((int)(12L & var3) >> 2) + 1;
      this.lengthSizeOfSampleNum = (int)(var3 & 3L) + 1;
      var3 = e.a(var1);
      this.entries = new ArrayList();

      for(int var2 = 0; (long)var2 < var3; ++var2) {
         TrackFragmentRandomAccessBox.a var5 = new TrackFragmentRandomAccessBox.a();
         if(this.getVersion() == 1) {
            var5.a = e.f(var1);
            var5.b = e.f(var1);
         } else {
            var5.a = e.a(var1);
            var5.b = e.a(var1);
         }

         var5.c = f.a(var1, this.lengthSizeOfTrafNum);
         var5.d = f.a(var1, this.lengthSizeOfTrunNum);
         var5.e = f.a(var1, this.lengthSizeOfSampleNum);
         this.entries.add(var5);
      }

   }

   protected void getContent(ByteBuffer var1) {
      this.writeVersionAndFlags(var1);
      g.b(var1, this.trackId);
      g.b(var1, (long)(this.reserved << 6) | (long)((this.lengthSizeOfTrafNum - 1 & 3) << 4) | (long)((this.lengthSizeOfTrunNum - 1 & 3) << 2) | (long)(this.lengthSizeOfSampleNum - 1 & 3));
      g.b(var1, (long)this.entries.size());
      Iterator var2 = this.entries.iterator();

      while(var2.hasNext()) {
         TrackFragmentRandomAccessBox.a var3 = (TrackFragmentRandomAccessBox.a)var2.next();
         if(this.getVersion() == 1) {
            g.a(var1, var3.a);
            g.a(var1, var3.b);
         } else {
            g.b(var1, var3.a);
            g.b(var1, var3.b);
         }

         h.a(var3.c, var1, this.lengthSizeOfTrafNum);
         h.a(var3.d, var1, this.lengthSizeOfTrunNum);
         h.a(var3.e, var1, this.lengthSizeOfSampleNum);
      }

   }

   protected long getContentSize() {
      long var1 = 4L + 12L;
      if(this.getVersion() == 1) {
         var1 += (long)(this.entries.size() * 16);
      } else {
         var1 += (long)(this.entries.size() * 8);
      }

      return var1 + (long)(this.lengthSizeOfTrafNum * this.entries.size()) + (long)(this.lengthSizeOfTrunNum * this.entries.size()) + (long)(this.lengthSizeOfSampleNum * this.entries.size());
   }

   public List getEntries() {
      org.mp4parser.aspectj.lang.a var1 = b.a(ajc$tjp_10, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return Collections.unmodifiableList(this.entries);
   }

   public int getLengthSizeOfSampleNum() {
      org.mp4parser.aspectj.lang.a var1 = b.a(ajc$tjp_8, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.lengthSizeOfSampleNum;
   }

   public int getLengthSizeOfTrafNum() {
      org.mp4parser.aspectj.lang.a var1 = b.a(ajc$tjp_6, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.lengthSizeOfTrafNum;
   }

   public int getLengthSizeOfTrunNum() {
      org.mp4parser.aspectj.lang.a var1 = b.a(ajc$tjp_7, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.lengthSizeOfTrunNum;
   }

   public long getNumberOfEntries() {
      org.mp4parser.aspectj.lang.a var1 = b.a(ajc$tjp_9, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return (long)this.entries.size();
   }

   public int getReserved() {
      org.mp4parser.aspectj.lang.a var1 = b.a(ajc$tjp_5, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.reserved;
   }

   public long getTrackId() {
      org.mp4parser.aspectj.lang.a var1 = b.a(ajc$tjp_4, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.trackId;
   }

   public void setEntries(List var1) {
      org.mp4parser.aspectj.lang.a var2 = b.a(ajc$tjp_11, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.entries = var1;
   }

   public void setLengthSizeOfSampleNum(int var1) {
      org.mp4parser.aspectj.lang.a var2 = b.a(ajc$tjp_3, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.lengthSizeOfSampleNum = var1;
   }

   public void setLengthSizeOfTrafNum(int var1) {
      org.mp4parser.aspectj.lang.a var2 = b.a(ajc$tjp_1, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.lengthSizeOfTrafNum = var1;
   }

   public void setLengthSizeOfTrunNum(int var1) {
      org.mp4parser.aspectj.lang.a var2 = b.a(ajc$tjp_2, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.lengthSizeOfTrunNum = var1;
   }

   public void setTrackId(long var1) {
      org.mp4parser.aspectj.lang.a var3 = b.a(ajc$tjp_0, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var3);
      this.trackId = var1;
   }

   public String toString() {
      org.mp4parser.aspectj.lang.a var1 = b.a(ajc$tjp_12, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return "TrackFragmentRandomAccessBox{trackId=" + this.trackId + ", entries=" + this.entries + '}';
   }

   public static class a {
      private long a;
      private long b;
      private long c;
      private long d;
      private long e;

      public boolean equals(Object var1) {
         boolean var2 = true;
         if(this != var1) {
            if(var1 != null && this.getClass() == var1.getClass()) {
               TrackFragmentRandomAccessBox.a var3 = (TrackFragmentRandomAccessBox.a)var1;
               if(this.b != var3.b) {
                  var2 = false;
               } else if(this.e != var3.e) {
                  var2 = false;
               } else if(this.a != var3.a) {
                  var2 = false;
               } else if(this.c != var3.c) {
                  var2 = false;
               } else if(this.d != var3.d) {
                  var2 = false;
               }
            } else {
               var2 = false;
            }
         }

         return var2;
      }

      public int hashCode() {
         return ((((int)(this.a ^ this.a >>> 32) * 31 + (int)(this.b ^ this.b >>> 32)) * 31 + (int)(this.c ^ this.c >>> 32)) * 31 + (int)(this.d ^ this.d >>> 32)) * 31 + (int)(this.e ^ this.e >>> 32);
      }

      public String toString() {
         return "Entry{time=" + this.a + ", moofOffset=" + this.b + ", trafNumber=" + this.c + ", trunNumber=" + this.d + ", sampleNumber=" + this.e + '}';
      }
   }
}
