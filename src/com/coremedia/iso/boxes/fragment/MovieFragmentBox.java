package com.coremedia.iso.boxes.fragment;

import com.coremedia.iso.boxes.SampleDependencyTypeBox;
import com.googlecode.mp4parser.AbstractContainerBox;
import com.googlecode.mp4parser.b;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MovieFragmentBox extends AbstractContainerBox {
   public static final String TYPE = "moof";

   public MovieFragmentBox() {
      super("moof");
   }

   public b getFileChannel() {
      return this.dataSource;
   }

   public List getSyncSamples(SampleDependencyTypeBox var1) {
      ArrayList var4 = new ArrayList();
      Iterator var5 = var1.getEntries().iterator();

      for(long var2 = 1L; var5.hasNext(); ++var2) {
         if(((SampleDependencyTypeBox.a)var5.next()).b() == 2) {
            var4.add(Long.valueOf(var2));
         }
      }

      return var4;
   }

   public int getTrackCount() {
      return this.getBoxes(TrackFragmentBox.class, false).size();
   }

   public List getTrackFragmentHeaderBoxes() {
      return this.getBoxes(TrackFragmentHeaderBox.class, true);
   }

   public long[] getTrackNumbers() {
      List var2 = this.getBoxes(TrackFragmentBox.class, false);
      long[] var3 = new long[var2.size()];

      for(int var1 = 0; var1 < var2.size(); ++var1) {
         var3[var1] = ((TrackFragmentBox)var2.get(var1)).getTrackFragmentHeaderBox().getTrackId();
      }

      return var3;
   }

   public List getTrackRunBoxes() {
      return this.getBoxes(TrackRunBox.class, true);
   }
}
