package com.coremedia.iso.boxes;

import com.coremedia.iso.e;
import com.coremedia.iso.g;
import com.googlecode.mp4parser.AbstractFullBox;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class SubSampleInformationBox extends AbstractFullBox {
   public static final String TYPE = "subs";
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_0;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_1;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_2;
   private List entries = new ArrayList();

   static {
      ajc$preClinit();
   }

   public SubSampleInformationBox() {
      super("subs");
   }

   private static void ajc$preClinit() {
      org.mp4parser.aspectj.a.b.b var0 = new org.mp4parser.aspectj.a.b.b("SubSampleInformationBox.java", SubSampleInformationBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getEntries", "com.coremedia.iso.boxes.SubSampleInformationBox", "", "", "", "java.util.List"), 50);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "setEntries", "com.coremedia.iso.boxes.SubSampleInformationBox", "java.util.List", "entries", "", "void"), 54);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "toString", "com.coremedia.iso.boxes.SubSampleInformationBox", "", "", "", "java.lang.String"), 124);
   }

   public void _parseDetails(ByteBuffer var1) {
      this.parseVersionAndFlags(var1);
      long var7 = e.a(var1);

      for(int var2 = 0; (long)var2 < var7; ++var2) {
         SubSampleInformationBox.a var10 = new SubSampleInformationBox.a();
         var10.a(e.a(var1));
         int var4 = e.c(var1);

         for(int var3 = 0; var3 < var4; ++var3) {
            SubSampleInformationBox.a var9 = new SubSampleInformationBox.a();
            long var5;
            if(this.getVersion() == 1) {
               var5 = e.a(var1);
            } else {
               var5 = (long)e.c(var1);
            }

            var9.a(var5);
            var9.a(e.d(var1));
            var9.b(e.d(var1));
            var9.b(e.a(var1));
            var10.c().add(var9);
         }

         this.entries.add(var10);
      }

   }

   protected void getContent(ByteBuffer var1) {
      this.writeVersionAndFlags(var1);
      g.b(var1, (long)this.entries.size());
      Iterator var2 = this.entries.iterator();

      while(var2.hasNext()) {
         SubSampleInformationBox.a var3 = (SubSampleInformationBox.a)var2.next();
         g.b(var1, var3.a());
         g.b(var1, var3.b());
         Iterator var4 = var3.c().iterator();

         while(var4.hasNext()) {
            SubSampleInformationBox.a var5 = (SubSampleInformationBox.a)var4.next();
            if(this.getVersion() == 1) {
               g.b(var1, var5.a());
            } else {
               g.b(var1, com.googlecode.mp4parser.c.b.a(var5.a()));
            }

            g.c(var1, var5.b());
            g.c(var1, var5.c());
            g.b(var1, var5.d());
         }
      }

   }

   protected long getContentSize() {
      Iterator var7 = this.entries.iterator();
      long var4 = 8L;

      while(var7.hasNext()) {
         SubSampleInformationBox.a var6 = (SubSampleInformationBox.a)var7.next();
         long var2 = var4 + 4L + 2L;
         int var1 = 0;

         while(true) {
            var4 = var2;
            if(var1 >= var6.c().size()) {
               break;
            }

            if(this.getVersion() == 1) {
               var2 += 4L;
            } else {
               var2 += 2L;
            }

            var2 = var2 + 2L + 4L;
            ++var1;
         }
      }

      return var4;
   }

   public List getEntries() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.entries;
   }

   public void setEntries(List var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_1, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.entries = var1;
   }

   public String toString() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_2, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return "SubSampleInformationBox{entryCount=" + this.entries.size() + ", entries=" + this.entries + '}';
   }

   public static class a {
      private long a;
      private List b = new ArrayList();

      public long a() {
         return this.a;
      }

      public void a(long var1) {
         this.a = var1;
      }

      public int b() {
         return this.b.size();
      }

      public List c() {
         return this.b;
      }

      public String toString() {
         return "SampleEntry{sampleDelta=" + this.a + ", subsampleCount=" + this.b.size() + ", subsampleEntries=" + this.b + '}';
      }
   }

   public static class a {
      private long a;
      private int b;
      private int c;
      private long d;

      public long a() {
         return this.a;
      }

      public void a(int var1) {
         this.b = var1;
      }

      public void a(long var1) {
         this.a = var1;
      }

      public int b() {
         return this.b;
      }

      public void b(int var1) {
         this.c = var1;
      }

      public void b(long var1) {
         this.d = var1;
      }

      public int c() {
         return this.c;
      }

      public long d() {
         return this.d;
      }

      public String toString() {
         return "SubsampleEntry{subsampleSize=" + this.a + ", subsamplePriority=" + this.b + ", discardable=" + this.c + ", reserved=" + this.d + '}';
      }
   }
}
