package com.coremedia.iso.boxes;

import com.coremedia.iso.e;
import com.coremedia.iso.g;
import com.coremedia.iso.j;
import com.googlecode.mp4parser.AbstractFullBox;
import java.nio.ByteBuffer;

public class KeywordsBox extends AbstractFullBox {
   public static final String TYPE = "kywd";
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_0;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_1;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_2;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_3;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_4;
   private String[] keywords;
   private String language;

   static {
      ajc$preClinit();
   }

   public KeywordsBox() {
      super("kywd");
   }

   private static void ajc$preClinit() {
      org.mp4parser.aspectj.a.b.b var0 = new org.mp4parser.aspectj.a.b.b("KeywordsBox.java", KeywordsBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getLanguage", "com.coremedia.iso.boxes.KeywordsBox", "", "", "", "java.lang.String"), 40);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "getKeywords", "com.coremedia.iso.boxes.KeywordsBox", "", "", "", "[Ljava.lang.String;"), 44);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "setLanguage", "com.coremedia.iso.boxes.KeywordsBox", "java.lang.String", "language", "", "void"), 48);
      ajc$tjp_3 = var0.a("method-execution", var0.a("1", "setKeywords", "com.coremedia.iso.boxes.KeywordsBox", "[Ljava.lang.String;", "keywords", "", "void"), 52);
      ajc$tjp_4 = var0.a("method-execution", var0.a("1", "toString", "com.coremedia.iso.boxes.KeywordsBox", "", "", "", "java.lang.String"), 87);
   }

   public void _parseDetails(ByteBuffer var1) {
      this.parseVersionAndFlags(var1);
      this.language = e.j(var1);
      int var3 = e.d(var1);
      this.keywords = new String[var3];

      for(int var2 = 0; var2 < var3; ++var2) {
         e.d(var1);
         this.keywords[var2] = e.e(var1);
      }

   }

   protected void getContent(ByteBuffer var1) {
      this.writeVersionAndFlags(var1);
      g.a(var1, this.language);
      g.c(var1, this.keywords.length);
      String[] var5 = this.keywords;
      int var3 = var5.length;

      for(int var2 = 0; var2 < var3; ++var2) {
         String var4 = var5[var2];
         g.c(var1, j.b(var4) + 1);
         var1.put(j.a(var4));
      }

   }

   protected long getContentSize() {
      long var3 = 7L;
      String[] var5 = this.keywords;
      int var2 = var5.length;

      for(int var1 = 0; var1 < var2; ++var1) {
         var3 += (long)(j.b(var5[var1]) + 1 + 1);
      }

      return var3;
   }

   public String[] getKeywords() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_1, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.keywords;
   }

   public String getLanguage() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.language;
   }

   public void setKeywords(String[] var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_3, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.keywords = var1;
   }

   public void setLanguage(String var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_2, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.language = var1;
   }

   public String toString() {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_4, this, this);
      com.googlecode.mp4parser.e.a().a(var2);
      StringBuffer var3 = new StringBuffer();
      var3.append("KeywordsBox[language=").append(this.getLanguage());

      for(int var1 = 0; var1 < this.keywords.length; ++var1) {
         var3.append(";keyword").append(var1).append("=").append(this.keywords[var1]);
      }

      var3.append("]");
      return var3.toString();
   }
}
