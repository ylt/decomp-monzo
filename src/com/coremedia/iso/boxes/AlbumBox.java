package com.coremedia.iso.boxes;

import com.coremedia.iso.e;
import com.coremedia.iso.g;
import com.coremedia.iso.j;
import com.googlecode.mp4parser.AbstractFullBox;
import java.nio.ByteBuffer;

public class AlbumBox extends AbstractFullBox {
   public static final String TYPE = "albm";
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_0;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_1;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_2;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_3;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_4;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_5;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_6;
   private String albumTitle;
   private String language;
   private int trackNumber;

   static {
      ajc$preClinit();
   }

   public AlbumBox() {
      super("albm");
   }

   private static void ajc$preClinit() {
      org.mp4parser.aspectj.a.b.b var0 = new org.mp4parser.aspectj.a.b.b("AlbumBox.java", AlbumBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getLanguage", "com.coremedia.iso.boxes.AlbumBox", "", "", "", "java.lang.String"), 51);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "getAlbumTitle", "com.coremedia.iso.boxes.AlbumBox", "", "", "", "java.lang.String"), 55);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "getTrackNumber", "com.coremedia.iso.boxes.AlbumBox", "", "", "", "int"), 59);
      ajc$tjp_3 = var0.a("method-execution", var0.a("1", "setLanguage", "com.coremedia.iso.boxes.AlbumBox", "java.lang.String", "language", "", "void"), 63);
      ajc$tjp_4 = var0.a("method-execution", var0.a("1", "setAlbumTitle", "com.coremedia.iso.boxes.AlbumBox", "java.lang.String", "albumTitle", "", "void"), 67);
      ajc$tjp_5 = var0.a("method-execution", var0.a("1", "setTrackNumber", "com.coremedia.iso.boxes.AlbumBox", "int", "trackNumber", "", "void"), 71);
      ajc$tjp_6 = var0.a("method-execution", var0.a("1", "toString", "com.coremedia.iso.boxes.AlbumBox", "", "", "", "java.lang.String"), 103);
   }

   public void _parseDetails(ByteBuffer var1) {
      this.parseVersionAndFlags(var1);
      this.language = e.j(var1);
      this.albumTitle = e.e(var1);
      if(var1.remaining() > 0) {
         this.trackNumber = e.d(var1);
      } else {
         this.trackNumber = -1;
      }

   }

   public String getAlbumTitle() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_1, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.albumTitle;
   }

   protected void getContent(ByteBuffer var1) {
      this.writeVersionAndFlags(var1);
      g.a(var1, this.language);
      var1.put(j.a(this.albumTitle));
      var1.put(0);
      if(this.trackNumber != -1) {
         g.c(var1, this.trackNumber);
      }

   }

   protected long getContentSize() {
      int var2 = j.b(this.albumTitle);
      byte var1;
      if(this.trackNumber == -1) {
         var1 = 0;
      } else {
         var1 = 1;
      }

      return (long)(var1 + var2 + 6 + 1);
   }

   public String getLanguage() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.language;
   }

   public int getTrackNumber() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_2, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.trackNumber;
   }

   public void setAlbumTitle(String var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_4, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.albumTitle = var1;
   }

   public void setLanguage(String var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_3, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.language = var1;
   }

   public void setTrackNumber(int var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_5, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.trackNumber = var1;
   }

   public String toString() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_6, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      StringBuilder var2 = new StringBuilder();
      var2.append("AlbumBox[language=").append(this.getLanguage()).append(";");
      var2.append("albumTitle=").append(this.getAlbumTitle());
      if(this.trackNumber >= 0) {
         var2.append(";trackNumber=").append(this.getTrackNumber());
      }

      var2.append("]");
      return var2.toString();
   }
}
