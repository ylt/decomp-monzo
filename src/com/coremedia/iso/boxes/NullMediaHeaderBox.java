package com.coremedia.iso.boxes;

import java.nio.ByteBuffer;

public class NullMediaHeaderBox extends AbstractMediaHeaderBox {
   public static final String TYPE = "nmhd";

   public NullMediaHeaderBox() {
      super("nmhd");
   }

   public void _parseDetails(ByteBuffer var1) {
      this.parseVersionAndFlags(var1);
   }

   protected void getContent(ByteBuffer var1) {
      this.writeVersionAndFlags(var1);
   }

   protected long getContentSize() {
      return 4L;
   }
}
