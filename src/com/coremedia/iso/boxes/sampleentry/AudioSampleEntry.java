package com.coremedia.iso.boxes.sampleentry;

import com.coremedia.iso.e;
import com.coremedia.iso.g;
import com.coremedia.iso.boxes.a;
import com.googlecode.mp4parser.b;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;

public final class AudioSampleEntry extends AbstractSampleEntry {
   // $FF: synthetic field
   static final boolean $assertionsDisabled;
   public static final String TYPE1 = "samr";
   public static final String TYPE10 = "mlpa";
   public static final String TYPE11 = "dtsl";
   public static final String TYPE12 = "dtsh";
   public static final String TYPE13 = "dtse";
   public static final String TYPE2 = "sawb";
   public static final String TYPE3 = "mp4a";
   public static final String TYPE4 = "drms";
   public static final String TYPE5 = "alac";
   public static final String TYPE7 = "owma";
   public static final String TYPE8 = "ac-3";
   public static final String TYPE9 = "ec-3";
   public static final String TYPE_ENCRYPTED = "enca";
   private long bytesPerFrame;
   private long bytesPerPacket;
   private long bytesPerSample;
   private int channelCount;
   private int compressionId;
   private int packetSize;
   private int reserved1;
   private long reserved2;
   private long sampleRate;
   private int sampleSize;
   private long samplesPerPacket;
   private int soundVersion;
   private byte[] soundVersion2Data;

   static {
      boolean var0;
      if(!AudioSampleEntry.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      $assertionsDisabled = var0;
   }

   public AudioSampleEntry(String var1) {
      super(var1);
   }

   public void getBox(WritableByteChannel var1) throws IOException {
      byte var3 = 0;
      var1.write(this.getHeader());
      byte var2;
      if(this.soundVersion == 1) {
         var2 = 16;
      } else {
         var2 = 0;
      }

      if(this.soundVersion == 2) {
         var3 = 36;
      }

      ByteBuffer var4 = ByteBuffer.allocate(var2 + 28 + var3);
      var4.position(6);
      g.b(var4, this.dataReferenceIndex);
      g.b(var4, this.soundVersion);
      g.b(var4, this.reserved1);
      g.b(var4, this.reserved2);
      g.b(var4, this.channelCount);
      g.b(var4, this.sampleSize);
      g.b(var4, this.compressionId);
      g.b(var4, this.packetSize);
      if(this.type.equals("mlpa")) {
         g.b(var4, this.getSampleRate());
      } else {
         g.b(var4, this.getSampleRate() << 16);
      }

      if(this.soundVersion == 1) {
         g.b(var4, this.samplesPerPacket);
         g.b(var4, this.bytesPerPacket);
         g.b(var4, this.bytesPerFrame);
         g.b(var4, this.bytesPerSample);
      }

      if(this.soundVersion == 2) {
         g.b(var4, this.samplesPerPacket);
         g.b(var4, this.bytesPerPacket);
         g.b(var4, this.bytesPerFrame);
         g.b(var4, this.bytesPerSample);
         var4.put(this.soundVersion2Data);
      }

      var1.write((ByteBuffer)var4.rewind());
      this.writeContainer(var1);
   }

   public long getBytesPerFrame() {
      return this.bytesPerFrame;
   }

   public long getBytesPerPacket() {
      return this.bytesPerPacket;
   }

   public long getBytesPerSample() {
      return this.bytesPerSample;
   }

   public int getChannelCount() {
      return this.channelCount;
   }

   public int getCompressionId() {
      return this.compressionId;
   }

   public int getPacketSize() {
      return this.packetSize;
   }

   public int getReserved1() {
      return this.reserved1;
   }

   public long getReserved2() {
      return this.reserved2;
   }

   public long getSampleRate() {
      return this.sampleRate;
   }

   public int getSampleSize() {
      return this.sampleSize;
   }

   public long getSamplesPerPacket() {
      return this.samplesPerPacket;
   }

   public long getSize() {
      byte var3 = 16;
      byte var2 = 0;
      byte var1;
      if(this.soundVersion == 1) {
         var1 = 16;
      } else {
         var1 = 0;
      }

      if(this.soundVersion == 2) {
         var2 = 36;
      }

      long var4 = (long)(var1 + 28 + var2) + this.getContainerSize();
      var1 = var3;
      if(!this.largeBox) {
         if(8L + var4 >= 4294967296L) {
            var1 = var3;
         } else {
            var1 = 8;
         }
      }

      return (long)var1 + var4;
   }

   public int getSoundVersion() {
      return this.soundVersion;
   }

   public byte[] getSoundVersion2Data() {
      return this.soundVersion2Data;
   }

   public void parse(b var1, final ByteBuffer var2, final long var3, com.coremedia.iso.b var5) throws IOException {
      byte var8 = 36;
      byte var6 = 16;
      byte var7 = 0;
      var2 = ByteBuffer.allocate(28);
      var1.a(var2);
      var2.position(6);
      this.dataReferenceIndex = e.c(var2);
      this.soundVersion = e.c(var2);
      this.reserved1 = e.c(var2);
      this.reserved2 = e.a(var2);
      this.channelCount = e.c(var2);
      this.sampleSize = e.c(var2);
      this.compressionId = e.c(var2);
      this.packetSize = e.c(var2);
      this.sampleRate = e.a(var2);
      if(!this.type.equals("mlpa")) {
         this.sampleRate >>>= 16;
      }

      if(this.soundVersion == 1) {
         var2 = ByteBuffer.allocate(16);
         var1.a(var2);
         var2.rewind();
         this.samplesPerPacket = e.a(var2);
         this.bytesPerPacket = e.a(var2);
         this.bytesPerFrame = e.a(var2);
         this.bytesPerSample = e.a(var2);
      }

      if(this.soundVersion == 2) {
         var2 = ByteBuffer.allocate(36);
         var1.a(var2);
         var2.rewind();
         this.samplesPerPacket = e.a(var2);
         this.bytesPerPacket = e.a(var2);
         this.bytesPerFrame = e.a(var2);
         this.bytesPerSample = e.a(var2);
         this.soundVersion2Data = new byte[20];
         var2.get(this.soundVersion2Data);
      }

      long var9;
      if("owma".equals(this.type)) {
         System.err.println("owma");
         if(this.soundVersion != 1) {
            var6 = 0;
         }

         var9 = (long)var6;
         var6 = var7;
         if(this.soundVersion == 2) {
            var6 = 36;
         }

         var3 = var3 - 28L - var9 - (long)var6;
         var2 = ByteBuffer.allocate(com.googlecode.mp4parser.c.b.a(var3));
         var1.a(var2);
         this.addBox(new a() {
            public void getBox(WritableByteChannel var1) throws IOException {
               var2.rewind();
               var1.write(var2);
            }

            public long getOffset() {
               return 0L;
            }

            public com.coremedia.iso.boxes.b getParent() {
               return AudioSampleEntry.this;
            }

            public long getSize() {
               return var3;
            }

            public String getType() {
               return "----";
            }

            public void parse(b var1, ByteBuffer var2x, long var3x, com.coremedia.iso.b var5) throws IOException {
               throw new RuntimeException("NotImplemented");
            }

            public void setParent(com.coremedia.iso.boxes.b var1) {
               if(!AudioSampleEntry.$assertionsDisabled && var1 != AudioSampleEntry.this) {
                  throw new AssertionError("you cannot diswown this special box");
               }
            }
         });
      } else {
         if(this.soundVersion != 1) {
            var6 = 0;
         }

         var9 = (long)var6;
         if(this.soundVersion == 2) {
            var6 = var8;
         } else {
            var6 = 0;
         }

         this.initContainer(var1, var3 - 28L - var9 - (long)var6, var5);
      }

   }

   public void setBytesPerFrame(long var1) {
      this.bytesPerFrame = var1;
   }

   public void setBytesPerPacket(long var1) {
      this.bytesPerPacket = var1;
   }

   public void setBytesPerSample(long var1) {
      this.bytesPerSample = var1;
   }

   public void setChannelCount(int var1) {
      this.channelCount = var1;
   }

   public void setCompressionId(int var1) {
      this.compressionId = var1;
   }

   public void setPacketSize(int var1) {
      this.packetSize = var1;
   }

   public void setReserved1(int var1) {
      this.reserved1 = var1;
   }

   public void setReserved2(long var1) {
      this.reserved2 = var1;
   }

   public void setSampleRate(long var1) {
      this.sampleRate = var1;
   }

   public void setSampleSize(int var1) {
      this.sampleSize = var1;
   }

   public void setSamplesPerPacket(long var1) {
      this.samplesPerPacket = var1;
   }

   public void setSoundVersion(int var1) {
      this.soundVersion = var1;
   }

   public void setSoundVersion2Data(byte[] var1) {
      this.soundVersion2Data = var1;
   }

   public void setType(String var1) {
      this.type = var1;
   }

   public String toString() {
      return "AudioSampleEntry{bytesPerSample=" + this.bytesPerSample + ", bytesPerFrame=" + this.bytesPerFrame + ", bytesPerPacket=" + this.bytesPerPacket + ", samplesPerPacket=" + this.samplesPerPacket + ", packetSize=" + this.packetSize + ", compressionId=" + this.compressionId + ", soundVersion=" + this.soundVersion + ", sampleRate=" + this.sampleRate + ", sampleSize=" + this.sampleSize + ", channelCount=" + this.channelCount + ", boxes=" + this.getBoxes() + '}';
   }
}
