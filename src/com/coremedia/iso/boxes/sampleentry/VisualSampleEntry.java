package com.coremedia.iso.boxes.sampleentry;

import com.coremedia.iso.e;
import com.coremedia.iso.g;
import com.coremedia.iso.j;
import com.coremedia.iso.boxes.b;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;

public final class VisualSampleEntry extends AbstractSampleEntry implements b {
   // $FF: synthetic field
   static final boolean $assertionsDisabled;
   public static final String TYPE1 = "mp4v";
   public static final String TYPE2 = "s263";
   public static final String TYPE3 = "avc1";
   public static final String TYPE4 = "avc3";
   public static final String TYPE5 = "drmi";
   public static final String TYPE6 = "hvc1";
   public static final String TYPE7 = "hev1";
   public static final String TYPE_ENCRYPTED = "encv";
   private String compressorname = "";
   private int depth = 24;
   private int frameCount = 1;
   private int height;
   private double horizresolution = 72.0D;
   private long[] predefined = new long[3];
   private double vertresolution = 72.0D;
   private int width;

   static {
      boolean var0;
      if(!VisualSampleEntry.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      $assertionsDisabled = var0;
   }

   public VisualSampleEntry() {
      super("avc1");
   }

   public VisualSampleEntry(String var1) {
      super(var1);
   }

   public void getBox(WritableByteChannel var1) throws IOException {
      var1.write(this.getHeader());
      ByteBuffer var3 = ByteBuffer.allocate(78);
      var3.position(6);
      g.b(var3, this.dataReferenceIndex);
      g.b(var3, 0);
      g.b(var3, 0);
      g.b(var3, this.predefined[0]);
      g.b(var3, this.predefined[1]);
      g.b(var3, this.predefined[2]);
      g.b(var3, this.getWidth());
      g.b(var3, this.getHeight());
      g.a(var3, this.getHorizresolution());
      g.a(var3, this.getVertresolution());
      g.b(var3, 0L);
      g.b(var3, this.getFrameCount());
      g.c(var3, j.b(this.getCompressorname()));
      var3.put(j.a(this.getCompressorname()));
      int var2 = j.b(this.getCompressorname());

      while(var2 < 31) {
         ++var2;
         var3.put(0);
      }

      g.b(var3, this.getDepth());
      g.b(var3, '\uffff');
      var1.write((ByteBuffer)var3.rewind());
      this.writeContainer(var1);
   }

   public String getCompressorname() {
      return this.compressorname;
   }

   public int getDepth() {
      return this.depth;
   }

   public int getFrameCount() {
      return this.frameCount;
   }

   public int getHeight() {
      return this.height;
   }

   public double getHorizresolution() {
      return this.horizresolution;
   }

   public long getSize() {
      long var2 = this.getContainerSize();
      byte var1;
      if(!this.largeBox && var2 + 78L + 8L < 4294967296L) {
         var1 = 8;
      } else {
         var1 = 16;
      }

      return (long)var1 + var2 + 78L;
   }

   public double getVertresolution() {
      return this.vertresolution;
   }

   public int getWidth() {
      return this.width;
   }

   public void parse(final com.googlecode.mp4parser.b var1, ByteBuffer var2, long var3, com.coremedia.iso.b var5) throws IOException {
      long var8 = var1.b();
      var2 = ByteBuffer.allocate(78);
      var1.a(var2);
      var2.position(6);
      this.dataReferenceIndex = e.c(var2);
      long var10 = (long)e.c(var2);
      if(!$assertionsDisabled && 0L != var10) {
         throw new AssertionError("reserved byte not 0");
      } else {
         var10 = (long)e.c(var2);
         if(!$assertionsDisabled && 0L != var10) {
            throw new AssertionError("reserved byte not 0");
         } else {
            this.predefined[0] = e.a(var2);
            this.predefined[1] = e.a(var2);
            this.predefined[2] = e.a(var2);
            this.width = e.c(var2);
            this.height = e.c(var2);
            this.horizresolution = e.g(var2);
            this.vertresolution = e.g(var2);
            var10 = e.a(var2);
            if(!$assertionsDisabled && 0L != var10) {
               throw new AssertionError("reserved byte not 0");
            } else {
               this.frameCount = e.c(var2);
               int var7 = e.d(var2);
               int var6 = var7;
               if(var7 > 31) {
                  var6 = 31;
               }

               byte[] var12 = new byte[var6];
               var2.get(var12);
               this.compressorname = j.a(var12);
               if(var6 < 31) {
                  var2.get(new byte[31 - var6]);
               }

               this.depth = e.c(var2);
               var10 = (long)e.c(var2);
               if(!$assertionsDisabled && 65535L != var10) {
                  throw new AssertionError();
               } else {
                  this.initContainer(new com.googlecode.mp4parser.b(var8 + var3) {
                     // $FF: synthetic field
                     private final long b;

                     {
                        this.b = var2;
                     }

                     public int a(ByteBuffer var1x) throws IOException {
                        int var2;
                        if(this.b == var1.b()) {
                           var2 = -1;
                        } else if((long)var1x.remaining() > this.b - var1.b()) {
                           ByteBuffer var3 = ByteBuffer.allocate(com.googlecode.mp4parser.c.b.a(this.b - var1.b()));
                           var1.a(var3);
                           var1x.put((ByteBuffer)var3.rewind());
                           var2 = var3.capacity();
                        } else {
                           var2 = var1.a(var1x);
                        }

                        return var2;
                     }

                     public long a() throws IOException {
                        return this.b;
                     }

                     public long a(long var1x, long var3, WritableByteChannel var5) throws IOException {
                        return var1.a(var1x, var3, var5);
                     }

                     public ByteBuffer a(long var1x, long var3) throws IOException {
                        return var1.a(var1x, var3);
                     }

                     public void a(long var1x) throws IOException {
                        var1.a(var1x);
                     }

                     public long b() throws IOException {
                        return var1.b();
                     }

                     public void close() throws IOException {
                        var1.close();
                     }
                  }, var3 - 78L, var5);
               }
            }
         }
      }
   }

   public void setCompressorname(String var1) {
      this.compressorname = var1;
   }

   public void setDepth(int var1) {
      this.depth = var1;
   }

   public void setFrameCount(int var1) {
      this.frameCount = var1;
   }

   public void setHeight(int var1) {
      this.height = var1;
   }

   public void setHorizresolution(double var1) {
      this.horizresolution = var1;
   }

   public void setType(String var1) {
      this.type = var1;
   }

   public void setVertresolution(double var1) {
      this.vertresolution = var1;
   }

   public void setWidth(int var1) {
      this.width = var1;
   }
}
