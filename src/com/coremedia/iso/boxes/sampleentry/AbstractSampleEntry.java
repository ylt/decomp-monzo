package com.coremedia.iso.boxes.sampleentry;

import com.googlecode.mp4parser.AbstractContainerBox;
import com.googlecode.mp4parser.b;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;

public abstract class AbstractSampleEntry extends AbstractContainerBox implements SampleEntry {
   protected int dataReferenceIndex = 1;

   protected AbstractSampleEntry(String var1) {
      super(var1);
   }

   public abstract void getBox(WritableByteChannel var1) throws IOException;

   public int getDataReferenceIndex() {
      return this.dataReferenceIndex;
   }

   public abstract void parse(b var1, ByteBuffer var2, long var3, com.coremedia.iso.b var5) throws IOException;

   public void setDataReferenceIndex(int var1) {
      this.dataReferenceIndex = var1;
   }
}
