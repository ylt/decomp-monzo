package com.coremedia.iso.boxes.sampleentry;

import com.coremedia.iso.e;
import com.coremedia.iso.g;
import com.googlecode.mp4parser.b;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;

public class Ovc1VisualSampleEntryImpl extends AbstractSampleEntry {
   public static final String TYPE = "ovc1";
   private byte[] vc1Content = new byte[0];

   public Ovc1VisualSampleEntryImpl() {
      super("ovc1");
   }

   public void getBox(WritableByteChannel var1) throws IOException {
      var1.write(this.getHeader());
      ByteBuffer var2 = ByteBuffer.allocate(8);
      var2.position(6);
      g.b(var2, this.dataReferenceIndex);
      var1.write((ByteBuffer)var2.rewind());
      var1.write(ByteBuffer.wrap(this.vc1Content));
   }

   public long getSize() {
      byte var1;
      if(!this.largeBox && (long)(this.vc1Content.length + 16) < 4294967296L) {
         var1 = 8;
      } else {
         var1 = 16;
      }

      return (long)var1 + (long)this.vc1Content.length + 8L;
   }

   public byte[] getVc1Content() {
      return this.vc1Content;
   }

   public void parse(b var1, ByteBuffer var2, long var3, com.coremedia.iso.b var5) throws IOException {
      var2 = ByteBuffer.allocate(com.googlecode.mp4parser.c.b.a(var3));
      var1.a(var2);
      var2.position(6);
      this.dataReferenceIndex = e.c(var2);
      this.vc1Content = new byte[var2.remaining()];
      var2.get(this.vc1Content);
   }

   public void setVc1Content(byte[] var1) {
      this.vc1Content = var1;
   }
}
