package com.coremedia.iso.boxes.sampleentry;

import com.coremedia.iso.e;
import com.coremedia.iso.g;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;
import java.util.Arrays;

public class TextSampleEntry extends AbstractSampleEntry {
   public static final String TYPE1 = "tx3g";
   public static final String TYPE_ENCRYPTED = "enct";
   private int[] backgroundColorRgba = new int[4];
   private TextSampleEntry.a boxRecord = new TextSampleEntry.a();
   private long displayFlags;
   private int horizontalJustification;
   private TextSampleEntry.b styleRecord = new TextSampleEntry.b();
   private int verticalJustification;

   public TextSampleEntry() {
      super("tx3g");
   }

   public TextSampleEntry(String var1) {
      super(var1);
   }

   public int[] getBackgroundColorRgba() {
      return this.backgroundColorRgba;
   }

   public void getBox(WritableByteChannel var1) throws IOException {
      var1.write(this.getHeader());
      ByteBuffer var2 = ByteBuffer.allocate(38);
      var2.position(6);
      g.b(var2, this.dataReferenceIndex);
      g.b(var2, this.displayFlags);
      g.c(var2, this.horizontalJustification);
      g.c(var2, this.verticalJustification);
      g.c(var2, this.backgroundColorRgba[0]);
      g.c(var2, this.backgroundColorRgba[1]);
      g.c(var2, this.backgroundColorRgba[2]);
      g.c(var2, this.backgroundColorRgba[3]);
      this.boxRecord.b(var2);
      this.styleRecord.b(var2);
      var1.write((ByteBuffer)var2.rewind());
      this.writeContainer(var1);
   }

   public TextSampleEntry.a getBoxRecord() {
      return this.boxRecord;
   }

   public int getHorizontalJustification() {
      return this.horizontalJustification;
   }

   public long getSize() {
      long var2 = this.getContainerSize();
      byte var1;
      if(!this.largeBox && var2 + 38L < 4294967296L) {
         var1 = 8;
      } else {
         var1 = 16;
      }

      return (long)var1 + var2 + 38L;
   }

   public TextSampleEntry.b getStyleRecord() {
      return this.styleRecord;
   }

   public int getVerticalJustification() {
      return this.verticalJustification;
   }

   public boolean isContinuousKaraoke() {
      boolean var1;
      if((this.displayFlags & 2048L) == 2048L) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public boolean isFillTextRegion() {
      boolean var1;
      if((this.displayFlags & 262144L) == 262144L) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public boolean isScrollDirection() {
      boolean var1;
      if((this.displayFlags & 384L) == 384L) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public boolean isScrollIn() {
      boolean var1;
      if((this.displayFlags & 32L) == 32L) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public boolean isScrollOut() {
      boolean var1;
      if((this.displayFlags & 64L) == 64L) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public boolean isWriteTextVertically() {
      boolean var1;
      if((this.displayFlags & 131072L) == 131072L) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public void parse(com.googlecode.mp4parser.b var1, ByteBuffer var2, long var3, com.coremedia.iso.b var5) throws IOException {
      var2 = ByteBuffer.allocate(38);
      var1.a(var2);
      var2.position(6);
      this.dataReferenceIndex = e.c(var2);
      this.displayFlags = e.a(var2);
      this.horizontalJustification = e.d(var2);
      this.verticalJustification = e.d(var2);
      this.backgroundColorRgba = new int[4];
      this.backgroundColorRgba[0] = e.d(var2);
      this.backgroundColorRgba[1] = e.d(var2);
      this.backgroundColorRgba[2] = e.d(var2);
      this.backgroundColorRgba[3] = e.d(var2);
      this.boxRecord = new TextSampleEntry.a();
      this.boxRecord.a(var2);
      this.styleRecord = new TextSampleEntry.b();
      this.styleRecord.a(var2);
      this.initContainer(var1, var3 - 38L, var5);
   }

   public void setBackgroundColorRgba(int[] var1) {
      this.backgroundColorRgba = var1;
   }

   public void setBoxRecord(TextSampleEntry.a var1) {
      this.boxRecord = var1;
   }

   public void setContinuousKaraoke(boolean var1) {
      if(var1) {
         this.displayFlags |= 2048L;
      } else {
         this.displayFlags &= -2049L;
      }

   }

   public void setFillTextRegion(boolean var1) {
      if(var1) {
         this.displayFlags |= 262144L;
      } else {
         this.displayFlags &= -262145L;
      }

   }

   public void setHorizontalJustification(int var1) {
      this.horizontalJustification = var1;
   }

   public void setScrollDirection(boolean var1) {
      if(var1) {
         this.displayFlags |= 384L;
      } else {
         this.displayFlags &= -385L;
      }

   }

   public void setScrollIn(boolean var1) {
      if(var1) {
         this.displayFlags |= 32L;
      } else {
         this.displayFlags &= -33L;
      }

   }

   public void setScrollOut(boolean var1) {
      if(var1) {
         this.displayFlags |= 64L;
      } else {
         this.displayFlags &= -65L;
      }

   }

   public void setStyleRecord(TextSampleEntry.b var1) {
      this.styleRecord = var1;
   }

   public void setType(String var1) {
      this.type = var1;
   }

   public void setVerticalJustification(int var1) {
      this.verticalJustification = var1;
   }

   public void setWriteTextVertically(boolean var1) {
      if(var1) {
         this.displayFlags |= 131072L;
      } else {
         this.displayFlags &= -131073L;
      }

   }

   public String toString() {
      return "TextSampleEntry";
   }

   public static class a {
      int a;
      int b;
      int c;
      int d;

      public void a(ByteBuffer var1) {
         this.a = e.c(var1);
         this.b = e.c(var1);
         this.c = e.c(var1);
         this.d = e.c(var1);
      }

      public void b(ByteBuffer var1) {
         g.b(var1, this.a);
         g.b(var1, this.b);
         g.b(var1, this.c);
         g.b(var1, this.d);
      }

      public boolean equals(Object var1) {
         boolean var2 = true;
         if(this != var1) {
            if(var1 != null && this.getClass() == var1.getClass()) {
               TextSampleEntry.a var3 = (TextSampleEntry.a)var1;
               if(this.c != var3.c) {
                  var2 = false;
               } else if(this.b != var3.b) {
                  var2 = false;
               } else if(this.d != var3.d) {
                  var2 = false;
               } else if(this.a != var3.a) {
                  var2 = false;
               }
            } else {
               var2 = false;
            }
         }

         return var2;
      }

      public int hashCode() {
         return ((this.a * 31 + this.b) * 31 + this.c) * 31 + this.d;
      }
   }

   public static class b {
      int a;
      int b;
      int c;
      int d;
      int e;
      int[] f = new int[]{255, 255, 255, 255};

      public void a(ByteBuffer var1) {
         this.a = e.c(var1);
         this.b = e.c(var1);
         this.c = e.c(var1);
         this.d = e.d(var1);
         this.e = e.d(var1);
         this.f = new int[4];
         this.f[0] = e.d(var1);
         this.f[1] = e.d(var1);
         this.f[2] = e.d(var1);
         this.f[3] = e.d(var1);
      }

      public void b(ByteBuffer var1) {
         g.b(var1, this.a);
         g.b(var1, this.b);
         g.b(var1, this.c);
         g.c(var1, this.d);
         g.c(var1, this.e);
         g.c(var1, this.f[0]);
         g.c(var1, this.f[1]);
         g.c(var1, this.f[2]);
         g.c(var1, this.f[3]);
      }

      public boolean equals(Object var1) {
         boolean var2 = true;
         if(this != var1) {
            if(var1 != null && this.getClass() == var1.getClass()) {
               TextSampleEntry.b var3 = (TextSampleEntry.b)var1;
               if(this.b != var3.b) {
                  var2 = false;
               } else if(this.d != var3.d) {
                  var2 = false;
               } else if(this.c != var3.c) {
                  var2 = false;
               } else if(this.e != var3.e) {
                  var2 = false;
               } else if(this.a != var3.a) {
                  var2 = false;
               } else if(!Arrays.equals(this.f, var3.f)) {
                  var2 = false;
               }
            } else {
               var2 = false;
            }
         }

         return var2;
      }

      public int hashCode() {
         int var2 = this.a;
         int var3 = this.b;
         int var5 = this.c;
         int var6 = this.d;
         int var4 = this.e;
         int var1;
         if(this.f != null) {
            var1 = Arrays.hashCode(this.f);
         } else {
            var1 = 0;
         }

         return var1 + ((((var2 * 31 + var3) * 31 + var5) * 31 + var6) * 31 + var4) * 31;
      }
   }
}
