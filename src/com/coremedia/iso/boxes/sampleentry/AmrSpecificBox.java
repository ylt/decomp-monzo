package com.coremedia.iso.boxes.sampleentry;

import com.coremedia.iso.d;
import com.coremedia.iso.e;
import com.coremedia.iso.g;
import com.googlecode.mp4parser.AbstractBox;
import java.nio.ByteBuffer;
import org.mp4parser.aspectj.a.b.b;
import org.mp4parser.aspectj.lang.a;

public class AmrSpecificBox extends AbstractBox {
   public static final String TYPE = "damr";
   private static final a.a ajc$tjp_0;
   private static final a.a ajc$tjp_1;
   private static final a.a ajc$tjp_2;
   private static final a.a ajc$tjp_3;
   private static final a.a ajc$tjp_4;
   private static final a.a ajc$tjp_5;
   private static final a.a ajc$tjp_6;
   private int decoderVersion;
   private int framesPerSample;
   private int modeChangePeriod;
   private int modeSet;
   private String vendor;

   static {
      ajc$preClinit();
   }

   public AmrSpecificBox() {
      super("damr");
   }

   private static void ajc$preClinit() {
      b var0 = new b("AmrSpecificBox.java", AmrSpecificBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getVendor", "com.coremedia.iso.boxes.sampleentry.AmrSpecificBox", "", "", "", "java.lang.String"), 46);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "getDecoderVersion", "com.coremedia.iso.boxes.sampleentry.AmrSpecificBox", "", "", "", "int"), 50);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "getModeSet", "com.coremedia.iso.boxes.sampleentry.AmrSpecificBox", "", "", "", "int"), 54);
      ajc$tjp_3 = var0.a("method-execution", var0.a("1", "getModeChangePeriod", "com.coremedia.iso.boxes.sampleentry.AmrSpecificBox", "", "", "", "int"), 58);
      ajc$tjp_4 = var0.a("method-execution", var0.a("1", "getFramesPerSample", "com.coremedia.iso.boxes.sampleentry.AmrSpecificBox", "", "", "", "int"), 62);
      ajc$tjp_5 = var0.a("method-execution", var0.a("1", "getContent", "com.coremedia.iso.boxes.sampleentry.AmrSpecificBox", "java.nio.ByteBuffer", "byteBuffer", "", "void"), 84);
      ajc$tjp_6 = var0.a("method-execution", var0.a("1", "toString", "com.coremedia.iso.boxes.sampleentry.AmrSpecificBox", "", "", "", "java.lang.String"), 92);
   }

   public void _parseDetails(ByteBuffer var1) {
      byte[] var2 = new byte[4];
      var1.get(var2);
      this.vendor = d.a(var2);
      this.decoderVersion = e.d(var1);
      this.modeSet = e.c(var1);
      this.modeChangePeriod = e.d(var1);
      this.framesPerSample = e.d(var1);
   }

   public void getContent(ByteBuffer var1) {
      a var2 = b.a(ajc$tjp_5, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      var1.put(d.a(this.vendor));
      g.c(var1, this.decoderVersion);
      g.b(var1, this.modeSet);
      g.c(var1, this.modeChangePeriod);
      g.c(var1, this.framesPerSample);
   }

   protected long getContentSize() {
      return 9L;
   }

   public int getDecoderVersion() {
      a var1 = b.a(ajc$tjp_1, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.decoderVersion;
   }

   public int getFramesPerSample() {
      a var1 = b.a(ajc$tjp_4, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.framesPerSample;
   }

   public int getModeChangePeriod() {
      a var1 = b.a(ajc$tjp_3, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.modeChangePeriod;
   }

   public int getModeSet() {
      a var1 = b.a(ajc$tjp_2, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.modeSet;
   }

   public String getVendor() {
      a var1 = b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.vendor;
   }

   public String toString() {
      a var1 = b.a(ajc$tjp_6, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      StringBuilder var2 = new StringBuilder();
      var2.append("AmrSpecificBox[vendor=").append(this.getVendor());
      var2.append(";decoderVersion=").append(this.getDecoderVersion());
      var2.append(";modeSet=").append(this.getModeSet());
      var2.append(";modeChangePeriod=").append(this.getModeChangePeriod());
      var2.append(";framesPerSample=").append(this.getFramesPerSample());
      var2.append("]");
      return var2.toString();
   }
}
