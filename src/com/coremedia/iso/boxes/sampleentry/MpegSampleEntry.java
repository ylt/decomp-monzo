package com.coremedia.iso.boxes.sampleentry;

import com.coremedia.iso.e;
import com.coremedia.iso.g;
import com.googlecode.mp4parser.b;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;
import java.util.Arrays;
import java.util.List;

public class MpegSampleEntry extends AbstractSampleEntry {
   public MpegSampleEntry() {
      super("mp4s");
   }

   public MpegSampleEntry(String var1) {
      super(var1);
   }

   public void getBox(WritableByteChannel var1) throws IOException {
      var1.write(this.getHeader());
      ByteBuffer var2 = ByteBuffer.allocate(8);
      var2.position(6);
      g.b(var2, this.dataReferenceIndex);
      var1.write((ByteBuffer)var2.rewind());
      this.writeContainer(var1);
   }

   public long getSize() {
      long var2 = this.getContainerSize();
      byte var1;
      if(!this.largeBox && var2 + 8L < 4294967296L) {
         var1 = 8;
      } else {
         var1 = 16;
      }

      return (long)var1 + var2 + 8L;
   }

   public void parse(b var1, ByteBuffer var2, long var3, com.coremedia.iso.b var5) throws IOException {
      var2 = ByteBuffer.allocate(8);
      var1.a(var2);
      var2.position(6);
      this.dataReferenceIndex = e.c(var2);
      this.initContainer(var1, var3 - 8L, var5);
   }

   public String toString() {
      return "MpegSampleEntry" + Arrays.asList(new List[]{this.getBoxes()});
   }
}
