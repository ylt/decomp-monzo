package com.coremedia.iso.boxes.sampleentry;

import com.coremedia.iso.boxes.a;
import com.coremedia.iso.boxes.b;

public interface SampleEntry extends a, b {
   int getDataReferenceIndex();

   void setDataReferenceIndex(int var1);
}
