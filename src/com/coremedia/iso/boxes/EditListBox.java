package com.coremedia.iso.boxes;

import com.coremedia.iso.e;
import com.coremedia.iso.g;
import com.googlecode.mp4parser.AbstractFullBox;
import java.nio.ByteBuffer;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class EditListBox extends AbstractFullBox {
   public static final String TYPE = "elst";
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_0;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_1;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_2;
   private List entries = new LinkedList();

   static {
      ajc$preClinit();
   }

   public EditListBox() {
      super("elst");
   }

   private static void ajc$preClinit() {
      org.mp4parser.aspectj.a.b.b var0 = new org.mp4parser.aspectj.a.b.b("EditListBox.java", EditListBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getEntries", "com.coremedia.iso.boxes.EditListBox", "", "", "", "java.util.List"), 68);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "setEntries", "com.coremedia.iso.boxes.EditListBox", "java.util.List", "entries", "", "void"), 72);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "toString", "com.coremedia.iso.boxes.EditListBox", "", "", "", "java.lang.String"), 108);
   }

   public void _parseDetails(ByteBuffer var1) {
      this.parseVersionAndFlags(var1);
      int var3 = com.googlecode.mp4parser.c.b.a(e.a(var1));
      this.entries = new LinkedList();

      for(int var2 = 0; var2 < var3; ++var2) {
         this.entries.add(new EditListBox.a(this, var1));
      }

   }

   protected void getContent(ByteBuffer var1) {
      this.writeVersionAndFlags(var1);
      g.b(var1, (long)this.entries.size());
      Iterator var2 = this.entries.iterator();

      while(var2.hasNext()) {
         ((EditListBox.a)var2.next()).a(var1);
      }

   }

   protected long getContentSize() {
      long var1;
      if(this.getVersion() == 1) {
         var1 = 8L + (long)(this.entries.size() * 20);
      } else {
         var1 = 8L + (long)(this.entries.size() * 12);
      }

      return var1;
   }

   public List getEntries() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.entries;
   }

   public void setEntries(List var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_1, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.entries = var1;
   }

   public String toString() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_2, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return "EditListBox{entries=" + this.entries + '}';
   }

   public static class a {
      EditListBox a;
      private long b;
      private long c;
      private double d;

      public a(EditListBox var1, long var2, long var4, double var6) {
         this.b = var2;
         this.c = var4;
         this.d = var6;
         this.a = var1;
      }

      public a(EditListBox var1, ByteBuffer var2) {
         if(var1.getVersion() == 1) {
            this.b = e.f(var2);
            this.c = var2.getLong();
            this.d = e.g(var2);
         } else {
            this.b = e.a(var2);
            this.c = (long)var2.getInt();
            this.d = e.g(var2);
         }

         this.a = var1;
      }

      public long a() {
         return this.b;
      }

      public void a(ByteBuffer var1) {
         if(this.a.getVersion() == 1) {
            g.a(var1, this.b);
            var1.putLong(this.c);
         } else {
            g.b(var1, (long)com.googlecode.mp4parser.c.b.a(this.b));
            var1.putInt(com.googlecode.mp4parser.c.b.a(this.c));
         }

         g.a(var1, this.d);
      }

      public long b() {
         return this.c;
      }

      public double c() {
         return this.d;
      }

      public boolean equals(Object var1) {
         boolean var2 = true;
         if(this != var1) {
            if(var1 != null && this.getClass() == var1.getClass()) {
               EditListBox.a var3 = (EditListBox.a)var1;
               if(this.c != var3.c) {
                  var2 = false;
               } else if(this.b != var3.b) {
                  var2 = false;
               }
            } else {
               var2 = false;
            }
         }

         return var2;
      }

      public int hashCode() {
         return (int)(this.b ^ this.b >>> 32) * 31 + (int)(this.c ^ this.c >>> 32);
      }

      public String toString() {
         return "Entry{segmentDuration=" + this.b + ", mediaTime=" + this.c + ", mediaRate=" + this.d + '}';
      }
   }
}
