package com.coremedia.iso.boxes;

public interface FullBox extends a {
   int getFlags();

   int getVersion();

   void setFlags(int var1);

   void setVersion(int var1);
}
