package com.coremedia.iso.boxes;

import com.coremedia.iso.e;
import com.coremedia.iso.g;
import com.googlecode.mp4parser.AbstractFullBox;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class SampleToChunkBox extends AbstractFullBox {
   public static final String TYPE = "stsc";
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_0;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_1;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_2;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_3;
   List entries = Collections.emptyList();

   static {
      ajc$preClinit();
   }

   public SampleToChunkBox() {
      super("stsc");
   }

   private static void ajc$preClinit() {
      org.mp4parser.aspectj.a.b.b var0 = new org.mp4parser.aspectj.a.b.b("SampleToChunkBox.java", SampleToChunkBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getEntries", "com.coremedia.iso.boxes.SampleToChunkBox", "", "", "", "java.util.List"), 47);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "setEntries", "com.coremedia.iso.boxes.SampleToChunkBox", "java.util.List", "entries", "", "void"), 51);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "toString", "com.coremedia.iso.boxes.SampleToChunkBox", "", "", "", "java.lang.String"), 84);
      ajc$tjp_3 = var0.a("method-execution", var0.a("1", "blowup", "com.coremedia.iso.boxes.SampleToChunkBox", "int", "chunkCount", "", "[J"), 95);
   }

   public void _parseDetails(ByteBuffer var1) {
      this.parseVersionAndFlags(var1);
      int var3 = com.googlecode.mp4parser.c.b.a(e.a(var1));
      this.entries = new ArrayList(var3);

      for(int var2 = 0; var2 < var3; ++var2) {
         this.entries.add(new SampleToChunkBox.a(e.a(var1), e.a(var1), e.a(var1)));
      }

   }

   public long[] blowup(int var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_3, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      long[] var4 = new long[var1];
      LinkedList var6 = new LinkedList(this.entries);
      Collections.reverse(var6);
      Iterator var5 = var6.iterator();
      SampleToChunkBox.a var7 = (SampleToChunkBox.a)var5.next();

      SampleToChunkBox.a var3;
      for(var1 = var4.length; var1 > 1; var7 = var3) {
         var4[var1 - 1] = var7.b();
         var3 = var7;
         if((long)var1 == var7.a()) {
            var3 = (SampleToChunkBox.a)var5.next();
         }

         --var1;
      }

      var4[0] = var7.b();
      return var4;
   }

   protected void getContent(ByteBuffer var1) {
      this.writeVersionAndFlags(var1);
      g.b(var1, (long)this.entries.size());
      Iterator var3 = this.entries.iterator();

      while(var3.hasNext()) {
         SampleToChunkBox.a var2 = (SampleToChunkBox.a)var3.next();
         g.b(var1, var2.a());
         g.b(var1, var2.b());
         g.b(var1, var2.c());
      }

   }

   protected long getContentSize() {
      return (long)(this.entries.size() * 12 + 8);
   }

   public List getEntries() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.entries;
   }

   public void setEntries(List var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_1, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.entries = var1;
   }

   public String toString() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_2, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return "SampleToChunkBox[entryCount=" + this.entries.size() + "]";
   }

   public static class a {
      long a;
      long b;
      long c;

      public a(long var1, long var3, long var5) {
         this.a = var1;
         this.b = var3;
         this.c = var5;
      }

      public long a() {
         return this.a;
      }

      public long b() {
         return this.b;
      }

      public long c() {
         return this.c;
      }

      public boolean equals(Object var1) {
         boolean var2 = true;
         if(this != var1) {
            if(var1 != null && this.getClass() == var1.getClass()) {
               SampleToChunkBox.a var3 = (SampleToChunkBox.a)var1;
               if(this.a != var3.a) {
                  var2 = false;
               } else if(this.c != var3.c) {
                  var2 = false;
               } else if(this.b != var3.b) {
                  var2 = false;
               }
            } else {
               var2 = false;
            }
         }

         return var2;
      }

      public int hashCode() {
         return ((int)(this.a ^ this.a >>> 32) * 31 + (int)(this.b ^ this.b >>> 32)) * 31 + (int)(this.c ^ this.c >>> 32);
      }

      public String toString() {
         return "Entry{firstChunk=" + this.a + ", samplesPerChunk=" + this.b + ", sampleDescriptionIndex=" + this.c + '}';
      }
   }
}
