package com.coremedia.iso.boxes;

import com.coremedia.iso.e;
import com.coremedia.iso.g;
import com.coremedia.iso.j;
import com.googlecode.mp4parser.AbstractFullBox;
import java.nio.ByteBuffer;

public class PerformerBox extends AbstractFullBox {
   public static final String TYPE = "perf";
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_0;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_1;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_2;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_3;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_4;
   private String language;
   private String performer;

   static {
      ajc$preClinit();
   }

   public PerformerBox() {
      super("perf");
   }

   private static void ajc$preClinit() {
      org.mp4parser.aspectj.a.b.b var0 = new org.mp4parser.aspectj.a.b.b("PerformerBox.java", PerformerBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getLanguage", "com.coremedia.iso.boxes.PerformerBox", "", "", "", "java.lang.String"), 41);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "getPerformer", "com.coremedia.iso.boxes.PerformerBox", "", "", "", "java.lang.String"), 45);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "setLanguage", "com.coremedia.iso.boxes.PerformerBox", "java.lang.String", "language", "", "void"), 49);
      ajc$tjp_3 = var0.a("method-execution", var0.a("1", "setPerformer", "com.coremedia.iso.boxes.PerformerBox", "java.lang.String", "performer", "", "void"), 53);
      ajc$tjp_4 = var0.a("method-execution", var0.a("1", "toString", "com.coremedia.iso.boxes.PerformerBox", "", "", "", "java.lang.String"), 76);
   }

   public void _parseDetails(ByteBuffer var1) {
      this.parseVersionAndFlags(var1);
      this.language = e.j(var1);
      this.performer = e.e(var1);
   }

   protected void getContent(ByteBuffer var1) {
      this.writeVersionAndFlags(var1);
      g.a(var1, this.language);
      var1.put(j.a(this.performer));
      var1.put(0);
   }

   protected long getContentSize() {
      return (long)(j.b(this.performer) + 6 + 1);
   }

   public String getLanguage() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.language;
   }

   public String getPerformer() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_1, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.performer;
   }

   public void setLanguage(String var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_2, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.language = var1;
   }

   public void setPerformer(String var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_3, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.performer = var1;
   }

   public String toString() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_4, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return "PerformerBox[language=" + this.getLanguage() + ";performer=" + this.getPerformer() + "]";
   }
}
