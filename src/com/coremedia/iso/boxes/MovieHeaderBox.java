package com.coremedia.iso.boxes;

import com.coremedia.iso.e;
import com.coremedia.iso.g;
import com.googlecode.mp4parser.AbstractFullBox;
import com.googlecode.mp4parser.c.c;
import com.googlecode.mp4parser.c.f;
import com.googlecode.mp4parser.c.h;
import java.nio.ByteBuffer;
import java.util.Date;

public class MovieHeaderBox extends AbstractFullBox {
   private static f LOG;
   public static final String TYPE = "mvhd";
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_0;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_1;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_10;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_11;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_12;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_13;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_14;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_15;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_16;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_17;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_18;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_19;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_2;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_20;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_21;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_22;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_23;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_24;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_25;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_26;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_27;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_28;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_3;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_4;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_5;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_6;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_7;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_8;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_9;
   private Date creationTime;
   private int currentTime;
   private long duration;
   private h matrix;
   private Date modificationTime;
   private long nextTrackId;
   private int posterTime;
   private int previewDuration;
   private int previewTime;
   private double rate = 1.0D;
   private int selectionDuration;
   private int selectionTime;
   private long timescale;
   private float volume = 1.0F;

   static {
      ajc$preClinit();
      LOG = f.a(MovieHeaderBox.class);
   }

   public MovieHeaderBox() {
      super("mvhd");
      this.matrix = h.j;
   }

   private static void ajc$preClinit() {
      org.mp4parser.aspectj.a.b.b var0 = new org.mp4parser.aspectj.a.b.b("MovieHeaderBox.java", MovieHeaderBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getCreationTime", "com.coremedia.iso.boxes.MovieHeaderBox", "", "", "", "java.util.Date"), 66);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "getModificationTime", "com.coremedia.iso.boxes.MovieHeaderBox", "", "", "", "java.util.Date"), 70);
      ajc$tjp_10 = var0.a("method-execution", var0.a("1", "setModificationTime", "com.coremedia.iso.boxes.MovieHeaderBox", "java.util.Date", "modificationTime", "", "void"), 212);
      ajc$tjp_11 = var0.a("method-execution", var0.a("1", "setTimescale", "com.coremedia.iso.boxes.MovieHeaderBox", "long", "timescale", "", "void"), 220);
      ajc$tjp_12 = var0.a("method-execution", var0.a("1", "setDuration", "com.coremedia.iso.boxes.MovieHeaderBox", "long", "duration", "", "void"), 224);
      ajc$tjp_13 = var0.a("method-execution", var0.a("1", "setRate", "com.coremedia.iso.boxes.MovieHeaderBox", "double", "rate", "", "void"), 231);
      ajc$tjp_14 = var0.a("method-execution", var0.a("1", "setVolume", "com.coremedia.iso.boxes.MovieHeaderBox", "float", "volume", "", "void"), 235);
      ajc$tjp_15 = var0.a("method-execution", var0.a("1", "setMatrix", "com.coremedia.iso.boxes.MovieHeaderBox", "com.googlecode.mp4parser.util.Matrix", "matrix", "", "void"), 239);
      ajc$tjp_16 = var0.a("method-execution", var0.a("1", "setNextTrackId", "com.coremedia.iso.boxes.MovieHeaderBox", "long", "nextTrackId", "", "void"), 243);
      ajc$tjp_17 = var0.a("method-execution", var0.a("1", "getPreviewTime", "com.coremedia.iso.boxes.MovieHeaderBox", "", "", "", "int"), 247);
      ajc$tjp_18 = var0.a("method-execution", var0.a("1", "setPreviewTime", "com.coremedia.iso.boxes.MovieHeaderBox", "int", "previewTime", "", "void"), 251);
      ajc$tjp_19 = var0.a("method-execution", var0.a("1", "getPreviewDuration", "com.coremedia.iso.boxes.MovieHeaderBox", "", "", "", "int"), 255);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "getTimescale", "com.coremedia.iso.boxes.MovieHeaderBox", "", "", "", "long"), 74);
      ajc$tjp_20 = var0.a("method-execution", var0.a("1", "setPreviewDuration", "com.coremedia.iso.boxes.MovieHeaderBox", "int", "previewDuration", "", "void"), 259);
      ajc$tjp_21 = var0.a("method-execution", var0.a("1", "getPosterTime", "com.coremedia.iso.boxes.MovieHeaderBox", "", "", "", "int"), 263);
      ajc$tjp_22 = var0.a("method-execution", var0.a("1", "setPosterTime", "com.coremedia.iso.boxes.MovieHeaderBox", "int", "posterTime", "", "void"), 267);
      ajc$tjp_23 = var0.a("method-execution", var0.a("1", "getSelectionTime", "com.coremedia.iso.boxes.MovieHeaderBox", "", "", "", "int"), 271);
      ajc$tjp_24 = var0.a("method-execution", var0.a("1", "setSelectionTime", "com.coremedia.iso.boxes.MovieHeaderBox", "int", "selectionTime", "", "void"), 275);
      ajc$tjp_25 = var0.a("method-execution", var0.a("1", "getSelectionDuration", "com.coremedia.iso.boxes.MovieHeaderBox", "", "", "", "int"), 279);
      ajc$tjp_26 = var0.a("method-execution", var0.a("1", "setSelectionDuration", "com.coremedia.iso.boxes.MovieHeaderBox", "int", "selectionDuration", "", "void"), 283);
      ajc$tjp_27 = var0.a("method-execution", var0.a("1", "getCurrentTime", "com.coremedia.iso.boxes.MovieHeaderBox", "", "", "", "int"), 287);
      ajc$tjp_28 = var0.a("method-execution", var0.a("1", "setCurrentTime", "com.coremedia.iso.boxes.MovieHeaderBox", "int", "currentTime", "", "void"), 291);
      ajc$tjp_3 = var0.a("method-execution", var0.a("1", "getDuration", "com.coremedia.iso.boxes.MovieHeaderBox", "", "", "", "long"), 78);
      ajc$tjp_4 = var0.a("method-execution", var0.a("1", "getRate", "com.coremedia.iso.boxes.MovieHeaderBox", "", "", "", "double"), 82);
      ajc$tjp_5 = var0.a("method-execution", var0.a("1", "getVolume", "com.coremedia.iso.boxes.MovieHeaderBox", "", "", "", "float"), 86);
      ajc$tjp_6 = var0.a("method-execution", var0.a("1", "getMatrix", "com.coremedia.iso.boxes.MovieHeaderBox", "", "", "", "com.googlecode.mp4parser.util.Matrix"), 90);
      ajc$tjp_7 = var0.a("method-execution", var0.a("1", "getNextTrackId", "com.coremedia.iso.boxes.MovieHeaderBox", "", "", "", "long"), 94);
      ajc$tjp_8 = var0.a("method-execution", var0.a("1", "toString", "com.coremedia.iso.boxes.MovieHeaderBox", "", "", "", "java.lang.String"), 148);
      ajc$tjp_9 = var0.a("method-execution", var0.a("1", "setCreationTime", "com.coremedia.iso.boxes.MovieHeaderBox", "java.util.Date", "creationTime", "", "void"), 204);
   }

   public void _parseDetails(ByteBuffer var1) {
      this.parseVersionAndFlags(var1);
      if(this.getVersion() == 1) {
         this.creationTime = c.a(e.f(var1));
         this.modificationTime = c.a(e.f(var1));
         this.timescale = e.a(var1);
         this.duration = var1.getLong();
      } else {
         this.creationTime = c.a(e.a(var1));
         this.modificationTime = c.a(e.a(var1));
         this.timescale = e.a(var1);
         this.duration = (long)var1.getInt();
      }

      if(this.duration < -1L) {
         LOG.b("mvhd duration is not in expected range");
      }

      this.rate = e.g(var1);
      this.volume = e.i(var1);
      e.c(var1);
      e.a(var1);
      e.a(var1);
      this.matrix = h.a(var1);
      this.previewTime = var1.getInt();
      this.previewDuration = var1.getInt();
      this.posterTime = var1.getInt();
      this.selectionTime = var1.getInt();
      this.selectionDuration = var1.getInt();
      this.currentTime = var1.getInt();
      this.nextTrackId = e.a(var1);
   }

   protected void getContent(ByteBuffer var1) {
      this.writeVersionAndFlags(var1);
      if(this.getVersion() == 1) {
         g.a(var1, c.a(this.creationTime));
         g.a(var1, c.a(this.modificationTime));
         g.b(var1, this.timescale);
         var1.putLong(this.duration);
      } else {
         g.b(var1, c.a(this.creationTime));
         g.b(var1, c.a(this.modificationTime));
         g.b(var1, this.timescale);
         var1.putInt((int)this.duration);
      }

      g.a(var1, this.rate);
      g.c(var1, (double)this.volume);
      g.b(var1, 0);
      g.b(var1, 0L);
      g.b(var1, 0L);
      this.matrix.b(var1);
      var1.putInt(this.previewTime);
      var1.putInt(this.previewDuration);
      var1.putInt(this.posterTime);
      var1.putInt(this.selectionTime);
      var1.putInt(this.selectionDuration);
      var1.putInt(this.currentTime);
      g.b(var1, this.nextTrackId);
   }

   protected long getContentSize() {
      long var1;
      if(this.getVersion() == 1) {
         var1 = 4L + 28L;
      } else {
         var1 = 4L + 16L;
      }

      return var1 + 80L;
   }

   public Date getCreationTime() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.creationTime;
   }

   public int getCurrentTime() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_27, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.currentTime;
   }

   public long getDuration() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_3, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.duration;
   }

   public h getMatrix() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_6, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.matrix;
   }

   public Date getModificationTime() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_1, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.modificationTime;
   }

   public long getNextTrackId() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_7, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.nextTrackId;
   }

   public int getPosterTime() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_21, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.posterTime;
   }

   public int getPreviewDuration() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_19, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.previewDuration;
   }

   public int getPreviewTime() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_17, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.previewTime;
   }

   public double getRate() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_4, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.rate;
   }

   public int getSelectionDuration() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_25, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.selectionDuration;
   }

   public int getSelectionTime() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_23, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.selectionTime;
   }

   public long getTimescale() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_2, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.timescale;
   }

   public float getVolume() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_5, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.volume;
   }

   public void setCreationTime(Date var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_9, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.creationTime = var1;
      if(c.a(var1) >= 4294967296L) {
         this.setVersion(1);
      }

   }

   public void setCurrentTime(int var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_28, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.currentTime = var1;
   }

   public void setDuration(long var1) {
      org.mp4parser.aspectj.lang.a var3 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_12, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var3);
      this.duration = var1;
      if(var1 >= 4294967296L) {
         this.setVersion(1);
      }

   }

   public void setMatrix(h var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_15, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.matrix = var1;
   }

   public void setModificationTime(Date var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_10, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.modificationTime = var1;
      if(c.a(var1) >= 4294967296L) {
         this.setVersion(1);
      }

   }

   public void setNextTrackId(long var1) {
      org.mp4parser.aspectj.lang.a var3 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_16, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var3);
      this.nextTrackId = var1;
   }

   public void setPosterTime(int var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_22, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.posterTime = var1;
   }

   public void setPreviewDuration(int var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_20, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.previewDuration = var1;
   }

   public void setPreviewTime(int var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_18, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.previewTime = var1;
   }

   public void setRate(double var1) {
      org.mp4parser.aspectj.lang.a var3 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_13, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var3);
      this.rate = var1;
   }

   public void setSelectionDuration(int var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_26, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.selectionDuration = var1;
   }

   public void setSelectionTime(int var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_24, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.selectionTime = var1;
   }

   public void setTimescale(long var1) {
      org.mp4parser.aspectj.lang.a var3 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_11, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var3);
      this.timescale = var1;
   }

   public void setVolume(float var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_14, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.volume = var1;
   }

   public String toString() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_8, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      StringBuilder var2 = new StringBuilder();
      var2.append("MovieHeaderBox[");
      var2.append("creationTime=").append(this.getCreationTime());
      var2.append(";");
      var2.append("modificationTime=").append(this.getModificationTime());
      var2.append(";");
      var2.append("timescale=").append(this.getTimescale());
      var2.append(";");
      var2.append("duration=").append(this.getDuration());
      var2.append(";");
      var2.append("rate=").append(this.getRate());
      var2.append(";");
      var2.append("volume=").append(this.getVolume());
      var2.append(";");
      var2.append("matrix=").append(this.matrix);
      var2.append(";");
      var2.append("nextTrackId=").append(this.getNextTrackId());
      var2.append("]");
      return var2.toString();
   }
}
