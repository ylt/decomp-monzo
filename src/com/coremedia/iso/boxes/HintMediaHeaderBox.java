package com.coremedia.iso.boxes;

import com.coremedia.iso.e;
import com.coremedia.iso.g;
import java.nio.ByteBuffer;

public class HintMediaHeaderBox extends AbstractMediaHeaderBox {
   public static final String TYPE = "hmhd";
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_0;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_1;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_2;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_3;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_4;
   private long avgBitrate;
   private int avgPduSize;
   private long maxBitrate;
   private int maxPduSize;

   static {
      ajc$preClinit();
   }

   public HintMediaHeaderBox() {
      super("hmhd");
   }

   private static void ajc$preClinit() {
      org.mp4parser.aspectj.a.b.b var0 = new org.mp4parser.aspectj.a.b.b("HintMediaHeaderBox.java", HintMediaHeaderBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getMaxPduSize", "com.coremedia.iso.boxes.HintMediaHeaderBox", "", "", "", "int"), 42);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "getAvgPduSize", "com.coremedia.iso.boxes.HintMediaHeaderBox", "", "", "", "int"), 46);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "getMaxBitrate", "com.coremedia.iso.boxes.HintMediaHeaderBox", "", "", "", "long"), 50);
      ajc$tjp_3 = var0.a("method-execution", var0.a("1", "getAvgBitrate", "com.coremedia.iso.boxes.HintMediaHeaderBox", "", "", "", "long"), 54);
      ajc$tjp_4 = var0.a("method-execution", var0.a("1", "toString", "com.coremedia.iso.boxes.HintMediaHeaderBox", "", "", "", "java.lang.String"), 84);
   }

   public void _parseDetails(ByteBuffer var1) {
      this.parseVersionAndFlags(var1);
      this.maxPduSize = e.c(var1);
      this.avgPduSize = e.c(var1);
      this.maxBitrate = e.a(var1);
      this.avgBitrate = e.a(var1);
      e.a(var1);
   }

   public long getAvgBitrate() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_3, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.avgBitrate;
   }

   public int getAvgPduSize() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_1, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.avgPduSize;
   }

   protected void getContent(ByteBuffer var1) {
      this.writeVersionAndFlags(var1);
      g.b(var1, this.maxPduSize);
      g.b(var1, this.avgPduSize);
      g.b(var1, this.maxBitrate);
      g.b(var1, this.avgBitrate);
      g.b(var1, 0L);
   }

   protected long getContentSize() {
      return 20L;
   }

   public long getMaxBitrate() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_2, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.maxBitrate;
   }

   public int getMaxPduSize() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.maxPduSize;
   }

   public String toString() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_4, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return "HintMediaHeaderBox{maxPduSize=" + this.maxPduSize + ", avgPduSize=" + this.avgPduSize + ", maxBitrate=" + this.maxBitrate + ", avgBitrate=" + this.avgBitrate + '}';
   }
}
