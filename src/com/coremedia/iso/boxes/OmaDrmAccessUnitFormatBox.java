package com.coremedia.iso.boxes;

import com.coremedia.iso.e;
import com.coremedia.iso.g;
import com.googlecode.mp4parser.AbstractFullBox;
import java.nio.ByteBuffer;

public final class OmaDrmAccessUnitFormatBox extends AbstractFullBox {
   public static final String TYPE = "odaf";
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_0;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_1;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_2;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_3;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_4;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_5;
   private byte allBits;
   private int initVectorLength;
   private int keyIndicatorLength;
   private boolean selectiveEncryption;

   static {
      ajc$preClinit();
   }

   public OmaDrmAccessUnitFormatBox() {
      super("odaf");
   }

   private static void ajc$preClinit() {
      org.mp4parser.aspectj.a.b.b var0 = new org.mp4parser.aspectj.a.b.b("OmaDrmAccessUnitFormatBox.java", OmaDrmAccessUnitFormatBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "isSelectiveEncryption", "com.coremedia.iso.boxes.OmaDrmAccessUnitFormatBox", "", "", "", "boolean"), 46);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "getKeyIndicatorLength", "com.coremedia.iso.boxes.OmaDrmAccessUnitFormatBox", "", "", "", "int"), 50);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "getInitVectorLength", "com.coremedia.iso.boxes.OmaDrmAccessUnitFormatBox", "", "", "", "int"), 54);
      ajc$tjp_3 = var0.a("method-execution", var0.a("1", "setInitVectorLength", "com.coremedia.iso.boxes.OmaDrmAccessUnitFormatBox", "int", "initVectorLength", "", "void"), 58);
      ajc$tjp_4 = var0.a("method-execution", var0.a("1", "setKeyIndicatorLength", "com.coremedia.iso.boxes.OmaDrmAccessUnitFormatBox", "int", "keyIndicatorLength", "", "void"), 62);
      ajc$tjp_5 = var0.a("method-execution", var0.a("1", "setAllBits", "com.coremedia.iso.boxes.OmaDrmAccessUnitFormatBox", "byte", "allBits", "", "void"), 66);
   }

   public void _parseDetails(ByteBuffer var1) {
      this.parseVersionAndFlags(var1);
      this.allBits = (byte)e.d(var1);
      boolean var2;
      if((this.allBits & 128) == 128) {
         var2 = true;
      } else {
         var2 = false;
      }

      this.selectiveEncryption = var2;
      this.keyIndicatorLength = e.d(var1);
      this.initVectorLength = e.d(var1);
   }

   protected void getContent(ByteBuffer var1) {
      this.writeVersionAndFlags(var1);
      g.c(var1, this.allBits);
      g.c(var1, this.keyIndicatorLength);
      g.c(var1, this.initVectorLength);
   }

   protected long getContentSize() {
      return 7L;
   }

   public int getInitVectorLength() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_2, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.initVectorLength;
   }

   public int getKeyIndicatorLength() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_1, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.keyIndicatorLength;
   }

   public boolean isSelectiveEncryption() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.selectiveEncryption;
   }

   public void setAllBits(byte var1) {
      org.mp4parser.aspectj.lang.a var3 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_5, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var3);
      this.allBits = var1;
      boolean var2;
      if((var1 & 128) == 128) {
         var2 = true;
      } else {
         var2 = false;
      }

      this.selectiveEncryption = var2;
   }

   public void setInitVectorLength(int var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_3, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.initVectorLength = var1;
   }

   public void setKeyIndicatorLength(int var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_4, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.keyIndicatorLength = var1;
   }
}
