package com.coremedia.iso.boxes;

import com.coremedia.iso.e;
import com.coremedia.iso.g;
import com.googlecode.mp4parser.AbstractFullBox;
import java.nio.ByteBuffer;

public class SampleSizeBox extends AbstractFullBox {
   public static final String TYPE = "stsz";
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_0;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_1;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_2;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_3;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_4;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_5;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_6;
   int sampleCount;
   private long sampleSize;
   private long[] sampleSizes = new long[0];

   static {
      ajc$preClinit();
   }

   public SampleSizeBox() {
      super("stsz");
   }

   private static void ajc$preClinit() {
      org.mp4parser.aspectj.a.b.b var0 = new org.mp4parser.aspectj.a.b.b("SampleSizeBox.java", SampleSizeBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getSampleSize", "com.coremedia.iso.boxes.SampleSizeBox", "", "", "", "long"), 50);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "setSampleSize", "com.coremedia.iso.boxes.SampleSizeBox", "long", "sampleSize", "", "void"), 54);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "getSampleSizeAtIndex", "com.coremedia.iso.boxes.SampleSizeBox", "int", "index", "", "long"), 59);
      ajc$tjp_3 = var0.a("method-execution", var0.a("1", "getSampleCount", "com.coremedia.iso.boxes.SampleSizeBox", "", "", "", "long"), 67);
      ajc$tjp_4 = var0.a("method-execution", var0.a("1", "getSampleSizes", "com.coremedia.iso.boxes.SampleSizeBox", "", "", "", "[J"), 76);
      ajc$tjp_5 = var0.a("method-execution", var0.a("1", "setSampleSizes", "com.coremedia.iso.boxes.SampleSizeBox", "[J", "sampleSizes", "", "void"), 80);
      ajc$tjp_6 = var0.a("method-execution", var0.a("1", "toString", "com.coremedia.iso.boxes.SampleSizeBox", "", "", "", "java.lang.String"), 119);
   }

   public void _parseDetails(ByteBuffer var1) {
      this.parseVersionAndFlags(var1);
      this.sampleSize = e.a(var1);
      this.sampleCount = com.googlecode.mp4parser.c.b.a(e.a(var1));
      if(this.sampleSize == 0L) {
         this.sampleSizes = new long[this.sampleCount];

         for(int var2 = 0; var2 < this.sampleCount; ++var2) {
            this.sampleSizes[var2] = e.a(var1);
         }
      }

   }

   protected void getContent(ByteBuffer var1) {
      this.writeVersionAndFlags(var1);
      g.b(var1, this.sampleSize);
      if(this.sampleSize == 0L) {
         g.b(var1, (long)this.sampleSizes.length);
         long[] var4 = this.sampleSizes;
         int var3 = var4.length;

         for(int var2 = 0; var2 < var3; ++var2) {
            g.b(var1, var4[var2]);
         }
      } else {
         g.b(var1, (long)this.sampleCount);
      }

   }

   protected long getContentSize() {
      int var1;
      if(this.sampleSize == 0L) {
         var1 = this.sampleSizes.length * 4;
      } else {
         var1 = 0;
      }

      return (long)(var1 + 12);
   }

   public long getSampleCount() {
      org.mp4parser.aspectj.lang.a var3 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_3, this, this);
      com.googlecode.mp4parser.e.a().a(var3);
      long var1;
      if(this.sampleSize > 0L) {
         var1 = (long)this.sampleCount;
      } else {
         var1 = (long)this.sampleSizes.length;
      }

      return var1;
   }

   public long getSampleSize() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.sampleSize;
   }

   public long getSampleSizeAtIndex(int var1) {
      org.mp4parser.aspectj.lang.a var4 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_2, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var4);
      long var2;
      if(this.sampleSize > 0L) {
         var2 = this.sampleSize;
      } else {
         var2 = this.sampleSizes[var1];
      }

      return var2;
   }

   public long[] getSampleSizes() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_4, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.sampleSizes;
   }

   public void setSampleSize(long var1) {
      org.mp4parser.aspectj.lang.a var3 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_1, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var3);
      this.sampleSize = var1;
   }

   public void setSampleSizes(long[] var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_5, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.sampleSizes = var1;
   }

   public String toString() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_6, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return "SampleSizeBox[sampleSize=" + this.getSampleSize() + ";sampleCount=" + this.getSampleCount() + "]";
   }
}
