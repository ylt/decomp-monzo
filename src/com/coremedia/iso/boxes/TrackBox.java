package com.coremedia.iso.boxes;

import com.googlecode.mp4parser.AbstractContainerBox;
import java.util.Iterator;
import java.util.List;

public class TrackBox extends AbstractContainerBox {
   public static final String TYPE = "trak";
   private SampleTableBox sampleTableBox;

   public TrackBox() {
      super("trak");
   }

   public MediaBox getMediaBox() {
      Iterator var2 = this.getBoxes().iterator();

      MediaBox var3;
      while(true) {
         if(!var2.hasNext()) {
            var3 = null;
            break;
         }

         a var1 = (a)var2.next();
         if(var1 instanceof MediaBox) {
            var3 = (MediaBox)var1;
            break;
         }
      }

      return var3;
   }

   public SampleTableBox getSampleTableBox() {
      SampleTableBox var1;
      if(this.sampleTableBox != null) {
         var1 = this.sampleTableBox;
      } else {
         MediaBox var2 = this.getMediaBox();
         if(var2 != null) {
            MediaInformationBox var3 = var2.getMediaInformationBox();
            if(var3 != null) {
               this.sampleTableBox = var3.getSampleTableBox();
               var1 = this.sampleTableBox;
               return var1;
            }
         }

         var1 = null;
      }

      return var1;
   }

   public TrackHeaderBox getTrackHeaderBox() {
      Iterator var2 = this.getBoxes().iterator();

      TrackHeaderBox var3;
      while(true) {
         if(!var2.hasNext()) {
            var3 = null;
            break;
         }

         a var1 = (a)var2.next();
         if(var1 instanceof TrackHeaderBox) {
            var3 = (TrackHeaderBox)var1;
            break;
         }
      }

      return var3;
   }

   public void setBoxes(List var1) {
      super.setBoxes(var1);
      this.sampleTableBox = null;
   }
}
