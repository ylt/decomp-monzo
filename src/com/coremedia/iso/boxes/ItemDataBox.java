package com.coremedia.iso.boxes;

import com.googlecode.mp4parser.AbstractBox;
import com.googlecode.mp4parser.e;
import java.nio.ByteBuffer;

public class ItemDataBox extends AbstractBox {
   public static final String TYPE = "idat";
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_0;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_1;
   ByteBuffer data = ByteBuffer.allocate(0);

   static {
      ajc$preClinit();
   }

   public ItemDataBox() {
      super("idat");
   }

   private static void ajc$preClinit() {
      org.mp4parser.aspectj.a.b.b var0 = new org.mp4parser.aspectj.a.b.b("ItemDataBox.java", ItemDataBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getData", "com.coremedia.iso.boxes.ItemDataBox", "", "", "", "java.nio.ByteBuffer"), 19);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "setData", "com.coremedia.iso.boxes.ItemDataBox", "java.nio.ByteBuffer", "data", "", "void"), 23);
   }

   public void _parseDetails(ByteBuffer var1) {
      this.data = var1.slice();
      var1.position(var1.position() + var1.remaining());
   }

   protected void getContent(ByteBuffer var1) {
      var1.put(this.data);
   }

   protected long getContentSize() {
      return (long)this.data.limit();
   }

   public ByteBuffer getData() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_0, this, this);
      e.a().a(var1);
      return this.data;
   }

   public void setData(ByteBuffer var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_1, this, this, (Object)var1);
      e.a().a(var2);
      this.data = var1;
   }
}
