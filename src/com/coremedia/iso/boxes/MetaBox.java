package com.coremedia.iso.boxes;

import com.coremedia.iso.e;
import com.coremedia.iso.g;
import com.googlecode.mp4parser.AbstractContainerBox;
import com.googlecode.mp4parser.d;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;

public class MetaBox extends AbstractContainerBox {
   public static final String TYPE = "meta";
   private int flags;
   private boolean isFullBox = true;
   private int version;

   public MetaBox() {
      super("meta");
   }

   public void getBox(WritableByteChannel var1) throws IOException {
      var1.write(this.getHeader());
      if(this.isFullBox) {
         ByteBuffer var2 = ByteBuffer.allocate(4);
         this.writeVersionAndFlags(var2);
         var1.write((ByteBuffer)var2.rewind());
      }

      this.writeContainer(var1);
   }

   public int getFlags() {
      return this.flags;
   }

   public long getSize() {
      long var4 = this.getContainerSize();
      long var2 = 0L;
      if(this.isFullBox) {
         var2 = 0L + 4L;
      }

      byte var1;
      if(!this.largeBox && var2 + var4 < 4294967296L) {
         var1 = 8;
      } else {
         var1 = 16;
      }

      return (long)var1 + var4 + var2;
   }

   public int getVersion() {
      return this.version;
   }

   public void parse(com.googlecode.mp4parser.b var1, ByteBuffer var2, long var3, com.coremedia.iso.b var5) throws IOException {
      var2 = ByteBuffer.allocate(com.googlecode.mp4parser.c.b.a(var3));
      var1.a(var2);
      var2.position(4);
      if("hdlr".equals(e.k(var2))) {
         this.isFullBox = false;
         this.initContainer(new d((ByteBuffer)var2.rewind()), var3, var5);
      } else {
         this.isFullBox = true;
         this.parseVersionAndFlags((ByteBuffer)var2.rewind());
         this.initContainer(new d(var2), var3 - 4L, var5);
      }

   }

   protected final long parseVersionAndFlags(ByteBuffer var1) {
      this.version = e.d(var1);
      this.flags = e.b(var1);
      return 4L;
   }

   public void setFlags(int var1) {
      this.flags = var1;
   }

   public void setVersion(int var1) {
      this.version = var1;
   }

   protected final void writeVersionAndFlags(ByteBuffer var1) {
      g.c(var1, this.version);
      g.a(var1, this.flags);
   }
}
