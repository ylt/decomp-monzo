package com.coremedia.iso.boxes;

import com.coremedia.iso.d;
import com.coremedia.iso.e;
import com.coremedia.iso.g;
import com.coremedia.iso.j;
import com.googlecode.mp4parser.AbstractFullBox;
import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class HandlerBox extends AbstractFullBox {
   public static final String TYPE = "hdlr";
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_0;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_1;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_2;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_3;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_4;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_5;
   public static final Map readableTypes;
   private long a;
   private long b;
   private long c;
   private String handlerType;
   private String name = null;
   private long shouldBeZeroButAppleWritesHereSomeValue;
   private boolean zeroTerm = true;

   static {
      ajc$preClinit();
      HashMap var0 = new HashMap();
      var0.put("odsm", "ObjectDescriptorStream - defined in ISO/IEC JTC1/SC29/WG11 - CODING OF MOVING PICTURES AND AUDIO");
      var0.put("crsm", "ClockReferenceStream - defined in ISO/IEC JTC1/SC29/WG11 - CODING OF MOVING PICTURES AND AUDIO");
      var0.put("sdsm", "SceneDescriptionStream - defined in ISO/IEC JTC1/SC29/WG11 - CODING OF MOVING PICTURES AND AUDIO");
      var0.put("m7sm", "MPEG7Stream - defined in ISO/IEC JTC1/SC29/WG11 - CODING OF MOVING PICTURES AND AUDIO");
      var0.put("ocsm", "ObjectContentInfoStream - defined in ISO/IEC JTC1/SC29/WG11 - CODING OF MOVING PICTURES AND AUDIO");
      var0.put("ipsm", "IPMP Stream - defined in ISO/IEC JTC1/SC29/WG11 - CODING OF MOVING PICTURES AND AUDIO");
      var0.put("mjsm", "MPEG-J Stream - defined in ISO/IEC JTC1/SC29/WG11 - CODING OF MOVING PICTURES AND AUDIO");
      var0.put("mdir", "Apple Meta Data iTunes Reader");
      var0.put("mp7b", "MPEG-7 binary XML");
      var0.put("mp7t", "MPEG-7 XML");
      var0.put("vide", "Video Track");
      var0.put("soun", "Sound Track");
      var0.put("hint", "Hint Track");
      var0.put("appl", "Apple specific");
      var0.put("meta", "Timed Metadata track - defined in ISO/IEC JTC1/SC29/WG11 - CODING OF MOVING PICTURES AND AUDIO");
      readableTypes = Collections.unmodifiableMap(var0);
   }

   public HandlerBox() {
      super("hdlr");
   }

   private static void ajc$preClinit() {
      org.mp4parser.aspectj.a.b.b var0 = new org.mp4parser.aspectj.a.b.b("HandlerBox.java", HandlerBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getHandlerType", "com.coremedia.iso.boxes.HandlerBox", "", "", "", "java.lang.String"), 78);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "setName", "com.coremedia.iso.boxes.HandlerBox", "java.lang.String", "name", "", "void"), 87);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "setHandlerType", "com.coremedia.iso.boxes.HandlerBox", "java.lang.String", "handlerType", "", "void"), 91);
      ajc$tjp_3 = var0.a("method-execution", var0.a("1", "getName", "com.coremedia.iso.boxes.HandlerBox", "", "", "", "java.lang.String"), 95);
      ajc$tjp_4 = var0.a("method-execution", var0.a("1", "getHumanReadableTrackType", "com.coremedia.iso.boxes.HandlerBox", "", "", "", "java.lang.String"), 99);
      ajc$tjp_5 = var0.a("method-execution", var0.a("1", "toString", "com.coremedia.iso.boxes.HandlerBox", "", "", "", "java.lang.String"), 149);
   }

   public void _parseDetails(ByteBuffer var1) {
      this.parseVersionAndFlags(var1);
      this.shouldBeZeroButAppleWritesHereSomeValue = e.a(var1);
      this.handlerType = e.k(var1);
      this.a = e.a(var1);
      this.b = e.a(var1);
      this.c = e.a(var1);
      if(var1.remaining() > 0) {
         this.name = e.a(var1, var1.remaining());
         if(this.name.endsWith("\u0000")) {
            this.name = this.name.substring(0, this.name.length() - 1);
            this.zeroTerm = true;
         } else {
            this.zeroTerm = false;
         }
      } else {
         this.zeroTerm = false;
      }

   }

   protected void getContent(ByteBuffer var1) {
      this.writeVersionAndFlags(var1);
      g.b(var1, this.shouldBeZeroButAppleWritesHereSomeValue);
      var1.put(d.a(this.handlerType));
      g.b(var1, this.a);
      g.b(var1, this.b);
      g.b(var1, this.c);
      if(this.name != null) {
         var1.put(j.a(this.name));
      }

      if(this.zeroTerm) {
         var1.put(0);
      }

   }

   protected long getContentSize() {
      long var1;
      if(this.zeroTerm) {
         var1 = (long)(j.b(this.name) + 25);
      } else {
         var1 = (long)(j.b(this.name) + 24);
      }

      return var1;
   }

   public String getHandlerType() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.handlerType;
   }

   public String getHumanReadableTrackType() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_4, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      String var2;
      if(readableTypes.get(this.handlerType) != null) {
         var2 = (String)readableTypes.get(this.handlerType);
      } else {
         var2 = "Unknown Handler Type";
      }

      return var2;
   }

   public String getName() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_3, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.name;
   }

   public void setHandlerType(String var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_2, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.handlerType = var1;
   }

   public void setName(String var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_1, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.name = var1;
   }

   public String toString() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_5, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return "HandlerBox[handlerType=" + this.getHandlerType() + ";name=" + this.getName() + "]";
   }
}
