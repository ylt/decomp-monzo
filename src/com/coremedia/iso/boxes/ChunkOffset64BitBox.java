package com.coremedia.iso.boxes;

import com.coremedia.iso.e;
import com.coremedia.iso.g;
import java.nio.ByteBuffer;

public class ChunkOffset64BitBox extends ChunkOffsetBox {
   public static final String TYPE = "co64";
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_0;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_1;
   private long[] chunkOffsets;

   static {
      ajc$preClinit();
   }

   public ChunkOffset64BitBox() {
      super("co64");
   }

   private static void ajc$preClinit() {
      org.mp4parser.aspectj.a.b.b var0 = new org.mp4parser.aspectj.a.b.b("ChunkOffset64BitBox.java", ChunkOffset64BitBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getChunkOffsets", "com.coremedia.iso.boxes.ChunkOffset64BitBox", "", "", "", "[J"), 23);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "setChunkOffsets", "com.coremedia.iso.boxes.ChunkOffset64BitBox", "[J", "chunkOffsets", "", "void"), 28);
   }

   public void _parseDetails(ByteBuffer var1) {
      this.parseVersionAndFlags(var1);
      int var3 = com.googlecode.mp4parser.c.b.a(e.a(var1));
      this.chunkOffsets = new long[var3];

      for(int var2 = 0; var2 < var3; ++var2) {
         this.chunkOffsets[var2] = e.f(var1);
      }

   }

   public long[] getChunkOffsets() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.chunkOffsets;
   }

   protected void getContent(ByteBuffer var1) {
      this.writeVersionAndFlags(var1);
      g.b(var1, (long)this.chunkOffsets.length);
      long[] var4 = this.chunkOffsets;
      int var3 = var4.length;

      for(int var2 = 0; var2 < var3; ++var2) {
         g.a(var1, var4[var2]);
      }

   }

   protected long getContentSize() {
      return (long)(this.chunkOffsets.length * 8 + 8);
   }

   public void setChunkOffsets(long[] var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_1, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.chunkOffsets = var1;
   }
}
