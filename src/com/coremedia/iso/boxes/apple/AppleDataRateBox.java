package com.coremedia.iso.boxes.apple;

import com.coremedia.iso.e;
import com.coremedia.iso.g;
import com.googlecode.mp4parser.AbstractFullBox;
import java.nio.ByteBuffer;
import org.mp4parser.aspectj.a.b.b;
import org.mp4parser.aspectj.lang.a;

public class AppleDataRateBox extends AbstractFullBox {
   public static final String TYPE = "rmdr";
   private static final a.a ajc$tjp_0;
   private long dataRate;

   static {
      ajc$preClinit();
   }

   public AppleDataRateBox() {
      super("rmdr");
   }

   private static void ajc$preClinit() {
      b var0 = new b("AppleDataRateBox.java", AppleDataRateBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getDataRate", "com.coremedia.iso.boxes.apple.AppleDataRateBox", "", "", "", "long"), 53);
   }

   public void _parseDetails(ByteBuffer var1) {
      this.parseVersionAndFlags(var1);
      this.dataRate = e.a(var1);
   }

   protected void getContent(ByteBuffer var1) {
      this.writeVersionAndFlags(var1);
      g.b(var1, this.dataRate);
   }

   protected long getContentSize() {
      return 8L;
   }

   public long getDataRate() {
      a var1 = b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.dataRate;
   }
}
