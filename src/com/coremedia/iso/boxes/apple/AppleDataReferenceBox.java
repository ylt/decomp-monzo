package com.coremedia.iso.boxes.apple;

import com.coremedia.iso.d;
import com.coremedia.iso.e;
import com.coremedia.iso.g;
import com.coremedia.iso.j;
import com.googlecode.mp4parser.AbstractFullBox;
import java.nio.ByteBuffer;
import org.mp4parser.aspectj.a.b.b;
import org.mp4parser.aspectj.lang.a;

public class AppleDataReferenceBox extends AbstractFullBox {
   public static final String TYPE = "rdrf";
   private static final a.a ajc$tjp_0;
   private static final a.a ajc$tjp_1;
   private static final a.a ajc$tjp_2;
   private String dataReference;
   private int dataReferenceSize;
   private String dataReferenceType;

   static {
      ajc$preClinit();
   }

   public AppleDataReferenceBox() {
      super("rdrf");
   }

   private static void ajc$preClinit() {
      b var0 = new b("AppleDataReferenceBox.java", AppleDataReferenceBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getDataReferenceSize", "com.coremedia.iso.boxes.apple.AppleDataReferenceBox", "", "", "", "long"), 63);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "getDataReferenceType", "com.coremedia.iso.boxes.apple.AppleDataReferenceBox", "", "", "", "java.lang.String"), 67);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "getDataReference", "com.coremedia.iso.boxes.apple.AppleDataReferenceBox", "", "", "", "java.lang.String"), 71);
   }

   public void _parseDetails(ByteBuffer var1) {
      this.parseVersionAndFlags(var1);
      this.dataReferenceType = e.k(var1);
      this.dataReferenceSize = com.googlecode.mp4parser.c.b.a(e.a(var1));
      this.dataReference = e.a(var1, this.dataReferenceSize);
   }

   protected void getContent(ByteBuffer var1) {
      this.writeVersionAndFlags(var1);
      var1.put(d.a(this.dataReferenceType));
      g.b(var1, (long)this.dataReferenceSize);
      var1.put(j.a(this.dataReference));
   }

   protected long getContentSize() {
      return (long)(this.dataReferenceSize + 12);
   }

   public String getDataReference() {
      a var1 = b.a(ajc$tjp_2, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.dataReference;
   }

   public long getDataReferenceSize() {
      a var1 = b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return (long)this.dataReferenceSize;
   }

   public String getDataReferenceType() {
      a var1 = b.a(ajc$tjp_1, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.dataReferenceType;
   }
}
