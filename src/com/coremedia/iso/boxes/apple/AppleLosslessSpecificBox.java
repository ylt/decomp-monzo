package com.coremedia.iso.boxes.apple;

import com.coremedia.iso.e;
import com.coremedia.iso.g;
import com.googlecode.mp4parser.AbstractFullBox;
import java.nio.ByteBuffer;
import org.mp4parser.aspectj.a.b.b;
import org.mp4parser.aspectj.lang.a;

public final class AppleLosslessSpecificBox extends AbstractFullBox {
   public static final String TYPE = "alac";
   private static final a.a ajc$tjp_0;
   private static final a.a ajc$tjp_1;
   private static final a.a ajc$tjp_10;
   private static final a.a ajc$tjp_11;
   private static final a.a ajc$tjp_12;
   private static final a.a ajc$tjp_13;
   private static final a.a ajc$tjp_14;
   private static final a.a ajc$tjp_15;
   private static final a.a ajc$tjp_16;
   private static final a.a ajc$tjp_17;
   private static final a.a ajc$tjp_18;
   private static final a.a ajc$tjp_19;
   private static final a.a ajc$tjp_2;
   private static final a.a ajc$tjp_20;
   private static final a.a ajc$tjp_21;
   private static final a.a ajc$tjp_3;
   private static final a.a ajc$tjp_4;
   private static final a.a ajc$tjp_5;
   private static final a.a ajc$tjp_6;
   private static final a.a ajc$tjp_7;
   private static final a.a ajc$tjp_8;
   private static final a.a ajc$tjp_9;
   private long bitRate;
   private int channels;
   private int historyMult;
   private int initialHistory;
   private int kModifier;
   private long maxCodedFrameSize;
   private long maxSamplePerFrame;
   private long sampleRate;
   private int sampleSize;
   private int unknown1;
   private int unknown2;

   static {
      ajc$preClinit();
   }

   public AppleLosslessSpecificBox() {
      super("alac");
   }

   private static void ajc$preClinit() {
      b var0 = new b("AppleLosslessSpecificBox.java", AppleLosslessSpecificBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getMaxSamplePerFrame", "com.coremedia.iso.boxes.apple.AppleLosslessSpecificBox", "", "", "", "long"), 34);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "setMaxSamplePerFrame", "com.coremedia.iso.boxes.apple.AppleLosslessSpecificBox", "int", "maxSamplePerFrame", "", "void"), 38);
      ajc$tjp_10 = var0.a("method-execution", var0.a("1", "getKModifier", "com.coremedia.iso.boxes.apple.AppleLosslessSpecificBox", "", "", "", "int"), 74);
      ajc$tjp_11 = var0.a("method-execution", var0.a("1", "setKModifier", "com.coremedia.iso.boxes.apple.AppleLosslessSpecificBox", "int", "kModifier", "", "void"), 78);
      ajc$tjp_12 = var0.a("method-execution", var0.a("1", "getChannels", "com.coremedia.iso.boxes.apple.AppleLosslessSpecificBox", "", "", "", "int"), 82);
      ajc$tjp_13 = var0.a("method-execution", var0.a("1", "setChannels", "com.coremedia.iso.boxes.apple.AppleLosslessSpecificBox", "int", "channels", "", "void"), 86);
      ajc$tjp_14 = var0.a("method-execution", var0.a("1", "getUnknown2", "com.coremedia.iso.boxes.apple.AppleLosslessSpecificBox", "", "", "", "int"), 90);
      ajc$tjp_15 = var0.a("method-execution", var0.a("1", "setUnknown2", "com.coremedia.iso.boxes.apple.AppleLosslessSpecificBox", "int", "unknown2", "", "void"), 94);
      ajc$tjp_16 = var0.a("method-execution", var0.a("1", "getMaxCodedFrameSize", "com.coremedia.iso.boxes.apple.AppleLosslessSpecificBox", "", "", "", "long"), 98);
      ajc$tjp_17 = var0.a("method-execution", var0.a("1", "setMaxCodedFrameSize", "com.coremedia.iso.boxes.apple.AppleLosslessSpecificBox", "int", "maxCodedFrameSize", "", "void"), 102);
      ajc$tjp_18 = var0.a("method-execution", var0.a("1", "getBitRate", "com.coremedia.iso.boxes.apple.AppleLosslessSpecificBox", "", "", "", "long"), 106);
      ajc$tjp_19 = var0.a("method-execution", var0.a("1", "setBitRate", "com.coremedia.iso.boxes.apple.AppleLosslessSpecificBox", "int", "bitRate", "", "void"), 110);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "getUnknown1", "com.coremedia.iso.boxes.apple.AppleLosslessSpecificBox", "", "", "", "int"), 42);
      ajc$tjp_20 = var0.a("method-execution", var0.a("1", "getSampleRate", "com.coremedia.iso.boxes.apple.AppleLosslessSpecificBox", "", "", "", "long"), 114);
      ajc$tjp_21 = var0.a("method-execution", var0.a("1", "setSampleRate", "com.coremedia.iso.boxes.apple.AppleLosslessSpecificBox", "int", "sampleRate", "", "void"), 118);
      ajc$tjp_3 = var0.a("method-execution", var0.a("1", "setUnknown1", "com.coremedia.iso.boxes.apple.AppleLosslessSpecificBox", "int", "unknown1", "", "void"), 46);
      ajc$tjp_4 = var0.a("method-execution", var0.a("1", "getSampleSize", "com.coremedia.iso.boxes.apple.AppleLosslessSpecificBox", "", "", "", "int"), 50);
      ajc$tjp_5 = var0.a("method-execution", var0.a("1", "setSampleSize", "com.coremedia.iso.boxes.apple.AppleLosslessSpecificBox", "int", "sampleSize", "", "void"), 54);
      ajc$tjp_6 = var0.a("method-execution", var0.a("1", "getHistoryMult", "com.coremedia.iso.boxes.apple.AppleLosslessSpecificBox", "", "", "", "int"), 58);
      ajc$tjp_7 = var0.a("method-execution", var0.a("1", "setHistoryMult", "com.coremedia.iso.boxes.apple.AppleLosslessSpecificBox", "int", "historyMult", "", "void"), 62);
      ajc$tjp_8 = var0.a("method-execution", var0.a("1", "getInitialHistory", "com.coremedia.iso.boxes.apple.AppleLosslessSpecificBox", "", "", "", "int"), 66);
      ajc$tjp_9 = var0.a("method-execution", var0.a("1", "setInitialHistory", "com.coremedia.iso.boxes.apple.AppleLosslessSpecificBox", "int", "initialHistory", "", "void"), 70);
   }

   public void _parseDetails(ByteBuffer var1) {
      this.parseVersionAndFlags(var1);
      this.maxSamplePerFrame = e.a(var1);
      this.unknown1 = e.d(var1);
      this.sampleSize = e.d(var1);
      this.historyMult = e.d(var1);
      this.initialHistory = e.d(var1);
      this.kModifier = e.d(var1);
      this.channels = e.d(var1);
      this.unknown2 = e.c(var1);
      this.maxCodedFrameSize = e.a(var1);
      this.bitRate = e.a(var1);
      this.sampleRate = e.a(var1);
   }

   public long getBitRate() {
      a var1 = b.a(ajc$tjp_18, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.bitRate;
   }

   public int getChannels() {
      a var1 = b.a(ajc$tjp_12, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.channels;
   }

   protected void getContent(ByteBuffer var1) {
      this.writeVersionAndFlags(var1);
      g.b(var1, this.maxSamplePerFrame);
      g.c(var1, this.unknown1);
      g.c(var1, this.sampleSize);
      g.c(var1, this.historyMult);
      g.c(var1, this.initialHistory);
      g.c(var1, this.kModifier);
      g.c(var1, this.channels);
      g.b(var1, this.unknown2);
      g.b(var1, this.maxCodedFrameSize);
      g.b(var1, this.bitRate);
      g.b(var1, this.sampleRate);
   }

   protected long getContentSize() {
      return 28L;
   }

   public int getHistoryMult() {
      a var1 = b.a(ajc$tjp_6, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.historyMult;
   }

   public int getInitialHistory() {
      a var1 = b.a(ajc$tjp_8, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.initialHistory;
   }

   public int getKModifier() {
      a var1 = b.a(ajc$tjp_10, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.kModifier;
   }

   public long getMaxCodedFrameSize() {
      a var1 = b.a(ajc$tjp_16, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.maxCodedFrameSize;
   }

   public long getMaxSamplePerFrame() {
      a var1 = b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.maxSamplePerFrame;
   }

   public long getSampleRate() {
      a var1 = b.a(ajc$tjp_20, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.sampleRate;
   }

   public int getSampleSize() {
      a var1 = b.a(ajc$tjp_4, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.sampleSize;
   }

   public int getUnknown1() {
      a var1 = b.a(ajc$tjp_2, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.unknown1;
   }

   public int getUnknown2() {
      a var1 = b.a(ajc$tjp_14, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.unknown2;
   }

   public void setBitRate(int var1) {
      a var2 = b.a(ajc$tjp_19, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.bitRate = (long)var1;
   }

   public void setChannels(int var1) {
      a var2 = b.a(ajc$tjp_13, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.channels = var1;
   }

   public void setHistoryMult(int var1) {
      a var2 = b.a(ajc$tjp_7, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.historyMult = var1;
   }

   public void setInitialHistory(int var1) {
      a var2 = b.a(ajc$tjp_9, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.initialHistory = var1;
   }

   public void setKModifier(int var1) {
      a var2 = b.a(ajc$tjp_11, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.kModifier = var1;
   }

   public void setMaxCodedFrameSize(int var1) {
      a var2 = b.a(ajc$tjp_17, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.maxCodedFrameSize = (long)var1;
   }

   public void setMaxSamplePerFrame(int var1) {
      a var2 = b.a(ajc$tjp_1, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.maxSamplePerFrame = (long)var1;
   }

   public void setSampleRate(int var1) {
      a var2 = b.a(ajc$tjp_21, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.sampleRate = (long)var1;
   }

   public void setSampleSize(int var1) {
      a var2 = b.a(ajc$tjp_5, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.sampleSize = var1;
   }

   public void setUnknown1(int var1) {
      a var2 = b.a(ajc$tjp_3, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.unknown1 = var1;
   }

   public void setUnknown2(int var1) {
      a var2 = b.a(ajc$tjp_15, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.unknown2 = var1;
   }
}
