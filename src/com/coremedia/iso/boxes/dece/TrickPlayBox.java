package com.coremedia.iso.boxes.dece;

import com.coremedia.iso.e;
import com.coremedia.iso.g;
import com.googlecode.mp4parser.AbstractFullBox;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.mp4parser.aspectj.a.b.b;

public class TrickPlayBox extends AbstractFullBox {
   public static final String TYPE = "trik";
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_0;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_1;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_2;
   private List entries = new ArrayList();

   static {
      ajc$preClinit();
   }

   public TrickPlayBox() {
      super("trik");
   }

   private static void ajc$preClinit() {
      b var0 = new b("TrickPlayBox.java", TrickPlayBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "setEntries", "com.coremedia.iso.boxes.dece.TrickPlayBox", "java.util.List", "entries", "", "void"), 32);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "getEntries", "com.coremedia.iso.boxes.dece.TrickPlayBox", "", "", "", "java.util.List"), 36);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "toString", "com.coremedia.iso.boxes.dece.TrickPlayBox", "", "", "", "java.lang.String"), 103);
   }

   public void _parseDetails(ByteBuffer var1) {
      this.parseVersionAndFlags(var1);

      while(var1.remaining() > 0) {
         this.entries.add(new TrickPlayBox.a(e.d(var1)));
      }

   }

   protected void getContent(ByteBuffer var1) {
      this.writeVersionAndFlags(var1);
      Iterator var2 = this.entries.iterator();

      while(var2.hasNext()) {
         g.c(var1, ((TrickPlayBox.a)var2.next()).a);
      }

   }

   protected long getContentSize() {
      return (long)(this.entries.size() + 4);
   }

   public List getEntries() {
      org.mp4parser.aspectj.lang.a var1 = b.a(ajc$tjp_1, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.entries;
   }

   public void setEntries(List var1) {
      org.mp4parser.aspectj.lang.a var2 = b.a(ajc$tjp_0, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.entries = var1;
   }

   public String toString() {
      org.mp4parser.aspectj.lang.a var1 = b.a(ajc$tjp_2, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      StringBuilder var2 = new StringBuilder();
      var2.append("TrickPlayBox");
      var2.append("{entries=").append(this.entries);
      var2.append('}');
      return var2.toString();
   }

   public static class a {
      private int a;

      public a() {
      }

      public a(int var1) {
         this.a = var1;
      }

      public int a() {
         return this.a >> 6 & 3;
      }

      public int b() {
         return this.a & 63;
      }

      public String toString() {
         StringBuilder var1 = new StringBuilder();
         var1.append("Entry");
         var1.append("{picType=").append(this.a());
         var1.append(",dependencyLevel=").append(this.b());
         var1.append('}');
         return var1.toString();
      }
   }
}
