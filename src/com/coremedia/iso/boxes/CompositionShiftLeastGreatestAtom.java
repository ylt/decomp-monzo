package com.coremedia.iso.boxes;

import com.googlecode.mp4parser.AbstractFullBox;
import com.googlecode.mp4parser.e;
import java.nio.ByteBuffer;

public class CompositionShiftLeastGreatestAtom extends AbstractFullBox {
   public static final String TYPE = "cslg";
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_0;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_1;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_2;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_3;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_4;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_5;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_6;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_7;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_8;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_9;
   int compositionOffsetToDisplayOffsetShift;
   int displayEndTime;
   int displayStartTime;
   int greatestDisplayOffset;
   int leastDisplayOffset;

   static {
      ajc$preClinit();
   }

   public CompositionShiftLeastGreatestAtom() {
      super("cslg");
   }

   private static void ajc$preClinit() {
      org.mp4parser.aspectj.a.b.b var0 = new org.mp4parser.aspectj.a.b.b("CompositionShiftLeastGreatestAtom.java", CompositionShiftLeastGreatestAtom.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getCompositionOffsetToDisplayOffsetShift", "com.coremedia.iso.boxes.CompositionShiftLeastGreatestAtom", "", "", "", "int"), 66);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "setCompositionOffsetToDisplayOffsetShift", "com.coremedia.iso.boxes.CompositionShiftLeastGreatestAtom", "int", "compositionOffsetToDisplayOffsetShift", "", "void"), 70);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "getLeastDisplayOffset", "com.coremedia.iso.boxes.CompositionShiftLeastGreatestAtom", "", "", "", "int"), 74);
      ajc$tjp_3 = var0.a("method-execution", var0.a("1", "setLeastDisplayOffset", "com.coremedia.iso.boxes.CompositionShiftLeastGreatestAtom", "int", "leastDisplayOffset", "", "void"), 78);
      ajc$tjp_4 = var0.a("method-execution", var0.a("1", "getGreatestDisplayOffset", "com.coremedia.iso.boxes.CompositionShiftLeastGreatestAtom", "", "", "", "int"), 82);
      ajc$tjp_5 = var0.a("method-execution", var0.a("1", "setGreatestDisplayOffset", "com.coremedia.iso.boxes.CompositionShiftLeastGreatestAtom", "int", "greatestDisplayOffset", "", "void"), 86);
      ajc$tjp_6 = var0.a("method-execution", var0.a("1", "getDisplayStartTime", "com.coremedia.iso.boxes.CompositionShiftLeastGreatestAtom", "", "", "", "int"), 90);
      ajc$tjp_7 = var0.a("method-execution", var0.a("1", "setDisplayStartTime", "com.coremedia.iso.boxes.CompositionShiftLeastGreatestAtom", "int", "displayStartTime", "", "void"), 94);
      ajc$tjp_8 = var0.a("method-execution", var0.a("1", "getDisplayEndTime", "com.coremedia.iso.boxes.CompositionShiftLeastGreatestAtom", "", "", "", "int"), 98);
      ajc$tjp_9 = var0.a("method-execution", var0.a("1", "setDisplayEndTime", "com.coremedia.iso.boxes.CompositionShiftLeastGreatestAtom", "int", "displayEndTime", "", "void"), 102);
   }

   public void _parseDetails(ByteBuffer var1) {
      this.parseVersionAndFlags(var1);
      this.compositionOffsetToDisplayOffsetShift = var1.getInt();
      this.leastDisplayOffset = var1.getInt();
      this.greatestDisplayOffset = var1.getInt();
      this.displayStartTime = var1.getInt();
      this.displayEndTime = var1.getInt();
   }

   public int getCompositionOffsetToDisplayOffsetShift() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_0, this, this);
      e.a().a(var1);
      return this.compositionOffsetToDisplayOffsetShift;
   }

   protected void getContent(ByteBuffer var1) {
      this.writeVersionAndFlags(var1);
      var1.putInt(this.compositionOffsetToDisplayOffsetShift);
      var1.putInt(this.leastDisplayOffset);
      var1.putInt(this.greatestDisplayOffset);
      var1.putInt(this.displayStartTime);
      var1.putInt(this.displayEndTime);
   }

   protected long getContentSize() {
      return 24L;
   }

   public int getDisplayEndTime() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_8, this, this);
      e.a().a(var1);
      return this.displayEndTime;
   }

   public int getDisplayStartTime() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_6, this, this);
      e.a().a(var1);
      return this.displayStartTime;
   }

   public int getGreatestDisplayOffset() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_4, this, this);
      e.a().a(var1);
      return this.greatestDisplayOffset;
   }

   public int getLeastDisplayOffset() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_2, this, this);
      e.a().a(var1);
      return this.leastDisplayOffset;
   }

   public void setCompositionOffsetToDisplayOffsetShift(int var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_1, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      e.a().a(var2);
      this.compositionOffsetToDisplayOffsetShift = var1;
   }

   public void setDisplayEndTime(int var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_9, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      e.a().a(var2);
      this.displayEndTime = var1;
   }

   public void setDisplayStartTime(int var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_7, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      e.a().a(var2);
      this.displayStartTime = var1;
   }

   public void setGreatestDisplayOffset(int var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_5, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      e.a().a(var2);
      this.greatestDisplayOffset = var1;
   }

   public void setLeastDisplayOffset(int var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_3, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      e.a().a(var2);
      this.leastDisplayOffset = var1;
   }
}
