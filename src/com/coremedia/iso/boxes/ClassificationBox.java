package com.coremedia.iso.boxes;

import com.coremedia.iso.d;
import com.coremedia.iso.e;
import com.coremedia.iso.g;
import com.coremedia.iso.j;
import com.googlecode.mp4parser.AbstractFullBox;
import java.nio.ByteBuffer;

public class ClassificationBox extends AbstractFullBox {
   public static final String TYPE = "clsf";
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_0;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_1;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_2;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_3;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_4;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_5;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_6;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_7;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_8;
   private String classificationEntity;
   private String classificationInfo;
   private int classificationTableIndex;
   private String language;

   static {
      ajc$preClinit();
   }

   public ClassificationBox() {
      super("clsf");
   }

   private static void ajc$preClinit() {
      org.mp4parser.aspectj.a.b.b var0 = new org.mp4parser.aspectj.a.b.b("ClassificationBox.java", ClassificationBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getLanguage", "com.coremedia.iso.boxes.ClassificationBox", "", "", "", "java.lang.String"), 44);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "getClassificationEntity", "com.coremedia.iso.boxes.ClassificationBox", "", "", "", "java.lang.String"), 48);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "getClassificationTableIndex", "com.coremedia.iso.boxes.ClassificationBox", "", "", "", "int"), 52);
      ajc$tjp_3 = var0.a("method-execution", var0.a("1", "getClassificationInfo", "com.coremedia.iso.boxes.ClassificationBox", "", "", "", "java.lang.String"), 56);
      ajc$tjp_4 = var0.a("method-execution", var0.a("1", "setClassificationEntity", "com.coremedia.iso.boxes.ClassificationBox", "java.lang.String", "classificationEntity", "", "void"), 60);
      ajc$tjp_5 = var0.a("method-execution", var0.a("1", "setClassificationTableIndex", "com.coremedia.iso.boxes.ClassificationBox", "int", "classificationTableIndex", "", "void"), 64);
      ajc$tjp_6 = var0.a("method-execution", var0.a("1", "setLanguage", "com.coremedia.iso.boxes.ClassificationBox", "java.lang.String", "language", "", "void"), 68);
      ajc$tjp_7 = var0.a("method-execution", var0.a("1", "setClassificationInfo", "com.coremedia.iso.boxes.ClassificationBox", "java.lang.String", "classificationInfo", "", "void"), 72);
      ajc$tjp_8 = var0.a("method-execution", var0.a("1", "toString", "com.coremedia.iso.boxes.ClassificationBox", "", "", "", "java.lang.String"), 101);
   }

   public void _parseDetails(ByteBuffer var1) {
      this.parseVersionAndFlags(var1);
      byte[] var2 = new byte[4];
      var1.get(var2);
      this.classificationEntity = d.a(var2);
      this.classificationTableIndex = e.c(var1);
      this.language = e.j(var1);
      this.classificationInfo = e.e(var1);
   }

   public String getClassificationEntity() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_1, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.classificationEntity;
   }

   public String getClassificationInfo() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_3, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.classificationInfo;
   }

   public int getClassificationTableIndex() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_2, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.classificationTableIndex;
   }

   protected void getContent(ByteBuffer var1) {
      var1.put(d.a(this.classificationEntity));
      g.b(var1, this.classificationTableIndex);
      g.a(var1, this.language);
      var1.put(j.a(this.classificationInfo));
      var1.put(0);
   }

   protected long getContentSize() {
      return (long)(j.b(this.classificationInfo) + 8 + 1);
   }

   public String getLanguage() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.language;
   }

   public void setClassificationEntity(String var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_4, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.classificationEntity = var1;
   }

   public void setClassificationInfo(String var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_7, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.classificationInfo = var1;
   }

   public void setClassificationTableIndex(int var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_5, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.classificationTableIndex = var1;
   }

   public void setLanguage(String var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_6, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.language = var1;
   }

   public String toString() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_8, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      StringBuilder var2 = new StringBuilder();
      var2.append("ClassificationBox[language=").append(this.getLanguage());
      var2.append("classificationEntity=").append(this.getClassificationEntity());
      var2.append(";classificationTableIndex=").append(this.getClassificationTableIndex());
      var2.append(";language=").append(this.getLanguage());
      var2.append(";classificationInfo=").append(this.getClassificationInfo());
      var2.append("]");
      return var2.toString();
   }
}
