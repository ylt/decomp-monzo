package com.coremedia.iso.boxes;

import com.googlecode.mp4parser.AbstractContainerBox;
import java.util.Iterator;

public class SampleTableBox extends AbstractContainerBox {
   public static final String TYPE = "stbl";
   private SampleToChunkBox sampleToChunkBox;

   public SampleTableBox() {
      super("stbl");
   }

   public ChunkOffsetBox getChunkOffsetBox() {
      Iterator var2 = this.getBoxes().iterator();

      ChunkOffsetBox var3;
      while(true) {
         if(!var2.hasNext()) {
            var3 = null;
            break;
         }

         a var1 = (a)var2.next();
         if(var1 instanceof ChunkOffsetBox) {
            var3 = (ChunkOffsetBox)var1;
            break;
         }
      }

      return var3;
   }

   public CompositionTimeToSample getCompositionTimeToSample() {
      Iterator var2 = this.getBoxes().iterator();

      CompositionTimeToSample var3;
      while(true) {
         if(!var2.hasNext()) {
            var3 = null;
            break;
         }

         a var1 = (a)var2.next();
         if(var1 instanceof CompositionTimeToSample) {
            var3 = (CompositionTimeToSample)var1;
            break;
         }
      }

      return var3;
   }

   public SampleDependencyTypeBox getSampleDependencyTypeBox() {
      Iterator var2 = this.getBoxes().iterator();

      SampleDependencyTypeBox var3;
      while(true) {
         if(!var2.hasNext()) {
            var3 = null;
            break;
         }

         a var1 = (a)var2.next();
         if(var1 instanceof SampleDependencyTypeBox) {
            var3 = (SampleDependencyTypeBox)var1;
            break;
         }
      }

      return var3;
   }

   public SampleDescriptionBox getSampleDescriptionBox() {
      Iterator var1 = this.getBoxes().iterator();

      SampleDescriptionBox var3;
      while(true) {
         if(!var1.hasNext()) {
            var3 = null;
            break;
         }

         a var2 = (a)var1.next();
         if(var2 instanceof SampleDescriptionBox) {
            var3 = (SampleDescriptionBox)var2;
            break;
         }
      }

      return var3;
   }

   public SampleSizeBox getSampleSizeBox() {
      Iterator var2 = this.getBoxes().iterator();

      SampleSizeBox var3;
      while(true) {
         if(!var2.hasNext()) {
            var3 = null;
            break;
         }

         a var1 = (a)var2.next();
         if(var1 instanceof SampleSizeBox) {
            var3 = (SampleSizeBox)var1;
            break;
         }
      }

      return var3;
   }

   public SampleToChunkBox getSampleToChunkBox() {
      SampleToChunkBox var1;
      if(this.sampleToChunkBox != null) {
         var1 = this.sampleToChunkBox;
      } else {
         Iterator var2 = this.getBoxes().iterator();

         while(var2.hasNext()) {
            a var3 = (a)var2.next();
            if(var3 instanceof SampleToChunkBox) {
               this.sampleToChunkBox = (SampleToChunkBox)var3;
               var1 = this.sampleToChunkBox;
               return var1;
            }
         }

         var1 = null;
      }

      return var1;
   }

   public SyncSampleBox getSyncSampleBox() {
      Iterator var1 = this.getBoxes().iterator();

      SyncSampleBox var3;
      while(true) {
         if(!var1.hasNext()) {
            var3 = null;
            break;
         }

         a var2 = (a)var1.next();
         if(var2 instanceof SyncSampleBox) {
            var3 = (SyncSampleBox)var2;
            break;
         }
      }

      return var3;
   }

   public TimeToSampleBox getTimeToSampleBox() {
      Iterator var1 = this.getBoxes().iterator();

      TimeToSampleBox var3;
      while(true) {
         if(!var1.hasNext()) {
            var3 = null;
            break;
         }

         a var2 = (a)var1.next();
         if(var2 instanceof TimeToSampleBox) {
            var3 = (TimeToSampleBox)var2;
            break;
         }
      }

      return var3;
   }
}
