package com.coremedia.iso.boxes;

import com.coremedia.iso.e;
import com.coremedia.iso.g;
import java.nio.ByteBuffer;

public class VideoMediaHeaderBox extends AbstractMediaHeaderBox {
   public static final String TYPE = "vmhd";
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_0;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_1;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_2;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_3;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_4;
   private int graphicsmode = 0;
   private int[] opcolor = new int[3];

   static {
      ajc$preClinit();
   }

   public VideoMediaHeaderBox() {
      super("vmhd");
      this.setFlags(1);
   }

   private static void ajc$preClinit() {
      org.mp4parser.aspectj.a.b.b var0 = new org.mp4parser.aspectj.a.b.b("VideoMediaHeaderBox.java", VideoMediaHeaderBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getGraphicsmode", "com.coremedia.iso.boxes.VideoMediaHeaderBox", "", "", "", "int"), 39);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "getOpcolor", "com.coremedia.iso.boxes.VideoMediaHeaderBox", "", "", "", "[I"), 43);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "toString", "com.coremedia.iso.boxes.VideoMediaHeaderBox", "", "", "", "java.lang.String"), 71);
      ajc$tjp_3 = var0.a("method-execution", var0.a("1", "setOpcolor", "com.coremedia.iso.boxes.VideoMediaHeaderBox", "[I", "opcolor", "", "void"), 75);
      ajc$tjp_4 = var0.a("method-execution", var0.a("1", "setGraphicsmode", "com.coremedia.iso.boxes.VideoMediaHeaderBox", "int", "graphicsmode", "", "void"), 79);
   }

   public void _parseDetails(ByteBuffer var1) {
      this.parseVersionAndFlags(var1);
      this.graphicsmode = e.c(var1);
      this.opcolor = new int[3];

      for(int var2 = 0; var2 < 3; ++var2) {
         this.opcolor[var2] = e.c(var1);
      }

   }

   protected void getContent(ByteBuffer var1) {
      this.writeVersionAndFlags(var1);
      g.b(var1, this.graphicsmode);
      int[] var4 = this.opcolor;
      int var3 = var4.length;

      for(int var2 = 0; var2 < var3; ++var2) {
         g.b(var1, var4[var2]);
      }

   }

   protected long getContentSize() {
      return 12L;
   }

   public int getGraphicsmode() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.graphicsmode;
   }

   public int[] getOpcolor() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_1, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.opcolor;
   }

   public void setGraphicsmode(int var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_4, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.graphicsmode = var1;
   }

   public void setOpcolor(int[] var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_3, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.opcolor = var1;
   }

   public String toString() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_2, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return "VideoMediaHeaderBox[graphicsmode=" + this.getGraphicsmode() + ";opcolor0=" + this.getOpcolor()[0] + ";opcolor1=" + this.getOpcolor()[1] + ";opcolor2=" + this.getOpcolor()[2] + "]";
   }
}
