package com.coremedia.iso.boxes;

import com.googlecode.mp4parser.AbstractContainerBox;
import java.util.Iterator;

public class MediaBox extends AbstractContainerBox {
   public static final String TYPE = "mdia";

   public MediaBox() {
      super("mdia");
   }

   public HandlerBox getHandlerBox() {
      Iterator var1 = this.getBoxes().iterator();

      HandlerBox var3;
      while(true) {
         if(!var1.hasNext()) {
            var3 = null;
            break;
         }

         a var2 = (a)var1.next();
         if(var2 instanceof HandlerBox) {
            var3 = (HandlerBox)var2;
            break;
         }
      }

      return var3;
   }

   public MediaHeaderBox getMediaHeaderBox() {
      Iterator var2 = this.getBoxes().iterator();

      MediaHeaderBox var3;
      while(true) {
         if(!var2.hasNext()) {
            var3 = null;
            break;
         }

         a var1 = (a)var2.next();
         if(var1 instanceof MediaHeaderBox) {
            var3 = (MediaHeaderBox)var1;
            break;
         }
      }

      return var3;
   }

   public MediaInformationBox getMediaInformationBox() {
      Iterator var2 = this.getBoxes().iterator();

      MediaInformationBox var3;
      while(true) {
         if(!var2.hasNext()) {
            var3 = null;
            break;
         }

         a var1 = (a)var2.next();
         if(var1 instanceof MediaInformationBox) {
            var3 = (MediaInformationBox)var1;
            break;
         }
      }

      return var3;
   }
}
