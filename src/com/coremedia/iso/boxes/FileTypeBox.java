package com.coremedia.iso.boxes;

import com.coremedia.iso.d;
import com.coremedia.iso.e;
import com.coremedia.iso.g;
import com.googlecode.mp4parser.AbstractBox;
import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class FileTypeBox extends AbstractBox {
   public static final String TYPE = "ftyp";
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_0;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_1;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_2;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_3;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_4;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_5;
   private List compatibleBrands = Collections.emptyList();
   private String majorBrand;
   private long minorVersion;

   static {
      ajc$preClinit();
   }

   public FileTypeBox() {
      super("ftyp");
   }

   public FileTypeBox(String var1, long var2, List var4) {
      super("ftyp");
      this.majorBrand = var1;
      this.minorVersion = var2;
      this.compatibleBrands = var4;
   }

   private static void ajc$preClinit() {
      org.mp4parser.aspectj.a.b.b var0 = new org.mp4parser.aspectj.a.b.b("FileTypeBox.java", FileTypeBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getMajorBrand", "com.coremedia.iso.boxes.FileTypeBox", "", "", "", "java.lang.String"), 85);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "setMajorBrand", "com.coremedia.iso.boxes.FileTypeBox", "java.lang.String", "majorBrand", "", "void"), 94);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "setMinorVersion", "com.coremedia.iso.boxes.FileTypeBox", "long", "minorVersion", "", "void"), 103);
      ajc$tjp_3 = var0.a("method-execution", var0.a("1", "getMinorVersion", "com.coremedia.iso.boxes.FileTypeBox", "", "", "", "long"), 113);
      ajc$tjp_4 = var0.a("method-execution", var0.a("1", "getCompatibleBrands", "com.coremedia.iso.boxes.FileTypeBox", "", "", "", "java.util.List"), 122);
      ajc$tjp_5 = var0.a("method-execution", var0.a("1", "setCompatibleBrands", "com.coremedia.iso.boxes.FileTypeBox", "java.util.List", "compatibleBrands", "", "void"), 126);
   }

   public void _parseDetails(ByteBuffer var1) {
      this.majorBrand = e.k(var1);
      this.minorVersion = e.a(var1);
      int var3 = var1.remaining() / 4;
      this.compatibleBrands = new LinkedList();

      for(int var2 = 0; var2 < var3; ++var2) {
         this.compatibleBrands.add(e.k(var1));
      }

   }

   public List getCompatibleBrands() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_4, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.compatibleBrands;
   }

   protected void getContent(ByteBuffer var1) {
      var1.put(d.a(this.majorBrand));
      g.b(var1, this.minorVersion);
      Iterator var2 = this.compatibleBrands.iterator();

      while(var2.hasNext()) {
         var1.put(d.a((String)var2.next()));
      }

   }

   protected long getContentSize() {
      return (long)(this.compatibleBrands.size() * 4 + 8);
   }

   public String getMajorBrand() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.majorBrand;
   }

   public long getMinorVersion() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_3, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.minorVersion;
   }

   public void setCompatibleBrands(List var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_5, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.compatibleBrands = var1;
   }

   public void setMajorBrand(String var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_1, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.majorBrand = var1;
   }

   public void setMinorVersion(long var1) {
      org.mp4parser.aspectj.lang.a var3 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_2, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var3);
      this.minorVersion = var1;
   }

   public String toString() {
      StringBuilder var1 = new StringBuilder();
      var1.append("FileTypeBox[");
      var1.append("majorBrand=").append(this.getMajorBrand());
      var1.append(";");
      var1.append("minorVersion=").append(this.getMinorVersion());
      Iterator var2 = this.compatibleBrands.iterator();

      while(var2.hasNext()) {
         String var3 = (String)var2.next();
         var1.append(";");
         var1.append("compatibleBrand=").append(var3);
      }

      var1.append("]");
      return var1.toString();
   }
}
