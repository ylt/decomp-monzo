package com.coremedia.iso.boxes;

import com.googlecode.mp4parser.AbstractBox;
import com.googlecode.mp4parser.e;
import java.nio.ByteBuffer;

public class UserBox extends AbstractBox {
   public static final String TYPE = "uuid";
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_0;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_1;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_2;
   byte[] data;

   static {
      ajc$preClinit();
   }

   public UserBox(byte[] var1) {
      super("uuid", var1);
   }

   private static void ajc$preClinit() {
      org.mp4parser.aspectj.a.b.b var0 = new org.mp4parser.aspectj.a.b.b("UserBox.java", UserBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "toString", "com.coremedia.iso.boxes.UserBox", "", "", "", "java.lang.String"), 40);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "getData", "com.coremedia.iso.boxes.UserBox", "", "", "", "[B"), 47);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "setData", "com.coremedia.iso.boxes.UserBox", "[B", "data", "", "void"), 51);
   }

   public void _parseDetails(ByteBuffer var1) {
      this.data = new byte[var1.remaining()];
      var1.get(this.data);
   }

   protected void getContent(ByteBuffer var1) {
      var1.put(this.data);
   }

   protected long getContentSize() {
      return (long)this.data.length;
   }

   public byte[] getData() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_1, this, this);
      e.a().a(var1);
      return this.data;
   }

   public void setData(byte[] var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_2, this, this, (Object)var1);
      e.a().a(var2);
      this.data = var1;
   }

   public String toString() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_0, this, this);
      e.a().a(var1);
      return "UserBox[type=" + this.getType() + ";userType=" + new String(this.getUserType()) + ";contentLength=" + this.data.length + "]";
   }
}
