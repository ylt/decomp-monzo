package com.coremedia.iso.boxes;

import com.coremedia.iso.e;
import com.coremedia.iso.g;
import com.googlecode.mp4parser.AbstractFullBox;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class SampleDependencyTypeBox extends AbstractFullBox {
   public static final String TYPE = "sdtp";
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_0;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_1;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_2;
   private List entries = new ArrayList();

   static {
      ajc$preClinit();
   }

   public SampleDependencyTypeBox() {
      super("sdtp");
   }

   private static void ajc$preClinit() {
      org.mp4parser.aspectj.a.b.b var0 = new org.mp4parser.aspectj.a.b.b("SampleDependencyTypeBox.java", SampleDependencyTypeBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getEntries", "com.coremedia.iso.boxes.SampleDependencyTypeBox", "", "", "", "java.util.List"), 139);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "setEntries", "com.coremedia.iso.boxes.SampleDependencyTypeBox", "java.util.List", "entries", "", "void"), 143);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "toString", "com.coremedia.iso.boxes.SampleDependencyTypeBox", "", "", "", "java.lang.String"), 148);
   }

   public void _parseDetails(ByteBuffer var1) {
      this.parseVersionAndFlags(var1);

      while(var1.remaining() > 0) {
         this.entries.add(new SampleDependencyTypeBox.a(e.d(var1)));
      }

   }

   protected void getContent(ByteBuffer var1) {
      this.writeVersionAndFlags(var1);
      Iterator var2 = this.entries.iterator();

      while(var2.hasNext()) {
         g.c(var1, ((SampleDependencyTypeBox.a)var2.next()).a);
      }

   }

   protected long getContentSize() {
      return (long)(this.entries.size() + 4);
   }

   public List getEntries() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.entries;
   }

   public void setEntries(List var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_1, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.entries = var1;
   }

   public String toString() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_2, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      StringBuilder var2 = new StringBuilder();
      var2.append("SampleDependencyTypeBox");
      var2.append("{entries=").append(this.entries);
      var2.append('}');
      return var2.toString();
   }

   public static class a {
      private int a;

      public a(int var1) {
         this.a = var1;
      }

      public int a() {
         return this.a >> 6 & 3;
      }

      public int b() {
         return this.a >> 4 & 3;
      }

      public int c() {
         return this.a >> 2 & 3;
      }

      public int d() {
         return this.a & 3;
      }

      public boolean equals(Object var1) {
         boolean var2 = true;
         if(this != var1) {
            if(var1 != null && this.getClass() == var1.getClass()) {
               SampleDependencyTypeBox.a var3 = (SampleDependencyTypeBox.a)var1;
               if(this.a != var3.a) {
                  var2 = false;
               }
            } else {
               var2 = false;
            }
         }

         return var2;
      }

      public int hashCode() {
         return this.a;
      }

      public String toString() {
         return "Entry{isLeading=" + this.a() + ", sampleDependsOn=" + this.b() + ", sampleIsDependentOn=" + this.c() + ", sampleHasRedundancy=" + this.d() + '}';
      }
   }
}
