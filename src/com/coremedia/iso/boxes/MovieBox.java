package com.coremedia.iso.boxes;

import com.googlecode.mp4parser.AbstractContainerBox;
import java.util.Iterator;
import java.util.List;

public class MovieBox extends AbstractContainerBox {
   public static final String TYPE = "moov";

   public MovieBox() {
      super("moov");
   }

   public MovieHeaderBox getMovieHeaderBox() {
      Iterator var2 = this.getBoxes().iterator();

      MovieHeaderBox var3;
      while(true) {
         if(!var2.hasNext()) {
            var3 = null;
            break;
         }

         a var1 = (a)var2.next();
         if(var1 instanceof MovieHeaderBox) {
            var3 = (MovieHeaderBox)var1;
            break;
         }
      }

      return var3;
   }

   public int getTrackCount() {
      return this.getBoxes(TrackBox.class).size();
   }

   public long[] getTrackNumbers() {
      List var2 = this.getBoxes(TrackBox.class);
      long[] var3 = new long[var2.size()];

      for(int var1 = 0; var1 < var2.size(); ++var1) {
         var3[var1] = ((TrackBox)var2.get(var1)).getTrackHeaderBox().getTrackId();
      }

      return var3;
   }
}
