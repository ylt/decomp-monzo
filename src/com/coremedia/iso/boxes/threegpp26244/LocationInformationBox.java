package com.coremedia.iso.boxes.threegpp26244;

import com.coremedia.iso.e;
import com.coremedia.iso.g;
import com.coremedia.iso.j;
import com.googlecode.mp4parser.AbstractFullBox;
import java.nio.ByteBuffer;
import org.mp4parser.aspectj.a.b.b;
import org.mp4parser.aspectj.lang.a;

public class LocationInformationBox extends AbstractFullBox {
   public static final String TYPE = "loci";
   private static final a.a ajc$tjp_0;
   private static final a.a ajc$tjp_1;
   private static final a.a ajc$tjp_10;
   private static final a.a ajc$tjp_11;
   private static final a.a ajc$tjp_12;
   private static final a.a ajc$tjp_13;
   private static final a.a ajc$tjp_14;
   private static final a.a ajc$tjp_15;
   private static final a.a ajc$tjp_2;
   private static final a.a ajc$tjp_3;
   private static final a.a ajc$tjp_4;
   private static final a.a ajc$tjp_5;
   private static final a.a ajc$tjp_6;
   private static final a.a ajc$tjp_7;
   private static final a.a ajc$tjp_8;
   private static final a.a ajc$tjp_9;
   private String additionalNotes = "";
   private double altitude;
   private String astronomicalBody = "";
   private String language;
   private double latitude;
   private double longitude;
   private String name = "";
   private int role;

   static {
      ajc$preClinit();
   }

   public LocationInformationBox() {
      super("loci");
   }

   private static void ajc$preClinit() {
      b var0 = new b("LocationInformationBox.java", LocationInformationBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getLanguage", "com.coremedia.iso.boxes.threegpp26244.LocationInformationBox", "", "", "", "java.lang.String"), 30);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "setLanguage", "com.coremedia.iso.boxes.threegpp26244.LocationInformationBox", "java.lang.String", "language", "", "void"), 34);
      ajc$tjp_10 = var0.a("method-execution", var0.a("1", "getAltitude", "com.coremedia.iso.boxes.threegpp26244.LocationInformationBox", "", "", "", "double"), 70);
      ajc$tjp_11 = var0.a("method-execution", var0.a("1", "setAltitude", "com.coremedia.iso.boxes.threegpp26244.LocationInformationBox", "double", "altitude", "", "void"), 74);
      ajc$tjp_12 = var0.a("method-execution", var0.a("1", "getAstronomicalBody", "com.coremedia.iso.boxes.threegpp26244.LocationInformationBox", "", "", "", "java.lang.String"), 78);
      ajc$tjp_13 = var0.a("method-execution", var0.a("1", "setAstronomicalBody", "com.coremedia.iso.boxes.threegpp26244.LocationInformationBox", "java.lang.String", "astronomicalBody", "", "void"), 82);
      ajc$tjp_14 = var0.a("method-execution", var0.a("1", "getAdditionalNotes", "com.coremedia.iso.boxes.threegpp26244.LocationInformationBox", "", "", "", "java.lang.String"), 86);
      ajc$tjp_15 = var0.a("method-execution", var0.a("1", "setAdditionalNotes", "com.coremedia.iso.boxes.threegpp26244.LocationInformationBox", "java.lang.String", "additionalNotes", "", "void"), 90);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "getName", "com.coremedia.iso.boxes.threegpp26244.LocationInformationBox", "", "", "", "java.lang.String"), 38);
      ajc$tjp_3 = var0.a("method-execution", var0.a("1", "setName", "com.coremedia.iso.boxes.threegpp26244.LocationInformationBox", "java.lang.String", "name", "", "void"), 42);
      ajc$tjp_4 = var0.a("method-execution", var0.a("1", "getRole", "com.coremedia.iso.boxes.threegpp26244.LocationInformationBox", "", "", "", "int"), 46);
      ajc$tjp_5 = var0.a("method-execution", var0.a("1", "setRole", "com.coremedia.iso.boxes.threegpp26244.LocationInformationBox", "int", "role", "", "void"), 50);
      ajc$tjp_6 = var0.a("method-execution", var0.a("1", "getLongitude", "com.coremedia.iso.boxes.threegpp26244.LocationInformationBox", "", "", "", "double"), 54);
      ajc$tjp_7 = var0.a("method-execution", var0.a("1", "setLongitude", "com.coremedia.iso.boxes.threegpp26244.LocationInformationBox", "double", "longitude", "", "void"), 58);
      ajc$tjp_8 = var0.a("method-execution", var0.a("1", "getLatitude", "com.coremedia.iso.boxes.threegpp26244.LocationInformationBox", "", "", "", "double"), 62);
      ajc$tjp_9 = var0.a("method-execution", var0.a("1", "setLatitude", "com.coremedia.iso.boxes.threegpp26244.LocationInformationBox", "double", "latitude", "", "void"), 66);
   }

   public void _parseDetails(ByteBuffer var1) {
      this.parseVersionAndFlags(var1);
      this.language = e.j(var1);
      this.name = e.e(var1);
      this.role = e.d(var1);
      this.longitude = e.g(var1);
      this.latitude = e.g(var1);
      this.altitude = e.g(var1);
      this.astronomicalBody = e.e(var1);
      this.additionalNotes = e.e(var1);
   }

   public String getAdditionalNotes() {
      a var1 = b.a(ajc$tjp_14, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.additionalNotes;
   }

   public double getAltitude() {
      a var1 = b.a(ajc$tjp_10, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.altitude;
   }

   public String getAstronomicalBody() {
      a var1 = b.a(ajc$tjp_12, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.astronomicalBody;
   }

   protected void getContent(ByteBuffer var1) {
      this.writeVersionAndFlags(var1);
      g.a(var1, this.language);
      var1.put(j.a(this.name));
      var1.put(0);
      g.c(var1, this.role);
      g.a(var1, this.longitude);
      g.a(var1, this.latitude);
      g.a(var1, this.altitude);
      var1.put(j.a(this.astronomicalBody));
      var1.put(0);
      var1.put(j.a(this.additionalNotes));
      var1.put(0);
   }

   protected long getContentSize() {
      return (long)(j.a(this.name).length + 22 + j.a(this.astronomicalBody).length + j.a(this.additionalNotes).length);
   }

   public String getLanguage() {
      a var1 = b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.language;
   }

   public double getLatitude() {
      a var1 = b.a(ajc$tjp_8, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.latitude;
   }

   public double getLongitude() {
      a var1 = b.a(ajc$tjp_6, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.longitude;
   }

   public String getName() {
      a var1 = b.a(ajc$tjp_2, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.name;
   }

   public int getRole() {
      a var1 = b.a(ajc$tjp_4, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.role;
   }

   public void setAdditionalNotes(String var1) {
      a var2 = b.a(ajc$tjp_15, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.additionalNotes = var1;
   }

   public void setAltitude(double var1) {
      a var3 = b.a(ajc$tjp_11, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var3);
      this.altitude = var1;
   }

   public void setAstronomicalBody(String var1) {
      a var2 = b.a(ajc$tjp_13, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.astronomicalBody = var1;
   }

   public void setLanguage(String var1) {
      a var2 = b.a(ajc$tjp_1, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.language = var1;
   }

   public void setLatitude(double var1) {
      a var3 = b.a(ajc$tjp_9, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var3);
      this.latitude = var1;
   }

   public void setLongitude(double var1) {
      a var3 = b.a(ajc$tjp_7, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var3);
      this.longitude = var1;
   }

   public void setName(String var1) {
      a var2 = b.a(ajc$tjp_3, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.name = var1;
   }

   public void setRole(int var1) {
      a var2 = b.a(ajc$tjp_5, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.role = var1;
   }
}
