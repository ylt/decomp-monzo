package com.coremedia.iso.boxes;

import com.coremedia.iso.e;
import com.coremedia.iso.g;
import com.googlecode.mp4parser.AbstractContainerBox;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;

public class ItemProtectionBox extends AbstractContainerBox implements FullBox {
   public static final String TYPE = "ipro";
   private int flags;
   private int version;

   public ItemProtectionBox() {
      super("ipro");
   }

   public void getBox(WritableByteChannel var1) throws IOException {
      var1.write(this.getHeader());
      ByteBuffer var2 = ByteBuffer.allocate(6);
      g.c(var2, this.version);
      g.a(var2, this.flags);
      g.b(var2, this.getBoxes().size());
      var1.write((ByteBuffer)var2.rewind());
      this.writeContainer(var1);
   }

   public int getFlags() {
      return this.flags;
   }

   public SchemeInformationBox getItemProtectionScheme() {
      SchemeInformationBox var1;
      if(!this.getBoxes(SchemeInformationBox.class).isEmpty()) {
         var1 = (SchemeInformationBox)this.getBoxes(SchemeInformationBox.class).get(0);
      } else {
         var1 = null;
      }

      return var1;
   }

   public long getSize() {
      long var2 = this.getContainerSize();
      byte var1;
      if(!this.largeBox && var2 + 6L < 4294967296L) {
         var1 = 8;
      } else {
         var1 = 16;
      }

      return (long)var1 + var2 + 6L;
   }

   public int getVersion() {
      return this.version;
   }

   public void parse(com.googlecode.mp4parser.b var1, ByteBuffer var2, long var3, com.coremedia.iso.b var5) throws IOException {
      var2 = ByteBuffer.allocate(6);
      var1.a(var2);
      var2.rewind();
      this.version = e.d(var2);
      this.flags = e.b(var2);
      this.initContainer(var1, var3 - 6L, var5);
   }

   public void setFlags(int var1) {
      this.flags = var1;
   }

   public void setVersion(int var1) {
      this.version = var1;
   }
}
