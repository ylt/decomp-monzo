package com.coremedia.iso.boxes;

import com.coremedia.iso.d;
import com.coremedia.iso.e;
import com.googlecode.mp4parser.AbstractBox;
import java.nio.ByteBuffer;

public class OriginalFormatBox extends AbstractBox {
   // $FF: synthetic field
   static final boolean $assertionsDisabled;
   public static final String TYPE = "frma";
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_0;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_1;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_2;
   private String dataFormat = "    ";

   static {
      ajc$preClinit();
      boolean var0;
      if(!OriginalFormatBox.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      $assertionsDisabled = var0;
   }

   public OriginalFormatBox() {
      super("frma");
   }

   private static void ajc$preClinit() {
      org.mp4parser.aspectj.a.b.b var0 = new org.mp4parser.aspectj.a.b.b("OriginalFormatBox.java", OriginalFormatBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getDataFormat", "com.coremedia.iso.boxes.OriginalFormatBox", "", "", "", "java.lang.String"), 42);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "setDataFormat", "com.coremedia.iso.boxes.OriginalFormatBox", "java.lang.String", "dataFormat", "", "void"), 47);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "toString", "com.coremedia.iso.boxes.OriginalFormatBox", "", "", "", "java.lang.String"), 67);
   }

   public void _parseDetails(ByteBuffer var1) {
      this.dataFormat = e.k(var1);
   }

   protected void getContent(ByteBuffer var1) {
      var1.put(d.a(this.dataFormat));
   }

   protected long getContentSize() {
      return 4L;
   }

   public String getDataFormat() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.dataFormat;
   }

   public void setDataFormat(String var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_1, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      if(!$assertionsDisabled && var1.length() != 4) {
         throw new AssertionError();
      } else {
         this.dataFormat = var1;
      }
   }

   public String toString() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_2, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return "OriginalFormatBox[dataFormat=" + this.getDataFormat() + "]";
   }
}
