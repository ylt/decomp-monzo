package com.coremedia.iso.boxes;

import com.googlecode.mp4parser.e;
import java.nio.ByteBuffer;

public class SubtitleMediaHeaderBox extends AbstractMediaHeaderBox {
   public static final String TYPE = "sthd";
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_0;

   static {
      ajc$preClinit();
   }

   public SubtitleMediaHeaderBox() {
      super("sthd");
   }

   private static void ajc$preClinit() {
      org.mp4parser.aspectj.a.b.b var0 = new org.mp4parser.aspectj.a.b.b("SubtitleMediaHeaderBox.java", SubtitleMediaHeaderBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "toString", "com.coremedia.iso.boxes.SubtitleMediaHeaderBox", "", "", "", "java.lang.String"), 30);
   }

   public void _parseDetails(ByteBuffer var1) {
      this.parseVersionAndFlags(var1);
   }

   protected void getContent(ByteBuffer var1) {
      this.writeVersionAndFlags(var1);
   }

   protected long getContentSize() {
      return 4L;
   }

   public String toString() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_0, this, this);
      e.a().a(var1);
      return "SubtitleMediaHeaderBox";
   }
}
