package com.coremedia.iso.boxes;

import com.googlecode.mp4parser.AbstractFullBox;
import com.googlecode.mp4parser.e;

public abstract class ChunkOffsetBox extends AbstractFullBox {
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_0;

   static {
      ajc$preClinit();
   }

   public ChunkOffsetBox(String var1) {
      super(var1);
   }

   private static void ajc$preClinit() {
      org.mp4parser.aspectj.a.b.b var0 = new org.mp4parser.aspectj.a.b.b("ChunkOffsetBox.java", ChunkOffsetBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "toString", "com.coremedia.iso.boxes.ChunkOffsetBox", "", "", "", "java.lang.String"), 18);
   }

   public abstract long[] getChunkOffsets();

   public abstract void setChunkOffsets(long[] var1);

   public String toString() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_0, this, this);
      e.a().a(var1);
      return this.getClass().getSimpleName() + "[entryCount=" + this.getChunkOffsets().length + "]";
   }
}
