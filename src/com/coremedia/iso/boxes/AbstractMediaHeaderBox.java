package com.coremedia.iso.boxes;

import com.googlecode.mp4parser.AbstractFullBox;

public abstract class AbstractMediaHeaderBox extends AbstractFullBox {
   protected AbstractMediaHeaderBox(String var1) {
      super(var1);
   }
}
