package com.coremedia.iso.boxes;

import com.coremedia.iso.e;
import com.coremedia.iso.f;
import com.coremedia.iso.g;
import com.coremedia.iso.h;
import com.googlecode.mp4parser.AbstractFullBox;
import java.nio.ByteBuffer;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class ItemLocationBox extends AbstractFullBox {
   public static final String TYPE = "iloc";
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_0;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_1;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_10;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_11;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_2;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_3;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_4;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_5;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_6;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_7;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_8;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_9;
   public int baseOffsetSize = 8;
   public int indexSize = 0;
   public List items = new LinkedList();
   public int lengthSize = 8;
   public int offsetSize = 8;

   static {
      ajc$preClinit();
   }

   public ItemLocationBox() {
      super("iloc");
   }

   private static void ajc$preClinit() {
      org.mp4parser.aspectj.a.b.b var0 = new org.mp4parser.aspectj.a.b.b("ItemLocationBox.java", ItemLocationBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getOffsetSize", "com.coremedia.iso.boxes.ItemLocationBox", "", "", "", "int"), 119);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "setOffsetSize", "com.coremedia.iso.boxes.ItemLocationBox", "int", "offsetSize", "", "void"), 123);
      ajc$tjp_10 = var0.a("method-execution", var0.a("1", "createItem", "com.coremedia.iso.boxes.ItemLocationBox", "int:int:int:long:java.util.List", "itemId:constructionMethod:dataReferenceIndex:baseOffset:extents", "", "com.coremedia.iso.boxes.ItemLocationBox$Item"), 160);
      ajc$tjp_11 = var0.a("method-execution", var0.a("1", "createExtent", "com.coremedia.iso.boxes.ItemLocationBox", "long:long:long", "extentOffset:extentLength:extentIndex", "", "com.coremedia.iso.boxes.ItemLocationBox$Extent"), 285);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "getLengthSize", "com.coremedia.iso.boxes.ItemLocationBox", "", "", "", "int"), 127);
      ajc$tjp_3 = var0.a("method-execution", var0.a("1", "setLengthSize", "com.coremedia.iso.boxes.ItemLocationBox", "int", "lengthSize", "", "void"), 131);
      ajc$tjp_4 = var0.a("method-execution", var0.a("1", "getBaseOffsetSize", "com.coremedia.iso.boxes.ItemLocationBox", "", "", "", "int"), 135);
      ajc$tjp_5 = var0.a("method-execution", var0.a("1", "setBaseOffsetSize", "com.coremedia.iso.boxes.ItemLocationBox", "int", "baseOffsetSize", "", "void"), 139);
      ajc$tjp_6 = var0.a("method-execution", var0.a("1", "getIndexSize", "com.coremedia.iso.boxes.ItemLocationBox", "", "", "", "int"), 143);
      ajc$tjp_7 = var0.a("method-execution", var0.a("1", "setIndexSize", "com.coremedia.iso.boxes.ItemLocationBox", "int", "indexSize", "", "void"), 147);
      ajc$tjp_8 = var0.a("method-execution", var0.a("1", "getItems", "com.coremedia.iso.boxes.ItemLocationBox", "", "", "", "java.util.List"), 151);
      ajc$tjp_9 = var0.a("method-execution", var0.a("1", "setItems", "com.coremedia.iso.boxes.ItemLocationBox", "java.util.List", "items", "", "void"), 155);
   }

   public void _parseDetails(ByteBuffer var1) {
      this.parseVersionAndFlags(var1);
      int var2 = e.d(var1);
      this.offsetSize = var2 >>> 4;
      this.lengthSize = var2 & 15;
      var2 = e.d(var1);
      this.baseOffsetSize = var2 >>> 4;
      if(this.getVersion() == 1) {
         this.indexSize = var2 & 15;
      }

      int var3 = e.c(var1);

      for(var2 = 0; var2 < var3; ++var2) {
         this.items.add(new ItemLocationBox.b(var1));
      }

   }

   public ItemLocationBox.a createExtent(long var1, long var3, long var5) {
      org.mp4parser.aspectj.lang.a var7 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_11, this, this, (Object[])(new Object[]{org.mp4parser.aspectj.a.a.a.a(var1), org.mp4parser.aspectj.a.a.a.a(var3), org.mp4parser.aspectj.a.a.a.a(var5)}));
      com.googlecode.mp4parser.e.a().a(var7);
      return new ItemLocationBox.a(var1, var3, var5);
   }

   ItemLocationBox.a createExtent(ByteBuffer var1) {
      return new ItemLocationBox.a(var1);
   }

   public ItemLocationBox.b createItem(int var1, int var2, int var3, long var4, List var6) {
      org.mp4parser.aspectj.lang.a var7 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_10, this, this, (Object[])(new Object[]{org.mp4parser.aspectj.a.a.a.a(var1), org.mp4parser.aspectj.a.a.a.a(var2), org.mp4parser.aspectj.a.a.a.a(var3), org.mp4parser.aspectj.a.a.a.a(var4), var6}));
      com.googlecode.mp4parser.e.a().a(var7);
      return new ItemLocationBox.b(var1, var2, var3, var4, var6);
   }

   ItemLocationBox.b createItem(ByteBuffer var1) {
      return new ItemLocationBox.b(var1);
   }

   public int getBaseOffsetSize() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_4, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.baseOffsetSize;
   }

   protected void getContent(ByteBuffer var1) {
      this.writeVersionAndFlags(var1);
      g.c(var1, this.offsetSize << 4 | this.lengthSize);
      if(this.getVersion() == 1) {
         g.c(var1, this.baseOffsetSize << 4 | this.indexSize);
      } else {
         g.c(var1, this.baseOffsetSize << 4);
      }

      g.b(var1, this.items.size());
      Iterator var2 = this.items.iterator();

      while(var2.hasNext()) {
         ((ItemLocationBox.b)var2.next()).a(var1);
      }

   }

   protected long getContentSize() {
      Iterator var3 = this.items.iterator();

      long var1;
      for(var1 = 8L; var3.hasNext(); var1 += (long)((ItemLocationBox.b)var3.next()).a()) {
         ;
      }

      return var1;
   }

   public int getIndexSize() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_6, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.indexSize;
   }

   public List getItems() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_8, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.items;
   }

   public int getLengthSize() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_2, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.lengthSize;
   }

   public int getOffsetSize() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.offsetSize;
   }

   public void setBaseOffsetSize(int var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_5, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.baseOffsetSize = var1;
   }

   public void setIndexSize(int var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_7, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.indexSize = var1;
   }

   public void setItems(List var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_9, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.items = var1;
   }

   public void setLengthSize(int var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_3, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.lengthSize = var1;
   }

   public void setOffsetSize(int var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_1, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.offsetSize = var1;
   }

   public class a {
      public long a;
      public long b;
      public long c;

      public a(long var2, long var4, long var6) {
         this.a = var2;
         this.b = var4;
         this.c = var6;
      }

      public a(ByteBuffer var2) {
         if(ItemLocationBox.this.getVersion() == 1 && ItemLocationBox.this.indexSize > 0) {
            this.c = f.a(var2, ItemLocationBox.this.indexSize);
         }

         this.a = f.a(var2, ItemLocationBox.this.offsetSize);
         this.b = f.a(var2, ItemLocationBox.this.lengthSize);
      }

      public int a() {
         int var1;
         if(ItemLocationBox.this.indexSize > 0) {
            var1 = ItemLocationBox.this.indexSize;
         } else {
            var1 = 0;
         }

         return var1 + ItemLocationBox.this.offsetSize + ItemLocationBox.this.lengthSize;
      }

      public void a(ByteBuffer var1) {
         if(ItemLocationBox.this.getVersion() == 1 && ItemLocationBox.this.indexSize > 0) {
            h.a(this.c, var1, ItemLocationBox.this.indexSize);
         }

         h.a(this.a, var1, ItemLocationBox.this.offsetSize);
         h.a(this.b, var1, ItemLocationBox.this.lengthSize);
      }

      public boolean equals(Object var1) {
         boolean var2 = true;
         if(this != var1) {
            if(var1 != null && this.getClass() == var1.getClass()) {
               ItemLocationBox.a var3 = (ItemLocationBox.a)var1;
               if(this.c != var3.c) {
                  var2 = false;
               } else if(this.b != var3.b) {
                  var2 = false;
               } else if(this.a != var3.a) {
                  var2 = false;
               }
            } else {
               var2 = false;
            }
         }

         return var2;
      }

      public int hashCode() {
         return ((int)(this.a ^ this.a >>> 32) * 31 + (int)(this.b ^ this.b >>> 32)) * 31 + (int)(this.c ^ this.c >>> 32);
      }

      public String toString() {
         StringBuilder var1 = new StringBuilder();
         var1.append("Extent");
         var1.append("{extentOffset=").append(this.a);
         var1.append(", extentLength=").append(this.b);
         var1.append(", extentIndex=").append(this.c);
         var1.append('}');
         return var1.toString();
      }
   }

   public class b {
      public int a;
      public int b;
      public int c;
      public long d;
      public List e = new LinkedList();

      public b(int var2, int var3, int var4, long var5, List var7) {
         this.a = var2;
         this.b = var3;
         this.c = var4;
         this.d = var5;
         this.e = var7;
      }

      public b(ByteBuffer var2) {
         this.a = e.c(var2);
         if(ItemLocationBox.this.getVersion() == 1) {
            this.b = e.c(var2) & 15;
         }

         this.c = e.c(var2);
         if(ItemLocationBox.this.baseOffsetSize > 0) {
            this.d = f.a(var2, ItemLocationBox.this.baseOffsetSize);
         } else {
            this.d = 0L;
         }

         int var4 = e.c(var2);

         for(int var3 = 0; var3 < var4; ++var3) {
            this.e.add(ItemLocationBox.this.new a(var2));
         }

      }

      public int a() {
         byte var1 = 2;
         if(ItemLocationBox.this.getVersion() == 1) {
            var1 = 4;
         }

         int var2 = ItemLocationBox.this.baseOffsetSize;
         Iterator var3 = this.e.iterator();

         int var4;
         for(var4 = var1 + 2 + var2 + 2; var3.hasNext(); var4 += ((ItemLocationBox.a)var3.next()).a()) {
            ;
         }

         return var4;
      }

      public void a(ByteBuffer var1) {
         g.b(var1, this.a);
         if(ItemLocationBox.this.getVersion() == 1) {
            g.b(var1, this.b);
         }

         g.b(var1, this.c);
         if(ItemLocationBox.this.baseOffsetSize > 0) {
            h.a(this.d, var1, ItemLocationBox.this.baseOffsetSize);
         }

         g.b(var1, this.e.size());
         Iterator var2 = this.e.iterator();

         while(var2.hasNext()) {
            ((ItemLocationBox.a)var2.next()).a(var1);
         }

      }

      public boolean equals(Object var1) {
         boolean var2 = true;
         if(this != var1) {
            if(var1 != null && this.getClass() == var1.getClass()) {
               ItemLocationBox.b var3 = (ItemLocationBox.b)var1;
               if(this.d != var3.d) {
                  var2 = false;
               } else if(this.b != var3.b) {
                  var2 = false;
               } else if(this.c != var3.c) {
                  var2 = false;
               } else if(this.a != var3.a) {
                  var2 = false;
               } else {
                  if(this.e != null) {
                     if(this.e.equals(var3.e)) {
                        return var2;
                     }
                  } else if(var3.e == null) {
                     return var2;
                  }

                  var2 = false;
               }
            } else {
               var2 = false;
            }
         }

         return var2;
      }

      public int hashCode() {
         int var3 = this.a;
         int var4 = this.b;
         int var5 = this.c;
         int var2 = (int)(this.d ^ this.d >>> 32);
         int var1;
         if(this.e != null) {
            var1 = this.e.hashCode();
         } else {
            var1 = 0;
         }

         return var1 + (((var3 * 31 + var4) * 31 + var5) * 31 + var2) * 31;
      }

      public String toString() {
         return "Item{baseOffset=" + this.d + ", itemId=" + this.a + ", constructionMethod=" + this.b + ", dataReferenceIndex=" + this.c + ", extents=" + this.e + '}';
      }
   }
}
