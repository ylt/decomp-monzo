package com.coremedia.iso.boxes;

import com.googlecode.mp4parser.AbstractContainerBox;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;

public class UserDataBox extends AbstractContainerBox {
   public static final String TYPE = "udta";

   public UserDataBox() {
      super("udta");
   }

   public void getBox(WritableByteChannel var1) throws IOException {
      super.getBox(var1);
   }

   public void parse(com.googlecode.mp4parser.b var1, ByteBuffer var2, long var3, com.coremedia.iso.b var5) throws IOException {
      super.parse(var1, var2, var3, var5);
   }
}
