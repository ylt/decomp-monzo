package com.coremedia.iso.boxes;

import com.coremedia.iso.e;
import com.coremedia.iso.g;
import com.googlecode.mp4parser.AbstractFullBox;
import com.googlecode.mp4parser.c.c;
import com.googlecode.mp4parser.c.f;
import java.nio.ByteBuffer;
import java.util.Date;

public class MediaHeaderBox extends AbstractFullBox {
   private static f LOG;
   public static final String TYPE = "mdhd";
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_0;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_1;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_10;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_2;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_3;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_4;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_5;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_6;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_7;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_8;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_9;
   private Date creationTime = new Date();
   private long duration;
   private String language = "eng";
   private Date modificationTime = new Date();
   private long timescale;

   static {
      ajc$preClinit();
      LOG = f.a(MediaHeaderBox.class);
   }

   public MediaHeaderBox() {
      super("mdhd");
   }

   private static void ajc$preClinit() {
      org.mp4parser.aspectj.a.b.b var0 = new org.mp4parser.aspectj.a.b.b("MediaHeaderBox.java", MediaHeaderBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getCreationTime", "com.coremedia.iso.boxes.MediaHeaderBox", "", "", "", "java.util.Date"), 48);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "getModificationTime", "com.coremedia.iso.boxes.MediaHeaderBox", "", "", "", "java.util.Date"), 52);
      ajc$tjp_10 = var0.a("method-execution", var0.a("1", "toString", "com.coremedia.iso.boxes.MediaHeaderBox", "", "", "", "java.lang.String"), 125);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "getTimescale", "com.coremedia.iso.boxes.MediaHeaderBox", "", "", "", "long"), 56);
      ajc$tjp_3 = var0.a("method-execution", var0.a("1", "getDuration", "com.coremedia.iso.boxes.MediaHeaderBox", "", "", "", "long"), 60);
      ajc$tjp_4 = var0.a("method-execution", var0.a("1", "getLanguage", "com.coremedia.iso.boxes.MediaHeaderBox", "", "", "", "java.lang.String"), 64);
      ajc$tjp_5 = var0.a("method-execution", var0.a("1", "setCreationTime", "com.coremedia.iso.boxes.MediaHeaderBox", "java.util.Date", "creationTime", "", "void"), 81);
      ajc$tjp_6 = var0.a("method-execution", var0.a("1", "setModificationTime", "com.coremedia.iso.boxes.MediaHeaderBox", "java.util.Date", "modificationTime", "", "void"), 85);
      ajc$tjp_7 = var0.a("method-execution", var0.a("1", "setTimescale", "com.coremedia.iso.boxes.MediaHeaderBox", "long", "timescale", "", "void"), 89);
      ajc$tjp_8 = var0.a("method-execution", var0.a("1", "setDuration", "com.coremedia.iso.boxes.MediaHeaderBox", "long", "duration", "", "void"), 93);
      ajc$tjp_9 = var0.a("method-execution", var0.a("1", "setLanguage", "com.coremedia.iso.boxes.MediaHeaderBox", "java.lang.String", "language", "", "void"), 97);
   }

   public void _parseDetails(ByteBuffer var1) {
      this.parseVersionAndFlags(var1);
      if(this.getVersion() == 1) {
         this.creationTime = c.a(e.f(var1));
         this.modificationTime = c.a(e.f(var1));
         this.timescale = e.a(var1);
         this.duration = var1.getLong();
      } else {
         this.creationTime = c.a(e.a(var1));
         this.modificationTime = c.a(e.a(var1));
         this.timescale = e.a(var1);
         this.duration = e.a(var1);
      }

      if(this.duration < -1L) {
         LOG.b("mdhd duration is not in expected range");
      }

      this.language = e.j(var1);
      e.c(var1);
   }

   protected void getContent(ByteBuffer var1) {
      this.writeVersionAndFlags(var1);
      if(this.getVersion() == 1) {
         g.a(var1, c.a(this.creationTime));
         g.a(var1, c.a(this.modificationTime));
         g.b(var1, this.timescale);
         var1.putLong(this.duration);
      } else {
         g.b(var1, c.a(this.creationTime));
         g.b(var1, c.a(this.modificationTime));
         g.b(var1, this.timescale);
         var1.putInt((int)this.duration);
      }

      g.a(var1, this.language);
      g.b(var1, 0);
   }

   protected long getContentSize() {
      long var1;
      if(this.getVersion() == 1) {
         var1 = 4L + 28L;
      } else {
         var1 = 4L + 16L;
      }

      return var1 + 2L + 2L;
   }

   public Date getCreationTime() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.creationTime;
   }

   public long getDuration() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_3, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.duration;
   }

   public String getLanguage() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_4, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.language;
   }

   public Date getModificationTime() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_1, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.modificationTime;
   }

   public long getTimescale() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_2, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.timescale;
   }

   public void setCreationTime(Date var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_5, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.creationTime = var1;
   }

   public void setDuration(long var1) {
      org.mp4parser.aspectj.lang.a var3 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_8, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var3);
      this.duration = var1;
   }

   public void setLanguage(String var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_9, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.language = var1;
   }

   public void setModificationTime(Date var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_6, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.modificationTime = var1;
   }

   public void setTimescale(long var1) {
      org.mp4parser.aspectj.lang.a var3 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_7, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var3);
      this.timescale = var1;
   }

   public String toString() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_10, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      StringBuilder var2 = new StringBuilder();
      var2.append("MediaHeaderBox[");
      var2.append("creationTime=").append(this.getCreationTime());
      var2.append(";");
      var2.append("modificationTime=").append(this.getModificationTime());
      var2.append(";");
      var2.append("timescale=").append(this.getTimescale());
      var2.append(";");
      var2.append("duration=").append(this.getDuration());
      var2.append(";");
      var2.append("language=").append(this.getLanguage());
      var2.append("]");
      return var2.toString();
   }
}
