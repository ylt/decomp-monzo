package com.coremedia.iso.boxes;

import com.coremedia.iso.e;
import com.coremedia.iso.g;
import com.googlecode.mp4parser.AbstractFullBox;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class CompositionTimeToSample extends AbstractFullBox {
   // $FF: synthetic field
   static final boolean $assertionsDisabled;
   public static final String TYPE = "ctts";
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_0;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_1;
   List entries = Collections.emptyList();

   static {
      ajc$preClinit();
      boolean var0;
      if(!CompositionTimeToSample.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      $assertionsDisabled = var0;
   }

   public CompositionTimeToSample() {
      super("ctts");
   }

   private static void ajc$preClinit() {
      org.mp4parser.aspectj.a.b.b var0 = new org.mp4parser.aspectj.a.b.b("CompositionTimeToSample.java", CompositionTimeToSample.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getEntries", "com.coremedia.iso.boxes.CompositionTimeToSample", "", "", "", "java.util.List"), 57);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "setEntries", "com.coremedia.iso.boxes.CompositionTimeToSample", "java.util.List", "entries", "", "void"), 61);
   }

   public static int[] blowupCompositionTimes(List var0) {
      Iterator var6 = var0.iterator();

      long var4;
      for(var4 = 0L; var6.hasNext(); var4 += (long)((CompositionTimeToSample.a)var6.next()).a()) {
         ;
      }

      if(!$assertionsDisabled && var4 > 2147483647L) {
         throw new AssertionError();
      } else {
         int[] var9 = new int[(int)var4];
         Iterator var7 = var0.iterator();
         int var1 = 0;

         while(var7.hasNext()) {
            CompositionTimeToSample.a var8 = (CompositionTimeToSample.a)var7.next();
            int var3 = 0;
            int var2 = var1;

            while(true) {
               var1 = var2;
               if(var3 >= var8.a()) {
                  break;
               }

               var9[var2] = var8.b();
               ++var3;
               ++var2;
            }
         }

         return var9;
      }
   }

   public void _parseDetails(ByteBuffer var1) {
      this.parseVersionAndFlags(var1);
      int var3 = com.googlecode.mp4parser.c.b.a(e.a(var1));
      this.entries = new ArrayList(var3);

      for(int var2 = 0; var2 < var3; ++var2) {
         CompositionTimeToSample.a var4 = new CompositionTimeToSample.a(com.googlecode.mp4parser.c.b.a(e.a(var1)), var1.getInt());
         this.entries.add(var4);
      }

   }

   protected void getContent(ByteBuffer var1) {
      this.writeVersionAndFlags(var1);
      g.b(var1, (long)this.entries.size());
      Iterator var2 = this.entries.iterator();

      while(var2.hasNext()) {
         CompositionTimeToSample.a var3 = (CompositionTimeToSample.a)var2.next();
         g.b(var1, (long)var3.a());
         var1.putInt(var3.b());
      }

   }

   protected long getContentSize() {
      return (long)(this.entries.size() * 8 + 8);
   }

   public List getEntries() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.entries;
   }

   public void setEntries(List var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_1, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.entries = var1;
   }

   public static class a {
      int a;
      int b;

      public a(int var1, int var2) {
         this.a = var1;
         this.b = var2;
      }

      public int a() {
         return this.a;
      }

      public void a(int var1) {
         this.a = var1;
      }

      public int b() {
         return this.b;
      }

      public String toString() {
         return "Entry{count=" + this.a + ", offset=" + this.b + '}';
      }
   }
}
