package com.coremedia.iso.boxes;

import com.coremedia.iso.g;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class FreeBox implements a {
   // $FF: synthetic field
   static final boolean $assertionsDisabled;
   public static final String TYPE = "free";
   ByteBuffer data;
   private long offset;
   private b parent;
   List replacers = new LinkedList();

   static {
      boolean var0;
      if(!FreeBox.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      $assertionsDisabled = var0;
   }

   public FreeBox() {
      this.data = ByteBuffer.wrap(new byte[0]);
   }

   public FreeBox(int var1) {
      this.data = ByteBuffer.allocate(var1);
   }

   public void addAndReplace(a var1) {
      this.data.position(com.googlecode.mp4parser.c.b.a(var1.getSize()));
      this.data = this.data.slice();
      this.replacers.add(var1);
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(this != var1) {
         if(var1 != null && this.getClass() == var1.getClass()) {
            FreeBox var3 = (FreeBox)var1;
            if(this.getData() != null) {
               if(this.getData().equals(var3.getData())) {
                  return var2;
               }
            } else if(var3.getData() == null) {
               return var2;
            }

            var2 = false;
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public void getBox(WritableByteChannel var1) throws IOException {
      Iterator var2 = this.replacers.iterator();

      while(var2.hasNext()) {
         ((a)var2.next()).getBox(var1);
      }

      ByteBuffer var3 = ByteBuffer.allocate(8);
      g.b(var3, (long)(this.data.limit() + 8));
      var3.put("free".getBytes());
      var3.rewind();
      var1.write(var3);
      var3.rewind();
      this.data.rewind();
      var1.write(this.data);
      this.data.rewind();
   }

   public ByteBuffer getData() {
      ByteBuffer var1;
      if(this.data != null) {
         var1 = (ByteBuffer)this.data.duplicate().rewind();
      } else {
         var1 = null;
      }

      return var1;
   }

   public long getOffset() {
      return this.offset;
   }

   public b getParent() {
      return this.parent;
   }

   public long getSize() {
      Iterator var3 = this.replacers.iterator();

      long var1;
      for(var1 = 8L; var3.hasNext(); var1 += ((a)var3.next()).getSize()) {
         ;
      }

      return (long)this.data.limit() + var1;
   }

   public String getType() {
      return "free";
   }

   public int hashCode() {
      int var1;
      if(this.data != null) {
         var1 = this.data.hashCode();
      } else {
         var1 = 0;
      }

      return var1;
   }

   public void parse(com.googlecode.mp4parser.b var1, ByteBuffer var2, long var3, com.coremedia.iso.b var5) throws IOException {
      this.offset = var1.b() - (long)var2.remaining();
      if(var3 > 1048576L) {
         this.data = var1.a(var1.b(), var3);
         var1.a(var1.b() + var3);
      } else {
         if(!$assertionsDisabled && var3 >= 2147483647L) {
            throw new AssertionError();
         }

         this.data = ByteBuffer.allocate(com.googlecode.mp4parser.c.b.a(var3));
         var1.a(this.data);
      }

   }

   public void setData(ByteBuffer var1) {
      this.data = var1;
   }

   public void setParent(b var1) {
      this.parent = var1;
   }
}
