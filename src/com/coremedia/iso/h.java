package com.coremedia.iso;

import java.nio.ByteBuffer;

public final class h {
   public static void a(long var0, ByteBuffer var2, int var3) {
      switch(var3) {
      case 1:
         g.c(var2, (int)(255L & var0));
         break;
      case 2:
         g.b(var2, (int)(65535L & var0));
         break;
      case 3:
         g.a(var2, (int)(16777215L & var0));
         break;
      case 4:
         g.b(var2, var0);
         break;
      case 5:
      case 6:
      case 7:
      default:
         throw new RuntimeException("I don't know how to read " + var3 + " bytes");
      case 8:
         g.a(var2, var0);
      }

   }
}
