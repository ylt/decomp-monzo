package com.coremedia.iso;

import java.io.EOFException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.logging.Logger;

public abstract class a implements b {
   private static Logger b = Logger.getLogger(a.class.getName());
   ThreadLocal a = new ThreadLocal() {
      protected ByteBuffer a() {
         return ByteBuffer.allocate(32);
      }

      // $FF: synthetic method
      protected Object initialValue() {
         return this.a();
      }
   };

   public com.coremedia.iso.boxes.a a(com.googlecode.mp4parser.b var1, com.coremedia.iso.boxes.b var2) throws IOException {
      com.coremedia.iso.boxes.a var6 = null;
      long var4 = var1.b();
      ((ByteBuffer)this.a.get()).rewind().limit(8);

      int var3;
      do {
         var3 = var1.a((ByteBuffer)this.a.get());
         if(var3 == 8) {
            ((ByteBuffer)this.a.get()).rewind();
            var4 = e.a((ByteBuffer)this.a.get());
            com.coremedia.iso.boxes.a var9;
            if(var4 < 8L && var4 > 1L) {
               b.severe("Plausibility check failed: size < 8 (size = " + var4 + "). Stop parsing!");
               var9 = var6;
            } else {
               String var8 = e.k((ByteBuffer)this.a.get());
               if(var4 == 1L) {
                  ((ByteBuffer)this.a.get()).limit(16);
                  var1.a((ByteBuffer)this.a.get());
                  ((ByteBuffer)this.a.get()).position(8);
                  var4 = e.f((ByteBuffer)this.a.get()) - 16L;
               } else if(var4 == 0L) {
                  var4 = var1.a() - var1.b();
               } else {
                  var4 -= 8L;
               }

               byte[] var10;
               if(!"uuid".equals(var8)) {
                  var10 = null;
               } else {
                  ((ByteBuffer)this.a.get()).limit(((ByteBuffer)this.a.get()).limit() + 16);
                  var1.a((ByteBuffer)this.a.get());
                  var10 = new byte[16];

                  for(var3 = ((ByteBuffer)this.a.get()).position() - 16; var3 < ((ByteBuffer)this.a.get()).position(); ++var3) {
                     var10[var3 - (((ByteBuffer)this.a.get()).position() - 16)] = ((ByteBuffer)this.a.get()).get(var3);
                  }

                  var4 -= 16L;
               }

               String var7;
               if(var2 instanceof com.coremedia.iso.boxes.a) {
                  var7 = ((com.coremedia.iso.boxes.a)var2).getType();
               } else {
                  var7 = "";
               }

               var6 = this.a(var8, var10, var7);
               var6.setParent(var2);
               ((ByteBuffer)this.a.get()).rewind();
               var6.parse(var1, (ByteBuffer)this.a.get(), var4, this);
               var9 = var6;
            }

            return var9;
         }
      } while(var3 >= 0);

      var1.a(var4);
      throw new EOFException();
   }

   public abstract com.coremedia.iso.boxes.a a(String var1, byte[] var2, String var3);
}
