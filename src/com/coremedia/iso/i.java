package com.coremedia.iso;

import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class i extends a {
   static String[] g = new String[0];
   Properties b;
   Pattern c;
   StringBuilder d;
   ThreadLocal e;
   ThreadLocal f;

   public i(String... param1) {
      // $FF: Couldn't be decompiled
   }

   public com.coremedia.iso.boxes.a a(String param1, byte[] param2, String param3) {
      // $FF: Couldn't be decompiled
   }

   public void b(String var1, byte[] var2, String var3) {
      String var4;
      String var8;
      if(var2 != null) {
         if(!"uuid".equals(var1)) {
            throw new RuntimeException("we have a userType but no uuid box type. Something's wrong");
         }

         String var5 = this.b.getProperty("uuid[" + c.a(var2).toUpperCase() + "]");
         var4 = var5;
         if(var5 == null) {
            var4 = this.b.getProperty(var3 + "-uuid[" + c.a(var2).toUpperCase() + "]");
         }

         var8 = var4;
         if(var4 == null) {
            var8 = this.b.getProperty("uuid");
         }
      } else {
         var4 = this.b.getProperty(var1);
         var8 = var4;
         if(var4 == null) {
            var8 = this.d.append(var3).append('-').append(var1).toString();
            this.d.setLength(0);
            var8 = this.b.getProperty(var8);
         }
      }

      var3 = var8;
      if(var8 == null) {
         var3 = this.b.getProperty("default");
      }

      if(var3 == null) {
         throw new RuntimeException("No box object found for " + var1);
      } else {
         if(!var3.endsWith(")")) {
            this.f.set(g);
            this.e.set(var3);
         } else {
            Matcher var6 = this.c.matcher(var3);
            if(!var6.matches()) {
               throw new RuntimeException("Cannot work with that constructor: " + var3);
            }

            this.e.set(var6.group(1));
            if(var6.group(2).length() == 0) {
               this.f.set(g);
            } else {
               ThreadLocal var9 = this.f;
               String[] var7;
               if(var6.group(2).length() > 0) {
                  var7 = var6.group(2).split(",");
               } else {
                  var7 = new String[0];
               }

               var9.set(var7);
            }
         }

      }
   }
}
