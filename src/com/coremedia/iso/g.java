package com.coremedia.iso;

import java.nio.ByteBuffer;

public final class g {
   // $FF: synthetic field
   static final boolean a;

   static {
      boolean var0;
      if(!g.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
   }

   public static void a(ByteBuffer var0, double var1) {
      int var3 = (int)(65536.0D * var1);
      var0.put((byte)((-16777216 & var3) >> 24));
      var0.put((byte)((16711680 & var3) >> 16));
      var0.put((byte)(('\uff00' & var3) >> 8));
      var0.put((byte)(var3 & 255));
   }

   public static void a(ByteBuffer var0, int var1) {
      var1 &= 16777215;
      b(var0, var1 >> 8);
      c(var0, var1);
   }

   public static void a(ByteBuffer var0, long var1) {
      if(!a && var1 < 0L) {
         throw new AssertionError("The given long is negative");
      } else {
         var0.putLong(var1);
      }
   }

   public static void a(ByteBuffer var0, String var1) {
      int var3 = 0;
      if(var1.getBytes().length != 3) {
         throw new IllegalArgumentException("\"" + var1 + "\" language string isn't exactly 3 characters long!");
      } else {
         int var2;
         for(var2 = 0; var3 < 3; ++var3) {
            var2 += var1.getBytes()[var3] - 96 << (2 - var3) * 5;
         }

         b(var0, var2);
      }
   }

   public static void b(ByteBuffer var0, double var1) {
      int var3 = (int)(1.073741824E9D * var1);
      var0.put((byte)((-16777216 & var3) >> 24));
      var0.put((byte)((16711680 & var3) >> 16));
      var0.put((byte)(('\uff00' & var3) >> 8));
      var0.put((byte)(var3 & 255));
   }

   public static void b(ByteBuffer var0, int var1) {
      var1 &= '\uffff';
      c(var0, var1 >> 8);
      c(var0, var1 & 255);
   }

   public static void b(ByteBuffer var0, long var1) {
      if(a || var1 >= 0L && var1 <= 4294967296L) {
         var0.putInt((int)var1);
      } else {
         throw new AssertionError("The given long is not in the range of uint32 (" + var1 + ")");
      }
   }

   public static void b(ByteBuffer var0, String var1) {
      var0.put(j.a(var1));
      c(var0, 0);
   }

   public static void c(ByteBuffer var0, double var1) {
      short var3 = (short)((int)(256.0D * var1));
      var0.put((byte)(('\uff00' & var3) >> 8));
      var0.put((byte)(var3 & 255));
   }

   public static void c(ByteBuffer var0, int var1) {
      var0.put((byte)(var1 & 255));
   }

   public static void c(ByteBuffer var0, long var1) {
      var1 &= 281474976710655L;
      b(var0, (int)(var1 >> 32));
      b(var0, var1 & 4294967295L);
   }

   public static void c(ByteBuffer var0, String var1) {
      var0.put(j.a(var1));
      c(var0, 0);
   }
}
