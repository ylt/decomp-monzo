package com.coremedia.iso;

import java.nio.ByteBuffer;

public final class f {
   public static long a(ByteBuffer var0, int var1) {
      long var2;
      switch(var1) {
      case 1:
         var2 = (long)e.d(var0);
         break;
      case 2:
         var2 = (long)e.c(var0);
         break;
      case 3:
         var2 = (long)e.b(var0);
         break;
      case 4:
         var2 = e.a(var0);
         break;
      case 5:
      case 6:
      case 7:
      default:
         throw new RuntimeException("I don't know how to read " + var1 + " bytes");
      case 8:
         var2 = e.f(var0);
      }

      return var2;
   }
}
