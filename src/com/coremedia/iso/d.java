package com.coremedia.iso;

import com.coremedia.iso.boxes.MovieBox;
import java.io.Closeable;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Iterator;

public class d extends com.googlecode.mp4parser.a implements Closeable {
   private static com.googlecode.mp4parser.c.f a = com.googlecode.mp4parser.c.f.a(d.class);

   public d(com.googlecode.mp4parser.b var1) throws IOException {
      this(var1, new i(new String[0]));
   }

   public d(com.googlecode.mp4parser.b var1, b var2) throws IOException {
      this.initContainer(var1, var1.a(), var2);
   }

   public static String a(byte[] var0) {
      byte[] var1 = new byte[4];
      if(var0 != null) {
         System.arraycopy(var0, 0, var1, 0, Math.min(var0.length, 4));
      }

      try {
         String var3 = new String(var1, "ISO-8859-1");
         return var3;
      } catch (UnsupportedEncodingException var2) {
         throw new Error("Required character encoding is missing", var2);
      }
   }

   public static byte[] a(String var0) {
      byte[] var2 = new byte[4];
      if(var0 != null) {
         for(int var1 = 0; var1 < Math.min(4, var0.length()); ++var1) {
            var2[var1] = (byte)var0.charAt(var1);
         }
      }

      return var2;
   }

   public MovieBox a() {
      Iterator var2 = this.getBoxes().iterator();

      MovieBox var3;
      while(true) {
         if(!var2.hasNext()) {
            var3 = null;
            break;
         }

         com.coremedia.iso.boxes.a var1 = (com.coremedia.iso.boxes.a)var2.next();
         if(var1 instanceof MovieBox) {
            var3 = (MovieBox)var1;
            break;
         }
      }

      return var3;
   }

   public void close() throws IOException {
      this.dataSource.close();
   }

   public String toString() {
      return "model(" + this.dataSource.toString() + ")";
   }
}
