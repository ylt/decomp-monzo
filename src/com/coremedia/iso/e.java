package com.coremedia.iso;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;

public final class e {
   public static int a(byte var0) {
      int var1 = var0;
      if(var0 < 0) {
         var1 = var0 + 256;
      }

      return var1;
   }

   public static long a(ByteBuffer var0) {
      long var3 = (long)var0.getInt();
      long var1 = var3;
      if(var3 < 0L) {
         var1 = var3 + 4294967296L;
      }

      return var1;
   }

   public static String a(ByteBuffer var0, int var1) {
      byte[] var2 = new byte[var1];
      var0.get(var2);
      return j.a(var2);
   }

   public static int b(ByteBuffer var0) {
      return 0 + (c(var0) << 8) + a(var0.get());
   }

   public static int c(ByteBuffer var0) {
      return 0 + (a(var0.get()) << 8) + a(var0.get());
   }

   public static int d(ByteBuffer var0) {
      return a(var0.get());
   }

   public static String e(ByteBuffer var0) {
      ByteArrayOutputStream var2 = new ByteArrayOutputStream();

      while(true) {
         byte var1 = var0.get();
         if(var1 == 0) {
            return j.a(var2.toByteArray());
         }

         var2.write(var1);
      }
   }

   public static long f(ByteBuffer var0) {
      long var1 = (a(var0) << 32) + 0L;
      if(var1 < 0L) {
         throw new RuntimeException("I don't know how to deal with UInt64! long is not sufficient and I don't want to use BigInt");
      } else {
         return var1 + a(var0);
      }
   }

   public static double g(ByteBuffer var0) {
      byte[] var4 = new byte[4];
      var0.get(var4);
      byte var2 = var4[0];
      byte var3 = var4[1];
      byte var1 = var4[2];
      return (double)(var4[3] & 255 | var2 << 24 & -16777216 | 0 | var3 << 16 & 16711680 | var1 << 8 & '\uff00') / 65536.0D;
   }

   public static double h(ByteBuffer var0) {
      byte[] var4 = new byte[4];
      var0.get(var4);
      byte var1 = var4[0];
      byte var2 = var4[1];
      byte var3 = var4[2];
      return (double)(var4[3] & 255 | var1 << 24 & -16777216 | 0 | var2 << 16 & 16711680 | var3 << 8 & '\uff00') / 1.073741824E9D;
   }

   public static float i(ByteBuffer var0) {
      byte[] var2 = new byte[2];
      var0.get(var2);
      short var1 = (short)(var2[0] << 8 & '\uff00' | 0);
      return (float)((short)(var2[1] & 255 | var1)) / 256.0F;
   }

   public static String j(ByteBuffer var0) {
      int var2 = c(var0);
      StringBuilder var3 = new StringBuilder();

      for(int var1 = 0; var1 < 3; ++var1) {
         var3.append((char)((var2 >> (2 - var1) * 5 & 31) + 96));
      }

      return var3.toString();
   }

   public static String k(ByteBuffer var0) {
      byte[] var1 = new byte[4];
      var0.get(var1);

      try {
         String var3 = new String(var1, "ISO-8859-1");
         return var3;
      } catch (UnsupportedEncodingException var2) {
         throw new RuntimeException(var2);
      }
   }

   public static long l(ByteBuffer var0) {
      long var1 = (long)c(var0) << 32;
      if(var1 < 0L) {
         throw new RuntimeException("I don't know how to deal with UInt64! long is not sufficient and I don't want to use BigInt");
      } else {
         return var1 + a(var0);
      }
   }
}
