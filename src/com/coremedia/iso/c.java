package com.coremedia.iso;

public class c {
   private static final char[] a = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

   public static String a(byte[] var0) {
      return a(var0, 0);
   }

   public static String a(byte[] var0, int var1) {
      byte var4 = 0;
      int var5 = var0.length;
      int var2;
      if(var1 > 0) {
         var2 = var5 / var1;
      } else {
         var2 = 0;
      }

      char[] var6 = new char[var2 + (var5 << 1)];
      int var3 = 0;

      for(var2 = var4; var3 < var5; ++var3) {
         int var7;
         if(var1 > 0 && var3 % var1 == 0 && var2 > 0) {
            var7 = var2 + 1;
            var6[var2] = 45;
            var2 = var7;
         }

         var7 = var2 + 1;
         var6[var2] = a[(var0[var3] & 240) >>> 4];
         var2 = var7 + 1;
         var6[var7] = a[var0[var3] & 15];
      }

      return new String(var6);
   }
}
