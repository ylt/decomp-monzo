package com.getkeepsafe.relinker;

import android.os.Build;
import android.os.Build.VERSION;

final class d implements b.b {
   public void a(String var1) {
      System.loadLibrary(var1);
   }

   public String[] a() {
      String[] var1;
      if(VERSION.SDK_INT >= 21 && Build.SUPPORTED_ABIS.length > 0) {
         var1 = Build.SUPPORTED_ABIS;
      } else if(!e.a(Build.CPU_ABI2)) {
         var1 = new String[]{Build.CPU_ABI, Build.CPU_ABI2};
      } else {
         var1 = new String[]{Build.CPU_ABI};
      }

      return var1;
   }

   public void b(String var1) {
      System.load(var1);
   }

   public String c(String var1) {
      if(!var1.startsWith("lib") || !var1.endsWith(".so")) {
         var1 = System.mapLibraryName(var1);
      }

      return var1;
   }

   public String d(String var1) {
      return var1.substring(3, var1.length() - 3);
   }
}
