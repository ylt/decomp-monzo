package com.getkeepsafe.relinker.a;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class e extends c.b {
   private final f j;

   public e(boolean var1, f var2) throws IOException {
      this.a = var1;
      this.j = var2;
      ByteBuffer var4 = ByteBuffer.allocate(8);
      ByteOrder var3;
      if(var1) {
         var3 = ByteOrder.BIG_ENDIAN;
      } else {
         var3 = ByteOrder.LITTLE_ENDIAN;
      }

      var4.order(var3);
      this.b = var2.d(var4, 16L);
      this.c = var2.b(var4, 32L);
      this.d = var2.b(var4, 40L);
      this.e = var2.d(var4, 54L);
      this.f = var2.d(var4, 56L);
      this.g = var2.d(var4, 58L);
      this.h = var2.d(var4, 60L);
      this.i = var2.d(var4, 62L);
   }

   public c.a a(long var1, int var3) throws IOException {
      return new b(this.j, this, var1, var3);
   }

   public c.c a(long var1) throws IOException {
      return new h(this.j, this, var1);
   }

   public c.d a(int var1) throws IOException {
      return new j(this.j, this, var1);
   }
}
