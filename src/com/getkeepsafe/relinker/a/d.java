package com.getkeepsafe.relinker.a;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class d extends c.b {
   private final f j;

   public d(boolean var1, f var2) throws IOException {
      this.a = var1;
      this.j = var2;
      ByteBuffer var4 = ByteBuffer.allocate(4);
      ByteOrder var3;
      if(var1) {
         var3 = ByteOrder.BIG_ENDIAN;
      } else {
         var3 = ByteOrder.LITTLE_ENDIAN;
      }

      var4.order(var3);
      this.b = var2.d(var4, 16L);
      this.c = var2.c(var4, 28L);
      this.d = var2.c(var4, 32L);
      this.e = var2.d(var4, 42L);
      this.f = var2.d(var4, 44L);
      this.g = var2.d(var4, 46L);
      this.h = var2.d(var4, 48L);
      this.i = var2.d(var4, 50L);
   }

   public c.a a(long var1, int var3) throws IOException {
      return new a(this.j, this, var1, var3);
   }

   public c.c a(long var1) throws IOException {
      return new g(this.j, this, var1);
   }

   public c.d a(int var1) throws IOException {
      return new i(this.j, this, var1);
   }
}
