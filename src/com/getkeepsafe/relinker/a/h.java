package com.getkeepsafe.relinker.a;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class h extends c.c {
   public h(f var1, c.b var2, long var3) throws IOException {
      ByteBuffer var6 = ByteBuffer.allocate(8);
      ByteOrder var5;
      if(var2.a) {
         var5 = ByteOrder.BIG_ENDIAN;
      } else {
         var5 = ByteOrder.LITTLE_ENDIAN;
      }

      var6.order(var5);
      var3 = var2.c + (long)var2.e * var3;
      this.a = var1.c(var6, var3);
      this.b = var1.b(var6, 8L + var3);
      this.c = var1.b(var6, 16L + var3);
      this.d = var1.b(var6, var3 + 40L);
   }
}
