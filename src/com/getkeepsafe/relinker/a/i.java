package com.getkeepsafe.relinker.a;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class i extends c.d {
   public i(f var1, c.b var2, int var3) throws IOException {
      ByteBuffer var5 = ByteBuffer.allocate(4);
      ByteOrder var4;
      if(var2.a) {
         var4 = ByteOrder.BIG_ENDIAN;
      } else {
         var4 = ByteOrder.LITTLE_ENDIAN;
      }

      var5.order(var4);
      this.a = var1.c(var5, var2.d + (long)(var2.g * var3) + 28L);
   }
}
