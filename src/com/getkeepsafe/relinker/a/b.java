package com.getkeepsafe.relinker.a;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class b extends c.a {
   public b(f var1, c.b var2, long var3, int var5) throws IOException {
      ByteBuffer var6 = ByteBuffer.allocate(4);
      ByteOrder var7;
      if(var2.a) {
         var7 = ByteOrder.BIG_ENDIAN;
      } else {
         var7 = ByteOrder.LITTLE_ENDIAN;
      }

      var6.order(var7);
      var3 += (long)(var5 * 16);
      this.a = var1.b(var6, var3);
      this.b = var1.b(var6, var3 + 8L);
   }
}
