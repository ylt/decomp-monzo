package com.getkeepsafe.relinker.a;

import java.io.Closeable;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class f implements c, Closeable {
   private final int a = 1179403647;
   private final FileChannel b;

   public f(File var1) throws FileNotFoundException {
      if(var1 != null && var1.exists()) {
         this.b = (new FileInputStream(var1)).getChannel();
      } else {
         throw new IllegalArgumentException("File is null or does not exist");
      }
   }

   private long a(c.b var1, long var2, long var4) throws IOException {
      for(long var6 = 0L; var6 < var2; ++var6) {
         c.c var8 = var1.a(var6);
         if(var8.a == 1L && var8.c <= var4 && var4 <= var8.c + var8.d) {
            return var4 - var8.c + var8.b;
         }
      }

      throw new IllegalStateException("Could not map vma to file offset!");
   }

   public c.b a() throws IOException {
      this.b.position(0L);
      ByteBuffer var3 = ByteBuffer.allocate(8);
      var3.order(ByteOrder.LITTLE_ENDIAN);
      if(this.c(var3, 0L) != 1179403647L) {
         throw new IllegalArgumentException("Invalid ELF Magic!");
      } else {
         short var1 = this.e(var3, 4L);
         boolean var2;
         if(this.e(var3, 5L) == 2) {
            var2 = true;
         } else {
            var2 = false;
         }

         Object var4;
         if(var1 == 1) {
            var4 = new d(var2, this);
         } else {
            if(var1 != 2) {
               throw new IllegalStateException("Invalid class type!");
            }

            var4 = new e(var2, this);
         }

         return (c.b)var4;
      }
   }

   protected String a(ByteBuffer var1, long var2) throws IOException {
      StringBuilder var5 = new StringBuilder();

      while(true) {
         short var4 = this.e(var1, var2);
         if(var4 == 0) {
            return var5.toString();
         }

         var5.append((char)var4);
         ++var2;
      }
   }

   protected void a(ByteBuffer var1, long var2, int var4) throws IOException {
      var1.position(0);
      var1.limit(var4);

      int var5;
      for(long var6 = 0L; var6 < (long)var4; var6 += (long)var5) {
         var5 = this.b.read(var1, var2 + var6);
         if(var5 == -1) {
            throw new EOFException();
         }
      }

      var1.position(0);
   }

   protected long b(ByteBuffer var1, long var2) throws IOException {
      this.a(var1, var2, 8);
      return var1.getLong();
   }

   public List b() throws IOException {
      this.b.position(0L);
      ArrayList var11 = new ArrayList();
      c.b var13 = this.a();
      ByteBuffer var12 = ByteBuffer.allocate(8);
      ByteOrder var10;
      if(var13.a) {
         var10 = ByteOrder.BIG_ENDIAN;
      } else {
         var10 = ByteOrder.LITTLE_ENDIAN;
      }

      var12.order(var10);
      long var4 = (long)var13.f;
      long var2 = var4;
      if(var4 == 65535L) {
         var2 = var13.a(0).a;
      }

      var4 = 0L;

      while(true) {
         if(var4 >= var2) {
            var4 = 0L;
            break;
         }

         c.c var15 = var13.a(var4);
         if(var15.a == 2L) {
            var4 = var15.b;
            break;
         }

         ++var4;
      }

      Object var16;
      if(var4 == 0L) {
         var16 = Collections.unmodifiableList(var11);
      } else {
         int var1 = 0;
         ArrayList var14 = new ArrayList();
         long var8 = 0L;

         long var6;
         c.a var17;
         do {
            var17 = var13.a(var4, var1);
            if(var17.a == 1L) {
               var14.add(Long.valueOf(var17.b));
               var6 = var8;
            } else {
               var6 = var8;
               if(var17.a == 5L) {
                  var6 = var17.b;
               }
            }

            ++var1;
            var8 = var6;
         } while(var17.a != 0L);

         if(var6 == 0L) {
            throw new IllegalStateException("String table offset not found!");
         }

         var2 = this.a(var13, var2, var6);
         Iterator var18 = var14.iterator();

         while(var18.hasNext()) {
            var11.add(this.a(var12, ((Long)var18.next()).longValue() + var2));
         }

         var16 = var11;
      }

      return (List)var16;
   }

   protected long c(ByteBuffer var1, long var2) throws IOException {
      this.a(var1, var2, 4);
      return (long)var1.getInt() & 4294967295L;
   }

   public void close() throws IOException {
      this.b.close();
   }

   protected int d(ByteBuffer var1, long var2) throws IOException {
      this.a(var1, var2, 2);
      return var1.getShort() & '\uffff';
   }

   protected short e(ByteBuffer var1, long var2) throws IOException {
      this.a(var1, var2, 1);
      return (short)(var1.get() & 255);
   }
}
