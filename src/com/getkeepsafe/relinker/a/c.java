package com.getkeepsafe.relinker.a;

import java.io.IOException;

public interface c {
   public abstract static class a {
      public long a;
      public long b;
   }

   public abstract static class b {
      public boolean a;
      public int b;
      public long c;
      public long d;
      public int e;
      public int f;
      public int g;
      public int h;
      public int i;

      public abstract c.a a(long var1, int var3) throws IOException;

      public abstract c.c a(long var1) throws IOException;

      public abstract c.d a(int var1) throws IOException;
   }

   public abstract static class c {
      public long a;
      public long b;
      public long c;
      public long d;
   }

   public abstract static class d {
      public long a;
   }
}
