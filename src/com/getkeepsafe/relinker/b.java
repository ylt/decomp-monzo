package com.getkeepsafe.relinker;

import android.content.Context;
import java.io.File;

public class b {
   public static void a(Context var0, String var1, String var2) {
      a(var0, var1, var2, (b.c)null);
   }

   public static void a(Context var0, String var1, String var2, b.c var3) {
      (new c()).a(var0, var1, var2, var3);
   }

   public interface a {
      void a(Context var1, String[] var2, String var3, File var4, c var5);
   }

   public interface b {
      void a(String var1);

      String[] a();

      void b(String var1);

      String c(String var1);

      String d(String var1);
   }

   public interface c {
      void a();

      void a(Throwable var1);
   }

   public interface d {
      void a(String var1);
   }
}
