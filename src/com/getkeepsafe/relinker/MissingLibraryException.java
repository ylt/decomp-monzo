package com.getkeepsafe.relinker;

public class MissingLibraryException extends RuntimeException {
   public MissingLibraryException(String var1) {
      super(var1);
   }
}
