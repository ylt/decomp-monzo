package com.getkeepsafe.relinker;

import android.content.Context;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class a implements b.a {
   private long a(InputStream var1, OutputStream var2) throws IOException {
      long var4 = 0L;
      byte[] var6 = new byte[4096];

      while(true) {
         int var3 = var1.read(var6);
         if(var3 == -1) {
            var2.flush();
            return var4;
         }

         var2.write(var6, 0, var3);
         var4 += (long)var3;
      }
   }

   private void a(Closeable var1) {
      if(var1 != null) {
         try {
            var1.close();
         } catch (IOException var2) {
            ;
         }
      }

   }

   public void a(Context param1, String[] param2, String param3, File param4, c param5) {
      // $FF: Couldn't be decompiled
   }
}
