package com.getkeepsafe.relinker;

import android.content.Context;
import java.io.File;
import java.io.FilenameFilter;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

public class c {
   protected final Set a;
   protected final b.b b;
   protected final b.a c;
   protected boolean d;
   protected boolean e;
   protected b.d f;

   protected c() {
      this(new d(), new a());
   }

   protected c(b.b var1, b.a var2) {
      this.a = new HashSet();
      if(var1 == null) {
         throw new IllegalArgumentException("Cannot pass null library loader");
      } else if(var2 == null) {
         throw new IllegalArgumentException("Cannot pass null library installer");
      } else {
         this.b = var1;
         this.c = var2;
      }
   }

   private void c(Context param1, String param2, String param3) {
      // $FF: Couldn't be decompiled
   }

   protected File a(Context var1) {
      return var1.getDir("lib", 0);
   }

   protected File a(Context var1, String var2, String var3) {
      var2 = this.b.c(var2);
      File var4;
      if(e.a(var3)) {
         var4 = new File(this.a(var1), var2);
      } else {
         var4 = new File(this.a(var1), var2 + "." + var3);
      }

      return var4;
   }

   public void a(Context var1, String var2) {
      this.a((Context)var1, (String)var2, (String)null, (b.c)null);
   }

   public void a(final Context var1, final String var2, final String var3, final b.c var4) {
      if(var1 == null) {
         throw new IllegalArgumentException("Given context is null");
      } else if(e.a(var2)) {
         throw new IllegalArgumentException("Given library is either null or empty");
      } else {
         this.a("Beginning load of %s...", new Object[]{var2});
         if(var4 == null) {
            this.c(var1, var2, var3);
         } else {
            (new Thread(new Runnable() {
               public void run() {
                  try {
                     c.this.c(var1, var2, var3);
                     var4.a();
                  } catch (UnsatisfiedLinkError var2x) {
                     var4.a(var2x);
                  } catch (MissingLibraryException var3x) {
                     var4.a(var3x);
                  }

               }
            })).start();
         }

      }
   }

   public void a(String var1) {
      if(this.f != null) {
         this.f.a(var1);
      }

   }

   public void a(String var1, Object... var2) {
      this.a(String.format(Locale.US, var1, var2));
   }

   protected void b(Context var1, String var2, String var3) {
      File var6 = this.a(var1);
      File var7 = this.a(var1, var2, var3);
      File[] var9 = var6.listFiles(new FilenameFilter(this.b.c(var2)) {
         // $FF: synthetic field
         final String a;

         {
            this.a = var2;
         }

         public boolean accept(File var1, String var2) {
            return var2.startsWith(this.a);
         }
      });
      if(var9 != null) {
         int var5 = var9.length;

         for(int var4 = 0; var4 < var5; ++var4) {
            File var8 = var9[var4];
            if(this.d || !var8.getAbsolutePath().equals(var7.getAbsolutePath())) {
               var8.delete();
            }
         }
      }

   }
}
