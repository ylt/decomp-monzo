package com.bumptech.glide.i;

public class g {
   private Class a;
   private Class b;

   public g() {
   }

   public g(Class var1, Class var2) {
      this.a(var1, var2);
   }

   public void a(Class var1, Class var2) {
      this.a = var1;
      this.b = var2;
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(this != var1) {
         if(var1 != null && this.getClass() == var1.getClass()) {
            g var3 = (g)var1;
            if(!this.a.equals(var3.a)) {
               var2 = false;
            } else if(!this.b.equals(var3.b)) {
               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public int hashCode() {
      return this.a.hashCode() * 31 + this.b.hashCode();
   }

   public String toString() {
      return "MultiClassKey{first=" + this.a + ", second=" + this.b + '}';
   }
}
