package com.bumptech.glide.i;

import java.util.LinkedHashMap;
import java.util.Map.Entry;

public class e {
   private final LinkedHashMap a = new LinkedHashMap(100, 0.75F, true);
   private int b;
   private final int c;
   private int d = 0;

   public e(int var1) {
      this.c = var1;
      this.b = var1;
   }

   private void c() {
      this.b(this.b);
   }

   protected int a(Object var1) {
      return 1;
   }

   public void a() {
      this.b(0);
   }

   protected void a(Object var1, Object var2) {
   }

   public int b() {
      return this.d;
   }

   public Object b(Object var1) {
      return this.a.get(var1);
   }

   public Object b(Object var1, Object var2) {
      if(this.a(var2) >= this.b) {
         this.a(var1, var2);
         var1 = null;
      } else {
         var1 = this.a.put(var1, var2);
         if(var2 != null) {
            this.d += this.a(var2);
         }

         if(var1 != null) {
            this.d -= this.a(var1);
         }

         this.c();
      }

      return var1;
   }

   protected void b(int var1) {
      while(this.d > var1) {
         Entry var3 = (Entry)this.a.entrySet().iterator().next();
         Object var2 = var3.getValue();
         this.d -= this.a(var2);
         Object var4 = var3.getKey();
         this.a.remove(var4);
         this.a(var4, var2);
      }

   }

   public Object c(Object var1) {
      var1 = this.a.remove(var1);
      if(var1 != null) {
         this.d -= this.a(var1);
      }

      return var1;
   }
}
