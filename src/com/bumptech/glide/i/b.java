package com.bumptech.glide.i;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public final class b extends FilterInputStream {
   private final long a;
   private int b;

   b(InputStream var1, long var2) {
      super(var1);
      this.a = var2;
   }

   private int a(int var1) throws IOException {
      if(var1 >= 0) {
         this.b += var1;
      } else if(this.a - (long)this.b > 0L) {
         throw new IOException("Failed to read all expected data, expected: " + this.a + ", but read: " + this.b);
      }

      return var1;
   }

   public static InputStream a(InputStream var0, long var1) {
      return new b(var0, var1);
   }

   public int available() throws IOException {
      synchronized(this){}
      boolean var6 = false;

      long var2;
      try {
         var6 = true;
         var2 = Math.max(this.a - (long)this.b, (long)this.in.available());
         var6 = false;
      } finally {
         if(var6) {
            ;
         }
      }

      int var1 = (int)var2;
      return var1;
   }

   public int read() throws IOException {
      synchronized(this){}

      int var1;
      try {
         var1 = this.a(super.read());
      } finally {
         ;
      }

      return var1;
   }

   public int read(byte[] var1) throws IOException {
      return this.read(var1, 0, var1.length);
   }

   public int read(byte[] var1, int var2, int var3) throws IOException {
      synchronized(this){}

      try {
         var2 = this.a(super.read(var1, var2, var3));
      } finally {
         ;
      }

      return var2;
   }
}
