package com.bumptech.glide.i;

import java.io.IOException;
import java.io.InputStream;
import java.util.Queue;

public class c extends InputStream {
   private static final Queue a = h.a(0);
   private InputStream b;
   private IOException c;

   public static c a(InputStream param0) {
      // $FF: Couldn't be decompiled
   }

   public IOException a() {
      return this.c;
   }

   public int available() throws IOException {
      return this.b.available();
   }

   public void b() {
      // $FF: Couldn't be decompiled
   }

   void b(InputStream var1) {
      this.b = var1;
   }

   public void close() throws IOException {
      this.b.close();
   }

   public void mark(int var1) {
      this.b.mark(var1);
   }

   public boolean markSupported() {
      return this.b.markSupported();
   }

   public int read() throws IOException {
      int var1;
      try {
         var1 = this.b.read();
      } catch (IOException var3) {
         this.c = var3;
         var1 = -1;
      }

      return var1;
   }

   public int read(byte[] var1) throws IOException {
      int var2;
      try {
         var2 = this.b.read(var1);
      } catch (IOException var3) {
         this.c = var3;
         var2 = -1;
      }

      return var2;
   }

   public int read(byte[] var1, int var2, int var3) throws IOException {
      try {
         var2 = this.b.read(var1, var2, var3);
      } catch (IOException var4) {
         this.c = var4;
         var2 = -1;
      }

      return var2;
   }

   public void reset() throws IOException {
      synchronized(this){}

      try {
         this.b.reset();
      } finally {
         ;
      }

   }

   public long skip(long var1) throws IOException {
      try {
         var1 = this.b.skip(var1);
      } catch (IOException var4) {
         this.c = var4;
         var1 = 0L;
      }

      return var1;
   }
}
