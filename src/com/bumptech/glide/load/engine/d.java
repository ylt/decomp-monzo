package com.bumptech.glide.load.engine;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Handler.Callback;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

class d implements i.a {
   private static final d.a a = new d.a();
   private static final Handler b = new Handler(Looper.getMainLooper(), new d.b());
   private final List c;
   private final d.a d;
   private final e e;
   private final com.bumptech.glide.load.c f;
   private final ExecutorService g;
   private final ExecutorService h;
   private final boolean i;
   private boolean j;
   private k k;
   private boolean l;
   private Exception m;
   private boolean n;
   private Set o;
   private i p;
   private h q;
   private volatile Future r;

   public d(com.bumptech.glide.load.c var1, ExecutorService var2, ExecutorService var3, boolean var4, e var5) {
      this(var1, var2, var3, var4, var5, a);
   }

   public d(com.bumptech.glide.load.c var1, ExecutorService var2, ExecutorService var3, boolean var4, e var5, d.a var6) {
      this.c = new ArrayList();
      this.f = var1;
      this.g = var2;
      this.h = var3;
      this.i = var4;
      this.e = var5;
      this.d = var6;
   }

   private void b() {
      if(this.j) {
         this.k.d();
      } else {
         if(this.c.isEmpty()) {
            throw new IllegalStateException("Received a resource without any callbacks to notify");
         }

         this.q = this.d.a(this.k, this.i);
         this.l = true;
         this.q.e();
         this.e.a(this.f, this.q);
         Iterator var2 = this.c.iterator();

         while(var2.hasNext()) {
            com.bumptech.glide.g.e var1 = (com.bumptech.glide.g.e)var2.next();
            if(!this.d(var1)) {
               this.q.e();
               var1.a((k)this.q);
            }
         }

         this.q.f();
      }

   }

   private void c() {
      if(!this.j) {
         if(this.c.isEmpty()) {
            throw new IllegalStateException("Received an exception without any callbacks to notify");
         }

         this.n = true;
         this.e.a((com.bumptech.glide.load.c)this.f, (h)null);
         Iterator var1 = this.c.iterator();

         while(var1.hasNext()) {
            com.bumptech.glide.g.e var2 = (com.bumptech.glide.g.e)var1.next();
            if(!this.d(var2)) {
               var2.a(this.m);
            }
         }
      }

   }

   private void c(com.bumptech.glide.g.e var1) {
      if(this.o == null) {
         this.o = new HashSet();
      }

      this.o.add(var1);
   }

   private boolean d(com.bumptech.glide.g.e var1) {
      boolean var2;
      if(this.o != null && this.o.contains(var1)) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   void a() {
      if(!this.n && !this.l && !this.j) {
         this.p.a();
         Future var1 = this.r;
         if(var1 != null) {
            var1.cancel(true);
         }

         this.j = true;
         this.e.a(this, this.f);
      }

   }

   public void a(com.bumptech.glide.g.e var1) {
      com.bumptech.glide.i.h.a();
      if(this.l) {
         var1.a((k)this.q);
      } else if(this.n) {
         var1.a(this.m);
      } else {
         this.c.add(var1);
      }

   }

   public void a(i var1) {
      this.p = var1;
      this.r = this.g.submit(var1);
   }

   public void a(k var1) {
      this.k = var1;
      b.obtainMessage(1, this).sendToTarget();
   }

   public void a(Exception var1) {
      this.m = var1;
      b.obtainMessage(2, this).sendToTarget();
   }

   public void b(com.bumptech.glide.g.e var1) {
      com.bumptech.glide.i.h.a();
      if(!this.l && !this.n) {
         this.c.remove(var1);
         if(this.c.isEmpty()) {
            this.a();
         }
      } else {
         this.c(var1);
      }

   }

   public void b(i var1) {
      this.r = this.h.submit(var1);
   }

   static class a {
      public h a(k var1, boolean var2) {
         return new h(var1, var2);
      }
   }

   private static class b implements Callback {
      private b() {
      }

      // $FF: synthetic method
      b(Object var1) {
         this();
      }

      public boolean handleMessage(Message var1) {
         boolean var2;
         if(1 != var1.what && 2 != var1.what) {
            var2 = false;
         } else {
            d var3 = (d)var1.obj;
            if(1 == var1.what) {
               var3.b();
            } else {
               var3.c();
            }

            var2 = true;
         }

         return var2;
      }
   }
}
