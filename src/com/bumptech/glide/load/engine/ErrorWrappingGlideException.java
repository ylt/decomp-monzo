package com.bumptech.glide.load.engine;

public class ErrorWrappingGlideException extends Exception {
   public ErrorWrappingGlideException(Error var1) {
      super(var1);
      if(var1 == null) {
         throw new NullPointerException("The causing error must not be null");
      }
   }

   public Error a() {
      return (Error)super.getCause();
   }

   // $FF: synthetic method
   public Throwable getCause() {
      return this.a();
   }
}
