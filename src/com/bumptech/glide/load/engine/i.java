package com.bumptech.glide.load.engine;

import android.util.Log;

class i implements com.bumptech.glide.load.engine.c.b, Runnable {
   private final com.bumptech.glide.i a;
   private final i.a b;
   private final a c;
   private i.b d;
   private volatile boolean e;

   public i(i.a var1, a var2, com.bumptech.glide.i var3) {
      this.b = var1;
      this.c = var2;
      this.d = i.b.a;
      this.a = var3;
   }

   private void a(k var1) {
      this.b.a(var1);
   }

   private void a(Exception var1) {
      if(this.c()) {
         this.d = i.b.b;
         this.b.b(this);
      } else {
         this.b.a(var1);
      }

   }

   private boolean c() {
      boolean var1;
      if(this.d == i.b.a) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   private k d() throws Exception {
      k var1;
      if(this.c()) {
         var1 = this.e();
      } else {
         var1 = this.f();
      }

      return var1;
   }

   private k e() throws Exception {
      k var1;
      try {
         var1 = this.c.a();
      } catch (Exception var3) {
         if(Log.isLoggable("EngineRunnable", 3)) {
            Log.d("EngineRunnable", "Exception decoding result from cache: " + var3);
         }

         var1 = null;
      }

      k var2 = var1;
      if(var1 == null) {
         var2 = this.c.b();
      }

      return var2;
   }

   private k f() throws Exception {
      return this.c.c();
   }

   public void a() {
      this.e = true;
      this.c.d();
   }

   public int b() {
      return this.a.ordinal();
   }

   public void run() {
      Object var2 = null;
      if(!this.e) {
         k var1;
         try {
            var1 = this.d();
         } catch (OutOfMemoryError var3) {
            if(Log.isLoggable("EngineRunnable", 2)) {
               Log.v("EngineRunnable", "Out Of Memory Error decoding", var3);
            }

            var2 = new ErrorWrappingGlideException(var3);
            var1 = null;
         } catch (Exception var4) {
            var2 = var4;
            if(Log.isLoggable("EngineRunnable", 2)) {
               Log.v("EngineRunnable", "Exception decoding", var4);
            }

            var1 = null;
         }

         if(this.e) {
            if(var1 != null) {
               var1.d();
            }
         } else if(var1 == null) {
            this.a((Exception)var2);
         } else {
            this.a(var1);
         }
      }

   }

   interface a extends com.bumptech.glide.g.e {
      void b(i var1);
   }

   private static enum b {
      a,
      b;
   }
}
