package com.bumptech.glide.load.engine;

public enum b {
   a(true, true),
   b(false, false),
   c(true, false),
   d(false, true);

   private final boolean e;
   private final boolean f;

   private b(boolean var3, boolean var4) {
      this.e = var3;
      this.f = var4;
   }

   public boolean a() {
      return this.e;
   }

   public boolean b() {
      return this.f;
   }
}
