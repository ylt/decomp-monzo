package com.bumptech.glide.load.engine;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.security.MessageDigest;

class f implements com.bumptech.glide.load.c {
   private final String a;
   private final int b;
   private final int c;
   private final com.bumptech.glide.load.e d;
   private final com.bumptech.glide.load.e e;
   private final com.bumptech.glide.load.g f;
   private final com.bumptech.glide.load.f g;
   private final com.bumptech.glide.load.resource.e.c h;
   private final com.bumptech.glide.load.b i;
   private final com.bumptech.glide.load.c j;
   private String k;
   private int l;
   private com.bumptech.glide.load.c m;

   public f(String var1, com.bumptech.glide.load.c var2, int var3, int var4, com.bumptech.glide.load.e var5, com.bumptech.glide.load.e var6, com.bumptech.glide.load.g var7, com.bumptech.glide.load.f var8, com.bumptech.glide.load.resource.e.c var9, com.bumptech.glide.load.b var10) {
      this.a = var1;
      this.j = var2;
      this.b = var3;
      this.c = var4;
      this.d = var5;
      this.e = var6;
      this.f = var7;
      this.g = var8;
      this.h = var9;
      this.i = var10;
   }

   public com.bumptech.glide.load.c a() {
      if(this.m == null) {
         this.m = new j(this.a, this.j);
      }

      return this.m;
   }

   public void a(MessageDigest var1) throws UnsupportedEncodingException {
      byte[] var2 = ByteBuffer.allocate(8).putInt(this.b).putInt(this.c).array();
      this.j.a(var1);
      var1.update(this.a.getBytes("UTF-8"));
      var1.update(var2);
      String var3;
      if(this.d != null) {
         var3 = this.d.a();
      } else {
         var3 = "";
      }

      var1.update(var3.getBytes("UTF-8"));
      if(this.e != null) {
         var3 = this.e.a();
      } else {
         var3 = "";
      }

      var1.update(var3.getBytes("UTF-8"));
      if(this.f != null) {
         var3 = this.f.a();
      } else {
         var3 = "";
      }

      var1.update(var3.getBytes("UTF-8"));
      if(this.g != null) {
         var3 = this.g.a();
      } else {
         var3 = "";
      }

      var1.update(var3.getBytes("UTF-8"));
      if(this.i != null) {
         var3 = this.i.a();
      } else {
         var3 = "";
      }

      var1.update(var3.getBytes("UTF-8"));
   }

   public boolean equals(Object var1) {
      boolean var5 = false;
      boolean var4;
      if(this == var1) {
         var4 = true;
      } else {
         var4 = var5;
         if(var1 != null) {
            var4 = var5;
            if(this.getClass() == var1.getClass()) {
               f var6 = (f)var1;
               var4 = var5;
               if(this.a.equals(var6.a)) {
                  var4 = var5;
                  if(this.j.equals(var6.j)) {
                     var4 = var5;
                     if(this.c == var6.c) {
                        var4 = var5;
                        if(this.b == var6.b) {
                           boolean var2;
                           if(this.f == null) {
                              var2 = true;
                           } else {
                              var2 = false;
                           }

                           boolean var3;
                           if(var6.f == null) {
                              var3 = true;
                           } else {
                              var3 = false;
                           }

                           var4 = var5;
                           if(!(var2 ^ var3)) {
                              if(this.f != null) {
                                 var4 = var5;
                                 if(!this.f.a().equals(var6.f.a())) {
                                    return var4;
                                 }
                              }

                              if(this.e == null) {
                                 var2 = true;
                              } else {
                                 var2 = false;
                              }

                              if(var6.e == null) {
                                 var3 = true;
                              } else {
                                 var3 = false;
                              }

                              var4 = var5;
                              if(!(var2 ^ var3)) {
                                 if(this.e != null) {
                                    var4 = var5;
                                    if(!this.e.a().equals(var6.e.a())) {
                                       return var4;
                                    }
                                 }

                                 if(this.d == null) {
                                    var2 = true;
                                 } else {
                                    var2 = false;
                                 }

                                 if(var6.d == null) {
                                    var3 = true;
                                 } else {
                                    var3 = false;
                                 }

                                 var4 = var5;
                                 if(!(var2 ^ var3)) {
                                    if(this.d != null) {
                                       var4 = var5;
                                       if(!this.d.a().equals(var6.d.a())) {
                                          return var4;
                                       }
                                    }

                                    if(this.g == null) {
                                       var2 = true;
                                    } else {
                                       var2 = false;
                                    }

                                    if(var6.g == null) {
                                       var3 = true;
                                    } else {
                                       var3 = false;
                                    }

                                    var4 = var5;
                                    if(!(var2 ^ var3)) {
                                       if(this.g != null) {
                                          var4 = var5;
                                          if(!this.g.a().equals(var6.g.a())) {
                                             return var4;
                                          }
                                       }

                                       if(this.h == null) {
                                          var2 = true;
                                       } else {
                                          var2 = false;
                                       }

                                       if(var6.h == null) {
                                          var3 = true;
                                       } else {
                                          var3 = false;
                                       }

                                       var4 = var5;
                                       if(!(var2 ^ var3)) {
                                          if(this.h != null) {
                                             var4 = var5;
                                             if(!this.h.a().equals(var6.h.a())) {
                                                return var4;
                                             }
                                          }

                                          if(this.i == null) {
                                             var2 = true;
                                          } else {
                                             var2 = false;
                                          }

                                          if(var6.i == null) {
                                             var3 = true;
                                          } else {
                                             var3 = false;
                                          }

                                          var4 = var5;
                                          if(!(var2 ^ var3)) {
                                             if(this.i != null) {
                                                var4 = var5;
                                                if(!this.i.a().equals(var6.i.a())) {
                                                   return var4;
                                                }
                                             }

                                             var4 = true;
                                          }
                                       }
                                    }
                                 }
                              }
                           }
                        }
                     }
                  }
               }
            }
         }
      }

      return var4;
   }

   public int hashCode() {
      byte var2 = 0;
      if(this.l == 0) {
         this.l = this.a.hashCode();
         this.l = this.l * 31 + this.j.hashCode();
         this.l = this.l * 31 + this.b;
         this.l = this.l * 31 + this.c;
         int var3 = this.l;
         int var1;
         if(this.d != null) {
            var1 = this.d.a().hashCode();
         } else {
            var1 = 0;
         }

         this.l = var1 + var3 * 31;
         var3 = this.l;
         if(this.e != null) {
            var1 = this.e.a().hashCode();
         } else {
            var1 = 0;
         }

         this.l = var1 + var3 * 31;
         var3 = this.l;
         if(this.f != null) {
            var1 = this.f.a().hashCode();
         } else {
            var1 = 0;
         }

         this.l = var1 + var3 * 31;
         var3 = this.l;
         if(this.g != null) {
            var1 = this.g.a().hashCode();
         } else {
            var1 = 0;
         }

         this.l = var1 + var3 * 31;
         var3 = this.l;
         if(this.h != null) {
            var1 = this.h.a().hashCode();
         } else {
            var1 = 0;
         }

         this.l = var1 + var3 * 31;
         var3 = this.l;
         var1 = var2;
         if(this.i != null) {
            var1 = this.i.a().hashCode();
         }

         this.l = var3 * 31 + var1;
      }

      return this.l;
   }

   public String toString() {
      if(this.k == null) {
         StringBuilder var2 = (new StringBuilder()).append("EngineKey{").append(this.a).append('+').append(this.j).append("+[").append(this.b).append('x').append(this.c).append("]+").append('\'');
         String var1;
         if(this.d != null) {
            var1 = this.d.a();
         } else {
            var1 = "";
         }

         var2 = var2.append(var1).append('\'').append('+').append('\'');
         if(this.e != null) {
            var1 = this.e.a();
         } else {
            var1 = "";
         }

         var2 = var2.append(var1).append('\'').append('+').append('\'');
         if(this.f != null) {
            var1 = this.f.a();
         } else {
            var1 = "";
         }

         var2 = var2.append(var1).append('\'').append('+').append('\'');
         if(this.g != null) {
            var1 = this.g.a();
         } else {
            var1 = "";
         }

         var2 = var2.append(var1).append('\'').append('+').append('\'');
         if(this.h != null) {
            var1 = this.h.a();
         } else {
            var1 = "";
         }

         var2 = var2.append(var1).append('\'').append('+').append('\'');
         if(this.i != null) {
            var1 = this.i.a();
         } else {
            var1 = "";
         }

         this.k = var2.append(var1).append('\'').append('}').toString();
      }

      return this.k;
   }
}
