package com.bumptech.glide.load.engine.c;

import android.os.Process;
import android.util.Log;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.RunnableFuture;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class a extends ThreadPoolExecutor {
   private final AtomicInteger a;
   private final a.c b;

   public a(int var1) {
      this(var1, a.c.b);
   }

   public a(int var1, int var2, long var3, TimeUnit var5, ThreadFactory var6, a.c var7) {
      super(var1, var2, var3, var5, new PriorityBlockingQueue(), var6);
      this.a = new AtomicInteger();
      this.b = var7;
   }

   public a(int var1, a.c var2) {
      this(var1, var1, 0L, TimeUnit.MILLISECONDS, new a.a(), var2);
   }

   protected void afterExecute(Runnable var1, Throwable var2) {
      super.afterExecute(var1, var2);
      if(var2 == null && var1 instanceof Future) {
         Future var5 = (Future)var1;
         if(var5.isDone() && !var5.isCancelled()) {
            try {
               var5.get();
            } catch (InterruptedException var3) {
               this.b.a(var3);
            } catch (ExecutionException var4) {
               this.b.a(var4);
            }
         }
      }

   }

   protected RunnableFuture newTaskFor(Runnable var1, Object var2) {
      return new a.b(var1, var2, this.a.getAndIncrement());
   }

   public static class a implements ThreadFactory {
      int a = 0;

      public Thread newThread(Runnable var1) {
         Thread var2 = new Thread(var1, "fifo-pool-thread-" + this.a) {
            public void run() {
               Process.setThreadPriority(10);
               super.run();
            }
         };
         ++this.a;
         return var2;
      }
   }

   static class b extends FutureTask implements Comparable {
      private final int a;
      private final int b;

      public b(Runnable var1, Object var2, int var3) {
         super(var1, var2);
         if(!(var1 instanceof b)) {
            throw new IllegalArgumentException("FifoPriorityThreadPoolExecutor must be given Runnables that implement Prioritized");
         } else {
            this.a = ((b)var1).b();
            this.b = var3;
         }
      }

      public int a(a.b var1) {
         int var3 = this.a - var1.a;
         int var2 = var3;
         if(var3 == 0) {
            var2 = this.b - var1.b;
         }

         return var2;
      }

      // $FF: synthetic method
      public int compareTo(Object var1) {
         return this.a((a.b)var1);
      }

      public boolean equals(Object var1) {
         boolean var3 = false;
         boolean var2 = var3;
         if(var1 instanceof a.b) {
            a.b var4 = (a.b)var1;
            var2 = var3;
            if(this.b == var4.b) {
               var2 = var3;
               if(this.a == var4.a) {
                  var2 = true;
               }
            }
         }

         return var2;
      }

      public int hashCode() {
         return this.a * 31 + this.b;
      }
   }

   public static enum c {
      a,
      b {
         protected void a(Throwable var1) {
            if(Log.isLoggable("PriorityExecutor", 6)) {
               Log.e("PriorityExecutor", "Request threw uncaught throwable", var1);
            }

         }
      },
      c {
         protected void a(Throwable var1) {
            super.a(var1);
            throw new RuntimeException(var1);
         }
      };

      private c() {
      }

      // $FF: synthetic method
      c(Object var3) {
         this();
      }

      protected void a(Throwable var1) {
      }
   }
}
