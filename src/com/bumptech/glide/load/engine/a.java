package com.bumptech.glide.load.engine;

import android.util.Log;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

class a {
   private static final a.b a = new a.b();
   private final f b;
   private final int c;
   private final int d;
   private final com.bumptech.glide.load.a.c e;
   private final com.bumptech.glide.f.b f;
   private final com.bumptech.glide.load.g g;
   private final com.bumptech.glide.load.resource.e.c h;
   private final a.a i;
   private final b j;
   private final com.bumptech.glide.i k;
   private final a.b l;
   private volatile boolean m;

   public a(f var1, int var2, int var3, com.bumptech.glide.load.a.c var4, com.bumptech.glide.f.b var5, com.bumptech.glide.load.g var6, com.bumptech.glide.load.resource.e.c var7, a.a var8, b var9, com.bumptech.glide.i var10) {
      this(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10, a);
   }

   a(f var1, int var2, int var3, com.bumptech.glide.load.a.c var4, com.bumptech.glide.f.b var5, com.bumptech.glide.load.g var6, com.bumptech.glide.load.resource.e.c var7, a.a var8, b var9, com.bumptech.glide.i var10, a.b var11) {
      this.b = var1;
      this.c = var2;
      this.d = var3;
      this.e = var4;
      this.f = var5;
      this.g = var6;
      this.h = var7;
      this.i = var8;
      this.j = var9;
      this.k = var10;
      this.l = var11;
   }

   // $FF: synthetic method
   static a.b a(a var0) {
      return var0.l;
   }

   private k a(com.bumptech.glide.load.c var1) throws IOException {
      k var2 = null;
      File var3 = this.i.a().a(var1);
      if(var3 != null) {
         boolean var5 = false;

         k var7;
         try {
            var5 = true;
            var7 = this.f.a().a(var3, this.c, this.d);
            var5 = false;
         } finally {
            if(var5) {
               if(true) {
                  this.i.a().b(var1);
               }

            }
         }

         var2 = var7;
         if(var7 == null) {
            this.i.a().b(var1);
            var2 = var7;
         }
      }

      return var2;
   }

   private k a(k var1) {
      long var2 = com.bumptech.glide.i.d.a();
      var1 = this.c(var1);
      if(Log.isLoggable("DecodeJob", 2)) {
         this.a("Transformed resource from source", var2);
      }

      this.b(var1);
      var2 = com.bumptech.glide.i.d.a();
      var1 = this.d(var1);
      if(Log.isLoggable("DecodeJob", 2)) {
         this.a("Transcoded transformed from source", var2);
      }

      return var1;
   }

   private k a(Object var1) throws IOException {
      k var5;
      if(this.j.a()) {
         var5 = this.b(var1);
      } else {
         long var2 = com.bumptech.glide.i.d.a();
         k var4 = this.f.b().a(var1, this.c, this.d);
         var5 = var4;
         if(Log.isLoggable("DecodeJob", 2)) {
            this.a("Decoded from source", var2);
            var5 = var4;
         }
      }

      return var5;
   }

   private void a(String var1, long var2) {
      Log.v("DecodeJob", var1 + " in " + com.bumptech.glide.i.d.a(var2) + ", key: " + this.b);
   }

   private k b(Object var1) throws IOException {
      long var2 = com.bumptech.glide.i.d.a();
      a.c var4 = new a.c(this.f.c(), var1);
      this.i.a().a(this.b.a(), var4);
      if(Log.isLoggable("DecodeJob", 2)) {
         this.a("Wrote source to cache", var2);
      }

      var2 = com.bumptech.glide.i.d.a();
      k var5 = this.a(this.b.a());
      if(Log.isLoggable("DecodeJob", 2) && var5 != null) {
         this.a("Decoded source from cache", var2);
      }

      return var5;
   }

   private void b(k var1) {
      if(var1 != null && this.j.b()) {
         long var2 = com.bumptech.glide.i.d.a();
         a.c var4 = new a.c(this.f.d(), var1);
         this.i.a().a(this.b, var4);
         if(Log.isLoggable("DecodeJob", 2)) {
            this.a("Wrote transformed from source to cache", var2);
         }
      }

   }

   private k c(k var1) {
      k var2;
      if(var1 == null) {
         var2 = null;
      } else {
         k var3 = this.g.a(var1, this.c, this.d);
         var2 = var3;
         if(!var1.equals(var3)) {
            var1.d();
            var2 = var3;
         }
      }

      return var2;
   }

   private k d(k var1) {
      if(var1 == null) {
         var1 = null;
      } else {
         var1 = this.h.a(var1);
      }

      return var1;
   }

   private k e() throws Exception {
      // $FF: Couldn't be decompiled
   }

   public k a() throws Exception {
      k var3;
      if(!this.j.b()) {
         var3 = null;
      } else {
         long var1 = com.bumptech.glide.i.d.a();
         var3 = this.a((com.bumptech.glide.load.c)this.b);
         if(Log.isLoggable("DecodeJob", 2)) {
            this.a("Decoded transformed from cache", var1);
         }

         var1 = com.bumptech.glide.i.d.a();
         k var4 = this.d(var3);
         var3 = var4;
         if(Log.isLoggable("DecodeJob", 2)) {
            this.a("Transcoded transformed from cache", var1);
            var3 = var4;
         }
      }

      return var3;
   }

   public k b() throws Exception {
      k var3;
      if(!this.j.a()) {
         var3 = null;
      } else {
         long var1 = com.bumptech.glide.i.d.a();
         var3 = this.a(this.b.a());
         if(Log.isLoggable("DecodeJob", 2)) {
            this.a("Decoded source from cache", var1);
         }

         var3 = this.a(var3);
      }

      return var3;
   }

   public k c() throws Exception {
      return this.a(this.e());
   }

   public void d() {
      this.m = true;
      this.e.c();
   }

   interface a {
      com.bumptech.glide.load.engine.b.a a();
   }

   static class b {
      public OutputStream a(File var1) throws FileNotFoundException {
         return new BufferedOutputStream(new FileOutputStream(var1));
      }
   }

   class c implements com.bumptech.glide.load.engine.b.a.b {
      private final com.bumptech.glide.load.b b;
      private final Object c;

      public c(com.bumptech.glide.load.b var2, Object var3) {
         this.b = var2;
         this.c = var3;
      }

      public boolean a(File param1) {
         // $FF: Couldn't be decompiled
      }
   }
}
