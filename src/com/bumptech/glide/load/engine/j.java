package com.bumptech.glide.load.engine;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;

class j implements com.bumptech.glide.load.c {
   private final String a;
   private final com.bumptech.glide.load.c b;

   public j(String var1, com.bumptech.glide.load.c var2) {
      this.a = var1;
      this.b = var2;
   }

   public void a(MessageDigest var1) throws UnsupportedEncodingException {
      var1.update(this.a.getBytes("UTF-8"));
      this.b.a(var1);
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(this != var1) {
         if(var1 != null && this.getClass() == var1.getClass()) {
            j var3 = (j)var1;
            if(!this.a.equals(var3.a)) {
               var2 = false;
            } else if(!this.b.equals(var3.b)) {
               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public int hashCode() {
      return this.a.hashCode() * 31 + this.b.hashCode();
   }
}
