package com.bumptech.glide.load.engine.a;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;

public interface c {
   Bitmap a(int var1, int var2, Config var3);

   void a();

   void a(int var1);

   boolean a(Bitmap var1);

   Bitmap b(int var1, int var2, Config var3);
}
