package com.bumptech.glide.load.engine.a;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.os.Build.VERSION;
import android.util.Log;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class f implements c {
   private static final Config a;
   private final g b;
   private final Set c;
   private final int d;
   private final f.a e;
   private int f;
   private int g;
   private int h;
   private int i;
   private int j;
   private int k;

   static {
      a = Config.ARGB_8888;
   }

   public f(int var1) {
      this(var1, e(), f());
   }

   f(int var1, g var2, Set var3) {
      this.d = var1;
      this.f = var1;
      this.b = var2;
      this.c = var3;
      this.e = new f.b();
   }

   private void b() {
      this.b(this.f);
   }

   private void b(int param1) {
      // $FF: Couldn't be decompiled
   }

   private void c() {
      if(Log.isLoggable("LruBitmapPool", 2)) {
         this.d();
      }

   }

   private void d() {
      Log.v("LruBitmapPool", "Hits=" + this.h + ", misses=" + this.i + ", puts=" + this.j + ", evictions=" + this.k + ", currentSize=" + this.g + ", maxSize=" + this.f + "\nStrategy=" + this.b);
   }

   private static g e() {
      Object var0;
      if(VERSION.SDK_INT >= 19) {
         var0 = new i();
      } else {
         var0 = new a();
      }

      return (g)var0;
   }

   private static Set f() {
      HashSet var0 = new HashSet();
      var0.addAll(Arrays.asList(Config.values()));
      if(VERSION.SDK_INT >= 19) {
         var0.add((Object)null);
      }

      return Collections.unmodifiableSet(var0);
   }

   public Bitmap a(int param1, int param2, Config param3) {
      // $FF: Couldn't be decompiled
   }

   public void a() {
      if(Log.isLoggable("LruBitmapPool", 3)) {
         Log.d("LruBitmapPool", "clearMemory");
      }

      this.b(0);
   }

   @SuppressLint({"InlinedApi"})
   public void a(int var1) {
      if(Log.isLoggable("LruBitmapPool", 3)) {
         Log.d("LruBitmapPool", "trimMemory, level=" + var1);
      }

      if(var1 >= 60) {
         this.a();
      } else if(var1 >= 40) {
         this.b(this.f / 2);
      }

   }

   public boolean a(Bitmap param1) {
      // $FF: Couldn't be decompiled
   }

   @TargetApi(12)
   public Bitmap b(int param1, int param2, Config param3) {
      // $FF: Couldn't be decompiled
   }

   private interface a {
      void a(Bitmap var1);

      void b(Bitmap var1);
   }

   private static class b implements f.a {
      private b() {
      }

      // $FF: synthetic method
      b(Object var1) {
         this();
      }

      public void a(Bitmap var1) {
      }

      public void b(Bitmap var1) {
      }
   }
}
