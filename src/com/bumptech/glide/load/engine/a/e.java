package com.bumptech.glide.load.engine.a;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class e {
   private final e.a a = new e.a();
   private final Map b = new HashMap();

   private void a(e.a var1) {
      d(var1);
      var1.b = this.a;
      var1.a = this.a.a;
      c(var1);
   }

   private void b(e.a var1) {
      d(var1);
      var1.b = this.a.b;
      var1.a = this.a;
      c(var1);
   }

   private static void c(e.a var0) {
      var0.a.b = var0;
      var0.b.a = var0;
   }

   private static void d(e.a var0) {
      var0.b.a = var0.a;
      var0.a.b = var0.b;
   }

   public Object a() {
      e.a var1 = this.a.b;

      Object var3;
      while(true) {
         if(var1.equals(this.a)) {
            var3 = null;
            break;
         }

         Object var2 = var1.a();
         if(var2 != null) {
            var3 = var2;
            break;
         }

         d(var1);
         this.b.remove(var1.c);
         ((h)var1.c).a();
         var1 = var1.b;
      }

      return var3;
   }

   public Object a(h var1) {
      e.a var2 = (e.a)this.b.get(var1);
      e.a var3;
      if(var2 == null) {
         var2 = new e.a(var1);
         this.b.put(var1, var2);
         var3 = var2;
      } else {
         var1.a();
         var3 = var2;
      }

      this.a(var3);
      return var3.a();
   }

   public void a(h var1, Object var2) {
      e.a var3 = (e.a)this.b.get(var1);
      e.a var4;
      if(var3 == null) {
         var3 = new e.a(var1);
         this.b(var3);
         this.b.put(var1, var3);
         var4 = var3;
      } else {
         var1.a();
         var4 = var3;
      }

      var4.a(var2);
   }

   public String toString() {
      StringBuilder var3 = new StringBuilder("GroupedLinkedMap( ");
      e.a var2 = this.a.a;

      boolean var1;
      for(var1 = false; !var2.equals(this.a); var2 = var2.a) {
         var1 = true;
         var3.append('{').append(var2.c).append(':').append(var2.b()).append("}, ");
      }

      if(var1) {
         var3.delete(var3.length() - 2, var3.length());
      }

      return var3.append(" )").toString();
   }

   private static class a {
      e.a a;
      e.a b;
      private final Object c;
      private List d;

      public a() {
         this((Object)null);
      }

      public a(Object var1) {
         this.b = this;
         this.a = this;
         this.c = var1;
      }

      public Object a() {
         int var1 = this.b();
         Object var2;
         if(var1 > 0) {
            var2 = this.d.remove(var1 - 1);
         } else {
            var2 = null;
         }

         return var2;
      }

      public void a(Object var1) {
         if(this.d == null) {
            this.d = new ArrayList();
         }

         this.d.add(var1);
      }

      public int b() {
         int var1;
         if(this.d != null) {
            var1 = this.d.size();
         } else {
            var1 = 0;
         }

         return var1;
      }
   }
}
