package com.bumptech.glide.load.engine.a;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;

class a implements g {
   private final a.b a = new a.b();
   private final e b = new e();

   private static String d(int var0, int var1, Config var2) {
      return "[" + var0 + "x" + var1 + "], " + var2;
   }

   private static String d(Bitmap var0) {
      return d(var0.getWidth(), var0.getHeight(), var0.getConfig());
   }

   public Bitmap a() {
      return (Bitmap)this.b.a();
   }

   public Bitmap a(int var1, int var2, Config var3) {
      a.a var4 = this.a.a(var1, var2, var3);
      return (Bitmap)this.b.a((h)var4);
   }

   public void a(Bitmap var1) {
      a.a var2 = this.a.a(var1.getWidth(), var1.getHeight(), var1.getConfig());
      this.b.a(var2, var1);
   }

   public String b(int var1, int var2, Config var3) {
      return d(var1, var2, var3);
   }

   public String b(Bitmap var1) {
      return d(var1);
   }

   public int c(Bitmap var1) {
      return com.bumptech.glide.i.h.a(var1);
   }

   public String toString() {
      return "AttributeStrategy:\n  " + this.b;
   }

   static class a implements h {
      private final a.b a;
      private int b;
      private int c;
      private Config d;

      public a(a.b var1) {
         this.a = var1;
      }

      public void a() {
         this.a.a(this);
      }

      public void a(int var1, int var2, Config var3) {
         this.b = var1;
         this.c = var2;
         this.d = var3;
      }

      public boolean equals(Object var1) {
         boolean var3 = false;
         boolean var2 = var3;
         if(var1 instanceof a.a) {
            a.a var4 = (a.a)var1;
            var2 = var3;
            if(this.b == var4.b) {
               var2 = var3;
               if(this.c == var4.c) {
                  var2 = var3;
                  if(this.d == var4.d) {
                     var2 = true;
                  }
               }
            }
         }

         return var2;
      }

      public int hashCode() {
         int var2 = this.b;
         int var3 = this.c;
         int var1;
         if(this.d != null) {
            var1 = this.d.hashCode();
         } else {
            var1 = 0;
         }

         return var1 + (var2 * 31 + var3) * 31;
      }

      public String toString() {
         return a.d(this.b, this.c, this.d);
      }
   }

   static class b extends b {
      protected a.a a() {
         return new a.a(this);
      }

      public a.a a(int var1, int var2, Config var3) {
         a.a var4 = (a.a)this.c();
         var4.a(var1, var2, var3);
         return var4;
      }

      // $FF: synthetic method
      protected h b() {
         return this.a();
      }
   }
}
