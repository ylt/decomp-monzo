package com.bumptech.glide.load.engine.a;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;
import java.util.Map.Entry;

@TargetApi(19)
public class i implements g {
   private static final Config[] a;
   private static final Config[] b;
   private static final Config[] c;
   private static final Config[] d;
   private final i.b e = new i.b();
   private final e f = new e();
   private final Map g = new HashMap();

   static {
      a = new Config[]{Config.ARGB_8888, null};
      b = new Config[]{Config.RGB_565};
      c = new Config[]{Config.ARGB_4444};
      d = new Config[]{Config.ALPHA_8};
   }

   private i.a a(i.a var1, int var2, Config var3) {
      Config[] var9 = b(var3);
      int var5 = var9.length;
      int var4 = 0;

      i.a var6;
      while(true) {
         var6 = var1;
         if(var4 >= var5) {
            break;
         }

         Config var8 = var9[var4];
         Integer var7 = (Integer)this.a(var8).ceilingKey(Integer.valueOf(var2));
         if(var7 != null && var7.intValue() <= var2 * 8) {
            if(var7.intValue() == var2) {
               if(var8 == null) {
                  var6 = var1;
                  if(var3 == null) {
                     break;
                  }
               } else {
                  var6 = var1;
                  if(var8.equals(var3)) {
                     break;
                  }
               }
            }

            this.e.a(var1);
            var6 = this.e.a(var7.intValue(), var8);
            break;
         }

         ++var4;
      }

      return var6;
   }

   private NavigableMap a(Config var1) {
      NavigableMap var3 = (NavigableMap)this.g.get(var1);
      Object var2 = var3;
      if(var3 == null) {
         var2 = new TreeMap();
         this.g.put(var1, var2);
      }

      return (NavigableMap)var2;
   }

   private void a(Integer var1, Config var2) {
      NavigableMap var4 = this.a(var2);
      Integer var3 = (Integer)var4.get(var1);
      if(var3.intValue() == 1) {
         var4.remove(var1);
      } else {
         var4.put(var1, Integer.valueOf(var3.intValue() - 1));
      }

   }

   private static String b(int var0, Config var1) {
      return "[" + var0 + "](" + var1 + ")";
   }

   private static Config[] b(Config var0) {
      Config[] var2;
      switch(null.a[var0.ordinal()]) {
      case 1:
         var2 = a;
         break;
      case 2:
         var2 = b;
         break;
      case 3:
         var2 = c;
         break;
      case 4:
         var2 = d;
         break;
      default:
         Config[] var1 = new Config[]{var0};
         var2 = var1;
      }

      return var2;
   }

   public Bitmap a() {
      Bitmap var1 = (Bitmap)this.f.a();
      if(var1 != null) {
         this.a(Integer.valueOf(com.bumptech.glide.i.h.a(var1)), var1.getConfig());
      }

      return var1;
   }

   public Bitmap a(int var1, int var2, Config var3) {
      int var4 = com.bumptech.glide.i.h.a(var1, var2, var3);
      i.a var6 = this.a(this.e.a(var4, var3), var4, var3);
      Bitmap var5 = (Bitmap)this.f.a((h)var6);
      if(var5 != null) {
         this.a(Integer.valueOf(com.bumptech.glide.i.h.a(var5)), var5.getConfig());
         if(var5.getConfig() != null) {
            var3 = var5.getConfig();
         } else {
            var3 = Config.ARGB_8888;
         }

         var5.reconfigure(var1, var2, var3);
      }

      return var5;
   }

   public void a(Bitmap var1) {
      int var2 = com.bumptech.glide.i.h.a(var1);
      i.a var4 = this.e.a(var2, var1.getConfig());
      this.f.a(var4, var1);
      NavigableMap var6 = this.a(var1.getConfig());
      Integer var5 = (Integer)var6.get(Integer.valueOf(var4.b));
      int var3 = var4.b;
      if(var5 == null) {
         var2 = 1;
      } else {
         var2 = var5.intValue() + 1;
      }

      var6.put(Integer.valueOf(var3), Integer.valueOf(var2));
   }

   public String b(int var1, int var2, Config var3) {
      return b(com.bumptech.glide.i.h.a(var1, var2, var3), var3);
   }

   public String b(Bitmap var1) {
      return b(com.bumptech.glide.i.h.a(var1), var1.getConfig());
   }

   public int c(Bitmap var1) {
      return com.bumptech.glide.i.h.a(var1);
   }

   public String toString() {
      StringBuilder var2 = (new StringBuilder()).append("SizeConfigStrategy{groupedMap=").append(this.f).append(", sortedSizes=(");
      Iterator var3 = this.g.entrySet().iterator();

      while(var3.hasNext()) {
         Entry var1 = (Entry)var3.next();
         var2.append(var1.getKey()).append('[').append(var1.getValue()).append("], ");
      }

      if(!this.g.isEmpty()) {
         var2.replace(var2.length() - 2, var2.length(), "");
      }

      return var2.append(")}").toString();
   }

   static final class a implements h {
      private final i.b a;
      private int b;
      private Config c;

      public a(i.b var1) {
         this.a = var1;
      }

      public void a() {
         this.a.a(this);
      }

      public void a(int var1, Config var2) {
         this.b = var1;
         this.c = var2;
      }

      public boolean equals(Object var1) {
         boolean var3 = false;
         boolean var2 = var3;
         if(var1 instanceof i.a) {
            i.a var4 = (i.a)var1;
            var2 = var3;
            if(this.b == var4.b) {
               if(this.c == null) {
                  var2 = var3;
                  if(var4.c != null) {
                     return var2;
                  }
               } else {
                  var2 = var3;
                  if(!this.c.equals(var4.c)) {
                     return var2;
                  }
               }

               var2 = true;
            }
         }

         return var2;
      }

      public int hashCode() {
         int var2 = this.b;
         int var1;
         if(this.c != null) {
            var1 = this.c.hashCode();
         } else {
            var1 = 0;
         }

         return var1 + var2 * 31;
      }

      public String toString() {
         return i.b(this.b, this.c);
      }
   }

   static class b extends b {
      protected i.a a() {
         return new i.a(this);
      }

      public i.a a(int var1, Config var2) {
         i.a var3 = (i.a)this.c();
         var3.a(var1, var2);
         return var3;
      }

      // $FF: synthetic method
      protected h b() {
         return this.a();
      }
   }
}
