package com.bumptech.glide.load.engine.b;

import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.content.Context;
import android.os.Build.VERSION;
import android.text.format.Formatter;
import android.util.DisplayMetrics;
import android.util.Log;

public class i {
   private final int a;
   private final int b;
   private final Context c;

   public i(Context var1) {
      this(var1, (ActivityManager)var1.getSystemService("activity"), new i.a(var1.getResources().getDisplayMetrics()));
   }

   i(Context var1, ActivityManager var2, i.b var3) {
      this.c = var1;
      int var5 = a(var2);
      int var6 = var3.a() * var3.b() * 4;
      int var4 = var6 * 4;
      var6 *= 2;
      if(var6 + var4 <= var5) {
         this.b = var6;
         this.a = var4;
      } else {
         int var7 = Math.round((float)var5 / 6.0F);
         this.b = var7 * 2;
         this.a = var7 * 4;
      }

      if(Log.isLoggable("MemorySizeCalculator", 3)) {
         StringBuilder var9 = (new StringBuilder()).append("Calculated memory cache size: ").append(this.a(this.b)).append(" pool size: ").append(this.a(this.a)).append(" memory class limited? ");
         boolean var8;
         if(var6 + var4 > var5) {
            var8 = true;
         } else {
            var8 = false;
         }

         Log.d("MemorySizeCalculator", var9.append(var8).append(" max size: ").append(this.a(var5)).append(" memoryClass: ").append(var2.getMemoryClass()).append(" isLowMemoryDevice: ").append(b(var2)).toString());
      }

   }

   private static int a(ActivityManager var0) {
      int var3 = var0.getMemoryClass();
      boolean var4 = b(var0);
      float var2 = (float)(var3 * 1024 * 1024);
      float var1;
      if(var4) {
         var1 = 0.33F;
      } else {
         var1 = 0.4F;
      }

      return Math.round(var1 * var2);
   }

   private String a(int var1) {
      return Formatter.formatFileSize(this.c, (long)var1);
   }

   @TargetApi(19)
   private static boolean b(ActivityManager var0) {
      boolean var1;
      if(VERSION.SDK_INT >= 19) {
         var1 = var0.isLowRamDevice();
      } else if(VERSION.SDK_INT < 11) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public int a() {
      return this.b;
   }

   public int b() {
      return this.a;
   }

   private static class a implements i.b {
      private final DisplayMetrics a;

      public a(DisplayMetrics var1) {
         this.a = var1;
      }

      public int a() {
         return this.a.widthPixels;
      }

      public int b() {
         return this.a.heightPixels;
      }
   }

   interface b {
      int a();

      int b();
   }
}
