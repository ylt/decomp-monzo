package com.bumptech.glide.load.engine.b;

import android.util.Log;
import java.io.File;
import java.io.IOException;

public class e implements a {
   private static e a = null;
   private final c b = new c();
   private final j c;
   private final File d;
   private final int e;
   private com.bumptech.glide.a.a f;

   protected e(File var1, int var2) {
      this.d = var1;
      this.e = var2;
      this.c = new j();
   }

   private com.bumptech.glide.a.a a() throws IOException {
      synchronized(this){}

      com.bumptech.glide.a.a var1;
      try {
         if(this.f == null) {
            this.f = com.bumptech.glide.a.a.a(this.d, 1, 1, (long)this.e);
         }

         var1 = this.f;
      } finally {
         ;
      }

      return var1;
   }

   public static a a(File var0, int var1) {
      synchronized(e.class){}

      e var5;
      try {
         if(a == null) {
            e var2 = new e(var0, var1);
            a = var2;
         }

         var5 = a;
      } finally {
         ;
      }

      return var5;
   }

   public File a(com.bumptech.glide.load.c param1) {
      // $FF: Couldn't be decompiled
   }

   public void a(com.bumptech.glide.load.c param1, a.b param2) {
      // $FF: Couldn't be decompiled
   }

   public void b(com.bumptech.glide.load.c var1) {
      String var3 = this.c.a(var1);

      try {
         this.a().c(var3);
      } catch (IOException var2) {
         if(Log.isLoggable("DiskLruCacheWrapper", 5)) {
            Log.w("DiskLruCacheWrapper", "Unable to delete from disk cache", var2);
         }
      }

   }
}
