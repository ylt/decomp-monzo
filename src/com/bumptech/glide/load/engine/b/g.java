package com.bumptech.glide.load.engine.b;

import android.annotation.SuppressLint;
import com.bumptech.glide.load.engine.k;

public class g extends com.bumptech.glide.i.e implements h {
   private h.a a;

   public g(int var1) {
      super(var1);
   }

   protected int a(k var1) {
      return var1.c();
   }

   // $FF: synthetic method
   public k a(com.bumptech.glide.load.c var1) {
      return (k)super.c(var1);
   }

   @SuppressLint({"InlinedApi"})
   public void a(int var1) {
      if(var1 >= 60) {
         this.a();
      } else if(var1 >= 40) {
         this.b(this.b() / 2);
      }

   }

   protected void a(com.bumptech.glide.load.c var1, k var2) {
      if(this.a != null) {
         this.a.b(var2);
      }

   }

   public void a(h.a var1) {
      this.a = var1;
   }
}
