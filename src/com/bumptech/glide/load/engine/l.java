package com.bumptech.glide.load.engine;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Handler.Callback;

class l {
   private boolean a;
   private final Handler b = new Handler(Looper.getMainLooper(), new l.a());

   public void a(k var1) {
      com.bumptech.glide.i.h.a();
      if(this.a) {
         this.b.obtainMessage(1, var1).sendToTarget();
      } else {
         this.a = true;
         var1.d();
         this.a = false;
      }

   }

   private static class a implements Callback {
      private a() {
      }

      // $FF: synthetic method
      a(Object var1) {
         this();
      }

      public boolean handleMessage(Message var1) {
         boolean var2;
         if(var1.what == 1) {
            ((k)var1.obj).d();
            var2 = true;
         } else {
            var2 = false;
         }

         return var2;
      }
   }
}
