package com.bumptech.glide.load.engine;

import android.os.Looper;

class h implements k {
   private final k a;
   private final boolean b;
   private h.a c;
   private com.bumptech.glide.load.c d;
   private int e;
   private boolean f;

   h(k var1, boolean var2) {
      if(var1 == null) {
         throw new NullPointerException("Wrapped resource must not be null");
      } else {
         this.a = var1;
         this.b = var2;
      }
   }

   void a(com.bumptech.glide.load.c var1, h.a var2) {
      this.d = var1;
      this.c = var2;
   }

   boolean a() {
      return this.b;
   }

   public Object b() {
      return this.a.b();
   }

   public int c() {
      return this.a.c();
   }

   public void d() {
      if(this.e > 0) {
         throw new IllegalStateException("Cannot recycle a resource while it is still acquired");
      } else if(this.f) {
         throw new IllegalStateException("Cannot recycle a resource that has already been recycled");
      } else {
         this.f = true;
         this.a.d();
      }
   }

   void e() {
      if(this.f) {
         throw new IllegalStateException("Cannot acquire a recycled resource");
      } else if(!Looper.getMainLooper().equals(Looper.myLooper())) {
         throw new IllegalThreadStateException("Must call acquire on the main thread");
      } else {
         ++this.e;
      }
   }

   void f() {
      if(this.e <= 0) {
         throw new IllegalStateException("Cannot release a recycled or not yet acquired resource");
      } else if(!Looper.getMainLooper().equals(Looper.myLooper())) {
         throw new IllegalThreadStateException("Must call release on the main thread");
      } else {
         int var1 = this.e - 1;
         this.e = var1;
         if(var1 == 0) {
            this.c.b(this.d, this);
         }

      }
   }

   interface a {
      void b(com.bumptech.glide.load.c var1, h var2);
   }
}
