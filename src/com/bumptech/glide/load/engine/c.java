package com.bumptech.glide.load.engine;

import android.os.Looper;
import android.os.MessageQueue.IdleHandler;
import android.util.Log;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;

public class c implements com.bumptech.glide.load.engine.b.h.a, e, h.a {
   private final Map a;
   private final g b;
   private final com.bumptech.glide.load.engine.b.h c;
   private final c.a d;
   private final Map e;
   private final l f;
   private final c.b g;
   private ReferenceQueue h;

   public c(com.bumptech.glide.load.engine.b.h var1, com.bumptech.glide.load.engine.b.a.a var2, ExecutorService var3, ExecutorService var4) {
      this(var1, var2, var3, var4, (Map)null, (g)null, (Map)null, (c.a)null, (l)null);
   }

   c(com.bumptech.glide.load.engine.b.h var1, com.bumptech.glide.load.engine.b.a.a var2, ExecutorService var3, ExecutorService var4, Map var5, g var6, Map var7, c.a var8, l var9) {
      this.c = var1;
      this.g = new c.b(var2);
      Object var10 = var7;
      if(var7 == null) {
         var10 = new HashMap();
      }

      this.e = (Map)var10;
      g var11 = var6;
      if(var6 == null) {
         var11 = new g();
      }

      this.b = var11;
      var10 = var5;
      if(var5 == null) {
         var10 = new HashMap();
      }

      this.a = (Map)var10;
      c.a var12 = var8;
      if(var8 == null) {
         var12 = new c.a(var3, var4, this);
      }

      this.d = var12;
      l var13 = var9;
      if(var9 == null) {
         var13 = new l();
      }

      this.f = var13;
      var1.a((com.bumptech.glide.load.engine.b.h.a)this);
   }

   private h a(com.bumptech.glide.load.c var1) {
      k var2 = this.c.a(var1);
      h var3;
      if(var2 == null) {
         var3 = null;
      } else if(var2 instanceof h) {
         var3 = (h)var2;
      } else {
         var3 = new h(var2, true);
      }

      return var3;
   }

   private h a(com.bumptech.glide.load.c var1, boolean var2) {
      WeakReference var3 = null;
      h var4;
      if(!var2) {
         var4 = var3;
      } else {
         var3 = (WeakReference)this.e.get(var1);
         if(var3 != null) {
            h var5 = (h)var3.get();
            if(var5 != null) {
               var5.e();
               var4 = var5;
            } else {
               this.e.remove(var1);
               var4 = var5;
            }
         } else {
            var4 = null;
         }
      }

      return var4;
   }

   private ReferenceQueue a() {
      if(this.h == null) {
         this.h = new ReferenceQueue();
         Looper.myQueue().addIdleHandler(new c.d(this.e, this.h));
      }

      return this.h;
   }

   private static void a(String var0, long var1, com.bumptech.glide.load.c var3) {
      Log.v("Engine", var0 + " in " + com.bumptech.glide.i.d.a(var1) + "ms, key: " + var3);
   }

   private h b(com.bumptech.glide.load.c var1, boolean var2) {
      h var3;
      if(!var2) {
         var3 = null;
      } else {
         h var4 = this.a(var1);
         var3 = var4;
         if(var4 != null) {
            var4.e();
            this.e.put(var1, new c.e(var1, var4, this.a()));
            var3 = var4;
         }
      }

      return var3;
   }

   public c.c a(com.bumptech.glide.load.c var1, int var2, int var3, com.bumptech.glide.load.a.c var4, com.bumptech.glide.f.b var5, com.bumptech.glide.load.g var6, com.bumptech.glide.load.resource.e.c var7, com.bumptech.glide.i var8, boolean var9, b var10, com.bumptech.glide.g.e var11) {
      com.bumptech.glide.i.h.a();
      long var12 = com.bumptech.glide.i.d.a();
      String var14 = var4.b();
      f var15 = this.b.a(var14, var1, var2, var3, var5.a(), var5.b(), var6, var5.d(), var7, var5.c());
      h var18 = this.b(var15, var9);
      c.c var16;
      if(var18 != null) {
         var11.a((k)var18);
         if(Log.isLoggable("Engine", 2)) {
            a("Loaded resource from cache", var12, var15);
         }

         var16 = null;
      } else {
         var18 = this.a(var15, var9);
         if(var18 != null) {
            var11.a((k)var18);
            if(Log.isLoggable("Engine", 2)) {
               a("Loaded resource from active resources", var12, var15);
            }

            var16 = null;
         } else {
            d var19 = (d)this.a.get(var15);
            if(var19 != null) {
               var19.a(var11);
               if(Log.isLoggable("Engine", 2)) {
                  a("Added to existing load", var12, var15);
               }

               var16 = new c.c(var11, var19);
            } else {
               var19 = this.d.a(var15, var9);
               i var17 = new i(var19, new a(var15, var2, var3, var4, var5, var6, var7, this.g, var10, var8), var8);
               this.a.put(var15, var19);
               var19.a(var11);
               var19.a(var17);
               if(Log.isLoggable("Engine", 2)) {
                  a("Started new load", var12, var15);
               }

               var16 = new c.c(var11, var19);
            }
         }
      }

      return var16;
   }

   public void a(com.bumptech.glide.load.c var1, h var2) {
      com.bumptech.glide.i.h.a();
      if(var2 != null) {
         var2.a(var1, this);
         if(var2.a()) {
            this.e.put(var1, new c.e(var1, var2, this.a()));
         }
      }

      this.a.remove(var1);
   }

   public void a(d var1, com.bumptech.glide.load.c var2) {
      com.bumptech.glide.i.h.a();
      if(var1.equals((d)this.a.get(var2))) {
         this.a.remove(var2);
      }

   }

   public void a(k var1) {
      com.bumptech.glide.i.h.a();
      if(var1 instanceof h) {
         ((h)var1).f();
      } else {
         throw new IllegalArgumentException("Cannot release anything but an EngineResource");
      }
   }

   public void b(com.bumptech.glide.load.c var1, h var2) {
      com.bumptech.glide.i.h.a();
      this.e.remove(var1);
      if(var2.a()) {
         this.c.b(var1, var2);
      } else {
         this.f.a(var2);
      }

   }

   public void b(k var1) {
      com.bumptech.glide.i.h.a();
      this.f.a(var1);
   }

   static class a {
      private final ExecutorService a;
      private final ExecutorService b;
      private final e c;

      public a(ExecutorService var1, ExecutorService var2, e var3) {
         this.a = var1;
         this.b = var2;
         this.c = var3;
      }

      public d a(com.bumptech.glide.load.c var1, boolean var2) {
         return new d(var1, this.a, this.b, var2, this.c);
      }
   }

   private static class b implements a.a {
      private final com.bumptech.glide.load.engine.b.a.a a;
      private volatile com.bumptech.glide.load.engine.b.a b;

      public b(com.bumptech.glide.load.engine.b.a.a var1) {
         this.a = var1;
      }

      public com.bumptech.glide.load.engine.b.a a() {
         // $FF: Couldn't be decompiled
      }
   }

   public static class c {
      private final d a;
      private final com.bumptech.glide.g.e b;

      public c(com.bumptech.glide.g.e var1, d var2) {
         this.b = var1;
         this.a = var2;
      }

      public void a() {
         this.a.b(this.b);
      }
   }

   private static class d implements IdleHandler {
      private final Map a;
      private final ReferenceQueue b;

      public d(Map var1, ReferenceQueue var2) {
         this.a = var1;
         this.b = var2;
      }

      public boolean queueIdle() {
         c.e var1 = (c.e)this.b.poll();
         if(var1 != null) {
            this.a.remove(var1.a);
         }

         return true;
      }
   }

   private static class e extends WeakReference {
      private final com.bumptech.glide.load.c a;

      public e(com.bumptech.glide.load.c var1, h var2, ReferenceQueue var3) {
         super(var2, var3);
         this.a = var1;
      }
   }
}
