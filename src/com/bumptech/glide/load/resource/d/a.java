package com.bumptech.glide.load.resource.d;

import com.bumptech.glide.load.engine.k;

public class a {
   private final k a;
   private final k b;

   public a(k var1, k var2) {
      if(var1 != null && var2 != null) {
         throw new IllegalArgumentException("Can only contain either a bitmap resource or a gif resource, not both");
      } else if(var1 == null && var2 == null) {
         throw new IllegalArgumentException("Must contain either a bitmap resource or a gif resource");
      } else {
         this.b = var1;
         this.a = var2;
      }
   }

   public int a() {
      int var1;
      if(this.b != null) {
         var1 = this.b.c();
      } else {
         var1 = this.a.c();
      }

      return var1;
   }

   public k b() {
      return this.b;
   }

   public k c() {
      return this.a;
   }
}
