package com.bumptech.glide.load.resource.d;

public class g implements com.bumptech.glide.f.b {
   private final com.bumptech.glide.load.e a;
   private final com.bumptech.glide.load.e b;
   private final com.bumptech.glide.load.f c;
   private final com.bumptech.glide.load.b d;

   public g(com.bumptech.glide.f.b var1, com.bumptech.glide.f.b var2, com.bumptech.glide.load.engine.a.c var3) {
      c var4 = new c(var1.b(), var2.b(), var3);
      this.a = new com.bumptech.glide.load.resource.b.c(new e(var4));
      this.b = var4;
      this.c = new d(var1.d(), var2.d());
      this.d = var1.c();
   }

   public com.bumptech.glide.load.e a() {
      return this.a;
   }

   public com.bumptech.glide.load.e b() {
      return this.b;
   }

   public com.bumptech.glide.load.b c() {
      return this.d;
   }

   public com.bumptech.glide.load.f d() {
      return this.c;
   }
}
