package com.bumptech.glide.load.resource.d;

import com.bumptech.glide.load.engine.k;

public class f implements com.bumptech.glide.load.g {
   private final com.bumptech.glide.load.g a;
   private final com.bumptech.glide.load.g b;

   public f(com.bumptech.glide.load.engine.a.c var1, com.bumptech.glide.load.g var2) {
      this((com.bumptech.glide.load.g)var2, new com.bumptech.glide.load.resource.c.e(var2, var1));
   }

   f(com.bumptech.glide.load.g var1, com.bumptech.glide.load.g var2) {
      this.a = var1;
      this.b = var2;
   }

   public k a(k var1, int var2, int var3) {
      k var5 = ((a)var1.b()).b();
      k var6 = ((a)var1.b()).c();
      Object var4;
      if(var5 != null && this.a != null) {
         var6 = this.a.a(var5, var2, var3);
         var4 = var1;
         if(!var5.equals(var6)) {
            var4 = new b(new a(var6, ((a)var1.b()).c()));
         }
      } else {
         var4 = var1;
         if(var6 != null) {
            var4 = var1;
            if(this.b != null) {
               var5 = this.b.a(var6, var2, var3);
               var4 = var1;
               if(!var6.equals(var5)) {
                  var4 = new b(new a(((a)var1.b()).b(), var5));
               }
            }
         }
      }

      return (k)var4;
   }

   public String a() {
      return this.a.a();
   }
}
