package com.bumptech.glide.load.resource.d;

import com.bumptech.glide.load.engine.k;
import java.io.OutputStream;

public class d implements com.bumptech.glide.load.f {
   private final com.bumptech.glide.load.f a;
   private final com.bumptech.glide.load.f b;
   private String c;

   public d(com.bumptech.glide.load.f var1, com.bumptech.glide.load.f var2) {
      this.a = var1;
      this.b = var2;
   }

   public String a() {
      if(this.c == null) {
         this.c = this.a.a() + this.b.a();
      }

      return this.c;
   }

   public boolean a(k var1, OutputStream var2) {
      a var4 = (a)var1.b();
      var1 = var4.b();
      boolean var3;
      if(var1 != null) {
         var3 = this.a.a(var1, var2);
      } else {
         var3 = this.b.a(var4.c(), var2);
      }

      return var3;
   }
}
