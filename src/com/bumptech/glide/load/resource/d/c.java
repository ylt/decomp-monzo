package com.bumptech.glide.load.resource.d;

import com.bumptech.glide.load.engine.k;
import com.bumptech.glide.load.resource.bitmap.ImageHeaderParser;
import com.bumptech.glide.load.resource.bitmap.RecyclableBufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

public class c implements com.bumptech.glide.load.e {
   private static final c.b a = new c.b();
   private static final c.a b = new c.a();
   private final com.bumptech.glide.load.e c;
   private final com.bumptech.glide.load.e d;
   private final com.bumptech.glide.load.engine.a.c e;
   private final c.b f;
   private final c.a g;
   private String h;

   public c(com.bumptech.glide.load.e var1, com.bumptech.glide.load.e var2, com.bumptech.glide.load.engine.a.c var3) {
      this(var1, var2, var3, a, b);
   }

   c(com.bumptech.glide.load.e var1, com.bumptech.glide.load.e var2, com.bumptech.glide.load.engine.a.c var3, c.b var4, c.a var5) {
      this.c = var1;
      this.d = var2;
      this.e = var3;
      this.f = var4;
      this.g = var5;
   }

   private a a(com.bumptech.glide.load.b.g var1, int var2, int var3, byte[] var4) throws IOException {
      a var5;
      if(var1.a() != null) {
         var5 = this.b(var1, var2, var3, var4);
      } else {
         var5 = this.b(var1, var2, var3);
      }

      return var5;
   }

   private a a(InputStream var1, int var2, int var3) throws IOException {
      k var4 = this.d.a(var1, var2, var3);
      a var6;
      if(var4 != null) {
         com.bumptech.glide.load.resource.c.b var5 = (com.bumptech.glide.load.resource.c.b)var4.b();
         if(var5.e() > 1) {
            var6 = new a((k)null, var4);
         } else {
            var6 = new a(new com.bumptech.glide.load.resource.bitmap.c(var5.b(), this.e), (k)null);
         }
      } else {
         var6 = null;
      }

      return var6;
   }

   private a b(com.bumptech.glide.load.b.g var1, int var2, int var3) throws IOException {
      k var4 = this.c.a(var1, var2, var3);
      a var5;
      if(var4 != null) {
         var5 = new a(var4, (k)null);
      } else {
         var5 = null;
      }

      return var5;
   }

   private a b(com.bumptech.glide.load.b.g var1, int var2, int var3, byte[] var4) throws IOException {
      InputStream var6 = this.g.a(var1.a(), var4);
      var6.mark(2048);
      ImageHeaderParser.ImageType var5 = this.f.a(var6);
      var6.reset();
      a var8 = null;
      if(var5 == ImageHeaderParser.ImageType.GIF) {
         var8 = this.a(var6, var2, var3);
      }

      a var7 = var8;
      if(var8 == null) {
         var7 = this.b(new com.bumptech.glide.load.b.g(var6, var1.b()), var2, var3);
      }

      return var7;
   }

   public k a(com.bumptech.glide.load.b.g var1, int var2, int var3) throws IOException {
      com.bumptech.glide.i.a var4 = com.bumptech.glide.i.a.a();
      byte[] var5 = var4.b();

      a var8;
      try {
         var8 = this.a(var1, var2, var3, var5);
      } finally {
         var4.a(var5);
      }

      b var9;
      if(var8 != null) {
         var9 = new b(var8);
      } else {
         var9 = null;
      }

      return var9;
   }

   public String a() {
      if(this.h == null) {
         this.h = this.d.a() + this.c.a();
      }

      return this.h;
   }

   static class a {
      public InputStream a(InputStream var1, byte[] var2) {
         return new RecyclableBufferedInputStream(var1, var2);
      }
   }

   static class b {
      public ImageHeaderParser.ImageType a(InputStream var1) throws IOException {
         return (new ImageHeaderParser(var1)).b();
      }
   }
}
