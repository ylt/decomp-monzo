package com.bumptech.glide.load.resource.d;

import com.bumptech.glide.load.engine.k;

public class b implements k {
   private final a a;

   public b(a var1) {
      if(var1 == null) {
         throw new NullPointerException("Data must not be null");
      } else {
         this.a = var1;
      }
   }

   public a a() {
      return this.a;
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }

   public int c() {
      return this.a.a();
   }

   public void d() {
      k var1 = this.a.b();
      if(var1 != null) {
         var1.d();
      }

      var1 = this.a.c();
      if(var1 != null) {
         var1.d();
      }

   }
}
