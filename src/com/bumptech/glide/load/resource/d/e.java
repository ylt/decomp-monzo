package com.bumptech.glide.load.resource.d;

import android.os.ParcelFileDescriptor;
import com.bumptech.glide.load.engine.k;
import java.io.IOException;
import java.io.InputStream;

public class e implements com.bumptech.glide.load.e {
   private final com.bumptech.glide.load.e a;

   public e(com.bumptech.glide.load.e var1) {
      this.a = var1;
   }

   public k a(InputStream var1, int var2, int var3) throws IOException {
      return this.a.a(new com.bumptech.glide.load.b.g(var1, (ParcelFileDescriptor)null), var2, var3);
   }

   public String a() {
      return this.a.a();
   }
}
