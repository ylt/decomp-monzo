package com.bumptech.glide.load.resource;

import com.bumptech.glide.load.engine.k;

public class c implements k {
   protected final Object a;

   public c(Object var1) {
      if(var1 == null) {
         throw new NullPointerException("Data must not be null");
      } else {
         this.a = var1;
      }
   }

   public final Object b() {
      return this.a;
   }

   public final int c() {
      return 1;
   }

   public void d() {
   }
}
