package com.bumptech.glide.load.resource.a;

import android.graphics.drawable.Drawable;
import com.bumptech.glide.load.engine.k;

public abstract class a implements k {
   protected final Drawable a;

   public a(Drawable var1) {
      if(var1 == null) {
         throw new NullPointerException("Drawable must not be null!");
      } else {
         this.a = var1;
      }
   }

   public final Drawable a() {
      return this.a.getConstantState().newDrawable();
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }
}
