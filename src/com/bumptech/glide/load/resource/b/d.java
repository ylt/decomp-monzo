package com.bumptech.glide.load.resource.b;

import com.bumptech.glide.load.e;
import com.bumptech.glide.load.f;
import com.bumptech.glide.load.b.o;
import com.bumptech.glide.load.engine.k;
import java.io.InputStream;

public class d implements com.bumptech.glide.f.b {
   private static final d.a a = new d.a();
   private final e b = new a();
   private final com.bumptech.glide.load.b c = new o();

   public e a() {
      return this.b;
   }

   public e b() {
      return a;
   }

   public com.bumptech.glide.load.b c() {
      return this.c;
   }

   public f d() {
      return com.bumptech.glide.load.resource.b.b();
   }

   private static class a implements e {
      private a() {
      }

      // $FF: synthetic method
      a(Object var1) {
         this();
      }

      public k a(InputStream var1, int var2, int var3) {
         throw new Error("You cannot decode a File from an InputStream by default, try either #diskCacheStratey(DiskCacheStrategy.SOURCE) to avoid this call or #decoder(ResourceDecoder) to replace this Decoder");
      }

      public String a() {
         return "";
      }
   }
}
