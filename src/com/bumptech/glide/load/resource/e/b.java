package com.bumptech.glide.load.resource.e;

import android.content.res.Resources;
import android.graphics.Bitmap;
import com.bumptech.glide.load.engine.k;
import com.bumptech.glide.load.resource.bitmap.j;

public class b implements c {
   private final Resources a;
   private final com.bumptech.glide.load.engine.a.c b;

   public b(Resources var1, com.bumptech.glide.load.engine.a.c var2) {
      this.a = var1;
      this.b = var2;
   }

   public k a(k var1) {
      return new com.bumptech.glide.load.resource.bitmap.k(new j(this.a, (Bitmap)var1.b()), this.b);
   }

   public String a() {
      return "GlideBitmapDrawableTranscoder.com.bumptech.glide.load.resource.transcode";
   }
}
