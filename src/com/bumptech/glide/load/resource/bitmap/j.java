package com.bumptech.glide.load.resource.bitmap;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Drawable.ConstantState;
import android.view.Gravity;

public class j extends com.bumptech.glide.load.resource.a.b {
   private final Rect a;
   private int b;
   private int c;
   private boolean d;
   private boolean e;
   private j.a f;

   public j(Resources var1, Bitmap var2) {
      this(var1, new j.a(var2));
   }

   j(Resources var1, j.a var2) {
      this.a = new Rect();
      if(var2 == null) {
         throw new NullPointerException("BitmapState must not be null");
      } else {
         this.f = var2;
         int var3;
         if(var1 != null) {
            int var4 = var1.getDisplayMetrics().densityDpi;
            var3 = var4;
            if(var4 == 0) {
               var3 = 160;
            }

            var2.b = var3;
         } else {
            var3 = var2.b;
         }

         this.b = var2.a.getScaledWidth(var3);
         this.c = var2.a.getScaledHeight(var3);
      }
   }

   public void a(int var1) {
   }

   public boolean a() {
      return false;
   }

   public Bitmap b() {
      return this.f.a;
   }

   public void draw(Canvas var1) {
      if(this.d) {
         Gravity.apply(119, this.b, this.c, this.getBounds(), this.a);
         this.d = false;
      }

      var1.drawBitmap(this.f.a, (Rect)null, this.a, this.f.c);
   }

   public ConstantState getConstantState() {
      return this.f;
   }

   public int getIntrinsicHeight() {
      return this.c;
   }

   public int getIntrinsicWidth() {
      return this.b;
   }

   public int getOpacity() {
      Bitmap var2 = this.f.a;
      byte var1;
      if(var2 != null && !var2.hasAlpha() && this.f.c.getAlpha() >= 255) {
         var1 = -1;
      } else {
         var1 = -3;
      }

      return var1;
   }

   public boolean isRunning() {
      return false;
   }

   public Drawable mutate() {
      if(!this.e && super.mutate() == this) {
         this.f = new j.a(this.f);
         this.e = true;
      }

      return this;
   }

   protected void onBoundsChange(Rect var1) {
      super.onBoundsChange(var1);
      this.d = true;
   }

   public void setAlpha(int var1) {
      if(this.f.c.getAlpha() != var1) {
         this.f.a(var1);
         this.invalidateSelf();
      }

   }

   public void setColorFilter(ColorFilter var1) {
      this.f.a(var1);
      this.invalidateSelf();
   }

   public void start() {
   }

   public void stop() {
   }

   static class a extends ConstantState {
      private static final Paint d = new Paint(6);
      final Bitmap a;
      int b;
      Paint c;

      public a(Bitmap var1) {
         this.c = d;
         this.a = var1;
      }

      a(j.a var1) {
         this(var1.a);
         this.b = var1.b;
      }

      void a() {
         if(d == this.c) {
            this.c = new Paint(6);
         }

      }

      void a(int var1) {
         this.a();
         this.c.setAlpha(var1);
      }

      void a(ColorFilter var1) {
         this.a();
         this.c.setColorFilter(var1);
      }

      public int getChangingConfigurations() {
         return 0;
      }

      public Drawable newDrawable() {
         return new j((Resources)null, this);
      }

      public Drawable newDrawable(Resources var1) {
         return new j(var1, this);
      }
   }
}
