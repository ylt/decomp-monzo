package com.bumptech.glide.load.resource.bitmap;

public class n implements com.bumptech.glide.f.b {
   private final o a;
   private final b b;
   private final com.bumptech.glide.load.b.o c = new com.bumptech.glide.load.b.o();
   private final com.bumptech.glide.load.resource.b.c d;

   public n(com.bumptech.glide.load.engine.a.c var1, com.bumptech.glide.load.a var2) {
      this.a = new o(var1, var2);
      this.b = new b();
      this.d = new com.bumptech.glide.load.resource.b.c(this.a);
   }

   public com.bumptech.glide.load.e a() {
      return this.d;
   }

   public com.bumptech.glide.load.e b() {
      return this.a;
   }

   public com.bumptech.glide.load.b c() {
      return this.c;
   }

   public com.bumptech.glide.load.f d() {
      return this.b;
   }
}
