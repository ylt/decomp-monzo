package com.bumptech.glide.load.resource.bitmap;

public class m implements com.bumptech.glide.f.b {
   private final l a;
   private final com.bumptech.glide.load.e b;
   private final com.bumptech.glide.load.f c;
   private final com.bumptech.glide.load.b.h d;

   public m(com.bumptech.glide.f.b var1, com.bumptech.glide.f.b var2) {
      this.c = var1.d();
      this.d = new com.bumptech.glide.load.b.h(var1.c(), var2.c());
      this.b = var1.a();
      this.a = new l(var1.b(), var2.b());
   }

   public com.bumptech.glide.load.e a() {
      return this.b;
   }

   public com.bumptech.glide.load.e b() {
      return this.a;
   }

   public com.bumptech.glide.load.b c() {
      return this.d;
   }

   public com.bumptech.glide.load.f d() {
      return this.c;
   }
}
