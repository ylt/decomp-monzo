package com.bumptech.glide.load.resource.bitmap;

import android.util.Log;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public class RecyclableBufferedInputStream extends FilterInputStream {
   private volatile byte[] a;
   private int b;
   private int c;
   private int d = -1;
   private int e;

   public RecyclableBufferedInputStream(InputStream var1, byte[] var2) {
      super(var1);
      if(var2 != null && var2.length != 0) {
         this.a = var2;
      } else {
         throw new IllegalArgumentException("buffer is null or empty");
      }
   }

   private int a(InputStream var1, byte[] var2) throws IOException {
      int var3;
      int var4;
      if(this.d != -1 && this.e - this.d < this.c) {
         byte[] var5;
         if(this.d == 0 && this.c > var2.length && this.b == var2.length) {
            var4 = var2.length * 2;
            var3 = var4;
            if(var4 > this.c) {
               var3 = this.c;
            }

            if(Log.isLoggable("BufferedIs", 3)) {
               Log.d("BufferedIs", "allocate buffer of length: " + var3);
            }

            var5 = new byte[var3];
            System.arraycopy(var2, 0, var5, 0, var2.length);
            this.a = var5;
         } else {
            var5 = var2;
            if(this.d > 0) {
               System.arraycopy(var2, this.d, var2, 0, var2.length - this.d);
               var5 = var2;
            }
         }

         this.e -= this.d;
         this.d = 0;
         this.b = 0;
         var4 = var1.read(var5, this.e, var5.length - this.e);
         if(var4 <= 0) {
            var3 = this.e;
         } else {
            var3 = this.e + var4;
         }

         this.b = var3;
         var3 = var4;
      } else {
         var4 = var1.read(var2);
         var3 = var4;
         if(var4 > 0) {
            this.d = -1;
            this.e = 0;
            this.b = var4;
            var3 = var4;
         }
      }

      return var3;
   }

   private static IOException b() throws IOException {
      throw new IOException("BufferedInputStream is closed");
   }

   public void a() {
      synchronized(this){}

      try {
         this.c = this.a.length;
      } finally {
         ;
      }

   }

   public int available() throws IOException {
      // $FF: Couldn't be decompiled
   }

   public void close() throws IOException {
      this.a = null;
      InputStream var1 = this.in;
      this.in = null;
      if(var1 != null) {
         var1.close();
      }

   }

   public void mark(int var1) {
      synchronized(this){}

      try {
         this.c = Math.max(this.c, var1);
         this.d = this.e;
      } finally {
         ;
      }

   }

   public boolean markSupported() {
      return true;
   }

   public int read() throws IOException {
      // $FF: Couldn't be decompiled
   }

   public int read(byte[] param1, int param2, int param3) throws IOException {
      // $FF: Couldn't be decompiled
   }

   public void reset() throws IOException {
      synchronized(this){}

      try {
         if(this.a == null) {
            IOException var4 = new IOException("Stream is closed");
            throw var4;
         }

         if(-1 == this.d) {
            RecyclableBufferedInputStream.InvalidMarkException var1 = new RecyclableBufferedInputStream.InvalidMarkException("Mark has been invalidated");
            throw var1;
         }

         this.e = this.d;
      } finally {
         ;
      }

   }

   public long skip(long param1) throws IOException {
      // $FF: Couldn't be decompiled
   }

   public static class InvalidMarkException extends RuntimeException {
      public InvalidMarkException(String var1) {
         super(var1);
      }
   }
}
