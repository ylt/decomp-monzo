package com.bumptech.glide.load.resource.bitmap;

import java.io.InputStream;

public class o implements com.bumptech.glide.load.e {
   private final f a;
   private com.bumptech.glide.load.engine.a.c b;
   private com.bumptech.glide.load.a c;
   private String d;

   public o(com.bumptech.glide.load.engine.a.c var1, com.bumptech.glide.load.a var2) {
      this(f.a, var1, var2);
   }

   public o(f var1, com.bumptech.glide.load.engine.a.c var2, com.bumptech.glide.load.a var3) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
   }

   public com.bumptech.glide.load.engine.k a(InputStream var1, int var2, int var3) {
      return c.a(this.a.a(var1, this.b, var2, var3, this.c), this.b);
   }

   public String a() {
      if(this.d == null) {
         this.d = "StreamBitmapDecoder.com.bumptech.glide.load.resource.bitmap" + this.a.a() + this.c.name();
      }

      return this.d;
   }
}
