package com.bumptech.glide.load.resource.bitmap;

import android.graphics.Bitmap;

public abstract class d implements com.bumptech.glide.load.g {
   private com.bumptech.glide.load.engine.a.c a;

   public d(com.bumptech.glide.load.engine.a.c var1) {
      this.a = var1;
   }

   protected abstract Bitmap a(com.bumptech.glide.load.engine.a.c var1, Bitmap var2, int var3, int var4);

   public final com.bumptech.glide.load.engine.k a(com.bumptech.glide.load.engine.k var1, int var2, int var3) {
      if(!com.bumptech.glide.i.h.a(var2, var3)) {
         throw new IllegalArgumentException("Cannot apply transformation on width: " + var2 + " or height: " + var3 + " less than or equal to zero and not Target.SIZE_ORIGINAL");
      } else {
         Bitmap var5 = (Bitmap)((com.bumptech.glide.load.engine.k)var1).b();
         int var4 = var2;
         if(var2 == Integer.MIN_VALUE) {
            var4 = var5.getWidth();
         }

         var2 = var3;
         if(var3 == Integer.MIN_VALUE) {
            var2 = var5.getHeight();
         }

         Bitmap var6 = this.a(this.a, var5, var4, var2);
         if(!var5.equals(var6)) {
            var1 = c.a(var6, this.a);
         }

         return (com.bumptech.glide.load.engine.k)var1;
      }
   }
}
