package com.bumptech.glide.load.resource.bitmap;

import android.util.Log;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class ImageHeaderParser {
   private static final byte[] a;
   private static final int[] b = new int[]{0, 1, 1, 2, 4, 8, 1, 1, 2, 4, 8, 4, 8};
   private final ImageHeaderParser.b c;

   static {
      byte[] var0 = new byte[0];

      label13: {
         byte[] var1;
         try {
            var1 = "Exif\u0000\u0000".getBytes("UTF-8");
         } catch (UnsupportedEncodingException var2) {
            break label13;
         }

         var0 = var1;
      }

      a = var0;
   }

   public ImageHeaderParser(InputStream var1) {
      this.c = new ImageHeaderParser.b(var1);
   }

   private static int a(int var0, int var1) {
      return var0 + 2 + var1 * 12;
   }

   private static int a(ImageHeaderParser.a var0) {
      int var2 = "Exif\u0000\u0000".length();
      short var1 = var0.b(var2);
      ByteOrder var8;
      if(var1 == 19789) {
         var8 = ByteOrder.BIG_ENDIAN;
      } else if(var1 == 18761) {
         var8 = ByteOrder.LITTLE_ENDIAN;
      } else {
         if(Log.isLoggable("ImageHeaderParser", 3)) {
            Log.d("ImageHeaderParser", "Unknown endianness = " + var1);
         }

         var8 = ByteOrder.BIG_ENDIAN;
      }

      var0.a(var8);
      var2 += var0.a(var2 + 4);
      short var3 = var0.b(var2);
      int var9 = 0;

      while(true) {
         if(var9 >= var3) {
            var1 = -1;
            break;
         }

         int var7 = a(var2, var9);
         short var4 = var0.b(var7);
         if(var4 == 274) {
            short var6 = var0.b(var7 + 2);
            if(var6 >= 1 && var6 <= 12) {
               int var5 = var0.a(var7 + 4);
               if(var5 < 0) {
                  if(Log.isLoggable("ImageHeaderParser", 3)) {
                     Log.d("ImageHeaderParser", "Negative tiff component count");
                  }
               } else {
                  if(Log.isLoggable("ImageHeaderParser", 3)) {
                     Log.d("ImageHeaderParser", "Got tagIndex=" + var9 + " tagType=" + var4 + " formatCode=" + var6 + " componentCount=" + var5);
                  }

                  var5 += b[var6];
                  if(var5 > 4) {
                     if(Log.isLoggable("ImageHeaderParser", 3)) {
                        Log.d("ImageHeaderParser", "Got byte count > 4, not orientation, continuing, formatCode=" + var6);
                     }
                  } else {
                     int var10 = var7 + 8;
                     if(var10 >= 0 && var10 <= var0.a()) {
                        if(var5 >= 0 && var10 + var5 <= var0.a()) {
                           var1 = var0.b(var10);
                           break;
                        }

                        if(Log.isLoggable("ImageHeaderParser", 3)) {
                           Log.d("ImageHeaderParser", "Illegal number of bytes for TI tag data tagType=" + var4);
                        }
                     } else if(Log.isLoggable("ImageHeaderParser", 3)) {
                        Log.d("ImageHeaderParser", "Illegal tagValueOffset=" + var10 + " tagType=" + var4);
                     }
                  }
               }
            } else if(Log.isLoggable("ImageHeaderParser", 3)) {
               Log.d("ImageHeaderParser", "Got invalid format code=" + var6);
            }
         }

         ++var9;
      }

      return var1;
   }

   private static boolean a(int var0) {
      boolean var1;
      if((var0 & '\uffd8') != '\uffd8' && var0 != 19789 && var0 != 18761) {
         var1 = false;
      } else {
         var1 = true;
      }

      return var1;
   }

   private byte[] d() throws IOException {
      Object var7 = null;

      byte[] var6;
      while(true) {
         short var1 = this.c.b();
         if(var1 != 255) {
            var6 = (byte[])var7;
            if(Log.isLoggable("ImageHeaderParser", 3)) {
               Log.d("ImageHeaderParser", "Unknown segmentId=" + var1);
               var6 = (byte[])var7;
            }
            break;
         }

         short var3 = this.c.b();
         var6 = (byte[])var7;
         if(var3 == 218) {
            break;
         }

         if(var3 == 217) {
            var6 = (byte[])var7;
            if(Log.isLoggable("ImageHeaderParser", 3)) {
               Log.d("ImageHeaderParser", "Found MARKER_EOI in exif segment");
               var6 = (byte[])var7;
            }
            break;
         }

         int var2 = this.c.a() - 2;
         if(var3 != 225) {
            long var4 = this.c.a((long)var2);
            if(var4 == (long)var2) {
               continue;
            }

            var6 = (byte[])var7;
            if(Log.isLoggable("ImageHeaderParser", 3)) {
               Log.d("ImageHeaderParser", "Unable to skip enough data, type: " + var3 + ", wanted to skip: " + var2 + ", but actually skipped: " + var4);
               var6 = (byte[])var7;
            }
            break;
         }

         var6 = new byte[var2];
         int var8 = this.c.a(var6);
         if(var8 != var2) {
            var6 = (byte[])var7;
            if(Log.isLoggable("ImageHeaderParser", 3)) {
               Log.d("ImageHeaderParser", "Unable to read segment data, type: " + var3 + ", length: " + var2 + ", actually read: " + var8);
               var6 = (byte[])var7;
            }
         }
         break;
      }

      return var6;
   }

   public boolean a() throws IOException {
      return this.b().hasAlpha();
   }

   public ImageHeaderParser.ImageType b() throws IOException {
      int var1 = this.c.a();
      ImageHeaderParser.ImageType var2;
      if(var1 == '\uffd8') {
         var2 = ImageHeaderParser.ImageType.JPEG;
      } else {
         var1 = var1 << 16 & -65536 | this.c.a() & '\uffff';
         if(var1 == -1991225785) {
            this.c.a(21L);
            if(this.c.c() >= 3) {
               var2 = ImageHeaderParser.ImageType.PNG_A;
            } else {
               var2 = ImageHeaderParser.ImageType.PNG;
            }
         } else if(var1 >> 8 == 4671814) {
            var2 = ImageHeaderParser.ImageType.GIF;
         } else {
            var2 = ImageHeaderParser.ImageType.UNKNOWN;
         }
      }

      return var2;
   }

   public int c() throws IOException {
      boolean var3 = false;
      int var1;
      if(!a(this.c.a())) {
         var1 = -1;
      } else {
         byte[] var4 = this.d();
         boolean var5;
         if(var4 != null && var4.length > a.length) {
            var5 = true;
         } else {
            var5 = false;
         }

         if(var5) {
            for(int var2 = 0; var2 < a.length; ++var2) {
               if(var4[var2] != a[var2]) {
                  var5 = var3;
                  break;
               }
            }
         }

         if(var5) {
            var1 = a(new ImageHeaderParser.a(var4));
         } else {
            var1 = -1;
         }
      }

      return var1;
   }

   public static enum ImageType {
      GIF(true),
      JPEG(false),
      PNG(false),
      PNG_A(true),
      UNKNOWN(false);

      private final boolean a;

      private ImageType(boolean var3) {
         this.a = var3;
      }

      public boolean hasAlpha() {
         return this.a;
      }
   }

   private static class a {
      private final ByteBuffer a;

      public a(byte[] var1) {
         this.a = ByteBuffer.wrap(var1);
         this.a.order(ByteOrder.BIG_ENDIAN);
      }

      public int a() {
         return this.a.array().length;
      }

      public int a(int var1) {
         return this.a.getInt(var1);
      }

      public void a(ByteOrder var1) {
         this.a.order(var1);
      }

      public short b(int var1) {
         return this.a.getShort(var1);
      }
   }

   private static class b {
      private final InputStream a;

      public b(InputStream var1) {
         this.a = var1;
      }

      public int a() throws IOException {
         return this.a.read() << 8 & '\uff00' | this.a.read() & 255;
      }

      public int a(byte[] var1) throws IOException {
         int var2;
         int var3;
         for(var2 = var1.length; var2 > 0; var2 -= var3) {
            var3 = this.a.read(var1, var1.length - var2, var2);
            if(var3 == -1) {
               break;
            }
         }

         return var1.length - var2;
      }

      public long a(long var1) throws IOException {
         if(var1 < 0L) {
            var1 = 0L;
         } else {
            long var3 = var1;

            while(var3 > 0L) {
               long var5 = this.a.skip(var3);
               if(var5 > 0L) {
                  var3 -= var5;
               } else {
                  if(this.a.read() == -1) {
                     break;
                  }

                  --var3;
               }
            }

            var1 -= var3;
         }

         return var1;
      }

      public short b() throws IOException {
         return (short)(this.a.read() & 255);
      }

      public int c() throws IOException {
         return this.a.read();
      }
   }
}
