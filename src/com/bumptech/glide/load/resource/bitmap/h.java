package com.bumptech.glide.load.resource.bitmap;

import android.os.ParcelFileDescriptor;
import java.io.IOException;

public class h implements com.bumptech.glide.load.e {
   private final q a;
   private final com.bumptech.glide.load.engine.a.c b;
   private com.bumptech.glide.load.a c;

   public h(com.bumptech.glide.load.engine.a.c var1, com.bumptech.glide.load.a var2) {
      this(new q(), var1, var2);
   }

   public h(q var1, com.bumptech.glide.load.engine.a.c var2, com.bumptech.glide.load.a var3) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
   }

   public com.bumptech.glide.load.engine.k a(ParcelFileDescriptor var1, int var2, int var3) throws IOException {
      return c.a(this.a.a(var1, this.b, var2, var3, this.c), this.b);
   }

   public String a() {
      return "FileDescriptorBitmapDecoder.com.bumptech.glide.load.data.bitmap";
   }
}
