package com.bumptech.glide.load.resource.bitmap;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.util.Log;
import java.io.OutputStream;

public class b implements com.bumptech.glide.load.f {
   private CompressFormat a;
   private int b;

   public b() {
      this((CompressFormat)null, 90);
   }

   public b(CompressFormat var1, int var2) {
      this.a = var1;
      this.b = var2;
   }

   private CompressFormat a(Bitmap var1) {
      CompressFormat var2;
      if(this.a != null) {
         var2 = this.a;
      } else if(var1.hasAlpha()) {
         var2 = CompressFormat.PNG;
      } else {
         var2 = CompressFormat.JPEG;
      }

      return var2;
   }

   public String a() {
      return "BitmapEncoder.com.bumptech.glide.load.resource.bitmap";
   }

   public boolean a(com.bumptech.glide.load.engine.k var1, OutputStream var2) {
      Bitmap var5 = (Bitmap)var1.b();
      long var3 = com.bumptech.glide.i.d.a();
      CompressFormat var6 = this.a(var5);
      var5.compress(var6, this.b, var2);
      if(Log.isLoggable("BitmapEncoder", 2)) {
         Log.v("BitmapEncoder", "Compressed with type: " + var6 + " of size " + com.bumptech.glide.i.h.a(var5) + " in " + com.bumptech.glide.i.d.a(var3));
      }

      return true;
   }
}
