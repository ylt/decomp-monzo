package com.bumptech.glide.load.resource.bitmap;

import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.os.ParcelFileDescriptor;
import java.io.IOException;

public class q implements a {
   private static final q.a a = new q.a();
   private q.a b;
   private int c;

   public q() {
      this(a, -1);
   }

   q(q.a var1, int var2) {
      this.b = var1;
      this.c = var2;
   }

   public Bitmap a(ParcelFileDescriptor var1, com.bumptech.glide.load.engine.a.c var2, int var3, int var4, com.bumptech.glide.load.a var5) throws IOException {
      MediaMetadataRetriever var6 = this.b.a();
      var6.setDataSource(var1.getFileDescriptor());
      Bitmap var7;
      if(this.c >= 0) {
         var7 = var6.getFrameAtTime((long)this.c);
      } else {
         var7 = var6.getFrameAtTime();
      }

      var6.release();
      var1.close();
      return var7;
   }

   public String a() {
      return "VideoBitmapDecoder.com.bumptech.glide.load.resource.bitmap";
   }

   static class a {
      public MediaMetadataRetriever a() {
         return new MediaMetadataRetriever();
      }
   }
}
