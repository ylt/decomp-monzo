package com.bumptech.glide.load.resource.bitmap;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;

public class e extends d {
   public e(com.bumptech.glide.load.engine.a.c var1) {
      super(var1);
   }

   protected Bitmap a(com.bumptech.glide.load.engine.a.c var1, Bitmap var2, int var3, int var4) {
      Config var5;
      if(var2.getConfig() != null) {
         var5 = var2.getConfig();
      } else {
         var5 = Config.ARGB_8888;
      }

      Bitmap var6 = var1.a(var3, var4, var5);
      var2 = p.a(var6, var2, var3, var4);
      if(var6 != null && var6 != var2 && !var1.a(var6)) {
         var6.recycle();
      }

      return var2;
   }

   public String a() {
      return "CenterCrop.com.bumptech.glide.load.resource.bitmap";
   }
}
