package com.bumptech.glide.load.resource.bitmap;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory.Options;
import android.os.Build.VERSION;
import android.util.Log;
import java.io.IOException;
import java.io.InputStream;
import java.util.EnumSet;
import java.util.Queue;
import java.util.Set;

public abstract class f implements a {
   public static final f a;
   public static final f b;
   public static final f c;
   private static final Set d;
   private static final Queue e;

   static {
      d = EnumSet.of(ImageHeaderParser.ImageType.JPEG, ImageHeaderParser.ImageType.PNG_A, ImageHeaderParser.ImageType.PNG);
      e = com.bumptech.glide.i.h.a(0);
      a = new f() {
         protected int a(int var1, int var2, int var3, int var4) {
            return Math.min(var2 / var4, var1 / var3);
         }

         public String a() {
            return "AT_LEAST.com.bumptech.glide.load.data.bitmap";
         }
      };
      b = new f() {
         protected int a(int var1, int var2, int var3, int var4) {
            byte var5 = 1;
            var1 = (int)Math.ceil((double)Math.max((float)var2 / (float)var4, (float)var1 / (float)var3));
            var2 = Math.max(1, Integer.highestOneBit(var1));
            byte var6;
            if(var2 < var1) {
               var6 = var5;
            } else {
               var6 = 0;
            }

            return var2 << var6;
         }

         public String a() {
            return "AT_MOST.com.bumptech.glide.load.data.bitmap";
         }
      };
      c = new f() {
         protected int a(int var1, int var2, int var3, int var4) {
            return 0;
         }

         public String a() {
            return "NONE.com.bumptech.glide.load.data.bitmap";
         }
      };
   }

   private int a(int var1, int var2, int var3, int var4, int var5) {
      int var6 = var5;
      if(var5 == Integer.MIN_VALUE) {
         var6 = var3;
      }

      var5 = var4;
      if(var4 == Integer.MIN_VALUE) {
         var5 = var2;
      }

      if(var1 != 90 && var1 != 270) {
         var1 = this.a(var2, var3, var5, var6);
      } else {
         var1 = this.a(var3, var2, var5, var6);
      }

      if(var1 == 0) {
         var1 = 0;
      } else {
         var1 = Integer.highestOneBit(var1);
      }

      return Math.max(1, var1);
   }

   private static Config a(InputStream var0, com.bumptech.glide.load.a var1) {
      Config var17;
      if(var1 != com.bumptech.glide.load.a.a && var1 != com.bumptech.glide.load.a.b && VERSION.SDK_INT != 16) {
         var0.mark(1024);
         boolean var11 = false;

         boolean var2;
         label145: {
            boolean var3;
            label146: {
               try {
                  var11 = true;
                  ImageHeaderParser var18 = new ImageHeaderParser(var0);
                  var3 = var18.a();
                  var11 = false;
                  break label146;
               } catch (IOException var15) {
                  if(Log.isLoggable("Downsampler", 5)) {
                     StringBuilder var4 = new StringBuilder();
                     Log.w("Downsampler", var4.append("Cannot determine whether the image has alpha or not from header for format ").append(var1).toString(), var15);
                     var11 = false;
                  } else {
                     var11 = false;
                  }
               } finally {
                  if(var11) {
                     try {
                        var0.reset();
                     } catch (IOException var12) {
                        if(Log.isLoggable("Downsampler", 5)) {
                           Log.w("Downsampler", "Cannot reset the input stream", var12);
                        }
                     }

                  }
               }

               try {
                  var0.reset();
               } catch (IOException var14) {
                  if(Log.isLoggable("Downsampler", 5)) {
                     Log.w("Downsampler", "Cannot reset the input stream", var14);
                  }

                  var2 = false;
                  break label145;
               }

               var2 = false;
               break label145;
            }

            try {
               var0.reset();
            } catch (IOException var13) {
               var2 = var3;
               if(Log.isLoggable("Downsampler", 5)) {
                  Log.w("Downsampler", "Cannot reset the input stream", var13);
                  var2 = var3;
               }
               break label145;
            }

            var2 = var3;
         }

         if(var2) {
            var17 = Config.ARGB_8888;
         } else {
            var17 = Config.RGB_565;
         }
      } else {
         var17 = Config.ARGB_8888;
      }

      return var17;
   }

   private Bitmap a(com.bumptech.glide.i.f var1, RecyclableBufferedInputStream var2, Options var3, com.bumptech.glide.load.engine.a.c var4, int var5, int var6, int var7, com.bumptech.glide.load.a var8) {
      Config var9 = a((InputStream)var1, (com.bumptech.glide.load.a)var8);
      var3.inSampleSize = var7;
      var3.inPreferredConfig = var9;
      if((var3.inSampleSize == 1 || 19 <= VERSION.SDK_INT) && a((InputStream)var1)) {
         a(var3, var4.b((int)Math.ceil((double)var5 / (double)var7), (int)Math.ceil((double)var6 / (double)var7), var9));
      }

      return b(var1, var2, var3);
   }

   private static void a(Options param0) {
      // $FF: Couldn't be decompiled
   }

   @TargetApi(11)
   private static void a(Options var0, Bitmap var1) {
      if(11 <= VERSION.SDK_INT) {
         var0.inBitmap = var1;
      }

   }

   private static boolean a(InputStream var0) {
      boolean var1;
      if(19 <= VERSION.SDK_INT) {
         var1 = true;
      } else {
         var0.mark(1024);
         boolean var9 = false;

         boolean var2;
         label129: {
            try {
               var9 = true;
               ImageHeaderParser var3 = new ImageHeaderParser(var0);
               ImageHeaderParser.ImageType var15 = var3.b();
               var2 = d.contains(var15);
               var9 = false;
               break label129;
            } catch (IOException var13) {
               if(Log.isLoggable("Downsampler", 5)) {
                  Log.w("Downsampler", "Cannot determine the image type from header", var13);
                  var9 = false;
               } else {
                  var9 = false;
               }
            } finally {
               if(var9) {
                  try {
                     var0.reset();
                  } catch (IOException var10) {
                     if(Log.isLoggable("Downsampler", 5)) {
                        Log.w("Downsampler", "Cannot reset the input stream", var10);
                     }
                  }

               }
            }

            try {
               var0.reset();
            } catch (IOException var11) {
               if(Log.isLoggable("Downsampler", 5)) {
                  Log.w("Downsampler", "Cannot reset the input stream", var11);
               }
            }

            var1 = false;
            return var1;
         }

         try {
            var0.reset();
         } catch (IOException var12) {
            var1 = var2;
            if(Log.isLoggable("Downsampler", 5)) {
               Log.w("Downsampler", "Cannot reset the input stream", var12);
               var1 = var2;
            }

            return var1;
         }

         var1 = var2;
      }

      return var1;
   }

   private static Bitmap b(com.bumptech.glide.i.f var0, RecyclableBufferedInputStream var1, Options var2) {
      if(var2.inJustDecodeBounds) {
         var0.mark(5242880);
      } else {
         var1.a();
      }

      Bitmap var4 = BitmapFactory.decodeStream(var0, (Rect)null, var2);

      try {
         if(var2.inJustDecodeBounds) {
            var0.reset();
         }
      } catch (IOException var3) {
         if(Log.isLoggable("Downsampler", 6)) {
            Log.e("Downsampler", "Exception loading inDecodeBounds=" + var2.inJustDecodeBounds + " sample=" + var2.inSampleSize, var3);
         }
      }

      return var4;
   }

   @TargetApi(11)
   private static Options b() {
      // $FF: Couldn't be decompiled
   }

   @TargetApi(11)
   private static void b(Options var0) {
      var0.inTempStorage = null;
      var0.inDither = false;
      var0.inScaled = false;
      var0.inSampleSize = 1;
      var0.inPreferredConfig = null;
      var0.inJustDecodeBounds = false;
      var0.outWidth = 0;
      var0.outHeight = 0;
      var0.outMimeType = null;
      if(11 <= VERSION.SDK_INT) {
         var0.inBitmap = null;
         var0.inMutable = true;
      }

   }

   protected abstract int a(int var1, int var2, int var3, int var4);

   public Bitmap a(InputStream param1, com.bumptech.glide.load.engine.a.c param2, int param3, int param4, com.bumptech.glide.load.a param5) {
      // $FF: Couldn't be decompiled
   }

   public int[] a(com.bumptech.glide.i.f var1, RecyclableBufferedInputStream var2, Options var3) {
      var3.inJustDecodeBounds = true;
      b(var1, var2, var3);
      var3.inJustDecodeBounds = false;
      return new int[]{var3.outWidth, var3.outHeight};
   }
}
