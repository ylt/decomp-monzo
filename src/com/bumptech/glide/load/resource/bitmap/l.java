package com.bumptech.glide.load.resource.bitmap;

import android.os.ParcelFileDescriptor;
import android.util.Log;
import java.io.IOException;
import java.io.InputStream;

public class l implements com.bumptech.glide.load.e {
   private final com.bumptech.glide.load.e a;
   private final com.bumptech.glide.load.e b;

   public l(com.bumptech.glide.load.e var1, com.bumptech.glide.load.e var2) {
      this.a = var1;
      this.b = var2;
   }

   public com.bumptech.glide.load.engine.k a(com.bumptech.glide.load.b.g var1, int var2, int var3) throws IOException {
      com.bumptech.glide.load.engine.k var8;
      label25: {
         InputStream var4 = var1.a();
         if(var4 != null) {
            try {
               var8 = this.a.a(var4, var2, var3);
               break label25;
            } catch (IOException var6) {
               if(Log.isLoggable("ImageVideoDecoder", 2)) {
                  Log.v("ImageVideoDecoder", "Failed to load image from stream, trying FileDescriptor", var6);
               }
            }
         }

         var8 = null;
      }

      com.bumptech.glide.load.engine.k var5 = var8;
      if(var8 == null) {
         ParcelFileDescriptor var7 = var1.b();
         var5 = var8;
         if(var7 != null) {
            var5 = this.b.a(var7, var2, var3);
         }
      }

      return var5;
   }

   public String a() {
      return "ImageVideoBitmapDecoder.com.bumptech.glide.load.resource.bitmap";
   }
}
