package com.bumptech.glide.load.resource.bitmap;

import android.graphics.Bitmap;

public class c implements com.bumptech.glide.load.engine.k {
   private final Bitmap a;
   private final com.bumptech.glide.load.engine.a.c b;

   public c(Bitmap var1, com.bumptech.glide.load.engine.a.c var2) {
      if(var1 == null) {
         throw new NullPointerException("Bitmap must not be null");
      } else if(var2 == null) {
         throw new NullPointerException("BitmapPool must not be null");
      } else {
         this.a = var1;
         this.b = var2;
      }
   }

   public static c a(Bitmap var0, com.bumptech.glide.load.engine.a.c var1) {
      c var2;
      if(var0 == null) {
         var2 = null;
      } else {
         var2 = new c(var0, var1);
      }

      return var2;
   }

   public Bitmap a() {
      return this.a;
   }

   // $FF: synthetic method
   public Object b() {
      return this.a();
   }

   public int c() {
      return com.bumptech.glide.i.h.a(this.a);
   }

   public void d() {
      if(!this.b.a(this.a)) {
         this.a.recycle();
      }

   }
}
