package com.bumptech.glide.load.resource.bitmap;

public class g implements com.bumptech.glide.f.b {
   private final com.bumptech.glide.load.e a;
   private final h b;
   private final b c;
   private final com.bumptech.glide.load.b d;

   public g(com.bumptech.glide.load.engine.a.c var1, com.bumptech.glide.load.a var2) {
      this.a = new com.bumptech.glide.load.resource.b.c(new o(var1, var2));
      this.b = new h(var1, var2);
      this.c = new b();
      this.d = com.bumptech.glide.load.resource.a.b();
   }

   public com.bumptech.glide.load.e a() {
      return this.a;
   }

   public com.bumptech.glide.load.e b() {
      return this.b;
   }

   public com.bumptech.glide.load.b c() {
      return this.d;
   }

   public com.bumptech.glide.load.f d() {
      return this.c;
   }
}
