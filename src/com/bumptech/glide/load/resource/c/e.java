package com.bumptech.glide.load.resource.c;

import android.graphics.Bitmap;
import com.bumptech.glide.load.engine.k;

public class e implements com.bumptech.glide.load.g {
   private final com.bumptech.glide.load.g a;
   private final com.bumptech.glide.load.engine.a.c b;

   public e(com.bumptech.glide.load.g var1, com.bumptech.glide.load.engine.a.c var2) {
      this.a = var1;
      this.b = var2;
   }

   public k a(k var1, int var2, int var3) {
      b var5 = (b)((k)var1).b();
      Bitmap var4 = ((b)((k)var1).b()).b();
      com.bumptech.glide.load.resource.bitmap.c var6 = new com.bumptech.glide.load.resource.bitmap.c(var4, this.b);
      Bitmap var7 = (Bitmap)this.a.a(var6, var2, var3).b();
      if(!var7.equals(var4)) {
         var1 = new d(new b(var5, var7, this.a));
      }

      return (k)var1;
   }

   public String a() {
      return this.a.a();
   }
}
