package com.bumptech.glide.load.resource.c;

import android.graphics.Bitmap;
import android.util.Log;
import com.bumptech.glide.load.engine.k;
import java.io.IOException;
import java.io.OutputStream;

public class j implements com.bumptech.glide.load.f {
   private static final j.a a = new j.a();
   private final com.bumptech.glide.b.a.a b;
   private final com.bumptech.glide.load.engine.a.c c;
   private final j.a d;

   public j(com.bumptech.glide.load.engine.a.c var1) {
      this(var1, a);
   }

   j(com.bumptech.glide.load.engine.a.c var1, j.a var2) {
      this.c = var1;
      this.b = new a(var1);
      this.d = var2;
   }

   private com.bumptech.glide.b.a a(byte[] var1) {
      com.bumptech.glide.b.d var2 = this.d.a();
      var2.a(var1);
      com.bumptech.glide.b.c var3 = var2.b();
      com.bumptech.glide.b.a var4 = this.d.a(this.b);
      var4.a(var3, var1);
      var4.a();
      return var4;
   }

   private k a(Bitmap var1, com.bumptech.glide.load.g var2, b var3) {
      k var4 = this.d.a(var1, this.c);
      k var5 = var2.a(var4, var3.getIntrinsicWidth(), var3.getIntrinsicHeight());
      if(!var4.equals(var5)) {
         var4.d();
      }

      return var5;
   }

   private boolean a(byte[] var1, OutputStream var2) {
      boolean var3 = true;

      try {
         var2.write(var1);
      } catch (IOException var4) {
         if(Log.isLoggable("GifEncoder", 3)) {
            Log.d("GifEncoder", "Failed to write data to output stream in GifResourceEncoder", var4);
         }

         var3 = false;
      }

      return var3;
   }

   public String a() {
      return "";
   }

   public boolean a(k param1, OutputStream param2) {
      // $FF: Couldn't be decompiled
   }

   static class a {
      public com.bumptech.glide.b.a a(com.bumptech.glide.b.a.a var1) {
         return new com.bumptech.glide.b.a(var1);
      }

      public com.bumptech.glide.b.d a() {
         return new com.bumptech.glide.b.d();
      }

      public k a(Bitmap var1, com.bumptech.glide.load.engine.a.c var2) {
         return new com.bumptech.glide.load.resource.bitmap.c(var1, var2);
      }

      public com.bumptech.glide.c.a b() {
         return new com.bumptech.glide.c.a();
      }
   }
}
