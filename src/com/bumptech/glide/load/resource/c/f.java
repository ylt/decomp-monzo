package com.bumptech.glide.load.resource.c;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.os.Handler.Callback;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.util.UUID;

class f {
   private final f.b a;
   private final com.bumptech.glide.b.a b;
   private final Handler c;
   private boolean d;
   private boolean e;
   private com.bumptech.glide.e f;
   private f.a g;
   private boolean h;

   public f(Context var1, f.b var2, com.bumptech.glide.b.a var3, int var4, int var5) {
      this(var2, var3, (Handler)null, a(var1, var3, var4, var5, com.bumptech.glide.g.a(var1).a()));
   }

   f(f.b var1, com.bumptech.glide.b.a var2, Handler var3, com.bumptech.glide.e var4) {
      this.d = false;
      this.e = false;
      Handler var5 = var3;
      if(var3 == null) {
         var5 = new Handler(Looper.getMainLooper(), new f.c());
      }

      this.a = var1;
      this.b = var2;
      this.c = var5;
      this.f = var4;
   }

   private static com.bumptech.glide.e a(Context var0, com.bumptech.glide.b.a var1, int var2, int var3, com.bumptech.glide.load.engine.a.c var4) {
      h var7 = new h(var4);
      g var6 = new g();
      com.bumptech.glide.load.b var5 = com.bumptech.glide.load.resource.a.b();
      return com.bumptech.glide.g.b(var0).a(var6, com.bumptech.glide.b.a.class).a((Object)var1).a(Bitmap.class).b(var5).b((com.bumptech.glide.load.e)var7).b(true).b(com.bumptech.glide.load.engine.b.b).b(var2, var3);
   }

   private void e() {
      if(this.d && !this.e) {
         this.e = true;
         long var3 = SystemClock.uptimeMillis();
         long var1 = (long)this.b.b();
         this.b.a();
         f.a var5 = new f.a(this.c, this.b.d(), var3 + var1);
         this.f.b((com.bumptech.glide.load.c)(new f.d())).a((com.bumptech.glide.g.b.j)var5);
      }

   }

   public void a() {
      if(!this.d) {
         this.d = true;
         this.h = false;
         this.e();
      }

   }

   public void a(com.bumptech.glide.load.g var1) {
      if(var1 == null) {
         throw new NullPointerException("Transformation must not be null");
      } else {
         this.f = this.f.b(new com.bumptech.glide.load.g[]{var1});
      }
   }

   void a(f.a var1) {
      if(this.h) {
         this.c.obtainMessage(2, var1).sendToTarget();
      } else {
         f.a var2 = this.g;
         this.g = var1;
         this.a.b(var1.b);
         if(var2 != null) {
            this.c.obtainMessage(2, var2).sendToTarget();
         }

         this.e = false;
         this.e();
      }

   }

   public void b() {
      this.d = false;
   }

   public void c() {
      this.b();
      if(this.g != null) {
         com.bumptech.glide.g.a((com.bumptech.glide.g.b.j)this.g);
         this.g = null;
      }

      this.h = true;
   }

   public Bitmap d() {
      Bitmap var1;
      if(this.g != null) {
         var1 = this.g.a();
      } else {
         var1 = null;
      }

      return var1;
   }

   static class a extends com.bumptech.glide.g.b.g {
      private final Handler a;
      private final int b;
      private final long c;
      private Bitmap d;

      public a(Handler var1, int var2, long var3) {
         this.a = var1;
         this.b = var2;
         this.c = var3;
      }

      public Bitmap a() {
         return this.d;
      }

      public void a(Bitmap var1, com.bumptech.glide.g.a.c var2) {
         this.d = var1;
         Message var3 = this.a.obtainMessage(1, this);
         this.a.sendMessageAtTime(var3, this.c);
      }
   }

   public interface b {
      void b(int var1);
   }

   private class c implements Callback {
      private c() {
      }

      // $FF: synthetic method
      c(Object var2) {
         this();
      }

      public boolean handleMessage(Message var1) {
         boolean var2;
         if(var1.what == 1) {
            f.a var3 = (f.a)var1.obj;
            f.this.a(var3);
            var2 = true;
         } else {
            if(var1.what == 2) {
               com.bumptech.glide.g.a((com.bumptech.glide.g.b.j)((f.a)var1.obj));
            }

            var2 = false;
         }

         return var2;
      }
   }

   static class d implements com.bumptech.glide.load.c {
      private final UUID a;

      public d() {
         this(UUID.randomUUID());
      }

      d(UUID var1) {
         this.a = var1;
      }

      public void a(MessageDigest var1) throws UnsupportedEncodingException {
         throw new UnsupportedOperationException("Not implemented");
      }

      public boolean equals(Object var1) {
         boolean var2;
         if(var1 instanceof f.d) {
            var2 = ((f.d)var1).a.equals(this.a);
         } else {
            var2 = false;
         }

         return var2;
      }

      public int hashCode() {
         return this.a.hashCode();
      }
   }
}
