package com.bumptech.glide.load.resource.c;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;

class a implements com.bumptech.glide.b.a.a {
   private final com.bumptech.glide.load.engine.a.c a;

   public a(com.bumptech.glide.load.engine.a.c var1) {
      this.a = var1;
   }

   public Bitmap a(int var1, int var2, Config var3) {
      return this.a.b(var1, var2, var3);
   }

   public void a(Bitmap var1) {
      if(!this.a.a(var1)) {
         var1.recycle();
      }

   }
}
