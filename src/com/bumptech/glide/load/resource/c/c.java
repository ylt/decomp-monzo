package com.bumptech.glide.load.resource.c;

import android.content.Context;
import com.bumptech.glide.load.b.o;

public class c implements com.bumptech.glide.f.b {
   private final i a;
   private final j b;
   private final o c;
   private final com.bumptech.glide.load.resource.b.c d;

   public c(Context var1, com.bumptech.glide.load.engine.a.c var2) {
      this.a = new i(var1, var2);
      this.d = new com.bumptech.glide.load.resource.b.c(this.a);
      this.b = new j(var2);
      this.c = new o();
   }

   public com.bumptech.glide.load.e a() {
      return this.d;
   }

   public com.bumptech.glide.load.e b() {
      return this.a;
   }

   public com.bumptech.glide.load.b c() {
      return this.c;
   }

   public com.bumptech.glide.load.f d() {
      return this.b;
   }
}
