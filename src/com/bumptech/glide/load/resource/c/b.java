package com.bumptech.glide.load.resource.c;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Drawable.ConstantState;
import android.os.Build.VERSION;
import android.view.Gravity;

public class b extends com.bumptech.glide.load.resource.a.b implements f.b {
   private final Paint a;
   private final Rect b;
   private final b.a c;
   private final com.bumptech.glide.b.a d;
   private final f e;
   private boolean f;
   private boolean g;
   private boolean h;
   private boolean i;
   private int j;
   private int k;
   private boolean l;

   public b(Context var1, com.bumptech.glide.b.a.a var2, com.bumptech.glide.load.engine.a.c var3, com.bumptech.glide.load.g var4, int var5, int var6, com.bumptech.glide.b.c var7, byte[] var8, Bitmap var9) {
      this(new b.a(var7, var8, var1, var4, var5, var6, var2, var3, var9));
   }

   b(b.a var1) {
      this.b = new Rect();
      this.i = true;
      this.k = -1;
      if(var1 == null) {
         throw new NullPointerException("GifState must not be null");
      } else {
         this.c = var1;
         this.d = new com.bumptech.glide.b.a(var1.g);
         this.a = new Paint();
         this.d.a(var1.a, var1.b);
         this.e = new f(var1.c, this, this.d, var1.e, var1.f);
         this.e.a(var1.d);
      }
   }

   public b(b var1, Bitmap var2, com.bumptech.glide.load.g var3) {
      this(new b.a(var1.c.a, var1.c.b, var1.c.c, var3, var1.c.e, var1.c.f, var1.c.g, var1.c.h, var2));
   }

   private void g() {
      this.j = 0;
   }

   private void h() {
      this.e.c();
      this.invalidateSelf();
   }

   private void i() {
      if(this.d.c() == 1) {
         this.invalidateSelf();
      } else if(!this.f) {
         this.f = true;
         this.e.a();
         this.invalidateSelf();
      }

   }

   private void j() {
      this.f = false;
      this.e.b();
   }

   public void a(int var1) {
      byte var2 = -1;
      if(var1 <= 0 && var1 != -1 && var1 != 0) {
         throw new IllegalArgumentException("Loop count must be greater than 0, or equal to GlideDrawable.LOOP_FOREVER, or equal to GlideDrawable.LOOP_INTRINSIC");
      } else {
         if(var1 == 0) {
            var1 = this.d.e();
            if(var1 == 0) {
               var1 = var2;
            }

            this.k = var1;
         } else {
            this.k = var1;
         }

      }
   }

   public boolean a() {
      return true;
   }

   public Bitmap b() {
      return this.c.i;
   }

   @TargetApi(11)
   public void b(int var1) {
      if(VERSION.SDK_INT >= 11 && this.getCallback() == null) {
         this.stop();
         this.h();
      } else {
         this.invalidateSelf();
         if(var1 == this.d.c() - 1) {
            ++this.j;
         }

         if(this.k != -1 && this.j >= this.k) {
            this.stop();
         }
      }

   }

   public com.bumptech.glide.load.g c() {
      return this.c.d;
   }

   public byte[] d() {
      return this.c.b;
   }

   public void draw(Canvas var1) {
      if(!this.h) {
         if(this.l) {
            Gravity.apply(119, this.getIntrinsicWidth(), this.getIntrinsicHeight(), this.getBounds(), this.b);
            this.l = false;
         }

         Bitmap var2 = this.e.d();
         if(var2 == null) {
            var2 = this.c.i;
         }

         var1.drawBitmap(var2, (Rect)null, this.b, this.a);
      }

   }

   public int e() {
      return this.d.c();
   }

   public void f() {
      this.h = true;
      this.c.h.a(this.c.i);
      this.e.c();
      this.e.b();
   }

   public ConstantState getConstantState() {
      return this.c;
   }

   public int getIntrinsicHeight() {
      return this.c.i.getHeight();
   }

   public int getIntrinsicWidth() {
      return this.c.i.getWidth();
   }

   public int getOpacity() {
      return -2;
   }

   public boolean isRunning() {
      return this.f;
   }

   protected void onBoundsChange(Rect var1) {
      super.onBoundsChange(var1);
      this.l = true;
   }

   public void setAlpha(int var1) {
      this.a.setAlpha(var1);
   }

   public void setColorFilter(ColorFilter var1) {
      this.a.setColorFilter(var1);
   }

   public boolean setVisible(boolean var1, boolean var2) {
      this.i = var1;
      if(!var1) {
         this.j();
      } else if(this.g) {
         this.i();
      }

      return super.setVisible(var1, var2);
   }

   public void start() {
      this.g = true;
      this.g();
      if(this.i) {
         this.i();
      }

   }

   public void stop() {
      this.g = false;
      this.j();
      if(VERSION.SDK_INT < 11) {
         this.h();
      }

   }

   static class a extends ConstantState {
      com.bumptech.glide.b.c a;
      byte[] b;
      Context c;
      com.bumptech.glide.load.g d;
      int e;
      int f;
      com.bumptech.glide.b.a.a g;
      com.bumptech.glide.load.engine.a.c h;
      Bitmap i;

      public a(com.bumptech.glide.b.c var1, byte[] var2, Context var3, com.bumptech.glide.load.g var4, int var5, int var6, com.bumptech.glide.b.a.a var7, com.bumptech.glide.load.engine.a.c var8, Bitmap var9) {
         if(var9 == null) {
            throw new NullPointerException("The first frame of the GIF must not be null");
         } else {
            this.a = var1;
            this.b = var2;
            this.h = var8;
            this.i = var9;
            this.c = var3.getApplicationContext();
            this.d = var4;
            this.e = var5;
            this.f = var6;
            this.g = var7;
         }
      }

      public int getChangingConfigurations() {
         return 0;
      }

      public Drawable newDrawable() {
         return new b(this);
      }

      public Drawable newDrawable(Resources var1) {
         return this.newDrawable();
      }
   }
}
