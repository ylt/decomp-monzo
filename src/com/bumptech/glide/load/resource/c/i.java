package com.bumptech.glide.load.resource.c;

import android.content.Context;
import android.graphics.Bitmap;
import java.io.InputStream;
import java.util.Queue;

public class i implements com.bumptech.glide.load.e {
   private static final i.b a = new i.b();
   private static final i.a b = new i.a();
   private final Context c;
   private final i.b d;
   private final com.bumptech.glide.load.engine.a.c e;
   private final i.a f;
   private final a g;

   public i(Context var1, com.bumptech.glide.load.engine.a.c var2) {
      this(var1, var2, a, b);
   }

   i(Context var1, com.bumptech.glide.load.engine.a.c var2, i.b var3, i.a var4) {
      this.c = var1.getApplicationContext();
      this.e = var2;
      this.f = var4;
      this.g = new a(var2);
      this.d = var3;
   }

   private Bitmap a(com.bumptech.glide.b.a var1, com.bumptech.glide.b.c var2, byte[] var3) {
      var1.a(var2, var3);
      var1.a();
      return var1.f();
   }

   private d a(byte[] var1, int var2, int var3, com.bumptech.glide.b.d var4, com.bumptech.glide.b.a var5) {
      Object var6 = null;
      com.bumptech.glide.b.c var7 = var4.b();
      d var8 = (d)var6;
      if(var7.a() > 0) {
         if(var7.b() != 0) {
            var8 = (d)var6;
         } else {
            Bitmap var9 = this.a(var5, var7, var1);
            var8 = (d)var6;
            if(var9 != null) {
               com.bumptech.glide.load.resource.d var10 = com.bumptech.glide.load.resource.d.b();
               var8 = new d(new b(this.c, this.g, this.e, var10, var2, var3, var7, var1, var9));
            }
         }
      }

      return var8;
   }

   private static byte[] a(InputStream param0) {
      // $FF: Couldn't be decompiled
   }

   public d a(InputStream var1, int var2, int var3) {
      byte[] var5 = a(var1);
      com.bumptech.glide.b.d var4 = this.d.a(var5);
      com.bumptech.glide.b.a var9 = this.f.a((com.bumptech.glide.b.a.a)this.g);

      d var8;
      try {
         var8 = this.a(var5, var2, var3, var4, var9);
      } finally {
         this.d.a(var4);
         this.f.a(var9);
      }

      return var8;
   }

   public String a() {
      return "";
   }

   static class a {
      private final Queue a = com.bumptech.glide.i.h.a(0);

      public com.bumptech.glide.b.a a(com.bumptech.glide.b.a.a param1) {
         // $FF: Couldn't be decompiled
      }

      public void a(com.bumptech.glide.b.a var1) {
         synchronized(this){}

         try {
            var1.g();
            this.a.offer(var1);
         } finally {
            ;
         }

      }
   }

   static class b {
      private final Queue a = com.bumptech.glide.i.h.a(0);

      public com.bumptech.glide.b.d a(byte[] param1) {
         // $FF: Couldn't be decompiled
      }

      public void a(com.bumptech.glide.b.d var1) {
         synchronized(this){}

         try {
            var1.a();
            this.a.offer(var1);
         } finally {
            ;
         }

      }
   }
}
