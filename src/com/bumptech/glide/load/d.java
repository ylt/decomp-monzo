package com.bumptech.glide.load;

import com.bumptech.glide.load.engine.k;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;

public class d implements g {
   private final Collection a;
   private String b;

   @SafeVarargs
   public d(g... var1) {
      if(var1.length < 1) {
         throw new IllegalArgumentException("MultiTransformation must contain at least one Transformation");
      } else {
         this.a = Arrays.asList(var1);
      }
   }

   public k a(k var1, int var2, int var3) {
      Iterator var6 = this.a.iterator();

      k var4;
      k var5;
      for(var4 = var1; var6.hasNext(); var4 = var5) {
         var5 = ((g)var6.next()).a(var4, var2, var3);
         if(var4 != null && !var4.equals(var1) && !var4.equals(var5)) {
            var4.d();
         }
      }

      return var4;
   }

   public String a() {
      if(this.b == null) {
         StringBuilder var2 = new StringBuilder();
         Iterator var1 = this.a.iterator();

         while(var1.hasNext()) {
            var2.append(((g)var1.next()).a());
         }

         this.b = var2.toString();
      }

      return this.b;
   }
}
