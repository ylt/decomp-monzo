package com.bumptech.glide.load.b;

import android.net.Uri;
import android.text.TextUtils;
import java.io.File;

public class p implements l {
   private final l a;

   public p(l var1) {
      this.a = var1;
   }

   private static Uri a(String var0) {
      return Uri.fromFile(new File(var0));
   }

   public com.bumptech.glide.load.a.c a(String var1, int var2, int var3) {
      com.bumptech.glide.load.a.c var6;
      if(TextUtils.isEmpty(var1)) {
         var6 = null;
      } else {
         Uri var4;
         if(var1.startsWith("/")) {
            var4 = a(var1);
         } else {
            Uri var5 = Uri.parse(var1);
            var4 = var5;
            if(var5.getScheme() == null) {
               var4 = a(var1);
            }
         }

         var6 = this.a.a(var4, var2, var3);
      }

      return var6;
   }
}
