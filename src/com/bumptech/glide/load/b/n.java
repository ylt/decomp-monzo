package com.bumptech.glide.load.b;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.Resources.NotFoundException;
import android.net.Uri;
import android.util.Log;

public class n implements l {
   private final l a;
   private final Resources b;

   public n(Context var1, l var2) {
      this(var1.getResources(), var2);
   }

   public n(Resources var1, l var2) {
      this.b = var1;
      this.a = var2;
   }

   public com.bumptech.glide.load.a.c a(Integer var1, int var2, int var3) {
      com.bumptech.glide.load.a.c var4 = null;

      Uri var7;
      label23: {
         Uri var8;
         try {
            StringBuilder var5 = new StringBuilder();
            var8 = Uri.parse(var5.append("android.resource://").append(this.b.getResourcePackageName(var1.intValue())).append('/').append(this.b.getResourceTypeName(var1.intValue())).append('/').append(this.b.getResourceEntryName(var1.intValue())).toString());
         } catch (NotFoundException var6) {
            if(Log.isLoggable("ResourceLoader", 5)) {
               Log.w("ResourceLoader", "Received invalid resource id: " + var1, var6);
            }

            var7 = null;
            break label23;
         }

         var7 = var8;
      }

      if(var7 != null) {
         var4 = this.a.a(var7, var2, var3);
      }

      return var4;
   }
}
