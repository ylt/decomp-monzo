package com.bumptech.glide.load.b;

import android.os.ParcelFileDescriptor;
import android.util.Log;
import java.io.InputStream;

public class f implements l {
   private final l a;
   private final l b;

   public f(l var1, l var2) {
      if(var1 == null && var2 == null) {
         throw new NullPointerException("At least one of streamLoader and fileDescriptorLoader must be non null");
      } else {
         this.a = var1;
         this.b = var2;
      }
   }

   public com.bumptech.glide.load.a.c a(Object var1, int var2, int var3) {
      f.a var5 = null;
      com.bumptech.glide.load.a.c var4;
      if(this.a != null) {
         var4 = this.a.a(var1, var2, var3);
      } else {
         var4 = null;
      }

      com.bumptech.glide.load.a.c var6;
      if(this.b != null) {
         var6 = this.b.a(var1, var2, var3);
      } else {
         var6 = null;
      }

      if(var4 != null || var6 != null) {
         var5 = new f.a(var4, var6);
      }

      return var5;
   }

   static class a implements com.bumptech.glide.load.a.c {
      private final com.bumptech.glide.load.a.c a;
      private final com.bumptech.glide.load.a.c b;

      public a(com.bumptech.glide.load.a.c var1, com.bumptech.glide.load.a.c var2) {
         this.a = var1;
         this.b = var2;
      }

      // $FF: synthetic method
      public Object a(com.bumptech.glide.i var1) throws Exception {
         return this.b(var1);
      }

      public void a() {
         if(this.a != null) {
            this.a.a();
         }

         if(this.b != null) {
            this.b.a();
         }

      }

      public g b(com.bumptech.glide.i var1) throws Exception {
         InputStream var2;
         Object var4;
         label38: {
            var4 = null;
            if(this.a != null) {
               try {
                  var2 = (InputStream)this.a.a(var1);
                  break label38;
               } catch (Exception var6) {
                  if(Log.isLoggable("IVML", 2)) {
                     Log.v("IVML", "Exception fetching input stream, trying ParcelFileDescriptor", var6);
                  }

                  if(this.b == null) {
                     throw var6;
                  }
               }
            }

            var2 = null;
         }

         ParcelFileDescriptor var3 = (ParcelFileDescriptor)var4;
         if(this.b != null) {
            try {
               var3 = (ParcelFileDescriptor)this.b.a(var1);
            } catch (Exception var5) {
               if(Log.isLoggable("IVML", 2)) {
                  Log.v("IVML", "Exception fetching ParcelFileDescriptor", var5);
               }

               var3 = (ParcelFileDescriptor)var4;
               if(var2 == null) {
                  throw var5;
               }
            }
         }

         return new g(var2, var3);
      }

      public String b() {
         String var1;
         if(this.a != null) {
            var1 = this.a.b();
         } else {
            var1 = this.b.b();
         }

         return var1;
      }

      public void c() {
         if(this.a != null) {
            this.a.c();
         }

         if(this.b != null) {
            this.b.c();
         }

      }
   }
}
