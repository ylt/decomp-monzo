package com.bumptech.glide.load.b;

import java.util.Queue;

public class k {
   private final com.bumptech.glide.i.e a;

   public k() {
      this(250);
   }

   public k(int var1) {
      this.a = new com.bumptech.glide.i.e(var1) {
         protected void a(k.a var1, Object var2) {
            var1.a();
         }
      };
   }

   public Object a(Object var1, int var2, int var3) {
      k.a var5 = k.a.a(var1, var2, var3);
      Object var4 = this.a.b(var5);
      var5.a();
      return var4;
   }

   public void a(Object var1, int var2, int var3, Object var4) {
      k.a var5 = k.a.a(var1, var2, var3);
      this.a.b(var5, var4);
   }

   static final class a {
      private static final Queue a = com.bumptech.glide.i.h.a(0);
      private int b;
      private int c;
      private Object d;

      static k.a a(Object var0, int var1, int var2) {
         k.a var4 = (k.a)a.poll();
         k.a var3 = var4;
         if(var4 == null) {
            var3 = new k.a();
         }

         var3.b(var0, var1, var2);
         return var3;
      }

      private void b(Object var1, int var2, int var3) {
         this.d = var1;
         this.c = var2;
         this.b = var3;
      }

      public void a() {
         a.offer(this);
      }

      public boolean equals(Object var1) {
         boolean var3 = false;
         boolean var2 = var3;
         if(var1 instanceof k.a) {
            k.a var4 = (k.a)var1;
            var2 = var3;
            if(this.c == var4.c) {
               var2 = var3;
               if(this.b == var4.b) {
                  var2 = var3;
                  if(this.d.equals(var4.d)) {
                     var2 = true;
                  }
               }
            }
         }

         return var2;
      }

      public int hashCode() {
         return (this.b * 31 + this.c) * 31 + this.d.hashCode();
      }
   }
}
