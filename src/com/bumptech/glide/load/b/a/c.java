package com.bumptech.glide.load.b.a;

import android.content.Context;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import com.bumptech.glide.load.b.l;
import com.bumptech.glide.load.b.m;
import com.bumptech.glide.load.b.n;

public class c extends n implements b {
   public c(Context var1, l var2) {
      super(var1, var2);
   }

   public static class a implements m {
      public l a(Context var1, com.bumptech.glide.load.b.c var2) {
         return new c(var1, var2.a(Uri.class, ParcelFileDescriptor.class));
      }

      public void a() {
      }
   }
}
