package com.bumptech.glide.load.b.a;

import android.content.Context;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import com.bumptech.glide.load.b.l;
import com.bumptech.glide.load.b.m;
import com.bumptech.glide.load.b.p;

public class d extends p implements b {
   public d(l var1) {
      super(var1);
   }

   public static class a implements m {
      public l a(Context var1, com.bumptech.glide.load.b.c var2) {
         return new d(var2.a(Uri.class, ParcelFileDescriptor.class));
      }

      public void a() {
      }
   }
}
