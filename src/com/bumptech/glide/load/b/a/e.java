package com.bumptech.glide.load.b.a;

import android.content.Context;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import com.bumptech.glide.load.b.l;
import com.bumptech.glide.load.b.m;
import com.bumptech.glide.load.b.q;

public class e extends q implements b {
   public e(Context var1, l var2) {
      super(var1, var2);
   }

   protected com.bumptech.glide.load.a.c a(Context var1, Uri var2) {
      return new com.bumptech.glide.load.a.e(var1, var2);
   }

   protected com.bumptech.glide.load.a.c a(Context var1, String var2) {
      return new com.bumptech.glide.load.a.d(var1.getApplicationContext().getAssets(), var2);
   }

   public static class a implements m {
      public l a(Context var1, com.bumptech.glide.load.b.c var2) {
         return new e(var1, var2.a(com.bumptech.glide.load.b.d.class, ParcelFileDescriptor.class));
      }

      public void a() {
      }
   }
}
