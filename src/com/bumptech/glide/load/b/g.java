package com.bumptech.glide.load.b;

import android.os.ParcelFileDescriptor;
import java.io.InputStream;

public class g {
   private final InputStream a;
   private final ParcelFileDescriptor b;

   public g(InputStream var1, ParcelFileDescriptor var2) {
      this.a = var1;
      this.b = var2;
   }

   public InputStream a() {
      return this.a;
   }

   public ParcelFileDescriptor b() {
      return this.b;
   }
}
