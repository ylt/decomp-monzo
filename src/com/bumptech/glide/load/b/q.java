package com.bumptech.glide.load.b;

import android.content.Context;
import android.net.Uri;

public abstract class q implements l {
   private final Context a;
   private final l b;

   public q(Context var1, l var2) {
      this.a = var1;
      this.b = var2;
   }

   private static boolean a(String var0) {
      boolean var1;
      if(!"file".equals(var0) && !"content".equals(var0) && !"android.resource".equals(var0)) {
         var1 = false;
      } else {
         var1 = true;
      }

      return var1;
   }

   protected abstract com.bumptech.glide.load.a.c a(Context var1, Uri var2);

   protected abstract com.bumptech.glide.load.a.c a(Context var1, String var2);

   public final com.bumptech.glide.load.a.c a(Uri var1, int var2, int var3) {
      String var6 = var1.getScheme();
      Object var5 = null;
      com.bumptech.glide.load.a.c var4;
      if(a(var6)) {
         if(a.a(var1)) {
            String var7 = a.b(var1);
            var4 = this.a(this.a, var7);
         } else {
            var4 = this.a(this.a, var1);
         }
      } else {
         var4 = (com.bumptech.glide.load.a.c)var5;
         if(this.b != null) {
            if(!"http".equals(var6)) {
               var4 = (com.bumptech.glide.load.a.c)var5;
               if(!"https".equals(var6)) {
                  return var4;
               }
            }

            var4 = this.b.a(new d(var1.toString()), var2, var3);
         }
      }

      return var4;
   }
}
