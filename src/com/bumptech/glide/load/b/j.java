package com.bumptech.glide.load.b;

import android.text.TextUtils;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public final class j implements e {
   private final Map c;
   private volatile Map d;

   j(Map var1) {
      this.c = Collections.unmodifiableMap(var1);
   }

   private Map b() {
      HashMap var4 = new HashMap();
      Iterator var6 = this.c.entrySet().iterator();

      while(var6.hasNext()) {
         Entry var3 = (Entry)var6.next();
         StringBuilder var5 = new StringBuilder();
         List var2 = (List)var3.getValue();

         for(int var1 = 0; var1 < var2.size(); ++var1) {
            var5.append(((i)var2.get(var1)).a());
            if(var1 != var2.size() - 1) {
               var5.append(',');
            }
         }

         var4.put(var3.getKey(), var5.toString());
      }

      return var4;
   }

   public Map a() {
      // $FF: Couldn't be decompiled
   }

   public boolean equals(Object var1) {
      boolean var2;
      if(var1 instanceof j) {
         j var3 = (j)var1;
         var2 = this.c.equals(var3.c);
      } else {
         var2 = false;
      }

      return var2;
   }

   public int hashCode() {
      return this.c.hashCode();
   }

   public String toString() {
      return "LazyHeaders{headers=" + this.c + '}';
   }

   public static final class a {
      private static final String a = System.getProperty("http.agent");
      private static final Map b;
      private boolean c = true;
      private Map d;
      private boolean e;
      private boolean f;

      static {
         HashMap var0 = new HashMap(2);
         if(!TextUtils.isEmpty(a)) {
            var0.put("User-Agent", Collections.singletonList(new j.b(a)));
         }

         var0.put("Accept-Encoding", Collections.singletonList(new j.b("identity")));
         b = Collections.unmodifiableMap(var0);
      }

      public a() {
         this.d = b;
         this.e = true;
         this.f = true;
      }

      public j a() {
         this.c = true;
         return new j(this.d);
      }
   }

   static final class b implements i {
      private final String a;

      b(String var1) {
         this.a = var1;
      }

      public String a() {
         return this.a;
      }

      public boolean equals(Object var1) {
         boolean var2;
         if(var1 instanceof j.b) {
            j.b var3 = (j.b)var1;
            var2 = this.a.equals(var3.a);
         } else {
            var2 = false;
         }

         return var2;
      }

      public int hashCode() {
         return this.a.hashCode();
      }

      public String toString() {
         return "StringHeaderFactory{value='" + this.a + '\'' + '}';
      }
   }
}
