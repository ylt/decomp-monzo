package com.bumptech.glide.load.b;

import android.content.Context;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class c {
   private static final l c = new l() {
      public com.bumptech.glide.load.a.c a(Object var1, int var2, int var3) {
         throw new NoSuchMethodError("This should never be called!");
      }

      public String toString() {
         return "NULL_MODEL_LOADER";
      }
   };
   private final Map a = new HashMap();
   private final Map b = new HashMap();
   private final Context d;

   public c(Context var1) {
      this.d = var1.getApplicationContext();
   }

   private void a(Class var1, Class var2, l var3) {
      Map var5 = (Map)this.b.get(var1);
      Object var4 = var5;
      if(var5 == null) {
         var4 = new HashMap();
         this.b.put(var1, var4);
      }

      ((Map)var4).put(var2, var3);
   }

   private void b(Class var1, Class var2) {
      this.a(var1, var2, c);
   }

   private l c(Class var1, Class var2) {
      Map var3 = (Map)this.b.get(var1);
      l var4;
      if(var3 != null) {
         var4 = (l)var3.get(var2);
      } else {
         var4 = null;
      }

      return var4;
   }

   private m d(Class var1, Class var2) {
      Map var3 = (Map)this.a.get(var1);
      m var6;
      if(var3 != null) {
         var6 = (m)var3.get(var2);
      } else {
         var6 = null;
      }

      m var4 = var6;
      if(var6 == null) {
         Iterator var5 = this.a.keySet().iterator();

         while(true) {
            if(!var5.hasNext()) {
               var4 = var6;
               break;
            }

            Class var7 = (Class)var5.next();
            if(var7.isAssignableFrom(var1)) {
               Map var8 = (Map)this.a.get(var7);
               if(var8 != null) {
                  var4 = (m)var8.get(var2);
                  var6 = var4;
                  if(var4 != null) {
                     break;
                  }
               }
            }
         }
      }

      return var4;
   }

   public l a(Class param1, Class param2) {
      // $FF: Couldn't be decompiled
   }

   public m a(Class param1, Class param2, m param3) {
      // $FF: Couldn't be decompiled
   }
}
