package com.bumptech.glide.load.b;

import android.net.Uri;
import android.text.TextUtils;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

public class d {
   private final URL a;
   private final e b;
   private final String c;
   private String d;
   private URL e;

   public d(String var1) {
      this(var1, e.b);
   }

   public d(String var1, e var2) {
      if(TextUtils.isEmpty(var1)) {
         throw new IllegalArgumentException("String url must not be empty or null: " + var1);
      } else if(var2 == null) {
         throw new IllegalArgumentException("Headers must not be null");
      } else {
         this.c = var1;
         this.a = null;
         this.b = var2;
      }
   }

   public d(URL var1) {
      this(var1, e.b);
   }

   public d(URL var1, e var2) {
      if(var1 == null) {
         throw new IllegalArgumentException("URL must not be null!");
      } else if(var2 == null) {
         throw new IllegalArgumentException("Headers must not be null");
      } else {
         this.a = var1;
         this.c = null;
         this.b = var2;
      }
   }

   private URL d() throws MalformedURLException {
      if(this.e == null) {
         this.e = new URL(this.e());
      }

      return this.e;
   }

   private String e() {
      if(TextUtils.isEmpty(this.d)) {
         String var2 = this.c;
         String var1 = var2;
         if(TextUtils.isEmpty(var2)) {
            var1 = this.a.toString();
         }

         this.d = Uri.encode(var1, "@#&=*+-_.,:!?()/~'%");
      }

      return this.d;
   }

   public URL a() throws MalformedURLException {
      return this.d();
   }

   public Map b() {
      return this.b.a();
   }

   public String c() {
      String var1;
      if(this.c != null) {
         var1 = this.c;
      } else {
         var1 = this.a.toString();
      }

      return var1;
   }

   public boolean equals(Object var1) {
      boolean var3 = false;
      boolean var2 = var3;
      if(var1 instanceof d) {
         d var4 = (d)var1;
         var2 = var3;
         if(this.c().equals(var4.c())) {
            var2 = var3;
            if(this.b.equals(var4.b)) {
               var2 = true;
            }
         }
      }

      return var2;
   }

   public int hashCode() {
      return this.c().hashCode() * 31 + this.b.hashCode();
   }

   public String toString() {
      return this.c() + '\n' + this.b.toString();
   }
}
