package com.bumptech.glide.load.b.b;

import android.content.Context;
import android.net.Uri;
import com.bumptech.glide.load.b.l;
import com.bumptech.glide.load.b.m;
import java.io.InputStream;

public class c extends com.bumptech.glide.load.b.b implements d {
   public c(l var1) {
      super(var1);
   }

   public static class a implements m {
      public l a(Context var1, com.bumptech.glide.load.b.c var2) {
         return new c(var2.a(Uri.class, InputStream.class));
      }

      public void a() {
      }
   }
}
