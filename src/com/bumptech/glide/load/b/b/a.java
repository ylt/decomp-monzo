package com.bumptech.glide.load.b.b;

import android.content.Context;
import com.bumptech.glide.load.b.k;
import com.bumptech.glide.load.b.l;
import com.bumptech.glide.load.b.m;

public class a implements d {
   private final k a;

   public a() {
      this((k)null);
   }

   public a(k var1) {
      this.a = var1;
   }

   public com.bumptech.glide.load.a.c a(com.bumptech.glide.load.b.d var1, int var2, int var3) {
      com.bumptech.glide.load.b.d var4 = var1;
      if(this.a != null) {
         var4 = (com.bumptech.glide.load.b.d)this.a.a(var1, 0, 0);
         if(var4 == null) {
            this.a.a(var1, 0, 0, var1);
            var4 = var1;
         }
      }

      return new com.bumptech.glide.load.a.f(var4);
   }

   public static class a implements m {
      private final k a = new k(500);

      public l a(Context var1, com.bumptech.glide.load.b.c var2) {
         return new a(this.a);
      }

      public void a() {
      }
   }
}
