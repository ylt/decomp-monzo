package com.bumptech.glide.load.b.b;

import android.content.Context;
import android.net.Uri;
import com.bumptech.glide.load.b.l;
import com.bumptech.glide.load.b.m;
import com.bumptech.glide.load.b.p;
import java.io.InputStream;

public class f extends p implements d {
   public f(l var1) {
      super(var1);
   }

   public static class a implements m {
      public l a(Context var1, com.bumptech.glide.load.b.c var2) {
         return new f(var2.a(Uri.class, InputStream.class));
      }

      public void a() {
      }
   }
}
