package com.bumptech.glide.load.b.b;

import android.content.Context;
import android.net.Uri;
import com.bumptech.glide.load.a.i;
import com.bumptech.glide.load.b.l;
import com.bumptech.glide.load.b.m;
import com.bumptech.glide.load.b.q;
import java.io.InputStream;

public class g extends q implements d {
   public g(Context var1, l var2) {
      super(var1, var2);
   }

   protected com.bumptech.glide.load.a.c a(Context var1, Uri var2) {
      return new i(var1, var2);
   }

   protected com.bumptech.glide.load.a.c a(Context var1, String var2) {
      return new com.bumptech.glide.load.a.h(var1.getApplicationContext().getAssets(), var2);
   }

   public static class a implements m {
      public l a(Context var1, com.bumptech.glide.load.b.c var2) {
         return new g(var1, var2.a(com.bumptech.glide.load.b.d.class, InputStream.class));
      }

      public void a() {
      }
   }
}
