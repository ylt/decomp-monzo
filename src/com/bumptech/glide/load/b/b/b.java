package com.bumptech.glide.load.b.b;

import android.content.Context;
import com.bumptech.glide.load.b.l;
import com.bumptech.glide.load.b.m;

public class b implements d {
   private final String a;

   public b() {
      this("");
   }

   @Deprecated
   public b(String var1) {
      this.a = var1;
   }

   public com.bumptech.glide.load.a.c a(byte[] var1, int var2, int var3) {
      return new com.bumptech.glide.load.a.b(var1, this.a);
   }

   public static class a implements m {
      public l a(Context var1, com.bumptech.glide.load.b.c var2) {
         return new b();
      }

      public void a() {
      }
   }
}
