package com.bumptech.glide.load.b.b;

import android.content.Context;
import android.net.Uri;
import com.bumptech.glide.load.b.l;
import com.bumptech.glide.load.b.m;
import com.bumptech.glide.load.b.n;
import java.io.InputStream;

public class e extends n implements d {
   public e(Context var1, l var2) {
      super(var1, var2);
   }

   public static class a implements m {
      public l a(Context var1, com.bumptech.glide.load.b.c var2) {
         return new e(var1, var2.a(Uri.class, InputStream.class));
      }

      public void a() {
      }
   }
}
