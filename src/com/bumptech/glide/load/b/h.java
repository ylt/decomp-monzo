package com.bumptech.glide.load.b;

import java.io.OutputStream;

public class h implements com.bumptech.glide.load.b {
   private final com.bumptech.glide.load.b a;
   private final com.bumptech.glide.load.b b;
   private String c;

   public h(com.bumptech.glide.load.b var1, com.bumptech.glide.load.b var2) {
      this.a = var1;
      this.b = var2;
   }

   public String a() {
      if(this.c == null) {
         this.c = this.a.a() + this.b.a();
      }

      return this.c;
   }

   public boolean a(g var1, OutputStream var2) {
      boolean var3;
      if(var1.a() != null) {
         var3 = this.a.a(var1.a(), var2);
      } else {
         var3 = this.b.a(var1.b(), var2);
      }

      return var3;
   }
}
