package com.bumptech.glide.load.b;

import android.net.Uri;

final class a {
   private static final int a = "file:///android_asset/".length();

   public static boolean a(Uri var0) {
      boolean var2 = false;
      boolean var1 = var2;
      if("file".equals(var0.getScheme())) {
         var1 = var2;
         if(!var0.getPathSegments().isEmpty()) {
            var1 = var2;
            if("android_asset".equals(var0.getPathSegments().get(0))) {
               var1 = true;
            }
         }
      }

      return var1;
   }

   public static String b(Uri var0) {
      return var0.toString().substring(a);
   }
}
