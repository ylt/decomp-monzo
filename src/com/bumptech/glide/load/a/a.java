package com.bumptech.glide.load.a;

import android.content.res.AssetManager;
import android.util.Log;
import java.io.IOException;

public abstract class a implements c {
   private final String a;
   private final AssetManager b;
   private Object c;

   public a(AssetManager var1, String var2) {
      this.b = var1;
      this.a = var2;
   }

   protected abstract Object a(AssetManager var1, String var2) throws IOException;

   public Object a(com.bumptech.glide.i var1) throws Exception {
      this.c = this.a(this.b, this.a);
      return this.c;
   }

   public void a() {
      if(this.c != null) {
         try {
            this.a(this.c);
         } catch (IOException var2) {
            if(Log.isLoggable("AssetUriFetcher", 2)) {
               Log.v("AssetUriFetcher", "Failed to close data", var2);
            }
         }
      }

   }

   protected abstract void a(Object var1) throws IOException;

   public String b() {
      return this.a;
   }

   public void c() {
   }
}
