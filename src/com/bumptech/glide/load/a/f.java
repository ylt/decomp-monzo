package com.bumptech.glide.load.a;

import android.text.TextUtils;
import android.util.Log;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class f implements c {
   private static final f.b a = new f.a();
   private final com.bumptech.glide.load.b.d b;
   private final f.b c;
   private HttpURLConnection d;
   private InputStream e;
   private volatile boolean f;

   public f(com.bumptech.glide.load.b.d var1) {
      this(var1, a);
   }

   f(com.bumptech.glide.load.b.d var1, f.b var2) {
      this.b = var1;
      this.c = var2;
   }

   private InputStream a(HttpURLConnection var1) throws IOException {
      if(TextUtils.isEmpty(var1.getContentEncoding())) {
         int var2 = var1.getContentLength();
         this.e = com.bumptech.glide.i.b.a(var1.getInputStream(), (long)var2);
      } else {
         if(Log.isLoggable("HttpUrlFetcher", 3)) {
            Log.d("HttpUrlFetcher", "Got non empty content encoding: " + var1.getContentEncoding());
         }

         this.e = var1.getInputStream();
      }

      return this.e;
   }

   private InputStream a(URL var1, int var2, URL var3, Map var4) throws IOException {
      if(var2 >= 5) {
         throw new IOException("Too many (> 5) redirects!");
      } else {
         if(var3 != null) {
            try {
               if(var1.toURI().equals(var3.toURI())) {
                  IOException var11 = new IOException("In re-direct loop");
                  throw var11;
               }
            } catch (URISyntaxException var7) {
               ;
            }
         }

         this.d = this.c.a(var1);
         Iterator var9 = var4.entrySet().iterator();

         while(var9.hasNext()) {
            Entry var6 = (Entry)var9.next();
            this.d.addRequestProperty((String)var6.getKey(), (String)var6.getValue());
         }

         this.d.setConnectTimeout(2500);
         this.d.setReadTimeout(2500);
         this.d.setUseCaches(false);
         this.d.setDoInput(true);
         this.d.connect();
         InputStream var8;
         if(this.f) {
            var8 = null;
         } else {
            int var5 = this.d.getResponseCode();
            if(var5 / 100 == 2) {
               var8 = this.a(this.d);
            } else {
               if(var5 / 100 != 3) {
                  if(var5 == -1) {
                     throw new IOException("Unable to retrieve response code from HttpUrlConnection.");
                  }

                  throw new IOException("Request failed " + var5 + ": " + this.d.getResponseMessage());
               }

               String var10 = this.d.getHeaderField("Location");
               if(TextUtils.isEmpty(var10)) {
                  throw new IOException("Received empty or null redirect url");
               }

               var8 = this.a(new URL(var1, var10), var2 + 1, var1, var4);
            }
         }

         return var8;
      }
   }

   // $FF: synthetic method
   public Object a(com.bumptech.glide.i var1) throws Exception {
      return this.b(var1);
   }

   public void a() {
      if(this.e != null) {
         try {
            this.e.close();
         } catch (IOException var2) {
            ;
         }
      }

      if(this.d != null) {
         this.d.disconnect();
      }

   }

   public InputStream b(com.bumptech.glide.i var1) throws Exception {
      return this.a(this.b.a(), 0, (URL)null, this.b.b());
   }

   public String b() {
      return this.b.c();
   }

   public void c() {
      this.f = true;
   }

   private static class a implements f.b {
      private a() {
      }

      // $FF: synthetic method
      a(Object var1) {
         this();
      }

      public HttpURLConnection a(URL var1) throws IOException {
         return (HttpURLConnection)var1.openConnection();
      }
   }

   interface b {
      HttpURLConnection a(URL var1) throws IOException;
   }
}
