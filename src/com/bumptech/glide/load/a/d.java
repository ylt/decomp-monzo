package com.bumptech.glide.load.a;

import android.content.res.AssetManager;
import android.os.ParcelFileDescriptor;
import java.io.IOException;

public class d extends a {
   public d(AssetManager var1, String var2) {
      super(var1, var2);
   }

   // $FF: synthetic method
   protected Object a(AssetManager var1, String var2) throws IOException {
      return this.b(var1, var2);
   }

   protected void a(ParcelFileDescriptor var1) throws IOException {
      var1.close();
   }

   protected ParcelFileDescriptor b(AssetManager var1, String var2) throws IOException {
      return var1.openFd(var2).getParcelFileDescriptor();
   }
}
