package com.bumptech.glide.load.a;

import android.annotation.TargetApi;
import android.content.ContentResolver;
import android.content.Context;
import android.content.UriMatcher;
import android.net.Uri;
import android.os.Build.VERSION;
import android.provider.ContactsContract.Contacts;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class i extends g {
   private static final UriMatcher a = new UriMatcher(-1);

   static {
      a.addURI("com.android.contacts", "contacts/lookup/*/#", 1);
      a.addURI("com.android.contacts", "contacts/lookup/*", 1);
      a.addURI("com.android.contacts", "contacts/#/photo", 2);
      a.addURI("com.android.contacts", "contacts/#", 3);
      a.addURI("com.android.contacts", "contacts/#/display_photo", 4);
   }

   public i(Context var1, Uri var2) {
      super(var1, var2);
   }

   @TargetApi(14)
   private InputStream a(ContentResolver var1, Uri var2) {
      InputStream var3;
      if(VERSION.SDK_INT < 14) {
         var3 = Contacts.openContactPhotoInputStream(var1, var2);
      } else {
         var3 = Contacts.openContactPhotoInputStream(var1, var2, true);
      }

      return var3;
   }

   private InputStream a(Uri var1, ContentResolver var2, int var3) throws FileNotFoundException {
      InputStream var5;
      switch(var3) {
      case 1:
      case 3:
         Uri var4 = var1;
         if(var3 == 1) {
            var1 = Contacts.lookupContact(var2, var1);
            var4 = var1;
            if(var1 == null) {
               throw new FileNotFoundException("Contact cannot be found");
            }
         }

         var5 = this.a(var2, var4);
         break;
      case 2:
      default:
         var5 = var2.openInputStream(var1);
      }

      return var5;
   }

   protected InputStream a(Uri var1, ContentResolver var2) throws FileNotFoundException {
      return this.a(var1, var2, a.match(var1));
   }

   protected void a(InputStream var1) throws IOException {
      var1.close();
   }

   // $FF: synthetic method
   protected Object b(Uri var1, ContentResolver var2) throws FileNotFoundException {
      return this.a(var1, var2);
   }
}
