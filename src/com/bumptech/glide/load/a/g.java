package com.bumptech.glide.load.a;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.util.Log;
import java.io.FileNotFoundException;
import java.io.IOException;

public abstract class g implements c {
   private final Uri a;
   private final Context b;
   private Object c;

   public g(Context var1, Uri var2) {
      this.b = var1.getApplicationContext();
      this.a = var2;
   }

   public final Object a(com.bumptech.glide.i var1) throws Exception {
      ContentResolver var2 = this.b.getContentResolver();
      this.c = this.b(this.a, var2);
      return this.c;
   }

   public void a() {
      if(this.c != null) {
         try {
            this.a(this.c);
         } catch (IOException var2) {
            if(Log.isLoggable("LocalUriFetcher", 2)) {
               Log.v("LocalUriFetcher", "failed to close data", var2);
            }
         }
      }

   }

   protected abstract void a(Object var1) throws IOException;

   protected abstract Object b(Uri var1, ContentResolver var2) throws FileNotFoundException;

   public String b() {
      return this.a.toString();
   }

   public void c() {
   }
}
