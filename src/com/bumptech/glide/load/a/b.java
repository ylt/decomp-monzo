package com.bumptech.glide.load.a;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

public class b implements c {
   private final byte[] a;
   private final String b;

   public b(byte[] var1, String var2) {
      this.a = var1;
      this.b = var2;
   }

   // $FF: synthetic method
   public Object a(com.bumptech.glide.i var1) throws Exception {
      return this.b(var1);
   }

   public void a() {
   }

   public InputStream b(com.bumptech.glide.i var1) {
      return new ByteArrayInputStream(this.a);
   }

   public String b() {
      return this.b;
   }

   public void c() {
   }
}
