package com.bumptech.glide.load;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;

public interface c {
   void a(MessageDigest var1) throws UnsupportedEncodingException;

   boolean equals(Object var1);

   int hashCode();
}
