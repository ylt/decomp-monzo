package com.bumptech.glide;

import android.content.Context;
import com.bumptech.glide.d.m;
import com.bumptech.glide.load.b.l;

public class f extends e {
   private final l g;
   private final Class h;
   private final Class i;
   private final j.c j;

   f(Context var1, g var2, Class var3, l var4, Class var5, Class var6, m var7, com.bumptech.glide.d.g var8, j.c var9) {
      super(var1, var3, a(var2, var4, var5, var6, com.bumptech.glide.load.resource.e.e.b()), var6, var2, var7, var8);
      this.g = var4;
      this.h = var5;
      this.i = var6;
      this.j = var9;
   }

   private static com.bumptech.glide.f.f a(g var0, l var1, Class var2, Class var3, com.bumptech.glide.load.resource.e.c var4) {
      return new com.bumptech.glide.f.e(var1, var4, var0.b(var2, var3));
   }
}
