package com.bumptech.glide.c;

class c {
   protected int a;
   protected byte[] b;
   protected int c;
   protected int d;
   protected int[][] e;
   protected int[] f = new int[256];
   protected int[] g = new int[256];
   protected int[] h = new int[256];
   protected int[] i = new int[32];

   public c(byte[] var1, int var2, int var3) {
      this.b = var1;
      this.c = var2;
      this.d = var3;
      this.e = new int[256][];

      for(var2 = 0; var2 < 256; ++var2) {
         this.e[var2] = new int[4];
         int[] var4 = this.e[var2];
         var3 = (var2 << 12) / 256;
         var4[2] = var3;
         var4[1] = var3;
         var4[0] = var3;
         this.h[var2] = 256;
         this.g[var2] = 0;
      }

   }

   public int a(int var1, int var2, int var3) {
      int var6 = this.f[var2];
      int var4 = -1;
      int var5 = 1000;
      int var7 = var6 - 1;

      while(true) {
         while(var6 < 256 || var7 >= 0) {
            int var8;
            int var9;
            int var10;
            int[] var11;
            if(var6 < 256) {
               var11 = this.e[var6];
               var8 = var11[1] - var2;
               if(var8 >= var5) {
                  short var12 = 256;
                  var6 = var4;
                  var4 = var12;
               } else {
                  label73: {
                     var9 = var6 + 1;
                     var6 = var8;
                     if(var8 < 0) {
                        var6 = -var8;
                     }

                     var10 = var11[0] - var1;
                     var8 = var10;
                     if(var10 < 0) {
                        var8 = -var10;
                     }

                     var10 = var8 + var6;
                     if(var10 < var5) {
                        var8 = var11[2] - var3;
                        var6 = var8;
                        if(var8 < 0) {
                           var6 = -var8;
                        }

                        var8 = var10 + var6;
                        if(var8 < var5) {
                           var6 = var11[3];
                           var5 = var8;
                           var4 = var9;
                           break label73;
                        }
                     }

                     var6 = var4;
                     var4 = var9;
                  }
               }
            } else {
               var8 = var6;
               var6 = var4;
               var4 = var8;
            }

            var8 = var7;
            if(var7 >= 0) {
               var11 = this.e[var7];
               var9 = var2 - var11[1];
               if(var9 >= var5) {
                  var7 = var6;
                  byte var13 = -1;
                  var6 = var4;
                  var4 = var7;
                  var7 = var13;
                  continue;
               }

               --var7;
               var8 = var9;
               if(var9 < 0) {
                  var8 = -var9;
               }

               var10 = var11[0] - var1;
               var9 = var10;
               if(var10 < 0) {
                  var9 = -var10;
               }

               var10 = var9 + var8;
               var8 = var7;
               if(var10 < var5) {
                  var9 = var11[2] - var3;
                  var8 = var9;
                  if(var9 < 0) {
                     var8 = -var9;
                  }

                  var9 = var8 + var10;
                  var8 = var7;
                  if(var9 < var5) {
                     var8 = var11[3];
                     var6 = var4;
                     var5 = var9;
                     var4 = var8;
                     continue;
                  }
               }
            }

            var7 = var4;
            var4 = var6;
            var6 = var7;
            var7 = var8;
         }

         return var4;
      }
   }

   protected void a(int var1, int var2, int var3, int var4, int var5) {
      int var8 = var2 - var1;
      if(var8 < -1) {
         var8 = -1;
      }

      var1 += var2;
      int var9 = var1;
      if(var1 > 256) {
         var9 = 256;
      }

      byte var6 = 1;
      int var7 = var2 - 1;
      var1 = var2 + 1;
      var2 = var6;

      while(true) {
         while(var1 < var9 || var7 > var8) {
            int[] var11 = this.i;
            int var14 = var2 + 1;
            int var10 = var11[var2];
            int[][] var15;
            if(var1 < var9) {
               label57: {
                  var15 = this.e;
                  var2 = var1 + 1;
                  var11 = var15[var1];

                  try {
                     var11[0] -= (var11[0] - var3) * var10 / 262144;
                     var11[1] -= (var11[1] - var4) * var10 / 262144;
                     var11[2] -= (var11[2] - var5) * var10 / 262144;
                  } catch (Exception var12) {
                     var1 = var2;
                     break label57;
                  }

                  var1 = var2;
               }
            }

            if(var7 > var8) {
               var15 = this.e;
               var2 = var7 - 1;
               var11 = var15[var7];

               try {
                  var11[0] -= (var11[0] - var3) * var10 / 262144;
                  var11[1] -= (var11[1] - var4) * var10 / 262144;
                  var11[2] -= var10 * (var11[2] - var5) / 262144;
               } catch (Exception var13) {
                  var7 = var2;
                  var2 = var14;
                  continue;
               }

               var7 = var2;
               var2 = var14;
            } else {
               var2 = var14;
            }
         }

         return;
      }
   }

   public byte[] a() {
      byte[] var7 = new byte[768];
      int[] var6 = new int[256];

      int var1;
      for(var1 = 0; var1 < 256; var6[this.e[var1][3]] = var1++) {
         ;
      }

      var1 = 0;

      for(int var2 = 0; var1 < 256; ++var1) {
         int var3 = var6[var1];
         int var4 = var2 + 1;
         var7[var2] = (byte)this.e[var3][0];
         int var5 = var4 + 1;
         var7[var4] = (byte)this.e[var3][1];
         var2 = var5 + 1;
         var7[var5] = (byte)this.e[var3][2];
      }

      return var7;
   }

   protected int b(int var1, int var2, int var3) {
      int var6 = Integer.MAX_VALUE;
      int var5 = -1;
      int var8 = Integer.MAX_VALUE;
      int var7 = -1;

      int var9;
      int[] var13;
      for(int var4 = 0; var4 < 256; var7 = var9) {
         var13 = this.e[var4];
         int var10 = var13[0] - var1;
         var9 = var10;
         if(var10 < 0) {
            var9 = -var10;
         }

         int var11 = var13[1] - var2;
         var10 = var11;
         if(var11 < 0) {
            var10 = -var11;
         }

         int var12 = var13[2] - var3;
         var11 = var12;
         if(var12 < 0) {
            var11 = -var12;
         }

         var10 = var10 + var9 + var11;
         if(var10 < var8) {
            var7 = var10;
            var8 = var4;
         } else {
            var9 = var8;
            var8 = var7;
            var7 = var9;
         }

         var9 = var10 - (this.g[var4] >> 12);
         if(var9 < var6) {
            var5 = var4;
            var6 = var9;
         }

         var9 = this.h[var4] >> 10;
         var13 = this.h;
         var13[var4] -= var9;
         var13 = this.g;
         var13[var4] += var9 << 10;
         ++var4;
         var9 = var8;
         var8 = var7;
      }

      var13 = this.h;
      var13[var7] += 64;
      var13 = this.g;
      var13[var7] -= 65536;
      return var5;
   }

   public void b() {
      int var3 = 0;
      int var2 = 0;

      int var1;
      for(var1 = 0; var1 < 256; ++var1) {
         int[] var8 = this.e[var1];
         int var4 = var8[1];
         int var5 = var1 + 1;

         int var6;
         int var7;
         int[] var9;
         for(var6 = var1; var5 < 256; var4 = var7) {
            var9 = this.e[var5];
            var7 = var4;
            if(var9[1] < var4) {
               var7 = var9[1];
               var6 = var5;
            }

            ++var5;
         }

         var9 = this.e[var6];
         if(var1 != var6) {
            var5 = var9[0];
            var9[0] = var8[0];
            var8[0] = var5;
            var5 = var9[1];
            var9[1] = var8[1];
            var8[1] = var5;
            var5 = var9[2];
            var9[2] = var8[2];
            var8[2] = var5;
            var5 = var9[3];
            var9[3] = var8[3];
            var8[3] = var5;
         }

         if(var4 != var2) {
            this.f[var2] = var3 + var1 >> 1;
            ++var2;

            while(var2 < var4) {
               this.f[var2] = var1;
               ++var2;
            }

            var2 = var4;
            var3 = var1;
         }
      }

      this.f[var2] = var3 + 255 >> 1;

      for(var1 = var2 + 1; var1 < 256; ++var1) {
         this.f[var1] = 255;
      }

   }

   protected void b(int var1, int var2, int var3, int var4, int var5) {
      int[] var6 = this.e[var2];
      var6[0] -= (var6[0] - var3) * var1 / 1024;
      var6[1] -= (var6[1] - var4) * var1 / 1024;
      var6[2] -= (var6[2] - var5) * var1 / 1024;
   }

   public void c() {
      if(this.c < 1509) {
         this.d = 1;
      }

      this.a = (this.d - 1) / 3 + 30;
      byte[] var14 = this.b;
      int var10 = this.c;
      int var11 = this.c / (this.d * 3);
      int var2 = var11 / 100;

      for(int var1 = 0; var1 < 32; ++var1) {
         this.i[var1] = (1024 - var1 * var1) * 256 / 1024 * 1024;
      }

      short var15;
      if(this.c < 1509) {
         var15 = 3;
      } else if(this.c % 499 != 0) {
         var15 = 1497;
      } else if(this.c % 491 != 0) {
         var15 = 1473;
      } else if(this.c % 487 != 0) {
         var15 = 1461;
      } else {
         var15 = 1509;
      }

      int var3 = 0;
      int var7 = 32;
      int var5 = 2048;
      int var4 = 0;
      int var6 = 1024;

      while(true) {
         while(var4 < var11) {
            int var8 = (var14[var3 + 0] & 255) << 4;
            int var9 = (var14[var3 + 1] & 255) << 4;
            int var13 = (var14[var3 + 2] & 255) << 4;
            int var12 = this.b(var8, var9, var13);
            this.b(var6, var12, var8, var9, var13);
            if(var7 != 0) {
               this.a(var7, var12, var8, var9, var13);
            }

            var3 += var15;
            if(var3 >= var10) {
               var3 -= this.c;
            }

            var8 = var4 + 1;
            if(var2 == 0) {
               var2 = 1;
            }

            if(var8 % var2 == 0) {
               var6 -= var6 / this.a;
               var9 = var5 - var5 / 30;
               var5 = var9 >> 6;
               var4 = var5;
               if(var5 <= 1) {
                  var4 = 0;
               }

               for(var5 = 0; var5 < var4; ++var5) {
                  this.i[var5] = (var4 * var4 - var5 * var5) * 256 / (var4 * var4) * var6;
               }

               var7 = var4;
               var5 = var9;
               var4 = var8;
            } else {
               var4 = var8;
            }
         }

         return;
      }
   }

   public byte[] d() {
      this.c();
      this.e();
      this.b();
      return this.a();
   }

   public void e() {
      for(int var1 = 0; var1 < 256; this.e[var1][3] = var1++) {
         int[] var2 = this.e[var1];
         var2[0] >>= 4;
         var2 = this.e[var1];
         var2[1] >>= 4;
         var2 = this.e[var1];
         var2[2] >>= 4;
      }

   }
}
