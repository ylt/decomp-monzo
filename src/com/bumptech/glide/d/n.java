package com.bumptech.glide.d;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.v4.app.Fragment;
import android.util.Log;
import java.util.HashSet;

public class n extends Fragment {
   private com.bumptech.glide.j a;
   private final a b;
   private final l c;
   private final HashSet d;
   private n e;

   public n() {
      this(new a());
   }

   @SuppressLint({"ValidFragment"})
   public n(a var1) {
      this.c = new n.a();
      this.d = new HashSet();
      this.b = var1;
   }

   private void a(n var1) {
      this.d.add(var1);
   }

   private void b(n var1) {
      this.d.remove(var1);
   }

   a a() {
      return this.b;
   }

   public void a(com.bumptech.glide.j var1) {
      this.a = var1;
   }

   public com.bumptech.glide.j b() {
      return this.a;
   }

   public l c() {
      return this.c;
   }

   public void onAttach(Activity var1) {
      super.onAttach(var1);

      try {
         this.e = k.a().a(this.getActivity().getSupportFragmentManager());
         if(this.e != this) {
            this.e.a(this);
         }
      } catch (IllegalStateException var2) {
         if(Log.isLoggable("SupportRMFragment", 5)) {
            Log.w("SupportRMFragment", "Unable to register fragment with root", var2);
         }
      }

   }

   public void onDestroy() {
      super.onDestroy();
      this.b.c();
   }

   public void onDetach() {
      super.onDetach();
      if(this.e != null) {
         this.e.b(this);
         this.e = null;
      }

   }

   public void onLowMemory() {
      super.onLowMemory();
      if(this.a != null) {
         this.a.a();
      }

   }

   public void onStart() {
      super.onStart();
      this.b.a();
   }

   public void onStop() {
      super.onStop();
      this.b.b();
   }

   private class a implements l {
      private a() {
      }

      // $FF: synthetic method
      a(Object var2) {
         this();
      }
   }
}
