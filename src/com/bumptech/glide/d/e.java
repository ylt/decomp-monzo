package com.bumptech.glide.d;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

class e implements c {
   private final Context a;
   private final c.a b;
   private boolean c;
   private boolean d;
   private final BroadcastReceiver e = new BroadcastReceiver() {
      public void onReceive(Context var1, Intent var2) {
         boolean var3 = e.this.c;
         e.this.c = e.this.a(var1);
         if(var3 != e.this.c) {
            e.this.b.a(e.this.c);
         }

      }
   };

   public e(Context var1, c.a var2) {
      this.a = var1.getApplicationContext();
      this.b = var2;
   }

   private void a() {
      if(!this.d) {
         this.c = this.a(this.a);
         this.a.registerReceiver(this.e, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
         this.d = true;
      }

   }

   private boolean a(Context var1) {
      NetworkInfo var3 = ((ConnectivityManager)var1.getSystemService("connectivity")).getActiveNetworkInfo();
      boolean var2;
      if(var3 != null && var3.isConnected()) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   private void b() {
      if(this.d) {
         this.a.unregisterReceiver(this.e);
         this.d = false;
      }

   }

   public void d() {
      this.a();
   }

   public void e() {
      this.b();
   }

   public void f() {
   }
}
