package com.bumptech.glide.d;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.WeakHashMap;

public class m {
   private final Set a = Collections.newSetFromMap(new WeakHashMap());
   private final List b = new ArrayList();
   private boolean c;

   public void a() {
      this.c = true;
      Iterator var1 = com.bumptech.glide.i.h.a((Collection)this.a).iterator();

      while(var1.hasNext()) {
         com.bumptech.glide.g.b var2 = (com.bumptech.glide.g.b)var1.next();
         if(var2.f()) {
            var2.e();
            this.b.add(var2);
         }
      }

   }

   public void a(com.bumptech.glide.g.b var1) {
      this.a.add(var1);
      if(!this.c) {
         var1.b();
      } else {
         this.b.add(var1);
      }

   }

   public void b() {
      this.c = false;
      Iterator var1 = com.bumptech.glide.i.h.a((Collection)this.a).iterator();

      while(var1.hasNext()) {
         com.bumptech.glide.g.b var2 = (com.bumptech.glide.g.b)var1.next();
         if(!var2.g() && !var2.i() && !var2.f()) {
            var2.b();
         }
      }

      this.b.clear();
   }

   public void b(com.bumptech.glide.g.b var1) {
      this.a.remove(var1);
      this.b.remove(var1);
   }

   public void c() {
      Iterator var1 = com.bumptech.glide.i.h.a((Collection)this.a).iterator();

      while(var1.hasNext()) {
         ((com.bumptech.glide.g.b)var1.next()).d();
      }

      this.b.clear();
   }

   public void d() {
      Iterator var2 = com.bumptech.glide.i.h.a((Collection)this.a).iterator();

      while(var2.hasNext()) {
         com.bumptech.glide.g.b var1 = (com.bumptech.glide.g.b)var2.next();
         if(!var1.g() && !var1.i()) {
            var1.e();
            if(!this.c) {
               var1.b();
            } else {
               this.b.add(var1);
            }
         }
      }

   }
}
