package com.bumptech.glide.d;

import android.content.Context;

public class d {
   public c a(Context var1, c.a var2) {
      boolean var3;
      if(var1.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE") == 0) {
         var3 = true;
      } else {
         var3 = false;
      }

      Object var4;
      if(var3) {
         var4 = new e(var1, var2);
      } else {
         var4 = new i();
      }

      return (c)var4;
   }
}
