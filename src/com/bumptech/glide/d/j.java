package com.bumptech.glide.d;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.util.Log;
import java.util.HashSet;

@TargetApi(11)
public class j extends Fragment {
   private final a a;
   private final l b;
   private com.bumptech.glide.j c;
   private final HashSet d;
   private j e;

   public j() {
      this(new a());
   }

   @SuppressLint({"ValidFragment"})
   j(a var1) {
      this.b = new j.a();
      this.d = new HashSet();
      this.a = var1;
   }

   private void a(j var1) {
      this.d.add(var1);
   }

   private void b(j var1) {
      this.d.remove(var1);
   }

   a a() {
      return this.a;
   }

   public void a(com.bumptech.glide.j var1) {
      this.c = var1;
   }

   public com.bumptech.glide.j b() {
      return this.c;
   }

   public l c() {
      return this.b;
   }

   public void onAttach(Activity var1) {
      super.onAttach(var1);

      try {
         this.e = k.a().a(this.getActivity().getFragmentManager());
         if(this.e != this) {
            this.e.a(this);
         }
      } catch (IllegalStateException var2) {
         if(Log.isLoggable("RMFragment", 5)) {
            Log.w("RMFragment", "Unable to register fragment with root", var2);
         }
      }

   }

   public void onDestroy() {
      super.onDestroy();
      this.a.c();
   }

   public void onDetach() {
      super.onDetach();
      if(this.e != null) {
         this.e.b(this);
         this.e = null;
      }

   }

   public void onLowMemory() {
      if(this.c != null) {
         this.c.a();
      }

   }

   public void onStart() {
      super.onStart();
      this.a.a();
   }

   public void onStop() {
      super.onStop();
      this.a.b();
   }

   public void onTrimMemory(int var1) {
      if(this.c != null) {
         this.c.a(var1);
      }

   }

   private class a implements l {
      private a() {
      }

      // $FF: synthetic method
      a(Object var2) {
         this();
      }
   }
}
