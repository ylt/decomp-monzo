package com.bumptech.glide.d;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.app.FragmentManager;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Build.VERSION;
import android.os.Handler.Callback;
import android.support.v4.app.Fragment;
import android.util.Log;
import java.util.HashMap;
import java.util.Map;

public class k implements Callback {
   private static final k c = new k();
   final Map a = new HashMap();
   final Map b = new HashMap();
   private volatile com.bumptech.glide.j d;
   private final Handler e = new Handler(Looper.getMainLooper(), this);

   public static k a() {
      return c;
   }

   private com.bumptech.glide.j b(Context param1) {
      // $FF: Couldn't be decompiled
   }

   @TargetApi(17)
   private static void b(Activity var0) {
      if(VERSION.SDK_INT >= 17 && var0.isDestroyed()) {
         throw new IllegalArgumentException("You cannot start a load for a destroyed activity");
      }
   }

   @TargetApi(17)
   j a(FragmentManager var1) {
      j var3 = (j)var1.findFragmentByTag("com.bumptech.glide.manager");
      j var2 = var3;
      if(var3 == null) {
         var3 = (j)this.a.get(var1);
         var2 = var3;
         if(var3 == null) {
            var2 = new j();
            this.a.put(var1, var2);
            var1.beginTransaction().add(var2, "com.bumptech.glide.manager").commitAllowingStateLoss();
            this.e.obtainMessage(1, var1).sendToTarget();
         }
      }

      return var2;
   }

   n a(android.support.v4.app.n var1) {
      n var3 = (n)var1.a("com.bumptech.glide.manager");
      n var2 = var3;
      if(var3 == null) {
         var3 = (n)this.b.get(var1);
         var2 = var3;
         if(var3 == null) {
            var2 = new n();
            this.b.put(var1, var2);
            var1.a().a(var2, "com.bumptech.glide.manager").d();
            this.e.obtainMessage(2, var1).sendToTarget();
         }
      }

      return var2;
   }

   @TargetApi(11)
   public com.bumptech.glide.j a(Activity var1) {
      com.bumptech.glide.j var2;
      if(!com.bumptech.glide.i.h.c() && VERSION.SDK_INT >= 11) {
         b(var1);
         var2 = this.a(var1, (FragmentManager)var1.getFragmentManager());
      } else {
         var2 = this.a(var1.getApplicationContext());
      }

      return var2;
   }

   public com.bumptech.glide.j a(Context var1) {
      if(var1 == null) {
         throw new IllegalArgumentException("You cannot start a load on a null Context");
      } else {
         com.bumptech.glide.j var2;
         if(com.bumptech.glide.i.h.b() && !(var1 instanceof Application)) {
            if(var1 instanceof android.support.v4.app.j) {
               var2 = this.a((android.support.v4.app.j)var1);
               return var2;
            }

            if(var1 instanceof Activity) {
               var2 = this.a((Activity)var1);
               return var2;
            }

            if(var1 instanceof ContextWrapper) {
               var2 = this.a(((ContextWrapper)var1).getBaseContext());
               return var2;
            }
         }

         var2 = this.b(var1);
         return var2;
      }
   }

   @TargetApi(11)
   com.bumptech.glide.j a(Context var1, FragmentManager var2) {
      j var4 = this.a(var2);
      com.bumptech.glide.j var3 = var4.b();
      com.bumptech.glide.j var5 = var3;
      if(var3 == null) {
         var5 = new com.bumptech.glide.j(var1, var4.a(), var4.c());
         var4.a(var5);
      }

      return var5;
   }

   com.bumptech.glide.j a(Context var1, android.support.v4.app.n var2) {
      n var4 = this.a(var2);
      com.bumptech.glide.j var3 = var4.b();
      com.bumptech.glide.j var5 = var3;
      if(var3 == null) {
         var5 = new com.bumptech.glide.j(var1, var4.a(), var4.c());
         var4.a(var5);
      }

      return var5;
   }

   public com.bumptech.glide.j a(Fragment var1) {
      if(var1.getActivity() == null) {
         throw new IllegalArgumentException("You cannot start a load on a fragment before it is attached");
      } else {
         com.bumptech.glide.j var3;
         if(com.bumptech.glide.i.h.c()) {
            var3 = this.a(var1.getActivity().getApplicationContext());
         } else {
            android.support.v4.app.n var2 = var1.getChildFragmentManager();
            var3 = this.a(var1.getActivity(), (android.support.v4.app.n)var2);
         }

         return var3;
      }
   }

   public com.bumptech.glide.j a(android.support.v4.app.j var1) {
      com.bumptech.glide.j var2;
      if(com.bumptech.glide.i.h.c()) {
         var2 = this.a(var1.getApplicationContext());
      } else {
         b((Activity)var1);
         var2 = this.a(var1, (android.support.v4.app.n)var1.getSupportFragmentManager());
      }

      return var2;
   }

   public boolean handleMessage(Message var1) {
      Object var4 = null;
      boolean var2 = true;
      Object var3;
      Object var5;
      switch(var1.what) {
      case 1:
         var5 = (FragmentManager)var1.obj;
         var3 = this.a.remove(var5);
         break;
      case 2:
         var5 = (android.support.v4.app.n)var1.obj;
         var3 = this.b.remove(var5);
         break;
      default:
         var2 = false;
         var3 = null;
         var5 = var4;
      }

      if(var2 && var3 == null && Log.isLoggable("RMRetriever", 5)) {
         Log.w("RMRetriever", "Failed to remove expected request manager fragment, manager: " + var5);
      }

      return var2;
   }
}
