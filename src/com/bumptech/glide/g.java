package com.bumptech.glide;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.ParcelFileDescriptor;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.widget.ImageView;
import com.bumptech.glide.d.k;
import com.bumptech.glide.load.b.l;
import com.bumptech.glide.load.resource.bitmap.m;
import com.bumptech.glide.load.resource.bitmap.n;
import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.util.Collections;
import java.util.List;

public class g {
   private static volatile g a;
   private static boolean b = true;
   private final com.bumptech.glide.load.b.c c;
   private final com.bumptech.glide.load.engine.c d;
   private final com.bumptech.glide.load.engine.a.c e;
   private final com.bumptech.glide.load.engine.b.h f;
   private final com.bumptech.glide.load.a g;
   private final com.bumptech.glide.g.b.f h = new com.bumptech.glide.g.b.f();
   private final com.bumptech.glide.load.resource.e.d i = new com.bumptech.glide.load.resource.e.d();
   private final com.bumptech.glide.f.c j;
   private final com.bumptech.glide.load.resource.bitmap.e k;
   private final com.bumptech.glide.load.resource.d.f l;
   private final com.bumptech.glide.load.resource.bitmap.i m;
   private final com.bumptech.glide.load.resource.d.f n;
   private final Handler o;
   private final com.bumptech.glide.load.engine.d.a p;

   g(com.bumptech.glide.load.engine.c var1, com.bumptech.glide.load.engine.b.h var2, com.bumptech.glide.load.engine.a.c var3, Context var4, com.bumptech.glide.load.a var5) {
      this.d = var1;
      this.e = var3;
      this.f = var2;
      this.g = var5;
      this.c = new com.bumptech.glide.load.b.c(var4);
      this.o = new Handler(Looper.getMainLooper());
      this.p = new com.bumptech.glide.load.engine.d.a(var2, var3, var5);
      this.j = new com.bumptech.glide.f.c();
      n var6 = new n(var3, var5);
      this.j.a(InputStream.class, Bitmap.class, var6);
      com.bumptech.glide.load.resource.bitmap.g var8 = new com.bumptech.glide.load.resource.bitmap.g(var3, var5);
      this.j.a(ParcelFileDescriptor.class, Bitmap.class, var8);
      m var9 = new m(var6, var8);
      this.j.a(com.bumptech.glide.load.b.g.class, Bitmap.class, var9);
      com.bumptech.glide.load.resource.c.c var7 = new com.bumptech.glide.load.resource.c.c(var4, var3);
      this.j.a(InputStream.class, com.bumptech.glide.load.resource.c.b.class, var7);
      this.j.a(com.bumptech.glide.load.b.g.class, com.bumptech.glide.load.resource.d.a.class, new com.bumptech.glide.load.resource.d.g(var9, var7, var3));
      this.j.a(InputStream.class, File.class, new com.bumptech.glide.load.resource.b.d());
      this.a(File.class, ParcelFileDescriptor.class, (com.bumptech.glide.load.b.m)(new com.bumptech.glide.load.b.a.a.a()));
      this.a(File.class, InputStream.class, (com.bumptech.glide.load.b.m)(new com.bumptech.glide.load.b.b.c.a()));
      this.a(Integer.TYPE, ParcelFileDescriptor.class, (com.bumptech.glide.load.b.m)(new com.bumptech.glide.load.b.a.c.a()));
      this.a(Integer.TYPE, InputStream.class, (com.bumptech.glide.load.b.m)(new com.bumptech.glide.load.b.b.e.a()));
      this.a(Integer.class, ParcelFileDescriptor.class, (com.bumptech.glide.load.b.m)(new com.bumptech.glide.load.b.a.c.a()));
      this.a(Integer.class, InputStream.class, (com.bumptech.glide.load.b.m)(new com.bumptech.glide.load.b.b.e.a()));
      this.a(String.class, ParcelFileDescriptor.class, (com.bumptech.glide.load.b.m)(new com.bumptech.glide.load.b.a.d.a()));
      this.a(String.class, InputStream.class, (com.bumptech.glide.load.b.m)(new com.bumptech.glide.load.b.b.f.a()));
      this.a(Uri.class, ParcelFileDescriptor.class, (com.bumptech.glide.load.b.m)(new com.bumptech.glide.load.b.a.e.a()));
      this.a(Uri.class, InputStream.class, (com.bumptech.glide.load.b.m)(new com.bumptech.glide.load.b.b.g.a()));
      this.a(URL.class, InputStream.class, (com.bumptech.glide.load.b.m)(new com.bumptech.glide.load.b.b.h.a()));
      this.a(com.bumptech.glide.load.b.d.class, InputStream.class, (com.bumptech.glide.load.b.m)(new com.bumptech.glide.load.b.b.a.a()));
      this.a(byte[].class, InputStream.class, (com.bumptech.glide.load.b.m)(new com.bumptech.glide.load.b.b.b.a()));
      this.i.a(Bitmap.class, com.bumptech.glide.load.resource.bitmap.j.class, new com.bumptech.glide.load.resource.e.b(var4.getResources(), var3));
      this.i.a(com.bumptech.glide.load.resource.d.a.class, com.bumptech.glide.load.resource.a.b.class, new com.bumptech.glide.load.resource.e.a(new com.bumptech.glide.load.resource.e.b(var4.getResources(), var3)));
      this.k = new com.bumptech.glide.load.resource.bitmap.e(var3);
      this.l = new com.bumptech.glide.load.resource.d.f(var3, this.k);
      this.m = new com.bumptech.glide.load.resource.bitmap.i(var3);
      this.n = new com.bumptech.glide.load.resource.d.f(var3, this.m);
   }

   public static g a(Context param0) {
      // $FF: Couldn't be decompiled
   }

   public static j a(Fragment var0) {
      return k.a().a(var0);
   }

   public static j a(android.support.v4.app.j var0) {
      return k.a().a(var0);
   }

   public static l a(Class var0, Context var1) {
      return a(var0, InputStream.class, var1);
   }

   public static l a(Class var0, Class var1, Context var2) {
      l var3;
      if(var0 == null) {
         if(Log.isLoggable("Glide", 3)) {
            Log.d("Glide", "Unable to load null model, setting placeholder only");
         }

         var3 = null;
      } else {
         var3 = a(var2).i().a(var0, var1);
      }

      return var3;
   }

   public static void a(com.bumptech.glide.g.b.j var0) {
      com.bumptech.glide.i.h.a();
      com.bumptech.glide.g.b var1 = var0.c();
      if(var1 != null) {
         var1.d();
         var0.a((com.bumptech.glide.g.b)null);
      }

   }

   public static j b(Context var0) {
      return k.a().a(var0);
   }

   public static l b(Class var0, Context var1) {
      return a(var0, ParcelFileDescriptor.class, var1);
   }

   private static List c(Context var0) {
      List var1;
      if(b) {
         var1 = (new com.bumptech.glide.e.b(var0)).a();
      } else {
         var1 = Collections.emptyList();
      }

      return var1;
   }

   private com.bumptech.glide.load.b.c i() {
      return this.c;
   }

   com.bumptech.glide.g.b.j a(ImageView var1, Class var2) {
      return this.h.a(var1, var2);
   }

   public com.bumptech.glide.load.engine.a.c a() {
      return this.e;
   }

   com.bumptech.glide.load.resource.e.c a(Class var1, Class var2) {
      return this.i.a(var1, var2);
   }

   public void a(int var1) {
      com.bumptech.glide.i.h.a();
      this.f.a(var1);
      this.e.a(var1);
   }

   public void a(Class var1, Class var2, com.bumptech.glide.load.b.m var3) {
      com.bumptech.glide.load.b.m var4 = this.c.a(var1, var2, var3);
      if(var4 != null) {
         var4.a();
      }

   }

   com.bumptech.glide.f.b b(Class var1, Class var2) {
      return this.j.a(var1, var2);
   }

   com.bumptech.glide.load.engine.c b() {
      return this.d;
   }

   com.bumptech.glide.load.resource.bitmap.e c() {
      return this.k;
   }

   com.bumptech.glide.load.resource.bitmap.i d() {
      return this.m;
   }

   com.bumptech.glide.load.resource.d.f e() {
      return this.l;
   }

   com.bumptech.glide.load.resource.d.f f() {
      return this.n;
   }

   com.bumptech.glide.load.a g() {
      return this.g;
   }

   public void h() {
      com.bumptech.glide.i.h.a();
      this.f.a();
      this.e.a();
   }
}
