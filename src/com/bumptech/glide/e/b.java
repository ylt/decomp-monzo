package com.bumptech.glide.e;

import android.content.Context;
import java.util.List;

public final class b {
   private final Context a;

   public b(Context var1) {
      this.a = var1;
   }

   private static a a(String var0) {
      Class var5;
      try {
         var5 = Class.forName(var0);
      } catch (ClassNotFoundException var4) {
         throw new IllegalArgumentException("Unable to find GlideModule implementation", var4);
      }

      Object var1;
      try {
         var1 = var5.newInstance();
      } catch (InstantiationException var2) {
         throw new RuntimeException("Unable to instantiate GlideModule implementation for " + var5, var2);
      } catch (IllegalAccessException var3) {
         throw new RuntimeException("Unable to instantiate GlideModule implementation for " + var5, var3);
      }

      if(!(var1 instanceof a)) {
         throw new RuntimeException("Expected instanceof GlideModule, but found: " + var1);
      } else {
         return (a)var1;
      }
   }

   public List a() {
      // $FF: Couldn't be decompiled
   }
}
