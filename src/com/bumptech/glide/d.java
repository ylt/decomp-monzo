package com.bumptech.glide;

import android.content.Context;
import com.bumptech.glide.d.m;
import com.bumptech.glide.load.b.l;

public class d extends c {
   private final l g;
   private final l h;
   private final j.c i;

   d(Class var1, l var2, l var3, Context var4, g var5, m var6, com.bumptech.glide.d.g var7, j.c var8) {
      super(var4, var1, a(var5, var2, var3, com.bumptech.glide.load.resource.d.a.class, com.bumptech.glide.load.resource.a.b.class, (com.bumptech.glide.load.resource.e.c)null), var5, var6, var7);
      this.g = var2;
      this.h = var3;
      this.i = var8;
   }

   private static com.bumptech.glide.f.e a(g var0, l var1, l var2, Class var3, Class var4, com.bumptech.glide.load.resource.e.c var5) {
      com.bumptech.glide.f.e var8;
      if(var1 == null && var2 == null) {
         var8 = null;
      } else {
         com.bumptech.glide.load.resource.e.c var6 = var5;
         if(var5 == null) {
            var6 = var0.a(var3, var4);
         }

         com.bumptech.glide.f.b var7 = var0.b(com.bumptech.glide.load.b.g.class, var3);
         var8 = new com.bumptech.glide.f.e(new com.bumptech.glide.load.b.f(var1, var2), var6, var7);
      }

      return var8;
   }

   public b h() {
      return (b)this.i.a(new b(this, this.g, this.h, this.i));
   }
}
