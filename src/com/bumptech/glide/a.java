package com.bumptech.glide;

import android.widget.ImageView;
import com.bumptech.glide.load.resource.bitmap.o;

public class a extends e {
   private final com.bumptech.glide.load.engine.a.c g;
   private com.bumptech.glide.load.resource.bitmap.f h;
   private com.bumptech.glide.load.a i;
   private com.bumptech.glide.load.e j;
   private com.bumptech.glide.load.e k;

   a(com.bumptech.glide.f.f var1, Class var2, e var3) {
      super(var1, var2, var3);
      this.h = com.bumptech.glide.load.resource.bitmap.f.a;
      this.g = var3.c.a();
      this.i = var3.c.g();
      this.j = new o(this.g, this.i);
      this.k = new com.bumptech.glide.load.resource.bitmap.h(this.g, this.i);
   }

   public a a() {
      return this.a(new com.bumptech.glide.load.resource.bitmap.d[]{this.c.c()});
   }

   public a a(float var1) {
      super.b(var1);
      return this;
   }

   public a a(int var1) {
      super.b(var1);
      return this;
   }

   public a a(int var1, int var2) {
      super.b(var1, var2);
      return this;
   }

   public a a(com.bumptech.glide.g.a.d var1) {
      super.b(var1);
      return this;
   }

   public a a(com.bumptech.glide.g.d var1) {
      super.b(var1);
      return this;
   }

   public a a(com.bumptech.glide.load.b var1) {
      super.b(var1);
      return this;
   }

   public a a(com.bumptech.glide.load.c var1) {
      super.b(var1);
      return this;
   }

   public a a(com.bumptech.glide.load.e var1) {
      super.b(var1);
      return this;
   }

   public a a(com.bumptech.glide.load.engine.b var1) {
      super.b(var1);
      return this;
   }

   public a a(Object var1) {
      super.b(var1);
      return this;
   }

   public a a(boolean var1) {
      super.b(var1);
      return this;
   }

   public a a(com.bumptech.glide.load.g... var1) {
      super.b(var1);
      return this;
   }

   public a a(com.bumptech.glide.load.resource.bitmap.d... var1) {
      super.b((com.bumptech.glide.load.g[])var1);
      return this;
   }

   public com.bumptech.glide.g.b.j a(ImageView var1) {
      return super.a(var1);
   }

   public a b() {
      return this.a(new com.bumptech.glide.load.resource.bitmap.d[]{this.c.d()});
   }

   // $FF: synthetic method
   public e b(float var1) {
      return this.a(var1);
   }

   // $FF: synthetic method
   public e b(int var1) {
      return this.a(var1);
   }

   // $FF: synthetic method
   public e b(int var1, int var2) {
      return this.a(var1, var2);
   }

   // $FF: synthetic method
   public e b(com.bumptech.glide.g.a.d var1) {
      return this.a(var1);
   }

   // $FF: synthetic method
   public e b(com.bumptech.glide.g.d var1) {
      return this.a(var1);
   }

   // $FF: synthetic method
   public e b(com.bumptech.glide.load.b var1) {
      return this.a(var1);
   }

   // $FF: synthetic method
   public e b(com.bumptech.glide.load.c var1) {
      return this.a(var1);
   }

   // $FF: synthetic method
   public e b(com.bumptech.glide.load.e var1) {
      return this.a(var1);
   }

   // $FF: synthetic method
   public e b(com.bumptech.glide.load.engine.b var1) {
      return this.a(var1);
   }

   // $FF: synthetic method
   public e b(Object var1) {
      return this.a(var1);
   }

   // $FF: synthetic method
   public e b(boolean var1) {
      return this.a(var1);
   }

   // $FF: synthetic method
   public e b(com.bumptech.glide.load.g[] var1) {
      return this.a(var1);
   }

   public a c() {
      return (a)super.f();
   }

   // $FF: synthetic method
   public Object clone() throws CloneNotSupportedException {
      return this.c();
   }

   void d() {
      this.b();
   }

   void e() {
      this.a();
   }

   // $FF: synthetic method
   public e f() {
      return this.c();
   }
}
