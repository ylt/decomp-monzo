package com.bumptech.glide.b;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.os.Build.VERSION;
import android.util.Log;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.Iterator;

public class a {
   private static final String a = a.class.getSimpleName();
   private static final Config b;
   private int[] c;
   private final int[] d = new int[256];
   private ByteBuffer e;
   private final byte[] f = new byte[256];
   private short[] g;
   private byte[] h;
   private byte[] i;
   private byte[] j;
   private int[] k;
   private int l;
   private byte[] m;
   private c n;
   private a.a o;
   private Bitmap p;
   private boolean q;
   private int r;

   static {
      b = Config.ARGB_8888;
   }

   public a(a.a var1) {
      this.o = var1;
      this.n = new c();
   }

   private Bitmap a(b var1, b var2) {
      int var10 = this.n.f;
      int var11 = this.n.g;
      int[] var13 = this.k;
      if(var2 == null) {
         Arrays.fill(var13, 0);
      }

      int var3;
      int var5;
      int var6;
      int var7;
      int var8;
      if(var2 != null && var2.g > 0) {
         if(var2.g == 2) {
            var3 = 0;
            int var4;
            if(!var1.f) {
               var4 = this.n.l;
               var3 = var4;
               if(var1.k != null) {
                  var3 = var4;
                  if(this.n.j == var1.h) {
                     var3 = 0;
                  }
               }
            }

            var4 = var2.b;
            var6 = var2.a + var4 * var10;
            var7 = var2.d;

            for(var4 = var6; var4 < var6 + var7 * var10; var4 += var10) {
               var8 = var2.c;

               for(var5 = var4; var5 < var4 + var8; ++var5) {
                  var13[var5] = var3;
               }
            }
         } else if(var2.g == 3 && this.p != null) {
            this.p.getPixels(var13, 0, var10, 0, 0, var10, var11);
         }
      }

      this.a(var1);
      var7 = 1;
      byte var16 = 8;
      var8 = 0;

      byte var15;
      for(var5 = 0; var5 < var1.d; var16 = var15) {
         int var9;
         if(var1.e) {
            var3 = var8;
            var15 = var16;
            var9 = var7;
            if(var8 >= var1.d) {
               var9 = var7 + 1;
               switch(var9) {
               case 2:
                  var3 = 4;
                  var15 = var16;
                  break;
               case 3:
                  var3 = 2;
                  var15 = 4;
                  break;
               case 4:
                  var3 = 1;
                  var15 = 2;
                  break;
               default:
                  var15 = var16;
                  var3 = var8;
               }
            }

            var8 = var3 + var15;
            var7 = var9;
         } else {
            var3 = var5;
            var15 = var16;
         }

         var3 += var1.b;
         if(var3 < this.n.g) {
            int var12 = this.n.f * var3;
            var6 = var12 + var1.a;
            var9 = var1.c + var6;
            var3 = var9;
            if(this.n.f + var12 < var9) {
               var3 = this.n.f + var12;
            }

            for(var9 = var1.c * var5; var6 < var3; ++var9) {
               byte var17 = this.j[var9];
               var12 = this.c[var17 & 255];
               if(var12 != 0) {
                  var13[var6] = var12;
               }

               ++var6;
            }
         }

         ++var5;
      }

      if(this.q && (var1.g == 0 || var1.g == 1)) {
         if(this.p == null) {
            this.p = this.j();
         }

         this.p.setPixels(var13, 0, var10, 0, 0, var10, var11);
      }

      Bitmap var14 = this.j();
      var14.setPixels(var13, 0, var10, 0, 0, var10, var11);
      return var14;
   }

   @TargetApi(12)
   private static void a(Bitmap var0) {
      if(VERSION.SDK_INT >= 12) {
         var0.setHasAlpha(true);
      }

   }

   private void a(b var1) {
      if(var1 != null) {
         this.e.position(var1.j);
      }

      int var15;
      if(var1 == null) {
         var15 = this.n.f * this.n.g;
      } else {
         var15 = var1.c * var1.d;
      }

      if(this.j == null || this.j.length < var15) {
         this.j = new byte[var15];
      }

      if(this.g == null) {
         this.g = new short[4096];
      }

      if(this.h == null) {
         this.h = new byte[4096];
      }

      if(this.i == null) {
         this.i = new byte[4097];
      }

      int var20 = this.h();
      int var19 = 1 << var20;
      int var16 = -1;
      int var3 = var20 + 1;

      int var2;
      for(var2 = 0; var2 < var19; ++var2) {
         this.g[var2] = 0;
         this.h[var2] = (byte)var2;
      }

      int var11 = 0;
      int var14 = 0;
      int var9 = 0;
      int var5 = 0;
      int var10 = 0;
      int var7 = var3;
      int var8 = (1 << var3) - 1;
      int var6 = var19 + 2;
      var3 = 0;
      int var4 = 0;
      var2 = 0;

      label107:
      while(var14 < var15) {
         int var12 = var3;
         var3 = var4;
         if(var4 == 0) {
            var3 = this.i();
            if(var3 <= 0) {
               this.r = 3;
               break;
            }

            var12 = 0;
         }

         byte var18 = this.f[var12];
         int var13 = var12 + 1;
         var12 = var3 - 1;
         var4 = var7;
         int var17 = var8;
         var8 = var11 + ((var18 & 255) << var10);
         var3 = var6;
         var11 = var10 + 8;
         var6 = var16;
         var7 = var2;
         var2 = var17;

         while(true) {
            while(var11 >= var4) {
               var10 = var8 & var2;
               var8 >>= var4;
               var11 -= var4;
               if(var10 != var19) {
                  if(var10 > var3) {
                     this.r = 3;
                     var10 = var11;
                     var16 = var4;
                     var17 = var3;
                     var3 = var13;
                     var11 = var8;
                     var8 = var2;
                     var4 = var12;
                     var2 = var7;
                     var7 = var16;
                     var16 = var6;
                     var6 = var17;
                     continue label107;
                  }

                  if(var10 == var19 + 1) {
                     var10 = var11;
                     var16 = var4;
                     var17 = var3;
                     var3 = var13;
                     var11 = var8;
                     var8 = var2;
                     var4 = var12;
                     var2 = var7;
                     var7 = var16;
                     var16 = var6;
                     var6 = var17;
                     continue label107;
                  }

                  if(var6 == -1) {
                     this.i[var9] = this.h[var10];
                     ++var9;
                     var5 = var10;
                     var6 = var10;
                  } else {
                     if(var10 >= var3) {
                        this.i[var9] = (byte)var5;
                        ++var9;
                        var5 = var6;
                     } else {
                        var5 = var10;
                     }

                     while(var5 >= var19) {
                        this.i[var9] = this.h[var5];
                        var5 = this.g[var5];
                        ++var9;
                     }

                     var17 = this.h[var5] & 255;
                     byte[] var21 = this.i;
                     int var22 = var9 + 1;
                     var21[var9] = (byte)var17;
                     var16 = var4;
                     var9 = var2;
                     var5 = var3;
                     if(var3 < 4096) {
                        this.g[var3] = (short)var6;
                        this.h[var3] = (byte)var17;
                        ++var3;
                        var16 = var4;
                        var9 = var2;
                        var5 = var3;
                        if((var3 & var2) == 0) {
                           var16 = var4;
                           var9 = var2;
                           var5 = var3;
                           if(var3 < 4096) {
                              var16 = var4 + 1;
                              var9 = var2 + var3;
                              var5 = var3;
                           }
                        }
                     }

                     var3 = var14;

                     for(var2 = var22; var2 > 0; ++var7) {
                        --var2;
                        this.j[var7] = this.i[var2];
                        ++var3;
                     }

                     var14 = var3;
                     var6 = var10;
                     var10 = var2;
                     var4 = var16;
                     var2 = var9;
                     var3 = var5;
                     var9 = var10;
                     var5 = var17;
                  }
               } else {
                  var4 = var20 + 1;
                  var2 = (1 << var4) - 1;
                  var3 = var19 + 2;
                  var6 = -1;
               }
            }

            var17 = var2;
            var2 = var12;
            var16 = var4;
            var12 = var3;
            var10 = var11;
            var3 = var13;
            var4 = var2;
            var2 = var7;
            var11 = var8;
            var7 = var16;
            var16 = var6;
            var8 = var17;
            var6 = var12;
            break;
         }
      }

      while(var2 < var15) {
         this.j[var2] = 0;
         ++var2;
      }

   }

   private int h() {
      int var1 = 0;

      byte var2;
      try {
         var2 = this.e.get();
      } catch (Exception var4) {
         this.r = 1;
         return var1;
      }

      var1 = var2 & 255;
      return var1;
   }

   private int i() {
      int var3 = this.h();
      int var2 = 0;
      int var1 = 0;
      if(var3 > 0) {
         while(true) {
            var2 = var1;
            if(var1 >= var3) {
               break;
            }

            var2 = var3 - var1;

            try {
               this.e.get(this.f, var1, var2);
            } catch (Exception var5) {
               Log.w(a, "Error Reading Block", var5);
               this.r = 1;
               var2 = var1;
               break;
            }

            var1 += var2;
         }
      }

      return var2;
   }

   private Bitmap j() {
      Bitmap var2 = this.o.a(this.n.f, this.n.g, b);
      Bitmap var1 = var2;
      if(var2 == null) {
         var1 = Bitmap.createBitmap(this.n.f, this.n.g, b);
      }

      a(var1);
      return var1;
   }

   public int a(int var1) {
      byte var3 = -1;
      int var2 = var3;
      if(var1 >= 0) {
         var2 = var3;
         if(var1 < this.n.c) {
            var2 = ((b)this.n.e.get(var1)).i;
         }
      }

      return var2;
   }

   public void a() {
      this.l = (this.l + 1) % this.n.c;
   }

   public void a(c var1, byte[] var2) {
      this.n = var1;
      this.m = var2;
      this.r = 0;
      this.l = -1;
      this.e = ByteBuffer.wrap(var2);
      this.e.rewind();
      this.e.order(ByteOrder.LITTLE_ENDIAN);
      this.q = false;
      Iterator var3 = var1.e.iterator();

      while(var3.hasNext()) {
         if(((b)var3.next()).g == 3) {
            this.q = true;
            break;
         }
      }

      this.j = new byte[var1.f * var1.g];
      this.k = new int[var1.f * var1.g];
   }

   public int b() {
      int var1;
      if(this.n.c > 0 && this.l >= 0) {
         var1 = this.a(this.l);
      } else {
         var1 = -1;
      }

      return var1;
   }

   public int c() {
      return this.n.c;
   }

   public int d() {
      return this.l;
   }

   public int e() {
      int var1;
      if(this.n.m == -1) {
         var1 = 1;
      } else if(this.n.m == 0) {
         var1 = 0;
      } else {
         var1 = this.n.m + 1;
      }

      return var1;
   }

   public Bitmap f() {
      // $FF: Couldn't be decompiled
   }

   public void g() {
      this.n = null;
      this.m = null;
      this.j = null;
      this.k = null;
      if(this.p != null) {
         this.o.a(this.p);
      }

      this.p = null;
      this.e = null;
   }

   public interface a {
      Bitmap a(int var1, int var2, Config var3);

      void a(Bitmap var1);
   }
}
