package com.bumptech.glide;

import android.content.Context;
import android.widget.ImageView;
import com.bumptech.glide.d.m;

public class c extends e {
   c(Context var1, Class var2, com.bumptech.glide.f.f var3, g var4, m var5, com.bumptech.glide.d.g var6) {
      super(var1, var2, var3, com.bumptech.glide.load.resource.a.b.class, var4, var5, var6);
      this.c();
   }

   public c a() {
      return this.a(new com.bumptech.glide.load.g[]{this.c.e()});
   }

   public c a(float var1) {
      super.b(var1);
      return this;
   }

   public c a(int var1) {
      super.b(var1);
      return this;
   }

   public c a(int var1, int var2) {
      super.b(var1, var2);
      return this;
   }

   public c a(com.bumptech.glide.g.a.d var1) {
      super.b(var1);
      return this;
   }

   public c a(com.bumptech.glide.g.d var1) {
      super.b(var1);
      return this;
   }

   public c a(com.bumptech.glide.load.b var1) {
      super.b(var1);
      return this;
   }

   public c a(com.bumptech.glide.load.c var1) {
      super.b(var1);
      return this;
   }

   public c a(com.bumptech.glide.load.e var1) {
      super.b(var1);
      return this;
   }

   public c a(com.bumptech.glide.load.engine.b var1) {
      super.b(var1);
      return this;
   }

   public c a(Object var1) {
      super.b(var1);
      return this;
   }

   public c a(boolean var1) {
      super.b(var1);
      return this;
   }

   public c a(com.bumptech.glide.load.g... var1) {
      super.b(var1);
      return this;
   }

   public com.bumptech.glide.g.b.j a(ImageView var1) {
      return super.a(var1);
   }

   public c b() {
      return this.a(new com.bumptech.glide.load.g[]{this.c.f()});
   }

   // $FF: synthetic method
   public e b(float var1) {
      return this.a(var1);
   }

   // $FF: synthetic method
   public e b(int var1) {
      return this.a(var1);
   }

   // $FF: synthetic method
   public e b(int var1, int var2) {
      return this.a(var1, var2);
   }

   // $FF: synthetic method
   public e b(com.bumptech.glide.g.a.d var1) {
      return this.a(var1);
   }

   // $FF: synthetic method
   public e b(com.bumptech.glide.g.d var1) {
      return this.a(var1);
   }

   // $FF: synthetic method
   public e b(com.bumptech.glide.load.b var1) {
      return this.a(var1);
   }

   // $FF: synthetic method
   public e b(com.bumptech.glide.load.c var1) {
      return this.a(var1);
   }

   // $FF: synthetic method
   public e b(com.bumptech.glide.load.e var1) {
      return this.a(var1);
   }

   // $FF: synthetic method
   public e b(com.bumptech.glide.load.engine.b var1) {
      return this.a(var1);
   }

   // $FF: synthetic method
   public e b(Object var1) {
      return this.a(var1);
   }

   // $FF: synthetic method
   public e b(boolean var1) {
      return this.a(var1);
   }

   // $FF: synthetic method
   public e b(com.bumptech.glide.load.g[] var1) {
      return this.a(var1);
   }

   public final c c() {
      super.b((com.bumptech.glide.g.a.d)(new com.bumptech.glide.g.a.a()));
      return this;
   }

   // $FF: synthetic method
   public Object clone() throws CloneNotSupportedException {
      return this.g();
   }

   void d() {
      this.b();
   }

   void e() {
      this.a();
   }

   // $FF: synthetic method
   public e f() {
      return this.g();
   }

   public c g() {
      return (c)super.f();
   }
}
