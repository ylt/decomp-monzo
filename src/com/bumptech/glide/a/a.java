package com.bumptech.glide.a;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.concurrent.Callable;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public final class a implements Closeable {
   final ThreadPoolExecutor a;
   private final File b;
   private final File c;
   private final File d;
   private final File e;
   private final int f;
   private long g;
   private final int h;
   private long i = 0L;
   private Writer j;
   private final LinkedHashMap k = new LinkedHashMap(0, 0.75F, true);
   private int l;
   private long m = 0L;
   private final Callable n;

   private a(File var1, int var2, int var3, long var4) {
      this.a = new ThreadPoolExecutor(0, 1, 60L, TimeUnit.SECONDS, new LinkedBlockingQueue());
      this.n = new Callable() {
         public Void a() throws Exception {
            // $FF: Couldn't be decompiled
         }

         // $FF: synthetic method
         public Object call() throws Exception {
            return this.a();
         }
      };
      this.b = var1;
      this.f = var2;
      this.c = new File(var1, "journal");
      this.d = new File(var1, "journal.tmp");
      this.e = new File(var1, "journal.bkp");
      this.h = var3;
      this.g = var4;
   }

   // $FF: synthetic method
   static int a(a var0, int var1) {
      var0.l = var1;
      return var1;
   }

   private a.a a(String param1, long param2) throws IOException {
      // $FF: Couldn't be decompiled
   }

   public static a a(File var0, int var1, int var2, long var3) throws IOException {
      if(var3 <= 0L) {
         throw new IllegalArgumentException("maxSize <= 0");
      } else if(var2 <= 0) {
         throw new IllegalArgumentException("valueCount <= 0");
      } else {
         File var6 = new File(var0, "journal.bkp");
         if(var6.exists()) {
            File var5 = new File(var0, "journal");
            if(var5.exists()) {
               var6.delete();
            } else {
               a(var6, var5, false);
            }
         }

         a var9 = new a(var0, var1, var2, var3);
         a var8;
         if(var9.c.exists()) {
            label41: {
               try {
                  var9.b();
                  var9.c();
               } catch (IOException var7) {
                  System.out.println("DiskLruCache " + var0 + " is corrupt: " + var7.getMessage() + ", removing");
                  var9.a();
                  break label41;
               }

               var8 = var9;
               return var8;
            }
         }

         var0.mkdirs();
         var8 = new a(var0, var1, var2, var3);
         var8.d();
         return var8;
      }
   }

   // $FF: synthetic method
   static Writer a(a var0) {
      return var0.j;
   }

   private void a(a.a param1, boolean param2) throws IOException {
      // $FF: Couldn't be decompiled
   }

   private static void a(File var0) throws IOException {
      if(var0.exists() && !var0.delete()) {
         throw new IOException();
      }
   }

   private static void a(File var0, File var1, boolean var2) throws IOException {
      if(var2) {
         a(var1);
      }

      if(!var0.renameTo(var1)) {
         throw new IOException();
      }
   }

   private void b() throws IOException {
      // $FF: Couldn't be decompiled
   }

   // $FF: synthetic method
   static void b(a var0) throws IOException {
      var0.g();
   }

   private void c() throws IOException {
      a(this.d);
      Iterator var2 = this.k.values().iterator();

      while(true) {
         while(var2.hasNext()) {
            a.b var3 = (a.b)var2.next();
            int var1;
            if(var3.g == null) {
               for(var1 = 0; var1 < this.h; ++var1) {
                  this.i += var3.e[var1];
               }
            } else {
               var3.g = null;

               for(var1 = 0; var1 < this.h; ++var1) {
                  a(var3.a(var1));
                  a(var3.b(var1));
               }

               var2.remove();
            }
         }

         return;
      }
   }

   // $FF: synthetic method
   static boolean c(a var0) {
      return var0.e();
   }

   private void d() throws IOException {
      // $FF: Couldn't be decompiled
   }

   // $FF: synthetic method
   static void d(a var0) throws IOException {
      var0.d();
   }

   private void d(String var1) throws IOException {
      int var3 = var1.indexOf(32);
      if(var3 == -1) {
         throw new IOException("unexpected journal line: " + var1);
      } else {
         int var2 = var3 + 1;
         int var4 = var1.indexOf(32, var2);
         String var5;
         if(var4 == -1) {
            var5 = var1.substring(var2);
            if(var3 == "REMOVE".length() && var1.startsWith("REMOVE")) {
               this.k.remove(var5);
               return;
            }
         } else {
            var5 = var1.substring(var2, var4);
         }

         a.b var7 = (a.b)this.k.get(var5);
         a.b var6 = var7;
         if(var7 == null) {
            var6 = new a.b(var5, null);
            this.k.put(var5, var6);
         }

         if(var4 != -1 && var3 == "CLEAN".length() && var1.startsWith("CLEAN")) {
            String[] var8 = var1.substring(var4 + 1).split(" ");
            var6.f = true;
            var6.g = null;
            var6.a(var8);
         } else if(var4 == -1 && var3 == "DIRTY".length() && var1.startsWith("DIRTY")) {
            var6.g = new a.a(var6, null);
         } else if(var4 != -1 || var3 != "READ".length() || !var1.startsWith("READ")) {
            throw new IOException("unexpected journal line: " + var1);
         }

      }
   }

   private boolean e() {
      boolean var1;
      if(this.l >= 2000 && this.l >= this.k.size()) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   private void f() {
      if(this.j == null) {
         throw new IllegalStateException("cache is closed");
      }
   }

   private void g() throws IOException {
      while(this.i > this.g) {
         this.c((String)((Entry)this.k.entrySet().iterator().next()).getKey());
      }

   }

   public a.c a(String param1) throws IOException {
      // $FF: Couldn't be decompiled
   }

   public void a() throws IOException {
      this.close();
      c.a(this.b);
   }

   public a.a b(String var1) throws IOException {
      return this.a(var1, -1L);
   }

   public boolean c(String param1) throws IOException {
      // $FF: Couldn't be decompiled
   }

   public void close() throws IOException {
      // $FF: Couldn't be decompiled
   }

   public final class a {
      private final a.b b;
      private final boolean[] c;
      private boolean d;

      private a(a.b var2) {
         this.b = var2;
         boolean[] var3;
         if(var2.f) {
            var3 = null;
         } else {
            var3 = new boolean[a.this.h];
         }

         this.c = var3;
      }

      // $FF: synthetic method
      a(a.b var2, Object var3) {
         this(var2);
      }

      // $FF: synthetic method
      static a.b a(a.a var0) {
         return var0.b;
      }

      // $FF: synthetic method
      static boolean[] b(a.a var0) {
         return var0.c;
      }

      public File a(int param1) throws IOException {
         // $FF: Couldn't be decompiled
      }

      public void a() throws IOException {
         a.this.a(this, true);
         this.d = true;
      }

      public void b() throws IOException {
         a.this.a(this, false);
      }

      public void c() {
         if(!this.d) {
            try {
               this.b();
            } catch (IOException var2) {
               ;
            }
         }

      }
   }

   private final class b {
      File[] a;
      File[] b;
      private final String d;
      private final long[] e;
      private boolean f;
      private a.a g;
      private long h;

      private b(String var2) {
         this.d = var2;
         this.e = new long[a.this.h];
         this.a = new File[a.this.h];
         this.b = new File[a.this.h];
         StringBuilder var5 = (new StringBuilder(var2)).append('.');
         int var4 = var5.length();

         for(int var3 = 0; var3 < a.this.h; ++var3) {
            var5.append(var3);
            this.a[var3] = new File(a.this.b, var5.toString());
            var5.append(".tmp");
            this.b[var3] = new File(a.this.b, var5.toString());
            var5.setLength(var4);
         }

      }

      // $FF: synthetic method
      b(String var2, Object var3) {
         this(var2);
      }

      // $FF: synthetic method
      static long a(a.b var0, long var1) {
         var0.h = var1;
         return var1;
      }

      private void a(String[] var1) throws IOException {
         if(var1.length != a.this.h) {
            throw this.b(var1);
         } else {
            int var2 = 0;

            while(true) {
               try {
                  if(var2 >= var1.length) {
                     return;
                  }

                  this.e[var2] = Long.parseLong(var1[var2]);
               } catch (NumberFormatException var4) {
                  throw this.b(var1);
               }

               ++var2;
            }
         }
      }

      private IOException b(String[] var1) throws IOException {
         throw new IOException("unexpected journal line: " + Arrays.toString(var1));
      }

      // $FF: synthetic method
      static String c(a.b var0) {
         return var0.d;
      }

      // $FF: synthetic method
      static long e(a.b var0) {
         return var0.h;
      }

      public File a(int var1) {
         return this.a[var1];
      }

      public String a() throws IOException {
         StringBuilder var6 = new StringBuilder();
         long[] var5 = this.e;
         int var2 = var5.length;

         for(int var1 = 0; var1 < var2; ++var1) {
            long var3 = var5[var1];
            var6.append(' ').append(var3);
         }

         return var6.toString();
      }

      public File b(int var1) {
         return this.b[var1];
      }
   }

   public final class c {
      private final String b;
      private final long c;
      private final long[] d;
      private final File[] e;

      private c(String var2, long var3, File[] var5, long[] var6) {
         this.b = var2;
         this.c = var3;
         this.e = var5;
         this.d = var6;
      }

      // $FF: synthetic method
      c(String var2, long var3, File[] var5, long[] var6, Object var7) {
         this(var2, var3, var5, var6);
      }

      public File a(int var1) {
         return this.e[var1];
      }
   }
}
