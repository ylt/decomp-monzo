package com.bumptech.glide;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import com.bumptech.glide.d.m;

public class e implements Cloneable {
   private boolean A;
   private Drawable B;
   private int C;
   protected final Class a;
   protected final Context b;
   protected final g c;
   protected final Class d;
   protected final m e;
   protected final com.bumptech.glide.d.g f;
   private com.bumptech.glide.f.a g;
   private Object h;
   private com.bumptech.glide.load.c i;
   private boolean j;
   private int k;
   private int l;
   private com.bumptech.glide.g.d m;
   private Float n;
   private e o;
   private Float p;
   private Drawable q;
   private Drawable r;
   private i s;
   private boolean t;
   private com.bumptech.glide.g.a.d u;
   private int v;
   private int w;
   private com.bumptech.glide.load.engine.b x;
   private com.bumptech.glide.load.g y;
   private boolean z;

   e(Context var1, Class var2, com.bumptech.glide.f.f var3, Class var4, g var5, m var6, com.bumptech.glide.d.g var7) {
      Object var8 = null;
      super();
      this.i = com.bumptech.glide.h.a.a();
      this.p = Float.valueOf(1.0F);
      this.s = null;
      this.t = true;
      this.u = com.bumptech.glide.g.a.e.a();
      this.v = -1;
      this.w = -1;
      this.x = com.bumptech.glide.load.engine.b.d;
      this.y = com.bumptech.glide.load.resource.d.b();
      this.b = var1;
      this.a = var2;
      this.d = var4;
      this.c = var5;
      this.e = var6;
      this.f = var7;
      com.bumptech.glide.f.a var9 = (com.bumptech.glide.f.a)var8;
      if(var3 != null) {
         var9 = new com.bumptech.glide.f.a(var3);
      }

      this.g = var9;
      if(var1 == null) {
         throw new NullPointerException("Context can't be null");
      } else if(var2 != null && var3 == null) {
         throw new NullPointerException("LoadProvider must not be null");
      }
   }

   e(com.bumptech.glide.f.f var1, Class var2, e var3) {
      this(var3.b, var3.a, var1, var2, var3.c, var3.e, var3.f);
      this.h = var3.h;
      this.j = var3.j;
      this.i = var3.i;
      this.x = var3.x;
      this.t = var3.t;
   }

   private com.bumptech.glide.g.b a(com.bumptech.glide.g.b.j var1, float var2, i var3, com.bumptech.glide.g.c var4) {
      return com.bumptech.glide.g.a.a(this.g, this.h, this.i, this.b, var3, var1, var2, this.q, this.k, this.r, this.l, this.B, this.C, this.m, var4, this.c.b(), this.y, this.d, this.t, this.u, this.w, this.v, this.x);
   }

   private com.bumptech.glide.g.b a(com.bumptech.glide.g.b.j var1, com.bumptech.glide.g.f var2) {
      Object var5;
      if(this.o != null) {
         if(this.A) {
            throw new IllegalStateException("You cannot use a request as both the main request and a thumbnail, consider using clone() on the request(s) passed to thumbnail()");
         }

         if(this.o.u.equals(com.bumptech.glide.g.a.e.a())) {
            this.o.u = this.u;
         }

         if(this.o.s == null) {
            this.o.s = this.a();
         }

         if(com.bumptech.glide.i.h.a(this.w, this.v) && !com.bumptech.glide.i.h.a(this.o.w, this.o.v)) {
            this.o.b(this.w, this.v);
         }

         var2 = new com.bumptech.glide.g.f(var2);
         com.bumptech.glide.g.b var3 = this.a(var1, this.p.floatValue(), this.s, var2);
         this.A = true;
         com.bumptech.glide.g.b var4 = this.o.a(var1, var2);
         this.A = false;
         var2.a(var3, var4);
         var5 = var2;
      } else if(this.n != null) {
         var2 = new com.bumptech.glide.g.f(var2);
         var2.a(this.a(var1, this.p.floatValue(), this.s, var2), this.a(var1, this.n.floatValue(), this.a(), var2));
         var5 = var2;
      } else {
         var5 = this.a(var1, this.p.floatValue(), this.s, var2);
      }

      return (com.bumptech.glide.g.b)var5;
   }

   private i a() {
      i var1;
      if(this.s == i.d) {
         var1 = i.c;
      } else if(this.s == i.c) {
         var1 = i.b;
      } else {
         var1 = i.a;
      }

      return var1;
   }

   private com.bumptech.glide.g.b b(com.bumptech.glide.g.b.j var1) {
      if(this.s == null) {
         this.s = i.c;
      }

      return this.a(var1, (com.bumptech.glide.g.f)null);
   }

   public com.bumptech.glide.g.b.j a(ImageView var1) {
      com.bumptech.glide.i.h.a();
      if(var1 == null) {
         throw new IllegalArgumentException("You must pass in a non null View");
      } else {
         if(!this.z && var1.getScaleType() != null) {
            switch(null.a[var1.getScaleType().ordinal()]) {
            case 1:
               this.e();
               break;
            case 2:
            case 3:
            case 4:
               this.d();
            }
         }

         return this.a(this.c.a(var1, this.d));
      }
   }

   public com.bumptech.glide.g.b.j a(com.bumptech.glide.g.b.j var1) {
      com.bumptech.glide.i.h.a();
      if(var1 == null) {
         throw new IllegalArgumentException("You must pass in a non null Target");
      } else if(!this.j) {
         throw new IllegalArgumentException("You must first set a model (try #load())");
      } else {
         com.bumptech.glide.g.b var2 = var1.c();
         if(var2 != null) {
            var2.d();
            this.e.b(var2);
            var2.a();
         }

         var2 = this.b(var1);
         var1.a(var2);
         this.f.a(var1);
         this.e.a(var2);
         return var1;
      }
   }

   public e b(float var1) {
      if(var1 >= 0.0F && var1 <= 1.0F) {
         this.n = Float.valueOf(var1);
         return this;
      } else {
         throw new IllegalArgumentException("sizeMultiplier must be between 0 and 1");
      }
   }

   public e b(int var1) {
      this.k = var1;
      return this;
   }

   public e b(int var1, int var2) {
      if(!com.bumptech.glide.i.h.a(var1, var2)) {
         throw new IllegalArgumentException("Width and height must be Target#SIZE_ORIGINAL or > 0");
      } else {
         this.w = var1;
         this.v = var2;
         return this;
      }
   }

   public e b(com.bumptech.glide.g.a.d var1) {
      if(var1 == null) {
         throw new NullPointerException("Animation factory must not be null!");
      } else {
         this.u = var1;
         return this;
      }
   }

   public e b(com.bumptech.glide.g.d var1) {
      this.m = var1;
      return this;
   }

   public e b(com.bumptech.glide.load.b var1) {
      if(this.g != null) {
         this.g.a(var1);
      }

      return this;
   }

   public e b(com.bumptech.glide.load.c var1) {
      if(var1 == null) {
         throw new NullPointerException("Signature must not be null");
      } else {
         this.i = var1;
         return this;
      }
   }

   public e b(com.bumptech.glide.load.e var1) {
      if(this.g != null) {
         this.g.a(var1);
      }

      return this;
   }

   public e b(com.bumptech.glide.load.engine.b var1) {
      this.x = var1;
      return this;
   }

   public e b(Object var1) {
      this.h = var1;
      this.j = true;
      return this;
   }

   public e b(boolean var1) {
      if(!var1) {
         var1 = true;
      } else {
         var1 = false;
      }

      this.t = var1;
      return this;
   }

   public e b(com.bumptech.glide.load.g... var1) {
      this.z = true;
      if(var1.length == 1) {
         this.y = var1[0];
      } else {
         this.y = new com.bumptech.glide.load.d(var1);
      }

      return this;
   }

   // $FF: synthetic method
   public Object clone() throws CloneNotSupportedException {
      return this.f();
   }

   void d() {
   }

   void e() {
   }

   public e f() {
      // $FF: Couldn't be decompiled
   }
}
