package com.bumptech.glide;

import android.content.Context;
import android.os.Build.VERSION;
import java.util.concurrent.ExecutorService;

public class h {
   private final Context a;
   private com.bumptech.glide.load.engine.c b;
   private com.bumptech.glide.load.engine.a.c c;
   private com.bumptech.glide.load.engine.b.h d;
   private ExecutorService e;
   private ExecutorService f;
   private com.bumptech.glide.load.a g;
   private com.bumptech.glide.load.engine.b.a.a h;

   public h(Context var1) {
      this.a = var1.getApplicationContext();
   }

   g a() {
      if(this.e == null) {
         this.e = new com.bumptech.glide.load.engine.c.a(Math.max(1, Runtime.getRuntime().availableProcessors()));
      }

      if(this.f == null) {
         this.f = new com.bumptech.glide.load.engine.c.a(1);
      }

      com.bumptech.glide.load.engine.b.i var1 = new com.bumptech.glide.load.engine.b.i(this.a);
      if(this.c == null) {
         if(VERSION.SDK_INT >= 11) {
            this.c = new com.bumptech.glide.load.engine.a.f(var1.b());
         } else {
            this.c = new com.bumptech.glide.load.engine.a.d();
         }
      }

      if(this.d == null) {
         this.d = new com.bumptech.glide.load.engine.b.g(var1.a());
      }

      if(this.h == null) {
         this.h = new com.bumptech.glide.load.engine.b.f(this.a);
      }

      if(this.b == null) {
         this.b = new com.bumptech.glide.load.engine.c(this.d, this.h, this.f, this.e);
      }

      if(this.g == null) {
         this.g = com.bumptech.glide.load.a.d;
      }

      return new g(this.b, this.d, this.c, this.a, this.g);
   }
}
