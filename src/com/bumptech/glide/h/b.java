package com.bumptech.glide.h;

import com.bumptech.glide.load.c;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;

public class b implements c {
   private final String a;

   public b(String var1) {
      if(var1 == null) {
         throw new NullPointerException("Signature cannot be null!");
      } else {
         this.a = var1;
      }
   }

   public void a(MessageDigest var1) throws UnsupportedEncodingException {
      var1.update(this.a.getBytes("UTF-8"));
   }

   public boolean equals(Object var1) {
      boolean var2;
      if(this == var1) {
         var2 = true;
      } else if(var1 != null && this.getClass() == var1.getClass()) {
         b var3 = (b)var1;
         var2 = this.a.equals(var3.a);
      } else {
         var2 = false;
      }

      return var2;
   }

   public int hashCode() {
      return this.a.hashCode();
   }

   public String toString() {
      return "StringSignature{signature='" + this.a + '\'' + '}';
   }
}
