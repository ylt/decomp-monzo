package com.bumptech.glide;

import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import com.bumptech.glide.d.l;
import com.bumptech.glide.d.m;
import java.io.File;

public class j implements com.bumptech.glide.d.h {
   private final Context a;
   private final com.bumptech.glide.d.g b;
   private final l c;
   private final m d;
   private final g e;
   private final j.c f;
   private j.a g;

   public j(Context var1, com.bumptech.glide.d.g var2, l var3) {
      this(var1, var2, var3, new m(), new com.bumptech.glide.d.d());
   }

   j(Context var1, final com.bumptech.glide.d.g var2, l var3, m var4, com.bumptech.glide.d.d var5) {
      this.a = var1.getApplicationContext();
      this.b = var2;
      this.c = var3;
      this.d = var4;
      this.e = g.a(var1);
      this.f = new j.c();
      com.bumptech.glide.d.c var6 = var5.a(var1, new j.d(var4));
      if(com.bumptech.glide.i.h.c()) {
         (new Handler(Looper.getMainLooper())).post(new Runnable() {
            public void run() {
               var2.a(j.this);
            }
         });
      } else {
         var2.a(this);
      }

      var2.a(var6);
   }

   private d a(Class var1) {
      com.bumptech.glide.load.b.l var2 = g.a(var1, this.a);
      com.bumptech.glide.load.b.l var3 = g.b(var1, this.a);
      if(var1 != null && var2 == null && var3 == null) {
         throw new IllegalArgumentException("Unknown type " + var1 + ". You must provide a Model of a type for" + " which there is a registered ModelLoader, if you are using a custom model, you must first call" + " Glide#register with a ModelLoaderFactory for your custom model class");
      } else {
         return (d)this.f.a(new d(var1, var2, var3, this.a, this.e, this.d, this.b, this.f));
      }
   }

   private static Class b(Object var0) {
      Class var1;
      if(var0 != null) {
         var1 = var0.getClass();
      } else {
         var1 = null;
      }

      return var1;
   }

   public d a(Uri var1) {
      return (d)this.h().a(var1);
   }

   public d a(File var1) {
      return (d)this.i().a(var1);
   }

   public d a(String var1) {
      return (d)this.g().a(var1);
   }

   public j.b a(com.bumptech.glide.load.b.l var1, Class var2) {
      return new j.b(var1, var2);
   }

   public void a() {
      this.e.h();
   }

   public void a(int var1) {
      this.e.a(var1);
   }

   public void b() {
      com.bumptech.glide.i.h.a();
      this.d.a();
   }

   public void c() {
      com.bumptech.glide.i.h.a();
      this.d.b();
   }

   public void d() {
      this.c();
   }

   public void e() {
      this.b();
   }

   public void f() {
      this.d.c();
   }

   public d g() {
      return this.a(String.class);
   }

   public d h() {
      return this.a(Uri.class);
   }

   public d i() {
      return this.a(File.class);
   }

   public interface a {
      void a(e var1);
   }

   public final class b {
      private final com.bumptech.glide.load.b.l b;
      private final Class c;

      b(com.bumptech.glide.load.b.l var2, Class var3) {
         this.b = var2;
         this.c = var3;
      }

      public j.a a(Object var1) {
         return new j.a(var1);
      }
   }

   public final class a {
      private final Object b;
      private final Class c;
      private final boolean d = true;

      a(Object var2) {
         this.b = var2;
         this.c = j.b(var2);
      }

      public f a(Class var1) {
         f var2 = (f)j.super.a.f.a(new f(j.super.a.a, j.super.a.e, this.c, j.super.b, j.super.c, var1, j.super.a.d, j.super.a.b, j.super.a.f));
         if(this.d) {
            var2.b(this.b);
         }

         return var2;
      }
   }

   class c {
      public e a(e var1) {
         if(j.this.g != null) {
            j.this.g.a(var1);
         }

         return var1;
      }
   }

   private static class d implements com.bumptech.glide.d.c.a {
      private final m a;

      public d(m var1) {
         this.a = var1;
      }

      public void a(boolean var1) {
         if(var1) {
            this.a.d();
         }

      }
   }
}
