package com.bumptech.glide.g;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import com.bumptech.glide.i;
import com.bumptech.glide.g.b.h;
import com.bumptech.glide.g.b.j;
import com.bumptech.glide.load.g;
import com.bumptech.glide.load.engine.k;
import java.util.Queue;

public final class a implements b, h, e {
   private static final Queue a = com.bumptech.glide.i.h.a(0);
   private k A;
   private com.bumptech.glide.load.engine.c.c B;
   private long C;
   private a.a D;
   private final String b = String.valueOf(this.hashCode());
   private com.bumptech.glide.load.c c;
   private Drawable d;
   private int e;
   private int f;
   private int g;
   private Context h;
   private g i;
   private com.bumptech.glide.f.f j;
   private c k;
   private Object l;
   private Class m;
   private boolean n;
   private i o;
   private j p;
   private d q;
   private float r;
   private com.bumptech.glide.load.engine.c s;
   private com.bumptech.glide.g.a.d t;
   private int u;
   private int v;
   private com.bumptech.glide.load.engine.b w;
   private Drawable x;
   private Drawable y;
   private boolean z;

   public static a a(com.bumptech.glide.f.f var0, Object var1, com.bumptech.glide.load.c var2, Context var3, i var4, j var5, float var6, Drawable var7, int var8, Drawable var9, int var10, Drawable var11, int var12, d var13, c var14, com.bumptech.glide.load.engine.c var15, g var16, Class var17, boolean var18, com.bumptech.glide.g.a.d var19, int var20, int var21, com.bumptech.glide.load.engine.b var22) {
      a var24 = (a)a.poll();
      a var23 = var24;
      if(var24 == null) {
         var23 = new a();
      }

      var23.b(var0, var1, var2, var3, var4, var5, var6, var7, var8, var9, var10, var11, var12, var13, var14, var15, var16, var17, var18, var19, var20, var21, var22);
      return var23;
   }

   private void a(k var1, Object var2) {
      boolean var3 = this.p();
      this.D = a.a.d;
      this.A = var1;
      if(this.q == null || !this.q.a(var2, this.l, this.p, this.z, var3)) {
         com.bumptech.glide.g.a.c var4 = this.t.a(this.z, var3);
         this.p.a(var2, var4);
      }

      this.q();
      if(Log.isLoggable("GenericRequest", 2)) {
         this.a("Resource ready in " + com.bumptech.glide.i.d.a(this.C) + " size: " + (double)var1.c() * 9.5367431640625E-7D + " fromCache: " + this.z);
      }

   }

   private void a(String var1) {
      Log.v("GenericRequest", var1 + " this: " + this.b);
   }

   private static void a(String var0, Object var1, String var2) {
      if(var1 == null) {
         StringBuilder var3 = new StringBuilder(var0);
         var3.append(" must not be null");
         if(var2 != null) {
            var3.append(", ");
            var3.append(var2);
         }

         throw new NullPointerException(var3.toString());
      }
   }

   private void b(com.bumptech.glide.f.f var1, Object var2, com.bumptech.glide.load.c var3, Context var4, i var5, j var6, float var7, Drawable var8, int var9, Drawable var10, int var11, Drawable var12, int var13, d var14, c var15, com.bumptech.glide.load.engine.c var16, g var17, Class var18, boolean var19, com.bumptech.glide.g.a.d var20, int var21, int var22, com.bumptech.glide.load.engine.b var23) {
      this.j = var1;
      this.l = var2;
      this.c = var3;
      this.d = var12;
      this.e = var13;
      this.h = var4.getApplicationContext();
      this.o = var5;
      this.p = var6;
      this.r = var7;
      this.x = var8;
      this.f = var9;
      this.y = var10;
      this.g = var11;
      this.q = var14;
      this.k = var15;
      this.s = var16;
      this.i = var17;
      this.m = var18;
      this.n = var19;
      this.t = var20;
      this.u = var21;
      this.v = var22;
      this.w = var23;
      this.D = a.a.a;
      if(var2 != null) {
         a("ModelLoader", var1.e(), "try .using(ModelLoader)");
         a("Transcoder", var1.f(), "try .as*(Class).transcode(ResourceTranscoder)");
         a("Transformation", var17, "try .transform(UnitTransformation.get())");
         if(var23.a()) {
            a("SourceEncoder", var1.c(), "try .sourceEncoder(Encoder) or .diskCacheStrategy(NONE/RESULT)");
         } else {
            a("SourceDecoder", var1.b(), "try .decoder/.imageDecoder/.videoDecoder(ResourceDecoder) or .diskCacheStrategy(ALL/SOURCE)");
         }

         if(var23.a() || var23.b()) {
            a("CacheDecoder", var1.a(), "try .cacheDecoder(ResouceDecoder) or .diskCacheStrategy(NONE)");
         }

         if(var23.b()) {
            a("Encoder", var1.d(), "try .encode(ResourceEncoder) or .diskCacheStrategy(NONE/SOURCE)");
         }
      }

   }

   private void b(k var1) {
      this.s.a(var1);
      this.A = null;
   }

   private void b(Exception var1) {
      if(this.o()) {
         Drawable var3;
         if(this.l == null) {
            var3 = this.k();
         } else {
            var3 = null;
         }

         Drawable var2 = var3;
         if(var3 == null) {
            var2 = this.l();
         }

         var3 = var2;
         if(var2 == null) {
            var3 = this.m();
         }

         this.p.a(var1, var3);
      }

   }

   private Drawable k() {
      if(this.d == null && this.e > 0) {
         this.d = this.h.getResources().getDrawable(this.e);
      }

      return this.d;
   }

   private Drawable l() {
      if(this.y == null && this.g > 0) {
         this.y = this.h.getResources().getDrawable(this.g);
      }

      return this.y;
   }

   private Drawable m() {
      if(this.x == null && this.f > 0) {
         this.x = this.h.getResources().getDrawable(this.f);
      }

      return this.x;
   }

   private boolean n() {
      boolean var1;
      if(this.k != null && !this.k.a(this)) {
         var1 = false;
      } else {
         var1 = true;
      }

      return var1;
   }

   private boolean o() {
      boolean var1;
      if(this.k != null && !this.k.b(this)) {
         var1 = false;
      } else {
         var1 = true;
      }

      return var1;
   }

   private boolean p() {
      boolean var1;
      if(this.k != null && this.k.c()) {
         var1 = false;
      } else {
         var1 = true;
      }

      return var1;
   }

   private void q() {
      if(this.k != null) {
         this.k.c(this);
      }

   }

   public void a() {
      this.j = null;
      this.l = null;
      this.h = null;
      this.p = null;
      this.x = null;
      this.y = null;
      this.d = null;
      this.q = null;
      this.k = null;
      this.i = null;
      this.t = null;
      this.z = false;
      this.B = null;
      a.offer(this);
   }

   public void a(int var1, int var2) {
      if(Log.isLoggable("GenericRequest", 2)) {
         this.a("Got onSizeReady in " + com.bumptech.glide.i.d.a(this.C));
      }

      if(this.D == a.a.c) {
         this.D = a.a.b;
         var1 = Math.round(this.r * (float)var1);
         var2 = Math.round(this.r * (float)var2);
         com.bumptech.glide.load.a.c var5 = this.j.e().a(this.l, var1, var2);
         if(var5 == null) {
            this.a(new Exception("Failed to load model: '" + this.l + "'"));
         } else {
            com.bumptech.glide.load.resource.e.c var4 = this.j.f();
            if(Log.isLoggable("GenericRequest", 2)) {
               this.a("finished setup for calling load in " + com.bumptech.glide.i.d.a(this.C));
            }

            this.z = true;
            this.B = this.s.a(this.c, var1, var2, var5, this.j, this.i, var4, this.o, this.n, this.w, this);
            boolean var3;
            if(this.A != null) {
               var3 = true;
            } else {
               var3 = false;
            }

            this.z = var3;
            if(Log.isLoggable("GenericRequest", 2)) {
               this.a("finished onSizeReady in " + com.bumptech.glide.i.d.a(this.C));
            }
         }
      }

   }

   public void a(k var1) {
      if(var1 == null) {
         this.a(new Exception("Expected to receive a Resource<R> with an object of " + this.m + " inside, but instead got null."));
      } else {
         Object var3 = var1.b();
         if(var3 != null && this.m.isAssignableFrom(var3.getClass())) {
            if(!this.n()) {
               this.b(var1);
               this.D = a.a.d;
            } else {
               this.a(var1, var3);
            }
         } else {
            this.b(var1);
            StringBuilder var4 = (new StringBuilder()).append("Expected to receive an object of ").append(this.m).append(" but instead got ");
            Object var2;
            if(var3 != null) {
               var2 = var3.getClass();
            } else {
               var2 = "";
            }

            StringBuilder var6 = var4.append(var2).append("{").append(var3).append("}").append(" inside Resource{").append(var1).append("}.");
            String var5;
            if(var3 != null) {
               var5 = "";
            } else {
               var5 = " To indicate failure return a null Resource object, rather than a Resource object containing null data.";
            }

            this.a(new Exception(var6.append(var5).toString()));
         }
      }

   }

   public void a(Exception var1) {
      if(Log.isLoggable("GenericRequest", 3)) {
         Log.d("GenericRequest", "load failed", var1);
      }

      this.D = a.a.e;
      if(this.q == null || !this.q.a(var1, this.l, this.p, this.p())) {
         this.b(var1);
      }

   }

   public void b() {
      this.C = com.bumptech.glide.i.d.a();
      if(this.l == null) {
         this.a((Exception)null);
      } else {
         this.D = a.a.c;
         if(com.bumptech.glide.i.h.a(this.u, this.v)) {
            this.a(this.u, this.v);
         } else {
            this.p.a((h)this);
         }

         if(!this.g() && !this.j() && this.o()) {
            this.p.c(this.m());
         }

         if(Log.isLoggable("GenericRequest", 2)) {
            this.a("finished run method in " + com.bumptech.glide.i.d.a(this.C));
         }
      }

   }

   void c() {
      this.D = a.a.f;
      if(this.B != null) {
         this.B.a();
         this.B = null;
      }

   }

   public void d() {
      com.bumptech.glide.i.h.a();
      if(this.D != a.a.g) {
         this.c();
         if(this.A != null) {
            this.b(this.A);
         }

         if(this.o()) {
            this.p.b(this.m());
         }

         this.D = a.a.g;
      }

   }

   public void e() {
      this.d();
      this.D = a.a.h;
   }

   public boolean f() {
      boolean var1;
      if(this.D != a.a.b && this.D != a.a.c) {
         var1 = false;
      } else {
         var1 = true;
      }

      return var1;
   }

   public boolean g() {
      boolean var1;
      if(this.D == a.a.d) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public boolean h() {
      return this.g();
   }

   public boolean i() {
      boolean var1;
      if(this.D != a.a.f && this.D != a.a.g) {
         var1 = false;
      } else {
         var1 = true;
      }

      return var1;
   }

   public boolean j() {
      boolean var1;
      if(this.D == a.a.e) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   private static enum a {
      a,
      b,
      c,
      d,
      e,
      f,
      g,
      h;
   }
}
