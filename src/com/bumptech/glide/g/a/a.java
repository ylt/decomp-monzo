package com.bumptech.glide.g.a;

import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;

public class a implements d {
   private final g a;
   private final int b;
   private b c;
   private b d;

   public a() {
      this(300);
   }

   public a(int var1) {
      this(new g(new a.a(var1)), var1);
   }

   a(g var1, int var2) {
      this.a = var1;
      this.b = var2;
   }

   private c a() {
      if(this.c == null) {
         this.c = new b(this.a.a(false, true), this.b);
      }

      return this.c;
   }

   private c b() {
      if(this.d == null) {
         this.d = new b(this.a.a(false, false), this.b);
      }

      return this.d;
   }

   public c a(boolean var1, boolean var2) {
      c var3;
      if(var1) {
         var3 = e.b();
      } else if(var2) {
         var3 = this.a();
      } else {
         var3 = this.b();
      }

      return var3;
   }

   private static class a implements f.a {
      private final int a;

      a(int var1) {
         this.a = var1;
      }

      public Animation a() {
         AlphaAnimation var1 = new AlphaAnimation(0.0F, 1.0F);
         var1.setDuration((long)this.a);
         return var1;
      }
   }
}
