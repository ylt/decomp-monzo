package com.bumptech.glide.g.a;

import android.view.View;
import android.view.animation.Animation;

public class f implements c {
   private final f.a a;

   f(f.a var1) {
      this.a = var1;
   }

   public boolean a(Object var1, c.a var2) {
      View var3 = var2.a();
      if(var3 != null) {
         var3.clearAnimation();
         var3.startAnimation(this.a.a());
      }

      return false;
   }

   interface a {
      Animation a();
   }
}
