package com.bumptech.glide.g.a;

import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;

public class b implements c {
   private final c a;
   private final int b;

   public b(c var1, int var2) {
      this.a = var1;
      this.b = var2;
   }

   public boolean a(Drawable var1, c.a var2) {
      boolean var3 = true;
      Drawable var4 = var2.b();
      if(var4 != null) {
         TransitionDrawable var5 = new TransitionDrawable(new Drawable[]{var4, var1});
         var5.setCrossFadeEnabled(true);
         var5.startTransition(this.b);
         var2.a(var5);
      } else {
         this.a.a(var1, var2);
         var3 = false;
      }

      return var3;
   }
}
