package com.bumptech.glide.g.b;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

public class f {
   public j a(ImageView var1, Class var2) {
      Object var3;
      if(com.bumptech.glide.load.resource.a.b.class.isAssignableFrom(var2)) {
         var3 = new d(var1);
      } else if(Bitmap.class.equals(var2)) {
         var3 = new b(var1);
      } else {
         if(!Drawable.class.isAssignableFrom(var2)) {
            throw new IllegalArgumentException("Unhandled class: " + var2 + ", try .as*(Class).transcode(ResourceTranscoder)");
         }

         var3 = new c(var1);
      }

      return (j)var3;
   }
}
