package com.bumptech.glide.g.b;

import android.annotation.TargetApi;
import android.graphics.Point;
import android.os.Build.VERSION;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewTreeObserver.OnPreDrawListener;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public abstract class k extends a {
   private static boolean a = false;
   private static Integer b = null;
   protected final View c;
   private final k.a d;

   public k(View var1) {
      if(var1 == null) {
         throw new NullPointerException("View must not be null!");
      } else {
         this.c = var1;
         this.d = new k.a(var1);
      }
   }

   private void a(Object var1) {
      if(b == null) {
         a = true;
         this.c.setTag(var1);
      } else {
         this.c.setTag(b.intValue(), var1);
      }

   }

   private Object g() {
      Object var1;
      if(b == null) {
         var1 = this.c.getTag();
      } else {
         var1 = this.c.getTag(b.intValue());
      }

      return var1;
   }

   public View a() {
      return this.c;
   }

   public void a(h var1) {
      this.d.a(var1);
   }

   public void a(com.bumptech.glide.g.b var1) {
      this.a((Object)var1);
   }

   public com.bumptech.glide.g.b c() {
      Object var1 = this.g();
      com.bumptech.glide.g.b var2;
      if(var1 != null) {
         if(!(var1 instanceof com.bumptech.glide.g.b)) {
            throw new IllegalArgumentException("You must not call setTag() on a view Glide is targeting");
         }

         var2 = (com.bumptech.glide.g.b)var1;
      } else {
         var2 = null;
      }

      return var2;
   }

   public String toString() {
      return "Target for: " + this.c;
   }

   private static class a {
      private final View a;
      private final List b = new ArrayList();
      private k.a c;
      private Point d;

      public a(View var1) {
         this.a = var1;
      }

      private int a(int var1, boolean var2) {
         if(var1 == -2) {
            Point var3 = this.d();
            if(var2) {
               var1 = var3.y;
            } else {
               var1 = var3.x;
            }
         }

         return var1;
      }

      private void a() {
         if(!this.b.isEmpty()) {
            int var2 = this.c();
            int var1 = this.b();
            if(this.a(var2) && this.a(var1)) {
               this.a(var2, var1);
               ViewTreeObserver var3 = this.a.getViewTreeObserver();
               if(var3.isAlive()) {
                  var3.removeOnPreDrawListener(this.c);
               }

               this.c = null;
            }
         }

      }

      private void a(int var1, int var2) {
         Iterator var3 = this.b.iterator();

         while(var3.hasNext()) {
            ((h)var3.next()).a(var1, var2);
         }

         this.b.clear();
      }

      private boolean a(int var1) {
         boolean var2;
         if(var1 <= 0 && var1 != -2) {
            var2 = false;
         } else {
            var2 = true;
         }

         return var2;
      }

      private int b() {
         LayoutParams var2 = this.a.getLayoutParams();
         int var1;
         if(this.a(this.a.getHeight())) {
            var1 = this.a.getHeight();
         } else if(var2 != null) {
            var1 = this.a(var2.height, true);
         } else {
            var1 = 0;
         }

         return var1;
      }

      private int c() {
         int var1 = 0;
         LayoutParams var2 = this.a.getLayoutParams();
         if(this.a(this.a.getWidth())) {
            var1 = this.a.getWidth();
         } else if(var2 != null) {
            var1 = this.a(var2.width, false);
         }

         return var1;
      }

      @TargetApi(13)
      private Point d() {
         Point var1;
         if(this.d != null) {
            var1 = this.d;
         } else {
            Display var2 = ((WindowManager)this.a.getContext().getSystemService("window")).getDefaultDisplay();
            if(VERSION.SDK_INT >= 13) {
               this.d = new Point();
               var2.getSize(this.d);
            } else {
               this.d = new Point(var2.getWidth(), var2.getHeight());
            }

            var1 = this.d;
         }

         return var1;
      }

      public void a(h var1) {
         int var2 = this.c();
         int var3 = this.b();
         if(this.a(var2) && this.a(var3)) {
            var1.a(var2, var3);
         } else {
            if(!this.b.contains(var1)) {
               this.b.add(var1);
            }

            if(this.c == null) {
               ViewTreeObserver var4 = this.a.getViewTreeObserver();
               this.c = new k.a(this);
               var4.addOnPreDrawListener(this.c);
            }
         }

      }
   }

   private static class a implements OnPreDrawListener {
      private final WeakReference a;

      public a(k.a var1) {
         this.a = new WeakReference(var1);
      }

      public boolean onPreDraw() {
         if(Log.isLoggable("ViewTarget", 2)) {
            Log.v("ViewTarget", "OnGlobalLayoutListener called listener=" + this);
         }

         k.a var1 = (k.a)this.a.get();
         if(var1 != null) {
            var1.a();
         }

         return true;
      }
   }
}
