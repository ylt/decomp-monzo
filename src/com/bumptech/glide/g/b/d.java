package com.bumptech.glide.g.b;

import android.widget.ImageView;

public class d extends e {
   private int a;
   private com.bumptech.glide.load.resource.a.b b;

   public d(ImageView var1) {
      this(var1, -1);
   }

   public d(ImageView var1, int var2) {
      super(var1);
      this.a = var2;
   }

   protected void a(com.bumptech.glide.load.resource.a.b var1) {
      ((ImageView)this.c).setImageDrawable(var1);
   }

   public void a(com.bumptech.glide.load.resource.a.b var1, com.bumptech.glide.g.a.c var2) {
      Object var5 = var1;
      if(!var1.a()) {
         float var4 = (float)((ImageView)this.c).getWidth() / (float)((ImageView)this.c).getHeight();
         float var3 = (float)var1.getIntrinsicWidth() / (float)var1.getIntrinsicHeight();
         var5 = var1;
         if(Math.abs(var4 - 1.0F) <= 0.05F) {
            var5 = var1;
            if(Math.abs(var3 - 1.0F) <= 0.05F) {
               var5 = new i(var1, ((ImageView)this.c).getWidth());
            }
         }
      }

      super.a(var5, var2);
      this.b = (com.bumptech.glide.load.resource.a.b)var5;
      ((com.bumptech.glide.load.resource.a.b)var5).a(this.a);
      ((com.bumptech.glide.load.resource.a.b)var5).start();
   }

   public void d() {
      if(this.b != null) {
         this.b.start();
      }

   }

   public void e() {
      if(this.b != null) {
         this.b.stop();
      }

   }
}
