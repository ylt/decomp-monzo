package com.bumptech.glide.g.b;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;

public abstract class e extends k implements com.bumptech.glide.g.a.c.a {
   public e(ImageView var1) {
      super(var1);
   }

   public void a(Drawable var1) {
      ((ImageView)this.c).setImageDrawable(var1);
   }

   public void a(Exception var1, Drawable var2) {
      ((ImageView)this.c).setImageDrawable(var2);
   }

   protected abstract void a(Object var1);

   public void a(Object var1, com.bumptech.glide.g.a.c var2) {
      if(var2 == null || !var2.a(var1, this)) {
         this.a(var1);
      }

   }

   public Drawable b() {
      return ((ImageView)this.c).getDrawable();
   }

   public void b(Drawable var1) {
      ((ImageView)this.c).setImageDrawable(var1);
   }

   public void c(Drawable var1) {
      ((ImageView)this.c).setImageDrawable(var1);
   }
}
