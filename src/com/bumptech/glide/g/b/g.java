package com.bumptech.glide.g.b;

public abstract class g extends a {
   private final int a;
   private final int b;

   public g() {
      this(Integer.MIN_VALUE, Integer.MIN_VALUE);
   }

   public g(int var1, int var2) {
      this.a = var1;
      this.b = var2;
   }

   public final void a(h var1) {
      if(!com.bumptech.glide.i.h.a(this.a, this.b)) {
         throw new IllegalArgumentException("Width and height must both be > 0 or Target#SIZE_ORIGINAL, but given width: " + this.a + " and height: " + this.b + ", either provide dimensions in the constructor" + " or call override()");
      } else {
         var1.a(this.a, this.b);
      }
   }
}
