package com.bumptech.glide.g;

public class f implements b, c {
   private b a;
   private b b;
   private c c;

   public f() {
      this((c)null);
   }

   public f(c var1) {
      this.c = var1;
   }

   private boolean j() {
      boolean var1;
      if(this.c != null && !this.c.a(this)) {
         var1 = false;
      } else {
         var1 = true;
      }

      return var1;
   }

   private boolean k() {
      boolean var1;
      if(this.c != null && !this.c.b(this)) {
         var1 = false;
      } else {
         var1 = true;
      }

      return var1;
   }

   private boolean l() {
      boolean var1;
      if(this.c != null && this.c.c()) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public void a() {
      this.a.a();
      this.b.a();
   }

   public void a(b var1, b var2) {
      this.a = var1;
      this.b = var2;
   }

   public boolean a(b var1) {
      boolean var2;
      if(!this.j() || !var1.equals(this.a) && this.a.h()) {
         var2 = false;
      } else {
         var2 = true;
      }

      return var2;
   }

   public void b() {
      if(!this.b.f()) {
         this.b.b();
      }

      if(!this.a.f()) {
         this.a.b();
      }

   }

   public boolean b(b var1) {
      boolean var2;
      if(this.k() && var1.equals(this.a) && !this.c()) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   public void c(b var1) {
      if(!var1.equals(this.b)) {
         if(this.c != null) {
            this.c.c(this);
         }

         if(!this.b.g()) {
            this.b.d();
         }
      }

   }

   public boolean c() {
      boolean var1;
      if(!this.l() && !this.h()) {
         var1 = false;
      } else {
         var1 = true;
      }

      return var1;
   }

   public void d() {
      this.b.d();
      this.a.d();
   }

   public void e() {
      this.a.e();
      this.b.e();
   }

   public boolean f() {
      return this.a.f();
   }

   public boolean g() {
      boolean var1;
      if(!this.a.g() && !this.b.g()) {
         var1 = false;
      } else {
         var1 = true;
      }

      return var1;
   }

   public boolean h() {
      boolean var1;
      if(!this.a.h() && !this.b.h()) {
         var1 = false;
      } else {
         var1 = true;
      }

      return var1;
   }

   public boolean i() {
      return this.a.i();
   }
}
