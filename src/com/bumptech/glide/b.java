package com.bumptech.glide;

import android.graphics.Bitmap;
import com.bumptech.glide.load.b.l;

public class b extends a {
   private final l g;
   private final l h;
   private final g i;
   private final j.c j;

   b(e var1, l var2, l var3, j.c var4) {
      super(a(var1.c, var2, var3, Bitmap.class, (com.bumptech.glide.load.resource.e.c)null), Bitmap.class, var1);
      this.g = var2;
      this.h = var3;
      this.i = var1.c;
      this.j = var4;
   }

   private static com.bumptech.glide.f.e a(g var0, l var1, l var2, Class var3, com.bumptech.glide.load.resource.e.c var4) {
      com.bumptech.glide.f.e var7;
      if(var1 == null && var2 == null) {
         var7 = null;
      } else {
         com.bumptech.glide.load.resource.e.c var5 = var4;
         if(var4 == null) {
            var5 = var0.a(Bitmap.class, var3);
         }

         com.bumptech.glide.f.b var6 = var0.b(com.bumptech.glide.load.b.g.class, Bitmap.class);
         var7 = new com.bumptech.glide.f.e(new com.bumptech.glide.load.b.f(var1, var2), var5, var6);
      }

      return var7;
   }
}
