package com.bumptech.glide.f;

import com.bumptech.glide.load.b.l;

public class a implements f, Cloneable {
   private final f a;
   private com.bumptech.glide.load.e b;
   private com.bumptech.glide.load.e c;
   private com.bumptech.glide.load.f d;
   private com.bumptech.glide.load.resource.e.c e;
   private com.bumptech.glide.load.b f;

   public a(f var1) {
      this.a = var1;
   }

   public com.bumptech.glide.load.e a() {
      com.bumptech.glide.load.e var1;
      if(this.b != null) {
         var1 = this.b;
      } else {
         var1 = this.a.a();
      }

      return var1;
   }

   public void a(com.bumptech.glide.load.b var1) {
      this.f = var1;
   }

   public void a(com.bumptech.glide.load.e var1) {
      this.c = var1;
   }

   public com.bumptech.glide.load.e b() {
      com.bumptech.glide.load.e var1;
      if(this.c != null) {
         var1 = this.c;
      } else {
         var1 = this.a.b();
      }

      return var1;
   }

   public com.bumptech.glide.load.b c() {
      com.bumptech.glide.load.b var1;
      if(this.f != null) {
         var1 = this.f;
      } else {
         var1 = this.a.c();
      }

      return var1;
   }

   // $FF: synthetic method
   public Object clone() throws CloneNotSupportedException {
      return this.g();
   }

   public com.bumptech.glide.load.f d() {
      com.bumptech.glide.load.f var1;
      if(this.d != null) {
         var1 = this.d;
      } else {
         var1 = this.a.d();
      }

      return var1;
   }

   public l e() {
      return this.a.e();
   }

   public com.bumptech.glide.load.resource.e.c f() {
      com.bumptech.glide.load.resource.e.c var1;
      if(this.e != null) {
         var1 = this.e;
      } else {
         var1 = this.a.f();
      }

      return var1;
   }

   public a g() {
      try {
         a var1 = (a)super.clone();
         return var1;
      } catch (CloneNotSupportedException var2) {
         throw new RuntimeException(var2);
      }
   }
}
