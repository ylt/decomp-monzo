package com.bumptech.glide.f;

import com.bumptech.glide.load.b.l;

public class e implements f {
   private final l a;
   private final com.bumptech.glide.load.resource.e.c b;
   private final b c;

   public e(l var1, com.bumptech.glide.load.resource.e.c var2, b var3) {
      if(var1 == null) {
         throw new NullPointerException("ModelLoader must not be null");
      } else {
         this.a = var1;
         if(var2 == null) {
            throw new NullPointerException("Transcoder must not be null");
         } else {
            this.b = var2;
            if(var3 == null) {
               throw new NullPointerException("DataLoadProvider must not be null");
            } else {
               this.c = var3;
            }
         }
      }
   }

   public com.bumptech.glide.load.e a() {
      return this.c.a();
   }

   public com.bumptech.glide.load.e b() {
      return this.c.b();
   }

   public com.bumptech.glide.load.b c() {
      return this.c.c();
   }

   public com.bumptech.glide.load.f d() {
      return this.c.d();
   }

   public l e() {
      return this.a;
   }

   public com.bumptech.glide.load.resource.e.c f() {
      return this.b;
   }
}
