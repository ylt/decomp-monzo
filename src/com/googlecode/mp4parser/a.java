package com.googlecode.mp4parser;

import com.googlecode.mp4parser.c.f;
import java.io.Closeable;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

public class a implements com.coremedia.iso.boxes.b, Closeable, Iterator {
   private static final com.coremedia.iso.boxes.a EOF = new AbstractBox("eof ") {
      protected void _parseDetails(ByteBuffer var1) {
      }

      protected void getContent(ByteBuffer var1) {
      }

      protected long getContentSize() {
         return 0L;
      }
   };
   private static f LOG = f.a(a.class);
   protected com.coremedia.iso.b boxParser;
   private List boxes = new ArrayList();
   protected b dataSource;
   long endPosition = 0L;
   com.coremedia.iso.boxes.a lookahead = null;
   long parsePosition = 0L;
   long startPosition = 0L;

   public void addBox(com.coremedia.iso.boxes.a var1) {
      if(var1 != null) {
         this.boxes = new ArrayList(this.getBoxes());
         var1.setParent(this);
         this.boxes.add(var1);
      }

   }

   public void close() throws IOException {
      this.dataSource.close();
   }

   public List getBoxes() {
      Object var1;
      if(this.dataSource != null && this.lookahead != EOF) {
         var1 = new com.googlecode.mp4parser.c.e(this.boxes, this);
      } else {
         var1 = this.boxes;
      }

      return (List)var1;
   }

   public List getBoxes(Class var1) {
      com.coremedia.iso.boxes.a var4 = null;
      List var7 = this.getBoxes();
      int var2 = 0;

      Object var3;
      Object var9;
      for(var3 = null; var2 < var7.size(); var3 = var9) {
         com.coremedia.iso.boxes.a var8;
         label29: {
            com.coremedia.iso.boxes.a var6 = (com.coremedia.iso.boxes.a)var7.get(var2);
            Object var5 = var3;
            if(var1.isInstance(var6)) {
               if(var4 == null) {
                  var9 = var3;
                  var8 = var6;
                  break label29;
               }

               var5 = var3;
               if(var3 == null) {
                  var5 = new ArrayList(2);
                  ((List)var5).add(var4);
               }

               ((List)var5).add(var6);
            }

            var8 = var4;
            var9 = var5;
         }

         ++var2;
         var4 = var8;
      }

      if(var3 == null) {
         if(var4 != null) {
            var3 = Collections.singletonList(var4);
         } else {
            var3 = Collections.emptyList();
         }
      }

      return (List)var3;
   }

   public List getBoxes(Class var1, boolean var2) {
      ArrayList var4 = new ArrayList(2);
      List var5 = this.getBoxes();

      for(int var3 = 0; var3 < var5.size(); ++var3) {
         com.coremedia.iso.boxes.a var6 = (com.coremedia.iso.boxes.a)var5.get(var3);
         if(var1.isInstance(var6)) {
            var4.add(var6);
         }

         if(var2 && var6 instanceof com.coremedia.iso.boxes.b) {
            var4.addAll(((com.coremedia.iso.boxes.b)var6).getBoxes(var1, var2));
         }
      }

      return var4;
   }

   public ByteBuffer getByteBuffer(long param1, long param3) throws IOException {
      // $FF: Couldn't be decompiled
   }

   protected long getContainerSize() {
      long var2 = 0L;

      for(int var1 = 0; var1 < this.getBoxes().size(); ++var1) {
         var2 += ((com.coremedia.iso.boxes.a)this.boxes.get(var1)).getSize();
      }

      return var2;
   }

   public boolean hasNext() {
      boolean var1 = false;
      if(this.lookahead != EOF) {
         if(this.lookahead != null) {
            var1 = true;
         } else {
            try {
               this.lookahead = this.next();
            } catch (NoSuchElementException var3) {
               this.lookahead = EOF;
               return var1;
            }

            var1 = true;
         }
      }

      return var1;
   }

   public void initContainer(b var1, long var2, com.coremedia.iso.b var4) throws IOException {
      this.dataSource = var1;
      long var5 = var1.b();
      this.startPosition = var5;
      this.parsePosition = var5;
      var1.a(var1.b() + var2);
      this.endPosition = var1.b();
      this.boxParser = var4;
   }

   public com.coremedia.iso.boxes.a next() {
      // $FF: Couldn't be decompiled
   }

   public void remove() {
      throw new UnsupportedOperationException();
   }

   public void setBoxes(List var1) {
      this.boxes = new ArrayList(var1);
      this.lookahead = EOF;
      this.dataSource = null;
   }

   public String toString() {
      StringBuilder var2 = new StringBuilder();
      var2.append(this.getClass().getSimpleName()).append("[");

      for(int var1 = 0; var1 < this.boxes.size(); ++var1) {
         if(var1 > 0) {
            var2.append(";");
         }

         var2.append(((com.coremedia.iso.boxes.a)this.boxes.get(var1)).toString());
      }

      var2.append("]");
      return var2.toString();
   }

   public final void writeContainer(WritableByteChannel var1) throws IOException {
      Iterator var2 = this.getBoxes().iterator();

      while(var2.hasNext()) {
         ((com.coremedia.iso.boxes.a)var2.next()).getBox(var1);
      }

   }
}
