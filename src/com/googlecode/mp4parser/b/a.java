package com.googlecode.mp4parser.b;

public class a {
   private char[] a;
   private int b;

   public a(int var1) {
      this.a = new char[var1];
   }

   public void a() {
      this.b = 0;
   }

   public void a(char var1) {
      if(this.b < this.a.length - 1) {
         this.a[this.b] = var1;
         ++this.b;
      }

   }

   public int b() {
      return this.b;
   }

   public String toString() {
      return new String(this.a, 0, this.b);
   }
}
