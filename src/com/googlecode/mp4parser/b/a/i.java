package com.googlecode.mp4parser.b.a;

public class i {
   public boolean a;
   public int b;
   public int c;
   public boolean d;
   public boolean e;
   public boolean f;
   public int g;
   public boolean h;
   public boolean i;
   public int j;
   public int k;
   public int l;
   public boolean m;
   public int n;
   public int o;
   public boolean p;
   public int q;
   public int r;
   public boolean s;
   public boolean t;
   public boolean u;
   public d v;
   public d w;
   public i.a x;
   public a y;

   public String toString() {
      return "VUIParameters{\naspect_ratio_info_present_flag=" + this.a + "\n" + ", sar_width=" + this.b + "\n" + ", sar_height=" + this.c + "\n" + ", overscan_info_present_flag=" + this.d + "\n" + ", overscan_appropriate_flag=" + this.e + "\n" + ", video_signal_type_present_flag=" + this.f + "\n" + ", video_format=" + this.g + "\n" + ", video_full_range_flag=" + this.h + "\n" + ", colour_description_present_flag=" + this.i + "\n" + ", colour_primaries=" + this.j + "\n" + ", transfer_characteristics=" + this.k + "\n" + ", matrix_coefficients=" + this.l + "\n" + ", chroma_loc_info_present_flag=" + this.m + "\n" + ", chroma_sample_loc_type_top_field=" + this.n + "\n" + ", chroma_sample_loc_type_bottom_field=" + this.o + "\n" + ", timing_info_present_flag=" + this.p + "\n" + ", num_units_in_tick=" + this.q + "\n" + ", time_scale=" + this.r + "\n" + ", fixed_frame_rate_flag=" + this.s + "\n" + ", low_delay_hrd_flag=" + this.t + "\n" + ", pic_struct_present_flag=" + this.u + "\n" + ", nalHRDParams=" + this.v + "\n" + ", vclHRDParams=" + this.w + "\n" + ", bitstreamRestriction=" + this.x + "\n" + ", aspect_ratio=" + this.y + "\n" + '}';
   }

   public static class a {
      public boolean a;
      public int b;
      public int c;
      public int d;
      public int e;
      public int f;
      public int g;

      public String toString() {
         StringBuilder var1 = new StringBuilder("BitstreamRestriction{");
         var1.append("motion_vectors_over_pic_boundaries_flag=").append(this.a);
         var1.append(", max_bytes_per_pic_denom=").append(this.b);
         var1.append(", max_bits_per_mb_denom=").append(this.c);
         var1.append(", log2_max_mv_length_horizontal=").append(this.d);
         var1.append(", log2_max_mv_length_vertical=").append(this.e);
         var1.append(", num_reorder_frames=").append(this.f);
         var1.append(", max_dec_frame_buffering=").append(this.g);
         var1.append('}');
         return var1.toString();
      }
   }
}
