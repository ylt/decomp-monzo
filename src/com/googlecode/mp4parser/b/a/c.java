package com.googlecode.mp4parser.b.a;

public class c {
   public static c a = new c(0, 0, 0);
   public static c b = new c(1, 2, 2);
   public static c c = new c(2, 2, 1);
   public static c d = new c(3, 1, 1);
   private int e;
   private int f;
   private int g;

   public c(int var1, int var2, int var3) {
      this.e = var1;
      this.f = var2;
      this.g = var3;
   }

   public static c a(int var0) {
      c var1;
      if(var0 == a.e) {
         var1 = a;
      } else if(var0 == b.e) {
         var1 = b;
      } else if(var0 == c.e) {
         var1 = c;
      } else if(var0 == d.e) {
         var1 = d;
      } else {
         var1 = null;
      }

      return var1;
   }

   public String toString() {
      return "ChromaFormat{\nid=" + this.e + ",\n" + " subWidth=" + this.f + ",\n" + " subHeight=" + this.g + '}';
   }
}
