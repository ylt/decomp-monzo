package com.googlecode.mp4parser.b.a;

public class a {
   public static final a a = new a(255);
   private int b;

   private a(int var1) {
      this.b = var1;
   }

   public static a a(int var0) {
      a var1;
      if(var0 == a.b) {
         var1 = a;
      } else {
         var1 = new a(var0);
      }

      return var1;
   }

   public String toString() {
      StringBuilder var1 = new StringBuilder("AspectRatio{");
      var1.append("value=").append(this.b);
      var1.append('}');
      return var1.toString();
   }
}
