package com.googlecode.mp4parser.b.a;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

public class e extends b {
   public boolean a;
   public int b;
   public int c;
   public int d;
   public int e;
   public int f;
   public boolean g;
   public int h;
   public int i;
   public boolean j;
   public int k;
   public int l;
   public int m;
   public int n;
   public boolean o;
   public boolean p;
   public boolean q;
   public int[] r;
   public int[] s;
   public int[] t;
   public boolean u;
   public int[] v;
   public e.a w;

   public static e a(InputStream var0) throws IOException {
      byte var1 = 3;
      com.googlecode.mp4parser.b.b.b var4 = new com.googlecode.mp4parser.b.b.b(var0);
      e var5 = new e();
      var5.e = var4.a("PPS: pic_parameter_set_id");
      var5.f = var4.a("PPS: seq_parameter_set_id");
      var5.a = var4.c("PPS: entropy_coding_mode_flag");
      var5.g = var4.c("PPS: pic_order_present_flag");
      var5.h = var4.a("PPS: num_slice_groups_minus1");
      int var6;
      if(var5.h > 0) {
         var5.i = var4.a("PPS: slice_group_map_type");
         var5.r = new int[var5.h + 1];
         var5.s = new int[var5.h + 1];
         var5.t = new int[var5.h + 1];
         if(var5.i == 0) {
            for(var6 = 0; var6 <= var5.h; ++var6) {
               var5.t[var6] = var4.a("PPS: run_length_minus1");
            }
         } else if(var5.i == 2) {
            for(var6 = 0; var6 < var5.h; ++var6) {
               var5.r[var6] = var4.a("PPS: top_left");
               var5.s[var6] = var4.a("PPS: bottom_right");
            }
         } else if(var5.i != 3 && var5.i != 4 && var5.i != 5) {
            if(var5.i == 6) {
               if(var5.h + 1 <= 4) {
                  if(var5.h + 1 > 2) {
                     var1 = 2;
                  } else {
                     var1 = 1;
                  }
               }

               int var3 = var4.a("PPS: pic_size_in_map_units_minus1");
               var5.v = new int[var3 + 1];

               for(int var2 = 0; var2 <= var3; ++var2) {
                  var5.v[var2] = var4.b(var1, "PPS: slice_group_id [" + var2 + "]f");
               }
            }
         } else {
            var5.u = var4.c("PPS: slice_group_change_direction_flag");
            var5.d = var4.a("PPS: slice_group_change_rate_minus1");
         }
      }

      var5.b = var4.a("PPS: num_ref_idx_l0_active_minus1");
      var5.c = var4.a("PPS: num_ref_idx_l1_active_minus1");
      var5.j = var4.c("PPS: weighted_pred_flag");
      var5.k = (int)var4.a(2, "PPS: weighted_bipred_idc");
      var5.l = var4.b("PPS: pic_init_qp_minus26");
      var5.m = var4.b("PPS: pic_init_qs_minus26");
      var5.n = var4.b("PPS: chroma_qp_index_offset");
      var5.o = var4.c("PPS: deblocking_filter_control_present_flag");
      var5.p = var4.c("PPS: constrained_intra_pred_flag");
      var5.q = var4.c("PPS: redundant_pic_cnt_present_flag");
      if(var4.b()) {
         var5.w = new e.a();
         var5.w.a = var4.c("PPS: transform_8x8_mode_flag");
         if(var4.c("PPS: pic_scaling_matrix_present_flag")) {
            var6 = 0;

            while(true) {
               byte var7;
               if(var5.w.a) {
                  var7 = 1;
               } else {
                  var7 = 0;
               }

               if(var6 >= var7 * 2 + 6) {
                  break;
               }

               if(var4.c("PPS: pic_scaling_list_present_flag")) {
                  var5.w.b.a = new f[8];
                  var5.w.b.b = new f[8];
                  if(var6 < 6) {
                     var5.w.b.a[var6] = f.a(var4, 16);
                  } else {
                     var5.w.b.b[var6 - 6] = f.a(var4, 64);
                  }
               }

               ++var6;
            }
         }

         var5.w.c = var4.b("PPS: second_chroma_qp_index_offset");
      }

      var4.d();
      return var5;
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(this != var1) {
         if(var1 == null) {
            var2 = false;
         } else if(this.getClass() != var1.getClass()) {
            var2 = false;
         } else {
            e var3 = (e)var1;
            if(!Arrays.equals(this.s, var3.s)) {
               var2 = false;
            } else if(this.n != var3.n) {
               var2 = false;
            } else if(this.p != var3.p) {
               var2 = false;
            } else if(this.o != var3.o) {
               var2 = false;
            } else if(this.a != var3.a) {
               var2 = false;
            } else {
               if(this.w == null) {
                  if(var3.w != null) {
                     var2 = false;
                     return var2;
                  }
               } else if(!this.w.equals(var3.w)) {
                  var2 = false;
                  return var2;
               }

               if(this.b != var3.b) {
                  var2 = false;
               } else if(this.c != var3.c) {
                  var2 = false;
               } else if(this.h != var3.h) {
                  var2 = false;
               } else if(this.l != var3.l) {
                  var2 = false;
               } else if(this.m != var3.m) {
                  var2 = false;
               } else if(this.g != var3.g) {
                  var2 = false;
               } else if(this.e != var3.e) {
                  var2 = false;
               } else if(this.q != var3.q) {
                  var2 = false;
               } else if(!Arrays.equals(this.t, var3.t)) {
                  var2 = false;
               } else if(this.f != var3.f) {
                  var2 = false;
               } else if(this.u != var3.u) {
                  var2 = false;
               } else if(this.d != var3.d) {
                  var2 = false;
               } else if(!Arrays.equals(this.v, var3.v)) {
                  var2 = false;
               } else if(this.i != var3.i) {
                  var2 = false;
               } else if(!Arrays.equals(this.r, var3.r)) {
                  var2 = false;
               } else if(this.k != var3.k) {
                  var2 = false;
               } else if(this.j != var3.j) {
                  var2 = false;
               }
            }
         }
      }

      return var2;
   }

   public int hashCode() {
      short var8 = 1231;
      int var10 = Arrays.hashCode(this.s);
      int var9 = this.n;
      short var1;
      if(this.p) {
         var1 = 1231;
      } else {
         var1 = 1237;
      }

      short var2;
      if(this.o) {
         var2 = 1231;
      } else {
         var2 = 1237;
      }

      short var3;
      if(this.a) {
         var3 = 1231;
      } else {
         var3 = 1237;
      }

      int var4;
      if(this.w == null) {
         var4 = 0;
      } else {
         var4 = this.w.hashCode();
      }

      int var11 = this.b;
      int var12 = this.c;
      int var15 = this.h;
      int var13 = this.l;
      int var14 = this.m;
      short var5;
      if(this.g) {
         var5 = 1231;
      } else {
         var5 = 1237;
      }

      int var16 = this.e;
      short var6;
      if(this.q) {
         var6 = 1231;
      } else {
         var6 = 1237;
      }

      int var18 = Arrays.hashCode(this.t);
      int var17 = this.f;
      short var7;
      if(this.u) {
         var7 = 1231;
      } else {
         var7 = 1237;
      }

      int var21 = this.d;
      int var20 = Arrays.hashCode(this.v);
      int var22 = this.i;
      int var19 = Arrays.hashCode(this.r);
      int var23 = this.k;
      if(!this.j) {
         var8 = 1237;
      }

      return ((((((var7 + (((var6 + ((var5 + ((((((var4 + (var3 + (var2 + (var1 + ((var10 + 31) * 31 + var9) * 31) * 31) * 31) * 31) * 31 + var11) * 31 + var12) * 31 + var15) * 31 + var13) * 31 + var14) * 31) * 31 + var16) * 31) * 31 + var18) * 31 + var17) * 31) * 31 + var21) * 31 + var20) * 31 + var22) * 31 + var19) * 31 + var23) * 31 + var8;
   }

   public String toString() {
      return "PictureParameterSet{\n       entropy_coding_mode_flag=" + this.a + ",\n       num_ref_idx_l0_active_minus1=" + this.b + ",\n       num_ref_idx_l1_active_minus1=" + this.c + ",\n       slice_group_change_rate_minus1=" + this.d + ",\n       pic_parameter_set_id=" + this.e + ",\n       seq_parameter_set_id=" + this.f + ",\n       pic_order_present_flag=" + this.g + ",\n       num_slice_groups_minus1=" + this.h + ",\n       slice_group_map_type=" + this.i + ",\n       weighted_pred_flag=" + this.j + ",\n       weighted_bipred_idc=" + this.k + ",\n       pic_init_qp_minus26=" + this.l + ",\n       pic_init_qs_minus26=" + this.m + ",\n       chroma_qp_index_offset=" + this.n + ",\n       deblocking_filter_control_present_flag=" + this.o + ",\n       constrained_intra_pred_flag=" + this.p + ",\n       redundant_pic_cnt_present_flag=" + this.q + ",\n       top_left=" + this.r + ",\n       bottom_right=" + this.s + ",\n       run_length_minus1=" + this.t + ",\n       slice_group_change_direction_flag=" + this.u + ",\n       slice_group_id=" + this.v + ",\n       extended=" + this.w + '}';
   }

   public static class a {
      public boolean a;
      public g b = new g();
      public int c;
      public boolean[] d;

      public String toString() {
         return "PPSExt{transform_8x8_mode_flag=" + this.a + ", scalindMatrix=" + this.b + ", second_chroma_qp_index_offset=" + this.c + ", pic_scaling_list_present_flag=" + this.d + '}';
      }
   }
}
