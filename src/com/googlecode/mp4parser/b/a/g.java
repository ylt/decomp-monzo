package com.googlecode.mp4parser.b.a;

import java.util.Arrays;
import java.util.List;

public class g {
   public f[] a;
   public f[] b;

   public String toString() {
      Object var2 = null;
      StringBuilder var3 = new StringBuilder("ScalingMatrix{ScalingList4x4=");
      List var1;
      if(this.a == null) {
         var1 = null;
      } else {
         var1 = Arrays.asList(this.a);
      }

      var3 = var3.append(var1).append("\n").append(", ScalingList8x8=");
      if(this.b == null) {
         var1 = (List)var2;
      } else {
         var1 = Arrays.asList(this.b);
      }

      return var3.append(var1).append("\n").append('}').toString();
   }
}
