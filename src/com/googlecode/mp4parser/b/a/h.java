package com.googlecode.mp4parser.b.a;

import java.io.IOException;
import java.io.InputStream;

public class h extends b {
   public boolean A;
   public int B;
   public int C;
   public int D;
   public boolean E;
   public boolean F;
   public boolean G;
   public int H;
   public int I;
   public int J;
   public int K;
   public int[] L;
   public i M;
   public g N;
   public int O;
   public int a;
   public boolean b;
   public boolean c;
   public boolean d;
   public int e;
   public boolean f;
   public boolean g;
   public boolean h;
   public c i;
   public int j;
   public int k;
   public int l;
   public int m;
   public int n;
   public int o;
   public boolean p;
   public int q;
   public long r;
   public boolean s;
   public boolean t;
   public boolean u;
   public boolean v;
   public boolean w;
   public boolean x;
   public int y;
   public int z;

   public static h a(InputStream var0) throws IOException {
      com.googlecode.mp4parser.b.b.b var2 = new com.googlecode.mp4parser.b.b.b(var0);
      h var3 = new h();
      var3.q = (int)var2.a(8, "SPS: profile_idc");
      var3.s = var2.c("SPS: constraint_set_0_flag");
      var3.t = var2.c("SPS: constraint_set_1_flag");
      var3.u = var2.c("SPS: constraint_set_2_flag");
      var3.v = var2.c("SPS: constraint_set_3_flag");
      var3.w = var2.c("SPS: constraint_set_4_flag");
      var3.x = var2.c("SPS: constraint_set_5_flag");
      var3.r = var2.a(2, "SPS: reserved_zero_2bits");
      var3.y = (int)var2.a(8, "SPS: level_idc");
      var3.z = var2.a("SPS: seq_parameter_set_id");
      if(var3.q != 100 && var3.q != 110 && var3.q != 122 && var3.q != 144) {
         var3.i = c.b;
      } else {
         var3.i = c.a(var2.a("SPS: chroma_format_idc"));
         if(var3.i == c.d) {
            var3.A = var2.c("SPS: residual_color_transform_flag");
         }

         var3.n = var2.a("SPS: bit_depth_luma_minus8");
         var3.o = var2.a("SPS: bit_depth_chroma_minus8");
         var3.p = var2.c("SPS: qpprime_y_zero_transform_bypass_flag");
         if(var2.c("SPS: seq_scaling_matrix_present_lag")) {
            a(var2, var3);
         }
      }

      var3.j = var2.a("SPS: log2_max_frame_num_minus4");
      var3.a = var2.a("SPS: pic_order_cnt_type");
      if(var3.a == 0) {
         var3.k = var2.a("SPS: log2_max_pic_order_cnt_lsb_minus4");
      } else if(var3.a == 1) {
         var3.c = var2.c("SPS: delta_pic_order_always_zero_flag");
         var3.B = var2.b("SPS: offset_for_non_ref_pic");
         var3.C = var2.b("SPS: offset_for_top_to_bottom_field");
         var3.O = var2.a("SPS: num_ref_frames_in_pic_order_cnt_cycle");
         var3.L = new int[var3.O];

         for(int var1 = 0; var1 < var3.O; ++var1) {
            var3.L[var1] = var2.b("SPS: offsetForRefFrame [" + var1 + "]");
         }
      }

      var3.D = var2.a("SPS: num_ref_frames");
      var3.E = var2.c("SPS: gaps_in_frame_num_value_allowed_flag");
      var3.m = var2.a("SPS: pic_width_in_mbs_minus1");
      var3.l = var2.a("SPS: pic_height_in_map_units_minus1");
      var3.F = var2.c("SPS: frame_mbs_only_flag");
      if(!var3.F) {
         var3.g = var2.c("SPS: mb_adaptive_frame_field_flag");
      }

      var3.h = var2.c("SPS: direct_8x8_inference_flag");
      var3.G = var2.c("SPS: frame_cropping_flag");
      if(var3.G) {
         var3.H = var2.a("SPS: frame_crop_left_offset");
         var3.I = var2.a("SPS: frame_crop_right_offset");
         var3.J = var2.a("SPS: frame_crop_top_offset");
         var3.K = var2.a("SPS: frame_crop_bottom_offset");
      }

      if(var2.c("SPS: vui_parameters_present_flag")) {
         var3.M = a(var2);
      }

      var2.d();
      return var3;
   }

   private static i a(com.googlecode.mp4parser.b.b.b var0) throws IOException {
      i var3 = new i();
      var3.a = var0.c("VUI: aspect_ratio_info_present_flag");
      if(var3.a) {
         var3.y = a.a((int)var0.a(8, "VUI: aspect_ratio"));
         if(var3.y == a.a) {
            var3.b = (int)var0.a(16, "VUI: sar_width");
            var3.c = (int)var0.a(16, "VUI: sar_height");
         }
      }

      var3.d = var0.c("VUI: overscan_info_present_flag");
      if(var3.d) {
         var3.e = var0.c("VUI: overscan_appropriate_flag");
      }

      var3.f = var0.c("VUI: video_signal_type_present_flag");
      if(var3.f) {
         var3.g = (int)var0.a(3, "VUI: video_format");
         var3.h = var0.c("VUI: video_full_range_flag");
         var3.i = var0.c("VUI: colour_description_present_flag");
         if(var3.i) {
            var3.j = (int)var0.a(8, "VUI: colour_primaries");
            var3.k = (int)var0.a(8, "VUI: transfer_characteristics");
            var3.l = (int)var0.a(8, "VUI: matrix_coefficients");
         }
      }

      var3.m = var0.c("VUI: chroma_loc_info_present_flag");
      if(var3.m) {
         var3.n = var0.a("VUI chroma_sample_loc_type_top_field");
         var3.o = var0.a("VUI chroma_sample_loc_type_bottom_field");
      }

      var3.p = var0.c("VUI: timing_info_present_flag");
      if(var3.p) {
         var3.q = (int)var0.a(32, "VUI: num_units_in_tick");
         var3.r = (int)var0.a(32, "VUI: time_scale");
         var3.s = var0.c("VUI: fixed_frame_rate_flag");
      }

      boolean var2 = var0.c("VUI: nal_hrd_parameters_present_flag");
      if(var2) {
         var3.v = b(var0);
      }

      boolean var1 = var0.c("VUI: vcl_hrd_parameters_present_flag");
      if(var1) {
         var3.w = b(var0);
      }

      if(var2 || var1) {
         var3.t = var0.c("VUI: low_delay_hrd_flag");
      }

      var3.u = var0.c("VUI: pic_struct_present_flag");
      if(var0.c("VUI: bitstream_restriction_flag")) {
         var3.x = new i.a();
         var3.x.a = var0.c("VUI: motion_vectors_over_pic_boundaries_flag");
         var3.x.b = var0.a("VUI max_bytes_per_pic_denom");
         var3.x.c = var0.a("VUI max_bits_per_mb_denom");
         var3.x.d = var0.a("VUI log2_max_mv_length_horizontal");
         var3.x.e = var0.a("VUI log2_max_mv_length_vertical");
         var3.x.f = var0.a("VUI num_reorder_frames");
         var3.x.g = var0.a("VUI max_dec_frame_buffering");
      }

      return var3;
   }

   private static void a(com.googlecode.mp4parser.b.b.b var0, h var1) throws IOException {
      var1.N = new g();

      for(int var2 = 0; var2 < 8; ++var2) {
         if(var0.c("SPS: seqScalingListPresentFlag")) {
            var1.N.a = new f[8];
            var1.N.b = new f[8];
            if(var2 < 6) {
               var1.N.a[var2] = f.a(var0, 16);
            } else {
               var1.N.b[var2 - 6] = f.a(var0, 64);
            }
         }
      }

   }

   private static d b(com.googlecode.mp4parser.b.b.b var0) throws IOException {
      d var2 = new d();
      var2.a = var0.a("SPS: cpb_cnt_minus1");
      var2.b = (int)var0.a(4, "HRD: bit_rate_scale");
      var2.c = (int)var0.a(4, "HRD: cpb_size_scale");
      var2.d = new int[var2.a + 1];
      var2.e = new int[var2.a + 1];
      var2.f = new boolean[var2.a + 1];

      for(int var1 = 0; var1 <= var2.a; ++var1) {
         var2.d[var1] = var0.a("HRD: bit_rate_value_minus1");
         var2.e[var1] = var0.a("HRD: cpb_size_value_minus1");
         var2.f[var1] = var0.c("HRD: cbr_flag");
      }

      var2.g = (int)var0.a(5, "HRD: initial_cpb_removal_delay_length_minus1");
      var2.h = (int)var0.a(5, "HRD: cpb_removal_delay_length_minus1");
      var2.i = (int)var0.a(5, "HRD: dpb_output_delay_length_minus1");
      var2.j = (int)var0.a(5, "HRD: time_offset_length");
      return var2;
   }

   public String toString() {
      return "SeqParameterSet{ \n        pic_order_cnt_type=" + this.a + ", \n        field_pic_flag=" + this.b + ", \n        delta_pic_order_always_zero_flag=" + this.c + ", \n        weighted_pred_flag=" + this.d + ", \n        weighted_bipred_idc=" + this.e + ", \n        entropy_coding_mode_flag=" + this.f + ", \n        mb_adaptive_frame_field_flag=" + this.g + ", \n        direct_8x8_inference_flag=" + this.h + ", \n        chroma_format_idc=" + this.i + ", \n        log2_max_frame_num_minus4=" + this.j + ", \n        log2_max_pic_order_cnt_lsb_minus4=" + this.k + ", \n        pic_height_in_map_units_minus1=" + this.l + ", \n        pic_width_in_mbs_minus1=" + this.m + ", \n        bit_depth_luma_minus8=" + this.n + ", \n        bit_depth_chroma_minus8=" + this.o + ", \n        qpprime_y_zero_transform_bypass_flag=" + this.p + ", \n        profile_idc=" + this.q + ", \n        constraint_set_0_flag=" + this.s + ", \n        constraint_set_1_flag=" + this.t + ", \n        constraint_set_2_flag=" + this.u + ", \n        constraint_set_3_flag=" + this.v + ", \n        constraint_set_4_flag=" + this.w + ", \n        constraint_set_5_flag=" + this.x + ", \n        level_idc=" + this.y + ", \n        seq_parameter_set_id=" + this.z + ", \n        residual_color_transform_flag=" + this.A + ", \n        offset_for_non_ref_pic=" + this.B + ", \n        offset_for_top_to_bottom_field=" + this.C + ", \n        num_ref_frames=" + this.D + ", \n        gaps_in_frame_num_value_allowed_flag=" + this.E + ", \n        frame_mbs_only_flag=" + this.F + ", \n        frame_cropping_flag=" + this.G + ", \n        frame_crop_left_offset=" + this.H + ", \n        frame_crop_right_offset=" + this.I + ", \n        frame_crop_top_offset=" + this.J + ", \n        frame_crop_bottom_offset=" + this.K + ", \n        offsetForRefFrame=" + this.L + ", \n        vuiParams=" + this.M + ", \n        scalingMatrix=" + this.N + ", \n        num_ref_frames_in_pic_order_cnt_cycle=" + this.O + '}';
   }
}
