package com.googlecode.mp4parser.b.a;

import java.io.IOException;

public class f {
   public int[] a;
   public boolean b;

   public static f a(com.googlecode.mp4parser.b.b.b var0, int var1) throws IOException {
      int var5 = 8;
      f var8 = new f();
      var8.a = new int[var1];
      int var4 = 0;

      int var2;
      for(int var3 = 8; var4 < var1; var5 = var2) {
         var2 = var5;
         if(var5 != 0) {
            var2 = (var0.b("deltaScale") + var3 + 256) % 256;
            boolean var6;
            if(var4 == 0 && var2 == 0) {
               var6 = true;
            } else {
               var6 = false;
            }

            var8.b = var6;
         }

         int[] var7 = var8.a;
         if(var2 != 0) {
            var3 = var2;
         }

         var7[var4] = var3;
         var3 = var8.a[var4];
         ++var4;
      }

      return var8;
   }

   public String toString() {
      return "ScalingList{scalingList=" + this.a + ", useDefaultScalingMatrixFlag=" + this.b + '}';
   }
}
