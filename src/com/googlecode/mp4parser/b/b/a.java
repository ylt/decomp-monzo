package com.googlecode.mp4parser.b.b;

import java.io.IOException;
import java.io.InputStream;

public class a {
   protected static int b;
   int a;
   protected com.googlecode.mp4parser.b.a c = new com.googlecode.mp4parser.b.a(50);
   private InputStream d;
   private int e;
   private int f;

   public a(InputStream var1) throws IOException {
      this.d = var1;
      this.e = var1.read();
      this.f = var1.read();
   }

   private void d() throws IOException {
      this.e = this.f;
      this.f = this.d.read();
      this.a = 0;
   }

   public int a() throws IOException {
      int var2 = -1;
      if(this.a == 8) {
         this.d();
         if(this.e == -1) {
            return var2;
         }
      }

      var2 = this.e >> 7 - this.a & 1;
      ++this.a;
      com.googlecode.mp4parser.b.a var3 = this.c;
      char var1;
      if(var2 == 0) {
         var1 = 48;
      } else {
         var1 = 49;
      }

      var3.a(var1);
      ++b;
      return var2;
   }

   public long a(int var1) throws IOException {
      if(var1 > 64) {
         throw new IllegalArgumentException("Can not readByte more then 64 bit");
      } else {
         long var3 = 0L;

         for(int var2 = 0; var2 < var1; ++var2) {
            var3 = var3 << 1 | (long)this.a();
         }

         return var3;
      }
   }

   public boolean b() throws IOException {
      boolean var3 = true;
      if(this.a == 8) {
         this.d();
      }

      int var1 = 1 << 8 - this.a - 1;
      boolean var4;
      if(((var1 << 1) - 1 & this.e) == var1) {
         var4 = true;
      } else {
         var4 = false;
      }

      boolean var2;
      if(this.e != -1) {
         var2 = var3;
         if(this.f != -1) {
            return var2;
         }

         var2 = var3;
         if(!var4) {
            return var2;
         }
      }

      var2 = false;
      return var2;
   }

   public long c() throws IOException {
      return this.a(8 - this.a);
   }
}
