package com.googlecode.mp4parser.b.b;

import java.io.IOException;
import java.io.InputStream;

public class b extends a {
   public b(InputStream var1) throws IOException {
      super(var1);
   }

   private void a(String var1, String var2) {
      byte var4 = 0;
      StringBuilder var8 = new StringBuilder();
      String var7 = String.valueOf(a.b - this.c.b());
      int var5 = var7.length();
      var8.append("@" + var7);

      int var3;
      for(var3 = 0; var3 < 8 - var5; ++var3) {
         var8.append(' ');
      }

      var8.append(var1);
      var5 = var8.length();
      int var6 = this.c.b();

      for(var3 = var4; var3 < 100 - var5 - var6; ++var3) {
         var8.append(' ');
      }

      var8.append(this.c);
      var8.append(" (" + var2 + ")");
      this.c.a();
      com.googlecode.mp4parser.b.b.a(var8.toString());
   }

   private int e() throws IOException {
      int var2 = 0;

      int var1;
      for(var1 = 0; this.a() == 0; ++var1) {
         ;
      }

      if(var1 > 0) {
         long var3 = this.a(var1);
         var2 = (int)((long)((1 << var1) - 1) + var3);
      }

      return var2;
   }

   public int a(String var1) throws IOException {
      int var2 = this.e();
      this.a(var1, String.valueOf(var2));
      return var2;
   }

   public long a(int var1, String var2) throws IOException {
      long var3 = this.a(var1);
      this.a(var2, String.valueOf(var3));
      return var3;
   }

   public int b(int var1, String var2) throws IOException {
      return (int)this.a(var1, var2);
   }

   public int b(String var1) throws IOException {
      int var2 = this.e();
      var2 = ((var2 & 1) + (var2 >> 1)) * (((var2 & 1) << 1) - 1);
      this.a(var1, String.valueOf(var2));
      return var2;
   }

   public boolean c(String var1) throws IOException {
      boolean var2;
      if(this.a() == 0) {
         var2 = false;
      } else {
         var2 = true;
      }

      String var3;
      if(var2) {
         var3 = "1";
      } else {
         var3 = "0";
      }

      this.a(var1, var3);
      return var2;
   }

   public void d() throws IOException {
      this.a();
      this.c();
   }
}
