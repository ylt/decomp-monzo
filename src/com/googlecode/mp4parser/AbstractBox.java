package com.googlecode.mp4parser;

import com.coremedia.iso.g;
import com.googlecode.mp4parser.c.f;
import com.googlecode.mp4parser.c.j;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;

public abstract class AbstractBox implements com.coremedia.iso.boxes.a {
   // $FF: synthetic field
   static final boolean $assertionsDisabled;
   private static f LOG;
   private ByteBuffer content;
   b dataSource;
   private ByteBuffer deadBytes = null;
   boolean isParsed;
   long offset;
   private com.coremedia.iso.boxes.b parent;
   protected String type;
   private byte[] userType;

   static {
      boolean var0;
      if(!AbstractBox.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      $assertionsDisabled = var0;
      LOG = f.a(AbstractBox.class);
   }

   protected AbstractBox(String var1) {
      this.type = var1;
      this.isParsed = true;
   }

   protected AbstractBox(String var1, byte[] var2) {
      this.type = var1;
      this.userType = var2;
      this.isParsed = true;
   }

   private void getHeader(ByteBuffer var1) {
      if(this.isSmallBox()) {
         g.b(var1, this.getSize());
         var1.put(com.coremedia.iso.d.a(this.getType()));
      } else {
         g.b(var1, 1L);
         var1.put(com.coremedia.iso.d.a(this.getType()));
         g.a(var1, this.getSize());
      }

      if("uuid".equals(this.getType())) {
         var1.put(this.getUserType());
      }

   }

   private boolean isSmallBox() {
      byte var1 = 8;
      if("uuid".equals(this.getType())) {
         var1 = 24;
      }

      boolean var3;
      if(this.isParsed) {
         long var4 = this.getContentSize();
         int var2;
         if(this.deadBytes != null) {
            var2 = this.deadBytes.limit();
         } else {
            var2 = 0;
         }

         if(var4 + (long)var2 + (long)var1 < 4294967296L) {
            var3 = true;
         } else {
            var3 = false;
         }
      } else if((long)(var1 + this.content.limit()) < 4294967296L) {
         var3 = true;
      } else {
         var3 = false;
      }

      return var3;
   }

   private boolean verify(ByteBuffer var1) {
      boolean var7 = false;
      long var8 = this.getContentSize();
      int var4;
      if(this.deadBytes != null) {
         var4 = this.deadBytes.limit();
      } else {
         var4 = 0;
      }

      ByteBuffer var10 = ByteBuffer.allocate(com.googlecode.mp4parser.c.b.a(var8 + (long)var4));
      this.getContent(var10);
      if(this.deadBytes != null) {
         this.deadBytes.rewind();

         while(this.deadBytes.remaining() > 0) {
            var10.put(this.deadBytes);
         }
      }

      var1.rewind();
      var10.rewind();
      if(var1.remaining() != var10.remaining()) {
         System.err.print(this.getType() + ": remaining differs " + var1.remaining() + " vs. " + var10.remaining());
         LOG.c(this.getType() + ": remaining differs " + var1.remaining() + " vs. " + var10.remaining());
      } else {
         int var6 = var1.position();
         var4 = var1.limit() - 1;

         for(int var5 = var10.limit() - 1; var4 >= var6; --var5) {
            byte var3 = var1.get(var4);
            byte var2 = var10.get(var5);
            if(var3 != var2) {
               LOG.c(String.format("%s: buffers differ at %d: %2X/%2X", new Object[]{this.getType(), Integer.valueOf(var4), Byte.valueOf(var3), Byte.valueOf(var2)}));
               byte[] var11 = new byte[var1.remaining()];
               byte[] var12 = new byte[var10.remaining()];
               var1.get(var11);
               var10.get(var12);
               System.err.println("original      : " + com.coremedia.iso.c.a(var11, 4));
               System.err.println("reconstructed : " + com.coremedia.iso.c.a(var12, 4));
               return var7;
            }

            --var4;
         }

         var7 = true;
      }

      return var7;
   }

   protected abstract void _parseDetails(ByteBuffer var1);

   public void getBox(WritableByteChannel var1) throws IOException {
      byte var3 = 16;
      ByteBuffer var4;
      if(this.isParsed) {
         var4 = ByteBuffer.allocate(com.googlecode.mp4parser.c.b.a(this.getSize()));
         this.getHeader(var4);
         this.getContent(var4);
         if(this.deadBytes != null) {
            this.deadBytes.rewind();

            while(this.deadBytes.remaining() > 0) {
               var4.put(this.deadBytes);
            }
         }

         var1.write((ByteBuffer)var4.rewind());
      } else {
         byte var2;
         if(this.isSmallBox()) {
            var2 = 8;
         } else {
            var2 = 16;
         }

         if(!"uuid".equals(this.getType())) {
            var3 = 0;
         }

         var4 = ByteBuffer.allocate(var2 + var3);
         this.getHeader(var4);
         var1.write((ByteBuffer)var4.rewind());
         var1.write((ByteBuffer)this.content.position(0));
      }

   }

   protected abstract void getContent(ByteBuffer var1);

   protected abstract long getContentSize();

   public long getOffset() {
      return this.offset;
   }

   public com.coremedia.iso.boxes.b getParent() {
      return this.parent;
   }

   public String getPath() {
      return j.a(this);
   }

   public long getSize() {
      byte var3 = 0;
      int var1;
      long var4;
      if(this.isParsed) {
         var4 = this.getContentSize();
      } else {
         if(this.content != null) {
            var1 = this.content.limit();
         } else {
            var1 = 0;
         }

         var4 = (long)var1;
      }

      byte var8;
      if(var4 >= 4294967288L) {
         var8 = 8;
      } else {
         var8 = 0;
      }

      byte var2;
      if("uuid".equals(this.getType())) {
         var2 = 16;
      } else {
         var2 = 0;
      }

      long var6 = (long)(var2 + var8 + 8);
      if(this.deadBytes == null) {
         var1 = var3;
      } else {
         var1 = this.deadBytes.limit();
      }

      return (long)var1 + var4 + var6;
   }

   public String getType() {
      return this.type;
   }

   public byte[] getUserType() {
      return this.userType;
   }

   public boolean isParsed() {
      return this.isParsed;
   }

   public void parse(b var1, ByteBuffer var2, long var3, com.coremedia.iso.b var5) throws IOException {
      this.offset = var1.b() - (long)var2.remaining();
      this.dataSource = var1;
      this.content = ByteBuffer.allocate(com.googlecode.mp4parser.c.b.a(var3));

      while(this.content.remaining() > 0) {
         var1.a(this.content);
      }

      this.content.position(0);
      this.isParsed = false;
   }

   public final void parseDetails() {
      synchronized(this){}

      try {
         f var1 = LOG;
         StringBuilder var2 = new StringBuilder("parsing details of ");
         var1.a(var2.append(this.getType()).toString());
         if(this.content != null) {
            ByteBuffer var5 = this.content;
            this.isParsed = true;
            var5.rewind();
            this._parseDetails(var5);
            if(var5.remaining() > 0) {
               this.deadBytes = var5.slice();
            }

            this.content = null;
            if(!$assertionsDisabled && !this.verify(var5)) {
               AssertionError var6 = new AssertionError();
               throw var6;
            }
         }
      } finally {
         ;
      }

   }

   public void setParent(com.coremedia.iso.boxes.b var1) {
      this.parent = var1;
   }
}
