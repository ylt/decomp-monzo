package com.googlecode.mp4parser.authoring.builder;

import com.googlecode.mp4parser.authoring.f;
import com.googlecode.mp4parser.c.i;
import java.util.Arrays;

public class a implements c {
   private double a;

   public a(double var1) {
      this.a = var1;
   }

   public long[] a(f var1) {
      long var8 = var1.k().b();
      long var12 = (long)(this.a * (double)var8);
      long[] var20 = new long[0];
      long[] var23 = var1.b();
      long[] var21 = var1.i();
      int var6;
      long var10;
      long[] var24;
      if(var23 != null) {
         long[] var22 = new long[var23.length];
         var8 = 0L;
         long var14 = var1.e();

         for(var6 = 0; var6 < var21.length; ++var6) {
            int var7 = Arrays.binarySearch(var23, (long)var6 + 1L);
            if(var7 >= 0) {
               var22[var7] = var8;
            }

            var8 += var21[var6];
         }

         var10 = 0L;
         var6 = 0;

         for(var24 = var20; var6 < var22.length - 1; var10 = var8) {
            long var18 = var22[var6];
            long var16 = var22[var6 + 1];
            var20 = var24;
            var8 = var10;
            if(var10 <= var16) {
               var20 = var24;
               var8 = var10;
               if(Math.abs(var18 - var10) < Math.abs(var16 - var10)) {
                  var20 = i.a(var24, new long[]{var23[var6]});
                  var8 = var22[var6] + var12;
               }
            }

            ++var6;
            var24 = var20;
         }

         var20 = var24;
         if(var14 - var22[var22.length - 1] > var12 / 2L) {
            var20 = i.a(var24, new long[]{var23[var22.length - 1]});
         }
      } else {
         double var2 = 0.0D;
         var24 = new long[]{1L};

         for(var6 = 1; var6 < var21.length; var24 = var20) {
            double var4 = var2 + (double)var21[var6] / (double)var8;
            var20 = var24;
            var2 = var4;
            if(var4 >= this.a) {
               var20 = var24;
               if(var6 > 0) {
                  var20 = i.a(var24, new long[]{(long)(var6 + 1)});
               }

               var2 = 0.0D;
            }

            ++var6;
         }

         var20 = var24;
         if(var2 < this.a) {
            var20 = var24;
            if(var24.length > 1) {
               var10 = (long)(var21.length + 1);
               var12 = var24[var24.length - 2];
               var6 = var24.length;
               var8 = var24[var24.length - 2];
               var24[var6 - 1] = (var10 - var12) / 2L + var8;
               var20 = var24;
            }
         }
      }

      return var20;
   }
}
