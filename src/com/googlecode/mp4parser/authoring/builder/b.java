package com.googlecode.mp4parser.authoring.builder;

import com.googlecode.mp4parser.authoring.f;
import java.util.List;
import java.util.logging.Logger;

public class b {
   // $FF: synthetic field
   static final boolean a;
   private static final Logger b;

   static {
      boolean var0;
      if(!b.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      a = var0;
      b = Logger.getLogger(b.class.getName());
   }

   protected List a(long var1, long var3, f var5) {
      return var5.h().subList(com.googlecode.mp4parser.c.b.a(var1) - 1, com.googlecode.mp4parser.c.b.a(var3) - 1);
   }
}
