package com.googlecode.mp4parser.authoring.builder;

import com.coremedia.iso.d;
import com.coremedia.iso.boxes.ChunkOffsetBox;
import com.coremedia.iso.boxes.CompositionTimeToSample;
import com.coremedia.iso.boxes.DataEntryUrlBox;
import com.coremedia.iso.boxes.DataInformationBox;
import com.coremedia.iso.boxes.DataReferenceBox;
import com.coremedia.iso.boxes.EditBox;
import com.coremedia.iso.boxes.EditListBox;
import com.coremedia.iso.boxes.FileTypeBox;
import com.coremedia.iso.boxes.HandlerBox;
import com.coremedia.iso.boxes.HintMediaHeaderBox;
import com.coremedia.iso.boxes.MediaBox;
import com.coremedia.iso.boxes.MediaHeaderBox;
import com.coremedia.iso.boxes.MediaInformationBox;
import com.coremedia.iso.boxes.MovieBox;
import com.coremedia.iso.boxes.MovieHeaderBox;
import com.coremedia.iso.boxes.NullMediaHeaderBox;
import com.coremedia.iso.boxes.SampleDependencyTypeBox;
import com.coremedia.iso.boxes.SampleSizeBox;
import com.coremedia.iso.boxes.SampleTableBox;
import com.coremedia.iso.boxes.SampleToChunkBox;
import com.coremedia.iso.boxes.SoundMediaHeaderBox;
import com.coremedia.iso.boxes.StaticChunkOffsetBox;
import com.coremedia.iso.boxes.SubtitleMediaHeaderBox;
import com.coremedia.iso.boxes.SyncSampleBox;
import com.coremedia.iso.boxes.TimeToSampleBox;
import com.coremedia.iso.boxes.TrackBox;
import com.coremedia.iso.boxes.TrackHeaderBox;
import com.coremedia.iso.boxes.VideoMediaHeaderBox;
import com.googlecode.mp4parser.authoring.e;
import com.googlecode.mp4parser.boxes.dece.SampleEncryptionBox;
import com.googlecode.mp4parser.boxes.mp4.samplegrouping.SampleGroupDescriptionBox;
import com.googlecode.mp4parser.boxes.mp4.samplegrouping.SampleToGroupBox;
import com.googlecode.mp4parser.c.f;
import com.googlecode.mp4parser.c.g;
import com.googlecode.mp4parser.c.i;
import com.googlecode.mp4parser.c.j;
import com.mp4parser.iso14496.part12.SampleAuxiliaryInformationOffsetsBox;
import com.mp4parser.iso14496.part12.SampleAuxiliaryInformationSizesBox;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

public class DefaultMp4Builder {
   // $FF: synthetic field
   static final boolean e;
   private static f f;
   Map a = new HashMap();
   Set b = new HashSet();
   HashMap c = new HashMap();
   HashMap d = new HashMap();
   private c g;

   static {
      boolean var0;
      if(!DefaultMp4Builder.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      e = var0;
      f = f.a(DefaultMp4Builder.class);
   }

   private static long a(int[] var0) {
      long var3 = 0L;
      int var2 = var0.length;

      for(int var1 = 0; var1 < var2; ++var1) {
         var3 += (long)var0[var1];
      }

      return var3;
   }

   private static long a(long[] var0) {
      long var3 = 0L;
      int var2 = var0.length;

      for(int var1 = 0; var1 < var2; ++var1) {
         var3 += var0[var1];
      }

      return var3;
   }

   protected MovieBox a(com.googlecode.mp4parser.authoring.c var1, Map var2) {
      MovieBox var11 = new MovieBox();
      MovieHeaderBox var12 = new MovieHeaderBox();
      var12.setCreationTime(new Date());
      var12.setModificationTime(new Date());
      var12.setMatrix(var1.d());
      long var9 = this.d(var1);
      Iterator var13 = var1.a().iterator();
      long var7 = 0L;

      long var5;
      Iterator var18;
      while(var13.hasNext()) {
         com.googlecode.mp4parser.authoring.f var14 = (com.googlecode.mp4parser.authoring.f)var13.next();
         if(var14.f() != null && !var14.f().isEmpty()) {
            double var3 = 0.0D;

            for(var18 = var14.f().iterator(); var18.hasNext(); var3 += (double)((long)((com.googlecode.mp4parser.authoring.b)var18.next()).b())) {
               ;
            }

            var5 = (long)((double)var9 * var3);
         } else {
            var5 = var14.e() * var9 / var14.k().b();
         }

         if(var5 > var7) {
            var7 = var5;
         }
      }

      var12.setDuration(var7);
      var12.setTimescale(var9);
      var18 = var1.a().iterator();
      var5 = 0L;

      while(var18.hasNext()) {
         com.googlecode.mp4parser.authoring.f var17 = (com.googlecode.mp4parser.authoring.f)var18.next();
         if(var5 < var17.k().f()) {
            var5 = var17.k().f();
         }
      }

      var12.setNextTrackId(1L + var5);
      var11.addBox(var12);
      Iterator var16 = var1.a().iterator();

      while(var16.hasNext()) {
         var11.addBox(this.a((com.googlecode.mp4parser.authoring.f)var16.next(), var1, var2));
      }

      com.coremedia.iso.boxes.a var15 = this.c(var1);
      if(var15 != null) {
         var11.addBox(var15);
      }

      return var11;
   }

   protected TrackBox a(com.googlecode.mp4parser.authoring.f var1, com.googlecode.mp4parser.authoring.c var2, Map var3) {
      TrackBox var6 = new TrackBox();
      TrackHeaderBox var7 = new TrackHeaderBox();
      var7.setEnabled(true);
      var7.setInMovie(true);
      var7.setMatrix(var1.k().j());
      var7.setAlternateGroup(var1.k().i());
      var7.setCreationTime(var1.k().c());
      if(var1.f() != null && !var1.f().isEmpty()) {
         Iterator var8 = var1.f().iterator();

         long var4;
         for(var4 = 0L; var8.hasNext(); var4 += (long)((com.googlecode.mp4parser.authoring.b)var8.next()).b()) {
            ;
         }

         var7.setDuration(var1.k().b() * var4);
      } else {
         var7.setDuration(var1.e() * this.d(var2) / var1.k().b());
      }

      var7.setHeight(var1.k().e());
      var7.setWidth(var1.k().d());
      var7.setLayer(var1.k().g());
      var7.setModificationTime(new Date());
      var7.setTrackId(var1.k().f());
      var7.setVolume(var1.k().h());
      var6.addBox(var7);
      var6.addBox(this.a(var1, var2));
      MediaBox var12 = new MediaBox();
      var6.addBox(var12);
      MediaHeaderBox var13 = new MediaHeaderBox();
      var13.setCreationTime(var1.k().c());
      var13.setDuration(var1.e());
      var13.setTimescale(var1.k().b());
      var13.setLanguage(var1.k().a());
      var12.addBox(var13);
      HandlerBox var14 = new HandlerBox();
      var12.addBox(var14);
      var14.setHandlerType(var1.l());
      MediaInformationBox var15 = new MediaInformationBox();
      if(var1.l().equals("vide")) {
         var15.addBox(new VideoMediaHeaderBox());
      } else if(var1.l().equals("soun")) {
         var15.addBox(new SoundMediaHeaderBox());
      } else if(var1.l().equals("text")) {
         var15.addBox(new NullMediaHeaderBox());
      } else if(var1.l().equals("subt")) {
         var15.addBox(new SubtitleMediaHeaderBox());
      } else if(var1.l().equals("hint")) {
         var15.addBox(new HintMediaHeaderBox());
      } else if(var1.l().equals("sbtl")) {
         var15.addBox(new NullMediaHeaderBox());
      }

      DataInformationBox var10 = new DataInformationBox();
      DataReferenceBox var9 = new DataReferenceBox();
      var10.addBox(var9);
      DataEntryUrlBox var11 = new DataEntryUrlBox();
      var11.setFlags(1);
      var9.addBox(var11);
      var15.addBox(var10);
      var15.addBox(this.b(var1, var2, var3));
      var12.addBox(var15);
      f.a("done with trak for track_" + var1.k().f());
      return var6;
   }

   protected com.coremedia.iso.boxes.a a(com.googlecode.mp4parser.authoring.f var1, com.googlecode.mp4parser.authoring.c var2) {
      EditBox var7;
      if(var1.f() != null && var1.f().size() > 0) {
         EditListBox var3 = new EditListBox();
         var3.setVersion(0);
         ArrayList var5 = new ArrayList();
         Iterator var4 = var1.f().iterator();

         while(var4.hasNext()) {
            com.googlecode.mp4parser.authoring.b var6 = (com.googlecode.mp4parser.authoring.b)var4.next();
            var5.add(new EditListBox.a(var3, Math.round(var6.b() * (double)var2.c()), var6.c() * var1.k().b() / var6.a(), var6.d()));
         }

         var3.setEntries(var5);
         var7 = new EditBox();
         var7.addBox(var3);
      } else {
         var7 = null;
      }

      return var7;
   }

   public com.coremedia.iso.boxes.b a(com.googlecode.mp4parser.authoring.c var1) {
      if(this.g == null) {
         this.g = new a(2.0D);
      }

      f.a("Creating movie " + var1);
      Iterator var7 = var1.a().iterator();

      int var2;
      while(var7.hasNext()) {
         com.googlecode.mp4parser.authoring.f var5 = (com.googlecode.mp4parser.authoring.f)var7.next();
         List var8 = var5.h();
         this.a(var5, var8);
         long[] var6 = new long[var8.size()];

         for(var2 = 0; var2 < var6.length; ++var2) {
            var6[var2] = ((e)var8.get(var2)).a();
         }

         this.d.put(var5, var6);
      }

      com.googlecode.mp4parser.a var20 = new com.googlecode.mp4parser.a();
      var20.addBox(this.b(var1));
      HashMap var14 = new HashMap();
      Iterator var23 = var1.a().iterator();

      while(var23.hasNext()) {
         com.googlecode.mp4parser.authoring.f var17 = (com.googlecode.mp4parser.authoring.f)var23.next();
         var14.put(var17, this.a(var17));
      }

      MovieBox var18 = this.a((com.googlecode.mp4parser.authoring.c)var1, (Map)var14);
      var20.addBox(var18);
      List var19 = j.a((com.coremedia.iso.boxes.a)var18, "trak/mdia/minf/stbl/stsz");
      long var3 = 0L;

      for(Iterator var21 = var19.iterator(); var21.hasNext(); var3 += a(((SampleSizeBox)var21.next()).getSampleSizes())) {
         ;
      }

      f.a("About to create mdat");
      DefaultMp4Builder.InterleaveChunkMdat var11 = new DefaultMp4Builder.InterleaveChunkMdat(var1, var14, var3, (DefaultMp4Builder.InterleaveChunkMdat)null);
      var20.addBox(var11);
      f.a("mdat crated");
      var3 = var11.getDataOffset();
      Iterator var15 = this.a.values().iterator();

      long[] var12;
      while(var15.hasNext()) {
         var12 = ((StaticChunkOffsetBox)var15.next()).getChunkOffsets();

         for(var2 = 0; var2 < var12.length; ++var2) {
            var12[var2] += var3;
         }
      }

      var23 = this.b.iterator();

      while(var23.hasNext()) {
         SampleAuxiliaryInformationOffsetsBox var16 = (SampleAuxiliaryInformationOffsetsBox)var23.next();
         var3 = var16.getSize() + 44L;
         Object var13 = var16;

         while(true) {
            com.coremedia.iso.boxes.b var22 = ((com.coremedia.iso.boxes.a)var13).getParent();

            com.coremedia.iso.boxes.a var9;
            for(Iterator var10 = ((com.coremedia.iso.boxes.b)var22).getBoxes().iterator(); var10.hasNext(); var3 += var9.getSize()) {
               var9 = (com.coremedia.iso.boxes.a)var10.next();
               if(var9 == var13) {
                  break;
               }
            }

            if(!(var22 instanceof com.coremedia.iso.boxes.a)) {
               var12 = var16.getOffsets();

               for(var2 = 0; var2 < var12.length; ++var2) {
                  var12[var2] += var3;
               }

               var16.setOffsets(var12);
               break;
            }

            var13 = var22;
         }
      }

      return var20;
   }

   protected List a(com.googlecode.mp4parser.authoring.f var1, List var2) {
      return (List)this.c.put(var1, var2);
   }

   protected void a(com.googlecode.mp4parser.authoring.f var1, SampleTableBox var2) {
      if(var1.d() != null) {
         var2.addBox(var1.d());
      }

   }

   protected void a(com.googlecode.mp4parser.authoring.f var1, com.googlecode.mp4parser.authoring.c var2, Map var3, SampleTableBox var4) {
      if(this.a.get(var1) == null) {
         f.a("Calculating chunk offsets for track_" + var1.k().f());
         ArrayList var20 = new ArrayList(var3.keySet());
         Collections.sort(var20, new Comparator() {
            public int a(com.googlecode.mp4parser.authoring.f var1, com.googlecode.mp4parser.authoring.f var2) {
               return com.googlecode.mp4parser.c.b.a(var1.k().f() - var2.k().f());
            }

            // $FF: synthetic method
            public int compare(Object var1, Object var2) {
               return this.a((com.googlecode.mp4parser.authoring.f)var1, (com.googlecode.mp4parser.authoring.f)var2);
            }
         });
         HashMap var19 = new HashMap();
         HashMap var18 = new HashMap();
         HashMap var21 = new HashMap();
         Iterator var17 = var20.iterator();

         com.googlecode.mp4parser.authoring.f var23;
         while(var17.hasNext()) {
            var23 = (com.googlecode.mp4parser.authoring.f)var17.next();
            var19.put(var23, Integer.valueOf(0));
            var18.put(var23, Integer.valueOf(0));
            var21.put(var23, Double.valueOf(0.0D));
            this.a.put(var23, new StaticChunkOffsetBox());
         }

         long var13 = 0L;

         label43:
         while(true) {
            Iterator var22 = var20.iterator();
            var23 = null;

            while(true) {
               com.googlecode.mp4parser.authoring.f var24;
               do {
                  if(!var22.hasNext()) {
                     if(var23 == null) {
                        break label43;
                     }

                     ChunkOffsetBox var25 = (ChunkOffsetBox)this.a.get(var23);
                     var25.setChunkOffsets(i.a(var25.getChunkOffsets(), new long[]{var13}));
                     int var12 = ((Integer)var19.get(var23)).intValue();
                     int var11 = ((int[])var3.get(var23))[var12];
                     int var10 = ((Integer)var18.get(var23)).intValue();
                     double var5 = ((Double)var21.get(var23)).doubleValue();
                     long[] var26 = var23.i();

                     long var15;
                     for(int var9 = var10; var9 < var10 + var11; var13 += var15) {
                        var15 = ((long[])this.d.get(var23))[var9];
                        double var7 = (double)var26[var9] / (double)var23.k().b();
                        ++var9;
                        var5 += var7;
                     }

                     var19.put(var23, Integer.valueOf(var12 + 1));
                     var18.put(var23, Integer.valueOf(var10 + var11));
                     var21.put(var23, Double.valueOf(var5));
                     continue label43;
                  }

                  var24 = (com.googlecode.mp4parser.authoring.f)var22.next();
               } while(var23 != null && ((Double)var21.get(var24)).doubleValue() >= ((Double)var21.get(var23)).doubleValue());

               if(((Integer)var19.get(var24)).intValue() < ((int[])var3.get(var24)).length) {
                  var23 = var24;
               }
            }
         }
      }

      var4.addBox((com.coremedia.iso.boxes.a)this.a.get(var1));
   }

   protected void a(com.googlecode.mp4parser.authoring.f var1, Map var2, SampleTableBox var3) {
      int[] var9 = (int[])var2.get(var1);
      SampleToChunkBox var10 = new SampleToChunkBox();
      var10.setEntries(new LinkedList());
      long var7 = -2147483648L;

      long var5;
      for(int var4 = 0; var4 < var9.length; var7 = var5) {
         var5 = var7;
         if(var7 != (long)var9[var4]) {
            var10.getEntries().add(new SampleToChunkBox.a((long)(var4 + 1), (long)var9[var4], 1L));
            var5 = (long)var9[var4];
         }

         ++var4;
      }

      var3.addBox(var10);
   }

   protected void a(com.googlecode.mp4parser.authoring.tracks.a var1, SampleTableBox var2, int[] var3) {
      SampleAuxiliaryInformationSizesBox var9 = new SampleAuxiliaryInformationSizesBox();
      var9.setAuxInfoType("cenc");
      var9.setFlags(1);
      List var10 = var1.m();
      int var4;
      if(var1.n()) {
         short[] var11 = new short[var10.size()];

         for(var4 = 0; var4 < var11.length; ++var4) {
            var11[var4] = (short)((com.mp4parser.iso23001.part7.a)var10.get(var4)).a();
         }

         var9.setSampleInfoSizes(var11);
      } else {
         var9.setDefaultSampleInfoSize(8);
         var9.setSampleCount(var1.h().size());
      }

      SampleAuxiliaryInformationOffsetsBox var14 = new SampleAuxiliaryInformationOffsetsBox();
      SampleEncryptionBox var12 = new SampleEncryptionBox();
      var12.setSubSampleEncryption(var1.n());
      var12.setEntries(var10);
      long var7 = (long)var12.getOffsetToFirstIV();
      long[] var13 = new long[var3.length];
      var4 = 0;

      for(int var5 = 0; var5 < var3.length; ++var5) {
         var13[var5] = var7;

         for(int var6 = 0; var6 < var3[var5]; ++var4) {
            var7 += (long)((com.mp4parser.iso23001.part7.a)var10.get(var4)).a();
            ++var6;
         }
      }

      var14.setOffsets(var13);
      var2.addBox(var9);
      var2.addBox(var14);
      var2.addBox(var12);
      this.b.add(var14);
   }

   int[] a(com.googlecode.mp4parser.authoring.f var1) {
      long[] var8 = this.g.a(var1);
      int[] var7 = new int[var8.length];

      for(int var2 = 0; var2 < var8.length; ++var2) {
         long var5 = var8[var2];
         long var3;
         if(var8.length == var2 + 1) {
            var3 = (long)var1.h().size();
         } else {
            var3 = var8[var2 + 1] - 1L;
         }

         var7[var2] = com.googlecode.mp4parser.c.b.a(var3 - (var5 - 1L));
      }

      if(!e && (long)((List)this.c.get(var1)).size() != a(var7)) {
         throw new AssertionError("The number of samples and the sum of all chunk lengths must be equal");
      } else {
         return var7;
      }
   }

   protected FileTypeBox b(com.googlecode.mp4parser.authoring.c var1) {
      LinkedList var2 = new LinkedList();
      var2.add("mp42");
      var2.add("iso6");
      var2.add("avc1");
      var2.add("isom");
      return new FileTypeBox("iso6", 1L, var2);
   }

   protected com.coremedia.iso.boxes.a b(com.googlecode.mp4parser.authoring.f var1, com.googlecode.mp4parser.authoring.c var2, Map var3) {
      SampleTableBox var8 = new SampleTableBox();
      this.b(var1, var8);
      this.g(var1, var8);
      this.f(var1, var8);
      this.e(var1, var8);
      this.d(var1, var8);
      this.a(var1, var3, var8);
      this.c(var1, var8);
      this.a(var1, var2, var3, var8);
      HashMap var11 = new HashMap();

      Entry var12;
      Object var13;
      for(Iterator var9 = var1.g().entrySet().iterator(); var9.hasNext(); ((List)var13).add((com.googlecode.mp4parser.boxes.mp4.samplegrouping.b)var12.getKey())) {
         var12 = (Entry)var9.next();
         String var10 = ((com.googlecode.mp4parser.boxes.mp4.samplegrouping.b)var12.getKey()).a();
         List var7 = (List)var11.get(var10);
         var13 = var7;
         if(var7 == null) {
            var13 = new ArrayList();
            var11.put(var10, var13);
         }
      }

      Iterator var19 = var11.entrySet().iterator();

      while(var19.hasNext()) {
         Entry var18 = (Entry)var19.next();
         SampleGroupDescriptionBox var16 = new SampleGroupDescriptionBox();
         String var14 = (String)var18.getKey();
         var16.setGroupingType(var14);
         var16.setGroupEntries((List)var18.getValue());
         SampleToGroupBox var17 = new SampleToGroupBox();
         var17.setGroupingType(var14);
         SampleToGroupBox.a var15 = null;

         for(int var4 = 0; var4 < var1.h().size(); ++var4) {
            int var6 = 0;

            for(int var5 = 0; var5 < ((List)var18.getValue()).size(); ++var5) {
               com.googlecode.mp4parser.boxes.mp4.samplegrouping.b var20 = (com.googlecode.mp4parser.boxes.mp4.samplegrouping.b)((List)var18.getValue()).get(var5);
               if(Arrays.binarySearch((long[])var1.g().get(var20), (long)var4) >= 0) {
                  var6 = var5 + 1;
               }
            }

            if(var15 != null && var15.b() == var6) {
               var15.a(var15.a() + 1L);
            } else {
               var15 = new SampleToGroupBox.a(1L, var6);
               var17.getEntries().add(var15);
            }
         }

         var8.addBox(var16);
         var8.addBox(var17);
      }

      if(var1 instanceof com.googlecode.mp4parser.authoring.tracks.a) {
         this.a((com.googlecode.mp4parser.authoring.tracks.a)var1, var8, (int[])var3.get(var1));
      }

      this.a(var1, var8);
      f.a("done with stbl for track_" + var1.k().f());
      return var8;
   }

   protected void b(com.googlecode.mp4parser.authoring.f var1, SampleTableBox var2) {
      var2.addBox(var1.j());
   }

   protected com.coremedia.iso.boxes.a c(com.googlecode.mp4parser.authoring.c var1) {
      return null;
   }

   protected void c(com.googlecode.mp4parser.authoring.f var1, SampleTableBox var2) {
      SampleSizeBox var3 = new SampleSizeBox();
      var3.setSampleSizes((long[])this.d.get(var1));
      var2.addBox(var3);
   }

   public long d(com.googlecode.mp4parser.authoring.c var1) {
      long var2 = ((com.googlecode.mp4parser.authoring.f)var1.a().iterator().next()).k().b();

      for(Iterator var4 = var1.a().iterator(); var4.hasNext(); var2 = g.b(var2, ((com.googlecode.mp4parser.authoring.f)var4.next()).k().b())) {
         ;
      }

      return var2;
   }

   protected void d(com.googlecode.mp4parser.authoring.f var1, SampleTableBox var2) {
      if(var1.c() != null && !var1.c().isEmpty()) {
         SampleDependencyTypeBox var3 = new SampleDependencyTypeBox();
         var3.setEntries(var1.c());
         var2.addBox(var3);
      }

   }

   protected void e(com.googlecode.mp4parser.authoring.f var1, SampleTableBox var2) {
      long[] var4 = var1.b();
      if(var4 != null && var4.length > 0) {
         SyncSampleBox var3 = new SyncSampleBox();
         var3.setSampleNumber(var4);
         var2.addBox(var3);
      }

   }

   protected void f(com.googlecode.mp4parser.authoring.f var1, SampleTableBox var2) {
      List var3 = var1.a();
      if(var3 != null && !var3.isEmpty()) {
         CompositionTimeToSample var4 = new CompositionTimeToSample();
         var4.setEntries(var3);
         var2.addBox(var4);
      }

   }

   protected void g(com.googlecode.mp4parser.authoring.f var1, SampleTableBox var2) {
      ArrayList var7 = new ArrayList();
      long[] var8 = var1.i();
      int var4 = var8.length;
      TimeToSampleBox.a var9 = null;

      for(int var3 = 0; var3 < var4; ++var3) {
         long var5 = var8[var3];
         if(var9 != null && var9.b() == var5) {
            var9.a(var9.a() + 1L);
         } else {
            var9 = new TimeToSampleBox.a(1L, var5);
            var7.add(var9);
         }
      }

      TimeToSampleBox var10 = new TimeToSampleBox();
      var10.setEntries(var7);
      var2.addBox(var10);
   }

   private class InterleaveChunkMdat implements com.coremedia.iso.boxes.a {
      List chunkList;
      long contentSize;
      com.coremedia.iso.boxes.b parent;
      List tracks;

      private InterleaveChunkMdat(com.googlecode.mp4parser.authoring.c var2, Map var3, long var4) {
         this.chunkList = new ArrayList();
         this.contentSize = var4;
         this.tracks = var2.a();
         ArrayList var15 = new ArrayList(var3.keySet());
         Collections.sort(var15, new Comparator() {
            public int a(com.googlecode.mp4parser.authoring.f var1, com.googlecode.mp4parser.authoring.f var2) {
               return com.googlecode.mp4parser.c.b.a(var1.k().f() - var2.k().f());
            }

            // $FF: synthetic method
            public int compare(Object var1, Object var2) {
               return this.a((com.googlecode.mp4parser.authoring.f)var1, (com.googlecode.mp4parser.authoring.f)var2);
            }
         });
         HashMap var14 = new HashMap();
         HashMap var13 = new HashMap();
         HashMap var12 = new HashMap();
         Iterator var18 = var15.iterator();

         com.googlecode.mp4parser.authoring.f var17;
         while(var18.hasNext()) {
            var17 = (com.googlecode.mp4parser.authoring.f)var18.next();
            var14.put(var17, Integer.valueOf(0));
            var13.put(var17, Integer.valueOf(0));
            var12.put(var17, Double.valueOf(0.0D));
         }

         label40:
         while(true) {
            Iterator var16 = var15.iterator();
            var17 = null;

            while(true) {
               com.googlecode.mp4parser.authoring.f var19;
               do {
                  if(!var16.hasNext()) {
                     if(var17 == null) {
                        return;
                     }

                     int var11 = ((Integer)var14.get(var17)).intValue();
                     int var10 = ((int[])var3.get(var17))[var11];
                     int var9 = ((Integer)var13.get(var17)).intValue();
                     double var6 = ((Double)var12.get(var17)).doubleValue();

                     for(int var8 = var9; var8 < var9 + var10; ++var8) {
                        var6 += (double)var17.i()[var8] / (double)var17.k().b();
                     }

                     this.chunkList.add(var17.h().subList(var9, var9 + var10));
                     var14.put(var17, Integer.valueOf(var11 + 1));
                     var13.put(var17, Integer.valueOf(var9 + var10));
                     var12.put(var17, Double.valueOf(var6));
                     continue label40;
                  }

                  var19 = (com.googlecode.mp4parser.authoring.f)var16.next();
               } while(var17 != null && ((Double)var12.get(var19)).doubleValue() >= ((Double)var12.get(var17)).doubleValue());

               if(((Integer)var14.get(var19)).intValue() < ((int[])var3.get(var19)).length) {
                  var17 = var19;
               }
            }
         }
      }

      // $FF: synthetic method
      InterleaveChunkMdat(com.googlecode.mp4parser.authoring.c var2, Map var3, long var4, DefaultMp4Builder.InterleaveChunkMdat var6) {
         this(var2, var3, var4);
      }

      private boolean isSmallBox(long var1) {
         boolean var3;
         if(8L + var1 < 4294967296L) {
            var3 = true;
         } else {
            var3 = false;
         }

         return var3;
      }

      public void getBox(WritableByteChannel var1) throws IOException {
         ByteBuffer var10 = ByteBuffer.allocate(16);
         long var2 = this.getSize();
         if(this.isSmallBox(var2)) {
            com.coremedia.iso.g.b(var10, var2);
         } else {
            com.coremedia.iso.g.b(var10, 1L);
         }

         var10.put(d.a("mdat"));
         if(this.isSmallBox(var2)) {
            var10.put(new byte[8]);
         } else {
            com.coremedia.iso.g.a(var10, var2);
         }

         var10.rewind();
         var1.write(var10);
         DefaultMp4Builder.f.a("About to write " + this.contentSize);
         Iterator var13 = this.chunkList.iterator();
         long var8 = 0L;
         long var6 = 0L;

         while(var13.hasNext()) {
            Iterator var12 = ((List)var13.next()).iterator();
            var2 = var6;
            long var4 = var8;

            while(true) {
               var8 = var4;
               var6 = var2;
               if(!var12.hasNext()) {
                  break;
               }

               e var11 = (e)var12.next();
               var11.a(var1);
               var6 = var2 + var11.a();
               var2 = var6;
               if(var6 > 1048576L) {
                  var2 = var6 - 1048576L;
                  ++var4;
                  DefaultMp4Builder.f.a("Written " + var4 + "MB");
               }
            }
         }

      }

      public long getDataOffset() {
         long var1 = 16L;

         com.coremedia.iso.boxes.a var5;
         for(Object var3 = this; var3 instanceof com.coremedia.iso.boxes.a; var3 = ((com.coremedia.iso.boxes.a)var3).getParent()) {
            for(Iterator var4 = ((com.coremedia.iso.boxes.a)var3).getParent().getBoxes().iterator(); var4.hasNext(); var1 += var5.getSize()) {
               var5 = (com.coremedia.iso.boxes.a)var4.next();
               if(var3 == var5) {
                  break;
               }
            }
         }

         return var1;
      }

      public long getOffset() {
         throw new RuntimeException("Doesn't have any meaning for programmatically created boxes");
      }

      public com.coremedia.iso.boxes.b getParent() {
         return this.parent;
      }

      public long getSize() {
         return 16L + this.contentSize;
      }

      public String getType() {
         return "mdat";
      }

      public void parse(com.googlecode.mp4parser.b var1, ByteBuffer var2, long var3, com.coremedia.iso.b var5) throws IOException {
      }

      public void setParent(com.coremedia.iso.boxes.b var1) {
         this.parent = var1;
      }
   }
}
