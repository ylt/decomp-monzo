package com.googlecode.mp4parser.authoring.tracks;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public class b extends FilterInputStream {
   int a = -1;
   int b = -1;

   public b(InputStream var1) {
      super(var1);
   }

   public boolean markSupported() {
      return false;
   }

   public int read() throws IOException {
      int var2 = super.read();
      int var1 = var2;
      if(var2 == 3) {
         var1 = var2;
         if(this.a == 0) {
            var1 = var2;
            if(this.b == 0) {
               this.a = -1;
               this.b = -1;
               var1 = super.read();
            }
         }
      }

      this.a = this.b;
      this.b = var1;
      return var1;
   }

   public int read(byte[] var1, int var2, int var3) throws IOException {
      int var4 = -1;
      if(var1 == null) {
         throw new NullPointerException();
      } else if(var2 >= 0 && var3 >= 0 && var3 <= var1.length - var2) {
         if(var3 == 0) {
            var4 = 0;
         } else {
            int var5 = this.read();
            if(var5 != -1) {
               var1[var2] = (byte)var5;

               for(var4 = 1; var4 < var3; ++var4) {
                  try {
                     var5 = this.read();
                  } catch (IOException var6) {
                     break;
                  }

                  if(var5 == -1) {
                     break;
                  }

                  var1[var2 + var4] = (byte)var5;
               }
            }
         }

         return var4;
      } else {
         throw new IndexOutOfBoundsException();
      }
   }
}
