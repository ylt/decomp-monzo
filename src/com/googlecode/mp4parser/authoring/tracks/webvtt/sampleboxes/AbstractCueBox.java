package com.googlecode.mp4parser.authoring.tracks.webvtt.sampleboxes;

import com.coremedia.iso.d;
import com.coremedia.iso.g;
import com.coremedia.iso.j;
import com.googlecode.mp4parser.c.b;
import com.mp4parser.streaming.WriteOnlyBox;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;

public abstract class AbstractCueBox extends WriteOnlyBox {
   String content = "";

   public AbstractCueBox(String var1) {
      super(var1);
   }

   public void getBox(WritableByteChannel var1) throws IOException {
      ByteBuffer var2 = ByteBuffer.allocate(b.a(this.getSize()));
      g.b(var2, this.getSize());
      var2.put(d.a(this.getType()));
      var2.put(j.a(this.content));
      var1.write((ByteBuffer)var2.rewind());
   }

   public String getContent() {
      return this.content;
   }

   public long getSize() {
      return (long)(j.b(this.content) + 8);
   }

   public void setContent(String var1) {
      this.content = var1;
   }
}
