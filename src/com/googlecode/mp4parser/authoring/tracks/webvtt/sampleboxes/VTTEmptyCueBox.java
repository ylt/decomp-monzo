package com.googlecode.mp4parser.authoring.tracks.webvtt.sampleboxes;

import com.coremedia.iso.d;
import com.coremedia.iso.g;
import com.mp4parser.streaming.WriteOnlyBox;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;

public class VTTEmptyCueBox extends WriteOnlyBox {
   public VTTEmptyCueBox() {
      super("vtte");
   }

   public void getBox(WritableByteChannel var1) throws IOException {
      ByteBuffer var2 = ByteBuffer.allocate(8);
      g.b(var2, this.getSize());
      var2.put(d.a(this.getType()));
      var1.write((ByteBuffer)var2.rewind());
   }

   public long getSize() {
      return 8L;
   }
}
