package com.googlecode.mp4parser.authoring.tracks.webvtt.sampleboxes;

import com.coremedia.iso.d;
import com.coremedia.iso.g;
import com.mp4parser.streaming.WriteOnlyBox;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;

public class VTTCueBox extends WriteOnlyBox {
   CueIDBox cueIDBox;
   CuePayloadBox cuePayloadBox;
   CueSettingsBox cueSettingsBox;
   CueSourceIDBox cueSourceIDBox;
   CueTimeBox cueTimeBox;

   public VTTCueBox() {
      super("vtcc");
   }

   public void getBox(WritableByteChannel var1) throws IOException {
      ByteBuffer var2 = ByteBuffer.allocate(8);
      g.b(var2, this.getSize());
      var2.put(d.a(this.getType()));
      var1.write((ByteBuffer)var2.rewind());
      if(this.cueSourceIDBox != null) {
         this.cueSourceIDBox.getBox(var1);
      }

      if(this.cueIDBox != null) {
         this.cueIDBox.getBox(var1);
      }

      if(this.cueTimeBox != null) {
         this.cueTimeBox.getBox(var1);
      }

      if(this.cueSettingsBox != null) {
         this.cueSettingsBox.getBox(var1);
      }

      if(this.cuePayloadBox != null) {
         this.cuePayloadBox.getBox(var1);
      }

   }

   public CueIDBox getCueIDBox() {
      return this.cueIDBox;
   }

   public CuePayloadBox getCuePayloadBox() {
      return this.cuePayloadBox;
   }

   public CueSettingsBox getCueSettingsBox() {
      return this.cueSettingsBox;
   }

   public CueSourceIDBox getCueSourceIDBox() {
      return this.cueSourceIDBox;
   }

   public CueTimeBox getCueTimeBox() {
      return this.cueTimeBox;
   }

   public long getSize() {
      long var9 = 0L;
      long var1;
      if(this.cueSourceIDBox != null) {
         var1 = this.cueSourceIDBox.getSize();
      } else {
         var1 = 0L;
      }

      long var3;
      if(this.cueIDBox != null) {
         var3 = this.cueIDBox.getSize();
      } else {
         var3 = 0L;
      }

      long var5;
      if(this.cueTimeBox != null) {
         var5 = this.cueTimeBox.getSize();
      } else {
         var5 = 0L;
      }

      long var7;
      if(this.cueSettingsBox != null) {
         var7 = this.cueSettingsBox.getSize();
      } else {
         var7 = 0L;
      }

      if(this.cuePayloadBox != null) {
         var9 = this.cuePayloadBox.getSize();
      }

      return var7 + 8L + var1 + var3 + var5 + var9;
   }

   public void setCueIDBox(CueIDBox var1) {
      this.cueIDBox = var1;
   }

   public void setCuePayloadBox(CuePayloadBox var1) {
      this.cuePayloadBox = var1;
   }

   public void setCueSettingsBox(CueSettingsBox var1) {
      this.cueSettingsBox = var1;
   }

   public void setCueSourceIDBox(CueSourceIDBox var1) {
      this.cueSourceIDBox = var1;
   }

   public void setCueTimeBox(CueTimeBox var1) {
      this.cueTimeBox = var1;
   }
}
