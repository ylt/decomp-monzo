package com.googlecode.mp4parser.authoring;

import com.googlecode.mp4parser.c.h;
import java.util.Date;

public class g implements Cloneable {
   int a;
   private String b = "eng";
   private long c;
   private Date d = new Date();
   private Date e = new Date();
   private h f;
   private double g;
   private double h;
   private float i;
   private long j;
   private int k;

   public g() {
      this.f = h.j;
      this.j = 1L;
      this.k = 0;
   }

   public String a() {
      return this.b;
   }

   public void a(double var1) {
      this.g = var1;
   }

   public void a(float var1) {
      this.i = var1;
   }

   public void a(int var1) {
      this.a = var1;
   }

   public void a(long var1) {
      this.c = var1;
   }

   public void a(h var1) {
      this.f = var1;
   }

   public void a(String var1) {
      this.b = var1;
   }

   public void a(Date var1) {
      this.d = var1;
   }

   public long b() {
      return this.c;
   }

   public void b(double var1) {
      this.h = var1;
   }

   public void b(long var1) {
      this.j = var1;
   }

   public void b(Date var1) {
      this.e = var1;
   }

   public Date c() {
      return this.e;
   }

   public Object clone() {
      Object var1;
      try {
         var1 = super.clone();
      } catch (CloneNotSupportedException var2) {
         var1 = null;
      }

      return var1;
   }

   public double d() {
      return this.g;
   }

   public double e() {
      return this.h;
   }

   public long f() {
      return this.j;
   }

   public int g() {
      return this.a;
   }

   public float h() {
      return this.i;
   }

   public int i() {
      return this.k;
   }

   public h j() {
      return this.f;
   }
}
