package com.googlecode.mp4parser.authoring;

import com.coremedia.iso.boxes.SubSampleInformationBox;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class a implements f {
   String a;
   List b = new ArrayList();
   Map c = new HashMap();

   public a(String var1) {
      this.a = var1;
   }

   public List a() {
      return null;
   }

   public long[] b() {
      return null;
   }

   public List c() {
      return null;
   }

   public SubSampleInformationBox d() {
      return null;
   }

   public long e() {
      long var3 = 0L;
      long[] var5 = this.i();
      int var2 = var5.length;

      for(int var1 = 0; var1 < var2; ++var1) {
         var3 += var5[var1];
      }

      return var3;
   }

   public List f() {
      return this.b;
   }

   public Map g() {
      return this.c;
   }
}
