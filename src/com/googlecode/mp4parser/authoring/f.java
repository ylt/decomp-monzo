package com.googlecode.mp4parser.authoring;

import com.coremedia.iso.boxes.SampleDescriptionBox;
import com.coremedia.iso.boxes.SubSampleInformationBox;
import java.io.Closeable;
import java.util.List;
import java.util.Map;

public interface f extends Closeable {
   List a();

   long[] b();

   List c();

   SubSampleInformationBox d();

   long e();

   List f();

   Map g();

   List h();

   long[] i();

   SampleDescriptionBox j();

   g k();

   String l();
}
