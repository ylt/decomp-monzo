package com.googlecode.mp4parser.authoring;

import com.googlecode.mp4parser.c.h;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class c {
   h a;
   List b;

   public c() {
      this.a = h.j;
      this.b = new LinkedList();
   }

   public static long a(long var0, long var2) {
      if(var2 != 0L) {
         var0 = a(var2, var0 % var2);
      }

      return var0;
   }

   public f a(long var1) {
      Iterator var4 = this.b.iterator();

      f var3;
      do {
         if(!var4.hasNext()) {
            var3 = null;
            break;
         }

         var3 = (f)var4.next();
      } while(var3.k().f() != var1);

      return var3;
   }

   public List a() {
      return this.b;
   }

   public void a(f var1) {
      if(this.a(var1.k().f()) != null) {
         var1.k().b(this.b());
      }

      this.b.add(var1);
   }

   public void a(h var1) {
      this.a = var1;
   }

   public long b() {
      Iterator var4 = this.b.iterator();
      long var1 = 0L;

      while(var4.hasNext()) {
         f var3 = (f)var4.next();
         if(var1 < var3.k().f()) {
            var1 = var3.k().f();
         }
      }

      return 1L + var1;
   }

   public long c() {
      long var1 = ((f)this.a().iterator().next()).k().b();

      for(Iterator var3 = this.a().iterator(); var3.hasNext(); var1 = a(((f)var3.next()).k().b(), var1)) {
         ;
      }

      return var1;
   }

   public h d() {
      return this.a;
   }

   public String toString() {
      Iterator var2 = this.b.iterator();

      String var1;
      f var3;
      for(var1 = "Movie{ "; var2.hasNext(); var1 = var1 + "track_" + var3.k().f() + " (" + var3.l() + ") ") {
         var3 = (f)var2.next();
      }

      return var1 + '}';
   }
}
