package com.googlecode.mp4parser.authoring;

public class b {
   private long a;
   private double b;
   private long c;
   private double d;

   public b(long var1, long var3, double var5, double var7) {
      this.a = var3;
      this.b = var7;
      this.c = var1;
      this.d = var5;
   }

   public long a() {
      return this.a;
   }

   public double b() {
      return this.b;
   }

   public long c() {
      return this.c;
   }

   public double d() {
      return this.d;
   }
}
