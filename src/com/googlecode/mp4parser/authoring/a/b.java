package com.googlecode.mp4parser.authoring.a;

import com.coremedia.iso.d;
import com.coremedia.iso.boxes.TrackBox;
import com.coremedia.iso.boxes.fragment.MovieFragmentBox;
import com.coremedia.iso.boxes.fragment.TrackExtendsBox;
import com.coremedia.iso.boxes.fragment.TrackFragmentBox;
import com.coremedia.iso.boxes.fragment.TrackFragmentHeaderBox;
import com.coremedia.iso.boxes.fragment.TrackRunBox;
import com.googlecode.mp4parser.authoring.e;
import com.googlecode.mp4parser.c.j;
import java.io.IOException;
import java.lang.ref.SoftReference;
import java.lang.reflect.Array;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class b extends AbstractList {
   com.coremedia.iso.boxes.b a;
   d[] b;
   TrackBox c = null;
   TrackExtendsBox d = null;
   private SoftReference[] e;
   private List f;
   private Map g = new HashMap();
   private int[] h;
   private int i = -1;

   public b(long var1, com.coremedia.iso.boxes.b var3, d... var4) {
      this.a = var3;
      this.b = var4;
      Iterator var7 = j.a(var3, "moov[0]/trak").iterator();

      while(var7.hasNext()) {
         TrackBox var5 = (TrackBox)var7.next();
         if(var5.getTrackHeaderBox().getTrackId() == var1) {
            this.c = var5;
         }
      }

      if(this.c == null) {
         throw new RuntimeException("This MP4 does not contain track " + var1);
      } else {
         Iterator var6 = j.a(var3, "moov[0]/mvex[0]/trex").iterator();

         while(var6.hasNext()) {
            TrackExtendsBox var8 = (TrackExtendsBox)var6.next();
            if(var8.getTrackId() == this.c.getTrackHeaderBox().getTrackId()) {
               this.d = var8;
            }
         }

         this.e = (SoftReference[])Array.newInstance(SoftReference.class, this.size());
         this.a();
      }
   }

   private int a(TrackFragmentBox var1) {
      List var5 = var1.getBoxes();
      int var3 = 0;

      int var2;
      int var4;
      for(var4 = 0; var3 < var5.size(); var4 = var2) {
         com.coremedia.iso.boxes.a var6 = (com.coremedia.iso.boxes.a)var5.get(var3);
         var2 = var4;
         if(var6 instanceof TrackRunBox) {
            var2 = var4 + com.googlecode.mp4parser.c.b.a(((TrackRunBox)var6).getSampleCount());
         }

         ++var3;
      }

      return var4;
   }

   private List a() {
      Object var4;
      if(this.f != null) {
         var4 = this.f;
      } else {
         var4 = new ArrayList();
         Iterator var5 = this.a.getBoxes(MovieFragmentBox.class).iterator();

         while(var5.hasNext()) {
            Iterator var6 = ((MovieFragmentBox)var5.next()).getBoxes(TrackFragmentBox.class).iterator();

            while(var6.hasNext()) {
               TrackFragmentBox var7 = (TrackFragmentBox)var6.next();
               if(var7.getTrackFragmentHeaderBox().getTrackId() == this.c.getTrackHeaderBox().getTrackId()) {
                  ((List)var4).add(var7);
               }
            }
         }

         int var1;
         int var2;
         if(this.b != null) {
            d[] var9 = this.b;
            var2 = var9.length;

            for(var1 = 0; var1 < var2; ++var1) {
               Iterator var11 = var9[var1].getBoxes(MovieFragmentBox.class).iterator();

               while(var11.hasNext()) {
                  Iterator var8 = ((MovieFragmentBox)var11.next()).getBoxes(TrackFragmentBox.class).iterator();

                  while(var8.hasNext()) {
                     TrackFragmentBox var10 = (TrackFragmentBox)var8.next();
                     if(var10.getTrackFragmentHeaderBox().getTrackId() == this.c.getTrackHeaderBox().getTrackId()) {
                        ((List)var4).add(var10);
                     }
                  }
               }
            }
         }

         this.f = (List)var4;
         this.h = new int[this.f.size()];
         var1 = 0;

         int var3;
         for(var2 = 1; var1 < this.f.size(); var2 += var3) {
            this.h[var1] = var2;
            var3 = this.a((TrackFragmentBox)this.f.get(var1));
            ++var1;
         }
      }

      return (List)var4;
   }

   public e a(int var1) {
      e var14;
      if(this.e[var1] != null) {
         var14 = (e)this.e[var1].get();
         if(var14 != null) {
            return var14;
         }
      }

      int var3 = var1 + 1;

      final int var2;
      for(var2 = this.h.length - 1; var3 - this.h[var2] < 0; --var2) {
         ;
      }

      TrackFragmentBox var21 = (TrackFragmentBox)this.f.get(var2);
      int var5 = var3 - this.h[var2];
      MovieFragmentBox var16 = (MovieFragmentBox)var21.getParent();
      Iterator var15 = var21.getBoxes().iterator();
      var3 = 0;

      while(var15.hasNext()) {
         com.coremedia.iso.boxes.a var17 = (com.coremedia.iso.boxes.a)var15.next();
         if(var17 instanceof TrackRunBox) {
            TrackRunBox var30 = (TrackRunBox)var17;
            if(var30.getEntries().size() > var5 - var3) {
               List var18 = var30.getEntries();
               TrackFragmentHeaderBox var19 = var21.getTrackFragmentHeaderBox();
               boolean var12 = var30.isSampleSizePresent();
               boolean var13 = var19.hasDefaultSampleSize();
               final long var6;
               if(!var12) {
                  if(var13) {
                     var6 = var19.getDefaultSampleSize();
                  } else {
                     if(this.d == null) {
                        throw new RuntimeException("File doesn't contain trex box but track fragments aren't fully self contained. Cannot determine sample size.");
                     }

                     var6 = this.d.getDefaultSampleSize();
                  }
               } else {
                  var6 = 0L;
               }

               SoftReference var22 = (SoftReference)this.g.get(var30);
               ByteBuffer var23;
               if(var22 != null) {
                  var23 = (ByteBuffer)var22.get();
               } else {
                  var23 = null;
               }

               final ByteBuffer var24 = var23;
               if(var23 == null) {
                  long var8 = 0L;
                  Object var26 = var16;
                  if(var19.hasBaseDataOffset()) {
                     var8 = 0L + var19.getBaseDataOffset();
                     var26 = var16.getParent();
                  }

                  long var10 = var8;
                  if(var30.isDataOffsetPresent()) {
                     var10 = var8 + (long)var30.getDataOffset();
                  }

                  Iterator var27 = var18.iterator();
                  var2 = 0;

                  while(var27.hasNext()) {
                     TrackRunBox.a var25 = (TrackRunBox.a)var27.next();
                     if(var12) {
                        var2 = (int)((long)var2 + var25.b());
                     } else {
                        var2 = (int)((long)var2 + var6);
                     }
                  }

                  var8 = (long)var2;

                  try {
                     var24 = ((com.coremedia.iso.boxes.b)var26).getByteBuffer(var10, var8);
                     Map var29 = this.g;
                     SoftReference var28 = new SoftReference(var24);
                     var29.put(var30, var28);
                  } catch (IOException var20) {
                     throw new RuntimeException(var20);
                  }
               }

               var2 = 0;

               for(int var4 = 0; var4 < var5 - var3; ++var4) {
                  if(var12) {
                     var2 = (int)((long)var2 + ((TrackRunBox.a)var18.get(var4)).b());
                  } else {
                     var2 = (int)((long)var2 + var6);
                  }
               }

               if(var12) {
                  var6 = ((TrackRunBox.a)var18.get(var5 - var3)).b();
               }

               var14 = new e() {
                  public long a() {
                     return var6;
                  }

                  public void a(WritableByteChannel var1) throws IOException {
                     var1.write(this.b());
                  }

                  public ByteBuffer b() {
                     return (ByteBuffer)((ByteBuffer)var24.position(var2)).slice().limit(com.googlecode.mp4parser.c.b.a(var6));
                  }
               };
               this.e[var1] = new SoftReference(var14);
               return var14;
            }

            var3 += var30.getEntries().size();
         }
      }

      throw new RuntimeException("Couldn't find sample in the traf I was looking");
   }

   // $FF: synthetic method
   public Object get(int var1) {
      return this.a(var1);
   }

   public int size() {
      byte var3 = 0;
      int var1;
      if(this.i != -1) {
         var1 = this.i;
      } else {
         Iterator var8 = this.a.getBoxes(MovieFragmentBox.class).iterator();
         var1 = 0;

         int var2;
         long var5;
         Iterator var7;
         Iterator var14;
         label70:
         while(var8.hasNext()) {
            var7 = ((MovieFragmentBox)var8.next()).getBoxes(TrackFragmentBox.class).iterator();
            var2 = var1;

            while(true) {
               TrackFragmentBox var9;
               do {
                  var1 = var2;
                  if(!var7.hasNext()) {
                     continue label70;
                  }

                  var9 = (TrackFragmentBox)var7.next();
               } while(var9.getTrackFragmentHeaderBox().getTrackId() != this.c.getTrackHeaderBox().getTrackId());

               var14 = var9.getBoxes(TrackRunBox.class).iterator();
               var1 = var2;

               while(true) {
                  var2 = var1;
                  if(!var14.hasNext()) {
                     break;
                  }

                  TrackRunBox var10 = (TrackRunBox)var14.next();
                  var5 = (long)var1;
                  var1 = (int)(var10.getSampleCount() + var5);
               }
            }
         }

         d[] var13 = this.b;
         int var4 = var13.length;

         for(var2 = var3; var2 < var4; ++var2) {
            var7 = var13[var2].getBoxes(MovieFragmentBox.class).iterator();

            label47:
            while(var7.hasNext()) {
               var14 = ((MovieFragmentBox)var7.next()).getBoxes(TrackFragmentBox.class).iterator();
               int var12 = var1;

               while(true) {
                  TrackFragmentBox var15;
                  do {
                     var1 = var12;
                     if(!var14.hasNext()) {
                        continue label47;
                     }

                     var15 = (TrackFragmentBox)var14.next();
                  } while(var15.getTrackFragmentHeaderBox().getTrackId() != this.c.getTrackHeaderBox().getTrackId());

                  Iterator var16 = var15.getBoxes(TrackRunBox.class).iterator();
                  var1 = var12;

                  while(true) {
                     var12 = var1;
                     if(!var16.hasNext()) {
                        break;
                     }

                     TrackRunBox var11 = (TrackRunBox)var16.next();
                     var5 = (long)var1;
                     var1 = (int)(var11.getSampleCount() + var5);
                  }
               }
            }
         }

         this.i = var1;
      }

      return var1;
   }
}
