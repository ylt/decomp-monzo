package com.googlecode.mp4parser.authoring.a;

import com.coremedia.iso.boxes.MovieBox;
import com.coremedia.iso.boxes.SampleSizeBox;
import com.coremedia.iso.boxes.SampleToChunkBox;
import com.coremedia.iso.boxes.TrackBox;
import com.googlecode.mp4parser.authoring.e;
import com.googlecode.mp4parser.c.f;
import java.io.IOException;
import java.lang.ref.SoftReference;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;
import java.util.AbstractList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class a extends AbstractList {
   private static final f j = f.a(a.class);
   com.coremedia.iso.boxes.b a;
   TrackBox b = null;
   SoftReference[] c = null;
   int[] d;
   long[] e;
   long[] f;
   long[][] g;
   SampleSizeBox h;
   int i = 0;

   public a(long var1, com.coremedia.iso.boxes.b var3) {
      this.a = var3;
      Iterator var12 = ((MovieBox)var3.getBoxes(MovieBox.class).get(0)).getBoxes(TrackBox.class).iterator();

      while(var12.hasNext()) {
         TrackBox var13 = (TrackBox)var12.next();
         if(var13.getTrackHeaderBox().getTrackId() == var1) {
            this.b = var13;
         }
      }

      if(this.b == null) {
         throw new RuntimeException("This MP4 does not contain track " + var1);
      } else {
         this.e = this.b.getSampleTableBox().getChunkOffsetBox().getChunkOffsets();
         this.f = new long[this.e.length];
         this.c = new SoftReference[this.e.length];
         Arrays.fill(this.c, new SoftReference((Object)null));
         this.g = new long[this.e.length][];
         this.h = this.b.getSampleTableBox().getSampleSizeBox();
         List var14 = this.b.getSampleTableBox().getSampleToChunkBox().getEntries();
         SampleToChunkBox.a[] var15 = (SampleToChunkBox.a[])var14.toArray(new SampleToChunkBox.a[var14.size()]);
         SampleToChunkBox.a var18 = var15[0];
         var1 = var18.a();
         int var4 = com.googlecode.mp4parser.c.b.a(var18.b());
         int var11 = this.size();
         int var6 = 0;
         int var9 = 0;
         int var5 = 1;
         int var7 = 1;

         while(true) {
            ++var9;
            int var8;
            if((long)var9 == var1) {
               if(var15.length > var5) {
                  var18 = var15[var5];
                  var8 = com.googlecode.mp4parser.c.b.a(var18.b());
                  var1 = var18.a();
                  var6 = var5 + 1;
                  var5 = var4;
                  var4 = var8;
               } else {
                  byte var10 = -1;
                  var6 = var4;
                  var8 = var5;
                  var1 = Long.MAX_VALUE;
                  var4 = var10;
                  var5 = var6;
                  var6 = var8;
               }
            } else {
               var8 = var6;
               var6 = var5;
               var5 = var8;
            }

            this.g[var9 - 1] = new long[var5];
            var7 += var5;
            if(var7 > var11) {
               this.d = new int[var9 + 1];
               var18 = var15[0];
               var6 = 0;
               var8 = 0;
               var1 = var18.a();
               var4 = com.googlecode.mp4parser.c.b.a(var18.b());
               var7 = 1;
               var5 = 1;

               while(true) {
                  int[] var19 = this.d;
                  int var17 = var6 + 1;
                  var19[var6] = var7;
                  if((long)var17 == var1) {
                     if(var15.length > var5) {
                        var8 = var5 + 1;
                        var18 = var15[var5];
                        var6 = com.googlecode.mp4parser.c.b.a(var18.b());
                        var1 = var18.a();
                        var5 = var4;
                        var4 = var8;
                     } else {
                        var1 = Long.MAX_VALUE;
                        var8 = var5;
                        var6 = -1;
                        var5 = var4;
                        var4 = var8;
                     }
                  } else {
                     var9 = var5;
                     var6 = var4;
                     var5 = var8;
                     var4 = var9;
                  }

                  var7 += var5;
                  if(var7 > var11) {
                     this.d[var17] = Integer.MAX_VALUE;
                     var5 = 0;
                     var1 = 0L;

                     for(var4 = 1; (long)var4 <= this.h.getSampleCount(); ++var4) {
                        while(var4 == this.d[var5]) {
                           ++var5;
                           var1 = 0L;
                        }

                        long[] var16 = this.f;
                        var6 = var5 - 1;
                        var16[var6] += this.h.getSampleSizeAtIndex(var4 - 1);
                        this.g[var5 - 1][var4 - this.d[var5 - 1]] = var1;
                        var1 += this.h.getSampleSizeAtIndex(var4 - 1);
                     }

                     return;
                  }

                  var9 = var4;
                  var4 = var6;
                  var8 = var5;
                  var6 = var17;
                  var5 = var9;
               }
            }

            var8 = var6;
            var6 = var5;
            var5 = var8;
         }
      }
   }

   // $FF: synthetic method
   static f a() {
      return j;
   }

   int a(int param1) {
      // $FF: Couldn't be decompiled
   }

   public e b(int var1) {
      if((long)var1 >= this.h.getSampleCount()) {
         throw new IndexOutOfBoundsException();
      } else {
         return new a.a(var1);
      }
   }

   // $FF: synthetic method
   public Object get(int var1) {
      return this.b(var1);
   }

   public int size() {
      return com.googlecode.mp4parser.c.b.a(this.b.getSampleTableBox().getSampleSizeBox().getSampleCount());
   }

   class a implements e {
      private int b;

      public a(int var2) {
         this.b = var2;
      }

      public long a() {
         return a.this.h.getSampleSizeAtIndex(this.b);
      }

      public void a(WritableByteChannel var1) throws IOException {
         var1.write(this.b());
      }

      public ByteBuffer b() {
         // $FF: Couldn't be decompiled
      }

      public String toString() {
         return "Sample(index: " + this.b + " size: " + a.this.h.getSampleSizeAtIndex(this.b) + ")";
      }
   }
}
