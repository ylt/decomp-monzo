package com.googlecode.mp4parser.authoring;

import com.coremedia.iso.boxes.CompositionTimeToSample;
import com.coremedia.iso.boxes.EditListBox;
import com.coremedia.iso.boxes.MediaHeaderBox;
import com.coremedia.iso.boxes.MovieHeaderBox;
import com.coremedia.iso.boxes.SampleDescriptionBox;
import com.coremedia.iso.boxes.SampleTableBox;
import com.coremedia.iso.boxes.SubSampleInformationBox;
import com.coremedia.iso.boxes.TimeToSampleBox;
import com.coremedia.iso.boxes.TrackBox;
import com.coremedia.iso.boxes.TrackHeaderBox;
import com.coremedia.iso.boxes.fragment.MovieExtendsBox;
import com.coremedia.iso.boxes.fragment.MovieFragmentBox;
import com.coremedia.iso.boxes.fragment.TrackExtendsBox;
import com.coremedia.iso.boxes.fragment.TrackFragmentBox;
import com.coremedia.iso.boxes.fragment.TrackFragmentHeaderBox;
import com.coremedia.iso.boxes.fragment.TrackRunBox;
import com.googlecode.mp4parser.AbstractContainerBox;
import com.googlecode.mp4parser.boxes.mp4.samplegrouping.SampleGroupDescriptionBox;
import com.googlecode.mp4parser.boxes.mp4.samplegrouping.SampleToGroupBox;
import com.googlecode.mp4parser.c.i;
import com.googlecode.mp4parser.c.j;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class d extends a {
   // $FF: synthetic field
   static final boolean f;
   TrackBox d;
   com.coremedia.iso.d[] e;
   private List g;
   private SampleDescriptionBox h;
   private long[] i;
   private List j;
   private long[] k = null;
   private List l;
   private g m = new g();
   private String n;
   private SubSampleInformationBox o = null;

   static {
      boolean var0;
      if(!d.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      f = var0;
   }

   public d(String var1, TrackBox var2, com.coremedia.iso.d... var3) {
      super(var1);
      this.d = var2;
      long var10 = var2.getTrackHeaderBox().getTrackId();
      this.g = new com.coremedia.iso.boxes.mdat.a(var2, var3);
      SampleTableBox var14 = var2.getMediaBox().getMediaInformationBox().getSampleTableBox();
      this.n = var2.getMediaBox().getHandlerBox().getHandlerType();
      ArrayList var12 = new ArrayList();
      this.j = new ArrayList();
      this.l = new ArrayList();
      var12.addAll(var14.getTimeToSampleBox().getEntries());
      if(var14.getCompositionTimeToSample() != null) {
         this.j.addAll(var14.getCompositionTimeToSample().getEntries());
      }

      if(var14.getSampleDependencyTypeBox() != null) {
         this.l.addAll(var14.getSampleDependencyTypeBox().getEntries());
      }

      if(var14.getSyncSampleBox() != null) {
         this.k = var14.getSyncSampleBox().getSampleNumber();
      }

      this.o = (SubSampleInformationBox)j.a((AbstractContainerBox)var14, "subs");
      ArrayList var13 = new ArrayList();
      var13.addAll(((com.coremedia.iso.boxes.a)var2.getParent()).getParent().getBoxes(MovieFragmentBox.class));
      int var5 = var3.length;

      for(int var4 = 0; var4 < var5; ++var4) {
         var13.addAll(var3[var4].getBoxes(MovieFragmentBox.class));
      }

      this.h = var14.getSampleDescriptionBox();
      List var24 = var2.getParent().getBoxes(MovieExtendsBox.class);
      Iterator var29;
      if(var24.size() > 0) {
         Iterator var17 = var24.iterator();

         label156:
         while(var17.hasNext()) {
            var29 = ((MovieExtendsBox)var17.next()).getBoxes(TrackExtendsBox.class).iterator();

            while(true) {
               TrackExtendsBox var16;
               do {
                  if(!var29.hasNext()) {
                     continue label156;
                  }

                  var16 = (TrackExtendsBox)var29.next();
               } while(var16.getTrackId() != var10);

               if(j.a(((com.coremedia.iso.boxes.a)var2.getParent()).getParent(), "/moof/traf/subs").size() > 0) {
                  this.o = new SubSampleInformationBox();
               }

               long var6 = 1L;
               Iterator var18 = var13.iterator();

               label152:
               while(var18.hasNext()) {
                  Iterator var15 = ((MovieFragmentBox)var18.next()).getBoxes(TrackFragmentBox.class).iterator();

                  while(true) {
                     TrackFragmentBox var25;
                     do {
                        if(!var15.hasNext()) {
                           continue label152;
                        }

                        var25 = (TrackFragmentBox)var15.next();
                     } while(var25.getTrackFragmentHeaderBox().getTrackId() != var10);

                     this.c = this.a(var14.getBoxes(SampleGroupDescriptionBox.class), j.a((com.coremedia.iso.boxes.b)var25, "sgpd"), j.a((com.coremedia.iso.boxes.b)var25, "sbgp"), this.c, var6 - 1L);
                     SubSampleInformationBox var19 = (SubSampleInformationBox)j.a((AbstractContainerBox)var25, "subs");
                     long var8;
                     if(var19 != null) {
                        var8 = var6 - (long)0 - 1L;

                        SubSampleInformationBox.a var21;
                        for(Iterator var20 = var19.getEntries().iterator(); var20.hasNext(); this.o.getEntries().add(var21)) {
                           SubSampleInformationBox.a var35 = (SubSampleInformationBox.a)var20.next();
                           var21 = new SubSampleInformationBox.a();
                           var21.c().addAll(var35.c());
                           if(var8 != 0L) {
                              var21.a(var8 + var35.a());
                              var8 = 0L;
                           } else {
                              var21.a(var35.a());
                           }
                        }
                     }

                     Iterator var38 = var25.getBoxes(TrackRunBox.class).iterator();
                     var8 = var6;

                     while(true) {
                        var6 = var8;
                        if(!var38.hasNext()) {
                           break;
                        }

                        TrackRunBox var37 = (TrackRunBox)var38.next();
                        TrackFragmentHeaderBox var36 = ((TrackFragmentBox)var37.getParent()).getTrackFragmentHeaderBox();
                        Iterator var22 = var37.getEntries().iterator();
                        boolean var33 = true;
                        var6 = var8;

                        while(true) {
                           var8 = var6;
                           if(!var22.hasNext()) {
                              break;
                           }

                           TrackRunBox.a var26 = (TrackRunBox.a)var22.next();
                           if(var37.isSampleDurationPresent()) {
                              if(var12.size() != 0 && ((TimeToSampleBox.a)var12.get(var12.size() - 1)).b() == var26.a()) {
                                 TimeToSampleBox.a var23 = (TimeToSampleBox.a)var12.get(var12.size() - 1);
                                 var23.a(var23.a() + 1L);
                              } else {
                                 var12.add(new TimeToSampleBox.a(1L, var26.a()));
                              }
                           } else if(var36.hasDefaultSampleDuration()) {
                              var12.add(new TimeToSampleBox.a(1L, var36.getDefaultSampleDuration()));
                           } else {
                              var12.add(new TimeToSampleBox.a(1L, var16.getDefaultSampleDuration()));
                           }

                           if(var37.isSampleCompositionTimeOffsetPresent()) {
                              if(this.j.size() != 0 && (long)((CompositionTimeToSample.a)this.j.get(this.j.size() - 1)).b() == var26.d()) {
                                 CompositionTimeToSample.a var39 = (CompositionTimeToSample.a)this.j.get(this.j.size() - 1);
                                 var39.a(var39.a() + 1);
                              } else {
                                 this.j.add(new CompositionTimeToSample.a(1, com.googlecode.mp4parser.c.b.a(var26.d())));
                              }
                           }

                           com.coremedia.iso.boxes.fragment.a var28;
                           if(var37.isSampleFlagsPresent()) {
                              var28 = var26.c();
                           } else if(var33 && var37.isFirstSampleFlagsPresent()) {
                              var28 = var37.getFirstSampleFlags();
                           } else if(var36.hasDefaultSampleFlags()) {
                              var28 = var36.getDefaultSampleFlags();
                           } else {
                              var28 = var16.getDefaultSampleFlags();
                           }

                           if(var28 != null && !var28.a()) {
                              this.k = i.a(this.k, new long[]{var6});
                           }

                           ++var6;
                           var33 = false;
                        }
                     }
                  }
               }
            }
         }
      } else {
         this.c = this.a(var14.getBoxes(SampleGroupDescriptionBox.class), (List)null, var14.getBoxes(SampleToGroupBox.class), this.c, 0L);
      }

      this.i = TimeToSampleBox.blowupTimeToSamples(var12);
      MediaHeaderBox var31 = var2.getMediaBox().getMediaHeaderBox();
      TrackHeaderBox var30 = var2.getTrackHeaderBox();
      this.m.b(var30.getTrackId());
      this.m.b(var31.getCreationTime());
      this.m.a(var31.getLanguage());
      this.m.a(var31.getModificationTime());
      this.m.a(var31.getTimescale());
      this.m.b(var30.getHeight());
      this.m.a(var30.getWidth());
      this.m.a(var30.getLayer());
      this.m.a(var30.getMatrix());
      this.m.a(var30.getVolume());
      EditListBox var32 = (EditListBox)j.a((AbstractContainerBox)var2, "edts/elst");
      MovieHeaderBox var27 = (MovieHeaderBox)j.a((AbstractContainerBox)var2, "../mvhd");
      if(var32 != null) {
         var29 = var32.getEntries().iterator();

         while(var29.hasNext()) {
            EditListBox.a var34 = (EditListBox.a)var29.next();
            this.b.add(new b(var34.b(), var31.getTimescale(), var34.c(), (double)var34.a() / (double)var27.getTimescale()));
         }
      }

   }

   private Map a(List var1, List var2, List var3, Map var4, long var5) {
      Iterator var13 = var3.iterator();

      while(var13.hasNext()) {
         SampleToGroupBox var11 = (SampleToGroupBox)var13.next();
         Iterator var12 = var11.getEntries().iterator();

         SampleToGroupBox.a var14;
         for(int var7 = 0; var12.hasNext(); var7 = (int)((long)var7 + var14.a())) {
            var14 = (SampleToGroupBox.a)var12.next();
            if(var14.b() > 0) {
               com.googlecode.mp4parser.boxes.mp4.samplegrouping.b var15;
               com.googlecode.mp4parser.boxes.mp4.samplegrouping.b var17;
               if(var14.b() > '\uffff') {
                  Iterator var9 = var2.iterator();
                  var15 = null;

                  while(var9.hasNext()) {
                     SampleGroupDescriptionBox var10 = (SampleGroupDescriptionBox)var9.next();
                     if(var10.getGroupingType().equals(var11.getGroupingType())) {
                        var15 = (com.googlecode.mp4parser.boxes.mp4.samplegrouping.b)var10.getGroupEntries().get(var14.b() - 1 & '\uffff');
                     }
                  }

                  var17 = var15;
               } else {
                  Iterator var19 = var1.iterator();
                  var15 = null;

                  while(true) {
                     var17 = var15;
                     if(!var19.hasNext()) {
                        break;
                     }

                     SampleGroupDescriptionBox var18 = (SampleGroupDescriptionBox)var19.next();
                     if(var18.getGroupingType().equals(var11.getGroupingType())) {
                        var15 = (com.googlecode.mp4parser.boxes.mp4.samplegrouping.b)var18.getGroupEntries().get(var14.b() - 1);
                     }
                  }
               }

               if(!f && var17 == null) {
                  throw new AssertionError();
               }

               long[] var20 = (long[])var4.get(var17);
               long[] var16 = var20;
               if(var20 == null) {
                  var16 = new long[0];
               }

               var20 = new long[com.googlecode.mp4parser.c.b.a(var14.a()) + var16.length];
               System.arraycopy(var16, 0, var20, 0, var16.length);

               for(int var8 = 0; (long)var8 < var14.a(); ++var8) {
                  var20[var16.length + var8] = (long)var7 + var5 + (long)var8;
               }

               var4.put(var17, var20);
            }
         }
      }

      return var4;
   }

   public List a() {
      return this.j;
   }

   public long[] b() {
      long[] var1;
      if(this.k != null && this.k.length != this.g.size()) {
         var1 = this.k;
      } else {
         var1 = null;
      }

      return var1;
   }

   public List c() {
      return this.l;
   }

   public void close() throws IOException {
      com.coremedia.iso.boxes.b var3 = this.d.getParent();
      if(var3 instanceof com.googlecode.mp4parser.a) {
         ((com.googlecode.mp4parser.a)var3).close();
      }

      if(this.e != null) {
         com.coremedia.iso.d[] var4 = this.e;
         int var2 = var4.length;

         for(int var1 = 0; var1 < var2; ++var1) {
            var4[var1].close();
         }
      }

   }

   public SubSampleInformationBox d() {
      return this.o;
   }

   public List h() {
      return this.g;
   }

   public long[] i() {
      synchronized(this){}

      long[] var1;
      try {
         var1 = this.i;
      } finally {
         ;
      }

      return var1;
   }

   public SampleDescriptionBox j() {
      return this.h;
   }

   public g k() {
      return this.m;
   }

   public String l() {
      return this.n;
   }
}
