package com.googlecode.mp4parser.boxes;

import com.googlecode.mp4parser.AbstractBox;
import com.googlecode.mp4parser.e;
import com.googlecode.mp4parser.boxes.mp4.a.c;
import com.googlecode.mp4parser.boxes.mp4.a.d;
import java.nio.ByteBuffer;
import org.mp4parser.aspectj.a.b.b;
import org.mp4parser.aspectj.lang.a;

public class AC3SpecificBox extends AbstractBox {
   public static final String TYPE = "dac3";
   private static final a.a ajc$tjp_0;
   private static final a.a ajc$tjp_1;
   private static final a.a ajc$tjp_10;
   private static final a.a ajc$tjp_11;
   private static final a.a ajc$tjp_12;
   private static final a.a ajc$tjp_13;
   private static final a.a ajc$tjp_14;
   private static final a.a ajc$tjp_2;
   private static final a.a ajc$tjp_3;
   private static final a.a ajc$tjp_4;
   private static final a.a ajc$tjp_5;
   private static final a.a ajc$tjp_6;
   private static final a.a ajc$tjp_7;
   private static final a.a ajc$tjp_8;
   private static final a.a ajc$tjp_9;
   int acmod;
   int bitRateCode;
   int bsid;
   int bsmod;
   int fscod;
   int lfeon;
   int reserved;

   static {
      ajc$preClinit();
   }

   public AC3SpecificBox() {
      super("dac3");
   }

   private static void ajc$preClinit() {
      b var0 = new b("AC3SpecificBox.java", AC3SpecificBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getFscod", "com.googlecode.mp4parser.boxes.AC3SpecificBox", "", "", "", "int"), 55);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "setFscod", "com.googlecode.mp4parser.boxes.AC3SpecificBox", "int", "fscod", "", "void"), 59);
      ajc$tjp_10 = var0.a("method-execution", var0.a("1", "getBitRateCode", "com.googlecode.mp4parser.boxes.AC3SpecificBox", "", "", "", "int"), 95);
      ajc$tjp_11 = var0.a("method-execution", var0.a("1", "setBitRateCode", "com.googlecode.mp4parser.boxes.AC3SpecificBox", "int", "bitRateCode", "", "void"), 99);
      ajc$tjp_12 = var0.a("method-execution", var0.a("1", "getReserved", "com.googlecode.mp4parser.boxes.AC3SpecificBox", "", "", "", "int"), 103);
      ajc$tjp_13 = var0.a("method-execution", var0.a("1", "setReserved", "com.googlecode.mp4parser.boxes.AC3SpecificBox", "int", "reserved", "", "void"), 107);
      ajc$tjp_14 = var0.a("method-execution", var0.a("1", "toString", "com.googlecode.mp4parser.boxes.AC3SpecificBox", "", "", "", "java.lang.String"), 112);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "getBsid", "com.googlecode.mp4parser.boxes.AC3SpecificBox", "", "", "", "int"), 63);
      ajc$tjp_3 = var0.a("method-execution", var0.a("1", "setBsid", "com.googlecode.mp4parser.boxes.AC3SpecificBox", "int", "bsid", "", "void"), 67);
      ajc$tjp_4 = var0.a("method-execution", var0.a("1", "getBsmod", "com.googlecode.mp4parser.boxes.AC3SpecificBox", "", "", "", "int"), 71);
      ajc$tjp_5 = var0.a("method-execution", var0.a("1", "setBsmod", "com.googlecode.mp4parser.boxes.AC3SpecificBox", "int", "bsmod", "", "void"), 75);
      ajc$tjp_6 = var0.a("method-execution", var0.a("1", "getAcmod", "com.googlecode.mp4parser.boxes.AC3SpecificBox", "", "", "", "int"), 79);
      ajc$tjp_7 = var0.a("method-execution", var0.a("1", "setAcmod", "com.googlecode.mp4parser.boxes.AC3SpecificBox", "int", "acmod", "", "void"), 83);
      ajc$tjp_8 = var0.a("method-execution", var0.a("1", "getLfeon", "com.googlecode.mp4parser.boxes.AC3SpecificBox", "", "", "", "int"), 87);
      ajc$tjp_9 = var0.a("method-execution", var0.a("1", "setLfeon", "com.googlecode.mp4parser.boxes.AC3SpecificBox", "int", "lfeon", "", "void"), 91);
   }

   public void _parseDetails(ByteBuffer var1) {
      c var2 = new c(var1);
      this.fscod = var2.a(2);
      this.bsid = var2.a(5);
      this.bsmod = var2.a(3);
      this.acmod = var2.a(3);
      this.lfeon = var2.a(1);
      this.bitRateCode = var2.a(5);
      this.reserved = var2.a(5);
   }

   public int getAcmod() {
      a var1 = b.a(ajc$tjp_6, this, this);
      e.a().a(var1);
      return this.acmod;
   }

   public int getBitRateCode() {
      a var1 = b.a(ajc$tjp_10, this, this);
      e.a().a(var1);
      return this.bitRateCode;
   }

   public int getBsid() {
      a var1 = b.a(ajc$tjp_2, this, this);
      e.a().a(var1);
      return this.bsid;
   }

   public int getBsmod() {
      a var1 = b.a(ajc$tjp_4, this, this);
      e.a().a(var1);
      return this.bsmod;
   }

   protected void getContent(ByteBuffer var1) {
      d var2 = new d(var1);
      var2.a(this.fscod, 2);
      var2.a(this.bsid, 5);
      var2.a(this.bsmod, 3);
      var2.a(this.acmod, 3);
      var2.a(this.lfeon, 1);
      var2.a(this.bitRateCode, 5);
      var2.a(this.reserved, 5);
   }

   protected long getContentSize() {
      return 3L;
   }

   public int getFscod() {
      a var1 = b.a(ajc$tjp_0, this, this);
      e.a().a(var1);
      return this.fscod;
   }

   public int getLfeon() {
      a var1 = b.a(ajc$tjp_8, this, this);
      e.a().a(var1);
      return this.lfeon;
   }

   public int getReserved() {
      a var1 = b.a(ajc$tjp_12, this, this);
      e.a().a(var1);
      return this.reserved;
   }

   public void setAcmod(int var1) {
      a var2 = b.a(ajc$tjp_7, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      e.a().a(var2);
      this.acmod = var1;
   }

   public void setBitRateCode(int var1) {
      a var2 = b.a(ajc$tjp_11, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      e.a().a(var2);
      this.bitRateCode = var1;
   }

   public void setBsid(int var1) {
      a var2 = b.a(ajc$tjp_3, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      e.a().a(var2);
      this.bsid = var1;
   }

   public void setBsmod(int var1) {
      a var2 = b.a(ajc$tjp_5, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      e.a().a(var2);
      this.bsmod = var1;
   }

   public void setFscod(int var1) {
      a var2 = b.a(ajc$tjp_1, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      e.a().a(var2);
      this.fscod = var1;
   }

   public void setLfeon(int var1) {
      a var2 = b.a(ajc$tjp_9, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      e.a().a(var2);
      this.lfeon = var1;
   }

   public void setReserved(int var1) {
      a var2 = b.a(ajc$tjp_13, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      e.a().a(var2);
      this.reserved = var1;
   }

   public String toString() {
      a var1 = b.a(ajc$tjp_14, this, this);
      e.a().a(var1);
      return "AC3SpecificBox{fscod=" + this.fscod + ", bsid=" + this.bsid + ", bsmod=" + this.bsmod + ", acmod=" + this.acmod + ", lfeon=" + this.lfeon + ", bitRateCode=" + this.bitRateCode + ", reserved=" + this.reserved + '}';
   }
}
