package com.googlecode.mp4parser.boxes;

import com.googlecode.mp4parser.AbstractBox;
import com.googlecode.mp4parser.e;
import com.googlecode.mp4parser.boxes.mp4.a.c;
import com.googlecode.mp4parser.boxes.mp4.a.d;
import java.nio.ByteBuffer;
import org.mp4parser.aspectj.a.b.b;
import org.mp4parser.aspectj.lang.a;

public class MLPSpecificBox extends AbstractBox {
   public static final String TYPE = "dmlp";
   private static final a.a ajc$tjp_0;
   private static final a.a ajc$tjp_1;
   private static final a.a ajc$tjp_2;
   private static final a.a ajc$tjp_3;
   private static final a.a ajc$tjp_4;
   private static final a.a ajc$tjp_5;
   private static final a.a ajc$tjp_6;
   private static final a.a ajc$tjp_7;
   int format_info;
   int peak_data_rate;
   int reserved;
   int reserved2;

   static {
      ajc$preClinit();
   }

   public MLPSpecificBox() {
      super("dmlp");
   }

   private static void ajc$preClinit() {
      b var0 = new b("MLPSpecificBox.java", MLPSpecificBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getFormat_info", "com.googlecode.mp4parser.boxes.MLPSpecificBox", "", "", "", "int"), 49);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "setFormat_info", "com.googlecode.mp4parser.boxes.MLPSpecificBox", "int", "format_info", "", "void"), 53);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "getPeak_data_rate", "com.googlecode.mp4parser.boxes.MLPSpecificBox", "", "", "", "int"), 57);
      ajc$tjp_3 = var0.a("method-execution", var0.a("1", "setPeak_data_rate", "com.googlecode.mp4parser.boxes.MLPSpecificBox", "int", "peak_data_rate", "", "void"), 61);
      ajc$tjp_4 = var0.a("method-execution", var0.a("1", "getReserved", "com.googlecode.mp4parser.boxes.MLPSpecificBox", "", "", "", "int"), 65);
      ajc$tjp_5 = var0.a("method-execution", var0.a("1", "setReserved", "com.googlecode.mp4parser.boxes.MLPSpecificBox", "int", "reserved", "", "void"), 69);
      ajc$tjp_6 = var0.a("method-execution", var0.a("1", "getReserved2", "com.googlecode.mp4parser.boxes.MLPSpecificBox", "", "", "", "int"), 73);
      ajc$tjp_7 = var0.a("method-execution", var0.a("1", "setReserved2", "com.googlecode.mp4parser.boxes.MLPSpecificBox", "int", "reserved2", "", "void"), 77);
   }

   public void _parseDetails(ByteBuffer var1) {
      c var2 = new c(var1);
      this.format_info = var2.a(32);
      this.peak_data_rate = var2.a(15);
      this.reserved = var2.a(1);
      this.reserved2 = var2.a(32);
   }

   protected void getContent(ByteBuffer var1) {
      d var2 = new d(var1);
      var2.a(this.format_info, 32);
      var2.a(this.peak_data_rate, 15);
      var2.a(this.reserved, 1);
      var2.a(this.reserved2, 32);
   }

   protected long getContentSize() {
      return 10L;
   }

   public int getFormat_info() {
      a var1 = b.a(ajc$tjp_0, this, this);
      e.a().a(var1);
      return this.format_info;
   }

   public int getPeak_data_rate() {
      a var1 = b.a(ajc$tjp_2, this, this);
      e.a().a(var1);
      return this.peak_data_rate;
   }

   public int getReserved() {
      a var1 = b.a(ajc$tjp_4, this, this);
      e.a().a(var1);
      return this.reserved;
   }

   public int getReserved2() {
      a var1 = b.a(ajc$tjp_6, this, this);
      e.a().a(var1);
      return this.reserved2;
   }

   public void setFormat_info(int var1) {
      a var2 = b.a(ajc$tjp_1, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      e.a().a(var2);
      this.format_info = var1;
   }

   public void setPeak_data_rate(int var1) {
      a var2 = b.a(ajc$tjp_3, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      e.a().a(var2);
      this.peak_data_rate = var1;
   }

   public void setReserved(int var1) {
      a var2 = b.a(ajc$tjp_5, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      e.a().a(var2);
      this.reserved = var1;
   }

   public void setReserved2(int var1) {
      a var2 = b.a(ajc$tjp_7, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      e.a().a(var2);
      this.reserved2 = var1;
   }
}
