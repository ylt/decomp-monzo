package com.googlecode.mp4parser.boxes.piff;

import com.coremedia.iso.e;
import com.coremedia.iso.g;
import com.googlecode.mp4parser.AbstractFullBox;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.mp4parser.aspectj.a.b.b;

public class TfrfBox extends AbstractFullBox {
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_0;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_1;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_2;
   public List entries = new ArrayList();

   static {
      ajc$preClinit();
   }

   public TfrfBox() {
      super("uuid");
   }

   private static void ajc$preClinit() {
      b var0 = new b("TfrfBox.java", TfrfBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getFragmentCount", "com.googlecode.mp4parser.boxes.piff.TfrfBox", "", "", "", "long"), 91);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "getEntries", "com.googlecode.mp4parser.boxes.piff.TfrfBox", "", "", "", "java.util.List"), 95);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "toString", "com.googlecode.mp4parser.boxes.piff.TfrfBox", "", "", "", "java.lang.String"), 100);
   }

   public void _parseDetails(ByteBuffer var1) {
      this.parseVersionAndFlags(var1);
      int var3 = e.d(var1);

      for(int var2 = 0; var2 < var3; ++var2) {
         TfrfBox.a var4 = new TfrfBox.a();
         if(this.getVersion() == 1) {
            var4.a = e.f(var1);
            var4.b = e.f(var1);
         } else {
            var4.a = e.a(var1);
            var4.b = e.a(var1);
         }

         this.entries.add(var4);
      }

   }

   protected void getContent(ByteBuffer var1) {
      this.writeVersionAndFlags(var1);
      g.c(var1, this.entries.size());
      Iterator var2 = this.entries.iterator();

      while(var2.hasNext()) {
         TfrfBox.a var3 = (TfrfBox.a)var2.next();
         if(this.getVersion() == 1) {
            g.a(var1, var3.a);
            g.a(var1, var3.b);
         } else {
            g.b(var1, var3.a);
            g.b(var1, var3.b);
         }
      }

   }

   protected long getContentSize() {
      int var2 = this.entries.size();
      byte var1;
      if(this.getVersion() == 1) {
         var1 = 16;
      } else {
         var1 = 8;
      }

      return (long)(var1 * var2 + 5);
   }

   public List getEntries() {
      org.mp4parser.aspectj.lang.a var1 = b.a(ajc$tjp_1, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.entries;
   }

   public long getFragmentCount() {
      org.mp4parser.aspectj.lang.a var1 = b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return (long)this.entries.size();
   }

   public byte[] getUserType() {
      return new byte[]{-44, -128, 126, -14, -54, 57, 70, -107, -114, 84, 38, -53, -98, 70, -89, -97};
   }

   public String toString() {
      org.mp4parser.aspectj.lang.a var1 = b.a(ajc$tjp_2, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      StringBuilder var2 = new StringBuilder();
      var2.append("TfrfBox");
      var2.append("{entries=").append(this.entries);
      var2.append('}');
      return var2.toString();
   }

   public class a {
      long a;
      long b;

      public String toString() {
         StringBuilder var1 = new StringBuilder();
         var1.append("Entry");
         var1.append("{fragmentAbsoluteTime=").append(this.a);
         var1.append(", fragmentAbsoluteDuration=").append(this.b);
         var1.append('}');
         return var1.toString();
      }
   }
}
