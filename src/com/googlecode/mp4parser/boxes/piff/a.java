package com.googlecode.mp4parser.boxes.piff;

import com.coremedia.iso.c;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public abstract class a {
   protected static Map a = new HashMap();

   public static a a(UUID var0, ByteBuffer var1) {
      Class var5 = (Class)a.get(var0);
      a var6;
      if(var5 != null) {
         try {
            var6 = (a)var5.newInstance();
         } catch (InstantiationException var3) {
            throw new RuntimeException(var3);
         } catch (IllegalAccessException var4) {
            throw new RuntimeException(var4);
         }
      } else {
         var6 = null;
      }

      Object var2 = var6;
      if(var6 == null) {
         var2 = new com.googlecode.mp4parser.a.a();
      }

      ((a)var2).a(var1);
      return (a)var2;
   }

   public abstract ByteBuffer a();

   public abstract void a(ByteBuffer var1);

   public boolean equals(Object var1) {
      throw new RuntimeException("somebody called equals on me but that's not supposed to happen.");
   }

   public String toString() {
      StringBuilder var3 = new StringBuilder();
      var3.append("ProtectionSpecificHeader");
      var3.append("{data=");
      ByteBuffer var1 = this.a().duplicate();
      var1.rewind();
      byte[] var2 = new byte[var1.limit()];
      var1.get(var2);
      var3.append(c.a(var2));
      var3.append('}');
      return var3.toString();
   }
}
