package com.googlecode.mp4parser.boxes.piff;

import com.coremedia.iso.e;
import com.coremedia.iso.g;
import com.googlecode.mp4parser.AbstractFullBox;
import java.nio.ByteBuffer;
import org.mp4parser.aspectj.a.b.b;

public class TfxdBox extends AbstractFullBox {
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_0;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_1;
   public long fragmentAbsoluteDuration;
   public long fragmentAbsoluteTime;

   static {
      ajc$preClinit();
   }

   public TfxdBox() {
      super("uuid");
   }

   private static void ajc$preClinit() {
      b var0 = new b("TfxdBox.java", TfxdBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getFragmentAbsoluteTime", "com.googlecode.mp4parser.boxes.piff.TfxdBox", "", "", "", "long"), 79);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "getFragmentAbsoluteDuration", "com.googlecode.mp4parser.boxes.piff.TfxdBox", "", "", "", "long"), 83);
   }

   public void _parseDetails(ByteBuffer var1) {
      this.parseVersionAndFlags(var1);
      if(this.getVersion() == 1) {
         this.fragmentAbsoluteTime = e.f(var1);
         this.fragmentAbsoluteDuration = e.f(var1);
      } else {
         this.fragmentAbsoluteTime = e.a(var1);
         this.fragmentAbsoluteDuration = e.a(var1);
      }

   }

   protected void getContent(ByteBuffer var1) {
      this.writeVersionAndFlags(var1);
      if(this.getVersion() == 1) {
         g.a(var1, this.fragmentAbsoluteTime);
         g.a(var1, this.fragmentAbsoluteDuration);
      } else {
         g.b(var1, this.fragmentAbsoluteTime);
         g.b(var1, this.fragmentAbsoluteDuration);
      }

   }

   protected long getContentSize() {
      byte var1;
      if(this.getVersion() == 1) {
         var1 = 20;
      } else {
         var1 = 12;
      }

      return (long)var1;
   }

   public long getFragmentAbsoluteDuration() {
      org.mp4parser.aspectj.lang.a var1 = b.a(ajc$tjp_1, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.fragmentAbsoluteDuration;
   }

   public long getFragmentAbsoluteTime() {
      org.mp4parser.aspectj.lang.a var1 = b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.fragmentAbsoluteTime;
   }

   public byte[] getUserType() {
      return new byte[]{109, 29, -101, 5, 66, -43, 68, -26, -128, -30, 20, 29, -81, -9, 87, -78};
   }
}
