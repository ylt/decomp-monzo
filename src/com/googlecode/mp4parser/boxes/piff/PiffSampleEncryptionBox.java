package com.googlecode.mp4parser.boxes.piff;

import com.googlecode.mp4parser.e;
import com.googlecode.mp4parser.boxes.AbstractSampleEncryptionBox;
import org.mp4parser.aspectj.a.b.b;

public class PiffSampleEncryptionBox extends AbstractSampleEncryptionBox {
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_0;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_1;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_2;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_3;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_4;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_5;

   static {
      ajc$preClinit();
   }

   public PiffSampleEncryptionBox() {
      super("uuid");
   }

   private static void ajc$preClinit() {
      b var0 = new b("PiffSampleEncryptionBox.java", PiffSampleEncryptionBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getAlgorithmId", "com.googlecode.mp4parser.boxes.piff.PiffSampleEncryptionBox", "", "", "", "int"), 46);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "setAlgorithmId", "com.googlecode.mp4parser.boxes.piff.PiffSampleEncryptionBox", "int", "algorithmId", "", "void"), 50);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "getIvSize", "com.googlecode.mp4parser.boxes.piff.PiffSampleEncryptionBox", "", "", "", "int"), 54);
      ajc$tjp_3 = var0.a("method-execution", var0.a("1", "setIvSize", "com.googlecode.mp4parser.boxes.piff.PiffSampleEncryptionBox", "int", "ivSize", "", "void"), 58);
      ajc$tjp_4 = var0.a("method-execution", var0.a("1", "getKid", "com.googlecode.mp4parser.boxes.piff.PiffSampleEncryptionBox", "", "", "", "[B"), 62);
      ajc$tjp_5 = var0.a("method-execution", var0.a("1", "setKid", "com.googlecode.mp4parser.boxes.piff.PiffSampleEncryptionBox", "[B", "kid", "", "void"), 66);
   }

   public int getAlgorithmId() {
      org.mp4parser.aspectj.lang.a var1 = b.a(ajc$tjp_0, this, this);
      e.a().a(var1);
      return this.algorithmId;
   }

   public int getIvSize() {
      org.mp4parser.aspectj.lang.a var1 = b.a(ajc$tjp_2, this, this);
      e.a().a(var1);
      return this.ivSize;
   }

   public byte[] getKid() {
      org.mp4parser.aspectj.lang.a var1 = b.a(ajc$tjp_4, this, this);
      e.a().a(var1);
      return this.kid;
   }

   public byte[] getUserType() {
      return new byte[]{-94, 57, 79, 82, 90, -101, 79, 20, -94, 68, 108, 66, 124, 100, -115, -12};
   }

   public boolean isOverrideTrackEncryptionBoxParameters() {
      boolean var1;
      if((this.getFlags() & 1) > 0) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public void setAlgorithmId(int var1) {
      org.mp4parser.aspectj.lang.a var2 = b.a(ajc$tjp_1, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      e.a().a(var2);
      this.algorithmId = var1;
   }

   public void setIvSize(int var1) {
      org.mp4parser.aspectj.lang.a var2 = b.a(ajc$tjp_3, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      e.a().a(var2);
      this.ivSize = var1;
   }

   public void setKid(byte[] var1) {
      org.mp4parser.aspectj.lang.a var2 = b.a(ajc$tjp_5, this, this, (Object)var1);
      e.a().a(var2);
      this.kid = var1;
   }

   public void setOverrideTrackEncryptionBoxParameters(boolean var1) {
      if(var1) {
         this.setFlags(this.getFlags() | 1);
      } else {
         this.setFlags(this.getFlags() & 16777214);
      }

   }
}
