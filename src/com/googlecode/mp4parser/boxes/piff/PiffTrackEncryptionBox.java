package com.googlecode.mp4parser.boxes.piff;

import com.googlecode.mp4parser.e;
import com.googlecode.mp4parser.boxes.AbstractTrackEncryptionBox;
import org.mp4parser.aspectj.a.b.b;

public class PiffTrackEncryptionBox extends AbstractTrackEncryptionBox {
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_0;

   static {
      ajc$preClinit();
   }

   public PiffTrackEncryptionBox() {
      super("uuid");
   }

   private static void ajc$preClinit() {
      b var0 = new b("PiffTrackEncryptionBox.java", PiffTrackEncryptionBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getFlags", "com.googlecode.mp4parser.boxes.piff.PiffTrackEncryptionBox", "", "", "", "int"), 29);
   }

   public int getFlags() {
      org.mp4parser.aspectj.lang.a var1 = b.a(ajc$tjp_0, this, this);
      e.a().a(var1);
      return 0;
   }

   public byte[] getUserType() {
      return new byte[]{-119, 116, -37, -50, 123, -25, 76, 81, -124, -7, 113, 72, -7, -120, 37, 84};
   }
}
