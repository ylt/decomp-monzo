package com.googlecode.mp4parser.boxes.piff;

import com.coremedia.iso.e;
import com.coremedia.iso.g;
import com.googlecode.mp4parser.AbstractFullBox;
import com.googlecode.mp4parser.c.k;
import java.nio.ByteBuffer;
import java.util.UUID;
import org.mp4parser.aspectj.a.b.b;

public class UuidBasedProtectionSystemSpecificHeaderBox extends AbstractFullBox {
   public static byte[] USER_TYPE;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_0;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_1;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_2;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_3;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_4;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_5;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_6;
   a protectionSpecificHeader;
   UUID systemId;

   static {
      ajc$preClinit();
      USER_TYPE = new byte[]{-48, -118, 79, 24, 16, -13, 74, -126, -74, -56, 50, -40, -85, -95, -125, -45};
   }

   public UuidBasedProtectionSystemSpecificHeaderBox() {
      super("uuid", USER_TYPE);
   }

   private static void ajc$preClinit() {
      b var0 = new b("UuidBasedProtectionSystemSpecificHeaderBox.java", UuidBasedProtectionSystemSpecificHeaderBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getSystemId", "com.googlecode.mp4parser.boxes.piff.UuidBasedProtectionSystemSpecificHeaderBox", "", "", "", "java.util.UUID"), 67);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "setSystemId", "com.googlecode.mp4parser.boxes.piff.UuidBasedProtectionSystemSpecificHeaderBox", "java.util.UUID", "systemId", "", "void"), 71);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "getSystemIdString", "com.googlecode.mp4parser.boxes.piff.UuidBasedProtectionSystemSpecificHeaderBox", "", "", "", "java.lang.String"), 75);
      ajc$tjp_3 = var0.a("method-execution", var0.a("1", "getProtectionSpecificHeader", "com.googlecode.mp4parser.boxes.piff.UuidBasedProtectionSystemSpecificHeaderBox", "", "", "", "com.googlecode.mp4parser.boxes.piff.ProtectionSpecificHeader"), 79);
      ajc$tjp_4 = var0.a("method-execution", var0.a("1", "getProtectionSpecificHeaderString", "com.googlecode.mp4parser.boxes.piff.UuidBasedProtectionSystemSpecificHeaderBox", "", "", "", "java.lang.String"), 83);
      ajc$tjp_5 = var0.a("method-execution", var0.a("1", "setProtectionSpecificHeader", "com.googlecode.mp4parser.boxes.piff.UuidBasedProtectionSystemSpecificHeaderBox", "com.googlecode.mp4parser.boxes.piff.ProtectionSpecificHeader", "protectionSpecificHeader", "", "void"), 87);
      ajc$tjp_6 = var0.a("method-execution", var0.a("1", "toString", "com.googlecode.mp4parser.boxes.piff.UuidBasedProtectionSystemSpecificHeaderBox", "", "", "", "java.lang.String"), 92);
   }

   protected void _parseDetails(ByteBuffer var1) {
      this.parseVersionAndFlags(var1);
      byte[] var2 = new byte[16];
      var1.get(var2);
      this.systemId = k.a(var2);
      com.googlecode.mp4parser.c.b.a(e.a(var1));
      this.protectionSpecificHeader = a.a(this.systemId, var1);
   }

   protected void getContent(ByteBuffer var1) {
      this.writeVersionAndFlags(var1);
      g.a(var1, this.systemId.getMostSignificantBits());
      g.a(var1, this.systemId.getLeastSignificantBits());
      ByteBuffer var2 = this.protectionSpecificHeader.a();
      var2.rewind();
      g.b(var1, (long)var2.limit());
      var1.put(var2);
   }

   protected long getContentSize() {
      return (long)(this.protectionSpecificHeader.a().limit() + 24);
   }

   public a getProtectionSpecificHeader() {
      org.mp4parser.aspectj.lang.a var1 = b.a(ajc$tjp_3, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.protectionSpecificHeader;
   }

   public String getProtectionSpecificHeaderString() {
      org.mp4parser.aspectj.lang.a var1 = b.a(ajc$tjp_4, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.protectionSpecificHeader.toString();
   }

   public UUID getSystemId() {
      org.mp4parser.aspectj.lang.a var1 = b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.systemId;
   }

   public String getSystemIdString() {
      org.mp4parser.aspectj.lang.a var1 = b.a(ajc$tjp_2, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.systemId.toString();
   }

   public byte[] getUserType() {
      return USER_TYPE;
   }

   public void setProtectionSpecificHeader(a var1) {
      org.mp4parser.aspectj.lang.a var2 = b.a(ajc$tjp_5, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.protectionSpecificHeader = var1;
   }

   public void setSystemId(UUID var1) {
      org.mp4parser.aspectj.lang.a var2 = b.a(ajc$tjp_1, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.systemId = var1;
   }

   public String toString() {
      org.mp4parser.aspectj.lang.a var1 = b.a(ajc$tjp_6, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      StringBuilder var2 = new StringBuilder();
      var2.append("UuidBasedProtectionSystemSpecificHeaderBox");
      var2.append("{systemId=").append(this.systemId.toString());
      var2.append(", dataSize=").append(this.protectionSpecificHeader.a().limit());
      var2.append('}');
      return var2.toString();
   }
}
