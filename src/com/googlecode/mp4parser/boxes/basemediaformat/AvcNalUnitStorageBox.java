package com.googlecode.mp4parser.boxes.basemediaformat;

import com.googlecode.mp4parser.AbstractBox;
import com.googlecode.mp4parser.e;
import com.mp4parser.iso14496.part15.AvcConfigurationBox;
import java.nio.ByteBuffer;
import java.util.List;
import org.mp4parser.aspectj.a.b.b;
import org.mp4parser.aspectj.lang.a;

public class AvcNalUnitStorageBox extends AbstractBox {
   public static final String TYPE = "avcn";
   private static final a.a ajc$tjp_0;
   private static final a.a ajc$tjp_1;
   private static final a.a ajc$tjp_2;
   private static final a.a ajc$tjp_3;
   private static final a.a ajc$tjp_4;
   private static final a.a ajc$tjp_5;
   private static final a.a ajc$tjp_6;
   private static final a.a ajc$tjp_7;
   com.mp4parser.iso14496.part15.a avcDecoderConfigurationRecord;

   static {
      ajc$preClinit();
   }

   public AvcNalUnitStorageBox() {
      super("avcn");
   }

   public AvcNalUnitStorageBox(AvcConfigurationBox var1) {
      super("avcn");
      this.avcDecoderConfigurationRecord = var1.getavcDecoderConfigurationRecord();
   }

   private static void ajc$preClinit() {
      b var0 = new b("AvcNalUnitStorageBox.java", AvcNalUnitStorageBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getAvcDecoderConfigurationRecord", "com.googlecode.mp4parser.boxes.basemediaformat.AvcNalUnitStorageBox", "", "", "", "com.mp4parser.iso14496.part15.AvcDecoderConfigurationRecord"), 44);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "getLengthSizeMinusOne", "com.googlecode.mp4parser.boxes.basemediaformat.AvcNalUnitStorageBox", "", "", "", "int"), 49);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "getSPS", "com.googlecode.mp4parser.boxes.basemediaformat.AvcNalUnitStorageBox", "", "", "", "[Ljava.lang.String;"), 53);
      ajc$tjp_3 = var0.a("method-execution", var0.a("1", "getPPS", "com.googlecode.mp4parser.boxes.basemediaformat.AvcNalUnitStorageBox", "", "", "", "[Ljava.lang.String;"), 57);
      ajc$tjp_4 = var0.a("method-execution", var0.a("1", "getSequenceParameterSetsAsStrings", "com.googlecode.mp4parser.boxes.basemediaformat.AvcNalUnitStorageBox", "", "", "", "java.util.List"), 61);
      ajc$tjp_5 = var0.a("method-execution", var0.a("1", "getSequenceParameterSetExtsAsStrings", "com.googlecode.mp4parser.boxes.basemediaformat.AvcNalUnitStorageBox", "", "", "", "java.util.List"), 65);
      ajc$tjp_6 = var0.a("method-execution", var0.a("1", "getPictureParameterSetsAsStrings", "com.googlecode.mp4parser.boxes.basemediaformat.AvcNalUnitStorageBox", "", "", "", "java.util.List"), 69);
      ajc$tjp_7 = var0.a("method-execution", var0.a("1", "toString", "com.googlecode.mp4parser.boxes.basemediaformat.AvcNalUnitStorageBox", "", "", "", "java.lang.String"), 89);
   }

   public void _parseDetails(ByteBuffer var1) {
      this.avcDecoderConfigurationRecord = new com.mp4parser.iso14496.part15.a(var1);
   }

   public com.mp4parser.iso14496.part15.a getAvcDecoderConfigurationRecord() {
      a var1 = b.a(ajc$tjp_0, this, this);
      e.a().a(var1);
      return this.avcDecoderConfigurationRecord;
   }

   protected void getContent(ByteBuffer var1) {
      this.avcDecoderConfigurationRecord.a(var1);
   }

   protected long getContentSize() {
      return this.avcDecoderConfigurationRecord.a();
   }

   public int getLengthSizeMinusOne() {
      a var1 = b.a(ajc$tjp_1, this, this);
      e.a().a(var1);
      return this.avcDecoderConfigurationRecord.e;
   }

   public String[] getPPS() {
      a var1 = b.a(ajc$tjp_3, this, this);
      e.a().a(var1);
      return this.avcDecoderConfigurationRecord.b();
   }

   public List getPictureParameterSetsAsStrings() {
      a var1 = b.a(ajc$tjp_6, this, this);
      e.a().a(var1);
      return this.avcDecoderConfigurationRecord.f();
   }

   public String[] getSPS() {
      a var1 = b.a(ajc$tjp_2, this, this);
      e.a().a(var1);
      return this.avcDecoderConfigurationRecord.c();
   }

   public List getSequenceParameterSetExtsAsStrings() {
      a var1 = b.a(ajc$tjp_5, this, this);
      e.a().a(var1);
      return this.avcDecoderConfigurationRecord.e();
   }

   public List getSequenceParameterSetsAsStrings() {
      a var1 = b.a(ajc$tjp_4, this, this);
      e.a().a(var1);
      return this.avcDecoderConfigurationRecord.d();
   }

   public String toString() {
      a var1 = b.a(ajc$tjp_7, this, this);
      e.a().a(var1);
      return "AvcNalUnitStorageBox{SPS=" + this.avcDecoderConfigurationRecord.d() + ",PPS=" + this.avcDecoderConfigurationRecord.f() + ",lengthSize=" + (this.avcDecoderConfigurationRecord.e + 1) + '}';
   }
}
