package com.googlecode.mp4parser.boxes.threegpp26245;

import com.coremedia.iso.e;
import com.coremedia.iso.g;
import com.coremedia.iso.j;
import com.googlecode.mp4parser.AbstractBox;
import java.nio.ByteBuffer;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import org.mp4parser.aspectj.a.b.b;

public class FontTableBox extends AbstractBox {
   public static final String TYPE = "ftab";
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_0;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_1;
   List entries = new LinkedList();

   static {
      ajc$preClinit();
   }

   public FontTableBox() {
      super("ftab");
   }

   private static void ajc$preClinit() {
      b var0 = new b("FontTableBox.java", FontTableBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getEntries", "com.googlecode.mp4parser.boxes.threegpp26245.FontTableBox", "", "", "", "java.util.List"), 52);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "setEntries", "com.googlecode.mp4parser.boxes.threegpp26245.FontTableBox", "java.util.List", "entries", "", "void"), 56);
   }

   public void _parseDetails(ByteBuffer var1) {
      int var3 = e.c(var1);

      for(int var2 = 0; var2 < var3; ++var2) {
         FontTableBox.a var4 = new FontTableBox.a();
         var4.a(var1);
         this.entries.add(var4);
      }

   }

   protected void getContent(ByteBuffer var1) {
      g.b(var1, this.entries.size());
      Iterator var2 = this.entries.iterator();

      while(var2.hasNext()) {
         ((FontTableBox.a)var2.next()).b(var1);
      }

   }

   protected long getContentSize() {
      Iterator var2 = this.entries.iterator();

      int var1;
      for(var1 = 2; var2.hasNext(); var1 += ((FontTableBox.a)var2.next()).a()) {
         ;
      }

      return (long)var1;
   }

   public List getEntries() {
      org.mp4parser.aspectj.lang.a var1 = b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.entries;
   }

   public void setEntries(List var1) {
      org.mp4parser.aspectj.lang.a var2 = b.a(ajc$tjp_1, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.entries = var1;
   }

   public static class a {
      int a;
      String b;

      public int a() {
         return j.b(this.b) + 3;
      }

      public void a(ByteBuffer var1) {
         this.a = e.c(var1);
         this.b = e.a(var1, e.d(var1));
      }

      public void b(ByteBuffer var1) {
         g.b(var1, this.a);
         g.c(var1, this.b.length());
         var1.put(j.a(this.b));
      }

      public String toString() {
         return "FontRecord{fontId=" + this.a + ", fontname='" + this.b + '\'' + '}';
      }
   }
}
