package com.googlecode.mp4parser.boxes;

import com.coremedia.iso.e;
import com.coremedia.iso.g;
import com.googlecode.mp4parser.AbstractFullBox;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.UUID;
import org.mp4parser.aspectj.a.b.b;
import org.mp4parser.aspectj.lang.a;

public abstract class AbstractTrackEncryptionBox extends AbstractFullBox {
   private static final a.a ajc$tjp_0;
   private static final a.a ajc$tjp_1;
   private static final a.a ajc$tjp_2;
   private static final a.a ajc$tjp_3;
   private static final a.a ajc$tjp_4;
   private static final a.a ajc$tjp_5;
   private static final a.a ajc$tjp_6;
   private static final a.a ajc$tjp_7;
   int defaultAlgorithmId;
   int defaultIvSize;
   byte[] default_KID;

   static {
      ajc$preClinit();
   }

   protected AbstractTrackEncryptionBox(String var1) {
      super(var1);
   }

   private static void ajc$preClinit() {
      b var0 = new b("AbstractTrackEncryptionBox.java", AbstractTrackEncryptionBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getDefaultAlgorithmId", "com.googlecode.mp4parser.boxes.AbstractTrackEncryptionBox", "", "", "", "int"), 24);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "setDefaultAlgorithmId", "com.googlecode.mp4parser.boxes.AbstractTrackEncryptionBox", "int", "defaultAlgorithmId", "", "void"), 28);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "getDefaultIvSize", "com.googlecode.mp4parser.boxes.AbstractTrackEncryptionBox", "", "", "", "int"), 32);
      ajc$tjp_3 = var0.a("method-execution", var0.a("1", "setDefaultIvSize", "com.googlecode.mp4parser.boxes.AbstractTrackEncryptionBox", "int", "defaultIvSize", "", "void"), 36);
      ajc$tjp_4 = var0.a("method-execution", var0.a("1", "getDefault_KID", "com.googlecode.mp4parser.boxes.AbstractTrackEncryptionBox", "", "", "", "java.util.UUID"), 40);
      ajc$tjp_5 = var0.a("method-execution", var0.a("1", "setDefault_KID", "com.googlecode.mp4parser.boxes.AbstractTrackEncryptionBox", "java.util.UUID", "uuid", "", "void"), 46);
      ajc$tjp_6 = var0.a("method-execution", var0.a("1", "equals", "com.googlecode.mp4parser.boxes.AbstractTrackEncryptionBox", "java.lang.Object", "o", "", "boolean"), 76);
      ajc$tjp_7 = var0.a("method-execution", var0.a("1", "hashCode", "com.googlecode.mp4parser.boxes.AbstractTrackEncryptionBox", "", "", "", "int"), 90);
   }

   public void _parseDetails(ByteBuffer var1) {
      this.parseVersionAndFlags(var1);
      this.defaultAlgorithmId = e.b(var1);
      this.defaultIvSize = e.d(var1);
      this.default_KID = new byte[16];
      var1.get(this.default_KID);
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      a var3 = b.a(ajc$tjp_6, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var3);
      if(this != var1) {
         if(var1 != null && this.getClass() == var1.getClass()) {
            AbstractTrackEncryptionBox var4 = (AbstractTrackEncryptionBox)var1;
            if(this.defaultAlgorithmId != var4.defaultAlgorithmId) {
               var2 = false;
            } else if(this.defaultIvSize != var4.defaultIvSize) {
               var2 = false;
            } else if(!Arrays.equals(this.default_KID, var4.default_KID)) {
               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   protected void getContent(ByteBuffer var1) {
      this.writeVersionAndFlags(var1);
      g.a(var1, this.defaultAlgorithmId);
      g.c(var1, this.defaultIvSize);
      var1.put(this.default_KID);
   }

   protected long getContentSize() {
      return 24L;
   }

   public int getDefaultAlgorithmId() {
      a var1 = b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.defaultAlgorithmId;
   }

   public int getDefaultIvSize() {
      a var1 = b.a(ajc$tjp_2, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.defaultIvSize;
   }

   public UUID getDefault_KID() {
      a var1 = b.a(ajc$tjp_4, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      ByteBuffer var2 = ByteBuffer.wrap(this.default_KID);
      var2.order(ByteOrder.BIG_ENDIAN);
      return new UUID(var2.getLong(), var2.getLong());
   }

   public int hashCode() {
      a var4 = b.a(ajc$tjp_7, this, this);
      com.googlecode.mp4parser.e.a().a(var4);
      int var2 = this.defaultAlgorithmId;
      int var3 = this.defaultIvSize;
      int var1;
      if(this.default_KID != null) {
         var1 = Arrays.hashCode(this.default_KID);
      } else {
         var1 = 0;
      }

      return var1 + (var2 * 31 + var3) * 31;
   }

   public void setDefaultAlgorithmId(int var1) {
      a var2 = b.a(ajc$tjp_1, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.defaultAlgorithmId = var1;
   }

   public void setDefaultIvSize(int var1) {
      a var2 = b.a(ajc$tjp_3, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.defaultIvSize = var1;
   }

   public void setDefault_KID(UUID var1) {
      a var2 = b.a(ajc$tjp_5, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      ByteBuffer var3 = ByteBuffer.wrap(new byte[16]);
      var3.putLong(var1.getMostSignificantBits());
      var3.putLong(var1.getLeastSignificantBits());
      this.default_KID = var3.array();
   }
}
