package com.googlecode.mp4parser.boxes.threegpp26244;

import com.coremedia.iso.e;
import com.coremedia.iso.g;
import com.googlecode.mp4parser.AbstractFullBox;
import com.googlecode.mp4parser.boxes.mp4.a.c;
import com.googlecode.mp4parser.boxes.mp4.a.d;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.mp4parser.aspectj.a.b.b;

public class SegmentIndexBox extends AbstractFullBox {
   public static final String TYPE = "sidx";
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_0;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_1;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_10;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_11;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_12;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_2;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_3;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_4;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_5;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_6;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_7;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_8;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_9;
   long earliestPresentationTime;
   List entries = new ArrayList();
   long firstOffset;
   long referenceId;
   int reserved;
   long timeScale;

   static {
      ajc$preClinit();
   }

   public SegmentIndexBox() {
      super("sidx");
   }

   private static void ajc$preClinit() {
      b var0 = new b("SegmentIndexBox.java", SegmentIndexBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getEntries", "com.googlecode.mp4parser.boxes.threegpp26244.SegmentIndexBox", "", "", "", "java.util.List"), 128);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "setEntries", "com.googlecode.mp4parser.boxes.threegpp26244.SegmentIndexBox", "java.util.List", "entries", "", "void"), 132);
      ajc$tjp_10 = var0.a("method-execution", var0.a("1", "getReserved", "com.googlecode.mp4parser.boxes.threegpp26244.SegmentIndexBox", "", "", "", "int"), 168);
      ajc$tjp_11 = var0.a("method-execution", var0.a("1", "setReserved", "com.googlecode.mp4parser.boxes.threegpp26244.SegmentIndexBox", "int", "reserved", "", "void"), 172);
      ajc$tjp_12 = var0.a("method-execution", var0.a("1", "toString", "com.googlecode.mp4parser.boxes.threegpp26244.SegmentIndexBox", "", "", "", "java.lang.String"), 298);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "getReferenceId", "com.googlecode.mp4parser.boxes.threegpp26244.SegmentIndexBox", "", "", "", "long"), 136);
      ajc$tjp_3 = var0.a("method-execution", var0.a("1", "setReferenceId", "com.googlecode.mp4parser.boxes.threegpp26244.SegmentIndexBox", "long", "referenceId", "", "void"), 140);
      ajc$tjp_4 = var0.a("method-execution", var0.a("1", "getTimeScale", "com.googlecode.mp4parser.boxes.threegpp26244.SegmentIndexBox", "", "", "", "long"), 144);
      ajc$tjp_5 = var0.a("method-execution", var0.a("1", "setTimeScale", "com.googlecode.mp4parser.boxes.threegpp26244.SegmentIndexBox", "long", "timeScale", "", "void"), 148);
      ajc$tjp_6 = var0.a("method-execution", var0.a("1", "getEarliestPresentationTime", "com.googlecode.mp4parser.boxes.threegpp26244.SegmentIndexBox", "", "", "", "long"), 152);
      ajc$tjp_7 = var0.a("method-execution", var0.a("1", "setEarliestPresentationTime", "com.googlecode.mp4parser.boxes.threegpp26244.SegmentIndexBox", "long", "earliestPresentationTime", "", "void"), 156);
      ajc$tjp_8 = var0.a("method-execution", var0.a("1", "getFirstOffset", "com.googlecode.mp4parser.boxes.threegpp26244.SegmentIndexBox", "", "", "", "long"), 160);
      ajc$tjp_9 = var0.a("method-execution", var0.a("1", "setFirstOffset", "com.googlecode.mp4parser.boxes.threegpp26244.SegmentIndexBox", "long", "firstOffset", "", "void"), 164);
   }

   protected void _parseDetails(ByteBuffer var1) {
      this.parseVersionAndFlags(var1);
      this.referenceId = e.a(var1);
      this.timeScale = e.a(var1);
      if(this.getVersion() == 0) {
         this.earliestPresentationTime = e.a(var1);
         this.firstOffset = e.a(var1);
      } else {
         this.earliestPresentationTime = e.f(var1);
         this.firstOffset = e.f(var1);
      }

      this.reserved = e.c(var1);
      int var3 = e.c(var1);

      for(int var2 = 0; var2 < var3; ++var2) {
         c var5 = new c(var1);
         SegmentIndexBox.a var4 = new SegmentIndexBox.a();
         var4.a((byte)var5.a(1));
         var4.a(var5.a(31));
         var4.a(e.a(var1));
         var5 = new c(var1);
         var4.b((byte)var5.a(1));
         var4.c((byte)var5.a(3));
         var4.b(var5.a(28));
         this.entries.add(var4);
      }

   }

   protected void getContent(ByteBuffer var1) {
      this.writeVersionAndFlags(var1);
      g.b(var1, this.referenceId);
      g.b(var1, this.timeScale);
      if(this.getVersion() == 0) {
         g.b(var1, this.earliestPresentationTime);
         g.b(var1, this.firstOffset);
      } else {
         g.a(var1, this.earliestPresentationTime);
         g.a(var1, this.firstOffset);
      }

      g.b(var1, this.reserved);
      g.b(var1, this.entries.size());
      Iterator var3 = this.entries.iterator();

      while(var3.hasNext()) {
         SegmentIndexBox.a var2 = (SegmentIndexBox.a)var3.next();
         d var4 = new d(var1);
         var4.a(var2.a(), 1);
         var4.a(var2.b(), 31);
         g.b(var1, var2.c());
         var4 = new d(var1);
         var4.a(var2.d(), 1);
         var4.a(var2.e(), 3);
         var4.a(var2.f(), 28);
      }

   }

   protected long getContentSize() {
      byte var1;
      if(this.getVersion() == 0) {
         var1 = 8;
      } else {
         var1 = 16;
      }

      return (long)var1 + 4L + 4L + 4L + 2L + 2L + (long)(this.entries.size() * 12);
   }

   public long getEarliestPresentationTime() {
      org.mp4parser.aspectj.lang.a var1 = b.a(ajc$tjp_6, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.earliestPresentationTime;
   }

   public List getEntries() {
      org.mp4parser.aspectj.lang.a var1 = b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.entries;
   }

   public long getFirstOffset() {
      org.mp4parser.aspectj.lang.a var1 = b.a(ajc$tjp_8, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.firstOffset;
   }

   public long getReferenceId() {
      org.mp4parser.aspectj.lang.a var1 = b.a(ajc$tjp_2, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.referenceId;
   }

   public int getReserved() {
      org.mp4parser.aspectj.lang.a var1 = b.a(ajc$tjp_10, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.reserved;
   }

   public long getTimeScale() {
      org.mp4parser.aspectj.lang.a var1 = b.a(ajc$tjp_4, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.timeScale;
   }

   public void setEarliestPresentationTime(long var1) {
      org.mp4parser.aspectj.lang.a var3 = b.a(ajc$tjp_7, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var3);
      this.earliestPresentationTime = var1;
   }

   public void setEntries(List var1) {
      org.mp4parser.aspectj.lang.a var2 = b.a(ajc$tjp_1, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.entries = var1;
   }

   public void setFirstOffset(long var1) {
      org.mp4parser.aspectj.lang.a var3 = b.a(ajc$tjp_9, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var3);
      this.firstOffset = var1;
   }

   public void setReferenceId(long var1) {
      org.mp4parser.aspectj.lang.a var3 = b.a(ajc$tjp_3, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var3);
      this.referenceId = var1;
   }

   public void setReserved(int var1) {
      org.mp4parser.aspectj.lang.a var2 = b.a(ajc$tjp_11, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.reserved = var1;
   }

   public void setTimeScale(long var1) {
      org.mp4parser.aspectj.lang.a var3 = b.a(ajc$tjp_5, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var3);
      this.timeScale = var1;
   }

   public String toString() {
      org.mp4parser.aspectj.lang.a var1 = b.a(ajc$tjp_12, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return "SegmentIndexBox{entries=" + this.entries + ", referenceId=" + this.referenceId + ", timeScale=" + this.timeScale + ", earliestPresentationTime=" + this.earliestPresentationTime + ", firstOffset=" + this.firstOffset + ", reserved=" + this.reserved + '}';
   }

   public static class a {
      byte a;
      int b;
      long c;
      byte d;
      byte e;
      int f;

      public byte a() {
         return this.a;
      }

      public void a(byte var1) {
         this.a = var1;
      }

      public void a(int var1) {
         this.b = var1;
      }

      public void a(long var1) {
         this.c = var1;
      }

      public int b() {
         return this.b;
      }

      public void b(byte var1) {
         this.d = var1;
      }

      public void b(int var1) {
         this.f = var1;
      }

      public long c() {
         return this.c;
      }

      public void c(byte var1) {
         this.e = var1;
      }

      public byte d() {
         return this.d;
      }

      public byte e() {
         return this.e;
      }

      public boolean equals(Object var1) {
         boolean var2 = true;
         if(this != var1) {
            if(var1 != null && this.getClass() == var1.getClass()) {
               SegmentIndexBox.a var3 = (SegmentIndexBox.a)var1;
               if(this.a != var3.a) {
                  var2 = false;
               } else if(this.b != var3.b) {
                  var2 = false;
               } else if(this.f != var3.f) {
                  var2 = false;
               } else if(this.e != var3.e) {
                  var2 = false;
               } else if(this.d != var3.d) {
                  var2 = false;
               } else if(this.c != var3.c) {
                  var2 = false;
               }
            } else {
               var2 = false;
            }
         }

         return var2;
      }

      public int f() {
         return this.f;
      }

      public int hashCode() {
         return ((((this.a * 31 + this.b) * 31 + (int)(this.c ^ this.c >>> 32)) * 31 + this.d) * 31 + this.e) * 31 + this.f;
      }

      public String toString() {
         return "Entry{referenceType=" + this.a + ", referencedSize=" + this.b + ", subsegmentDuration=" + this.c + ", startsWithSap=" + this.d + ", sapType=" + this.e + ", sapDeltaTime=" + this.f + '}';
      }
   }
}
