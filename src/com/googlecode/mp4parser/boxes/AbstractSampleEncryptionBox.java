package com.googlecode.mp4parser.boxes;

import com.coremedia.iso.e;
import com.coremedia.iso.g;
import com.googlecode.mp4parser.AbstractFullBox;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.mp4parser.aspectj.a.b.b;
import org.mp4parser.aspectj.lang.a;

public abstract class AbstractSampleEncryptionBox extends AbstractFullBox {
   private static final a.a ajc$tjp_0;
   private static final a.a ajc$tjp_1;
   private static final a.a ajc$tjp_2;
   private static final a.a ajc$tjp_3;
   private static final a.a ajc$tjp_4;
   private static final a.a ajc$tjp_5;
   protected int algorithmId = -1;
   List entries = Collections.emptyList();
   protected int ivSize = -1;
   protected byte[] kid = new byte[]{-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1};

   static {
      ajc$preClinit();
   }

   protected AbstractSampleEncryptionBox(String var1) {
      super(var1);
   }

   private static void ajc$preClinit() {
      b var0 = new b("AbstractSampleEncryptionBox.java", AbstractSampleEncryptionBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getOffsetToFirstIV", "com.googlecode.mp4parser.boxes.AbstractSampleEncryptionBox", "", "", "", "int"), 29);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "getEntries", "com.googlecode.mp4parser.boxes.AbstractSampleEncryptionBox", "", "", "", "java.util.List"), 89);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "setEntries", "com.googlecode.mp4parser.boxes.AbstractSampleEncryptionBox", "java.util.List", "entries", "", "void"), 93);
      ajc$tjp_3 = var0.a("method-execution", var0.a("1", "equals", "com.googlecode.mp4parser.boxes.AbstractSampleEncryptionBox", "java.lang.Object", "o", "", "boolean"), 173);
      ajc$tjp_4 = var0.a("method-execution", var0.a("1", "hashCode", "com.googlecode.mp4parser.boxes.AbstractSampleEncryptionBox", "", "", "", "int"), 200);
      ajc$tjp_5 = var0.a("method-execution", var0.a("1", "getEntrySizes", "com.googlecode.mp4parser.boxes.AbstractSampleEncryptionBox", "", "", "", "java.util.List"), 208);
   }

   private int getNonEmptyEntriesNum() {
      Iterator var2 = this.entries.iterator();
      int var1 = 0;

      while(var2.hasNext()) {
         if(((com.mp4parser.iso23001.part7.a)var2.next()).a() > 0) {
            ++var1;
         }
      }

      return var1;
   }

   private List parseEntries(ByteBuffer param1, long param2, int param4) {
      // $FF: Couldn't be decompiled
   }

   public void _parseDetails(ByteBuffer var1) {
      this.parseVersionAndFlags(var1);
      if((this.getFlags() & 1) > 0) {
         this.algorithmId = e.b(var1);
         this.ivSize = e.d(var1);
         this.kid = new byte[16];
         var1.get(this.kid);
      }

      long var2 = e.a(var1);
      ByteBuffer var4 = var1.duplicate();
      ByteBuffer var5 = var1.duplicate();
      this.entries = this.parseEntries(var4, var2, 8);
      if(this.entries == null) {
         this.entries = this.parseEntries(var5, var2, 16);
         var1.position(var1.position() + var1.remaining() - var5.remaining());
      } else {
         var1.position(var1.position() + var1.remaining() - var4.remaining());
      }

      if(this.entries == null) {
         throw new RuntimeException("Cannot parse SampleEncryptionBox");
      }
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      a var3 = b.a(ajc$tjp_3, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var3);
      if(this != var1) {
         if(var1 != null && this.getClass() == var1.getClass()) {
            AbstractSampleEncryptionBox var4 = (AbstractSampleEncryptionBox)var1;
            if(this.algorithmId != var4.algorithmId) {
               var2 = false;
            } else if(this.ivSize != var4.ivSize) {
               var2 = false;
            } else {
               label38: {
                  if(this.entries != null) {
                     if(this.entries.equals(var4.entries)) {
                        break label38;
                     }
                  } else if(var4.entries == null) {
                     break label38;
                  }

                  var2 = false;
                  return var2;
               }

               if(!Arrays.equals(this.kid, var4.kid)) {
                  var2 = false;
               }
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public void getBox(WritableByteChannel var1) throws IOException {
      super.getBox(var1);
   }

   protected void getContent(ByteBuffer var1) {
      this.writeVersionAndFlags(var1);
      if(this.isOverrideTrackEncryptionBoxParameters()) {
         g.a(var1, this.algorithmId);
         g.c(var1, this.ivSize);
         var1.put(this.kid);
      }

      g.b(var1, (long)this.getNonEmptyEntriesNum());
      Iterator var4 = this.entries.iterator();

      while(true) {
         com.mp4parser.iso23001.part7.a var5;
         do {
            do {
               if(!var4.hasNext()) {
                  return;
               }

               var5 = (com.mp4parser.iso23001.part7.a)var4.next();
            } while(var5.a() <= 0);

            if(var5.a.length != 8 && var5.a.length != 16) {
               throw new RuntimeException("IV must be either 8 or 16 bytes");
            }

            var1.put(var5.a);
         } while(!this.isSubSampleEncryption());

         g.b(var1, var5.b.length);
         com.mp4parser.iso23001.part7.a.j[] var7 = var5.b;
         int var3 = var7.length;

         for(int var2 = 0; var2 < var3; ++var2) {
            com.mp4parser.iso23001.part7.a.j var6 = var7[var2];
            g.b(var1, var6.a());
            g.b(var1, var6.b());
         }
      }
   }

   protected long getContentSize() {
      long var1;
      if(this.isOverrideTrackEncryptionBoxParameters()) {
         var1 = 4L + 4L + (long)this.kid.length;
      } else {
         var1 = 4L;
      }

      Iterator var3 = this.entries.iterator();

      for(var1 += 4L; var3.hasNext(); var1 += (long)((com.mp4parser.iso23001.part7.a)var3.next()).a()) {
         ;
      }

      return var1;
   }

   public List getEntries() {
      a var1 = b.a(ajc$tjp_1, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.entries;
   }

   public List getEntrySizes() {
      a var3 = b.a(ajc$tjp_5, this, this);
      com.googlecode.mp4parser.e.a().a(var3);
      ArrayList var6 = new ArrayList(this.entries.size());

      short var1;
      for(Iterator var5 = this.entries.iterator(); var5.hasNext(); var6.add(Short.valueOf(var1))) {
         com.mp4parser.iso23001.part7.a var4 = (com.mp4parser.iso23001.part7.a)var5.next();
         var1 = (short)var4.a.length;
         if(this.isSubSampleEncryption()) {
            short var2 = (short)(var1 + 2);
            var1 = (short)(var4.b.length * 6 + var2);
         }
      }

      return var6;
   }

   public int getOffsetToFirstIV() {
      a var3 = b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var3);
      byte var1;
      if(this.getSize() > 4294967296L) {
         var1 = 16;
      } else {
         var1 = 8;
      }

      int var2;
      if(this.isOverrideTrackEncryptionBoxParameters()) {
         var2 = this.kid.length + 4;
      } else {
         var2 = 0;
      }

      return var1 + var2 + 4;
   }

   public int hashCode() {
      int var2 = 0;
      a var5 = b.a(ajc$tjp_4, this, this);
      com.googlecode.mp4parser.e.a().a(var5);
      int var3 = this.algorithmId;
      int var4 = this.ivSize;
      int var1;
      if(this.kid != null) {
         var1 = Arrays.hashCode(this.kid);
      } else {
         var1 = 0;
      }

      if(this.entries != null) {
         var2 = this.entries.hashCode();
      }

      return (var1 + (var3 * 31 + var4) * 31) * 31 + var2;
   }

   protected boolean isOverrideTrackEncryptionBoxParameters() {
      boolean var1;
      if((this.getFlags() & 1) > 0) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public boolean isSubSampleEncryption() {
      boolean var1;
      if((this.getFlags() & 2) > 0) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public void setEntries(List var1) {
      a var2 = b.a(ajc$tjp_2, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.entries = var1;
   }

   public void setSubSampleEncryption(boolean var1) {
      if(var1) {
         this.setFlags(this.getFlags() | 2);
      } else {
         this.setFlags(this.getFlags() & 16777213);
      }

   }
}
