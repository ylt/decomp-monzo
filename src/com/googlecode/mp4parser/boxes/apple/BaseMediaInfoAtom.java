package com.googlecode.mp4parser.boxes.apple;

import com.coremedia.iso.e;
import com.coremedia.iso.g;
import com.googlecode.mp4parser.AbstractFullBox;
import java.nio.ByteBuffer;
import org.mp4parser.aspectj.a.b.b;
import org.mp4parser.aspectj.lang.a;

public class BaseMediaInfoAtom extends AbstractFullBox {
   public static final String TYPE = "gmin";
   private static final a.a ajc$tjp_0;
   private static final a.a ajc$tjp_1;
   private static final a.a ajc$tjp_10;
   private static final a.a ajc$tjp_11;
   private static final a.a ajc$tjp_12;
   private static final a.a ajc$tjp_2;
   private static final a.a ajc$tjp_3;
   private static final a.a ajc$tjp_4;
   private static final a.a ajc$tjp_5;
   private static final a.a ajc$tjp_6;
   private static final a.a ajc$tjp_7;
   private static final a.a ajc$tjp_8;
   private static final a.a ajc$tjp_9;
   short balance;
   short graphicsMode = 64;
   int opColorB = '耀';
   int opColorG = '耀';
   int opColorR = '耀';
   short reserved;

   static {
      ajc$preClinit();
   }

   public BaseMediaInfoAtom() {
      super("gmin");
   }

   private static void ajc$preClinit() {
      b var0 = new b("BaseMediaInfoAtom.java", BaseMediaInfoAtom.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getGraphicsMode", "com.googlecode.mp4parser.boxes.apple.BaseMediaInfoAtom", "", "", "", "short"), 54);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "setGraphicsMode", "com.googlecode.mp4parser.boxes.apple.BaseMediaInfoAtom", "short", "graphicsMode", "", "void"), 58);
      ajc$tjp_10 = var0.a("method-execution", var0.a("1", "getReserved", "com.googlecode.mp4parser.boxes.apple.BaseMediaInfoAtom", "", "", "", "short"), 94);
      ajc$tjp_11 = var0.a("method-execution", var0.a("1", "setReserved", "com.googlecode.mp4parser.boxes.apple.BaseMediaInfoAtom", "short", "reserved", "", "void"), 98);
      ajc$tjp_12 = var0.a("method-execution", var0.a("1", "toString", "com.googlecode.mp4parser.boxes.apple.BaseMediaInfoAtom", "", "", "", "java.lang.String"), 103);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "getOpColorR", "com.googlecode.mp4parser.boxes.apple.BaseMediaInfoAtom", "", "", "", "int"), 62);
      ajc$tjp_3 = var0.a("method-execution", var0.a("1", "setOpColorR", "com.googlecode.mp4parser.boxes.apple.BaseMediaInfoAtom", "int", "opColorR", "", "void"), 66);
      ajc$tjp_4 = var0.a("method-execution", var0.a("1", "getOpColorG", "com.googlecode.mp4parser.boxes.apple.BaseMediaInfoAtom", "", "", "", "int"), 70);
      ajc$tjp_5 = var0.a("method-execution", var0.a("1", "setOpColorG", "com.googlecode.mp4parser.boxes.apple.BaseMediaInfoAtom", "int", "opColorG", "", "void"), 74);
      ajc$tjp_6 = var0.a("method-execution", var0.a("1", "getOpColorB", "com.googlecode.mp4parser.boxes.apple.BaseMediaInfoAtom", "", "", "", "int"), 78);
      ajc$tjp_7 = var0.a("method-execution", var0.a("1", "setOpColorB", "com.googlecode.mp4parser.boxes.apple.BaseMediaInfoAtom", "int", "opColorB", "", "void"), 82);
      ajc$tjp_8 = var0.a("method-execution", var0.a("1", "getBalance", "com.googlecode.mp4parser.boxes.apple.BaseMediaInfoAtom", "", "", "", "short"), 86);
      ajc$tjp_9 = var0.a("method-execution", var0.a("1", "setBalance", "com.googlecode.mp4parser.boxes.apple.BaseMediaInfoAtom", "short", "balance", "", "void"), 90);
   }

   protected void _parseDetails(ByteBuffer var1) {
      this.parseVersionAndFlags(var1);
      this.graphicsMode = var1.getShort();
      this.opColorR = e.c(var1);
      this.opColorG = e.c(var1);
      this.opColorB = e.c(var1);
      this.balance = var1.getShort();
      this.reserved = var1.getShort();
   }

   public short getBalance() {
      a var1 = b.a(ajc$tjp_8, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.balance;
   }

   protected void getContent(ByteBuffer var1) {
      this.writeVersionAndFlags(var1);
      var1.putShort(this.graphicsMode);
      g.b(var1, this.opColorR);
      g.b(var1, this.opColorG);
      g.b(var1, this.opColorB);
      var1.putShort(this.balance);
      var1.putShort(this.reserved);
   }

   protected long getContentSize() {
      return 16L;
   }

   public short getGraphicsMode() {
      a var1 = b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.graphicsMode;
   }

   public int getOpColorB() {
      a var1 = b.a(ajc$tjp_6, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.opColorB;
   }

   public int getOpColorG() {
      a var1 = b.a(ajc$tjp_4, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.opColorG;
   }

   public int getOpColorR() {
      a var1 = b.a(ajc$tjp_2, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.opColorR;
   }

   public short getReserved() {
      a var1 = b.a(ajc$tjp_10, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.reserved;
   }

   public void setBalance(short var1) {
      a var2 = b.a(ajc$tjp_9, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.balance = var1;
   }

   public void setGraphicsMode(short var1) {
      a var2 = b.a(ajc$tjp_1, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.graphicsMode = var1;
   }

   public void setOpColorB(int var1) {
      a var2 = b.a(ajc$tjp_7, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.opColorB = var1;
   }

   public void setOpColorG(int var1) {
      a var2 = b.a(ajc$tjp_5, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.opColorG = var1;
   }

   public void setOpColorR(int var1) {
      a var2 = b.a(ajc$tjp_3, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.opColorR = var1;
   }

   public void setReserved(short var1) {
      a var2 = b.a(ajc$tjp_11, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.reserved = var1;
   }

   public String toString() {
      a var1 = b.a(ajc$tjp_12, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return "BaseMediaInfoAtom{graphicsMode=" + this.graphicsMode + ", opColorR=" + this.opColorR + ", opColorG=" + this.opColorG + ", opColorB=" + this.opColorB + ", balance=" + this.balance + ", reserved=" + this.reserved + '}';
   }
}
