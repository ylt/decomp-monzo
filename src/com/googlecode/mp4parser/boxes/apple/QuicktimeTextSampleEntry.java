package com.googlecode.mp4parser.boxes.apple;

import com.coremedia.iso.e;
import com.coremedia.iso.g;
import com.coremedia.iso.boxes.a;
import com.coremedia.iso.boxes.sampleentry.AbstractSampleEntry;
import com.googlecode.mp4parser.b;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;
import java.util.List;

public class QuicktimeTextSampleEntry extends AbstractSampleEntry {
   public static final String TYPE = "text";
   int backgroundB;
   int backgroundG;
   int backgroundR;
   int dataReferenceIndex;
   long defaultTextBox;
   int displayFlags;
   short fontFace;
   String fontName = "";
   short fontNumber;
   int foregroundB = '\uffff';
   int foregroundG = '\uffff';
   int foregroundR = '\uffff';
   long reserved1;
   byte reserved2;
   short reserved3;
   int textJustification;

   public QuicktimeTextSampleEntry() {
      super("text");
   }

   public void addBox(a var1) {
      throw new RuntimeException("QuicktimeTextSampleEntries may not have child boxes");
   }

   public int getBackgroundB() {
      return this.backgroundB;
   }

   public int getBackgroundG() {
      return this.backgroundG;
   }

   public int getBackgroundR() {
      return this.backgroundR;
   }

   public void getBox(WritableByteChannel var1) throws IOException {
      var1.write(this.getHeader());
      int var2;
      if(this.fontName != null) {
         var2 = this.fontName.length();
      } else {
         var2 = 0;
      }

      ByteBuffer var3 = ByteBuffer.allocate(var2 + 52);
      var3.position(6);
      g.b(var3, this.dataReferenceIndex);
      var3.putInt(this.displayFlags);
      var3.putInt(this.textJustification);
      g.b(var3, this.backgroundR);
      g.b(var3, this.backgroundG);
      g.b(var3, this.backgroundB);
      g.a(var3, this.defaultTextBox);
      g.a(var3, this.reserved1);
      var3.putShort(this.fontNumber);
      var3.putShort(this.fontFace);
      var3.put(this.reserved2);
      var3.putShort(this.reserved3);
      g.b(var3, this.foregroundR);
      g.b(var3, this.foregroundG);
      g.b(var3, this.foregroundB);
      if(this.fontName != null) {
         g.c(var3, this.fontName.length());
         var3.put(this.fontName.getBytes());
      }

      var1.write((ByteBuffer)var3.rewind());
   }

   public long getDefaultTextBox() {
      return this.defaultTextBox;
   }

   public int getDisplayFlags() {
      return this.displayFlags;
   }

   public short getFontFace() {
      return this.fontFace;
   }

   public String getFontName() {
      return this.fontName;
   }

   public short getFontNumber() {
      return this.fontNumber;
   }

   public int getForegroundB() {
      return this.foregroundB;
   }

   public int getForegroundG() {
      return this.foregroundG;
   }

   public int getForegroundR() {
      return this.foregroundR;
   }

   public long getReserved1() {
      return this.reserved1;
   }

   public byte getReserved2() {
      return this.reserved2;
   }

   public short getReserved3() {
      return this.reserved3;
   }

   public long getSize() {
      long var2 = this.getContainerSize();
      int var1;
      if(this.fontName != null) {
         var1 = this.fontName.length();
      } else {
         var1 = 0;
      }

      var2 = 52L + var2 + (long)var1;
      byte var4;
      if(!this.largeBox && 8L + var2 < 4294967296L) {
         var4 = 8;
      } else {
         var4 = 16;
      }

      return (long)var4 + var2;
   }

   public int getTextJustification() {
      return this.textJustification;
   }

   public void parse(b var1, ByteBuffer var2, long var3, com.coremedia.iso.b var5) throws IOException {
      var2 = ByteBuffer.allocate(com.googlecode.mp4parser.c.b.a(var3));
      var1.a(var2);
      var2.position(6);
      this.dataReferenceIndex = e.c(var2);
      this.displayFlags = var2.getInt();
      this.textJustification = var2.getInt();
      this.backgroundR = e.c(var2);
      this.backgroundG = e.c(var2);
      this.backgroundB = e.c(var2);
      this.defaultTextBox = e.f(var2);
      this.reserved1 = e.f(var2);
      this.fontNumber = var2.getShort();
      this.fontFace = var2.getShort();
      this.reserved2 = var2.get();
      this.reserved3 = var2.getShort();
      this.foregroundR = e.c(var2);
      this.foregroundG = e.c(var2);
      this.foregroundB = e.c(var2);
      if(var2.remaining() > 0) {
         byte[] var6 = new byte[e.d(var2)];
         var2.get(var6);
         this.fontName = new String(var6);
      } else {
         this.fontName = null;
      }

   }

   public void setBackgroundB(int var1) {
      this.backgroundB = var1;
   }

   public void setBackgroundG(int var1) {
      this.backgroundG = var1;
   }

   public void setBackgroundR(int var1) {
      this.backgroundR = var1;
   }

   public void setBoxes(List var1) {
      throw new RuntimeException("QuicktimeTextSampleEntries may not have child boxes");
   }

   public void setDefaultTextBox(long var1) {
      this.defaultTextBox = var1;
   }

   public void setDisplayFlags(int var1) {
      this.displayFlags = var1;
   }

   public void setFontFace(short var1) {
      this.fontFace = var1;
   }

   public void setFontName(String var1) {
      this.fontName = var1;
   }

   public void setFontNumber(short var1) {
      this.fontNumber = var1;
   }

   public void setForegroundB(int var1) {
      this.foregroundB = var1;
   }

   public void setForegroundG(int var1) {
      this.foregroundG = var1;
   }

   public void setForegroundR(int var1) {
      this.foregroundR = var1;
   }

   public void setReserved1(long var1) {
      this.reserved1 = var1;
   }

   public void setReserved2(byte var1) {
      this.reserved2 = var1;
   }

   public void setReserved3(short var1) {
      this.reserved3 = var1;
   }

   public void setTextJustification(int var1) {
      this.textJustification = var1;
   }
}
