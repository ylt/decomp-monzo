package com.googlecode.mp4parser.boxes.apple;

import com.googlecode.mp4parser.e;
import java.nio.ByteBuffer;
import org.mp4parser.aspectj.a.b.b;
import org.mp4parser.aspectj.lang.a;

public class AppleTrackNumberBox extends AppleDataBox {
   private static final a.a ajc$tjp_0;
   private static final a.a ajc$tjp_1;
   private static final a.a ajc$tjp_2;
   private static final a.a ajc$tjp_3;
   int a;
   int b;

   static {
      ajc$preClinit();
   }

   public AppleTrackNumberBox() {
      super("trkn", 0);
   }

   private static void ajc$preClinit() {
      b var0 = new b("AppleTrackNumberBox.java", AppleTrackNumberBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getA", "com.googlecode.mp4parser.boxes.apple.AppleTrackNumberBox", "", "", "", "int"), 16);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "setA", "com.googlecode.mp4parser.boxes.apple.AppleTrackNumberBox", "int", "a", "", "void"), 20);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "getB", "com.googlecode.mp4parser.boxes.apple.AppleTrackNumberBox", "", "", "", "int"), 24);
      ajc$tjp_3 = var0.a("method-execution", var0.a("1", "setB", "com.googlecode.mp4parser.boxes.apple.AppleTrackNumberBox", "int", "b", "", "void"), 28);
   }

   public int getA() {
      a var1 = b.a(ajc$tjp_0, this, this);
      e.a().a(var1);
      return this.a;
   }

   public int getB() {
      a var1 = b.a(ajc$tjp_2, this, this);
      e.a().a(var1);
      return this.b;
   }

   protected int getDataLength() {
      return 8;
   }

   protected void parseData(ByteBuffer var1) {
      this.a = var1.getInt();
      this.b = var1.getInt();
   }

   public void setA(int var1) {
      a var2 = b.a(ajc$tjp_1, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      e.a().a(var2);
      this.a = var1;
   }

   public void setB(int var1) {
      a var2 = b.a(ajc$tjp_3, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      e.a().a(var2);
      this.b = var1;
   }

   protected byte[] writeData() {
      ByteBuffer var1 = ByteBuffer.allocate(8);
      var1.putInt(this.a);
      var1.putInt(this.b);
      return var1.array();
   }
}
