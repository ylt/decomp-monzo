package com.googlecode.mp4parser.boxes.apple;

import com.coremedia.iso.e;
import com.coremedia.iso.g;
import com.googlecode.mp4parser.AbstractFullBox;
import java.nio.ByteBuffer;
import org.mp4parser.aspectj.a.b.b;
import org.mp4parser.aspectj.lang.a;

public class CleanApertureAtom extends AbstractFullBox {
   public static final String TYPE = "clef";
   private static final a.a ajc$tjp_0;
   private static final a.a ajc$tjp_1;
   private static final a.a ajc$tjp_2;
   private static final a.a ajc$tjp_3;
   double height;
   double width;

   static {
      ajc$preClinit();
   }

   public CleanApertureAtom() {
      super("clef");
   }

   private static void ajc$preClinit() {
      b var0 = new b("CleanApertureAtom.java", CleanApertureAtom.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getWidth", "com.googlecode.mp4parser.boxes.apple.CleanApertureAtom", "", "", "", "double"), 45);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "setWidth", "com.googlecode.mp4parser.boxes.apple.CleanApertureAtom", "double", "width", "", "void"), 49);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "getHeight", "com.googlecode.mp4parser.boxes.apple.CleanApertureAtom", "", "", "", "double"), 53);
      ajc$tjp_3 = var0.a("method-execution", var0.a("1", "setHeight", "com.googlecode.mp4parser.boxes.apple.CleanApertureAtom", "double", "height", "", "void"), 57);
   }

   protected void _parseDetails(ByteBuffer var1) {
      this.parseVersionAndFlags(var1);
      this.width = e.g(var1);
      this.height = e.g(var1);
   }

   protected void getContent(ByteBuffer var1) {
      this.writeVersionAndFlags(var1);
      g.a(var1, this.width);
      g.a(var1, this.height);
   }

   protected long getContentSize() {
      return 12L;
   }

   public double getHeight() {
      a var1 = b.a(ajc$tjp_2, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.height;
   }

   public double getWidth() {
      a var1 = b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.width;
   }

   public void setHeight(double var1) {
      a var3 = b.a(ajc$tjp_3, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var3);
      this.height = var1;
   }

   public void setWidth(double var1) {
      a var3 = b.a(ajc$tjp_1, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var3);
      this.width = var1;
   }
}
