package com.googlecode.mp4parser.boxes.apple;

import com.googlecode.mp4parser.e;
import java.nio.ByteBuffer;
import org.mp4parser.aspectj.a.b.b;
import org.mp4parser.aspectj.lang.a;

public class AppleCoverBox extends AppleDataBox {
   private static final int IMAGE_TYPE_JPG = 13;
   private static final int IMAGE_TYPE_PNG = 14;
   private static final a.a ajc$tjp_0;
   private static final a.a ajc$tjp_1;
   private static final a.a ajc$tjp_2;
   private byte[] data;

   static {
      ajc$preClinit();
   }

   public AppleCoverBox() {
      super("covr", 1);
   }

   private static void ajc$preClinit() {
      b var0 = new b("AppleCoverBox.java", AppleCoverBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getCoverData", "com.googlecode.mp4parser.boxes.apple.AppleCoverBox", "", "", "", "[B"), 21);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "setJpg", "com.googlecode.mp4parser.boxes.apple.AppleCoverBox", "[B", "data", "", "void"), 25);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "setPng", "com.googlecode.mp4parser.boxes.apple.AppleCoverBox", "[B", "data", "", "void"), 29);
   }

   private void setImageData(byte[] var1, int var2) {
      this.data = var1;
      this.dataType = var2;
   }

   public byte[] getCoverData() {
      a var1 = b.a(ajc$tjp_0, this, this);
      e.a().a(var1);
      return this.data;
   }

   protected int getDataLength() {
      return this.data.length;
   }

   protected void parseData(ByteBuffer var1) {
      this.data = new byte[var1.limit()];
      var1.get(this.data);
   }

   public void setJpg(byte[] var1) {
      a var2 = b.a(ajc$tjp_1, this, this, (Object)var1);
      e.a().a(var2);
      this.setImageData(var1, 13);
   }

   public void setPng(byte[] var1) {
      a var2 = b.a(ajc$tjp_2, this, this, (Object)var1);
      e.a().a(var2);
      this.setImageData(var1, 14);
   }

   protected byte[] writeData() {
      return this.data;
   }
}
