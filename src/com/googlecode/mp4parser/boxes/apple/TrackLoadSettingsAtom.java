package com.googlecode.mp4parser.boxes.apple;

import com.googlecode.mp4parser.AbstractBox;
import com.googlecode.mp4parser.e;
import java.nio.ByteBuffer;
import org.mp4parser.aspectj.a.b.b;
import org.mp4parser.aspectj.lang.a;

public class TrackLoadSettingsAtom extends AbstractBox {
   public static final String TYPE = "load";
   private static final a.a ajc$tjp_0;
   private static final a.a ajc$tjp_1;
   private static final a.a ajc$tjp_2;
   private static final a.a ajc$tjp_3;
   private static final a.a ajc$tjp_4;
   private static final a.a ajc$tjp_5;
   private static final a.a ajc$tjp_6;
   private static final a.a ajc$tjp_7;
   int defaultHints;
   int preloadDuration;
   int preloadFlags;
   int preloadStartTime;

   static {
      ajc$preClinit();
   }

   public TrackLoadSettingsAtom() {
      super("load");
   }

   private static void ajc$preClinit() {
      b var0 = new b("TrackLoadSettingsAtom.java", TrackLoadSettingsAtom.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getPreloadStartTime", "com.googlecode.mp4parser.boxes.apple.TrackLoadSettingsAtom", "", "", "", "int"), 49);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "setPreloadStartTime", "com.googlecode.mp4parser.boxes.apple.TrackLoadSettingsAtom", "int", "preloadStartTime", "", "void"), 53);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "getPreloadDuration", "com.googlecode.mp4parser.boxes.apple.TrackLoadSettingsAtom", "", "", "", "int"), 57);
      ajc$tjp_3 = var0.a("method-execution", var0.a("1", "setPreloadDuration", "com.googlecode.mp4parser.boxes.apple.TrackLoadSettingsAtom", "int", "preloadDuration", "", "void"), 61);
      ajc$tjp_4 = var0.a("method-execution", var0.a("1", "getPreloadFlags", "com.googlecode.mp4parser.boxes.apple.TrackLoadSettingsAtom", "", "", "", "int"), 65);
      ajc$tjp_5 = var0.a("method-execution", var0.a("1", "setPreloadFlags", "com.googlecode.mp4parser.boxes.apple.TrackLoadSettingsAtom", "int", "preloadFlags", "", "void"), 69);
      ajc$tjp_6 = var0.a("method-execution", var0.a("1", "getDefaultHints", "com.googlecode.mp4parser.boxes.apple.TrackLoadSettingsAtom", "", "", "", "int"), 73);
      ajc$tjp_7 = var0.a("method-execution", var0.a("1", "setDefaultHints", "com.googlecode.mp4parser.boxes.apple.TrackLoadSettingsAtom", "int", "defaultHints", "", "void"), 77);
   }

   protected void _parseDetails(ByteBuffer var1) {
      this.preloadStartTime = var1.getInt();
      this.preloadDuration = var1.getInt();
      this.preloadFlags = var1.getInt();
      this.defaultHints = var1.getInt();
   }

   protected void getContent(ByteBuffer var1) {
      var1.putInt(this.preloadStartTime);
      var1.putInt(this.preloadDuration);
      var1.putInt(this.preloadFlags);
      var1.putInt(this.defaultHints);
   }

   protected long getContentSize() {
      return 16L;
   }

   public int getDefaultHints() {
      a var1 = b.a(ajc$tjp_6, this, this);
      e.a().a(var1);
      return this.defaultHints;
   }

   public int getPreloadDuration() {
      a var1 = b.a(ajc$tjp_2, this, this);
      e.a().a(var1);
      return this.preloadDuration;
   }

   public int getPreloadFlags() {
      a var1 = b.a(ajc$tjp_4, this, this);
      e.a().a(var1);
      return this.preloadFlags;
   }

   public int getPreloadStartTime() {
      a var1 = b.a(ajc$tjp_0, this, this);
      e.a().a(var1);
      return this.preloadStartTime;
   }

   public void setDefaultHints(int var1) {
      a var2 = b.a(ajc$tjp_7, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      e.a().a(var2);
      this.defaultHints = var1;
   }

   public void setPreloadDuration(int var1) {
      a var2 = b.a(ajc$tjp_3, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      e.a().a(var2);
      this.preloadDuration = var1;
   }

   public void setPreloadFlags(int var1) {
      a var2 = b.a(ajc$tjp_5, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      e.a().a(var2);
      this.preloadFlags = var1;
   }

   public void setPreloadStartTime(int var1) {
      a var2 = b.a(ajc$tjp_1, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      e.a().a(var2);
      this.preloadStartTime = var1;
   }
}
