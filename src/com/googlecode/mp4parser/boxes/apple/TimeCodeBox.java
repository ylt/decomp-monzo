package com.googlecode.mp4parser.boxes.apple;

import com.coremedia.iso.e;
import com.coremedia.iso.g;
import com.coremedia.iso.boxes.b;
import com.coremedia.iso.boxes.sampleentry.SampleEntry;
import com.googlecode.mp4parser.AbstractBox;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;
import java.util.Collections;
import java.util.List;
import org.mp4parser.aspectj.lang.a;

public class TimeCodeBox extends AbstractBox implements b, SampleEntry {
   public static final String TYPE = "tmcd";
   private static final a.a ajc$tjp_0;
   private static final a.a ajc$tjp_1;
   private static final a.a ajc$tjp_10;
   private static final a.a ajc$tjp_11;
   private static final a.a ajc$tjp_12;
   private static final a.a ajc$tjp_13;
   private static final a.a ajc$tjp_14;
   private static final a.a ajc$tjp_15;
   private static final a.a ajc$tjp_16;
   private static final a.a ajc$tjp_17;
   private static final a.a ajc$tjp_18;
   private static final a.a ajc$tjp_19;
   private static final a.a ajc$tjp_2;
   private static final a.a ajc$tjp_20;
   private static final a.a ajc$tjp_21;
   private static final a.a ajc$tjp_22;
   private static final a.a ajc$tjp_3;
   private static final a.a ajc$tjp_4;
   private static final a.a ajc$tjp_5;
   private static final a.a ajc$tjp_6;
   private static final a.a ajc$tjp_7;
   private static final a.a ajc$tjp_8;
   private static final a.a ajc$tjp_9;
   int dataReferenceIndex;
   long flags;
   int frameDuration;
   int numberOfFrames;
   int reserved1;
   int reserved2;
   byte[] rest = new byte[0];
   int timeScale;

   static {
      ajc$preClinit();
   }

   public TimeCodeBox() {
      super("tmcd");
   }

   private static void ajc$preClinit() {
      org.mp4parser.aspectj.a.b.b var0 = new org.mp4parser.aspectj.a.b.b("TimeCodeBox.java", TimeCodeBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getDataReferenceIndex", "com.googlecode.mp4parser.boxes.apple.TimeCodeBox", "", "", "", "int"), 88);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "setDataReferenceIndex", "com.googlecode.mp4parser.boxes.apple.TimeCodeBox", "int", "dataReferenceIndex", "", "void"), 92);
      ajc$tjp_10 = var0.a("method-execution", var0.a("1", "setReserved1", "com.googlecode.mp4parser.boxes.apple.TimeCodeBox", "int", "reserved1", "", "void"), 137);
      ajc$tjp_11 = var0.a("method-execution", var0.a("1", "getReserved2", "com.googlecode.mp4parser.boxes.apple.TimeCodeBox", "", "", "", "int"), 141);
      ajc$tjp_12 = var0.a("method-execution", var0.a("1", "setReserved2", "com.googlecode.mp4parser.boxes.apple.TimeCodeBox", "int", "reserved2", "", "void"), 145);
      ajc$tjp_13 = var0.a("method-execution", var0.a("1", "getFlags", "com.googlecode.mp4parser.boxes.apple.TimeCodeBox", "", "", "", "long"), 149);
      ajc$tjp_14 = var0.a("method-execution", var0.a("1", "setFlags", "com.googlecode.mp4parser.boxes.apple.TimeCodeBox", "long", "flags", "", "void"), 153);
      ajc$tjp_15 = var0.a("method-execution", var0.a("1", "getRest", "com.googlecode.mp4parser.boxes.apple.TimeCodeBox", "", "", "", "[B"), 157);
      ajc$tjp_16 = var0.a("method-execution", var0.a("1", "setRest", "com.googlecode.mp4parser.boxes.apple.TimeCodeBox", "[B", "rest", "", "void"), 161);
      ajc$tjp_17 = var0.a("method-execution", var0.a("1", "getBoxes", "com.googlecode.mp4parser.boxes.apple.TimeCodeBox", "", "", "", "java.util.List"), 166);
      ajc$tjp_18 = var0.a("method-execution", var0.a("1", "setBoxes", "com.googlecode.mp4parser.boxes.apple.TimeCodeBox", "java.util.List", "boxes", "", "void"), 170);
      ajc$tjp_19 = var0.a("method-execution", var0.a("1", "getBoxes", "com.googlecode.mp4parser.boxes.apple.TimeCodeBox", "java.lang.Class", "clazz", "", "java.util.List"), 174);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "toString", "com.googlecode.mp4parser.boxes.apple.TimeCodeBox", "", "", "", "java.lang.String"), 98);
      ajc$tjp_20 = var0.a("method-execution", var0.a("1", "getBoxes", "com.googlecode.mp4parser.boxes.apple.TimeCodeBox", "java.lang.Class:boolean", "clazz:recursive", "", "java.util.List"), 178);
      ajc$tjp_21 = var0.a("method-execution", var0.a("1", "getByteBuffer", "com.googlecode.mp4parser.boxes.apple.TimeCodeBox", "long:long", "start:size", "java.io.IOException", "java.nio.ByteBuffer"), 182);
      ajc$tjp_22 = var0.a("method-execution", var0.a("1", "writeContainer", "com.googlecode.mp4parser.boxes.apple.TimeCodeBox", "java.nio.channels.WritableByteChannel", "bb", "java.io.IOException", "void"), 186);
      ajc$tjp_3 = var0.a("method-execution", var0.a("1", "getTimeScale", "com.googlecode.mp4parser.boxes.apple.TimeCodeBox", "", "", "", "int"), 109);
      ajc$tjp_4 = var0.a("method-execution", var0.a("1", "setTimeScale", "com.googlecode.mp4parser.boxes.apple.TimeCodeBox", "int", "timeScale", "", "void"), 113);
      ajc$tjp_5 = var0.a("method-execution", var0.a("1", "getFrameDuration", "com.googlecode.mp4parser.boxes.apple.TimeCodeBox", "", "", "", "int"), 117);
      ajc$tjp_6 = var0.a("method-execution", var0.a("1", "setFrameDuration", "com.googlecode.mp4parser.boxes.apple.TimeCodeBox", "int", "frameDuration", "", "void"), 121);
      ajc$tjp_7 = var0.a("method-execution", var0.a("1", "getNumberOfFrames", "com.googlecode.mp4parser.boxes.apple.TimeCodeBox", "", "", "", "int"), 125);
      ajc$tjp_8 = var0.a("method-execution", var0.a("1", "setNumberOfFrames", "com.googlecode.mp4parser.boxes.apple.TimeCodeBox", "int", "numberOfFrames", "", "void"), 129);
      ajc$tjp_9 = var0.a("method-execution", var0.a("1", "getReserved1", "com.googlecode.mp4parser.boxes.apple.TimeCodeBox", "", "", "", "int"), 133);
   }

   protected void _parseDetails(ByteBuffer var1) {
      var1.position(6);
      this.dataReferenceIndex = e.c(var1);
      this.reserved1 = var1.getInt();
      this.flags = e.a(var1);
      this.timeScale = var1.getInt();
      this.frameDuration = var1.getInt();
      this.numberOfFrames = e.d(var1);
      this.reserved2 = e.b(var1);
      this.rest = new byte[var1.remaining()];
      var1.get(this.rest);
   }

   public List getBoxes() {
      a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_17, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return Collections.emptyList();
   }

   public List getBoxes(Class var1) {
      a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_19, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      return Collections.emptyList();
   }

   public List getBoxes(Class var1, boolean var2) {
      a var3 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_20, this, this, var1, org.mp4parser.aspectj.a.a.a.a(var2));
      com.googlecode.mp4parser.e.a().a(var3);
      return Collections.emptyList();
   }

   public ByteBuffer getByteBuffer(long var1, long var3) throws IOException {
      a var5 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_21, this, this, org.mp4parser.aspectj.a.a.a.a(var1), org.mp4parser.aspectj.a.a.a.a(var3));
      com.googlecode.mp4parser.e.a().a(var5);
      return null;
   }

   protected void getContent(ByteBuffer var1) {
      var1.put(new byte[6]);
      g.b(var1, this.dataReferenceIndex);
      var1.putInt(this.reserved1);
      g.b(var1, this.flags);
      var1.putInt(this.timeScale);
      var1.putInt(this.frameDuration);
      g.c(var1, this.numberOfFrames);
      g.a(var1, this.reserved2);
      var1.put(this.rest);
   }

   protected long getContentSize() {
      return (long)(this.rest.length + 28);
   }

   public int getDataReferenceIndex() {
      a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.dataReferenceIndex;
   }

   public long getFlags() {
      a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_13, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.flags;
   }

   public int getFrameDuration() {
      a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_5, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.frameDuration;
   }

   public int getNumberOfFrames() {
      a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_7, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.numberOfFrames;
   }

   public int getReserved1() {
      a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_9, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.reserved1;
   }

   public int getReserved2() {
      a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_11, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.reserved2;
   }

   public byte[] getRest() {
      a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_15, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.rest;
   }

   public int getTimeScale() {
      a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_3, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.timeScale;
   }

   public void setBoxes(List var1) {
      a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_18, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      throw new RuntimeException("Time Code Box doesn't accept any children");
   }

   public void setDataReferenceIndex(int var1) {
      a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_1, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.dataReferenceIndex = var1;
   }

   public void setFlags(long var1) {
      a var3 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_14, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var3);
      this.flags = var1;
   }

   public void setFrameDuration(int var1) {
      a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_6, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.frameDuration = var1;
   }

   public void setNumberOfFrames(int var1) {
      a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_8, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.numberOfFrames = var1;
   }

   public void setReserved1(int var1) {
      a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_10, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.reserved1 = var1;
   }

   public void setReserved2(int var1) {
      a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_12, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.reserved2 = var1;
   }

   public void setRest(byte[] var1) {
      a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_16, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.rest = var1;
   }

   public void setTimeScale(int var1) {
      a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_4, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.timeScale = var1;
   }

   public String toString() {
      a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_2, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return "TimeCodeBox{timeScale=" + this.timeScale + ", frameDuration=" + this.frameDuration + ", numberOfFrames=" + this.numberOfFrames + ", reserved1=" + this.reserved1 + ", reserved2=" + this.reserved2 + ", flags=" + this.flags + '}';
   }

   public void writeContainer(WritableByteChannel var1) throws IOException {
      a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_22, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
   }
}
