package com.googlecode.mp4parser.boxes.apple;

import com.googlecode.mp4parser.AbstractBox;
import com.googlecode.mp4parser.e;
import java.nio.ByteBuffer;
import org.mp4parser.aspectj.a.b.b;
import org.mp4parser.aspectj.lang.a;

public class GenericMediaHeaderTextAtom extends AbstractBox {
   public static final String TYPE = "text";
   private static final a.a ajc$tjp_0;
   private static final a.a ajc$tjp_1;
   private static final a.a ajc$tjp_10;
   private static final a.a ajc$tjp_11;
   private static final a.a ajc$tjp_12;
   private static final a.a ajc$tjp_13;
   private static final a.a ajc$tjp_14;
   private static final a.a ajc$tjp_15;
   private static final a.a ajc$tjp_16;
   private static final a.a ajc$tjp_17;
   private static final a.a ajc$tjp_2;
   private static final a.a ajc$tjp_3;
   private static final a.a ajc$tjp_4;
   private static final a.a ajc$tjp_5;
   private static final a.a ajc$tjp_6;
   private static final a.a ajc$tjp_7;
   private static final a.a ajc$tjp_8;
   private static final a.a ajc$tjp_9;
   int unknown_1 = 65536;
   int unknown_2;
   int unknown_3;
   int unknown_4;
   int unknown_5 = 65536;
   int unknown_6;
   int unknown_7;
   int unknown_8;
   int unknown_9 = 1073741824;

   static {
      ajc$preClinit();
   }

   public GenericMediaHeaderTextAtom() {
      super("text");
   }

   private static void ajc$preClinit() {
      b var0 = new b("GenericMediaHeaderTextAtom.java", GenericMediaHeaderTextAtom.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getUnknown_1", "com.googlecode.mp4parser.boxes.apple.GenericMediaHeaderTextAtom", "", "", "", "int"), 60);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "setUnknown_1", "com.googlecode.mp4parser.boxes.apple.GenericMediaHeaderTextAtom", "int", "unknown_1", "", "void"), 64);
      ajc$tjp_10 = var0.a("method-execution", var0.a("1", "getUnknown_6", "com.googlecode.mp4parser.boxes.apple.GenericMediaHeaderTextAtom", "", "", "", "int"), 100);
      ajc$tjp_11 = var0.a("method-execution", var0.a("1", "setUnknown_6", "com.googlecode.mp4parser.boxes.apple.GenericMediaHeaderTextAtom", "int", "unknown_6", "", "void"), 104);
      ajc$tjp_12 = var0.a("method-execution", var0.a("1", "getUnknown_7", "com.googlecode.mp4parser.boxes.apple.GenericMediaHeaderTextAtom", "", "", "", "int"), 108);
      ajc$tjp_13 = var0.a("method-execution", var0.a("1", "setUnknown_7", "com.googlecode.mp4parser.boxes.apple.GenericMediaHeaderTextAtom", "int", "unknown_7", "", "void"), 112);
      ajc$tjp_14 = var0.a("method-execution", var0.a("1", "getUnknown_8", "com.googlecode.mp4parser.boxes.apple.GenericMediaHeaderTextAtom", "", "", "", "int"), 116);
      ajc$tjp_15 = var0.a("method-execution", var0.a("1", "setUnknown_8", "com.googlecode.mp4parser.boxes.apple.GenericMediaHeaderTextAtom", "int", "unknown_8", "", "void"), 120);
      ajc$tjp_16 = var0.a("method-execution", var0.a("1", "getUnknown_9", "com.googlecode.mp4parser.boxes.apple.GenericMediaHeaderTextAtom", "", "", "", "int"), 124);
      ajc$tjp_17 = var0.a("method-execution", var0.a("1", "setUnknown_9", "com.googlecode.mp4parser.boxes.apple.GenericMediaHeaderTextAtom", "int", "unknown_9", "", "void"), 128);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "getUnknown_2", "com.googlecode.mp4parser.boxes.apple.GenericMediaHeaderTextAtom", "", "", "", "int"), 68);
      ajc$tjp_3 = var0.a("method-execution", var0.a("1", "setUnknown_2", "com.googlecode.mp4parser.boxes.apple.GenericMediaHeaderTextAtom", "int", "unknown_2", "", "void"), 72);
      ajc$tjp_4 = var0.a("method-execution", var0.a("1", "getUnknown_3", "com.googlecode.mp4parser.boxes.apple.GenericMediaHeaderTextAtom", "", "", "", "int"), 76);
      ajc$tjp_5 = var0.a("method-execution", var0.a("1", "setUnknown_3", "com.googlecode.mp4parser.boxes.apple.GenericMediaHeaderTextAtom", "int", "unknown_3", "", "void"), 80);
      ajc$tjp_6 = var0.a("method-execution", var0.a("1", "getUnknown_4", "com.googlecode.mp4parser.boxes.apple.GenericMediaHeaderTextAtom", "", "", "", "int"), 84);
      ajc$tjp_7 = var0.a("method-execution", var0.a("1", "setUnknown_4", "com.googlecode.mp4parser.boxes.apple.GenericMediaHeaderTextAtom", "int", "unknown_4", "", "void"), 88);
      ajc$tjp_8 = var0.a("method-execution", var0.a("1", "getUnknown_5", "com.googlecode.mp4parser.boxes.apple.GenericMediaHeaderTextAtom", "", "", "", "int"), 92);
      ajc$tjp_9 = var0.a("method-execution", var0.a("1", "setUnknown_5", "com.googlecode.mp4parser.boxes.apple.GenericMediaHeaderTextAtom", "int", "unknown_5", "", "void"), 96);
   }

   protected void _parseDetails(ByteBuffer var1) {
      this.unknown_1 = var1.getInt();
      this.unknown_2 = var1.getInt();
      this.unknown_3 = var1.getInt();
      this.unknown_4 = var1.getInt();
      this.unknown_5 = var1.getInt();
      this.unknown_6 = var1.getInt();
      this.unknown_7 = var1.getInt();
      this.unknown_8 = var1.getInt();
      this.unknown_9 = var1.getInt();
   }

   protected void getContent(ByteBuffer var1) {
      var1.putInt(this.unknown_1);
      var1.putInt(this.unknown_2);
      var1.putInt(this.unknown_3);
      var1.putInt(this.unknown_4);
      var1.putInt(this.unknown_5);
      var1.putInt(this.unknown_6);
      var1.putInt(this.unknown_7);
      var1.putInt(this.unknown_8);
      var1.putInt(this.unknown_9);
   }

   protected long getContentSize() {
      return 36L;
   }

   public int getUnknown_1() {
      a var1 = b.a(ajc$tjp_0, this, this);
      e.a().a(var1);
      return this.unknown_1;
   }

   public int getUnknown_2() {
      a var1 = b.a(ajc$tjp_2, this, this);
      e.a().a(var1);
      return this.unknown_2;
   }

   public int getUnknown_3() {
      a var1 = b.a(ajc$tjp_4, this, this);
      e.a().a(var1);
      return this.unknown_3;
   }

   public int getUnknown_4() {
      a var1 = b.a(ajc$tjp_6, this, this);
      e.a().a(var1);
      return this.unknown_4;
   }

   public int getUnknown_5() {
      a var1 = b.a(ajc$tjp_8, this, this);
      e.a().a(var1);
      return this.unknown_5;
   }

   public int getUnknown_6() {
      a var1 = b.a(ajc$tjp_10, this, this);
      e.a().a(var1);
      return this.unknown_6;
   }

   public int getUnknown_7() {
      a var1 = b.a(ajc$tjp_12, this, this);
      e.a().a(var1);
      return this.unknown_7;
   }

   public int getUnknown_8() {
      a var1 = b.a(ajc$tjp_14, this, this);
      e.a().a(var1);
      return this.unknown_8;
   }

   public int getUnknown_9() {
      a var1 = b.a(ajc$tjp_16, this, this);
      e.a().a(var1);
      return this.unknown_9;
   }

   public void setUnknown_1(int var1) {
      a var2 = b.a(ajc$tjp_1, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      e.a().a(var2);
      this.unknown_1 = var1;
   }

   public void setUnknown_2(int var1) {
      a var2 = b.a(ajc$tjp_3, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      e.a().a(var2);
      this.unknown_2 = var1;
   }

   public void setUnknown_3(int var1) {
      a var2 = b.a(ajc$tjp_5, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      e.a().a(var2);
      this.unknown_3 = var1;
   }

   public void setUnknown_4(int var1) {
      a var2 = b.a(ajc$tjp_7, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      e.a().a(var2);
      this.unknown_4 = var1;
   }

   public void setUnknown_5(int var1) {
      a var2 = b.a(ajc$tjp_9, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      e.a().a(var2);
      this.unknown_5 = var1;
   }

   public void setUnknown_6(int var1) {
      a var2 = b.a(ajc$tjp_11, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      e.a().a(var2);
      this.unknown_6 = var1;
   }

   public void setUnknown_7(int var1) {
      a var2 = b.a(ajc$tjp_13, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      e.a().a(var2);
      this.unknown_7 = var1;
   }

   public void setUnknown_8(int var1) {
      a var2 = b.a(ajc$tjp_15, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      e.a().a(var2);
      this.unknown_8 = var1;
   }

   public void setUnknown_9(int var1) {
      a var2 = b.a(ajc$tjp_17, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      e.a().a(var2);
      this.unknown_9 = var1;
   }
}
