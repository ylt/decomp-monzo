package com.googlecode.mp4parser.boxes.apple;

import com.coremedia.iso.j;
import com.googlecode.mp4parser.e;
import java.nio.ByteBuffer;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import org.mp4parser.aspectj.a.b.b;
import org.mp4parser.aspectj.lang.a;

public class AppleRecordingYearBox extends AppleDataBox {
   private static final a.a ajc$tjp_0;
   private static final a.a ajc$tjp_1;
   Date date = new Date();
   DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'kk:mm:ssZ");

   static {
      ajc$preClinit();
   }

   public AppleRecordingYearBox() {
      super("©day", 1);
      this.df.setTimeZone(TimeZone.getTimeZone("UTC"));
   }

   private static void ajc$preClinit() {
      b var0 = new b("AppleRecordingYearBox.java", AppleRecordingYearBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getDate", "com.googlecode.mp4parser.boxes.apple.AppleRecordingYearBox", "", "", "", "java.util.Date"), 27);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "setDate", "com.googlecode.mp4parser.boxes.apple.AppleRecordingYearBox", "java.util.Date", "date", "", "void"), 31);
   }

   protected static String iso8601toRfc822Date(String var0) {
      return var0.replaceAll("Z$", "+0000").replaceAll("([0-9][0-9]):([0-9][0-9])$", "$1$2");
   }

   protected static String rfc822toIso8601Date(String var0) {
      return var0.replaceAll("\\+0000$", "Z");
   }

   protected int getDataLength() {
      return j.a(rfc822toIso8601Date(this.df.format(this.date))).length;
   }

   public Date getDate() {
      a var1 = b.a(ajc$tjp_0, this, this);
      e.a().a(var1);
      return this.date;
   }

   protected void parseData(ByteBuffer var1) {
      String var3 = com.coremedia.iso.e.a(var1, var1.remaining());

      try {
         this.date = this.df.parse(iso8601toRfc822Date(var3));
      } catch (ParseException var2) {
         throw new RuntimeException(var2);
      }
   }

   public void setDate(Date var1) {
      a var2 = b.a(ajc$tjp_1, this, this, (Object)var1);
      e.a().a(var2);
      this.date = var1;
   }

   protected byte[] writeData() {
      return j.a(rfc822toIso8601Date(this.df.format(this.date)));
   }
}
