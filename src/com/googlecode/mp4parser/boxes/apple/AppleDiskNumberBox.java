package com.googlecode.mp4parser.boxes.apple;

import com.googlecode.mp4parser.e;
import java.nio.ByteBuffer;
import org.mp4parser.aspectj.a.b.b;
import org.mp4parser.aspectj.lang.a;

public class AppleDiskNumberBox extends AppleDataBox {
   private static final a.a ajc$tjp_0;
   private static final a.a ajc$tjp_1;
   private static final a.a ajc$tjp_2;
   private static final a.a ajc$tjp_3;
   int a;
   short b;

   static {
      ajc$preClinit();
   }

   public AppleDiskNumberBox() {
      super("disk", 0);
   }

   private static void ajc$preClinit() {
      b var0 = new b("AppleDiskNumberBox.java", AppleDiskNumberBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getA", "com.googlecode.mp4parser.boxes.apple.AppleDiskNumberBox", "", "", "", "int"), 16);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "setA", "com.googlecode.mp4parser.boxes.apple.AppleDiskNumberBox", "int", "a", "", "void"), 20);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "getB", "com.googlecode.mp4parser.boxes.apple.AppleDiskNumberBox", "", "", "", "short"), 24);
      ajc$tjp_3 = var0.a("method-execution", var0.a("1", "setB", "com.googlecode.mp4parser.boxes.apple.AppleDiskNumberBox", "short", "b", "", "void"), 28);
   }

   public int getA() {
      a var1 = b.a(ajc$tjp_0, this, this);
      e.a().a(var1);
      return this.a;
   }

   public short getB() {
      a var1 = b.a(ajc$tjp_2, this, this);
      e.a().a(var1);
      return this.b;
   }

   protected int getDataLength() {
      return 6;
   }

   protected void parseData(ByteBuffer var1) {
      this.a = var1.getInt();
      this.b = var1.getShort();
   }

   public void setA(int var1) {
      a var2 = b.a(ajc$tjp_1, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      e.a().a(var2);
      this.a = var1;
   }

   public void setB(short var1) {
      a var2 = b.a(ajc$tjp_3, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      e.a().a(var2);
      this.b = var1;
   }

   protected byte[] writeData() {
      ByteBuffer var1 = ByteBuffer.allocate(6);
      var1.putInt(this.a);
      var1.putShort(this.b);
      return var1.array();
   }
}
