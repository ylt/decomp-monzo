package com.googlecode.mp4parser.boxes.apple;

import com.coremedia.iso.g;
import com.googlecode.mp4parser.AbstractBox;
import com.googlecode.mp4parser.e;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Locale;
import org.mp4parser.aspectj.a.b.b;
import org.mp4parser.aspectj.lang.a;

public abstract class AppleDataBox extends AbstractBox {
   private static final a.a ajc$tjp_0;
   private static final a.a ajc$tjp_1;
   private static final a.a ajc$tjp_2;
   private static final a.a ajc$tjp_3;
   private static final a.a ajc$tjp_4;
   private static final a.a ajc$tjp_5;
   private static HashMap language;
   int dataCountry;
   int dataLanguage;
   int dataType;

   static {
      ajc$preClinit();
      language = new HashMap();
      language.put("0", "English");
      language.put("1", "French");
      language.put("2", "German");
      language.put("3", "Italian");
      language.put("4", "Dutch");
      language.put("5", "Swedish");
      language.put("6", "Spanish");
      language.put("7", "Danish");
      language.put("8", "Portuguese");
      language.put("9", "Norwegian");
      language.put("10", "Hebrew");
      language.put("11", "Japanese");
      language.put("12", "Arabic");
      language.put("13", "Finnish");
      language.put("14", "Greek");
      language.put("15", "Icelandic");
      language.put("16", "Maltese");
      language.put("17", "Turkish");
      language.put("18", "Croatian");
      language.put("19", "Traditional_Chinese");
      language.put("20", "Urdu");
      language.put("21", "Hindi");
      language.put("22", "Thai");
      language.put("23", "Korean");
      language.put("24", "Lithuanian");
      language.put("25", "Polish");
      language.put("26", "Hungarian");
      language.put("27", "Estonian");
      language.put("28", "Lettish");
      language.put("29", "Sami");
      language.put("30", "Faroese");
      language.put("31", "Farsi");
      language.put("32", "Russian");
      language.put("33", "Simplified_Chinese");
      language.put("34", "Flemish");
      language.put("35", "Irish");
      language.put("36", "Albanian");
      language.put("37", "Romanian");
      language.put("38", "Czech");
      language.put("39", "Slovak");
      language.put("40", "Slovenian");
      language.put("41", "Yiddish");
      language.put("42", "Serbian");
      language.put("43", "Macedonian");
      language.put("44", "Bulgarian");
      language.put("45", "Ukrainian");
      language.put("46", "Belarusian");
      language.put("47", "Uzbek");
      language.put("48", "Kazakh");
      language.put("49", "Azerbaijani");
      language.put("50", "AzerbaijanAr");
      language.put("51", "Armenian");
      language.put("52", "Georgian");
      language.put("53", "Moldavian");
      language.put("54", "Kirghiz");
      language.put("55", "Tajiki");
      language.put("56", "Turkmen");
      language.put("57", "Mongolian");
      language.put("58", "MongolianCyr");
      language.put("59", "Pashto");
      language.put("60", "Kurdish");
      language.put("61", "Kashmiri");
      language.put("62", "Sindhi");
      language.put("63", "Tibetan");
      language.put("64", "Nepali");
      language.put("65", "Sanskrit");
      language.put("66", "Marathi");
      language.put("67", "Bengali");
      language.put("68", "Assamese");
      language.put("69", "Gujarati");
      language.put("70", "Punjabi");
      language.put("71", "Oriya");
      language.put("72", "Malayalam");
      language.put("73", "Kannada");
      language.put("74", "Tamil");
      language.put("75", "Telugu");
      language.put("76", "Sinhala");
      language.put("77", "Burmese");
      language.put("78", "Khmer");
      language.put("79", "Lao");
      language.put("80", "Vietnamese");
      language.put("81", "Indonesian");
      language.put("82", "Tagalog");
      language.put("83", "MalayRoman");
      language.put("84", "MalayArabic");
      language.put("85", "Amharic");
      language.put("87", "Galla");
      language.put("87", "Oromo");
      language.put("88", "Somali");
      language.put("89", "Swahili");
      language.put("90", "Kinyarwanda");
      language.put("91", "Rundi");
      language.put("92", "Nyanja");
      language.put("93", "Malagasy");
      language.put("94", "Esperanto");
      language.put("128", "Welsh");
      language.put("129", "Basque");
      language.put("130", "Catalan");
      language.put("131", "Latin");
      language.put("132", "Quechua");
      language.put("133", "Guarani");
      language.put("134", "Aymara");
      language.put("135", "Tatar");
      language.put("136", "Uighur");
      language.put("137", "Dzongkha");
      language.put("138", "JavaneseRom");
      language.put("32767", "Unspecified");
   }

   protected AppleDataBox(String var1, int var2) {
      super(var1);
      this.dataType = var2;
   }

   private static void ajc$preClinit() {
      b var0 = new b("AppleDataBox.java", AppleDataBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getLanguageString", "com.googlecode.mp4parser.boxes.apple.AppleDataBox", "", "", "", "java.lang.String"), 25);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "getDataType", "com.googlecode.mp4parser.boxes.apple.AppleDataBox", "", "", "", "int"), 43);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "getDataCountry", "com.googlecode.mp4parser.boxes.apple.AppleDataBox", "", "", "", "int"), 47);
      ajc$tjp_3 = var0.a("method-execution", var0.a("1", "setDataCountry", "com.googlecode.mp4parser.boxes.apple.AppleDataBox", "int", "dataCountry", "", "void"), 51);
      ajc$tjp_4 = var0.a("method-execution", var0.a("1", "getDataLanguage", "com.googlecode.mp4parser.boxes.apple.AppleDataBox", "", "", "", "int"), 55);
      ajc$tjp_5 = var0.a("method-execution", var0.a("1", "setDataLanguage", "com.googlecode.mp4parser.boxes.apple.AppleDataBox", "int", "dataLanguage", "", "void"), 59);
   }

   protected void _parseDetails(ByteBuffer var1) {
      this.parseData(this.parseDataLength4ccTypeCountryLanguageAndReturnRest(var1));
   }

   protected void getContent(ByteBuffer var1) {
      this.writeDataLength4ccTypeCountryLanguage(var1);
      var1.put(this.writeData());
   }

   protected long getContentSize() {
      return (long)(this.getDataLength() + 16);
   }

   public int getDataCountry() {
      a var1 = b.a(ajc$tjp_2, this, this);
      e.a().a(var1);
      return this.dataCountry;
   }

   public int getDataLanguage() {
      a var1 = b.a(ajc$tjp_4, this, this);
      e.a().a(var1);
      return this.dataLanguage;
   }

   protected abstract int getDataLength();

   public int getDataType() {
      a var1 = b.a(ajc$tjp_1, this, this);
      e.a().a(var1);
      return this.dataType;
   }

   public String getLanguageString() {
      a var1 = b.a(ajc$tjp_0, this, this);
      e.a().a(var1);
      String var2 = (String)language.get("" + this.dataLanguage);
      String var3 = var2;
      if(var2 == null) {
         ByteBuffer var4 = ByteBuffer.wrap(new byte[2]);
         g.b(var4, this.dataLanguage);
         var4.reset();
         var3 = (new Locale(com.coremedia.iso.e.j(var4))).getDisplayLanguage();
      }

      return var3;
   }

   protected abstract void parseData(ByteBuffer var1);

   protected ByteBuffer parseDataLength4ccTypeCountryLanguageAndReturnRest(ByteBuffer var1) {
      int var2 = var1.getInt();
      var1.getInt();
      this.dataType = var1.getInt();
      this.dataCountry = var1.getShort();
      if(this.dataCountry < 0) {
         this.dataCountry += 65536;
      }

      this.dataLanguage = var1.getShort();
      if(this.dataLanguage < 0) {
         this.dataLanguage += 65536;
      }

      ByteBuffer var3 = (ByteBuffer)var1.duplicate().slice().limit(var2 - 16);
      var1.position(var2 - 16 + var1.position());
      return var3;
   }

   public void setDataCountry(int var1) {
      a var2 = b.a(ajc$tjp_3, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      e.a().a(var2);
      this.dataCountry = var1;
   }

   public void setDataLanguage(int var1) {
      a var2 = b.a(ajc$tjp_5, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      e.a().a(var2);
      this.dataLanguage = var1;
   }

   protected abstract byte[] writeData();

   protected void writeDataLength4ccTypeCountryLanguage(ByteBuffer var1) {
      var1.putInt(this.getDataLength() + 16);
      var1.put("data".getBytes());
      var1.putInt(this.dataType);
      g.b(var1, this.dataCountry);
      g.b(var1, this.dataLanguage);
   }
}
