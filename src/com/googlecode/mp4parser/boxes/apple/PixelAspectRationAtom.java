package com.googlecode.mp4parser.boxes.apple;

import com.googlecode.mp4parser.AbstractBox;
import com.googlecode.mp4parser.e;
import java.nio.ByteBuffer;
import org.mp4parser.aspectj.a.b.b;
import org.mp4parser.aspectj.lang.a;

public class PixelAspectRationAtom extends AbstractBox {
   public static final String TYPE = "pasp";
   private static final a.a ajc$tjp_0;
   private static final a.a ajc$tjp_1;
   private static final a.a ajc$tjp_2;
   private static final a.a ajc$tjp_3;
   private int hSpacing;
   private int vSpacing;

   static {
      ajc$preClinit();
   }

   public PixelAspectRationAtom() {
      super("pasp");
   }

   private static void ajc$preClinit() {
      b var0 = new b("PixelAspectRationAtom.java", PixelAspectRationAtom.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "gethSpacing", "com.googlecode.mp4parser.boxes.apple.PixelAspectRationAtom", "", "", "", "int"), 35);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "sethSpacing", "com.googlecode.mp4parser.boxes.apple.PixelAspectRationAtom", "int", "hSpacing", "", "void"), 39);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "getvSpacing", "com.googlecode.mp4parser.boxes.apple.PixelAspectRationAtom", "", "", "", "int"), 43);
      ajc$tjp_3 = var0.a("method-execution", var0.a("1", "setvSpacing", "com.googlecode.mp4parser.boxes.apple.PixelAspectRationAtom", "int", "vSpacing", "", "void"), 47);
   }

   protected void _parseDetails(ByteBuffer var1) {
      this.hSpacing = var1.getInt();
      this.vSpacing = var1.getInt();
   }

   protected void getContent(ByteBuffer var1) {
      var1.putInt(this.hSpacing);
      var1.putInt(this.vSpacing);
   }

   protected long getContentSize() {
      return 8L;
   }

   public int gethSpacing() {
      a var1 = b.a(ajc$tjp_0, this, this);
      e.a().a(var1);
      return this.hSpacing;
   }

   public int getvSpacing() {
      a var1 = b.a(ajc$tjp_2, this, this);
      e.a().a(var1);
      return this.vSpacing;
   }

   public void sethSpacing(int var1) {
      a var2 = b.a(ajc$tjp_1, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      e.a().a(var2);
      this.hSpacing = var1;
   }

   public void setvSpacing(int var1) {
      a var2 = b.a(ajc$tjp_3, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      e.a().a(var2);
      this.vSpacing = var1;
   }
}
