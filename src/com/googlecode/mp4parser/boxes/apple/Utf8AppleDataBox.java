package com.googlecode.mp4parser.boxes.apple;

import com.coremedia.iso.j;
import com.googlecode.mp4parser.e;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import org.mp4parser.aspectj.a.b.b;
import org.mp4parser.aspectj.lang.a;

public abstract class Utf8AppleDataBox extends AppleDataBox {
   private static final a.a ajc$tjp_0;
   private static final a.a ajc$tjp_1;
   String value;

   static {
      ajc$preClinit();
   }

   protected Utf8AppleDataBox(String var1) {
      super(var1, 1);
   }

   private static void ajc$preClinit() {
      b var0 = new b("Utf8AppleDataBox.java", Utf8AppleDataBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getValue", "com.googlecode.mp4parser.boxes.apple.Utf8AppleDataBox", "", "", "", "java.lang.String"), 21);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "setValue", "com.googlecode.mp4parser.boxes.apple.Utf8AppleDataBox", "java.lang.String", "value", "", "void"), 30);
   }

   protected int getDataLength() {
      return this.value.getBytes(Charset.forName("UTF-8")).length;
   }

   public String getValue() {
      a var1 = b.a(ajc$tjp_0, this, this);
      e.a().a(var1);
      if(!this.isParsed()) {
         this.parseDetails();
      }

      return this.value;
   }

   protected void parseData(ByteBuffer var1) {
      this.value = com.coremedia.iso.e.a(var1, var1.remaining());
   }

   public void setValue(String var1) {
      a var2 = b.a(ajc$tjp_1, this, this, (Object)var1);
      e.a().a(var2);
      this.value = var1;
   }

   public byte[] writeData() {
      return j.a(this.value);
   }
}
