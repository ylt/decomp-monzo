package com.googlecode.mp4parser.boxes.apple;

import com.coremedia.iso.f;
import com.coremedia.iso.h;
import com.googlecode.mp4parser.e;
import java.nio.ByteBuffer;
import org.mp4parser.aspectj.a.b.b;
import org.mp4parser.aspectj.lang.a;

public abstract class AppleVariableSignedIntegerBox extends AppleDataBox {
   private static final a.a ajc$tjp_0;
   private static final a.a ajc$tjp_1;
   private static final a.a ajc$tjp_2;
   private static final a.a ajc$tjp_3;
   int intLength = 1;
   long value;

   static {
      ajc$preClinit();
   }

   protected AppleVariableSignedIntegerBox(String var1) {
      super(var1, 15);
   }

   private static void ajc$preClinit() {
      b var0 = new b("AppleVariableSignedIntegerBox.java", AppleVariableSignedIntegerBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getIntLength", "com.googlecode.mp4parser.boxes.apple.AppleVariableSignedIntegerBox", "", "", "", "int"), 19);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "setIntLength", "com.googlecode.mp4parser.boxes.apple.AppleVariableSignedIntegerBox", "int", "intLength", "", "void"), 23);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "getValue", "com.googlecode.mp4parser.boxes.apple.AppleVariableSignedIntegerBox", "", "", "", "long"), 27);
      ajc$tjp_3 = var0.a("method-execution", var0.a("1", "setValue", "com.googlecode.mp4parser.boxes.apple.AppleVariableSignedIntegerBox", "long", "value", "", "void"), 36);
   }

   protected int getDataLength() {
      return this.intLength;
   }

   public int getIntLength() {
      a var1 = b.a(ajc$tjp_0, this, this);
      e.a().a(var1);
      return this.intLength;
   }

   public long getValue() {
      a var1 = b.a(ajc$tjp_2, this, this);
      e.a().a(var1);
      if(!this.isParsed()) {
         this.parseDetails();
      }

      return this.value;
   }

   protected void parseData(ByteBuffer var1) {
      int var2 = var1.remaining();
      this.value = f.a(var1, var2);
      this.intLength = var2;
   }

   public void setIntLength(int var1) {
      a var2 = b.a(ajc$tjp_1, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      e.a().a(var2);
      this.intLength = var1;
   }

   public void setValue(long var1) {
      a var3 = b.a(ajc$tjp_3, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      e.a().a(var3);
      if(var1 <= 127L && var1 > -128L) {
         this.intLength = 1;
      } else if(var1 <= 32767L && var1 > -32768L && this.intLength < 2) {
         this.intLength = 2;
      } else if(var1 <= 8388607L && var1 > -8388608L && this.intLength < 3) {
         this.intLength = 3;
      } else {
         this.intLength = 4;
      }

      this.value = var1;
   }

   protected byte[] writeData() {
      int var1 = this.getDataLength();
      ByteBuffer var2 = ByteBuffer.wrap(new byte[var1]);
      h.a(this.value, var2, var1);
      return var2.array();
   }
}
