package com.googlecode.mp4parser.boxes.apple;

import com.coremedia.iso.j;
import com.googlecode.mp4parser.AbstractBox;
import com.googlecode.mp4parser.e;
import java.nio.ByteBuffer;
import org.mp4parser.aspectj.a.b.b;
import org.mp4parser.aspectj.lang.a;

public class AppleGPSCoordinatesBox extends AbstractBox {
   private static final int DEFAULT_LANG = 5575;
   public static final String TYPE = "©xyz";
   private static final a.a ajc$tjp_0;
   private static final a.a ajc$tjp_1;
   private static final a.a ajc$tjp_2;
   String coords;
   int lang = 5575;

   static {
      ajc$preClinit();
   }

   public AppleGPSCoordinatesBox() {
      super("©xyz");
   }

   private static void ajc$preClinit() {
      b var0 = new b("AppleGPSCoordinatesBox.java", AppleGPSCoordinatesBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getValue", "com.googlecode.mp4parser.boxes.apple.AppleGPSCoordinatesBox", "", "", "", "java.lang.String"), 22);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "setValue", "com.googlecode.mp4parser.boxes.apple.AppleGPSCoordinatesBox", "java.lang.String", "iso6709String", "", "void"), 26);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "toString", "com.googlecode.mp4parser.boxes.apple.AppleGPSCoordinatesBox", "", "", "", "java.lang.String"), 52);
   }

   protected void _parseDetails(ByteBuffer var1) {
      short var2 = var1.getShort();
      this.lang = var1.getShort();
      byte[] var3 = new byte[var2];
      var1.get(var3);
      this.coords = j.a(var3);
   }

   protected void getContent(ByteBuffer var1) {
      var1.putShort((short)this.coords.length());
      var1.putShort((short)this.lang);
      var1.put(j.a(this.coords));
   }

   protected long getContentSize() {
      return (long)(j.b(this.coords) + 4);
   }

   public String getValue() {
      a var1 = b.a(ajc$tjp_0, this, this);
      e.a().a(var1);
      return this.coords;
   }

   public void setValue(String var1) {
      a var2 = b.a(ajc$tjp_1, this, this, (Object)var1);
      e.a().a(var2);
      this.lang = 5575;
      this.coords = var1;
   }

   public String toString() {
      a var1 = b.a(ajc$tjp_2, this, this);
      e.a().a(var1);
      return "AppleGPSCoordinatesBox[" + this.coords + "]";
   }
}
