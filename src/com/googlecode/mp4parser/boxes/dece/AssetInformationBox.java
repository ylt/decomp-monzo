package com.googlecode.mp4parser.boxes.dece;

import com.coremedia.iso.e;
import com.coremedia.iso.j;
import com.googlecode.mp4parser.AbstractFullBox;
import java.nio.ByteBuffer;
import org.mp4parser.aspectj.a.b.b;
import org.mp4parser.aspectj.lang.a;

public class AssetInformationBox extends AbstractFullBox {
   // $FF: synthetic field
   static final boolean $assertionsDisabled;
   public static final String TYPE = "ainf";
   private static final a.a ajc$tjp_0;
   private static final a.a ajc$tjp_1;
   private static final a.a ajc$tjp_2;
   private static final a.a ajc$tjp_3;
   String apid = "";
   String profileVersion = "0000";

   static {
      ajc$preClinit();
      boolean var0;
      if(!AssetInformationBox.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      $assertionsDisabled = var0;
   }

   public AssetInformationBox() {
      super("ainf");
   }

   private static void ajc$preClinit() {
      b var0 = new b("AssetInformationBox.java", AssetInformationBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getApid", "com.googlecode.mp4parser.boxes.dece.AssetInformationBox", "", "", "", "java.lang.String"), 131);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "setApid", "com.googlecode.mp4parser.boxes.dece.AssetInformationBox", "java.lang.String", "apid", "", "void"), 135);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "getProfileVersion", "com.googlecode.mp4parser.boxes.dece.AssetInformationBox", "", "", "", "java.lang.String"), 139);
      ajc$tjp_3 = var0.a("method-execution", var0.a("1", "setProfileVersion", "com.googlecode.mp4parser.boxes.dece.AssetInformationBox", "java.lang.String", "profileVersion", "", "void"), 143);
   }

   public void _parseDetails(ByteBuffer var1) {
      this.parseVersionAndFlags(var1);
      this.profileVersion = e.a(var1, 4);
      this.apid = e.e(var1);
   }

   public String getApid() {
      a var1 = b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.apid;
   }

   protected void getContent(ByteBuffer var1) {
      this.writeVersionAndFlags(var1);
      if(this.getVersion() == 0) {
         var1.put(j.a(this.profileVersion), 0, 4);
         var1.put(j.a(this.apid));
         var1.put(0);
      } else {
         throw new RuntimeException("Unknown ainf version " + this.getVersion());
      }
   }

   protected long getContentSize() {
      return (long)(j.b(this.apid) + 9);
   }

   public String getProfileVersion() {
      a var1 = b.a(ajc$tjp_2, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.profileVersion;
   }

   public boolean isHidden() {
      boolean var1 = true;
      if((this.getFlags() & 1) != 1) {
         var1 = false;
      }

      return var1;
   }

   public void setApid(String var1) {
      a var2 = b.a(ajc$tjp_1, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.apid = var1;
   }

   public void setHidden(boolean var1) {
      int var2 = this.getFlags();
      if(this.isHidden() ^ var1) {
         if(var1) {
            this.setFlags(var2 | 1);
         } else {
            this.setFlags(var2 & 16777214);
         }
      }

   }

   public void setProfileVersion(String var1) {
      a var2 = b.a(ajc$tjp_3, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      if($assertionsDisabled || var1 != null && var1.length() == 4) {
         this.profileVersion = var1;
      } else {
         throw new AssertionError();
      }
   }
}
