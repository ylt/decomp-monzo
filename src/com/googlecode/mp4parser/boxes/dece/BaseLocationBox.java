package com.googlecode.mp4parser.boxes.dece;

import com.coremedia.iso.e;
import com.coremedia.iso.j;
import com.googlecode.mp4parser.AbstractFullBox;
import java.nio.ByteBuffer;
import org.mp4parser.aspectj.a.b.b;
import org.mp4parser.aspectj.lang.a;

public class BaseLocationBox extends AbstractFullBox {
   public static final String TYPE = "bloc";
   private static final a.a ajc$tjp_0;
   private static final a.a ajc$tjp_1;
   private static final a.a ajc$tjp_2;
   private static final a.a ajc$tjp_3;
   private static final a.a ajc$tjp_4;
   private static final a.a ajc$tjp_5;
   private static final a.a ajc$tjp_6;
   String baseLocation = "";
   String purchaseLocation = "";

   static {
      ajc$preClinit();
   }

   public BaseLocationBox() {
      super("bloc");
   }

   public BaseLocationBox(String var1, String var2) {
      super("bloc");
      this.baseLocation = var1;
      this.purchaseLocation = var2;
   }

   private static void ajc$preClinit() {
      b var0 = new b("BaseLocationBox.java", BaseLocationBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getBaseLocation", "com.googlecode.mp4parser.boxes.dece.BaseLocationBox", "", "", "", "java.lang.String"), 44);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "setBaseLocation", "com.googlecode.mp4parser.boxes.dece.BaseLocationBox", "java.lang.String", "baseLocation", "", "void"), 48);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "getPurchaseLocation", "com.googlecode.mp4parser.boxes.dece.BaseLocationBox", "", "", "", "java.lang.String"), 52);
      ajc$tjp_3 = var0.a("method-execution", var0.a("1", "setPurchaseLocation", "com.googlecode.mp4parser.boxes.dece.BaseLocationBox", "java.lang.String", "purchaseLocation", "", "void"), 56);
      ajc$tjp_4 = var0.a("method-execution", var0.a("1", "equals", "com.googlecode.mp4parser.boxes.dece.BaseLocationBox", "java.lang.Object", "o", "", "boolean"), 86);
      ajc$tjp_5 = var0.a("method-execution", var0.a("1", "hashCode", "com.googlecode.mp4parser.boxes.dece.BaseLocationBox", "", "", "", "int"), 100);
      ajc$tjp_6 = var0.a("method-execution", var0.a("1", "toString", "com.googlecode.mp4parser.boxes.dece.BaseLocationBox", "", "", "", "java.lang.String"), 107);
   }

   public void _parseDetails(ByteBuffer var1) {
      this.parseVersionAndFlags(var1);
      this.baseLocation = e.e(var1);
      var1.get(new byte[256 - j.b(this.baseLocation) - 1]);
      this.purchaseLocation = e.e(var1);
      var1.get(new byte[256 - j.b(this.purchaseLocation) - 1]);
      var1.get(new byte[512]);
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      a var3 = b.a(ajc$tjp_4, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var3);
      if(this != var1) {
         if(var1 != null && this.getClass() == var1.getClass()) {
            BaseLocationBox var4;
            label43: {
               var4 = (BaseLocationBox)var1;
               if(this.baseLocation != null) {
                  if(this.baseLocation.equals(var4.baseLocation)) {
                     break label43;
                  }
               } else if(var4.baseLocation == null) {
                  break label43;
               }

               var2 = false;
               return var2;
            }

            if(this.purchaseLocation != null) {
               if(this.purchaseLocation.equals(var4.purchaseLocation)) {
                  return var2;
               }
            } else if(var4.purchaseLocation == null) {
               return var2;
            }

            var2 = false;
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public String getBaseLocation() {
      a var1 = b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.baseLocation;
   }

   protected void getContent(ByteBuffer var1) {
      this.writeVersionAndFlags(var1);
      var1.put(j.a(this.baseLocation));
      var1.put(new byte[256 - j.b(this.baseLocation)]);
      var1.put(j.a(this.purchaseLocation));
      var1.put(new byte[256 - j.b(this.purchaseLocation)]);
      var1.put(new byte[512]);
   }

   protected long getContentSize() {
      return 1028L;
   }

   public String getPurchaseLocation() {
      a var1 = b.a(ajc$tjp_2, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.purchaseLocation;
   }

   public int hashCode() {
      int var2 = 0;
      a var3 = b.a(ajc$tjp_5, this, this);
      com.googlecode.mp4parser.e.a().a(var3);
      int var1;
      if(this.baseLocation != null) {
         var1 = this.baseLocation.hashCode();
      } else {
         var1 = 0;
      }

      if(this.purchaseLocation != null) {
         var2 = this.purchaseLocation.hashCode();
      }

      return var1 * 31 + var2;
   }

   public void setBaseLocation(String var1) {
      a var2 = b.a(ajc$tjp_1, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.baseLocation = var1;
   }

   public void setPurchaseLocation(String var1) {
      a var2 = b.a(ajc$tjp_3, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.purchaseLocation = var1;
   }

   public String toString() {
      a var1 = b.a(ajc$tjp_6, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return "BaseLocationBox{baseLocation='" + this.baseLocation + '\'' + ", purchaseLocation='" + this.purchaseLocation + '\'' + '}';
   }
}
