package com.googlecode.mp4parser.boxes.dece;

import com.coremedia.iso.e;
import com.coremedia.iso.g;
import com.coremedia.iso.j;
import com.googlecode.mp4parser.AbstractFullBox;
import java.nio.ByteBuffer;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import org.mp4parser.aspectj.a.b.b;
import org.mp4parser.aspectj.lang.a;

public class ContentInformationBox extends AbstractFullBox {
   public static final String TYPE = "cinf";
   private static final a.a ajc$tjp_0;
   private static final a.a ajc$tjp_1;
   private static final a.a ajc$tjp_10;
   private static final a.a ajc$tjp_11;
   private static final a.a ajc$tjp_12;
   private static final a.a ajc$tjp_13;
   private static final a.a ajc$tjp_2;
   private static final a.a ajc$tjp_3;
   private static final a.a ajc$tjp_4;
   private static final a.a ajc$tjp_5;
   private static final a.a ajc$tjp_6;
   private static final a.a ajc$tjp_7;
   private static final a.a ajc$tjp_8;
   private static final a.a ajc$tjp_9;
   Map brandEntries = new LinkedHashMap();
   String codecs;
   Map idEntries = new LinkedHashMap();
   String languages;
   String mimeSubtypeName;
   String profileLevelIdc;
   String protection;

   static {
      ajc$preClinit();
   }

   public ContentInformationBox() {
      super("cinf");
   }

   private static void ajc$preClinit() {
      b var0 = new b("ContentInformationBox.java", ContentInformationBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getMimeSubtypeName", "com.googlecode.mp4parser.boxes.dece.ContentInformationBox", "", "", "", "java.lang.String"), 144);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "setMimeSubtypeName", "com.googlecode.mp4parser.boxes.dece.ContentInformationBox", "java.lang.String", "mimeSubtypeName", "", "void"), 148);
      ajc$tjp_10 = var0.a("method-execution", var0.a("1", "getBrandEntries", "com.googlecode.mp4parser.boxes.dece.ContentInformationBox", "", "", "", "java.util.Map"), 184);
      ajc$tjp_11 = var0.a("method-execution", var0.a("1", "setBrandEntries", "com.googlecode.mp4parser.boxes.dece.ContentInformationBox", "java.util.Map", "brandEntries", "", "void"), 188);
      ajc$tjp_12 = var0.a("method-execution", var0.a("1", "getIdEntries", "com.googlecode.mp4parser.boxes.dece.ContentInformationBox", "", "", "", "java.util.Map"), 192);
      ajc$tjp_13 = var0.a("method-execution", var0.a("1", "setIdEntries", "com.googlecode.mp4parser.boxes.dece.ContentInformationBox", "java.util.Map", "idEntries", "", "void"), 196);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "getProfileLevelIdc", "com.googlecode.mp4parser.boxes.dece.ContentInformationBox", "", "", "", "java.lang.String"), 152);
      ajc$tjp_3 = var0.a("method-execution", var0.a("1", "setProfileLevelIdc", "com.googlecode.mp4parser.boxes.dece.ContentInformationBox", "java.lang.String", "profileLevelIdc", "", "void"), 156);
      ajc$tjp_4 = var0.a("method-execution", var0.a("1", "getCodecs", "com.googlecode.mp4parser.boxes.dece.ContentInformationBox", "", "", "", "java.lang.String"), 160);
      ajc$tjp_5 = var0.a("method-execution", var0.a("1", "setCodecs", "com.googlecode.mp4parser.boxes.dece.ContentInformationBox", "java.lang.String", "codecs", "", "void"), 164);
      ajc$tjp_6 = var0.a("method-execution", var0.a("1", "getProtection", "com.googlecode.mp4parser.boxes.dece.ContentInformationBox", "", "", "", "java.lang.String"), 168);
      ajc$tjp_7 = var0.a("method-execution", var0.a("1", "setProtection", "com.googlecode.mp4parser.boxes.dece.ContentInformationBox", "java.lang.String", "protection", "", "void"), 172);
      ajc$tjp_8 = var0.a("method-execution", var0.a("1", "getLanguages", "com.googlecode.mp4parser.boxes.dece.ContentInformationBox", "", "", "", "java.lang.String"), 176);
      ajc$tjp_9 = var0.a("method-execution", var0.a("1", "setLanguages", "com.googlecode.mp4parser.boxes.dece.ContentInformationBox", "java.lang.String", "languages", "", "void"), 180);
   }

   protected void _parseDetails(ByteBuffer var1) {
      this.parseVersionAndFlags(var1);
      this.mimeSubtypeName = e.e(var1);
      this.profileLevelIdc = e.e(var1);
      this.codecs = e.e(var1);
      this.protection = e.e(var1);
      this.languages = e.e(var1);

      int var2;
      for(var2 = e.d(var1); var2 > 0; --var2) {
         this.brandEntries.put(e.e(var1), e.e(var1));
      }

      for(var2 = e.d(var1); var2 > 0; --var2) {
         this.idEntries.put(e.e(var1), e.e(var1));
      }

   }

   public Map getBrandEntries() {
      a var1 = b.a(ajc$tjp_10, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.brandEntries;
   }

   public String getCodecs() {
      a var1 = b.a(ajc$tjp_4, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.codecs;
   }

   protected void getContent(ByteBuffer var1) {
      this.writeVersionAndFlags(var1);
      g.b(var1, this.mimeSubtypeName);
      g.b(var1, this.profileLevelIdc);
      g.b(var1, this.codecs);
      g.b(var1, this.protection);
      g.b(var1, this.languages);
      g.c(var1, this.brandEntries.size());
      Iterator var2 = this.brandEntries.entrySet().iterator();

      Entry var3;
      while(var2.hasNext()) {
         var3 = (Entry)var2.next();
         g.b(var1, (String)var3.getKey());
         g.b(var1, (String)var3.getValue());
      }

      g.c(var1, this.idEntries.size());
      var2 = this.idEntries.entrySet().iterator();

      while(var2.hasNext()) {
         var3 = (Entry)var2.next();
         g.b(var1, (String)var3.getKey());
         g.b(var1, (String)var3.getValue());
      }

   }

   protected long getContentSize() {
      long var7 = (long)(j.b(this.mimeSubtypeName) + 1);
      long var1 = (long)(j.b(this.profileLevelIdc) + 1);
      long var3 = (long)(j.b(this.codecs) + 1);
      long var9 = (long)(j.b(this.protection) + 1);
      long var5 = (long)(j.b(this.languages) + 1);
      Iterator var11 = this.brandEntries.entrySet().iterator();

      Entry var12;
      for(var1 = 4L + var7 + var1 + var3 + var9 + var5 + 1L; var11.hasNext(); var1 = (long)(j.b((String)var12.getValue()) + 1) + var1 + var3) {
         var12 = (Entry)var11.next();
         var3 = (long)(j.b((String)var12.getKey()) + 1);
      }

      var11 = this.idEntries.entrySet().iterator();
      ++var1;

      while(var11.hasNext()) {
         var12 = (Entry)var11.next();
         var3 = (long)(j.b((String)var12.getKey()) + 1);
         var1 = (long)(j.b((String)var12.getValue()) + 1) + var1 + var3;
      }

      return var1;
   }

   public Map getIdEntries() {
      a var1 = b.a(ajc$tjp_12, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.idEntries;
   }

   public String getLanguages() {
      a var1 = b.a(ajc$tjp_8, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.languages;
   }

   public String getMimeSubtypeName() {
      a var1 = b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.mimeSubtypeName;
   }

   public String getProfileLevelIdc() {
      a var1 = b.a(ajc$tjp_2, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.profileLevelIdc;
   }

   public String getProtection() {
      a var1 = b.a(ajc$tjp_6, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.protection;
   }

   public void setBrandEntries(Map var1) {
      a var2 = b.a(ajc$tjp_11, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.brandEntries = var1;
   }

   public void setCodecs(String var1) {
      a var2 = b.a(ajc$tjp_5, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.codecs = var1;
   }

   public void setIdEntries(Map var1) {
      a var2 = b.a(ajc$tjp_13, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.idEntries = var1;
   }

   public void setLanguages(String var1) {
      a var2 = b.a(ajc$tjp_9, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.languages = var1;
   }

   public void setMimeSubtypeName(String var1) {
      a var2 = b.a(ajc$tjp_1, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.mimeSubtypeName = var1;
   }

   public void setProfileLevelIdc(String var1) {
      a var2 = b.a(ajc$tjp_3, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.profileLevelIdc = var1;
   }

   public void setProtection(String var1) {
      a var2 = b.a(ajc$tjp_7, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.protection = var1;
   }
}
