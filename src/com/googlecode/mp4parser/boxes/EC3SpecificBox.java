package com.googlecode.mp4parser.boxes;

import com.googlecode.mp4parser.AbstractBox;
import com.googlecode.mp4parser.e;
import com.googlecode.mp4parser.boxes.mp4.a.c;
import com.googlecode.mp4parser.boxes.mp4.a.d;
import java.nio.ByteBuffer;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import org.mp4parser.aspectj.a.b.b;

public class EC3SpecificBox extends AbstractBox {
   public static final String TYPE = "dec3";
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_0;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_1;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_2;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_3;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_4;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_5;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_6;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_7;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_8;
   int dataRate;
   List entries = new LinkedList();
   int numIndSub;

   static {
      ajc$preClinit();
   }

   public EC3SpecificBox() {
      super("dec3");
   }

   private static void ajc$preClinit() {
      b var0 = new b("EC3SpecificBox.java", EC3SpecificBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getContentSize", "com.googlecode.mp4parser.boxes.EC3SpecificBox", "", "", "", "long"), 25);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "getContent", "com.googlecode.mp4parser.boxes.EC3SpecificBox", "java.nio.ByteBuffer", "byteBuffer", "", "void"), 65);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "getEntries", "com.googlecode.mp4parser.boxes.EC3SpecificBox", "", "", "", "java.util.List"), 86);
      ajc$tjp_3 = var0.a("method-execution", var0.a("1", "setEntries", "com.googlecode.mp4parser.boxes.EC3SpecificBox", "java.util.List", "entries", "", "void"), 90);
      ajc$tjp_4 = var0.a("method-execution", var0.a("1", "addEntry", "com.googlecode.mp4parser.boxes.EC3SpecificBox", "com.googlecode.mp4parser.boxes.EC3SpecificBox$Entry", "entry", "", "void"), 94);
      ajc$tjp_5 = var0.a("method-execution", var0.a("1", "getDataRate", "com.googlecode.mp4parser.boxes.EC3SpecificBox", "", "", "", "int"), 98);
      ajc$tjp_6 = var0.a("method-execution", var0.a("1", "setDataRate", "com.googlecode.mp4parser.boxes.EC3SpecificBox", "int", "dataRate", "", "void"), 102);
      ajc$tjp_7 = var0.a("method-execution", var0.a("1", "getNumIndSub", "com.googlecode.mp4parser.boxes.EC3SpecificBox", "", "", "", "int"), 106);
      ajc$tjp_8 = var0.a("method-execution", var0.a("1", "setNumIndSub", "com.googlecode.mp4parser.boxes.EC3SpecificBox", "int", "numIndSub", "", "void"), 110);
   }

   public void _parseDetails(ByteBuffer var1) {
      c var3 = new c(var1);
      this.dataRate = var3.a(13);
      this.numIndSub = var3.a(3) + 1;

      for(int var2 = 0; var2 < this.numIndSub; ++var2) {
         EC3SpecificBox.a var4 = new EC3SpecificBox.a();
         var4.a = var3.a(2);
         var4.b = var3.a(5);
         var4.c = var3.a(5);
         var4.d = var3.a(3);
         var4.e = var3.a(1);
         var4.f = var3.a(3);
         var4.g = var3.a(4);
         if(var4.g > 0) {
            var4.h = var3.a(9);
         } else {
            var4.i = var3.a(1);
         }

         this.entries.add(var4);
      }

   }

   public void addEntry(EC3SpecificBox.a var1) {
      org.mp4parser.aspectj.lang.a var2 = b.a(ajc$tjp_4, this, this, (Object)var1);
      e.a().a(var2);
      this.entries.add(var1);
   }

   public void getContent(ByteBuffer var1) {
      org.mp4parser.aspectj.lang.a var2 = b.a(ajc$tjp_1, this, this, (Object)var1);
      e.a().a(var2);
      d var5 = new d(var1);
      var5.a(this.dataRate, 13);
      var5.a(this.entries.size() - 1, 3);
      Iterator var3 = this.entries.iterator();

      while(var3.hasNext()) {
         EC3SpecificBox.a var4 = (EC3SpecificBox.a)var3.next();
         var5.a(var4.a, 2);
         var5.a(var4.b, 5);
         var5.a(var4.c, 5);
         var5.a(var4.d, 3);
         var5.a(var4.e, 1);
         var5.a(var4.f, 3);
         var5.a(var4.g, 4);
         if(var4.g > 0) {
            var5.a(var4.h, 9);
         } else {
            var5.a(var4.i, 1);
         }
      }

   }

   public long getContentSize() {
      org.mp4parser.aspectj.lang.a var3 = b.a(ajc$tjp_0, this, this);
      e.a().a(var3);
      Iterator var4 = this.entries.iterator();
      long var1 = 2L;

      while(var4.hasNext()) {
         if(((EC3SpecificBox.a)var4.next()).g > 0) {
            var1 += 4L;
         } else {
            var1 += 3L;
         }
      }

      return var1;
   }

   public int getDataRate() {
      org.mp4parser.aspectj.lang.a var1 = b.a(ajc$tjp_5, this, this);
      e.a().a(var1);
      return this.dataRate;
   }

   public List getEntries() {
      org.mp4parser.aspectj.lang.a var1 = b.a(ajc$tjp_2, this, this);
      e.a().a(var1);
      return this.entries;
   }

   public int getNumIndSub() {
      org.mp4parser.aspectj.lang.a var1 = b.a(ajc$tjp_7, this, this);
      e.a().a(var1);
      return this.numIndSub;
   }

   public void setDataRate(int var1) {
      org.mp4parser.aspectj.lang.a var2 = b.a(ajc$tjp_6, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      e.a().a(var2);
      this.dataRate = var1;
   }

   public void setEntries(List var1) {
      org.mp4parser.aspectj.lang.a var2 = b.a(ajc$tjp_3, this, this, (Object)var1);
      e.a().a(var2);
      this.entries = var1;
   }

   public void setNumIndSub(int var1) {
      org.mp4parser.aspectj.lang.a var2 = b.a(ajc$tjp_8, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      e.a().a(var2);
      this.numIndSub = var1;
   }

   public static class a {
      public int a;
      public int b;
      public int c;
      public int d;
      public int e;
      public int f;
      public int g;
      public int h;
      public int i;

      public String toString() {
         return "Entry{fscod=" + this.a + ", bsid=" + this.b + ", bsmod=" + this.c + ", acmod=" + this.d + ", lfeon=" + this.e + ", reserved=" + this.f + ", num_dep_sub=" + this.g + ", chan_loc=" + this.h + ", reserved2=" + this.i + '}';
      }
   }
}
