package com.googlecode.mp4parser.boxes;

import com.coremedia.iso.e;
import com.coremedia.iso.g;
import com.googlecode.mp4parser.AbstractBox;
import com.googlecode.mp4parser.boxes.mp4.a.c;
import com.googlecode.mp4parser.boxes.mp4.a.d;
import java.nio.ByteBuffer;
import org.mp4parser.aspectj.a.b.b;
import org.mp4parser.aspectj.lang.a;

public class DTSSpecificBox extends AbstractBox {
   public static final String TYPE = "ddts";
   private static final a.a ajc$tjp_0;
   private static final a.a ajc$tjp_1;
   private static final a.a ajc$tjp_10;
   private static final a.a ajc$tjp_11;
   private static final a.a ajc$tjp_12;
   private static final a.a ajc$tjp_13;
   private static final a.a ajc$tjp_14;
   private static final a.a ajc$tjp_15;
   private static final a.a ajc$tjp_16;
   private static final a.a ajc$tjp_17;
   private static final a.a ajc$tjp_18;
   private static final a.a ajc$tjp_19;
   private static final a.a ajc$tjp_2;
   private static final a.a ajc$tjp_20;
   private static final a.a ajc$tjp_21;
   private static final a.a ajc$tjp_22;
   private static final a.a ajc$tjp_23;
   private static final a.a ajc$tjp_24;
   private static final a.a ajc$tjp_25;
   private static final a.a ajc$tjp_26;
   private static final a.a ajc$tjp_27;
   private static final a.a ajc$tjp_28;
   private static final a.a ajc$tjp_29;
   private static final a.a ajc$tjp_3;
   private static final a.a ajc$tjp_30;
   private static final a.a ajc$tjp_31;
   private static final a.a ajc$tjp_4;
   private static final a.a ajc$tjp_5;
   private static final a.a ajc$tjp_6;
   private static final a.a ajc$tjp_7;
   private static final a.a ajc$tjp_8;
   private static final a.a ajc$tjp_9;
   long DTSSamplingFrequency;
   int LBRDurationMod;
   long avgBitRate;
   int channelLayout;
   int coreLFEPresent;
   int coreLayout;
   int coreSize;
   int frameDuration;
   long maxBitRate;
   int multiAssetFlag;
   int pcmSampleDepth;
   int representationType;
   int reserved;
   int reservedBoxPresent;
   int stereoDownmix;
   int streamConstruction;

   static {
      ajc$preClinit();
   }

   public DTSSpecificBox() {
      super("ddts");
   }

   private static void ajc$preClinit() {
      b var0 = new b("DTSSpecificBox.java", DTSSpecificBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getAvgBitRate", "com.googlecode.mp4parser.boxes.DTSSpecificBox", "", "", "", "long"), 89);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "setAvgBitRate", "com.googlecode.mp4parser.boxes.DTSSpecificBox", "long", "avgBitRate", "", "void"), 93);
      ajc$tjp_10 = var0.a("method-execution", var0.a("1", "getStreamConstruction", "com.googlecode.mp4parser.boxes.DTSSpecificBox", "", "", "", "int"), 129);
      ajc$tjp_11 = var0.a("method-execution", var0.a("1", "setStreamConstruction", "com.googlecode.mp4parser.boxes.DTSSpecificBox", "int", "streamConstruction", "", "void"), 133);
      ajc$tjp_12 = var0.a("method-execution", var0.a("1", "getCoreLFEPresent", "com.googlecode.mp4parser.boxes.DTSSpecificBox", "", "", "", "int"), 137);
      ajc$tjp_13 = var0.a("method-execution", var0.a("1", "setCoreLFEPresent", "com.googlecode.mp4parser.boxes.DTSSpecificBox", "int", "coreLFEPresent", "", "void"), 141);
      ajc$tjp_14 = var0.a("method-execution", var0.a("1", "getCoreLayout", "com.googlecode.mp4parser.boxes.DTSSpecificBox", "", "", "", "int"), 145);
      ajc$tjp_15 = var0.a("method-execution", var0.a("1", "setCoreLayout", "com.googlecode.mp4parser.boxes.DTSSpecificBox", "int", "coreLayout", "", "void"), 149);
      ajc$tjp_16 = var0.a("method-execution", var0.a("1", "getCoreSize", "com.googlecode.mp4parser.boxes.DTSSpecificBox", "", "", "", "int"), 153);
      ajc$tjp_17 = var0.a("method-execution", var0.a("1", "setCoreSize", "com.googlecode.mp4parser.boxes.DTSSpecificBox", "int", "coreSize", "", "void"), 157);
      ajc$tjp_18 = var0.a("method-execution", var0.a("1", "getStereoDownmix", "com.googlecode.mp4parser.boxes.DTSSpecificBox", "", "", "", "int"), 161);
      ajc$tjp_19 = var0.a("method-execution", var0.a("1", "setStereoDownmix", "com.googlecode.mp4parser.boxes.DTSSpecificBox", "int", "stereoDownmix", "", "void"), 165);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "getDTSSamplingFrequency", "com.googlecode.mp4parser.boxes.DTSSpecificBox", "", "", "", "long"), 97);
      ajc$tjp_20 = var0.a("method-execution", var0.a("1", "getRepresentationType", "com.googlecode.mp4parser.boxes.DTSSpecificBox", "", "", "", "int"), 169);
      ajc$tjp_21 = var0.a("method-execution", var0.a("1", "setRepresentationType", "com.googlecode.mp4parser.boxes.DTSSpecificBox", "int", "representationType", "", "void"), 173);
      ajc$tjp_22 = var0.a("method-execution", var0.a("1", "getChannelLayout", "com.googlecode.mp4parser.boxes.DTSSpecificBox", "", "", "", "int"), 177);
      ajc$tjp_23 = var0.a("method-execution", var0.a("1", "setChannelLayout", "com.googlecode.mp4parser.boxes.DTSSpecificBox", "int", "channelLayout", "", "void"), 181);
      ajc$tjp_24 = var0.a("method-execution", var0.a("1", "getMultiAssetFlag", "com.googlecode.mp4parser.boxes.DTSSpecificBox", "", "", "", "int"), 185);
      ajc$tjp_25 = var0.a("method-execution", var0.a("1", "setMultiAssetFlag", "com.googlecode.mp4parser.boxes.DTSSpecificBox", "int", "multiAssetFlag", "", "void"), 189);
      ajc$tjp_26 = var0.a("method-execution", var0.a("1", "getLBRDurationMod", "com.googlecode.mp4parser.boxes.DTSSpecificBox", "", "", "", "int"), 193);
      ajc$tjp_27 = var0.a("method-execution", var0.a("1", "setLBRDurationMod", "com.googlecode.mp4parser.boxes.DTSSpecificBox", "int", "LBRDurationMod", "", "void"), 197);
      ajc$tjp_28 = var0.a("method-execution", var0.a("1", "getReserved", "com.googlecode.mp4parser.boxes.DTSSpecificBox", "", "", "", "int"), 201);
      ajc$tjp_29 = var0.a("method-execution", var0.a("1", "setReserved", "com.googlecode.mp4parser.boxes.DTSSpecificBox", "int", "reserved", "", "void"), 205);
      ajc$tjp_3 = var0.a("method-execution", var0.a("1", "setDTSSamplingFrequency", "com.googlecode.mp4parser.boxes.DTSSpecificBox", "long", "DTSSamplingFrequency", "", "void"), 101);
      ajc$tjp_30 = var0.a("method-execution", var0.a("1", "getReservedBoxPresent", "com.googlecode.mp4parser.boxes.DTSSpecificBox", "", "", "", "int"), 209);
      ajc$tjp_31 = var0.a("method-execution", var0.a("1", "setReservedBoxPresent", "com.googlecode.mp4parser.boxes.DTSSpecificBox", "int", "reservedBoxPresent", "", "void"), 213);
      ajc$tjp_4 = var0.a("method-execution", var0.a("1", "getMaxBitRate", "com.googlecode.mp4parser.boxes.DTSSpecificBox", "", "", "", "long"), 105);
      ajc$tjp_5 = var0.a("method-execution", var0.a("1", "setMaxBitRate", "com.googlecode.mp4parser.boxes.DTSSpecificBox", "long", "maxBitRate", "", "void"), 109);
      ajc$tjp_6 = var0.a("method-execution", var0.a("1", "getPcmSampleDepth", "com.googlecode.mp4parser.boxes.DTSSpecificBox", "", "", "", "int"), 113);
      ajc$tjp_7 = var0.a("method-execution", var0.a("1", "setPcmSampleDepth", "com.googlecode.mp4parser.boxes.DTSSpecificBox", "int", "pcmSampleDepth", "", "void"), 117);
      ajc$tjp_8 = var0.a("method-execution", var0.a("1", "getFrameDuration", "com.googlecode.mp4parser.boxes.DTSSpecificBox", "", "", "", "int"), 121);
      ajc$tjp_9 = var0.a("method-execution", var0.a("1", "setFrameDuration", "com.googlecode.mp4parser.boxes.DTSSpecificBox", "int", "frameDuration", "", "void"), 125);
   }

   public void _parseDetails(ByteBuffer var1) {
      this.DTSSamplingFrequency = e.a(var1);
      this.maxBitRate = e.a(var1);
      this.avgBitRate = e.a(var1);
      this.pcmSampleDepth = e.d(var1);
      c var2 = new c(var1);
      this.frameDuration = var2.a(2);
      this.streamConstruction = var2.a(5);
      this.coreLFEPresent = var2.a(1);
      this.coreLayout = var2.a(6);
      this.coreSize = var2.a(14);
      this.stereoDownmix = var2.a(1);
      this.representationType = var2.a(3);
      this.channelLayout = var2.a(16);
      this.multiAssetFlag = var2.a(1);
      this.LBRDurationMod = var2.a(1);
      this.reservedBoxPresent = var2.a(1);
      this.reserved = var2.a(5);
   }

   public long getAvgBitRate() {
      a var1 = b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.avgBitRate;
   }

   public int getChannelLayout() {
      a var1 = b.a(ajc$tjp_22, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.channelLayout;
   }

   protected void getContent(ByteBuffer var1) {
      g.b(var1, this.DTSSamplingFrequency);
      g.b(var1, this.maxBitRate);
      g.b(var1, this.avgBitRate);
      g.c(var1, this.pcmSampleDepth);
      d var2 = new d(var1);
      var2.a(this.frameDuration, 2);
      var2.a(this.streamConstruction, 5);
      var2.a(this.coreLFEPresent, 1);
      var2.a(this.coreLayout, 6);
      var2.a(this.coreSize, 14);
      var2.a(this.stereoDownmix, 1);
      var2.a(this.representationType, 3);
      var2.a(this.channelLayout, 16);
      var2.a(this.multiAssetFlag, 1);
      var2.a(this.LBRDurationMod, 1);
      var2.a(this.reservedBoxPresent, 1);
      var2.a(this.reserved, 5);
   }

   protected long getContentSize() {
      return 20L;
   }

   public int getCoreLFEPresent() {
      a var1 = b.a(ajc$tjp_12, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.coreLFEPresent;
   }

   public int getCoreLayout() {
      a var1 = b.a(ajc$tjp_14, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.coreLayout;
   }

   public int getCoreSize() {
      a var1 = b.a(ajc$tjp_16, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.coreSize;
   }

   public long getDTSSamplingFrequency() {
      a var1 = b.a(ajc$tjp_2, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.DTSSamplingFrequency;
   }

   public int[] getDashAudioChannelConfiguration() {
      int var6 = this.getChannelLayout();
      byte var1;
      byte var3;
      if((var6 & 1) == 1) {
         var1 = 4;
         var3 = 1;
      } else {
         var1 = 0;
         var3 = 0;
      }

      int var4 = var1;
      int var2 = var3;
      if((var6 & 2) == 2) {
         var2 = var3 + 2;
         var4 = var1 | 1 | 2;
      }

      int var8 = var4;
      int var7 = var2;
      if((var6 & 4) == 4) {
         var7 = var2 + 2;
         var8 = var4 | 16 | 32;
      }

      var4 = var8;
      var2 = var7;
      if((var6 & 8) == 8) {
         var2 = var7 + 1;
         var4 = var8 | 8;
      }

      var7 = var4;
      var8 = var2;
      if((var6 & 16) == 16) {
         var8 = var2 + 1;
         var7 = var4 | 256;
      }

      var4 = var7;
      var2 = var8;
      if((var6 & 32) == 32) {
         var2 = var8 + 2;
         var4 = var7 | 4096 | 16384;
      }

      var7 = var4;
      int var5 = var2;
      if((var6 & 64) == 64) {
         var5 = var2 + 2;
         var7 = var4 | 16 | 32;
      }

      var8 = var7;
      var2 = var5;
      if((var6 & 128) == 128) {
         var2 = var5 + 1;
         var8 = var7 | 8192;
      }

      var4 = var8;
      var7 = var2;
      if((var6 & 256) == 256) {
         var7 = var2 + 1;
         var4 = var8 | 2048;
      }

      var8 = var4;
      var5 = var7;
      if((var6 & 512) == 512) {
         var5 = var7 + 2;
         var8 = var4 | 64 | 128;
      }

      var7 = var8;
      var2 = var5;
      if((var6 & 1024) == 1024) {
         var2 = var5 + 2;
         var7 = var8 | 512 | 1024;
      }

      var8 = var7;
      var4 = var2;
      if((var6 & 2048) == 2048) {
         var4 = var2 + 2;
         var8 = var7 | 16 | 32;
      }

      var2 = var8;
      var7 = var4;
      if((var6 & 4096) == 4096) {
         var7 = var4 + 1;
         var2 = var8 | 8;
      }

      var4 = var2;
      var8 = var7;
      if((var6 & 8192) == 8192) {
         var8 = var7 + 2;
         var4 = var2 | 16 | 32;
      }

      var7 = var4;
      var2 = var8;
      if((var6 & 16384) == 16384) {
         var2 = var8 + 1;
         var7 = var4 | 65536;
      }

      if((var6 & '耀') == '耀') {
         var2 += 2;
         var8 = var7 | '耀' | 131072;
      } else {
         var8 = var7;
      }

      var7 = var2;
      if((var6 & 65536) == 65536) {
         var7 = var2 + 1;
      }

      var2 = var7;
      if((var6 & 131072) == 131072) {
         var2 = var7 + 2;
      }

      return new int[]{var2, var8};
   }

   public int getFrameDuration() {
      a var1 = b.a(ajc$tjp_8, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.frameDuration;
   }

   public int getLBRDurationMod() {
      a var1 = b.a(ajc$tjp_26, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.LBRDurationMod;
   }

   public long getMaxBitRate() {
      a var1 = b.a(ajc$tjp_4, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.maxBitRate;
   }

   public int getMultiAssetFlag() {
      a var1 = b.a(ajc$tjp_24, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.multiAssetFlag;
   }

   public int getPcmSampleDepth() {
      a var1 = b.a(ajc$tjp_6, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.pcmSampleDepth;
   }

   public int getRepresentationType() {
      a var1 = b.a(ajc$tjp_20, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.representationType;
   }

   public int getReserved() {
      a var1 = b.a(ajc$tjp_28, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.reserved;
   }

   public int getReservedBoxPresent() {
      a var1 = b.a(ajc$tjp_30, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.reservedBoxPresent;
   }

   public int getStereoDownmix() {
      a var1 = b.a(ajc$tjp_18, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.stereoDownmix;
   }

   public int getStreamConstruction() {
      a var1 = b.a(ajc$tjp_10, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.streamConstruction;
   }

   public void setAvgBitRate(long var1) {
      a var3 = b.a(ajc$tjp_1, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var3);
      this.avgBitRate = var1;
   }

   public void setChannelLayout(int var1) {
      a var2 = b.a(ajc$tjp_23, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.channelLayout = var1;
   }

   public void setCoreLFEPresent(int var1) {
      a var2 = b.a(ajc$tjp_13, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.coreLFEPresent = var1;
   }

   public void setCoreLayout(int var1) {
      a var2 = b.a(ajc$tjp_15, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.coreLayout = var1;
   }

   public void setCoreSize(int var1) {
      a var2 = b.a(ajc$tjp_17, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.coreSize = var1;
   }

   public void setDTSSamplingFrequency(long var1) {
      a var3 = b.a(ajc$tjp_3, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var3);
      this.DTSSamplingFrequency = var1;
   }

   public void setFrameDuration(int var1) {
      a var2 = b.a(ajc$tjp_9, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.frameDuration = var1;
   }

   public void setLBRDurationMod(int var1) {
      a var2 = b.a(ajc$tjp_27, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.LBRDurationMod = var1;
   }

   public void setMaxBitRate(long var1) {
      a var3 = b.a(ajc$tjp_5, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var3);
      this.maxBitRate = var1;
   }

   public void setMultiAssetFlag(int var1) {
      a var2 = b.a(ajc$tjp_25, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.multiAssetFlag = var1;
   }

   public void setPcmSampleDepth(int var1) {
      a var2 = b.a(ajc$tjp_7, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.pcmSampleDepth = var1;
   }

   public void setRepresentationType(int var1) {
      a var2 = b.a(ajc$tjp_21, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.representationType = var1;
   }

   public void setReserved(int var1) {
      a var2 = b.a(ajc$tjp_29, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.reserved = var1;
   }

   public void setReservedBoxPresent(int var1) {
      a var2 = b.a(ajc$tjp_31, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.reservedBoxPresent = var1;
   }

   public void setStereoDownmix(int var1) {
      a var2 = b.a(ajc$tjp_19, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.stereoDownmix = var1;
   }

   public void setStreamConstruction(int var1) {
      a var2 = b.a(ajc$tjp_11, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.streamConstruction = var1;
   }
}
