package com.googlecode.mp4parser.boxes.microsoft;

import com.googlecode.mp4parser.AbstractBox;
import com.googlecode.mp4parser.e;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Date;
import java.util.Iterator;
import java.util.Vector;

public class XtraBox extends AbstractBox {
   private static final long FILETIME_EPOCH_DIFF = 11644473600000L;
   private static final long FILETIME_ONE_MILLISECOND = 10000L;
   public static final int MP4_XTRA_BT_FILETIME = 21;
   public static final int MP4_XTRA_BT_GUID = 72;
   public static final int MP4_XTRA_BT_INT64 = 19;
   public static final int MP4_XTRA_BT_UNICODE = 8;
   public static final String TYPE = "Xtra";
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_0;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_1;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_10;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_2;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_3;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_4;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_5;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_6;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_7;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_8;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_9;
   ByteBuffer data;
   private boolean successfulParse = false;
   Vector tags = new Vector();

   static {
      ajc$preClinit();
   }

   public XtraBox() {
      super("Xtra");
   }

   public XtraBox(String var1) {
      super(var1);
   }

   private static void ajc$preClinit() {
      org.mp4parser.aspectj.a.b.b var0 = new org.mp4parser.aspectj.a.b.b("XtraBox.java", XtraBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "toString", "com.googlecode.mp4parser.boxes.microsoft.XtraBox", "", "", "", "java.lang.String"), 88);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "getAllTagNames", "com.googlecode.mp4parser.boxes.microsoft.XtraBox", "", "", "", "[Ljava.lang.String;"), 151);
      ajc$tjp_10 = var0.a("method-execution", var0.a("1", "setTagValue", "com.googlecode.mp4parser.boxes.microsoft.XtraBox", "java.lang.String:long", "name:value", "", "void"), 289);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "getFirstStringValue", "com.googlecode.mp4parser.boxes.microsoft.XtraBox", "java.lang.String", "name", "", "java.lang.String"), 166);
      ajc$tjp_3 = var0.a("method-execution", var0.a("1", "getFirstDateValue", "com.googlecode.mp4parser.boxes.microsoft.XtraBox", "java.lang.String", "name", "", "java.util.Date"), 183);
      ajc$tjp_4 = var0.a("method-execution", var0.a("1", "getFirstLongValue", "com.googlecode.mp4parser.boxes.microsoft.XtraBox", "java.lang.String", "name", "", "java.lang.Long"), 200);
      ajc$tjp_5 = var0.a("method-execution", var0.a("1", "getValues", "com.googlecode.mp4parser.boxes.microsoft.XtraBox", "java.lang.String", "name", "", "[Ljava.lang.Object;"), 216);
      ajc$tjp_6 = var0.a("method-execution", var0.a("1", "removeTag", "com.googlecode.mp4parser.boxes.microsoft.XtraBox", "java.lang.String", "name", "", "void"), 236);
      ajc$tjp_7 = var0.a("method-execution", var0.a("1", "setTagValues", "com.googlecode.mp4parser.boxes.microsoft.XtraBox", "java.lang.String:[Ljava.lang.String;", "name:values", "", "void"), 249);
      ajc$tjp_8 = var0.a("method-execution", var0.a("1", "setTagValue", "com.googlecode.mp4parser.boxes.microsoft.XtraBox", "java.lang.String:java.lang.String", "name:value", "", "void"), 265);
      ajc$tjp_9 = var0.a("method-execution", var0.a("1", "setTagValue", "com.googlecode.mp4parser.boxes.microsoft.XtraBox", "java.lang.String:java.util.Date", "name:date", "", "void"), 276);
   }

   private int detailSize() {
      int var2 = 0;

      int var1;
      for(var1 = 0; var2 < this.tags.size(); ++var2) {
         var1 += ((XtraBox.a)this.tags.elementAt(var2)).a();
      }

      return var1;
   }

   private static long filetimeToMillis(long var0) {
      return var0 / 10000L - 11644473600000L;
   }

   private XtraBox.a getTagByName(String var1) {
      Iterator var3 = this.tags.iterator();

      XtraBox.a var4;
      while(true) {
         if(!var3.hasNext()) {
            var4 = null;
            break;
         }

         XtraBox.a var2 = (XtraBox.a)var3.next();
         if(var2.b.equals(var1)) {
            var4 = var2;
            break;
         }
      }

      return var4;
   }

   private static long millisToFiletime(long var0) {
      return (11644473600000L + var0) * 10000L;
   }

   private static String readAsciiString(ByteBuffer var0, int var1) {
      byte[] var2 = new byte[var1];
      var0.get(var2);

      try {
         String var4 = new String(var2, "US-ASCII");
         return var4;
      } catch (UnsupportedEncodingException var3) {
         throw new RuntimeException("Shouldn't happen", var3);
      }
   }

   private static String readUtf16String(ByteBuffer var0, int var1) {
      char[] var3 = new char[var1 / 2 - 1];

      for(int var2 = 0; var2 < var1 / 2 - 1; ++var2) {
         var3[var2] = var0.getChar();
      }

      var0.getChar();
      return new String(var3);
   }

   private static void writeAsciiString(ByteBuffer var0, String var1) {
      try {
         var0.put(var1.getBytes("US-ASCII"));
      } catch (UnsupportedEncodingException var2) {
         throw new RuntimeException("Shouldn't happen", var2);
      }
   }

   private static void writeUtf16String(ByteBuffer var0, String var1) {
      char[] var3 = var1.toCharArray();

      for(int var2 = 0; var2 < var3.length; ++var2) {
         var0.putChar(var3[var2]);
      }

      var0.putChar('\u0000');
   }

   public void _parseDetails(ByteBuffer param1) {
      // $FF: Couldn't be decompiled
   }

   public String[] getAllTagNames() {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_1, this, this);
      e.a().a(var2);
      String[] var3 = new String[this.tags.size()];

      for(int var1 = 0; var1 < this.tags.size(); ++var1) {
         var3[var1] = ((XtraBox.a)this.tags.elementAt(var1)).b;
      }

      return var3;
   }

   protected void getContent(ByteBuffer var1) {
      if(this.successfulParse) {
         for(int var2 = 0; var2 < this.tags.size(); ++var2) {
            ((XtraBox.a)this.tags.elementAt(var2)).b(var1);
         }
      } else {
         this.data.rewind();
         var1.put(this.data);
      }

   }

   protected long getContentSize() {
      long var1;
      if(this.successfulParse) {
         var1 = (long)this.detailSize();
      } else {
         var1 = (long)this.data.limit();
      }

      return var1;
   }

   public Date getFirstDateValue(String var1) {
      org.mp4parser.aspectj.lang.a var4 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_3, this, this, (Object)var1);
      e.a().a(var4);
      Object[] var5 = this.getValues(var1);
      int var3 = var5.length;
      int var2 = 0;

      Date var6;
      while(true) {
         if(var2 >= var3) {
            var6 = null;
            break;
         }

         Object var7 = var5[var2];
         if(var7 instanceof Date) {
            var6 = (Date)var7;
            break;
         }

         ++var2;
      }

      return var6;
   }

   public Long getFirstLongValue(String var1) {
      org.mp4parser.aspectj.lang.a var4 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_4, this, this, (Object)var1);
      e.a().a(var4);
      Object[] var7 = this.getValues(var1);
      int var3 = var7.length;
      int var2 = 0;

      Long var6;
      while(true) {
         if(var2 >= var3) {
            var6 = null;
            break;
         }

         Object var5 = var7[var2];
         if(var5 instanceof Long) {
            var6 = (Long)var5;
            break;
         }

         ++var2;
      }

      return var6;
   }

   public String getFirstStringValue(String var1) {
      org.mp4parser.aspectj.lang.a var4 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_2, this, this, (Object)var1);
      e.a().a(var4);
      Object[] var6 = this.getValues(var1);
      int var3 = var6.length;
      int var2 = 0;

      while(true) {
         if(var2 >= var3) {
            var1 = null;
            break;
         }

         Object var5 = var6[var2];
         if(var5 instanceof String) {
            var1 = (String)var5;
            break;
         }

         ++var2;
      }

      return var1;
   }

   public Object[] getValues(String var1) {
      org.mp4parser.aspectj.lang.a var3 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_5, this, this, (Object)var1);
      e.a().a(var3);
      XtraBox.a var5 = this.getTagByName(var1);
      Object[] var4;
      if(var5 != null) {
         var4 = new Object[var5.c.size()];

         for(int var2 = 0; var2 < var5.c.size(); ++var2) {
            var4[var2] = ((XtraBox.b)var5.c.elementAt(var2)).a();
         }
      } else {
         var4 = new Object[0];
      }

      return var4;
   }

   public void removeTag(String var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_6, this, this, (Object)var1);
      e.a().a(var2);
      XtraBox.a var3 = this.getTagByName(var1);
      if(var3 != null) {
         this.tags.remove(var3);
      }

   }

   public void setTagValue(String var1, long var2) {
      org.mp4parser.aspectj.lang.a var4 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_10, this, this, var1, org.mp4parser.aspectj.a.a.a.a(var2));
      e.a().a(var4);
      this.removeTag(var1);
      XtraBox.a var5 = new XtraBox.a(var1, (XtraBox.a)null);
      var5.c.addElement(new XtraBox.b(var2, (XtraBox.b)null));
      this.tags.addElement(var5);
   }

   public void setTagValue(String var1, String var2) {
      org.mp4parser.aspectj.lang.a var3 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_8, this, this, var1, var2);
      e.a().a(var3);
      this.setTagValues(var1, new String[]{var2});
   }

   public void setTagValue(String var1, Date var2) {
      org.mp4parser.aspectj.lang.a var3 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_9, this, this, var1, var2);
      e.a().a(var3);
      this.removeTag(var1);
      XtraBox.a var4 = new XtraBox.a(var1, (XtraBox.a)null);
      var4.c.addElement(new XtraBox.b(var2, (XtraBox.b)null));
      this.tags.addElement(var4);
   }

   public void setTagValues(String var1, String[] var2) {
      org.mp4parser.aspectj.lang.a var4 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_7, this, this, var1, var2);
      e.a().a(var4);
      this.removeTag(var1);
      XtraBox.a var5 = new XtraBox.a(var1, (XtraBox.a)null);

      for(int var3 = 0; var3 < var2.length; ++var3) {
         var5.c.addElement(new XtraBox.b(var2[var3], (XtraBox.b)null));
      }

      this.tags.addElement(var5);
   }

   public String toString() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_0, this, this);
      e.a().a(var1);
      if(!this.isParsed()) {
         this.parseDetails();
      }

      StringBuffer var6 = new StringBuffer();
      var6.append("XtraBox[");
      Iterator var2 = this.tags.iterator();

      while(var2.hasNext()) {
         XtraBox.a var3 = (XtraBox.a)var2.next();
         Iterator var4 = var3.c.iterator();

         while(var4.hasNext()) {
            XtraBox.b var5 = (XtraBox.b)var4.next();
            var6.append(var3.b);
            var6.append("=");
            var6.append(var5.toString());
            var6.append(";");
         }
      }

      var6.append("]");
      return var6.toString();
   }

   private static class a {
      private int a;
      private String b;
      private Vector c;

      private a() {
         this.c = new Vector();
      }

      // $FF: synthetic method
      a(XtraBox.a var1) {
         this();
      }

      private a(String var1) {
         this();
         this.b = var1;
      }

      // $FF: synthetic method
      a(String var1, XtraBox.a var2) {
         this(var1);
      }

      private int a() {
         int var2 = this.b.length() + 12;

         for(int var1 = 0; var1 < this.c.size(); ++var1) {
            var2 += ((XtraBox.b)this.c.elementAt(var1)).b();
         }

         return var2;
      }

      // $FF: synthetic method
      static void a(XtraBox.a var0, ByteBuffer var1) {
         var0.a(var1);
      }

      private void a(ByteBuffer var1) {
         this.a = var1.getInt();
         this.b = XtraBox.readAsciiString(var1, var1.getInt());
         int var3 = var1.getInt();

         for(int var2 = 0; var2 < var3; ++var2) {
            XtraBox.b var4 = new XtraBox.b((XtraBox.b)null);
            var4.a(var1);
            this.c.addElement(var4);
         }

         if(this.a != this.a()) {
            throw new RuntimeException("Improperly handled Xtra tag: Sizes don't match ( " + this.a + "/" + this.a() + ") on " + this.b);
         }
      }

      private void b(ByteBuffer var1) {
         var1.putInt(this.a());
         var1.putInt(this.b.length());
         XtraBox.writeAsciiString(var1, this.b);
         var1.putInt(this.c.size());

         for(int var2 = 0; var2 < this.c.size(); ++var2) {
            ((XtraBox.b)this.c.elementAt(var2)).b(var1);
         }

      }

      public String toString() {
         StringBuffer var2 = new StringBuffer();
         var2.append(this.b);
         var2.append(" [");
         var2.append(this.a);
         var2.append("/");
         var2.append(this.c.size());
         var2.append("]:\n");

         for(int var1 = 0; var1 < this.c.size(); ++var1) {
            var2.append("  ");
            var2.append(((XtraBox.b)this.c.elementAt(var1)).toString());
            var2.append("\n");
         }

         return var2.toString();
      }
   }

   private static class b {
      public int a;
      public String b;
      public long c;
      public byte[] d;
      public Date e;

      private b() {
      }

      private b(long var1) {
         this.a = 19;
         this.c = var1;
      }

      // $FF: synthetic method
      b(long var1, XtraBox.b var3) {
         this(var1);
      }

      // $FF: synthetic method
      b(XtraBox.b var1) {
         this();
      }

      private b(String var1) {
         this.a = 8;
         this.b = var1;
      }

      // $FF: synthetic method
      b(String var1, XtraBox.b var2) {
         this(var1);
      }

      private b(Date var1) {
         this.a = 21;
         this.e = var1;
      }

      // $FF: synthetic method
      b(Date var1, XtraBox.b var2) {
         this(var1);
      }

      private Object a() {
         Object var1;
         switch(this.a) {
         case 8:
            var1 = this.b;
            break;
         case 19:
            var1 = new Long(this.c);
            break;
         case 21:
            var1 = this.e;
            break;
         default:
            var1 = this.d;
         }

         return var1;
      }

      private void a(ByteBuffer var1) {
         int var2 = var1.getInt() - 6;
         this.a = var1.getShort();
         var1.order(ByteOrder.LITTLE_ENDIAN);
         switch(this.a) {
         case 8:
            this.b = XtraBox.readUtf16String(var1, var2);
            break;
         case 19:
            this.c = var1.getLong();
            break;
         case 21:
            this.e = new Date(XtraBox.filetimeToMillis(var1.getLong()));
            break;
         default:
            this.d = new byte[var2];
            var1.get(this.d);
         }

         var1.order(ByteOrder.BIG_ENDIAN);
      }

      private int b() {
         int var1;
         switch(this.a) {
         case 8:
            var1 = 6 + this.b.length() * 2 + 2;
            break;
         case 19:
         case 21:
            var1 = 14;
            break;
         default:
            var1 = 6 + this.d.length;
         }

         return var1;
      }

      private void b(ByteBuffer var1) {
         try {
            var1.putInt(this.b());
            var1.putShort((short)this.a);
            var1.order(ByteOrder.LITTLE_ENDIAN);
            switch(this.a) {
            case 8:
               XtraBox.writeUtf16String(var1, this.b);
               break;
            case 19:
               var1.putLong(this.c);
               break;
            case 21:
               var1.putLong(XtraBox.millisToFiletime(this.e.getTime()));
               break;
            default:
               var1.put(this.d);
            }
         } finally {
            var1.order(ByteOrder.BIG_ENDIAN);
         }

      }

      public String toString() {
         String var1;
         switch(this.a) {
         case 8:
            var1 = "[string]" + this.b;
            break;
         case 19:
            var1 = "[long]" + String.valueOf(this.c);
            break;
         case 21:
            var1 = "[filetime]" + this.e.toString();
            break;
         default:
            var1 = "[GUID](nonParsed)";
         }

         return var1;
      }
   }
}
