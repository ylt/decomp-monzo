package com.googlecode.mp4parser.boxes.mp4.a;

import java.io.IOException;
import java.lang.reflect.Modifier;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class l {
   protected static Logger a = Logger.getLogger(l.class.getName());
   protected static Map b = new HashMap();

   static {
      HashSet var3 = new HashSet();
      var3.add(f.class);
      var3.add(n.class);
      var3.add(b.class);
      var3.add(i.class);
      var3.add(k.class);
      var3.add(m.class);
      var3.add(a.class);
      var3.add(j.class);
      var3.add(h.class);
      var3.add(e.class);
      Iterator var6 = var3.iterator();

      while(var6.hasNext()) {
         Class var5 = (Class)var6.next();
         g var8 = (g)var5.getAnnotation(g.class);
         int[] var7 = var8.a();
         int var1 = var8.b();
         Map var4 = (Map)b.get(Integer.valueOf(var1));
         Object var9 = var4;
         if(var4 == null) {
            var9 = new HashMap();
         }

         int var2 = var7.length;

         for(int var0 = 0; var0 < var2; ++var0) {
            ((Map)var9).put(Integer.valueOf(var7[var0]), var5);
         }

         b.put(Integer.valueOf(var1), var9);
      }

   }

   public static b a(int var0, ByteBuffer var1) throws IOException {
      int var2 = com.coremedia.iso.e.d(var1);
      Map var4 = (Map)b.get(Integer.valueOf(var0));
      Map var3 = var4;
      if(var4 == null) {
         var3 = (Map)b.get(Integer.valueOf(-1));
      }

      Class var7 = (Class)var3.get(Integer.valueOf(var2));
      Object var6;
      if(var7 != null && !var7.isInterface() && !Modifier.isAbstract(var7.getModifiers())) {
         try {
            var6 = (b)var7.newInstance();
         } catch (Exception var5) {
            a.log(Level.SEVERE, "Couldn't instantiate BaseDescriptor class " + var7 + " for objectTypeIndication " + var0 + " and tag " + var2, var5);
            throw new RuntimeException(var5);
         }
      } else {
         a.warning("No ObjectDescriptor found for objectTypeIndication " + Integer.toHexString(var0) + " and tag " + Integer.toHexString(var2) + " found: " + var7);
         var6 = new o();
      }

      ((b)var6).a(var2, var1);
      return (b)var6;
   }
}
