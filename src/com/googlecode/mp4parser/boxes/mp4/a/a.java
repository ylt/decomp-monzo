package com.googlecode.mp4parser.boxes.mp4.a;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@g(
   a = {5},
   b = 64
)
public class a extends b {
   public static Map a = new HashMap();
   public static Map b = new HashMap();
   public int A;
   public int B;
   public boolean C;
   public boolean D;
   public boolean E;
   public int F;
   public boolean G;
   public int H;
   public int I;
   public int J;
   public int K;
   public int L;
   public int M;
   public int N;
   public int O;
   public int P;
   public int Q;
   public int R;
   public int S;
   public int T;
   public int U;
   public boolean V;
   byte[] W;
   boolean X = false;
   public a.a c;
   public int d;
   public int e;
   public int f;
   public int g;
   public int h;
   public int i;
   public boolean j;
   public boolean k;
   public int l = -1;
   public int m;
   public int n;
   public int o;
   public int p;
   public int q;
   public int r;
   public int s = -1;
   public int t = -1;
   public int u = -1;
   public int v;
   public int w;
   public int x;
   public int y;
   public int z;

   static {
      a.put(Integer.valueOf(0), Integer.valueOf(96000));
      a.put(Integer.valueOf(1), Integer.valueOf(88200));
      a.put(Integer.valueOf(2), Integer.valueOf('切'));
      a.put(Integer.valueOf(3), Integer.valueOf('뮀'));
      a.put(Integer.valueOf(4), Integer.valueOf('걄'));
      a.put(Integer.valueOf(5), Integer.valueOf(32000));
      a.put(Integer.valueOf(6), Integer.valueOf(24000));
      a.put(Integer.valueOf(7), Integer.valueOf(22050));
      a.put(Integer.valueOf(8), Integer.valueOf(16000));
      a.put(Integer.valueOf(9), Integer.valueOf(12000));
      a.put(Integer.valueOf(10), Integer.valueOf(11025));
      a.put(Integer.valueOf(11), Integer.valueOf(8000));
      b.put(Integer.valueOf(1), "AAC main");
      b.put(Integer.valueOf(2), "AAC LC");
      b.put(Integer.valueOf(3), "AAC SSR");
      b.put(Integer.valueOf(4), "AAC LTP");
      b.put(Integer.valueOf(5), "SBR");
      b.put(Integer.valueOf(6), "AAC Scalable");
      b.put(Integer.valueOf(7), "TwinVQ");
      b.put(Integer.valueOf(8), "CELP");
      b.put(Integer.valueOf(9), "HVXC");
      b.put(Integer.valueOf(10), "(reserved)");
      b.put(Integer.valueOf(11), "(reserved)");
      b.put(Integer.valueOf(12), "TTSI");
      b.put(Integer.valueOf(13), "Main synthetic");
      b.put(Integer.valueOf(14), "Wavetable synthesis");
      b.put(Integer.valueOf(15), "General MIDI");
      b.put(Integer.valueOf(16), "Algorithmic Synthesis and Audio FX");
      b.put(Integer.valueOf(17), "ER AAC LC");
      b.put(Integer.valueOf(18), "(reserved)");
      b.put(Integer.valueOf(19), "ER AAC LTP");
      b.put(Integer.valueOf(20), "ER AAC Scalable");
      b.put(Integer.valueOf(21), "ER TwinVQ");
      b.put(Integer.valueOf(22), "ER BSAC");
      b.put(Integer.valueOf(23), "ER AAC LD");
      b.put(Integer.valueOf(24), "ER CELP");
      b.put(Integer.valueOf(25), "ER HVXC");
      b.put(Integer.valueOf(26), "ER HILN");
      b.put(Integer.valueOf(27), "ER Parametric");
      b.put(Integer.valueOf(28), "SSC");
      b.put(Integer.valueOf(29), "PS");
      b.put(Integer.valueOf(30), "MPEG Surround");
      b.put(Integer.valueOf(31), "(escape)");
      b.put(Integer.valueOf(32), "Layer-1");
      b.put(Integer.valueOf(33), "Layer-2");
      b.put(Integer.valueOf(34), "Layer-3");
      b.put(Integer.valueOf(35), "DST");
      b.put(Integer.valueOf(36), "ALS");
      b.put(Integer.valueOf(37), "SLS");
      b.put(Integer.valueOf(38), "SLS non-core");
      b.put(Integer.valueOf(39), "ER AAC ELD");
      b.put(Integer.valueOf(40), "SMR Simple");
      b.put(Integer.valueOf(41), "SMR Main");
   }

   public a() {
      this.Y = 5;
   }

   private int a(c var1) throws IOException {
      int var3 = var1.a(5);
      int var2 = var3;
      if(var3 == 31) {
         var2 = var1.a(6) + 32;
      }

      return var2;
   }

   private void a(int var1, int var2, int var3, c var4) throws IOException {
      this.v = var4.a(1);
      this.w = var4.a(1);
      if(this.w == 1) {
         this.x = var4.a(14);
      }

      this.y = var4.a(1);
      if(var2 == 0) {
         throw new UnsupportedOperationException("can't parse program_config_element yet");
      } else {
         if(var3 == 6 || var3 == 20) {
            this.z = var4.a(3);
         }

         if(this.y == 1) {
            if(var3 == 22) {
               this.A = var4.a(5);
               this.B = var4.a(11);
            }

            if(var3 == 17 || var3 == 19 || var3 == 20 || var3 == 23) {
               this.C = var4.a();
               this.D = var4.a();
               this.E = var4.a();
            }

            this.F = var4.a(1);
            if(this.F == 1) {
               throw new RuntimeException("not yet implemented");
            }
         }

         this.G = true;
      }
   }

   private void a(int var1, d var2) {
      if(var1 >= 32) {
         var2.a(31, 5);
         var2.a(var1 - 32, 6);
      } else {
         var2.a(var1, 5);
      }

   }

   private void a(d var1) {
      var1.a(this.v, 1);
      var1.a(this.w, 1);
      if(this.w == 1) {
         var1.a(this.x, 14);
      }

      var1.a(this.y, 1);
      if(this.h == 0) {
         throw new UnsupportedOperationException("can't parse program_config_element yet");
      } else {
         if(this.d == 6 || this.d == 20) {
            var1.a(this.z, 3);
         }

         if(this.y == 1) {
            if(this.d == 22) {
               var1.a(this.A, 5);
               var1.a(this.B, 11);
            }

            if(this.d == 17 || this.d == 19 || this.d == 20 || this.d == 23) {
               var1.a(this.C);
               var1.a(this.D);
               var1.a(this.E);
            }

            var1.a(this.F, 1);
            if(this.F == 1) {
               throw new RuntimeException("not yet implemented");
            }
         }

      }
   }

   private void b(int var1, int var2, int var3, c var4) throws IOException {
      this.H = var4.a(1);
      if(this.H == 1) {
         this.c(var1, var2, var3, var4);
      } else {
         this.f(var1, var2, var3, var4);
      }

   }

   private void c(int var1, int var2, int var3, c var4) throws IOException {
      this.I = var4.a(2);
      if(this.I != 1) {
         this.d(var1, var2, var3, var4);
      }

      if(this.I != 0) {
         this.e(var1, var2, var3, var4);
      }

      this.J = var4.a(1);
      this.V = true;
   }

   private void d(int var1, int var2, int var3, c var4) throws IOException {
      this.K = var4.a(1);
      this.L = var4.a(2);
      this.M = var4.a(1);
      if(this.M == 1) {
         this.N = var4.a(1);
      }

   }

   private int e() {
      byte var1 = 2;
      if(this.w == 1) {
         var1 = 16;
      }

      int var2 = var1 + 1;
      if(this.h == 0) {
         throw new UnsupportedOperationException("can't parse program_config_element yet");
      } else {
         int var3;
         label38: {
            if(this.d != 6) {
               var3 = var2;
               if(this.d != 20) {
                  break label38;
               }
            }

            var3 = var2 + 3;
         }

         var2 = var3;
         if(this.y == 1) {
            var2 = var3;
            if(this.d == 22) {
               var2 = var3 + 5 + 11;
            }

            label30: {
               if(this.d != 17 && this.d != 19 && this.d != 20) {
                  var3 = var2;
                  if(this.d != 23) {
                     break label30;
                  }
               }

               var3 = var2 + 1 + 1 + 1;
            }

            var2 = var3 + 1;
            if(this.F == 1) {
               throw new RuntimeException("Not implemented");
            }
         }

         return var2;
      }
   }

   private void e(int var1, int var2, int var3, c var4) throws IOException {
      this.O = var4.a(1);
      this.P = var4.a(8);
      this.Q = var4.a(4);
      this.R = var4.a(12);
      this.S = var4.a(2);
   }

   private ByteBuffer f() {
      ByteBuffer var1 = ByteBuffer.wrap(new byte[this.a()]);
      d var2 = new d(var1);
      this.a(this.e, var2);
      var2.a(this.f, 4);
      if(this.f == 15) {
         var2.a(this.g, 24);
      }

      var2.a(this.h, 4);
      if(this.d == 5 || this.d == 29) {
         this.i = 5;
         this.j = true;
         if(this.d == 29) {
            this.k = true;
         }

         var2.a(this.l, 4);
         if(this.l == 15) {
            var2.a(this.m, 24);
         }

         this.a(this.d, var2);
         if(this.d == 22) {
            var2.a(this.n, 4);
         }
      }

      switch(this.d) {
      case 1:
      case 2:
      case 3:
      case 4:
      case 6:
      case 7:
      case 17:
      case 19:
      case 20:
      case 21:
      case 22:
      case 23:
         this.a(var2);
      case 5:
      case 10:
      case 11:
      case 18:
      case 29:
      case 31:
      default:
         break;
      case 8:
         throw new UnsupportedOperationException("can't write CelpSpecificConfig yet");
      case 9:
         throw new UnsupportedOperationException("can't write HvxcSpecificConfig yet");
      case 12:
         throw new UnsupportedOperationException("can't write TTSSpecificConfig yet");
      case 13:
      case 14:
      case 15:
      case 16:
         throw new UnsupportedOperationException("can't write StructuredAudioSpecificConfig yet");
      case 24:
         throw new UnsupportedOperationException("can't write ErrorResilientCelpSpecificConfig yet");
      case 25:
         throw new UnsupportedOperationException("can't write ErrorResilientHvxcSpecificConfig yet");
      case 26:
      case 27:
         throw new UnsupportedOperationException("can't write parseParametricSpecificConfig yet");
      case 28:
         throw new UnsupportedOperationException("can't write SSCSpecificConfig yet");
      case 30:
         var2.a(this.o, 1);
         throw new UnsupportedOperationException("can't write SpatialSpecificConfig yet");
      case 32:
      case 33:
      case 34:
         throw new UnsupportedOperationException("can't write MPEG_1_2_SpecificConfig yet");
      case 35:
         throw new UnsupportedOperationException("can't write DSTSpecificConfig yet");
      case 36:
         var2.a(this.p, 5);
         throw new UnsupportedOperationException("can't write ALSSpecificConfig yet");
      case 37:
      case 38:
         throw new UnsupportedOperationException("can't write SLSSpecificConfig yet");
      case 39:
         throw new UnsupportedOperationException("can't write ELDSpecificConfig yet");
      case 40:
      case 41:
         throw new UnsupportedOperationException("can't parse SymbolicMusicSpecificConfig yet");
      }

      switch(this.d) {
      case 17:
      case 19:
      case 20:
      case 21:
      case 22:
      case 23:
      case 24:
      case 25:
      case 26:
      case 27:
      case 39:
         var2.a(this.q, 2);
         if(this.q == 2 || this.q == 3) {
            throw new UnsupportedOperationException("can't parse ErrorProtectionSpecificConfig yet");
         }

         if(this.q == 3) {
            var2.a(this.r, 1);
            if(this.r == 0) {
               throw new RuntimeException("not implemented");
            }
         }
      case 18:
      case 28:
      case 29:
      case 30:
      case 31:
      case 32:
      case 33:
      case 34:
      case 35:
      case 36:
      case 37:
      case 38:
      }

      if(this.u >= 0) {
         var2.a(this.u, 11);
         if(this.u == 695) {
            this.a(this.i, var2);
            if(this.i == 5) {
               var2.a(this.j);
               if(this.j) {
                  var2.a(this.l, 4);
                  if(this.l == 15) {
                     var2.a(this.m, 24);
                  }

                  if(this.t >= 0) {
                     var2.a(this.t, 11);
                     if(this.s == 1352) {
                        var2.a(this.k);
                     }
                  }
               }
            }

            if(this.i == 22) {
               var2.a(this.j);
               if(this.j) {
                  var2.a(this.l, 4);
                  if(this.l == 15) {
                     var2.a(this.m, 24);
                  }
               }

               var2.a(this.n, 4);
            }
         }
      }

      return (ByteBuffer)var1.rewind();
   }

   private void f(int var1, int var2, int var3, c var4) throws IOException {
      this.T = var4.a(1);
      if(this.T == 1) {
         this.U = var4.a(2);
      }

   }

   int a() {
      byte var1;
      if(this.e > 30) {
         var1 = 11;
      } else {
         var1 = 5;
      }

      int var2 = var1 + 4;
      int var3 = var2;
      if(this.f == 15) {
         var3 = var2 + 24;
      }

      label61: {
         var2 = var3 + 4;
         if(this.d != 5) {
            var3 = var2;
            if(this.d != 29) {
               break label61;
            }
         }

         var2 += 4;
         var3 = var2;
         if(this.l == 15) {
            var3 = var2 + 24;
         }
      }

      var2 = var3;
      if(this.d == 22) {
         var2 = var3 + 4;
      }

      var3 = var2;
      if(this.G) {
         var3 = var2 + this.e();
      }

      var2 = var3;
      if(this.u >= 0) {
         var3 += 11;
         var2 = var3;
         if(this.u == 695) {
            var3 += 5;
            var2 = var3;
            if(this.i > 30) {
               var2 = var3 + 6;
            }

            var3 = var2;
            if(this.i == 5) {
               ++var2;
               var3 = var2;
               if(this.j) {
                  var3 = var2 + 4;
                  var2 = var3;
                  if(this.l == 15) {
                     var2 = var3 + 24;
                  }

                  var3 = var2;
                  if(this.t >= 0) {
                     var2 += 11;
                     var3 = var2;
                     if(this.t == 1352) {
                        var3 = var2 + 1;
                     }
                  }
               }
            }

            var2 = var3;
            if(this.i == 22) {
               var2 = var3 + 1;
               var3 = var2;
               if(this.j) {
                  var2 += 4;
                  var3 = var2;
                  if(this.l == 15) {
                     var3 = var2 + 24;
                  }
               }

               var2 = var3 + 4;
            }
         }
      }

      return (int)Math.ceil((double)var2 / 8.0D);
   }

   public void a(ByteBuffer var1) throws IOException {
      this.X = true;
      ByteBuffer var3 = var1.slice();
      var3.limit(this.Z);
      var1.position(var1.position() + this.Z);
      this.W = new byte[this.Z];
      var3.get(this.W);
      var3.rewind();
      c var4 = new c(var3);
      int var2 = this.a(var4);
      this.d = var2;
      this.e = var2;
      this.f = var4.a(4);
      if(this.f == 15) {
         this.g = var4.a(24);
      }

      this.h = var4.a(4);
      if(this.d != 5 && this.d != 29) {
         this.i = 0;
      } else {
         this.i = 5;
         this.j = true;
         if(this.d == 29) {
            this.k = true;
         }

         this.l = var4.a(4);
         if(this.l == 15) {
            this.m = var4.a(24);
         }

         this.d = this.a(var4);
         if(this.d == 22) {
            this.n = var4.a(4);
         }
      }

      switch(this.d) {
      case 1:
      case 2:
      case 3:
      case 4:
      case 6:
      case 7:
      case 17:
      case 19:
      case 20:
      case 21:
      case 22:
      case 23:
         this.a(this.f, this.h, this.d, var4);
      case 5:
      case 10:
      case 11:
      case 18:
      case 29:
      case 31:
      default:
         break;
      case 8:
         throw new UnsupportedOperationException("can't parse CelpSpecificConfig yet");
      case 9:
         throw new UnsupportedOperationException("can't parse HvxcSpecificConfig yet");
      case 12:
         throw new UnsupportedOperationException("can't parse TTSSpecificConfig yet");
      case 13:
      case 14:
      case 15:
      case 16:
         throw new UnsupportedOperationException("can't parse StructuredAudioSpecificConfig yet");
      case 24:
         throw new UnsupportedOperationException("can't parse ErrorResilientCelpSpecificConfig yet");
      case 25:
         throw new UnsupportedOperationException("can't parse ErrorResilientHvxcSpecificConfig yet");
      case 26:
      case 27:
         this.b(this.f, this.h, this.d, var4);
         break;
      case 28:
         throw new UnsupportedOperationException("can't parse SSCSpecificConfig yet");
      case 30:
         this.o = var4.a(1);
         throw new UnsupportedOperationException("can't parse SpatialSpecificConfig yet");
      case 32:
      case 33:
      case 34:
         throw new UnsupportedOperationException("can't parse MPEG_1_2_SpecificConfig yet");
      case 35:
         throw new UnsupportedOperationException("can't parse DSTSpecificConfig yet");
      case 36:
         this.p = var4.a(5);
         throw new UnsupportedOperationException("can't parse ALSSpecificConfig yet");
      case 37:
      case 38:
         throw new UnsupportedOperationException("can't parse SLSSpecificConfig yet");
      case 39:
         this.c = new a.a(this.h, var4);
         break;
      case 40:
      case 41:
         throw new UnsupportedOperationException("can't parse SymbolicMusicSpecificConfig yet");
      }

      switch(this.d) {
      case 17:
      case 19:
      case 20:
      case 21:
      case 22:
      case 23:
      case 24:
      case 25:
      case 26:
      case 27:
      case 39:
         this.q = var4.a(2);
         if(this.q == 2 || this.q == 3) {
            throw new UnsupportedOperationException("can't parse ErrorProtectionSpecificConfig yet");
         } else if(this.q == 3) {
            this.r = var4.a(1);
            if(this.r == 0) {
               throw new RuntimeException("not implemented");
            }
         }
      case 18:
      case 28:
      case 29:
      case 30:
      case 31:
      case 32:
      case 33:
      case 34:
      case 35:
      case 36:
      case 37:
      case 38:
      default:
         if(this.i != 5 && var4.b() >= 16) {
            var2 = var4.a(11);
            this.s = var2;
            this.u = var2;
            if(this.s == 695) {
               this.i = this.a(var4);
               if(this.i == 5) {
                  this.j = var4.a();
                  if(this.j) {
                     this.l = var4.a(4);
                     if(this.l == 15) {
                        this.m = var4.a(24);
                     }

                     if(var4.b() >= 12) {
                        var2 = var4.a(11);
                        this.s = var2;
                        this.t = var2;
                        if(this.s == 1352) {
                           this.k = var4.a();
                        }
                     }
                  }
               }

               if(this.i == 22) {
                  this.j = var4.a();
                  if(this.j) {
                     this.l = var4.a(4);
                     if(this.l == 15) {
                        this.m = var4.a(24);
                     }
                  }

                  this.n = var4.a(4);
               }
            }
         }

      }
   }

   public ByteBuffer b() {
      ByteBuffer var1 = ByteBuffer.allocate(this.d());
      com.coremedia.iso.g.c(var1, this.Y);
      this.a(var1, this.a());
      var1.put(this.f());
      return (ByteBuffer)var1.rewind();
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(this != var1) {
         if(var1 != null && this.getClass() == var1.getClass()) {
            a var3 = (a)var1;
            if(this.D != var3.D) {
               var2 = false;
            } else if(this.C != var3.C) {
               var2 = false;
            } else if(this.E != var3.E) {
               var2 = false;
            } else if(this.d != var3.d) {
               var2 = false;
            } else if(this.h != var3.h) {
               var2 = false;
            } else if(this.x != var3.x) {
               var2 = false;
            } else if(this.w != var3.w) {
               var2 = false;
            } else if(this.r != var3.r) {
               var2 = false;
            } else if(this.q != var3.q) {
               var2 = false;
            } else if(this.M != var3.M) {
               var2 = false;
            } else if(this.i != var3.i) {
               var2 = false;
            } else if(this.n != var3.n) {
               var2 = false;
            } else if(this.y != var3.y) {
               var2 = false;
            } else if(this.F != var3.F) {
               var2 = false;
            } else if(this.m != var3.m) {
               var2 = false;
            } else if(this.l != var3.l) {
               var2 = false;
            } else if(this.p != var3.p) {
               var2 = false;
            } else if(this.v != var3.v) {
               var2 = false;
            } else if(this.G != var3.G) {
               var2 = false;
            } else if(this.S != var3.S) {
               var2 = false;
            } else if(this.T != var3.T) {
               var2 = false;
            } else if(this.U != var3.U) {
               var2 = false;
            } else if(this.R != var3.R) {
               var2 = false;
            } else if(this.P != var3.P) {
               var2 = false;
            } else if(this.O != var3.O) {
               var2 = false;
            } else if(this.Q != var3.Q) {
               var2 = false;
            } else if(this.L != var3.L) {
               var2 = false;
            } else if(this.K != var3.K) {
               var2 = false;
            } else if(this.H != var3.H) {
               var2 = false;
            } else if(this.z != var3.z) {
               var2 = false;
            } else if(this.B != var3.B) {
               var2 = false;
            } else if(this.A != var3.A) {
               var2 = false;
            } else if(this.J != var3.J) {
               var2 = false;
            } else if(this.I != var3.I) {
               var2 = false;
            } else if(this.V != var3.V) {
               var2 = false;
            } else if(this.k != var3.k) {
               var2 = false;
            } else if(this.o != var3.o) {
               var2 = false;
            } else if(this.g != var3.g) {
               var2 = false;
            } else if(this.f != var3.f) {
               var2 = false;
            } else if(this.j != var3.j) {
               var2 = false;
            } else if(this.s != var3.s) {
               var2 = false;
            } else if(this.N != var3.N) {
               var2 = false;
            } else if(!Arrays.equals(this.W, var3.W)) {
               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public int hashCode() {
      byte var8 = 1;
      int var1;
      if(this.W != null) {
         var1 = Arrays.hashCode(this.W);
      } else {
         var1 = 0;
      }

      int var10 = this.d;
      int var12 = this.f;
      int var11 = this.g;
      int var9 = this.h;
      int var13 = this.i;
      byte var2;
      if(this.j) {
         var2 = 1;
      } else {
         var2 = 0;
      }

      byte var3;
      if(this.k) {
         var3 = 1;
      } else {
         var3 = 0;
      }

      int var24 = this.l;
      int var25 = this.m;
      int var28 = this.n;
      int var16 = this.o;
      int var17 = this.p;
      int var19 = this.q;
      int var27 = this.r;
      int var14 = this.s;
      int var26 = this.v;
      int var22 = this.w;
      int var18 = this.x;
      int var20 = this.y;
      int var15 = this.z;
      int var21 = this.A;
      int var23 = this.B;
      byte var4;
      if(this.C) {
         var4 = 1;
      } else {
         var4 = 0;
      }

      byte var5;
      if(this.D) {
         var5 = 1;
      } else {
         var5 = 0;
      }

      byte var6;
      if(this.E) {
         var6 = 1;
      } else {
         var6 = 0;
      }

      int var29 = this.F;
      byte var7;
      if(this.G) {
         var7 = 1;
      } else {
         var7 = 0;
      }

      int var42 = this.H;
      int var35 = this.I;
      int var30 = this.J;
      int var32 = this.K;
      int var40 = this.L;
      int var38 = this.M;
      int var41 = this.N;
      int var37 = this.O;
      int var43 = this.P;
      int var33 = this.Q;
      int var39 = this.R;
      int var31 = this.S;
      int var34 = this.T;
      int var36 = this.U;
      if(!this.V) {
         var8 = 0;
      }

      return (((((((((((((((var7 + ((var6 + (var5 + (var4 + ((((((((((((((((var3 + (var2 + (((((var1 * 31 + var10) * 31 + var12) * 31 + var11) * 31 + var9) * 31 + var13) * 31) * 31) * 31 + var24) * 31 + var25) * 31 + var28) * 31 + var16) * 31 + var17) * 31 + var19) * 31 + var27) * 31 + var14) * 31 + var26) * 31 + var22) * 31 + var18) * 31 + var20) * 31 + var15) * 31 + var21) * 31 + var23) * 31) * 31) * 31) * 31 + var29) * 31) * 31 + var42) * 31 + var35) * 31 + var30) * 31 + var32) * 31 + var40) * 31 + var38) * 31 + var41) * 31 + var37) * 31 + var43) * 31 + var33) * 31 + var39) * 31 + var31) * 31 + var34) * 31 + var36) * 31 + var8;
   }

   public String toString() {
      StringBuilder var1 = new StringBuilder();
      var1.append("AudioSpecificConfig");
      var1.append("{configBytes=").append(com.coremedia.iso.c.a(this.W));
      var1.append(", audioObjectType=").append(this.d).append(" (").append((String)b.get(Integer.valueOf(this.d))).append(")");
      var1.append(", samplingFrequencyIndex=").append(this.f).append(" (").append(a.get(Integer.valueOf(this.f))).append(")");
      var1.append(", samplingFrequency=").append(this.g);
      var1.append(", channelConfiguration=").append(this.h);
      if(this.i > 0) {
         var1.append(", extensionAudioObjectType=").append(this.i).append(" (").append((String)b.get(Integer.valueOf(this.i))).append(")");
         var1.append(", sbrPresentFlag=").append(this.j);
         var1.append(", psPresentFlag=").append(this.k);
         var1.append(", extensionSamplingFrequencyIndex=").append(this.l).append(" (").append(a.get(Integer.valueOf(this.l))).append(")");
         var1.append(", extensionSamplingFrequency=").append(this.m);
         var1.append(", extensionChannelConfiguration=").append(this.n);
      }

      var1.append(", syncExtensionType=").append(this.s);
      if(this.G) {
         var1.append(", frameLengthFlag=").append(this.v);
         var1.append(", dependsOnCoreCoder=").append(this.w);
         var1.append(", coreCoderDelay=").append(this.x);
         var1.append(", extensionFlag=").append(this.y);
         var1.append(", layerNr=").append(this.z);
         var1.append(", numOfSubFrame=").append(this.A);
         var1.append(", layer_length=").append(this.B);
         var1.append(", aacSectionDataResilienceFlag=").append(this.C);
         var1.append(", aacScalefactorDataResilienceFlag=").append(this.D);
         var1.append(", aacSpectralDataResilienceFlag=").append(this.E);
         var1.append(", extensionFlag3=").append(this.F);
      }

      if(this.V) {
         var1.append(", isBaseLayer=").append(this.H);
         var1.append(", paraMode=").append(this.I);
         var1.append(", paraExtensionFlag=").append(this.J);
         var1.append(", hvxcVarMode=").append(this.K);
         var1.append(", hvxcRateMode=").append(this.L);
         var1.append(", erHvxcExtensionFlag=").append(this.M);
         var1.append(", var_ScalableFlag=").append(this.N);
         var1.append(", hilnQuantMode=").append(this.O);
         var1.append(", hilnMaxNumLine=").append(this.P);
         var1.append(", hilnSampleRateCode=").append(this.Q);
         var1.append(", hilnFrameLength=").append(this.R);
         var1.append(", hilnContMode=").append(this.S);
         var1.append(", hilnEnhaLayer=").append(this.T);
         var1.append(", hilnEnhaQuantMode=").append(this.U);
      }

      var1.append('}');
      return var1.toString();
   }

   public class a {
      public boolean a;
      public boolean b;
      public boolean c;
      public boolean d;
      public boolean e;
      public boolean f;
      public boolean g;

      public a(int var2, c var3) {
         this.a = var3.a();
         this.b = var3.a();
         this.c = var3.a();
         this.d = var3.a();
         this.e = var3.a();
         if(this.e) {
            this.f = var3.a();
            this.g = var3.a();
            this.a(var2, var3);
         }

         while(var3.a(4) != 0) {
            var2 = var3.a(4);
            int var5;
            if(var2 == 15) {
               var5 = var3.a(8);
               var2 += var5;
            } else {
               var5 = 0;
            }

            int var4 = var2;
            if(var5 == 255) {
               var4 = var2 + var3.a(16);
            }

            for(var2 = 0; var2 < var4; ++var2) {
               var3.a(8);
            }
         }

      }

      public void a(int var1, c var2) {
         int var3 = 0;
         byte var4;
         switch(var1) {
         case 1:
         case 2:
            var4 = 1;
            break;
         case 3:
            var4 = 2;
            break;
         case 4:
         case 5:
         case 6:
            var4 = 3;
            break;
         case 7:
            var4 = 4;
            break;
         default:
            var4 = 0;
         }

         while(var3 < var4) {
            a.this.new b(var2);
            ++var3;
         }

      }
   }

   public class b {
      public boolean a;
      public int b;
      public int c;
      public int d;
      public int e;
      public boolean f;
      public boolean g;
      public int h;
      public boolean i;
      public int j;
      public int k;
      public int l;
      public boolean m;
      public boolean n;

      public b(c var2) {
         this.a = var2.a();
         this.b = var2.a(4);
         this.c = var2.a(4);
         this.d = var2.a(3);
         this.e = var2.a(2);
         this.f = var2.a();
         this.g = var2.a();
         if(this.f) {
            this.h = var2.a(2);
            this.i = var2.a();
            this.j = var2.a(2);
         }

         if(this.g) {
            this.k = var2.a(2);
            this.l = var2.a(2);
            this.m = var2.a();
         }

         this.n = var2.a();
      }
   }
}
