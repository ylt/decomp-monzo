package com.googlecode.mp4parser.boxes.mp4.a;

import java.io.IOException;
import java.nio.ByteBuffer;

@g(
   a = {0}
)
public abstract class b {
   // $FF: synthetic field
   static final boolean ab;
   int Y;
   int Z;
   int aa;

   static {
      boolean var0;
      if(!b.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      ab = var0;
   }

   abstract int a();

   public final void a(int var1, ByteBuffer var2) throws IOException {
      this.Y = var1;
      int var3 = com.coremedia.iso.e.d(var2);
      this.Z = var3 & 127;

      for(var1 = 1; var3 >>> 7 == 1; this.Z = this.Z << 7 | var3 & 127) {
         var3 = com.coremedia.iso.e.d(var2);
         ++var1;
      }

      this.aa = var1;
      ByteBuffer var4 = var2.slice();
      var4.limit(this.Z);
      this.a(var4);
      if(!ab && var4.remaining() != 0) {
         throw new AssertionError(this.getClass().getSimpleName() + " has not been fully parsed");
      } else {
         var2.position(var2.position() + this.Z);
      }
   }

   public abstract void a(ByteBuffer var1) throws IOException;

   public void a(ByteBuffer var1, int var2) {
      int var4 = var1.position();

      for(int var3 = 0; var2 > 0 || var3 < this.aa; var2 >>>= 7) {
         ++var3;
         if(var2 > 0) {
            var1.put(this.c() + var4 - var3, (byte)(var2 & 127));
         } else {
            var1.put(this.c() + var4 - var3, -128);
         }
      }

      var1.position(this.c() + var4);
   }

   public int c() {
      int var1 = this.a();

      int var2;
      for(var2 = 0; var1 > 0 || var2 < this.aa; ++var2) {
         var1 >>>= 7;
      }

      return var2;
   }

   public int d() {
      return this.a() + this.c() + 1;
   }

   public String toString() {
      StringBuilder var1 = new StringBuilder();
      var1.append("BaseDescriptor");
      var1.append("{tag=").append(this.Y);
      var1.append(", sizeOfInstance=").append(this.Z);
      var1.append('}');
      return var1.toString();
   }
}
