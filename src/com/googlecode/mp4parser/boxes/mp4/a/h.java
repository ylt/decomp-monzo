package com.googlecode.mp4parser.boxes.mp4.a;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@g(
   a = {3}
)
public class h extends b {
   private static Logger n = Logger.getLogger(h.class.getName());
   int a;
   int b;
   int c;
   int d;
   int e;
   int f = 0;
   String g;
   int h;
   int i;
   int j;
   e k;
   n l;
   List m = new ArrayList();

   public h() {
      this.Y = 3;
   }

   int a() {
      byte var2 = 3;
      if(this.b > 0) {
         var2 = 5;
      }

      int var1 = var2;
      if(this.c > 0) {
         var1 = var2 + this.f + 1;
      }

      int var4 = var1;
      if(this.d > 0) {
         var4 = var1 + 2;
      }

      var1 = this.k.d();
      int var3 = this.l.d();
      if(this.m.size() > 0) {
         throw new RuntimeException(" Doesn't handle other descriptors yet");
      } else {
         return var4 + var1 + var3;
      }
   }

   public void a(ByteBuffer var1) throws IOException {
      this.a = com.coremedia.iso.e.c(var1);
      int var2 = com.coremedia.iso.e.d(var1);
      this.b = var2 >>> 7;
      this.c = var2 >>> 6 & 1;
      this.d = var2 >>> 5 & 1;
      this.e = var2 & 31;
      if(this.b == 1) {
         this.i = com.coremedia.iso.e.c(var1);
      }

      if(this.c == 1) {
         this.f = com.coremedia.iso.e.d(var1);
         this.g = com.coremedia.iso.e.a(var1, this.f);
      }

      if(this.d == 1) {
         this.j = com.coremedia.iso.e.c(var1);
      }

      while(var1.remaining() > 1) {
         b var3 = l.a(-1, var1);
         if(var3 instanceof e) {
            this.k = (e)var3;
         } else if(var3 instanceof n) {
            this.l = (n)var3;
         } else {
            this.m.add(var3);
         }
      }

   }

   public ByteBuffer b() {
      ByteBuffer var3 = ByteBuffer.wrap(new byte[this.d()]);
      com.coremedia.iso.g.c(var3, 3);
      this.a(var3, this.a());
      com.coremedia.iso.g.b(var3, this.a);
      com.coremedia.iso.g.c(var3, this.b << 7 | this.c << 6 | this.d << 5 | this.e & 31);
      if(this.b > 0) {
         com.coremedia.iso.g.b(var3, this.i);
      }

      if(this.c > 0) {
         com.coremedia.iso.g.c(var3, this.f);
         com.coremedia.iso.g.c(var3, this.g);
      }

      if(this.d > 0) {
         com.coremedia.iso.g.b(var3, this.j);
      }

      ByteBuffer var1 = this.k.b();
      ByteBuffer var2 = this.l.b();
      var3.put(var1.array());
      var3.put(var2.array());
      return var3;
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(this != var1) {
         if(var1 != null && this.getClass() == var1.getClass()) {
            h var3 = (h)var1;
            if(this.c != var3.c) {
               var2 = false;
            } else if(this.f != var3.f) {
               var2 = false;
            } else if(this.i != var3.i) {
               var2 = false;
            } else if(this.a != var3.a) {
               var2 = false;
            } else if(this.j != var3.j) {
               var2 = false;
            } else if(this.d != var3.d) {
               var2 = false;
            } else if(this.h != var3.h) {
               var2 = false;
            } else if(this.b != var3.b) {
               var2 = false;
            } else if(this.e != var3.e) {
               var2 = false;
            } else {
               label99: {
                  if(this.g != null) {
                     if(!this.g.equals(var3.g)) {
                        break label99;
                     }
                  } else if(var3.g != null) {
                     break label99;
                  }

                  label100: {
                     if(this.k != null) {
                        if(this.k.equals(var3.k)) {
                           break label100;
                        }
                     } else if(var3.k == null) {
                        break label100;
                     }

                     var2 = false;
                     return var2;
                  }

                  label101: {
                     if(this.m != null) {
                        if(!this.m.equals(var3.m)) {
                           break label101;
                        }
                     } else if(var3.m != null) {
                        break label101;
                     }

                     if(this.l != null) {
                        if(this.l.equals(var3.l)) {
                           return var2;
                        }
                     } else if(var3.l == null) {
                        return var2;
                     }

                     var2 = false;
                     return var2;
                  }

                  var2 = false;
                  return var2;
               }

               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public int hashCode() {
      int var4 = 0;
      int var5 = this.a;
      int var9 = this.b;
      int var6 = this.c;
      int var8 = this.d;
      int var10 = this.e;
      int var7 = this.f;
      int var1;
      if(this.g != null) {
         var1 = this.g.hashCode();
      } else {
         var1 = 0;
      }

      int var12 = this.h;
      int var13 = this.i;
      int var11 = this.j;
      int var2;
      if(this.k != null) {
         var2 = this.k.hashCode();
      } else {
         var2 = 0;
      }

      int var3;
      if(this.l != null) {
         var3 = this.l.hashCode();
      } else {
         var3 = 0;
      }

      if(this.m != null) {
         var4 = this.m.hashCode();
      }

      return (var3 + (var2 + ((((var1 + (((((var5 * 31 + var9) * 31 + var6) * 31 + var8) * 31 + var10) * 31 + var7) * 31) * 31 + var12) * 31 + var13) * 31 + var11) * 31) * 31) * 31 + var4;
   }

   public String toString() {
      StringBuilder var1 = new StringBuilder();
      var1.append("ESDescriptor");
      var1.append("{esId=").append(this.a);
      var1.append(", streamDependenceFlag=").append(this.b);
      var1.append(", URLFlag=").append(this.c);
      var1.append(", oCRstreamFlag=").append(this.d);
      var1.append(", streamPriority=").append(this.e);
      var1.append(", URLLength=").append(this.f);
      var1.append(", URLString='").append(this.g).append('\'');
      var1.append(", remoteODFlag=").append(this.h);
      var1.append(", dependsOnEsId=").append(this.i);
      var1.append(", oCREsId=").append(this.j);
      var1.append(", decoderConfigDescriptor=").append(this.k);
      var1.append(", slConfigDescriptor=").append(this.l);
      var1.append('}');
      return var1.toString();
   }
}
