package com.googlecode.mp4parser.boxes.mp4.a;

import java.io.IOException;
import java.nio.ByteBuffer;

@g(
   a = {6}
)
public class n extends b {
   int a;

   public n() {
      this.Y = 6;
   }

   int a() {
      return 1;
   }

   public void a(ByteBuffer var1) throws IOException {
      this.a = com.coremedia.iso.e.d(var1);
   }

   public ByteBuffer b() {
      ByteBuffer var1 = ByteBuffer.allocate(this.d());
      com.coremedia.iso.g.c(var1, 6);
      this.a(var1, this.a());
      com.coremedia.iso.g.c(var1, this.a);
      return var1;
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(this != var1) {
         if(var1 != null && this.getClass() == var1.getClass()) {
            n var3 = (n)var1;
            if(this.a != var3.a) {
               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public int hashCode() {
      return this.a;
   }

   public String toString() {
      StringBuilder var1 = new StringBuilder();
      var1.append("SLConfigDescriptor");
      var1.append("{predefined=").append(this.a);
      var1.append('}');
      return var1.toString();
   }
}
