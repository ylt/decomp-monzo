package com.googlecode.mp4parser.boxes.mp4.a;

import java.io.IOException;
import java.nio.ByteBuffer;

@g(
   a = {19}
)
public class j extends b {
   byte[] a;

   public j() {
      this.Y = 19;
   }

   int a() {
      throw new RuntimeException("Not Implemented");
   }

   public void a(ByteBuffer var1) throws IOException {
      if(this.d() > 0) {
         this.a = new byte[this.d()];
         var1.get(this.a);
      }

   }

   public String toString() {
      StringBuilder var2 = new StringBuilder();
      var2.append("ExtensionDescriptor");
      StringBuilder var3 = var2.append("{bytes=");
      String var1;
      if(this.a == null) {
         var1 = "null";
      } else {
         var1 = com.coremedia.iso.c.a(this.a);
      }

      var3.append(var1);
      var2.append('}');
      return var2.toString();
   }
}
