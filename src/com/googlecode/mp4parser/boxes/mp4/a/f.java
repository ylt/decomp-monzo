package com.googlecode.mp4parser.boxes.mp4.a;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Arrays;

@g(
   a = {5}
)
public class f extends b {
   byte[] a;

   public f() {
      this.Y = 5;
   }

   int a() {
      return this.a.length;
   }

   public void a(ByteBuffer var1) throws IOException {
      this.a = new byte[var1.remaining()];
      var1.get(this.a);
   }

   public ByteBuffer b() {
      ByteBuffer var1 = ByteBuffer.allocate(this.d());
      com.coremedia.iso.g.c(var1, this.Y);
      this.a(var1, this.a());
      var1.put(this.a);
      return (ByteBuffer)var1.rewind();
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(this != var1) {
         if(var1 != null && this.getClass() == var1.getClass()) {
            f var3 = (f)var1;
            if(!Arrays.equals(this.a, var3.a)) {
               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public int hashCode() {
      int var1;
      if(this.a != null) {
         var1 = Arrays.hashCode(this.a);
      } else {
         var1 = 0;
      }

      return var1;
   }

   public String toString() {
      StringBuilder var3 = new StringBuilder();
      var3.append("DecoderSpecificInfo");
      StringBuilder var2 = var3.append("{bytes=");
      String var1;
      if(this.a == null) {
         var1 = "null";
      } else {
         var1 = com.coremedia.iso.c.a(this.a);
      }

      var2.append(var1);
      var3.append('}');
      return var3.toString();
   }
}
