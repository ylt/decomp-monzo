package com.googlecode.mp4parser.boxes.mp4.a;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

@g(
   a = {4}
)
public class e extends b {
   private static Logger k = Logger.getLogger(e.class.getName());
   int a;
   int b;
   int c;
   int d;
   long e;
   long f;
   f g;
   a h;
   List i = new ArrayList();
   byte[] j;

   public e() {
      this.Y = 4;
   }

   int a() {
      int var2 = 0;
      int var1;
      if(this.h == null) {
         var1 = 0;
      } else {
         var1 = this.h.d();
      }

      if(this.g != null) {
         var2 = this.g.d();
      }

      Iterator var3 = this.i.iterator();

      for(var1 = var1 + 13 + var2; var3.hasNext(); var1 += ((m)var3.next()).d()) {
         ;
      }

      return var1;
   }

   public void a(ByteBuffer var1) throws IOException {
      this.a = com.coremedia.iso.e.d(var1);
      int var2 = com.coremedia.iso.e.d(var1);
      this.b = var2 >>> 2;
      this.c = var2 >> 1 & 1;
      this.d = com.coremedia.iso.e.b(var1);
      this.e = com.coremedia.iso.e.a(var1);
      this.f = com.coremedia.iso.e.a(var1);

      while(var1.remaining() > 2) {
         var2 = var1.position();
         b var6 = l.a(this.a, var1);
         int var3 = var1.position() - var2;
         Logger var7 = k;
         StringBuilder var5 = (new StringBuilder()).append(var6).append(" - DecoderConfigDescr1 read: ").append(var3).append(", size: ");
         Integer var4;
         if(var6 != null) {
            var4 = Integer.valueOf(var6.d());
         } else {
            var4 = null;
         }

         var7.finer(var5.append(var4).toString());
         if(var6 != null) {
            var2 = var6.d();
            if(var3 < var2) {
               this.j = new byte[var2 - var3];
               var1.get(this.j);
            }
         }

         if(var6 instanceof f) {
            this.g = (f)var6;
         } else if(var6 instanceof a) {
            this.h = (a)var6;
         } else if(var6 instanceof m) {
            this.i.add((m)var6);
         }
      }

   }

   public ByteBuffer b() {
      ByteBuffer var2 = ByteBuffer.allocate(this.d());
      com.coremedia.iso.g.c(var2, this.Y);
      this.a(var2, this.a());
      com.coremedia.iso.g.c(var2, this.a);
      com.coremedia.iso.g.c(var2, this.b << 2 | this.c << 1 | 1);
      com.coremedia.iso.g.a(var2, this.d);
      com.coremedia.iso.g.b(var2, this.e);
      com.coremedia.iso.g.b(var2, this.f);
      if(this.g != null) {
         var2.put(this.g.b());
      }

      if(this.h != null) {
         var2.put(this.h.b());
      }

      Iterator var1 = this.i.iterator();

      while(var1.hasNext()) {
         var2.put(((m)var1.next()).b());
      }

      return (ByteBuffer)var2.rewind();
   }

   public String toString() {
      StringBuilder var2 = new StringBuilder();
      var2.append("DecoderConfigDescriptor");
      var2.append("{objectTypeIndication=").append(this.a);
      var2.append(", streamType=").append(this.b);
      var2.append(", upStream=").append(this.c);
      var2.append(", bufferSizeDB=").append(this.d);
      var2.append(", maxBitRate=").append(this.e);
      var2.append(", avgBitRate=").append(this.f);
      var2.append(", decoderSpecificInfo=").append(this.g);
      var2.append(", audioSpecificInfo=").append(this.h);
      StringBuilder var3 = var2.append(", configDescriptorDeadBytes=");
      byte[] var1;
      if(this.j != null) {
         var1 = this.j;
      } else {
         var1 = new byte[0];
      }

      var3.append(com.coremedia.iso.c.a(var1));
      var3 = var2.append(", profileLevelIndicationDescriptors=");
      String var4;
      if(this.i == null) {
         var4 = "null";
      } else {
         var4 = Arrays.asList(new List[]{this.i}).toString();
      }

      var3.append(var4);
      var2.append('}');
      return var2.toString();
   }
}
