package com.googlecode.mp4parser.boxes.mp4.a;

import java.nio.ByteBuffer;

public class d {
   // $FF: synthetic field
   static final boolean c;
   int a;
   int b = 0;
   private ByteBuffer d;

   static {
      boolean var0;
      if(!d.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      c = var0;
   }

   public d(ByteBuffer var1) {
      this.d = var1;
      this.a = var1.position();
   }

   public void a(int var1, int var2) {
      if(!c && var1 > (1 << var2) - 1) {
         throw new AssertionError(String.format("Trying to write a value bigger (%s) than the number bits (%s) allows. Please mask the value before writing it and make your code is really working as intended.", new Object[]{Integer.valueOf(var1), Integer.valueOf((1 << var2) - 1)}));
      } else {
         int var5 = 8 - this.b % 8;
         int var3;
         ByteBuffer var6;
         if(var2 <= var5) {
            byte var4 = this.d.get(this.a + this.b / 8);
            var3 = var4;
            if(var4 < 0) {
               var3 = var4 + 256;
            }

            var3 += var1 << var5 - var2;
            var6 = this.d;
            var5 = this.a;
            int var8 = this.b / 8;
            var1 = var3;
            if(var3 > 127) {
               var1 = var3 - 256;
            }

            var6.put(var5 + var8, (byte)var1);
            this.b += var2;
         } else {
            var2 -= var5;
            this.a(var1 >> var2, var5);
            this.a((1 << var2) - 1 & var1, var2);
         }

         var6 = this.d;
         var2 = this.a;
         var3 = this.b / 8;
         byte var7;
         if(this.b % 8 > 0) {
            var7 = 1;
         } else {
            var7 = 0;
         }

         var6.position(var7 + var3 + var2);
      }
   }

   public void a(boolean var1) {
      byte var2;
      if(var1) {
         var2 = 1;
      } else {
         var2 = 0;
      }

      this.a(var2, 1);
   }
}
