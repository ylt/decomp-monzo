package com.googlecode.mp4parser.boxes.mp4.a;

import java.nio.ByteBuffer;

public class c {
   int a;
   int b;
   private ByteBuffer c;

   public c(ByteBuffer var1) {
      this.c = var1;
      this.a = var1.position();
   }

   public int a(int var1) {
      byte var3 = this.c.get(this.a + this.b / 8);
      int var2 = var3;
      if(var3 < 0) {
         var2 = var3 + 256;
      }

      int var4 = 8 - this.b % 8;
      if(var1 <= var4) {
         var2 = (var2 << this.b % 8 & 255) >> var4 - var1 + this.b % 8;
         this.b += var1;
         var1 = var2;
      } else {
         var1 -= var4;
         var2 = this.a(var4);
         var1 = this.a(var1) + (var2 << var1);
      }

      this.c.position(this.a + (int)Math.ceil((double)this.b / 8.0D));
      return var1;
   }

   public boolean a() {
      boolean var1 = true;
      if(this.a(1) != 1) {
         var1 = false;
      }

      return var1;
   }

   public int b() {
      return this.c.limit() * 8 - this.b;
   }
}
