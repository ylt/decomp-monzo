package com.googlecode.mp4parser.boxes.mp4.samplegrouping;

import java.nio.ByteBuffer;

public abstract class b {
   public abstract String a();

   public abstract void a(ByteBuffer var1);

   public abstract ByteBuffer b();

   public int c() {
      return this.b().limit();
   }
}
