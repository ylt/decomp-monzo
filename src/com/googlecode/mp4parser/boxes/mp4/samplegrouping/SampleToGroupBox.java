package com.googlecode.mp4parser.boxes.mp4.samplegrouping;

import com.googlecode.mp4parser.AbstractFullBox;
import java.nio.ByteBuffer;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class SampleToGroupBox extends AbstractFullBox {
   public static final String TYPE = "sbgp";
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_0;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_1;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_2;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_3;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_4;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_5;
   List entries = new LinkedList();
   private String groupingType;
   private String groupingTypeParameter;

   static {
      ajc$preClinit();
   }

   public SampleToGroupBox() {
      super("sbgp");
   }

   private static void ajc$preClinit() {
      org.mp4parser.aspectj.a.b.b var0 = new org.mp4parser.aspectj.a.b.b("SampleToGroupBox.java", SampleToGroupBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getGroupingType", "com.googlecode.mp4parser.boxes.mp4.samplegrouping.SampleToGroupBox", "", "", "", "java.lang.String"), 150);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "setGroupingType", "com.googlecode.mp4parser.boxes.mp4.samplegrouping.SampleToGroupBox", "java.lang.String", "groupingType", "", "void"), 154);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "getGroupingTypeParameter", "com.googlecode.mp4parser.boxes.mp4.samplegrouping.SampleToGroupBox", "", "", "", "java.lang.String"), 158);
      ajc$tjp_3 = var0.a("method-execution", var0.a("1", "setGroupingTypeParameter", "com.googlecode.mp4parser.boxes.mp4.samplegrouping.SampleToGroupBox", "java.lang.String", "groupingTypeParameter", "", "void"), 162);
      ajc$tjp_4 = var0.a("method-execution", var0.a("1", "getEntries", "com.googlecode.mp4parser.boxes.mp4.samplegrouping.SampleToGroupBox", "", "", "", "java.util.List"), 166);
      ajc$tjp_5 = var0.a("method-execution", var0.a("1", "setEntries", "com.googlecode.mp4parser.boxes.mp4.samplegrouping.SampleToGroupBox", "java.util.List", "entries", "", "void"), 170);
   }

   protected void _parseDetails(ByteBuffer var1) {
      this.parseVersionAndFlags(var1);
      this.groupingType = com.coremedia.iso.e.k(var1);
      if(this.getVersion() == 1) {
         this.groupingTypeParameter = com.coremedia.iso.e.k(var1);
      }

      for(long var2 = com.coremedia.iso.e.a(var1); var2 > 0L; --var2) {
         this.entries.add(new SampleToGroupBox.a((long)com.googlecode.mp4parser.c.b.a(com.coremedia.iso.e.a(var1)), com.googlecode.mp4parser.c.b.a(com.coremedia.iso.e.a(var1))));
      }

   }

   protected void getContent(ByteBuffer var1) {
      this.writeVersionAndFlags(var1);
      var1.put(this.groupingType.getBytes());
      if(this.getVersion() == 1) {
         var1.put(this.groupingTypeParameter.getBytes());
      }

      com.coremedia.iso.g.b(var1, (long)this.entries.size());
      Iterator var2 = this.entries.iterator();

      while(var2.hasNext()) {
         SampleToGroupBox.a var3 = (SampleToGroupBox.a)var2.next();
         com.coremedia.iso.g.b(var1, var3.a());
         com.coremedia.iso.g.b(var1, (long)var3.b());
      }

   }

   protected long getContentSize() {
      int var1;
      if(this.getVersion() == 1) {
         var1 = this.entries.size() * 8 + 16;
      } else {
         var1 = this.entries.size() * 8 + 12;
      }

      return (long)var1;
   }

   public List getEntries() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_4, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.entries;
   }

   public String getGroupingType() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.groupingType;
   }

   public String getGroupingTypeParameter() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_2, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.groupingTypeParameter;
   }

   public void setEntries(List var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_5, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.entries = var1;
   }

   public void setGroupingType(String var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_1, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.groupingType = var1;
   }

   public void setGroupingTypeParameter(String var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_3, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.groupingTypeParameter = var1;
   }

   public static class a {
      private long a;
      private int b;

      public a(long var1, int var3) {
         this.a = var1;
         this.b = var3;
      }

      public long a() {
         return this.a;
      }

      public void a(long var1) {
         this.a = var1;
      }

      public int b() {
         return this.b;
      }

      public boolean equals(Object var1) {
         boolean var2 = true;
         if(this != var1) {
            if(var1 != null && this.getClass() == var1.getClass()) {
               SampleToGroupBox.a var3 = (SampleToGroupBox.a)var1;
               if(this.b != var3.b) {
                  var2 = false;
               } else if(this.a != var3.a) {
                  var2 = false;
               }
            } else {
               var2 = false;
            }
         }

         return var2;
      }

      public int hashCode() {
         return (int)(this.a ^ this.a >>> 32) * 31 + this.b;
      }

      public String toString() {
         return "Entry{sampleCount=" + this.a + ", groupDescriptionIndex=" + this.b + '}';
      }
   }
}
