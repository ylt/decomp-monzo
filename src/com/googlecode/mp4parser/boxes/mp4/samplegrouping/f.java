package com.googlecode.mp4parser.boxes.mp4.samplegrouping;

import java.nio.ByteBuffer;

public class f extends b {
   private ByteBuffer a;
   private String b;

   public f(String var1) {
      this.b = var1;
   }

   public String a() {
      return this.b;
   }

   public void a(ByteBuffer var1) {
      this.a = (ByteBuffer)var1.duplicate().rewind();
   }

   public ByteBuffer b() {
      return this.a.duplicate();
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(this != var1) {
         if(var1 != null && this.getClass() == var1.getClass()) {
            f var3 = (f)var1;
            if(this.a != null) {
               if(this.a.equals(var3.a)) {
                  return var2;
               }
            } else if(var3.a == null) {
               return var2;
            }

            var2 = false;
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public int hashCode() {
      int var1;
      if(this.a != null) {
         var1 = this.a.hashCode();
      } else {
         var1 = 0;
      }

      return var1;
   }

   public String toString() {
      ByteBuffer var1 = this.a.duplicate();
      var1.rewind();
      byte[] var2 = new byte[var1.limit()];
      var1.get(var2);
      return "UnknownEntry{content=" + com.coremedia.iso.c.a(var2) + '}';
   }
}
