package com.googlecode.mp4parser.boxes.mp4.samplegrouping;

import com.googlecode.mp4parser.AbstractFullBox;
import java.nio.ByteBuffer;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class SampleGroupDescriptionBox extends AbstractFullBox {
   public static final String TYPE = "sgpd";
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_0;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_1;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_2;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_3;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_4;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_5;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_6;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_7;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_8;
   private int defaultLength;
   private List groupEntries = new LinkedList();
   private String groupingType;

   static {
      ajc$preClinit();
   }

   public SampleGroupDescriptionBox() {
      super("sgpd");
      this.setVersion(1);
   }

   private static void ajc$preClinit() {
      org.mp4parser.aspectj.a.b.b var0 = new org.mp4parser.aspectj.a.b.b("SampleGroupDescriptionBox.java", SampleGroupDescriptionBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getGroupingType", "com.googlecode.mp4parser.boxes.mp4.samplegrouping.SampleGroupDescriptionBox", "", "", "", "java.lang.String"), 57);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "setGroupingType", "com.googlecode.mp4parser.boxes.mp4.samplegrouping.SampleGroupDescriptionBox", "java.lang.String", "groupingType", "", "void"), 61);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "getDefaultLength", "com.googlecode.mp4parser.boxes.mp4.samplegrouping.SampleGroupDescriptionBox", "", "", "", "int"), 153);
      ajc$tjp_3 = var0.a("method-execution", var0.a("1", "setDefaultLength", "com.googlecode.mp4parser.boxes.mp4.samplegrouping.SampleGroupDescriptionBox", "int", "defaultLength", "", "void"), 157);
      ajc$tjp_4 = var0.a("method-execution", var0.a("1", "getGroupEntries", "com.googlecode.mp4parser.boxes.mp4.samplegrouping.SampleGroupDescriptionBox", "", "", "", "java.util.List"), 161);
      ajc$tjp_5 = var0.a("method-execution", var0.a("1", "setGroupEntries", "com.googlecode.mp4parser.boxes.mp4.samplegrouping.SampleGroupDescriptionBox", "java.util.List", "groupEntries", "", "void"), 165);
      ajc$tjp_6 = var0.a("method-execution", var0.a("1", "equals", "com.googlecode.mp4parser.boxes.mp4.samplegrouping.SampleGroupDescriptionBox", "java.lang.Object", "o", "", "boolean"), 170);
      ajc$tjp_7 = var0.a("method-execution", var0.a("1", "hashCode", "com.googlecode.mp4parser.boxes.mp4.samplegrouping.SampleGroupDescriptionBox", "", "", "", "int"), 191);
      ajc$tjp_8 = var0.a("method-execution", var0.a("1", "toString", "com.googlecode.mp4parser.boxes.mp4.samplegrouping.SampleGroupDescriptionBox", "", "", "", "java.lang.String"), 199);
   }

   private b parseGroupEntry(ByteBuffer var1, String var2) {
      Object var3;
      if("roll".equals(var2)) {
         var3 = new d();
      } else if("rash".equals(var2)) {
         var3 = new c();
      } else if("seig".equals(var2)) {
         var3 = new a();
      } else if("rap ".equals(var2)) {
         var3 = new g();
      } else if("tele".equals(var2)) {
         var3 = new e();
      } else if("sync".equals(var2)) {
         var3 = new com.mp4parser.iso14496.part15.d();
      } else if("tscl".equals(var2)) {
         var3 = new com.mp4parser.iso14496.part15.e();
      } else if("tsas".equals(var2)) {
         var3 = new com.mp4parser.iso14496.part15.f();
      } else if("stsa".equals(var2)) {
         var3 = new com.mp4parser.iso14496.part15.c();
      } else {
         var3 = new f(var2);
      }

      ((b)var3).a(var1);
      return (b)var3;
   }

   protected void _parseDetails(ByteBuffer var1) {
      this.parseVersionAndFlags(var1);
      if(this.getVersion() != 1) {
         throw new RuntimeException("SampleGroupDescriptionBox are only supported in version 1");
      } else {
         this.groupingType = com.coremedia.iso.e.k(var1);
         if(this.getVersion() == 1) {
            this.defaultLength = com.googlecode.mp4parser.c.b.a(com.coremedia.iso.e.a(var1));
         }

         for(long var4 = com.coremedia.iso.e.a(var1); var4 > 0L; --var4) {
            int var2 = this.defaultLength;
            if(this.getVersion() != 1) {
               throw new RuntimeException("This should be implemented");
            }

            if(this.defaultLength == 0) {
               var2 = com.googlecode.mp4parser.c.b.a(com.coremedia.iso.e.a(var1));
            }

            int var3 = var1.position();
            ByteBuffer var6 = var1.slice();
            var6.limit(var2);
            this.groupEntries.add(this.parseGroupEntry(var6, this.groupingType));
            var1.position(var3 + var2);
         }

      }
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      org.mp4parser.aspectj.lang.a var3 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_6, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var3);
      if(this != var1) {
         if(var1 != null && this.getClass() == var1.getClass()) {
            SampleGroupDescriptionBox var4 = (SampleGroupDescriptionBox)var1;
            if(this.defaultLength != var4.defaultLength) {
               var2 = false;
            } else {
               if(this.groupEntries != null) {
                  if(this.groupEntries.equals(var4.groupEntries)) {
                     return var2;
                  }
               } else if(var4.groupEntries == null) {
                  return var2;
               }

               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   protected void getContent(ByteBuffer var1) {
      this.writeVersionAndFlags(var1);
      var1.put(com.coremedia.iso.d.a(this.groupingType));
      if(this.getVersion() == 1) {
         com.coremedia.iso.g.b(var1, (long)this.defaultLength);
      }

      com.coremedia.iso.g.b(var1, (long)this.groupEntries.size());

      b var3;
      for(Iterator var2 = this.groupEntries.iterator(); var2.hasNext(); var1.put(var3.b())) {
         var3 = (b)var2.next();
         if(this.getVersion() == 1 && this.defaultLength == 0) {
            com.coremedia.iso.g.b(var1, (long)var3.b().limit());
         }
      }

   }

   protected long getContentSize() {
      long var1 = 8L;
      if(this.getVersion() == 1) {
         var1 = 8L + 4L;
      }

      Iterator var5 = this.groupEntries.iterator();

      long var3;
      b var6;
      for(var1 += 4L; var5.hasNext(); var1 = (long)var6.c() + var3) {
         var6 = (b)var5.next();
         var3 = var1;
         if(this.getVersion() == 1) {
            var3 = var1;
            if(this.defaultLength == 0) {
               var3 = var1 + 4L;
            }
         }
      }

      return var1;
   }

   public int getDefaultLength() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_2, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.defaultLength;
   }

   public List getGroupEntries() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_4, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.groupEntries;
   }

   public String getGroupingType() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.groupingType;
   }

   public int hashCode() {
      org.mp4parser.aspectj.lang.a var3 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_7, this, this);
      com.googlecode.mp4parser.e.a().a(var3);
      int var2 = this.defaultLength;
      int var1;
      if(this.groupEntries != null) {
         var1 = this.groupEntries.hashCode();
      } else {
         var1 = 0;
      }

      return var1 + (var2 + 0) * 31;
   }

   public void setDefaultLength(int var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_3, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.defaultLength = var1;
   }

   public void setGroupEntries(List var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_5, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.groupEntries = var1;
   }

   public void setGroupingType(String var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_1, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.groupingType = var1;
   }

   public String toString() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_8, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      StringBuilder var2 = new StringBuilder("SampleGroupDescriptionBox{groupingType='");
      String var3;
      if(this.groupEntries.size() > 0) {
         var3 = ((b)this.groupEntries.get(0)).a();
      } else {
         var3 = "????";
      }

      return var2.append(var3).append('\'').append(", defaultLength=").append(this.defaultLength).append(", groupEntries=").append(this.groupEntries).append('}').toString();
   }
}
