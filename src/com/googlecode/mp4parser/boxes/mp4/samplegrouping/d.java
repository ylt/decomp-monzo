package com.googlecode.mp4parser.boxes.mp4.samplegrouping;

import java.nio.ByteBuffer;

public class d extends b {
   private short a;

   public String a() {
      return "roll";
   }

   public void a(ByteBuffer var1) {
      this.a = var1.getShort();
   }

   public ByteBuffer b() {
      ByteBuffer var1 = ByteBuffer.allocate(2);
      var1.putShort(this.a);
      var1.rewind();
      return var1;
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(this != var1) {
         if(var1 != null && this.getClass() == var1.getClass()) {
            d var3 = (d)var1;
            if(this.a != var3.a) {
               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public int hashCode() {
      return this.a;
   }
}
