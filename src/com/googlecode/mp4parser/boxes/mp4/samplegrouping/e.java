package com.googlecode.mp4parser.boxes.mp4.samplegrouping;

import java.nio.ByteBuffer;

public class e extends b {
   private boolean a;
   private short b;

   public String a() {
      return "tele";
   }

   public void a(ByteBuffer var1) {
      boolean var2;
      if((var1.get() & 128) == 128) {
         var2 = true;
      } else {
         var2 = false;
      }

      this.a = var2;
   }

   public ByteBuffer b() {
      ByteBuffer var2 = ByteBuffer.allocate(1);
      short var1;
      if(this.a) {
         var1 = 128;
      } else {
         var1 = 0;
      }

      var2.put((byte)var1);
      var2.rewind();
      return var2;
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(this != var1) {
         if(var1 != null && this.getClass() == var1.getClass()) {
            e var3 = (e)var1;
            if(this.a != var3.a) {
               var2 = false;
            } else if(this.b != var3.b) {
               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public int hashCode() {
      byte var1;
      if(this.a) {
         var1 = 1;
      } else {
         var1 = 0;
      }

      return var1 * 31 + this.b;
   }

   public String toString() {
      StringBuilder var1 = new StringBuilder();
      var1.append("TemporalLevelEntry");
      var1.append("{levelIndependentlyDecodable=").append(this.a);
      var1.append('}');
      return var1.toString();
   }
}
