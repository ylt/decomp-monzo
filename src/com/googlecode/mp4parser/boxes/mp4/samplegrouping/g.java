package com.googlecode.mp4parser.boxes.mp4.samplegrouping;

import java.nio.ByteBuffer;

public class g extends b {
   private boolean a;
   private short b;

   public String a() {
      return "rap ";
   }

   public void a(ByteBuffer var1) {
      byte var2 = var1.get();
      boolean var3;
      if((var2 & 128) == 128) {
         var3 = true;
      } else {
         var3 = false;
      }

      this.a = var3;
      this.b = (short)(var2 & 127);
   }

   public ByteBuffer b() {
      ByteBuffer var2 = ByteBuffer.allocate(1);
      short var1;
      if(this.a) {
         var1 = 128;
      } else {
         var1 = 0;
      }

      var2.put((byte)(var1 | this.b & 127));
      var2.rewind();
      return var2;
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(this != var1) {
         if(var1 != null && this.getClass() == var1.getClass()) {
            g var3 = (g)var1;
            if(this.b != var3.b) {
               var2 = false;
            } else if(this.a != var3.a) {
               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public int hashCode() {
      byte var1;
      if(this.a) {
         var1 = 1;
      } else {
         var1 = 0;
      }

      return var1 * 31 + this.b;
   }

   public String toString() {
      StringBuilder var1 = new StringBuilder();
      var1.append("VisualRandomAccessEntry");
      var1.append("{numLeadingSamplesKnown=").append(this.a);
      var1.append(", numLeadingSamples=").append(this.b);
      var1.append('}');
      return var1.toString();
   }
}
