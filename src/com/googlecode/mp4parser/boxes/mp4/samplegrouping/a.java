package com.googlecode.mp4parser.boxes.mp4.samplegrouping;

import com.googlecode.mp4parser.c.k;
import java.nio.ByteBuffer;
import java.util.UUID;

public class a extends b {
   private boolean a;
   private byte b;
   private UUID c;

   public String a() {
      return "seig";
   }

   public void a(ByteBuffer var1) {
      boolean var2 = true;
      if(com.coremedia.iso.e.b(var1) != 1) {
         var2 = false;
      }

      this.a = var2;
      this.b = (byte)com.coremedia.iso.e.d(var1);
      byte[] var3 = new byte[16];
      var1.get(var3);
      this.c = k.a(var3);
   }

   public ByteBuffer b() {
      ByteBuffer var2 = ByteBuffer.allocate(20);
      byte var1;
      if(this.a) {
         var1 = 1;
      } else {
         var1 = 0;
      }

      com.coremedia.iso.g.a(var2, var1);
      if(this.a) {
         com.coremedia.iso.g.c(var2, this.b);
         var2.put(k.a(this.c));
      } else {
         var2.put(new byte[17]);
      }

      var2.rewind();
      return var2;
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(this != var1) {
         if(var1 != null && this.getClass() == var1.getClass()) {
            a var3 = (a)var1;
            if(this.a != var3.a) {
               var2 = false;
            } else if(this.b != var3.b) {
               var2 = false;
            } else {
               if(this.c != null) {
                  if(this.c.equals(var3.c)) {
                     return var2;
                  }
               } else if(var3.c == null) {
                  return var2;
               }

               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public int hashCode() {
      byte var1;
      if(this.a) {
         var1 = 7;
      } else {
         var1 = 19;
      }

      byte var3 = this.b;
      int var2;
      if(this.c != null) {
         var2 = this.c.hashCode();
      } else {
         var2 = 0;
      }

      return var2 + (var1 * 31 + var3) * 31;
   }

   public String toString() {
      return "CencSampleEncryptionInformationGroupEntry{isEncrypted=" + this.a + ", ivSize=" + this.b + ", kid=" + this.c + '}';
   }
}
