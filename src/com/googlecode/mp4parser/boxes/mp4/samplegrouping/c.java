package com.googlecode.mp4parser.boxes.mp4.samplegrouping;

import java.nio.ByteBuffer;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class c extends b {
   private short a;
   private short b;
   private List c = new LinkedList();
   private int d;
   private int e;
   private short f;

   public String a() {
      return "rash";
   }

   public void a(ByteBuffer var1) {
      this.a = var1.getShort();
      if(this.a == 1) {
         this.b = var1.getShort();
      } else {
         for(int var2 = this.a; var2 > 0; --var2) {
            this.c.add(new c.a(com.googlecode.mp4parser.c.b.a(com.coremedia.iso.e.a(var1)), var1.getShort()));
         }
      }

      this.d = com.googlecode.mp4parser.c.b.a(com.coremedia.iso.e.a(var1));
      this.e = com.googlecode.mp4parser.c.b.a(com.coremedia.iso.e.a(var1));
      this.f = (short)com.coremedia.iso.e.d(var1);
   }

   public ByteBuffer b() {
      int var1;
      if(this.a == 1) {
         var1 = 13;
      } else {
         var1 = this.a * 6 + 11;
      }

      ByteBuffer var3 = ByteBuffer.allocate(var1);
      var3.putShort(this.a);
      if(this.a == 1) {
         var3.putShort(this.b);
      } else {
         Iterator var4 = this.c.iterator();

         while(var4.hasNext()) {
            c.a var2 = (c.a)var4.next();
            var3.putInt(var2.a());
            var3.putShort(var2.b());
         }
      }

      var3.putInt(this.d);
      var3.putInt(this.e);
      com.coremedia.iso.g.c(var3, this.f);
      var3.rewind();
      return var3;
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(this != var1) {
         if(var1 != null && this.getClass() == var1.getClass()) {
            c var3 = (c)var1;
            if(this.f != var3.f) {
               var2 = false;
            } else if(this.d != var3.d) {
               var2 = false;
            } else if(this.e != var3.e) {
               var2 = false;
            } else if(this.a != var3.a) {
               var2 = false;
            } else if(this.b != var3.b) {
               var2 = false;
            } else {
               if(this.c != null) {
                  if(this.c.equals(var3.c)) {
                     return var2;
                  }
               } else if(var3.c == null) {
                  return var2;
               }

               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public int hashCode() {
      short var3 = this.a;
      short var2 = this.b;
      int var1;
      if(this.c != null) {
         var1 = this.c.hashCode();
      } else {
         var1 = 0;
      }

      return (((var1 + (var3 * 31 + var2) * 31) * 31 + this.d) * 31 + this.e) * 31 + this.f;
   }

   public static class a {
      int a;
      short b;

      public a(int var1, short var2) {
         this.a = var1;
         this.b = var2;
      }

      public int a() {
         return this.a;
      }

      public short b() {
         return this.b;
      }

      public boolean equals(Object var1) {
         boolean var2 = true;
         if(this != var1) {
            if(var1 != null && this.getClass() == var1.getClass()) {
               c.a var3 = (c.a)var1;
               if(this.a != var3.a) {
                  var2 = false;
               } else if(this.b != var3.b) {
                  var2 = false;
               }
            } else {
               var2 = false;
            }
         }

         return var2;
      }

      public int hashCode() {
         return this.a * 31 + this.b;
      }

      public String toString() {
         return "{availableBitrate=" + this.a + ", targetRateShare=" + this.b + '}';
      }
   }
}
