package com.googlecode.mp4parser.boxes.mp4;

import com.googlecode.mp4parser.AbstractFullBox;
import com.googlecode.mp4parser.e;
import com.googlecode.mp4parser.boxes.mp4.a.b;
import com.googlecode.mp4parser.boxes.mp4.a.l;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.mp4parser.aspectj.lang.a;

public class AbstractDescriptorBox extends AbstractFullBox {
   private static final a.a ajc$tjp_0;
   private static final a.a ajc$tjp_1;
   private static final a.a ajc$tjp_2;
   private static final a.a ajc$tjp_3;
   private static final a.a ajc$tjp_4;
   private static Logger log;
   protected ByteBuffer data;
   protected b descriptor;

   static {
      ajc$preClinit();
      log = Logger.getLogger(AbstractDescriptorBox.class.getName());
   }

   public AbstractDescriptorBox(String var1) {
      super(var1);
   }

   private static void ajc$preClinit() {
      org.mp4parser.aspectj.a.b.b var0 = new org.mp4parser.aspectj.a.b.b("AbstractDescriptorBox.java", AbstractDescriptorBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getData", "com.googlecode.mp4parser.boxes.mp4.AbstractDescriptorBox", "", "", "", "java.nio.ByteBuffer"), 42);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "setData", "com.googlecode.mp4parser.boxes.mp4.AbstractDescriptorBox", "java.nio.ByteBuffer", "data", "", "void"), 46);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "getDescriptor", "com.googlecode.mp4parser.boxes.mp4.AbstractDescriptorBox", "", "", "", "com.googlecode.mp4parser.boxes.mp4.objectdescriptors.BaseDescriptor"), 62);
      ajc$tjp_3 = var0.a("method-execution", var0.a("1", "setDescriptor", "com.googlecode.mp4parser.boxes.mp4.AbstractDescriptorBox", "com.googlecode.mp4parser.boxes.mp4.objectdescriptors.BaseDescriptor", "descriptor", "", "void"), 66);
      ajc$tjp_4 = var0.a("method-execution", var0.a("1", "getDescriptorAsString", "com.googlecode.mp4parser.boxes.mp4.AbstractDescriptorBox", "", "", "", "java.lang.String"), 70);
   }

   public void _parseDetails(ByteBuffer var1) {
      this.parseVersionAndFlags(var1);
      this.data = var1.slice();
      var1.position(var1.position() + var1.remaining());

      try {
         this.data.rewind();
         this.descriptor = l.a(-1, this.data.duplicate());
      } catch (IOException var2) {
         log.log(Level.WARNING, "Error parsing ObjectDescriptor", var2);
      } catch (IndexOutOfBoundsException var3) {
         log.log(Level.WARNING, "Error parsing ObjectDescriptor", var3);
      }

   }

   protected void getContent(ByteBuffer var1) {
      this.writeVersionAndFlags(var1);
      this.data.rewind();
      var1.put(this.data);
   }

   protected long getContentSize() {
      return (long)(this.data.limit() + 4);
   }

   public ByteBuffer getData() {
      a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_0, this, this);
      e.a().a(var1);
      return this.data;
   }

   public b getDescriptor() {
      a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_2, this, this);
      e.a().a(var1);
      return this.descriptor;
   }

   public String getDescriptorAsString() {
      a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_4, this, this);
      e.a().a(var1);
      return this.descriptor.toString();
   }

   public void setData(ByteBuffer var1) {
      a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_1, this, this, (Object)var1);
      e.a().a(var2);
      this.data = var1;
   }

   public void setDescriptor(b var1) {
      a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_3, this, this, (Object)var1);
      e.a().a(var2);
      this.descriptor = var1;
   }
}
