package com.googlecode.mp4parser.boxes.mp4;

import com.googlecode.mp4parser.e;
import com.googlecode.mp4parser.boxes.mp4.a.h;
import java.nio.ByteBuffer;
import org.mp4parser.aspectj.a.b.b;
import org.mp4parser.aspectj.lang.a;

public class ESDescriptorBox extends AbstractDescriptorBox {
   public static final String TYPE = "esds";
   private static final a.a ajc$tjp_0;
   private static final a.a ajc$tjp_1;
   private static final a.a ajc$tjp_2;
   private static final a.a ajc$tjp_3;

   static {
      ajc$preClinit();
   }

   public ESDescriptorBox() {
      super("esds");
   }

   private static void ajc$preClinit() {
      b var0 = new b("ESDescriptorBox.java", ESDescriptorBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getEsDescriptor", "com.googlecode.mp4parser.boxes.mp4.ESDescriptorBox", "", "", "", "com.googlecode.mp4parser.boxes.mp4.objectdescriptors.ESDescriptor"), 35);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "setEsDescriptor", "com.googlecode.mp4parser.boxes.mp4.ESDescriptorBox", "com.googlecode.mp4parser.boxes.mp4.objectdescriptors.ESDescriptor", "esDescriptor", "", "void"), 39);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "equals", "com.googlecode.mp4parser.boxes.mp4.ESDescriptorBox", "java.lang.Object", "o", "", "boolean"), 44);
      ajc$tjp_3 = var0.a("method-execution", var0.a("1", "hashCode", "com.googlecode.mp4parser.boxes.mp4.ESDescriptorBox", "", "", "", "int"), 55);
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      a var3 = b.a(ajc$tjp_2, this, this, (Object)var1);
      e.a().a(var3);
      if(this != var1) {
         if(var1 != null && this.getClass() == var1.getClass()) {
            ESDescriptorBox var4 = (ESDescriptorBox)var1;
            if(this.data != null) {
               if(this.data.equals(var4.data)) {
                  return var2;
               }
            } else if(var4.data == null) {
               return var2;
            }

            var2 = false;
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   protected void getContent(ByteBuffer var1) {
      this.writeVersionAndFlags(var1);
      h var2 = this.getEsDescriptor();
      if(var2 != null) {
         var1.put((ByteBuffer)var2.b().rewind());
      } else {
         var1.put(this.data.duplicate());
      }

   }

   protected long getContentSize() {
      h var3 = this.getEsDescriptor();
      long var1;
      if(var3 != null) {
         var1 = (long)(var3.d() + 4);
      } else {
         var1 = (long)(this.data.remaining() + 4);
      }

      return var1;
   }

   public h getEsDescriptor() {
      a var1 = b.a(ajc$tjp_0, this, this);
      e.a().a(var1);
      return (h)super.getDescriptor();
   }

   public int hashCode() {
      a var2 = b.a(ajc$tjp_3, this, this);
      e.a().a(var2);
      int var1;
      if(this.data != null) {
         var1 = this.data.hashCode();
      } else {
         var1 = 0;
      }

      return var1;
   }

   public void setEsDescriptor(h var1) {
      a var2 = b.a(ajc$tjp_1, this, this, (Object)var1);
      e.a().a(var2);
      super.setDescriptor(var1);
   }
}
