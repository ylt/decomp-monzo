package com.googlecode.mp4parser;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;

public class d implements b {
   ByteBuffer a;

   public d(ByteBuffer var1) {
      this.a = var1;
   }

   public int a(ByteBuffer var1) throws IOException {
      int var2;
      if(this.a.remaining() == 0 && var1.remaining() != 0) {
         var2 = -1;
      } else {
         var2 = Math.min(var1.remaining(), this.a.remaining());
         if(var1.hasArray()) {
            var1.put(this.a.array(), this.a.position(), var2);
            this.a.position(this.a.position() + var2);
         } else {
            byte[] var3 = new byte[var2];
            this.a.get(var3);
            var1.put(var3);
         }
      }

      return var2;
   }

   public long a() throws IOException {
      return (long)this.a.capacity();
   }

   public long a(long var1, long var3, WritableByteChannel var5) throws IOException {
      return (long)var5.write((ByteBuffer)((ByteBuffer)this.a.position(com.googlecode.mp4parser.c.b.a(var1))).slice().limit(com.googlecode.mp4parser.c.b.a(var3)));
   }

   public ByteBuffer a(long var1, long var3) throws IOException {
      int var5 = this.a.position();
      this.a.position(com.googlecode.mp4parser.c.b.a(var1));
      ByteBuffer var6 = this.a.slice();
      var6.limit(com.googlecode.mp4parser.c.b.a(var3));
      this.a.position(var5);
      return var6;
   }

   public void a(long var1) throws IOException {
      this.a.position(com.googlecode.mp4parser.c.b.a(var1));
   }

   public long b() throws IOException {
      return (long)this.a.position();
   }

   public void close() throws IOException {
   }
}
