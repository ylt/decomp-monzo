package com.googlecode.mp4parser;

import com.coremedia.iso.g;
import com.coremedia.iso.boxes.FullBox;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;
import java.util.List;
import java.util.logging.Logger;

public abstract class FullContainerBox extends AbstractContainerBox implements FullBox {
   private static Logger LOG = Logger.getLogger(FullContainerBox.class.getName());
   private int flags;
   private int version;

   public FullContainerBox(String var1) {
      super(var1);
   }

   public void getBox(WritableByteChannel var1) throws IOException {
      super.getBox(var1);
   }

   public List getBoxes(Class var1) {
      return this.getBoxes(var1, false);
   }

   public int getFlags() {
      return this.flags;
   }

   protected ByteBuffer getHeader() {
      byte[] var1;
      ByteBuffer var2;
      if(!this.largeBox && this.getSize() < 4294967296L) {
         var1 = new byte[12];
         var1[4] = this.type.getBytes()[0];
         var1[5] = this.type.getBytes()[1];
         var1[6] = this.type.getBytes()[2];
         var1[7] = this.type.getBytes()[3];
         var2 = ByteBuffer.wrap(var1);
         g.b(var2, this.getSize());
         var2.position(8);
         this.writeVersionAndFlags(var2);
      } else {
         var1 = new byte[20];
         var1[3] = 1;
         var1[4] = this.type.getBytes()[0];
         var1[5] = this.type.getBytes()[1];
         var1[6] = this.type.getBytes()[2];
         var1[7] = this.type.getBytes()[3];
         var2 = ByteBuffer.wrap(var1);
         var2.position(8);
         g.a(var2, this.getSize());
         this.writeVersionAndFlags(var2);
      }

      var2.rewind();
      return var2;
   }

   public int getVersion() {
      return this.version;
   }

   public void parse(b var1, ByteBuffer var2, long var3, com.coremedia.iso.b var5) throws IOException {
      ByteBuffer var6 = ByteBuffer.allocate(4);
      var1.a(var6);
      this.parseVersionAndFlags((ByteBuffer)var6.rewind());
      super.parse(var1, var2, var3, var5);
   }

   protected final long parseVersionAndFlags(ByteBuffer var1) {
      this.version = com.coremedia.iso.e.d(var1);
      this.flags = com.coremedia.iso.e.b(var1);
      return 4L;
   }

   public void setFlags(int var1) {
      this.flags = var1;
   }

   public void setVersion(int var1) {
      this.version = var1;
   }

   public String toString() {
      return this.getClass().getSimpleName() + "[childBoxes]";
   }

   protected final void writeVersionAndFlags(ByteBuffer var1) {
      g.c(var1, this.version);
      g.a(var1, this.flags);
   }
}
