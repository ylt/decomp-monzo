package com.googlecode.mp4parser;

import com.coremedia.iso.g;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;

public class AbstractContainerBox extends a implements com.coremedia.iso.boxes.a {
   protected boolean largeBox;
   private long offset;
   com.coremedia.iso.boxes.b parent;
   protected String type;

   public AbstractContainerBox(String var1) {
      this.type = var1;
   }

   public void getBox(WritableByteChannel var1) throws IOException {
      var1.write(this.getHeader());
      this.writeContainer(var1);
   }

   protected ByteBuffer getHeader() {
      byte[] var1;
      ByteBuffer var2;
      if(!this.largeBox && this.getSize() < 4294967296L) {
         var1 = new byte[8];
         var1[4] = this.type.getBytes()[0];
         var1[5] = this.type.getBytes()[1];
         var1[6] = this.type.getBytes()[2];
         var1[7] = this.type.getBytes()[3];
         var2 = ByteBuffer.wrap(var1);
         g.b(var2, this.getSize());
      } else {
         var1 = new byte[16];
         var1[3] = 1;
         var1[4] = this.type.getBytes()[0];
         var1[5] = this.type.getBytes()[1];
         var1[6] = this.type.getBytes()[2];
         var1[7] = this.type.getBytes()[3];
         var2 = ByteBuffer.wrap(var1);
         var2.position(8);
         g.a(var2, this.getSize());
      }

      var2.rewind();
      return var2;
   }

   public long getOffset() {
      return this.offset;
   }

   public com.coremedia.iso.boxes.b getParent() {
      return this.parent;
   }

   public long getSize() {
      long var2 = this.getContainerSize();
      byte var1;
      if(!this.largeBox && 8L + var2 < 4294967296L) {
         var1 = 8;
      } else {
         var1 = 16;
      }

      return (long)var1 + var2;
   }

   public String getType() {
      return this.type;
   }

   public void initContainer(b var1, long var2, com.coremedia.iso.b var4) throws IOException {
      this.dataSource = var1;
      this.parsePosition = var1.b();
      long var6 = this.parsePosition;
      byte var5;
      if(!this.largeBox && 8L + var2 < 4294967296L) {
         var5 = 8;
      } else {
         var5 = 16;
      }

      this.startPosition = var6 - (long)var5;
      var1.a(var1.b() + var2);
      this.endPosition = var1.b();
      this.boxParser = var4;
   }

   public void parse(b var1, ByteBuffer var2, long var3, com.coremedia.iso.b var5) throws IOException {
      this.offset = var1.b() - (long)var2.remaining();
      boolean var6;
      if(var2.remaining() == 16) {
         var6 = true;
      } else {
         var6 = false;
      }

      this.largeBox = var6;
      this.initContainer(var1, var3, var5);
   }

   public void setParent(com.coremedia.iso.boxes.b var1) {
      this.parent = var1;
   }
}
