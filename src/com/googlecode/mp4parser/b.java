package com.googlecode.mp4parser;

import java.io.Closeable;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;

public interface b extends Closeable {
   int a(ByteBuffer var1) throws IOException;

   long a() throws IOException;

   long a(long var1, long var3, WritableByteChannel var5) throws IOException;

   ByteBuffer a(long var1, long var3) throws IOException;

   void a(long var1) throws IOException;

   long b() throws IOException;

   void close() throws IOException;
}
