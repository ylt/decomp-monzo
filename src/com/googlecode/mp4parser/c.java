package com.googlecode.mp4parser;

import com.googlecode.mp4parser.c.f;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.WritableByteChannel;
import java.nio.channels.FileChannel.MapMode;

public class c implements b {
   private static f c = f.a(c.class);
   FileChannel a;
   String b;

   public c(File var1) throws FileNotFoundException {
      this.a = (new FileInputStream(var1)).getChannel();
      this.b = var1.getName();
   }

   public int a(ByteBuffer var1) throws IOException {
      synchronized(this){}

      int var2;
      try {
         var2 = this.a.read(var1);
      } finally {
         ;
      }

      return var2;
   }

   public long a() throws IOException {
      synchronized(this){}

      long var1;
      try {
         var1 = this.a.size();
      } finally {
         ;
      }

      return var1;
   }

   public long a(long var1, long var3, WritableByteChannel var5) throws IOException {
      synchronized(this){}

      try {
         var1 = this.a.transferTo(var1, var3, var5);
      } finally {
         ;
      }

      return var1;
   }

   public ByteBuffer a(long var1, long var3) throws IOException {
      synchronized(this){}

      MappedByteBuffer var5;
      try {
         var5 = this.a.map(MapMode.READ_ONLY, var1, var3);
      } finally {
         ;
      }

      return var5;
   }

   public void a(long var1) throws IOException {
      synchronized(this){}

      try {
         this.a.position(var1);
      } finally {
         ;
      }

   }

   public long b() throws IOException {
      synchronized(this){}

      long var1;
      try {
         var1 = this.a.position();
      } finally {
         ;
      }

      return var1;
   }

   public void close() throws IOException {
      this.a.close();
   }

   public String toString() {
      return this.b;
   }
}
