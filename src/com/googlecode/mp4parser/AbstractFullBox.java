package com.googlecode.mp4parser;

import com.coremedia.iso.g;
import com.coremedia.iso.boxes.FullBox;
import java.nio.ByteBuffer;

public abstract class AbstractFullBox extends AbstractBox implements FullBox {
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_0;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_1;
   private int flags;
   private int version;

   static {
      ajc$preClinit();
   }

   protected AbstractFullBox(String var1) {
      super(var1);
   }

   protected AbstractFullBox(String var1, byte[] var2) {
      super(var1, var2);
   }

   private static void ajc$preClinit() {
      org.mp4parser.aspectj.a.b.b var0 = new org.mp4parser.aspectj.a.b.b("AbstractFullBox.java", AbstractFullBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "setVersion", "com.googlecode.mp4parser.AbstractFullBox", "int", "version", "", "void"), 51);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "setFlags", "com.googlecode.mp4parser.AbstractFullBox", "int", "flags", "", "void"), 64);
   }

   public int getFlags() {
      if(!this.isParsed) {
         this.parseDetails();
      }

      return this.flags;
   }

   public int getVersion() {
      if(!this.isParsed) {
         this.parseDetails();
      }

      return this.version;
   }

   protected final long parseVersionAndFlags(ByteBuffer var1) {
      this.version = com.coremedia.iso.e.d(var1);
      this.flags = com.coremedia.iso.e.b(var1);
      return 4L;
   }

   public void setFlags(int var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_1, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      e.a().a(var2);
      this.flags = var1;
   }

   public void setVersion(int var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_0, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      e.a().a(var2);
      this.version = var1;
   }

   protected final void writeVersionAndFlags(ByteBuffer var1) {
      g.c(var1, this.version);
      g.a(var1, this.flags);
   }
}
