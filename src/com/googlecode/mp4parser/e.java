package com.googlecode.mp4parser;

import org.mp4parser.aspectj.lang.NoAspectBoundException;

public class e {
   public static final e a;
   private static Throwable b;

   static {
      try {
         b();
      } catch (Throwable var1) {
         b = var1;
      }

   }

   public static e a() {
      if(a == null) {
         throw new NoAspectBoundException("com.googlecode.mp4parser.RequiresParseDetailAspect", b);
      } else {
         return a;
      }
   }

   private static void b() {
      a = new e();
   }

   public void a(org.mp4parser.aspectj.lang.a var1) {
      if(var1.a() instanceof AbstractBox) {
         if(!((AbstractBox)var1.a()).isParsed()) {
            ((AbstractBox)var1.a()).parseDetails();
         }

      } else {
         throw new RuntimeException("Only methods in subclasses of " + AbstractBox.class.getName() + " can  be annotated with ParseDetail");
      }
   }
}
