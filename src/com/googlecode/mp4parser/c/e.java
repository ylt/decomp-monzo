package com.googlecode.mp4parser.c;

import java.util.AbstractList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

public class e extends AbstractList {
   private static final f c = f.a(e.class);
   List a;
   Iterator b;

   public e(List var1, Iterator var2) {
      this.a = var1;
      this.b = var2;
   }

   private void a() {
      c.a("blowup running");

      while(this.b.hasNext()) {
         this.a.add(this.b.next());
      }

   }

   public Object get(int var1) {
      Object var2;
      if(this.a.size() > var1) {
         var2 = this.a.get(var1);
      } else {
         if(!this.b.hasNext()) {
            throw new NoSuchElementException();
         }

         this.a.add(this.b.next());
         var2 = this.get(var1);
      }

      return var2;
   }

   public Iterator iterator() {
      return new Iterator() {
         int a = 0;

         public boolean hasNext() {
            boolean var1;
            if(this.a >= e.this.a.size() && !e.this.b.hasNext()) {
               var1 = false;
            } else {
               var1 = true;
            }

            return var1;
         }

         public Object next() {
            Object var3;
            if(this.a < e.this.a.size()) {
               List var2 = e.this.a;
               int var1 = this.a;
               this.a = var1 + 1;
               var3 = var2.get(var1);
            } else {
               e.this.a.add(e.this.b.next());
               var3 = this.next();
            }

            return var3;
         }

         public void remove() {
            throw new UnsupportedOperationException();
         }
      };
   }

   public int size() {
      c.a("potentially expensive size() call");
      this.a();
      return this.a.size();
   }
}
