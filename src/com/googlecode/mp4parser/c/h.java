package com.googlecode.mp4parser.c;

import java.nio.ByteBuffer;

public class h {
   public static final h j = new h(1.0D, 0.0D, 0.0D, 1.0D, 0.0D, 0.0D, 1.0D, 0.0D, 0.0D);
   public static final h k = new h(0.0D, 1.0D, -1.0D, 0.0D, 0.0D, 0.0D, 1.0D, 0.0D, 0.0D);
   public static final h l = new h(-1.0D, 0.0D, 0.0D, -1.0D, 0.0D, 0.0D, 1.0D, 0.0D, 0.0D);
   public static final h m = new h(0.0D, -1.0D, 1.0D, 0.0D, 0.0D, 0.0D, 1.0D, 0.0D, 0.0D);
   double a;
   double b;
   double c;
   double d;
   double e;
   double f;
   double g;
   double h;
   double i;

   public h(double var1, double var3, double var5, double var7, double var9, double var11, double var13, double var15, double var17) {
      this.a = var9;
      this.b = var11;
      this.c = var13;
      this.d = var1;
      this.e = var3;
      this.f = var5;
      this.g = var7;
      this.h = var15;
      this.i = var17;
   }

   public static h a(double var0, double var2, double var4, double var6, double var8, double var10, double var12, double var14, double var16) {
      return new h(var0, var2, var6, var8, var4, var10, var16, var12, var14);
   }

   public static h a(ByteBuffer var0) {
      return a(com.coremedia.iso.e.g(var0), com.coremedia.iso.e.g(var0), com.coremedia.iso.e.h(var0), com.coremedia.iso.e.g(var0), com.coremedia.iso.e.g(var0), com.coremedia.iso.e.h(var0), com.coremedia.iso.e.g(var0), com.coremedia.iso.e.g(var0), com.coremedia.iso.e.h(var0));
   }

   public void b(ByteBuffer var1) {
      com.coremedia.iso.g.a(var1, this.d);
      com.coremedia.iso.g.a(var1, this.e);
      com.coremedia.iso.g.b(var1, this.a);
      com.coremedia.iso.g.a(var1, this.f);
      com.coremedia.iso.g.a(var1, this.g);
      com.coremedia.iso.g.b(var1, this.b);
      com.coremedia.iso.g.a(var1, this.h);
      com.coremedia.iso.g.a(var1, this.i);
      com.coremedia.iso.g.b(var1, this.c);
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(this != var1) {
         if(var1 != null && this.getClass() == var1.getClass()) {
            h var3 = (h)var1;
            if(Double.compare(var3.d, this.d) != 0) {
               var2 = false;
            } else if(Double.compare(var3.e, this.e) != 0) {
               var2 = false;
            } else if(Double.compare(var3.f, this.f) != 0) {
               var2 = false;
            } else if(Double.compare(var3.g, this.g) != 0) {
               var2 = false;
            } else if(Double.compare(var3.h, this.h) != 0) {
               var2 = false;
            } else if(Double.compare(var3.i, this.i) != 0) {
               var2 = false;
            } else if(Double.compare(var3.a, this.a) != 0) {
               var2 = false;
            } else if(Double.compare(var3.b, this.b) != 0) {
               var2 = false;
            } else if(Double.compare(var3.c, this.c) != 0) {
               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public int hashCode() {
      long var9 = Double.doubleToLongBits(this.a);
      int var5 = (int)(var9 ^ var9 >>> 32);
      var9 = Double.doubleToLongBits(this.b);
      int var2 = (int)(var9 ^ var9 >>> 32);
      var9 = Double.doubleToLongBits(this.c);
      int var8 = (int)(var9 ^ var9 >>> 32);
      var9 = Double.doubleToLongBits(this.d);
      int var1 = (int)(var9 ^ var9 >>> 32);
      var9 = Double.doubleToLongBits(this.e);
      int var7 = (int)(var9 ^ var9 >>> 32);
      var9 = Double.doubleToLongBits(this.f);
      int var3 = (int)(var9 ^ var9 >>> 32);
      var9 = Double.doubleToLongBits(this.g);
      int var6 = (int)(var9 ^ var9 >>> 32);
      var9 = Double.doubleToLongBits(this.h);
      int var4 = (int)(var9 ^ var9 >>> 32);
      var9 = Double.doubleToLongBits(this.i);
      return (((((((var5 * 31 + var2) * 31 + var8) * 31 + var1) * 31 + var7) * 31 + var3) * 31 + var6) * 31 + var4) * 31 + (int)(var9 ^ var9 >>> 32);
   }

   public String toString() {
      String var1;
      if(this.equals(j)) {
         var1 = "Rotate 0°";
      } else if(this.equals(k)) {
         var1 = "Rotate 90°";
      } else if(this.equals(l)) {
         var1 = "Rotate 180°";
      } else if(this.equals(m)) {
         var1 = "Rotate 270°";
      } else {
         var1 = "Matrix{u=" + this.a + ", v=" + this.b + ", w=" + this.c + ", a=" + this.d + ", b=" + this.e + ", c=" + this.f + ", d=" + this.g + ", tx=" + this.h + ", ty=" + this.i + '}';
      }

      return var1;
   }
}
