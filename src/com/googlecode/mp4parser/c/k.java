package com.googlecode.mp4parser.c;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.UUID;

public class k {
   public static UUID a(byte[] var0) {
      ByteBuffer var1 = ByteBuffer.wrap(var0);
      var1.order(ByteOrder.BIG_ENDIAN);
      return new UUID(var1.getLong(), var1.getLong());
   }

   public static byte[] a(UUID var0) {
      byte var2 = 8;
      long var5 = var0.getMostSignificantBits();
      long var3 = var0.getLeastSignificantBits();
      byte[] var7 = new byte[16];

      int var1;
      for(var1 = 0; var1 < 8; ++var1) {
         var7[var1] = (byte)((int)(var5 >>> (7 - var1) * 8));
      }

      for(var1 = var2; var1 < 16; ++var1) {
         var7[var1] = (byte)((int)(var3 >>> (7 - var1) * 8));
      }

      return var7;
   }
}
