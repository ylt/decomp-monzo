package com.googlecode.mp4parser.c;

public final class i {
   public static long[] a(long[] var0, long... var1) {
      long[] var2 = var0;
      if(var0 == null) {
         var2 = new long[0];
      }

      var0 = var1;
      if(var1 == null) {
         var0 = new long[0];
      }

      var1 = new long[var2.length + var0.length];
      System.arraycopy(var2, 0, var1, 0, var2.length);
      System.arraycopy(var0, 0, var1, var2.length, var0.length);
      return var1;
   }
}
