package com.googlecode.mp4parser.c;

import java.util.logging.Level;
import java.util.logging.Logger;

public class d extends f {
   Logger a;

   public d(String var1) {
      this.a = Logger.getLogger(var1);
   }

   public void a(String var1) {
      this.a.log(Level.FINE, var1);
   }

   public void b(String var1) {
      this.a.log(Level.WARNING, var1);
   }

   public void c(String var1) {
      this.a.log(Level.SEVERE, var1);
   }
}
