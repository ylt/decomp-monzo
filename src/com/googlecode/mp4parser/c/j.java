package com.googlecode.mp4parser.c;

import com.googlecode.mp4parser.AbstractContainerBox;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class j {
   static Pattern a;
   // $FF: synthetic field
   static final boolean b;

   static {
      boolean var0;
      if(!j.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      b = var0;
      a = Pattern.compile("(....|\\.\\.)(\\[(.*)\\])?");
   }

   public static com.coremedia.iso.boxes.a a(AbstractContainerBox var0, String var1) {
      List var2 = a(var0, var1, true);
      com.coremedia.iso.boxes.a var3;
      if(var2.isEmpty()) {
         var3 = null;
      } else {
         var3 = (com.coremedia.iso.boxes.a)var2.get(0);
      }

      return var3;
   }

   public static String a(com.coremedia.iso.boxes.a var0) {
      return b(var0, "");
   }

   public static List a(com.coremedia.iso.boxes.a var0, String var1) {
      return a(var0, var1, false);
   }

   private static List a(com.coremedia.iso.boxes.a var0, String var1, boolean var2) {
      return a((Object)var0, var1, var2);
   }

   public static List a(com.coremedia.iso.boxes.b var0, String var1) {
      return a(var0, var1, false);
   }

   private static List a(com.coremedia.iso.boxes.b var0, String var1, boolean var2) {
      return a((Object)var0, var1, var2);
   }

   private static List a(AbstractContainerBox var0, String var1, boolean var2) {
      return a((Object)var0, var1, var2);
   }

   private static List a(Object var0, String var1, boolean var2) {
      int var3 = 0;
      if(var1.startsWith("/")) {
         for(var1 = var1.substring(1); var0 instanceof com.coremedia.iso.boxes.a; var0 = ((com.coremedia.iso.boxes.a)var0).getParent()) {
            ;
         }
      }

      if(var1.length() == 0) {
         if(!(var0 instanceof com.coremedia.iso.boxes.a)) {
            throw new RuntimeException("Result of path expression seems to be the root container. This is not allowed!");
         }

         var0 = Collections.singletonList((com.coremedia.iso.boxes.a)var0);
      } else {
         String var5;
         if(var1.contains("/")) {
            var5 = var1.substring(var1.indexOf(47) + 1);
            var1 = var1.substring(0, var1.indexOf(47));
         } else {
            var5 = "";
         }

         Matcher var7 = a.matcher(var1);
         if(!var7.matches()) {
            throw new RuntimeException(var1 + " is invalid path.");
         }

         String var6 = var7.group(1);
         if("..".equals(var6)) {
            if(var0 instanceof com.coremedia.iso.boxes.a) {
               var0 = a(((com.coremedia.iso.boxes.a)var0).getParent(), var5, var2);
            } else {
               var0 = Collections.emptyList();
            }
         } else if(var0 instanceof com.coremedia.iso.boxes.b) {
            int var4;
            if(var7.group(2) != null) {
               var4 = Integer.parseInt(var7.group(3));
            } else {
               var4 = -1;
            }

            LinkedList var9 = new LinkedList();
            Iterator var10 = ((com.coremedia.iso.boxes.b)var0).getBoxes().iterator();

            while(true) {
               do {
                  if(!var10.hasNext()) {
                     var0 = var9;
                     return (List)var0;
                  }

                  com.coremedia.iso.boxes.a var8 = (com.coremedia.iso.boxes.a)var10.next();
                  if(var8.getType().matches(var6)) {
                     if(var4 == -1 || var4 == var3) {
                        var9.addAll(a(var8, var5, var2));
                     }

                     ++var3;
                  }
               } while(!var2 && var4 < 0);

               if(!var9.isEmpty()) {
                  var0 = var9;
                  break;
               }
            }
         } else {
            var0 = Collections.emptyList();
         }
      }

      return (List)var0;
   }

   private static String b(com.coremedia.iso.boxes.a var0, String var1) {
      com.coremedia.iso.boxes.b var3 = var0.getParent();
      Iterator var4 = var3.getBoxes().iterator();
      int var2 = 0;

      while(var4.hasNext()) {
         com.coremedia.iso.boxes.a var5 = (com.coremedia.iso.boxes.a)var4.next();
         if(var5.getType().equals(var0.getType())) {
            if(var5 == var0) {
               break;
            }

            ++var2;
         }
      }

      String var6 = String.format("/%s[%d]", new Object[]{var0.getType(), Integer.valueOf(var2)}) + var1;
      if(var3 instanceof com.coremedia.iso.boxes.a) {
         var6 = b((com.coremedia.iso.boxes.a)var3, var6);
      }

      return var6;
   }
}
