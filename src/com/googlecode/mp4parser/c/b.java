package com.googlecode.mp4parser.c;

public class b {
   public static int a(long var0) {
      if(var0 <= 2147483647L && var0 >= -2147483648L) {
         return (int)var0;
      } else {
         throw new RuntimeException("A cast to int has gone wrong. Please contact the mp4parser discussion group (" + var0 + ")");
      }
   }
}
