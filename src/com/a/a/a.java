package com.a.a;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.graphics.drawable.shapes.RectShape;
import android.graphics.drawable.shapes.RoundRectShape;

public class a extends ShapeDrawable {
   private final Paint a;
   private final Paint b;
   private final String c;
   private final int d;
   private final RectShape e;
   private final int f;
   private final int g;
   private final int h;
   private final float i;
   private final int j;

   private a(a.a var1) {
      super(var1.i);
      this.e = var1.i;
      this.f = var1.g;
      this.g = var1.f;
      this.i = var1.b;
      String var2;
      if(var1.l) {
         var2 = var1.c.toUpperCase();
      } else {
         var2 = var1.c;
      }

      this.c = var2;
      this.d = var1.d;
      this.h = var1.j;
      this.a = new Paint();
      this.a.setColor(var1.a);
      this.a.setAntiAlias(true);
      this.a.setFakeBoldText(var1.k);
      this.a.setStyle(Style.FILL);
      this.a.setTypeface(var1.h);
      this.a.setTextAlign(Align.CENTER);
      this.a.setStrokeWidth((float)var1.e);
      this.j = var1.e;
      this.b = new Paint();
      this.b.setColor(this.a(this.d));
      this.b.setStyle(Style.STROKE);
      this.b.setStrokeWidth((float)this.j);
      this.getPaint().setColor(this.d);
   }

   // $FF: synthetic method
   a(a.a var1, Object var2) {
      this(var1);
   }

   private int a(int var1) {
      return Color.rgb((int)((float)Color.red(var1) * 0.9F), (int)((float)Color.green(var1) * 0.9F), (int)((float)Color.blue(var1) * 0.9F));
   }

   public static a.d a() {
      return new a.a();
   }

   private void a(Canvas var1) {
      RectF var2 = new RectF(this.getBounds());
      var2.inset((float)(this.j / 2), (float)(this.j / 2));
      if(this.e instanceof OvalShape) {
         var1.drawOval(var2, this.b);
      } else if(this.e instanceof RoundRectShape) {
         var1.drawRoundRect(var2, this.i, this.i, this.b);
      } else {
         var1.drawRect(var2, this.b);
      }

   }

   public void draw(Canvas var1) {
      super.draw(var1);
      Rect var6 = this.getBounds();
      if(this.j > 0) {
         this.a(var1);
      }

      int var5 = var1.save();
      var1.translate((float)var6.left, (float)var6.top);
      int var2;
      if(this.g < 0) {
         var2 = var6.width();
      } else {
         var2 = this.g;
      }

      int var3;
      if(this.f < 0) {
         var3 = var6.height();
      } else {
         var3 = this.f;
      }

      int var4;
      if(this.h < 0) {
         var4 = Math.min(var2, var3) / 2;
      } else {
         var4 = this.h;
      }

      this.a.setTextSize((float)var4);
      var1.drawText(this.c, (float)(var2 / 2), (float)(var3 / 2) - (this.a.descent() + this.a.ascent()) / 2.0F, this.a);
      var1.restoreToCount(var5);
   }

   public int getIntrinsicHeight() {
      return this.f;
   }

   public int getIntrinsicWidth() {
      return this.g;
   }

   public int getOpacity() {
      return -3;
   }

   public void setAlpha(int var1) {
      this.a.setAlpha(var1);
   }

   public void setColorFilter(ColorFilter var1) {
      this.a.setColorFilter(var1);
   }

   public static class a implements a.b, a.c, a.d {
      public int a;
      public float b;
      private String c;
      private int d;
      private int e;
      private int f;
      private int g;
      private Typeface h;
      private RectShape i;
      private int j;
      private boolean k;
      private boolean l;

      private a() {
         this.c = "";
         this.d = -7829368;
         this.a = -1;
         this.e = 0;
         this.f = -1;
         this.g = -1;
         this.i = new RectShape();
         this.h = Typeface.create("sans-serif-light", 0);
         this.j = -1;
         this.k = false;
         this.l = false;
      }

      // $FF: synthetic method
      a(Object var1) {
         this();
      }

      public a.c a() {
         this.l = true;
         return this;
      }

      public a.c a(int var1) {
         this.a = var1;
         return this;
      }

      public a.c a(Typeface var1) {
         this.h = var1;
         return this;
      }

      public a a(String var1, int var2) {
         this.d();
         return this.c(var1, var2);
      }

      public a.c b() {
         return this;
      }

      public a.c b(int var1) {
         this.j = var1;
         return this;
      }

      public a b(String var1, int var2) {
         this.e();
         return this.c(var1, var2);
      }

      public a.d c() {
         return this;
      }

      public a c(String var1, int var2) {
         this.d = var2;
         this.c = var1;
         return new a(this);
      }

      public a.b d() {
         this.i = new RectShape();
         return this;
      }

      public a.b e() {
         this.i = new OvalShape();
         return this;
      }
   }

   public interface b {
   }

   public interface c {
      a.c a();

      a.c a(int var1);

      a.c a(Typeface var1);

      a.c b(int var1);

      a.d c();
   }

   public interface d {
      a a(String var1, int var2);

      a.c b();

      a b(String var1, int var2);
   }
}
