package com.b.b;

import io.reactivex.t;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public final class b extends d {
   private static final Object[] d = new Object[0];
   private static final b.a[] f = new b.a[0];
   final AtomicReference a;
   final Lock b;
   long c;
   private final AtomicReference e;
   private final Lock g;

   private b() {
      ReentrantReadWriteLock var1 = new ReentrantReadWriteLock();
      this.b = var1.readLock();
      this.g = var1.writeLock();
      this.e = new AtomicReference(f);
      this.a = new AtomicReference();
   }

   private b(Object var1) {
      this();
      if(var1 == null) {
         throw new NullPointerException("defaultValue == null");
      } else {
         this.a.lazySet(var1);
      }
   }

   public static b a() {
      return new b();
   }

   public static b b(Object var0) {
      return new b(var0);
   }

   private void b(b.a var1) {
      b.a[] var3;
      b.a[] var4;
      do {
         var4 = (b.a[])this.e.get();
         int var2 = var4.length;
         var3 = new b.a[var2 + 1];
         System.arraycopy(var4, 0, var3, 0, var2);
         var3[var2] = var1;
      } while(!this.e.compareAndSet(var4, var3));

   }

   private void c(Object var1) {
      this.g.lock();

      try {
         ++this.c;
         this.a.lazySet(var1);
      } finally {
         this.g.unlock();
      }

   }

   void a(b.a var1) {
      while(true) {
         b.a[] var7 = (b.a[])this.e.get();
         if(var7 != f) {
            int var5 = var7.length;
            byte var4 = -1;
            int var2 = 0;

            int var3;
            while(true) {
               var3 = var4;
               if(var2 >= var5) {
                  break;
               }

               if(var7[var2] == var1) {
                  var3 = var2;
                  break;
               }

               ++var2;
            }

            if(var3 >= 0) {
               b.a[] var6;
               if(var5 == 1) {
                  var6 = f;
               } else {
                  var6 = new b.a[var5 - 1];
                  System.arraycopy(var7, 0, var6, 0, var3);
                  System.arraycopy(var7, var3 + 1, var6, var3, var5 - var3 - 1);
               }

               if(!this.e.compareAndSet(var7, var6)) {
                  continue;
               }
            }
         }

         return;
      }
   }

   public void a(Object var1) {
      if(var1 == null) {
         throw new NullPointerException("value == null");
      } else {
         this.c(var1);
         b.a[] var4 = (b.a[])this.e.get();
         int var3 = var4.length;

         for(int var2 = 0; var2 < var3; ++var2) {
            var4[var2].a(var1, this.c);
         }

      }
   }

   public Object b() {
      return this.a.get();
   }

   protected void subscribeActual(t var1) {
      b.a var2 = new b.a(var1, this);
      var1.onSubscribe(var2);
      this.b(var2);
      if(var2.g) {
         this.a(var2);
      } else {
         var2.a();
      }

   }

   static final class a implements a.a, io.reactivex.b.b {
      final t a;
      final b b;
      boolean c;
      boolean d;
      a e;
      boolean f;
      volatile boolean g;
      long h;

      a(t var1, b var2) {
         this.a = var1;
         this.b = var2;
      }

      void a() {
         // $FF: Couldn't be decompiled
      }

      void a(Object param1, long param2) {
         // $FF: Couldn't be decompiled
      }

      public boolean a(Object var1) {
         if(!this.g) {
            this.a.onNext(var1);
         }

         return false;
      }

      void b() {
         // $FF: Couldn't be decompiled
      }

      public void dispose() {
         if(!this.g) {
            this.g = true;
            this.b.a(this);
         }

      }

      public boolean isDisposed() {
         return this.g;
      }
   }
}
