package com.b.b;

import io.reactivex.t;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

public final class c extends d {
   private static final c.a[] a = new c.a[0];
   private final AtomicReference b;

   private c() {
      this.b = new AtomicReference(a);
   }

   public static c a() {
      return new c();
   }

   private void b(c.a var1) {
      c.a[] var3;
      c.a[] var4;
      do {
         var3 = (c.a[])this.b.get();
         int var2 = var3.length;
         var4 = new c.a[var2 + 1];
         System.arraycopy(var3, 0, var4, 0, var2);
         var4[var2] = var1;
      } while(!this.b.compareAndSet(var3, var4));

   }

   void a(c.a var1) {
      while(true) {
         c.a[] var7 = (c.a[])this.b.get();
         if(var7 != a) {
            int var5 = var7.length;
            byte var4 = -1;
            int var2 = 0;

            int var3;
            while(true) {
               var3 = var4;
               if(var2 >= var5) {
                  break;
               }

               if(var7[var2] == var1) {
                  var3 = var2;
                  break;
               }

               ++var2;
            }

            if(var3 >= 0) {
               c.a[] var6;
               if(var5 == 1) {
                  var6 = a;
               } else {
                  var6 = new c.a[var5 - 1];
                  System.arraycopy(var7, 0, var6, 0, var3);
                  System.arraycopy(var7, var3 + 1, var6, var3, var5 - var3 - 1);
               }

               if(!this.b.compareAndSet(var7, var6)) {
                  continue;
               }
            }
         }

         return;
      }
   }

   public void a(Object var1) {
      if(var1 == null) {
         throw new NullPointerException("value == null");
      } else {
         c.a[] var4 = (c.a[])this.b.get();
         int var3 = var4.length;

         for(int var2 = 0; var2 < var3; ++var2) {
            var4[var2].a(var1);
         }

      }
   }

   public void subscribeActual(t var1) {
      c.a var2 = new c.a(var1, this);
      var1.onSubscribe(var2);
      this.b(var2);
      if(var2.isDisposed()) {
         this.a(var2);
      }

   }

   static final class a extends AtomicBoolean implements io.reactivex.b.b {
      final t a;
      final c b;

      a(t var1, c var2) {
         this.a = var1;
         this.b = var2;
      }

      void a(Object var1) {
         if(!this.get()) {
            this.a.onNext(var1);
         }

      }

      public void dispose() {
         if(this.compareAndSet(false, true)) {
            this.b.a(this);
         }

      }

      public boolean isDisposed() {
         return this.get();
      }
   }
}
