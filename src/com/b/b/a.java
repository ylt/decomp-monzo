package com.b.b;

import io.reactivex.c.q;

class a {
   private final int a;
   private final Object[] b;
   private Object[] c;
   private int d;

   a(int var1) {
      this.a = var1;
      this.b = new Object[var1 + 1];
      this.c = this.b;
   }

   void a(a.a var1) {
      Object[] var4 = this.b;

      for(int var3 = this.a; var4 != null; var4 = (Object[])((Object[])var4[var3])) {
         for(int var2 = 0; var2 < var3; ++var2) {
            Object var5 = var4[var2];
            if(var5 == null || var1.a(var5)) {
               break;
            }
         }
      }

   }

   void a(Object var1) {
      int var4 = this.a;
      int var3 = this.d;
      int var2 = var3;
      if(var3 == var4) {
         Object[] var5 = new Object[var4 + 1];
         this.c[var4] = var5;
         this.c = var5;
         var2 = 0;
      }

      this.c[var2] = var1;
      this.d = var2 + 1;
   }

   public interface a extends q {
      boolean a(Object var1);
   }
}
