package com.b.a.d;

import android.text.Editable;
import android.widget.TextView;

public abstract class f {
   public static f a(TextView var0, Editable var1) {
      return new a(var0, var1);
   }

   public abstract TextView a();

   public abstract Editable b();
}
