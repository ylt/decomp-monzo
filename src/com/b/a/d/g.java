package com.b.a.d;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.TextView;
import io.reactivex.t;

final class g extends com.b.a.a {
   private final TextView a;

   g(TextView var1) {
      this.a = var1;
   }

   // $FF: synthetic method
   protected Object a() {
      return this.c();
   }

   protected void a(t var1) {
      g.a var2 = new g.a(this.a, var1);
      var1.onSubscribe(var2);
      this.a.addTextChangedListener(var2);
   }

   protected f c() {
      return f.a(this.a, this.a.getEditableText());
   }

   static final class a extends io.reactivex.a.a implements TextWatcher {
      private final TextView a;
      private final t b;

      a(TextView var1, t var2) {
         this.a = var1;
         this.b = var2;
      }

      protected void a() {
         this.a.removeTextChangedListener(this);
      }

      public void afterTextChanged(Editable var1) {
         this.b.onNext(f.a(this.a, var1));
      }

      public void beforeTextChanged(CharSequence var1, int var2, int var3, int var4) {
      }

      public void onTextChanged(CharSequence var1, int var2, int var3, int var4) {
      }
   }
}
