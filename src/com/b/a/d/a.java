package com.b.a.d;

import android.text.Editable;
import android.widget.TextView;

final class a extends f {
   private final TextView a;
   private final Editable b;

   a(TextView var1, Editable var2) {
      if(var1 == null) {
         throw new NullPointerException("Null view");
      } else {
         this.a = var1;
         this.b = var2;
      }
   }

   public TextView a() {
      return this.a;
   }

   public Editable b() {
      return this.b;
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(var1 != this) {
         if(var1 instanceof f) {
            f var3 = (f)var1;
            if(this.a.equals(var3.a())) {
               if(this.b == null) {
                  if(var3.b() == null) {
                     return var2;
                  }
               } else if(this.b.equals(var3.b())) {
                  return var2;
               }
            }

            var2 = false;
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public int hashCode() {
      int var2 = this.a.hashCode();
      int var1;
      if(this.b == null) {
         var1 = 0;
      } else {
         var1 = this.b.hashCode();
      }

      return var1 ^ 1000003 * (var2 ^ 1000003);
   }

   public String toString() {
      return "TextViewAfterTextChangeEvent{view=" + this.a + ", editable=" + this.b + "}";
   }
}
