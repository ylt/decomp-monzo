package com.b.a.d;

import android.widget.TextView;
import io.reactivex.n;
import io.reactivex.c.q;

public final class e {
   public static n a(TextView var0) {
      com.b.a.a.c.a(var0, "view == null");
      return a(var0, com.b.a.a.a.b);
   }

   public static n a(TextView var0, q var1) {
      com.b.a.a.c.a(var0, "view == null");
      com.b.a.a.c.a(var1, "handled == null");
      return new j(var0, var1);
   }

   public static n b(TextView var0) {
      com.b.a.a.c.a(var0, "view == null");
      return b(var0, com.b.a.a.a.b);
   }

   public static n b(TextView var0, q var1) {
      com.b.a.a.c.a(var0, "view == null");
      com.b.a.a.c.a(var1, "handled == null");
      return new i(var0, var1);
   }

   public static com.b.a.a c(TextView var0) {
      com.b.a.a.c.a(var0, "view == null");
      return new k(var0);
   }

   public static com.b.a.a d(TextView var0) {
      com.b.a.a.c.a(var0, "view == null");
      return new g(var0);
   }
}
