package com.b.a.d;

import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import io.reactivex.t;

final class c extends com.b.a.a {
   private final CompoundButton a;

   c(CompoundButton var1) {
      this.a = var1;
   }

   // $FF: synthetic method
   protected Object a() {
      return this.c();
   }

   protected void a(t var1) {
      if(com.b.a.a.c.a(var1)) {
         c.a var2 = new c.a(this.a, var1);
         var1.onSubscribe(var2);
         this.a.setOnCheckedChangeListener(var2);
      }

   }

   protected Boolean c() {
      return Boolean.valueOf(this.a.isChecked());
   }

   static final class a extends io.reactivex.a.a implements OnCheckedChangeListener {
      private final CompoundButton a;
      private final t b;

      a(CompoundButton var1, t var2) {
         this.a = var1;
         this.b = var2;
      }

      protected void a() {
         this.a.setOnCheckedChangeListener((OnCheckedChangeListener)null);
      }

      public void onCheckedChanged(CompoundButton var1, boolean var2) {
         if(!this.isDisposed()) {
            this.b.onNext(Boolean.valueOf(var2));
         }

      }
   }
}
