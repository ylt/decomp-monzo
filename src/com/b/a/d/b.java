package com.b.a.d;

import android.view.KeyEvent;
import android.widget.TextView;

final class b extends h {
   private final TextView a;
   private final int b;
   private final KeyEvent c;

   b(TextView var1, int var2, KeyEvent var3) {
      if(var1 == null) {
         throw new NullPointerException("Null view");
      } else {
         this.a = var1;
         this.b = var2;
         this.c = var3;
      }
   }

   public TextView a() {
      return this.a;
   }

   public int b() {
      return this.b;
   }

   public KeyEvent c() {
      return this.c;
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(var1 != this) {
         if(var1 instanceof h) {
            h var3 = (h)var1;
            if(this.a.equals(var3.a()) && this.b == var3.b()) {
               if(this.c == null) {
                  if(var3.c() == null) {
                     return var2;
                  }
               } else if(this.c.equals(var3.c())) {
                  return var2;
               }
            }

            var2 = false;
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public int hashCode() {
      int var2 = this.a.hashCode();
      int var3 = this.b;
      int var1;
      if(this.c == null) {
         var1 = 0;
      } else {
         var1 = this.c.hashCode();
      }

      return var1 ^ ((var2 ^ 1000003) * 1000003 ^ var3) * 1000003;
   }

   public String toString() {
      return "TextViewEditorActionEvent{view=" + this.a + ", actionId=" + this.b + ", keyEvent=" + this.c + "}";
   }
}
