package com.b.a.d;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.TextView;
import io.reactivex.t;

final class k extends com.b.a.a {
   private final TextView a;

   k(TextView var1) {
      this.a = var1;
   }

   // $FF: synthetic method
   protected Object a() {
      return this.c();
   }

   protected void a(t var1) {
      k.a var2 = new k.a(this.a, var1);
      var1.onSubscribe(var2);
      this.a.addTextChangedListener(var2);
   }

   protected CharSequence c() {
      return this.a.getText();
   }

   static final class a extends io.reactivex.a.a implements TextWatcher {
      private final TextView a;
      private final t b;

      a(TextView var1, t var2) {
         this.a = var1;
         this.b = var2;
      }

      protected void a() {
         this.a.removeTextChangedListener(this);
      }

      public void afterTextChanged(Editable var1) {
      }

      public void beforeTextChanged(CharSequence var1, int var2, int var3, int var4) {
      }

      public void onTextChanged(CharSequence var1, int var2, int var3, int var4) {
         if(!this.isDisposed()) {
            this.b.onNext(var1);
         }

      }
   }
}
