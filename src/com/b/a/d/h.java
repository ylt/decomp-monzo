package com.b.a.d;

import android.view.KeyEvent;
import android.widget.TextView;

public abstract class h {
   public static h a(TextView var0, int var1, KeyEvent var2) {
      return new b(var0, var1, var2);
   }

   public abstract TextView a();

   public abstract int b();

   public abstract KeyEvent c();
}
