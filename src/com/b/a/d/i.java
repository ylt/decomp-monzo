package com.b.a.d;

import android.view.KeyEvent;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import io.reactivex.n;
import io.reactivex.t;
import io.reactivex.c.q;

final class i extends n {
   private final TextView a;
   private final q b;

   i(TextView var1, q var2) {
      this.a = var1;
      this.b = var2;
   }

   protected void subscribeActual(t var1) {
      if(com.b.a.a.c.a(var1)) {
         i.a var2 = new i.a(this.a, var1, this.b);
         var1.onSubscribe(var2);
         this.a.setOnEditorActionListener(var2);
      }

   }

   static final class a extends io.reactivex.a.a implements OnEditorActionListener {
      private final TextView a;
      private final t b;
      private final q c;

      a(TextView var1, t var2, q var3) {
         this.a = var1;
         this.b = var2;
         this.c = var3;
      }

      protected void a() {
         this.a.setOnEditorActionListener((OnEditorActionListener)null);
      }

      public boolean onEditorAction(TextView var1, int var2, KeyEvent var3) {
         h var6 = h.a(this.a, var2, var3);

         boolean var4;
         label28: {
            try {
               if(!this.isDisposed() && this.c.a(var6)) {
                  this.b.onNext(var6);
                  break label28;
               }
            } catch (Exception var5) {
               this.b.onError(var5);
               this.dispose();
            }

            var4 = false;
            return var4;
         }

         var4 = true;
         return var4;
      }
   }
}
