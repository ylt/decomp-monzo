package com.b.a.c;

import android.view.View;
import android.view.View.OnFocusChangeListener;
import io.reactivex.t;

final class e extends com.b.a.a {
   private final View a;

   e(View var1) {
      this.a = var1;
   }

   // $FF: synthetic method
   protected Object a() {
      return this.c();
   }

   protected void a(t var1) {
      e.a var2 = new e.a(this.a, var1);
      var1.onSubscribe(var2);
      this.a.setOnFocusChangeListener(var2);
   }

   protected Boolean c() {
      return Boolean.valueOf(this.a.hasFocus());
   }

   static final class a extends io.reactivex.a.a implements OnFocusChangeListener {
      private final View a;
      private final t b;

      a(View var1, t var2) {
         this.a = var1;
         this.b = var2;
      }

      protected void a() {
         this.a.setOnFocusChangeListener((OnFocusChangeListener)null);
      }

      public void onFocusChange(View var1, boolean var2) {
         if(!this.isDisposed()) {
            this.b.onNext(Boolean.valueOf(var2));
         }

      }
   }
}
