package com.b.a.c;

import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import io.reactivex.n;
import io.reactivex.t;
import io.reactivex.c.q;

final class f extends n {
   private final View a;
   private final q b;

   f(View var1, q var2) {
      this.a = var1;
      this.b = var2;
   }

   protected void subscribeActual(t var1) {
      if(com.b.a.a.c.a(var1)) {
         f.a var2 = new f.a(this.a, this.b, var1);
         var1.onSubscribe(var2);
         this.a.setOnTouchListener(var2);
      }

   }

   static final class a extends io.reactivex.a.a implements OnTouchListener {
      private final View a;
      private final q b;
      private final t c;

      a(View var1, q var2, t var3) {
         this.a = var1;
         this.b = var2;
         this.c = var3;
      }

      protected void a() {
         this.a.setOnTouchListener((OnTouchListener)null);
      }

      public boolean onTouch(View var1, MotionEvent var2) {
         boolean var3;
         label25: {
            if(!this.isDisposed()) {
               try {
                  if(this.b.a(var2)) {
                     this.c.onNext(var2);
                     break label25;
                  }
               } catch (Exception var4) {
                  this.c.onError(var4);
                  this.dispose();
               }
            }

            var3 = false;
            return var3;
         }

         var3 = true;
         return var3;
      }
   }
}
