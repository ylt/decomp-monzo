package com.b.a.c;

import android.view.View;
import android.view.View.OnClickListener;
import io.reactivex.n;
import io.reactivex.t;

final class d extends n {
   private final View a;

   d(View var1) {
      this.a = var1;
   }

   protected void subscribeActual(t var1) {
      if(com.b.a.a.c.a(var1)) {
         d.a var2 = new d.a(this.a, var1);
         var1.onSubscribe(var2);
         this.a.setOnClickListener(var2);
      }

   }

   static final class a extends io.reactivex.a.a implements OnClickListener {
      private final View a;
      private final t b;

      a(View var1, t var2) {
         this.a = var1;
         this.b = var2;
      }

      protected void a() {
         this.a.setOnClickListener((OnClickListener)null);
      }

      public void onClick(View var1) {
         if(!this.isDisposed()) {
            this.b.onNext(com.b.a.a.b.a);
         }

      }
   }
}
