package com.b.a.c;

import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import io.reactivex.n;
import io.reactivex.t;
import io.reactivex.c.q;

final class a extends n {
   private final MenuItem a;
   private final q b;

   a(MenuItem var1, q var2) {
      this.a = var1;
      this.b = var2;
   }

   protected void subscribeActual(t var1) {
      if(com.b.a.a.c.a(var1)) {
         a.a var2 = new a.a(this.a, this.b, var1);
         var1.onSubscribe(var2);
         this.a.setOnMenuItemClickListener(var2);
      }

   }

   static final class a extends io.reactivex.a.a implements OnMenuItemClickListener {
      private final MenuItem a;
      private final q b;
      private final t c;

      a(MenuItem var1, q var2, t var3) {
         this.a = var1;
         this.b = var2;
         this.c = var3;
      }

      protected void a() {
         this.a.setOnMenuItemClickListener((OnMenuItemClickListener)null);
      }

      public boolean onMenuItemClick(MenuItem var1) {
         boolean var2;
         label25: {
            if(!this.isDisposed()) {
               try {
                  if(this.b.a(this.a)) {
                     this.c.onNext(com.b.a.a.b.a);
                     break label25;
                  }
               } catch (Exception var3) {
                  this.c.onError(var3);
                  this.dispose();
               }
            }

            var2 = false;
            return var2;
         }

         var2 = true;
         return var2;
      }
   }
}
