package com.b.a.b.b.a;

import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.f;
import io.reactivex.t;

final class b extends com.b.a.a {
   private final ViewPager a;

   b(ViewPager var1) {
      this.a = var1;
   }

   // $FF: synthetic method
   protected Object a() {
      return this.c();
   }

   protected void a(t var1) {
      b.a var2 = new b.a(this.a, var1);
      var1.onSubscribe(var2);
      this.a.a(var2);
   }

   protected Integer c() {
      return Integer.valueOf(this.a.getCurrentItem());
   }

   static final class a extends io.reactivex.a.a implements f {
      private final ViewPager a;
      private final t b;

      a(ViewPager var1, t var2) {
         this.a = var1;
         this.b = var2;
      }

      protected void a() {
         this.a.b(this);
      }

      public void a(int var1) {
      }

      public void a(int var1, float var2, int var3) {
      }

      public void b(int var1) {
         if(!this.isDisposed()) {
            this.b.onNext(Integer.valueOf(var1));
         }

      }
   }
}
