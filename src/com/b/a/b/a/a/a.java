package com.b.a.b.a.a;

import android.support.design.widget.BottomNavigationView;
import android.view.Menu;
import android.view.MenuItem;
import com.b.a.a.c;
import io.reactivex.n;
import io.reactivex.t;

final class a extends n {
   private final BottomNavigationView a;

   a(BottomNavigationView var1) {
      this.a = var1;
   }

   protected void subscribeActual(t var1) {
      if(c.a(var1)) {
         a.a var4 = new a.a(this.a, var1);
         var1.onSubscribe(var4);
         this.a.setOnNavigationItemSelectedListener(var4);
         Menu var5 = this.a.getMenu();
         int var2 = 0;

         for(int var3 = var5.size(); var2 < var3; ++var2) {
            MenuItem var6 = var5.getItem(var2);
            if(var6.isChecked()) {
               var1.onNext(var6);
               break;
            }
         }
      }

   }

   static final class a extends io.reactivex.a.a implements android.support.design.widget.BottomNavigationView.b {
      private final BottomNavigationView a;
      private final t b;

      a(BottomNavigationView var1, t var2) {
         this.a = var1;
         this.b = var2;
      }

      protected void a() {
         this.a.setOnNavigationItemSelectedListener((android.support.design.widget.BottomNavigationView.b)null);
      }

      public boolean a(MenuItem var1) {
         if(!this.isDisposed()) {
            this.b.onNext(var1);
         }

         return true;
      }
   }
}
