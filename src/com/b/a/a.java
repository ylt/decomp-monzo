package com.b.a;

import io.reactivex.n;
import io.reactivex.t;

public abstract class a extends n {
   protected abstract Object a();

   protected abstract void a(t var1);

   public final n b() {
      return new a.a();
   }

   protected final void subscribeActual(t var1) {
      this.a(var1);
      var1.onNext(this.a());
   }

   private final class a extends n {
      protected void subscribeActual(t var1) {
         a.this.a(var1);
      }
   }
}
