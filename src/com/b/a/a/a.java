package com.b.a.a;

import io.reactivex.c.q;
import java.util.concurrent.Callable;

public final class a {
   public static final Callable a;
   public static final q b;
   private static final a.a c = new a.a(Boolean.valueOf(true));

   static {
      a = c;
      b = c;
   }

   private static final class a implements q, Callable {
      private final Boolean a;

      a(Boolean var1) {
         this.a = var1;
      }

      public Boolean a() {
         return this.a;
      }

      public boolean a(Object var1) throws Exception {
         return this.a.booleanValue();
      }

      // $FF: synthetic method
      public Object call() throws Exception {
         return this.a();
      }
   }
}
