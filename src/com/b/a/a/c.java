package com.b.a.a;

import android.os.Looper;
import io.reactivex.t;

public final class c {
   public static void a(Object var0, String var1) {
      if(var0 == null) {
         throw new NullPointerException(var1);
      }
   }

   public static boolean a(t var0) {
      boolean var1;
      if(Looper.myLooper() != Looper.getMainLooper()) {
         var0.onError(new IllegalStateException("Expected to be called on the main thread but was " + Thread.currentThread().getName()));
         var1 = false;
      } else {
         var1 = true;
      }

      return var1;
   }
}
