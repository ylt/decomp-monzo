package com.stripe.android.net;

import com.stripe.android.model.BankAccount;
import com.stripe.android.util.StripeJsonUtils;
import com.stripe.android.util.StripeTextUtils;
import org.json.JSONException;
import org.json.JSONObject;

class BankAccountParser {
   private static final String FIELD_ACCOUNT_HOLDER_NAME = "account_holder_name";
   private static final String FIELD_ACCOUNT_HOLDER_TYPE = "account_holder_type";
   private static final String FIELD_BANK_NAME = "bank_name";
   private static final String FIELD_COUNTRY = "country";
   private static final String FIELD_CURRENCY = "currency";
   private static final String FIELD_FINGERPRINT = "fingerprint";
   private static final String FIELD_LAST4 = "last4";
   private static final String FIELD_ROUTING_NUMBER = "routing_number";

   static BankAccount parseBankAccount(String var0) throws JSONException {
      return parseBankAccount(new JSONObject(var0));
   }

   static BankAccount parseBankAccount(JSONObject var0) {
      return new BankAccount(StripeJsonUtils.optString(var0, "account_holder_name"), StripeTextUtils.asBankAccountType(StripeJsonUtils.optString(var0, "account_holder_type")), StripeJsonUtils.optString(var0, "bank_name"), StripeJsonUtils.optCountryCode(var0, "country"), StripeJsonUtils.optCurrency(var0, "currency"), StripeJsonUtils.optString(var0, "fingerprint"), StripeJsonUtils.optString(var0, "last4"), StripeJsonUtils.optString(var0, "routing_number"));
   }
}
