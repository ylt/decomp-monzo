package com.stripe.android.net;

import com.stripe.android.util.StripeTextUtils;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class RequestOptions {
   public static final String TYPE_JSON = "json_data";
   public static final String TYPE_QUERY = "source";
   private final String mApiVersion;
   private final String mGuid;
   private final String mIdempotencyKey;
   private final String mPublishableApiKey;
   private final String mRequestType;
   private final String mStripeAccount;

   private RequestOptions(String var1, String var2, String var3, String var4, String var5, String var6) {
      this.mApiVersion = var1;
      this.mGuid = var2;
      this.mIdempotencyKey = var3;
      this.mPublishableApiKey = var4;
      this.mRequestType = var5;
      this.mStripeAccount = var6;
   }

   // $FF: synthetic method
   RequestOptions(String var1, String var2, String var3, String var4, String var5, String var6, Object var7) {
      this(var1, var2, var3, var4, var5, var6);
   }

   public static RequestOptions.RequestOptionsBuilder builder(String var0) {
      return builder(var0, "source");
   }

   public static RequestOptions.RequestOptionsBuilder builder(String var0, String var1) {
      return new RequestOptions.RequestOptionsBuilder(var0, var1);
   }

   public static RequestOptions.RequestOptionsBuilder builder(String var0, String var1, String var2) {
      return (new RequestOptions.RequestOptionsBuilder(var0, var2)).setStripeAccount(var1);
   }

   String getApiVersion() {
      return this.mApiVersion;
   }

   String getGuid() {
      return this.mGuid;
   }

   String getIdempotencyKey() {
      return this.mIdempotencyKey;
   }

   String getPublishableApiKey() {
      return this.mPublishableApiKey;
   }

   String getRequestType() {
      return this.mRequestType;
   }

   String getStripeAccount() {
      return this.mStripeAccount;
   }

   public static final class RequestOptionsBuilder {
      private String apiVersion;
      private String guid;
      private String idempotencyKey;
      private String publishableApiKey;
      private String requestType;
      private String stripeAccount;

      RequestOptionsBuilder(String var1, String var2) {
         this.publishableApiKey = var1;
         this.requestType = var2;
      }

      public RequestOptions build() {
         return new RequestOptions(this.apiVersion, this.guid, this.idempotencyKey, this.publishableApiKey, this.requestType, this.stripeAccount);
      }

      RequestOptions.RequestOptionsBuilder setApiVersion(String var1) {
         String var2 = var1;
         if(StripeTextUtils.isBlank(var1)) {
            var2 = null;
         }

         this.apiVersion = var2;
         return this;
      }

      RequestOptions.RequestOptionsBuilder setGuid(String var1) {
         this.guid = var1;
         return this;
      }

      RequestOptions.RequestOptionsBuilder setIdempotencyKey(String var1) {
         this.idempotencyKey = var1;
         return this;
      }

      RequestOptions.RequestOptionsBuilder setPublishableApiKey(String var1) {
         this.publishableApiKey = var1;
         return this;
      }

      RequestOptions.RequestOptionsBuilder setStripeAccount(String var1) {
         this.stripeAccount = var1;
         return this;
      }
   }

   @Retention(RetentionPolicy.SOURCE)
   public @interface RequestType {
   }
}
