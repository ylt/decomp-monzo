package com.stripe.android.net;

import com.stripe.android.exception.StripeException;
import com.stripe.android.model.Source;

interface SourceRetriever {
   Source retrieveSource(String var1, String var2, String var3) throws StripeException;
}
