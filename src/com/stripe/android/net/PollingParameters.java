package com.stripe.android.net;

@Deprecated
class PollingParameters {
   private static final long DEFAULT_TIMEOUT_MS = 10000L;
   private static final long INITIAL_DELAY_MS = 1000L;
   private static final long MAX_DELAY_MS = 15000L;
   private static final int MAX_RETRY_COUNT = 5;
   private static final long MAX_TIMEOUT_MS = 300000L;
   private static final int POLLING_MULTIPLIER = 2;
   private final long mDefaultTimeoutMs;
   private final long mInitialDelayMs;
   private final long mMaxDelayMs;
   private final int mMaxRetryCount;
   private final long mMaxTimeoutMs;
   private final int mPollingMultiplier;

   PollingParameters(long var1, long var3, long var5, int var7, long var8, int var10) {
      this.mDefaultTimeoutMs = var1;
      this.mInitialDelayMs = var3;
      this.mMaxDelayMs = var5;
      this.mMaxRetryCount = var7;
      this.mMaxTimeoutMs = var8;
      this.mPollingMultiplier = var10;
   }

   static PollingParameters generateDefaultParameters() {
      return new PollingParameters(10000L, 1000L, 15000L, 5, 300000L, 2);
   }

   long getDefaultTimeoutMs() {
      return this.mDefaultTimeoutMs;
   }

   long getInitialDelayMs() {
      return this.mInitialDelayMs;
   }

   int getInitialDelayMsInt() {
      return (int)this.mInitialDelayMs;
   }

   long getMaxDelayMs() {
      return this.mMaxDelayMs;
   }

   int getMaxRetryCount() {
      return this.mMaxRetryCount;
   }

   long getMaxTimeoutMs() {
      return this.mMaxTimeoutMs;
   }

   int getPollingMultiplier() {
      return this.mPollingMultiplier;
   }
}
