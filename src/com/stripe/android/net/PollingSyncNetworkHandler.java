package com.stripe.android.net;

import android.os.SystemClock;
import com.stripe.android.exception.StripeException;
import com.stripe.android.model.Source;

@Deprecated
class PollingSyncNetworkHandler {
   private final String mClientSecret;
   private final PollingParameters mPollingParameters;
   private final String mPublishableKey;
   private final String mSourceId;
   private SourceRetriever mSourceRetriever;
   private long mTimeOutMs;
   private PollingSyncNetworkHandler.TimeRetriever mTimeRetriever;

   PollingSyncNetworkHandler(String var1, String var2, String var3, Integer var4, SourceRetriever var5, PollingSyncNetworkHandler.TimeRetriever var6, PollingParameters var7) {
      this.mSourceId = var1;
      this.mClientSecret = var2;
      this.mPublishableKey = var3;
      this.mPollingParameters = var7;
      long var8;
      if(var4 == null) {
         var8 = this.mPollingParameters.getDefaultTimeoutMs();
      } else {
         var8 = Math.min(var4.longValue(), this.mPollingParameters.getMaxTimeoutMs());
      }

      this.mTimeOutMs = var8;
      SourceRetriever var10 = var5;
      if(var5 == null) {
         var10 = generateSourceRetriever();
      }

      this.mSourceRetriever = var10;
      PollingSyncNetworkHandler.TimeRetriever var11 = var6;
      if(var6 == null) {
         var11 = generateTimeRetriever();
      }

      this.mTimeRetriever = var11;
   }

   private static SourceRetriever generateSourceRetriever() {
      return new SourceRetriever() {
         public Source retrieveSource(String var1, String var2, String var3) throws StripeException {
            return StripeApiHandler.retrieveSource(var1, var2, var3);
         }
      };
   }

   private static PollingSyncNetworkHandler.TimeRetriever generateTimeRetriever() {
      return new PollingSyncNetworkHandler.TimeRetriever() {
         public long getCurrentTimeInMillis() {
            return SystemClock.uptimeMillis();
         }
      };
   }

   long getTimeOutMs() {
      return this.mTimeOutMs;
   }

   PollingResponse pollForSourceUpdate() {
      // $FF: Couldn't be decompiled
   }

   interface TimeRetriever {
      long getCurrentTimeInMillis();
   }
}
