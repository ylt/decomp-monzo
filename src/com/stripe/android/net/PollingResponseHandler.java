package com.stripe.android.net;

@Deprecated
public interface PollingResponseHandler {
   void onPollingResponse(PollingResponse var1);
}
