package com.stripe.android.net;

import com.stripe.android.model.BankAccount;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.android.util.StripeJsonUtils;
import com.stripe.android.util.StripeTextUtils;
import java.util.Date;
import org.json.JSONException;
import org.json.JSONObject;

public class TokenParser {
   private static final String FIELD_BANK_ACCOUNT = "bank_account";
   private static final String FIELD_CARD = "card";
   private static final String FIELD_CREATED = "created";
   private static final String FIELD_ID = "id";
   private static final String FIELD_LIVEMODE = "livemode";
   private static final String FIELD_TYPE = "type";
   private static final String FIELD_USED = "used";

   public static Token parseToken(String var0) throws JSONException {
      JSONObject var8 = new JSONObject(var0);
      String var3 = StripeJsonUtils.getString(var8, "id");
      long var1 = var8.getLong("created");
      Boolean var5 = Boolean.valueOf(var8.getBoolean("livemode"));
      String var7 = StripeTextUtils.asTokenType(StripeJsonUtils.getString(var8, "type"));
      Boolean var4 = Boolean.valueOf(var8.getBoolean("used"));
      Date var6 = new Date(Long.valueOf(var1).longValue() * 1000L);
      Token var9 = null;
      if("bank_account".equals(var7)) {
         BankAccount var10 = BankAccountParser.parseBankAccount(var8.getJSONObject("bank_account"));
         var9 = new Token(var3, var5.booleanValue(), var6, var4, var10);
      } else if("card".equals(var7)) {
         Card var11 = Card.fromJson(var8.getJSONObject("card"));
         var9 = new Token(var3, var5.booleanValue(), var6, var4, var11);
      } else if("pii".equals(var7)) {
         var9 = new Token(var3, var5.booleanValue(), var6, var4);
      }

      return var9;
   }
}
