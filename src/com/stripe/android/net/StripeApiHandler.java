package com.stripe.android.net;

import android.content.Context;
import android.os.Build.VERSION;
import android.text.TextUtils;
import com.stripe.android.exception.APIConnectionException;
import com.stripe.android.exception.APIException;
import com.stripe.android.exception.AuthenticationException;
import com.stripe.android.exception.CardException;
import com.stripe.android.exception.InvalidRequestException;
import com.stripe.android.exception.PermissionException;
import com.stripe.android.exception.RateLimitException;
import com.stripe.android.exception.StripeException;
import com.stripe.android.model.Source;
import com.stripe.android.model.SourceParams;
import com.stripe.android.model.Token;
import com.stripe.android.util.LoggingUtils;
import com.stripe.android.util.StripeNetworkUtils;
import com.stripe.android.util.StripeTextUtils;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.Security;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;
import java.util.Map.Entry;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSocketFactory;
import org.json.JSONException;
import org.json.JSONObject;

public class StripeApiHandler {
   public static final String CHARSET = "UTF-8";
   private static final String DNS_CACHE_TTL_PROPERTY_NAME = "networkaddress.cache.ttl";
   static final String GET = "GET";
   public static final String LIVE_API_BASE = "https://api.stripe.com";
   public static final String LIVE_LOGGING_BASE = "https://q.stripe.com";
   private static final String LOGGING_ENDPOINT = "https://m.stripe.com/4";
   static final String POST = "POST";
   public static final String SOURCES = "sources";
   private static final SSLSocketFactory SSL_SOCKET_FACTORY = new StripeSSLSocketFactory();
   public static final String TOKENS = "tokens";

   private static void attachPseudoCookie(HttpURLConnection var0, RequestOptions var1) {
      if(var1.getGuid() != null && !TextUtils.isEmpty(var1.getGuid())) {
         var0.setRequestProperty("Cookie", "m=" + var1.getGuid());
      }

   }

   private static HttpURLConnection createGetConnection(String var0, String var1, RequestOptions var2) throws IOException {
      HttpURLConnection var3 = createStripeConnection(formatURL(var0, var1), var2);
      var3.setRequestMethod("GET");
      return var3;
   }

   private static HttpURLConnection createPostConnection(String param0, Map param1, RequestOptions param2) throws IOException, InvalidRequestException {
      // $FF: Couldn't be decompiled
   }

   static String createQuery(Map var0) throws UnsupportedEncodingException, InvalidRequestException {
      StringBuilder var1 = new StringBuilder();
      Iterator var3 = flattenParams(var0).iterator();

      while(var3.hasNext()) {
         if(var1.length() > 0) {
            var1.append("&");
         }

         StripeApiHandler.Parameter var2 = (StripeApiHandler.Parameter)var3.next();
         var1.append(urlEncodePair(var2.key, var2.value));
      }

      return var1.toString();
   }

   @Deprecated
   public static Source createSourceOnServer(Context var0, SourceParams var1, String var2) throws AuthenticationException, InvalidRequestException, APIConnectionException, APIException {
      return createSourceOnServer((StripeNetworkUtils.UidProvider)null, var0, var1, var2, (StripeApiHandler.LoggingResponseListener)null);
   }

   @Deprecated
   public static Source createSourceOnServer(SourceParams var0, String var1) throws AuthenticationException, InvalidRequestException, APIConnectionException, APIException {
      return createSourceOnServer((SourceParams)var0, (String)var1, (StripeApiHandler.LoggingResponseListener)null);
   }

   @Deprecated
   static Source createSourceOnServer(SourceParams var0, String var1, StripeApiHandler.LoggingResponseListener var2) throws AuthenticationException, InvalidRequestException, APIConnectionException, APIException {
      Map var3 = var0.toParamMap();
      RequestOptions var4 = RequestOptions.builder(var1).build();

      Source var7;
      try {
         String var5 = var4.getPublishableApiKey();
         if(!StripeTextUtils.isBlank(var5)) {
            logApiCall(LoggingUtils.getSourceCreationParams(var5, var0.getType()), RequestOptions.builder(var1).build(), var2);
            var7 = Source.fromString(requestData("POST", getSourcesUrl(), var3, var4).getResponseBody());
            return var7;
         }
      } catch (CardException var6) {
         throw new APIException(var6.getMessage(), var6.getRequestId(), var6.getStatusCode(), var6);
      }

      var7 = null;
      return var7;
   }

   @Deprecated
   public static Source createSourceOnServer(StripeNetworkUtils.UidProvider var0, Context var1, SourceParams var2, String var3, StripeApiHandler.LoggingResponseListener var4) throws AuthenticationException, InvalidRequestException, APIConnectionException, APIException {
      return createSourceOnServer(var0, var1, var2, var3, (String)null, var4);
   }

   public static Source createSourceOnServer(StripeNetworkUtils.UidProvider var0, Context var1, SourceParams var2, String var3, String var4, StripeApiHandler.LoggingResponseListener var5) throws AuthenticationException, InvalidRequestException, APIConnectionException, APIException {
      return createSourceOnServer(var0, var1, var2, var3, var4, var5, (StripeApiHandler.StripeResponseListener)null);
   }

   static Source createSourceOnServer(StripeNetworkUtils.UidProvider param0, Context param1, SourceParams param2, String param3, String param4, StripeApiHandler.LoggingResponseListener param5, StripeApiHandler.StripeResponseListener param6) throws AuthenticationException, InvalidRequestException, APIConnectionException, APIException {
      // $FF: Couldn't be decompiled
   }

   private static HttpURLConnection createStripeConnection(String var0, RequestOptions var1) throws IOException {
      HttpURLConnection var2 = (HttpURLConnection)(new URL(var0)).openConnection();
      var2.setConnectTimeout(30000);
      var2.setReadTimeout(80000);
      var2.setUseCaches(false);
      if(urlNeedsHeaderData(var0)) {
         Iterator var4 = getHeaders(var1).entrySet().iterator();

         while(var4.hasNext()) {
            Entry var3 = (Entry)var4.next();
            var2.setRequestProperty((String)var3.getKey(), (String)var3.getValue());
         }
      }

      if(urlNeedsPseudoCookie(var0)) {
         attachPseudoCookie(var2, var1);
      }

      if(var2 instanceof HttpsURLConnection) {
         ((HttpsURLConnection)var2).setSSLSocketFactory(SSL_SOCKET_FACTORY);
      }

      return var2;
   }

   public static Token createTokenOnServer(Context var0, Map var1, RequestOptions var2, String var3, StripeApiHandler.LoggingResponseListener var4) throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
      Token var8;
      label25: {
         try {
            String var5 = var2.getPublishableApiKey();
            if(StripeTextUtils.isBlank(var5)) {
               break label25;
            }

            List var6 = (List)var1.get("product_usage");
            var1.remove("product_usage");
            setTelemetryData(var0, var4);
            logApiCall(LoggingUtils.getTokenCreationParams(var6, var5, var3), var2, var4);
         } catch (ClassCastException var7) {
            var1.remove("product_usage");
         }

         var8 = requestToken("POST", getApiUrl(), var1, var2);
         return var8;
      }

      var8 = null;
      return var8;
   }

   @Deprecated
   public static Token createTokenOnServer(Map var0, RequestOptions var1, String var2, StripeApiHandler.LoggingResponseListener var3) throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
      Token var7;
      label25: {
         try {
            String var5 = var1.getPublishableApiKey();
            if(StripeTextUtils.isBlank(var5)) {
               break label25;
            }

            List var4 = (List)var0.get("product_usage");
            var0.remove("product_usage");
            logApiCall(LoggingUtils.getTokenCreationParams(var4, var5, var2), var1, var3);
         } catch (ClassCastException var6) {
            var0.remove("product_usage");
         }

         var7 = requestToken("POST", getApiUrl(), var0, var1);
         return var7;
      }

      var7 = null;
      return var7;
   }

   private static void fireAndForgetApiCall(Map param0, String param1, String param2, RequestOptions param3, StripeApiHandler.LoggingResponseListener param4) {
      // $FF: Couldn't be decompiled
   }

   private static List flattenParams(Map var0) throws InvalidRequestException {
      return flattenParamsMap(var0, (String)null);
   }

   private static List flattenParamsList(List var0, String var1) throws InvalidRequestException {
      LinkedList var4 = new LinkedList();
      Iterator var3 = var0.iterator();
      String var2 = String.format("%s[]", new Object[]{var1});
      if(var0.isEmpty()) {
         var4.add(new StripeApiHandler.Parameter(var1, ""));
      } else {
         while(var3.hasNext()) {
            var4.addAll(flattenParamsValue(var3.next(), var2));
         }
      }

      return var4;
   }

   private static List flattenParamsMap(Map var0, String var1) throws InvalidRequestException {
      LinkedList var3 = new LinkedList();
      Object var5;
      String var7;
      if(var0 != null) {
         for(Iterator var4 = var0.entrySet().iterator(); var4.hasNext(); var3.addAll(flattenParamsValue(var5, var7))) {
            Entry var6 = (Entry)var4.next();
            String var2 = (String)var6.getKey();
            var5 = var6.getValue();
            var7 = var2;
            if(var1 != null) {
               var7 = String.format("%s[%s]", new Object[]{var1, var2});
            }
         }
      }

      return var3;
   }

   private static List flattenParamsValue(Object var0, String var1) throws InvalidRequestException {
      if(var0 instanceof Map) {
         var0 = flattenParamsMap((Map)var0, var1);
      } else if(var0 instanceof List) {
         var0 = flattenParamsList((List)var0, var1);
      } else {
         if("".equals(var0)) {
            throw new InvalidRequestException("You cannot set '" + var1 + "' to an empty string. We interpret empty strings as null in requests. You may set '" + var1 + "' to null to delete the property.", var1, (String)null, Integer.valueOf(0), (Throwable)null);
         }

         if(var0 == null) {
            var0 = new LinkedList();
            ((List)var0).add(new StripeApiHandler.Parameter(var1, ""));
         } else {
            LinkedList var2 = new LinkedList();
            var2.add(new StripeApiHandler.Parameter(var1, var0.toString()));
            var0 = var2;
         }
      }

      return (List)var0;
   }

   private static String formatURL(String var0, String var1) {
      String var2 = var0;
      if(var1 != null) {
         if(var1.isEmpty()) {
            var2 = var0;
         } else {
            if(var0.contains("?")) {
               var2 = "&";
            } else {
               var2 = "?";
            }

            var2 = String.format("%s%s%s", new Object[]{var0, var2, var1});
         }
      }

      return var2;
   }

   static String getApiUrl() {
      return String.format(Locale.ENGLISH, "%s/v1/%s", new Object[]{"https://api.stripe.com", "tokens"});
   }

   private static String getContentType(RequestOptions var0) {
      String var1;
      if("json_data".equals(var0.getRequestType())) {
         var1 = String.format("application/json; charset=%s", new Object[]{"UTF-8"});
      } else {
         var1 = String.format("application/x-www-form-urlencoded;charset=%s", new Object[]{"UTF-8"});
      }

      return var1;
   }

   static Map getHeaders(RequestOptions var0) {
      HashMap var2 = new HashMap();
      var2.put("Accept-Charset", "UTF-8");
      var2.put("Accept", "application/json");
      var2.put("User-Agent", String.format("Stripe/v1 AndroidBindings/%s", new Object[]{"4.1.3"}));
      if(var0 != null) {
         var2.put("Authorization", String.format(Locale.ENGLISH, "Bearer %s", new Object[]{var0.getPublishableApiKey()}));
      }

      HashMap var1 = new HashMap();
      var1.put("java.version", System.getProperty("java.version"));
      var1.put("os.name", "android");
      var1.put("os.version", String.valueOf(VERSION.SDK_INT));
      var1.put("bindings.version", "4.1.3");
      var1.put("lang", "Java");
      var1.put("publisher", "Stripe");
      var2.put("X-Stripe-Client-User-Agent", (new JSONObject(var1)).toString());
      if(var0 != null && var0.getApiVersion() != null) {
         var2.put("Stripe-Version", var0.getApiVersion());
      }

      if(var0 != null && var0.getIdempotencyKey() != null) {
         var2.put("Idempotency-Key", var0.getIdempotencyKey());
      }

      if(var0 != null && var0.getStripeAccount() != null) {
         var2.put("Stripe-Account", var0.getStripeAccount());
      }

      return var2;
   }

   private static byte[] getOutputBytes(Map param0, RequestOptions param1) throws InvalidRequestException {
      // $FF: Couldn't be decompiled
   }

   private static String getResponseBody(InputStream var0) throws IOException {
      String var1 = (new Scanner(var0, "UTF-8")).useDelimiter("\\A").next();
      var0.close();
      return var1;
   }

   static String getRetrieveSourceApiUrl(String var0) {
      return String.format(Locale.ENGLISH, "%s/%s", new Object[]{getSourcesUrl(), var0});
   }

   static String getRetrieveTokenApiUrl(String var0) {
      return String.format("%s/%s", new Object[]{getApiUrl(), var0});
   }

   static String getSourcesUrl() {
      return String.format(Locale.ENGLISH, "%s/v1/%s", new Object[]{"https://api.stripe.com", "sources"});
   }

   private static StripeResponse getStripeResponse(String param0, String param1, Map param2, RequestOptions param3) throws InvalidRequestException, APIConnectionException, APIException {
      // $FF: Couldn't be decompiled
   }

   private static void handleAPIError(String var0, int var1, String var2) throws InvalidRequestException, AuthenticationException, CardException, APIException {
      ErrorParser.StripeError var3 = ErrorParser.parseError(var0);
      switch(var1) {
      case 400:
         throw new InvalidRequestException(var3.message, var3.param, var2, Integer.valueOf(var1), (Throwable)null);
      case 401:
         throw new AuthenticationException(var3.message, var2, Integer.valueOf(var1));
      case 402:
         throw new CardException(var3.message, var2, var3.code, var3.param, var3.decline_code, var3.charge, Integer.valueOf(var1), (Throwable)null);
      case 403:
         throw new PermissionException(var3.message, var2, Integer.valueOf(var1));
      case 404:
         throw new InvalidRequestException(var3.message, var3.param, var2, Integer.valueOf(var1), (Throwable)null);
      case 429:
         throw new RateLimitException(var3.message, var3.param, var2, Integer.valueOf(var1), (Throwable)null);
      default:
         throw new APIException(var3.message, var2, Integer.valueOf(var1), (Throwable)null);
      }
   }

   public static void logApiCall(Map var0, RequestOptions var1, StripeApiHandler.LoggingResponseListener var2) {
      if(var1 != null && (var2 == null || var2.shouldLogTest()) && !var1.getPublishableApiKey().trim().isEmpty()) {
         fireAndForgetApiCall(var0, "https://q.stripe.com", "GET", var1, var2);
      }

   }

   public static void pollSource(String var0, String var1, String var2, PollingResponseHandler var3, Integer var4) {
      (new PollingNetworkHandler(var0, var1, var2, var3, var4, (SourceRetriever)null, PollingParameters.generateDefaultParameters())).start();
   }

   public static PollingResponse pollSourceSynchronous(String var0, String var1, String var2, Integer var3) {
      return (new PollingSyncNetworkHandler(var0, var1, var2, var3, (SourceRetriever)null, (PollingSyncNetworkHandler.TimeRetriever)null, PollingParameters.generateDefaultParameters())).pollForSourceUpdate();
   }

   private static StripeResponse requestData(String var0, String var1, Map var2, RequestOptions var3) throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
      Object var7 = null;
      StripeResponse var10;
      if(var3 == null) {
         var10 = null;
      } else {
         String var5;
         Boolean var6;
         label55: {
            label54: {
               label53: {
                  try {
                     var5 = Security.getProperty("networkaddress.cache.ttl");
                  } catch (SecurityException var9) {
                     var5 = null;
                     break label53;
                  }

                  try {
                     Security.setProperty("networkaddress.cache.ttl", "0");
                     break label54;
                  } catch (SecurityException var8) {
                     ;
                  }
               }

               var6 = Boolean.valueOf(false);
               break label55;
            }

            var6 = Boolean.valueOf(true);
         }

         if(var3.getPublishableApiKey().trim().isEmpty()) {
            throw new AuthenticationException("No API key provided. (HINT: set your API key using 'Stripe.apiKey = <API-KEY>'. You can generate API keys from the Stripe web interface. See https://stripe.com/api for details or email support@stripe.com if you have questions.", (String)null, Integer.valueOf(0));
         }

         StripeResponse var13 = getStripeResponse(var0, var1, var2, var3);
         int var4 = var13.getResponseCode();
         String var14 = var13.getResponseBody();
         Map var11 = var13.getResponseHeaders();
         List var12;
         if(var11 == null) {
            var12 = null;
         } else {
            var12 = (List)var11.get("Request-Id");
         }

         var1 = (String)var7;
         if(var12 != null) {
            var1 = (String)var7;
            if(var12.size() > 0) {
               var1 = (String)var12.get(0);
            }
         }

         if(var4 < 200 || var4 >= 300) {
            handleAPIError(var14, var4, var1);
         }

         if(var6.booleanValue()) {
            if(var5 == null) {
               Security.setProperty("networkaddress.cache.ttl", "-1");
            } else {
               Security.setProperty("networkaddress.cache.ttl", var5);
            }
         }

         var10 = var13;
      }

      return var10;
   }

   private static Token requestToken(String var0, String var1, Map var2, RequestOptions var3) throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
      Token var5;
      try {
         var5 = TokenParser.parseToken(requestData(var0, var1, var2, var3).getResponseBody());
      } catch (JSONException var4) {
         var5 = null;
      }

      return var5;
   }

   public static Source retrieveSource(String var0, String var1, String var2) throws AuthenticationException, InvalidRequestException, APIConnectionException, APIException {
      Map var5 = SourceParams.createRetrieveSourceParams(var1);
      RequestOptions var6 = RequestOptions.builder(var2).build();

      try {
         Source var4 = Source.fromString(requestData("GET", getRetrieveSourceApiUrl(var0), var5, var6).getResponseBody());
         return var4;
      } catch (CardException var3) {
         throw new APIException(var3.getMessage(), var3.getRequestId(), var3.getStatusCode(), var3);
      }
   }

   public static Token retrieveTokenFromServer(RequestOptions var0, String var1) throws AuthenticationException, InvalidRequestException, APIConnectionException, APIException {
      try {
         Token var3 = requestToken("GET", getRetrieveTokenApiUrl(var1), (Map)null, var0);
         return var3;
      } catch (CardException var2) {
         throw new APIException(var2.getMessage(), var2.getRequestId(), var2.getStatusCode(), var2);
      }
   }

   public static void setTelemetryData(Context var0, StripeApiHandler.LoggingResponseListener var1) {
      Map var2 = TelemetryClientUtil.createTelemetryMap(var0);
      StripeNetworkUtils.removeNullAndEmptyParams(var2);
      if(var1 == null || var1.shouldLogTest()) {
         fireAndForgetApiCall(var2, "https://m.stripe.com/4", "POST", RequestOptions.builder((String)null, "json_data").setGuid(TelemetryClientUtil.getHashedId(var0)).build(), var1);
      }

   }

   private static String urlEncode(String var0) throws UnsupportedEncodingException {
      if(var0 == null) {
         var0 = null;
      } else {
         var0 = URLEncoder.encode(var0, "UTF-8");
      }

      return var0;
   }

   private static String urlEncodePair(String var0, String var1) throws UnsupportedEncodingException {
      return String.format("%s=%s", new Object[]{urlEncode(var0), urlEncode(var1)});
   }

   private static boolean urlNeedsHeaderData(String var0) {
      boolean var1;
      if(!var0.startsWith("https://api.stripe.com") && !var0.startsWith("https://q.stripe.com")) {
         var1 = false;
      } else {
         var1 = true;
      }

      return var1;
   }

   private static boolean urlNeedsPseudoCookie(String var0) {
      return var0.startsWith("https://m.stripe.com/4");
   }

   public interface LoggingResponseListener {
      void onLoggingResponse(StripeResponse var1);

      void onStripeException(StripeException var1);

      boolean shouldLogTest();
   }

   private static final class Parameter {
      public final String key;
      public final String value;

      public Parameter(String var1, String var2) {
         this.key = var1;
         this.value = var2;
      }
   }

   interface StripeResponseListener {
      void onStripeResponse(StripeResponse var1);
   }
}
