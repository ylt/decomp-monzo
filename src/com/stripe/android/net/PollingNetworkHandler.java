package com.stripe.android.net;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import com.stripe.android.exception.StripeException;
import com.stripe.android.model.Source;

@Deprecated
class PollingNetworkHandler {
   private static final int ERROR = -1;
   private static final int EXPIRED = -2;
   private static final int FAILURE = 3;
   private static final int PENDING = 2;
   private static final int SUCCESS = 1;
   private final String mClientSecret;
   private final boolean mIsInSingleThreadMode;
   private Source mLatestRetrievedSource;
   private Handler mNetworkHandler;
   private final PollingParameters mPollingParameters;
   private final String mPublishableKey;
   private int mRetryCount;
   private final String mSourceId;
   private SourceRetriever mSourceRetriever;
   private final long mTimeoutMs;
   private Handler mUiHandler;
   private final Runnable pollRunnable = new Runnable() {
      public void run() {
         // $FF: Couldn't be decompiled
      }
   };

   PollingNetworkHandler(String var1, String var2, String var3, final PollingResponseHandler var4, Integer var5, SourceRetriever var6, PollingParameters var7) {
      this.mSourceId = var1;
      this.mClientSecret = var2;
      this.mPublishableKey = var3;
      boolean var8;
      if(var6 != null) {
         var8 = true;
      } else {
         var8 = false;
      }

      this.mIsInSingleThreadMode = var8;
      this.mPollingParameters = var7;
      SourceRetriever var11 = var6;
      if(var6 == null) {
         var11 = new SourceRetriever() {
            public Source retrieveSource(String var1, String var2, String var3) throws StripeException {
               return StripeApiHandler.retrieveSource(var1, var2, var3);
            }
         };
      }

      this.mSourceRetriever = var11;
      long var9;
      if(var5 == null) {
         var9 = this.mPollingParameters.getDefaultTimeoutMs();
      } else {
         var9 = Math.min(var5.longValue(), this.mPollingParameters.getMaxTimeoutMs());
      }

      this.mTimeoutMs = var9;
      this.mRetryCount = 0;
      this.mUiHandler = new Handler(Looper.getMainLooper()) {
         int delayMs;
         boolean terminated;

         {
            this.delayMs = PollingNetworkHandler.this.mPollingParameters.getInitialDelayMsInt();
            this.terminated = false;
         }

         public void handleMessage(Message var1) {
            super.handleMessage(var1);
            if(!this.terminated) {
               switch(var1.what) {
               case -2:
                  this.terminated = true;
                  var4.onPollingResponse(new PollingResponse(PollingNetworkHandler.this.mLatestRetrievedSource, false, true));
                  this.removeCallbacksAndMessages((Object)null);
                  break;
               case -1:
                  PollingNetworkHandler.access$708(PollingNetworkHandler.this);
                  if(PollingNetworkHandler.this.mRetryCount >= PollingNetworkHandler.this.mPollingParameters.getMaxRetryCount()) {
                     this.terminated = true;
                     var4.onPollingResponse(new PollingResponse(PollingNetworkHandler.this.mLatestRetrievedSource, (StripeException)var1.obj));
                     this.removeCallbacksAndMessages((Object)null);
                  } else {
                     this.delayMs = Math.min(this.delayMs * PollingNetworkHandler.this.mPollingParameters.getPollingMultiplier(), (int)PollingNetworkHandler.this.mPollingParameters.getMaxDelayMs());
                     PollingNetworkHandler.this.mNetworkHandler.sendEmptyMessage(this.delayMs);
                  }
               case 0:
               default:
                  break;
               case 1:
                  this.terminated = true;
                  var4.onPollingResponse(new PollingResponse((Source)var1.obj, true, false));
                  this.removeCallbacksAndMessages((Object)null);
                  break;
               case 2:
                  PollingNetworkHandler.this.mRetryCount = 0;
                  this.delayMs = PollingNetworkHandler.this.mPollingParameters.getInitialDelayMsInt();
                  PollingNetworkHandler.this.mNetworkHandler.sendEmptyMessage(this.delayMs);
                  break;
               case 3:
                  this.terminated = true;
                  var4.onPollingResponse(new PollingResponse((Source)var1.obj, false, false));
                  this.removeCallbacksAndMessages((Object)null);
               }
            }

         }
      };
      HandlerThread var12 = null;
      if(!this.mIsInSingleThreadMode) {
         var12 = new HandlerThread("Network Handler Thread");
         var12.start();
      }

      final Looper var13;
      if(this.mIsInSingleThreadMode) {
         var13 = Looper.getMainLooper();
      } else {
         var13 = var12.getLooper();
      }

      this.mNetworkHandler = new Handler(var13) {
         boolean terminated = false;

         public void handleMessage(Message var1) {
            super.handleMessage(var1);
            if(!this.terminated) {
               if(var1.what >= 0) {
                  this.postDelayed(PollingNetworkHandler.this.pollRunnable, (long)var1.what);
               } else {
                  this.terminated = true;
                  if(!PollingNetworkHandler.this.mIsInSingleThreadMode) {
                     var13.quit();
                  }

                  PollingNetworkHandler.this.mUiHandler.removeMessages(1);
                  PollingNetworkHandler.this.mUiHandler.removeMessages(2);
                  PollingNetworkHandler.this.mUiHandler.removeMessages(3);
                  this.removeCallbacks(PollingNetworkHandler.this.pollRunnable);
               }
            }

         }
      };
   }

   // $FF: synthetic method
   static String access$000(PollingNetworkHandler var0) {
      return var0.mSourceId;
   }

   // $FF: synthetic method
   static String access$100(PollingNetworkHandler var0) {
      return var0.mClientSecret;
   }

   // $FF: synthetic method
   static String access$200(PollingNetworkHandler var0) {
      return var0.mPublishableKey;
   }

   // $FF: synthetic method
   static SourceRetriever access$300(PollingNetworkHandler var0) {
      return var0.mSourceRetriever;
   }

   // $FF: synthetic method
   static Source access$402(PollingNetworkHandler var0, Source var1) {
      var0.mLatestRetrievedSource = var1;
      return var1;
   }

   // $FF: synthetic method
   static int access$708(PollingNetworkHandler var0) {
      int var1 = var0.mRetryCount;
      var0.mRetryCount = var1 + 1;
      return var1;
   }

   int getRetryCount() {
      return this.mRetryCount;
   }

   long getTimeoutMs() {
      return this.mTimeoutMs;
   }

   void setSourceRetriever(SourceRetriever var1) {
      this.mSourceRetriever = var1;
   }

   void start() {
      this.mNetworkHandler.post(this.pollRunnable);
      this.mNetworkHandler.sendEmptyMessageDelayed(-2, this.mTimeoutMs);
      this.mUiHandler.sendEmptyMessageDelayed(-2, this.mTimeoutMs);
   }
}
