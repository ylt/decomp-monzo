package com.stripe.android.net;

import com.stripe.android.exception.StripeException;
import com.stripe.android.model.Source;

@Deprecated
public class PollingResponse {
   private boolean mIsExpired;
   private boolean mIsSuccess;
   private Source mSource;
   private StripeException mStripeException;

   PollingResponse(Source var1, StripeException var2) {
      this.mSource = var1;
      this.mStripeException = var2;
      this.mIsExpired = false;
      this.mIsSuccess = false;
   }

   PollingResponse(Source var1, boolean var2, boolean var3) {
      this.mSource = var1;
      this.mIsExpired = var3;
      this.mIsSuccess = var2;
   }

   public Source getSource() {
      return this.mSource;
   }

   public StripeException getStripeException() {
      return this.mStripeException;
   }

   public boolean isExpired() {
      return this.mIsExpired;
   }

   public boolean isSuccess() {
      return this.mIsSuccess;
   }
}
