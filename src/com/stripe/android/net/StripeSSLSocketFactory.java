package com.stripe.android.net;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashSet;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

public class StripeSSLSocketFactory extends SSLSocketFactory {
   private static final String TLSv11Proto = "TLSv1.1";
   private static final String TLSv12Proto = "TLSv1.2";
   private final boolean tlsv11Supported;
   private final boolean tlsv12Supported;
   private final SSLSocketFactory under;

   public StripeSSLSocketFactory() {
      boolean var4 = false;
      super();
      this.under = HttpsURLConnection.getDefaultSSLSocketFactory();

      String[] var6;
      try {
         var6 = SSLContext.getDefault().getSupportedSSLParameters().getProtocols();
      } catch (NoSuchAlgorithmException var8) {
         var6 = new String[0];
      }

      int var2 = var6.length;
      int var1 = 0;

      boolean var3;
      boolean var5;
      for(var3 = false; var1 < var2; var3 = var5) {
         String var7 = var6[var1];
         if(var7.equals("TLSv1.1")) {
            var5 = true;
         } else {
            var5 = var3;
            if(var7.equals("TLSv1.2")) {
               var4 = true;
               var5 = var3;
            }
         }

         ++var1;
      }

      this.tlsv11Supported = var3;
      this.tlsv12Supported = var4;
   }

   private Socket fixupSocket(Socket var1) {
      if(var1 instanceof SSLSocket) {
         var1 = (SSLSocket)var1;
         HashSet var2 = new HashSet(Arrays.asList(((SSLSocket)var1).getEnabledProtocols()));
         if(this.tlsv11Supported) {
            var2.add("TLSv1.1");
         }

         if(this.tlsv12Supported) {
            var2.add("TLSv1.2");
         }

         ((SSLSocket)var1).setEnabledProtocols((String[])var2.toArray(new String[0]));
      }

      return (Socket)var1;
   }

   public Socket createSocket(String var1, int var2) throws IOException {
      return this.fixupSocket(this.under.createSocket(var1, var2));
   }

   public Socket createSocket(String var1, int var2, InetAddress var3, int var4) throws IOException {
      return this.fixupSocket(this.under.createSocket(var1, var2, var3, var4));
   }

   public Socket createSocket(InetAddress var1, int var2) throws IOException {
      return this.fixupSocket(this.under.createSocket(var1, var2));
   }

   public Socket createSocket(InetAddress var1, int var2, InetAddress var3, int var4) throws IOException {
      return this.fixupSocket(this.under.createSocket(var1, var2, var3, var4));
   }

   public Socket createSocket(Socket var1, String var2, int var3, boolean var4) throws IOException {
      return this.fixupSocket(this.under.createSocket(var1, var2, var3, var4));
   }

   public String[] getDefaultCipherSuites() {
      return this.under.getDefaultCipherSuites();
   }

   public String[] getSupportedCipherSuites() {
      return this.under.getSupportedCipherSuites();
   }
}
