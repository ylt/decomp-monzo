package com.stripe.android.net;

import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build;
import android.os.Build.VERSION;
import android.provider.Settings.Secure;
import com.stripe.android.util.StripeTextUtils;
import java.math.BigDecimal;
import java.math.MathContext;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

class TelemetryClientUtil {
   private static Map createSingleValuePair(Object var0) {
      HashMap var1 = new HashMap();
      var1.put("v", var0);
      return var1;
   }

   static Map createTelemetryMap(Context var0) {
      HashMap var1 = new HashMap();
      HashMap var3 = new HashMap();
      HashMap var2 = new HashMap();
      var1.put("v2", Integer.valueOf(1));
      var1.put("tag", "4.1.3");
      var1.put("src", "android-sdk");
      var3.put("c", createSingleValuePair(Locale.getDefault().toString()));
      var3.put("d", createSingleValuePair(getAndroidVersionString()));
      var3.put("f", createSingleValuePair(getScreen(var0)));
      var3.put("g", createSingleValuePair(getTimeZoneString()));
      var1.put("a", var3);
      var2.put("d", getHashedMuid(var0));
      String var5 = getPackageName(var0);
      var2.put("k", var5);
      var2.put("o", VERSION.RELEASE);
      var2.put("p", Integer.valueOf(VERSION.SDK_INT));
      var2.put("q", Build.MANUFACTURER);
      var2.put("r", Build.BRAND);
      var2.put("s", Build.MODEL);
      var2.put("t", Build.TAGS);
      if(var0.getPackageName() != null) {
         try {
            var2.put("l", var0.getPackageManager().getPackageInfo(var5, 0).versionName);
         } catch (NameNotFoundException var4) {
            ;
         }
      }

      var1.put("b", var2);
      return var1;
   }

   private static String getAndroidVersionString() {
      StringBuilder var0 = new StringBuilder();
      var0.append("Android").append(" ").append(VERSION.RELEASE).append(" ").append(VERSION.CODENAME).append(" ").append(VERSION.SDK_INT);
      return var0.toString();
   }

   static String getHashedId(Context var0) {
      String var2 = Secure.getString(var0.getContentResolver(), "android_id");
      if(StripeTextUtils.isBlank(var2)) {
         var2 = "";
      } else {
         String var1 = StripeTextUtils.shaHashInput(var2);
         var2 = var1;
         if(var1 == null) {
            var2 = "";
         }
      }

      return var2;
   }

   private static String getHashedMuid(Context var0) {
      String var1 = getHashedId(var0);
      String var2 = getPackageName(var0);
      var1 = StripeTextUtils.shaHashInput(var2 + var1);
      var2 = var1;
      if(var1 == null) {
         var2 = "";
      }

      return var2;
   }

   private static String getPackageName(Context var0) {
      String var1;
      if(var0.getApplicationContext() != null && var0.getApplicationContext().getPackageName() != null) {
         var1 = var0.getApplicationContext().getPackageName();
      } else {
         var1 = "";
      }

      return var1;
   }

   private static String getScreen(Context var0) {
      String var4;
      if(var0.getResources() == null) {
         var4 = "";
      } else {
         int var3 = var0.getResources().getDisplayMetrics().widthPixels;
         int var2 = var0.getResources().getDisplayMetrics().heightPixels;
         int var1 = var0.getResources().getDisplayMetrics().densityDpi;
         var4 = String.format(Locale.ENGLISH, "%dw_%dh_%ddpi", new Object[]{Integer.valueOf(var3), Integer.valueOf(var2), Integer.valueOf(var1)});
      }

      return var4;
   }

   private static String getTimeZoneString() {
      int var0 = (int)TimeUnit.MINUTES.convert((long)TimeZone.getDefault().getRawOffset(), TimeUnit.MILLISECONDS);
      String var1;
      if(var0 % 60 == 0) {
         var1 = String.valueOf(var0 / 60);
      } else {
         var1 = (new BigDecimal(var0)).setScale(2, 6).divide(new BigDecimal(60), new MathContext(2)).setScale(2, 6).toString();
      }

      return var1;
   }
}
