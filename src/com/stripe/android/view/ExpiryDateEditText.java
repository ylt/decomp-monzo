package com.stripe.android.view;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import com.stripe.android.util.DateUtils;

public class ExpiryDateEditText extends StripeEditText {
   static final int INVALID_INPUT = -1;
   private static final int MAX_INPUT_LENGTH = 5;
   private ExpiryDateEditText.ExpiryDateEditListener mExpiryDateEditListener;
   private boolean mIsDateValid;

   public ExpiryDateEditText(Context var1) {
      super(var1);
      this.listenForTextChanges();
   }

   public ExpiryDateEditText(Context var1, AttributeSet var2) {
      super(var1, var2);
      this.listenForTextChanges();
   }

   public ExpiryDateEditText(Context var1, AttributeSet var2, int var3) {
      super(var1, var2, var3);
      this.listenForTextChanges();
   }

   private void listenForTextChanges() {
      this.addTextChangedListener(new TextWatcher() {
         boolean ignoreChanges = false;
         int latestChangeStart;
         int latestInsertionSize;
         String[] parts = new String[2];

         public void afterTextChanged(Editable var1) {
            boolean var2;
            if(this.parts[0].length() == 2 && !DateUtils.isValidMonth(this.parts[0])) {
               var2 = true;
            } else {
               var2 = false;
            }

            boolean var3;
            if(this.parts[0].length() == 2 && this.parts[1].length() == 2) {
               boolean var4 = ExpiryDateEditText.this.mIsDateValid;
               ExpiryDateEditText.this.updateInputValues(this.parts);
               if(!ExpiryDateEditText.this.mIsDateValid) {
                  var2 = true;
               } else {
                  var2 = false;
               }

               var3 = var2;
               if(!var4) {
                  var3 = var2;
                  if(ExpiryDateEditText.this.mIsDateValid) {
                     var3 = var2;
                     if(ExpiryDateEditText.this.mExpiryDateEditListener != null) {
                        ExpiryDateEditText.this.mExpiryDateEditListener.onExpiryDateComplete();
                        var3 = var2;
                     }
                  }
               }
            } else {
               ExpiryDateEditText.this.mIsDateValid = false;
               var3 = var2;
            }

            ExpiryDateEditText.this.setShouldShowError(var3);
         }

         public void beforeTextChanged(CharSequence var1, int var2, int var3, int var4) {
            if(!this.ignoreChanges) {
               this.latestChangeStart = var2;
               this.latestInsertionSize = var4;
            }

         }

         public void onTextChanged(CharSequence var1, int var2, int var3, int var4) {
            if(!this.ignoreChanges) {
               String var5 = var1.toString().replaceAll("/", "");
               String var6;
               if(var5.length() == 1 && this.latestChangeStart == 0 && this.latestInsertionSize == 1) {
                  char var7 = var5.charAt(0);
                  var6 = var5;
                  if(var7 != 48) {
                     var6 = var5;
                     if(var7 != 49) {
                        var6 = "0" + var5;
                        ++this.latestInsertionSize;
                     }
                  }
               } else {
                  var6 = var5;
                  if(var5.length() == 2) {
                     var6 = var5;
                     if(this.latestChangeStart == 2) {
                        var6 = var5;
                        if(this.latestInsertionSize == 0) {
                           var6 = var5.substring(0, 1);
                        }
                     }
                  }
               }

               this.parts = DateUtils.separateDateStringParts(var6);
               boolean var8;
               if(!DateUtils.isValidMonth(this.parts[0])) {
                  var8 = true;
               } else {
                  var8 = false;
               }

               StringBuilder var9 = new StringBuilder();
               var9.append(this.parts[0]);
               if(this.parts[0].length() == 2 && this.latestInsertionSize > 0 && !var8 || var6.length() > 2) {
                  var9.append("/");
               }

               var9.append(this.parts[1]);
               var6 = var9.toString();
               var2 = ExpiryDateEditText.this.updateSelectionIndex(var6.length(), this.latestChangeStart, this.latestInsertionSize);
               this.ignoreChanges = true;
               ExpiryDateEditText.this.setText(var6);
               ExpiryDateEditText.this.setSelection(var2);
               this.ignoreChanges = false;
            }

         }
      });
   }

   private void updateInputValues(String[] var1) {
      int var3 = -1;
      int var2;
      if(var1[0].length() != 2) {
         var2 = -1;
      } else {
         try {
            var2 = Integer.parseInt(var1[0]);
         } catch (NumberFormatException var6) {
            var2 = -1;
         }
      }

      if(var1[1].length() == 2) {
         label21: {
            int var4;
            try {
               var4 = DateUtils.convertTwoDigitYearToFour(Integer.parseInt(var1[1]));
            } catch (NumberFormatException var7) {
               break label21;
            }

            var3 = var4;
         }
      }

      this.mIsDateValid = DateUtils.isExpiryDataValid(var2, var3);
   }

   public int[] getValidDateFields() {
      int[] var3 = null;
      if(this.mIsDateValid) {
         String[] var4 = DateUtils.separateDateStringParts(this.getText().toString().replaceAll("/", ""));

         int var1;
         int var2;
         try {
            var2 = Integer.parseInt(var4[0]);
            var1 = DateUtils.convertTwoDigitYearToFour(Integer.parseInt(var4[1]));
         } catch (NumberFormatException var5) {
            return var3;
         }

         var3 = new int[]{var2, var1};
      }

      return var3;
   }

   public boolean isDateValid() {
      return this.mIsDateValid;
   }

   public void setExpiryDateEditListener(ExpiryDateEditText.ExpiryDateEditListener var1) {
      this.mExpiryDateEditListener = var1;
   }

   int updateSelectionIndex(int var1, int var2, int var3) {
      boolean var6 = false;
      byte var5;
      if(var2 <= 2 && var2 + var3 >= 2) {
         var5 = 1;
      } else {
         var5 = 0;
      }

      boolean var4 = var6;
      if(var3 == 0) {
         var4 = var6;
         if(var2 == 3) {
            var4 = true;
         }
      }

      var3 = var2 + var3 + var5;
      var2 = var3;
      if(var4) {
         var2 = var3;
         if(var3 > 0) {
            var2 = var3 - 1;
         }
      }

      var3 = var1;
      if(var2 <= var1) {
         var3 = var2;
      }

      return var3;
   }

   interface ExpiryDateEditListener {
      void onExpiryDateComplete();
   }
}
