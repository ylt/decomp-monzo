package com.stripe.android.view;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources.Theme;
import android.graphics.Color;
import android.os.Build.VERSION;
import android.support.v7.widget.n;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputConnectionWrapper;
import com.stripe.android.R;

public class StripeEditText extends n {
   private StripeEditText.AfterTextChangedListener mAfterTextChangedListener;
   private ColorStateList mCachedColorStateList;
   private int mDefaultErrorColorResId;
   private StripeEditText.DeleteEmptyListener mDeleteEmptyListener;
   private int mErrorColor;
   private boolean mShouldShowError;

   public StripeEditText(Context var1) {
      super(var1);
      this.initView();
   }

   public StripeEditText(Context var1, AttributeSet var2) {
      super(var1, var2);
      this.initView();
   }

   public StripeEditText(Context var1, AttributeSet var2, int var3) {
      super(var1, var2, var3);
      this.initView();
   }

   private void determineDefaultErrorColor() {
      this.mCachedColorStateList = this.getTextColors();
      if(isColorDark(this.mCachedColorStateList.getDefaultColor())) {
         this.mDefaultErrorColorResId = R.color.error_text_light_theme;
      } else {
         this.mDefaultErrorColorResId = R.color.error_text_dark_theme;
      }

   }

   private void initView() {
      this.listenForTextChanges();
      this.listenForDeleteEmpty();
      this.determineDefaultErrorColor();
      this.mCachedColorStateList = this.getTextColors();
   }

   static boolean isColorDark(int var0) {
      boolean var1;
      if((0.299D * (double)Color.red(var0) + 0.587D * (double)Color.green(var0) + 0.114D * (double)Color.blue(var0)) / 255.0D > 0.5D) {
         var1 = false;
      } else {
         var1 = true;
      }

      return var1;
   }

   private void listenForDeleteEmpty() {
      this.setOnKeyListener(new OnKeyListener() {
         public boolean onKey(View var1, int var2, KeyEvent var3) {
            if(var2 == 67 && var3.getAction() == 0 && StripeEditText.this.mDeleteEmptyListener != null && StripeEditText.this.length() == 0) {
               StripeEditText.this.mDeleteEmptyListener.onDeleteEmpty();
            }

            return false;
         }
      });
   }

   private void listenForTextChanges() {
      this.addTextChangedListener(new TextWatcher() {
         public void afterTextChanged(Editable var1) {
            if(StripeEditText.this.mAfterTextChangedListener != null) {
               StripeEditText.this.mAfterTextChangedListener.onTextChanged(var1.toString());
            }

         }

         public void beforeTextChanged(CharSequence var1, int var2, int var3, int var4) {
         }

         public void onTextChanged(CharSequence var1, int var2, int var3, int var4) {
         }
      });
   }

   public ColorStateList getCachedColorStateList() {
      return this.mCachedColorStateList;
   }

   public int getDefaultErrorColorInt() {
      this.determineDefaultErrorColor();
      int var1;
      if(VERSION.SDK_INT >= 23) {
         var1 = this.getResources().getColor(this.mDefaultErrorColorResId, (Theme)null);
      } else {
         var1 = this.getResources().getColor(this.mDefaultErrorColorResId);
      }

      return var1;
   }

   public boolean getShouldShowError() {
      return this.mShouldShowError;
   }

   public InputConnection onCreateInputConnection(EditorInfo var1) {
      return new StripeEditText.SoftDeleteInputConnection(super.onCreateInputConnection(var1), true);
   }

   public void setAfterTextChangedListener(StripeEditText.AfterTextChangedListener var1) {
      this.mAfterTextChangedListener = var1;
   }

   public void setDeleteEmptyListener(StripeEditText.DeleteEmptyListener var1) {
      this.mDeleteEmptyListener = var1;
   }

   public void setErrorColor(int var1) {
      this.mErrorColor = var1;
   }

   public void setShouldShowError(boolean var1) {
      this.mShouldShowError = var1;
      if(this.mShouldShowError) {
         this.setTextColor(this.mErrorColor);
      } else {
         this.setTextColor(this.mCachedColorStateList);
      }

      this.refreshDrawableState();
   }

   public interface AfterTextChangedListener {
      void onTextChanged(String var1);
   }

   public interface DeleteEmptyListener {
      void onDeleteEmpty();
   }

   private class SoftDeleteInputConnection extends InputConnectionWrapper {
      public SoftDeleteInputConnection(InputConnection var2, boolean var3) {
         super(var2, var3);
      }

      public boolean deleteSurroundingText(int var1, int var2) {
         if(this.getTextBeforeCursor(1, 0).length() == 0 && StripeEditText.this.mDeleteEmptyListener != null) {
            StripeEditText.this.mDeleteEmptyListener.onDeleteEmpty();
         }

         return super.deleteSurroundingText(var1, var2);
      }
   }
}
