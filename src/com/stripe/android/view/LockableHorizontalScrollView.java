package com.stripe.android.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.HorizontalScrollView;

public class LockableHorizontalScrollView extends HorizontalScrollView {
   private LockableHorizontalScrollView.LockableScrollChangedListener mLockableScrollChangedListener;
   private boolean mScrollable;

   public LockableHorizontalScrollView(Context var1) {
      super(var1);
   }

   public LockableHorizontalScrollView(Context var1, AttributeSet var2) {
      super(var1, var2);
   }

   public LockableHorizontalScrollView(Context var1, AttributeSet var2, int var3) {
      super(var1, var2, var3);
   }

   public boolean isScrollable() {
      return this.mScrollable;
   }

   public boolean onInterceptTouchEvent(MotionEvent var1) {
      boolean var2;
      if(this.mScrollable && super.onInterceptTouchEvent(var1)) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   public boolean onTouchEvent(MotionEvent var1) {
      boolean var2;
      if(var1.getAction() != 0) {
         var2 = super.onTouchEvent(var1);
      } else if(this.mScrollable && super.onTouchEvent(var1)) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   public void scrollTo(int var1, int var2) {
      if(this.mScrollable) {
         super.scrollTo(var1, var2);
      }

   }

   void setScrollChangedListener(LockableHorizontalScrollView.LockableScrollChangedListener var1) {
      this.mLockableScrollChangedListener = var1;
   }

   public void setScrollable(boolean var1) {
      this.mScrollable = var1;
      this.setSmoothScrollingEnabled(var1);
   }

   void wrappedSmoothScrollBy(int var1, int var2) {
      if(this.mScrollable) {
         this.smoothScrollBy(var1, var2);
         if(this.mLockableScrollChangedListener != null) {
            this.mLockableScrollChangedListener.onSmoothScrollBy(var1, var2);
         }
      }

   }

   interface LockableScrollChangedListener {
      void onSmoothScrollBy(int var1, int var2);
   }
}
