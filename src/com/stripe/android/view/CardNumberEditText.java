package com.stripe.android.view;

import android.content.Context;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.text.InputFilter.LengthFilter;
import android.util.AttributeSet;
import com.stripe.android.util.CardUtils;
import com.stripe.android.util.StripeTextUtils;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class CardNumberEditText extends StripeEditText {
   private static final int MAX_LENGTH_AMEX_DINERS = 17;
   private static final int MAX_LENGTH_COMMON = 19;
   private static final Integer[] SPACES_ARRAY_AMEX;
   private static final Integer[] SPACES_ARRAY_COMMON = new Integer[]{Integer.valueOf(4), Integer.valueOf(9), Integer.valueOf(14)};
   private static final Set SPACE_SET_AMEX;
   private static final Set SPACE_SET_COMMON;
   String mCardBrand = "Unknown";
   private CardNumberEditText.CardBrandChangeListener mCardBrandChangeListener;
   private CardNumberEditText.CardNumberCompleteListener mCardNumberCompleteListener;
   private boolean mIgnoreChanges = false;
   private boolean mIsCardNumberValid = false;
   private int mLengthMax = 19;

   static {
      SPACE_SET_COMMON = new HashSet(Arrays.asList(SPACES_ARRAY_COMMON));
      SPACES_ARRAY_AMEX = new Integer[]{Integer.valueOf(4), Integer.valueOf(11)};
      SPACE_SET_AMEX = new HashSet(Arrays.asList(SPACES_ARRAY_AMEX));
   }

   public CardNumberEditText(Context var1) {
      super(var1);
      this.listenForTextChanges();
   }

   public CardNumberEditText(Context var1, AttributeSet var2) {
      super(var1, var2);
      this.listenForTextChanges();
   }

   public CardNumberEditText(Context var1, AttributeSet var2, int var3) {
      super(var1, var2, var3);
      this.listenForTextChanges();
   }

   private static int getLengthForBrand(String var0) {
      byte var1;
      if(!"American Express".equals(var0) && !"Diners Club".equals(var0)) {
         var1 = 19;
      } else {
         var1 = 17;
      }

      return var1;
   }

   private void listenForTextChanges() {
      this.addTextChangedListener(new TextWatcher() {
         int latestChangeStart;
         int latestInsertionSize;

         public void afterTextChanged(Editable var1) {
            boolean var3 = true;
            boolean var2 = true;
            CardNumberEditText var4;
            if(var1.length() == CardNumberEditText.this.mLengthMax) {
               var3 = CardNumberEditText.this.mIsCardNumberValid;
               CardNumberEditText.this.mIsCardNumberValid = CardUtils.isValidCardNumber(var1.toString());
               var4 = CardNumberEditText.this;
               if(CardNumberEditText.this.mIsCardNumberValid) {
                  var2 = false;
               }

               var4.setShouldShowError(var2);
               if(!var3 && CardNumberEditText.this.mIsCardNumberValid && CardNumberEditText.this.mCardNumberCompleteListener != null) {
                  CardNumberEditText.this.mCardNumberCompleteListener.onCardNumberComplete();
               }
            } else {
               var4 = CardNumberEditText.this;
               if(CardNumberEditText.this.getText() != null && CardUtils.isValidCardNumber(CardNumberEditText.this.getText().toString())) {
                  var2 = var3;
               } else {
                  var2 = false;
               }

               var4.mIsCardNumberValid = var2;
               CardNumberEditText.this.setShouldShowError(false);
            }

         }

         public void beforeTextChanged(CharSequence var1, int var2, int var3, int var4) {
            if(!CardNumberEditText.this.mIgnoreChanges) {
               this.latestChangeStart = var2;
               this.latestInsertionSize = var4;
            }

         }

         public void onTextChanged(CharSequence var1, int var2, int var3, int var4) {
            if(!CardNumberEditText.this.mIgnoreChanges) {
               if(var2 < 4) {
                  CardNumberEditText.this.updateCardBrandFromNumber(var1.toString());
               }

               if(var2 <= 16) {
                  String var6 = StripeTextUtils.removeSpacesAndHyphens(var1.toString());
                  if(var6 != null) {
                     String[] var5 = CardUtils.separateCardNumberGroups(var6, CardNumberEditText.this.mCardBrand);
                     StringBuilder var7 = new StringBuilder();

                     for(var2 = 0; var2 < var5.length && var5[var2] != null; ++var2) {
                        if(var2 != 0) {
                           var7.append(' ');
                        }

                        var7.append(var5[var2]);
                     }

                     var6 = var7.toString();
                     var2 = CardNumberEditText.this.updateSelectionIndex(var6.length(), this.latestChangeStart, this.latestInsertionSize);
                     CardNumberEditText.this.mIgnoreChanges = true;
                     CardNumberEditText.this.setText(var6);
                     CardNumberEditText.this.setSelection(var2);
                     CardNumberEditText.this.mIgnoreChanges = false;
                  }
               }
            }

         }
      });
   }

   private void updateCardBrand(String var1) {
      if(!this.mCardBrand.equals(var1)) {
         this.mCardBrand = var1;
         if(this.mCardBrandChangeListener != null) {
            this.mCardBrandChangeListener.onCardBrandChanged(this.mCardBrand);
         }

         int var2 = this.mLengthMax;
         this.mLengthMax = getLengthForBrand(this.mCardBrand);
         if(var2 != this.mLengthMax) {
            this.setFilters(new InputFilter[]{new LengthFilter(this.mLengthMax)});
         }
      }

   }

   private void updateCardBrandFromNumber(String var1) {
      this.updateCardBrand(CardUtils.getPossibleCardType(var1));
   }

   public String getCardBrand() {
      return this.mCardBrand;
   }

   public String getCardNumber() {
      String var1;
      if(this.mIsCardNumberValid) {
         var1 = StripeTextUtils.removeSpacesAndHyphens(this.getText().toString());
      } else {
         var1 = null;
      }

      return var1;
   }

   public int getLengthMax() {
      return this.mLengthMax;
   }

   public boolean isCardNumberValid() {
      return this.mIsCardNumberValid;
   }

   void setCardBrandChangeListener(CardNumberEditText.CardBrandChangeListener var1) {
      this.mCardBrandChangeListener = var1;
      this.mCardBrandChangeListener.onCardBrandChanged(this.mCardBrand);
   }

   void setCardNumberCompleteListener(CardNumberEditText.CardNumberCompleteListener var1) {
      this.mCardNumberCompleteListener = var1;
   }

   int updateSelectionIndex(int var1, int var2, int var3) {
      boolean var4 = false;
      Set var7;
      if("American Express".equals(this.mCardBrand)) {
         var7 = SPACE_SET_AMEX;
      } else {
         var7 = SPACE_SET_COMMON;
      }

      Iterator var9 = var7.iterator();

      int var5;
      int var6;
      for(var6 = 0; var9.hasNext(); var6 = var5) {
         Integer var8 = (Integer)var9.next();
         var5 = var6;
         if(var2 <= var8.intValue()) {
            var5 = var6;
            if(var2 + var3 > var8.intValue()) {
               var5 = var6 + 1;
            }
         }

         if(var3 == 0 && var2 == var8.intValue() + 1) {
            var4 = true;
         }
      }

      var3 = var2 + var3 + var6;
      var2 = var3;
      if(var4) {
         var2 = var3;
         if(var3 > 0) {
            var2 = var3 - 1;
         }
      }

      var3 = var1;
      if(var2 <= var1) {
         var3 = var2;
      }

      return var3;
   }

   interface CardBrandChangeListener {
      void onCardBrandChanged(String var1);
   }

   interface CardNumberCompleteListener {
      void onCardNumberComplete();
   }
}
