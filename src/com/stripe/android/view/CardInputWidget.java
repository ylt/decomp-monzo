package com.stripe.android.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.a.a.a;
import android.text.InputFilter;
import android.text.Layout;
import android.text.InputFilter.LengthFilter;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.Transformation;
import android.view.animation.Animation.AnimationListener;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.FrameLayout.LayoutParams;
import com.stripe.android.R;
import com.stripe.android.model.Card;
import com.stripe.android.util.DateUtils;
import com.stripe.android.util.StripeTextUtils;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class CardInputWidget extends LinearLayout {
   private static final long ANIMATION_LENGTH = 150L;
   public static final Map BRAND_RESOURCE_MAP = new HashMap() {
      {
         this.put("American Express", Integer.valueOf(R.drawable.ic_amex));
         this.put("Diners Club", Integer.valueOf(R.drawable.ic_diners));
         this.put("Discover", Integer.valueOf(R.drawable.ic_discover));
         this.put("JCB", Integer.valueOf(R.drawable.ic_jcb));
         this.put("MasterCard", Integer.valueOf(R.drawable.ic_mastercard));
         this.put("Visa", Integer.valueOf(R.drawable.ic_visa));
         this.put("Unknown", Integer.valueOf(R.drawable.ic_unknown));
      }
   };
   private static final String CVC_PLACEHOLDER_AMEX = "2345";
   private static final String CVC_PLACEHOLDER_COMMON = "CVC";
   private static final int DEFAULT_READER_ID = 42424242;
   private static final String EXTRA_CARD_VIEWED = "extra_card_viewed";
   private static final String EXTRA_SUPER_STATE = "extra_super_state";
   public static final String FOCUS_CARD = "focus_card";
   public static final String FOCUS_CVC = "focus_cvc";
   public static final String FOCUS_EXPIRY = "focus_expiry";
   private static final String FULL_SIZING_CARD_TEXT = "4242 4242 4242 4242";
   private static final String FULL_SIZING_DATE_TEXT = "MM/YY";
   private static final String HIDDEN_TEXT_AMEX = "3434 343434 ";
   private static final String HIDDEN_TEXT_COMMON = "4242 4242 4242 ";
   private static final String PEEK_TEXT_AMEX = "34343";
   private static final String PEEK_TEXT_COMMON = "4242";
   private static final String PEEK_TEXT_DINERS = "88";
   private String mCardHintText;
   private ImageView mCardIconImageView;
   private CardInputWidget.CardInputListener mCardInputListener;
   private CardNumberEditText mCardNumberEditText;
   private boolean mCardNumberIsViewed = true;
   private StripeEditText mCvcNumberEditText;
   private CardInputWidget.DimensionOverrideSettings mDimensionOverrides;
   private int mErrorColorInt;
   private ExpiryDateEditText mExpiryDateEditText;
   private FrameLayout mFrameLayout;
   private boolean mInitFlag;
   private boolean mIsAmEx;
   private CardInputWidget.PlacementParameters mPlacementParameters;
   private int mTintColorInt;
   private int mTotalLengthInPixels;

   public CardInputWidget(Context var1) {
      super(var1);
      this.initView((AttributeSet)null);
   }

   public CardInputWidget(Context var1, AttributeSet var2) {
      super(var1, var2);
      this.initView(var2);
   }

   public CardInputWidget(Context var1, AttributeSet var2, int var3) {
      super(var1, var2, var3);
      this.initView(var2);
   }

   private void applyTint(boolean var1) {
      if(var1 || "Unknown".equals(this.mCardNumberEditText.getCardBrand())) {
         Drawable var2 = a.g(this.mCardIconImageView.getDrawable());
         a.a(var2.mutate(), this.mTintColorInt);
         this.mCardIconImageView.setImageDrawable(a.h(var2));
      }

   }

   private String getCvcPlaceHolderForBrand(String var1) {
      if("American Express".equals(var1)) {
         var1 = "2345";
      } else {
         var1 = "CVC";
      }

      return var1;
   }

   private int getDesiredWidthInPixels(String var1, StripeEditText var2) {
      int var3;
      if(this.mDimensionOverrides == null) {
         var3 = (int)Layout.getDesiredWidth(var1, var2.getPaint());
      } else {
         var3 = this.mDimensionOverrides.getPixelWidth(var1, var2);
      }

      return var3;
   }

   private int getFrameWidth() {
      int var1;
      if(this.mDimensionOverrides == null) {
         var1 = this.mFrameLayout.getWidth();
      } else {
         var1 = this.mDimensionOverrides.getFrameWidth();
      }

      return var1;
   }

   private String getHiddenTextForBrand(String var1) {
      if("American Express".equals(var1)) {
         var1 = "3434 343434 ";
      } else {
         var1 = "4242 4242 4242 ";
      }

      return var1;
   }

   private String getPeekCardTextForBrand(String var1) {
      if("American Express".equals(var1)) {
         var1 = "34343";
      } else if("Diners Club".equals(var1)) {
         var1 = "88";
      } else {
         var1 = "4242";
      }

      return var1;
   }

   private void initView(AttributeSet var1) {
      inflate(this.getContext(), R.layout.card_input_widget, this);
      if(this.getId() == -1) {
         this.setId(42424242);
      }

      this.setOrientation(0);
      this.setMinimumWidth(this.getResources().getDimensionPixelSize(R.dimen.card_widget_min_width));
      this.mPlacementParameters = new CardInputWidget.PlacementParameters();
      this.mCardIconImageView = (ImageView)this.findViewById(R.id.iv_card_icon);
      this.mCardNumberEditText = (CardNumberEditText)this.findViewById(R.id.et_card_number);
      this.mExpiryDateEditText = (ExpiryDateEditText)this.findViewById(R.id.et_expiry_date);
      this.mCvcNumberEditText = (StripeEditText)this.findViewById(R.id.et_cvc_number);
      this.mCardNumberIsViewed = true;
      this.mFrameLayout = (FrameLayout)this.findViewById(R.id.frame_container);
      this.mErrorColorInt = this.mCardNumberEditText.getDefaultErrorColorInt();
      this.mTintColorInt = this.mCardNumberEditText.getHintTextColors().getDefaultColor();
      if(var1 != null) {
         TypedArray var2 = this.getContext().getTheme().obtainStyledAttributes(var1, R.styleable.CardInputView, 0, 0);

         try {
            this.mErrorColorInt = var2.getColor(R.styleable.CardInputView_cardTextErrorColor, this.mErrorColorInt);
            this.mTintColorInt = var2.getColor(R.styleable.CardInputView_cardTint, this.mTintColorInt);
            this.mCardHintText = var2.getString(R.styleable.CardInputView_cardHintText);
         } finally {
            var2.recycle();
         }
      }

      if(this.mCardHintText != null) {
         this.mCardNumberEditText.setHint(this.mCardHintText);
      }

      this.mCardNumberEditText.setErrorColor(this.mErrorColorInt);
      this.mExpiryDateEditText.setErrorColor(this.mErrorColorInt);
      this.mCvcNumberEditText.setErrorColor(this.mErrorColorInt);
      this.mCardNumberEditText.setOnFocusChangeListener(new OnFocusChangeListener() {
         public void onFocusChange(View var1, boolean var2) {
            if(var2) {
               CardInputWidget.this.scrollLeft();
               if(CardInputWidget.this.mCardInputListener != null) {
                  CardInputWidget.this.mCardInputListener.onFocusChange("focus_card");
               }
            }

         }
      });
      this.mExpiryDateEditText.setOnFocusChangeListener(new OnFocusChangeListener() {
         public void onFocusChange(View var1, boolean var2) {
            if(var2) {
               CardInputWidget.this.scrollRight();
               if(CardInputWidget.this.mCardInputListener != null) {
                  CardInputWidget.this.mCardInputListener.onFocusChange("focus_expiry");
               }
            }

         }
      });
      this.mExpiryDateEditText.setDeleteEmptyListener(new CardInputWidget.BackUpFieldDeleteListener(this.mCardNumberEditText));
      this.mCvcNumberEditText.setDeleteEmptyListener(new CardInputWidget.BackUpFieldDeleteListener(this.mExpiryDateEditText));
      this.mCvcNumberEditText.setOnFocusChangeListener(new OnFocusChangeListener() {
         public void onFocusChange(View var1, boolean var2) {
            if(var2) {
               CardInputWidget.this.scrollRight();
               if(CardInputWidget.this.mCardInputListener != null) {
                  CardInputWidget.this.mCardInputListener.onFocusChange("focus_cvc");
               }
            }

            CardInputWidget.this.updateIconCvc(CardInputWidget.this.mCardNumberEditText.getCardBrand(), var2, CardInputWidget.this.mCvcNumberEditText.getText().toString());
         }
      });
      this.mCvcNumberEditText.setAfterTextChangedListener(new StripeEditText.AfterTextChangedListener() {
         public void onTextChanged(String var1) {
            if(CardInputWidget.this.mCardInputListener != null && CardInputWidget.isCvcMaximalLength(CardInputWidget.this.mCardNumberEditText.getCardBrand(), var1)) {
               CardInputWidget.this.mCardInputListener.onCvcComplete();
            }

            CardInputWidget.this.updateIconCvc(CardInputWidget.this.mCardNumberEditText.getCardBrand(), CardInputWidget.this.mCvcNumberEditText.hasFocus(), var1);
         }
      });
      this.mCardNumberEditText.setCardNumberCompleteListener(new CardNumberEditText.CardNumberCompleteListener() {
         public void onCardNumberComplete() {
            CardInputWidget.this.scrollRight();
            if(CardInputWidget.this.mCardInputListener != null) {
               CardInputWidget.this.mCardInputListener.onCardComplete();
            }

         }
      });
      this.mCardNumberEditText.setCardBrandChangeListener(new CardNumberEditText.CardBrandChangeListener() {
         public void onCardBrandChanged(String var1) {
            CardInputWidget.this.mIsAmEx = "American Express".equals(var1);
            CardInputWidget.this.updateIcon(var1);
            CardInputWidget.this.updateCvc(var1);
         }
      });
      this.mExpiryDateEditText.setExpiryDateEditListener(new ExpiryDateEditText.ExpiryDateEditListener() {
         public void onExpiryDateComplete() {
            CardInputWidget.this.mCvcNumberEditText.requestFocus();
            if(CardInputWidget.this.mCardInputListener != null) {
               CardInputWidget.this.mCardInputListener.onExpirationComplete();
            }

         }
      });
      this.mCardNumberEditText.requestFocus();
   }

   private static boolean isCvcMaximalLength(String var0, String var1) {
      boolean var2 = true;
      if(var1 == null) {
         var2 = false;
      } else if("American Express".equals(var0)) {
         if(var1.length() != 4) {
            var2 = false;
         }
      } else if(var1.length() != 3) {
         var2 = false;
      }

      return var2;
   }

   private void scrollLeft() {
      if(!this.mCardNumberIsViewed && this.mInitFlag) {
         final int var1 = this.mPlacementParameters.peekCardWidth;
         var1 += this.mPlacementParameters.cardDateSeparation;
         final int var2 = this.mPlacementParameters.dateWidth;
         final int var3 = this.mPlacementParameters.dateCvcSeparation + var2 + var1;
         this.updateSpaceSizes(true);
         Animation var7 = new Animation(((LayoutParams)this.mCardNumberEditText.getLayoutParams()).leftMargin) {
            // $FF: synthetic field
            final int val$startPoint;

            {
               this.val$startPoint = var2;
            }

            protected void applyTransformation(float var1, Transformation var2) {
               super.applyTransformation(var1, var2);
               LayoutParams var3 = (LayoutParams)CardInputWidget.this.mCardNumberEditText.getLayoutParams();
               var3.leftMargin = (int)((float)this.val$startPoint * (1.0F - var1));
               CardInputWidget.this.mCardNumberEditText.setLayoutParams(var3);
            }
         };
         var2 = this.mPlacementParameters.cardWidth + this.mPlacementParameters.cardDateSeparation;
         Animation var5 = new Animation() {
            protected void applyTransformation(float var1x, Transformation var2x) {
               super.applyTransformation(var1x, var2x);
               int var3 = (int)((float)var2 * var1x + (1.0F - var1x) * (float)var1);
               LayoutParams var4 = (LayoutParams)CardInputWidget.this.mExpiryDateEditText.getLayoutParams();
               var4.leftMargin = var3;
               CardInputWidget.this.mExpiryDateEditText.setLayoutParams(var4);
            }
         };
         Animation var6 = new Animation(var2 - var1 + var3) {
            // $FF: synthetic field
            final int val$cvcDestination;

            {
               this.val$cvcDestination = var2;
            }

            protected void applyTransformation(float var1, Transformation var2) {
               super.applyTransformation(var1, var2);
               int var3x = (int)((float)this.val$cvcDestination * var1 + (1.0F - var1) * (float)var3);
               LayoutParams var4 = (LayoutParams)CardInputWidget.this.mCvcNumberEditText.getLayoutParams();
               var4.leftMargin = var3x;
               var4.rightMargin = 0;
               var4.width = CardInputWidget.this.mPlacementParameters.cvcWidth;
               CardInputWidget.this.mCvcNumberEditText.setLayoutParams(var4);
            }
         };
         var7.setAnimationListener(new CardInputWidget.AnimationEndListener() {
            public void onAnimationEnd(Animation var1) {
               CardInputWidget.this.mCardNumberEditText.requestFocus();
            }
         });
         var7.setDuration(150L);
         var5.setDuration(150L);
         var6.setDuration(150L);
         AnimationSet var4 = new AnimationSet(true);
         var4.addAnimation(var7);
         var4.addAnimation(var5);
         var4.addAnimation(var6);
         this.mFrameLayout.startAnimation(var4);
         this.mCardNumberIsViewed = true;
      }

   }

   private void scrollRight() {
      if(this.mCardNumberIsViewed && this.mInitFlag) {
         final int var3 = this.mPlacementParameters.cardWidth + this.mPlacementParameters.cardDateSeparation;
         this.updateSpaceSizes(false);
         Animation var5 = new Animation() {
            protected void applyTransformation(float var1, Transformation var2) {
               super.applyTransformation(var1, var2);
               LayoutParams var3 = (LayoutParams)CardInputWidget.this.mCardNumberEditText.getLayoutParams();
               var3.leftMargin = (int)((float)(CardInputWidget.this.mPlacementParameters.hiddenCardWidth * -1) * var1);
               CardInputWidget.this.mCardNumberEditText.setLayoutParams(var3);
            }
         };
         final int var2 = this.mPlacementParameters.peekCardWidth + this.mPlacementParameters.cardDateSeparation;
         Animation var4 = new Animation() {
            protected void applyTransformation(float var1, Transformation var2x) {
               super.applyTransformation(var1, var2x);
               int var3x = (int)((float)var2 * var1 + (1.0F - var1) * (float)var3);
               LayoutParams var4 = (LayoutParams)CardInputWidget.this.mExpiryDateEditText.getLayoutParams();
               var4.leftMargin = var3x;
               CardInputWidget.this.mExpiryDateEditText.setLayoutParams(var4);
            }
         };
         final int var1 = this.mPlacementParameters.peekCardWidth + this.mPlacementParameters.cardDateSeparation + this.mPlacementParameters.dateWidth + this.mPlacementParameters.dateCvcSeparation;
         Animation var7 = new Animation(var3 - var2 + var1) {
            // $FF: synthetic field
            final int val$cvcStartMargin;

            {
               this.val$cvcStartMargin = var3;
            }

            protected void applyTransformation(float var1x, Transformation var2) {
               super.applyTransformation(var1x, var2);
               int var3 = (int)((float)var1 * var1x + (1.0F - var1x) * (float)this.val$cvcStartMargin);
               LayoutParams var4 = (LayoutParams)CardInputWidget.this.mCvcNumberEditText.getLayoutParams();
               var4.leftMargin = var3;
               var4.rightMargin = 0;
               var4.width = CardInputWidget.this.mPlacementParameters.cvcWidth;
               CardInputWidget.this.mCvcNumberEditText.setLayoutParams(var4);
            }
         };
         var5.setDuration(150L);
         var4.setDuration(150L);
         var7.setDuration(150L);
         var5.setAnimationListener(new CardInputWidget.AnimationEndListener() {
            public void onAnimationEnd(Animation var1) {
               CardInputWidget.this.mExpiryDateEditText.requestFocus();
            }
         });
         AnimationSet var6 = new AnimationSet(true);
         var6.addAnimation(var5);
         var6.addAnimation(var4);
         var6.addAnimation(var7);
         this.mFrameLayout.startAnimation(var6);
         this.mCardNumberIsViewed = false;
      }

   }

   private void setLayoutValues(int var1, int var2, StripeEditText var3) {
      LayoutParams var4 = (LayoutParams)var3.getLayoutParams();
      var4.width = var1;
      var4.leftMargin = var2;
      var3.setLayoutParams(var4);
   }

   static boolean shouldIconShowBrand(String var0, boolean var1, String var2) {
      if(!var1) {
         var1 = true;
      } else {
         var1 = isCvcMaximalLength(var0, var2);
      }

      return var1;
   }

   private void updateCvc(String var1) {
      if("American Express".equals(var1)) {
         this.mCvcNumberEditText.setFilters(new InputFilter[]{new LengthFilter(4)});
         this.mCvcNumberEditText.setHint(R.string.cvc_amex_hint);
      } else {
         this.mCvcNumberEditText.setFilters(new InputFilter[]{new LengthFilter(3)});
         this.mCvcNumberEditText.setHint(R.string.cvc_number_hint);
      }

   }

   private void updateIcon(String var1) {
      if("Unknown".equals(var1)) {
         Drawable var2 = this.getResources().getDrawable(R.drawable.ic_unknown);
         this.mCardIconImageView.setImageDrawable(var2);
         this.applyTint(false);
      } else {
         this.mCardIconImageView.setImageResource(((Integer)BRAND_RESOURCE_MAP.get(var1)).intValue());
      }

   }

   private void updateIconCvc(String var1, boolean var2, String var3) {
      if(shouldIconShowBrand(var1, var2, var3)) {
         this.updateIcon(var1);
      } else {
         this.updateIconForCvcEntry("American Express".equals(var1));
      }

   }

   private void updateIconForCvcEntry(boolean var1) {
      if(var1) {
         this.mCardIconImageView.setImageResource(R.drawable.ic_cvc_amex);
      } else {
         this.mCardIconImageView.setImageResource(R.drawable.ic_cvc);
      }

      this.applyTint(true);
   }

   public void clear() {
      if(this.mCardNumberEditText.hasFocus() || this.mExpiryDateEditText.hasFocus() || this.mCvcNumberEditText.hasFocus() || this.hasFocus()) {
         this.mCardNumberEditText.requestFocus();
      }

      this.mCvcNumberEditText.setText("");
      this.mExpiryDateEditText.setText("");
      this.mCardNumberEditText.setText("");
   }

   public Card getCard() {
      String var4 = this.mCardNumberEditText.getCardNumber();
      int[] var3 = this.mExpiryDateEditText.getValidDateFields();
      Card var2;
      if(var4 != null && var3 != null && var3.length == 2) {
         byte var1;
         if(this.mIsAmEx) {
            var1 = 4;
         } else {
            var1 = 3;
         }

         String var5 = this.mCvcNumberEditText.getText().toString();
         if(!StripeTextUtils.isBlank(var5) && var5.length() == var1) {
            var2 = (new Card(var4, Integer.valueOf(var3[0]), Integer.valueOf(var3[1]), var5)).addLoggingToken("CardInputView");
         } else {
            var2 = null;
         }
      } else {
         var2 = null;
      }

      return var2;
   }

   StripeEditText getFocusRequestOnTouch(int var1) {
      Object var4 = null;
      int var2 = this.mFrameLayout.getLeft();
      Object var3;
      if(this.mCardNumberIsViewed) {
         if(var1 < var2 + this.mPlacementParameters.cardWidth) {
            var3 = var4;
         } else if(var1 < this.mPlacementParameters.cardTouchBufferLimit) {
            var3 = this.mCardNumberEditText;
         } else {
            var3 = var4;
            if(var1 < this.mPlacementParameters.dateStartPosition) {
               var3 = this.mExpiryDateEditText;
            }
         }
      } else {
         var3 = var4;
         if(var1 >= var2 + this.mPlacementParameters.peekCardWidth) {
            if(var1 < this.mPlacementParameters.cardTouchBufferLimit) {
               var3 = this.mCardNumberEditText;
            } else if(var1 < this.mPlacementParameters.dateStartPosition) {
               var3 = this.mExpiryDateEditText;
            } else {
               var3 = var4;
               if(var1 >= this.mPlacementParameters.dateStartPosition + this.mPlacementParameters.dateWidth) {
                  if(var1 < this.mPlacementParameters.dateRightTouchBufferLimit) {
                     var3 = this.mExpiryDateEditText;
                  } else {
                     var3 = var4;
                     if(var1 < this.mPlacementParameters.cvcStartPosition) {
                        var3 = this.mCvcNumberEditText;
                     }
                  }
               }
            }
         }
      }

      return (StripeEditText)var3;
   }

   CardInputWidget.PlacementParameters getPlacementParameters() {
      return this.mPlacementParameters;
   }

   public boolean onInterceptTouchEvent(MotionEvent var1) {
      boolean var2;
      if(var1.getAction() != 0) {
         var2 = super.onInterceptTouchEvent(var1);
      } else {
         StripeEditText var3 = this.getFocusRequestOnTouch((int)var1.getX());
         if(var3 != null) {
            var3.requestFocus();
            var2 = true;
         } else {
            var2 = super.onInterceptTouchEvent(var1);
         }
      }

      return var2;
   }

   protected void onLayout(boolean var1, int var2, int var3, int var4, int var5) {
      super.onLayout(var1, var2, var3, var4, var5);
      if(!this.mInitFlag && this.getWidth() != 0) {
         this.mInitFlag = true;
         this.mTotalLengthInPixels = this.getFrameWidth();
         this.updateSpaceSizes(this.mCardNumberIsViewed);
         if(this.mCardNumberIsViewed) {
            var2 = 0;
         } else {
            var2 = this.mPlacementParameters.hiddenCardWidth * -1;
         }

         this.setLayoutValues(this.mPlacementParameters.cardWidth, var2, this.mCardNumberEditText);
         if(this.mCardNumberIsViewed) {
            var2 = this.mPlacementParameters.cardWidth + this.mPlacementParameters.cardDateSeparation;
         } else {
            var2 = this.mPlacementParameters.peekCardWidth + this.mPlacementParameters.cardDateSeparation;
         }

         this.setLayoutValues(this.mPlacementParameters.dateWidth, var2, this.mExpiryDateEditText);
         if(this.mCardNumberIsViewed) {
            var2 = this.mTotalLengthInPixels;
         } else {
            var2 = this.mPlacementParameters.peekCardWidth + this.mPlacementParameters.cardDateSeparation + this.mPlacementParameters.dateWidth + this.mPlacementParameters.dateCvcSeparation;
         }

         this.setLayoutValues(this.mPlacementParameters.cvcWidth, var2, this.mCvcNumberEditText);
      }

   }

   protected void onRestoreInstanceState(Parcelable var1) {
      if(var1 instanceof Bundle) {
         Bundle var5 = (Bundle)var1;
         this.mCardNumberIsViewed = var5.getBoolean("extra_card_viewed", true);
         this.updateSpaceSizes(this.mCardNumberIsViewed);
         this.mTotalLengthInPixels = this.getFrameWidth();
         int var2;
         int var3;
         int var4;
         if(this.mCardNumberIsViewed) {
            var4 = 0;
            var2 = this.mPlacementParameters.cardWidth;
            var3 = this.mPlacementParameters.cardDateSeparation + var2;
            var2 = this.mTotalLengthInPixels;
         } else {
            var4 = this.mPlacementParameters.hiddenCardWidth * -1;
            var2 = this.mPlacementParameters.peekCardWidth;
            var3 = this.mPlacementParameters.cardDateSeparation + var2;
            var2 = this.mPlacementParameters.dateWidth + var3 + this.mPlacementParameters.dateCvcSeparation;
         }

         this.setLayoutValues(this.mPlacementParameters.cardWidth, var4, this.mCardNumberEditText);
         this.setLayoutValues(this.mPlacementParameters.dateWidth, var3, this.mExpiryDateEditText);
         this.setLayoutValues(this.mPlacementParameters.cvcWidth, var2, this.mCvcNumberEditText);
         super.onRestoreInstanceState(var5.getParcelable("extra_super_state"));
      } else {
         super.onRestoreInstanceState(var1);
      }

   }

   protected Parcelable onSaveInstanceState() {
      Bundle var1 = new Bundle();
      var1.putParcelable("extra_super_state", super.onSaveInstanceState());
      var1.putBoolean("extra_card_viewed", this.mCardNumberIsViewed);
      return var1;
   }

   public void onWindowFocusChanged(boolean var1) {
      super.onWindowFocusChanged(var1);
      if(var1) {
         this.applyTint(false);
      }

   }

   public void setCardInputListener(CardInputWidget.CardInputListener var1) {
      this.mCardInputListener = var1;
   }

   public void setCardNumber(String var1) {
      this.mCardNumberEditText.setText(var1);
      boolean var2;
      if(!this.mCardNumberEditText.isCardNumberValid()) {
         var2 = true;
      } else {
         var2 = false;
      }

      this.setCardNumberIsViewed(var2);
   }

   void setCardNumberIsViewed(boolean var1) {
      this.mCardNumberIsViewed = var1;
   }

   public void setCvcCode(String var1) {
      this.mCvcNumberEditText.setText(var1);
   }

   void setDimensionOverrideSettings(CardInputWidget.DimensionOverrideSettings var1) {
      this.mDimensionOverrides = var1;
   }

   public void setExpiryDate(int var1, int var2) {
      this.mExpiryDateEditText.setText(DateUtils.createDateStringFromIntegerInput(var1, var2));
   }

   void updateSpaceSizes(boolean var1) {
      int var2 = this.getFrameWidth();
      int var3 = this.mFrameLayout.getLeft();
      if(var2 != 0) {
         this.mPlacementParameters.cardWidth = this.getDesiredWidthInPixels("4242 4242 4242 4242", this.mCardNumberEditText);
         this.mPlacementParameters.dateWidth = this.getDesiredWidthInPixels("MM/YY", this.mExpiryDateEditText);
         String var4 = this.mCardNumberEditText.getCardBrand();
         this.mPlacementParameters.hiddenCardWidth = this.getDesiredWidthInPixels(this.getHiddenTextForBrand(var4), this.mCardNumberEditText);
         this.mPlacementParameters.cvcWidth = this.getDesiredWidthInPixels(this.getCvcPlaceHolderForBrand(var4), this.mCvcNumberEditText);
         this.mPlacementParameters.peekCardWidth = this.getDesiredWidthInPixels(this.getPeekCardTextForBrand(var4), this.mCardNumberEditText);
         if(var1) {
            this.mPlacementParameters.cardDateSeparation = var2 - this.mPlacementParameters.cardWidth - this.mPlacementParameters.dateWidth;
            this.mPlacementParameters.cardTouchBufferLimit = this.mPlacementParameters.cardWidth + var3 + this.mPlacementParameters.cardDateSeparation / 2;
            this.mPlacementParameters.dateStartPosition = var3 + this.mPlacementParameters.cardWidth + this.mPlacementParameters.cardDateSeparation;
         } else {
            this.mPlacementParameters.cardDateSeparation = var2 / 2 - this.mPlacementParameters.peekCardWidth - this.mPlacementParameters.dateWidth / 2;
            this.mPlacementParameters.dateCvcSeparation = var2 - this.mPlacementParameters.peekCardWidth - this.mPlacementParameters.cardDateSeparation - this.mPlacementParameters.dateWidth - this.mPlacementParameters.cvcWidth;
            this.mPlacementParameters.cardTouchBufferLimit = this.mPlacementParameters.peekCardWidth + var3 + this.mPlacementParameters.cardDateSeparation / 2;
            this.mPlacementParameters.dateStartPosition = var3 + this.mPlacementParameters.peekCardWidth + this.mPlacementParameters.cardDateSeparation;
            this.mPlacementParameters.dateRightTouchBufferLimit = this.mPlacementParameters.dateStartPosition + this.mPlacementParameters.dateWidth + this.mPlacementParameters.dateCvcSeparation / 2;
            this.mPlacementParameters.cvcStartPosition = this.mPlacementParameters.dateStartPosition + this.mPlacementParameters.dateWidth + this.mPlacementParameters.dateCvcSeparation;
         }
      }

   }

   private abstract class AnimationEndListener implements AnimationListener {
      private AnimationEndListener() {
      }

      // $FF: synthetic method
      AnimationEndListener(Object var2) {
         this();
      }

      public void onAnimationRepeat(Animation var1) {
      }

      public void onAnimationStart(Animation var1) {
      }
   }

   private class BackUpFieldDeleteListener implements StripeEditText.DeleteEmptyListener {
      private StripeEditText backUpTarget;

      BackUpFieldDeleteListener(StripeEditText var2) {
         this.backUpTarget = var2;
      }

      public void onDeleteEmpty() {
         String var1 = this.backUpTarget.getText().toString();
         if(var1.length() > 1) {
            this.backUpTarget.setText(var1.substring(0, var1.length() - 1));
         }

         this.backUpTarget.requestFocus();
         this.backUpTarget.setSelection(this.backUpTarget.length());
      }
   }

   public interface CardInputListener {
      void onCardComplete();

      void onCvcComplete();

      void onExpirationComplete();

      void onFocusChange(String var1);
   }

   interface DimensionOverrideSettings {
      int getFrameWidth();

      int getPixelWidth(String var1, EditText var2);
   }

   @Retention(RetentionPolicy.SOURCE)
   public @interface FocusField {
   }

   class PlacementParameters {
      int cardDateSeparation;
      int cardTouchBufferLimit;
      int cardWidth;
      int cvcStartPosition;
      int cvcWidth;
      int dateCvcSeparation;
      int dateRightTouchBufferLimit;
      int dateStartPosition;
      int dateWidth;
      int hiddenCardWidth;
      int peekCardWidth;

      public String toString() {
         String var2 = String.format(Locale.ENGLISH, "Touch Buffer Data:\nCardTouchBufferLimit = %d\nDateStartPosition = %d\nDateRightTouchBufferLimit = %d\nCvcStartPosition = %d", new Object[]{Integer.valueOf(this.cardTouchBufferLimit), Integer.valueOf(this.dateStartPosition), Integer.valueOf(this.dateRightTouchBufferLimit), Integer.valueOf(this.cvcStartPosition)});
         String var1 = String.format(Locale.ENGLISH, "CardWidth = %d\nHiddenCardWidth = %d\nPeekCardWidth = %d\nCardDateSeparation = %d\nDateWidth = %d\nDateCvcSeparation = %d\nCvcWidth = %d\n", new Object[]{Integer.valueOf(this.cardWidth), Integer.valueOf(this.hiddenCardWidth), Integer.valueOf(this.peekCardWidth), Integer.valueOf(this.cardDateSeparation), Integer.valueOf(this.dateWidth), Integer.valueOf(this.dateCvcSeparation), Integer.valueOf(this.cvcWidth)});
         return var1 + var2;
      }
   }
}
