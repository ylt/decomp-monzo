package com.stripe.android;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import com.stripe.android.exception.APIConnectionException;
import com.stripe.android.exception.APIException;
import com.stripe.android.exception.AuthenticationException;
import com.stripe.android.exception.CardException;
import com.stripe.android.exception.InvalidRequestException;
import com.stripe.android.exception.StripeException;
import com.stripe.android.model.BankAccount;
import com.stripe.android.model.Card;
import com.stripe.android.model.Source;
import com.stripe.android.model.SourceParams;
import com.stripe.android.model.Token;
import com.stripe.android.net.PollingResponse;
import com.stripe.android.net.PollingResponseHandler;
import com.stripe.android.net.RequestOptions;
import com.stripe.android.net.StripeApiHandler;
import com.stripe.android.util.StripeNetworkUtils;
import java.util.Map;
import java.util.concurrent.Executor;

public class Stripe {
   private Context mContext;
   private String mDefaultPublishableKey;
   private StripeApiHandler.LoggingResponseListener mLoggingResponseListener;
   Stripe.SourceCreator mSourceCreator = new Stripe.SourceCreator() {
      public void create(final SourceParams var1, final String var2, final String var3, Executor var4, final SourceCallback var5) {
         AsyncTask var6 = new AsyncTask() {
            protected Stripe.ResponseWrapper doInBackground(Void... var1x) {
               Stripe.ResponseWrapper var4;
               try {
                  Source var2x = StripeApiHandler.createSourceOnServer((StripeNetworkUtils.UidProvider)null, Stripe.this.mContext, var1, var2, var3, (StripeApiHandler.LoggingResponseListener)null);
                  var4 = Stripe.this.new ResponseWrapper(var2x, null);
               } catch (StripeException var3x) {
                  var4 = Stripe.this.new ResponseWrapper(var3x, null);
               }

               return var4;
            }

            protected void onPostExecute(Stripe.ResponseWrapper var1x) {
               if(var1x.source != null) {
                  var5.onSuccess(var1x.source);
               } else if(var1x.error != null) {
                  var5.onError(var1x.error);
               }

            }
         };
         Stripe.this.executeTask(var4, var6);
      }
   };
   private String mStripeAccount;
   Stripe.TokenCreator mTokenCreator = new Stripe.TokenCreator() {
      public void create(final Map var1, final String var2, final String var3, final String var4, Executor var5, final TokenCallback var6) {
         AsyncTask var7 = new AsyncTask() {
            protected Stripe.ResponseWrapper doInBackground(Void... var1x) {
               Stripe.ResponseWrapper var4x;
               try {
                  RequestOptions var5 = RequestOptions.builder(var2, var3, "source").build();
                  Token var2x = StripeApiHandler.createTokenOnServer(Stripe.this.mContext, var1, var5, var4, Stripe.this.mLoggingResponseListener);
                  var4x = Stripe.this.new ResponseWrapper(var2x, null);
               } catch (StripeException var3x) {
                  var4x = Stripe.this.new ResponseWrapper(var3x, null);
               }

               return var4x;
            }

            protected void onPostExecute(Stripe.ResponseWrapper var1x) {
               Stripe.this.tokenTaskPostExecution(var1x, var6);
            }
         };
         Stripe.this.executeTask(var5, var7);
      }
   };

   public Stripe(Context var1) {
      this.mContext = var1;
   }

   public Stripe(Context var1, String var2) {
      this.mContext = var1;
      this.setDefaultPublishableKey(var2);
   }

   private void createTokenFromParams(Map var1, String var2, String var3, Executor var4, TokenCallback var5) {
      if(var5 == null) {
         throw new RuntimeException("Required Parameter: 'callback' is required to use the created token and handle errors");
      } else {
         this.validateKey(var2);
         this.mTokenCreator.create(var1, var2, this.mStripeAccount, var3, var4, var5);
      }
   }

   private void executeTask(Executor var1, AsyncTask var2) {
      if(var1 != null && VERSION.SDK_INT > 11) {
         var2.executeOnExecutor(var1, new Void[0]);
      } else {
         var2.execute(new Void[0]);
      }

   }

   private void tokenTaskPostExecution(Stripe.ResponseWrapper var1, TokenCallback var2) {
      if(var1.token != null) {
         var2.onSuccess(var1.token);
      } else if(var1.error != null) {
         var2.onError(var1.error);
      } else {
         var2.onError(new RuntimeException("Somehow got neither a token response or an error response"));
      }

   }

   private void validateKey(String var1) {
      if(var1 != null && var1.length() != 0) {
         if(var1.startsWith("sk_")) {
            throw new IllegalArgumentException("Invalid Publishable Key: You are using a secret key to create a token, instead of the publishable one. For more info, see https://stripe.com/docs/stripe.js");
         }
      } else {
         throw new IllegalArgumentException("Invalid Publishable Key: You must use a valid publishable key to create a token.  For more info, see https://stripe.com/docs/stripe.js.");
      }
   }

   public void createBankAccountToken(BankAccount var1, TokenCallback var2) {
      this.createBankAccountToken(var1, this.mDefaultPublishableKey, (Executor)null, var2);
   }

   public void createBankAccountToken(BankAccount var1, String var2, Executor var3, TokenCallback var4) {
      if(var1 == null) {
         throw new RuntimeException("Required parameter: 'bankAccount' is requred to create a token");
      } else {
         this.createTokenFromParams(StripeNetworkUtils.hashMapFromBankAccount(this.mContext, var1), var2, "bank_account", var3, var4);
      }
   }

   public Token createBankAccountTokenSynchronous(BankAccount var1) throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
      return this.createBankAccountTokenSynchronous(var1, this.mDefaultPublishableKey);
   }

   public Token createBankAccountTokenSynchronous(BankAccount var1, String var2) throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
      this.validateKey(var2);
      RequestOptions var3 = RequestOptions.builder(var2, this.mStripeAccount, "source").build();
      return StripeApiHandler.createTokenOnServer(this.mContext, StripeNetworkUtils.hashMapFromBankAccount(this.mContext, var1), var3, "bank_account", this.mLoggingResponseListener);
   }

   public void createPiiToken(String var1, TokenCallback var2) {
      this.createPiiToken(var1, this.mDefaultPublishableKey, (Executor)null, var2);
   }

   public void createPiiToken(String var1, String var2, Executor var3, TokenCallback var4) {
      this.createTokenFromParams(StripeNetworkUtils.hashMapFromPersonalId(this.mContext, var1), var2, "pii", var3, var4);
   }

   public Token createPiiTokenSynchronous(String var1) throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
      return this.createPiiTokenSynchronous(var1, this.mDefaultPublishableKey);
   }

   public Token createPiiTokenSynchronous(String var1, String var2) throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
      this.validateKey(var2);
      RequestOptions var3 = RequestOptions.builder(var2, this.mStripeAccount, "source").build();
      return StripeApiHandler.createTokenOnServer(this.mContext, StripeNetworkUtils.hashMapFromPersonalId(this.mContext, var1), var3, "pii", this.mLoggingResponseListener);
   }

   public void createSource(SourceParams var1, SourceCallback var2) {
      this.createSource(var1, var2, (String)null, (Executor)null);
   }

   public void createSource(SourceParams var1, SourceCallback var2, String var3, Executor var4) {
      if(var3 == null) {
         var3 = this.mDefaultPublishableKey;
      }

      if(var3 != null) {
         this.mSourceCreator.create(var1, var3, this.mStripeAccount, var4, var2);
      }

   }

   public Source createSourceSynchronous(SourceParams var1) throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
      return this.createSourceSynchronous(var1, (String)null);
   }

   public Source createSourceSynchronous(SourceParams var1, String var2) throws AuthenticationException, InvalidRequestException, APIConnectionException, APIException {
      Object var3 = null;
      if(var2 == null) {
         var2 = this.mDefaultPublishableKey;
      }

      Source var4;
      if(var2 == null) {
         var4 = (Source)var3;
      } else {
         var4 = StripeApiHandler.createSourceOnServer((StripeNetworkUtils.UidProvider)null, this.mContext, var1, var2, this.mStripeAccount, this.mLoggingResponseListener);
      }

      return var4;
   }

   public void createToken(Card var1, TokenCallback var2) {
      this.createToken(var1, this.mDefaultPublishableKey, var2);
   }

   public void createToken(Card var1, String var2, TokenCallback var3) {
      this.createToken(var1, var2, (Executor)null, var3);
   }

   public void createToken(Card var1, String var2, Executor var3, TokenCallback var4) {
      if(var1 == null) {
         throw new RuntimeException("Required Parameter: 'card' is required to create a token");
      } else {
         this.createTokenFromParams(StripeNetworkUtils.hashMapFromCard(this.mContext, var1), var2, "card", var3, var4);
      }
   }

   public void createToken(Card var1, Executor var2, TokenCallback var3) {
      this.createToken(var1, this.mDefaultPublishableKey, var2, var3);
   }

   public Token createTokenSynchronous(Card var1) throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
      return this.createTokenSynchronous(var1, this.mDefaultPublishableKey);
   }

   public Token createTokenSynchronous(Card var1, String var2) throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
      this.validateKey(var2);
      RequestOptions var3 = RequestOptions.builder(var2, this.mStripeAccount, "source").build();
      return StripeApiHandler.createTokenOnServer(this.mContext, StripeNetworkUtils.hashMapFromCard(this.mContext, var1), var3, "card", this.mLoggingResponseListener);
   }

   @Deprecated
   public void pollSource(String var1, String var2, String var3, PollingResponseHandler var4, Integer var5) {
      String var6 = var3;
      if(var3 == null) {
         var6 = this.mDefaultPublishableKey;
      }

      if(var6 != null) {
         StripeApiHandler.pollSource(var1, var2, var6, var4, var5);
      }

   }

   @Deprecated
   public PollingResponse pollSourceSynchronous(String var1, String var2, String var3, Integer var4) {
      String var5 = var3;
      if(var3 == null) {
         var5 = this.mDefaultPublishableKey;
      }

      PollingResponse var6;
      if(var5 == null) {
         var6 = null;
      } else {
         var6 = StripeApiHandler.pollSourceSynchronous(var1, var2, var5, var4);
      }

      return var6;
   }

   public Source retrieveSourceSynchronous(String var1, String var2) throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
      return this.retrieveSourceSynchronous(var1, var2, (String)null);
   }

   public Source retrieveSourceSynchronous(String var1, String var2, String var3) throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
      String var4 = var3;
      if(var3 == null) {
         var4 = this.mDefaultPublishableKey;
      }

      Source var5;
      if(var4 == null) {
         var5 = null;
      } else {
         var5 = StripeApiHandler.retrieveSource(var1, var2, var4);
      }

      return var5;
   }

   public void setDefaultPublishableKey(String var1) {
      this.validateKey(var1);
      this.mDefaultPublishableKey = var1;
   }

   void setLoggingResponseListener(StripeApiHandler.LoggingResponseListener var1) {
      this.mLoggingResponseListener = var1;
   }

   public void setStripeAccount(String var1) {
      this.mStripeAccount = var1;
   }

   private class ResponseWrapper {
      final Exception error;
      final Source source;
      final Token token;

      private ResponseWrapper(Source var2) {
         this.source = var2;
         this.error = null;
         this.token = null;
      }

      // $FF: synthetic method
      ResponseWrapper(Source var2, Object var3) {
         this((Source)var2);
      }

      private ResponseWrapper(Token var2) {
         this.token = var2;
         this.source = null;
         this.error = null;
      }

      // $FF: synthetic method
      ResponseWrapper(Token var2, Object var3) {
         this((Token)var2);
      }

      private ResponseWrapper(Exception var2) {
         this.error = var2;
         this.source = null;
         this.token = null;
      }

      // $FF: synthetic method
      ResponseWrapper(Exception var2, Object var3) {
         this((Exception)var2);
      }
   }

   interface SourceCreator {
      void create(SourceParams var1, String var2, String var3, Executor var4, SourceCallback var5);
   }

   interface TokenCreator {
      void create(Map var1, String var2, String var3, String var4, Executor var5, TokenCallback var6);
   }
}
