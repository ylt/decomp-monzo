package com.stripe.android;

import com.stripe.android.model.Source;

public interface SourceCallback {
   void onError(Exception var1);

   void onSuccess(Source var1);
}
