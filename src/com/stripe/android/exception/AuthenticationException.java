package com.stripe.android.exception;

public class AuthenticationException extends StripeException {
   public AuthenticationException(String var1, String var2, Integer var3) {
      super(var1, var2, var3);
   }
}
