package com.stripe.android.exception;

public class APIException extends StripeException {
   public APIException(String var1, String var2, Integer var3, Throwable var4) {
      super(var1, var2, var3, var4);
   }
}
