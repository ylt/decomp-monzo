package com.stripe.android.exception;

public abstract class StripeException extends Exception {
   protected static final long serialVersionUID = 1L;
   private String requestId;
   private Integer statusCode;

   public StripeException(String var1, String var2, Integer var3) {
      super(var1, (Throwable)null);
      this.requestId = var2;
      this.statusCode = var3;
   }

   public StripeException(String var1, String var2, Integer var3, Throwable var4) {
      super(var1, var4);
      this.statusCode = var3;
      this.requestId = var2;
   }

   public String getRequestId() {
      return this.requestId;
   }

   public Integer getStatusCode() {
      return this.statusCode;
   }

   public String toString() {
      String var1 = "";
      if(this.requestId != null) {
         var1 = "; request-id: " + this.requestId;
      }

      return super.toString() + var1;
   }
}
