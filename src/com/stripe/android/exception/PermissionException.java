package com.stripe.android.exception;

public class PermissionException extends AuthenticationException {
   public PermissionException(String var1, String var2, Integer var3) {
      super(var1, var2, var3);
   }
}
