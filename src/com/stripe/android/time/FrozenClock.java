package com.stripe.android.time;

import java.util.Calendar;

public class FrozenClock extends Clock {
   public static void freeze(Calendar var0) {
      getInstance().calendarInstance = var0;
   }

   public static void unfreeze() {
      getInstance().calendarInstance = null;
   }
}
