package com.stripe.android.model;

import com.stripe.android.util.StripeJsonUtils;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class CustomerSource extends StripeJsonModel implements StripePaymentSource {
   private StripePaymentSource mStripePaymentSource;

   private CustomerSource(StripePaymentSource var1) {
      this.mStripePaymentSource = var1;
   }

   public static CustomerSource fromJson(JSONObject var0) {
      CustomerSource var1 = null;
      if(var0 != null) {
         String var2 = StripeJsonUtils.optString(var0, "object");
         Object var3;
         if("card".equals(var2)) {
            var3 = Card.fromJson(var0);
         } else if("source".equals(var2)) {
            var3 = Source.fromJson(var0);
         } else {
            var3 = null;
         }

         if(var3 != null) {
            var1 = new CustomerSource((StripePaymentSource)var3);
         }
      }

      return var1;
   }

   public static CustomerSource fromString(String var0) {
      CustomerSource var3;
      try {
         JSONObject var1 = new JSONObject(var0);
         var3 = fromJson(var1);
      } catch (JSONException var2) {
         var3 = null;
      }

      return var3;
   }

   public Card asCard() {
      Card var1;
      if(this.mStripePaymentSource instanceof Card) {
         var1 = (Card)this.mStripePaymentSource;
      } else {
         var1 = null;
      }

      return var1;
   }

   public Source asSource() {
      Source var1;
      if(this.mStripePaymentSource instanceof Source) {
         var1 = (Source)this.mStripePaymentSource;
      } else {
         var1 = null;
      }

      return var1;
   }

   public String getId() {
      String var1;
      if(this.mStripePaymentSource == null) {
         var1 = null;
      } else {
         var1 = this.mStripePaymentSource.getId();
      }

      return var1;
   }

   public StripePaymentSource getStripePaymentSource() {
      return this.mStripePaymentSource;
   }

   public JSONObject toJson() {
      JSONObject var1;
      if(this.mStripePaymentSource instanceof Source) {
         var1 = ((Source)this.mStripePaymentSource).toJson();
      } else if(this.mStripePaymentSource instanceof Card) {
         var1 = ((Card)this.mStripePaymentSource).toJson();
      } else {
         var1 = new JSONObject();
      }

      return var1;
   }

   public Map toMap() {
      Object var1;
      if(this.mStripePaymentSource instanceof Source) {
         var1 = ((Source)this.mStripePaymentSource).toMap();
      } else if(this.mStripePaymentSource instanceof Card) {
         var1 = ((Card)this.mStripePaymentSource).toMap();
      } else {
         var1 = new HashMap();
      }

      return (Map)var1;
   }
}
