package com.stripe.android.model;

import com.stripe.android.util.StripeJsonUtils;
import com.stripe.android.util.StripeNetworkUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Customer extends StripeJsonModel {
   private static final String FIELD_DATA = "data";
   private static final String FIELD_DEFAULT_SOURCE = "default_source";
   private static final String FIELD_HAS_MORE = "has_more";
   private static final String FIELD_ID = "id";
   private static final String FIELD_OBJECT = "object";
   private static final String FIELD_SHIPPING = "shipping";
   private static final String FIELD_SOURCES = "sources";
   private static final String FIELD_TOTAL_COUNT = "total_count";
   private static final String FIELD_URL = "url";
   private static final String VALUE_CUSTOMER = "customer";
   private static final String VALUE_LIST = "list";
   private String mDefaultSource;
   private Boolean mHasMore;
   private String mId;
   private ShippingInformation mShippingInformation;
   private List mSources = new ArrayList();
   private Integer mTotalCount;
   private String mUrl;

   public static Customer fromJson(JSONObject var0) {
      Customer var6;
      if(!"customer".equals(StripeJsonUtils.optString(var0, "object"))) {
         var6 = null;
      } else {
         Customer var2 = new Customer();
         var2.mId = StripeJsonUtils.optString(var0, "id");
         var2.mDefaultSource = StripeJsonUtils.optString(var0, "default_source");
         var2.mShippingInformation = ShippingInformation.fromJson(var0.optJSONObject("shipping"));
         JSONObject var3 = var0.optJSONObject("sources");
         if(var3 != null && "list".equals(StripeJsonUtils.optString(var3, "object"))) {
            var2.mHasMore = StripeJsonUtils.optBoolean(var3, "has_more");
            var2.mTotalCount = StripeJsonUtils.optInteger(var3, "total_count");
            var2.mUrl = StripeJsonUtils.optString(var3, "url");
            ArrayList var7 = new ArrayList();
            JSONArray var8 = var3.optJSONArray("data");

            for(int var1 = 0; var1 < var8.length(); ++var1) {
               try {
                  var7.add(CustomerSource.fromJson(var8.getJSONObject(var1)));
               } catch (JSONException var5) {
                  ;
               }
            }

            var2.mSources = var7;
         }

         var6 = var2;
      }

      return var6;
   }

   public static Customer fromString(String var0) {
      Customer var3;
      try {
         JSONObject var1 = new JSONObject(var0);
         var3 = fromJson(var1);
      } catch (JSONException var2) {
         var3 = null;
      }

      return var3;
   }

   public String getDefaultSource() {
      return this.mDefaultSource;
   }

   public Boolean getHasMore() {
      return this.mHasMore;
   }

   public String getId() {
      return this.mId;
   }

   public ShippingInformation getShippingInformation() {
      return this.mShippingInformation;
   }

   public List getSources() {
      return this.mSources;
   }

   public Integer getTotalCount() {
      return this.mTotalCount;
   }

   public String getUrl() {
      return this.mUrl;
   }

   public JSONObject toJson() {
      JSONObject var2 = new JSONObject();
      StripeJsonUtils.putStringIfNotNull(var2, "id", this.mId);
      StripeJsonUtils.putStringIfNotNull(var2, "object", "customer");
      StripeJsonUtils.putStringIfNotNull(var2, "default_source", this.mDefaultSource);
      StripeJsonModel.putStripeJsonModelIfNotNull(var2, "shipping", this.mShippingInformation);
      JSONObject var1 = new JSONObject();
      StripeJsonUtils.putStringIfNotNull(var1, "object", "list");
      StripeJsonUtils.putBooleanIfNotNull(var1, "has_more", this.mHasMore);
      StripeJsonUtils.putIntegerIfNotNull(var1, "total_count", this.mTotalCount);
      putStripeJsonModelListIfNotNull(var1, "data", this.mSources);
      StripeJsonUtils.putStringIfNotNull(var1, "url", this.mUrl);
      StripeJsonUtils.putObjectIfNotNull(var2, "sources", var1);
      return var2;
   }

   public Map toMap() {
      HashMap var2 = new HashMap();
      var2.put("id", this.mId);
      var2.put("object", "customer");
      var2.put("default_source", this.mDefaultSource);
      StripeJsonModel.putStripeJsonModelMapIfNotNull(var2, "shipping", this.mShippingInformation);
      HashMap var1 = new HashMap();
      var1.put("has_more", this.mHasMore);
      var1.put("total_count", this.mTotalCount);
      var1.put("object", "list");
      var1.put("url", this.mUrl);
      StripeJsonModel.putStripeJsonModelListIfNotNull((Map)var1, "data", this.mSources);
      StripeNetworkUtils.removeNullAndEmptyParams(var1);
      var2.put("sources", var1);
      StripeNetworkUtils.removeNullAndEmptyParams(var2);
      return var2;
   }
}
