package com.stripe.android.model;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Date;

public class Token implements StripePaymentSource {
   public static final String TYPE_BANK_ACCOUNT = "bank_account";
   public static final String TYPE_CARD = "card";
   public static final String TYPE_PII = "pii";
   private final BankAccount mBankAccount;
   private final Card mCard;
   private final Date mCreated;
   private final String mId;
   private final boolean mLivemode;
   private final String mType;
   private final boolean mUsed;

   public Token(String var1, boolean var2, Date var3, Boolean var4) {
      this.mId = var1;
      this.mType = "pii";
      this.mCreated = var3;
      this.mCard = null;
      this.mBankAccount = null;
      this.mUsed = var4.booleanValue();
      this.mLivemode = var2;
   }

   public Token(String var1, boolean var2, Date var3, Boolean var4, BankAccount var5) {
      this.mId = var1;
      this.mType = "bank_account";
      this.mCreated = var3;
      this.mLivemode = var2;
      this.mCard = null;
      this.mUsed = var4.booleanValue();
      this.mBankAccount = var5;
   }

   public Token(String var1, boolean var2, Date var3, Boolean var4, Card var5) {
      this.mId = var1;
      this.mType = "card";
      this.mCreated = var3;
      this.mLivemode = var2;
      this.mCard = var5;
      this.mUsed = var4.booleanValue();
      this.mBankAccount = null;
   }

   public BankAccount getBankAccount() {
      return this.mBankAccount;
   }

   public Card getCard() {
      return this.mCard;
   }

   public Date getCreated() {
      return this.mCreated;
   }

   public String getId() {
      return this.mId;
   }

   public boolean getLivemode() {
      return this.mLivemode;
   }

   public String getType() {
      return this.mType;
   }

   public boolean getUsed() {
      return this.mUsed;
   }

   @Retention(RetentionPolicy.SOURCE)
   public @interface TokenType {
   }
}
