package com.stripe.android.model;

import com.stripe.android.util.StripeJsonUtils;
import com.stripe.android.util.StripeNetworkUtils;
import com.stripe.android.util.StripeTextUtils;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class SourceCardData extends StripeSourceTypeModel {
   public static final String FIELD_ADDRESS_LINE1_CHECK = "address_line1_check";
   public static final String FIELD_ADDRESS_ZIP_CHECK = "address_zip_check";
   public static final String FIELD_BRAND = "brand";
   public static final String FIELD_COUNTRY = "country";
   public static final String FIELD_CVC_CHECK = "cvc_check";
   public static final String FIELD_DYNAMIC_LAST4 = "dynamic_last4";
   public static final String FIELD_EXP_MONTH = "exp_month";
   public static final String FIELD_EXP_YEAR = "exp_year";
   public static final String FIELD_FUNDING = "funding";
   public static final String FIELD_LAST4 = "last4";
   public static final String FIELD_THREE_D_SECURE = "three_d_secure";
   public static final String FIELD_TOKENIZATION_METHOD = "tokenization_method";
   public static final String NOT_SUPPORTED = "not_supported";
   public static final String OPTIONAL = "optional";
   public static final String REQUIRED = "required";
   public static final String UNKNOWN = "unknown";
   private String mAddressLine1Check;
   private String mAddressZipCheck;
   private String mBrand;
   private String mCountry;
   private String mCvcCheck;
   private String mDynamicLast4;
   private Integer mExpiryMonth;
   private Integer mExpiryYear;
   private String mFunding;
   private String mLast4;
   private String mThreeDSecureStatus;
   private String mTokenizationMethod;

   private SourceCardData() {
      this.addStandardFields(new String[]{"address_line1_check", "address_zip_check", "brand", "country", "cvc_check", "dynamic_last4", "exp_month", "exp_year", "funding", "last4", "three_d_secure", "tokenization_method"});
   }

   static String asThreeDSecureStatus(String var0) {
      if(StripeJsonUtils.nullIfNullOrEmpty(var0) == null) {
         var0 = null;
      } else if("required".equalsIgnoreCase(var0)) {
         var0 = "required";
      } else if("optional".equalsIgnoreCase(var0)) {
         var0 = "optional";
      } else if("not_supported".equalsIgnoreCase(var0)) {
         var0 = "not_supported";
      } else {
         var0 = "unknown";
      }

      return var0;
   }

   static SourceCardData fromJson(JSONObject var0) {
      SourceCardData var3;
      if(var0 == null) {
         var3 = null;
      } else {
         SourceCardData var1 = new SourceCardData();
         var1.setAddressLine1Check(StripeJsonUtils.optString(var0, "address_line1_check")).setAddressZipCheck(StripeJsonUtils.optString(var0, "address_zip_check")).setBrand(StripeTextUtils.asCardBrand(StripeJsonUtils.optString(var0, "brand"))).setCountry(StripeJsonUtils.optString(var0, "country")).setCvcCheck(StripeJsonUtils.optString(var0, "cvc_check")).setDynamicLast4(StripeJsonUtils.optString(var0, "dynamic_last4")).setExpiryMonth(StripeJsonUtils.optInteger(var0, "exp_month")).setExpiryYear(StripeJsonUtils.optInteger(var0, "exp_year")).setFunding(StripeTextUtils.asFundingType(StripeJsonUtils.optString(var0, "funding"))).setLast4(StripeJsonUtils.optString(var0, "last4")).setThreeDSecureStatus(asThreeDSecureStatus(StripeJsonUtils.optString(var0, "three_d_secure"))).setTokenizationMethod(StripeJsonUtils.optString(var0, "tokenization_method"));
         Map var2 = jsonObjectToMapWithoutKeys(var0, var1.mStandardFields);
         var3 = var1;
         if(var2 != null) {
            var1.setAdditionalFields(var2);
            var3 = var1;
         }
      }

      return var3;
   }

   static SourceCardData fromString(String var0) {
      SourceCardData var3;
      try {
         JSONObject var1 = new JSONObject(var0);
         var3 = fromJson(var1);
      } catch (JSONException var2) {
         var3 = null;
      }

      return var3;
   }

   private SourceCardData setAddressLine1Check(String var1) {
      this.mAddressLine1Check = var1;
      return this;
   }

   private SourceCardData setAddressZipCheck(String var1) {
      this.mAddressZipCheck = var1;
      return this;
   }

   private SourceCardData setBrand(String var1) {
      this.mBrand = var1;
      return this;
   }

   private SourceCardData setCountry(String var1) {
      this.mCountry = var1;
      return this;
   }

   private SourceCardData setCvcCheck(String var1) {
      this.mCvcCheck = var1;
      return this;
   }

   private SourceCardData setDynamicLast4(String var1) {
      this.mDynamicLast4 = var1;
      return this;
   }

   private SourceCardData setExpiryMonth(Integer var1) {
      this.mExpiryMonth = var1;
      return this;
   }

   private SourceCardData setExpiryYear(Integer var1) {
      this.mExpiryYear = var1;
      return this;
   }

   private SourceCardData setFunding(String var1) {
      this.mFunding = var1;
      return this;
   }

   private SourceCardData setLast4(String var1) {
      this.mLast4 = var1;
      return this;
   }

   private SourceCardData setThreeDSecureStatus(String var1) {
      this.mThreeDSecureStatus = var1;
      return this;
   }

   private SourceCardData setTokenizationMethod(String var1) {
      this.mTokenizationMethod = var1;
      return this;
   }

   public String getAddressLine1Check() {
      return this.mAddressLine1Check;
   }

   public String getAddressZipCheck() {
      return this.mAddressZipCheck;
   }

   public String getBrand() {
      return this.mBrand;
   }

   public String getCountry() {
      return this.mCountry;
   }

   public String getCvcCheck() {
      return this.mCvcCheck;
   }

   public String getDynamicLast4() {
      return this.mDynamicLast4;
   }

   public Integer getExpiryMonth() {
      return this.mExpiryMonth;
   }

   public Integer getExpiryYear() {
      return this.mExpiryYear;
   }

   public String getFunding() {
      return this.mFunding;
   }

   public String getLast4() {
      return this.mLast4;
   }

   public String getThreeDSecureStatus() {
      return this.mThreeDSecureStatus;
   }

   public String getTokenizationMethod() {
      return this.mTokenizationMethod;
   }

   public JSONObject toJson() {
      JSONObject var1 = new JSONObject();
      StripeJsonUtils.putStringIfNotNull(var1, "address_line1_check", this.mAddressLine1Check);
      StripeJsonUtils.putStringIfNotNull(var1, "address_zip_check", this.mAddressZipCheck);
      StripeJsonUtils.putStringIfNotNull(var1, "brand", this.mBrand);
      StripeJsonUtils.putStringIfNotNull(var1, "country", this.mCountry);
      StripeJsonUtils.putStringIfNotNull(var1, "dynamic_last4", this.mDynamicLast4);
      StripeJsonUtils.putIntegerIfNotNull(var1, "exp_month", this.mExpiryMonth);
      StripeJsonUtils.putIntegerIfNotNull(var1, "exp_year", this.mExpiryYear);
      StripeJsonUtils.putStringIfNotNull(var1, "funding", this.mFunding);
      StripeJsonUtils.putStringIfNotNull(var1, "last4", this.mLast4);
      StripeJsonUtils.putStringIfNotNull(var1, "three_d_secure", this.mThreeDSecureStatus);
      StripeJsonUtils.putStringIfNotNull(var1, "tokenization_method", this.mTokenizationMethod);
      putAdditionalFieldsIntoJsonObject(var1, this.mAdditionalFields);
      return var1;
   }

   public Map toMap() {
      HashMap var1 = new HashMap();
      var1.put("address_line1_check", this.mAddressLine1Check);
      var1.put("address_zip_check", this.mAddressZipCheck);
      var1.put("brand", this.mBrand);
      var1.put("country", this.mCountry);
      var1.put("dynamic_last4", this.mDynamicLast4);
      var1.put("exp_month", this.mExpiryMonth);
      var1.put("exp_year", this.mExpiryYear);
      var1.put("funding", this.mFunding);
      var1.put("last4", this.mLast4);
      var1.put("three_d_secure", this.mThreeDSecureStatus);
      var1.put("tokenization_method", this.mTokenizationMethod);
      putAdditionalFieldsIntoMap(var1, this.mAdditionalFields);
      StripeNetworkUtils.removeNullAndEmptyParams(var1);
      return var1;
   }

   @Retention(RetentionPolicy.SOURCE)
   public @interface ThreeDSecureStatus {
   }
}
