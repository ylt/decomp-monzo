package com.stripe.android.model;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;

abstract class StripeSourceTypeModel extends StripeJsonModel {
   private static final String NULL = "null";
   Map mAdditionalFields = new HashMap();
   Set mStandardFields = new HashSet();

   static Map jsonObjectToMapWithoutKeys(JSONObject var0, Set var1) {
      HashMap var6;
      if(var0 == null) {
         var6 = null;
      } else {
         Object var2 = var1;
         if(var1 == null) {
            var2 = new HashSet();
         }

         HashMap var7 = new HashMap();
         Iterator var4 = var0.keys();

         while(var4.hasNext()) {
            String var5 = (String)var4.next();
            Object var3 = var0.opt(var5);
            if(!"null".equals(var3) && var3 != null && !((Set)var2).contains(var5)) {
               var7.put(var5, var3);
            }
         }

         if(var7.isEmpty()) {
            var6 = null;
         } else {
            var6 = var7;
         }
      }

      return var6;
   }

   static void putAdditionalFieldsIntoJsonObject(JSONObject var0, Map var1) {
      if(var0 != null && var1 != null && !var1.isEmpty()) {
         Iterator var2 = var1.keySet().iterator();

         while(var2.hasNext()) {
            String var3 = (String)var2.next();

            try {
               if(var1.get(var3) != null) {
                  var0.put(var3, var1.get(var3));
               }
            } catch (JSONException var4) {
               ;
            }
         }
      }

   }

   static void putAdditionalFieldsIntoMap(Map var0, Map var1) {
      if(var0 != null && var1 != null && !var1.isEmpty()) {
         Iterator var3 = var1.keySet().iterator();

         while(var3.hasNext()) {
            String var2 = (String)var3.next();
            var0.put(var2, var1.get(var2));
         }
      }

   }

   void addStandardFields(String... var1) {
      Collections.addAll(this.mStandardFields, var1);
   }

   public Map getAdditionalFields() {
      return this.mAdditionalFields;
   }

   void setAdditionalFields(Map var1) {
      this.mAdditionalFields = var1;
   }
}
