package com.stripe.android.model;

import com.stripe.android.util.StripeJsonUtils;
import com.stripe.android.util.StripeNetworkUtils;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class SourceRedirect extends StripeJsonModel {
   public static final String FAILED = "failed";
   static final String FIELD_RETURN_URL = "return_url";
   static final String FIELD_STATUS = "status";
   static final String FIELD_URL = "url";
   public static final String PENDING = "pending";
   public static final String SUCCEEDED = "succeeded";
   private String mReturnUrl;
   private String mStatus;
   private String mUrl;

   SourceRedirect(String var1, String var2, String var3) {
      this.mReturnUrl = var1;
      this.mStatus = var2;
      this.mUrl = var3;
   }

   private static String asStatus(String var0) {
      if("pending".equals(var0)) {
         var0 = "pending";
      } else if("succeeded".equals(var0)) {
         var0 = "succeeded";
      } else if("failed".equals(var0)) {
         var0 = "failed";
      } else {
         var0 = null;
      }

      return var0;
   }

   public static SourceRedirect fromJson(JSONObject var0) {
      SourceRedirect var1;
      if(var0 == null) {
         var1 = null;
      } else {
         var1 = new SourceRedirect(StripeJsonUtils.optString(var0, "return_url"), asStatus(StripeJsonUtils.optString(var0, "status")), StripeJsonUtils.optString(var0, "url"));
      }

      return var1;
   }

   public static SourceRedirect fromString(String var0) {
      SourceRedirect var3;
      try {
         JSONObject var1 = new JSONObject(var0);
         var3 = fromJson(var1);
      } catch (JSONException var2) {
         var3 = null;
      }

      return var3;
   }

   public String getReturnUrl() {
      return this.mReturnUrl;
   }

   public String getStatus() {
      return this.mStatus;
   }

   public String getUrl() {
      return this.mUrl;
   }

   public void setReturnUrl(String var1) {
      this.mReturnUrl = var1;
   }

   public void setStatus(String var1) {
      this.mStatus = var1;
   }

   public void setUrl(String var1) {
      this.mUrl = var1;
   }

   public JSONObject toJson() {
      JSONObject var1 = new JSONObject();
      StripeJsonUtils.putStringIfNotNull(var1, "return_url", this.mReturnUrl);
      StripeJsonUtils.putStringIfNotNull(var1, "status", this.mStatus);
      StripeJsonUtils.putStringIfNotNull(var1, "url", this.mUrl);
      return var1;
   }

   public Map toMap() {
      HashMap var1 = new HashMap();
      var1.put("return_url", this.mReturnUrl);
      var1.put("status", this.mStatus);
      var1.put("url", this.mUrl);
      StripeNetworkUtils.removeNullAndEmptyParams(var1);
      return var1;
   }
}
