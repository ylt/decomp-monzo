package com.stripe.android.model;

import com.stripe.android.util.StripeJsonUtils;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class SourceAddress extends StripeJsonModel {
   private static final String FIELD_CITY = "city";
   private static final String FIELD_COUNTRY = "country";
   private static final String FIELD_LINE_1 = "line1";
   private static final String FIELD_LINE_2 = "line2";
   private static final String FIELD_POSTAL_CODE = "postal_code";
   private static final String FIELD_STATE = "state";
   private String mCity;
   private String mCountry;
   private String mLine1;
   private String mLine2;
   private String mPostalCode;
   private String mState;

   SourceAddress(String var1, String var2, String var3, String var4, String var5, String var6) {
      this.mCity = var1;
      this.mCountry = var2;
      this.mLine1 = var3;
      this.mLine2 = var4;
      this.mPostalCode = var5;
      this.mState = var6;
   }

   public static SourceAddress fromJson(JSONObject var0) {
      SourceAddress var1;
      if(var0 == null) {
         var1 = null;
      } else {
         var1 = new SourceAddress(StripeJsonUtils.optString(var0, "city"), StripeJsonUtils.optString(var0, "country"), StripeJsonUtils.optString(var0, "line1"), StripeJsonUtils.optString(var0, "line2"), StripeJsonUtils.optString(var0, "postal_code"), StripeJsonUtils.optString(var0, "state"));
      }

      return var1;
   }

   public static SourceAddress fromString(String var0) {
      SourceAddress var3;
      try {
         JSONObject var1 = new JSONObject(var0);
         var3 = fromJson(var1);
      } catch (JSONException var2) {
         var3 = null;
      }

      return var3;
   }

   public String getCity() {
      return this.mCity;
   }

   public String getCountry() {
      return this.mCountry;
   }

   public String getLine1() {
      return this.mLine1;
   }

   public String getLine2() {
      return this.mLine2;
   }

   public String getPostalCode() {
      return this.mPostalCode;
   }

   public String getState() {
      return this.mState;
   }

   public void setCity(String var1) {
      this.mCity = var1;
   }

   public void setCountry(String var1) {
      this.mCountry = var1;
   }

   public void setLine1(String var1) {
      this.mLine1 = var1;
   }

   public void setLine2(String var1) {
      this.mLine2 = var1;
   }

   public void setPostalCode(String var1) {
      this.mPostalCode = var1;
   }

   public void setState(String var1) {
      this.mState = var1;
   }

   public JSONObject toJson() {
      JSONObject var1 = new JSONObject();
      StripeJsonUtils.putStringIfNotNull(var1, "city", this.mCity);
      StripeJsonUtils.putStringIfNotNull(var1, "country", this.mCountry);
      StripeJsonUtils.putStringIfNotNull(var1, "line1", this.mLine1);
      StripeJsonUtils.putStringIfNotNull(var1, "line2", this.mLine2);
      StripeJsonUtils.putStringIfNotNull(var1, "postal_code", this.mPostalCode);
      StripeJsonUtils.putStringIfNotNull(var1, "state", this.mState);
      return var1;
   }

   public Map toMap() {
      HashMap var1 = new HashMap();
      var1.put("city", this.mCity);
      var1.put("country", this.mCountry);
      var1.put("line1", this.mLine1);
      var1.put("line2", this.mLine2);
      var1.put("postal_code", this.mPostalCode);
      var1.put("state", this.mState);
      return var1;
   }
}
