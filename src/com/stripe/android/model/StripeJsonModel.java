package com.stripe.android.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class StripeJsonModel {
   static void putStripeJsonModelIfNotNull(JSONObject var0, String var1, StripeJsonModel var2) {
      if(var2 != null) {
         try {
            var0.put(var1, var2.toJson());
         } catch (JSONException var3) {
            ;
         }
      }

   }

   static void putStripeJsonModelListIfNotNull(Map var0, String var1, List var2) {
      if(var2 != null) {
         ArrayList var4 = new ArrayList();

         for(int var3 = 0; var3 < var2.size(); ++var3) {
            var4.add(((StripeJsonModel)var2.get(var3)).toMap());
         }

         var0.put(var1, var4);
      }

   }

   static void putStripeJsonModelListIfNotNull(JSONObject param0, String param1, List param2) {
      // $FF: Couldn't be decompiled
   }

   static void putStripeJsonModelMapIfNotNull(Map var0, String var1, StripeJsonModel var2) {
      if(var2 != null) {
         var0.put(var1, var2.toMap());
      }

   }

   public abstract JSONObject toJson();

   public abstract Map toMap();

   public String toString() {
      return this.toJson().toString();
   }
}
