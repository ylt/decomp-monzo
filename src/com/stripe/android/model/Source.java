package com.stripe.android.model;

import com.stripe.android.util.StripeJsonUtils;
import com.stripe.android.util.StripeNetworkUtils;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;

public class Source extends StripeJsonModel implements StripePaymentSource {
   public static final String BANCONTACT = "bancontact";
   public static final String BITCOIN = "bitcoin";
   public static final String CANCELED = "canceled";
   public static final String CARD = "card";
   public static final String CHARGEABLE = "chargeable";
   public static final String CODE_VERIFICATION = "code_verification";
   public static final String CONSUMED = "consumed";
   static final String EURO = "eur";
   public static final String FAILED = "failed";
   static final String FIELD_AMOUNT = "amount";
   static final String FIELD_CLIENT_SECRET = "client_secret";
   static final String FIELD_CODE_VERIFICATION = "code_verification";
   static final String FIELD_CREATED = "created";
   static final String FIELD_CURRENCY = "currency";
   static final String FIELD_FLOW = "flow";
   static final String FIELD_ID = "id";
   static final String FIELD_LIVEMODE = "livemode";
   static final String FIELD_METADATA = "metadata";
   static final String FIELD_OBJECT = "object";
   static final String FIELD_OWNER = "owner";
   static final String FIELD_RECEIVER = "receiver";
   static final String FIELD_REDIRECT = "redirect";
   static final String FIELD_STATUS = "status";
   static final String FIELD_TYPE = "type";
   static final String FIELD_USAGE = "usage";
   public static final String GIROPAY = "giropay";
   public static final String IDEAL = "ideal";
   public static final Set MODELED_TYPES = new HashSet();
   public static final String NONE = "none";
   public static final String PENDING = "pending";
   public static final String RECEIVER = "receiver";
   public static final String REDIRECT = "redirect";
   public static final String REUSABLE = "reusable";
   public static final String SEPA_DEBIT = "sepa_debit";
   public static final String SINGLE_USE = "single_use";
   public static final String SOFORT = "sofort";
   public static final String THREE_D_SECURE = "three_d_secure";
   public static final String UNKNOWN = "unknown";
   static final String USD = "usd";
   static final String VALUE_SOURCE = "source";
   private Long mAmount;
   private String mClientSecret;
   private SourceCodeVerification mCodeVerification;
   private Long mCreated;
   private String mCurrency;
   private String mFlow;
   private String mId;
   private Boolean mLiveMode;
   private Map mMetaData;
   private SourceOwner mOwner;
   private SourceReceiver mReceiver;
   private SourceRedirect mRedirect;
   private Map mSourceTypeData;
   private StripeSourceTypeModel mSourceTypeModel;
   private String mStatus;
   private String mType;
   private String mTypeRaw;
   private String mUsage;

   static {
      MODELED_TYPES.add("card");
      MODELED_TYPES.add("sepa_debit");
   }

   Source(String var1, Long var2, String var3, SourceCodeVerification var4, Long var5, String var6, String var7, Boolean var8, Map var9, SourceOwner var10, SourceReceiver var11, SourceRedirect var12, String var13, Map var14, StripeSourceTypeModel var15, String var16, String var17, String var18) {
      this.mId = var1;
      this.mAmount = var2;
      this.mClientSecret = var3;
      this.mCodeVerification = var4;
      this.mCreated = var5;
      this.mCurrency = var6;
      this.mFlow = var7;
      this.mLiveMode = var8;
      this.mMetaData = var9;
      this.mOwner = var10;
      this.mReceiver = var11;
      this.mRedirect = var12;
      this.mStatus = var13;
      this.mSourceTypeData = var14;
      this.mSourceTypeModel = var15;
      this.mType = var16;
      this.mTypeRaw = var17;
      this.mUsage = var18;
   }

   static String asSourceFlow(String var0) {
      if("redirect".equals(var0)) {
         var0 = "redirect";
      } else if("receiver".equals(var0)) {
         var0 = "receiver";
      } else if("code_verification".equals(var0)) {
         var0 = "code_verification";
      } else if("none".equals(var0)) {
         var0 = "none";
      } else {
         var0 = null;
      }

      return var0;
   }

   static String asSourceStatus(String var0) {
      if("pending".equals(var0)) {
         var0 = "pending";
      } else if("chargeable".equals(var0)) {
         var0 = "chargeable";
      } else if("consumed".equals(var0)) {
         var0 = "consumed";
      } else if("canceled".equals(var0)) {
         var0 = "canceled";
      } else if("failed".equals(var0)) {
         var0 = "failed";
      } else {
         var0 = null;
      }

      return var0;
   }

   static String asSourceType(String var0) {
      if("bitcoin".equals(var0)) {
         var0 = "bitcoin";
      } else if("card".equals(var0)) {
         var0 = "card";
      } else if("three_d_secure".equals(var0)) {
         var0 = "three_d_secure";
      } else if("giropay".equals(var0)) {
         var0 = "giropay";
      } else if("sepa_debit".equals(var0)) {
         var0 = "sepa_debit";
      } else if("ideal".equals(var0)) {
         var0 = "ideal";
      } else if("sofort".equals(var0)) {
         var0 = "sofort";
      } else if("bancontact".equals(var0)) {
         var0 = "bancontact";
      } else if("unknown".equals(var0)) {
         var0 = "unknown";
      } else {
         var0 = null;
      }

      return var0;
   }

   static String asUsage(String var0) {
      if("reusable".equals(var0)) {
         var0 = "reusable";
      } else if("single_use".equals(var0)) {
         var0 = "single_use";
      } else {
         var0 = null;
      }

      return var0;
   }

   public static Source fromJson(JSONObject var0) {
      Source var18;
      if(var0 != null && "source".equals(var0.optString("object"))) {
         String var13 = StripeJsonUtils.optString(var0, "id");
         Long var5 = StripeJsonUtils.optLong(var0, "amount");
         String var15 = StripeJsonUtils.optString(var0, "client_secret");
         SourceCodeVerification var11 = (SourceCodeVerification)optStripeJsonModel(var0, "code_verification", SourceCodeVerification.class);
         Long var7 = StripeJsonUtils.optLong(var0, "created");
         String var8 = StripeJsonUtils.optString(var0, "currency");
         String var10 = asSourceFlow(StripeJsonUtils.optString(var0, "flow"));
         boolean var1 = var0.optBoolean("livemode");
         Map var16 = StripeJsonUtils.jsonObjectToStringMap(var0.optJSONObject("metadata"));
         SourceOwner var12 = (SourceOwner)optStripeJsonModel(var0, "owner", SourceOwner.class);
         SourceReceiver var6 = (SourceReceiver)optStripeJsonModel(var0, "receiver", SourceReceiver.class);
         SourceRedirect var9 = (SourceRedirect)optStripeJsonModel(var0, "redirect", SourceRedirect.class);
         String var14 = asSourceStatus(StripeJsonUtils.optString(var0, "status"));
         String var3 = StripeJsonUtils.optString(var0, "type");
         String var2 = var3;
         if(var3 == null) {
            var2 = "unknown";
         }

         String var4 = asSourceType(var2);
         var3 = var4;
         if(var4 == null) {
            var3 = "unknown";
         }

         Map var17 = StripeJsonUtils.jsonObjectToMap(var0.optJSONObject(var2));
         StripeSourceTypeModel var19;
         if(MODELED_TYPES.contains(var2)) {
            var19 = (StripeSourceTypeModel)optStripeJsonModel(var0, var2, StripeSourceTypeModel.class);
         } else {
            var19 = null;
         }

         var18 = new Source(var13, var5, var15, var11, var7, var8, var10, Boolean.valueOf(var1), var16, var12, var6, var9, var14, var17, var19, var3, var2, asUsage(StripeJsonUtils.optString(var0, "usage")));
      } else {
         var18 = null;
      }

      return var18;
   }

   public static Source fromString(String var0) {
      Source var3;
      try {
         JSONObject var1 = new JSONObject(var0);
         var3 = fromJson(var1);
      } catch (JSONException var2) {
         var3 = null;
      }

      return var3;
   }

   static StripeJsonModel optStripeJsonModel(JSONObject var0, String var1, Class var2) {
      Object var4 = null;
      StripeJsonModel var5;
      if(!var0.has(var1)) {
         var5 = (StripeJsonModel)var4;
      } else {
         byte var3 = -1;
         switch(var1.hashCode()) {
         case -808719889:
            if(var1.equals("receiver")) {
               var3 = 2;
            }
            break;
         case -776144932:
            if(var1.equals("redirect")) {
               var3 = 3;
            }
            break;
         case 3046160:
            if(var1.equals("card")) {
               var3 = 4;
            }
            break;
         case 106164915:
            if(var1.equals("owner")) {
               var3 = 1;
            }
            break;
         case 1615551277:
            if(var1.equals("code_verification")) {
               var3 = 0;
            }
            break;
         case 1636477296:
            if(var1.equals("sepa_debit")) {
               var3 = 5;
            }
         }

         switch(var3) {
         case 0:
            var5 = (StripeJsonModel)var2.cast(SourceCodeVerification.fromJson(var0.optJSONObject("code_verification")));
            break;
         case 1:
            var5 = (StripeJsonModel)var2.cast(SourceOwner.fromJson(var0.optJSONObject("owner")));
            break;
         case 2:
            var5 = (StripeJsonModel)var2.cast(SourceReceiver.fromJson(var0.optJSONObject("receiver")));
            break;
         case 3:
            var5 = (StripeJsonModel)var2.cast(SourceRedirect.fromJson(var0.optJSONObject("redirect")));
            break;
         case 4:
            var5 = (StripeJsonModel)var2.cast(SourceCardData.fromJson(var0.optJSONObject("card")));
            break;
         case 5:
            var5 = (StripeJsonModel)var2.cast(SourceSepaDebitData.fromJson(var0.optJSONObject("sepa_debit")));
            break;
         default:
            var5 = (StripeJsonModel)var4;
         }
      }

      return var5;
   }

   public Long getAmount() {
      return this.mAmount;
   }

   public String getClientSecret() {
      return this.mClientSecret;
   }

   public SourceCodeVerification getCodeVerification() {
      return this.mCodeVerification;
   }

   public Long getCreated() {
      return this.mCreated;
   }

   public String getCurrency() {
      return this.mCurrency;
   }

   public String getFlow() {
      return this.mFlow;
   }

   public String getId() {
      return this.mId;
   }

   public Map getMetaData() {
      return this.mMetaData;
   }

   public SourceOwner getOwner() {
      return this.mOwner;
   }

   public SourceReceiver getReceiver() {
      return this.mReceiver;
   }

   public SourceRedirect getRedirect() {
      return this.mRedirect;
   }

   public Map getSourceTypeData() {
      return this.mSourceTypeData;
   }

   public StripeSourceTypeModel getSourceTypeModel() {
      return this.mSourceTypeModel;
   }

   public String getStatus() {
      return this.mStatus;
   }

   public String getType() {
      return this.mType;
   }

   public String getTypeRaw() {
      return this.mTypeRaw;
   }

   public String getUsage() {
      return this.mUsage;
   }

   public Boolean isLiveMode() {
      return this.mLiveMode;
   }

   public void setAmount(long var1) {
      this.mAmount = Long.valueOf(var1);
   }

   public void setClientSecret(String var1) {
      this.mClientSecret = var1;
   }

   public void setCodeVerification(SourceCodeVerification var1) {
      this.mCodeVerification = var1;
   }

   public void setCreated(long var1) {
      this.mCreated = Long.valueOf(var1);
   }

   public void setCurrency(String var1) {
      this.mCurrency = var1;
   }

   public void setFlow(String var1) {
      this.mFlow = var1;
   }

   public void setId(String var1) {
      this.mId = var1;
   }

   public void setLiveMode(boolean var1) {
      this.mLiveMode = Boolean.valueOf(var1);
   }

   public void setMetaData(Map var1) {
      this.mMetaData = var1;
   }

   public void setOwner(SourceOwner var1) {
      this.mOwner = var1;
   }

   public void setReceiver(SourceReceiver var1) {
      this.mReceiver = var1;
   }

   public void setRedirect(SourceRedirect var1) {
      this.mRedirect = var1;
   }

   public void setSourceTypeData(Map var1) {
      this.mSourceTypeData = var1;
   }

   public void setStatus(String var1) {
      this.mStatus = var1;
   }

   public void setType(String var1) {
      this.mType = var1;
   }

   public void setTypeRaw(String var1) {
      this.mTypeRaw = var1;
      this.setType("unknown");
   }

   public void setUsage(String var1) {
      this.mUsage = var1;
   }

   public JSONObject toJson() {
      // $FF: Couldn't be decompiled
   }

   public Map toMap() {
      HashMap var1 = new HashMap();
      var1.put("id", this.mId);
      var1.put("amount", this.mAmount);
      var1.put("client_secret", this.mClientSecret);
      putStripeJsonModelMapIfNotNull(var1, "code_verification", this.mCodeVerification);
      var1.put("created", this.mCreated);
      var1.put("currency", this.mCurrency);
      var1.put("flow", this.mFlow);
      var1.put("livemode", this.mLiveMode);
      var1.put("metadata", this.mMetaData);
      putStripeJsonModelMapIfNotNull(var1, "owner", this.mOwner);
      putStripeJsonModelMapIfNotNull(var1, "receiver", this.mReceiver);
      putStripeJsonModelMapIfNotNull(var1, "redirect", this.mRedirect);
      var1.put(this.mTypeRaw, this.mSourceTypeData);
      var1.put("status", this.mStatus);
      var1.put("type", this.mTypeRaw);
      var1.put("usage", this.mUsage);
      StripeNetworkUtils.removeNullAndEmptyParams(var1);
      return var1;
   }

   @Retention(RetentionPolicy.SOURCE)
   public @interface SourceFlow {
   }

   @Retention(RetentionPolicy.SOURCE)
   public @interface SourceStatus {
   }

   @Retention(RetentionPolicy.SOURCE)
   public @interface SourceType {
   }

   @Retention(RetentionPolicy.SOURCE)
   public @interface Usage {
   }
}
