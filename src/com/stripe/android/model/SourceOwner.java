package com.stripe.android.model;

import com.stripe.android.util.StripeJsonUtils;
import com.stripe.android.util.StripeNetworkUtils;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class SourceOwner extends StripeJsonModel {
   private static final String FIELD_ADDRESS = "address";
   private static final String FIELD_EMAIL = "email";
   private static final String FIELD_NAME = "name";
   private static final String FIELD_PHONE = "phone";
   private static final String FIELD_VERIFIED_ADDRESS = "verified_address";
   private static final String FIELD_VERIFIED_EMAIL = "verified_email";
   private static final String FIELD_VERIFIED_NAME = "verified_name";
   private static final String FIELD_VERIFIED_PHONE = "verified_phone";
   private static final String VERIFIED = "verified_";
   private SourceAddress mAddress;
   private String mEmail;
   private String mName;
   private String mPhone;
   private SourceAddress mVerifiedAddress;
   private String mVerifiedEmail;
   private String mVerifiedName;
   private String mVerifiedPhone;

   SourceOwner(SourceAddress var1, String var2, String var3, String var4, SourceAddress var5, String var6, String var7, String var8) {
      this.mAddress = var1;
      this.mEmail = var2;
      this.mName = var3;
      this.mPhone = var4;
      this.mVerifiedAddress = var5;
      this.mVerifiedEmail = var6;
      this.mVerifiedName = var7;
      this.mVerifiedPhone = var8;
   }

   public static SourceOwner fromJson(JSONObject var0) {
      JSONObject var1 = null;
      SourceOwner var6;
      if(var0 == null) {
         var6 = var1;
      } else {
         var1 = var0.optJSONObject("address");
         SourceAddress var7;
         if(var1 != null) {
            var7 = SourceAddress.fromJson(var1);
         } else {
            var7 = null;
         }

         String var5 = StripeJsonUtils.optString(var0, "email");
         String var3 = StripeJsonUtils.optString(var0, "name");
         String var4 = StripeJsonUtils.optString(var0, "phone");
         JSONObject var2 = var0.optJSONObject("verified_address");
         SourceAddress var8;
         if(var2 != null) {
            var8 = SourceAddress.fromJson(var2);
         } else {
            var8 = null;
         }

         var6 = new SourceOwner(var7, var5, var3, var4, var8, StripeJsonUtils.optString(var0, "verified_email"), StripeJsonUtils.optString(var0, "verified_name"), StripeJsonUtils.optString(var0, "verified_phone"));
      }

      return var6;
   }

   public static SourceOwner fromString(String var0) {
      SourceOwner var3;
      try {
         JSONObject var1 = new JSONObject(var0);
         var3 = fromJson(var1);
      } catch (JSONException var2) {
         var3 = null;
      }

      return var3;
   }

   public SourceAddress getAddress() {
      return this.mAddress;
   }

   public String getEmail() {
      return this.mEmail;
   }

   public String getName() {
      return this.mName;
   }

   public String getPhone() {
      return this.mPhone;
   }

   public SourceAddress getVerifiedAddress() {
      return this.mVerifiedAddress;
   }

   public String getVerifiedEmail() {
      return this.mVerifiedEmail;
   }

   public String getVerifiedName() {
      return this.mVerifiedName;
   }

   public String getVerifiedPhone() {
      return this.mVerifiedPhone;
   }

   void setAddress(SourceAddress var1) {
      this.mAddress = var1;
   }

   void setEmail(String var1) {
      this.mEmail = var1;
   }

   void setName(String var1) {
      this.mName = var1;
   }

   void setPhone(String var1) {
      this.mPhone = var1;
   }

   void setVerifiedAddress(SourceAddress var1) {
      this.mVerifiedAddress = var1;
   }

   void setVerifiedEmail(String var1) {
      this.mVerifiedEmail = var1;
   }

   void setVerifiedName(String var1) {
      this.mVerifiedName = var1;
   }

   void setVerifiedPhone(String var1) {
      this.mVerifiedPhone = var1;
   }

   public JSONObject toJson() {
      // $FF: Couldn't be decompiled
   }

   public Map toMap() {
      HashMap var1 = new HashMap();
      if(this.mAddress != null) {
         var1.put("address", this.mAddress.toMap());
      }

      var1.put("email", this.mEmail);
      var1.put("name", this.mName);
      var1.put("phone", this.mPhone);
      if(this.mVerifiedAddress != null) {
         var1.put("verified_address", this.mVerifiedAddress.toMap());
      }

      var1.put("verified_email", this.mVerifiedEmail);
      var1.put("verified_name", this.mVerifiedName);
      var1.put("verified_phone", this.mVerifiedPhone);
      StripeNetworkUtils.removeNullAndEmptyParams(var1);
      return var1;
   }
}
