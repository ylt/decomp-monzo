package com.stripe.android.model;

import com.stripe.android.util.StripeNetworkUtils;
import java.util.HashMap;
import java.util.Map;

public class SourceParams {
   static final String API_PARAM_AMOUNT = "amount";
   static final String API_PARAM_CLIENT_SECRET = "client_secret";
   static final String API_PARAM_CURRENCY = "currency";
   static final String API_PARAM_METADATA = "metadata";
   static final String API_PARAM_OWNER = "owner";
   static final String API_PARAM_REDIRECT = "redirect";
   static final String API_PARAM_TOKEN = "token";
   static final String API_PARAM_TYPE = "type";
   static final String FIELD_ADDRESS = "address";
   static final String FIELD_BANK = "bank";
   static final String FIELD_CARD = "card";
   static final String FIELD_CITY = "city";
   static final String FIELD_COUNTRY = "country";
   static final String FIELD_CVC = "cvc";
   static final String FIELD_EMAIL = "email";
   static final String FIELD_EXP_MONTH = "exp_month";
   static final String FIELD_EXP_YEAR = "exp_year";
   static final String FIELD_IBAN = "iban";
   static final String FIELD_LINE_1 = "line1";
   static final String FIELD_LINE_2 = "line2";
   static final String FIELD_NAME = "name";
   static final String FIELD_NUMBER = "number";
   static final String FIELD_POSTAL_CODE = "postal_code";
   static final String FIELD_RETURN_URL = "return_url";
   static final String FIELD_STATE = "state";
   static final String FIELD_STATEMENT_DESCRIPTOR = "statement_descriptor";
   private Long mAmount;
   private Map mApiParameterMap;
   private String mCurrency;
   private Map mMetaData;
   private Map mOwner;
   private Map mRedirect;
   private String mToken;
   private String mType;
   private String mTypeRaw;

   public static SourceParams createBancontactParams(long var0, String var2, String var3, String var4) {
      SourceParams var5 = (new SourceParams()).setType("bancontact").setCurrency("eur").setAmount(var0).setOwner(createSimpleMap("name", var2)).setRedirect(createSimpleMap("return_url", var3));
      if(var4 != null) {
         var5.setApiParameterMap(createSimpleMap("statement_descriptor", var4));
      }

      return var5;
   }

   public static SourceParams createBitcoinParams(long var0, String var2, String var3) {
      return (new SourceParams()).setType("bitcoin").setAmount(var0).setCurrency(var2).setOwner(createSimpleMap("email", var3));
   }

   public static SourceParams createCardParams(Card var0) {
      SourceParams var1 = (new SourceParams()).setType("card");
      HashMap var2 = new HashMap();
      var2.put("number", var0.getNumber());
      var2.put("exp_month", var0.getExpMonth());
      var2.put("exp_year", var0.getExpYear());
      var2.put("cvc", var0.getCVC());
      StripeNetworkUtils.removeNullAndEmptyParams(var2);
      var1.setApiParameterMap(var2);
      var2 = new HashMap();
      var2.put("line1", var0.getAddressLine1());
      var2.put("line2", var0.getAddressLine2());
      var2.put("city", var0.getAddressCity());
      var2.put("country", var0.getAddressCountry());
      var2.put("state", var0.getAddressState());
      var2.put("postal_code", var0.getAddressZip());
      StripeNetworkUtils.removeNullAndEmptyParams(var2);
      HashMap var3 = new HashMap();
      var3.put("name", var0.getName());
      if(var2.keySet().size() > 0) {
         var3.put("address", var2);
      }

      StripeNetworkUtils.removeNullAndEmptyParams(var3);
      if(var3.keySet().size() > 0) {
         var1.setOwner(var3);
      }

      return var1;
   }

   public static SourceParams createCustomParams() {
      return new SourceParams();
   }

   public static SourceParams createGiropayParams(long var0, String var2, String var3, String var4) {
      SourceParams var5 = (new SourceParams()).setType("giropay").setCurrency("eur").setAmount(var0).setOwner(createSimpleMap("name", var2)).setRedirect(createSimpleMap("return_url", var3));
      if(var4 != null) {
         var5.setApiParameterMap(createSimpleMap("statement_descriptor", var4));
      }

      return var5;
   }

   public static SourceParams createIdealParams(long var0, String var2, String var3, String var4, String var5) {
      SourceParams var6 = (new SourceParams()).setType("ideal").setCurrency("eur").setAmount(var0).setOwner(createSimpleMap("name", var2)).setRedirect(createSimpleMap("return_url", var3));
      if(var4 != null && var5 != null) {
         var6.setApiParameterMap(createSimpleMap("statement_descriptor", var4, "bank", var5));
      }

      return var6;
   }

   public static Map createRetrieveSourceParams(String var0) {
      HashMap var1 = new HashMap();
      var1.put("client_secret", var0);
      return var1;
   }

   public static SourceParams createSepaDebitParams(String var0, String var1, String var2, String var3, String var4, String var5) {
      SourceParams var6 = (new SourceParams()).setType("sepa_debit").setCurrency("eur");
      HashMap var7 = new HashMap();
      var7.put("line1", var2);
      var7.put("city", var3);
      var7.put("postal_code", var4);
      var7.put("country", var5);
      HashMap var8 = new HashMap();
      var8.put("name", var0);
      var8.put("address", var7);
      var6.setOwner(var8).setApiParameterMap(createSimpleMap("iban", var1));
      return var6;
   }

   private static Map createSimpleMap(String var0, Object var1) {
      HashMap var2 = new HashMap();
      var2.put(var0, var1);
      return var2;
   }

   private static Map createSimpleMap(String var0, Object var1, String var2, Object var3) {
      HashMap var4 = new HashMap();
      var4.put(var0, var1);
      var4.put(var2, var3);
      return var4;
   }

   public static SourceParams createSofortParams(long var0, String var2, String var3, String var4) {
      SourceParams var5 = (new SourceParams()).setType("sofort").setCurrency("eur").setAmount(var0).setRedirect(createSimpleMap("return_url", var2));
      Map var6 = createSimpleMap("country", var3);
      if(var4 != null) {
         var6.put("statement_descriptor", var4);
      }

      var5.setApiParameterMap(var6);
      return var5;
   }

   public static SourceParams createThreeDSecureParams(long var0, String var2, String var3, String var4) {
      SourceParams var5 = (new SourceParams()).setType("three_d_secure").setCurrency(var2).setAmount(var0).setRedirect(createSimpleMap("return_url", var3));
      var5.setApiParameterMap(createSimpleMap("card", var4));
      return var5;
   }

   public Long getAmount() {
      return this.mAmount;
   }

   public Map getApiParameterMap() {
      return this.mApiParameterMap;
   }

   public String getCurrency() {
      return this.mCurrency;
   }

   public Map getMetaData() {
      return this.mMetaData;
   }

   public Map getOwner() {
      return this.mOwner;
   }

   public Map getRedirect() {
      return this.mRedirect;
   }

   public String getType() {
      return this.mType;
   }

   public String getTypeRaw() {
      return this.mTypeRaw;
   }

   public SourceParams setAmount(long var1) {
      this.mAmount = Long.valueOf(var1);
      return this;
   }

   public SourceParams setApiParameterMap(Map var1) {
      this.mApiParameterMap = var1;
      return this;
   }

   public SourceParams setCurrency(String var1) {
      this.mCurrency = var1;
      return this;
   }

   public SourceParams setMetaData(Map var1) {
      this.mMetaData = var1;
      return this;
   }

   public SourceParams setOwner(Map var1) {
      this.mOwner = var1;
      return this;
   }

   public SourceParams setRedirect(Map var1) {
      this.mRedirect = var1;
      return this;
   }

   public SourceParams setReturnUrl(String var1) {
      if(this.mRedirect == null) {
         this.setRedirect(createSimpleMap("return_url", var1));
      } else {
         this.mRedirect.put("return_url", var1);
      }

      return this;
   }

   public SourceParams setToken(String var1) {
      this.mToken = var1;
      return this;
   }

   public SourceParams setType(String var1) {
      this.mType = var1;
      this.mTypeRaw = var1;
      return this;
   }

   public SourceParams setTypeRaw(String var1) {
      this.mType = Source.asSourceType(var1);
      if(this.mType == null) {
         this.mType = "unknown";
      }

      this.mTypeRaw = var1;
      return this;
   }

   public Map toParamMap() {
      HashMap var1 = new HashMap();
      var1.put("type", this.mTypeRaw);
      var1.put(this.mTypeRaw, this.mApiParameterMap);
      var1.put("amount", this.mAmount);
      var1.put("currency", this.mCurrency);
      var1.put("owner", this.mOwner);
      var1.put("redirect", this.mRedirect);
      var1.put("metadata", this.mMetaData);
      var1.put("token", this.mToken);
      StripeNetworkUtils.removeNullAndEmptyParams(var1);
      return var1;
   }
}
