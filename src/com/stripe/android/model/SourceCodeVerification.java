package com.stripe.android.model;

import com.stripe.android.util.StripeJsonUtils;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class SourceCodeVerification extends StripeJsonModel {
   static final String FAILED = "failed";
   private static final String FIELD_ATTEMPTS_REMAINING = "attempts_remaining";
   private static final String FIELD_STATUS = "status";
   private static final int INVALID_ATTEMPTS_REMAINING = -1;
   static final String PENDING = "pending";
   static final String SUCCEEDED = "succeeded";
   private int mAttemptsRemaining;
   private String mStatus;

   SourceCodeVerification(int var1, String var2) {
      this.mAttemptsRemaining = var1;
      this.mStatus = var2;
   }

   private static String asStatus(String var0) {
      if("pending".equals(var0)) {
         var0 = "pending";
      } else if("succeeded".equals(var0)) {
         var0 = "succeeded";
      } else if("failed".equals(var0)) {
         var0 = "failed";
      } else {
         var0 = null;
      }

      return var0;
   }

   public static SourceCodeVerification fromJson(JSONObject var0) {
      SourceCodeVerification var1;
      if(var0 == null) {
         var1 = null;
      } else {
         var1 = new SourceCodeVerification(var0.optInt("attempts_remaining", -1), asStatus(StripeJsonUtils.optString(var0, "status")));
      }

      return var1;
   }

   public static SourceCodeVerification fromString(String var0) {
      SourceCodeVerification var3;
      try {
         JSONObject var1 = new JSONObject(var0);
         var3 = fromJson(var1);
      } catch (JSONException var2) {
         var3 = null;
      }

      return var3;
   }

   int getAttemptsRemaining() {
      return this.mAttemptsRemaining;
   }

   String getStatus() {
      return this.mStatus;
   }

   void setAttemptsRemaining(int var1) {
      this.mAttemptsRemaining = var1;
   }

   void setStatus(String var1) {
      this.mStatus = var1;
   }

   public JSONObject toJson() {
      JSONObject var1 = new JSONObject();

      try {
         var1.put("attempts_remaining", this.mAttemptsRemaining);
         StripeJsonUtils.putStringIfNotNull(var1, "status", this.mStatus);
      } catch (JSONException var3) {
         ;
      }

      return var1;
   }

   public Map toMap() {
      HashMap var1 = new HashMap();
      var1.put("attempts_remaining", Integer.valueOf(this.mAttemptsRemaining));
      if(this.mStatus != null) {
         var1.put("status", this.mStatus);
      }

      return var1;
   }
}
