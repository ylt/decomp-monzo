package com.stripe.android.model;

import com.stripe.android.util.StripeJsonUtils;
import com.stripe.android.util.StripeNetworkUtils;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

public class ShippingInformation extends StripeJsonModel {
   private static final String FIELD_ADDRESS = "address";
   private static final String FIELD_NAME = "name";
   private static final String FIELD_PHONE = "phone";
   private SourceAddress mAddress;
   private String mName;
   private String mPhone;

   public static ShippingInformation fromJson(JSONObject var0) {
      ShippingInformation var2;
      if(var0 == null) {
         var2 = null;
      } else {
         ShippingInformation var1 = new ShippingInformation();
         var1.mName = StripeJsonUtils.optString(var0, "name");
         var1.mPhone = StripeJsonUtils.optString(var0, "phone");
         var1.mAddress = SourceAddress.fromJson(var0.optJSONObject("address"));
         var2 = var1;
      }

      return var2;
   }

   public SourceAddress getAddress() {
      return this.mAddress;
   }

   public String getName() {
      return this.mName;
   }

   public String getPhone() {
      return this.mPhone;
   }

   public JSONObject toJson() {
      JSONObject var1 = new JSONObject();
      StripeJsonUtils.putStringIfNotNull(var1, "name", this.mName);
      StripeJsonUtils.putStringIfNotNull(var1, "phone", this.mPhone);
      putStripeJsonModelIfNotNull(var1, "address", this.mAddress);
      return var1;
   }

   public Map toMap() {
      HashMap var1 = new HashMap();
      var1.put("name", this.mName);
      var1.put("phone", this.mPhone);
      putStripeJsonModelMapIfNotNull(var1, "address", this.mAddress);
      StripeNetworkUtils.removeNullAndEmptyParams(var1);
      return var1;
   }
}
