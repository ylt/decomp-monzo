package com.stripe.android.model;

import com.stripe.android.util.StripeJsonUtils;
import com.stripe.android.util.StripeNetworkUtils;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class SourceSepaDebitData extends StripeSourceTypeModel {
   private static final String FIELD_BANK_CODE = "bank_code";
   private static final String FIELD_BRANCH_CODE = "branch_code";
   private static final String FIELD_COUNTRY = "country";
   private static final String FIELD_FINGERPRINT = "fingerprint";
   private static final String FIELD_LAST4 = "last4";
   private static final String FIELD_MANDATE_REFERENCE = "mandate_reference";
   private static final String FIELD_MANDATE_URL = "mandate_url";
   private String mBankCode;
   private String mBranchCode;
   private String mCountry;
   private String mFingerPrint;
   private String mLast4;
   private String mMandateReference;
   private String mMandateUrl;

   private SourceSepaDebitData() {
      this.addStandardFields(new String[]{"bank_code", "branch_code", "country", "fingerprint", "last4", "mandate_reference", "mandate_url"});
   }

   public static SourceSepaDebitData fromJson(JSONObject var0) {
      SourceSepaDebitData var3;
      if(var0 == null) {
         var3 = null;
      } else {
         SourceSepaDebitData var1 = new SourceSepaDebitData();
         var1.setBankCode(StripeJsonUtils.optString(var0, "bank_code")).setBranchCode(StripeJsonUtils.optString(var0, "branch_code")).setCountry(StripeJsonUtils.optString(var0, "country")).setFingerPrint(StripeJsonUtils.optString(var0, "fingerprint")).setLast4(StripeJsonUtils.optString(var0, "last4")).setMandateReference(StripeJsonUtils.optString(var0, "mandate_reference")).setMandateUrl(StripeJsonUtils.optString(var0, "mandate_url"));
         Map var2 = jsonObjectToMapWithoutKeys(var0, var1.mStandardFields);
         var3 = var1;
         if(var2 != null) {
            var1.setAdditionalFields(var2);
            var3 = var1;
         }
      }

      return var3;
   }

   static SourceSepaDebitData fromString(String var0) {
      SourceSepaDebitData var3;
      try {
         JSONObject var1 = new JSONObject(var0);
         var3 = fromJson(var1);
      } catch (JSONException var2) {
         var3 = null;
      }

      return var3;
   }

   private SourceSepaDebitData setBankCode(String var1) {
      this.mBankCode = var1;
      return this;
   }

   private SourceSepaDebitData setBranchCode(String var1) {
      this.mBranchCode = var1;
      return this;
   }

   private SourceSepaDebitData setCountry(String var1) {
      this.mCountry = var1;
      return this;
   }

   private SourceSepaDebitData setFingerPrint(String var1) {
      this.mFingerPrint = var1;
      return this;
   }

   private SourceSepaDebitData setLast4(String var1) {
      this.mLast4 = var1;
      return this;
   }

   private SourceSepaDebitData setMandateReference(String var1) {
      this.mMandateReference = var1;
      return this;
   }

   private SourceSepaDebitData setMandateUrl(String var1) {
      this.mMandateUrl = var1;
      return this;
   }

   public String getBankCode() {
      return this.mBankCode;
   }

   public String getBranchCode() {
      return this.mBranchCode;
   }

   public String getCountry() {
      return this.mCountry;
   }

   public String getFingerPrint() {
      return this.mFingerPrint;
   }

   public String getLast4() {
      return this.mLast4;
   }

   public String getMandateReference() {
      return this.mMandateReference;
   }

   public String getMandateUrl() {
      return this.mMandateUrl;
   }

   public JSONObject toJson() {
      JSONObject var1 = new JSONObject();
      StripeJsonUtils.putStringIfNotNull(var1, "bank_code", this.mBankCode);
      StripeJsonUtils.putStringIfNotNull(var1, "branch_code", this.mBranchCode);
      StripeJsonUtils.putStringIfNotNull(var1, "country", this.mCountry);
      StripeJsonUtils.putStringIfNotNull(var1, "fingerprint", this.mFingerPrint);
      StripeJsonUtils.putStringIfNotNull(var1, "last4", this.mLast4);
      StripeJsonUtils.putStringIfNotNull(var1, "mandate_reference", this.mMandateReference);
      StripeJsonUtils.putStringIfNotNull(var1, "mandate_url", this.mMandateUrl);
      putAdditionalFieldsIntoJsonObject(var1, this.mAdditionalFields);
      return var1;
   }

   public Map toMap() {
      HashMap var1 = new HashMap();
      var1.put("bank_code", this.mBankCode);
      var1.put("branch_code", this.mBranchCode);
      var1.put("country", this.mCountry);
      var1.put("fingerprint", this.mFingerPrint);
      var1.put("last4", this.mLast4);
      var1.put("mandate_reference", this.mMandateReference);
      var1.put("mandate_url", this.mMandateUrl);
      putAdditionalFieldsIntoMap(var1, this.mAdditionalFields);
      StripeNetworkUtils.removeNullAndEmptyParams(var1);
      return var1;
   }
}
