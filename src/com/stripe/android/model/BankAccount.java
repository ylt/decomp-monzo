package com.stripe.android.model;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class BankAccount {
   public static final String TYPE_COMPANY = "company";
   public static final String TYPE_INDIVIDUAL = "individual";
   private String mAccountHolderName;
   private String mAccountHolderType;
   private String mAccountNumber;
   private String mBankName;
   private String mCountryCode;
   private String mCurrency;
   private String mFingerprint;
   private String mLast4;
   private String mRoutingNumber;

   public BankAccount(String var1, String var2, String var3, String var4) {
      this.mAccountNumber = var1;
      this.mCountryCode = var2;
      this.mCurrency = var3;
      this.mRoutingNumber = var4;
   }

   public BankAccount(String var1, String var2, String var3, String var4, String var5, String var6, String var7, String var8) {
      this.mAccountHolderName = var1;
      this.mAccountHolderType = var2;
      this.mBankName = var3;
      this.mCountryCode = var4;
      this.mCurrency = var5;
      this.mFingerprint = var6;
      this.mLast4 = var7;
      this.mRoutingNumber = var8;
   }

   public String getAccountHolderName() {
      return this.mAccountHolderName;
   }

   public String getAccountHolderType() {
      return this.mAccountHolderType;
   }

   public String getAccountNumber() {
      return this.mAccountNumber;
   }

   public String getBankName() {
      return this.mBankName;
   }

   public String getCountryCode() {
      return this.mCountryCode;
   }

   public String getCurrency() {
      return this.mCurrency;
   }

   public String getFingerprint() {
      return this.mFingerprint;
   }

   public String getLast4() {
      return this.mLast4;
   }

   public String getRoutingNumber() {
      return this.mRoutingNumber;
   }

   public BankAccount setAccountHolderName(String var1) {
      this.mAccountHolderName = var1;
      return this;
   }

   public BankAccount setAccountHolderType(String var1) {
      this.mAccountHolderType = var1;
      return this;
   }

   @Retention(RetentionPolicy.SOURCE)
   public @interface BankAccountType {
   }
}
