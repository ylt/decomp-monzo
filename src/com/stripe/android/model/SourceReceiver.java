package com.stripe.android.model;

import com.stripe.android.util.StripeJsonUtils;
import com.stripe.android.util.StripeTextUtils;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class SourceReceiver extends StripeJsonModel {
   private static final String FIELD_ADDRESS = "address";
   private static final String FIELD_AMOUNT_CHARGED = "amount_charged";
   private static final String FIELD_AMOUNT_RECEIVED = "amount_received";
   private static final String FIELD_AMOUNT_RETURNED = "amount_returned";
   private String mAddress;
   private long mAmountCharged;
   private long mAmountReceived;
   private long mAmountReturned;

   SourceReceiver(String var1, long var2, long var4, long var6) {
      this.mAddress = var1;
      this.mAmountCharged = var2;
      this.mAmountReceived = var4;
      this.mAmountReturned = var6;
   }

   public static SourceReceiver fromJson(JSONObject var0) {
      SourceReceiver var1;
      if(var0 == null) {
         var1 = null;
      } else {
         var1 = new SourceReceiver(StripeJsonUtils.optString(var0, "address"), var0.optLong("amount_charged"), var0.optLong("amount_received"), var0.optLong("amount_returned"));
      }

      return var1;
   }

   public static SourceReceiver fromString(String var0) {
      SourceReceiver var3;
      try {
         JSONObject var1 = new JSONObject(var0);
         var3 = fromJson(var1);
      } catch (JSONException var2) {
         var3 = null;
      }

      return var3;
   }

   public String getAddress() {
      return this.mAddress;
   }

   public long getAmountCharged() {
      return this.mAmountCharged;
   }

   public long getAmountReceived() {
      return this.mAmountReceived;
   }

   public long getAmountReturned() {
      return this.mAmountReturned;
   }

   public void setAddress(String var1) {
      this.mAddress = var1;
   }

   public void setAmountCharged(long var1) {
      this.mAmountCharged = var1;
   }

   public void setAmountReceived(long var1) {
      this.mAmountReceived = var1;
   }

   public void setAmountReturned(long var1) {
      this.mAmountReturned = var1;
   }

   public JSONObject toJson() {
      JSONObject var2 = new JSONObject();
      StripeJsonUtils.putStringIfNotNull(var2, "address", this.mAddress);

      try {
         var2.put("amount_charged", this.mAmountCharged);
         var2.put("amount_received", this.mAmountReceived);
         var2.put("amount_returned", this.mAmountReturned);
      } catch (JSONException var3) {
         ;
      }

      return var2;
   }

   public Map toMap() {
      HashMap var1 = new HashMap();
      if(!StripeTextUtils.isBlank(this.mAddress)) {
         var1.put("address", this.mAddress);
      }

      var1.put("address", this.mAddress);
      var1.put("amount_charged", Long.valueOf(this.mAmountCharged));
      var1.put("amount_received", Long.valueOf(this.mAmountReceived));
      var1.put("amount_returned", Long.valueOf(this.mAmountReturned));
      return var1;
   }
}
