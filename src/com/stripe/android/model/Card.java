package com.stripe.android.model;

import com.stripe.android.util.CardUtils;
import com.stripe.android.util.DateUtils;
import com.stripe.android.util.StripeJsonUtils;
import com.stripe.android.util.StripeNetworkUtils;
import com.stripe.android.util.StripeTextUtils;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class Card extends StripeJsonModel implements StripePaymentSource {
   public static final String AMERICAN_EXPRESS = "American Express";
   public static final int CVC_LENGTH_AMERICAN_EXPRESS = 4;
   public static final int CVC_LENGTH_COMMON = 3;
   public static final String DINERS_CLUB = "Diners Club";
   public static final String DISCOVER = "Discover";
   private static final String FIELD_ADDRESS_CITY = "address_city";
   private static final String FIELD_ADDRESS_COUNTRY = "address_country";
   private static final String FIELD_ADDRESS_LINE1 = "address_line1";
   private static final String FIELD_ADDRESS_LINE1_CHECK = "address_line1_check";
   private static final String FIELD_ADDRESS_LINE2 = "address_line2";
   private static final String FIELD_ADDRESS_STATE = "address_state";
   private static final String FIELD_ADDRESS_ZIP = "address_zip";
   private static final String FIELD_ADDRESS_ZIP_CHECK = "address_zip_check";
   private static final String FIELD_BRAND = "brand";
   private static final String FIELD_COUNTRY = "country";
   private static final String FIELD_CURRENCY = "currency";
   private static final String FIELD_CUSTOMER = "customer";
   private static final String FIELD_CVC_CHECK = "cvc_check";
   private static final String FIELD_EXP_MONTH = "exp_month";
   private static final String FIELD_EXP_YEAR = "exp_year";
   private static final String FIELD_FINGERPRINT = "fingerprint";
   private static final String FIELD_FUNDING = "funding";
   private static final String FIELD_ID = "id";
   private static final String FIELD_LAST4 = "last4";
   private static final String FIELD_NAME = "name";
   private static final String FIELD_OBJECT = "object";
   public static final String FUNDING_CREDIT = "credit";
   public static final String FUNDING_DEBIT = "debit";
   public static final String FUNDING_PREPAID = "prepaid";
   public static final String FUNDING_UNKNOWN = "unknown";
   public static final String JCB = "JCB";
   public static final String MASTERCARD = "MasterCard";
   public static final int MAX_LENGTH_AMERICAN_EXPRESS = 15;
   public static final int MAX_LENGTH_DINERS_CLUB = 14;
   public static final int MAX_LENGTH_STANDARD = 16;
   public static final String[] PREFIXES_AMERICAN_EXPRESS = new String[]{"34", "37"};
   public static final String[] PREFIXES_DINERS_CLUB = new String[]{"300", "301", "302", "303", "304", "305", "309", "36", "38", "39"};
   public static final String[] PREFIXES_DISCOVER = new String[]{"60", "62", "64", "65"};
   public static final String[] PREFIXES_JCB = new String[]{"35"};
   public static final String[] PREFIXES_MASTERCARD = new String[]{"2221", "2222", "2223", "2224", "2225", "2226", "2227", "2228", "2229", "223", "224", "225", "226", "227", "228", "229", "23", "24", "25", "26", "270", "271", "2720", "50", "51", "52", "53", "54", "55"};
   public static final String[] PREFIXES_VISA = new String[]{"4"};
   public static final String UNKNOWN = "Unknown";
   public static final String VALUE_CARD = "card";
   public static final String VISA = "Visa";
   private String addressCity;
   private String addressCountry;
   private String addressLine1;
   private String addressLine1Check;
   private String addressLine2;
   private String addressState;
   private String addressZip;
   private String addressZipCheck;
   private String brand;
   private String country;
   private String currency;
   private String customerId;
   private String cvc;
   private String cvcCheck;
   private Integer expMonth;
   private Integer expYear;
   private String fingerprint;
   private String funding;
   private String id;
   private String last4;
   private List loggingTokens;
   private String name;
   private String number;

   private Card(Card.Builder var1) {
      this.loggingTokens = new ArrayList();
      this.number = StripeTextUtils.nullIfBlank(this.normalizeCardNumber(var1.number));
      this.expMonth = var1.expMonth;
      this.expYear = var1.expYear;
      this.cvc = StripeTextUtils.nullIfBlank(var1.cvc);
      this.name = StripeTextUtils.nullIfBlank(var1.name);
      this.addressLine1 = StripeTextUtils.nullIfBlank(var1.addressLine1);
      this.addressLine1Check = StripeTextUtils.nullIfBlank(var1.addressLine1Check);
      this.addressLine2 = StripeTextUtils.nullIfBlank(var1.addressLine2);
      this.addressCity = StripeTextUtils.nullIfBlank(var1.addressCity);
      this.addressState = StripeTextUtils.nullIfBlank(var1.addressState);
      this.addressZip = StripeTextUtils.nullIfBlank(var1.addressZip);
      this.addressZipCheck = StripeTextUtils.nullIfBlank(var1.addressZipCheck);
      this.addressCountry = StripeTextUtils.nullIfBlank(var1.addressCountry);
      String var2;
      if(StripeTextUtils.nullIfBlank(var1.last4) == null) {
         var2 = this.getLast4();
      } else {
         var2 = var1.last4;
      }

      this.last4 = var2;
      if(StripeTextUtils.asCardBrand(var1.brand) == null) {
         var2 = this.getBrand();
      } else {
         var2 = var1.brand;
      }

      this.brand = var2;
      this.fingerprint = StripeTextUtils.nullIfBlank(var1.fingerprint);
      this.funding = StripeTextUtils.asFundingType(var1.funding);
      this.country = StripeTextUtils.nullIfBlank(var1.country);
      this.currency = StripeTextUtils.nullIfBlank(var1.currency);
      this.customerId = StripeTextUtils.nullIfBlank(var1.customer);
      this.cvcCheck = StripeTextUtils.nullIfBlank(var1.cvcCheck);
      this.id = StripeTextUtils.nullIfBlank(var1.id);
   }

   // $FF: synthetic method
   Card(Card.Builder var1, Object var2) {
      this(var1);
   }

   public Card(String var1, Integer var2, Integer var3, String var4) {
      this(var1, var2, var3, var4, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null);
   }

   public Card(String var1, Integer var2, Integer var3, String var4, String var5, String var6, String var7, String var8, String var9, String var10, String var11, String var12) {
      this(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10, var11, (String)null, (String)null, (String)null, (String)null, (String)null, var12, (String)null);
   }

   public Card(String var1, Integer var2, Integer var3, String var4, String var5, String var6, String var7, String var8, String var9, String var10, String var11, String var12, String var13, String var14, String var15, String var16, String var17, String var18) {
      this.loggingTokens = new ArrayList();
      this.number = StripeTextUtils.nullIfBlank(this.normalizeCardNumber(var1));
      this.expMonth = var2;
      this.expYear = var3;
      this.cvc = StripeTextUtils.nullIfBlank(var4);
      this.name = StripeTextUtils.nullIfBlank(var5);
      this.addressLine1 = StripeTextUtils.nullIfBlank(var6);
      this.addressLine2 = StripeTextUtils.nullIfBlank(var7);
      this.addressCity = StripeTextUtils.nullIfBlank(var8);
      this.addressState = StripeTextUtils.nullIfBlank(var9);
      this.addressZip = StripeTextUtils.nullIfBlank(var10);
      this.addressCountry = StripeTextUtils.nullIfBlank(var11);
      var1 = var12;
      if(StripeTextUtils.asCardBrand(var12) == null) {
         var1 = this.getBrand();
      }

      this.brand = var1;
      var1 = var13;
      if(StripeTextUtils.nullIfBlank(var13) == null) {
         var1 = this.getLast4();
      }

      this.last4 = var1;
      this.fingerprint = StripeTextUtils.nullIfBlank(var14);
      this.funding = StripeTextUtils.asFundingType(var15);
      this.country = StripeTextUtils.nullIfBlank(var16);
      this.currency = StripeTextUtils.nullIfBlank(var17);
      this.id = StripeTextUtils.nullIfBlank(var18);
   }

   public static Card fromJson(JSONObject var0) {
      Integer var2 = null;
      Card var1 = var2;
      if(var0 != null) {
         if(!"card".equals(var0.optString("object"))) {
            var1 = var2;
         } else {
            var2 = StripeJsonUtils.optInteger(var0, "exp_month");
            Integer var3 = StripeJsonUtils.optInteger(var0, "exp_year");
            Integer var4 = var2;
            if(var2 != null) {
               label31: {
                  if(var2.intValue() >= 1) {
                     var4 = var2;
                     if(var2.intValue() <= 12) {
                        break label31;
                     }
                  }

                  var4 = null;
               }
            }

            var2 = var3;
            if(var3 != null) {
               var2 = var3;
               if(var3.intValue() < 0) {
                  var2 = null;
               }
            }

            Card.Builder var5 = new Card.Builder((String)null, var4, var2, (String)null);
            var5.addressCity(StripeJsonUtils.optString(var0, "address_city"));
            var5.addressLine1(StripeJsonUtils.optString(var0, "address_line1"));
            var5.addressLine1Check(StripeJsonUtils.optString(var0, "address_line1_check"));
            var5.addressLine2(StripeJsonUtils.optString(var0, "address_line2"));
            var5.addressCountry(StripeJsonUtils.optString(var0, "address_country"));
            var5.addressState(StripeJsonUtils.optString(var0, "address_state"));
            var5.addressZip(StripeJsonUtils.optString(var0, "address_zip"));
            var5.addressZipCheck(StripeJsonUtils.optString(var0, "address_zip_check"));
            var5.brand(StripeTextUtils.asCardBrand(StripeJsonUtils.optString(var0, "brand")));
            var5.country(StripeJsonUtils.optCountryCode(var0, "country"));
            var5.customer(StripeJsonUtils.optString(var0, "customer"));
            var5.currency(StripeJsonUtils.optCurrency(var0, "currency"));
            var5.cvcCheck(StripeJsonUtils.optString(var0, "cvc_check"));
            var5.funding(StripeTextUtils.asFundingType(StripeJsonUtils.optString(var0, "funding")));
            var5.fingerprint(StripeJsonUtils.optString(var0, "fingerprint"));
            var5.id(StripeJsonUtils.optString(var0, "id"));
            var5.last4(StripeJsonUtils.optString(var0, "last4"));
            var5.name(StripeJsonUtils.optString(var0, "name"));
            var1 = var5.build();
         }
      }

      return var1;
   }

   public static Card fromString(String var0) {
      Card var3;
      try {
         JSONObject var1 = new JSONObject(var0);
         var3 = fromJson(var1);
      } catch (JSONException var2) {
         var3 = null;
      }

      return var3;
   }

   private String normalizeCardNumber(String var1) {
      if(var1 == null) {
         var1 = null;
      } else {
         var1 = var1.trim().replaceAll("\\s+|-", "");
      }

      return var1;
   }

   public Card addLoggingToken(String var1) {
      this.loggingTokens.add(var1);
      return this;
   }

   public String getAddressCity() {
      return this.addressCity;
   }

   public String getAddressCountry() {
      return this.addressCountry;
   }

   public String getAddressLine1() {
      return this.addressLine1;
   }

   public String getAddressLine1Check() {
      return this.addressLine1Check;
   }

   public String getAddressLine2() {
      return this.addressLine2;
   }

   public String getAddressState() {
      return this.addressState;
   }

   public String getAddressZip() {
      return this.addressZip;
   }

   public String getAddressZipCheck() {
      return this.addressZipCheck;
   }

   public String getBrand() {
      if(StripeTextUtils.isBlank(this.brand) && !StripeTextUtils.isBlank(this.number)) {
         String var1;
         if(StripeTextUtils.hasAnyPrefix(this.number, PREFIXES_AMERICAN_EXPRESS)) {
            var1 = "American Express";
         } else if(StripeTextUtils.hasAnyPrefix(this.number, PREFIXES_DISCOVER)) {
            var1 = "Discover";
         } else if(StripeTextUtils.hasAnyPrefix(this.number, PREFIXES_JCB)) {
            var1 = "JCB";
         } else if(StripeTextUtils.hasAnyPrefix(this.number, PREFIXES_DINERS_CLUB)) {
            var1 = "Diners Club";
         } else if(StripeTextUtils.hasAnyPrefix(this.number, PREFIXES_VISA)) {
            var1 = "Visa";
         } else if(StripeTextUtils.hasAnyPrefix(this.number, PREFIXES_MASTERCARD)) {
            var1 = "MasterCard";
         } else {
            var1 = "Unknown";
         }

         this.brand = var1;
      }

      return this.brand;
   }

   public String getCVC() {
      return this.cvc;
   }

   public String getCountry() {
      return this.country;
   }

   public String getCurrency() {
      return this.currency;
   }

   public String getCustomerId() {
      return this.customerId;
   }

   public String getCvcCheck() {
      return this.cvcCheck;
   }

   public Integer getExpMonth() {
      return this.expMonth;
   }

   public Integer getExpYear() {
      return this.expYear;
   }

   public String getFingerprint() {
      return this.fingerprint;
   }

   public String getFunding() {
      return this.funding;
   }

   public String getId() {
      return this.id;
   }

   public String getLast4() {
      String var1;
      if(!StripeTextUtils.isBlank(this.last4)) {
         var1 = this.last4;
      } else if(this.number != null && this.number.length() > 4) {
         this.last4 = this.number.substring(this.number.length() - 4, this.number.length());
         var1 = this.last4;
      } else {
         var1 = null;
      }

      return var1;
   }

   public List getLoggingTokens() {
      return this.loggingTokens;
   }

   public String getName() {
      return this.name;
   }

   public String getNumber() {
      return this.number;
   }

   @Deprecated
   public String getType() {
      return this.getBrand();
   }

   public void setAddressCity(String var1) {
      this.addressCity = var1;
   }

   public void setAddressCountry(String var1) {
      this.addressCountry = var1;
   }

   public void setAddressLine1(String var1) {
      this.addressLine1 = var1;
   }

   public void setAddressLine2(String var1) {
      this.addressLine2 = var1;
   }

   public void setAddressState(String var1) {
      this.addressState = var1;
   }

   public void setAddressZip(String var1) {
      this.addressZip = var1;
   }

   @Deprecated
   public void setCVC(String var1) {
      this.cvc = var1;
   }

   public void setCurrency(String var1) {
      this.currency = var1;
   }

   @Deprecated
   public void setExpMonth(Integer var1) {
      this.expMonth = var1;
   }

   @Deprecated
   public void setExpYear(Integer var1) {
      this.expYear = var1;
   }

   public void setName(String var1) {
      this.name = var1;
   }

   @Deprecated
   public void setNumber(String var1) {
      this.number = var1;
      this.brand = null;
      this.last4 = null;
   }

   public JSONObject toJson() {
      JSONObject var1 = new JSONObject();
      StripeJsonUtils.putStringIfNotNull(var1, "name", this.name);
      StripeJsonUtils.putStringIfNotNull(var1, "address_city", this.addressCity);
      StripeJsonUtils.putStringIfNotNull(var1, "address_country", this.addressCountry);
      StripeJsonUtils.putStringIfNotNull(var1, "address_line1", this.addressLine1);
      StripeJsonUtils.putStringIfNotNull(var1, "address_line1_check", this.addressLine1Check);
      StripeJsonUtils.putStringIfNotNull(var1, "address_line2", this.addressLine2);
      StripeJsonUtils.putStringIfNotNull(var1, "address_state", this.addressState);
      StripeJsonUtils.putStringIfNotNull(var1, "address_zip", this.addressZip);
      StripeJsonUtils.putStringIfNotNull(var1, "address_zip_check", this.addressZipCheck);
      StripeJsonUtils.putStringIfNotNull(var1, "brand", this.brand);
      StripeJsonUtils.putStringIfNotNull(var1, "currency", this.currency);
      StripeJsonUtils.putStringIfNotNull(var1, "country", this.country);
      StripeJsonUtils.putStringIfNotNull(var1, "customer", this.customerId);
      StripeJsonUtils.putIntegerIfNotNull(var1, "exp_month", this.expMonth);
      StripeJsonUtils.putIntegerIfNotNull(var1, "exp_year", this.expYear);
      StripeJsonUtils.putStringIfNotNull(var1, "fingerprint", this.fingerprint);
      StripeJsonUtils.putStringIfNotNull(var1, "funding", this.funding);
      StripeJsonUtils.putStringIfNotNull(var1, "cvc_check", this.cvcCheck);
      StripeJsonUtils.putStringIfNotNull(var1, "last4", this.last4);
      StripeJsonUtils.putStringIfNotNull(var1, "id", this.id);
      StripeJsonUtils.putStringIfNotNull(var1, "object", "card");
      return var1;
   }

   public Map toMap() {
      HashMap var1 = new HashMap();
      var1.put("name", this.name);
      var1.put("address_city", this.addressCity);
      var1.put("address_country", this.addressCountry);
      var1.put("address_line1", this.addressLine1);
      var1.put("address_line1_check", this.addressLine1Check);
      var1.put("address_line2", this.addressLine2);
      var1.put("address_state", this.addressState);
      var1.put("address_zip", this.addressZip);
      var1.put("address_zip_check", this.addressZipCheck);
      var1.put("brand", this.brand);
      var1.put("currency", this.currency);
      var1.put("country", this.country);
      var1.put("customer", this.customerId);
      var1.put("cvc_check", this.cvcCheck);
      var1.put("exp_month", this.expMonth);
      var1.put("exp_year", this.expYear);
      var1.put("fingerprint", this.fingerprint);
      var1.put("funding", this.funding);
      var1.put("id", this.id);
      var1.put("last4", this.last4);
      var1.put("object", "card");
      StripeNetworkUtils.removeNullAndEmptyParams(var1);
      return var1;
   }

   public boolean validateCVC() {
      boolean var3 = true;
      boolean var2 = false;
      if(!StripeTextUtils.isBlank(this.cvc)) {
         String var5 = this.cvc.trim();
         String var4 = this.getBrand();
         boolean var1;
         if((var4 != null || var5.length() < 3 || var5.length() > 4) && (!"American Express".equals(var4) || var5.length() != 4) && var5.length() != 3) {
            var1 = false;
         } else {
            var1 = true;
         }

         if(StripeTextUtils.isWholePositiveNumber(var5) && var1) {
            var2 = var3;
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public boolean validateCard() {
      boolean var1 = true;
      if(this.cvc == null) {
         if(!this.validateNumber() || !this.validateExpiryDate()) {
            var1 = false;
         }
      } else if(!this.validateNumber() || !this.validateExpiryDate() || !this.validateCVC()) {
         var1 = false;
      }

      return var1;
   }

   public boolean validateExpMonth() {
      boolean var1 = true;
      if(this.expMonth == null || this.expMonth.intValue() < 1 || this.expMonth.intValue() > 12) {
         var1 = false;
      }

      return var1;
   }

   public boolean validateExpYear() {
      boolean var1;
      if(this.expYear != null && !DateUtils.hasYearPassed(this.expYear.intValue())) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public boolean validateExpiryDate() {
      boolean var2 = false;
      boolean var1;
      if(!this.validateExpMonth()) {
         var1 = var2;
      } else {
         var1 = var2;
         if(this.validateExpYear()) {
            var1 = var2;
            if(!DateUtils.hasMonthPassed(this.expYear.intValue(), this.expMonth.intValue())) {
               var1 = true;
            }
         }
      }

      return var1;
   }

   public boolean validateNumber() {
      return CardUtils.isValidCardNumber(this.number);
   }

   public static class Builder {
      private String addressCity;
      private String addressCountry;
      private String addressLine1;
      private String addressLine1Check;
      private String addressLine2;
      private String addressState;
      private String addressZip;
      private String addressZipCheck;
      private String brand;
      private String country;
      private String currency;
      private String customer;
      private final String cvc;
      private String cvcCheck;
      private final Integer expMonth;
      private final Integer expYear;
      private String fingerprint;
      private String funding;
      private String id;
      private String last4;
      private String name;
      private final String number;

      public Builder(String var1, Integer var2, Integer var3, String var4) {
         this.number = var1;
         this.expMonth = var2;
         this.expYear = var3;
         this.cvc = var4;
      }

      public Card.Builder addressCity(String var1) {
         this.addressCity = var1;
         return this;
      }

      public Card.Builder addressCountry(String var1) {
         this.addressCountry = var1;
         return this;
      }

      public Card.Builder addressLine1(String var1) {
         this.addressLine1 = var1;
         return this;
      }

      public Card.Builder addressLine1Check(String var1) {
         this.addressLine1Check = var1;
         return this;
      }

      public Card.Builder addressLine2(String var1) {
         this.addressLine2 = var1;
         return this;
      }

      public Card.Builder addressState(String var1) {
         this.addressState = var1;
         return this;
      }

      public Card.Builder addressZip(String var1) {
         this.addressZip = var1;
         return this;
      }

      public Card.Builder addressZipCheck(String var1) {
         this.addressZipCheck = var1;
         return this;
      }

      public Card.Builder brand(String var1) {
         this.brand = var1;
         return this;
      }

      public Card build() {
         return new Card(this);
      }

      public Card.Builder country(String var1) {
         this.country = var1;
         return this;
      }

      public Card.Builder currency(String var1) {
         this.currency = var1;
         return this;
      }

      public Card.Builder customer(String var1) {
         this.customer = var1;
         return this;
      }

      public Card.Builder cvcCheck(String var1) {
         this.cvcCheck = var1;
         return this;
      }

      public Card.Builder fingerprint(String var1) {
         this.fingerprint = var1;
         return this;
      }

      public Card.Builder funding(String var1) {
         this.funding = var1;
         return this;
      }

      public Card.Builder id(String var1) {
         this.id = var1;
         return this;
      }

      public Card.Builder last4(String var1) {
         this.last4 = var1;
         return this;
      }

      public Card.Builder name(String var1) {
         this.name = var1;
         return this;
      }
   }

   @Retention(RetentionPolicy.SOURCE)
   public @interface CardBrand {
   }

   @Retention(RetentionPolicy.SOURCE)
   public @interface FundingType {
   }
}
