package com.stripe.android.util;

import com.stripe.android.time.Clock;
import java.util.Calendar;
import java.util.Locale;

public class DateUtils {
   static final int MAX_VALID_YEAR = 9980;

   public static int convertTwoDigitYearToFour(int var0) {
      return convertTwoDigitYearToFour(var0, Calendar.getInstance());
   }

   static int convertTwoDigitYearToFour(int var0, Calendar var1) {
      int var4 = var1.get(1);
      int var3 = var4 / 100;
      int var2;
      if(var4 % 100 > 80 && var0 < 20) {
         var2 = var3 + 1;
      } else {
         var2 = var3;
         if(var4 % 100 < 20) {
            var2 = var3;
            if(var0 > 80) {
               var2 = var3 - 1;
            }
         }
      }

      return var2 * 100 + var0;
   }

   public static String createDateStringFromIntegerInput(int var0, int var1) {
      String var2 = String.valueOf(var0);
      String var3 = var2;
      if(var2.length() == 1) {
         var3 = "0" + var2;
      }

      String var4 = String.valueOf(var1);
      if(var4.length() == 3) {
         var2 = "";
      } else {
         if(var4.length() > 2) {
            var2 = var4.substring(var4.length() - 2);
         } else {
            var2 = var4;
            if(var4.length() == 1) {
               var2 = "0" + var4;
            }
         }

         var2 = var3 + var2;
      }

      return var2;
   }

   public static boolean hasMonthPassed(int var0, int var1) {
      boolean var2 = true;
      if(!hasYearPassed(var0)) {
         Calendar var3 = Clock.getCalendarInstance();
         if(normalizeYear(var0) != var3.get(1) || var1 >= var3.get(2) + 1) {
            var2 = false;
         }
      }

      return var2;
   }

   public static boolean hasYearPassed(int var0) {
      boolean var1 = true;
      if(normalizeYear(var0) >= Clock.getCalendarInstance().get(1)) {
         var1 = false;
      }

      return var1;
   }

   public static boolean isExpiryDataValid(int var0, int var1) {
      return isExpiryDataValid(var0, var1, Calendar.getInstance());
   }

   static boolean isExpiryDataValid(int var0, int var1, Calendar var2) {
      boolean var5 = true;
      boolean var6 = false;
      boolean var4 = var6;
      if(var0 >= 1) {
         if(var0 > 12) {
            var4 = var6;
         } else {
            var4 = var6;
            if(var1 >= 0) {
               var4 = var6;
               if(var1 <= 9980) {
                  int var3 = var2.get(1);
                  var4 = var6;
                  if(var1 >= var3) {
                     if(var1 > var3) {
                        var4 = true;
                     } else if(var0 >= var2.get(2) + 1) {
                        var4 = var5;
                     } else {
                        var4 = false;
                     }
                  }
               }
            }
         }
      }

      return var4;
   }

   public static boolean isValidMonth(String var0) {
      boolean var3 = false;
      boolean var2;
      if(var0 == null) {
         var2 = var3;
      } else {
         int var1;
         try {
            var1 = Integer.parseInt(var0);
         } catch (NumberFormatException var4) {
            var2 = var3;
            return var2;
         }

         var2 = var3;
         if(var1 > 0) {
            var2 = var3;
            if(var1 <= 12) {
               var2 = true;
            }
         }
      }

      return var2;
   }

   private static int normalizeYear(int var0) {
      int var1 = var0;
      if(var0 < 100) {
         var1 = var0;
         if(var0 >= 0) {
            String var2 = String.valueOf(Clock.getCalendarInstance().get(1));
            var2 = var2.substring(0, var2.length() - 2);
            var1 = Integer.parseInt(String.format(Locale.US, "%s%02d", new Object[]{var2, Integer.valueOf(var0)}));
         }
      }

      return var1;
   }

   public static String[] separateDateStringParts(String var0) {
      String[] var1 = new String[2];
      if(var0.length() >= 2) {
         var1[0] = var0.substring(0, 2);
         var1[1] = var0.substring(2);
      } else {
         var1[0] = var0;
         var1[1] = "";
      }

      return var1;
   }
}
