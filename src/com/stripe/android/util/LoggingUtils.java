package com.stripe.android.util;

import android.os.Build;
import android.os.Build.VERSION;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class LoggingUtils {
   private static final String ANALYTICS_NAME = "stripe_android";
   private static final String ANALYTICS_PREFIX = "analytics";
   private static final String ANALYTICS_VERSION = "1.0";
   public static final String ANDROID_PAY_TOKEN = "AndroidPay";
   public static final String CARD_WIDGET_TOKEN = "CardInputView";
   public static final String EVENT_SOURCE_CREATION = "source_creation";
   public static final String EVENT_TOKEN_CREATION = "token_creation";
   static final String FIELD_ANALYTICS_UA = "analytics_ua";
   static final String FIELD_BINDINGS_VERSION = "bindings_version";
   static final String FIELD_DEVICE_TYPE = "device_type";
   static final String FIELD_EVENT = "event";
   static final String FIELD_OS_NAME = "os_name";
   static final String FIELD_OS_RELEASE = "os_release";
   static final String FIELD_OS_VERSION = "os_version";
   public static final String FIELD_PRODUCT_USAGE = "product_usage";
   static final String FIELD_PUBLISHABLE_KEY = "publishable_key";
   static final String FIELD_SOURCE_TYPE = "source_type";
   static final String FIELD_TOKEN_TYPE = "token_type";
   public static final String PII_TOKEN = "PII";
   public static final Set VALID_LOGGING_TOKENS = new HashSet();
   static final Set VALID_PARAM_FIELDS;

   static {
      VALID_LOGGING_TOKENS.add("AndroidPay");
      VALID_LOGGING_TOKENS.add("CardInputView");
      VALID_PARAM_FIELDS = new HashSet();
      VALID_PARAM_FIELDS.add("analytics_ua");
      VALID_PARAM_FIELDS.add("bindings_version");
      VALID_PARAM_FIELDS.add("device_type");
      VALID_PARAM_FIELDS.add("event");
      VALID_PARAM_FIELDS.add("os_version");
      VALID_PARAM_FIELDS.add("os_name");
      VALID_PARAM_FIELDS.add("os_release");
      VALID_PARAM_FIELDS.add("product_usage");
      VALID_PARAM_FIELDS.add("publishable_key");
      VALID_PARAM_FIELDS.add("source_type");
      VALID_PARAM_FIELDS.add("token_type");
   }

   static String getAnalyticsUa() {
      return "analytics.stripe_android-1.0";
   }

   static String getDeviceLoggingString() {
      StringBuilder var0 = new StringBuilder();
      var0.append(Build.MANUFACTURER).append('_').append(Build.BRAND).append('_').append(Build.MODEL);
      return var0.toString();
   }

   public static Map getEventLoggingParams(List var0, String var1, String var2, String var3, String var4) {
      HashMap var5 = new HashMap();
      var5.put("analytics_ua", getAnalyticsUa());
      var5.put("event", getEventParamName(var4));
      var5.put("publishable_key", var3);
      var5.put("os_name", VERSION.CODENAME);
      var5.put("os_release", VERSION.RELEASE);
      var5.put("os_version", Integer.valueOf(VERSION.SDK_INT));
      var5.put("device_type", getDeviceLoggingString());
      var5.put("bindings_version", "4.1.3");
      if(var0 != null) {
         var5.put("product_usage", var0);
      }

      if(var1 != null) {
         var5.put("source_type", var1);
      }

      if(var2 != null) {
         var5.put("token_type", var2);
      } else if(var1 == null) {
         var5.put("token_type", "unknown");
      }

      return var5;
   }

   static String getEventParamName(String var0) {
      return "stripe_android." + var0;
   }

   public static Map getSourceCreationParams(String var0, String var1) {
      return getEventLoggingParams((List)null, var1, (String)null, var0, "source_creation");
   }

   public static Map getTokenCreationParams(List var0, String var1, String var2) {
      return getEventLoggingParams(var0, (String)null, var2, var1, "token_creation");
   }

   @Retention(RetentionPolicy.SOURCE)
   public @interface LoggingEventName {
   }

   @Retention(RetentionPolicy.SOURCE)
   public @interface LoggingToken {
   }
}
