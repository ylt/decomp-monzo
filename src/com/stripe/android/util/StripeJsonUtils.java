package com.stripe.android.util;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class StripeJsonUtils {
   static final String EMPTY = "";
   static final String NULL = "null";

   public static String getString(JSONObject var0, String var1) throws JSONException {
      return nullIfNullOrEmpty(var0.getString(var1));
   }

   public static List jsonArrayToList(JSONArray param0) {
      // $FF: Couldn't be decompiled
   }

   public static Map jsonObjectToMap(JSONObject var0) {
      HashMap var5;
      if(var0 == null) {
         var5 = null;
      } else {
         HashMap var1 = new HashMap();
         Iterator var4 = var0.keys();

         while(var4.hasNext()) {
            String var2 = (String)var4.next();
            Object var3 = var0.opt(var2);
            if(!"null".equals(var3) && var3 != null) {
               if(var3 instanceof JSONObject) {
                  var1.put(var2, jsonObjectToMap((JSONObject)var3));
               } else if(var3 instanceof JSONArray) {
                  var1.put(var2, jsonArrayToList((JSONArray)var3));
               } else {
                  var1.put(var2, var3);
               }
            }
         }

         var5 = var1;
      }

      return var5;
   }

   public static Map jsonObjectToStringMap(JSONObject var0) {
      HashMap var5;
      if(var0 == null) {
         var5 = null;
      } else {
         HashMap var1 = new HashMap();
         Iterator var3 = var0.keys();

         while(var3.hasNext()) {
            String var4 = (String)var3.next();
            Object var2 = var0.opt(var4);
            if(!"null".equals(var2) && var2 != null) {
               var1.put(var4, var2.toString());
            }
         }

         var5 = var1;
      }

      return var5;
   }

   public static JSONArray listToJsonArray(List var0) {
      JSONArray var4;
      if(var0 == null) {
         var4 = null;
      } else {
         JSONArray var1 = new JSONArray();
         Iterator var5 = var0.iterator();

         while(true) {
            while(var5.hasNext()) {
               Object var2 = var5.next();
               if(var2 instanceof Map) {
                  try {
                     var1.put(mapToJsonObject((Map)var2));
                  } catch (ClassCastException var3) {
                     ;
                  }
               } else if(var2 instanceof List) {
                  var1.put(listToJsonArray((List)var2));
               } else if(!(var2 instanceof Number) && !(var2 instanceof Boolean)) {
                  var1.put(var2.toString());
               } else {
                  var1.put(var2);
               }
            }

            var4 = var1;
            break;
         }
      }

      return var4;
   }

   public static JSONObject mapToJsonObject(Map param0) {
      // $FF: Couldn't be decompiled
   }

   public static String nullIfNullOrEmpty(String var0) {
      String var1;
      if(!"null".equals(var0)) {
         var1 = var0;
         if(!"".equals(var0)) {
            return var1;
         }
      }

      var1 = null;
      return var1;
   }

   public static Boolean optBoolean(JSONObject var0, String var1) {
      Boolean var2;
      if(!var0.has(var1)) {
         var2 = null;
      } else {
         var2 = Boolean.valueOf(var0.optBoolean(var1));
      }

      return var2;
   }

   public static String optCountryCode(JSONObject var0, String var1) {
      String var2 = nullIfNullOrEmpty(var0.optString(var1));
      if(var2 == null || var2.length() != 2) {
         var2 = null;
      }

      return var2;
   }

   public static String optCurrency(JSONObject var0, String var1) {
      String var2 = nullIfNullOrEmpty(var0.optString(var1));
      if(var2 == null || var2.length() != 3) {
         var2 = null;
      }

      return var2;
   }

   public static Map optHash(JSONObject var0, String var1) {
      var0 = var0.optJSONObject(var1);
      Map var2;
      if(var0 == null) {
         var2 = null;
      } else {
         var2 = jsonObjectToStringMap(var0);
      }

      return var2;
   }

   public static Integer optInteger(JSONObject var0, String var1) {
      Integer var2;
      if(!var0.has(var1)) {
         var2 = null;
      } else {
         var2 = Integer.valueOf(var0.optInt(var1));
      }

      return var2;
   }

   public static Long optLong(JSONObject var0, String var1) {
      Long var2;
      if(!var0.has(var1)) {
         var2 = null;
      } else {
         var2 = Long.valueOf(var0.optLong(var1));
      }

      return var2;
   }

   public static Map optMap(JSONObject var0, String var1) {
      var0 = var0.optJSONObject(var1);
      Map var2;
      if(var0 == null) {
         var2 = null;
      } else {
         var2 = jsonObjectToMap(var0);
      }

      return var2;
   }

   public static String optString(JSONObject var0, String var1) {
      return nullIfNullOrEmpty(var0.optString(var1));
   }

   public static void putBooleanIfNotNull(JSONObject var0, String var1, Boolean var2) {
      if(var2 != null) {
         try {
            var0.put(var1, var2.booleanValue());
         } catch (JSONException var3) {
            ;
         }
      }

   }

   public static void putIntegerIfNotNull(JSONObject var0, String var1, Integer var2) {
      if(var2 != null) {
         try {
            var0.put(var1, var2.intValue());
         } catch (JSONException var3) {
            ;
         }
      }

   }

   public static void putLongIfNotNull(JSONObject var0, String var1, Long var2) {
      if(var2 != null) {
         try {
            var0.put(var1, var2.longValue());
         } catch (JSONException var3) {
            ;
         }
      }

   }

   public static void putMapIfNotNull(JSONObject var0, String var1, Map var2) {
      if(var2 != null) {
         JSONObject var4 = mapToJsonObject(var2);
         if(var4 != null) {
            try {
               var0.put(var1, var4);
            } catch (JSONException var3) {
               ;
            }
         }
      }

   }

   public static void putObjectIfNotNull(JSONObject var0, String var1, JSONObject var2) {
      if(var2 != null) {
         try {
            var0.put(var1, var2);
         } catch (JSONException var3) {
            ;
         }
      }

   }

   public static void putStringHashIfNotNull(JSONObject var0, String var1, Map var2) {
      if(var2 != null) {
         JSONObject var4 = stringHashToJsonObject(var2);
         if(var4 != null) {
            try {
               var0.put(var1, var4);
            } catch (JSONException var3) {
               ;
            }
         }
      }

   }

   public static void putStringIfNotNull(JSONObject var0, String var1, String var2) {
      if(!StripeTextUtils.isBlank(var2)) {
         try {
            var0.put(var1, var2);
         } catch (JSONException var3) {
            ;
         }
      }

   }

   public static JSONObject stringHashToJsonObject(Map var0) {
      JSONObject var5;
      if(var0 == null) {
         var5 = null;
      } else {
         JSONObject var1 = new JSONObject();
         Iterator var2 = var0.keySet().iterator();

         while(var2.hasNext()) {
            String var3 = (String)var2.next();

            try {
               var1.put(var3, var0.get(var3));
            } catch (JSONException var4) {
               ;
            }
         }

         var5 = var1;
      }

      return var5;
   }
}
