package com.stripe.android.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class StripeTextUtils {
   private static final char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();
   private static int[] NON_DELETE_KEYS = new int[]{7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 76};

   public static String asBankAccountType(String var0) {
      if("company".equals(var0)) {
         var0 = "company";
      } else if("individual".equals(var0)) {
         var0 = "individual";
      } else {
         var0 = null;
      }

      return var0;
   }

   public static String asCardBrand(String var0) {
      if(isBlank(var0)) {
         var0 = null;
      } else if("American Express".equalsIgnoreCase(var0)) {
         var0 = "American Express";
      } else if("MasterCard".equalsIgnoreCase(var0)) {
         var0 = "MasterCard";
      } else if("Diners Club".equalsIgnoreCase(var0)) {
         var0 = "Diners Club";
      } else if("Discover".equalsIgnoreCase(var0)) {
         var0 = "Discover";
      } else if("JCB".equalsIgnoreCase(var0)) {
         var0 = "JCB";
      } else if("Visa".equalsIgnoreCase(var0)) {
         var0 = "Visa";
      } else {
         var0 = "Unknown";
      }

      return var0;
   }

   public static String asFundingType(String var0) {
      if(isBlank(var0)) {
         var0 = null;
      } else if("credit".equalsIgnoreCase(var0)) {
         var0 = "credit";
      } else if("debit".equalsIgnoreCase(var0)) {
         var0 = "debit";
      } else if("prepaid".equalsIgnoreCase(var0)) {
         var0 = "prepaid";
      } else {
         var0 = "unknown";
      }

      return var0;
   }

   public static String asTokenType(String var0) {
      String var1 = null;
      if(!isBlank(var0)) {
         if("card".equals(var0)) {
            var1 = "card";
         } else if("bank_account".equals(var0)) {
            var1 = "bank_account";
         } else if("pii".equals(var0)) {
            var1 = "pii";
         }
      }

      return var1;
   }

   private static String bytesToHex(byte[] var0) {
      char[] var3 = new char[var0.length * 2];

      for(int var1 = 0; var1 < var0.length; ++var1) {
         int var2 = var0[var1] & 255;
         var3[var1 * 2] = HEX_ARRAY[var2 >>> 4];
         var3[var1 * 2 + 1] = HEX_ARRAY[var2 & 15];
      }

      return new String(var3);
   }

   public static boolean hasAnyPrefix(String var0, String... var1) {
      boolean var5 = false;
      boolean var4;
      if(var0 == null) {
         var4 = var5;
      } else {
         int var3 = var1.length;
         int var2 = 0;

         while(true) {
            var4 = var5;
            if(var2 >= var3) {
               break;
            }

            if(var0.startsWith(var1[var2])) {
               var4 = true;
               break;
            }

            ++var2;
         }
      }

      return var4;
   }

   public static boolean isBlank(String var0) {
      boolean var1;
      if(var0 != null && var0.trim().length() != 0) {
         var1 = false;
      } else {
         var1 = true;
      }

      return var1;
   }

   public static boolean isWholePositiveNumber(String var0) {
      boolean var3 = false;
      boolean var2;
      if(var0 == null) {
         var2 = var3;
      } else {
         int var1 = 0;

         while(true) {
            if(var1 >= var0.length()) {
               var2 = true;
               break;
            }

            var2 = var3;
            if(!Character.isDigit(var0.charAt(var1))) {
               break;
            }

            ++var1;
         }
      }

      return var2;
   }

   public static String nullIfBlank(String var0) {
      String var1 = var0;
      if(isBlank(var0)) {
         var1 = null;
      }

      return var1;
   }

   public static String removeSpacesAndHyphens(String var0) {
      if(isBlank(var0)) {
         var0 = null;
      } else {
         var0 = var0.replaceAll("\\s|-", "");
      }

      return var0;
   }

   public static String shaHashInput(String var0) {
      Object var1 = null;
      if(isBlank(var0)) {
         var0 = (String)var1;
      } else {
         try {
            MessageDigest var2 = MessageDigest.getInstance("SHA-1");
            byte[] var5 = var0.getBytes("UTF-8");
            var2.update(var5, 0, var5.length);
            var0 = bytesToHex(var2.digest());
         } catch (NoSuchAlgorithmException var3) {
            var0 = (String)var1;
         } catch (UnsupportedEncodingException var4) {
            var0 = (String)var1;
         }
      }

      return var0;
   }
}
