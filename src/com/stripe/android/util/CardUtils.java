package com.stripe.android.util;

import com.stripe.android.model.Card;

public class CardUtils {
   public static final int CVC_LENGTH_AMEX = 4;
   public static final int CVC_LENGTH_COMMON = 3;
   public static final int LENGTH_AMERICAN_EXPRESS = 15;
   public static final int LENGTH_COMMON_CARD = 16;
   public static final int LENGTH_DINERS_CLUB = 14;

   public static String getPossibleCardType(String var0) {
      return getPossibleCardType(var0, true);
   }

   private static String getPossibleCardType(String var0, boolean var1) {
      if(StripeTextUtils.isBlank(var0)) {
         var0 = "Unknown";
      } else {
         String var2 = var0;
         if(var1) {
            var2 = StripeTextUtils.removeSpacesAndHyphens(var0);
         }

         if(StripeTextUtils.hasAnyPrefix(var2, Card.PREFIXES_AMERICAN_EXPRESS)) {
            var0 = "American Express";
         } else if(StripeTextUtils.hasAnyPrefix(var2, Card.PREFIXES_DISCOVER)) {
            var0 = "Discover";
         } else if(StripeTextUtils.hasAnyPrefix(var2, Card.PREFIXES_JCB)) {
            var0 = "JCB";
         } else if(StripeTextUtils.hasAnyPrefix(var2, Card.PREFIXES_DINERS_CLUB)) {
            var0 = "Diners Club";
         } else if(StripeTextUtils.hasAnyPrefix(var2, Card.PREFIXES_VISA)) {
            var0 = "Visa";
         } else if(StripeTextUtils.hasAnyPrefix(var2, Card.PREFIXES_MASTERCARD)) {
            var0 = "MasterCard";
         } else {
            var0 = "Unknown";
         }
      }

      return var0;
   }

   public static boolean isValidCardLength(String var0) {
      boolean var1 = false;
      if(var0 != null) {
         var1 = isValidCardLength(var0, getPossibleCardType(var0, false));
      }

      return var1;
   }

   public static boolean isValidCardLength(String var0, String var1) {
      boolean var4 = true;
      if(var0 != null && !"Unknown".equals(var1)) {
         int var3 = var0.length();
         byte var2 = -1;
         switch(var1.hashCode()) {
         case -993787207:
            if(var1.equals("Diners Club")) {
               var2 = 1;
            }
            break;
         case -298759312:
            if(var1.equals("American Express")) {
               var2 = 0;
            }
         }

         switch(var2) {
         case 0:
            if(var3 != 15) {
               var4 = false;
            }
            break;
         case 1:
            if(var3 != 14) {
               var4 = false;
            }
            break;
         default:
            if(var3 != 16) {
               var4 = false;
            }
         }
      } else {
         var4 = false;
      }

      return var4;
   }

   public static boolean isValidCardNumber(String var0) {
      var0 = StripeTextUtils.removeSpacesAndHyphens(var0);
      boolean var1;
      if(isValidLuhnNumber(var0) && isValidCardLength(var0)) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public static boolean isValidLuhnNumber(String var0) {
      boolean var8 = false;
      boolean var7;
      if(var0 == null) {
         var7 = var8;
      } else {
         int var5 = var0.length() - 1;
         int var4 = 0;
         boolean var2 = true;

         while(true) {
            if(var5 < 0) {
               if(var4 % 10 == 0) {
                  var7 = true;
               } else {
                  var7 = false;
               }
               break;
            }

            char var1 = var0.charAt(var5);
            var7 = var8;
            if(!Character.isDigit(var1)) {
               break;
            }

            int var6 = Character.getNumericValue(var1);
            if(!var2) {
               var2 = true;
            } else {
               var2 = false;
            }

            int var3 = var6;
            if(var2) {
               var3 = var6 * 2;
            }

            var6 = var3;
            if(var3 > 9) {
               var6 = var3 - 9;
            }

            var4 += var6;
            --var5;
         }
      }

      return var7;
   }

   public static String[] separateCardNumberGroups(String var0, String var1) {
      byte var4 = 10;
      int var2 = 0;
      int var3 = 0;
      String[] var6;
      String[] var7;
      if(var1.equals("American Express")) {
         var7 = new String[3];
         int var5 = var0.length();
         byte var8;
         if(var5 > 4) {
            var7[0] = var0.substring(0, 4);
            var8 = 4;
         } else {
            var8 = 0;
         }

         if(var5 > 10) {
            var7[1] = var0.substring(4, 10);
            var8 = var4;
         }

         while(var3 < 3) {
            if(var7[var3] == null) {
               var7[var3] = var0.substring(var8);
               break;
            }

            ++var3;
         }

         var6 = var7;
      } else {
         var7 = new String[4];

         for(var3 = 0; (var2 + 1) * 4 < var0.length(); ++var2) {
            var7[var2] = var0.substring(var3, (var2 + 1) * 4);
            var3 = (var2 + 1) * 4;
         }

         var7[var2] = var0.substring(var3);
         var6 = var7;
      }

      return var6;
   }
}
