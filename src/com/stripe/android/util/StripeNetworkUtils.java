package com.stripe.android.util;

import android.content.Context;
import android.provider.Settings.Secure;
import android.text.TextUtils;
import com.stripe.android.model.BankAccount;
import com.stripe.android.model.Card;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

public class StripeNetworkUtils {
   private static final String GUID = "guid";
   private static final String MUID = "muid";

   public static void addUidParams(StripeNetworkUtils.UidProvider var0, Context var1, Map var2) {
      String var3;
      if(var0 == null) {
         var3 = Secure.getString(var1.getContentResolver(), "android_id");
      } else {
         var3 = var0.getUid();
      }

      if(!StripeTextUtils.isBlank(var3)) {
         String var4 = StripeTextUtils.shaHashInput(var3);
         String var5;
         if(var0 == null) {
            var5 = var1.getApplicationContext().getPackageName() + var3;
         } else {
            var5 = var0.getPackageName() + var3;
         }

         var5 = StripeTextUtils.shaHashInput(var5);
         if(!StripeTextUtils.isBlank(var4)) {
            var2.put("guid", var4);
         }

         if(!StripeTextUtils.isBlank(var5)) {
            var2.put("muid", var5);
         }
      }

   }

   public static Map hashMapFromBankAccount(Context var0, BankAccount var1) {
      return hashMapFromBankAccount((StripeNetworkUtils.UidProvider)null, var0, var1);
   }

   private static Map hashMapFromBankAccount(StripeNetworkUtils.UidProvider var0, Context var1, BankAccount var2) {
      HashMap var4 = new HashMap();
      HashMap var3 = new HashMap();
      var3.put("country", var2.getCountryCode());
      var3.put("currency", var2.getCurrency());
      var3.put("account_number", var2.getAccountNumber());
      var3.put("routing_number", StripeTextUtils.nullIfBlank(var2.getRoutingNumber()));
      var3.put("account_holder_name", StripeTextUtils.nullIfBlank(var2.getAccountHolderName()));
      var3.put("account_holder_type", StripeTextUtils.nullIfBlank(var2.getAccountHolderType()));
      removeNullAndEmptyParams(var3);
      var4.put("bank_account", var3);
      addUidParams(var0, var1, var4);
      return var4;
   }

   public static Map hashMapFromCard(Context var0, Card var1) {
      return hashMapFromCard((StripeNetworkUtils.UidProvider)null, var0, var1);
   }

   private static Map hashMapFromCard(StripeNetworkUtils.UidProvider var0, Context var1, Card var2) {
      HashMap var3 = new HashMap();
      HashMap var4 = new HashMap();
      var4.put("number", StripeTextUtils.nullIfBlank(var2.getNumber()));
      var4.put("cvc", StripeTextUtils.nullIfBlank(var2.getCVC()));
      var4.put("exp_month", var2.getExpMonth());
      var4.put("exp_year", var2.getExpYear());
      var4.put("name", StripeTextUtils.nullIfBlank(var2.getName()));
      var4.put("currency", StripeTextUtils.nullIfBlank(var2.getCurrency()));
      var4.put("address_line1", StripeTextUtils.nullIfBlank(var2.getAddressLine1()));
      var4.put("address_line2", StripeTextUtils.nullIfBlank(var2.getAddressLine2()));
      var4.put("address_city", StripeTextUtils.nullIfBlank(var2.getAddressCity()));
      var4.put("address_zip", StripeTextUtils.nullIfBlank(var2.getAddressZip()));
      var4.put("address_state", StripeTextUtils.nullIfBlank(var2.getAddressState()));
      var4.put("address_country", StripeTextUtils.nullIfBlank(var2.getAddressCountry()));
      removeNullAndEmptyParams(var4);
      var3.put("product_usage", var2.getLoggingTokens());
      var3.put("card", var4);
      addUidParams(var0, var1, var3);
      return var3;
   }

   public static Map hashMapFromPersonalId(Context var0, String var1) {
      HashMap var2 = new HashMap();
      var2.put("personal_id_number", var1);
      HashMap var3 = new HashMap();
      var3.put("pii", var2);
      return var3;
   }

   public static void removeNullAndEmptyParams(Map var0) {
      Iterator var1 = (new HashSet(var0.keySet())).iterator();

      while(var1.hasNext()) {
         String var2 = (String)var1.next();
         if(var0.get(var2) == null) {
            var0.remove(var2);
         }

         if(var0.get(var2) instanceof CharSequence && TextUtils.isEmpty((CharSequence)var0.get(var2))) {
            var0.remove(var2);
         }

         if(var0.get(var2) instanceof Map) {
            removeNullAndEmptyParams((Map)var0.get(var2));
         }
      }

   }

   public interface UidProvider {
      String getPackageName();

      String getUid();
   }
}
