package com.mp4parser.iso23009.part1;

import com.coremedia.iso.e;
import com.coremedia.iso.g;
import com.coremedia.iso.j;
import com.googlecode.mp4parser.AbstractFullBox;
import java.nio.ByteBuffer;
import org.mp4parser.aspectj.a.b.b;
import org.mp4parser.aspectj.lang.a;

public class EventMessageBox extends AbstractFullBox {
   public static final String TYPE = "emsg";
   private static final a.a ajc$tjp_0;
   private static final a.a ajc$tjp_1;
   private static final a.a ajc$tjp_10;
   private static final a.a ajc$tjp_11;
   private static final a.a ajc$tjp_12;
   private static final a.a ajc$tjp_13;
   private static final a.a ajc$tjp_2;
   private static final a.a ajc$tjp_3;
   private static final a.a ajc$tjp_4;
   private static final a.a ajc$tjp_5;
   private static final a.a ajc$tjp_6;
   private static final a.a ajc$tjp_7;
   private static final a.a ajc$tjp_8;
   private static final a.a ajc$tjp_9;
   long eventDuration;
   long id;
   byte[] messageData;
   long presentationTimeDelta;
   String schemeIdUri;
   long timescale;
   String value;

   static {
      ajc$preClinit();
   }

   public EventMessageBox() {
      super("emsg");
   }

   private static void ajc$preClinit() {
      b var0 = new b("EventMessageBox.java", EventMessageBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getSchemeIdUri", "com.mp4parser.iso23009.part1.EventMessageBox", "", "", "", "java.lang.String"), 59);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "setSchemeIdUri", "com.mp4parser.iso23009.part1.EventMessageBox", "java.lang.String", "schemeIdUri", "", "void"), 63);
      ajc$tjp_10 = var0.a("method-execution", var0.a("1", "getId", "com.mp4parser.iso23009.part1.EventMessageBox", "", "", "", "long"), 99);
      ajc$tjp_11 = var0.a("method-execution", var0.a("1", "setId", "com.mp4parser.iso23009.part1.EventMessageBox", "long", "id", "", "void"), 103);
      ajc$tjp_12 = var0.a("method-execution", var0.a("1", "getMessageData", "com.mp4parser.iso23009.part1.EventMessageBox", "", "", "", "[B"), 107);
      ajc$tjp_13 = var0.a("method-execution", var0.a("1", "setMessageData", "com.mp4parser.iso23009.part1.EventMessageBox", "[B", "messageData", "", "void"), 111);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "getValue", "com.mp4parser.iso23009.part1.EventMessageBox", "", "", "", "java.lang.String"), 67);
      ajc$tjp_3 = var0.a("method-execution", var0.a("1", "setValue", "com.mp4parser.iso23009.part1.EventMessageBox", "java.lang.String", "value", "", "void"), 71);
      ajc$tjp_4 = var0.a("method-execution", var0.a("1", "getTimescale", "com.mp4parser.iso23009.part1.EventMessageBox", "", "", "", "long"), 75);
      ajc$tjp_5 = var0.a("method-execution", var0.a("1", "setTimescale", "com.mp4parser.iso23009.part1.EventMessageBox", "long", "timescale", "", "void"), 79);
      ajc$tjp_6 = var0.a("method-execution", var0.a("1", "getPresentationTimeDelta", "com.mp4parser.iso23009.part1.EventMessageBox", "", "", "", "long"), 83);
      ajc$tjp_7 = var0.a("method-execution", var0.a("1", "setPresentationTimeDelta", "com.mp4parser.iso23009.part1.EventMessageBox", "long", "presentationTimeDelta", "", "void"), 87);
      ajc$tjp_8 = var0.a("method-execution", var0.a("1", "getEventDuration", "com.mp4parser.iso23009.part1.EventMessageBox", "", "", "", "long"), 91);
      ajc$tjp_9 = var0.a("method-execution", var0.a("1", "setEventDuration", "com.mp4parser.iso23009.part1.EventMessageBox", "long", "eventDuration", "", "void"), 95);
   }

   protected void _parseDetails(ByteBuffer var1) {
      this.parseVersionAndFlags(var1);
      this.schemeIdUri = e.e(var1);
      this.value = e.e(var1);
      this.timescale = e.a(var1);
      this.presentationTimeDelta = e.a(var1);
      this.eventDuration = e.a(var1);
      this.id = e.a(var1);
      this.messageData = new byte[var1.remaining()];
      var1.get(this.messageData);
   }

   protected void getContent(ByteBuffer var1) {
      this.writeVersionAndFlags(var1);
      g.c(var1, this.schemeIdUri);
      g.c(var1, this.value);
      g.b(var1, this.timescale);
      g.b(var1, this.presentationTimeDelta);
      g.b(var1, this.eventDuration);
      g.b(var1, this.id);
      var1.put(this.messageData);
   }

   protected long getContentSize() {
      return (long)(j.b(this.schemeIdUri) + 22 + j.b(this.value) + this.messageData.length);
   }

   public long getEventDuration() {
      a var1 = b.a(ajc$tjp_8, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.eventDuration;
   }

   public long getId() {
      a var1 = b.a(ajc$tjp_10, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.id;
   }

   public byte[] getMessageData() {
      a var1 = b.a(ajc$tjp_12, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.messageData;
   }

   public long getPresentationTimeDelta() {
      a var1 = b.a(ajc$tjp_6, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.presentationTimeDelta;
   }

   public String getSchemeIdUri() {
      a var1 = b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.schemeIdUri;
   }

   public long getTimescale() {
      a var1 = b.a(ajc$tjp_4, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.timescale;
   }

   public String getValue() {
      a var1 = b.a(ajc$tjp_2, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.value;
   }

   public void setEventDuration(long var1) {
      a var3 = b.a(ajc$tjp_9, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var3);
      this.eventDuration = var1;
   }

   public void setId(long var1) {
      a var3 = b.a(ajc$tjp_11, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var3);
      this.id = var1;
   }

   public void setMessageData(byte[] var1) {
      a var2 = b.a(ajc$tjp_13, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.messageData = var1;
   }

   public void setPresentationTimeDelta(long var1) {
      a var3 = b.a(ajc$tjp_7, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var3);
      this.presentationTimeDelta = var1;
   }

   public void setSchemeIdUri(String var1) {
      a var2 = b.a(ajc$tjp_1, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.schemeIdUri = var1;
   }

   public void setTimescale(long var1) {
      a var3 = b.a(ajc$tjp_5, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var3);
      this.timescale = var1;
   }

   public void setValue(String var1) {
      a var2 = b.a(ajc$tjp_3, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.value = var1;
   }
}
