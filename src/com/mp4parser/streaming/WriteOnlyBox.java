package com.mp4parser.streaming;

import java.io.IOException;
import java.nio.ByteBuffer;

public abstract class WriteOnlyBox implements com.coremedia.iso.boxes.a {
   private com.coremedia.iso.boxes.b parent;
   private final String type;

   public WriteOnlyBox(String var1) {
      this.type = var1;
   }

   public long getOffset() {
      throw new RuntimeException("It's a´write only box");
   }

   public com.coremedia.iso.boxes.b getParent() {
      return this.parent;
   }

   public String getType() {
      return this.type;
   }

   public void parse(com.googlecode.mp4parser.b var1, ByteBuffer var2, long var3, com.coremedia.iso.b var5) throws IOException {
      throw new RuntimeException("It's a´write only box");
   }

   public void setParent(com.coremedia.iso.boxes.b var1) {
      this.parent = var1;
   }
}
