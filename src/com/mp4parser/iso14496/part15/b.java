package com.mp4parser.iso14496.part15;

import com.coremedia.iso.g;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class b {
   boolean A;
   int a;
   int b;
   boolean c;
   int d;
   long e;
   long f;
   int g;
   int h = 15;
   int i;
   int j = 63;
   int k;
   int l = 63;
   int m;
   int n = 31;
   int o;
   int p = 31;
   int q;
   int r;
   int s;
   int t;
   boolean u;
   int v;
   List w = new ArrayList();
   boolean x;
   boolean y;
   boolean z;

   public int a() {
      Iterator var3 = this.w.iterator();
      int var2 = 23;

      while(var3.hasNext()) {
         b.a var4 = (b.a)var3.next();
         int var1 = var2 + 3;
         Iterator var5 = var4.d.iterator();

         while(true) {
            var2 = var1;
            if(!var5.hasNext()) {
               break;
            }

            var1 = var1 + 2 + ((byte[])var5.next()).length;
         }
      }

      return var2;
   }

   public void a(ByteBuffer var1) {
      this.a = com.coremedia.iso.e.d(var1);
      int var2 = com.coremedia.iso.e.d(var1);
      this.b = (var2 & 192) >> 6;
      boolean var6;
      if((var2 & 32) > 0) {
         var6 = true;
      } else {
         var6 = false;
      }

      this.c = var6;
      this.d = var2 & 31;
      this.e = com.coremedia.iso.e.a(var1);
      this.f = com.coremedia.iso.e.l(var1);
      if((this.f >> 44 & 8L) > 0L) {
         var6 = true;
      } else {
         var6 = false;
      }

      this.x = var6;
      if((this.f >> 44 & 4L) > 0L) {
         var6 = true;
      } else {
         var6 = false;
      }

      this.y = var6;
      if((this.f >> 44 & 2L) > 0L) {
         var6 = true;
      } else {
         var6 = false;
      }

      this.z = var6;
      if((this.f >> 44 & 1L) > 0L) {
         var6 = true;
      } else {
         var6 = false;
      }

      this.A = var6;
      this.f &= 140737488355327L;
      this.g = com.coremedia.iso.e.d(var1);
      var2 = com.coremedia.iso.e.c(var1);
      this.h = ('\uf000' & var2) >> 12;
      this.i = var2 & 4095;
      var2 = com.coremedia.iso.e.d(var1);
      this.j = (var2 & 252) >> 2;
      this.k = var2 & 3;
      var2 = com.coremedia.iso.e.d(var1);
      this.l = (var2 & 252) >> 2;
      this.m = var2 & 3;
      var2 = com.coremedia.iso.e.d(var1);
      this.n = (var2 & 248) >> 3;
      this.o = var2 & 7;
      var2 = com.coremedia.iso.e.d(var1);
      this.p = (var2 & 248) >> 3;
      this.q = var2 & 7;
      this.r = com.coremedia.iso.e.c(var1);
      var2 = com.coremedia.iso.e.d(var1);
      this.s = (var2 & 192) >> 6;
      this.t = (var2 & 56) >> 3;
      if((var2 & 4) > 0) {
         var6 = true;
      } else {
         var6 = false;
      }

      this.u = var6;
      this.v = var2 & 3;
      int var4 = com.coremedia.iso.e.d(var1);
      this.w = new ArrayList();

      for(var2 = 0; var2 < var4; ++var2) {
         b.a var7 = new b.a();
         int var3 = com.coremedia.iso.e.d(var1);
         if((var3 & 128) > 0) {
            var6 = true;
         } else {
            var6 = false;
         }

         var7.a = var6;
         if((var3 & 64) > 0) {
            var6 = true;
         } else {
            var6 = false;
         }

         var7.b = var6;
         var7.c = var3 & 63;
         int var5 = com.coremedia.iso.e.c(var1);
         var7.d = new ArrayList();

         for(var3 = 0; var3 < var5; ++var3) {
            byte[] var8 = new byte[com.coremedia.iso.e.c(var1)];
            var1.get(var8);
            var7.d.add(var8);
         }

         this.w.add(var7);
      }

   }

   public void b(ByteBuffer var1) {
      g.c(var1, this.a);
      int var3 = this.b;
      byte var2;
      if(this.c) {
         var2 = 32;
      } else {
         var2 = 0;
      }

      g.c(var1, var2 + (var3 << 6) + this.d);
      g.b(var1, this.e);
      long var7 = this.f;
      long var5 = var7;
      if(this.x) {
         var5 = var7 | 140737488355328L;
      }

      var7 = var5;
      if(this.y) {
         var7 = var5 | 70368744177664L;
      }

      var5 = var7;
      if(this.z) {
         var5 = var7 | 35184372088832L;
      }

      var7 = var5;
      if(this.A) {
         var7 = var5 | 17592186044416L;
      }

      g.c(var1, var7);
      g.c(var1, this.g);
      g.b(var1, (this.h << 12) + this.i);
      g.c(var1, (this.j << 2) + this.k);
      g.c(var1, (this.l << 2) + this.m);
      g.c(var1, (this.n << 3) + this.o);
      g.c(var1, (this.p << 3) + this.q);
      g.b(var1, this.r);
      int var4 = this.s;
      var3 = this.t;
      if(this.u) {
         var2 = 4;
      } else {
         var2 = 0;
      }

      g.c(var1, var2 + (var3 << 3) + (var4 << 6) + this.v);
      g.c(var1, this.w.size());
      Iterator var9 = this.w.iterator();

      while(var9.hasNext()) {
         b.a var10 = (b.a)var9.next();
         short var13;
         if(var10.a) {
            var13 = 128;
         } else {
            var13 = 0;
         }

         byte var12;
         if(var10.b) {
            var12 = 64;
         } else {
            var12 = 0;
         }

         g.c(var1, var13 + var12 + var10.c);
         g.b(var1, var10.d.size());
         Iterator var14 = var10.d.iterator();

         while(var14.hasNext()) {
            byte[] var11 = (byte[])var14.next();
            g.b(var1, var11.length);
            var1.put(var11);
         }
      }

   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(this != var1) {
         if(var1 != null && this.getClass() == var1.getClass()) {
            b var3 = (b)var1;
            if(this.r != var3.r) {
               var2 = false;
            } else if(this.q != var3.q) {
               var2 = false;
            } else if(this.o != var3.o) {
               var2 = false;
            } else if(this.m != var3.m) {
               var2 = false;
            } else if(this.a != var3.a) {
               var2 = false;
            } else if(this.s != var3.s) {
               var2 = false;
            } else if(this.f != var3.f) {
               var2 = false;
            } else if(this.g != var3.g) {
               var2 = false;
            } else if(this.e != var3.e) {
               var2 = false;
            } else if(this.d != var3.d) {
               var2 = false;
            } else if(this.b != var3.b) {
               var2 = false;
            } else if(this.c != var3.c) {
               var2 = false;
            } else if(this.v != var3.v) {
               var2 = false;
            } else if(this.i != var3.i) {
               var2 = false;
            } else if(this.t != var3.t) {
               var2 = false;
            } else if(this.k != var3.k) {
               var2 = false;
            } else if(this.h != var3.h) {
               var2 = false;
            } else if(this.j != var3.j) {
               var2 = false;
            } else if(this.l != var3.l) {
               var2 = false;
            } else if(this.n != var3.n) {
               var2 = false;
            } else if(this.p != var3.p) {
               var2 = false;
            } else if(this.u != var3.u) {
               var2 = false;
            } else {
               if(this.w != null) {
                  if(this.w.equals(var3.w)) {
                     return var2;
                  }
               } else if(var3.w == null) {
                  return var2;
               }

               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public int hashCode() {
      byte var2 = 1;
      int var3 = 0;
      int var5 = this.a;
      int var4 = this.b;
      byte var1;
      if(this.c) {
         var1 = 1;
      } else {
         var1 = 0;
      }

      int var11 = this.d;
      int var16 = (int)(this.e ^ this.e >>> 32);
      int var13 = (int)(this.f ^ this.f >>> 32);
      int var15 = this.g;
      int var10 = this.h;
      int var19 = this.i;
      int var7 = this.j;
      int var22 = this.k;
      int var12 = this.l;
      int var18 = this.m;
      int var6 = this.n;
      int var21 = this.o;
      int var20 = this.p;
      int var9 = this.q;
      int var8 = this.r;
      int var14 = this.s;
      int var17 = this.t;
      if(!this.u) {
         var2 = 0;
      }

      int var23 = this.v;
      if(this.w != null) {
         var3 = this.w.hashCode();
      }

      return ((((((((((((((((((((var1 + (var5 * 31 + var4) * 31) * 31 + var11) * 31 + var16) * 31 + var13) * 31 + var15) * 31 + var10) * 31 + var19) * 31 + var7) * 31 + var22) * 31 + var12) * 31 + var18) * 31 + var6) * 31 + var21) * 31 + var20) * 31 + var9) * 31 + var8) * 31 + var14) * 31 + var17) * 31 + var2) * 31 + var23) * 31 + var3;
   }

   public String toString() {
      StringBuilder var2 = (new StringBuilder("HEVCDecoderConfigurationRecord{configurationVersion=")).append(this.a).append(", general_profile_space=").append(this.b).append(", general_tier_flag=").append(this.c).append(", general_profile_idc=").append(this.d).append(", general_profile_compatibility_flags=").append(this.e).append(", general_constraint_indicator_flags=").append(this.f).append(", general_level_idc=").append(this.g);
      String var1;
      if(this.h != 15) {
         var1 = ", reserved1=" + this.h;
      } else {
         var1 = "";
      }

      var2 = var2.append(var1).append(", min_spatial_segmentation_idc=").append(this.i);
      if(this.j != 63) {
         var1 = ", reserved2=" + this.j;
      } else {
         var1 = "";
      }

      var2 = var2.append(var1).append(", parallelismType=").append(this.k);
      if(this.l != 63) {
         var1 = ", reserved3=" + this.l;
      } else {
         var1 = "";
      }

      var2 = var2.append(var1).append(", chromaFormat=").append(this.m);
      if(this.n != 31) {
         var1 = ", reserved4=" + this.n;
      } else {
         var1 = "";
      }

      var2 = var2.append(var1).append(", bitDepthLumaMinus8=").append(this.o);
      if(this.p != 31) {
         var1 = ", reserved5=" + this.p;
      } else {
         var1 = "";
      }

      return var2.append(var1).append(", bitDepthChromaMinus8=").append(this.q).append(", avgFrameRate=").append(this.r).append(", constantFrameRate=").append(this.s).append(", numTemporalLayers=").append(this.t).append(", temporalIdNested=").append(this.u).append(", lengthSizeMinusOne=").append(this.v).append(", arrays=").append(this.w).append('}').toString();
   }

   public static class a {
      public boolean a;
      public boolean b;
      public int c;
      public List d;

      public boolean equals(Object var1) {
         boolean var3 = false;
         boolean var2;
         if(this == var1) {
            var2 = true;
         } else {
            var2 = var3;
            if(var1 != null) {
               var2 = var3;
               if(this.getClass() == var1.getClass()) {
                  b.a var4 = (b.a)var1;
                  var2 = var3;
                  if(this.a == var4.a) {
                     var2 = var3;
                     if(this.c == var4.c) {
                        var2 = var3;
                        if(this.b == var4.b) {
                           ListIterator var7 = this.d.listIterator();
                           ListIterator var5 = var4.d.listIterator();

                           while(var7.hasNext() && var5.hasNext()) {
                              byte[] var8 = (byte[])var7.next();
                              byte[] var6 = (byte[])var5.next();
                              if(var8 == null) {
                                 if(var6 != null) {
                                    var2 = var3;
                                    return var2;
                                 }
                              } else if(!Arrays.equals(var8, var6)) {
                                 var2 = var3;
                                 return var2;
                              }
                           }

                           if(!var7.hasNext() && !var5.hasNext()) {
                              var2 = true;
                           } else {
                              var2 = false;
                           }
                        }
                     }
                  }
               }
            }
         }

         return var2;
      }

      public int hashCode() {
         byte var2 = 1;
         int var3 = 0;
         byte var1;
         if(this.a) {
            var1 = 1;
         } else {
            var1 = 0;
         }

         if(!this.b) {
            var2 = 0;
         }

         int var4 = this.c;
         if(this.d != null) {
            var3 = this.d.hashCode();
         }

         return ((var1 * 31 + var2) * 31 + var4) * 31 + var3;
      }

      public String toString() {
         return "Array{nal_unit_type=" + this.c + ", reserved=" + this.b + ", array_completeness=" + this.a + ", num_nals=" + this.d.size() + '}';
      }
   }
}
