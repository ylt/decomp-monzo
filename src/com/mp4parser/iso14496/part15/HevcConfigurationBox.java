package com.mp4parser.iso14496.part15;

import com.googlecode.mp4parser.AbstractBox;
import java.nio.ByteBuffer;
import java.util.List;

public class HevcConfigurationBox extends AbstractBox {
   public static final String TYPE = "hvcC";
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_0;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_1;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_10;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_11;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_12;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_13;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_14;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_15;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_16;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_17;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_18;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_19;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_2;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_20;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_21;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_3;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_4;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_5;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_6;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_7;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_8;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_9;
   private b hevcDecoderConfigurationRecord = new b();

   static {
      ajc$preClinit();
   }

   public HevcConfigurationBox() {
      super("hvcC");
   }

   private static void ajc$preClinit() {
      org.mp4parser.aspectj.a.b.b var0 = new org.mp4parser.aspectj.a.b.b("HevcConfigurationBox.java", HevcConfigurationBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getHevcDecoderConfigurationRecord", "com.mp4parser.iso14496.part15.HevcConfigurationBox", "", "", "", "com.mp4parser.iso14496.part15.HevcDecoderConfigurationRecord"), 38);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "setHevcDecoderConfigurationRecord", "com.mp4parser.iso14496.part15.HevcConfigurationBox", "com.mp4parser.iso14496.part15.HevcDecoderConfigurationRecord", "hevcDecoderConfigurationRecord", "", "void"), 42);
      ajc$tjp_10 = var0.a("method-execution", var0.a("1", "getGeneral_level_idc", "com.mp4parser.iso14496.part15.HevcConfigurationBox", "", "", "", "int"), 90);
      ajc$tjp_11 = var0.a("method-execution", var0.a("1", "getMin_spatial_segmentation_idc", "com.mp4parser.iso14496.part15.HevcConfigurationBox", "", "", "", "int"), 94);
      ajc$tjp_12 = var0.a("method-execution", var0.a("1", "getParallelismType", "com.mp4parser.iso14496.part15.HevcConfigurationBox", "", "", "", "int"), 98);
      ajc$tjp_13 = var0.a("method-execution", var0.a("1", "getChromaFormat", "com.mp4parser.iso14496.part15.HevcConfigurationBox", "", "", "", "int"), 102);
      ajc$tjp_14 = var0.a("method-execution", var0.a("1", "getBitDepthLumaMinus8", "com.mp4parser.iso14496.part15.HevcConfigurationBox", "", "", "", "int"), 106);
      ajc$tjp_15 = var0.a("method-execution", var0.a("1", "getBitDepthChromaMinus8", "com.mp4parser.iso14496.part15.HevcConfigurationBox", "", "", "", "int"), 110);
      ajc$tjp_16 = var0.a("method-execution", var0.a("1", "getAvgFrameRate", "com.mp4parser.iso14496.part15.HevcConfigurationBox", "", "", "", "int"), 114);
      ajc$tjp_17 = var0.a("method-execution", var0.a("1", "getNumTemporalLayers", "com.mp4parser.iso14496.part15.HevcConfigurationBox", "", "", "", "int"), 118);
      ajc$tjp_18 = var0.a("method-execution", var0.a("1", "getLengthSizeMinusOne", "com.mp4parser.iso14496.part15.HevcConfigurationBox", "", "", "", "int"), 122);
      ajc$tjp_19 = var0.a("method-execution", var0.a("1", "isTemporalIdNested", "com.mp4parser.iso14496.part15.HevcConfigurationBox", "", "", "", "boolean"), 126);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "equals", "com.mp4parser.iso14496.part15.HevcConfigurationBox", "java.lang.Object", "o", "", "boolean"), 47);
      ajc$tjp_20 = var0.a("method-execution", var0.a("1", "getConstantFrameRate", "com.mp4parser.iso14496.part15.HevcConfigurationBox", "", "", "", "int"), 130);
      ajc$tjp_21 = var0.a("method-execution", var0.a("1", "getArrays", "com.mp4parser.iso14496.part15.HevcConfigurationBox", "", "", "", "java.util.List"), 134);
      ajc$tjp_3 = var0.a("method-execution", var0.a("1", "hashCode", "com.mp4parser.iso14496.part15.HevcConfigurationBox", "", "", "", "int"), 60);
      ajc$tjp_4 = var0.a("method-execution", var0.a("1", "getConfigurationVersion", "com.mp4parser.iso14496.part15.HevcConfigurationBox", "", "", "", "int"), 65);
      ajc$tjp_5 = var0.a("method-execution", var0.a("1", "getGeneral_profile_space", "com.mp4parser.iso14496.part15.HevcConfigurationBox", "", "", "", "int"), 69);
      ajc$tjp_6 = var0.a("method-execution", var0.a("1", "isGeneral_tier_flag", "com.mp4parser.iso14496.part15.HevcConfigurationBox", "", "", "", "boolean"), 73);
      ajc$tjp_7 = var0.a("method-execution", var0.a("1", "getGeneral_profile_idc", "com.mp4parser.iso14496.part15.HevcConfigurationBox", "", "", "", "int"), 78);
      ajc$tjp_8 = var0.a("method-execution", var0.a("1", "getGeneral_profile_compatibility_flags", "com.mp4parser.iso14496.part15.HevcConfigurationBox", "", "", "", "long"), 82);
      ajc$tjp_9 = var0.a("method-execution", var0.a("1", "getGeneral_constraint_indicator_flags", "com.mp4parser.iso14496.part15.HevcConfigurationBox", "", "", "", "long"), 86);
   }

   protected void _parseDetails(ByteBuffer var1) {
      this.hevcDecoderConfigurationRecord.a(var1);
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      org.mp4parser.aspectj.lang.a var3 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_2, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var3);
      if(this != var1) {
         if(var1 != null && this.getClass() == var1.getClass()) {
            HevcConfigurationBox var4 = (HevcConfigurationBox)var1;
            if(this.hevcDecoderConfigurationRecord != null) {
               if(this.hevcDecoderConfigurationRecord.equals(var4.hevcDecoderConfigurationRecord)) {
                  return var2;
               }
            } else if(var4.hevcDecoderConfigurationRecord == null) {
               return var2;
            }

            var2 = false;
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public List getArrays() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_21, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.hevcDecoderConfigurationRecord.w;
   }

   public int getAvgFrameRate() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_16, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.hevcDecoderConfigurationRecord.r;
   }

   public int getBitDepthChromaMinus8() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_15, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.hevcDecoderConfigurationRecord.q;
   }

   public int getBitDepthLumaMinus8() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_14, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.hevcDecoderConfigurationRecord.o;
   }

   public int getChromaFormat() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_13, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.hevcDecoderConfigurationRecord.m;
   }

   public int getConfigurationVersion() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_4, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.hevcDecoderConfigurationRecord.a;
   }

   public int getConstantFrameRate() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_20, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.hevcDecoderConfigurationRecord.s;
   }

   protected void getContent(ByteBuffer var1) {
      this.hevcDecoderConfigurationRecord.b(var1);
   }

   protected long getContentSize() {
      return (long)this.hevcDecoderConfigurationRecord.a();
   }

   public long getGeneral_constraint_indicator_flags() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_9, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.hevcDecoderConfigurationRecord.f;
   }

   public int getGeneral_level_idc() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_10, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.hevcDecoderConfigurationRecord.g;
   }

   public long getGeneral_profile_compatibility_flags() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_8, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.hevcDecoderConfigurationRecord.e;
   }

   public int getGeneral_profile_idc() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_7, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.hevcDecoderConfigurationRecord.d;
   }

   public int getGeneral_profile_space() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_5, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.hevcDecoderConfigurationRecord.b;
   }

   public b getHevcDecoderConfigurationRecord() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.hevcDecoderConfigurationRecord;
   }

   public int getLengthSizeMinusOne() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_18, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.hevcDecoderConfigurationRecord.v;
   }

   public int getMin_spatial_segmentation_idc() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_11, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.hevcDecoderConfigurationRecord.i;
   }

   public int getNumTemporalLayers() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_17, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.hevcDecoderConfigurationRecord.t;
   }

   public int getParallelismType() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_12, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.hevcDecoderConfigurationRecord.k;
   }

   public int hashCode() {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_3, this, this);
      com.googlecode.mp4parser.e.a().a(var2);
      int var1;
      if(this.hevcDecoderConfigurationRecord != null) {
         var1 = this.hevcDecoderConfigurationRecord.hashCode();
      } else {
         var1 = 0;
      }

      return var1;
   }

   public boolean isGeneral_tier_flag() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_6, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.hevcDecoderConfigurationRecord.c;
   }

   public boolean isTemporalIdNested() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_19, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.hevcDecoderConfigurationRecord.u;
   }

   public void setHevcDecoderConfigurationRecord(b var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_1, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.hevcDecoderConfigurationRecord = var1;
   }
}
