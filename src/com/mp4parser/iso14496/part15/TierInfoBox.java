package com.mp4parser.iso14496.part15;

import com.coremedia.iso.g;
import com.googlecode.mp4parser.AbstractBox;
import java.nio.ByteBuffer;

public class TierInfoBox extends AbstractBox {
   public static final String TYPE = "tiri";
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_0;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_1;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_10;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_11;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_12;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_13;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_14;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_15;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_16;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_17;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_18;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_19;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_2;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_20;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_21;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_3;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_4;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_5;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_6;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_7;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_8;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_9;
   int constantFrameRate;
   int discardable;
   int frameRate;
   int levelIndication;
   int profileIndication;
   int profile_compatibility;
   int reserved1 = 0;
   int reserved2 = 0;
   int tierID;
   int visualHeight;
   int visualWidth;

   static {
      ajc$preClinit();
   }

   public TierInfoBox() {
      super("tiri");
   }

   private static void ajc$preClinit() {
      org.mp4parser.aspectj.a.b.b var0 = new org.mp4parser.aspectj.a.b.b("TierInfoBox.java", TierInfoBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getTierID", "com.mp4parser.iso14496.part15.TierInfoBox", "", "", "", "int"), 69);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "setTierID", "com.mp4parser.iso14496.part15.TierInfoBox", "int", "tierID", "", "void"), 73);
      ajc$tjp_10 = var0.a("method-execution", var0.a("1", "getVisualWidth", "com.mp4parser.iso14496.part15.TierInfoBox", "", "", "", "int"), 109);
      ajc$tjp_11 = var0.a("method-execution", var0.a("1", "setVisualWidth", "com.mp4parser.iso14496.part15.TierInfoBox", "int", "visualWidth", "", "void"), 113);
      ajc$tjp_12 = var0.a("method-execution", var0.a("1", "getVisualHeight", "com.mp4parser.iso14496.part15.TierInfoBox", "", "", "", "int"), 117);
      ajc$tjp_13 = var0.a("method-execution", var0.a("1", "setVisualHeight", "com.mp4parser.iso14496.part15.TierInfoBox", "int", "visualHeight", "", "void"), 121);
      ajc$tjp_14 = var0.a("method-execution", var0.a("1", "getDiscardable", "com.mp4parser.iso14496.part15.TierInfoBox", "", "", "", "int"), 125);
      ajc$tjp_15 = var0.a("method-execution", var0.a("1", "setDiscardable", "com.mp4parser.iso14496.part15.TierInfoBox", "int", "discardable", "", "void"), 129);
      ajc$tjp_16 = var0.a("method-execution", var0.a("1", "getConstantFrameRate", "com.mp4parser.iso14496.part15.TierInfoBox", "", "", "", "int"), 133);
      ajc$tjp_17 = var0.a("method-execution", var0.a("1", "setConstantFrameRate", "com.mp4parser.iso14496.part15.TierInfoBox", "int", "constantFrameRate", "", "void"), 137);
      ajc$tjp_18 = var0.a("method-execution", var0.a("1", "getReserved2", "com.mp4parser.iso14496.part15.TierInfoBox", "", "", "", "int"), 141);
      ajc$tjp_19 = var0.a("method-execution", var0.a("1", "setReserved2", "com.mp4parser.iso14496.part15.TierInfoBox", "int", "reserved2", "", "void"), 145);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "getProfileIndication", "com.mp4parser.iso14496.part15.TierInfoBox", "", "", "", "int"), 77);
      ajc$tjp_20 = var0.a("method-execution", var0.a("1", "getFrameRate", "com.mp4parser.iso14496.part15.TierInfoBox", "", "", "", "int"), 149);
      ajc$tjp_21 = var0.a("method-execution", var0.a("1", "setFrameRate", "com.mp4parser.iso14496.part15.TierInfoBox", "int", "frameRate", "", "void"), 153);
      ajc$tjp_3 = var0.a("method-execution", var0.a("1", "setProfileIndication", "com.mp4parser.iso14496.part15.TierInfoBox", "int", "profileIndication", "", "void"), 81);
      ajc$tjp_4 = var0.a("method-execution", var0.a("1", "getProfile_compatibility", "com.mp4parser.iso14496.part15.TierInfoBox", "", "", "", "int"), 85);
      ajc$tjp_5 = var0.a("method-execution", var0.a("1", "setProfile_compatibility", "com.mp4parser.iso14496.part15.TierInfoBox", "int", "profile_compatibility", "", "void"), 89);
      ajc$tjp_6 = var0.a("method-execution", var0.a("1", "getLevelIndication", "com.mp4parser.iso14496.part15.TierInfoBox", "", "", "", "int"), 93);
      ajc$tjp_7 = var0.a("method-execution", var0.a("1", "setLevelIndication", "com.mp4parser.iso14496.part15.TierInfoBox", "int", "levelIndication", "", "void"), 97);
      ajc$tjp_8 = var0.a("method-execution", var0.a("1", "getReserved1", "com.mp4parser.iso14496.part15.TierInfoBox", "", "", "", "int"), 101);
      ajc$tjp_9 = var0.a("method-execution", var0.a("1", "setReserved1", "com.mp4parser.iso14496.part15.TierInfoBox", "int", "reserved1", "", "void"), 105);
   }

   protected void _parseDetails(ByteBuffer var1) {
      this.tierID = com.coremedia.iso.e.c(var1);
      this.profileIndication = com.coremedia.iso.e.d(var1);
      this.profile_compatibility = com.coremedia.iso.e.d(var1);
      this.levelIndication = com.coremedia.iso.e.d(var1);
      this.reserved1 = com.coremedia.iso.e.d(var1);
      this.visualWidth = com.coremedia.iso.e.c(var1);
      this.visualHeight = com.coremedia.iso.e.c(var1);
      int var2 = com.coremedia.iso.e.d(var1);
      this.discardable = (var2 & 192) >> 6;
      this.constantFrameRate = (var2 & 48) >> 4;
      this.reserved2 = var2 & 15;
      this.frameRate = com.coremedia.iso.e.c(var1);
   }

   public int getConstantFrameRate() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_16, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.constantFrameRate;
   }

   protected void getContent(ByteBuffer var1) {
      g.b(var1, this.tierID);
      g.c(var1, this.profileIndication);
      g.c(var1, this.profile_compatibility);
      g.c(var1, this.levelIndication);
      g.c(var1, this.reserved1);
      g.b(var1, this.visualWidth);
      g.b(var1, this.visualHeight);
      g.c(var1, (this.discardable << 6) + (this.constantFrameRate << 4) + this.reserved2);
      g.b(var1, this.frameRate);
   }

   protected long getContentSize() {
      return 13L;
   }

   public int getDiscardable() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_14, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.discardable;
   }

   public int getFrameRate() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_20, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.frameRate;
   }

   public int getLevelIndication() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_6, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.levelIndication;
   }

   public int getProfileIndication() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_2, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.profileIndication;
   }

   public int getProfile_compatibility() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_4, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.profile_compatibility;
   }

   public int getReserved1() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_8, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.reserved1;
   }

   public int getReserved2() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_18, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.reserved2;
   }

   public int getTierID() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.tierID;
   }

   public int getVisualHeight() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_12, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.visualHeight;
   }

   public int getVisualWidth() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_10, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.visualWidth;
   }

   public void setConstantFrameRate(int var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_17, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.constantFrameRate = var1;
   }

   public void setDiscardable(int var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_15, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.discardable = var1;
   }

   public void setFrameRate(int var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_21, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.frameRate = var1;
   }

   public void setLevelIndication(int var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_7, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.levelIndication = var1;
   }

   public void setProfileIndication(int var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_3, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.profileIndication = var1;
   }

   public void setProfile_compatibility(int var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_5, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.profile_compatibility = var1;
   }

   public void setReserved1(int var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_9, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.reserved1 = var1;
   }

   public void setReserved2(int var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_19, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.reserved2 = var1;
   }

   public void setTierID(int var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_1, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.tierID = var1;
   }

   public void setVisualHeight(int var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_13, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.visualHeight = var1;
   }

   public void setVisualWidth(int var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_11, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.visualWidth = var1;
   }
}
