package com.mp4parser.iso14496.part15;

import com.googlecode.mp4parser.AbstractBox;
import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.List;

public final class AvcConfigurationBox extends AbstractBox {
   public static final String TYPE = "avcC";
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_0;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_1;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_10;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_11;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_12;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_13;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_14;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_15;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_16;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_17;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_18;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_19;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_2;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_20;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_21;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_22;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_23;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_24;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_25;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_26;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_27;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_28;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_29;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_3;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_4;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_5;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_6;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_7;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_8;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_9;
   public a avcDecoderConfigurationRecord = new a();

   static {
      ajc$preClinit();
   }

   public AvcConfigurationBox() {
      super("avcC");
   }

   private static void ajc$preClinit() {
      org.mp4parser.aspectj.a.b.b var0 = new org.mp4parser.aspectj.a.b.b("AvcConfigurationBox.java", AvcConfigurationBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getConfigurationVersion", "com.mp4parser.iso14496.part15.AvcConfigurationBox", "", "", "", "int"), 44);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "getAvcProfileIndication", "com.mp4parser.iso14496.part15.AvcConfigurationBox", "", "", "", "int"), 48);
      ajc$tjp_10 = var0.a("method-execution", var0.a("1", "setAvcLevelIndication", "com.mp4parser.iso14496.part15.AvcConfigurationBox", "int", "avcLevelIndication", "", "void"), 84);
      ajc$tjp_11 = var0.a("method-execution", var0.a("1", "setLengthSizeMinusOne", "com.mp4parser.iso14496.part15.AvcConfigurationBox", "int", "lengthSizeMinusOne", "", "void"), 88);
      ajc$tjp_12 = var0.a("method-execution", var0.a("1", "setSequenceParameterSets", "com.mp4parser.iso14496.part15.AvcConfigurationBox", "java.util.List", "sequenceParameterSets", "", "void"), 92);
      ajc$tjp_13 = var0.a("method-execution", var0.a("1", "setPictureParameterSets", "com.mp4parser.iso14496.part15.AvcConfigurationBox", "java.util.List", "pictureParameterSets", "", "void"), 96);
      ajc$tjp_14 = var0.a("method-execution", var0.a("1", "getChromaFormat", "com.mp4parser.iso14496.part15.AvcConfigurationBox", "", "", "", "int"), 100);
      ajc$tjp_15 = var0.a("method-execution", var0.a("1", "setChromaFormat", "com.mp4parser.iso14496.part15.AvcConfigurationBox", "int", "chromaFormat", "", "void"), 104);
      ajc$tjp_16 = var0.a("method-execution", var0.a("1", "getBitDepthLumaMinus8", "com.mp4parser.iso14496.part15.AvcConfigurationBox", "", "", "", "int"), 108);
      ajc$tjp_17 = var0.a("method-execution", var0.a("1", "setBitDepthLumaMinus8", "com.mp4parser.iso14496.part15.AvcConfigurationBox", "int", "bitDepthLumaMinus8", "", "void"), 112);
      ajc$tjp_18 = var0.a("method-execution", var0.a("1", "getBitDepthChromaMinus8", "com.mp4parser.iso14496.part15.AvcConfigurationBox", "", "", "", "int"), 116);
      ajc$tjp_19 = var0.a("method-execution", var0.a("1", "setBitDepthChromaMinus8", "com.mp4parser.iso14496.part15.AvcConfigurationBox", "int", "bitDepthChromaMinus8", "", "void"), 120);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "getProfileCompatibility", "com.mp4parser.iso14496.part15.AvcConfigurationBox", "", "", "", "int"), 52);
      ajc$tjp_20 = var0.a("method-execution", var0.a("1", "getSequenceParameterSetExts", "com.mp4parser.iso14496.part15.AvcConfigurationBox", "", "", "", "java.util.List"), 124);
      ajc$tjp_21 = var0.a("method-execution", var0.a("1", "setSequenceParameterSetExts", "com.mp4parser.iso14496.part15.AvcConfigurationBox", "java.util.List", "sequenceParameterSetExts", "", "void"), 128);
      ajc$tjp_22 = var0.a("method-execution", var0.a("1", "hasExts", "com.mp4parser.iso14496.part15.AvcConfigurationBox", "", "", "", "boolean"), 132);
      ajc$tjp_23 = var0.a("method-execution", var0.a("1", "setHasExts", "com.mp4parser.iso14496.part15.AvcConfigurationBox", "boolean", "hasExts", "", "void"), 136);
      ajc$tjp_24 = var0.a("method-execution", var0.a("1", "getContentSize", "com.mp4parser.iso14496.part15.AvcConfigurationBox", "", "", "", "long"), 147);
      ajc$tjp_25 = var0.a("method-execution", var0.a("1", "getContent", "com.mp4parser.iso14496.part15.AvcConfigurationBox", "java.nio.ByteBuffer", "byteBuffer", "", "void"), 153);
      ajc$tjp_26 = var0.a("method-execution", var0.a("1", "getSPS", "com.mp4parser.iso14496.part15.AvcConfigurationBox", "", "", "", "[Ljava.lang.String;"), 158);
      ajc$tjp_27 = var0.a("method-execution", var0.a("1", "getPPS", "com.mp4parser.iso14496.part15.AvcConfigurationBox", "", "", "", "[Ljava.lang.String;"), 162);
      ajc$tjp_28 = var0.a("method-execution", var0.a("1", "getavcDecoderConfigurationRecord", "com.mp4parser.iso14496.part15.AvcConfigurationBox", "", "", "", "com.mp4parser.iso14496.part15.AvcDecoderConfigurationRecord"), 167);
      ajc$tjp_29 = var0.a("method-execution", var0.a("1", "toString", "com.mp4parser.iso14496.part15.AvcConfigurationBox", "", "", "", "java.lang.String"), 172);
      ajc$tjp_3 = var0.a("method-execution", var0.a("1", "getAvcLevelIndication", "com.mp4parser.iso14496.part15.AvcConfigurationBox", "", "", "", "int"), 56);
      ajc$tjp_4 = var0.a("method-execution", var0.a("1", "getLengthSizeMinusOne", "com.mp4parser.iso14496.part15.AvcConfigurationBox", "", "", "", "int"), 60);
      ajc$tjp_5 = var0.a("method-execution", var0.a("1", "getSequenceParameterSets", "com.mp4parser.iso14496.part15.AvcConfigurationBox", "", "", "", "java.util.List"), 64);
      ajc$tjp_6 = var0.a("method-execution", var0.a("1", "getPictureParameterSets", "com.mp4parser.iso14496.part15.AvcConfigurationBox", "", "", "", "java.util.List"), 68);
      ajc$tjp_7 = var0.a("method-execution", var0.a("1", "setConfigurationVersion", "com.mp4parser.iso14496.part15.AvcConfigurationBox", "int", "configurationVersion", "", "void"), 72);
      ajc$tjp_8 = var0.a("method-execution", var0.a("1", "setAvcProfileIndication", "com.mp4parser.iso14496.part15.AvcConfigurationBox", "int", "avcProfileIndication", "", "void"), 76);
      ajc$tjp_9 = var0.a("method-execution", var0.a("1", "setProfileCompatibility", "com.mp4parser.iso14496.part15.AvcConfigurationBox", "int", "profileCompatibility", "", "void"), 80);
   }

   public void _parseDetails(ByteBuffer var1) {
      this.avcDecoderConfigurationRecord = new a(var1);
   }

   public int getAvcLevelIndication() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_3, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.avcDecoderConfigurationRecord.d;
   }

   public int getAvcProfileIndication() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_1, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.avcDecoderConfigurationRecord.b;
   }

   public int getBitDepthChromaMinus8() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_18, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.avcDecoderConfigurationRecord.k;
   }

   public int getBitDepthLumaMinus8() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_16, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.avcDecoderConfigurationRecord.j;
   }

   public int getChromaFormat() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_14, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.avcDecoderConfigurationRecord.i;
   }

   public int getConfigurationVersion() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.avcDecoderConfigurationRecord.a;
   }

   public void getContent(ByteBuffer var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_25, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.avcDecoderConfigurationRecord.a(var1);
   }

   public long getContentSize() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_24, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.avcDecoderConfigurationRecord.a();
   }

   public int getLengthSizeMinusOne() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_4, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.avcDecoderConfigurationRecord.e;
   }

   public String[] getPPS() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_27, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.avcDecoderConfigurationRecord.b();
   }

   public List getPictureParameterSets() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_6, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return Collections.unmodifiableList(this.avcDecoderConfigurationRecord.g);
   }

   public int getProfileCompatibility() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_2, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.avcDecoderConfigurationRecord.c;
   }

   public String[] getSPS() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_26, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.avcDecoderConfigurationRecord.c();
   }

   public List getSequenceParameterSetExts() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_20, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.avcDecoderConfigurationRecord.l;
   }

   public List getSequenceParameterSets() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_5, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return Collections.unmodifiableList(this.avcDecoderConfigurationRecord.f);
   }

   public a getavcDecoderConfigurationRecord() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_28, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.avcDecoderConfigurationRecord;
   }

   public boolean hasExts() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_22, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.avcDecoderConfigurationRecord.h;
   }

   public void setAvcLevelIndication(int var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_10, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.avcDecoderConfigurationRecord.d = var1;
   }

   public void setAvcProfileIndication(int var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_8, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.avcDecoderConfigurationRecord.b = var1;
   }

   public void setBitDepthChromaMinus8(int var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_19, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.avcDecoderConfigurationRecord.k = var1;
   }

   public void setBitDepthLumaMinus8(int var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_17, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.avcDecoderConfigurationRecord.j = var1;
   }

   public void setChromaFormat(int var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_15, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.avcDecoderConfigurationRecord.i = var1;
   }

   public void setConfigurationVersion(int var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_7, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.avcDecoderConfigurationRecord.a = var1;
   }

   public void setHasExts(boolean var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_23, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.avcDecoderConfigurationRecord.h = var1;
   }

   public void setLengthSizeMinusOne(int var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_11, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.avcDecoderConfigurationRecord.e = var1;
   }

   public void setPictureParameterSets(List var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_13, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.avcDecoderConfigurationRecord.g = var1;
   }

   public void setProfileCompatibility(int var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_9, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.avcDecoderConfigurationRecord.c = var1;
   }

   public void setSequenceParameterSetExts(List var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_21, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.avcDecoderConfigurationRecord.l = var1;
   }

   public void setSequenceParameterSets(List var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_12, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.avcDecoderConfigurationRecord.f = var1;
   }

   public String toString() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_29, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return "AvcConfigurationBox{avcDecoderConfigurationRecord=" + this.avcDecoderConfigurationRecord + '}';
   }
}
