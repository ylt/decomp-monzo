package com.mp4parser.iso14496.part15;

import com.coremedia.iso.g;
import com.googlecode.mp4parser.AbstractBox;
import java.nio.ByteBuffer;

public class TierBitRateBox extends AbstractBox {
   public static final String TYPE = "tibr";
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_0;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_1;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_10;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_11;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_2;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_3;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_4;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_5;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_6;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_7;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_8;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_9;
   long avgBitRate;
   long baseBitRate;
   long maxBitRate;
   long tierAvgBitRate;
   long tierBaseBitRate;
   long tierMaxBitRate;

   static {
      ajc$preClinit();
   }

   public TierBitRateBox() {
      super("tibr");
   }

   private static void ajc$preClinit() {
      org.mp4parser.aspectj.a.b.b var0 = new org.mp4parser.aspectj.a.b.b("TierBitRateBox.java", TierBitRateBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getBaseBitRate", "com.mp4parser.iso14496.part15.TierBitRateBox", "", "", "", "long"), 52);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "setBaseBitRate", "com.mp4parser.iso14496.part15.TierBitRateBox", "long", "baseBitRate", "", "void"), 56);
      ajc$tjp_10 = var0.a("method-execution", var0.a("1", "getTierAvgBitRate", "com.mp4parser.iso14496.part15.TierBitRateBox", "", "", "", "long"), 92);
      ajc$tjp_11 = var0.a("method-execution", var0.a("1", "setTierAvgBitRate", "com.mp4parser.iso14496.part15.TierBitRateBox", "long", "tierAvgBitRate", "", "void"), 96);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "getMaxBitRate", "com.mp4parser.iso14496.part15.TierBitRateBox", "", "", "", "long"), 60);
      ajc$tjp_3 = var0.a("method-execution", var0.a("1", "setMaxBitRate", "com.mp4parser.iso14496.part15.TierBitRateBox", "long", "maxBitRate", "", "void"), 64);
      ajc$tjp_4 = var0.a("method-execution", var0.a("1", "getAvgBitRate", "com.mp4parser.iso14496.part15.TierBitRateBox", "", "", "", "long"), 68);
      ajc$tjp_5 = var0.a("method-execution", var0.a("1", "setAvgBitRate", "com.mp4parser.iso14496.part15.TierBitRateBox", "long", "avgBitRate", "", "void"), 72);
      ajc$tjp_6 = var0.a("method-execution", var0.a("1", "getTierBaseBitRate", "com.mp4parser.iso14496.part15.TierBitRateBox", "", "", "", "long"), 76);
      ajc$tjp_7 = var0.a("method-execution", var0.a("1", "setTierBaseBitRate", "com.mp4parser.iso14496.part15.TierBitRateBox", "long", "tierBaseBitRate", "", "void"), 80);
      ajc$tjp_8 = var0.a("method-execution", var0.a("1", "getTierMaxBitRate", "com.mp4parser.iso14496.part15.TierBitRateBox", "", "", "", "long"), 84);
      ajc$tjp_9 = var0.a("method-execution", var0.a("1", "setTierMaxBitRate", "com.mp4parser.iso14496.part15.TierBitRateBox", "long", "tierMaxBitRate", "", "void"), 88);
   }

   protected void _parseDetails(ByteBuffer var1) {
      this.baseBitRate = com.coremedia.iso.e.a(var1);
      this.maxBitRate = com.coremedia.iso.e.a(var1);
      this.avgBitRate = com.coremedia.iso.e.a(var1);
      this.tierBaseBitRate = com.coremedia.iso.e.a(var1);
      this.tierMaxBitRate = com.coremedia.iso.e.a(var1);
      this.tierAvgBitRate = com.coremedia.iso.e.a(var1);
   }

   public long getAvgBitRate() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_4, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.avgBitRate;
   }

   public long getBaseBitRate() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.baseBitRate;
   }

   protected void getContent(ByteBuffer var1) {
      g.b(var1, this.baseBitRate);
      g.b(var1, this.maxBitRate);
      g.b(var1, this.avgBitRate);
      g.b(var1, this.tierBaseBitRate);
      g.b(var1, this.tierMaxBitRate);
      g.b(var1, this.tierAvgBitRate);
   }

   protected long getContentSize() {
      return 24L;
   }

   public long getMaxBitRate() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_2, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.maxBitRate;
   }

   public long getTierAvgBitRate() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_10, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.tierAvgBitRate;
   }

   public long getTierBaseBitRate() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_6, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.tierBaseBitRate;
   }

   public long getTierMaxBitRate() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_8, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.tierMaxBitRate;
   }

   public void setAvgBitRate(long var1) {
      org.mp4parser.aspectj.lang.a var3 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_5, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var3);
      this.avgBitRate = var1;
   }

   public void setBaseBitRate(long var1) {
      org.mp4parser.aspectj.lang.a var3 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_1, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var3);
      this.baseBitRate = var1;
   }

   public void setMaxBitRate(long var1) {
      org.mp4parser.aspectj.lang.a var3 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_3, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var3);
      this.maxBitRate = var1;
   }

   public void setTierAvgBitRate(long var1) {
      org.mp4parser.aspectj.lang.a var3 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_11, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var3);
      this.tierAvgBitRate = var1;
   }

   public void setTierBaseBitRate(long var1) {
      org.mp4parser.aspectj.lang.a var3 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_7, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var3);
      this.tierBaseBitRate = var1;
   }

   public void setTierMaxBitRate(long var1) {
      org.mp4parser.aspectj.lang.a var3 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_9, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var3);
      this.tierMaxBitRate = var1;
   }
}
