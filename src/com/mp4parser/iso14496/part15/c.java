package com.mp4parser.iso14496.part15;

import java.nio.ByteBuffer;

public class c extends com.googlecode.mp4parser.boxes.mp4.samplegrouping.b {
   public String a() {
      return "stsa";
   }

   public void a(ByteBuffer var1) {
   }

   public ByteBuffer b() {
      return ByteBuffer.allocate(0);
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(this != var1 && (var1 == null || this.getClass() != var1.getClass())) {
         var2 = false;
      }

      return var2;
   }

   public int hashCode() {
      return 37;
   }
}
