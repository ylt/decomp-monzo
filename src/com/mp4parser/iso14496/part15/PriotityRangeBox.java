package com.mp4parser.iso14496.part15;

import com.coremedia.iso.g;
import com.googlecode.mp4parser.AbstractBox;
import java.nio.ByteBuffer;

public class PriotityRangeBox extends AbstractBox {
   public static final String TYPE = "svpr";
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_0;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_1;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_2;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_3;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_4;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_5;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_6;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_7;
   int max_priorityId;
   int min_priorityId;
   int reserved1 = 0;
   int reserved2 = 0;

   static {
      ajc$preClinit();
   }

   public PriotityRangeBox() {
      super("svpr");
   }

   private static void ajc$preClinit() {
      org.mp4parser.aspectj.a.b.b var0 = new org.mp4parser.aspectj.a.b.b("PriotityRangeBox.java", PriotityRangeBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getReserved1", "com.mp4parser.iso14496.part15.PriotityRangeBox", "", "", "", "int"), 45);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "setReserved1", "com.mp4parser.iso14496.part15.PriotityRangeBox", "int", "reserved1", "", "void"), 49);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "getMin_priorityId", "com.mp4parser.iso14496.part15.PriotityRangeBox", "", "", "", "int"), 53);
      ajc$tjp_3 = var0.a("method-execution", var0.a("1", "setMin_priorityId", "com.mp4parser.iso14496.part15.PriotityRangeBox", "int", "min_priorityId", "", "void"), 57);
      ajc$tjp_4 = var0.a("method-execution", var0.a("1", "getReserved2", "com.mp4parser.iso14496.part15.PriotityRangeBox", "", "", "", "int"), 61);
      ajc$tjp_5 = var0.a("method-execution", var0.a("1", "setReserved2", "com.mp4parser.iso14496.part15.PriotityRangeBox", "int", "reserved2", "", "void"), 65);
      ajc$tjp_6 = var0.a("method-execution", var0.a("1", "getMax_priorityId", "com.mp4parser.iso14496.part15.PriotityRangeBox", "", "", "", "int"), 69);
      ajc$tjp_7 = var0.a("method-execution", var0.a("1", "setMax_priorityId", "com.mp4parser.iso14496.part15.PriotityRangeBox", "int", "max_priorityId", "", "void"), 73);
   }

   protected void _parseDetails(ByteBuffer var1) {
      this.min_priorityId = com.coremedia.iso.e.d(var1);
      this.reserved1 = (this.min_priorityId & 192) >> 6;
      this.min_priorityId &= 63;
      this.max_priorityId = com.coremedia.iso.e.d(var1);
      this.reserved2 = (this.max_priorityId & 192) >> 6;
      this.max_priorityId &= 63;
   }

   protected void getContent(ByteBuffer var1) {
      g.c(var1, (this.reserved1 << 6) + this.min_priorityId);
      g.c(var1, (this.reserved2 << 6) + this.max_priorityId);
   }

   protected long getContentSize() {
      return 2L;
   }

   public int getMax_priorityId() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_6, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.max_priorityId;
   }

   public int getMin_priorityId() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_2, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.min_priorityId;
   }

   public int getReserved1() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.reserved1;
   }

   public int getReserved2() {
      org.mp4parser.aspectj.lang.a var1 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_4, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.reserved2;
   }

   public void setMax_priorityId(int var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_7, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.max_priorityId = var1;
   }

   public void setMin_priorityId(int var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_3, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.min_priorityId = var1;
   }

   public void setReserved1(int var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_1, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.reserved1 = var1;
   }

   public void setReserved2(int var1) {
      org.mp4parser.aspectj.lang.a var2 = org.mp4parser.aspectj.a.b.b.a(ajc$tjp_5, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.reserved2 = var1;
   }
}
