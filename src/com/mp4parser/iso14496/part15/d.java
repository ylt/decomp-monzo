package com.mp4parser.iso14496.part15;

import com.coremedia.iso.g;
import java.nio.ByteBuffer;

public class d extends com.googlecode.mp4parser.boxes.mp4.samplegrouping.b {
   int a;
   int b;

   public String a() {
      return "sync";
   }

   public void a(ByteBuffer var1) {
      int var2 = com.coremedia.iso.e.d(var1);
      this.a = (var2 & 192) >> 6;
      this.b = var2 & 63;
   }

   public ByteBuffer b() {
      ByteBuffer var1 = ByteBuffer.allocate(1);
      g.c(var1, this.b + (this.a << 6));
      return (ByteBuffer)var1.rewind();
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(this != var1) {
         if(var1 != null && this.getClass() == var1.getClass()) {
            d var3 = (d)var1;
            if(this.b != var3.b) {
               var2 = false;
            } else if(this.a != var3.a) {
               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public int hashCode() {
      return this.a * 31 + this.b;
   }

   public String toString() {
      return "SyncSampleEntry{reserved=" + this.a + ", nalUnitType=" + this.b + '}';
   }
}
