package com.mp4parser.iso14496.part15;

import com.coremedia.iso.g;
import java.nio.ByteBuffer;

public class e extends com.googlecode.mp4parser.boxes.mp4.samplegrouping.b {
   int a;
   int b;
   boolean c;
   int d;
   long e;
   long f;
   int g;
   int h;
   int i;
   int j;
   int k;

   public String a() {
      return "tscl";
   }

   public void a(ByteBuffer var1) {
      this.a = com.coremedia.iso.e.d(var1);
      int var2 = com.coremedia.iso.e.d(var1);
      this.b = (var2 & 192) >> 6;
      boolean var3;
      if((var2 & 32) > 0) {
         var3 = true;
      } else {
         var3 = false;
      }

      this.c = var3;
      this.d = var2 & 31;
      this.e = com.coremedia.iso.e.a(var1);
      this.f = com.coremedia.iso.e.l(var1);
      this.g = com.coremedia.iso.e.d(var1);
      this.h = com.coremedia.iso.e.c(var1);
      this.i = com.coremedia.iso.e.c(var1);
      this.j = com.coremedia.iso.e.d(var1);
      this.k = com.coremedia.iso.e.c(var1);
   }

   public ByteBuffer b() {
      ByteBuffer var3 = ByteBuffer.allocate(20);
      g.c(var3, this.a);
      int var2 = this.b;
      byte var1;
      if(this.c) {
         var1 = 32;
      } else {
         var1 = 0;
      }

      g.c(var3, var1 + (var2 << 6) + this.d);
      g.b(var3, this.e);
      g.c(var3, this.f);
      g.c(var3, this.g);
      g.b(var3, this.h);
      g.b(var3, this.i);
      g.c(var3, this.j);
      g.b(var3, this.k);
      return (ByteBuffer)var3.rewind();
   }

   public int c() {
      return 20;
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(this != var1) {
         if(var1 != null && this.getClass() == var1.getClass()) {
            e var3 = (e)var1;
            if(this.a != var3.a) {
               var2 = false;
            } else if(this.i != var3.i) {
               var2 = false;
            } else if(this.k != var3.k) {
               var2 = false;
            } else if(this.j != var3.j) {
               var2 = false;
            } else if(this.h != var3.h) {
               var2 = false;
            } else if(this.f != var3.f) {
               var2 = false;
            } else if(this.g != var3.g) {
               var2 = false;
            } else if(this.e != var3.e) {
               var2 = false;
            } else if(this.d != var3.d) {
               var2 = false;
            } else if(this.b != var3.b) {
               var2 = false;
            } else if(this.c != var3.c) {
               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public int hashCode() {
      int var2 = this.a;
      int var3 = this.b;
      byte var1;
      if(this.c) {
         var1 = 1;
      } else {
         var1 = 0;
      }

      return ((((((((var1 + (var2 * 31 + var3) * 31) * 31 + this.d) * 31 + (int)(this.e ^ this.e >>> 32)) * 31 + (int)(this.f ^ this.f >>> 32)) * 31 + this.g) * 31 + this.h) * 31 + this.i) * 31 + this.j) * 31 + this.k;
   }

   public String toString() {
      return "TemporalLayerSampleGroup{temporalLayerId=" + this.a + ", tlprofile_space=" + this.b + ", tltier_flag=" + this.c + ", tlprofile_idc=" + this.d + ", tlprofile_compatibility_flags=" + this.e + ", tlconstraint_indicator_flags=" + this.f + ", tllevel_idc=" + this.g + ", tlMaxBitRate=" + this.h + ", tlAvgBitRate=" + this.i + ", tlConstantFrameRate=" + this.j + ", tlAvgFrameRate=" + this.k + '}';
   }
}
