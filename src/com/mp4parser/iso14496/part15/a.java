package com.mp4parser.iso14496.part15;

import com.coremedia.iso.g;
import com.googlecode.mp4parser.b.a.h;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class a {
   public int a;
   public int b;
   public int c;
   public int d;
   public int e;
   public List f;
   public List g;
   public boolean h;
   public int i;
   public int j;
   public int k;
   public List l;
   public int m;
   public int n;
   public int o;
   public int p;
   public int q;

   public a() {
      this.f = new ArrayList();
      this.g = new ArrayList();
      this.h = true;
      this.i = 1;
      this.j = 0;
      this.k = 0;
      this.l = new ArrayList();
      this.m = 63;
      this.n = 7;
      this.o = 31;
      this.p = 31;
      this.q = 31;
   }

   public a(ByteBuffer var1) {
      byte var3 = 0;
      super();
      this.f = new ArrayList();
      this.g = new ArrayList();
      this.h = true;
      this.i = 1;
      this.j = 0;
      this.k = 0;
      this.l = new ArrayList();
      this.m = 63;
      this.n = 7;
      this.o = 31;
      this.p = 31;
      this.q = 31;
      this.a = com.coremedia.iso.e.d(var1);
      this.b = com.coremedia.iso.e.d(var1);
      this.c = com.coremedia.iso.e.d(var1);
      this.d = com.coremedia.iso.e.d(var1);
      com.googlecode.mp4parser.boxes.mp4.a.c var7 = new com.googlecode.mp4parser.boxes.mp4.a.c(var1);
      this.m = var7.a(6);
      this.e = var7.a(2);
      this.n = var7.a(3);
      int var4 = var7.a(5);

      int var2;
      byte[] var8;
      for(var2 = 0; var2 < var4; ++var2) {
         var8 = new byte[com.coremedia.iso.e.c(var1)];
         var1.get(var8);
         this.f.add(var8);
      }

      long var5 = (long)com.coremedia.iso.e.d(var1);

      for(var2 = 0; (long)var2 < var5; ++var2) {
         var8 = new byte[com.coremedia.iso.e.c(var1)];
         var1.get(var8);
         this.g.add(var8);
      }

      if(var1.remaining() < 4) {
         this.h = false;
      }

      if(this.h && (this.b == 100 || this.b == 110 || this.b == 122 || this.b == 144)) {
         var7 = new com.googlecode.mp4parser.boxes.mp4.a.c(var1);
         this.o = var7.a(6);
         this.i = var7.a(2);
         this.p = var7.a(5);
         this.j = var7.a(3);
         this.q = var7.a(5);
         this.k = var7.a(3);
         var5 = (long)com.coremedia.iso.e.d(var1);

         for(var2 = var3; (long)var2 < var5; ++var2) {
            var8 = new byte[com.coremedia.iso.e.c(var1)];
            var1.get(var8);
            this.l.add(var8);
         }
      } else {
         this.i = -1;
         this.j = -1;
         this.k = -1;
      }

   }

   public long a() {
      Iterator var5 = this.f.iterator();

      long var1;
      for(var1 = 5L + 1L; var5.hasNext(); var1 = (long)((byte[])var5.next()).length + var1 + 2L) {
         ;
      }

      var5 = this.g.iterator();
      ++var1;

      while(var5.hasNext()) {
         var1 = (long)((byte[])var5.next()).length + var1 + 2L;
      }

      long var3 = var1;
      if(this.h) {
         if(this.b != 100 && this.b != 110 && this.b != 122) {
            var3 = var1;
            if(this.b != 144) {
               return var3;
            }
         }

         var5 = this.l.iterator();

         for(var1 += 4L; var5.hasNext(); var1 = (long)((byte[])var5.next()).length + var1 + 2L) {
            ;
         }

         var3 = var1;
      }

      return var3;
   }

   public void a(ByteBuffer var1) {
      g.c(var1, this.a);
      g.c(var1, this.b);
      g.c(var1, this.c);
      g.c(var1, this.d);
      com.googlecode.mp4parser.boxes.mp4.a.d var2 = new com.googlecode.mp4parser.boxes.mp4.a.d(var1);
      var2.a(this.m, 6);
      var2.a(this.e, 2);
      var2.a(this.n, 3);
      var2.a(this.g.size(), 5);
      Iterator var4 = this.f.iterator();

      byte[] var3;
      while(var4.hasNext()) {
         var3 = (byte[])var4.next();
         g.b(var1, var3.length);
         var1.put(var3);
      }

      g.c(var1, this.g.size());
      var4 = this.g.iterator();

      while(var4.hasNext()) {
         var3 = (byte[])var4.next();
         g.b(var1, var3.length);
         var1.put(var3);
      }

      if(this.h && (this.b == 100 || this.b == 110 || this.b == 122 || this.b == 144)) {
         var2 = new com.googlecode.mp4parser.boxes.mp4.a.d(var1);
         var2.a(this.o, 6);
         var2.a(this.i, 2);
         var2.a(this.p, 5);
         var2.a(this.j, 3);
         var2.a(this.q, 5);
         var2.a(this.k, 3);
         Iterator var6 = this.l.iterator();

         while(var6.hasNext()) {
            byte[] var5 = (byte[])var6.next();
            g.b(var1, var5.length);
            var1.put(var5);
         }
      }

   }

   public String[] b() {
      ArrayList var1 = new ArrayList();

      String var6;
      for(Iterator var2 = this.g.iterator(); var2.hasNext(); var1.add(var6)) {
         byte[] var4 = (byte[])var2.next();

         try {
            ByteArrayInputStream var3 = new ByteArrayInputStream(var4, 1, var4.length - 1);
            var6 = com.googlecode.mp4parser.b.a.e.a(var3).toString();
         } catch (IOException var5) {
            throw new RuntimeException(var5);
         }
      }

      return (String[])var1.toArray(new String[var1.size()]);
   }

   public String[] c() {
      ArrayList var2 = new ArrayList();

      String var1;
      for(Iterator var3 = this.f.iterator(); var3.hasNext(); var2.add(var1)) {
         byte[] var5 = (byte[])var3.next();

         try {
            ByteArrayInputStream var4 = new ByteArrayInputStream(var5, 1, var5.length - 1);
            com.googlecode.mp4parser.authoring.tracks.b var7 = new com.googlecode.mp4parser.authoring.tracks.b(var4);
            var1 = h.a((InputStream)var7).toString();
         } catch (IOException var6) {
            var1 = "not parsable";
         }
      }

      return (String[])var2.toArray(new String[var2.size()]);
   }

   public List d() {
      ArrayList var2 = new ArrayList(this.f.size());
      Iterator var1 = this.f.iterator();

      while(var1.hasNext()) {
         var2.add(com.coremedia.iso.c.a((byte[])var1.next()));
      }

      return var2;
   }

   public List e() {
      ArrayList var1 = new ArrayList(this.l.size());
      Iterator var2 = this.l.iterator();

      while(var2.hasNext()) {
         var1.add(com.coremedia.iso.c.a((byte[])var2.next()));
      }

      return var1;
   }

   public List f() {
      ArrayList var2 = new ArrayList(this.g.size());
      Iterator var1 = this.g.iterator();

      while(var1.hasNext()) {
         var2.add(com.coremedia.iso.c.a((byte[])var1.next()));
      }

      return var2;
   }

   public String toString() {
      return "AvcDecoderConfigurationRecord{configurationVersion=" + this.a + ", avcProfileIndication=" + this.b + ", profileCompatibility=" + this.c + ", avcLevelIndication=" + this.d + ", lengthSizeMinusOne=" + this.e + ", hasExts=" + this.h + ", chromaFormat=" + this.i + ", bitDepthLumaMinus8=" + this.j + ", bitDepthChromaMinus8=" + this.k + ", lengthSizeMinusOnePaddingBits=" + this.m + ", numberOfSequenceParameterSetsPaddingBits=" + this.n + ", chromaFormatPaddingBits=" + this.o + ", bitDepthLumaMinus8PaddingBits=" + this.p + ", bitDepthChromaMinus8PaddingBits=" + this.q + '}';
   }
}
