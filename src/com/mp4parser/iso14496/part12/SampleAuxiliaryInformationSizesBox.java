package com.mp4parser.iso14496.part12;

import com.coremedia.iso.d;
import com.coremedia.iso.e;
import com.coremedia.iso.g;
import com.googlecode.mp4parser.AbstractFullBox;
import java.nio.ByteBuffer;
import org.mp4parser.aspectj.a.b.b;
import org.mp4parser.aspectj.lang.a;

public class SampleAuxiliaryInformationSizesBox extends AbstractFullBox {
   // $FF: synthetic field
   static final boolean $assertionsDisabled;
   public static final String TYPE = "saiz";
   private static final a.a ajc$tjp_0;
   private static final a.a ajc$tjp_1;
   private static final a.a ajc$tjp_10;
   private static final a.a ajc$tjp_11;
   private static final a.a ajc$tjp_2;
   private static final a.a ajc$tjp_3;
   private static final a.a ajc$tjp_4;
   private static final a.a ajc$tjp_5;
   private static final a.a ajc$tjp_6;
   private static final a.a ajc$tjp_7;
   private static final a.a ajc$tjp_8;
   private static final a.a ajc$tjp_9;
   private String auxInfoType;
   private String auxInfoTypeParameter;
   private short defaultSampleInfoSize;
   private int sampleCount;
   private short[] sampleInfoSizes = new short[0];

   static {
      ajc$preClinit();
      boolean var0;
      if(!SampleAuxiliaryInformationSizesBox.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      $assertionsDisabled = var0;
   }

   public SampleAuxiliaryInformationSizesBox() {
      super("saiz");
   }

   private static void ajc$preClinit() {
      b var0 = new b("SampleAuxiliaryInformationSizesBox.java", SampleAuxiliaryInformationSizesBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getSize", "com.mp4parser.iso14496.part12.SampleAuxiliaryInformationSizesBox", "int", "index", "", "short"), 57);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "getAuxInfoType", "com.mp4parser.iso14496.part12.SampleAuxiliaryInformationSizesBox", "", "", "", "java.lang.String"), 106);
      ajc$tjp_10 = var0.a("method-execution", var0.a("1", "setSampleCount", "com.mp4parser.iso14496.part12.SampleAuxiliaryInformationSizesBox", "int", "sampleCount", "", "void"), 146);
      ajc$tjp_11 = var0.a("method-execution", var0.a("1", "toString", "com.mp4parser.iso14496.part12.SampleAuxiliaryInformationSizesBox", "", "", "", "java.lang.String"), 151);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "setAuxInfoType", "com.mp4parser.iso14496.part12.SampleAuxiliaryInformationSizesBox", "java.lang.String", "auxInfoType", "", "void"), 110);
      ajc$tjp_3 = var0.a("method-execution", var0.a("1", "getAuxInfoTypeParameter", "com.mp4parser.iso14496.part12.SampleAuxiliaryInformationSizesBox", "", "", "", "java.lang.String"), 114);
      ajc$tjp_4 = var0.a("method-execution", var0.a("1", "setAuxInfoTypeParameter", "com.mp4parser.iso14496.part12.SampleAuxiliaryInformationSizesBox", "java.lang.String", "auxInfoTypeParameter", "", "void"), 118);
      ajc$tjp_5 = var0.a("method-execution", var0.a("1", "getDefaultSampleInfoSize", "com.mp4parser.iso14496.part12.SampleAuxiliaryInformationSizesBox", "", "", "", "int"), 122);
      ajc$tjp_6 = var0.a("method-execution", var0.a("1", "setDefaultSampleInfoSize", "com.mp4parser.iso14496.part12.SampleAuxiliaryInformationSizesBox", "int", "defaultSampleInfoSize", "", "void"), 126);
      ajc$tjp_7 = var0.a("method-execution", var0.a("1", "getSampleInfoSizes", "com.mp4parser.iso14496.part12.SampleAuxiliaryInformationSizesBox", "", "", "", "[S"), 131);
      ajc$tjp_8 = var0.a("method-execution", var0.a("1", "setSampleInfoSizes", "com.mp4parser.iso14496.part12.SampleAuxiliaryInformationSizesBox", "[S", "sampleInfoSizes", "", "void"), 137);
      ajc$tjp_9 = var0.a("method-execution", var0.a("1", "getSampleCount", "com.mp4parser.iso14496.part12.SampleAuxiliaryInformationSizesBox", "", "", "", "int"), 142);
   }

   public void _parseDetails(ByteBuffer var1) {
      this.parseVersionAndFlags(var1);
      if((this.getFlags() & 1) == 1) {
         this.auxInfoType = e.k(var1);
         this.auxInfoTypeParameter = e.k(var1);
      }

      this.defaultSampleInfoSize = (short)e.d(var1);
      this.sampleCount = com.googlecode.mp4parser.c.b.a(e.a(var1));
      if(this.defaultSampleInfoSize == 0) {
         this.sampleInfoSizes = new short[this.sampleCount];

         for(int var2 = 0; var2 < this.sampleCount; ++var2) {
            this.sampleInfoSizes[var2] = (short)e.d(var1);
         }
      }

   }

   public String getAuxInfoType() {
      a var1 = b.a(ajc$tjp_1, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.auxInfoType;
   }

   public String getAuxInfoTypeParameter() {
      a var1 = b.a(ajc$tjp_3, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.auxInfoTypeParameter;
   }

   protected void getContent(ByteBuffer var1) {
      this.writeVersionAndFlags(var1);
      if((this.getFlags() & 1) == 1) {
         var1.put(d.a(this.auxInfoType));
         var1.put(d.a(this.auxInfoTypeParameter));
      }

      g.c(var1, this.defaultSampleInfoSize);
      if(this.defaultSampleInfoSize == 0) {
         g.b(var1, (long)this.sampleInfoSizes.length);
         short[] var4 = this.sampleInfoSizes;
         int var3 = var4.length;

         for(int var2 = 0; var2 < var3; ++var2) {
            g.c(var1, var4[var2]);
         }
      } else {
         g.b(var1, (long)this.sampleCount);
      }

   }

   protected long getContentSize() {
      byte var1 = 4;
      if((this.getFlags() & 1) == 1) {
         var1 = 12;
      }

      int var2;
      if(this.defaultSampleInfoSize == 0) {
         var2 = this.sampleInfoSizes.length;
      } else {
         var2 = 0;
      }

      return (long)(var2 + var1 + 5);
   }

   public int getDefaultSampleInfoSize() {
      a var1 = b.a(ajc$tjp_5, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.defaultSampleInfoSize;
   }

   public int getSampleCount() {
      a var1 = b.a(ajc$tjp_9, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.sampleCount;
   }

   public short[] getSampleInfoSizes() {
      a var1 = b.a(ajc$tjp_7, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      short[] var2 = new short[this.sampleInfoSizes.length];
      System.arraycopy(this.sampleInfoSizes, 0, var2, 0, this.sampleInfoSizes.length);
      return var2;
   }

   public short getSize(int var1) {
      a var3 = b.a(ajc$tjp_0, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var3);
      short var2;
      if(this.getDefaultSampleInfoSize() == 0) {
         var2 = this.sampleInfoSizes[var1];
      } else {
         var2 = this.defaultSampleInfoSize;
      }

      return var2;
   }

   public void setAuxInfoType(String var1) {
      a var2 = b.a(ajc$tjp_2, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.auxInfoType = var1;
   }

   public void setAuxInfoTypeParameter(String var1) {
      a var2 = b.a(ajc$tjp_4, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.auxInfoTypeParameter = var1;
   }

   public void setDefaultSampleInfoSize(int var1) {
      a var2 = b.a(ajc$tjp_6, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      if(!$assertionsDisabled && var1 > 255) {
         throw new AssertionError();
      } else {
         this.defaultSampleInfoSize = (short)var1;
      }
   }

   public void setSampleCount(int var1) {
      a var2 = b.a(ajc$tjp_10, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var2);
      this.sampleCount = var1;
   }

   public void setSampleInfoSizes(short[] var1) {
      a var2 = b.a(ajc$tjp_8, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.sampleInfoSizes = new short[var1.length];
      System.arraycopy(var1, 0, this.sampleInfoSizes, 0, var1.length);
   }

   public String toString() {
      a var1 = b.a(ajc$tjp_11, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return "SampleAuxiliaryInformationSizesBox{defaultSampleInfoSize=" + this.defaultSampleInfoSize + ", sampleCount=" + this.sampleCount + ", auxInfoType='" + this.auxInfoType + '\'' + ", auxInfoTypeParameter='" + this.auxInfoTypeParameter + '\'' + '}';
   }
}
