package com.mp4parser.iso14496.part12;

import com.coremedia.iso.e;
import com.coremedia.iso.g;
import com.googlecode.mp4parser.AbstractBox;
import com.googlecode.mp4parser.c.i;
import java.nio.ByteBuffer;
import org.mp4parser.aspectj.a.b.b;
import org.mp4parser.aspectj.lang.a;

public class TrackReferenceTypeBox extends AbstractBox {
   private static final a.a ajc$tjp_0;
   private static final a.a ajc$tjp_1;
   long[] trackIds = new long[0];

   static {
      ajc$preClinit();
   }

   public TrackReferenceTypeBox(String var1) {
      super(var1);
   }

   private static void ajc$preClinit() {
      b var0 = new b("TrackReferenceTypeBox.java", TrackReferenceTypeBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getTrackIds", "com.mp4parser.iso14496.part12.TrackReferenceTypeBox", "", "", "", "[J"), 58);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "setTrackIds", "com.mp4parser.iso14496.part12.TrackReferenceTypeBox", "[J", "trackIds", "", "void"), 62);
   }

   protected void _parseDetails(ByteBuffer var1) {
      while(var1.remaining() >= 4) {
         this.trackIds = i.a(this.trackIds, new long[]{e.a(var1)});
      }

   }

   protected void getContent(ByteBuffer var1) {
      long[] var4 = this.trackIds;
      int var3 = var4.length;

      for(int var2 = 0; var2 < var3; ++var2) {
         g.b(var1, var4[var2]);
      }

   }

   protected long getContentSize() {
      return (long)(this.trackIds.length * 4);
   }

   public long[] getTrackIds() {
      a var1 = b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.trackIds;
   }

   public void setTrackIds(long[] var1) {
      a var2 = b.a(ajc$tjp_1, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.trackIds = var1;
   }
}
