package com.mp4parser.iso14496.part12;

import com.coremedia.iso.e;
import com.coremedia.iso.g;
import com.googlecode.mp4parser.AbstractBox;
import java.nio.ByteBuffer;
import org.mp4parser.aspectj.a.b.b;
import org.mp4parser.aspectj.lang.a;

public final class BitRateBox extends AbstractBox {
   public static final String TYPE = "btrt";
   private static final a.a ajc$tjp_0;
   private static final a.a ajc$tjp_1;
   private static final a.a ajc$tjp_2;
   private static final a.a ajc$tjp_3;
   private static final a.a ajc$tjp_4;
   private static final a.a ajc$tjp_5;
   private long avgBitrate;
   private long bufferSizeDb;
   private long maxBitrate;

   static {
      ajc$preClinit();
   }

   public BitRateBox() {
      super("btrt");
   }

   private static void ajc$preClinit() {
      b var0 = new b("BitRateBox.java", BitRateBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getBufferSizeDb", "com.mp4parser.iso14496.part12.BitRateBox", "", "", "", "long"), 74);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "setBufferSizeDb", "com.mp4parser.iso14496.part12.BitRateBox", "long", "bufferSizeDb", "", "void"), 82);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "getMaxBitrate", "com.mp4parser.iso14496.part12.BitRateBox", "", "", "", "long"), 90);
      ajc$tjp_3 = var0.a("method-execution", var0.a("1", "setMaxBitrate", "com.mp4parser.iso14496.part12.BitRateBox", "long", "maxBitrate", "", "void"), 98);
      ajc$tjp_4 = var0.a("method-execution", var0.a("1", "getAvgBitrate", "com.mp4parser.iso14496.part12.BitRateBox", "", "", "", "long"), 106);
      ajc$tjp_5 = var0.a("method-execution", var0.a("1", "setAvgBitrate", "com.mp4parser.iso14496.part12.BitRateBox", "long", "avgBitrate", "", "void"), 114);
   }

   public void _parseDetails(ByteBuffer var1) {
      this.bufferSizeDb = e.a(var1);
      this.maxBitrate = e.a(var1);
      this.avgBitrate = e.a(var1);
   }

   public long getAvgBitrate() {
      a var1 = b.a(ajc$tjp_4, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.avgBitrate;
   }

   public long getBufferSizeDb() {
      a var1 = b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.bufferSizeDb;
   }

   protected void getContent(ByteBuffer var1) {
      g.b(var1, this.bufferSizeDb);
      g.b(var1, this.maxBitrate);
      g.b(var1, this.avgBitrate);
   }

   protected long getContentSize() {
      return 12L;
   }

   public long getMaxBitrate() {
      a var1 = b.a(ajc$tjp_2, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.maxBitrate;
   }

   public void setAvgBitrate(long var1) {
      a var3 = b.a(ajc$tjp_5, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var3);
      this.avgBitrate = var1;
   }

   public void setBufferSizeDb(long var1) {
      a var3 = b.a(ajc$tjp_1, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var3);
      this.bufferSizeDb = var1;
   }

   public void setMaxBitrate(long var1) {
      a var3 = b.a(ajc$tjp_3, this, this, (Object)org.mp4parser.aspectj.a.a.a.a(var1));
      com.googlecode.mp4parser.e.a().a(var3);
      this.maxBitrate = var1;
   }
}
