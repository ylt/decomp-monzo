package com.mp4parser.iso14496.part12;

import com.coremedia.iso.d;
import com.coremedia.iso.e;
import com.coremedia.iso.g;
import com.googlecode.mp4parser.AbstractFullBox;
import java.nio.ByteBuffer;
import org.mp4parser.aspectj.a.b.b;
import org.mp4parser.aspectj.lang.a;

public class SampleAuxiliaryInformationOffsetsBox extends AbstractFullBox {
   public static final String TYPE = "saio";
   private static final a.a ajc$tjp_0;
   private static final a.a ajc$tjp_1;
   private static final a.a ajc$tjp_2;
   private static final a.a ajc$tjp_3;
   private static final a.a ajc$tjp_4;
   private static final a.a ajc$tjp_5;
   private String auxInfoType;
   private String auxInfoTypeParameter;
   private long[] offsets = new long[0];

   static {
      ajc$preClinit();
   }

   public SampleAuxiliaryInformationOffsetsBox() {
      super("saio");
   }

   private static void ajc$preClinit() {
      b var0 = new b("SampleAuxiliaryInformationOffsetsBox.java", SampleAuxiliaryInformationOffsetsBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getAuxInfoType", "com.mp4parser.iso14496.part12.SampleAuxiliaryInformationOffsetsBox", "", "", "", "java.lang.String"), 107);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "setAuxInfoType", "com.mp4parser.iso14496.part12.SampleAuxiliaryInformationOffsetsBox", "java.lang.String", "auxInfoType", "", "void"), 111);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "getAuxInfoTypeParameter", "com.mp4parser.iso14496.part12.SampleAuxiliaryInformationOffsetsBox", "", "", "", "java.lang.String"), 115);
      ajc$tjp_3 = var0.a("method-execution", var0.a("1", "setAuxInfoTypeParameter", "com.mp4parser.iso14496.part12.SampleAuxiliaryInformationOffsetsBox", "java.lang.String", "auxInfoTypeParameter", "", "void"), 119);
      ajc$tjp_4 = var0.a("method-execution", var0.a("1", "getOffsets", "com.mp4parser.iso14496.part12.SampleAuxiliaryInformationOffsetsBox", "", "", "", "[J"), 123);
      ajc$tjp_5 = var0.a("method-execution", var0.a("1", "setOffsets", "com.mp4parser.iso14496.part12.SampleAuxiliaryInformationOffsetsBox", "[J", "offsets", "", "void"), 127);
   }

   public void _parseDetails(ByteBuffer var1) {
      this.parseVersionAndFlags(var1);
      if((this.getFlags() & 1) == 1) {
         this.auxInfoType = e.k(var1);
         this.auxInfoTypeParameter = e.k(var1);
      }

      int var3 = com.googlecode.mp4parser.c.b.a(e.a(var1));
      this.offsets = new long[var3];

      for(int var2 = 0; var2 < var3; ++var2) {
         if(this.getVersion() == 0) {
            this.offsets[var2] = e.a(var1);
         } else {
            this.offsets[var2] = e.f(var1);
         }
      }

   }

   public String getAuxInfoType() {
      a var1 = b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.auxInfoType;
   }

   public String getAuxInfoTypeParameter() {
      a var1 = b.a(ajc$tjp_2, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.auxInfoTypeParameter;
   }

   protected void getContent(ByteBuffer var1) {
      this.writeVersionAndFlags(var1);
      if((this.getFlags() & 1) == 1) {
         var1.put(d.a(this.auxInfoType));
         var1.put(d.a(this.auxInfoTypeParameter));
      }

      g.b(var1, (long)this.offsets.length);
      long[] var4 = this.offsets;
      int var3 = var4.length;

      for(int var2 = 0; var2 < var3; ++var2) {
         Long var5 = Long.valueOf(var4[var2]);
         if(this.getVersion() == 0) {
            g.b(var1, var5.longValue());
         } else {
            g.a(var1, var5.longValue());
         }
      }

   }

   protected long getContentSize() {
      int var1;
      if(this.getVersion() == 0) {
         var1 = this.offsets.length * 4;
      } else {
         var1 = this.offsets.length * 8;
      }

      byte var2;
      if((this.getFlags() & 1) == 1) {
         var2 = 8;
      } else {
         var2 = 0;
      }

      return (long)(var2 + var1 + 8);
   }

   public long[] getOffsets() {
      a var1 = b.a(ajc$tjp_4, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.offsets;
   }

   public void setAuxInfoType(String var1) {
      a var2 = b.a(ajc$tjp_1, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.auxInfoType = var1;
   }

   public void setAuxInfoTypeParameter(String var1) {
      a var2 = b.a(ajc$tjp_3, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.auxInfoTypeParameter = var1;
   }

   public void setOffsets(long[] var1) {
      a var2 = b.a(ajc$tjp_5, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.offsets = var1;
   }
}
