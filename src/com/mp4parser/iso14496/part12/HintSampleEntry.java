package com.mp4parser.iso14496.part12;

import com.coremedia.iso.e;
import com.coremedia.iso.g;
import com.coremedia.iso.boxes.sampleentry.AbstractSampleEntry;
import com.googlecode.mp4parser.b;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;

public class HintSampleEntry extends AbstractSampleEntry {
   protected byte[] data;

   public HintSampleEntry(String var1) {
      super(var1);
   }

   public void getBox(WritableByteChannel var1) throws IOException {
      var1.write(this.getHeader());
      ByteBuffer var2 = ByteBuffer.allocate(8);
      var2.position(6);
      g.b(var2, this.dataReferenceIndex);
      var2.rewind();
      var1.write(var2);
      var1.write(ByteBuffer.wrap(this.data));
   }

   public byte[] getData() {
      return this.data;
   }

   public long getSize() {
      long var2 = (long)(this.data.length + 8);
      byte var1;
      if(!this.largeBox && 8L + var2 < 4294967296L) {
         var1 = 8;
      } else {
         var1 = 16;
      }

      return (long)var1 + var2;
   }

   public void parse(b var1, ByteBuffer var2, long var3, com.coremedia.iso.b var5) throws IOException {
      var2 = ByteBuffer.allocate(8);
      var1.a(var2);
      var2.position(6);
      this.dataReferenceIndex = e.c(var2);
      this.data = new byte[com.googlecode.mp4parser.c.b.a(var3 - 8L)];
      var1.a(ByteBuffer.wrap(this.data));
   }

   public void setData(byte[] var1) {
      this.data = var1;
   }
}
