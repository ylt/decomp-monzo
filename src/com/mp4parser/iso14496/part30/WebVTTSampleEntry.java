package com.mp4parser.iso14496.part30;

import com.coremedia.iso.boxes.sampleentry.AbstractSampleEntry;
import com.googlecode.mp4parser.AbstractContainerBox;
import com.googlecode.mp4parser.b;
import com.googlecode.mp4parser.c.j;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;

public class WebVTTSampleEntry extends AbstractSampleEntry {
   public static final String TYPE = "wvtt";

   public WebVTTSampleEntry() {
      super("wvtt");
   }

   public void getBox(WritableByteChannel var1) throws IOException {
      var1.write(this.getHeader());
      this.writeContainer(var1);
   }

   public WebVTTConfigurationBox getConfig() {
      return (WebVTTConfigurationBox)j.a((AbstractContainerBox)this, "vttC");
   }

   public WebVTTSourceLabelBox getSourceLabel() {
      return (WebVTTSourceLabelBox)j.a((AbstractContainerBox)this, "vlab");
   }

   public void parse(b var1, ByteBuffer var2, long var3, com.coremedia.iso.b var5) throws IOException {
      this.initContainer(var1, var3, var5);
   }
}
