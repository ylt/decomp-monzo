package com.mp4parser.iso14496.part30;

import com.coremedia.iso.e;
import com.coremedia.iso.j;
import com.googlecode.mp4parser.AbstractBox;
import java.nio.ByteBuffer;
import org.mp4parser.aspectj.a.b.b;
import org.mp4parser.aspectj.lang.a;

public class WebVTTSourceLabelBox extends AbstractBox {
   public static final String TYPE = "vlab";
   private static final a.a ajc$tjp_0;
   private static final a.a ajc$tjp_1;
   String sourceLabel = "";

   static {
      ajc$preClinit();
   }

   public WebVTTSourceLabelBox() {
      super("vlab");
   }

   private static void ajc$preClinit() {
      b var0 = new b("WebVTTSourceLabelBox.java", WebVTTSourceLabelBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getSourceLabel", "com.mp4parser.iso14496.part30.WebVTTSourceLabelBox", "", "", "", "java.lang.String"), 37);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "setSourceLabel", "com.mp4parser.iso14496.part30.WebVTTSourceLabelBox", "java.lang.String", "sourceLabel", "", "void"), 41);
   }

   protected void _parseDetails(ByteBuffer var1) {
      this.sourceLabel = e.a(var1, var1.remaining());
   }

   protected void getContent(ByteBuffer var1) {
      var1.put(j.a(this.sourceLabel));
   }

   protected long getContentSize() {
      return (long)j.b(this.sourceLabel);
   }

   public String getSourceLabel() {
      a var1 = b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.sourceLabel;
   }

   public void setSourceLabel(String var1) {
      a var2 = b.a(ajc$tjp_1, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.sourceLabel = var1;
   }
}
