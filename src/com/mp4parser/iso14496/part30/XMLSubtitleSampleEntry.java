package com.mp4parser.iso14496.part30;

import com.coremedia.iso.e;
import com.coremedia.iso.g;
import com.coremedia.iso.boxes.sampleentry.AbstractSampleEntry;
import com.googlecode.mp4parser.b;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;

public class XMLSubtitleSampleEntry extends AbstractSampleEntry {
   public static final String TYPE = "stpp";
   private String auxiliaryMimeTypes = "";
   private String namespace = "";
   private String schemaLocation = "";

   public XMLSubtitleSampleEntry() {
      super("stpp");
   }

   public String getAuxiliaryMimeTypes() {
      return this.auxiliaryMimeTypes;
   }

   public void getBox(WritableByteChannel var1) throws IOException {
      var1.write(this.getHeader());
      ByteBuffer var2 = ByteBuffer.allocate(this.namespace.length() + 8 + this.schemaLocation.length() + this.auxiliaryMimeTypes.length() + 3);
      var2.position(6);
      g.b(var2, this.dataReferenceIndex);
      g.b(var2, this.namespace);
      g.b(var2, this.schemaLocation);
      g.b(var2, this.auxiliaryMimeTypes);
      var1.write((ByteBuffer)var2.rewind());
      this.writeContainer(var1);
   }

   public String getNamespace() {
      return this.namespace;
   }

   public String getSchemaLocation() {
      return this.schemaLocation;
   }

   public long getSize() {
      long var4 = this.getContainerSize();
      long var2 = (long)(this.namespace.length() + 8 + this.schemaLocation.length() + this.auxiliaryMimeTypes.length() + 3);
      byte var1;
      if(!this.largeBox && var4 + var2 + 8L < 4294967296L) {
         var1 = 8;
      } else {
         var1 = 16;
      }

      return (long)var1 + var4 + var2;
   }

   public void parse(b var1, ByteBuffer var2, long var3, com.coremedia.iso.b var5) throws IOException {
      ByteBuffer var8 = ByteBuffer.allocate(8);
      var1.a((ByteBuffer)var8.rewind());
      var8.position(6);
      this.dataReferenceIndex = e.c(var8);
      long var6 = var1.b();
      var8 = ByteBuffer.allocate(1024);
      var1.a((ByteBuffer)var8.rewind());
      this.namespace = e.e((ByteBuffer)var8.rewind());
      var1.a((long)this.namespace.length() + var6 + 1L);
      var1.a((ByteBuffer)var8.rewind());
      this.schemaLocation = e.e((ByteBuffer)var8.rewind());
      var1.a((long)this.namespace.length() + var6 + (long)this.schemaLocation.length() + 2L);
      var1.a((ByteBuffer)var8.rewind());
      this.auxiliaryMimeTypes = e.e((ByteBuffer)var8.rewind());
      var1.a((long)this.namespace.length() + var6 + (long)this.schemaLocation.length() + (long)this.auxiliaryMimeTypes.length() + 3L);
      this.initContainer(var1, var3 - (long)(var2.remaining() + this.namespace.length() + this.schemaLocation.length() + this.auxiliaryMimeTypes.length() + 3), var5);
   }

   public void setAuxiliaryMimeTypes(String var1) {
      this.auxiliaryMimeTypes = var1;
   }

   public void setNamespace(String var1) {
      this.namespace = var1;
   }

   public void setSchemaLocation(String var1) {
      this.schemaLocation = var1;
   }
}
