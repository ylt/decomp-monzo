package com.mp4parser.iso14496.part30;

import com.coremedia.iso.e;
import com.coremedia.iso.j;
import com.googlecode.mp4parser.AbstractBox;
import java.nio.ByteBuffer;
import org.mp4parser.aspectj.a.b.b;
import org.mp4parser.aspectj.lang.a;

public class WebVTTConfigurationBox extends AbstractBox {
   public static final String TYPE = "vttC";
   private static final a.a ajc$tjp_0;
   private static final a.a ajc$tjp_1;
   String config = "";

   static {
      ajc$preClinit();
   }

   public WebVTTConfigurationBox() {
      super("vttC");
   }

   private static void ajc$preClinit() {
      b var0 = new b("WebVTTConfigurationBox.java", WebVTTConfigurationBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getConfig", "com.mp4parser.iso14496.part30.WebVTTConfigurationBox", "", "", "", "java.lang.String"), 36);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "setConfig", "com.mp4parser.iso14496.part30.WebVTTConfigurationBox", "java.lang.String", "config", "", "void"), 40);
   }

   protected void _parseDetails(ByteBuffer var1) {
      this.config = e.a(var1, var1.remaining());
   }

   public String getConfig() {
      a var1 = b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.config;
   }

   protected void getContent(ByteBuffer var1) {
      var1.put(j.a(this.config));
   }

   protected long getContentSize() {
      return (long)j.b(this.config);
   }

   public void setConfig(String var1) {
      a var2 = b.a(ajc$tjp_1, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.config = var1;
   }
}
