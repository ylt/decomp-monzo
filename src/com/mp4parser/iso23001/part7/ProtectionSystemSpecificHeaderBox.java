package com.mp4parser.iso23001.part7;

import com.coremedia.iso.e;
import com.coremedia.iso.g;
import com.googlecode.mp4parser.AbstractFullBox;
import com.googlecode.mp4parser.c.k;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import org.mp4parser.aspectj.a.b.b;

public class ProtectionSystemSpecificHeaderBox extends AbstractFullBox {
   // $FF: synthetic field
   static final boolean $assertionsDisabled;
   public static byte[] OMA2_SYSTEM_ID;
   public static byte[] PLAYREADY_SYSTEM_ID;
   public static final String TYPE = "pssh";
   public static byte[] WIDEVINE;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_0;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_1;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_2;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_3;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_4;
   private static final org.mp4parser.aspectj.lang.a.a ajc$tjp_5;
   byte[] content;
   List keyIds = new ArrayList();
   byte[] systemId;

   static {
      ajc$preClinit();
      boolean var0;
      if(!ProtectionSystemSpecificHeaderBox.class.desiredAssertionStatus()) {
         var0 = true;
      } else {
         var0 = false;
      }

      $assertionsDisabled = var0;
      OMA2_SYSTEM_ID = k.a(UUID.fromString("A2B55680-6F43-11E0-9A3F-0002A5D5C51B"));
      WIDEVINE = k.a(UUID.fromString("edef8ba9-79d6-4ace-a3c8-27dcd51d21ed"));
      PLAYREADY_SYSTEM_ID = k.a(UUID.fromString("9A04F079-9840-4286-AB92-E65BE0885F95"));
   }

   public ProtectionSystemSpecificHeaderBox() {
      super("pssh");
   }

   public ProtectionSystemSpecificHeaderBox(byte[] var1, byte[] var2) {
      super("pssh");
      this.content = var2;
      this.systemId = var1;
   }

   private static void ajc$preClinit() {
      b var0 = new b("ProtectionSystemSpecificHeaderBox.java", ProtectionSystemSpecificHeaderBox.class);
      ajc$tjp_0 = var0.a("method-execution", var0.a("1", "getKeyIds", "com.mp4parser.iso23001.part7.ProtectionSystemSpecificHeaderBox", "", "", "", "java.util.List"), 50);
      ajc$tjp_1 = var0.a("method-execution", var0.a("1", "setKeyIds", "com.mp4parser.iso23001.part7.ProtectionSystemSpecificHeaderBox", "java.util.List", "keyIds", "", "void"), 54);
      ajc$tjp_2 = var0.a("method-execution", var0.a("1", "getSystemId", "com.mp4parser.iso23001.part7.ProtectionSystemSpecificHeaderBox", "", "", "", "[B"), 61);
      ajc$tjp_3 = var0.a("method-execution", var0.a("1", "setSystemId", "com.mp4parser.iso23001.part7.ProtectionSystemSpecificHeaderBox", "[B", "systemId", "", "void"), 65);
      ajc$tjp_4 = var0.a("method-execution", var0.a("1", "getContent", "com.mp4parser.iso23001.part7.ProtectionSystemSpecificHeaderBox", "", "", "", "[B"), 70);
      ajc$tjp_5 = var0.a("method-execution", var0.a("1", "setContent", "com.mp4parser.iso23001.part7.ProtectionSystemSpecificHeaderBox", "[B", "content", "", "void"), 74);
   }

   protected void _parseDetails(ByteBuffer var1) {
      this.parseVersionAndFlags(var1);
      this.systemId = new byte[16];
      var1.get(this.systemId);
      if(this.getVersion() > 0) {
         for(int var2 = com.googlecode.mp4parser.c.b.a(e.a(var1)); var2 > 0; --var2) {
            byte[] var5 = new byte[16];
            var1.get(var5);
            this.keyIds.add(k.a(var5));
         }
      }

      long var3 = e.a(var1);
      this.content = new byte[var1.remaining()];
      var1.get(this.content);
      if(!$assertionsDisabled && var3 != (long)this.content.length) {
         throw new AssertionError();
      }
   }

   protected void getContent(ByteBuffer var1) {
      this.writeVersionAndFlags(var1);
      if(!$assertionsDisabled && this.systemId.length != 16) {
         throw new AssertionError();
      } else {
         var1.put(this.systemId, 0, 16);
         if(this.getVersion() > 0) {
            g.b(var1, (long)this.keyIds.size());
            Iterator var2 = this.keyIds.iterator();

            while(var2.hasNext()) {
               var1.put(k.a((UUID)var2.next()));
            }
         }

         g.b(var1, (long)this.content.length);
         var1.put(this.content);
      }
   }

   public byte[] getContent() {
      org.mp4parser.aspectj.lang.a var1 = b.a(ajc$tjp_4, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.content;
   }

   protected long getContentSize() {
      long var3 = (long)(this.content.length + 24);
      long var1 = var3;
      if(this.getVersion() > 0) {
         var1 = var3 + 4L + (long)(this.keyIds.size() * 16);
      }

      return var1;
   }

   public List getKeyIds() {
      org.mp4parser.aspectj.lang.a var1 = b.a(ajc$tjp_0, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.keyIds;
   }

   public byte[] getSystemId() {
      org.mp4parser.aspectj.lang.a var1 = b.a(ajc$tjp_2, this, this);
      com.googlecode.mp4parser.e.a().a(var1);
      return this.systemId;
   }

   public void setContent(byte[] var1) {
      org.mp4parser.aspectj.lang.a var2 = b.a(ajc$tjp_5, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.content = var1;
   }

   public void setKeyIds(List var1) {
      org.mp4parser.aspectj.lang.a var2 = b.a(ajc$tjp_1, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      this.keyIds = var1;
   }

   public void setSystemId(byte[] var1) {
      org.mp4parser.aspectj.lang.a var2 = b.a(ajc$tjp_3, this, this, (Object)var1);
      com.googlecode.mp4parser.e.a().a(var2);
      if(!$assertionsDisabled && var1.length != 16) {
         throw new AssertionError();
      } else {
         this.systemId = var1;
      }
   }
}
