package com.mp4parser.iso23001.part7;

import java.math.BigInteger;
import java.util.Arrays;

public class a {
   public byte[] a = new byte[0];
   public a.j[] b = null;

   public int a() {
      int var2 = this.a.length;
      int var1 = var2;
      if(this.b != null) {
         var1 = var2;
         if(this.b.length > 0) {
            var1 = var2 + 2 + this.b.length * 6;
         }
      }

      return var1;
   }

   public a.j a(int var1, long var2) {
      Object var4;
      if(var1 <= 127) {
         if(var2 <= 127L) {
            var4 = new a.b(var1, var2);
         } else if(var2 <= 32767L) {
            var4 = new a.e(var1, var2);
         } else if(var2 <= 2147483647L) {
            var4 = new a.c(var1, var2);
         } else {
            var4 = new a.d(var1, var2);
         }
      } else if(var1 <= 32767) {
         if(var2 <= 127L) {
            var4 = new a.k(var1, var2);
         } else if(var2 <= 32767L) {
            var4 = new a.n(var1, var2);
         } else if(var2 <= 2147483647L) {
            var4 = new a.l(var1, var2);
         } else {
            var4 = new a.m(var1, var2);
         }
      } else if(var2 <= 127L) {
         var4 = new a.f(var1, var2);
      } else if(var2 <= 32767L) {
         var4 = new a.i(var1, var2);
      } else if(var2 <= 2147483647L) {
         var4 = new a.g(var1, var2);
      } else {
         var4 = new a.h(var1, var2);
      }

      return (a.j)var4;
   }

   public boolean equals(Object var1) {
      boolean var2 = true;
      if(this != var1) {
         if(var1 != null && this.getClass() == var1.getClass()) {
            a var3 = (a)var1;
            if(!(new BigInteger(this.a)).equals(new BigInteger(var3.a))) {
               var2 = false;
            } else {
               if(this.b != null) {
                  if(Arrays.equals(this.b, var3.b)) {
                     return var2;
                  }
               } else if(var3.b == null) {
                  return var2;
               }

               var2 = false;
            }
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public int hashCode() {
      int var2 = 0;
      int var1;
      if(this.a != null) {
         var1 = Arrays.hashCode(this.a);
      } else {
         var1 = 0;
      }

      if(this.b != null) {
         var2 = Arrays.hashCode(this.b);
      }

      return var1 * 31 + var2;
   }

   public String toString() {
      return "Entry{iv=" + com.coremedia.iso.c.a(this.a) + ", pairs=" + Arrays.toString(this.b) + '}';
   }

   private abstract class a implements a.j {
      private a() {
      }

      // $FF: synthetic method
      a(a.a var2) {
         this();
      }

      public boolean equals(Object var1) {
         boolean var2 = true;
         if(this != var1) {
            if(var1 != null && this.getClass() == var1.getClass()) {
               a.j var3 = (a.j)var1;
               if(this.a() != var3.a()) {
                  var2 = false;
               } else if(this.b() != var3.b()) {
                  var2 = false;
               }
            } else {
               var2 = false;
            }
         }

         return var2;
      }

      public String toString() {
         return "P(" + this.a() + "|" + this.b() + ")";
      }
   }

   private class b extends a.a {
      private byte c;
      private byte d;

      public b(int var2, long var3) {
         super((a.a)null);
         this.c = (byte)var2;
         this.d = (byte)((int)var3);
      }

      public int a() {
         return this.c;
      }

      public long b() {
         return (long)this.d;
      }
   }

   private class c extends a.a {
      private byte c;
      private int d;

      public c(int var2, long var3) {
         super((a.a)null);
         this.c = (byte)var2;
         this.d = (int)var3;
      }

      public int a() {
         return this.c;
      }

      public long b() {
         return (long)this.d;
      }
   }

   private class d extends a.a {
      private byte c;
      private long d;

      public d(int var2, long var3) {
         super((a.a)null);
         this.c = (byte)var2;
         this.d = var3;
      }

      public int a() {
         return this.c;
      }

      public long b() {
         return this.d;
      }
   }

   private class e extends a.a {
      private byte c;
      private short d;

      public e(int var2, long var3) {
         super((a.a)null);
         this.c = (byte)var2;
         this.d = (short)((int)var3);
      }

      public int a() {
         return this.c;
      }

      public long b() {
         return (long)this.d;
      }
   }

   private class f extends a.a {
      private int c;
      private byte d;

      public f(int var2, long var3) {
         super((a.a)null);
         this.c = var2;
         this.d = (byte)((int)var3);
      }

      public int a() {
         return this.c;
      }

      public long b() {
         return (long)this.d;
      }
   }

   private class g extends a.a {
      private int c;
      private int d;

      public g(int var2, long var3) {
         super((a.a)null);
         this.c = var2;
         this.d = (int)var3;
      }

      public int a() {
         return this.c;
      }

      public long b() {
         return (long)this.d;
      }
   }

   private class h extends a.a {
      private int c;
      private long d;

      public h(int var2, long var3) {
         super((a.a)null);
         this.c = var2;
         this.d = var3;
      }

      public int a() {
         return this.c;
      }

      public long b() {
         return this.d;
      }
   }

   private class i extends a.a {
      private int c;
      private short d;

      public i(int var2, long var3) {
         super((a.a)null);
         this.c = var2;
         this.d = (short)((int)var3);
      }

      public int a() {
         return this.c;
      }

      public long b() {
         return (long)this.d;
      }
   }

   public interface j {
      int a();

      long b();
   }

   private class k extends a.a {
      private short c;
      private byte d;

      public k(int var2, long var3) {
         super((a.a)null);
         this.c = (short)var2;
         this.d = (byte)((int)var3);
      }

      public int a() {
         return this.c;
      }

      public long b() {
         return (long)this.d;
      }
   }

   private class l extends a.a {
      private short c;
      private int d;

      public l(int var2, long var3) {
         super((a.a)null);
         this.c = (short)var2;
         this.d = (int)var3;
      }

      public int a() {
         return this.c;
      }

      public long b() {
         return (long)this.d;
      }
   }

   private class m extends a.a {
      private short c;
      private long d;

      public m(int var2, long var3) {
         super((a.a)null);
         this.c = (short)var2;
         this.d = var3;
      }

      public int a() {
         return this.c;
      }

      public long b() {
         return this.d;
      }
   }

   private class n extends a.a {
      private short c;
      private short d;

      public n(int var2, long var3) {
         super((a.a)null);
         this.c = (short)var2;
         this.d = (short)((int)var3);
      }

      public int a() {
         return this.c;
      }

      public long b() {
         return (long)this.d;
      }
   }
}
