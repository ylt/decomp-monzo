package com.airbnb.lottie.a;

import android.graphics.PointF;
import android.support.v4.g.n;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import com.airbnb.lottie.e;
import com.airbnb.lottie.c.a.m;
import com.airbnb.lottie.d.b;
import com.airbnb.lottie.d.f;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class a {
   private static final Interpolator f = new LinearInterpolator();
   public final Object a;
   public final Object b;
   public final Interpolator c;
   public final float d;
   public Float e;
   private final e g;
   private float h = Float.MIN_VALUE;
   private float i = Float.MIN_VALUE;

   public a(e var1, Object var2, Object var3, Interpolator var4, float var5, Float var6) {
      this.g = var1;
      this.a = var2;
      this.b = var3;
      this.c = var4;
      this.d = var5;
      this.e = var6;
   }

   public static void a(List var0) {
      int var2 = var0.size();

      for(int var1 = 0; var1 < var2 - 1; ++var1) {
         ((a)var0.get(var1)).e = Float.valueOf(((a)var0.get(var1 + 1)).d);
      }

      a var3 = (a)var0.get(var2 - 1);
      if(var3.a == null) {
         var0.remove(var3);
      }

   }

   public float a() {
      if(this.h == Float.MIN_VALUE) {
         this.h = (this.d - (float)this.g.g()) / this.g.m();
      }

      return this.h;
   }

   public boolean a(float var1) {
      boolean var2;
      if(var1 >= this.a() && var1 <= this.b()) {
         var2 = true;
      } else {
         var2 = false;
      }

      return var2;
   }

   public float b() {
      if(this.i == Float.MIN_VALUE) {
         if(this.e == null) {
            this.i = 1.0F;
         } else {
            this.i = this.a() + (this.e.floatValue() - this.d) / this.g.m();
         }
      }

      return this.i;
   }

   public boolean c() {
      boolean var1;
      if(this.c == null) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public String toString() {
      return "Keyframe{startValue=" + this.a + ", endValue=" + this.b + ", startFrame=" + this.d + ", endFrame=" + this.e + ", interpolator=" + this.c + '}';
   }

   public static class a {
      private static n a;

      private static n a() {
         if(a == null) {
            a = new n();
         }

         return a;
      }

      public static a a(JSONObject var0, e var1, float var2, m.a var3) {
         float var4 = 0.0F;
         Object var6;
         Object var14;
         Interpolator var16;
         if(var0.has("t")) {
            var4 = (float)var0.optDouble("t", 0.0D);
            var6 = var0.opt("s");
            if(var6 != null) {
               var6 = var3.b(var6, var2);
            } else {
               var6 = null;
            }

            Object var7 = var0.opt("e");
            Object var15;
            if(var7 != null) {
               var15 = var3.b(var7, var2);
            } else {
               var15 = null;
            }

            JSONObject var8 = var0.optJSONObject("o");
            JSONObject var18 = var0.optJSONObject("i");
            PointF var9;
            PointF var10;
            if(var8 != null && var18 != null) {
               var9 = b.a(var8, var2);
               var10 = b.a(var18, var2);
            } else {
               var10 = null;
               var9 = null;
            }

            boolean var5;
            if(var0.optInt("h", 0) == 1) {
               var5 = true;
            } else {
               var5 = false;
            }

            Interpolator var12;
            if(var5) {
               var12 = a.f;
               var7 = var6;
            } else if(var9 != null) {
               label65: {
                  var9.x = com.airbnb.lottie.d.e.b(var9.x, -var2, var2);
                  var9.y = com.airbnb.lottie.d.e.b(var9.y, -100.0F, 100.0F);
                  var10.x = com.airbnb.lottie.d.e.b(var10.x, -var2, var2);
                  var10.y = com.airbnb.lottie.d.e.b(var10.y, -100.0F, 100.0F);
                  int var17 = f.a(var9.x, var9.y, var10.x, var10.y);
                  WeakReference var13 = a(var17);
                  Interpolator var19;
                  if(var13 != null) {
                     var19 = (Interpolator)var13.get();
                  } else {
                     var19 = null;
                  }

                  if(var13 != null) {
                     var12 = var19;
                     var7 = var15;
                     if(var19 != null) {
                        break label65;
                     }
                  }

                  var12 = android.support.v4.view.b.f.a(var9.x / var2, var9.y / var2, var10.x / var2, var10.y / var2);

                  try {
                     WeakReference var20 = new WeakReference(var12);
                     a(var17, var20);
                  } catch (ArrayIndexOutOfBoundsException var11) {
                     var7 = var15;
                     break label65;
                  }

                  var7 = var15;
               }
            } else {
               var12 = a.f;
               var7 = var15;
            }

            var16 = var12;
            var14 = var7;
            var2 = var4;
         } else {
            var14 = var3.b(var0, var2);
            var16 = null;
            var6 = var14;
            var2 = var4;
         }

         return new a(var1, var6, var14, var16, var2, (Float)null);
      }

      private static WeakReference a(int param0) {
         // $FF: Couldn't be decompiled
      }

      public static List a(JSONArray var0, e var1, float var2, m.a var3) {
         int var5 = var0.length();
         Object var7;
         if(var5 == 0) {
            var7 = Collections.emptyList();
         } else {
            ArrayList var6 = new ArrayList();

            for(int var4 = 0; var4 < var5; ++var4) {
               var6.add(a(var0.optJSONObject(var4), var1, var2, var3));
            }

            a.a(var6);
            var7 = var6;
         }

         return (List)var7;
      }

      private static void a(int param0, WeakReference param1) {
         // $FF: Couldn't be decompiled
      }
   }
}
