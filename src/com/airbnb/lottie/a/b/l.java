package com.airbnb.lottie.a.b;

import android.graphics.Path;
import java.util.List;

public class l extends a {
   private final com.airbnb.lottie.c.b.l b = new com.airbnb.lottie.c.b.l();
   private final Path c = new Path();

   public l(List var1) {
      super(var1);
   }

   // $FF: synthetic method
   public Object a(com.airbnb.lottie.a.a var1, float var2) {
      return this.b(var1, var2);
   }

   public Path b(com.airbnb.lottie.a.a var1, float var2) {
      com.airbnb.lottie.c.b.l var3 = (com.airbnb.lottie.c.b.l)var1.a;
      com.airbnb.lottie.c.b.l var4 = (com.airbnb.lottie.c.b.l)var1.b;
      this.b.a(var3, var4, var2);
      com.airbnb.lottie.d.e.a(this.b, this.c);
      return this.c;
   }
}
