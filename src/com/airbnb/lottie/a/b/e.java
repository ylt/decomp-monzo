package com.airbnb.lottie.a.b;

import java.util.List;

public class e extends f {
   public e(List var1) {
      super(var1);
   }

   // $FF: synthetic method
   Object a(com.airbnb.lottie.a.a var1, float var2) {
      return this.b(var1, var2);
   }

   Integer b(com.airbnb.lottie.a.a var1, float var2) {
      if(var1.a != null && var1.b != null) {
         return Integer.valueOf(com.airbnb.lottie.d.e.a(((Integer)var1.a).intValue(), ((Integer)var1.b).intValue(), var2));
      } else {
         throw new IllegalStateException("Missing values for keyframe.");
      }
   }
}
