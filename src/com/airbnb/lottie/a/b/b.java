package com.airbnb.lottie.a.b;

import java.util.List;

public class b extends f {
   public b(List var1) {
      super(var1);
   }

   // $FF: synthetic method
   public Object a(com.airbnb.lottie.a.a var1, float var2) {
      return this.b(var1, var2);
   }

   public Integer b(com.airbnb.lottie.a.a var1, float var2) {
      if(var1.a != null && var1.b != null) {
         return Integer.valueOf(com.airbnb.lottie.d.a.a(var2, ((Integer)var1.a).intValue(), ((Integer)var1.b).intValue()));
      } else {
         throw new IllegalStateException("Missing values for keyframe.");
      }
   }
}
