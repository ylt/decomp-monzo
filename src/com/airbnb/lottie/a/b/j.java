package com.airbnb.lottie.a.b;

import android.graphics.PointF;
import java.util.List;

public class j extends f {
   private final PointF b = new PointF();

   public j(List var1) {
      super(var1);
   }

   // $FF: synthetic method
   public Object a(com.airbnb.lottie.a.a var1, float var2) {
      return this.b(var1, var2);
   }

   public PointF b(com.airbnb.lottie.a.a var1, float var2) {
      if(var1.a != null && var1.b != null) {
         PointF var7 = (PointF)var1.a;
         PointF var9 = (PointF)var1.b;
         PointF var8 = this.b;
         float var5 = var7.x;
         float var3 = var9.x;
         float var4 = var7.x;
         float var6 = var7.y;
         var8.set(var5 + (var3 - var4) * var2, (var9.y - var7.y) * var2 + var6);
         return this.b;
      } else {
         throw new IllegalStateException("Missing values for keyframe.");
      }
   }
}
