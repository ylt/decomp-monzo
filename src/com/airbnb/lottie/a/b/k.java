package com.airbnb.lottie.a.b;

import java.util.List;

public class k extends f {
   public k(List var1) {
      super(var1);
   }

   // $FF: synthetic method
   public Object a(com.airbnb.lottie.a.a var1, float var2) {
      return this.b(var1, var2);
   }

   public com.airbnb.lottie.c.k b(com.airbnb.lottie.a.a var1, float var2) {
      if(var1.a != null && var1.b != null) {
         com.airbnb.lottie.c.k var3 = (com.airbnb.lottie.c.k)var1.a;
         com.airbnb.lottie.c.k var4 = (com.airbnb.lottie.c.k)var1.b;
         return new com.airbnb.lottie.c.k(com.airbnb.lottie.d.e.a(var3.a(), var4.a(), var2), com.airbnb.lottie.d.e.a(var3.b(), var4.b(), var2));
      } else {
         throw new IllegalStateException("Missing values for keyframe.");
      }
   }
}
