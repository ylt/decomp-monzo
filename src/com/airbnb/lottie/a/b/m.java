package com.airbnb.lottie.a.b;

import android.graphics.PointF;
import java.util.Collections;

public class m extends a {
   private final PointF b = new PointF();
   private final a c;
   private final a d;

   public m(a var1, a var2) {
      super(Collections.emptyList());
      this.c = var1;
      this.d = var2;
   }

   // $FF: synthetic method
   Object a(com.airbnb.lottie.a.a var1, float var2) {
      return this.b(var1, var2);
   }

   public void a(float var1) {
      this.c.a(var1);
      this.d.a(var1);
      this.b.set(((Float)this.c.b()).floatValue(), ((Float)this.d.b()).floatValue());

      for(int var2 = 0; var2 < this.a.size(); ++var2) {
         ((a.a)this.a.get(var2)).a();
      }

   }

   PointF b(com.airbnb.lottie.a.a var1, float var2) {
      return this.b;
   }

   // $FF: synthetic method
   public Object b() {
      return this.d();
   }

   public PointF d() {
      return this.b((com.airbnb.lottie.a.a)null, 0.0F);
   }
}
