package com.airbnb.lottie.a.b;

import java.util.ArrayList;
import java.util.List;

public class g {
   private final List a;
   private final List b;
   private final List c;

   public g(List var1) {
      this.c = var1;
      this.a = new ArrayList(var1.size());
      this.b = new ArrayList(var1.size());

      for(int var2 = 0; var2 < var1.size(); ++var2) {
         this.a.add(((com.airbnb.lottie.c.b.g)var1.get(var2)).b().a());
         com.airbnb.lottie.c.a.d var3 = ((com.airbnb.lottie.c.b.g)var1.get(var2)).c();
         this.b.add(var3.a());
      }

   }

   public List a() {
      return this.c;
   }

   public List b() {
      return this.a;
   }

   public List c() {
      return this.b;
   }
}
