package com.airbnb.lottie.a.b;

import java.util.List;

public class d extends f {
   private final com.airbnb.lottie.c.b.c b;

   public d(List var1) {
      super(var1);
      com.airbnb.lottie.c.b.c var3 = (com.airbnb.lottie.c.b.c)((com.airbnb.lottie.a.a)var1.get(0)).a;
      int var2;
      if(var3 == null) {
         var2 = 0;
      } else {
         var2 = var3.c();
      }

      this.b = new com.airbnb.lottie.c.b.c(new float[var2], new int[var2]);
   }

   // $FF: synthetic method
   Object a(com.airbnb.lottie.a.a var1, float var2) {
      return this.b(var1, var2);
   }

   com.airbnb.lottie.c.b.c b(com.airbnb.lottie.a.a var1, float var2) {
      this.b.a((com.airbnb.lottie.c.b.c)var1.a, (com.airbnb.lottie.c.b.c)var1.b, var2);
      return this.b;
   }
}
