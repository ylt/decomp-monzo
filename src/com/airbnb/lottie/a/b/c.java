package com.airbnb.lottie.a.b;

import java.util.List;

public class c extends f {
   public c(List var1) {
      super(var1);
   }

   // $FF: synthetic method
   Object a(com.airbnb.lottie.a.a var1, float var2) {
      return this.b(var1, var2);
   }

   Float b(com.airbnb.lottie.a.a var1, float var2) {
      if(var1.a != null && var1.b != null) {
         return Float.valueOf(com.airbnb.lottie.d.e.a(((Float)var1.a).floatValue(), ((Float)var1.b).floatValue(), var2));
      } else {
         throw new IllegalStateException("Missing values for keyframe.");
      }
   }
}
