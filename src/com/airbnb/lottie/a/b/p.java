package com.airbnb.lottie.a.b;

import android.graphics.Matrix;
import android.graphics.PointF;

public class p {
   private final Matrix a = new Matrix();
   private final a b;
   private final a c;
   private final a d;
   private final a e;
   private final a f;
   private final a g;
   private final a h;

   public p(com.airbnb.lottie.c.a.l var1) {
      this.b = var1.a().a();
      this.c = var1.b().a();
      this.d = var1.c().a();
      this.e = var1.d().a();
      this.f = var1.e().a();
      if(var1.f() != null) {
         this.g = var1.f().a();
      } else {
         this.g = null;
      }

      if(var1.g() != null) {
         this.h = var1.g().a();
      } else {
         this.h = null;
      }

   }

   public Matrix a(float var1) {
      PointF var4 = (PointF)this.c.b();
      PointF var5 = (PointF)this.b.b();
      com.airbnb.lottie.c.k var3 = (com.airbnb.lottie.c.k)this.d.b();
      float var2 = ((Float)this.e.b()).floatValue();
      this.a.reset();
      this.a.preTranslate(var4.x * var1, var4.y * var1);
      this.a.preScale((float)Math.pow((double)var3.a(), (double)var1), (float)Math.pow((double)var3.b(), (double)var1));
      this.a.preRotate(var2 * var1, var5.x, var5.y);
      return this.a;
   }

   public a a() {
      return this.f;
   }

   public void a(a.a var1) {
      this.b.a(var1);
      this.c.a(var1);
      this.d.a(var1);
      this.e.a(var1);
      this.f.a(var1);
      if(this.g != null) {
         this.g.a(var1);
      }

      if(this.h != null) {
         this.h.a(var1);
      }

   }

   public void a(com.airbnb.lottie.c.c.a var1) {
      var1.a(this.b);
      var1.a(this.c);
      var1.a(this.d);
      var1.a(this.e);
      var1.a(this.f);
      if(this.g != null) {
         var1.a(this.g);
      }

      if(this.h != null) {
         var1.a(this.h);
      }

   }

   public a b() {
      return this.g;
   }

   public a c() {
      return this.h;
   }

   public Matrix d() {
      this.a.reset();
      PointF var2 = (PointF)this.c.b();
      if(var2.x != 0.0F || var2.y != 0.0F) {
         this.a.preTranslate(var2.x, var2.y);
      }

      float var1 = ((Float)this.e.b()).floatValue();
      if(var1 != 0.0F) {
         this.a.preRotate(var1);
      }

      com.airbnb.lottie.c.k var3 = (com.airbnb.lottie.c.k)this.d.b();
      if(var3.a() != 1.0F || var3.b() != 1.0F) {
         this.a.preScale(var3.a(), var3.b());
      }

      var2 = (PointF)this.b.b();
      if(var2.x != 0.0F || var2.y != 0.0F) {
         this.a.preTranslate(-var2.x, -var2.y);
      }

      return this.a;
   }
}
