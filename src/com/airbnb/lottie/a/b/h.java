package com.airbnb.lottie.a.b;

import android.graphics.Path;
import android.graphics.PointF;
import android.view.animation.Interpolator;
import org.json.JSONArray;
import org.json.JSONObject;

public class h extends com.airbnb.lottie.a.a {
   private Path f;

   private h(com.airbnb.lottie.e var1, PointF var2, PointF var3, Interpolator var4, float var5, Float var6) {
      super(var1, var2, var3, var4, var5, var6);
   }

   // $FF: synthetic method
   h(com.airbnb.lottie.e var1, PointF var2, PointF var3, Interpolator var4, float var5, Float var6, Object var7) {
      this(var1, var2, var3, var4, var5, var6);
   }

   Path e() {
      return this.f;
   }

   public static class a {
      public static h a(JSONObject var0, com.airbnb.lottie.e var1, com.airbnb.lottie.c.a.m.a var2) {
         com.airbnb.lottie.a.a var4 = com.airbnb.lottie.a.a.a.a(var0, var1, var1.n(), var2);
         JSONArray var5 = var0.optJSONArray("ti");
         JSONArray var6 = var0.optJSONArray("to");
         PointF var7;
         PointF var9;
         if(var5 != null && var6 != null) {
            var9 = com.airbnb.lottie.d.b.a(var6, var1.n());
            var7 = com.airbnb.lottie.d.b.a(var5, var1.n());
         } else {
            var7 = null;
            var9 = null;
         }

         h var8 = new h(var1, (PointF)var4.a, (PointF)var4.b, var4.c, var4.d, var4.e);
         boolean var3;
         if(var4.b != null && var4.a != null && ((PointF)var4.a).equals(((PointF)var4.b).x, ((PointF)var4.b).y)) {
            var3 = true;
         } else {
            var3 = false;
         }

         if(var8.b != null && !var3) {
            var8.f = com.airbnb.lottie.d.f.a((PointF)var4.a, (PointF)var4.b, var9, var7);
         }

         return var8;
      }
   }
}
