package com.airbnb.lottie.a.b;

import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.PointF;
import java.util.List;

public class i extends f {
   private final PointF b = new PointF();
   private final float[] c = new float[2];
   private h d;
   private PathMeasure e;

   public i(List var1) {
      super(var1);
   }

   // $FF: synthetic method
   public Object a(com.airbnb.lottie.a.a var1, float var2) {
      return this.b(var1, var2);
   }

   public PointF b(com.airbnb.lottie.a.a var1, float var2) {
      h var3 = (h)var1;
      Path var4 = var3.e();
      PointF var5;
      if(var4 == null) {
         var5 = (PointF)var1.a;
      } else {
         if(this.d != var3) {
            this.e = new PathMeasure(var4, false);
            this.d = var3;
         }

         this.e.getPosTan(this.e.getLength() * var2, this.c, (float[])null);
         this.b.set(this.c[0], this.c[1]);
         var5 = this.b;
      }

      return var5;
   }
}
