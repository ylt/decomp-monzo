package com.airbnb.lottie.a.b;

import java.util.Collections;

public class n extends a {
   private final Object b;

   public n(Object var1) {
      super(Collections.emptyList());
      this.b = var1;
   }

   public Object a(com.airbnb.lottie.a.a var1, float var2) {
      return this.b;
   }

   public void a(float var1) {
   }

   public void a(a.a var1) {
   }

   public Object b() {
      return this.b;
   }
}
