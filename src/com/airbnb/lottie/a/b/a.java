package com.airbnb.lottie.a.b;

import java.util.ArrayList;
import java.util.List;

public abstract class a {
   final List a = new ArrayList();
   private boolean b = false;
   private final List c;
   private float d = 0.0F;
   private com.airbnb.lottie.a.a e;

   a(List var1) {
      this.c = var1;
   }

   private com.airbnb.lottie.a.a d() {
      int var1 = 0;
      if(this.c.isEmpty()) {
         throw new IllegalStateException("There are no keyframes");
      } else {
         com.airbnb.lottie.a.a var2;
         if(this.e != null && this.e.a(this.d)) {
            var2 = this.e;
         } else {
            com.airbnb.lottie.a.a var3 = (com.airbnb.lottie.a.a)this.c.get(0);
            var2 = var3;
            if(this.d < var3.a()) {
               this.e = var3;
               var2 = var3;
            } else {
               while(!var2.a(this.d) && var1 < this.c.size()) {
                  var2 = (com.airbnb.lottie.a.a)this.c.get(var1);
                  ++var1;
               }

               this.e = var2;
            }
         }

         return var2;
      }
   }

   private float e() {
      float var1 = 0.0F;
      if(!this.b) {
         com.airbnb.lottie.a.a var5 = this.d();
         if(!var5.c()) {
            var1 = this.d;
            float var3 = var5.a();
            float var4 = var5.b();
            float var2 = var5.a();
            var1 = var5.c.getInterpolation((var1 - var3) / (var4 - var2));
         }
      }

      return var1;
   }

   private float f() {
      float var1;
      if(this.c.isEmpty()) {
         var1 = 0.0F;
      } else {
         var1 = ((com.airbnb.lottie.a.a)this.c.get(0)).a();
      }

      return var1;
   }

   private float g() {
      float var1;
      if(this.c.isEmpty()) {
         var1 = 1.0F;
      } else {
         var1 = ((com.airbnb.lottie.a.a)this.c.get(this.c.size() - 1)).b();
      }

      return var1;
   }

   abstract Object a(com.airbnb.lottie.a.a var1, float var2);

   public void a() {
      this.b = true;
   }

   public void a(float var1) {
      float var2;
      if(var1 < this.f()) {
         var2 = 0.0F;
      } else {
         var2 = var1;
         if(var1 > this.g()) {
            var2 = 1.0F;
         }
      }

      if(var2 != this.d) {
         this.d = var2;

         for(int var3 = 0; var3 < this.a.size(); ++var3) {
            ((a.a)this.a.get(var3)).a();
         }
      }

   }

   public void a(a.a var1) {
      this.a.add(var1);
   }

   public Object b() {
      return this.a(this.d(), this.e());
   }

   public float c() {
      return this.d;
   }

   public interface a {
      void a();
   }
}
