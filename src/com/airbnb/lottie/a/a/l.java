package com.airbnb.lottie.a.a;

import android.graphics.Path;
import android.graphics.PointF;
import java.util.List;

public class l implements k, com.airbnb.lottie.a.b.a.a {
   private final Path a = new Path();
   private final String b;
   private final com.airbnb.lottie.f c;
   private final com.airbnb.lottie.c.b.i.b d;
   private final com.airbnb.lottie.a.b.a e;
   private final com.airbnb.lottie.a.b.a f;
   private final com.airbnb.lottie.a.b.a g;
   private final com.airbnb.lottie.a.b.a h;
   private final com.airbnb.lottie.a.b.a i;
   private final com.airbnb.lottie.a.b.a j;
   private final com.airbnb.lottie.a.b.a k;
   private q l;
   private boolean m;

   public l(com.airbnb.lottie.f var1, com.airbnb.lottie.c.c.a var2, com.airbnb.lottie.c.b.i var3) {
      this.c = var1;
      this.b = var3.a();
      this.d = var3.b();
      this.e = var3.c().a();
      this.f = var3.d().a();
      this.g = var3.e().a();
      this.i = var3.g().a();
      this.k = var3.i().a();
      if(this.d == com.airbnb.lottie.c.b.i.b.a) {
         this.h = var3.f().a();
         this.j = var3.h().a();
      } else {
         this.h = null;
         this.j = null;
      }

      var2.a(this.e);
      var2.a(this.f);
      var2.a(this.g);
      var2.a(this.i);
      var2.a(this.k);
      if(this.d == com.airbnb.lottie.c.b.i.b.a) {
         var2.a(this.h);
         var2.a(this.j);
      }

      this.e.a(this);
      this.f.a(this);
      this.g.a(this);
      this.i.a(this);
      this.k.a(this);
      if(this.d == com.airbnb.lottie.c.b.i.b.a) {
         this.i.a(this);
         this.k.a(this);
      }

   }

   private void c() {
      this.m = false;
      this.c.invalidateSelf();
   }

   private void d() {
      float var14 = ((Float)this.e.b()).floatValue();
      double var1;
      if(this.g == null) {
         var1 = 0.0D;
      } else {
         var1 = (double)((Float)this.g.b()).floatValue();
      }

      var1 = Math.toRadians(var1 - 90.0D);
      float var24 = (float)(6.283185307179586D / (double)var14);
      float var20 = var24 / 2.0F;
      float var25 = var14 - (float)((int)var14);
      if(var25 != 0.0F) {
         var1 += (double)((1.0F - var25) * var20);
      }

      float var10 = ((Float)this.i.b()).floatValue();
      float var9 = ((Float)this.h.b()).floatValue();
      float var7;
      if(this.j != null) {
         var7 = ((Float)this.j.b()).floatValue() / 100.0F;
      } else {
         var7 = 0.0F;
      }

      float var8 = 0.0F;
      if(this.k != null) {
         var8 = ((Float)this.k.b()).floatValue() / 100.0F;
      }

      float var11;
      float var12;
      float var13;
      if(var25 != 0.0F) {
         var11 = (var10 - var9) * var25 + var9;
         var12 = (float)((double)var11 * Math.cos(var1));
         var13 = (float)((double)var11 * Math.sin(var1));
         this.a.moveTo(var12, var13);
         var1 += (double)(var24 * var25 / 2.0F);
      } else {
         var12 = (float)((double)var10 * Math.cos(var1));
         var13 = (float)((double)var10 * Math.sin(var1));
         this.a.moveTo(var12, var13);
         var1 += (double)var20;
         var11 = 0.0F;
      }

      double var3 = Math.ceil((double)var14) * 2.0D;
      int var29 = 0;
      boolean var28 = false;
      float var16 = var12;

      double var5;
      for(float var15 = var13; (double)var29 < var3; var1 += var5) {
         if(var28) {
            var12 = var10;
         } else {
            var12 = var9;
         }

         float var17;
         if(var11 != 0.0F && (double)var29 == var3 - 2.0D) {
            var17 = var24 * var25 / 2.0F;
         } else {
            var17 = var20;
         }

         var13 = var12;
         if(var11 != 0.0F) {
            var13 = var12;
            if((double)var29 == var3 - 1.0D) {
               var13 = var11;
            }
         }

         float var21 = (float)((double)var13 * Math.cos(var1));
         float var22 = (float)((double)var13 * Math.sin(var1));
         if(var7 == 0.0F && var8 == 0.0F) {
            this.a.lineTo(var21, var22);
         } else {
            var12 = (float)(Math.atan2((double)var15, (double)var16) - 1.5707963267948966D);
            var13 = (float)Math.cos((double)var12);
            float var27 = (float)Math.sin((double)var12);
            var12 = (float)(Math.atan2((double)var22, (double)var21) - 1.5707963267948966D);
            float var23 = (float)Math.cos((double)var12);
            float var26 = (float)Math.sin((double)var12);
            if(var28) {
               var12 = var7;
            } else {
               var12 = var8;
            }

            if(var28) {
               var14 = var8;
            } else {
               var14 = var7;
            }

            float var19;
            if(var28) {
               var19 = var9;
            } else {
               var19 = var10;
            }

            float var18;
            if(var28) {
               var18 = var10;
            } else {
               var18 = var9;
            }

            label93: {
               var13 *= var19 * var12 * 0.47829F;
               var19 = var19 * var12 * 0.47829F * var27;
               var12 = var18 * var14 * 0.47829F * var23;
               var14 = var18 * var14 * 0.47829F * var26;
               if(var25 != 0.0F) {
                  if(var29 == 0) {
                     var18 = var19 * var25;
                     var19 = var13 * var25;
                     var13 = var12;
                     var12 = var19;
                     break label93;
                  }

                  if((double)var29 == var3 - 1.0D) {
                     var14 *= var25;
                     var23 = var12 * var25;
                     var12 = var13;
                     var18 = var19;
                     var13 = var23;
                     break label93;
                  }
               }

               var18 = var13;
               var13 = var12;
               var12 = var18;
               var18 = var19;
            }

            this.a.cubicTo(var16 - var12, var15 - var18, var13 + var21, var14 + var22, var21, var22);
         }

         var5 = (double)var17;
         if(!var28) {
            var28 = true;
         } else {
            var28 = false;
         }

         ++var29;
         var15 = var22;
         var16 = var21;
      }

      PointF var30 = (PointF)this.f.b();
      this.a.offset(var30.x, var30.y);
      this.a.close();
   }

   private void f() {
      int var18 = (int)Math.floor((double)((Float)this.e.b()).floatValue());
      double var1;
      if(this.g == null) {
         var1 = 0.0D;
      } else {
         var1 = (double)((Float)this.g.b()).floatValue();
      }

      double var5 = Math.toRadians(var1 - 90.0D);
      float var13 = (float)(6.283185307179586D / (double)var18);
      float var11 = ((Float)this.k.b()).floatValue() / 100.0F;
      float var12 = ((Float)this.i.b()).floatValue();
      float var8 = (float)((double)var12 * Math.cos(var5));
      float var7 = (float)((double)var12 * Math.sin(var5));
      this.a.moveTo(var8, var7);
      var1 = (double)var13;
      double var3 = Math.ceil((double)var18);
      var18 = 0;

      float var9;
      for(var1 += var5; (double)var18 < var3; var7 = var9) {
         float var10 = (float)((double)var12 * Math.cos(var1));
         var9 = (float)((double)var12 * Math.sin(var1));
         if(var11 != 0.0F) {
            float var15 = (float)(Math.atan2((double)var7, (double)var8) - 1.5707963267948966D);
            float var14 = (float)Math.cos((double)var15);
            var15 = (float)Math.sin((double)var15);
            float var17 = (float)(Math.atan2((double)var9, (double)var10) - 1.5707963267948966D);
            float var16 = (float)Math.cos((double)var17);
            var17 = (float)Math.sin((double)var17);
            this.a.cubicTo(var8 - var14 * var12 * var11 * 0.25F, var7 - var12 * var11 * 0.25F * var15, var10 + var16 * var12 * var11 * 0.25F, var17 * var12 * var11 * 0.25F + var9, var10, var9);
         } else {
            this.a.lineTo(var10, var9);
         }

         var5 = (double)var13;
         ++var18;
         var8 = var10;
         var1 += var5;
      }

      PointF var19 = (PointF)this.f.b();
      this.a.offset(var19.x, var19.y);
      this.a.close();
   }

   public void a() {
      this.c();
   }

   public void a(List var1, List var2) {
      for(int var3 = 0; var3 < var1.size(); ++var3) {
         b var4 = (b)var1.get(var3);
         if(var4 instanceof q && ((q)var4).c() == com.airbnb.lottie.c.b.q.b.a) {
            this.l = (q)var4;
            this.l.a(this);
         }
      }

   }

   public String b() {
      return this.b;
   }

   public Path e() {
      Path var1;
      if(this.m) {
         var1 = this.a;
      } else {
         this.a.reset();
         switch(null.a[this.d.ordinal()]) {
         case 1:
            this.d();
            break;
         case 2:
            this.f();
         }

         this.a.close();
         com.airbnb.lottie.d.f.a(this.a, this.l);
         this.m = true;
         var1 = this.a;
      }

      return var1;
   }
}
