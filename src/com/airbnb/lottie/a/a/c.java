package com.airbnb.lottie.a.a;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Path;
import android.graphics.RectF;
import java.util.ArrayList;
import java.util.List;

public class c implements d, k, com.airbnb.lottie.a.b.a.a {
   private final Matrix a;
   private final Path b;
   private final RectF c;
   private final String d;
   private final List e;
   private final com.airbnb.lottie.f f;
   private List g;
   private com.airbnb.lottie.a.b.p h;

   public c(com.airbnb.lottie.f var1, com.airbnb.lottie.c.c.a var2, com.airbnb.lottie.c.b.n var3) {
      this(var1, var2, var3.a(), a(var1, var2, var3.b()), a(var3.b()));
   }

   c(com.airbnb.lottie.f var1, com.airbnb.lottie.c.c.a var2, String var3, List var4, com.airbnb.lottie.c.a.l var5) {
      this.a = new Matrix();
      this.b = new Path();
      this.c = new RectF();
      this.d = var3;
      this.f = var1;
      this.e = var4;
      if(var5 != null) {
         this.h = var5.h();
         this.h.a(var2);
         this.h.a((com.airbnb.lottie.a.b.a.a)this);
      }

      ArrayList var8 = new ArrayList();

      int var6;
      for(var6 = var4.size() - 1; var6 >= 0; --var6) {
         b var7 = (b)var4.get(var6);
         if(var7 instanceof i) {
            var8.add((i)var7);
         }
      }

      for(var6 = var8.size() - 1; var6 >= 0; --var6) {
         ((i)var8.get(var6)).a(var4.listIterator(var4.size()));
      }

   }

   static com.airbnb.lottie.c.a.l a(List var0) {
      int var1 = 0;

      com.airbnb.lottie.c.a.l var3;
      while(true) {
         if(var1 >= var0.size()) {
            var3 = null;
            break;
         }

         com.airbnb.lottie.c.b.b var2 = (com.airbnb.lottie.c.b.b)var0.get(var1);
         if(var2 instanceof com.airbnb.lottie.c.a.l) {
            var3 = (com.airbnb.lottie.c.a.l)var2;
            break;
         }

         ++var1;
      }

      return var3;
   }

   private static List a(com.airbnb.lottie.f var0, com.airbnb.lottie.c.c.a var1, List var2) {
      ArrayList var4 = new ArrayList(var2.size());

      for(int var3 = 0; var3 < var2.size(); ++var3) {
         b var5 = ((com.airbnb.lottie.c.b.b)var2.get(var3)).a(var0, var1);
         if(var5 != null) {
            var4.add(var5);
         }
      }

      return var4;
   }

   public void a() {
      this.f.invalidateSelf();
   }

   public void a(Canvas var1, Matrix var2, int var3) {
      this.a.set(var2);
      int var4 = var3;
      if(this.h != null) {
         this.a.preConcat(this.h.d());
         var4 = (int)((float)((Integer)this.h.a().b()).intValue() / 100.0F * (float)var3 / 255.0F * 255.0F);
      }

      for(var3 = this.e.size() - 1; var3 >= 0; --var3) {
         Object var5 = this.e.get(var3);
         if(var5 instanceof d) {
            ((d)var5).a(var1, this.a, var4);
         }
      }

   }

   public void a(RectF var1, Matrix var2) {
      this.a.set(var2);
      if(this.h != null) {
         this.a.preConcat(this.h.d());
      }

      this.c.set(0.0F, 0.0F, 0.0F, 0.0F);

      for(int var3 = this.e.size() - 1; var3 >= 0; --var3) {
         b var4 = (b)this.e.get(var3);
         if(var4 instanceof d) {
            ((d)var4).a(this.c, this.a);
            if(var1.isEmpty()) {
               var1.set(this.c);
            } else {
               var1.set(Math.min(var1.left, this.c.left), Math.min(var1.top, this.c.top), Math.max(var1.right, this.c.right), Math.max(var1.bottom, this.c.bottom));
            }
         }
      }

   }

   public void a(String var1, String var2, ColorFilter var3) {
      for(int var4 = 0; var4 < this.e.size(); ++var4) {
         b var6 = (b)this.e.get(var4);
         if(var6 instanceof d) {
            d var5 = (d)var6;
            if(var2 != null && !var2.equals(var6.b())) {
               var5.a(var1, var2, var3);
            } else {
               var5.a(var1, (String)null, var3);
            }
         }
      }

   }

   public void a(List var1, List var2) {
      ArrayList var5 = new ArrayList(var1.size() + this.e.size());
      var5.addAll(var1);

      for(int var3 = this.e.size() - 1; var3 >= 0; --var3) {
         b var4 = (b)this.e.get(var3);
         var4.a(var5, this.e.subList(0, var3));
         var5.add(var4);
      }

   }

   public String b() {
      return this.d;
   }

   List c() {
      if(this.g == null) {
         this.g = new ArrayList();

         for(int var1 = 0; var1 < this.e.size(); ++var1) {
            b var2 = (b)this.e.get(var1);
            if(var2 instanceof k) {
               this.g.add((k)var2);
            }
         }
      }

      return this.g;
   }

   Matrix d() {
      Matrix var1;
      if(this.h != null) {
         var1 = this.h.d();
      } else {
         this.a.reset();
         var1 = this.a;
      }

      return var1;
   }

   public Path e() {
      this.a.reset();
      if(this.h != null) {
         this.a.set(this.h.d());
      }

      this.b.reset();

      for(int var1 = this.e.size() - 1; var1 >= 0; --var1) {
         b var2 = (b)this.e.get(var1);
         if(var2 instanceof k) {
            this.b.addPath(((k)var2).e(), this.a);
         }
      }

      return this.b;
   }
}
