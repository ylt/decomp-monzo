package com.airbnb.lottie.a.a;

import android.graphics.Path;
import android.graphics.Path.FillType;
import java.util.List;

public class o implements k, com.airbnb.lottie.a.b.a.a {
   private final Path a = new Path();
   private final String b;
   private final com.airbnb.lottie.f c;
   private final com.airbnb.lottie.a.b.a d;
   private boolean e;
   private q f;

   public o(com.airbnb.lottie.f var1, com.airbnb.lottie.c.c.a var2, com.airbnb.lottie.c.b.o var3) {
      this.b = var3.a();
      this.c = var1;
      this.d = var3.b().a();
      var2.a(this.d);
      this.d.a(this);
   }

   private void c() {
      this.e = false;
      this.c.invalidateSelf();
   }

   public void a() {
      this.c();
   }

   public void a(List var1, List var2) {
      for(int var3 = 0; var3 < var1.size(); ++var3) {
         b var4 = (b)var1.get(var3);
         if(var4 instanceof q && ((q)var4).c() == com.airbnb.lottie.c.b.q.b.a) {
            this.f = (q)var4;
            this.f.a(this);
         }
      }

   }

   public String b() {
      return this.b;
   }

   public Path e() {
      Path var1;
      if(this.e) {
         var1 = this.a;
      } else {
         this.a.reset();
         this.a.set((Path)this.d.b());
         this.a.setFillType(FillType.EVEN_ODD);
         com.airbnb.lottie.d.f.a(this.a, this.f);
         this.e = true;
         var1 = this.a;
      }

      return var1;
   }
}
