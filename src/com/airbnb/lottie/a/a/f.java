package com.airbnb.lottie.a.a;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import java.util.ArrayList;
import java.util.List;

public class f implements d, com.airbnb.lottie.a.b.a.a {
   private final Path a = new Path();
   private final Paint b = new Paint(1);
   private final String c;
   private final List d = new ArrayList();
   private final com.airbnb.lottie.a.b.a e;
   private final com.airbnb.lottie.a.b.a f;
   private final com.airbnb.lottie.f g;

   public f(com.airbnb.lottie.f var1, com.airbnb.lottie.c.c.a var2, com.airbnb.lottie.c.b.m var3) {
      this.c = var3.a();
      this.g = var1;
      if(var3.b() != null && var3.c() != null) {
         this.a.setFillType(var3.d());
         this.e = var3.b().a();
         this.e.a(this);
         var2.a(this.e);
         this.f = var3.c().a();
         this.f.a(this);
         var2.a(this.f);
      } else {
         this.e = null;
         this.f = null;
      }

   }

   public void a() {
      this.g.invalidateSelf();
   }

   public void a(Canvas var1, Matrix var2, int var3) {
      com.airbnb.lottie.d.a("FillContent#draw");
      this.b.setColor(((Integer)this.e.b()).intValue());
      float var4 = (float)var3 / 255.0F;
      var3 = (int)((float)((Integer)this.f.b()).intValue() * var4 / 100.0F * 255.0F);
      this.b.setAlpha(var3);
      this.a.reset();

      for(var3 = 0; var3 < this.d.size(); ++var3) {
         this.a.addPath(((k)this.d.get(var3)).e(), var2);
      }

      var1.drawPath(this.a, this.b);
      com.airbnb.lottie.d.b("FillContent#draw");
   }

   public void a(RectF var1, Matrix var2) {
      this.a.reset();

      for(int var3 = 0; var3 < this.d.size(); ++var3) {
         this.a.addPath(((k)this.d.get(var3)).e(), var2);
      }

      this.a.computeBounds(var1, false);
      var1.set(var1.left - 1.0F, var1.top - 1.0F, var1.right + 1.0F, var1.bottom + 1.0F);
   }

   public void a(String var1, String var2, ColorFilter var3) {
      this.b.setColorFilter(var3);
   }

   public void a(List var1, List var2) {
      for(int var3 = 0; var3 < var2.size(); ++var3) {
         b var4 = (b)var2.get(var3);
         if(var4 instanceof k) {
            this.d.add((k)var4);
         }
      }

   }

   public String b() {
      return this.c;
   }
}
