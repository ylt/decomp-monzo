package com.airbnb.lottie.a.a;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.RectF;

public interface d extends b {
   void a(Canvas var1, Matrix var2, int var3);

   void a(RectF var1, Matrix var2);

   void a(String var1, String var2, ColorFilter var3);
}
