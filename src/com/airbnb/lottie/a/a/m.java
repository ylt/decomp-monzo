package com.airbnb.lottie.a.a;

import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.RectF;
import java.util.List;

public class m implements k, com.airbnb.lottie.a.b.a.a {
   private final Path a = new Path();
   private final RectF b = new RectF();
   private final String c;
   private final com.airbnb.lottie.f d;
   private final com.airbnb.lottie.a.b.a e;
   private final com.airbnb.lottie.a.b.a f;
   private final com.airbnb.lottie.a.b.a g;
   private q h;
   private boolean i;

   public m(com.airbnb.lottie.f var1, com.airbnb.lottie.c.c.a var2, com.airbnb.lottie.c.b.j var3) {
      this.c = var3.a();
      this.d = var1;
      this.e = var3.d().a();
      this.f = var3.c().a();
      this.g = var3.b().a();
      var2.a(this.e);
      var2.a(this.f);
      var2.a(this.g);
      this.e.a(this);
      this.f.a(this);
      this.g.a(this);
   }

   private void c() {
      this.i = false;
      this.d.invalidateSelf();
   }

   public void a() {
      this.c();
   }

   public void a(List var1, List var2) {
      for(int var3 = 0; var3 < var1.size(); ++var3) {
         b var4 = (b)var1.get(var3);
         if(var4 instanceof q && ((q)var4).c() == com.airbnb.lottie.c.b.q.b.a) {
            this.h = (q)var4;
            this.h.a(this);
         }
      }

   }

   public String b() {
      return this.c;
   }

   public Path e() {
      Path var5;
      if(this.i) {
         var5 = this.a;
      } else {
         this.a.reset();
         PointF var6 = (PointF)this.f.b();
         float var3 = var6.x / 2.0F;
         float var4 = var6.y / 2.0F;
         float var1;
         if(this.g == null) {
            var1 = 0.0F;
         } else {
            var1 = ((Float)this.g.b()).floatValue();
         }

         float var2 = Math.min(var3, var4);
         if(var1 > var2) {
            var1 = var2;
         }

         var6 = (PointF)this.e.b();
         this.a.moveTo(var6.x + var3, var6.y - var4 + var1);
         this.a.lineTo(var6.x + var3, var6.y + var4 - var1);
         if(var1 > 0.0F) {
            this.b.set(var6.x + var3 - 2.0F * var1, var6.y + var4 - 2.0F * var1, var6.x + var3, var6.y + var4);
            this.a.arcTo(this.b, 0.0F, 90.0F, false);
         }

         this.a.lineTo(var6.x - var3 + var1, var6.y + var4);
         if(var1 > 0.0F) {
            this.b.set(var6.x - var3, var6.y + var4 - 2.0F * var1, var6.x - var3 + 2.0F * var1, var6.y + var4);
            this.a.arcTo(this.b, 90.0F, 90.0F, false);
         }

         this.a.lineTo(var6.x - var3, var6.y - var4 + var1);
         if(var1 > 0.0F) {
            this.b.set(var6.x - var3, var6.y - var4, var6.x - var3 + 2.0F * var1, var6.y - var4 + 2.0F * var1);
            this.a.arcTo(this.b, 180.0F, 90.0F, false);
         }

         this.a.lineTo(var6.x + var3 - var1, var6.y - var4);
         if(var1 > 0.0F) {
            this.b.set(var6.x + var3 - 2.0F * var1, var6.y - var4, var3 + var6.x, var6.y - var4 + var1 * 2.0F);
            this.a.arcTo(this.b, 270.0F, 90.0F, false);
         }

         this.a.close();
         com.airbnb.lottie.d.f.a(this.a, this.h);
         this.i = true;
         var5 = this.a;
      }

      return var5;
   }
}
