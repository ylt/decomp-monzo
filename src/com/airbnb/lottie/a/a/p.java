package com.airbnb.lottie.a.a;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Matrix;

public class p extends a {
   private final String b;
   private final com.airbnb.lottie.a.b.a c;

   public p(com.airbnb.lottie.f var1, com.airbnb.lottie.c.c.a var2, com.airbnb.lottie.c.b.p var3) {
      super(var1, var2, var3.g().a(), var3.h().a(), var3.c(), var3.d(), var3.e(), var3.f());
      this.b = var3.a();
      this.c = var3.b().a();
      this.c.a(this);
      var2.a(this.c);
   }

   public void a(Canvas var1, Matrix var2, int var3) {
      this.a.setColor(((Integer)this.c.b()).intValue());
      super.a(var1, var2, var3);
   }

   public void a(String var1, String var2, ColorFilter var3) {
      this.a.setColorFilter(var3);
   }

   public String b() {
      return this.b;
   }
}
