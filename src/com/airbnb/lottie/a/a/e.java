package com.airbnb.lottie.a.a;

import android.graphics.Path;
import android.graphics.PointF;
import java.util.List;

public class e implements k, com.airbnb.lottie.a.b.a.a {
   private final Path a = new Path();
   private final String b;
   private final com.airbnb.lottie.f c;
   private final com.airbnb.lottie.a.b.a d;
   private final com.airbnb.lottie.a.b.a e;
   private q f;
   private boolean g;

   public e(com.airbnb.lottie.f var1, com.airbnb.lottie.c.c.a var2, com.airbnb.lottie.c.b.a var3) {
      this.b = var3.a();
      this.c = var1;
      this.d = var3.c().a();
      this.e = var3.b().a();
      var2.a(this.d);
      var2.a(this.e);
      this.d.a(this);
      this.e.a(this);
   }

   private void c() {
      this.g = false;
      this.c.invalidateSelf();
   }

   public void a() {
      this.c();
   }

   public void a(List var1, List var2) {
      for(int var3 = 0; var3 < var1.size(); ++var3) {
         b var4 = (b)var1.get(var3);
         if(var4 instanceof q && ((q)var4).c() == com.airbnb.lottie.c.b.q.b.a) {
            this.f = (q)var4;
            this.f.a(this);
         }
      }

   }

   public String b() {
      return this.b;
   }

   public Path e() {
      Path var5;
      if(this.g) {
         var5 = this.a;
      } else {
         this.a.reset();
         PointF var6 = (PointF)this.d.b();
         float var2 = var6.x / 2.0F;
         float var4 = var6.y / 2.0F;
         float var1 = var2 * 0.55228F;
         float var3 = var4 * 0.55228F;
         this.a.reset();
         this.a.moveTo(0.0F, -var4);
         this.a.cubicTo(0.0F + var1, -var4, var2, 0.0F - var3, var2, 0.0F);
         this.a.cubicTo(var2, 0.0F + var3, 0.0F + var1, var4, 0.0F, var4);
         this.a.cubicTo(0.0F - var1, var4, -var2, 0.0F + var3, -var2, 0.0F);
         this.a.cubicTo(-var2, 0.0F - var3, 0.0F - var1, -var4, 0.0F, -var4);
         var6 = (PointF)this.e.b();
         this.a.offset(var6.x, var6.y);
         this.a.close();
         com.airbnb.lottie.d.f.a(this.a, this.f);
         this.g = true;
         var5 = this.a;
      }

      return var5;
   }
}
