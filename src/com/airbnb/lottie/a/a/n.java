package com.airbnb.lottie.a.a;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Path;
import android.graphics.RectF;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;

public class n implements d, i, k, com.airbnb.lottie.a.b.a.a {
   private final Matrix a = new Matrix();
   private final Path b = new Path();
   private final com.airbnb.lottie.f c;
   private final com.airbnb.lottie.c.c.a d;
   private final String e;
   private final com.airbnb.lottie.a.b.a f;
   private final com.airbnb.lottie.a.b.a g;
   private final com.airbnb.lottie.a.b.p h;
   private c i;

   public n(com.airbnb.lottie.f var1, com.airbnb.lottie.c.c.a var2, com.airbnb.lottie.c.b.k var3) {
      this.c = var1;
      this.d = var2;
      this.e = var3.a();
      this.f = var3.b().a();
      var2.a(this.f);
      this.f.a(this);
      this.g = var3.c().a();
      var2.a(this.g);
      this.g.a(this);
      this.h = var3.d().h();
      this.h.a(var2);
      this.h.a((com.airbnb.lottie.a.b.a.a)this);
   }

   public void a() {
      this.c.invalidateSelf();
   }

   public void a(Canvas var1, Matrix var2, int var3) {
      float var7 = ((Float)this.f.b()).floatValue();
      float var6 = ((Float)this.g.b()).floatValue();
      float var9 = ((Float)this.h.b().b()).floatValue() / 100.0F;
      float var4 = ((Float)this.h.c().b()).floatValue() / 100.0F;

      for(int var10 = (int)var7 - 1; var10 >= 0; --var10) {
         this.a.set(var2);
         this.a.preConcat(this.h.a((float)var10 + var6));
         float var8 = (float)var3;
         float var5 = com.airbnb.lottie.d.e.a(var9, var4, (float)var10 / var7);
         this.i.a(var1, this.a, (int)(var8 * var5));
      }

   }

   public void a(RectF var1, Matrix var2) {
      this.i.a(var1, var2);
   }

   public void a(String var1, String var2, ColorFilter var3) {
      this.i.a(var1, var2, var3);
   }

   public void a(List var1, List var2) {
      this.i.a(var1, var2);
   }

   public void a(ListIterator var1) {
      if(this.i == null) {
         while(true) {
            if(!var1.hasPrevious() || var1.previous() == this) {
               ArrayList var2 = new ArrayList();

               while(var1.hasPrevious()) {
                  var2.add(var1.previous());
                  var1.remove();
               }

               Collections.reverse(var2);
               this.i = new c(this.c, this.d, "Repeater", var2, (com.airbnb.lottie.c.a.l)null);
               break;
            }
         }
      }

   }

   public String b() {
      return this.e;
   }

   public Path e() {
      Path var4 = this.i.e();
      this.b.reset();
      float var1 = ((Float)this.f.b()).floatValue();
      float var2 = ((Float)this.g.b()).floatValue();

      for(int var3 = (int)var1 - 1; var3 >= 0; --var3) {
         this.a.set(this.h.a((float)var3 + var2));
         this.b.addPath(var4, this.a);
      }

      return this.b;
   }
}
