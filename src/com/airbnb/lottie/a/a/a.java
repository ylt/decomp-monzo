package com.airbnb.lottie.a.a;

import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.RectF;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Join;
import android.graphics.Paint.Style;
import java.util.ArrayList;
import java.util.List;

public abstract class a implements d, com.airbnb.lottie.a.b.a.a {
   final Paint a;
   private final PathMeasure b;
   private final Path c;
   private final Path d;
   private final RectF e;
   private final com.airbnb.lottie.f f;
   private final List g;
   private final float[] h;
   private final com.airbnb.lottie.a.b.a i;
   private final com.airbnb.lottie.a.b.a j;
   private final List k;
   private final com.airbnb.lottie.a.b.a l;

   a(com.airbnb.lottie.f var1, com.airbnb.lottie.c.c.a var2, Cap var3, Join var4, com.airbnb.lottie.c.a.d var5, com.airbnb.lottie.c.a.b var6, List var7, com.airbnb.lottie.c.a.b var8) {
      byte var10 = 0;
      super();
      this.b = new PathMeasure();
      this.c = new Path();
      this.d = new Path();
      this.e = new RectF();
      this.g = new ArrayList();
      this.a = new Paint(1);
      this.f = var1;
      this.a.setStyle(Style.STROKE);
      this.a.setStrokeCap(var3);
      this.a.setStrokeJoin(var4);
      this.j = var5.a();
      this.i = var6.a();
      if(var8 == null) {
         this.l = null;
      } else {
         this.l = var8.a();
      }

      this.k = new ArrayList(var7.size());
      this.h = new float[var7.size()];

      int var9;
      for(var9 = 0; var9 < var7.size(); ++var9) {
         this.k.add(((com.airbnb.lottie.c.a.b)var7.get(var9)).a());
      }

      var2.a(this.j);
      var2.a(this.i);

      for(var9 = 0; var9 < this.k.size(); ++var9) {
         var2.a((com.airbnb.lottie.a.b.a)this.k.get(var9));
      }

      if(this.l != null) {
         var2.a(this.l);
      }

      this.j.a(this);
      this.i.a(this);

      for(var9 = var10; var9 < var7.size(); ++var9) {
         ((com.airbnb.lottie.a.b.a)this.k.get(var9)).a(this);
      }

      if(this.l != null) {
         this.l.a(this);
      }

   }

   private void a(Canvas var1, a.a var2, Matrix var3) {
      com.airbnb.lottie.d.a("StrokeContent#applyTrimPath");
      if(var2.b == null) {
         com.airbnb.lottie.d.b("StrokeContent#applyTrimPath");
      } else {
         this.c.reset();

         int var11;
         for(var11 = var2.a.size() - 1; var11 >= 0; --var11) {
            this.c.addPath(((k)var2.a.get(var11)).e(), var3);
         }

         this.b.setPath(this.c, false);

         float var4;
         for(var4 = this.b.getLength(); this.b.nextContour(); var4 += this.b.getLength()) {
            ;
         }

         float var5 = ((Float)var2.b.f().b()).floatValue() * var4 / 360.0F;
         float var8 = ((Float)var2.b.d().b()).floatValue() * var4 / 100.0F + var5;
         float var9 = ((Float)var2.b.e().b()).floatValue() * var4 / 100.0F + var5;
         var11 = var2.a.size() - 1;

         float var10;
         for(var5 = 0.0F; var11 >= 0; var5 += var10) {
            this.d.set(((k)var2.a.get(var11)).e());
            this.d.transform(var3);
            this.b.setPath(this.d, false);
            var10 = this.b.getLength();
            float var6;
            float var7;
            if(var9 > var4 && var9 - var4 < var5 + var10 && var5 < var9 - var4) {
               if(var8 > var4) {
                  var6 = (var8 - var4) / var10;
               } else {
                  var6 = 0.0F;
               }

               var7 = Math.min((var9 - var4) / var10, 1.0F);
               com.airbnb.lottie.d.f.a(this.d, var6, var7, 0.0F);
               var1.drawPath(this.d, this.a);
            } else if(var5 + var10 >= var8 && var5 <= var9) {
               if(var5 + var10 <= var9 && var8 < var5) {
                  var1.drawPath(this.d, this.a);
               } else {
                  if(var8 < var5) {
                     var6 = 0.0F;
                  } else {
                     var6 = (var8 - var5) / var10;
                  }

                  if(var9 > var5 + var10) {
                     var7 = 1.0F;
                  } else {
                     var7 = (var9 - var5) / var10;
                  }

                  com.airbnb.lottie.d.f.a(this.d, var6, var7, 0.0F);
                  var1.drawPath(this.d, this.a);
               }
            }

            --var11;
         }

         com.airbnb.lottie.d.b("StrokeContent#applyTrimPath");
      }

   }

   private void a(Matrix var1) {
      com.airbnb.lottie.d.a("StrokeContent#applyDashPattern");
      if(this.k.isEmpty()) {
         com.airbnb.lottie.d.b("StrokeContent#applyDashPattern");
      } else {
         float var2 = com.airbnb.lottie.d.f.a(var1);

         for(int var3 = 0; var3 < this.k.size(); ++var3) {
            this.h[var3] = ((Float)((com.airbnb.lottie.a.b.a)this.k.get(var3)).b()).floatValue();
            if(var3 % 2 == 0) {
               if(this.h[var3] < 1.0F) {
                  this.h[var3] = 1.0F;
               }
            } else if(this.h[var3] < 0.1F) {
               this.h[var3] = 0.1F;
            }

            float[] var4 = this.h;
            var4[var3] *= var2;
         }

         if(this.l == null) {
            var2 = 0.0F;
         } else {
            var2 = ((Float)this.l.b()).floatValue();
         }

         this.a.setPathEffect(new DashPathEffect(this.h, var2));
         com.airbnb.lottie.d.b("StrokeContent#applyDashPattern");
      }

   }

   public void a() {
      this.f.invalidateSelf();
   }

   public void a(Canvas var1, Matrix var2, int var3) {
      com.airbnb.lottie.d.a("StrokeContent#draw");
      float var4 = (float)var3 / 255.0F;
      var3 = (int)((float)((Integer)this.j.b()).intValue() * var4 / 100.0F * 255.0F);
      this.a.setAlpha(var3);
      this.a.setStrokeWidth(((Float)this.i.b()).floatValue() * com.airbnb.lottie.d.f.a(var2));
      if(this.a.getStrokeWidth() <= 0.0F) {
         com.airbnb.lottie.d.b("StrokeContent#draw");
      } else {
         this.a(var2);

         for(var3 = 0; var3 < this.g.size(); ++var3) {
            a.a var6 = (a.a)this.g.get(var3);
            if(var6.b != null) {
               this.a(var1, var6, var2);
            } else {
               com.airbnb.lottie.d.a("StrokeContent#buildPath");
               this.c.reset();

               for(int var5 = var6.a.size() - 1; var5 >= 0; --var5) {
                  this.c.addPath(((k)var6.a.get(var5)).e(), var2);
               }

               com.airbnb.lottie.d.b("StrokeContent#buildPath");
               com.airbnb.lottie.d.a("StrokeContent#drawPath");
               var1.drawPath(this.c, this.a);
               com.airbnb.lottie.d.b("StrokeContent#drawPath");
            }
         }

         com.airbnb.lottie.d.b("StrokeContent#draw");
      }

   }

   public void a(RectF var1, Matrix var2) {
      com.airbnb.lottie.d.a("StrokeContent#getBounds");
      this.c.reset();

      for(int var11 = 0; var11 < this.g.size(); ++var11) {
         a.a var13 = (a.a)this.g.get(var11);

         for(int var12 = 0; var12 < var13.a.size(); ++var12) {
            this.c.addPath(((k)var13.a.get(var12)).e(), var2);
         }
      }

      this.c.computeBounds(this.e, false);
      float var5 = ((Float)this.i.b()).floatValue();
      RectF var14 = this.e;
      float var3 = this.e.left;
      float var4 = var5 / 2.0F;
      float var6 = this.e.top;
      float var8 = var5 / 2.0F;
      float var10 = this.e.right;
      float var7 = var5 / 2.0F;
      float var9 = this.e.bottom;
      var14.set(var3 - var4, var6 - var8, var10 + var7, var5 / 2.0F + var9);
      var1.set(this.e);
      var1.set(var1.left - 1.0F, var1.top - 1.0F, var1.right + 1.0F, var1.bottom + 1.0F);
      com.airbnb.lottie.d.b("StrokeContent#getBounds");
   }

   public void a(List var1, List var2) {
      int var3 = var1.size() - 1;

      q var4;
      b var5;
      for(var4 = null; var3 >= 0; --var3) {
         var5 = (b)var1.get(var3);
         if(var5 instanceof q && ((q)var5).c() == com.airbnb.lottie.c.b.q.b.b) {
            var4 = (q)var5;
         }
      }

      if(var4 != null) {
         var4.a(this);
      }

      var3 = var2.size() - 1;

      a.a var6;
      for(var6 = null; var3 >= 0; --var3) {
         var5 = (b)var2.get(var3);
         if(var5 instanceof q && ((q)var5).c() == com.airbnb.lottie.c.b.q.b.b) {
            if(var6 != null) {
               this.g.add(var6);
            }

            var6 = new a.a((q)var5);
            ((q)var5).a(this);
         } else if(var5 instanceof k) {
            if(var6 == null) {
               var6 = new a.a(var4);
            }

            var6.a.add((k)var5);
         }
      }

      if(var6 != null) {
         this.g.add(var6);
      }

   }

   private static final class a {
      private final List a;
      private final q b;

      private a(q var1) {
         this.a = new ArrayList();
         this.b = var1;
      }

      // $FF: synthetic method
      a(q var1, Object var2) {
         this(var1);
      }
   }
}
