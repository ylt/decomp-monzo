package com.airbnb.lottie.a.a;

import android.annotation.TargetApi;
import android.graphics.Path;
import android.graphics.Path.Op;
import android.os.Build.VERSION;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

@TargetApi(19)
public class j implements i, k {
   private final Path a = new Path();
   private final Path b = new Path();
   private final Path c = new Path();
   private final String d;
   private final List e = new ArrayList();
   private final com.airbnb.lottie.c.b.h f;

   public j(com.airbnb.lottie.c.b.h var1) {
      if(VERSION.SDK_INT < 19) {
         throw new IllegalStateException("Merge paths are not supported pre-KitKat.");
      } else {
         this.d = var1.a();
         this.f = var1;
      }
   }

   private void a() {
      for(int var1 = 0; var1 < this.e.size(); ++var1) {
         this.c.addPath(((k)this.e.get(var1)).e());
      }

   }

   @TargetApi(19)
   private void a(Op var1) {
      byte var4 = 0;
      this.b.reset();
      this.a.reset();

      int var2;
      List var7;
      for(var2 = this.e.size() - 1; var2 >= 1; --var2) {
         k var6 = (k)this.e.get(var2);
         if(var6 instanceof c) {
            var7 = ((c)var6).c();

            for(int var3 = var7.size() - 1; var3 >= 0; --var3) {
               Path var5 = ((k)var7.get(var3)).e();
               var5.transform(((c)var6).d());
               this.b.addPath(var5);
            }
         } else {
            this.b.addPath(var6.e());
         }
      }

      k var8 = (k)this.e.get(0);
      if(var8 instanceof c) {
         var7 = ((c)var8).c();

         for(var2 = var4; var2 < var7.size(); ++var2) {
            Path var9 = ((k)var7.get(var2)).e();
            var9.transform(((c)var8).d());
            this.a.addPath(var9);
         }
      } else {
         this.a.set(var8.e());
      }

      this.c.op(this.a, this.b, var1);
   }

   public void a(List var1, List var2) {
      for(int var3 = 0; var3 < this.e.size(); ++var3) {
         ((k)this.e.get(var3)).a(var1, var2);
      }

   }

   public void a(ListIterator var1) {
      while(var1.hasPrevious() && var1.previous() != this) {
         ;
      }

      while(var1.hasPrevious()) {
         b var2 = (b)var1.previous();
         if(var2 instanceof k) {
            this.e.add((k)var2);
            var1.remove();
         }
      }

   }

   public String b() {
      return this.d;
   }

   public Path e() {
      this.c.reset();
      switch(null.a[this.f.b().ordinal()]) {
      case 1:
         this.a();
         break;
      case 2:
         this.a(Op.UNION);
         break;
      case 3:
         this.a(Op.REVERSE_DIFFERENCE);
         break;
      case 4:
         this.a(Op.INTERSECT);
         break;
      case 5:
         this.a(Op.XOR);
      }

      return this.c;
   }
}
