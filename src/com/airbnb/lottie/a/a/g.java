package com.airbnb.lottie.a.a;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.RadialGradient;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.Shader.TileMode;
import java.util.ArrayList;
import java.util.List;

public class g implements d, com.airbnb.lottie.a.b.a.a {
   private final String a;
   private final android.support.v4.g.f b = new android.support.v4.g.f();
   private final android.support.v4.g.f c = new android.support.v4.g.f();
   private final Matrix d = new Matrix();
   private final Path e = new Path();
   private final Paint f = new Paint(1);
   private final RectF g = new RectF();
   private final List h = new ArrayList();
   private final com.airbnb.lottie.c.b.f i;
   private final com.airbnb.lottie.a.b.a j;
   private final com.airbnb.lottie.a.b.a k;
   private final com.airbnb.lottie.a.b.a l;
   private final com.airbnb.lottie.a.b.a m;
   private final com.airbnb.lottie.f n;
   private final int o;

   public g(com.airbnb.lottie.f var1, com.airbnb.lottie.c.c.a var2, com.airbnb.lottie.c.b.d var3) {
      this.a = var3.a();
      this.n = var1;
      this.i = var3.b();
      this.e.setFillType(var3.c());
      this.o = (int)(var1.m().c() / 32L);
      this.j = var3.d().a();
      this.j.a(this);
      var2.a(this.j);
      this.k = var3.e().a();
      this.k.a(this);
      var2.a(this.k);
      this.l = var3.f().a();
      this.l.a(this);
      var2.a(this.l);
      this.m = var3.g().a();
      this.m.a(this);
      var2.a(this.m);
   }

   private LinearGradient c() {
      int var1 = this.e();
      LinearGradient var2 = (LinearGradient)this.b.a((long)var1);
      if(var2 == null) {
         PointF var4 = (PointF)this.l.b();
         PointF var3 = (PointF)this.m.b();
         com.airbnb.lottie.c.b.c var5 = (com.airbnb.lottie.c.b.c)this.j.b();
         int[] var7 = var5.b();
         float[] var6 = var5.a();
         var2 = new LinearGradient(var4.x, var4.y, var3.x, var3.y, var7, var6, TileMode.CLAMP);
         this.b.b((long)var1, var2);
      }

      return var2;
   }

   private RadialGradient d() {
      int var5 = this.e();
      RadialGradient var6 = (RadialGradient)this.c.a((long)var5);
      if(var6 == null) {
         PointF var10 = (PointF)this.l.b();
         PointF var7 = (PointF)this.m.b();
         com.airbnb.lottie.c.b.c var9 = (com.airbnb.lottie.c.b.c)this.j.b();
         int[] var8 = var9.b();
         float[] var11 = var9.a();
         float var3 = var10.x;
         float var2 = var10.y;
         float var1 = var7.x;
         float var4 = var7.y;
         var6 = new RadialGradient(var3, var2, (float)Math.hypot((double)(var1 - var3), (double)(var4 - var2)), var8, var11, TileMode.CLAMP);
         this.c.b((long)var5, var6);
      }

      return var6;
   }

   private int e() {
      int var2 = Math.round(this.l.c() * (float)this.o);
      int var4 = Math.round(this.m.c() * (float)this.o);
      int var3 = Math.round(this.j.c() * (float)this.o);
      int var1 = 17;
      if(var2 != 0) {
         var1 = var2 * 527;
      }

      var2 = var1;
      if(var4 != 0) {
         var2 = var1 * 31 * var4;
      }

      var1 = var2;
      if(var3 != 0) {
         var1 = var2 * 31 * var3;
      }

      return var1;
   }

   public void a() {
      this.n.invalidateSelf();
   }

   public void a(Canvas var1, Matrix var2, int var3) {
      com.airbnb.lottie.d.a("GradientFillContent#draw");
      this.e.reset();

      for(int var5 = 0; var5 < this.h.size(); ++var5) {
         this.e.addPath(((k)this.h.get(var5)).e(), var2);
      }

      this.e.computeBounds(this.g, false);
      Object var6;
      if(this.i == com.airbnb.lottie.c.b.f.a) {
         var6 = this.c();
      } else {
         var6 = this.d();
      }

      this.d.set(var2);
      ((Shader)var6).setLocalMatrix(this.d);
      this.f.setShader((Shader)var6);
      float var4 = (float)var3 / 255.0F;
      var3 = (int)((float)((Integer)this.k.b()).intValue() * var4 / 100.0F * 255.0F);
      this.f.setAlpha(var3);
      var1.drawPath(this.e, this.f);
      com.airbnb.lottie.d.b("GradientFillContent#draw");
   }

   public void a(RectF var1, Matrix var2) {
      this.e.reset();

      for(int var3 = 0; var3 < this.h.size(); ++var3) {
         this.e.addPath(((k)this.h.get(var3)).e(), var2);
      }

      this.e.computeBounds(var1, false);
      var1.set(var1.left - 1.0F, var1.top - 1.0F, var1.right + 1.0F, var1.bottom + 1.0F);
   }

   public void a(String var1, String var2, ColorFilter var3) {
   }

   public void a(List var1, List var2) {
      for(int var3 = 0; var3 < var2.size(); ++var3) {
         b var4 = (b)var2.get(var3);
         if(var4 instanceof k) {
            this.h.add((k)var4);
         }
      }

   }

   public String b() {
      return this.a;
   }
}
