package com.airbnb.lottie.a.a;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.RadialGradient;
import android.graphics.RectF;
import android.graphics.Shader.TileMode;

public class h extends a {
   private final String b;
   private final android.support.v4.g.f c = new android.support.v4.g.f();
   private final android.support.v4.g.f d = new android.support.v4.g.f();
   private final RectF e = new RectF();
   private final com.airbnb.lottie.c.b.f f;
   private final int g;
   private final com.airbnb.lottie.a.b.a h;
   private final com.airbnb.lottie.a.b.a i;
   private final com.airbnb.lottie.a.b.a j;

   public h(com.airbnb.lottie.f var1, com.airbnb.lottie.c.c.a var2, com.airbnb.lottie.c.b.e var3) {
      super(var1, var2, var3.h().a(), var3.i().a(), var3.d(), var3.g(), var3.j(), var3.k());
      this.b = var3.a();
      this.f = var3.b();
      this.g = (int)(var1.m().c() / 32L);
      this.h = var3.c().a();
      this.h.a(this);
      var2.a(this.h);
      this.i = var3.e().a();
      this.i.a(this);
      var2.a(this.i);
      this.j = var3.f().a();
      this.j.a(this);
      var2.a(this.j);
   }

   private LinearGradient c() {
      int var4 = this.e();
      LinearGradient var8 = (LinearGradient)this.c.a((long)var4);
      if(var8 == null) {
         PointF var13 = (PointF)this.i.b();
         PointF var9 = (PointF)this.j.b();
         com.airbnb.lottie.c.b.c var11 = (com.airbnb.lottie.c.b.c)this.h.b();
         int[] var10 = var11.b();
         float[] var12 = var11.a();
         int var3 = (int)(this.e.left + this.e.width() / 2.0F + var13.x);
         float var1 = this.e.top;
         float var2 = this.e.height() / 2.0F;
         int var6 = (int)(var13.y + var1 + var2);
         int var7 = (int)(this.e.left + this.e.width() / 2.0F + var9.x);
         int var5 = (int)(this.e.top + this.e.height() / 2.0F + var9.y);
         var8 = new LinearGradient((float)var3, (float)var6, (float)var7, (float)var5, var10, var12, TileMode.CLAMP);
         this.c.b((long)var4, var8);
      }

      return var8;
   }

   private RadialGradient d() {
      int var5 = this.e();
      RadialGradient var8 = (RadialGradient)this.d.a((long)var5);
      if(var8 == null) {
         PointF var13 = (PointF)this.i.b();
         PointF var9 = (PointF)this.j.b();
         com.airbnb.lottie.c.b.c var11 = (com.airbnb.lottie.c.b.c)this.h.b();
         int[] var10 = var11.b();
         float[] var12 = var11.a();
         int var4 = (int)(this.e.left + this.e.width() / 2.0F + var13.x);
         float var2 = this.e.top;
         float var1 = this.e.height() / 2.0F;
         int var7 = (int)(var13.y + var2 + var1);
         int var6 = (int)(this.e.left + this.e.width() / 2.0F + var9.x);
         var1 = this.e.top;
         var2 = this.e.height() / 2.0F;
         int var3 = (int)(var9.y + var1 + var2);
         var1 = (float)Math.hypot((double)(var6 - var4), (double)(var3 - var7));
         var8 = new RadialGradient((float)var4, (float)var7, var1, var10, var12, TileMode.CLAMP);
         this.d.b((long)var5, var8);
      }

      return var8;
   }

   private int e() {
      int var2 = Math.round(this.i.c() * (float)this.g);
      int var4 = Math.round(this.j.c() * (float)this.g);
      int var3 = Math.round(this.h.c() * (float)this.g);
      int var1 = 17;
      if(var2 != 0) {
         var1 = var2 * 527;
      }

      var2 = var1;
      if(var4 != 0) {
         var2 = var1 * 31 * var4;
      }

      var1 = var2;
      if(var3 != 0) {
         var1 = var2 * 31 * var3;
      }

      return var1;
   }

   public void a(Canvas var1, Matrix var2, int var3) {
      this.a(this.e, var2);
      if(this.f == com.airbnb.lottie.c.b.f.a) {
         this.a.setShader(this.c());
      } else {
         this.a.setShader(this.d());
      }

      super.a(var1, var2, var3);
   }

   public void a(String var1, String var2, ColorFilter var3) {
   }

   public String b() {
      return this.b;
   }
}
