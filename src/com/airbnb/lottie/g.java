package com.airbnb.lottie;

import org.json.JSONObject;

public class g {
   private final int a;
   private final int b;
   private final String c;
   private final String d;

   private g(int var1, int var2, String var3, String var4) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
      this.d = var4;
   }

   // $FF: synthetic method
   g(int var1, int var2, String var3, String var4, Object var5) {
      this(var1, var2, var3, var4);
   }

   public String a() {
      return this.c;
   }

   public String b() {
      return this.d;
   }

   static class a {
      static g a(JSONObject var0) {
         return new g(var0.optInt("w"), var0.optInt("h"), var0.optString("id"), var0.optString("p"));
      }
   }
}
