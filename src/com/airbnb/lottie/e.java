package com.airbnb.lottie;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.support.v4.g.n;
import android.util.Log;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class e {
   private final Map a;
   private final Map b;
   private final Map c;
   private final n d;
   private final android.support.v4.g.f e;
   private final List f;
   private final HashSet g;
   private final i h;
   private final Rect i;
   private final long j;
   private final long k;
   private final float l;
   private final float m;
   private final int n;
   private final int o;
   private final int p;

   private e(Rect var1, long var2, long var4, float var6, float var7, int var8, int var9, int var10) {
      this.a = new HashMap();
      this.b = new HashMap();
      this.c = new HashMap();
      this.d = new n();
      this.e = new android.support.v4.g.f();
      this.f = new ArrayList();
      this.g = new HashSet();
      this.h = new i();
      this.i = var1;
      this.j = var2;
      this.k = var4;
      this.l = var6;
      this.m = var7;
      this.n = var8;
      this.o = var9;
      this.p = var10;
      if(!com.airbnb.lottie.d.f.a(this, 4, 5, 0)) {
         this.a("Lottie only supports bodymovin >= 4.5.0");
      }

   }

   // $FF: synthetic method
   e(Rect var1, long var2, long var4, float var6, float var7, int var8, int var9, int var10, Object var11) {
      this(var1, var2, var4, var6, var7, var8, var9, var10);
   }

   public com.airbnb.lottie.c.c.d a(long var1) {
      return (com.airbnb.lottie.c.c.d)this.e.a(var1);
   }

   public i a() {
      return this.h;
   }

   public void a(String var1) {
      Log.w("LOTTIE", var1);
      this.g.add(var1);
   }

   public void a(boolean var1) {
      this.h.a(var1);
   }

   public Rect b() {
      return this.i;
   }

   public List b(String var1) {
      return (List)this.a.get(var1);
   }

   public long c() {
      return (long)((float)(this.k - this.j) / this.l * 1000.0F);
   }

   public int d() {
      return this.n;
   }

   public int e() {
      return this.o;
   }

   public int f() {
      return this.p;
   }

   public long g() {
      return this.j;
   }

   public long h() {
      return this.k;
   }

   public List i() {
      return this.f;
   }

   public n j() {
      return this.d;
   }

   public Map k() {
      return this.c;
   }

   Map l() {
      return this.b;
   }

   public float m() {
      return (float)this.c() * this.l / 1000.0F;
   }

   public float n() {
      return this.m;
   }

   public String toString() {
      StringBuilder var2 = new StringBuilder("LottieComposition:\n");
      Iterator var1 = this.f.iterator();

      while(var1.hasNext()) {
         var2.append(((com.airbnb.lottie.c.c.d)var1.next()).a("\t"));
      }

      return var2.toString();
   }

   public static class a {
      public static a a(Context var0, InputStream var1, h var2) {
         com.airbnb.lottie.c.e var3 = new com.airbnb.lottie.c.e(var0.getResources(), var2);
         var3.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new InputStream[]{var1});
         return var3;
      }

      public static a a(Context var0, String var1, h var2) {
         InputStream var3;
         try {
            var3 = var0.getAssets().open(var1);
         } catch (IOException var4) {
            throw new IllegalStateException("Unable to find file " + var1, var4);
         }

         return a(var0, var3, var2);
      }

      public static a a(Resources var0, JSONObject var1, h var2) {
         com.airbnb.lottie.c.h var3 = new com.airbnb.lottie.c.h(var0, var2);
         var3.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new JSONObject[]{var1});
         return var3;
      }

      public static e a(Resources var0, InputStream var1) {
         e var10;
         try {
            IllegalStateException var2;
            try {
               byte[] var3 = new byte[var1.available()];
               var1.read(var3);
               String var11 = new String(var3, "UTF-8");
               JSONObject var12 = new JSONObject(var11);
               var10 = a(var0, var12);
               return var10;
            } catch (IOException var7) {
               var2 = new IllegalStateException("Unable to find file.", var7);
               Log.e("LOTTIE", "Failed to load composition.", var2);
            } catch (JSONException var8) {
               var2 = new IllegalStateException("Unable to load JSON.", var8);
               Log.e("LOTTIE", "Failed to load composition.", var2);
            }
         } finally {
            com.airbnb.lottie.d.f.a((Closeable)var1);
         }

         var10 = null;
         return var10;
      }

      public static e a(Resources var0, JSONObject var1) {
         float var3 = var0.getDisplayMetrics().density;
         int var4 = var1.optInt("w", -1);
         int var5 = var1.optInt("h", -1);
         Rect var12;
         if(var4 != -1 && var5 != -1) {
            var12 = new Rect(0, 0, (int)((float)var4 * var3), (int)((float)var5 * var3));
         } else {
            var12 = null;
         }

         long var8 = var1.optLong("ip", 0L);
         long var6 = var1.optLong("op", 0L);
         float var2 = (float)var1.optDouble("fr", 0.0D);
         String[] var10 = var1.optString("v").split("[.]");
         e var11 = new e(var12, var8, var6, var2, var3, Integer.parseInt(var10[0]), Integer.parseInt(var10[1]), Integer.parseInt(var10[2]));
         JSONArray var13 = var1.optJSONArray("assets");
         b(var13, var11);
         a(var13, var11);
         b(var1.optJSONObject("fonts"), var11);
         c(var1.optJSONArray("chars"), var11);
         a(var1, var11);
         return var11;
      }

      private static void a(List var0, android.support.v4.g.f var1, com.airbnb.lottie.c.c.d var2) {
         var0.add(var2);
         var1.b(var2.e(), var2);
      }

      private static void a(JSONArray var0, e var1) {
         if(var0 != null) {
            int var4 = var0.length();

            for(int var2 = 0; var2 < var4; ++var2) {
               JSONObject var6 = var0.optJSONObject(var2);
               JSONArray var8 = var6.optJSONArray("layers");
               if(var8 != null) {
                  ArrayList var5 = new ArrayList(var8.length());
                  android.support.v4.g.f var9 = new android.support.v4.g.f();

                  for(int var3 = 0; var3 < var8.length(); ++var3) {
                     com.airbnb.lottie.c.c.d var7 = com.airbnb.lottie.c.c.d.a.a(var8.optJSONObject(var3), var1);
                     var9.b(var7.e(), var7);
                     var5.add(var7);
                  }

                  String var10 = var6.optString("id");
                  var1.a.put(var10, var5);
               }
            }
         }

      }

      private static void a(JSONObject var0, e var1) {
         int var4 = 0;
         JSONArray var6 = var0.optJSONArray("layers");
         if(var6 != null) {
            int var5 = var6.length();

            int var3;
            for(int var2 = 0; var2 < var5; var4 = var3) {
               com.airbnb.lottie.c.c.d var7 = com.airbnb.lottie.c.c.d.a.a(var6.optJSONObject(var2), var1);
               var3 = var4;
               if(var7.k() == com.airbnb.lottie.c.c.d.b.c) {
                  var3 = var4 + 1;
               }

               a(var1.f, var1.e, var7);
               ++var2;
            }

            if(var4 > 4) {
               var1.a("You have " + var4 + " images. Lottie should primarily be " + "used with shapes. If you are using Adobe Illustrator, convert the Illustrator layers" + " to shape layers.");
            }
         }

      }

      private static void b(JSONArray var0, e var1) {
         if(var0 != null) {
            int var3 = var0.length();

            for(int var2 = 0; var2 < var3; ++var2) {
               JSONObject var4 = var0.optJSONObject(var2);
               if(var4.has("p")) {
                  g var5 = g.a.a(var4);
                  var1.b.put(var5.a(), var5);
               }
            }
         }

      }

      private static void b(JSONObject var0, e var1) {
         if(var0 != null) {
            JSONArray var5 = var0.optJSONArray("list");
            if(var5 != null) {
               int var3 = var5.length();

               for(int var2 = 0; var2 < var3; ++var2) {
                  com.airbnb.lottie.c.f var4 = com.airbnb.lottie.c.f.a.a(var5.optJSONObject(var2));
                  var1.c.put(var4.b(), var4);
               }
            }
         }

      }

      private static void c(JSONArray var0, e var1) {
         if(var0 != null) {
            int var3 = var0.length();

            for(int var2 = 0; var2 < var3; ++var2) {
               com.airbnb.lottie.c.g var4 = com.airbnb.lottie.c.g.a.a(var0.optJSONObject(var2), var1);
               var1.d.b(var4.hashCode(), var4);
            }
         }

      }
   }
}
