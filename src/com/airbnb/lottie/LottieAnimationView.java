package com.airbnb.lottie;

import android.animation.Animator.AnimatorListener;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.v7.widget.AppCompatImageView;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View.BaseSavedState;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

public class LottieAnimationView extends AppCompatImageView {
   private static final String a = LottieAnimationView.class.getSimpleName();
   private static final Map b = new HashMap();
   private static final Map c = new HashMap();
   private final h d = new h() {
      public void a(e var1) {
         if(var1 != null) {
            LottieAnimationView.this.setComposition(var1);
         }

         LottieAnimationView.this.k = null;
      }
   };
   private final f e = new f();
   private LottieAnimationView.a f;
   private String g;
   private boolean h = false;
   private boolean i = false;
   private boolean j = false;
   private a k;
   private e l;

   public LottieAnimationView(Context var1) {
      super(var1);
      this.a((AttributeSet)null);
   }

   public LottieAnimationView(Context var1, AttributeSet var2) {
      super(var1, var2);
      this.a(var2);
   }

   public LottieAnimationView(Context var1, AttributeSet var2, int var3) {
      super(var1, var2, var3);
      this.a(var2);
   }

   private void a(AttributeSet var1) {
      TypedArray var3 = this.getContext().obtainStyledAttributes(var1, j.a.LottieAnimationView);
      int var2 = var3.getInt(j.a.LottieAnimationView_lottie_cacheStrategy, LottieAnimationView.a.b.ordinal());
      this.f = LottieAnimationView.a.values()[var2];
      String var4 = var3.getString(j.a.LottieAnimationView_lottie_fileName);
      if(!this.isInEditMode() && var4 != null) {
         this.setAnimation(var4);
      }

      if(var3.getBoolean(j.a.LottieAnimationView_lottie_autoPlay, false)) {
         this.e.h();
         this.i = true;
      }

      this.e.c(var3.getBoolean(j.a.LottieAnimationView_lottie_loop, false));
      this.setImageAssetsFolder(var3.getString(j.a.LottieAnimationView_lottie_imageAssetsFolder));
      this.setProgress(var3.getFloat(j.a.LottieAnimationView_lottie_progress, 0.0F));
      this.a(var3.getBoolean(j.a.LottieAnimationView_lottie_enableMergePathsForKitKatAndAbove, false));
      if(var3.hasValue(j.a.LottieAnimationView_lottie_colorFilter)) {
         this.a((ColorFilter)(new k(var3.getColor(j.a.LottieAnimationView_lottie_colorFilter, 0))));
      }

      if(var3.hasValue(j.a.LottieAnimationView_lottie_scale)) {
         this.e.e(var3.getFloat(j.a.LottieAnimationView_lottie_scale, 1.0F));
      }

      var3.recycle();
      if(com.airbnb.lottie.d.f.a(this.getContext()) == 0.0F) {
         this.e.e();
      }

      this.h();
   }

   private void g() {
      if(this.k != null) {
         this.k.a();
         this.k = null;
      }

   }

   private void h() {
      byte var2 = 1;
      boolean var1;
      if(this.j && this.e.g()) {
         var1 = true;
      } else {
         var1 = false;
      }

      if(var1) {
         var2 = 2;
      }

      this.setLayerType(var2, (Paint)null);
   }

   void a() {
      if(this.e != null) {
         this.e.c();
      }

   }

   public void a(AnimatorListener var1) {
      this.e.a(var1);
   }

   public void a(ColorFilter var1) {
      this.e.a(var1);
   }

   public void a(final String var1, final LottieAnimationView.a var2) {
      this.g = var1;
      if(c.containsKey(var1)) {
         e var3 = (e)((WeakReference)c.get(var1)).get();
         if(var3 != null) {
            this.setComposition(var3);
            return;
         }
      } else if(b.containsKey(var1)) {
         this.setComposition((e)b.get(var1));
         return;
      }

      this.g = var1;
      this.e.n();
      this.g();
      this.k = e.a.a(this.getContext(), var1, new h() {
         public void a(e var1x) {
            if(var2 == LottieAnimationView.a.c) {
               LottieAnimationView.b.put(var1, var1x);
            } else if(var2 == LottieAnimationView.a.b) {
               LottieAnimationView.c.put(var1, new WeakReference(var1x));
            }

            LottieAnimationView.this.setComposition(var1x);
         }
      });
   }

   public void a(boolean var1) {
      this.e.a(var1);
   }

   public void b(boolean var1) {
      this.e.c(var1);
   }

   public boolean b() {
      return this.e.g();
   }

   public void c() {
      this.e.h();
      this.h();
   }

   public void d() {
      this.e.n();
      this.h();
   }

   public long getDuration() {
      long var1;
      if(this.l != null) {
         var1 = this.l.c();
      } else {
         var1 = 0L;
      }

      return var1;
   }

   public String getImageAssetsFolder() {
      return this.e.b();
   }

   public i getPerformanceTracker() {
      return this.e.d();
   }

   public float getProgress() {
      return this.e.i();
   }

   public float getScale() {
      return this.e.l();
   }

   public void invalidateDrawable(Drawable var1) {
      if(this.getDrawable() == this.e) {
         super.invalidateDrawable(this.e);
      } else {
         super.invalidateDrawable(var1);
      }

   }

   protected void onAttachedToWindow() {
      super.onAttachedToWindow();
      if(this.i && this.h) {
         this.c();
      }

   }

   protected void onDetachedFromWindow() {
      if(this.b()) {
         this.d();
         this.h = true;
      }

      this.a();
      super.onDetachedFromWindow();
   }

   protected void onRestoreInstanceState(Parcelable var1) {
      if(!(var1 instanceof LottieAnimationView.b)) {
         super.onRestoreInstanceState(var1);
      } else {
         LottieAnimationView.b var2 = (LottieAnimationView.b)var1;
         super.onRestoreInstanceState(var2.getSuperState());
         this.g = var2.a;
         if(!TextUtils.isEmpty(this.g)) {
            this.setAnimation(this.g);
         }

         this.setProgress(var2.b);
         this.b(var2.d);
         if(var2.c) {
            this.c();
         }

         this.e.a(var2.e);
      }

   }

   protected Parcelable onSaveInstanceState() {
      LottieAnimationView.b var1 = new LottieAnimationView.b(super.onSaveInstanceState());
      var1.a = this.g;
      var1.b = this.e.i();
      var1.c = this.e.g();
      var1.d = this.e.f();
      var1.e = this.e.b();
      return var1;
   }

   public void setAnimation(String var1) {
      this.a(var1, this.f);
   }

   public void setAnimation(JSONObject var1) {
      this.g();
      this.k = e.a.a(this.getResources(), var1, this.d);
   }

   public void setComposition(e var1) {
      this.e.setCallback(this);
      boolean var2 = this.e.a(var1);
      this.h();
      if(var2) {
         this.setImageDrawable((Drawable)null);
         this.setImageDrawable(this.e);
         this.l = var1;
         this.requestLayout();
      }

   }

   public void setFontAssetDelegate(b var1) {
      this.e.a(var1);
   }

   public void setImageAssetDelegate(c var1) {
      this.e.a(var1);
   }

   public void setImageAssetsFolder(String var1) {
      this.e.a(var1);
   }

   public void setImageBitmap(Bitmap var1) {
      this.a();
      this.g();
      super.setImageBitmap(var1);
   }

   public void setImageDrawable(Drawable var1) {
      if(var1 != this.e) {
         this.a();
      }

      this.g();
      super.setImageDrawable(var1);
   }

   public void setImageResource(int var1) {
      this.a();
      this.g();
      super.setImageResource(var1);
   }

   public void setMaxFrame(int var1) {
      this.e.b(var1);
   }

   public void setMaxProgress(float var1) {
      this.e.b(var1);
   }

   public void setMinFrame(int var1) {
      this.e.a(var1);
   }

   public void setMinProgress(float var1) {
      this.e.a(var1);
   }

   public void setPerformanceTrackingEnabled(boolean var1) {
      this.e.b(var1);
   }

   public void setProgress(float var1) {
      this.e.d(var1);
   }

   public void setScale(float var1) {
      this.e.e(var1);
      if(this.getDrawable() == this.e) {
         this.setImageDrawable((Drawable)null);
         this.setImageDrawable(this.e);
      }

   }

   public void setSpeed(float var1) {
      this.e.c(var1);
   }

   public void setTextDelegate(l var1) {
      this.e.a(var1);
   }

   public static enum a {
      a,
      b,
      c;
   }

   private static class b extends BaseSavedState {
      public static final Creator CREATOR = new Creator() {
         public LottieAnimationView.b a(Parcel var1) {
            return new LottieAnimationView.b(var1, null);
         }

         public LottieAnimationView.b[] a(int var1) {
            return new LottieAnimationView.b[var1];
         }

         // $FF: synthetic method
         public Object createFromParcel(Parcel var1) {
            return this.a(var1);
         }

         // $FF: synthetic method
         public Object[] newArray(int var1) {
            return this.a(var1);
         }
      };
      String a;
      float b;
      boolean c;
      boolean d;
      String e;

      private b(Parcel var1) {
         boolean var3 = true;
         super(var1);
         this.a = var1.readString();
         this.b = var1.readFloat();
         boolean var2;
         if(var1.readInt() == 1) {
            var2 = true;
         } else {
            var2 = false;
         }

         this.c = var2;
         if(var1.readInt() == 1) {
            var2 = var3;
         } else {
            var2 = false;
         }

         this.d = var2;
         this.e = var1.readString();
      }

      // $FF: synthetic method
      b(Parcel var1, Object var2) {
         this(var1);
      }

      b(Parcelable var1) {
         super(var1);
      }

      public void writeToParcel(Parcel var1, int var2) {
         byte var3 = 1;
         super.writeToParcel(var1, var2);
         var1.writeString(this.a);
         var1.writeFloat(this.b);
         byte var4;
         if(this.c) {
            var4 = 1;
         } else {
            var4 = 0;
         }

         var1.writeInt(var4);
         if(this.d) {
            var4 = var3;
         } else {
            var4 = 0;
         }

         var1.writeInt(var4);
         var1.writeString(this.e);
      }
   }
}
