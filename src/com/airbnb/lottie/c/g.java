package com.airbnb.lottie.c;

import com.airbnb.lottie.c.b.n;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class g {
   private final List a;
   private final char b;
   private final int c;
   private final double d;
   private final String e;
   private final String f;

   g(List var1, char var2, int var3, double var4, String var6, String var7) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
      this.d = var4;
      this.e = var6;
      this.f = var7;
   }

   public static int a(char var0, String var1, String var2) {
      return ((var0 + 0) * 31 + var1.hashCode()) * 31 + var2.hashCode();
   }

   public List a() {
      return this.a;
   }

   public double b() {
      return this.d;
   }

   public int hashCode() {
      return a(this.b, this.f, this.e);
   }

   public static class a {
      public static g a(JSONObject var0, com.airbnb.lottie.e var1) {
         char var2 = var0.optString("ch").charAt(0);
         int var6 = var0.optInt("size");
         double var3 = var0.optDouble("w");
         String var8 = var0.optString("style");
         String var9 = var0.optString("fFamily");
         JSONObject var10 = var0.optJSONObject("data");
         List var7 = Collections.emptyList();
         Object var12 = var7;
         if(var10 != null) {
            JSONArray var11 = var10.optJSONArray("shapes");
            var12 = var7;
            if(var11 != null) {
               var12 = new ArrayList(var11.length());

               for(int var5 = 0; var5 < var11.length(); ++var5) {
                  ((List)var12).add((n)n.a(var11.optJSONObject(var5), var1));
               }
            }
         }

         return new g((List)var12, var2, var6, var3, var8, var9);
      }
   }
}
