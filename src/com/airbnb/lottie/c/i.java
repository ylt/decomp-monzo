package com.airbnb.lottie.c;

public class i {
   Object a;
   Object b;

   private static boolean b(Object var0, Object var1) {
      boolean var2;
      if(var0 != var1 && (var0 == null || !var0.equals(var1))) {
         var2 = false;
      } else {
         var2 = true;
      }

      return var2;
   }

   public void a(Object var1, Object var2) {
      this.a = var1;
      this.b = var2;
   }

   public boolean equals(Object var1) {
      boolean var3 = false;
      boolean var2;
      if(!(var1 instanceof android.support.v4.g.j)) {
         var2 = var3;
      } else {
         android.support.v4.g.j var4 = (android.support.v4.g.j)var1;
         var2 = var3;
         if(b(var4.a, this.a)) {
            var2 = var3;
            if(b(var4.b, this.b)) {
               var2 = true;
            }
         }
      }

      return var2;
   }

   public int hashCode() {
      int var2 = 0;
      int var1;
      if(this.a == null) {
         var1 = 0;
      } else {
         var1 = this.a.hashCode();
      }

      if(this.b != null) {
         var2 = this.b.hashCode();
      }

      return var1 ^ var2;
   }

   public String toString() {
      return "Pair{" + String.valueOf(this.a) + " " + this.b + "}";
   }
}
