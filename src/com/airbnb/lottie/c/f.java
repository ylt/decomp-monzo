package com.airbnb.lottie.c;

import org.json.JSONObject;

public class f {
   private final String a;
   private final String b;
   private final String c;
   private final float d;

   f(String var1, String var2, String var3, float var4) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
      this.d = var4;
   }

   public String a() {
      return this.a;
   }

   public String b() {
      return this.b;
   }

   public String c() {
      return this.c;
   }

   public static class a {
      public static f a(JSONObject var0) {
         return new f(var0.optString("fFamily"), var0.optString("fName"), var0.optString("fStyle"), (float)var0.optDouble("ascent"));
      }
   }
}
