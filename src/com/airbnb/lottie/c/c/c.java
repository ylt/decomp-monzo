package com.airbnb.lottie.c.c;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;

public class c extends a {
   private final Paint e = new Paint(3);
   private final Rect f = new Rect();
   private final Rect g = new Rect();
   private final float h;

   c(com.airbnb.lottie.f var1, d var2, float var3) {
      super(var1, var2);
      this.h = var3;
   }

   private Bitmap f() {
      String var1 = this.c.g();
      return this.b.b(var1);
   }

   public void a(RectF var1, Matrix var2) {
      super.a(var1, var2);
      Bitmap var3 = this.f();
      if(var3 != null) {
         var1.set(var1.left, var1.top, Math.min(var1.right, (float)var3.getWidth()), Math.min(var1.bottom, (float)var3.getHeight()));
         this.a.mapRect(var1);
      }

   }

   public void a(String var1, String var2, ColorFilter var3) {
      this.e.setColorFilter(var3);
   }

   public void b(Canvas var1, Matrix var2, int var3) {
      Bitmap var4 = this.f();
      if(var4 != null) {
         this.e.setAlpha(var3);
         var1.save();
         var1.concat(var2);
         this.f.set(0, 0, var4.getWidth(), var4.getHeight());
         this.g.set(0, 0, (int)((float)var4.getWidth() * this.h), (int)((float)var4.getHeight() * this.h));
         var1.drawBitmap(var4, this.f, this.g, this.e);
         var1.restore();
      }

   }
}
