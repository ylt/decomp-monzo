package com.airbnb.lottie.c.c;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.RectF;
import java.util.ArrayList;
import java.util.List;

public class b extends a {
   private final com.airbnb.lottie.a.b.a e;
   private final List f = new ArrayList();
   private final RectF g = new RectF();
   private final RectF h = new RectF();

   public b(com.airbnb.lottie.f var1, d var2, List var3, com.airbnb.lottie.e var4) {
      super(var1, var2);
      com.airbnb.lottie.c.a.b var10 = var2.u();
      if(var10 != null) {
         this.e = var10.a();
         this.a(this.e);
         this.e.a(this);
      } else {
         this.e = null;
      }

      android.support.v4.g.f var7 = new android.support.v4.g.f(var4.i().size());
      int var5 = var3.size() - 1;

      a var11;
      for(var11 = null; var5 >= 0; --var5) {
         d var8 = (d)var3.get(var5);
         a var6 = a.a(var8, var1, var4);
         if(var6 != null) {
            var7.b(var6.c().e(), var6);
            if(var11 != null) {
               var11.a(var6);
               var11 = null;
            } else {
               this.f.add(0, var6);
               switch(null.a[var8.l().ordinal()]) {
               case 1:
               case 2:
                  var11 = var6;
               }
            }
         }
      }

      for(var5 = 0; var5 < var7.b(); ++var5) {
         var11 = (a)var7.a(var7.b(var5));
         a var9 = (a)var7.a(var11.c().m());
         if(var9 != null) {
            var11.b(var9);
         }
      }

   }

   public void a(float var1) {
      super.a(var1);
      if(this.e != null) {
         long var4 = this.b.m().c();
         var1 = (float)((long)(((Float)this.e.b()).floatValue() * 1000.0F)) / (float)var4;
      }

      float var2 = var1;
      if(this.c.b() != 0.0F) {
         var2 = var1 / this.c.b();
      }

      var1 = this.c.c();

      for(int var3 = this.f.size() - 1; var3 >= 0; --var3) {
         ((a)this.f.get(var3)).a(var2 - var1);
      }

   }

   public void a(RectF var1, Matrix var2) {
      super.a(var1, var2);
      this.g.set(0.0F, 0.0F, 0.0F, 0.0F);

      for(int var3 = this.f.size() - 1; var3 >= 0; --var3) {
         ((a)this.f.get(var3)).a(this.g, this.a);
         if(var1.isEmpty()) {
            var1.set(this.g);
         } else {
            var1.set(Math.min(var1.left, this.g.left), Math.min(var1.top, this.g.top), Math.max(var1.right, this.g.right), Math.max(var1.bottom, this.g.bottom));
         }
      }

   }

   public void a(String var1, String var2, ColorFilter var3) {
      for(int var4 = 0; var4 < this.f.size(); ++var4) {
         a var5 = (a)this.f.get(var4);
         String var6 = var5.c().f();
         if(var1 == null) {
            var5.a((String)null, (String)null, (ColorFilter)var3);
         } else if(var6.equals(var1)) {
            var5.a(var1, var2, var3);
         }
      }

   }

   void b(Canvas var1, Matrix var2, int var3) {
      com.airbnb.lottie.d.a("CompositionLayer#draw");
      var1.save();
      this.h.set(0.0F, 0.0F, (float)this.c.h(), (float)this.c.i());
      var2.mapRect(this.h);

      for(int var4 = this.f.size() - 1; var4 >= 0; --var4) {
         boolean var5 = true;
         if(!this.h.isEmpty()) {
            var5 = var1.clipRect(this.h);
         }

         if(var5) {
            ((a)this.f.get(var4)).a(var1, var2, var3);
         }
      }

      var1.restore();
      com.airbnb.lottie.d.b("CompositionLayer#draw");
   }
}
