package com.airbnb.lottie.c.c;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.RectF;
import com.airbnb.lottie.c.b.n;
import java.util.Collections;

public class f extends a {
   private final com.airbnb.lottie.a.a.c e;

   f(com.airbnb.lottie.f var1, d var2) {
      super(var1, var2);
      this.e = new com.airbnb.lottie.a.a.c(var1, this, new n(var2.f(), var2.n()));
      this.e.a(Collections.emptyList(), Collections.emptyList());
   }

   public void a(RectF var1, Matrix var2) {
      super.a(var1, var2);
      this.e.a(var1, this.a);
   }

   public void a(String var1, String var2, ColorFilter var3) {
      this.e.a(var1, var2, var3);
   }

   void b(Canvas var1, Matrix var2, int var3) {
      this.e.a(var1, var2, var3);
   }
}
