package com.airbnb.lottie.c.c;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Paint.Style;

public class g extends a {
   private final RectF e = new RectF();
   private final Paint f = new Paint();
   private final d g;

   g(com.airbnb.lottie.f var1, d var2) {
      super(var1, var2);
      this.g = var2;
      this.f.setAlpha(0);
      this.f.setStyle(Style.FILL);
      this.f.setColor(var2.p());
   }

   private void a(Matrix var1) {
      this.e.set(0.0F, 0.0F, (float)this.g.r(), (float)this.g.q());
      var1.mapRect(this.e);
   }

   public void a(RectF var1, Matrix var2) {
      super.a(var1, var2);
      this.a(this.a);
      var1.set(this.e);
   }

   public void a(String var1, String var2, ColorFilter var3) {
      this.f.setColorFilter(var3);
   }

   public void b(Canvas var1, Matrix var2, int var3) {
      int var6 = Color.alpha(this.g.p());
      if(var6 != 0) {
         float var5 = (float)var3 / 255.0F;
         float var4 = (float)var6 / 255.0F;
         var3 = (int)((float)((Integer)this.d.a().b()).intValue() * var4 / 100.0F * var5 * 255.0F);
         this.f.setAlpha(var3);
         if(var3 > 0) {
            this.a(var2);
            var1.drawRect(this.e, this.f);
         }
      }

   }
}
