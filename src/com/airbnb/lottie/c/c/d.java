package com.airbnb.lottie.c.c;

import android.graphics.Color;
import android.graphics.Rect;
import android.view.animation.Interpolator;
import com.airbnb.lottie.c.a.j;
import com.airbnb.lottie.c.a.k;
import com.airbnb.lottie.c.a.l;
import com.airbnb.lottie.c.b.n;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONObject;

public class d {
   private static final String a = d.class.getSimpleName();
   private final List b;
   private final com.airbnb.lottie.e c;
   private final String d;
   private final long e;
   private final d.b f;
   private final long g;
   private final String h;
   private final List i;
   private final l j;
   private final int k;
   private final int l;
   private final int m;
   private final float n;
   private final float o;
   private final int p;
   private final int q;
   private final j r;
   private final k s;
   private final com.airbnb.lottie.c.a.b t;
   private final List u;
   private final d.c v;

   private d(List var1, com.airbnb.lottie.e var2, String var3, long var4, d.b var6, long var7, String var9, List var10, l var11, int var12, int var13, int var14, float var15, float var16, int var17, int var18, j var19, k var20, List var21, d.c var22, com.airbnb.lottie.c.a.b var23) {
      this.b = var1;
      this.c = var2;
      this.d = var3;
      this.e = var4;
      this.f = var6;
      this.g = var7;
      this.h = var9;
      this.i = var10;
      this.j = var11;
      this.k = var12;
      this.l = var13;
      this.m = var14;
      this.n = var15;
      this.o = var16;
      this.p = var17;
      this.q = var18;
      this.r = var19;
      this.s = var20;
      this.u = var21;
      this.v = var22;
      this.t = var23;
   }

   // $FF: synthetic method
   d(List var1, com.airbnb.lottie.e var2, String var3, long var4, d.b var6, long var7, String var9, List var10, l var11, int var12, int var13, int var14, float var15, float var16, int var17, int var18, j var19, k var20, List var21, d.c var22, com.airbnb.lottie.c.a.b var23, Object var24) {
      this(var1, var2, var3, var4, var6, var7, var9, var10, var11, var12, var13, var14, var15, var16, var17, var18, var19, var20, var21, var22, var23);
   }

   com.airbnb.lottie.e a() {
      return this.c;
   }

   public String a(String var1) {
      StringBuilder var3 = new StringBuilder();
      var3.append(var1).append(this.f()).append("\n");
      d var2 = this.c.a(this.m());
      if(var2 != null) {
         var3.append("\t\tParents: ").append(var2.f());

         for(var2 = this.c.a(var2.m()); var2 != null; var2 = this.c.a(var2.m())) {
            var3.append("->").append(var2.f());
         }

         var3.append(var1).append("\n");
      }

      if(!this.j().isEmpty()) {
         var3.append(var1).append("\tMasks: ").append(this.j().size()).append("\n");
      }

      if(this.r() != 0 && this.q() != 0) {
         var3.append(var1).append("\tBackground: ").append(String.format(Locale.US, "%dx%d %X\n", new Object[]{Integer.valueOf(this.r()), Integer.valueOf(this.q()), Integer.valueOf(this.p())}));
      }

      if(!this.b.isEmpty()) {
         var3.append(var1).append("\tShapes:\n");
         Iterator var4 = this.b.iterator();

         while(var4.hasNext()) {
            Object var5 = var4.next();
            var3.append(var1).append("\t\t").append(var5).append("\n");
         }
      }

      return var3.toString();
   }

   float b() {
      return this.n;
   }

   float c() {
      return this.o;
   }

   List d() {
      return this.u;
   }

   public long e() {
      return this.e;
   }

   String f() {
      return this.d;
   }

   String g() {
      return this.h;
   }

   int h() {
      return this.p;
   }

   int i() {
      return this.q;
   }

   List j() {
      return this.i;
   }

   public d.b k() {
      return this.f;
   }

   d.c l() {
      return this.v;
   }

   long m() {
      return this.g;
   }

   List n() {
      return this.b;
   }

   l o() {
      return this.j;
   }

   int p() {
      return this.m;
   }

   int q() {
      return this.l;
   }

   int r() {
      return this.k;
   }

   j s() {
      return this.r;
   }

   k t() {
      return this.s;
   }

   public String toString() {
      return this.a("");
   }

   com.airbnb.lottie.c.a.b u() {
      return this.t;
   }

   public static class a {
      public static d a(com.airbnb.lottie.e var0) {
         Rect var1 = var0.b();
         return new d(Collections.emptyList(), var0, "root", -1L, d.b.a, -1L, (String)null, Collections.emptyList(), l.a.a(), 0, 0, 0, 0.0F, 0.0F, var1.width(), var1.height(), (j)null, (k)null, Collections.emptyList(), d.c.a, (com.airbnb.lottie.c.a.b)null);
      }

      public static d a(JSONObject var0, com.airbnb.lottie.e var1) {
         String var21 = var0.optString("nm");
         String var20 = var0.optString("refId");
         if(var21.endsWith(".ai") || var0.optString("cl", "").equals("ai")) {
            var1.a("Convert your Illustrator layers to shape layers.");
         }

         long var14 = var0.optLong("ind");
         int var8 = 0;
         int var7 = 0;
         int var6 = 0;
         byte var11 = 0;
         int var10 = 0;
         int var9 = var0.optInt("ty", -1);
         d.b var16;
         if(var9 < d.b.g.ordinal()) {
            var16 = d.b.values()[var9];
         } else {
            var16 = d.b.g;
         }

         d.b var17 = var16;
         if(var16 == d.b.f) {
            var17 = var16;
            if(!com.airbnb.lottie.d.f.a(var1, 4, 8, 0)) {
               var17 = d.b.g;
               var1.a("Text is only supported on bodymovin >= 4.8.0");
            }
         }

         long var12 = var0.optLong("parent", -1L);
         if(var17 == d.b.b) {
            var8 = (int)((float)var0.optInt("sw") * var1.n());
            var7 = (int)((float)var0.optInt("sh") * var1.n());
            var6 = Color.parseColor(var0.optString("sc"));
         }

         l var25 = l.a.a(var0.optJSONObject("ks"), var1);
         d.c var22 = d.c.values()[var0.optInt("tt")];
         ArrayList var23 = new ArrayList();
         ArrayList var24 = new ArrayList();
         JSONArray var27 = var0.optJSONArray("masksProperties");
         if(var27 != null) {
            for(var9 = 0; var9 < var27.length(); ++var9) {
               var23.add(com.airbnb.lottie.c.b.g.a.a(var27.optJSONObject(var9), var1));
            }
         }

         ArrayList var26 = new ArrayList();
         var27 = var0.optJSONArray("shapes");
         if(var27 != null) {
            for(var9 = 0; var9 < var27.length(); ++var9) {
               com.airbnb.lottie.c.b.b var18 = n.a(var27.optJSONObject(var9), var1);
               if(var18 != null) {
                  var26.add(var18);
               }
            }
         }

         j var28 = null;
         k var29 = null;
         JSONObject var19 = var0.optJSONObject("t");
         if(var19 != null) {
            var28 = j.a.a(var19.optJSONObject("d"), var1);
            var29 = k.a.a(var19.optJSONArray("a").optJSONObject(0), var1);
         }

         if(var0.has("ef")) {
            var1.a("Lottie doesn't support layer effects. If you are using them for  fills, strokes, trim paths etc. then try adding them directly as contents  in your shape.");
         }

         float var5 = (float)var0.optDouble("sr", 1.0D);
         float var3 = (float)var0.optDouble("st") / var1.m();
         var9 = var11;
         if(var17 == d.b.a) {
            var9 = (int)((float)var0.optInt("w") * var1.n());
            var10 = (int)((float)var0.optInt("h") * var1.n());
         }

         float var4 = (float)var0.optLong("ip") / var5;
         float var2 = (float)var0.optLong("op") / var5;
         if(var4 > 0.0F) {
            var24.add(new com.airbnb.lottie.a.a(var1, Float.valueOf(0.0F), Float.valueOf(0.0F), (Interpolator)null, 0.0F, Float.valueOf(var4)));
         }

         if(var2 <= 0.0F) {
            var2 = (float)(var1.h() + 1L);
         }

         var24.add(new com.airbnb.lottie.a.a(var1, Float.valueOf(1.0F), Float.valueOf(1.0F), (Interpolator)null, var4, Float.valueOf(var2)));
         var24.add(new com.airbnb.lottie.a.a(var1, Float.valueOf(0.0F), Float.valueOf(0.0F), (Interpolator)null, var2, Float.valueOf(Float.MAX_VALUE)));
         com.airbnb.lottie.c.a.b var30 = null;
         if(var0.has("tm")) {
            var30 = com.airbnb.lottie.c.a.b.a.a(var0.optJSONObject("tm"), var1, false);
         }

         return new d(var26, var1, var21, var14, var17, var12, var20, var23, var25, var8, var7, var6, var5, var3, var9, var10, var28, var29, var24, var22, var30);
      }
   }

   public static enum b {
      a,
      b,
      c,
      d,
      e,
      f,
      g;
   }

   static enum c {
      a,
      b,
      c,
      d;
   }
}
