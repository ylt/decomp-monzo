package com.airbnb.lottie.c.c;

import android.annotation.SuppressLint;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.graphics.Path.FillType;
import android.graphics.PorterDuff.Mode;
import android.util.Log;
import com.airbnb.lottie.a.b.n;
import com.airbnb.lottie.a.b.p;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public abstract class a implements com.airbnb.lottie.a.a.d, com.airbnb.lottie.a.b.a.a {
   final Matrix a = new Matrix();
   final com.airbnb.lottie.f b;
   final d c;
   final p d;
   private final Path e = new Path();
   private final Matrix f = new Matrix();
   private final Paint g = new Paint(1);
   private final Paint h = new Paint(1);
   private final Paint i = new Paint(1);
   private final Paint j = new Paint();
   private final RectF k = new RectF();
   private final RectF l = new RectF();
   private final RectF m = new RectF();
   private final RectF n = new RectF();
   private final String o;
   private com.airbnb.lottie.a.b.g p;
   private a q;
   private a r;
   private List s;
   private final List t = new ArrayList();
   private boolean u = true;

   a(com.airbnb.lottie.f var1, d var2) {
      this.b = var1;
      this.c = var2;
      this.o = var2.f() + "#draw";
      this.j.setXfermode(new PorterDuffXfermode(Mode.CLEAR));
      this.h.setXfermode(new PorterDuffXfermode(Mode.DST_IN));
      if(var2.l() == d.c.c) {
         this.i.setXfermode(new PorterDuffXfermode(Mode.DST_OUT));
      } else {
         this.i.setXfermode(new PorterDuffXfermode(Mode.DST_IN));
      }

      this.d = var2.o().h();
      this.d.a((com.airbnb.lottie.a.b.a.a)this);
      this.d.a(this);
      if(var2.j() != null && !var2.j().isEmpty()) {
         this.p = new com.airbnb.lottie.a.b.g(var2.j());
         Iterator var4 = this.p.b().iterator();

         com.airbnb.lottie.a.b.a var3;
         while(var4.hasNext()) {
            var3 = (com.airbnb.lottie.a.b.a)var4.next();
            this.a(var3);
            var3.a(this);
         }

         var4 = this.p.c().iterator();

         while(var4.hasNext()) {
            var3 = (com.airbnb.lottie.a.b.a)var4.next();
            this.a(var3);
            var3.a(this);
         }
      }

      this.f();
   }

   static a a(d var0, com.airbnb.lottie.f var1, com.airbnb.lottie.e var2) {
      Object var3;
      switch(null.a[var0.k().ordinal()]) {
      case 1:
         var3 = new f(var1, var0);
         break;
      case 2:
         var3 = new b(var1, var0, var2.b(var0.g()), var2);
         break;
      case 3:
         var3 = new g(var1, var0);
         break;
      case 4:
         var3 = new c(var1, var0, var2.n());
         break;
      case 5:
         var3 = new e(var1, var0);
         break;
      case 6:
         var3 = new h(var1, var0);
         break;
      default:
         Log.w("LOTTIE", "Unknown layer type " + var0.k());
         var3 = null;
      }

      return (a)var3;
   }

   private void a(Canvas var1) {
      com.airbnb.lottie.d.a("Layer#clearLayer");
      var1.drawRect(this.k.left - 1.0F, this.k.top - 1.0F, this.k.right + 1.0F, 1.0F + this.k.bottom, this.j);
      com.airbnb.lottie.d.b("Layer#clearLayer");
   }

   @SuppressLint({"WrongConstant"})
   private void a(Canvas var1, Matrix var2) {
      com.airbnb.lottie.d.a("Layer#drawMask");
      com.airbnb.lottie.d.a("Layer#saveLayer");
      var1.saveLayer(this.k, this.h, 19);
      com.airbnb.lottie.d.b("Layer#saveLayer");
      this.a(var1);
      int var4 = this.p.a().size();

      for(int var3 = 0; var3 < var4; ++var3) {
         com.airbnb.lottie.c.b.g var6 = (com.airbnb.lottie.c.b.g)this.p.a().get(var3);
         Path var7 = (Path)((com.airbnb.lottie.a.b.a)this.p.b().get(var3)).b();
         this.e.set(var7);
         this.e.transform(var2);
         switch(null.b[var6.a().ordinal()]) {
         case 1:
            this.e.setFillType(FillType.INVERSE_WINDING);
            break;
         default:
            this.e.setFillType(FillType.WINDING);
         }

         com.airbnb.lottie.a.b.a var8 = (com.airbnb.lottie.a.b.a)this.p.c().get(var3);
         int var5 = this.g.getAlpha();
         this.g.setAlpha((int)((float)((Integer)var8.b()).intValue() * 2.55F));
         var1.drawPath(this.e, this.g);
         this.g.setAlpha(var5);
      }

      com.airbnb.lottie.d.a("Layer#restoreLayer");
      var1.restore();
      com.airbnb.lottie.d.b("Layer#restoreLayer");
      com.airbnb.lottie.d.b("Layer#drawMask");
   }

   private void a(boolean var1) {
      if(var1 != this.u) {
         this.u = var1;
         this.g();
      }

   }

   private void b(float var1) {
      this.b.m().a().a(this.c.f(), var1);
   }

   private void b(RectF var1, Matrix var2) {
      this.l.set(0.0F, 0.0F, 0.0F, 0.0F);
      if(this.e()) {
         int var4 = this.p.a().size();
         int var3 = 0;

         while(true) {
            if(var3 >= var4) {
               var1.set(Math.max(var1.left, this.l.left), Math.max(var1.top, this.l.top), Math.min(var1.right, this.l.right), Math.min(var1.bottom, this.l.bottom));
               break;
            }

            com.airbnb.lottie.c.b.g var6 = (com.airbnb.lottie.c.b.g)this.p.a().get(var3);
            Path var5 = (Path)((com.airbnb.lottie.a.b.a)this.p.b().get(var3)).b();
            this.e.set(var5);
            this.e.transform(var2);
            switch(null.b[var6.a().ordinal()]) {
            case 1:
            case 2:
            case 3:
               return;
            default:
               this.e.computeBounds(this.n, false);
               if(var3 == 0) {
                  this.l.set(this.n);
               } else {
                  this.l.set(Math.min(this.l.left, this.n.left), Math.min(this.l.top, this.n.top), Math.max(this.l.right, this.n.right), Math.max(this.l.bottom, this.n.bottom));
               }

               ++var3;
            }
         }
      }

   }

   private void c(RectF var1, Matrix var2) {
      if(this.d() && this.c.l() != d.c.c) {
         this.q.a(this.m, var2);
         var1.set(Math.max(var1.left, this.m.left), Math.max(var1.top, this.m.top), Math.min(var1.right, this.m.right), Math.min(var1.bottom, this.m.bottom));
      }

   }

   private void f() {
      if(!this.c.d().isEmpty()) {
         final com.airbnb.lottie.a.b.c var2 = new com.airbnb.lottie.a.b.c(this.c.d());
         var2.a();
         var2.a(new com.airbnb.lottie.a.b.a.a() {
            public void a() {
               a var2x = a.this;
               boolean var1;
               if(((Float)var2.b()).floatValue() == 1.0F) {
                  var1 = true;
               } else {
                  var1 = false;
               }

               var2x.a(var1);
            }
         });
         boolean var1;
         if(((Float)var2.b()).floatValue() == 1.0F) {
            var1 = true;
         } else {
            var1 = false;
         }

         this.a(var1);
         this.a((com.airbnb.lottie.a.b.a)var2);
      } else {
         this.a(true);
      }

   }

   private void g() {
      this.b.invalidateSelf();
   }

   private void h() {
      if(this.s == null) {
         if(this.r == null) {
            this.s = Collections.emptyList();
         } else {
            this.s = new ArrayList();

            for(a var1 = this.r; var1 != null; var1 = var1.r) {
               this.s.add(var1);
            }
         }
      }

   }

   public void a() {
      this.g();
   }

   void a(float var1) {
      float var2 = var1;
      if(this.c.b() != 0.0F) {
         var2 = var1 / this.c.b();
      }

      if(this.q != null) {
         this.q.a(var2);
      }

      for(int var3 = 0; var3 < this.t.size(); ++var3) {
         ((com.airbnb.lottie.a.b.a)this.t.get(var3)).a(var2);
      }

   }

   @SuppressLint({"WrongConstant"})
   public void a(Canvas var1, Matrix var2, int var3) {
      com.airbnb.lottie.d.a(this.o);
      if(!this.u) {
         com.airbnb.lottie.d.b(this.o);
      } else {
         this.h();
         com.airbnb.lottie.d.a("Layer#parentMatrix");
         this.f.reset();
         this.f.set(var2);

         for(int var5 = this.s.size() - 1; var5 >= 0; --var5) {
            this.f.preConcat(((a)this.s.get(var5)).d.d());
         }

         com.airbnb.lottie.d.b("Layer#parentMatrix");
         float var4 = (float)var3 / 255.0F;
         var3 = (int)((float)((Integer)this.d.a().b()).intValue() * var4 / 100.0F * 255.0F);
         if(!this.d() && !this.e()) {
            this.f.preConcat(this.d.d());
            com.airbnb.lottie.d.a("Layer#drawLayer");
            this.b(var1, this.f, var3);
            com.airbnb.lottie.d.b("Layer#drawLayer");
            this.b(com.airbnb.lottie.d.b(this.o));
         } else {
            com.airbnb.lottie.d.a("Layer#computeBounds");
            this.k.set(0.0F, 0.0F, 0.0F, 0.0F);
            this.a(this.k, this.f);
            this.c(this.k, this.f);
            this.f.preConcat(this.d.d());
            this.b(this.k, this.f);
            this.k.set(0.0F, 0.0F, (float)var1.getWidth(), (float)var1.getHeight());
            com.airbnb.lottie.d.b("Layer#computeBounds");
            com.airbnb.lottie.d.a("Layer#saveLayer");
            var1.saveLayer(this.k, this.g, 31);
            com.airbnb.lottie.d.b("Layer#saveLayer");
            this.a(var1);
            com.airbnb.lottie.d.a("Layer#drawLayer");
            this.b(var1, this.f, var3);
            com.airbnb.lottie.d.b("Layer#drawLayer");
            if(this.e()) {
               this.a(var1, this.f);
            }

            if(this.d()) {
               com.airbnb.lottie.d.a("Layer#drawMatte");
               com.airbnb.lottie.d.a("Layer#saveLayer");
               var1.saveLayer(this.k, this.i, 19);
               com.airbnb.lottie.d.b("Layer#saveLayer");
               this.a(var1);
               this.q.a(var1, var2, var3);
               com.airbnb.lottie.d.a("Layer#restoreLayer");
               var1.restore();
               com.airbnb.lottie.d.b("Layer#restoreLayer");
               com.airbnb.lottie.d.b("Layer#drawMatte");
            }

            com.airbnb.lottie.d.a("Layer#restoreLayer");
            var1.restore();
            com.airbnb.lottie.d.b("Layer#restoreLayer");
            this.b(com.airbnb.lottie.d.b(this.o));
         }
      }

   }

   public void a(RectF var1, Matrix var2) {
      this.a.set(var2);
      this.a.preConcat(this.d.d());
   }

   public void a(com.airbnb.lottie.a.b.a var1) {
      if(!(var1 instanceof n)) {
         this.t.add(var1);
      }

   }

   void a(a var1) {
      this.q = var1;
   }

   public void a(String var1, String var2, ColorFilter var3) {
   }

   public void a(List var1, List var2) {
   }

   public String b() {
      return this.c.f();
   }

   abstract void b(Canvas var1, Matrix var2, int var3);

   void b(a var1) {
      this.r = var1;
   }

   d c() {
      return this.c;
   }

   boolean d() {
      boolean var1;
      if(this.q != null) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   boolean e() {
      boolean var1;
      if(this.p != null && !this.p.b().isEmpty()) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }
}
