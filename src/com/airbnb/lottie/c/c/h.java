package com.airbnb.lottie.c.c;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.Paint.Style;
import com.airbnb.lottie.l;
import com.airbnb.lottie.a.b.o;
import com.airbnb.lottie.c.a.k;
import com.airbnb.lottie.c.b.n;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class h extends a {
   private final char[] e = new char[1];
   private final RectF f = new RectF();
   private final Matrix g = new Matrix();
   private final Paint h = new Paint(1) {
      {
         this.setStyle(Style.FILL);
      }
   };
   private final Paint i = new Paint(1) {
      {
         this.setStyle(Style.STROKE);
      }
   };
   private final Map j = new HashMap();
   private final o k;
   private final com.airbnb.lottie.f l;
   private final com.airbnb.lottie.e m;
   private com.airbnb.lottie.a.b.a n;
   private com.airbnb.lottie.a.b.a o;
   private com.airbnb.lottie.a.b.a p;
   private com.airbnb.lottie.a.b.a q;

   h(com.airbnb.lottie.f var1, d var2) {
      super(var1, var2);
      this.l = var1;
      this.m = var2.a();
      this.k = var2.s().b();
      this.k.a(this);
      this.a(this.k);
      k var3 = var2.t();
      if(var3 != null && var3.a != null) {
         this.n = var3.a.a();
         this.n.a(this);
         this.a(this.n);
      }

      if(var3 != null && var3.b != null) {
         this.o = var3.b.a();
         this.o.a(this);
         this.a(this.o);
      }

      if(var3 != null && var3.c != null) {
         this.p = var3.c.a();
         this.p.a(this);
         this.a(this.p);
      }

      if(var3 != null && var3.d != null) {
         this.q = var3.d.a();
         this.q.a(this);
         this.a(this.q);
      }

   }

   private List a(com.airbnb.lottie.c.g var1) {
      Object var7;
      if(this.j.containsKey(var1)) {
         var7 = (List)this.j.get(var1);
      } else {
         List var5 = var1.a();
         int var3 = var5.size();
         ArrayList var4 = new ArrayList(var3);

         for(int var2 = 0; var2 < var3; ++var2) {
            n var6 = (n)var5.get(var2);
            var4.add(new com.airbnb.lottie.a.a.c(this.l, this, var6));
         }

         this.j.put(var1, var4);
         var7 = var4;
      }

      return (List)var7;
   }

   private void a(char var1, com.airbnb.lottie.c.d var2, Canvas var3) {
      this.e[0] = var1;
      if(var2.j) {
         this.a(this.e, this.h, var3);
         this.a(this.e, this.i, var3);
      } else {
         this.a(this.e, this.i, var3);
         this.a(this.e, this.h, var3);
      }

   }

   private void a(Path var1, Paint var2, Canvas var3) {
      if(var2.getColor() != 0 && (var2.getStyle() != Style.STROKE || var2.getStrokeWidth() != 0.0F)) {
         var3.drawPath(var1, var2);
      }

   }

   private void a(com.airbnb.lottie.c.d var1, Matrix var2, com.airbnb.lottie.c.f var3, Canvas var4) {
      float var7 = (float)var1.c / 100.0F;
      float var9 = com.airbnb.lottie.d.f.a(var2);
      String var13 = var1.a;

      for(int var10 = 0; var10 < var13.length(); ++var10) {
         int var11 = com.airbnb.lottie.c.g.a(var13.charAt(var10), var3.a(), var3.c());
         com.airbnb.lottie.c.g var12 = (com.airbnb.lottie.c.g)this.m.j().a(var11);
         if(var12 != null) {
            this.a(var12, var2, var7, var1, var4);
            float var6 = (float)var12.b();
            float var8 = this.m.n();
            float var5 = (float)var1.e / 10.0F;
            if(this.q != null) {
               var5 += ((Float)this.q.b()).floatValue();
            }

            var4.translate(var5 * var9 + var6 * var7 * var8 * var9, 0.0F);
         }
      }

   }

   private void a(com.airbnb.lottie.c.d var1, com.airbnb.lottie.c.f var2, Matrix var3, Canvas var4) {
      float var7 = com.airbnb.lottie.d.f.a(var3);
      Typeface var12 = this.l.a(var2.a(), var2.c());
      if(var12 != null) {
         String var11 = var1.a;
         l var10 = this.l.j();
         if(var10 != null) {
            var11 = var10.b(var11);
         }

         this.h.setTypeface(var12);
         this.h.setTextSize((float)var1.c * this.m.n());
         this.i.setTypeface(this.h.getTypeface());
         this.i.setTextSize(this.h.getTextSize());

         for(int var9 = 0; var9 < var11.length(); ++var9) {
            char var5 = var11.charAt(var9);
            this.a(var5, var1, var4);
            this.e[0] = var5;
            float var8 = this.h.measureText(this.e, 0, 1);
            float var6 = (float)var1.e / 10.0F;
            if(this.q != null) {
               var6 += ((Float)this.q.b()).floatValue();
            }

            var4.translate(var6 * var7 + var8, 0.0F);
         }
      }

   }

   private void a(com.airbnb.lottie.c.g var1, Matrix var2, float var3, com.airbnb.lottie.c.d var4, Canvas var5) {
      List var7 = this.a(var1);

      for(int var6 = 0; var6 < var7.size(); ++var6) {
         Path var8 = ((com.airbnb.lottie.a.a.c)var7.get(var6)).e();
         var8.computeBounds(this.f, false);
         this.g.set(var2);
         this.g.preScale(var3, var3);
         var8.transform(this.g);
         if(var4.j) {
            this.a(var8, this.h, var5);
            this.a(var8, this.i, var5);
         } else {
            this.a(var8, this.i, var5);
            this.a(var8, this.h, var5);
         }
      }

   }

   private void a(char[] var1, Paint var2, Canvas var3) {
      if(var2.getColor() != 0 && (var2.getStyle() != Style.STROKE || var2.getStrokeWidth() != 0.0F)) {
         var3.drawText(var1, 0, 1, 0.0F, 0.0F, var2);
      }

   }

   void b(Canvas var1, Matrix var2, int var3) {
      var1.save();
      if(!this.l.k()) {
         var1.setMatrix(var2);
      }

      com.airbnb.lottie.c.d var5 = (com.airbnb.lottie.c.d)this.k.b();
      com.airbnb.lottie.c.f var6 = (com.airbnb.lottie.c.f)this.m.k().get(var5.b);
      if(var6 == null) {
         var1.restore();
      } else {
         if(this.n != null) {
            this.h.setColor(((Integer)this.n.b()).intValue());
         } else {
            this.h.setColor(var5.g);
         }

         if(this.o != null) {
            this.i.setColor(((Integer)this.o.b()).intValue());
         } else {
            this.i.setColor(var5.h);
         }

         var3 = ((Integer)this.d.a().b()).intValue() * 255 / 100;
         this.h.setAlpha(var3);
         this.i.setAlpha(var3);
         if(this.p != null) {
            this.i.setStrokeWidth(((Float)this.p.b()).floatValue());
         } else {
            float var4 = com.airbnb.lottie.d.f.a(var2);
            this.i.setStrokeWidth(var4 * (float)var5.i * this.m.n());
         }

         if(this.l.k()) {
            this.a(var5, var2, var6, var1);
         } else {
            this.a(var5, var6, var2, var1);
         }

         var1.restore();
      }

   }
}
