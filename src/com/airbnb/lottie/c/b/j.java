package com.airbnb.lottie.c.b;

import org.json.JSONObject;

public class j implements b {
   private final String a;
   private final com.airbnb.lottie.c.a.m b;
   private final com.airbnb.lottie.c.a.f c;
   private final com.airbnb.lottie.c.a.b d;

   private j(String var1, com.airbnb.lottie.c.a.m var2, com.airbnb.lottie.c.a.f var3, com.airbnb.lottie.c.a.b var4) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
      this.d = var4;
   }

   // $FF: synthetic method
   j(String var1, com.airbnb.lottie.c.a.m var2, com.airbnb.lottie.c.a.f var3, com.airbnb.lottie.c.a.b var4, Object var5) {
      this(var1, var2, var3, var4);
   }

   public com.airbnb.lottie.a.a.b a(com.airbnb.lottie.f var1, com.airbnb.lottie.c.c.a var2) {
      return new com.airbnb.lottie.a.a.m(var1, var2, this);
   }

   public String a() {
      return this.a;
   }

   public com.airbnb.lottie.c.a.b b() {
      return this.d;
   }

   public com.airbnb.lottie.c.a.f c() {
      return this.c;
   }

   public com.airbnb.lottie.c.a.m d() {
      return this.b;
   }

   public String toString() {
      return "RectangleShape{cornerRadius=" + this.d.b() + ", position=" + this.b + ", size=" + this.c + '}';
   }

   static class a {
      static j a(JSONObject var0, com.airbnb.lottie.e var1) {
         return new j(var0.optString("nm"), com.airbnb.lottie.c.a.e.a(var0.optJSONObject("p"), var1), com.airbnb.lottie.c.a.f.a.a(var0.optJSONObject("s"), var1), com.airbnb.lottie.c.a.b.a.a(var0.optJSONObject("r"), var1));
      }
   }
}
