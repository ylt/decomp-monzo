package com.airbnb.lottie.c.b;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class e implements b {
   private final String a;
   private final f b;
   private final com.airbnb.lottie.c.a.c c;
   private final com.airbnb.lottie.c.a.d d;
   private final com.airbnb.lottie.c.a.f e;
   private final com.airbnb.lottie.c.a.f f;
   private final com.airbnb.lottie.c.a.b g;
   private final p.b h;
   private final p.c i;
   private final List j;
   private final com.airbnb.lottie.c.a.b k;

   private e(String var1, f var2, com.airbnb.lottie.c.a.c var3, com.airbnb.lottie.c.a.d var4, com.airbnb.lottie.c.a.f var5, com.airbnb.lottie.c.a.f var6, com.airbnb.lottie.c.a.b var7, p.b var8, p.c var9, List var10, com.airbnb.lottie.c.a.b var11) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
      this.d = var4;
      this.e = var5;
      this.f = var6;
      this.g = var7;
      this.h = var8;
      this.i = var9;
      this.j = var10;
      this.k = var11;
   }

   // $FF: synthetic method
   e(String var1, f var2, com.airbnb.lottie.c.a.c var3, com.airbnb.lottie.c.a.d var4, com.airbnb.lottie.c.a.f var5, com.airbnb.lottie.c.a.f var6, com.airbnb.lottie.c.a.b var7, p.b var8, p.c var9, List var10, com.airbnb.lottie.c.a.b var11, Object var12) {
      this(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10, var11);
   }

   public com.airbnb.lottie.a.a.b a(com.airbnb.lottie.f var1, com.airbnb.lottie.c.c.a var2) {
      return new com.airbnb.lottie.a.a.h(var1, var2, this);
   }

   public String a() {
      return this.a;
   }

   public f b() {
      return this.b;
   }

   public com.airbnb.lottie.c.a.c c() {
      return this.c;
   }

   public com.airbnb.lottie.c.a.d d() {
      return this.d;
   }

   public com.airbnb.lottie.c.a.f e() {
      return this.e;
   }

   public com.airbnb.lottie.c.a.f f() {
      return this.f;
   }

   public com.airbnb.lottie.c.a.b g() {
      return this.g;
   }

   public p.b h() {
      return this.h;
   }

   public p.c i() {
      return this.i;
   }

   public List j() {
      return this.j;
   }

   public com.airbnb.lottie.c.a.b k() {
      return this.k;
   }

   static class a {
      static e a(JSONObject var0, com.airbnb.lottie.e var1) {
         String var9 = var0.optString("nm");
         JSONObject var3 = var0.optJSONObject("g");
         JSONObject var4 = var3;
         if(var3 != null) {
            var4 = var3;
            if(var3.has("k")) {
               var4 = var3.optJSONObject("k");
            }
         }

         com.airbnb.lottie.c.a.c var18 = null;
         if(var4 != null) {
            var18 = com.airbnb.lottie.c.a.c.a.a(var4, var1);
         }

         JSONObject var5 = var0.optJSONObject("o");
         com.airbnb.lottie.c.a.d var19 = null;
         if(var5 != null) {
            var19 = com.airbnb.lottie.c.a.d.a.a(var5, var1);
         }

         f var20;
         if(var0.optInt("t", 1) == 1) {
            var20 = f.a;
         } else {
            var20 = f.b;
         }

         JSONObject var7 = var0.optJSONObject("s");
         com.airbnb.lottie.c.a.f var6 = null;
         if(var7 != null) {
            var6 = com.airbnb.lottie.c.a.f.a.a(var7, var1);
         }

         JSONObject var8 = var0.optJSONObject("e");
         com.airbnb.lottie.c.a.f var21 = null;
         if(var8 != null) {
            var21 = com.airbnb.lottie.c.a.f.a.a(var8, var1);
         }

         com.airbnb.lottie.c.a.b var11 = com.airbnb.lottie.c.a.b.a.a(var0.optJSONObject("w"), var1);
         p.b var13 = p.b.values()[var0.optInt("lc") - 1];
         p.c var10 = p.c.values()[var0.optInt("lj") - 1];
         com.airbnb.lottie.c.a.b var22 = null;
         ArrayList var12 = new ArrayList();
         if(var0.has("d")) {
            JSONArray var14 = var0.optJSONArray("d");
            com.airbnb.lottie.c.a.b var17 = null;

            for(int var2 = 0; var2 < var14.length(); var17 = var22) {
               JSONObject var16 = var14.optJSONObject(var2);
               String var15 = var16.optString("n");
               if(var15.equals("o")) {
                  var22 = com.airbnb.lottie.c.a.b.a.a(var16.optJSONObject("v"), var1);
               } else {
                  label65: {
                     if(!var15.equals("d")) {
                        var22 = var17;
                        if(!var15.equals("g")) {
                           break label65;
                        }
                     }

                     var12.add(com.airbnb.lottie.c.a.b.a.a(var16.optJSONObject("v"), var1));
                     var22 = var17;
                  }
               }

               ++var2;
            }

            if(var12.size() == 1) {
               var12.add(var12.get(0));
            }

            var22 = var17;
         }

         return new e(var9, var20, var18, var19, var6, var21, var11, var13, var10, var12, var22);
      }
   }
}
