package com.airbnb.lottie.c.b;

import android.graphics.Paint.Cap;
import android.graphics.Paint.Join;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class p implements b {
   private final String a;
   private final com.airbnb.lottie.c.a.b b;
   private final List c;
   private final com.airbnb.lottie.c.a.a d;
   private final com.airbnb.lottie.c.a.d e;
   private final com.airbnb.lottie.c.a.b f;
   private final p.b g;
   private final p.c h;

   private p(String var1, com.airbnb.lottie.c.a.b var2, List var3, com.airbnb.lottie.c.a.a var4, com.airbnb.lottie.c.a.d var5, com.airbnb.lottie.c.a.b var6, p.b var7, p.c var8) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
      this.d = var4;
      this.e = var5;
      this.f = var6;
      this.g = var7;
      this.h = var8;
   }

   // $FF: synthetic method
   p(String var1, com.airbnb.lottie.c.a.b var2, List var3, com.airbnb.lottie.c.a.a var4, com.airbnb.lottie.c.a.d var5, com.airbnb.lottie.c.a.b var6, p.b var7, p.c var8, Object var9) {
      this(var1, var2, var3, var4, var5, var6, var7, var8);
   }

   public com.airbnb.lottie.a.a.b a(com.airbnb.lottie.f var1, com.airbnb.lottie.c.c.a var2) {
      return new com.airbnb.lottie.a.a.p(var1, var2, this);
   }

   public String a() {
      return this.a;
   }

   public com.airbnb.lottie.c.a.a b() {
      return this.d;
   }

   public com.airbnb.lottie.c.a.d c() {
      return this.e;
   }

   public com.airbnb.lottie.c.a.b d() {
      return this.f;
   }

   public List e() {
      return this.c;
   }

   public com.airbnb.lottie.c.a.b f() {
      return this.b;
   }

   public p.b g() {
      return this.g;
   }

   public p.c h() {
      return this.h;
   }

   static class a {
      static p a(JSONObject var0, com.airbnb.lottie.e var1) {
         String var8 = var0.optString("nm");
         ArrayList var7 = new ArrayList();
         com.airbnb.lottie.c.a.a var10 = com.airbnb.lottie.c.a.a.a.a(var0.optJSONObject("c"), var1);
         com.airbnb.lottie.c.a.b var4 = com.airbnb.lottie.c.a.b.a.a(var0.optJSONObject("w"), var1);
         com.airbnb.lottie.c.a.d var6 = com.airbnb.lottie.c.a.d.a.a(var0.optJSONObject("o"), var1);
         p.b var9 = p.b.values()[var0.optInt("lc") - 1];
         p.c var5 = p.c.values()[var0.optInt("lj") - 1];
         com.airbnb.lottie.c.a.b var3 = null;
         if(var0.has("d")) {
            JSONArray var11 = var0.optJSONArray("d");
            com.airbnb.lottie.c.a.b var14 = null;

            for(int var2 = 0; var2 < var11.length(); var14 = var3) {
               JSONObject var13 = var11.optJSONObject(var2);
               String var12 = var13.optString("n");
               if(var12.equals("o")) {
                  var3 = com.airbnb.lottie.c.a.b.a.a(var13.optJSONObject("v"), var1);
               } else {
                  label37: {
                     if(!var12.equals("d")) {
                        var3 = var14;
                        if(!var12.equals("g")) {
                           break label37;
                        }
                     }

                     var7.add(com.airbnb.lottie.c.a.b.a.a(var13.optJSONObject("v"), var1));
                     var3 = var14;
                  }
               }

               ++var2;
            }

            if(var7.size() == 1) {
               var7.add(var7.get(0));
            }

            var3 = var14;
         }

         return new p(var8, var3, var7, var10, var6, var4, var9, var5, null);
      }
   }

   public static enum b {
      a,
      b,
      c;

      public Cap a() {
         Cap var1;
         switch(null.a[this.ordinal()]) {
         case 1:
            var1 = Cap.BUTT;
            break;
         case 2:
            var1 = Cap.ROUND;
            break;
         default:
            var1 = Cap.SQUARE;
         }

         return var1;
      }
   }

   public static enum c {
      a,
      b,
      c;

      public Join a() {
         Join var1;
         switch(null.b[this.ordinal()]) {
         case 1:
            var1 = Join.BEVEL;
            break;
         case 2:
            var1 = Join.MITER;
            break;
         case 3:
            var1 = Join.ROUND;
            break;
         default:
            var1 = null;
         }

         return var1;
      }
   }
}
