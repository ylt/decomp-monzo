package com.airbnb.lottie.c.b;

import org.json.JSONObject;

public class k implements b {
   private final String a;
   private final com.airbnb.lottie.c.a.b b;
   private final com.airbnb.lottie.c.a.b c;
   private final com.airbnb.lottie.c.a.l d;

   k(String var1, com.airbnb.lottie.c.a.b var2, com.airbnb.lottie.c.a.b var3, com.airbnb.lottie.c.a.l var4) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
      this.d = var4;
   }

   public com.airbnb.lottie.a.a.b a(com.airbnb.lottie.f var1, com.airbnb.lottie.c.c.a var2) {
      return new com.airbnb.lottie.a.a.n(var1, var2, this);
   }

   public String a() {
      return this.a;
   }

   public com.airbnb.lottie.c.a.b b() {
      return this.b;
   }

   public com.airbnb.lottie.c.a.b c() {
      return this.c;
   }

   public com.airbnb.lottie.c.a.l d() {
      return this.d;
   }

   static final class a {
      static k a(JSONObject var0, com.airbnb.lottie.e var1) {
         return new k(var0.optString("nm"), com.airbnb.lottie.c.a.b.a.a(var0.optJSONObject("c"), var1, false), com.airbnb.lottie.c.a.b.a.a(var0.optJSONObject("o"), var1, false), com.airbnb.lottie.c.a.l.a.a(var0.optJSONObject("tr"), var1));
      }
   }
}
