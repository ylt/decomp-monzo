package com.airbnb.lottie.c.b;

import android.graphics.Path.FillType;
import org.json.JSONException;
import org.json.JSONObject;

public class d implements b {
   private final f a;
   private final FillType b;
   private final com.airbnb.lottie.c.a.c c;
   private final com.airbnb.lottie.c.a.d d;
   private final com.airbnb.lottie.c.a.f e;
   private final com.airbnb.lottie.c.a.f f;
   private final String g;
   private final com.airbnb.lottie.c.a.b h;
   private final com.airbnb.lottie.c.a.b i;

   private d(String var1, f var2, FillType var3, com.airbnb.lottie.c.a.c var4, com.airbnb.lottie.c.a.d var5, com.airbnb.lottie.c.a.f var6, com.airbnb.lottie.c.a.f var7, com.airbnb.lottie.c.a.b var8, com.airbnb.lottie.c.a.b var9) {
      this.a = var2;
      this.b = var3;
      this.c = var4;
      this.d = var5;
      this.e = var6;
      this.f = var7;
      this.g = var1;
      this.h = var8;
      this.i = var9;
   }

   // $FF: synthetic method
   d(String var1, f var2, FillType var3, com.airbnb.lottie.c.a.c var4, com.airbnb.lottie.c.a.d var5, com.airbnb.lottie.c.a.f var6, com.airbnb.lottie.c.a.f var7, com.airbnb.lottie.c.a.b var8, com.airbnb.lottie.c.a.b var9, Object var10) {
      this(var1, var2, var3, var4, var5, var6, var7, var8, var9);
   }

   public com.airbnb.lottie.a.a.b a(com.airbnb.lottie.f var1, com.airbnb.lottie.c.c.a var2) {
      return new com.airbnb.lottie.a.a.g(var1, var2, this);
   }

   public String a() {
      return this.g;
   }

   public f b() {
      return this.a;
   }

   public FillType c() {
      return this.b;
   }

   public com.airbnb.lottie.c.a.c d() {
      return this.c;
   }

   public com.airbnb.lottie.c.a.d e() {
      return this.d;
   }

   public com.airbnb.lottie.c.a.f f() {
      return this.e;
   }

   public com.airbnb.lottie.c.a.f g() {
      return this.f;
   }

   static class a {
      static d a(JSONObject var0, com.airbnb.lottie.e var1) {
         String var8 = var0.optString("nm");
         JSONObject var4 = var0.optJSONObject("g");
         JSONObject var3 = var4;
         if(var4 != null) {
            var3 = var4;
            if(var4.has("k")) {
               int var2 = var4.optInt("p");
               var3 = var4.optJSONObject("k");

               try {
                  var3.put("p", var2);
               } catch (JSONException var9) {
                  ;
               }
            }
         }

         com.airbnb.lottie.c.a.c var11;
         if(var3 != null) {
            var11 = com.airbnb.lottie.c.a.c.a.a(var3, var1);
         } else {
            var11 = null;
         }

         var4 = var0.optJSONObject("o");
         com.airbnb.lottie.c.a.d var12;
         if(var4 != null) {
            var12 = com.airbnb.lottie.c.a.d.a.a(var4, var1);
         } else {
            var12 = null;
         }

         FillType var5;
         if(var0.optInt("r", 1) == 1) {
            var5 = FillType.WINDING;
         } else {
            var5 = FillType.EVEN_ODD;
         }

         f var6;
         if(var0.optInt("t", 1) == 1) {
            var6 = f.a;
         } else {
            var6 = f.b;
         }

         JSONObject var7 = var0.optJSONObject("s");
         com.airbnb.lottie.c.a.f var13;
         if(var7 != null) {
            var13 = com.airbnb.lottie.c.a.f.a.a(var7, var1);
         } else {
            var13 = null;
         }

         var0 = var0.optJSONObject("e");
         com.airbnb.lottie.c.a.f var10;
         if(var0 != null) {
            var10 = com.airbnb.lottie.c.a.f.a.a(var0, var1);
         } else {
            var10 = null;
         }

         return new d(var8, var6, var5, var11, var12, var13, var10, (com.airbnb.lottie.c.a.b)null, (com.airbnb.lottie.c.a.b)null);
      }
   }
}
