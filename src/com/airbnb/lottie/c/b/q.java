package com.airbnb.lottie.c.b;

import org.json.JSONObject;

public class q implements b {
   private final String a;
   private final q.b b;
   private final com.airbnb.lottie.c.a.b c;
   private final com.airbnb.lottie.c.a.b d;
   private final com.airbnb.lottie.c.a.b e;

   private q(String var1, q.b var2, com.airbnb.lottie.c.a.b var3, com.airbnb.lottie.c.a.b var4, com.airbnb.lottie.c.a.b var5) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
      this.d = var4;
      this.e = var5;
   }

   // $FF: synthetic method
   q(String var1, q.b var2, com.airbnb.lottie.c.a.b var3, com.airbnb.lottie.c.a.b var4, com.airbnb.lottie.c.a.b var5, Object var6) {
      this(var1, var2, var3, var4, var5);
   }

   public com.airbnb.lottie.a.a.b a(com.airbnb.lottie.f var1, com.airbnb.lottie.c.c.a var2) {
      return new com.airbnb.lottie.a.a.q(var2, this);
   }

   public String a() {
      return this.a;
   }

   public q.b b() {
      return this.b;
   }

   public com.airbnb.lottie.c.a.b c() {
      return this.d;
   }

   public com.airbnb.lottie.c.a.b d() {
      return this.c;
   }

   public com.airbnb.lottie.c.a.b e() {
      return this.e;
   }

   public String toString() {
      return "Trim Path: {start: " + this.c + ", end: " + this.d + ", offset: " + this.e + "}";
   }

   static class a {
      static q a(JSONObject var0, com.airbnb.lottie.e var1) {
         return new q(var0.optString("nm"), q.b.a(var0.optInt("m", 1)), com.airbnb.lottie.c.a.b.a.a(var0.optJSONObject("s"), var1, false), com.airbnb.lottie.c.a.b.a.a(var0.optJSONObject("e"), var1, false), com.airbnb.lottie.c.a.b.a.a(var0.optJSONObject("o"), var1, false));
      }
   }

   public static enum b {
      a,
      b;

      static q.b a(int var0) {
         q.b var1;
         switch(var0) {
         case 1:
            var1 = a;
            break;
         case 2:
            var1 = b;
            break;
         default:
            throw new IllegalArgumentException("Unknown trim path type " + var0);
         }

         return var1;
      }
   }
}
