package com.airbnb.lottie.c.b;

import android.util.Log;
import org.json.JSONObject;

public class h implements b {
   private final String a;
   private final h.b b;

   private h(String var1, h.b var2) {
      this.a = var1;
      this.b = var2;
   }

   // $FF: synthetic method
   h(String var1, h.b var2, Object var3) {
      this(var1, var2);
   }

   public com.airbnb.lottie.a.a.b a(com.airbnb.lottie.f var1, com.airbnb.lottie.c.c.a var2) {
      com.airbnb.lottie.a.a.j var3;
      if(!var1.a()) {
         Log.w("LOTTIE", "Animation contains merge paths but they are disabled.");
         var3 = null;
      } else {
         var3 = new com.airbnb.lottie.a.a.j(this);
      }

      return var3;
   }

   public String a() {
      return this.a;
   }

   public h.b b() {
      return this.b;
   }

   public String toString() {
      return "MergePaths{mode=" + this.b + '}';
   }

   static class a {
      static h a(JSONObject var0) {
         return new h(var0.optString("nm"), h.b.b(var0.optInt("mm", 1)));
      }
   }

   public static enum b {
      a,
      b,
      c,
      d,
      e;

      private static h.b b(int var0) {
         h.b var1;
         switch(var0) {
         case 1:
            var1 = a;
            break;
         case 2:
            var1 = b;
            break;
         case 3:
            var1 = c;
            break;
         case 4:
            var1 = d;
            break;
         case 5:
            var1 = e;
            break;
         default:
            var1 = a;
         }

         return var1;
      }
   }
}
