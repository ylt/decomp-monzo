package com.airbnb.lottie.c.b;

public class c {
   private final float[] a;
   private final int[] b;

   public c(float[] var1, int[] var2) {
      this.a = var1;
      this.b = var2;
   }

   public void a(c var1, c var2, float var3) {
      if(var1.b.length != var2.b.length) {
         throw new IllegalArgumentException("Cannot interpolate between gradients. Lengths vary (" + var1.b.length + " vs " + var2.b.length + ")");
      } else {
         for(int var4 = 0; var4 < var1.b.length; ++var4) {
            this.a[var4] = com.airbnb.lottie.d.e.a(var1.a[var4], var2.a[var4], var3);
            this.b[var4] = com.airbnb.lottie.d.a.a(var3, var1.b[var4], var2.b[var4]);
         }

      }
   }

   public float[] a() {
      return this.a;
   }

   public int[] b() {
      return this.b;
   }

   public int c() {
      return this.b.length;
   }
}
