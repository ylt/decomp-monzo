package com.airbnb.lottie.c.b;

import org.json.JSONObject;

public class i implements b {
   private final String a;
   private final i.b b;
   private final com.airbnb.lottie.c.a.b c;
   private final com.airbnb.lottie.c.a.m d;
   private final com.airbnb.lottie.c.a.b e;
   private final com.airbnb.lottie.c.a.b f;
   private final com.airbnb.lottie.c.a.b g;
   private final com.airbnb.lottie.c.a.b h;
   private final com.airbnb.lottie.c.a.b i;

   private i(String var1, i.b var2, com.airbnb.lottie.c.a.b var3, com.airbnb.lottie.c.a.m var4, com.airbnb.lottie.c.a.b var5, com.airbnb.lottie.c.a.b var6, com.airbnb.lottie.c.a.b var7, com.airbnb.lottie.c.a.b var8, com.airbnb.lottie.c.a.b var9) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
      this.d = var4;
      this.e = var5;
      this.f = var6;
      this.g = var7;
      this.h = var8;
      this.i = var9;
   }

   // $FF: synthetic method
   i(String var1, i.b var2, com.airbnb.lottie.c.a.b var3, com.airbnb.lottie.c.a.m var4, com.airbnb.lottie.c.a.b var5, com.airbnb.lottie.c.a.b var6, com.airbnb.lottie.c.a.b var7, com.airbnb.lottie.c.a.b var8, com.airbnb.lottie.c.a.b var9, Object var10) {
      this(var1, var2, var3, var4, var5, var6, var7, var8, var9);
   }

   public com.airbnb.lottie.a.a.b a(com.airbnb.lottie.f var1, com.airbnb.lottie.c.c.a var2) {
      return new com.airbnb.lottie.a.a.l(var1, var2, this);
   }

   public String a() {
      return this.a;
   }

   public i.b b() {
      return this.b;
   }

   public com.airbnb.lottie.c.a.b c() {
      return this.c;
   }

   public com.airbnb.lottie.c.a.m d() {
      return this.d;
   }

   public com.airbnb.lottie.c.a.b e() {
      return this.e;
   }

   public com.airbnb.lottie.c.a.b f() {
      return this.f;
   }

   public com.airbnb.lottie.c.a.b g() {
      return this.g;
   }

   public com.airbnb.lottie.c.a.b h() {
      return this.h;
   }

   public com.airbnb.lottie.c.a.b i() {
      return this.i;
   }

   static class a {
      static i a(JSONObject var0, com.airbnb.lottie.e var1) {
         String var9 = var0.optString("nm");
         i.b var4 = i.b.a(var0.optInt("sy"));
         com.airbnb.lottie.c.a.b var8 = com.airbnb.lottie.c.a.b.a.a(var0.optJSONObject("pt"), var1, false);
         com.airbnb.lottie.c.a.m var6 = com.airbnb.lottie.c.a.e.a(var0.optJSONObject("p"), var1);
         com.airbnb.lottie.c.a.b var5 = com.airbnb.lottie.c.a.b.a.a(var0.optJSONObject("r"), var1, false);
         com.airbnb.lottie.c.a.b var3 = com.airbnb.lottie.c.a.b.a.a(var0.optJSONObject("or"), var1);
         com.airbnb.lottie.c.a.b var7 = com.airbnb.lottie.c.a.b.a.a(var0.optJSONObject("os"), var1, false);
         com.airbnb.lottie.c.a.b var10;
         com.airbnb.lottie.c.a.b var11;
         if(var4 == i.b.a) {
            com.airbnb.lottie.c.a.b var2 = com.airbnb.lottie.c.a.b.a.a(var0.optJSONObject("ir"), var1);
            var10 = com.airbnb.lottie.c.a.b.a.a(var0.optJSONObject("is"), var1, false);
            var11 = var2;
         } else {
            var10 = null;
            var11 = null;
         }

         return new i(var9, var4, var8, var6, var5, var11, var3, var10, var7);
      }
   }

   public static enum b {
      a(1),
      b(2);

      private final int c;

      private b(int var3) {
         this.c = var3;
      }

      static i.b a(int var0) {
         i.b[] var4 = values();
         int var2 = var4.length;
         int var1 = 0;

         i.b var3;
         while(true) {
            if(var1 >= var2) {
               var3 = null;
               break;
            }

            var3 = var4[var1];
            if(var3.c == var0) {
               break;
            }

            ++var1;
         }

         return var3;
      }
   }
}
