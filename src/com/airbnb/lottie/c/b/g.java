package com.airbnb.lottie.c.b;

import org.json.JSONObject;

public class g {
   private final g.b a;
   private final com.airbnb.lottie.c.a.h b;
   private final com.airbnb.lottie.c.a.d c;

   private g(g.b var1, com.airbnb.lottie.c.a.h var2, com.airbnb.lottie.c.a.d var3) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
   }

   // $FF: synthetic method
   g(g.b var1, com.airbnb.lottie.c.a.h var2, com.airbnb.lottie.c.a.d var3, Object var4) {
      this(var1, var2, var3);
   }

   public g.b a() {
      return this.a;
   }

   public com.airbnb.lottie.c.a.h b() {
      return this.b;
   }

   public com.airbnb.lottie.c.a.d c() {
      return this.c;
   }

   public static class a {
      public static g a(JSONObject var0, com.airbnb.lottie.e var1) {
         String var3 = var0.optString("mode");
         byte var2 = -1;
         switch(var3.hashCode()) {
         case 97:
            if(var3.equals("a")) {
               var2 = 0;
            }
            break;
         case 105:
            if(var3.equals("i")) {
               var2 = 2;
            }
            break;
         case 115:
            if(var3.equals("s")) {
               var2 = 1;
            }
         }

         g.b var4;
         switch(var2) {
         case 0:
            var4 = g.b.a;
            break;
         case 1:
            var4 = g.b.b;
            break;
         case 2:
            var4 = g.b.c;
            break;
         default:
            var4 = g.b.d;
         }

         return new g(var4, com.airbnb.lottie.c.a.h.a.a(var0.optJSONObject("pt"), var1), com.airbnb.lottie.c.a.d.a.a(var0.optJSONObject("o"), var1));
      }
   }

   public static enum b {
      a,
      b,
      c,
      d;
   }
}
