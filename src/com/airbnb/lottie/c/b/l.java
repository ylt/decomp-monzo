package com.airbnb.lottie.c.b;

import android.graphics.PointF;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class l {
   private final List a;
   private PointF b;
   private boolean c;

   public l() {
      this.a = new ArrayList();
   }

   private l(PointF var1, boolean var2, List var3) {
      this.a = new ArrayList();
      this.b = var1;
      this.c = var2;
      this.a.addAll(var3);
   }

   // $FF: synthetic method
   l(PointF var1, boolean var2, List var3, Object var4) {
      this(var1, var2, var3);
   }

   private void a(float var1, float var2) {
      if(this.b == null) {
         this.b = new PointF();
      }

      this.b.set(var1, var2);
   }

   public PointF a() {
      return this.b;
   }

   public void a(l var1, l var2, float var3) {
      if(this.b == null) {
         this.b = new PointF();
      }

      boolean var5;
      if(!var1.b() && !var2.b()) {
         var5 = false;
      } else {
         var5 = true;
      }

      this.c = var5;
      if(!this.a.isEmpty() && this.a.size() != var1.c().size() && this.a.size() != var2.c().size()) {
         throw new IllegalStateException("Curves must have the same number of control points. This: " + this.c().size() + "\tShape 1: " + var1.c().size() + "\tShape 2: " + var2.c().size());
      } else {
         int var4;
         if(this.a.isEmpty()) {
            for(var4 = var1.c().size() - 1; var4 >= 0; --var4) {
               this.a.add(new com.airbnb.lottie.c.c());
            }
         }

         PointF var6 = var1.a();
         PointF var7 = var2.a();
         this.a(com.airbnb.lottie.d.e.a(var6.x, var7.x, var3), com.airbnb.lottie.d.e.a(var6.y, var7.y, var3));

         for(var4 = this.a.size() - 1; var4 >= 0; --var4) {
            com.airbnb.lottie.c.c var8 = (com.airbnb.lottie.c.c)var1.c().get(var4);
            com.airbnb.lottie.c.c var10 = (com.airbnb.lottie.c.c)var2.c().get(var4);
            var7 = var8.a();
            var6 = var8.b();
            PointF var9 = var8.c();
            PointF var11 = var10.a();
            PointF var12 = var10.b();
            PointF var13 = var10.c();
            ((com.airbnb.lottie.c.c)this.a.get(var4)).a(com.airbnb.lottie.d.e.a(var7.x, var11.x, var3), com.airbnb.lottie.d.e.a(var7.y, var11.y, var3));
            ((com.airbnb.lottie.c.c)this.a.get(var4)).b(com.airbnb.lottie.d.e.a(var6.x, var12.x, var3), com.airbnb.lottie.d.e.a(var6.y, var12.y, var3));
            ((com.airbnb.lottie.c.c)this.a.get(var4)).c(com.airbnb.lottie.d.e.a(var9.x, var13.x, var3), com.airbnb.lottie.d.e.a(var9.y, var13.y, var3));
         }

      }
   }

   public boolean b() {
      return this.c;
   }

   public List c() {
      return this.a;
   }

   public String toString() {
      return "ShapeData{numCurves=" + this.a.size() + "closed=" + this.c + '}';
   }

   public static class a implements com.airbnb.lottie.c.a.m.a {
      public static final l.a a = new l.a();

      private static PointF a(int var0, JSONArray var1) {
         if(var0 >= var1.length()) {
            throw new IllegalArgumentException("Invalid index " + var0 + ". There are only " + var1.length() + " points.");
         } else {
            JSONArray var4 = var1.optJSONArray(var0);
            Object var5 = var4.opt(0);
            Object var6 = var4.opt(1);
            float var2;
            if(var5 instanceof Double) {
               var2 = ((Double)var5).floatValue();
            } else {
               var2 = (float)((Integer)var5).intValue();
            }

            float var3;
            if(var6 instanceof Double) {
               var3 = ((Double)var6).floatValue();
            } else {
               var3 = (float)((Integer)var6).intValue();
            }

            return new PointF(var2, var3);
         }
      }

      public l a(Object var1, float var2) {
         JSONArray var7 = null;
         JSONObject var6;
         if(var1 instanceof JSONArray) {
            var1 = ((JSONArray)var1).opt(0);
            JSONObject var14;
            if(var1 instanceof JSONObject && ((JSONObject)var1).has("v")) {
               var14 = (JSONObject)var1;
            } else {
               var14 = null;
            }

            var6 = var14;
         } else {
            var6 = var7;
            if(var1 instanceof JSONObject) {
               var6 = var7;
               if(((JSONObject)var1).has("v")) {
                  var6 = (JSONObject)var1;
               }
            }
         }

         l var15;
         if(var6 == null) {
            var15 = null;
         } else {
            JSONArray var9 = var6.optJSONArray("v");
            var7 = var6.optJSONArray("i");
            JSONArray var8 = var6.optJSONArray("o");
            boolean var5 = var6.optBoolean("c", false);
            if(var9 == null || var7 == null || var8 == null || var9.length() != var7.length() || var9.length() != var8.length()) {
               throw new IllegalStateException("Unable to process points array or tangents. " + var6);
            }

            if(var9.length() == 0) {
               var15 = new l(new PointF(), false, Collections.emptyList());
            } else {
               int var4 = var9.length();
               PointF var16 = a(0, var9);
               var16.x *= var2;
               var16.y *= var2;
               ArrayList var17 = new ArrayList(var4);

               PointF var10;
               for(int var3 = 1; var3 < var4; ++var3) {
                  var10 = a(var3, var9);
                  PointF var13 = a(var3 - 1, var9);
                  PointF var12 = a(var3 - 1, var8);
                  PointF var11 = a(var3, var7);
                  var12 = com.airbnb.lottie.d.e.a(var13, var12);
                  var11 = com.airbnb.lottie.d.e.a(var10, var11);
                  var12.x *= var2;
                  var12.y *= var2;
                  var11.x *= var2;
                  var11.y *= var2;
                  var10.x *= var2;
                  var10.y *= var2;
                  var17.add(new com.airbnb.lottie.c.c(var12, var11, var10));
               }

               if(var5) {
                  var10 = a(0, var9);
                  PointF var20 = a(var4 - 1, var9);
                  PointF var19 = a(var4 - 1, var8);
                  PointF var18 = a(0, var7);
                  var19 = com.airbnb.lottie.d.e.a(var20, var19);
                  var18 = com.airbnb.lottie.d.e.a(var10, var18);
                  if(var2 != 1.0F) {
                     var19.x *= var2;
                     var19.y *= var2;
                     var18.x *= var2;
                     var18.y *= var2;
                     var10.x *= var2;
                     var10.y *= var2;
                  }

                  var17.add(new com.airbnb.lottie.c.c(var19, var18, var10));
               }

               var15 = new l(var16, var5, var17);
            }
         }

         return var15;
      }

      // $FF: synthetic method
      public Object b(Object var1, float var2) {
         return this.a(var1, var2);
      }
   }
}
