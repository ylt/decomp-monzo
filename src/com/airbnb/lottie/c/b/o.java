package com.airbnb.lottie.c.b;

import org.json.JSONObject;

public class o implements b {
   private final String a;
   private final int b;
   private final com.airbnb.lottie.c.a.h c;

   private o(String var1, int var2, com.airbnb.lottie.c.a.h var3) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
   }

   // $FF: synthetic method
   o(String var1, int var2, com.airbnb.lottie.c.a.h var3, Object var4) {
      this(var1, var2, var3);
   }

   public com.airbnb.lottie.a.a.b a(com.airbnb.lottie.f var1, com.airbnb.lottie.c.c.a var2) {
      return new com.airbnb.lottie.a.a.o(var1, var2, this);
   }

   public String a() {
      return this.a;
   }

   public com.airbnb.lottie.c.a.h b() {
      return this.c;
   }

   public String toString() {
      return "ShapePath{name=" + this.a + ", index=" + this.b + ", hasAnimation=" + this.c.d() + '}';
   }

   static class a {
      static o a(JSONObject var0, com.airbnb.lottie.e var1) {
         com.airbnb.lottie.c.a.h var2 = com.airbnb.lottie.c.a.h.a.a(var0.optJSONObject("ks"), var1);
         return new o(var0.optString("nm"), var0.optInt("ind"), var2);
      }
   }
}
