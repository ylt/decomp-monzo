package com.airbnb.lottie.c.b;

import org.json.JSONObject;

public class a implements b {
   private final String a;
   private final com.airbnb.lottie.c.a.m b;
   private final com.airbnb.lottie.c.a.f c;

   private a(String var1, com.airbnb.lottie.c.a.m var2, com.airbnb.lottie.c.a.f var3) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
   }

   // $FF: synthetic method
   a(String var1, com.airbnb.lottie.c.a.m var2, com.airbnb.lottie.c.a.f var3, Object var4) {
      this(var1, var2, var3);
   }

   public com.airbnb.lottie.a.a.b a(com.airbnb.lottie.f var1, com.airbnb.lottie.c.c.a var2) {
      return new com.airbnb.lottie.a.a.e(var1, var2, this);
   }

   public String a() {
      return this.a;
   }

   public com.airbnb.lottie.c.a.m b() {
      return this.b;
   }

   public com.airbnb.lottie.c.a.f c() {
      return this.c;
   }

   static class a {
      static a a(JSONObject var0, com.airbnb.lottie.e var1) {
         return new a(var0.optString("nm"), com.airbnb.lottie.c.a.e.a(var0.optJSONObject("p"), var1), com.airbnb.lottie.c.a.f.a.a(var0.optJSONObject("s"), var1));
      }
   }
}
