package com.airbnb.lottie.c.b;

import android.graphics.Path.FillType;
import org.json.JSONObject;

public class m implements b {
   private final boolean a;
   private final FillType b;
   private final String c;
   private final com.airbnb.lottie.c.a.a d;
   private final com.airbnb.lottie.c.a.d e;

   private m(String var1, boolean var2, FillType var3, com.airbnb.lottie.c.a.a var4, com.airbnb.lottie.c.a.d var5) {
      this.c = var1;
      this.a = var2;
      this.b = var3;
      this.d = var4;
      this.e = var5;
   }

   // $FF: synthetic method
   m(String var1, boolean var2, FillType var3, com.airbnb.lottie.c.a.a var4, com.airbnb.lottie.c.a.d var5, Object var6) {
      this(var1, var2, var3, var4, var5);
   }

   public com.airbnb.lottie.a.a.b a(com.airbnb.lottie.f var1, com.airbnb.lottie.c.c.a var2) {
      return new com.airbnb.lottie.a.a.f(var1, var2, this);
   }

   public String a() {
      return this.c;
   }

   public com.airbnb.lottie.c.a.a b() {
      return this.d;
   }

   public com.airbnb.lottie.c.a.d c() {
      return this.e;
   }

   public FillType d() {
      return this.b;
   }

   public String toString() {
      StringBuilder var2 = (new StringBuilder()).append("ShapeFill{color=");
      String var1;
      if(this.d == null) {
         var1 = "null";
      } else {
         var1 = Integer.toHexString(((Integer)this.d.c()).intValue());
      }

      var2 = var2.append(var1).append(", fillEnabled=").append(this.a).append(", opacity=");
      Object var3;
      if(this.e == null) {
         var3 = "null";
      } else {
         var3 = this.e.b();
      }

      return var2.append(var3).append('}').toString();
   }

   static class a {
      static m a(JSONObject var0, com.airbnb.lottie.e var1) {
         String var4 = var0.optString("nm");
         JSONObject var3 = var0.optJSONObject("c");
         com.airbnb.lottie.c.a.a var8;
         if(var3 != null) {
            var8 = com.airbnb.lottie.c.a.a.a.a(var3, var1);
         } else {
            var8 = null;
         }

         JSONObject var5 = var0.optJSONObject("o");
         com.airbnb.lottie.c.a.d var7;
         if(var5 != null) {
            var7 = com.airbnb.lottie.c.a.d.a.a(var5, var1);
         } else {
            var7 = null;
         }

         boolean var2 = var0.optBoolean("fillEnabled");
         FillType var6;
         if(var0.optInt("r", 1) == 1) {
            var6 = FillType.WINDING;
         } else {
            var6 = FillType.EVEN_ODD;
         }

         return new m(var4, var2, var6, var8, var7);
      }
   }
}
