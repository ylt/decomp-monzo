package com.airbnb.lottie.c.b;

import android.util.Log;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class n implements b {
   private final String a;
   private final List b;

   public n(String var1, List var2) {
      this.a = var1;
      this.b = var2;
   }

   public static b a(JSONObject var0, com.airbnb.lottie.e var1) {
      String var3 = var0.optString("ty");
      byte var2 = -1;
      switch(var3.hashCode()) {
      case 3239:
         if(var3.equals("el")) {
            var2 = 7;
         }
         break;
      case 3270:
         if(var3.equals("fl")) {
            var2 = 3;
         }
         break;
      case 3295:
         if(var3.equals("gf")) {
            var2 = 4;
         }
         break;
      case 3307:
         if(var3.equals("gr")) {
            var2 = 0;
         }
         break;
      case 3308:
         if(var3.equals("gs")) {
            var2 = 2;
         }
         break;
      case 3488:
         if(var3.equals("mm")) {
            var2 = 11;
         }
         break;
      case 3633:
         if(var3.equals("rc")) {
            var2 = 8;
         }
         break;
      case 3646:
         if(var3.equals("rp")) {
            var2 = 12;
         }
         break;
      case 3669:
         if(var3.equals("sh")) {
            var2 = 6;
         }
         break;
      case 3679:
         if(var3.equals("sr")) {
            var2 = 10;
         }
         break;
      case 3681:
         if(var3.equals("st")) {
            var2 = 1;
         }
         break;
      case 3705:
         if(var3.equals("tm")) {
            var2 = 9;
         }
         break;
      case 3710:
         if(var3.equals("tr")) {
            var2 = 5;
         }
      }

      Object var4;
      switch(var2) {
      case 0:
         var4 = n.a.b(var0, var1);
         break;
      case 1:
         var4 = p.a.a(var0, var1);
         break;
      case 2:
         var4 = e.a.a(var0, var1);
         break;
      case 3:
         var4 = m.a.a(var0, var1);
         break;
      case 4:
         var4 = d.a.a(var0, var1);
         break;
      case 5:
         var4 = com.airbnb.lottie.c.a.l.a.a(var0, var1);
         break;
      case 6:
         var4 = o.a.a(var0, var1);
         break;
      case 7:
         var4 = a.a.a(var0, var1);
         break;
      case 8:
         var4 = j.a.a(var0, var1);
         break;
      case 9:
         var4 = q.a.a(var0, var1);
         break;
      case 10:
         var4 = i.a.a(var0, var1);
         break;
      case 11:
         var4 = h.a.a(var0);
         break;
      case 12:
         var4 = k.a.a(var0, var1);
         break;
      default:
         Log.w("LOTTIE", "Unknown shape type " + var3);
         var4 = null;
      }

      return (b)var4;
   }

   public com.airbnb.lottie.a.a.b a(com.airbnb.lottie.f var1, com.airbnb.lottie.c.c.a var2) {
      return new com.airbnb.lottie.a.a.c(var1, var2, this);
   }

   public String a() {
      return this.a;
   }

   public List b() {
      return this.b;
   }

   public String toString() {
      return "ShapeGroup{name='" + this.a + "' Shapes: " + Arrays.toString(this.b.toArray()) + '}';
   }

   static class a {
      private static n b(JSONObject var0, com.airbnb.lottie.e var1) {
         JSONArray var3 = var0.optJSONArray("it");
         String var6 = var0.optString("nm");
         ArrayList var4 = new ArrayList();

         for(int var2 = 0; var2 < var3.length(); ++var2) {
            b var5 = n.a(var3.optJSONObject(var2), var1);
            if(var5 != null) {
               var4.add(var5);
            }
         }

         return new n(var6, var4);
      }
   }
}
