package com.airbnb.lottie.c;

import android.graphics.Color;
import org.json.JSONArray;
import org.json.JSONObject;

public class d {
   public String a;
   public String b;
   public int c;
   int d;
   public int e;
   double f;
   public int g;
   public int h;
   public int i;
   public boolean j;

   d(String var1, String var2, int var3, int var4, int var5, double var6, int var8, int var9, int var10, boolean var11) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
      this.d = var4;
      this.e = var5;
      this.f = var6;
      this.g = var8;
      this.h = var9;
      this.i = var10;
      this.j = var11;
   }

   public int hashCode() {
      int var1 = this.a.hashCode();
      int var2 = this.b.hashCode();
      int var5 = this.c;
      int var3 = this.d;
      int var4 = this.e;
      long var6 = Double.doubleToLongBits(this.f);
      return (((((var1 * 31 + var2) * 31 + var5) * 31 + var3) * 31 + var4) * 31 + (int)(var6 ^ var6 >>> 32)) * 31 + this.g;
   }

   public static final class a {
      public static d a(JSONObject var0) {
         String var8 = var0.optString("t");
         String var9 = var0.optString("f");
         int var6 = var0.optInt("s");
         int var5 = var0.optInt("j");
         int var4 = var0.optInt("tr");
         double var1 = var0.optDouble("lh");
         JSONArray var10 = var0.optJSONArray("fc");
         int var7 = Color.argb(255, (int)(var10.optDouble(0) * 255.0D), (int)(var10.optDouble(1) * 255.0D), (int)(var10.optDouble(2) * 255.0D));
         var10 = var0.optJSONArray("sc");
         int var3 = 0;
         if(var10 != null) {
            var3 = Color.argb(255, (int)(var10.optDouble(0) * 255.0D), (int)(var10.optDouble(1) * 255.0D), (int)(var10.optDouble(2) * 255.0D));
         }

         return new d(var8, var9, var6, var5, var4, var1, var7, var3, var0.optInt("sw"), var0.optBoolean("of"));
      }
   }
}
