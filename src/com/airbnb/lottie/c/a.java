package com.airbnb.lottie.c;

import android.graphics.Color;
import com.airbnb.lottie.c.a.m;
import org.json.JSONArray;

public class a implements m.a {
   public static final a a = new a();

   public Integer a(Object var1, float var2) {
      JSONArray var5 = (JSONArray)var1;
      Integer var6;
      if(var5.length() == 4) {
         int var3 = 0;

         boolean var4;
         for(var4 = true; var3 < var5.length(); ++var3) {
            if(var5.optDouble(var3) > 1.0D) {
               var4 = false;
            }
         }

         if(var4) {
            var2 = 255.0F;
         } else {
            var2 = 1.0F;
         }

         var6 = Integer.valueOf(Color.argb((int)(var5.optDouble(3) * (double)var2), (int)(var5.optDouble(0) * (double)var2), (int)(var5.optDouble(1) * (double)var2), (int)(var5.optDouble(2) * (double)var2)));
      } else {
         var6 = Integer.valueOf(-16777216);
      }

      return var6;
   }

   // $FF: synthetic method
   public Object b(Object var1, float var2) {
      return this.a(var1, var2);
   }
}
