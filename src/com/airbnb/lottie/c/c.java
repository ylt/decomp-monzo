package com.airbnb.lottie.c;

import android.graphics.PointF;

public class c {
   private final PointF a;
   private final PointF b;
   private final PointF c;

   public c() {
      this.a = new PointF();
      this.b = new PointF();
      this.c = new PointF();
   }

   public c(PointF var1, PointF var2, PointF var3) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
   }

   public PointF a() {
      return this.a;
   }

   public void a(float var1, float var2) {
      this.a.set(var1, var2);
   }

   public PointF b() {
      return this.b;
   }

   public void b(float var1, float var2) {
      this.b.set(var1, var2);
   }

   public PointF c() {
      return this.c;
   }

   public void c(float var1, float var2) {
      this.c.set(var1, var2);
   }
}
