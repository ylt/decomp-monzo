package com.airbnb.lottie.c;

import android.graphics.PointF;
import com.airbnb.lottie.c.a.m;
import org.json.JSONArray;
import org.json.JSONObject;

public class j implements m.a {
   public static final j a = new j();

   public PointF a(Object var1, float var2) {
      PointF var3;
      if(var1 instanceof JSONArray) {
         var3 = com.airbnb.lottie.d.b.a((JSONArray)var1, var2);
      } else {
         if(!(var1 instanceof JSONObject)) {
            throw new IllegalArgumentException("Unable to parse point from " + var1);
         }

         var3 = com.airbnb.lottie.d.b.a((JSONObject)var1, var2);
      }

      return var3;
   }

   // $FF: synthetic method
   public Object b(Object var1, float var2) {
      return this.a(var1, var2);
   }
}
