package com.airbnb.lottie.c;

import android.content.res.Resources;
import org.json.JSONObject;

public final class h extends b {
   private final Resources a;
   private final com.airbnb.lottie.h b;

   public h(Resources var1, com.airbnb.lottie.h var2) {
      this.a = var1;
      this.b = var2;
   }

   protected com.airbnb.lottie.e a(JSONObject... var1) {
      return com.airbnb.lottie.e.a.a(this.a, var1[0]);
   }

   protected void a(com.airbnb.lottie.e var1) {
      this.b.a(var1);
   }

   // $FF: synthetic method
   protected Object doInBackground(Object[] var1) {
      return this.a((JSONObject[])var1);
   }

   // $FF: synthetic method
   protected void onPostExecute(Object var1) {
      this.a((com.airbnb.lottie.e)var1);
   }
}
