package com.airbnb.lottie.c;

import com.airbnb.lottie.c.a.m;
import org.json.JSONArray;

public class k {
   private final float a;
   private final float b;

   public k() {
      this(1.0F, 1.0F);
   }

   public k(float var1, float var2) {
      this.a = var1;
      this.b = var2;
   }

   public float a() {
      return this.a;
   }

   public float b() {
      return this.b;
   }

   public String toString() {
      return this.a() + "x" + this.b();
   }

   public static class a implements m.a {
      public static final k.a a = new k.a();

      public k a(Object var1, float var2) {
         JSONArray var3 = (JSONArray)var1;
         return new k((float)var3.optDouble(0, 1.0D) / 100.0F * var2, (float)var3.optDouble(1, 1.0D) / 100.0F * var2);
      }

      // $FF: synthetic method
      public Object b(Object var1, float var2) {
         return this.a(var1, var2);
      }
   }
}
