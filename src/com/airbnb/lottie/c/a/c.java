package com.airbnb.lottie.c.a;

import android.graphics.Color;
import android.util.Log;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class c extends o {
   private c(List var1, com.airbnb.lottie.c.b.c var2) {
      super(var1, var2);
   }

   // $FF: synthetic method
   c(List var1, com.airbnb.lottie.c.b.c var2, Object var3) {
      this(var1, var2);
   }

   public com.airbnb.lottie.a.b.a a() {
      Object var1;
      if(!this.d()) {
         var1 = new com.airbnb.lottie.a.b.n(this.b);
      } else {
         var1 = new com.airbnb.lottie.a.b.d(this.a);
      }

      return (com.airbnb.lottie.a.b.a)var1;
   }

   public static final class a {
      public static c a(JSONObject var0, com.airbnb.lottie.e var1) {
         n.a var2 = n.a(var0, 1.0F, var1, new c.b(var0.optInt("p", var0.optJSONArray("k").length() / 4))).a();
         com.airbnb.lottie.c.b.c var3 = (com.airbnb.lottie.c.b.c)var2.b;
         return new c(var2.a, var3);
      }
   }

   private static class b implements m.a {
      private final int a;

      private b(int var1) {
         this.a = var1;
      }

      // $FF: synthetic method
      b(int var1, Object var2) {
         this(var1);
      }

      private int a(double var1, double[] var3, double[] var4) {
         int var9 = 1;

         while(true) {
            if(var9 >= var3.length) {
               var9 = (int)(var4[var4.length - 1] * 255.0D);
               break;
            }

            double var7 = var3[var9 - 1];
            double var5 = var3[var9];
            if(var3[var9] >= var1) {
               var1 = (var1 - var7) / (var5 - var7);
               var9 = (int)(com.airbnb.lottie.d.e.a(var4[var9 - 1], var4[var9], var1) * 255.0D);
               break;
            }

            ++var9;
         }

         return var9;
      }

      private void a(com.airbnb.lottie.c.b.c var1, JSONArray var2) {
         byte var6 = 0;
         int var3 = this.a * 4;
         if(var2.length() > var3) {
            int var4 = (var2.length() - var3) / 2;
            double[] var8 = new double[var4];
            double[] var7 = new double[var4];
            int var5 = 0;

            while(true) {
               var4 = var6;
               if(var3 >= var2.length()) {
                  while(var4 < var1.c()) {
                     var3 = var1.b()[var4];
                     var3 = Color.argb(this.a((double)var1.a()[var4], var8, var7), Color.red(var3), Color.green(var3), Color.blue(var3));
                     var1.b()[var4] = var3;
                     ++var4;
                  }
                  break;
               }

               if(var3 % 2 == 0) {
                  var8[var5] = var2.optDouble(var3);
               } else {
                  var7[var5] = var2.optDouble(var3);
                  ++var5;
               }

               ++var3;
            }
         }

      }

      public com.airbnb.lottie.c.b.c a(Object var1, float var2) {
         int var6 = 0;
         JSONArray var12 = (JSONArray)var1;
         float[] var10 = new float[this.a];
         int[] var9 = new int[this.a];
         com.airbnb.lottie.c.b.c var11 = new com.airbnb.lottie.c.b.c(var10, var9);
         if(var12.length() != this.a * 4) {
            Log.w("LOTTIE", "Unexpected gradient length: " + var12.length() + ". Expected " + this.a * 4 + ". This may affect the appearance of the gradient. " + "Make sure to save your After Effects file before exporting an animation with " + "gradients.");
         }

         int var7 = 0;

         for(int var5 = 0; var6 < this.a * 4; ++var6) {
            int var8 = var6 / 4;
            double var3 = var12.optDouble(var6);
            switch(var6 % 4) {
            case 0:
               var10[var8] = (float)var3;
               break;
            case 1:
               var5 = (int)(var3 * 255.0D);
               break;
            case 2:
               var7 = (int)(var3 * 255.0D);
               break;
            case 3:
               var9[var8] = Color.argb(255, var5, var7, (int)(var3 * 255.0D));
            }
         }

         this.a(var11, var12);
         return var11;
      }

      // $FF: synthetic method
      public Object b(Object var1, float var2) {
         return this.a(var1, var2);
      }
   }
}
