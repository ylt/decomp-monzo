package com.airbnb.lottie.c.a;

import java.util.List;
import org.json.JSONObject;

public class b extends o {
   private b() {
      super(Float.valueOf(0.0F));
   }

   // $FF: synthetic method
   b(Object var1) {
      this();
   }

   private b(List var1, Float var2) {
      super(var1, var2);
   }

   // $FF: synthetic method
   b(List var1, Float var2, Object var3) {
      this(var1, var2);
   }

   public com.airbnb.lottie.a.b.a a() {
      Object var1;
      if(!this.d()) {
         var1 = new com.airbnb.lottie.a.b.n(this.b);
      } else {
         var1 = new com.airbnb.lottie.a.b.c(this.a);
      }

      return (com.airbnb.lottie.a.b.a)var1;
   }

   public Float b() {
      return (Float)this.b;
   }

   // $FF: synthetic method
   public Object c() {
      return this.b();
   }

   public static final class a {
      static b a() {
         return new b();
      }

      public static b a(JSONObject var0, com.airbnb.lottie.e var1) {
         return a(var0, var1, true);
      }

      public static b a(JSONObject var0, com.airbnb.lottie.e var1, boolean var2) {
         float var3;
         if(var2) {
            var3 = var1.n();
         } else {
            var3 = 1.0F;
         }

         if(var0 != null && var0.has("x")) {
            var1.a("Lottie doesn't support expressions.");
         }

         n.a var4 = n.a(var0, var3, var1, b.b.a).a();
         return new b(var4.a, (Float)var4.b);
      }
   }

   private static class b implements m.a {
      static final b.b a = new b.b();

      public Float a(Object var1, float var2) {
         return Float.valueOf(com.airbnb.lottie.d.b.a(var1) * var2);
      }

      // $FF: synthetic method
      public Object b(Object var1, float var2) {
         return this.a(var1, var2);
      }
   }
}
