package com.airbnb.lottie.c.a;

import org.json.JSONObject;

public class k {
   public final a a;
   public final a b;
   public final b c;
   public final b d;

   k(a var1, a var2, b var3, b var4) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
      this.d = var4;
   }

   public static final class a {
      public static k a(JSONObject var0, com.airbnb.lottie.e var1) {
         b var4 = null;
         k var6;
         if(var0 != null && var0.has("a")) {
            JSONObject var5 = var0.optJSONObject("a");
            var0 = var5.optJSONObject("fc");
            a var7;
            if(var0 != null) {
               var7 = a.a.a(var0, var1);
            } else {
               var7 = null;
            }

            JSONObject var2 = var5.optJSONObject("sc");
            a var8;
            if(var2 != null) {
               var8 = a.a.a(var2, var1);
            } else {
               var8 = null;
            }

            JSONObject var3 = var5.optJSONObject("sw");
            b var9;
            if(var3 != null) {
               var9 = b.a.a(var3, var1);
            } else {
               var9 = null;
            }

            var5 = var5.optJSONObject("t");
            if(var5 != null) {
               var4 = b.a.a(var5, var1);
            }

            var6 = new k(var7, var8, var9, var4);
         } else {
            var6 = new k((a)null, (a)null, (b)null, (b)null);
         }

         return var6;
      }
   }
}
