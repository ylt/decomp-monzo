package com.airbnb.lottie.c.a;

import android.graphics.PointF;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class e implements m {
   private final List a = new ArrayList();
   private PointF b;

   e() {
      this.b = new PointF(0.0F, 0.0F);
   }

   e(Object var1, com.airbnb.lottie.e var2) {
      if(this.a(var1)) {
         JSONArray var5 = (JSONArray)var1;
         int var4 = var5.length();

         for(int var3 = 0; var3 < var4; ++var3) {
            com.airbnb.lottie.a.b.h var6 = com.airbnb.lottie.a.b.h.a.a(var5.optJSONObject(var3), var2, e.a.a);
            this.a.add(var6);
         }

         com.airbnb.lottie.a.a.a(this.a);
      } else {
         this.b = com.airbnb.lottie.d.b.a((JSONArray)var1, var2.n());
      }

   }

   public static m a(JSONObject var0, com.airbnb.lottie.e var1) {
      Object var2;
      if(var0.has("k")) {
         var2 = new e(var0.opt("k"), var1);
      } else {
         var2 = new i(b.a.a(var0.optJSONObject("x"), var1), b.a.a(var0.optJSONObject("y"), var1));
      }

      return (m)var2;
   }

   private boolean a(Object var1) {
      boolean var2 = false;
      if(var1 instanceof JSONArray) {
         var1 = ((JSONArray)var1).opt(0);
         if(var1 instanceof JSONObject && ((JSONObject)var1).has("t")) {
            var2 = true;
         } else {
            var2 = false;
         }
      }

      return var2;
   }

   public com.airbnb.lottie.a.b.a a() {
      Object var1;
      if(!this.b()) {
         var1 = new com.airbnb.lottie.a.b.n(this.b);
      } else {
         var1 = new com.airbnb.lottie.a.b.i(this.a);
      }

      return (com.airbnb.lottie.a.b.a)var1;
   }

   public boolean b() {
      boolean var1;
      if(!this.a.isEmpty()) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public String toString() {
      return "initialPoint=" + this.b;
   }

   private static class a implements m.a {
      private static final m.a a = new e.a();

      public PointF a(Object var1, float var2) {
         return com.airbnb.lottie.d.b.a((JSONArray)var1, var2);
      }

      // $FF: synthetic method
      public Object b(Object var1, float var2) {
         return this.a(var1, var2);
      }
   }
}
