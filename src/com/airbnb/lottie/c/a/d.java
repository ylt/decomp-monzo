package com.airbnb.lottie.c.a;

import java.util.List;
import org.json.JSONObject;

public class d extends o {
   private d() {
      super(Integer.valueOf(100));
   }

   // $FF: synthetic method
   d(Object var1) {
      this();
   }

   d(List var1, Integer var2) {
      super(var1, var2);
   }

   public com.airbnb.lottie.a.b.a a() {
      Object var1;
      if(!this.d()) {
         var1 = new com.airbnb.lottie.a.b.n(this.b);
      } else {
         var1 = new com.airbnb.lottie.a.b.e(this.a);
      }

      return (com.airbnb.lottie.a.b.a)var1;
   }

   public Integer b() {
      return (Integer)this.b;
   }

   // $FF: synthetic method
   public Object c() {
      return this.b();
   }

   public static final class a {
      static d a() {
         return new d();
      }

      public static d a(JSONObject var0, com.airbnb.lottie.e var1) {
         if(var0 != null && var0.has("x")) {
            var1.a("Lottie doesn't support expressions.");
         }

         n.a var3 = n.a(var0, 1.0F, var1, d.b.a).a();
         Integer var2 = (Integer)var3.b;
         return new d(var3.a, var2);
      }
   }

   private static class b implements m.a {
      private static final d.b a = new d.b();

      public Integer a(Object var1, float var2) {
         return Integer.valueOf(Math.round(com.airbnb.lottie.d.b.a(var1) * var2));
      }

      // $FF: synthetic method
      public Object b(Object var1, float var2) {
         return this.a(var1, var2);
      }
   }
}
