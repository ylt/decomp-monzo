package com.airbnb.lottie.c.a;

import android.graphics.Path;
import java.util.List;
import org.json.JSONObject;

public class h extends o {
   private final Path c;

   private h(List var1, com.airbnb.lottie.c.b.l var2) {
      super(var1, var2);
      this.c = new Path();
   }

   // $FF: synthetic method
   h(List var1, com.airbnb.lottie.c.b.l var2, Object var3) {
      this(var1, var2);
   }

   Path a(com.airbnb.lottie.c.b.l var1) {
      this.c.reset();
      com.airbnb.lottie.d.e.a(var1, this.c);
      return this.c;
   }

   public com.airbnb.lottie.a.b.a a() {
      Object var1;
      if(!this.d()) {
         var1 = new com.airbnb.lottie.a.b.n(this.a((com.airbnb.lottie.c.b.l)this.b));
      } else {
         var1 = new com.airbnb.lottie.a.b.l(this.a);
      }

      return (com.airbnb.lottie.a.b.a)var1;
   }

   public static final class a {
      public static h a(JSONObject var0, com.airbnb.lottie.e var1) {
         n.a var2 = n.a(var0, var1.n(), var1, com.airbnb.lottie.c.b.l.a.a).a();
         return new h(var2.a, (com.airbnb.lottie.c.b.l)var2.b);
      }
   }
}
