package com.airbnb.lottie.c.a;

import java.util.Collections;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class n {
   private final JSONObject a;
   private final float b;
   private final com.airbnb.lottie.e c;
   private final m.a d;

   private n(JSONObject var1, float var2, com.airbnb.lottie.e var3, m.a var4) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
      this.d = var4;
   }

   static n a(JSONObject var0, float var1, com.airbnb.lottie.e var2, m.a var3) {
      return new n(var0, var1, var2, var3);
   }

   private Object a(List var1) {
      Object var2;
      if(this.a != null) {
         if(!var1.isEmpty()) {
            var2 = ((com.airbnb.lottie.a.a)var1.get(0)).a;
         } else {
            var2 = this.d.b(this.a.opt("k"), this.b);
         }
      } else {
         var2 = null;
      }

      return var2;
   }

   private static boolean a(Object var0) {
      boolean var1 = false;
      if(var0 instanceof JSONArray) {
         var0 = ((JSONArray)var0).opt(0);
         if(var0 instanceof JSONObject && ((JSONObject)var0).has("t")) {
            var1 = true;
         } else {
            var1 = false;
         }
      }

      return var1;
   }

   private List b() {
      List var2;
      if(this.a != null) {
         Object var1 = this.a.opt("k");
         if(a(var1)) {
            var2 = com.airbnb.lottie.a.a.a.a((JSONArray)var1, this.c, this.b, this.d);
         } else {
            var2 = Collections.emptyList();
         }
      } else {
         var2 = Collections.emptyList();
      }

      return var2;
   }

   n.a a() {
      List var1 = this.b();
      return new n.a(var1, this.a(var1));
   }

   static class a {
      final List a;
      final Object b;

      a(List var1, Object var2) {
         this.a = var1;
         this.b = var2;
      }
   }
}
