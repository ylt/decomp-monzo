package com.airbnb.lottie.c.a;

import android.util.Log;
import com.airbnb.lottie.a.b.p;
import java.util.Collections;
import org.json.JSONObject;

public class l implements com.airbnb.lottie.c.b.b {
   private final e a;
   private final m b;
   private final g c;
   private final b d;
   private final d e;
   private final b f;
   private final b g;

   private l(e var1, m var2, g var3, b var4, d var5, b var6, b var7) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
      this.d = var4;
      this.e = var5;
      this.f = var6;
      this.g = var7;
   }

   // $FF: synthetic method
   l(e var1, m var2, g var3, b var4, d var5, b var6, b var7, Object var8) {
      this(var1, var2, var3, var4, var5, var6, var7);
   }

   public com.airbnb.lottie.a.a.b a(com.airbnb.lottie.f var1, com.airbnb.lottie.c.c.a var2) {
      return null;
   }

   public e a() {
      return this.a;
   }

   public m b() {
      return this.b;
   }

   public g c() {
      return this.c;
   }

   public b d() {
      return this.d;
   }

   public d e() {
      return this.e;
   }

   public b f() {
      return this.f;
   }

   public b g() {
      return this.g;
   }

   public p h() {
      return new p(this);
   }

   public static class a {
      public static l a() {
         return new l(new e(), new e(), g.a.a(), b.a.a(), d.a.a(), b.a.a(), b.a.a());
      }

      public static l a(JSONObject var0, com.airbnb.lottie.e var1) {
         JSONObject var2 = var0.optJSONObject("a");
         e var9;
         if(var2 != null) {
            var9 = new e(var2.opt("k"), var1);
         } else {
            Log.w("LOTTIE", "Layer has no transform property. You may be using an unsupported layer type such as a camera.");
            var9 = new e();
         }

         JSONObject var3 = var0.optJSONObject("p");
         m var10;
         if(var3 != null) {
            var10 = e.a(var3, var1);
         } else {
            a("position");
            var10 = null;
         }

         JSONObject var4 = var0.optJSONObject("s");
         g var11;
         if(var4 != null) {
            var11 = g.a.a(var4, var1);
         } else {
            var11 = new g(Collections.emptyList(), new com.airbnb.lottie.c.k());
         }

         JSONObject var6 = var0.optJSONObject("r");
         JSONObject var5 = var6;
         if(var6 == null) {
            var5 = var0.optJSONObject("rz");
         }

         b var12;
         if(var5 != null) {
            var12 = b.a.a(var5, var1, false);
         } else {
            a("rotation");
            var12 = null;
         }

         var6 = var0.optJSONObject("o");
         d var13;
         if(var6 != null) {
            var13 = d.a.a(var6, var1);
         } else {
            var13 = new d(Collections.emptyList(), Integer.valueOf(100));
         }

         JSONObject var7 = var0.optJSONObject("so");
         b var14;
         if(var7 != null) {
            var14 = b.a.a(var7, var1, false);
         } else {
            var14 = null;
         }

         var0 = var0.optJSONObject("eo");
         b var8;
         if(var0 != null) {
            var8 = b.a.a(var0, var1, false);
         } else {
            var8 = null;
         }

         return new l(var9, var10, var11, var12, var13, var14, var8);
      }

      private static void a(String var0) {
         throw new IllegalArgumentException("Missing transform for " + var0);
      }
   }
}
