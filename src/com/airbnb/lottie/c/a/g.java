package com.airbnb.lottie.c.a;

import java.util.List;
import org.json.JSONObject;

public class g extends o {
   private g() {
      super(new com.airbnb.lottie.c.k());
   }

   // $FF: synthetic method
   g(Object var1) {
      this();
   }

   g(List var1, com.airbnb.lottie.c.k var2) {
      super(var1, var2);
   }

   public com.airbnb.lottie.a.b.a a() {
      Object var1;
      if(!this.d()) {
         var1 = new com.airbnb.lottie.a.b.n(this.b);
      } else {
         var1 = new com.airbnb.lottie.a.b.k(this.a);
      }

      return (com.airbnb.lottie.a.b.a)var1;
   }

   static final class a {
      static g a() {
         return new g();
      }

      static g a(JSONObject var0, com.airbnb.lottie.e var1) {
         n.a var2 = n.a(var0, 1.0F, var1, com.airbnb.lottie.c.k.a.a).a();
         return new g(var2.a, (com.airbnb.lottie.c.k)var2.b);
      }
   }
}
