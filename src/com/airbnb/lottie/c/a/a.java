package com.airbnb.lottie.c.a;

import java.util.List;
import org.json.JSONObject;

public class a extends o {
   private a(List var1, Integer var2) {
      super(var1, var2);
   }

   // $FF: synthetic method
   a(List var1, Integer var2, Object var3) {
      this(var1, var2);
   }

   public com.airbnb.lottie.a.b.a a() {
      Object var1;
      if(!this.d()) {
         var1 = new com.airbnb.lottie.a.b.n(this.b);
      } else {
         var1 = new com.airbnb.lottie.a.b.b(this.a);
      }

      return (com.airbnb.lottie.a.b.a)var1;
   }

   public String toString() {
      return "AnimatableColorValue{initialValue=" + this.b + '}';
   }

   public static final class a {
      public static a a(JSONObject var0, com.airbnb.lottie.e var1) {
         n.a var2 = n.a(var0, 1.0F, var1, com.airbnb.lottie.c.a.a).a();
         return new a(var2.a, (Integer)var2.b);
      }
   }
}
