package com.airbnb.lottie.c.a;

import java.util.List;
import org.json.JSONObject;

public class j extends o {
   j(List var1, com.airbnb.lottie.c.d var2) {
      super(var1, var2);
   }

   // $FF: synthetic method
   public com.airbnb.lottie.a.b.a a() {
      return this.b();
   }

   public com.airbnb.lottie.a.b.o b() {
      return new com.airbnb.lottie.a.b.o(this.a);
   }

   public static final class a {
      public static j a(JSONObject var0, com.airbnb.lottie.e var1) {
         if(var0 != null && var0.has("x")) {
            var1.a("Lottie doesn't support expressions.");
         }

         n.a var2 = n.a(var0, 1.0F, var1, j.b.a).a();
         return new j(var2.a, (com.airbnb.lottie.c.d)var2.b);
      }
   }

   private static class b implements m.a {
      private static final j.b a = new j.b();

      public com.airbnb.lottie.c.d a(Object var1, float var2) {
         return com.airbnb.lottie.c.d.a.a((JSONObject)var1);
      }

      // $FF: synthetic method
      public Object b(Object var1, float var2) {
         return this.a(var1, var2);
      }
   }
}
