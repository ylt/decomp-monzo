package com.airbnb.lottie.c.a;

import android.graphics.PointF;
import java.util.List;
import org.json.JSONObject;

public class f extends o {
   private f(List var1, PointF var2) {
      super(var1, var2);
   }

   // $FF: synthetic method
   f(List var1, PointF var2, Object var3) {
      this(var1, var2);
   }

   public com.airbnb.lottie.a.b.a a() {
      Object var1;
      if(!this.d()) {
         var1 = new com.airbnb.lottie.a.b.n(this.b);
      } else {
         var1 = new com.airbnb.lottie.a.b.j(this.a);
      }

      return (com.airbnb.lottie.a.b.a)var1;
   }

   public static final class a {
      public static f a(JSONObject var0, com.airbnb.lottie.e var1) {
         n.a var2 = n.a(var0, var1.n(), var1, com.airbnb.lottie.c.j.a).a();
         return new f(var2.a, (PointF)var2.b);
      }
   }
}
