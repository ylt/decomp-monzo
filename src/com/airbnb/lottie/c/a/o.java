package com.airbnb.lottie.c.a;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public abstract class o implements m {
   final List a;
   final Object b;

   o(Object var1) {
      this(Collections.emptyList(), var1);
   }

   o(List var1, Object var2) {
      this.a = var1;
      this.b = var2;
   }

   Object a(Object var1) {
      return var1;
   }

   public Object c() {
      return this.a(this.b);
   }

   public boolean d() {
      boolean var1;
      if(!this.a.isEmpty()) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public String toString() {
      StringBuilder var1 = new StringBuilder();
      var1.append("parseInitialValue=").append(this.b);
      if(!this.a.isEmpty()) {
         var1.append(", values=").append(Arrays.toString(this.a.toArray()));
      }

      return var1.toString();
   }
}
