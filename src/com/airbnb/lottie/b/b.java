package com.airbnb.lottie.b;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.graphics.BitmapFactory.Options;
import android.graphics.drawable.Drawable.Callback;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import com.airbnb.lottie.c;
import com.airbnb.lottie.g;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class b {
   private final Context a;
   private String b;
   private c c;
   private final Map d;
   private final Map e = new HashMap();

   public b(Callback var1, String var2, c var3, Map var4) {
      this.b = var2;
      if(!TextUtils.isEmpty(var2) && this.b.charAt(this.b.length() - 1) != 47) {
         this.b = this.b + '/';
      }

      if(!(var1 instanceof View)) {
         Log.w("LOTTIE", "LottieDrawable must be inside of a view for images to work.");
         this.d = new HashMap();
         this.a = null;
      } else {
         this.a = ((View)var1).getContext();
         this.d = var4;
         this.a(var3);
      }

   }

   public Bitmap a(String var1) {
      Bitmap var3 = (Bitmap)this.e.get(var1);
      Bitmap var2 = var3;
      if(var3 == null) {
         g var9 = (g)this.d.get(var1);
         if(var9 == null) {
            var2 = null;
         } else if(this.c != null) {
            var3 = this.c.a(var9);
            var2 = var3;
            if(var3 != null) {
               this.e.put(var1, var3);
               var2 = var3;
            }
         } else {
            InputStream var10;
            try {
               if(TextUtils.isEmpty(this.b)) {
                  IllegalStateException var6 = new IllegalStateException("You must set an images folder before loading an image. Set it with LottieComposition#setImagesFolder or LottieDrawable#setImagesFolder");
                  throw var6;
               }

               AssetManager var4 = this.a.getAssets();
               StringBuilder var7 = new StringBuilder();
               var10 = var4.open(var7.append(this.b).append(var9.b()).toString());
            } catch (IOException var5) {
               Log.w("LOTTIE", "Unable to open asset.", var5);
               var2 = null;
               return var2;
            }

            Options var8 = new Options();
            var8.inScaled = true;
            var8.inDensity = 160;
            var2 = BitmapFactory.decodeStream(var10, (Rect)null, var8);
            this.e.put(var1, var2);
         }
      }

      return var2;
   }

   public void a() {
      Iterator var1 = this.e.entrySet().iterator();

      while(var1.hasNext()) {
         ((Bitmap)((Entry)var1.next()).getValue()).recycle();
         var1.remove();
      }

   }

   public void a(c var1) {
      this.c = var1;
   }

   public boolean a(Context var1) {
      boolean var2;
      if((var1 != null || this.a != null) && (var1 == null || !this.a.equals(var1))) {
         var2 = false;
      } else {
         var2 = true;
      }

      return var2;
   }
}
