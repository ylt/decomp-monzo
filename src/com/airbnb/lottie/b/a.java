package com.airbnb.lottie.b;

import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable.Callback;
import android.util.Log;
import android.view.View;
import com.airbnb.lottie.c.i;
import java.util.HashMap;
import java.util.Map;

public class a {
   private final i a = new i();
   private final Map b = new HashMap();
   private final Map c = new HashMap();
   private final AssetManager d;
   private com.airbnb.lottie.b e;
   private String f = ".ttf";

   public a(Callback var1, com.airbnb.lottie.b var2) {
      this.e = var2;
      if(!(var1 instanceof View)) {
         Log.w("LOTTIE", "LottieDrawable must be inside of a view for images to work.");
         this.d = null;
      } else {
         this.d = ((View)var1).getContext().getAssets();
      }

   }

   private Typeface a(Typeface var1, String var2) {
      byte var3 = 0;
      boolean var4 = var2.contains("Italic");
      boolean var5 = var2.contains("Bold");
      if(var4 && var5) {
         var3 = 3;
      } else if(var4) {
         var3 = 2;
      } else if(var5) {
         var3 = 1;
      }

      if(var1.getStyle() != var3) {
         var1 = Typeface.create(var1, var3);
      }

      return var1;
   }

   private Typeface a(String var1) {
      Typeface var3 = (Typeface)this.c.get(var1);
      if(var3 == null) {
         var3 = null;
         if(this.e != null) {
            var3 = this.e.a(var1);
         }

         Typeface var2 = var3;
         if(this.e != null) {
            var2 = var3;
            if(var3 == null) {
               String var4 = this.e.b(var1);
               var2 = var3;
               if(var4 != null) {
                  var2 = Typeface.createFromAsset(this.d, var4);
               }
            }
         }

         var3 = var2;
         if(var2 == null) {
            String var5 = "fonts/" + var1 + this.f;
            var3 = Typeface.createFromAsset(this.d, var5);
         }

         this.c.put(var1, var3);
      }

      return var3;
   }

   public Typeface a(String var1, String var2) {
      this.a.a(var1, var2);
      Typeface var3 = (Typeface)this.b.get(this.a);
      Typeface var4;
      if(var3 != null) {
         var4 = var3;
      } else {
         var4 = this.a(this.a(var1), var2);
         this.b.put(this.a, var4);
      }

      return var4;
   }

   public void a(com.airbnb.lottie.b var1) {
      this.e = var1;
   }
}
