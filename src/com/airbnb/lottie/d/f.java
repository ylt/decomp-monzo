package com.airbnb.lottie.d;

import android.content.Context;
import android.graphics.Matrix;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.PointF;
import android.os.Build.VERSION;
import android.provider.Settings.Global;
import android.provider.Settings.System;
import com.airbnb.lottie.a.a.q;
import java.io.Closeable;

public final class f {
   private static final PathMeasure a = new PathMeasure();
   private static final Path b = new Path();
   private static final Path c = new Path();
   private static final float[] d = new float[4];
   private static final float e = (float)Math.sqrt(2.0D);

   public static float a(Context var0) {
      float var1;
      if(VERSION.SDK_INT >= 17) {
         var1 = Global.getFloat(var0.getContentResolver(), "animator_duration_scale", 1.0F);
      } else {
         var1 = System.getFloat(var0.getContentResolver(), "animator_duration_scale", 1.0F);
      }

      return var1;
   }

   public static float a(Matrix var0) {
      d[0] = 0.0F;
      d[1] = 0.0F;
      d[2] = e;
      d[3] = e;
      var0.mapPoints(d);
      float var3 = d[2];
      float var2 = d[0];
      float var4 = d[3];
      float var1 = d[1];
      return (float)Math.hypot((double)(var3 - var2), (double)(var4 - var1)) / 2.0F;
   }

   public static int a(float var0, float var1, float var2, float var3) {
      int var4 = 17;
      if(var0 != 0.0F) {
         var4 = (int)((float)527 * var0);
      }

      int var5 = var4;
      if(var1 != 0.0F) {
         var5 = (int)((float)(var4 * 31) * var1);
      }

      var4 = var5;
      if(var2 != 0.0F) {
         var4 = (int)((float)(var5 * 31) * var2);
      }

      var5 = var4;
      if(var3 != 0.0F) {
         var5 = (int)((float)(var4 * 31) * var3);
      }

      return var5;
   }

   public static Path a(PointF var0, PointF var1, PointF var2, PointF var3) {
      Path var4 = new Path();
      var4.moveTo(var0.x, var0.y);
      if(var2 == null || var3 == null || var2.length() == 0.0F && var3.length() == 0.0F) {
         var4.lineTo(var1.x, var1.y);
      } else {
         var4.cubicTo(var0.x + var2.x, var0.y + var2.y, var1.x + var3.x, var1.y + var3.y, var1.x, var1.y);
      }

      return var4;
   }

   public static void a(Path var0, float var1, float var2, float var3) {
      com.airbnb.lottie.d.a("applyTrimPathIfNeeded");
      a.setPath(var0, false);
      float var5 = a.getLength();
      if(var1 == 1.0F && var2 == 0.0F) {
         com.airbnb.lottie.d.b("applyTrimPathIfNeeded");
      } else if(var5 >= 1.0F && (double)Math.abs(var2 - var1 - 1.0F) >= 0.01D) {
         float var4 = var5 * var1;
         var2 = var5 * var2;
         var1 = Math.min(var4, var2);
         var4 = Math.max(var4, var2);
         var3 *= var5;
         var2 = var1 + var3;
         var4 += var3;
         var3 = var2;
         var1 = var4;
         if(var2 >= var5) {
            var3 = var2;
            var1 = var4;
            if(var4 >= var5) {
               var3 = (float)e.a(var2, var5);
               var1 = (float)e.a(var4, var5);
            }
         }

         var2 = var3;
         if(var3 < 0.0F) {
            var2 = (float)e.a(var3, var5);
         }

         var3 = var1;
         if(var1 < 0.0F) {
            var3 = (float)e.a(var1, var5);
         }

         if(var2 == var3) {
            var0.reset();
            com.airbnb.lottie.d.b("applyTrimPathIfNeeded");
         } else {
            var1 = var2;
            if(var2 >= var3) {
               var1 = var2 - var5;
            }

            b.reset();
            a.getSegment(var1, var3, b, true);
            if(var3 > var5) {
               c.reset();
               a.getSegment(0.0F, var3 % var5, c, true);
               b.addPath(c);
            } else if(var1 < 0.0F) {
               c.reset();
               a.getSegment(var1 + var5, var5, c, true);
               b.addPath(c);
            }

            var0.set(b);
            com.airbnb.lottie.d.b("applyTrimPathIfNeeded");
         }
      } else {
         com.airbnb.lottie.d.b("applyTrimPathIfNeeded");
      }

   }

   public static void a(Path var0, q var1) {
      if(var1 != null) {
         a(var0, ((Float)var1.d().b()).floatValue() / 100.0F, ((Float)var1.e().b()).floatValue() / 100.0F, ((Float)var1.f().b()).floatValue() / 360.0F);
      }

   }

   public static void a(Closeable var0) {
      if(var0 != null) {
         try {
            var0.close();
         } catch (RuntimeException var1) {
            throw var1;
         } catch (Exception var2) {
            ;
         }
      }

   }

   public static boolean a(com.airbnb.lottie.e var0, int var1, int var2, int var3) {
      boolean var5 = true;
      boolean var4;
      if(var0.d() < var1) {
         var4 = false;
      } else {
         var4 = var5;
         if(var0.d() <= var1) {
            if(var0.e() < var2) {
               var4 = false;
            } else {
               var4 = var5;
               if(var0.e() <= var2) {
                  var4 = var5;
                  if(var0.f() < var3) {
                     var4 = false;
                  }
               }
            }
         }
      }

      return var4;
   }
}
