package com.airbnb.lottie.d;

import android.graphics.Path;
import android.graphics.PointF;
import com.airbnb.lottie.c.b.l;

public class e {
   public static double a(double var0, double var2, double var4) {
      return (var2 - var0) * var4 + var0;
   }

   public static float a(float var0, float var1, float var2) {
      return (var1 - var0) * var2 + var0;
   }

   public static int a(float var0, float var1) {
      return a((int)var0, (int)var1);
   }

   public static int a(int var0, int var1) {
      return var0 - b(var0, var1) * var1;
   }

   public static int a(int var0, int var1, float var2) {
      return (int)((float)var0 + (float)(var1 - var0) * var2);
   }

   public static PointF a(PointF var0, PointF var1) {
      return new PointF(var0.x + var1.x, var0.y + var1.y);
   }

   public static void a(l var0, Path var1) {
      var1.reset();
      PointF var3 = var0.a();
      var1.moveTo(var3.x, var3.y);
      PointF var5 = new PointF(var3.x, var3.y);

      for(int var2 = 0; var2 < var0.c().size(); ++var2) {
         com.airbnb.lottie.c.c var6 = (com.airbnb.lottie.c.c)var0.c().get(var2);
         PointF var4 = var6.a();
         var3 = var6.b();
         PointF var7 = var6.c();
         if(var4.equals(var5) && var3.equals(var7)) {
            var1.lineTo(var7.x, var7.y);
         } else {
            var1.cubicTo(var4.x, var4.y, var3.x, var3.y, var7.x, var7.y);
         }

         var5.set(var7.x, var7.y);
      }

      if(var0.b()) {
         var1.close();
      }

   }

   public static float b(float var0, float var1, float var2) {
      return Math.max(var1, Math.min(var2, var0));
   }

   private static int b(int var0, int var1) {
      int var3 = var0 / var1;
      int var2 = var3;
      if((var0 ^ var1) < 0) {
         var2 = var3;
         if(var3 * var1 != var0) {
            var2 = var3 - 1;
         }
      }

      return var2;
   }
}
