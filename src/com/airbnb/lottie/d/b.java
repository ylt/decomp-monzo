package com.airbnb.lottie.d;

import android.graphics.PointF;
import org.json.JSONArray;
import org.json.JSONObject;

public class b {
   public static float a(Object var0) {
      float var1;
      if(var0 instanceof Float) {
         var1 = ((Float)var0).floatValue();
      } else if(var0 instanceof Integer) {
         var1 = (float)((Integer)var0).intValue();
      } else if(var0 instanceof Double) {
         var1 = (float)((Double)var0).doubleValue();
      } else if(var0 instanceof JSONArray) {
         var1 = (float)((JSONArray)var0).optDouble(0);
      } else {
         var1 = 0.0F;
      }

      return var1;
   }

   public static PointF a(JSONArray var0, float var1) {
      if(var0.length() < 2) {
         throw new IllegalArgumentException("Unable to parse point for " + var0);
      } else {
         return new PointF((float)var0.optDouble(0, 1.0D) * var1, (float)var0.optDouble(1, 1.0D) * var1);
      }
   }

   public static PointF a(JSONObject var0, float var1) {
      return new PointF(a(var0.opt("x")) * var1, a(var0.opt("y")) * var1);
   }
}
