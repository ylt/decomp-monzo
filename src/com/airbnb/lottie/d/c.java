package com.airbnb.lottie.d;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;

public class c extends ValueAnimator {
   private boolean a = false;
   private boolean b = false;
   private float c = 0.0F;
   private float d = 1.0F;
   private long e;
   private float f = 0.0F;

   public c() {
      this.setFloatValues(new float[]{0.0F, 1.0F});
      this.addListener(new AnimatorListenerAdapter() {
         public void onAnimationCancel(Animator var1) {
            c.this.a(c.this.c, c.this.d);
         }

         public void onAnimationEnd(Animator var1) {
            c.this.a(c.this.c, c.this.d);
         }
      });
      this.addUpdateListener(new AnimatorUpdateListener() {
         public void onAnimationUpdate(ValueAnimator var1) {
            if(!c.this.a) {
               c.this.f = ((Float)var1.getAnimatedValue()).floatValue();
            }

         }
      });
   }

   private void d(float var1) {
      float var2;
      if(var1 < this.c) {
         var2 = this.c;
      } else {
         var2 = var1;
         if(var1 > this.d) {
            var2 = this.d;
         }
      }

      this.f = var2;
      if(this.getDuration() > 0L) {
         this.setCurrentPlayTime((long)((var2 - this.c) / (this.d - this.c) * (float)this.getDuration()));
      }

   }

   public void a() {
      this.a = true;
   }

   public void a(float var1) {
      if(this.f != var1) {
         this.d(var1);
      }

   }

   public void a(float var1, float var2) {
      float var3 = Math.min(var1, var2);
      var1 = Math.max(var1, var2);
      if(this.b) {
         var2 = var1;
      } else {
         var2 = var3;
      }

      float var4;
      if(this.b) {
         var4 = var3;
      } else {
         var4 = var1;
      }

      this.setFloatValues(new float[]{var2, var4});
      super.setDuration((long)((float)this.e * (var1 - var3)));
      this.a(this.c());
   }

   public void a(boolean var1) {
      this.b = var1;
      this.a(this.c, this.d);
   }

   public void b() {
      this.d(this.c());
   }

   public void b(float var1) {
      this.c = var1;
      this.a(var1, this.d);
   }

   public float c() {
      return this.f;
   }

   public void c(float var1) {
      this.d = var1;
      this.a(this.c, var1);
   }

   public float d() {
      return this.d;
   }

   public void e() {
      float var1 = this.f;
      this.start();
      this.a(var1);
   }

   public ValueAnimator setDuration(long var1) {
      this.e = var1;
      this.a(this.c, this.d);
      return this;
   }

   public void start() {
      if(this.a) {
         this.a(this.d());
         this.end();
      } else {
         super.start();
      }

   }
}
