package com.airbnb.lottie.d;

public class a {
   private static float a(float var0) {
      if(var0 <= 0.0031308F) {
         var0 = 12.92F * var0;
      } else {
         var0 = (float)(Math.pow((double)var0, 0.4166666567325592D) * 1.0549999475479126D - 0.054999999701976776D);
      }

      return var0;
   }

   public static int a(float var0, int var1, int var2) {
      float var4 = (float)(var1 >> 24 & 255) / 255.0F;
      float var5 = (float)(var1 >> 16 & 255) / 255.0F;
      float var8 = (float)(var1 >> 8 & 255) / 255.0F;
      float var6 = (float)(var1 & 255) / 255.0F;
      float var3 = (float)(var2 >> 24 & 255) / 255.0F;
      float var10 = (float)(var2 >> 16 & 255) / 255.0F;
      float var9 = (float)(var2 >> 8 & 255) / 255.0F;
      float var7 = (float)(var2 & 255) / 255.0F;
      var5 = b(var5);
      var8 = b(var8);
      var6 = b(var6);
      var10 = b(var10);
      var9 = b(var9);
      var7 = b(var7);
      var5 = a(var5 + (var10 - var5) * var0);
      var8 = a(var8 + (var9 - var8) * var0);
      var6 = a(var6 + (var7 - var6) * var0);
      return Math.round((var4 + (var3 - var4) * var0) * 255.0F) << 24 | Math.round(var5 * 255.0F) << 16 | Math.round(var8 * 255.0F) << 8 | Math.round(var6 * 255.0F);
   }

   private static float b(float var0) {
      if(var0 <= 0.04045F) {
         var0 /= 12.92F;
      } else {
         var0 = (float)Math.pow((double)((0.055F + var0) / 1.055F), 2.4000000953674316D);
      }

      return var0;
   }
}
