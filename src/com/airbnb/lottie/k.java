package com.airbnb.lottie;

import android.graphics.PorterDuffColorFilter;
import android.graphics.PorterDuff.Mode;

public class k extends PorterDuffColorFilter {
   public k(int var1) {
      super(var1, Mode.SRC_ATOP);
   }
}
