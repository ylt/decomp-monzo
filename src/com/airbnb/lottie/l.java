package com.airbnb.lottie;

import java.util.Map;

public class l {
   private final Map a;
   private boolean b;

   public String a(String var1) {
      return var1;
   }

   public final String b(String var1) {
      String var2;
      if(this.b && this.a.containsKey(var1)) {
         var2 = (String)this.a.get(var1);
      } else {
         String var3 = this.a(var1);
         var2 = var3;
         if(this.b) {
            this.a.put(var1, var3);
            var2 = var3;
         }
      }

      return var2;
   }
}
