package com.airbnb.lottie;

import android.animation.ValueAnimator;
import android.animation.Animator.AnimatorListener;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Drawable.Callback;
import android.os.Build.VERSION;
import android.util.Log;
import android.view.View;
import android.view.animation.LinearInterpolator;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class f extends Drawable implements Callback {
   private static final String c = f.class.getSimpleName();
   b a;
   l b;
   private boolean d;
   private final Matrix e = new Matrix();
   private e f;
   private final com.airbnb.lottie.d.c g = new com.airbnb.lottie.d.c();
   private float h = 1.0F;
   private float i = 1.0F;
   private final Set j = new HashSet();
   private final ArrayList k = new ArrayList();
   private com.airbnb.lottie.b.b l;
   private String m;
   private c n;
   private com.airbnb.lottie.b.a o;
   private boolean p;
   private com.airbnb.lottie.c.c.b q;
   private int r = 255;
   private boolean s;

   public f() {
      this.g.setRepeatCount(0);
      this.g.setInterpolator(new LinearInterpolator());
      this.g.addUpdateListener(new AnimatorUpdateListener() {
         public void onAnimationUpdate(ValueAnimator var1) {
            if(f.this.q != null) {
               f.this.q.a(f.this.g.c());
            }

         }
      });
   }

   private float a(Canvas var1) {
      return Math.min((float)var1.getWidth() / (float)this.f.b().width(), (float)var1.getHeight() / (float)this.f.b().height());
   }

   private void a(String var1, String var2, ColorFilter var3) {
      f.a var4 = new f.a(var1, var2, var3);
      if(var3 == null && this.j.contains(var4)) {
         this.j.remove(var4);
      } else {
         this.j.add(new f.a(var1, var2, var3));
      }

      if(this.q != null) {
         this.q.a(var1, var2, var3);
      }

   }

   private void d(final boolean var1) {
      if(this.q == null) {
         this.k.add(new f.b() {
            public void a(e var1x) {
               f.this.d(var1);
            }
         });
      } else if(var1) {
         this.g.start();
      } else {
         this.g.e();
      }

   }

   private void o() {
      this.q = new com.airbnb.lottie.c.c.b(this, com.airbnb.lottie.c.c.d.a.a(this.f), this.f.i(), this.f);
   }

   private void p() {
      if(this.q != null) {
         Iterator var1 = this.j.iterator();

         while(var1.hasNext()) {
            f.a var2 = (f.a)var1.next();
            this.q.a(var2.a, var2.b, var2.c);
         }
      }

   }

   private void q() {
      this.c();
      this.q = null;
      this.l = null;
      this.invalidateSelf();
   }

   private void r() {
      if(this.f != null) {
         float var1 = this.l();
         this.setBounds(0, 0, (int)((float)this.f.b().width() * var1), (int)(var1 * (float)this.f.b().height()));
      }

   }

   private com.airbnb.lottie.b.b s() {
      com.airbnb.lottie.b.b var1 = null;
      if(this.getCallback() != null) {
         if(this.l != null && !this.l.a(this.u())) {
            this.l.a();
            this.l = null;
         }

         if(this.l == null) {
            this.l = new com.airbnb.lottie.b.b(this.getCallback(), this.m, this.n, this.f.l());
         }

         var1 = this.l;
      }

      return var1;
   }

   private com.airbnb.lottie.b.a t() {
      com.airbnb.lottie.b.a var1;
      if(this.getCallback() == null) {
         var1 = null;
      } else {
         if(this.o == null) {
            this.o = new com.airbnb.lottie.b.a(this.getCallback(), this.a);
         }

         var1 = this.o;
      }

      return var1;
   }

   private Context u() {
      Callback var1 = this.getCallback();
      Context var2;
      if(var1 == null) {
         var2 = null;
      } else if(var1 instanceof View) {
         var2 = ((View)var1).getContext();
      } else {
         var2 = null;
      }

      return var2;
   }

   public Typeface a(String var1, String var2) {
      com.airbnb.lottie.b.a var3 = this.t();
      Typeface var4;
      if(var3 != null) {
         var4 = var3.a(var1, var2);
      } else {
         var4 = null;
      }

      return var4;
   }

   public void a(float var1) {
      this.g.b(var1);
   }

   public void a(final int var1) {
      if(this.f == null) {
         this.k.add(new f.b() {
            public void a(e var1x) {
               f.this.a(var1);
            }
         });
      } else {
         this.a((float)var1 / this.f.m());
      }

   }

   public void a(AnimatorListener var1) {
      this.g.addListener(var1);
   }

   public void a(ColorFilter var1) {
      this.a((String)null, (String)null, var1);
   }

   public void a(b var1) {
      this.a = var1;
      if(this.o != null) {
         this.o.a(var1);
      }

   }

   public void a(c var1) {
      this.n = var1;
      if(this.l != null) {
         this.l.a(var1);
      }

   }

   public void a(l var1) {
      this.b = var1;
   }

   public void a(String var1) {
      this.m = var1;
   }

   public void a(boolean var1) {
      if(VERSION.SDK_INT < 19) {
         Log.w(c, "Merge paths are not supported pre-Kit Kat.");
      } else {
         this.p = var1;
         if(this.f != null) {
            this.o();
         }
      }

   }

   public boolean a() {
      return this.p;
   }

   public boolean a(e var1) {
      boolean var2;
      if(this.f == var1) {
         var2 = false;
      } else {
         this.q();
         this.f = var1;
         this.c(this.h);
         this.e(this.i);
         this.r();
         this.o();
         this.p();
         Iterator var3 = (new ArrayList(this.k)).iterator();

         while(var3.hasNext()) {
            ((f.b)var3.next()).a(var1);
            var3.remove();
         }

         this.k.clear();
         var1.a(this.s);
         this.g.b();
         var2 = true;
      }

      return var2;
   }

   public Bitmap b(String var1) {
      com.airbnb.lottie.b.b var2 = this.s();
      Bitmap var3;
      if(var2 != null) {
         var3 = var2.a(var1);
      } else {
         var3 = null;
      }

      return var3;
   }

   public String b() {
      return this.m;
   }

   public void b(float var1) {
      this.g.c(var1);
   }

   public void b(final int var1) {
      if(this.f == null) {
         this.k.add(new f.b() {
            public void a(e var1x) {
               f.this.b(var1);
            }
         });
      } else {
         this.b((float)var1 / this.f.m());
      }

   }

   public void b(boolean var1) {
      this.s = var1;
      if(this.f != null) {
         this.f.a(var1);
      }

   }

   public void c() {
      if(this.l != null) {
         this.l.a();
      }

   }

   public void c(float var1) {
      this.h = var1;
      com.airbnb.lottie.d.c var3 = this.g;
      boolean var2;
      if(var1 < 0.0F) {
         var2 = true;
      } else {
         var2 = false;
      }

      var3.a(var2);
      if(this.f != null) {
         this.g.setDuration((long)((float)this.f.c() / Math.abs(var1)));
      }

   }

   public void c(boolean var1) {
      com.airbnb.lottie.d.c var3 = this.g;
      byte var2;
      if(var1) {
         var2 = -1;
      } else {
         var2 = 0;
      }

      var3.setRepeatCount(var2);
   }

   public i d() {
      i var1;
      if(this.f != null) {
         var1 = this.f.a();
      } else {
         var1 = null;
      }

      return var1;
   }

   public void d(float var1) {
      this.g.a(var1);
      if(this.q != null) {
         this.q.a(var1);
      }

   }

   public void draw(Canvas var1) {
      d.a("Drawable#draw");
      if(this.q != null) {
         float var3 = this.i;
         float var2 = this.a(var1);
         if(var3 > var2) {
            var3 = this.i / var2;
         } else {
            var2 = var3;
            var3 = 1.0F;
         }

         if(var3 > 1.0F) {
            var1.save();
            float var6 = (float)this.f.b().width() / 2.0F;
            float var7 = (float)this.f.b().height() / 2.0F;
            float var5 = var6 * var2;
            float var4 = var7 * var2;
            var1.translate(var6 * this.l() - var5, var7 * this.l() - var4);
            var1.scale(var3, var3, var5, var4);
         }

         this.e.reset();
         this.e.preScale(var2, var2);
         this.q.a(var1, this.e, this.r);
         d.b("Drawable#draw");
         if(var3 > 1.0F) {
            var1.restore();
         }
      }

   }

   void e() {
      this.d = true;
      this.g.a();
   }

   public void e(float var1) {
      this.i = var1;
      this.r();
   }

   public boolean f() {
      boolean var1;
      if(this.g.getRepeatCount() == -1) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public boolean g() {
      return this.g.isRunning();
   }

   public int getAlpha() {
      return this.r;
   }

   public int getIntrinsicHeight() {
      int var1;
      if(this.f == null) {
         var1 = -1;
      } else {
         var1 = (int)((float)this.f.b().height() * this.l());
      }

      return var1;
   }

   public int getIntrinsicWidth() {
      int var1;
      if(this.f == null) {
         var1 = -1;
      } else {
         var1 = (int)((float)this.f.b().width() * this.l());
      }

      return var1;
   }

   public int getOpacity() {
      return -3;
   }

   public void h() {
      this.d(true);
   }

   public float i() {
      return this.g.c();
   }

   public void invalidateDrawable(Drawable var1) {
      Callback var2 = this.getCallback();
      if(var2 != null) {
         var2.invalidateDrawable(this);
      }

   }

   public void invalidateSelf() {
      Callback var1 = this.getCallback();
      if(var1 != null) {
         var1.invalidateDrawable(this);
      }

   }

   public l j() {
      return this.b;
   }

   public boolean k() {
      boolean var1;
      if(this.b == null && this.f.j().b() > 0) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public float l() {
      return this.i;
   }

   public e m() {
      return this.f;
   }

   public void n() {
      this.k.clear();
      this.g.cancel();
   }

   public void scheduleDrawable(Drawable var1, Runnable var2, long var3) {
      Callback var5 = this.getCallback();
      if(var5 != null) {
         var5.scheduleDrawable(this, var2, var3);
      }

   }

   public void setAlpha(int var1) {
      this.r = var1;
   }

   public void setColorFilter(ColorFilter var1) {
      throw new UnsupportedOperationException("Use addColorFilter instead.");
   }

   public void unscheduleDrawable(Drawable var1, Runnable var2) {
      Callback var3 = this.getCallback();
      if(var3 != null) {
         var3.unscheduleDrawable(this, var2);
      }

   }

   private static class a {
      final String a;
      final String b;
      final ColorFilter c;

      a(String var1, String var2, ColorFilter var3) {
         this.a = var1;
         this.b = var2;
         this.c = var3;
      }

      public boolean equals(Object var1) {
         boolean var2 = true;
         if(this != var1) {
            if(!(var1 instanceof f.a)) {
               var2 = false;
            } else {
               f.a var3 = (f.a)var1;
               if(this.hashCode() != var3.hashCode() || this.c != var3.c) {
                  var2 = false;
               }
            }
         }

         return var2;
      }

      public int hashCode() {
         int var1 = 17;
         if(this.a != null) {
            var1 = this.a.hashCode() * 527;
         }

         int var2 = var1;
         if(this.b != null) {
            var2 = var1 * 31 * this.b.hashCode();
         }

         return var2;
      }
   }

   private interface b {
      void a(e var1);
   }
}
