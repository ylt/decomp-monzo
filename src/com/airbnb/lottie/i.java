package com.airbnb.lottie;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class i {
   private boolean a = false;
   private final Set b = new android.support.v4.g.b();
   private Map c = new HashMap();
   private final Comparator d = new Comparator() {
      public int a(android.support.v4.g.j var1, android.support.v4.g.j var2) {
         float var4 = ((Float)var1.b).floatValue();
         float var3 = ((Float)var2.b).floatValue();
         byte var5;
         if(var3 > var4) {
            var5 = 1;
         } else if(var4 > var3) {
            var5 = -1;
         } else {
            var5 = 0;
         }

         return var5;
      }

      // $FF: synthetic method
      public int compare(Object var1, Object var2) {
         return this.a((android.support.v4.g.j)var1, (android.support.v4.g.j)var2);
      }
   };

   public void a(String var1, float var2) {
      if(this.a) {
         com.airbnb.lottie.d.d var4 = (com.airbnb.lottie.d.d)this.c.get(var1);
         com.airbnb.lottie.d.d var3 = var4;
         if(var4 == null) {
            var3 = new com.airbnb.lottie.d.d();
            this.c.put(var1, var3);
         }

         var3.a(var2);
         if(var1.equals("root")) {
            Iterator var5 = this.b.iterator();

            while(var5.hasNext()) {
               ((i.a)var5.next()).a(var2);
            }
         }
      }

   }

   void a(boolean var1) {
      this.a = var1;
   }

   public interface a {
      void a(float var1);
   }
}
