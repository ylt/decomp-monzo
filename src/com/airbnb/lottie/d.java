package com.airbnb.lottie;

public class d {
   private static boolean a = false;
   private static String[] b;
   private static long[] c;
   private static int d = 0;
   private static int e = 0;

   public static void a(String var0) {
      if(a) {
         if(d == 20) {
            ++e;
         } else {
            b[d] = var0;
            c[d] = System.nanoTime();
            android.support.v4.os.e.a(var0);
            ++d;
         }
      }

   }

   public static float b(String var0) {
      float var1 = 0.0F;
      if(e > 0) {
         --e;
      } else if(a) {
         --d;
         if(d == -1) {
            throw new IllegalStateException("Can't end trace section. There are none.");
         }

         if(!var0.equals(b[d])) {
            throw new IllegalStateException("Unbalanced trace call " + var0 + ". Expected " + b[d] + ".");
         }

         android.support.v4.os.e.a();
         var1 = (float)(System.nanoTime() - c[d]) / 1000000.0F;
      }

      return var1;
   }
}
