package com.squareup.moshi;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.annotation.Nullable;

public final class v {
   static final List a = new ArrayList(5);
   private final List b;
   private final ThreadLocal c = new ThreadLocal();
   private final Map d = new LinkedHashMap();

   static {
      a.add(w.a);
      a.add(f.a);
      a.add(u.a);
      a.add(c.a);
      a.add(e.a);
   }

   v(v.a var1) {
      ArrayList var2 = new ArrayList(var1.a.size() + a.size());
      var2.addAll(var1.a);
      var2.addAll(a);
      this.b = Collections.unmodifiableList(var2);
   }

   private Object b(Type var1, Set var2) {
      if(!var2.isEmpty()) {
         var1 = Arrays.asList(new Object[]{var1, var2});
      }

      return var1;
   }

   public i a(i.a var1, Type var2, Set var3) {
      var2 = y.d(var2);
      int var4 = this.b.indexOf(var1);
      if(var4 == -1) {
         throw new IllegalArgumentException("Unable to skip past unknown factory " + var1);
      } else {
         int var5 = this.b.size();
         ++var4;

         while(var4 < var5) {
            i var6 = ((i.a)this.b.get(var4)).a(var2, var3, this);
            if(var6 != null) {
               return var6;
            }

            ++var4;
         }

         throw new IllegalArgumentException("No next JsonAdapter for " + var2 + " annotated " + var3);
      }
   }

   public i a(Type var1) {
      return this.a(var1, z.a);
   }

   public i a(Type param1, Set param2) {
      // $FF: Couldn't be decompiled
   }

   public static final class a {
      final List a = new ArrayList();

      public v.a a(i.a var1) {
         if(var1 == null) {
            throw new IllegalArgumentException("factory == null");
         } else {
            this.a.add(var1);
            return this;
         }
      }

      public v.a a(Object var1) {
         if(var1 == null) {
            throw new IllegalArgumentException("adapter == null");
         } else {
            return this.a((i.a)b.a(var1));
         }
      }

      public v a() {
         return new v(this);
      }
   }

   private static class b extends i {
      @Nullable
      Object a;
      @Nullable
      private i b;

      b(Object var1) {
         this.a = var1;
      }

      public Object a(k var1) throws IOException {
         if(this.b == null) {
            throw new IllegalStateException("Type adapter isn't ready");
         } else {
            return this.b.a(var1);
         }
      }

      void a(i var1) {
         this.b = var1;
         this.a = null;
      }

      public void a(p var1, Object var2) throws IOException {
         if(this.b == null) {
            throw new IllegalStateException("Type adapter isn't ready");
         } else {
            this.b.a(var1, var2);
         }
      }
   }
}
